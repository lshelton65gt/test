/*
 *
 * [ File Version : 1.199 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h> // strchr

#include "VhdlValue_Elab.h"

// Operators are implementation of VhdlNonconst class routines

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Strings.h"
#include "RuntimeFlags.h" // for run-time flags

#include "vhdl_tokens.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlNonconst *
VhdlNonconst::Invert(const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from) return 0 ;

#ifdef DB_INFER_WIDE_OPERATORS
    // Create wide inverter.
    // Do this only for multibit operations on non-constants
    if (Size()>1 && !IsConstant() && _nl) {
        Array *outputs = new Array(Size()) ;
        (void) _nl->WideInv(_nets, outputs, from->Linefile()) ;
        delete _nets ;
        _nets = outputs ;
        return this ;
    }
#endif // DB_INFER_WIDE_OPERATORS
    // Invert every bit individually
    Net *net ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_nets, i, net) {
        _nets->Insert(i,from->Inv(net)) ;
    }
    return this ; // For convenience
}

VhdlNonconstBit *
VhdlNonconst::Reduce(unsigned type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from) return 0 ;

    // Determine default return value. (VIPER #2068)
    // Defaults taken from Synopsys' pragma for reduction of zero-size arrays.
    Net *o = 0;
    switch (type) {
    case VHDL_REDAND    :
    case VHDL_REDNAND   : o = Pwr() ; break ;
    case VHDL_REDOR     :
    case VHDL_REDNOR    : o = Gnd() ; break ;
    case VHDL_REDXOR    :
    case VHDL_REDXNOR   : o = Gnd() ; break ;
    default: VERIFIC_ASSERT(0) ;
    }

    // Don't worry about VHDL semantics for X nets in reduction operations
    // VHDL does not define Reduction operators, and they (X nets) are
    // filtered out in VHDL Equal routine before they get here.
    // So we don't care about them (just let them go through regular logic,
    // like other binary operations
    unsigned done = 0 ;
    Set *reduce = 0 ;
    Net *net ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_nets, i, net) {
        if (done) break ;
        switch (type) {
        case VHDL_REDAND :
        case VHDL_REDNAND :
            if (net==Gnd()) { o = Gnd() ; done = 1 ; continue ;}  // Done
            if (net==Pwr()) continue ; // bit ignored
            // fall through to next case

        case VHDL_REDOR :
        case VHDL_REDNOR :
            if (net==Pwr()) { o = Pwr() ; done = 1 ; continue ;} // Done
            if (net==Gnd()) continue ; // bit ignored
            break ;
        case VHDL_REDXOR :
        case VHDL_REDXNOR :
            if (net==Gnd()) continue ; // bit ignored
            if (reduce) break ; // Once this is determined to be non-constant, remaining bits need to be reduced
            if (net==o)     { o = Gnd() ; continue ; }
            if (net==Pwr()) { o = Pwr() ; continue ; }
            break ;
        default : VERIFIC_ASSERT(0) ;
        }

        // If execution gets here, then this means this is a non-constant bit

        if (!reduce) {
            reduce = new Set(POINTER_HASH, _nets->Size()) ; // Use Set to prevent duplicates.
            // If 'o' is Pwr(), then we need to insert this into the set.
            if ((o == Pwr()) && (type == VHDL_REDXOR || type == VHDL_REDXNOR)) (void) reduce->Insert(o) ;
        }

        if (!reduce->Insert(net)) {
            // Optimization:  If insertion fails, then this net is already in the set.
            // We need to remove it then only for xor/xnors.  This is a nice, quick optimization trick.
            if ((type == VHDL_REDXOR) || (type == VHDL_REDXNOR)) (void) reduce->Remove(net) ;
        }
    }

    // NOTE : if 'done' == 1, then output 'o' should be used

    if (!done && reduce && reduce->Size()>1 && _nl) {
    } else {
        if (!done && reduce && reduce->Size() == 1) {
            // Single-wire reduction
            o = (Net*)reduce->GetAt(0) ;
        }
        // Fix inversion for single-input stuff
        switch (type) {
        case VHDL_REDNAND   :
        case VHDL_REDNOR    :
        case VHDL_REDXNOR   :
            o = from->Inv(o) ;
            break ;
        default : break ; // no inversion
        }
    }

    // Result is a single-bit value
    delete reduce ;
    delete this ;
    return new VhdlNonconstBit(o) ;
}

VhdlNonconst *
VhdlNonconst::Abs(const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!_is_signed) return this ;

    if (_nets->Size()==0) return this ; // ?

    if (!from) return this ;

#if 0
    // CHECKME: Do we want to instantiate an "abs" operator if the size
    // of 'this' value is 1 (_nets->Size()==1)? We can simply return 'this'
    // in such case since that will be the resulting netlist at the end.

    // Instantiate Abs operator in netlist:
    Array *output = new Array(_nets->Size()) ;
    (void) _nl->Abs(_nets, output, from->Linefile()) ;

    // We should make the result (this) unsigned
    _is_signed = 0 ;

    // Push output nets into result :
    delete _nets ;
    _nets = output ;

    return this ;
#endif

    // Since result is always unsigned, we don't need an additional
    // bit. But we cannot strip-off a bit either :
    // ABS(-4(signed)) == ABS("100"(signed)) = 4(unsigned) == "100"(unsigned)
    // So we need same amount of bits in and out.

    // Sign bit 0 : return this + 0 ;
    // Sign bit 1 : return -this == (-this-1) + 1 == NOT(this) + 1
    // So : use sign bit to XOR every bit on 'a' side of adder,
    //      and as input to ci.
    //      'b' side of adder is all '0'
    // Then pump them through an unsigned adder.
    Net *sign_bit = (Net*)_nets->At(0) ; // Sign bit (MSB) is at index 0

    // XOR every bit of me with sign bit
    unsigned i ;
    Net *n ;
    FOREACH_ARRAY_ITEM(_nets, i, n) {
        n = from->Xor(n, sign_bit) ;
        _nets->Insert(i, n) ;
    }

    // Create right side '0' nonconst
    VhdlNonconst *right = (new VhdlInt((verific_int64)0))->ToNonconst() ;

    // We should make the result (this) unsigned
    _is_signed = 0 ;

    // Now add me with 'right', using sign_bit as carry
    return Plus(right, sign_bit, from) ;
}

VhdlNonconst *
VhdlNonconst::UnaryMin(const VhdlTreeNode *from, Net *carry_in)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from) return 0 ;

    // Optional unary minus.
    // Do a unary minus of the value, if carry_in is 1.
    // Pass value through if carry_in is 0.

    // If incoming value is 'unsigned', result is 'signed' ; we will need one more bit :
    // (-(3(unsigned) = -("11"(unsigned)) = "101"(signed) = -3(signed))
    // If incoming value is 'signed', result is signed ; we also need one more bit :
    // (-(-4) = -("100"(signed)) = "0100"(signed) = 4(signed)

    // Issue 3024 : ALWAYS adjust to one bit larger. Also for WIDE_OPERATORS.
    Adjust(Size()+1) ;

    // VIPER #3812: Make it signed here so that it effects wide operators also.
    // Make it always 'signed', so Plus() does the right thing
    // We can do this, since we adjusted one bit larger than needed.
    SetSigned() ;

#ifdef DB_INFER_WIDE_OPERATORS
    // Wide Operator : Call unary minus directly
    if (!IsConstant() && _nl && carry_in==Pwr()) {
        Array *outputs = new Array(Size()) ;
        (void) _nl->UnaryMinus(_nets, outputs, from->Linefile()) ;
        delete _nets ;
        _nets = outputs ;
        return this ;
    }
#endif // DB_INFER_WIDE_OPERATORS

    // Use this formula :
    // -a == (-a-1) + 1 == NOT(a) + 1

    // First, invert all my bits (if carry_in is 1) :
    // XOR every bit of me with carry_in bit
    unsigned i ;
    Net *n ;
    FOREACH_ARRAY_ITEM(_nets, i, n) {
        n = from->Xor(n, carry_in) ;
        _nets->Insert(i, n) ;
    }

    // Create right side '0' nonconst
    VhdlNonconst *right = (new VhdlInt((verific_int64)0))->ToNonconst() ;

    // Now just add ; carry-in (if 1) will do the increment
    (void) Plus(right, carry_in, from) ;
    return this ;
}

/********************************************************************************************/
/* Selector logic : (binary)Nto1MUX, (onehot)Selector, (prio'ed)PrioSelector                */
/* These guys are used for multi-element selection (such as case statement resolving        */
/* All static routines (work just off the 'input_values' to determine a good result value   */
/********************************************************************************************/
#ifdef DB_INFER_WIDE_OPERATORS
/* static */ unsigned
VhdlValue::AreAllInputSetsEqual(const Array *inputs, unsigned size, Array *one_set /* = 0 */)
{
    if (one_set) one_set->Reset() ; // Always clear the Array first

    if (!size || !inputs || !inputs->Size()) return 1 ; // Empty input set is always equal

    // Find the number of input sets we need to check:
    unsigned input_sets = inputs->Size() / size ;

    // This equality should always hold, otherwise we have something bad:
    VERIFIC_ASSERT((input_sets*size) == inputs->Size()) ;

    // Pick up run-time flag :
    unsigned long preserve_x = RuntimeFlags::GetVar("vhdl_preserve_x") ;

    // Check all the input sets for equality (considering X's as don't care):
    for (unsigned i=0; i<size; i++) {
        const Net *input = 0 ;
        // Check the i th input net of all input sets:
        for (unsigned j=0; j<input_sets; j++) {
            // Get the i'th net of j'th input set:
            const Net *net = (Net *)inputs->At(j*size+i) ;
            // Insert this input net or X for don't care into the Array for the first time:
            if (one_set && !j) one_set->Insert(i, ((net) ? net : X())) ;
            if (!net) continue ;                    // Don't care condition

            // VIPER #3034 (Issue 2): Consider X nets as don't care while checking inputs for equality:
            if (!preserve_x && net->IsX()) continue ;              // X matches to anything

            if (!input) input = net ;               // Take this to match with other inputs
            if (input != net) {
                if (one_set) one_set->Reset() ;
                return 0 ;                          // Failed: Input sets are not equal
            }
            if (one_set) one_set->Insert(i, net) ;  // Overwrite this position with this valid input
        }
    }

    return 1 ; // All input sets are equal
}
#endif // DB_INFER_WIDE_OPERATORS

VhdlValue *
VhdlValue::Nto1Mux(const VhdlNonconst *condition, const Array *input_values, const VhdlTreeNode *from)
{
    if (!from) return 0 ;
    if (!input_values) return 0 ; // Return 'don't-care' value (0). Correct ?

    // If there is only one value, take it : condition MUST be 0 bits for this to be valid. Check this ??
    VhdlValue *item_val ;
    if (input_values->Size()==1) {
        item_val = (VhdlValue*)input_values->GetFirst() ;
        // 'item_val' can be 0, if value is don't care.
        return (item_val) ? item_val->Copy() : 0 ;
    }

    // Determine if we are dealing with 'composite', 'bit' or a 'multi-bit' values.
    unsigned i, j ;
    unsigned is_bit = 0 ;
    unsigned is_multibit = 0 ;
    unsigned is_composite = 0 ;
#ifdef DB_INFER_WIDE_OPERATORS
    unsigned is_bitvector = 0 ;
#endif // DB_INFER_WIDE_OPERATORS
    unsigned size = 0 ;
    // For multi-bit scalars :
    unsigned is_signed = 0 ;
    unsigned largest_unsigned = 0 ;
    unsigned user_encoded = 0 ;
    FOREACH_ARRAY_ITEM(input_values, i, item_val) {
        if (!item_val) continue ; // don't-care value
        if (item_val->IsBit()) {
            is_bit = 1 ;
            size = 1 ; // always
            break ; // all values will be bit (dictated by language)
        }
        if (item_val->IsComposite()) {
            is_composite = 1 ;
            size = item_val->NumElements() ; // All values will be composite, and must have same size (dictated by language).
#ifdef DB_INFER_WIDE_OPERATORS
            // VIPER #2171: Only check for bit-vector if it is composite!
            if (item_val->IsBitVector()) is_bitvector = 1 ;
#endif // DB_INFER_WIDE_OPERATORS
            break ;
        }
        // Otherwise, its a multi-bit scalar :
        is_multibit = 1 ;
        // Also determine maximum size and 'signed' property of these guys :
        j = item_val->NumBits() ;
        if (j>size) size = j ;
        if (item_val->IsSigned()) {
            is_signed = 1 ;
        } else {
            if (j>largest_unsigned) largest_unsigned = j ;
        }
        // Also, determine the 'user' encoding. Must be same for all values
        user_encoded = item_val->UserEncoded() ;
    }
    // CAUTION if a small value is signed, and the largest one is not
    // we need to make sure to extend the result one bit, to make sure the
    // unsigned stays unsigned
    // FIX ME : Should not do this if it does not fit in the constraint
    if (is_signed && largest_unsigned==size) size++ ;

#ifdef DB_INFER_WIDE_OPERATORS
    unsigned use_wide_operator = 1 ; // Viper #5201
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    use_wide_operator = VhdlVariableSlice::UseWideOperator() ;
#endif // VHDL_SUPPORT_VARIABLE_SLICE
#endif // DB_INFER_WIDE_OPERATORS

    // Now choose the appropriate action :
    if (is_composite) {
#ifdef DB_INFER_WIDE_OPERATORS
        // VIPER #2171: bit-vectors are always composite values, so check and create wide operator here.
        if (is_bitvector && use_wide_operator) {
            // VIPER Issue #2057 (VHDL side): Check whether it is partially assigned or not.
            // Check if for any particular bit of the vector all the assigned values from
            // 'input_values' have same net repeated. In such case we assume that partial
            // assignment has been done and don't create wide operators to protect us from
            // creating bad multiple driver case.
            unsigned part_assigned = 0 ;
            unsigned is_constant = 1 ; // Viper #4393
            FOREACH_ARRAY_ITEM(input_values, j, item_val) { // Viper #4393
                if (item_val && !item_val->IsConstant()) is_constant = 0 ;
                if (!is_constant) break ;
            }

            for (i=0; i<size; i++) {
                Net *matching_net = 0 ;
                Net *in_net = 0 ;
                unsigned bit_is_assigned = 0 ;
                FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                    if (!item_val) {
                        in_net = 0 ; // don't-care value... Assume assignment..
                    } else {
                        // It is a bit vector (composite value), get the net via ValueAt().
                        VhdlValue *elem_val = item_val->ValueAt(i) ;
                        in_net = (elem_val) ? (Net*)elem_val->GetBit() : 0 ;
                    }

                    if (!matching_net) {
                        // If there was a constant, this bit was certainly assigned.
                        if (ConstNet(in_net)) {
                            bit_is_assigned = 1 ;
                            break ;
                        }
                        matching_net = in_net ;
                    } else {
                        // If this net is different from the first one, this bit was certainly assigned.
                        if (in_net && matching_net != in_net) {
                            bit_is_assigned = 1 ;
                            break ;
                        }
                    }
                }
                // If this bit is not assigned, then we have a partial assignment :
                if (!bit_is_assigned) {
                    part_assigned = 1 ;
                    break ;
                }
            }

            // Create wide operator if it is not partially assigned, otherwise proceed to normal composite processing below.
            // Viper #4393 : Do NOT infer wide operator if input data is fully constant
            if (!part_assigned && !is_constant) {
                // Copy the condition nets array
                Array conditions(*condition->Nets()) ;
                // Prepare the input nets array
                Array in_nets(input_values->Size() * size) ;
                // VIPER #2237: Traverse the Array from beginning to end and accumulate
                // input data Nets. So, MSB will be at pos 0 for both bus and bit in a bus.
                // See also Operators.cpp where we fill-in the 'WideSelector' and 'WideNto1Mux'
                // operators using 'Selector' and 'Nto1Mux' operators respectively:
                FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                    if (!item_val) {
                        // If a value is not present insert 'X'
                        for (i=0; i<size; i++) in_nets.InsertLast(X()) ;
                    } else {
                        // Its is a composite value, so change it to non-const first
                        // and then append the Array of nets into the in_nets Array
                        VhdlNonconst *nc_item_val = item_val->Copy()->ToNonconst() ;
                        in_nets.Append(nc_item_val->Nets()) ; // nets_of_one_bus
                        delete nc_item_val ;
                    }
                }

                // Prepare the output array
                Array *result_nets = new Array(size) ;

                // Instantiate the wide Nto1Mux operator if size is not zero. 'size' can be zero in some case.
                if (size) {
                    // Calculate the number of data bus (size != 0, so no divide by zero exception here!)
                    unsigned number_of_bus = in_nets.Size()/size ;
                    (void) _nl->WideNto1Mux(&conditions, &in_nets, result_nets, number_of_bus, from->Linefile()) ;
                }

                VhdlValue *result = new VhdlNonconst(result_nets, is_signed, user_encoded) ;
                // Return composite value - it was a bit-vector (and thus composite value)
                return result->ToComposite(size) ;
            }
        }
#endif // DB_INFER_WIDE_OPERATORS

        // Normal composite processing : visit each element
        VhdlValue *elem_val ;
        Array *new_values = new Array(size) ;
        Array tmp_array(input_values->Size()) ;
        for (i=0;i<size;i++) {
            // Mux elements at position 'i' :
            tmp_array.Reset() ;
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                elem_val = (item_val) ? item_val->ValueAt(i) : 0 ;
                tmp_array.InsertLast(elem_val) ;
            }
            elem_val = Nto1Mux(condition, &tmp_array, from) ;
            new_values->InsertLast(elem_val) ;
        }
        return new VhdlCompositeValue(new_values) ;
    }

    Net *in_net = 0 ;
    Net *result_net = 0 ;

    if (is_bit) {
        // Single-bit-encoded elements.
        Array in_nets(input_values->Size()) ;

        // Copy the condition nets into 'conditions', since we will mess-up these nets
        Array conditions(condition->Size()) ;
        conditions.Append(condition->Nets()) ;
        // Now set-up the input nets. GetBit() should always work : it always gets a nonconstbit of single-bit constant
        FOREACH_ARRAY_ITEM(input_values, i, item_val) {
            // If there is no item_val, its a true don't-care. Insert 0 net.
            in_net = (item_val) ? (Net*)item_val->GetBit() : 0 ;
            in_nets.InsertLast(in_net) ;
        }
        result_net = BuildNto1Mux(conditions, in_nets, from) ;

        // Return the net as a bit value
        return new VhdlNonconstBit(result_net) ;
    }

    if (is_multibit) {
        // Multi-bit scalars.
        Array *result_nets = new Array(size) ;
#ifdef DB_INFER_WIDE_OPERATORS
        if (use_wide_operator) {
            // VIPER #2171: No need to check for partial assignment, multibits cannot be partially assigned!
            // Copy the condition nets array
            Array conditions(*condition->Nets()) ;
            // Prepare the input nets array
            Array in_nets(input_values->Size() * size) ;
            // VIPER #2237: Traverse the Array from beginning to end and accumulate
            // input data Nets. So, MSB will be at pos 0 for both bus and bit in a bus.
            // See also Operators.cpp where we fill-in the 'WideSelector' and 'WideNto1Mux'
            // operators using 'Selector' and 'Nto1Mux' operators respectively:
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                // Prepare the set of inputs for this value:
                for (i=0; i<size; i++) {
                    // For multi-bit value, directly get the net or if a value is not present insert 0:
                    in_net = (item_val) ? (Net*)item_val->GetBit((size-i)-1) : 0 ;
                    // Insert the input net into the array
                    in_nets.InsertLast(in_net) ;
                }
            }

            // Instantiate the wide Nto1Mux operator if size is not zero
            // 'size' can be zero in case such as this...
            // There is an enum type data element. Outside of any 'case' statement the first
            // enum literal is assigned to it. Inside a single 'case item' the same enum
            // lteral is assigned. The first literal has the value '0' - so it gets the size '0'.
            // In this case we don't instantiate the wide mux, the value '0' gets assigned to
            // the enum type data element.
            // VIPER #2865: If all the set of inputs to this mux is equal, then just use any one
            // of the set of inputs as the result, don't create an unnecessary wide mux here.
            // VIPER #3034 (Issue 2): Consider X nets as don't care while checking inputs for equality:
            // VIPER #3979: If all the set of inputs to this selector are equal, then just use any one
            // of the set of inputs as the result, don't create an unnecessary wide selector here.
            if (AreAllInputSetsEqual(&in_nets, size, result_nets)) {
                // It has alerady inserted one set of inputs into 'result_nets', nothing more to do here :-)
            } else if (size) {
                // Calculate the number of data bus (size != 0, so no divide by zero exception here!)
                unsigned number_of_bus = in_nets.Size()/size ;
                // Replace potential 0 nets in there by 'X':
                FOREACH_ARRAY_ITEM(&in_nets, j, in_net) {
                    if (!in_net) in_nets.Insert(j, X()) ;
                }
                (void) _nl->WideNto1Mux(&conditions, &in_nets, result_nets, number_of_bus, from->Linefile()) ;
            }
            // The resulting nets are now there in the 'result_nets' array. Values are created from
            // this net later and we return that non-constant value!
        } else {
#endif // DB_INFER_WIDE_OPERATORS

        Array in_nets(input_values->Size()) ;
        Array conditions(condition->Size()) ;
        for (i=0; i<size; i++) {
            // Clear arrays for the next iteration
            in_nets.Reset() ;
            conditions.Reset() ;
            // Copy conditions into 'conditions' array
            conditions.Append(condition->Nets()) ;

            // Get the i'th bit out of each input value
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                // In absence of a value, insert 0 (real don't-care)
                // Cuidado :
                // i is the index into a net array (ordered MSB->LSB).
                // So i==0 means MSB.
                // GetBit(pos) works the other way around : pos==0 is LSB.
                // So get opposite side of ordering here :
                in_net = (item_val) ? (Net*)item_val->GetBit((size - i) - 1) : 0 ; // '0' means don't-care.
                in_nets.InsertLast(in_net) ;
            }
            result_net = BuildNto1Mux(conditions, in_nets, from) ;

            result_nets->InsertLast(result_net) ;
        }
#ifdef DB_INFER_WIDE_OPERATORS
        }
#endif // DB_INFER_WIDE_OPERATORS

        // result is 'encoded' if the target constraint is encoded
        return new VhdlNonconst(result_nets, is_signed, user_encoded) ;
    }

    // Here, it is none of the above (not a composite, not a bit, not a multibit scalar.
    // Could legally happen if there are no values to select, but most likely an error.
    // 0 (don't-care, or error) seems an appropriate return value.
    return 0 ;
}

Net *
VhdlValue::BuildNto1Mux(Array &conditions, Array &in_nets, const VhdlTreeNode *from)
{
    if (!from) return 0 ;

    // Pick up run-time flag preserve_x :
    unsigned long preserve_x = RuntimeFlags::GetVar("vhdl_preserve_x") ;

    // Build a N-to-1 MUX function from conditions (control) and in_nets (data).

    // Prune down mux size by looking at all-equal Karnough map halfs.
    Net *c_net, *v_net, *test_net ;
    unsigned j, k ;
    FOREACH_ARRAY_ITEM_BACK(&conditions, j, c_net) {
        // Get index from lsb side of condition :
        unsigned from_lsb = (conditions.Size() - 1) - j ;
        // Calculate distance to other side of karnaugh map : modval = 2**from_lsb
        unsigned modval = 1 << from_lsb ;
        unsigned is_equal = 1 ;
        FOREACH_ARRAY_ITEM(&in_nets, k, v_net) {
            // Bits in the upper karnough map are already compared to their equivalent bit in other half of karnaugh map
            if (modval && (k % (2 * modval)) >= modval) continue ; // JJ 130322 check modval != 0
            if (!v_net) continue ; // don't-care optimization : 0 compares to anything
            if (!preserve_x && v_net==X()) continue ; // 'X' optimization : 'X' compares to anything

            // Compare bit k with bit k+modval
            // If there is no such in_net, take 0 (real don't-care)
            test_net = (in_nets.Size()>(k+modval)) ? (Net*)in_nets.At(k+modval) : 0 ;
            if (test_net == v_net) continue ;
            if (!test_net) continue ; // don't-care optimization : 0 compares to anything
            if (!preserve_x && test_net==X()) continue ; // 'X' optimization : 'X' compares to anything

            is_equal=0 ;
            break ;
        }
        if (!is_equal) continue ;

        // Here, this control bit (c_net) is redundant
        // Since we run lsb->msb, we can safely remove it
        conditions.Remove(j) ;
        // Also remove the value bits..
        // Run 'BACK' through the array, so we can delete items without desturbing the ordering
        // Remove the lower-karnaugh map bits. Leave the upper ones in there
        FOREACH_ARRAY_ITEM_BACK(&in_nets, k, v_net) {
            if (k%(2*modval) >= modval) {
                // This is a 'upper' net, to remain.
                // If this upper net is 0, its a real don't-care (Eg from non-existing state)
                // As for 'X' optimization (below), select the other (lower-karnaugh map) net to survive.
                if (!v_net) {
                    test_net = (Net*)in_nets.At(k-modval) ;
                    in_nets.Insert(k,test_net) ;
                }
                // If it is 'X', take the lower net for it, so that
                // net N muxed with 'X' results in N and not in 'X'.
                // Only need to do this test if we don't preserve X.
                if (!preserve_x && v_net==X()) {
                    test_net = (Net*)in_nets.At(k-modval) ;
                    in_nets.Insert(k,test_net) ;
                }
            } else {
                in_nets.Remove(k) ;
            }
        }
        // 'in_nets' and 'conditions' will be adjusted now,
        // so next iteration 'from_lsb' will be the same as
        // this iteration, but represent the next bit in control.
    }

    // FIX ME : prune down mux size, based on constant bits in 'condition'
    // CHECK ME : If all inputs are constant, this looks like a ROM.....

    Net *result_net = 0 ;
    if (in_nets.Size()==1) {
        // Well, Let's just take the input net then, shall we ?
        result_net = (Net*)in_nets.At(0) ;
    } else if (in_nets.Size()==2) {
        // Lets create a nice, small 2->1 MUX primitive
        // Note that first bit is selected at '1', second selected at '0' (since it's ordered MSB->LSB
        result_net = from->Mux((Net*)in_nets.At(1), (Net*)in_nets.At(0), (Net*)conditions.At(0)) ;
    } else {
    }
    if (!result_net) {
        // could happen if all inputs are don't care.
        // Don't return 0. That is for failures. Return 'X' instead.
        result_net = X() ;
    }
    return result_net ;
}

VhdlValue *
VhdlValue::Selector(Array *one_hot_nets, const Array *input_values, const VhdlTreeNode *from)
{
    if (!from || !one_hot_nets || !input_values) return 0 ;

    // If there is only one value, take it : condition MUST be 0 bits for this to be valid. Check this ??
    VhdlValue *item_val ;
    if (input_values->Size()==1) {
        item_val = (VhdlValue*)input_values->GetFirst() ;
        // 'item_val' can be 0, if value is don't care.
        return (item_val) ? item_val->Copy() : 0 ;
    }
    if (input_values->Size()==2) {
        // Create a 2->1 MUX (issue 2045 triggered this)
        VhdlValue *val1 = (VhdlValue*)input_values->GetFirst() ;
        VhdlValue *val2 = (VhdlValue*)input_values->GetLast() ;
        Net *c = (Net*)one_hot_nets->GetFirst() ; // Will select first value
        // Now call a selector on this :
        if (val1 && val2) {
            val1 = val1->Copy() ;
            val2 = val2->Copy() ;
            VhdlNonconstBit cval(c) ;
            item_val = val2->SelectOn1(&cval, val1, 0/*don't know initial value*/, from) ;
            return item_val ;
        }
        // if one side is 0 (don't-care), pass through normal processing.
    }

#ifdef DB_INFER_WIDE_OPERATORS
    unsigned use_wide_operator = 1 ; // Viper #5201
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    use_wide_operator = VhdlVariableSlice::UseWideOperator() ;
#endif // VHDL_SUPPORT_VARIABLE_SLICE
#endif // DB_INFER_WIDE_OPERATORS

    // (just as with Nto1 MUX) :
    // Determine if we are dealing with 'composite', 'bit' or a 'multi-bit' values.
    unsigned i, j ;
    unsigned is_bit = 0 ;
    unsigned is_multibit = 0 ;
    unsigned is_composite = 0 ;
#ifdef DB_INFER_WIDE_OPERATORS
    unsigned is_bitvector = 0 ;
#endif // DB_INFER_WIDE_OPERATORS
    unsigned size = 0 ;
    // For multi-bit scalars :
    unsigned is_signed = 0 ;
    unsigned largest_unsigned = 0 ;
    unsigned user_encoded = 0 ;
    FOREACH_ARRAY_ITEM(input_values, i, item_val) {
        if (!item_val) continue ; // don't-care value
        if (item_val->IsBit()) {
            is_bit = 1 ;
            size = 1 ; // always
            break ; // all values will be bit (dictated by language)
        }
        if (item_val->IsComposite()) {
            is_composite = 1 ;
            size = item_val->NumElements() ;
#ifdef DB_INFER_WIDE_OPERATORS
            // VIPER #2171: Only check for bit-vector if it is composite!
            if (item_val->IsBitVector()) is_bitvector = 1 ;
#endif // DB_INFER_WIDE_OPERATORS
            break ; // All values will be composite, and must have same size (dictated by language).
        }
        // Otherwise, its a multi-bit scalar :
        is_multibit = 1 ;
        // Also determine maximum size and 'signed' property of these guys :
        j = item_val->NumBits() ;
        if (j>size) size = j ;
        if (item_val->IsSigned()) {
            is_signed = 1 ;
        } else {
            if (j>largest_unsigned) largest_unsigned = j ;
        }
        // Also, determine the 'user' encoding. Must be same for all values
        user_encoded = item_val->UserEncoded() ;
    }

    // (for multi-bit scalars:) If a small value is signed, and the largest one is not
    // we need to make sure to extend the result one bit, to make sure the
    // unsigned stays unsigned
    // FIX ME : Should not do this if it does not fit in the constraint
    if (is_signed && largest_unsigned==size) size++ ;

    // Now choose the appropriate action :
    if (is_composite) {
#ifdef DB_INFER_WIDE_OPERATORS
        // VIPER #2171: bit-vectors are always composite values, so check and create wide operator here.
        if (is_bitvector && use_wide_operator) {
            // VIPER Issue #2057 (VHDL side): Check whether it is partially assigned or not.
            // Check if for any particular bit of the vector all the assigned values from
            // 'input_values' have same net repeated. In such case we assume that partial
            // assignment has been done and don't create wide operators to protect us from
            // creating bad multiple driver case.
            unsigned part_assigned = 0 ;
            for (i=0; i<size; i++) {
                Net *matching_net = 0 ;
                Net *in_net = 0 ;
                unsigned bit_is_assigned = 0 ;
                FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                    if (!item_val) {
                        in_net = 0 ; // don't-care value... Assume assignment..
                    } else {
                        // It is a bit vector (composite value), get the net via ValueAt().
                        VhdlValue *elem_val =  item_val->ValueAt(i) ;
                        in_net = (elem_val) ? (Net*)elem_val->GetBit() : 0 ;
                    }
                    if (!matching_net) {
                        // If there was a constant, this bit was certainly assigned.
                        if (ConstNet(in_net)) {
                            bit_is_assigned = 1 ;
                            break ;
                        }
                        matching_net = in_net ;
                    } else {
                        // If this net is different from the first one, this bit was certainly assigned.
                        if (matching_net != in_net) {
                            bit_is_assigned = 1 ;
                            break ;
                        }
                    }
                }
                if (!bit_is_assigned) {
                    part_assigned = 1 ;
                    break ;
                }
            }

            // Create wide operator if it is not partially assigned, otherwise proceed to normal composite processing below.
            if (!part_assigned) {
                // Copy the condition nets array
                Array conditions(*one_hot_nets) ;
                // Prepare the input nets array
                Array in_nets(input_values->Size() * size) ;
                // VIPER #2237: Traverse the Array from beginning to end and accumulate
                // input data Nets. So, MSB will be at pos 0 for both bus and bit in a bus.
                // See also Operators.cpp where we fill-in the 'WideSelector' and 'WideNto1Mux'
                // operators using 'Selector' and 'Nto1Mux' operators respectively:
                FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                    if (!item_val) {
                        // If a value is not present insert 'X'
                        for (i=0; i<size; i++) in_nets.InsertLast(X()) ;
                    } else {
                        // Its is a composite value, so change it to non-const first
                        // and then append the Array of nets into the in_nets Array
                        VhdlNonconst *nc_item_val = item_val->Copy()->ToNonconst() ;
                        in_nets.Append(nc_item_val->Nets()) ;
                        delete nc_item_val ;
                    }
                }

                // Prepare the output array
                Array *result_nets = new Array(size) ;

                // Instantiate the wide Selector operator if size is not zero. 'size' can be zero in some case.
                // VIPER #3979: If all the set of inputs to this selector are equal, then just use any one
                // of the set of inputs as the result, don't create an unnecessary wide selector here.
                if (AreAllInputSetsEqual(&in_nets, size, result_nets)) {
                    // It has alerady inserted one set of inputs into 'result_nets', nothing more to do here :-)
                } else if (size) {
                    // Calculate the number of data bus (size != 0, so no divide by zero exception here!)
                    unsigned number_of_bus = in_nets.Size() / size ;
                    (void) _nl->WideSelector(&conditions, &in_nets, result_nets, number_of_bus, from->Linefile()) ;
                }

                VhdlValue *result = new VhdlNonconst(result_nets, is_signed, user_encoded) ;
                // Return composite value - it was a bit-vector (and thus composite value)
                return result->ToComposite(size) ;
            }
        }
#endif // DB_INFER_WIDE_OPERATORS

        // Normal composite processing : visit each element
        VhdlValue *elem_val ;
        Array *new_values = new Array(size) ;
        Array tmp_array(input_values->Size()) ;
        for (i=0;i<size;i++) {
            // Select elements at position 'i' :
            tmp_array.Reset() ;
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                elem_val = (item_val) ? item_val->ValueAt(i) : 0 ;
                tmp_array.InsertLast(elem_val) ;
            }
            elem_val = Selector(one_hot_nets, &tmp_array, from) ;
            new_values->InsertLast(elem_val) ;
        }
        return new VhdlCompositeValue(new_values) ;
    }

    Map val_to_conds(POINTER_HASH,input_values->Size()) ;
    Net *in_net = 0, *c_net ;
    Net *result_net = 0 ;
    MapIter mi ;

    if (is_bit) {
        // Single-bit values
        // Set-up value->conditions array
        unsigned all_nets_identical = 1 ; // Viper #3979
        Net *prev_net = 0 ;
        FOREACH_ARRAY_ITEM(input_values, i, item_val) {
            c_net = (Net*)one_hot_nets->At(i) ;
            in_net = (item_val) ? (Net*)item_val->GetBit() : 0 ; // if item_val was 0, it means real don't-care
            if (!prev_net) prev_net = in_net ;
            if (in_net != prev_net) all_nets_identical = 0 ;
            InsertValToConds(&val_to_conds, c_net, in_net) ;
        }
        // Now create a selector with that
        // Viper #3979 : If all selector inputs are identical, we do not need selector
        result_net = (all_nets_identical && prev_net) ? prev_net : BuildSelector(val_to_conds, from) ;

        // Crean up 'val_to_conds'
        Array *this_c_nets ;
        FOREACH_MAP_ITEM(&val_to_conds, mi, 0, &this_c_nets) delete this_c_nets ;

        // Return a single bit value
        return new VhdlNonconstBit(result_net) ;
    }

    if (is_multibit) {
        // Multi-bit scalars.
        Array *result_nets = new Array(size) ;
#ifdef DB_INFER_WIDE_OPERATORS
        if (use_wide_operator) {
            // VIPER #2171: No need to check for partial assignment, multibits cannot be partially assigned!
            // Copy the condition nets array
            Array conditions(*one_hot_nets) ;
            // Prepare the input nets array
            Array in_nets(input_values->Size() * size) ;
            // VIPER #2237: Traverse the Array from beginning to end and accumulate
            // input data Nets. So, MSB will be at pos 0 for both bus and bit in a bus.
            // See also Operators.cpp where we fill-in the 'WideSelector' and 'WideNto1Mux'
            // operators using 'Selector' and 'Nto1Mux' operators respectively:
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                if (!item_val) {
                    // If a value is not present insert 'X'
                    for (i=0; i<size; i++) in_nets.InsertLast(X()) ;
                } else {
                    for (i=0; i<size; i++) {
                        // for multi-bit value, directly get the net
                        in_net = (Net*)item_val->GetBit((size-i)-1) ;
                        // Insert the input net into the array
                        in_nets.InsertLast(in_net) ;
                    }
                }
            }

            // Instantiate the wide Selector operator if size is not zero
            // 'size' can be zero in case such as this...
            // There is an enum type data element. Outside of any 'case' statement the first
            // enum literal is assigned to it. Inside a single 'case item' the same enum
            // lteral is assigned. The first literal has the value '0' - so it gets the size '0'.
            // In this case we don't instantiate the wide mux, the value '0' gets assigned to
            // the enum type data element.
            // VIPER #3979: If all the set of inputs to this selector are equal, then just use any one
            // of the set of inputs as the result, don't create an unnecessary wide selector here.
            if (AreAllInputSetsEqual(&in_nets, size, result_nets)) {
                // It has alerady inserted one set of inputs into 'result_nets', nothing more to do here :-)
            } else if (size) {
                // Calculate the number of data bus (size != 0, so no divide by zero exception here!)
                unsigned number_of_bus = in_nets.Size() / size ;
                (void) _nl->WideSelector(&conditions, &in_nets, result_nets, number_of_bus, from->Linefile()) ;
            }
            // The resulting nets are now there in the 'result_nets' array. Values are created from
        } else {
#endif // DB_INFER_WIDE_OPERATORS

        // this net later and we return that non-constant value!
        for (i=0; i<size; i++) {
            // Create a (Map) relation of (unique) value _nets->control _nets that enable it
            val_to_conds.Reset() ;
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                c_net = (Net*)one_hot_nets->At(j) ; // j'th value is j'th c_net, regardless of the bits involved
                // Cuidado :
                // i is the index into a net array (ordered MSB->LSB).
                // So i==0 means MSB.
                // GetBit(pos) works the other way around : pos==0 is LSB.
                // So get opposite side of ordering here :
                in_net = (item_val) ? (Net*)item_val->GetBit((size - i) - 1) : 0 ; // j'th value, take i'th bit
                InsertValToConds(&val_to_conds, c_net, in_net) ;
            }
            // Now create a selector with that
            result_net = BuildSelector(val_to_conds, from) ;

            result_nets->InsertLast(result_net) ;
            // Clean up 'val_to_conds'
            Array *this_c_nets ;
            FOREACH_MAP_ITEM(&val_to_conds, mi, 0, &this_c_nets) delete this_c_nets ;
        }
#ifdef DB_INFER_WIDE_OPERATORS
        }
#endif // DB_INFER_WIDE_OPERATORS

        // result is 'encoded' if the target constraint is encoded
        return new VhdlNonconst(result_nets, is_signed, user_encoded) ;
    }

    // Here, it is none of the above (not a composite, not a bit, not a multibit scalar.
    // Could legally happen if there are no values to select, but most likely an error.
    // 0 (don't-care, or error) seems an appropriate return value.
    return 0 ;
}

VhdlValue *
VhdlValue::PrioSelector(const VhdlValue *else_value, Array *selector_nets, const Array *input_values, const VhdlTreeNode *from)
{
    if (!from) return 0 ;

    // 'else' value can be 0 (to indicate 'don't-care' value)
    if (!input_values || input_values->Size()==0) {
        return (else_value) ? else_value->Copy() : 0 ;
    }

    if (!selector_nets) return 0 ;

    // If there is only one value, Create a 2-1 selector :
    if (else_value && input_values->Size()==1) {
        VhdlValue *val0 = else_value->Copy() ;
        VhdlValue *val1 = ((VhdlValue*)input_values->GetFirst()) ;
        if (val1) {
            val1 = val1->Copy() ;
            Net *sel_net = (Net*)selector_nets->At(0) ;
            VhdlNonconstBit s(sel_net) ;
            return val0->SelectOn1(&s, val1, 0/*no hold value*/, from) ;
        } else {
            // no value in normal flow means 'don't-care'.
            // Return the else value.
            return val0 ;
        }
    }
    VhdlValue *item_val, *elem_val, *elem_else_val ;

    // Determine if we are dealing with 'composite', 'bit' or a 'multi-bit' values.
    unsigned i, j ;
    unsigned is_bit = 0 ;
    unsigned is_multibit = 0 ;
    unsigned is_composite = 0 ;
    unsigned size = 0 ;
    // For multi-bit scalars :
    unsigned is_signed = 0 ;
    unsigned largest_unsigned = 0 ;
    unsigned user_encoded = 0 ;
    FOREACH_ARRAY_ITEM(input_values, i, item_val) {
        if (!item_val) continue ; // don't-care value
        if (item_val->IsBit()) {
            is_bit = 1 ;
            size = 1 ; // always
            break ; // all values will be bit (dictated by language)
        }
        if (item_val->IsComposite()) {
            is_composite = 1 ;
            size = item_val->NumElements() ;
            break ; // All values will be composite, and must have same size (dictated by language).
        }
        // Otherwise, its a multi-bit scalar :
        is_multibit = 1 ;
        // Also determine maximum size and 'signed' property of these guys :
        j = item_val->NumBits() ;
        if (j>size) size = j ;
        if (item_val->IsSigned()) {
            is_signed = 1 ;
        } else {
            if (j>largest_unsigned) largest_unsigned = j ;
        }
        // Also, determine the 'user' encoding. Must be same for all values
        user_encoded = item_val->UserEncoded() ;
    }

    // Use else-value also to determine type, size and sign info
    if (else_value) {
        if (else_value->IsBit()) {
            is_bit = 1 ;
            size = 1 ;
        } else if (else_value->IsComposite()) {
            is_composite = 1 ;
            size = else_value->NumElements() ;
        } else if (else_value->IsBit()) {
            is_bit = 1 ;
        } else { // multi-bit scalar
            is_multibit = 1 ;
            // Also determine maximum size and 'signed' property of these guys :
            j = else_value->NumBits() ;
            if (j>size) size = j ;
            if (else_value->IsSigned()) {
                is_signed = 1 ;
            } else {
                if (j>largest_unsigned) largest_unsigned = j ;
            }
            // Also, determine the 'user' encoding. Must be same for all values
            user_encoded = else_value->UserEncoded() ;
        }
    }

    // (for multi-bit scalars:) If a small value is signed, and the largest one is not
    // we need to make sure to extend the result one bit, to make sure the
    // unsigned stays unsigned
    // FIX ME : Should not do this if it does not fit in the constraint
    if (is_signed && largest_unsigned==size) size++ ;

    // Now choose the appropriate action :
    if (is_composite) {
        Array *new_values = new Array(size) ;
        Array tmp_array(input_values->Size()) ;
        for (i=0; i<size; i++) {
            tmp_array.Reset() ;
            elem_else_val = (else_value) ? else_value->ValueAt(i) : 0 ;
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                elem_val = (item_val) ? item_val->ValueAt(i) : 0 ;
                tmp_array.InsertLast(elem_val) ;
            }
            elem_val = PrioSelector(elem_else_val, selector_nets, &tmp_array, from) ;
            new_values->InsertLast(elem_val) ;
        }
        return new VhdlCompositeValue(new_values) ;
    }

    Net *result_net = 0 ;
    Array data_nets(input_values->Size()) ;
    Net *else_net, *in_net ;
    unsigned data_is_same ;

    if (is_bit) {
        data_is_same = 1 ;
        else_net = (else_value) ? else_value->GetBit() : X() ;
        FOREACH_ARRAY_ITEM(input_values, i, item_val) {
            in_net = (item_val) ? (Net*)item_val->GetBit() : X() ;
            if (in_net != else_net) data_is_same = 0 ;
            data_nets.InsertLast(in_net) ;
        }

        // Now create a priority-selector into 'result_net'
        result_net = 0 ; // Create a new result net with the selector
        if (data_is_same) {
            result_net = else_net ;
        } else {
        }

        // Return a single bit value
        return new VhdlNonconstBit(result_net) ;
    }

    if (is_multibit) {
        Array *result_nets = new Array(size) ;
        for (i=0; i<size; i++) {
            data_nets.Reset() ;
            data_is_same = 1 ;

            // The only optimization we do is that if ALL bits are the
            // same, we return the result without a prio selector.
            // That is required, to avoid that bogus logic gets created for unassigned bits in
            // an overall id value.

            // Get 'else' net at bit i (count back from MSB)
            else_net = (else_value) ? else_value->GetBit((size - i) - 1) : X() ; // no 'else' value means 'don't-care'

            // Get the data nets at bit i
            FOREACH_ARRAY_ITEM(input_values, j, item_val) {
                // Cuidado :
                // i is the index into a net array (ordered MSB->LSB).
                // So i==0 means MSB.
                // GetBit(pos) works the other way around : pos==0 is LSB.
                // So get opposite side of ordering here :
                in_net = (item_val) ? (Net*)item_val->GetBit((size - i) - 1) : X() ; // j'th value, take i'th bit
                VERIFIC_ASSERT(in_net) ;
                if (in_net != else_net) data_is_same = 0 ;
                data_nets.InsertLast(in_net) ;
            }

            // Now create a priority-selector into 'result_net'
            result_net = 0 ; // Create a new result net with the selector
            if (data_is_same) {
                result_net = else_net ;
            } else {
            }
            result_nets->InsertLast(result_net) ;
        }

        return new VhdlNonconst(result_nets, is_signed, user_encoded) ;
    }

    // Can determine type of return value.
    return 0 ;
}

void
VhdlValue::InsertValToConds(Map *val_to_conds, const Net *c_net, const Net *val_net)
{
    // Utility for selector generation code
    if (!val_net || !c_net) return ; // real don't-care input nets get ignored

    // x optimization : ignore x selections (if we do not preserve x)
    if ((val_net==X() || c_net==X()) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return ;

    // Insert valnet->cond_nets uniquely into an association
    Array *this_c_nets = (Array*)val_to_conds->GetValue(val_net) ;
    if (!this_c_nets) {
        this_c_nets = new Array() ;
        (void) val_to_conds->Insert(val_net, this_c_nets) ;
    }
    this_c_nets->InsertLast(c_net) ;
}

Net *
VhdlValue::BuildSelector(Map &val_to_conds, const VhdlTreeNode *from)
{
    if (!from) return 0 ;

    Net *result_net = 0 ;
    if (val_to_conds.Size()==0) {
        // no data : return x ?
        return X() ;
    }
    if (val_to_conds.Size()==1) {
        // All data nets are the same. Take the data net.
        MapItem *tmp_mi = val_to_conds.GetItemAt(0) ;
        result_net = (tmp_mi) ? (Net*) tmp_mi->Key() : 0 ;
        return result_net ;
    }

    // Set-up new 'in_nets' and 'c_nets' arrays
    Array in_nets(val_to_conds.Size()) ;
    Array c_nets(val_to_conds.Size()) ;
    MapIter mi ;
    Net *best_control_net = 0 ;
    Net *in_net, *c_net ;
    Array *this_c_nets ;
    unsigned represent_control_lines = 0 ;
    FOREACH_MAP_ITEM(&val_to_conds, mi, &in_net, &this_c_nets) {
        // OR this_c_nets
        c_net = 0 ;
        if (this_c_nets->Size()==1) {
            c_net = (Net*)this_c_nets->At(0) ;
        } else if (this_c_nets->Size()==2) {
            // Create a simple 2-input OR gate
            c_net = from->Or((Net*)this_c_nets->At(0), (Net*)this_c_nets->At(1)) ;
        } else {
            // If all c_nets are constant, there is a max of 2 c_nets ('0' and '1').
        }
        in_nets.InsertLast(in_net) ;
        c_nets.InsertLast(c_net) ;

        // Find the best control net in case we need it
        if (!best_control_net || (represent_control_lines > this_c_nets->Size())) {
            represent_control_lines = this_c_nets->Size() ;
            best_control_net = c_net ;
        }
    }
    // Create result_net
    result_net = 0 ;
    if (in_nets.Size()==1) {
        // Create one AND gate
        result_net = from->And((Net*)in_nets.At(0), (Net*)c_nets.At(0)) ;
    } else if (in_nets.Size()==2) {
        // This is really a 2->1 MUX
        // Use 'best_control_net' as the selector
        c_net = best_control_net ;
        if (c_nets.At(0) == c_net) {
            // It's the one at index 0 (selects net (at index) 0 if it is '1'
            result_net = from->Mux((Net*)in_nets.At(1), (Net*)in_nets.At(0), c_net) ;
        } else {
            // It's the one at index 1
            result_net = from->Mux((Net*)in_nets.At(0), (Net*)in_nets.At(1), c_net) ;
        }
    } else {
        // If all in_nets are constant, there is a max of 2 in_nets ('0' and '1').
    }
    return result_net ;
}

VhdlNonconst *
VhdlNonconst::Plus(VhdlNonconst *r, Net *carry, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    // We can handle only both signed,
    // or both unsigned numbers.
    // If one side is unsigned, need to
    // adjust it to signed, extending one bit (0) first.
    if (IsSigned() && !r->IsSigned()) {
        r->Adjust(r->Size()+1) ;
        r->SetSigned() ;
    }
    if (r->IsSigned() && !IsSigned()) {
        Adjust(Size()+1) ;
        SetSigned() ;
    }

    // Note that if one side is signed, the result is signed.
    // Since we now set the _signed flag on both values,
    // _signed in 'this' (or 'r' for that matter) will now
    // represent the correct return sign value.

    // We can handle only equal-sized values. Extend the smallest.
    if (Size() != r->Size()) {
        unsigned size = MAX(Size(), r->Size()) ;
        Adjust(size) ;
        r->Adjust(size) ;
    }

    // Skip out if there are 0 bits (null range) :
    if (!_nets || _nets->Size()==0) {
        delete r ;
        return this ;
    }

    // Get the sign bits before we change _nets to contain the result :
    Net *sign_a = (_is_signed) ? (Net*)_nets->At(0) : Gnd() ;
    Net *sign_b = (_is_signed) ? (Net*)r->_nets->At(0) : Gnd() ;

    // Here, bit-level unsigned addition
    // Add two VhdlNonconst's. Put result in 'this'
    // Since inputs and outputs have same size, there is
    // no difference between signed and unsigned

    // Start at lsb to optimize constant bits
    Net *lnet, *rnet, *sum ;
    unsigned i ;

    FOREACH_ARRAY_ITEM_BACK(_nets, i, lnet) {
        rnet = (Net*)r->_nets->At(i) ;
        sum = 0 ;
        // Powerfull optimization if two out of three _nets are the same
        if (lnet==rnet) {
            sum = carry ; carry = lnet ;
        } else if (carry==lnet) {
            sum = rnet ; // carry unchanged
        } else if (carry==rnet) {
            sum = lnet ; // carry unchanged
        } else if ((lnet==Gnd() && rnet==Pwr()) || (lnet==Pwr() && rnet==Gnd())) {
            // Happens for constant additions
            sum = from->Inv(carry) ; // carry unchanged
#ifdef VHDL_MINIMIZE_INCREMENTOR
        // Don't do the next ones if it turns out to be overkill.
        // It always creates inverters at inc/dec LSB's, reducing one bit out of the adder.
        // replacing it with a simple inverter, and using the carry-in.
        } else if ((carry==Gnd() && rnet==Pwr()) || (carry==Pwr() && rnet==Gnd())) {
            // Happens often for first bit in incrementor or decrementor
            sum = from->Inv(lnet) ;
            carry = lnet ;
        } else if ((carry==Gnd() && lnet==Pwr()) || (carry==Pwr() && lnet==Gnd())) {
            // Happens often for first bit in incrementor or decrementor
            sum = from->Inv(rnet) ;
            carry = rnet ;
#endif // #ifdef VHDL_MINIMIZE_INCREMENTOR
        }

        if (sum) {
            // VIPER #4240 : Modify this value when we have sum
            // Insert the (constant) bit
            _nets->Insert(i,sum) ;
        }
    }

    // Create the N+1'th bit from the adder
    // For unsigned, we can always use the carry-out.
    // For signed, we could extend the adder one bit, and take the MSB 'sum' bit.
    // But this will extend the adder in all cases, while most
    // of the time the N+1'th bit is not used.
    // Instead, create some boolean logic (2 XORs) for the last sign bit.
    // If not used, the logic will be blown away.
    if (_is_signed) {
        // Create a signed last bit (sign bit).
        // It's    A XOR B XOR C   where A and B are the Nth input bit,
        // and C is the unsigned regular carry out.
        carry = from->Xor(carry, from->Xor(sign_a,sign_b)) ;
    }

    // Insert as N+1th bit (at MSB) in the result.
    _nets->InsertFirst(carry) ;

    delete r ;
    return this ;
}

VhdlNonconst *
VhdlNonconst::Minus(VhdlNonconst *r, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    // result = left - right
    // Use this formula :
    //    -a == (-a-1) + 1 == NOT(a) + 1
    // so
    // result = left + NOT(right) + 1
    //

    // We always do signed addition (result is signed).
    unsigned size_extended = 0 ; // Needed for Viper #5183
    if (!r->IsSigned()) {
        // adjust one bit up
        r->Adjust(r->Size()+1) ;
        r->SetSigned() ;
        if (r->Size() > Size()) size_extended = 1 ; // Viper #5183
    }
    // And the other way around too.
    if (!IsSigned()) {
        Adjust(Size()+1) ;
        SetSigned() ;
        if (Size() > r->Size()) size_extended = 1 ; // Viper #5183
    }

    // Skip out if there are 0 bits (null range) :
    if (_nets->Size()==0) {
        delete r ;
        return this ;
    }

#ifdef DB_INFER_WIDE_OPERATORS
    // Create a wide minus operator only if both sizes are non-constant.
    if (!IsConstant() && !r->IsConstant() && _nl) {
        // The (wide) minus operator can handle only equal-sized values.
        // Get the maximum size of the arguments :
        unsigned size = MAX(Size(),r->Size()) ;

        // VIPER 4295 : N bit subtraction results in N+1 bit result.
        if (!size_extended) size = size+1 ;

        // Now create equal sized arguments :
        Adjust(size) ;
        r->Adjust(size) ;

        // Create a 'minus' operator directly. Don't go to plus.
        Array *outputs = new Array(size) ;
        (void) _nl->Minus(_nets, r->_nets, outputs, from->Linefile()) ;

        // Push output nets into result :
        delete _nets ;
        _nets = outputs ;
        // Delete r, return this.
        delete r ;
        return this ;
    }
    // Otherwise, drop to creation of a normal adder/invertor combination.
#else
    (void) size_extended ;
#endif // DB_INFER_WIDE_OPERATORS

    // Don't need to adjust the sizes ; Sign bit can
    // extended after inversion or before. Value is signed,
    // so extension always happens correctly (in ::Plus()).

    // Now invert, and add (with increment)
    return Plus(r->Invert(from), Pwr(), from) ;
}

VhdlNonconst *
VhdlNonconst::Multiply(VhdlNonconst *r, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    unsigned l_neg = (IsSigned() && _nets->Size() && ((Net*)_nets->GetFirst() == _pwr)) ? 1: 0 ;  // Storing the negative-ness of left-side operand, added Size check for Viper #4846
    unsigned r_neg = (r->IsSigned() && r->_nets && r->_nets->Size() && ((Net*)r->_nets->GetFirst() == _pwr)) ? 1: 0 ; // Storing the negative-ness of right-side operand, added Size check for Viper #4846
    // First handle multiply by power of 2

    // Actually, shifting works for constant power-of-two multiply,
    // even if the other side is 'signed'.
    Net *lnet ;
    // Viper 6208: Only perform integer arithmatic if Active Size is less than integer size
    if (r->IsConstant() && r->ActiveSize()<=sizeof(int)*8 && !r->HasX()) { // VIPER issue 2272 : preserve X info (do not interpret as don't-care)
        // Multiply an unsigned by a constant amount
        verific_int64 amount = r->Integer() ;
        if (amount==0) {
            // Multiply by 0
            unsigned i ;
            FOREACH_ARRAY_ITEM(_nets,i,lnet) _nets->Insert(i,Gnd()) ;
            delete r ;
            return this ;
        }
        unsigned swap_it = 0 ;
        if (amount<0) {
            swap_it = 1 ;
            amount = -amount ;
        }
        // Calculate power of 2 thats at least as large as our number. Block integer overflow
        // Viper #4151: Changed to verific_int64 and "unsigned long long" for size check
        verific_int64 cnt=1; unsigned bits=0 ;
        while (cnt<amount && bits<=sizeof(verific_int64)*8) { bits++; cnt*=2; }
        // Now see if it is exactly power-of-2
        if (cnt==amount) {
            if (swap_it) (void) UnaryMin(from, VhdlNode::Pwr()) ; // swap sign of left side
            // First make sure the result can be handled in the return value
            Adjust(Size()+bits) ;
            // Now left-shift (LSB->MSB) of 'bits'
            // Shift((new VhdlInt(bits))->ToNonconst(),0,0,0,from) ;
            // Do shift directly

            // Start on MSB size, so as not to overwrite bits
            for (unsigned i=0; i<Size(); i++) {
                // Get bit from the right and stick in here
                _nets->Insert(i, (i>=Size()-bits) ? Gnd() : _nets->At(i+bits)) ;
            }

            delete r ;
            return this ;
        } // else : not power of 2 ; drop through to regular multiplication
    }
    // Viper 6208: Only perform integer arithmatic if Active Size is less than integer size
    if (IsConstant() && ActiveSize()<=sizeof(int)*8 && !HasX()) { // VIPER issue 2272 : preserve X info (do not interpret as don't-care)
        // Multiply an unsigned by a constant amount
        verific_int64 amount = Integer() ;
        if (amount==0) {
            // Multiply by 0
            unsigned i ;
            FOREACH_ARRAY_ITEM(r->_nets,i,lnet) r->_nets->Insert(i,Gnd()) ;
            delete this ;
            return r ;
        }
        unsigned swap_it = 0 ;
        if (amount<0) {
            swap_it = 1 ;
            amount = -amount ;
        }
        // Calculate power of 2 thats at least as large as our number. Block integer overflow
        // Viper #4151: Changed to verific_int64 and "unsigned long long" for size check
        verific_int64 cnt=1; unsigned bits=0 ;
        while (cnt<amount && bits<=sizeof(verific_int64)*8) { bits++; cnt*=2 ; }
        // Now see if it is exactly power-of-2
        if (cnt==amount) {
            if (swap_it) (void) r->UnaryMin(from, VhdlNode::Pwr()) ; // Swap right side
            // First make sure the result can be handled in the return value
            r->Adjust(r->Size()+bits) ;
            // Now left-shift (LSB->MSB) of 'bits'
            // r->Shift((new VhdlInt(bits))->ToNonconst(),0,0,0,from) ;
            // Do shift directly
            // Start on MSB size, so as not to overwrite bits
            for (unsigned i=0; i<r->Size(); i++) {
                // Get bit from the right and stick in here
                r->_nets->Insert(i, (i>=r->Size()-bits) ? Gnd() : r->_nets->At(i+bits)) ;
            }

            delete this ;
            return r ;
        } // else : not power of 2 ; drop through to regular multiplication
    }

    // If one argument is signed, both arguments should become signed,
    // and result becomes 'signed'
    if (IsSigned() || r->IsSigned()) {
        if (!IsSigned()) { Adjust(Size()+1) ; SetSigned() ; } ; // add one (zero) bit, and make signed
        if (!r->IsSigned()) { r->Adjust(r->Size()+1) ; r->SetSigned() ; } ; // add one (zero) bit, and make signed
    }

    // VIPER 2503 : If both sides are constant and they fit in an integer.
    // Go to integer and calculate left * right.
    // Viper #4151: Changed to "long long" for size check
    // Viper #4317 : Following part is commented out because it will not work
    // if the result of the multiplication is more that 64-bit number
    /*if (IsConstant() && r->IsConstant() && Size() <= 8*sizeof(verific_int64) && r->Size() <= 8*sizeof(verific_int64)) {
        verific_int64 rint = r->Integer() ;
        verific_int64 lint = Integer() ;
        VhdlValue *result = new VhdlInt(lint*rint) ;
        VhdlNonconst *ncresult = result->ToNonconst() ;
        delete r ;
        delete this ;
        return ncresult ;
    }*/

    // VIPER 4317 : A shift-and-add multiplication algorithm is implemented
    // in alter of the above code.
    if (IsConstant() && r->IsConstant()) {
        unsigned l_size = Size() ;
        unsigned r_size = r->Size() ;
        unsigned ret_len = l_size + r_size ;

        Array *initial_net = new Array(1) ;
        initial_net->Insert(_gnd) ;
        VhdlNonconst *mul_val = new VhdlNonconst(initial_net) ;
        mul_val->Adjust(ret_len) ;

        if (l_neg) (void)UnaryMin(from,VhdlNode::Pwr()) ;
        if (r_neg) (void) r->UnaryMin(from,VhdlNode::Pwr()) ;

        // This is the implementation of shift-and-add multiplication operation:
        // The result of the multiplication will be of length equal to the summation
        // of the lengths of two operands. Them while traversing the multiplier
        // from MSB, if the bit encountered is 0, just left shift the result otherwise
        // if the bit encountered is 1, left-shift the result and add with multiplicand.
        VhdlNonconst *multiplicand = this ;
        VhdlNonconst *multiplier = r ;
        unsigned min_size = r_size ;

        if (r_size > l_size) {
            multiplicand = r ;
            multiplier = this ;
            min_size = l_size ;
        }

        multiplicand->Adjust(ret_len) ;
        for (int i = (int)(min_size-1); i >= 0; i--) {
            VERIFIC_ASSERT(mul_val) ; // Just to be sure
            mul_val = mul_val->Shift((new VhdlInt(verific_int64(1)))->ToNonconst(),0,0,0,from) ;
            if (multiplier->GetBit((unsigned)i) == _pwr) {
                VERIFIC_ASSERT(mul_val) ; // Just to be sure
                mul_val = mul_val->Plus(multiplicand->Copy()->ToNonconst(),_gnd,from) ;
            }
        }

        // If the negative-ness of both the operands are different the result will be negative
        // thus swaping the sign of the result
        VERIFIC_ASSERT(mul_val) ; // Just to be sure
        if (l_neg ^ r_neg) (void) mul_val->UnaryMin(from,VhdlNode::Pwr()) ;
        if (IsSigned()) mul_val->SetSigned() ;

        delete multiplicand ;
        delete multiplier ;
        return mul_val ;
    }

    if (IsStaticElab()) { // Don't proceed for static elaboration
        delete r ;
        delete this ;
        return 0 ;
    }
    delete r ;
    return 0 ;
}

VhdlNonconst *
VhdlNonconst::Divide(VhdlNonconst *r, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    // First handle divide by constant power-of-two.
    // Even if the non-constant side is 'signed', we can shift.
    if (r->IsConstant() && !r->HasX()) { // VIPER issue 2272 : preserve X info (do not interpret as don't-care)
        // Divide an unsigned by a constant amount
        verific_int64 amount = r->Integer() ;
        unsigned swap_it = 0 ;
        if (amount<0) {
            swap_it = 1 ;
            amount = -amount ;
        }
        if (amount==0) {
            from->Error("attempt to divide by 0") ;
            delete r ;
            delete this ;
            return 0 ;
        }

        // Viper #5442 : No need for a divider if both operands are constants
        if (IsConstant() && !HasX()) {
            verific_int64 left_amount = Integer() ;
            verific_int64 div_result = left_amount / amount ;
            if (swap_it) div_result = -div_result ;

            VhdlNonconst *result = (new VhdlInt(div_result))->ToNonconst() ;
            result->Adjust(Size()) ;

            delete this ;
            delete r ;
            return result ;
        }
        // Calculate power of 2 thats at least as large as our number. Block integer overflow
        verific_int64 cnt=1; unsigned bits=0 ;
        while (cnt<amount && bits<=sizeof(unsigned)*8) { bits++; cnt*=2 ; }
        // Now see if it is exactly power-of-2
        if (cnt==amount) {
            // Divide by a power of two
            // For 'unsigned' LHS, This is a right-shift (MSB->LSB) of 'bits'
            // For 'signed', we need to be more creative (see issue 1196)
            // Problem : LHS is signed, shifting negative numbers rounds down, and not
            // up as VHDL defines (-1 / 2 = "111">>1 = "11" = -1, while in VHDL it's 0.)
            // But it is not always incorrect : -6 / 2 = "11010" / 2 = "1101" = -3. Correct.
            // We can do unaryminus - shift - unaryminus, but thats a lot of logic.
            // Try decrement - shift - increment : nicer, but not the best
            // Here goes (Verific Intellectual Property) :
            //  OR the LSB bits that are dropped : That's the 'rounding' difference between signed and unsigned
            //  Put a incrementor in the other (size-bits) bits
            //  Drive the incrementor carry-in with (sign AND the_OR).
            // This will always do the shift, only if a negative number has
            // all LSB bits are : then its incremented too.
            // That is exactly compensating for the 'rounding' problem with
            // dividing negative numbers (by a power of two).

            Net *round_up = Gnd() ; // bit to determine if we need signed round-up.
            if (IsSigned()) {
                Net *sign = (Net*)_nets->At(0) ;
                // Get the LSB bits :
                VhdlNonconst *or_val = Copy()->ToNonconst() ;
                // Strip off all but the LSB bits (that will be shifted out :
                or_val->Adjust(bits) ;
                // OR these guys :
                VhdlNonconstBit *or_result = or_val->Reduce(VHDL_REDOR, from) ;
                round_up = (or_result) ? or_result->GetBit() : 0 ;
                delete or_result ;
                // AND this with the sign bit (don't round-up if value is not negative) :
                round_up = from->And(round_up, sign) ;
            }

            // Now cut-off the 'amount' lsb bits :
            _nets->ReSize((Size()>bits) ? Size()-bits : 0) ;
            // If it went down to 0 bits, result is always 0 :
            // Viper #3880 : If Size() is 0, no adder needed, result is 0
            unsigned result_always_zero = 0 ;
            if (Size()==0) {
                result_always_zero = 1 ; // Viper #3880
                _nets->InsertLast(Gnd()) ;
            } else if (IsSigned() && swap_it) {
                // VIPER# 3572 : Need to extend by one bit. When dividing by negative
                // number, if numerator is signed, taking two's complement won't work
                // when numerator is max_negative_number of that width (-128 for 8 bits)
                _nets->InsertFirst(_nets->At(0)) ;
            }

            // See if we need an adder at all.
            if (!result_always_zero && (round_up!=Gnd() || swap_it)) {
                // Prepare right side of adder :
                // Just add "000..0", so it becomes the round-off increment adjuster.
                // If RHS is negative, we also use the adder to decrement ( -x = INV(x - 1) ). So add "111..1" :
                VhdlNonconst *right = (new VhdlInt((verific_int64)((swap_it) ? -1 : 0)))->ToNonconst() ;

                // Now increment this (on carry).
                (void) Plus(right, round_up, from) ;
                // Strip off adder's carry-out (not needed, since result always fits in size-bits size)
                _nets->Remove(/*position*/0, /*amount of bits removed*/1) ;

                // If we needed to swap sign, invert the result
                // (after we decremented the in the adder) :
                if (swap_it) (void) Invert(from) ;
            }

            delete r ;
            return this ;
        } // else : not a power of two, so build regular divide block
    }
    if (IsStaticElab()) { // Don't proceed for static elaboration
        delete r ;
        delete this ;
        return 0 ;
    }

    if (IsEqual(r)) { // Viper #3122: value/value -> 1
        VhdlNonconst *result = (new VhdlInt((verific_int64)1))->ToNonconst() ;
        result->Adjust(Size()) ;

        delete this ;
        delete r ;
        return result ;
    }

    delete r ;
    return 0 ;
}

VhdlNonconst *
VhdlNonconst::Modulo(VhdlNonconst *r, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    // For modulo, we can treat the (left) value as unsigned list of bits.
    // Even if left is signed. Works both for positive and negative 'right' side.
    // Just set the 'sign' interpretation of the result to the 'right' sign.

    // Pick up the sign bit before we adjust (truncate) the value :
    Net *sign_bit = (r->IsSigned()) ? (Net*)r->_nets->At(0) : 0 ;

    if (r->IsConstant() && !r->HasX()) { // VIPER issue 2272 : preserve X info (do not interpret as don't-care)
        verific_int64 amount = r->Integer() ;
        if (amount<0) {
            // Modulo follows ABS value of the right side
            amount = -amount ;
        }
        if (amount==0) {
            from->Error("attempt to divide by 0") ;
            delete r ;
            delete this ;
            return 0 ;
        }

        // if amount is 1 (or -1) then the result must always be 0.. (issue 2208)
        if (amount==1) {
            _nets->Reset() ;
            _nets->InsertLast(Gnd()) ;
            delete r ;
            return this ;
        }

        if (IsConstant() && !HasX()) { // VIPER issue 2272 : preserve X info (do not interpret as don't-care)
            // Viper #5923: both numerator and denominator are constants
            verific_int64 left_amount = Integer() ;
            verific_int64 right_amount = r->Integer() ;

            unsigned left_signed = (left_amount < 0) ;
            unsigned right_signed = (right_amount < 0) ;

            // VIPER #6699 : If value of left operand is 0, result will be 0. If
            // (left_amount % right_amount) == 0 (sign may be different), result will be 0
            verific_int64 result_value = 0 ;
            if (left_amount == 0) {
                result_value = 0 ;
            } else if (left_signed == right_signed) {
                result_value = left_amount % right_amount ;
            } else {
                verific_int64 tmp_result = (left_amount % right_amount) ;
                result_value = (tmp_result==0) ? 0 : (right_amount % (tmp_result)) ;
            }

            VhdlNonconst *result = (new VhdlInt(result_value))->ToNonconst() ;
            result->Adjust(Size()) ;

            delete this ;
            delete r ;
            return result ;
        }

        // Calculate power of 2 thats at least as large as our number. Block integer overflow
        verific_int64 cnt=1; unsigned bits=0 ;
        while (cnt<amount && bits<=sizeof(unsigned)*8) { bits++; cnt*=2 ; }
        // Now see if it is exactly power-of-2
        if (cnt==amount) {
            // Mod is a power of 2. Adjust to the required number of bits :
            // unsigned will be cut-off at value N-1 (N==2**bits). signed will be cut-off at value -N.
            Adjust(bits) ;

            // Just make sure to set the sign of the result based on sign of right :
            // Issue 3022 : do not rely on IsSigned() of the value. Use the sign bit instead :
            // So now (after size adjustment) adjust the sign of the return value : It is signed if the sign bit is not constant GND.
            _is_signed = (sign_bit && sign_bit!=Gnd()) ? 1 : 0 ;
            delete r ;
            return this ;
        }
    }
    if (IsStaticElab()) { // Don't proceed for static elaboration
        delete r ;
        delete this ;
        return 0 ;
    }

    if (IsEqual(r)) { // Viper #3122: value%value -> 0
        VhdlNonconst *result = (new VhdlInt((verific_int64)0))->ToNonconst() ;
        result->Adjust(Size()) ;

        delete this ;
        delete r ;
        return result ;
    }

    delete r ;
    return 0 ;
}

VhdlNonconst *
VhdlNonconst::Remainder(VhdlNonconst *r, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    // For remainder, we always take the absolute value of
    // either operand, and take the sign of the left value.

    // Issue 3022 : do not rely on IsSigned() of the value. Use the sign bit instead :
    Net *sign_bit = (IsSigned()) ? (Net*)_nets->At(0) : 0 ;

    if (r->IsConstant() && !r->HasX()) { // VIPER issue 2272 : preserve X info (do not interpret as don't-care)
        verific_int64 amount = r->Integer() ;
        if (amount<0) {
            // Remainder follows ABS value of the right side
            amount = -amount ;
        }
        if (amount==0) {
            from->Error("attempt to divide by 0") ;
            delete r ;
            delete this ;
            return 0 ;
        }
        // if amount is 1 (or -1) then the result must always be 0..
        if (amount==1) {
            _nets->Reset() ;
            _nets->InsertLast(Gnd()) ;
            delete r ;
            return this ;
        }

        if (IsConstant() && !HasX()) { // VIPER issue 2272 : preserve X info (do not interpret as don't-care)
            // Viper #5923: both numerator and denominator are constants
            verific_int64 left_amount = Integer() ;
            verific_int64 right_amount = r->Integer() ;

            verific_int64 result_value = (left_amount % right_amount) ;

            VhdlNonconst *result = (new VhdlInt(result_value))->ToNonconst() ;
            result->Adjust(Size()) ;

            delete this ;
            delete r ;
            return result ;
        }

        // Calculate power of 2 thats at least as large as our number. Block integer overflow
        verific_int64 cnt=1; unsigned bits=0 ;
        while (cnt<amount && bits<=sizeof(unsigned)*8) { bits++; cnt*=2 ; }
        // Now see if it is exactly power-of-2
        if (cnt==amount) {
            // Mod is a power of 2. Adjust to the required number of bits :
            // Now cut-off all but the lsb 'bits' number of bits bits, so
            // unsigned will be cut-off at value N-1 (N==2**bits). signed will be cut-off at value -N.
            Adjust(bits) ;

            // Just make sure to set the sign of the result based on sign of left :
            // Issue 3022 : do not rely on IsSigned() of the value. Use the sign bit instead :
            // So now (after size adjustment) adjust the sign of the return value : It is signed if the sign bit is not constant GND.
            _is_signed = (sign_bit && sign_bit!=Gnd()) ? 1 : 0 ;
            delete r ;
            return this ;
        }
    }
    if (IsStaticElab()) { // Don't proceed for static elaboration
        delete r ;
        delete this ;
        return 0 ;
    }

    delete r ;
    return 0 ;
}

VhdlNonconst *
VhdlNonconst::Binary(VhdlNonconst *r, unsigned type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    // Bit-by-bit Binary operators
    if (Size() != r->Size()) {
        // If sizes differ, need to pad the smallest
        unsigned s = MAX(Size(),r->Size()) ;
        Adjust(s) ;
        r->Adjust(s) ;
    }

#ifdef DB_INFER_WIDE_OPERATORS
    // Create wide operators
    // Only do this for multibit operations AND
    // if at least one side is non-constant.
    // Otherwise, we give in too much in constant propagation (which could result in errors).
    if (Size()>1 && (!IsConstant() || !r->IsConstant()) && _nl) {
        // Create output net array :
        Array *outputs = new Array(Size()) ;
        prim_type gate_type = OPER_WIDE_AND ;
        switch (type) {
        case VHDL_and  : gate_type = OPER_WIDE_AND ;  break ;
        case VHDL_nand : gate_type = OPER_WIDE_NAND ; break ;
        case VHDL_or   : gate_type = OPER_WIDE_OR ;   break ;
        case VHDL_nor  : gate_type = OPER_WIDE_NOR ;  break ;
        case VHDL_xor  : gate_type = OPER_WIDE_XOR ;  break ;
        case VHDL_xnor : gate_type = OPER_WIDE_XNOR ; break ;
        default : VERIFIC_ASSERT(0) ; // these were the only wide binary operators allowed in vhdl
        }
        (void) _nl->WideBinary(gate_type, _nets, r->_nets, outputs, from->Linefile()) ;
        // Put result inside returned value (this) :
        delete _nets ;
        _nets = outputs ;
        delete r ;
        return this ;
    }
#endif // DB_INFER_WIDE_OPERATORS
    Net *lnet, *rnet ;
    unsigned i ;
    FOREACH_ARRAY_ITEM_BACK(_nets, i, lnet) {
        rnet = (Net*)r->_nets->At(i) ;
        switch (type) {
        case VHDL_and  : lnet = from->And(lnet,rnet) ;  break ;
        case VHDL_nand : lnet = from->Nand(lnet,rnet) ; break ;
        case VHDL_or   : lnet = from->Or(lnet,rnet) ;   break ;
        case VHDL_nor  : lnet = from->Nor(lnet,rnet) ;  break ;
        case VHDL_xor  : lnet = from->Xor(lnet,rnet) ;  break ;
        case VHDL_xnor : lnet = from->Xnor(lnet,rnet) ; break ;
        default : VERIFIC_ASSERT(0) ;
        }
        _nets->Insert(i,lnet) ;
    }
    delete r ;
    return this ;
}

VhdlNonconstBit *
VhdlNonconst::Equal(VhdlNonconst *r, unsigned neq, const VhdlTreeNode *from, const Set* this_dc /*=0*/, const Set* dc_r /* 0*/)
{
    // This flag x_is_meta is used for creating equal operators for choice in
    // matching case and select expressions containing meta value in choice as
    // in Vhdl 2008. Unlike Vhdl2002 then the net X() created for meta value
    // should not match with only X net but with any net. x_is_meta can only be true
    // for matching case/select stmts synthesis

    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    // Equal-compare two scalar values.

    // If sign of left and right is different, we need to do 'signed' compare.
    // In that case, if the unsigned side is the largest, we need
    // to extend that (unsigned) side one bit, to make it signed positive.
    if (IsSigned() != r->IsSigned()) {
        if (!IsSigned() && Size()>=r->Size()) Adjust(Size()+1) ;
        if (!r->IsSigned() && r->Size()>=Size()) r->Adjust(r->Size()+1) ;
    }

    // Now, make both sides equal length.
    if (Size() != r->Size()) {
        // If sizes differ, need to zero-pad the smallest
        unsigned s = MAX(Size(),r->Size()) ;
        Adjust(s) ;
        r->Adjust(s) ;
    }

#ifdef DB_INFER_WIDE_OPERATORS
    // Create equal operator directly
    // for non-constant result and
    // don't do this for any meta logic compare... (CHECK ME what to do there)

    // Viper #7171: Check width.. Infer wide operator only if it is wide
    if ((Size() > 1) && (!IsConstant() || !r->IsConstant()) &&
        (!IsMetalogical() && !r->IsMetalogical()) && _nl) {  // Not a constant compare
        Net *result = 0 ;
        if (neq) {
            (void) _nl->NotEqual(_nets, r->_nets, &result, from->Linefile()) ;
        } else {
            (void) _nl->Equal(_nets, r->_nets, &result, from->Linefile()) ;
        }

        delete r ;
        delete this ;
        return new VhdlNonconstBit(result) ;
    }
#endif // DB_INFER_WIDE_OPERATORS

    // If we compare user-encoded (and thus scalar) values,
    // the X really means a don't care : one-hot encoding.
    // So, treat X values differently for user-encoded values

    Net *lnet, *rnet, *result ;
    if (Size() == 0) {
        // Comparing two NULL arrays.
        // return TRUE
        delete r ;
        delete this ;
        return new VhdlNonconstBit((neq) ? Gnd() : Pwr()) ;
    } else if (Size() == 1) {
        // Single-bit Equal/Nequal
        // Use immediate binary gates, to avoid double inverters
        // created when we use the xor and reduction operator below
        // This (double inverters) is annoying since we loose clk info on clk=='1' compares
        lnet = GetBit() ;
        rnet = r->GetBit() ;
        // Unlike Verilog, Equal in VHDL is always EXACT compare.
        if ((this_dc && this_dc->Get((void*)(unsigned long)(0 + 1))) || (dc_r && dc_r->Get((void*)(unsigned long)(0 + 1)))) {
            result = Pwr() ;
        } else if (lnet == X() || rnet == X()) {
            // Different handling of X for user-encoded values or regular values.
            if (UserEncoded()) {
                result = Pwr() ; // compare to x is always TRUE.
            } else {
                result = (lnet==rnet) ? Pwr() : Gnd() ; // Result is only TRUE if both nets are X
            }
            if (neq) result = from->Inv(result) ;
        } else if (lnet == Z() && rnet == Z()) { // Viper 7874: For equal operators with z return true if both are equal to z
            result = Pwr() ;
        } else if (neq) {
            // Take a normal XOR for not-equal)
            result = from->Xor(lnet,rnet) ;
        } else {
            // Take a normal XNOR for equal)
            result = from->Xnor(lnet,rnet) ;
        }
        delete r ;
        delete this ;
        return new VhdlNonconstBit(result) ;
    }

    // First XOR the bits, filter out the X nets,
    // then call ReduceNor (==0) on the result
    // This will create Reduce(N)or's for all comparisons in the
    // system. Nice, and consistent.
    //
    // NOTE : result nets are filled for NOT equal.
    // For EQUAL : the NOT of overall result will be taken in the reduction operator below
    unsigned i ;
    unsigned one_hot = (OnehotEncoded() || r->OnehotEncoded()) ;
    FOREACH_ARRAY_ITEM_BACK(_nets,i,lnet) {
        rnet = (Net*)r->_nets->At(i) ;

        if (one_hot && ((lnet == Gnd() && rnet != Pwr()) ||
                        (rnet == Gnd() && lnet != Pwr()))) {
            // compare with a one-hot gnd net. Treat as a real don't care compare.
            result = Gnd() ; // This bit is always true
        } else if ((this_dc && this_dc->Get((void*)(unsigned long)(i+1))) || (dc_r && dc_r->Get((void*)(unsigned long)(i+1)))) {
            result = Gnd() ;
        } else if (lnet == X() || rnet == X()) {
            // Different handling of X for user-encoded values or regular values.
            if (UserEncoded()) {
                result = Gnd() ; // compare to x is always TRUE.
            } else {
                result = (lnet==rnet) ? Gnd() : Pwr() ; // Result is only TRUE if both nets are X
            }
            // The pwr (if the bits were not equal) will constant prop through the reduction operator below
        } else if(lnet == Z() && rnet==Z()) { // Viper 7874: For equal operators with z return true if both are equal to z
            result = Gnd() ;
        } else {
            // Take a normal XOR for not-equal
            result = from->Xor(lnet,rnet) ;
        }
        _nets->Insert(i,result) ;
    }
    delete r ;

    return (neq) ? Reduce(VHDL_REDOR,from) : Reduce(VHDL_REDNOR,from) ;
}
// Viper 7537 : Added Equal routinne for VhdlEnum to handle metalogical values.
VhdlNonconstBit *
VhdlEnum::Equal(VhdlValue *r, unsigned neq, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_enum_id) ;

    if (!IsMetalogicalWithDC() && !r->IsMetalogicalWithDC()) {
        return (ToNonconstBit()->Equal(r->ToNonconstBit(), neq, from)) ;
    }

    delete r ;
    delete this ;
    return new VhdlNonconstBit((neq) ? Gnd() : Pwr()) ;
}

VhdlNonconstBit *
VhdlCompositeValue::Equal(VhdlValue *r, unsigned neq, const VhdlTreeNode *from)
{
    // Viper 7157: New equal function over VhdlComposite Value deals with comparison with meta
    // values. Any enum compared to a meta value returns true.
    VERIFIC_ASSERT(_values) ;

    if (!IsMetalogicalWithDC() && !r->IsMetalogicalWithDC()) {
        return (ToNonconst()->Equal(r->ToNonconst(), neq, from)) ;
    }

    if (!from || !r) { delete this ; return 0 ; }

    Set this_dc(NUM_HASH) ;
    Set dc_r(NUM_HASH) ;
    unsigned int runner = 1 ;

    // Collect the dont_cares index w.r.t to this_pointer->ToNonconst()->_nets and
    // r->ToNonconst()->_nets in the set this_dc and dc_r respectively.
    // If _nets(i) is corresponding to a meta value then i+1 is inserted in this_dc/dc_r.
    // An offset of 1 is added because 0 value cannot be added to the set s.

    CollectDCs(this_dc, runner) ;
    runner = 1 ;
    r->CollectDCs(dc_r, runner) ;

    return (ToNonconst()->Equal(r->ToNonconst(), neq, from, &this_dc, &dc_r)) ;
}

VhdlNonconstBit *
VhdlNonconst::LessThan(VhdlNonconst *r, unsigned or_equal, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from || !r) { delete this ; return 0 ; }

    if (UserEncoded() || r->UserEncoded()) {
        // Magnitude comparison between user-encoded values is hard, since
        // left and right are not binary-encoded.
        //
        if (OnehotEncoded() || r->OnehotEncoded()) {
            // One-hot encoded values CAN be compared as normal :
            // but since we fill the '1' hot from the left, we need to
            // reverse the magnitude comparison.
            // Now that we know how to compare, remove the one-hot encoding flags on the values.
            _user_encoded = 0 ;
            r->_user_encoded = 0 ;
            return r->LessThan(this, or_equal, from) ;
        } else {
            // RTL synthesis IEEE 1076.6 section 7.1.8 : allow user-encoded values, but issue warning :
            from->Warning("magnitude comparator on user-encoded values can create simulation-synthesis differences") ;
            // drop through and create a normal comparator
        }
    }

    // If sign of left and right is different, we need to do 'signed' compare.
    // In that case, if the unsigned side is the largest, we need
    // to extend that (unsigned) side one bit, to make it signed positive.
    if (IsSigned() != r->IsSigned()) {
        if (!IsSigned() && Size()>=r->Size()) Adjust(Size()+1) ;
        if (!r->IsSigned() && r->Size()>=Size()) r->Adjust(r->Size()+1) ;
    }

    // Now, make both sides equal in length.
    if (Size() != r->Size()) {
        // If sizes differ, need to extend the smallest
        unsigned s = MAX(Size(),r->Size()) ;
        Adjust(s) ;
        r->Adjust(s) ;
    }

    Net *lnet, *rnet ;
    // Signed compare of two equal-length scalars :
    // If one of them is signed, we can just swap their MSB bits !
    // No need to invert any bits.
    if (IsSigned() || r->IsSigned()) {
        // Swap MSB bits now
        lnet = (Net*)_nets->At(0) ;
        rnet = (Net*)r->_nets->At(0) ;
        _nets->Insert(0,rnet) ;
        r->_nets->Insert(0,lnet) ;
    }

    // Try constant prop first, and if non-constant, determine the lsb and msb bounds of the comparator.
    // Run LSB->MSB, carrying a 'carry' bit with us that tells if the LSB side comparison is 'true', 'false' or non-constant.
    // Then see how each bit into MSB changes that result.
    // Once we cannot do more constant comparison, we turn to a non-constant comparator.

    // Set starting carry :
    Net *carry = (or_equal) ? Pwr() : Gnd() ; // LSB below LSB bit : for <= is true, and < is false
    Net *lsb_carry = carry ; // the carry bit if comparison becomes non-constant.
    unsigned i ;
    unsigned msb_bit = 0 ;
    unsigned lsb_bit = Size()-1 ;
    FOREACH_ARRAY_ITEM_BACK(_nets, i, lnet) {
        rnet = (r->_nets) ? (Net*)r->_nets->At(i) : 0 ;
        if (lnet==X() || rnet==X()) { carry = X() ; break ; /* done */ }

        if (rnet==lnet) continue ; // Can ignore this bit

        if (lnet==Gnd() && rnet==Pwr()) { carry = Pwr() ; continue ; } // always true, regardless of LSB bits
        if (lnet==Pwr() && rnet==Gnd()) { carry = Gnd() ; continue ; } // always false, regardless of LSB bits

        // VIPER 4978 : 0 <= anything (or anything <= 1) is true (for this bit) unless LSB side is 'false', so ignore the bit, but do not yet reset carry :
        if (carry==Pwr() && (lnet==Gnd() || rnet==Pwr())) continue ;
        // Similarly : anything < 0 (or 1 < anything) is false (for this bit) unless proven 'true' on LSB side, so ignore the bit, but do not yet reset carry :
        if (carry==Gnd() && (lnet==Pwr() || rnet==Gnd())) continue ;

#ifdef VHDL_MINIMIZE_MAGNITUDE_COMPARATOR
        // One step further : Some optimizations can be done for non-constant compares, which could result in reduced size or elimination of the comparator operator.
        // 0 < something = something
        if (carry==Gnd() && lnet==Gnd()) { carry = rnet ; continue ; }
        // Similarly, 1 <= something = something :
        if (carry==Pwr() && lnet==Pwr()) { carry = rnet ; continue ; }
        // And even go to inversions :
        // something < 1 = !something    or   something <= 0 = !something
        if (carry==Gnd() && rnet==Pwr()) { carry = from->Inv(lnet) ; continue ; }
        if (carry==Pwr() && rnet==Gnd()) { carry = from->Inv(lnet) ; continue ; }
#endif

        // Here, we are going non-constant
        // if we came out of a constant compare, then save (set) the lsb bit and it's carry bit :
        if (carry) {
#ifdef VHDL_MINIMIZE_MAGNITUDE_COMPARATOR
            // Start the comparison at this lsb bit
            lsb_carry = carry ;
            lsb_bit = i ;
#else
            // To avoid regressions, do NOT adjust the lsb bit for now.
            lsb_carry = (or_equal) ? Pwr() : Gnd() ;
            lsb_bit = Size()-1 ;
#endif
            carry = 0 ;
        }
        // always update msb bit for each non-constant that is part of the comparison :
        msb_bit = i ;
    }

    if (carry) {
        // Result is constant !
        delete r ;
        delete this ;
        return new VhdlNonconstBit(carry) ;
    }
    if (IsStaticElab()) { // Don't proceed for static elaboration
        delete r ;
        delete this ;
        return 0 ;
    }

    (void) lsb_bit ; // to shut off compiler warning
    (void) msb_bit ; // to shut off compiler warning
    (void) lsb_carry ; // to shut off compiler warning
    delete r ;
    return 0 ;
}

VhdlNonconst *
VhdlNonconst::Shift(VhdlNonconst *r, unsigned shift_right, unsigned arithmetic, unsigned rotate, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    // Viper #6429: check for _nets->Size
    if (!from || !r || !_nets->Size()) { delete this ; delete r ; return 0 ;}

    unsigned i ;
    Net *n ;

    unsigned size = Size() ;
    if (r->IsConstant()) {
        verific_int64 amount = r->Integer() ;
        if (amount < 0) {
            // Swap direction
            amount = -amount ;
            shift_right = (shift_right) ? 0 : 1 ;
        }
        // Need temporary array, at least for rotators
        Array output(Size()) ;
        if (shift_right) {
            for (i=0;i<size;i++) {
                n = ((int)i<amount) ?
                        ((rotate) ? (Net*)_nets->At((i+size-((unsigned)amount % size)) % size) :
                         (arithmetic) ? (Net*)_nets->At(0) :
                         Gnd() )
                    : (Net*)_nets->At(i-(unsigned)amount) ;
                output.Insert(i,n) ;
            }
        } else {
            for (i=0;i<size;i++) {
                n = (i + (unsigned)amount >= size) ?
                        ((rotate) ? (Net*)_nets->At((i+ (unsigned)amount)%size) :
                         (arithmetic) ? (Net*)_nets->At(size-1) :
                         Gnd() )
                    : (Net*)_nets->At(i+ (unsigned)amount) ;
                output.Insert(i,n) ;
            }
        }
        // Shuv output back into _nets
        _nets->Insert(&output,0) ;
    } else {
    }
    delete r ;
    return this ;
}

VhdlNonconst *
VhdlNonconst::Power(VhdlNonconst *r, const VhdlTreeNode *from)
{
    // We support constant, power-of-2 bases - VIPER #1940
    if (!r) return 0 ;

    if (IsConstant()) {
        verific_int64 num = Integer() ;

        // If the base (left) is zero or one, then return left value
        if (num == 0 || num == 1) {
            delete r ;
            return this ;
        }

        // num**.. execution.
        // if num==2 : Take value '0001' and shift it by the 'this*1' amount.
        // if num==4 : Take value '0001' and shift it by the 'this*2' amount.
        // if num==8 : Take value '0001' and shift it by the 'this*3' amount.
        // etc.
        // This can be done efficiently by inserting '0's in between each of the end-result bits,
        // but that requires a bit of tricky coding.

        // So for now, only support 2**..:
//        // check to see if this (base) is a power-of-2 :
//        if ((num & (num - 1)) == 0) {
        if (num==2) {
            // FIXME : nonconstant signed exponent no allowed (2**(-Y)) ? Allow through, or else issue 1940 stalls.
            // if (r->IsSigned() && !r->IsConstant()) break ; // will trigger error below

            // Create value '0...001' :
            VhdlNonconst *result = (new VhdlInt((verific_int64)1))->ToNonconst() ; // Viper issue 2094. Always start with 00001 as the shifter input.

            // Need to adjust base operand in order to size the output size correctly.
            // First Prune the exponent (this), so that minimum size results :
            r->Prune() ;
            // Maximum shift amount is 2**'Size' (for integers) or input size for arrays :
            unsigned result_size = (r->Size()>=32) ? r->Size() : 1<<r->Size() ;

            // Viper #3885: if result_size is >= 32, output would be zero
            if (result_size > 32) result_size = 32 ;

            result->Adjust(result_size) ;

            // shift this (1) by 'this' amount of bits.
            result = result->Shift(r,0,0,0, from) ;
            if (result && IsSigned()) result->SetSigned() ; // Viper #3885

            // FIX ME : Do the 0-insertion in 'left->nets' for num = 4,8,16 etc here.
            delete this ; // don't need it any more.
            return result ;
        }
    }

    if (r->IsConstant()) {
        verific_int64 num = r->Integer() ;

        // If the right is zero or one, result is 1 or left
        if (num == 1) {
            delete r ;
            return this ;
        }

        if (num == 0) {
            VhdlNonconst *result = (new VhdlInt((verific_int64)1))->ToNonconst() ;
            if (result) {
                result->Adjust(Size()) ;
                if (IsSigned()) result->SetSigned() ;
            }

            delete this ; // don't need it any more.
            delete r ;
            return result ;
        }
    }

    // Here, a non-constant exponent. Give warning as our power box is empty
    // VIPER #7533: Support exponent of constant: remove the warning
    //if (from && IsRtlElab()) from->Warning("non-constant exponent operations not supported") ;

    // Viper #4914: Added a Power Box
    if (IsStaticElab()) { // Don't proceed for static elaboration
        delete r ;
        delete this ;
        return 0 ;
    }

    delete r ;
    return 0 ;
}

// Mux : controlled by 'select', select either 'this'(select==0) or 'one_val'(select==1)
VhdlValue *VhdlNonconst::SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VhdlNonconst *nonconst_val = one_val ? one_val->ToNonconst() : 0 ;
    return nonconst_val ? nonconst_val->SelectOn0(select, this, initial_value, from) : 0 ;
}
VhdlValue *VhdlNonconstBit::SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VhdlNonconstBit *nonconst_val = one_val ? one_val->ToNonconstBit() : 0 ;
    return nonconst_val ? nonconst_val->SelectOn0(select, this, initial_value, from) : 0 ;
}
VhdlValue *VhdlEnum::SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_enum_id) ;

    if (!one_val) return 0 ;

    if (IsBit() && one_val->IsBit()) {
        VhdlNonconstBit *nonconst_val = one_val->ToNonconstBit() ;
        // single-bit values : go to single bit nonconst
        return nonconst_val ? nonconst_val->SelectOn0(select, ToNonconstBit(), initial_value, from) : 0 ;
    } else {
        VhdlNonconst *nonconst_val = one_val->ToNonconst() ;
        // multi-bit constant : go to multibit nonconst
        return nonconst_val ? nonconst_val->SelectOn0(select, ToNonconst(), initial_value, from) : 0 ;
    }
}
VhdlValue *VhdlInt::SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VhdlNonconst *nonconst_val = one_val ? one_val->ToNonconst() : 0 ;
    return nonconst_val ? nonconst_val->SelectOn0(select, ToNonconst(), initial_value, from) : 0 ;
}
VhdlValue *VhdlDouble::SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VhdlNonconst *nonconst_val = one_val ? one_val->ToNonconst() : 0 ;
    return nonconst_val ? nonconst_val->SelectOn0(select, ToNonconst(), initial_value, from) : 0 ;
}
VhdlValue *VhdlCompositeValue::SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;
    VhdlCompositeValue *comp_val = one_val ? one_val->ToComposite(_values->Size()) : 0 ;
    return comp_val ? comp_val->SelectOn0(select, this, initial_value, from) : 0 ;
}

VhdlCompositeValue *VhdlCompositeValue::SelectOn0(VhdlNonconstBit *select, VhdlCompositeValue *zero_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

    if (!from || !zero_val || !select) {
        delete zero_val ;
        //delete select ; // This should not be deleted, check other SelectOn0() routines
        delete this ;
        return 0 ;
    }

#ifdef DB_INFER_WIDE_OPERATORS
    // For arrays of bit-encoded value like 'bit_vector' or 'std_logic_vector'
    // we will insert wide operators.
    if (IsBitVector() && (NumBits() > 1)) {
        VhdlValue *converted_initial_value = initial_value ? initial_value->Copy()->ToNonconst() : 0 ;
        VhdlNonconst *result = ToNonconst()->SelectOn0(select, zero_val->ToNonconst(), converted_initial_value, from) ;
        delete converted_initial_value ;
        return result ? result->ToComposite(result->Size()) : 0 ;
    }
#endif // DB_INFER_WIDE_OPERATORS
    // Do this for every element
    VhdlValue *zero_elem, *one_elem, *initial_elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_values, i, one_elem) {
        if (!one_elem) one_elem = (_default_elem_val) ? _default_elem_val->Copy() : 0 ; // default applies
        if (!one_elem) continue ; // CHECK ME : error case ? Triggered by 'aggregates1.vhdl'
        zero_elem = zero_val->TakeValueAt(i) ;
        if (!zero_elem) continue ; // CHECK ME : error case ?
        initial_elem = (initial_value) ? initial_value->ValueAt(i) : 0 ; // hold element
        one_elem = zero_elem->SelectOn1(select, one_elem, initial_elem, from) ;
        _values->Insert(i,one_elem) ;   // Re-insert value in my array
    }
    delete zero_val ; // This is now just a shell
    return this ;
}

VhdlCompositeValue *VhdlDualPortValue::SelectOn0(VhdlNonconstBit *select, VhdlCompositeValue *zero_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

    if (!from || !zero_val || !select) {
        delete zero_val ;
        delete this ;
        return 0 ;
    }

    // Do this only for for every element
    VhdlValue *zero_elem, *one_elem, *initial_elem ;

    // For dual-port aggregates, as always, we create logic for the 'write' side (index 2,3,4),
    // On a 'read' side, we do something special with the 'read_address' :
    // if condition is clocked, we should NOT call the selector. Just pass 'this' (the 'one_val').
    //   Otherwise, we will get a register on the read address. (Registers should be on the 'write' side).
    // if condition is not clocked, we should really call the selector, to follow the selection.
    // 'read_data' is always unchanged (it's only 'used', never 'assigned'.
    // All logic will be created on the users (of the read_data result),
    for (unsigned i=0; i<=4; i++) {
        if (i==0) continue ; // 'read_data' skipped.
        if (i==1 && select->IsEdge()) continue ; // 'read_address' needs to be selected..?
        // Call selector on the element :
        one_elem = ValueAt(i) ;
        if (!one_elem) continue ; // CHECK ME : error case ? Triggered by 'aggregates1.vhdl'
        zero_elem = zero_val->TakeValueAt(i) ; // actually remove the element value from the array
        if (!zero_elem) continue ; // CHECK ME : error case ?
        initial_elem = (initial_value) ? initial_value->ValueAt(i) : 0 ; // hold element
        one_elem = zero_elem->SelectOn1(select, one_elem, initial_elem, from) ;
        _values->Insert(i,one_elem) ;   // Re-insert value in my array
    }
    delete zero_val ; // This is now just a shell
    return this ;
}

VhdlNonconstBit *VhdlNonconstBit::SelectOn0(VhdlNonconstBit *select, VhdlNonconstBit *zero_val, VhdlValue *initial_value, const VhdlTreeNode *from)
{
    (void) select ;        // Need to prevent "unused" build warnings
    (void) zero_val ;      // Need to prevent "unused" build warnings
    (void) initial_value ; // Need to prevent "unused" build warnings
    (void) from ;          // Need to prevent "unused" build warnings
    return 0 ;
}

// (select) ? nonconst : nonconst
VhdlNonconst *VhdlNonconst::SelectOn0(const VhdlNonconstBit *select, VhdlNonconst *zero_val, const VhdlValue *initial_value, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;

    if (!from || !select || !zero_val) {
        delete zero_val ;
        delete this ;
        return 0 ;
    }

    Net *select_net = select->GetBit() ;
    if (select_net == Gnd()) {
        delete this ;
        return zero_val ;
    } else if (select_net == Pwr()) {
        delete zero_val ;
        return this ;
    }

    (void) initial_value ;        // Need to prevent "unused" build warnings
    return 0 ;
}

#ifdef DB_INFER_WIDE_OPERATORS

// Viper 7307: Check for non synthesizability for variables assigned in posedge and negedge of same clock
void VhdlNonconst::CheckHoldNet(const VhdlNonconstBit *select, VhdlNonconst *zero_val, const VhdlValue *initial_value, const VhdlTreeNode *from) const
{
    if (!select || !zero_val || !from || !_nets) return ;

    Net* one_net ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_nets, i, one_net) {
        if (!one_net) continue ;

        if (select->IsEdge()) {
            Net* zero_net = (Net*)zero_val->_nets->At(i) ;

            if (zero_net == one_net) continue ; // No selection logic needed (not even if select is an edge)

            Net* hold_net = (initial_value) ? initial_value->GetBit((_nets->Size() - 1) - i) : zero_net ;

            // NOTE : 'hold_net' could be 0 even if the initial value is here.
            // Happens if we try to get a 'above' range bit from the initial value.
            // That could happen while in large expressions. Remember that this is before we assign back to the target.
            // So, if 'hold_net' is zero, we cannot (should not) do anything special.

            // Check VIPER 5252/5074 situation : if (clk1) ... else if (clk2) ...
            // Assume clocks are different edges (mutual exclusive conditions), so this is OK as long as 'one_net' is the hold net.
            if (hold_net!=zero_net && one_net==hold_net) {
                // Check if zero_net is clocked :
                if (IsClocked(zero_net)) {
                    // here, only the already done (zero_net) clock will have any effect.
                    continue ;
                }
            }

            // We are at the LSB bit. Test for synthesizability :
            // 'zero_val' is hold value. Test that here.
            // Also the following (!ConstNet(hold_net) && !hold_net->IsUserDeclared()) check is because of VIPER #1637
            if (hold_net && ((zero_net != hold_net) || (!ConstNet(hold_net) && !hold_net->IsUserDeclared()))) {
                from->Error("%s is not synthesizable since it does not hold its value under NOT(clock-edge) condition", "statement") ;
            }
        }
    }
}
#endif

/*************************************************************/
// Buffer Operation for concurrent assignments
/*************************************************************/

// Value 'this' will be assigned (concurrently) to target value 'init_val'.
// Purpose of this routine is to guarantee that user-defined nets do not get removed in the subsequent net-merging.
//
// So insert a buffer if :
//    1) 'this' is a constant.
//    2) 'this' represents a user-net itself
//    3) 'this' is already used by some other net in a concurrent assignment (it is 'Wired').
// VIPER 3732 :
//    4) If target size differs from the value size, do a size ADJUSTMENT first, so that all target nets are buffered individually (and do not end up being merged).
//
// Since concurrent assignment values might contain un-assigned parts of the total target value,
// there is a possibility that 'this' contains the same net as the target 'init_val'. In that
// case NEVER insert a buffer, since we would create a buffer with a feedback loops into itself.
//

VhdlValue *
VhdlInt::PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from)
{
    if (!from) return this ; // Can't insert buffers ; don't know from where they are.

    // Translate to nonconst and buffer
    VhdlNonconst *v = ToNonconst() ;

    // Without a 'init_val', we have to create maximum amount of buffers for an integer,
    // to make sure every target bit gets a buffer, regardless of incoming integer size
    // Adjustment should not have to be done here.
    if (!init_val) v->Adjust(sizeof(int)*8) ;
    return v->PreserveUserNetBuffer(0,from) ;
}

VhdlValue *
VhdlDouble::PreserveUserNetBuffer(VhdlValue * /*init_val*/, const VhdlTreeNode * /*from*/)
{
    return this ; // FIX ME for double
}

VhdlValue *
VhdlEnum::PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_enum_id) ;

    if (!from) return this ; // Can't insert buffers ; don't know from where they are.

    if (IsBit()) {
        return ToNonconstBit()->PreserveUserNetBuffer(init_val,from) ;
    } else {
        return ToNonconst()->PreserveUserNetBuffer(init_val,from) ;
    }
}

VhdlValue *
VhdlNonconstBit::PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from )
{
    VERIFIC_ASSERT(_net) ;

    if (!from) return this ; // Can't insert buffers ; don't know from where they are.

    if (ConstNet(_net) || _net->IsUserDeclared() || _net->IsWired()) {
         if (init_val && init_val->GetBit()==_net) return this ; // Don't insert buffer that would wire into init net
         _net = from->Buf(_net) ;
    }
    return this ;
}

VhdlValue *
VhdlNonconst::PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from) return this ; // Can't insert buffers ; don't know from where they are.

    Net *net ;
    unsigned i ;

    // Insert a buffer for every user net of this value
    FOREACH_ARRAY_ITEM(_nets, i, net) {
        if (ConstNet(net) || net->IsUserDeclared() || net->IsWired()) {
            if (init_val && init_val->GetBit((_nets->Size() - i) - 1)==net) continue ; // Don't insert buffer that would wire into init net
            _nets->Insert(i,from->Buf(net)) ;
        }
    }
    return this ; // For convenience
}

VhdlValue *
VhdlCompositeValue::PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

#ifdef DB_INFER_WIDE_OPERATORS
    // For arrays of bit-encoded value like 'bit_vector' or 'std_logic_vector'
    // we will insert wide buffer here by converting it to VhdlNonconst.
    if (IsBitVector() && (NumBits() > 1)) {
        VhdlValue *converted_init_val = init_val ? init_val->Copy()->ToNonconst() : 0 ;
        VhdlValue *result = ToNonconst()->PreserveUserNetBuffer(converted_init_val, from) ;
        delete converted_init_val ;
        return result ? result->ToComposite(result->NumBits()) : 0 ;
    }
#endif // DB_INFER_WIDE_OPERATORS

    VhdlValue *elem_val, *elem_init ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_values, i, elem_val) {
        if (!elem_val) elem_val = (_default_elem_val) ? _default_elem_val->Copy() : 0 ; // default applies
        if (!elem_val) continue ;
        elem_init = (init_val) ? init_val->ValueAt(i) : 0 ;
        elem_val = elem_val->PreserveUserNetBuffer(elem_init, from) ;
        _values->Insert(i, elem_val) ;
    }
    return this ;
}

/*************************************************************/
// Buffer Operation for sequential assignments
/*************************************************************/

// Value 'this' will be assigned to some target value in a (sequential) signal/variable assignment.
// Purpose of this routine is to guarantee that there is some logic that represents the line/file
// where the assignment happened.
//
// So insert a buffer if :
//    1) 'this' is a constant and user requested buffering of constants.
//    2) 'this' is not a constant, and it does not yet have a driver or the driver is from a different line/file that the (from) assignment itself.
//
// Since 'this' represents only the exact value (part) that will be assigned to its target, there is
// no danger in putting a buffer in every net of 'this'. There is no loop-around-buffer problem as in PreserveUserNetBuffer().

VhdlValue *
VhdlInt::PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from)
{
    if (!from) return this ; // Can't insert buffers ; don't know from where they are.
    if (!buffer_constants_too) return this ;

    // Translate to nonconst and buffer
    VhdlNonconst *v = ToNonconst() ;

    // FIX ME : gotta create maximum amount of buffers for an integer,
    // to make sure every target bit gets a buffer, regardless of incoming integer size
    // Adjustment should not have to be done here.
    v->Adjust(sizeof(int)*8) ;
    return v->PreserveAssignmentBuffer(1,from) ;
}

VhdlValue *
VhdlDouble::PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode * /*from*/)
{
    if (!buffer_constants_too) return this ;
    return this ; // FIX ME for double
}

VhdlValue *
VhdlEnum::PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_enum_id) ;

    if (!from) return this ; // Can't insert buffers ; don't know from where they are.
    if (!buffer_constants_too) return this ;

    if (IsBit()) {
        return ToNonconstBit()->PreserveAssignmentBuffer(1,from) ;
    } else {
        return ToNonconst()->PreserveAssignmentBuffer(1,from) ;
    }
}

VhdlValue *
VhdlNonconstBit::PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from )
{
    VERIFIC_ASSERT(_net) ;

    if (!from) return this ; // Can't insert buffers ; don't know from where they are.

    if (ConstNet(_net)) {
        if (buffer_constants_too) _net = from->Buf(_net) ;
    } else {
    }
    return this ;
}

VhdlValue *
VhdlNonconst::PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_nets) ;
    if (!from) return this ; // Can't insert buffers ; don't know from where they are.

    // Buffering at any assignment statement :
    // These are the buffer requirements to support the 'PRESERVE_ASSIGNMENTS' switch :
    // Only buffer constants if 'buffer_constants_too' is set,
    // (otherwise we break constant propagation).
    // and if the driver (expression) of this value is at a different line/file than the current assignment (from).
    // (otherwise we would create massive amounts of buffers where they are not needed to preserve the location of the assignment).
    Net *net ;
    unsigned i ;

    FOREACH_ARRAY_ITEM(_nets, i, net) {
        if (ConstNet(net)) {
            if (buffer_constants_too) net = from->Buf(net) ;
        } else {
        }
        _nets->Insert(i,net) ; // overwrite net
    }
    return this ;
}

VhdlValue *
VhdlCompositeValue::PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

    if (!from) return this ; // Can't insert buffers ; don't know from where they are.

#ifdef DB_INFER_WIDE_OPERATORS
    // For arrays of bit-encoded value like 'bit_vector' or 'std_logic_vector'
    // we will insert wide buffer here by converting it to VhdlNonconst.
    if (IsBitVector() && (NumBits() > 1)) {
        VhdlValue *result = ToNonconst()->PreserveAssignmentBuffer(buffer_constants_too, from) ;
        return result ? result->ToComposite(result->NumBits()) : 0 ;
    }
#endif // DB_INFER_WIDE_OPERATORS

    VhdlValue *elem_val ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_values, i, elem_val) {
        if (!elem_val) elem_val = (_default_elem_val) ? _default_elem_val->Copy() : 0 ; // default applies
        if (!elem_val) continue ;
        elem_val = elem_val->PreserveAssignmentBuffer(buffer_constants_too, from) ;
        _values->Insert(i, elem_val) ;
    }
    return this ;
}

/*************************************************************/
// Bit value operations
/*************************************************************/

VhdlNonconstBit *
VhdlNonconstBit::Invert(const VhdlTreeNode *from )
{
    VERIFIC_ASSERT(_net) ;

    if (!from) return 0 ;
    _net = from->Inv(_net) ;
    // VIPER #4412 : If Inv cannot return _net, return 0
    if (!_net) { delete this ; return 0 ; }
    return this ;
}

VhdlNonconstBit *
VhdlNonconstBit::Equal(VhdlNonconstBit *r, unsigned neq, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_net) ;

    // FIX ME : pass clock info through...?
    if (!from || !r) { delete this ; return 0 ; }

    if (_net == X() || r->_net==X()) {
        // Unlike Verilog, Equal in VHDL is always EXACT compare.
        // So if one bit is x, the other MUST be x to be equal
        _net = (_net==r->_net) ? Pwr() : Gnd() ;
        if (neq) _net = from->Inv(_net) ;
    } else if (neq) {
        // NOT equal is XOR
        _net = from->Xor(_net, r->_net) ;
    } else {
        _net = from->Xnor(_net, r->_net) ;
    }
    delete r ;
    // VIPER #5938 : if _net is null, return 0
    if (!_net) { delete this ; return 0 ; }
    return this ;
}

VhdlNonconstBit *
VhdlNonconstBit::Binary(VhdlNonconstBit *r, unsigned oper_type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_net) ;

    if (!from || !r) { delete this ; return 0 ; }

    // event handling. FIX ME presently errors on misuse of 'stable are not so good.
    unsigned is_event = 0 ;
    unsigned is_edge = 0 ;

    Net *rnet = r->GetBit() ;

    if (IsEvent() || r->IsEvent()) {
        // (at least) one of them is an event

        // Check that we deal with the same signals :
        if (!ContainsOnly(_net,rnet) || !ContainsOnly(rnet,_net)) {
            // VIPER 2146 : Clock enable extraction
            // Signals are not same and operation is anding, set non-edge signal as clock
            // enable to edged value and return edged value.
            if (IsEvent() && oper_type==VHDL_and) {
                SetEnableNet(GetEnableNet() ? from->And(GetEnableNet(), rnet) : rnet) ;
                delete r ;
                return this ;
            }
            if (r->IsEvent() && oper_type==VHDL_and) {
                r->SetEnableNet(r->GetEnableNet() ? from->And(r->GetEnableNet(), _net) : _net) ;
                delete this ;
                return r ;
            }
            if (IsRtlElab()) from->Error("multiple signals in event expression is not synthesizable") ;
            delete r ;
            delete this ;
            return 0 ;
        }

        if (IsEvent() && r->IsEvent()) {
            // drop through ; special case
        } else if (IsEvent() && oper_type==VHDL_and) {
            // 'r' is the clock
            Net *clk = r->GetBit() ;
            delete r ;
            // VIPER 2146 : set clock enable to returned clock edge from event
            VhdlNonconstBit *e = new VhdlEdge(clk) ;
            e->SetEnableNet(GetEnableNet()) ;
            delete this ;
            return e ;
        } else if (r->IsEvent() && oper_type==VHDL_and) {
            // 'this' is the clock
            Net *clk = GetBit() ;
            // VIPER 2146 : set clock enable to returned clock edge from event
            VhdlNonconstBit *e = new VhdlEdge(clk) ;
            e->SetEnableNet(r->GetEnableNet()) ;
            delete r ;
            delete this ;
            return e ;
        }

        // Label as 'event'
        is_event = 1 ;
    }
    if (IsEdge() || r->IsEdge()) {
        // At least one of them is an edge.

        // Check that we deal with the same signals :
        if (!ContainsOnly(_net,rnet) || !ContainsOnly(rnet,_net)) {
            // VIPER 2146 : Clock enable extraction
            // Signals are not same and operation is anding, set non-edge signal as clock
            // enable to edged value and return edged value.
            if (IsEdge() && oper_type==VHDL_and) {
                SetEnableNet(GetEnableNet() ? from->And(GetEnableNet(), rnet) : rnet) ;
                delete r ;
                return this ;
            }
            if (r->IsEdge() && oper_type==VHDL_and) {
                r->SetEnableNet(r->GetEnableNet() ? from->And(r->GetEnableNet(), _net) : _net) ;
                delete this ;
                return r ;
            }
            if (IsRtlElab()) from->Error("multiple signals in event expression is not synthesizable") ;
            delete r ;
            delete this ;
            return 0 ;
        }
        is_edge = 1 ;
    }

    // Issue 2213 :
    if (IsStable() || r->IsStable()) {
        // Binary operator on a `stable expression.
        // This is probably a typo (should be NOT <id>`stable to be an event),
        // but if it occurs, we can assume that `stable is always true. See VIPER issue 2213.
        if (IsStable() && oper_type==VHDL_and) {
            // return r (assume that this`stable is always true
            delete this ;
            return r ;
        } else if (r->IsStable() && oper_type==VHDL_and) {
            // return this (assume that r`stable is always true
            delete r ;
            return this ;
        }
        // Otherwise : (not an 'and'), treat this as a combinational expression..
    }

    // The actual network operation
    Net *result = 0 ;
    switch (oper_type) {
    case VHDL_and :   result = from->And(_net,r->_net) ; break ;
    case VHDL_nand :  result = from->Nand(_net,r->_net) ; break ;
    case VHDL_or :    result = from->Or(_net,r->_net) ; break ;
    case VHDL_nor :   result = from->Nor(_net,r->_net) ; break ;
    case VHDL_xor :   result = from->Xor(_net,r->_net) ; break ;
    case VHDL_xnor :  result = from->Xnor(_net,r->_net) ; break ;
    default : break ;
    }

    // Make sure that edge or event info does not get lost
    if (is_edge) {
        delete r ;
        delete this ;
        return new VhdlEdge(result) ;
    } else if (is_event) {
        delete r ;
        delete this ;
        return new VhdlEvent(result,1) ;
    } else {
        delete r ;
        _net = result ;
        // VIPER #5938 : if _net is null, return 0
        if (!_net) { delete this ; return 0 ; }
        return this ;
    }
}

VhdlNonconstBit *
VhdlEvent::Invert(const VhdlTreeNode *from)
{
    if (!from) return 0 ;

    // invert takes a 'event to a 'stable  or vica versa
    _is_event = (_is_event) ? 0 : 1 ;
    _net = from->Inv(_net) ; // Needed ?
    // VIPER #5938 : if _net is null, return 0
    if (!_net) { delete this ; return 0 ; }
    return this ;
}

// Viper #2101: The case statement should infer a flop when only
// one of the parallel branches has clocked region and the rest of
// the branches assign constants. The constant branches should infer
// asynchronous set reset for the flop.
VhdlValue *
VhdlValue::ConnectSetResetNetsToRegister(Array& selector_nets, Array& values, const VhdlValue *hold_value, const VhdlTreeNode *from)
{
 // Need to prevent "unused" build warnings
 (void) selector_nets ;
 (void) values ;
 (void) hold_value ;
 (void) from ;
 return 0 ;
}
