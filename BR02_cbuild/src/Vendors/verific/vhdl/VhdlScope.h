/*
 *
 * [ File Version : 1.110 - 2014/02/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_SCOPE_H_
#define _VERIFIC_VHDL_SCOPE_H_

#include "VerificSystem.h"
#include "VhdlCompileFlags.h" // Vhdl-specific compile flags

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Map ;
class Set ;
class SaveRestore;

class VhdlTreeNode ;
class VhdlIdDef ;
class VhdlMapForCopy ;
class VhdlConstraint ;
class VhdlName ;
class VhdlBlockConfiguration ;

/* -------------------------------------------------------------- */

/*
   VhdlScope is the scope class which describes the scope of a declaration.  This class contains maps
   which contain the scopes who depend on this scope, as well as the scopes which this scope
   depends on.  Identifiers of use clauses that pertain to this scope are stored here.
   The following VHDL objects have a scope (as defined by the VHDL LRM):
        entity decl w/arch. body,           configuration decl,             subprogram decl w/body,
        package decl w/body,                record type decl,               component decl,
        block statement,                    process statement,              loop statement,
        block configuration,                componenet configuration,       generate statement
*/
class VFC_DLL_PORT VhdlScope
{
public:
    explicit VhdlScope(VhdlScope *upper=0, VhdlIdDef *owner=0, unsigned transparent=0) ; // Create a new level scope

    // Copy tree node
    VhdlScope(const VhdlScope &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlScope(SaveRestore &save_restore, unsigned for_pkg_inst=0) ;

    ~VhdlScope() ;

private:
    // Prevent compiler from defining the following
    VhdlScope(const VhdlScope &) ;            // Purposely leave unimplemented
    VhdlScope& operator=(const VhdlScope &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Persistence (Binary save/restore via SaveRestore class)
    void                Save(SaveRestore &save_restore, unsigned for_pkg_inst) const ;
    unsigned            RegisterDependencies(SaveRestore &save_restore, Set *modified_using = 0) const ;
    unsigned            RegisterIdDefs(SaveRestore &save_restore) const ;
    void                SaveUsingSet(SaveRestore &save_restore) const ;
    void                RestoreUsingSet(SaveRestore &save_restore) ;
    void                CreateDependentList(Set &dependent_unit_scopes) const ; // 3757

    // Copy scope
    // Only create and return a new scope. Fields are not poupulated by this routine.
    VhdlScope *         Copy(VhdlMapForCopy &old2new) const ;

    // Populate the fields of 'this' scope inspecting 'old2new',
    // as the scope is not the owner of any identifier.
    void                Update(const VhdlScope *scope, VhdlMapForCopy &old2new, unsigned for_pkg_inst=0) ; // utility used during copy

    // Push/pop scopes while parsing. Push creates a new scope. Pop gives the old one back.
    static void         Push(VhdlIdDef *owner=0, unsigned transparent=0) ; // VIPER 2597 : Added an argument to push transparent scope
    static VhdlScope *  Pop() ;

    void                SetOwner(VhdlIdDef *owner) ;      // A way to define the 'owner' of the scope post-construction. Needed for design units.
    void                SetUpper(VhdlScope *p) ;

    // Add identifier to the scope
    unsigned            Declare(VhdlIdDef *id) ;          // Add identifier to scope
    unsigned            DeclareLabel(VhdlIdDef *id) ;     // VIPER 2597 : Declare label identifiers in non-transparent scopes. This should be called to declare only design defined label ids.
    void                Undeclare(VhdlIdDef *id) ;        // Remove identifier from scope
    void                UndeclareLabel(VhdlIdDef *id) ;   // Remove label identifiers from non-transparent scopes
    unsigned            IsDeclaredHere(VhdlIdDef *id, unsigned look_in_upper=0) const ; // Find identifier locally

    // Special routine to store predefined operators (not owned by parse-tree)
    void                DeclarePredefined(VhdlIdDef *id) ;

    // Special routine to store implicit alias ids (not owned by parse-tree)
    void                DeclareImplicitAlias(VhdlIdDef* target_id, const VhdlName* target_type_name) ;

    // Extend decl area with this scope region (e.g. set decl region for architecture to its entity scope
    void                AddDeclRegion(VhdlScope *extended_region) ;

    // Add use-clause included items
    void                AddUseClause(Set &items) ; // old way of storing use clauses
    void                AddUseClause(VhdlIdDef *prefix, const char *suffix, VhdlTreeNode *from) ; // Add use clause "use (lib.)prefix.suffix". for "use prefix.all" store suffix==NULL.

    // Dependencies : label that this scope uses another scope (by use clause or selected name)
    void                Using(VhdlScope *other_scope, VhdlTreeNode const *from = 0) ;
    void                NoLongerUsing(const VhdlScope *other_scope) ;

    // Direct access to these fields :
    Set *               GetUsing() const        { return _using ; }
    Set *               GetUsedBy() const       { return _used_by ; }

    // Give pointer to the local declarative region
    Map *               DeclArea() const ;            // Not including extended area. (char*)->(VhdlIdDef*) map of declared identifiers.
    Map *               ExtendedDeclArea() const ;    // Not including local decl area. (char*)->(VhdlIdDef*) map of declared identifiers.

    // Give pointer to the upper scope (The scope above 'this' one)
    VhdlScope *         Upper() const ;

    // Give pointer to the upper most scope (should be scope of the design unit we are in).
    VhdlScope *         TopScope() const ;

    // Find identifiers in the scope
    VhdlIdDef *         FindSingleObject(const char *name)  const ;            // Find one object by name.
    VhdlIdDef *         FindAll(const char *name, Set &result) const ;         // Find All directly visible identifiers declared by this name. Return last one included.

    VhdlIdDef *         FindSingleObjectLocal(const char *name) const ;        // Find the object if declared in this local scope (and extended area)
    VhdlIdDef *         FindAllLocal(const char *name, Set &result) const ;    // Find All identifiers declared by this name in this immediate scope (and extended area). Return last one included.
    void                FindAllLocal(Set &result) const ;    // Find All identifiers declared in this immediate scope (and extended area) Added for Viper #5694
    VhdlIdDef *         FindSingleObjectIncluded(const char *name) const ;     // Find the (single) object if use-clause 'included' in this scope
    VhdlIdDef *         FindAllIncluded(const char *name, Set &result) const ; // Find id's only in use-clauses. Return last one included.
    void                FindCommaOperators(Set &result, Set &done_scopes) const ; // Find all comma (aggregate) operators. Special since these are not real VHDL operators. Just an artifact of how we do aggregate type-resolving. Generalized for all operators, after VIPER 2962.

    // Specialty find routine : find default binding (entity) from a component name. This routine can be called during elaboration. Routine is silent. Return 0 if none found.
    VhdlIdDef *         FindDefaultBinding(const char *name, const VhdlIdDef *component=0) ; // From this scope, find which primary unit by this name is visible. Default binding rules LRM. Pass in component identifier for LRM 5.2.2(c)

    // Find identifiers that own the scopes
    VhdlIdDef *         GetOwner() const ;                 // Get the identifier that created this scope.
    VhdlIdDef *         GetSubprogram() const ;            // Get the subprogram that encompasses this scope. If any.
    VhdlIdDef *         GetLabel() const ;                 // Get the label that most closely encompasses this scope. If any.
    VhdlIdDef *         GetContainingPrimaryUnit() const ; // Get the primary unit identifier that encompasses this scope. If any (it should).
    VhdlIdDef *         GetContainingDesignUnit() const ;  // Get the secondary or primary unit that encompasses this scope (owner of the top-level scope).

    // Get the scope of containing subprogram or process :
    VhdlScope *         GetSubprogramScope() ; // If this scope occurs inside a subprogram, return that subprogram scope.
    VhdlScope *         GetProcessScope() ; // If this scope occurs inside a process, return that process scope.

    // Additional accessor methods
    Map *               GetThisScope() const             { return _this_scope ; }    // Same functionality as DeclArea(), but the Name is more explicit
    Map *               GetUseClauses() const            { return _use_clauses ; }
    Map *               GetUseClauseUnits() const        { return _use_clause_units ; } // Map (VhdlIdDef*)->(char*) represents all use clauses in this scope "use (lib.)id.all" creates id->NULL entry, "use (lib.)id.name" creates an id->name entry.
    VhdlScope *         GetExtendedDeclAreaScope() const { return _extended_decl_area ; }
    Array *             GetPredefinedOperators() const   { return _predefined ; }

    // VIPER #3174 : Check declared identifiers to issue error for incomplete type
    void                CheckIncompleteTypeDefs() const ;

    // Check/set if scope is from a process or subprogram :
    unsigned            IsProcessScope() const            { return _is_process_scope ; } // Returns 1 this scope is the scope of a process
    void                SetIsProcessScope()               { _is_process_scope = 1 ; } // Set that this scope is the scope of a process
    unsigned            IsSubprogramScope() const         { return _is_subprogram_scope ; } // Returns 1 this scope is the scope of a subprogram
    void                SetIsSubprogramScope()            { _is_subprogram_scope = 1 ; } // Set that this scope is the scope of a subprogram

    // wait statement handling
    unsigned            IsWaitStatementNotAllowed() const { return _wait_not_allowed ; } // Returns 1 if wait statement is not allowed within this scope
    void                SetWaitStatementNotAllowed()      { _wait_not_allowed = 1 ; } // Set that wait statement is not allowed within this scope
    unsigned            HasWaitStatement() const          { return _has_wait_statement ; }     // Returns 1 if scope contains a wait statement
    void                SetHasWaitStatement()             { _has_wait_statement = 1 ; } // Set that scope contains a wait statement

    // impure access (access to external signal/variable)
    unsigned            HasImpureAccess() const           { return _has_impure_access || _has_local_file_access ; } // Returns 1 is external variable/signal access is done within this scope
    void                SetHasImpureAccess()              { _has_impure_access = 1 ; } // Set that external variable/signal access is done within this scope
    void                SetHasLocalFileAccess()           { _has_local_file_access = 1 ; } // Set that internal file access is done within this scope (VIPER #6801)
    unsigned            HasLocalFileAccess() const        { return _has_local_file_access ; }

    // Transparent scope :
    void                SetTransparent()                  { _transparent = 1 ; }
    unsigned            IsTransparent() const             { return _transparent ; } // Returns 1 if this scope is transparent

    unsigned            IsContainedIn(const VhdlScope *scope) const ;
    void                ResetThisScope() ; // VIPER #6341
    void                ResetPredefinedOps() ;
    void                ResetPredefinedOpsForType(const VhdlIdDef *type_id) ;
    // Internal routine to blindly store iddefs in _this_scope
    void                ForceDeclare(const VhdlIdDef *id) ;

    // Calculate the signature of this scope, based on the ids it contains.
    unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

    // Elaboration
    void                ElaborateDependencies() const ; // Elaborate all packages that this scope depends on

    unsigned            PushStack() ;   // Push this scope (identifier values/constraints) on a stack.
    unsigned            PopStack() ;    // Pop the last scope off the stack (re-install identifier values/constraints)
    unsigned            PushStack(Map* scope_stack) ;   // Push this scope (identifier values/constraints) on a stack.
    unsigned            PopStack(Map* scope_stack) ;    // Pop the last scope off the stack (re-install identifier values/constraints)
    static void         ResetStack() ;

    void                PushStackDependencies(Map* scope_stack) const ;
    void                PopStackDependencies(Map* scope_stack) const ;

private:
    static void         SetConstraintRecursively(VhdlIdDef *id, VhdlConstraint *constraint) ; // For use in PopStack() routine

private:
    static Array *_elaboration_stack ;  // Stack of scope identifier constraint/values.

private:
    VhdlScope    *_upper ;              // Upper (directly visible) scope level
    Map          *_this_scope ;         // The present-level scope table
    Map          *_use_clauses ;        // Use-Clause included id's (now replaced by use_clause_units).
    Map          *_use_clause_units ;   // Map (VhdlIdDef*)->(char*) represents all use clauses in this scope "use (lib.)id.all" creates id->NULL entry, "use (lib.)id.name" creates an id->name entry.

    VhdlScope    *_extended_decl_area ; // A declarative area that's part of this decl region. Entity scope for a architecture for example.
    Array        *_predefined ;         // List to store predefined operators / anonymous types, so we can delete them with the scope

    // (External) dependencies : links to VhdlScope's that this scope depends on, and scopes that depend on me. (via selected names/use clauses).
    Set          *_using ;              // Set of VhdlScope* that this scope depends on. (scopes from use clauses called within this scope)
    Set          *_used_by ;            // Set of VhdlScope* that depend on 'this' scope. (scopes that 'use' this scope).

    // Special ids
    VhdlIdDef    *_owner_id ;           // Identifier that created the scope (subprogram or label or record or unit or so)

    // Used only by VhdlScope::AddUseClause for warning message handling
    Set          *_hidden_ids ;         // Multiply-defined ids that were made hidden (saved by name)

    // Bit flags (set during analysis) :
    unsigned     _transparent:1 ;         // Flag to indicate transparent scope (VIPER #2597)
    unsigned     _is_process_scope:1 ;    // Flag to say that this scope is the scope of a VHDL process.
    unsigned     _is_subprogram_scope:1 ; // Flag to say that this scope is the scope of a VHDL subprogram.
    unsigned     _wait_not_allowed:1 ;    // Flag to indicate scoped object where wait statement is not allowed (VIPER #3496, #2966)
    unsigned     _has_wait_statement:1 ;  // Flag to indicate that a wait statement occurrs somewhere in this (process or subprogram) scope (VIPER 4518)
    unsigned     _has_impure_access:1 ;   // Flag to indicate that there is 'external' variable/signal access done from within this scope.
    unsigned     _has_local_file_access:1 ;   // Flag to indicate that there is internal file access done from within this scope.(VIPER #6801)
} ; // class VhdlScope

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_SCOPE_H_

