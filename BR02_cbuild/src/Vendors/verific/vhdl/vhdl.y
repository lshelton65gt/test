%{
/*
 *
 * [ File Version : 1.236 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

////////////////////////////////////////////////////////////////////////////
//
//                        DEVELOPERS NOTE
//
// Verific recommends that you do not change this file, so that you do NOT
// need to re-run bison either. Verific ran bison on it already, which
// resulted in the file vhdl_yacc.cpp, which is included in this directory.
//
// If you need to make a custom change to this .y file, or have another
// reason why you need to re-run bison, then make sure you use bison 1.35
// or later, and note that Verific cannot guarantee that the created vhdl_yacc.cpp
// file will behave properly or even compile on various compilers.
//
// This file contains to %locations directive to keep track
// of accurate line/file info for all bison rules. This is not supported
// in bison 1.28, so use bison 1.35 or later to compile this file.
// Specifically, Verific tested this file only with Bison version 1.35 and 1.875.
//
// Use
//    bison -v -d -l -p vhdl -o vhdl_yacc.cpp vhdl.y
//     mv yeri_yacc.c vhdl_yacc.cpp
//     mv vhdl_yacc.h vhdl_yacc.h
// To create a valid cpp file for compilation with the flex-generated tokenizer
// and the rest of the analyzer code.
//
////////////////////////////////////////////////////////////////////////////

/* Bison forgot to rename this global var */
#define yylloc   vhdllloc
//#define YYDEBUG 1
#define YYLTYPE_IS_TRIVIAL 1 // to fix stack-grow problem in Bison 1.35

#include "VerificSystem.h"

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Strings.h"
#include "Message.h"

#include "VhdlTreeNode.h"
#include "vhdl_file.h"
#include "VhdlScope.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlSpecification.h"
#include "VhdlStatement.h"
#include "VhdlName.h"
#include "VhdlUnits.h"
#include "VhdlMisc.h"
#include "VhdlIdDef.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Control how deep the parser stack can grow before a stack overflow occurs
// (usually for right recursion). Overwrite the default bison value of 10000
// to set a higher depth.  VIPER #5111
#define YYMAXDEPTH 100000

// Declare main parser functions, since yacc forgets that due to renaming
extern void vhdlerror(const char *s) ;
extern int vhdllex(void) ;
extern int vhdlparse(void) ;

 //CARBON_BEGIN
 void* gLastDeclVhdlId; // the last declared vhdl id
 //CARBON_END

/******************************* Column/Line/File Info in rules **********************/

// Since 9/2004, we let flex and bison always work together to process location
// information (column/line/file info) of parsed tokens.
// This is done because of three reasons :
//    (1) in bison, we want accurate line info for the constructs (syntax rules) parsed
//        (not relying on just the last token parsed, but generate info based on the
//        sub-rules per bison rule).
//        The only way to do that is to enable location processing, and use the
//        location range info (yyloc in bison).
//    (2) We have compile switch VERIFIC_LINEFILE_INCLUDES_COLUMNS, which requires
//        us to process location (token/rule start/end column/line) info upon at compile time.
//    (3) Bison 1.375 and later do not allow compile-time control over location processing.

// Need these 4 variables to pick-up rule line/column info from YYLLOC_DEFAULT,
// since the variable that bison uses (yyloc) is a local variable, invisible outside 'parse()'.
static unsigned first_line = 0 ;
static unsigned first_column = 0 ;
static unsigned last_line = 0 ;
static unsigned last_column = 0 ;
static unsigned rhs_0_initialized = 0 ; // VIPER #6608

// Overwrite YYLLOC_DEFAULT (executed just before a rule action executes),
// to calculate and store the rule's column info so it can be
// picked up by constructors in the rule's actions.
// NOTE: This only works for Bison version 1.35 or newer.
// Fix Bison problem for rules with 0 args. Need to take yylloc from flex directly.

// Issue 2216 : do not use line/file info from empty sub-rules.
// We need some line/file info for empty subrules, so pick up next token as normal (yylloc)
// but set their last column/line to first column/line. This way, empty rules will
// never cover text into the next token (which is not part of the subrule).
// Bison 2.00 initializes columns to 0. We initialize to 1. Need to correct this here (test for 0 column).
// VIPER #6608: Update the empty rule line/column/file using Rhs[0] only when it is initialized.
// It is initialized after bison matches any rule, and whenever bison matches a rule it calls this macro.
// So, at the first call of this macro Rhs[0] may be uninitialized but after that it is initialized.
#define YYLLOC_DEFAULT(Current, Rhs, N)                 \
    if ((N)==0) {                                       \
        if (yylloc.first_column==0) { (yylloc.first_column)++ ; (yylloc.last_column)++ ; } \
        Current              = yylloc ;                 \
        if (rhs_0_initialized) {                        \
            Current.last_line    = (Rhs)[0].last_line ; \
            Current.last_column  = (Rhs)[0].last_column ;\
        } else {                                        \
            Current.last_line    = 1 ;                  \
            Current.last_column  = 1 ;                  \
        }                                               \
    } else {                                            \
        Current.first_line   = (Rhs)[1].first_line ;    \
        Current.first_column = (Rhs)[1].first_column ;  \
        Current.last_line    = (Rhs)[N].last_line ;     \
        Current.last_column  = (Rhs)[N].last_column ;   \
    }                                                   \
    first_line   = Current.first_line ;                 \
    first_column = Current.first_column ;               \
    last_line    = Current.last_line ;                  \
    last_column  = Current.last_column ;                \
    rhs_0_initialized  = 1 ; // Now it is initialized
    // set static variables, for pick-up of 'last rule location info'..

/******************************* Routines used locally **********************/

/* present_scope is static in VhdlTreeNode. Bit dirty, but works */

static void
vhdl_declare(VhdlIdDef *id)
{
    if (VhdlTreeNode::_present_scope) (void) VhdlTreeNode::_present_scope->Declare(id) ;
}

static void
vhdl_undeclare(VhdlIdDef *id)
{
    if (!VhdlTreeNode::_present_scope) return ;

    VhdlTreeNode::_present_scope->Undeclare(id) ;
    VhdlTreeNode::_present_scope->ResetPredefinedOpsForType(id) ;
}

// VIPER 2597 : Don't declare label identifiers defined in the design in a
// transparent scope. But we should declare label ids in transparent scope
// for proper implementation of exit, continue.
static void
vhdl_declare_label(VhdlIdDef *id)
{
    if (VhdlTreeNode::_present_scope) (void) VhdlTreeNode::_present_scope->DeclareLabel(id) ;
}
/* Static to get context clause into design unit */
static Array *context_clause = 0 ;

static int inside_design_unit = 0 ;
/* Static to follow VHDL-2008 LRM section 6.5.6.1. Set type of a generic
  just during identifier creation */
static unsigned in_generic_clause = 0 ;
/* Static to find the label of a statement while we are in it */
static VhdlIdDef *last_label = 0 ;
/* Static variables to maintain implicit loop sequence number and process sequence number*/
static unsigned loop_sequence_num = 0 ; // VIPER #7494
static unsigned process_sequence_num = 0 ; // VIPER #7494

void create_context_clause()
{
    if (context_clause) return ;

    VhdlTreeNode::_present_scope = 0 ;
    VhdlScope::Push() ;

    /* Now create a 'context_clause' tree node (an Array of context items). */
    context_clause = new Array() ;

    /* Define library names 'STD' and 'WORK' */
    Array *libs = new Array() ;

    /* Declare library identifier 'std' */
    VhdlIdDef *id = new VhdlLibraryId(Strings::save("std")) ;
    vhdl_declare(id) ;
    libs->InsertLast(id) ;

    /* Declare library identifier 'work' */
    id = new VhdlLibraryId(Strings::save("work")) ;
    vhdl_declare(id) ;
    libs->InsertLast(id) ;

    /* Set them in the parse tree */
    context_clause->InsertLast(new VhdlLibraryClause(libs, 1 /*implicitly declared*/)) ;
    return ;
}

void cleanup_context_clause()
{
    unsigned i ;
    VhdlTreeNode *node ;
    FOREACH_ARRAY_ITEM(context_clause, i, node) delete node ;
    delete context_clause ;
    context_clause = 0 ;
}

void cleanup_treenode_array(Array *tree_node_array)
{
    unsigned i ;
    VhdlTreeNode *node ;
    FOREACH_ARRAY_ITEM(tree_node_array, i, node) delete node ;
    delete tree_node_array ;
}

void cleanup_iddef_array(Array *iddef_node_array)
{
    unsigned i ;
    VhdlIdDef *node ;
    FOREACH_ARRAY_ITEM(iddef_node_array, i, node) {
        vhdl_undeclare(node) ;
        delete node ;
    }
    delete iddef_node_array ;
}
// VIPER #8329 : Delete declaration if id is declared in _present_scope
void cleanup_declnode_array(Array *decl_node_array)
{
    unsigned i, j ;
    VhdlDeclaration *decl ;
    unsigned can_delete_arr = 1 ;
    FOREACH_ARRAY_ITEM(decl_node_array, i, decl) {
        Array *ids = decl ? decl->GetIds(): 0 ;
        VhdlIdDef *id = decl ? decl->GetTypeId(): 0 ;
        unsigned can_delete = 1 ;
        if (ids) {
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (!id) continue ;
                if ((VhdlTreeNode::_present_scope) && !VhdlTreeNode::_present_scope->IsDeclaredHere(id)) {
                    can_delete = 0 ;
                    break ;
                }
            }
        } else if (id) {
            if ((VhdlTreeNode::_present_scope) && !VhdlTreeNode::_present_scope->IsDeclaredHere(id)) {
                can_delete = 0 ;
            }
        }
        if (can_delete) {
            delete decl ;
            decl_node_array->Insert(i, 0) ;
        } else {
            can_delete_arr = 0 ;
        }
    }
    if (can_delete_arr) delete decl_node_array ;
}
// VIPER #8429 : Delete statement is its label can be undeclared
void cleanup_statement_array(Array *stmt_array)
{
    unsigned i ;
    VhdlStatement *stmt ;
    unsigned can_delete_arr = 1 ;
    FOREACH_ARRAY_ITEM(stmt_array, i, stmt) {
        VhdlIdDef *label = stmt ? stmt->GetLabel(): 0 ;
        if (label && (VhdlTreeNode::_present_scope) && !VhdlTreeNode::_present_scope->IsDeclaredHere(label)) {
            can_delete_arr = 0 ;
            continue ;
        }
        delete stmt ;
        stmt_array->Insert(i, 0) ;
    }
    if (can_delete_arr) delete stmt_array ;
}
// VIPER #6229 : Update linefile of the given node to match current rule linefile
// When update_ending_linefile is set, it will update the ending position otherw ise
// it will not update the ending line/column of the linefile. We need this because
// if the last sub-rule is not present or it is comment the linefile is not set.
void update_linefile(VhdlTreeNode *node, unsigned update_ending_linefile = 1)
{
    linefile_type lf = (node) ? node->Linefile() : 0 ;
    if (!lf) return ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    lf->SetLeftCol(first_column) ;
    lf->SetLeftLine(first_line) ;
    if (update_ending_linefile) lf->SetRightCol(last_column) ;
    if (update_ending_linefile) lf->SetRightLine(last_line) ;
#else
    (void) update_ending_linefile; // to circumvent unused parameter warning
#endif
}

static void
add_pre_comments_to_node(VhdlTreeNode *node, Array *comments)
{
    if (!comments) return ; // Nothing to do
    if (node) {
        node->AddComments(comments) ; // Add the comments to the node
    } else {
        VhdlNode::DeleteComments(comments) ; // Delete comments to fix memory leak
    }
}

static void
add_post_comments_to_node(VhdlTreeNode *node, Array *comments)
{
    if (!comments) return ; // Nothing to do
    if (node) {
        vhdl_file::SetComments(node->ProcessPostComments(comments)) ;
    } else {
        VhdlNode::DeleteComments(comments) ; // Delete comments to fix memory leak
    }
}
// VIPER #7494 : Create label for argument specified concurrent statement
static void CreateAndAddImplicitLabel(VhdlStatement *stmt)
{
    if (!RuntimeFlags::GetVar("vhdl_create_implicit_labels")) return ;
    if (!stmt || stmt->GetLabel()) return ; // No need to add label

    // Create implicit label id
    char name[20] ;
    sprintf(name, "_P%d", process_sequence_num) ;

    process_sequence_num++ ;
    VhdlIdDef *implicit_label_id = new VhdlLabelId(Strings::save(name)) ;
    vhdl_declare_label(implicit_label_id) ;
    stmt->SetImplicitLabel(implicit_label_id) ;
}

%}

// Since 9/2004, we let flex and bison always work together to process location
// information (column/line/file info) of parsed tokens. (see comments elsewhere in this file)
%locations

%union {
    char      *str ;
    unsigned   unsigned_number ;
    double     real_number ;

#undef VN
#ifdef VERIFIC_NAMESPACE
#define VN Verific // Need to define this to get things working with Verific namespace active
#else
#define VN
#endif

    VN::Array     *array ;

    VN::VhdlDesignUnit *library_unit ;
    VN::VhdlDeclaration *declaration ;
    VN::VhdlStatement *statement ;
    VN::VhdlDiscreteRange *discrete_range ;
    VN::VhdlSubtypeIndication *subtype_indication ;
    VN::VhdlExpression *expression ;
    VN::VhdlName *name ;
    VN::VhdlDesignator *designator ;
    VN::VhdlIdRef *id ;
    VN::VhdlIdDef *id_def ;
    VN::VhdlTypeDef *type_def ;
    VN::VhdlSignature *signature ;
    VN::VhdlElementDecl *element_decl ;
    VN::VhdlPhysicalUnitDecl *physical_unit_decl ;
    VN::VhdlConfigurationItem *configuration_item ;
    VN::VhdlBlockConfiguration *block_configuration ;
    VN::VhdlComponentConfiguration *component_configuration ;
    VN::VhdlBindingIndication *binding_indication ;
    VN::VhdlSpecification *specification ;
    VN::VhdlInterfaceDecl *interface_decl ;
    VN::VhdlIterScheme *iter_scheme ;
    VN::VhdlOptions *options ;
    VN::VhdlDelayMechanism *delay_mechanism ;
    VN::VhdlBlockGenerics *block_generics ;
    VN::VhdlBlockPorts *block_ports ;
    VN::VhdlCaseStatementAlternative *case_statement_alternative ;
    VN::VhdlFileOpenInfo *file_open_info ;
    VN::VhdlEntityClassEntry *entity_class_entry ;
    VN::VhdlTreeNode *tree_node;
    struct { VN::Array *decls; VN::Array *stmts ; VN::VhdlScope *scope ; } generate_stmt_body ; // structure with two pointers
    struct {unsigned path_token; unsigned hat_count; VN::VhdlName *path ; } ext_path_name ;
}

%start source_text

// Need to assign explicit token values at least for tokens which get saved in the parse tree.
// Otherwise, bison might assign version-dependent value to them, which will break
// back-ward compatibility of the parse tree save/restore functionality.
%token
VHDL_AMPERSAND              257   "&"
VHDL_TICK                   258
VHDL_OPAREN                 259   "("
VHDL_CPAREN                 260   ")"
VHDL_STAR                   261
VHDL_PLUS                   262
VHDL_COMMA                  263   ","
VHDL_MINUS                  264
VHDL_DOT                    265
VHDL_SLASH                  266
VHDL_COLON                  267   ":"
VHDL_SEMI                   268   ";"
VHDL_STHAN                  269   "<"
VHDL_EQUAL                  270   "="
VHDL_GTHAN                  271   ">"
VHDL_VERTICAL               272   "|" /* both | and !, need to split this into two tokens, see VHDL_BANG */
VHDL_BANG                   273   "!" /* new token */
VHDL_OBRACK                 274   "["
VHDL_CBRACK                 275   "]"
VHDL_ARROW                  276
VHDL_EXPONENT               277
VHDL_VARASSIGN              278
VHDL_NEQUAL                 279
VHDL_GEQUAL                 280
VHDL_SEQUAL                 281
VHDL_BOX                    282

VHDL_abs                    283
VHDL_access                 284
VHDL_after                  285
VHDL_alias                  286
VHDL_all                    287
VHDL_and                    288   "and"
VHDL_architecture           289
VHDL_array                  290
VHDL_assert                 291   "assert"
VHDL_attribute              292
VHDL_begin                  293
VHDL_block                  294
VHDL_body                   295
VHDL_buffer                 296
VHDL_bus                    297
VHDL_case                   298
VHDL_component              299
VHDL_configuration          300
VHDL_constant               301
VHDL_disconnect             302
VHDL_downto                 303
VHDL_else                   304
VHDL_elsif                  305
VHDL_end                    306
VHDL_entity                 307
VHDL_exit                   308
VHDL_file                   309
VHDL_for                    310
VHDL_function               311
VHDL_generate               312
VHDL_generic                313
VHDL_group                  314
VHDL_guarded                315
VHDL_if                     316
VHDL_impure                 317
VHDL_in                     318   "in"
VHDL_inertial               319
VHDL_inout                  320
VHDL_is                     321   "is"
VHDL_label                  322
VHDL_library                323
VHDL_linkage                324
VHDL_literal                325
VHDL_loop                   326
VHDL_map                    327
VHDL_mod                    328
VHDL_nand                   329   "nand"
VHDL_new                    330
VHDL_next                   331   "next"
VHDL_nor                    332   "nor"
VHDL_not                    333   "not"
VHDL_null                   334
VHDL_of                     335
VHDL_on                     336
VHDL_open                   337
VHDL_or                     338   "or"
VHDL_others                 339
VHDL_out                    340
VHDL_package                341
VHDL_port                   342
VHDL_postponed              343
VHDL_procedure              344
VHDL_process                345
VHDL_pure                   346
VHDL_range                  347
VHDL_record                 348
VHDL_register               349
VHDL_reject                 350
VHDL_rem                    351
VHDL_report                 352
VHDL_return                 353
VHDL_rol                    354
VHDL_ror                    355
VHDL_select                 356
VHDL_severity               357
VHDL_signal                 358
VHDL_shared                 359
VHDL_sla                    360
VHDL_sll                    361
VHDL_sra                    362
VHDL_srl                    363
VHDL_subtype                364
VHDL_then                   365
VHDL_to                     366   "to"
VHDL_transport              367
VHDL_type                   368
VHDL_unaffected             369
VHDL_units                  370
VHDL_until                  371   "until"
VHDL_use                    372
VHDL_variable               373
VHDL_wait                   374
VHDL_when                   375
VHDL_while                  376
VHDL_with                   377
VHDL_xnor                   378   "xnor"
VHDL_xor                    379   "xor"

VHDL_INTEGER                380
VHDL_REAL                   381
VHDL_STRING                 382
VHDL_BIT_STRING             383
VHDL_ID                     384
VHDL_EXTENDED_ID            385
VHDL_CHARACTER              386
VHDL_FATAL_ERROR            387

/* comment token */
VHDL_OPT_COMMENT            484
VHDL_NO_COMMENT             485
VHDL_NO_OPAREN              486

VHDL_implicit_guard         492
VHDL_protected              493
VHDL_interfacerange         494
VHDL_interfaceread          495
VHDL_interfaceprefixread    496
VHDL_subtyperange           497
VHDL_subtyperangebound      498

/* Vhdl 2008 */
VHDL_minimum                550
VHDL_maximum                551
VHDL_condition              552   "??"
VHDL_matching_equal         553   "?="
VHDL_matching_nequal        554   "?/="
VHDL_matching_gthan         555   "?>"
VHDL_matching_sthan         556   "?<"
VHDL_matching_gequal        557   "?>="
VHDL_matching_sequal        558   "?<="
VHDL_context                559
VHDL_end_generate           560
VHDL_parameter              561
VHDL_default                562
VHDL_matching_op            563   "?"
VHDL_to_string              564
VHDL_force                  565
VHDL_release                566
VHDL_LANGLEQ                567    "<<"  /* left angle quotation*/
VHDL_RANGLEQ                568    ">>"  /* right angle quotation*/
VHDL_AT                     569    "@"  /* similar symbol in psl */
VHDL_ACCENT                 570    "^"

VHDL_BEGIN_EXPR             571  /* Dummy internal token for expression parsing */

VHDL_to_bstring             572
VHDL_to_binary_string       573
VHDL_to_ostring             574
VHDL_to_octal_string        575
VHDL_to_hstring             576
VHDL_to_hex_string          577
VHDL_recordrange            578 /* VIPER #8085 */

//CARBON_BEGIN
// To avoid conflicts with customer defined token labels Verific has reserved the range 3000:4000 for use by customers.
// This range specified by Verific customer support (Debashis De 2014/02/12).
// If someday Verific starts to use one of these carbon defined token names then the conflict will be reported by the
// 'make lexyacc' step and the duplicate can be removed from this carbon specific list.
// TODO: These enums are used in VhdlTreeNode.cpp. Some of these already exist as defines in 
// vhdl_tokens.h (e.g. VHDL_or_reduce is VHDL_REDOR).Should we be using those instead?
VHDL_resize                 3000
VHDL_uns_resize             3001
VHDL_signed_mult            3002
VHDL_unsigned_mult          3003
VHDL_numeric_signed_mult    3004
VHDL_numeric_unsigned_mult  3005
VHDL_numeric_sign_div       3006
VHDL_numeric_uns_div        3007
VHDL_numeric_minus          3008
VHDL_numeric_unary_minus    3009
VHDL_numeric_plus           3010
VHDL_is_uns_less            3011
VHDL_is_uns_gt              3012
VHDL_is_gt                  3013
VHDL_is_less                3014
VHDL_is_uns_gt_or_equal     3015
VHDL_is_uns_less_or_equal   3016
VHDL_is_gt_or_equal         3017
VHDL_is_less_or_equal       3018
VHDL_and_reduce             3019
VHDL_or_reduce              3020
VHDL_xor_reduce             3021
VHDL_numeric_mod            3022
VHDL_i_to_s                 3023
VHDL_s_to_u                 3024
VHDL_s_to_i                 3025
VHDL_u_to_i                 3026
VHDL_u_to_u                 3027
VHDL_s_xt                   3028
//CARBON_END


%type <str>
        VHDL_STRING VHDL_BIT_STRING VHDL_ID
        VHDL_CHARACTER

%destructor { Strings::free($$) ; }
    VHDL_STRING VHDL_BIT_STRING
    VHDL_ID VHDL_CHARACTER

%type <unsigned_number>
        VHDL_INTEGER
entity_class
opt_interface_kind
opt_mode
relational_oper
shift_operator
adding_operator
multiplying_operator
logical_operator
opt_VHDL_package_body
opt_pure_impure
opt_subprogram_kind
opt_signal_kind
opt_VHDL_architecture
opt_VHDL_entity
opt_VHDL_context
opt_VHDL_package
opt_VHDL_configuration
opt_VHDL_is
opt_VHDL_BOX
opt_VHDL_shared
opt_VHDL_postponed
opt_VHDL_guarded
direction
opt_matching
opt_force_mode
opt_parameter_keyword

%type <real_number>
        VHDL_REAL

%type <declaration>
use_clause
commented_use_clause
library_clause
context_item
plain_context_item
entity_decl_item
plain_entity_decl_item
block_decl_item
plain_block_decl_item
package_decl_item
plain_package_decl_item
package_body_decl_item
plain_package_body_decl_item
subprogram_decl_item
plain_subprogram_decl_item
process_decl_item
plain_process_decl_item
configuration_decl_item
plain_configuration_decl_item
plain_protected_type_declarative_item
protected_type_declarative_item
protected_type_body_declarative_item
plain_protected_type_body_declarative_item

subprogram_decl
subprogram_body
type_decl
subtype_decl
constant_decl
signal_decl
variable_decl
file_decl
alias_decl
component_decl
attribute_decl
attribute_spec
configuration_spec
disconnection_spec
group_template_decl
group_decl

%type <statement>
entity_statement
concurrent_statement
plain_concurrent_statement
unlabeled_cc_statement
block_statement
component_instantiation_statement
generate_statement
concurrent_procedure_call_statement
process_statement
assertion_statement
conditional_signal_assignment
selected_signal_assignment
cc_conditional_signal_assignment
cc_selected_signal_assignment

sequential_statement
plain_sequential_statement
unlabeled_statement
wait_statement
report_statement
signal_assignment_statement
simple_signal_assignment
variable_assignment_statement
simple_assignment_statement
conditional_assignment_statement
selected_assignment_statement
procedure_call_statement
if_statement
case_statement
loop_statement
next_statement
exit_statement
return_statement
null_statement

plain_generate_statement
for_generate_statement
if_generate_statement
if_generate_statement_part
elsif_generate_statement
else_elsif_generate_statement
else_generate_statement
case_generate_statement
case_generate_alternative

%type <discrete_range>
discrete_range
discrete_range_subset1
range
range_subset1
range_constraint
choice
assoc_element
plain_assoc_element

%destructor { delete ($$) ; }
discrete_range
discrete_range_subset1
range
range_subset1
range_constraint
choice
assoc_element
plain_assoc_element

%type <expression>
expression
opt_expression
relation
AND_expression
OR_expression
XOR_expression
XNOR_expression
shift_expression
simple_expression
term
factor
primary
allocator
qualified_expression
record_resolution_element

waveform_element
opt_guard_expression
opt_report_expression
opt_severity_expression
opt_condition_clause
opt_timeout_clause
opt_when_condition
actual_designator
actual_part
opt_file_open
file_logical_name
opt_init_assign
element_assoc
aggregate
target
interface_subprogram_default
opt_interface_subprogram_default

%destructor { delete ($$) ; }
expression
opt_expression
relation
AND_expression
OR_expression
XOR_expression
XNOR_expression
shift_expression
simple_expression
term
factor
primary
allocator
qualified_expression
record_resolution_element

waveform_element
opt_guard_expression
opt_report_expression
opt_severity_expression
opt_condition_clause
opt_timeout_clause
opt_when_condition
actual_designator
actual_part
opt_file_open
file_logical_name
opt_init_assign
element_assoc
aggregate
target
interface_subprogram_default
opt_interface_subprogram_default

%type <name>
name
selected_name
attribute_name
indexed_name
entity_designator
prefix
signatured_prefix
simple_prefix
type_mark
resolution_indication
array_resolution_indication
record_resolution_indication
group_template_name
instantiated_unit
formal_part
opt_return_type
entity_aspect
block_spec

literal
abstract_literal
physical_literal
bit_string_literal
uninstantiated_package_name
uninstantiated_subprog_name
opt_return
external_name

%destructor { delete ($$) ; }
name
selected_name
attribute_name
indexed_name
entity_designator
prefix
signatured_prefix
simple_prefix
type_mark
resolution_indication
array_resolution_indication
record_resolution_indication
group_template_name
instantiated_unit
formal_part
opt_return_type
entity_aspect
block_spec

literal
abstract_literal
physical_literal
bit_string_literal
uninstantiated_package_name
uninstantiated_subprog_name
opt_return
external_name

%type <subtype_indication>
opt_colon_subtype_indication
subtype_indication
subtype_indication_subset1
subtype_indication_subset2

%type <designator>
designator
opt_designator
suffix
character_literal
string_literal
operator_symbol

%destructor { delete ($$) ; }
designator
opt_designator
suffix
character_literal
string_literal
operator_symbol

%type <id_def>
enumeration_id_def
subprogram_id_def
procedure_id_def
alias_id_def
physical_unit_id_def
record_element_id_def
library_id_def
interface_id_def
subtype_id_def
type_id_def
generic_type_id_def
constant_id_def
signal_id_def
variable_id_def
file_id_def
component_id_def
attribute_id_def
group_template_id_def
group_id_def
context_id_def
entity_id_def
architecture_id_def
configuration_id_def
package_id_def
package_body_id_def
label

/* VIPER #7686 : Set last_label null, when we are deleting last label*/
%destructor { if (($$) && ($$)->IsIncompleteType()) ($$)->Error("missing full type definition for %s", ($$)->Name()) ; vhdl_undeclare ($$) ; if (($$)==last_label) last_label = 0 ; delete ($$) ; }
subtype_id_def
type_id_def
enumeration_id_def

%type <id>
id
opt_id
attribute_designator
entity_name

%destructor { delete ($$) ; }
id
opt_id
attribute_designator
entity_name

%type <array>
context_item_list
generic_clause_wo_semi
generic_clause
opt_generic_clause
port_clause
opt_port_clause
choices
use_clause_list
selected_name_list
interface_list
enumeration_literal_list
physical_unit_decl_list
entity_class_entry_list
entity_designator_list
name_list
discrete_range_list
element_decl_list
type_mark_list
waveform_element_list
elsif_list
else_when_list
assoc_list
element_assoc_list
opt_generic_map_aspect
generic_map_aspect
opt_port_map_aspect
port_map_aspect
opt_package_header
opt_subprogram_header
interface_package_generic_map_aspect
record_resolution_element_list

entity_decl_part
opt_entity_statement_part
entity_statement_part
architecture_decl_part
architecture_statement_part
configuration_decl_part
configuration_item_list
package_decl_part
package_body_decl_part
subprogram_decl_part
subprogram_statement_part
block_decl_part
block_statement_part
process_decl_part
process_statement_part
protected_type_declarative_part
protected_type_body_declarative_part

opt_formal_parameter_list
index_constraint
conditional_waveforms
conditional_expressions
else_when_waveform_list
selected_waveforms
selected_expressions
waveform
opt_sensitivity_list
sensitivity_list
opt_sensitivity_clause
sequence_of_statements
else_statements
case_statement_alternatives
opt_type_mark_list

element_id_def_list
library_id_def_list
interface_id_def_list
signal_id_def_list
variable_id_def_list
constant_id_def_list
file_id_def_list

opt_comment
pre_comment

case_generate_alternatives
gen_block_statement_list
gen_block_decl_list
opt_accent_list
accent_list

%destructor { cleanup_iddef_array ($$) ; }
enumeration_literal_list
element_id_def_list
library_id_def_list
interface_id_def_list
signal_id_def_list
variable_id_def_list
constant_id_def_list
file_id_def_list

%destructor { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_statement_array ($$) ; }
architecture_statement_part
entity_statement_part
process_statement_part
subprogram_statement_part

%destructor { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_declnode_array ($$) ; }
architecture_decl_part
entity_decl_part

%type <type_def>
type_def
scalar_type_def
composite_type_def
access_type_def
file_type_def
enumeration_type_def
physical_type_def
array_type_def
record_type_def
protected_type_def
protected_type_def_body

%type <signature>
signature
opt_signature

%type <element_decl>
element_decl

%type <physical_unit_decl>
physical_unit_decl
base_unit_declaration

%type <configuration_item>
configuration_item

%type <block_configuration>
/* opt_block_configuration */
block_configuration
plain_block_configuration

%type <component_configuration>
component_configuration
plain_component_configuration

%type <binding_indication>
opt_binding_indication
binding_indication

/* Check out : class-unify specifications */
%type <specification>
subprogram_spec
entity_spec
component_spec
guarded_signal_spec
interface_subprogram_spec

%type <interface_decl>
interface_decl
plain_interface_decl
interface_object_decl
interface_type_decl
interface_subprogram_decl
interface_package_decl

%type <iter_scheme>
//generation_scheme
opt_iteration_scheme
case_scheme
else_scheme
elsif_scheme
if_scheme
for_scheme

%destructor { delete ($$) ; }
opt_iteration_scheme
case_scheme
else_scheme
elsif_scheme
if_scheme
for_scheme

%type <options>
options

%type <delay_mechanism>
delay_mechanism
opt_delay_mechanism

%type <block_generics>
block_generics

%type <block_ports>
block_ports

/* Check out : class-unify WHEN choices -> statements */
%type <case_statement_alternative>
case_statement_alternative

%type <file_open_info>
file_open_info
opt_file_open_info

%type <entity_class_entry>
entity_class_entry

%type <tree_node>
else_with_waveform
else_with_expr

%type <generate_stmt_body>
generate_statement_body
generate_statement_body_int /* VIPER #7494 : To reset process sequence number */
%type <ext_path_name>
external_path_name
package_pathname
absolute_pathname
relative_pathName

/* for comment preservation */
%type <library_unit>
library_unit
plain_library_unit
primary_unit
secondary_unit
entity_decl
configuration_decl
package_decl
architecture_body
package_body
package_instantiation_decl
context_decl

/* HDL operators should be here, did they mean for this to also include PSLs
 * usage of AND_OP OR_OP and NOT_OP?  If not where to put them?  Also this is
 * not how VHDL treats the precedence / associativity of and and or. */
%left VHDL_and VHDL_or VHDL_xor VHDL_xnor VHDL_nand VHDL_nor
%left VHDL_EQUAL VHDL_GTHAN VHDL_STHAN VHDL_NEQUAL VHDL_GEQUAL VHDL_SEQUAL VHDL_matching_equal VHDL_matching_nequal VHDL_matching_gthan VHDL_matching_sthan VHDL_matching_gequal VHDL_matching_sequal

%left VHDL_PLUS VHDL_MINUS VHDL_AMPERSAND
%left VHDL_STAR VHDL_SLASH VHDL_mod VHDL_rem
%nonassoc VHDL_not

// ambiguities occur mostly for statements that do not end with a specific token, and are in a list with itself.
%nonassoc VHDL_NO_COMMENT  // force comments to attach to inner statement
%nonassoc VHDL_OPT_COMMENT

%nonassoc VHDL_NO_OPAREN
%nonassoc VHDL_OPAREN

%%

/******************************* Source Text **********************/
/* NOTE : we accept empty files. VHDL defines that at least
 * one design unit should be there, but that gives too many
 * problems in actual usage (e.g. pragma use).
*/

source_text :
        | source_text design_unit
        | VHDL_BEGIN_EXPR expression
          {
              // Expression Parsing special token to resolve conflicts
              vhdl_file::SetParsedExpr($2) ;
          }
        | error
        ;

design_unit : context_clause
        //{
          /* remember the context_clause, we need it when we create the library unit tree */
            //create_context_clause() ;
            // VIPER #6316: Add 'context_item' when it is not null
            //if ($1) context_clause->InsertLast($1) ;
        //}
          // VIPER #6341 : Modified context clause creation. Implicit library clauses
          // will be created before starting of library unit parsing
          library_unit
        ;

/******************************* Context Clause **********************/

context_clause : { create_context_clause() ; }
        | context_clause context_item
          { if (context_clause && $2) context_clause->InsertLast($2) ; }
        ;

context_item : pre_comment plain_context_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

context_item_list : { $$ = 0 ; }
        | context_item_list context_item
          // VIPER #6316: Add 'context_item' when it is not null
          { $$ = ($1) ? ($1) : (($2) ? new Array():0) ; if ($$ && $2) ($$)->InsertLast($2) ; }
        ;

plain_context_item : library_clause
        | use_clause
         // VIPER #6341 : Parse only context reference within context_clause rule
         // Context reference :
        | VHDL_context selected_name_list VHDL_SEMI
          {
              /* VHDL 2008 LRM section 13.4 */
              $$ = new VhdlContextReference($2) ;
          }
        ;

library_clause : VHDL_library library_id_def_list VHDL_SEMI
         { $$ = new VhdlLibraryClause($2, 0 /*implicitly declared*/) ; }
        ;

use_clause : VHDL_use selected_name_list VHDL_SEMI
         { $$ = new VhdlUseClause($2, 0 /*implicitly declared*/) ; }
        ;

/******************************* Units **********************/

library_unit :  pre_comment plain_library_unit opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_library_unit : primary_unit
        | secondary_unit
        ;

primary_unit : entity_decl
        | configuration_decl
        | package_decl
        | package_instantiation_decl
        | context_decl
        ;

secondary_unit : architecture_body
        | package_body
        ;

/******************************* Context **********************/

context_decl : VHDL_context context_id_def VHDL_is
          {
              /* TODO: This is a DesignUnit. Need to rearrange the rules */
              /* VHDL 2008 LRM section 13.3 */
              if (context_clause && context_clause->Size() > 1/*anything apart from implicit*/) $2->Error("non empty context clause cannot precede a context declaration") ; // Viper 6757 : LRM 1076-2008 Section : 13.1

              cleanup_context_clause() ; // ignore preceding use clauses, if any
              // VIPER #6341 : Scope of context decl is now created earlier.
              // So we have already created library id 'std' and 'work' there.
              // Undeclare those first and then set owner
              VhdlTreeNode::_present_scope->ResetThisScope() ;
              // Create cross-link between the scope and the id that owns it.
              // Could not do this during construction time.
              VhdlTreeNode::_present_scope->SetOwner($2) ;

              //VhdlScope::Push($2) ;
              /* coverity[resource_leak] */
              context_clause = new Array() ; // not so clean
              // Declare the identifier (this makes it visible) :
              vhdl_declare($2) ;
          }
          context_item_list
          VHDL_end opt_VHDL_context opt_id VHDL_SEMI
          {
              VhdlContextDecl *context_decl = new VhdlContextDecl($5, $2, VhdlScope::Pop()) ;
              cleanup_context_clause() ; // not so clean
              context_decl->ClosingLabel($8) ;

              /* Add the unit to the library */
              VhdlLibrary *work = vhdl_file::GetWorkLib() ;

              $$ = (work->AddPrimUnit(context_decl)) ? context_decl : 0 ;
          }
          // VIPER #6341 : Moved to plain_context_item
        //| VHDL_context selected_name_list VHDL_SEMI
          //{
              /* VHDL 2008 LRM section 13.4 */
              //$$ = new VhdlContextReference($2) ;
          //}
        ;

context_id_def :
          VHDL_ID
          { $$ = new VhdlContextId($1) ; }
        ;

opt_VHDL_context : { $$=0; }
        | VHDL_context { $$=VHDL_context ; }
        ;

/******************************* Entity **********************/

entity_decl : VHDL_entity entity_id_def VHDL_is
        {
            inside_design_unit++ ;
            //create_context_clause() ;

            // Create cross-link between the scope and the id that owns it.
            // Could not do this during construction time.
            VhdlTreeNode::_present_scope->SetOwner($2) ;

            /* Include std.standard.all ; with a use-clause */
            VhdlName *n = new VhdlSelectedName(
                        new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                        new VhdlIdRef(Strings::save("standard"))),
                        new VhdlAll()) ;
            Array *use_clauses = new Array() ;
            use_clauses->InsertLast(n) ;
            VERIFIC_ASSERT(context_clause) ; // By design
            (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;

            ($2)->Info("analyzing entity %s",($2)->Name()) ;

            // Declare the identifier (this makes it visible) :
            vhdl_declare($2) ;
            process_sequence_num = 0 ; // VIPER #7494: Reset process sequence number
        }
            opt_generic_clause
            opt_port_clause
            entity_decl_part
            opt_entity_statement_part
        VHDL_end opt_VHDL_entity opt_id VHDL_SEMI
        {
            VhdlEntityDecl *unit = new VhdlEntityDecl(context_clause,$2,$5,$6,$7,$8,VhdlScope::Pop()) ;
            context_clause = 0 ;
            inside_design_unit-- ;
            unit->ClosingLabel($11) ;

            /* Add the unit to the library */
            VhdlLibrary *work = vhdl_file::GetWorkLib() ;

            $$ = (work->AddPrimUnit(unit)) ? unit : 0 ;

            // Viper 7168: Do not proceed parsing this file if an error occurred
            // and the primary unit was not inserted in the library
            // Proceeding would only cause avalange of errors in the architecture(s)
            if (!($$) && Message::ErrorCount()) { // Do not abort if there was no error reported
                YYABORT ; // stop the parser
            }
        }
        ;

generic_clause_wo_semi : VHDL_generic VHDL_OPAREN { in_generic_clause = 1 ; } interface_list VHDL_CPAREN
        {
            // Set the interface list objects to be 'generics' :
            Array *ids ;
            VhdlInterfaceDecl *decl ;
            VhdlIdDef *id ;
            unsigned i, j ;
            FOREACH_ARRAY_ITEM($4, i, decl) {
                ids = decl->GetIds() ; // collect all identifiers in this list
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    id->SetObjectKind(VHDL_generic) ;
                }
            }
            in_generic_clause = 0 ;
            // Return the interface list itself..
            $$ = $4 ;
        }
        ;

generic_clause : generic_clause_wo_semi VHDL_SEMI
        {
            $$ = $1 ;
        }
        ;

port_clause : VHDL_port VHDL_OPAREN interface_list VHDL_CPAREN VHDL_SEMI
        {
            // Set the interface list objects to be 'ports' :
            Array *ids ;
            VhdlInterfaceDecl *decl ;
            VhdlIdDef *id ;
            unsigned i, j ;
            FOREACH_ARRAY_ITEM($3, i, decl) {
                if (decl->IsInterfaceTypeDecl() || decl->IsInterfaceSubprogDecl() || decl->IsInterfacePackageDecl()) {
                    // Viper 7124 : Vhdl 2008, type, subprogram, package cannot  be used as port
                    decl->Error("%s cannot be used as a port",decl->IsInterfaceTypeDecl() ? "type" : decl->IsInterfaceSubprogDecl() ? "subprogram" : "package" ) ;
                }
                ids = decl->GetIds() ; // collect all identifiers in this list
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    id->SetObjectKind(VHDL_port) ;
                }
            }
            // Return the interface list itself..
            $$ = $3 ;
        }
        ;

entity_decl_part :   { $$ = 0 ; }
        | entity_decl_part entity_decl_item
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        | error  { $$ = 0 ; }
        ;

entity_decl_item : pre_comment plain_entity_decl_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
            if ($2 && $2->IsPackageBody()) $$ = 0 ; // Package body will not be added in decl list
        }
        ;

plain_entity_decl_item :
          subprogram_decl
        | subprogram_body
        | type_decl
        | subtype_decl
        | constant_decl
        | signal_decl
        | variable_decl
        | file_decl
        | alias_decl
        | attribute_decl
        | attribute_spec
        | disconnection_spec
        | use_clause
        | group_template_decl
        | group_decl
        | package_instantiation_decl { $$ = $1 ; }// VHDL-2008
        | package_decl { $$ = $1 ; } // VHDL-2008 : package in decl part
        | package_body { $$ = $1 ; } // VHDL-2008 : package body in decl part
        ;

opt_entity_statement_part : { $$ = 0 ; }
        | VHDL_begin entity_statement_part
          { $$ = $2 ; }
        ;

entity_statement_part :
          { $$ = new Array() ; }
        | entity_statement_part entity_statement
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; }
        ;

entity_statement : concurrent_statement
         {  CreateAndAddImplicitLabel($1) ;}
         /* FIX ME : This is overkill. Check Restrictions. */
        ;

/******************************* Architecture **********************/

architecture_body : VHDL_architecture architecture_id_def VHDL_of entity_name VHDL_is
        {
            inside_design_unit++ ;
            //create_context_clause() ;

            // Create cross-link between the scope and the id that owns it.
            // Could not do this during construction time.
            VhdlTreeNode::_present_scope->SetOwner($2) ;

            VhdlLibrary *work = vhdl_file::GetWorkLib() ;
            // VIPER #7361: Find unit without modifying the name
            VhdlPrimaryUnit *entity = work->GetPrimUnit(($4)->Name(), 1, 0 /* do not modify name*/) ;

            if (!entity) {
                ($4)->Error("entity %s is not yet compiled", ($4)->Name()) ;
            }

            ($2)->Info("analyzing architecture %s",($2)->Name()) ;

            if (entity) {
                if (!entity->Id()->IsEntity()) {
                    ($4)->Error("%s is not an entity", ($4)->Name()) ;
                } else {
                    // Store entity in the architecture id
                    ($2)->DeclareArchitectureId(entity->Id()) ;
                    // And link the declarative region.
                    VhdlTreeNode::_present_scope->AddDeclRegion(entity->LocalScope()) ;
                }
            }

            // Declare the identifier (this makes it visible) :
            vhdl_declare($2) ;

            if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                // VIPER #7494: Process sequence number for architecture's concurent
                // statements should be one greater than the last process sequence
                // number used in corresponding entity.
                // So determine the process sequence number of architecture
                process_sequence_num = 0 ;
                VhdlIdDef *existing_implicit_label = 0 ;
                VhdlScope *entity_scope = entity ? entity->LocalScope(): 0 ;
                if (entity_scope) {
                    // Get the last used process sequence number in entity
                    int count = -1 ;
                    do {
                        count++ ;
                        char name[20] ;
                        sprintf(name, "_p%d", count) ;
                        existing_implicit_label = entity_scope->FindSingleObjectLocal(name) ;
                    } while (existing_implicit_label) ;
                    process_sequence_num = count ; // sequence number of architecture's stmts
                }
            }
        }
                architecture_decl_part
        VHDL_begin
                architecture_statement_part
        VHDL_end opt_VHDL_architecture opt_id VHDL_SEMI
        {
            VhdlArchitectureBody *unit = new VhdlArchitectureBody(context_clause,$2,$4,$7,$9,VhdlScope::Pop()) ;
            context_clause = 0 ;
            inside_design_unit-- ;
            process_sequence_num = 0 ; // VIPER #7494: Reset process sequence number
            unit->ClosingLabel($12) ;

            VhdlLibrary *work = vhdl_file::GetWorkLib() ;
            // VIPER #7361: Find unit without modifying the name
            VhdlPrimaryUnit *entity = work->GetPrimUnit(($4)->Name(), 1, 0 /* do not modify name*/) ;
            // Add the unit to the entity
            if (entity) {
                // If addition fails, the unit is now removed. So invalidate the return value.
                unit = (entity->AddSecondaryUnit(unit)) ? unit : 0 ;
                $$ = unit ;
            } else {
                // If not there, this unit is unbound.
                // That's dangerous, since it's hanging in space, without an owner.
                // Delete it in that case.
                delete unit ;
                $$ = 0 ;
            }
        }
        ;

architecture_decl_part :  { $$ = 0 ; }
        | architecture_decl_part block_decl_item
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        | error { $$ = 0 ; }
        ;

block_decl_item : pre_comment plain_block_decl_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
            if ($2 && $2->IsPackageBody()) $$ = 0 ; // Package body will not be added in decl list
        }
        ;

plain_block_decl_item :
          subprogram_decl
        | subprogram_body
        | type_decl
        | subtype_decl
        | constant_decl
        | signal_decl
        | variable_decl
        | file_decl
        | alias_decl
        | component_decl
        | attribute_decl
        | attribute_spec
        | configuration_spec
        | disconnection_spec
        | use_clause
        | group_template_decl
        | group_decl
        | package_instantiation_decl { $$ = $1 ; }// VHDL-2008
        | package_decl { $$ = $1 ; } // VHDL-2008 : package in decl part
        | package_body { $$ = $1 ; } // VHDL-2008 : package body in decl part
        ;

architecture_statement_part : { $$ = 0 ; }
        | architecture_statement_part concurrent_statement
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2) CreateAndAddImplicitLabel($2) ; }
        | error { $$ = 0 ; }
        ;

/******************************* Configuration **********************/

configuration_decl : VHDL_configuration configuration_id_def VHDL_of
        {
            inside_design_unit++ ;
            //create_context_clause() ;

                // Create cross-link between the scope and the id that owns it.
                // Could not do this during construction time.
                VhdlTreeNode::_present_scope->SetOwner($2) ;

                    /* Include std.standard.all ; with a use-clause */
                VhdlName *n = new VhdlSelectedName(
                            new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                            new VhdlIdRef(Strings::save("standard"))),
                            new VhdlAll()) ;
                Array *use_clauses = new Array() ;
                use_clauses->InsertLast(n) ;
                VERIFIC_ASSERT(context_clause) ; // By design
                (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;

                // Declare the identifier (this makes it visible) :
                vhdl_declare($2) ;
        }
        entity_name VHDL_is
        {
                ($2)->Info("analyzing configuration %s", ($2)->Name()) ;

                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                // VIPER #7361: Vhdl does not allow name modification for searching
                VhdlPrimaryUnit *entity = work->GetPrimUnit(($5)->Name(), 1, 0 /* do not modify name*/) ;
                if (!entity) {
                    ($5)->Error("entity %s is not yet compiled", ($5)->Name()) ;
                } else if (!entity->Id()->IsEntity()) {
                    ($5)->Error("%s is not an entity", ($5)->Name()) ;
                } else {
                    // Store entity in the configuration id
                    ($2)->DeclareConfigurationId(entity->Id()) ;

                    // Register the dependency of the configuration on the entity :
                    VhdlTreeNode::_present_scope->Using(entity->LocalScope()) ;

                    // Block spec in block configuration will find the architecture
                    // in the entity by scope selection.
                }
        }
                configuration_decl_part
                block_configuration
        VHDL_end opt_VHDL_configuration opt_id VHDL_SEMI
        {
                VhdlConfigurationDecl *unit = new VhdlConfigurationDecl(context_clause,$2,$5,$8,$9,VhdlScope::Pop()) ;
                context_clause = 0 ;
                inside_design_unit-- ;
                unit->ClosingLabel($12) ;

                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                $$ = (work->AddPrimUnit(unit)) ? unit : 0 ;

                // Viper 7168: Do not proceed parsing this file if an error occurred
                // and the primary unit was not inserted in the library
                // Proceeding would only cause avalange of errors in later units
                if (!($$) && Message::ErrorCount()) { // Do not abort if there was no error reported
                     YYABORT ; // stop the parser
                }
        }
        ;

configuration_decl_part :  { $$ = 0 ; }
        | configuration_decl_part configuration_decl_item
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error { $$ = 0 ; }
        ;

configuration_decl_item : pre_comment plain_configuration_decl_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_configuration_decl_item : use_clause
        | attribute_spec
        | group_decl
        ;

block_configuration : pre_comment plain_block_configuration opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_block_configuration : VHDL_for block_spec
        {
          // LRM 1.3.1 : external/internal block configuration :
          // Need to find the block_spec (architecture or block) first,
          // and extend the present scope with its scope
          VhdlIdDef *owner = ($2) ? ($2)->BlockSpec() : 0 ;

          VhdlScope::Push() ;

          // LRM 10.4 : declarations of a configured block are visible in the block configuration.
          if (owner) VhdlTreeNode::_present_scope->AddDeclRegion(owner->LocalScope()) ;
        }
                use_clause_list configuration_item_list
        VHDL_end VHDL_for VHDL_SEMI
        {
          $$ = new VhdlBlockConfiguration($2,$4,$5,VhdlScope::Pop()) ;
        }
        ;

block_spec : name
        /* index_spec is covered as indexed name */
          { $$ = $1 ; }
    ;

use_clause_list : { $$ = 0 ; }
        | use_clause_list commented_use_clause
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        ;

commented_use_clause : pre_comment use_clause opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

configuration_item_list :  { $$ = 0 ; }
        | configuration_item_list configuration_item
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error { $$ = 0 ; }
        ;

configuration_item : block_configuration
           { $$ = $1 ; }
        | component_configuration
           { $$ = $1 ; }
        ;

component_configuration : pre_comment plain_component_configuration opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_component_configuration :
        VHDL_for component_spec  opt_binding_indication
        {
            // Component configuration WITHOUT an inner block_configuration.

            // Create scope for the component configuration (LRM 10.1 tells us one is needed, although no declarations can be in here).
            VhdlScope::Push() ;

            // Resolve the component spec with its binding indication.
            if ($2) (void)($2)->ResolveSpecs($3,1/*incremental*/) ;
            // No need to make secondary units (of the primary binding) visible, since there is no block-config after this.
        }
        VHDL_end VHDL_for VHDL_SEMI
        {
            // plain component configuration without inner block-config.
            $$ = new VhdlComponentConfiguration($2,$3,0,VhdlScope::Pop()) ;
        }
        | VHDL_for component_spec  opt_binding_indication
        {
            // Component configuration with an inner block_configuration.

            // Create scope for the component configuration (LRM 10.1 tells us one is needed, although no declarations can be in here).
            VhdlScope::Push() ;

            // Resolve the component specification using the (optional) binding indication :
            // We need to do this before we decent into the block configuration, since we
            // need the entity aspect of the primary binding of the component spec's labels
            // so we can make the entity scope visible to the block_configuration below.
            // If we don't do that here, any block configuration will error out (block label (architecture) not found)
            // This solves VIPER 3032/3042.
            // Issue 1957 also needs the full binding (the binding with the entity aspect).
            VhdlIdDef *entity_id = ($2) ? ($2)->ResolveSpecs($3,1/*incremental*/) : 0 ;

            // LRM 1.3.1 : if component is fully bound, we need to make the
            // architecture id (the binding unit) visible for later block config's
            // Get the primary unit from the binding indication around this block,
            // and make all its architectures visible.
            // CHECK ME : Maybe we should only make one architecture visible ? Check 1.3.1.

            // If this is an entity, make its architecture names visible here,
            // so that the block configuration can find them :
            if (entity_id && entity_id->IsEntity()) {
                VhdlPrimaryUnit *entity = entity_id->GetPrimaryUnit() ;

                // Register the dependency of the configuration on the entity :
                VhdlTreeNode::_present_scope->Using(entity->LocalScope()) ;

                // Make the architecture id's of this entity visible in the present scope.
                VhdlSecondaryUnit *arch ;
                Set archs(POINTER_HASH) ;
                MapIter mi ;
                FOREACH_VHDL_SECONDARY_UNIT(entity, mi, arch) {
                    (void) archs.Insert(arch->Id()) ;
                    // Register the dependency of the configuration on the architectures.
                    // CHECK ME : Do we REALLY need to register these dependencies ?
                    // We will only use the architecture name mentioned in the block-config....
                    VhdlTreeNode::_present_scope->Using(arch->LocalScope()) ;
                }
                // Now add these guys as 'use' items, so they become visible in deeper block-configs :
                // So we do something similar to "use lib.prim_name.sec_name"..
                VhdlTreeNode::_present_scope->AddUseClause(archs) ;
            }
        }
            block_configuration
        VHDL_end VHDL_for VHDL_SEMI
        {
           $$ = new VhdlComponentConfiguration($2,$3,$5,VhdlScope::Pop()) ;
        }
        ;

opt_binding_indication :  { $$ = 0 ; }
        | binding_indication VHDL_SEMI
          { $$ = $1 ; }
        ;

/* opt_block_configuration : { $$ = 0 ; }
        | block_configuration
        ;
*/

/******************************* Package Declaration **********************/

package_decl : VHDL_package package_id_def VHDL_is
        {
            if (inside_design_unit) {
                // Package in other library unit
                VhdlScope::Push() ;
            } else {
                // Package as library unit
                //create_context_clause() ;
            }

                // Create cross-link between the scope and the id that owns it.
                // Could not do this during construction time.
                VhdlTreeNode::_present_scope->SetOwner($2) ;

                ($2)->Info("analyzing package %s", ($2)->Name()) ;

                // If this is package standard in library std itself,
                // then we should not include std.standard.
                if (!inside_design_unit && (!Strings::compare(($2)->Name(), "standard") ||
                    !Strings::compare((vhdl_file::GetWorkLib())->Name(), "std"))) {
                    /* Include std.standard.all ; with a use-clause */
                    VhdlName *n = new VhdlSelectedName(
                                new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                                new VhdlIdRef(Strings::save("standard"))),
                                new VhdlAll()) ;
                    Array *use_clauses = new Array() ;
                    use_clauses->InsertLast(n) ;
                    VERIFIC_ASSERT(context_clause) ; // By design
                    (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;
                }
                // Declare the identifier (this makes it visible) :
                if (inside_design_unit) {
                    // This package declaration is in a declarative region of
                    // package/entity/architecture etc. So declare the identifier
                    // in upper scope
                    VhdlScope *old_scope = VhdlTreeNode::_present_scope ;
                    VhdlTreeNode::_present_scope = old_scope->Upper() ;
                    // Viper 6858 : For nested design unit the usual scoping and visibility rules apply
                    VhdlIdDef *exist =  VhdlTreeNode::_present_scope->FindSingleObjectLocal($2->Name()) ;
                    VhdlIdDef *scope_owner = VhdlTreeNode::_present_scope->GetOwner() ;
                    // Do not error out for same name package inside a package.
                    // This is checked by $2 ne scope_owner
                    if (exist && exist!=scope_owner && $2!=scope_owner) {
                        if ((exist->IsSubprogram() || exist->IsEnumerationLiteral()) &&
                            ($2->IsSubprogram() || $2->IsEnumerationLiteral())) {
                        } else if ($2->IsLibrary()) {
                        } else if (exist->IsLibrary()) {
                            // Ignore the existing library identifier. Other declarations always override a library.
                        } else if (exist->IsDesignUnit() || $2->IsDesignUnit()) {
                            $2->Error("%s is already declared in this region", exist->Name()) ;
                        }
                    }
                    vhdl_declare($2) ;
                    VhdlTreeNode::_present_scope = old_scope ;
                } else {
                    vhdl_declare($2) ;
                }
                inside_design_unit++ ;
        }
                opt_package_header
                package_decl_part
        VHDL_end opt_VHDL_package opt_id VHDL_SEMI
        {
                Array *generic_clause = 0 ;
                Array *generic_map_aspect = 0 ;
                if ($5) {
                    if (($5)->Size() > 0) generic_clause = (Array *)($5)->At(0) ;
                    if (($5)->Size() > 1) generic_map_aspect = (Array *)($5)->At(1) ;
                    //($2)->Warning("VHDL 1076-2008 construct not yet supported") ;
                    // VIPER #6258 : Produce error if generic list in package appears in
                    // any dialect other than 2008
                    if (!VhdlNode::IsVhdl2008()) {
                        if ($2) ($2)->Error("this construct is only supported in VHDL 1076-2008") ;
                    }
                    delete ($5) ;
                }
                inside_design_unit-- ;
                VhdlPackageDecl *unit = new VhdlPackageDecl(inside_design_unit ? 0: context_clause, $2, generic_clause, generic_map_aspect, $6, VhdlScope::Pop()) ;
                if (!inside_design_unit) context_clause = 0 ;
                unit->ClosingLabel($9) ;

                if (!inside_design_unit) {
                    // Package is declared as library unit, add it to library
                    VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                    $$ = (work->AddPrimUnit(unit)) ? unit : 0 ;
                    // Viper 7168: Do not proceed parsing this file if an error occurred
                    // and the package decl was not inserted in the library
                    // Proceeding would only cause avalange of errors in the package body
                    if (!($$) && Message::ErrorCount()) { // Do not abort if there was no error reported
                        YYABORT ; // stop the parser
                    }
                } else {
                    $$ = unit ;
                    // VIPER #6258 : Produce error if nested package appears in
                    // any dialect other than 2008
                    if (!VhdlNode::IsVhdl2008()) {
                        ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
                    }
                }
        }
        ;

opt_package_header :  { $$ = 0 ; }
        | generic_clause
          {
              $$ = new Array() ; if ($1) ($$)->InsertLast($1) ;
          }
        | generic_clause generic_map_aspect VHDL_SEMI
          {
              $$ = new Array() ; if ($1) ($$)->InsertLast($1) ;
              if ($2) ($$)->InsertLast($2) ;
          }
        ;

opt_subprogram_header :  { $$ = 0 ; }
        | generic_clause_wo_semi opt_generic_map_aspect
          {
              $$ = new Array() ;
              if ($1) ($$)->InsertLast($1) ;
              if ($2) ($$)->InsertLast($2) ;
          }
        ;

package_decl_part :  { $$ = 0 ; }
        | package_decl_part package_decl_item
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        | error { $$ = 0 ; }
        ;

package_decl_item : pre_comment plain_package_decl_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_package_decl_item :
          subprogram_decl
        | type_decl
        | subtype_decl
        | constant_decl
        | signal_decl
        | variable_decl
        | file_decl
        | alias_decl
        | component_decl
        | attribute_decl
        | attribute_spec
        | disconnection_spec
        | use_clause
        | group_template_decl
        | group_decl
        | subprogram_body // if subprogram_body error, allow subprogram_instantiation_decl
        | package_instantiation_decl  { $$ = $1 ; }// VHDL-2008
        | package_decl { $$ = $1 ; } // VHDL-2008 : package in decl part
        ;

/******************************* Package Body **********************/

package_body : VHDL_package VHDL_body package_body_id_def VHDL_is
        {
            if (inside_design_unit) {
                VhdlScope::Push() ;
            } else {
                //create_context_clause() ;
            }

            // Create cross-link between the scope and the id that owns it.
            // Could not do this during construction time.
            VhdlTreeNode::_present_scope->SetOwner($3) ;

            ($3)->Info("analyzing package body %s",($3)->Name()) ;

            // Find the package declaration :
            // VIPER #6582 : If this declaration is inside any design unit, find the package
            // declaration first in _present_scope
            VhdlPrimaryUnit *entity = 0 ;
            if (inside_design_unit) {
                // Package declaration may be in the declarative part of other construct
                // Find there
                VhdlScope *container = VhdlTreeNode::_present_scope->Upper() ;
                VhdlIdDef *id = container ? container->FindSingleObjectLocal(($3)->Name()): 0 ;
                entity = id ? id->GetPrimaryUnit(): 0 ;
            } else {
                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                // VIPER #7361: Find package in library without modifying the name
                // 'GetPrimUnit' also searches by unescaping the name if 3 rd argument
                // is 1 (for mixed language design)
                entity = work->GetPrimUnit(($3)->Name(), 1, 0/* do not modify name for searching*/) ;
            }
            if (!entity) {
                    ($3)->Error("package declaration %s is not yet compiled", ($3)->Name()) ;
            } else if (!entity->Id()->IsPackage()) {
                    ($3)->Error("%s is not a package", ($3)->Name()) ;
            } else {
                    VhdlTreeNode::_present_scope->AddDeclRegion(entity->LocalScope()) ;
            }

            // VIPER 3080 : Delete the existing package body (if there), or else we cannot link a new body to an unchanged header.
            VhdlSecondaryUnit *exist = (entity) ? entity->GetSecondaryUnit(($3)->Name()) : 0 ;
            // delete it if it exists. This will uncouple the body-header function links.
            delete exist ;

            // Declare the identifier (this makes it visible) :
            // Never do this for package body ? Only the package decl is visible
            // vhdl_declare($3) ;
            inside_design_unit++ ;
        }
                package_body_decl_part
        VHDL_end opt_VHDL_package_body opt_id VHDL_SEMI
        {
            inside_design_unit-- ;
            VhdlPackageBody *unit = new VhdlPackageBody(inside_design_unit ? 0: context_clause,$3,$6,VhdlScope::Pop()) ;
            if (!inside_design_unit) context_clause = 0 ;
            unit->ClosingLabel($9) ;

            // VIPER #6582 : If this declaration is inside any design unit, find the package
            // declaration first in _present_scope
            VhdlPrimaryUnit *entity = 0 ;
            if (inside_design_unit){
                // Package declaration may be in the declarative part of other construct
                // Find there
                VhdlScope *container = VhdlTreeNode::_present_scope ;
                VhdlIdDef *id = container ? container->FindSingleObjectLocal(($3)->Name()): 0 ;
                entity = id ? id->GetPrimaryUnit(): 0 ;
            } else {
                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                // VIPER #7361: Find package in library without modifying the name
                // 'GetPrimUnit' also searches by unescaping the name if 3 rd argument
                // is 1 (for mixed language design)
                entity = work->GetPrimUnit(($3)->Name(), 1, 0/* do not modify name for searching*/) ;
            }
            // Add the unit to the package header (entity)
            if (entity) {
                // If addition failed, the unit is now removed. So invalidate the return value.
                $$ = (entity->AddSecondaryUnit(unit)) ? unit : 0 ;
            } else {
                // If not there, this unit is unbound.
                // That's dangerous, since it's hanging in space, without an owner.
                // Delete it in that case.
                delete unit ;
                $$ = 0 ;
            }
        }
        ;

package_body_decl_part :  { $$ = 0 ; }
        | package_body_decl_part package_body_decl_item
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        | error { $$ = 0 ; }
        ;

package_body_decl_item : pre_comment plain_package_body_decl_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
            if ($2 && $2->IsPackageBody()) $$ = 0 ; // Package body will not be added in decl list
        }
        ;

plain_package_body_decl_item :
          subprogram_decl
        | subprogram_body
        | type_decl
        | subtype_decl
        | constant_decl
        // VIPER #7149: Remove signal declaration from package body decl item
        // and add attribute decl and spec in 2008 mode only
        //| signal_decl
        | attribute_decl
          {
             if (!VhdlNode::IsVhdl2008() && $1) {
                 ($1)->Error("this construct is only supported in VHDL 1076-2008") ;
             }
             $$ = $1 ;
          }
        | attribute_spec
          {
             if (!VhdlNode::IsVhdl2008() && $1) {
                 ($1)->Error("this construct is only supported in VHDL 1076-2008") ;
             }
             $$ = $1 ;
          }
        | variable_decl
        | file_decl
        | alias_decl
        | use_clause
        | group_template_decl
        | group_decl
        | package_instantiation_decl { $$ = $1 ; }// VHDL-2008
        | package_decl { $$ = $1 ; } // VHDL-2008 : package in decl part
        | package_body { $$ = $1 ; } // VHDL-2008 : package body in decl part
        ;

package_instantiation_decl : VHDL_package package_id_def VHDL_is VHDL_new
         {
            if (inside_design_unit) {
                VhdlScope::Push() ;
            } else {
                //create_context_clause() ;
            }
            // Create cross-link between the scope and the id that owns it.
            VhdlTreeNode::_present_scope->SetOwner($2) ;

            if (!inside_design_unit) {
                /* Include std.standard.all ; with a use-clause */
                VhdlName *n = new VhdlSelectedName(
                        new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                        new VhdlIdRef(Strings::save("standard"))),
                        new VhdlAll()) ;
                Array *use_clauses = new Array() ;
                use_clauses->InsertLast(n) ;
                VERIFIC_ASSERT(context_clause) ; // By design
                (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;
            }
            // Declare the identifier (this makes it visible) :
            if (!inside_design_unit) {
                vhdl_declare($2) ;
            } else {
                // This package declaration is in a declarative region of
                // package/entity/architecture etc. So declare the identifier
                // in upper scope
                VhdlScope *old_scope = VhdlTreeNode::_present_scope ;
                VhdlTreeNode::_present_scope = old_scope->Upper() ;
                vhdl_declare($2) ;
                VhdlTreeNode::_present_scope = old_scope ;
            }
          }
          uninstantiated_package_name opt_generic_map_aspect VHDL_SEMI
          {
              VhdlPackageInstantiationDecl *decl ;

              decl = new VhdlPackageInstantiationDecl(inside_design_unit ? 0: context_clause, $2, $6, $7, VhdlScope::Pop()) ;
              if (!inside_design_unit) context_clause = 0 ;
              if (!inside_design_unit) {
                  // Package is declared as library unit, add it to library
                  VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                  $$ = (work->AddPrimUnit(decl)) ? decl : 0 ;

                  // Viper 7168: Do not proceed parsing this file if an error occurred
                  // and the primary unit was not inserted in the library
                  // Proceeding would only cause avalange of errors in later units
                  if (!($$) && Message::ErrorCount()) { // Do not abort if there was no error reported
                       YYABORT ; // stop the parser
                  }
               } else {
                  $$ = decl ;
               }
          }
        ;

/******************************* SubPrograms *****************/

subprogram_spec : VHDL_procedure procedure_id_def { VhdlScope::Push($2) ; ($2)->SetAsProcedure() ; }
                opt_subprogram_header
                opt_parameter_keyword opt_formal_parameter_list
          {
              if ($5 && !$6) {
                  if ($2) $2->Error("parameter should be followed by its list") ;
              }
              Array *generic_clause = 0 ;
              Array *generic_map_aspect = 0 ;
              if ($4) { // VHDL-2008 LRM Section 4.2.1
                  if (($4)->Size() > 0) generic_clause = (Array *)($4)->At(0) ;
                  if (($4)->Size() > 1) generic_map_aspect = (Array *)($4)->At(1) ;
                  //($2)->Warning("VHDL 1076-2008 construct not yet supported") ;
                  // VIPER #6258 : Produce error if generic list in subprogram appears in
                  // any dialect other than 2008
                  if (!VhdlNode::IsVhdl2008()) {
                      if ($2) ($2)->Error("this construct is only supported in VHDL 1076-2008") ;
                  }
                  delete ($4) ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM($6, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind($5 ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }
              $$ = new VhdlProcedureSpec($2,generic_clause,generic_map_aspect,$6,VhdlTreeNode::_present_scope, $5==VHDL_parameter) ;
          }
        | opt_pure_impure VHDL_function subprogram_id_def { VhdlScope::Push($3) ; }
                opt_subprogram_header
                opt_parameter_keyword opt_formal_parameter_list opt_return //VHDL_return type_mark
          {
              if ($6 && !$7) {
                  if ($3) $3->Error("parameter should be followed by its list") ;
              }
              Array *generic_clause = 0 ;
              Array *generic_map_aspect = 0 ;
              if ($5) { // VHDL-2008 LRM Section 4.2.1
                  if (($5)->Size() > 0) generic_clause = (Array *)($5)->At(0) ;
                  if (($5)->Size() > 1) generic_map_aspect = (Array *)($5)->At(1) ;
                  //($3)->Warning("VHDL 1076-2008 construct not yet supported") ;
                  // VIPER #6258 : Produce error if generic list in subprogram appears in
                  // any dialect other than 2008
                  if (!VhdlNode::IsVhdl2008()) {
                      if ($3) ($3)->Error("this construct is only supported in VHDL 1076-2008") ;
                  }
                  delete ($5) ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM($7, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind($6 ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }

              $$ = new VhdlFunctionSpec($1,$3,generic_clause,generic_map_aspect,$7,$8,VhdlTreeNode::_present_scope, $6==VHDL_parameter) ;
          }
        ;

opt_return : { $$ = 0 ; }
        | VHDL_return type_mark { $$ = $2 ; }
        ;

subprogram_decl : subprogram_spec VHDL_SEMI
        {
            // Scope is already stored in the subprogram spec. So just pop.
            (void) VhdlScope::Pop() ;
            // Create the tree after we pop scope
            $$ = new VhdlSubprogramDecl($1) ;
            // VIPER #6664 : Produce error if function spec does not contain return type
            if ($1 && $1->IsFunction() && !$1->GetReturnType()) {
                $1->Error("function return type is not specified") ;
            }
        }
        ;

sequential_stmts_begin : VHDL_begin
         {
             loop_sequence_num = 0 ; // Reset loop sequence number (VIPER #7494)
         }
        ;

subprogram_body : subprogram_spec VHDL_is
                subprogram_decl_part
        //VHDL_begin
        sequential_stmts_begin // VIPER #7494 : To reset loop sequence number
                subprogram_statement_part
        VHDL_end opt_subprogram_kind opt_designator VHDL_SEMI
        {
            // Scope is already stored in the spec
            VhdlTreeNode::_present_scope = $1->LocalScope() ; // VIPER #1976
            (void) VhdlScope::Pop() ;
            // Create the tree after we pop scope
            $$ = new VhdlSubprogramBody($1,$3,$5) ;
            // Check that opt_subprogram_kind closing (if it exists) matches subprogram_spec type
            if ($7) { // VIPER #2017
                if ($1->IsProcedure() && ($7 != VHDL_procedure)) {
                    $$->Error("subprogram type at end of subprogram body does not match specification type '%s'", "procedure");
                } else if ($1->IsFunction() && ($7 != VHDL_function)) {
                    $$->Error("subprogram type at end of subprogram body does not match specification type '%s'", "function");
                }
            }
            // VIPER #6664 : Produce error if function spec does not contain return type
            if ($1 && $1->IsFunction() && !$1->GetReturnType()) {
                $1->Error("function return type is not specified") ;
            }
            ($$)->ClosingLabel($8) ;
        }
        // VIPER #5918 : VHDL-2008 : Subprogram instantiation declaration
        | subprogram_spec VHDL_is VHDL_new
          uninstantiated_subprog_name opt_signature opt_generic_map_aspect VHDL_SEMI
        {
            (void) VhdlScope::Pop() ; $$ = new VhdlSubprogInstantiationDecl($1, ($5) ? new VhdlSignaturedName($4, $5): $4, $6) ;
            // VIPER #6258 : Produce error if subprogram instantiation declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
        ;

opt_formal_parameter_list : { $$ = 0 ; }
        | VHDL_OPAREN interface_list VHDL_CPAREN
        {
            // Set the interface list objects to be subprogram 'parameters' :
            Array *ids ;
            VhdlInterfaceDecl *decl ;
            VhdlIdDef *id ;
            unsigned i, j ;
            FOREACH_ARRAY_ITEM($2, i, decl) {
                ids = decl->GetIds() ; // collect all identifiers in this list
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    id->SetObjectKind(0) ; // indication that it is a 'parameter'..Viper 6653
                }
            }
            // Return the interface list itself..
            $$ = $2 ;
        }
        ;

opt_parameter_keyword : { $$ = 0 ;} // Viper 6653
        | VHDL_parameter { $$ = VHDL_parameter ;}
        ;

subprogram_decl_part :  { $$ = 0 ; }
        | subprogram_decl_part subprogram_decl_item
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        | error { $$ = 0 ; }
        ;

subprogram_decl_item: pre_comment plain_subprogram_decl_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
            if ($2 && $2->IsPackageBody()) $$ = 0 ; // Package body will not be added in decl list
        }
        ;

plain_subprogram_decl_item:
          subprogram_decl
        | subprogram_body
        | type_decl
        | subtype_decl
        | constant_decl
        | signal_decl
        | variable_decl
        | file_decl
        | alias_decl
        | attribute_decl
        | attribute_spec
        | use_clause
        | group_template_decl
        | group_decl
        | package_instantiation_decl { $$ = $1 ; }// VHDL-2008
        | package_decl { $$ = $1 ; } // VHDL-2008 : package in decl part
        | package_body { $$ = $1 ; } // VHDL-2008 : package body in decl part
        ;

subprogram_statement_part :  { $$ = 0 ; }
        | subprogram_statement_part sequential_statement
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error { $$ = 0 ; }
        ;

/******************************* Types *****************/

subtype_decl : VHDL_subtype subtype_id_def VHDL_is subtype_indication VHDL_SEMI
          { $$ = new VhdlSubtypeDecl($2,$4) ; }
        ;

type_decl : VHDL_type type_id_def
          { last_label = $2 ; }
            VHDL_is type_def VHDL_SEMI
          { // Actually, we use last_label here to have access to the record type id
            // when we create its scope. That's an OK use of last_label, even though its not a label.
            if (!last_label) last_label = ($5)->GetId() ;
            $$ = new VhdlFullTypeDecl(last_label,$5) ;
            last_label = 0 ;
          }
        | VHDL_type type_id_def VHDL_SEMI
          { $$ = new VhdlIncompleteTypeDecl($2) ;}
        ;

type_def : scalar_type_def
        | composite_type_def
        | access_type_def
        | file_type_def
        ;

scalar_type_def : enumeration_type_def
        | range_constraint { $$ = new VhdlScalarTypeDef($1) ; }
        | physical_type_def
        ;

composite_type_def : array_type_def
        | record_type_def
        | protected_type_def
        | protected_type_def_body
        ;

array_type_def : VHDL_array index_constraint VHDL_of subtype_indication
          { $$ = new VhdlArrayTypeDef($2,$4) ; }

          /* merged : index_constraint contains "type_mark range <>", so
           * array_type_def works for both unconstrained and constrained
           * array type definitions
          */
        ;

record_type_def : VHDL_record { VhdlScope::Push(last_label) ; }
                element_decl_list
        VHDL_end VHDL_record opt_id
        {
           $$ = new VhdlRecordTypeDef($3,VhdlScope::Pop()) ;
           ($$)->ClosingLabel($6) ;
        }
        ;

element_decl : element_id_def_list VHDL_COLON subtype_indication VHDL_SEMI
          { $$ = new VhdlElementDecl($1,$3) ; }
        ;

file_type_def : VHDL_file VHDL_of type_mark
          { $$ = new VhdlFileTypeDef($3) ; }
        ;

enumeration_type_def : VHDL_OPAREN enumeration_literal_list VHDL_CPAREN
          { $$ = new VhdlEnumerationTypeDef($2) ; }
        ;

physical_type_def : range_constraint VHDL_units
                base_unit_declaration // VIPER #3622 : base unit can be specified once
                physical_unit_decl_list
        VHDL_end VHDL_units opt_id
          {
              Array *decl_list = ($4) ? $4 : new Array(1) ;
              if ($3 && decl_list) decl_list->InsertFirst($3) ;
              $$ = new VhdlPhysicalTypeDef($1,decl_list) ;
              // No call to ClosingLabel() made, since VhdlPhysicalTypeDef
              // does not have a local scope. Call this separately:
              // VIPER #6662: Only call SetClosingLabel() is the label is specified:
              if ($7) ($$)->SetClosingLabel($7) ; // VIPER #6666 ($7 may get deleted here)
          }
        ;
base_unit_declaration : physical_unit_id_def VHDL_SEMI
          { $$ = new VhdlPhysicalUnitDecl($1,0) ; }
       ;

physical_unit_decl : //physical_unit_id_def VHDL_SEMI
          //{ $$ = new VhdlPhysicalUnitDecl($1,0) ; }
         physical_unit_id_def VHDL_EQUAL physical_literal VHDL_SEMI
          { $$ = new VhdlPhysicalUnitDecl($1,$3) ; }
        ;

/*************************** Protected type def *****************/

protected_type_def : VHDL_protected
           {
               VERIFIC_ASSERT(last_label) ;

               VhdlTreeNode::_present_scope->Undeclare(last_label) ;
               VhdlIdDef *protected_id = new VhdlProtectedTypeId(Strings::save(last_label->Name())) ;

               (void) VhdlTreeNode::_present_scope->Declare(protected_id) ;
               delete last_label ;
               last_label = 0 ;

               VhdlScope::Push(protected_id) ;
           }
           protected_type_declarative_part VHDL_end VHDL_protected opt_id
        {
            $$ = new VhdlProtectedTypeDef($3, VhdlScope::Pop()) ;
            ($$)->ClosingLabel($6) ;
        }
        ;

protected_type_declarative_part : { $$ = 0 ; }
        | protected_type_declarative_part protected_type_declarative_item
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        ;

protected_type_body_declarative_part : { $$ = 0 ; }
        | protected_type_body_declarative_part protected_type_body_declarative_item
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        ;

protected_type_declarative_item : pre_comment plain_protected_type_declarative_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        | error { $$ = 0 ; }
        ;

protected_type_body_declarative_item : pre_comment plain_protected_type_body_declarative_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
            if ($2 && $2->IsPackageBody()) $$ = 0 ; // Package body will not be added in decl list
        }
        | error { $$ = 0 ; }
        ;

plain_protected_type_declarative_item :
          subprogram_decl
        | use_clause
        | attribute_spec
        // VIPER #5918 : VHDL-2008 : Subprogram instantiation declaration
        | subprogram_spec VHDL_is VHDL_new
          uninstantiated_subprog_name opt_signature opt_generic_map_aspect VHDL_SEMI
        {
            (void) VhdlScope::Pop() ; $$ = new VhdlSubprogInstantiationDecl($1, ($5) ? new VhdlSignaturedName($4, $5): $4, $6) ;
            // VIPER #6258 : Produce error if subprogram instantiation declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
        ;

plain_protected_type_body_declarative_item :
          subprogram_decl
        | subprogram_body
        | type_decl
        | subtype_decl
        | constant_decl
        | variable_decl
        | file_decl
        | alias_decl
        | attribute_decl
        | attribute_spec
        | use_clause
        | group_template_decl
        | group_decl
        | package_instantiation_decl { $$ = $1 ; }// VHDL-2008
        | package_decl { $$ = $1 ; } // VHDL-2008 : package in decl part
        | package_body { $$ = $1 ; } // VHDL-2008 : package body in decl part
        //| subprogram_instantiation_decl // VHDL-2008
        ;

protected_type_def_body : VHDL_protected VHDL_body
           {
               VERIFIC_ASSERT(last_label) ;
               if (!last_label->IsProtectedBody()) last_label->Error("protected body %s has no protected type defined", last_label->Name()) ;

               VhdlIdDef *header_id = VhdlTreeNode::_present_scope->FindSingleObjectLocal(last_label->Name()) ;
               VhdlScope *header_scope = header_id->LocalScope() ;
               VhdlScope::Push(last_label) ;
               // VIPER #7775 : Produce error if header already contains back pointer to body
               if (header_id && header_id->GetProtectedTypeBodyId()) {
                   last_label->Error("protected type %s body is already defined", last_label->Name()) ;
               } else {
                   if (header_id) header_id->SetProtectedTypeBodyId(last_label) ;
               }

               VhdlTreeNode::_present_scope->AddDeclRegion(header_scope) ;

               last_label = 0 ;
           }
           protected_type_body_declarative_part VHDL_end VHDL_protected VHDL_body opt_id
        {
            $$ = new VhdlProtectedTypeDefBody($4, VhdlScope::Pop()) ;
            ($$)->ClosingLabel($8) ;
        }
        ;

/******************************* Constants/Variables *****************/

constant_decl : VHDL_constant constant_id_def_list VHDL_COLON subtype_indication
                        opt_init_assign VHDL_SEMI
          { $$ = new VhdlConstantDecl($2,$4,$5) ; }
        ;

signal_decl : VHDL_signal signal_id_def_list VHDL_COLON subtype_indication
                        opt_signal_kind opt_init_assign VHDL_SEMI
          { $$ = new VhdlSignalDecl($2,$4,$5,$6) ; }
        ;

variable_decl : opt_VHDL_shared VHDL_variable variable_id_def_list VHDL_COLON subtype_indication
                        opt_init_assign VHDL_SEMI
          { $$ = new VhdlVariableDecl($1,$3,$5,$6) ; }
        ;

file_decl : VHDL_file file_id_def_list VHDL_COLON subtype_indication
                        opt_file_open_info VHDL_SEMI
          { $$ = new VhdlFileDecl($2,$4,$5) ; }
        ;

alias_decl : VHDL_alias alias_id_def opt_colon_subtype_indication
                        VHDL_is name opt_signature VHDL_SEMI
          { $$ = new VhdlAliasDecl($2,$3,
                        ($6) ? new VhdlSignaturedName($5,$6) : $5) ;
            // We cannot declare an alias before we processed what it is.
            // The constructor will have processed this, so now we can declare it.
            vhdl_declare($2) ;
          }
        ;

opt_colon_subtype_indication : { $$ = 0 ; }
        | VHDL_COLON subtype_indication
          { $$ = $2 ; }
        ;

component_decl : VHDL_component component_id_def opt_VHDL_is { VhdlScope::Push($2) ; }
                opt_generic_clause
                opt_port_clause
        VHDL_end VHDL_component opt_id VHDL_SEMI
        {
           $$ = new VhdlComponentDecl($2,$5,$6,VhdlScope::Pop()) ;
           ($$)->ClosingLabel($9) ;
        }
        ;

attribute_decl : VHDL_attribute attribute_id_def VHDL_COLON type_mark VHDL_SEMI
           { $$ = new VhdlAttributeDecl($2,$4) ; }
        ;

attribute_spec : VHDL_attribute attribute_designator VHDL_of entity_spec
                        VHDL_is expression VHDL_SEMI
           { $$ = new VhdlAttributeSpec($2,$4,$6) ; }
        ;

configuration_spec : VHDL_for component_spec binding_indication VHDL_SEMI
           { $$ = new VhdlConfigurationSpec($2, $3) ; }
        ;

disconnection_spec : VHDL_disconnect guarded_signal_spec VHDL_after expression VHDL_SEMI
           { $$ = new VhdlDisconnectionSpec($2,$4) ; }
        ;

/******************************* Groups *****************/

group_template_decl : VHDL_group group_template_id_def VHDL_is
                VHDL_OPAREN entity_class_entry_list VHDL_CPAREN VHDL_SEMI
          { $$ = new VhdlGroupTemplateDecl($2, $5) ; }
        ;

entity_class_entry : entity_class opt_VHDL_BOX
          { $$ = new VhdlEntityClassEntry($1,$2) ; }
        ;

entity_class : VHDL_entity    { $$ = VHDL_entity ; }
        | VHDL_architecture   { $$ = VHDL_architecture ; }
        | VHDL_configuration  { $$ = VHDL_configuration ; }
        | VHDL_procedure      { $$ = VHDL_procedure ; }
        | VHDL_function       { $$ = VHDL_function ; }
        | VHDL_package        { $$ = VHDL_package ; }
        | VHDL_type           { $$ = VHDL_type ; }
        | VHDL_subtype        { $$ = VHDL_subtype ; }
        | VHDL_constant       { $$ = VHDL_constant ; }
        | VHDL_signal         { $$ = VHDL_signal ; }
        | VHDL_variable       { $$ = VHDL_variable ; }
        | VHDL_component      { $$ = VHDL_component ; }
        | VHDL_label          { $$ = VHDL_label ; }
        | VHDL_literal        { $$ = VHDL_literal ; }
        | VHDL_units          { $$ = VHDL_units ; }
        | VHDL_group          { $$ = VHDL_group ; }
        | VHDL_file           { $$ = VHDL_file ; }
        ;

group_decl : VHDL_group group_id_def VHDL_COLON group_template_name VHDL_SEMI
          { $$ = new VhdlGroupDecl($2, $4) ; }
        ;

group_template_name : name
        /* MERGED : group_constituent list is in indexed_name */
        ;

/******************************* Interface Decls *****************/

// Since 'interface_decl's are always separate by comma's, bison does not need
// to parse any new tokens know when we enter a interface item. So, don't use
// the special pre_comment (invented to avoid s/r conflicts for list items without a separator),
// but use the normal 'opt_comment' at the start of the rule.
// Otherwise, we drop previous comments : see VIPER issue 1995.
interface_decl : opt_comment plain_interface_decl opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;
plain_interface_decl : interface_object_decl
        | interface_type_decl
        | interface_subprogram_decl
        | interface_package_decl
        ;

interface_object_decl : opt_interface_kind interface_id_def_list VHDL_COLON opt_mode
                subtype_indication opt_signal_kind opt_init_assign
          { $$ = new VhdlInterfaceDecl($1,$2,$4,$5,$6,$7) ; }
        ;

opt_interface_kind :  { $$ = 0 ; }
        | VHDL_constant { $$ = VHDL_constant ; }
        | VHDL_signal   { $$ = VHDL_signal ; }
        | VHDL_variable { $$ = VHDL_variable ; }
        | VHDL_file     { $$ = VHDL_file ; }
        ;

opt_mode : { $$ = 0 ; }
        | VHDL_in      { $$ = VHDL_in ; }
        | VHDL_out     { $$ = VHDL_out ; }
        | VHDL_inout   { $$ = VHDL_inout ; }
        | VHDL_buffer  { $$ = VHDL_buffer ; }
        | VHDL_linkage { $$ = VHDL_linkage ; }
        ;

opt_force_mode : { $$ = 0 ; }
        | VHDL_in      { $$ = VHDL_in ; }
        | VHDL_out     { $$ = VHDL_out ; }
        ;

interface_type_decl : VHDL_type generic_type_id_def
        {
            $$ = new VhdlInterfaceTypeDecl($2) ;
            // VIPER #6258 : Produce error if interface type declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
        ;

interface_subprogram_decl : interface_subprogram_spec opt_interface_subprogram_default
        {
            (void) VhdlScope::Pop() ; $$ = new VhdlInterfaceSubprogDecl($1, $2) ;
            // VIPER #6258 : Produce error if interface subprogram declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
        ;

interface_subprogram_spec : VHDL_procedure procedure_id_def { VhdlScope::Push($2) ; ($2)->SetAsProcedure() ; }
                opt_parameter_keyword opt_formal_parameter_list
          {
              if ($4 && !$5) {
                  if ($2) $2->Error("parameter should be followed by its list") ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM($5, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind($4 ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }
              $$ = new VhdlProcedureSpec($2,0,0,$5,VhdlTreeNode::_present_scope, $4==VHDL_parameter) ;
          }
        | opt_pure_impure VHDL_function subprogram_id_def { VhdlScope::Push($3) ; }
                opt_parameter_keyword opt_formal_parameter_list VHDL_return type_mark
          {
              if ($5 && !$6) {
                  if ($3) $3->Error("parameter should be followed by its list") ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM($6, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind($5 ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }

              $$ = new VhdlFunctionSpec($1,$3,0,0,$6,$8,VhdlTreeNode::_present_scope, $5==VHDL_parameter) ;
          }
        ;

opt_interface_subprogram_default :  { $$ = 0 ; }
        | VHDL_is interface_subprogram_default { $$ = $2 ; }
        ;

interface_subprogram_default : name { $$ = $1 ; }
        | VHDL_BOX { $$ = new VhdlBox() ; }
        ;

interface_package_decl : VHDL_package package_id_def VHDL_is VHDL_new
         {
            VhdlScope::Push() ;
            // Create cross-link between the scope and the id that owns it.
            VhdlTreeNode::_present_scope->SetOwner($2) ;

            // This package declaration is in a declarative region of
            // package/entity/architecture etc. So declare the identifier
            // in upper scope
            VhdlScope *old_scope = VhdlTreeNode::_present_scope ;
            VhdlTreeNode::_present_scope = old_scope->Upper() ;
            vhdl_declare($2) ;
            VhdlTreeNode::_present_scope = old_scope ;
          }
           name interface_package_generic_map_aspect
          {
              $$ = new VhdlInterfacePackageDecl($2, $6, $7, VhdlScope::Pop()) ;
              // VIPER #6258 : Produce error if interface package declaration appears in
              // any dialect other than 2008
              if (!VhdlNode::IsVhdl2008()) {
                  ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
              }
          }
        ;

interface_package_generic_map_aspect : generic_map_aspect
        | VHDL_generic VHDL_map VHDL_OPAREN VHDL_BOX VHDL_CPAREN
          { $$ = new Array(1) ; ($$)->InsertLast(new VhdlBox()) ; }
        | VHDL_generic VHDL_map VHDL_OPAREN VHDL_default VHDL_CPAREN
          { $$ = new Array(1) ; ($$)->InsertLast(new VhdlDefault()) ; }
        ;

entity_spec : entity_designator_list VHDL_COLON entity_class
          { $$ = new VhdlEntitySpec($1,$3) ; }
        ;

component_spec : entity_designator_list VHDL_COLON name
          { $$ = new VhdlComponentSpec($1,$3) ; }
        ;

guarded_signal_spec : entity_designator_list VHDL_COLON type_mark
          { $$ = new VhdlGuardedSignalSpec($1,$3) ; }
        ;

/******************************* Concurrent Statements *****************/

concurrent_statement : pre_comment plain_concurrent_statement opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_concurrent_statement : unlabeled_cc_statement
          { $$ = $1 ; }
        | VHDL_postponed unlabeled_cc_statement
          { $$ = $2 ; if ($$) ($$)->SetLabel(0,VHDL_postponed) ; update_linefile($$, 0/*not ending linefile*/) ;}
        | label VHDL_COLON unlabeled_cc_statement
          { $$=$3 ; if ($$) ($$)->SetLabel($1) ; last_label = 0 ; update_linefile($$, 0/*not ending linefile*/) ;}
        | label VHDL_COLON VHDL_postponed unlabeled_cc_statement
          { $$=$4 ; if ($$) ($$)->SetLabel($1,VHDL_postponed) ; last_label = 0 ; update_linefile($$, 0/*not ending linefile*/) ;}
        | block_statement
        | component_instantiation_statement
        | generate_statement
        | concurrent_procedure_call_statement
        ;

unlabeled_cc_statement : process_statement
        | assertion_statement
        | cc_conditional_signal_assignment
        | cc_selected_signal_assignment
        ;

concurrent_procedure_call_statement : name VHDL_SEMI
          { $$ = new VhdlProcedureCallStatement($1) ; }
        | VHDL_postponed name VHDL_SEMI
          { $$ = new VhdlProcedureCallStatement($2) ; ($$)->SetLabel(0,VHDL_postponed) ; }
        | label VHDL_COLON VHDL_postponed name VHDL_SEMI
          {
            $$ = new VhdlProcedureCallStatement($4) ;
            ($$)->SetLabel($1, VHDL_postponed) ;
            last_label = 0 ;
          }

        /* The simple 'labeled' form of procedure call ends up in
         * 'component_instantiation'. There we need a non-optional
         * label, or else keyword 'component' in component instantiation
         * will interfere with 'component' in component declaration. This
         * interference of a statement and a declaration item messes up
         * the generate_statement : There, we need to determine if we are in
         * a statement or in a declaration with LR1 (one token). With token
         * 'component' we cannot figure that out, so we would get a
         * shift/reduce (actually a reduce/reduce) conflict inside a
         * generate_statement. That's why we need a non-optional label
         * on component instantiation and we cannot nicely merge rules of
         * procedure calls with other optionally labeled statements.
         * Don't you just love Yacc ? It's so freeking consistent.
        */

        ;

component_instantiation_statement :
        label VHDL_COLON instantiated_unit opt_generic_map_aspect opt_port_map_aspect VHDL_SEMI
          {
            // There is one corner-case where a labeled procedure call with no arguments could
            // match this rule.  In this case, we want to make sure to instantiated the correct
            // class.  Due to this, the following check is needed. (VIPER #2791)
            VhdlIdDef *unit = ($3) ? ($3)->EntityAspect() : 0 ;
            if (unit && unit->IsSubprogram()) {
                // This is actually a label procedure call with no arguments! (VIPER #2791)
                //VERIFIC_ASSERT(!$4 && !$5) ;
                // VIPER #4692: Subprogram call with port map or generic map is
                // an error case. Produce error now.
                if (!$4 && !$5) {
                    $$ = new VhdlProcedureCallStatement($3) ;
                } else {
                    $$ = 0 ;
                    if ($3) {
                        $3->Error("%s is not a %s", unit->Name(), "component") ;
                        VhdlTreeNode::_present_scope->Undeclare($1) ;
                        delete $3 ;
                        delete $1 ;
                        cleanup_treenode_array($4) ;
                        cleanup_treenode_array($5) ;
                    }
                }
            } else {
                $$ = new VhdlComponentInstantiationStatement($3,$4,$5) ;
            }
            if ($$) ($$)->SetLabel($1) ;
            last_label = 0 ;
          }
        ;

instantiated_unit : name
        | VHDL_component name
          { $$ = new VhdlInstantiatedUnit(VHDL_component, $2) ; }
        | VHDL_entity name
          { $$ = new VhdlInstantiatedUnit(VHDL_entity, $2) ; }
        | VHDL_configuration name
          { $$ = new VhdlInstantiatedUnit(VHDL_configuration, $2) ; }
        ;

generate_statement : label VHDL_COLON plain_generate_statement
           {
             $$ = $3 ;
             if ($$) ($$)->SetLabel($1) ;
             last_label = 0 ;
             update_linefile($$, 0/*not ending linefile*/) ; // VIPER #6229 : Update line-column
           }
         ;
// VIPER #5918 : Support for VHDL-2008 specific generate construct :
plain_generate_statement : for_generate_statement { $$ = $1 ; }
        | if_generate_statement { $$ = $1 ; }
        | case_generate_statement
          {
               $$ = $1 ;
               // VIPER #6258 : Produce error if case generate appears in
               // any dialect other than 2008
               if (!VhdlNode::IsVhdl2008()) {
                    if ($1) ($1)->Error("this construct is only supported in VHDL 1076-2008") ;
               }
           }
        ;

for_generate_statement : for_scheme VHDL_generate generate_statement_body
           VHDL_end VHDL_generate opt_id VHDL_SEMI
           {
               $$ = new VhdlGenerateStatement($1, $3.decls, $3.stmts, $3.scope) ;
               ($$)->ClosingLabel($6) ;
           }
         | for_scheme VHDL_generate generate_statement_body
            VHDL_end VHDL_SEMI // for-generate should not have end alternative_label
            VHDL_end VHDL_generate opt_id VHDL_SEMI
           {
               $$ = new VhdlGenerateStatement($1, $3.decls, $3.stmts, $3.scope) ;
               ($$)->ClosingLabel($8) ;
           }
        ;

for_scheme : VHDL_for { VhdlScope::Push(last_label) ; last_label = 0 ; } id VHDL_in discrete_range
            { $$ = new VhdlForScheme($3, $5, 1) ; }
        ;

if_generate_statement : if_generate_statement_part
            VHDL_end VHDL_generate opt_id VHDL_SEMI
           { $$ = $1 ; if ($$) ($$)->ClosingLabel($4) ; update_linefile($$) ; /* VIPER #6229*/}
        | if_generate_statement_part VHDL_end opt_id VHDL_SEMI
            VHDL_end VHDL_generate opt_id VHDL_SEMI
            {
               $$ = $1 ; if ($1) ($1)->ClosingAlternativeLabel($3) ; update_linefile($$) ; /* VIPER #6229*/
               // VIPER #6258 : Produce error if if generate with alternative label appears in
               // any dialect other than 2008
               if (!VhdlNode::IsVhdl2008()) {
                    if ($1) ($1)->Error("this construct is only supported in VHDL 1076-2008") ;
               }
               // No call to ClosingLabel() made, since 'if_scheme' below may not use
               // the matching 'last_label' if alternate label is there. In that case
               // calling ClosingLabel() may incorrectly error out. Call this separately:
               // VIPER #6662: Only call SetClosingLabel() is the label is specified:
               if ($7) ($$)->SetClosingLabel($7) ; // VIPER #6666 ($7 may get deleted here)
            }
        ;

if_scheme : VHDL_if expression
           { $$ = new VhdlIfScheme($2) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
        | VHDL_if id VHDL_COLON expression
           {
               $$ = new VhdlIfScheme($4, $2) ; VhdlScope::Push(($$)->GetAlternativeLabelId()) ;  // Push with alternatve_label ?
               // VIPER #6258 : Produce error if alternative label appears in
               // any dialect other than 2008
               if (!VhdlNode::IsVhdl2008()) {
                    ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
               }
            }
        ;

if_generate_statement_part : if_scheme VHDL_generate generate_statement_body
           { $$ = new VhdlGenerateStatement($1, $3.decls, $3.stmts, $3.scope) ; }
        | if_generate_statement_part else_elsif_generate_statement
           {
                $$ = ($1) ? ($1)->AddElsifElse($2): 0 ;
                // VIPER #6258 : Produce error if else-elsif generate appears in
                // any dialect other than 2008
                if (!VhdlNode::IsVhdl2008()) {
                    if ($$) ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
                }
            }
        | if_generate_statement_part VHDL_end opt_id VHDL_SEMI else_elsif_generate_statement
           {
                if ($1) $1->ClosingAlternativeLabel($3) ; $$ = ($1) ? ($1)->AddElsifElse($5): 0 ;
                // VIPER #6258 : Produce error if else-elsif generate appears in
                // any dialect other than 2008
                if (!VhdlNode::IsVhdl2008()) {
                    if ($$) ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
                }
            }
        ;

else_elsif_generate_statement : elsif_generate_statement
        | else_generate_statement
        ;

elsif_generate_statement : elsif_scheme VHDL_generate generate_statement_body
           { $$ = new VhdlGenerateStatement($1, $3.decls, $3.stmts, $3.scope) ; }
        ;

elsif_scheme : VHDL_elsif expression
           { $$ = new VhdlElsifElseScheme($2, 0) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
        | VHDL_elsif id VHDL_COLON expression
           { $$ = new VhdlElsifElseScheme($4, $2) ; VhdlScope::Push(($$)->GetAlternativeLabelId()) ; }
        ;

else_generate_statement : else_scheme VHDL_generate generate_statement_body
           { $$ = new VhdlGenerateStatement($1, $3.decls, $3.stmts, $3.scope) ; }
        ;

else_scheme : VHDL_else
           { $$ = new VhdlElsifElseScheme(0, 0) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
        | VHDL_else id VHDL_COLON
           { $$ = new VhdlElsifElseScheme(0, $2) ; VhdlScope::Push(($$)->GetAlternativeLabelId()) ; }
        ;

case_generate_statement : VHDL_case expression VHDL_generate
           case_generate_alternatives
           VHDL_end VHDL_generate opt_id VHDL_SEMI
           { $$ = new VhdlCaseGenerateStatement($2, $4) ; if ($$) ($$)->ClosingLabel($7) ;}
        | VHDL_case expression VHDL_generate
          case_generate_alternatives
          VHDL_end opt_id VHDL_SEMI
          VHDL_end VHDL_generate opt_id VHDL_SEMI
           {
               $$ = new VhdlCaseGenerateStatement($2, $4) ; if ($$) ($$)->ClosingLabel($10) ;
               VhdlStatement *last_alt = ($4) ? (VhdlStatement*)($4)->GetLast(): 0 ;
               if (last_alt) last_alt->ClosingAlternativeLabel($6) ;
           }
        ;

case_generate_alternatives : case_generate_alternative
           { $$ = new Array(1) ; ($$)->InsertLast($1) ; }
        | case_generate_alternatives case_generate_alternative
           // VIPER #6935 : Create new Array if $1 is not present
           { $$ = ($1) ? $1 : new Array(1) ; ($$)->InsertLast($2) ; }
        | case_generate_alternatives VHDL_end opt_id VHDL_SEMI case_generate_alternative
           {
               VhdlStatement *last_alt = ($1) ? (VhdlStatement*)($1)->GetLast(): 0 ;
               if (last_alt) last_alt->ClosingAlternativeLabel($3) ;
               // VIPER #6935 : Create new Array if $1 is not present
               $$ = ($1) ? $1 : new Array(1) ; ($$)->InsertLast($5) ;
           }
        | error
           { $$ = 0 ; }
        ;

case_generate_alternative : case_scheme generate_statement_body
           { $$ = new VhdlGenerateStatement($1, ($2).decls, ($2).stmts, ($2).scope) ; }
        ;

case_scheme : VHDL_when choices VHDL_ARROW
           { $$ = new VhdlCaseItemScheme($2, 0) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
        | VHDL_when id VHDL_COLON choices VHDL_ARROW
           { $$ = new VhdlCaseItemScheme($4, $2) ; VhdlScope::Push(($$)->GetAlternativeLabelId()) ; }
        ;

generate_statement_body :
             {
                 if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                     // VIPER #7494 : Reset process sequence number remembering current one
                     $<unsigned_number>$ = process_sequence_num ;
                     process_sequence_num = 0 ;
                 }
             }
             generate_statement_body_int
             {
                 $$ = $2 ;
                 if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                     // Pop process sequence number
                     process_sequence_num = $<unsigned_number>1 ;
                 }
             }
        ;

generate_statement_body_int : gen_block_statement_list
          { $$.decls = 0; $$.stmts = $1; $$.scope = VhdlScope::Pop() ; }
        | gen_block_decl_list VHDL_begin
          gen_block_statement_list
          // VIPER #7719: Create empty array for decls since "begin" keyword was specified:
          { $$.decls = (($1)?($1):new Array(1)); $$.stmts = $3 ; $$.scope = VhdlScope::Pop() ; }
        | error { $$.decls = 0 ; $$.stmts = 0 ; $$.scope = 0 ;}
        ;

// Instead of using block_decl_part and block_stmt_part, add new rules without error handling
// So that error handling can be done in upper level i.e generate_statement_body to avoid
// illegal block creation in error condition :
gen_block_decl_list : { $$ = 0 ; }
        | gen_block_decl_list block_decl_item
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        ;

gen_block_statement_list : { $$ = 0 ; }
        | gen_block_statement_list concurrent_statement
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2) CreateAndAddImplicitLabel($2) ; /* VIPER #7494*/}
        ;

/*
generate_statement : label VHDL_COLON
                        generation_scheme VHDL_generate
                        block_statement_part
                VHDL_end VHDL_generate opt_id VHDL_SEMI
        {
           $$ = new VhdlGenerateStatement($3, 0, $5, VhdlScope::Pop()) ;
           ($$)->SetLabel($1) ;
           ($$)->ClosingLabel($8) ;
           last_label = 0 ;
        }
        | label VHDL_COLON
                        generation_scheme VHDL_generate
                        block_decl_part VHDL_begin
                        block_statement_part
                VHDL_end VHDL_generate opt_id VHDL_SEMI
        {
           $$ = new VhdlGenerateStatement($3, $5, $7, VhdlScope::Pop()) ;
           ($$)->SetLabel($1) ;
           ($$)->ClosingLabel($10) ;
           last_label = 0 ;
        }
        ;
generation_scheme : VHDL_for { VhdlScope::Push(last_label) ; last_label = 0 ; } id VHDL_in discrete_range
          { $$ = new VhdlForScheme($3, $5, 1) ; }
        | VHDL_if { VhdlScope::Push(last_label) ; last_label = 0 ; } expression
          { $$ = new VhdlIfScheme($3) ; }
        ;
*/

/******************************* Assignments *****************/

cc_conditional_signal_assignment :
        target VHDL_SEQUAL options conditional_waveforms VHDL_SEMI
          { $$ = new VhdlConditionalSignalAssignment($1,$3,$4) ; }
        | target VHDL_SEQUAL options waveform VHDL_SEMI
          {
            // Added this rule to have this type of assignment
            // which was otherwise included in conditional_waveforms
            // definition
            Array *wv_list = new Array() ;
            wv_list->InsertLast(new VhdlConditionalWaveform($4,0)) ;
            $$ = new VhdlConditionalSignalAssignment($1,$3,wv_list) ;
          }
        ;

cc_selected_signal_assignment : VHDL_with expression VHDL_select opt_matching
                target VHDL_SEQUAL options selected_waveforms VHDL_SEMI
          { $$ = new VhdlSelectedSignalAssignment($2, $5, $7, $8, 0, $4 ? 1: 0) ; }
        ;

target : name   { $$ = $1 ;}
        | aggregate { $$ = $1 ;}
        ;

options : opt_VHDL_guarded opt_delay_mechanism
          { $$ = ($1 || $2) ? new VhdlOptions($1,$2) : 0 ; }
        ;

delay_mechanism : VHDL_transport
          { $$ = new VhdlTransport() ; }
        | VHDL_inertial
          { $$ = new VhdlInertialDelay(0, VHDL_inertial) ; }
        | VHDL_reject expression VHDL_inertial
          { $$ = new VhdlInertialDelay($2, VHDL_inertial) ; }
        ;

// The rules are re-written to avoid right recursion. This also makes the rules re-usable for
// conditional waveform assignments statements in sequential area as per Vhdl 2008 requirement
conditional_waveforms : waveform VHDL_when expression else_when_waveform_list else_with_waveform
           {
             $$ = ($4) ? $4 : new Array() ;
             ($$)->InsertFirst(new VhdlConditionalWaveform($1, $3)) ;
             if ($5) ($$)->InsertLast($5) ;
           }
        ;

else_when_waveform_list :
               { $$ = 0 ; }
             | else_when_waveform_list VHDL_else waveform VHDL_when expression
               { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast(new VhdlConditionalWaveform($3,$5)) ; }
        ;

else_with_waveform :
               { $$ = 0 ; }
             | VHDL_else waveform
               { $$ = new VhdlConditionalWaveform($2,0) ; }
        ;

selected_waveforms : waveform VHDL_when choices
          { $$ = new Array() ;
            ($$)->InsertLast(new VhdlSelectedWaveform($1,$3)) ;
          }
        | selected_waveforms VHDL_COMMA waveform VHDL_when choices
          { $$ = ($1) ? ($1) : new Array() ;
            ($$)->InsertLast(new VhdlSelectedWaveform($3,$5)) ;
          }
        ;

waveform : waveform_element_list
        | VHDL_unaffected
          { $$ = new Array() ; ($$)->InsertLast(new VhdlUnaffected()) ; }
        ;

waveform_element_list : waveform_element
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | waveform_element_list VHDL_COMMA waveform_element
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

waveform_element : expression
        | expression VHDL_after expression
          { $$ = new VhdlWaveformElement($1,$3) ; }
        ;

/******************************* Blocks *****************/

block_statement : label VHDL_COLON VHDL_block opt_guard_expression opt_VHDL_is
        {
            // Need a 'block-label' here. yacc does not accept that, so create one now :
            // Make sure to preserve the original name label.
            VhdlIdDef *block_label = new VhdlBlockId( Strings::save(($1)->OrigName()) ) ;
            // VIPER #3407: Set the linefile of this block id from the original label:
            block_label->SetLinefile(($1)->Linefile()) ;
            // Remove the old one from the scope :
            VhdlTreeNode::_present_scope->Undeclare($1) ;
            // Declare the new one :
            vhdl_declare(block_label) ;
            // delete the old one.
            delete $1 ;
            // Set the new one as $1 :
            $1 = block_label ;

            /* Start a new scope */
            VhdlScope::Push($1) ;
            last_label = 0 ;
            if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                // VIPER #7494 : Store current process sequence number
                $<unsigned_number>$ = process_sequence_num ;
                process_sequence_num = 0 ;
            }
        }
                block_generics
                block_ports
        {
            if ($4) {
                /* There is a guard expression. Declare "guard" (LRM 9.1) here at the beginning of the decl part */
                VhdlIdDef *guard = new VhdlSignalId(Strings::save("guard")) ;
                vhdl_declare(guard) ;

                /* Make it of type Boolean */
                guard->DeclareSignal(VhdlNode::StdType("boolean"),0, 1, 1, VHDL_implicit_guard) ;
            }
        }
                block_decl_part
        VHDL_begin
                block_statement_part
        VHDL_end VHDL_block opt_id VHDL_SEMI
        {
           $$ = new VhdlBlockStatement($4, $7, $8, $10, $12, VhdlScope::Pop()) ;
           ($$)->SetLabel($1) ;
           ($$)->ClosingLabel($15) ;
           if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
               // VIPER #7494 : Reset process sequence number
               process_sequence_num = $<unsigned_number>6 ;
           }
        }
        ;

block_generics :  { $$ = 0 ; }
        | generic_clause
          { $$ = new VhdlBlockGenerics($1, 0) ; }
        | generic_clause generic_map_aspect VHDL_SEMI
          { $$ = new VhdlBlockGenerics($1, $2) ; }
        ;

block_ports : { $$ = 0 ; }
        | port_clause
          { $$ = new VhdlBlockPorts($1, 0) ; }
        | port_clause port_map_aspect VHDL_SEMI
          { $$ = new VhdlBlockPorts($1, $2) ; }
        ;

block_decl_part :  { $$ = 0 ; }
        | block_decl_part block_decl_item
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        | error { $$ = 0 ;}
        ;

block_statement_part : { $$ = 0 ; }
        | block_statement_part concurrent_statement
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2) CreateAndAddImplicitLabel($2) /* VIPER #7494 */ ;}
        | error { $$ = 0 ;}
        ;

opt_guard_expression : { $$ = 0 ; }
        | VHDL_OPAREN expression VHDL_CPAREN
          { $$ = $2 ; }
        ;
/******************************* Process  *****************/

process_statement :
        VHDL_process opt_sensitivity_list opt_VHDL_is
        {  VhdlScope::Push(last_label) ; last_label = 0 ;
           // Flag the scope as coming from a process
           VhdlTreeNode::_present_scope->SetIsProcessScope() ;
           // Flag that no wait statement is allowed (this process has a sensitivity list)
           if ($2) VhdlTreeNode::_present_scope->SetWaitStatementNotAllowed() ;
        }
                process_decl_part
        //VHDL_begin
        sequential_stmts_begin // VIPER #7494 : To reset loop sequence number
                process_statement_part
        VHDL_end opt_VHDL_postponed VHDL_process opt_id VHDL_SEMI
        {
           $$ = new VhdlProcessStatement($2, $5, $7, VhdlScope::Pop()) ;
           ($$)->ClosingLabel($11) ;
        }
        ;

opt_sensitivity_list : { $$ = 0 ; }
        | VHDL_OPAREN sensitivity_list VHDL_CPAREN
          { $$ = $2 ; }
        | VHDL_OPAREN VHDL_all VHDL_CPAREN
          {   // Viper #5918 for Vhdl 2008 support
              VhdlAll *vhd_all = new VhdlAll() ;
              $$ = new Array() ;
              ($$)->InsertLast(vhd_all) ;

              if (!vhdl_file::IsVhdlPsl() && !vhdl_file::IsVhdl2008()) vhd_all->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        ;

sensitivity_list : name_list
        ;

process_decl_part : { $$ = 0 ; }
        | process_decl_part process_decl_item
          { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; if ($2 && $2->IsPrimaryUnit()) $2->SetContainerArray($$) ;}
        | error { $$ = 0 ; }
        ;

process_decl_item : pre_comment plain_process_decl_item opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
            if ($2 && $2->IsPackageBody()) $$ = 0 ; // Package body will not be added in decl list
        }
        ;

plain_process_decl_item : subprogram_decl
        | subprogram_body
        | type_decl
        | subtype_decl
        | constant_decl
        | variable_decl
        | file_decl
        | alias_decl
        | attribute_decl
        | attribute_spec
        | use_clause
        | group_template_decl
        | group_decl
        | package_decl { $$ = $1 ; } // VHDL-2008
        | package_instantiation_decl { $$ = $1 ; } // VHDL-2008
        | package_body { $$ = $1 ; } // VHDL-2008 : package body in decl part
        ;

process_statement_part : { $$ = 0 ; }
        | process_statement_part sequential_statement
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error { $$ = 0 ; }
        ;

/******************************* Sequential Statements *****************/

sequential_statement : pre_comment plain_sequential_statement opt_comment
        {
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_sequential_statement : unlabeled_statement
          { $$ = $1 ; }
        | label VHDL_COLON unlabeled_statement
          { $$ = $3 ; ($$)->SetLabel($1) ; last_label = 0 ; update_linefile($$, 0/*not ending linefile*/) /* VIPER #6274 */; }
        ;

unlabeled_statement : wait_statement
        | assertion_statement
        | report_statement
        | signal_assignment_statement
        | variable_assignment_statement
        | procedure_call_statement
        | if_statement
        | case_statement
        | loop_statement
        | next_statement
        | exit_statement
        | return_statement
        | null_statement
        ;

assertion_statement :
      VHDL_assert expression opt_report_expression opt_severity_expression VHDL_SEMI
        { $$ = new VhdlAssertionStatement($2,$3,$4) ; }
        ;

report_statement : VHDL_report expression opt_severity_expression VHDL_SEMI
          { $$ = new VhdlReportStatement($2, $3) ; }
        ;

opt_report_expression :  { $$ = 0 ; }
        | VHDL_report expression { $$ = $2 ; }
        ;

opt_severity_expression :  { $$ = 0 ; }
        | VHDL_severity expression { $$ = $2 ; }
        ;

wait_statement : VHDL_wait opt_sensitivity_clause
                opt_condition_clause opt_timeout_clause VHDL_SEMI
          { $$ = new VhdlWaitStatement($2,$3,$4) ; }
        ;

opt_sensitivity_clause : { $$ = 0 ; }
        | VHDL_on sensitivity_list { $$ = $2 ; }
        ;

opt_condition_clause : { $$ = 0 ; }
        | VHDL_until expression { $$ = $2 ; }
        ;

opt_timeout_clause : { $$ = 0 ; }
        | VHDL_for expression { $$ = $2 ; }
        ;

signal_assignment_statement : simple_signal_assignment
        | conditional_signal_assignment
          {
            $$ = $1 ;
            if (!VhdlNode::IsVhdl2008()) $1->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        | selected_signal_assignment
          {
            $$ = $1 ;
            if (!VhdlNode::IsVhdl2008()) $1->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        ;

conditional_signal_assignment :
        target VHDL_SEQUAL opt_delay_mechanism conditional_waveforms VHDL_SEMI
          { $$ = new VhdlConditionalSignalAssignment($1,0,$4,$3) ; }
         // VHDL-2008 : Conditional force assignment
        | target VHDL_SEQUAL VHDL_force opt_force_mode conditional_expressions VHDL_SEMI
          { $$ = new VhdlConditionalForceAssignmentStatement($1, $4, $5) ; }
        ;

selected_signal_assignment : VHDL_with expression VHDL_select VHDL_matching_op
                target VHDL_SEQUAL opt_delay_mechanism selected_waveforms VHDL_SEMI
          {
            $$ = new VhdlSelectedSignalAssignment($2, $5, 0, $8, $7, 1) ;
          }
        | VHDL_with expression VHDL_select
             target VHDL_SEQUAL opt_delay_mechanism selected_waveforms VHDL_SEMI

          { $$ = new VhdlSelectedSignalAssignment($2, $4, 0, $7, $6) ; }
          // VHDL-2008 : Selected force assignment
        | VHDL_with expression VHDL_select
             target VHDL_SEQUAL VHDL_force opt_force_mode selected_expressions VHDL_SEMI
          {
              // VIPER #8152: Create parse tree for selected force assignment
              $$ = new VhdlSelectedForceAssignment($2, 0, $4, $7, $8) ;
          }
        | VHDL_with expression VHDL_select VHDL_matching_op
             target VHDL_SEQUAL VHDL_force opt_force_mode selected_expressions VHDL_SEMI
          {
              // VIPER #8152: Create parse tree for selected force assignment
              $$ = new VhdlSelectedForceAssignment($2, 1, $5, $8, $9) ;
          }
        ;

simple_signal_assignment : target VHDL_SEQUAL opt_delay_mechanism waveform VHDL_SEMI
          { $$ = new VhdlSignalAssignmentStatement($1, $3, $4) ; }
          // VHDL-2008 : simple force assignment
        | target VHDL_SEQUAL VHDL_force opt_force_mode expression VHDL_SEMI
          {
            $$ = new VhdlForceAssignmentStatement($1, $4, $5) ;
            if (!VhdlNode::IsVhdl2008()) $$->Error("this construct is only supported in VHDL 1076-2008") ;
          }
          // VHDL-2008 : simple release assignment
        | target VHDL_SEQUAL VHDL_release opt_force_mode VHDL_SEMI
          {
            $$ = new VhdlReleaseAssignmentStatement($1, $4) ;
            if (!VhdlNode::IsVhdl2008()) $$->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        ;

variable_assignment_statement : simple_assignment_statement
        | conditional_assignment_statement
          {
            $$ = $1 ;
            if (!VhdlNode::IsVhdl2008()) $1->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        | selected_assignment_statement
          {
            $$ = $1 ;
            if (!VhdlNode::IsVhdl2008()) $1->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        ;

simple_assignment_statement : target VHDL_VARASSIGN expression VHDL_SEMI
          { $$ = new VhdlVariableAssignmentStatement($1, $3) ; }
        ;

conditional_assignment_statement : target VHDL_VARASSIGN conditional_expressions VHDL_SEMI
          { $$ = new VhdlConditionalVariableAssignmentStatement($1, $3) ; }
        ;

conditional_expressions : expression VHDL_when expression else_when_list else_with_expr
           {
             $$ = ($4) ? $4 : new Array() ;
             ($$)->InsertFirst(new VhdlConditionalExpression($1, $3)) ; // Viper 7460 InsertFirst instead of InsertLast
             if ($5) ($$)->InsertLast($5) ;
           }
                        ;
else_when_list :
               { $$ = 0 ; }
             | else_when_list VHDL_else expression VHDL_when expression
               { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast(new VhdlConditionalExpression($3,$5)) ; }
        ;

else_with_expr :
               { $$ = 0 ; }
             | VHDL_else expression
               { $$ = new VhdlConditionalExpression($2,0) ; }
        ;

selected_assignment_statement : VHDL_with expression VHDL_select VHDL_matching_op target VHDL_VARASSIGN selected_expressions VHDL_SEMI
          { $$ = new VhdlSelectedVariableAssignmentStatement($2, $5, $7, 1/* matching select */) ; }
        | VHDL_with expression VHDL_select
            target VHDL_VARASSIGN selected_expressions VHDL_SEMI
          { $$ = new VhdlSelectedVariableAssignmentStatement($2, $4, $6) ; }
        ;

selected_expressions : expression VHDL_when choices
             { $$ = new Array() ;
               ($$)->InsertLast(new VhdlSelectedExpression($1,$3)) ;
             }
             | selected_expressions VHDL_COMMA expression VHDL_when choices
             {
               $$ = ($1) ? ($1) : new Array() ;
               ($$)->InsertLast(new VhdlSelectedExpression($3,$5)) ;
             }
        ;

procedure_call_statement : name VHDL_SEMI
          { $$ = new VhdlProcedureCallStatement($1) ; }
        ;

if_statement : VHDL_if expression VHDL_then
                sequence_of_statements
                elsif_list
                else_statements
        VHDL_end VHDL_if opt_id VHDL_SEMI
           {
                $$ = new VhdlIfStatement($2, $4, $5, $6) ;
                ($$)->ClosingLabel($9) ;
           }
        ;

elsif_list :
          { $$ = 0 ; }
        | elsif_list VHDL_elsif expression VHDL_then sequence_of_statements
          { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast(new VhdlElsif($3,$5)) ; }
        ;

else_statements : { $$ = 0 ; }
        | VHDL_else sequence_of_statements
           { $$ = ($2) ? ($2) : new Array(1) ; } // VIPER #7866: Create empty Array for else part with no statement but having the 'else' keyword.
        ;

sequence_of_statements : { $$ = 0 ; }
        | sequence_of_statements sequential_statement
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error { $$ = 0 ; }
        ;

case_statement : VHDL_case opt_matching expression VHDL_is
                        case_statement_alternatives
                VHDL_end VHDL_case opt_matching opt_id VHDL_SEMI
           {
                $$ = new VhdlCaseStatement($3,$5, $2 ? 1: 0) ;
                // LRM 1076-2008 : Section 10.6 says question mark delimiter should
                // present either in both places or in neither place.
                if (($2 && !$8) || (!$2 && $8)) {
                    $$->Error("question mark delimiter should be in both places after keyword case") ;
                }
                ($$)->ClosingLabel($9) ;
           }
        ;

opt_matching : { $$ = 0 ; }
        | VHDL_matching_op { $$ = VHDL_matching_op ; }
        ;

case_statement_alternatives : case_statement_alternative
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | case_statement_alternatives case_statement_alternative
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error
          { $$ = 0 ; }
        ;

case_statement_alternative : VHDL_when choices VHDL_ARROW sequence_of_statements
          { $$ = new VhdlCaseStatementAlternative($2,$4) ; }
        ;

loop_statement :{        /* Create a new scope */
                        VhdlIdDef *implicit_label_id = 0 ;
                        if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                            // VIPER #7494: If label is not specified, create implicit
                            // label identifier according to VHDL-2008 (19.4.2.2)
                            if (!last_label) {
                                char name[20] ;
                                sprintf(name, "_L%d", loop_sequence_num) ;
                                implicit_label_id = new VhdlLabelId(Strings::save(name)) ;
                                vhdl_declare_label(implicit_label_id) ;
                                last_label = implicit_label_id ;
                            }
                        }
                        // Viper 2597 : Create a transparent scope for loop, so that
                        // labels of nested statements are declared in a non-transparent scope
                        VhdlScope::Push(last_label, 1) ; last_label = 0 ;
                        if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                            // VIPER #7494: Store implicit label to add in statement
                            $<id_def>$ = implicit_label_id ;
                            // Increase implicit loop sequence number
                            loop_sequence_num++ ;
                        }
                }
                opt_iteration_scheme VHDL_loop
                        sequence_of_statements
                VHDL_end VHDL_loop opt_id VHDL_SEMI
                {
                        $$ = new VhdlLoopStatement($2, $4, VhdlScope::Pop()) ;
                        if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                            // VIPER #7494: Set implicit label id to loop statement
                            VhdlIdDef *implicit_label_id = $<id_def>1 ;
                            if (implicit_label_id) ($$)->SetLabel(implicit_label_id) ;
                        }
                        ($$)->ClosingLabel($7) ;
                }
        ;

opt_iteration_scheme :
          { /* infinite loop */ $$ = new VhdlWhileScheme(0) ; }
        | VHDL_while expression
          { $$ = new VhdlWhileScheme($2) ; }
        | VHDL_for id VHDL_in discrete_range
          { $$ = new VhdlForScheme($2, $4, 0) ; }
        | VHDL_for error
          { $$ = 0 ; /*VIPER #1900*/ }
        ;

next_statement : VHDL_next opt_id opt_when_condition VHDL_SEMI
          { $$ = new VhdlNextStatement($2,$3) ; }
        ;

exit_statement : VHDL_exit opt_id opt_when_condition VHDL_SEMI
          { $$ = new VhdlExitStatement($2,$3) ; }
        ;

return_statement : VHDL_return opt_expression VHDL_SEMI
          { $$ = new VhdlReturnStatement($2,VhdlTreeNode::_present_scope->GetSubprogram()) ; }
        ;

null_statement : VHDL_null VHDL_SEMI
          { $$ = new VhdlNullStatement() ; }
        ;

opt_when_condition :  { $$=0; }
        | VHDL_when expression
          { $$ = $2 ; }
        ;

/******************************* Misc *****************/

binding_indication : opt_generic_map_aspect opt_port_map_aspect
          { $$ = new VhdlBindingIndication(0,$1,$2) ; }
        | VHDL_use entity_aspect opt_generic_map_aspect opt_port_map_aspect
          { $$ = new VhdlBindingIndication($2,$3,$4) ; }
        ;

entity_aspect : VHDL_entity  name
          /* Architecture (name) moved into indexed name */
          { $$ = new VhdlEntityAspect(VHDL_entity, $2) ; }
        | VHDL_configuration name
          { $$ = new VhdlEntityAspect(VHDL_configuration, $2) ; }
        | VHDL_open
          { $$ = new VhdlOpen() ; }
        ;

opt_generic_map_aspect :  { $$=0; }
        | generic_map_aspect
        ;

opt_port_map_aspect :  { $$=0; }
        | port_map_aspect
        ;

generic_map_aspect : VHDL_generic VHDL_map VHDL_OPAREN assoc_list VHDL_CPAREN
          { $$ = $4 ; }
        ;

port_map_aspect : VHDL_port VHDL_map VHDL_OPAREN assoc_list VHDL_CPAREN
          { $$ = $4 ; }
        ;

assoc_list : assoc_element
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | assoc_list VHDL_COMMA assoc_element
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

assoc_element : pre_comment plain_assoc_element opt_comment
        {
            // VIPER #6534: Preserve comments around assoc-elements:
            $$ = $2 ;
            add_pre_comments_to_node($$, $1) ; // pre-comments
            add_post_comments_to_node($$, $3) ; // trailing comments
        }
        ;

plain_assoc_element : actual_part { $$ = $1 ; }
        | formal_part VHDL_ARROW actual_part
          { $$ = new VhdlAssocElement($1,$3) ; }
        | discrete_range_subset1  { $$ = $1 ; }
          /* for sliced_name and index_constraint */
        ;

formal_part : name
          /* MERGED : function call is in name. Also covers type_mark */
        ;

actual_part : actual_designator {$$ = $1 ; }
        |  VHDL_inertial actual_part  // Viper 6660 inertial is a part of actual
           {
               $$ = new VhdlInertialElement($2) ;
           } ;
          /* MERGED : function call is in 'name' of 'expression'. */
        ;

actual_designator :
        expression
        | VHDL_open { $$ = new VhdlOpen() ; }
        ;

signature : VHDL_OBRACK opt_type_mark_list opt_return_type VHDL_CBRACK
          { $$ = new VhdlSignature($2, $3) ; }
        ;

opt_return_type : { $$ = 0 ; }
        | VHDL_return type_mark
          { $$ = $2 ; }
        ;

file_open_info : opt_file_open VHDL_is opt_mode file_logical_name
        /* CHECK ME : 'opt_mode' is not legal VHDL'93, but is in '87
         * definition. Adding is here will make backward compatibility
         * a lot easier (so '93 will be a superset of '87).
        */
           { $$ = new VhdlFileOpenInfo($1, $3, $4) ; }
        ;

opt_file_open : { $$ = 0 ; }
        | VHDL_open expression { $$ = $2 ; }
        ;

file_logical_name : expression
        ;

/******************************* Constraints *****************/

range_constraint : VHDL_range range
          { $$ = $2 ; }
        | VHDL_range VHDL_BOX
          /* range constraint extended, for unconstrained array constraints */
          { $$ = new VhdlBox() ; }
        ;

index_constraint : VHDL_OPAREN discrete_range_list VHDL_CPAREN
          { $$ = $2 ; }
        ;

range : attribute_name { $$ = $1 ; }
        | range_subset1 { $$ = $1 ; }
        ;

range_subset1 : simple_expression direction simple_expression
          { $$ = new VhdlRange($1, $2, $3) ; }
        ;

direction : VHDL_to   { $$ = VHDL_to ; }
        | VHDL_downto { $$ = VHDL_downto ; }
        ;

// According to 2002 1376 LRM section 3.2.1: If a subtype indication
// appears as a discrete range, the subtype indication must not
// contain a resolution function: Hence we split subtype_indication_subset1
// into subtype_indication_subset2 which is used in discrete_range
// and discrete_range_subset1

discrete_range : name { $$ = $1 ; }
        | subtype_indication_subset2 { $$ = $1 ; }
        | range_subset1 { $$ = $1 ; }
        ;

discrete_range_subset1 : subtype_indication_subset2 { $$ = $1 ; }
        | range_subset1 { $$ = $1 ; }
        ;

subtype_indication : name  { $$ = $1 ; }
        | subtype_indication_subset1 { $$ = $1 ; }
        | subtype_indication_subset2 { $$ = $1 ; }
        ;

subtype_indication_subset1 : resolution_indication type_mark range_constraint
           { $$ = new VhdlExplicitSubtypeIndication($1, $2, $3) ; }
        | resolution_indication type_mark
           { $$ = new VhdlExplicitSubtypeIndication($1, $2, 0) ; }
        ;
subtype_indication_subset2 : type_mark range_constraint
           { $$ = new VhdlExplicitSubtypeIndication(0, $1, $2) ; }
        ;

// With Vhdl 2008 LRM section 6.3 the resolution function name can be placed
// in a nesting of parenthesis. The degree of nesting of parentheses
// indicates how deeply nested in the type structure the resolution
// function is associated. Two levels indicate that the resolution function
// is associated with the elements of the elements of the type.
// Now changed the resolution_indication from name to id to avoid conflicts

resolution_indication : id  { $$= $1 ; }
        | array_resolution_indication
        | record_resolution_indication
        ;

array_resolution_indication : VHDL_OPAREN resolution_indication VHDL_CPAREN
          {
              $$ = new VhdlArrayResFunction($2) ;
              if (!vhdl_file::IsVhdl2008()) ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        ;

record_resolution_indication : VHDL_OPAREN record_resolution_element_list VHDL_CPAREN
          {
              $$ = new VhdlRecordResFunction($2) ;
              if (!vhdl_file::IsVhdl2008()) ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
          }

record_resolution_element_list : record_resolution_element
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | record_resolution_element_list VHDL_COMMA record_resolution_element
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        ;

record_resolution_element : id  resolution_indication
           { $$ = new VhdlRecResFunctionElement($1, $2) ; }
        ;

/******************************* Expressions *****************/

/* Odd rules due to odd operator preferences in VHDL */
/* Added reduction operators as well for VHDL 1076-2008 section 9.2.2 */
expression : relation
        | AND_expression
        | OR_expression
        | XOR_expression
        | XNOR_expression
        | relation VHDL_nand relation
          { $$ = new VhdlOperator ($1, VHDL_nand, $3) ; }
        | relation VHDL_nor relation
          { $$ = new VhdlOperator ($1, VHDL_nor, $3) ; }
        ;

AND_expression : relation VHDL_and relation
          { $$ = new VhdlOperator ($1, VHDL_and, $3) ; }
        | AND_expression VHDL_and relation
          { $$ = new VhdlOperator ($1, VHDL_and, $3) ; }
        ;

OR_expression : relation VHDL_or relation
          { $$ = new VhdlOperator ($1, VHDL_or, $3) ; }
        | OR_expression VHDL_or relation
          { $$ = new VhdlOperator ($1, VHDL_or, $3) ; }
        ;

XOR_expression : relation VHDL_xor relation
          { $$ = new VhdlOperator ($1, VHDL_xor, $3) ; }
        | XOR_expression VHDL_xor relation
          { $$ = new VhdlOperator ($1, VHDL_xor, $3) ; }
        ;

XNOR_expression : relation VHDL_xnor relation
          { $$ = new VhdlOperator ($1, VHDL_xnor, $3) ; }
        | XNOR_expression VHDL_xnor relation
          { $$ = new VhdlOperator ($1, VHDL_xnor, $3) ; }
        ;

relation : shift_expression
        | shift_expression relational_oper shift_expression
          { $$ = new VhdlOperator ($1,$2,$3) ; }
        ;

relational_oper : VHDL_EQUAL    { $$ = VHDL_EQUAL ; }
        | VHDL_NEQUAL           { $$ = VHDL_NEQUAL ; }
        | VHDL_GTHAN            { $$ = VHDL_GTHAN ; }
        | VHDL_STHAN            { $$ = VHDL_STHAN ; }
        | VHDL_GEQUAL           { $$ = VHDL_GEQUAL ; }
        | VHDL_SEQUAL           { $$ = VHDL_SEQUAL ; }
        | VHDL_matching_equal   { $$ = VHDL_matching_equal ; }
        | VHDL_matching_nequal  { $$ = VHDL_matching_nequal ; }
        | VHDL_matching_gthan   { $$ = VHDL_matching_gthan ; }
        | VHDL_matching_sthan   { $$ = VHDL_matching_sthan ; }
        | VHDL_matching_gequal  { $$ = VHDL_matching_gequal ; }
        | VHDL_matching_sequal  { $$ = VHDL_matching_sequal ; }
        ;

shift_expression : simple_expression
        | simple_expression shift_operator simple_expression
          { $$ = new VhdlOperator ($1,$2,$3) ; }
        ;

shift_operator : VHDL_sll { $$ = VHDL_sll ; }
        | VHDL_srl        { $$ = VHDL_srl ; }
        | VHDL_sla        { $$ = VHDL_sla ; }
        | VHDL_sra        { $$ = VHDL_sra ; }
        | VHDL_rol        { $$ = VHDL_rol ; }
        | VHDL_ror        { $$ = VHDL_ror ; }
        ;

simple_expression : term
        | VHDL_PLUS term %prec VHDL_STAR
          { $$ = new VhdlOperator($2,VHDL_PLUS,0) ; }
        | VHDL_MINUS term %prec VHDL_STAR
          { $$ = new VhdlOperator($2,VHDL_MINUS,0) ; }
        | simple_expression adding_operator term
          { $$ = new VhdlOperator ($1,$2,$3) ; }
        ;

adding_operator : VHDL_PLUS { $$ = VHDL_PLUS ; }
        | VHDL_MINUS        { $$ = VHDL_MINUS ; }
        | VHDL_AMPERSAND    { $$ = VHDL_AMPERSAND ; }
        ;

term : factor
        | term multiplying_operator factor
          { $$ = new VhdlOperator($1, $2, $3) ; }
        ;

multiplying_operator : VHDL_STAR { $$ = VHDL_STAR ; }
        | VHDL_SLASH             { $$ = VHDL_SLASH ; }
        | VHDL_mod               { $$ = VHDL_mod ; }
        | VHDL_rem               { $$ = VHDL_rem ; }
        ;

logical_operator : VHDL_and { $$ = VHDL_and ; }
        | VHDL_or { $$ = VHDL_or ; }
        | VHDL_xor { $$ = VHDL_xor ; }
        | VHDL_xnor { $$ = VHDL_xnor ; }
        | VHDL_nand { $$ = VHDL_nand ; }
        | VHDL_nor { $$ = VHDL_nor ; }
        ;

factor : primary
        | primary VHDL_EXPONENT primary
          { $$ = new VhdlOperator($1, VHDL_EXPONENT, $3) ; }
        | VHDL_abs primary
          { $$ = new VhdlOperator($2,VHDL_abs,0) ; }
        | VHDL_not primary
          { $$ = new VhdlOperator($2,VHDL_not,0) ; }
        | VHDL_condition primary
          { $$ = new VhdlOperator($2,VHDL_condition,0) ; }
        | logical_operator primary %prec VHDL_EQUAL
          {
              // Viper #6187: logical reduction operators 1076-2008 LRM 9.2.2
              $$ = new VhdlOperator ($2, $1, 0) ;
              if (!vhdl_file::IsVhdl2008()) ($$)->Error("this construct is only supported in VHDL 1076-2008") ;
          }
        ;

primary : name { $$=$1 ;}
        | literal { $$=$1 ;}
        | aggregate  { $$=$1 ;}
        | allocator  { $$=$1 ;}
        | qualified_expression  { $$=$1 ; }
        /* | function_call    covered by 'indexed_name' */
        /* | type_conversion   covered by 'indexed_name' */
        /* | VHDL_OPAREN expression VHDL_CPAREN  covered by 'aggregate' */
        ;

literal : /* This is what is remaining of 'literal' after 'name' in
           * 'primary' stipped so much of it
          */
        abstract_literal  { $$ = $1 ; }
        | abstract_literal name /* remaining for physical literals */
          { $$ = new VhdlPhysicalLiteral($1,$2) ; }
        | bit_string_literal  { $$ = $1 ; }
        | character_literal { $$ = $1 ; }
        | VHDL_null { $$ = new VhdlNull() ; }
        ;

aggregate : VHDL_OPAREN element_assoc_list VHDL_CPAREN
         /* Single element association needs to be named.
          * If not, it's a parenthesized expression. Check that here !
         */
          {
             if (($2) && ($2)->Size()==1) {
                VhdlExpression *elem = (VhdlExpression*)($2)->GetFirst() ;
                if (elem && elem->IsAssoc()) {
                    /* Its an aggregate like (others=>'0') */
                    $$ = new VhdlAggregate($2) ;
                } else {
                    /* It's a parenthesized expression */
                    /* Delete the array, return the expression */
                    delete $2 ;
                    elem->IncreaseNesting() ;
                    $$ = elem ;
                }
             } else {
                $$ = new VhdlAggregate($2) ;
             }
           }
        ;

allocator : VHDL_new name
          { $$ = new VhdlAllocator($2) ; }
        | VHDL_new qualified_expression
          { $$ = new VhdlAllocator($2) ; }
        /* covers qualified expression and
          * subtype_indication with index constraint
        */
        /* FIX ME : subtype_indication with range constraint gets s/r conflicts ?? */
        ;

element_assoc_list : element_assoc
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | element_assoc_list VHDL_COMMA element_assoc
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

element_assoc : choices VHDL_ARROW expression
          { $$ = new VhdlElementAssoc($1,$3) ; }
        | expression
        ;

choices : choice
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | choices VHDL_VERTICAL choice
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | choices VHDL_BANG choice
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

choice : simple_expression { $$ = $1 ; }
        | discrete_range_subset1 { $$ = $1 ; }
        | VHDL_others
          { $$ = new VhdlOthers() ; }
        ;

/******************************* Optionals *****************/

opt_id : { $$=0; }
        | id { $$=$1; }
        ;
opt_expression : { $$=0; }
        | expression { $$=$1; }
        ;
opt_signature : { $$=0; }
        | signature { $$=$1; }
        ;
opt_generic_clause : { $$=0; }
        | generic_clause { $$=$1; }
        ;
opt_port_clause : { $$=0; }
        | port_clause { $$=$1; }
        ;
opt_designator : { $$=0; }
        | designator { $$=$1; }
        ;
opt_file_open_info : { $$=0; }
        | file_open_info { $$=$1; }
        ;
opt_delay_mechanism : { $$=0; }
        | delay_mechanism { $$=$1; }
        ;
opt_type_mark_list : { $$=0; }
        | type_mark_list { $$=$1; }
        ;

opt_VHDL_package_body : { $$=0; }
        | VHDL_package VHDL_body { $$=VHDL_body; }
        ;
opt_pure_impure : { $$=0; }
        | VHDL_pure { $$=VHDL_pure; }
        | VHDL_impure { $$=VHDL_impure; }
        ;
opt_subprogram_kind : { $$=0; }
        | VHDL_procedure { $$=VHDL_procedure; }
        | VHDL_function { $$=VHDL_function; }
        ;
opt_signal_kind : { $$=0; }
        | VHDL_bus { $$=VHDL_bus; }
        | VHDL_register { $$=VHDL_register; }
        ;
opt_VHDL_architecture : { $$=0; }
        | VHDL_architecture { $$=VHDL_architecture; }
        ;
opt_VHDL_entity : { $$=0; }
        | VHDL_entity { $$=VHDL_entity; }
        ;
opt_VHDL_package : { $$=0; }
        | VHDL_package { $$=VHDL_package; }
        ;
opt_VHDL_configuration : { $$=0; }
        | VHDL_configuration { $$=VHDL_configuration; }
        ;
opt_VHDL_is : { $$=0; }
        | VHDL_is { $$=VHDL_is; }
        ;
opt_VHDL_BOX : { $$=0; }
        | VHDL_BOX { $$=VHDL_BOX; }
        ;
opt_VHDL_shared : { $$=0; }
        | VHDL_shared { $$=VHDL_shared; }
        ;
opt_VHDL_postponed : { $$=0; }
        | VHDL_postponed { $$=VHDL_postponed; }
        ;
opt_VHDL_guarded : { $$=0; }
        | VHDL_guarded { $$=VHDL_guarded; }
        ;
opt_init_assign :  { $$=0 ; }
        | VHDL_VARASSIGN expression { $$=$2 ; }
        ;

/******************************* lists *****************/

selected_name_list : selected_name
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | selected_name_list VHDL_COMMA selected_name
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

interface_list : interface_decl
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | interface_list VHDL_SEMI opt_comment interface_decl
          {
              // Attach comments after the semicolon to the previous decl if it is in the same line.
              $$ = ($1) ? ($1) : new Array() ;
              // Get the last interface decl item from the Array.
              VhdlInterfaceDecl *last_decl = (VhdlInterfaceDecl *)((($$)->Size()) ? ($$)->GetLast() : 0) ;
              if (last_decl && $3) { // trailing comments
                  // Take any comments accumulated till now.
                  Array *comments_after = vhdl_file::TakeComments() ;
                  // Set the comments to the previous node that is on the same line.
                  Array *comments_before = last_decl->ProcessPostComments($3) ;
                  // Also, set the comments to the next node that is on its line.
                  comments_before = ($4)->ProcessPostComments(comments_before) ;
                  // Append the comment array taken forcefully after the left over comments.
                  if (comments_before) {
                      comments_before->Append(comments_after) ;
                      // Delete this array we have already appended its elements to the other one.
                      delete comments_after ;
                  } else {
                      // We don't want to loose any comments
                      comments_before = comments_after ;
                  }
                  // Store the comment array for future needs.
                  vhdl_file::SetComments(comments_before) ;
              } else {
                  VhdlNode::DeleteComments($3) ; // Delete comments to fix memory leak
              }
              ($$)->InsertLast($4) ;
          }
        /* CHECK ME : cant get a good error rule in here */
        ;

enumeration_literal_list : enumeration_id_def
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | enumeration_literal_list VHDL_COMMA enumeration_id_def
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

physical_unit_decl_list : { $$ = 0 ; }
        | physical_unit_decl_list physical_unit_decl
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error { $$ = 0 ; }
        ;

entity_class_entry_list : entity_class_entry
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | entity_class_entry_list VHDL_COMMA entity_class_entry
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

entity_designator_list : entity_designator
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | entity_designator_list VHDL_COMMA entity_designator
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

name_list : name
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | name_list VHDL_COMMA name
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

discrete_range_list : discrete_range
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | discrete_range_list VHDL_COMMA discrete_range
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

element_decl_list : element_decl
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | element_decl_list element_decl
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
        | error { $$ = 0 ; }
        ;

type_mark_list : type_mark
          { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | type_mark_list VHDL_COMMA type_mark
          { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

element_id_def_list : record_element_id_def
           { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | element_id_def_list VHDL_COMMA record_element_id_def
           { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

library_id_def_list : library_id_def
           { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | library_id_def_list VHDL_COMMA library_id_def
           { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        ;

interface_id_def_list : interface_id_def
           { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | interface_id_def_list VHDL_COMMA interface_id_def
           { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

signal_id_def_list : signal_id_def
           { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | signal_id_def_list VHDL_COMMA signal_id_def
           { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

constant_id_def_list : constant_id_def
           { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | constant_id_def_list VHDL_COMMA constant_id_def
           { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

variable_id_def_list : variable_id_def
           { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | variable_id_def_list VHDL_COMMA variable_id_def
           { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

file_id_def_list : file_id_def
           { $$ = new Array() ; ($$)->InsertLast($1) ; }
        | file_id_def_list VHDL_COMMA file_id_def
           { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
        | error { $$ = 0 ; }
        ;

access_type_def : VHDL_access subtype_indication
          { $$ = new VhdlAccessTypeDef($2) ; }
        ;

/******************************* Literals **********************/

abstract_literal : VHDL_INTEGER
          { $$ = new VhdlInteger($1) ; }
        | VHDL_REAL
          { $$ = new VhdlReal($1) ; }
        ;

physical_literal : name
          { $$ = new VhdlPhysicalLiteral(0, $1) ; }
        | abstract_literal name
          { $$ = new VhdlPhysicalLiteral($1,$2) ; }
        ;

character_literal : VHDL_CHARACTER
          { $$ = new VhdlCharacterLiteral($1) ; }
        ;

string_literal : VHDL_STRING
          { $$ = new VhdlStringLiteral($1) ; }
        ;

bit_string_literal : VHDL_BIT_STRING
          { $$ = new VhdlBitStringLiteral($1) ; }
        ;

/******************************* Names **********************/

name : id  { $$=$1 ; }
        | string_literal  { $$=$1 ; }
          /* Covers operator_symbol for 'name'
           * also covers 'string_literal' for 'literal' in primary
           */
        | selected_name
        | attribute_name
        | indexed_name  /* This also covers sliced_name, function call,
                         * index_constraint, type_conversion and group_constituent_list
                         */
        | external_name  // VHDL-2008
        ;

qualified_expression :
        prefix VHDL_TICK aggregate  /* covers qualified expressions */
          { $$ = new VhdlQualifiedExpression($1,$3) ; }
        ;

attribute_name : prefix VHDL_TICK attribute_designator %prec VHDL_NO_OPAREN
            /* MERGED : optional signature merged into 'prefix' */
          { $$ = new VhdlAttributeName($1,$3,0) ; }
        | prefix VHDL_TICK attribute_designator VHDL_OPAREN assoc_list VHDL_CPAREN
          { // The Attribute index collides with indexed name.
            // This causes a shift-reduce conflict on an VHDL_OPAREN token.
            // This is resolved by setting precedence to stay inside this rule (shift)
            // a bit higher than reducing to indexed name.
            // If it turned out that no argument of this attribute is possible,
            // or there are more than one arguments, then apparently we needed an indexed name after all.
            // Determine that here and create the correct parse tree nodes :
            // VIPER #4305 : Create attribute name only for those attributes which
            // can have one argument
            if (($3) && VhdlAttributeName::IsPredefinedAttributeWithOneArg(($3)->Name()) && ($5) && ($5)->Size()==1) {
                VhdlExpression *expr = (VhdlExpression*) ($5)->GetFirst() ;
                delete $5 ; // Need to delete this array since it doesn't get absorbed!
                $$ = new VhdlAttributeName($1,$3,expr) ;
            } else {
                $$ = new VhdlIndexedName(new VhdlAttributeName($1, $3, 0), $5) ;
            }
          }
        ;

// Viper 6859 : Selected name prefix cannot be signatured name but only simple name
selected_name : simple_prefix VHDL_DOT suffix
          { $$ = new VhdlSelectedName($1,$3) ; }
        ;

indexed_name : prefix VHDL_OPAREN assoc_list VHDL_CPAREN
         /* assoc_list is overkill on indexed name but
          * This also covers sliced_name, function call,
          * index_constraint, type_conversion
          * and group_constituent_list
         */
          { $$ = new VhdlIndexedName($1, $3) ; }
        ;

external_name :
         //external_constant_name
         VHDL_LANGLEQ VHDL_constant external_path_name VHDL_COLON subtype_indication VHDL_RANGLEQ
         { $$ = new VhdlExternalName(VHDL_constant, $3.path_token, $3.hat_count, $3.path, $5) ; }
         // external_signal_name
        | VHDL_LANGLEQ VHDL_signal external_path_name VHDL_COLON subtype_indication VHDL_RANGLEQ
         { $$ = new VhdlExternalName(VHDL_signal, $3.path_token, $3.hat_count, $3.path, $5) ; }
         // external_variable_name
        | VHDL_LANGLEQ VHDL_variable external_path_name VHDL_COLON subtype_indication VHDL_RANGLEQ
         { $$ = new VhdlExternalName(VHDL_variable, $3.path_token, $3.hat_count, $3.path, $5) ; }
        ;

external_path_name : package_pathname
        | absolute_pathname
        | relative_pathName
        ;

package_pathname : VHDL_AT name
          { $$.path_token = VHDL_AT ; $$.hat_count = 0; $$.path = $2 ; }
        ;

absolute_pathname : VHDL_DOT name
          { $$.path_token = VHDL_DOT ; $$.hat_count = 0; $$.path = $2 ; }
        ;

relative_pathName : opt_accent_list name
          { $$.path_token = ($1) ? VHDL_ACCENT: 0 ; $$.hat_count = $1 ? $1->Size(): 0; $$.path = $2 ; delete $1 ; }
        ;

opt_accent_list : { $$ = 0 ; }
        | accent_list VHDL_DOT
          { $$ = $1 ; }
        ;

accent_list :VHDL_ACCENT
          { $$ = new Array() ; $$->InsertLast(0) ; }
        | accent_list VHDL_DOT VHDL_ACCENT
          { $$ = $1 ? $1 : new Array() ; $$->InsertLast(0) ; }
        ;

designator : id { $$ = $1 ; }
        | operator_symbol { $$ = $1 ; }
        | character_literal { $$ = $1 ; }
        ;

attribute_designator : id
        | VHDL_range /* the keyword that is also attribute */
          { $$ = new VhdlIdRef(Strings::save("range")) ; }
        | VHDL_subtype /* VIPER #7522: vhdl_2008 attribute: the keyword that is also attribute */
          { $$ = new VhdlIdRef(Strings::save("subtype")) ; }
        ;

entity_designator : name opt_signature
          { $$ = ($2) ? new VhdlSignaturedName($1,$2) : ($1) ; }
          /* These following rules added to simplify other rules
           * that designate objects :
           *    - entity_spec
           *    - component_spec
           *    - guarded_signal_spec
          */
        | VHDL_others { $$ = new VhdlOthers() ; }
        | VHDL_all    { $$ = new VhdlAll() ; }
        ;

prefix : simple_prefix
         /* MERGED : function_call is in indexed_name of name */
         /* opt_signature added here for 'attribute_name' prefix. */
          { $$ = $1 ; }
        | signatured_prefix { $$ = $1 ; }
        ;

simple_prefix : name { $$ = $1 ; }
        ;

signatured_prefix : name signature
          { $$ = ($2) ? new VhdlSignaturedName($1,$2) : $1 ; }
        ;

suffix : id { $$ = $1 ; }
        | character_literal { $$ = $1 ; }
        | operator_symbol { $$ = $1 ; }
        | VHDL_all
          { $$ = new VhdlAll() ; }
        ;

operator_symbol : string_literal
        ;

type_mark : name
        ;

entity_name : id
          { $$ = $1 ; }
        | id VHDL_DOT id
          {
              // entity name for architecture or configuration.
              // $1 should denote the name of present work library, or "work"
              if (!Strings::compare($1->Name(),"work") &&
                  !Strings::compare($1->Name(),vhdl_file::GetWorkLib()->Name())) {
                 $1->Error("entity should be in present work library") ;
              }
              delete $1 ;
              $$ = $3 ;
          }
        ;

id : VHDL_ID
          { $$ = new VhdlIdRef($1) ; }
        ;

uninstantiated_package_name : name
        ;

uninstantiated_subprog_name : id { $$ = $1 ; }
        | VHDL_STRING { $$ = new VhdlStringLiteral($1) ; }
        ;

physical_unit_id_def : VHDL_ID
        { $$= new VhdlPhysicalUnitId($1) ;vhdl_declare($$) ; }
        ;
record_element_id_def : VHDL_ID
        { $$= new VhdlElementId($1) ;vhdl_declare($$) ; }
        ;
library_id_def : VHDL_ID
        { $$= new VhdlLibraryId($1) ; vhdl_declare($$) ; }
        ;
interface_id_def : VHDL_ID
        // VHDL-2008 LRM section 6.5.6.1: One generic is visible to 2 nd generic decl
        { $$= new VhdlInterfaceId($1) ; vhdl_declare($$) ; if (in_generic_clause && $$ && VhdlNode::IsVhdl2008()) $$->SetObjectKind(VHDL_generic) ;}
        ;
subtype_id_def : VHDL_ID
        { $$= new VhdlSubtypeId($1) ; vhdl_declare($$) ; }
        ;
type_id_def : VHDL_ID
        {
            $$ = new VhdlTypeId($1) ;
            VhdlIdDef *exist = VhdlTreeNode::_present_scope->FindSingleObjectLocal($$->Name()) ;
            if (exist && exist->IsProtected()) {
                char *name = Strings::save($1) ;
                delete $$ ;
                $$ = new VhdlProtectedTypeBodyId(name) ;
            } else {
                vhdl_declare($$) ;
            }
        }
        ;
generic_type_id_def : VHDL_ID
        {
            $$ = new VhdlGenericTypeId($1) ;
            VhdlIdDef *exist = VhdlTreeNode::_present_scope->FindSingleObjectLocal($$->Name()) ;
            if (exist && exist->IsProtected()) {
                char *name = Strings::save($1) ;
                delete $$ ;
                $$ = new VhdlProtectedTypeBodyId(name) ;
            } else {
                vhdl_declare($$) ;
            }
        }
        ;
constant_id_def: VHDL_ID
        { $$= new VhdlConstantId($1) ; vhdl_declare($$) ; }
        ;
signal_id_def : VHDL_ID
        //CARBON_BEGIN
        //{ $$ = new VhdlSignalDecl($2,$4,$5,$6) ; }  // OldVerificVersion
        { $$= new VhdlSignalId($1) ; vhdl_declare($$) ; gLastDeclVhdlId = $$ ; }
        //CARBON_END
        ;
variable_id_def: VHDL_ID
        { $$= new VhdlVariableId($1) ; vhdl_declare($$) ; }
        ;
file_id_def: VHDL_ID
        { $$= new VhdlFileId($1) ; vhdl_declare($$) ; }
        ;
component_id_def: VHDL_ID
        { $$= new VhdlComponentId($1) ; vhdl_declare($$) ; }
        ;
attribute_id_def: VHDL_ID
        { $$= new VhdlAttributeId($1) ; vhdl_declare($$) ; }
        ;
group_template_id_def: VHDL_ID
        { $$=new VhdlGroupTemplateId($1) ; vhdl_declare($$) ; }
        ;
group_id_def: VHDL_ID
        { $$=new VhdlGroupId($1) ; vhdl_declare($$) ; }
        ;
subprogram_id_def :
          VHDL_ID
          { $$= new VhdlSubprogramId($1) ; gLastDeclVhdlId = $$ ; vhdl_declare($$) ; } /* CARBON_CHANGED */
        | VHDL_STRING
          { $$ = new VhdlSubprogramId($1) ; gLastDeclVhdlId = $$ ; vhdl_declare($$) ; } /* CARBON_CHANGED */
        ;
procedure_id_def : // VIPER #3489 : only identifier is allowed as procedure name
          VHDL_ID
          { $$= new VhdlSubprogramId($1) ; gLastDeclVhdlId = $$ ; vhdl_declare($$) ; } /* CARBON_CHANGED */
        ;
enumeration_id_def :
          VHDL_ID
          { $$= new VhdlEnumerationId($1) ; vhdl_declare($$) ; }
        | VHDL_CHARACTER
          { $$= new VhdlEnumerationId($1) ; vhdl_declare($$) ; }
        ;
alias_id_def:
          VHDL_ID
          { $$= new VhdlAliasId($1) ; } // cannot declare an alias before we know what it is.
        | VHDL_STRING
          { $$ = new VhdlAliasId($1) ; }
        | VHDL_CHARACTER
          { $$= new VhdlAliasId($1) ; }
        ;

label : VHDL_ID
           /* Label : implicit declaration ? */
          { $$ = new VhdlLabelId($1) ; vhdl_declare_label($$) ; last_label = $$ ; }
        ;

/* Design Units ; Create identifiers. Put in scope (declare) when we are supposed to (after word IS in the declaration) */
entity_id_def :
          VHDL_ID
          //CARBON_BEGIN
          //{ $$= new VhdlEntityId($1) ; } // OldVerificVersion
          { $$= new VhdlEntityId($1) ; gLastDeclVhdlId = $$ ; }
          //CARBON_END
        ;
architecture_id_def :
          VHDL_ID
          { $$= new VhdlArchitectureId($1) ; }
        ;
configuration_id_def :
          VHDL_ID
          { $$= new VhdlConfigurationId($1) ; }
        ;
package_id_def :
          VHDL_ID
          { $$= new VhdlPackageId($1) ; }
        ;
package_body_id_def :
          VHDL_ID
          { $$= new VhdlPackageBodyId($1) ;  }
        ;

/********************* Pick up comment from flex **********************/

 /* pre_comment : no need to force bison to read next token. just pick up with TakeComments() */
pre_comment :
       { $$ = vhdl_file::TakeComments() ; }
    ;

 /* opt_comment : read comments after a rule completes : force bison to read next token, using a non-existing token request */
opt_comment :
          { $$ = vhdl_file::TakeComments() ; } %prec VHDL_NO_COMMENT
        | VHDL_OPT_COMMENT
          { $$ = 0 ; VERIFIC_ASSERT(0) ; /* force bison to read next token */ }
        ;

%%

void
vhdl_file::StartYacc()
{
    VhdlTreeNode::_present_scope = 0 ; /* Just in case */
    first_line = 0 ;
    first_column = 0 ;
    last_line = 0 ;
    last_column = 0 ;
    context_clause = 0 ;
    inside_design_unit = 0 ;
    in_generic_clause = 0 ;
    //CARBON_BEGIN
    delete last_label ; last_label = 0 ; // VIPER #7714: Reset all labels
    //CARBON_END

// Temporary storage for actual analysis mode; currently needed for embedded psl
    rhs_0_initialized  = 0 ; // VIPER #6608: Start with Rhs[0] as uninitialized
}

linefile_type
vhdl_file::GetRuleLineFile()
{
    // Get the linefile info from the last 'rule' (as opposed to last token)

    // get it for the last token (since that includes the filename-id, which is not available here in bison)
    linefile_type result = GetLineFile() ;
    // VIPER #4002 side effect: Don't try to set line & file info if GetLineFile returns null:
    if (!result) return result ;
    // and adjust it to the info in the last rule :

    // Exact range info from the last rule is available in the bison-generated yylloc variable.
    // However, that one is local to routine 'yyparse', so we export it via
    // static variables, set in the overridden macro YYLLOC_DEFAULT.

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Use all location info to initialize the result to latest bison rule that was executed.
    // GetLineFile() already made a copy.
    result->SetLeftCol(first_column) ;
    result->SetLeftLine(first_line) ;
    result->SetRightCol(last_column) ;
    result->SetRightLine(last_line) ;
#else
    // Use only line info from the current (latest) bison rule executed:
    // Decide here which line number (first or last in the rule) should be set as the rule's line number..
#ifdef VHDL_USE_STARTING_LINEFILE_INFO
    result = LineFile::SetLineNo(result, first_line) ;
#else
    result = LineFile::SetLineNo(result, last_line) ;
#endif

#endif
    return result ;
}

void
vhdl_file::EndYacc()
{
    cleanup_context_clause() ;
    //CARBON_BEGIN
    delete last_label ; last_label = 0 ; // VIPER #7714: Reset all labels
    //CARBON_END
}

int
vhdl_file::Parse()
{
#if YYDEBUG
    yydebug = 1 ;
#endif
    inside_design_unit = 0 ;
    return yyparse() ;
}

