/*
 *
 * [ File Version : 1.315 - 2014/03/21 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlValue_Elab.h" // Compile flags are defined within here

#include <stdio.h>   // for sprintf
#include <math.h>    // for floor, ceil

#include "Array.h"
#include "Message.h"
#include "Strings.h"
#include "RuntimeFlags.h"
#include "VhdlName.h"
#include "Set.h"

#include "vhdl_tokens.h"
#include "VhdlIdDef.h"
#include "vhdl_file.h"
#include "VhdlDataFlow_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

 ////////////////////////////////////////////////////////////////////
 // Net (4-value constant)
 ////////////////////////////////////////////////////////////////////
Net::Net(const char *name, linefile_type /*linefile*/)
{
    if (name) _name = Strings::save(name) ;
}
Net::~Net()
{
    Strings::free(_name) ;
}
const char *
Net::Name()
{
    return _name ;
}
unsigned
Net::IsConstant() const
{
    if ((this == VhdlNode::Gnd()) || (this == VhdlNode::Pwr()) || (this == VhdlNode::X()) || (this == VhdlNode::Z())) return 1 ;
    return 0 ;
}
unsigned
Net::IsPwr() const
{
    return this == VhdlNode::Pwr() ;
}
unsigned
Net::IsGnd() const
{
    return this == VhdlNode::Gnd() ;
}
unsigned
Net::IsX() const
{
    return this == VhdlNode::X() ;
}
unsigned
Net::IsZ() const
{
    return this == VhdlNode::Z() ;
}

Net  *
Net::InvNet() const
{
    if (IsGnd()) return VhdlNode::Pwr() ;
    if (IsPwr()) return VhdlNode::Gnd() ;
    if (IsX() || IsZ()) return const_cast<Net*>(this) ;
    return 0 ;
}

const unsigned std_ulogic_matching_eq_table[9][9] =
                                         { {0, 0, 0, 0, 0, 0, 0, 0, 3},
                                           {0, 1, 1, 1, 1, 1, 1, 1, 3},
                                           {0, 1, 3, 2, 1, 1, 3, 2, 3},
                                           {0, 1, 2, 3, 1, 1, 2, 3, 3},
                                           {0, 1, 1, 1, 1, 1, 1, 1, 3},
                                           {0, 1, 1, 1, 1, 1, 1, 1, 3},
                                           {0, 1, 3, 2, 1, 1, 3, 2, 3},
                                           {0, 1, 2, 3, 1, 1, 2, 3, 3},
                                           {3, 3, 3, 3, 3, 3, 3, 3, 3}
                                         } ;

const unsigned std_ulogic_matching_lthan_table[9][9] =
                                         { {0, 0, 0, 0, 0, 0, 0, 0, 1},
                                           {0, 1, 1, 1, 1, 1, 1, 1, 1},
                                           {0, 1, 2, 3, 1, 1, 2, 3, 1},
                                           {0, 1, 2, 2, 1, 1, 2, 2, 1},
                                           {0, 1, 1, 1, 1, 1, 1, 1, 1},
                                           {0, 1, 1, 1, 1, 1, 1, 1, 1},
                                           {0, 1, 2, 3, 1, 1, 2, 3, 1},
                                           {0, 1, 2, 2, 1, 1, 2, 2, 1},
                                           {1, 1, 1, 1, 1, 1, 1, 1, 1}
                                         } ;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VhdlValue::VhdlValue() : VhdlNode() {}
VhdlValue::~VhdlValue() {}

VhdlInt::VhdlInt(verific_int64 value)
  : VhdlValue(),
    _value(value)
{
}
VhdlInt::~VhdlInt() {}

VhdlPhysicalValue::VhdlPhysicalValue(verific_int64 value)
  : VhdlInt(value)
{
}
VhdlPhysicalValue::~VhdlPhysicalValue() {}

VhdlDouble::VhdlDouble(double value)
  : VhdlValue(),
    _value(value)
{
}
VhdlDouble::~VhdlDouble() {}

VhdlEnum::VhdlEnum(VhdlIdDef *enum_id)
  : VhdlValue(),
    _enum_id(enum_id)
{
    // I always want enumid to be there. Don't create Enum without it.
    VERIFIC_ASSERT(_enum_id) ;
}
VhdlEnum::~VhdlEnum() {}

VhdlNonconst::VhdlNonconst(Array *nets, unsigned is_signed, unsigned user_encoded)
  : VhdlValue(),
    _nets(nets),
    _is_signed(is_signed),
    _user_encoded(user_encoded)
{
    VERIFIC_ASSERT(Gnd() && Pwr()) ; // Cannot create nonconst values without having (constant) nets around.
    VERIFIC_ASSERT(_nets) ;
}
VhdlNonconst::~VhdlNonconst()
{
    delete _nets ;
}

VhdlNonconstBit::VhdlNonconstBit(Net *net)
  : VhdlValue(),
    _net(net)
{
    VERIFIC_ASSERT(Gnd() && Pwr()) ; // Cannot create nonconst values without having (constant) nets around.
    VERIFIC_ASSERT(_net) ;
}
VhdlNonconstBit::~VhdlNonconstBit() {}

VhdlCompositeValue::VhdlCompositeValue(Array *values, VhdlValue *default_elem_val, unsigned is_signed, VhdlConstraint *range)
  : VhdlValue(),
    _values(values),
    _default_elem_val(default_elem_val),
    _range(range),
    _is_signed(is_signed)
#ifdef VHDL_PRESERVE_LITERALS
    , _base_specifier(0)         // VIPER #8308 : Preserve the base_specifier in the composite value
#endif
{
    if (!_values) _values = new Array() ;
    VERIFIC_ASSERT(_values) ;
}
VhdlCompositeValue::~VhdlCompositeValue()
{
    unsigned i ;
    VhdlValue *val ;
    FOREACH_ARRAY_ITEM(_values, i, val) delete val ;
    delete _values ;
    delete _default_elem_val ;
    delete _range ;
}

VhdlAccessValue::VhdlAccessValue(VhdlValue *value, VhdlConstraint *constraint)
  : VhdlValue(),
    _constraint(constraint),  // Viper : 3764/3771 - access_value carries (owns) the constraint information
    _value(value),
    _is_recursion(0)
{
    VhdlNode::SetInnerAccessValue(value) ; // (VIPER #4464, 7292): remember the value for later cleanup
}

VhdlAccessValue::~VhdlAccessValue()
{
    _value = 0 ;
    delete _constraint ;  // Viper : 3764/3771 - access_value carries (owns) the constraint information
}

VhdlDualPortValue::VhdlDualPortValue(Array *values)
  : VhdlCompositeValue(values)
{
    // Dual port value needs 9 fields :
    VERIFIC_ASSERT(_values && _values->Size()==9) ;
}
VhdlDualPortValue::~VhdlDualPortValue() { /*composite destructor does all work*/ }

VhdlEvent::VhdlEvent(Net *net, unsigned is_event)
  : VhdlNonconstBit(net),
    _is_event(is_event),
    _enable(0)
{
    // Created as event ( not stable )
}
VhdlEvent::~VhdlEvent() {}

VhdlEdge::VhdlEdge(Net *net)
  : VhdlNonconstBit(net),
  _enable(0)
{
    // Created as edge on the net
}
VhdlEdge::~VhdlEdge() {}

/***************************************************************/
// Copy
/***************************************************************/

VhdlValue *VhdlValue::CopyExplicit() const       { return Copy() ; }
VhdlValue *VhdlAccessValue::CopyExplicit() const { return new VhdlAccessValue(_value ? _value->CopyExplicit() : 0, _constraint ? _constraint->Copy() : 0) ; } // Viper : 3764/3771 - access_value carries (owns) the constraint information

VhdlValue *VhdlInt::Copy() const            { return new VhdlInt(_value) ; }
VhdlValue *VhdlPhysicalValue::Copy() const  { return new VhdlPhysicalValue(_value) ; }
VhdlValue *VhdlDouble::Copy() const         { return new VhdlDouble(_value) ; }
VhdlValue *VhdlAccessValue::Copy() const    { return new VhdlAccessValue(_value, _constraint ? _constraint->Copy() : 0) ; } // Viper : 3764/3771 - access_value carries (owns) the constraint information
VhdlValue *VhdlEnum::Copy() const           { VERIFIC_ASSERT(_enum_id) ; return new VhdlEnum(_enum_id) ; }
VhdlValue *VhdlNonconst::Copy() const       { VERIFIC_ASSERT(_nets) ; return new VhdlNonconst(new Array(*_nets) , _is_signed, _user_encoded) ; }
VhdlValue *VhdlNonconstBit::Copy() const    { VERIFIC_ASSERT(_net) ; return new VhdlNonconstBit(_net) ; }
VhdlValue *VhdlCompositeValue::Copy() const
{
    VERIFIC_ASSERT(_values) ;

    Array *values = new Array(_values->Size()) ;
    unsigned i ;
    VhdlValue *val ;
    FOREACH_ARRAY_ITEM(_values, i, val) values->InsertLast((val) ? val->Copy() : 0) ;
    VhdlValue *default_elem_val = (_default_elem_val) ? _default_elem_val->Copy() : 0 ;

    VhdlConstraint *range = _range ? _range->Copy() : 0 ;
    VhdlCompositeValue* ret_val = new VhdlCompositeValue(values, default_elem_val, _is_signed, range) ;
#ifdef VHDL_PRESERVE_LITERALS
    ret_val->SetBaseSpecifier(_base_specifier) ;
#endif
    return ret_val ;
}
VhdlValue *VhdlDualPortValue::Copy() const
{
    VERIFIC_ASSERT(_values) ;

    Array *values = new Array(_values->Size()) ;
    unsigned i ;
    VhdlValue *val ;
    FOREACH_ARRAY_ITEM(_values, i, val) values->InsertLast((val) ? val->Copy() : 0) ;
    return new VhdlDualPortValue(values) ;
}
VhdlValue *VhdlEvent::Copy() const
{
    VhdlValue *r = new VhdlEvent(_net,_is_event) ;
    r->SetEnableNet(_enable) ; // Set clock enable in event value
    return r ;
}
VhdlValue *VhdlEdge::Copy() const
{
    VhdlValue *r = new VhdlEdge(_net) ;
    r->SetEnableNet(_enable) ; // Set clock enable in event value
    return r ;
}

/**********************************************/
// Special Value Tests
/**********************************************/

unsigned VhdlValue::IsEvent() const         { return 0 ; }
unsigned VhdlEvent::IsEvent() const         { return _is_event ; }

unsigned VhdlValue::IsStable() const        { return 0 ; }
unsigned VhdlEvent::IsStable() const        { return (_is_event) ? 0 : 1 ; }

unsigned VhdlValue::IsEdge() const          { return 0 ; }
unsigned VhdlEdge::IsEdge() const           { return 1 ; }

/**********************************************/
// Tests
/**********************************************/

unsigned VhdlInt::IsConstant() const         { return 1 ; }
unsigned VhdlDouble::IsConstant() const      { return 1 ; }
unsigned VhdlEnum::IsConstant() const        { return 1 ; }
unsigned VhdlAccessValue::IsConstant() const { return _value ? _value->IsConstant() : 1 ; } // Viper #3778
unsigned VhdlNonconst::IsConstant() const
{
    // nonconstant bits are likely to be on LSB side. So start there
    unsigned i ;
    Net *n ;
    FOREACH_ARRAY_ITEM_BACK(_nets, i, n) {
        if (!ConstNet(n)) return 0 ;
    }
    return 1 ;
}
unsigned VhdlNonconstBit::IsConstant() const { return ConstNet(_net) ; }
unsigned VhdlCompositeValue::IsConstant() const
{
    VERIFIC_ASSERT(_values) ;

    unsigned default_is_constant = (_default_elem_val) ? _default_elem_val->IsConstant() : 1 ;
    // Composite is constant if all elements are constant
    unsigned i ;
    VhdlValue *val ;
    FOREACH_ARRAY_ITEM(_values, i, val) {
        if (!val) {
            if (!default_is_constant) return 0 ;
            if (IsStaticElab() && !_default_elem_val) return 0 ; // Viper #4383: For static elab null has special meaning
            continue ; // assumed constant
        }
        if (!val->IsConstant()) return 0 ;
    }
    return 1 ;
}

unsigned VhdlValue::IsTrue() const                  { return 0 ; }
unsigned VhdlAccessValue::IsTrue() const            { return _value ? _value->IsTrue() : 0 ; }
unsigned VhdlInt::IsTrue() const                    { return (_value == 1) ? 1 : 0 ; }
unsigned VhdlEnum::IsTrue() const                   { VERIFIC_ASSERT(_enum_id) ; return (_enum_id->GetBitEncodingForElab()=='1') ? 1 : 0 ; } // only works for bit-encoded literals
unsigned VhdlNonconst::IsTrue() const               { VERIFIC_ASSERT(_nets) ; return (Size()==1 && (_nets->At(0)==Pwr())) ? 1 : 0 ; }
unsigned VhdlNonconstBit::IsTrue() const            { return (_net == Pwr()) ? 1 : 0 ; }

void VhdlInt::SetSigned()                           { } // directive does not make a difference
void VhdlDouble::SetSigned()                        { } // directive does not make a difference
void VhdlEnum::SetSigned()                          { } // directive does not make a difference
void VhdlNonconst::SetSigned()                      { _is_signed = 1 ; }
void VhdlNonconstBit::SetSigned()                   { } // directive does not make a difference
void VhdlCompositeValue::SetSigned()                { _is_signed = 1 ; }
void VhdlAccessValue::SetSigned()                   { if (_value) _value->SetSigned() ; }

unsigned VhdlInt::IsSigned() const                  { return (_value<0) ? 1 : 0 ; }
unsigned VhdlDouble::IsSigned() const               { return (_value<0.0) ? 1 : 0 ; }
unsigned VhdlEnum::IsSigned() const                 { return 0 ; }
unsigned VhdlNonconst::IsSigned() const             { return _is_signed ; }
unsigned VhdlNonconstBit::IsSigned() const          { return 0 ; }
unsigned VhdlCompositeValue::IsSigned() const       { return _is_signed ; }
unsigned VhdlAccessValue::IsSigned() const          { return _value ? _value->IsSigned() : 0 ; }

unsigned VhdlValue::IsComposite() const             { return 0 ; }
unsigned VhdlCompositeValue::IsComposite() const    { return 1 ; }

unsigned VhdlValue::IsAccessValue() const           { return 0 ; }
unsigned VhdlAccessValue::IsAccessValue() const     { return 1 ; }

unsigned VhdlValue::IsDualPortValue() const         { return 0 ; }
unsigned VhdlDualPortValue::IsDualPortValue() const { return 1 ; }

// Position number of a value
verific_int64 VhdlValue::Position() const                     { return Integer() ; } // nonconst as constant
// LRM 3.1.2 : position number for integer value is the value itself !
verific_int64 VhdlInt::Position() const                       { return _value ; }
// LRM 3.1.1 : position number of enum value : position of its identifier in its type
verific_int64 VhdlEnum::Position() const                      { VERIFIC_ASSERT(_enum_id) ; return (verific_int64)_enum_id->Position() ; }

verific_int64 VhdlAccessValue::Position() const               { return _value ? _value->Position() : 0 ; }

// Type of a value. We only have that for constant enum values
VhdlIdDef *VhdlValue::GetType() const               { return 0 ; }
VhdlIdDef *VhdlAccessValue::GetType() const         { return _value ? _value->GetType() : 0 ; }
VhdlIdDef *VhdlEnum::GetType() const                { VERIFIC_ASSERT(_enum_id) ; return _enum_id->Type() ; }

#ifdef DB_INFER_WIDE_OPERATORS
// Composite value of type 'bit vector' or 'std logic vector' can be
// considered as an array of bits. This routine checks the size of
// its each element. If size of each element is '1', 1 is returned.
unsigned VhdlValue::IsBitVector() const             { return 0 ; }
unsigned VhdlAccessValue::IsBitVector() const       { return _value ? _value->IsBitVector() : 0 ; }
unsigned VhdlCompositeValue::IsBitVector() const
{
    VERIFIC_ASSERT(_values) ;

    unsigned default_is_bit = (_default_elem_val) ? _default_elem_val->IsBit() : 1 ; // assume 'inconclusive' if no default given

    // Have to check all elements before we know if this is a real bit-vector,
    // since this is not guaranteed to be an 'array' type value.
    // And records can have different fields...
    VhdlValue *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            if (!default_is_bit) return 0 ;
            continue ; // inconclusive
        }
        if (!elem->IsBit()) return 0 ;
    }
    return 1 ;
}
#endif // DB_INFER_WIDE_OPERATORS

// Physical value test:
unsigned VhdlValue::IsPhysicalValue() const         { return 0 ; }
unsigned VhdlPhysicalValue::IsPhysicalValue() const { return 1 ; }

/***************************************************************/
// Check if value has a VHDL meta-value (X or Z (not '0','1'))
/***************************************************************/

unsigned VhdlInt::HasX() const                      { return 0 ; }
unsigned VhdlDouble::HasX() const                   { return 0 ; }
unsigned VhdlAccessValue::HasX() const              { return _value ? _value->HasX() : 0 ; }

unsigned VhdlEnum::HasX() const
{
    VERIFIC_ASSERT(_enum_id) ;

    // Enum literal has meta-value if it is 'X' or 'Z'
    switch (_enum_id->GetBitEncodingForElab()) {
    case 'X' : return 1 ;
    case 'Z' : return 1 ;
    case '0' : return 0 ;
    case '1' : return 0 ;
    default :  break ;
    }
    // In a multi-bit encoded scalar, an X value MUST come from
    // user-encoding. That is NOT considered a meta-value.
    // It came from the user, not from VHDL
    return 0 ;
}

unsigned VhdlNonconst::HasX() const
{
    // In a multi-bit encoded scalar, an X value MUST come from
    // user-encoding. That is NOT considered a meta-value.
    // It has specific meaning for sythesis, but not for VHDL
    // RD: 11/05: X/Z can also come into VhdlNonconst via ToNonconst() from an array value (VIPER 2272).
    // So check all bits any way :
    unsigned i ;
    Net *net ;
    FOREACH_ARRAY_ITEM(_nets, i, net) {
        if (net==X() || net==Z()) return 1 ;
    }
    return 0 ;
}

unsigned VhdlNonconstBit::HasX() const
{
    VERIFIC_ASSERT(_net) ;
    return (_net==X() || _net==Z()) ? 1 : 0 ;
}

unsigned VhdlCompositeValue::HasX() const
{
    VERIFIC_ASSERT(_values) ;

    unsigned default_has_x = (_default_elem_val) ? _default_elem_val->HasX() : 0 ; // assume 'inconclusive' if no default given.

    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            if (default_has_x) return 1 ;
            continue ; // inconclusive
        }
        if (elem->HasX()) return 1 ;
    }
    return 0 ;
}

// Meta value BEGIN
/***************************************************************/
// Check if value has a VHDL meta-value (L, H, X or Z (not '0','1'))
/***************************************************************/

unsigned VhdlInt::IsMetalogical() const                      { return 0 ; }
unsigned VhdlDouble::IsMetalogical() const                   { return 0 ; }
unsigned VhdlAccessValue::IsMetalogical() const
{
    return _value ? _value->IsMetalogical() : 0 ;
}

unsigned VhdlEnum::IsMetalogical() const
{
    VERIFIC_ASSERT(_enum_id) ;

    // Enum literal has meta-value if it is NOT the specific '0' or '1' value :
    const char bit_encoding = _enum_id->GetBitEncodingForElab() ;

    if (!bit_encoding) return 0 ; // has no bit encoding so not a meta value

    if (bit_encoding!='0' && bit_encoding!='1') {
        return 1 ; // it's X or Z or so : meta value
    }

    // VIPER 4285 : two-valued enumeration types are never metalogical :
    // if (_enum_id->Type() && _enum_id->Type()->NumOfEnums()) return 0 ;

    // Alternative for 4285 fix : simply remove special code below (and interpret all values that are NOT encoded with a '0' or '1' as metalogical).
#if 0
    // Also, if the enum id is named 'L' or 'H' or anything else not '0' and not '1',
    // then we also consider it a meta value :
    const char *enum_id_name = _enum_id->Name() ;
    if (!enum_id_name) return 0 ;

    if ( (*enum_id_name == '\'') && (*(enum_id_name+1)!='0') && (*(enum_id_name+1)!='1') ) {
        // It has to be a character literal value to be a meta value.
        return 1 ; // it's L or H or so : meta value
    }
#endif

    return 0 ; // not a meta-value
}

unsigned VhdlNonconst::IsMetalogical() const
{
    // In a multi-bit encoded scalar, an X value MUST come from
    // user-encoding. That is NOT considered a meta-value.
    // It has specific meaning for sythesis, but not for VHDL
    // RD: 11/05: X/Z can also come into VhdlNonconst via ToNonconst() from an array value (VIPER 2272).
    // So check all bits any way :
    unsigned i ;
    Net *net ;
    FOREACH_ARRAY_ITEM(_nets, i, net) {
        if (net==X() || net==Z()) return 1 ;
    }
    return 0 ;
}

unsigned VhdlNonconstBit::IsMetalogical() const
{
    VERIFIC_ASSERT(_net) ;
    return (_net==X() || _net==Z()) ? 1 : 0 ;
}

unsigned VhdlCompositeValue::IsMetalogical() const
{
    VERIFIC_ASSERT(_values) ;

    unsigned default_is_meta = (_default_elem_val) ? _default_elem_val->IsMetalogical() : 0 ; // assume 'inconclusive' if no default given.

    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            // no element value here. Default applies :
            if (default_is_meta) return 1 ;
            continue ; // inconclusive
        }
        if (elem->IsMetalogical()) return 1 ;
    }
    return 0 ;
}
// Meta value END

/**********************************************/
// check whether it is a metalogical value with DC.
/**********************************************/
// Meta value as only "_" i.e. dont_care.
unsigned VhdlAccessValue::IsMetalogicalWithDC() const
{
    return _value ? _value->IsMetalogicalWithDC() : 0 ;
}

unsigned VhdlEnum::IsMetalogicalWithDC() const
{
    VERIFIC_ASSERT(_enum_id) ;
    // Enum literal has meta-value if it is NOT the specific '0' or '1' value :
    const char* image = _enum_id->Name() ;
    if (Strings::compare(image, "'-'")) {
        return 1 ;
    }
    return 0 ; // not a meta-value with dont_care
}

unsigned VhdlCompositeValue::IsMetalogicalWithDC() const
{
    VERIFIC_ASSERT(_values) ;

    unsigned default_is_meta = (_default_elem_val) ? _default_elem_val->IsMetalogicalWithDC() : 0 ; // assume 'inconclusive' if no default given.

    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            // no element value here. Default applies :
            if (default_is_meta) return 1 ;
            continue ; // inconclusive
        }
        if (elem->IsMetalogicalWithDC()) return 1 ;
    }
    return 0 ;
}

/**********************************************/
// Return the number of values represented by this meta value
/**********************************************/
// For Example -1 will return 2.
unsigned VhdlAccessValue::RepresentCount() const
{
    if (_value) return (_value->RepresentCount()) ;
    return 1 ;
}

unsigned VhdlEnum::RepresentCount() const
{
    if (IsMetalogicalWithDC()) return 2 ;
    return 1 ;
}
// Meta value as only "_" i.e. dont_care.
unsigned VhdlCompositeValue::RepresentCount() const
{
    VERIFIC_ASSERT(_values) ;

    if (!IsMetalogicalWithDC()) return 1 ;

    unsigned default_is_meta = (_default_elem_val) ? _default_elem_val->IsMetalogicalWithDC() : 0 ; // assume 'inconclusive' if no default given.

    unsigned count = 1 ;
    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            // no element value here. Default applies :
            if (default_is_meta) count *= 2 ;
            continue ; // inconclusive
        }
        if (elem->IsMetalogicalWithDC())  count *= 2 ;
    }
    return count ;
}

/**********************************************/
// Return all the values represented by this meta value
/**********************************************/
void VhdlAccessValue::PopulateValues(Array* values) const
{
    if(_value) _value->PopulateValues(values) ;
}

// Viper 7157: Meta value as only "_" i.e. dont_care. Populate values
// array by filling the dont_cares. For eg -1 is filled as 01 and 11.
void VhdlCompositeValue::PopulateValues(Array* values) const
{
    VERIFIC_ASSERT(_values) ;

    if (!IsMetalogical()) values->InsertLast(Copy()) ;

    if (!IsMetalogicalWithDC()) return ;

    unsigned default_is_meta = (_default_elem_val) ? _default_elem_val->IsMetalogicalWithDC() : 0 ; // assume 'inconclusive' if no default given.
    unsigned i ;
    VhdlValue *elem ;
    VhdlValue *zero_val = Copy() ;
    VhdlValue *one_val = Copy() ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            // no element value here. Default applies :
            if (default_is_meta) {
                VhdlIdDef *id = _default_elem_val ? _default_elem_val->GetEnumId() : 0 ;
                VhdlIdDef *type = id ? id->Type() : 0 ;
                VhdlIdDef *zero_enum_id = type ? type->GetEnumWithBitEncoding('0') : 0 ;
                VhdlIdDef *one_enum_id =  type ? type->GetEnumWithBitEncoding('1') : 0 ;
                if (zero_enum_id && one_enum_id) {
                    VhdlEnum *zero_enum = new VhdlEnum(zero_enum_id) ;
                    VhdlEnum *one_enum = new VhdlEnum(one_enum_id) ;
                    zero_val->SetValueAt(i, zero_enum) ;
                    one_val->SetValueAt(i, one_enum) ;
                    zero_val->PopulateValues(values) ;
                    one_val->PopulateValues(values) ;
                }
                break ;
            }
        }
        if (!elem) continue ;

        if (elem->IsMetalogicalWithDC()) {
            VhdlIdDef *id = elem->GetEnumId() ;
            VhdlIdDef *type = id ? id->Type() : 0 ;
            VhdlIdDef *zero_enum_id = type ? type->GetEnumWithBitEncoding('0') : 0 ;
            VhdlIdDef *one_enum_id =  type ? type->GetEnumWithBitEncoding('1') : 0 ;
            if (zero_enum_id && one_enum_id) {
                VhdlEnum *zero_enum = new VhdlEnum(zero_enum_id) ;
                VhdlEnum *one_enum = new VhdlEnum(one_enum_id) ;
                zero_val->SetValueAt(i, zero_enum) ;
                one_val->SetValueAt(i, one_enum) ;
                zero_val->PopulateValues(values) ;
                one_val->PopulateValues(values) ;
            }
            break ;
        }
    }

    delete zero_val ;
    delete one_val ;

    return ;
}

/**********************************************/
// check whether it is a metalogical value with U X Z or W.
/**********************************************/
unsigned VhdlAccessValue::HasUXZW() const
{
    return _value ? _value->HasUXZW() : 0 ;
}

unsigned VhdlEnum::HasUXZW() const
{
    VERIFIC_ASSERT(_enum_id) ;

    // Enum literal has meta-value if it is NOT the specific '0' or '1' value :
    const char* image = _enum_id->Name() ;
    if (Strings::compare(image, "'-'")) {
        return 0 ;
    }

    // BitEncoding maps every thing to X or Z except 0,1,H and L. Since this
    // enum is not DC then it must be one of  U X Z or W.
    if (_enum_id->GetBitEncodingForElab()=='X' || _enum_id->GetBitEncodingForElab()=='Z') return 1 ;

    return 0 ; // not a meta-value
}

unsigned VhdlCompositeValue::HasUXZW() const
{
    VERIFIC_ASSERT(_values) ;

    unsigned default_has_UXZW = (_default_elem_val) ? _default_elem_val->HasUXZW() : 0 ; // assume 'inconclusive' if no default given.

    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            // no element value here. Default applies :
            if (default_has_UXZW) return 1 ;
            continue ; // inconclusive
        }
        if (elem->HasUXZW()) return 1 ;
    }
    return 0 ;
}

/**********************************************/

/**********************************************/
// Return integer value for this (constant) value
/**********************************************/

verific_int64 VhdlInt::Integer() const        { return _value ; }

verific_int64 VhdlDouble::Integer() const
{
    // Viper #4877 : Overflow/Underflow Check added for SIGFPE
    if (_value > (double)VERIFIC_INT64_MAX) {
        return VERIFIC_INT64_MAX;
    } else if (_value < (double)VERIFIC_INT64_MIN) {
        return VERIFIC_INT64_MIN;
    } else {
        return (verific_int64)_value ; // FIX ME : follow VHDL rounding ?
    }
}

verific_int64 VhdlAccessValue::Integer() const { return _value ? _value->Integer() : 0 ; }

verific_int64 VhdlEnum::Integer() const
{
    VERIFIC_ASSERT(_enum_id) ;

    if (_enum_id->GetBitEncodingForElab()) {
        // single-bit encoded
        return ((_enum_id->GetBitEncoding()=='1') ? 1 : 0) ;
    }
    if (_enum_id->IsOnehotEncoded()) {
        // CHECK ME : What to do for one-hot encoding ?
        // We should probably return the integer 1<<(Size()-1-Pos())
        // Since one-hot encoding at position 0 is "100..." (MSB set).
        VERIFIC_ASSERT(_enum_id->Type()) ;
        return ((verific_int64)1 << ((int)(_enum_id->Type()->NumOfEnums() - 1) - Position())) ;
    }
    if (_enum_id->GetEncodingForElab()) {
        // user multi-bit encoded
        verific_int64 result = 0 ;
        const char *encoding = _enum_id->GetEncoding() ;
        for (const char *tmp = encoding; *tmp; tmp++) {
            result = (result << 1) | ((*tmp=='1')?1:0) ;
        }
        return result ;
    }

    // binary encoded
    return (verific_int64)_enum_id->Position() ;
}

verific_int64 VhdlNonconstBit::Integer() const
{
    VERIFIC_ASSERT(_net) ;
    return (_net==Pwr()) ? 1 : 0 ;
}

verific_int64 VhdlNonconst::Integer() const
{
    VERIFIC_ASSERT(_nets) ;

    if (!_nets->Size()) return 0 ;

    // Integer representation of this nonconst
    // Issue 1445/1446 : make sure to test if sign bit is set before
    // deciding if the value is negative.
    Net *n ;
    // Start with negative result if signed, and MSB is set :
    n = (Net*)_nets->GetFirst() ;
    verific_int64 result = (_is_signed && n==Pwr()) ? ~((verific_int64)0) : 0 ;

    // Run MSB->LSB
    unsigned i ;
    FOREACH_ARRAY_ITEM(_nets, i, n) {
        result = result << 1 ; // free up LSB bit
        if (n==Pwr()) result |= 1 ; ; // Set LSB bit
    }
    return result ;
}

verific_int64 VhdlCompositeValue::Integer() const
{
    VERIFIC_ASSERT(_values) ;

    // We use this in case-statement evaluation stuff
    // Run MSB to LSB
    verific_int64 result = 0 ;
    VhdlValue *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            // no element value here. Default applies :
            elem = _default_elem_val ;
            if (!elem) continue ; // no default, no idea..
        }
        // Shift to left with number of bits of elem
        result = result << elem->NumBits() ;
        // Add the last one (LSB)
        result += elem->Integer() ;
    }
    return result ;
}

/**********************************************/
// Count number of bits in this value
/**********************************************/

unsigned VhdlInt::NumBits() const
{
    // count actual number of bits needed
    unsigned size = (_value<0) ? 1 : 0 ; // need sign bit
    verific_uint64 num = (_value<0) ? (verific_uint64)((-_value)-1) : (verific_uint64)_value ; // We can store one more value in signed
    for (; num!=0; num=num/2) size++ ;
    if (!size) size++ ; // Use one bit for value 0
    return size ;
}

unsigned VhdlDouble::NumBits() const
{
    return sizeof(double)*8 ;
}

unsigned VhdlEnum::NumBits() const
{
    VERIFIC_ASSERT(_enum_id && _enum_id->Type()) ;

    if (_enum_id->GetBitEncodingForElab()) {
        // single-bit encoded
        return 1 ;
    }
    if (_enum_id->IsOnehotEncoded()) {
        // one-hot encoded. Same number of bits as enum values in its type..
        return _enum_id->Type()->NumOfEnums() ;
    }
    if (_enum_id->GetEncodingForElab()) {
        // user-encoded : count number of bits in there
        return (unsigned)Strings::len(_enum_id->GetEncoding()) ;
    }

    // Otherwise, assume binary encoding :
    // return num of bits to encode unsigned Position
    verific_uint64 i = _enum_id->Position() ;
    unsigned result = 0 ;
    while (i>>result) result++ ;
    return result ;
}

unsigned VhdlNonconst::NumBits() const
{
    VERIFIC_ASSERT(_nets) ;

    return _nets->Size() ;
}

unsigned VhdlNonconstBit::NumBits() const
{
    VERIFIC_ASSERT(_net) ;

    return 1 ;
}

unsigned VhdlAccessValue::NumBits() const { return _value ? _value->NumBits() : 0 ; }

unsigned VhdlCompositeValue::NumBits() const
{
    VERIFIC_ASSERT(_values) ;

    // Accumulate bits of the elements
    // (In record values, the number of bits per element differs)
    unsigned result = 0 ;
    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            // no element value here. Default applies :
            elem = _default_elem_val ;
            if (!elem) continue ; // no default, no idea..
        }
        result += elem->NumBits() ;
    }
    return result ;
}

/**********************************************/
// Check value against constraint (subtype check)
/**********************************************/

// Check value that will be assigned to a target with 'constraint'.
// For arrays, error out on mismatch in number of elements
// For Scalars, check range : if value exceeds range, truncate the value.
// For constants : check if the value is contained by the constraint
unsigned VhdlAccessValue::CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df)
{
    if (constraint && constraint->GetDesignatedType()) return 1 ; // Viper #8390: access constraint with not expanded forward decl
    return _value ? _value->CheckAgainst(constraint, from, df) : 1 ; // if null, nothing to check
}

unsigned VhdlInt::CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df)
{
    if (!constraint) return 0 ;
    if (!constraint->Contains(this)) {
        char *v = Image() ;
        char *r = constraint->Image() ;
        // VIPER #7655 : Produce warning for non-constant branch
        if (df && df->IsNonconstBranch()) {
            if (from) from->Warning("value %s is out of target constraint range %s",v,r) ;
        } else {
            if (from) from->Error("value %s is out of target constraint range %s",v,r) ;
        }
        Strings::free(v) ;
        Strings::free(r) ;
        return 0 ;
    }
    return 1 ;
}

unsigned VhdlPhysicalValue::CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode * /* from */, const VhdlDataFlow * /*df*/)
{
    if (!constraint) return 0 ;

    // VIPER #2651: Do not check for the constraint for physical values.
    // Standard simulators seem to allow out-of-constraint range physical values,
    // specially for standard time. So, we don't check anything here to follow them.

    return 1 ; // Always success
}

unsigned VhdlDouble::CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df)
{
    if (!constraint) return 0 ;
    if (!constraint->Contains(this)) {
        char *v = Image() ;
        char *r = constraint->Image() ;
        // VIPER #7655 : Produce warning for non-constant branch
        if (df && df->IsNonconstBranch()) {
            if (from) from->Warning("value %s is out of target constraint range %s",v,r) ;
        } else {
            if (from) from->Error("value %s is out of target constraint range %s",v,r) ;
        }
        Strings::free(v) ;
        Strings::free(r) ;
        return 0 ;
    }
    return 1 ;
}

unsigned VhdlEnum::CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df)
{
    VERIFIC_ASSERT(_enum_id) ;

    if (!constraint) return 0 ;
    if (!constraint->Contains(this)) {
        char *v = Image() ;
        char *r = constraint->Image() ;
        // VIPER #7655 : Produce warning for non-constant branch
        if (df && df->IsNonconstBranch()) {
            if (from) from->Warning("value %s is out of target constraint range %s",v,r) ;
        } else {
            if (from) from->Error("value %s is out of target constraint range %s",v,r) ;
        }
        Strings::free(v) ;
        Strings::free(r) ;
        return 0 ;
    }
    return 1 ;
}

unsigned VhdlNonconst::CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode * /*from*/, const VhdlDataFlow * /*df*/)
{
    VERIFIC_ASSERT(_nets) ;

    if (!constraint) return 0 ;

    // Check the target number of bits needed to encode the constraint, versus the number of bits in the value.
    // Adjust the value if the target constraint has less bits than the value

    // Calculate the number of bits that are determined by the constraint :
    // Done same way as in CreateValue()
    unsigned size = constraint->NumBitsOfRange() ;
    unsigned is_signed = constraint->IsSignedRange() ;

    // Check if the constraint is unsigned and the value is signed.
    // If that is the case, we need to make the value unsigned too.
    // Otherwise, we carry the 'signed' info through the assignment, which is not correct.
    // The value really becomes unsigned after the assignment.
    //
    // The other way around (if constraint is signed and value is unsigned) is fine :
    // We continue to have an 'unsigned' value, which will be adjusted to signed if needed later.
    // CHECK ME : Can we optimize the MSB bit of the value away too if it is targeting the sign bit of the target ?
    if (_is_signed && !is_signed) {
        // Make value unsigned
        _is_signed = 0 ;

        // VIPER 3579. signed value to unsigned target optimization :
        // The value has a sign bit, but if it is set, then the simulator would error out
        // because we can never assign a negative value to an unsigned target.
        // So we can nicely optimize the sign bit away. It will become '0' if the value is extended later.
        // So this is a 'don't-care' optimization, so only do this if don't cares do not need to be preserved.
        // Do this before any other size adjustments, since we can only strip the sign bit once.
        if (!RuntimeFlags::GetVar("vhdl_preserve_x")) {
            if (NumBits()) Adjust(NumBits()-1) ;
            // Alternatively, we could replace the sign bit by an 'x' net...
        }
    }

    // Now check against the number of bits in the value
    if (NumBits() > size) {
        // Don't warn here, since in most cases it is benign. (for example :   x := x+1)
        // if (from) from->WarningId("value can be out of constraint range of target") ;
        Adjust(size) ;
    }

    return 1 ; // It is never an error to adjust the value down in size.
}

unsigned VhdlNonconstBit::CheckAgainst(const VhdlConstraint * /*constraint*/, const VhdlTreeNode * /*from*/, const VhdlDataFlow * /*df*/)
{
    VERIFIC_ASSERT(_net) ;

    // Nonconstbit is any single-bit value. Should always be OK for any constraint on the bit.
    return 1 ;
}

unsigned VhdlCompositeValue::CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df)
{
    VERIFIC_ASSERT(_values) ;

    if (!constraint) return 0 ;
    if (constraint->IsUnconstrained()) {
        // Issue 2592 : An 'unconstrained' index constraint can still force a restriction on the number of elements that a value can hold.
        // Test that here.
        // This test is actually only needed for enumeration index constraints, since there are no values left of right of the bounds, and that can (worst case) cause a core dump.
        // All constraints are always tested later to fall within bounds...
        //
        // Check if this is an enum index constraint :
        VhdlConstraint *index_constraint = constraint->IndexConstraint() ;
        if (index_constraint && index_constraint->IsEnumRange()) {
            // Only test if there are enough enum values in the index type for this amount of value elements:
            if (_values->Size() > constraint->Length()) {
                {
                    if (from) {
                        // Viper 4449 : Add more details to error message
                        VhdlName *formal = from->FormalPart() ;
                        const char *formal_name = formal ? formal->Name() : 0 ;
                        if (!formal_name) {
                            VhdlName *prefix = formal ? formal->GetPrefix() : 0 ;
                            formal_name = prefix ? prefix->Name() : 0 ;
                        }
                        if (formal && formal_name) {
                            // VIPER #7655 : Produce warning for non-constant branch
                            if (df && df->IsNonconstBranch()) {
                                from->Warning("expression has %d elements ; formal %s expects %d", _values->Size(), formal_name, constraint->Length()) ;
                            } else {
                                from->Error("expression has %d elements ; formal %s expects %d", _values->Size(), formal_name, constraint->Length()) ;
                            }
                        } else {
                            // VIPER #7655 : Produce warning for non-constant branch
                            if (df && df->IsNonconstBranch()) {
                                from->Warning("expression has %d elements ; expected %d", _values->Size(), constraint->Length()) ;
                            } else {
                                from->Error("expression has %d elements ; expected %d", _values->Size(), constraint->Length()) ;
                            }
                        }
                    }
                }
                return 0 ;
            }
        }
        return 1 ; // no check possible
    }

    if (constraint->IsNullRange() && !_values->Size()) return 1 ; // Null range matches 0 elements

    if (_values->Size() != constraint->Length()) {
        {
            if (from) {
                // Viper 4449 : Add more details to error message
                VhdlName *formal = from->FormalPart() ;
                const char *formal_name = formal ? formal->Name() : 0 ;
                if (!formal_name) {
                    VhdlName *prefix = formal ? formal->GetPrefix() : 0 ;
                    formal_name = prefix ? prefix->Name() : 0 ;
                }
                if (formal && formal_name) {
                    // VIPER #7655 : Produce warning for non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        from->Warning("expression has %d elements ; formal %s expects %d", _values->Size(), formal_name, constraint->Length()) ;
                    } else {
                        from->Error("expression has %d elements ; formal %s expects %d", _values->Size(), formal_name, constraint->Length()) ;
                    }
                } else {
                    // VIPER #7655 : Produce warning for non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        from->Warning("expression has %d elements ; expected %d", _values->Size(), constraint->Length()) ;
                    } else {
                        from->Error("expression has %d elements ; expected %d", _values->Size(), constraint->Length()) ;
                    }
                }
            }
        }
        return 0 ;
    }

    // Check each element too. Skip checking elements of single-dimensional arrays -- why ?
    // Commented out this check for Viper #3624
    // if (constraint->IsArrayConstraint() && !constraint->ElementConstraint()->IsArrayConstraint()) return 1 ;

    // Otherwise : check every element (of record or multi-dim array)
    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) continue ;
        if (!elem->CheckAgainst(constraint->ElementConstraintAt(i),from, df)) return 0 ;
    }

    // Viper #4950 : Also need to check the default element value against
    // the element constraint.. as after Viper 4950 such compact value
    // representation is more common
    if (_default_elem_val && !_default_elem_val->CheckAgainst(constraint->ElementConstraintAt(0),from, df)) return 0 ;

    return 1 ;
}

/**********************************************/
// Operations
/**********************************************/

/**********************************************/
// Range Bound Checking
/**********************************************/

// These guys are needed, since 'real' does not have a 'position'.
// Otherewise (if all ranges were discrete) we could use val1->Position() < val2->Position()
// for LessThan test.

// Nonconst that is a constant. Take Integer.
unsigned VhdlValue::LT(const VhdlValue *v) const          { return (v) ? (Integer() < v->Integer()) : 0 ; }

unsigned VhdlAccessValue::LT(const VhdlValue *v) const    { return (_value) ? _value->LT(v) : 0 ; }
unsigned VhdlInt::LT(const VhdlValue *v) const            { return (v) ? v->GT(this) : 0 ; }
unsigned VhdlDouble::LT(const VhdlValue *v) const         { return (v) ? v->GT(this) : 0 ; }
unsigned VhdlEnum::LT(const VhdlValue *v) const           { return (v) ? v->GT(this) : 0 ; }

unsigned VhdlValue::GT(const VhdlInt *v) const            { return GT_Int(v) ; }
unsigned VhdlValue::GT(const VhdlDouble *v) const         { return GT_Double(v) ; }
unsigned VhdlValue::GT(const VhdlEnum *v) const           { return GT_Enum(v) ; }

// Nonconst that is a constant. Take 'Integer'
unsigned VhdlValue::GT_Int(const VhdlInt *v) const        { return (v) ? (Integer() > v->Integer()) : 0 ; }
// Nonconst that is a constant. FIX ME : take 'Double' once we support Real.
unsigned VhdlValue::GT_Double(const VhdlDouble *v) const  { return (v) ? (Integer() > v->Real()) : 0 ; }
// Nonconst that is a constant. FIX ME : We assume integer encoding here.
unsigned VhdlValue::GT_Enum(const VhdlEnum *v) const      { return (v) ? (Integer() > v->Integer()) : 0 ; }

unsigned VhdlInt::GT_Int(const VhdlInt *v) const          { return (v) ? (_value > v->_value) : 0 ; }
unsigned VhdlDouble::GT_Double(const VhdlDouble *v) const { return (v) ? (_value > v->_value) : 0 ; }
unsigned VhdlEnum::GT_Enum(const VhdlEnum *v) const       { return (v) ? (_enum_id->Position() > v->_enum_id->Position()) : 0 ; }

/**********************************************/
// NumElements : return number of elements in composite value
/**********************************************/

unsigned VhdlValue::NumElements() const                   { return 0 ; }
unsigned VhdlCompositeValue::NumElements() const          { return (_values) ? _values->Size(): 0 ; }

/**********************************************/
// Return name (not allocated image) if this is an enumeration literal value
/**********************************************/

const char *VhdlEnum::EnumName() const {
    if (!_enum_id) return 0 ;
    return _enum_id->Name() ;
}

const char *VhdlAccessValue::EnumName() const {
    return _value ? _value->EnumName() : 0 ;
}

const char *VhdlNonconstBit::EnumName() const
{
    VERIFIC_ASSERT(_net) ;

    if (_net==Gnd()) return "'0'" ;
    if (_net==Pwr()) return "'1'" ;
    if (_net==X()) return "'X'" ;
    if (_net==Z()) return "'Z'" ;
    return "'.'" ; // non-constant bit ?
}

/**********************************************/
// StringImage : Produce a char* allocated string for an array of VeriValue that is known to be an array of 'character' elements (needed for 'report' and 'assertion' statements).
/**********************************************/

char *VhdlCompositeValue::StringImage() const
{
    VERIFIC_ASSERT(_values) ;
    // Array of characters..
    char *result = Strings::allocate(_values->Size()+3) ;
    result[0] = '"' ;
    unsigned long size = 1 ;
    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) elem = _default_elem_val ;
        char c = (elem) ? (char)elem->Position() : 0 ; // ASCII position of character
        if (!c) break ; // 'null' character marks end of string
        result[size++] = c ;
    }
    result[size++] = '"' ;
    result[size] = '\0' ;
    return result ;
}

char *VhdlAccessValue::StringImage() const
{
    return _value ? _value->StringImage() : Strings::save("null") ;
}

/**********************************************/
// Image : return allocated string that represents the value
/**********************************************/

char *VhdlAccessValue::Image() const
{
    return _value ? _value->Image() : Strings::save("null") ;
}

char *VhdlInt::Image() const
{
    char *r = Strings::save("[ some even bigger number ]") ;
    ::sprintf(r,"%lld",_value) ;
    return r ;
}

char *VhdlDouble::Image() const
{
    return Strings::dtoa(_value) ;
}

char *VhdlEnum::Image() const
{
    VERIFIC_ASSERT(_enum_id) ;

    // char *r = Strings::save(_enum_id->OrigName()) ;

    // VIPER #3418 : LRM section 14.1 on 'Image
    // Return value must be all lowercase
    char *r = Strings::save(_enum_id->Name()) ;
    return r ;
}

char *VhdlNonconst::Image() const
{
    // return an integer equivalent
    char *r = Strings::save("[ some even bigger number ]") ;
    ::sprintf(r,"%lld",Integer()) ;
    return r ;
}

char *VhdlNonconstBit::Image() const
{
    VERIFIC_ASSERT(_net) ;

    char *r = Strings::save("'.'") ;
    if (_net==Gnd()) r[1] = '0' ;
    if (_net==Pwr()) r[1] = '1' ;
    if (_net==X()) r[1] = 'X' ;
    if (_net==Z()) r[1] = 'Z' ;
    return r ;
}

char *VhdlCompositeValue::Image() const
{
    VERIFIC_ASSERT(_values) ;

    VhdlValue *elem ;
    unsigned i ;
    char *tmp2 ;
    char *result = 0 ;

    // Get the element type.
    // This could be a 'virtual' type, if value has multiple dimensions.
    // VIPER #3537 side effect: Need to check all the value elements for their types.
    // RD: 7/26: Cleanup : we can print the image as a string literal if every element is a character literal.
    unsigned all_character = 1 ;
    const char *enum_image = 0 ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) elem = _default_elem_val ;
        if (!elem) continue ; // ??
        enum_image = elem->EnumName() ;
        // It's a character literal if name starts with ' :
        if (enum_image && enum_image[0]=='\'') continue ;
        // otherwise, can't print as a string :
        all_character = 0 ;
        break ;
    }

    if (!_values->Size()) {
        // empty array. Cannot print that any legal way in VHDL (?), only as an empty string...
        // That's also probably how it originated...
        result = Strings::save("\"\"") ;
    } else if (all_character) {
        // Array of characters. Print as string :
        result = Strings::allocate(_values->Size()+3) ;
        result[0] = '"' ;
        unsigned long size = 1 ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ; // ??
            enum_image = elem->EnumName() ;
            result[size] = enum_image ? enum_image[1]: ' ' ;
            size++ ;
        }
        result[size++] = '"' ;
        result[size] = '\0' ;
    } else {
        // regular array. Print as aggregate
        result = Strings::save("(") ;
        char *tmp ;
        unsigned first = 1 ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            tmp = elem->Image() ;
            tmp2 = result ;
            result = Strings::save(result,(first) ? "" : ",", tmp) ;
            first = 0 ;
            Strings::free( tmp2 ) ;
            Strings::free( tmp ) ;
        }
        tmp2 = result ;
        result = Strings::save(result,")") ;
        Strings::free( tmp2 ) ;
    }

#if 0 // Viper #7288: debug
    if (_range) {
        char *range_image = _range->Image() ;
        char *result_constraint = Strings::save(" (", range_image, ") ") ;
        Strings::free(range_image) ;

        tmp2 = result ;
        result = Strings::save(result_constraint, result) ;

        Strings::free(tmp2) ;
        Strings::free(result_constraint) ;
    }
#endif

// Viper 8308 : Convert to Hex/Octal
#ifdef VHDL_PRESERVE_LITERALS
    char* ret_str = 0 ;
    char* image = 0 ;
    switch(_base_specifier) {
    case 'o' :
        if (result && result[0] == '"') {
            result[0] = '0' ;
            image = result + 1 ;
        }
        if (result && result[Strings::len(result)-1] == '"') result[Strings::len(result)-1] = '\0' ;
        ret_str = Strings::BinToOct(image ? image : result) ;
        Strings::free(result) ;
        return ret_str ;
    case 'x' :
        if (result && result[0] == '"') {
            result[0] = '0' ;
            image = result + 1 ;
        }
        if (result && result[Strings::len(result)-1] == '"') result[Strings::len(result)-1] = '\0' ;
        ret_str = Strings::BinToHex(image ? image : result) ;
        Strings::free(result) ;
        return ret_str ;
    default:
        break ;
    }
#endif

    return result ;
}

/**********************************************/
// Vhdl 2008 LRM 5.7, 5.3.2.4 etc
// predefined to_string/to_ostring/to_hstring routines
char *VhdlValue::ToString(unsigned /*flavor*/, const VhdlValue * /*arg2*/) const
{
    return Image() ;
}

char *VhdlDouble::ToString(unsigned /*flavor*/, const VhdlValue *arg2) const
{
    if (!arg2) {
        return Image() ;
    } else if (arg2->IsIntVal()) {
        // second argument is digit
        unsigned digits = (unsigned) arg2->Integer() ;
        char *result = Image() ;
        if (!result) return 0 ;

        char *tmp = result ;
        while (*tmp != '.' && *tmp != '\0') ++tmp ;

        if (!digits) {
            *tmp = '\0' ; // overwrite '.'
        } else {
            ++digits ; // for '.'
            while (*tmp != '\0' && digits) {
                ++tmp ;
                --digits ;
            }
            *tmp = '\0' ;
        }
        return result ;
    } else {
        // second arg is format
        char *r = Strings::save("[  taking  a  very  very  large  number  ]") ;
        char *arg2_val = arg2->Image() ;
        ::sprintf(r, arg2_val, _value) ;
        Strings::free(arg2_val) ; // Memory leak fix
        return r ;
    }
    return 0 ;
}

char *VhdlPhysicalValue::ToString(unsigned /*flavor*/, const VhdlValue *arg2) const
{
    verific_int64 arg2_val = (arg2 && arg2->IsPhysicalValue()) ? arg2->Integer() : 0 ;
    if (!arg2_val) {
        return Image() ;
    } else {
        VhdlPhysicalValue tmp_val(Integer() / arg2_val) ;
        return tmp_val.Image() ;
    }
    return 0 ;
}

char *VhdlCompositeValue::ToString(unsigned flavor, const VhdlValue * /*arg2*/) const
{
    char *image = Image() ;
    char *ret_str = 0 ;
    switch(flavor) {
    case VHDL_to_string :
    case VHDL_to_bstring :
    case VHDL_to_binary_string :
        return image ;
    case VHDL_to_ostring :
    case VHDL_to_octal_string :
        if (image && image[0] == '"') image[0] = '0' ;
        if (image && image[Strings::len(image)-1] == '"') image[Strings::len(image)-1] = '\0' ;
        ret_str = Strings::BinToOct(image) ;
        Strings::free(image) ;
        return ret_str ;
    case VHDL_to_hstring :
    case VHDL_to_hex_string :
        if (image && image[0] == '"') image[0] = '0' ;
        if (image && image[Strings::len(image)-1] == '"') image[Strings::len(image)-1] = '\0' ;
        ret_str = Strings::BinToHex(image) ;
        Strings::free(image) ;
        return ret_str ;
    default:
        Strings::free(image) ;
    }
    return 0 ;
}

/**********************************************/
// ImageShort : return allocated string that represents composite value as others => _default_elem_value
/**********************************************/

char *VhdlValue::ImageShort() const
{
    // Return Image except for composite value
    return (Image()) ;
}

// This function is only used in CreateUniqueUnit
char *VhdlCompositeValue::ImageShort() const
{
    // Called only from CreateUnique Name
    // to create shorter string for composite
    // values which are all null
    // (hence point to default_elem_value)
    // This was done due to huge memory requirement
    // for creating strings from large composite
    // value this was reported in Viper 6093
    VERIFIC_ASSERT(_values) ;

    VhdlValue *elem ;
    unsigned i ;
    char *tmp2 ;
    char *result = 0 ;

    // Get the element type.
    // This could be a 'virtual' type, if value has multiple dimensions.
    // VIPER #3537 side effect: Need to check all the value elements for their types.
    unsigned all_character = 1 ;
    unsigned all_bit_encoded = 1 ;
    VhdlIdDef *elem_type ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) elem = _default_elem_val ;
        elem_type = (elem) ? elem->GetType() : 0 ;
        // If the type cannot be determined, resert this flags and break out of the loop:
        if (!elem_type) { all_character = 0 ; all_bit_encoded = 0 ; break ; }
        // Compare the type of this element with these types:
        if (all_character && !Strings::compare_nocase(elem_type->Name(), "character")) all_character = 0 ;
        if (Strings::compare_nocase(elem_type->Name(), "boolean")) all_bit_encoded = 0 ;
        if (!elem_type->Constraint() || !elem_type->Constraint()->IsBitEncoded()) all_bit_encoded = 0 ;
        if (!all_character && !all_bit_encoded) break ; // No need to continue
    }

    if (!_values->Size()) {
        // empty array. Cannot print that any legal way in VHDL (?), only as an empty string...
        // That's also probably how it originated...
        result = Strings::save("\"\"") ;
    } else if (all_character) {
        // Array of character..
        result = Strings::save("\"") ;
        char c ;
        char tmp[3] ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            c = (char)elem->Position() ; // ASCII position of character
            ::sprintf(tmp,"%c",c) ;
            tmp2 = result ;
            result = Strings::save(result,tmp) ;
            Strings::free( tmp2 ) ;
        }
        tmp2 = result ;
        result = Strings::save(result,"\"") ;
        Strings::free( tmp2 ) ;
    } else if (all_bit_encoded) {
        // Viper 6093 : Use others=>default value
        // as the expression instead of a long string
        // this is only done in CreateUniqueName.
        unsigned all_null = 0 ;
        if (_default_elem_val) {
            all_null = 1 ;
            unsigned j ;
            VhdlValue *val ;
            FOREACH_ARRAY_ITEM(_values, j, val) {
                if (val) {
                    all_null = 0 ;
                    break ;
                }
            }
        }
        if (!all_null) {
            // Viper 6093. Using a single allocation
            // call instead multiple save/free calls
            // of String class. This caused large run
            // time and possible memory fragmentation.
            // Array of bits. Print as string.
            result = Strings::allocate(_values->Size()+3) ;
            result[0] = '"' ;
            char *p ;
            unsigned long size = 1 ;
            FOREACH_ARRAY_ITEM(_values, i, elem) {
                if (!elem) elem = _default_elem_val ;
                if (!elem) continue ;
                p = elem->Image() ;
                result[size] = p[1] ;
                size++ ;
                Strings::free(p) ;
            }
            result[size++] = '"' ;
            result[size] = '\0' ;
        } else {
            if (_default_elem_val) {
                char *default_image = _default_elem_val->Image() ;
                result = Strings::save("others =>", default_image) ;
                Strings::free(default_image) ;
            } else {
                result = Strings::save("\"\""); //NULL string
            }
        }
    } else {
        // regular array. Print as aggregate
        result = Strings::save("(") ;
        char *tmp ;
        unsigned first = 1 ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            tmp = elem->Image() ;
            tmp2 = result ;
            result = Strings::save(result,(first) ? "" : ",", tmp) ;
            first = 0 ;
            Strings::free( tmp2 ) ;
            Strings::free( tmp ) ;
        }
        tmp2 = result ;
        result = Strings::save(result,")") ;
        Strings::free( tmp2 ) ;
    }

// Viper 8308 : Convert to Hex/Octal
#ifdef VHDL_PRESERVE_LITERALS
    char* ret_str = 0 ;
    char* image = 0 ;
    switch(_base_specifier) {
    case 'o' :
        if (result && result[0] == '"') {
            result[0] = '0' ;
            image = result + 1 ;
        }
        if (result && result[Strings::len(result)-1] == '"') result[Strings::len(result)-1] = '\0' ;
        ret_str = Strings::BinToOct(image ? image : result) ;
        Strings::free(result) ;
        return ret_str ;
    case 'x' :
        if (result && result[0] == '"') {
            result[0] = '0' ;
            image = result + 1 ;
        }
        if (result && result[Strings::len(result)-1] == '"') result[Strings::len(result)-1] = '\0' ;
        ret_str = Strings::BinToHex(image ? image : result) ;
        Strings::free(result) ;
        return ret_str ;
    default:
        break ;
    }
#endif
    return result ;
}

/**********************************************/
// ImageVerilog : return allocated string that represents the value as a Verilog literal.
/**********************************************/

char *VhdlAccessValue::ImageVerilog() const     { return _value ? _value->ImageVerilog() : 0 ; }

char *VhdlInt::ImageVerilog() const
{
    char *r = Strings::save("[ some even bigger number ]") ;
    ::sprintf(r,"%lld",_value) ;
    return r ;
}

char *VhdlDouble::ImageVerilog() const
{
    return Strings::dtoa(_value) ;
}

char *VhdlEnum::ImageVerilog() const
{
    VERIFIC_ASSERT(_enum_id) ;

    // If this is a bit-encoded literal, print the bit-encoding as Verilog literal :
    char *r = 0 ;
    // Viper 6414 use verilog x for VHDL enums W and -
    // use verilog 0 for L 1 for H
    char bit_encoding = 0 ;
    if (_enum_id->GetBitEncoding()) {
        bit_encoding = _enum_id->GetBitEncoding() ;
        // Viper 7321 : use IsStdUlogicType istead of StdType
    } else if (_enum_id->Type() && _enum_id->Type()->IsStdULogicType()) {
        // Viper 7285: Do special processing for std_ulogic even if it is not logic encoded.
        // Treat it like bit encoding.
        bit_encoding = *(_enum_id->Name()+1) ;
    }

    switch (bit_encoding) {
        case 'L' :
        case '0' : r = Strings::save("1'b0") ; break ;
        case 'H' :
        case '1' : r = Strings::save("1'b1") ; break ;
        case 'W' :
        case '-' :
        case 'X' : r = Strings::save("1'bx") ; break ;
        case 'Z' : r = Strings::save("1'bz") ; break ;
        default :  r = Strings::save(_enum_id->OrigName()) ; // not (single) bit-encoded
    }
    // Viper 3395: For multibit encoding of enums we need to
    // create strings like 2'b00 for _enum_id == 0 for 2 bit
    // encoding
    if (!bit_encoding) {
        unsigned base = NumBits() ;
        if (!base) base = 1 ; // VIPER #7642: Even 0 needs 1 bit to store!
        char* encoding = Strings::itoa((int)(base)) ;
        char* bits = Strings::allocate((unsigned long)(base)) ;
        const char *encoding_name = _enum_id->OrigName() ;

        unsigned i ;
        for (i = 0; i < base -1 ; i++) bits[i] = '0' ;
        bits[i] = '\0' ;
        //encoding_name is of the form 'v'. Thus encoding_name[1] is the character
        Strings::free(r) ; // free r, about to reassign
        // Viper 6414 use verilog x for VHDL enums W and -
        // use verilog 0 for L 1 for H
        switch (encoding_name[1]) {
            case 'L' :
            case '0' : r = Strings::save(encoding, "'b", bits, "0") ; break ;
            case 'H' :
            case '1' : r = Strings::save(encoding, "'b", bits, "1") ; break ;
            case 'W' :
            case '-' :
            case 'X' : r = Strings::save(encoding, "'b", bits, "x") ; break ;
            case 'Z' : r = Strings::save(encoding, "'b", bits, "z") ; break ;
            default :  r = Strings::itoa((int)Integer()) ; break ; // convert to integer better than the enum name
        }
        Strings::free(bits) ;
        Strings::free(encoding) ;
    }
    return r ;
}

char *VhdlNonconst::ImageVerilog() const
{
    // Multibit scalar. return an integer representation
    char *r = Strings::save("[ some even bigger number ]") ;
    ::sprintf(r,"%lld",Integer()) ;
    return r ;
}

char *VhdlNonconstBit::ImageVerilog() const
{
    VERIFIC_ASSERT(_net) ;

    const char *r = "." ; // default : non-constant bit...
    if (_net==Gnd()) r = "1'b0" ;
    if (_net==Pwr()) r = "1'b1" ;
    if (_net==X()) r = "1'bx" ;
    if (_net==Z()) r = "1'bz" ;
    return Strings::save(r) ;
}

char *VhdlCompositeValue::ImageVerilog() const
{
    VERIFIC_ASSERT(_values) ;

    VhdlValue *elem ;
    unsigned i ;
    char *tmp2 ;
    char *result = 0 ;

    // Get the element type.
    // This could be a 'virtual' type, if value has multiple dimensions.
    // VIPER #3537 side effect: Need to check all the value elements for their types.
    unsigned all_character = 1 ;
    unsigned all_bit_encoded = 1 ;
    unsigned all_std_ulogic = 1 ;
    VhdlIdDef *elem_type ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) elem = _default_elem_val ;
        elem_type = (elem) ? elem->GetType() : 0 ;
        // If the type cannot be determined, resert this flags and break out of the loop:
        if (!elem_type) { all_character = 0 ; all_bit_encoded = 0 ; break ; }
        // Compare the type of this element with these types:
        if (all_character && !Strings::compare_nocase(elem_type->Name(), "character")) all_character = 0 ;
        if (!elem_type->Constraint() || !elem_type->Constraint()->IsBitEncoded()) all_bit_encoded = 0 ;
        // Viper 7285 check for std_ulogic  even if it is not logic encoded.
        // Viper 7321 : avoid using stdType here.
        if (!elem_type->IsStdULogicType()) all_std_ulogic = 0 ;
        if (!all_character && !all_bit_encoded && !all_std_ulogic) break ; // No need to continue
    }

    if (!_values->Size()) {
        // empty array. Cannot print that any legal way in Verilog, only as an empty string...
        // That's also probably how it originated...
        result = Strings::save("\"\"") ;
    } else if (all_character) {
        // Array of character.. Print as a string.
        result = Strings::save("\"") ;
        char c ;
        char tmp[3] ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            c = (char)elem->Position() ; // ASCII position of character
            ::sprintf(tmp,"%c",c) ;
            tmp2 = result ;
            result = Strings::save(result,tmp) ;
            Strings::free( tmp2 ) ;
        }
        tmp2 = result ;
        result = Strings::save(result,"\"") ;
        Strings::free( tmp2 ) ;
    } else if (all_bit_encoded || all_std_ulogic) {
        // Viper 7285: Also do bit encoding if std_ulogic is not logic encoded.
        // Array of bits. Print as Verilog bit-string literal :
        // VIPER #7429: Include size in the created image:
        char *size = Strings::itoa((int)_values->Size()) ;
        result = Strings::save(size, "'b") ;
        Strings::free(size) ;
        char *p ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            p = elem->ImageVerilog() ; // Execute Verilog Image, so that we get bit-literals (1'b0 etc) back.
            tmp2 = result ;
            result = Strings::save(result,p+Strings::len(p)-1/*only take the last character*/) ;
            Strings::free( tmp2 ) ;
            Strings::free(p) ;
        }
    } else {
        // regular array. Print as Verilog aggregate ?
        result = Strings::save("{") ;
        char *tmp ;
        unsigned first = 1 ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            tmp = elem->ImageVerilog() ; // call ImageVerilog on the elements.
            tmp2 = result ;
            result = Strings::save(result,(first) ? "" : ",", tmp) ;
            first = 0 ;
            Strings::free( tmp2 ) ;
            Strings::free( tmp ) ;
        }
        tmp2 = result ;
        result = Strings::save(result,"}") ;
        Strings::free( tmp2 ) ;
    }
    return result ;
}

/**********************************************/
// Check if a value is equal to another value
/**********************************************/

unsigned VhdlAccessValue::IsEqual(const VhdlValue *other_val) const
{
    return _value ? _value->IsEqual(other_val) : 0 ;
}

unsigned VhdlInt::IsEqual(const VhdlValue *other_val) const
{
    if (!other_val) return 0 ;
    // 'this' is constant, so 'other_val' better be constant too :
    if (!other_val->IsConstant()) return 0 ;
    // Check if integer representation is the same :
    return (other_val->Integer() == Integer()) ? 1 : 0 ;
}

unsigned VhdlDouble::IsEqual(const VhdlValue *other_val) const
{
    if (!other_val) return 0 ;
    // 'this' is constant, so 'other_val' better be constant too :
    if (!other_val->IsConstant()) return 0 ;
    // FIX ME for double
    return (other_val->Integer() == Integer()) ? 1 : 0 ;
}

unsigned VhdlEnum::IsEqual(const VhdlValue *other_val) const
{
    VERIFIC_ASSERT(_enum_id) ;

    if (!other_val) return 0 ;
    // 'this' is constant, so 'other_val' better be constant too :
    if (!other_val->IsConstant()) return 0 ;

    // Use 'Position'. That is fast and works regardless of encoding
    // Issue 2241 : That is not true if 'other_val' is a NonconstBit :
    // in that case, other_val->Position() will return 0 or 1 for gnd or pwr,
    // but 'this' value will continue to report the position of the enum literal in the type.

    // So, we need to consider encoding here.
    // Since we know that 'this' is constant, we can use 'Integer', as with the other cosntants.
    // That one is sensitive to encoding :

    // Viper 7289: For Enum with Enum comparison one should look at position
    if (other_val->GetEnumId()) {
        return (other_val->Position() == Position()) ;
    }
    return (other_val->Integer()==Integer()) ? 1 : 0 ;
//    return (other_val->Position() == Position()) ? 1 : 0 ;
}

unsigned VhdlNonconstBit::IsEqual(const VhdlValue *other_val) const
{
    VERIFIC_ASSERT(_net) ;

    if (!other_val) return 0 ;
    return (_net == other_val->GetBit()) ? 1 : 0 ;
}

unsigned VhdlNonconst::IsEqual(const VhdlValue *other_val) const
{
    VERIFIC_ASSERT(_nets) ;

    if (!other_val) return 0 ;
    if (other_val == this) return 1 ; // Probably never happens..
    unsigned size = MAX(other_val->NumBits(), NumBits()) ;
    unsigned i ;
    for (i=0; i<size; i++) {
        if (GetBit(i) != other_val->GetBit(i)) return 0 ;
    }
    return 1 ;
}

unsigned VhdlCompositeValue::IsEqual(const VhdlValue *other_val) const
{
    VERIFIC_ASSERT(_values) ;

    if (!other_val) return 0 ;
    if (other_val == this) return 1 ; // Probably never happens..
    if (!other_val->IsComposite()) return 0 ;
    if (NumElements() != other_val->NumElements()) return 0 ;  // unequal size means values are not equal

    unsigned i ;
    VhdlValue *elem_val1, *elem_val2 ;
    for (i=0;i<_values->Size();i++) {
        elem_val1 = ValueAt(i) ;
        elem_val2 = other_val->ValueAt(i) ;
        if (elem_val1 == elem_val2) continue ; // Covers that both are 0
        if (elem_val1 && !elem_val1->IsEqual(elem_val2)) return 0 ;
    }
    return 1 ;
}

/**********************************************/
// Check if this value represents a REAL number
/**********************************************/

unsigned VhdlValue::IsReal() const  { return 0 ; }
unsigned VhdlDouble::IsReal() const { return 1 ; }

/**********************************************/
// Check if this is an IntVal
/**********************************************/

unsigned VhdlValue::IsIntVal() const  { return 0 ; }
unsigned VhdlInt::IsIntVal() const { return 1 ; }

/**********************************************/
// Pointer-Manipulation on composite values
/**********************************************/
void VhdlValue::PrependElement(VhdlValue * /*val*/) { VERIFIC_ASSERT(0) ; }
void VhdlCompositeValue::PrependElement(VhdlValue *val)
{
    VERIFIC_ASSERT(_values) ;
    _values->InsertFirst(val) ;

    if (_range) {
        VhdlValue *low = _range->Low() ;
        VhdlValue *high = _range->High() ;
        VERIFIC_ASSERT(low && high) ;

        high = high->Copy()->UnaryOper(VHDL_INCR, 0) ;
        low = low->Copy() ;

        unsigned dir = _range->Dir() ;
        unsigned is_unconstrainted = _range->IsUnconstrained() ;
        delete _range ;
        if (dir == VHDL_to) {
            _range = new VhdlRangeConstraint(low, dir, high, is_unconstrainted) ;
        } else {
            _range = new VhdlRangeConstraint(high, dir, low, is_unconstrainted) ;
        }
    }
}

void VhdlValue::AppendElement(VhdlValue * /*val*/) { VERIFIC_ASSERT(0) ; }
void VhdlCompositeValue::AppendElement(VhdlValue *val)
{
    VERIFIC_ASSERT(_values) ;
    _values->InsertLast(val) ;

    if (_range) {
        VhdlValue *low = _range->Low() ;
        VhdlValue *high = _range->High() ;
        VERIFIC_ASSERT(low && high) ;

        high = high->Copy()->UnaryOper(VHDL_INCR, 0) ;
        low = low->Copy() ;

        unsigned dir = _range->Dir() ;
        unsigned is_unconstrainted = _range->IsUnconstrained() ;
        delete _range ;
        if (dir == VHDL_to) {
            _range = new VhdlRangeConstraint(low, dir, high, is_unconstrainted) ;
        } else {
            _range = new VhdlRangeConstraint(high, dir, low, is_unconstrainted) ;
        }
    }
}

/**********************************************/
// Check if a value came from a enum-encoded single-bit value
/**********************************************/

unsigned VhdlInt::IsBit() const                 { return 0 ; }
unsigned VhdlDouble::IsBit() const              { return 0 ; }
unsigned VhdlEnum::IsBit() const                { return (_enum_id->GetBitEncodingForElab()) ? 1 : 0 ; }
unsigned VhdlNonconstBit::IsBit() const         { return 1 ; }
unsigned VhdlNonconst::IsBit() const            { return 0 ; }
unsigned VhdlCompositeValue::IsBit() const      { return 0 ; }
unsigned VhdlAccessValue::IsBit() const         { return 0 ; }

/**********************************************/
// Check if a value came from a multi-bit user-encoded enumeration value
/**********************************************/

unsigned VhdlValue::UserEncoded() const         { return 0 ; }
unsigned VhdlEnum::UserEncoded() const          { return (_enum_id->IsOnehotEncoded()) ? 2 : (_enum_id->GetEncodingForElab() ? 1 : 0) ; }
unsigned VhdlNonconst::UserEncoded() const      { return _user_encoded ; }

/**********************************************/
// Check if a value is one-hot encoded
/**********************************************/

unsigned VhdlValue::OnehotEncoded() const       { return 0 ; }
unsigned VhdlEnum::OnehotEncoded() const        { return (_enum_id->IsOnehotEncoded()) ? 1 : 0 ; }
unsigned VhdlNonconst::OnehotEncoded() const    { return (_user_encoded==2) ? 1 : 0 ; }

/**********************************************/
// Get single bit encoded net out of a value
/**********************************************/
Net *VhdlInt::GetBit() const
{
    // Can this happen ?
    return (_value) ? Pwr() : Gnd() ;
}

Net *VhdlDouble::GetBit() const
{
    // Can this happen ?
    return (_value != 0.0) ? Pwr() : Gnd() ;
}

Net *VhdlEnum::GetBit() const
{
    VERIFIC_ASSERT(_enum_id) ;

    // Get net from single-bit encoded identifiers
    switch (_enum_id->GetBitEncodingForElab()) {
    case '0' : return Gnd() ;
    case '1' : return Pwr() ;
    case 'X' : return X() ;
    case 'Z' : return Z() ;
    default :  return 0 ; // not (single) bit-encoded
    }
}

Net *VhdlNonconstBit::GetBit() const { return _net ; }

Net *VhdlNonconst::GetBit() const
{
    if (!_nets || Size()!=1) return 0 ;
    return (Net*)_nets->At(0) ;
}

Net *VhdlCompositeValue::GetBit() const
{
    return 0 ;
}

/*********************************************************/
// Get bit out of a multi-bit encoded value
// Don't allow this for Composites
// pos==0 is LSB !!!
// This is different from normal array indexing (MSB==0)
// If position is beyond present position, do virtual sign/zero extend
/*********************************************************/
Net *VhdlInt::GetBit(unsigned pos) const
{
    return (((verific_uint64)_value>>pos)&1) ? Pwr() : Gnd() ;
}

Net *VhdlDouble::GetBit(unsigned /*pos*/) const
{
    return Gnd() ; // CHECK ME
}

Net *VhdlEnum::GetBit(unsigned pos) const
{
    VERIFIC_ASSERT(_enum_id) ;

    if (_enum_id->GetBitEncodingForElab()) {
        // single-bit encoded literal
        return (pos==0) ? GetBit() : 0 ;
    }
    if (_enum_id->IsOnehotEncoded()) {
        // return '1' if this enum 'pos' is same as Position(), '0' otherwise.
        // Actually, we need to count from the LSB size :
        return (pos==(NumBits()-_enum_id->Position())-1) ? Pwr() : Gnd() ;
    }
    if (_enum_id->GetEncodingForElab()) {
        // multi-bit user-encoded
        const char *encoding = _enum_id->GetEncoding() ;
        unsigned len = NumBits() ;
        if (pos>=len) {
            // 0-extend (this probably never happens)
            return Gnd() ;
        } else {
            // 'pos'==0 is LSB (right-hand-side). So, find position from right
            switch (encoding[(len-1)-pos]) {
            case '0' : return Gnd() ;
            case '1' : return Pwr() ;
            case 'X' : return X() ;
            case 'Z' : return X() ; // 'Z' is 'X' in multi-bit encoding...
            default : return X() ; // VERIFIC_ASSERT ?
            }
        }
    }

    // else : use binary encoding : use position
    return (((_enum_id->Position())>>pos)&1) ? Pwr() : Gnd() ;
}

Net *VhdlNonconstBit::GetBit(unsigned pos) const
{
    VERIFIC_ASSERT(_net) ;

    // Do 0 extension if needed (NonconstBit is never signed)
    return (pos==0) ? _net : Gnd() ;
}

Net *VhdlNonconst::GetBit(unsigned pos) const
{
    VERIFIC_ASSERT(_nets) ;

    if (pos>=_nets->Size()) {
        // VIPER #7661 : Check the size of _nets before accessing its first element
        return (_is_signed && _nets->Size()) ? (Net*)_nets->At(0) : Gnd() ;
    } else {
        // NOTE : pos==0 is LSB. LSB in a nonconst is highest-index bit !
        // since nonconst is ordered MSB->LSB.
        return (Net*)_nets->At((_nets->Size() - pos) - 1) ;
    }
}

Net *VhdlCompositeValue::GetBit(unsigned /*pos*/) const
{
    return 0 ; // VERIFIC_ASSERT ?
}

/******************************************************/
// Value Translators ABSORB 'this' !!
// NOTE : ALL VALUE TRANSLATORS WORK PROPERLY ON
// ARRAY-OF-BIT, BUT NOT ON OTHER COMPOSITE VALUES !!
/******************************************************/

VhdlNonconst *VhdlInt::ToNonconst()
{
    // Run MSB->LSB to match nonconst array' bit-ordering
    // figure out how many bits we need
    // Same thing we do in constraint bound calculation : size = log2(val)
    unsigned size = (_value<0) ? 1 : 0 ; // need sign bit
    verific_uint64 num = (_value<0) ? (verific_uint64)(-_value-1) : (verific_uint64)_value ; // We can store one more value in signed
    for (; num!=0; num=num/2) size++ ;
    if (!size) size++ ; // Use one bit for value 0

    // Fill nets MSB->LSB
    Net *net ;
    Array *nets = new Array() ;
    while (size-- != 0) {
        net = (((verific_uint64)_value>>size)&1) ? Pwr() : Gnd() ;
        nets->InsertLast(net) ;
    }
    num = (_value<0) ? 1 : 0 ; // serves as sign flag, since we will now destroy 'this'
    delete this ;
    return new VhdlNonconst(nets,(unsigned)num,0/*not user-encoded*/) ;
}

VhdlNonconst *VhdlDouble::ToNonconst()
{
    // Cannot do this yet
    // We have no 'from' to warn from, but we error out on variable real's
    // so there is not much need for an error here.
    // Message::Error("not supported use of real number") ;
    //
    // Try to stay close to the truth, translating to integer and going to nonconst
    VhdlInt *val = new VhdlInt((verific_int64)_value) ;
    delete this ;
    return val->ToNonconst() ;
}

VhdlNonconst *VhdlEnum::ToNonconst()
{
    VERIFIC_ASSERT(_enum_id) ;

    Array *nets = new Array() ;
    Net *net = GetBit() ;
    unsigned user_encoded = 0 ;
    if (net) {
        // Single-bit encoded constant ;
        // should not really get here very often any more
        // since we translate to nonconstbit most of the time
        nets->InsertLast(net) ;
    } else if (_enum_id->IsOnehotEncoded()) {
        // one-hot encoded value
        user_encoded = 2 ;
        // return a pwr at position 'Position'
        verific_uint64 pos = _enum_id->Position() ;
        VERIFIC_ASSERT(_enum_id->Type()) ;
        unsigned n = _enum_id->Type()->NumOfEnums() ;
        for (unsigned i=0; i<n; i++) {
            net = (i==pos) ? Pwr() : Gnd() ;
            nets->InsertLast(net) ;
        }
    } else if (_enum_id->GetEncodingForElab()) {
        user_encoded = 1 ;
        // multi-bit user-encoded value : Get the values from the user-defined encoding
        for (const char *tmp = _enum_id->GetEncoding(); *tmp; tmp++) {
            switch (*tmp) {
            case '0' : net = Gnd() ; break ;
            case 'L' : net = Gnd() ; break ;
            case '1' : net = Pwr() ; break ;
            case 'H' : net = Pwr() ; break ;
            case 'X' : net = X() ; break ;
            case 'Z' : net = X() ; break ; // 'Z' is 'X' in multi-bit encoding
            default :  net = X() ; break ; // VERIFIC_ASSERT ?
            }
            nets->InsertLast(net) ;
        }
    } else {
        // Default : Do binary encoding : use 'position' as value
        verific_uint64 pos = _enum_id->Position() ;
        unsigned n = _enum_id->Type()->NumOfEnums() ;
        if (n) n = n-1 ; // Calculate log2(n-1).
        unsigned size = 0 ;
        while (n != 0) { size++ ; n = n / 2 ; }
        while (size-- != 0) {
            net = ((pos>>size)&1) ? Pwr() : Gnd() ;
            nets->InsertLast(net) ;
        }
    }
    delete this ;
    return new VhdlNonconst(nets,0/*unsigned*/,user_encoded) ;
}

VhdlNonconst *VhdlNonconstBit::ToNonconst()
{
    VERIFIC_ASSERT(_net) ;

    Array *a = new Array() ;
    a->InsertLast(_net) ;
    delete this ;
    return new VhdlNonconst(a,0/*unsigned*/,0/*not-user-encoded*/) ;
}

VhdlNonconst *VhdlNonconst::ToNonconst()
{
    return this ;
}

VhdlNonconst *VhdlCompositeValue::ToNonconst()
{
    VERIFIC_ASSERT(_values) ;

    // Stack all bits from the elements into one single array of nets
    // MSB left, LSB right (within elements too)
    Array *nets = new Array(_values->Size()) ;
    VhdlValue *elem;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) {
            if (_default_elem_val) {
                // default value applies.
                if (_default_elem_val->IsBit()) {
                    // single-bit element
                    nets->InsertLast(_default_elem_val->GetBit()) ;
                } else {
                    // multi-bit element
                    // Make a copy _default_elem_val because we do not own it.
                    VhdlNonconst *n = _default_elem_val->Copy()->ToNonconst() ;
                    nets->Append(n->Nets()) ;
                    delete n ;
                }
            }
            continue ;
        }
        if (elem->IsBit()) {
            // single-bit element
            nets->InsertLast(elem->GetBit()) ;
        } else {
            // multi-bit element. Can take 'elem' if we clean position 'i'.
            VhdlNonconst *n = elem->ToNonconst() ;
            nets->Append(n->Nets()) ;
            delete n ;
            _values->Insert(i,0) ;
        }
    }
    i = _is_signed ;
    delete this ;
    return new VhdlNonconst(nets,i,0/*not user-encoded*/) ;
}

// ToNonconstBit : call only if we are sure this value represents one bit
VhdlNonconstBit *VhdlInt::ToNonconstBit()
{
    // Not sure if this ever happens, but we can translate integer 0/1
    // to a bit value
    VERIFIC_ASSERT(_value>=0 && _value<=1) ;
    verific_int64 val = _value ;
    delete this ;
    return new VhdlNonconstBit((val==1) ? Pwr() : Gnd()) ;
}

VhdlNonconstBit *VhdlDouble::ToNonconstBit()
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}

VhdlNonconstBit *VhdlEnum::ToNonconstBit()
{
    VERIFIC_ASSERT(_enum_id) ;

    // pick-up the value bit. It MUST be bit-encoded.
    Net *n = GetBit() ;
    VERIFIC_ASSERT(n) ;
    delete this ;
    return new VhdlNonconstBit(n) ;
}

VhdlNonconstBit *VhdlNonconst::ToNonconstBit()
{
    VERIFIC_ASSERT(_nets && Size()==1) ;
    Net *n = (Net*)_nets->At(0) ;
    delete this ;
    return new VhdlNonconstBit(n) ;
}

VhdlNonconstBit *VhdlNonconstBit::ToNonconstBit()
{
    return this ;
}

VhdlNonconstBit *VhdlCompositeValue::ToNonconstBit()
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}

VhdlCompositeValue *VhdlInt::ToComposite(unsigned size)
{
    // Go via 'nonconst'
    return ToNonconst()->ToComposite(size) ;
}

VhdlCompositeValue *VhdlDouble::ToComposite(unsigned size)
{
    // Go via 'nonconst'
    return ToNonconst()->ToComposite(size) ;
}

VhdlCompositeValue *VhdlEnum::ToComposite(unsigned size)
{
    VERIFIC_ASSERT(_enum_id) ;

    // Go via 'nonconst'
    return ToNonconst()->ToComposite(size) ;
}

VhdlCompositeValue *VhdlNonconst::ToComposite(unsigned size)
{
    // Split-up the bits
    Array *a = new Array(Size()) ;
    unsigned cut_off = (Size() > size) ? 1 : 0 ;
    unsigned i ;
    Net *net ;
    if (!cut_off) {
        // sign/zero extend
        i = size - Size() ;
        net = (_is_signed && _nets && _nets->Size()) ? (Net*)_nets->At(0) : Gnd() ;
        while (i--!=0) {
            a->InsertLast(new VhdlNonconstBit(net)) ;
        }
    }
    FOREACH_ARRAY_ITEM(_nets, i, net) {
        // Cut-off on 'size' (take only 'size' bits on LSB side
        if (cut_off && i<Size()-size) continue ;
        a->InsertLast(new VhdlNonconstBit(net)) ;
    }
    // CHECK ME : composite value should never be 'signed',
    // except when explicitly set by 'SetSigned' in the pragma function routine
    // So do NOT pass sign bit through.. ?
    unsigned is_signed = 0 ; // _is_signed ;
    delete this ;
    return new VhdlCompositeValue(a,0,is_signed) ;
}

VhdlCompositeValue *VhdlNonconstBit::ToComposite(unsigned size)
{
    VERIFIC_ASSERT(_net) ;

    Array *a = new Array() ;
    // zero extend to 'size'
    unsigned i = (size) ? size-1 : 0 ;
    while (i--!=0) a->InsertLast(new VhdlNonconstBit(Gnd())) ;
    // insert the bit itself
    if (size) a->InsertLast(this) ;
    // Don't delete 'this', since it was inserted into array.
    return new VhdlCompositeValue(a,0/*unsigned*/) ;
}

VhdlCompositeValue *VhdlCompositeValue::ToComposite(unsigned size)
{
    VERIFIC_ASSERT(_values) ;

    VhdlValue *low = _range ? _range->Low() : 0 ;
    VhdlValue *high = _range ? _range->High() : 0 ;

    if (low) low = low->Copy() ;
    if (high) high = high->Copy() ;

    // sign/zero extend, or cut-off
    unsigned i ;
    VhdlValue *elem_val ;
    if (size < _values->Size()) {
        // cut-off // delete the elements
        for (i=0;i<_values->Size()-size;i++) {
            elem_val = (VhdlValue*)_values->At(i) ;
            delete elem_val ;
            if (high) high = high->UnaryOper(VHDL_DECR, 0) ;
        }
        // Reduce left side of array
        _values->Remove(0,_values->Size()-size) ;
    } else if (size > _values->Size()) {
        // sign/zero extend
        Array tmp(size-_values->Size()) ;
        elem_val = (_is_signed) ? (VhdlValue*)_values->At(0) : 0 ;
        for (i=0;i<size-_values->Size();i++) {
            tmp.InsertLast((elem_val) ? elem_val->Copy() : new VhdlNonconstBit(Gnd())) ;
            if (high) high = high->UnaryOper(VHDL_INCR, 0) ;
        }
        _values->Prepend(&tmp) ;
        delete elem_val ;
    }

    if (_range && high && low) {
        unsigned dir = _range->Dir() ;
        unsigned is_unconstrainted = _range->IsUnconstrained() ;
        delete _range ;
        if (dir == VHDL_to) {
            _range = new VhdlRangeConstraint(low, dir, high, is_unconstrainted) ;
        } else {
            _range = new VhdlRangeConstraint(high, dir, low, is_unconstrainted) ;
        }
    } else {
        delete high ; delete low ; delete _range ;
    }

    return this ;
}

/***********************************************************************************************/
// Viper 7157 and Viper 7868:
// CollectDCs is like ToNonconst only it stores the net index which would be created corresponding
// to the meta value dont_care. Once the composite value is converted to non const value the dont_care
// meta value information is lost. However we need to preserve this information for a net during creating
// the Equal operator. This is because comparison with meta value would always result true value of the
// condition. One should appretiate the fact that the meta value may be located deeper in the composite
// value and we have to store which net is constructed from a meta value.

// CollectDCs take a input set s to store the numbers the current index of the net in the _nets array
// would be maintained in runner.

// If _net(i) is corresponding to a meta value then i+1 is inserted in dc. An offset
// of 1 is added because 0 value cannot be added to the set s.

// A new testcase is added to Viper 7157

void VhdlInt::CollectDCs(Set& /*s*/, unsigned& runner)
{
    // Run MSB->LSB to match nonconst array' bit-ordering
    // figure out how many bits we need
    // Same thing we do in constraint bound calculation : size = log2(val)
    unsigned size = (_value<0) ? 1 : 0 ; // need sign bit
    verific_uint64 num = (_value<0) ? (verific_uint64)(-_value-1) : (verific_uint64)_value ; // We can store one more value in signed
    for (; num!=0; num=num/2) size++ ;
    if (!size) size++ ; // Use one bit for value 0
    runner += size ;
}
void VhdlDouble::CollectDCs(Set& s, unsigned &runner)
{
    VhdlInt *val = new VhdlInt((verific_int64)_value) ;
    val->CollectDCs(s, runner) ;
    delete val ;
}
void VhdlEnum::CollectDCs(Set& s, unsigned &runner)
{
    VERIFIC_ASSERT(_enum_id) ;

    unsigned is_dc = IsMetalogicalWithDC() ;

    Net *net = GetBit() ;
    if (net) {
        // Single-bit encoded constant ;
        // should not really get here very often any more
        // since we translate to nonconstbit most of the time
        if (is_dc) {
            (void) s.Insert((void*)(unsigned long)(runner++)) ;
        } else {
            ++runner ;
        }
    } else if (_enum_id->IsOnehotEncoded()) {
        verific_uint64 pos = _enum_id->Position() ;
        VERIFIC_ASSERT(_enum_id->Type()) ;
        unsigned n = _enum_id->Type()->NumOfEnums() ;
        for (unsigned i=0; i<n; i++) {
            if (i==pos && is_dc) {
                (void) s.Insert((void*)(unsigned long)(runner++)) ;
            } else {
                ++runner ;
            }
        }
    } else if (_enum_id->GetEncodingForElab()) {
        // multi-bit user-encoded value : Get the values from the user-defined encoding
        if (is_dc) {
            (void) s.Insert((void*)(unsigned long)(runner++)) ;
        } else {
            ++runner ;
        }
    } else {
        // Default : Do binary encoding : use 'position' as value
        verific_uint64 pos = _enum_id->Position() ;
        unsigned n = _enum_id->Type()->NumOfEnums() ;
        if (n) n = n-1 ; // Calculate log2(n-1).
        unsigned size = 0 ;
        while (n != 0) { size++ ; n = n / 2 ; }
        while (size-- != 0) {
            if ((pos>>size)&1 && is_dc) {
                (void) s.Insert((void*)(unsigned long)(runner++)) ;
            } else {
                ++runner ;
            }
        }
    }
}
void VhdlNonconstBit::CollectDCs(Set& /*s*/, unsigned& runner) { ++runner ; }
void VhdlNonconst::CollectDCs(Set& /*s*/, unsigned& runner) { runner += _nets ? _nets->Size() : 0 ; }
void VhdlCompositeValue::CollectDCs(Set& s, unsigned& runner)
{
    VERIFIC_ASSERT(_values) ;

    // Stack all bits from the elements into one single array of nets
    // MSB left, LSB right (within elements too)
    VhdlValue *elem;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) elem = _default_elem_val ;
        if (!elem) continue ;
        elem->CollectDCs(s, runner) ;
    }
}
/***********************************************************************************************/

// Translate a constant VhdlNonconstVal back to a real constant.
// VERIFIC_ASSERT if this does not work.
// Also VERIFIC_ASSERT if the value is not constant !

// This routine is needed when we evaluate constants in packages.
// We want to get rid of the nets in the constant values.
// These nets could have been created when we evaluate the constant
// expressions assigned to package constants.

VhdlValue *VhdlInt::ToConst(VhdlIdDef * /*type*/)       { return this ; }
VhdlValue *VhdlDouble::ToConst(VhdlIdDef * /*type*/)    { return this ; }
VhdlValue *VhdlEnum::ToConst(VhdlIdDef * /*type*/)      { return this ; }

VhdlValue *VhdlNonconst::ToConst(VhdlIdDef *type)
{
    // First, translate the value to an integer
    verific_int64 val = Integer() ;
    if (type && type->IsEnumerationType()) {
        if (OnehotEncoded()) { // Viper #7813
            unsigned idx = 0 ;
            while ((val = val/2) != 0) ++idx ;
            val = (unsigned) ((type->NumOfEnums() - 1) - idx) ;
        }

        VERIFIC_ASSERT(val>=0) ;
        // FIX ME : Assuming binary enum-encoding here.. That's not always correct
        VhdlIdDef *enum_id = type->GetEnumAt((unsigned)val) ;
        VERIFIC_ASSERT(enum_id) ;
        delete this ;
        return new VhdlEnum(enum_id) ;
    }

    // Otherwise : assume it is a integer type. Translate to constant integer.
    delete this ;
    return new VhdlInt(val) ;
}

VhdlValue *VhdlNonconstBit::ToConst(VhdlIdDef *type)
{
    VERIFIC_ASSERT(_net) ;

    // Could be an enumeration type, or... ?
    if (type && type->IsEnumerationType()) {
        // Pick-up the right enumeration literal :
        VhdlIdDef *enum_id = type->GetEnumWithBitEncoding((_net==Gnd())?'0':(_net==Pwr())?'1':(_net==Z())?'Z':'X') ;
        if (!enum_id) {
            // Must be a two-value regular encoded regular enum type (boolean or so).... 0 is first, 1 is second
            enum_id = type->GetEnumAt((_net==Gnd())?0:1) ;
        }
        VERIFIC_ASSERT(enum_id) ; // If this fails, I have no idea what sort of enum type/value this is
        delete this ;
        return new VhdlEnum(enum_id) ;
    }
    // Otherwise : assume it is a integer type. Translate to constant integer.
    verific_int64 val = Integer() ;
    delete this ;
    return new VhdlInt(val) ;
}

VhdlValue *VhdlCompositeValue::ToConst(VhdlIdDef *type)
{
    VERIFIC_ASSERT(_values) ;

    // Translate all elements to constant
    VERIFIC_ASSERT(type) ;

    // FIX ME :
    // For this routine to work properly, ToConst must be called on all subelements
    // of this composite value. We cannot do that without a type.
    // But since multi-dimensional array types don't have their 'intermediate'
    // types available, we cannot properly always decend with the correct element type.
    //
    // Multi-dimensional array types with a scalar element type can still be
    // handled correctly, since we can never decend lower than a scalar (see FIX ME below)

    // So bail out just for multi-dim array types with non-scalar element type.
    // This is a hack, since now it is not guaranteed that ToConst is called for
    // every subelement. So result might still be NonConstVal (in very, very rare cases).
    // Since ToConst is mostly used for cosmetic purposes (enumeration values print their name),
    // this hack should never cause a critical defect.
    if (type->Dimension() > 1  && type->ElementType() && !type->ElementType()->IsScalarType()) return this ;

    unsigned i ;
    VhdlValue *elem ;
    VhdlIdDef *elem_type = 0 ;
    if (type->IsArrayType()) {
        elem_type = type->ElementType() ;
    } else if (type->IsRecordType()) {
        // elem_type will be set per element
    } else {
        // Dirty : We were probably in a multi-dimensional array,
        // and decended into the element, when the composite value
        // is still halfway the multi-dim value.
        // Hack : set elem_type as this (scalar) type, so when we
        // hit the real element, we will be back in sync. Happens on 2910 example.
        // FIX ME : There should be a better way to do this...
        elem_type = type ;
    }
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) continue ;
        if (type->IsRecordType()) {
            // Get the record element type by position
            VhdlIdDef *elem_id = type->GetElementAt(i) ;
            elem_type = (elem_id) ? elem_id->Type() : 0 ;
        }
        elem = elem->ToConst(elem_type) ;
        _values->Insert(i, elem) ;
    }
    // VIPER #5062 : Convert default element value to constant
    _default_elem_val = (_default_elem_val) ? _default_elem_val->ToConst(elem_type ? elem_type : type->ElementType()): 0 ;
    return this ;
}

/**********************************************/
// VHDL type conversion Elaboration of LRM 7.3.5
/**********************************************/

VhdlValue *VhdlValue::ToType(VhdlIdDef *type, VhdlTreeNode *from)
{
    // In VHDL, we only need to change value types if
    // we convert (numeric types) from real to int or back.
    // Otherwise, we always keep the same value.
    // Right now, that's only between VhdlInt and VhdlDouble.
    //
    // non-constant values don't differ between double and
    // int yet, so we stay the same there.
    // Viper #5257 : The exception is when constant values are
    // (very often) represented as non-constant values (in terms
    // of the constant pwr/gnd nets). We need to convert them to
    // real values so that other binary operations on real values
    // can be performed (they are defined only for real 'op' real)
    if (type && type->IsRealType()) {
        if (IsConstant()) { // Viper #5257
            double result = (double)Integer() ;
            (void) CheckAgainst(type->Constraint(), from, 0) ;
            delete this ;
            return new VhdlDouble(result) ;
        } else {
            // if (from) from->Error("nonconstant REAL value %s is not supported for synthesis", "") ;
        }
    }

    // enumeration values cannot be converted, so we atay the same.
    //
    // Even for array types, the value representation
    // remains the same since the element types must be the
    // same for a valid VHDL type conversion.
    // Easy, no ?
    return this ;
}
// VIPER #7516 : Convert real array to integer array or back
VhdlValue *VhdlCompositeValue::ToType(VhdlIdDef *type, VhdlTreeNode *from)
{
    unsigned i ;
    VhdlValue *elem, *new_elem ;
    VhdlIdDef *elem_type = 0 ;
    if (type->IsArrayType()) {
        // Conversion is allowed only for array
        elem_type = type->ElementType() ;
    } else {
        // Call base API
        return VhdlValue::ToType(type, from) ;
    }
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) continue ;
        if (type->IsRecordType()) {
            // Get the record element type by position
            VhdlIdDef *elem_id = type->GetElementAt(i) ;
            elem_type = (elem_id) ? elem_id->Type() : 0 ;
        }
        new_elem = elem->ToType(elem_type, from) ;
        if (new_elem) _values->Insert(i, new_elem) ;
    }
    _default_elem_val = (_default_elem_val) ? _default_elem_val->ToType(elem_type ? elem_type : type->ElementType(), from): 0 ;
    return this ;
}

VhdlValue *VhdlInt::ToType(VhdlIdDef *type, VhdlTreeNode *from)
{
    if (!type) return this ;
    if (type->Constraint()) {
        (void) CheckAgainst(type->Constraint(), from, 0) ; // Check with Type Conversion Type VIPER 4461
    }
    // Integer value : Check which type we go to :
    if (type->IsIntegerType()) {
        return this ;
    } else if (type->IsRealType()) {
        // Integer to real conversion : cast directly
        double result = (double)_value ;
        delete this ;
        return new VhdlDouble(result) ;
    }
    // else : should not happen. VERIFIC_ASSERT ?

    return this ;
}

VhdlValue *VhdlDouble::ToType(VhdlIdDef *type, VhdlTreeNode *from)
{
    if (!type) return this ;
    if (type->Constraint()) {
        (void) CheckAgainst(type->Constraint(), from, 0) ; // Check with Type Conversion Type VIPER 4461
    }
    // Floating point value : Check which type we go to :
    if (type->IsIntegerType()) {
        // conversion of double to int : LRM 7.3.5 : round to nearest integer :
        verific_int64 result = (verific_int64)::floor(_value+0.5) ; // Hope that always floor returns a "no-fraction" double.
        delete this ;
        return new VhdlInt(result) ;
    } else if (type->IsRealType()) {
        return this ;
    }
    // else : should not happen. VERIFIC_ASSERT ?
    return this ;
}

/**********************************************/
// Support routines for nonconst
/**********************************************/

unsigned VhdlNonconst::Size() const { VERIFIC_ASSERT(_nets) ; return _nets->Size() ; }

unsigned VhdlNonconst::ActiveSize() const
{
    // Quick estimate of bits needed to represent the value.
    // Disregard x'es and z's.
    if (!_nets || _nets->Size()==0) return 0 ;
    Net *net = 0 ;
    Net *padbit = (_is_signed) ? (Net*)_nets->At(0) : _gnd ;
    unsigned i ;
    unsigned result = _nets->Size() ;
    // Run MSB->LSB until we differ from the padbit.
    FOREACH_ARRAY_ITEM(_nets,i,net) {
        if (net!=padbit) break ;
        result-- ;
    }
    if (_is_signed) result++ ;  // leave one sign bit on.
    if (!result) result++ ;     // value 0 needs 1 bit.
    return result ;
}

Array *VhdlNonconst::Nets() const   { VERIFIC_ASSERT(_nets) ; return _nets ; }

void VhdlNonconst::Adjust(unsigned size)
{
    VERIFIC_ASSERT(_nets) ;

    if (size < Size()) {
        // truncate
        _nets->Remove(0,Size() - size) ;
    } else if (size > Size()) {
        // zero/sign extend
        Array tmp(size - Size()) ;
        Net *n = (_is_signed && _nets->Size()) ? (Net*)_nets->At(0) : Gnd() ;
        for (unsigned i=0; i<size-Size();i++) tmp.InsertLast(n) ;
        _nets->Prepend(&tmp) ;
    }
}

// Reduce to minimum size possible, by removing MSB constants
void VhdlNonconst::Prune()
{
    VERIFIC_ASSERT(_nets) ;

    // Run MSB to LSB
    Net *net ;
    unsigned i ;
    unsigned strip = 0 ;
    if (_is_signed) {
        Net *sign = 0 ;
        FOREACH_ARRAY_ITEM(_nets,i,net) {
            if (i==0) {
                sign = net ;
                continue ;
            }
            if (net!=sign) break ;
            strip++ ;
        }
    } else {
        FOREACH_ARRAY_ITEM(_nets,i,net) {
            if (net!=Gnd()) break ;
            strip++ ;
        }
    }
    if (!strip) return ;

    // Now strip-off 'strip' bits from MSB side
    // Avoid going back to 0 bits
    if (strip==_nets->Size()) strip-- ;
    _nets->Remove(0,strip) ;
}

/**********************************************/
// Unary Operators
/**********************************************/

VhdlValue *VhdlAccessValue::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
     if (from) from->Error("operator %s not supported on this value", VhdlTreeNode::OperatorName(oper_type)) ;
    return 0 ;
}

VhdlValue *VhdlInt::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
    switch (oper_type) {
    case VHDL_not :  break ;
    case VHDL_abs :  if (_value < 0) _value = -_value ; return this ;
    case VHDL_MINUS: _value = -_value ; return this ;
    case VHDL_PLUS:  return this ;
    case VHDL_INCR:  _value++ ; return this ;
    case VHDL_DECR:  _value-- ; return this ;
    default : break ;
    }
    if (from) from->Error("operator %s not defined on integer values", VhdlTreeNode::OperatorName(oper_type)) ;
    return 0 ;
}

VhdlValue *VhdlPhysicalValue::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
    switch (oper_type) {
    case VHDL_not :  break ;
    case VHDL_abs :  if (_value < 0) _value = -_value ; return this ;
    case VHDL_MINUS: _value = -_value ; return this ;
    case VHDL_PLUS:  return this ;
    case VHDL_INCR:  _value++ ; return this ;
    case VHDL_DECR:  _value-- ; return this ;
    default : break ;
    }
    if (from) from->Error("operator %s not defined on physical values", VhdlTreeNode::OperatorName(oper_type)) ;
    return 0 ;
}

VhdlValue *VhdlDouble::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
    switch (oper_type) {
    case VHDL_not : break ;
    case VHDL_abs : if (_value < 0.0) _value = -_value ; return this ;
    case VHDL_MINUS: _value = -_value ; return this ;
    case VHDL_PLUS:  return this ;
    case VHDL_INCR: _value = _value + 1.0 ; return this ;
    case VHDL_DECR: _value = _value - 1.0 ; return this ;
    default : break ;
    }
    if (from) from->Error("operator %s not defined on real values", VhdlTreeNode::OperatorName(oper_type)) ;
    return 0 ;
}

VhdlValue *VhdlEnum::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_enum_id) ;

    VhdlIdDef *result ;
    // unary operator on enum value : NOT, INCR/DECR (for 'succ and 'pred attributes etc)
    switch (oper_type) {
    case VHDL_INCR:
        result = _enum_id->Type()->GetEnumAt((unsigned)_enum_id->Position()+1) ;
        if (result) {
            _enum_id = result ;
        } else {
            from->Error("there is no value right of %s in type %s", _enum_id->Name(), _enum_id->Type()->Name()) ;
        }
        return this ;
    case VHDL_DECR:
        result = _enum_id->Type()->GetEnumAt((unsigned)_enum_id->Position()-1) ;
        if (result) {
            _enum_id = result ;
        } else {
            from->Error("there is no value left of %s in type %s", _enum_id->Name(), _enum_id->Type()->Name()) ;
        }
        return this ;
    case VHDL_not:
        if (IsBit()) {
            // This must be NOT on 'bit' type.
            // Find the other value :
            result = _enum_id->Type()->GetEnumAt( (_enum_id->Position()==1) ? 0 : 1 ) ;
            if (result) {
                _enum_id = result ;
            } else {
                from->Error("there is no NOT of %s in type %s", _enum_id->Name(), _enum_id->Type()->Name()) ;
            }
            return this ;
        }
        // 'not' on non-bit encoded enumeration value : Should not happen.. Drop to error
        break ;
    case VHDL_condition :
    {
        // 1076-2008 LRM section 9.2.9
        VhdlValue *one = new VhdlNonconstBit(Pwr()) ;
        return BinaryOper(one, VHDL_EQUAL, from) ;
    }
    default : break ;
    }
    if (from) from->Error("operator %s not supported on this value", VhdlTreeNode::OperatorName(oper_type)) ;
    return this ;
}

VhdlValue *VhdlNonconst::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
    switch (oper_type) {
    case VHDL_not :  return Invert(from) ;
    case VHDL_abs :  return Abs(from) ;
    case VHDL_MINUS: return UnaryMin(from, VhdlNode::Pwr()) ;
    case VHDL_PLUS:  return this ;
    case VHDL_INCR:  return Plus((new VhdlInt((verific_int64)0))->ToNonconst(),Pwr(),from) ;
    case VHDL_DECR:  return Minus((new VhdlInt((verific_int64)1))->ToNonconst(),from) ;
#if 0
    // standard VHDL does not support reduction operators.
    // So reduction operators are always handled via pragma'd functions
    // They never end-up here.
    case VHDL_REDAND : return Reduce(oper_type,from) ;
    case VHDL_REDNAND: return Reduce(oper_type,from) ;
    case VHDL_REDOR :  return Reduce(oper_type,from) ;
    case VHDL_REDNOR : return Reduce(oper_type,from) ;
    case VHDL_REDXOR : return Reduce(oper_type,from) ;
    case VHDL_REDXNOR: return Reduce(oper_type,from) ;
#endif
    default : break ;
    }
    if (from) from->Error("operator %s not supported on this value", VhdlTreeNode::OperatorName(oper_type)) ;
    return this ;
}

VhdlValue *VhdlNonconstBit::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_net) ;

    switch (oper_type) {
    case VHDL_not : return Invert(from) ;
    case VHDL_abs : return this ; // bit-values are always unsigned
    case VHDL_condition :
    {
        // 1076-2008 LRM section 9.2.9
        VhdlValue *result = new VhdlNonconstBit(Pwr()) ;
        return BinaryOper(result, VHDL_EQUAL, from) ;
    }
    default : break ;
    }
    if (from) from->Error("operator %s not supported on this value", VhdlTreeNode::OperatorName(oper_type)) ;
    return this ;
}

VhdlValue *VhdlCompositeValue::UnaryOper(unsigned oper_type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

    unsigned i ;
    VhdlValue *val ;
    switch (oper_type) {
    case VHDL_not :
#ifdef DB_INFER_WIDE_OPERATORS
        // For bit encoded non constant composite value, insert wide operator
        // Translate it to non constant and then back to composite
        if ((NumBits() == NumElements()) && !IsConstant()) {
            i = NumElements() ; // remember the size
            val = ToNonconst() ;
            val = (val) ? val->UnaryOper(oper_type,from): 0 ; // Execute the function
            return (val) ? val->ToComposite(i) : 0 ; // Back to composite
        }
#endif // DB_INFER_WIDE_OPERATORS
        // element-by-element operation
        // VIPER #3586 : Check whether val is null to avoid crash
        FOREACH_ARRAY_ITEM(_values, i, val) if (val) (void) val->UnaryOper(oper_type,from) ;
        if (_default_elem_val) (void) _default_elem_val->UnaryOper(oper_type,from) ; // VIPER #6556
        return this ;
    case VHDL_PLUS:  return this ; // unary plus
    case VHDL_abs :  // absolute value
    case VHDL_MINUS: // unary minus
    case VHDL_INCR:  // incrementor
    case VHDL_DECR:  // decrementor
        // Go to nonconst
        i = NumElements() ; // remember the size
        val = ToNonconst() ;
        val = (val) ? val->UnaryOper(oper_type,from): 0 ; // Execute the function
        return (val) ? val->ToComposite(i) : 0 ; // Back to composite
    case VHDL_minimum:
    case VHDL_maximum:
    {
        // This is the first implementation and is suboptimal
        // This implementation follows the LRM 5.3.2.4, but could
        // easily optimized for performance (visible for large arrays)
        VhdlValue *first_value = TakeValueAt(0) ;

        if (!NumElements()) {
            delete this ;
            return first_value ;
        }

        RemoveValueAt(0) ; // remove the first value

        VhdlValue *min_max_value = UnaryOper(oper_type, from) ;
        // this pointer already free'd during recursion termination
        // delete this ;

        if (!min_max_value) return first_value ;
        return first_value ? first_value->BinaryOper(min_max_value, oper_type, from) : 0 ;
    }
    case VHDL_and: return ToNonconst()->Reduce(VHDL_REDAND, from) ;
    case VHDL_or: return ToNonconst()->Reduce(VHDL_REDOR, from) ;
    case VHDL_nand: return ToNonconst()->Reduce(VHDL_REDNAND, from) ;
    case VHDL_nor: return ToNonconst()->Reduce(VHDL_REDNOR, from) ;
    case VHDL_xor: return ToNonconst()->Reduce(VHDL_REDXOR, from) ;
    case VHDL_xnor: return ToNonconst()->Reduce(VHDL_REDXNOR, from) ;
    default : break ;
    }
    if (from) from->Error("operator %s not supported on this value", VhdlTreeNode::OperatorName(oper_type)) ;
    return this ;
}

/**********************************************/
// Binary Operators
/**********************************************/

// Virtual resolving : turn arguments around
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlInt *left_val, unsigned oper_type, const VhdlTreeNode *from)            { return ReverseBinaryOper_Int(left_val, oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlDouble *left_val, unsigned oper_type, const VhdlTreeNode *from)         { return ReverseBinaryOper_Double(left_val, oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlEnum *left_val, unsigned oper_type, const VhdlTreeNode *from)           { return ReverseBinaryOper_Enum(left_val, oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlNonconst *left_val, unsigned oper_type, const VhdlTreeNode *from)       { return ReverseBinaryOper_Nonconst(left_val, oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlNonconstBit *left_val, unsigned oper_type, const VhdlTreeNode *from)    { return ReverseBinaryOper_NonconstBit(left_val, oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlPhysicalValue *left_val, unsigned oper_type, const VhdlTreeNode *from)  { return ReverseBinaryOper_Physical(left_val, oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlCompositeValue *left_val, unsigned oper_type, const VhdlTreeNode *from) { return ReverseBinaryOper_Composite(left_val, oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper(VhdlAccessValue *left_val, unsigned oper_type, const VhdlTreeNode *from)    { return ReverseBinaryOper_Access(left_val, oper_type, from) ; }

VhdlValue *VhdlAccessValue::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)                { return val->ReverseBinaryOper(this,oper_type, from) ; }
VhdlValue *VhdlInt::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)                        { return val->ReverseBinaryOper(this,oper_type, from) ; }
VhdlValue *VhdlDouble::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)                     { return val->ReverseBinaryOper(this,oper_type, from) ; }
VhdlValue *VhdlEnum::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)                       { return val->ReverseBinaryOper(this,oper_type, from) ; }
VhdlValue *VhdlNonconst::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)                   { return val->ReverseBinaryOper(this,oper_type, from) ; }
VhdlValue *VhdlNonconstBit::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)                { return val->ReverseBinaryOper(this,oper_type, from) ; }
VhdlValue *VhdlPhysicalValue::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)              { return val->ReverseBinaryOper(this,oper_type, from) ; }
VhdlValue *VhdlCompositeValue::BinaryOper(VhdlValue *val, unsigned oper_type, const VhdlTreeNode *from)             { return val->ReverseBinaryOper(this,oper_type, from) ; }

// For not-equal type operations (constant<->nonconstant) : Go to nonconstants on each side
// FIX ME : Should we VERIFIC_ASSERT that they should not happen ?
VhdlValue *VhdlValue::ReverseBinaryOper_Int(VhdlInt *val, unsigned oper_type, const VhdlTreeNode *from)                   { return ToNonconst()->ReverseBinaryOper(val->ToNonconst(),oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper_Enum(VhdlEnum *val, unsigned oper_type, const VhdlTreeNode *from)                 { return ToNonconst()->ReverseBinaryOper(val->ToNonconst(),oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper_Nonconst(VhdlNonconst *val, unsigned oper_type, const VhdlTreeNode *from)         { return ToNonconst()->ReverseBinaryOper(val,oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper_NonconstBit(VhdlNonconstBit *val, unsigned oper_type, const VhdlTreeNode *from)   { return ToNonconst()->ReverseBinaryOper(val->ToNonconst(),oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper_Physical(VhdlPhysicalValue *val, unsigned oper_type, const VhdlTreeNode *from)    { return ToNonconst()->ReverseBinaryOper(val->ToNonconst(),oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper_Double(VhdlDouble *val, unsigned oper_type, const VhdlTreeNode *from)             { return ToNonconst()->ReverseBinaryOper(val->ToNonconst(),oper_type, from) ; }
VhdlValue *VhdlValue::ReverseBinaryOper_Composite(VhdlCompositeValue *val, unsigned oper_type, const VhdlTreeNode *from)
{
    // Not so fast here. left is composite, right is not.
    // Happens for constant shifters and rollers.
    // Translate to nonconst, but translate 'left' back to composite.
    unsigned size = val->NumElements() ;
    VhdlValue *result = ToNonconst()->ReverseBinaryOper(val->ToNonconst(), oper_type, from) ;
    return (result) ? result->ToComposite(size) : 0 ;
}
VhdlValue *VhdlValue::ReverseBinaryOper_Access(VhdlAccessValue *val, unsigned oper_type, const VhdlTreeNode *from)
{
    // operator not defined for access type and non-access type.. Only equality and
    // inequality are defined between two access types
    if (from) from->Error("operator %s not supported on these values", VhdlTreeNode::OperatorName(oper_type)) ;

    delete val ;
    delete this ;

    return 0 ;
}

VhdlValue *VhdlAccessValue::ReverseBinaryOper_Access(VhdlAccessValue *val, unsigned oper_type, const VhdlTreeNode *from)
{
    if ((oper_type != VHDL_EQUAL) && (oper_type != VHDL_NEQUAL)) {
        if (from) from->Error("operator %s not supported on these values", VhdlTreeNode::OperatorName(oper_type)) ;
    }

    VhdlValue *result = 0 ;
    if (oper_type == VHDL_EQUAL) {
        (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((val->_value == _value) ? 1 : 0)) : result = new VhdlNonconstBit((val->_value == _value) ? Pwr() : Gnd()) ;
    } else if (oper_type == VHDL_NEQUAL) { // oper_type == VHDL_NEQUAL
        (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((val->_value != _value) ? 1 : 0)) : result = new VhdlNonconstBit((val->_value != _value) ? Pwr() : Gnd()) ;
    }

    delete val ;
    delete this ;

    return result ;
}

// Enum versus nonconst single/multibit values
VhdlValue *VhdlNonconst::ReverseBinaryOper_Enum(VhdlEnum *left, unsigned oper_type, const VhdlTreeNode *from)
{
    return ReverseBinaryOper(left->ToNonconst(), oper_type, from) ;
}

VhdlValue *VhdlNonconstBit::ReverseBinaryOper_Enum(VhdlEnum *left, unsigned oper_type, const VhdlTreeNode *from)
{
    return ReverseBinaryOper(left->ToNonconstBit(), oper_type, from) ;
}

VhdlValue *VhdlEnum::ReverseBinaryOper_Nonconst(VhdlNonconst *left, unsigned oper_type, const VhdlTreeNode *from)
{
    return ToNonconst()->ReverseBinaryOper(left, oper_type, from) ;
}

VhdlValue *VhdlEnum::ReverseBinaryOper_NonconstBit(VhdlNonconstBit *left, unsigned oper_type, const VhdlTreeNode *from)
{
    return ToNonconstBit()->ReverseBinaryOper(left, oper_type, from) ;
}

VhdlValue *VhdlEnum::ReverseBinaryOper_Enum(VhdlEnum *left, unsigned oper_type, const VhdlTreeNode *from)
{
    VhdlValue *result = 0 ;

    if (_boolean_type) {
        switch (oper_type) {
        case VHDL_EQUAL : result = new VhdlEnum(_boolean_type->GetEnumAt((left->Position() == Position()) ? 1 : 0)) ; break ;
        case VHDL_NEQUAL: result = new VhdlEnum(_boolean_type->GetEnumAt((left->Position() != Position()) ? 1 : 0)) ; break ;
        case VHDL_STHAN : result = new VhdlEnum(_boolean_type->GetEnumAt((left->Position() < Position())  ? 1 : 0)) ; break ;
        case VHDL_GTHAN : result = new VhdlEnum(_boolean_type->GetEnumAt((left->Position() > Position())  ? 1 : 0)) ; break ;
        case VHDL_GEQUAL: result = new VhdlEnum(_boolean_type->GetEnumAt((left->Position() >= Position()) ? 1 : 0)) ; break ;
        case VHDL_SEQUAL: result = new VhdlEnum(_boolean_type->GetEnumAt((left->Position() <= Position()) ? 1 : 0)) ; break ;
        default: break ;
        }
        if (result) { delete left ; delete this ; return result ; }
    }

    VhdlIdDef *enum_type = GetType() ;
    if (enum_type) {
        // VIPER #5918 : VHDL-2008 LRM section 9.2.3 specified matching relational operators
        // operator      Operation       Operand-type            Result-type
        //==================================================================
        //   ?=          Matching        bit or std_ulogic       Same type
        //               equality        1D array of bit or      Element type
        //                               std_ulogic
        //
        //   ?/=         Matching        bit or std_ulogic       Same type
        //               inequality      1D array of bit or      Element type
        //                               std_ulogic
        //
        // ?<, ?<=,      Matching        bit or std_ulogic       Same type
        // ?>, ?>=       ordering
        unsigned need_invert = 0 ;
        switch (oper_type) {
        case VHDL_matching_nequal: need_invert = 1 ;
        case VHDL_matching_equal:
        {
            result = (enum_type->IsStdULogicType()) ? new VhdlEnum(enum_type->GetEnumAt(std_ulogic_matching_eq_table[left->Position()][Position()])) : new VhdlEnum(enum_type->GetEnumAt((left->Position() == Position()) ? 1 : 0)) ;
            break ;
        }
        case VHDL_matching_gequal: need_invert = 1 ;
        case VHDL_matching_sthan:
        {
            result = (enum_type->IsStdULogicType()) ? new VhdlEnum(enum_type->GetEnumAt(std_ulogic_matching_lthan_table[left->Position()][Position()])) : new VhdlEnum(enum_type->GetEnumAt((left->Position() < Position()) ? 1 : 0)) ;
            break ;
        }
        case VHDL_matching_gthan: need_invert = 1 ;
        case VHDL_matching_sequal:
        {
            if (!enum_type->IsStdULogicType()) {
                result = new VhdlEnum(enum_type->GetEnumAt((left->Position() <= Position()) ? 1 : 0)) ;
            } else {
                // result = is_less_than || is_equal
                result = new VhdlEnum(enum_type->GetEnumAt(std_ulogic_matching_lthan_table[left->Position()][Position()])) ;
                VhdlEnum *is_equal = new VhdlEnum(enum_type->GetEnumAt(std_ulogic_matching_eq_table[left->Position()][Position()])) ;
                result = result->BinaryOper(is_equal, VHDL_or, from) ;
            }
            break ;
        }
        default: break ;
        }
        if (result) {
            delete left ; delete this ;
            return need_invert ? result->UnaryOper(VHDL_not,from) : result ;
        }
    }

    if (IsBit()) {
        // Single-bit values : go via nonconstbit to handle events
        return ToNonconstBit()->ReverseBinaryOper(left->ToNonconstBit(),oper_type,from) ;
    } else {
        // Multi-bit values : go via nonconst
        return ToNonconst()->ReverseBinaryOper(left->ToNonconst(),oper_type,from) ;
    }
}

// Double<->Int
VhdlValue *VhdlInt::ReverseBinaryOper_Double(VhdlDouble *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }
    // VIPER 3378 :
    // VHDL allows a few operations between integer and float.
    // All physical types should always generate 'integer' values, since physical types are numeric.
    //     (physical_type) * (REAL) => (physical_type)
    //     (REAL) * (physical_type) => (physical_type)
    //     (physical_type) / (REAL) => (physical_type)
    //     (float_type) ** INTEGER => (float_type)
    // Unfortunately, LRM 7.5 also mentions int <->  real operations that should return real :
    //     (universal_real) * (universal_integer) => (universal_real)
    //     (universal_integer) * (universal_real) => (universal_real)
    //     (universal_real) / (universal_integer) => (universal_real)
    // So, we need a way to distinguish between a 'integer_type' and a 'physical_type' here at the value level.
    // Type VhdlPhysicalValue was introduced for that reason.
    // VIPER 2859 included that addition, which now correctly catches Physical type operations.
    // So here we only now deal with only the universal operators (last 3 on the list).
    switch (oper_type) {
    case VHDL_STAR :
        //     (REAL) * (physical_type) => (physical_type)
        left->_value = left->_value * _value ;
        break ;
    case VHDL_SLASH :
        if (_value==0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        left->_value = left->_value / _value ;
        break ;
    case VHDL_EXPONENT :
        {
        // integer exponent on double value
        double val = 1 ;
        verific_int64 exp = _value ;
        if (exp > 0) {
            while (exp-- != 0) val = val * left->_value ;
        } else if (exp < 0) {
            while (exp++ != 0) val = val / left->_value ;
        }
        left->_value = val ;
        }
        break ;
    default :
        // Could happen for physical types ?
        from->Error("operation on a REAL number not supported") ;
        break ;
    }
    delete this ;
    return left ;
}

// Int<->Double
VhdlValue *VhdlDouble::ReverseBinaryOper_Int(VhdlInt *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }
    // LRM 7.5 states INTEGER - REAL -> REAL.
    // So we should return 'this' (the REAL).
    // We cast the integer to real first.
    switch (oper_type) {
    case VHDL_STAR :
        _value = ((double)left->_value) * _value ;
        break ;
    case VHDL_SLASH :
        // integer divided by real. Can this happen in VHDL ?
        // Possibly for physical types...
        if (_value == 0.0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        _value = ((double)left->_value) / _value ;
        break ;
    default :
        // Could happen for physical types ?
        from->Error("operation on a REAL number not supported") ;
        break ;
    }
    delete left ;
    return this ;
}

// Int<->Int
VhdlValue *VhdlInt::ReverseBinaryOper_Int(VhdlInt *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    VhdlValue *result = 0 ;

    switch (oper_type) {
    case VHDL_EQUAL : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value == _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value == _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_NEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value != _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value != _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_STHAN : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value < _value)  ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value < _value) ? Pwr() : Gnd())  ;  break ;
    case VHDL_GTHAN : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value > _value)  ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value > _value) ? Pwr() : Gnd())  ;  break ;
    case VHDL_GEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value >= _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value >= _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_SEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value <= _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value <= _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_minimum:  result = (left->_value < _value) ? left->Copy() : Copy() ; break ; // 2008 LRM 5.2.6
    case VHDL_maximum:  result = (left->_value > _value) ? left->Copy() : Copy() ; break ; // 2008 LRM 5.2.6
    default: break ;
    }
    if (result) { delete left ; delete this ; return result ; }

    switch (oper_type) {
    case VHDL_MINUS : _value = left->_value - _value ; result = this ; break ;
    case VHDL_PLUS :  _value = left->_value + _value ; result = this ; break ;
    case VHDL_STAR :  _value = left->_value * _value ; result = this ; break ;
    case VHDL_SLASH :
        if (_value==0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        _value = left->_value / _value ;
        result = this ;
        break ;
    case VHDL_mod :
        {
        if (_value==0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        //
        // Don't work with C++ interpretation of % is arguments are negative
        // Do the exact LRM complience work here :
        //
        // A mod B has sign of B and
        // A mod B absolute value less than absolute value of B
        //
        // Also :    A mod B = A - B*N    (N being a integer)
        //
        // if A<0 :   A mod B = -(-A) - B*N
        //                    = - (((-A)mod B) + B*M ) - B*N
        //                    = - ((-A)mod B)  - B(N-M)
        //
        // So if A < 0, take negative of the result with a positive A
        // and then possibly adjust the result to reflect the sign of B in the result

        verific_int64 a = left->_value ;
        verific_int64 b = _value ;
        // Calculate the modulo of the absolute values of these guys :
        verific_int64 r = ABSOLUTE(a) % ABSOLUTE(b) ;
        // Calculations above : if A is negative, we need the negative
        // of (-A mod B) :
        if (a<0) r = -r ;
        // Next :
        // Might have to adjust the result : sign needs to reflect sign of B :
        if (r<0 && b>0) r = r + b ; // add b to get positive
        if (r>0 && b<0) r = r + b ; // add b (subtract abs(b)) to get negative
        // These last two checks are the difference with REM

        // Set this value as result
        _value = r ;
        result = this ;
        break ;
        }
    case VHDL_rem :
        {
        if (_value==0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        //
        // Don't work with C++ interpretation of % is arguments are negative
        // Do the exact LRM complience work here :
        //
        // A mod B has sign of B and
        // A mod B absolute value less than absolute value of B
        //
        // Also :    A rem B = A - (A/B)*B
        //
        // if A<0 :   A rem B = (-A) - (-A/B)*B
        //                    = A - (A/B)*B - 2*(A - (A/B)*B)
        //                    = (-A) rem B   - 2((-A) rem B)
        //                    = -( (-A) rem B) )
        // So if A < 0, take negative of the result with a positive A
        // That is also the result, since the absolute value fits in
        // the absolute value of b (otherwise the % operator did not work)
        //
        verific_int64 a = left->_value ;
        verific_int64 b = _value ;
        // Calculate the modulo of the absolute values of these guys :
        verific_int64 r = ABSOLUTE(a) % ABSOLUTE(b) ;
        // Calculations above : if A is negative, we need the negative
        // of (-A mod B) :
        if (a<0) r = -r ;
        // Done. Sign automativally reflects the sign of A
        // (this is the difference with MOD)
        _value = r ;
        result = this ;
        break ;
        }
    default : break ;
    }
    if (result) { delete left ; return result ; }

    if (oper_type==VHDL_EXPONENT) { // TODO : Merge this with previous case stmt.
        verific_int64 val = 1 ;
        verific_int64 exp = _value ;
        if (exp > 0) {
            while (exp-- != 0) val = val * left->_value ;
        } else if (exp < 0) {
            if (left->_value == 1) { // Viper : 3130
                val = 1 ; // 1 ** anything is 1
            } else {
                // negative exponent on integer value. Error according to LRM 7.2.7
                if (from) from->Error("exponentiation with negative exponent is only allowed for REAL values") ;
            }
        }
        _value = val ;
        delete left ;
        return this ;
    }
    if (from) from->Error("operator %s not defined on integer values", VhdlTreeNode::OperatorName(oper_type)) ;
    delete left ;
    delete this ;
    return 0 ;
}

// Double<->Double
VhdlValue *VhdlDouble::ReverseBinaryOper_Double(VhdlDouble *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    VhdlValue *result = 0 ;

    switch (oper_type) {
    case VHDL_EQUAL : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->GetValue() == GetValue()) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->GetValue() == GetValue()) ? Pwr() : Gnd()) ;  break ;
    case VHDL_NEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->GetValue() != GetValue()) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->GetValue() != GetValue()) ? Pwr() : Gnd()) ;  break ;
    case VHDL_STHAN : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->GetValue() < GetValue())  ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->GetValue() < GetValue()) ? Pwr() : Gnd())  ;  break ;
    case VHDL_GTHAN : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->GetValue() > GetValue())  ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->GetValue() > GetValue()) ? Pwr() : Gnd())  ;  break ;
    case VHDL_GEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->GetValue() >= GetValue()) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->GetValue() >= GetValue()) ? Pwr() : Gnd()) ;  break ;
    case VHDL_SEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->GetValue() <= GetValue()) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->GetValue() <= GetValue()) ? Pwr() : Gnd()) ;  break ;
    case VHDL_minimum:  result = (left->GetValue() < GetValue()) ? left->Copy() : Copy() ; break ; // 2008 LRM 5.2.6
    case VHDL_maximum:  result = (left->GetValue() > GetValue()) ? left->Copy() : Copy() ; break ; // 2008 LRM 5.2.6
    default: break ;
    }

    if (result) { delete left ; delete this ; return result ; }

    switch (oper_type) {
    case VHDL_MINUS : _value = left->_value - _value ; result = this ; break ;
    case VHDL_PLUS :  _value = left->_value + _value ; result = this ; break ;
    case VHDL_STAR :  _value = left->_value * _value ; result = this ; break ;
    case VHDL_SLASH :
        if (_value==0.0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1.0 ;
        }
        _value = left->_value / _value ; result = this ;
        break ;
    default: break ;
    }
    if (result) { delete left ; return result ; }

    if (from) from->Error("operator %s not defined on real values", VhdlTreeNode::OperatorName(oper_type)) ;
    delete left ;
    delete this ;
    return 0 ;
}

// Int<->Physical
VhdlValue *VhdlPhysicalValue::ReverseBinaryOper_Int(VhdlInt *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    // VHDL allows only * with left-integer <-> right-physical arguments: return type is physical

    VhdlValue *result = 0 ;
    switch (oper_type) {
    case VHDL_STAR :  _value = left->_value * _value ; result = this ; break ;
    default: break ;
    }
    if (result) { delete left ; return result ; }

    if (from) from->Error("operator %s not defined on physical values", VhdlTreeNode::OperatorName(oper_type)) ;
    delete left ;
    delete this ;
    return 0 ;
}

// Physical<->Int
VhdlValue *VhdlInt::ReverseBinaryOper_Physical(VhdlPhysicalValue *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    // VHDL allows only *, / with left-physical <-> right-integer arguments: return type is physical

    VhdlValue *result = 0 ;
    switch (oper_type) {
    case VHDL_STAR :  left->_value = left->_value * _value ; result = left ; break ;
    case VHDL_SLASH :
        if (_value==0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        left->_value = left->_value / _value ;
        result = left ;
        break ;
    default: break ;
    }
    if (result) { delete this ; return result ; }

    if (from) from->Error("operator %s not defined on physical values", VhdlTreeNode::OperatorName(oper_type)) ;
    delete left ;
    delete this ;
    return 0 ;
}

// Double<->Physical
VhdlValue *VhdlPhysicalValue::ReverseBinaryOper_Double(VhdlDouble *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    // VHDL allows only * with left-real <-> right-physical arguments: return type is physical

    VhdlValue *result = 0 ;
    switch (oper_type) {
    case VHDL_STAR : _value = (verific_int64)((double)left->_value * _value) ; result = this ; break ;
    default : break ;
    }
    if (result) { delete left ; return result ; }

    if (from) from->Error("operation on a REAL number not supported") ;
    delete this ;
    delete left ;
    return 0 ;
}

// Physical<->Double
VhdlValue *VhdlDouble::ReverseBinaryOper_Physical(VhdlPhysicalValue *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    // VHDL allows only *, / with left-physical <-> right-real arguments: return type is physical

    VhdlValue *result = 0 ;
    switch (oper_type) {
    case VHDL_STAR : left->_value = (verific_int64)((double)left->_value * _value) ; result = left ; break ;
    case VHDL_SLASH :
        if (_value==0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        left->_value = (verific_int64)((double)left->_value / _value) ;
        result = left ;
        break ;
    default : break ;
    }
    if (result) { delete this ; return result ; }

    if (from) from->Error("operation on a REAL number not supported") ;
    delete this ;
    delete left ;
    return 0 ;
}

// Physical<->Physical
VhdlValue *VhdlPhysicalValue::ReverseBinaryOper_Physical(VhdlPhysicalValue *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    // VIPER #3409: VHDL allows only the following left-physical <-> right-physical arguments with these return type:
    //     Relational operators   : =, /=, , <, <=, >, >= : return type is boolean
    //     Adding operators    : +, -                  : return type is physical
    //     Multiplying operator: /                     :return type is universal integer

    VhdlValue *result = 0 ;
    switch (oper_type) {
    case VHDL_EQUAL : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value == _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value == _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_NEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value != _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value != _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_STHAN : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value < _value)  ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value < _value) ? Pwr() : Gnd())  ;  break ;
    case VHDL_GTHAN : (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value > _value)  ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value > _value) ? Pwr() : Gnd())  ;  break ;
    case VHDL_GEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value >= _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value >= _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_SEQUAL: (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt((left->_value <= _value) ? 1 : 0)) :
                                             result = new VhdlNonconstBit((left->_value <= _value) ? Pwr() : Gnd()) ;  break ;
    case VHDL_minimum:  result = (left->_value < _value) ? left->Copy() : Copy() ; break ; // 2008 LRM 5.2.6
    case VHDL_maximum:  result = (left->_value > _value) ? left->Copy() : Copy() ; break ; // 2008 LRM 5.2.6
    default: break ;
    }
    if (result) { delete left ; delete this ; return result ; }

    switch (oper_type) {
    case VHDL_PLUS :  _value = left->_value + _value ; result = this ; break ;
    case VHDL_MINUS : _value = left->_value - _value ; result = this ; break ;
    case VHDL_SLASH :
        if (_value==0) {
            if (from) from->Error("attempt to divide by 0") ;
            _value = 1 ;
        }
        // Physical value divided by another physical value yields an universal integer value:
        result = new VhdlInt(left->_value / _value) ;
        delete this ;
        break ;
    default : break ;
    }
    if (result) { delete left ; return result ; }

    if (from) from->Error("operator %s not defined on physical values", VhdlTreeNode::OperatorName(oper_type)) ;
    delete left ;
    delete this ;
    return 0 ;
}

// Nonconst<->Nonconst
VhdlValue *VhdlNonconst::ReverseBinaryOper_Nonconst(VhdlNonconst *left, unsigned oper_type, const VhdlTreeNode *from)
{
    if (!left) { delete this ; return 0 ; }

    switch (oper_type) {
    case VHDL_nand      : return Binary(left, oper_type, from) ;
    case VHDL_nor       : return Binary(left, oper_type, from) ;
    case VHDL_and       : return Binary(left, oper_type, from) ;
    case VHDL_or        : return Binary(left, oper_type, from) ;
    case VHDL_xor       : return Binary(left, oper_type, from) ;
    case VHDL_xnor      : return Binary(left, oper_type, from) ;
    case VHDL_matching_equal:
    case VHDL_EQUAL     : return Equal(left, 0, from) ;
    case VHDL_matching_nequal:
    case VHDL_NEQUAL    : return Equal(left, 1, from) ;
    case VHDL_matching_sthan:
    case VHDL_STHAN     : return left->LessThan(this,0, from) ;
    case VHDL_matching_gthan:
    case VHDL_GTHAN     : return LessThan(left,0, from) ;
    case VHDL_matching_gequal:
    case VHDL_GEQUAL    : return LessThan(left,1, from) ;
    case VHDL_matching_sequal:
    case VHDL_SEQUAL    : return left->LessThan(this,1, from) ;
    case VHDL_sll       : return left->Shift(this,0,0,0, from) ;
    case VHDL_srl       : return left->Shift(this,1,0,0, from) ;
    case VHDL_rol       : return left->Shift(this,0,0,1, from) ;
    case VHDL_ror       : return left->Shift(this,1,0,1, from) ;
    case VHDL_sla       : return left->Shift(this,0,1,0, from) ;
    case VHDL_sra       : return left->Shift(this,1,1,0, from) ;
    case VHDL_MINUS     : return left->Minus(this, from) ;
    case VHDL_PLUS      : return left->Plus(this, Gnd(), from) ;
    case VHDL_STAR      : return left->Multiply(this, from) ;
    case VHDL_SLASH     : return left->Divide(this, from) ;
    case VHDL_mod       : return left->Modulo(this, from) ;
    case VHDL_rem       : return left->Remainder(this, from) ;
    case VHDL_AMPERSAND : break ; // Should not go through this routine
    case VHDL_EXPONENT  : return left->Power(this, from) ;
    case VHDL_minimum   :  // 2008 LRM 5.3.2.4 and 5.2.6
    {
        // cond = left < this
        VhdlValue *cond = left->Copy()->BinaryOper(Copy(), VHDL_STHAN, from) ;
        if (!cond) break ;

        if (cond->IsConstant()) {
            unsigned is_true = cond->IsTrue() ;
            delete cond ;

            if (is_true) {
                delete this ;
                return left ;
            } else {
                delete left ;
                return this ;
            }
        }

        VhdlNonconstBit *cond_bit = cond->ToNonconstBit() ;
        VhdlValue *result = SelectOn1(cond_bit, left, 0, from) ;
        delete cond_bit ;
        return result ;
    }
    case VHDL_maximum:  // 2008 LRM 5.3.2.4 and 5.2.6
    {
        // cond = this < left
        VhdlValue *cond = Copy()->BinaryOper(left->Copy(), VHDL_STHAN, from) ;
        if (!cond) break ;

        if (cond->IsConstant()) {
            unsigned is_true = cond->IsTrue() ;
            delete cond ;

            if (is_true) {
                delete this ;
                return left ;
            } else {
                delete left ;
                return this ;
            }
        }

        VhdlNonconstBit *cond_bit = cond->ToNonconstBit() ;
        VhdlValue *result = SelectOn1(cond_bit, left, 0, from) ;
        delete cond_bit ;
        return result ;
    }
    default             : break ;
    }
    if (from) from->Error("operator %s not supported on these values", VhdlTreeNode::OperatorName(oper_type)) ;
    delete this ;
    delete left ;
    return 0 ;
}

// Nonconstbit<->Nonconstbit
VhdlValue *VhdlNonconstBit::ReverseBinaryOper_NonconstBit(VhdlNonconstBit *left, unsigned oper_type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_net) ;

    if (!left) { delete this ; return 0 ; }

    switch (oper_type) {
    case VHDL_nand :
    case VHDL_nor :
    case VHDL_and :
    case VHDL_or :
    case VHDL_xor :
    case VHDL_xnor :  return left->Binary(this, oper_type, from) ;
        // Equal compare. Call 'Equal' for clock-handling
    case VHDL_matching_equal:
    case VHDL_EQUAL : return left->Equal(this, 0, from) ;
    case VHDL_matching_nequal:
    case VHDL_NEQUAL: return left->Equal(this, 1, from) ;
        // magnitude compare on bits
        // a < b  :  b and NOT(a)
        // a <= b :  b or NOT(a)
    case VHDL_matching_sthan:
    case VHDL_STHAN:  return left->Invert(from)->Binary(this, VHDL_and, from) ;
    case VHDL_matching_gthan:
    case VHDL_GTHAN:  return Invert(from)->Binary(left, VHDL_and, from) ;
    case VHDL_matching_sequal:
    case VHDL_SEQUAL: return left->Invert(from)->Binary(this, VHDL_or, from) ;
    case VHDL_matching_gequal:
    case VHDL_GEQUAL: return Invert(from)->Binary(left, VHDL_or, from) ;
    case VHDL_minimum:  // 2008 LRM 5.3.2.4 and 5.2.6
    {
        // cond = left < this
        VhdlValue *cond = left->Copy()->BinaryOper(Copy(), VHDL_STHAN, from) ;
        if (!cond) break ;

        if (cond->IsConstant()) {
            unsigned is_true = cond->IsTrue() ;
            delete cond ;

            if (is_true) {
                delete this ;
                return left ;
            } else {
                delete left ;
                return this ;
            }
        }

        VhdlNonconstBit *cond_bit = cond->ToNonconstBit() ;
        VhdlValue *result = SelectOn1(cond_bit, left, 0, from) ;
        delete cond_bit ;
        return result ;
    }
    case VHDL_maximum:  // 2008 LRM 5.3.2.4 and 5.2.6
    {
        // cond = this < left
        VhdlValue *cond = Copy()->BinaryOper(left->Copy(), VHDL_STHAN, from) ;
        if (!cond) break ;

        if (cond->IsConstant()) {
            unsigned is_true = cond->IsTrue() ;
            delete cond ;

            if (is_true) {
                delete this ;
                return left ;
            } else {
                delete left ;
                return this ;
            }
        }

        VhdlNonconstBit *cond_bit = cond->ToNonconstBit() ;
        VhdlValue *result = SelectOn1(cond_bit, left, 0, from) ;
        delete cond_bit ;
        return result ;
    }
    default : break ;
    }

    // Don't allow any other operations on nonconstbit
    if (from) from->Error("operator %s not supported on these values", VhdlTreeNode::OperatorName(oper_type)) ;
    delete this ;
    delete left ;
    return 0 ;
}

// Composite<->Composite
VhdlValue *VhdlCompositeValue::ReverseBinaryOper_Composite(VhdlCompositeValue *left, unsigned oper_type, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

    if (!left) { delete this ; return 0 ; }

    // Composite <-> Composite
    unsigned size ;

    switch (oper_type) {
    case VHDL_nand :
    case VHDL_nor :
    case VHDL_and :
    case VHDL_or :
    case VHDL_xor :
    case VHDL_xnor :
        if (NumElements() != left->NumElements()) {
            if (from) from->Error("unequal length arguments for operator %s",VhdlTreeNode::OperatorName(oper_type)) ;
            delete this ; delete left ;
            return 0 ;
        }
        // go via nonconst and back
        size = NumElements() ;
        return (ToNonconst()->Binary(left->ToNonconst(), oper_type, from))->ToComposite(size) ;

    case VHDL_NEQUAL :
    case VHDL_EQUAL :
        {
        // VHDL semantics
        if (NumElements() != left->NumElements()) {
            // Issue 2291 : possibly we need to use GTE/STE array truncation/extension (below) rules to make them equal length...
            VhdlValue *result = 0 ;
            if (oper_type==VHDL_EQUAL) { // equal : return FALSE
                if (from) from->Warning("comparison between unequal length arrays always returns %s", "FALSE") ;

                if (_boolean_type) {
                    result = new VhdlEnum(_boolean_type->GetEnumAt(0)) ; // FALSE
                } else {
                    result = new VhdlNonconstBit(Gnd()) ; // 0
                }
            } else { // not-equal : return TRUE
                if (from) from->Warning("comparison between unequal length arrays always returns %s", "TRUE") ;

                if (_boolean_type) {
                    result = new VhdlEnum(_boolean_type->GetEnumAt(1)) ; // TRUE
                } else {
                    result = new VhdlNonconstBit(Pwr()) ; // 1
                }
            }
            delete this ;
            delete left ;
            return result ;
        }

        // Issue 2342 : cannot serialize an aggregate value. Do element by element :

        // Check if the element is a bit. If so, go via 'nonconst' for the vector.
        // Otherwise, have to do this element-by-element
        //VhdlValue *elem = (_values->Size()) ? (VhdlValue*)_values->At(0) : 0 ;
        VhdlValue *elem = ValueAt(0) ; // VIPER #6777: Get via ValueAt() to apply default value
        if (!elem || elem->IsBit()) {
            // Element is bit-encoded. Use 'vector' compare
            switch (oper_type) {
            case VHDL_EQUAL :  return Equal(left,0, from) ;
            case VHDL_NEQUAL :  return Equal(left,1, from) ;
            default : break ;
            }
        } else {
            // Element is not bit-encoded.
            unsigned i ;
            VhdlValue *left_elem ;
            VhdlValue *equal ; // element-by-element compare

            // Go element-by-element, LSB->MSB (right to left) (consistent with magnitude compare).
            // Set initial carry value to PWR. We will calculate (=) on each element and AND it
            // Start with result TRUE :
            VhdlValue *carry = 0 ;
            if (_boolean_type) {
                carry = new VhdlEnum(_boolean_type->GetEnumAt(1)) ;
            } else {
                carry = new VhdlNonconstBit(Pwr()) ;
            }
            FOREACH_ARRAY_ITEM_BACK(_values, i, elem) {
                if (!elem) elem = _default_elem_val ; // VIPER #6777: Default value applies
                left_elem = left->ValueAt(i) ;
                if (!elem || !left_elem) continue ; // CHECK ME error ?

                // Calculate left-element "==" this(right)-elem
                equal = left_elem->Copy()->BinaryOper(elem->Copy(), VHDL_EQUAL, from) ;
                if (!equal) continue ; // CHECK ME error ?
                // Now calculate the overall carry : (carry AND elem"==")
                carry = carry->BinaryOper(equal, VHDL_and, from) ;
            }
            // Now invert the result for "!="
            carry = (oper_type==VHDL_NEQUAL) ? carry->UnaryOper(VHDL_not,from) : carry ;
            delete left ; delete this ; return carry ;
        }

        // Unlike Verilog, VHDL = is always 'exact compare'
        // return ToNonconst()->Equal(left->ToNonconst(),0, from) ;
        return 0 ;
        }

    case VHDL_matching_nequal:
    case VHDL_matching_equal:
    {
        // Viper #5918: Vhdl-2008 : LRM 9.2.3
        unsigned is_neq = (oper_type == VHDL_matching_nequal) ;
        //VhdlValue *elem = (_values->Size()) ? (VhdlValue*)_values->At(0) : 0 ;
        VhdlValue *elem = ValueAt(0) ; // VIPER #6777: Get via ValueAt() to apply default value
        VhdlIdDef *enum_type = elem ? elem->GetType() : 0 ;
        if (!enum_type) enum_type = StdType("bit") ;
        unsigned is_std_logic = enum_type ? enum_type->IsStdULogicType() : 0 ;
        if (enum_type && (NumElements() != left->NumElements())) {
            VhdlValue *result = 0 ;
            result = is_std_logic ? new VhdlEnum(enum_type->GetEnumAt(is_neq ? 3 : 2)) : new VhdlEnum(enum_type->GetEnumAt(is_neq ? 1 : 0)) ;
            delete this ;
            delete left ;
            return result ;
        }

        unsigned i ;
        VhdlValue *left_elem ;
        VhdlValue *equal ; // element-by-element compare
        VhdlValue *carry = enum_type ? (is_std_logic ? new VhdlEnum(enum_type->GetEnumAt(3)) : new VhdlEnum(enum_type->GetEnumAt(1))) : 0 ;

        FOREACH_ARRAY_ITEM_BACK(_values, i, elem) {
            if (!elem) elem = _default_elem_val ; // VIPER #6777: Default value applies
            left_elem = left->ValueAt(i) ;
            if (!elem || !left_elem) continue ; // CHECK ME error ?

            // Calculate left-element "==" this(right)-elem
            equal = left_elem->Copy()->BinaryOper(elem->Copy(), VHDL_matching_equal, from) ;
            if (!equal) continue ; // CHECK ME error ?
            // Now calculate the overall carry : (carry AND elem"==")
            carry = carry ? carry->BinaryOper(equal, VHDL_and, from) : 0 ;
        }
        // Now invert the result for "!="
        carry = is_neq ? (carry ? carry->UnaryOper(VHDL_not,from) : 0) : carry ;
        delete left ; delete this ; return carry ;
    }
    case VHDL_minimum:  // 2008 LRM 5.3.2.4 and 5.2.6
    {
        // cond = left < this
        VhdlValue *cond = left->Copy()->BinaryOper(Copy(), VHDL_STHAN, from) ;
        if (!cond) break ;

        if (cond->IsConstant()) {
            unsigned is_true = cond->IsTrue() ;
            delete cond ;

            if (is_true) {
                delete this ;
                return left ;
            } else {
                delete left ;
                return this ;
            }
        }

        VhdlNonconstBit *cond_bit = cond->ToNonconstBit() ;
        VhdlValue *result = SelectOn1(cond_bit, left, 0, from) ;
        delete cond_bit ;
        return result ;
    }
    case VHDL_maximum:  // 2008 LRM 5.3.2.4 and 5.2.6
    {
        // cond = this < left
        VhdlValue *cond = Copy()->BinaryOper(left->Copy(), VHDL_STHAN, from) ;
        if (!cond) break ;

        if (cond->IsConstant()) {
            unsigned is_true = cond->IsTrue() ;
            delete cond ;

            if (is_true) {
                delete this ;
                return left ;
            } else {
                delete left ;
                return this ;
            }
        }

        VhdlNonconstBit *cond_bit = cond->ToNonconstBit() ;
        VhdlValue *result = SelectOn1(cond_bit, left, 0, from) ;
        delete cond_bit ;
        return result ;
    }
    case VHDL_GTHAN :
    case VHDL_STHAN :
    case VHDL_GEQUAL :
    case VHDL_SEQUAL :
        {
        // array magnitude compare.
        // NULL arrays work fine, according to LRM 7.2.2 rules.
        if (NumElements() != left->NumElements()) {
            // Unequal sized : compare the LEFT part that overlaps.
            unsigned left_is_smaller = (left->NumElements() < NumElements()) ? 1 : 0 ;
            unsigned smallest = (left_is_smaller) ? left->NumElements() : NumElements() ;
            unsigned largest = (left_is_smaller) ? NumElements() : left->NumElements() ;

            // Remove excess values from the largest array
            // Remove from the RIGHT side. (we end up comparing the left side)
            // Strip from the end (is faster)
            unsigned i = largest ;
            while (i-- != smallest) { // Loop works if smallest == 0
                if (left_is_smaller) {
                    RemoveValueAt(i) ; // Strip from right argument
                } else {
                    left->RemoveValueAt(i) ; // Strip from left argument
                }
            }
            VERIFIC_ASSERT (NumElements() == left->NumElements()) ;

            // Now compare the two (now equal sized) arguments
            // Need to change the functions :
            //
            // Since unequal sized arguments are never the same in value, > and >= will
            // start to behave the same. Also for < and <=.
            //
            // Also, > will become >=, if the right argument was larger (num elements) (since
            // if the left argument is equal to stripped right argument,
            // the right argument wins (since it had more elements). So a > b becomes a >= b.
            //
            // These are the rules of LRM 7.2.2 for discrete array type magnitude compares.
            // This leads to the following function transformations.
            switch (oper_type) {
            case VHDL_GTHAN :  oper_type = (left_is_smaller) ? VHDL_GTHAN  : VHDL_GEQUAL ; break ;
            case VHDL_STHAN :  oper_type = (left_is_smaller) ? VHDL_SEQUAL : VHDL_STHAN ; break ;
            case VHDL_GEQUAL : oper_type = (left_is_smaller) ? VHDL_GTHAN  : VHDL_GEQUAL ; break ;
            case VHDL_SEQUAL : oper_type = (left_is_smaller) ? VHDL_SEQUAL : VHDL_STHAN ; break ;
            default : break ; // should not happen.
            }

            // They are equal sizes now. Execute normally ..
        }
        // equal sizes
        // Check if the element is a bit. If so, go via 'nonconst' for the vector.
        // Otherwise, have to do this element-by-element
        //VhdlValue *elem = (_values->Size()) ? (VhdlValue*)_values->At(0) : 0 ;
        VhdlValue *elem = ValueAt(0) ; // VIPER #6777: Get via ValueAt() to apply default value
        if (!elem || elem->IsBit()) {
            // Element is bit-encoded. Use 'vector' compare
            switch (oper_type) {
            case VHDL_STHAN :  return left->ToNonconst()->LessThan(ToNonconst(),0, from) ;
            case VHDL_GTHAN :  return ToNonconst()->LessThan(left->ToNonconst(),0, from) ;
            case VHDL_GEQUAL:  return ToNonconst()->LessThan(left->ToNonconst(),1, from) ;
            case VHDL_SEQUAL:  return left->ToNonconst()->LessThan(ToNonconst(),1, from) ;
            default : break ;
            }
        } else {
            // Element is not bit-encoded.
            unsigned i ;
            VhdlValue *left_elem ;
            VhdlValue *lessthan, *equal ; // element-by-element compare

            // Go element-by-element, LSB->MSB (right to left)
            // Set initial carry value. We will calculate < or <= carry (=) is true if we
            // look for > (NOT <=) or <= .  It is false for < (<) or >= (NOT(<)
            VERIFIC_ASSERT(Pwr() && Gnd()) ; // FIX ME: still don't know if Pwr()/Gnd() will always be valid here
            VhdlNonconstBit *carry = new VhdlNonconstBit((oper_type==VHDL_GTHAN || oper_type==VHDL_SEQUAL) ? VhdlTreeNode::Pwr() : VhdlTreeNode::Gnd()) ;
            FOREACH_ARRAY_ITEM_BACK(_values, i, elem) {
                if (!elem) elem = _default_elem_val ; // VIPER #6777: Default value applies
                left_elem = left->ValueAt(i) ;
                if (!elem || !left_elem) continue ; // CHECK ME error ?

                // Calculate this-element"==" and this-element"<"
                lessthan = left_elem->Copy()->BinaryOper(elem->Copy(), VHDL_STHAN, from) ;
                equal = left_elem->Copy()->BinaryOper(elem->Copy(), VHDL_EQUAL, from) ;
                if (!lessthan || !equal) continue ; // CHECK ME error ?
                // Now calculate the overall carry : (carry-in AND elem"==") OR elem"<"
                carry = lessthan->ToNonconstBit()->Binary( carry->Binary(equal->ToNonconstBit(), VHDL_and, from), VHDL_or, from) ;
            }
            // Now invert the result for > (NOT <=) and for >= (NOT <)
            carry = (oper_type==VHDL_GTHAN || oper_type==VHDL_GEQUAL) ? carry->Invert(from) : carry ;
            delete left ; delete this ; return carry ;
        }
        return 0 ;
        }

    case VHDL_AMPERSAND : break ; // Should not end-up here
    default : break ;
    }

    if (from) from->Error("operator %s not supported on this value", VhdlTreeNode::OperatorName(oper_type)) ;
    delete this ;
    delete left ;
    return 0 ;
}

/**********************************************/
// Index into composite value
/**********************************************/

VhdlValue *VhdlValue::ValueAt(unsigned /*pos*/) const           { return 0 ; }
VhdlValue *VhdlCompositeValue::ValueAt(unsigned pos) const
{
    VERIFIC_ASSERT(_values) ;

    if (pos >= _values->Size()) return 0 ;
    VhdlValue *elem = (VhdlValue*)_values->At(pos) ;
    if (!elem) elem = _default_elem_val ; // default applies
    return elem ;
}
VhdlValue *VhdlAccessValue::ValueAt(unsigned pos) const
{
    return _value ? _value->ValueAt(pos) : 0 ;
}

VhdlValue *VhdlValue::TakeValueAt(unsigned /*pos*/) const       { return 0 ; }
VhdlValue *VhdlAccessValue::TakeValueAt(unsigned pos) const
{
    // Viper #5881: It is not safe to take value out of the
    // inner value, as it is not owned by the wrapper class
    VhdlValue *pos_value = _value ? _value->ValueAt(pos) : 0 ;
    return pos_value ? pos_value->Copy() : 0 ;
}
VhdlValue *VhdlCompositeValue::TakeValueAt(unsigned pos) const
{
    VERIFIC_ASSERT(_values) ;

    if (pos >= _values->Size()) return 0 ;
    VhdlValue *elem = (VhdlValue*)_values->At(pos) ;
    _values->Insert(pos,0) ;
    if (!elem && _default_elem_val) elem = _default_elem_val->Copy() ; // default applies
    return elem ;
}
void VhdlValue::SetValueAt(unsigned /*pos*/, VhdlValue * /*elem*/)
{
    VERIFIC_ASSERT(0) ;
}

void VhdlAccessValue::SetValueAt(unsigned pos, VhdlValue *elem)
{
    VERIFIC_ASSERT(_value) ;
    _value->SetValueAt(pos, elem) ;
}

void VhdlCompositeValue::SetValueAt(unsigned pos, VhdlValue *elem)
{
    VERIFIC_ASSERT (_values && pos < _values->Size()) ;

    VhdlValue *exist = (VhdlValue*)_values->At(pos) ;
    delete exist ;
    _values->Insert(pos,elem) ;
}

// Wire nets of a value 'elem' into the indicated position of an existing non-constant value
void VhdlValue::NetAssignAt(unsigned /*pos*/, VhdlValue * /*elem*/)
{
    VERIFIC_ASSERT(0) ;
}

void VhdlCompositeValue::NetAssignAt(unsigned pos, VhdlValue *elem)
{
    VERIFIC_ASSERT(_values) ;

    VhdlValue *exist = ValueAt(pos) ;
    if (!exist) return ; // happens in case of errors
    // FIX ME : We don't have a resolution function, nor a 'from' linefile.
    exist->NetAssign(elem, 0, 0) ; // FIX ME : a indexed concurrent assignment with a 'Z' in value or clocked, does not get line-number info in its generated logic..
}

// Remove elements from the array
void VhdlValue::RemoveValueAt(unsigned /*pos*/) { }

void VhdlCompositeValue::RemoveValueAt(unsigned pos)
{
    VERIFIC_ASSERT (_values && pos < _values->Size()) ;

    // Delete the element there :
    VhdlValue *elem = (VhdlValue*)_values->At(pos) ;
    delete elem ;
    // Remove the entry from the array :
    _values->Remove(pos,1) ;

    if (_range) { // assuming array range
        VhdlValue *low = _range->Low() ;
        VhdlValue *high = _range->High() ;

        if (low) low = low->Copy() ;
        if (high) high = high->Copy()->UnaryOper(VHDL_DECR, 0) ;

        unsigned dir = _range->Dir() ;
        unsigned is_unconstrainted = _range->IsUnconstrained() ;
        delete _range ;

        if (dir == VHDL_to) {
            _range = new VhdlRangeConstraint(low, dir, high, is_unconstrainted) ;
        } else {
            _range = new VhdlRangeConstraint(high, dir, low, is_unconstrainted) ;
        }
    }
}
void VhdlAccessValue::RemoveValueAt(unsigned pos)
{
    VERIFIC_ASSERT(_value) ;
    _value->RemoveValueAt(pos) ;

    VhdlConstraint *value_range = _value->GetRange() ;
    VhdlConstraint *index_range = _constraint ? _constraint->IndexConstraint() : 0 ;
    if (index_range && value_range) {
        index_range->SetUnconstrained() ;
        index_range->ConstraintBy(value_range) ;
    }
}

// wrote this for Viper 6649 to allow scalar vs array
// and array s of different sizes to match
void VhdlCompositeValue::Adjust(unsigned size) {
    if (!_values) return ;
    if (_values->Size()==size) return ;

    VhdlValue *low = _range ? _range->Low() : 0 ;
    VhdlValue *high = _range ? _range->High() : 0 ;

    if (low) low = low->Copy() ;
    if (high) high = high->Copy() ;

    if (_values->Size() > size) {
        unsigned i ;
        for (i=0;i<_values->Size()-size;i++) if (high) high = high->UnaryOper(VHDL_DECR, 0) ;
        _values->Remove(0, _values->Size() - size) ;
    } else {
        unsigned i ;
        // VIPER #7529 : Donot use '_values->Size()' in condition, as '_values->Size()'
        // is increasing inside loop
        unsigned length = (size - _values->Size()) ;
        //for (i =  0 ; i < (size - _values->Size()) ; i++) {
        for (i =  0 ; i < length ; i++) {
            // No need to copy the default element value, it is used to save memory, just insert NULL:
            //_values->InsertFirst(_default_elem_val ? _default_elem_val->Copy() : 0 /*new VhdlNonconstBit(_gnd)*/) ;
            _values->InsertFirst(0) ; // Default should apply
            if (high) high = high->UnaryOper(VHDL_INCR, 0) ;
        }
    }

    if (_range && high && low) {
        unsigned dir = _range->Dir() ;
        unsigned is_unconstrainted = _range->IsUnconstrained() ;
        delete _range ;
        if (dir == VHDL_to) {
            _range = new VhdlRangeConstraint(low, dir, high, is_unconstrainted) ;
        } else {
            _range = new VhdlRangeConstraint(high, dir, low, is_unconstrainted) ;
        }
    } else {
        delete high ; delete low ; delete _range ;
    }
}

/***************************************************/
// Set attributes on netlist objects in this value
// This routine is only effective during RTL elaboration.
/***************************************************/

void VhdlValue::SetAttribute(const char * /*att_name*/, const char * /*att_value*/)
{
    return ; // no netlist object here.
}

void VhdlNonconst::SetAttribute(const char *att_name, const char *att_value)
{
    if (!att_name || !att_value) return ;
}

void VhdlNonconstBit::SetAttribute(const char *att_name, const char *att_value)
{
    if (!att_name || !att_value) return ;
}

void VhdlCompositeValue::SetAttribute(const char *att_name, const char *att_value)
{
    if (!att_name || !att_value) return ;
}

/**********************************************/
// Net assignment : Wire nets of new value val into existing value
// Absorb 'val'. As always because ne need to get to same value type
/**********************************************/

void VhdlInt::NetAssign(VhdlValue *val, VhdlIdDef * /*res_func*/, const VhdlTreeNode *from)
{
    // There should not be a constant in to-be-assigned value
    if (from) from->Error("illegal target for assignment") ; // FIX ME : should catch this in analyzer
    delete val ; // Need to delete val
}

void VhdlDouble::NetAssign(VhdlValue *val, VhdlIdDef * /*res_func*/, const VhdlTreeNode *from)
{
    // There should not be a constant in to-be-assigned value
    if (from) from->Error("illegal target for assignment") ; // FIX ME : should catch this in analyzer
    delete val ; // Need to delete val
}

void VhdlEnum::NetAssign(VhdlValue *val, VhdlIdDef * /*res_func*/, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_enum_id) ;

    // There should not be a constant in to-be-assigned value
    if (from) from->Error("illegal target for assignment") ; // FIX ME : should catch this in analyzer
    delete val ; // Need to delete val
}

void VhdlNonconst::NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from)
{
    (void) val ;  // Needed to prevent "unused" build warnings
    (void) from ; // Needed to prevent "unused" build warnings
    (void) res_func ; // Needed to prevent "unused" build warnings
}

void VhdlNonconstBit::NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from)
{
    (void) val ;  // Needed to prevent "unused" build warnings
    (void) from ; // Needed to prevent "unused" build warnings
    (void) res_func ; // Needed to prevent "unused" build warnings
}

void VhdlCompositeValue::NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

    if (!val || val==this) return ; // done
    // VERIFIC_ASSERT that they are the same size
    // VERIFIC_ASSERT(NumElements() == val->NumElements()) ;
    // VIPER issue 2299 : do not assert. Assignment with illegal range could occur dispite error.
    // TakeValueAt will return 0 if out of range..
    unsigned i ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        // Note that uninitialized access value may contain NULL element
        // and is valid/correct and should not use default element value.
        if (!elem) continue ;
        elem->NetAssign(val->TakeValueAt(i), res_func, from) ;
    }
    delete val ; // The shell of it
}

void VhdlDualPortValue::NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_values) ;

    if (!val || val==this) return ; // done
    // VERIFIC_ASSERT that they are the same size
    VERIFIC_ASSERT(NumElements() == val->NumElements()) ;

    // Here, 'this' is the original value of the RAM interface,
    // and 'expr' is the elaborated value after it went through
    // some sequential process.

    // If there was no assignment to the ram, the elab_value
    // will still be the same as initial value.
    // Otherwise, we need to wire (NetAssign) the respective
    // elaborated values into the inputs of the RAM.

    // Note that index 0 is the read_data (not affected by assignments)
    // Index 1->4 are 'initial' values used for elaboration
    // Index 5->8 are 'target' values, to which we will assign

    // Skip output. Go through inputs (index 1->4) :
    VhdlValue *init_elem, *elab_elem, *target_elem ;
    for (unsigned i=1; i<=4; i++) {
        init_elem = ValueAt(i) ;
        elab_elem = val->TakeValueAt(i) ;
        VERIFIC_ASSERT(init_elem && elab_elem) ;

        // Compare the initial value to the assigned value.
        if (init_elem->IsEqual(elab_elem)) {
            // Values are the same : there was no assignment. Skip this.
            delete elab_elem ;
            continue ;
        }

        // Here, elaboration value differs from initial value.
        // So, an assignment happened to the 'write' side of the RAM.
        // Find the corresponding write value (RAM input).
        // That should be index 4 higher :
        target_elem = ValueAt(i+4) ;
        VERIFIC_ASSERT(target_elem) ;
        // And do a net-assign on that :
        target_elem->NetAssign(elab_elem, res_func, from) ;
    }
    // Delete the remainder of elab_value :
    delete val ;
}

/**********************************************/
/**********************************************/
// Constraints
/**********************************************/
/**********************************************/

VhdlConstraint::VhdlConstraint() : VhdlNode() {}
VhdlConstraint::~VhdlConstraint() {}

VhdlRangeConstraint::VhdlRangeConstraint(VhdlValue *left, unsigned dir, VhdlValue *right, unsigned is_unconstrained)
  : VhdlConstraint(),
    _left(left),
    _right(right),
    _dir(dir),
    _is_unconstrained(is_unconstrained)
{
    // Accept range constraints only with valid bounds.
    // So we don't need to check in all routines that follow
    VERIFIC_ASSERT(_left && _right) ;
}

VhdlRangeConstraint::~VhdlRangeConstraint()
{
    delete _left ;
    delete _right ;
}

// bit weird constructor
VhdlArrayConstraint::VhdlArrayConstraint(VhdlConstraint *index_constraint, VhdlConstraint *element_constraint)
  : VhdlConstraint(),
    _index_constraint(index_constraint),
    _element_constraint(element_constraint)
{
    // Accept array constraints only with valid index and element constraint.
    // So we don't need to check in all routines that follow
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;
}

VhdlArrayConstraint::~VhdlArrayConstraint()
{
    delete _index_constraint ;
    delete _element_constraint ;
}

VhdlRecordConstraint::VhdlRecordConstraint(Array *element_constraints, Array *element_ids)
  : VhdlConstraint(),
    _element_constraints(element_constraints),
    _element_ids(element_ids)
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;
    VERIFIC_ASSERT (_element_constraints->Size() == _element_ids->Size()) ;
}

VhdlRecordConstraint::~VhdlRecordConstraint()
{
    unsigned i ;
    VhdlConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) delete elem_constraint ;
    delete _element_constraints ;
    delete _element_ids ;
}

/***************************************************************/
// Copy
/***************************************************************/

VhdlConstraint *VhdlRangeConstraint::Copy() const
{
    VERIFIC_ASSERT(_left && _right) ;
    return new VhdlRangeConstraint(_left->Copy(), _dir, _right->Copy(), _is_unconstrained) ;
}
VhdlConstraint *VhdlArrayConstraint::Copy() const
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;
    return new VhdlArrayConstraint(_index_constraint->Copy(), _element_constraint->Copy()) ;
}
VhdlConstraint *VhdlRecordConstraint::Copy() const
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    Array *element_constraints = new Array(_element_constraints->Size()) ;
    unsigned i ;
    VhdlConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) {
        element_constraints->InsertLast((elem_constraint)?elem_constraint->Copy():0) ;
    }
    Array *all_ids = new Array(*_element_ids) ;
    return new VhdlRecordConstraint(element_constraints, all_ids) ;
}

/***************************************************************/
// Constraint Access Routines
/***************************************************************/

VhdlConstraint *VhdlConstraint::ElementConstraint() const       { return 0 ; }
VhdlConstraint *VhdlArrayConstraint::ElementConstraint() const  { return _element_constraint ; }

VhdlConstraint *VhdlConstraint::IndexConstraint() const         { return 0 ; }
VhdlConstraint *VhdlArrayConstraint::IndexConstraint() const    { return _index_constraint ; }

VhdlConstraint *VhdlConstraint::ElementConstraintAt(unsigned /*pos*/) const         { return 0 ; }
VhdlConstraint *VhdlArrayConstraint::ElementConstraintAt(unsigned /*pos*/) const    { return _element_constraint ; }
VhdlConstraint *VhdlRecordConstraint::ElementConstraintAt(unsigned pos) const
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    if (_element_constraints->Size() <= pos) return 0 ;
    return (VhdlConstraint*)_element_constraints->At(pos) ;
}

VhdlConstraint *VhdlConstraint::ElementConstraintFor(VhdlIdDef * /*ele_id*/) const    { return 0 ; }
VhdlConstraint *VhdlRecordConstraint::ElementConstraintFor(VhdlIdDef *ele_id) const
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;
    if (!ele_id) return 0 ;

    unsigned i ;
    VhdlConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) {
        VhdlIdDef *field = (VhdlIdDef*)_element_ids->At(i) ;
        if (field == ele_id) return elem_constraint ;
    }
    return 0 ;
}

// Get scalar / index range bounds
VhdlValue *VhdlConstraint::Left() const                     { return 0 ; }
VhdlValue *VhdlConstraint::Right() const                    { return 0 ; }
VhdlValue *VhdlConstraint::Low() const                      { return 0 ; }
VhdlValue *VhdlConstraint::High() const                     { return 0 ; }
unsigned   VhdlConstraint::Dir() const                      { return VHDL_to ; } // for record constraints

VhdlValue *VhdlRangeConstraint::Left() const                { return _left ; }
VhdlValue *VhdlRangeConstraint::Right() const               { return _right ; }

// VIPER 3582, 3589, 3677:
// The Low() and High() routines implement the lower_bound and upper_bound definitions of a range.
// We used to follow LRM 3.1 for that (take the larger (or smaller) of the two bounds).
// Instead, language clarification VHDL Issue Number:1044 to VASG-ISAC, 1991 states that we should
// change the definition to use ascending or decending range, essentially following LRM 14.1, NOTE 1.
// So change the implementation to check the direction (to or downto) and choose low/high accordingly.
// Additional benefit of this is that we do not need to compare values to obtain the low or high bound.
//
// VhdlValue *VhdlRangeConstraint::Low() const                 { return _left->LT(_right) ? _left : _right ; }
// VhdlValue *VhdlRangeConstraint::High() const                { return _right->LT(_left) ? _left : _right ; }
VhdlValue *VhdlRangeConstraint::Low() const                 { return (_dir==VHDL_to) ? _left : _right ; }
VhdlValue *VhdlRangeConstraint::High() const                { return (_dir==VHDL_to) ? _right : _left ; }

unsigned   VhdlRangeConstraint::Dir() const                 { return _dir ; }

VhdlValue *VhdlArrayConstraint::Left() const                { return _index_constraint->Left() ; }
VhdlValue *VhdlArrayConstraint::Right() const               { return _index_constraint->Right() ; }
VhdlValue *VhdlArrayConstraint::Low() const                 { return _index_constraint->Low() ; }
VhdlValue *VhdlArrayConstraint::High() const                { return _index_constraint->High() ; }
unsigned   VhdlArrayConstraint::Dir() const                 { return _index_constraint->Dir() ; }

// Viper #4222: return type changed from unsigned -> verific_uint64
verific_uint64 VhdlConstraint::Length() const               { return 0 ; }
verific_uint64 VhdlArrayConstraint::Length() const          { return _index_constraint->Length() ; }
verific_uint64 VhdlRangeConstraint::Length() const
{
    VERIFIC_ASSERT(_left && _right) ;
    // VIPER #4519: should error out on arrayy length and aggregate length mis-match
    if (IsNullRange()) return 0 ;

    // Result will always be positive and valid for any non-null range.
    // It's up to the caller to ensure that the range is non-null, or else
    // incorrect behavior may ensue.
    return (_dir==VHDL_to) ? (verific_uint64) ((_right->Position() - _left->Position()) + 1) :
           (_dir==VHDL_downto) ? (verific_uint64) ((_left->Position() - _right->Position()) + 1) :
           0 ;
}
verific_uint64 VhdlRecordConstraint::Length() const         { return (verific_uint64)_element_constraints->Size() ; }

// Calculate the number of bits needed to encode this range or index range of array, or number of elements of record.
unsigned VhdlConstraint::NumBitsOfRange() const             { return 0 ; }
unsigned VhdlArrayConstraint::NumBitsOfRange() const        { return _index_constraint->NumBitsOfRange() ; }
unsigned VhdlRangeConstraint::NumBitsOfRange() const
{
    // Special encodings have their own number of bits to encode the range :
    if (IsBitEncoded()) {
        // Encodes in 1 bit, by user directive
        return 1 ;
    } else if (UserEncoded() || OnehotEncoded()) {
        // Encodes all values in the same size. Take the one from the left bound.
        return _left->NumBits() ;
    }

    // Here, a normal (binary encoded) range :

    // Check how many bits are needed to encode this range.
    // Check the left bound and right bound, and consider signed/unsigned.
    // Assume binary encoding (unless speced otherwise).

    // Get maximum of the absolute value of the range bounds
    verific_int64 l = Left()->Position() ;
    verific_int64 r = Right()->Position() ;

    // For negative numbers : we can store one more value
    // in the same amount of bits, but we need 1 sign bit.
    // Label value as signed if any of its bounds is negative.
    unsigned size = 0 ;
    if (l<0) { l=-(l+1) ; size = 1 ; }
    if (r<0) { r=-(r+1) ; size = 1 ; }
    verific_uint64 top = (l>r) ? (verific_uint64)l : (verific_uint64)r ;

    // Now calculate the number of bits needed
    for (; top!=0; top=top>>1) size++ ;

    // Make sure to at least create one bit (for range 0 to 0 sort of variables).
    // CHECK ME : Is this really what we want ?
    if (!size) size++ ;

    return size ;
}
unsigned VhdlRecordConstraint::NumBitsOfRange() const
{
    unsigned size = 0 ;

    // See how many elements there are in this record.
    unsigned l = _element_constraints->Size() - 1 ;

    // Now calculate the number of bits needed to identify each individually :
    for (; l!=0; l=l>>1) size++ ;

    return size ;
}

// to return the log(width) needed to represent a value for this
verific_uint64 VhdlConstraint::GetNumBitsForConstraint() const
{
    verific_uint64 top = GetSizeForConstraint() ;
    if (top) top -= 1 ; // -1 to fix the boundary. 32 needs 5 bits
    verific_uint64 size = 0 ;
    for (; top!=0; top=top>>1) size++ ;
    if (!size) size++ ;

    return size ;
}

verific_uint64 VhdlArrayConstraint::GetNumBitsForConstraint() const
{
    if (IsUnconstrained()) return 0 ;
    VERIFIC_ASSERT(_element_constraint) ;

    verific_uint64 top = Length() ;
    if (top) top -= 1 ; // -1 to fix the boundary. 32 needs 5 bits
    verific_uint64 size = 0 ;
    for (; top!=0; top=top>>1) size++ ;
    if (!size) size++ ;

    // Exponents are added for multiplication
    verific_uint64 element_size = _element_constraint->GetNumBitsForConstraint() ;
    if (element_size == 1) element_size = 0 ; // size count of 1 bits => size
    if (size == 1 && element_size) size = 0 ; // 1 element_size => element_size

    return size + element_size ;
}

// to return the width needed to represent a value for this
verific_uint64 VhdlConstraint::GetSizeForConstraint() const { return NumBitsOfRange() ; }

verific_uint64 VhdlArrayConstraint::GetSizeForConstraint() const
{
    if (IsUnconstrained()) return 0 ;
    VERIFIC_ASSERT(_element_constraint) ;
    return Length() * _element_constraint->GetSizeForConstraint() ;
}

verific_uint64 VhdlRangeConstraint::GetSizeForConstraint() const
{
    if (IsUnconstrained() || IsNullRange()) return 0 ;
    return NumBitsOfRange() ;
}

verific_uint64 VhdlRecordConstraint::GetSizeForConstraint() const
{
    verific_uint64 n_bits = 0 ;
    unsigned i ;
    VhdlConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) {
        if (elem_constraint) n_bits += elem_constraint->GetSizeForConstraint() ;
    }

    return n_bits ;
}

// Check if this range or index range contains negative values.
unsigned VhdlConstraint::IsSignedRange() const              { return 0 ; }
unsigned VhdlArrayConstraint::IsSignedRange() const         { return _index_constraint->IsSignedRange() ; }
unsigned VhdlRangeConstraint::IsSignedRange() const
{
    verific_int64 l = Left()->Position() ;
    verific_int64 r = Right()->Position() ;
    // Check if either of them is negative :
    return (l<0 || r<0) ? 1 : 0 ;
}

// Check if this has a null range : Added for Viper #7150
unsigned VhdlConstraint::HasNullRange()  const               { return 0 ; }
unsigned VhdlRangeConstraint::HasNullRange() const           { return IsNullRange() ; }
unsigned VhdlArrayConstraint::HasNullRange() const
{
    if (_index_constraint->HasNullRange()) return 1 ;
    if (_element_constraint && _element_constraint->HasNullRange()) return 1 ;
    return 0 ;
}
unsigned VhdlRecordConstraint::HasNullRange() const
{
    unsigned i ;
    VhdlConstraint *ele_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele_constraint) {
        if (!ele_constraint) continue ;
        if (ele_constraint->HasNullRange()) {
            return 1 ;
        }
    }
    return 0 ;
}

// Check if this is a null range
unsigned VhdlConstraint::IsNullRange()  const               { return 0 ; }
unsigned VhdlArrayConstraint::IsNullRange() const           { return _index_constraint->IsNullRange() ; }
unsigned VhdlRangeConstraint::IsNullRange() const
{
    VERIFIC_ASSERT(_left && _right) ;

    // Range is a NULL constraint if :
    //      left < right  with direction DOWNTO or
    //      left > right  with direction TO
    return (_dir==VHDL_to) ? (_right->LT(_left)) :
           (_dir==VHDL_downto) ? (_left->LT(_right)) :
           0 ;
}

/**********************************************/
// Test various types of constraints
/**********************************************/

unsigned   VhdlConstraint::IsRangeConstraint() const        { return 0 ; }
unsigned   VhdlRangeConstraint::IsRangeConstraint() const   { return 1 ; }

unsigned   VhdlConstraint::IsArrayConstraint() const        { return 0 ; }
unsigned   VhdlArrayConstraint::IsArrayConstraint() const   { return 1 ; }
unsigned   VhdlConstraint::IsRecordConstraint() const       { return 0 ; }
unsigned   VhdlRecordConstraint::IsRecordConstraint() const { return 1 ; }

unsigned   VhdlConstraint::IsEnumRange() const              { return 0 ; }
unsigned   VhdlRangeConstraint::IsEnumRange() const         { return (_left->GetEnumId()) ? 1 : 0 ; }
unsigned   VhdlConstraint::IsRealRange() const              { return 0 ; }
unsigned   VhdlRangeConstraint::IsRealRange() const         { return _left->IsReal() ; }

/***************************************************************/
// Check if range is encoded in a single bit (implemented with 1 net)
/***************************************************************/

unsigned VhdlConstraint::IsBitEncoded() const               { return 0 ; }
unsigned VhdlRangeConstraint::IsBitEncoded() const          { return (_left->IsBit()) ? 1 : 0 ; }

/***************************************************************/
// Check if range is encoded in a multiple bits
/***************************************************************/

unsigned VhdlConstraint::UserEncoded() const                { return 0 ; }
unsigned VhdlRangeConstraint::UserEncoded() const           { return _left->UserEncoded() ; }

/***************************************************************/
// Check if range is one-hot encoded
/***************************************************************/

unsigned VhdlConstraint::OnehotEncoded() const              { return 0 ; }
unsigned VhdlRangeConstraint::OnehotEncoded() const         { return _left->OnehotEncoded() ; }

/***************************************************************/
// Label range as unconstrained (for index ranges of unconstrained subtypes)
/***************************************************************/

void     VhdlConstraint::SetUnconstrained()                 { return ; } // VERIFIC_ASSERT ?
void     VhdlArrayConstraint::SetUnconstrained()            { _index_constraint->SetUnconstrained() ; }
void     VhdlRangeConstraint::SetUnconstrained()            { _is_unconstrained = 1 ; }

// VIPER #6938 : Access constraint cannot be unconstrained
// type string_ptr is access string ; -- string_ptr is not unconstrained type
unsigned VhdlConstraint::IsUnconstrained() const        { return 0 ; }
unsigned VhdlRangeConstraint::IsUnconstrained() const   { return _is_unconstrained ; }
unsigned VhdlArrayConstraint::IsUnconstrained() const
{
    //if (IsAccessConstraint()) return 0 ;
    if (_index_constraint->IsUnconstrained()) return 1 ;
    //return (_element_constraint ? _element_constraint->IsUnconstrained() : 0 ) ;
    if (_element_constraint && _element_constraint->IsUnconstrained()) {
        return 1 ;
    }
    return 0 ;
}
// VIPER #6938 : Overwrite for record constraint as element can be
// unconstrained
unsigned VhdlRecordConstraint::IsUnconstrained() const
{
    //if (IsAccessConstraint()) return 0 ;
    unsigned i ;
    VhdlConstraint *ele_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele_constraint) {
        if (!ele_constraint) continue ;
        if (ele_constraint->IsUnconstrained()) {
            return 1 ;
        }
    }
    return 0 ;
}
/***************************************************************/
// Test if a value is contained by (is in) a range
/***************************************************************/

unsigned VhdlConstraint::Contains(VhdlValue * /*value*/) const { return 0 ; }
unsigned VhdlArrayConstraint::Contains(VhdlValue *value) const { return _index_constraint->Contains(value) ; }
unsigned VhdlRangeConstraint::Contains(VhdlValue *value) const
{
    VERIFIC_ASSERT(_left && _right) ;

    if (!value) return 0 ;

    // VIPER #3408: Physical values are always contained in constraints. This is how
    // simulators seem to handle some physical types like time. So we do the same:
    // Look into VIPER #2651, fix of this issue is just the opposite of that VIPER.

    // Viper 5242 fix the issue of calculating the value of a physical unit for user
    // defined time unit. The following early return does not seem to be needed any more.
    // if (value->IsPhysicalValue()) return 1 ;

    // Value is OUT of range if its position is below the lowest, or above the highest.
    return (_dir==VHDL_to) ? !(_right->LT(value) || value->LT(_left)) :
           (_dir==VHDL_downto) ? !(_left->LT(value) || value->LT(_right)) :
           0 ;
}

unsigned VhdlRecordConstraint::Contains(VhdlValue *value) const
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    if (!value) return 0 ;
    // For record constraints : Check if this value (representing a position)
    // is indeed an element position of the record
    // This routine needed in formal_assoc evaluation.
    // FIX ME : can probably do without..
    verific_int64 pos = value->Position() ;
    return (pos >= 0 && pos < (verific_int64)_element_constraints->Size()) ;
}

/***************************************************************/
// PosOf : position number of a (constant) discret value in the range,
// counting from the left.
// USE ONLY ON value THAT IS TESTED TO BE IN this RANGE.
// Used for indexing in array values.
/***************************************************************/

unsigned VhdlConstraint::PosOf(VhdlValue * /*value*/) const { return 0 ; }
unsigned VhdlArrayConstraint::PosOf(VhdlValue *value) const { return _index_constraint->PosOf(value) ; }
unsigned VhdlRangeConstraint::PosOf(VhdlValue *value) const
{
    VERIFIC_ASSERT(_left && _right) ;

    if (!value) return 0 ;
    return (_dir==VHDL_to) ? (unsigned)(value->Position() - Left()->Position()) :
           (_dir==VHDL_downto) ? (unsigned)(Left()->Position() - value->Position()) :
           0 ;
}
unsigned VhdlRecordConstraint::PosOf(VhdlValue *value) const
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;
    if (!value) return 0 ;
    // return just the position of the value :
    // This happens for 'position' determination in record target selected names.
    // FIX ME : Should do that differently..
    return (unsigned)value->Position() ;
}

/***************************************************************/
/***************************************************************/
void VhdlConstraint::ReverseDirection() { return ; }
void VhdlArrayConstraint::ReverseDirection()
{
    VERIFIC_ASSERT(_index_constraint) ;
    _index_constraint->ReverseDirection() ;
}
void VhdlRangeConstraint::ReverseDirection()
{
    VERIFIC_ASSERT(_left && _right) ;
    VhdlValue *temp = _left ;
    _left = _right ;
    _right = temp ;
    _dir = (_dir==VHDL_to) ? VHDL_downto : VHDL_to ;
}

/***************************************************************/
// Constraint a unconstrained constraint by a value
/***************************************************************/

void VhdlConstraint::ConstraintBy(VhdlValue * /*value*/) { return ; }
void VhdlArrayConstraint::ConstraintBy(VhdlValue *value)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    // Constraint the index constraint by this array value

    // For access value, use the inner value to get the constraint
    value = value ? value->GetInnerValue() : 0 ;
    if (!value) return ;

    // Use num of elements in value, left bound and direction of constraint
    // to create a new constrained value
    _index_constraint->ConstraintBy(value) ;

    // Do this for elements too (multi-dim arrays)
    // use first element for that
    _element_constraint->ConstraintBy(value->ValueAt(0)) ;
}

void VhdlRecordConstraint::ConstraintBy(VhdlValue *value)
{
    // Viper 7505: Vhdl2008 records can be constrained.
    if (!IsVhdl2008()) return ;
    unsigned i ;
    VhdlConstraint *ele ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele) {
        if (!ele || !ele->IsUnconstrained()) continue ;
        VhdlValue *ele_val = value ? value->ValueAt(i): 0 ;
        ele->ConstraintBy(ele_val) ;
    }
}

void VhdlRangeConstraint::ConstraintBy(VhdlValue *value)
{
    VERIFIC_ASSERT(_left && _right) ;

    // For access value, use the inner value to get the constraint
    value = value ? value->GetInnerValue() : 0 ;
    if (!value || !_is_unconstrained) return ;

    VhdlConstraint *value_range = value->GetRange() ;
    if (value_range && !value_range->IsUnconstrained()) { // Viper #7288
        ConstraintBy(value_range) ;
        return ;
    }

    // Use the constraint's left bound, direction and size of value
    // to come up with a (default) index constraint.
    // This is not quite according to the LRM, but is a good
    // default (most expressions adjust to left bound/direction of the base type.
    unsigned elems = value->NumElements() ;
    if (elems==0) {
        // Create a NULL range : swap the direction to create NULL range.
        // CHECK ME : Does LRM provide info on how to create null range within the unconstrained range ?
        //_dir = (_dir==VHDL_to) ? VHDL_downto : VHDL_to ;

        // if _left == _right, above does not create a null range.
        // ensure that now the range is really a null range
        // VIPER #7286 : IEEE Std 1076-2008 LRM section 9.3.2 says
        // For a null array value represented by either a string or bit string
        // literal, the direction and leftmost bound of the index range of the
        // array value are determined as follows: the direction and nominal
        // leftmost bound of the index range of the array value are determined as
        // in the non-null case. If there is a value to the left of the nominal
        // leftmost bound (given by the 'LEFTOF attribute), then the leftmost
        // bound is the nominal leftmost bound, and the rightmost bound is the
        // value to the left of the nominal leftmost bound. Otherwise, the
        // leftmost bound is the value to the right of the nominal leftmost
        // bound, and the rightmost bound is the nominal leftmost bound.
        if (IsEnumRange() && !IsNullRange()) {
            _dir = VHDL_to ;
            verific_int64 left_pos = PosOf(_left) ;
            if (left_pos == 0) {
                // _left will be right of previous _left and right will be previous _left
                delete _right ;
                _right = _left ;
                _left = _right->Copy() ;
                (void) _left->UnaryOper(VHDL_INCR, 0) ;
            } else if (left_pos > 0) {
                // _left will remain same, _right will be left of _left
                delete _right ;
                _right = _left->Copy() ;
                (void) _right->UnaryOper(VHDL_DECR, 0) ;
            } else {
                //VERIFIC_ASSERT(0) ;
                _dir = (_dir==VHDL_to) ? VHDL_downto : VHDL_to ;
            }
        } else if (!IsNullRange()) {
            delete _right ;
            _right = _left->Copy() ;
            (void) _right->UnaryOper((_dir==VHDL_to) ? VHDL_DECR : VHDL_INCR, 0) ;
        }
    } else {
        // Replace 'right' with an updated version of 'left', adjusted for position
        // Check if this is a null range and yet marked unconstrained! Correct it.
        // VIPER #7286 : Now we are not changing the _dir field while making the
        // constraint null. So no need to change _dir here also to get back correct definition
        //if (IsNullRange()) _dir = (_dir==VHDL_to) ? VHDL_downto : VHDL_to ;

        delete _right ;
        _right = _left->Copy() ;
        // FIX ME : There should be a better way to get the
        // N-1th position value from a left side of the range.
        unsigned i ;
        for (i=1;i<elems;i++) (void) _right->UnaryOper((_dir==VHDL_to) ? VHDL_INCR : VHDL_DECR,0) ;
    }
    // now, it's constrained
    _is_unconstrained = 0 ;
}

// Constraint by a (name) constraint

// For this composite value, use the index constraint indicated.
// Make a copy of the constraint.
void VhdlConstraint::ConstraintBy(VhdlConstraint * /*constraint*/) { }

void VhdlRangeConstraint::ConstraintBy(VhdlConstraint *constraint)
{
    VERIFIC_ASSERT(_left && _right) ;

    // Constrainting a single range (probably a index constraint) by another range
    if (!IsUnconstrained()) return ;
    // Cannot constraint with an unconstrained range
    if (!constraint) return ;
    if (constraint->IsUnconstrained()) return ;

    // Replace left, right and direction by the new constraint ranges
    _dir = constraint->Dir() ;

    delete _left ;
    _left = constraint->Left()->Copy() ;
    delete _right ;
    _right = constraint->Right()->Copy() ;

    // And make it constrained (as we already know it is) :
    _is_unconstrained = 0 ;
}

void VhdlArrayConstraint::ConstraintBy(VhdlConstraint *constraint)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    // Constraint a unconstrained constraint by a (name) constraint

    // Don't constraint an already constrained target :
    if (!_index_constraint->IsUnconstrained() && !_element_constraint->IsUnconstrained()) return ;

    // Cannot constraint with an unconstrained index range :
    if (!constraint || constraint->IsUnconstrained()) return ;

    // This routine should only operate to fix LRM 3.2.1.1 requirements
    // of passing actual constraints to unconstrained formals
    VhdlConstraint *index = constraint->IndexConstraint() ;
    if (!index) return ;

    // Now constraint the index range by this range :
    _index_constraint->ConstraintBy(index) ;

    // Do from elements too ? CHECK ME : Does LRM require this at all ? Yes, for multi-dim arrays.
    _element_constraint->ConstraintBy(constraint->ElementConstraint()) ;
}

void VhdlRecordConstraint::ConstraintBy(VhdlConstraint *constraint)
{
    // Viper 7505: Vhdl2008 records can be constrained.
    if (!IsVhdl2008()) return ;
    unsigned i ;
    VhdlConstraint *ele ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele) {
        if (!ele || !ele->IsUnconstrained()) continue ;
        VhdlConstraint *ele_constr = constraint ? constraint->ElementConstraintAt(i) : 0 ;
        ele->ConstraintBy(ele_constr) ;
    }
}

// **********************************************
// MergeConstraints(VhdlConstraint *other_constraint, unsigned oper_type)
// Apply the operators on the two ranges
// **********************************************
VhdlConstraint *VhdlConstraint::MergeConstraints(VhdlConstraint *other_constraint, unsigned /*oper_type*/)
{
    // Viper #6473: apply the operator oper_type on the two constraints
    // e.g. (0 to 5) plus (3 downto -2) should generate (-2 to 8)

    // works only with range constraint
    delete other_constraint ;
    delete this ;

    return 0 ;
}

VhdlConstraint *VhdlRangeConstraint::MergeConstraints(VhdlConstraint *right_constraint, unsigned oper_type)
{
    // Viper #6473: apply the operator oper_type on the two constraints
    // e.g. (0 to 5) plus (3 downto -2) should generate (-2 to 8)
    if (right_constraint && !right_constraint->IsRangeConstraint()) {
        delete right_constraint ;
        delete this ;

        return 0 ;
    }

    if (IsUnconstrained() || (right_constraint && right_constraint->IsUnconstrained())) {
        delete right_constraint ;
        delete this ;
        return 0 ;
    }

    VhdlValue *this_low = Low() ;
    VERIFIC_ASSERT(this_low && this_low->IsConstant()) ;

    VhdlValue *this_high = High() ;
    VERIFIC_ASSERT(this_high && this_high->IsConstant()) ;

    VhdlValue *right_low = right_constraint ? right_constraint->Low() : 0 ;
    VERIFIC_ASSERT(!right_low || right_low->IsConstant()) ;

    VhdlValue *right_high = right_constraint ? right_constraint->High() : 0 ;
    VERIFIC_ASSERT(!right_high || right_high->IsConstant()) ;

    VhdlValue *new_low = 0, *new_high = 0 ;
    switch(oper_type) {
    case VHDL_PLUS:
    {
        new_low = right_low ? this_low->Copy()->BinaryOper(right_low->Copy(), oper_type, 0) : this_low->Copy()->UnaryOper(oper_type, 0) ;
        new_high = right_high ? this_high->Copy()->BinaryOper(right_high->Copy(), oper_type, 0) : this_high->Copy()->UnaryOper(oper_type, 0) ;

        break ;
    }
    case VHDL_MINUS:
    {
        new_low = right_high ? this_low->Copy()->BinaryOper(right_high->Copy(), oper_type, 0) : this_high->Copy()->UnaryOper(oper_type, 0) ;
        new_high = right_low ? this_high->Copy()->BinaryOper(right_low->Copy(), oper_type, 0) : this_low->Copy()->UnaryOper(oper_type, 0) ;

        break ;
    }
    default :
        break ;
    }

    delete right_constraint ;
    delete this ;

    if (!new_high || !new_low) {
        delete new_high ;
        delete new_low ;
        return 0 ;
    }

    return new VhdlRangeConstraint(new_low, VHDL_to, new_high) ;
}

/**********************************************/
// ConstraintByChoice : adjust the bounds of this constraint to include 'index'. Needed for constraint building from aggregate choices.
/**********************************************/

void VhdlConstraint::ConstraintByChoice(const VhdlValue * /*index*/)
{
    // ASSERT ?
}

void VhdlRangeConstraint::ConstraintByChoice(const VhdlValue *index)
{
    if (!index) return ; // nothing to do.

    // If this constraint is still 'unconstrained',
    // then blindly insert 'index' in left and right position,
    // and then mark it as constrained:
    if (_is_unconstrained) {
        // Happens for the first call. Blindly replace left and right with 'index' :
        delete _left ;
        delete _right ;
        _left = index->Copy() ;
        _right = index->Copy() ;
        _is_unconstrained = 0 ;
        return ;
    }

    // Otherwise, adjust the bounds only if 'index' is outside them.
    // This happens for subsequent calls to 'ConstraintByChoice' for different values.

    // Check the 'position' of index (the value of it) :
    verific_int64 pos = index->Position() ;

    // Check if 'index' is in the current range. If not, set it as the new bound :

    // check if 'pos' is left of the current left bound :
    if ((_dir == VHDL_to && pos < _left->Position()) ||
        (_dir == VHDL_downto && pos > _left->Position())) {
        delete _left ;
        _left = index->Copy() ;
    }
    if ((_dir == VHDL_to && pos > _right->Position()) ||
        (_dir == VHDL_downto && pos < _right->Position())) {
        delete _right ;
        _right = index->Copy() ;
    }
}

void VhdlArrayConstraint::ConstraintByChoice(const VhdlValue *index)
{
    if (!_index_constraint) return ;
    // Adjust the index range :
    _index_constraint->ConstraintByChoice(index) ;
}

/**********************************************/
// Image : return allocated string that represents the constraint
/**********************************************/

char *VhdlRangeConstraint::Image() const
{
    VERIFIC_ASSERT(_left && _right) ;

    char *l = Left()->Image() ;
    char *r = Right()->Image() ;
    char *result = Strings::save(l, (Dir()==VHDL_to) ? " to " : " downto ", r) ;
    Strings::free(l) ;
    Strings::free(r) ;
    return result ;
}

char *VhdlArrayConstraint::Image() const
{
    // Print only the index constraint..?
    return _index_constraint->Image() ;
}

char *VhdlRecordConstraint::Image() const
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    // Print leftid to rightid ?
    if (!_element_ids->Size()) return Strings::save("") ; // can this happen ?
    VhdlIdDef *left_id = (VhdlIdDef*)_element_ids->At(0) ;
    VhdlIdDef *right_id = (VhdlIdDef*)_element_ids->At(_element_ids->Size()-1) ;
    if (!left_id || !right_id) return Strings::save("") ; // can this happen ?
    char *result = Strings::save(left_id->Name(), " to ", right_id->Name()) ;
    return result ;
}

/**********************************************/
// Create a variable (nonconst) value from a constraint
/**********************************************/

VhdlValue *VhdlRangeConstraint::CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *id, Instance *inst)
{
    VERIFIC_ASSERT(_left && _right) ;

    (void) name ;              // Needed to prevent "unused" build warnings
    (void) prepend_name_path ; // Needed to prevent "unused" build warnings
    (void) id ;                // Needed to prevent "unused" build warnings
    (void) inst ;              // Needed to prevent "unused" build warnings
    return 0 ;
}

VhdlValue *VhdlArrayConstraint::CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *id, Instance *inst)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    (void) name ;              // Needed to prevent "unused" build warnings
    (void) prepend_name_path ; // Needed to prevent "unused" build warnings
    (void) id ;                // Needed to prevent "unused" build warnings
    (void) inst ;              // Needed to prevent "unused" build warnings
    return 0 ;
}

VhdlValue *VhdlRecordConstraint::CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *id, Instance *inst)
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    (void) name ;              // Needed to prevent "unused" build warnings
    (void) prepend_name_path ; // Needed to prevent "unused" build warnings
    (void) id ;                // Needed to prevent "unused" build warnings
    (void) inst ;              // Needed to prevent "unused" build warnings
    return 0 ;
}

/**********************************************/
// Create a initial value for this constraint
// LRM rules 4.3.1.3 : scalars get type'left
// composites accumulate the elements.
/**********************************************/

VhdlValue *VhdlRangeConstraint::CreateInitialValue(const VhdlTreeNode * /*from*/)
{
    VERIFIC_ASSERT(_left && _right) ;

    if (IsAccessConstraint()) return new VhdlAccessValue(0, Copy()) ;

    // Return the 'left value in the range
    return Left()->Copy() ;
}

VhdlValue *VhdlArrayConstraint::CreateInitialValue(const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    if (IsAccessConstraint()) return new VhdlAccessValue(0, Copy()) ;

    // Return composite of elements

    if (_index_constraint->IsUnconstrained()) return 0 ; // Cannot do this for unconstrained values

    // Catch null ranges before the loop runs into space. Create empty array.
    if (IsNullRange()) return new VhdlCompositeValue(new Array(), 0, 0, _index_constraint->Copy()) ;

    unsigned size = (unsigned) Length() ; // The number of elements required
    unsigned max_array_size = vhdl_file::GetMaxArraySize() ;
    if (max_array_size > 31) max_array_size = 31 ; // Viper #5866
    // Viper #8066: use log2(constraint_size) for comparison
    if (GetNumBitsForConstraint() > max_array_size) { // Viper #4950 - runtime control
        if (from) from->Error("array size is larger than 2**%d", max_array_size) ;
        return 0 ; // don't even try. Would explode on memory usage.
    }

    VhdlValue *elem_val = _element_constraint->CreateInitialValue(from) ; // The element value

    // VIPER 4526 note that 'elem_val' can be 0, so do null-pointer checks.

    // Take advantage of the fact that ALL elements have the same value.
    // So set 'elem_val' to the element initial value, and fill the array with 0's.
    // This save massive amounts of element values (see VIPER 3905)

    // Create an array of these element values
    Array *values = new Array(size) ;
    for (unsigned i = 0 ; i<size ; i++) {
        // values->InsertLast(elem_val ? elem_val->Copy() : 0) ;
        // values->InsertLast(0) ; // initial value _default_elem_val will apply
        // Exception to above rule is access_values, as they can potentially have different values with different constraints inside
        values->InsertLast((elem_val && elem_val->IsAccessValue()) ? elem_val->Copy() : 0) ;
    }
    //delete elem_val ;
    if (elem_val && elem_val->IsAccessValue()) {
        delete elem_val ;
        elem_val = 0 ;
    }

    // And return the composite value for it
    return new VhdlCompositeValue(values, elem_val/*default element value*/, 0, _index_constraint->Copy()) ;
}

VhdlValue *VhdlRecordConstraint::CreateInitialValue(const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    if (IsAccessConstraint()) return new VhdlAccessValue(0, Copy()) ;

    // Return composite of elements
    Array *values = new Array(_element_constraints->Size()) ;
    unsigned i ;
    VhdlConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) {
        // can be 0 for access type (record elements) constraints. Issue 1526.
        // For access type initial value will be "null" i.e. AccessValue with
        // core value as Null
        VhdlValue *val = (elem_constraint) ? elem_constraint->CreateInitialValue(from) : 0 ;
        VhdlIdDef *id = (i < _element_ids->Size()) ? (VhdlIdDef*)_element_ids->At(i) : 0 ;
        VhdlIdDef *type = id ? id->Type() : 0 ;
        // VIPER : 3771/3764 - access_value to carry the constraint information
        // Viper #4533 - If the value is already an access value, don't create another wrapper
        if (type && type->IsAccessType() && (!val || !val->IsAccessValue())) val = new VhdlAccessValue(val, elem_constraint ? elem_constraint->Copy() : 0) ;

        values->InsertLast(val) ;
    }
    return new VhdlCompositeValue(values) ;
}

/**********************************************/
// Same as CreateInitialValue(), but use a fully-filled array value (do not use '_default_elem_val').
// This is presently used only for partial-formal checking routines.
// Essentially this is the old way of CreateInitialValue().
/**********************************************/

VhdlValue *VhdlArrayConstraint::CreateFullInitialValue(const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    // Return composite of elements
    if (IsUnconstrained()) return 0 ; // Cannot do this for unconstrained values

    // Catch null ranges before the loop runs into space. Create empty array.
    if (IsNullRange()) return new VhdlCompositeValue(new Array()) ;

    unsigned size = (unsigned) Length() ; // The number of elements required
    unsigned max_array_size = vhdl_file::GetMaxArraySize() ;
    if (max_array_size > 31) max_array_size = 31 ; // Viper #5866
    // Viper #8066: use log2(constraint_size) for comparison
    if (GetNumBitsForConstraint() > max_array_size) { // Viper #4950 - runtime control
        if (from) from->Error("array size is larger than 2**%d", max_array_size) ;
        return 0 ; // don't even try. Would explode on memory usage.
    }

    VhdlValue *elem_val = _element_constraint->CreateInitialValue(from) ; // The element value

    // Create an array of these element values
    Array *values = new Array(size) ;
    for (unsigned i = 0 ; i<size ; i++) {
        values->InsertLast(elem_val ? elem_val->Copy() : 0) ;
    }
    delete elem_val ;

    // And return the composite value for it
    return new VhdlCompositeValue(values, 0/*default element value*/) ;
}

VhdlValue *VhdlRecordConstraint::CreateFullInitialValue(const VhdlTreeNode *from)
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    if (IsAccessConstraint()) return new VhdlAccessValue(0, Copy()) ;

    // Return composite of elements
    Array *values = new Array(_element_constraints->Size()) ;
    unsigned i ;
    VhdlConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) {
        // can be 0 for access type (record elements) constraints. Issue 1526.
        // For access type initial value will be "null" i.e. AccessValue with
        // core value as Null
        VhdlValue *val = (elem_constraint) ? elem_constraint->CreateFullInitialValue(from) : 0 ;
        VhdlIdDef *id = (i < _element_ids->Size()) ? (VhdlIdDef*)_element_ids->At(i) : 0 ;
        VhdlIdDef *type = id ? id->Type() : 0 ;
        // VIPER : 3771/3764 - access_value to carry the constraint information
        // Viper #4533 - If the value is already an access value, don't create another wrapper
        if (type && type->IsAccessType() && (!val || !val->IsAccessValue())) val = new VhdlAccessValue(val, elem_constraint ? elem_constraint->Copy() : 0) ;

        values->InsertLast(val) ;
    }
    return new VhdlCompositeValue(values) ;
}
/**********************************************/
// Create a (nonconst) X value for this constraint
// This is presently only needed for RAM inference
/**********************************************/

VhdlValue *VhdlRangeConstraint::CreateXValue()
{
    VERIFIC_ASSERT(_left && _right) ;

    // For any scalar, create 'x' nets (as many as are needed for the constraint).

    // Calculate how many bits be need to encode this range :
    unsigned size = NumBitsOfRange() ;
    // Check if this range is signed (contains negative values) :
    unsigned is_signed = IsSignedRange() ;

    // Create the array of nets :
    Array *nets = new Array(size) ;
    for (unsigned i=0; i<size; i++) nets->InsertLast(X()) ;
    // Create the (scalar) value for it.
    return new VhdlNonconst(nets, is_signed) ;
}

VhdlValue *VhdlArrayConstraint::CreateXValue()
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    // Return composite of elements
    if (IsUnconstrained()) return 0 ; // Cannot do this for unconstrained values

    // Catch null ranges before the loop runs into space. Create empty array.
    if (IsNullRange()) return new VhdlCompositeValue(new Array()) ;

    unsigned size = (unsigned) Length() ; // The number of elements required
    VhdlValue *elem_val = _element_constraint->CreateXValue() ; // The element value

    // Take advantage of the fact that ALL elements have the same value.
    // So set 'elem_val' to the element initial value, and fill the array with 0's.
    // This save massive amounts of element values (see VIPER 3905)

    // Create an array of these element values
    Array *values = new Array(size) ;
    for (unsigned i = 0 ; i<size ; i++) {
        //values->InsertLast(elem_val->Copy()) ;
        values->InsertLast(0) ;
    }
    //delete elem_val ;

    // And return the composite value for it
    return new VhdlCompositeValue(values,elem_val) ;
}

VhdlValue *VhdlRecordConstraint::CreateXValue()
{
    VERIFIC_ASSERT(_element_constraints && _element_ids) ;

    // Return composite of elements
    Array *values = new Array(_element_constraints->Size()) ;
    unsigned i ;
    VhdlConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) {
        // can be 0 for access type (record elements) constraints. Issue 1526.
        values->InsertLast((elem_constraint) ? elem_constraint->CreateXValue() : 0) ;
    }
    return new VhdlCompositeValue(values) ;
}

// Initial value preservation
/* virtual */ void
VhdlValue::SetInitialValue(VhdlValue * /*init_val*/)
{
    // Virtual catch
    // Do nothing, produce an warning message?
}

/* virtual */ void
VhdlNonconst::SetInitialValue(VhdlValue *init_val)
{
    if (!init_val || !_nets) return ;
}
/* virtual */ void
VhdlNonconstBit::SetInitialValue(VhdlValue *init_val)
{
    VERIFIC_ASSERT(_net) ;

    if (!init_val) return ;
}

/* virtual */ void
VhdlCompositeValue::SetInitialValue(VhdlValue *init_val)
{
    VERIFIC_ASSERT(_values) ;

    if (!init_val) return ;
}

// VIPER 2636 : Generic routine to check if two constraints are compatible by value.
// We do not need to check the type (and dimensions etc), so we can assume that the constraints are of same 'type'.

unsigned VhdlRangeConstraint::CheckAgainst(VhdlConstraint *other, VhdlTreeNode * from, const VhdlDataFlow *df)
{
    if (!other || !other->IsRangeConstraint()) return 1 ;
    if (other->IsNullRange() || IsNullRange()) return 1 ;

    // VIPER 3143.
#if 1 // RD: should no longer be needed. Constant values are now tested upon assignment.
      // SD: Re-Enabled for viper 4163 and 4168
    VhdlValue *other_high = other->High() ;
    if ((other_high && other_high->LT(Low())) || (High() && High()->LT(other->Low()))) {
        // VIPER #7655 : Produce warning for non-constant branch
        if (df && df->IsNonconstBranch()) {
            if (from) from->Warning("expression has incompatible type") ;
        } else {
            if (from) from->Error("expression has incompatible type") ;
        }
        return 0 ;
    }
#endif

    // fix for VIPER 3143 precipitates compiler warning because pointer 'from' is never used
    if (from) (void) from ;

    // Do not check index range bounds, different range bounds are allowed if
    // there is overlap in range. We can do overlapping check later

    return 1 ;
}
unsigned VhdlArrayConstraint::CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df)
{
    if (!other) return 1 ;
    // Don't check for unconstrained index constraint
    if (IsUnconstrained() || other->IsUnconstrained()) return 1 ;

    // Issue error for length mismatch
    // VIPER #2858 : Consider null range as length 0
    unsigned this_length = IsNullRange() ? 0 : (unsigned) Length() ;
    unsigned other_length = other->IsNullRange() ? 0 : (unsigned) other->Length() ;
    if (this_length != other_length) {
        {
            if (from) {
                // Viper 4449 : Add more details to error message
                VhdlName *formal = from->FormalPart() ;
                const char *formal_name = formal ? formal->Name() : 0 ;
                if (!formal_name) {
                    VhdlName *prefix = formal ? formal->GetPrefix() : 0 ;
                    formal_name = prefix ? prefix->Name() : 0 ;
                }
                if (formal && formal_name) {
                    // VIPER #7655 : Produce warning for non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        from->Warning("expression has %d elements ; formal %s expects %d", other_length, formal_name, this_length) ;
                    } else {
                        from->Error("expression has %d elements ; formal %s expects %d", other_length, formal_name, this_length) ;
                    }
                } else {
                    // VIPER #7655 : Produce warning for non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        from->Warning("expression has %d elements ; expected %d", other_length, this_length) ;
                    } else {
                        from->Error("expression has %d elements ; expected %d", other_length, this_length) ;
                    }
                }
            }
        }
        return 0 ;
    }
    // Check elements too.
    return _element_constraint ? _element_constraint->CheckAgainst(other->ElementConstraint(), from, df) : 0 ;
}
unsigned VhdlRecordConstraint::CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df)
{
    if (!other) return 1 ;
    // Don't check for unconstrained index constraint
    if (IsUnconstrained() || other->IsUnconstrained()) return 1 ;

    // Issue error for length mismatch
    if (Length() != other->Length()) {
        {
            if (from) {
                VhdlName *formal = from->FormalPart() ;
                // Viper 4449 : Add more details to error message
                const char *formal_name = formal ? formal->Name() : 0 ;
                if (!formal_name) {
                    VhdlName *prefix = formal ? formal->GetPrefix() : 0 ;
                    formal_name = prefix ? prefix->Name() : 0 ;
                }
                if (formal && formal_name) {
                    // VIPER #7655 : Produce warning for non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        from->Warning("expression has %d elements ; formal %s expects %d", other->Length(), formal_name, Length()) ;
                    } else {
                        from->Error("expression has %d elements ; formal %s expects %d", other->Length(), formal_name, Length()) ;
                    }
                } else {
                    // VIPER #7655 : Produce warning for non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        from->Warning("expression has %d elements ; expected %d", other->Length(), Length()) ;
                    } else {
                        from->Error("expression has %d elements ; expected %d", other->Length(), Length()) ;
                    }
                }
            }
        }
        return 0 ;
    }
    // Check constraint of elements too
    unsigned i ;
    VhdlConstraint *ele ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele) {
        VhdlConstraint *other_ele = other->ElementConstraintAt(i) ;
        if (ele && !ele->CheckAgainst(other_ele, from, df)) return 0 ;
    }
    return 1 ; // Matched
}
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #3368 : The following routines are used to set proper range to unconstrained
// constraint from the argument specified size :
void VhdlConstraint::ConstraintBy(unsigned /*actual_size*/, VhdlTreeNode *from)
{
    const VhdlIdDef *id = (from) ? from->TreeNodeId() : 0 ;
    VhdlIdDef *id_type = id ? id->Type() : 0 ;
    if (from && id && id_type) {
        from->Error("the type %s of vhdl port %s is invalid for verilog connection", id_type->Name(), id->Name()) ;
    } else if (from) {
        from->Error("the type of vhdl port is invalid for verilog connection") ;
    }
}
void VhdlArrayConstraint::ConstraintBy(unsigned actual_size, VhdlTreeNode *from)
{
    if (!actual_size) return ;
    if (_index_constraint) _index_constraint->ConstraintBy(actual_size, from) ;

    // Do not process for multi-dim arrays
    if (_element_constraint && _element_constraint->IsArrayConstraint()) {
        const VhdlIdDef *id = (from) ? from->TreeNodeId() : 0 ;
        VhdlIdDef *id_type = id ? id->Type() : 0 ;
        if (from && id && id_type) {
            from->Error("the type %s of vhdl port %s is invalid for verilog connection", id_type->Name(), id->Name()) ;
        } else if (from) {
            from->Error("the type of vhdl port is invalid for verilog connection") ;
        }
    }
}
void VhdlRangeConstraint::ConstraintBy(unsigned actual_size, VhdlTreeNode * /*from*/)
{
    if (!_is_unconstrained) return ;

    // Expression is packed :
    if (actual_size == 0) {
        // Create a NULL range : swap the direction to create NULL range.
        _dir = (_dir==VHDL_to) ? VHDL_downto : VHDL_to ;
    } else {
        // Replace 'right' with an updated version of 'left', adjusted for position
        delete _right ;
        _right = _left->Copy() ;
        unsigned i ;
        for (i=1;i<actual_size;i++) (void) _right->UnaryOper((_dir==VHDL_to) ? VHDL_INCR : VHDL_DECR,0) ;
    }
    // now, it's constrained
    _is_unconstrained = 0 ;
}
#endif //VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #7163 : Always constrained when value is access value
void VhdlConstraint::ConstraintByAccessValue(VhdlValue * /*value*/)
{
    return ;
#if 0 // Viper #7288
    if (!value || !value->IsAccessValue()) return ; // consider only access value
    SetUnconstrained() ;
    ConstraintBy(value->Constraint()) ;
#endif
}
void VhdlRecordConstraint::ConstraintByAccessValue(VhdlValue *value)
{
    unsigned i ;
    VhdlConstraint *ele ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele) {
        VhdlValue *ele_val = value ? value->ValueAt(i): 0 ;
        if (ele) ele->ConstraintByAccessValue(ele_val) ;
    }
}

unsigned VhdlValue::HasAccessValue() const { return 0 ; }
unsigned VhdlCompositeValue::HasAccessValue() const
{
    unsigned i ;
    VhdlValue *ele ;
    FOREACH_ARRAY_ITEM(_values, i, ele) {
        if (ele && ele->HasAccessValue()) return 1 ;
    }
    return 0 ;
}
unsigned VhdlAccessValue::HasAccessValue() const
{
    if (_value) return 1 ;
    return 0 ;
}

/* -------------------------------------------------------------- */
// VhdlAccessConstraint Routines
/* -------------------------------------------------------------- */

VhdlAccessConstraint::VhdlAccessConstraint(VhdlIdDef *designated_type)
    : VhdlConstraint(),
      _designated_type(designated_type),
      _designated_constraint(0),
      _is_recursion(0)
{
    VERIFIC_ASSERT(_designated_type) ;
    // _designated_constraint = GetDesignatedConstraint() ;
}

VhdlAccessConstraint::VhdlAccessConstraint(VhdlConstraint *designated_constraint)
    : VhdlConstraint(),
      _designated_type(0),
      _designated_constraint(designated_constraint),
      _is_recursion(0)
{
    VERIFIC_ASSERT(_designated_constraint) ;
}

VhdlAccessConstraint::~VhdlAccessConstraint()
{
    delete _designated_constraint ;
}

VhdlConstraint *VhdlAccessConstraint::Copy() const
{
    if (_designated_type) return new VhdlAccessConstraint(_designated_type) ;

    VERIFIC_ASSERT(_designated_constraint) ;
    return new VhdlAccessConstraint(_designated_constraint->Copy()) ;
}

VhdlConstraint *VhdlAccessConstraint::ElementConstraint() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->ElementConstraint() ;
}

VhdlConstraint *VhdlAccessConstraint::IndexConstraint() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IndexConstraint() ;
}

VhdlConstraint *VhdlAccessConstraint::ElementConstraintAt(unsigned pos) const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->ElementConstraintAt(pos) ;
}

VhdlConstraint *VhdlAccessConstraint::ElementConstraintFor(VhdlIdDef *ele_id) const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->ElementConstraintFor(ele_id) ;
}

VhdlValue *VhdlAccessConstraint::Left() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->Left() ;
}

VhdlValue *VhdlAccessConstraint::Right() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->Right() ;
}

VhdlValue *VhdlAccessConstraint::Low() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->Low() ;
}

VhdlValue *VhdlAccessConstraint::High() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->High() ;
}

unsigned VhdlAccessConstraint::Dir() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->Dir() ;
}

verific_uint64 VhdlAccessConstraint::Length() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->Length() ;
}

unsigned VhdlAccessConstraint::NumBitsOfRange() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->NumBitsOfRange() ;
}

unsigned VhdlAccessConstraint::IsSignedRange() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsSignedRange() ;
}

unsigned VhdlAccessConstraint::PosOf(VhdlValue *value) const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->PosOf(value) ;
}

void VhdlAccessConstraint::ReverseDirection()
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return ;

    designated_constraint->ReverseDirection() ;
}

char *VhdlAccessConstraint::Image() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->Image() ;
}

VhdlValue *VhdlAccessConstraint::CreateValue(const char * /*name*/, unsigned /*prepend_name_path*/, VhdlIdDef * /*port_id*/, Instance * /*inst*/)
{
    return new VhdlAccessValue(0, Copy()) ;
}

VhdlValue *VhdlAccessConstraint::CreateInitialValue(const VhdlTreeNode * /*from*/)
{
    return new VhdlAccessValue(0, Copy()) ;
}

VhdlValue *VhdlAccessConstraint::CreateXValue()
{
    return 0 ; // break loop: ask directly on the designated_constraint
}

VhdlValue *VhdlAccessConstraint::CreateFullInitialValue(const VhdlTreeNode * /*from*/)
{
    return new VhdlAccessValue(0, Copy()) ;
}

verific_uint64 VhdlAccessConstraint::GetSizeForConstraint() const
{
    if (_is_recursion) return 0 ; // return non-deciding value

    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    const_cast<VhdlAccessConstraint *>(this)->_is_recursion = 1 ;
    verific_uint64 return_value = designated_constraint->HasNullRange() ;
    const_cast<VhdlAccessConstraint *>(this)->_is_recursion = 0 ;

    return return_value ;
}

verific_uint64 VhdlAccessConstraint::GetNumBitsForConstraint() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->GetNumBitsForConstraint() ;
}

void VhdlAccessConstraint::SetUnconstrained()
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return ;

    designated_constraint->SetUnconstrained() ;
}

unsigned VhdlAccessConstraint::IsNullRange() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsNullRange() ;
}

unsigned VhdlAccessConstraint::HasNullRange() const
{
    if (_is_recursion) return 0 ; // return non-deciding value

    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    const_cast<VhdlAccessConstraint *>(this)->_is_recursion = 1 ;
    unsigned return_value = designated_constraint->HasNullRange() ;
    const_cast<VhdlAccessConstraint *>(this)->_is_recursion = 0 ;

    return return_value ;
}

unsigned VhdlAccessConstraint::IsUnconstrained() const
{
    if (_is_recursion) return 0 ; // return non-deciding value

    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    const_cast<VhdlAccessConstraint *>(this)->_is_recursion = 1 ;
    unsigned return_value = designated_constraint->IsUnconstrained() ;
    const_cast<VhdlAccessConstraint *>(this)->_is_recursion = 0 ;

    return return_value ;
}

unsigned VhdlAccessConstraint::IsBitEncoded() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsBitEncoded() ;
}

unsigned VhdlAccessConstraint::UserEncoded() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->UserEncoded() ;
}

unsigned VhdlAccessConstraint::OnehotEncoded() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->OnehotEncoded() ;
}

unsigned VhdlAccessConstraint::Contains(VhdlValue *value) const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->Contains(value) ;
}

unsigned VhdlAccessConstraint::IsRangeConstraint() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsRangeConstraint() ;
}

unsigned VhdlAccessConstraint::IsArrayConstraint() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsArrayConstraint() ;
}

unsigned VhdlAccessConstraint::IsRecordConstraint() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsRecordConstraint() ;
}

unsigned VhdlAccessConstraint::IsAccessConstraint() const { return 1 ; }

unsigned VhdlAccessConstraint::IsEnumRange() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsEnumRange() ;
}

unsigned VhdlAccessConstraint::IsRealRange() const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->IsRealRange() ;
}

VhdlConstraint *VhdlAccessConstraint::MergeConstraints(VhdlConstraint *other_constraint, unsigned oper_type)
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->MergeConstraints(other_constraint, oper_type) ;
}

void VhdlAccessConstraint::ConstraintByAccessValue(VhdlValue * /*value*/)
{
    return ;
#if 0 // Viper #7288
    if (!value) return ;

    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return ;

    designated_constraint->ConstraintByAccessValue(value) ;
#endif
}

void VhdlAccessConstraint::ConstraintBy(VhdlValue * /*value*/)
{
    return ;
#if 0 // Viper #7288
    if (!value) return ;

    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return ;

    designated_constraint->ConstraintBy(value) ;
#endif
}

VhdlDiscreteRange *VhdlAccessConstraint::CreateRangeNode(linefile_type linefile)
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->CreateRangeNode(linefile) ;
}

Array *VhdlAccessConstraint::CreateAssocList(linefile_type linefile, VhdlIdDef *type)
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->CreateAssocList(linefile, type) ;
}

VhdlValue *VhdlAccessConstraint::ValueOf(unsigned pos) const
{
    const VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->ValueOf(pos) ;
}

VhdlSubtypeIndication *VhdlAccessConstraint::CreateSubtype(VhdlIdDef *base_type, VhdlTreeNode *from)
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    return designated_constraint->CreateSubtype(base_type, from) ;
}

unsigned VhdlAccessConstraint::CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df)
{
    if (_is_recursion) return 1 ; // return non-deciding value

    // Viper #8198: both are forward decl constraint from same type
    const VhdlIdDef *other_type = other ? other->GetDesignatedType() : 0 ;
    if (_designated_type && _designated_type == other_type) return 1 ;

    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return 0 ;

    _is_recursion = 1 ;
    unsigned return_value = designated_constraint->CheckAgainst(other, from, df) ;
    _is_recursion = 0 ;

    return return_value ;
}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
void VhdlAccessConstraint::FillUpRanges(VeriScope* scope, VeriIdDef* id, unsigned pos, const linefile_type line_file)
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return ;

    designated_constraint->FillUpRanges(scope, id, pos, line_file) ;
}
#endif

void VhdlAccessConstraint::ConstraintBy(VhdlConstraint * /*constraint*/)
{
    return ;
#if 0 // Viper #7288
    if (_is_recursion) return ;

    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return ;

    _is_recursion = 1 ;
    designated_constraint->ConstraintBy(constraint) ;
    _is_recursion = 0 ;
#endif
}

VhdlConstraint *VhdlConstraint::GetDesignatedConstraint() { return this ; }

VhdlConstraint *VhdlAccessConstraint::GetDesignatedConstraint()
{
    if (!_designated_constraint && _designated_type) {
        VhdlConstraint *constraint = _designated_type->Constraint() ;
        if (constraint && !_is_recursion) {
            _is_recursion = 1 ;
            _designated_constraint = constraint->Copy() ;
            _is_recursion = 0 ;
            _designated_type = 0 ;
        }
    }

    return _designated_constraint ;
}

const VhdlConstraint *VhdlConstraint::GetDesignatedConstraint() const { return this ; }

const VhdlConstraint *VhdlAccessConstraint::GetDesignatedConstraint() const
{
    return _designated_type ? _designated_type->Constraint() : _designated_constraint ;
}

/***************************************************************/
// Constraint a composite value by range constraint
/***************************************************************/

void VhdlValue::ConstraintBy(VhdlConstraint * /* constraint */) { }

void VhdlCompositeValue::ConstraintBy(VhdlConstraint *constraint)
{
    if (!_values || !constraint || constraint->IsUnconstrained()) return ;

    delete _range ;
    VhdlConstraint *index_constraint = constraint->IndexConstraint() ;
    _range = index_constraint ? index_constraint->Copy() : 0 ;

    unsigned i ;
    VhdlValue *elem_val ;
    FOREACH_ARRAY_ITEM(_values, i, elem_val) {
        if (!elem_val) continue ;
        elem_val->ConstraintBy(constraint->ElementConstraintAt(i)) ;
    }

    if (_default_elem_val) _default_elem_val->ConstraintBy(constraint->ElementConstraint()) ;
}

void VhdlAccessValue::ConstraintBy(VhdlConstraint *constraint)
{
    if (!constraint || !_value || _is_recursion) return ;

    // access value must be constrained by access constraint
    VERIFIC_ASSERT(constraint->IsAccessConstraint()) ;

    if (_constraint) _constraint->ConstraintBy(constraint) ;
    _is_recursion = 1 ;
    _value->ConstraintBy(constraint->GetDesignatedConstraint()) ;
    _is_recursion = 0 ;
}

const VhdlIdDef *VhdlConstraint::GetDesignatedType() const { return 0 ; }
const VhdlIdDef *VhdlAccessConstraint::GetDesignatedType() const { return _designated_type ; }

void VhdlConstraint::ReConstraintByValue(VhdlValue *value)
{
    ConstraintBy(value) ;
}

void VhdlRecordConstraint::ReConstraintByValue(VhdlValue *value)
{
    unsigned i ;
    VhdlConstraint *ele ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele) {
        if (!ele || !ele->IsUnconstrained()) continue ;
        VhdlValue *ele_val = value ? value->ValueAt(i): 0 ;
        ele->ReConstraintByValue(ele_val) ;
    }
}

void VhdlArrayConstraint::ReConstraintByValue(VhdlValue *value)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;
    if (!value) return ;

    // Use num of elements in value, left bound and direction of constraint
    // to create a new constrained value
    _index_constraint->ReConstraintByValue(value) ;

    // Do this for elements too (multi-dim arrays)
    // use first element for that
    _element_constraint->ReConstraintByValue(value->ValueAt(0)) ;
}

void VhdlAccessConstraint::ReConstraintByValue(VhdlValue *value)
{
    VhdlConstraint *designated_constraint = GetDesignatedConstraint() ;
    if (!designated_constraint) return ;

    designated_constraint->ReConstraintByValue(value) ;
}

