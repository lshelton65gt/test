/*
 *
 * [ File Version : 1.178 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_VALUE_H_
#define _VERIFIC_VHDL_VALUE_H_

#include "VhdlTreeNode.h"

// Values and constraints are ONLY valid during elaboration

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VhdlIdDef ;
class Net ;
class Map ;
class Set ;
class Instance ;

class VhdlInt ;
class VhdlDouble ;
class VhdlEnum ;
class VhdlNonconst ;
class VhdlNonconstBit ;
class VhdlPhysicalValue ;
class VhdlCompositeValue ;
class VhdlAccessValue ;

class VhdlConstraint ;
class VhdlRangeConstraint ;
class VhdlArrayConstraint ;
class VhdlRecordConstraint ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Need for vhdl to verilog package conversion
class VeriScope ;
class VeriIdDef ;
class VeriExpression ;
#endif

// Handy macro's
#undef MAX
#define MAX(a,b) (((a)>(b)) ? (a) : (b))
#undef ABSOLUTE
#define ABSOLUTE(s) (((s) > 0) ? (s) : -(s))

 //  Define net structure. This is used to create
 //  constant nets in static elaboration process.
 //  Constant nets are used within the value structure.
class VFC_DLL_PORT Net
{
public:
    explicit Net(const char *name = 0, linefile_type linefile = 0) ;
    ~Net();

private:
    // Prevent compiler from defining the following
    Net(const Net &) ;            // Purposely leave unimplemented
    Net& operator=(const Net &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    const char *Name() ;

    unsigned  IsConstant() const ;
    unsigned  IsPwr() const ;
    unsigned  IsGnd() const ;
    unsigned  IsX() const ;
    unsigned  IsZ() const ;
    unsigned  IsWired() const { return 0 ; }
    unsigned  IsUserDeclared() const { return 0 ; }
    Net  *InvNet() const ;
private:
    char *_name ;
} ; // class Net

/* -------------------------------------------------------------- */

/*
   VhdlValue is the abstract base class for all value classes (i.e. integers, reals, etc...).
   This class does not need to be a VhdlTreeNode, since it's an elaboration-specific class,
   but it is a VhdlNode since it needs certain VhdlNode static methods.
*/
class VFC_DLL_PORT VhdlValue : public VhdlNode
{
protected: // Abstract class
    VhdlValue();

public:
    virtual ~VhdlValue();

private:
    // Prevent compiler from defining the following
    VhdlValue(const VhdlValue &) ;            // Purposely leave unimplemented
    VhdlValue& operator=(const VhdlValue &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *         Copy() const = 0 ;
    virtual VhdlValue *         CopyExplicit() const ;

    virtual void                MakeInteger() { } // Viper #6518

    // Tests
    virtual unsigned            IsConstant() const = 0 ;
    virtual unsigned            IsNull() const { return 0 ; }
    virtual unsigned            IsAccessValue() const ;
    virtual unsigned            IsTrue() const ;
    virtual unsigned            IsSigned() const=0 ;
    virtual void                SetSigned()=0 ;
    virtual unsigned            HasX() const =0 ;
    virtual unsigned            HasUXZW() const  { return 0 ; }
    // RAM value related tests :
    virtual unsigned            IsComposite() const ;
    virtual unsigned            IsEnumValue() const { return 0 ; }
    virtual unsigned            IsDualPortValue() const ;
    // Checks whether a value contains metalogical values
    // like X Z L H
    virtual unsigned            IsMetalogical() const =0 ;
    virtual unsigned            IsMetalogicalWithDC() const { return 0 ;}
    virtual unsigned            CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df)=0 ; // Subtype check of constraint against value
    virtual unsigned            NumBits() const=0 ;    // The number of bits this value contains
    virtual verific_int64       Integer() const=0 ;    // Return an 'integer' value for the (constant) value
    virtual double              Real() const   { return 0 ; }
    virtual char *              Image() const=0;       // Return allocated string that represents the (constant) value
    virtual const char         *EnumName() const { return 0 ; } // Return name (not allocated image) if this is an enumeration literal value
    virtual char *              StringImage() const { return 0 ; } // Produce a char* allocated string for an array of VeriValue that is known to be an array of 'character' elements (needed for 'report' and 'assertion' statements).
    virtual char *              ToString(unsigned /*flavor*/, const VhdlValue * /*arg2*/) const ; // Vhdl 2008 LRM 5.7, 5.3.2.4 etc
    virtual char *              ImageVerilog() const=0;       // Return allocated string that represents the (constant) value in Verilog literal form.
    virtual unsigned            IsEqual(const VhdlValue *other_val) const = 0 ; // Returns 1 if two constant values are the same. 0 otherwise.
    virtual unsigned            IsReal() const ;       // Test if this value is a 'real' number
    virtual unsigned            IsIntVal() const ;     // Test if this value is an IntVal
    virtual unsigned            IsPhysicalValue() const ; // Test if this value is a 'physical' value like 'time'
    virtual char *              ImageShort() const;       // Same as image except for composite value Viper #6093

    // For 'event' values : clock-enable net (VIPER 2146) :
    virtual void                SetEnableNet(Net * /*en*/)   {  }
    virtual Net *               GetEnableNet() const         { return 0 ; }

    virtual VhdlConstraint *    Constraint()                 { return 0 ; }

#ifdef DB_INFER_WIDE_OPERATORS
    // Check only for composite value
    virtual unsigned            IsBitVector() const ; // returns 1 if each element of this value is of size '1', returns 0 otherwise
    static unsigned             AreAllInputSetsEqual(const Array *inputs, unsigned size, Array *one_set = 0) ; // Checks if all set of inputs are equal (X matches to anything). Inserts one of the sets into Array 'one_set'
#endif // DB_INFER_WIDE_OPERATORS

    // Initial value preservation routine, properly defined on VhdlNonconstBit, VhdlNonconst and VhdlCompositeValue
    virtual void                SetInitialValue(VhdlValue *init_val) ;

    // Operator calls
    virtual VhdlValue *         UnaryOper(unsigned oper_type, const VhdlTreeNode *from)=0 ;
    virtual VhdlValue *         BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from)=0 ;
    virtual VhdlValue *         SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from)=0 ;
    // Wide selectors/Mux created based on a constraint (of the result value)
    static VhdlValue *          Nto1Mux(const VhdlNonconst *condition, const Array *input_values, const VhdlTreeNode *from) ;
    static VhdlValue *          Selector(Array *one_hot_nets, const Array *input_values, const VhdlTreeNode *from) ;
    static VhdlValue *          PrioSelector(const VhdlValue *else_value, Array *selector_nets, const Array *input_values, const VhdlTreeNode *from) ;

    virtual VhdlValue *         PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from)=0 ;              // Insert buffers into this value to preserve user nets in a concurrent assignment. (support for the PRESERVE_USER_NETS switch)
    virtual VhdlValue *         PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from)=0 ; // Insert buffers to preserve line/file of a sequential assignment. (support for the PRESERVE_ASSIGNMENTS switch)

    // Constant, scalar, same-type value tests
    virtual unsigned            LT(const VhdlValue *v) const ;

    // Purposely don't make the following public GT methods virtual.  Only the protected ones
    // are virtual, because we DON'T want to have overloaded virtual methods!
    unsigned                    GT(const VhdlInt *v) const ;
    unsigned                    GT(const VhdlDouble *v) const ;
    unsigned                    GT(const VhdlEnum *v) const ;
protected :
    virtual unsigned            GT_Int(const VhdlInt *v) const ;
    virtual unsigned            GT_Double(const VhdlDouble *v) const ;
    virtual unsigned            GT_Enum(const VhdlEnum *v) const ;

public :
    virtual verific_int64       Position() const ;              // Integer number representing a constant scalar value position LRM 3.1
    virtual VhdlIdDef *         GetType() const ;               // Type of this value. Works only for constant enum values
    virtual VhdlIdDef *         GetEnumId() const { return 0 ;} // Works only for constant enum values

    // Index into a composite value (array or record), by hard position
    virtual VhdlValue *         ValueAt(unsigned pos) const ;
    virtual VhdlValue *         TakeValueAt(unsigned pos) const ;            // Take memory control over the element (it will be removed from the array it was in)
    virtual void                SetValueAt(unsigned pos, VhdlValue *elem) ;  // Set new element value and overwrite old one
    virtual void                RemoveValueAt(unsigned pos) ;                // Actually remove the element at position 'pos' from the composite value
    virtual void                NetAssignAt(unsigned pos, VhdlValue *elem) ; // Wire nets of this element value into new value nets

    virtual unsigned            NumElements() const ;            // Number of elements in composite value
    virtual void                PrependElement(VhdlValue *val) ;
    virtual void                AppendElement(VhdlValue *val) ;

    virtual VhdlValue *         GetAccessedValue() const { return 0 ; } // Access values only : return value that this access value points to.
    virtual void                SetAccessedValue(VhdlValue * /*value*/) { } // Access values only : swap pointer of an access value to another value.

    virtual unsigned            UserEncoded() const ;            // Check if this value is user-encoded
    virtual unsigned            OnehotEncoded() const ;          // Check if this value is one-hot encoded

    virtual unsigned            IsBit() const=0 ;                // Check if value came from a bit-encoded enumeration type
    virtual Net *               GetBit() const=0 ;               // Get bit Net from single-bit encoded values
    virtual Net *               GetBit(unsigned pos) const=0 ;   // Get bit Net from a multi-bit encoded value

    virtual Array *             GetValues() const    { return 0 ; }  // Needed for VhdlCompositeValue
    virtual VhdlValue *         GetInnerValue()      { return this ; } // Returns inner value for access_val

// Obsolete now that we handle external references/errors in database    virtual Netlist   *GetNetlist() const=0 ;           // Return the netlist that this value came from. Return 0 for constants.

    // Value translators
    virtual VhdlNonconst *       ToNonconst()=0 ;
    virtual VhdlNonconstBit *    ToNonconstBit()=0 ;
    virtual VhdlCompositeValue * ToComposite(unsigned size)=0 ;
    virtual VhdlValue *          ToConst(VhdlIdDef *type)=0 ; // Translate a value back to a real constant ; works only for constant NonConstVal's
    virtual void                 Adjust(unsigned /*size*/) { return ; }

    virtual void                 CollectDCs(Set& /*s*/, unsigned & /*runner*/) { }

    // Type conversion (VHDL LRM 7.3.5 semantics)
    virtual VhdlValue *         ToType(VhdlIdDef *type, VhdlTreeNode *from) ;

    // Wire an assigned value val into the nets of nonconstant 'this'
    virtual void                NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from)=0 ; // Wire nets of this value into new value nets

    // Set attributes on netlist objects in this value
    virtual void                SetAttribute(const char *att_name, const char *att_value) ;

    // Special values
    virtual unsigned            IsEvent() const ;
    virtual unsigned            IsStable() const ;
    virtual unsigned            IsEdge() const ;
    virtual void                PopulateValues(Array* /*values*/) const { }
    virtual unsigned            RepresentCount() const { return 1 ; }
    virtual VhdlEnum *          ToEnum() { return 0 ; }

    // Viper #7288: range over composite value for irregular array constraint
    virtual void                ConstraintBy(VhdlConstraint *range) ;
    virtual VhdlConstraint     *GetRange() const { return 0 ; }
#ifdef VHDL_PRESERVE_LITERALS
    virtual char                GetBaseSpecifier() const  { return 0 ; }
    virtual void                SetBaseSpecifier(char /*base*/)   { }
#endif

public:
    // Purposely don't make the following public ReverseBinaryOper methods virtual.  Only the
    // protected ones are virtual, because we DON'T want to have overloaded virtual methods!
    VhdlValue *                 ReverseBinaryOper(VhdlInt *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlValue *                 ReverseBinaryOper(VhdlDouble *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlValue *                 ReverseBinaryOper(VhdlEnum *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlValue *                 ReverseBinaryOper(VhdlNonconst *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlValue *                 ReverseBinaryOper(VhdlNonconstBit *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlValue *                 ReverseBinaryOper(VhdlPhysicalValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlValue *                 ReverseBinaryOper(VhdlCompositeValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlValue *                 ReverseBinaryOper(VhdlAccessValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

protected:
    virtual VhdlValue *         ReverseBinaryOper_Int(VhdlInt *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Double(VhdlDouble *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Enum(VhdlEnum *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Nonconst(VhdlNonconst *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_NonconstBit(VhdlNonconstBit *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Physical(VhdlPhysicalValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Composite(VhdlCompositeValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Access(VhdlAccessValue * /*left_val*/, unsigned oper_type, const VhdlTreeNode *from) ;

public:
    virtual VhdlExpression *    CreateConstantVhdlExpression(linefile_type linefile, VhdlIdDef * type, int dim) ;// Convert 'this' to expression, works only for constant values
    virtual VhdlExpression *    CreateConstantVhdlExpressionInternal(linefile_type /*linefile*/ , VhdlIdDef * /*type*/, int /*dim*/, unsigned /*without_other*/) { return 0 ; } // Convert 'this' to expression, works only for constant values
    virtual unsigned            HasAccessValue() const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression *    ConvertToVerilogExpression(VeriScope* /*scope*/, VhdlIdDef* /*type*/, const linefile_type /*line_file*/)   { return 0 ; }
#endif

    // Support routines for wide selectors/mux (not using value itself)
    static Net *                BuildNto1Mux(Array &conditions, Array &in_nets, const VhdlTreeNode *from) ;
    static void                 InsertValToConds(Map *val_to_conds, const Net *c_net, const Net *val_net) ;
    static Net *                BuildSelector(Map &val_to_conds, const VhdlTreeNode *from) ;

    static VhdlValue *          ConnectSetResetNetsToRegister(Array& selector_nets, Array& values, const VhdlValue *hold_value, const VhdlTreeNode *from) ;
} ; // class VhdlValue

/* -------------------------------------------------------------- */

/*
  VhdlInt is a VhdlValue which represents an integer value.
*/
class VFC_DLL_PORT VhdlInt : public VhdlValue
{
public:
    explicit VhdlInt(verific_int64 value);
    virtual ~VhdlInt();

private:
    // Prevent compiler from defining the following
    VhdlInt() ;                           // Purposely leave unimplemented
    VhdlInt(const VhdlInt &) ;            // Purposely leave unimplemented
    VhdlInt& operator=(const VhdlInt &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *         Copy() const ;

    // Modify..
    virtual void                MakeInteger() { _value = (int) (_value) ; } // Viper #6518

    // Tests
    virtual unsigned            IsConstant() const ;
    virtual unsigned            IsTrue() const ;
    virtual unsigned            IsSigned() const ;
    virtual void                SetSigned() ;
    virtual unsigned            HasX() const ;
    virtual unsigned            IsMetalogical() const ;
    virtual unsigned            CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value
    virtual unsigned            NumBits() const ; // The number of bits this value contains
    virtual verific_int64       Integer() const ;
    verific_int64               GetValue() const   { return _value ; } // Same functionality as Integer(), except name is more generic.
    virtual char *              Image() const ;
    virtual char *              ImageVerilog() const ;
    virtual unsigned            IsEqual(const VhdlValue *other_val) const ;
    virtual unsigned            IsIntVal() const ; // Test if this value is an IntVal

    // Operators
    virtual VhdlValue *         UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;
    virtual VhdlValue *         PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from) ;              // Insert buffers into this value..
    virtual VhdlValue *         PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from) ; // Insert buffers to preserve line/file of a sequential assignment.

    // Constant value tests
    virtual unsigned            LT(const VhdlValue *v) const ;
protected:
    virtual unsigned            GT_Int(const VhdlInt *v) const ;
public:
    virtual verific_int64       Position() const ;

    virtual unsigned            IsBit() const ;
    virtual Net *               GetBit() const ;
    virtual Net *               GetBit(unsigned pos) const ;

    virtual VhdlNonconst *      ToNonconst() ;
    virtual VhdlNonconstBit *   ToNonconstBit() ;
    virtual VhdlCompositeValue * ToComposite(unsigned size) ;
    virtual VhdlValue *         ToConst(VhdlIdDef *type) ;
    virtual VhdlValue *         ToType(VhdlIdDef *type, VhdlTreeNode *from) ;

    virtual void                CollectDCs(Set& /*s*/, unsigned & runner) ;

    virtual void                NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from) ; // Wire nets of this value into new value nets

protected:
    virtual VhdlValue *         ReverseBinaryOper_Int(VhdlInt *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Double(VhdlDouble *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Physical(VhdlPhysicalValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

public:
    virtual VhdlExpression *    CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other ) ; // Convert 'this' to expression, works only for constant values

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression *    ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file) ;
#endif
protected:
    friend class VhdlDouble ; // needs access for double<->int operations
    friend class VhdlPhysicalValue ; // Needs access for physical<->int operations
    verific_int64  _value ;
} ; // class VhdlInt

/* -------------------------------------------------------------- */

/*
   VhdlPhysicalValue is a VhdlValue which represents a physical value like 'time'.
*/
class VFC_DLL_PORT VhdlPhysicalValue : public VhdlInt
{
public:
    explicit VhdlPhysicalValue(verific_int64 value) ;
    virtual ~VhdlPhysicalValue() ;

private:
    // Prevent compiler from defining the following
    VhdlPhysicalValue() ;                                     // Purposely leave unimplemented
    VhdlPhysicalValue(const VhdlPhysicalValue &) ;            // Purposely leave unimplemented
    VhdlPhysicalValue& operator=(const VhdlPhysicalValue &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *         Copy() const ;

    // Tests
    virtual unsigned            IsPhysicalValue() const ; // Test if this value is a 'physical' value like 'time'

    virtual unsigned            CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value

    virtual VhdlValue *         UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual char *              ToString(unsigned /*flavor*/, const VhdlValue *arg2) const; // Vhdl 2008 LRM 5.7, 5.3.2.4 etc

protected:
    virtual VhdlValue *         ReverseBinaryOper_Int(VhdlInt *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Double(VhdlDouble *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Physical(VhdlPhysicalValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
} ; // class VhdlPhysicalValue

/* -------------------------------------------------------------- */

/*
   VhdlDouble is a VhdlValue which represents an double (or real) value.
*/
class VFC_DLL_PORT VhdlDouble : public VhdlValue
{
public:
    explicit VhdlDouble(double value);
    virtual ~VhdlDouble();

private:
    // Prevent compiler from defining the following
    VhdlDouble() ;                              // Purposely leave unimplemented
    VhdlDouble(const VhdlDouble &) ;            // Purposely leave unimplemented
    VhdlDouble& operator=(const VhdlDouble &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *         Copy() const ;

    // Tests
    virtual unsigned            IsConstant() const ;
    virtual unsigned            IsSigned() const ;
    virtual void                SetSigned() ;
    virtual unsigned            HasX() const ;
    virtual unsigned            IsMetalogical() const ;
    virtual unsigned            CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value
    virtual unsigned            NumBits() const ;  // The number of bits this value contains
    virtual verific_int64       Integer() const ;
    double                      GetValue() const   { return _value ; }
    virtual double              Real() const   { return _value ; }
    virtual char *              Image() const ;
    virtual char *              ToString(unsigned /*flavor*/, const VhdlValue *arg2) const; // Vhdl 2008 LRM 5.7, 5.3.2.4 etc
    virtual char *              ImageVerilog() const ;
    virtual unsigned            IsEqual(const VhdlValue *other_val) const ;
    virtual unsigned            IsReal() const ;   // Test if this value is a 'real' number

    // Operators
    virtual VhdlValue *         UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;
    virtual VhdlValue *         PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from) ; // Insert buffers into this value..
    virtual VhdlValue *         PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from) ; // Insert buffers to preserve line/file of a sequential assignment.

    // Constant value tests
    virtual unsigned            LT(const VhdlValue *v) const ;
protected:
    virtual unsigned            GT_Double(const VhdlDouble *v) const ;

public:
    virtual unsigned            IsBit() const ;
    virtual Net *               GetBit() const ;
    virtual Net *               GetBit(unsigned pos) const ;

    virtual VhdlNonconst *      ToNonconst() ;
    virtual VhdlNonconstBit *   ToNonconstBit() ;
    virtual VhdlCompositeValue * ToComposite(unsigned size) ;
    virtual VhdlValue *         ToConst(VhdlIdDef *type) ;
    virtual VhdlValue *         ToType(VhdlIdDef *type, VhdlTreeNode *from) ;

    virtual void                CollectDCs(Set& s, unsigned & runner) ;

    virtual void                NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from) ; // Wire nets of this value into new value nets

protected:
    virtual VhdlValue *         ReverseBinaryOper_Double(VhdlDouble *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Int(VhdlInt *left, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         ReverseBinaryOper_Physical(VhdlPhysicalValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

public:
    virtual VhdlExpression *    CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other ) ; // Convert 'this' to expression, works only for constant values

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression *    ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file) ;
#endif
protected:
    friend class VhdlInt ; // Needs access for double-int operations
    friend class VhdlPhysicalValue ; // Needs access for double-physical operations
    double  _value ;
} ; // class VhdlDouble

/* -------------------------------------------------------------- */

/*
   VhdlEnum is a VhdlValue which represents an enumeration value.
*/
class VFC_DLL_PORT VhdlEnum : public VhdlValue
{
public:
    explicit VhdlEnum(VhdlIdDef *enum_id);
    virtual ~VhdlEnum();

private:
    // Prevent compiler from defining the following
    VhdlEnum() ;                            // Purposely leave unimplemented
    VhdlEnum(const VhdlEnum &) ;            // Purposely leave unimplemented
    VhdlEnum& operator=(const VhdlEnum &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *     Copy() const ;
    virtual unsigned        IsTrue() const ;
    virtual unsigned        IsSigned() const ;
    virtual void            SetSigned() ;
    virtual unsigned        HasX() const ;
    virtual unsigned        HasUXZW() const ;
    virtual unsigned        IsMetalogical() const ;
    virtual unsigned        IsMetalogicalWithDC() const ;
    virtual unsigned        CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value
    virtual unsigned        NumBits() const ;          // The number of bits this value contains
    virtual verific_int64   Integer() const ;
    virtual const char     *EnumName() const ; // return name (not allocated image) if this is an enumeration literal value
    virtual char *          Image() const ;
    virtual char *          ImageVerilog() const ;
    virtual unsigned        IsEqual(const VhdlValue *other_val) const ;

    // Tests
    virtual unsigned        IsConstant() const ;
    virtual unsigned        RepresentCount() const ;
    virtual unsigned        IsEnumValue() const { return 1 ; }

    // Operators
    virtual VhdlValue *     UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;
    virtual VhdlValue *     PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from) ; // Insert buffers into this value..
    virtual VhdlValue *     PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from) ; // Insert buffers to preserve line/file of a sequential assignment.

    // Constant value tests
    virtual unsigned        LT(const VhdlValue *v) const ;
protected:
    virtual unsigned        GT_Enum(const VhdlEnum *v) const ;

public:
    virtual verific_int64   Position() const ;
    virtual VhdlIdDef *     GetType() const ;        // Works only for constant enum values
    virtual VhdlIdDef *     GetEnumId() const    { return _enum_id ; }

    virtual unsigned        UserEncoded() const ;     // Check if this value is user-encoded (instead of default binary)
    virtual unsigned        OnehotEncoded() const ;   // Check if this value is one-hot encoded
    virtual unsigned        IsBit() const ;
    virtual Net *           GetBit() const ;
    virtual Net *           GetBit(unsigned pos) const ;

    virtual VhdlNonconst *          ToNonconst() ;
    virtual VhdlNonconstBit *       ToNonconstBit() ;
    virtual VhdlCompositeValue *    ToComposite(unsigned size) ;
    virtual VhdlValue *             ToConst(VhdlIdDef *type) ;

    virtual void                    CollectDCs(Set& s, unsigned & runner) ;

    virtual void            NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from) ; // Wire nets of this value into new value nets
    VhdlNonconstBit *       Equal(VhdlValue *r, unsigned neq, const VhdlTreeNode *from) ;
    virtual VhdlEnum *      ToEnum() { return this ;}

protected:
    virtual VhdlValue *     ReverseBinaryOper_Enum(VhdlEnum *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     ReverseBinaryOper_Nonconst(VhdlNonconst *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     ReverseBinaryOper_NonconstBit(VhdlNonconstBit *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

public:
    virtual VhdlExpression *CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other ) ; // Convert 'this' to expression, works only for constant values

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression *ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file) ;
#endif
protected:
    VhdlIdDef   *_enum_id ;
} ; // class VhdlEnum

/* -------------------------------------------------------------- */

/*
   VhdlNonconst is a VhdlValue which represents non-constant scalars.
*/
class VFC_DLL_PORT VhdlNonconst : public VhdlValue
{
public:
    explicit VhdlNonconst(Array *nets, unsigned is_signed=0, unsigned is_encoded=0);
    virtual ~VhdlNonconst();

private:
    // Prevent compiler from defining the following
    VhdlNonconst() ;                                // Purposely leave unimplemented
    VhdlNonconst(const VhdlNonconst &) ;            // Purposely leave unimplemented
    VhdlNonconst& operator=(const VhdlNonconst &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *     Copy() const ;

    // Tests
    virtual unsigned        IsConstant() const ;
    virtual unsigned        IsTrue() const ;
    virtual unsigned        IsSigned() const ;
    virtual void            SetSigned() ;
    virtual unsigned        HasX() const ;
    virtual unsigned        IsMetalogical() const ;
    virtual unsigned        CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value
    virtual unsigned        NumBits() const ; // The number of bits this value contains
    virtual verific_int64   Integer() const ;
    virtual char *          Image() const ;
    virtual char *          ImageVerilog() const ;
    virtual unsigned        IsEqual(const VhdlValue *other_val) const ;

    // Initial value preservation routine
    virtual void            SetInitialValue(VhdlValue *init_val) ;

    // Operators
    virtual VhdlValue *     UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;
    virtual VhdlValue *     PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from) ;              // Insert buffers into this value..
    virtual VhdlValue *     PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from) ; // Insert buffers to preserve line/file of a sequential assignment.

    virtual unsigned        UserEncoded() const ;     // Check if this value is user-encoded (instead of default binary)
    virtual unsigned        OnehotEncoded() const ;   // Check if this value is one-hot encoded
    virtual unsigned        IsBit() const ;
    virtual Net *           GetBit() const ;
    virtual Net *           GetBit(unsigned pos) const ;

    virtual VhdlNonconst *       ToNonconst() ;
    virtual VhdlNonconstBit *    ToNonconstBit() ;
    virtual VhdlCompositeValue * ToComposite(unsigned size) ;
    virtual VhdlValue *          ToConst(VhdlIdDef *type) ;

    virtual void                 CollectDCs(Set& /*s*/, unsigned & runner) ;

    virtual void            NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from) ; // Wire nets of this value into new value nets

    virtual void            SetAttribute(const char *att_name, const char *att_value) ;

protected:
    virtual VhdlValue *     ReverseBinaryOper_Nonconst(VhdlNonconst *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     ReverseBinaryOper_Enum(VhdlEnum *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

public:
    // Support routines for operators
    virtual void            Adjust(unsigned size) ;
    void                    Prune() ;          // Reduce to minimum size (prune MSB side for 0/sign bits)
    unsigned                Size() const;      // Number of nets in here. Old routine. use NumBits instead.
    unsigned                ActiveSize() const; // Number of nets in here. Old routine. use NumBits instead.
    Array *                 Nets() const ;     // The nets in here

    // Actual Operators
    VhdlNonconst *          Invert(const VhdlTreeNode *from) ;
    VhdlNonconstBit *       Reduce(unsigned type, const VhdlTreeNode *from) ;
    VhdlNonconst *          Abs(const VhdlTreeNode *from) ;
    VhdlNonconst *          UnaryMin(const VhdlTreeNode *from, Net *carry_in) ;
    VhdlNonconst *          Plus(VhdlNonconst *r, Net *carry_in, const VhdlTreeNode *from) ;
    VhdlNonconst *          Minus(VhdlNonconst *r, const VhdlTreeNode *from) ;
    VhdlNonconst *          Multiply(VhdlNonconst *r, const VhdlTreeNode *from) ;
    VhdlNonconst *          Divide(VhdlNonconst *r, const VhdlTreeNode *from) ;
    VhdlNonconst *          Modulo(VhdlNonconst *r, const VhdlTreeNode *from) ;
    VhdlNonconst *          Remainder(VhdlNonconst *r, const VhdlTreeNode *from) ;
    VhdlNonconst *          Binary(VhdlNonconst *r, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlNonconstBit *       Equal(VhdlNonconst *r, unsigned neq, const VhdlTreeNode *from, const Set* this_dc = 0, const Set* dc_r = 0) ;
    VhdlNonconstBit *       LessThan(VhdlNonconst *r, unsigned or_equal, const VhdlTreeNode *from) ;
    VhdlNonconst *          Shift(VhdlNonconst *r, unsigned shift_right, unsigned arithmetic, unsigned rotate, const VhdlTreeNode *from) ;
    VhdlNonconst *          Power(VhdlNonconst *r, const VhdlTreeNode *from) ;
    VhdlNonconst *          SelectOn0(const VhdlNonconstBit *select, VhdlNonconst *zero_val, const VhdlValue *initial_value, const VhdlTreeNode *from) ;
#ifdef DB_INFER_WIDE_OPERATORS
    void                    CheckHoldNet(const VhdlNonconstBit *select, VhdlNonconst *zero_val, const VhdlValue *initial_value, const VhdlTreeNode *from) const ;
#endif

    virtual VhdlExpression *CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other ) ; // Convert 'this' to expression, works only for constant values

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression *ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file) ;
#endif
protected:
    Array       *_nets ;            // Array of Net*
    unsigned     _is_signed:1 ;     // Indicates that the value is in 2-complement notation. Not unsigned
    unsigned     _user_encoded:4 ;  // Value 0 that indicates not user encoded. 1 is user encoded. 2 is one-hot encoded.
} ; // class VhdlNonconst

/* -------------------------------------------------------------- */

/*
   VhdlNonconstBit is a VhdlValue which represents non-constant bit.
*/
class VFC_DLL_PORT VhdlNonconstBit : public VhdlValue
{
public:
    explicit VhdlNonconstBit(Net *net);
    virtual ~VhdlNonconstBit();

private:
    // Prevent compiler from defining the following
    VhdlNonconstBit() ;                                   // Purposely leave unimplemented
    VhdlNonconstBit(const VhdlNonconstBit &) ;            // Purposely leave unimplemented
    VhdlNonconstBit& operator=(const VhdlNonconstBit &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *     Copy() const ;

    // Tests
    virtual unsigned        IsConstant() const ;
    virtual unsigned        IsTrue() const ;
    virtual unsigned        IsSigned() const ;
    virtual void            SetSigned() ;
    virtual unsigned        HasX() const ;
    virtual unsigned        IsMetalogical() const ;
    virtual unsigned        CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value
    virtual unsigned        NumBits() const ; // The number of bits this value contains
    virtual verific_int64   Integer() const ;
    virtual const char     *EnumName() const ; // Return name (not allocated image) if this is an enumeration literal value
    virtual char *          Image() const ;
    virtual char *          ImageVerilog() const ;
    virtual unsigned        IsEqual(const VhdlValue *other_val) const ;

    // Initial value preservation routine
    virtual void            SetInitialValue(VhdlValue *init_val) ;

    // Operators
    virtual VhdlValue *     UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;
    virtual VhdlValue *     PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from) ;              // Insert buffers into this value..
    virtual VhdlValue *     PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from) ; // Insert buffers to preserve line/file of a sequential assignment.

    virtual unsigned        IsBit() const ;
    virtual Net *           GetBit() const ;  // Same as "return _net"
    virtual Net *           GetBit(unsigned pos) const ;

    virtual VhdlNonconst *       ToNonconst() ;
    virtual VhdlNonconstBit *    ToNonconstBit() ;
    virtual VhdlCompositeValue * ToComposite(unsigned size) ;
    virtual VhdlValue *          ToConst(VhdlIdDef *type) ;

    virtual void                 CollectDCs(Set& /*s*/, unsigned & runner) ;

    virtual void            NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from) ; // Wire nets of this value into new value nets

    virtual void            SetAttribute(const char *att_name, const char *att_value) ;

protected:
    virtual VhdlValue *     ReverseBinaryOper_NonconstBit(VhdlNonconstBit *left_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     ReverseBinaryOper_Enum(VhdlEnum *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

public :
    virtual VhdlNonconstBit *SelectOn0(VhdlNonconstBit *select, VhdlNonconstBit *zero_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;

    virtual VhdlNonconstBit *Invert(const VhdlTreeNode *from) ; // Virtual, so Zvalue or Event can inherit
    VhdlNonconstBit *       Binary(VhdlNonconstBit *r, unsigned oper_type, const VhdlTreeNode *from) ;
    VhdlNonconstBit *       Equal(VhdlNonconstBit *r, unsigned neq, const VhdlTreeNode *from) ;

    virtual VhdlExpression *CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other ) ; // Convert 'this' to expression, works only for constant values

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression *ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file) ;
#endif

protected:
    Net *_net ;
} ; // class VhdlNonconstBit

/* -------------------------------------------------------------- */

/*
   VhdlCompositeValue is a VhdlValue which represent composite values (Array and Record)
   _values isthe an array of element values (as VhdlValue). All element values are owned.
   _default_elem_val serves as a default element value in absense of an explicit element value.
   Used mostly to minimize initial value memory usage (see VIPER 3905) when all element values are identical.
   is_signed is only needed in specific cases : mostly for pragma function arguments (can have
   'signed' array arguments).
*/
class VFC_DLL_PORT VhdlCompositeValue : public VhdlValue
{
public:
    explicit VhdlCompositeValue(Array *values, VhdlValue *default_elem_val=0, unsigned is_signed=0, VhdlConstraint *range = 0);
    virtual ~VhdlCompositeValue();

private:
    // Prevent compiler from defining the following
    VhdlCompositeValue() ;                                      // Purposely leave unimplemented
    VhdlCompositeValue(const VhdlCompositeValue &) ;            // Purposely leave unimplemented
    VhdlCompositeValue& operator=(const VhdlCompositeValue &) ; // Purposely leave unimplemented

public:
    //CARBON_BEGIN
    virtual VhdlValue *     GetDefaultValue() const { return _default_elem_val; }
    //CARBON_END
    virtual VhdlValue *     Copy() const ;

    // Tests
    virtual unsigned        IsConstant() const ;
    virtual unsigned        IsComposite() const ;
    virtual unsigned        IsSigned() const ;
    virtual void            SetSigned() ;
    virtual unsigned        HasX() const ;
    virtual unsigned        HasUXZW() const ;
    virtual unsigned        IsMetalogical() const ;
    virtual unsigned        IsMetalogicalWithDC() const ;
    virtual unsigned        CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value
    virtual unsigned        NumBits() const ; // The number of bits this value contains
    virtual verific_int64   Integer() const ;
    virtual char *          Image() const ;
    virtual char *          ImageVerilog() const ;
    virtual char *          ImageShort() const;       // Same as image except for composite value Viper #6093
    virtual char *          StringImage() const ; // Produce a char* allocated string for an array of VeriValue that is known to be an array of 'character' elements (needed for 'report' and 'assertion' statements).
    virtual char *          ToString(unsigned flavor, const VhdlValue * /*arg2*/) const ; // Vhdl 2008 LRM 5.7, 5.3.2.4 etc
    virtual unsigned        IsEqual(const VhdlValue *other_val) const ;

#ifdef DB_INFER_WIDE_OPERATORS
    virtual unsigned        IsBitVector() const ; // returns 1 if each element of this value is of size '1', returns 0 otherwise
#endif // DB_INFER_WIDE_OPERATORS

    // Index into composite value
    virtual VhdlValue *     ValueAt(unsigned pos) const ;
    virtual VhdlValue *     TakeValueAt(unsigned pos) const ;            // Take memory control over the element (it will be removed from the array it was in)
    virtual void            SetValueAt(unsigned pos, VhdlValue *elem) ;  // Set new element value and overwrite old one
    virtual void            RemoveValueAt(unsigned pos) ;                // Actually remove the element at position 'pos' from the composite value
    virtual void            NetAssignAt(unsigned pos, VhdlValue *elem) ; // Wire nets of this element value into new value nets

    virtual unsigned        NumElements() const ; // Number of elements in composite value

    // Manipulate the array contents
    virtual void            PrependElement(VhdlValue *val) ;
    virtual void            AppendElement(VhdlValue *val) ;

    // Initial value preservation routine
    virtual void            SetInitialValue(VhdlValue *init_val) ;

    // Operators
    virtual VhdlValue *     UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *     SelectOn1(VhdlNonconstBit *select, VhdlValue *one_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;
    virtual VhdlValue *     PreserveUserNetBuffer(VhdlValue *init_val, const VhdlTreeNode *from) ;              // Insert buffers into this value..
    virtual VhdlValue *     PreserveAssignmentBuffer(unsigned buffer_constants_too, const VhdlTreeNode *from) ; // Insert buffers to preserve line/file of a sequential assignment.

    virtual unsigned        IsBit() const ;
    virtual Net *           GetBit() const ;
    virtual Net *           GetBit(unsigned pos) const ;

    virtual VhdlNonconst *       ToNonconst() ;
    virtual VhdlNonconstBit *    ToNonconstBit() ;
    virtual VhdlCompositeValue * ToComposite(unsigned size) ;
    virtual VhdlValue *          ToConst(VhdlIdDef *type) ;

    virtual void                 CollectDCs(Set& s, unsigned & runner) ;

    // Type conversion (VHDL LRM 7.3.5 semantics)
    virtual VhdlValue *     ToType(VhdlIdDef *type, VhdlTreeNode *from) ; // VIPER #7516
    virtual void            NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from) ; // Wire nets of this value into new value nets

    virtual void            SetAttribute(const char *att_name, const char *att_value) ;

    virtual Array *         GetValues() const    { return _values ; }

    virtual void            PopulateValues(Array* values) const ;
    virtual unsigned        RepresentCount() const ;

#ifdef VHDL_PRESERVE_LITERALS
    virtual char            GetBaseSpecifier() const  { return _base_specifier ; }
    virtual void            SetBaseSpecifier(char base)   { _base_specifier = base ; }
#endif
    VhdlNonconstBit*        Equal(VhdlValue *r, unsigned neq, const VhdlTreeNode *from) ;

    virtual VhdlExpression *CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other ) ; // Convert 'this' to expression, works only for constant values
    virtual unsigned        HasAccessValue() const ;

    virtual VhdlCompositeValue *SelectOn0(VhdlNonconstBit *select, VhdlCompositeValue *zero_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;

    virtual void             Adjust(unsigned size) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression  *ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file) ;
#endif

    // Viper #7288: range over composite value for irregular array constraint
    virtual void            ConstraintBy(VhdlConstraint *range) ;
    virtual VhdlConstraint *GetRange() const { return _range ; }

protected:
    virtual VhdlValue *     ReverseBinaryOper_Composite(VhdlCompositeValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

protected:
    Array       *_values ;      // Array of VhdlValue* ; the element values. This array is always present and of correct size.
    VhdlValue   *_default_elem_val ; // value that serves as element value in absense of entry in the array. Used when all / most elements have identical values.
    VhdlConstraint *_range ; // range constraint for this value range
    unsigned     _is_signed:1 ; // This field only applicable when converting nonconst->composite and back
#ifdef VHDL_PRESERVE_LITERALS
    char          _base_specifier ;          // VIPER #8308: Preserve the original value string (radix and value) from the input RTL.
#endif

} ; // class VhdlCompositeValue

/* -------------------------------------------------------------- */

/*
   VhdlAccessValue is a VhdlValue which represent Access Type Values
*/
class VFC_DLL_PORT VhdlAccessValue : public VhdlValue
{
public:
    explicit VhdlAccessValue(VhdlValue *value, VhdlConstraint *constraint = 0) ;
    virtual ~VhdlAccessValue() ;

private:
    // Prevent compiler from defining the following
    VhdlAccessValue() ;                                   // Purposely leave unimplemented
    VhdlAccessValue(const VhdlAccessValue &) ;            // Purposely leave unimplemented
    VhdlAccessValue& operator=(const VhdlAccessValue &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *         Copy() const ; // Will NOT copy _value
    virtual VhdlValue *         CopyExplicit() const ; // Will copy even _value, used during multiple initialization e.g. variable v1, v2, v3 : a_T := new a_T(arg) ; This is equivalent to 3 allocations

    // Tests
    virtual unsigned            IsConstant() const ;
    virtual unsigned            IsNull() const { return (_value == 0) ; }
    virtual unsigned            IsAccessValue() const ;
    virtual unsigned            IsTrue() const ;
    virtual unsigned            IsSigned() const ;
    virtual void                SetSigned() ;
    virtual unsigned            HasX() const ;
    virtual unsigned            HasUXZW() const ;
    virtual unsigned            IsMetalogical() const ;
    virtual unsigned            IsMetalogicalWithDC() const ;
    virtual unsigned            CheckAgainst(const VhdlConstraint *constraint, const VhdlTreeNode *from, const VhdlDataFlow *df) ; // Subtype check of constraint against value
    virtual unsigned            NumBits() const ; // The number of bits this value contains
    virtual verific_int64       Integer() const ;
    virtual const char         *EnumName() const ; // Return name (not allocated image) if this is an enumeration literal value
    virtual char *              StringImage() const ; // Produce a char* allocated string for an array of VeriValue that is known to be an array of 'character' elements (needed for 'report' and 'assertion' statements).

    virtual char *              Image() const ;
    virtual char *              ImageVerilog() const ;
    virtual unsigned            IsEqual(const VhdlValue *other_val) const ;
    virtual VhdlIdDef *         GetType() const ;        // Works only for constant enum values

    // Access value is a pointer to another value. Accessor functions here :
    virtual VhdlValue *         GetAccessedValue() const   { return _value ; }
    virtual void                SetAccessedValue(VhdlValue * value) { _value = value ; VhdlNode::SetInnerAccessValue(value) ; } // Access values only : swap pointer of an access value to another value. DO not delete the old _value. (VIPER #4464, 7292): remember the value for later cleanup

    virtual VhdlValue *         GetInnerValue()      { return _value ; }

    virtual VhdlConstraint *    Constraint()         { return _constraint ; }
    virtual VhdlConstraint *    GetRange() const     { return _value ? _value->GetRange() : 0 ; }

    // Operators
    virtual VhdlValue *         UnaryOper(unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         BinaryOper(VhdlValue *right_val, unsigned oper_type, const VhdlTreeNode *from) ;
    virtual VhdlValue *         SelectOn1(VhdlNonconstBit * /*select*/, VhdlValue * /*one_val*/, VhdlValue * /*initial_value*/, const VhdlTreeNode * /*from*/) { return 0 ; } // rtl elab utility
    virtual VhdlValue *         PreserveUserNetBuffer(VhdlValue * /*init_val*/, const VhdlTreeNode * /*from*/)  { return 0 ; } // rtl elab utility
    virtual VhdlValue *         PreserveAssignmentBuffer(unsigned /*buffer_constants_too*/, const VhdlTreeNode * /*from*/)  { return 0 ; } // rtl elab utility

#ifdef DB_INFER_WIDE_OPERATORS
    virtual unsigned            IsBitVector() const ; // returns 1 if each element of this value is of size '1', returns 0 otherwise
#endif // DB_INFER_WIDE_OPERATORS

    // Constant value tests
    virtual unsigned            LT(const VhdlValue *v) const ;

public:
    virtual verific_int64       Position() const ;

    virtual unsigned            IsBit() const ;
    virtual Net *               GetBit() const { return 0 ; }
    virtual Net *               GetBit(unsigned /*pos*/) const { return  0 ; }

    virtual Array *             GetValues() const    { return _value ? _value->GetValues() : 0 ; }
    virtual unsigned            NumElements() const  { return _value ? _value->NumElements() : 0 ; }
    virtual void                PrependElement(VhdlValue *val) { if (_value) _value->PrependElement(val) ; }
    virtual void                AppendElement(VhdlValue *val) { if (_value) _value->AppendElement(val) ; }

    virtual VhdlNonconst *      ToNonconst() { return 0 ; }
    virtual VhdlNonconstBit *   ToNonconstBit() { return 0 ; }
    virtual VhdlCompositeValue *ToComposite(unsigned /*size*/) { return 0 ; }
    virtual VhdlValue *         ToConst(VhdlIdDef * /*type*/) { return this ; }
    virtual VhdlValue *         ToType(VhdlIdDef * /*type*/, VhdlTreeNode * /*from*/) { return this ; }

    virtual void                CollectDCs(Set& /*s*/, unsigned & /*runner*/) { }

    virtual void                NetAssign(VhdlValue *val, VhdlIdDef * /*res_func*/, const VhdlTreeNode * /*from*/) { delete val ; return ; }

    virtual VhdlValue *         ValueAt(unsigned pos) const ;

    virtual VhdlValue *         TakeValueAt(unsigned pos) const ;            // Take memory control over the element (it will be removed from the array it was in)

    virtual void                SetValueAt(unsigned pos, VhdlValue *elem) ;  // Set new element value and overwrite old one
    virtual void                RemoveValueAt(unsigned pos) ;                // Actually remove the element at position 'pos' from the composite value
    virtual void                PopulateValues(Array* values) const ;
    virtual unsigned            RepresentCount() const ;

    // Viper #7288: range over composite value for irregular array constraint
    virtual void                ConstraintBy(VhdlConstraint *range) ;

protected:
    virtual VhdlValue *         ReverseBinaryOper_Access(VhdlAccessValue *left_val, unsigned oper_type, const VhdlTreeNode *from) ;

public:
    virtual VhdlExpression *    CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other ) ; // Convert 'this' to expression, works only for constant values
    virtual unsigned            HasAccessValue() const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Create verilog expression forom vhdl value: during elaboration
    virtual VeriExpression      *ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file) ;
#endif

protected:
    VhdlConstraint  *_constraint ; // Viper 3771/3764 : The constraint (owned) of _value
    VhdlValue       *_value ;    // The value (not owned) access type object points to
    unsigned        _is_recursion:1 ; // Internal use, Mark to prevent recursive loop
} ; // class VhdlAccessValue

/* -------------------------------------------------------------- */

/**********************************************/
/* Special-case values */
/**********************************************/

// A value that represents a dual-port RAM contents during elaboration.
// Contains 9 fields.
//  Index 0 is the read_data (RAM output)
//  Index 1->4 are 'initial' values used for start of elaboration (read_address,
//  Index 5->8 are 'target' (RAM input) values, to which we will assign.
class VFC_DLL_PORT VhdlDualPortValue : public VhdlCompositeValue
{
public:
    explicit VhdlDualPortValue(Array *values);
    virtual ~VhdlDualPortValue();

private:
    // Prevent compiler from defining the following
    VhdlDualPortValue() ;                                     // Purposely leave unimplemented
    VhdlDualPortValue(const VhdlDualPortValue &) ;            // Purposely leave unimplemented
    VhdlDualPortValue& operator=(const VhdlDualPortValue &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *          Copy() const ;

    virtual unsigned             IsDualPortValue() const ;

    // Routines that behave (slightly) different for DualPortValue than for normal composite :
    virtual void                 NetAssign(VhdlValue *val, VhdlIdDef *res_func, const VhdlTreeNode *from) ; // Wire nets of this value into new value nets
    virtual VhdlCompositeValue * SelectOn0(VhdlNonconstBit *select, VhdlCompositeValue *zero_val, VhdlValue *initial_value, const VhdlTreeNode *from) ;
} ; // class VhdlDualPortValue

/* -------------------------------------------------------------- */

/*
  A value that represents 'event
*/
class VFC_DLL_PORT VhdlEvent : public VhdlNonconstBit
{
public:
    VhdlEvent(Net *net, unsigned is_event) ;
    virtual ~VhdlEvent() ;

private:
    // Prevent compiler from defining the following
    VhdlEvent() ;                             // Purposely leave unimplemented
    VhdlEvent(const VhdlEvent &) ;            // Purposely leave unimplemented
    VhdlEvent& operator=(const VhdlEvent &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *       Copy() const ;

    virtual unsigned          IsEvent() const ;
    virtual unsigned          IsStable() const ;

    virtual VhdlNonconstBit * Invert(const VhdlTreeNode *from) ;
    virtual void              SetEnableNet(Net *en) { _enable = en ; } // VIPER 2146 : Set clock enable net
    virtual Net *             GetEnableNet() const  { return _enable ; } // VIPER 2146 : Get clock enable net
protected:
    unsigned _is_event:1 ; // 0 if its 'stable
    Net *_enable ; // Enable net for this clock edge edge : add for #2146
} ; // class VhdlEvent

/* -------------------------------------------------------------- */

/*
   A value that represents an up or down edge
*/
class VFC_DLL_PORT VhdlEdge : public VhdlNonconstBit
{
public:
    explicit VhdlEdge(Net *net) ;
    virtual ~VhdlEdge() ;

private:
    // Prevent compiler from defining the following
    VhdlEdge() ;                            // Purposely leave unimplemented
    VhdlEdge(const VhdlEdge &) ;            // Purposely leave unimplemented
    VhdlEdge& operator=(const VhdlEdge &) ; // Purposely leave unimplemented

public:
    virtual VhdlValue *     Copy() const ;

    virtual unsigned        IsEdge() const ;
    virtual void            SetEnableNet(Net *en) { _enable = en ; } // VIPER 2146 : Set clock enable net
    virtual Net *           GetEnableNet() const  { return _enable ; } // VIPER 2146 : Get clock enable net
protected:
    Net *_enable ; // Enable net for this clock edge edge : add for #2146
} ; // class VhdlEdge

/* -------------------------------------------------------------- */

/**********************************************/
/* Constraints */
/**********************************************/

/*
   VhdlConstraint is a class which represents VHDL object constraints.
*/
class VFC_DLL_PORT VhdlConstraint : public VhdlNode
{
protected: // Abstract class
    VhdlConstraint() ;

public:
    virtual ~VhdlConstraint();

private:
    // Prevent compiler from defining the following
    VhdlConstraint(const VhdlConstraint &) ;            // Purposely leave unimplemented
    VhdlConstraint& operator=(const VhdlConstraint &) ; // Purposely leave unimplemented

public:
    virtual VhdlConstraint *        Copy() const = 0 ; // Return a new'ed copy of this object

    virtual const VhdlConstraint *  GetDesignatedConstraint() const ;           // The inner constraint
    virtual VhdlConstraint *        GetDesignatedConstraint() ;                 // The inner constraint
    virtual const VhdlIdDef *       GetDesignatedType() const ;                 // The inner constraint

    // Get pointer to array index or element constraint.  Element constraint goes down ONE dimension only.
    // So, for multi-dimensional arrays it does not go to the actually declared element constraint.
    virtual VhdlConstraint *        ElementConstraint() const ;                 // Array constraints : Get pointer to element constraint
    virtual VhdlConstraint *        IndexConstraint() const ;                   // Array constraints : Get pointer to index constraint
    virtual VhdlConstraint *        ElementConstraintAt(unsigned pos) const ;   // Composite constraints : element constraint by index position (for arrays ; always same 'ElementConstraint())'
    virtual VhdlConstraint *        ElementConstraintFor(VhdlIdDef *ele_id) const ; // Only relevant for record_constraint

    // Get pointer to range bounds
    virtual VhdlValue *             Left() const ;       // Get the left bound value
    virtual VhdlValue *             Right() const ;      // Get the right bound value
    virtual VhdlValue *             Low() const ;        // Get the low bound value
    virtual VhdlValue *             High() const ;       // Get the high bound value

    virtual unsigned                Dir() const ;        // Get the direction of range (downto, to)
    virtual verific_uint64          Length() const ;     // Get the length of range
    virtual unsigned                NumBitsOfRange() const ; // Calculate the number of bits needed to encode this range or index range of array, or number of elements of record.
    virtual unsigned                IsSignedRange() const ;  // Check if this range or index range contains negative values.
    virtual unsigned                PosOf(VhdlValue *value) const ; // Position of value in the range, counting from the left

    virtual void                    ReverseDirection() ; // switches _left/_right and range direction

    // Make an unconstrained index constraint 'constrained' by number of elements in the range
    virtual void                    ConstraintBy(VhdlValue *value) ;
    // Make an unconstrained index constraint 'constrained' by another constraint
    virtual void                    ConstraintBy(VhdlConstraint *constraint) ;
    // VIPER #7163 : Make 'constrained' when corresponding value is access value
    virtual void                    ConstraintByAccessValue(VhdlValue *value) ;
    virtual void                    ReConstraintByValue(VhdlValue *value) ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                    ConstraintBy(unsigned actual_size, VhdlTreeNode *from) ;
#endif
    // Adjust this (range) constraint by extending its bounds to include 'index'. Needed for aggregate index constraint building by aggregate 'choice'.
    virtual void                    ConstraintByChoice(const VhdlValue *index) ;
    // Push out a allocated string representation of the constraint :
    virtual char *                  Image() const=0 ;

    // Create nonconstant value from a constraint, create ports if needed,  or portinstances if needed
    virtual VhdlValue *             CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *port_id, Instance *inst)=0 ;
    virtual VhdlValue *             CreateInitialValue(const VhdlTreeNode * /*from*/)=0 ; // Create an initial value from this constraint.
    virtual VhdlValue *             CreateXValue()=0 ; // Create an value with all X'es from this constraint (used only for RAM initial values now)
    virtual VhdlValue *             CreateFullInitialValue(const VhdlTreeNode *from) { return CreateInitialValue(from) ; } // old style initial value creation. Overridden (different) only for ArrayConstraints.

    // to return the width needed to represent a value for this
    virtual verific_uint64          GetSizeForConstraint() const ;

    // to return the log(width) needed to represent a value for this
    virtual verific_uint64          GetNumBitsForConstraint() const ;

    // Label the range as 'unconstrained' (for index ranges)
    virtual void                    SetUnconstrained() ;

    // Tests
    virtual unsigned                IsNullRange() const ;                // Is this constraint a NULL range?
    virtual unsigned                HasNullRange() const ;                // Is this constraint has a NULL range?
    virtual unsigned                IsUnconstrained() const ;            // Range is there, but from an unconstrained index constraint
    virtual unsigned                IsBitEncoded() const ;               // Range is implemented with one bit
    virtual unsigned                UserEncoded() const ;                // Range is implemented with user-encoded number of bits
    virtual unsigned                OnehotEncoded() const ;              // Check if this range is one-hot encoded
    virtual unsigned                Contains(VhdlValue *value) const ;   // Test if value is in the range
    virtual unsigned                IsRangeConstraint() const ;
    virtual unsigned                IsArrayConstraint() const ;
    virtual unsigned                IsRecordConstraint() const ;

    virtual unsigned                IsAccessConstraint() const { return 0 ; } // Viper #4394

    virtual unsigned                IsEnumRange() const ;                // Test if this is a constraint bound by enumeration values.
    virtual unsigned                IsRealRange() const ;                // Test if the constraint represents a range of 'real' type

    // Viper #6473: apply the operator oper_type on the two constraints
    // e.g. (0 to 5) plus (3 downto -2) should generate (-2 to 8)
    virtual VhdlConstraint *        MergeConstraints(VhdlConstraint *other_constraint, unsigned oper_type) ;

    virtual VhdlDiscreteRange *     CreateRangeNode(linefile_type /*linefile*/)  { return 0 ; } // Create range node from 'this'
    virtual Array *                 CreateAssocList(linefile_type /*linefile*/, VhdlIdDef * /*type*/)              { return 0 ; } // Create range list from 'this': VIPER #2849: Pass type id for assoc list size

    // VIPER 2636 : Routines to check value constraint against target constraint
    virtual VhdlValue *             ValueOf(unsigned pos) const ; // Value of a particular position in range
    virtual VhdlSubtypeIndication*  CreateSubtype(VhdlIdDef *base_type, VhdlTreeNode *from) ;
    virtual unsigned                CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df) = 0 ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Fill up verilog unconstraint ranges from vhdl constraint
    virtual void                    FillUpRanges(VeriScope* /*scope*/, VeriIdDef* /*id*/, unsigned /*pos*/, const linefile_type /*line_file*/) { }
#endif

} ; // class VhdlConstraint

/* -------------------------------------------------------------- */

/*
  Constraint is a discrete range
*/
class VFC_DLL_PORT VhdlRangeConstraint : public VhdlConstraint
{
public:
    VhdlRangeConstraint(VhdlValue *left, unsigned dir, VhdlValue *right, unsigned is_unconstrained=0);
    virtual ~VhdlRangeConstraint();

private:
    // Prevent compiler from defining the following
    VhdlRangeConstraint() ;                                       // Purposely leave unimplemented
    VhdlRangeConstraint(const VhdlRangeConstraint &) ;            // Purposely leave unimplemented
    VhdlRangeConstraint& operator=(const VhdlRangeConstraint &) ; // Purposely leave unimplemented

public:
    virtual VhdlConstraint *        Copy() const ;

    // Label the (index) range as 'unconstrained'
    virtual void                    SetUnconstrained() ;

    // Make an unconstrained index constraint 'constrained' by number of elements in the range
    virtual void                    ConstraintBy(VhdlValue *value) ;
    // Make an unconstrained index range 'constrained' by another range
    virtual void                    ConstraintBy(VhdlConstraint *constraint) ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                    ConstraintBy(unsigned actual_size, VhdlTreeNode *from) ;
#endif
    // Adjust this (range) constraint by extending its bounds to include 'index'. Needed for aggregate index constraint building by aggregate 'choice'.
    virtual void                    ConstraintByChoice(const VhdlValue *index) ;

    // to return the width needed to represent a value for this
    virtual verific_uint64          GetSizeForConstraint() const ;

    // Tests
    virtual unsigned                IsRangeConstraint() const ;
    virtual unsigned                IsNullRange() const ;
    virtual unsigned                HasNullRange() const ;                // Is this constraint has a NULL range?
    virtual unsigned                IsUnconstrained() const ; // Range is there, but from an unconstrained index constraint
    virtual unsigned                IsBitEncoded() const ;    // Range can be implemented with one bit
    virtual unsigned                UserEncoded() const ;     // Range is implemented with user-encoded number of bits
    virtual unsigned                OnehotEncoded() const ;   // Check if this range is one-hot encoded
    virtual unsigned                IsRealRange() const ;     // Test if the constraint represents a range of 'real' type
    virtual unsigned                IsEnumRange() const ;     // Test if this is a constraint bound by enumeration values.
    virtual unsigned                Contains(VhdlValue *value) const ;

    // Get scalar / index range bounds
    virtual VhdlValue *             Left() const ;
    virtual VhdlValue *             Right() const ;
    virtual VhdlValue *             Low() const ;
    virtual VhdlValue *             High() const ;

    virtual unsigned                Dir() const ; // direction of the range (token VHDL_to or VHDL_downto).
    virtual verific_uint64          Length() const ;
    virtual unsigned                NumBitsOfRange() const ; // Calculate the number of bits needed to encode this range
    virtual unsigned                IsSignedRange() const ;  // Check if this range or index range contains negative values.
    virtual unsigned                PosOf(VhdlValue *value) const ; // Position of value in the range, counting from the left

    virtual void                    ReverseDirection() ; // switches _left/_right and range direction

    // Create nonconstant value from a constraint
    virtual VhdlValue *             CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *port_id, Instance *inst) ;
    virtual VhdlValue *             CreateInitialValue(const VhdlTreeNode *from) ;  // Create an initial value from this constraint.
    virtual VhdlValue *             CreateXValue() ;        // Create an value with all X'es from this constraint (used only for RAM initial values now)

    // Push out a allocated string representation of the constraint :
    virtual char *                  Image() const ;

    // Viper #6473: apply the operator oper_type on the two constraints
    // e.g. (0 to 5) plus (3 downto -2) should generate (-2 to 8)
    virtual VhdlConstraint *        MergeConstraints(VhdlConstraint *other_constraint, unsigned oper_type) ;

    virtual VhdlDiscreteRange *     CreateRangeNode(linefile_type linefile) ; // Create range node from 'this'
    virtual Array *                 CreateAssocList(linefile_type linefile, VhdlIdDef * /*type*/) ; // Create range list from 'this'
    virtual VhdlValue *             ValueOf(unsigned pos) const ; // Value of a particular position in range
    virtual unsigned                CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df) ;

private:
    VhdlValue *_left ;
    VhdlValue *_right ;
    unsigned   _dir:20 ; // Direction VHDL_to or VHDL_downto
    unsigned   _is_unconstrained:1 ;
} ; // class VhdlRangeConstraint

/* -------------------------------------------------------------- */

/*
   VhdlArrayConstraint is a range constraint with an element constraint
*/
class VFC_DLL_PORT VhdlArrayConstraint : public VhdlConstraint
{
public:
    // Use index constraint as a template (don't change it), absorb element_constraint
    VhdlArrayConstraint(VhdlConstraint *index_constraint, VhdlConstraint *element_constraint) ;
    virtual ~VhdlArrayConstraint();

private:
    // Prevent compiler from defining the following
    VhdlArrayConstraint() ;                                       // Purposely leave unimplemented
    VhdlArrayConstraint(const VhdlArrayConstraint &) ;            // Purposely leave unimplemented
    VhdlArrayConstraint& operator=(const VhdlArrayConstraint &) ; // Purposely leave unimplemented

public:
    virtual VhdlConstraint *    Copy() const ;
    virtual VhdlConstraint *    ElementConstraint() const ;
    virtual VhdlConstraint *    IndexConstraint() const ;
    virtual VhdlConstraint *    ElementConstraintAt(unsigned pos) const ; // By index position (for arrays ; always same 'ElementConstraint())'

    // Label the (index) range as 'unconstrained'
    virtual void                SetUnconstrained() ;

    // to return the width needed to represent a value for this
    virtual verific_uint64      GetSizeForConstraint() const ;

    // to return the log(width) needed to represent a value for this
    virtual verific_uint64      GetNumBitsForConstraint() const ;

    // Tests
    virtual unsigned            IsNullRange() const ;
    virtual unsigned            HasNullRange() const ;                // Is this constraint has a NULL range?
    virtual unsigned            IsUnconstrained() const ; // Range is there, but from an unconstrained index constraint
    virtual unsigned            Contains(VhdlValue *value) const ;
    virtual unsigned            IsArrayConstraint() const ;

    // Get scalar / index range bounds
    virtual VhdlValue *         Left() const ;
    virtual VhdlValue *         Right() const ;
    virtual VhdlValue *         Low() const ;
    virtual VhdlValue *         High() const ;

    virtual unsigned            Dir() const ;
    virtual verific_uint64      Length() const ;
    virtual unsigned            NumBitsOfRange() const ; // Calculate the number of bits needed to encode this index range of array.
    virtual unsigned            IsSignedRange() const ;  // Check if this range or index range contains negative values.
    virtual unsigned            PosOf(VhdlValue *value) const ; // Position of value in the range, counting from the left

    virtual void                ReverseDirection() ; // switches _left/_right and range direction

    // Make an unconstrained index constraint 'constrained' by number of elements in the range
    virtual void                ConstraintBy(VhdlValue *value) ;
    virtual void                ReConstraintByValue(VhdlValue *value) ;
    // Make an unconstrained index constraint 'constrained' by another constraint
    virtual void                ConstraintBy(VhdlConstraint *constraint) ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                ConstraintBy(unsigned actual_size, VhdlTreeNode *from) ;
#endif
    // Adjust this (range) constraint by extending its bounds to include 'index'. Needed for aggregate index constraint building by aggregate 'choice'.
    virtual void                ConstraintByChoice(const VhdlValue *index) ;

    // Push out a allocated string representation of the constraint :
    virtual char *              Image() const ;

    // Create nonconstant value from a constraint
    virtual VhdlValue *         CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *port_id, Instance *inst) ;
    virtual VhdlValue *         CreateInitialValue(const VhdlTreeNode *from) ; // Create an initial value from this constraint.
    virtual VhdlValue *         CreateXValue() ; // Create an value with all X'es from this constraint (used only for RAM initial values now)
    virtual VhdlValue *         CreateFullInitialValue(const VhdlTreeNode *from) ; // Create an initial value from this constraint. This creates a fully-filled initial value, not using '_default_elem_val'.

    virtual VhdlDiscreteRange * CreateRangeNode(linefile_type linefile) ; // create range node from 'this'
    virtual Array *             CreateAssocList(linefile_type linefile, VhdlIdDef *type) ; // create range list from 'this'
    virtual VhdlValue *         ValueOf(unsigned pos) const ; // Value of a particular position in range
    virtual VhdlSubtypeIndication* CreateSubtype(VhdlIdDef *base_type, VhdlTreeNode *from) ;
    virtual unsigned            CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Fill up verilog unconstraint ranges from vhdl constraint
    virtual void                FillUpRanges(VeriScope *scope, VeriIdDef *id, unsigned pos, const linefile_type line_file) ;
#endif

private:
    VhdlConstraint *_index_constraint ;
    VhdlConstraint *_element_constraint ;
} ; // class VhdlArrayConstraint

/* -------------------------------------------------------------- */

/*
   VhdlRecordConstraint is an array of constraints
*/
class VFC_DLL_PORT VhdlRecordConstraint : public VhdlConstraint
{
public:
    VhdlRecordConstraint(Array *element_constraints, Array *element_ids) ;
    virtual ~VhdlRecordConstraint();

private:
    // Prevent compiler from defining the following
    VhdlRecordConstraint() ;                                        // Purposely leave unimplemented
    VhdlRecordConstraint(const VhdlRecordConstraint &) ;            // Purposely leave unimplemented
    VhdlRecordConstraint& operator=(const VhdlRecordConstraint &) ; // Purposely leave unimplemented

public:
    virtual VhdlConstraint *    Copy() const ;

    virtual unsigned            Contains(VhdlValue *value) const ;  // Overload 'position' value
    virtual verific_uint64      Length() const ;                    // Overload to represent the number of elements in the record
    virtual unsigned            NumBitsOfRange() const ;            // Calculate the number of bits needed to encode the number of elements of record.
    virtual unsigned            PosOf(VhdlValue *value) const ;     // Overloaded to represent position of value

    virtual unsigned            HasNullRange() const ;                // Is this constraint has a NULL range?

    // Push out a allocated string representation of the constraint :
    virtual char *              Image() const ;

    virtual VhdlValue *         CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *port_id, Instance *inst) ;
    virtual VhdlValue *         CreateInitialValue(const VhdlTreeNode *from) ;  // Create an initial value from this constraint.
    virtual VhdlValue *         CreateFullInitialValue(const VhdlTreeNode *from) ; // old style initial value creation. Overridden (different) only for ArrayConstraints.
    virtual VhdlValue *         CreateXValue() ;        // Create an value with all X'es from this constraint (used only for RAM initial values now)
    // Make an unconstrained  element constraint 'constrained' by elements in the composite value
    virtual void                ConstraintBy(VhdlValue *value) ;
    virtual void                ConstraintBy(VhdlConstraint *constraint) ;
    virtual void                ReConstraintByValue(VhdlValue *value) ;

    // to return the width needed to represent a value for this
    virtual verific_uint64      GetSizeForConstraint() const ;

    virtual unsigned            IsRecordConstraint() const ;
    virtual unsigned            IsUnconstrained() const ; // Range is there, but from an unconstrained index constraint
    virtual VhdlConstraint *    ElementConstraintAt(unsigned pos) const ;
    virtual VhdlConstraint *    ElementConstraintFor(VhdlIdDef *ele_id) const ;
    // VIPER #7163 : Make 'constrained' when corresponding value is access value
    virtual void                ConstraintByAccessValue(VhdlValue *value) ;

    virtual VhdlDiscreteRange * CreateRangeNode(linefile_type linefile) ; // Create range node from 'this'.
    virtual Array *             CreateAssocList(linefile_type linefile, VhdlIdDef * /*type*/) ; // Create range list from 'this'
    virtual unsigned            CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df) ;

    // Accessor methods
    Array *                     GetElementConstraints() const    { return _element_constraints ; }
    Array *                     GetElementIds() const            { return _element_ids ; }

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Fill up verilog unconstraint ranges from vhdl constraint
    virtual void                FillUpRanges(VeriScope *scope, VeriIdDef *id, unsigned pos, const linefile_type line_file) ;
#endif

private:
    Array   *_element_constraints ; // Array of VhdlConstraint*
    Array   *_element_ids ;         // Array of (record field) VhdlIdDef*. This one only needed to get element field names into a record.
} ; // class VhdlRecordConstraint

/* -------------------------------------------------------------- */
/*
  VhdlAccessConstraint is a class which represents VHDL object constraints for incomplete forward declaration types
*/
class VFC_DLL_PORT VhdlAccessConstraint : public VhdlConstraint
{
public:
    explicit VhdlAccessConstraint(VhdlIdDef *designated_type) ;
    explicit VhdlAccessConstraint(VhdlConstraint *designated_constraint) ;
    virtual ~VhdlAccessConstraint();

private:
    // Prevent compiler from defining the following
    VhdlAccessConstraint(const VhdlAccessConstraint&) ;            // Purposely leave unimplemented
    VhdlAccessConstraint& operator=(const VhdlAccessConstraint&) ; // Purposely leave unimplemented

public:
    virtual VhdlConstraint *        Copy() const ; // Return a new'ed copy of this object

    virtual const VhdlConstraint *  GetDesignatedConstraint() const ;           // The inner constraint
    virtual VhdlConstraint *        GetDesignatedConstraint() ;                 // The inner constraint
    virtual const VhdlIdDef *       GetDesignatedType() const ;                 // The inner constraint

    // Get pointer to array index or element constraint.  Element constraint goes down ONE dimension only.
    // So, for multi-dimensional arrays it does not go to the actually declared element constraint.
    virtual VhdlConstraint *        ElementConstraint() const ;                 // Array constraints : Get pointer to element constraint
    virtual VhdlConstraint *        IndexConstraint() const ;                   // Array constraints : Get pointer to index constraint
    virtual VhdlConstraint *        ElementConstraintAt(unsigned pos) const ;   // Composite constraints : element constraint by index position (for arrays ; always same 'ElementConstraint())'
    virtual VhdlConstraint *        ElementConstraintFor(VhdlIdDef *ele_id) const ; // Only relevant for record_constraint

    // Get pointer to range bounds
    virtual VhdlValue *             Left() const ;       // Get the left bound value
    virtual VhdlValue *             Right() const ;      // Get the right bound value
    virtual VhdlValue *             Low() const ;        // Get the low bound value
    virtual VhdlValue *             High() const ;       // Get the high bound value

    virtual unsigned                Dir() const ;        // Get the direction of range (downto, to)
    virtual verific_uint64          Length() const ;     // Get the length of range
    virtual unsigned                NumBitsOfRange() const ; // Calculate the number of bits needed to encode this range or index range of array, or number of elements of record.
    virtual unsigned                IsSignedRange() const ;  // Check if this range or index range contains negative values.
    virtual unsigned                PosOf(VhdlValue *value) const ; // Position of value in the range, counting from the left

    virtual void                    ReverseDirection() ; // switches _left/_right and range direction

    // Push out a allocated string representation of the constraint :
    virtual char *                  Image() const ;

    // Create nonconstant value from a constraint, create ports if needed,  or portinstances if needed
    virtual VhdlValue *             CreateValue(const char *name, unsigned prepend_name_path, VhdlIdDef *port_id, Instance *inst) ;
    virtual VhdlValue *             CreateInitialValue(const VhdlTreeNode *from) ; // Create an initial value from this constraint.
    virtual VhdlValue *             CreateXValue() ; // Create an value with all X'es from this constraint (used only for RAM initial values now)
    virtual VhdlValue *             CreateFullInitialValue(const VhdlTreeNode *from) ; // old style initial value creation. Overridden (different) only for ArrayConstraints.

    // to return the width needed to represent a value for this
    virtual verific_uint64          GetSizeForConstraint() const ;

    // to return the log(width) needed to represent a value for this
    virtual verific_uint64          GetNumBitsForConstraint() const ;

    // Label the range as 'unconstrained' (for index ranges)
    virtual void                    SetUnconstrained() ;

    virtual void                    ConstraintByAccessValue(VhdlValue *value) ;
    virtual void                    ConstraintBy(VhdlValue *value) ;
    virtual void                    ConstraintBy(VhdlConstraint *constraint) ;
    virtual void                    ReConstraintByValue(VhdlValue *value) ;

    // Tests
    virtual unsigned                IsNullRange() const ;                // Is this constraint a NULL range?
    virtual unsigned                HasNullRange() const ;               // Is this constraint has a NULL range?
    virtual unsigned                IsUnconstrained() const ;            // Range is there, but from an unconstrained index constraint
    virtual unsigned                IsBitEncoded() const ;               // Range is implemented with one bit
    virtual unsigned                UserEncoded() const ;                // Range is implemented with user-encoded number of bits
    virtual unsigned                OnehotEncoded() const ;              // Check if this range is one-hot encoded
    virtual unsigned                Contains(VhdlValue *value) const ;   // Test if value is in the range
    virtual unsigned                IsRangeConstraint() const ;
    virtual unsigned                IsArrayConstraint() const ;
    virtual unsigned                IsRecordConstraint() const ;

    virtual unsigned                IsAccessConstraint() const ; // Viper #4394

    virtual unsigned                IsEnumRange() const ;                // Test if this is a constraint bound by enumeration values.
    virtual unsigned                IsRealRange() const ;                // Test if the constraint represents a range of 'real' type

    // Viper #6473: apply the operator oper_type on the two constraints
    // e.g. (0 to 5) plus (3 downto -2) should generate (-2 to 8)
    virtual VhdlConstraint *        MergeConstraints(VhdlConstraint *other_constraint, unsigned oper_type) ;

    virtual VhdlDiscreteRange *     CreateRangeNode(linefile_type linefile) ; // Create range node from 'this'
    virtual Array *                 CreateAssocList(linefile_type linefile, VhdlIdDef *type) ; // Create range list from 'this': VIPER #2849: Pass type id for assoc list size

    // VIPER 2636 : Routines to check value constraint against target constraint
    virtual VhdlValue *             ValueOf(unsigned pos) const ; // Value of a particular position in range
    virtual VhdlSubtypeIndication*  CreateSubtype(VhdlIdDef *base_type, VhdlTreeNode *from) ;
    virtual unsigned                CheckAgainst(VhdlConstraint *other, VhdlTreeNode *from, const VhdlDataFlow *df) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Fill up verilog unconstraint ranges from vhdl constraint
    virtual void                    FillUpRanges(VeriScope* scope, VeriIdDef* id, unsigned pos, const linefile_type line_file) ;
#endif

private:
    VhdlIdDef      *_designated_type ; // not owned : forward declaration
    VhdlConstraint *_designated_constraint ; // owned
    unsigned        _is_recursion:1 ; // Internal use, Mark to prevent recursive loop
} ; // class VhdlAccessConstraint

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_VALUE_H_

