/*
 *
 * [ File Version : 1.100 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlMisc.h"

#include "Hash.h"
#include "Array.h"
#include "Strings.h"
#include "Message.h"

#include "vhdl_tokens.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlScope.h"
#include "VhdlIdDef.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlSignature::VhdlSignature(Array *type_mark_list, VhdlName *return_type)
  : VhdlTreeNode(),
    _type_mark_list(type_mark_list),
    _return_type(return_type)
{ }
VhdlSignature::~VhdlSignature()
{
    unsigned i ;
    VhdlName *elem ;
    FOREACH_ARRAY_ITEM(_type_mark_list,i,elem) delete elem ;
    delete _type_mark_list ;
    delete _return_type ;
}

VhdlFileOpenInfo::VhdlFileOpenInfo(VhdlExpression *file_open, unsigned open_mode, VhdlExpression *file_logical_name)
  : VhdlTreeNode(),
    _file_open(file_open),
    _open_mode(open_mode),
    _file_logical_name(file_logical_name)
{
    // _open_mode is set for VHDL'87. Nothing to check there.
    // _file_open is set for VHDL'93.

    // Could do some vhdl'87/93 mode checks to see if correct dialect is used...
    // if (IsVhdl87() && _file_open) {
    //    Error("illegal use of 'file_open' expression in VHDL'87 mode") ;
    // }
    // if (!IsVhdl87() && _open_mode) {
    //    Error("illegal use of file 'mode' in VHDL'93 mode") ;
    // }

    // Type infer file_open expression if there.
    if (_file_open) {
        // infer hard with 'file_open_kind' type..
        (void) _file_open->TypeInfer(StdType("file_open_kind"), 0, VHDL_read,0) ;
    }

    // Type infer the file_logical_name if there :
    if (_file_logical_name) {
        (void) _file_logical_name->TypeInfer(StdType("string"), 0, VHDL_read,0) ;
    }
}
VhdlFileOpenInfo::~VhdlFileOpenInfo()
{
    delete _file_open ;
    delete _file_logical_name ;
}

VhdlBindingIndication::VhdlBindingIndication(VhdlName *entity_aspect, Array *generic_map_aspect, Array *port_map_aspect)
  : VhdlTreeNode(),
    _entity_aspect(entity_aspect),
    _generic_map_aspect(generic_map_aspect),
    _port_map_aspect(port_map_aspect),
    _unit(0)
{
    if (!_entity_aspect || _entity_aspect->IsOpen()) {
        // In some cases, binding without entity aspect is allowed (LRM 5.2).
        // FIX ME : see if we can do checks already.
        return ;
    }

    // First, get the entity aspect. This can ONLY be an entity aspect. FIX ME : Restrict yacc.
    // We need the 'entity' from the entity aspect :
    _unit = _entity_aspect->EntityAspect() ;
    if (!_unit) return ; // Something is wrong

    // Check :
    if (!_unit->IsEntity() && !_unit->IsConfiguration()) {
        Error("entity aspect %s does not denote a entity or configuration",_unit->Name()) ;
        _unit = 0 ;
        return ;
    }

    // Cannot infer binding indication yet. Need the component it will
    // bind to. That is known in the caller of this one :
    // a ComponentConfiguration  and a ConfigurationSpec.
    // The constructors for these will call BindingIndication::Analyze(),
    // which will do the analysis work for generic/port map aspects.
}
VhdlBindingIndication::~VhdlBindingIndication()
{
    delete _entity_aspect ;
    unsigned i ;
    VhdlDiscreteRange *elem ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,elem) delete elem ;
    delete _generic_map_aspect ;
    FOREACH_ARRAY_ITEM(_port_map_aspect,i,elem) delete elem ;
    delete _port_map_aspect ;
}

VhdlIterScheme::VhdlIterScheme() : VhdlTreeNode() { }
VhdlIterScheme::~VhdlIterScheme() { }

VhdlWhileScheme::VhdlWhileScheme(VhdlExpression *condition)
  : VhdlIterScheme(),
    _condition(condition),
    _exit_label(new VhdlLabelId(Strings::save(" exit_label"))),
    _next_label(new VhdlLabelId(Strings::save(" next_label")))
{
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    // Declare an exit and a next label
    (void) _present_scope->Declare(_exit_label) ;
    (void) _present_scope->Declare(_next_label) ;

    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
}
VhdlWhileScheme::~VhdlWhileScheme()
{
    delete _condition ;
    // Delete the on-the-fly created label identifiers :
    delete _exit_label ;
    delete _next_label ;
}

VhdlForScheme::VhdlForScheme(VhdlIdRef *id, VhdlDiscreteRange *range, unsigned is_generate)
  : VhdlIterScheme(),
    _id(id),
    _range(range),
    _iter_id(0),
    _exit_label(new VhdlLabelId(Strings::save(" exit_label"))),
    _next_label(new VhdlLabelId(Strings::save(" next_label")))
{
    // Declare an exit and a next label
    (void) _present_scope->Declare(_exit_label) ;
    (void) _present_scope->Declare(_next_label) ;

    VhdlIdDef *range_type = _range->TypeInfer(0,0,VHDL_range,0) ;

    if (range_type && !range_type->IsDiscreteType()) {
        Error("type error near discrete range ; type %s is not a discrete type", range_type->Name()) ;
    }

    // LRM 3.2.1.1 if discrete range both sides are universal_integer, convert to INTEGER
    // Also do this if there is no result type. This will reduce errors further down
    if (!range_type || range_type->IsUniversalInteger()) {
        range_type = StdType("integer") ;
    }

    // Declare the identifier as a constant
    _iter_id = new VhdlConstantId(Strings::save(id->Name())) ;
    _iter_id->DeclareConstant(range_type, 0, is_generate ? 0 : 1, 1, 1, 1) ; // added is_generate to indicate that this id is globally static, even though it is a loop_index Done for Viper #4517
    (void) _present_scope->Declare(_iter_id) ;
}
VhdlForScheme::~VhdlForScheme()
{
    delete _id ;
    delete _range ;
    delete _iter_id ; // Identifiers are owned by the tree
    // Delete the on-the-fly created label identifiers :
    delete _exit_label ;
    delete _next_label ;
}

VhdlIdDef* VhdlForScheme::CheckIterType(VhdlDiscreteRange *generate_spec, VhdlTreeNode const *from) const
{
    if (!generate_spec || !_iter_id) return 0 ; // don't know type yet

    VhdlIdDef *iter_type = _iter_id->Type() ;
    if (!iter_type) return 0 ;

    VhdlIdDef *type = generate_spec->TypeInfer(0,0, (generate_spec && generate_spec->IsRange()) ? VHDL_range : VHDL_READ,0) ;

    // Viper #3488 : Label may not have an indexed name
    if (!generate_spec->IsGloballyStatic() && from) from->Error("index/slice name with label prefix must have static index/slice") ;

    if (type && (!(type->TypeMatch(iter_type) ||
          (iter_type->IsIntegerType() && type->IsIntegerType()) ||
          (iter_type->IsRealType() && type->IsRealType())))) {
        if (from) from->Error("type error near %s ; expected type %s", type->Name(), iter_type->Name()) ;
    }
    return 0 ; // cannot have alternative label
}

VhdlIfScheme::VhdlIfScheme(VhdlExpression *condition)
  : VhdlIterScheme(),
    _condition(condition),
    _alternative_label(0),
    _alternative_label_id(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _alternative_closing_label(0)
#endif
{
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;

    // VIPER #7432: VHDL 2008 LRM section 11.8 says condition of if-generate will
    // be static expression
    if (_condition && !_condition->IsExternalName() && !_condition->IsLocallyStatic() && !_condition->IsGloballyStatic()) {
        _condition->Warning("condition in if generate must be static") ;
    }
}
VhdlIfScheme::VhdlIfScheme(VhdlExpression *condition, VhdlIdRef *alternative_label)
  : VhdlIterScheme(),
    _condition(condition),
    _alternative_label(alternative_label),
    _alternative_label_id(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _alternative_closing_label(0)
#endif
{
    if (_alternative_label) {
        _alternative_label_id = new VhdlLabelId(Strings::save(_alternative_label->Name())) ;
        if (_present_scope) (void) _present_scope->Declare(_alternative_label_id) ;
    }
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
    // VIPER #7432: VHDL 2008 LRM section 11.8 says condition of if-generate will
    // be static expression
    if (_condition && !_condition->IsExternalName() && !_condition->IsLocallyStatic() && !_condition->IsGloballyStatic()) {
        _condition->Warning("condition in if generate must be static") ;
    }
}
VhdlIfScheme::~VhdlIfScheme()
{
    delete _condition ;
    delete _alternative_label ;
    delete _alternative_label_id ;
    _alternative_label_id = 0 ;
}
VhdlIdDef *VhdlIfScheme::CheckIterType(VhdlDiscreteRange *generate_spec, VhdlTreeNode const *from) const
{
    if (!generate_spec) return 0 ;

    // 'generate_spec' i.e. generate specification can only be alternative label

    VhdlIdDef *alt_label = generate_spec->EntityAspect() ;

    if (!alt_label || !alt_label->IsLabel()) {
        if (from) from->Error("only alternative label can be used with if/case generate statement") ;
    }
    // VIPER #7639: Check and return alternative label
    if (alt_label && _alternative_label_id && (alt_label == _alternative_label_id)) return _alternative_label_id ;
    return 0 ;
}

VhdlElsifElseScheme::VhdlElsifElseScheme(VhdlExpression *condition, VhdlIdRef *alternative_label)
  : VhdlIfScheme(condition, alternative_label)
{}
VhdlElsifElseScheme::~VhdlElsifElseScheme() {}

VhdlCaseItemScheme::VhdlCaseItemScheme(Array *choices, VhdlIdRef *alternative_label)
  : VhdlIterScheme(),
    _choices(choices),
    _alternative_label(alternative_label),
    _alternative_label_id(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _alternative_closing_label(0)
#endif
{
    if (_alternative_label) {
        _alternative_label_id = new VhdlLabelId(Strings::save(_alternative_label->Name())) ;
        if (_present_scope) (void) _present_scope->Declare(_alternative_label_id) ;
    }
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice && choice->IsOthers() && _choices->Size() > 1) {
            choice->Error("OTHERS should be the only choice in alternative") ;
        }
        // VIPER #7432: VHDL 2008 LRM section 11.8 says expression of case generate will
        // be globally static
        if (choice && !choice->IsExternalName() && !choice->IsGloballyStatic()) {
            choice->Warning("expression in case generate must be globally static") ;
        }
    }
}
VhdlCaseItemScheme::~VhdlCaseItemScheme()
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) delete choice ;
    delete _choices ;
    delete _alternative_label ;
    delete _alternative_label_id ;
    _alternative_label_id = 0 ;
}
VhdlIdDef *VhdlCaseItemScheme::CheckIterType(VhdlDiscreteRange *generate_spec, VhdlTreeNode const *from) const
{
    if (!generate_spec) return 0 ;

    // 'generate_spec' i.e. generate specification can only be alternative label

    VhdlIdDef *alt_label = generate_spec->EntityAspect() ;

    if (!alt_label || !alt_label->IsLabel()) {
        if (from) from->Error("only alternative label can be used with if/case generate statement") ;
    }
    // VIPER #7639: Check and return alternative label
    if (_alternative_label_id && alt_label && (alt_label == _alternative_label_id)) return _alternative_label_id ;
    return 0 ;
}
VhdlOptions::VhdlOptions(unsigned guarded, VhdlDelayMechanism *delay_mechanism)
  : VhdlTreeNode(),
    _guarded(guarded),
    _delay_mechanism(delay_mechanism)
{ }
VhdlOptions::~VhdlOptions()
{
    delete _delay_mechanism ;
}

unsigned VhdlOptions::IsGuarded() const { return _guarded ; }

VhdlDelayMechanism::VhdlDelayMechanism() : VhdlTreeNode() { }
VhdlDelayMechanism::~VhdlDelayMechanism() { }

VhdlTransport::VhdlTransport() : VhdlDelayMechanism() { }
VhdlTransport::~VhdlTransport() { }

VhdlInertialDelay::VhdlInertialDelay(VhdlExpression *reject_expression, unsigned inertial)
  : VhdlDelayMechanism(),
    _reject_expression(reject_expression),
    _inertial(inertial)
{
    if (_reject_expression) (void) _reject_expression->TypeInfer(StdType("time"),0,VHDL_READ,0) ;
}
VhdlInertialDelay::~VhdlInertialDelay()
{
    delete _reject_expression ;
}

VhdlConditionalWaveform::VhdlConditionalWaveform(Array *waveform, VhdlExpression *when_condition)
  : VhdlTreeNode(),
    _waveform(waveform),
    _when_condition(when_condition)
{
    // Check if there is an 'unaffected' in a multiple element waveform
    if (_waveform && _waveform->Size()>1) {
        unsigned i ;
        VhdlExpression *expr ;
        FOREACH_ARRAY_ITEM(_waveform, i, expr) {
            if (expr && expr->IsUnaffected()) {
                Error("'unaffected' should be the only waveform element") ;
            }
        }
    }
    if (_when_condition) _when_condition = _when_condition->GetConditionalExpression() ;
    if (_when_condition) (void) _when_condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
}
VhdlConditionalWaveform::~VhdlConditionalWaveform()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) delete elem ;
    delete _waveform ;
    delete _when_condition ;
}

VhdlIdDef *VhdlConditionalWaveform::TypeInfer(VhdlIdDef *wave_type)
{
    VhdlExpression *elem ;
    unsigned i ;
    VhdlIdDef *result_type = 0 ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) {
        if (!elem) continue ;
        // VIPER #3651 : Null waveform in concurrent signal assignment
        if (elem->IsNull()) {
            elem->Error("cannot use null waveform in concurrent signal assignment") ;
            result_type = wave_type ;
            continue ;
        }
        result_type = elem->TypeInfer(wave_type,0,VHDL_READ,0) ;
    }
    return result_type ;
}
#if 0
unsigned VhdlConditionalWaveform::IsNull() const
{
    VhdlExpression *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) {
        if (elem && elem->IsNull()) return 1 ; // Null waveform element
    }
    return 0 ; // No waveform element is null
}
#endif

VhdlSelectedWaveform::VhdlSelectedWaveform(Array *waveform, Array *when_choices)
  : VhdlTreeNode(),
    _waveform(waveform),
    _when_choices(when_choices)
{
    // Check if there is an 'unaffected' in a multiple element waveform
    if (_waveform && _waveform->Size()>1) {
        unsigned i ;
        VhdlExpression *expr ;
        FOREACH_ARRAY_ITEM(_waveform, i, expr) {
            if (expr && expr->IsUnaffected()) {
                Error("'unaffected' should be the only waveform element") ;
            }
        }
    }

    // LRM 9.5.2 : Test that IF there is an others clause that it is the only choice
    // (so if there are multiple choices, error out if there is an others clause in there
    if (_when_choices && _when_choices->Size()>1) {
        unsigned i ;
        VhdlDiscreteRange *choice ;
        FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
            if (choice && choice->IsOthers()) {
                choice->Error("OTHERS should be the only choice in alternative") ;
            }
        }
    }
}

VhdlSelectedExpression::VhdlSelectedExpression(VhdlExpression *expr, Array *when_choices)
  : VhdlTreeNode(),
    _expr(expr),
    _when_choices(when_choices)
{
    // LRM 9.5.2 : Test that IF there is an others clause that it is the only choice
    // (so if there are multiple choices, error out if there is an others clause in there
    if (_when_choices && _when_choices->Size()>1) {
        unsigned i ;
        VhdlDiscreteRange *choice ;
        FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
            if (choice && choice->IsOthers()) {
                choice->Error("OTHERS should be the only choice in alternative") ;
            }
        }
    }
}
unsigned VhdlSelectedWaveform::IsOthers() const
{
    // We already tested that the others clause is the only one if its there.
    // So just check one choice now :
    if (!_when_choices || !_when_choices->Size()) return 0 ;
    VhdlDiscreteRange *choice = (VhdlDiscreteRange*)_when_choices->GetLast() ;
    if (!choice) return 0 ;
    return choice->IsOthers() ;
}
unsigned VhdlSelectedExpression::IsOthers() const
{
    // We already tested that the others clause is the only one if its there.
    // So just check one choice now :
    if (!_when_choices || !_when_choices->Size()) return 0 ;
    VhdlDiscreteRange *choice = (VhdlDiscreteRange*)_when_choices->GetLast() ;
    if (!choice) return 0 ;
    return choice->IsOthers() ;
}

VhdlConditionalExpression::VhdlConditionalExpression(VhdlExpression *expr, VhdlExpression *condition)
  : VhdlTreeNode(),
    _expr(expr),
    _condition(condition)
{
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
}

VhdlConditionalExpression::~VhdlConditionalExpression()
{
    delete _expr ;
    delete _condition ;
}

VhdlIdDef *VhdlConditionalExpression::TypeInfer(VhdlIdDef *expr_type)
{
    VhdlIdDef *result_type = _expr ? _expr->TypeInfer(expr_type,0,VHDL_READ,0) : 0 ;
    return result_type ;
}
#if 0
unsigned VhdlSelectedWaveform::IsNull() const
{
    VhdlExpression *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) {
        if (elem && elem->IsNull()) return 1 ; // Null waveform element
    }
    return 0 ; // No waveform element is null
}
#endif

VhdlSelectedWaveform::~VhdlSelectedWaveform()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) delete elem ;
    delete _waveform ;
    FOREACH_ARRAY_ITEM(_when_choices,i,elem) delete elem ;
    delete _when_choices ;
}
VhdlSelectedExpression::~VhdlSelectedExpression()
{
    unsigned i ;
    delete _expr ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_when_choices,i,elem) delete elem ;
    delete _when_choices ;
}
VhdlIdDef *VhdlSelectedWaveform::TypeInfer(VhdlIdDef *wave_type, VhdlIdDef *cond_type)
{
    VhdlExpression *elem ;
    unsigned i ;
    VhdlIdDef *result_type = 0 ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) {
        if (!elem) continue ;
        // VIPER #3651 : Null waveform in concurrent signal assignment
        if (elem->IsNull()) {
            elem->Error("cannot use null waveform in concurrent signal assignment") ;
            result_type = wave_type ;
            continue ;
        }
        result_type = elem->TypeInfer(wave_type,0,VHDL_READ,0) ;
    }
    FOREACH_ARRAY_ITEM(_when_choices,i,elem) {
        if (!elem) continue ;
        (void) elem->TypeInfer(cond_type,0, (elem->IsRange()) ? VHDL_range : VHDL_READ,0) ;
    }
    return result_type ;
}
VhdlIdDef *VhdlSelectedExpression::TypeInfer(VhdlIdDef *expr_type, VhdlIdDef *cond_type)
{
    VhdlExpression *elem ;
    unsigned i ;
    VhdlIdDef *result_type = 0 ;
    result_type = _expr ? _expr->TypeInfer(expr_type,0,VHDL_READ,0) : 0 ;
    FOREACH_ARRAY_ITEM(_when_choices,i,elem) {
        if (!elem) continue ;
        (void) elem->TypeInfer(cond_type,0, (elem->IsRange()) ? VHDL_range : VHDL_READ,0) ;
    }
    return result_type ;
}

VhdlBlockGenerics::VhdlBlockGenerics(Array *generic_clause, Array *generic_map_aspect)
  : VhdlTreeNode(),
    _generic_clause(generic_clause),
    _generic_map_aspect(generic_map_aspect),
    _block_label(0)
{
    Array *ids ;
    VhdlInterfaceDecl *decl ;
    unsigned i ;

    // Accumulate declared generics by order, and set on the block label.
    Array *generics = (_generic_clause) ? new Array(_generic_clause->Size()) : 0 ;

    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (!decl) continue ;
        ids = decl->GetIds() ;
        // Add these generics to the order-based generics array
        if (ids) generics->Append(ids) ;
        id = decl->GetId() ; // interface type/subprogram and package
        if (id) generics->InsertLast(id) ;
    }

    // Set the generics on the block label :
    _block_label = _present_scope->GetOwner() ;
    VERIFIC_ASSERT(_block_label && _block_label->IsBlock()) ;

    _block_label->DeclareBlockId(generics, 0) ;

    // For type-inference / checking of the association list
    // We need to set the 'actual' scope the the 'upper' scope,
    // since the present scope is the same as the 'local' scope of the block.
    VhdlScope *save_scope = _present_scope ;
    _present_scope = _present_scope->Upper() ;

    // Now do the association list (the generic map aspect) :
    _block_label->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;

    _present_scope = save_scope ;
}
VhdlBlockGenerics::~VhdlBlockGenerics()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,elem) delete elem ;
    delete _generic_clause ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,elem) delete elem ;
    delete _generic_map_aspect ;
}

VhdlBlockPorts::VhdlBlockPorts(Array *port_clause, Array *port_map_aspect)
  : VhdlTreeNode(),
    _port_clause(port_clause),
    _port_map_aspect(port_map_aspect),
    _block_label(0)
{
    Array *ids ;
    VhdlInterfaceDecl *decl ;
    unsigned i ;

    // Accumulate declared ports by order, and set on the block label.
    Array *ports = (_port_clause) ? new Array(_port_clause->Size()) : 0 ;

    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (!decl) continue ;
        ids = decl->GetIds() ;
        // Add these ports to the order-based ports array
        ports->Append(ids) ;
    }

    // Set the block label pointer.
    _block_label = _present_scope->GetOwner() ;
    VERIFIC_ASSERT(_block_label && _block_label->IsBlock()) ;

    // Set the ports on the block label :
    _block_label->DeclareBlockId(0, ports) ;

    // For type-inference / checking of the association list
    // We need to set the 'actual' scope the the 'upper' scope,
    // since the present scope is the same as the 'local' scope of the block.
    VhdlScope *save_scope = _present_scope ;
    _present_scope = _present_scope->Upper() ;

    // Now do the association list (the port map aspect) :
    _block_label->TypeInferAssociationList(_port_map_aspect, VHDL_port, 0, this) ;

    _present_scope = save_scope ;
}
VhdlBlockPorts::~VhdlBlockPorts()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_port_clause,i,elem) delete elem ;
    delete _port_clause ;
    FOREACH_ARRAY_ITEM(_port_map_aspect,i,elem) delete elem ;
    delete _port_map_aspect ;
}

VhdlCaseStatementAlternative::VhdlCaseStatementAlternative(Array *choices, Array *statements)
  : VhdlTreeNode(),
    _choices(choices),
    _statements(statements)
{
    // LRM 8.8 : Test that IF there is an others clause that it is the only choice
    // (so if there are multiple choices, error out if there is an others clause in there
#if 0
    if (_choices && _choices->Size()>1) {
        unsigned i ;
        VhdlDiscreteRange *choice ;
        FOREACH_ARRAY_ITEM(_choices, i, choice) {
            if (choice && choice->IsOthers()) {
                choice->Error("OTHERS should be the only choice in alternative") ;
            }
        }
    }
#endif

    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice && choice->IsOthers() && _choices->Size() > 1) {
            choice->Error("OTHERS should be the only choice in alternative") ;
        }
    }
}
VhdlCaseStatementAlternative::~VhdlCaseStatementAlternative()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_choices,i,elem) delete elem ;
    delete _choices ;
    FOREACH_ARRAY_ITEM(_statements,i,elem) delete elem ;
    delete _statements ;
}

unsigned VhdlCaseStatementAlternative::IsOthers() const
{
    // Check if 'others' is in the choices.
    // We already tested that IF there is an others, that it is the only one.
    // So we can just test cases that are one choice.
    if (!_choices || _choices->Size()!=1) return 0 ;
    VhdlDiscreteRange *choice = (VhdlDiscreteRange*)_choices->GetLast() ;
    if (!choice) return 0 ;
    return choice->IsOthers() ;
}

void VhdlCaseStatementAlternative::TypeInferChoices(VhdlIdDef *case_type)
{
    VhdlExpression *elem ;
    VhdlIdDef *elem_type ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_choices,i,elem) {
        if (!elem) continue ;
        elem_type = elem->TypeInfer(case_type,0, (elem->IsRange()) ? VHDL_range : VHDL_READ,0) ;
        if (elem_type && elem->IsRange() && !elem_type->IsDiscreteType()) {
            elem->Error("choice must be a discrete range") ;
        }

        // LRM Section 8.8 : TODO : More checks
        if (!elem->IsLocallyStatic()) {
            // Viper #5273, 5494: Relax the check for globally static
            if (elem->IsGloballyStatic()) {
                elem->Warning("case choice must be a locally static expression") ;
            } else {
                elem->Error("case choice must be a locally static expression") ;
            }
        }
    }
}

unsigned VhdlCaseStatementAlternative::HasRangeChoice() const
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice && choice->IsRange()) return 1 ;
    }
    return 0 ;
}

unsigned VhdlSelectedWaveform::HasRangeChoice() const
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice && choice->IsRange()) return 1 ;
    }
    return 0 ;
}

unsigned VhdlSelectedExpression::HasRangeChoice() const
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice && choice->IsRange()) return 1 ;
    }
    return 0 ;
}

VhdlElsif::VhdlElsif(VhdlExpression *condition, Array *statements)
  : VhdlTreeNode(),
    _condition(condition),
    _statements(statements) {
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
}
VhdlElsif::~VhdlElsif()
{
    delete _condition ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_statements,i,elem) delete elem ;
    delete _statements ;
}

VhdlEntityClassEntry::VhdlEntityClassEntry(unsigned entity_class, unsigned box)
  : VhdlTreeNode(),
    _entity_class(entity_class),
    _box(box)
{ }
VhdlEntityClassEntry::~VhdlEntityClassEntry() { }

/**********************************************************/
//  Other routines
/**********************************************************/

void
VhdlSignature::FillProfile(VhdlOperatorId &profile)
{
    // Fill the profile, and set it in the operator

    // Follow the rules in LRM 2.3.2
    VhdlIdDef *ret_type = (_return_type) ? _return_type->TypeMark() : 0 ;

    Array *args = new Array() ;
    unsigned i ;
    VhdlName *arg_name ;
    FOREACH_ARRAY_ITEM(_type_mark_list, i, arg_name) {
        if (!arg_name) continue ;
        VhdlIdDef *type_mark = arg_name->TypeMark() ;
        args->Insert(type_mark) ;
        // VIPER #7535: Signature should contain
        // type name only, error out otherwise.
        if (!type_mark) {
            Error("%s is not a type", 0) ;
        }
    }

    if (ret_type) {
        profile.DeclareFunction(ret_type, args, 0) ;
    } else {
        profile.DeclareProcedure(args, 0) ;
    }
}

const char *
VhdlBindingIndication::ArchitectureNameAspect() const
{
    // Return the (optional) architecture name in the entity aspect.
    return (_entity_aspect) ? _entity_aspect->ArchitectureNameAspect() : 0 ;
}

/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlSignature::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_type_mark_list, i, name) {
        if (name) sig = HASH_VAL(sig, name->CalculateSignature()) ;
    }
    if (_return_type) sig = HASH_VAL(sig, _return_type->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlFileOpenInfo::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_file_open) sig = HASH_VAL(sig, _file_open->CalculateSignature()) ;
    sig = HASH_VAL(sig, _open_mode) ;
    if (_file_logical_name) sig = HASH_VAL(sig, _file_logical_name->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlBindingIndication::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_entity_aspect) sig = HASH_VAL(sig, _entity_aspect->CalculateSignature()) ;
    unsigned i ;
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    // back-pointer : if (_unit) sig = HASH_VAL(sig, _unit->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlIterScheme::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    return sig ;
}

unsigned long
VhdlWhileScheme::CalculateSignature() const
{
    unsigned long sig = VhdlIterScheme::CalculateSignature() ;
    if (_condition) sig = HASH_VAL(sig, _condition->CalculateSignature()) ;
    if (_exit_label) sig = HASH_VAL(sig, _exit_label->CalculateSignature()) ;
    if (_next_label) sig = HASH_VAL(sig, _next_label->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlForScheme::CalculateSignature() const
{
    unsigned long sig = VhdlIterScheme::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    if (_range) sig = HASH_VAL(sig, _range->CalculateSignature()) ;
    if (_iter_id) sig = HASH_VAL(sig, _iter_id->CalculateSignature()) ;
    if (_exit_label) sig = HASH_VAL(sig, _exit_label->CalculateSignature()) ;
    if (_next_label) sig = HASH_VAL(sig, _next_label->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlIfScheme::CalculateSignature() const
{
    unsigned long sig = VhdlIterScheme::CalculateSignature() ;
    if (_condition) sig = HASH_VAL(sig, _condition->CalculateSignature()) ;
    if (_alternative_label) sig = HASH_VAL(sig, _alternative_label->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlElsifElseScheme::CalculateSignature() const
{
    unsigned long sig = VhdlIfScheme::CalculateSignature() ;
    sig = HASH_VAL(sig, GetClassId()) ;
    return sig ;
}
unsigned long
VhdlCaseItemScheme::CalculateSignature() const
{
    unsigned long sig = VhdlIterScheme::CalculateSignature() ;
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice) sig = HASH_VAL(sig, choice->CalculateSignature()) ;
    }
    if (_alternative_label) sig = HASH_VAL(sig, _alternative_label->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlDelayMechanism::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    return sig ;
}

unsigned long
VhdlTransport::CalculateSignature() const
{
    unsigned long sig = VhdlDelayMechanism::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlInertialDelay::CalculateSignature() const
{
    unsigned long sig = VhdlDelayMechanism::CalculateSignature() ;
    if (_reject_expression) sig = HASH_VAL(sig, _reject_expression->CalculateSignature()) ;
    sig = HASH_VAL(sig, _inertial) ;
    return sig ;
}

unsigned long
VhdlOptions::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    sig = HASH_VAL(sig, _guarded) ;
    if (_delay_mechanism) sig = HASH_VAL(sig, _delay_mechanism->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlConditionalWaveform::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    VhdlExpression *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_waveform, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    if (_when_condition) sig = HASH_VAL(sig, _when_condition->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlConditionalExpression::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    if (_condition) sig = HASH_VAL(sig, _condition->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlSelectedWaveform::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    unsigned i ;
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_waveform, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_when_choices, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlSelectedExpression::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    unsigned i ;
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_when_choices, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlBlockGenerics::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    // back-pointer : if (_block_label) sig = HASH_VAL(sig, _block_label->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlBlockPorts::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    // back-pointer : if (_block_label) sig = HASH_VAL(sig, _block_label->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlCaseStatementAlternative::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    unsigned i ;
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_choices, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlElsif::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_condition) sig = HASH_VAL(sig, _condition->CalculateSignature()) ;
    VhdlStatement *stmt ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlEntityClassEntry::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    sig = HASH_VAL(sig, _entity_class) ;
    sig = HASH_VAL(sig, _box) ;
    return sig ;
}

VhdlDesignator *VhdlIfScheme::GetAlternativeClosingLabel() const // VIPER #6666 & #6662
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    return _alternative_closing_label ;
#else
    return 0 ;
#endif
}

void VhdlIfScheme::SetAlternativeClosingLabel(VhdlDesignator *label) // VIPER #6666 & #6662
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _alternative_closing_label ;
    _alternative_closing_label = label ;
#else
    delete label ;
#endif
}

VhdlDesignator *VhdlCaseItemScheme::GetAlternativeClosingLabel() const // VIPER #6666 & #6662
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    return _alternative_closing_label ;
#else
    return 0 ;
#endif
}

void VhdlCaseItemScheme::SetAlternativeClosingLabel(VhdlDesignator *label) // VIPER #6666 & #6662
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _alternative_closing_label ;
    _alternative_closing_label = label ;
#else
    delete label ;
#endif
}

