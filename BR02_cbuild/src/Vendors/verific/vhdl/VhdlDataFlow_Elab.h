/*
 *
 * [ File Version : 1.88 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_DATAFLOW_H_
#define _VERIFIC_VHDL_DATAFLOW_H_

#include "VerificSystem.h"
#include "VhdlCompileFlags.h" // Vhdl-specific compile flags

// VhdlDataFlow is ONLY valid during elaboration

#include "Map.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Set ;
class Array ;

class Net;

class VhdlValue ;
class VhdlIdDef ;
class VhdlTreeNode ;
class VhdlNonconst ;
class VhdlNonconstBit ;
class VhdlScope ;

class VhdlGotoArea ;
class VhdlWaitStatement ;

/* -------------------------------------------------------------- */

/*
   VhdlDataFlow represents end-points of identifier values in a control/data-flow branch.
   This class is created when entering a control/data flow branch by running over the parse-tree,
   and is cleaned up when the branch is merged into its containing control/data flow branch.
   The 'Map' is a identifier pointer->end-value associated hash structure.
   This class is only used as temporary storage during elaboration.
*/
class VFC_DLL_PORT VhdlDataFlow : public Map
{
public:
    // Dataflow constructor : pass in upper(owner) dataflow area, flag if its clocked,
    // and borrowed pointers to the scope (locally declared identifiers) and any sensitivity list which might apply
    explicit VhdlDataFlow(VhdlDataFlow *owner, Net *clock = 0, VhdlScope *scope = 0, Set *sensitivity_list = 0);

    // Note: Destructor of base class Map is not virtual, do not delete a VhdlDataFlow object by Map *
    virtual ~VhdlDataFlow();

private:
    // Prevent compiler from defining the following
    VhdlDataFlow() ;                                // Purposely leave unimplemented
    VhdlDataFlow(const VhdlDataFlow &) ;            // Purposely leave unimplemented
    VhdlDataFlow& operator=(const VhdlDataFlow &) ; // Purposely leave unimplemented

public:
    // The upper dataflow area
    VhdlDataFlow *      Owner() const               { return _owner ; }  // Return the uppor dataflow area

    // Obtain/set id<->value pairs in this dataflow
    VhdlValue *         IdValue(VhdlIdDef *id) const ;                   // Pick-up existing value, but user does not own it
    VhdlValue *         AssignValue(VhdlIdDef *id) ;                     // Obtain a new value in this data flow to assign to
    VhdlValue *         SignalValue(VhdlIdDef *id) const ;               // Viper #5553: Pick-up id->Value except under the (id == id_value) conditional branch, in which case return id_value
    void                SetAssignValue(const VhdlIdDef *id, const VhdlValue *val) ;  // Set/overwrite a new value in this dataflow

    unsigned            SetConditionIdValueMap(Map *id_value_map) ; // Viper #5553

    // Get the sensitivity list used to enter this flow
    Set *               GetSensitivityList() const      { return _sensitivity_list ; }
    void                SetSensitivityList(Set *sens_list) ;
    unsigned            SensListCheck(const VhdlIdDef *id) const ;          // Check if this (signal) is on the sensitivity list
    unsigned            IsSensListIncomplete() const { return _incomplete_sensitivity_list ; }
    void                SetIncompleteSensList() ;

    // Viper #7413: Mark for process(all)
    unsigned            IsSensListAll() const { return _sensitivity_list_all ; }
    void                SetSensitivityListAll() ;

    unsigned            IsImplicitClock(const VhdlValue *condition) const ; // Check if this value is an implicit clock edge (if it's the only one on the sensitivity list)

    // Obtain access to the (enclosing) scope related to this dataflow area
    VhdlScope *         GetScope() const            { return _scope ; }  // Get the immediate scope in which this data flow operates.
    VhdlScope *         GetEnclosingScope() const   { return (_scope) ? _scope : (_owner) ? _owner->GetEnclosingScope() : 0 ; } // Get the nearest scope in which this dataflow operates.

    // Populate Set's with assigned identifiers or used goto-labels in this dataflow.
    void                InsertIds(Set &list) const ;     // Insert id's that are assigned in this area into Set
    void                InsertLabels(Set &list) const ;  // Insert goto-labels that are used in this area into Set

    // Tests
    unsigned            IsTemporaryDataFlow() const ; // Check if this dataflow is of temporary lifetime (subprogram) as opposed to live-forever (a process).
// RD: Demoted IsWaiting flag processing during elaboration. We now already do the checks in analyser.
//    unsigned            IsWaiting() const           { return _is_waiting ; }    // Check if the dataflow has a wait statement in (every branch of) it
//    void                SetIsWaiting()              { _is_waiting = 1 ; } // set that a wait statement was executed previously
    unsigned            IsClocked() const           { return (_clock) ? 1 : 0 ; }    // Check if this dataflow is clocked
    unsigned            HasClockEnable() const      { return (_clock_enable) ? 1 : 0 ; }    // Viper #4958: Check if this dataflow has clock enable
    Net *               GetClock() const            { return _clock ; } // Get the clock net (if this dataflow is clocked)
    void                SetClock(Net *clock)        { _clock = clock ; } // Set the clock of this dataflow.
    Net *               GetClockEnable() const      { return _clock_enable ; } // Viper #4958: Get the clock enable net
    void                SetClockEnable(Net *enable) { _clock_enable = enable ; } // Viper #4958: Set the clock enable of this dataflow.

    // Dead-Code flag :
    void                SetDeadCode()               { _is_dead_code = 1 ; }     // Flag that elaboration jumped out of this dataflow (e.g. after a return statement)
    void                SetIsAlive()                { _is_dead_code = 0 ; }     // Flag that elaboration jumped back to this dataflow (e.g. after a loop which was exited)
    unsigned            IsDeadCode() const          { return _is_dead_code ; }  // Check if elaboration jumped out of this dataflow (e.g. after a return statement)
    unsigned            HasBeenAssigned(VhdlIdDef *id) const ; // Check if this identifier has been assigned previously (here or in upper flow).
    unsigned            IsInInitial() const         { return _in_initial ; }
    void                SetInInitial()              { _in_initial = 1 ; } // Flag that elaboration is now in the initial time frame
    void                SetNotInInitial()           { _in_initial = 0 ; } // Flag that elaboration is now not in the initial time frame

    // Viper #8210, 8219
    void                SetForcedInitial()          { _in_initial = 1 ; _is_forced_initial = 1 ; }
    unsigned            IsForcedInitial() const     { return _is_forced_initial ; }

    unsigned            IsInSubprogramOrProcess() const { return _in_subprogram_or_process ; }
    void                SetInSubprogramOrProcess()  { _in_subprogram_or_process = 1 ; }

    // Static elaboration specific non constant expression indication flag
    void                SetNonconstExpr()           { _nonconst_expr = 1 ; }
    unsigned            IsNonconstExpr() const      { return _nonconst_expr ; }
    // VIPER #3815 : Routine to identify branch with non-constant condition
    unsigned            IsNonconstBranch() const    { return (_nonconst_expr && _owner) ; }

// RD: These two (and the field) no longer needed since VIPER 7018 fix.
//    void                SetConditionExpr(unsigned val) { _condition_expr = val ; } // Added for Viper #4233/4234
//    unsigned            IsConditionExpression() const  { return _condition_expr ; }
    void                InvalidateIdValues(const Array *dataflows, const VhdlTreeNode *from) ; // VIPER #7286

    // Create different styles of logic to pop out of various constructs in this dataflow :
    void                PopOutOfCaseWithMuxes(const VhdlNonconst *case_expr, const Map *position_to_dataflow, const VhdlDataFlow *others, const VhdlTreeNode *from) ;
    void                PopOutOfCaseWithEquals(const VhdlValue *case_expr, Map *choice_to_dataflow, const VhdlDataFlow *others, const VhdlTreeNode *from, unsigned matching_case = 0) ;
    void                PopOutOfIf(const Map *condition_to_dataflow, const VhdlDataFlow *else_flow, const VhdlTreeNode *from) ;

    // Goto areas (dataflow jump control) :
    void                AddGotoArea(VhdlGotoArea *area, const VhdlTreeNode *from) ;
    VhdlGotoArea *      GetGotoArea(const VhdlIdDef *label) const ;
    Map *               GotoAreas() const { return _goto_areas ; } // Get the goto states (areas) that were executed under this dataflow
    void                DoGoto(VhdlIdDef *goto_label, const VhdlValue *condition, const VhdlTreeNode *from) ; // Interrupt dataflow, to go to the 'label' under 'condition'
    void                Resolve(const VhdlIdDef *goto_label, const VhdlTreeNode *from) ; // Resolve all goto's to this label

    void                UpdateInitialValues() ; // Viper #5687: To update the initial values from the initial dataflow

    // Create (and store internally) an identifier which will contain a single-bit net
    // which represents the condition under which this data flow is active.
    // This is as if there is a single assignment to '1' from 'this' dataflow, and '0' from anything else.
    // Return the (initial value) net which will eventually represent 'this-dataflow-is-active' :
    Net                *GenerateCondition(const VhdlTreeNode *from) ;

    // Test if id is an assertion : Only applicable for RTL elab with assertion synthesis.
    unsigned            IsConditionId(const VhdlIdDef *id) const ; // check if this id is an assertion identifier.

    // APIs added for Viper #3765
#ifdef VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
    void                SetCallerTreeNode(VhdlTreeNode *caller_node) ;
#endif // VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
    VhdlTreeNode       *GetCallerTreeNode(void) const ;

    void                SetSubprogInstanceId(VhdlIdDef *id) { _subprog_inst = id ; }
    VhdlIdDef          *GetSubprogInstanceId() const        { return _subprog_inst ; }

private:

    VhdlDataFlow        *_owner ;
    Map                 *_goto_areas ;         // Hash table of :  Key:goto-label(VhdlIdDef)  value:VhdlGotoArea
    Set                 *_sensitivity_list ;   // List of id's on the sensitivity list of an upper process. Used to test 'used' ids when evaluating an expression. If not there, it means that this dataflow does not require id sens-list checking. The Set is NOT owned by 'DataFlow', just borrowed from above.
    VhdlScope           *_scope ;              // The scope which this dataflow refers to (if any). Allows access to locally declared variables, and all VeriScope's API from this dataflow.

    Map                 *_condition_id_value_map ; // Viper #5553: id->id_value extracted from (id == id_value) condition

    Set                 *_conditionids ;       // set of condition identifiers (for condition generation during RTL elaboration.

    Net *                _clock ;              // Clock (edge-condition) of this dataflow.

    Net *                _clock_enable ;       // Viper #4958: Clock-Enable condition of this dataflow.

#ifdef VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
    VhdlTreeNode        *_from_treenode ;      // Viper #3765
#endif

    unsigned             _is_dead_code:1 ;     // Flag that dataflow has a return statement itself, or in every (sub) branch
    unsigned             _is_waiting:1 ;       // Flag that dataflow has a wait statement itself, or in every (sub) branch
    unsigned             _nonconst_expr:1 ;    // Static elaboration specific flag that df has a non constant expr
// RD: field no longer needed since VIPER 7018 fix :
//    unsigned             _condition_expr:1 ;   // Static elaboration specific flag Viper #4233/4234
    unsigned             _in_initial:1 ;       // Flag that elaboration is now in the initial time frame
    unsigned             _is_forced_initial:1 ; // Viper #8210, 8219: in seq area but faked as initial : do not preserve initial values in subprograms
    unsigned             _in_subprogram_or_process:1 ; // Flag that elaboration is now inside subprogram or process
    unsigned             _incomplete_sensitivity_list:1 ; // Flag to mark if sensitivity list is incomplete
    unsigned             _sensitivity_list_all:1 ; // Flag to mark if Vhdl_all is present in the sensitivity list : Viper #7413
    VhdlIdDef           *_subprog_inst ;       // Subprogram instance id for subprog-df
} ; // class VhdlDataFlow

/* -------------------------------------------------------------- */

/*
   VhdlGotoArea is a class which represents the temporary id values of disabled portions of code
   within a VhdlDataFlow.  This only pertains to 'exit' and 'next' statements within a loop.
   This class is only used as temporary storage during elaboration.
*/
class VFC_DLL_PORT VhdlGotoArea : public Map
{
public:
    VhdlGotoArea(VhdlIdDef *goto_label, Net *condition) ;
    // Note: Destructor of base class Map is not virtual, do not delete a VhdlGotoArea object by Map *
    virtual ~VhdlGotoArea() ;

private:
    // Prevent compiler from defining the following
    VhdlGotoArea() ;                                // Purposely leave unimplemented
    VhdlGotoArea(const VhdlGotoArea &) ;            // Purposely leave unimplemented
    VhdlGotoArea& operator=(const VhdlGotoArea &) ; // Purposely leave unimplemented

public:
    void            InsertIds(Set &list) const ;    // Insert id's that are assigned in this area into Set
    void            SetOwner(VhdlDataFlow *owner)   { _owner = owner ; }

    // Accessor methods
    VhdlDataFlow *  Owner() const                   { return _owner ; }          // The data flow in which this goto-label jump occurs.
    VhdlIdDef *     Label() const                   { return _goto_label ; }     // The label to which we jump
    Net *           Condition() const               { return _condition ; }      // The condition under which the jump occurs.
    void            SetCondition(Net *condition)    { _condition = condition ; } // modify condition after creation

private:
    VhdlDataFlow    *_owner ; // The data flow in which this goto-label jump occurs.
    VhdlIdDef       *_goto_label ; // The label to which we jump
    Net             *_condition ; // The condition under which the jump occurs.
} ; // class VhdlGotoArea

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_DATAFLOW_H_

