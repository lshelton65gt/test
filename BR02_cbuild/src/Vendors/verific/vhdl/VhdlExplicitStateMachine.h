/*
 *
 * [ File Version : 1.8 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_EXPLICITSTATEMACHINE_H_
#define _VERIFIC_VHDL_EXPLICITSTATEMACHINE_H_

#include "Array.h"
#include "Set.h"
#include "Map.h"

#include "VhdlVisitor.h"    // Visitor base class definition
#include "VhdlCopy.h"
#include "ControlFlow.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VhdlScope ;
class VhdlExpression ;

/*
   Class to visit unit and the hierarchy under it. This class is used to create the
   control flow graph under checking some conditions and limitations and finally create
   a new process statement replacing the older one:
*/
class VFC_DLL_PORT VhdlFsmProcessStatementVisitor : public VhdlVisitor
{
public:
    VhdlFsmProcessStatementVisitor();
    virtual ~VhdlFsmProcessStatementVisitor();

private:
    // Prevent the compiler from implementing the following
    VhdlFsmProcessStatementVisitor(const VhdlFsmProcessStatementVisitor &node) ;           // Purposely leave unimplemented
    VhdlFsmProcessStatementVisitor& operator=(const VhdlFsmProcessStatementVisitor &rhs) ; // Purposely leave unimplemented

public:
    // The following methods need to be redefined for our purpose :
    virtual void            VHDL_VISIT(VhdlPrimaryUnit, unit) ;
    virtual void            VHDL_VISIT(VhdlProcessStatement, process_stmt) ;
    virtual void            VHDL_VISIT(VhdlGenerateStatement, generate_stmt) ;
    virtual void            VHDL_VISIT(VhdlWaitStatement, wait_stmt) ;
    virtual void            VHDL_VISIT(VhdlBlockStatement, blk_stmt) ;

    VhdlProcessStatement*   ConvertToExplicitStateMachine(const VhdlProcessStatement *stmt) ;
    VhdlProcessStatement*   ProcessStatement(const VhdlProcessStatement *stmt) ; // Create the cfg and return new process statement
    void                    CountStateNode(VerificCFGRootNode *root, Array &state_node, Set *visited_set) ; // Count the total number of event controls in an process statement
    void                    ExplicitStateMachine(VerificCFGRootNode *child, const VerificCFGRootNode *node, Map &state_map, Array *expr_arr, Array *stmts, VhdlMapForCopy &old2new) ; // Generate ExplicitStateMachine
    virtual void            GenerateCaseState(VerificCFGRootNode *node, Array *stmts) ; // Generate statement for state node(event control)

    unsigned                GetNumOfProcessStmtReplaced() const      { return _process_stmt_replaced ; } // Return the total number of items replaced
    void                    SetResetVariable(unsigned reset)         { _reset = reset ; }

    // Create unique name for variable: checking name conflicts in that scope and the upper scopes
    static char*            CreateVariableName(const char *prefix, const Array *label_arr, VhdlScope *scope, VhdlScope *process_scope=0) ;
    static char*            FindStringLiteralOfStateNum(VerificCFGRootNode *node, unsigned digit) ;
    void                    CleanupParseTreeArray(Array *arr) const ; // TreeNode deletion
    void                    Reset() ;

protected:
    virtual void            TraverseArray(const Array *array) ; // Overwrite the VhdlVisitor::TraverseArray()

private:
    Set                   _event_expr_set ;               // store the string made from event expression array of the first event control statement traversed
    Set                   _event_control_set ;            // store the total number of events within a process statement: do not convert to explicit state m/c if total number of event is less than 1
    VhdlScope             *_unit_scope ;                  // store the unit scope
    VhdlScope             *_scope ;                       // store the parent scope
    VhdlProcessStatement  *_process ;                     // store the newly created process statement
    unsigned              _process_stmt_replaced: 10 ;    // represent how many process statements are replaced
    unsigned              _has_process: 1 ;               // flag indicating unit containing process statement
    unsigned              _process_construct: 10 ;        // contian the number of the process statement that is processed
    unsigned              _has_wait_stmt: 1 ;             // flag indicating unit containing event control
    unsigned              _is_edged: 1 ;                  // flag indicating a event expression is edged
    unsigned              _mult_clk: 1 ;                  // this feature is not yet supported: flag indicating unit containing multiple clocking event
    unsigned              _infinite_loop: 1 ;             // flag indicating presence of cycles
    VhdlIdDef             *_var_id ;                      // store the id of the state variable required for explicit state m/c generation
    unsigned              _digit ;
    Array                 *_sensitivity_list ;
    VerificCFGDeclarationNode *_decl_node ;               // node contain all the local declarations and repeat variable declarations
    unsigned              _reset: 1 ;                     // flag for reset variable, required to execute reset block
} ; // class VhdlFsmProcessStatementVisitor

/* -------------------------------------------------------------- */

/*
   Class to visit plain/event control/conditional/loop statements under process statement:
   This class is used to create the control flow graph and a new process statement:
*/
class VFC_DLL_PORT VhdlFsmProcessVisitor : public VhdlVisitor
{
public:
    VhdlFsmProcessVisitor();
    virtual ~VhdlFsmProcessVisitor();

private:
    // Prevent the compiler from implementing the following
    VhdlFsmProcessVisitor(const VhdlFsmProcessVisitor &node);
    VhdlFsmProcessVisitor& operator=(const VhdlFsmProcessVisitor &rhs);

public:
    // The following methods need to be redefined for our purpose :
    virtual void      VHDL_VISIT(VhdlStatement, stmt) ;
    virtual void      VHDL_VISIT(VhdlWaitStatement, wait_stmt) ;
    virtual void      VHDL_VISIT(VhdlIfStatement, if_stmt) ;
    virtual void      VHDL_VISIT(VhdlElsif, elseif) ;
    virtual void      VHDL_VISIT(VhdlCaseStatement, case_stmt) ;
    virtual void      VHDL_VISIT(VhdlCaseStatementAlternative, case_alt) ;
    virtual void      VHDL_VISIT(VhdlLoopStatement, loop_stmt) ;
    virtual void      VHDL_VISIT(VhdlForScheme, for_schm) ;
    virtual void      VHDL_VISIT(VhdlWhileScheme, while_schm) ;
    virtual void      VHDL_VISIT(VhdlExitStatement, exit) ;
    virtual void      VHDL_VISIT(VhdlNextStatement, next) ;

    VerificCFGRootNode*        GetCurrentNode() const                        { return _current_node ; } // Get the current processing node
    VerificCFGRootNode*        GetParentNode() const                         { return _parent_node ; }  // Get the parent node
    void                       SetUnitScope(VhdlScope *scope)                { _unit_scope = scope ; }
    void                       SetScope(VhdlScope *scope)                    { _scope = scope ; }
    void                       SetProcessScope(VhdlScope *scope)             { _process_scope = scope ; } // set the newly created process scope avaiable to this visitor
    Array*                     GetNewlyCreatedArray()                        { return _newly_created_arr ; }
    void                       SetDeclNode(VerificCFGDeclarationNode *decl)  { _decl_node = decl ; }
    Map                        GetDeclNodeMap() const                        { return _decl_node_map ; }
    unsigned                   GetStateNum() const                           { return _state_num ; }

private:
    VerificCFGRootNode        *_parent_node ;       // pointer to the parent node
    VerificCFGRootNode        *_current_node ;      // pointer to currently processing node
    VerificCFGRootNode        *_loop_node ;         // keep the parent loop node for treating break/continue statements
    Map                       _labeled_loop_map ;   // store label id vrs the end node of a sequential block
    unsigned                  _state_num ;          // state
    VhdlScope                 *_unit_scope ;        // contains the unit scope
    VhdlScope                 *_scope ;             // contains the present scope
    VhdlScope                 *_process_scope ;     // contains the new process statement scope
    Map                       _incr_node_map ;
    Map                       _next_stmt_nodes ;    // contains the nodes for continue statements in loops
    VhdlExpression            *_cond_expr ;
    VhdlVariableAssignmentStatement *_incr_stmt ;
    Array                     *_newly_created_arr ; // array of statements that are created during CFG formation
    VerificCFGDeclarationNode *_decl_node ;         // node contain all the local declarations and repeat variable declarations
    Map                       _decl_node_map ;      // VIPER #6879: map containing the iter id vrs new data declaration for iter ids of for loop
    Array                     *_elsif_arr ;
    Array                     *_else_arr ;
    verific_int64             _left ;
    verific_int64             _right ;
} ; // class VhdlFsmProcessVisitor
/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_EXPLICITSTATEMACHINE_H_

