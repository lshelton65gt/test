/*
 *
 * [ File Version : 1.408 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlTreeNode.h"

#include <string.h>  // for strchr
#include <ctype.h>   // for isalpha

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Message.h"
#include "Strings.h"

#include "vhdl_file.h"

#include "vhdl_tokens.h"
#include "VhdlUnits.h"
#include "VhdlScope.h"     // For std types
#include "VhdlIdDef.h"     // For UniversalInteger
#include "VhdlName.h"      // For VhdlDesignator

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VhdlScope *VhdlNode::_present_scope = 0 ;

Map *VhdlNode::_pragma_table = 0 ;
Set *VhdlNode::_pragma_signed_table = 0 ;
Set *VhdlNode::_operators = 0 ;

Map *VhdlNode::_msgs = 0 ;

Map *VhdlTreeNode::_map_of_comment_arr = 0 ; // VIPER #7322

// Constructor/destructor have nothing to do. No members
VhdlNode::VhdlNode()
{
}

VhdlNode::~VhdlNode()
{
}

// Set mode of analyzer (87, 93, PSL dialect)
unsigned VhdlNode::IsVhdl87()   { return vhdl_file::IsVhdl87() ; }
unsigned VhdlNode::IsVhdl93()   { return vhdl_file::IsVhdl93() ; }
unsigned VhdlNode::IsVhdlPsl()  { return vhdl_file::IsVhdlPsl() ; }
unsigned VhdlNode::IsVhdl2K()   { return vhdl_file::IsVhdl2K() ; }
unsigned VhdlNode::IsVhdl2008()   { return vhdl_file::IsVhdl2008() ; }
void VhdlNode::SetVhdl87()      { vhdl_file::SetVhdl87() ; }
void VhdlNode::SetVhdl93()      { vhdl_file::SetVhdl93() ; }
void VhdlNode::SetVhdl2K()      { vhdl_file::SetVhdl2K() ; }
void VhdlNode::SetVhdl2008()      { vhdl_file::SetVhdl2008() ; }
void VhdlNode::SetVhdlPsl()     { vhdl_file::SetVhdlPsl() ; }

// Access static IdDef's. They are 'system-wide' Id's

VhdlIdDef *
VhdlNode::UniversalInteger()
{
    if (!_present_scope) return 0 ; // something badly wrong

    // First, find it as a normal standard type :
    VhdlIdDef *result = StdType(" universal_integer") ;

    // If still not found, this must be the first time we call this universal type.
    // So declare it (in the top (standard)) scope :
    if (!result) {
        // if it is not set (yet), create it
        VhdlTypeId *tmp = new VhdlUniversalInteger(Strings::save(" universal_integer")) ;

        // Declare the type in the present scope (as 'predefined', so that it's deleted later).
        // Issue 1797 : make sure to declare in the top-level scope, or else we wont find it later.
        _present_scope->TopScope()->DeclarePredefined(tmp) ;

        // LRM 7.5 : declare universal operators for universal integer too
        tmp->DeclareOperators() ;

        result = tmp ;
    }
    return result ;
}

VhdlIdDef *
VhdlNode::UniversalReal()
{
    if (!_present_scope) return 0 ; // something badly wrong

    // First, find it as a normal standard type :
    VhdlIdDef *result = StdType(" universal_real") ;

    if (!result) {
        // if it is not set (yet), create it
        VhdlTypeId *tmp = new VhdlUniversalReal(Strings::save(" universal_real")) ;

        // Declare the type in the present scope (as 'predefined', so that it's deleted later).
        // Issue 1797 : make sure to declare in the top-level scope, or else we wont find it later.
        _present_scope->TopScope()->DeclarePredefined(tmp) ;

        // LRM 7.5 : declare universal operators for universal real too
        tmp->DeclareOperators() ;

        result = tmp ;
    }
    return result ;
}

VhdlIdDef *
VhdlNode::UniversalVoid()
{
    if (!_present_scope) return 0 ; // something badly wrong

    // First, find it as a normal standard type :
    VhdlIdDef *result = StdType(" void") ;

    if (!result) {
        // if it is not set (yet), create it (and declare in the top level scope)
        // Create a UniversalReal. In fact, it should not matter what 'predefined' form we create.
        result = new VhdlUniversalReal(Strings::save(" void")) ;

        // Declare the type in the present scope (as 'predefined', so that it's deleted later).
        // Issue 1797 : make sure to declare in the top-level scope, or else we wont find it later.
        _present_scope->TopScope()->DeclarePredefined(result) ;

        // Don't declare predefined operators for 'VOID' type. It does not have any.
    }
    return result ;
}

VhdlIdDef *
VhdlNode::StdType(const char *std_type_name)
{
    // Find a standard type by name.
    // This routine is guaranteed to work during analysis, but not during elaboration.
    VERIFIC_ASSERT(_present_scope) ; // Present-scope must be set (we are in the analyzer).

    // Go to the top level scope
    VhdlScope *top_scope = (_present_scope) ? _present_scope->TopScope() : 0 ;
    VERIFIC_ASSERT(top_scope) ;

    // Find there as normal standard (included) type :
    VhdlIdDef *result = top_scope->FindSingleObjectIncluded(std_type_name) ;

    // If we are inside the standard package, find it local :
    if (!result) result = top_scope->FindSingleObjectLocal(std_type_name) ;

    return result ;
}

/*static*/
char *
VhdlNode::UnescapeVhdlName(const char *name)
{
    if (!name) return 0 ;

    unsigned len = (unsigned)Strings::len(name) ;
    unsigned is_escaped = (*name == '\\' && len && name[len-1] == '\\') ;
    // TODO : also ignore trailing spaces

    if (!is_escaped) return 0 ;

    char *new_name = Strings::save(name) ;

    unsigned i = 0 ;
    unsigned j = 0 ;
    for (i = 1; i < len-1; ++i, ++j) {
        new_name[j] = name[i] ;
        if (name[i] == '\\' && name[i+1] == '\\') ++i ;
    }

    new_name[j] = '\0' ;
    return new_name ;
}

// Name of built-in operators as they are stored in scope tables
const char *
VhdlNode::OperatorName(unsigned oper_type)
{
    switch (oper_type) {
    case VHDL_nand :            return "\"nand\"" ;
    case VHDL_nor :             return "\"nor\"" ;
    case VHDL_and :             return "\"and\"" ;
    case VHDL_or :              return "\"or\"" ;
    case VHDL_xor :             return "\"xor\"" ;
    case VHDL_xnor :            return "\"xnor\"" ;
    case VHDL_EQUAL :           return "\"=\"" ;
    case VHDL_NEQUAL :          return "\"/=\"" ;
    case VHDL_GTHAN :           return "\">\"" ;
    case VHDL_STHAN :           return "\"<\"" ;
    case VHDL_GEQUAL :          return "\">=\"" ;
    case VHDL_SEQUAL :          return "\"<=\"" ;
    case VHDL_sll :             return "\"sll\"" ;
    case VHDL_srl :             return "\"srl\"" ;
    case VHDL_rol :             return "\"rol\"" ;
    case VHDL_ror :             return "\"ror\"" ;
    case VHDL_sla :             return "\"sla\"" ;
    case VHDL_sra :             return "\"sra\"" ;
    case VHDL_MINUS :           return "\"-\"" ;
    case VHDL_PLUS :            return "\"+\"" ;
    case VHDL_AMPERSAND :       return "\"&\"" ;
    case VHDL_STAR :            return "\"*\"" ;
    case VHDL_SLASH :           return "\"/\"" ;
    case VHDL_mod :             return "\"mod\"" ;
    case VHDL_rem :             return "\"rem\"" ;
    case VHDL_EXPONENT :        return "\"**\"" ;
    case VHDL_abs :             return "\"abs\"" ;
    case VHDL_not :             return "\"not\"" ;
    case VHDL_minimum :         return "minimum" ; // 2008 LRM 5.3.2.4 and 5.2.6
    case VHDL_maximum :         return "maximum" ; // 2008 LRM 5.3.2.4 and 5.2.6
    case VHDL_to_string :       return "to_string" ; // 2008 LRM 5.7
    case VHDL_to_ostring :      return "to_ostring" ; // 2008 LRM 5.3.2.4
    case VHDL_to_hstring :      return "to_hstring" ; // 2008 LRM 5.3.2.4
    // TODO: following four are alias declarations. keep as operator for simplicity
    case VHDL_to_bstring :      return "to_bstring" ; // 2008 LRM 5.3.2.4
    case VHDL_to_binary_string :return "to_binary_string" ; // 2008 LRM 5.3.2.4
    case VHDL_to_octal_string : return "to_octal_string" ; // 2008 LRM 5.3.2.4
    case VHDL_to_hex_string :   return "to_hex_string" ; // 2008 LRM 5.3.2.4
    case VHDL_condition :       return "\"??\"" ;
    case VHDL_matching_equal :  return "\"?=\"" ;
    case VHDL_matching_nequal : return "\"?/=\"" ;
    case VHDL_matching_gthan :  return "\"?>\"" ;
    case VHDL_matching_sthan :  return "\"?<\"" ;
    case VHDL_matching_gequal : return "\"?>=\"" ;
    case VHDL_matching_sequal : return "\"?<=\"" ;
    // COMMA is artificial. For aggregates
    case VHDL_COMMA :           return "\",\"" ;
    // This was added for named predefined functions/procedures
    case VHDL_deallocate :      return "deallocate" ;
    case VHDL_file_open :       return "file_open" ;
    case VHDL_file_close :      return "file_close" ;
    case VHDL_read :            return "read" ;
    case VHDL_write :           return "write" ;
    case VHDL_endfile :         return "endfile" ;
    case VHDL_flush :           return "flush" ;
    case VHDL_now :             return "now" ;
    case VHDL_readline :        return "readline" ;
    case VHDL_writeline :       return "writeline" ;
    case VHDL_hread :           return "hread" ;
    case VHDL_hwrite :          return "hwrite" ;
    case VHDL_oread :           return "oread" ;
    case VHDL_owrite :          return "owrite" ;

    // Added for pragma'ed functions :
    case VHDL_UMINUS :          return "uminus" ;
    case VHDL_buf :             return "buf" ;
    case VHDL_REDAND :          return "reduce_and" ;
    case VHDL_REDNAND :         return "reduce_nand" ;
    case VHDL_REDOR :           return "reduce_or" ;
    case VHDL_REDNOR :          return "reduce_nor" ;
    case VHDL_REDXOR :          return "reduce_xor" ;
    case VHDL_REDXNOR :         return "reduce_xnor" ;
    case VHDL_FEEDTHROUGH :     return "feedthrough" ;
    case VHDL_PULLUP :          return "pullup" ;
    case VHDL_PULLDOWN :        return "pulldown" ;
    case VHDL_TRSTMEM :         return "trstmem" ;
    case VHDL_WIRED_THREE_STATE : return "wired_three_state" ;
    case VHDL_falling_edge :    return "falling_edge" ;
    case VHDL_rising_edge :     return "rising_edge" ;
    case VHDL_IGNORE_SUBPROGRAM : return "ignore_subprogram" ;

    // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
    case VHDL_math_ceil:        return "ceil" ;
    case VHDL_math_floor:       return "floor" ;
    case VHDL_math_sqrt:        return "sqrt" ;
    case VHDL_math_cbrt:        return "cbrt" ;
    case VHDL_math_exp:         return "exp" ;
    case VHDL_math_log:         return "log" ;
    case VHDL_math_log2:        return "log2" ;
    case VHDL_math_log10:       return "log10" ;
    case VHDL_math_sin:         return "sin" ;
    case VHDL_math_cos:         return "cos" ;
    case VHDL_math_tan:         return "tan" ;
    case VHDL_math_asin:        return "arcsin" ;
    case VHDL_math_acos:        return "arccos" ;
    case VHDL_math_atan:        return "arctan" ;
    case VHDL_math_sinh:        return "sinh" ;
    case VHDL_math_cosh:        return "cosh" ;
    case VHDL_math_tanh:        return "tanh" ;
    case VHDL_math_asinh:       return "asinh" ;
    case VHDL_math_acosh:       return "arccosh" ;
    case VHDL_math_atanh:       return "arctanh" ;
    case VHDL_math_mod:         return "\"mod\"" ;
    case VHDL_math_atan2:       return "arctan" ;
    case VHDL_math_pow:         return "**" ;
    case VHDL_AT:               return "@" ;

    default : return "unknown operator" ;
    }
}

// Name of built-in operators as they show up for usage in VHDL source code
const char *
VhdlNode::OperatorSimpleName(unsigned oper_type)
{
    switch (oper_type) {
    case VHDL_nand :            return "nand" ;
    case VHDL_nor :             return "nor" ;
    case VHDL_and :             return "and" ;
    case VHDL_or :              return "or" ;
    case VHDL_xor :             return "xor" ;
    case VHDL_xnor :            return "xnor" ;
    case VHDL_EQUAL :           return "=" ;
    case VHDL_NEQUAL :          return "/=" ;
    case VHDL_GTHAN :           return ">" ;
    case VHDL_STHAN :           return "<" ;
    case VHDL_GEQUAL :          return ">=" ;
    case VHDL_SEQUAL :          return "<=" ;
    case VHDL_sll :             return "sll" ;
    case VHDL_srl :             return "srl" ;
    case VHDL_rol :             return "rol" ;
    case VHDL_ror :             return "ror" ;
    case VHDL_sla :             return "sla" ;
    case VHDL_sra :             return "sra" ;
    case VHDL_MINUS :           return "-" ;
    case VHDL_PLUS :            return "+" ;
    case VHDL_AMPERSAND :       return "&" ;
    case VHDL_STAR :            return "*" ;
    case VHDL_SLASH :           return "/" ;
    case VHDL_mod :             return "mod" ;
    case VHDL_rem :             return "rem" ;
    case VHDL_EXPONENT :        return "**" ;
    case VHDL_abs :             return "abs" ;
    case VHDL_not :             return "not" ;
    // COMMA is artificial. For aggregates
    case VHDL_COMMA :           return "," ;
    case VHDL_condition :       return "??" ;
    case VHDL_matching_equal :  return "?=" ;
    case VHDL_matching_nequal : return "?/=" ;
    case VHDL_matching_gthan :  return "?>" ;
    case VHDL_matching_sthan :  return "?<" ;
    case VHDL_matching_gequal : return "?>=" ;
    case VHDL_matching_sequal : return "?<=" ;
    default : break ;
    }

    // If not called on an legal actual 'operator' token, then push out the name of the token :
    return OperatorName(oper_type) ;
}

unsigned
VhdlNode::IsMathRealPragma(unsigned pragma_function)
{
    // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
    switch (pragma_function) {
    case VHDL_math_ceil:
    case VHDL_math_floor:
    case VHDL_math_sqrt:
    case VHDL_math_cbrt:
    case VHDL_math_exp:
    case VHDL_math_log:
    case VHDL_math_log2:
    case VHDL_math_log10:
    case VHDL_math_sin:
    case VHDL_math_cos:
    case VHDL_math_tan:
    case VHDL_math_asin:
    case VHDL_math_acos:
    case VHDL_math_atan:
    case VHDL_math_sinh:
    case VHDL_math_cosh:
    case VHDL_math_tanh:
    case VHDL_math_asinh:
    case VHDL_math_acosh:
    case VHDL_math_atanh:
    case VHDL_math_mod:
    case VHDL_math_atan2:
    case VHDL_math_pow:
        return 1 ;
    default:
        break ;
    }
    return 0 ;
}

const char *
VhdlNode::EntityClassName(unsigned entity_class)
{
    switch (entity_class) {
    case VHDL_entity :          return "entity" ;
    case VHDL_architecture :    return "architecture" ;
    case VHDL_configuration :   return "configuration" ;
    case VHDL_procedure :       return "procedure" ;
    case VHDL_function :        return "function" ;
    case VHDL_package :         return "package" ;
    case VHDL_type :            return "type" ;
    case VHDL_subtype :         return "subtype" ;
    case VHDL_constant :        return "constant" ;
    case VHDL_signal :          return "signal" ;
    case VHDL_variable :        return "variable" ;
    case VHDL_component :       return "component" ;
    case VHDL_label :           return "label" ;
    case VHDL_literal :         return "literal" ;
    case VHDL_units :           return "units" ;
    case VHDL_group :           return "group" ;
    case VHDL_file :            return "file" ;
    // added for printing other values :
    case VHDL_generic :         return "generic" ;
    case VHDL_port :            return "port" ;
    case VHDL_pure :            return "pure" ;
    case VHDL_impure :          return "impure" ;
    case VHDL_in :              return "in" ;
    case VHDL_out :             return "out" ;
    case VHDL_inout :           return "inout" ;
    case VHDL_buffer :          return "buffer" ;
    case VHDL_linkage :         return "linkage" ;
    case VHDL_bus :             return "bus" ;
    case VHDL_register :        return "register" ;
    default : break ;
    }
    return "object" ; // Or whatever default
}

/****************************************************************/
/***************** Tree Node  ***********************************/
/****************************************************************/

VhdlTreeNode::VhdlTreeNode(linefile_type linefile)
  : VhdlNode(),
    _linefile(linefile)
{
    // If no linefile is given, take the one from the last syntax rule parsed
    // The constructor most likely applies to the the last syntax rule, so pick that info up.
    if (!_linefile) _linefile = vhdl_file::GetRuleLineFile() ;

    if (IsTickProtectedRtlRegion(1)) { // VHDL LRM 1076_2008 Section 24.1
        // This is protected RTL. Modify the _linefile to carry only filename
        // and 0 as the line number. This is a special linefile to denote the
        // protected RTL region
        const char *file_name = LineFile::GetFileName(_linefile) ;
        if (file_name) _linefile = LineFile::EncodeLineFile(file_name, 0) ;
    }
}

unsigned VhdlTreeNode::IsTickProtectedNode() const
{
    // VHDL LRM 1076_2008 Section 24.1
    // We reserve Linefile field state (file_name && !line_number) for protected nodes.
    return LineFile::IsProtectedLinefile(_linefile) ;
}

VhdlTreeNode::~VhdlTreeNode()
{
    DeleteComments(TakeComments()) ; // JJ: we need to free the memory allocated
}

unsigned VhdlTreeNode::GetVhdlAccent()
{
    return VHDL_ACCENT ;
}
VhdlInterfaceId *
VhdlTreeNode::CreateVhdlGeneric(const char* name)
{
     // Create VhdlInterfaceId for 'generic_name'
     VhdlInterfaceId *new_generic = new VhdlInterfaceId(Strings::save(name)) ;
     (void) VhdlTreeNode::_present_scope->Declare(new_generic) ;
     // Mark created id as generic (VhdlInterfaceId also created for port)
     new_generic->SetObjectKind(VHDL_generic) ;
     return (new_generic) ;
}
VhdlInterfaceId *
VhdlTreeNode::CreateVhdlPort(const char* name)
{
    // Create VhdlInterfaceId for 'name'
    VhdlInterfaceId *new_port = new VhdlInterfaceId(Strings::save(name)) ;
    (void) VhdlTreeNode::_present_scope->Declare(new_port) ;
    // Mark created id as port (VhdlInterfaceId also created for generic)
    new_port->SetObjectKind(VHDL_port) ;
    return (new_port) ;
}
VhdlRange *
VhdlTreeNode::CreateVhdlRange(verific_int64 left_bound, verific_int64 right_bound)
{
    VhdlRange* range = 0 ;
    VhdlInteger *left = new VhdlInteger(left_bound) ; // Create a VhdlInteger type
    VhdlInteger *right = new VhdlInteger(right_bound) ; // Create a VhdlInteger type
    unsigned port_dir =  (left_bound > right_bound) ? VHDL_downto : VHDL_to ;
    // VIPER #5152 : Always create range even for (0 to 0)
    range = new VhdlRange(left, port_dir, right) ; // create the range
    return range;
}
VhdlRange *
VhdlTreeNode::CreateVhdlRangeWithExpr(VhdlExpression* left, VhdlExpression* right)
{
    if (!left || !right) return 0 ;
    VhdlRange* range = 0 ;
    range = new VhdlRange(left, VHDL_to, right) ; // create the range
    return range;
}
unsigned
VhdlTreeNode::GetInputMode()
{
    return (VHDL_in) ;
}
unsigned
VhdlTreeNode::GetOutputMode()
{
    return (VHDL_out) ;
}
unsigned
VhdlTreeNode::GetInoutMode()
{
    return (VHDL_inout) ;
}
unsigned
VhdlTreeNode::GetEntityId()
{
    return (VHDL_entity) ;
}

void
VhdlTreeNode::AddComments(Array *comments)
{
    if (!comments) return ;

    // Create the global static map if not there:
    if (!_map_of_comment_arr) _map_of_comment_arr = new Map(POINTER_HASH) ;

    // Attempt to insert comments directly:
    if (_map_of_comment_arr->Insert(this, (void *)comments)) return ; // Done

    // Here, we failed to insert comments. This means there are existing 'comments' on this node.
    // Pick these up and append:
    Array *comment_array = GetComments() ;
    VERIFIC_ASSERT(comment_array) ; // should be there, or else Insert() would not have failed.

    // Append it into the existing array:
    comment_array->Append(comments) ;
    delete comments ; // Absorbed
}

Array *
VhdlTreeNode::GetComments() const
{
    if (!_map_of_comment_arr) return 0 ; // no comments anywhere
    return (Array *) _map_of_comment_arr->GetValue(this) ;
}

void
VhdlTreeNode::SetComments(Array *comments)
{
    if (!comments) return ;

    DeleteComments(TakeComments()) ;
    AddComments(comments) ;
}

Array *
VhdlTreeNode::TakeComments()
{
    if (!_map_of_comment_arr) return 0 ; // no comments anywhere

    Array *comments = (Array *)_map_of_comment_arr->GetValue(this) ;
    if (!comments) return 0 ; // no comments on this node

    // If there were comments on this node, then remove the node from the global map.
    (void) _map_of_comment_arr->Remove(this) ;

    if (!_map_of_comment_arr->Size()) {
        // Free the global map, if empty
        delete _map_of_comment_arr ;
        _map_of_comment_arr = 0 ;
    }

    return comments ;
}

/*static*/ void
VhdlTreeNode::ResetComments()
{
    delete _map_of_comment_arr ;
    _map_of_comment_arr = 0 ;
}

Array *
VhdlTreeNode::ProcessPostComments(Array *comments)
{
    if (!comments) return 0 ;

    unsigned i ;
    VhdlCommentNode *node ;
    FOREACH_ARRAY_ITEM(comments, i, node) {
        if (!node) continue ;
        // Take out ONLY the comments which have the same or less starting line number
        // as this statement's ending line number.
        // We assume these belong to 'this' statement.
        // Leave other comments in the array.

        unsigned comment_line_no = node->GetStartingLineNumber() ; // comments' starting line/file
        linefile_type prev_line_file = EndingLinefile() ; // my ending line/file
        unsigned prev_stmt_line_no = LineFile::GetLineNo(prev_line_file) ;
        if (comment_line_no <= prev_stmt_line_no) {
            Array *comment_arr = new Array (1) ;
            comment_arr->InsertLast(node) ;
            AddComments(comment_arr) ;
            comments->Insert(i, 0) ;
        }
    }

    return comments ;
}

///////////////////////////////////////////////////////////////////////////

/************************ Comment handling  ************************/

// Comment node operations
VhdlCommentNode::VhdlCommentNode(linefile_type starting_linefile) :
    VhdlTreeNode(starting_linefile),
    _str(0)
{
    // Comment node initialized with 'starting' linefile rather than with 'ending' linefile.
}

VhdlCommentNode::~VhdlCommentNode()
{
    Strings::free(_str) ;
}

// Appends string with the existing comment.
void VhdlCommentNode::AppendComment(const char *str)
{
    // If we are from pragma to short_comment state
    // then we might have _str left from previous
    // comment. Then we get a memory leak here!
    if (_str) {
        //Warning("MEMORY LEAK:\n  Previous: %s  Current: %s", _str, str) ;
        Strings::free(_str) ;
    }

    // Store the argument string with the existing string.
    _str = Strings::save(str) ;
}

// Return the comment string.
const char *VhdlCommentNode::GetCommentString() const
{
    return _str ;
}

// Return the starting line number.
unsigned VhdlCommentNode::GetStartingLineNumber() const
{
    return LineFile::GetLineNo(StartingLinefile()) ;
}

void
VhdlCommentNode::AdjustClosingLinefile(unsigned right_line, unsigned right_col) const
{
    // Check that we can adjust the linefile correctly:
    if (!right_line || !right_col) return ;
    linefile_type com_lf = Linefile() ;
    if (!com_lf) return ; // No existing linefile info!

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Adjust only the right side of the linefile:
    com_lf->SetRightLine(right_line) ;
    com_lf->SetRightCol(right_col) ;
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
}

// This is called from destructors of VhdlDesignUnit, VhdlDeclaration, VhdlStatement
// and VhdlDiscreteRange. This method deletes an array of comment nodes.
void VhdlNode::DeleteComments(Array *comments)
{
    if (!comments) return ;

    // Delete these comments and the array they are in.
    unsigned i ;
    VhdlCommentNode *node ;
    FOREACH_ARRAY_ITEM(comments, i, node) delete node ;
    delete comments ;
}

/************************ Error reporting  ******************************/

// New style messages : with varargs and message id :
void
VhdlTreeNode::Comment(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_COMMENT, GetMessageId(format), Linefile(), format, args) ;
    va_end(args) ;
}

void
VhdlTreeNode::Error(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_ERROR, GetMessageId(format), Linefile(), format, args) ;
    va_end(args) ;
}

void
VhdlTreeNode::Warning(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_WARNING, GetMessageId(format), Linefile(), format, args) ;
    va_end(args) ;
}

void
VhdlTreeNode::Info(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_INFO, GetMessageId(format), Linefile(), format, args) ;
    va_end(args) ;
}

/************************ PrettyPrint support ***********************/

// 80 spaces
static const char *spaces = "                                                                                " ;
const char *
VhdlTreeNode::PrintLevel(unsigned level)
{
    // Return a pointer to a string of level*4 space characters
    // Don't overflow on the fixed-length spaces constant
    unsigned space = (unsigned)Strings::len(spaces) ;
    if (4*level > space) return spaces ;
    return spaces+space-4*level ;
}

/************************ Tree Labeling Support *********************/

void VhdlTreeNode::ClosingLabel(VhdlDesignator *id)
{
    if (!id) return ;

    // Check closing label 'id' against earlier set opening label

    // Delete 'id' (Absorb in this function)

    // NOTE : This function works for all named / labeled declarations or statements,
    // To check if a closing name (id) matches the definition of the entity.
    //
    // It looks in the scope to find the match.
    // Therefore, it does not work for library units (library units are
    // not in the scope).
    // So, library units are handled as special case in VhdlUnit.cpp with
    // VhdlDesignUnit::ClosingLabel().
    //
    // Same thing for subprogram body : since it is overloadable, there is
    // no guarantee that we will find the right subprogram id. So we overloaded
    // in VhdlSubprogramBody::ClosingLabel()
    //
    // Come to think of it : we should probably overload this function for
    // all applicable classes. That will also remove the need to look in the scope.
    VhdlIdDef *label = id->FindSingleObject() ;

    // FIX ME : This VhdlTreeNode::ClosingLabel() should not be needed :
    // Checking closing labels already required specific virtual overloads
    // for design units and subprograms. Only records, components and statements will
    // now end-up here. If we would write closing label checks for these
    // also, we might as well add that (checking) code to the constructor
    // of the statement/record/designunit/component etc.

    // The local 'scope' of this 'label' should be the scope of the
    // tree that it closes. Check that :

    VhdlScope *label_scope = (label) ? label->LocalScope() : 0 ;
    VhdlScope *tree_scope = LocalScope() ;
    if (label && (label_scope != tree_scope)) {
        // Label points to some other construct but this one
        id->Error("%s is not a label of this statement", id->Name()) ;
    }

    SetClosingLabel(id) ; // VIPER #6666 (id may get deleted here)
    // Cest Ca. I love scopes, sometimes.
}

void VhdlTreeNode::SetClosingLabel(VhdlDesignator *label) // VIPER #6666
{
    delete label ; // No place to store it, delete now.
}

/************************ Virtual Catchers   ***********************/

VhdlScope *VhdlTreeNode::LocalScope() const { return 0 ; } // default

/************************ pragmas  ***********************/

unsigned
VhdlNode::PragmaFunction(const char *function)
{
    if (!function) return 0 ;

    // Synopsys, Synergy, Ambit  pragma's and Exemplar directives
    if (!_pragma_table) {
        _pragma_table = new Map(STRING_HASH_CASE_INSENSITIVE, 181) ; // Give reasonable initial size

        // Exemplar directives are in string form
        // Also used for Ambit 'builtin-operator' pragma
        _pragma_table->Insert("\"uminus\"",(void*)VHDL_UMINUS) ;
        _pragma_table->Insert("\"add\"",(void*)VHDL_PLUS) ;
        _pragma_table->Insert("\"sub\"",(void*)VHDL_MINUS) ;
        _pragma_table->Insert("\"mult\"",(void*)VHDL_STAR) ;
        _pragma_table->Insert("\"div\"",(void*)VHDL_SLASH) ;
        _pragma_table->Insert("\"abs\"",(void*)VHDL_abs) ;
        _pragma_table->Insert("\"mod\"",(void*)VHDL_mod) ;
        _pragma_table->Insert("\"rem\"",(void*)VHDL_rem) ;
        _pragma_table->Insert("\"power\"",(void*)VHDL_EXPONENT) ;

        _pragma_table->Insert("\"feed_through\"",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("\"wired_three_state\"",(void*)VHDL_WIRED_THREE_STATE) ;

        _pragma_table->Insert("\"not\"",(void*)VHDL_not) ;
        _pragma_table->Insert("\"and\"",(void*)VHDL_and) ;
        _pragma_table->Insert("\"nand\"",(void*)VHDL_nand) ;
        _pragma_table->Insert("\"or\"",(void*)VHDL_or) ;
        _pragma_table->Insert("\"nor\"",(void*)VHDL_nor) ;
        _pragma_table->Insert("\"xor\"",(void*)VHDL_xor) ;
        _pragma_table->Insert("\"xnor\"",(void*)VHDL_xnor) ;

        _pragma_table->Insert("\"reduce_and\"",(void*)VHDL_REDAND) ;
        _pragma_table->Insert("\"reduce_nand\"",(void*)VHDL_REDNAND) ;
        _pragma_table->Insert("\"reduce_or\"",(void*)VHDL_REDOR) ;
        _pragma_table->Insert("\"reduce_nor\"",(void*)VHDL_REDNOR) ;
        _pragma_table->Insert("\"reduce_xor\"",(void*)VHDL_REDXOR) ;
        _pragma_table->Insert("\"reduce_xnor\"",(void*)VHDL_REDXNOR) ;

        _pragma_table->Insert("\"eq\"",(void*)VHDL_EQUAL) ;
        _pragma_table->Insert("\"neq\"",(void*)VHDL_NEQUAL) ;
        _pragma_table->Insert("\"lt\"",(void*)VHDL_STHAN) ;
        _pragma_table->Insert("\"gt\"",(void*)VHDL_GTHAN) ;
        _pragma_table->Insert("\"lte\"",(void*)VHDL_SEQUAL) ;
        _pragma_table->Insert("\"gte\"",(void*)VHDL_GEQUAL) ;

        _pragma_table->Insert("\"sll\"",(void*)VHDL_sll) ;
        _pragma_table->Insert("\"srl\"",(void*)VHDL_srl) ;
        _pragma_table->Insert("\"sla\"",(void*)VHDL_sla) ;
        _pragma_table->Insert("\"sra\"",(void*)VHDL_sra) ;
        _pragma_table->Insert("\"rol\"",(void*)VHDL_rol) ;
        _pragma_table->Insert("\"ror\"",(void*)VHDL_ror) ;

        _pragma_table->Insert("\"pull_up\"",(void*)VHDL_PULLUP) ;
        _pragma_table->Insert("\"pull_dn\"",(void*)VHDL_PULLDOWN) ;
        _pragma_table->Insert("\"trst_mem\"",(void*)VHDL_TRSTMEM) ;
        // ignore these Exemplar directives
        _pragma_table->Insert("directive",0) ;
        _pragma_table->Insert("directives",0) ;

        // Synopsys pragmas
        // Synopsys logic functions
        //CARBON_BEGIN
        // TODO: The #define values used here are Carbon specific, and are defined in vhdl.y.
        // It looks like the wrong enum/macro is probably being used here. 
        _pragma_table->Insert("syn_resize", (void*)VHDL_resize);
        _pragma_table->Insert("syn_uns_resize", (void*)VHDL_uns_resize);
        _pragma_table->Insert("syn_signed_mult", (void*)VHDL_signed_mult);
        _pragma_table->Insert("syn_unsigned_mult", (void*)VHDL_unsigned_mult);
        _pragma_table->Insert("syn_numeric_signed_mult", (void*)VHDL_numeric_signed_mult);
        _pragma_table->Insert("syn_numeric_unsigned_mult", (void*)VHDL_numeric_unsigned_mult);
        _pragma_table->Insert("syn_numeric_sign_div", (void*)VHDL_numeric_sign_div);
        _pragma_table->Insert("syn_numeric_uns_div", (void*)VHDL_numeric_uns_div);
        _pragma_table->Insert("syn_minus", (void*)VHDL_MINUS);
        _pragma_table->Insert("syn_numeric_minus", (void*)VHDL_numeric_minus);
        _pragma_table->Insert("syn_numeric_unary_minus", (void*)VHDL_numeric_unary_minus);
        _pragma_table->Insert("syn_plus", (void*)VHDL_PLUS);
        _pragma_table->Insert("syn_numeric_plus", (void*)VHDL_numeric_plus);
        _pragma_table->Insert("syn_abs", (void*)VHDL_abs);
        _pragma_table->Insert("syn_is_uns_less", (void*)VHDL_is_uns_less);
        _pragma_table->Insert("syn_is_uns_gt", (void*)VHDL_is_uns_gt);
        _pragma_table->Insert("syn_is_gt", (void*)VHDL_is_gt);
        _pragma_table->Insert("syn_is_less", (void*)VHDL_is_less);
        _pragma_table->Insert("syn_is_uns_gt_or_equal", (void*)VHDL_is_uns_gt_or_equal);
        _pragma_table->Insert("syn_is_uns_less_or_equal", (void*)VHDL_is_uns_less_or_equal);
        _pragma_table->Insert("syn_is_gt_or_equal", (void*)VHDL_is_gt_or_equal);
        _pragma_table->Insert("syn_is_less_or_equal", (void*)VHDL_is_less_or_equal);
        _pragma_table->Insert("syn_and_reduce", (void*)VHDL_and_reduce);
        _pragma_table->Insert("syn_or_reduce", (void*)VHDL_or_reduce);
        _pragma_table->Insert("syn_xor_reduce", (void*)VHDL_xor_reduce);
        _pragma_table->Insert("syn_sll",(void*)VHDL_sll) ;
        _pragma_table->Insert("syn_srl",(void*)VHDL_srl) ;
        _pragma_table->Insert("syn_sla",(void*)VHDL_sla) ;
        _pragma_table->Insert("syn_sra",(void*)VHDL_sra) ;
        _pragma_table->Insert("syn_rol",(void*)VHDL_rol) ;
        _pragma_table->Insert("syn_ror",(void*)VHDL_ror) ;
        _pragma_table->Insert("syn_numeric_mod",(void*)VHDL_numeric_mod) ;
        //CARBON_END

        _pragma_table->Insert("syn_and",(void*)VHDL_and) ;
        _pragma_table->Insert("syn_or",(void*)VHDL_or) ;
        _pragma_table->Insert("syn_xor",(void*)VHDL_xor) ;
        _pragma_table->Insert("syn_nand",(void*)VHDL_nand) ;
        _pragma_table->Insert("syn_nor",(void*)VHDL_nor) ;
        _pragma_table->Insert("syn_xnor",(void*)VHDL_xnor) ;

        _pragma_table->Insert("syn_buf",(void*)VHDL_buf) ;
        _pragma_table->Insert("syn_not",(void*)VHDL_not) ;

        _pragma_table->Insert("syn_eql",(void*)VHDL_EQUAL) ;
        _pragma_table->Insert("syn_neq",(void*)VHDL_NEQUAL) ;

        // Synopsys feed-throughs
        _pragma_table->Insert("syn_feed_thru",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("syn_feed_through",(void*)VHDL_FEEDTHROUGH) ;            // CARBON_ADDED
        _pragma_table->Insert("syn_integer_to_signed",(void*)VHDL_i_to_s) ;            // CARBON_CHANGED
        _pragma_table->Insert("syn_integer_to_unsigned",(void*)VHDL_s_to_u) ;          // CARBON_CHANGED
        _pragma_table->Insert("syn_integer_to_bit_vector",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("syn_signed_to_integer",(void*)VHDL_s_to_i) ;            // CARBON_CHANGED
        _pragma_table->Insert("syn_unsigned_to_integer",(void*)VHDL_u_to_i) ;          // CARBON_CHANGED
        _pragma_table->Insert("syn_zero_extend",(void*)VHDL_u_to_u) ;                  // CARBON_CHANGED
        _pragma_table->Insert("syn_sign_extend",(void*)VHDL_s_xt) ;                    // CARBON_CHANGED

        // Synopsys arithmetics
        _pragma_table->Insert("add_tc_op",(void*)VHDL_PLUS) ;
        _pragma_table->Insert("add_uns_op",(void*)VHDL_PLUS) ;
        _pragma_table->Insert("sub_tc_op",(void*)VHDL_MINUS) ;
        _pragma_table->Insert("sub_uns_op",(void*)VHDL_MINUS) ;
        _pragma_table->Insert("mult_tc_op",(void*)VHDL_STAR) ;
        _pragma_table->Insert("mult_uns_op",(void*)VHDL_STAR) ;
        _pragma_table->Insert("div_tc_op",(void*)VHDL_SLASH) ;  // Not sure if this is supported by Synopsys
        _pragma_table->Insert("div_uns_op",(void*)VHDL_SLASH) ; // Not sure if this is supported by Synopsys

        _pragma_table->Insert("lt_tc_op",(void*)VHDL_STHAN) ;
        _pragma_table->Insert("lt_uns_op",(void*)VHDL_STHAN) ;
        _pragma_table->Insert("leq_tc_op",(void*)VHDL_SEQUAL) ;
        _pragma_table->Insert("leq_uns_op",(void*)VHDL_SEQUAL) ;
        _pragma_table->Insert("gt_tc_op",(void*)VHDL_GTHAN) ;   // Not sure if this is supported by Synopsys
        _pragma_table->Insert("gt_uns_op",(void*)VHDL_GTHAN) ;  // Not sure if this is supported by Synopsys
        _pragma_table->Insert("gte_tc_op",(void*)VHDL_GEQUAL) ; // Not sure if this is supported by Synopsys
        _pragma_table->Insert("gte_uns_op",(void*)VHDL_GEQUAL) ;// Not sure if this is supported by Synopsys

        // Synopsys resolution methods (here since VIPER 3655)
        _pragma_table->Insert("wired_and",(void*)VHDL_REDAND) ; // No new token for wired_and, since it's behavior is similar to reduction.
        _pragma_table->Insert("wired_or",(void*)VHDL_REDOR) ; // No new token for wired_and, since it's behavior is similar to reduction.
        _pragma_table->Insert("three_state",(void*)VHDL_WIRED_THREE_STATE) ; // No new token for three_state, since it's behavior is similar to wired_three_state.

        // Ambit feed-throughs
        // Not sure which one of these is actually used
        _pragma_table->Insert("builtin_feed_thru",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("builtin_integer_to_signed",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("builtin_integer_to_unsigned",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("builtin_signed_to_integer",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("builtin_unsigned_to_integer",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("builtin_zero_extend",(void*)VHDL_FEEDTHROUGH) ;
        _pragma_table->Insert("builtin_sign_extend",(void*)VHDL_FEEDTHROUGH) ;

        // Ambit's most general type-converter :
        _pragma_table->Insert("builtin_type_conversion",(void*)VHDL_FEEDTHROUGH) ;

        // Ambit's shifters
        _pragma_table->Insert("builtin_sll",(void*)VHDL_sll) ;
        _pragma_table->Insert("builtin_srl",(void*)VHDL_srl) ;
        _pragma_table->Insert("builtin_sla",(void*)VHDL_sla) ;
        _pragma_table->Insert("builtin_sra",(void*)VHDL_sra) ;
        _pragma_table->Insert("builtin_rol",(void*)VHDL_rol) ;
        _pragma_table->Insert("builtin_ror",(void*)VHDL_ror) ;

        // Ambit's boolean operators
        _pragma_table->Insert("builtin_not",(void*)VHDL_not) ;
        _pragma_table->Insert("builtin_buf",(void*)VHDL_buf) ;
        _pragma_table->Insert("builtin_and",(void*)VHDL_and) ;
        _pragma_table->Insert("builtin_nand",(void*)VHDL_nand) ;
        _pragma_table->Insert("builtin_or",(void*)VHDL_or) ;
        _pragma_table->Insert("builtin_nor",(void*)VHDL_nor) ;
        _pragma_table->Insert("builtin_xor",(void*)VHDL_xor) ;
        _pragma_table->Insert("builtin_xnor",(void*)VHDL_xnor) ;

        // Ambit's reduction operators
        _pragma_table->Insert("builtin_and_reduce",(void*)VHDL_REDAND) ;
        _pragma_table->Insert("builtin_nand_reduce",(void*)VHDL_REDNAND) ;
        _pragma_table->Insert("builtin_or_reduce",(void*)VHDL_REDOR) ;
        _pragma_table->Insert("builtin_nor_reduce",(void*)VHDL_REDNOR) ;
        _pragma_table->Insert("builtin_xor_reduce",(void*)VHDL_REDXOR) ;
        _pragma_table->Insert("builtin_xnor_reduce",(void*)VHDL_REDXNOR) ;

        // Ambit's equal/not-equal
        _pragma_table->Insert("builtin_eq",(void*)VHDL_EQUAL) ;
        _pragma_table->Insert("builtin_neq",(void*)VHDL_NEQUAL) ; // not sure if correct

        // Ambit's arithmetics
        _pragma_table->Insert("builtin_uns_add",(void*)VHDL_PLUS) ;
        _pragma_table->Insert("builtin_uns_sub",(void*)VHDL_MINUS) ;
        _pragma_table->Insert("builtin_uns_mult",(void*)VHDL_STAR) ;
        _pragma_table->Insert("builtin_uns_div",(void*)VHDL_SLASH) ;

        // Ambit's sick support of clocks
        _pragma_table->Insert("builtin_rising_edge",(void*)VHDL_rising_edge) ;
        _pragma_table->Insert("builtin_falling_edge",(void*)VHDL_falling_edge) ;

        // Ambit's idea : figure the function from the name :
        _pragma_table->Insert("builtin_operator",(void*)VHDL_builtin_operator) ;

        // Ambit 'builtin_operator' include the arithmetic operators :
        _pragma_table->Insert("\"+\"",(void*)VHDL_PLUS) ;
        _pragma_table->Insert("\"-\"",(void*)VHDL_MINUS) ;
        _pragma_table->Insert("\"*\"",(void*)VHDL_STAR) ;
        _pragma_table->Insert("\"/\"",(void*)VHDL_SLASH) ;
        _pragma_table->Insert("\"**\"",(void*)VHDL_EXPONENT) ;

        // Ambit 'builtin_operator' include the relational operators :
        _pragma_table->Insert("\"=\"",(void*)VHDL_EQUAL) ;
        _pragma_table->Insert("\"/=\"",(void*)VHDL_NEQUAL) ;
        _pragma_table->Insert("\"<\"",(void*)VHDL_STHAN) ;
        _pragma_table->Insert("\">\"",(void*)VHDL_GTHAN) ;
        _pragma_table->Insert("\"<=\"",(void*)VHDL_SEQUAL) ;
        _pragma_table->Insert("\">=\"",(void*)VHDL_GEQUAL) ;

        // More Ambit functions labeled as 'builtin_operator' :
        _pragma_table->Insert("unsigned_equal",(void*)VHDL_EQUAL) ;
        _pragma_table->Insert("signed_equal",(void*)VHDL_EQUAL) ;
        // _pragma_table->Insert("unsigned_nequal",(void*)VHDL_NEQUAL) ; // Not sure if exists
        _pragma_table->Insert("unsigned_less",(void*)VHDL_STHAN) ;
        _pragma_table->Insert("signed_less",(void*)VHDL_STHAN) ;
        // _pragma_table->Insert("unsigned_more",(void*)VHDL_GTHAN) ; // Not sure if exists
        _pragma_table->Insert("unsigned_less_or_equal",(void*)VHDL_SEQUAL) ;
        _pragma_table->Insert("signed_less_or_equal",(void*)VHDL_SEQUAL) ;
        // _pragma_table->Insert("unsigned_more_or_equal",(void*)VHDL_GEQUAL) ; // Not sure if exists

        // Ambit's pragma's that I want to ignore for now.
        _pragma_table->Insert("propagate_label_to",(void*)0) ;
        _pragma_table->Insert("label",(void*)0) ;
        _pragma_table->Insert("off",(void*)0) ;
        _pragma_table->Insert("on",(void*)0) ;
        _pragma_table->Insert("resolution",(void*)0) ;

        // Verific pragma's to handle textio functions :
        _pragma_table->Insert("readline",(void*)VHDL_readline) ;
        _pragma_table->Insert("writeline",(void*)VHDL_writeline) ;
        _pragma_table->Insert("read",(void*)VHDL_read) ;
        _pragma_table->Insert("sread",(void*)VHDL_sread) ;
        _pragma_table->Insert("write",(void*)VHDL_write) ;
        _pragma_table->Insert("hread",(void*)VHDL_hread) ;
        _pragma_table->Insert("hwrite",(void*)VHDL_hwrite) ;
        _pragma_table->Insert("oread",(void*)VHDL_oread) ;
        _pragma_table->Insert("owrite",(void*)VHDL_owrite) ;

        // Extension of for attributes (set with 'synthesis_return' attribute)
        _pragma_table->Insert("\"readline\"",(void*)VHDL_readline) ;
        _pragma_table->Insert("\"writeline\"",(void*)VHDL_writeline) ;
        _pragma_table->Insert("\"read\"",(void*)VHDL_read) ;
        _pragma_table->Insert("\"sread\"",(void*)VHDL_sread) ;
        _pragma_table->Insert("\"write\"",(void*)VHDL_write) ;
        _pragma_table->Insert("\"hread\"",(void*)VHDL_hread) ;
        _pragma_table->Insert("\"hwrite\"",(void*)VHDL_hwrite) ;
        _pragma_table->Insert("\"oread\"",(void*)VHDL_oread) ;
        _pragma_table->Insert("\"owrite\"",(void*)VHDL_owrite) ;

        // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
        _pragma_table->Insert("\"verific_ceil\"",(void*)VHDL_math_ceil) ;
        _pragma_table->Insert("\"verific_floor\"",(void*)VHDL_math_floor) ;
        _pragma_table->Insert("\"verific_fmod\"",(void*)VHDL_math_mod) ;
        _pragma_table->Insert("\"verific_sqrt\"",(void*)VHDL_math_sqrt) ;
        _pragma_table->Insert("\"verific_cbrt\"",(void*)VHDL_math_cbrt) ;
        _pragma_table->Insert("\"verific_pow\"",(void*)VHDL_math_pow) ;
        _pragma_table->Insert("\"verific_exp\"",(void*)VHDL_math_exp) ;
        _pragma_table->Insert("\"verific_log\"",(void*)VHDL_math_log) ;
        _pragma_table->Insert("\"verific_log2\"",(void*)VHDL_math_log2) ;
        _pragma_table->Insert("\"verific_log10\"",(void*)VHDL_math_log10) ;
        _pragma_table->Insert("\"verific_sin\"",(void*)VHDL_math_sin) ;
        _pragma_table->Insert("\"verific_cos\"",(void*)VHDL_math_cos) ;
        _pragma_table->Insert("\"verific_tan\"",(void*)VHDL_math_tan) ;
        _pragma_table->Insert("\"verific_asin\"",(void*)VHDL_math_asin) ;
        _pragma_table->Insert("\"verific_acos\"",(void*)VHDL_math_acos) ;
        _pragma_table->Insert("\"verific_atan\"",(void*)VHDL_math_atan) ;
        _pragma_table->Insert("\"verific_atan2\"",(void*)VHDL_math_atan2) ;
        _pragma_table->Insert("\"verific_sinh\"",(void*)VHDL_math_sinh) ;
        _pragma_table->Insert("\"verific_cosh\"",(void*)VHDL_math_cosh) ;
        _pragma_table->Insert("\"verific_tanh\"",(void*)VHDL_math_tanh) ;
        _pragma_table->Insert("\"verific_asinh\"",(void*)VHDL_math_asinh) ;
        _pragma_table->Insert("\"verific_acosh\"",(void*)VHDL_math_acosh) ;
        _pragma_table->Insert("\"verific_atanh\"",(void*)VHDL_math_atanh) ;

        // Verific pragma, to ignore a procedure or function
        _pragma_table->Insert("ignore_subprogram",(void*)VHDL_IGNORE_SUBPROGRAM) ;
    }

    // Go via long for 64 bit compile
    unsigned long rlong = (unsigned long)_pragma_table->GetValue(function) ;
    unsigned r = (unsigned)rlong;
    VERIFIC_ASSERT(r==rlong) ;
    return r ;
}

unsigned VhdlNode::PragmaKnown(const char *function)
{
    if (!_pragma_table) (void) PragmaFunction(function) ;
    VERIFIC_ASSERT(_pragma_table) ;
    return (_pragma_table->GetItem(function)) ? 1 : 0 ;
}

// VIPER #3166 : Return 1 if the operator name is a valid vhdl operator as dictated by LRM section 7.2
unsigned VhdlNode::IsValidOperator(const char *op)
{
    if (!op) return 0 ;

    if (!_operators) {
        _operators = new Set(STRING_HASH_CASE_INSENSITIVE, 37) ;
        (void) _operators->Insert("\"and\"") ;
        (void) _operators->Insert("\"or\"") ;
        (void) _operators->Insert("\"nand\"") ;
        (void) _operators->Insert("\"nor\"") ;
        (void) _operators->Insert("\"xor\"") ;
        (void) _operators->Insert("\"xnor\"") ;
        (void) _operators->Insert("\"=\"") ;
        (void) _operators->Insert("\"/=\"") ;
        (void) _operators->Insert("\"<\"") ;
        (void) _operators->Insert("\"<=\"") ;
        (void) _operators->Insert("\">\"") ;
        (void) _operators->Insert("\">=\"") ;
        (void) _operators->Insert("\"sll\"") ;
        (void) _operators->Insert("\"srl\"") ;
        (void) _operators->Insert("\"sla\"") ;
        (void) _operators->Insert("\"sra\"") ;
        (void) _operators->Insert("\"rol\"") ;
        (void) _operators->Insert("\"ror\"") ;
        (void) _operators->Insert("\"+\"") ;
        (void) _operators->Insert("\"-\"") ;
        (void) _operators->Insert("\"&\"") ;
        (void) _operators->Insert("\"*\"") ;
        (void) _operators->Insert("\"/\"") ;
        (void) _operators->Insert("\"mod\"") ;
        (void) _operators->Insert("\"rem\"") ;
        (void) _operators->Insert("\"**\"") ;
        (void) _operators->Insert("\"abs\"") ;
        (void) _operators->Insert("\"not\"") ;
        (void) _operators->Insert("\",\"") ;
        // Following are new operators included in 1076-2008
        (void) _operators->Insert("\"??\"") ;
        (void) _operators->Insert("\"?=\"") ;
        (void) _operators->Insert("\"?/=\"") ;
        (void) _operators->Insert("\"?>\"") ;
        (void) _operators->Insert("\"?<\"") ;
        (void) _operators->Insert("\"?>=\"") ;
        (void) _operators->Insert("\"?<=\"") ;
    }

    return (_operators->GetItem(op)) ? 1 : 0 ;
}

/*static*/ void
VhdlNode::ResetOperatorsTable()
{
    delete _operators ;
    _operators = 0 ;
}

/*static*/ void
VhdlNode::ResetPragmaTables()
{
    delete _pragma_table ;
    _pragma_table = 0 ;

    delete _pragma_signed_table ;
    _pragma_signed_table = 0 ;
}

unsigned VhdlNode::PragmaSigned(const char *function)
{
    if (!function) return 0 ;

    // Check if this (Synopsys) pragma interprets incoming signals as signed values
    if (!_pragma_signed_table) {
        _pragma_signed_table = new Set(STRING_HASH_CASE_INSENSITIVE,23) ;

        // Signed Synopsys feed-throughs
        _pragma_signed_table->Insert("syn_integer_to_signed") ;
        _pragma_signed_table->Insert("syn_signed_to_integer") ;
        _pragma_signed_table->Insert("syn_sign_extend") ;

        // Signed Synopsys arithmetics
        _pragma_signed_table->Insert("add_tc_op") ;
        _pragma_signed_table->Insert("sub_tc_op") ;
        _pragma_signed_table->Insert("mult_tc_op") ;
        _pragma_signed_table->Insert("div_tc_op") ; // Not sure if this is supported by Synopsys
        _pragma_signed_table->Insert("lt_tc_op") ;
        _pragma_signed_table->Insert("leq_tc_op") ;
        _pragma_signed_table->Insert("gt_tc_op") ;  // Not sure if this is supported by Synopsys
        _pragma_signed_table->Insert("gte_tc_op") ; // Not sure if this is supported by Synopsys

        // Signed Ambit builtin operations :
        _pragma_signed_table->Insert("signed_equal") ;
        _pragma_signed_table->Insert("signed_less") ;
        _pragma_signed_table->Insert("signed_less_or_equal") ;
    }
    return (_pragma_signed_table->GetItem(function)) ? 1 : 0 ;
}

const char *
VhdlNode::GetMessageId(const char *msg)
{
    if (!msg) return 0 ;

    // find a message ID from a message format string
    // Return 0 if not found.

    if (!_msgs) {
        _msgs = new Map(STRING_HASH, 613) ; // Table size : choose prime number for good hashing. Enough initial storage until message VHDL-1767.

//CARBON_BEGIN
        // put carbon specific messages here (at the top of the list to reduce merge conflicts), all carbon specifc messages must use IDs in the range 9000-9999
        _msgs->Insert("WAIT statement inside a subprogram body is not supported","VHDL-9000");  // 2/12/2014 added:, perhaps should have used "VHDL-1295"
//CARBON_END

// retired         : _msgs->Insert("access type object %s does not denote a record", "VHDL-1000") ;
        _msgs->Insert("access type or file type constraint cannot have a resolution function", "VHDL-1001") ;
        _msgs->Insert("access type %s does not access a %s", "VHDL-1002") ;
// retired         : _msgs->Insert("access type %s does not denote a record", "VHDL-1003") ;
        _msgs->Insert("aggregate already has a choice in this range", "VHDL-1004") ;
        _msgs->Insert("a homograph of %s is already declared in this region", "VHDL-1005") ;
        _msgs->Insert("alias target name does not have same number of elements as alias subtype", "VHDL-1006") ;
        _msgs->Insert("allocator is not synthesizable", "VHDL-1007") ;
        _msgs->Insert("ambiguous name", "VHDL-1008") ;
        _msgs->Insert("ambiguous selected name", "VHDL-1009") ;
        _msgs->Insert("analyzing architecture %s", "VHDL-1010") ;
        _msgs->Insert("analyzing configuration %s", "VHDL-1011") ;
        _msgs->Insert("analyzing entity %s", "VHDL-1012") ;
        _msgs->Insert("analyzing package body %s", "VHDL-1013") ;
        _msgs->Insert("analyzing package %s", "VHDL-1014") ;
        _msgs->Insert("architecture %s not found in entity %s", "VHDL-1015") ;
        _msgs->Insert("array element type cannot be unconstrained", "VHDL-1016") ;
// retired 04/02   : _msgs->Insert("array %s expects %d index argument(s)", "VHDL-1017") ;
        _msgs->Insert("array type %s expects %d index argument(s)", "VHDL-1018") ;
        _msgs->Insert("assignment ignored", "VHDL-1019") ;
        _msgs->Insert("attempt to divide by 0", "VHDL-1020") ;
        _msgs->Insert("attribute %s does not return type %s", "VHDL-1021") ;
        _msgs->Insert("attribute %s expects an argument", "VHDL-1022") ;
        _msgs->Insert("attribute %s illegal in expression", "VHDL-1023") ;
        _msgs->Insert("attribute %s index type should be integer", "VHDL-1024") ;
        _msgs->Insert("attribute %s on multiple bits is not synthesizable", "VHDL-1025") ;
// retired 04/02   : _msgs->Insert("bit-string is not a %s", "VHDL-1026") ;
        _msgs->Insert("cannot assign to constant %s", "VHDL-1027") ;
        _msgs->Insert("cannot assign to non-object %s", "VHDL-1028") ;
        _msgs->Insert("cannot convert type %s to type %s", "VHDL-1029") ;
// retired 4/13/05 : _msgs->Insert("cannot elaborate entity %s since it has uninitialized generics", "VHDL-1030") ;
        _msgs->Insert("cannot execute unknown pragma for function %s", "VHDL-1031") ;
        _msgs->Insert("cannot find port %s on cell %s", "VHDL-1032") ;
        _msgs->Insert("cannot read from 'out' object %s ; use 'buffer' or 'inout'", "VHDL-1033") ;
        _msgs->Insert("magnitude comparator on user-encoded values can create simulation-synthesis differences", "VHDL-1034") ;
        _msgs->Insert("case expression type %s is not a character array type", "VHDL-1035") ;
        _msgs->Insert("case expression type %s is not a discrete or 1-dimensional character array type", "VHDL-1036") ;
        _msgs->Insert("case statement does not cover all choices. 'others' clause is needed", "VHDL-1037") ;
        _msgs->Insert("case-statement with edge or event expression not supported for synthesis ; use an if-statement instead", "VHDL-1038") ;
        _msgs->Insert("character '%c' is not in element type %s", "VHDL-1039") ;
        _msgs->Insert("character %s is not in type %s", "VHDL-1040") ;
        _msgs->Insert("choice ignored", "VHDL-1041") ;
        _msgs->Insert("choice is not constant", "VHDL-1042") ;
        _msgs->Insert("choice must be a discrete range", "VHDL-1043") ;
// retired 08/06 (replaced by 1048) :       _msgs->Insert("choice %s does not belong to the index subtype", "VHDL-1044") ;
        _msgs->Insert("choice %s is already covered", "VHDL-1045") ;
// retired 04/02   : _msgs->Insert("choice %s is already covered in the aggregate", "VHDL-1046") ;
// retired 04/02   : _msgs->Insert("choice %s is already covered in this aggregate", "VHDL-1047") ;
        _msgs->Insert("choice %s is out of range %s for the index subtype", "VHDL-1048") ;
        _msgs->Insert("choice %s should have %d elements", "VHDL-1049") ;
        _msgs->Insert("choice with meta-value %s is ignored for synthesis", "VHDL-1050") ;
        _msgs->Insert("choice with meta-values (X,Z etc) ignored", "VHDL-1051") ;
        _msgs->Insert("%d definitions of operator %s match here", "VHDL-1052") ;
        _msgs->Insert("digit '%c' is larger than base in %s", "VHDL-1053") ;
        _msgs->Insert("edge or event as condition for a return, exit or next statement is not supported", "VHDL-1054") ;
// retired 04/02   : _msgs->Insert("element %s is not in record %s", "VHDL-1055") ;
        _msgs->Insert("element %s is not in record type %s", "VHDL-1056") ;
        _msgs->Insert("entity aspect %s does not denote a entity or configuration", "VHDL-1057") ;
        _msgs->Insert("entity should be in present work library", "VHDL-1058") ;
        _msgs->Insert("entity %s is not yet compiled", "VHDL-1059") ;
// retired 4/13/05 : _msgs->Insert("entity %s not found in the work library", "VHDL-1060") ;
        _msgs->Insert("enum_encoding attribute specifies incorrect number of valid encoding values for type %s ; encoding ignored", "VHDL-1061") ;
// retired 4/14/08 (replaced by 1061) :        _msgs->Insert("enum_encoding attribute specifies too many literals for type %s", "VHDL-1062") ;
        _msgs->Insert("enum_encoding elements should all have the same length", "VHDL-1063") ;
        _msgs->Insert("enumeration value %s is already declared in type %s", "VHDL-1064") ;
// retired         : _msgs->Insert("Error: state stack now", "VHDL-1065") ;
        _msgs->Insert("executing configuration %s", "VHDL-1066") ;
        _msgs->Insert("executing %s(%s)", "VHDL-1067") ;
        _msgs->Insert("execution of entity %s failed", "VHDL-1068") ;
        _msgs->Insert("execution of %s(%s) failed", "VHDL-1069") ;
        _msgs->Insert("exit statement is not inside a loop", "VHDL-1070") ;
// retired 11/01   : _msgs->Insert("EXIT statement with explicit label name not yet supported", "VHDL-1071") ;
        _msgs->Insert("expected a constant integer literal", "VHDL-1072") ;
        _msgs->Insert("expected an architecture identifier in index", "VHDL-1073") ;
        _msgs->Insert("expected exactly one index specification", "VHDL-1074") ;
        _msgs->Insert("exponentiation with negative exponent is only allowed for REAL values", "VHDL-1075") ;
        _msgs->Insert("exponent of integer literal cannot be negative", "VHDL-1076") ;
        _msgs->Insert("expression has %d elements ; expected %d", "VHDL-1077") ;
        _msgs->Insert("expression is not constant", "VHDL-1078") ;
// retired 04/02   : _msgs->Insert("formal designator is not a type-conversion, function call or formal name", "VHDL-1079") ;
// retired 04/02   : _msgs->Insert("formal indexed designator should have one argument", "VHDL-1080") ;
        _msgs->Insert("formal %s has no actual or default value", "VHDL-1081") ;
        _msgs->Insert("formal %s is already associated", "VHDL-1082") ;
        _msgs->Insert("formal %s is not a %s", "VHDL-1083") ;
        _msgs->Insert("formal %s is not declared", "VHDL-1084") ;
        _msgs->Insert("function call NOW ignored", "VHDL-1085") ;
// retired 11/01   : _msgs->Insert("function parameter %s must be mode 'in'", "VHDL-1086") ;
        _msgs->Insert("function %s does not always return a value", "VHDL-1087") ;
        _msgs->Insert("function %s does not take type %s as argument", "VHDL-1088") ;
        _msgs->Insert("generate condition is not constant", "VHDL-1089") ;
// retired 04/02   : _msgs->Insert("generic map aspect on block not yet supported", "VHDL-1090") ;
// retired 03/04 (1334 takes over)        _msgs->Insert("generic %s does not exist on binding unit %s", "VHDL-1091") ;
// retired 10/01   : _msgs->Insert("guard on expression not yet supported", "VHDL-1092") ;
// retired 04/02   : _msgs->Insert("identifier %s is not a %s", "VHDL-1093") ;
        _msgs->Insert("ignoring unknown pragma value %s", "VHDL-1094") ;
        _msgs->Insert("illegal block specification name", "VHDL-1095") ;
        _msgs->Insert("illegal constrained element in unconstrained array declaration", "VHDL-1096") ;
        //_msgs->Insert("illegal formal", "VHDL-1097") ; // retired 12/28/04
        _msgs->Insert("illegal formal part", "VHDL-1098") ;
        _msgs->Insert("illegal function call in assignment", "VHDL-1099") ;
        _msgs->Insert("illegal <> in expression", "VHDL-1100") ;
        _msgs->Insert("illegal named association in array index", "VHDL-1101") ;
        _msgs->Insert("illegal named association in index constraint", "VHDL-1102") ;
        _msgs->Insert("illegal named association in type conversion", "VHDL-1103") ;
        _msgs->Insert("illegal name for assignment", "VHDL-1104") ;
        _msgs->Insert("illegal name in constraint", "VHDL-1105") ;
        _msgs->Insert("illegal name on the sensitivity list", "VHDL-1106") ;
        _msgs->Insert("illegal range constraint in expression", "VHDL-1107") ;
        _msgs->Insert("illegal range in expression", "VHDL-1108") ;
        _msgs->Insert("illegal selected name for assignment", "VHDL-1109") ;
        _msgs->Insert("illegal selected name prefix", "VHDL-1110") ;
        _msgs->Insert("illegal target for assignment", "VHDL-1111") ;
        _msgs->Insert("illegal unconstrained element in constrained array declaration", "VHDL-1112") ;
        _msgs->Insert("illegal unit name", "VHDL-1113") ;
        _msgs->Insert("illegal use of attribute %s in constraint", "VHDL-1114") ;
        _msgs->Insert("illegal use of <> in subtype indication", "VHDL-1115") ;
        _msgs->Insert("incorrect number of elements in target aggregate", "VHDL-1116") ;
        _msgs->Insert("index constraint illegal in expression", "VHDL-1117") ;
// Retired 12/04 (replaced by 1124)        _msgs->Insert("index constraint is not compatible with index range of %s", "VHDL-1118") ;
        _msgs->Insert("index constraint cannot apply to non array type %s", "VHDL-1119") ;
        _msgs->Insert("indexed name is not a %s", "VHDL-1120") ;
        _msgs->Insert("indexed name prefix type %s expects %d dimensions", "VHDL-1121") ;
        _msgs->Insert("indexed name prefix type %s is not an array type", "VHDL-1122") ;
        _msgs->Insert("index %s is out of array constraint %s for target %s", "VHDL-1123") ;
        _msgs->Insert("index value %s is out of range %s", "VHDL-1124") ;
        _msgs->Insert("index value %s is always out of array index range %s", "VHDL-1125") ;
        _msgs->Insert("infinite loops not supported for synthesis", "VHDL-1126") ;
        _msgs->Insert("in index constraint; %s is not a discrete type", "VHDL-1127") ;
        _msgs->Insert("initial value for constant declaration is not constant", "VHDL-1128") ;
        _msgs->Insert("initial value for signal ignored", "VHDL-1129") ;
        _msgs->Insert("initial value for variable is ignored", "VHDL-1130") ;
        _msgs->Insert("%s is not found", "VHDL-1131") ;
        _msgs->Insert("left bound of slice is outside its index type range", "VHDL-1132") ;
        _msgs->Insert("left range bound is not constant", "VHDL-1133") ;
        _msgs->Insert("mismatch in number of elements assigned in conditional signal assignment", "VHDL-1134") ;
        _msgs->Insert("mismatch on label ; expected %s", "VHDL-1135") ;
        _msgs->Insert("multiple signals in event expression is not synthesizable", "VHDL-1136") ;
        _msgs->Insert("multiple wait statements in one process not supported for synthesis", "VHDL-1137") ;
// retired 04/04   : _msgs->Insert("named association of unconstrained output formals is not supported", "VHDL-1138") ;
        _msgs->Insert("near bit string literal %s ; %d visible types match here", "VHDL-1139") ;
        _msgs->Insert("near character %s ; %d visible types match here", "VHDL-1140") ;
        _msgs->Insert("near formal type-conversion ; cannot convert type %s to type %s", "VHDL-1141") ;
        _msgs->Insert("near NEW ; result type %s is not a %s", "VHDL-1142") ;
        _msgs->Insert("illegal use of NULL literal", "VHDL-1143") ;
        _msgs->Insert("near %s ; attribute dimensionality incorrect", "VHDL-1144") ;
// retired 9/28/05 : _msgs->Insert("near %s ; %d identifiers match here", "VHDL-1145") ;
        _msgs->Insert("near %s ; %d visible identifiers match here", "VHDL-1146") ;
// retired 9/28/05 : _msgs->Insert("near %s ; %d visible types match here", "VHDL-1147") ;
// retired 04/02   : _msgs->Insert("near %s ; name does not match type %s", "VHDL-1148") ;
        _msgs->Insert("near %s ; prefix should denote a scalar or array type", "VHDL-1149") ;
        _msgs->Insert("near %s ; signature profile does not match expected type %s", "VHDL-1150") ;
// retired 04/02   : _msgs->Insert("near %s; sliced name is allowed only on single-dimensional arrays", "VHDL-1151") ;
        _msgs->Insert("near string %s ; %d visible types match here", "VHDL-1152") ;
        _msgs->Insert("near %s ; type conversion does not match type %s", "VHDL-1153") ;
        _msgs->Insert("near %s ; type conversion expects one single argument", "VHDL-1154") ;
        _msgs->Insert("near %s ; type conversion expression type cannot be determined uniquely", "VHDL-1155") ;
        _msgs->Insert("netlist %s(%s) remains a blackbox, due to errors in its contents", "VHDL-1156") ;
        _msgs->Insert("next statement is not inside a loop", "VHDL-1157") ;
// retired 11/01   : _msgs->Insert("NEXT statement with explicit label name not yet supported", "VHDL-1158") ;
        _msgs->Insert("non-constant exponent operations not supported", "VHDL-1159") ;
        _msgs->Insert("nonconstant REAL value %s is not supported for synthesis", "VHDL-1160") ;
// retired 8/06    : _msgs->Insert("nonobject alias is not supported", "VHDL-1161") ;
        _msgs->Insert("no support for attribute %s", "VHDL-1162") ;
        _msgs->Insert("no support for partial association of unconstrained formals", "VHDL-1163") ;
        _msgs->Insert("no support for selected name assignments to out-of-scope objects", "VHDL-1164") ;
// retired 04/04   : _msgs->Insert("unconstrained ports are not supported", "VHDL-1165") ;
        _msgs->Insert("operation on a REAL number not supported", "VHDL-1166") ;
        _msgs->Insert("operator %s not defined on integer values", "VHDL-1167") ;
        _msgs->Insert("operator %s not defined on real values", "VHDL-1168") ;
        _msgs->Insert("operator %s not supported on these values", "VHDL-1169") ;
        _msgs->Insert("operator %s not supported on this value", "VHDL-1170") ;
        _msgs->Insert("'others' choice should be last in alternatives", "VHDL-1171") ;
        _msgs->Insert("others clause is never selected", "VHDL-1172") ;
        _msgs->Insert("'others' clause must be last, single choice in an aggregate", "VHDL-1173") ;
        _msgs->Insert("OTHERS is illegal aggregate choice for unconstrained target", "VHDL-1174") ;
        _msgs->Insert("'others' is illegal in assignment target aggregate", "VHDL-1175") ;
        _msgs->Insert("OTHERS should be the only choice in alternative", "VHDL-1176") ;
        _msgs->Insert("overwriting existing primary unit %s", "VHDL-1177") ;
        _msgs->Insert("overwriting existing secondary unit %s", "VHDL-1178") ;
        _msgs->Insert("package declaration %s is not yet compiled", "VHDL-1179") ;
// retired 11/01   : _msgs->Insert("parameter %s must be mode 'in','out' or 'inout'", "VHDL-1180") ;
// retired  7/02   : _msgs->Insert("partial assignment to an alias is not supported", "VHDL-1181") ;
// retired 04/02   : _msgs->Insert("physical literal is not a %s", "VHDL-1182") ;
        _msgs->Insert("physical range should denote an integer type", "VHDL-1183") ;
        _msgs->Insert("physical unit does not denote a physical type", "VHDL-1184") ;
        _msgs->Insert("physical value should denote an integer or real value", "VHDL-1185") ;
// retired 04/02   : _msgs->Insert("port map aspect on block not yet supported", "VHDL-1186") ;
        _msgs->Insert("positional association cannot follow named association", "VHDL-1187") ;
// retired 12/01   : _msgs->Insert("positional association cannot follow named association in aggregate", "VHDL-1188") ;
        _msgs->Insert("pragma %s not supported ; function ignored", "VHDL-1189") ;
        _msgs->Insert("prefix of attribute %s should be a discrete or physical (sub)type", "VHDL-1190") ;
        _msgs->Insert("prefix of LENGTH attribute should be an array or array (sub)type", "VHDL-1191") ;
// retired 07/03   : _msgs->Insert("prefix of qualified expression should denote a type", "VHDL-1192") ;
// retired 04/02   : _msgs->Insert("primary binding with port map not yet supported", "VHDL-1193") ;
        _msgs->Insert("primary unit of binding component %s is not an entity", "VHDL-1194") ;
        _msgs->Insert("process cannot have both a wait statement and a sensitivity list", "VHDL-1195") ;
        _msgs->Insert("possible infinite loop; process does not have a wait statement", "VHDL-1196") ; // VIPER 3710 : change message and made a warning.
        _msgs->Insert("range choice ignored", "VHDL-1197") ;
        _msgs->Insert("range choice is illegal in assignment target aggregate", "VHDL-1198") ;
        _msgs->Insert("range extends beyond constraint of type %s", "VHDL-1199") ;
        _msgs->Insert("range is empty (null range)", "VHDL-1200") ;
        _msgs->Insert("re-analyze unit %s since unit %s is overwritten or removed", "VHDL-1201") ;
        _msgs->Insert("record element cannot be unconstrained", "VHDL-1202") ;
// retired 04/02   : _msgs->Insert("record field %s is not a %s", "VHDL-1203") ;
        _msgs->Insert("record name prefix type %s is not a record type", "VHDL-1204") ;
        _msgs->Insert("replacing existing netlist %s(%s)", "VHDL-1205") ;
        _msgs->Insert("result of attribute %s is not in range of type %s", "VHDL-1206") ;
        _msgs->Insert("return statement in function needs an expression", "VHDL-1207") ;
        _msgs->Insert("return statement in procedure should not have an expression", "VHDL-1208") ;
        _msgs->Insert("return statement outside a subprogram", "VHDL-1209") ;
        _msgs->Insert("right bound of slice is outside its index type range", "VHDL-1210") ;
        _msgs->Insert("right range bound is not constant", "VHDL-1211") ;
        _msgs->Insert("%s already has a binding", "VHDL-1212") ;
// retired 04/02   : _msgs->Insert("%s already has a primary binding", "VHDL-1213") ;
        _msgs->Insert("%s already imposes an index constraint", "VHDL-1214") ;
        _msgs->Insert("scalar type range should denote an integer or floating point type", "VHDL-1215") ;
        _msgs->Insert("%s does not have a iteration scheme", "VHDL-1216") ;
        _msgs->Insert("%s does not have attribute %s", "VHDL-1217") ;
        _msgs->Insert("%s does not instantiate component %s", "VHDL-1218") ;
        _msgs->Insert("second argument of %s should be a constant", "VHDL-1219") ;
// retired 04/02   : _msgs->Insert("selected name identifier %s is not a %s", "VHDL-1220") ;
        _msgs->Insert("signal cannot be unconstrained", "VHDL-1221") ;
// retired 04/02   : _msgs->Insert("signatured name %s is not a %s", "VHDL-1222") ;
        _msgs->Insert("%s is already declared in this region", "VHDL-1223") ;
        _msgs->Insert("%s is not a block label", "VHDL-1224") ;
        _msgs->Insert("%s is not a component", "VHDL-1225") ;
        _msgs->Insert("%s is not a label", "VHDL-1226") ;
        _msgs->Insert("%s is not a label of this statement", "VHDL-1227") ;
        _msgs->Insert("%s is not a loop label", "VHDL-1228") ;
// retired 04/02   : _msgs->Insert("%s is not an architecture of entity %s", "VHDL-1229") ;
        _msgs->Insert("%s is not an attribute", "VHDL-1230") ;
        _msgs->Insert("%s is not an entity", "VHDL-1231") ;
// retired 09/05   : _msgs->Insert("%s is not an object", "VHDL-1232") ;
        _msgs->Insert("%s is not a package", "VHDL-1233") ;
        _msgs->Insert("%s is not a (resolution) function", "VHDL-1234") ;
        _msgs->Insert("%s is not a %s", "VHDL-1235") ;
        _msgs->Insert("%s is not a signal", "VHDL-1236") ;
        _msgs->Insert("%s is not a subprogram", "VHDL-1237") ;
        _msgs->Insert("%s is not a type", "VHDL-1238") ;
        _msgs->Insert("%s is not a valid resolution function for type %s", "VHDL-1239") ;
        _msgs->Insert("'%s' is not compiled in library %s", "VHDL-1240") ;
        _msgs->Insert("%s is not declared", "VHDL-1241") ;
        _msgs->Insert("%s is not synthesizable since it does not hold its value under NOT(clock-edge) condition", "VHDL-1242") ;
        _msgs->Insert("slice direction differs from its index subtype range", "VHDL-1243") ;
        _msgs->Insert("slice direction differs from its index type range", "VHDL-1244") ;
        _msgs->Insert("sliced name is allowed only on single-dimensional arrays", "VHDL-1245") ;
        _msgs->Insert("some element(s) in aggregate are missing", "VHDL-1246") ;
// retired 03/04 (1333 takes over)       _msgs->Insert("%s on binding unit %s has a different type", "VHDL-1247") ;
// retired 03/04   : _msgs->Insert("%s on binding unit %s is not a generic", "VHDL-1248") ;
// retired 07/12        _msgs->Insert("%s on constant net ignored", "VHDL-1249") ;
        _msgs->Insert("%s remains a black-box since it has no binding entity", "VHDL-1250") ;
        _msgs->Insert("%s should be on the sensitivity list of the process", "VHDL-1251") ;
        _msgs->Insert("stack overflow on recursion via %s, limit of %d has exceeded", "VHDL-1252") ;
        _msgs->Insert("statement might not cover all choices ; 'others' clause recommended", "VHDL-1253") ;
// retired 04/02   : _msgs->Insert("string is not a %s", "VHDL-1254") ;
        _msgs->Insert("subprogram %s does not have a body", "VHDL-1255") ;
        _msgs->Insert("subtype of alias %s does not have the same bounds or direction as the target name", "VHDL-1256") ;
        _msgs->Insert("subtype of case expression is not locally static", "VHDL-1257") ;
        _msgs->Insert("suffix of record name cannot be 'all'", "VHDL-1258") ;
        _msgs->Insert("%s is declared here", "VHDL-1259") ;
// replaced by VHDL-1261        _msgs->Insert("syntax error near '%s'", "VHDL-1260") ;
        _msgs->Insert("syntax error near %s", "VHDL-1261") ;
        _msgs->Insert("synthesis directive attribute %s ignored ; incorrect number of bit values for type %s", "VHDL-1262") ;
        _msgs->Insert("synthesis ignores all but the first waveform", "VHDL-1263") ;
        _msgs->Insert("target of NEW ; type %s is not an access type", "VHDL-1264") ;
        _msgs->Insert("target slice is %d elements ; value is %d elements", "VHDL-1265") ;
        _msgs->Insert("there is no NOT of %s in type %s", "VHDL-1266") ;
        _msgs->Insert("there is no value left of %s in type %s", "VHDL-1267") ;
        _msgs->Insert("there is no value right of %s in type %s", "VHDL-1268") ;
// retired 04/02   : _msgs->Insert("too many generics", "VHDL-1269") ;
// retired 04/02   : _msgs->Insert("too many ports", "VHDL-1270") ;
        _msgs->Insert("type error near discrete range ; type %s is not a discrete type", "VHDL-1271") ;
        _msgs->Insert("type error near %s ; expected type %s", "VHDL-1272") ;
        _msgs->Insert("type mismatch at operator %s ; expected type %s", "VHDL-1273") ;
        _msgs->Insert("type mismatch between 'others' choices of record association", "VHDL-1274") ;
        _msgs->Insert("type of aggregate cannot be determined without context ; %d visible types match here", "VHDL-1275") ;
        _msgs->Insert("type %s does not match with a string literal", "VHDL-1276") ;
        _msgs->Insert("type %s does not match with the bit-string literal ; should be array of BIT", "VHDL-1277") ;
        _msgs->Insert("type %s does not match with the integer literal", "VHDL-1278") ;
        _msgs->Insert("type %s does not match with the real literal", "VHDL-1279") ;
        _msgs->Insert("types for range bounds are not compatible", "VHDL-1280") ;
        _msgs->Insert("'unaffected' should be the only waveform element", "VHDL-1281") ;
        _msgs->Insert("unequal length arguments for operator %s", "VHDL-1282") ;
        _msgs->Insert("unexpected EOF", "VHDL-1283") ;
        _msgs->Insert("unit %s ignored due to previous errors", "VHDL-1284") ;
        _msgs->Insert("unknown character '%c' in enumeration encoding is considered 'X'", "VHDL-1285") ;
        _msgs->Insert("unknown Exemplar directive %s", "VHDL-1286") ;
        _msgs->Insert("use of an expression is not allowed here", "VHDL-1287") ;
        _msgs->Insert("use <= to assign to signal %s", "VHDL-1288") ;
        _msgs->Insert("use := to assign to variable %s", "VHDL-1289") ;
// retired        : _msgs->Insert("value can be out of constraint range of target", "VHDL-1290") ;
        _msgs->Insert("value %s is out of target constraint range %s", "VHDL-1291") ;
        _msgs->Insert("variable cannot be unconstrained", "VHDL-1292") ;
        _msgs->Insert("variable in subprogram or process cannot be 'shared'", "VHDL-1293") ;
        _msgs->Insert("variable outside of subprogram or process must be 'shared'", "VHDL-1294") ;
        _msgs->Insert("wait statements in subprograms are not supported for synthesis", "VHDL-1295") ;
        _msgs->Insert("wait statement without UNTIL clause not supported for synthesis", "VHDL-1296") ;
        _msgs->Insert("wait until event is not supported for synthesis", "VHDL-1297") ;
        _msgs->Insert("non-constant loop limit of %d exceeded", "VHDL-1298") ;
        _msgs->Insert("non-static loop limit of %d exceeded", "VHDL-1299") ;

// Added after first list creation :
        _msgs->Insert("guarded signal assignment is not in a guarded block", "VHDL-1300") ;
        _msgs->Insert("%s is never used", "VHDL-1301") ;
        _msgs->Insert("%s is never assigned", "VHDL-1302") ;
        _msgs->Insert("using initial value %s for %s since it is never assigned", "VHDL-1303") ;
        // _msgs->Insert("assertion always occurs : %s", "VHDL-1304") ; // obsolete after VIPER 4667
        _msgs->Insert("deferred constant %s needs an initial value", "VHDL-1305") ;
        _msgs->Insert("signal declaration is not allowed in a subprogram", "VHDL-1306") ;
        _msgs->Insert("mode '%s' is not allowed for function parameters", "VHDL-1307") ;
        _msgs->Insert("mode '%s' is not allowed for subprogram parameters", "VHDL-1308") ;
        _msgs->Insert("object kind '%s' is not allowed for function parameters", "VHDL-1309") ;
        _msgs->Insert("object kind '%s' is not allowed for subprogram parameters", "VHDL-1310") ;
        _msgs->Insert("%s expects %d arguments", "VHDL-1311") ;
        _msgs->Insert("%s is illegal in an expression", "VHDL-1312") ;
        _msgs->Insert("OTHERS or ALL should be the only entity name in the list", "VHDL-1313") ;
        _msgs->Insert("exit statement is not inside loop %s", "VHDL-1314") ;
        _msgs->Insert("next statement is not inside loop %s", "VHDL-1315") ;
        _msgs->Insert("%s cannot be used within its own interface list", "VHDL-1316") ;
        _msgs->Insert("structural recursion (via %s) is not supported", "VHDL-1317") ;

// Added 04/02 interface object checks added and various other checks improved
        _msgs->Insert("actual of formal %s port %s cannot be an expression", "VHDL-1318") ;
        _msgs->Insert("actual %s of formal %s must be a %s", "VHDL-1319") ;
        _msgs->Insert("attribute %s is illegal in an expression", "VHDL-1320") ;
        _msgs->Insert("attribute %s is illegal in assignment", "VHDL-1321") ;
        _msgs->Insert("attribute %s is not a range", "VHDL-1322") ;
        _msgs->Insert("binding entity %s does not have generic %s", "VHDL-1323") ;
        _msgs->Insert("binding entity %s does not have port %s", "VHDL-1324") ;
        _msgs->Insert("block specification index cannot be open", "VHDL-1325") ;
        _msgs->Insert("cannot read or update 'linkage' object %s", "VHDL-1326") ;
        _msgs->Insert("cannot update 'in' object %s", "VHDL-1327") ;
        _msgs->Insert("choice others in a record aggregate should represent at least one element", "VHDL-1328") ;
        _msgs->Insert("constant declaration cannot be of a file type or access type", "VHDL-1329") ;
        _msgs->Insert("constant %s can only be of mode IN", "VHDL-1330") ;
        _msgs->Insert("designator in type-conversion cannot be OPEN", "VHDL-1331") ;
        _msgs->Insert("designator type-conversion should have one argument", "VHDL-1332") ;
        _msgs->Insert("entity generic %s does not match with type %s of component generic", "VHDL-1333") ;
        _msgs->Insert("entity port %s does not match with type %s of component port", "VHDL-1334") ;
        _msgs->Insert("formal file %s should be of a file type", "VHDL-1335") ;
        _msgs->Insert("formal port %s of mode %s cannot be associated with actual port %s of mode %s", "VHDL-1336") ;
        _msgs->Insert("formal %s %s cannot be of a file type or access type", "VHDL-1337") ;
        _msgs->Insert("guarded signal declaration of a scalar type should have a resolution function", "VHDL-1338") ;
// retired 5/07        _msgs->Insert("incremental binding not yet supported", "VHDL-1339") ;
        _msgs->Insert("input designator %s cannot contain a formal type-conversion", "VHDL-1340") ;
        _msgs->Insert("linkage interface object %s cannot have a default expression", "VHDL-1341") ;
        _msgs->Insert("mode or default expression is illegal on a interface file declaration", "VHDL-1342") ;
        _msgs->Insert("multiple configuration items match label %s", "VHDL-1343") ;
        _msgs->Insert("named association not allowed in designator", "VHDL-1344") ;
// retired 4/13/05      _msgs->Insert("output designator %s cannot contain a actual type-conversion", "VHDL-1345") ;
        _msgs->Insert("partially associated formal %s cannot have actual OPEN", "VHDL-1346") ;
        _msgs->Insert("%s has only %d generics", "VHDL-1347") ;
        _msgs->Insert("%s has only %d ports", "VHDL-1348") ;
        _msgs->Insert("signal declaration cannot be of a file type or access type", "VHDL-1349") ;
        _msgs->Insert("%s is not a range", "VHDL-1350") ;
        _msgs->Insert("%s is not a record element", "VHDL-1351") ;
        _msgs->Insert("%s is not a variable", "VHDL-1352") ;
        _msgs->Insert("%s is not declared in %s", "VHDL-1353") ;
        _msgs->Insert("%s is only allowed on signals", "VHDL-1354") ;
        _msgs->Insert("%s %s cannot have a default expression", "VHDL-1355") ;
        _msgs->Insert("%s should be a %s", "VHDL-1356") ;
        _msgs->Insert("%s variable %s cannot have a default expression", "VHDL-1357") ;
        _msgs->Insert("%s with mode 'in' cannot be updated", "VHDL-1358") ;
        _msgs->Insert("%s with mode 'out' cannot be read", "VHDL-1359") ;
        _msgs->Insert("variable declaration cannot be of a file type", "VHDL-1360") ;

        _msgs->Insert("unit %s does not have a generic named %s", "VHDL-1361") ; // handling of top-level user generic setting
        // VIPER #3401: Produce a better error message for this generic association mismatch (keep the message id same):
        //_msgs->Insert("unit %s does not have %d generics", "VHDL-1362") ; // handling of top-level user generic setting
        _msgs->Insert("number of generic actuals %d does not match with number of formals %d for unit %s", "VHDL-1362") ;
        _msgs->Insert("unknown actual generic literal value %s for generic %s ignored", "VHDL-1363") ; // handling of top-level user generic setting

// 5/2002 added :
        _msgs->Insert("%s has only %d record elements", "VHDL-1364") ;
        _msgs->Insert("some record elements are missing in this aggregate of %s", "VHDL-1365") ;
        _msgs->Insert("choice is not a record element of %s", "VHDL-1366") ;

// 8/2003 obsolete (checked in database)        _msgs->Insert("external non-constant %s not supported for synthesis", "VHDL-1367") ;
        _msgs->Insert("value for generic %s is not constant", "VHDL-1368") ;

        _msgs->Insert("partial association in subprogram calls not supported", "VHDL-1369") ;

// 10/2002 added
        _msgs->Insert("another match is here", "VHDL-1370") ;
        _msgs->Insert("illegal use of 'file_open' expression in VHDL'87 mode", "VHDL-1371") ;
        _msgs->Insert("illegal use of file 'mode' in VHDL'93 mode", "VHDL-1372") ;

// 11/2002 added
        _msgs->Insert("attribute %s can only be applied to a block label or architecture name", "VHDL-1373") ;
        _msgs->Insert("attribute %s has already been set on %s", "VHDL-1374") ;

// 12/2002 added
        _msgs->Insert("illegal name in specification", "VHDL-1375") ;

// 3/2003 added
        _msgs->Insert("unmatched %s translate/synthesis off pragma found; matching pair with same keywords is required", "VHDL-1376") ; // VIPER 3312/3314 changed this message
        _msgs->Insert("near %s ; prefix should denote a scalar type", "VHDL-1377") ;

// 4/2003 added pragma checks :
        _msgs->Insert("directive %s is only appropriate for functions", "VHDL-1380") ;
        _msgs->Insert("directive %s not supported on type %s", "VHDL-1381") ;
        _msgs->Insert("directive %s expects one argument", "VHDL-1382") ;
        _msgs->Insert("directive %s expects two arguments", "VHDL-1383") ;
        _msgs->Insert("directive %s on function %s ignored", "VHDL-1384") ;
        // start next at 1390 please.

// 5/2003 added :
        _msgs->Insert("comparison between unequal length arrays always returns %s", "VHDL-1390") ;

// 6/2003 added :
        _msgs->Insert("multiple declarations of %s included via multiple use clauses; none are made directly visible", "VHDL-1391") ;

// 8/2003 added :
        _msgs->Insert("illegal identifier : %s", "VHDL-1392") ;

// 9/2003 added :
        _msgs->Insert("signature may not appear in a declaration of an object alias", "VHDL-1393") ;
        _msgs->Insert("base type of object alias declaration must be the same of subtype indication's type mark", "VHDL-1394") ;
        _msgs->Insert("base type of object alias declaration must not be a multi-dimensional array type", "VHDL-1395") ;
        _msgs->Insert("illegal nonobject alias declaration target", "VHDL-1396") ;
        _msgs->Insert("subtype indication may not appear in a nonobject alias declaration", "VHDL-1397") ;
        _msgs->Insert("signature is required if the nonobject alias name denotes a subprogram or enumeration literal", "VHDL-1398") ;

// 4/2004 added :
        _msgs->Insert("going to verilog side to elaborate module %s", "VHDL-1399") ;
        _msgs->Insert("back to vhdl to continue elaboration", "VHDL-1400") ;
        _msgs->Insert("unit %s has no binding", "VHDL-1401") ;

// 5/2004 added :
        _msgs->Insert("ambiguous type: '%s' or '%s'", "VHDL-1402") ;             // VIPER #1807
        _msgs->Insert("subprogram call %s ignored for synthesis", "VHDL-1403") ; // VIPER #1769; pragma ignore_subprogram
        _msgs->Insert("entity %s does not have an architecture", "VHDL-1404") ;  // VIPER #1813

// 6/2004 added :
        _msgs->Insert("partial association of component port '%s' with formal '%s' is not supported in static elaboration", "VHDL-1405") ;

// 12/2004 added :
        _msgs->Insert("%s designator %s cannot contain an actual type-conversion", "VHDL-1406") ;

// 1/2005 added :
        _msgs->Insert("actual expression for generic %s cannot reference a signal", "VHDL-1407") ; // VIPER #1709

// 2/2005 added :
        _msgs->Insert("subprogram type at end of subprogram body does not match specification type '%s'", "VHDL-1408") ; // VIPER #2017

// 4/2005 added :
        _msgs->Insert("resolution function %s without synthesis directive will use three-state wiring", "VHDL-1409") ; // VIPER #1174 and #1243.

// 5/2005 added :
        _msgs->Insert("count is not constant", "VHDL-1410") ; // PSL
        _msgs->Insert("count must be positive", "VHDL-1411") ; // PSL

// 6/2005 added :
        _msgs->Insert("%s match for '%s' found here", "VHDL-1412") ;

// 9/2005 added :
        _msgs->Insert("subprogram %s does not conform with its declaration", "VHDL-1413") ; // VIPER #2185

// 10/2005 added :
        _msgs->Insert("if-condition is an event, not an edge", "VHDL-1414") ; // VIPER #2215

// 12/2005 added :
        _msgs->Insert("cannot access '%s' from inside pure function '%s'.", "VHDL-1415") ; // VIPER #2286

// 2/2006 added :
        _msgs->Insert("character with value 0x%x is not a graphic literal character", "VHDL-1416") ; // VIPER #2312

// 4/2006 added :
        //_msgs->Insert("direct entity instantiations of verilog modules are currently not supported", "VHDL-1417") ; // VIPER #2410 // Removed as a part of mix lang code cleanup

// 8/2006 added :
// retired 3/07 : _msgs->Insert("constraint of target is not compatible to that of value", "VHDL-1418") ; // VIPER #2636

// 2/2007 added :
        _msgs->Insert("range constraints cannot be applied to array types", "VHDL-1419") ; // VIPER #2864
        _msgs->Insert("construct '%s' not allowed in VHDL'87; use VHDL'93 mode", "VHDL-1420") ; // VIPER #2835

// 3/2007 added :
        _msgs->Insert("elements of file type are not allowed in composite types", "VHDL-1421") ; // VIPER 2921

// 5/2007 added :
        _msgs->Insert("argument to operator call %s is ambiguous. Use qualified expression or switch to 87 mode", "VHDL-1422") ; // VIPER 2982
        _msgs->Insert("design unit %s contains unconstrained port(s)", "VHDL-1423") ; // VIPER #3129
        _msgs->Insert("%s is not a valid operator symbol", "VHDL-1424") ; // VIPER #3166
        // 08/2007 deprecated by VHDL-1445 -> _msgs->Insert("error near %s : only static signal names are allowed in process sensitivity list", "VHDL-1425") ; // VIPER #3148, #3149
        _msgs->Insert("port %s is already associated in the primary binding", "VHDL-1426") ; // VIPER #2214 : incremental binding
        _msgs->Insert("near %s ; prefix should denote an array type", "VHDL-1427") ; // VIPER #2874/3045

// 6/2007 added :
        _msgs->Insert("size mismatch in mixed language port association, verilog port %s", "VHDL-1428") ; // VIPER #3254/3255 : Check for mixed language port consistency
        //_msgs->Insert("binding verilog module %s does not have port %s", "VHDL-1429") ; // VIPER #3254/3255 : Check for mixed language port consistency // Removed as a part of mix lang code cleanup
        //_msgs->Insert("incorrect number of port association in instantiation of verilog module %s", "VHDL-1430") ; // VIPER #3254/3255 : Check for mixed language port consistency // Removed as a part of mix lang code cleanup
        _msgs->Insert("literal %s exceeds maximum integer value", "VHDL-1431") ; // VIPER 3145
        _msgs->Insert("missing full type definition for %s", "VHDL-1432") ; // VIPER #3174

// 7/2007 added :
        _msgs->Insert("wait statement not allowed inside a function", "VHDL-1433") ; // VIPER #3147
        _msgs->Insert("deferred constant %s is allowed only in package declaration", "VHDL-1434") ; // VIPER #3162
        _msgs->Insert("operator %s not defined on physical values", "VHDL-1435") ;

// 8/2007 added :
        _msgs->Insert("non-constant loop found; will execute up to %d iterations", "VHDL-1436") ; // info message to explain long compilation time
        _msgs->Insert("formal %s of mode %s must have an associated actual", "VHDL-1437") ;
        _msgs->Insert("case choice must be a locally static expression", "VHDL-1438") ; // VIPER #3150
        _msgs->Insert("in an array aggregate expression, non-locally static choice is allowed only if it is the only choice of the only association", "VHDL-1439") ; // VIPER #3160
        _msgs->Insert("parameter in attribute delayed must be globally static expression", "VHDL-1440") ; // VIPER #3169
        _msgs->Insert("range constraint in scalar type definition must be locally static", "VHDL-1441") ; // VIPER #3171
        _msgs->Insert("aggregate target in assignment must be all locally static names", "VHDL-1442") ; // VIPER #3146
        _msgs->Insert("actual for formal port %s is neither a static name nor a globally static expression", "VHDL-1443") ; // VIPER #3183
        _msgs->Insert("expression has incompatible type", "VHDL-1444") ; // VIPER #3143
        _msgs->Insert("sensitivity list can have only static signal name", "VHDL-1445") ; // Viper #3149, #3148
        _msgs->Insert("attribute %s requires a static signal prefix", "VHDL-1446") ; // Viper #3483
        _msgs->Insert("index/slice name with label prefix must have static index/slice", "VHDL-1447") ; // Viper #3488
        _msgs->Insert("parameter value of attribute '%s is out of range", "VHDL-1448") ; // Viper #3480
        _msgs->Insert("%s not allowed in entity statement part", "VHDL-1449") ; // Viper #3487
        _msgs->Insert("attribute delayed may not be read from formal signal parameter %s", "VHDL-1450") ; // Viper #3473
        _msgs->Insert("cannot index the result of a type conversion", "VHDL-1451") ; // Viper #3471
        _msgs->Insert("allocator subtype indication cannot be unconstrained array", "VHDL-1452") ; // Viper #3517
        _msgs->Insert("partial formal element already associated", "VHDL-1453") ; // Viper #3469
        _msgs->Insert("not all partial formals of %s have actual", "VHDL-1454") ; // Viper #3530
        _msgs->Insert("function return type array bounds do not match function return value array bounds", "VHDL-1455") ; // VIPER #3151
        _msgs->Insert("designated type of access type cannot be file type", "VHDL-1456") ; // Viper #3156
        _msgs->Insert("formal parameter of type file must be associated with a file object", "VHDL-1457") ; // Viper #3470
        _msgs->Insert("case statement choice %s is outside the range of the select", "VHDL-1458") ; // Viper #3539
        _msgs->Insert("cannot drive implicit signal guard", "VHDL-1459") ; // Viper #3152

// 9/2007 added :
        _msgs->Insert("attribute %s requires a constrained array prefix type", "VHDL-1460") ; // VIPER 3167.
        _msgs->Insert("cannot assign null waveform to non-guarded signal", "VHDL-1461") ; // Viper #3185
        _msgs->Insert("direction/bounds for signal formal %s must match with that of actual type for subprogram %s", "VHDL-1462") ; // Viper #3430
        _msgs->Insert("extended identifier %s contains non-graphic character 0x%x", "VHDL-1463") ; // VIPER #3537

// 10/2007 added :
        _msgs->Insert("no index value can belong to null index range", "VHDL-1464") ; // VIPER #3164

        _msgs->Insert("designated type of file type cannot be file or access type", "VHDL-1465") ; // Viper# 3621
        _msgs->Insert("designated type of file type cannot be multidimensional array type", "VHDL-1466") ; // Viper# 3621
        _msgs->Insert("prefix of %s in the use clause must be library or package identifier", "VHDL-1467") ; // Viper# 3625
        _msgs->Insert("attribute may not be of file or access type", "VHDL-1468") ; // Viper# 3626
        _msgs->Insert("base value %d must be at least two and at most sixteen", "VHDL-1469") ; // Viper# 3644

// 11/2007 added :
        _msgs->Insert("cannot open file '%s'", "VHDL-1470") ; // VIPER 3341 : file initialization.
        _msgs->Insert("cannot read attribute %s of mode linkage signal %s", "VHDL-1471") ; // VIPER #3647
        _msgs->Insert("cannot use null waveform in concurrent signal assignment", "VHDL-1472") ; // VIPER #3651
        _msgs->Insert("attribute %s may not be read from formal signal parameter %s", "VHDL-1473") ; // VIPER #3623
        _msgs->Insert("illegal recursive instantiation of %s(%s)", "VHDL-1474") ; // VIPER #3668
        _msgs->Insert("type conversion (to %s) cannot have %s operand", "VHDL-1475") ; // VIPER #3648
        _msgs->Insert("types do not conform for deferred constant %s", "VHDL-1476") ; // VIPER #3694
        _msgs->Insert("an alias object must be a static name", "VHDL-1477") ; // VIPER #3700
        _msgs->Insert("attribute %s does not take a parameter when prefix is not an array type", "VHDL-1478") ; // VIPER #3696
        _msgs->Insert("attribute %s requires a locally static parameter", "VHDL-1479") ; // VIPER #3696

// 12/2007 added :
        _msgs->Insert("the type of vhdl port is invalid for verilog connection", "VHDL-1480") ; // VIPER #3368

// 01/2008 added :
        _msgs->Insert("Analyzing VHDL file %s", "VHDL-1481") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("VHDL file %s ignored due to errors", "VHDL-1482") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Reading PSL file %s", "VHDL-1483") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("PSL file %s ignored due to errors", "VHDL-1484") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("unit %s is not yet analyzed", "VHDL-1485") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("unit %s is not an entity or configuration", "VHDL-1486") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Reading VHDL file %s", "VHDL-1487") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Copying library %s", "VHDL-1488") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Pretty printing unit %s to file %s", "VHDL-1489") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Pretty printing all units in library %s to file %s", "VHDL-1490") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("unit %s not found in library %s", "VHDL-1491") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("%s was previously created using a PSL-enabled VHDL analyzer, since the current VHDL analyzer is not PSL-enabled, there is a high probability that a restore error will occur due to unknown PSL constructs", "VHDL-1492") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Restoring VHDL parse-tree %s.%s from %s", "VHDL-1493") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("%s.%s failed to restore", "VHDL-1494") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("%s was created using a later vhdl_version_number, forward compatibility is not supported", "VHDL-1495") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("The library %s was not found", "VHDL-1496") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("No VHDL library search path could be found. Please set a search path to where unit can be saved", "VHDL-1497") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Save failed due to mkdir failure", "VHDL-1498") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Saving VHDL parse-tree %s.%s into %s", "VHDL-1499") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("VHDL library search path not set; cannot restore", "VHDL-1500") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("%s does not contain a known VHDL parse-tree format", "VHDL-1501") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("%s was created using code corresponding to a vhdl_version_number of %s, backward compatibility of versions before 0x0ab00001 is not supported", "VHDL-1502") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("The path \"%s\" does not exist", "VHDL-1503") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("The default vhdl library search path is now \"%s\"", "VHDL-1504") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("The vhdl library search path for library \"%s\" is now \"%s\"", "VHDL-1505") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("This parse-tree is too large to be saved", "VHDL-1506") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("A VHDL parse-tree node is trying to be created from a SaveRestore object that is in save mode!", "VHDL-1507") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Binary saving %s '%s'", "VHDL-1508") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Registering Dependencies Error: unknown design unit type", "VHDL-1509") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Registering Dependencies Error: %s '%s' could not be found during restore", "VHDL-1510") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Registering Dependencies Error: library not defined", "VHDL-1511") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("%s needs to be re-saved since %s.%s changed", "VHDL-1512") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("%s needs to be re-saved since %s.%s.%s changed", "VHDL-1513") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("Registering Dependencies Error: unknown pointer type", "VHDL-1514") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("cannot open vhdl file %s", "VHDL-1515") ; // VIPER #3142 : add msg-id to all msg
        _msgs->Insert("VHDL reader: User Interrupt. Cleaning up....", "VHDL-1516") ; // VIPER #3142 : add msg-id to all msg

// 02/2008 added:
        _msgs->Insert("readline called past the end of file %s", "VHDL-1517") ; // VIPER #3883 : readline reading past the end_of_file
        _msgs->Insert("character/string literal may not be designator for an object alias", "VHDL-1518") ; // VIPER #3875 : LRM 4.3.3 : character/string literal designator must be for non-object alias

// 07/2008 added:
        _msgs->Insert("prefix of attribute %s is not a type mark", "VHDL-1519") ; // VIPER #4172
        _msgs->Insert("illegal use of incomplete type", "VHDL-1520") ; // VIPER #2893
        _msgs->Insert("illegal syntax for subtype indication", "VHDL-1521") ; // VIPER #4169, #4170
        _msgs->Insert("illegal use of deferred constant %s", "VHDL-1522") ; // VIPER 3505

// 10/2008 added:
        _msgs->Insert("signal formal %s cannot be associated with type conversion or function call at actual", "VHDL-1523") ; // VIPER #4341
// 11/2008 added:
        _msgs->Insert("the type %s of vhdl port %s is invalid for verilog connection", "VHDL-1524") ; // VIPER #4367
        _msgs->Insert("array size is larger than 2**%d", "VHDL-1525") ; // VIPER #4369/4374/4397

// 12/2008 added:
        _msgs->Insert("prefix of expanded name does not denote an enclosing construct", "VHDL-1526") ; // VIPER #4399, #3472
        _msgs->Insert("signal assignments not allowed in entity statement part", "VHDL-1527") ; // VIPER #4507
// replaced by VHDL-1433        _msgs->Insert("illegal use of wait statement inside a function or inside a procedure whose parent is a function", "VHDL-1528") ; // VIPER #4518
        _msgs->Insert("name of entity to be configured by %s is same as configuration name", "VHDL-1529") ; // VIPER #4487
        _msgs->Insert("the attribute specification for design unit %s is not within the declarative part of the design unit", "VHDL-1530") ; // VIPER #4513
        _msgs->Insert("wait statements in branch statements are not supported for synthesis", "VHDL-1531") ; // VIPER #4549
// 12/2008 added:
        //_msgs->Insert("subprogram %s doesn't have any body", "VHDL-1532") ; // VIPER 4508 // Similar to 1255 so commented it out

// 02/2009 added:
        _msgs->Insert("%s: %s", "VHDL-1533") ; // VIPER 4667
        _msgs->Insert("component instantiation %s is already bound with primary unit %s, should not be bound again with %s", "VHDL-1534") ; // VIPER 4706

// 03/2009 added:
        _msgs->Insert("binding indication in configuration specification must have entity aspect", "VHDL-1535") ; // VIPER #4802
        _msgs->Insert("%s configuration specification for component %s must be last one given", "VHDL-1536") ; // VIPER 4738
// 05/2009 added:
        _msgs->Insert("verilog module port %s does not match with type %s of component port", "VHDL-1537") ; // VIPER 4938 (2)
// 07/2009 added:
        _msgs->Insert("cannot assign signal %s from within a subprogram", "VHDL-1538") ; // VIPER 4587
// 08/2009 added:
        _msgs->Insert("cannot call impure function %s from within pure function %s", "VHDL-1539") ; // VIPER 3414/3412
        _msgs->Insert("cannot call side-effect procedure %s from within pure function %s", "VHDL-1540") ; // VIPER 3414/3412
        _msgs->Insert("protected body %s has no protected type defined", "VHDL-1541") ; // Viper #5188
        _msgs->Insert("protected type method argument cannot be file or access type", "VHDL-1542") ; // Viper #5188
        _msgs->Insert("protected type method cannot return file or access type", "VHDL-1543") ; // Viper #5188

// 09/2009 added:
        _msgs->Insert("array type case expression must be of a locally static subtype", "VHDL-1544") ; // Viper #4452 , Viper #3699

// 10/2009 added:
        _msgs->Insert("protected type is not yet supported for synthesis", "VHDL-1545") ; // Viper #5188
        _msgs->Insert("access type cannot be a formal of mode out", "VHDL-1546") ; // Viper #4514

// 12/2009 added:
        _msgs->Insert("near %s ; prefix should denote a discrete or physical type or subtype", "VHDL-1547") ; // Viper #4619

// 01/2010 added:
        _msgs->Insert("cannot dereference a null access value", "VHDL-1548") ;
        _msgs->Insert("expression has %d elements ; formal %s expects %d", "VHDL-1549") ; // Viper 4449
        _msgs->Insert("formal %s associated individually was not in contiguous sequence", "VHDL-1560") ; // Viper 4395

// 04/2010 added:
        _msgs->Insert("error in protected region near %s", "VHDL-1561") ; // support of protection envelope, LRM 1076_2008 Section 24.1
        _msgs->Insert("binary dump database must be encrypted to secure `protect RTL", "VHDL-1562") ;
        _msgs->Insert("verilog module parameter %s does not match with type %s of component generic", "VHDL-1563") ;

// 05/2010 added:
        // RSM retired 2012/09 in favor of similar and more specific VHDL-1576 message
        // _msgs->Insert("construct illegal in this mode of VHDL", "VHDL-1564") ; // Used for Vhdl 2008 enhancements

// 07/2010 added:
        _msgs->Insert("cannot truncate illegal sized bit string literal %s", "VHDL-1565") ; // Vhdl 2008 LRM Section 15.8 : enhancements for sized bit-string-literal
        _msgs->Insert("use of library logical name WORK is illegal inside context declaration", "VHDL-1566") ; // Vhdl 2008 LRM Section 13.3
        _msgs->Insert("suffix %s in the context reference does not denote a context declaration", "VHDL-1567") ; // Vhdl 2008 LRM Section 13.4
        _msgs->Insert("VHDL 1076-2008 construct not yet supported", "VHDL-1568") ; // Viper #5918

// 08/2010 added:
        _msgs->Insert("aggregate target name has already been specified", "VHDL-1569") ; // VIPER Issue #3650

// 10/2010 added:
        _msgs->Insert("Illegal if generate syntax, else branch should appear last", "VHDL-1570") ; // VIPER Issue #5918

// 11/2010 added:
        _msgs->Insert("%s is not an uninstantiated package", "VHDL-1571") ; // VIPER Issue #5918
        //_msgs->Insert("package instantiation declaration is not yet supported in elaboration", "VHDL-1572") ; // VIPER Issue #5918 (Obsolete)
        _msgs->Insert("subprogram kind mismatch in subprogram instantiation", "VHDL-1573") ; // VIPER Issue #5918
        //_msgs->Insert("only resolution function name allowed here", "VHDL-1574") ; // VIPER Issue #5918
        _msgs->Insert("illegal nesting of resolution indication function", "VHDL-1575") ; // VIPER Issue #5918

// 12/2010 added :
        _msgs->Insert("this construct is only supported in VHDL 1076-2008", "VHDL-1576") ; // VIPER Issue #6256
        _msgs->Insert("subprogram name should be specified as actual for formal generic subprogram %s", "VHDL-1577") ; // VIPER Issue #5918
        _msgs->Insert("package name should be specified as actual for formal generic package %s", "VHDL-1578") ; // VIPER Issue #5918
        _msgs->Insert("only alternative label can be used with if/case generate statement", "VHDL-1579") ; // VIPER Issue #5918

// 01/2011 added :
        _msgs->Insert("question mark delimiter should be in both places after keyword case", "VHDL-1580") ; // VIPER Issue #5918
        _msgs->Insert("case expression type of matching case is not a bit or a std_ulogic or 1-dimensional array of bit/std_ulogic", "VHDL-1581") ; // VIPER Issue #5918
        _msgs->Insert("case expression type of matching case is not a bit/std_ulogic array type", "VHDL-1582") ; // VIPER Issue #5918

// 02/2011 added :
        _msgs->Insert("external name referring %s is not yet supported", "VHDL-1583") ; // VIPER Issue #5918
        _msgs->Insert("package path name should start with library logical name", "VHDL-1584") ; // VIPER Issue #5918
        _msgs->Insert("absolute path name should start with primary unit name", "VHDL-1585") ; // VIPER Issue #5918

// 03/2011 added:
        _msgs->Insert("/* in comment", "VHDL-1586") ; // VIPER Issue #1851

// 05/2011 added:
        _msgs->Insert("element type of the array declaration is same as the parent array type", "VHDL-1587") ; // Viper #6520
        _msgs->Insert("a function name may not be an uninstantiated subprogram", "VHDL-1588") ; // Vhdl 2008 LRM Section 9.3.4
// 06/2011 added:
        _msgs->Insert("variable in package inside process,subprogram or protected block cannot be 'shared'", "VHDL-1589") ; // Vhdl 2008 LRM Section 6.4.2.4
        _msgs->Insert("VHDL expression ignored due to errors", "VHDL-1590") ; // VIPER #6576: VHDL expression parsing

// 07/2011 added:
        _msgs->Insert("size mismatch with variable slice", "VHDL-1591") ; // VIPER #6597
        _msgs->Insert("keyword inertial can only be used in port map", "VHDL-1592") ; // VIPER #6645
        _msgs->Insert("function return type is not specified", "VHDL-1593") ; // VIPER #6664
// 09/2011 added:
        _msgs->Insert("non empty context clause cannot precede a context declaration", "VHDL-1594") ; // VIPER #6757
// 10/2011 added:
        _msgs->Insert("multiple clocking is not yet supported", "VHDL-1595") ; // VIPER #6633
        _msgs->Insert("single wait statement, no need to convert to explicit state machine", "VHDL-1596") ; // VIPER #6633
        _msgs->Insert("not converting statement with event control without edged expression", "VHDL-1597") ; // VIPER #6633
        _msgs->Insert("top level unit is instantiated, use VHDL_COPY_TOP_BEFORE_STATIC_ELAB compile flag", "VHDL-1599") ; // VIPER #6793
        _msgs->Insert("parameter should be followed by its list", "VHDL-1600") ; // VIPER #6653
// 11/2011 added:
        _msgs->Insert("possible infinite loop in some certain condition", "VHDL-1601") ; // VIPER #6879
// 12/2011 added:
        _msgs->Insert("illegal element in external name", "VHDL-1602") ; // VIPER #6912
        _msgs->Insert("external reference %s remains unresolved", "VHDL-1603") ; // VIPER #6912
        _msgs->Insert("enumeration array type not supported in cross language port/generic connection", "VHDL-1604") ; // VIPER #6896

// 03/2012 added :
        _msgs->Insert("library name %s of instantiated unit conflicts with visible identifier", "VHDL-1605") ; // VIPER #7059
        _msgs->Insert("external name referring %s is not bound", "VHDL-1606") ; // VIPER #5918 VHDL 2008
        _msgs->Insert("illegal subtype indication in alias declaration for external name", "VHDL-1607") ; // VIPER #5918 VHDL 2008
        _msgs->Insert("%s is not the top unit", "VHDL-1608") ; // VIPER #5918 VHDL 2008

// 04/2012 added :
        _msgs->Insert("%s cannot be used as a port", "VHDL-1609") ; // VIPER #7124 VHDL 2008
        _msgs->Insert("no package instance is specified for interface package element %s", "VHDL-1610") ; // VIPER #5918 VHDL 2008
        _msgs->Insert("default expression or subprogram is not specified in uninstantiated package for interface package element %s", "VHDL-1611") ; // VIPER #5918 VHDL 2008
        _msgs->Insert("formal generic package %s does not match with actual", "VHDL-1612") ; // VIPER #5918 VHDL 2008
        _msgs->Insert("incomplete sensitivity list specified, assuming completeness", "VHDL-1613") ;
        _msgs->Insert("uninstantiated package cannot be specified in use clause", "VHDL-1614") ; // VIPER #5918 VHDL 2008
//  05/2012 added :
        _msgs->Insert("case statement have same choice in multiple items", "VHDL-1615") ; // VIPER #7157 VHDL 2008
        _msgs->Insert("matching choice with value %s does not represent any value", "VHDL-1616") ; // VIPER #7157 VHDL 2008

// 07/2012 added:
        _msgs->Insert("value in initialization depends on signal %s",  "VHDL-1617") ; // VIPER #7344
        _msgs->Insert("element type of the record element is same as the parent record type",  "VHDL-1618") ; // VIPER #7348
        _msgs->Insert("pragma function %s cannot fit multi-bit result into single-bit return type %s", "VHDL-1619") ; // VIPER #7346

// 08/2012 added:
        _msgs->Insert("block comment was not closed",  "VHDL-1620") ; // VIPER #7399

// 09/2012 added:
        _msgs->Insert("access value for %s is not allocated or already free'd",  "VHDL-1621") ; // VIPER #7292
        _msgs->Insert("condition in if generate must be static",  "VHDL-1622") ; // VIPER #7432
        _msgs->Insert("range in for generate must be static",  "VHDL-1623") ; // VIPER #7432
        _msgs->Insert("expression in case generate must be globally static",  "VHDL-1624") ; // VIPER #7432

// 10/2012 added:
        _msgs->Insert("group constituent %s must be %s",  "VHDL-1625") ; // VIPER #7447
        _msgs->Insert("%s group constituents in group declaration",  "VHDL-1626") ; // VIPER #7447
        _msgs->Insert("entity class entry with box must be the last one within the entity class entry list",  "VHDL-1627") ; // VIPER #7447
        _msgs->Insert("'%s' is not compiled in package %s", "VHDL-1628") ; // Viper 7458
        _msgs->Insert("record element %s is already constrained", "VHDL-1629") ; // Viper 7461

// 11/2012 added:
        _msgs->Insert("near %s ; prefix should denote an object or object alias", "VHDL-1630") ; // VIPER #7522: vhdl_2008: new predefined attribute
        _msgs->Insert("%s was created using an incompatible version_number, vhdl 2008 backward compatibility is not supported for this version", "VHDL-1631") ; // VIPER #7505: vhdl_2008: new class VhdlGenericTypeId
        _msgs->Insert("choice others is not permitted when aggregate expression is of the aggregate type", "VHDL-1632") ; // Vhdl 2008: Array aggregate support
        _msgs->Insert("array aggregate elements must be all named or all positional", "VHDL-1633") ; // Vhdl 2008: Array aggregate support
        _msgs->Insert("choice direction does not match with the context direction", "VHDL-1634") ; // Vhdl 2008: Array aggregate support
        _msgs->Insert("signature not allowed for non-overloadable entity", "VHDL-1635") ; // VIPER #7535
        _msgs->Insert("default expression of interface object is not globally static", "VHDL-1636") ; // VIPER #7536
        _msgs->Insert("matching case selector expression contains meta value '-'", "VHDL-1637") ; // VIPER #7539
        _msgs->Insert("actual values for generic %s do not match between package instantiation and interface package", "VHDL-1638") ; // VIPER #7479 // VIPER #7652 : Fixed typo

// 12/2012 added:
        // VIPER #7559: Add Psl specific messages in the message table:
        _msgs->Insert("analyzing psl unit %s", "VHDL-1639") ;
        _msgs->Insert("what to do in this case??", "VHDL-1640") ;
        _msgs->Insert("elaboration of directive failed", "VHDL-1641") ;
        _msgs->Insert("Strong Fairness directive is not supported", "VHDL-1642") ;
        _msgs->Insert("range must be of direction 'to'", "VHDL-1643") ;
        _msgs->Insert("low bound of range must be a non-negative number", "VHDL-1644") ;
        _msgs->Insert("high bound of the range must be greater or equal to low bound", "VHDL-1645") ; // VIPER #7652 : Fixed typo
        _msgs->Insert("must be a non-negative number", "VHDL-1646") ;
        _msgs->Insert("%s is not a PSL operator", "VHDL-1647") ;
        _msgs->Insert("count for operator %s must be a number", "VHDL-1648") ;
        _msgs->Insert("count for operator %s must be a positive number", "VHDL-1649") ;
        _msgs->Insert("operator %s requires a range", "VHDL-1650") ;
        _msgs->Insert("operator %s requires finite range", "VHDL-1651") ;
        _msgs->Insert("repetition count must be a non-negative number", "VHDL-1652") ;
        _msgs->Insert("repetition range must be non-negative", "VHDL-1653") ;
        _msgs->Insert("repetition count must be a positive number", "VHDL-1654") ;
        _msgs->Insert("repetition range must be positive", "VHDL-1655") ;
        _msgs->Insert("operator %s requires a finite positive range", "VHDL-1656") ;
        _msgs->Insert("PSL Fairness directive not supported", "VHDL-1657") ;
        _msgs->Insert("replicator values must be a statically evaluable integer or boolean expression", "VHDL-1658") ; // VIPER #7652 : Fixed typo
        _msgs->Insert("%s is not allowed in package declaration", "VHDL-1659") ; // VIPER #7620
        _msgs->Insert("range expression expected here", "VHDL-1660") ; // VIPER #7619

// 1/2012 added:
        _msgs->Insert("VHDL unit '%s' not found in SQL database '%s'; cannot restore", "VHDL-1661") ;
// 02/2013 added :
        _msgs->Insert("unaffected is not allowed in sequential signal assignment statement", "VHDL-1662") ; // VIPER #7644

// 2/2012 added:
        _msgs->Insert("type mismatch in %s", "VHDL-1663") ; // VIPER #7717
        _msgs->Insert("ambiguous type in %s, %s", "VHDL-1664") ; // VIPER #7717
        _msgs->Insert("character %s of type %s is not expected here", "VHDL-1665") ; // VIPER #7717
        _msgs->Insert("source of subprogram instantiation is not an uninstantiated subprogram", "VHDL-1666") ; // VIPER #7730

// 3/2012 added:
        _msgs->Insert("target of signal force must be a signal", "VHDL-1667") ; // VIPER #7731
        _msgs->Insert("aggregate is not allowed as the target of signal %s", "VHDL-1668") ; // VIPER #7731
        _msgs->Insert("design unit %s contains uninitialized generic(s)", "VHDL-1669") ; // VIPER #7727
        _msgs->Insert("cannot be considered as top level, ignoring", "VHDL-1670") ; // VIPER #7727
        _msgs->Insert("already bounded psl unit %s cannot be bound again", "VHDL-1671") ; // Embedded PSL Support Viper 7766
        _msgs->Insert("psl assert does not have severity", "VHDL-1672") ; // Embedded PSL Support Viper 7766

// 3/2013 added:
        _msgs->Insert("No library search path could be found. Please set a search path to where unit can be saved.", "VHDL-1673") ; // SQLManager support
        _msgs->Insert("saving unit %s.%s into %s", "VHDL-1674") ; // SQLManager support
        _msgs->Insert("saving unit %s.%s.%s as module %s into %s", "VHDL-1675") ; // SQLManager support
        _msgs->Insert("unit %s does not have a generic named %s to override", "VHDL-1676") ; // VIPER #7727
        _msgs->Insert("protected type %s body is already defined", "VHDL-1677") ; // VIPER #7775
        _msgs->Insert("argument of operator %s cannot be an OTHERS aggregate", "VHDL-1678") ; // VIPER # 7081

// 5/2013 added:
        _msgs->Insert("suffix of protected type name cannot be 'all'", "VHDL-1679") ; // VIPER #7893
        _msgs->Insert("element %s is not in protected type %s", "VHDL-1680") ; // VIPER #7893

// 6/2013 added:
        _msgs->Insert("shared variables must be of a protected type", "VHDL-1681") ; // VIPER #7921
        _msgs->Insert("statement is not synthesizable since it does not hold its value under same clock-edge condition", "VHDL-1682") ; // VIPER #7800

// 8/2013 added:
        // _msgs->Insert("unknown foreign directive %s", "VHDL-1683") ;
        _msgs->Insert("argument out of valid domain in function %s", "VHDL-1684") ;

// 9/2013 added:
        _msgs->Insert("bad record element constraint", "VHDL-1685") ; // VIPER #8085 : illegal record element constraint

// 10/2013 added:
        _msgs->Insert("signal %s is used in subtype-indication/type-definition", "VHDL-1686") ; // VIPER #8148 : signal in subtype indication
        //_msgs->Insert("hier tree creation not supported for vhdl design, treating unit %s as blackbox", "VHDL-1687") ; // HierTree Creation

// 11/2013 added:
        _msgs->Insert("%s was previously created using a non-PSL-enabled VHDL analyzer, and since the current VHDL analyzer is PSL-enabled, there is a high probability that a restore error will occur due to newer VHDL 2008 constructs", "VHDL-1688") ;

// 01/2014 added:
        _msgs->Insert("an index in an external name must be a globally static expression", "VHDL-1689") ; // VIPER #8330
        _msgs->Insert("%s is not the label of a generate", "VHDL-1690") ; // VIPER #8330
        _msgs->Insert("index value %s is out of range for for-generate %s", "VHDL-1691") ; // VIPER #8330
        _msgs->Insert("vhdl expression cannot be converted to verilog expression", "VHDL-1692") ; // hier_tree
// 03/2014
        _msgs->Insert("partial association of component generic '%s' with formal '%s' is not supported in static elaboration", "VHDL-1693") ; //hier_tree
        _msgs->Insert("this type hier name is not supported yet", "VHDL-1694") ; //hier_tree
    }
    return (const char*)_msgs->GetValue(msg) ;
}

/*static*/ void
VhdlNode::ResetMessageMap()
{
    delete _msgs ;
    _msgs = 0 ;
}

unsigned
VhdlTreeNode::IsLegalIdentifier(const char *str)
{
    // Determine if string is a legal VHDL identifier name
    if (!str || !*str) return 0 ;

    // If str begins and ends with a '\', then this is a legal escaped identifier
    if (str[0] == '\\' && str[Strings::len(str) - 1] == '\\') return 1 ;

    // Some identifiers can be bit or character literal. These identifiers are not escaped.
    if ((Strings::len(str) == 3) && (str[0] == '\'') && (str[2] == '\'')) return 1 ;

    // Operator names such as "abs" or "+" can be represented by an identifier.
    // In that case operator names are not escaped.
    if ((Strings::len(str) >= 3) && (str[0] == '"') && (str[Strings::len(str) - 1 ] == '"')) return 1 ;

    // First character should be [a-zA-Z]
    if (!isalpha(*str)) return 0 ;

    // Following characters should be [a-zA-Z0-9_]
    while (*++str) {
        if (!isalpha(*str) && !isdigit(*str) && (*str!='_')) return 0 ;
    }

    // Last character should not be '_'
    if (*--str == '_') return 0 ;

    return 1 ; // String is a legal identifier
}

char *
VhdlTreeNode::CreateLegalIdentifier(const char *str)
{
    if (!str || !*str) return 0 ;

    if (IsLegalIdentifier(str)) {
        // Since this string is already legal, just return a copy of it.
        return Strings::save(str) ;
    } else {
        // This str is not a legal VHDL identifier name, therefore we have
        // to escape it.  Any present back-slashes need to be first back-slashed,
        // if they aren't already.

        // First check if str already contains a back-slash
        if (strchr(str, '\\')) {
            // We need to back-slash any current back-slashes in order to
            // create a legal VHDL identifier.
            const char *p = str ;
            char *buf = Strings::allocate(Strings::len(str) + Strings::len(str)/2) ;
            char *p2 = buf ;
            while (p && *p) {
                *(p2++) = *(p++) ;
                if (p && *(p-1) == '\\' && *p != '\\') *(p2++) = '\\' ;
            }
            *p2 = '\0' ;
            char *tmp = Strings::save("\\", buf, "\\") ;
            Strings::free(buf) ;
            return tmp ;
        } else {
            return Strings::save("\\", str, "\\") ;
        }
    }
}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Conversion of vhdl package to verilog package
VhdlRange* VhdlNode::CreateConversionRange(linefile_type line_file)  // Create vhdl special handling range
{
    VhdlRange *range = new VhdlRange(0, VHDL_CONVERSION_HANDLING, 0) ;
    range->SetLinefile(line_file) ;
    return range ;
}
#endif

