/*
 *
 * [ File Version : 1.122 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlSpecification.h"

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Strings.h"

#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "VhdlIdDef.h"
#include "VhdlName.h"
#include "VhdlIdDef.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlSpecification::VhdlSpecification() : VhdlTreeNode() { }
VhdlSpecification::~VhdlSpecification() { }

unsigned VhdlSpecification::IsFunction() const  { return 0 ; }
unsigned VhdlSpecification::IsProcedure() const { return 0 ; }
unsigned VhdlFunctionSpec::IsFunction() const   { return 1 ; }
unsigned VhdlProcedureSpec::IsProcedure() const { return 1 ; }

VhdlProcedureSpec::VhdlProcedureSpec(VhdlIdDef *designator, Array *generic_clause, Array *generic_map_aspect, Array *formal_parameter_list, VhdlScope *local_scope, unsigned has_explicit_param)
  : VhdlSpecification(),
    _designator(designator),
    _formal_parameter_list(formal_parameter_list),
    _local_scope(local_scope),
    _generic_clause(generic_clause),
    _generic_map_aspect(generic_map_aspect),
    _has_explicit_param(has_explicit_param)
{
    VERIFIC_ASSERT(_designator) ;

    if ((_generic_clause || _generic_map_aspect) && !vhdl_file::IsVhdl2008()) Error("this construct is only supported in VHDL 1076-2008") ;

    // Declare the procedure
    // Get the types of the formals
    VhdlInterfaceDecl *decl ;
    unsigned i, j ;
    Array *args = new Array() ;
    Array *ids ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(formal_parameter_list, i, decl) {
        if (!decl) continue ;
        ids = decl->GetIds() ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            args->Insert(id) ;
        }
    }

    // Accumulate generics and set as such
    Array *generic_list = 0 ;
    if (_generic_clause) {
        generic_list = new Array(_generic_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these generics to the order-based generics array
            if (ids) generic_list->Append(ids) ;
            id = decl->GetId() ; // interface type/subprogram and package
            if (id) generic_list->InsertLast(id) ;
        }
    }

    _designator->DeclareProcedure(args, generic_list) ;

    if (_generic_map_aspect) _designator->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;

    // RD 9/2011: Moved declaration-body linking to caller (VhdlSubprogramBody constructor) after VIPER 6754
    // Here, just set the Spec() pointer in the designator to this specification
    _designator->SetSpec(this) ;

    // RD 10/2011: VIPER 6827: We have to link specs before subprogram body is executed,
    // so moved linking code back here.

    // Do homograph checking in the scope where this subprogram is declared :
    VhdlScope *scope = (_local_scope) ? _local_scope->Upper() : 0 ;

    // Find all visible identifiers in this scope :
    Set r(POINTER_HASH) ;
    if (scope) (void) scope->FindAllLocal(designator->Name(),r) ;

    VhdlIdDef *exist ;
    SetIter si ;
    FOREACH_SET_ITEM(&r, si, &exist) {
        if (exist==designator) continue ; // forget about myself
        if (designator->IsHomograph(exist)) {
            // It might be a 'specification'. So let it go in if it is.
            if (exist->IsPredefinedOperator()) {
                // Overwriting a predefined operator that is declared in this region
                // Remove the predefined operator from the scope (LRM ...)
                if (scope) scope->Undeclare(exist) ;
                // Set the fact that it is a re-written predefined operator. This flag is used in
                // VhdlScope::FindAllIncluded. Vhdl 2008 testsuite 12.4
                _designator->SetOverWrittenPredefinedOperator() ;
            } else if (!exist->IsSubprogram() || exist->Body() || exist->IsAlias()) { // alias here for VIPER 2693.
                // There is already an id (not subprogram) with this profile
                designator->Error("a homograph of %s is already declared in this region", designator->Name()) ;
                exist->Info("%s is declared here",exist->Name()) ;
                // There are now two homographic ids in the scope (declaration and body). Remove mine (body).
                if (scope) scope->Undeclare(designator) ;
            } else {
                // Found a prior 'declaration' of the subprogram specification.
                // Link the two specifications to each other :
                VhdlSpecification *specification = exist->Spec() ;

                // From here on, the subprogram will be accessed via the existing id (spec).
                // Set the link between the two identifiers :
                exist->SetSpec(this) ; // Let 'exist' point to my specification
                designator->SetSpec(specification) ; // and let my designator point to exist's specification

                // Undeclare me from the scope, so that any future scope search will point to 'exit'.
                if (scope) scope->Undeclare(designator) ;

                // The rest (setting body and conformance check etc) will be done at caller level (when we know how this spec is used)
                break ; // Done linking, don't try to link me to another one.
            }
        }
    }
}

VhdlProcedureSpec::~VhdlProcedureSpec()
{
    // uncouple spec-body loop
    VhdlSpecification *specification = (_designator) ? _designator->Spec() : 0 ;
    if (specification && (specification != this)) {
        // There is a spec-link. Undo it (set the spec pointer back to it's own spec) :
        VhdlIdDef *exist = specification->GetId() ;
        if (exist) {
            exist->SetSpec(specification) ;
            exist->SetBody(0) ;
        }
    }

    // Delete the contents of the construct
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list,i,elem) delete elem ;
    delete _formal_parameter_list ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
    // Delete the id after the scope that owns it.
    delete _designator ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,elem) delete elem ;
    delete _generic_clause ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,elem) delete elem ;
    delete _generic_map_aspect ;
}

VhdlFunctionSpec::VhdlFunctionSpec(unsigned pure_impure, VhdlIdDef *designator, Array *generic_clause, Array *generic_map_aspect, Array *formal_parameter_list, VhdlName *return_type, VhdlScope *local_scope, unsigned has_explicit_param)
  : VhdlSpecification(),
    _pure_impure(pure_impure),
    _designator(designator),
    _formal_parameter_list(formal_parameter_list),
    _return_type(return_type),
    _local_scope(local_scope),
    _generic_clause(generic_clause),
    _generic_map_aspect(generic_map_aspect),
    _has_explicit_param(has_explicit_param)
{
    VERIFIC_ASSERT(_designator) ;

    if ((_generic_clause || _generic_map_aspect) && !vhdl_file::IsVhdl2008()) Error("this construct is only supported in VHDL 1076-2008") ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    if (designator) designator->SetSubtypeIndication(_return_type) ;
#endif

    // Declare the function
    // Get the types of the formals
    VhdlInterfaceDecl *decl ;
    unsigned i, j ;
    Array *args = new Array() ;
    Array *ids ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(formal_parameter_list, i, decl) {
        if (!decl) continue ;
        ids = decl->GetIds() ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            args->Insert(id) ;
        }
    }

    // Accumulate generics and set as such
    Array *generic_list = 0 ;
    if (_generic_clause) {
        generic_list = new Array(_generic_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these generics to the order-based generics array
            if (ids) generic_list->Append(ids) ;
            id = decl->GetId() ; // interface type/subprogram and package
            if (id) generic_list->InsertLast(id) ;
        }
    }

    // Get the return type
    // VIPER #7982: Call TypeInfer() instead of TypeMark() to resolve _return_type
    // as _return_type can be selected name but TypeMark() only supports type name:
    //VhdlIdDef *type = _return_type ? _return_type->TypeMark(): 0 ;
    VhdlIdDef *type = _return_type ? _return_type->TypeInfer(0, 0, VHDL_range,0) : 0 ;

    _designator->DeclareFunction(type, args, generic_list) ;

    if (_generic_map_aspect) _designator->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;

    // RD 9/2011: Moved declaration-body linking to caller (VhdlSubprogramBody constructor) after VIPER 6754
    // Here, just set the Spec() pointer in the designator to this specification
    _designator->SetSpec(this) ;

    // RD 10/2011: VIPER 6827: We have to link specs before subprogram body is executed,
    // so moved linking code back here.

    // Do homograph checking in the scope where this subprogram is declared :
    VhdlScope *scope = (_local_scope) ? _local_scope->Upper() : 0 ;

    // Find all visible identifiers in this scope :
    Set r(POINTER_HASH) ;
    if (scope) (void) scope->FindAllLocal(designator->Name(),r) ;

    VhdlIdDef *exist ;
    SetIter si ;
    FOREACH_SET_ITEM(&r, si, &exist) {
        if (exist==designator) continue ; // forget about myself
        if (designator->IsHomograph(exist)) {
            // It might be a 'specification'. So let it go in if it is.
            if (exist->IsPredefinedOperator()) {
                // Overwriting a predefined operator that is declared in this region
                // Remove the predefined operator from the scope (LRM ...)
                if (scope) scope->Undeclare(exist) ;
                _designator->SetOverWrittenPredefinedOperator() ;
            } else if (!exist->IsSubprogram() || exist->Body() || exist->IsAlias()) { // alias here for VIPER 2693.
                // There is already an id (not subprogram) with this profile
                designator->Error("a homograph of %s is already declared in this region", designator->Name()) ;
                exist->Info("%s is declared here",exist->Name()) ;
                // There are now two homographic ids in the scope (declaration and body). Remove mine (body).
                if (scope) scope->Undeclare(designator) ;
            } else {
                // Found a prior 'declaration' of the subprogram specification.
                // Link the two specifications to each other :
                VhdlSpecification *specification = exist->Spec() ;

                // From here on, the subprogram will be accessed via the existing id (spec).
                // Set the link between the two identifiers :
                exist->SetSpec(this) ; // Let 'exist' point to my specification
                designator->SetSpec(specification) ; // and let my designator point to exist's specification

                // Undeclare me from the scope, so that any future scope search will point to 'exist'.
                if (scope) scope->Undeclare(designator) ;

                // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
                if (exist->GetPragmaFunction()) designator->SetPragmaFunction(exist->GetPragmaFunction()) ;

                // The rest (setting body and conformance check) will be done at caller level (when we know how this spec is used)
                break ; // Done linking, don't try to link me to another one.
            }
        }
    }
}

VhdlFunctionSpec::~VhdlFunctionSpec()
{
    // uncouple spec-body loop
    VhdlSpecification *specification = (_designator) ? _designator->Spec() : 0 ;
    if (specification && (specification != this)) {
        // There is a spec-link. Undo it (set the spec pointer back to it's own spec) :
        VhdlIdDef *exist = specification->GetId() ;
        if (exist) {
            exist->SetSpec(specification) ;
            exist->SetBody(0) ;
        }
    }

    // Delete the contents of the construct
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list,i,elem) delete elem ;
    delete _formal_parameter_list ;
    delete _return_type ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
    // Delete the id after the scope that owns it.
    delete _designator ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,elem) delete elem ;
    delete _generic_clause ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,elem) delete elem ;
    delete _generic_map_aspect ;
}

VhdlComponentSpec::VhdlComponentSpec(Array *id_list, VhdlName *name)
  : VhdlSpecification(),
    _id_list(id_list),
    _name(name),
    _component(0),
    _scope(0)
{
    // Find the component this component spec refers to :
    // LRM PROBLEM :
    // LRM 5.2 specifies nothing special about searching for the component name,
    // so we should just be able to pick it up with normal search in present scope.
    // Unfortunately, this rule is complicated.
    // LRM 10.3 (line 160-180) is very complex, and has special rules for visibility of
    // configuration blocks.

    // Issue 1377 use to require special code here since a "use work.all" clause would
    // block components that are declared in the configured blocks.
    // Also issue 1442 has this problem.
    // For component spec's, look through the extended (block) scopes first,
    // before we follow the normal scoping rules.
    if (_present_scope->GetExtendedDeclAreaScope() && _present_scope->Upper() && _name && _name->Name()) {
        _component = _present_scope->GetExtendedDeclAreaScope()->FindSingleObject(_name->Name()) ;
    }

    // If we did not find it there, use regular scope search :
    if (!_component || !_component->IsComponent()) {
        if (_name) _component = _name->FindSingleObject() ;
    }

    // Since the above search mechanism is a bit out of the ordinary (normally (in comp instantiations) we use subroutine EntityAspect())
    // we need to make some adjustments that are also made there :
    // VIPER 5137: If the component id is an alias, look through it :
    if (_component && _component->IsAlias()) _component = _component->GetTargetId() ;

    // Check that it is a component name :
    if (_component && !_component->IsComponent()) {
        Error("%s is not a component", _component->Name()) ;
    }

    // Cannot do anything with the label list (id_list) yet.
    // They still need to be created later.
    // We need these at elaboration time, to configure the component.
    // So, save the present scope, so we can find the labels at elaboration time..
    _scope = _present_scope ;
}
VhdlComponentSpec::~VhdlComponentSpec()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_id_list,i,elem) delete elem ;
    delete _id_list ;
    delete _name ;
}

VhdlGuardedSignalSpec::VhdlGuardedSignalSpec(Array *signal_list, VhdlName *type_mark)
  : VhdlSpecification(),
    _signal_list(signal_list),
    _type_mark(type_mark)
{ }
VhdlGuardedSignalSpec::~VhdlGuardedSignalSpec()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_signal_list,i,elem) delete elem ;
    delete _signal_list ;
    delete _type_mark ;
}

VhdlEntitySpec::VhdlEntitySpec(Array *entity_name_list, unsigned entity_class)
  : VhdlSpecification(),
    _entity_name_list(entity_name_list),
    _entity_class(entity_class),
    _all_ids(0)
{
    // All entity spec resolving will be done in VhdlAttributeSpec::ResolveSpecs.
}
VhdlEntitySpec::~VhdlEntitySpec()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_entity_name_list,i,elem) delete elem ;
    delete _entity_name_list ;
    delete _all_ids ;
}

// Return the designator
VhdlIdDef *VhdlSpecification::GetId() const     { return 0 ; }
VhdlIdDef *VhdlFunctionSpec::GetId() const      { return _designator ; }
VhdlIdDef *VhdlProcedureSpec::GetId() const     { return _designator ; }
VhdlIdDef *VhdlComponentSpec::GetId() const     { return _component ; }

// VIPER #4992 : According to LRM section 2.1 : a function is impure if its specification
// contains the reserved word 'impure', otherwise it is said to be pure.
unsigned VhdlFunctionSpec::IsPure() const       { return (_pure_impure != VHDL_impure) ; }
unsigned VhdlSpecification::IsImpure() const    { return 0 ; }
unsigned VhdlFunctionSpec::IsImpure() const     { return (_pure_impure == VHDL_impure) ; }

/******************************************************************/
//  Resolve Specifications
/******************************************************************/

void VhdlEntitySpec::ResolveSpecs(VhdlScope *scope, VhdlIdDef *attr, VhdlExpression *attribute_value)
{
    // Set the attribute id and its value expression as back-pointers
    // into the attributes table of each identifier in this entity spec :
    // We need both the attribute, and the scope in which this spec resides :
    if (!attr || !scope) return ;

    //VERIFIC_ASSERT(!_all_ids) ; // We expect this routine to only get called once per entity spec during analysis.
    if (!_all_ids) _all_ids = new Set(POINTER_HASH) ;

    unsigned only_others = 0 ;
    switch (_entity_class) {
    case VHDL_label :
    case VHDL_procedure :
    case VHDL_function :
    case VHDL_literal :
    case VHDL_units :
        break ; // named entities can be used before declaration, process now
    default :
        // VIPER #6305: Already processed in Validate, need to process 'all' and 'others' again
        only_others = 1 ;
        break ;
    }
    // See where this attribute should apply to :
    unsigned i ;
    VhdlName *item ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_entity_name_list, i, item) {
        if (!item) continue ;
        // Check if this is ALL or OTHERS :
        if (item->IsAll() || item->IsOthers()) {
            if (_entity_name_list->Size()>1) {
                // This is not tested by yacc :
                item->Error("OTHERS or ALL should be the only entity name in the list") ;
            }

            Set all_ids(POINTER_HASH) ;
            scope->FindAllLocal(all_ids) ; // Viper #5694

            // Add the identifiers that match the specified class
            SetIter si ;
            FOREACH_SET_ITEM(&all_ids, si, &id) {
                if (id->EntityClass()!=_entity_class) continue ;
                if (id->IsAlias()) continue ; // VIPER #5187
                // Insert attribute id into entity's attribute map.
                if (!id->SetAttribute(attr, attribute_value)) {
                    if (item->IsAll()) Error("attribute %s has already been set on %s", attr->Name(), id->Name()) ;
                } else {
                    (void) _all_ids->Insert(id) ; // Add it.
                }
            }
            return ;  // Exit early.  Don't need to execute _all_ids loops at the end of function.
        } else if (only_others) {
            // VIPER #6305 : Already processed in 'Validate', ignore here
            return ;
        }

        // Find all ids that correspond to this entity name,
        // only in the 'immediately enclosing declarative part' :
        (void) item->FindObjectsLocal(scope, *_all_ids) ;

        // Viper 7745: Deviate from LRM Section 5.1 and comply with
        // popular simulators. LRM 5.1 says all ids in the scope should
        // have this attribute. However popular simulators do not add this
        // attribute if the entity class of the found id differ w.r.t
        // the _entity_class. They also do not error out unless they find
        // no id with the same entity class.
        SetIter si ;
        FOREACH_SET_ITEM_BACK(_all_ids, si, &id) {
            if (id->EntityClass() != _entity_class) {
               (void) _all_ids->Remove(id) ;
            }
        }
        // Check that at least one matches.
        if (_all_ids && _all_ids->Size() == 0) {
            if (!item->Name()) {
                // Might be a selected name or so. Not allowed in entity spec in LRM.
                item->Error("illegal name in specification") ;
            } else {
                item->Error("%s is not declared", item->Name()) ;
            }
        }
    }

    // Test identifiers against _entity_class (LRM 5.1)
    // This works now : signal kind is set immediately during parsing.
    SetIter si ;
    FOREACH_SET_ITEM(_all_ids, si, &id) {
        // Post Viper 7745: The below commented code will never become active and
        // hence commented out. However this error reporting is as per LRM 5.1. Hence
        // decided to preserve the code under comment. If this code needs to be
        // uncommented the 7745 fix should be removed.
        /*
        if (id->EntityClass() != _entity_class) {
            Error("%s is not a %s", id->Name(), EntityClassName(_entity_class)) ;
        }
        */
        // Insert attribute id into entity's attribute map.
        if (!id->SetAttribute(attr, attribute_value)) {
            Error("attribute %s has already been set on %s", attr->Name(), id->Name()) ;
        }
    }
}

VhdlIdDef *
VhdlComponentSpec::ResolveSpecs(VhdlBindingIndication *binding, unsigned incremental)
{
    // If 'incremental' flag is set, this means that the binding is 'incremental', from a component configuration,
    // as opposed to the primary binding from a component specification.
    // For primary binding, we set the primary binding backpointers in the (instantiation) labels,
    // while for incremental binding we only resolve and check the labels.
    //
    // IMPORTANT: Do NOT set the label backpointers if this is from a component configuration (incremental binding),
    // because that would cause a backpointer into a configuration, UP in the hierarchy.
    // That is a dependency nightmare, as well as incorrect : multiple configurations can provide incremental binding to the same instance label,
    // since that represents a different hierarchy tree.
    // Only during elaboration will the (incremental) bindings for component configurations be taken into consideration.

    // Return value only needed for 'incremental' binding (to parse component configurations with incremental binding constructs) :
    // In that case, return the ('full') binding entity of this component spec.
    // Full binding entity can either directly be the entity from the 'incremental' 'binding' (from the component configuration),
    // or the entity from 'primary binding' previously set from a component specification on the label(s),
    // or (VIPER 3852) In absence of that, use the 'default' binding entity of the component.

    // Also : Type-infer the incoming binding using the full entity aspect. VHDL requires that :
    // Cannot have a binding that has port assocs or generic assocs, but no full (entity aspect) binding is available.

    if (!_scope) return 0 ; //  need the scope in which this spec resides.
    if (!_component) return 0 ; // The component was not set at analysis time.

    // Get full binding from this binding if there is an entity aspect :
    VhdlBindingIndication *full_binding = (binding && binding->EntityAspect()) ? binding : 0 ;
    // if 'full_binding' is still 0, then we need to get it from the labels.

    // Now set the binding on the spec'ed labels
    unsigned i ;
    VhdlName *label_name ;
    VhdlIdDef *label ;
    VhdlStatement *label_statement ;
    FOREACH_ARRAY_ITEM(_id_list, i, label_name) {
        if (!label_name) continue ;
        // LRM inconsistency :
        // LRM 5.2 states that we should look for the labels in the
        // (current) "immediately enclosing declarative part"
        // CHECK ME : This should not include the 'extended' decl area of current scope.
        // But that is not possible, since in configurations the scope is always
        // external. Might have to do with the implicit scope assumptions of configurations.
        // RD: 5/2007: for (non-incremental) configuration specs, we should look for primary binding indications.
        // And these should always be in immediate declarative part (the architecture in which the spec occurs)
        // RD: 2/2008: Possibly, we should look for the labels in only in immediate scope for NON-incremental binding,
        // and only in extended scope for incremental bindings.
        //
        // For now, just look both in the extended decl area of the scope,
        // and in the immediate scope area _scope itself.
        if (label_name->IsAll() || label_name->IsOthers()) {
            // Set the binding on all instances of the component 'component'

            // look for labels in the decl area of the component spec.
            Map *decl_area = _scope->DeclArea() ;
            MapIter mi ;
            FOREACH_MAP_ITEM(decl_area, mi, 0, &label) {
                if (!label || !label->IsLabel()) continue ;

                // Check if this label instantiates the right component
                label_statement = label->GetStatement() ;
                if (!label_statement) continue ; // bad label
                if (label_statement->GetInstantiatedUnit() != _component) continue ;

                // 'others' : Check if there is already a primary binding. Then others does not apply?
                if (label_name->IsOthers() && label->GetPrimaryBinding()) {
                    continue ;
                }

                // Set the binding (if primary), or get full binding from the label.
                if (incremental) {
                    // Get full binding from the label if this incremental binding has none.
                    if (!full_binding) {
                        full_binding = label->GetPrimaryBinding() ;
                    }
                } else {
                    label->AddBinding(binding) ;
                }
            }
            // Do the same in the extended decl area (if there) :
            decl_area = _scope->ExtendedDeclArea() ;
            FOREACH_MAP_ITEM(decl_area, mi, 0, &label) {
                if (!label || !label->IsLabel()) continue ;

                // Check if this label instantiates the right component
                label_statement = label->GetStatement() ;
                if (!label_statement) continue ; // bad label
                if (label_statement->GetInstantiatedUnit() != _component) continue ;

                // 'others' : Check if there is already a primary binding. Then others does not apply?
                if (label_name->IsOthers() && label->GetPrimaryBinding()) {
                    continue ;
                }

                // Set the binding (if primary), or get full binding from the label.
                if (incremental) {
                    // Get full binding from the label if this incremental binding has none.
                    if (!full_binding) {
                        full_binding = label->GetPrimaryBinding() ;
                    }
                } else {
                    label->AddBinding(binding) ;
                }
            }
        } else {
            // Regular named label
            // Get the name
            // CHECK ME : Where do we check that the label is a simple identifier (entity tag) ?
            label = _scope->FindSingleObjectLocal(label_name->Name()) ;
            if (!label && _scope->GetExtendedDeclAreaScope()) label = _scope->GetExtendedDeclAreaScope()->FindSingleObjectLocal(label_name->Name()) ;
            if (!label) {
                if (!label_name->Name()) {
                    // Might be a selected name or so. Not allowed in component spec in LRM.
                    label_name->Error("illegal name in specification") ;
                } else {
                    label_name->Error("%s is not declared", label_name->Name()) ;
                }
                continue ;
            }

            // Set the label in the idref (need this in ComponentSpec::MatchLabel()).
            // RD 5/2007: Lets not set the IdDef backpointer. It causes problems in tree copying and static elaboration :
            // In static elab, an instance in a generate loop get replaced by a range of instances, resulting in stale pointers to the original instance.
            // So, instead, in MatchLabel, we compare label by name.
            // label_name->SetId(label) ;

            if (!label->IsLabel()) {
                label_name->Error("%s is not a label", label->Name()) ;
                continue ;
            }

            // Check that this label instantiates the same component as the one the binding refers to
            label_statement = label->GetStatement() ;
            if (!label_statement) continue ; // Something bad happened
            if (label_statement->GetInstantiatedUnit() != _component) {
                label_name->Error("%s does not instantiate component %s", label->Name(), _component->Name()) ;
                continue ;
            }

            // Set the binding (if primary), or get full binding from the label.
            if (incremental) {
                // Get full binding from the label if this incremental binding has none.
                if (!full_binding) {
                    full_binding = label->GetPrimaryBinding() ;
                }
            } else {
                label->AddBinding(binding) ;
            }
        }
    }

    // Now get the binding unit (the primary unit to which the full_binding points)
    VhdlIdDef *binding_unit = (full_binding) ? full_binding->EntityAspect() : 0 ;

    // VIPER 3852 : If no explicit entity binding was given in a component configuration (incremental==1),
    // then the 'default' binding of the component applies (to find the architecture in).
    if (incremental && !binding_unit && !binding) {
        binding_unit = _scope->FindDefaultBinding(_component->Name(),_component) ;
    }

    // Now type-infer the binding indication.
    // Binding indication has the entity formals in the formals of the assoc list,
    // and component formals in the actuals of the assoc list.

    // So, set 'actual_binding_scope' to the component's scope.
    // That's needed for LRM 10.3 (h) and (i) :
    // we need the component formals to be visible in the actuals of the associations in the binding indication

    // Pass-in that we are from a binding indication. LRM 5.2.1 shows that
    // primary bindings can be 'open', as long as they are later incrementally bound.

    // Set the 'unit' to the bound entity for if this is a configuration.
    // Otherwise, the LocalScope inside this routine will not work.
    if (binding) {
        // Before we resolve the bindings, set the present_scope to this component spec's scope.
        // This is needed, because ResolveSpecs is often called after present_scope is already re-set.

        // Viper #4802, LRM: 5.2.1 : When a binding indication is used in a
        // configuration specification, it is an error if entity aspect is absent
        if (!incremental && !binding->GetEntityAspectNameNode()) binding->Error("binding indication in configuration specification must have entity aspect") ;

        VhdlScope *save_scope = _present_scope ;
        _present_scope = _scope ;

        // Get generic and port map aspects of this binding
        Array *generic_map_aspect = binding->GetGenericBinding() ;
        Array *port_map_aspect = binding->GetPortBinding() ;

        // If binding_unit is NOT there now, then this component is not fully bound..
        // CHECK ME : Error out if there is no full binding but there is a generic/port map aspect ?

        // Can do more checks here if 'primary binding' is set :
        // Check LRM 5.2.1.1...
        // If this incremental binding has an entity aspect (and primary binding too),
        // then it is actually no longer considered 'incremental'. It would be a full binding.

        // If binding unit is a configuration, then decent to the binding entity :
        if (binding_unit) binding_unit = binding_unit->GetEntity() ;

        // Now type-infer the association lists :
        if (binding_unit) binding_unit->TypeInferAssociationList(generic_map_aspect, VHDL_generic, _component->LocalScope(), this) ;
        if (binding_unit) binding_unit->TypeInferAssociationList(port_map_aspect, VHDL_port, _component->LocalScope(), this) ;

        // Set scope back :
        _present_scope = save_scope ;
    }

    // return the primary unit that binding indicates.
    return binding_unit ;
}

// Check if a subprogram is uninstantiated: 2008 LRM 4.2.1
unsigned
VhdlFunctionSpec::IsUninstantiatedSubprogram() const
{
    if (_generic_clause && _generic_clause->Size() && !_generic_map_aspect) return 1 ;
    return 0 ;
}

unsigned
VhdlProcedureSpec::IsUninstantiatedSubprogram() const
{
    if (_generic_clause && _generic_clause->Size() && !_generic_map_aspect) return 1 ;
    return 0 ;
}

void
VhdlComponentSpec::CheckLabel(Map *all_other_label)
{
    // Viper 4738. Need to check whether :all or :other is the last
    // label. LRM Section: 5.2
    if (!all_other_label) return ;
    const char *component_name = _name ? _name->Name() : 0 ;
    VhdlName *all_other_label_name = (VhdlName*)all_other_label->GetValue(component_name) ;
    if (all_other_label_name) {
       Error("%s configuration specification for component %s must be last one given", all_other_label_name->Name(), component_name) ;
    }
    unsigned i ;
    VhdlName *label_name ;
    FOREACH_ARRAY_ITEM(_id_list, i, label_name) {
        if (!label_name) continue ;
        if (label_name->IsAll() || label_name->IsOthers()) (void) all_other_label->Insert(component_name, label_name) ;
    }
}

unsigned
VhdlComponentSpec::MatchLabel(VhdlIdDef *label)
{
    if (!label) return 0 ;

    // Check if this label instantiates the right component
    VhdlStatement *label_statement = label->GetStatement() ;
    if (!label_statement) return 0 ; // bad label
    if (label_statement->GetInstantiatedUnit() != _component) return 0 ;

    // FIX ME : Where do we check component consistency ?
    unsigned i ;
    VhdlName *label_name ;
    // VhdlIdDef *spec_label ;
    FOREACH_ARRAY_ITEM(_id_list, i, label_name) {
        if (!label_name) continue ;
        if (label_name->IsAll()) {
            return 1 ; // always matches
        }
        if (label_name->IsOthers()) {
            return 2 ; // return 2, so that at caller level we know it is an 'others' match
        }

        // Check if this label matches by name (we do not want to rely on any back-pointers set).

        // and see if it is the same as the one we want.

        // RD 5/2007: Lets not set the IdDef backpointer. It causes problems in tree copying and static elaboration :
        // In static elab, an instance in a generate loop get replaced by a range of instances, resulting in stale pointers to the original instance.
        // So, instead, in MatchLabel, we compare label by name.

        // spec_label = label_name->GetId() ;
        // if (spec_label == label) return 1 ;
        if (Strings::compare(label_name->Name(), label->Name())) return 1 ;
    }
    return 0 ;
}

/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlSpecification::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    return sig ;
}

unsigned long
VhdlProcedureSpec::CalculateSignature() const
{
    unsigned long sig = VhdlSpecification::CalculateSignature() ;
    if (_designator) sig = HASH_VAL(sig, _designator->CalculateSignature()) ;
    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }

    // Viper 6653
    if (_has_explicit_param) sig = HASH_VAL(sig, _has_explicit_param) ;

    return sig ;
}

unsigned long
VhdlFunctionSpec::CalculateSignature() const
{
    unsigned long sig = VhdlSpecification::CalculateSignature() ;
    sig = HASH_VAL(sig, _pure_impure) ;
    if (_designator) sig = HASH_VAL(sig, _designator->CalculateSignature()) ;
    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    if (_return_type) sig = HASH_VAL(sig, _return_type->CalculateSignature()) ;
    // Viper 6653
    if (_has_explicit_param) sig = HASH_VAL(sig, _has_explicit_param) ;
    return sig ;
}

unsigned long
VhdlComponentSpec::CalculateSignature() const
{
    unsigned long sig = VhdlSpecification::CalculateSignature() ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    if (_name) sig = HASH_VAL(sig, _name->CalculateSignature()) ;
    // back-pointer : if (_component) sig = HASH_VAL(sig, _component->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlGuardedSignalSpec::CalculateSignature() const
{
    unsigned long sig = VhdlSpecification::CalculateSignature() ;
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_signal_list, i, name) {
        if (name) sig = HASH_VAL(sig, name->CalculateSignature()) ;
    }
    if (_type_mark) sig = HASH_VAL(sig, _type_mark->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlEntitySpec::CalculateSignature() const
{
    unsigned long sig = VhdlSpecification::CalculateSignature() ;
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_entity_name_list, i, name) {
        if (name) sig = HASH_VAL(sig, name->CalculateSignature()) ;
    }
    sig = HASH_VAL(sig, _entity_class) ;
    return sig ;
}

/******************************************************************/
//  Validate Specifications
/******************************************************************/
void VhdlSpecification::Validate(VhdlScope * /*scope*/, VhdlIdDef * /*attr*/, VhdlExpression * /*attribute_value*/) { }
void VhdlEntitySpec::Validate(VhdlScope *scope, VhdlIdDef *attr, VhdlExpression *attribute_value)
{
    if (!attr || !scope) return ; // Cannot validate anything without scope

    // It is not necessary to declare specified named entities before using here
    // for every entity class. For some entity classes like signal, variable,
    // constant named entities should be an existing one.
    switch (_entity_class) {
    case VHDL_label :
    case VHDL_procedure :
    case VHDL_function :
    case VHDL_literal :
    case VHDL_units :
        return ; // named entities can be used before declaration, return
    default :
        break ;
    }

    // These named entities should be declared before specifying here :
    // VIPER #3645 : Produce error if not declared yet.
    Set ids(POINTER_HASH) ;
    unsigned i ;
    VhdlName *item ;
    SetIter j ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM (_entity_name_list, i, item) {
        if (!item || item->IsAll()) continue ;
        if (!_all_ids) _all_ids = new Set(POINTER_HASH) ;
        if (item->IsOthers()) {
            // VIPER #6305: entity name list with other should be processed
            // during 'ResolveSpecs' when the declarative part of enclosing scope
            // is fully parsed. Here populate already declared ids, so that we can
            // catch error when attribute spec is defined for same entity later.
            if (_entity_name_list->Size()>1) {
                // This is not tested by yacc :
                item->Error("OTHERS or ALL should be the only entity name in the list") ;
            }

            Set all_ids(POINTER_HASH) ;
            scope->FindAllLocal(all_ids) ; // Viper #5694

            // Add the identifiers that match the specified class
            SetIter si ;
            FOREACH_SET_ITEM(&all_ids, si, &id) {
                if (id->EntityClass()!=_entity_class) continue ;
                if (id->IsAlias()) continue ; // VIPER #5187
                // Insert attribute id into entity's attribute map.
                if (!id->SetAttribute(attr, attribute_value)) {
                    if (item->IsAll()) Error("attribute %s has already been set on %s", attr->Name(), id->Name()) ;
                } else {
                    (void) _all_ids->Insert(id) ; // Add it.
                }
            }
            return ;  // Exit early.  Don't need to execute _all_ids loops at the end of function.
        }
        ids.Reset() ;
        // Find all ids only in the 'immediately enclosing declarative part' :
        (void) item->FindObjectsLocal(scope, ids) ;
        if (ids.Size() == 0) {
            if (!item->Name()) {
                // Might be a selected name or so. Not allowed in entity spec in LRM.
                item->Error("illegal name in specification") ;
            } else {
                item->Error("%s is not declared", item->Name()) ;
            }
        }
        FOREACH_SET_ITEM(&ids, j, &id) {
            if (!id) continue ;
            // VIPER #4513 : LRM section 5.1 (60) : an attribute specification for an attribute
            // of a design unit must appear immediately within the declarative part of that design unit.
            if (id->IsDesignUnit() && (id != scope->GetContainingDesignUnit())) {
                item->Error("the attribute specification for design unit %s is not within the declarative part of the design unit", id->Name()) ;
            }
            // VIPER #6305 : Populate _all_ids here to catch only ids defined before
            // this attribute specification
            (void) _all_ids->Insert(id) ;
            if (id->EntityClass() != _entity_class) {
                Error("%s is not a %s", id->Name(), EntityClassName(_entity_class)) ;
            }
            // Insert attribute id into entity's attribute map.
            if (!id->SetAttribute(attr, attribute_value)) {
                Error("attribute %s has already been set on %s", attr->Name(), id->Name()) ;
            }
        }
    }
}

