
/*
 *
 * [ File Version : 1.57 - 2014/03/12 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"
#include "Set.h"
#include "Strings.h"
#include "Message.h"

#include "vhdl_file.h"

#include "VhdlCopy.h"
#include "VhdlIdDef.h"
#include "VhdlUnits.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlMisc.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlStatement.h"

#include "VhdlScope.h"
#include "VhdlValue_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlMapForCopy::VhdlMapForCopy(hash_type type, unsigned init_bucket) :
    Map(type, init_bucket),
    _not_switch_formal(0),
    _for_lib_copy(0),
    _old_entity_id(0),
    _new_entity_id(0)
{}
VhdlIdDef *VhdlMapForCopy::GetCopiedEntityId(const VhdlIdDef *old_id) const
{
    if (old_id == _old_entity_id) return _new_entity_id ;
    return 0 ;
}
void
VhdlMapForCopy::SetCopiedId(const VhdlIdDef *old_id, const VhdlIdDef *new_id)
{
    if (!old_id || !new_id) return ;
    (void) Insert(old_id, new_id) ;
}

VhdlIdDef *
VhdlMapForCopy::GetCopiedId(VhdlIdDef *old_id) const
{
    if (!old_id) return 0 ;
    const MapItem *ele = GetItem(old_id) ;
    VhdlIdDef *new_id = 0 ;
    if (ele) {
        new_id = (VhdlIdDef *)ele->Value() ;
    } else {
        new_id = old_id ;
    }
    return new_id ;
}

void
VhdlMapForCopy::SetCopiedScope(const VhdlScope *old_scope, const VhdlScope *new_scope)
{
    if(!old_scope || !new_scope) return ;
    (void) Insert(old_scope, new_scope) ;
}
VhdlScope *
VhdlMapForCopy::GetCopiedScope(VhdlScope *old_scope) const
{
    if (!old_scope) return 0 ;
    const MapItem *ele = GetItem(old_scope) ;
    VhdlScope *new_scope = 0 ;
    if (ele) {
        new_scope = (VhdlScope *)ele->Value() ;
    } else {
        new_scope = old_scope ;
    }
    return new_scope ;
}

void
VhdlMapForCopy::SetCopiedSpecification(const VhdlSpecification *old_spec, const VhdlSpecification *new_spec)
{
    if (!old_spec || !new_spec) return ;
    (void) Insert(old_spec, new_spec) ;
}
VhdlSpecification *
VhdlMapForCopy::GetCopiedSpecification(VhdlSpecification *old_spec) const
{
    if (!old_spec) return 0 ;
    const MapItem *ele = GetItem(old_spec) ;
    VhdlSpecification *new_spec = 0 ;
    if(ele) {
        new_spec = (VhdlSpecification *)ele->Value() ;
    } else {
        new_spec = old_spec ;
    }
    return new_spec ;
}

void
VhdlMapForCopy::SetCopiedBinding(const VhdlBindingIndication *old_binding, const VhdlBindingIndication *new_binding)
{
    if (!old_binding || !new_binding) return ;
    (void) Insert(old_binding, new_binding) ;
}
VhdlBindingIndication *
VhdlMapForCopy::GetCopiedBinding(VhdlBindingIndication *old_binding) const
{
    if (!old_binding) return 0 ;
    const MapItem *ele = GetItem(old_binding) ;
    VhdlBindingIndication *new_binding = 0 ;
    if(ele) {
        new_binding = (VhdlBindingIndication *)ele->Value() ;
    } else {
        new_binding = old_binding ;
    }
    return new_binding ;
}

Array *
VhdlMapForCopy::CopyDeclarationArray(const Array *decl_array)
{
    if (!decl_array) return 0 ;
    Array *new_decl_array = new Array(decl_array->Size()) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(decl_array, i, decl) {
        VhdlDeclaration *new_decl = decl ? decl->CopyDeclaration(*this) : 0 ;
        new_decl_array->InsertLast(new_decl) ;
        if (new_decl && new_decl->IsPrimaryUnit()) new_decl->SetContainerArray(new_decl_array) ;
    }
    return new_decl_array ;
}
Array *
VhdlMapForCopy::CopyConfigurationItemArray(const Array *config_array)
{
    if (!config_array) return 0 ;
    Array *new_config_array = new Array(config_array->Size()) ;
    unsigned i ;
    VhdlConfigurationItem *item ;
    FOREACH_ARRAY_ITEM(config_array, i, item) {
        VhdlConfigurationItem *new_item = item ? item->CopyConfigurationItem(*this) : 0 ;
        new_config_array->InsertLast(new_item) ;
    }
    return new_config_array ;
}

void
VhdlMapForCopy::CopyPredefinedOperators(const VhdlScope *scope)
{
    if (!scope) return ;
    Array *ops = scope->GetPredefinedOperators() ;
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(ops, i, id) {
        VhdlIdDef *new_id = id ? id->CopyIdDef(*this) : 0 ;
        SetCopiedId(id, new_id) ;
    }
}

Array *
VhdlMapForCopy::CopyDiscreteRangeArray(const Array *range_array)
{
    if (!range_array) return 0 ;
    Array *new_range_list = new Array(range_array->Size()) ;
    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(range_array, i, range) {
        VhdlDiscreteRange *new_range = range ? range->CopyDiscreteRange(*this) : 0 ;
        new_range_list->InsertLast(new_range) ;
    }
    return new_range_list ;
}

Array *
VhdlMapForCopy::CopyIdDefArray(const Array *id_def_array)
{
    if (!id_def_array) return 0 ;
    Array *new_iddef_list = new Array(id_def_array->Size()) ;
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(id_def_array, i, id) {
        if (!id) continue ;
        VhdlIdDef *new_id = id->CopyIdDef(*this) ;
        SetCopiedId(id, new_id) ;
        new_iddef_list->InsertLast(new_id) ;
    }
    return new_iddef_list ;
}

Array *
VhdlMapForCopy::CopyNameArray(const Array *name_array)
{
    if (!name_array) return 0 ;
    Array *new_name_array = new Array(name_array->Size()) ;
    unsigned i ;
    VhdlName *name ;
    FOREACH_ARRAY_ITEM(name_array, i, name) {
        VhdlName *new_name = name ? name->CopyName(*this) : 0 ;
        new_name_array->InsertLast(new_name) ;
    }
    return new_name_array ;
}

Array *
VhdlMapForCopy::CopyStatementArray(const Array *stmt_array)
{
    if (!stmt_array) return 0 ;
    Array *new_stmt_array = new Array(stmt_array->Size()) ;
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(stmt_array, i, stmt) {
        VhdlStatement *new_stmt = stmt ? stmt->CopyStatement(*this) : 0 ;
        new_stmt_array->InsertLast(new_stmt) ;
    }
    return new_stmt_array ;
}

Array *
VhdlMapForCopy::PopulateIdDefArray(const Array *iddef_list) const
{
    if (!iddef_list) return 0 ;
    Array *new_iddef_list = new Array(iddef_list->Size()) ;
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(iddef_list, i, id) {
        VhdlIdDef *new_id = GetCopiedId(id) ;
        new_iddef_list->InsertLast(new_id) ;
    }
    return new_iddef_list ;
}

Array *
VhdlMapForCopy::CopyExpressionArray(const Array *expr_list)
{
    if (!expr_list) return 0 ;
    Array *new_expr_list = new Array(expr_list->Size()) ;
    unsigned i ;
    VhdlExpression *expr ;
    FOREACH_ARRAY_ITEM(expr_list, i, expr) {
         VhdlExpression *new_expr = expr ? expr->CopyExpression(*this) : 0 ;
         new_expr_list->InsertLast(new_expr) ;
    }
    return new_expr_list ;
}

void
VhdlMapForCopy::MoveAssociations(VhdlMapForCopy &to_map) const
{
    MapIter mi ;
    VhdlIdDef *id ;
    void *val ;
    FOREACH_MAP_ITEM(this, mi, &id, &val) {
        if (!id || !val) continue ;
        (void) to_map.Insert(id, val) ;
    }
    if (_old_entity_id) to_map.SetOldEntityId(_old_entity_id) ;
    if (_new_entity_id) to_map.SetNewEntityId(_new_entity_id) ;
}
// This routine is required when instantiated entity has multiple architecture and
// different instantiation use different architecture, but same generic settings.
// In that case we will create one unique entity and two architectures. In copied
// second architecture we have to use iddefs unique entity not the original one.
// For this an association of original iddefs->copied iddefs is created, so that
// we can use this map to copy architecture.
void
VhdlMapForCopy::MakeAssociations(const VhdlPrimaryUnit *old_entity, const VhdlPrimaryUnit *new_entity, unsigned recurse)
{
    if (!old_entity || !new_entity || (old_entity == new_entity)) return ; // No need to create association

    VhdlScope *old_scope = old_entity->LocalScope() ; // Get local scope of old entity
    VhdlScope *new_scope = new_entity->LocalScope() ; // Get local scope of new entity
    MakeAssociationsInternal(old_scope, new_scope, recurse) ;
}
void VhdlMapForCopy::MakeAssociationsInternal(const VhdlScope *old_scope, const VhdlScope *new_scope, unsigned recurse)
{
    if (!old_scope || !new_scope) return ;

    SetCopiedScope(old_scope, new_scope) ; // Make scope association

    // Here we are concerned only with iddefs declared by entity, as in Copy we only
    // copy those iddefs. For this we will create association list for iddefs stored in
    // this_scope and predefined operators

    // First get entity declared iddefs
    Map *old_this_scope = old_scope->GetThisScope() ;
    Map *new_this_scope = new_scope->GetThisScope() ;

    if (!old_this_scope || !new_this_scope) return ;

    MapIter mi ;
    char *id_name ;
    VhdlIdDef *old_id, *new_id ;
    FOREACH_MAP_ITEM(old_this_scope, mi, &id_name, &old_id) {
        new_id = (VhdlIdDef*)new_this_scope->GetValue(id_name) ;
        SetCopiedId(old_id, new_id) ; // Make association
        if (recurse) {
            VhdlScope *old_child = old_id ? old_id->LocalScope(): 0 ;
            VhdlScope *new_child = new_id ? new_id->LocalScope(): 0 ;
            if (old_child && new_child) {
                MakeAssociationsInternal(old_child, new_child, recurse) ;
            }
        }
    }
}
/////////////   Copy specific constructors and routines    //////////

// Scope itself does not own the iddefs, so in scope copy we only
// populate the iddefs and scopes from 'old2new' map. This leads
// to the conclusion that we should copy the scope after copying
// all members of a scoped object. But in that case the parent of
// any inner scope will not be set and if any parse tree node only
// holds the present scope as a back pointer, that back pointer
// field will not be updated. To handle these issues, when we copy
// a scoped object, we first create a new scope without populating
// its fields and when all other members of the scoped object
// are copied, we only update the fields of the scope. The following
// routine is used to update the copied scope.
void
VhdlScope::Update(const VhdlScope *scope, VhdlMapForCopy &old2new, unsigned for_pkg_inst)
{
    if (!scope) return ;

    // If parent is already copied, use the copied one, otherwise use the original parent
    if (!for_pkg_inst) _upper = old2new.GetCopiedScope(scope->_upper) ;
    if (scope->_this_scope) {
        // members are copied during declaration copy.
        // use the copied iddefs
        _this_scope = new Map(STRING_HASH, scope->_this_scope->Size()) ;
        MapIter iter ;
        const char *name ;
        VhdlIdDef *id ;
        FOREACH_MAP_ITEM(scope->_this_scope, iter, &name, &id) {
            VhdlIdDef *new_id = old2new.GetCopiedId(id) ;
            if ((new_id == 0) || (new_id == id)) continue ;
            (void) _this_scope->Insert(new_id->Name(), new_id, 0, 1/* force insert*/) ;
#ifdef VHDL_ID_SCOPE_BACKPOINTER
            // Set the scope backpointer of the identifier to this :
            new_id->SetOwningScope(this) ;
#endif
        }
    } else {
        _this_scope = 0 ;
    }

    // use_clauses : char*->VhdlIdDef table
    if (scope->_use_clauses) {
        _use_clauses = new Map(STRING_HASH, scope->_use_clauses->Size()) ;
        MapIter iter ;
        const char *name ;
        VhdlIdDef *id ;
        FOREACH_MAP_ITEM(scope->_use_clauses, iter, &name, &id) {
            // populate field with copied iddef (if any), otherwise use the original one
            VhdlIdDef *new_id = old2new.GetCopiedId(id) ;
            if (new_id) (void) _use_clauses->Insert(new_id->Name(), new_id, 0, 1/* force insert*/) ;
        }
    } else {
        _use_clauses = 0 ;
    }

    // use_clause_units : VhdlIdDef*->char* table. IdDefs will be mostly external primary unit ids. Some local library ids also possible (for a "use libname.all" clause)
    if (scope->_use_clause_units) {
        _use_clause_units = new Map(POINTER_HASH, scope->_use_clause_units->Size()) ;
        MapIter iter ;
        const char *name ;
        VhdlIdDef *id ;
        FOREACH_MAP_ITEM(scope->_use_clause_units, iter, &id, &name) {
            // populate field with copied iddef (if any), otherwise use the original one
            VhdlIdDef *new_id = old2new.GetCopiedId(id) ;
            // insert (new)id->(new)name :
            if (new_id) (void) _use_clause_units->Insert(new_id, Strings::save(name), 0, 1/*force insert*/) ;
        }
    } else {
        _use_clause_units = 0 ;
    }

    // If '_extended_decl_area' specific scope is copied use that one, otherwise use old one
    _extended_decl_area = old2new.GetCopiedScope(scope->_extended_decl_area) ;
    if (scope->_predefined) {
        // These iddefs are owned by this scope.
        // The operators are copied first during scoped object copy.
        // Here only the copied iddefs are used
        _predefined = new Array(scope->_predefined->Size()) ;
        unsigned i ;
        VhdlIdDef *op_id ;
        FOREACH_ARRAY_ITEM(scope->_predefined, i, op_id) {
            VhdlIdDef *new_id = old2new.GetCopiedId(op_id) ;
            if (new_id) {
                new_id->UpdateArgsAndType(old2new) ;
                new_id->UpdateAnonymousType(op_id, old2new) ; // Viper #6146
            }
            _predefined->InsertLast(new_id) ;
            // All predefined pointers are already stored in _this_scope, so have their back-pointers set.
        }
    } else {
        _predefined = 0 ;
    }

    // _using : VhdlScope* Set.
    if (scope->_using) {
        // If '_using' specific scope is copied, use the copied once and
        // populate the '_used_by' field of the copied scope
        _using = new Set(POINTER_HASH, scope->_using->Size()) ;
        SetIter si ;
        VhdlScope *used_scope ;
        FOREACH_SET_ITEM(scope->_using, si, &used_scope) {
            VhdlScope *new_used_scope = old2new.GetCopiedScope(used_scope) ;
            if (new_used_scope) {
                (void) _using->Insert(new_used_scope) ;
                if (!new_used_scope->_used_by) new_used_scope->_used_by = new Set(POINTER_HASH) ;
                (void) new_used_scope->_used_by->Insert(this) ;
            }
        }
    } else {
        _using = 0 ;
    }
    // In scope copy '_used_by' field is not populated. This field is
    // populated when the '_using' field is copied.
    _used_by = 0 ;

    // Set owner id and set the localscope field of owner id
    _owner_id = old2new.GetCopiedId(scope->_owner_id) ;
    // VIPER 2158 : Set local scope 'this' to _owner_id, if we get new _owner_id
    if(_owner_id && (_owner_id != scope->_owner_id)) _owner_id->SetLocalScope(this) ;
    if (scope->_hidden_ids) {
        _hidden_ids = new Set(STRING_HASH, scope->_hidden_ids->Size()) ;
        SetIter si ;
        char *name ;
        FOREACH_SET_ITEM(scope->_hidden_ids, si, &name) {
           // The original names are used.
           // Is it correct ?
            (void) _hidden_ids->Insert(name) ;
        }
    } else {
        _hidden_ids = 0 ;
    }
}
// Copy like constructor to copy scope
VhdlScope::VhdlScope(const VhdlScope &scope, VhdlMapForCopy &old2new)
  : _upper(0),
    _this_scope(0),
    _use_clauses(0),
    _use_clause_units(0),
    _extended_decl_area(0),
    _predefined(0),
    _using(0),
    _used_by(0),
    _owner_id(0),
    _hidden_ids(0),
    _transparent(scope._transparent),
    _is_process_scope(scope._is_process_scope),
    _is_subprogram_scope(scope._is_subprogram_scope),
    _wait_not_allowed(scope._wait_not_allowed),
    _has_wait_statement(scope._has_wait_statement),
    _has_impure_access(scope._has_impure_access),
    _has_local_file_access(scope._has_local_file_access)
{
    // Store the old one vs new one in map
    old2new.SetCopiedScope(&scope, this) ;
}

// The following constructor is called whenever any node is copied.
// This is used to copy the line file information from the old to new node.
VhdlTreeNode::VhdlTreeNode(const VhdlTreeNode &node, VhdlMapForCopy &old2new)
  : VhdlNode(),
    _linefile(node.Linefile())
{
    // Copy the comment array on this node
    Array *node_comments = node.GetComments() ;
    Array *comment_arr = (node_comments) ? new Array(node_comments->Size()) : 0 ;
    unsigned i ;
    VhdlCommentNode *comment ;
    FOREACH_ARRAY_ITEM(node_comments, i, comment) {
        // Use the copy constructor to save an unnecessary object creation of VhdlMapForCopy:
        VhdlCommentNode *cmnt = (comment) ? (new VhdlCommentNode(*comment, old2new)) : 0 ;
        comment_arr->Insert(cmnt) ;
    }
    AddComments(comment_arr) ;
}

// Base constructor to copy any design unit.
VhdlDesignUnit::VhdlDesignUnit(const VhdlDesignUnit &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0),
    _local_scope(0),
    _context_clause(0),
    _timestamp(node._timestamp),
    _is_elaborated(0),
    _is_static_elaborated(0),
// Set by the conversion routine to indicate that this unit is for internal use. Set to '0' by constructor.
    _is_verilog_module(0),
    _affected_by_path_name(node._affected_by_path_name),
    _analysis_mode(node._analysis_mode),
    _orig_design_unit(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    , _is_already_processed(0) // VIPER #6896: flag on package that is already elaborated
#endif
{
    // _id field is copied and _local_scope field is updated in specific
    // design unit constructor (VhdlEntityDecl etc.)
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _context_clause = old2new.CopyDeclarationArray(node._context_clause) ;

    // Get the design unit from which this is copied. If the unit from which it
    // is copying is itself copied unit, set the original unit of that copied unit
    // VIPER #6320 : Store 'OrigName()' instead of 'Name()' as original unit name
    VhdlIdDef *orig_id = node.Id() ;
    _orig_design_unit = node._orig_design_unit ? Strings::save(node._orig_design_unit) : (orig_id ? Strings::save(orig_id->OrigName()): 0) ;
// Set by the conversion routine to indicate that this unit is for internal use. Set to '0' by constructor.
    _is_verilog_module = node._is_verilog_module ;
}

VhdlPrimaryUnit::VhdlPrimaryUnit(const VhdlPrimaryUnit &node, VhdlMapForCopy &old2new) :
     VhdlDesignUnit(node, old2new),
     _compile_as_blackbox(node._compile_as_blackbox),
     _secondary_units(STRING_HASH), _owner(0)
    , _process_stmt_replaced(0)
{
    _owner = 0 ;
}

VhdlSecondaryUnit::VhdlSecondaryUnit(const VhdlSecondaryUnit &node, VhdlMapForCopy &old2new)
  : VhdlDesignUnit(node, old2new),
    _owner(0)
{
}

// Construct entity and its architectures.
// The first argument specifies the name of the copied entity.
// If the value of the third argument is '1', the secondary units
// of the entity will not be constructed. The default value of this
// field is '0'.
VhdlEntityDecl::VhdlEntityDecl(const char* new_prim_name, const VhdlEntityDecl &node, VhdlMapForCopy &old2new, unsigned exclude_archs)
  : VhdlPrimaryUnit(node, old2new),
    _generic_clause(0),
    _port_clause(0),
    _decl_part(0),
    _statement_part(0)
{
    _id = node._id ? new VhdlEntityId(Strings::save(new_prim_name)) : 0 ;
    old2new.SetOldEntityId(node._id) ;
    old2new.SetNewEntityId(_id) ;
    if (_id) _id->SetLinefile(node._id->Linefile()) ;
    //if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    // Copy generics
    _generic_clause = old2new.CopyDeclarationArray(node._generic_clause) ;
    // Copy ports
    _port_clause = old2new.CopyDeclarationArray(node._port_clause) ;
    if (_id) {
        _id->SetPrimaryUnit(this) ;
        Array *gen_list = old2new.PopulateIdDefArray(node._id->GetGenerics()) ;
        Array *port_list = old2new.PopulateIdDefArray(node._id->GetPorts()) ;
    // Set the generic ids and port ids in entity id.
        _id->DeclareEntityId(gen_list, port_list) ;
    }
    // Copy declarations
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;
    // Copy concurrent statements
    _statement_part = old2new.CopyStatementArray(node._statement_part) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    // Update the local scope
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
    // VIPER #2831 : Remove the entity identifier from 'old2new' so that
    // reference to entity name within architecture cannot point to copied entity
    old2new.RemoveId(node._id) ;

    if (!exclude_archs) {
        // VIPER #2998 : Create a VhdlMapForCopy containing old entity id vs new
        // entity id and use that to set entity reference in attribute specification
        // to copied entity identifier.
        // RD: NOTE: This should not be needed if 2831 would be fixed differently.
        VhdlMapForCopy tmpold2new ;
        tmpold2new.SetCopiedId(node._id, _id) ;

        // now copy the secondary units
        MapIter mi ;
        VhdlSecondaryUnit *sec_unit ;
        char *name ;
        FOREACH_MAP_ITEM(&node._secondary_units, mi, &name, &sec_unit) {
            if (!sec_unit) continue ;
            VhdlSecondaryUnit *new_sec_unit = sec_unit->CopySecondaryUnit(0, old2new)  ;
            new_sec_unit->ChangeEntityReference(_id) ;

            // VIPER #2998 : Attribute specification of architecture can have entity
            // references. We have copied architecture after removing entity id
            // from 'old2new', so any reference to entity is pointing to original
            // entity not the copied one. But in attribute specification entity
            // reference should point to copied entity. Do that now :
            // RD: NOTE: Second call to UpdateDecl(). First one was inside the Architecture constructor.
            // RD: NOTE: This should not be needed if 2831 would be fixed differently.
            Array *decls = new_sec_unit->GetDeclPart() ;
            FOREACH_ARRAY_ITEM(decls, i, decl) { // Iterate all declarations
                if (decl) decl->UpdateDecl(tmpold2new) ; // Set entity references
            }

            (void) _secondary_units.Insert(new_sec_unit->Name(), new_sec_unit) ;
            new_sec_unit->SetOwningUnit(this) ;
            new_sec_unit->Id()->DeclareArchitectureId(_id) ;
        }
    }
}

// Construct configuration
VhdlConfigurationDecl::VhdlConfigurationDecl(const char* new_prim_name, const VhdlConfigurationDecl &node, VhdlMapForCopy &old2new)
  : VhdlPrimaryUnit(node, old2new),
    _entity_name(0),
    _decl_part(0),
    _block_configuration(0)
{
    // Copy configuration id
    _id = node._id ? new VhdlConfigurationId(Strings::save(new_prim_name)) : 0 ;
    if (_id) _id->SetLinefile(node._id->Linefile()) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    if (_id) _id->SetPrimaryUnit(this) ;

    // Copy entity name reference
    _entity_name = node._entity_name ? node._entity_name->Copy(old2new) : 0 ;
    // Copy declarations
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;
    // Copy block configuation
    // Here copy like constructor is used directly. The copy
    // routine of block configuartion returns its base class. So
    // we cannot use that one.
    _block_configuration = node._block_configuration ? new VhdlBlockConfiguration(*(node._block_configuration), old2new) : 0 ;
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
    if (_id) {
        VhdlIdDef *new_entity = old2new.GetCopiedId(node._id->GetEntity()) ;
        _id->DeclareConfigurationId(new_entity) ;
    }

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
    // now copy the secondary units
    MapIter mi ;
    VhdlSecondaryUnit *sec_unit ;
    char *name ;
    FOREACH_MAP_ITEM(&node._secondary_units, mi, &name, &sec_unit) {
        if (!sec_unit) continue ;
        const char *tmp = sec_unit->Id() ? sec_unit->Id()->OrigName() : 0 ;
        VhdlSecondaryUnit *new_sec_unit = sec_unit->CopySecondaryUnit(tmp, old2new) ;
        (void) _secondary_units.Insert(new_sec_unit->Name(), new_sec_unit) ;
        new_sec_unit->SetOwningUnit(this) ;
    }
}
// Construct package declaration and its body
VhdlPackageDecl::VhdlPackageDecl(const char* new_prim_name, const VhdlPackageDecl &node, VhdlMapForCopy &old2new, unsigned exclude_body)
  : VhdlPrimaryUnit(node, old2new),
    _generic_clause(0),
    _generic_map_aspect(0),
    _decl_part(0),
    _container(0)
{
    // Copy package id
    _id = node._id ? new VhdlPackageId(Strings::save(new_prim_name)) : 0 ;
    if (_id) _id->SetLinefile(node._id->Linefile()) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;

    // Copy declarations
    _generic_clause = old2new.CopyDeclarationArray(node._generic_clause) ;
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;
    // Update local scope
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;

    if (_id) {
        _id->SetPrimaryUnit(this) ;
        if (_generic_clause) {
            Array *gen_list = old2new.PopulateIdDefArray(node._id->GetGenerics()) ;
            _id->DeclarePackageId(gen_list) ;
        }
    }

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
    if (!exclude_body) {
        // now copy the secondary units (package body)
        MapIter mi ;
        VhdlSecondaryUnit *sec_unit ;
        char *name ;
        FOREACH_MAP_ITEM(&node._secondary_units, mi, &name, &sec_unit) {
            if (!sec_unit) continue ;
            const char *tmp = sec_unit->Id() ? sec_unit->Id()->OrigName() : 0 ;
            // VIPER #7513: If package is to be copied with new name, use that name
            // to copy package body too
            VhdlSecondaryUnit *new_sec_unit = sec_unit->CopySecondaryUnit(new_prim_name ? new_prim_name: tmp, old2new) ;
            (void) _secondary_units.Insert(new_sec_unit->Name(), new_sec_unit) ;
            new_sec_unit->SetOwningUnit(this) ;
        }
    }
}

// Construct context declaration and its body
VhdlContextDecl::VhdlContextDecl(const char* new_prim_name, const VhdlContextDecl &node, VhdlMapForCopy &old2new)
  : VhdlPrimaryUnit(node, old2new)
{
    // Copy context id
    _id = node._id ? new VhdlContextId(Strings::save(new_prim_name)) : 0 ;
    if (_id) _id->SetLinefile(node._id->Linefile()) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    if (_id) _id->SetPrimaryUnit(this) ;

    // Update local scope
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
}
VhdlPackageInstantiationDecl::VhdlPackageInstantiationDecl(const char* new_prim_name, const VhdlPackageInstantiationDecl &node, VhdlMapForCopy &old2new)
  : VhdlPrimaryUnit(node, old2new),
    _inst_scope(0),
    _uninst_package_name(0),
    _generic_map_aspect(0),
    _generic_types(0),
    _initial_pkg(0)
    ,_elab_pkg(0)
{
    _inst_scope = node._inst_scope ? node._inst_scope->Copy(old2new) : 0 ;
    if (_inst_scope) _inst_scope->Update(node._inst_scope, old2new) ;

    if (new_prim_name) {
        _id = node._id ? new VhdlPackageId(Strings::save(new_prim_name)) : 0 ;
    } else {
        _id = (node._id) ? node._id->CopyIdDef(old2new): 0 ;
    }
    if (_id) _id->SetLinefile(node._id->Linefile()) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    if (_local_scope && _id) {
        _local_scope->SetOwner(_id) ;
        _id->SetLocalScope(_local_scope) ;
    }
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    _uninst_package_name = (node._uninst_package_name) ? node._uninst_package_name->CopyName(old2new) : 0 ;
    _initial_pkg = (node._initial_pkg) ? node._initial_pkg->CopyPrimaryUnit(0, old2new, 0): 0 ;
    if (_local_scope) _local_scope->SetUpper(old2new.GetCopiedScope(node._local_scope ? node._local_scope->Upper(): 0)) ;
    // Here identifiers of node._local_scope are owned by the scope, so need to
    // copy them
    Map *old_map = node._local_scope ? node._local_scope->DeclArea(): 0 ;
    MapIter mi ;
    char *name ;
    VhdlIdDef *old_id, *new_id ;
    Array *generics = 0 ;
    FOREACH_MAP_ITEM(old_map, mi, &name, &old_id) {
        if (old_id && (old_id == node._local_scope->GetOwner())) continue ;
        new_id = old_id ? old_id->CopyIdDef(old2new): 0 ;
        old2new.SetCopiedId(old_id, new_id) ;
        if (old_id && old_id->IsGeneric()) {
            if (!generics) generics = new Array(1) ;
            generics->InsertLast(new_id) ;
        }
        if (_local_scope) _local_scope->ForceDeclare(new_id) ;
#ifdef VHDL_ID_SCOPE_BACKPOINTER
        // VIPER #7258 : Set back pointer
        // Set the owning scope of the identifier :
        if (new_id) new_id->SetOwningScope(_local_scope) ;
#endif
    }
    if (_id && generics) _id->DeclarePackageId(generics) ;

    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    _generic_types = node._generic_types ? new Map(POINTER_HASH, node._generic_types->Size()): 0 ;
    if (_generic_types && node._generic_types) {
        VhdlIdDef *generic_id, *new_generic_id ;
        VhdlIdDef *actual_type, *new_actual_type ;
        FOREACH_MAP_ITEM(node._generic_types, mi, &generic_id, &actual_type) {
            new_generic_id = old2new.GetCopiedId(generic_id) ;
            if (!new_generic_id) new_generic_id = generic_id ;
            new_actual_type = old2new.GetCopiedId(actual_type) ;
            if (!new_actual_type) new_actual_type = actual_type ;
            (void) _generic_types->Insert(new_generic_id, new_actual_type) ;
        }
    }
}
// Construct architecture body. First argument is the new name of the architecture.
VhdlArchitectureBody::VhdlArchitectureBody(const char* new_prim_name, const VhdlArchitectureBody &node, VhdlMapForCopy &old2new)
  : VhdlSecondaryUnit(node, old2new),
    _entity_name(0),
    _decl_part(0),
    _statement_part(0)
   ,_bind_instances(0)
   ,_bind_verilog_instances(0)
    ,_access_allocated_values(0)
{
    // Copy architecture id
    _id = node._id ? new VhdlArchitectureId(Strings::save(new_prim_name)) : 0 ;
    if (_id) _id->SetLinefile(node._id->Linefile()) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    _entity_name = node._entity_name ? node._entity_name->Copy(old2new) : 0 ;

    // Copy declarations
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;

    // Copy Statements
    _statement_part = old2new.CopyStatementArray(node._statement_part) ;

    // Update local scope
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;

    // Set back pointer in architecture id
    if (_id) _id->SetSecondaryUnit(this) ;

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}

// Construct package body with first argument specific name
VhdlPackageBody::VhdlPackageBody(const char* new_prim_name, const VhdlPackageBody &node, VhdlMapForCopy &old2new)
  : VhdlSecondaryUnit(node, old2new),
    _decl_part(0)
{
    // Copy the package body id
    _id = node._id ? new VhdlPackageBodyId(Strings::save(new_prim_name)) : 0 ;
    if (_id) _id->SetLinefile(node._id->Linefile()) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;

    // Copy the declarations
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;

    // Update local scope
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;

    // Set back pointer in id
    if (_id) _id->SetSecondaryUnit(this) ;

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}

// Defined only to call base VhdlTreeNode constructor to set linefile
VhdlConfigurationItem::VhdlConfigurationItem(const VhdlConfigurationItem &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new)
{
}
// Construct block configuration
VhdlBlockConfiguration::VhdlBlockConfiguration(const VhdlBlockConfiguration &node, VhdlMapForCopy &old2new)
  : VhdlConfigurationItem(node, old2new),
    _block_spec(0),
    _use_clause_list(0),
    _configuration_item_list(0),
    _local_scope(0),
    _block_label(0)
    , _block_range(0)
{
    // Copy predefined operators
    old2new.CopyPredefinedOperators(node._local_scope) ;
    // Create new scope
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _block_spec = node._block_spec ? node._block_spec->CopyName(old2new) : 0 ;
    _use_clause_list = old2new.CopyDeclarationArray(node._use_clause_list) ;
    // VIPER #4552 : If we are copying entity having recursive instance via block configuration, do
    // not change the reference of formal identifiers in formal part of association. If we do this,
    // formal part will point to copied entity. If formal part in association is changed for recursive
    // instance, we cannot handle recursive instance properly (it may have different generic value/architecture setting).
    old2new.DonotSwitchIdInFormal() ;
    _configuration_item_list = old2new.CopyConfigurationItemArray(node._configuration_item_list) ;
    old2new.ResetDonotSwitchIdInFormal() ;
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
    _block_label = old2new.GetCopiedId(node._block_label) ;
    _block_range = node._block_range ? node._block_range->Copy() : 0 ;
}

// Construct component configuration
VhdlComponentConfiguration::VhdlComponentConfiguration(const VhdlComponentConfiguration &node, VhdlMapForCopy &old2new)
  : VhdlConfigurationItem(node, old2new),
    _component_spec(0),
    _binding(0),
    _block_configuration(0),
    _local_scope(0)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _component_spec = node._component_spec ? node._component_spec->CopySpecification(old2new,0) : 0 ;
    _binding = node._binding ?  node._binding->Copy(old2new) : 0 ;
    _block_configuration = node._block_configuration ? new VhdlBlockConfiguration((*node._block_configuration), old2new) : 0 ;
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
}
VhdlDeclaration::VhdlDeclaration(const VhdlDeclaration &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    if (node._closing_label) _closing_label = node._closing_label->CopyDesignator(old2new) ;
#endif
}
VhdlTypeDef::VhdlTypeDef(const VhdlTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    if (node._closing_label) _closing_label = node._closing_label->CopyDesignator(old2new) ;
#endif
}
VhdlScalarTypeDef::VhdlScalarTypeDef(const VhdlScalarTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _scalar_range(0)
{
    _scalar_range = node._scalar_range ? node._scalar_range->CopyDiscreteRange(old2new) : 0 ;
}

VhdlArrayTypeDef::VhdlArrayTypeDef(const VhdlArrayTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _index_constraint(0),
    _subtype_indication(0)
{
    _index_constraint = old2new.CopyDiscreteRangeArray(node._index_constraint) ;
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
}

VhdlRecordTypeDef::VhdlRecordTypeDef(const VhdlRecordTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _element_decl_list(0),
    _local_scope(0)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    if (node._element_decl_list) {
        _element_decl_list = new Array(node._element_decl_list->Size()) ;
        unsigned i ;
        VhdlElementDecl *decl ;
        FOREACH_ARRAY_ITEM(node._element_decl_list, i, decl) {
            VhdlElementDecl *new_decl = decl ? decl->Copy(old2new) : 0 ;
            _element_decl_list->InsertLast(new_decl) ;
        }
    } else {
        _element_decl_list = 0 ;
    }
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
}

VhdlProtectedTypeDef::VhdlProtectedTypeDef(const VhdlProtectedTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _decl_list(0),
    _local_scope(0)
{
    // Copy declarations
    _decl_list = old2new.CopyDeclarationArray(node._decl_list) ;
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    // Update local scope
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_list, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}

VhdlProtectedTypeDefBody::VhdlProtectedTypeDefBody(const VhdlProtectedTypeDefBody &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _decl_list(0),
    _local_scope(0)
{
    // Copy declarations
    _decl_list = old2new.CopyDeclarationArray(node._decl_list) ;
    // VIPER #7455 : Copy predefined operators
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    // Update local scope
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_list, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}
VhdlAccessTypeDef::VhdlAccessTypeDef(const VhdlAccessTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _subtype_indication(0)
{
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
}
VhdlFileTypeDef::VhdlFileTypeDef(const VhdlFileTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _file_type_mark(0)
{
    _file_type_mark = node._file_type_mark ?  node._file_type_mark->CopyName(old2new) : 0 ;
}

VhdlEnumerationTypeDef::VhdlEnumerationTypeDef(const VhdlEnumerationTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _enumeration_literal_list(0)
{
    _enumeration_literal_list = old2new.CopyIdDefArray(node._enumeration_literal_list) ;
}

VhdlPhysicalTypeDef::VhdlPhysicalTypeDef(const VhdlPhysicalTypeDef &node, VhdlMapForCopy &old2new)
  : VhdlTypeDef(node, old2new),
    _range_constraint(0),
    _physical_unit_decl_list(0)
{
    _range_constraint = node._range_constraint ? node._range_constraint->CopyDiscreteRange(old2new) : 0 ;
    if (node._physical_unit_decl_list) {
        _physical_unit_decl_list = new Array(node._physical_unit_decl_list->Size()) ;
        unsigned i ;
        VhdlPhysicalUnitDecl *decl ;
        FOREACH_ARRAY_ITEM(node._physical_unit_decl_list, i, decl) {
            VhdlPhysicalUnitDecl *new_decl = decl ? decl->Copy(old2new) : 0 ;
            _physical_unit_decl_list->InsertLast(new_decl) ;
        }
    } else {
        _physical_unit_decl_list = 0 ;
    }
}
// Copy declarations. Declaration owns iddefs, so here declared iddefs are copied.
VhdlElementDecl::VhdlElementDecl(const VhdlElementDecl &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _id_list(0),
    _subtype_indication(0)
{
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    // New id list contains copied iddefs
    _id_list = old2new.CopyIdDefArray(node._id_list) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

VhdlPhysicalUnitDecl::VhdlPhysicalUnitDecl(const VhdlPhysicalUnitDecl &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _id(0),
    _physical_literal(0)
{
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if(_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    _physical_literal = node._physical_literal ? node._physical_literal->CopyExpression(old2new) : 0 ;
}

VhdlUseClause::VhdlUseClause(const VhdlUseClause &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _selected_name_list(0),
    _is_implicit(node.IsImplicit())
{
    _selected_name_list = old2new.CopyNameArray(node._selected_name_list) ;
}

VhdlContextReference::VhdlContextReference(const VhdlContextReference &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _selected_name_list(0),
    _lib_ids(0)
{
    _selected_name_list = old2new.CopyNameArray(node._selected_name_list) ;
    _lib_ids = (node._lib_ids) ? new Set(POINTER_HASH, node._lib_ids->Size()): 0 ;
    SetIter si ;
    VhdlIdDef *old_id, *copied_id ;
    FOREACH_SET_ITEM(node._lib_ids, si, &old_id) {
        copied_id = old_id ? old_id->CopyIdDef(old2new): 0 ;
        if (_lib_ids) (void) _lib_ids->Insert(copied_id) ;
    }
}

VhdlLibraryClause::VhdlLibraryClause(const VhdlLibraryClause &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id_list(0),
    _is_implicit(node.IsImplicit())
{
    _id_list = old2new.CopyIdDefArray(node._id_list) ;
}

VhdlInterfaceDecl::VhdlInterfaceDecl(const VhdlInterfaceDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _interface_kind(0),
    _id_list(0),
    _mode(0),
    _subtype_indication(0),
    _signal_kind(0),
    _init_assign(0)
{
    _interface_kind = node._interface_kind ;
    _mode = node._mode ;
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    _signal_kind = node._signal_kind ;
    _id_list = old2new.CopyIdDefArray(node._id_list) ;
    _init_assign = node._init_assign ? node._init_assign->CopyExpression(old2new) : 0 ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

VhdlSubprogramDecl::VhdlSubprogramDecl(const VhdlSubprogramDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _subprogram_spec(0)
{
    _subprogram_spec = node._subprogram_spec ? node._subprogram_spec->CopySpecification(old2new,0) : 0 ;
}

VhdlSubprogramBody::VhdlSubprogramBody(const VhdlSubprogramBody &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _subprogram_spec(0),
    _decl_part(0),
    _statement_part(0)
{
    // Copy subprogram specification. But local scope is not updated within
    // specification copy. Scope will be updated here
    _subprogram_spec = node._subprogram_spec ? node._subprogram_spec->CopySpecification(old2new, 1) : 0 ;
    // Copy declarations
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;
    // Copy statements
    _statement_part = old2new.CopyStatementArray(node._statement_part) ;

    // Handle back pointers
    if (_subprogram_spec) {
        VhdlIdDef *bodyspec = _subprogram_spec->GetId() ;
        if (bodyspec) {
            bodyspec->SetBody(this) ;
            if (bodyspec->Spec()) {
                VhdlIdDef *specspec = bodyspec->Spec()->GetId() ;
                if (specspec) specspec->SetBody(this) ;
            }
        }
    }
    VhdlScope *scope = _subprogram_spec ? _subprogram_spec->LocalScope() : 0 ;
    if (scope) {
        scope->Update(node._subprogram_spec->LocalScope(), old2new) ;
        _subprogram_spec->SetLocalScope(scope) ;
    }

    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}
VhdlSubtypeDecl::VhdlSubtypeDecl(const VhdlSubtypeDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0),
    _subtype_indication(0)
{
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    if (_id) _id->SetSubtypeIndication(_subtype_indication) ;
#endif
}

VhdlFullTypeDecl::VhdlFullTypeDecl(const VhdlFullTypeDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0),
    _type_def(0)
{
    _type_def = node._type_def ? node._type_def->CopyTypeDef(old2new) : 0 ;
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    // Set back pointers in type id, enumeration id etc.
    if (_type_def) _type_def->DeclareForCopy(node._id, old2new) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // VIPER #7343 : Set declaration back pointer to type id
    if (_id) _id->SetDeclaration(this) ;
#endif
}

VhdlIncompleteTypeDecl::VhdlIncompleteTypeDecl(const VhdlIncompleteTypeDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0)
{
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
}
VhdlConstantDecl::VhdlConstantDecl(const VhdlConstantDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id_list(0),
    _subtype_indication(0),
    _init_assign(0),
    _type(0)
{
    // Type is copied before ids, so that while copying iddefs _type field
    // can be populated from map.
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    _init_assign = node._init_assign ? node._init_assign->CopyExpression(old2new) : 0 ;
    _id_list = old2new.CopyIdDefArray(node._id_list) ;
    unsigned i ;
    VhdlIdDef *id ;
    VhdlIdDef *new_id ;
    FOREACH_ARRAY_ITEM(node._id_list, i, id) {
        VhdlIdDef *decl = id ? id->Decl() : 0 ;
        VhdlIdDef *new_decl = old2new.GetCopiedId(decl) ;
        new_id = old2new.GetCopiedId(id) ;
        // Set itself/other constant id as declaration
        if (new_id) {
            new_id->SetDecl(new_decl) ;
            if (new_decl && (new_decl != decl)) new_decl->SetDecl(new_id) ;
        }
    }
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
//    unsigned i ;
//    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
    _type = old2new.GetCopiedId(node._type) ;
}

VhdlSignalDecl::VhdlSignalDecl(const VhdlSignalDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id_list(0),
    _subtype_indication(0),
    _signal_kind(0),
    _init_assign(0)
{
    // Type is copied before ids, so that while copying iddefs _type field
    // can be populated from map.
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    _signal_kind = node._signal_kind ;
    _init_assign = node._init_assign ? node._init_assign->CopyExpression(old2new) : 0 ;
    _id_list = old2new.CopyIdDefArray(node._id_list) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

VhdlVariableDecl::VhdlVariableDecl(const VhdlVariableDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _shared(0),
    _id_list(0),
    _subtype_indication(0),
    _init_assign(0)
{
    // Type is copied before ids, so that while copying iddefs _type field
    // can be populated from map.
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    _init_assign = node._init_assign ? node._init_assign->CopyExpression(old2new) : 0 ;
    _shared = node._shared ;
    _id_list = old2new.CopyIdDefArray(node._id_list) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

VhdlFileDecl::VhdlFileDecl(const VhdlFileDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id_list(0),
    _subtype_indication(0),
    _file_open_info(0)
{
    // Type is copied before ids, so that while copying iddefs _type field
    // can be populated from map.
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    _file_open_info = node._file_open_info ? node._file_open_info->Copy(old2new) : 0 ;
    _id_list = old2new.CopyIdDefArray(node._id_list) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

VhdlAliasDecl::VhdlAliasDecl(const VhdlAliasDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _designator(0),
    _subtype_indication(0),
    _alias_target_name(0)
{
    // Type is copied before ids, so that while copying iddefs _type field
    // can be populated from map.
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new) : 0 ;
    _alias_target_name = node._alias_target_name ? node._alias_target_name->CopyName(old2new) : 0 ;
    _designator = node._designator ? node._designator->CopyIdDef(old2new) : 0 ;
    if (_designator != node._designator) old2new.SetCopiedId(node._designator, _designator) ;
    if (_designator) _designator->SetTargetName(_alias_target_name) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    if (_designator) _designator->SetSubtypeIndication(_subtype_indication) ;
#endif
}
VhdlComponentDecl::VhdlComponentDecl(const VhdlComponentDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0),
    _generic_clause(0),
    _port_clause(0),
    _local_scope(0),
    _timestamp(0)
    , _orig_component(0)
{
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    CopyFrom(node, old2new) ; // Copy other fields from 'node' // #2375 : Use routine to reduce code duplication
}
// Introduced to support #2375 : Copy Component declaration with new name
void VhdlComponentDecl::CopyFrom(const VhdlComponentDecl &node, VhdlMapForCopy &old2new)
{
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;

    _generic_clause = old2new.CopyDeclarationArray(node._generic_clause) ;
    _port_clause = old2new.CopyDeclarationArray(node._port_clause) ;

    _timestamp = node._timestamp ;

    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;

    // Set back pointers in _id
    if (_id) _id->SetComponentDecl(this) ;
    if (_id) _id->SetLocalScope(_local_scope) ;
    // Traverse generic clause and port clause to set generic/port ids in id
    Array *ids ;
    VhdlInterfaceDecl *decl ;
    unsigned i ;

    Array *generic_list = 0 ;
    VhdlIdDef *gen_id ;
    if (_generic_clause) {
        generic_list = new Array(_generic_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these generics to the order-based generics array
            if (ids) generic_list->Append(ids) ;
            gen_id = decl->GetId() ; // interface type/subprogram and package
            if (gen_id) generic_list->InsertLast(gen_id) ;
        }
    }
    Array *port_list = 0 ;
    if (_port_clause) {
        port_list = new Array(_port_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these ports to the order-based ports array
            port_list->Append(ids) ;
        }
    }
    // generic_list and port_list are absorbed by the following id
    if (_id) _id->DeclareComponentId(generic_list, port_list) ;
    else {
        delete port_list ;
        delete generic_list ;
    }

    // Get the component from which this is copied. If the component from which it
    // is copying is itself copied component, set the original component of that copied component
    // VIPER #6320 : Set 'OrigName()' instead of 'Name(); as original component name
    _orig_component = node._orig_component ? Strings::save(node._orig_component) : Strings::save(node._id ? node._id->OrigName(): 0) ;
}
VhdlAttributeDecl::VhdlAttributeDecl(const VhdlAttributeDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0),
    _type_mark(0)
{
    _type_mark = node._type_mark ? node._type_mark->CopyName(old2new) : 0 ;
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype indication backpointer
    if (_id) _id->SetSubtypeIndication(_type_mark) ;
#endif
}

VhdlAttributeSpec::VhdlAttributeSpec(const VhdlAttributeSpec &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _designator(0),
    _entity_spec(0),
    _value(0)
{
    _designator = node._designator ? node._designator->CopyName(old2new) : 0 ;
    _entity_spec = node._entity_spec ? node._entity_spec->CopySpecification(old2new,0) : 0 ;
    _value = node._value ? node._value->CopyExpression(old2new) : 0 ;

    // Viper #5432
    VhdlIdDef *attr_id = _designator ? _designator->GetId() : 0 ;
    if (attr_id) attr_id->AddAttributeSpec(this) ;
}

VhdlConfigurationSpec::VhdlConfigurationSpec(const VhdlConfigurationSpec &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _component_spec(0),
    _binding(0)
{
    _component_spec = node._component_spec ? node._component_spec->CopySpecification(old2new,0) : 0 ;
    // VIPER #4552 : If we are copying entity having recursive instance via configuration spec, do
    // not change the reference of formal identifiers in formal part of association. If we do this,
    // formal part will point to copied entity. If formal part in association is changed for recursive
    // instance, we cannot handle recursive instance properly (it may have different generic value/architecture setting).
    old2new.DonotSwitchIdInFormal() ;
    _binding = node._binding ? node._binding->Copy(old2new) : 0 ;
    // Set the old->new binding link into the copy table :
    if (_binding) old2new.SetCopiedBinding(node._binding, _binding) ;
    old2new.ResetDonotSwitchIdInFormal() ;
}

VhdlDisconnectionSpec::VhdlDisconnectionSpec(const VhdlDisconnectionSpec &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _guarded_signal_spec(0),
    _after(0)
{
    _guarded_signal_spec = node._guarded_signal_spec ? node._guarded_signal_spec->CopySpecification(old2new,0) : 0 ;
    _after = node._after ? node._after->CopyExpression(old2new) : 0 ;
}
VhdlGroupTemplateDecl::VhdlGroupTemplateDecl(const VhdlGroupTemplateDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0),
    _entity_class_entry_list(0)
{
    if (node._entity_class_entry_list) {
        _entity_class_entry_list = new Array(node._entity_class_entry_list->Size()) ;
        unsigned i ;
        VhdlEntityClassEntry *entry ;
        FOREACH_ARRAY_ITEM(node._entity_class_entry_list, i, entry) {
            VhdlEntityClassEntry *new_entry = entry ? entry->Copy(old2new) : 0 ;
            _entity_class_entry_list->InsertLast(new_entry) ;
        }
    } else {
        _entity_class_entry_list = 0 ;
    }
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
    if (_id) _id->SetGroupTemplateDeclaration(this) ;
}

VhdlGroupDecl::VhdlGroupDecl(const VhdlGroupDecl &node, VhdlMapForCopy &old2new)
  : VhdlDeclaration(node, old2new),
    _id(0),
    _group_template_name(0)
{
    _group_template_name = node._group_template_name ? node._group_template_name->CopyName(old2new) : 0 ;
    _id = node._id ? node._id->CopyIdDef(old2new) : 0 ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;
}
VhdlSubprogInstantiationDecl::VhdlSubprogInstantiationDecl(const VhdlSubprogInstantiationDecl &node, VhdlMapForCopy &old2new)
   : VhdlDeclaration(node, old2new),
    _subprogram_spec(0),
    _uninstantiated_subprog_name(0),
    _generic_map_aspect(0),
    _generic_types(0)
{
    _subprogram_spec = (node._subprogram_spec) ? node._subprogram_spec->CopySpecification(old2new, 0 /* copy scope*/): 0 ;
    _uninstantiated_subprog_name = (node._uninstantiated_subprog_name) ? node._uninstantiated_subprog_name->CopyName(old2new) : 0 ;
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;

    VhdlIdDef *subprog_inst_id = _subprogram_spec ? _subprogram_spec->GetId() : 0 ;
    if (subprog_inst_id) subprog_inst_id->SetSubprogInstanceDecl(this) ;
    _generic_types = node._generic_types ? new Map(POINTER_HASH, node._generic_types->Size()): 0 ;
    if (_generic_types && node._generic_types) {
        MapIter mi ;
        VhdlIdDef *generic_id, *new_generic_id ;
        VhdlIdDef *actual_type, *new_actual_type ;
        FOREACH_MAP_ITEM(node._generic_types, mi, &generic_id, &actual_type) {
            new_generic_id = old2new.GetCopiedId(generic_id) ;
            if (!new_generic_id) new_generic_id = generic_id ;
            new_actual_type = old2new.GetCopiedId(actual_type) ;
            if (!new_actual_type) new_actual_type = actual_type ;
            (void) _generic_types->Insert(new_generic_id, new_actual_type) ;
        }
    }
}
VhdlInterfaceTypeDecl::VhdlInterfaceTypeDecl(const VhdlInterfaceTypeDecl &node, VhdlMapForCopy &old2new)
    : VhdlInterfaceDecl(node, old2new),
    _type_id(0)
{
    _type_id = node._type_id ? node._type_id->CopyIdDef(old2new): 0 ;
    if (_type_id) _type_id->SetLinefile(node._type_id->Linefile()) ;
    if (_type_id != node._type_id) old2new.SetCopiedId(node._type_id, _type_id) ;
}
VhdlInterfaceSubprogDecl::VhdlInterfaceSubprogDecl(const VhdlInterfaceSubprogDecl &node, VhdlMapForCopy &old2new)
    : VhdlInterfaceDecl(node, old2new),
    _subprogram_spec(0),
    _actual_subprog(0)
{
    _subprogram_spec = (node._subprogram_spec) ? node._subprogram_spec->CopySpecification(old2new, 0): 0 ;
    VhdlIdDef *spec = _subprogram_spec ? _subprogram_spec->GetId(): 0 ;
    if (spec) {
        spec->SetAsGeneric() ;
        spec->SetSubprogInstanceDecl(this) ;
    }
    _actual_subprog = (node._actual_subprog) ? old2new.GetCopiedId(node._actual_subprog) : node._actual_subprog ;
}
VhdlInterfacePackageDecl::VhdlInterfacePackageDecl(const VhdlInterfacePackageDecl &node, VhdlMapForCopy &old2new)
    : VhdlInterfaceDecl(node, old2new),
    _id(0),
    _uninst_package_name(0),
    _generic_map_aspect(0),
    _local_scope(0),
    _initial_pkg(0),
    _uninst_package_id(0),
    _generic_types(0),
    _actual_pkg(0)
    ,_actual_elab_pkg(0)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _id = (node._id) ? node._id->CopyIdDef(old2new): 0 ;
    if (_local_scope && _id) {
        _local_scope->SetOwner(_id) ;
        _id->SetLocalScope(_local_scope) ;
    }
    if (_id) _id->SetDeclaration(this) ;
    if (_id != node._id) old2new.SetCopiedId(node._id, _id) ;

    _uninst_package_name = (node._uninst_package_name) ? node._uninst_package_name->CopyName(old2new) : 0 ;
    _uninst_package_id = old2new.GetCopiedId(node._uninst_package_id) ;
    _initial_pkg = (node._initial_pkg) ? node._initial_pkg->CopyPrimaryUnit(0, old2new, 0): 0 ;
    if (_local_scope) _local_scope->SetUpper(old2new.GetCopiedScope(node._local_scope ? node._local_scope->Upper(): 0)) ;
    // Here identifiers of node._local_scope are owned by the scope, so need
    // to copy those
    Map *old_map = node._local_scope ? node._local_scope->DeclArea(): 0 ;
    MapIter mi ;
    char *name ;
    VhdlIdDef *old_id, *new_id ;
    Array *generics = 0 ;
    FOREACH_MAP_ITEM(old_map, mi, &name, &old_id) {
        if (old_id && (old_id == node._local_scope->GetOwner())) continue ;
        new_id = old_id ? old_id->CopyIdDef(old2new): 0 ;
        old2new.SetCopiedId(old_id, new_id) ;
        if (old_id && old_id->IsGeneric()) {
            if (!generics) generics = new Array(1) ;
            generics->InsertLast(new_id) ;
        }
        if (_local_scope) _local_scope->ForceDeclare(new_id) ;
#ifdef VHDL_ID_SCOPE_BACKPOINTER
        // VIPER #7258 : Set back pointer
        // Set the owning scope of the identifier :
        if (new_id) new_id->SetOwningScope(_local_scope) ;
#endif
    }
    if (_id && generics) _id->DeclarePackageId(generics) ;
    // Create alias identifier for each local identifier of '_initial_pkg'
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    _generic_types = node._generic_types ? new Map(POINTER_HASH, node._generic_types->Size()): 0 ;
    if (_generic_types && node._generic_types) {
        VhdlIdDef *generic_id, *new_generic_id ;
        VhdlIdDef *actual_type, *new_actual_type ;
        FOREACH_MAP_ITEM(node._generic_types, mi, &generic_id, &actual_type) {
            new_generic_id = old2new.GetCopiedId(generic_id) ;
            if (!new_generic_id) new_generic_id = generic_id ;
            new_actual_type = old2new.GetCopiedId(actual_type) ;
            if (!new_actual_type) new_actual_type = actual_type ;
            (void) _generic_types->Insert(new_generic_id, new_actual_type) ;
        }
    }
    _actual_pkg = old2new.GetCopiedId(node._actual_pkg) ;
}

VhdlDiscreteRange::VhdlDiscreteRange(const VhdlDiscreteRange &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new)
{
}
VhdlExpression::VhdlExpression(const VhdlExpression &node, VhdlMapForCopy &old2new)
  : VhdlDiscreteRange(node, old2new)
{
}
VhdlSubtypeIndication::VhdlSubtypeIndication(const VhdlSubtypeIndication &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new)
{
}
VhdlExplicitSubtypeIndication::VhdlExplicitSubtypeIndication(const VhdlExplicitSubtypeIndication &node, VhdlMapForCopy &old2new)
  : VhdlSubtypeIndication(node, old2new),
    _res_function(0),
    _type_mark(0),
    _range_constraint(0),
    _range_type(0)
{
    _res_function = node._res_function ? node._res_function->CopyName(old2new) : 0 ;
    _type_mark = node._type_mark ? node._type_mark->CopyName(old2new) : 0 ;
    _range_constraint = node._range_constraint ? node._range_constraint->CopyDiscreteRange(old2new) : 0 ;
    _range_type = old2new.GetCopiedId(node._range_type) ;
}
VhdlRange::VhdlRange(const VhdlRange &node, VhdlMapForCopy &old2new)
  : VhdlDiscreteRange(node, old2new),
    _left(0),
    _dir(0),
    _right(0)
{
    _left = node._left ? node._left->CopyExpression(old2new) : 0 ;
    _dir = node._dir ;
    _right = node._right ? node._right->CopyExpression(old2new) : 0 ;
}
VhdlBox::VhdlBox(const VhdlBox &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new)
{
}
VhdlAssocElement::VhdlAssocElement(const VhdlAssocElement &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _formal_part(0),
    _actual_part(0),
    _from_verilog(0)
{
    _formal_part = node._formal_part ? node._formal_part->CopyName(old2new) : 0 ;
    _actual_part = node._actual_part ?  node._actual_part->CopyExpression(old2new) : 0 ;
    _from_verilog = node._from_verilog ;
    // VIPER #4552 : If recursive entity instantiation/component specification exists,
    // do not point the formal identifiers to copied entity, let those point to original
    // entity. Otherwise we cannot handle recursive design properly.
    if (old2new.IsDonotSwitchIdInFormal() && !old2new.IsForLibraryCopy()) {
        // Get formal identifier of original formal part
        VhdlIdDef *old_formal =  node._formal_part ? node._formal_part->FindFullFormal(): 0 ;
        if (!old_formal) old_formal = node._formal_part ? node._formal_part->FindFormal(): 0 ;

        // Get formal identifier of copied formal part
        VhdlIdDef *new_formal = _formal_part ? _formal_part->FindFullFormal(): 0 ;
        if (!new_formal) new_formal = _formal_part ? _formal_part->FindFormal(): 0 ;

        // If formal identifier changed due to copy, point formal part to original formal
        if (old_formal && new_formal && (new_formal != old_formal)) {
            if (_formal_part) _formal_part->SetFormal(old_formal) ;
        }
    }
}
VhdlInertialElement::VhdlInertialElement(const VhdlInertialElement &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _actual_part(0)
{
    _actual_part = node._actual_part ?  node._actual_part->CopyExpression(old2new) : 0 ;
}

VhdlRecResFunctionElement::VhdlRecResFunctionElement(const VhdlRecResFunctionElement &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _record_id_name(0),
    _record_id_function(0)
{
    _record_id_name = node._record_id_name ? node._record_id_name->CopyName(old2new) : 0 ;
    _record_id_function = node._record_id_function ? node._record_id_function->CopyName(old2new) : 0 ;
}
VhdlOperator::VhdlOperator(const VhdlOperator &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _left(0),
    _oper(0),
    _is_implicit(0),
    _unused(0),
    _right(0),
    _operator(0)
{
    _left = node._left ? node._left->CopyExpression(old2new) : 0 ;
    _oper = node._oper ;
    _is_implicit = node._is_implicit ;
    _unused = node._unused ;
    _right = node._right ? node._right->CopyExpression(old2new) : 0 ;
    // Operator id is populated from map
    _operator = old2new.GetCopiedId(node._operator) ;
}
VhdlAllocator::VhdlAllocator(const VhdlAllocator &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _subtype(0)
{
    _subtype = node._subtype ? node._subtype->CopyExpression(old2new) : 0 ;
}
VhdlAggregate::VhdlAggregate(const VhdlAggregate &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _element_assoc_list(0),
    _aggregate_type(0),
    _element_type_list(0)
{
    _element_assoc_list = old2new.CopyDiscreteRangeArray(node._element_assoc_list) ;
    _aggregate_type = old2new.GetCopiedId(node._aggregate_type) ;
    _element_type_list = old2new.PopulateIdDefArray(node._element_type_list) ;
}
VhdlQualifiedExpression::VhdlQualifiedExpression(const VhdlQualifiedExpression &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _prefix(0),
    _aggregate(0)
{
    _prefix = node._prefix ? node._prefix->CopyName(old2new) : 0 ;
    _aggregate = node._aggregate ? node._aggregate->CopyExpression(old2new) : 0 ;
}
VhdlElementAssoc::VhdlElementAssoc(const VhdlElementAssoc &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _choices(0),
    _expr(0)
{
    _choices = old2new.CopyDiscreteRangeArray(node._choices) ;
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
}
VhdlWaveformElement::VhdlWaveformElement(const VhdlWaveformElement &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new),
    _value(0),
    _after(0)
{
    _value = node._value ? node._value->CopyExpression(old2new) : 0 ;
    _after = node._after ? node._after->CopyExpression(old2new) : 0 ;
}

// Copy the iddefs.
// type, attributes and name field of all identifiers are copied here.
// This constructor is called when any identifier is copied.
VhdlIdDef::VhdlIdDef(const VhdlIdDef &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _name((node._name) ? Strings::save(node._name) : 0),
#ifdef VHDL_PRESERVE_ID_CASE
    _orig_name((node._orig_name) ? Strings::save(node._orig_name) : 0),
#endif
    _type(0),
    _attributes(0)
#ifdef VHDL_ID_SCOPE_BACKPOINTER
    , _owning_scope(0)
#endif
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    , _subtype_indication(0)
#endif
    , _constraint(0)
    , _value(0)
    //CARBON_BEGIN
    , _pragmas(0)
    //CARBON_END
{
    _type = old2new.GetCopiedId(node._type) ;
#if 0
    // RD: Cannot copy attributes yet. They contain (attribute) id backpointers they will not yet have been copied.
    // Will set the attributes when we process the attribute specs (in UpdateDecl()).
    if (node._attributes) {
        //
        _attributes = new Map(POINTER_HASH, node._attributes->Size()) ;
        MapIter mi ;
        VhdlIdDef *id ;
        VhdlValue *value ;
        FOREACH_MAP_ITEM(node._attributes, mi, &id, &value) {
            VhdlIdDef *new_id = old2new.GetCopiedId(id) ;
            // FIX ME : value is now an VhdlExpression backpointer..
            VhdlValue *new_value = 0 ; // value ? value->Copy() : 0 ;
            (void) _attributes->Insert(new_id, new_value, 1) ;
        }
    }
#endif
    // _owning_scope gets set when identifier is added to a scope
    _constraint = node._constraint ? node._constraint->Copy() : 0 ;
    _value = node._value ? node._value->Copy() : 0 ;
    //CARBON_BEGIN
    if (node.gPragmas()) CopyPragmas(node);
    //CARBON_END
}
VhdlDefault::VhdlDefault(const VhdlDefault &node, VhdlMapForCopy &old2new)
  : VhdlExpression(node, old2new)
{
}
VhdlLibraryId::VhdlLibraryId(const VhdlLibraryId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new)
{
}
VhdlGroupId::VhdlGroupId(const VhdlGroupId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new)
{
}
VhdlGroupTemplateId::VhdlGroupTemplateId(const VhdlGroupTemplateId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _decl(0)
{
}
VhdlAttributeId::VhdlAttributeId(const VhdlAttributeId &node, VhdlMapForCopy &old2new)
    : VhdlIdDef(node, old2new), _attribute_specs(0)
{
}
VhdlComponentId::VhdlComponentId(const VhdlComponentId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _ports(0),
    _generics(0),
    _scope(0),
    _component(0)
{
    // In component declaration this id is used. While copying
    // component declaration, component id is copied after copying
    // ports and generics. This help us to populate the following
    // pointers here from map.
    //_ports = old2new.PopulateIdDefArray(node._ports) ; // #2375 : Don't populate here: will be from decl constructor
    //_generics = old2new.PopulateIdDefArray(node._generics) ;
}
VhdlAliasId::VhdlAliasId(const VhdlAliasId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _target_name(0),
    _target_id(0),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type),
    _kind(node._kind)
{
    // This is called from alias declaration where it is copied
    // after target id. So target id field can be populated from map.
    _target_id = old2new.GetCopiedId(node._target_id) ;
}
VhdlFileId::VhdlFileId(const VhdlFileId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _file(0)
{
    // _file is NOT copied. Re-initialize to 0.
}
VhdlVariableId::VhdlVariableId(const VhdlVariableId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _is_assigned(node._is_assigned),
    _is_used(node._is_used ),
    _is_concurrent_assigned(node._is_concurrent_assigned),
    _is_concurrent_used(node._is_concurrent_used),
    _can_be_dual_port_ram(node._can_be_dual_port_ram),
    _can_be_multi_port_ram(node._can_be_multi_port_ram),
    _is_assigned_before_used(node._is_assigned_before_used),
    _is_used_before_assigned(node._is_used_before_assigned),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type),
    _is_shared(node._is_shared)
{
}
VhdlSignalId::VhdlSignalId(const VhdlSignalId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _resolution_function(0),
    _is_assigned(node._is_assigned),
    _is_used(node._is_used),
    _is_concurrent_assigned(node._is_concurrent_assigned),
    _is_concurrent_used(node._is_concurrent_used),
    _can_be_dual_port_ram(node._can_be_dual_port_ram),
    _can_be_multi_port_ram(node._can_be_multi_port_ram),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type),
    _signal_kind(node._signal_kind)
{
    // This id is copied from signal declaration after copying its type.
    _resolution_function = old2new.GetCopiedId(node._resolution_function) ;
}
VhdlConstantId::VhdlConstantId(const VhdlConstantId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _decl(0),
    _is_locally_static(node._is_locally_static),
    _is_globally_static(node._is_globally_static),
    _is_seq_loop_iter(node._is_seq_loop_iter),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type)
{
}
VhdlTypeId::VhdlTypeId(const VhdlTypeId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _type_enum(node._type_enum),
    _list(0),
    _scope(0),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    ,_verilog_pkg_lib_name((node._verilog_pkg_lib_name) ? Strings::save(node._verilog_pkg_lib_name) : 0)
    ,_verilog_pkg_name((node._verilog_pkg_name) ? Strings::save(node._verilog_pkg_name) : 0)
#endif
    ,_type_decl(node._type_decl) // VIPER #7343
{
}
VhdlGenericTypeId::VhdlGenericTypeId(const VhdlGenericTypeId &node, VhdlMapForCopy &old2new)
    : VhdlTypeId(node, old2new)
{
}
VhdlProtectedTypeId::VhdlProtectedTypeId(const VhdlProtectedTypeId &node, VhdlMapForCopy &old2new)
    : VhdlTypeId(node, old2new),
      _type_def(0),
      _body_id(0)
{
}
VhdlProtectedTypeBodyId::VhdlProtectedTypeBodyId(const VhdlProtectedTypeBodyId &node, VhdlMapForCopy &old2new)
    : VhdlTypeId(node, old2new),
      _type_def_body(0),
      _typedef_id(0)
{
}
VhdlUniversalInteger::VhdlUniversalInteger(const VhdlUniversalInteger &node, VhdlMapForCopy &old2new)
  : VhdlTypeId(node, old2new)
{
}
VhdlUniversalReal::VhdlUniversalReal(const VhdlUniversalReal &node, VhdlMapForCopy &old2new)
  : VhdlTypeId(node, old2new)
{
}
VhdlAnonymousType::VhdlAnonymousType(const VhdlAnonymousType &node, VhdlMapForCopy &old2new)
  : VhdlTypeId(node, old2new)
{
}
VhdlSubtypeId::VhdlSubtypeId(const VhdlSubtypeId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _resolution_function(0),
    _is_unconstrained(node._is_unconstrained),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type),
    _type_decl(node._type_decl)
{
    _resolution_function = old2new.GetCopiedId(node._resolution_function) ;
}
VhdlSubprogramId::VhdlSubprogramId(const VhdlSubprogramId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _generics(0),
    _args(0),
    _spec(0),
    _body(0),
    _pragma_function(node._pragma_function),
    _pragma_signed(node._pragma_signed),
    _is_assigned_before_used(node._is_assigned_before_used),
    _is_used_before_assigned(node._is_used_before_assigned),
    _is_procedure(node._is_procedure),
    _is_processing(0),
    _is_label_applies_to_pragma(0),
    _is_generic(node._is_generic),
    _is_overwritten_predefinedOperator(node._is_overwritten_predefinedOperator),
    _sub_inst(0)
{
    _generics = old2new.PopulateIdDefArray(node._generics) ;
    _args = old2new.PopulateIdDefArray(node._args) ;
}
VhdlOperatorId::VhdlOperatorId(const VhdlOperatorId &node, VhdlMapForCopy &old2new)
  : VhdlSubprogramId(node, old2new),
    _oper_type(node._oper_type), _owns_arg_id(node._owns_arg_id)
{
    if (_owns_arg_id) { // VIPER #5996
        unsigned i ;
        VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(_args, i, id) {
            VhdlIdDef *new_id = id ? id->CopyIdDef(old2new) : 0 ;
            _args->Insert(i, new_id) ;
        }
    }
}
// Predefined operators are copied before copying the declarations
// of any scoped object. So after copying the declarations and statements
// of the scoped object, the type and args of operator id are to be
// updated.
void
VhdlOperatorId::UpdateArgsAndType(VhdlMapForCopy &old2new)
{
    if (_type) _type = old2new.GetCopiedId(_type) ;
    if (_args && !_owns_arg_id) { // VIPER #5996 : added _owns_arg_id
        unsigned i ;
        VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(_args, i, id) {
            VhdlIdDef *new_id = old2new.GetCopiedId(id) ;
            _args->Insert(i, new_id) ;
        }
    }
}
void
VhdlAnonymousType::UpdateAnonymousType(VhdlIdDef *old_id, VhdlMapForCopy &old2new)
{
    // Viper #6146 : fill in the fields correctly
    if (!old_id) return ;
    VhdlIdDef *ele_type = old_id->IsArrayType() ? old_id->ElementType() : 0 ;
    VhdlIdDef *ele_type_new = ele_type ? old2new.GetCopiedId(ele_type) : 0 ;
    if (ele_type_new) {
        Map *index_types = new Map(STRING_HASH) ;
        MapIter mi ;
        VhdlIdDef *id ;
        FOREACH_MAP_ITEM(old_id->GetTypeDefList(), mi, 0, &id) {
            VhdlIdDef *new_id = id ? old2new.GetCopiedId(id) : 0 ;
            if (new_id) (void) index_types->Insert(new_id->Name(), new_id, 0, 1) ;
        }

        _list = index_types ;
        _type = ele_type_new ;
    }
}
VhdlInterfaceId::VhdlInterfaceId(const VhdlInterfaceId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _resolution_function(0),
    _object_kind(node._object_kind),
    _kind(node._kind),
    _mode(node._mode),
    _signal_kind(node._signal_kind),
    _has_init_assign(node._has_init_assign),
    _is_unconstrained(node._is_unconstrained ),
    _is_assigned(node._is_assigned ),
    _is_used(node._is_used),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type),
    _is_ref_formal(node._is_ref_formal),
    _can_be_multi_port_ram(node._can_be_multi_port_ram)
   , _verilog_assoc_list(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
   , _verilog_actual_size(node._verilog_actual_size)
#endif
   , _is_actual_present(node._is_actual_present)
   , _ref_expr(0)
{
    if (node._verilog_assoc_list) {
        _verilog_assoc_list = old2new.CopyDiscreteRangeArray(node._verilog_assoc_list) ;
    }
    _resolution_function = old2new.GetCopiedId(node._resolution_function) ;
}
VhdlEnumerationId::VhdlEnumerationId(const VhdlEnumerationId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _position(node._position),
    _bit_encoding(node._bit_encoding),
    _onehot_encoded(node._onehot_encoded),
    _enum_encoded(node._enum_encoded),
    _encoding((node._encoding) ? Strings::save(node._encoding) : 0)
{
}
VhdlElementId::VhdlElementId(const VhdlElementId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _position(node._position),
    _is_locally_static_type(node._is_locally_static_type),
    _is_globally_static_type(node._is_globally_static_type)
{
}
VhdlPhysicalUnitId::VhdlPhysicalUnitId(const VhdlPhysicalUnitId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _position(node._position),
    _inv_position(node._inv_position)
{
}
VhdlEntityId::VhdlEntityId(const VhdlEntityId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _ports(0),
    _generics(0),
    _scope(0),
    _unit(0)
{
}
VhdlArchitectureId::VhdlArchitectureId(const VhdlArchitectureId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _entity(0),
    _scope(0),
    _unit(0)
{
}
VhdlConfigurationId::VhdlConfigurationId(const VhdlConfigurationId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _entity(0),
    _scope(0),
    _unit(0)
{
    _entity = old2new.GetCopiedId(node._entity) ;
}
VhdlPackageId::VhdlPackageId(const VhdlPackageId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _generics(0),
    _scope(0),
    _unit(0),
    _decl(0),
    _uninstantiated_pkg_id()
{
    _uninstantiated_pkg_id = old2new.GetCopiedId(node._uninstantiated_pkg_id) ;
}
VhdlPackageBodyId::VhdlPackageBodyId(const VhdlPackageBodyId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _scope(0),
    _unit(0)
{
}
VhdlContextId::VhdlContextId(const VhdlContextId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _scope(0),
    _unit(0)
{
}
VhdlLabelId::VhdlLabelId(const VhdlLabelId &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _scope(0),
    _statement(0),
    _primary_binding(0)
{
    _scope = old2new.GetCopiedScope(node._scope) ;

    // primary binding backpointer might need to point to the new binding (from a component spec).
    // RD: cannot simply set primary binding backpointer.
    // This one needs to be re-generated to point to the new configuration specification binding.
    // Assuming that this label gets created when the component instantiation is copied (and not when we copy the scope)
    // we should be able to get the new binding pointer from the table :
    if (node._primary_binding) {
        _primary_binding = old2new.GetCopiedBinding(node._primary_binding) ;
    }
}
VhdlBlockId::VhdlBlockId(const VhdlBlockId &node, VhdlMapForCopy &old2new)
  : VhdlLabelId(node, old2new),
    _generics(0),
    _ports(0)
{
}
VhdlPackageInstElement::VhdlPackageInstElement(const VhdlPackageInstElement &node, VhdlMapForCopy &old2new)
  : VhdlIdDef(node, old2new),
    _actual_id(0),
    _target_id(0)
{
    _actual_id = old2new.GetCopiedId(node._actual_id) ;
    _target_id = old2new.GetCopiedId(node._target_id) ;
}
VhdlSignature::VhdlSignature(const VhdlSignature &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _type_mark_list(0),
    _return_type(0)
{
    _type_mark_list = old2new.CopyNameArray(node._type_mark_list) ;
    _return_type = node._return_type ? node._return_type->CopyName(old2new) : 0 ;
}
VhdlFileOpenInfo::VhdlFileOpenInfo(const VhdlFileOpenInfo &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _file_open(0),
    _open_mode(node._open_mode),
    _file_logical_name(0)
{
    _file_open = node._file_open ? node._file_open->CopyExpression(old2new) : 0 ;
    _file_logical_name = node._file_logical_name ? node._file_logical_name->CopyExpression(old2new) : 0 ;
}

VhdlBindingIndication::VhdlBindingIndication(const VhdlBindingIndication &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _entity_aspect(0),
    _generic_map_aspect(0),
    _port_map_aspect(0),
    _unit(0)
{
    _entity_aspect = node._entity_aspect ? node._entity_aspect->CopyName(old2new) : 0 ;
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    _port_map_aspect = old2new.CopyDiscreteRangeArray(node._port_map_aspect) ;
    _unit = old2new.GetCopiedId(node._unit) ;
}
VhdlIterScheme::VhdlIterScheme(const VhdlIterScheme &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new)
{
}
VhdlWhileScheme::VhdlWhileScheme(const VhdlWhileScheme &node, VhdlMapForCopy &old2new)
  : VhdlIterScheme(node, old2new),
    _condition(0),
    _exit_label(0),
    _next_label(0)
{
    _condition = node._condition ? node._condition->CopyExpression(old2new) : 0 ;
    _exit_label = node._exit_label ? node._exit_label->CopyIdDef(old2new) : 0 ;
    if (_exit_label != node._exit_label) old2new.SetCopiedId(node._exit_label, _exit_label) ;
    _next_label = node._next_label ? node._next_label->CopyIdDef(old2new) : 0 ;
    if (_next_label != node._next_label) old2new.SetCopiedId(node._next_label, _next_label) ;
}
VhdlForScheme::VhdlForScheme(const VhdlForScheme &node, VhdlMapForCopy &old2new)
  : VhdlIterScheme(node, old2new),
    _id(0),
    _range(0),
    _iter_id(0),
    _exit_label(0),
    _next_label(0)
{
    _id = node._id ? node._id->Copy(old2new) : 0 ;
    _range = node._range ? node._range->CopyDiscreteRange(old2new) : 0 ;
    _exit_label = node._exit_label ? node._exit_label->CopyIdDef(old2new) : 0 ;
    if (_exit_label != node._exit_label) old2new.SetCopiedId(node._exit_label, _exit_label) ;
    _next_label = node._next_label ? node._next_label->CopyIdDef(old2new) : 0 ;
    if (_next_label != node._next_label) old2new.SetCopiedId(node._next_label, _next_label) ;
    _iter_id = node._iter_id ? node._iter_id->CopyIdDef(old2new) : 0 ;
    if (_iter_id != node._iter_id) old2new.SetCopiedId(node._iter_id, _iter_id) ;
}
VhdlIfScheme::VhdlIfScheme(const VhdlIfScheme &node, VhdlMapForCopy &old2new)
  : VhdlIterScheme(node, old2new),
    _condition(0),
    _alternative_label(0),
    _alternative_label_id(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _alternative_closing_label(0)
#endif
{
    _condition = node._condition ? node._condition->CopyExpression(old2new) : 0 ;
    _alternative_label_id = node._alternative_label_id ? node._alternative_label_id->CopyIdDef(old2new) : 0 ;
    if (_alternative_label_id != node._alternative_label_id) old2new.SetCopiedId(node._alternative_label_id, _alternative_label_id) ;
    _alternative_label = node._alternative_label ? node._alternative_label->Copy(old2new) : 0 ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    if (node._alternative_closing_label) _alternative_closing_label = node._alternative_closing_label->CopyDesignator(old2new) ;
#endif
}
VhdlElsifElseScheme::VhdlElsifElseScheme(const VhdlElsifElseScheme &node, VhdlMapForCopy &old2new)
  : VhdlIfScheme(node, old2new)
{}
VhdlCaseItemScheme::VhdlCaseItemScheme(const VhdlCaseItemScheme &node, VhdlMapForCopy &old2new)
  : VhdlIterScheme(node, old2new),
    _choices(0),
    _alternative_label(0),
    _alternative_label_id(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _alternative_closing_label(0)
#endif
{
    _choices = old2new.CopyDiscreteRangeArray(node._choices) ;
    _alternative_label_id = node._alternative_label_id ? node._alternative_label_id->CopyIdDef(old2new) : 0 ;
    if (_alternative_label_id != node._alternative_label_id) old2new.SetCopiedId(node._alternative_label_id, _alternative_label_id) ;
    _alternative_label = node._alternative_label ? node._alternative_label->Copy(old2new) : 0 ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    if (node._alternative_closing_label) _alternative_closing_label = node._alternative_closing_label->CopyDesignator(old2new) ;
#endif
}
VhdlDelayMechanism::VhdlDelayMechanism(const VhdlDelayMechanism &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new)
{
}
VhdlTransport::VhdlTransport(const VhdlTransport &node, VhdlMapForCopy &old2new)
  : VhdlDelayMechanism(node, old2new)
{
}
VhdlInertialDelay::VhdlInertialDelay(const VhdlInertialDelay &node, VhdlMapForCopy &old2new)
  : VhdlDelayMechanism(node, old2new),
    _reject_expression(0),
    _inertial(node._inertial)
{
    _reject_expression = node._reject_expression ? node._reject_expression->CopyExpression(old2new) : 0 ;
}
VhdlOptions::VhdlOptions(const VhdlOptions &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _guarded(node._guarded),
    _delay_mechanism(0)
{
    _delay_mechanism = node._delay_mechanism ? node._delay_mechanism->CopyDelayMechanism(old2new) : 0 ;
}
VhdlConditionalWaveform::VhdlConditionalWaveform(const VhdlConditionalWaveform &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _waveform(0),
    _when_condition(0)
{
    _waveform = old2new.CopyExpressionArray(node._waveform) ;
    _when_condition = node._when_condition ? node._when_condition->CopyExpression(old2new) : 0 ;
}
VhdlSelectedWaveform::VhdlSelectedWaveform(const VhdlSelectedWaveform &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _waveform(0),
    _when_choices(0)
{
    _waveform = old2new.CopyExpressionArray(node._waveform) ;
    _when_choices = old2new.CopyDiscreteRangeArray(node._when_choices) ;
}
VhdlSelectedExpression::VhdlSelectedExpression(const VhdlSelectedExpression &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _expr(0),
    _when_choices(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    _when_choices = old2new.CopyDiscreteRangeArray(node._when_choices) ;
}
VhdlConditionalExpression::VhdlConditionalExpression(const VhdlConditionalExpression &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _expr(0),
    _condition(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    _condition = node._condition ? node._condition->CopyExpression(old2new) : 0 ;
}
VhdlBlockGenerics::VhdlBlockGenerics(const VhdlBlockGenerics &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _generic_clause(0),
    _generic_map_aspect(0),
    _block_label(0)
{
    _generic_clause = old2new.CopyDeclarationArray(node._generic_clause) ;
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    Array *ids ;
    unsigned i ;
    Array *generics = (_generic_clause) ? new Array(_generic_clause->Size()) : 0 ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        ids = decl->GetIds() ;
        generics->Append(ids) ;
    }
    _block_label = old2new.GetCopiedId(node._block_label) ;
    // Set back pointers
    if (_block_label && (_block_label != node._block_label)) {
        _block_label->DeclareBlockId(generics, 0) ;
    } else {
        delete generics ;
    }
}
VhdlBlockPorts::VhdlBlockPorts(const VhdlBlockPorts &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _port_clause(0),
    _port_map_aspect(0),
    _block_label(0)
{
    _port_clause = old2new.CopyDeclarationArray(node._port_clause) ;
    _port_map_aspect = old2new.CopyDiscreteRangeArray(node._port_map_aspect) ;
    Array *ids ;
    VhdlInterfaceDecl *decl ;
    unsigned i ;
    Array *ports = (_port_clause) ? new Array(_port_clause->Size()) : 0 ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        ids = decl->GetIds() ;
        ports->Append(ids) ;
    }
    _block_label = old2new.GetCopiedId(node._block_label) ;
    // Set back pointers
    if (_block_label && (_block_label != node._block_label)) {
        _block_label->DeclareBlockId(0, ports) ;
    } else {
        delete ports ;
    }
}
VhdlCaseStatementAlternative::VhdlCaseStatementAlternative(const VhdlCaseStatementAlternative &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _choices(0),
    _statements(0)
{
    _choices = old2new.CopyDiscreteRangeArray(node._choices) ;
    _statements = old2new.CopyStatementArray(node._statements) ;
}
VhdlElsif::VhdlElsif(const VhdlElsif &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _condition(0),
    _statements(0)
{
    _condition = node._condition ? node._condition->CopyExpression(old2new) : 0 ;
    _statements = old2new.CopyStatementArray(node._statements) ;
}
VhdlEntityClassEntry::VhdlEntityClassEntry(const VhdlEntityClassEntry &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _entity_class(node._entity_class),
    _box(node._box)
{
}

// Copy name referencing, literals and designators.
// In the following copy routines iddefs are populated from map.
VhdlName::VhdlName(const VhdlName &node, VhdlMapForCopy &old2new)
  : VhdlSubtypeIndication(node, old2new)
{
}
VhdlDesignator::VhdlDesignator(const VhdlDesignator &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _name(0),
    _single_id(0)
{
    _single_id = (node._single_id) ? old2new.GetCopiedId(node._single_id) : 0 ;
    if (node._single_id && node._name && Strings::compare(node._single_id->Name(), node._name)) {
        _name = _single_id ? Strings::save(_single_id->Name()) : Strings::save(node._name) ;
    } else {
        _name = (node._name) ? Strings::save(node._name) : 0 ;
    }
}
VhdlLiteral::VhdlLiteral(const VhdlLiteral &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new)
{
}
VhdlAll::VhdlAll(const VhdlAll &node, VhdlMapForCopy &old2new)
  : VhdlDesignator(node, old2new)
{
}
VhdlOthers::VhdlOthers(const VhdlOthers &node, VhdlMapForCopy &old2new)
  : VhdlDesignator(node, old2new)
{
}
VhdlUnaffected::VhdlUnaffected(const VhdlUnaffected &node, VhdlMapForCopy &old2new)
  : VhdlDesignator(node, old2new)
{
}
VhdlInteger::VhdlInteger(const VhdlInteger &node, VhdlMapForCopy &old2new)
  : VhdlLiteral(node, old2new),
    _value(node._value)
{
}
VhdlReal::VhdlReal(const VhdlReal &node, VhdlMapForCopy &old2new)
  : VhdlLiteral(node, old2new),
    _value(node._value)
{
}
VhdlStringLiteral::VhdlStringLiteral(const VhdlStringLiteral &node, VhdlMapForCopy &old2new)
  : VhdlDesignator(node, old2new)
{
}
VhdlBitStringLiteral::VhdlBitStringLiteral(const VhdlBitStringLiteral &node, VhdlMapForCopy &old2new)
  : VhdlStringLiteral(node, old2new),
    _based_string((node._based_string) ? Strings::save(node._based_string) : 0)
{
}
VhdlCharacterLiteral::VhdlCharacterLiteral(const VhdlCharacterLiteral &node, VhdlMapForCopy &old2new)
  : VhdlDesignator(node, old2new)
{
}
VhdlPhysicalLiteral::VhdlPhysicalLiteral(const VhdlPhysicalLiteral &node, VhdlMapForCopy &old2new)
  : VhdlLiteral(node, old2new),
    _value(0),
    _unit(0)
{
    _value = node._value ? node._value->CopyExpression(old2new) : 0 ;
    _unit = node._unit ? node._unit->CopyName(old2new) : 0 ;
}
VhdlNull::VhdlNull(const VhdlNull &node, VhdlMapForCopy &old2new)
  : VhdlLiteral(node, old2new)
{
}
VhdlIdRef::VhdlIdRef(const VhdlIdRef &node, VhdlMapForCopy &old2new)
  : VhdlDesignator(node, old2new)
{
}
VhdlArrayResFunction::VhdlArrayResFunction(const VhdlArrayResFunction &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _res_function(0)
{
    _res_function = node._res_function ? node._res_function->CopyName(old2new) : 0 ;
}
VhdlRecordResFunction::VhdlRecordResFunction(const VhdlRecordResFunction &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _rec_res_function_list(0)
{
    _rec_res_function_list = old2new.CopyDiscreteRangeArray(node._rec_res_function_list) ;
}
VhdlSelectedName::VhdlSelectedName(const VhdlSelectedName &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _prefix(0),
    _suffix(0),
    _unique_id(0)
{
    _prefix = node._prefix ? node._prefix->CopyName(old2new) : 0 ;
    _suffix = node._suffix ? node._suffix->CopyDesignator(old2new) : 0 ;
    _unique_id = old2new.GetCopiedId(node._unique_id) ;
    if (node._unique_id && _unique_id && (Strings::compare(node._unique_id->Name(), _unique_id->Name())==0)) {
        // Modify name of _suffix, if name of _unique_id is changed
        if (_suffix) _suffix->SetName(Strings::save(_unique_id->Name())) ;
    }
}
VhdlIndexedName::VhdlIndexedName(const VhdlIndexedName &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _prefix(0),
    _assoc_list(0),
    _prefix_id(0),
    _prefix_type(0),
    _is_array_index(node._is_array_index),
    _is_array_slice(node._is_array_slice),
    _is_function_call(node._is_function_call),
    _is_index_constraint(node._is_index_constraint),
    _is_record_constraint(node._is_record_constraint),
    _is_type_conversion(node._is_type_conversion),
    _is_group_template_ref(node._is_group_template_ref)
{
    _prefix = node._prefix ? node._prefix->CopyName(old2new) : 0 ;
    _assoc_list = old2new.CopyDiscreteRangeArray(node._assoc_list) ;
    _prefix_id = old2new.GetCopiedId(node._prefix_id) ;
    _prefix_type = old2new.GetCopiedId(node._prefix_type) ;
}
VhdlSignaturedName::VhdlSignaturedName(const VhdlSignaturedName &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _name(0),
    _signature(0),
    _unique_id(0)
{
    _name = node._name ? node._name->CopyName(old2new) : 0 ;
    _signature = node._signature ? node._signature->Copy(old2new) : 0 ;
    _unique_id = old2new.GetCopiedId(node._unique_id) ;
}
VhdlAttributeName::VhdlAttributeName(const VhdlAttributeName &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _prefix(0),
    _designator(0),
    _expression(0),
    _attr_enum(node._attr_enum),
    _prefix_type(0),
    _result_type(0)
{
    _prefix = node._prefix ? node._prefix->CopyName(old2new) : 0 ;
    _designator = node._designator ? node._designator->Copy(old2new) : 0 ;
    _expression = node._expression ? node._expression->CopyExpression(old2new) : 0 ;
    _prefix_type = old2new.GetCopiedId(node._prefix_type) ;
    _result_type = old2new.GetCopiedId(node._result_type) ;

    VhdlIdDef *prefix_id = _prefix ? _prefix->GetId(): 0 ;

    // While copying 'old2new' may not contain copied version of entity identifier
    // for its original via GetCopiedId() routine. But we need that here when
    // prefix of attribute is entity identifier. They are stored during entity
    // copy in VhdlMapForCopy separately. Use that information now to replace
    // reference of entity identifier :
    if (prefix_id && prefix_id->IsEntity()) {
        VhdlIdDef *new_entity_id = old2new.GetCopiedEntityId(prefix_id) ;
        if (new_entity_id && _prefix) {
            _prefix->SetId(new_entity_id) ;
            _prefix->SetName(Strings::save(new_entity_id->Name())) ;
        }
    }
}
VhdlOpen::VhdlOpen(const VhdlOpen &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new)
{
}
VhdlEntityAspect::VhdlEntityAspect(const VhdlEntityAspect &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _entity_class(node._entity_class),
    _name(0)
{
    _name = node._name ? node._name->CopyName(old2new) : 0 ;
}
VhdlInstantiatedUnit::VhdlInstantiatedUnit(const VhdlInstantiatedUnit &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _entity_class(node._entity_class),
    _name(0)
{
    _entity_class = node._entity_class ;
    _name = node._name ? node._name->CopyName(old2new) : 0 ;
}
VhdlExternalName::VhdlExternalName(const VhdlExternalName &node, VhdlMapForCopy &old2new)
  : VhdlName(node, old2new),
    _class_type(0),
    _path_token(0),
    _hat_count(0),
    _ext_path_name(0),
    _subtype_indication(0),
    _unique_id(0)
{
    _class_type = node._class_type ;
    _path_token = node._path_token ;
    _hat_count = node._hat_count ;
    _ext_path_name = node._ext_path_name ? node._ext_path_name->CopyName(old2new) : 0 ;
    _subtype_indication = node._subtype_indication ? node._subtype_indication->CopySubtypeIndication(old2new): 0 ;
    _unique_id = old2new.GetCopiedId(node._unique_id) ;
}
// Copy specifications
VhdlSpecification::VhdlSpecification(const VhdlSpecification &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new)
{
}
VhdlProcedureSpec::VhdlProcedureSpec(const VhdlProcedureSpec &node, unsigned exclude_scope, VhdlMapForCopy &old2new)
  : VhdlSpecification(node, old2new),
    _designator(0),
    _formal_parameter_list(0),
    _local_scope(0),
    _generic_clause(0),
    _generic_map_aspect(0),
    _has_explicit_param(0)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _generic_clause = old2new.CopyDeclarationArray(node._generic_clause) ;
    _formal_parameter_list = old2new.CopyDeclarationArray(node._formal_parameter_list) ;
    _designator = node._designator ? node._designator->CopyIdDef(old2new) : 0 ;
    VERIFIC_ASSERT(_designator) ;
    if (_designator != node._designator) old2new.SetCopiedId(node._designator, _designator) ;
    if (!exclude_scope && _local_scope) _local_scope->Update(node._local_scope, old2new) ;
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    _has_explicit_param = node._has_explicit_param ;

    if (!exclude_scope) { // Only prototype
        _designator->SetSpec(this) ;
        old2new.SetCopiedSpecification(&node, this) ;
    } else {
        // Set back pointers
        VhdlIdDef *oldspec_id = node.GetId() ;
        VERIFIC_ASSERT(oldspec_id) ;
        if ((VhdlProcedureSpec*)oldspec_id->Spec() == &node) {
              // no cross link
             _designator->SetSpec(this) ;
        } else {
            // Cross link exists
            // Get the spec of subprogram id
            VhdlSpecification *prev_spec = oldspec_id->Spec() ;
            VhdlSpecification *copied_spec = old2new.GetCopiedSpecification(prev_spec) ;
            if (copied_spec && (copied_spec != prev_spec)) {
                _designator->SetSpec(copied_spec) ;
                if (copied_spec->GetId()) copied_spec->GetId()->SetSpec(this) ;
            } else {
                old2new.SetCopiedSpecification(&node, this) ;
            }
        }
    }
}
VhdlFunctionSpec::VhdlFunctionSpec(const VhdlFunctionSpec &node, unsigned exclude_scope, VhdlMapForCopy &old2new)
  : VhdlSpecification(node, old2new),
    _pure_impure(node._pure_impure),
    _designator(0),
    _formal_parameter_list(0),
    _return_type(0),
    _local_scope(0),
    _generic_clause(0),
    _generic_map_aspect(0),
    _has_explicit_param(0)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _generic_clause = old2new.CopyDeclarationArray(node._generic_clause) ;
    _formal_parameter_list = old2new.CopyDeclarationArray(node._formal_parameter_list) ;
    _return_type = node._return_type ? node._return_type->CopyName(old2new) : 0 ;
    _designator = node._designator ? node._designator->CopyIdDef(old2new) : 0 ;
    VERIFIC_ASSERT(_designator) ;

    if (_designator != node._designator) old2new.SetCopiedId(node._designator, _designator) ;
    if (!exclude_scope && _local_scope) _local_scope->Update(node._local_scope, old2new) ;
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    _has_explicit_param = node._has_explicit_param ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    _designator->SetSubtypeIndication(_return_type) ;
#endif

    if (!exclude_scope) { // Only prototype
        _designator->SetSpec(this) ;
        old2new.SetCopiedSpecification(&node, this) ;
    } else {
        VhdlIdDef *oldspec_id = node.GetId() ;
        VERIFIC_ASSERT(oldspec_id) ;
        if ((VhdlFunctionSpec*)oldspec_id->Spec() == &node) {
              // no cross link
             _designator->SetSpec(this) ;
        } else {
            // Cross link exists
            // Get the spec of subprogram id
            VhdlSpecification *prev_spec = oldspec_id->Spec() ;
            VhdlSpecification *copied_spec = old2new.GetCopiedSpecification(prev_spec) ;
            if (copied_spec && (prev_spec != copied_spec)) {
                _designator->SetSpec(copied_spec) ;
                if (copied_spec->GetId()) copied_spec->GetId()->SetSpec(this) ;
            } else {
                old2new.SetCopiedSpecification(&node, this) ;
            }
        }
    }
}
VhdlComponentSpec::VhdlComponentSpec(const VhdlComponentSpec &node, VhdlMapForCopy &old2new)
  : VhdlSpecification(node, old2new),
    _id_list(0),
    _name(0),
    _component(0),
    _scope(0)
{
    _id_list = old2new.CopyNameArray(node._id_list) ;
    _name = node._name ? node._name->CopyName(old2new) : 0 ;
    _component = old2new.GetCopiedId(node._component) ;
    _scope = old2new.GetCopiedScope(node._scope) ;
}
VhdlGuardedSignalSpec::VhdlGuardedSignalSpec(const VhdlGuardedSignalSpec &node, VhdlMapForCopy &old2new)
  : VhdlSpecification(node, old2new),
    _signal_list(0),
    _type_mark(0)
{
    _signal_list = old2new.CopyNameArray(node._signal_list) ;
    _type_mark = node._type_mark ? node._type_mark->CopyName(old2new) : 0 ;
}
VhdlEntitySpec::VhdlEntitySpec(const VhdlEntitySpec &node, VhdlMapForCopy &old2new)
  : VhdlSpecification(node, old2new),
    _entity_name_list(0),
    _entity_class(node._entity_class),
    _all_ids(0)
{
    _entity_name_list = old2new.CopyNameArray(node._entity_name_list) ;
    if (node._all_ids) {
        _all_ids = new Set(POINTER_HASH, node._all_ids->Size()) ;
        SetIter si ;
        VhdlIdDef *id ;
        FOREACH_SET_ITEM(node._all_ids, si, &id) {
            VhdlIdDef *new_id = old2new.GetCopiedId(id) ;
            (void) _all_ids->Insert(new_id) ;
        }
    }
}

// Copy the statements
VhdlStatement::VhdlStatement(const VhdlStatement &node, VhdlMapForCopy &old2new)
  : VhdlTreeNode(node, old2new),
    _label(0),
    _postponed(node._postponed)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{
    // Copy labels here and set this statement as a back pointer
    // in label id
    _label = node._label ? node._label->CopyIdDef(old2new) : 0 ;
    if (_label != node._label) old2new.SetCopiedId(node._label, _label) ;
    if (_label) _label->SetStatement(this) ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    if (node._closing_label) _closing_label = node._closing_label->CopyDesignator(old2new) ;
#endif
}
VhdlNullStatement::VhdlNullStatement(const VhdlNullStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new)
{
}
VhdlReturnStatement::VhdlReturnStatement(const VhdlReturnStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _expr(0),
    _owner(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    _owner = old2new.GetCopiedId(node._owner) ;
}
VhdlExitStatement::VhdlExitStatement(const VhdlExitStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _condition(0),
    _target_label(0)
{
    _target = node._target ? node._target->CopyName(old2new) : 0 ;
    _condition = node._condition ? node._condition->CopyExpression(old2new) : 0 ;
    _target_label = old2new.GetCopiedId(node._target_label) ;
}
VhdlNextStatement::VhdlNextStatement(const VhdlNextStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _condition(0),
    _target_label(0)
{
    _target = node._target ? node._target->CopyName(old2new) : 0 ;
    _condition = node._condition ? node._condition->CopyExpression(old2new) : 0 ;
    _target_label = old2new.GetCopiedId(node._target_label) ;
}
VhdlLoopStatement::VhdlLoopStatement(const VhdlLoopStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _iter_scheme(0),
    _statements(0),
    _local_scope(0)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _iter_scheme = node._iter_scheme ? node._iter_scheme->CopyIterScheme(old2new) : 0 ;
    _statements = old2new.CopyStatementArray(node._statements) ;
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
}
VhdlCaseStatement::VhdlCaseStatement(const VhdlCaseStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _expr(0),
    _alternatives(0),
    _is_matching_case(node._is_matching_case),
    _expr_type(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    if (node._alternatives) {
        _alternatives = new Array(node._alternatives->Size()) ;
        unsigned i ;
        VhdlCaseStatementAlternative *ele ;
        FOREACH_ARRAY_ITEM(node._alternatives, i, ele) {
            VhdlCaseStatementAlternative *new_ele = ele ? ele->Copy(old2new) : 0 ;
            _alternatives->InsertLast(new_ele) ;
        }
    }
    _expr_type = old2new.GetCopiedId(node._expr_type) ;
}
VhdlIfStatement::VhdlIfStatement(const VhdlIfStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _if_cond(0),
    _if_statements(0),
    _elsif_list(0),
    _else_statements(0)
{
    _if_cond = node._if_cond ? node._if_cond->CopyExpression(old2new) : 0 ;
    _if_statements = old2new.CopyStatementArray(node._if_statements) ;
    if (node._elsif_list) {
        _elsif_list = new Array(node._elsif_list->Size()) ;
        unsigned i ;
        VhdlElsif *else_if ;
        FOREACH_ARRAY_ITEM(node._elsif_list, i, else_if) {
            VhdlElsif *new_else_if = else_if ? else_if->Copy(old2new) : 0 ;
            _elsif_list->InsertLast(new_else_if) ;
        }
    }
    _else_statements = old2new.CopyStatementArray(node._else_statements) ;
}
VhdlVariableAssignmentStatement::VhdlVariableAssignmentStatement(const VhdlVariableAssignmentStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _value(0)
{
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    _value = node._value ? node._value->CopyExpression(old2new) : 0 ;
}
VhdlSignalAssignmentStatement::VhdlSignalAssignmentStatement(const VhdlSignalAssignmentStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _delay_mechanism(0),
    _waveform(0)
{
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    _delay_mechanism = node._delay_mechanism ? node._delay_mechanism->CopyDelayMechanism(old2new) : 0 ;
    _waveform = old2new.CopyExpressionArray(node._waveform) ;
}
VhdlForceAssignmentStatement::VhdlForceAssignmentStatement(const VhdlForceAssignmentStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _force_mode(node._force_mode),
    _expr(0)
{
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
}
VhdlReleaseAssignmentStatement::VhdlReleaseAssignmentStatement(const VhdlReleaseAssignmentStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _force_mode(node._force_mode)
{
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
}
VhdlWaitStatement::VhdlWaitStatement(const VhdlWaitStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _sensitivity_clause(0),
    _condition_clause(0),
    _timeout_clause(0)
{
    _sensitivity_clause = old2new.CopyNameArray(node._sensitivity_clause) ;
    _condition_clause = node._condition_clause ? node._condition_clause->CopyExpression(old2new) : 0 ;
    _timeout_clause = node._timeout_clause ? node._timeout_clause->CopyExpression(old2new) : 0 ;
}
VhdlReportStatement::VhdlReportStatement(const VhdlReportStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _report(0),
    _severity(0)
{
    _report = node._report ? node._report->CopyExpression(old2new) : 0 ;
    _severity = node._severity ? node._severity->CopyExpression(old2new) : 0 ;
}
VhdlAssertionStatement::VhdlAssertionStatement(const VhdlAssertionStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _condition(0),
    _report(0),
    _severity(0)
{
    _condition = node._condition ? node._condition->CopyExpression(old2new) : 0 ;
    _report = node._report ? node._report->CopyExpression(old2new) : 0 ;
    _severity = node._severity ? node._severity->CopyExpression(old2new) : 0 ;
}
VhdlProcessStatement::VhdlProcessStatement(const VhdlProcessStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _sensitivity_list(0),
    _decl_part(0),
    _statement_part(0),
    _local_scope(0)
{
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _sensitivity_list = old2new.CopyNameArray(node._sensitivity_list) ;
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;
    _statement_part = old2new.CopyStatementArray(node._statement_part) ;
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}
VhdlBlockStatement::VhdlBlockStatement(const VhdlBlockStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _guard(0),
    _generics(0),
    _ports(0),
    _decl_part(0),
    _statements(0),
    _local_scope(0),
    _guard_id(0)
    ,_is_static_elaborated(0)
    ,_is_elab_created_empty(node._is_elab_created_empty)
    ,_is_gen_elab_created(node._is_gen_elab_created)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _guard = node._guard ? node._guard->CopyExpression(old2new) : 0 ;
    _generics = node._generics ? node._generics->Copy(old2new) : 0 ;
    _ports = node._ports ? node._ports->Copy(old2new) : 0 ;
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;
    _statements = old2new.CopyStatementArray(node._statements) ;
    // VIPER #3713  : Guard identifier is implicitly declared signal 'guard'. So
    // while copying we should copy it first and declare it in new block scope
    _guard_id = (node._guard_id) ? node._guard_id->CopyIdDef(old2new) : 0 ;
    old2new.SetCopiedId(node._guard_id, _guard_id) ;
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}
VhdlGenerateStatement::VhdlGenerateStatement(const VhdlGenerateStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _scheme(0),
    _decl_part(0),
    _statement_part(0),
    _local_scope(0)
{
    old2new.CopyPredefinedOperators(node._local_scope) ;
    _local_scope = node._local_scope ? node._local_scope->Copy(old2new) : 0 ;
    _scheme = node._scheme ? node._scheme->CopyIterScheme(old2new) : 0 ;
    _decl_part = old2new.CopyDeclarationArray(node._decl_part) ;
    _statement_part = old2new.CopyStatementArray(node._statement_part) ;
    if (_local_scope) _local_scope->Update(node._local_scope, old2new) ;
    // Set backpointers to attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
}
VhdlComponentInstantiationStatement::VhdlComponentInstantiationStatement(const VhdlComponentInstantiationStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _instantiated_unit(0),
    _generic_map_aspect(0),
    _port_map_aspect(0),
    _unit(0)
    // VIPER #6467 : Elaboration specific
    ,_library_name(0) // name of instantiated unit's library
    ,_instantiated_unit_name(0) // name of instantiated entity
    ,_arch_name(0) // name of instantiated architecture
{
    _instantiated_unit = node._instantiated_unit ? node._instantiated_unit->CopyName(old2new) : 0 ;
    unsigned is_entity_instance = (node._unit && node._unit->IsComponent()) ? 0: 1 ;
    // VIPER #4552 : If we are copying entity instantiation, do not change the reference of formal
    // identifiers in formal part of association. If we do this, formal part will point to copied
    // entity for entity copy containing recursive instance. If formal part is changed for recursive
    // instance, we cannot handle recursive instance properly.
    if (is_entity_instance) old2new.DonotSwitchIdInFormal() ;
    _generic_map_aspect = old2new.CopyDiscreteRangeArray(node._generic_map_aspect) ;
    _port_map_aspect = old2new.CopyDiscreteRangeArray(node._port_map_aspect) ;
    _unit = old2new.GetCopiedId(node._unit) ;
    if (is_entity_instance) old2new.ResetDonotSwitchIdInFormal() ;
    // VIPER #6467 : Elaboration specific
    _library_name = Strings::save(node._library_name) ; // name of instantiated unit's library
    _instantiated_unit_name = Strings::save(node._instantiated_unit_name) ; // name of instantiated entity
    _arch_name = Strings::save(node._arch_name) ; // name of instantiated architecture
}
VhdlProcedureCallStatement::VhdlProcedureCallStatement(const VhdlProcedureCallStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _call(0)
{
    _call = node._call ? node._call->CopyName(old2new) : 0 ;
}
VhdlConditionalSignalAssignment::VhdlConditionalSignalAssignment(const VhdlConditionalSignalAssignment &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _options(0),
    _delay_mechanism(0),
    _conditional_waveforms(0),
    _guard_id(0)
{
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    _options = node._options ? node._options->Copy(old2new) : 0 ;
    _delay_mechanism = node._delay_mechanism ? node._delay_mechanism->CopyDelayMechanism(old2new) : 0 ;
    if (node._conditional_waveforms) {
        _conditional_waveforms = new Array(node._conditional_waveforms->Size()) ;
        unsigned i ;
        VhdlConditionalWaveform *c_wave_form ;
        FOREACH_ARRAY_ITEM(node._conditional_waveforms, i, c_wave_form) {
            VhdlConditionalWaveform *new_wave_form = c_wave_form ? c_wave_form->Copy(old2new) : 0 ;
            _conditional_waveforms->Insert(new_wave_form) ;
        }
    }
    _guard_id = old2new.GetCopiedId(node._guard_id) ;
}
VhdlConditionalForceAssignmentStatement::VhdlConditionalForceAssignmentStatement(const VhdlConditionalForceAssignmentStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _force_mode(node._force_mode),
    _conditional_expressions(0)
{
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    if (node._conditional_expressions) {
        _conditional_expressions = new Array(node._conditional_expressions->Size()) ;
        unsigned i ;
        VhdlConditionalExpression *c_expr ;
        FOREACH_ARRAY_ITEM(node._conditional_expressions, i, c_expr) {
            VhdlConditionalExpression *new_expr = c_expr ? c_expr->Copy(old2new) : 0 ;
            _conditional_expressions->Insert(new_expr) ;
        }
    }
}
VhdlSelectedSignalAssignment::VhdlSelectedSignalAssignment(const VhdlSelectedSignalAssignment &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _expr(0),
    _target(0),
    _options(0),
    _delay_mechanism(0),
    _selected_waveforms(0),
    _is_matching(node._is_matching),
    _expr_type(0),
    _guard_id(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    _options = node._options ? node._options->Copy(old2new) : 0 ;
    _delay_mechanism = node._delay_mechanism ? node._delay_mechanism->CopyDelayMechanism(old2new) : 0 ;
    if (node._selected_waveforms) {
        _selected_waveforms = new Array(node._selected_waveforms->Size()) ;
        unsigned i ;
        VhdlSelectedWaveform *s_wave_form ;
        FOREACH_ARRAY_ITEM(node._selected_waveforms, i, s_wave_form) {
            VhdlSelectedWaveform *new_s_form = s_wave_form ? s_wave_form->Copy(old2new) : 0 ;
            _selected_waveforms->InsertLast(new_s_form) ;
        }
    }
    _expr_type = old2new.GetCopiedId(node._expr_type) ;
    _guard_id = old2new.GetCopiedId(node._guard_id) ;
}
VhdlIfElsifGenerateStatement::VhdlIfElsifGenerateStatement(const VhdlIfElsifGenerateStatement &node, VhdlMapForCopy &old2new)
  : VhdlGenerateStatement(node, old2new),
    _elsif_list(0),
    _else_stmt(0)
{
    _elsif_list = old2new.CopyStatementArray(node._elsif_list) ;
    _else_stmt = node._else_stmt ? node._else_stmt->CopyStatement(old2new): 0 ;
}
VhdlCaseGenerateStatement::VhdlCaseGenerateStatement(const VhdlCaseGenerateStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _expr(0),
    _alternatives(0),
    _expr_type(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    _alternatives = old2new.CopyStatementArray(node._alternatives) ;
    _expr_type = old2new.GetCopiedId(node._expr_type) ;
}
VhdlSelectedVariableAssignmentStatement::VhdlSelectedVariableAssignmentStatement(const VhdlSelectedVariableAssignmentStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _expr(0),
    _target(0),
    _selected_exprs(0),
    _is_matching(node._is_matching),
    _expr_type(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    if (node._selected_exprs) {
        _selected_exprs = new Array(node._selected_exprs->Size()) ;
        unsigned i ;
        VhdlSelectedExpression *s_expr ;
        FOREACH_ARRAY_ITEM(node._selected_exprs, i, s_expr) {
            VhdlSelectedExpression *new_s_expr = s_expr ? s_expr->Copy(old2new) : 0 ;
            _selected_exprs->InsertLast(new_s_expr) ;
        }
    }
    _expr_type = old2new.GetCopiedId(node._expr_type) ;
}
VhdlSelectedForceAssignment::VhdlSelectedForceAssignment(const VhdlSelectedForceAssignment &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _expr(0),
    _target(0),
    _is_matching(node._is_matching),
    _force_mode(node._force_mode),
    _selected_exprs(0),
    _expr_type(0)
{
    _expr = node._expr ? node._expr->CopyExpression(old2new) : 0 ;
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    if (node._selected_exprs) {
        _selected_exprs = new Array(node._selected_exprs->Size()) ;
        unsigned i ;
        VhdlSelectedExpression *s_expr ;
        FOREACH_ARRAY_ITEM(node._selected_exprs, i, s_expr) {
            VhdlSelectedExpression *new_s_expr = s_expr ? s_expr->Copy(old2new) : 0 ;
            _selected_exprs->InsertLast(new_s_expr) ;
        }
    }
    _expr_type = old2new.GetCopiedId(node._expr_type) ;
}
VhdlConditionalVariableAssignmentStatement::VhdlConditionalVariableAssignmentStatement(const VhdlConditionalVariableAssignmentStatement &node, VhdlMapForCopy &old2new)
  : VhdlStatement(node, old2new),
    _target(0),
    _conditional_expressions(0)
{
    _target = node._target ? node._target->CopyExpression(old2new) : 0 ;
    if (node._conditional_expressions) {
        _conditional_expressions = new Array(node._conditional_expressions->Size()) ;
        unsigned i ;
        VhdlConditionalExpression *c_expr ;
        FOREACH_ARRAY_ITEM(node._conditional_expressions, i, c_expr) {
            VhdlConditionalExpression *new_expr = c_expr ? c_expr->Copy(old2new) : 0 ;
            _conditional_expressions->Insert(new_expr) ;
        }
    }
}

////////////////  Copy routines ////////////////////////
VhdlScope *VhdlScope::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlScope((*this), old2new) ;
}
// Copy primary units with first argument specific name. If the name is
// null, the name of the copied unit remains same.

VhdlPrimaryUnit *
VhdlPrimaryUnit::CopyPrimaryUnit(const char * /*new_unit_name*/, VhdlMapForCopy &/*old2new*/, unsigned /*exclude_archs*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlPrimaryUnit *
VhdlEntityDecl::CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const
{
    if (!new_unit_name && _id) new_unit_name = _id->OrigName() ;
    return new VhdlEntityDecl(new_unit_name, (*this), old2new, exclude_archs) ;
}
VhdlPrimaryUnit *
VhdlConfigurationDecl::CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned /*exclude_archs*/) const
{
    if (!new_unit_name && _id) new_unit_name = _id->OrigName() ;
    return new VhdlConfigurationDecl(new_unit_name, (*this), old2new) ;
}
VhdlPrimaryUnit *
VhdlPackageDecl::CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const
{
    if (!new_unit_name && _id) new_unit_name = _id->OrigName() ;
    return new VhdlPackageDecl(new_unit_name, (*this), old2new, exclude_archs) ;
}

VhdlPrimaryUnit *
VhdlContextDecl::CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned /*exclude_archs*/) const
{
    if (!new_unit_name && _id) new_unit_name = _id->OrigName() ;
    return new VhdlContextDecl(new_unit_name, (*this), old2new) ;
}
VhdlPrimaryUnit *
VhdlPackageInstantiationDecl::CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned /*exclude_archs*/) const
{
    if (!new_unit_name && _id) new_unit_name = _id->OrigName() ;
    return new VhdlPackageInstantiationDecl(new_unit_name, (*this), old2new) ;
}
VhdlPackageInstantiationDecl*
VhdlPackageInstantiationDecl::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlPackageInstantiationDecl(_id ? _id->OrigName(): 0, (*this), old2new) ;
}
// Copy secondary units with first argument specific name. If the name is
// null, the name of the copied unit remains same.
VhdlSecondaryUnit *
VhdlSecondaryUnit::CopySecondaryUnit(const char * /*new_unit_name*/, VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlSecondaryUnit *
VhdlArchitectureBody::CopySecondaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new) const
{
    if (!new_unit_name && _id) new_unit_name = _id->OrigName() ;
    return new VhdlArchitectureBody(new_unit_name, (*this), old2new) ;
}
VhdlSecondaryUnit *
VhdlPackageBody::CopySecondaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new) const
{
    if (!new_unit_name && _id) new_unit_name = _id->OrigName() ;
    return new VhdlPackageBody(new_unit_name, (*this), old2new) ;
}
VhdlPrimaryUnit *
VhdlEntityDecl::CopyEntityArchitecture(const char* new_entity_name,  const char *arch_to_copy, VhdlMapForCopy &old2new) const
{
    VhdlSecondaryUnit *sec_unit = GetSecondaryUnit(arch_to_copy) ;
    if (!sec_unit && !_is_verilog_module) return 0 ;
    if (!new_entity_name) new_entity_name = _id ? _id->OrigName(): Name() ;
    VhdlPrimaryUnit *new_prim_unit = CopyPrimaryUnit(new_entity_name, old2new, 1) ;
    VhdlSecondaryUnit *new_sec_unit = sec_unit ? sec_unit->CopySecondaryUnit(0, old2new) : 0 ;

    // VIPER #2998 : Create a VhdlMapForCopy containing old entity id vs new
    // entity id and use that to set entity reference in attribute specification
    // to copied entity identifier.
    VhdlMapForCopy tmpold2new ;
    if (new_prim_unit) tmpold2new.SetCopiedId(_id, new_prim_unit->Id()) ;

    if (new_prim_unit && new_sec_unit && new_sec_unit->Id()) {
        new_sec_unit->ChangeEntityReference(new_prim_unit->Id()) ;
        // VIPER #2998 : Attribute specification of architecture can have entity
        // references. We have copied architecture after removing entity id
        // from 'old2new', so any reference to entity is pointing to original
        // entity not the copied one. But in attribute specification entity
        // reference should point to copied entity. Do that now :
        unsigned i ;
        VhdlDeclaration *decl ;
        Array *decls = new_sec_unit->GetDeclPart() ;
        FOREACH_ARRAY_ITEM(decls, i, decl) { // Iterate all declarations
            if (decl) decl->UpdateDecl(tmpold2new) ; // Set entity references
        }
        new_sec_unit->Id()->DeclareArchitectureId(new_prim_unit->Id()) ;
        (void) new_prim_unit->AddSecondaryUnit(new_sec_unit) ;
    }
    return new_prim_unit ;
}

// Copy configuration item
VhdlConfigurationItem *
VhdlConfigurationItem::CopyConfigurationItem(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlConfigurationItem *
VhdlBlockConfiguration::CopyConfigurationItem(VhdlMapForCopy &old2new) const
{
    return new VhdlBlockConfiguration((*this), old2new) ;
}
VhdlConfigurationItem *
VhdlComponentConfiguration::CopyConfigurationItem(VhdlMapForCopy &old2new) const
{
    return new VhdlComponentConfiguration((*this), old2new) ;
}

// Copy type
VhdlTypeDef *
VhdlTypeDef::CopyTypeDef(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
// copy scalar type
VhdlTypeDef *
VhdlScalarTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlScalarTypeDef((*this), old2new) ;
}
// copy array type
VhdlTypeDef *
VhdlArrayTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlArrayTypeDef((*this), old2new) ;
}
// copy record type
VhdlTypeDef *
VhdlRecordTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlRecordTypeDef((*this), old2new) ;
}
// copy protected type
VhdlTypeDef *
VhdlProtectedTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlProtectedTypeDef((*this), old2new) ;
}
VhdlTypeDef *
VhdlProtectedTypeDefBody::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlProtectedTypeDefBody((*this), old2new) ;
}
// copy access type
VhdlTypeDef *
VhdlAccessTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlAccessTypeDef((*this), old2new) ;
}
// copy file type
VhdlTypeDef *
VhdlFileTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlFileTypeDef((*this), old2new) ;
}
// copy enumeration type
VhdlTypeDef *
VhdlEnumerationTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlEnumerationTypeDef((*this), old2new) ;
}
// copy physical type
VhdlTypeDef *
VhdlPhysicalTypeDef::CopyTypeDef(VhdlMapForCopy &old2new) const
{
    return new VhdlPhysicalTypeDef((*this), old2new) ;
}

///////////////// Copy declarations ///////////////////

// copy element declaration
VhdlElementDecl *
VhdlElementDecl::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlElementDecl((*this), old2new) ;
}
// copy physical unit declaration
VhdlPhysicalUnitDecl *
VhdlPhysicalUnitDecl::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlPhysicalUnitDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlDeclaration::CopyDeclaration(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlDeclaration *
VhdlUseClause::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlUseClause((*this), old2new) ;
}
VhdlDeclaration *
VhdlContextReference::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlContextReference((*this), old2new) ;
}
VhdlDeclaration *
VhdlLibraryClause::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlLibraryClause((*this), old2new) ;
}
VhdlDeclaration *
VhdlInterfaceDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlInterfaceDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlSubprogramDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlSubprogramDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlSubprogramBody::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlSubprogramBody((*this), old2new) ;
}
VhdlDeclaration *
VhdlSubtypeDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlSubtypeDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlFullTypeDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlFullTypeDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlIncompleteTypeDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlIncompleteTypeDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlConstantDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlConstantDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlSignalDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlSignalDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlVariableDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlVariableDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlFileDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlFileDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlAliasDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlAliasDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlComponentDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlComponentDecl((*this), old2new) ;
}
// VIPER 2375 : Copy component decl with a new name so that name of existing component instantiation can be changed
VhdlComponentDecl *
VhdlComponentDecl::CopyWithName(const char *new_name, VhdlMapForCopy &old2new) const
{
    if (!new_name || !_id) return 0 ; // No name specified, return null

    // Create new component id with new name
    VhdlComponentId *new_id = new VhdlComponentId(Strings::save(new_name)) ;
    new_id->SetLinefile(_id->Linefile()) ;
    old2new.SetCopiedId(_id, new_id) ; // Set new id in map for copy

    // Now create new component declaration, with other fields null
    VhdlComponentDecl *new_decl = new VhdlComponentDecl(new_id, 0, 0, 0) ;
    new_decl->SetLinefile(Linefile()) ;
    new_decl->CopyFrom(*this, old2new) ; // Call private method to copy other fields of 'new_decl' from 'this'
    return new_decl ;
}
VhdlDeclaration *
VhdlAttributeDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlAttributeDecl((*this), old2new) ;
}

VhdlDeclaration *
VhdlAttributeSpec::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlAttributeSpec((*this), old2new) ;
}
VhdlDeclaration *
VhdlConfigurationSpec::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlConfigurationSpec((*this), old2new) ;
}
VhdlDeclaration *
VhdlDisconnectionSpec::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlDisconnectionSpec((*this), old2new) ;
}
VhdlDeclaration *
VhdlGroupTemplateDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlGroupTemplateDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlGroupDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlGroupDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlSubprogInstantiationDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlSubprogInstantiationDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlInterfaceTypeDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlInterfaceTypeDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlInterfaceSubprogDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlInterfaceSubprogDecl((*this), old2new) ;
}
VhdlDeclaration *
VhdlInterfacePackageDecl::CopyDeclaration(VhdlMapForCopy &old2new) const
{
    return new VhdlInterfacePackageDecl((*this), old2new) ;
}
///////////////         Copy expression        //////////////////////////
VhdlExpression *
VhdlExpression::CopyExpression(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlDiscreteRange *
VhdlExpression::CopyDiscreteRange(VhdlMapForCopy &old2new) const
{
    return CopyExpression(old2new) ;
}
VhdlDiscreteRange *
VhdlDiscreteRange::CopyDiscreteRange(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlSubtypeIndication *
VhdlSubtypeIndication::CopySubtypeIndication(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlExpression *
VhdlSubtypeIndication::CopyExpression(VhdlMapForCopy &old2new) const
{
    return CopySubtypeIndication(old2new) ;
}
VhdlSubtypeIndication *
VhdlExplicitSubtypeIndication::CopySubtypeIndication(VhdlMapForCopy &old2new) const
{
    return new VhdlExplicitSubtypeIndication((*this), old2new) ;
}
VhdlDiscreteRange *
VhdlRange::CopyDiscreteRange(VhdlMapForCopy &old2new) const
{
    return new VhdlRange((*this), old2new) ;
}
VhdlExpression *
VhdlBox::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlBox((*this), old2new) ;
}
VhdlExpression *
VhdlAssocElement::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlAssocElement((*this), old2new) ;
}
VhdlExpression *
VhdlInertialElement::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlInertialElement((*this), old2new) ;
}
VhdlExpression *
VhdlRecResFunctionElement::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlRecResFunctionElement((*this), old2new) ;
}
VhdlExpression *
VhdlOperator::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlOperator((*this), old2new) ;
}
VhdlExpression *
VhdlAllocator::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlAllocator((*this), old2new) ;
}
VhdlExpression *
VhdlAggregate::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlAggregate((*this), old2new) ;
}
VhdlExpression *
VhdlQualifiedExpression::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlQualifiedExpression((*this), old2new) ;
}
VhdlExpression *
VhdlElementAssoc::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlElementAssoc((*this), old2new) ;
}
VhdlExpression *
VhdlWaveformElement::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlWaveformElement((*this), old2new) ;
}
VhdlExpression *
VhdlDefault::CopyExpression(VhdlMapForCopy &old2new) const
{
    return new VhdlDefault((*this), old2new) ;
}
///////////////// Copy identifiers ///////////////////////////
VhdlIdDef *
VhdlIdDef::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlIdDef((*this), old2new) ;
}
VhdlIdDef *
VhdlLibraryId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlLibraryId((*this), old2new) ;
}
VhdlIdDef *
VhdlGroupId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlGroupId((*this), old2new) ;
}
VhdlIdDef *
VhdlGroupTemplateId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlGroupTemplateId((*this), old2new) ;
}
VhdlIdDef *
VhdlAttributeId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlAttributeId((*this), old2new) ;
}
VhdlIdDef *
VhdlComponentId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlComponentId((*this), old2new) ;
}
VhdlIdDef *
VhdlAliasId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlAliasId((*this), old2new) ;
}
VhdlIdDef *
VhdlFileId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlFileId((*this), old2new) ;
}
VhdlIdDef *
VhdlVariableId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlVariableId((*this), old2new) ;
}
VhdlIdDef *
VhdlSignalId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlSignalId((*this), old2new) ;
}
VhdlIdDef *
VhdlConstantId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlConstantId((*this), old2new) ;
}
VhdlIdDef *
VhdlTypeId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlTypeId((*this), old2new) ;
}
VhdlIdDef *
VhdlGenericTypeId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlGenericTypeId((*this), old2new) ;
}
VhdlIdDef *
VhdlUniversalInteger::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlUniversalInteger((*this), old2new) ;
}
VhdlIdDef *
VhdlUniversalReal::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlUniversalReal((*this), old2new) ;
}
VhdlIdDef *
VhdlAnonymousType::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlAnonymousType((*this), old2new) ;
}
VhdlIdDef *
VhdlSubtypeId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlSubtypeId((*this), old2new) ;
}
VhdlIdDef *
VhdlSubprogramId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlSubprogramId((*this), old2new) ;
}
VhdlIdDef *
VhdlOperatorId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlOperatorId((*this), old2new) ;
}
VhdlIdDef *
VhdlInterfaceId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlInterfaceId((*this), old2new) ;
}
VhdlIdDef *
VhdlEnumerationId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlEnumerationId((*this), old2new) ;
}
VhdlIdDef *
VhdlElementId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlElementId((*this), old2new) ;
}
VhdlIdDef *
VhdlPhysicalUnitId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlPhysicalUnitId((*this), old2new) ;
}
VhdlIdDef *
VhdlEntityId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlEntityId((*this), old2new) ;
}
VhdlIdDef *
VhdlArchitectureId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlArchitectureId((*this), old2new) ;
}
VhdlIdDef *
VhdlConfigurationId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlConfigurationId((*this), old2new) ;
}
VhdlIdDef *
VhdlPackageId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlPackageId((*this), old2new) ;
}
VhdlIdDef *
VhdlPackageBodyId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlPackageBodyId((*this), old2new) ;
}
VhdlIdDef *
VhdlContextId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlContextId((*this), old2new) ;
}
VhdlIdDef *
VhdlLabelId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlLabelId((*this), old2new) ;
}
VhdlIdDef *
VhdlBlockId::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlBlockId((*this), old2new) ;
}
VhdlIdDef *
VhdlPackageInstElement::CopyIdDef(VhdlMapForCopy &old2new) const
{
    return new VhdlPackageInstElement((*this), old2new) ;
}
VhdlIdDef *
VhdlProtectedTypeId::CopyIdDef(VhdlMapForCopy &old2new) const // Viper #7037
{
    return new VhdlProtectedTypeId((*this), old2new) ;
}
VhdlIdDef *
VhdlProtectedTypeBodyId::CopyIdDef(VhdlMapForCopy &old2new) const // Viper #7037
{
    return new VhdlProtectedTypeBodyId((*this), old2new) ;
}
VhdlSignature *
VhdlSignature::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlSignature((*this), old2new) ;
}
VhdlFileOpenInfo *
VhdlFileOpenInfo::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlFileOpenInfo((*this), old2new) ;
}
VhdlBindingIndication *
VhdlBindingIndication::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlBindingIndication((*this), old2new) ;
}
/////////////// Copy iteration schemes  ///////////////////////////
VhdlIterScheme *
VhdlIterScheme::CopyIterScheme(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlIterScheme *
VhdlWhileScheme::CopyIterScheme(VhdlMapForCopy &old2new) const
{
    return new VhdlWhileScheme((*this), old2new) ;
}
VhdlIterScheme *
VhdlForScheme::CopyIterScheme(VhdlMapForCopy &old2new) const
{
    return new VhdlForScheme((*this), old2new) ;
}
VhdlIterScheme *
VhdlIfScheme::CopyIterScheme(VhdlMapForCopy &old2new) const
{
    return new VhdlIfScheme((*this), old2new) ;
}
VhdlIterScheme *
VhdlElsifElseScheme::CopyIterScheme(VhdlMapForCopy &old2new) const
{
    return new VhdlElsifElseScheme((*this), old2new) ;
}
VhdlIterScheme *
VhdlCaseItemScheme::CopyIterScheme(VhdlMapForCopy &old2new) const
{
    return new VhdlCaseItemScheme((*this), old2new) ;
}
//////////////    Copy delay mechanism      ///////////////////////////
VhdlDelayMechanism *
VhdlDelayMechanism::CopyDelayMechanism(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlDelayMechanism *
VhdlTransport::CopyDelayMechanism(VhdlMapForCopy &old2new) const
{
    return new VhdlTransport((*this), old2new) ;
}
VhdlDelayMechanism *
VhdlInertialDelay::CopyDelayMechanism(VhdlMapForCopy &old2new) const
{
    return new VhdlInertialDelay((*this), old2new) ;
}
// Copy options
VhdlOptions *
VhdlOptions::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlOptions((*this), old2new) ;
}
// copy wave forms
VhdlConditionalWaveform *
VhdlConditionalWaveform::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlConditionalWaveform((*this), old2new) ;
}
VhdlSelectedWaveform *
VhdlSelectedWaveform::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlSelectedWaveform((*this), old2new) ;
}
VhdlConditionalExpression *
VhdlConditionalExpression::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlConditionalExpression((*this), old2new) ;
}
VhdlSelectedExpression *
VhdlSelectedExpression::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlSelectedExpression((*this), old2new) ;
}
VhdlBlockGenerics *
VhdlBlockGenerics::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlBlockGenerics((*this), old2new) ;
}
VhdlBlockPorts *
VhdlBlockPorts::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlBlockPorts((*this), old2new) ;
}
VhdlCaseStatementAlternative *
VhdlCaseStatementAlternative::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlCaseStatementAlternative((*this), old2new) ;
}
VhdlElsif *
VhdlElsif::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlElsif((*this), old2new) ;
}
VhdlEntityClassEntry *
VhdlEntityClassEntry::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlEntityClassEntry((*this), old2new) ;
}
VhdlSubtypeIndication *
VhdlName::CopySubtypeIndication(VhdlMapForCopy &old2new) const
{
    return CopyName(old2new) ;
}
//////// Copy name references, designator and literals ////////////////
VhdlName *
VhdlName::CopyName(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlDesignator *
VhdlDesignator::CopyDesignator(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlName *
VhdlDesignator::CopyName(VhdlMapForCopy &old2new) const
{
    return CopyDesignator(old2new) ;
}
VhdlName *
VhdlLiteral::CopyName(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlDesignator *
VhdlAll::CopyDesignator(VhdlMapForCopy &old2new) const
{
    return new VhdlAll((*this), old2new) ;
}
VhdlDesignator *
VhdlOthers::CopyDesignator(VhdlMapForCopy &old2new) const
{
    return new VhdlOthers((*this), old2new) ;
}
VhdlDesignator *
VhdlUnaffected::CopyDesignator(VhdlMapForCopy &old2new) const
{
    return new VhdlUnaffected((*this), old2new) ;
}
VhdlName *
VhdlInteger::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlInteger((*this), old2new) ;
}
VhdlName *
VhdlReal::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlReal((*this), old2new) ;
}
VhdlDesignator *
VhdlStringLiteral::CopyDesignator(VhdlMapForCopy &old2new) const
{
    return new VhdlStringLiteral((*this), old2new) ;
}
VhdlDesignator *
VhdlBitStringLiteral::CopyDesignator(VhdlMapForCopy &old2new) const
{
    return new VhdlBitStringLiteral((*this), old2new) ;
}
VhdlDesignator *
VhdlCharacterLiteral::CopyDesignator(VhdlMapForCopy &old2new) const
{
    return new VhdlCharacterLiteral((*this), old2new) ;
}
VhdlName *
VhdlPhysicalLiteral::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlPhysicalLiteral((*this), old2new) ;
}
VhdlName *
VhdlNull::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlNull((*this), old2new) ;
}
VhdlDesignator *
VhdlIdRef::CopyDesignator(VhdlMapForCopy &old2new) const
{
    return new VhdlIdRef((*this), old2new) ;
}
VhdlIdRef *
VhdlIdRef::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlIdRef((*this), old2new) ;
}
VhdlArrayResFunction *
VhdlArrayResFunction::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlArrayResFunction((*this), old2new) ;
}
VhdlName *
VhdlArrayResFunction::CopyName(VhdlMapForCopy &old2new) const
{
    return Copy(old2new) ;
}
VhdlRecordResFunction *
VhdlRecordResFunction::Copy(VhdlMapForCopy &old2new) const
{
    return new VhdlRecordResFunction((*this), old2new) ;
}
VhdlName *
VhdlRecordResFunction::CopyName(VhdlMapForCopy &old2new) const
{
    return Copy(old2new) ;
}
VhdlName *
VhdlSelectedName::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlSelectedName((*this), old2new) ;
}
VhdlName *
VhdlIndexedName::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlIndexedName((*this), old2new) ;
}
VhdlName *
VhdlSignaturedName::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlSignaturedName((*this), old2new) ;
}
VhdlName *
VhdlAttributeName::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlAttributeName((*this), old2new) ;
}
VhdlName *
VhdlOpen::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlOpen((*this), old2new) ;
}
VhdlName *
VhdlEntityAspect::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlEntityAspect((*this), old2new) ;
}
VhdlName *
VhdlInstantiatedUnit::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlInstantiatedUnit((*this), old2new) ;
}
VhdlName *
VhdlExternalName::CopyName(VhdlMapForCopy &old2new) const
{
    return new VhdlExternalName((*this), old2new) ;
}
//////////////// Copy specifications /////////////////////
VhdlSpecification *
VhdlSpecification::CopySpecification(VhdlMapForCopy &/*old2new*/, unsigned /*exclude_scope*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlSpecification *
VhdlProcedureSpec::CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope) const
{
    return new VhdlProcedureSpec((*this), exclude_scope, old2new) ;
}
VhdlSpecification *
VhdlFunctionSpec::CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope) const
{
    return new VhdlFunctionSpec((*this), exclude_scope, old2new) ;
}
VhdlSpecification *
VhdlComponentSpec::CopySpecification(VhdlMapForCopy &old2new, unsigned /*exclude_scope*/) const
{
    return new VhdlComponentSpec((*this), old2new) ;
}
VhdlSpecification *
VhdlGuardedSignalSpec::CopySpecification(VhdlMapForCopy &old2new, unsigned /*exclude_scope*/) const
{
    return new VhdlGuardedSignalSpec((*this), old2new) ;
}
VhdlSpecification *
VhdlEntitySpec::CopySpecification(VhdlMapForCopy &old2new, unsigned /*exclude_scope*/) const
{
    return new VhdlEntitySpec((*this), old2new) ;
}

/////////////// Copy concurrent and procedural statements //////////////////
VhdlStatement *
VhdlStatement::CopyStatement(VhdlMapForCopy &/*old2new*/) const
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
VhdlStatement *
VhdlNullStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlNullStatement((*this), old2new) ;
}
VhdlStatement *
VhdlReturnStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlReturnStatement((*this), old2new) ;
}
VhdlStatement *
VhdlExitStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlExitStatement((*this), old2new) ;
}
VhdlStatement *
VhdlNextStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlNextStatement((*this), old2new) ;
}
VhdlStatement *
VhdlLoopStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlLoopStatement((*this), old2new) ;
}
VhdlStatement *
VhdlCaseStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlCaseStatement((*this), old2new) ;
}
VhdlStatement *
VhdlIfStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlIfStatement((*this), old2new) ;
}
VhdlStatement *
VhdlVariableAssignmentStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlVariableAssignmentStatement((*this), old2new) ;
}
VhdlStatement *
VhdlSignalAssignmentStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlSignalAssignmentStatement((*this), old2new) ;
}
VhdlStatement *
VhdlForceAssignmentStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlForceAssignmentStatement((*this), old2new) ;
}
VhdlStatement *
VhdlReleaseAssignmentStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlReleaseAssignmentStatement((*this), old2new) ;
}
VhdlStatement *
VhdlWaitStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlWaitStatement((*this), old2new) ;
}
VhdlStatement *
VhdlReportStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlReportStatement((*this), old2new) ;
}
VhdlStatement *
VhdlAssertionStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlAssertionStatement((*this), old2new) ;
}
VhdlStatement *
VhdlProcessStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlProcessStatement((*this), old2new) ;
}
VhdlStatement *
VhdlBlockStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlBlockStatement((*this), old2new) ;
}
VhdlStatement *
VhdlGenerateStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlGenerateStatement((*this), old2new) ;
}
VhdlStatement *
VhdlComponentInstantiationStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlComponentInstantiationStatement((*this), old2new) ;
}
VhdlStatement *
VhdlProcedureCallStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlProcedureCallStatement((*this), old2new) ;
}
VhdlStatement *
VhdlConditionalSignalAssignment::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlConditionalSignalAssignment((*this), old2new) ;
}
VhdlStatement *
VhdlConditionalForceAssignmentStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlConditionalForceAssignmentStatement((*this), old2new) ;
}
VhdlStatement *
VhdlConditionalVariableAssignmentStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlConditionalVariableAssignmentStatement((*this), old2new) ;
}
VhdlStatement *
VhdlSelectedSignalAssignment::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlSelectedSignalAssignment((*this), old2new) ;
}
VhdlStatement *
VhdlIfElsifGenerateStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlIfElsifGenerateStatement((*this), old2new) ;
}
VhdlStatement *
VhdlCaseGenerateStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlCaseGenerateStatement((*this), old2new) ;
}
VhdlStatement *
VhdlSelectedVariableAssignmentStatement::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlSelectedVariableAssignmentStatement((*this), old2new) ;
}
VhdlStatement *
VhdlSelectedForceAssignment::CopyStatement(VhdlMapForCopy &old2new) const
{
    return new VhdlSelectedForceAssignment((*this), old2new) ;
}

// Populate the back pointers in type id from type declaration
void
VhdlTypeId::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    if (!old_type_id) return ;
    if (old_type_id->GetTypeDefList()) {
        _list = new Map(STRING_HASH, old_type_id->GetTypeDefList()->Size()) ;
        MapIter mi ;
        VhdlIdDef *id ;
        FOREACH_MAP_ITEM(old_type_id->GetTypeDefList(), mi, 0, &id) {
            VhdlIdDef *new_id = old2new.GetCopiedId(id) ;
            // VIPER #3307 : Force-insert, for array types, index types (ids) are
            // stored and two index type can be same
            if (new_id) (void) _list->Insert(new_id->Name(), new_id, 0, 1) ;
        }
    } else {
        _list = 0 ;
    }
    _scope = old2new.GetCopiedScope(old_type_id->LocalScope()) ;
}

// The following function is used to set type identifier in physical
// unit id and physical unit ids in type id
void
VhdlPhysicalTypeDef::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    VhdlIdDef *type_id = old2new.GetCopiedId(old_type_id) ;
    if (!type_id) return ;

    unsigned i ;
    VhdlPhysicalUnitDecl *elem ;
    verific_uint64 pos ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list,i,elem) {
        VhdlIdDef *id = elem->GetId() ;
        if (id) {
            pos = id->Position() ;
            id->DeclarePhysicalUnitId(type_id, pos) ;
        }
    }
    type_id->DeclareForCopy(old_type_id, old2new) ;
}

// The function is used to set type identifier in enumeration literal
// and enumeration literals in type identifiers
void
VhdlEnumerationTypeDef::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    VhdlIdDef *type_id = old2new.GetCopiedId(old_type_id) ;
    if (!type_id) return ;

    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list,i,id) {
        id->DeclareEnumerationId(type_id,i) ;
    }
    type_id->DeclareForCopy(old_type_id, old2new) ;
}
// VIPER 2158 : Overwrite this routine for VhdlRecordTypeDef to set copied type id
// as owner of record's local scope
void
VhdlRecordTypeDef::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    VhdlIdDef *type_id = old2new.GetCopiedId(old_type_id) ;
    if (!type_id) return ;
    if (_local_scope) _local_scope->SetOwner(type_id) ;
    type_id->DeclareForCopy(old_type_id, old2new) ;
}
void
VhdlProtectedTypeDef::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    VhdlIdDef *type_id = old2new.GetCopiedId(old_type_id) ;
    if (!type_id) return ;
    if (_local_scope) _local_scope->SetOwner(type_id) ;
    type_id->DeclareForCopy(old_type_id, old2new) ;
    // VIPER #7455 : 'Declare' is not needed, 'DeclareForCopy' is sufficient
    //Declare(type_id) ;
}
void
VhdlProtectedTypeDefBody::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    VhdlIdDef *type_id = old2new.GetCopiedId(old_type_id) ;
    if (!type_id) return ;
    if (_local_scope) _local_scope->SetOwner(type_id) ;

    // VIPER #7775 : Find the protected type header id and set back pointer for body there
    VhdlIdDef *prev_header_id = old_type_id ? old_type_id->GetProtectedTypeDefId(): 0 ;
    VhdlIdDef *header_id = old2new.GetCopiedId(prev_header_id) ;
    if (header_id) header_id->SetProtectedTypeBodyId(type_id) ;

    type_id->DeclareForCopy(old_type_id, old2new) ;
    // VIPER #7455 : 'Declare' is not needed, 'DeclareForCopy' is sufficient
    //Declare(type_id) ;
}
void
VhdlTypeDef::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    VhdlIdDef *type_id = old2new.GetCopiedId(old_type_id) ;
    if (!type_id) return ;
    type_id->DeclareForCopy(old_type_id, old2new) ;
}

VhdlCommentNode *
VhdlCommentNode::CopyComment() const
{
    // Create a temporary VhdlMapForCopy object:
    VhdlMapForCopy old2new ;
    // Call the copy constructor:
    return (new VhdlCommentNode(*this, old2new)) ;
}

VhdlCommentNode::VhdlCommentNode(const VhdlCommentNode &node, VhdlMapForCopy &old2new)
    : VhdlTreeNode(node, old2new),
      _str(0)
{
    // Just copy the comment string:
    _str = Strings::save(node._str) ;
}

// Attribute specification contains back pointers to identifiers declared
// after this attribute specification in design file. So after copying statements
// of any block/process/architecture etc. we need to reset those back pointers in
// entity specification
void VhdlAttributeSpec::UpdateDecl(VhdlMapForCopy &old2new)
{
    // Pass the new attr_id -> attr_value back-pointers into the entity spec :
    VhdlIdDef *attr_id = (_designator) ? _designator->GetId() : 0 ;
    VhdlExpression *attr_value = _value ;
    // Update the target identifiers :
    if (_entity_spec) _entity_spec->SetEntityIdsForCopy(old2new, attr_id, attr_value) ;
}
void VhdlEntitySpec::SetEntityIdsForCopy(VhdlMapForCopy &old2new, VhdlIdDef *attr_id, VhdlExpression *attr_value)
{
    if (!_all_ids) return ; // No back pointer
    // While copying the attribute specification we have already populated this
    // '_all_ids' set, but it can contain original identifiers instead of copied
    // one, as attribute specification refers to identifiers to be declared later.
    // So in this routine we will check whether '_all_ids' contains any identifier
    // which is copied and stored with original one in 'old2new' map. If so, we
    // will replace those identifiers with copied one
    SetIter si ;
    VhdlIdDef *old_id ;
    VhdlIdDef *new_id ;
    Set *new_all_ids = new Set(POINTER_HASH, _all_ids->Size()) ; // Create a new set to store copied ids
    Map name_vs_id(STRING_HASH_CASE_INSENSITIVE) ; // Map to store name of original id vs. new iddef
    FOREACH_SET_ITEM(_all_ids, si, &old_id) {
        if (!old_id) continue ;
        new_id = old2new.GetCopiedId(old_id) ; // Check whether 'old_id' is copied
        if (!new_id) new_id = old_id ;

        // If 'old_id' is copied and copied one is 'new_id', insert new one, otherwise
        // insert old one
        (void) new_all_ids->Insert(new_id) ;
        if (new_id != old_id) (void) name_vs_id.Insert(old_id->OrigName(), new_id) ; // Only consider changed iddefs

        // Set the attribute value (expressions) into this new id :
        (void) new_id->SetAttribute(attr_id,attr_value) ;
    }
    delete _all_ids ;
    _all_ids = new_all_ids ;

    // Iterate over _entity_name_list and change their id. Name and resolved id
    // of those name are still pointing to old ones, but they should point to copied ones
    unsigned i ;
    VhdlName *name ;
    FOREACH_ARRAY_ITEM(_entity_name_list, i, name) {
        if (!name) continue ;
        new_id = (VhdlIdDef*)name_vs_id.GetValue(name->Name()) ;
        if (new_id) {
            name->SetId(new_id) ;
            // VIPER #3192 : Set name along with id, we print char *name in pretty_print
            name->SetName(Strings::save(new_id->OrigName())) ;
        } else {
            // VIPER #3192 : Check whether char *_name field of 'name' is different
            // from name->GetId()->Name()
            VhdlIdDef *id = name->GetId() ;
            if (id && !Strings::compare_nocase(id->OrigName(), name->Name())) {
                name->SetName(Strings::save(id->OrigName())) ;
            }
        }
    }
}
// Type identifier owned by incomplete type definition does not reside in scope,
// but it has a back pointer to full type definition. As incomplete type appears
// before full type definition in design, incomplete type identifier is copied
// before the identifier of its full type. So its back pointer cannot be updated
// while copying. So do it after copying the declaration part of design unit.
void VhdlIncompleteTypeDecl::UpdateDecl(VhdlMapForCopy &old2new)
{
    if (_id) _id->UpdateType(old2new) ;
}
void VhdlIdDef::UpdateType(VhdlMapForCopy & /*old2new*/) {}
void VhdlTypeId::UpdateType(VhdlMapForCopy &old2new)
{
    if (!_type) return ; // Nothing to update
    VhdlIdDef *new_type = old2new.GetCopiedId(_type) ;
    if (new_type && new_type != _type) _type = new_type ;
}

// Change the reference of entity in architecture
void
VhdlArchitectureBody::ChangeEntityReference(VhdlIdDef *entity_id)
{
    if (_entity_name && !_entity_name->GetId() && entity_id) {
        _entity_name->SetName(Strings::save(entity_id->OrigName())) ;
    }
}

unsigned
vhdl_file::Copy(const char *unit_name, const char *from_lib_name, const char *to_name, unsigned save_restore)
{
    // If from_lib_name is not set, copy from library "work"
    if (!from_lib_name) from_lib_name = "work" ;

    // Clear some static stuff :
    Message::ClearErrorCount() ;

    VhdlLibrary *from_lib = GetLibrary(from_lib_name, 1) ;

    VhdlMapForCopy old2new ;

    if(unit_name) {
        // Get the unit :
        VhdlPrimaryUnit *unit = _work_lib->GetPrimUnit(unit_name) ;

        if (!unit) {
            // Viper: 3142 - all verific msg should have msg-id
            // Message::PrintLine("unit ",unit_name, " is not yet analyzed") ;
            VhdlTreeNode tmp_print_node ;
            tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

            tmp_print_node.Comment("unit %s is not yet analyzed", unit_name) ;

            return 0 ;
        }
        if (!unit->Id()->IsEntity() && !unit->Id()->IsConfiguration()) {
            // Viper: 3142 - all verific msg should have msg-id
            // Message::PrintLine("unit ",unit_name," is not an entity or configuration") ;
            VhdlTreeNode tmp_print_node ;
            tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

            tmp_print_node.Comment("unit %s is not an entity or configuration", unit_name) ;

            return 0 ;
        }
        if (!to_name) to_name = unit->Id()->OrigName() ;
        VhdlPrimaryUnit *new_unit = unit->CopyPrimaryUnit(to_name, old2new, 0) ;
        if(new_unit) (void) from_lib->AddPrimUnit(new_unit) ;
        return 1 ;
    }
    if (!to_name) to_name = "copied_lib" ;

    // Viper: 3142 - all verific msg should have msg-id
    // Message::PrintLine("Copying library ", to_name) ;
    VhdlTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    tmp_print_node.Comment("Copying library %s", to_name) ;

    VhdlLibrary *to_lib = GetLibrary(to_name, 1) ;
    VhdlPrimaryUnit *unit ;
    MapIter mi ;

    // VIPER #5099 : Before copying the units of library, create an ordered
    // list of design units, so that dependent(used) units can be copied before
    // the units where those are used.
    Set ordered_list(POINTER_HASH) ; // Ordered list of design units
    Set done(POINTER_HASH) ; // VIPER #2845 : Set to catch mutual dependency
    FOREACH_VHDL_PRIMARY_UNIT(from_lib,mi,unit) {
        if (!unit) continue ;
        // Inspect the dependent list of 'unit' and
        // populate the 'ordered_list' in proper order.
        unit->CreateOrderedList(&ordered_list, &done) ;

        // Consider the secondary units and insert the units in which secondary units
        // depends before secondary units
        VhdlSecondaryUnit *sec_unit ;
        MapIter sec_unit_iter ;
        // Iterate over secondary units
        FOREACH_VHDL_SECONDARY_UNIT(unit,sec_unit_iter,sec_unit) {
            if (!sec_unit) continue ;
            sec_unit->CreateOrderedList(&ordered_list, &done) ;
        }
    }

    SetIter si ;
    VhdlDesignUnit *design_unit ;
    old2new.SetForLibraryCopy() ; // Mark that we are copying the library
    // Copy primary and secondary unit as they appear in ordered list to
    // handle mutual recursion properly.
    FOREACH_SET_ITEM(&ordered_list, si, &design_unit) {
        if (!design_unit) continue ;
        const char *new_name = design_unit->Id()->OrigName() ;
        if (design_unit->IsPrimaryUnit()) {
            // Copy primary unit and add that to library
            VhdlPrimaryUnit *new_unit = design_unit->CopyPrimaryUnit(new_name, old2new, 1) ;
            if (new_unit && to_lib->AddPrimUnit(new_unit)) {
                old2new.SetCopiedId(design_unit->Id(), new_unit->Id()) ;
            }
        } else {
            unit = design_unit->GetOwningUnit() ;
            // Copy secondary unit and add that to corresponding primary unit
            VhdlSecondaryUnit *sec_unit = design_unit->CopySecondaryUnit(new_name, old2new) ;
            unit = (unit) ? to_lib->GetPrimUnit(unit->Id()->OrigName()): 0 ;
            if (unit) (void) unit->AddSecondaryUnit(sec_unit) ;
        }
    }
    if(save_restore) {
        FOREACH_VHDL_PRIMARY_UNIT(from_lib, mi, unit) {
            if(!unit) continue ;
            const char *name = unit->Id()->OrigName() ;
            (void) Save(to_name, name) ;
        }
        FOREACH_VHDL_PRIMARY_UNIT(from_lib, mi, unit) {
            if(!unit) continue ;
            const char *name = unit->Id()->OrigName() ;
            (void) Restore(to_name, name) ;
        }
    }
    return 1 ;
}

