# Microsoft Developer Studio Project File - Name="vhdl" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=vhdl - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "vhdl.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "vhdl.mak" CFG="vhdl - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "vhdl - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "vhdl - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "vhdl - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /I "." /I "../containers" /I "../util" /I "../verilog" /I "../hier_tree" /I "../sqlmanager" /D "NDEBUG" /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "_WINDOWS" /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "vhdl - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Z7 /Od /I "." /I "../containers" /I "../util" /I "../verilog" /I "../hier_tree" /I "../sqlmanager" /D "_DEBUG" /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "vhdl - Win32 Release"
# Name "vhdl - Win32 Debug"
# Begin Group "Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\vhdl_file.h
# End Source File
# Begin Source File

SOURCE=.\vhdl_tokens.h
# End Source File
# Begin Source File

SOURCE=.\VhdlClassIds.h
# End Source File
# Begin Source File

SOURCE=.\VhdlCompileFlags.h
# End Source File
# Begin Source File

SOURCE=.\VhdlRuntimeFlags.h
# End Source File
# Begin Source File

SOURCE=.\VhdlConfiguration.h
# End Source File
# Begin Source File

SOURCE=.\VhdlCopy.h
# End Source File
# Begin Source File

SOURCE=.\VhdlDataFlow_Elab.h
# End Source File
# Begin Source File

SOURCE=.\VhdlDeclaration.h
# End Source File
# Begin Source File

SOURCE=.\VhdlExpression.h
# End Source File
# Begin Source File

SOURCE=.\VhdlExplicitStateMachine.h
# End Source File
# Begin Source File

SOURCE=.\VhdlIdDef.h
# End Source File
# Begin Source File

SOURCE=.\VhdlMisc.h
# End Source File
# Begin Source File

SOURCE=.\VhdlName.h
# End Source File
# Begin Source File

SOURCE=.\VhdlScope.h
# End Source File
# Begin Source File

SOURCE=.\VhdlSpecification.h
# End Source File
# Begin Source File

SOURCE=.\VhdlStatement.h
# End Source File
# Begin Source File

SOURCE=.\VhdlTreeNode.h
# End Source File
# Begin Source File

SOURCE=.\VhdlUnits.h
# End Source File
# Begin Source File

SOURCE=.\VhdlValue_Elab.h
# End Source File
# Begin Source File

SOURCE=.\VhdlVarUsage.h
# End Source File
# Begin Source File

SOURCE=.\VhdlVisitor.h
# End Source File
# End Group
# Begin Group "Analysis"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\vhdl_file.cpp
# End Source File
# Begin Source File

SOURCE=.\vhdl_lex.cpp
# End Source File
# Begin Source File

SOURCE=.\vhdl_yacc.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlBinarySaveRestore.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlConfiguration.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlCopy.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlCrossLanguageConversion.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlDeclaration.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlExpression.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlExplicitStateMachine.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlIdDef.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlMisc.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlName.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlPrettyPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlReplace.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlScope.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlSpecification.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlStatement.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlTreeNode.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlUnits.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlVarUsage.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlVisitor.cpp
# End Source File
# End Group
# Begin Group "Static Elaboration"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\vhdl_file_Stat.cpp
# End Source File
# End Group
# Begin Group "Elaboration"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\vhdl_file_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlConfiguration_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlDataFlow_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlDeclaration_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlModifyParseTree_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlExpression_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlIdDef_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlMisc_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlName_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlOperators_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlScope_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlSpecification_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlStatement_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlTreeNode_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlUnits_Elab.cpp
# End Source File
# Begin Source File

SOURCE=.\VhdlValue_Elab.cpp
# End Source File
# End Group
# End Target
# End Project
