/*
 *
 * [ File Version : 1.256 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h> // fclose

#include "VhdlDeclaration.h" // Compile flags are defined within here

#include "Array.h"
#include "Set.h"
#include "Strings.h"
#include "Message.h"
#include "RuntimeFlags.h"

#include "vhdl_tokens.h"
#include "VhdlIdDef.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"
#include "vhdl_file.h"
#include "VhdlUnits.h"

#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"
#include "VhdlCopy.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**********************************************************/
//  Elaboration ; constraint setting
//  Allow nonconstant (range bounds) as an option. Only applicable for pure 'range' (as used in sliced names etc).
/**********************************************************/

VhdlConstraint *VhdlScalarTypeDef::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    if (!_scalar_range) return 0 ;
    return _scalar_range->EvaluateConstraintInternal(df, 0) ;
}

VhdlConstraint *VhdlArrayTypeDef::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    if (!_subtype_indication) return 0 ;
    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(df, 0) ;
    if (!constraint) return 0 ;

    // LRM 3.2.1.1
    // VIPER #6938 : Consider access type constraint having unconstrained designated type as constrained
    if (constraint->IsUnconstrained() && !IsVhdl2008()) {
        // Viper #3363 : Incorrect error with access type
        unsigned is_access_type = _subtype_indication ? _subtype_indication->ContainsAccessType() : 0 ;
        if (!is_access_type) {
            _subtype_indication->Error("array element type cannot be unconstrained") ;
            delete constraint ;
            return 0 ;
        }
    }

    // Now, evaluate the index ranges.
    // Build an array constraint from the element constraint upward
    // (so run LSB->MSB).
    unsigned i ;
    VhdlDiscreteRange *index_range ;
    VhdlConstraint *range_constraint ;
    FOREACH_ARRAY_ITEM_BACK(_index_constraint, i, index_range) {
        range_constraint = index_range->EvaluateConstraintInternal(df, 0) ;
        if (!range_constraint) {
            delete constraint ;
            constraint = 0 ;
            break ;
        }
        constraint = new VhdlArrayConstraint(range_constraint, constraint) ;
    }
    return constraint ;
}

VhdlConstraint *VhdlRecordTypeDef::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    if (!_element_decl_list) return 0 ;

    unsigned i ;
    unsigned j ;
    VhdlElementDecl *elem ;
    VhdlIdDef *id ;
    VhdlConstraint *elem_constraint ;

    Array *elem_constraints = new Array(_element_decl_list->Size()) ;
    Array *all_ids = new Array(_element_decl_list->Size()) ;
    Array *ids ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, elem) {
        ids = elem->GetIds() ;
        elem_constraint = elem->EvaluateConstraintInternal(df, 0) ;
        // LRM 3.2.1.1
        // VIPER #6938 : Consider access type constraint having unconstrained designated type as constrained
        if (elem_constraint && elem_constraint->IsUnconstrained() && !IsVhdl2008()) {
            VhdlSubtypeIndication *subt_ind = elem->GetSubtypeIndication() ;
            unsigned is_access_type = subt_ind ? subt_ind->ContainsAccessType() : 0 ;

            if (!is_access_type) {
                elem->Error("record element cannot be unconstrained") ;
                delete elem_constraint ;
                elem_constraint = 0 ;
            }
        }
        all_ids->Append(ids) ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            elem_constraints->InsertLast((elem_constraint) ? elem_constraint->Copy() : 0) ;
            id->SetConstraint(elem_constraint ? elem_constraint->Copy() : 0) ;
        }
        delete elem_constraint ;
    }
    return new VhdlRecordConstraint(elem_constraints, all_ids) ;
}

VhdlConstraint *VhdlAccessTypeDef::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    VhdlConstraint *result = _subtype_indication->EvaluateConstraintInternal(df, 0) ;
    if (result) {
        result = new VhdlAccessConstraint(result) ; // Viper #4394
    } else { // Viper #7288
        VhdlIdDef *subtype_id = _subtype_indication->GetId() ;

        VhdlIdDef *designated_type = subtype_id && subtype_id->IsIncompleteType() ? subtype_id->DesignatedType() : 0 ;
        if (designated_type) result = new VhdlAccessConstraint(designated_type) ;
    }

    return result ;
}

VhdlConstraint *VhdlFileTypeDef::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    // FIX ME : subtype could be unconstrained
    // which will confuse other usage of constraints.
    // Need special file type constraint ?
    return 0 ;
#if 0
    return _file_type_mark->EvaluateConstraintInternal(df, 0) ;
#endif
}

VhdlConstraint *VhdlEnumerationTypeDef::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    // Elaboration of a physical unit type declaration

    // Verify that the _enumeration_literal_list actually contains at-least one enumeration value. Yacc does not allow this
    VERIFIC_ASSERT(_enumeration_literal_list) ;
    VERIFIC_ASSERT(_enumeration_literal_list->Size());

    // Create enum values for each enumeration value
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list, i, id) {
        id->SetValue(new VhdlEnum(id)) ;
    }

    // Return range left to right
    VhdlIdDef *left_id =  (VhdlIdDef*)_enumeration_literal_list->GetFirst() ;
    VhdlIdDef *right_id = (VhdlIdDef*)_enumeration_literal_list->GetLast() ;

    // If there are only two values in the enumeration type,
    // we can encode it with one bit. Do that here,
    // since here is where we create the constraint for the type.
    // By setting the enumbitval at the enumeration ids, we automatically
    // encode the resulting constraint as a 'bit'.
    if (_enumeration_literal_list->Size()<=2) {
        // Set first value to 0, second one to 1
        left_id->SetBitEncoding('0') ;
        if (right_id!=left_id) {
            right_id->SetBitEncoding('1') ;
            // Note that this type is NOT user encoded, so do NOT call SetUserEncoded() API on it.
        }
    }

#ifdef VHDL_CREATE_ONEHOT_FSMS
    // force one-hot encoding on all enumeration types with more that 2 values :
    if (_enumeration_literal_list->Size()>2) {
        FOREACH_ARRAY_ITEM(_enumeration_literal_list, i, id) {
            id->SetOnehotEncoded() ;
        }
    }
#endif

    VhdlValue *left  = new VhdlEnum(left_id) ;
    VhdlValue *right = new VhdlEnum(right_id) ;
    return new VhdlRangeConstraint(left, VHDL_to, right, 0) ;
}

VhdlConstraint *VhdlPhysicalTypeDef::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    if (!_range_constraint) return 0 ;
    // Elaboration of a physical unit type declaration

    // Calculate the range of the physical type (call the physical value specific routine).
    VhdlConstraint *constraint = _range_constraint->EvaluateConstraintPhysical(df) ;
    if (!constraint) return 0 ; // something bad went wrong.

    // Create values for the physical units. They are 'constants' if used as separate identifiers :
    // Their physical value is already calculated during 'analysis', and stored in the _position of the unit.
    // Now we just need to turn that into a integer or real value (depending on the type of range).
    // CHECK ME with VHDL semantics..
    unsigned i ;
    VhdlPhysicalUnitDecl *unit ;
    VhdlIdDef *id ;
    VhdlValue *val ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list, i, unit) {
        id = unit->GetId() ;
        //if (constraint->Left()->IsReal()) {
        //    val = new VhdlDouble((double)(verific_int64)id->Position()) ; // extra cast needed for VC++ 6.0 compiler.
        //} else {
        //    val = new VhdlInt((verific_int64)id->Position()) ;
        //}
        // Create a physical value with the position (the position is always 'integer'):
        val = new VhdlPhysicalValue((verific_int64)id->Position()) ;
        id->SetValue(val) ; // val is absorbed
    }

    return constraint ;
}

VhdlConstraint *VhdlElementDecl::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    if (!_subtype_indication) return 0 ;
    // Elaboration of a physical unit type declaration
    return _subtype_indication->EvaluateConstraintInternal(df, 0) ;
}

VhdlConstraint *VhdlProtectedTypeDef::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    if (IsStaticElab()) return 0 ;
    return 0 ;
}

VhdlConstraint *VhdlProtectedTypeDefBody::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    if (IsStaticElab()) return 0 ;
    return 0 ;
}

/**********************************************************/
//  Elaboration, declaration execution
/**********************************************************/

void VhdlTypeDef::ElaboratePhysicalTypeDef(VhdlIdDef * /*id*/)  { return ; }

void VhdlPhysicalTypeDef::ElaboratePhysicalTypeDef(VhdlIdDef *type_id)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if (!type_id) return ;

    unsigned i ;
    VhdlPhysicalUnitDecl *elem ;
    verific_uint64 pos ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list,i,elem) {
        if (!elem) continue ;
        VhdlIdDef *id = elem->GetId() ;
        if (!id) continue ;
        pos = elem->Infer(type_id) ; // Test the literal that defines this unit ; (like sec = 1000 ms) ; both need to be of the same type. Return position number
        id->DeclarePhysicalUnitId(type_id, pos) ;
    }
    FOREACH_ARRAY_ITEM_BACK(_physical_unit_decl_list,i,elem) {
        if (!elem) continue ;
        elem->SetInversePosition(type_id) ;
    }
}

void VhdlUseClause::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // use-clause elaboration (elaborate packages that this unit depends on)
    // is now done via at the design unit level. No need to do anything here.
}

void VhdlContextReference::Elaborate(VhdlDataFlow * /*df*/)
{
    // Vhdl 2008 LRM section 13.4
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // use-clause elaboration (elaborate packages that this unit depends on)
    // is now done via at the design unit level. No need to do anything here.
}

void VhdlLibraryClause::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // Nothing to do
}

void VhdlInterfaceDecl::Initialize()
{
    if (!_subtype_indication) return ;
    // VIPER #7628: Produce error if subtype indication is protected type
    if (IsRtlElab() && _subtype_indication->IsProtectedType()) {
        Error("protected type is not yet supported for synthesis") ;
        return ;
    }

    // DON'T bail out if there was an error; we could be initializing in a new netlist. Don't take risks.
    // if (Message::ErrorCount()) return ;

    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(0, 0) ;
    if (!constraint) return ;

    unsigned i ;
    VhdlIdDef *id ;

    // trick to reduce excess value copying
    unsigned single_id = (_id_list && _id_list->Size()==1) ? 1 : 0 ;

    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        // Set id's constraint
        id->SetConstraint((single_id) ? constraint : constraint->Copy()) ;

        // Re-set any previous value that was there.
        // Need to overwrite what was there earlier.
        id->SetValue(0) ;
    }
    if (!single_id) delete constraint ;
}
void VhdlInterfaceTypeDecl::Initialize()
{
    VhdlIdDef *id = GetId() ;
    if (id) id->SetConstraint(0) ;
    if (id) id->SetValue(0) ;
}
void VhdlInterfaceSubprogDecl::Initialize()
{
    // If this is from package instance, do not reset actual subprogram setting
    VhdlIdDef *container = _present_scope ? _present_scope->GetOwner(): 0 ;
    if (container && container->IsPackage()) return ;
    _actual_subprog = 0 ; // Reset actual subprogram setting
}
void VhdlInterfacePackageDecl::Initialize()
{
    // No need to initialize default package
    _actual_pkg = 0 ;
    //_actual_elab_pkg = 0 ;
}

void
VhdlInterfaceDecl::InitializeConstraints(Map &formal_to_constraint)
{
    if (!_subtype_indication) return ;
    // VIPER #7628: Produce error if subtype indication is protected type
    if (IsRtlElab() && _subtype_indication->IsProtectedType()) {
        Error("protected type is not yet supported for synthesis") ;
        return ;
    }

    // Create the constraints for the interface id's, and insert them under VhdlIdDef->VhdlConstraint
    // into the provided Map.
    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(0, 0) ;
    if (!constraint) return ;

    unsigned i ;
    VhdlIdDef *id ;

    // trick to reduce excess copying
    unsigned single_id = (_id_list && _id_list->Size()==1) ? 1 : 0 ;

    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        // VIPER 1863 & 1472
        // This routine may be called twice with same 'formal_to_constraint' map. So, we need
        // to delete old value from map and force insert new value to map
        VhdlConstraint *old_constraint = (VhdlConstraint*)formal_to_constraint.GetValue(id) ; // Get old value
        (void) formal_to_constraint.Insert(id, (single_id) ? constraint : constraint->Copy(), 1 /* force overwrite */) ;
        delete old_constraint ;
    }
    if (!single_id) delete constraint ;
}

void VhdlInterfaceTypeDecl::InitializeConstraints(Map &formal_to_constraint)
{
    (void) formal_to_constraint ;
}
void VhdlInterfaceSubprogDecl::InitializeConstraints(Map &formal_to_constraint)
{
    (void) formal_to_constraint ;
}
void VhdlInterfacePackageDecl::InitializeConstraints(Map &formal_to_constraint)
{
    (void) formal_to_constraint ;
}
void
VhdlInterfaceDecl::InitialValues(Instance *inst, unsigned has_recursion, VhdlDataFlow *df, VhdlScope * /*assoc_list_scope*/)
{
    // Run after actuals are associated.
    // Every interface element should now have a value
    // If not, set the initial value on it.
    // RTL elaboration only : If 'inst' is given, create portrefs into inst.
    (void) inst ; // Needed to prevent "unused" build warnings

    // VIPER 1863 & 1472 : begin
    // Get value map from value stack only for recursion. Return non-null for recursion.
    // Check if value of id is already set from map, as formal values and constraints
    // are set in value and constraint stack for recursive instantiation
    Map *id_values = has_recursion ? VhdlNode::GetValueMap(): 0 ;
    Map *id_constraints = has_recursion ? VhdlNode::GetConstraintMap(): 0 ;
    // VIPER 1863 & 1472 : end
    unsigned consider_init = 1 ;

    // Viper 7133 : Check if this is an interface of converted entity
    VhdlIdDef *owner_id = _present_scope ? _present_scope->GetOwner() : 0 ;
    VhdlPrimaryUnit *punit = owner_id ? owner_id->GetPrimaryUnit() : 0 ;
    unsigned is_verilog_module = punit ? punit->IsVerilogModule() : 0 ;

    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        // VIPER 1863 & 1472 : begin
        if (id_values) { // Have recursion : actuals are not set iddef, check from map
            if (id_values->GetValue(id)) continue ; // Its set already
        } else {
            if (id->Value()) continue ; // Its set already
        }
        // VIPER 1863 & 1472 : end

        if (!id || (!id->IsGeneric() && !id->IsInput() && IsRtlElab())) continue ; // Viper #4883 CHECKME: Should we have checked assigned_before_use ?

        // Skip file/access typed vars ; we don't support them ; they have no value either.
        VhdlIdDef *id_type = id->Type() ; // Type of identifier can be null, if some errors are suppressed earlier
        if (id_type && (id_type->IsFileType() || id_type->IsAccessType())) continue ;

        VhdlConstraint *constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id) : id->Constraint() ;

        // VIPER #4369 : If port is unconstrined type, but constraint is provided
        // from actual, do not evaluate initial value. Initial value will be 'left
        consider_init = 1 ;
        if (IsStaticElab() && id->IsPort() && id->IsUnconstrained() && constraint && !constraint->IsUnconstrained()) {
            consider_init = 0 ;
        }

        if (!id->IsInput() && (!_init_assign || !consider_init)) continue ; // Viper #4883

        if (_init_assign && consider_init) {
            // VIPER 1863 & 1472
            // For recursion : take formal constraint from map, not already set in iddefs
            //VhdlConstraint *constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id) : id->Constraint() ;

            if (!constraint) continue ; // Something bad happened in initialization.

            // VIPER 2651 : Evaluate initial value for generics and unconstrained ports
            // so that unconstrained ports become constrained from the value
            // VIPER #2841 : Evaluate initial value for all interfaces i.e. ports,
            // generics, formals of subprogram declaration. So comment the check
            // introduced by #2651 (evaluate for unconstrainted ports and generics)
            //if (IsStaticElab() && !constraint->IsUnconstrained() && !id->IsGeneric()) continue ;
            // init value should be locally static, so no DataFlow needed
            // VIPER #3367: 'now' can be specified as the initial value of ports/generic.
            // So, we need the dataflow do decide the time frame (we only evaluate 'now' for time 0):
            VhdlValue *val = _init_assign->Evaluate(constraint,df,0) ;
            if (!val) continue ;

            // VIPER #7536: Check for constantness of the default initial value of a generic having no actual:
            if (id->IsGeneric() && !val->IsConstant()) {
                _init_assign->Error("value for generic %s is not constant", id->Name()) ;
                delete val ;
                continue ;
            }

            // Viper 7133: Made code similar to Associate Generics:
            // Check actual against constraint (this should be kind of obsolete here since we already associate formal and actual in full).
            // Viper 5897: Do not call CheckAgainst if the type is vl_logic_vector as we are matching it with integer
            // for dual lang we would match integer with vector types in vhdl. So call CheckAgainst only if actual_type is array type.
            // this is the case of verilog in vhdl only as componet is present.
            if (!is_verilog_module && !val->CheckAgainst(constraint,_init_assign, 0)) {
                delete val ;
                continue ;
            }
            val = (val->IsConstant() && IsStaticElab()) ? val->ToConst(id->Type()): val ; //  Convert the value to constant if it is constant

            if (constraint->IsUnconstrained()) {
                constraint->ConstraintBy(val) ;
            }
            // Now, just set the value
            // VIPER 1863 & 1472 : begin
            if (id_values) { // Recursion : set initial value to map
                // This routine is called twice with same map 'id_values'. So, need to
                // delete old data from 'id_values' and use force insert
                VhdlValue *old_val = (VhdlValue*)id_values->GetValue(id) ; // Get old value
                (void) id_values->Insert(id, val, 1 /* force overwrite */) ;
                delete old_val ; // Delete old value
            } else {
            // VIPER 1863 & 1472 : end
                id->SetValue(val) ;
            }
        } else {
            // VIPER #7033 : Do not set 'left to subprogram formal, when actual is non-cnstant
            // This will create illegal value even in declarative region. (x - 'left_of_integer)
            // is out of range value
            // VIPER #4369/4374/4397 : Create initial value for port from constraint
            // in static elab mode
            if (IsStaticElab() && !id->IsGeneric() && !id->IsSubprogramFormal() && constraint) {
                VhdlValue *val = constraint->CreateInitialValue(id) ;
                id->SetValue(val) ;
            }
            // VIPER 2651 : Do not error out for ports as we cannot evaluate
            // values for ports in static elaboration.
            if (IsStaticElab() && !id->IsGeneric()) continue ;
            // If we are in non-constant region, do not issue error. It is for
            // formal of subprogram and static elaboration could not evaluate the
            // actuals of this subprogram call. So formal could not get values.
            if (df && df->IsNonconstExpr()) continue ;
            // Now that analysis does rigorous checks,
            // this should not happen very often any more
            // Maybe only with incomplete explicit bindings...
            id->Error("formal %s has no actual or default value", id->Name()) ; // FIX ME : LineFile should come from caller
        }
    }
}

void VhdlInterfaceTypeDecl::InitialValues(Instance * /*inst*/, unsigned has_recursion, VhdlDataFlow * /*df*/, VhdlScope * /*assoc_list_scope*/)
{
    // VIPER 1863 & 1472 : begin
    // Get value map from value stack only for recursion. Return non-null for recursion.
    // Check if value of id is already set from map, as formal values and constraints
    // are set in value and constraint stack for recursive instantiation
    Map *id_constraints = has_recursion ? VhdlNode::GetConstraintMap(): 0 ;

    if (id_constraints && id_constraints->GetValue(_type_id)) return ; // Constraint already associated

    if (_type_id && !_type_id->Constraint()) { // Constraint not set from generic map aspect
        _type_id->Error("formal %s has no actual or default value", _type_id->Name()) ;
    }
}
void VhdlInterfaceSubprogDecl::InitialValues(Instance * /*inst*/, unsigned  has_recursion, VhdlDataFlow * /*df*/, VhdlScope *assoc_list_scope)
{
    VhdlIdDef *formal = GetId() ;
    if (!formal) return ; // Something wrong somewhere
    // VIPER 1863 & 1472 : begin
    // Get actual id map from actual id stack only for recursion. Return non-null for recursion.
    // Check if actual_subprog is already set from map,
    Map *id_actuals = has_recursion ? VhdlNode::GetActualIdMap(): 0 ;
    if (id_actuals) {
        if (id_actuals->GetValue(formal)) return ;
    } else {
        if (_actual_subprog) return ; // Already associated
    }
    // Try to extract subprogram name from default expression :
    VhdlIdDef *actual_subprog = GetDefaultSubprogram(assoc_list_scope) ;

    if (!actual_subprog) {
        Error("formal %s has no actual or default value", formal->Name()) ;
    }
    if (id_actuals) {
        (void) id_actuals->Insert(formal, actual_subprog, 1) ;
    } else {
        _actual_subprog = actual_subprog ;
    }
}
void VhdlInterfacePackageDecl::InitialValues(Instance * /*inst*/, unsigned  /*has_recursion*/, VhdlDataFlow * /*df*/, VhdlScope * /*assoc_list_scope*/)
{
    // No need for recursion handling here, as actual should be
    // specified always in generic map
    if (!_actual_elab_pkg || !_initial_pkg) {
        Error("no package instance is specified for interface package element %s", _id ? _id->Name(): "") ;
    }
    if (_actual_elab_pkg || !_initial_pkg) return ; // Already associated

    VhdlScope *actual_scope = _initial_pkg->LocalScope() ;
    MapIter mi ;
    VhdlIdDef *formal, *actual ;
    char *name ;
    Map *local_map = _local_scope ? _local_scope->DeclArea(): 0 ;
    Map *actual_map = actual_scope ? actual_scope->DeclArea(): 0 ;
    unsigned idx = 0 ;
    FOREACH_MAP_ITEM(local_map, mi, &name, &formal) {
        actual = actual_map ? (VhdlIdDef*)actual_map->GetValueAt(idx): 0 ;
        if (formal && actual) formal->ConnectActualId(actual) ;
        idx++ ;
    }
    // Elaborate the _initial_pkg
    // Check generic map aspect
    //if (IsStaticElab()) StaticElaborate() ;
}

void VhdlInterfaceDecl::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // Should never be called. We use Initialize. associate, initialvalues in sequence.
}

void VhdlSubprogramDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_subprogram_spec) return ;

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // Elaborate the subprog spec.
    _subprogram_spec->Elaborate(df) ;
}

void VhdlSubprogramBody::Elaborate(VhdlDataFlow *df)
{
    if (!_subprogram_spec) return ;

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // Elaborate the subprog spec.
    _subprogram_spec->Elaborate(df) ;
}

void VhdlSubtypeDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_subtype_indication || !_id) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // Set the type constraint from the type definition
    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(df, 0) ;
    _id->SetConstraint(constraint) ;
}

void VhdlFullTypeDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_type_def || !_id) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    _type_def->ElaboratePhysicalTypeDef(_id) ; // This will process the TimeUnit if SetTimeResolution is used after Analysis

    // Set the type constraint from the type definition
    VhdlConstraint *constraint = _type_def->EvaluateConstraintInternal(df, 0) ;
    _id->SetConstraint(constraint) ;

    // Viper #3799 : This might be a better place to evaluate enum_attribute expression
    // and set encoding for the enum states... This is because attribute_expression may
    // involve generics and thus can only be evaluated during elaboration.
}

void VhdlIncompleteTypeDecl::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // Nothing to do
}

void VhdlConstantDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_subtype_indication) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // VIPER #7628: Produce error if subtype indication is protected type
    if (IsRtlElab() && _subtype_indication->IsProtectedType()) {
        Error("protected type is not yet supported for synthesis") ;
        return ;
    }

    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(df, 0) ;
    if (!constraint) return ;

    unsigned has_unconstrained_subtype = constraint->IsUnconstrained() ; // Customer specific VIPER #8989
    VhdlValue *init_val = (_init_assign) ? _init_assign->Evaluate(constraint,df,0) : 0 ;

    // Viper #4300 : Do not error out in static elab for non-constant branch during CheckAgainst
    //unsigned non_constant_df = 0 ;
    //if (IsStaticElab() && df && df->IsNonconstExpr()) non_constant_df = 1 ;
    //if (!init_val || !init_val->CheckAgainst(constraint, (non_constant_df) ? 0 : _init_assign)) {
    // VIPER #7655 : Warning will be produced instead of error from 'CheckAgainst'
    // No need to check IsNonconstExpr here
    if (!init_val || !init_val->CheckAgainst(constraint, _init_assign, df)) {
        delete init_val ;
        init_val = 0 ;
    }

    // Test if it is a real constant. It always should according to the LRM...
    if (init_val && !init_val->IsConstant()) {
        // VIPER 5158 : Constant declaration initial value in subprogarm call is allowed to depend on arguments,
        // In that case, it is allowed to be non-constant. Otherwise this is an error.
        if (!df || !df->IsTemporaryDataFlow()) {
            Error("initial value for constant declaration is not constant") ;
        }
    } else {
        // Translate constant value to a real constant value.
        // There might be nets in the value, if the evaluation was a
        // complecated expression. It should be translated to a value without
        // nets in there, since we might be in a package right now.
        // Cannot use nets in a package, since they are shared by multiple
        // entities (netlists). Here is where we need the type again.
        init_val = (init_val) ? init_val->ToConst(_type) : 0 ;
    }

    // LRM 3.2.1.1 if unconstrained array type in subtype_indication,
    // we need to extract the range(s) from the initial value !
    if (constraint->IsUnconstrained()) {
        constraint->ConstraintBy(init_val) ;
    }

    // trick to reduce excess value copying
    unsigned single_id = (_id_list && _id_list->Size()==1) ? 1 : 0 ;

    // FIX ME : if constant has no init value, is it a deferred constant ?
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        // Deferred constants should by now have a link to their defining constant :
        // Actually, we could have done this check after analysis of a package body,
        // But I guess its OK to do it in elaboration.
        if (!_init_assign && !id->Decl()) {
            id->Error("deferred constant %s needs an initial value", id->Name()) ;
        }

        id->SetConstraint((single_id) ? constraint : constraint->Copy()) ;
        id->SetValue((single_id) ? init_val : (init_val) ? init_val->Copy() : 0) ;
    }
#ifdef VHDL_REPLACE_CONST_EXPR
    if (IsStaticElab() && !(df && df->IsTemporaryDataFlow())) {
        id = (_id_list && _id_list->Size()) ? (VhdlIdDef*)_id_list->At(0) : 0 ;
        // Replace initial value expression with evaluated literals. It is done when
        // initial value is not aggregate or expanded name and we are not within subprogram
        if (!has_unconstrained_subtype && id && _init_assign && !_init_assign->IsAggregate() && !_init_assign->IsExpandedName()) {
            // With evaluated value of _init_assign, create a parse tree and replace that with '_init_assign'
            // Create parse tree from 'init_val':
            VhdlExpression *new_init = (init_val && !init_val->HasAccessValue()) ? init_val->CreateConstantVhdlExpressionInternal(_init_assign->Linefile(), id->Type(), -1, 0): 0 ;
            if (new_init) {
                // For vhdl-2008 do not call TypeInfer during elaboration as proper context
                // is not available to call scope->FindXXXX
                //(void) new_init->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ; // Type infer the created initial value
                //(void) new_init->TypeInfer(id->Type(), 0, VHDL_READ, 0) ; // Type infer the created initial value
                (void) ReplaceChildExpr(_init_assign, new_init) ;
            }
        }
        // VIPER #3878 :If constraint exists, re-create subtype indication for array types, so that range bounds can be
        // represented only by literals
        if (id) {
            if (_subtype_indication) _subtype_indication->ReplaceBoundsInSubtypeIndication(df) ;
            //VhdlSubtypeIndication *new_subtype = constraint->CreateSubtype(id->BaseType(),  _subtype_indication) ;
            //if (new_subtype) {
                //delete _subtype_indication ;
                //_subtype_indication = new_subtype ;
                //(void) _subtype_indication->TypeInfer(0,0,VHDL_range,0) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
                //FOREACH_ARRAY_ITEM(_id_list, i, id) { // Viper #4936
                    //id->SetSubtypeIndication(_subtype_indication) ;
                //}
#endif // VHDL_ID_SUBTYPE_BACKPOINTER
            //}
        }
    }
#endif // VHDL_REPLACE_CONST_EXPR
    if (!single_id) delete init_val ;
    if (!single_id) delete constraint ;
}

void VhdlSignalDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_subtype_indication) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // VIPER #7628: Produce error if subtype indication is protected type
    if (IsRtlElab() && _subtype_indication->IsProtectedType()) {
        Error("protected type is not yet supported for synthesis") ;
        return ;
    }
    // Get the subtype constraint
    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(df, 0) ;
    if (!constraint) return ;

    if (constraint->IsUnconstrained()) {
        // LRM 3.2.1.1
        // Viper #3363 : Incorrect error with access type
        // Viper 3771
        unsigned is_access_type = _subtype_indication ? _subtype_indication->ContainsAccessType() : 0 ;
        if (!is_access_type) _subtype_indication->Error("signal cannot be unconstrained") ;
    }

    // Get the initial value
    VhdlValue *init_val = (_init_assign) ? _init_assign->Evaluate(constraint,df,0) : 0 ;

    // Viper #4300 : Do not error out in static elab for non-constant branch during CheckAgainst
    //unsigned non_constant_df = 0 ;
    //if (IsStaticElab() && df && df->IsNonconstExpr()) non_constant_df = 1 ;
    //if (!init_val || !init_val->CheckAgainst(constraint, (non_constant_df) ? 0 : _init_assign)) {
    // VIPER #7655 : Warning will be produced instead of error from 'CheckAgainst'
    // No need to check IsNonconstExpr here
    if (!init_val || !init_val->CheckAgainst(constraint, _init_assign, df)) {
        delete init_val ;
        init_val = 0 ;
    }

    unsigned single_id = (_id_list && _id_list->Size()==1) ? 1 : 0 ;
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        // Enable these info message if needed :
        // if (!id->IsUsed()) Info("%s is never used",id->Name()) ;
        // if (!id->IsAssigned()) Info("%s is never assigned",id->Name()) ;

        id->SetConstraint((single_id) ? constraint : constraint->Copy()) ;

        // Check if we need to assign the initial value to the id (if id is not assigned anywhere)
        // Exclude multi-port RAMs : Unassigned multiport RAM is simply a ROM. Initial value set further down.
        VhdlValue *val = 0 ;
        // VIPER 3789/3795 : ALWAYS create the initial value for static elaboration of variables in non-temporary dataflow!
        if (IsStaticElab() || (!id->IsAssigned() && !id->CanBeMultiPortRam())) {
            // Id is used, but not assigned. Use initial value :
            if (init_val) {
                val = init_val->Copy() ;
            } else {
                // Use default value
                val = constraint->CreateInitialValue(id) ;
            }
        }

        if (val && val->IsConstant()) val = val->ToConst(id->Type()) ; // Translate constant nets to constant values
        id->SetValue(val) ;
    }
#ifdef VHDL_REPLACE_CONST_EXPR
    if (IsStaticElab() && !(df && df->IsTemporaryDataFlow())) {
        id = (_id_list && _id_list->Size()) ? (VhdlIdDef*)_id_list->At(0) : 0 ;
        // Replace initial value expression with evaluated literals. It is done when
        // initial value is not aggregate or expanded name and we are not within subprogram
        if (id && _init_assign && !_init_assign->IsAggregate() && !_init_assign->IsExpandedName()) {
            // With evaluated value of _init_assign, create a parse tree and replace that with '_init_assign'
            // Create parse tree from 'init_val':
            VhdlExpression *new_init = (init_val && !init_val->HasAccessValue()) ? init_val->CreateConstantVhdlExpressionInternal(_init_assign->Linefile(), id->Type(), -1, 0): 0 ;
            if (new_init) {
                //(void) new_init->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ; // Type infer the created initial value
                //(void) new_init->TypeInfer(id->Type(), 0, VHDL_READ, 0) ; // Type infer the created initial value
                (void) ReplaceChildExpr(_init_assign, new_init) ;
            }
        }
        // VIPER #3878 :If constraint exists, re-create subtype indication for array types, so that range bounds can be
        // represented only by literals
        if (id) {
            if (_subtype_indication) _subtype_indication->ReplaceBoundsInSubtypeIndication(df) ;
            //VhdlSubtypeIndication *new_subtype = constraint->CreateSubtype(id->BaseType(), _subtype_indication) ;
            //if (new_subtype) {
                //delete _subtype_indication ;
                //_subtype_indication = new_subtype ;
                //(void) _subtype_indication->TypeInfer(0,0,VHDL_range,0) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
                //FOREACH_ARRAY_ITEM(_id_list, i, id) { // Viper #4936
                    //id->SetSubtypeIndication(_subtype_indication) ;
                //}
#endif // VHDL_ID_SUBTYPE_BACKPOINTER
            //}
        }
    }
#endif // VHDL_REPLACE_CONST_EXPR
    if (!single_id) delete constraint ;
    delete init_val ;
}

void VhdlVariableDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_subtype_indication) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // VIPER #7628: Produce error if subtype indication is protected type
    if (IsRtlElab() && _subtype_indication->IsProtectedType()) {
        Error("protected type is not yet supported for synthesis") ;
        return ;
    }
#if 0
    // Viper 7220 : Moved this check to analysis.
    // VHDL 2008 LRM section 6.4.2.4 Variables declared immediately within a
    // package shall be not be shared variables if the package is
    // declared within a subprogram, process, or protected type body;
    unsigned package_under_process_sub_prot = 0 ;
    VhdlIdDef *owner = _present_scope ? _present_scope->GetOwner() : 0 ;
    if (IsVhdl2008() && owner && owner->IsPackage()) {
        VhdlScope *scope = _present_scope ? _present_scope->Upper() : 0 ;
        while (scope) {
            owner = scope->GetOwner() ;
            if ((owner && (owner->IsSubprogram() || owner->IsProtectedBody())) || scope->IsProcessScope()) {
                package_under_process_sub_prot = 1 ;
                break ;
            }
            scope = scope->Upper() ;
        }
    }

    // Some left-over syntax check that was not done before
    // Shared variables can only occur in concurrent areas (no dataflow)
    if (_shared && ((df && df->IsInSubprogramOrProcess()) || package_under_process_sub_prot)) {
        if (package_under_process_sub_prot) {
            Error("variable in package inside process,subprogram or protected block cannot be 'shared'") ;
        } else {
            Error("variable in subprogram or process cannot be 'shared'") ;
        }
    } else if (!_shared && (!df || !df->IsInSubprogramOrProcess())) {
        if (!package_under_process_sub_prot) Error("variable outside of subprogram or process must be 'shared'") ;
    }
#endif

    // Get the variable constraint
    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(df, 0) ;

    // Is this an access type ? Then contraint may be null or unconstrained.
    // Try to get the constraint from initial allocation, if present
    // VIPER #6750 : Try to get the constraint from initial expression when initial
    // expression is not null
    unsigned is_access_type = _subtype_indication->IsAccessType() ;
    if ((!constraint || constraint->IsUnconstrained()) && _init_assign && !_init_assign->IsNull() && is_access_type) {
        delete constraint ;
        constraint = _init_assign->EvaluateConstraintInternal(df, 0) ;
    }

    if (!constraint) return ;

    // Get the initial value, if unconstrained and access type, get the type
    // from initial value.
    VhdlValue *init_val = 0 ;
    if (!_init_assign) {
        init_val = is_access_type ? new VhdlAccessValue(0) : 0 ;
    } else {
        init_val = _init_assign->Evaluate(constraint, df, 0) ;
    }
    if (init_val && is_access_type && constraint->IsUnconstrained()) constraint->ConstraintBy(init_val) ;

    if (constraint->IsUnconstrained() && !_subtype_indication->ContainsAccessType()) {
        // LRM 3.2.1.1
        _subtype_indication->Error("variable cannot be unconstrained") ;
    }

    // Viper #4300 : Do not error out in static elab for non-constant branch during CheckAgainst
    //unsigned non_constant_df = 0 ;
    //if (IsStaticElab() && df && df->IsNonconstExpr()) non_constant_df = 1 ;
    // VIPER #7655 : Warning will be produced instead of error from 'CheckAgainst'
    // No need to check IsNonconstExpr here
    //if (!init_val || !init_val->CheckAgainst(constraint, (non_constant_df) ? 0 : _init_assign)) {
    if (!init_val || !init_val->CheckAgainst(constraint, _init_assign, df)) {
        delete init_val ;
        init_val = 0 ;
    }

    // trick to reduce excess value copying
    unsigned single_id = (_id_list && _id_list->Size()==1) ? 1 : 0 ;

    // Now set constraint and value on each variable in the declaration
    unsigned i ;
    VhdlIdDef *id ;
    VhdlValue *val ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        id->SetConstraint((single_id) ? constraint : constraint->Copy()) ;

        // Difference between variables in a process and variables in a subprogram
        // The ones in a process stay alive, so they are actually non-constant
        // Look at the 'dataflow' to determine if we are in a subprogram
        val = 0 ;
        if (df && df->IsTemporaryDataFlow()) {
            // We are in a subprogram here.
            // All variables come alive at decl time
            // Need constant initial value
            if (_init_assign || init_val) {//init_val) {
                // Use initial value given
                // (VIPER #7292): No need for copy explicit when single identifier
                val = (!single_id && _init_assign && _init_assign->IsAllocator()) ? (init_val ? init_val->CopyExplicit():0) : (init_val? init_val->Copy():0) ; // Viper #4394
            } else {
                VhdlIdDef *type = id->Type() ;
                // Use default initial value
                val = constraint->CreateInitialValue(id) ;
                if (type && type->IsAccessType()) val = new VhdlAccessValue(val, constraint->Copy()) ; // Viper 3764/3771
            }
        } else {
            // Variable in a process (or 'shared' variable).
            // Need constant value only if not assigned

            // Enable these messages if needed :
            // if (!id->IsAssigned()) Info("%s is never assigned",id->Name()) ;
            // if (!id->IsUsed()) Info("%s is never used",id->Name()) ;

            // Check if we need to assign the initial value to the id (if id is not assigned anywhere)
            // Exclude multi-port RAMs : Unassigned multiport RAM is simply a ROM. Initial value set further down.
            // VIPER 3789/3795 : ALWAYS create the initial value for static elaboration of variables in non-temporary dataflow!
            if (IsStaticElab() || (!id->IsAssigned() && !id->CanBeMultiPortRam())) {
                if (init_val) {
                    // Call CopyExplicit for VhdlAccessValue, as we need a fresh
                    // allocated value for each identifier. CopyExplicit for all
                    // other Value incarnations call Copy
                    // (VIPER #7292): No need for copy explicit when single identifier
                    val = (!single_id && _init_assign && _init_assign->IsAllocator()) ? init_val->CopyExplicit() : init_val->Copy() ; // Viper #4394
                } else {
                    VhdlIdDef *type = id->Type() ;
                    // Use default value
                    val = constraint->CreateInitialValue(id) ;
                    if (!_init_assign && type && type->IsAccessType()) val = new VhdlAccessValue(val, constraint->Copy()) ; // Viper 3764/3771
                }
            }
        }
        if (val && val->IsConstant()) val = val->ToConst(id->Type()) ;

        id->SetValue(val) ;
    }
#ifdef VHDL_REPLACE_CONST_EXPR
    if (IsStaticElab() && !(df && df->IsTemporaryDataFlow()) &&  !is_access_type) {
        id = (_id_list && _id_list->Size()) ? (VhdlIdDef*)_id_list->At(0) : 0 ;
        // Replace initial value expression with evaluated literals. It is done when
        // initial value is not aggregate or expanded name or access type and we are not within subprogram
        if (id && _init_assign && !_init_assign->IsAggregate() && !_init_assign->IsExpandedName()) {
            // With evaluated value of _init_assign, create a parse tree and replace that with '_init_assign'
            // Create parse tree from 'init_val':
            VhdlExpression *new_init = (init_val && !init_val->HasAccessValue()) ? init_val->CreateConstantVhdlExpressionInternal(_init_assign->Linefile(), id->Type(), -1, 0): 0 ;
            if (new_init) {
                // For vhdl-2008 do not call TypeInfer during elaboration as proper context
                // is not available to call scope->FindXXXX
                //(void) new_init->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ; // Type infer the created initial value
                //(void) new_init->TypeInfer(id->Type(), 0, VHDL_READ, 0) ; // Type infer the created initial value
                (void) ReplaceChildExpr(_init_assign, new_init) ;
            }
        }
        // VIPER #3878 :If constraint exists, re-create subtype indication for array types, so that range bounds can be
        // represented only by literals
        if (id) {
            if (_subtype_indication) _subtype_indication->ReplaceBoundsInSubtypeIndication(df) ;
            //VhdlSubtypeIndication *new_subtype = constraint->CreateSubtype(id->BaseType(), _subtype_indication) ;
            //if (new_subtype) {
                //delete _subtype_indication ;
                //_subtype_indication = new_subtype ;
                //(void) _subtype_indication->TypeInfer(0,0,VHDL_range,0) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
                //FOREACH_ARRAY_ITEM(_id_list, i, id) { // Viper #4936
                    //id->SetSubtypeIndication(_subtype_indication) ;
                //}
#endif // VHDL_ID_SUBTYPE_BACKPOINTER
            //}
        }
    }
#endif // VHDL_REPLACE_CONST_EXPR
    if (!single_id) delete constraint ;
    delete init_val ;
}

void VhdlFileDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_subtype_indication) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    VhdlConstraint *constraint = _subtype_indication->EvaluateConstraintInternal(df, 0) ;
    // if (!constraint) return ; // We don't have constraints for 'file of string' (file types).
    unsigned i ;

    // VIPER 3341 :
    // Open the file if we are in 'initial' mode :
    FILE *file = 0 ;
    if (_file_open_info) {
        // VIPER 3683 : Only open the file if we are in a subprogram (temporary data flow).
        // In a normal (process or package or so) dataflow, the file info can only affect running data, not initial data.
        if (!df || df->IsTemporaryDataFlow()) {
            file = _file_open_info->Elaborate(df) ;
            // 'file', if there, is now 'open'.
        }
    }

    // FIX ME : test file name and mode, Maybe even open the file ?
    VhdlFileId *id ; // _id_list is a list of VhdlFileId*.
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        // Set the constraint :
        if (constraint) id->SetConstraint(constraint->Copy()) ;
        // Set the file :

        // LRM 3.4.1 : implicit fclose occurs at end of elaboration of the scope of an identifier.
        // We don't have a routine running at the end, so just do the implicit fclose
        // here at initialization time :
        if (id->GetFile()) {
            fclose(id->GetFile()) ;
            id->SetFile(0) ;
        }

        // set the new file
        if (file) id->SetFile(file) ;

        // Multiple id's : LRM 4.3.1.4 : multiple identifiers on same file :
        // language does not define if multiple files are opened or only one.
        // we choose only one. So all id's refer to the same file object. Values are read only once, from any of the file ids.
    }
    delete constraint ;
}

void VhdlAliasDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_designator || !_alias_target_name) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // VIPER #6749: If _alias_target_name is external name, target_id may not be
    // set in alias id. Try to set that again
    if (!_designator->GetTargetId() && _alias_target_name->IsExternalName()) {
        VhdlIdDef *target_id = _alias_target_name->FindExternalObject(0, 0) ;
        if (target_id) _designator->SetTargetId(target_id) ;
    }

    VhdlConstraint *constraint = (_subtype_indication) ? _subtype_indication->EvaluateConstraintInternal(df, 0) : 0 ;
    if (!_subtype_indication && _alias_target_name->IsExternalName()) {
        VhdlSubtypeIndication *subtype_indication = _alias_target_name->GetSubtypeIndication() ;
        constraint = subtype_indication ? subtype_indication->EvaluateConstraintInternal(df, 0): 0 ;
    }

    // LRM rules 4.3.3.1.b applies for object aliases : constraints need to match
    if (constraint && !constraint->IsUnconstrained()) {
        // LRM 4.3.3.1.b for constrained subtypes
        VhdlConstraint *target_name_constraint = _alias_target_name->EvaluateConstraintInternal(df, 0) ;
        if (target_name_constraint) {
            unsigned check_constraint = 1 ;
            // Viper #6125
            if (IsStaticElab() && df && df->IsNonconstExpr() && target_name_constraint->IsUnconstrained()) check_constraint = 0 ;

            if (!check_constraint) { // Viper #6125
                // do nothing
            } else if (target_name_constraint->IsArrayConstraint() || target_name_constraint->IsRecordConstraint()) {
                // composite object
                if (target_name_constraint->Length() != constraint->Length()) {
                    Error("alias target name does not have same number of elements as alias subtype") ;
                }
            } else {
                // scalar object ; LRM is very strict here : Same bounds required
                if (!target_name_constraint->Left()->IsEqual(constraint->Left()) ||
                    !target_name_constraint->Right()->IsEqual(constraint->Right()) ||
                    target_name_constraint->Dir()!=constraint->Dir()) {
                    // VIPER #1943 - Even though the LRM states that this should be an
                    // error, Modeltech doesn't give an error.  Because of this, we are
                    // changing the following message from Error to Warning.
                    Warning("subtype of alias %s does not have the same bounds or direction as the target name", _designator->Name()) ;
                }
            }
        }
        delete target_name_constraint ;
    } else {
        // LRM 4.3.3.1.b for unconstrained or absent subtypes
        // Take the constraint from the name :
        delete constraint ;
        constraint = _alias_target_name->EvaluateConstraintInternal(df, 0) ; // allocates a new constraint..
    }

    _designator->SetConstraint(constraint) ;
}

void VhdlComponentDecl::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // Cant elaborate this declaration until we have actuals.
    // there is no network now.
    // This is similar to a function/procedure call.
    // Execution happens when we elaborate the call.
}

void
VhdlComponentDecl::AssociateGenerics(const Array *actual_generics) const
{
    if (!_id) return ;

    // Set generics based on a set of actual generics.

    // VIPER 1863 & 1472 : begin
    // Get last entered value and constraint map from corresponding stack.
    // Maps will be returned for recursive instantiation
    Map *id_values = VhdlNode::GetValueMap() ;
    Map *id_constraints = VhdlNode::GetConstraintMap() ;
    Map *id_actuals = VhdlNode::GetActualIdMap() ;
    // VIPER 1863 & 1472 : end
    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0, 0, _local_scope) ;
    df.SetInInitial() ;

    // VHDL-2008 LRM section 6.5.6.1 says 'A name that denotes an interface
    // declaration in a generic interface list may appear in an interface
    // declaration within the interface list containing the denoted interface
    // declaration'
    // To support the above, change the flow of generic association. We need to
    // consider one interface declaration at a time and perform
    // 1) Initialization of constraint
    // 2) Evaluate and associate actual
    // 3) Evaluate initial value if actual not present
    // After the above 3 steps we need to consider next interface declaration.
    // So first create a map containing generic id vs its actual from 'actual_generics'
    Map actuals(POINTER_HASH) ;
    unsigned i, j ;
    VhdlInterfaceDecl *decl ;
    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    VhdlIdDef *gen_id ;
    VhdlConstraint *constraint ;
    FOREACH_ARRAY_ITEM(actual_generics, i, assoc) {
        if (!assoc) continue ;
        if (assoc->IsAssoc()) {
            id = assoc->FindFormal() ;
            if (_local_scope && id) id = _local_scope->FindSingleObjectLocal(id->Name()) ;
        } else {
            id = _id->GetGenericAt(i) ;
        }
        if (id) (void) actuals.Insert(id, assoc, 0, 1 /* for partial assoc*/) ;
    }
    if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        // Initialize constraint
        decl->Initialize() ;

        // To evaluate the actual set value/constraint of containing unit in ids
        if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;

        // Get generic ids from declaration :
        Array *ids = decl->GetIds() ;
        Array tmp_ids(2) ;
        if (ids && ids->Size()) { // Value generics
            tmp_ids.Append(ids) ;
        } else { // For VHDL-2008 specific type/package/subprogram generic
            gen_id = decl->GetId() ;
            tmp_ids.InsertLast(gen_id) ;
        }
        FOREACH_ARRAY_ITEM(&tmp_ids, j, gen_id) { // Iterate over ids
            if (!gen_id) continue ;
            MapItem *item ;
            FOREACH_SAME_KEY_MAPITEM(&actuals, gen_id, item) { // Get proper actual
                assoc = (VhdlDiscreteRange*)item->Value() ;
                if (!assoc) continue ;

                // Find the formal.
                if (assoc->IsAssoc() ) {
                    // Find the formal :
                    id = assoc->FindFullFormal() ;
                    if (!id) {
                        // Partial association....

                        // If formal is not constrained, set proper constraint to it
                        VhdlNode::EvaluateConstraintForPartialAssoc(actual_generics, assoc, &df) ;
                        assoc->PartialAssociation(0, 0) ;
                        continue ;
                    }
                    // VIPER 2441 : sometimes we push an entity assoc list into the component.
                    // When that happens, we have entity port formals in this assoc formals.
                    // Switch them to the local component formals :
                    if (_local_scope) id = _local_scope->FindSingleObjectLocal(id->Name()) ;
                } else {
                    // positional association
                    id = gen_id ; //_id->GetGenericAt(j) ;
                }
                if (!id) continue ; // type-inference checks failed

                // Evaluate the actual (pass in constraint)
                // VIPER 1863 & 1472
                // For recursive instance get constraint from stack, as id has instantiating unit's constraint
                constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id) : id->Constraint() ; // Take formal constraint from map for recursion
                if (id->IsGenericType()) {
                    // VIPER #5918 : Formal is interface_type, we need to calculate constraint of it from
                    // actual and it cannot have any value
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, 0):0 ;
                    if (constraint) {
                        if (id_constraints && id_values) {
                            (void) id_constraints->Insert(id, constraint, 1) ;
                            (void) id_values->Insert(id, 0, 1) ;
                        } else {
                            id->SetConstraint(constraint) ;
                            id->SetValue(0) ;
                        }
                    }
                    VhdlIdDef *actual_type = actual_part ? actual_part->TypeInfer(0, 0, VHDL_range, 0): 0 ;
                    if (id_actuals) {
                        (void) id_actuals->Insert(id, actual_type, 1) ;
                    } else {
                        id->SetActualId(actual_type, actual_part ? actual_part->GetAssocList() : 0) ;
                    }
                    continue ;
                }
                if (id->IsSubprogram()) {
                    // Formal is generic subprogram, actual can only be subprogram name which
                    // conforms with formal
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_subprogram = actual_part ? actual_part->SubprogramAspect(id): 0 ;
                    if ((!actual_subprogram && actual_part) || (actual_subprogram && !actual_subprogram->IsSubprogram())) {
                        assoc->Error("subprogram name should be specified as actual for formal generic subprogram %s", id->Name()) ;
                    }
                    if (actual_subprogram && !id->IsHomograph(actual_subprogram)) {
                        assoc->Error("subprogram %s does not conform with its declaration", actual_subprogram->Name()) ;
                    }
                    if (!actual_subprogram) continue ;
                    if (id_actuals) {
                        (void) id_actuals->Insert(id, actual_subprogram, 1) ;
                    } else {
                        id->SetActualId(actual_subprogram, 0) ;
                    }
                    continue ;
                }
                if (id->IsPackage()) {
                    // Formal is a generic package, actual can be a package
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_package = actual_part ? actual_part->EntityAspect(): 0 ;
                    if ((!actual_package && actual_part) || (actual_package && !actual_package->IsPackage())) {
                        assoc->Error("package name should be specified as actual for formal generic package %s", id->Name()) ;
                    }
                    if (!actual_package) continue ;
                    if (id_actuals) {
                        (void) id_actuals->Insert(id, actual_package->GetElaboratedPackageId(), 1) ;
                    } else {
                        id->SetElaboratedPackageId(actual_package) ;
                    }
                    continue ;
                }

                if (!constraint) continue ; // something bad happened in initialization

                val = assoc->Evaluate(constraint,&df,0) ;
                if (!val) continue ; // can be for OPEN association. Don't set a value, so initial value will be set.

                // Generics are constant :
                if (!val->IsConstant()) {
                    assoc->Error("value for generic %s is not constant", id->Name()) ;
                    delete val ;
                    continue ;
                }

                // Translate back to a constant (get rid of any constant NonConstVal's)
                val = val->ToConst(id->Type()) ;

                // Adjust constraint if needed.
                if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

                // Check actual against constraint
                if (!val->CheckAgainst(constraint,assoc, 0)) {
                    delete val ;
                    continue ;
                }

                // VIPER 1863 & 1472 : begin
                if (id_values) { // Have recursion : set value to stack not in iddefs
                    // This routine is called twice with same map 'id_values'. So, need to
                    // delete old data from 'id_values' and use force insert
                    VhdlValue *old_val = (VhdlValue*)id_values->GetValue(id) ; // Get old value
                    (void) id_values->Insert(id, val, 1 /* force overwrite */) ; // Set value to value stack
                    delete old_val ;
                } else {
                // VIPER 1863 & 1472 : end
                    // Set the initial value
                    id->SetValue(val) ;
                }
            }
        }
        // Now evaluate initial expression :
        // To evaluate the actual set value/constraint of containing unit in ids
        if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;
        decl->InitialValues(0, 0, &df, _present_scope) ;
    }
    // Set value/constraint of instantiated unit in maps and instantiating unit in ids
    if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;
}

void VhdlComponentDecl::AssociatePorts(Instance *inst, const Array *actual_ports) const
{
    if (!_id) return ;

    // VIPER 1863 & 1472 : begin
    // Get last entered value and constraint map from corresponding stacks. Maps
    // will be returned if this component is instantiated recursively
    Map *id_values = VhdlNode::GetValueMap() ;
    Map *id_constraints = VhdlNode::GetConstraintMap() ;

    // To evaluate constraints of ports we need current value of generic ids
    // evaluated in 'AssociateGenerics'. Current values of generics are in
    // stack, so switch value/constraint between iddefs and stack
    VhdlNode::SwitchValueConstraintOfScope(_local_scope, 1 /* only generic ids*/) ;
    // VIPER 1863 & 1472 : end

    // Initialize the ports constraints and set values to 0.
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_port_clause,i,decl) {
        // VIPER 1863 & 1472 : begin
        if (id_constraints) { // Recursion : set formal-constraint to stack
            decl->InitializeConstraints(*id_constraints) ;
        } else {
        // VIPER 1863 & 1472 : end
            decl->Initialize() ;
        }
    }

    // And make the associations
    VhdlDiscreteRange *assoc ;
    VhdlConstraint *constraint ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    // VIPER 1863 & 1472
    // If value/constraint stack is active, evaluate actuals taking formal constraint
    // from constraint stack
    // Set previous run value/constraint to generic ids if value/constraint stack is active
    // As generic ids not carrying instantiated unit's value/constraint, but to calculate
    // actuals we need instantiating unit's value/constraint to generic ids

    VhdlIdDef *prev_formal_id = 0 ;
    Set partial_formals(POINTER_HASH) ;
    // Viper 4395 : Issue error for individually associated formal which is not in contiguous
    // sequence. Here prev_formal_id is the previous formal associated. All partially associated
    // formals are stored in partial_formals.
    VhdlNode::SwitchValueConstraintOfScope(_local_scope, 1 /* only generic ids*/) ;

    FOREACH_ARRAY_ITEM(actual_ports, i, assoc) {
        if (!assoc) continue ;

        // Find the formal.
        if (assoc->IsAssoc() ) {
            // Find the formal :
            id = assoc->FindFullFormal() ;
            if (!id) {
                // Partial association....
                // If formal is not constrained, set proper constraint to it
                VhdlNode::EvaluateConstraintForPartialAssoc(actual_ports, assoc, 0) ;
                assoc->PartialAssociation(inst, 0) ;
                VhdlName *formal_name = assoc->FormalPart() ;
                VhdlName *prefix_name = formal_name ? formal_name->GetPrefix() : 0 ;
                while (prefix_name && prefix_name->GetPrefix()) prefix_name = prefix_name->GetPrefix() ;
                VhdlIdDef *prefix_id = prefix_name ? prefix_name->GetId() : 0 ;
                // Viper 4395 : We issue error if the formal is already associated
                // previously but not in the one before this
                // Viper 6055: The prefix_id must be a port of _id
                if (prefix_id && prev_formal_id != prefix_id && partial_formals.Get(prefix_id) && _id->GetPort(prefix_id->Name())) {
                    assoc->Error("formal %s associated individually was not in contiguous sequence", prefix_id->Name()) ;
                }
                prev_formal_id = prefix_id ;
                (void) partial_formals.Insert(prefix_id, 0) ;
                continue ;
            }
        } else {
            // positional association
            id = _id->GetPortAt(i) ;
        }
        prev_formal_id = 0 ;
        if (!id) continue ; // type-inference checks failed

        // VIPER 1863 & 1472
        // If constraint stack is active get formal constraint from stack, otherwise from id itself
        constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id): id->Constraint() ;

        if (!constraint) continue ; // Something bad happened in initialization..

        // Create values : evaluate the actual, regardless of direction (so we can support unconstrained ports)
        val = assoc->Evaluate(constraint, 0,0) ;

        if (IsStaticElab()) {
            // VIPER #2651 : Static elaboration does not evaluate non-constant value.
            // If formal is unconstrained, formal should get constrained from
            // actual. So, try to evaluate constraint of actual and adjust target
            // constraint accordingly
            VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
            // VIPER #4547 : Mark this port of component if valid actual (not open) is specified for this
            if (actual_part && !actual_part->IsOpen()) id->SetIsActualPresent() ;

            if (!val) {
                VhdlConstraint *actual_constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, 0):0 ;
                if (constraint->IsUnconstrained()) {
                    constraint->ConstraintBy(actual_constraint) ;
                } else {
                    // VIPER #3140 : Check size mismatching in port association
                    if (actual_constraint && !actual_constraint->IsUnconstrained()) (void) constraint->CheckAgainst(actual_constraint, assoc, 0) ;
                }
                delete actual_constraint ;
            }
        }
        if (!val) continue ; // can be for OPEN association. Don't set a value, so initial value will be set.

        // Adjust constraint if needed.
        if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

        // Check actual against constraint.
        // Issue 1858 : For inputs, do this check before formal net-assigns.
        // or else we will enter net-assign with illegal values.
        if (id->IsInput() && !val->CheckAgainst(constraint,assoc, 0)) {
            delete val ;
            continue ;
        }

        // Check actual against constraint.
        // Issue 1858 : For outputs, do this check after formal net-assigns.
        // or else we will have cut-off scalar outputs already
        if (!id->IsInput() && !val->CheckAgainst(constraint,assoc, 0)) {
            delete val ;
            continue ;
        }

        // VIPER 1863 & 1472 : begin
        if (id_values) { // Has active value stack
            // This routine is called twice with same map 'id_values'. So, need to
            // delete old data from 'id_values' and use force insert
            VhdlValue *old_val = (VhdlValue*)id_values->GetValue(id) ; // Get old value
            (void) id_values->Insert(id, val, 1 /* force overwrite */) ; // Set value to value stack, not in id
            delete old_val ;
        } else {
        // VIPER 1863 & 1472 : end
            // Just plain value setting now.
            // VIPER #4369/4374/4397 : Do not set evaluated value of actual in static elab
            if (IsRtlElab()) {
                id->SetValue(val) ;
            } else {
                delete val ;
            }
        }
    }

    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0, 0, _local_scope) ;
    df.SetInInitial() ;

    // Check initial value setting (will only do something if no portrefs were created.
    FOREACH_ARRAY_ITEM(_port_clause,i,decl) {
        decl->InitialValues(inst, id_values ? 1 : 0, &df, 0) ; // Propagate recursion info by 2 nd arg
    }
}

void VhdlAttributeDecl::Elaborate(VhdlDataFlow *df)
{
    if (!_type_mark || !_id) return ;

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    VhdlConstraint *constraint = _type_mark->EvaluateConstraintInternal(df, 0) ;
    _id->SetConstraint(constraint) ;
}

void VhdlAttributeSpec::Elaborate(VhdlDataFlow *df)
{
    if (!_designator || !_value) return ;

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // LRM 12.3.2.1 DOES subscribe that we need to do a value<->constraint check here.
    // So we have to execute the expression here and test against the constraint.

    // Pick up the attribute identifier :
    VhdlIdDef *attr = _designator->GetId() ;
    if (!attr) return ;

    // Get the constraint on the attribute value
    VhdlConstraint *constraint = attr->Constraint() ;
    if (!constraint) return ;

    // VIPER #1332 fix: Make copy of the constraint (and delete later), because multiple attribute specs
    // can apply to the same attribute.  In the case where the attribute's type_mark
    // happens to be an unconstrained subtype, each attr spec pertaining to this attribute
    // can have a different value lengths, due to VhdlConstaint::ConstraintBy being called
    // on the constraint.
    constraint = constraint->Copy() ;

    // Evaluate the value
    VhdlValue *value = _value->Evaluate(constraint,df,0) ;
    if (!value) {
        delete constraint ;
        return ;
    }

    // Check actual against constraint
    if (!value->CheckAgainst(constraint,this, 0)) {
        delete value ;
        value = 0 ;
    }

    // RD: Since 4/2007, we no longer store VhdlValue of an attribute on the identifiers.
    // We store the VhdlExpression instead.
    // We do elaboration of attribute value at USAGE time (in VhdlAttributeName::Elaborate).
    // That is because attribute elaboration is dynamic (LRM 12.3.2.1 'expression need not be static'.
    // So we don't need to set the value into the entity spec.
    // But, that routine also sets the attribute value on netlist objects in the id's value (for signals/variables).
    if (_entity_spec) _entity_spec->SetAttribute(attr, value) ;

    // set_attribute makes a copy
    delete constraint ;
    delete value ;
}

void VhdlConfigurationSpec::Elaborate(VhdlDataFlow * /*df*/)
{
    if (!_component_spec) return ;

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // This is the time to link the labels to component binding
    // We did not even do the component-entity resolving, so
    // lets do that first.
    // For both labels and component, we need the scope.
    // This is why during elaboration, we make sure that
    // _present_scope is accurate and up-to-date.

    // Find the component and labels in the scope
    // and set the binding onto the labels

    // RD: 5/2007: this is already done in analysis (ResolveSpecs)
    // _component_spec->Configure(_binding,0/*no block config*/) ;
}

void VhdlDisconnectionSpec::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // FIX ME
}

void VhdlGroupTemplateDecl::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // FIX ME
}

void VhdlGroupDecl::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // FIX ME
}

void VhdlProtectedTypeDef::Elaborate(VhdlDataFlow * /*df*/)
{
    if (IsStaticElab()) return ;
}

void VhdlProtectedTypeDefBody::Elaborate(VhdlDataFlow * /*df*/)
{
    if (IsStaticElab()) return ;
}

// Execution of a subprogram body
VhdlValue *VhdlSubprogramBody::ElaborateSubprogram(const Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef *subprog_inst)
{
    if (!_subprogram_spec) return 0 ;

    // First push present values on the stack ; find via two-step path, to look through separate specification if there
    VhdlIdDef *subprog = _subprogram_spec->GetId() ;
    if (!subprog) return 0 ; // no use
    VhdlDeclaration *instance = subprog_inst ? subprog_inst->GetSubprogInstanceDecl(): 0 ;
    Array *actual_generics = instance ? instance->GetGenericMapAspect(): 0 ;

    // Store constraints/values in these Map's.
    // Needed since we still need the present values/constraints when we evaluate the actuals.
    // but we are already preparing the new formal constraints / values...
    Map formal_to_constraint(POINTER_HASH) ;
    Map formal_to_value(POINTER_HASH) ;
    // Store the referece target for output signals passed by reference. This implements the idea of stack
    Map formal_to_actual(POINTER_HASH) ;

    // VIPER #5918 : VHDL-2008 Enhancement : Associate the generics
    // Store constraints/values in these Map's.
    // Needed since we still need the present values/constraints when we evaluate the actuals.
    Map generic_to_constraint(POINTER_HASH) ;
    Map generic_to_value(POINTER_HASH) ;
    Map generic_to_id(POINTER_HASH) ;
    Array all_generics(2) ;
    _subprogram_spec->AssociateGenerics(actual_generics, &generic_to_constraint, &generic_to_value, &generic_to_id, &all_generics) ;

    _subprogram_spec->InitializeConstraints(formal_to_constraint) ;

    // After initialization of formal, we need to evaluate the arguments supplied
    // for formals and to evaluate that we need previous value/constraint of generics.
    _subprogram_spec->SwitchValueConstraintOfGenerics(&all_generics, &generic_to_constraint, &generic_to_value, &generic_to_id) ;

    // Create a new dataflow. Don't use old one...
    // Issue 1818 : We need to pass-in the upper dataflow, or else 'global' variables assignments
    // from within the subprogram do not obtain their existing value..
    VhdlDataFlow *subprog_df = new VhdlDataFlow(df,/*not clocked*/0, LocalScope(), /*no sensitivity list*/0) ;
    VERIFIC_ASSERT(subprog_df) ;
    subprog_df->SetInSubprogramOrProcess() ;
    // VIPER #5918 : Set subprogram instance id to the df created for subprogram, so that
    // recursive call of subprogram cann be considered as call to subprogram instance (LRM 4.2.1 Note 1)
    subprog_df->SetSubprogInstanceId(subprog_inst) ;

    // RD: run all subprograms without an outer 'df' in 'initial' mode, or else initial values of variables inside could not be trusted.
    if (!df && IsStaticElab()) subprog_df->SetInInitial() ; // Viper #4079

    (void) caller_node ;

    VhdlIdDef *prev_formal_id = 0 ;
    Set partial_formals(POINTER_HASH) ;

    VhdlScope *local_scope = LocalScope() ;

    // Now create the values :
    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    VhdlConstraint *constraint ;
    unsigned i ;
    unsigned is_non_const_args = 0 ; // Flag to indicate non-constant arguments
    // VIPER #4369/4374/4397 : Maintain a set of formals for those actuals are provided :
    Set ids_with_nonconst_actual(POINTER_HASH) ;
    FOREACH_ARRAY_ITEM(arguments, i, assoc) {
        if (!assoc) continue ;
        // Find the formal.
        if (assoc->IsAssoc() ) {
            // Find the formal :
            id = assoc->FindFullFormal() ;
            if (!id) {
                // Partial association....FIX ME
#if 0
                // Viper #3041: Turned off static elab non-const marking..
                if (IsStaticElab()) { // Don't give error in static elaboration here. may be procedure call inside process
                    subprog_df->SetNonconstExpr() ; // Non constant args, can't evaluate in static elaboration
                    is_non_const_args = 1 ; // Can't proceed further
                    continue ;
                }
#endif

                // Viper #3041: Partial association support...
                // assoc->Error("partial association in subprogram calls not supported") ;
                // break ;

                VhdlIdDef *prefix_id = assoc->FindFormal() ;
                if (!prefix_id) continue ; // something badly wrong

                // Take the existing constraint/value out, the core APIs depend
                // on the updated value on the identifiers
                VhdlConstraint *old_constraint = prefix_id->TakeConstraint() ;
                VhdlValue *old_value = prefix_id->TakeValue() ;

                // prefix_id could be from the subprogram spec in package header
                // obtain local_id from the subprogram body
                VhdlIdDef *local_id = (local_scope && !local_scope->IsDeclaredHere(prefix_id)) ? local_scope->FindSingleObjectLocal(prefix_id->Name()) : prefix_id ;

                // Obtain the value and constraint of this formal from the map
                // Set them on the identifiers
                VhdlConstraint *formal_constraint = (VhdlConstraint *)formal_to_constraint.GetValue(local_id) ;
                VhdlValue *formal_value = (VhdlValue *)formal_to_value.GetValue(local_id) ;
                prefix_id->SetConstraint(formal_constraint) ;
                prefix_id->SetValue(formal_value) ;

                // The core APIs for partial formal support
                // If formal is not constrained, set proper constraint to it
                VhdlNode::EvaluateConstraintForPartialAssoc(arguments, assoc, 0) ;
                assoc->PartialAssociation(0, df) ;

                // Restore the old value/constraints. Constraint would be present
                // in the formal_to_constraint map already. Value would be created
                // for the first partial formal for the full identifier, and inserted
                // in the map for the first time.
                (void) prefix_id->TakeConstraint() ;
                formal_value = prefix_id->TakeValue() ;
                if (!formal_to_value.GetValue(local_id)) (void) formal_to_value.Insert(local_id, formal_value) ;

                prefix_id->SetConstraint(old_constraint) ;
                prefix_id->SetValue(old_value) ;

                // Viper 4395 : We issue error if the formal is already associated
                // previously but not in the one before this
                // Viper 6055: The prefix_id must be an interface-id of subprogram
                if (prev_formal_id != prefix_id && partial_formals.Get(prefix_id)) {
                    assoc->Error("formal %s associated individually was not in contiguous sequence", prefix_id->Name()) ;
                }
                prev_formal_id = prefix_id ;
                (void) partial_formals.Insert(prefix_id, 0) ;
                continue ;
            } else if (!subprog_inst) {
                // Check if the interface-id in argument is from the package header
                // If so, find the one from the subprogram body
                VhdlIdDef *local_id = (local_scope && !local_scope->IsDeclaredHere(id)) ? local_scope->FindSingleObjectLocal(id->Name()) : 0 ;
                if (local_id) id = local_id ;
            }
        } else {
            // positional association
            id = subprog->Arg(i) ;
        }
        if (!id) continue ; // type-inference checks failed

        // Check if association is explicitly 'open' :
        if (assoc->IsOpen()) {
            // Viper# 3470 : It is illegal to keep file type formal open
            if (id->IsFile()) assoc->Error("formal parameter of type file must be associated with a file object") ;
            continue ; // Any other 'open' association will remain unassociated. Found while testing fix for 4354.
        }

        // Pick up the constraint to use :
        constraint = (VhdlConstraint*)formal_to_constraint.GetValue(id) ;

        // VIPER 5937 and 7081 : Catch user-defined functions with unconstrained formal and actual with an aggregate that has an 'others' choice.
        if (constraint && constraint->IsUnconstrained() && assoc->IsOthersAggregate()) {
            assoc->Error("OTHERS is illegal aggregate choice for unconstrained target") ;
        }

        // Create the actual value
        // We need to do this for all parameter modes (also out/inout) to get formal constraint adjusted if needed..
        val = assoc->Evaluate(constraint,df,0) ;
        if (!val && IsStaticElab()) {
            subprog_df->SetNonconstExpr() ; // Non constant args, can't evaluate in static elaboration
            is_non_const_args = 1 ; // Set non-constant argument
            // VIPER #2858 : Check the size mismatch between formal and actual
            if (constraint && !constraint->IsUnconstrained()) { // Check for uncontrained formals
                VhdlConstraint *actual_constraint = assoc->EvaluateConstraintInternal(df, 0) ;
                if (actual_constraint && !actual_constraint->IsUnconstrained()) {
                    (void) constraint->CheckAgainst(actual_constraint, assoc, df) ;
                }
                delete actual_constraint ;
            }
            // VIPER #7033 : Do not set 'left to id, when actual is non-cnstant
            // This will create illegal value even in declarative region. (x - 'left_of_integer)
            // is out of range value
            // VIPER #4369/4374/4397 : Initial value will be 'left, as non-constant
            // actual is provided for this formal:
            //if (!id->IsOutput() && constraint) {
                //val = constraint->CreateInitialValue(id) ;
                //(void) ids_with_nonconst_actual.Insert(id) ;
            //}
        }

        // Viper# 3430 : LRM Section 2.1.1.2 check
        if (id->IsSignal() && constraint && !constraint->IsUnconstrained()) {
            VhdlConstraint *actual_constraint = assoc->EvaluateConstraintInternal(df, 0) ;
            if (actual_constraint && !actual_constraint->IsUnconstrained()) {
                if (constraint->IsRangeConstraint() && actual_constraint->IsRangeConstraint()) {
                    VhdlValue *actual_low = actual_constraint->Low() ;
                    VhdlValue *actual_high = actual_constraint->High() ;

                    // A less strict check than what LRM says.. For modelsim compliance
                    if ((constraint->Dir() != actual_constraint->Dir()) || !constraint->Contains(actual_low) || !constraint->Contains(actual_high)) {
                        assoc->Error("direction/bounds for signal formal %s must match with that of actual type for subprogram %s", id->Name(), subprog->Name()) ;
                    }
                }
            }
            delete actual_constraint ;
        }
        if (!val) continue ;

        // Adjust constraint if needed.
        if (constraint && constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

        // Check actual against constraint
        if (id->IsOutput()) {
            // INOUT and INPUT both should pass the actual value to the formal.
            // But OUTPUT is different :

            // Issue 2187 :
            // For variable parameters 'output', we need to pass the constraint'left value as initial value
            // (LRM 2.1.1.1 states 'values' are transferred).
            // Do not use the value passed in from actual. That was needed only for 'constrainting' the constraint above.
            if (id->IsVariable() && constraint) {
                delete val ; // For 'output' parameter: ignore the value passed in from actual.
                val = constraint->CreateInitialValue(id) ;
            }

            // For signals, since a signal assignment should pass the condition
            // of assignment (the 'driver' condition), we have to use the actual value,
            // even for OUT signal parameters (LRM 2.1.1.2 states parameters passed by 'reference')
        } else {
            // Check constraint for this value going from actual to formal.
            // This is for INOUT and IN parameters only (issue 1387).
            if (!val->CheckAgainst(constraint,assoc, df)) {
                delete val ;
                continue ;
            }
        }

        // Set the value :

        (void) formal_to_value.Insert(id, val) ;

        // LRM 2.1.1.2 : Signals are passed by 'reference'. This means that if no assignment happens
        // to a signal formal during a subprogram call, the actual remains unchanged.
        // For 'out' and 'inout', the "driver" of the formal is associated with the "driver" of the actual.
        // So signals are like an alias, while formal/actual variables are clearly two different objects.
        //
        // We now set the actual as the _ref_expr of the formal. This now behaves like the alias declaration
        // any assignment / evaluation of the formal will result in assignment/evaluation of the actual.
        // This change is strategy was needed to support assignment of actuals inside procedure for Viper 6871

        // Viper 7599: Allow reference passing for concurrent area also.
        if (id->IsSignal() && !id->IsInput()) {
#if 0
            // With the new stategy by using ref target this code is obselete
            val = assoc->Evaluate(constraint, df, 1/*force returning the driving value*/) ;
            if (!val) continue ;
            // Set this as the starting value in the dataflow :
            subprog_df->SetAssignValue(id, val) ;
#endif

            // Viper 6871 and 6989: Set the reference target for output formal
            // passed by reference. This is treated like the alias declaration
            // when ever the formal is assigned or evaluated the corresponding
            // assoc is assigned or evaluated.
            VhdlDiscreteRange *ref_target = id->GetRefTarget() ;
            // Store the previous ref target here in Map. To be restored at the end
            (void) formal_to_actual.Insert(id, ref_target) ;
            // If the ref_target is not null then it is a recursive call
            // of the procedure. We have already stored the incoming target
            // in formal_to_actual. We have to cleverly update the target
            // for formals in recursive calls there can be different scenarios.
            // Scenario 1: We should always update the target even if it is a recursive call
            //   signal tmp_o : bit_vector (3 downto 0) ;
            //   signal temp : bit_vector(0 to 1) ;
            //   procedure recur_test1(i: in integer ; signal full: inout bit) is
            //   begin
            //      recur_test(i-1,temp) ;
            //   end;
            //   recur_test(5,tmp_o); --call to the procedure
            // here for the recursive call the incoming ref target is tmp_o and the
            // current ref target is temp which are different signal.
            // Scenario 2: Recursion (direct and indirect)
            // Direct :
            //   procedure recur_test2(i: in integer ; signal full: inout bit) is
            //   begin
            //      recur_test(i-1,full) ;
            //   end;
            // Indirect:
            //   procedure recur_test3(i: in integer ; signal full_1, full_2: inout bit) is
            //   begin
            //      recur_test(i-1,full_2, full_1) ;
            //   end;
            // The present strategy wont work for partial association of formals. For these
            // we take a bailout strategy i.e. revert back to the previous method of creating
            // values: However using values has its limitation if same formal is passed as actual
            // to an inner recursive call with different indices. Then the value of one will overwrite
            // the others value for ex:
            // procedure recur_test4(i: in integer ; signal full : inout bit_vector (3 downto 0) ;
            //                                     signal fbit: inout bit ) is
            // begin
            //  if (i/=0) then
            //      recur_test(i-1,full, full(1)) ;
            //  else
            //      fbit <= '1' ;
            //  end if;
            // end;
            // A new flag _is_ref_formal is maintained in the VhdlInterfaceId. This is because
            // for bail-out assocs we need to remember that the ref_target should not be evaluated
            // if we SetRefTarget to zero then in the next level of recursive call we do not have
            // any information whether the call is recursive or a first call of the subprogram.

            if (!id->GetRefTarget()) {
                id->SetRefTarget(assoc) ;
                // For non recursive procedure calls just set the assoc and restore it before leaving(Viper 7017)
                id->SetRefFormal(1) ;
            } else {
                VhdlDiscreteRange *actual = assoc->ActualPart() ;
                VhdlIdDef *full_actual_id = actual ? actual->FindFullFormal() : 0 ;
                VhdlDiscreteRange *ref_actual = full_actual_id ? (VhdlDiscreteRange*)formal_to_actual.GetValue(full_actual_id) : 0 ;
                if (!ref_actual && full_actual_id) ref_actual = full_actual_id->GetRefTarget() ;

                if (ref_actual) {
                    id->SetRefTarget(ref_actual) ;
                    id->SetRefFormal(1) ;
                } else {
                    // Bailout
                    // This is ONLY for 'signal', output mode formals. 'variable' outputs work with driving value any way.
                    // The driving value is not needed for 'in' mode ports, and also not
                    // for if there were certainly no assignments to the actual (if there is no dataflow).
                    val = assoc->Evaluate(constraint, df, 1/*force returning the driving value*/) ;
                    if (!val) continue ;
                    // Set this as the starting value in the dataflow :
                    subprog_df->SetAssignValue(id, val) ;
                    // This id should be evaluated using the value and not ref target
                    id->SetRefFormal(0) ;
                }
            }
        }
    }

    unsigned non_constant_recursion = 0 ;
    // Viper Issue #8411 : Moved this check down here after arguments are processed.
    // This is done to distinguish between recursion and nesting
    if (IsStaticElab() && df && df->IsNonconstExpr()) {
        // We are in non-constant region. Whenever we start processing a subprogram
        // body, we set a flag in subprogram identifier to mark that we are processing
        // this identifier. In non-constant region we do proces all branches of if/case
        // statement, so it is possible to go in an infinite recursion. So before entering
        // subprogram's body we are testing whether we have come here via this
        // subprogram. If yes, return
        if (subprog->IsProcessing()) non_constant_recursion = 1 ;
        subprog->SetIsProcessing(1) ; // Mark as processing to catch recursion
    }

    // Push all locally declared identifier values/constraints on a stack.
    // All existing values/constraints will be reset (to 0).
    if (non_constant_recursion || (local_scope && !local_scope->PushStack())) {
        // Something went badly wrong (maybe stack overflow!)
        // Delete all contraints before returning
        MapIter mi ;
        FOREACH_MAP_ITEM(&formal_to_constraint, mi, 0, &constraint) delete constraint ;
        FOREACH_MAP_ITEM(&generic_to_constraint, mi, 0, &constraint) delete constraint ;
        // Delete all values before returning
        FOREACH_MAP_ITEM(&formal_to_value, mi, 0, &val) delete val ;
        FOREACH_MAP_ITEM(&generic_to_value, mi, 0, &val) delete val ;
        // Delete dataflow
        delete subprog_df ;
        if (IsStaticElab() && !non_constant_recursion) subprog->SetIsProcessing(0) ; // Mark that processing of this subprogram is over
        return 0 ;
    }

    // Viper #8210, 8219
    if (!subprog_df->IsInInitial() && !is_non_const_args && IsStaticElab() && local_scope && !local_scope->HasWaitStatement() && !local_scope->HasImpureAccess() && !_subprogram_spec->IsImpure()) subprog_df->SetForcedInitial() ;

    MapIter mi ;

    // VIPER #5051 : Remember current path stack and create new for subprogram
    Array *old_path_name = _path_name ;
    Array *old_instance_name = _instance_name ;
    // Create path stack before pushing scope of subprogram
    CreateSubprogramPathName(subprog) ;
    VhdlScope *old_scope = _present_scope ;
    _present_scope = local_scope ;

    // Now push the generic values/constraints into the id
    FOREACH_MAP_ITEM(&generic_to_constraint, mi, &id, &constraint) {
        id->SetConstraint(constraint) ;
    }
    FOREACH_MAP_ITEM(&generic_to_value, mi, &id, &val) {
        id->SetValue(val) ;
    }
    VhdlIdDef *actual_id ;
    FOREACH_MAP_ITEM(&generic_to_id, mi, &id, &actual_id) {
        if (!id) continue ;
        if (id->IsPackage()) {
            _present_scope = old_scope ; // Context of instantiating unit
            id->SetElaboratedPackageId(actual_id) ;
            _present_scope = local_scope ;
        } else {
            id->SetActualId(actual_id, 0) ;
        }
    }
    // Now push the formal values/constraints into the id from the formals.
    FOREACH_MAP_ITEM(&formal_to_constraint, mi, &id, &constraint) {
        id->SetConstraint(constraint) ;
    }

    FOREACH_MAP_ITEM(&formal_to_value, mi, &id, &val) {
        id->SetValue(val) ;
    }

    // Check if all formals have a value now, fill in 'open' associations
    // This should go fine, since Analysis checks it pretty properly.
    _subprogram_spec->InitialValues(subprog_df, old_scope) ;

    // Now enter the body.

    // Push subprogram name on the name stack (for variable naming)
    PushNamedPath(subprog->OrigName()) ;

    // Now execute declarations
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        decl->Elaborate(subprog_df) ; // pass in dataflow, to indicate that we are in a subprogram decl area
    }
    // VIPER #4369/4374/4397 : Reset the values created for formals having non constant
    // actual. These values should only use its 'left value in declarative region :
    SetIter si ;
    FOREACH_SET_ITEM(&ids_with_nonconst_actual, si, &id) {
        if (id) id->SetValue(0) ;
    }

    if (IsStaticElab()) {
        // Viper #6064: Return for non-const + unconstrained argument in
        // static elaboration ONLY AFTER declarations are all elaborated
        // to set the static constraints
        FOREACH_MAP_ITEM(&formal_to_constraint, mi, &id, &constraint) {
            if (constraint && constraint->IsUnconstrained() && is_non_const_args) {
                // Formal is still unconstrained, can't proceed further
                // VIPER #4497 : Mark the coming dataflow non-constant if this subprogram dataflow is non-constant
                if (df && subprog_df->IsNonconstExpr()) df->SetNonconstExpr() ; // Propagate non const expr flag upwards
                delete subprog_df ;
                if (local_scope) (void) local_scope->PopStack() ;
                subprog->SetIsProcessing(0) ; // Mark that processing of this subprogram is over
                PopNamedPath() ;
                ClearAttributeSpecificPath() ;
                _path_name = old_path_name ;
                _instance_name = old_instance_name ;
                _present_scope = old_scope ;

                return 0 ;
            }
        }
    }

    //if (is_non_const_args) subprog_df->SetNonconstExpr() ;

    // And execute the statements
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        stmt->Elaborate(subprog_df,0) ;
    }

    // Pop subprogram name off the name stack
    PopNamedPath() ;
    ClearAttributeSpecificPath() ;
    // Return to current path stack :
    _path_name = old_path_name ;
    _instance_name = old_instance_name ;
    _present_scope = old_scope ;

    // Check : if its a function call, the dataflow area should
    // now be 'dead' (the function should always return).
    // Do this before we resolve the goto's of the return statement.
    if (subprog->IsFunction()) {
        if (!subprog_df->IsDeadCode()) {
            // This is an error in VHDL
            // Don't issue this message if we reported errors before. Its confusing.
            unsigned issue_error = 1 ;
            // Viper #4971 : functions in non-constant df should issue warning,
            // not error because under the function actuals, it may always return
            if (IsStaticElab() && subprog_df->IsNonconstExpr()) issue_error = 0 ;

            if (Message::ErrorCount()==0) {
                if (issue_error) {
                    Error("function %s does not always return a value", subprog->Name()) ;
                } else {
                    Warning("function %s does not always return a value", subprog->Name()) ;
                }
            }
        }
    }

    // The values in the present subprog_df are the 'others' values,
    // that will be used if no goto condition is active...
    // Resolve gotos to this subprogram end (RETURN statements from within the subprogram)
    subprog_df->Resolve(subprog, this) ;

    // The resolved return value and output param values are now in the dataflow subprog_df.
    // For functions, do only the output value (all parameters are IN)
    // For procedures, do only the parameters (there is no return value).
    VhdlValue *result = 0 ;
    if (subprog->IsProcedure()) {
        // Save the subdataflow formals in the formal_to_value map :
        FOREACH_MAP_ITEM(&formal_to_value, mi, &id, &val) {
            if (!id) continue ;
             // don't look at inputs. Only at outputs and inouts.
            if (id->IsInput()) continue ;
            val = (VhdlValue*)subprog_df->GetValue(id) ;
            // issue 2281 : revert to initial value if output was not assigned.
            // We need the id->Value() for that, so this is why we need to do this work before we pop the value stack..
            // Viper 7753: If the id is assigned a null value then we should update id with null value and not id->Value()
            // Viper 7868 : modified is assigned checking introduced for 7753 from checking id->IsAssigned() to
            // whether the module item containing the id is present in df. If the id is present in subprog_df
            // then the id was assigned. Now if the val is null that means Static elaboration could not evaluate
            // a non constant expression and hence returned NULL.

            // Hence our stategy is as follows: if the id is not assigned at all then keep its initial value
            // if it is assigned and the value is null then update the null value rather than the initial value.
            unsigned is_assigned = subprog_df->GetItem(id) ? 1 : 0 ;
            if (!val && id->Value() && !is_assigned) val = id->Value()->Copy() ;
            // force-overwrite this value into the 'formal_to_value' Map.
            (void) formal_to_value.Insert(id,val,1,0) ;
            // remove this identifier from dataflow, so that now the Map owns the value.
            (void) subprog_df->Remove(id) ;
        }
    } else {
        // Take the value from the dataflow (it will have been assigned).
        result = (VhdlValue*)subprog_df->GetValue(subprog) ;
        (void) subprog_df->Remove(subprog) ;

        // VIPER 4145 : Adjust the function's return constraint using the return value.
        // LRM 3.2.1.1 if unconstrained array type as return type, we need to extract it from the return value.
        constraint = subprog->Constraint() ;
        if (constraint && constraint->IsUnconstrained()) {
            constraint->ConstraintBy(result) ;
        }

        // LRM 3.2.1.1 Adjust target constraint to my constraint if needed (will only work for functions).
        if (target_constraint && target_constraint->IsUnconstrained()) {
            target_constraint->ConstraintBy(subprog->Constraint()) ;
        }
    }

    // Now pop the stack before we go back to assign to actuals :
    // swap old id values/constraints back into the id's.
    // Overwrite the values/constraints that we used in this subprogram.
    if (local_scope) (void) local_scope->PopStack() ;

    // The dataflow could contain assignments to external variables/signals.
    // LRM does not say what needs to happen with these.
    // But since ModelTech seems to resolve them as normal assignments, we better do it too.
    // Move them to the upper dataflow.
    // Issue 2281 : do this BEFORE we do the assignments to the output arguments.
    FOREACH_MAP_ITEM(subprog_df, mi, &id, &val) {
        // Don't do anything with values of local identifiers : these guys stop to exist out of this scope.
        if (!local_scope || local_scope->IsDeclaredHere(id)) continue ;
        // Don't do anything with the function variable itself either.
        if (id==subprog) continue ;

        // Here, a non-local identifier gets resolved.
        // Create an assignment to this id in the upper-level dataflow.
        // or wire it in if there is no upper dataflow (since then it must be concurrent area)

        // This is thus a regular assignment, similar to IdRef::Assign. Use the same code as there.
        // If there is dataflow, set the value in there.
        // Otherwise, wire the value into the initial value :
        // In that case, it's a real hard assignment
        if (df) {
#if 0 // Viper #5687
        if (df && (!df->IsInInitial() || df->IsInSubprogramOrProcess())) {
            // second condition for Viper #3841. For the condition identifier added to the
            // top df, if the df is initial_df for the architecture declarative region, no
            // one really iterates over those df to create final wire. So force a net-assign here.
#endif
            df->SetAssignValue(id, val) ;
        } else if (IsStaticElab() && (id->IsSignal() || id->IsVariable())) {
            // RSM: Viper #5730 : We still need this check when no df is present
            // RD: 12/19/2007: Check inserted to match baseline code, after Sandeep D. reported problem with parse-tree manipulated designs.
            // do not assign to a signal in static elaboration mode.
            // Check this late, so that all tests on the target name are done normal.
            // VIPER 3789/3795: do same for variables.
            // Note : we sometimes get here for generics, so only refuse for signal/variable
            delete val ;
        } else if (id->Value()) {
            id->Value()->NetAssign(val,id->ResolutionFunction(),this) ; // wire-in
        } else {
            // No initial value yet. Set it ? CHECK ME : Can this still happen ??
            id->SetValue(val) ;
        }

        // remove from the dataflow, because the value is now owned by the identifier.
        (void) subprog_df->Remove(id) ;
    }

    // Now process actual return values.
    // Get these from the subprogram dataflow.
    if (subprog->IsProcedure()) {
        // Move values from output parameters back into the actuals :
        FOREACH_ARRAY_ITEM(arguments, i, assoc) {
            if (!assoc) continue ;

            // Find the formal.
            if (assoc->IsAssoc() ) {
                // Find the formal :
                id = assoc->FindFullFormal() ;
                if (!id) {
                    // Viper #3041: Partial association....
                    // We already errored out before.
                    // break ;
                    VhdlIdDef *prefix_id = assoc->FindFormal() ;
                    VhdlName *assoc_formal = assoc->FormalPart() ;
                    if (!prefix_id || !assoc_formal || prefix_id->IsInput()) continue ;

                    // prefix_id could be from the subprogram spec in package header
                    // obtain local_id from the subprogram body
                    VhdlIdDef *local_id = (local_scope && !local_scope->IsDeclaredHere(prefix_id)) ? local_scope->FindSingleObjectLocal(prefix_id->Name()) : prefix_id ;
                    VhdlValue *formal_value = (VhdlValue *)formal_to_value.GetValue(local_id) ;
                    VhdlConstraint *formal_constraint = (VhdlConstraint *)formal_to_constraint.GetValue(local_id) ;

                    VhdlConstraint *old_constraint = prefix_id->TakeConstraint() ;
                    VhdlValue *old_value = prefix_id->TakeValue() ;

                    prefix_id->SetConstraint(formal_constraint) ;
                    prefix_id->SetValue(formal_value) ;

                    // Evaluate the part value. Full value is set on the id
                    VhdlValue *partial_value = assoc_formal->Evaluate(0, 0, 0) ;

                    (void) prefix_id->TakeConstraint() ;
                    (void) prefix_id->TakeValue() ;

                    prefix_id->SetConstraint(old_constraint) ;
                    prefix_id->SetValue(old_value) ;

                    if (partial_value) assoc->Assign(partial_value, df, 0) ;

                    continue ;
                } else if (!subprog_inst) {
                    // Check if the interface-id in argument is from the package header
                    // If so, find the one from the subprogram body
                    VhdlIdDef *local_id = (local_scope && !local_scope->IsDeclaredHere(id)) ? local_scope->FindSingleObjectLocal(id->Name()) : 0 ;
                    if (local_id) id = local_id ;
                }

            } else {
                // positional association
                id = subprog->Arg(i) ;
            }
            if (!id) continue ; // type-inference checks failed

            // Do only output/inout now
            if (id->IsInput()) continue ;
 
            // Take the value from the formals map (previously set from the data flow value)
            val = (VhdlValue*)formal_to_value.GetValue(id) ;

            // Viper 6871 and 6989: Restore to previous value

            if (id->IsRefFormal()) {
                VhdlDiscreteRange* ref_target = (VhdlDiscreteRange*)formal_to_actual.GetValue(id) ;
                // Viper 7273: set target back to its old value.
                id->SetRefTarget(ref_target) ;
                if (!ref_target) id->SetRefFormal(0) ; // Non Recursive.
                // For Recursive IsRefFormal remains true
                // delete the value as for other ids, val is absorbed in Assign() call below
                delete val ;
                continue ;
            }
            // Assign these back to the actual (in regular dataflow 'df')
            assoc->Assign(val, df, 0) ;
        }
    }

    if (IsStaticElab()) {
        if (subprog_df->IsNonconstExpr()) {
            if (df) df->SetNonconstExpr() ; // Propagate non const expr flag upwards
            // VIPER #7286 : Donot delete result, keep 'result' as it is
            //delete result ; // Delete result
            //result = 0 ; // Make result 0 to indicate non constant expression
        }
        // if (subprog_df->IsDeadCode() && df) df->SetDeadCode() ; // Propagate dead code flag upwards. RD: VIPER 3601 : Do NOT do this! Dead code in call does not mean dead code in upper flow.
//        if (subprog_df->IsWaiting() && df) df->SetIsWaiting() ; // VIPER #3538 : Propagate presence of wait statement upwards

        subprog->SetIsProcessing(0) ; // Mark as processing of this subprogram is over
    }

    // Check as per LRM Section 8.1 (around line 80). Viper #4518
// Check now done in analyser.
//    if (subprog->IsFunction() && subprog_df->IsWaiting()) Error("illegal use of wait statement inside a function or inside a procedure whose parent is a function") ;

    // Now delete the dataflow itself, with the (local variable) assignments in there that are useless now.
    delete subprog_df ;

    return result ; // Its copied
}
VhdlValue *VhdlSubprogInstantiationDecl::ElaborateSubprogramInst(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode * caller_node)
{
    // Elaborate the uninstantiated subprogram with generic map aspect
    VhdlIdDef *uninstantiated_subprog_id = GetUninstantiatedSubprogId() ;
    if (!uninstantiated_subprog_id) return 0 ; // nothing can be done

    VhdlValue *result = uninstantiated_subprog_id->ElaborateSubprogram(arguments, target_constraint, df, caller_node, GetId()) ;
    return result ;
}
VhdlValue *VhdlInterfaceSubprogDecl::ElaborateSubprogramInst(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode * caller_node)
{
    if (!_actual_subprog) return 0 ; // actual not set

    VhdlValue *result = _actual_subprog->ElaborateSubprogram(arguments, target_constraint, df, caller_node, GetId()) ;
    return result ;
}
void VhdlInterfacePackageDecl::SetElaboratedPackageId(VhdlIdDef *actual)
{
    if (!actual) {
        _actual_elab_pkg = 0 ;
        AssociateFormalActualElementIds(0) ;
        return ;
    }
    _actual_pkg = actual ;
    // Get and set elaboration specific package as actual package, as
    // when package instantiation is elaborated every time instantiated
    // package is copied and copied one is elaborated
    VhdlIdDef *elab_pkg = _actual_pkg->GetElaboratedPackageId() ;
    _actual_elab_pkg = elab_pkg ? elab_pkg : _actual_pkg ;
    // Check formal/actual mismatch only when package instance is
    // connected
    if (elab_pkg) CheckFormalActualMismatch(actual) ;
    AssociateFormalActualElementIds(_actual_elab_pkg) ;
}
VhdlIdDef *VhdlInterfacePackageDecl::GetElaboratedPackageId() const
{
    return _actual_elab_pkg ;
}
VhdlIdDef *VhdlInterfacePackageDecl::TakeElaboratedPackageId()
{
    VhdlIdDef *r = _actual_elab_pkg ;
    _actual_elab_pkg = 0 ;
    AssociateFormalActualElementIds(0) ;
    return r ;
}

void VhdlInterfacePackageDecl::CheckGenericMapAspectValueMismatch(const VhdlIdDef *actual_pkg) const
{
    if (!actual_pkg || !_id) return ;
    // If actual_pkg exits, get generic map aspect from that
    VhdlPrimaryUnit *pkg = actual_pkg->GetPrimaryUnit() ;
    Array *actual_genetic_map_aspect = pkg ? pkg->GetGenericMapAspect(): 0 ;

    if (!pkg) { // actual is interface package declaration
        VhdlDeclaration *decl = actual_pkg->GetDeclaration() ;
        actual_genetic_map_aspect = decl ? decl->GetGenericMapAspect(): 0 ;
    }
    unsigned actual_gen_map_aspect_created = 0 ;
    if (!actual_genetic_map_aspect && pkg) {
        // VIPER #7479 : Create generic map aspect from initial value of generics
        Array *actual_generic_clause = pkg->GetGenericClause() ;
        VhdlInterfaceDecl *decl ;
        unsigned i, j ;
        VhdlIdDef *id ;
        VhdlMapForCopy old2new ;
        FOREACH_ARRAY_ITEM(actual_generic_clause, i, decl) {
            if (!decl) continue ;
            Array *ids = decl->GetIds() ;
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (!id || (id->IsGenericType() || id->IsSubprogram() || id->IsPackage())) continue ;
                // Value generic
                VhdlExpression *init_val = decl->GetInitAssign() ;
                if (!init_val) continue ;
                if (!actual_genetic_map_aspect) actual_genetic_map_aspect = new Array() ;
                VhdlName *formal = new VhdlIdRef(id) ;
                formal->SetLinefile(init_val->Linefile()) ;
                VhdlDiscreteRange *assoc = new VhdlAssocElement(formal, init_val->CopyExpression(old2new)) ;
                assoc->SetLinefile(init_val->Linefile()) ;
                actual_genetic_map_aspect->InsertLast(assoc) ;
                actual_gen_map_aspect_created = 1 ;
            }
        }
    }
    if (!actual_genetic_map_aspect) return ;
    // VIPER #7479 : Check value of formal with actual
    Map actuals(POINTER_HASH) ;
    unsigned i ;
    VhdlIdDef *id ;
    VhdlScope *actual_scope = actual_pkg->LocalScope() ;
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(actual_genetic_map_aspect, i, assoc) {
        if (!assoc || assoc->IsBox() || assoc->IsDefault()) continue ;
        if (assoc->IsAssoc() ) {
            id = assoc->FindFormal() ;
        } else {
            id = _id->GetGenericAt(i) ;
            id = (actual_scope &&id) ? actual_scope->FindSingleObject(id->Name()) : 0 ;
        }
        if (id) (void) actuals.Insert(id, assoc, 0, 1 /* for partial assoc*/) ;
    }
    VhdlConstraint *constraint ;
    VhdlValue *actual_val = 0, *formal_val = 0 ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) {
        if (!assoc || assoc->IsBox() || assoc->IsDefault()) continue ;
        if (assoc->IsAssoc() ) {
            id = assoc->FindFormal() ;
        } else {
            id = _id->GetGenericAt(i) ;
        }
        if (!id || (id->IsGenericType() || id->IsSubprogram() || id->IsPackage())) continue ;
        constraint = id->Constraint() ;
        VhdlIdDef *actual_id = actual_scope ? actual_scope->FindSingleObject(id->Name()) : 0 ;
        VhdlDiscreteRange *actual = (VhdlDiscreteRange*)actuals.GetValue(actual_id) ;
        actual_val = actual ? actual->Evaluate(constraint, 0, 0): 0 ;
        if (!actual) {
            actual_val = actual_id ? actual_id->Value(): 0 ;
            if (actual_val) actual_val = actual_val->Copy() ;
        }
        formal_val = assoc->Evaluate(constraint, 0, 0) ;
        if (actual_val && formal_val && !actual_val->IsEqual(formal_val)) {
            assoc->Warning("actual values for generic %s do not match between package instantiation and interface package", id->Name()) ;
        }
        delete actual_val ;
        delete formal_val ;
    }
    if (actual_gen_map_aspect_created) {
        unsigned idx ;
        VhdlDiscreteRange *assoc_ele ;
        FOREACH_ARRAY_ITEM(actual_genetic_map_aspect, idx, assoc_ele) delete assoc_ele ;
        delete actual_genetic_map_aspect ;
    }
}

