/*
 *
 * [ File Version : 1.137 - 2014/03/19 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_STATEMENT_H_
#define _VERIFIC_VHDL_STATEMENT_H_

#include "VhdlTreeNode.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class BitArray ;
class VhdlName ;
class VhdlExpression ;
class VhdlScope ;
class VhdlIdDef ;
class VhdlIterScheme ;
class VhdlDelayMechanism ;
class VhdlOptions ;
class VhdlBlockGenerics ;
class VhdlBlockPorts ;
class VhdlDataFlow ;
class VhdlBlockConfiguration;
class VhdlMapForCopy ;

class VeriModule ; // pointer argument only

class VhdlVarUsageInfo ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlStatement : public VhdlTreeNode
{
protected: // Abstract class
    VhdlStatement() ;

    // Copy tree node
    VhdlStatement(const VhdlStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlStatement(SaveRestore &save_restore);

public:
    virtual ~VhdlStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlStatement(const VhdlStatement &) ;            // Purposely leave unimplemented
    VhdlStatement& operator=(const VhdlStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Internal routine for late tree addition
    virtual VhdlStatement *     AddElsifElse(VhdlStatement * /*elsif_else*/) { return 0 ; }
    virtual void                AddDecls(Array * /*decls*/) {}
    virtual void                TypeInferChoices(VhdlIdDef * /*case_type*/) {} // Works when this is case-generate-item
    virtual void                ClosingAlternativeLabel(VhdlDesignator *id) ;
    // Copy statement.
    // Copy all types of statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Deletes 'old_node', and puts new identifier in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/)                                    { return 0 ; } // Deletes 'old_node', and puts new expression in its place.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/)                                                { return 0 ; } // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildIterScheme(VhdlIterScheme * /*old_node*/, VhdlIterScheme * /*new_node*/)                              { return 0 ; } // Deletes 'old_node', and puts new iteration scheme in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement * /*old_node*/, VhdlStatement * /*new_node*/)                                      { return 0 ; } // Deletes 'old_node', and puts new statement in its place
    virtual unsigned            ReplaceChildCaseStmtAlt(VhdlCaseStatementAlternative * /*old_node*/, VhdlCaseStatementAlternative * /*new_node*/) { return 0 ; } // Deletes 'old_node', and puts new case statement alternative in its place
    virtual unsigned            ReplaceChildElsif(VhdlElsif * /*old_node*/, VhdlElsif * /*new_node*/)                                             { return 0 ; } // Deletes 'old_node', and puts new else-if in its place
    virtual unsigned            ReplaceChildDelayMechanism(VhdlDelayMechanism * /*old_node*/, VhdlDelayMechanism * /*new_node*/)                  { return 0 ; } // Deletes 'old_node', and puts new delay in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration * /*old_node*/, VhdlDeclaration * /*new_node*/)                                  { return 0 ; } // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildBlockGenerics(VhdlBlockGenerics * /*old_node*/, VhdlBlockGenerics * /*new_node*/)                     { return 0 ; } // Deletes 'old_node', and puts new block generics in its place
    virtual unsigned            ReplaceChildBlockPorts(VhdlBlockPorts * /*old_node*/, VhdlBlockPorts * /*new_node*/)                              { return 0 ; } // Deletes 'old_node', and puts new block ports in its place
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange * /*old_node*/, VhdlDiscreteRange * /*new_name*/)                     { return 0 ; } // Deletes 'old_node', and puts new generic map/port map aspect element in its place
    virtual unsigned            ReplaceChildOptions(VhdlOptions * /*old_node*/, VhdlOptions * /*new_name*/)                                       { return 0 ; } // Deletes 'old_node', and puts new options in its place
    virtual unsigned            ReplaceChildConditionalWaveform(VhdlConditionalWaveform * /*old_node*/, VhdlConditionalWaveform * /*new_name*/)   { return 0 ; } // Deletes 'old_node', and puts new conditional waveform in its place
    virtual unsigned            ReplaceChildSelectedWaveform(VhdlSelectedWaveform * /*old_node*/, VhdlSelectedWaveform * /*new_name*/)            { return 0 ; } // Deletes 'old_node', and puts new waveform in its place
    virtual unsigned            ReplaceChildSelectedExpression(VhdlSelectedExpression * /*old_node*/, VhdlSelectedExpression * /*new_name*/)      { return 0 ; } // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildBy(VhdlStatement * /*old*/, Array * /*new_stmts*/)                                                    { return 0 ; }
    static unsigned             ReplaceChildByInternal(Array *stmts, VhdlStatement *old_stmt, Array *new_stmts) ;
    virtual unsigned            InsertBeforeStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)                             { return 0 ; } // Insert new statement before 'target_node' in the array of statements.
    virtual unsigned            InsertAfterStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)                              { return 0 ; } // Insert new statement after 'target_node' in the array of statements.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)                         { return 0 ; } // Insert new declaration before 'target_node' in the declaration list.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)                          { return 0 ; } // Insert new declaration after 'target_node' in the declaration list.
    virtual unsigned            InsertBeforeElsif(const VhdlElsif * /*target_node*/, VhdlElsif * /*new_node*/)                                    { return 0 ; } // Insert new else-if before 'target_node' in the array of elsifs.
    virtual unsigned            InsertAfterElsif(const VhdlElsif * /*target_node*/, VhdlElsif * /*new_node*/)                                     { return 0 ; } // Insert new else-if after 'target_node' in the array of elsifs.
    virtual unsigned            InsertBeforeExpr(const VhdlExpression * /*target_node*/, VhdlExpression * /*new_node*/)                           { return 0 ; } // Insert new expression before 'target_node' in the array of expressions
    virtual unsigned            InsertAfterExpr(const VhdlExpression * /*target_node*/, VhdlExpression * /*new_node*/)                            { return 0 ; } // Insert new expression after 'target_node' in the array of expressions
    virtual unsigned            InsertBeforeName(const VhdlName * /*target_node*/, VhdlName * /*new_node*/)                                       { return 0 ; } // Insert new name before 'target_node' in the array of name (_sensitivity_clause)
    virtual unsigned            InsertAfterName(const VhdlName * /*target_node*/, VhdlName * /*new_node*/)                                        { return 0 ; } // Insert new name after 'target_node' in the array of name (_sensitivity_clause)
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)            { return 0 ; } // Insert new port/generic map aspect element before 'target_node' in the generic/port map aspect.
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)             { return 0 ; } // Insert new port/generic map aspect element after 'target_node' in the generic/port map aspect.
    virtual unsigned            InsertBeforeConditionalWaveform(const VhdlConditionalWaveform * /*target_node*/, VhdlConditionalWaveform * /*new_node*/)  { return 0 ; } // Insert new waveform before 'target_node' in the list of conditional waveforms.
    virtual unsigned            InsertAfterConditionalWaveform(const VhdlConditionalWaveform * /*target_node*/, VhdlConditionalWaveform * /*new_node*/)   { return 0 ; } // Insert new waveform after 'target_node' in the list of conditional waveforms.
    virtual unsigned            InsertBeforeSelectedWaveform(const VhdlSelectedWaveform * /*target_node*/, VhdlSelectedWaveform * /*new_node*/)           { return 0 ; } // Insert new waveform before 'target_node' in the list of selected waveforms.
    virtual unsigned            InsertAfterSelectedWaveform(const VhdlSelectedWaveform * /*target_node*/, VhdlSelectedWaveform * /*new_node*/)            { return 0 ; } // Insert new waveform after 'target_node' in the list of selected waveforms.
    virtual unsigned            InsertBeforeSelectedExpression(const VhdlSelectedExpression * /*target_node*/, VhdlSelectedExpression * /*new_node*/)           { return 0 ; } // Insert new waveform before 'target_node' in the list of selected waveforms.
    virtual unsigned            InsertAfterSelectedExpression(const VhdlSelectedExpression * /*target_node*/, VhdlSelectedExpression * /*new_node*/)            { return 0 ; } // Insert new waveform after 'target_node' in the list of selected waveforms.
    virtual unsigned            InsertBeforeCaseStmtAlt(const VhdlCaseStatementAlternative * /*target_node*/, VhdlCaseStatementAlternative * /*new_node*/){ return 0 ; } // Insert new node before 'target_node' in the array of alternatives.
    virtual unsigned            InsertAfterCaseStmtAlt(const VhdlCaseStatementAlternative * /*target_node*/, VhdlCaseStatementAlternative * /*new_node*/) { return 0 ; } // Insert new node after 'target_node' in the array of alternatives.

    virtual void                DeleteUnusedComSpec() {} // Delete configuration specification from decl part of block statement
    void                        SetLabelNull() { _label = 0 ; } // 6793
    virtual unsigned            IsElabCreatedEmpty() const      { return 0 ; } // VIPER #6793
    virtual unsigned            IsGenerateElabCreated() const   { return 0 ; }

    // VIPER #6467 : Associate instantiated unit_name, library_name and architecture name
    virtual const char *        GetInstantiatedUnitName() const            { return 0 ; }
    virtual const char *        GetLibraryOfInstantiatedUnit() const       { return 0 ; }
    virtual const char *        GetArchitectureOfInstantiatedUnit() const  { return 0 ; }

    void                        SetLabel(VhdlIdDef *label, unsigned postponed = 0) ;
    void                        SetImplicitLabel(VhdlIdDef *label) ; // VIPER #7494
    VhdlIdDef *                 GetLabel() const ;
    void                        ClosingLabel(VhdlDesignator *id) ; // Check closing label against statement label (if any). Absorbs/deletes 'id' because it is not stored in the parse tree.
    virtual VhdlIdDef *         GetInstantiatedUnit() const ; // For instantiation statements only
    virtual VhdlExpression *    GetActualExpression(VhdlIdDef* /*formal*/) const { return 0 ; } // only after assciateports/generics
    virtual VhdlName *          GetInstantiatedUnitNameNode() const         { return 0 ; } // Get the VhdlName tree node
    virtual Array *             GetGenericMapAspect() const                 { return 0 ; }
    virtual Array *             GetPortMapAspect() const                    { return 0 ; }

    virtual VhdlIterScheme *    GetScheme() const               { return 0 ; } // For generate_statement only
    virtual Array *             GetDeclPart() const             { return 0 ; }
    virtual Array *             GetStatementPart() const        { return 0 ; }
    virtual Array *             GetStatements() const           { return 0 ; }

    unsigned                    IsPostponed() const             { return _postponed ; }
    virtual unsigned            IsIfElsifGenerateStatement() const { return 0 ; } // Returns 1 for if-elsif-else generate statement
    virtual Array *             GetElsifList() const               { return 0 ; } // Return list of elsif statements (VhdlGenerateStatement*)
    virtual VhdlStatement *     GetElseStatement() const           { return 0 ; } // Return else part (VhdlGenerateStatement*)
    virtual unsigned            IsCaseGenerateStatement() const    { return 0 ; } // Returns 1 for case generate statement
    virtual Array *             GetAlternatives() const            { return 0 ; } // Returns the case generate items
    virtual unsigned            IsMatching() const                 { return 0 ; } // Returns 1 for matching-case/matching selected_signal_assign
    virtual unsigned            IsBlockStatement() const           { return 0 ; } // Returns 1 for block statement

    virtual VhdlDesignator *    GetClosingLabel() const ; // VIPER #6666
    virtual void                SetClosingLabel(VhdlDesignator *label) ; // VIPER #6666

    virtual unsigned            IsPslDirective() const              { return 0 ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config)=0 ;
    virtual unsigned            ElaborateElsifElse(VhdlIdDef * /*gen_label*/, VhdlBlockConfiguration * /*block_config*/, VhdlDataFlow * /*df*/) { return 0 ; } // For elsif/else branch elaboration of if-elsif-else generate
    static void                 AnalyzeFullParallel(const Array *item_vals, BitArray *done_columns, unsigned max_size, unsigned *is_parallel, unsigned *is_full) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlIdDef   *_label ;
    unsigned     _postponed ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    VhdlDesignator *_closing_label ; // VIPER #6666
#endif
} ; // class VhdlStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlNullStatement : public VhdlStatement
{
public:
    VhdlNullStatement() ;

    // Copy tree node
    VhdlNullStatement(const VhdlNullStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlNullStatement(SaveRestore &save_restore);

    virtual ~VhdlNullStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlNullStatement(const VhdlNullStatement &) ;            // Purposely leave unimplemented
    VhdlNullStatement& operator=(const VhdlNullStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLNULLSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy null statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
// Leaf node
} ; // class VhdlNullStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlReturnStatement : public VhdlStatement
{
public:
    explicit VhdlReturnStatement(VhdlExpression *expr, VhdlIdDef *owner=0) ;

    // Copy tree node
    VhdlReturnStatement(const VhdlReturnStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlReturnStatement(SaveRestore &save_restore);

    virtual ~VhdlReturnStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlReturnStatement() ;                                       // Purposely leave unimplemented
    VhdlReturnStatement(const VhdlReturnStatement &) ;            // Purposely leave unimplemented
    VhdlReturnStatement& operator=(const VhdlReturnStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLRETURNSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy return statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place.

    // Accessor methods
    VhdlExpression *            GetExpression() const       { return _expr ; }
    VhdlIdDef *                 GetOwner() const            { return _owner ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression *_expr ;
// (back) pointer set during analysis (not owned) :
    VhdlIdDef      *_owner ; // The subprogram that this return statement refers to
} ; // class VhdlReturnStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlExitStatement : public VhdlStatement
{
public:
    VhdlExitStatement(VhdlName *target, VhdlExpression *condition) ;

    // Copy tree node
    VhdlExitStatement(const VhdlExitStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlExitStatement(SaveRestore &save_restore);

    virtual ~VhdlExitStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlExitStatement() ;                                     // Purposely leave unimplemented
    VhdlExitStatement(const VhdlExitStatement &) ;            // Purposely leave unimplemented
    VhdlExitStatement& operator=(const VhdlExitStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLEXITSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy exit statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Accessor methods
    VhdlName *                  GetTarget() const           { return _target ; }
    VhdlExpression *            GetCondition() const        { return _condition ; }
    VhdlIdDef *                 GetTargetLabel() const      { return _target_label ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlName        *_target ;
    VhdlExpression  *_condition ;
// (back) pointer set during analysis (not owned) :
    VhdlIdDef       *_target_label ; // The label of the loop statement this exit applies to
} ; // class VhdlExitStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlNextStatement : public VhdlStatement
{
public:
    VhdlNextStatement(VhdlName *target, VhdlExpression *condition) ;

    // Copy tree node
    VhdlNextStatement(const VhdlNextStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlNextStatement(SaveRestore &save_restore);

    virtual ~VhdlNextStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlNextStatement() ;                                     // Purposely leave unimplemented
    VhdlNextStatement(const VhdlNextStatement &) ;            // Purposely leave unimplemented
    VhdlNextStatement& operator=(const VhdlNextStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLNEXTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy next statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Accessor methods
    VhdlName *                  GetTarget() const               { return _target ; }
    VhdlExpression *            GetCondition() const            { return _condition ; }
    VhdlIdDef *                 GetTargetLabel() const          { return _target_label ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlName        *_target ;
    VhdlExpression  *_condition ;
// (back) pointer set during analysis (not owned) :
    VhdlIdDef       *_target_label ; // The label of the loop statement this exit applies to
} ; // class VhdlNextStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlLoopStatement : public VhdlStatement
{
public:
    VhdlLoopStatement(VhdlIterScheme *iter_scheme, Array *statements, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlLoopStatement(const VhdlLoopStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlLoopStatement(SaveRestore &save_restore);

    virtual ~VhdlLoopStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlLoopStatement() ;                                     // Purposely leave unimplemented
    VhdlLoopStatement(const VhdlLoopStatement &) ;            // Purposely leave unimplemented
    VhdlLoopStatement& operator=(const VhdlLoopStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLLOOPSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy loop statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildIterScheme(VhdlIterScheme *old_node, VhdlIterScheme *new_node) ; // Deletes 'old_node', and puts new iteration scheme in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ; // Deletes 'old_node', and puts new statement in its place
    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement before 'target_node' in the array of statements.
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement after 'target_node' in the array of statements.

    // Accessor methods
    VhdlIterScheme *            GetIterScheme() const           { return _iter_scheme ; }
    virtual Array *             GetStatements() const           { return _statements ; }
    virtual VhdlScope *         LocalScope() const              { return _local_scope ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlIterScheme  *_iter_scheme ;
    Array           *_statements ;
    VhdlScope       *_local_scope ;
} ; // class VhdlLoopStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlCaseStatement : public VhdlStatement
{
public:
    VhdlCaseStatement(VhdlExpression *expr, Array *alternatives, unsigned is_matching_case = 0) ;

    // Copy tree node
    VhdlCaseStatement(const VhdlCaseStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlCaseStatement(SaveRestore &save_restore);

    virtual ~VhdlCaseStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlCaseStatement() ;                                     // Purposely leave unimplemented
    VhdlCaseStatement(const VhdlCaseStatement &) ;            // Purposely leave unimplemented
    VhdlCaseStatement& operator=(const VhdlCaseStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCASESTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy case statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildCaseStmtAlt(VhdlCaseStatementAlternative *old_node, VhdlCaseStatementAlternative *new_node) ; // Deletes 'old_node', and puts new case statement alternative in its place
    virtual unsigned            InsertBeforeCaseStmtAlt(const VhdlCaseStatementAlternative *target_node, VhdlCaseStatementAlternative *new_node) ; // Insert new node before 'target_node' in the array of alternatives.
    virtual unsigned            InsertAfterCaseStmtAlt(const VhdlCaseStatementAlternative *target_node, VhdlCaseStatementAlternative *new_node) ; // Insert new node after 'target_node' in the array of alternatives.

    // Accessor methods
    VhdlExpression *            GetExpression() const           { return _expr ; }
    Array *                     GetAlternatives() const         { return _alternatives ; }
    VhdlIdDef *                 GetExprType() const             { return _expr_type ; }
    virtual unsigned            IsMatching() const              { return _is_matching_case ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
    void                        CheckSanity(VhdlDataFlow *df) ;

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_expr ;
    Array           *_alternatives ;  // Array of VhdlCaseStatementAlternative*
    unsigned         _is_matching_case ; // 1 means matching case
// (back) pointer set during analysis (not owned) :
    VhdlIdDef       *_expr_type ; // type of expression. Set in type-inference
} ; // class VhdlCaseStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlIfStatement : public VhdlStatement
{
public:
    VhdlIfStatement(VhdlExpression *if_cond, Array *if_statements, Array *elsif_list, Array *else_statements) ;

    // Copy tree node
    VhdlIfStatement(const VhdlIfStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlIfStatement(SaveRestore &save_restore);

    virtual ~VhdlIfStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlIfStatement() ;                                   // Purposely leave unimplemented
    VhdlIfStatement(const VhdlIfStatement &) ;            // Purposely leave unimplemented
    VhdlIfStatement& operator=(const VhdlIfStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLIFSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy if statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ; // Deletes 'old_node', and puts new statement in its place
    virtual unsigned            ReplaceChildElsif(VhdlElsif *old_node, VhdlElsif *new_node) ; // Deletes 'old_node', and puts new else-if in its place
    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement before 'target_node' in the array of if/else-statements.
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement after 'target_node' in the array of if/else-statements.
    virtual unsigned            InsertBeforeElsif(const VhdlElsif *target_node, VhdlElsif *new_node) ; // Insert new else-if before 'target_node' in the array of elsifs.
    virtual unsigned            InsertAfterElsif(const VhdlElsif *target_node, VhdlElsif *new_node) ; // Insert new else-if after 'target_node' in the array of elsifs.

    // Accessor methods
    VhdlExpression *            GetIfCondition() const           { return _if_cond ; }
    Array *                     GetIfStatements() const          { return _if_statements ; }
    Array *                     GetElsifList() const             { return _elsif_list ; }
    Array *                     GetElseStatments() const         { return _else_statements ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
    void                        ElaborateCore(VhdlDataFlow *df) ; // Viper #5696: Eleborate is now a wrapper for variable slice loop under the compile flag. ElaborateCore is the original elaboration routine
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_if_cond ;
    Array           *_if_statements ;   // Array of VhdlStatement*
    Array           *_elsif_list ;      // Array of VhdlElsif*
    Array           *_else_statements ; // Array of VhdlStatement*
} ; // class VhdlIfStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlVariableAssignmentStatement : public VhdlStatement
{
public:
    VhdlVariableAssignmentStatement(VhdlExpression *target, VhdlExpression *value) ;

    // Copy tree node
    VhdlVariableAssignmentStatement(const VhdlVariableAssignmentStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlVariableAssignmentStatement(SaveRestore &save_restore);

    virtual ~VhdlVariableAssignmentStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlVariableAssignmentStatement() ;                                                   // Purposely leave unimplemented
    VhdlVariableAssignmentStatement(const VhdlVariableAssignmentStatement &) ;            // Purposely leave unimplemented
    VhdlVariableAssignmentStatement& operator=(const VhdlVariableAssignmentStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLVARIABLEASSIGNMENTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy variable assignment statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Accessor methods
    VhdlExpression *            GetTarget() const   { return _target ; }
    VhdlExpression *            GetValue() const    { return _value ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression *_target ;
    VhdlExpression *_value ;
} ; // class VhdlVariableAssignmentStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSelectedVariableAssignmentStatement : public VhdlStatement
{
public:
    VhdlSelectedVariableAssignmentStatement(VhdlExpression *expr, VhdlExpression *target, Array *selected_exprs, unsigned matching_select = 0) ;

    // Copy tree node
    VhdlSelectedVariableAssignmentStatement(const VhdlSelectedVariableAssignmentStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSelectedVariableAssignmentStatement(SaveRestore &save_restore);

    virtual ~VhdlSelectedVariableAssignmentStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlSelectedVariableAssignmentStatement() ;                                                // Purposely leave unimplemented
    VhdlSelectedVariableAssignmentStatement(const VhdlSelectedVariableAssignmentStatement &) ;            // Purposely leave unimplemented
    VhdlSelectedVariableAssignmentStatement& operator=(const VhdlSelectedVariableAssignmentStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSELECTEDVARIABLEASSIGNMENTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_name) ;                         // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildSelectedExpression(VhdlSelectedExpression *old_node, VhdlSelectedExpression *new_name) ; // Deletes 'old_node', and puts new waveform in its place

    virtual unsigned            InsertBeforeSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node) ; // Insert new waveform before 'target_node' in the list of selected waveforms.
    virtual unsigned            InsertAfterSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node) ; // Insert new waveform after 'target_node' in the list of selected waveforms.

    // Accessor methods
    VhdlExpression *            GetExpression() const           { return _expr ; }
    VhdlExpression *            GetTarget() const               { return _target ; }
    Array *                     GetSelectedExpressions() const  { return _selected_exprs ; }
    VhdlIdDef *                 GetExprType() const             { return _expr_type ; }
    virtual unsigned            IsMatching() const              { return _is_matching ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_expr ;      // The 'case' expression
    VhdlExpression  *_target ;
    Array           *_selected_exprs ; // Array of VhdlSelectedExpression*
    unsigned         _is_matching ; // Flag to indicate matching select
// (back) pointers set at analysis time (not owned) :
    VhdlIdDef       *_expr_type ; // The 'case' expression type
} ; // class VhdlSelectedVariableAssignmentStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConditionalVariableAssignmentStatement : public VhdlStatement
{
public:
    VhdlConditionalVariableAssignmentStatement(VhdlExpression *target, Array *conditional_expressions) ;

    // Copy tree node
    VhdlConditionalVariableAssignmentStatement(const VhdlConditionalVariableAssignmentStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConditionalVariableAssignmentStatement(SaveRestore &save_restore);

    virtual ~VhdlConditionalVariableAssignmentStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlConditionalVariableAssignmentStatement() ;                                                   // Purposely leave unimplemented
    VhdlConditionalVariableAssignmentStatement(const VhdlConditionalVariableAssignmentStatement &) ;            // Purposely leave unimplemented
    VhdlConditionalVariableAssignmentStatement& operator=(const VhdlConditionalVariableAssignmentStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONDITIONALVARIABLEASSIGNMENTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_name) ;                                 // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildConditionalExpression(VhdlConditionalExpression *old_node, VhdlConditionalExpression *new_name) ; // Deletes 'old_node', and puts new conditional waveform in its place

    virtual unsigned            InsertBeforeConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node) ; // Insert new waveform before 'target_node' in the list of conditional waveforms.
    virtual unsigned            InsertAfterConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node) ; // Insert new waveform after 'target_node' in the list of conditional waveforms.

    // Accessor methods
    VhdlExpression *            GetTarget() const                   { return _target ; }
    Array *                     GetConditionalExpressions() const   { return _conditional_expressions ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_target ;
    Array           *_conditional_expressions ; // Array of VhdlConditionalExpression*
} ; // class VhdlConditionalVariableAssignmentStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSignalAssignmentStatement : public VhdlStatement
{
public:
    VhdlSignalAssignmentStatement(VhdlExpression *target, VhdlDelayMechanism *delay_mechanism, Array *waveform) ;

    // Copy tree node
    VhdlSignalAssignmentStatement(const VhdlSignalAssignmentStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSignalAssignmentStatement(SaveRestore &save_restore);

    virtual ~VhdlSignalAssignmentStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlSignalAssignmentStatement() ;                                                 // Purposely leave unimplemented
    VhdlSignalAssignmentStatement(const VhdlSignalAssignmentStatement &) ;            // Purposely leave unimplemented
    VhdlSignalAssignmentStatement& operator=(const VhdlSignalAssignmentStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSIGNALASSIGNMENTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy signal assignment statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node) ; // Deletes 'old_node', and puts new delay in its place
    virtual unsigned            InsertBeforeExpr(const VhdlExpression *target_node, VhdlExpression *new_node) ; // Insert new expression before 'target_node' in the array of expressions
    virtual unsigned            InsertAfterExpr(const VhdlExpression *target_node, VhdlExpression *new_node) ; // Insert new expression after 'target_node' in the array of expressions

    // Accessor methods
    VhdlExpression *            GetTarget() const                   { return _target ; }
    VhdlDelayMechanism *        GetDelayMechanism() const           { return _delay_mechanism ; }
    Array *                     GetWaveform() const                 { return _waveform ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression      *_target ;
    VhdlDelayMechanism  *_delay_mechanism ;
    Array               *_waveform ; // Array of VhdlExpression*
} ; // class VhdlSignalAssignmentStatement

/* -------------------------------------------------------------- */

// VHDL-2008 : simple force assignment
class VFC_DLL_PORT VhdlForceAssignmentStatement : public VhdlStatement
{
public:
    VhdlForceAssignmentStatement(VhdlExpression *target, unsigned force_mode, VhdlExpression *expr) ;

    // Copy tree node
    VhdlForceAssignmentStatement(const VhdlForceAssignmentStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlForceAssignmentStatement(SaveRestore &save_restore);

    virtual ~VhdlForceAssignmentStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlForceAssignmentStatement() ;                                                 // Purposely leave unimplemented
    VhdlForceAssignmentStatement(const VhdlForceAssignmentStatement &) ;            // Purposely leave unimplemented
    VhdlForceAssignmentStatement& operator=(const VhdlForceAssignmentStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLFORCEASSIGNMENTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy signal assignment statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Accessor methods
    VhdlExpression *            GetTarget() const                   { return _target ; }
    unsigned                    GetForceMode() const                { return _force_mode ; }
    VhdlExpression *            GetExpression() const               { return _expr ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;

protected:
// Parse tree nodes (owned) :
    VhdlExpression      *_target ;
    unsigned             _force_mode ;
    VhdlExpression      *_expr ;
} ; // class VhdlForceAssignmentStatement

/* -------------------------------------------------------------- */

// VHDL-2008 : simple release assignment
class VFC_DLL_PORT VhdlReleaseAssignmentStatement : public VhdlStatement
{
public:
    VhdlReleaseAssignmentStatement(VhdlExpression *target, unsigned force_mode) ;

    // Copy tree node
    VhdlReleaseAssignmentStatement(const VhdlReleaseAssignmentStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlReleaseAssignmentStatement(SaveRestore &save_restore);

    virtual ~VhdlReleaseAssignmentStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlReleaseAssignmentStatement() ;                                                 // Purposely leave unimplemented
    VhdlReleaseAssignmentStatement(const VhdlReleaseAssignmentStatement &) ;            // Purposely leave unimplemented
    VhdlReleaseAssignmentStatement& operator=(const VhdlReleaseAssignmentStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLRELEASEASSIGNMENTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy signal assignment statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Accessor methods
    VhdlExpression *            GetTarget() const                   { return _target ; }
    unsigned                    GetForceMode() const                { return _force_mode ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;

protected:
// Parse tree nodes (owned) :
    VhdlExpression      *_target ;
    unsigned             _force_mode ;
} ; // class VhdlReleaseAssignmentStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlWaitStatement : public VhdlStatement
{
public:
    VhdlWaitStatement(Array *sensitivity_clause, VhdlExpression *condition_clause, VhdlExpression *timeout_clause) ;

    // Copy tree node
    VhdlWaitStatement(const VhdlWaitStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlWaitStatement(SaveRestore &save_restore);

    virtual ~VhdlWaitStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlWaitStatement() ;                                     // Purposely leave unimplemented
    VhdlWaitStatement(const VhdlWaitStatement &) ;            // Purposely leave unimplemented
    VhdlWaitStatement& operator=(const VhdlWaitStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLWAITSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy wait statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            InsertBeforeName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name before 'target_node' in the array of name (_sensitivity_clause)
    virtual unsigned            InsertAfterName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name after 'target_node' in the array of name (_sensitivity_clause)

    // Accessor methods
    Array *                     GetSensitivityClause() const        { return _sensitivity_clause ; }
    VhdlExpression *            UntilClause() const                 { return _condition_clause ; } // Pointer to the 'until' clause
    VhdlExpression *            GetTimeoutClause() const            { return _timeout_clause ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array           *_sensitivity_clause ; // Array of VhdlName*
    VhdlExpression  *_condition_clause ;
    VhdlExpression  *_timeout_clause ;
} ; // class VhdlWaitStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlReportStatement : public VhdlStatement
{
public:
    VhdlReportStatement(VhdlExpression *report, VhdlExpression *severity) ;

    // Copy tree node
    VhdlReportStatement(const VhdlReportStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlReportStatement(SaveRestore &save_restore);

    virtual ~VhdlReportStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlReportStatement() ;                                       // Purposely leave unimplemented
    VhdlReportStatement(const VhdlReportStatement &) ;            // Purposely leave unimplemented
    VhdlReportStatement& operator=(const VhdlReportStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLREPORTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy report statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Accessor methods
    VhdlExpression *            GetReport() const       { return _report ; }
    VhdlExpression *            GetSeverity() const     { return _severity ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression *_report ;
    VhdlExpression *_severity ;
} ; // class VhdlReportStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAssertionStatement : public VhdlStatement
{
public:
    VhdlAssertionStatement(VhdlExpression *condition, VhdlExpression *report, VhdlExpression *severity) ;

    // Copy tree node
    VhdlAssertionStatement(const VhdlAssertionStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAssertionStatement(SaveRestore &save_restore);

    virtual ~VhdlAssertionStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlAssertionStatement() ;                                          // Purposely leave unimplemented
    VhdlAssertionStatement(const VhdlAssertionStatement &) ;            // Purposely leave unimplemented
    VhdlAssertionStatement& operator=(const VhdlAssertionStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLASSERTIONSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy assertion statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Accessor methods
    VhdlExpression *            GetCondition() const    { return _condition ; }
    VhdlExpression *            GetReport() const       { return _report ; }
    VhdlExpression *            GetSeverity() const     { return _severity ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression *_condition ;
    VhdlExpression *_report ;
    VhdlExpression *_severity ;
} ; // class VhdlAssertionStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlProcessStatement : public VhdlStatement
{
public:
    VhdlProcessStatement(Array *sensitivity_list, Array *decl_part, Array *statement_part, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlProcessStatement(const VhdlProcessStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlProcessStatement(SaveRestore &save_restore);

    virtual ~VhdlProcessStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlProcessStatement() ;                                        // Purposely leave unimplemented
    VhdlProcessStatement(const VhdlProcessStatement &) ;            // Purposely leave unimplemented
    VhdlProcessStatement& operator=(const VhdlProcessStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPROCESSSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this entity statement part (LRM 1.1.3)
    virtual unsigned long       CalculateSignature() const ;

    // Copy process statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ;               // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ;     // Deletes 'old_node', and puts new statement in its place

    virtual unsigned            InsertBeforeName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name before 'target_node' in the sensitivity list.
    virtual unsigned            InsertAfterName(const VhdlName *target_node, VhdlName *new_node) ;  // Insert new name after 'target_node' in the sensitivity list.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node' in the declaration list.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;  // Insert new declaration after 'target_node' in the declaration list.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ;     // Insert new statement before 'target_node' in the statement list.
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ;      // Insert new statement after 'target_node' in the statement list.

    // Access methods
    Array *                     GetSensitivityList() const      { return _sensitivity_list ; }
    virtual Array *             GetDeclPart() const             { return _decl_part ; }
    Array *                     GetStatementPart() const        { return _statement_part ; }
    virtual VhdlScope *         LocalScope() const              { return _local_scope ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array       *_sensitivity_list ; // Array of VhdlName*
    Array       *_decl_part ;        // Array of VhdlDeclaration*
    Array       *_statement_part ;   // Array of VhdlStatement*
    VhdlScope   *_local_scope ;
} ; // class VhdlProcessStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlBlockStatement : public VhdlStatement
{
public:
    VhdlBlockStatement(VhdlExpression *guard, VhdlBlockGenerics *generics, VhdlBlockPorts *ports, Array *decl_part, Array *statements, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlBlockStatement(const VhdlBlockStatement &node, VhdlMapForCopy &old2new) ;

    // In generate elaboration we create block statement for each value of
    // loop index variable copying the declarations and statements within
    // for-gen. If we use existing constructor, decl->ResolveSpecs is called
    // unnecessarily. So to avoid those extra resolve function calls, we use
    // the following separate constructor.
    VhdlBlockStatement(Array *decl_part, Array *statements, VhdlScope *local_scope) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBlockStatement(SaveRestore &save_restore);

    virtual ~VhdlBlockStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlBlockStatement() ;                                      // Purposely leave unimplemented
    VhdlBlockStatement(const VhdlBlockStatement &) ;            // Purposely leave unimplemented
    VhdlBlockStatement& operator=(const VhdlBlockStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLBLOCKSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy block statement
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ;                // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildBlockGenerics(VhdlBlockGenerics *old_node, VhdlBlockGenerics *new_node) ; // Deletes 'old_node', and puts new block generics in its place
    virtual unsigned            ReplaceChildBlockPorts(VhdlBlockPorts *old_node, VhdlBlockPorts *new_node) ;          // Deletes 'old_node', and puts new block ports in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ;              // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ;                  // Deletes 'old_node', and puts new statement in its place
    virtual unsigned            ReplaceChildBy(VhdlStatement *old, Array *new_stmts) ;

    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node' in the declaration list.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration after 'target_node' in the declaration list.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement before 'target_node' in the statement list.
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement after 'target_node' in the statement list.

    // Parse-tree add/remove (VIPER #3003) :
    // Parse-tree port creation/deletion routines
    VhdlIdDef *                 AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication) ; // Add new port 'port_name' to primary unit
    unsigned                    RemovePort(const char *port_name) ; // Remove 'port_name' from port clause

    // Parse-tree generic creation/deletion routines
    VhdlIdDef *                 AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign) ; // Add generic 'generic_name' to primary unit
    unsigned                    RemoveGeneric(const char *generic_name) ; // Remove argument specific generic

    // Parse-tree signal creation/deletion routine
    VhdlIdDef *                 AddSignal(const char *signal_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign) ; // Create and add signal named 'signal_name'
    unsigned                    RemoveSignal(const char *signal_name) ; // Remove argument specific signal

    // Parse-tree port connection routines
    unsigned                    AddPortRef(const char *formal_port_name, VhdlExpression *actual) ; // Add port-ref to port map aspect of this block
    unsigned                    RemovePortRef(const char *formal_port_name) ; // Remove actual for formal 'formal_port_name' from port map aspect of this block statement

    // Parse-tree generic connection routines
    unsigned                    AddGenericRef(const char *formal_generic_name, VhdlExpression *actual) ; // Create and add generic reference in generic map aspect of this block
    unsigned                    RemoveGenericRef(const char *formal_generic_name) ; // Remove actual for formal 'formal_generic_name' from generic map aspect of this block statement

    // Parse-tree declaration addition routine
    void                        AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part
    void                        AddStatement(VhdlStatement *new_stmt) ; // Add argument specific statement to the statement part

    virtual unsigned            IsBlockStatement() const        { return 1 ; } // Returns 1 for block statement
    // Accessor methods
    VhdlExpression *            GetGuard() const                { return _guard ; }
    VhdlBlockGenerics *         GetGenerics() const             { return _generics ; }
    VhdlBlockPorts *            GetPorts() const                { return _ports ; }
    virtual Array *             GetDeclPart() const             { return _decl_part ; }
    virtual Array *             GetStatements() const           { return _statements ; }
    virtual VhdlScope *         LocalScope() const              { return _local_scope ; }
    VhdlIdDef *                 GetGuardId() const              { return _guard_id ; }

    // VIPER #7752: Returns the linefile of "begin" keyword or the empty space in there.
    // Note that it returns an *allocated* linefile, so avoid unnecessary calling.
    // At the same time, you do not need to delete the linefile either!
    // It only works under linefile-column mode, if it is not enabled, returns 0.
    linefile_type               GetDeclAndStatementSeparatorLinefile() const ;

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif
    void                        SetStaticElaborated()           { _is_static_elaborated = 1 ; }
// CARBON_BEGIN
    void                        ClearStaticElaborated()         { _is_static_elaborated = 0;  }
// CARBON_END
    unsigned                    IsStaticElaborated() const      { return _is_static_elaborated ; }
    void                        SetIsElabCreatedEmpty()         { _is_elab_created_empty = 1 ; }
    virtual unsigned            IsElabCreatedEmpty() const      { return _is_elab_created_empty ; } // VIPER #6793
    virtual void                DeleteUnusedComSpec() ; // Delete configuration specification from decl part of block statement
    void                        SetGenerateElabCreated()        { _is_gen_elab_created = 1 ; }
    virtual unsigned            IsGenerateElabCreated() const   { return _is_gen_elab_created ; }

protected:
// Parse tree nodes (owned) :
    VhdlExpression      *_guard ;
    VhdlBlockGenerics   *_generics ;
    VhdlBlockPorts      *_ports ;
    Array               *_decl_part ;   // Array of VhdlDeclaration*
    Array               *_statements ;  // Array of VhdlStatement*
    VhdlScope           *_local_scope ;
// Set at analysis time (actually owned, since it's declared here (if there is a guard expression))
    VhdlIdDef           *_guard_id ;    // Declared signal 'guard'
    unsigned             _is_static_elaborated:1 ; // Flag to indicate that this is already elaborated
    unsigned             _is_elab_created_empty:1 ; // Flag to indicate that this is generate elab created empty block(#6793)
    unsigned             _is_gen_elab_created:1 ; // Flag to indicate that this is generated from generate elaboration
} ; // class VhdlBlockStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlGenerateStatement : public VhdlStatement
{
public:
    VhdlGenerateStatement(VhdlIterScheme *scheme, Array *decl_part, Array *statement_part, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlGenerateStatement(const VhdlGenerateStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlGenerateStatement(SaveRestore &save_restore);

    virtual ~VhdlGenerateStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlGenerateStatement() ;                                         // Purposely leave unimplemented
    VhdlGenerateStatement(const VhdlGenerateStatement &) ;            // Purposely leave unimplemented
    VhdlGenerateStatement& operator=(const VhdlGenerateStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLGENERATESTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Internal routine for late tree addition
    virtual VhdlStatement *     AddElsifElse(VhdlStatement *elsif_else) ;
    virtual void                AddDecls(Array *decls) ;

    // Copy generate statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildIterScheme(VhdlIterScheme *old_node, VhdlIterScheme *new_node) ; // Deletes 'old_node', and puts new iteration scheme in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ;     // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ;         // Deletes 'old_node', and puts new statement in its place

    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node' in the declaration list.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration after 'target_node' in the declaration list.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement before 'target_node' in the statement list.
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement after 'target_node' in the statement list.

    // Static Elaboration internal routines.
    virtual void                StaticElaborate(VhdlBlockConfiguration *block_config, VhdlDataFlow *df) ; // Unroll loops, create block statement for each iteration of loop index variable and insert those in its statement list.
    static void                 ReplaceByBlocks(VhdlScope *container, const Map *new_ids, Array *new_stmts, VhdlStatement *gen_stmt) ;

    // Accessor methods
    virtual VhdlIterScheme *    GetScheme() const               { return _scheme ; }
    virtual Array *             GetDeclPart() const             { return _decl_part ; }
    virtual Array *             GetStatementPart() const        { return _statement_part ; }
    virtual VhdlScope *         LocalScope() const              { return _local_scope ; }

    // VIPER #7752: Returns the linefile of "begin" keyword or the empty space in there.
    // Note that it returns an *allocated* linefile, so avoid unnecessary calling.
    // At the same time, you do not need to delete the linefile either!
    // It only works under linefile-column mode, if it is not enabled, returns 0.
    linefile_type               GetDeclAndStatementSeparatorLinefile() const ;

    // Type infer routine
    virtual void                TypeInferChoices(VhdlIdDef *case_type) ; // Works when this is case-generate-item
    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
    virtual unsigned            ElaborateElsifElse(VhdlIdDef *gen_label, VhdlBlockConfiguration *block_config, VhdlDataFlow *df) ; // For elsif/else branch elaboration of if-elsif-else generate
    void                        ElaborateAlternative(VhdlIdDef *stmt_label, VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlIterScheme  *_scheme ;
    Array           *_decl_part ;      // Array of VhdlDeclaration*
    Array           *_statement_part ; // Array of VhdlStatement*
    VhdlScope       *_local_scope ;
} ; // class VhdlGenerateStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlComponentInstantiationStatement : public VhdlStatement
{
public:
    VhdlComponentInstantiationStatement(VhdlName *instantiated_unit, Array *generic_map_aspect, Array *port_map_aspect) ;

    // Copy tree node
    VhdlComponentInstantiationStatement(const VhdlComponentInstantiationStatement &node, VhdlMapForCopy &old2new) ;

    // Specific constructor without port/generic map aspect: for parse tree modification
    explicit VhdlComponentInstantiationStatement(VhdlName *instantiated_unit) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlComponentInstantiationStatement(SaveRestore &save_restore);

    virtual ~VhdlComponentInstantiationStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlComponentInstantiationStatement() ;                                                       // Purposely leave unimplemented
    VhdlComponentInstantiationStatement(const VhdlComponentInstantiationStatement &) ;            // Purposely leave unimplemented
    VhdlComponentInstantiationStatement& operator=(const VhdlComponentInstantiationStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy component instantiation statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_name) ;                            // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_name) ; // Deletes 'old_node', and puts new generic map/port map aspect element in its place
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new port/generic map aspect element before 'target_node' in the generic/port map aspect.
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new port/generic map aspect element after 'target_node' in the generic/port map aspect.

    // Parse-tree instance port connection routine
    void                        AddPortRef(VhdlDiscreteRange *assoc) ; // Add argument specific port-ref to port map aspect

    // Parse-tree instance generic connection routine
    void                        AddGenericRef(VhdlDiscreteRange *assoc) ; // Add argument specific generic-ref to generic map aspect

    // Static Elaboration
    void                        StaticElaborate(Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component, const char *arch_name, VhdlPrimaryUnit *actual_master) ;

    // Create port map aspect for entity instantiation
    void                        CreatePortAssociation(const Array *actual_ports, const VhdlIdDef *actual_component, const VhdlIdDef *actual_unit=0) ;
    Array*                      CreatePortAssociationArray(const Array *actual_ports, const VhdlIdDef *actual_component, const VhdlIdDef *actual_unit=0) const ;
    Array*                      CreateGenericAssociationArray(const Array *actual_generics, const VhdlIdDef *actual_component, const VhdlIdDef *actual_unit=0) const ;
    void                        CreateGenericAssociation() ;
    void                        StaticElaborateBlackBox() ; // VIPER #6281 : Uniquify black-boxes

    // VIPER #6467 : Associate instantiated unit_name, library_name and architecture name
    virtual const char *        GetInstantiatedUnitName() const            { return _instantiated_unit_name ; }
    virtual const char *        GetLibraryOfInstantiatedUnit() const       { return _library_name ; }
    virtual const char *        GetArchitectureOfInstantiatedUnit() const  { return _arch_name ; }

    // Accessor methods
    virtual VhdlName *          GetInstantiatedUnitNameNode() const         { return _instantiated_unit ; } // Get the VhdlName tree node
    virtual Array *             GetGenericMapAspect() const                 { return _generic_map_aspect ; }
    virtual Array *             GetPortMapAspect() const                    { return _port_map_aspect ; }
    virtual VhdlIdDef *         GetInstantiatedUnit() const ; // The instantiated unit
    virtual VhdlExpression *    GetActualExpression(VhdlIdDef* formal) const ; // only after assciateports/generics

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif
private:
    // internal routines to create generic/port binding from primary/secondary binding lists
    Array *                     CreateActualGenericBinding(VhdlPrimaryUnit const *prim_unit, VeriModule const *verilog_module, const Array *primary_generic_binding, const Array *incremental_generic_binding) const ;
    Array *                     CreateActualPortBinding(VhdlPrimaryUnit const *prim_unit, VeriModule const *verilog_module, const Array *primary_port_binding, const Array *incremental_port_binding) const ;
public:
    // last one for ModelSim complience (VIPER #2441, 2457, 2584, 2590, 3653, 3269)
    Array *                     CreateActualComponentGenericBinding(const Array *generic_map_aspect, const Array *generic_binding) const ;

protected:
// Parse tree nodes (owned) :
    VhdlName    *_instantiated_unit ;
    Array       *_generic_map_aspect ;  // Array of VhdlDiscreteRange*
    Array       *_port_map_aspect ;     // Array of VhdlDiscreteRange*
// (back) pointer set at analysis time (to the resolved 'instantiated unit' :
    VhdlIdDef   *_unit ;                // resolved at type-inference time : component, entity, configuration (or procedure without arguments)
    // VIPER #6467 : Associate information of instantiated unit during elaboration
    // The below three fields are populated for default for elaboration. Needed for resolving external names of VHDL 2008
    char        *_library_name ; // name of instantiated unit's library
    char        *_instantiated_unit_name ; // name of instantiated entity (it will be same as _unit->Name())
    char        *_arch_name ; // name of instantiated architecture
} ; // class VhdlComponentInstantiationStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlProcedureCallStatement : public VhdlStatement
{
public:
    explicit VhdlProcedureCallStatement(VhdlName *call) ;

    // Copy tree node
    VhdlProcedureCallStatement(const VhdlProcedureCallStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlProcedureCallStatement(SaveRestore &save_restore);

    virtual ~VhdlProcedureCallStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlProcedureCallStatement() ;                                              // Purposely leave unimplemented
    VhdlProcedureCallStatement(const VhdlProcedureCallStatement &) ;            // Purposely leave unimplemented
    VhdlProcedureCallStatement& operator=(const VhdlProcedureCallStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPROCEDURECALLSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_name) ; // Deletes 'old_node', and puts new name in its place

    // Accessor method
    VhdlName *                  GetCall() const   { return _call ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlName *_call ; // procedure call itself is a VhdlIndexedName..
} ; // class VhdlProcedureCallStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConditionalSignalAssignment : public VhdlStatement
{
public:
    VhdlConditionalSignalAssignment(VhdlExpression *target, VhdlOptions *options, Array *conditional_waveforms, VhdlDelayMechanism *delay_mechanism = 0) ;

    // Copy tree node
    VhdlConditionalSignalAssignment(const VhdlConditionalSignalAssignment &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConditionalSignalAssignment(SaveRestore &save_restore);

    virtual ~VhdlConditionalSignalAssignment() ;

private:
    // Prevent compiler from defining the following
    VhdlConditionalSignalAssignment() ;                                                   // Purposely leave unimplemented
    VhdlConditionalSignalAssignment(const VhdlConditionalSignalAssignment &) ;            // Purposely leave unimplemented
    VhdlConditionalSignalAssignment& operator=(const VhdlConditionalSignalAssignment &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONDITIONALSIGNALASSIGNMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_name) ;                                  // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildOptions(VhdlOptions *old_node, VhdlOptions *new_name) ;                                     // Deletes 'old_node', and puts new options in its place
    virtual unsigned            ReplaceChildConditionalWaveform(VhdlConditionalWaveform *old_node, VhdlConditionalWaveform *new_name) ; // Deletes 'old_node', and puts new conditional waveform in its place
    virtual unsigned            ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node) ;                // Deletes 'old_node', and puts new delay in its place

    virtual unsigned            InsertBeforeConditionalWaveform(const VhdlConditionalWaveform *target_node, VhdlConditionalWaveform *new_node) ; // Insert new waveform before 'target_node' in the list of conditional waveforms.
    virtual unsigned            InsertAfterConditionalWaveform(const VhdlConditionalWaveform *target_node, VhdlConditionalWaveform *new_node) ; // Insert new waveform after 'target_node' in the list of conditional waveforms.

    // Accessor methods
    VhdlExpression *            GetTarget() const                   { return _target ; }
    VhdlOptions *               GetOptions() const                  { return _options ; }
    Array *                     GetConditionalWaveforms() const     { return _conditional_waveforms ; }
    VhdlIdDef *                 GetGuardId() const                  { return _guard_id ; }
    VhdlDelayMechanism *        GetDelayMechanism() const           { return _delay_mechanism ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_target ;
    VhdlOptions     *_options ;
    VhdlDelayMechanism *_delay_mechanism ;
    Array           *_conditional_waveforms ; // Array of VhdlConditionalWaveform*
// Set at analysis time (not owned)
    VhdlIdDef       *_guard_id ; // Pointer to the guard signal (set if 'guarded')
} ; // class VhdlConditionalSignalAssignment

/* -------------------------------------------------------------- */

// VHDL-2008 : conditional force assignment :
// target <= force [ force_mode] conditional_expressions ;
class VFC_DLL_PORT VhdlConditionalForceAssignmentStatement : public VhdlStatement
{
public:
    VhdlConditionalForceAssignmentStatement(VhdlExpression *target, unsigned force_mode, Array *conditional_expressions) ;

    // Copy tree node
    VhdlConditionalForceAssignmentStatement(const VhdlConditionalForceAssignmentStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConditionalForceAssignmentStatement(SaveRestore &save_restore);

    virtual ~VhdlConditionalForceAssignmentStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlConditionalForceAssignmentStatement() ;                                                   // Purposely leave unimplemented
    VhdlConditionalForceAssignmentStatement(const VhdlConditionalForceAssignmentStatement &) ;            // Purposely leave unimplemented
    VhdlConditionalForceAssignmentStatement& operator=(const VhdlConditionalForceAssignmentStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONDITIONALFORCEASSIGNMENTSTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_name) ;                                 // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildConditionalExpression(VhdlConditionalExpression *old_node, VhdlConditionalExpression *new_node) ; // Deletes 'old_node', and puts new conditional waveform in its place

    virtual unsigned            InsertBeforeConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node) ; // Insert new waveform before 'target_node' in the list of conditional waveforms.
    virtual unsigned            InsertAfterConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node) ; // Insert new waveform after 'target_node' in the list of conditional waveforms.

    // Accessor methods
    VhdlExpression *            GetTarget() const                   { return _target ; }
    unsigned                    GetForceMode() const                { return _force_mode ; }
    Array *                     GetConditionalExpressions() const   { return _conditional_expressions ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_target ;
    unsigned         _force_mode ; // VHDL_in or VHDL_out
    Array           *_conditional_expressions ; // Array of VhdlConditionalExpression*
} ; // class VhdlConditionalForceAssignmentStatement
/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSelectedSignalAssignment : public VhdlStatement
{
public:
    VhdlSelectedSignalAssignment(VhdlExpression *expr, VhdlExpression *target, VhdlOptions *options, Array *selected_waveforms, VhdlDelayMechanism *delay_mechanism = 0, unsigned is_matching = 0) ;

    // Copy tree node
    VhdlSelectedSignalAssignment(const VhdlSelectedSignalAssignment &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSelectedSignalAssignment(SaveRestore &save_restore);

    virtual ~VhdlSelectedSignalAssignment() ;

private:
    // Prevent compiler from defining the following
    VhdlSelectedSignalAssignment() ;                                                // Purposely leave unimplemented
    VhdlSelectedSignalAssignment(const VhdlSelectedSignalAssignment &) ;            // Purposely leave unimplemented
    VhdlSelectedSignalAssignment& operator=(const VhdlSelectedSignalAssignment &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSELECTEDSIGNALASSIGNMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_name) ;                         // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildOptions(VhdlOptions *old_node, VhdlOptions *new_name) ;                            // Deletes 'old_node', and puts new options in its place
    virtual unsigned            ReplaceChildSelectedWaveform(VhdlSelectedWaveform *old_node, VhdlSelectedWaveform *new_name) ; // Deletes 'old_node', and puts new waveform in its place
    virtual unsigned            ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node) ;       // Deletes 'old_node', and puts new delay in its place

    virtual unsigned            InsertBeforeSelectedWaveform(const VhdlSelectedWaveform *target_node, VhdlSelectedWaveform *new_node) ; // Insert new waveform before 'target_node' in the list of selected waveforms.
    virtual unsigned            InsertAfterSelectedWaveform(const VhdlSelectedWaveform *target_node, VhdlSelectedWaveform *new_node) ; // Insert new waveform after 'target_node' in the list of selected waveforms.

    // Accessor methods
    VhdlExpression *            GetExpression() const           { return _expr ; }
    VhdlExpression *            GetTarget() const               { return _target ; }
    VhdlOptions *               GetOptions() const              { return _options ; }
    VhdlDelayMechanism *        GetDelayMechanism() const       { return _delay_mechanism ; }
    Array *                     GetSelectedWaveforms() const    { return _selected_waveforms ; }
    VhdlIdDef *                 GetExprType() const             { return _expr_type ; }
    VhdlIdDef *                 GetGuardId() const              { return _guard_id ; }
    virtual unsigned            IsMatching() const              { return _is_matching ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
    void                        CheckSanity(VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_expr ;      // The 'case' expression
    VhdlExpression  *_target ;
    VhdlOptions     *_options ;
    VhdlDelayMechanism  *_delay_mechanism ;
    Array           *_selected_waveforms ; // Array of VhdlSelectedWaveform*
    unsigned         _is_matching ; // for matching selection (VHDL-2008)
// (back) pointers set at analysis time (not owned) :
    VhdlIdDef       *_expr_type ; // The 'case' expression type
    VhdlIdDef       *_guard_id ;  // Pointer to the guard signal (set if 'guarded')
} ; // class VhdlSelectedSignalAssignment

/* -------------------------------------------------------------- */

/*
  Class to represent VHDL-2008 specified if-elsif-else generate statement
*/
class VFC_DLL_PORT VhdlIfElsifGenerateStatement : public VhdlGenerateStatement
{
public:
    VhdlIfElsifGenerateStatement(VhdlIterScheme *scheme, Array *decl_part, Array *statement_part, VhdlScope *local_scope, Array *elsif_list, VhdlStatement *else_stmt) ;

    // Copy tree node
    VhdlIfElsifGenerateStatement(const VhdlIfElsifGenerateStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlIfElsifGenerateStatement(SaveRestore &save_restore);

    virtual ~VhdlIfElsifGenerateStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlIfElsifGenerateStatement() ;                                                // Purposely leave unimplemented
    VhdlIfElsifGenerateStatement(const VhdlIfElsifGenerateStatement &) ;            // Purposely leave unimplemented
    VhdlIfElsifGenerateStatement& operator=(const VhdlIfElsifGenerateStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLIFELSIFGENERATESTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Internal routine for late tree addition
    virtual VhdlStatement *     AddElsifElse(VhdlStatement *elsif_else) ;
    virtual void                ClosingAlternativeLabel(VhdlDesignator *id) ;

    // Copy generate statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_stmt, VhdlStatement *new_stmt) ; // Deletes 'old_stmt, and puts new statement in its place.

    // Static Elaboration.
    virtual void                StaticElaborate(VhdlBlockConfiguration *block_config, VhdlDataFlow *df) ; // Unroll loops, create block statement for each iteration of loop index variable and insert those in its statement list.

    virtual unsigned            IsIfElsifGenerateStatement() const { return 1 ; }
    // Accessor methods
    virtual Array *             GetElsifList() const      { return _elsif_list ; } // Return list of elsif statements (VhdlGenerateStatement*)
    virtual VhdlStatement *     GetElseStatement() const  { return _else_stmt ; } // Return else part (VhdlGenerateStatement*)

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array            *_elsif_list ; // Array of VhdlGenerateStatement* (each element is one elseif statement)
    VhdlStatement    *_else_stmt ;  // Else statement (VhdlGenerateStatement*)
} ; // class VhdlIfElsifGenerateStatement

/* -------------------------------------------------------------- */

/*
  Class to represent VHDL-2008 specified case generate statement
*/
class VFC_DLL_PORT VhdlCaseGenerateStatement : public VhdlStatement
{
public:
    VhdlCaseGenerateStatement(VhdlExpression *expr, Array *alternatives) ;

    // Copy tree node
    VhdlCaseGenerateStatement(const VhdlCaseGenerateStatement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlCaseGenerateStatement(SaveRestore &save_restore);

    virtual ~VhdlCaseGenerateStatement() ;

private:
    // Prevent compiler from defining the following
    VhdlCaseGenerateStatement() ;                                             // Purposely leave unimplemented
    VhdlCaseGenerateStatement(const VhdlCaseGenerateStatement &) ;            // Purposely leave unimplemented
    VhdlCaseGenerateStatement& operator=(const VhdlCaseGenerateStatement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCASEGENERATESTATEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;

    // Copy generate statement.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place.
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_stmt, VhdlStatement *new_stmt) ; // Deletes 'old_stmt, and puts new statement in its place.

    // Static Elaboration (Internal routines).
    void                        StaticElaborateActiveAlternative(const VhdlGenerateStatement *active_alt, VhdlIdDef *stmt_label, VhdlDataFlow *df, const VhdlBlockConfiguration *block_config) ;

    virtual unsigned            IsCaseGenerateStatement() const { return 1 ; } // Returns 1 for case generate statement
    // Accessor methods
    VhdlExpression *            GetExpression() const           { return _expr ; }
    virtual Array *             GetAlternatives() const         { return _alternatives ; }
    VhdlIdDef *                 GetExprType() const             { return _expr_type ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression *_expr ; // Case condition
    Array          *_alternatives ; // Array of case-generate-alternatives (VhdlGenerateStatement*)
    VhdlIdDef      *_expr_type ; //  Type of expression. Set in type-inference
} ; // class VhdlCaseGenerateStatement

/* -------------------------------------------------------------- */

// VIPER #8152 : Selected force assignment
class VFC_DLL_PORT VhdlSelectedForceAssignment : public VhdlStatement
{
public:
    VhdlSelectedForceAssignment(VhdlExpression *expr, unsigned is_matching, VhdlExpression *target, unsigned force_mode, Array *selected_exprs) ;

    // Copy tree node
    VhdlSelectedForceAssignment(const VhdlSelectedForceAssignment &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSelectedForceAssignment(SaveRestore &save_restore);

    virtual ~VhdlSelectedForceAssignment() ;

private:
    // Prevent compiler from defining the following
    VhdlSelectedForceAssignment() ;                                                // Purposely leave unimplemented
    VhdlSelectedForceAssignment(const VhdlSelectedForceAssignment &) ;            // Purposely leave unimplemented
    VhdlSelectedForceAssignment& operator=(const VhdlSelectedForceAssignment &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSELECTEDFORCEASSIGNMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this statement
    virtual unsigned long       CalculateSignature() const ;
    // Copy parse-tree.
    virtual VhdlStatement *     CopyStatement(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_name) ;                         // Deletes 'old_node', and puts new expression in its place
    virtual unsigned            ReplaceChildSelectedExpression(VhdlSelectedExpression *old_node, VhdlSelectedExpression *new_name) ; // Deletes 'old_node', and puts new waveform in its place

    virtual unsigned            InsertBeforeSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node) ; // Insert new waveform before 'target_node' in the list of selected waveforms.
    virtual unsigned            InsertAfterSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node) ; // Insert new waveform after 'target_node' in the list of selected waveforms.

    // Accessor methods
    VhdlExpression *            GetExpression() const           { return _expr ; }
    VhdlExpression *            GetTarget() const               { return _target ; }
    unsigned                    GetForceMode() const            { return _force_mode ; }
    virtual unsigned            IsMatching() const              { return _is_matching ; }
    Array *                     GetSelectedExpressions() const  { return _selected_exprs ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() {}
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_expr ;      // The 'case' expression
    VhdlExpression  *_target ;
    unsigned         _is_matching ; // for matching selection (VHDL-2008)
    unsigned         _force_mode ;   // VHDL_in or VHDL_out
    Array           *_selected_exprs ; // Array of VhdlSelectedExpression*
// (back) pointers set at analysis time (not owned) :
    VhdlIdDef       *_expr_type ; // The 'case' expression type
} ; // class VhdlSelectedForceAssignment

/* -------------------------------------------------------------- */
#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_STATEMENT_H_

