/*
 *
 * [ File Version : 1.167 - 2014/03/04 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "vhdl_file.h"

#include "Strings.h"
#include "Message.h"
#include "Set.h"
#include "Protect.h"
//CARBON_BEGIN
#include "Map.h"
//CARBON_END

#include "../verilog/veri_file.h"
#include "../verilog/VeriModule.h"
#include "../verilog/VeriLibrary.h"
#include "../verilog/VeriTreeNode.h"

#include "VhdlUnits.h"
#include "VhdlScope.h"
#include "VhdlIdDef.h"
#include "VhdlName.h"
#include "VhdlValue_Elab.h"

#ifdef VHDL_FLEX_READ_FROM_STREAM
#include <VerificStream.h> // For reading from streams
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/************************ Static variables ******************/
// RD: 6/2012: Many run-time variables moved to utility "RuntimeFlags".

// Static members
unsigned  vhdl_file::_analysis_mode = VHDL_93 ; // Vhdl  93 is default

// All VhdlLibraries :
Map *vhdl_file::_all_libraries = 0 ; //new Map(STRING_HASH) ;

// The VhdlLibrary that is the (last set) work library
VhdlLibrary *vhdl_file::_work_lib = 0 ;

// Callback functions, registered by user :
unsigned (*vhdl_file::_interrupt_func)(linefile_type) = 0 ;
unsigned (*vhdl_file::_compile_as_blackbox_func)(const char *) = 0 ;

// Time resolution specific fields
int vhdl_file::_scale_factor_of_timeunit = 1 ;
char *vhdl_file::_time_unit = 0 ;

// Viper #6212: begin
// RD: 2/2011: Set runtime flags on by default, so that compile flags are by default effective
// RD: 6/2012: Many run-time variables moved to utility "Global".
// /*static*/ unsigned vhdl_file::_extract_mp_rams = 1 ; // Default on
// /*static*/ unsigned vhdl_file::_extract_rams = 1 ; // Default on
// Viper #6212: end

Set *vhdl_file::_ordered_libs = 0 ; // -L options :

// VIPER : 2947/4152 - This variable, if set by user, will
// RD: 6/2012: Many run-time variables moved to utility "Global".
// control the loop limit during elaboration.
// unsigned vhdl_file::_loop_limit = 0 ;

// VIPER #7125: This variable, if set by user, will control the stack limit during elaboration.
// unsigned vhdl_file::_max_stack_depth = 0 ;

// Viper #4950 : dynamic control for error message : array size is
// larger than 2**_max_array_size (VHDL-1525)
// unsigned vhdl_file::_max_array_size = 23 ; // default value similar to verilog

//CARBON_BEGIN
// Arrays that store user-given pragma trigger names :
Array *vhdl_file::_pragma_triggers = 0 ;
//CARBON_END

// VIPER #1777, 1781, 2023 and 3570: VHDL library aliasing:
Map *vhdl_file::_library_aliases = 0 ;

const char* vhdl_file::_top_unit_name = 0 ; // Vhdl 2008: top unit name if specified.

// unsigned vhdl_file::_minimum_ram_size = 0 ; // Viper #3903
unsigned vhdl_file::_ignore_dead_assignment = 0 ; // Viper #5700
Protect *vhdl_file::_protect = 0 ; // for protection of VDB
Protect *vhdl_file::_tick_protect = 0 ; // `protect envelope in the RTL LRM 1076_2008 Section 24.1

unsigned vhdl_file::_prune_logic_for_integer_constraint = 0 ; // Viper #6473

#ifdef VHDL_FLEX_READ_FROM_STREAM
// VIPER #5726: Read from streams:
verific_stream *(*vhdl_file::_stream_callback_func)(const char *file_name) = 0 ;
#endif

// VIPER #6576: Stores the newly parsed expression parsed by VHDL expression parser:
VhdlExpression *vhdl_file::_parsed_expr = 0 ;

VhdlPrimaryUnit *vhdl_file::_top_unit = 0 ;

/************************ General Read Command(s) ******************/

unsigned
vhdl_file::Analyze(const char *file_name, const char *lib_name, unsigned vhdl_mode)
{
    if (!file_name) return 0 ; // nothing to analyze.

    // Analyze the named file and store all units in there in parse-tree form
    // in the work library (of the _all_libraries root).

    // If lib_name is not set, store in library "work"
    if (!lib_name) lib_name = "work" ;

    // Set Vhdl 93 or Vhdl 87 mode
    if (vhdl_mode==vhdl_file::VHDL_87) {
        vhdl_file::SetVhdl87() ;
    } else if (vhdl_mode==vhdl_file::VHDL_2K) {
        vhdl_file::SetVhdl2K() ;
    } else if (vhdl_mode==vhdl_file::VHDL_2008) {
        vhdl_file::SetVhdl2008() ;
    } else {
        vhdl_file::SetVhdl93() ;
    }

    // Clear errors from previous run.
    Message::ClearErrorCount() ;

    // Find/Set the work library where design units will be compiled into.
    _work_lib = GetLibrary(lib_name,1) ;

    // Start up the big engines :
    if (!StartFlex(file_name)) {
        return 0 ;
    }

    // Viper: 3142 - all verific msg should have msg-id
    // Message::PrintLine("Analyzing VHDL file ",file_name) ;
    VhdlTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
    tmp_print_node.Comment("Analyzing VHDL file %s", file_name) ;

    StartYacc() ;
    (void) Parse() ;
    EndYacc() ;

    (void) EndFlex() ;

    if (Message::ErrorCount()) {
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("VHDL file ", file_name, " ignored due to errors") ;
        tmp_print_node.Comment("VHDL file %s ignored due to errors", file_name) ;

        // Units with an error will have been deleted already
        return 0 ; // Error occurred during parsing
    }
    //GenerateExplicitStateMachines() ;

    return 1 ;
}

// VIPER #6576: Support for expression parser in VHDL side:
VhdlExpression *
vhdl_file::AnalyzeExpr(const char *expr, unsigned vhdl_mode /*=VHDL_93*/, const linefile_type line_file /*=0*/)
{
    // Analyze the given string 'expr' and return the equivalent expression:
    //
    // Set _parsed_expr to zero.
    _parsed_expr = 0 ;

    unsigned old_analysis_mode = _analysis_mode ; // Save the current analysis mode
    // Save the _present_scope, parsing may change it
    VhdlScope *save_scope = VhdlTreeNode::_present_scope ;

    // Set Vhdl 93 or Vhdl 87 mode
    if (vhdl_mode==vhdl_file::VHDL_87) {
        vhdl_file::SetVhdl87() ;
    } else if (vhdl_mode==vhdl_file::VHDL_2K) {
        vhdl_file::SetVhdl2K() ;
    } else if (vhdl_mode==vhdl_file::VHDL_2008) {
        vhdl_file::SetVhdl2008() ;
    } else {
        vhdl_file::SetVhdl93() ;
    }

    // Do not clean the error count, instead store the count in a variable:
    // Clear errors from previous run.
    //Message::ClearErrorCount() ;
    unsigned error_count = Message::ErrorCount() ;

    // Start up the big engines :
    if (!StartFlexExpr(expr, line_file)) {
        _analysis_mode = old_analysis_mode ; // Restore the previous analysis mode
        VhdlTreeNode::_present_scope = save_scope ; // Restore the previous global scope
        return 0 ; // Error occurred with flex
    }

    StartYacc() ;
    (void) Parse() ;
    EndYacc() ;

    (void) EndFlex() ;

    _analysis_mode = old_analysis_mode ; // Restore the previous analysis mode
    VhdlTreeNode::_present_scope = save_scope ; // Restore the previous global scope

    if (Message::ErrorCount() > error_count) {
        VhdlTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        tmp_print_node.Comment("VHDL expression ignored due to errors") ;
        return 0 ; // Error occurred during parsing
    }

    return _parsed_expr ; // This is set from within Parse()
}

const char *
vhdl_file::TopUnit(const char *lib_name)
{
    if (!lib_name) lib_name = "work" ;

    // Get the library where to find the (top) unit :
    _work_lib = GetLibrary(lib_name, 0) ;
    if (!_work_lib) return 0 ; // Library doesn't exist

    // Return the last unit compiled (analyzed).
    // That's most simulators' default. LRM is not clear about what's the default top-level.
    MapIter mi ;
    VhdlPrimaryUnit *unit ;
    FOREACH_MAP_ITEM_BACK(_work_lib->AllPrimaryUnits(), mi, 0, &unit) {
        // if (unit->IsElaborated()) continue ;
        // Static elaboration copies instantiated entities and add those at the
        // end of primary unit list. So those instantiated units should not be
        // considered as top unit.
        if (unit->IsVerilogModule() || unit->IsStaticElaborated()) continue ;
        if (unit->Id()->IsEntity() || unit->Id()->IsConfiguration()) {
            return unit->Name() ;
        }
    }

    return 0 ; // Primary unit not found
}

unsigned
vhdl_file::RemoveUnit(const char *lib_name, const char *unit_name)
{
    if (!lib_name || !unit_name) return 0 ;

    VhdlLibrary *lib = GetLibrary(lib_name, 0) ; // Get Library
    if (!lib) return 0 ; // Library doesn't exist

    VhdlPrimaryUnit *unit = lib->GetPrimUnit(unit_name) ; // Get primary unit
    if (unit) {
        delete unit ;
        return 1 ;
    }

    return 0 ;
}

/*static*/ void
vhdl_file::RemoveAllUnits(unsigned packages_too)
{
    // Delete all units in all libraries

    // Because the order of libraries are not always guaranteed to be
    // in order of dependency, we're going to temporarily going to
    // ignore the "re-analyze unit" warning message while deleting
    // the units. (VIPER #1951)
    msg_type_t org_type = Message::GetMessageType("VHDL-1201") ;
    (void) Message::SetMessageType("VHDL-1201", VERIFIC_IGNORE);

    // Go BACK, to delete the latest libraries first, and the
    // first library (std) last. This reduces the amount of
    // out-of-date warnings.
    MapIter mi, mii ;
    VhdlLibrary *lib ;
    VhdlPrimaryUnit *unit ;
    FOREACH_MAP_ITEM_BACK(vhdl_file::AllLibraries(), mi, 0, &lib) {
        FOREACH_MAP_ITEM_BACK(lib->AllPrimaryUnits(), mii, 0, &unit) {
            if (!unit) continue ;
            if (!packages_too && unit->Id()->IsPackage()) continue ; // Skip packages
            delete unit ;
        }
        // Don't remove the library. Work library pointer might still be there.
    }

    // Turn on "re-analyze unit" warning. (VIPER #1951)
    // VIPER #8129: Properly restore the message type:
    if (org_type==VERIFIC_NONE) Message::ClearMessageType("VHDL-1201") ; else (void) Message::SetMessageType("VHDL-1201", org_type);
}

/*static*/ void
vhdl_file::RemoveAllUnits(const char *lib_name, unsigned packages_too)
{
    // If specified library name is NULL, then delete all units in all libraries.
    if (!lib_name) {
        RemoveAllUnits(packages_too) ;
        return ;
    }

    // Get specified library
    VhdlLibrary *lp = GetLibrary(lib_name, 0) ;
    if (!lp) return ;

    MapIter mi ;
    VhdlPrimaryUnit *unit ;
    FOREACH_MAP_ITEM_BACK(lp->AllPrimaryUnits(), mi, 0, &unit) {
        if (!unit) continue ;
        if (!packages_too && unit->Id()->IsPackage()) continue ; // Skip packages
        delete unit ;
    }

    // Don't remove the library. Work library pointer might still be there.
}

unsigned
vhdl_file::IsInsideStdPackageAnalysis()
{
    // Only std.standard package has no use clause, for all
    // others std.standard is implicitly available
    return (VhdlTreeNode::_present_scope && !VhdlTreeNode::_present_scope->GetUsing()) ;
}

// VHDL Library handling :

VhdlLibrary *
vhdl_file::GetLibrary(const char *name, unsigned create_if_needed)
{
    if (!name) return 0 ;

    if (!_all_libraries) _all_libraries = new Map(STRING_HASH) ;

    // First, do a quick case-sensitive check :
    VhdlLibrary *result = (VhdlLibrary*)_all_libraries->GetValue(name) ;
    if (result) return result ;

    // See if string 'name' is in mixed case. Downcase the name if needed :
    // VIPER #8444 : Do not downcase escaped name
    char *downcased = (name && *name != '\\') ? Strings::strtolower(Strings::save(name)): Strings::save(name) ;

    // Get the library :
    result = (VhdlLibrary*)_all_libraries->GetValue(downcased) ;
    if (result) {
        Strings::free( downcased ) ;
        return result ;
    }

    // VIPER #1777, 1781, 2023 and 3570: VHDL library aliasing:
    const char *alias = (_library_aliases) ? (const char *)_library_aliases->GetValue(name) : 0 ;
    if (alias) {
        Strings::free(downcased) ;
        return GetLibrary(alias, create_if_needed) ;
    }

    if (!create_if_needed) {
        Strings::free( downcased ) ;
        return 0 ;
    }

    // Create in down-cased form :
    result = new VhdlLibrary(downcased) ;
    Strings::free( downcased ) ;

    (void) _all_libraries->Insert(result->Name(), result) ;
    return result ;
}

void
vhdl_file::DeleteLibrary(const char *name)
{
    if (!name || !_all_libraries) return ;

    VhdlLibrary *result = (VhdlLibrary*)_all_libraries->GetValue(name) ;

    (void) _all_libraries->Remove(name) ;
    delete result ;
}

// VIPER #1777, 1781, 2023 and 3570: VHDL library aliasing
/*static*/ unsigned
vhdl_file::SetLibraryAlias(const char *alias_name, const char *target_lib)
{
    if (!alias_name || !target_lib) return 0 ;

    // Unset existing library alias, if it already exists:
    (void) UnsetLibraryAlias(alias_name) ;

    // Quick check whether the 'alias_name' exists as a logical library.
    // We can't set an actual logical library to be an alias of another one:
    if (GetLibrary(alias_name, 0 /* do not create */)) return 0 ;

    // If the alias Map is not created, create it now (with case insensitive hashing for VHDL):
    if (!_library_aliases) _library_aliases = new Map(STRING_HASH_CASE_INSENSITIVE) ;

    // Check if target itself is an alias, use that otherwise use the given target:
    const char *actual_target = (const char *)_library_aliases->GetValue(target_lib) ;
    if (!actual_target) actual_target = target_lib ;

    // Create the mapping of the library alias now:
    (void) _library_aliases->Insert(Strings::save(alias_name), Strings::save(actual_target)) ;

    // Create the actual logical library to stop the user from adding the target as another alias:
    (void) GetLibrary(actual_target, 1 /* create if needed */) ;

    return 1 ; // Alias created
}

/*static*/ unsigned
vhdl_file::UnsetLibraryAlias(const char *alias_name)
{
    MapItem *mi = (_library_aliases) ? _library_aliases->GetItem(alias_name) : 0 ;
    if (!mi) return 0 ;

    // Get the mapping (alias name and the target):
    char *key = (char *)mi->Key() ;
    char *value = (char *)mi->Value() ;

    // Remove the mapping:
    (void) _library_aliases->Remove(mi) ;

    // Cleanup:
    Strings::free(key) ;
    Strings::free(value) ;

    return 1 ; // Alias removed
}

/* static */ void
vhdl_file::UnsetAllLibraryAlias()
{
    MapIter mi ;
    char *key ;
    char *value ;
    FOREACH_MAP_ITEM(_library_aliases, mi, &key, &value) {
        //(void) _library_aliases->Remove(key) ;
        Strings::free(key) ;
        Strings::free(value) ;
    }
    delete _library_aliases ;
    _library_aliases = 0 ;
}

//CARBON_BEGIN
/*********************  Pragma Trigger handling ***********************/
void vhdl_file::AddPragmaTrigger(const char *trigger)
{
    if (!trigger) return ;
    if (!_pragma_triggers) _pragma_triggers = new Array() ;
    _pragma_triggers->InsertLast(Strings::save(trigger)) ;
}

void vhdl_file::RemoveAllPragmaTriggers()
{
    if (!_pragma_triggers) return ;
    unsigned i ;
    char *exist ;
    FOREACH_ARRAY_ITEM(_pragma_triggers, i, exist) {
        Strings::free( exist ) ;
    }
    delete _pragma_triggers ;
    _pragma_triggers = 0 ;
}

unsigned
vhdl_file::IsPragmaTrigger(const char * id)
{
    Verific::Array* pt_list = Verific::vhdl_file::PragmaTriggers() ;
    unsigned i ;
    char * upt;
    FOREACH_ARRAY_ITEM(pt_list, i, upt) {
        if (Strings::compare(id, upt)) {
            return 1; 
        }
    }
    return 0;
}
//CARBON_END

/************************ Interrupt handling ************************/

void
vhdl_file::RegisterInterrupt(unsigned(*interrupt_func)(linefile_type))
{
    _interrupt_func = interrupt_func ;
}

unsigned
vhdl_file::CheckForInterrupt(linefile_type linefile)
{
    // Check if we should stop processing :
    if (Message::ErrorCount()) return 1 ;
    if (!_interrupt_func) return 0 ;

    // Call user interrupt function :
    if (_interrupt_func(linefile)) {
        VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        tmp_print_node.Error("VHDL reader: User Interrupt. Cleaning up....") ;
        // Message::Error(0,"VHDL reader: User Interrupt. Cleaning up....") ;
        // This will set the message error counter, so we will not call
        // the user _interrupt_func any more until the error count is cleared.
        return 1 ;
    }
    return 0 ;
}

void
vhdl_file::Interrupt()
{
    // Interrupt the elaborator or analyzer. Do this by issuing an error message.
    // This will gracefully end the elaborator It will stop gracefully and clean
    // up behind itself. Side-effect is that various error messages might occur after this.
    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
    tmp_print_node.Error("VHDL reader: User Interrupt. Cleaning up....") ;
    // Message::Error(0,"VHDL reader: User Interrupt. Cleaning up....") ;
}

/************************ Black-box compilation handling *****************/

void
vhdl_file::RegisterCompileAsBlackbox(unsigned(*compile_as_blackbox_func)(const char *)) {
    _compile_as_blackbox_func = compile_as_blackbox_func ;
}

unsigned
vhdl_file::CheckCompileAsBlackbox(const char *unit_name)
{
    if (!_compile_as_blackbox_func) return 0 ;
    if (!unit_name) return 0 ;
    // Call the user-registered function :
    return _compile_as_blackbox_func(unit_name) ;
}

/********************* Time Resolution Setting **************************/
void
vhdl_file::SetTimeResolution(int scale, const char *unit)
{
    // Only 1, 10, 100 can be specified as scale-factor
    if (!((scale == 1) || (scale == 10) || (scale == 100))) return ; // not 1, 10, 100 return
    _scale_factor_of_timeunit = scale ;
    if (!unit) return ;
    Strings::free(_time_unit) ;
    if (Strings::compare_nocase(unit, "fs") || Strings::compare_nocase(unit, "ps") || Strings::compare_nocase(unit, "ns") || Strings::compare_nocase(unit, "us") || Strings::compare_nocase(unit, "ms") || Strings::compare_nocase(unit, "sec") || Strings::compare_nocase(unit, "min") || Strings::compare_nocase(unit, "hr")) {
        _time_unit = Strings::save(unit) ;
    }
}

/******************** -L option Setting ********************************/
void
vhdl_file::AddLOption(const char *lib_name)
{
    if (!lib_name) return ; // Nothing to add
    if (!_ordered_libs) _ordered_libs = new Set(STRING_HASH) ;

    // Add library name in library list, do not insert if already in
    if (!_ordered_libs->GetItem(lib_name)) (void) _ordered_libs->Insert(Strings::save(lib_name)) ;
}

void
vhdl_file::RemoveAllLOptions()
{
    SetIter si ;
    char *lib_name ;
    FOREACH_SET_ITEM(_ordered_libs, si, &lib_name) Strings::free(lib_name) ;
    delete _ordered_libs ;
    _ordered_libs = 0 ;
}

VhdlPrimaryUnit *
vhdl_file::GetUnitFromLOptions(const char *unit_name, unsigned restore, unsigned exclude_verilog)
{
    if (!unit_name || !_ordered_libs) return 0 ; // No name specified, return 0

    SetIter si ;
    char *lib_name ;
    VhdlPrimaryUnit *prim_unit = 0 ;
    FOREACH_SET_ITEM(_ordered_libs, si, &lib_name) { // Iterate over ordered libraries
        if (!lib_name) continue ;
        VhdlLibrary *lib = GetLibrary(lib_name, restore /* create if restore is 1*/) ;
        prim_unit = lib ? lib->GetPrimUnit(unit_name, restore) : 0 ; // Get the unit in library
        // Or look on the Verilog side :
        // VIPER #5035 : Look at verilog side only when 'exclude_verilog' is 0
        if (!prim_unit && lib && !exclude_verilog) {
            // VIPER #8444: Use the library as specified in -L option
            //prim_unit = vhdl_file::GetVhdlUnitFromVerilog(lib->Name(), unit_name, 1 /*from_l_options*/) ;
            prim_unit = vhdl_file::GetVhdlUnitFromVerilog(lib_name, unit_name, 1 /*from_l_options*/) ;
        }
        if (prim_unit) return prim_unit ; // Found, return
    }
    return 0 ;
}

VeriModule*
vhdl_file::GetVerilogModuleFromlib(const char* library_name, const char* module_name, unsigned from_l_options)
{
    if (!module_name || !library_name) return 0 ;

    // Viper 8259: Delete the verilog library if it is created but the Verilog module cannot be found.
    // We have to create the library to see if the module can be restored from saved binaries.
    // We may also check the existence of such saved binaries and may not create the library at all
    // However this may be costly. Hence decided to create the library and if it remains empty we
    // delete it at the end of this function.
    unsigned lib_is_created = 0 ;
    unsigned veri_file_all_libraries_present = veri_file::AllLibraries() ? 1 : 0 ;
    // VIPER 4527 :
    VhdlLibrary *l = (Strings::compare_nocase("work", library_name)) ?  GetWorkLib() : 0 ;
    // Viper 6979: For LOptions search honour the name and pick the library named work.
    if (from_l_options) l = 0 ;
    const char* lib_name = l ? l->Name() : library_name ;
    if (!lib_name) return 0 ;
    // VIPER #1904/2279 : Determine prefix/suffix actually pertains a verilog library/module.
    // VIPER #3095 : Get module from verilog library. Restore if required.
    // VIPER #4410 & #4551: Since vhdl is case insensitive, verilog library is searched using
    // lower case. If actually verilog library name is specified in upper case, we cannot
    // find that. Change this so that we can find library using same rule as module findings.
    // i.e. we will perform case insensitive search to find verilog library from vhdl.
    unsigned is_escaped = (module_name[0] == '\\' && module_name[Strings::len(module_name) - 1] == '\\') ;
    unsigned is_lib_escaped = (lib_name[0] == '\\' && lib_name[Strings::len(lib_name) - 1] == '\\') ;
    VeriLibrary *lib = 0 ;
    if (!is_lib_escaped) {
        // Find the library in verilog converting its name to lower case
        //char *lib_lower = Strings::strtolower(Strings::save(lib_name)) ;
        //lib = veri_file::GetLibrary(lib_lower, 0 /* do not create*/, 1 /* case sensitive*/) ;
        // If not found, do a case insensitive search :
        lib = veri_file::GetLibrary(lib_name, 0/* create if not*/, 0 /* case insensitive*/) ;
        if (!lib) {
            lib = veri_file::GetLibrary(lib_name, 1/* create if not*/, 0 /* case insensitive*/) ;
            lib_is_created  = 1 ;
        }
        //Strings::free(lib_lower) ;
    } else {
        // Library name may be escaped.. if so, it may not need escaping
        // in verilog. Strip off the escaping and search in verilog.
        char *l_name = VhdlNode::UnescapeVhdlName(lib_name) ;
        lib = l_name ? veri_file::GetLibrary(l_name, 0, 1 /* case sensitive*/) : 0 ;
        if (!lib) {
            lib = l_name ? veri_file::GetLibrary(l_name, 1, 1 /* case sensitive*/) : 0 ;
            lib_is_created  = 1 ;
        }
        Strings::free(l_name) ;
    }

    // If the veri_module name is not escaped and if there are multiple modules with same
    // name but different in case then here is the strategy to choose the correct one
    // if duplicate module does not exist then we will match the name case insensitively
    // provided the name is not escaped.
    // if multiple modules are present and if one of them is of has its name in lower case only
    // then we will pick that one only as Vhdl is case insensitive. If none of them are of lower
    // case then we will not choose any module. This behavior is in accordance with Modelsim
    VeriModule* veri_module = 0 ;
    char *suffix_name = Strings::save(module_name) ;
    if (!is_escaped) {
        char* suffix_name_lower = Strings::strtolower(Strings::save(suffix_name)) ;
        // VIPER #4410 & #4551: Use GetModule of library:
        veri_module = (lib) ? lib->GetModule(suffix_name_lower, 1 /* case sensitive */, 1) : 0 ;

        unsigned duplicate_module = 0 ; // VIPER #4410 & #4551
        if (!veri_module) {
            if (!lib) {
                lib = veri_file::GetLibrary(lib_name, 0/*create if not */, 0/*case_sensitive*/) ;
                if (!lib) {
                    lib = veri_file::GetLibrary(lib_name, 1/*create if not */, 0/*case_sensitive*/) ;
                    lib_is_created = 1 ;
                }
            }
            if (lib) {
                MapIter mi ;
                VeriModule *module ;
                FOREACH_VERILOG_MODULE_IN_LIBRARY(lib, mi, module) {
                    if (Strings::compare_nocase(module->GetName(), suffix_name_lower)) {
                        if (veri_module) {
                            veri_module = 0 ; //Duplicate match return 0
                            duplicate_module = 1 ; // VIPER #4410 & #4551
                            break ;
                        }
                        veri_module = module ;
                    }
                }
            }
        }

        // VIPER #4346 : Do case insensitive serach to get existing
        // module from library or restore module from sdb.
        // VIPER #4410 & #4551: Case insensitive search of library in mapped library paths:
        if (!veri_module && !duplicate_module) {
            veri_module = (lib) ? lib->GetModule(suffix_name, 0 /* case sensitive */, 1) : 0 ;
        }
        Strings::free(suffix_name_lower) ;
    }

    // Viper # 3009 : extension for dual language support..
    // Check following :
    // (1) suffix name may be escaped.. if so, it may not need escaping in
    //     verilog. Strip off the escaping and search in verilog.
    // (2) suffix name may need escaping in verilog. Escape it and search.
    // (3) suffix name may need escaping in both vhdl and verilog. Strip off
    //     vhdl escaping and do verilog escaping before searching for module.

    if (is_escaped) {
        // check for (1) above
        char *unescaped_suffix_name = VhdlNode::UnescapeVhdlName(suffix_name) ;
        // VIPER #4410 & #4551: Use GetModule of library
        veri_module = (lib) ? lib->GetModule(unescaped_suffix_name, 1, 1) : 0 ;
        Strings::free(unescaped_suffix_name) ;
    }
    Strings::free(suffix_name) ;
    if (lib_is_created && lib && !veri_module) {
        Map* module_table = lib->GetModuleTable() ;
        if (!module_table || !module_table->Size()) {
            veri_file::RemoveLibrary(lib) ;
            delete lib ;
            if (!veri_file_all_libraries_present) {
                veri_file::ResetAllLibraries() ;
            }
        }
    }
    return veri_module ;
}

VhdlPrimaryUnit *
vhdl_file::GetVhdlUnitFromVerilog(const char* lib_name, const char* module_name, unsigned from_l_options)
{
    // Get a VHDL translation (entity) for Verilog modules
    VhdlPrimaryUnit *pu = 0 ;
    (void)lib_name ;
    (void)module_name ;
    (void)from_l_options ;
    // Code here only exists if Verilog elaborator product is enabled
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // Get a VHDL translation (entity) for Verilog modules
    // The assumption is that vhdl_lib->GetPrimUnit(module_name) already returned NULL. So there is no existing VHDL unit in this library.
    // Viper: #2410, #2279, #1856, #2922

    // Find the Verilog module :
    VeriModule* veri_module = GetVerilogModuleFromlib(lib_name, module_name, from_l_options) ;
    if (!veri_module) return 0 ;

    // Viper 4611: take the veri_lib_name from verilog module
    VeriLibrary *veri_lib = veri_module->GetLibrary() ;
    if (!veri_lib)  return 0 ;

    // Find (or create) the VHDL library using the name of the verilog library :
    // VIPER #8444: If name of verilog library constains uppercase/keyword, create
    // escaped library name to preserve case
    char *vhdl_lib_name = vhdl_file::CreateVhdlEntityName(veri_lib->GetName()) ;
    //VhdlLibrary *vhdl_lib = vhdl_file::GetLibrary(veri_lib->GetName(), 1) ;
    VhdlLibrary *vhdl_lib = vhdl_file::GetLibrary(vhdl_lib_name, 1) ;
    Strings::free(vhdl_lib_name) ;
    if (!vhdl_lib) return 0 ;

    // For Verilog configurations, look up the name of the binding module :
    char *mod_name = Strings::save(veri_module->GetName()) ;

    // Search in the vhdl library if this is a case of name conflicts of
    // lower and upper case of verilog modules. In such cases the vhdl_lib->GetPrimUnit()
    // may be unsuccessful to get the correct module so we do a extensive search
    // The entity instantiation name may be unsecaped but the Entity name derived
    // from verilog can be escaped. If the entity instantiation name matches case
    // insensitively with a unique escaped named VeriModule. pick it up. o.w. error

    // Generate the name that 'ConvertModule' will generate.
    char *entity_name = vhdl_file::CreateVhdlEntityName(mod_name) ;
    if (!Strings::compare(entity_name, module_name)) {
        // module_name and entity_name are different, this may happen for verilog configurations and
        // for escaped names of verilog modules stored in Vhdl Library. Re-Check the library :
        pu = vhdl_lib->GetPrimUnit(entity_name) ;
        if (pu) {
            Strings::free(entity_name) ;
            Strings::free(mod_name) ;
            return pu ;
        }
    }
    Strings::free(entity_name) ;

    // Check if the 'vl' package is there :
    VhdlLibrary *vl_lib = vhdl_file::GetLibrary("vl",1 /*create if needed*/) ;
    if (!vl_lib || !vl_lib->GetPrimUnit("vl_types",1/*restore if needed*/)) {
        veri_module->Warning("cannot bind module %s, mixed language library vl not compiled", veri_module->GetName()) ;
        Strings::free(mod_name) ;
        return 0 ;
    }

    // Now convert the module to a VHDL entity :
    pu = veri_module->ConvertModule(vhdl_lib->Name()) ;
    if (pu) {
        // Add the unit to the library
        if (!vhdl_lib->AddPrimUnit(pu)) {
            // If error occurs while adding the entity to the library
            Strings::free(mod_name) ;
            return 0 ; // clean up and return
        }
    }
    // clean up
    Strings::free(mod_name) ;
#endif
    return pu ;
}

char*
vhdl_file::CreateVhdlEntityName(const char *module_name)
{
    if (!module_name) return 0 ;
    char *mod_name = Strings::save(module_name) ;
    // Viper 5610. If the name contain an upper case letter or the
    // name is itself verilog escaped escape the entity name

    if (VeriNode::IsEscapedIdentifier(module_name) || Strings::containupper(mod_name)) {
        Strings::free(mod_name) ;
        return(Strings::save("\\", module_name, "\\")) ;
    } else {
        Strings::free(mod_name) ;
        return (Strings::save(module_name)) ;
    }
}

#ifdef VHDL_FLEX_READ_FROM_STREAM
// VIPER #5726: Read from streams:
/* static */ void
vhdl_file::RegisterFlexStreamCallBack(verific_stream *(*stream_func)(const char *file_name))
{
    _stream_callback_func = stream_func ;
}

/* static */ verific_stream *
vhdl_file::GetUserFlexStream(const char *file_name)
{
    if (!_stream_callback_func) return 0 ;

    return _stream_callback_func(file_name) ;
}
#endif

// VIPER #6633: Generate Explicit State Machine: For generating explicit state machine from implicit state machine
// we follow the following algorithm:
// 1) Traverse the parse tree with VhdlFsmProcessStatementVisitor.
// 2) For process statement:
//       - check for some limitations and conditions.
//       - if conditions are fulfilled do the following:
//             1. Generate control flow graph with a new visitor VhdlFsmProcessVisitor.
//                   1) For generating CFG we need to declare 4 classes- statement node for
//                      plain statements, state node for event statements, conditional node
//                      for if-else/case/loop statements and end node for scope end of conditional
//                      and loop statements.
//                   2) During CFG generation each time we visit a node we check for event control
//                      within the block, otherwise we store the whole block of statements in statement node.
//                   3) For loop/conditional node we have more than one child: process each branch
//                      and create new parse tree branch for each branch and store the parent pointer
//                      in the conditional/loop node.
//                   4) Set an end node at the end of each branch of conditional node and else branch
//                      of loop node.
//                   5) Set the last node as the parent of the very first node.
//             2. Count the total number of events.
//             3. For each state nodes generate case item and store them in an array of items.
//             4. Generate a reset block for first time starting the simulation.
//             5. Generate a conditional statement with the reset and set block(case statement).
//             6. Keep the reset block in a compile flag.
//             8. Create a sensitivity list with the clocking event of the original parse tree.
//             9. Create a new process statement.
//             10. Delete the CFG.
// 3) If we can able to create a new process statement then replace the old one with it.
// 4) Return the new statement.
unsigned
vhdl_file::GenerateExplicitStateMachines(const char *unit_name, const char *lib_name, unsigned reset)
{
    unsigned process_stmt_replaced = 0 ;

    if (!unit_name) {
        MapIter mi, mii ;
        VhdlLibrary *lib ;
        VhdlPrimaryUnit *unit ;
        // Walk BACK so that we find the last analyzed, uncompiled unit.
        FOREACH_MAP_ITEM_BACK(AllLibraries(), mi, 0, &lib) {
            FOREACH_MAP_ITEM_BACK(lib->AllPrimaryUnits(), mii, 0, &unit) {
                if (!unit) continue ;
                VhdlIdDef *id = unit->Id() ;
                if (id && !id->IsEntity()) continue ;
                process_stmt_replaced = vhdl_file::GenerateExplicitStateMachine(unit, reset) ;
                unit->SetProcessStmtReplaced(process_stmt_replaced) ;
            }
        }
    } else {
        VhdlLibrary *lib = GetLibrary(lib_name) ;
        VhdlPrimaryUnit *unit = lib ? lib->GetPrimUnit(unit_name) : 0 ;
        if (unit) {
            VhdlIdDef *id = unit->Id() ;
            if (id && id->IsEntity()) {
               process_stmt_replaced =  vhdl_file::GenerateExplicitStateMachine(unit, reset) ;
               unit->SetProcessStmtReplaced(process_stmt_replaced) ;
            }
        }
    }
    if (process_stmt_replaced) return 1 ;

    return 0 ;
}

/* static */ void
vhdl_file::Reset()
{
    ResetParser() ;

    VhdlNode::ResetMessageMap() ;
    VhdlNode::ResetOperatorsTable() ;
    VhdlNode::ResetPragmaTables() ;
    VhdlNode::ClearNamedPath() ;
    VhdlNode::ClearStructuralStack() ;
    VhdlNode::ResetAccessValueSet() ;
    VhdlNode::ClearAttributeSpecificPath() ;
    VhdlTreeNode::CleanConfigNumMap() ;
    VhdlNode::ResetConstNets() ;
    VhdlTreeNode::CleanUnitNumMap() ;
    VhdlTreeNode::CleanLong2ShortMap() ;
    VhdlScope::ResetStack() ;
    VhdlTreeNode::ResetComments() ;
    VhdlAttributeName::ResetPredefAttrTable() ;
}

/* static */ void
vhdl_file::ResetParser()
{
    UnsetAllLibraryAlias() ;                    // Delete all library aliases
    ResetAllLibraryPaths() ;                    // Clear all library to directory mapping
    ClearDefaultLibraryPath() ;                 // Clear default library to directory mapping
    RemoveAllLOptions() ;                       // Clear out -L settings

    //CARBON_BEGIN
    RemoveAllPragmaTriggers();
    //CARBON_END
    RemoveAllUnits(1 /* packages too */) ;      // Cleans out ALL analyzed parse-trees (including all packages)
    // Delete all empty libraries now
    MapIter mi ;
    VhdlLibrary *lib ;
    FOREACH_VHDL_LIBRARY(mi, lib) {
        delete lib ;
    }
    delete _all_libraries ;
    _all_libraries = 0 ;

    _work_lib = 0 ;                             // Reset work library

    // CHECKME: Should we also reset the following?
    // If we need to, we have to somehow set it to the default value which may
    // differ from customer to customer. So, have to use MACRO for default value
    // and have to provide a Reset*() API for them. Or have to do this in some other way:
    //ResetLoopLimit() ;                        // Reset loop limit
    //SetMaxArraySize(23) ;                     // Reset maximum array size to default
    //SetMaxStackDepth(0) ;                     // Reset maximum stack depth
    //SetIgnoreDeadAssignment(0) ;              // Reset ignore dead assignment
    //SetIgnoreTranslateOff(0) ;                // Reset ignore translate-off handling
    //SetIgnoreAllPragmas(0) ;                  // Reset ignore all pragma handling
    //SetExtractRams(1) ;                       // Reset RAM dual-port RAM extraction
    //SetExtractMPRams(1) ;                     // Reset multi-port RAM extraction
    //SetMinimumRamSize(0) ;                    // Reset minimum RAM size
    //SetProtectionObject(0) ;                  // Set decipher object for binary save/restore
    //SetTickProtectObject(0) ;                 // Set decipher object for IEEE 1076 2008 `protect envelop decryption
}

