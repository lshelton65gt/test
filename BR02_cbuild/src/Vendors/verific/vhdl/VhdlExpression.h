/*
 *
 * [ File Version : 1.193 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_EXPRESSION_H_
#define _VERIFIC_VHDL_EXPRESSION_H_

#include "VhdlTreeNode.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Set ;

class VhdlName ;
class VhdlExpression ;
class VhdlIdDef ;
class VhdlName ;
class VhdlDesignator ;
class VhdlScope ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Need for conversion of vhdl package to verilog package
class VeriScope ;
class VeriDataType ;
class VeriRange ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Fillup ranges:
class VeriConstraint ;
#endif

// For elaboration
class VhdlValue ;
class VhdlConstraint ;
class VhdlDataFlow ;
class VhdlMapForCopy ;

class VhdlVarUsageInfo ;
class SynthesisInfo ;

// First, constraints/ranges

/* -------------------------------------------------------------- */

// Discrete range and expression can show up as assoc_element.
// So, 'assoc_element is the superset.
class VFC_DLL_PORT VhdlDiscreteRange : public VhdlTreeNode
{
protected: // Abstract class
    VhdlDiscreteRange() ;

    // Copy tree node
    VhdlDiscreteRange(const VhdlDiscreteRange &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlDiscreteRange(SaveRestore &save_restore);

public:
    virtual ~VhdlDiscreteRange() ;

private:
    // Prevent compiler from defining the following
    VhdlDiscreteRange(const VhdlDiscreteRange &) ;            // Purposely leave unimplemented
    VhdlDiscreteRange& operator=(const VhdlDiscreteRange &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual VhdlIdDef *         SubprogramAspect(VhdlIdDef* /*formal*/) ;
    virtual VhdlIdDef *         EntityAspect() ;                 // Return the interface unit (entity/component) of a entity aspect or instantiated unit

    // Copy parse-tree.
    // This virtual routine can be used to copy any discrete
    // range and any parse-tree node derived from 'this' class.
    virtual VhdlDiscreteRange * CopyDiscreteRange(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/) { return 0 ; }               // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/) { return 0 ; }                           // Delete 'old_node', puts new name in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange * /*old_node*/, VhdlDiscreteRange * /*new_node*/) { return 0 ; } // Delete 'old_node', puts new range constraint in its place.
    virtual void                ReplaceBoundsInSubtypeIndication(VhdlDataFlow *df) ;

    virtual VhdlName *          ChangeOutOfScopeRefs(VhdlScope * /*ref_scope*/, VhdlScope * /*def_scope*/, VhdlScope * /*container_scope*/, Map * /*lib_ids*/) { return 0 ; }

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) =0 ;
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode * /*init_assign*/, Set* /*unit_ids*/) const { }     // Checks for deffered constants defined for VhdlExpressions VIPER 3505, 4368

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the explicit resolution function of a subtype indication. If there.

    virtual unsigned            IsUnconstrained(unsigned all_level) const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            IsRange() ; // Can't make this 'const'
    virtual unsigned            IsAssoc() const ;
    virtual unsigned            IsOthers() const ;
    virtual unsigned            IsUnaffected() const ;
    virtual unsigned            IsOpen() const ;
    virtual unsigned            IsNull() const ; // Check if expression is a NULL literal
    virtual unsigned            IsAllocator() const  { return 0 ; } // Return 1 for allocator
    virtual unsigned            IsConstant() const ; // check if expression is a constant literal (or generic)
    virtual unsigned            FromVerilog() const { return 0 ; }
    virtual void                SetFromVerilog()    { }
    virtual unsigned            IsInertial() const  { return 0 ; }
    virtual unsigned            IsExternalNameRef() const { return 0 ; } // VIPER #7890: Test to see if a VhdlName refers to a VHDL 2008 external-name (defined only on VhdlName and on its derived classes)

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    // Viper #5073
    virtual unsigned            IsLocallyStaticAggregateTarget() const ;

    virtual unsigned            IsBox() const        { return 0 ; }
    virtual unsigned            IsDefault() const    { return 0 ; }

    // LRM : 1.1.1.2 and 4.3.2.2 Viper #3183
    // Actual must be static-name or globally-static-expression or
    // resolution function with static-name/static-expression single argument
    virtual unsigned            IsGoodForPortActual() const ;

    virtual unsigned            IsAggregate() const ;
    virtual unsigned            IsOthersAggregate() const ; // VIPER 7081 : Check if this is an aggregate with an 'others' clause.
    virtual unsigned            IsStringLiteral() const     { return 0 ; }
    virtual unsigned            IsBitStringLiteral() const  { return 0 ; }
    virtual unsigned            IsCharacterLiteral() const  { return 0 ; }
    virtual unsigned            IsInteger()                 { return 0 ; }

    virtual unsigned            IsSignal()                  { return 0 ; } // If the name/expression denotes a signal
    virtual unsigned            IsArraySlice() const        { return 0 ; }
    virtual unsigned            IsFunctionCall() const      { return 0 ; }
    virtual unsigned            IsTypeConversion() const    { return 0 ; } // Returns 1 if name is type conversion
    virtual unsigned            IsUsed() const ; // Viper #5700
    virtual unsigned            IgnoreAssignment() const ; // Viper #5700
    virtual unsigned            IsExternalName() const      { return 0 ; } // VIPER #7432

    virtual VhdlIdDef *         FindFullFormal() const ; // Return IdDef if it's the single (formal) id in this expression
    virtual VhdlIdDef *         FindFormal() const ;     // Return IdDef in this expression (find through prefix naming too).
    virtual void                SetFormal(VhdlIdDef * /*formal*/) {}

    // VIPER #6896: Conversion of verilog package to vhdl package
    virtual void                FillRange(unsigned /*left_bound*/, unsigned /*right_bound*/) { } // fillup range

    // The signature of the following three functions has been changed.
    // These guys are used only to extract the actual and formal part of
    // named association element and actual and formal part cannot be
    // range, they are always expressions.
    virtual VhdlExpression *    ActualPart() const       { return 0 ; }
    virtual VhdlExpression *    TakeActualPart()         { return 0 ; }
    virtual VhdlExpression *    FormalDesignator() const { return 0 ; }

    virtual VhdlName *          GetPrefix() const        { return 0 ; }
    virtual VhdlDesignator *    GetSuffix() const        { return 0 ; }
    virtual Array *             GetAssocList() const     { return 0 ; }

    virtual Array *             GetChoices() const       { return 0 ; }
    virtual VhdlExpression *    GetExpression() const    { return 0 ; }
    virtual VhdlExpression *    GetLeftExpression() const           { return 0 ; }
    virtual VhdlExpression *    GetRightExpression() const          { return 0 ; }
    virtual unsigned            GetDir() const                      { return 0 ; }
    virtual void                SetOperator(VhdlIdDef* /*oper*/)    {  } // VIPER #8170
    virtual VhdlIdDef *         GetOperator() const                 { return 0 ; }
    virtual unsigned            GetOperatorToken() const            { return 0 ; }
    virtual unsigned            IsVhdlOperator()                    { return 0 ; } // VIPER #8170
    virtual unsigned            FindVisibleOperators(Map& /*operator_map*/) { return 0 ; }
    virtual unsigned            FindVisibleOperatorsFromMap(Map& /*operator_map*/, Map& /*new_operator_map*/) { return 0 ; }
    virtual unsigned            PruneVisibleOperators(Map& /*orig_operator_map*/, Map& /*operator_map*/, Map& /*expected_types_map*/, Set& /*already_visited*/, unsigned /*environment*/, VhdlScope* /*selection_scope*/) { return 0 ; } // VIPER #8170
    virtual VhdlIdDef *         GetNamedPrefixId() const            { return 0 ; } // Defined for VhdlIndexedName
    virtual VhdlIdDef *         GetRecordElementName() const ; // VIPER #8085

    // Used on a simple name : See if an expression contains just a simple name or string
    virtual const char *        Name() const { return 0 ; }
    virtual char *              ExprString() const ; // Viper #7042
    // Use on simple names : Obtain the resolved identifier of this name. Only works after analysis (after type-inference (or FindObject) routine has run).
    virtual VhdlIdDef *         GetId() const { return 0 ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetId(VhdlIdDef * /*new_id*/) { } // set identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetName(char * /*name*/) {} // set _name field for this name. Seleced name suffix, indexed name prefix, or simple idref.

    virtual unsigned            FindIfEdge() const { return 0 ; } // Find out if this condition expression is a 'clock' edge expression. Used for VIPER 3603.
    virtual unsigned            FindIfStable() const { return 0 ; } // Find out if this condition expression is a 'clock' edge expression. Used for VIPER 3603.
    virtual VhdlDiscreteRange * GetRefTarget(Map * /*formal_to_actual*/) { return this ; }  // Viper 6781

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl range to verilog range: during analysis create unconstraint ranges
    virtual VeriRange *         ConvertRange(VeriScope* /*scope*/, Array* /*type_id_arr*/) const { return 0 ; }
#endif

    // Elaboration

    // Evaluate / Assign to an expression (aggregate is legal target)
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)=0 ;
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
    VhdlConstraint *            EvaluateConstraint(VhdlDataFlow *df, VhdlConstraint *target_constraint = 0) { return EvaluateConstraintInternal(df, target_constraint) ; }
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint)=0 ;
    virtual VhdlConstraint *    EvaluateConstraintPhysical(VhdlDataFlow *df) ; // For evaluating constraint of physical typedefs
    virtual VhdlConstraint *    EvaluateIntegerRangeConstraint(VhdlDataFlow * /*df*/) { return 0 ; } // Viper #6473

    virtual VhdlValue *         EvaluateCondition(VhdlDataFlow *df, Map *& /* condition_id_value_map */) ; // Viper #5553

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

    // Evaluate / Assign by choice-association (inside an aggregate)
    virtual void                EvaluateByChoice(Array *result_values, VhdlConstraint *constraint, VhdlDataFlow *df, VhdlValue *& default_value, Set *record_ele_pos, unsigned is_aggregate_type) ; // Viper #4950 : Added default_value argument (VIPER #6719 : Added record_ele_pos argument), Viper #7417/7477: is_aggregate_type added
    virtual void                AssignByChoice(VhdlValue *assign_value, VhdlConstraint *constraint, VhdlDataFlow *df, unsigned is_aggregate_type) ;
    virtual VhdlConstraint *    EvaluateConstraintByChoice(VhdlConstraint *previous_constraint, VhdlDataFlow *df, unsigned is_aggregate_type, unsigned check_dir) ; // Build the constraint of an aggregate based on the (named) index formals. Start with 0 or an unconstrained 'constraint', and call again for each element.
    virtual verific_uint64      NumAggregateElements(VhdlDataFlow *df) ; // Used for Vhdl 2008 array aggregate elaboration. The object is guranteed to be an array type lvalue. Returns the number of elements it has.

    // Associate a formal with actual. For component instantiation only for now.
    virtual void                PartialAssociation(Instance *inst, VhdlDataFlow *df) ;
    virtual void                MarkUsedFlag(SynthesisInfo * /*info*/) { }
} ; // class VhdlDiscreteRange

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlExpression : public VhdlDiscreteRange
{
protected: // Abstract class
    VhdlExpression() ;

    // Copy tree node
    VhdlExpression(const VhdlExpression &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlExpression(SaveRestore &save_restore);

public:
    virtual ~VhdlExpression() ;

private:
    // Prevent compiler from defining the following
    VhdlExpression(const VhdlExpression &) ;            // Purposely leave unimplemented
    VhdlExpression& operator=(const VhdlExpression &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            IsExpandedName() const    { return 0 ; }   // Tells if this name matches LRM 6.3 definition of expanded name. Versus access value or record element. Uses _present_scope

    // Copy expression.
    virtual VhdlDiscreteRange * CopyDiscreteRange(VhdlMapForCopy &old2new) const ;
    // Copy all types of expression.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/)                            { return 0 ; }               // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/)                                        { return 0 ; }                           // Delete 'old_node', puts new name in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange * /*old_node*/, VhdlDiscreteRange * /*new_node*/)             { return 0 ; } // Delete 'old_node', puts new range constraint in its place.
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)    { return 0 ; }
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)     { return 0 ; }

    virtual void                ModifyIndex(VhdlConstraint *&inst_unit_constraint, VhdlConstraint *&comp_constraint) ; // VIPER #3510, 3513 : Modify index of partial formal according to instantiated entity

    virtual unsigned            IsUnconstrained(unsigned all_level) const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            IsRange() ; // Can't make this 'const'
    virtual unsigned            IsAssoc() const ;
    virtual unsigned            IsOthers() const ;
    virtual unsigned            IsUnaffected() const ;
    virtual unsigned            IsOpen() const ;
    virtual unsigned            IsRecResFuncElement() const ;
    virtual unsigned            IsNull() const ; // Check if expression is a NULL literal
    virtual unsigned            IsConstant() const ; // check if expression is a constant literal (or generic)

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    virtual unsigned            IsAggregate() const ;
    virtual unsigned            IsOthersAggregate() const ; // VIPER 7081 : Check if this is an aggregate with an 'others' clause.

    virtual unsigned            IsQualifiedExpression() const ;

    // Following two APIs are meaningful for aggregates
    virtual unsigned            NumElements() const { return 0 ; }
    virtual VhdlExpression const *GetElementExpressionAt(unsigned /*idx*/) const { return 0 ; }

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) =0 ;
    virtual VhdlIdDef *         TypeInferChoices(VhdlIdDef *mother_type, Set *done_choices, unsigned n_assoc_elements) ;
    virtual verific_int64       EvaluateConstantInteger() ; // For attribute dimension ; before elaboration
    virtual void                SetInversePosition(VhdlIdDef * /*unit_decl_id*/) {}

    virtual VhdlIdDef *         FindFullFormal() const ;    // Return IdDef if it's the single (formal) id in this expression
    virtual VhdlIdDef *         FindFormal() const ;        // Return IdDef in this expression (find through prefix naming too).

    virtual VhdlExpression *    GetExpression() const       { return const_cast<VhdlExpression*>(this) ; }

    virtual VhdlExpression *    ActualPart() const          { return const_cast<VhdlExpression*>(this) ; }
    virtual VhdlExpression *    TakeActualPart()            { return 0 ; }
    virtual VhdlExpression *    FormalDesignator() const    { return const_cast<VhdlExpression*>(this) ; } // Return the formal designator (this looks past formal type-cast) in a association element
    // Used on a simple name : See if an expression contains just a simple name or string
    virtual const char *        Name() const                { return 0 ; }
    virtual VhdlExpression *    GetConditionalExpression() ;
    virtual VhdlName *          RecordIdName() const        { return 0 ; }
    virtual VhdlName *          RecordResFunction() const   { return 0 ; }
    virtual VhdlExpression *    GetAfter() const            { return 0 ; } // Returns after expression for VhdlWaveformElement (VIPER #7470)

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)=0 ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual VhdlValue *         EvaluateForVerilog(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/) { return 0; }
#endif

    virtual VhdlValue *         EvaluateAllocatorSubtype(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/) { return 0 ; }

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() { return ; }
#endif

    // Assign to an expression (name or aggregate)
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
    // Evaluate a range or name to get the constraint
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;

    virtual void SplitAndedSubExpressions(Array& anded_expressions_array) ; // Added for VIPER #3907
    virtual void                SetImplicit()        { }
    virtual void                IncreaseNesting() {  }
    virtual unsigned            GetNesting() { return 0 ; }
    virtual VhdlIdDef *         FindExternalObject(unsigned /*path_token*/, unsigned /*accent_count*/) { return 0 ; }
    virtual unsigned            CannotMatchType(VhdlIdDef * /*type_id*/) const { return 0 ; } // Quick check if this unresolved expression can certainly not match this type. Viper 1493(4400)

} ; // class VhdlExpression

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSubtypeIndication : public VhdlExpression
{
protected: // Abstract class
    VhdlSubtypeIndication() ;

    // Copy tree node
    VhdlSubtypeIndication(const VhdlSubtypeIndication &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSubtypeIndication(SaveRestore &save_restore);

public:
    virtual ~VhdlSubtypeIndication() ;

private:
    // Prevent compiler from defining the following
    VhdlSubtypeIndication(const VhdlSubtypeIndication &) ;            // Purposely leave unimplemented
    VhdlSubtypeIndication& operator=(const VhdlSubtypeIndication &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy type information.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;
    // Copy all types of type marks.
    virtual VhdlSubtypeIndication * CopySubtypeIndication(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/) { return 0 ; }                           // Delete 'old_node', puts new name in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange * /*old_node*/, VhdlDiscreteRange * /*new_node*/) { return 0 ; } // Delete 'old_node', puts new range constraint in its place.

    // Static Elaboration
    virtual VhdlSubtypeIndication * CreateSubtypeWithConstraint(VhdlConstraint * /*constraint*/) { VERIFIC_ASSERT(0) ; return 0 ; } // create type mark with argument specific constraint. Should not be called for this abstract class.

    virtual unsigned            IsRange() ; // Can't make this 'const'

    virtual unsigned            IsAccessType() { return 0 ; }
    virtual unsigned            ContainsAccessType() { return 0 ; }
    virtual unsigned            IsProtectedType() { return 0 ; } // VIPER #7628 : Returns 1 for protected type

    virtual VhdlName *          GetTypeMark() const  { return 0 ; }

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) =0 ;

    virtual void                ValidateSubtypeIndication() const {} // Validate syntax of subtype indication : VIPER #4169 & #4170

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned dimension) const ; // VIPER #7343
#endif // VHDL_ID_SUBTYPE_BACKPOINTER
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope* /*scope*/, Array* /*type_id_arr*/, unsigned /*processing_type*/) const { return 0 ; }
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges:
    virtual void                FillUpRanges(VeriScope* /*veri_scope*/, VeriConstraint* /*constraint*/, Map& /*type_id_map*/) { }
#endif

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)=0 ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint)=0 ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() { return ; }
#endif

} ; // class VhdlSubtypeIndication

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlExplicitSubtypeIndication : public VhdlSubtypeIndication
{
public:
    VhdlExplicitSubtypeIndication(VhdlName *res_function, VhdlName *type_mark, VhdlDiscreteRange *constraint) ;

    // Copy tree node
    VhdlExplicitSubtypeIndication(const VhdlExplicitSubtypeIndication &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlExplicitSubtypeIndication(SaveRestore &save_restore);

    virtual ~VhdlExplicitSubtypeIndication() ;

private:
    // Prevent compiler from defining the following
    VhdlExplicitSubtypeIndication() ;                                                 // Purposely leave unimplemented
    VhdlExplicitSubtypeIndication(const VhdlExplicitSubtypeIndication &) ;            // Purposely leave unimplemented
    VhdlExplicitSubtypeIndication& operator=(const VhdlExplicitSubtypeIndication &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLEXPLICITSUBTYPEINDICATION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy explicit subtype.
    virtual VhdlSubtypeIndication * CopySubtypeIndication(VhdlMapForCopy &old2new) const ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned dimension) const ; // VIPER #7343
#endif // VHDL_ID_SUBTYPE_BACKPOINTER

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895:
    // Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const ;
    //Convert vhdl range to verilog range: during analysis create unconstraint ranges
    virtual VeriRange *         ConvertRange(VeriScope *scope, Array *type_id_arr) const ;
#endif

    // Specific parse-tree manipulation routines.
    virtual unsigned                ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new resolution function/type mark in its place.
    virtual unsigned                ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range constraint in its place.
    virtual void                    ReplaceBoundsInSubtypeIndication(VhdlDataFlow *df) ;

    // Static Elaboration
    virtual VhdlSubtypeIndication * CreateSubtypeWithConstraint(VhdlConstraint *constraint) ; // Create type mark with argument specific constraint for unconstrained type.
    virtual VhdlName *              ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the resolution function of a subtype indication.

    virtual unsigned            IsUnconstrained(unsigned all_level) const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    // Accessor methods
    VhdlName *                  GetResolutionFunction() const       { return _res_function ; }
    VhdlName *                  GetTypeMark() const                 { return _type_mark ; }
    VhdlDiscreteRange *         GetRangeConstraint() const          { return _range_constraint ; }
    VhdlIdDef *                 GetRangeType() const                { return _range_type ; }

    // VIPER #7343 : Return following information from range of explicit subtype indication
    virtual VhdlExpression *    GetLeftExpression() const ;
    virtual unsigned            GetDir() const ;
    virtual VhdlExpression *    GetRightExpression() const ;
    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlName            *_res_function ;
    VhdlName            *_type_mark ;
    VhdlDiscreteRange   *_range_constraint ;
// back-pointers set during analysis (not owned) :
    VhdlIdDef           *_range_type ; // resolved type of the subtype indication.
} ; // class VhdlExplicitSubtypeIndication

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlRange : public VhdlDiscreteRange
{
public:
    VhdlRange(VhdlExpression *left, unsigned dir, VhdlExpression *right) ;

    // Copy tree node
    VhdlRange(const VhdlRange &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlRange(SaveRestore &save_restore);

    virtual ~VhdlRange() ;

private:
    // Prevent compiler from defining the following
    VhdlRange() ;                             // Purposely leave unimplemented
    VhdlRange(const VhdlRange &) ;            // Purposely leave unimplemented
    VhdlRange& operator=(const VhdlRange &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLRANGE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy range.
    virtual VhdlDiscreteRange * CopyDiscreteRange(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Static Elaboration :
    virtual VhdlName *          ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;
    virtual void                ReplaceBoundsInSubtypeIndication(VhdlDataFlow *df) ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const ;

    // Parse-tree manipulation methods:
    // VIPER #6896: Conversion of verilog package to vhdl package
    virtual void                FillRange(unsigned left_bound, unsigned right_bound) ; // fillup range

    // Accessor methods
    virtual VhdlExpression *    GetLeftExpression() const           { return _left ; }
    virtual unsigned            GetDir() const                      { return _dir ; }
    virtual VhdlExpression *    GetRightExpression() const          { return _right ; }

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl range to verilog range: during analysis create unconstraint ranges
    virtual VeriRange *         ConvertRange(VeriScope *scope, Array *type_id_arr) const ;
#endif

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
    virtual VhdlConstraint *    EvaluateConstraintPhysical(VhdlDataFlow *df) ; // For evaluating constraint of physical typedefs
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned)
    VhdlExpression  *_left ;
    unsigned         _dir ;  // VHDL_to, VHDL_downto
    VhdlExpression  *_right ;
} ; // class VhdlRange

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlBox : public VhdlExpression
{
public:
    VhdlBox() ;

    // Copy tree node
    VhdlBox(const VhdlBox &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBox(SaveRestore &save_restore);

    virtual ~VhdlBox() ;

private:
    // Prevent compiler from defining the following
    VhdlBox(const VhdlBox &) ;            // Purposely leave unimplemented
    VhdlBox& operator=(const VhdlBox &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLBOX; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual VhdlIdDef *         SubprogramAspect(VhdlIdDef* /*formal*/) ;

    // Following API follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsGloballyStatic() const ;

    virtual unsigned            IsBox() const        { return 1 ; }

    // Copy box.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    virtual unsigned            IsUnconstrained(unsigned all_level) const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl range to verilog range: during analysis create unconstraint ranges
    virtual VeriRange *         ConvertRange(VeriScope *scope, Array *type_id_arr) const ;
#endif

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned get_driver_value) ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() { return ; }
#endif
} ; // class VhdlBox

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAssocElement : public VhdlExpression
{
public:
    VhdlAssocElement(VhdlName *formal_part, VhdlExpression *actual_part) ;

    // Copy tree node
    VhdlAssocElement(const VhdlAssocElement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAssocElement(SaveRestore &save_restore);

    virtual ~VhdlAssocElement() ;

private:
    // Prevent compiler from defining the following
    VhdlAssocElement() ;                                    // Purposely leave unimplemented
    VhdlAssocElement(const VhdlAssocElement &) ;            // Purposely leave unimplemented
    VhdlAssocElement& operator=(const VhdlAssocElement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLASSOCELEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy association element
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new formal name in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new actual in its place

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    virtual unsigned            IsAssoc() const ;
    virtual unsigned            IsOpen() const ;

    // Accessor methods
    virtual VhdlName *          FormalPart() const   { return _formal_part ; }
    virtual VhdlExpression *    ActualPart() const   { return _actual_part ? (_actual_part->IsInertial() ? _actual_part->ActualPart() : _actual_part) : 0 ; }
    virtual unsigned            IsInertial() const   { return _actual_part && _actual_part->IsInertial() ? 1 : 0 ; }

    virtual VhdlExpression *    TakeActualPart()     { VhdlExpression *tmp = _actual_part ; _actual_part = 0 ; return tmp ; }

    virtual VhdlIdDef *         FindFullFormal() const ; // Return IdDef if it's the single (formal) id in this expression
    virtual VhdlIdDef *         FindFormal() const ;     // Return IdDef in this expression (find through prefix naming too).

    virtual unsigned            FromVerilog() const { return _from_verilog ; }
    virtual void                SetFromVerilog()    { _from_verilog = 1 ; }

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    // Assign to an expression
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif
    // Associate formal-actual. For concurrent areas (component/entity/block assocs).
    virtual void                PartialAssociation(Instance *inst, VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned)
    VhdlName        *_formal_part ;
    VhdlExpression  *_actual_part ;
    unsigned        _from_verilog:1 ;
} ; // class VhdlAssocElement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlInertialElement : public VhdlExpression
{
public:
    explicit VhdlInertialElement(VhdlExpression *actual_part) ;

    // Copy tree node
    VhdlInertialElement(const VhdlInertialElement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInertialElement(SaveRestore &save_restore);

    virtual ~VhdlInertialElement() ;

private:
    // Prevent compiler from defining the following
    VhdlInertialElement() ;                                    // Purposely leave unimplemented
    VhdlInertialElement(const VhdlInertialElement &) ;            // Purposely leave unimplemented
    VhdlInertialElement& operator=(const VhdlInertialElement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLINERTIALELEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy association element
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new actual in its place

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    virtual unsigned            IsOpen() const ;

    // Accessor methods
    virtual VhdlExpression *    ActualPart() const   { return _actual_part ; }

    virtual VhdlExpression *    TakeActualPart()     { VhdlExpression *tmp = _actual_part ; _actual_part = 0 ; return tmp ; }

    virtual unsigned            IsInertial() const  { return 1 ; }

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    // Assign to an expression
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned)
    VhdlExpression  *_actual_part ;
} ; // class VhdlInertialElement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlRecResFunctionElement : public VhdlExpression
{
public:
    VhdlRecResFunctionElement(VhdlName *record_id_name, VhdlName *record_id_function) ;

    // Copy tree node
    VhdlRecResFunctionElement(const VhdlRecResFunctionElement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlRecResFunctionElement(SaveRestore &save_restore);

    virtual ~VhdlRecResFunctionElement() ;

private:
    // Prevent compiler from defining the following
    VhdlRecResFunctionElement() ;                                    // Purposely leave unimplemented
    VhdlRecResFunctionElement(const VhdlRecResFunctionElement &) ;            // Purposely leave unimplemented
    VhdlRecResFunctionElement& operator=(const VhdlRecResFunctionElement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLRECRESFUNCTIONELEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy association element
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new id name in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/) ; // Delete 'old_node', puts new res-func in its place

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    virtual unsigned            IsRecResFuncElement() const ;

    // Accessor methods
    virtual VhdlName *          RecordIdName() const        { return _record_id_name ; }
    virtual VhdlName *          RecordResFunction() const   { return _record_id_function ; }

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/) ;

protected:
// Parse tree nodes (owned)
    VhdlName        *_record_id_name ;
    VhdlName        *_record_id_function ;
} ; // class VhdlRecResFunctionElement
/* -------------------------------------------------------------- */

// Operators
class VFC_DLL_PORT VhdlOperator : public VhdlExpression
{
public:
    VhdlOperator(VhdlExpression *left, unsigned oper, VhdlExpression *right) ;

    // Copy tree node
    VhdlOperator(const VhdlOperator &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlOperator(SaveRestore &save_restore);

    virtual ~VhdlOperator() ;

private:
    // Prevent compiler from defining the following
    VhdlOperator() ;                                // Purposely leave unimplemented
    VhdlOperator(const VhdlOperator &) ;            // Purposely leave unimplemented
    VhdlOperator& operator=(const VhdlOperator &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLOPERATOR; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action);

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy binary/unary operators.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new operand in its place.

    virtual VhdlName *          ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;

    virtual VhdlIdDef *         TypeInferIterative(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const ;

    virtual unsigned            FindIfEdge() const ; // Find out if this condition expression is a 'clock' edge expression. Used for VIPER 3603.
    virtual unsigned            CannotMatchType(VhdlIdDef* type_id) const ; // Viper 1493(4400)

    // Accessor methods
    virtual VhdlExpression *    GetLeftExpression() const           { return _left ; }
    virtual unsigned            GetOperatorToken() const            { return _oper ; }
    virtual VhdlExpression *    GetRightExpression() const          { return _right ; }
    virtual void                SetOperator(VhdlIdDef *oper)        { _operator = oper ; } // VIPER #8170
    virtual VhdlIdDef *         GetOperator() const                 { return _operator ; }
    virtual unsigned            IsVhdlOperator()                    { return 1 ; } // VIPER #8170
    virtual unsigned            FindVisibleOperators(Map &operator_map) ;
    virtual unsigned            FindVisibleOperatorsFromMap(Map &operator_map, Map &new_operator_map) ;
    virtual unsigned            PruneVisibleOperators(Map &orig_operator_map, Map &operator_map, Map &expected_types_map, Set &already_visited, unsigned environment, VhdlScope *selection_scope) ; // VIPER #8170

    virtual char *              ExprString() const ; // Viper #7042

    virtual verific_int64       EvaluateConstantInteger() ; // VIPER #6646: For attribute dimension ; before elaboration
    virtual unsigned            IsGoodForPortActual() const ;

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlConstraint *    EvaluateIntegerRangeConstraint(VhdlDataFlow *df) ; // Viper #6473
    virtual void SplitAndedSubExpressions(Array& anded_expressions_array) ; // Added for VIPER #3907
    virtual VhdlValue *         EvaluateCondition(VhdlDataFlow *df, Map *&condition_id_value_map) ; // Viper #5553
    virtual void                SetImplicit()        { _is_implicit = 1 ; }
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow * df, VhdlConstraint * /*target_constraint*/) ;

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_left ;
    unsigned         _oper:28 ;  // Token representing the operator (VHDL_and, VHDL_xor, VHDL_not, VHDL_minus etc) : Made size 28 as is done in operatorid
    unsigned         _is_implicit:1 ; // flag to refer to the implicit conditional operator
    unsigned         _unused:3 ;
    VhdlExpression  *_right ;    // Unary operators have _right==0.
// Field set during analysis (not owned)
    VhdlIdDef       *_operator ; // The type-resolved matching function identifier for this operator call. Always set during type-inference.
} ; // class VhdlOperator

/* -------------------------------------------------------------- */

// This needs extension
class VFC_DLL_PORT VhdlAllocator : public VhdlExpression
{
public:
    explicit VhdlAllocator(VhdlExpression *subtype) ;

    // Copy tree node
    VhdlAllocator(const VhdlAllocator &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAllocator(SaveRestore &save_restore);

    virtual ~VhdlAllocator() ;

private:
    // Prevent compiler from defining the following
    VhdlAllocator() ;                                 // Purposely leave unimplemented
    VhdlAllocator(const VhdlAllocator &) ;            // Purposely leave unimplemented
    VhdlAllocator& operator=(const VhdlAllocator &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLALLOCATOR; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsGloballyStatic() const ; // it is never locally static
    virtual unsigned            IsAllocator() const  { return 1 ; } // Return 1 for allocator

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy allocators.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specofic parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    virtual VhdlName *          ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    // Accessor method
    VhdlExpression *            GetSubtype() const  { return _subtype ; }

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree node (owned)
    VhdlExpression  *_subtype ;
} ; // class VhdlAllocator

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAggregate : public VhdlExpression
{
public:
    explicit VhdlAggregate(Array *element_assoc_list) ;

    // Copy tree node
    VhdlAggregate(const VhdlAggregate &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAggregate(SaveRestore &save_restore);

    virtual ~VhdlAggregate() ;

private:
    // Prevent compiler from defining the following
    VhdlAggregate() ;                                 // Purposely leave unimplemented
    VhdlAggregate(const VhdlAggregate &) ;            // Purposely leave unimplemented
    VhdlAggregate& operator=(const VhdlAggregate &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLAGGREGATE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    virtual unsigned            IsAggregate() const ;
    virtual unsigned            IsOthersAggregate() const ; // VIPER 7081 : Check if this is an aggregate with an 'others' clause.

    virtual unsigned            IsUsed() const ; // Viper #5700

    virtual unsigned            NumElements() const ;
    virtual VhdlExpression const *GetElementExpressionAt(unsigned idx) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    virtual char *              ExprString() const ; // Viper #7042: only for aggregate of string literals

    // Viper #5073
    virtual unsigned            IsLocallyStaticAggregateTarget() const ;

    // Copy aggregate.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range/expression in its place.

    // Insert new range/expression before/after 'target_node' in the list of range/expressions.
    // If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ;
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            CannotMatchType(VhdlIdDef *type_id) const ; // Quick check if this unresolved expression can certainly not match this type. Viper 1493(4400)

    virtual void                CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const ;

    // Accessor methods
    Array *                     GetElementAssocList() const     { return _element_assoc_list ; }
    VhdlIdDef *                 GetAggregateType() const        { return _aggregate_type ; }
    unsigned                    IsAggregateTypeAssoc(unsigned assoc_idx) const ;

    virtual void                MarkUsedFlag(SynthesisInfo *info) ;
    virtual unsigned            IsGoodForPortActual() const ; // Added for Viper 7645

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    // Evaluate the subtype of the aggregate :
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif
    // Assign to an aggregate
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
    virtual verific_uint64      NumAggregateElements(VhdlDataFlow *df) ; // Used for Vhdl 2008 array aggregate elaboration. The object is guranteed to be an array type lvalue. Returns the number of elements it has.

    void                        SetAggregateType(VhdlIdDef *t) ;
protected:
// Parse tree nodes (owned) :
    Array       *_element_assoc_list ;  // Array of VhdlDiscreteRange*.
// Field set during analysis (not owned)
    VhdlIdDef   *_aggregate_type ;      // resolved type of this aggregate. Always set during type-inference.
    Array       *_element_type_list ;   // _aggregate_type or _element_type
} ; // class VhdlAggregate

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlQualifiedExpression : public VhdlExpression
{
public:
    VhdlQualifiedExpression(VhdlName *prefix, VhdlExpression *aggregate) ;

    // Copy tree node
    VhdlQualifiedExpression(const VhdlQualifiedExpression &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlQualifiedExpression(SaveRestore &save_restore);

    virtual ~VhdlQualifiedExpression() ;

private:
    // Prevent compiler from defining the following
    VhdlQualifiedExpression() ;                                           // Purposely leave unimplemented
    VhdlQualifiedExpression(const VhdlQualifiedExpression &) ;            // Purposely leave unimplemented
    VhdlQualifiedExpression& operator=(const VhdlQualifiedExpression &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLQUALIFIEDEXPRESSION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree for qualified expression.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new prefix name in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', inserts new expression in its place.

    virtual VhdlName *          ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    virtual unsigned            IsQualifiedExpression() const ;

    virtual unsigned            IsGoodForPortActual() const ;

    // Accessor methods
    virtual VhdlName *          GetPrefix() const           { return _prefix ; }
    VhdlExpression *            GetAggregate() const        { return _aggregate ; }

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    // Evaluate the subtype of the qualified expression
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
    virtual VhdlValue *         EvaluateAllocatorSubtype(VhdlConstraint *target_constraint, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned)
    VhdlName        *_prefix ;
    VhdlExpression  *_aggregate ;
} ; // class VhdlQualifiedExpression

/* -------------------------------------------------------------- */

// Class used in VhdlAggregate :
class VFC_DLL_PORT VhdlElementAssoc : public VhdlExpression
{
public:
    VhdlElementAssoc(Array *choices, VhdlExpression *expr) ;

    // Copy tree node
    VhdlElementAssoc(const VhdlElementAssoc &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlElementAssoc(SaveRestore &save_restore);

    virtual ~VhdlElementAssoc() ;

private:
    // Prevent compiler from defining the following
    VhdlElementAssoc() ;                                    // Purposely leave unimplemented
    VhdlElementAssoc(const VhdlElementAssoc &) ;            // Purposely leave unimplemented
    VhdlElementAssoc& operator=(const VhdlElementAssoc &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLELEMENTASSOC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Insert new range before/after 'target_node' in the list of range. If 'target_node' is null, no insertion occurs.
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ;
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual VhdlIdDef *         TypeInferChoices(VhdlIdDef *mother_type, Set *done_choices, unsigned n_assoc_elements) ; // Viper #4512 : Added n_assoc_elements argument
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const ;

    virtual unsigned            IsAssoc() const ;
    virtual unsigned            IsOthers() const ;

    virtual unsigned            IsUsed() const ; // Viper #5700

    // Accessor methods
    virtual Array *             GetChoices() const              { return _choices ; }
    virtual VhdlExpression *    GetExpression() const           { return _expr ; }

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;

    virtual void                EvaluateByChoice(Array *result_values, VhdlConstraint *constraint, VhdlDataFlow *df, VhdlValue *& default_value, Set *record_ele_pos, unsigned is_aggregate_type) ; // Viper #4950 : Added default_value argument, Viper #7417/7477: is_aggregate_type added
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif
    virtual void                AssignByChoice(VhdlValue *assign_value, VhdlConstraint *constraint, VhdlDataFlow *df, unsigned is_aggregate_type) ;
    virtual VhdlConstraint *    EvaluateConstraintByChoice(VhdlConstraint *previous_constraint, VhdlDataFlow *df, unsigned is_aggregate_type, unsigned check_dir) ; // Build the constraint of an aggregate based on the (named) index formals. Start with 0 and call for each element.

protected:
// Parse tree nodes (owned) :
    Array           *_choices ; // VhdlDiscreteRange *
    VhdlExpression  *_expr ;
} ; // class VhdlElementAssoc

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlWaveformElement : public VhdlExpression
{
public:
    VhdlWaveformElement(VhdlExpression *value, VhdlExpression *after) ;

    // Copy tree node
    VhdlWaveformElement(const VhdlWaveformElement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlWaveformElement(SaveRestore &save_restore);

    virtual ~VhdlWaveformElement() ;

private:
    // Prevent compiler from defining the following
    VhdlWaveformElement() ;                                       // Purposely leave unimplemented
    VhdlWaveformElement(const VhdlWaveformElement &) ;            // Purposely leave unimplemented
    VhdlWaveformElement& operator=(const VhdlWaveformElement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLWAVEFORMELEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    virtual unsigned            IsUnaffected() const ;
    virtual unsigned            IsAggregate() const ;
    virtual unsigned            IsOthersAggregate() const ; // VIPER 7081 : Check if this is an aggregate with an 'others' clause.
    virtual unsigned            IsNull() const ; // Check if expression is a NULL literal

    // Copy waveform element.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const ;

    // Accessor methods
    VhdlExpression *            GetValue() const        { return _value ; }
    virtual VhdlExpression *    GetAfter() const        { return _after ; }

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint *target_constraint) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned)
    VhdlExpression  *_value ;
    VhdlExpression  *_after ;
} ; // class VhdlWaveformElement

/* -------------------------------------------------------------- */
// VIPER #5918 : VHDL-2008 : Default keyword can be used in generic map aspect
// of interface package declaration
//    generic map (default)
class VFC_DLL_PORT VhdlDefault : public VhdlExpression
{
public:
    VhdlDefault() ;

    // Copy tree node
    VhdlDefault(const VhdlDefault &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlDefault(SaveRestore &save_restore);

    virtual ~VhdlDefault() ;

private:
    // Prevent compiler from defining the following
    VhdlDefault(const VhdlDefault &) ;            // Purposely leave unimplemented
    VhdlDefault& operator=(const VhdlDefault &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLDEFAULT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;
    virtual unsigned            IsDefault() const { return 1 ; }

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy box.
    virtual VhdlExpression *    CopyExpression(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    // Elaboration
    // for elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned get_driver_value) ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() { return ; }
#endif
} ; // class VhdlDefault

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_EXPRESSION_H_

