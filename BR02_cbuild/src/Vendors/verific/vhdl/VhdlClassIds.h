/*
 *
 * [ File Version : 1.44 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_CLASSIDS_H_
#define _VERIFIC_VHDL_CLASSIDS_H_

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

// 10/2008: We have some dependencies on the starting value of the class ids in the binary
// save-restore utility. Previously it used the hard coded value 0xc0000000. After a customer
// suggested that we should really use a named constant (like #define) we decided to implement
// that but Verilog side has a nice dummy beginning enum literal for class ids which is very
// meaningfull. Unfortunately VHDL side, for historical reason, missing that. Decided to add
// a 'const' declaration to mimic what Verilog side actually have. Now we use this named
// constant in all the required places including binary save-restore utility.
const unsigned ID_VHDL_BEGIN_TABLE = 0xc0000000 ;

// The following enum contains all VHDL class ids.  We use GetClassId()
// instead of RTTI, because ANSI's C++ RTTI is too expensive.  These ids
// are also used during the saving and restoring of a VHDL parse tree.

enum VHDL_CLASS_ID
{
    // ClassId's start from the offset 0xc0000000, so this allows for ~1 billion pointer ids.
    // This pertains to Verific's VHDL Save/Restore algorithm.  If there are more than ~1 billion
    // pointer ids, then this scheme will break during restore!!!

    // Class Ids follow:
    ID_VHDLSCOPE = ID_VHDL_BEGIN_TABLE,      ID_VHDLFORSCHEME,                        ID_VHDLUNIVERSALINTEGER,
    ID_VHDLBLOCKPORTS,                       ID_VHDLPROCESSSTATEMENT,                 ID_VHDLBLOCKSTATEMENT,
    ID_VHDLSIGNALASSIGNMENTSTATEMENT,        ID_VHDLDECLARATION,                      ID_VHDLARRAYTYPEDEF,
    ID_VHDLINDEXEDNAME,                      ID_VHDLDELAYMECHANISM,                   ID_VHDLCONSTANTID,
    ID_VHDLSIGNATUREDNAME,                   ID_VHDLSUBTYPEINDICATION,                ID_VHDLCONFIGURATIONITEM,
    ID_VHDLBLOCKGENERICS,                    ID_VHDLPACKAGEBODYID,                    ID_VHDLEDGE,
    ID_VHDLNEXTSTATEMENT,                    ID_VHDLTYPEDEF,                          ID_VHDLASSERTIONSTATEMENT,
    ID_VHDLALL,                              ID_VHDLOPERATOR,                         ID_VHDLENTITYASPECT,
    ID_VHDLATTRIBUTEID,                      ID_VHDLDISCRETERANGE,                    ID_VHDLINTERFACEDECL,
    ID_VHDLCONSTANTDECL,                     ID_VHDLASSOCELEMENT,                     ID_VHDLPHYSICALUNITID,
    ID_VHDLSUBPROGRAMBODY,                   ID_VHDLPACKAGEDECL,                      ID_VHDLCONFIGURATIONSPEC,
    ID_VHDLARCHITECTUREBODY,                 ID_VHDLQUALIFIEDEXPRESSION,              ID_VHDLOPEN,
    ID_VHDLCONFIGURATIONDECL,                ID_VHDLOPERATORID,                       ID_VHDLSUBTYPEDECL,
    ID_VHDLDESIGNUNIT,                       ID_VHDLSIGNALID,                         ID_VHDLGOTOAREA,
    ID_VHDLCASESTATEMENTALTERNATIVE,         ID_VHDLENTITYCLASSENTRY,                 ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT,
    ID_VHDLPHYSICALLITERAL,                  ID_VHDLSELECTEDNAME,                     ID_VHDLCONFIGURATIONID,
    ID_VHDLARCHITECTUREID,                   ID_VHDLTRANSPORT,                        ID_VHDLNULL,
    ID_VHDLPROCEDURECALLSTATEMENT,           ID_VHDLGUARDEDSIGNALSPEC,                ID_VHDLCONDITIONALWAVEFORM,
    ID_VHDLACCESSTYPEDEF,                    ID_VHDLLABELID,                          ID_VHDLCHARACTERLITERAL,
    ID_VHDLFILETYPEDEF,                      ID_VHDLUSECLAUSE,                        ID_VHDLPRIMARYUNIT,
    ID_VHDLSIGNATURE,                        ID_VHDLINCOMPLETETYPEDECL,               ID_VHDLGROUPDECL,
    ID_VHDLANONYMOUSTYPE,                    ID_VHDLSCALARTYPEDEF,                    ID_VHDLWAITSTATEMENT,
    ID_VHDLCOMPONENTCONFIGURATION,           ID_VHDLDISCONNECTIONSPEC,                ID_VHDLTYPEID,
    ID_VHDLCOMPOSITEVALUE,                   ID_VHDLCONSTRAINT,                       ID_VHDLZVALUE,
    ID_VHDLIFSCHEME,                         ID_VHDLCOMPONENTID,                      ID_VHDLFILEID,
    ID_VHDLFUNCTIONSPEC,                     ID_VHDLELEMENTDECL,                      ID_VHDLSELECTEDSIGNALASSIGNMENT,
    ID_VHDLPHYSICALTYPEDEF,                  ID_VHDLLIBRARYID,                        ID_VHDLPHYSICALUNITDECL,
    ID_VHDLDESIGNATOR,                       ID_VHDLPROCEDURESPEC,                    ID_VHDLENTITYSPEC,
    ID_VHDLNONCONSTBIT,                      ID_VHDLATTRIBUTESPEC,                    ID_VHDLSECONDARYUNIT,
    ID_VHDLENUMERATIONTYPEDEF,               ID_VHDLINT,                              ID_VHDLGENERATESTATEMENT,
    ID_VHDLENTITYDECL,                       ID_VHDLATTRIBUTEDECL,                    ID_VHDLNULLSTATEMENT,
    ID_VHDLALIASID,                          ID_VHDLLOOPSTATEMENT,                    ID_VHDLCASESTATEMENT,
    ID_VHDLPACKAGEID,                        ID_VHDLITERSCHEME,                       ID_VHDLTREENODE,
    ID_VHDLRANGE,                            ID_VHDLBLOCKCONFIGURATION,               ID_VHDLSUBTYPEID,
    ID_VHDLINERTIALDELAY,                    ID_VHDLEXPLICITSUBTYPEINDICATION,        ID_VHDLGROUPID,
    ID_VHDLSUBPROGRAMDECL,                   ID_VHDLDATAFLOW,                         ID_VHDLNAME,
    ID_VHDLREPORTSTATEMENT,                  ID_VHDLRECORDTYPEDEF,                    ID_VHDLINTERFACEID,
    ID_VHDLSELECTEDWAVEFORM,                 ID_VHDLRETURNSTATEMENT,                  ID_VHDLDOUBLE,
    ID_VHDLUNIVERSALREAL,                    ID_VHDLVARIABLEID,                       ID_VHDLEXPRESSION,
    ID_VHDLCOMPONENTSPEC,                    ID_VHDLEVENT,                            ID_VHDLPACKAGEBODY,
    ID_VHDLREAL,                             ID_VHDLCOMPONENTDECL,                    ID_VHDLELSIF,
    ID_VHDLALLOCATOR,                        ID_VHDLSTATEMENT,                        ID_VHDLINTEGER,
    ID_VHDLLIBRARY,                          ID_VHDLLIBRARYCLAUSE,                    ID_VHDLIDREF,
    ID_VHDLWRITE,                            ID_VHDLVALUE,                            ID_VHDLWAVEFORMELEMENT,
    ID_VHDLVARIABLEASSIGNMENTSTATEMENT,      ID_VHDL_FILE,                            ID_VHDLEXITSTATEMENT,
    ID_VHDLOPTIONS,                          ID_VHDLRECORDCONSTRAINT,                 ID_VHDLINSTANTIATEDUNIT,
    ID_VHDLARRAYCONSTRAINT,                  ID_VHDLFILEOPENINFO,                     ID_VHDLIFSTATEMENT,
    ID_VHDLGROUPTEMPLATEDECL,                ID_VHDLBITSTRINGLITERAL,                 ID_VHDLNODE,
    ID_VHDLALIASDECL,                        ID_VHDLENUMERATIONID,                    ID_VHDLFULLTYPEDECL,
    ID_VHDLENUM,                             ID_VHDLSPECIFICATION,                    ID_VHDLRANGECONSTRAINT,
    ID_VHDLSUBPROGRAMID,                     ID_VHDLELEMENTID,                        ID_VHDLAGGREGATE,
    ID_VHDLLITERAL,                          ID_VHDLUNAFFECTED,                       ID_VHDLELEMENTASSOC,
    ID_VHDLSTRINGLITERAL,                    ID_VHDLGROUPTEMPLATEID,                  ID_VHDLNONCONST,
    ID_VHDLSIGNALDECL,                       ID_VHDLFILEDECL,                         ID_VHDLENTITYID,
    ID_VHDLCONDITIONALSIGNALASSIGNMENT,      ID_VHDLWHILESCHEME,                      ID_VHDLOTHERS,
    ID_VHDLATTRIBUTENAME,                    ID_VHDLIDDEF,                            ID_VHDLVARIABLEDECL,
    ID_VHDLBOX,                              ID_VHDLBINDINGINDICATION,                ID_VHDLBLOCKID,

    ID_VHDLCOMMENTNODE,                      ID_VHDLPROTECTEDTYPEID,                  ID_VHDLPROTECTEDTYPEDEF,
    ID_VHDLPROTECTEDTYPEDEFBODY,             ID_VHDLPROTECTEDTYPEBODYID,

    ID_VHDLCONTEXTID,                        ID_VHDLCONTEXTDECL,                      ID_VHDLCONTEXTREFERENCE,
    ID_VHDLSELECTEDVARIABLEASSIGNMENTSTATEMENT, ID_VHDLSELECTEDEXPRESSION,            ID_VHDLCONDITIONALEXPRESSION,
    ID_VHDLCONDITIONALVARIABLEASSIGNMENTSTATEMENT, ID_VHDLELSIFELSESCHEME,            ID_VHDLCASEITEMSCHEME,
    ID_VHDLIFELSIFGENERATESTATEMENT,         ID_VHDLCASEGENERATESTATEMENT,            ID_VHDLPACKAGEINSTANTIATIONDECL,
    ID_VHDLSUBPROGINSTANTIATIONDECL,         ID_VHDLINTERFACETYPEDECL,                ID_VHDLINTERFACESUBPROGDECL,
    ID_VHDLINTERFACEPACKAGEDECL,             ID_VHDLDEFAULT,                          ID_VHDLFORCEASSIGNMENTSTATEMENT,
    ID_VHDLRELEASEASSIGNMENTSTATEMENT,       ID_VHDLCONDITIONALFORCEASSIGNMENTSTATEMENT, ID_VHDLARRAYRESFUNCTION,
    ID_VHDLRECORDRESFUNCTION,                ID_VHDLRECRESFUNCTIONELEMENT,            ID_VHDLEXTERNALNAME,
    ID_VHDLINERTIALELEMENT,                  ID_VHDLPACKAGEINSTELEMENT,               ID_VHDLGENERICTYPEID,

    ID_VHDLSELECTEDFORCEASSIGNMENT,
    ID_VHDL_END // the last marker
};

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_CLASSIDS_H_
