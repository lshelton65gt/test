/*
 *
 * [ File Version : 1.260 - 2014/03/12 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_IDDEF_H_
#define _VERIFIC_VHDL_IDDEF_H_

#include <stdio.h> // for FILE
#include "VhdlTreeNode.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Map ;
class Set ;

class DesignObj ; // a netlist object
class Instance ;

class VhdlSubprogramBody ;
class VhdlExpression ;
class VhdlName ;

class VhdlConstraint ;
class VhdlValue ;
class VhdlDataFlow ;
class VhdlBindingIndication ;
class VhdlBlockConfiguration ;
class VhdlSpecification ;

class VhdlPrimaryUnit ;
class VhdlSecondaryUnit ;
class VhdlLibrary ;
class VhdlComponentDecl ;
class VhdlStatement ;
class VhdlAttributeId ;
class VhdlProtectedTypeDef ;
class VhdlMapForCopy ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
class VeriIdDef ;
#endif

class SynthesisInfo ; // defined in VeriId.h

class VhdlVarUsageInfo ;

// Define iterator of all attributes on a IdDef :
//    OBJ is the VhdlIdDef,
//    I is a MapIter,
//    ID is the (VhdlIdDef*) id of the attribute (should always be an VhdlAttributeId*),
//    ATT_VAL is the (VhdlExpression*) value of the attribute. NOTE : old versions used elab-only VhdlValue in ATT_VAL field.
// In elaboration : Call Evaluate() on the VhdlExpression to obtain the VhdlValue structure.
#define FOREACH_ATTRIBUTE_OF_VHDL_ID(OBJ,I,ID,ATT_VAL)  if (OBJ) FOREACH_MAP_ITEM((OBJ)->_attributes,I,&(ID),&(ATT_VAL))

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlIdDef : public VhdlTreeNode
{
public:
    explicit VhdlIdDef(char *name) ; // Absorb the name (no internal copy made) !

    // Copy tree node
    VhdlIdDef(const VhdlIdDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlIdDef(SaveRestore &save_restore);

    virtual ~VhdlIdDef() ;

private:
    // Prevent compiler from defining the following
    VhdlIdDef() ;                             // Purposely leave unimplemented
    VhdlIdDef(const VhdlIdDef &) ;            // Purposely leave unimplemented
    VhdlIdDef& operator=(const VhdlIdDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLIDDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ; // Used only for binary save/restore backward-compatibility

    // Copy parse-tree.
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;
    virtual void                SetTargetName(VhdlName * /*target_name*/)       { VERIFIC_ASSERT(0) ; } // Set back pointer to alias identifier
    virtual void                SetTargetId(VhdlIdDef * /*id*/) { VERIFIC_ASSERT(0) ; }
    virtual void                DeclareForCopy(VhdlIdDef * /*old_type_id*/, VhdlMapForCopy &/*old2new*/) { VERIFIC_ASSERT(0) ; } // Set back pointers in type identifier
    virtual void                UpdateType(VhdlMapForCopy &old2new) ;
    virtual void                UpdateArgsAndType(VhdlMapForCopy &/*old2new*/)  { } // Update _type field of identifiers and the arguments of operator id.
    virtual void                UpdateAnonymousType(VhdlIdDef * /*old_type_id*/, VhdlMapForCopy &/*old2new*/)  { } // Viper #6146 : Update _list field and declare the type

    // Parse-tree port/generic add routines
    virtual void                AddPort(VhdlIdDef * /*id*/) {} // Add port identifier as back pointer to entity/component id
    virtual void                AddGeneric(VhdlIdDef * /*id*/) {} // Add generic identifier as back pointer to entity/component id

    // Type infer an association (port_ref/generic-ref)
    virtual void                TypeInferAssociation(VhdlDiscreteRange *assoc, unsigned object_kind, VhdlScope *actual_binding_scope, const VhdlTreeNode * /*from*/) ;

    // Static Elaboration
    void                        ChangeName(char *new_name, unsigned add_in_lib=0) ; // Change the name of the identifier. Argument specific name is absorbed.
    virtual void                SetHasInitAssign()      { }  // Set _has_init_assign field.
    virtual void                ResetHasInitAssign()    { }
    virtual void                SetConstrained()        { }  // Set _is_unconstrained field to '0'

    virtual void                SetIsProcessing(unsigned /*s*/)  {}
    virtual unsigned            IsProcessing() const         { return 0 ; }
    // VIPER #4547 : Set/get routines to indicate whether valid actual (not open)
    // is specified for this port of component :
    virtual void                SetIsActualPresent()    {}
    virtual unsigned            IsActualPresent() const { return 0 ; }

    virtual void                SetRefFormal(unsigned /*is_ref_formal*/)        {  }
    virtual unsigned            IsRefFormal() const     { return 0 ; }
    virtual unsigned            IsPackageInstElement() const { return 0 ; }

    // Name
    const char *                Name() const ;
    virtual const VhdlIdDef *   TreeNodeId() const           { return this ; } // VIPER 4367. Same as this only made virtual for access from VeriTreeNode
    const char *                OrigName() const ; // returns original name if case-preservation (VHDL_PRESERVE_ID_CASE) is set.
    void                        ChangeIdName(char *new_name) ;

    // Type testing
    virtual unsigned            IsHomograph(VhdlIdDef *id) const ;

    // Local scope of the construct that this identifier defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;
    virtual VhdlIdDef *         ActualType() const { return _type ; }
    virtual void                SetActualTypes() ; // defined only for package inst
    virtual void                ConnectActualId(VhdlIdDef * /*ai*/) ;
    virtual VhdlIdDef *         GetConnectedActualId() const { return 0 ; }

#ifdef VHDL_ID_SCOPE_BACKPOINTER
    // Analysis : find out the context in which this identifier is declared :
    VhdlScope                  *GetOwningScope() const           { return _owning_scope ; } // Issue 2716 : back-pointer to the scope in which this identifier is declared.
    void                        SetOwningScope(VhdlScope *scope) { _owning_scope = scope ; }
#endif

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // Analysis : find the subtype indication of this identifier (set during analysis).
    VhdlSubtypeIndication      *GetSubtypeIndication() const { return _subtype_indication ; } // back-pointer to the subtype indication (parse tree node from the declaration of this identifier).
    void                        SetSubtypeIndication(VhdlSubtypeIndication *subtype_indication) { _subtype_indication = subtype_indication ; }

    // VIPER #7343 : Return the dimension (MSB dimension first) which is 'dimension'
    // dimensions deep into this identifier
    VhdlDiscreteRange *         GetDimensionAt(unsigned dimension) const ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                SetVerilogActualWidth(unsigned /*w*/) {}
    virtual unsigned            GetVerilogActualWidth() const{ return 0 ; }
    // Accessor functions to access Verilog pkg/lib names
    virtual void                SetVerilogPkgName(const char * /*pkg_name*/) {}
    virtual void                SetVerilogPkgLibName(const char * /*pkg_lib_name*/) {}
    virtual unsigned            IsVerilogType() const  { return 0 ; }
    virtual VeriIdDef *         GetVerilogTypeFromPackage() const { return 0 ; }
#endif
    virtual void                SetVerilogAssocList(Array* /*assoc_list*/) {  }
    virtual Array*              GetVerilogAssocList() const       { return 0 ; }
    // Get the library name (of the unit) in which this identifier resides
    // Works for identifiers with their own scope only : design units, components, subprog's etc
    virtual const char *        GetContainingLibraryName() const ;

    // Return the entity class as a VHDL token :
    virtual unsigned            EntityClass() const ;

    // identifier tests
    virtual unsigned            IsLabel() const ;
    virtual unsigned            IsLibrary() const ;
    virtual unsigned            IsEntity() const ;
    virtual unsigned            IsArchitecture() const ;
    virtual unsigned            IsConfiguration() const ;
    virtual unsigned            IsPackage() const ;
    virtual unsigned            IsContext() const ;
    virtual unsigned            IsPackageBody() const ;
    virtual unsigned            IsDesignUnit() const ; // Entity, architecture, configuration, architecture, package or package body.
    virtual unsigned            IsPackageInst() const ;
    virtual unsigned            IsComponent() const ;
    virtual unsigned            IsComponentInstantiation() const ;
    virtual unsigned            IsSubprogram() const ;
    virtual unsigned            IsSubprogramInst() const ;
    virtual unsigned            IsPslDeclId() const { return 0 ; }  // VIPER #4508
    virtual unsigned            IsAttribute() const ;
    virtual unsigned            IsEnumerationLiteral() const ;
    virtual unsigned            IsPhysicalUnit() const ;
    virtual unsigned            IsPredefinedOperator() const ;
    virtual unsigned            IsOverWrittenPredefinedOperator() const { return  0 ; }
    virtual void                SetOverWrittenPredefinedOperator() { }
    virtual unsigned            IsRecordElement() const ;
    virtual unsigned            IsFile() const ;
    virtual unsigned            IsGroup() const ;
    virtual unsigned            IsGroupTemplate() const ;
    virtual unsigned            IsBlock() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;
    virtual unsigned            GetLocallyStatic() const ;

    virtual void                SetLocallyStatic() ;
    virtual void                SetGloballyStatic() ;
    virtual void                SetLoopIterator() ;
    virtual void                SetAsGeneric() ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    virtual void                SetLocallyStaticType() ;
    virtual void                SetGloballyStaticType() ;

    virtual void                SetDefinedIncompleteType() {}

    virtual unsigned            IsVariable() const ;
    virtual unsigned            IsSharedVariable() const ;
    virtual unsigned            IsSignal() const ;
    virtual unsigned            IsConstant() const ;
    virtual unsigned            IsConstantId() const  { return 0 ; }  // Returns 1 only for VhdlConstantId
    unsigned                    IsSignalParameterOfSubprogram() const ; // Returns 1 if it is signal parameter of subprogram

    virtual unsigned            IsArray() const ;
    virtual unsigned            IsRecord() const ;
    virtual unsigned            IsProtected() const ;
    virtual unsigned            IsProtectedBody() const ;

    virtual unsigned            IsAlias() const ;

    virtual void                SetAsProcedure() ;
    virtual unsigned            IsFunction() const ;
    virtual unsigned            IsProcedure() const ;

    // Type object tests
    virtual unsigned            IsType() const ;
    virtual unsigned            IsIncompleteType() const ;
    virtual unsigned            IsDefinedIncompleteType() const ;
    virtual unsigned            IsIntegerType() const ;
    virtual unsigned            IsRealType() const ;
    virtual unsigned            IsEnumerationType() const ;
    virtual unsigned            IsPhysicalType() const ;
    virtual unsigned            IsStdTimeType() const ;
    virtual unsigned            IsArrayType() const ;
    virtual unsigned            IsUnconstrainedArrayType() const ;
    virtual unsigned            IsConstrainedArrayType() const ;
    virtual unsigned            IsRecordType() const ;
    virtual unsigned            IsProtectedType() const ;
    virtual unsigned            IsProtectedTypeBody() const ;
    virtual unsigned            IsStringType() const ;
    virtual unsigned            IsNaturalType() const ;
    virtual unsigned            IsPositiveType() const ;
    virtual unsigned            IsGenericType() const ;
    virtual unsigned            IsAccessType() const ;
    virtual unsigned            ContainsAccessType() const ;
    virtual unsigned            IsFileType() const ;
    virtual unsigned            IsSubtype() const ;
    virtual unsigned            IsScalarType() const ;
    virtual unsigned            IsNumericType() const ;
    virtual unsigned            IsDiscreteType() const ;
    virtual unsigned            IsUniversalInteger() const ;
    virtual unsigned            IsUniversalReal() const ;
    virtual unsigned            IsAnonymousType() const ;
    virtual unsigned            IsStdULogicType() const ;
    virtual unsigned            IsStdBitType() const ;
    virtual unsigned            IsStdBooleanType() const ;
    virtual unsigned            IsStdCharacterType() const ;

    virtual unsigned            IsTypeContainsFile(unsigned /*consider_access_type*/, Set& /*recursion_set*/) { return 0 ; }
    virtual unsigned            IsTypeContainsRecord() { return 0 ; } // Added for Viper #3751
    virtual unsigned            IsTypeContainsUnconstrainedArray(Set& /* recursion_set */) { return 0 ; } // Added for Viper #7758
    virtual VhdlIdDef *         GetUninstantiatedPackageId() const { return 0 ; }
    virtual void                SetUninstantiatedPackageId(VhdlIdDef * /*id*/) {}

    // Type Declarations
    virtual void                DeclareIntegerType() ;
    virtual void                DeclareRealType() ;
    virtual void                DeclarePhysicalType(Map *physical_units) ;
    virtual void                DeclareEnumerationType(Map *enumeration_literals) ;
    virtual void                DeclareConstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static) ;
    virtual void                DeclareUnconstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static) ;
    virtual void                DeclareRecordType(Map *element_decl_list) ;
    virtual void                DeclareProtectedType(VhdlProtectedTypeDef *type_def) ;
    virtual void                DeclareProtectedTypeBody(VhdlProtectedTypeDefBody *type_def) ;
    virtual void                DeclareFileType(VhdlIdDef *subtype_id) ;
    virtual void                DeclareAccessType(VhdlIdDef *subtype_id, int locally_static, int globally_static) ;
    virtual void                DeclareIncompleteType(VhdlIdDef *subtype_id) ; // Set the definition of the incomplete type

    // Object Declarations
    virtual void                DeclareEnumerationId(VhdlIdDef *type, unsigned pos) ;
    virtual void                DeclarePhysicalUnitId(VhdlIdDef *type, verific_uint64 pos) ;
    virtual void                DeclareElementId(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual void                DeclareRecordElement(unsigned pos) ;
    virtual void                DeclareInterfaceId(VhdlIdDef *type, unsigned kind, unsigned mode, unsigned signal_kind, unsigned has_init_assign, unsigned is_unconstrained, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual void                DeclareSubtype(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned is_unconstrained, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual void                DeclareConstant(VhdlIdDef *type, unsigned locally_static, unsigned loop_iter, unsigned globally_static, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual void                DeclareVariable(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type, unsigned is_shared) ;
    virtual void                DeclareSignal(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type, unsigned signal_kind) ;
    virtual void                DeclareFile(VhdlIdDef *type) ;
    virtual void                DeclareAlias(VhdlIdDef *type, VhdlName *target_name, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/, unsigned /*kind*/) ;
    virtual void                DeclareAttribute(VhdlIdDef *type) ;
    virtual void                DeclareFunction(VhdlIdDef *return_type, Array *args, Array *generics) ;
    virtual void                DeclareProcedure(Array *args, Array *generics) ;
    virtual void                DeclareEntityId(Array *generics, Array *ports) ;
    virtual void                DeclareComponentId(Array *generics, Array *ports) ;
    virtual void                DeclarePackageId(Array *generics) ;
    virtual void                DeclareBlockId(Array *generics, Array *ports) ;
    virtual void                DeclareArchitectureId(VhdlIdDef *entity) ;
    virtual void                DeclareConfigurationId(VhdlIdDef *entity) ;
    virtual void                DeclarePredefinedOp(unsigned /* op */, const VhdlIdDef * /* arg1_type */, const VhdlIdDef * /*arg2_type */, VhdlIdDef * /*ret_type */) const { }
    virtual void                DeclarePredefinedOper(unsigned op, Array *args, unsigned owns_arg_id, unsigned is_func) const ;

    // Set/Check if a variable/signal is used or assigned (happens during analysis)

    virtual inline void         SetUsed()                       { }
    virtual inline void         SetAssigned()                   { }
    virtual inline unsigned     IsUsed() const                  { return 0 ; }
    virtual inline unsigned     IsAssigned() const              { return 0 ; }

    virtual inline void         SetConcurrentUsed()             { }
    virtual inline void         SetConcurrentAssigned()         { }
    virtual inline unsigned     IsConcurrentUsed() const        { return 0 ; }
    virtual inline unsigned     IsConcurrentAssigned() const    { return 0 ; }

    // Usage information
    virtual inline void         SetAssignedBeforeUsed()         { }
    virtual inline void         SetUsedBeforeAssigned()         { }
    virtual inline void         UnSetAssignedBeforeUsed()       { }
    virtual inline void         UnSetUsedBeforeAssigned()       { }
    virtual inline unsigned     IsUsedBeforeAssigned() const    { return 0 ; }
    virtual inline unsigned     IsAssignedBeforeUsed() const    { return 0 ; } // Any non-variable is never assigned before used.

    // Set/Check VarUsage flags w.r.t. the relevant SynthesisInfo
    virtual void                SetUsed(SynthesisInfo * /* info */)                       { }
    virtual void                SetAssigned(SynthesisInfo * /* info */)                   { }
    virtual unsigned            IsUsed(SynthesisInfo * /* info */) const                  { return 0 ; }
    virtual unsigned            IsAssigned(SynthesisInfo * /* info */) const              { return 0 ; }

    virtual void                SetConcurrentUsed(SynthesisInfo * /* info */)             { }
    virtual void                SetConcurrentAssigned(SynthesisInfo * /* info */)         { }
    virtual unsigned            IsConcurrentUsed(SynthesisInfo * /* info */) const        { return 0 ; }
    virtual unsigned            IsConcurrentAssigned(SynthesisInfo * /* info */) const    { return 0 ; }

    // Usage information
    virtual void                SetAssignedBeforeUsed(SynthesisInfo * /* info */)         { }
    virtual void                SetUsedBeforeAssigned(SynthesisInfo * /* info */)         { }
    virtual void                UnSetAssignedBeforeUsed(SynthesisInfo * /* info */)       { }
    virtual void                UnSetUsedBeforeAssigned(SynthesisInfo * /* info */)       { }
    virtual unsigned            IsUsedBeforeAssigned(SynthesisInfo * /* info */) const    { return 0 ; }
    virtual unsigned            IsAssignedBeforeUsed(SynthesisInfo * /* info */) const    { return 0 ; } // Any non-variable is never assigned before used.

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam(SynthesisInfo * /* info */) const       { return 0 ; }   // Regular id cannot be a ram
    virtual void                SetCanBeDualPortRam(SynthesisInfo * /* info */)          { }              // Ignore setting for regular ids
    virtual void                SetCannotBeDualPortRam(SynthesisInfo * /* info */)       { }              // Ignore setting for regular ids

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam(SynthesisInfo * /* info */) const      { return 0 ; }  // Regular id cannot be a ram
    virtual void                SetCanBeMultiPortRam(SynthesisInfo * /* info */)         { }             // Ignore setting for regular ids
    virtual void                SetCannotBeMultiPortRam(SynthesisInfo * /* info */)      { }             // Ignore setting for regular ids

    // Signals / Subtypes
    virtual VhdlIdDef *         ResolutionFunction() const ; // Return resolution function of this (signal).
    virtual void                DeclareOperators() ;

    // Operators :
    virtual unsigned            GetOperatorType() const         { return 0 ; } // for predefined operators
    virtual void                SetOwnsArgId() { } // VIPER #5996

    // Alias
    virtual VhdlName *          GetAliasTarget() const          { return 0 ; }
    virtual VhdlIdDef *         GetTargetId() const             { return 0 ; }

    // Type matching
    virtual unsigned            TypeMatch(const VhdlIdDef *id) const ;
    virtual unsigned            TypeMatchDual(const VhdlIdDef * /*id*/, unsigned /*generic_type*/) const ;
    virtual unsigned            IsCloselyRelatedType(const VhdlIdDef *id) const ;

    // Access routines
    virtual Map *               GetTypeDefList() const          { return 0 ; } // Get type def list for VhdlIdType's
    virtual Map *               GetList() const                 { return 0 ; } // Backward compatibility
    virtual VhdlIdDef *         Type() const ;                       // Get type of the object. type of type is type itself
    virtual VhdlIdDef *         BaseType() const ;                   // Get base-type of the object (never returns a subtype)

    virtual unsigned            Dimension() const ;                  // Immediate Dimension of this object
    virtual VhdlIdDef *         ElementType() const ;                // Element type of an array type or object
    virtual VhdlIdDef *         ElementType(unsigned dim) ;
    virtual VhdlIdDef *         IndexType(unsigned dim) const ;      // Get an index type from an array (type)
    virtual VhdlIdDef *         AggregateElementType() const ;        // Hack for anonymous array aggregate types
    virtual unsigned            DeepDimension() const ;              // Dimension of this object including (deep)dimensions of it's element type

    virtual unsigned            NumOfEnums() const ;                 // Number of enumerated values in a enumeration type
    virtual VhdlIdDef *         GetEnum(const char *name) const ;    // Get enum enum literal from an enum type by name
    virtual VhdlIdDef *         GetEnumAt(unsigned pos) const ;      // Get enum literal by position from an enum type ; 0 is the first

    virtual unsigned            NumOfElements() const ;              // Number of record elements in this record type
    virtual VhdlIdDef *         GetElement(const char *name) const ; // Get a record element by name. Fast.
    virtual VhdlIdDef *         GetElementAt(unsigned pos) const ;   // Get a record element by position (also Fast)

    virtual VhdlIdDef *         GetUnit(const char *name) const ;    // Get a unit from physical type by name. Fast.

    virtual unsigned            GetKind() const ;  // for interface id

    // Encoding of enum literals :
    // single-bit encoding :
    virtual void                SetBitEncoding(char b) ;             // For an enumeration literal : set as a bit(encoded) value (0,1,X or Z)
    virtual char                GetBitEncoding() const ;             // For an enumeration literal : get the bit(encoded) value (0,1,X or Z)
    virtual char                GetBitEncodingForElab() const ;      // For an enumeration literal : get the bit(encoded) value (0,1,X or Z), ignores enum_encoding attribute for static elab
    virtual VhdlIdDef *         GetEnumWithBitEncoding(char b) const ;// For a bit-encoded enum object or type, Get the first enumeration literal encoded with this bit value
    // multi-bit encoding
    virtual void                SetEncoding(const char *encoding) ;  // For an enumeration literal : set as a multi-bit(encoded) value (0,1,X or Z)
    virtual const char *        GetEncoding() const ;                // For an enumeration literal : get the multi-bit(encoded) value (0,1,X or Z)
    virtual const char *        GetEncodingForElab() const ;         // For an enumeration literal : get the multi-bit(encoded) value (0,1,X or Z), ignores enum_encoding attribute for static elab
    // one-hot encoding
    virtual void                SetOnehotEncoded() ;                 // For an enumeration literal : flag that we encode it one-hot.
    virtual unsigned            IsOnehotEncoded() const ;            // For an enumeration literal : check if we encode it one-hot.

    virtual void                SetUserEncoded() ; // Viper #7182, #7346 : Label enumeration literal as 'user' encoded (via 'enum_encoding' or 'logic_type_encoding' attribute)
    virtual unsigned            IsUserEncoded() const ; // Viper #7182, #7346 : Check if enumeration literal is 'user' encoded (via 'enum_encoding' or 'logic_type_encoding' attribute)

    virtual VhdlIdDef *         DesignatedType() const ;             // Designated type for file types and access types
    virtual verific_uint64      Position() const ;                   // Position of Identifier in its type (for enum, physical, and record elements)
    // Viper #5242 : Set/Get inverse position for physical unit id :
    virtual verific_uint64      InversePosition() const ;
    virtual void                SetInversePosition(verific_uint64 /*ip*/) { }

    // Viper #5432
    virtual void                AddAttributeSpec(VhdlAttributeSpec *attr_spec) ;
    virtual void                RemoveAttributeSpec(VhdlAttributeSpec *attr_spec) ;

    virtual VhdlAttributeSpec * FindRelevantAttributeSpec(VhdlIdDef const * /*id*/) const ;

    // Entity/Component/Configuration/Architecture identifiers
    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual Array     *         GetGenerics() const { return 0 ; }   // Get generics (only defined for VhdlComponentId, VhdlEntityId, VhdlBlockId)
    virtual VhdlIdDef *         GetPortAt(unsigned pos) const ;      // Get port by order
    virtual VhdlIdDef *         GetPort(const char *name) const ;    // Get port by name
    virtual Array     *         GetPorts() const    { return 0 ; }   // Get ports (only defined for VhdlComponentId, VhdlEntityId, VhdlBlockId)
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in component/entity
    virtual unsigned            NumOfPorts() const ;                 // Number of ports in component/entity
    virtual VhdlIdDef *         GetEntity() const ;                  // Configurations/architecture : The entity they configure
    virtual VhdlScope *         GetUninstantiatedPackageScope() const { return 0 ; }

    // Pointers back to the parse trees that some identifiers define :
    virtual VhdlPrimaryUnit *   GetPrimaryUnit() const ;             // For entity/configuration/package ids : return pointer to their parse tree
    virtual VhdlSecondaryUnit * GetSecondaryUnit() const ;           // For architecture/packages_body ids : return pointer to their parse tree
    virtual VhdlDesignUnit *    GetDesignUnit() const ;              // For all design units : return pointer to their parse tree
    virtual VhdlComponentDecl * GetComponentDecl() const ;           // For component ids : return pointer to their parse tree
    virtual VhdlStatement *     GetStatement() const ;               // For labels : return pointer to the statement parse tree
    virtual void                SetPrimaryUnit(VhdlPrimaryUnit *unit) ;
    virtual void                SetSecondaryUnit(VhdlSecondaryUnit *unit) ;
    virtual void                SetComponentDecl(VhdlComponentDecl *unit) ;
    virtual void                SetStatement(VhdlStatement *unit) ;

    // Subprogram handling
    virtual unsigned            NumOfArgs() const ;
    virtual VhdlIdDef *         Arg(unsigned idx) const ;
    virtual VhdlIdDef *         ArgType(unsigned idx) const ;
    virtual Array *             GetArgs() const { return 0 ; } // Get program parameters
    virtual void                SetSpec(VhdlSpecification *spec) ;
    virtual VhdlSpecification * Spec() const ;
    virtual void                SetBody(VhdlSubprogramBody *body) ;
    virtual VhdlSubprogramBody * Body() const ;
    virtual void                SetSubprogInstanceDecl(VhdlDeclaration *inst) ;
    virtual VhdlDeclaration *   GetSubprogInstanceDecl() const ;
    virtual void                SetActualId(VhdlIdDef *id, Array* assoc_list) ; // For interface package/subprogram
    virtual VhdlIdDef *         GetActualId() const ; // For interface package/subprogram
    virtual VhdlIdDef *         TakeActualId() ;

    // Type cast
    virtual VhdlTypeId*         TypeCast() const { return 0 ; }

    // Interface Package declaration handling :
    virtual void                SetDeclaration(VhdlDeclaration *d) ;
    virtual VhdlDeclaration    *GetDeclaration() const ;

    // Type infer an association list with 'this' interface list ('this' should be a subprogram, component, entity or block).
    // Also do all LRM checks on interface-association list connections.
    // Translate the association list from named to positional.
    // 'object_kind' is VHDL_port (portlist) VHDL_generic (generic list) or 0 (subprog parameter list).
    // actual_binding scope is set to the component scope if this routine is called from a binding indication.
    virtual void                TypeInferAssociationList(Array *assoc_list, unsigned object_kind, VhdlScope *actual_binding_scope, const VhdlTreeNode *from) ;

    // Deferred constant handling : Make a loop to id with deferred id
    virtual void                SetDecl(VhdlIdDef *decl) ;
    virtual VhdlIdDef *         Decl() const ;
    virtual unsigned            HasInitAssign() const ; // Also for interface IDs

    // FILE identifiers :
    virtual FILE *              GetFile() const { return 0 ; }   // default catcher for FileId objects : get the FILE object during elaboration.
    virtual void                SetFile(FILE * /*file*/) { return ; }   // default catcher for FileId objects : set the FILE object during elaboration.

    // Interface Ids
    virtual void                SetObjectKind(unsigned object_kind) ;
    virtual unsigned            IsInterfaceObject(unsigned object_kind) const ;
    virtual unsigned            IsGeneric() const ;
    virtual unsigned            IsPort() const ;
    virtual unsigned            IsSubprogramFormal() const ; // Return 1 for subprogram formal (VIPER #7033)

    // Port directions :
    virtual unsigned            IsInput() const ;
    virtual unsigned            IsOutput() const ;
    virtual unsigned            IsInout() const ;
    virtual unsigned            IsLinkage() const ;
    virtual unsigned            IsBuffer() const ;
    virtual unsigned            Mode() const ;                         // Port direction as a token (from tokens.h).
    virtual void                SetMode(unsigned mode) ;               // Set port mode (direction)
    virtual unsigned            IsUnconstrained() const ;// Unconstrained port check.
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

    // Pragma settings on subprograms
    virtual unsigned            GetPragmaFunction() const ;
    virtual unsigned            GetPragmaSigned() const ;
    virtual unsigned            GetPragmaLabelAppliesTo() const ;
    virtual void                SetPragmaFunction(unsigned function) ;
    virtual void                SetPragmaSigned() ;
    virtual void                SetPragmaLabelAppliesTo() ;
    void                        CheckPragmaSetting() ; // Local function that checks/discards pragma if not appropriate for the function

    //CARBON_BEGIN
    // Register name value pragma pair
    void RegisterPragma(char* name, char* value);
    // Return pragmas
    Map* gPragmas() const {return _pragmas;} 
    // Cleanup pragma map 
    void RemovePragmas();
    // Copy pragmas from another idDef node
    void CopyPragmas(const VhdlIdDef& node);
    //CARBON_END

    virtual unsigned            GetSignalKind() const          { return 0 ; } // if it is bus or register : good only for signal_id and interface_id
    virtual unsigned            GetObjectKind() const          { return (unsigned)(-1) ; } // return max int. Defined for interface ids

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam() const       { return 0 ; }   // Regular id cannot be a ram
    virtual void                SetCanBeDualPortRam()          { }              // Ignore setting for regular ids
    virtual void                SetCannotBeDualPortRam()       { }              // Ignore setting for regular ids

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam() const      { return 0 ; }  // Regular id cannot be a ram
    virtual void                SetCanBeMultiPortRam()         { }             // Ignore setting for regular ids
    virtual void                SetCannotBeMultiPortRam()      { }             // Ignore setting for regular ids

    // Define/Get an attribute on this identifier.
    virtual unsigned            SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_value) ; // set attribute 'attr_id' with value expression 'attr_value' on this identifier. This is automatically called during analysis.
    virtual VhdlIdDef          *GetAttributeId(const char *attr_name) const ; // get the attribute 'id' of an attribute on this id (or null if no attribute like this is present.
    virtual VhdlExpression     *GetAttribute(const VhdlIdDef *attr_id) const ; // get the expression (value) of an attribute on this id (or null if the id has no such attribute set). Access by attr identifier.
    virtual VhdlExpression     *GetAttribute(const char *attr_name) const ; // get the expression of an attribute on this id (or null if the id has no such attribute set). Access by name.
    virtual unsigned            NumOfAttributes() const ;             // Return number of attributes on this id.
    virtual Map *               GetAttributes() const                               { return _attributes ; } // table of VhdlIdDef(attribute-id) -> VhdlExpression(attribute value) on this identifier.

    // Set/get primary binding pointers on instantiation labels :
    virtual void                    AddBinding(VhdlBindingIndication * /*binding*/) { } // Set primary binding on a label. Set during analysis.
    virtual VhdlBindingIndication * GetPrimaryBinding() const                       { return 0 ; }
    virtual void                    SetBinding(VhdlBindingIndication * /*binding*/) { } // Set/reset primary binding on a label
    virtual VhdlProtectedTypeDef *  GetProtectedTypeDef() const { return 0 ; }
    virtual VhdlProtectedTypeDefBody * GetProtectedTypeDefBody() const { return 0 ; }
    // VIPER #7775 : Set/get protected type body id to protected type id
    virtual void                        SetProtectedTypeBodyId(VhdlIdDef * /*body_id*/) {}
    virtual VhdlIdDef *                 GetProtectedTypeBodyId() const  { return 0 ; }
    // VIPER #7775 : Set/get protected type def id
    virtual void                        SetProtectedTypeDefId(VhdlIdDef * /*id*/) {}
    virtual VhdlIdDef *                 GetProtectedTypeDefId() const   { return 0 ; }
    // VIPER #7447 : Set/Get routines for group template
    virtual void                SetGroupTemplateDeclaration(VhdlGroupTemplateDecl * /*decl*/) { }
    virtual VhdlGroupTemplateDecl *GetGroupTemplateDeclaration() const { return 0 ; }

    // Elaboration

    // Values are on every id
    virtual void                SetValue(VhdlValue *initial_value) ; // Create initial value
    virtual VhdlValue *         Value() const ;                      // Return initial value of this id. Not allocated. Its just a pointer
    virtual VhdlValue *         TakeValue() ;                        // Take (memory) control over value away from id
    virtual unsigned            HasValue() const ;

    // Constraints are on every id
    virtual VhdlConstraint *    Constraint() const ; // Return pointer to constraint of this identifier
    virtual void                SetConstraint(VhdlConstraint *constraint) ;
    virtual VhdlConstraint *    TakeConstraint() ;   // take control of constraint away from id

    // Create a name for the entity/component with set generics
    // VIPER #6038 : Pass block configuration for name creation
    virtual char *              CreateUniqueName(const VhdlBlockConfiguration *block_config) const ;

    // Elaboration specific package identifier get/set for package instantiation/interface package generic
    virtual VhdlIdDef *         GetElaboratedPackageId() const { return 0 ; }
    virtual void                SetElaboratedPackageId(VhdlIdDef * /*id*/) {}
    virtual VhdlIdDef *         TakeElaboratedPackageId() { return 0 ; }

    // Get/Set attributes : Note virtual functions can be overridden by derived classes
    virtual void                SetAttribute(VhdlIdDef *attr, VhdlValue *value) ;
//    virtual VhdlValue *         GetAttribute(VhdlIdDef *attr) const ;

    // Execution of subprograms
    // unary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;
    // binary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;
    // function / procedure call
    virtual VhdlValue *         ElaborateSubprogram(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef *subprog_inst) ;
    // VIPER #6749 : Moved these APIs from VhdlInterfaceId to VhdlIdDef, as
    // these are now needed for VhdlAliasId too
    unsigned                    DecodeKind(unsigned kind) const ;
    unsigned                    EncodeKind(unsigned kind) const ;

    virtual void                SetRefTarget(VhdlDiscreteRange * /*r*/) {  }
    virtual VhdlDiscreteRange*  GetRefTarget() const    { return 0 ; } // Viper 6781
    virtual void                Prune(Set* /*ids*/)    { } // Viper 7515, 7518
    virtual void                RemoveIdFromAttributeSpec(VhdlIdDef const * /*id*/) const { }

    // Internal routine : VIPER #7717 : Returns true for vl types
    unsigned                    IsVlType() const ;
protected:
// All IdDef nodes are leaf nodes. Identifier definitions.
// Any pointer fields are back-pointers to previously declared IdDef nodes or other parse-tree nodes,
// or values/constraints created during elaboration
    char            *_name ;        // name of the identifier (lower-cased for regular identifiers).
#ifdef VHDL_PRESERVE_ID_CASE
    char            *_orig_name ;   // name as it appeared exactly in the vhdl file.
#endif
// Set at analysis time :
    VhdlIdDef       *_type ;        // back-pointer (not owned) to the type of this identifier. (0 for non-objects)
    Map             *_attributes ;  // char VhdlAttributeId* -> VhdlExpression* hash table of attributes.

#ifdef VHDL_ID_SCOPE_BACKPOINTER
    VhdlScope       *_owning_scope ; // Issue 2716 : back-pointer to the scope in which this identifier is declared.
#endif
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    VhdlSubtypeIndication  *_subtype_indication ; // back-pointer to the subtype indication (parse tree node from the declaration of this identifier).
#endif

// Set at elaboration time :
    VhdlConstraint  *_constraint ;  // constraint (subtype indication in elaborated form) of the identifier. 0 for non-objects except types.
    VhdlValue       *_value ;       // value (elaborated constant/non-constant) of the identifier. 0 for non-objects.
    //CARBON_BEGIN
    Map             *_pragmas ; // keep list of embedded carbon pragmas for this id (char* : char* => pragma_name, pragma_value)
    //CARBON_END
} ; // class VhdlIdDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlLibraryId : public VhdlIdDef
{
public:
    explicit VhdlLibraryId(char *name) ;

    // Copy tree node
    VhdlLibraryId(const VhdlLibraryId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlLibraryId(SaveRestore &save_restore) ;

    virtual ~VhdlLibraryId() ;

private:
    // Prevent compiler from defining the following
    VhdlLibraryId() ;                                 // Purposely leave unimplemented
    VhdlLibraryId(const VhdlLibraryId &) ;            // Purposely leave unimplemented
    VhdlLibraryId& operator=(const VhdlLibraryId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLLIBRARYID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy library identifier
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsLibrary() const ;

protected:
} ; // class VhdlLibraryId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlGroupId : public VhdlIdDef
{
public:
    explicit VhdlGroupId(char *name) ;

    // Copy tree node
    VhdlGroupId(const VhdlGroupId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlGroupId(SaveRestore &save_restore) ;

    virtual ~VhdlGroupId() ;

private:
    // Prevent compiler from defining the following
    VhdlGroupId() ;                               // Purposely leave unimplemented
    VhdlGroupId(const VhdlGroupId &) ;            // Purposely leave unimplemented
    VhdlGroupId& operator=(const VhdlGroupId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLGROUPID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy group id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsGroup() const ;
protected:
} ; // class VhdlGroupId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlGroupTemplateId : public VhdlIdDef
{
public:
    explicit VhdlGroupTemplateId(char *name) ;

    // Copy tree node
    VhdlGroupTemplateId(const VhdlGroupTemplateId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlGroupTemplateId(SaveRestore &save_restore) ;

    virtual ~VhdlGroupTemplateId() ;

private:
    // Prevent compiler from defining the following
    VhdlGroupTemplateId() ;                                       // Purposely leave unimplemented
    VhdlGroupTemplateId(const VhdlGroupTemplateId &) ;            // Purposely leave unimplemented
    VhdlGroupTemplateId& operator=(const VhdlGroupTemplateId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLGROUPTEMPLATEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy group template id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsGroupTemplate() const ;
    virtual void                TypeInferAssociationList(Array *assoc_list, unsigned object_kind, VhdlScope *actual_binding_scope, const VhdlTreeNode *from) ;

    // VIPER #7447 : Set/Get routines for group template
    virtual void                SetGroupTemplateDeclaration(VhdlGroupTemplateDecl *decl) { _decl = decl ; }
    virtual VhdlGroupTemplateDecl *GetGroupTemplateDeclaration() const { return _decl ; }
protected:
    VhdlGroupTemplateDecl *_decl ; // Back pointer to group template declaration (VIPER #7447)
} ; // class VhdlGroupTemplateId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAttributeId : public VhdlIdDef
{
public:
    explicit VhdlAttributeId(char *name) ;

    // Copy tree node
    VhdlAttributeId(const VhdlAttributeId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAttributeId(SaveRestore &save_restore) ;

    virtual ~VhdlAttributeId() ;

private:
    // Prevent compiler from defining the following
    VhdlAttributeId() ;                                   // Purposely leave unimplemented
    VhdlAttributeId(const VhdlAttributeId &) ;            // Purposely leave unimplemented
    VhdlAttributeId& operator=(const VhdlAttributeId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLATTRIBUTEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy attribute id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual void                DeclareAttribute(VhdlIdDef *type) ;
    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsAttribute() const ;

    // Viper #5432
    virtual void                AddAttributeSpec(VhdlAttributeSpec *attr_spec) ;
    virtual void                RemoveAttributeSpec(VhdlAttributeSpec *attr_spec) ;
    virtual void                RemoveIdFromAttributeSpec(VhdlIdDef const *id) const ;

    // Viper #3637/6805: Get the attribute spec that defines the attribute on the argument id
    virtual VhdlAttributeSpec * FindRelevantAttributeSpec(VhdlIdDef const *id) const ;

protected:

    Set     *_attribute_specs ; // Viper #5432: list of VhdlAttributeSpec *
} ; // class VhdlAttributeId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlComponentId : public VhdlIdDef
{
public:
    explicit VhdlComponentId(char *name) ;

    // Copy tree node
    VhdlComponentId(const VhdlComponentId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlComponentId(SaveRestore &save_restore) ;

    virtual ~VhdlComponentId() ;

private:
    // Prevent compiler from defining the following
    VhdlComponentId() ;                                   // Purposely leave unimplemented
    VhdlComponentId(const VhdlComponentId &) ;            // Purposely leave unimplemented
    VhdlComponentId& operator=(const VhdlComponentId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCOMPONENTID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy component id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // Parse-tree port/generic add routines
    virtual void                AddPort(VhdlIdDef *id) ;
    virtual void                AddGeneric(VhdlIdDef *id) ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetComponentDecl(VhdlComponentDecl *component) ;
    virtual VhdlComponentDecl * GetComponentDecl() const ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual void                DeclareComponentId(Array *generics, Array *ports) ;
    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsComponent() const ;

    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual Array *             GetGenerics() const         { return _generics ; }
    virtual VhdlIdDef *         GetPortAt(unsigned pos) const ;      // Get port by order
    virtual VhdlIdDef *         GetPort(const char *name) const ;    // Get port by name
    virtual Array *             GetPorts() const            { return _ports ; }
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in component/entity
    virtual unsigned            NumOfPorts() const ;                 // Number of ports in component/entity

    // Elaboration
    virtual char *              CreateUniqueName(const VhdlBlockConfiguration *block_config) const ; // Create a name for the entity/component withj set generics

protected:
// Set at analysis time :
    Array               *_ports ;     // (elements are not owned) Array of VhdlIdDef* to the ports of this component, ordered.
    Array               *_generics ;  // (elements are not owned) Array of VhdlIdDef* to the generics of this component, ordered.
    VhdlScope           *_scope ;     // back-pointer to the scope that the component declared
    VhdlComponentDecl   *_component ; // back-pointer to the tree that the id declares.
} ; // class VhdlComponentId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAliasId : public VhdlIdDef
{
public:
    explicit VhdlAliasId(char *name) ;

    // Copy tree node
    VhdlAliasId(const VhdlAliasId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAliasId(SaveRestore &save_restore) ;

    virtual ~VhdlAliasId() ;

private:
    // Prevent compiler from defining the following
    VhdlAliasId() ;                               // Purposely leave unimplemented
    VhdlAliasId(const VhdlAliasId &) ;            // Purposely leave unimplemented
    VhdlAliasId& operator=(const VhdlAliasId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLALIASID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy alias id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;
    virtual void                SetTargetName(VhdlName *target_name) { _target_name = target_name; } // Set back pointer _target_name in alias id.
    virtual void                SetTargetId(VhdlIdDef *id) { _target_id = id ; }

    virtual void                DeclareAlias(VhdlIdDef *type, VhdlName *target_name, unsigned locally_static_type, unsigned globally_static_type, unsigned kind) ;

    virtual VhdlScope *         LocalScope() const ;    // return scope of the target

    // Return the target Name to the alias. Available after declaration
    virtual VhdlName *          GetAliasTarget() const  { return _target_name ; }
    virtual VhdlIdDef *         GetTargetId() const     { return _target_id ; }

    // Overloaded type-matching routines (for type-aliases) :
    virtual unsigned            TypeMatch(const VhdlIdDef *id) const ;
    virtual unsigned            TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const ;
    virtual unsigned            IsCloselyRelatedType(const VhdlIdDef *id) const ;

    virtual unsigned            EntityClass() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    // Identifier tests :
    virtual unsigned            IsAlias() const ;
    virtual unsigned            IsLabel() const ;
    virtual unsigned            IsLibrary() const ;
    virtual unsigned            IsEntity() const ;
    virtual unsigned            IsArchitecture() const ;
    virtual unsigned            IsConfiguration() const ;
    virtual unsigned            IsPackage() const ;
    virtual unsigned            IsPackageBody() const ;
    virtual unsigned            IsDesignUnit() const ; // Entity, architecture, configuration, architecture, package or package body.
    virtual unsigned            IsComponent() const ;
    virtual unsigned            IsSubprogram() const ;
    virtual unsigned            IsAttribute() const ;
    virtual unsigned            IsEnumerationLiteral() const ;
    // virtual unsigned IsPredefinedOperator() const ; // issue 2004 : predefined operator is a property of the IdDef itself. Alias id is never a predefined operator, even if it points at one.
    virtual unsigned            IsRecordElement() const ;
    virtual unsigned            IsFile() const ;
    virtual unsigned            IsBlock() const ;
    virtual unsigned            IsGroup() const ;
    virtual unsigned            IsGroupTemplate() const ;

    virtual unsigned            IsVariable() const ;
    virtual unsigned            IsSharedVariable() const ;
    virtual unsigned            IsSignal() const ;
    virtual unsigned            IsConstant() const ;

    virtual unsigned            IsInterfaceObject(unsigned object_kind) const ;
    virtual unsigned            IsGeneric() const ;
    virtual unsigned            IsPort() const ;
    virtual unsigned            IsSubprogramFormal() const ;
    virtual unsigned            IsInput() const ;
    virtual unsigned            IsOutput() const ;
    virtual unsigned            IsInout() const ;
    virtual unsigned            IsLinkage() const ;
    virtual unsigned            IsBuffer() const ;
    virtual unsigned            Mode() const ;

    virtual unsigned            IsType() const ;
    virtual unsigned            IsIncompleteType() const ;
    virtual unsigned            IsIntegerType() const ;
    virtual unsigned            IsRealType() const ;
    virtual unsigned            IsEnumerationType() const ;
    virtual unsigned            IsStdTimeType() const ;
    virtual unsigned            IsPhysicalType() const ;
    virtual unsigned            IsArrayType() const ;
    virtual unsigned            IsUnconstrainedArrayType() const ;
    virtual unsigned            IsConstrainedArrayType() const ;
    virtual unsigned            IsRecordType() const ;
    virtual unsigned            IsAccessType() const ;
    virtual unsigned            ContainsAccessType() const ;
    virtual unsigned            IsFileType() const ;
    virtual unsigned            IsSubtype() const ;
    virtual unsigned            IsScalarType() const ;
    virtual unsigned            IsNumericType() const ;
    virtual unsigned            IsDiscreteType() const ;
    virtual unsigned            IsProtectedType() const ;
    virtual unsigned            IsProtectedTypeBody() const ;
    virtual unsigned            IsStringType() const ;
    virtual unsigned            IsNaturalType() const ;
    virtual unsigned            IsPositiveType() const ;
    virtual unsigned            IsStdULogicType() const ;
    virtual unsigned            IsStdBitType() const ;
    virtual unsigned            IsStdBooleanType() const ;
    virtual unsigned            IsStdCharacterType() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;
    
    virtual inline unsigned     HasInitAssign() const               { return _target_id ? _target_id->HasInitAssign() : 0 ; }

    virtual inline void         SetUsed()                           { if (_target_id) _target_id->SetUsed() ; }
    virtual inline void         SetAssigned()                       { if (_target_id) _target_id->SetAssigned() ; }
    virtual inline unsigned     IsUsed() const                      { return (_target_id) ? _target_id->IsUsed() : 0 ; }
    virtual inline unsigned     IsAssigned() const                  { return (_target_id) ? _target_id->IsAssigned() : 0 ; }

    virtual inline void         SetConcurrentUsed()                 { if (_target_id) _target_id->SetConcurrentUsed() ; }
    virtual inline void         SetConcurrentAssigned()             { if (_target_id) _target_id->SetConcurrentAssigned() ; }
    virtual inline unsigned     IsConcurrentUsed() const            { return (_target_id) ? _target_id->IsConcurrentUsed() : 0 ; }
    virtual inline unsigned     IsConcurrentAssigned() const        { return (_target_id) ? _target_id->IsConcurrentAssigned() : 0 ; }

    virtual inline void         SetAssignedBeforeUsed()             { if (_target_id) _target_id->SetAssignedBeforeUsed() ; }
    virtual inline void         SetUsedBeforeAssigned()             { if (_target_id) _target_id->SetUsedBeforeAssigned() ; }
    virtual inline unsigned     IsAssignedBeforeUsed() const        { return (_target_id) ? _target_id->IsAssignedBeforeUsed() : 0 ; }

    virtual inline void         UnSetAssignedBeforeUsed()           { if (_target_id) _target_id->UnSetAssignedBeforeUsed() ; }
    virtual inline void         UnSetUsedBeforeAssigned()           { if (_target_id) _target_id->UnSetUsedBeforeAssigned() ; }

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam() const        { return (_target_id) ? _target_id->CanBeDualPortRam() : 0 ; }
    virtual void                SetCanBeDualPortRam()           { if (_target_id) _target_id->SetCanBeDualPortRam() ; }
    virtual void                SetCannotBeDualPortRam()        { if (_target_id) _target_id->SetCannotBeDualPortRam() ; }

    // multi-port RAM detection : (VIPER 3749)
    virtual unsigned            CanBeMultiPortRam() const       { return (_target_id) ? _target_id->CanBeMultiPortRam() : 0 ; }
    virtual void                SetCanBeMultiPortRam()          { if (_target_id) _target_id->SetCanBeMultiPortRam() ; }
    virtual void                SetCannotBeMultiPortRam()       { if (_target_id) _target_id->SetCannotBeMultiPortRam() ; }

    // Set/Check VarUsage flags w.r.t. the relevant SynthesisInfo
    virtual void                SetUsed(SynthesisInfo *info)                       { if (_target_id) _target_id->SetUsed(info) ; }
    virtual void                SetAssigned(SynthesisInfo *info)                   { if (_target_id) _target_id->SetAssigned(info) ; }
    virtual unsigned            IsUsed(SynthesisInfo *info) const                  { return (_target_id) ? _target_id->IsUsed(info) : 0 ; }
    virtual unsigned            IsAssigned(SynthesisInfo *info) const              { return (_target_id) ? _target_id->IsAssigned(info) : 0 ; }

    virtual void                SetConcurrentUsed(SynthesisInfo *info)             { if (_target_id) _target_id->SetConcurrentUsed(info) ; }
    virtual void                SetConcurrentAssigned(SynthesisInfo *info)         { if (_target_id) _target_id->SetConcurrentAssigned(info) ; }
    virtual unsigned            IsConcurrentUsed(SynthesisInfo *info) const        { return (_target_id) ? _target_id->IsConcurrentUsed(info) : 0 ; }
    virtual unsigned            IsConcurrentAssigned(SynthesisInfo *info) const    { return (_target_id) ? _target_id->IsConcurrentAssigned(info) : 0 ; }

    // Usage information
    virtual void                SetAssignedBeforeUsed(SynthesisInfo *info)         { if (_target_id) _target_id->SetAssignedBeforeUsed(info) ; }
    virtual void                SetUsedBeforeAssigned(SynthesisInfo *info)         { if (_target_id) _target_id->SetUsedBeforeAssigned(info) ; }
    virtual void                UnSetAssignedBeforeUsed(SynthesisInfo *info)       { if (_target_id) _target_id->UnSetAssignedBeforeUsed(info) ; }
    virtual void                UnSetUsedBeforeAssigned(SynthesisInfo *info)       { if (_target_id) _target_id->UnSetUsedBeforeAssigned(info) ; }
    virtual unsigned            IsUsedBeforeAssigned(SynthesisInfo *info) const    { return (_target_id) ? _target_id->IsUsedBeforeAssigned(info) : 0 ; }
    virtual unsigned            IsAssignedBeforeUsed(SynthesisInfo *info) const    { return (_target_id) ? _target_id->IsAssignedBeforeUsed(info) : 0 ; } // Any non-variable is never assigned before used.

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam(SynthesisInfo *info) const       { return (_target_id) ? _target_id->CanBeDualPortRam(info) : 0 ; }   // Regular id cannot be a ram
    virtual void                SetCanBeDualPortRam(SynthesisInfo *info)          { if (_target_id) _target_id->SetCanBeDualPortRam(info) ; }              // Ignore setting for regular ids
    virtual void                SetCannotBeDualPortRam(SynthesisInfo *info)       { if (_target_id) _target_id->SetCannotBeDualPortRam(info) ; }              // Ignore setting for regular ids

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam(SynthesisInfo *info) const      { return (_target_id) ? _target_id->CanBeMultiPortRam(info) : 0 ; }  // Regular id cannot be a ram
    virtual void                SetCanBeMultiPortRam(SynthesisInfo *info)         { if (_target_id) _target_id->SetCanBeMultiPortRam(info) ; }             // Ignore setting for regular ids
    virtual void                SetCannotBeMultiPortRam(SynthesisInfo *info)      { if (_target_id) _target_id->SetCannotBeMultiPortRam(info) ; }             // Ignore setting for regular ids

    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual VhdlIdDef *         GetPortAt(unsigned pos) const ;      // Get port by order
    virtual VhdlIdDef *         GetPort(const char *name) const ;    // Get port by name
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in component/entity
    virtual unsigned            NumOfPorts() const ;                 // Number of ports in component/entity

    virtual unsigned            NumOfArgs() const ;                  // Number of arguments in a subprogram
    virtual VhdlIdDef *         ArgType(unsigned idx) const ;        // type of an argument by index
    virtual VhdlIdDef *         Arg(unsigned idx) const ;            // argument id by index
    // VIPER #5187 :Define/Get an attribute on this identifier.
    virtual unsigned            SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_value) ; // set attribute 'attr_id' with value expression 'attr_value' on this identifier. This is automatically called during analysis.
    virtual VhdlIdDef          *GetAttributeId(const char *attr_name) const ; // get the attribute 'id' of an attribute on this id (or null if no attribute like this is present.
    virtual VhdlExpression     *GetAttribute(const VhdlIdDef *attr_id) const ; // get the expression (value) of an attribute on this id (or null if the id has no such attribute set). Access by attr identifier.
    virtual VhdlExpression     *GetAttribute(const char *attr_name) const ; // get the expression of an attribute on this id (or null if the id has no such attribute set). Access by name.
    virtual unsigned            NumOfAttributes() const ;             // Return number of attributes on this id.
    virtual Map *               GetAttributes() const ; // table of VhdlIdDef(attribute-id) -> VhdlExpression(attribute value) on this identifier.
    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;
    // VIPER #6749 : Stored _kind in alias id when terget is external name :
    virtual unsigned            GetKind() const ;
    virtual void                Prune(Set* ids) ;

protected:
// back-pointers (not owned), set at analysis time :
    VhdlName *_target_name ; // back-pointer to the alias name.
    VhdlIdDef *_target_id ;  // back-pointer to the VhdlIdDef that the alias name refers to (could be 0 if its an alias of a indexed signal or so)
    unsigned   _is_locally_static_type:1 ;    // Viper #5354 : Set if the type is locally static. Needed as the subtype_ind is not available
    unsigned   _is_globally_static_type:1 ;   // Viper #5354 : Set if the type is globally static. Needed as the subtype_ind is not available
    unsigned   _kind:3 ;              // VHDL_constant, VHDL_signal, VHDL_variable, VHDL_file (VIPER #6749)
} ; // class VhdlAliasId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlFileId : public VhdlIdDef
{
public:
    explicit VhdlFileId(char *name) ;

    // Copy tree node
    VhdlFileId(const VhdlFileId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlFileId(SaveRestore &save_restore) ;

    virtual ~VhdlFileId() ;

private:
    // Prevent compiler from defining the following
    VhdlFileId() ;                              // Purposely leave unimplemented
    VhdlFileId(const VhdlFileId &) ;            // Purposely leave unimplemented
    VhdlFileId& operator=(const VhdlFileId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLFILEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy file id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsFile() const ;

    virtual void                DeclareFile(VhdlIdDef *type) ;

    // Accessor functions
    virtual FILE *              GetFile() const { return _file ; }
    virtual void                SetFile(FILE *file) { _file = file ; }   // set file pointer. File open/close status management done externally.

protected:
    // Set at elaboration time :
    // For VIPER 3341 : FILE * field needed during elaboration
    FILE *                      _file ;
} ; // class VhdlFileId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlVariableId : public VhdlIdDef
{
public:
    explicit VhdlVariableId(char *name) ;

    // Copy tree node
    VhdlVariableId(const VhdlVariableId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlVariableId(SaveRestore &save_restore) ;

    virtual ~VhdlVariableId() ;

private:
    // Prevent compiler from defining the following
    VhdlVariableId() ;                                  // Purposely leave unimplemented
    VhdlVariableId(const VhdlVariableId &) ;            // Purposely leave unimplemented
    VhdlVariableId& operator=(const VhdlVariableId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const              { return ID_VHDLVARIABLEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy variable id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsVariable() const ;

    virtual void                DeclareVariable(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type, unsigned is_shared) ;

    // Used/Assigned info
    virtual inline void         SetUsed()                       { _is_used = 1 ; }
    virtual inline void         SetAssigned()                   { _is_assigned = 1 ; }
    virtual inline unsigned     IsUsed() const                  { return _is_used ; }
    virtual inline unsigned     IsAssigned() const              { return _is_assigned ; }

    virtual inline void         SetConcurrentUsed()             { _is_concurrent_used = 1 ; _is_used = 1 ; }
    virtual inline void         SetConcurrentAssigned()         { _is_concurrent_assigned = 1 ; _is_assigned = 1 ; }
    virtual inline unsigned     IsConcurrentUsed() const        { return _is_concurrent_used ; }
    virtual inline unsigned     IsConcurrentAssigned() const    { return _is_concurrent_assigned ; }

    virtual inline void         SetAssignedBeforeUsed()         { _is_assigned_before_used = 1 ; }
    virtual inline void         SetUsedBeforeAssigned()         { _is_used_before_assigned = 1 ; }
    virtual inline unsigned     IsAssignedBeforeUsed() const    { return _is_assigned_before_used ; }
    virtual inline void         UnSetAssignedBeforeUsed()       { _is_assigned_before_used = 0 ; }
    virtual inline void         UnSetUsedBeforeAssigned()       { _is_used_before_assigned = 0 ; }
    virtual inline unsigned     IsUsedBeforeAssigned() const    { return _is_used_before_assigned ; }

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam() const        { return _can_be_dual_port_ram ; }
    virtual void                SetCanBeDualPortRam() ; // Set flag only when run-time switch is set.
    virtual void                SetCannotBeDualPortRam()        { _can_be_dual_port_ram = 0 ; }

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam() const       { return _can_be_multi_port_ram ; }
    virtual void                SetCanBeMultiPortRam() ;  // Set flag only when run-time switch is set.
    virtual void                SetCannotBeMultiPortRam()       { _can_be_multi_port_ram = 0 ; }

    // Set/Check VarUsage flags w.r.t. the relevant SynthesisInfo
    virtual void                SetUsed(SynthesisInfo *info) ;
    virtual void                SetAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsUsed(SynthesisInfo *info) const ;
    virtual unsigned            IsAssigned(SynthesisInfo *info) const ;

    virtual void                SetConcurrentUsed(SynthesisInfo *info) ;
    virtual void                SetConcurrentAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsConcurrentUsed(SynthesisInfo *info) const ;
    virtual unsigned            IsConcurrentAssigned(SynthesisInfo *info) const ;

    // Usage information
    virtual void                SetAssignedBeforeUsed(SynthesisInfo *info) ;
    virtual void                SetUsedBeforeAssigned(SynthesisInfo *info) ;
    virtual void                UnSetAssignedBeforeUsed(SynthesisInfo *info) ;
    virtual void                UnSetUsedBeforeAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsUsedBeforeAssigned(SynthesisInfo *info) const ;
    virtual unsigned            IsAssignedBeforeUsed(SynthesisInfo *info) const ;

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam(SynthesisInfo *info) const ;
    virtual void                SetCanBeDualPortRam(SynthesisInfo *info) ;
    virtual void                SetCannotBeDualPortRam(SynthesisInfo *info) ;

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam(SynthesisInfo *info) const ;
    virtual void                SetCanBeMultiPortRam(SynthesisInfo *info) ;
    virtual void                SetCannotBeMultiPortRam(SynthesisInfo *info) ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2 It
    // returns 1 if the subtype_indication used in the variable
    // declaration is locally/globally static type/subtype
    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual void                SetLocallyStaticType() ;
    virtual void                SetGloballyStaticType() ;

    virtual unsigned            IsSharedVariable() const        { return _is_shared ; }

protected:
// Flags set during analysis
    unsigned   _is_assigned:1 ;               // Set if the object is 'assigned' somewhere (in an assignment or association)
    unsigned   _is_used:1 ;                   // Set if the object is 'used' somewhere (in an expression or association)
    unsigned   _is_concurrent_assigned:1 ;    // Issue 2843 : shared variable can be (multiply) concurrently assigned. Set if the object is 'assigned' concurrently somewhere (in an assignment or association)
    unsigned   _is_concurrent_used:1 ;        // Issue 2843 : shared variable can be (multiply) concurrently used. Set if the object is 'used' concurrently somewhere (in an expression or association)
    unsigned   _can_be_dual_port_ram:1 ;      // Issue 2041 : Variable can represent a memory with a single-read-port and single-write-port.
    unsigned   _can_be_multi_port_ram:1 ;     // Issue 2730 : Veriable can represent a memory with any number or read-ports/write-ports.
    unsigned   _is_assigned_before_used:1 ;   // Set if the object is always assigned before used (no feedback logic needed).
    unsigned   _is_used_before_assigned:1 ;   // Set if the object is used before assigned (in at least one place).
    unsigned   _is_locally_static_type:1 ;    // Set if the type is locally static. Needed as the subtype_ind is not available
    unsigned   _is_globally_static_type:1 ;   // Set if the type is globally static. Needed as the subtype_ind is not available
    unsigned _is_shared:1 ;                   // shared variable
} ; // class VhdlVariableId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSignalId : public VhdlIdDef
{
public:
    explicit VhdlSignalId(char *name) ;

    // Copy tree node
    VhdlSignalId(const VhdlSignalId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSignalId(SaveRestore &save_restore) ;

    virtual ~VhdlSignalId() ;

private:
    // Prevent compiler from defining the following
    VhdlSignalId() ;                                // Purposely leave unimplemented
    VhdlSignalId(const VhdlSignalId &) ;            // Purposely leave unimplemented
    VhdlSignalId& operator=(const VhdlSignalId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const              { return ID_VHDLSIGNALID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy signal id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsSignal() const ;

    virtual void                DeclareSignal(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type, unsigned signal_kind) ;

    virtual VhdlIdDef *         ResolutionFunction() const ; // return resolution function of this (signal).

    // Used/Assigned info
    virtual inline void         SetUsed()                       { _is_used = 1 ; }
    virtual inline void         SetAssigned()                   { _is_assigned = 1 ; }
    virtual inline unsigned     IsUsed() const                  { return _is_used ; }
    virtual inline unsigned     IsAssigned() const              { return _is_assigned ; }

    virtual inline void         SetConcurrentUsed()             { _is_concurrent_used = 1 ; _is_used = 1 ; }
    virtual inline void         SetConcurrentAssigned()         { _is_concurrent_assigned = 1 ; _is_assigned = 1 ; }
    virtual inline unsigned     IsConcurrentUsed() const        { return _is_concurrent_used ; }
    virtual inline unsigned     IsConcurrentAssigned() const    { return _is_concurrent_assigned ; }

    // RAM detection :
    virtual unsigned            CanBeDualPortRam() const        { return _can_be_dual_port_ram ; }
    virtual void                SetCanBeDualPortRam() ;
    virtual void                SetCannotBeDualPortRam()        { _can_be_dual_port_ram = 0 ; }

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam() const       { return _can_be_multi_port_ram ; }
    virtual void                SetCanBeMultiPortRam() ;
    virtual void                SetCannotBeMultiPortRam()       { _can_be_multi_port_ram = 0 ; }

    // Set/Check VarUsage flags w.r.t. the relevant SynthesisInfo
    virtual void                SetUsed(SynthesisInfo *info) ;
    virtual void                SetAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsUsed(SynthesisInfo *info) const ;
    virtual unsigned            IsAssigned(SynthesisInfo *info) const ;

    virtual void                SetConcurrentUsed(SynthesisInfo *info) ;
    virtual void                SetConcurrentAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsConcurrentUsed(SynthesisInfo *info) const ;
    virtual unsigned            IsConcurrentAssigned(SynthesisInfo *info) const ;

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam(SynthesisInfo *info) const ;
    virtual void                SetCanBeDualPortRam(SynthesisInfo *info) ;
    virtual void                SetCannotBeDualPortRam(SynthesisInfo *info) ;

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam(SynthesisInfo *info) const ;
    virtual void                SetCanBeMultiPortRam(SynthesisInfo *info) ;
    virtual void                SetCannotBeMultiPortRam(SynthesisInfo *info) ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4. It
    // returns 1 if the subtype_indication used in the signal
    // declaration is locally/globally static type/subtype
    virtual void                SetLocallyStaticType() ;
    virtual void                SetGloballyStaticType() ;

    virtual unsigned            GetSignalKind() const ;
    unsigned                    DecodeSignalKind(unsigned signal_kind) const ;
    unsigned                    EncodeSignalKind(unsigned signal_kind) const ;

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

protected:
// back-pointer set during analysis (not owned)
    VhdlIdDef *_resolution_function ;       // Back-pointer to the resolution function (if this is a resolved signal)
// Flags set during analysis, that can be used afterwards (in elaboration).
    unsigned   _is_assigned:1 ;             // Set if the object is 'assigned' somewhere (in an assignment or association)
    unsigned   _is_used:1 ;                 // Set if the object is 'used' somewhere (in an expression or association)
    unsigned   _is_concurrent_assigned:1 ;  // Set if the object is 'assigned' concurrently somewhere (in an assignment or association)
    unsigned   _is_concurrent_used:1 ;      // Set if the object is 'used' concurrently somewhere (in an expression or association)
    unsigned   _can_be_dual_port_ram:1 ;    // Set if the signal can represent a memory with at most one readport and one writeport.
    unsigned   _can_be_multi_port_ram:1 ;   // Set if the signal can represent a memory with any number or read-ports/write-ports.
    unsigned   _is_locally_static_type:1 ;  // Set if the type is locally static. Needed as the subtype_ind is not available
    unsigned   _is_globally_static_type:1 ; // Set if the type is globally static. Needed as the subtype_ind is not available
    unsigned   _signal_kind:2 ;             // VHDL_register, VHDL_bus
} ; // class VhdlSignalId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConstantId : public VhdlIdDef
{
public:
    explicit VhdlConstantId(char *name) ;

    // Copy tree node
    VhdlConstantId(const VhdlConstantId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConstantId(SaveRestore &save_restore) ;

    virtual ~VhdlConstantId() ;

private:
    // Prevent compiler from defining the following
    VhdlConstantId() ;                                  // Purposely leave unimplemented
    VhdlConstantId(const VhdlConstantId &) ;            // Purposely leave unimplemented
    VhdlConstantId& operator=(const VhdlConstantId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLCONSTANTID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy constant id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual void                DeclareConstant(VhdlIdDef *type, unsigned locally_static, unsigned loop_iter, unsigned globally_static, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsConstant() const ;
    virtual unsigned            IsConstantId() const { return 1 ; }

    virtual unsigned            HasInitAssign() const ;

    // Following APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;
    virtual unsigned            GetLocallyStatic() const ;

    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    // For backward compatibility during database restore
    // Do not use these 2 APIs otherwise. Both fields are
    // supposed to be set only during construction through
    // DeclareConstant API. For old databases, we always set
    // Id to be locally static to "fool" the static checks.
    virtual void                SetLocallyStatic() ;
    virtual void                SetLoopIterator() ;

    // Set/Get pointer to constant declaration :
    virtual void                SetDecl(VhdlIdDef *decl) ;
    virtual VhdlIdDef *         Decl() const ;

    // Elaboration
    virtual void                SetValue(VhdlValue *initial_value) ; // Create initial value
    virtual void                SetConstraint(VhdlConstraint *constraint) ; // Viper 4884

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

protected:
// (non-owned) Pointer to the constant id with the initial value (for deferred constants)
// So there are three possibilities :
//    1) points to itself if it is a regular constant with a initial value
//    2) points to NULL if it is a deferred constant, but it has no initial value yet.
//    3) points to another id that is the deferred constant that has the initial value.
// Set during analysis.
    VhdlIdDef *_decl ;

// Set during analysis when we have this decl
    unsigned _is_locally_static:1 ;
    unsigned _is_globally_static:1 ; // VIPER Issue #6349
    unsigned _is_seq_loop_iter:1 ;
    unsigned _is_locally_static_type:1 ;
    unsigned _is_globally_static_type:1 ;
} ; // class VhdlConstantId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlTypeId : public VhdlIdDef
{
public:
    explicit VhdlTypeId(char *name) ;

    // Copy tree node
    VhdlTypeId(const VhdlTypeId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlTypeId(SaveRestore &save_restore) ;

    virtual ~VhdlTypeId() ;

private:
    // Prevent compiler from defining the following
    VhdlTypeId() ;                              // Purposely leave unimplemented
    VhdlTypeId(const VhdlTypeId &) ;            // Purposely leave unimplemented
    VhdlTypeId& operator=(const VhdlTypeId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLTYPEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy type id.
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ; // Set back pointers in type identifier
    virtual void                UpdateType(VhdlMapForCopy &old2new) ;

    // pointer back to the scope that this type defines (only for record types)
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual unsigned            EntityClass() const ;

    // Type of a VHDL type
    enum type_enum {
                     UNKNOWN_TYPE=0, INTEGER_TYPE, REAL_TYPE,
                     ENUMERATION_TYPE, PHYSICAL_TYPE,
                     UNCONSTRAINED_ARRAY_TYPE, CONSTRAINED_ARRAY_TYPE,
                     RECORD_TYPE, ACCESS_TYPE, FILE_TYPE, PHYSICAL_STD_TIME_TYPE,
                     PROTECTED_TYPE, PROTECTED_TYPE_BODY, GENERIC_TYPE,
                     INCOMPLETE_TYPE // VIPER #2893: To distinguish user defined incomplete type with unknown type
                   } ;

    virtual unsigned            IsType() const ;
    virtual unsigned            IsIncompleteType()  const;
    virtual unsigned            IsDefinedIncompleteType() const ;
    virtual unsigned            IsIntegerType() const ;
    virtual unsigned            IsRealType() const ;
    virtual unsigned            IsEnumerationType() const ;
    virtual unsigned            IsArrayType() const ;
    virtual unsigned            IsPhysicalType() const ;
    virtual unsigned            IsStdTimeType() const ;
    virtual unsigned            IsUnconstrainedArrayType() const ;
    virtual unsigned            IsConstrainedArrayType() const ;
    virtual unsigned            IsRecordType() const ;
    virtual unsigned            IsAccessType() const ;
    virtual unsigned            ContainsAccessType() const ;
    virtual unsigned            IsFileType() const ;
    virtual unsigned            IsScalarType() const ;
    virtual unsigned            IsNumericType() const ;
    virtual unsigned            IsDiscreteType() const ;
    virtual unsigned            IsProtectedType() const ;
    virtual unsigned            IsProtectedTypeBody() const ;
    virtual unsigned            IsStringType() const ;
    virtual unsigned            IsNaturalType() const ;
    virtual unsigned            IsPositiveType() const ;
    virtual unsigned            IsGenericType() const ;
    virtual unsigned            IsGeneric() const ;
    virtual unsigned            IsStdULogicType() const ;
    virtual unsigned            IsStdBitType() const ;
    virtual unsigned            IsStdBooleanType() const ;
    virtual unsigned            IsStdCharacterType() const ;

    virtual unsigned            IsArray() const ;
    virtual unsigned            IsRecord() const ;
    virtual unsigned            IsProtected() const ;
    virtual unsigned            IsProtectedBody() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    // It returns 1 if the subtype_indication used in the type
    // declaration is locally/globally static type/subtype For
    // typeid, IsLocallyStatic and IsLocallyStaticType are
    // equivalent, so are IsGloballyStatic and IsGloballyStaticType
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    virtual void                SetLocallyStaticType() ;
    virtual void                SetGloballyStaticType() ;
    virtual void                SetDefinedIncompleteType() ;

    // Type declarations
    virtual void                DeclareIntegerType() ;
    virtual void                DeclareRealType() ;
    virtual void                DeclarePhysicalType(Map *physical_units) ;
    virtual void                DeclareEnumerationType(Map *enumeration_literals) ;
    virtual void                DeclareConstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static) ;
    virtual void                DeclareUnconstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static) ;
    virtual void                DeclareRecordType(Map *element_decl_list) ;
    virtual void                DeclareFileType(VhdlIdDef *subtype_id) ;
    virtual void                DeclareAccessType(VhdlIdDef *subtype_id, int locally_static, int globally_static) ;
    virtual void                DeclareIncompleteType(VhdlIdDef *subtype_id) ; // Set the definition of the incomplete type

    virtual unsigned            TypeMatch(const VhdlIdDef *id) const ;
    virtual unsigned            TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const ;
    virtual unsigned            IsCloselyRelatedType(const VhdlIdDef *id) const ;

    virtual VhdlIdDef *         Type() const ;     // Type() of a type returns self
    virtual VhdlIdDef *         BaseType() const ; // same thing
    virtual VhdlTypeId*         TypeCast() const  { return const_cast<VhdlTypeId*>(this) ; }
    virtual unsigned            NumUnconstrainedRanges() const ;

    // Set attribute on a type :
    virtual unsigned            SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_value) ; // set attribute 'attr_id' with value expression 'attr_value' on this identifier. This is automatically called during analysis.

    // Array type related
    virtual unsigned            Dimension() const ;
    virtual VhdlIdDef *         ElementType() const ;
    virtual VhdlIdDef *         ElementType(unsigned dim) ;
    virtual VhdlIdDef *         IndexType(unsigned dim) const ;
    virtual VhdlIdDef *         AggregateElementType() const ;        // Hack for anonymous array aggregate types
    virtual unsigned            DeepDimension() const ;              // Dimension of this object including (deep)dimensions of it's element type

    // Enumeration type related
    virtual unsigned            NumOfEnums() const ;                  // Number of enumerated values in a enumeration type
    virtual VhdlIdDef *         GetEnum(const char *name) const ;     // Find a enumeration value in enum type. Fast.
    virtual VhdlIdDef *         GetEnumAt(unsigned pos) const ;       // Get enum by position ; 0 is the first
    virtual VhdlIdDef *         GetEnumWithBitEncoding(char b) const ;// Get the first enumeration literal encoded with this bit value

    // Record type related
    virtual unsigned            NumOfElements() const ;               // Number of record elements in this record type
    virtual VhdlIdDef *         GetElement(const char *name) const ;  // get a record element by name. Fast.
    virtual VhdlIdDef *         GetElementAt(unsigned pos) const ;    // get a record element by position (also Fast)

    // Physical type related
    virtual VhdlIdDef *         GetUnit(const char *name) const ;     // get a unit from physical type by name. Fast.

    // File/Access type related
    virtual VhdlIdDef *         DesignatedType() const ;

    // return resolution function of this type. This routine will look into an element type of the type (since a type itself cannot have a resolution function).
    virtual VhdlIdDef *         ResolutionFunction() const ;
    virtual unsigned            IsUnconstrained() const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

    // Declare the (predefined) operators that go with a type declaration (LRM 14.2)
    virtual void                DeclareOperators() ;

    // Viper #3898 : Added second argument for type recursion check
    virtual unsigned            IsTypeContainsFile(unsigned consider_access_type, Set& recursion_set) ;
    virtual unsigned            IsTypeContainsRecord() ; // Added for Viper #3751
    virtual unsigned            IsTypeContainsUnconstrainedArray(Set& recursion_set) ; // Added for Viper #7758
    virtual VhdlIdDef *         GetActualId() const ;
    virtual VhdlIdDef *         TakeActualId() ;

    enum type_enum              GetTypeEnum() const         { return _type_enum ; }
    virtual Map *               GetTypeDefList() const      { return _list ; }
    virtual Map *               GetList() const             { return _list ; } // Backward compatibility

    // Elaboration
    virtual void                SetAttribute(VhdlIdDef *attr, VhdlValue *value) ;

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

    // handy predefined oper declaration routine.
    virtual void                DeclarePredefinedOp(unsigned op, const VhdlIdDef *arg1_type, const VhdlIdDef *arg2_type, VhdlIdDef *ret_type) const ;
    virtual void                DeclarePredefinedOper(unsigned op, Array *args, unsigned owns_arg_id, unsigned is_func) const ;
    static void                 AddOperatorInterface(Array *args, char *arg_name, VhdlIdDef *type, unsigned kind, unsigned mode) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // Accessor functions to access Verilog pkg/lib names
    virtual void               SetVerilogPkgName(const char *pkg_name) ;
    virtual void               SetVerilogPkgLibName(const char *pkg_lib_name) ;
    virtual unsigned           IsVerilogType() const { return (_verilog_pkg_lib_name && _verilog_pkg_name) ; }
    virtual VeriIdDef *        GetVerilogTypeFromPackage() const ;

#endif

    // VIPER #7343 : Set/Get type declaration to type identifier only :
    virtual void               SetDeclaration(VhdlDeclaration *d) { _type_decl = d ; }
    virtual VhdlDeclaration *  GetDeclaration() const { return _type_decl ; }

protected:
// Fields set during analysis :
    enum type_enum   _type_enum ; // enum token for the style of type decl : CONSTRAINED_ARRAY_TYPE, RECORD_TYPE etc. list is above.
    Map             *_list ; // const char * -> VhdlIdDef*
                 // elements are not owned (neither key nor value).
                 // Used for store VhdlIdDefs ; meaning of these depends on '_type_enum' :
                 //    enum_types       : store enumeration ids of this type (VhdlEnumerationId)
                 //    physical types   : store physical unit ids of this type (VhdlPhysicalUnitId)
                 //    record types     : store record-element ids of this type (VhdlElementId)
                 //    array types      : store index types (ids) of this type (VhdlTypeId)
                 // All are stored with their name as key.
                 // Array types can have same (key & value) elements, since multi-dimensional index types can be the same at each dimension.
    VhdlScope       *_scope ; // back-pointer to the scope that the type declares (only for record types)
    unsigned         _is_locally_static_type:1 ;
    unsigned         _is_globally_static_type:1 ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    char            *_verilog_pkg_lib_name ;   // Viper 6896 : Store Verilog package Library name to get the Verilog type
    char            *_verilog_pkg_name ;       // Viper 6896 : Store Vhdl Package Name to get the Verilog type
#endif
    // When compile flag VHDL_ID_SUBTYPE_BACKPOINTER is on, we can get subtype indication
    // from port/signal/variable identifier. But if subtype indication is a reference
    // of type, we cannot get parse tree of that type declaration to check its
    // bounds and element type. To get that add this back-pointer
    VhdlDeclaration *_type_decl ;   // Viper 7343 : Store back pointer of type declaration
} ; // class VhdlTypeId

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
// This new class is made to store Vhdl2008 generic types. Earlier
// this was maintained in VhdlTypeId by the _type_enum(VHDL_GENERIC) flag. However
// since the enum was already used the constrained/unconstrained enum
// values of the generic type could not be updated. The _actual_type
// will not solve the problem. For example for a type like
// std_logic_vector(0 to 3) the actual_id would be std_logic_vector id
// which is always unconstrained and we have no way to store the dimension is
// present information in the type_enum. The type_enum is already used to
// store the GENERIC_TYPE, which is also very much required by IsGeneric routines.
class VFC_DLL_PORT VhdlGenericTypeId : public VhdlTypeId
{
public:
    explicit VhdlGenericTypeId(char *name) ;

    // Copy tree node
    VhdlGenericTypeId(const VhdlGenericTypeId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlGenericTypeId(SaveRestore &save_restore) ;

    virtual ~VhdlGenericTypeId() ;

private:
    // Prevent compiler from defining the following
    VhdlGenericTypeId() ;                              // Purposely leave unimplemented
    VhdlGenericTypeId(const VhdlGenericTypeId &) ;            // Purposely leave unimplemented
    VhdlGenericTypeId& operator=(const VhdlGenericTypeId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLGENERICTYPEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;
    virtual void                SetActualId(VhdlIdDef *id, Array* assoc_list) ;
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;
    virtual unsigned            IsGenericType() const ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

} ; // class VhdlGenericTypeId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlProtectedTypeId : public VhdlTypeId
{
public:
    explicit VhdlProtectedTypeId(char *name) ;

    // Copy tree node
    VhdlProtectedTypeId(const VhdlProtectedTypeId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlProtectedTypeId(SaveRestore &save_restore) ;

    virtual ~VhdlProtectedTypeId() ;

private:
    // Prevent compiler from defining the following
    VhdlProtectedTypeId() ;                                        // Purposely leave unimplemented
    VhdlProtectedTypeId(const VhdlProtectedTypeId &) ;            // Purposely leave unimplemented
    VhdlProtectedTypeId& operator=(const VhdlProtectedTypeId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPROTECTEDTYPEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual void                DeclareProtectedType(VhdlProtectedTypeDef *type_def) ;

    // Accessor function
    virtual VhdlProtectedTypeDef       *GetProtectedTypeDef() const { return _type_def ; }

    // VIPER #7775 : Set/get protected type body id to protected type id
    virtual void                        SetProtectedTypeBodyId(VhdlIdDef *body_id) ;
    virtual VhdlIdDef *                 GetProtectedTypeBodyId() const ;
protected:

    VhdlProtectedTypeDef       *_type_def ;
    VhdlIdDef                  *_body_id ; // VIPER #7775 : Back pointer to protected type body id
} ; // class VhdlProtectedTypeId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlProtectedTypeBodyId : public VhdlTypeId
{
public:
    explicit VhdlProtectedTypeBodyId(char *name) ;

    // Copy tree node
    VhdlProtectedTypeBodyId(const VhdlProtectedTypeBodyId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlProtectedTypeBodyId(SaveRestore &save_restore) ;

    virtual ~VhdlProtectedTypeBodyId() ;

private:
    // Prevent compiler from defining the following
    VhdlProtectedTypeBodyId() ;                                        // Purposely leave unimplemented
    VhdlProtectedTypeBodyId(const VhdlProtectedTypeBodyId &) ;            // Purposely leave unimplemented
    VhdlProtectedTypeBodyId& operator=(const VhdlProtectedTypeBodyId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPROTECTEDTYPEBODYID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual void                DeclareProtectedTypeBody(VhdlProtectedTypeDefBody *type_def) ;

    // Accessor function
    virtual VhdlProtectedTypeDefBody  *  GetProtectedTypeDefBody() const { return _type_def_body ; }

    // VIPER #7775 : Set/get protected type def id
    virtual void                         SetProtectedTypeDefId(VhdlIdDef *id) ;
    virtual VhdlIdDef *                  GetProtectedTypeDefId() const ;
protected:

    VhdlProtectedTypeDefBody   *_type_def_body ;
    VhdlIdDef                  *_typedef_id ; // VIPER #7775 : Back pointer to protected type def id
} ; // class VhdlProtectedTypeBodyId

/* -------------------------------------------------------------- */

// Three special types
class VFC_DLL_PORT VhdlUniversalInteger : public VhdlTypeId
{
public:
    explicit VhdlUniversalInteger(char *name) ;

    // Copy tree node
    VhdlUniversalInteger(const VhdlUniversalInteger &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlUniversalInteger(SaveRestore &save_restore) ;

    virtual ~VhdlUniversalInteger() ;

private:
    // Prevent compiler from defining the following
    VhdlUniversalInteger() ;                                        // Purposely leave unimplemented
    VhdlUniversalInteger(const VhdlUniversalInteger &) ;            // Purposely leave unimplemented
    VhdlUniversalInteger& operator=(const VhdlUniversalInteger &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLUNIVERSALINTEGER; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy universal integer
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            IsUniversalInteger() const ;

protected:
} ; // class VhdlUniversalInteger

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlUniversalReal : public VhdlTypeId
{
public:
    explicit VhdlUniversalReal(char *name) ;

    // Copy tree node
    VhdlUniversalReal(const VhdlUniversalReal &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlUniversalReal(SaveRestore &save_restore) ;

    virtual ~VhdlUniversalReal() ;

private:
    // Prevent compiler from defining the following
    VhdlUniversalReal() ;                                     // Purposely leave unimplemented
    VhdlUniversalReal(const VhdlUniversalReal &) ;            // Purposely leave unimplemented
    VhdlUniversalReal& operator=(const VhdlUniversalReal &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLUNIVERSALREAL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy universal real
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            IsUniversalReal() const ;

protected:
} ; // class VhdlUniversalReal

/* -------------------------------------------------------------- */

// Next one used as 'virtual' type halfway aggregates.
// Needed for multi-dimensional array types
class VFC_DLL_PORT VhdlAnonymousType : public VhdlTypeId
{
public:
    explicit VhdlAnonymousType(char *name) ;

    // Copy tree node
    VhdlAnonymousType(const VhdlAnonymousType &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAnonymousType(SaveRestore &save_restore) ;

    virtual ~VhdlAnonymousType() ;

private:
    // Prevent compiler from defining the following
    VhdlAnonymousType() ;                                     // Purposely leave unimplemented
    VhdlAnonymousType(const VhdlAnonymousType &) ;            // Purposely leave unimplemented
    VhdlAnonymousType& operator=(const VhdlAnonymousType &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLANONYMOUSTYPE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy anonymous type
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;
    virtual void                UpdateAnonymousType(VhdlIdDef * /*old_type_id*/, VhdlMapForCopy &/*old2new*/) ; // Viper #6146 : Update _list field and declare the type

    virtual unsigned            IsAnonymousType() const ;

    virtual void                DeclareOperators() ;

protected:
} ; // class VhdlAnonymousType

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSubtypeId : public VhdlIdDef
{
public:
    explicit VhdlSubtypeId(char *name) ;

    // Copy tree node
    VhdlSubtypeId(const VhdlSubtypeId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSubtypeId(SaveRestore &save_restore) ;

    virtual ~VhdlSubtypeId() ;

private:
    // Prevent compiler from defining the following
    VhdlSubtypeId() ;                                 // Purposely leave unimplemented
    VhdlSubtypeId(const VhdlSubtypeId &) ;            // Purposely leave unimplemented
    VhdlSubtypeId& operator=(const VhdlSubtypeId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSUBTYPEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy subtype id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual void                DeclareSubtype(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned is_unconstrained, unsigned locally_static_type, unsigned globally_static_type) ;

    virtual VhdlScope *         LocalScope() const ;    // return scope of the target type.

    virtual unsigned            EntityClass() const ;

    virtual Map *               GetTypeDefList() const  { return (_type) ? _type->GetTypeDefList() : 0 ; } // Return back pointer to type definition elements

    // VIPER #7804: Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    virtual unsigned            IsType() const ;
    virtual unsigned            IsIntegerType() const ;
    virtual unsigned            IsRealType() const ;
    virtual unsigned            IsEnumerationType() const ;
    virtual unsigned            IsPhysicalType() const ;
    virtual unsigned            IsStdTimeType() const ;
    virtual unsigned            IsArrayType() const ;
    virtual unsigned            IsUnconstrainedArrayType() const ;
    virtual unsigned            IsConstrainedArrayType() const ;
    virtual unsigned            IsRecordType() const ;
    virtual unsigned            IsAccessType() const ;
    virtual unsigned            ContainsAccessType() const ;
    virtual unsigned            IsFileType() const ;
    virtual unsigned            IsSubtype() const ;
    virtual unsigned            IsProtectedType() const ;
    virtual unsigned            IsProtectedTypeBody() const ;
    virtual unsigned            IsStdULogicType() const ;
    virtual unsigned            IsStdBitType() const ;
    virtual unsigned            IsStdBooleanType() const ;
    virtual unsigned            IsStdCharacterType() const ;

    virtual unsigned            IsScalarType() const ;
    virtual unsigned            IsNumericType() const ;
    virtual unsigned            IsDiscreteType() const ;
    virtual unsigned            IsStringType() const ;
    virtual unsigned            IsNaturalType() const ;
    virtual unsigned            IsPositiveType() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    // It returns 1 if the subtype_indication used in the type
    // declaration is locally/globally static type/subtype For
    // subtypeid, IsLocallyStatic and IsLocallyStaticType are
    // equivalent, so are IsGloballyStatic and IsGloballyStaticType
    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    virtual void                SetLocallyStaticType() ;
    virtual void                SetGloballyStaticType() ;

    virtual VhdlIdDef *         DesignatedType() const ; // subtype of subtype is target type..

    virtual VhdlIdDef *         ResolutionFunction() const ; // return resolution function of this (subtype).

    virtual unsigned            TypeMatch(const VhdlIdDef *id) const ;
    virtual unsigned            TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const ;
    virtual unsigned            IsCloselyRelatedType(const VhdlIdDef *id) const ;
    // Viper #3898 : Added second argument for type recursion check
    virtual unsigned            IsTypeContainsFile(unsigned consider_access_type, Set& recursion_set) ;
    virtual unsigned            IsTypeContainsUnconstrainedArray(Set& recursion_set) ; // Added for Viper #7758
    virtual void                SetDeclaration(VhdlDeclaration *d) { _type_decl = d ; }
    virtual VhdlDeclaration *   GetDeclaration() const { return _type_decl ; }
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

protected:
// back-pointers set during analysis (not owned)
    VhdlIdDef *_resolution_function ; // back-pointer to the resolution function (if this is a resolved subtype)
// 11/04 flag added for VIPER #1949
// flag set during declaration
    unsigned   _is_unconstrained:1 ;  // 0/1 (unconstrained ports)

    unsigned   _is_locally_static_type:1 ;
    unsigned   _is_globally_static_type:1 ;
    VhdlDeclaration *_type_decl ;   // Viper 7505 : Store back pointer of subtype declaration like type declaration
} ; // class VhdlSubtypeId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSubprogramId : public VhdlIdDef
{
public:
    explicit VhdlSubprogramId(char *name) ;

    // Copy tree node
    VhdlSubprogramId(const VhdlSubprogramId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSubprogramId(SaveRestore &save_restore) ;

    virtual ~VhdlSubprogramId() ;

private:
    // Prevent compiler from defining the following
    VhdlSubprogramId() ;                                    // Purposely leave unimplemented
    VhdlSubprogramId(const VhdlSubprogramId &) ;            // Purposely leave unimplemented
    VhdlSubprogramId& operator=(const VhdlSubprogramId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const              { return ID_VHDLSUBPROGRAMID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy subprogram id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         Type() const ;                       // Get type of the object. type of type is type itself
    virtual unsigned            IsHomograph(VhdlIdDef *id) const ;
    virtual unsigned            IsSubprogram() const ;
    virtual unsigned            IsSubprogramInst() const ;
    virtual unsigned            IsGeneric() const ;
    virtual void                SetAsGeneric() ;

    virtual unsigned            EntityClass() const ;

    virtual void                SetAsProcedure() ;
    virtual unsigned            IsFunction() const ;
    virtual unsigned            IsProcedure() const ;

    // flag setting
    virtual inline void         SetAssignedBeforeUsed()         { _is_assigned_before_used = 1 ; }
    virtual inline void         SetUsedBeforeAssigned()         { _is_used_before_assigned = 1 ; }
    virtual inline void         UnSetAssignedBeforeUsed()       { _is_assigned_before_used = 0 ; }
    virtual inline void         UnSetUsedBeforeAssigned()       { _is_used_before_assigned = 0 ; }
    virtual inline unsigned     IsAssignedBeforeUsed() const    { return _is_assigned_before_used ; }
    virtual inline unsigned     IsUsedBeforeAssigned() const    { return _is_used_before_assigned ; }

    // Usage information
    virtual void                SetAssignedBeforeUsed(SynthesisInfo *info) ;
    virtual void                SetUsedBeforeAssigned(SynthesisInfo *info) ;
    virtual void                UnSetAssignedBeforeUsed(SynthesisInfo *info) ;
    virtual void                UnSetUsedBeforeAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsUsedBeforeAssigned(SynthesisInfo *info) const ;
    virtual unsigned            IsAssignedBeforeUsed(SynthesisInfo *info) const ;

    virtual void                DeclareFunction(VhdlIdDef *return_type, Array *args, Array *generics) ;
    virtual void                DeclareProcedure(Array *args, Array *generics) ;

    // Set/Get the scope of this
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ; // get scope via the subprogram specification

    // Routines that return 0 here
    virtual unsigned            IsArray() const ;
    virtual unsigned            IsRecord() const ;
    virtual unsigned            IsProtected() const ;
    virtual unsigned            IsProtectedBody() const ;

    // Access argument types
    virtual unsigned            NumOfArgs() const ;
    virtual VhdlIdDef *         Arg(unsigned idx) const ;
    virtual VhdlIdDef *         ArgType(unsigned idx) const ;

    // Set/Get the subprogram spec/body
    virtual void                SetSpec(VhdlSpecification *spec) ;
    virtual VhdlSpecification * Spec() const ;
    virtual void                SetBody(VhdlSubprogramBody *body) ;
    virtual VhdlSubprogramBody *Body() const ;

    // pragma settings on subprograms
    virtual unsigned            GetPragmaFunction() const ;
    virtual unsigned            GetPragmaSigned() const ;
    virtual unsigned            GetPragmaLabelAppliesTo() const ;
    virtual void                SetPragmaFunction(unsigned function) ;
    virtual void                SetPragmaSigned() ;
    virtual void                SetPragmaLabelAppliesTo() ;

    // Set/Get the subprogram instantiation decl/interface subprogram decl
    virtual void                SetSubprogInstanceDecl(VhdlDeclaration *inst) ;
    virtual VhdlDeclaration *   GetSubprogInstanceDecl() const ;
    virtual void                SetActualId(VhdlIdDef *id, Array* assoc_list) ;
    virtual VhdlIdDef *         GetActualId() const ;
    virtual VhdlIdDef *         TakeActualId() ;

    virtual Array *             GetArgs() const         { return _args ; }  // Get program parameters

    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual Array *             GetGenerics() const         { return _generics ; }

    virtual void                SetIsProcessing(unsigned s)  { _is_processing = s ; } // Set _is_processing flag
    virtual unsigned            IsProcessing() const         { return _is_processing ; } // Return 1 if this subprogram is being processed now by application

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

    // Elaboration
    // unary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;
    // binary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;
    // function/procedure call
    virtual VhdlValue *         ElaborateSubprogram(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef *subprog_inst) ;
    // for pragma-directed functions
    VhdlValue *                 ElaboratePragmaFunction(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df) ; // for 'pragma' functions
    void                        ElaboratePragmaProcedure(const Array *arguments, VhdlDataFlow *df) ; // for 'pragma' procedures
  
    virtual unsigned            IsOverWrittenPredefinedOperator() const { return  _is_overwritten_predefinedOperator ; }
    virtual void                SetOverWrittenPredefinedOperator() { _is_overwritten_predefinedOperator = 1 ; }

    void                        CheckSubprogramForProtectedType() const ;
    virtual void                Prune(Set* ids) ;

protected:
// Set at analyze time :
    Array               *_generics ;                  // Vhdl 2008 - 4.2.1 (elements are not owned) Array of VhdlIdDef* to the generics of this subprogram, ordered.
    Array               *_args ;                      // Array of VhdlIdDef*.  back-pointers to the parameters of this subprogram, in order of declaration.
    VhdlSpecification   *_spec ;                      // Back-pointer to the subprogram spec
    VhdlSubprogramBody  *_body ;                      // Back-pointer to the subprogram body (if there) (this guy could point from primary unit spec to a secondary-unit body)
// Pragma's : if (synthesis) pragma was set on this subprogram, it ends up here :
    unsigned             _pragma_function:12 ;        // Pragma'ed function token; as returned by PragmaFunction(). 0 if no pragma on this function.
    unsigned             _pragma_signed:12 ;          // Pragma'ed function flag. as return by PragmaSigned(). 1 if signed pragma'd function, 0 in all other cases.
// Flag set if this is a procedure (rather than a function) :
    unsigned             _is_assigned_before_used:1 ; // Set if the object is always assigned before used (no feedback logic needed).
    unsigned             _is_used_before_assigned:1 ; // Set if the object is used before assigned (in at least one place).
    unsigned             _is_procedure:1 ;            // 1 if this is a procedure (0 for a function)
    unsigned             _is_processing:1 ;           // Flag to indicate that this subprogram is being processed now
    unsigned             _is_label_applies_to_pragma:1 ; // Viper #3765 : to indicate if this subprog has label_applies_to pragma
    unsigned             _is_generic:1 ;              // Viper #5918 : interface subprogram decl in generic clause
    unsigned             _is_overwritten_predefinedOperator:1 ; // Flag to denote that this name operator was originally predefined
    VhdlDeclaration     *_sub_inst ;                  // Back-pointer to subprogram instantiation/interface subprogram declaration
} ; // class VhdlSubprogramId

/* -------------------------------------------------------------- */

// Class used for pre-defined operators
class VFC_DLL_PORT VhdlOperatorId : public VhdlSubprogramId
{
public:
    explicit VhdlOperatorId(unsigned oper_type) ;

    // Copy tree node
    VhdlOperatorId(const VhdlOperatorId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlOperatorId(SaveRestore &save_restore) ;

    virtual ~VhdlOperatorId() ;

private:
    // Prevent compiler from defining the following
    VhdlOperatorId() ;                                  // Purposely leave unimplemented
    VhdlOperatorId(const VhdlOperatorId &) ;            // Purposely leave unimplemented
    VhdlOperatorId& operator=(const VhdlOperatorId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLOPERATORID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy operator id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;
    virtual void                UpdateArgsAndType(VhdlMapForCopy &old2new) ; // Update the arguments and _type field.

    virtual unsigned            IsPredefinedOperator() const ;
    virtual unsigned            GetOperatorType() const     { return _oper_type ; }
    virtual void                SetOwnsArgId() { _owns_arg_id = 1 ; } // VIPER #5996

    // Elaboration
    // unary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;
    // binary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;
    // function/procedure call
    virtual VhdlValue *         ElaborateSubprogram(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef *subprog_inst) ;

protected:
    unsigned _oper_type:28 ; // token representing the operator used (VHDL_and, VHDL_not, VHDL_minus etc).
    unsigned _owns_arg_id:1 ;
} ; // class VhdlOperatorId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlInterfaceId : public VhdlIdDef
{
public:
    explicit VhdlInterfaceId(char *name) ;

    // Copy tree node
    VhdlInterfaceId(const VhdlInterfaceId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInterfaceId(SaveRestore &save_restore) ;

    virtual ~VhdlInterfaceId() ;

private:
    // Prevent compiler from defining the following
    VhdlInterfaceId() ;                                   // Purposely leave unimplemented
    VhdlInterfaceId(const VhdlInterfaceId &) ;            // Purposely leave unimplemented
    VhdlInterfaceId& operator=(const VhdlInterfaceId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLINTERFACEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Copy interface id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // Static Elaboration
    virtual void                SetHasInitAssign()          { _has_init_assign = 1 ; } // Set back pointer
    virtual void                ResetHasInitAssign()        { _has_init_assign = 0 ; }
    // Set 'this' id as constrained
    virtual void                SetConstrained()            { _is_unconstrained = 0 ; }
    // VIPER #4547 : Set/get routines to indicate whether valid actual (not open)
    // is specified for this port of component :
    virtual void                SetIsActualPresent()        { _is_actual_present = 1 ; }
    virtual unsigned            IsActualPresent() const     { return _is_actual_present ; }

    virtual void                SetRefFormal(unsigned is_ref_formal)        { _is_ref_formal =  is_ref_formal ; }
    virtual unsigned            IsRefFormal() const     { return _is_ref_formal ; }

    virtual void                DeclareInterfaceId(VhdlIdDef *type, unsigned kind, unsigned mode, unsigned signal_kind, unsigned has_init_value, unsigned is_unconstrained, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type) ;

    virtual unsigned            HasInitAssign() const ;
    virtual void                SetObjectKind(unsigned object_kind) ;
    virtual unsigned            IsInterfaceObject(unsigned object_kind) const ;
    virtual unsigned            GetObjectKind() const ;
    virtual unsigned            IsGeneric() const ;
    virtual unsigned            IsPort() const ;
    virtual unsigned            IsSubprogramFormal() const ;
    virtual unsigned            IsInput() const ;
    virtual unsigned            IsOutput() const ;
    virtual unsigned            IsInout() const ;
    virtual unsigned            IsLinkage() const ;
    virtual unsigned            IsBuffer() const ;
    virtual unsigned            Mode() const ;               // Get port direction
    virtual void                SetMode(unsigned mode) ;     // Set port mode (direction)
    virtual VhdlIdDef *         ResolutionFunction() const ; // Return resolution function of this (signal).

    virtual unsigned            EntityClass() const ;
    virtual unsigned            GetKind() const ;
    virtual unsigned            GetSignalKind() const ;
    virtual unsigned            IsVariable() const ;
    virtual unsigned            IsSignal() const ;
    virtual unsigned            IsConstant() const ;
    virtual unsigned            IsFile() const ;

    virtual inline void         SetConcurrentUsed()         { _is_used = 1 ; }
    virtual inline void         SetConcurrentAssigned()     { _is_assigned = 1 ; }
    virtual inline void         SetUsed()                   { _is_used = 1 ; }
    virtual inline void         SetAssigned()               { _is_assigned = 1 ; }
    virtual inline unsigned     IsUsed() const              { return _is_used ; }
    virtual inline unsigned     IsAssigned() const          { return _is_assigned ; }

    // Set/Check VarUsage flags w.r.t. the relevant SynthesisInfo
    virtual void                SetUsed(SynthesisInfo *info) ;
    virtual void                SetAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsUsed(SynthesisInfo *info) const ;
    virtual unsigned            IsAssigned(SynthesisInfo *info) const ;
    virtual void                SetConcurrentUsed(SynthesisInfo *info) ;
    virtual void                SetConcurrentAssigned(SynthesisInfo *info) ;

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam(SynthesisInfo *info) const ;
    virtual void                SetCanBeMultiPortRam(SynthesisInfo *info) ;
    virtual void                SetCannotBeMultiPortRam(SynthesisInfo *info) ;

    virtual unsigned            IsUnconstrained() const     { return _is_unconstrained ; }

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2 It
    // returns 1 if the subtype_indication used in the interface
    // declaration is locally/globally static type/subtype
    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    virtual void                SetLocallyStaticType() ;
    virtual void                SetGloballyStaticType() ;

    virtual void                SetRefTarget(VhdlDiscreteRange * r) { _ref_expr = r ; }
    virtual VhdlDiscreteRange*  GetRefTarget() const { return _ref_expr ; } // Viper 6781

    // 7599: multi-port RAM detection for subprogram formals:
    virtual unsigned            CanBeMultiPortRam() const ;
    virtual void                SetCanBeMultiPortRam() ;
    virtual void                SetCannotBeMultiPortRam()       { _can_be_multi_port_ram = 0 ; }

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                SetVerilogActualWidth(unsigned s) { _verilog_actual_size = s ; }
    virtual unsigned            GetVerilogActualWidth() const     { return _verilog_actual_size ; }
#endif
    virtual void                SetVerilogAssocList(Array* assoc_list) { _verilog_assoc_list = assoc_list ; }
    virtual Array*              GetVerilogAssocList() const       { return _verilog_assoc_list ; }

private:
    // Following 8 APIs are meant for internal use to reduce the bit fields needed.
    // The 4 fields, obj_kind, kind, mode and signal_kind were using 10 bit fields
    // each. This is redundant. Compacting them helps reduce 32 bits for each object
    unsigned                    DecodeObjectKind(unsigned obj_kind) const ;
    unsigned                    EncodeObjectKind(unsigned obj_kind) const ;

    unsigned                    DecodeMode(unsigned mode) const ;
    unsigned                    EncodeMode(unsigned mode) const ;

    unsigned                    DecodeSignalKind(unsigned signal_kind) const ;
    unsigned                    EncodeSignalKind(unsigned signal_kind) const ;

protected:
// back-pointers set during analysis (not owned) :
    VhdlIdDef   *_resolution_function ; // Back-pointer to the resolution function id for if this is a signal interface id.
// Flags set as part of declaration
    unsigned     _object_kind:3 ;       // VHDL_generic, VHDL_port, VHDL_parameter, 0
    unsigned     _kind:3 ;              // VHDL_constant, VHDL_signal, VHDL_variable, VHDL_file
    unsigned     _mode:4 ;              // VHDL_in, VHDL_inout, VHDL_out, VHDL_buffer, VHDL_linkage
    unsigned     _signal_kind:2 ;       // VHDL_register, VHDL_bus
    unsigned     _has_init_assign:1 ;   // 0/1
    unsigned     _is_unconstrained:1 ;  // 0/1 (unconstrained ports)
// Flags set during analysis
    unsigned     _is_assigned:1 ;       // Set if the object is 'assigned' somewhere (in an assignment or association)
    unsigned     _is_used:1 ;           // Set if the object is 'used' somewhere (in an expression or association)

    unsigned     _is_locally_static_type:1 ;    // Set if the type is locally static. Needed as the subtype_ind is not available
    unsigned     _is_globally_static_type:1 ;    // Set if the type is globally static. Needed as the subtype_ind is not available
    unsigned     _is_ref_formal:1 ;  // This flag will be 1 if actual is specified and not open
    unsigned     _can_be_multi_port_ram:1 ; // Viper 7599: Allow ram detection for actuals in procedure calls
    Array*       _verilog_assoc_list ; // Array of VhdlRange or Open
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    unsigned     _verilog_actual_size ; // width of actual from vhdl instantiation in verilog
#endif
    unsigned     _is_actual_present:1 ;  // This flag will be 1 if actual is specified and not open
    VhdlDiscreteRange *  _ref_expr ; // Viper 6871 6989 :VhdlExpression to store the reference actual in formal to actual association in subprog call
} ; // class VhdlInterfaceId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlEnumerationId : public VhdlIdDef
{
public:
    explicit VhdlEnumerationId(char *name) ;

    // Copy tree node
    VhdlEnumerationId(const VhdlEnumerationId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlEnumerationId(SaveRestore &save_restore) ;

    virtual ~VhdlEnumerationId() ;

private:
    // Prevent compiler from defining the following
    VhdlEnumerationId() ;                                     // Purposely leave unimplemented
    VhdlEnumerationId(const VhdlEnumerationId &) ;            // Purposely leave unimplemented
    VhdlEnumerationId& operator=(const VhdlEnumerationId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLENUMERATIONID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    // Copy enumeration id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            IsHomograph(VhdlIdDef *id) const ;
    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsEnumerationLiteral() const ;
    virtual void                DeclareEnumerationId(VhdlIdDef *type, unsigned pos) ;
    virtual verific_uint64      Position() const ; // Position in its type

    // Elaboration : encoding
    virtual void                SetBitEncoding(char b) ;    // For an enumeration literal : set as a bit(encoded) value (0,1,X or Z)
    virtual char                GetBitEncoding() const ;    // For an enumeration literal : get the bit(encoded) value (0,1,X or Z)
    // multi-bit encoding
    virtual void                SetEncoding(const char *encoding) ; // For an enumeration literal : set as a multi-bit(encoded) value (0,1,X or Z)
    virtual const char *        GetEncoding() const ;               // For an enumeration literal : get the multi-bit(encoded) value (0,1,X or Z)
    // one-hot encoding
    virtual void                SetOnehotEncoded() ;        // For an enumeration literal : flag that we encode it one-hot.
    virtual unsigned            IsOnehotEncoded() const ;   // For an enumeration literal : check if we encode it one-hot.

    virtual void                SetUserEncoded() ; // Viper #7182/7346
    virtual unsigned            IsUserEncoded() const ; // Viper #7182/7346

protected:
// tokens set during analysis :
    unsigned    _position:20 ;        // Position of this enumeration value id in its enumeration type
// tokens set during elaboration (for synthesis encoding)
    unsigned    _bit_encoding:8 ;     // Single-bit encoding of this enumeration value : a single character ('0','1','X','Z')
    unsigned    _onehot_encoded:1 ;   // Flag if this value is one-hot encoded.
    unsigned    _enum_encoded:1 ;     // Flag if this value is enum encoded.
    char       *_encoding ;           // Multi-bit encoding of this enumeration value (string of characters ('0','1','X','Z') )
} ; // class VhdlEnumerationId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlElementId : public VhdlIdDef
{
public:
    explicit VhdlElementId(char *name) ;

    // Copy tree node
    VhdlElementId(const VhdlElementId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlElementId(SaveRestore &save_restore) ;

    virtual ~VhdlElementId() ;

private:
    // Prevent compiler from defining the following
    VhdlElementId() ;                                 // Purposely leave unimplemented
    VhdlElementId(const VhdlElementId &) ;            // Purposely leave unimplemented
    VhdlElementId& operator=(const VhdlElementId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLELEMENTID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy element id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // Record elements declared in two steps : first their (own) type,
    // later their 'position' in the record.
    // We don't store the record type inside the elementId
    virtual void                DeclareElementId(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual void                DeclareRecordElement(unsigned pos) ;
    virtual verific_uint64      Position() const ; // Position in its type

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsRecordElement() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2 It
    // returns 1 if the subtype_indication used in the interface
    // declaration is locally/globally static type/subtype
    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

protected:
// set at analyze time :
    unsigned _position:25 ; // position of this record element in its type.
    unsigned   _is_locally_static_type:1 ;    // Set if the type is locally static. Needed as the subtype_ind is not available
    unsigned   _is_globally_static_type:1 ;    // Set if the type is globally static. Needed as the subtype_ind is not available
} ; // class VhdlElementId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPhysicalUnitId : public VhdlIdDef
{
public:
    explicit VhdlPhysicalUnitId(char *name) ;

    // Copy tree node
    VhdlPhysicalUnitId(const VhdlPhysicalUnitId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPhysicalUnitId(SaveRestore &save_restore) ;

    virtual ~VhdlPhysicalUnitId() ;

private:
    // Prevent compiler from defining the following
    VhdlPhysicalUnitId() ;                                      // Purposely leave unimplemented
    VhdlPhysicalUnitId(const VhdlPhysicalUnitId &) ;            // Purposely leave unimplemented
    VhdlPhysicalUnitId& operator=(const VhdlPhysicalUnitId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLPHYSICALUNITID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy physical unit id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual void                DeclarePhysicalUnitId(VhdlIdDef *type, verific_uint64 pos) ;
    virtual verific_uint64      Position() const ; // Position number of this unit in its type
    // Viper #5242 : Set/Get inverse position for physical unit id :
    virtual verific_uint64      InversePosition() const ;
    virtual void                SetInversePosition(verific_uint64 ip) { _inv_position = ip ; }

    virtual unsigned            IsPhysicalUnit() const ;

protected:
// set at analyze time :
    verific_uint64 _position ; // position of this physical unit in its type.
    verific_uint64 _inv_position ; // inverse position of this physical unit in its type (for time only) Viper #5242
} ; // class VhdlPhysicalUnitId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlEntityId : public VhdlIdDef
{
public:
    explicit VhdlEntityId(char *name) ;

    // Copy tree node
    VhdlEntityId(const VhdlEntityId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlEntityId(SaveRestore &save_restore) ;

    virtual ~VhdlEntityId() ;

private:
    // Prevent compiler from defining the following
    VhdlEntityId() ;                                // Purposely leave unimplemented
    VhdlEntityId(const VhdlEntityId &) ;            // Purposely leave unimplemented
    VhdlEntityId& operator=(const VhdlEntityId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLENTITYID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy entity id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // Parse-tree port/generic add routines
    virtual void                AddPort(VhdlIdDef *id) ;
    virtual void                AddGeneric(VhdlIdDef *id) ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetPrimaryUnit(VhdlPrimaryUnit *unit) ;
    virtual VhdlPrimaryUnit *   GetPrimaryUnit() const ;
    virtual VhdlDesignUnit *    GetDesignUnit() const ;

    // pointer back to the scope that the id defines :
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual void                DeclareEntityId(Array *generics, Array *ports) ;
    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsEntity() const ;
    virtual unsigned            IsDesignUnit() const ;

    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual Array *             GetGenerics() const         { return _generics ; }
    virtual VhdlIdDef *         GetPortAt(unsigned pos) const ;      // Get port by order
    virtual VhdlIdDef *         GetPort(const char *name) const ;    // Get port by name
    virtual Array *             GetPorts() const            { return _ports ; }
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in component/entity
    virtual unsigned            NumOfPorts() const ;                 // Number of ports in component/entity

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

    // Elaboration
    virtual char *              CreateUniqueName(const VhdlBlockConfiguration *block_config) const ; // Create a name for the entity/component withj set generics

protected:
// back-pointers set during analysis (not owned) :
    Array           *_ports ;    // (elements are not owned) Array of VhdlIdDef* to the ports of this entity, ordered.
    Array           *_generics ; // (elements are not owned) Array of VhdlIdDef* to the generics of this entity, ordered.
    VhdlScope       *_scope ;    // Back-pointer to the scope that the design unit declares
    VhdlPrimaryUnit *_unit ;     // Back-pointer back to the tree that this unit declares
} ; // class VhdlEntityId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlArchitectureId : public VhdlIdDef
{
public:
    explicit VhdlArchitectureId(char *name) ;

    // Copy tree node
    VhdlArchitectureId(const VhdlArchitectureId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlArchitectureId(SaveRestore &save_restore) ;

    virtual ~VhdlArchitectureId() ;

private:
    // Prevent compiler from defining the following
    VhdlArchitectureId() ;                                      // Purposely leave unimplemented
    VhdlArchitectureId(const VhdlArchitectureId &) ;            // Purposely leave unimplemented
    VhdlArchitectureId& operator=(const VhdlArchitectureId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLARCHITECTUREID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy architecture id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetSecondaryUnit(VhdlSecondaryUnit *unit) ;
    virtual VhdlSecondaryUnit * GetSecondaryUnit() const ;
    virtual VhdlDesignUnit *    GetDesignUnit() const ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsArchitecture() const ;
    virtual unsigned            IsDesignUnit() const ;

    virtual void                DeclareArchitectureId(VhdlIdDef *entity) ;
    virtual VhdlIdDef *         GetEntity() const ; // The entity it configures

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

protected:
// back-pointers set during analysis (not owned) :
    VhdlIdDef           *_entity ; // Back-pointer to the entity of this architecture
    VhdlScope           *_scope ;  // Back-pointer to the scope that the design unit declares
    VhdlSecondaryUnit   *_unit ;   // Back-pointer back to the tree that this unit declares
} ; // class VhdlArchitectureId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConfigurationId : public VhdlIdDef
{
public:
    explicit VhdlConfigurationId(char *name) ;

    // Copy tree node
    VhdlConfigurationId(const VhdlConfigurationId &node, VhdlMapForCopy &old2new) ;

     // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConfigurationId(SaveRestore &save_restore) ;

    virtual ~VhdlConfigurationId() ;

private:
    // Prevent compiler from defining the following
    VhdlConfigurationId() ;                                       // Purposely leave unimplemented
    VhdlConfigurationId(const VhdlConfigurationId &) ;            // Purposely leave unimplemented
    VhdlConfigurationId& operator=(const VhdlConfigurationId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLCONFIGURATIONID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

     // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy configuration id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetPrimaryUnit(VhdlPrimaryUnit *unit) ;
    virtual VhdlPrimaryUnit *   GetPrimaryUnit() const ;
    virtual VhdlDesignUnit *    GetDesignUnit() const ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsConfiguration() const ;
    virtual unsigned            IsDesignUnit() const ;

    virtual void                DeclareConfigurationId(VhdlIdDef *entity) ;

    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual VhdlIdDef *         GetPortAt(unsigned pos) const ;      // Get port by order
    virtual VhdlIdDef *         GetPort(const char *name) const ;    // Get port by name
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in component/entity
    virtual unsigned            NumOfPorts() const ;                 // Number of ports in component/entity

    virtual VhdlIdDef *         GetEntity() const ;                  // The entity it configures

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

    // Elaboration
    virtual char *              CreateUniqueName(const VhdlBlockConfiguration *block_config) const ; // Create a name for the entity/component withj set generics

protected:
// back-pointers set during analysis (not owned) :
    VhdlIdDef       *_entity ; // Back-pointer to the entity of the configuration
    VhdlScope       *_scope ;  // Back-pointer to the scope that the design unit declares
    VhdlPrimaryUnit *_unit ;   // Back-pointer to the tree that this unit declares
} ; // class VhdlConfigurationId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPackageId : public VhdlIdDef
{
public:
    explicit VhdlPackageId(char *name) ;

    // Copy tree node
    VhdlPackageId(const VhdlPackageId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPackageId(SaveRestore &save_restore) ;

    virtual ~VhdlPackageId() ;

private:
    // Prevent compiler from defining the following
    VhdlPackageId() ;                                 // Purposely leave unimplemented
    VhdlPackageId(const VhdlPackageId &) ;            // Purposely leave unimplemented
    VhdlPackageId& operator=(const VhdlPackageId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLPACKAGEID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy package id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetPrimaryUnit(VhdlPrimaryUnit *unit) ;
    virtual VhdlPrimaryUnit *   GetPrimaryUnit() const ;
    virtual VhdlDesignUnit *    GetDesignUnit() const ;

    // Vhdl 2008 LRM section 4.7 - generic_clause in package header
    virtual Array *             GetGenerics() const         { return _generics ; }
    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in component/entity
    virtual void                DeclarePackageId(Array *generics) ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsPackage() const ;
    virtual unsigned            IsPackageInst() const ;
    virtual unsigned            IsDesignUnit() const ;
    virtual unsigned            IsGeneric() const ;
    virtual VhdlScope *         GetUninstantiatedPackageScope() const ;
    virtual char *              CreateUniqueName(const VhdlBlockConfiguration *block_config) const ;
    virtual VhdlIdDef *         GetElaboratedPackageId() const ;
    virtual void                SetElaboratedPackageId(VhdlIdDef *id) ;
    virtual VhdlIdDef *         TakeElaboratedPackageId() ;
    virtual void                SetActualTypes() ; // defined only for package inst
    virtual void                SetActualId(VhdlIdDef *id, Array* assoc_list) ;
    virtual VhdlIdDef *         GetActualId() const ;
    virtual VhdlIdDef *         TakeActualId() ;
    virtual void                SetDeclaration(VhdlDeclaration *d) ;
    virtual VhdlDeclaration    *GetDeclaration() const ;
    virtual VhdlIdDef *         GetUninstantiatedPackageId() const { return _uninstantiated_pkg_id ; }
    virtual void                SetUninstantiatedPackageId(VhdlIdDef *id) ;
protected:
// back-pointers set during analysis (not owned) :
    Array           *_generics ; // (elements are not owned) Array of VhdlIdDef* to the generics of this package, ordered.
    VhdlScope       *_scope ; // Back-pointer to the scope that the design unit declares
    VhdlPrimaryUnit *_unit ;  // Back-pointer to the tree that this unit declares
    VhdlDeclaration *_decl ;  // Back-pointer to interface package declaration
    VhdlIdDef       *_uninstantiated_pkg_id ; // Back-pointer to uninstantiated package id
} ; // class VhdlPackageId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPackageBodyId : public VhdlIdDef
{
public:
    explicit VhdlPackageBodyId(char *name) ;

    // Copy tree node
    VhdlPackageBodyId(const VhdlPackageBodyId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPackageBodyId(SaveRestore &save_restore) ;

    virtual ~VhdlPackageBodyId() ;

private:
    // Prevent compiler from defining the following
    VhdlPackageBodyId() ;                                     // Purposely leave unimplemented
    VhdlPackageBodyId(const VhdlPackageBodyId &) ;            // Purposely leave unimplemented
    VhdlPackageBodyId& operator=(const VhdlPackageBodyId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLPACKAGEBODYID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy package body id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetSecondaryUnit(VhdlSecondaryUnit *unit) ;
    virtual VhdlSecondaryUnit * GetSecondaryUnit() const ;
    virtual VhdlDesignUnit *    GetDesignUnit() const ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsPackageBody() const ;
    virtual unsigned            IsDesignUnit() const ;
protected:
// back-pointers set during analysis (not owned) :
    VhdlScope           *_scope ; // Back-pointer to the scope that the design unit declares
    VhdlSecondaryUnit   *_unit ;  // Back-pointer to the tree that this unit declares
} ; // class VhdlPackageBodyId

/* -------------------------------------------------------------- */

// VHDL 2008 LRM section 13.3
class VFC_DLL_PORT VhdlContextId : public VhdlIdDef
{
public:
    explicit VhdlContextId(char *name) ;

    // Copy tree node
    VhdlContextId(const VhdlContextId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlContextId(SaveRestore &save_restore) ;

    virtual ~VhdlContextId() ;

private:
    // Prevent compiler from defining the following
    VhdlContextId() ;                                 // Purposely leave unimplemented
    VhdlContextId(const VhdlContextId &) ;            // Purposely leave unimplemented
    VhdlContextId& operator=(const VhdlContextId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLCONTEXTID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy context id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetPrimaryUnit(VhdlPrimaryUnit *unit) ;
    virtual VhdlPrimaryUnit *   GetPrimaryUnit() const ;
    virtual VhdlDesignUnit *    GetDesignUnit() const ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsContext() const ;
    virtual unsigned            IsDesignUnit() const ;
protected:
// back-pointers set during analysis (not owned) :
    VhdlScope       *_scope ; // Back-pointer to the scope that the design unit declares
    VhdlPrimaryUnit *_unit ;  // Back-pointer to the tree that this unit declares
} ; // class VhdlContextId

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlLabelId : public VhdlIdDef
{
public:
    explicit VhdlLabelId(char *name) ;

    // Copy tree node
    VhdlLabelId(const VhdlLabelId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlLabelId(SaveRestore &save_restore) ;

    virtual ~VhdlLabelId() ;

private:
    // Prevent compiler from defining the following
    VhdlLabelId() ;                               // Purposely leave unimplemented
    VhdlLabelId(const VhdlLabelId &) ;            // Purposely leave unimplemented
    VhdlLabelId& operator=(const VhdlLabelId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLLABELID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy label id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsLabel() const ;
    virtual unsigned            IsComponentInstantiation() const ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetStatement(VhdlStatement *statement) ;
    virtual VhdlStatement *     GetStatement() const ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual VhdlScope *         LocalScope() const ;

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

    // Set/get primary binding pointers on instantiation labels :
    virtual void                AddBinding(VhdlBindingIndication *binding) ; // set primary binding

    // Accessor methods
    virtual VhdlBindingIndication * GetPrimaryBinding() const     { return _primary_binding ; }
    virtual void                    SetBinding(VhdlBindingIndication *binding) { _primary_binding = binding ; } // Set/reset primary binding on a label

protected:

// back-pointers set during analysis (not owned) :
    VhdlScope               *_scope ;               // Pointer to the scope that the label declares (e.g. loop statement) Set during analysis
    VhdlStatement           *_statement ;           // Pointer back to the tree that this label declares

// back-pointers used during elaboration (not owned) :
    VhdlBindingIndication   *_primary_binding ;     // Pointer used to set primary binding on this instance (set during analysis)
} ; // class VhdlLabelId

/* -------------------------------------------------------------- */

/*
   BlockId is the label of a block statement.
   It's derived from 'VhdlLabel', but has lists of ordered generics/ports extra.
   These lists are set during analysis.
   They are used to access generics/ports on the block by order number.
*/
class VFC_DLL_PORT VhdlBlockId : public VhdlLabelId
{
public:
    explicit VhdlBlockId(char *name) ;

    // Copy tree node
    VhdlBlockId(const VhdlBlockId &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBlockId(SaveRestore &save_restore) ;

    virtual ~VhdlBlockId() ;

private:
    // Prevent compiler from defining the following
    VhdlBlockId() ;                               // Purposely leave unimplemented
    VhdlBlockId(const VhdlBlockId &) ;            // Purposely leave unimplemented
    VhdlBlockId& operator=(const VhdlBlockId &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLBLOCKID; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Copy block id
    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;

    // Parse-tree port/generic add routines
    virtual void                AddPort(VhdlIdDef *id) ;
    virtual void                AddGeneric(VhdlIdDef *id) ;

    virtual unsigned            IsBlock() const ;

    virtual void                DeclareBlockId(Array *generics, Array *ports) ;

    // Access to ports/generics of the block :
    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name)  const; // Get generic by name
    virtual Array *             GetGenerics() const         { return _generics ; }
    virtual VhdlIdDef *         GetPortAt(unsigned pos) const ;      // Get port by order
    virtual VhdlIdDef *         GetPort(const char *name) const ;    // Get port by name
    virtual Array *             GetPorts() const            { return _ports ; }
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in block
    virtual unsigned            NumOfPorts() const ;                 // Number of ports in the block

protected:
// back-pointers set during analysis (not owned) :
    Array   *_generics ; // (elements are not owned) Array of VhdlIdDef* to the generics of this block, ordered.
    Array   *_ports ;    // (elements are not owned) Array of VhdlIdDef* to the ports of this block, ordered.
} ; // class VhdlBlockId

/* -------------------------------------------------------------- */
// It is like alias identifier for every object declared in uninstantiated
// package, used in interface package declaration
class VFC_DLL_PORT VhdlPackageInstElement : public VhdlIdDef
{
public:
    explicit VhdlPackageInstElement(char *name) ;

    // Copy tree node
    VhdlPackageInstElement(const VhdlPackageInstElement &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPackageInstElement(SaveRestore &save_restore) ;

    virtual ~VhdlPackageInstElement() ;

private:
    // Prevent compiler from defining the following
    VhdlPackageInstElement() ;                               // Purposely leave unimplemented
    VhdlPackageInstElement(const VhdlPackageInstElement &) ;            // Purposely leave unimplemented
    VhdlPackageInstElement& operator=(const VhdlPackageInstElement &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPACKAGEINSTELEMENT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual VhdlIdDef *         CopyIdDef(VhdlMapForCopy &old2new) const ;
    virtual VhdlScope *         LocalScope() const ;    // return scope of the target
    virtual unsigned            IsPackageInstElement() const { return 1 ; }

    // For VhdlLibraryId
    virtual unsigned            EntityClass() const ;
    virtual unsigned            IsLibrary() const ;

    // For VhdlGroupId
    virtual unsigned            IsGroup() const ;

    // For VhdlGroupTemplateId
    virtual unsigned            IsGroupTemplate() const ;

    // For VhdlAttributeId
    virtual void                DeclareAttribute(VhdlIdDef *type) ;
    virtual unsigned            IsAttribute() const ;

    // Viper #5432
    virtual void                AddAttributeSpec(VhdlAttributeSpec *attr_spec) ;
    virtual void                RemoveAttributeSpec(VhdlAttributeSpec *attr_spec) ;
    // Viper #3637/6805: Get the attribute spec that defines the attribute on the argument id
    virtual VhdlAttributeSpec * FindRelevantAttributeSpec(VhdlIdDef const *id) const ;

    // For VhdlComponentId
    // Parse-tree port/generic add routines
    virtual void                AddPort(VhdlIdDef *id) ;
    virtual void                AddGeneric(VhdlIdDef *id) ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetComponentDecl(VhdlComponentDecl *component) ;
    virtual VhdlComponentDecl * GetComponentDecl() const ;

    // pointer back to the scope that it defines
    virtual void                SetLocalScope(VhdlScope *scope) ;
    virtual void                DeclareComponentId(Array *generics, Array *ports) ;
    virtual unsigned            IsComponent() const ;

    virtual VhdlIdDef *         GetGenericAt(unsigned pos) const ;   // Get generic by order
    virtual VhdlIdDef *         GetGeneric(const char *name) const ; // Get generic by name
    virtual Array *             GetGenerics() const ;
    virtual VhdlIdDef *         GetPortAt(unsigned pos) const ;      // Get port by order
    virtual VhdlIdDef *         GetPort(const char *name) const ;    // Get port by name
    virtual Array *             GetPorts() const ;
    virtual unsigned            NumOfGenerics() const ;              // Number of generics in component/entity
    virtual unsigned            NumOfPorts() const ;                 // Number of ports in component/entity

    // Elaboration
    virtual char *              CreateUniqueName(const VhdlBlockConfiguration *block_config) const ; // Create a name for the entity/component withj set generics
    virtual VhdlIdDef *         GetElaboratedPackageId() const ;
    virtual void                SetElaboratedPackageId(VhdlIdDef *id) ;
    virtual VhdlIdDef *         TakeElaboratedPackageId() ;

    // For VhdlAliasId
    virtual void                SetTargetName(VhdlName *target_name) ; // Set back pointer _target_name in alias id.
    virtual void                SetTargetId(VhdlIdDef *id) ;

    virtual void                DeclareAlias(VhdlIdDef *type, VhdlName *target_name, unsigned locally_static_type, unsigned globally_static_type, unsigned kind) ;

    // Return the target Name to the alias. Available after declaration
    virtual VhdlName *          GetAliasTarget() const ;
    virtual VhdlIdDef *         GetTargetId() const ;
    // Overloaded type-matching routines (for type-aliases) :
    virtual unsigned            TypeMatch(const VhdlIdDef *id) const ;
    virtual unsigned            TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const ;
    virtual unsigned            IsCloselyRelatedType(const VhdlIdDef *id) const ;
    virtual unsigned            IsAlias() const ;

    // For VhdlFileId
    virtual unsigned            IsFile() const ;

    virtual void                DeclareFile(VhdlIdDef *type) ;

    // Accessor functions
    virtual FILE *              GetFile() const ;
    virtual void                SetFile(FILE *file) ;    // set file pointer. File open/close status management done externally.

    // For VhdlVariableId
    virtual unsigned            IsVariable() const ;

    virtual void                DeclareVariable(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type, unsigned is_shared) ;

    // Used/Assigned info
    virtual inline void         SetUsed()                            { if (_actual_id) _actual_id->SetUsed() ; }
    virtual inline void         SetAssigned()                        { if (_actual_id) _actual_id->SetAssigned() ; }
    virtual inline unsigned     IsUsed() const                       { return (_actual_id) ? _actual_id->IsUsed() : 0 ; }
    virtual inline unsigned     IsAssigned() const                   { return (_actual_id) ? _actual_id->IsAssigned() : 0 ; }

    virtual inline void         SetConcurrentUsed()                  { if (_actual_id) _actual_id->SetConcurrentUsed() ; }
    virtual inline void         SetConcurrentAssigned()              { if (_actual_id) _actual_id->SetConcurrentAssigned() ; }
    virtual inline unsigned     IsConcurrentUsed() const             { return (_actual_id) ? _actual_id->IsConcurrentUsed() : 0 ; }
    virtual inline unsigned     IsConcurrentAssigned() const         { return (_actual_id) ? _actual_id->IsConcurrentAssigned() : 0 ; }

    virtual inline void         SetAssignedBeforeUsed()              { if (_actual_id) _actual_id->SetAssignedBeforeUsed() ; }
    virtual inline void         SetUsedBeforeAssigned()              { if (_actual_id) _actual_id->SetUsedBeforeAssigned() ; }
    virtual inline unsigned     IsAssignedBeforeUsed() const         { return (_actual_id) ? _actual_id->IsAssignedBeforeUsed() : 0 ; }
    virtual inline void         UnSetAssignedBeforeUsed()            { if (_actual_id) _actual_id->UnSetAssignedBeforeUsed() ; }
    virtual inline void         UnSetUsedBeforeAssigned()            { if (_actual_id) _actual_id->UnSetUsedBeforeAssigned() ; }
    virtual inline unsigned     IsUsedBeforeAssigned() const         { return (_actual_id) ? _actual_id->IsUsedBeforeAssigned() : 0 ; }

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam() const             { return (_actual_id) ? _actual_id->CanBeDualPortRam() : 0 ; }
    virtual void                SetCanBeDualPortRam()                { if (_actual_id) _actual_id->SetCanBeDualPortRam() ; }
    virtual void                SetCannotBeDualPortRam()             { if (_actual_id) _actual_id->SetCannotBeDualPortRam() ; }

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam() const            { return (_actual_id) ? _actual_id->CanBeMultiPortRam()  : 0; }
    virtual void                SetCanBeMultiPortRam()               { if (_actual_id) _actual_id->SetCanBeMultiPortRam() ; }
    virtual void                SetCannotBeMultiPortRam()            { if (_actual_id) _actual_id->SetCannotBeMultiPortRam() ; }

    // Set/Check VarUsage flags w.r.t. the relevant SynthesisInfo
    virtual void                SetUsed(SynthesisInfo *info) ;
    virtual void                SetAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsUsed(SynthesisInfo *info) const ;
    virtual unsigned            IsAssigned(SynthesisInfo *info) const ;

    virtual void                SetConcurrentUsed(SynthesisInfo *info) ;
    virtual void                SetConcurrentAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsConcurrentUsed(SynthesisInfo *info) const ;
    virtual unsigned            IsConcurrentAssigned(SynthesisInfo *info) const ;

    // Usage information
    virtual void                SetAssignedBeforeUsed(SynthesisInfo *info) ;
    virtual void                SetUsedBeforeAssigned(SynthesisInfo *info) ;
    virtual void                UnSetAssignedBeforeUsed(SynthesisInfo *info) ;
    virtual void                UnSetUsedBeforeAssigned(SynthesisInfo *info) ;
    virtual unsigned            IsUsedBeforeAssigned(SynthesisInfo *info) const ;
    virtual unsigned            IsAssignedBeforeUsed(SynthesisInfo *info) const ;

    // dual-port RAM detection :
    virtual unsigned            CanBeDualPortRam(SynthesisInfo *info) const ;
    virtual void                SetCanBeDualPortRam(SynthesisInfo *info) ;
    virtual void                SetCannotBeDualPortRam(SynthesisInfo *info) ;

    // multi-port RAM detection :
    virtual unsigned            CanBeMultiPortRam(SynthesisInfo *info) const ;
    virtual void                SetCanBeMultiPortRam(SynthesisInfo *info) ;
    virtual void                SetCannotBeMultiPortRam(SynthesisInfo *info) ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2 It
    // returns 1 if the subtype_indication used in the variable
    // declaration is locally/globally static type/subtype
    virtual unsigned            IsLocallyStaticType() const ;
    virtual unsigned            IsGloballyStaticType() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual void                SetLocallyStaticType() ;
    virtual void                SetGloballyStaticType() ;

    virtual unsigned            IsSharedVariable() const ;

    // For VhdlSignalId
    virtual unsigned            IsSignal() const ;

    virtual void                DeclareSignal(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type, unsigned signal_kind) ;

    virtual VhdlIdDef *         ResolutionFunction() const ; // return resolution function of this (signal).

    virtual unsigned            GetSignalKind() const ;
    //unsigned                    DecodeSignalKind(unsigned signal_kind) const ;
    //unsigned                    EncodeSignalKind(unsigned signal_kind) const ;

    // For VhdlConstantId
    virtual void                DeclareConstant(VhdlIdDef *type, unsigned locally_static, unsigned loop_iter, unsigned globally_static, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual unsigned            IsConstant() const ;
    virtual unsigned            IsConstantId() const ;

    virtual unsigned            HasInitAssign() const ;

    // Following APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;
    virtual unsigned            GetLocallyStatic() const ;

    // For backward compatibility during database restore
    // Do not use these 2 APIs otherwise. Both fields are
    // supposed to be set only during construction through
    // DeclareConstant API. For old databases, we always set
    // Id to be locally static to "fool" the static checks.
    virtual void                SetLocallyStatic() ;
    virtual void                SetLoopIterator() ;

    // Set/Get pointer to constant declaration :
    virtual void                SetDecl(VhdlIdDef *decl) ;
    virtual VhdlIdDef *         Decl() const ;

    // Calculate the conformance signature of this identifier
    virtual unsigned long       CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ;

    // For VhdlTypeId
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ; // Set back pointers in type identifier
    virtual void                UpdateType(VhdlMapForCopy &old2new) ;

    virtual unsigned            IsType() const ;
    virtual unsigned            IsIncompleteType()  const;
    virtual unsigned            IsDefinedIncompleteType() const ;
    virtual unsigned            IsIntegerType() const ;
    virtual unsigned            IsRealType() const ;
    virtual unsigned            IsEnumerationType() const ;
    virtual unsigned            IsArrayType() const ;
    virtual unsigned            IsPhysicalType() const ;
    virtual unsigned            IsStdTimeType() const ;
    virtual unsigned            IsUnconstrainedArrayType() const ;
    virtual unsigned            IsConstrainedArrayType() const ;
    virtual unsigned            IsRecordType() const ;
    virtual unsigned            IsAccessType() const ;
    virtual unsigned            ContainsAccessType() const ;
    virtual unsigned            IsFileType() const ;
    virtual unsigned            IsScalarType() const ;
    virtual unsigned            IsNumericType() const ;
    virtual unsigned            IsDiscreteType() const ;
    virtual unsigned            IsProtectedType() const ;
    virtual unsigned            IsProtectedTypeBody() const ;
    virtual unsigned            IsStringType() const ;
    virtual unsigned            IsNaturalType() const ;
    virtual unsigned            IsPositiveType() const ;
    virtual unsigned            IsGenericType() const ;
    virtual unsigned            IsGeneric() const ;
    virtual unsigned            IsStdULogicType() const ;
    virtual unsigned            IsStdBitType() const ;
    virtual unsigned            IsStdBooleanType() const ;
    virtual unsigned            IsStdCharacterType() const ;
    virtual unsigned            IsArray() const ;
    virtual unsigned            IsRecord() const ;
    virtual unsigned            IsProtected() const ;
    virtual unsigned            IsProtectedBody() const ;

    // Type declarations
    virtual void                DeclareIntegerType() ;
    virtual void                DeclareRealType() ;
    virtual void                DeclarePhysicalType(Map *physical_units) ;
    virtual void                DeclareEnumerationType(Map *enumeration_literals) ;
    virtual void                DeclareConstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static) ;
    virtual void                DeclareUnconstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static) ;
    virtual void                DeclareRecordType(Map *element_decl_list) ;
    virtual void                DeclareFileType(VhdlIdDef *subtype_id) ;
    virtual void                DeclareAccessType(VhdlIdDef *subtype_id, int locally_static, int globally_static) ;
    virtual void                DeclareIncompleteType(VhdlIdDef *subtype_id) ; // Set the definition of the incomplete type

    virtual VhdlIdDef *         Type() const ;     // Type() of a type returns self
    virtual VhdlIdDef *         BaseType() const ; // same thing

    // Set attribute on a type :
    virtual unsigned            SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_value) ; // set attribute 'attr_id' with value expression 'attr_value' on this identifier. This is automatically called during analysis.

    // Array type related
    virtual unsigned            Dimension() const ;
    virtual VhdlIdDef *         ElementType() const ;
    virtual VhdlIdDef *         ElementType(unsigned dim) ;
    virtual VhdlIdDef *         IndexType(unsigned dim) const ;
    virtual VhdlIdDef *         AggregateElementType() const ;        // Hack for anonymous array aggregate types
    virtual unsigned            DeepDimension() const ;              // Dimension of this object including (deep)dimensions of it's element type

    // Enumeration type related
    virtual unsigned            NumOfEnums() const ;                  // Number of enumerated values in a enumeration type
    virtual VhdlIdDef *         GetEnum(const char *name) const ;     // Find a enumeration value in enum type. Fast.
    virtual VhdlIdDef *         GetEnumAt(unsigned pos) const ;       // Get enum by position ; 0 is the first
    virtual VhdlIdDef *         GetEnumWithBitEncoding(char b) const ;// Get the first enumeration literal encoded with this bit value

    // Record type related
    virtual unsigned            NumOfElements() const ;               // Number of record elements in this record type
    virtual VhdlIdDef *         GetElement(const char *name) const ;  // get a record element by name. Fast.
    virtual VhdlIdDef *         GetElementAt(unsigned pos) const ;    // get a record element by position (also Fast)

    // Physical type related
    virtual VhdlIdDef *         GetUnit(const char *name) const ;     // get a unit from physical type by name. Fast.

    // File/Access type related
    virtual VhdlIdDef *         DesignatedType() const ;

    // Declare the (predefined) operators that go with a type declaration (LRM 14.2)
    virtual void                DeclareOperators() ;

    // Viper #3898 : Added second argument for type recursion check
    virtual unsigned            IsTypeContainsFile(unsigned consider_access_type, Set& recursion_set) ;
    virtual unsigned            IsTypeContainsRecord() ; // Added for Viper #3751
    virtual unsigned            IsTypeContainsUnconstrainedArray(Set& recursion_set) ; // Added for Viper #7758

    virtual Map *               GetTypeDefList() const ;
    Map *                       GetList() const ; // Backward compatibility

    // handy predefined oper declaration routine.
    virtual void                DeclarePredefinedOp(unsigned op, const VhdlIdDef *arg1_type, const VhdlIdDef *arg2_type, VhdlIdDef *ret_type) const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // Accessor functions to access Verilog pkg/lib names
    virtual void                SetVerilogPkgName(const char *pkg_name) ;
    virtual void                SetVerilogPkgLibName(const char *pkg_lib_name) ;
    virtual unsigned            IsVerilogType() const ;
    virtual VeriIdDef *         GetVerilogTypeFromPackage() const ;

#endif

    // For VhdlProtectedTypeId
    virtual void                DeclareProtectedType(VhdlProtectedTypeDef *type_def) ;
    virtual VhdlProtectedTypeDef * GetProtectedTypeDef() const ;

    // For VhdlProtectedTypeBodyId
    virtual void                DeclareProtectedTypeBody(VhdlProtectedTypeDefBody *type_def) ;
    virtual VhdlProtectedTypeDefBody  *  GetProtectedTypeDefBody() const ;

    // For VhdlUniversalInteger
    virtual unsigned            IsUniversalInteger() const ;

    // For VhdlUniversalReal
    virtual unsigned            IsUniversalReal() const ;

    // For VhdlPslDeclId
    virtual unsigned            IsSubprogram() const ;
    virtual unsigned            IsFunction() const ;
    virtual unsigned            IsProcedure() const ;
    virtual unsigned            IsPslDeclId() const ;

    virtual void                DeclareFunction(VhdlIdDef *return_type, Array *args, Array *generics) ;

    virtual unsigned            NumOfArgs() const ;
    virtual VhdlIdDef *         Arg(unsigned idx) const ;
    virtual VhdlIdDef *         ArgType(unsigned idx) const ;
    virtual Array *             GetArgs() const ; // Get program parameters

    // Elaboration
    // function/procedure call
    virtual VhdlValue *         ElaborateSubprogram(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef *subprog_inst) ;

    // For VhdlAnonymousType
    virtual void                UpdateAnonymousType(VhdlIdDef * /*old_type_id*/, VhdlMapForCopy &/*old2new*/) ; // Viper #6146 : Update _list field and declare the type
    virtual unsigned            IsAnonymousType() const ;

    // For VhdlSubtypeId
    virtual void                DeclareSubtype(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned is_unconstrained, unsigned locally_static_type, unsigned globally_static_type) ;

    virtual unsigned            IsSubtype() const ;

    // For VhdlSubprogramId
    virtual unsigned            IsHomograph(VhdlIdDef *id) const ;
    virtual unsigned            IsSubprogramInst() const ;
    virtual void                SetAsGeneric() ;
    virtual void                SetAsProcedure() ;
    virtual void                DeclareProcedure(Array *args, Array *generics) ;

    virtual void                SetSpec(VhdlSpecification *spec) ;
    virtual VhdlSpecification * Spec() const ;
    virtual void                SetBody(VhdlSubprogramBody *body) ;
    virtual VhdlSubprogramBody *Body() const ;

    // pragma settings on subprograms
    virtual unsigned            GetPragmaFunction() const ;
    virtual unsigned            GetPragmaSigned() const ;
    virtual unsigned            GetPragmaLabelAppliesTo() const ;
    virtual void                SetPragmaFunction(unsigned function) ;
    virtual void                SetPragmaSigned() ;
    virtual void                SetPragmaLabelAppliesTo() ;

    // Set/Get the subprogram instantiation decl/interface subprogram decl
    virtual void                SetSubprogInstanceDecl(VhdlDeclaration *inst) ;
    virtual VhdlDeclaration *   GetSubprogInstanceDecl() const ;
    virtual void                SetActualId(VhdlIdDef *id, Array* assoc_list) ;
    virtual VhdlIdDef *         GetActualId() const ;
    virtual VhdlIdDef *         TakeActualId() ;

    virtual void                SetIsProcessing(unsigned s) ; // Set _is_processing flag
    virtual unsigned            IsProcessing() const ; // Return 1 if this subprogram is being processed now by application

    // Values are on every id
    virtual void                SetValue(VhdlValue *initial_value) ; // Create initial value
    virtual VhdlValue *         Value() const ;                      // Return initial value of this id. Not allocated. Its just a pointer
    virtual VhdlValue *         TakeValue() ;                        // Take (memory) control over value away from id
    virtual unsigned            HasValue() const ;

    // Constraints are on every id
    virtual VhdlConstraint *    Constraint() const ; // Return pointer to constraint of this identifier
    virtual void                SetConstraint(VhdlConstraint *constraint) ;
    virtual VhdlConstraint *    TakeConstraint() ;   // take control of constraint away from id

    // Elaboration
    // unary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;
    // binary operator
    virtual VhdlValue *         ElaborateOperator(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node) ;

    // For VhdlOperatorId
    virtual void                UpdateArgsAndType(VhdlMapForCopy &old2new) ; // Update the arguments and _type field.

    virtual unsigned            IsPredefinedOperator() const ;
    virtual unsigned            GetOperatorType() const ;
    virtual void                SetOwnsArgId() ; // VIPER #5996

    // For VhdlInterfaceId
    // Static Elaboration
    virtual void                SetHasInitAssign() ;  // Set back pointer
    virtual void                ResetHasInitAssign() ;
    // Set 'this' id as constrained
    virtual void                SetConstrained() ;
    // VIPER #4547 : Set/get routines to indicate whether valid actual (not open)
    // is specified for this port of component :
    virtual void                SetIsActualPresent() ;
    virtual unsigned            IsActualPresent() const ;

    virtual void                SetRefFormal(unsigned is_ref_formal) ;
    virtual unsigned            IsRefFormal() const ;

    virtual void                DeclareInterfaceId(VhdlIdDef *type, unsigned kind, unsigned mode, unsigned signal_kind, unsigned has_init_value, unsigned is_unconstrained, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type) ;

    virtual void                SetObjectKind(unsigned object_kind) ;
    virtual unsigned            IsInterfaceObject(unsigned object_kind) const ;
    virtual unsigned            GetObjectKind() const ;
    virtual unsigned            IsPort() const ;
    virtual unsigned            IsSubprogramFormal() const ;
    virtual unsigned            IsInput() const ;
    virtual unsigned            IsOutput() const ;
    virtual unsigned            IsInout() const ;
    virtual unsigned            IsLinkage() const ;
    virtual unsigned            IsBuffer() const ;
    virtual unsigned            Mode() const ;               // Get port direction
    virtual void                SetMode(unsigned mode) ;     // Set port mode (direction)

    virtual unsigned            GetKind() const ;

    virtual unsigned            IsUnconstrained() const ;
    virtual void                SetRefTarget(VhdlDiscreteRange * r) ;
    virtual VhdlDiscreteRange*  GetRefTarget() const ;  // Viper 6781

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                SetVerilogActualWidth(unsigned s) ;
    virtual unsigned            GetVerilogActualWidth() const ;
#endif

    // For VhdlEnumerationId
    virtual unsigned            IsEnumerationLiteral() const ;
    virtual void                DeclareEnumerationId(VhdlIdDef *type, unsigned pos) ;
    virtual verific_uint64      Position() const ; // Position in its type

    // Elaboration : encoding
    virtual void                SetBitEncoding(char b) ;    // For an enumeration literal : set as a bit(encoded) value (0,1,X or Z)
    virtual char                GetBitEncoding() const ;    // For an enumeration literal : get the bit(encoded) value (0,1,X or Z)
    // multi-bit encoding
    virtual void                SetEncoding(const char *encoding) ; // For an enumeration literal : set as a multi-bit(encoded) value (0,1,X or Z)
    virtual const char *        GetEncoding() const ;               // For an enumeration literal : get the multi-bit(encoded) value (0,1,X or Z)
    // one-hot encoding
    virtual void                SetOnehotEncoded() ;        // For an enumeration literal : flag that we encode it one-hot.
    virtual unsigned            IsOnehotEncoded() const ;   // For an enumeration literal : check if we encode it one-hot.

    // For VhdlElementId
    virtual void                DeclareElementId(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type) ;
    virtual void                DeclareRecordElement(unsigned pos) ;

    virtual unsigned            IsRecordElement() const ;

    // For VhdlPhysicalUnitId
    virtual void                DeclarePhysicalUnitId(VhdlIdDef *type, verific_uint64 pos) ;
    // Viper #5242 : Set/Get inverse position for physical unit id :
    virtual verific_uint64      InversePosition() const ;
    virtual void                SetInversePosition(verific_uint64 ip) ;
    virtual unsigned            IsPhysicalUnit() const ;

    // For VhdlEntityId
    // Parse-tree port/generic add routines
    // pointer back to the parse trees that some identifiers define :
    virtual void                SetPrimaryUnit(VhdlPrimaryUnit *unit) ;
    virtual VhdlPrimaryUnit *   GetPrimaryUnit() const ;
    virtual VhdlDesignUnit *    GetDesignUnit() const ;

    virtual void                DeclareEntityId(Array *generics, Array *ports) ;
    virtual unsigned            IsEntity() const ;
    virtual unsigned            IsDesignUnit() const ;

    // For VhdlArchitectureId
    // pointer back to the parse trees that some identifiers define :
    virtual void                SetSecondaryUnit(VhdlSecondaryUnit *unit) ;
    virtual VhdlSecondaryUnit * GetSecondaryUnit() const ;

    virtual unsigned            IsArchitecture() const ;

    virtual void                DeclareArchitectureId(VhdlIdDef *entity) ;
    virtual VhdlIdDef *         GetEntity() const ; // The entity it configures

    // For VhdlConfigurationId
    // pointer back to the parse trees that some identifiers define :

    virtual unsigned            IsConfiguration() const ;

    virtual void                DeclareConfigurationId(VhdlIdDef *entity) ;

    // For VhdlPackageId
    // Vhdl 2008 LRM section 4.7 - generic_clause in package header
    virtual VhdlIdDef *         GetUninstantiatedPackageId() const ; // Package : Uninstantiated package id for package instantiation id
    virtual void                DeclarePackageId(Array *generics) ;

    virtual unsigned            IsPackage() const ;
    virtual unsigned            IsPackageInst() const ;
    virtual VhdlScope *         GetUninstantiatedPackageScope() const ;
    virtual void                SetActualTypes() ; // defined only for package inst
    virtual void                SetDeclaration(VhdlDeclaration *d) ;
    virtual VhdlDeclaration    *GetDeclaration() const ;

    // For VhdlPackageBodyId
    // pointer back to the parse trees that some identifiers define :

    virtual unsigned            IsPackageBody() const ;

    // For VhdlContextId
    // pointer back to the parse trees that some identifiers define :

    virtual unsigned            IsContext() const ;

    // For VhdlLabelId
    virtual unsigned            IsLabel() const ;

    // pointer back to the parse trees that some identifiers define :
    virtual void                SetStatement(VhdlStatement *statement) ;
    virtual VhdlStatement *     GetStatement() const ;

    // Set/get primary binding pointers on instantiation labels :
    virtual void                AddBinding(VhdlBindingIndication *binding) ; // set primary binding

    // Accessor methods
    virtual VhdlBindingIndication * GetPrimaryBinding() const ;
    virtual void                    SetBinding(VhdlBindingIndication *binding) ;  // Set/reset primary binding on a label

    // For VhdlBlockId
    virtual unsigned            IsBlock() const ;
    virtual void                DeclareBlockId(Array *generics, Array *ports) ;

    virtual VhdlIdDef *         ActualType() const ;
    virtual void                ConnectActualId(VhdlIdDef *ai) ;
    virtual VhdlIdDef *         GetConnectedActualId() const { return _actual_id ; }
    void                        SetTargetPkgInstElement(VhdlIdDef *id) ;
    virtual void                Prune(Set* ids) ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not
protected:
// back-pointers (not owned), set at analysis time :
    VhdlIdDef *_actual_id ;  // back-pointer to the VhdlIdDef that this package element object refers to
    VhdlIdDef *_target_id ; // Target id (VhdlPackageInstElement) when this is for alias id
} ; // class VhdlPackageInstElement

/* -------------------------------------------------------------- */
#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_IDDEF_H_

