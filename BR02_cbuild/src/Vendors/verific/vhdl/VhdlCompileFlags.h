/*
 *
 * [ File Version : 1.96 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_COMPILEFLAGS_H_
#define _VERIFIC_VHDL_COMPILEFLAGS_H_

/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
 |                                                                                  |
 |    This header file contains all vhdl-related compile flags.  These flags        |
 |    can either be manually set (#defined) within this file, or they can be set    |
 |    from the Makefile/(Msdev project settings) environment.  If you want to       |
 |    control compile flag settings from the Makefile/(Msdev project settings)      |
 |    environment, then make sure all #define's are commented out below.            |
 |                                                                                  |
  \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/

/*-------------------------------------------------------------------------------------
    VERIFIC_NAMESPACE : (active in all projects)
  -------------------------------------------------------------------------------------
    This flag is controlled in the util/VerificSystem.h header file.  Please go
    there for a detailed description.
*/

/*-------------------------------------------------------------------------------------
    VHDL_PRESERVE_ID_CASE :
  -------------------------------------------------------------------------------------
    The VHDL LRM specifies that the identifier abc and ABc refer to the same object.
    However, this says nothing about the case a tool should output for object names.
    By default, Verific down-cases all identifiers from VHDL. This is what ModelTech
    and Exemplar do too.  It turns out that Synopsys and Synplicity try to preserve
    the user's case for the definitions of objects.  This makes it easier to interface
    VHDL designs with other design input like netlists or verilog.

    This switch preserves the case of declared identifiers, all through to
    the netlist objects. It mimics Synplicity and Synopsys behavior.
*/
#define VHDL_PRESERVE_ID_CASE // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_USE_STARTING_LINEFILE_INFO :
  -------------------------------------------------------------------------------------
    When printing (outputting) linefile information during Info, Warning, and Error
    messages, the linefile corresponding to the starting line of the construct will
    be used, instead of the default behavior of using the ending linefile info.
    You will only see a linefile difference from the different flag settings for
    multi-line constructs.
*/

//#define VHDL_USE_STARTING_LINEFILE_INFO // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_DUAL_LANGUAGE_ELABORATION :
  -------------------------------------------------------------------------------------
    This compile flag enables that the VHDL elaborator will probe the Verilog
    side while it is elaborating a unresolved instantiation (instantiation for
    which there is no definition). On the Verilog side, this compile switch
    allows probing the VHDL elaborator for unresolved modules. The cross-elaboration
    calls will take the parameter setting into consideration which cross-elaborating
    a design.

    This compile switch thus creates a fully transparent, fully automated cross-
    language elaborator.  Obviously, the compile switch will only operate if you
    purchased both VHDL and Verilog elaboration products.
*/

#define VHDL_DUAL_LANGUAGE_ELABORATION // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_IGNORE_SYNTHESIS_DIRECTIVES :
  -------------------------------------------------------------------------------------
    This compile switch affects RTL and static elaboration.
    Controls initialization of runtime flag "vhdl_ignore_synthesis_directives".

    The VHDL analyser annotates synthesis directives onto the parse trees.
    Such directives include the 'enum_encoding' or 'logic_type_encoding' attribute
    which are typically set on enumeration types (such as IEEE type 'std_ulogic') which
    need to be encoded into a single bit, and user-encodings set on FSM types to dictate
    a certain encoding of the FSM states.
    Synthesis directives also include pragmas on functions (mostly in IEEE standard packages) that
    tell the elaborator how to execute standard functions using these encoded types.

    Synthesis directives can cause a mismatch for applications which require simulation-exact
    elaboration behavior, including for the handling of metalogical values for such types as 'std_ulogic'.

    For such simulation-exact applications, this compile switch will let the elaborator
    ignore all synthesis-specific directives, and will resort to built-in VHDL simulation
    behavior. Please note that IF you enable this compile switch, you need to provide
    IEEE standard packages that have their functions and procedures expressed in legal
    built-in VHDL, and do not rely on any synthesis directives for their function definition.

    NOTE: per 7/2012, this flag only ignores attribute type encoding, and does not yet ignore pragmas on functions.
*/
//#define VHDL_IGNORE_SYNTHESIS_DIRECTIVES

/*-------------------------------------------------------------------------------------
    CHECK_CASE_STATEMENT_BEFORE_METALOGICAL_PRUNING :
  -------------------------------------------------------------------------------------
    This compile switch determines if the case completeness and choice
    overlapping checks are done before we prune the metalogical values
    in choice. Some versions of the Vital package, timing_b.vhd has
    incomplete case statements, if this check includes the metalogical
    values as possibilities (when this compile flag is turned on).
    Synthesis customers using such packages may consider disable this
    check by turning off the compile switch.

    We need this check affront after Viper #5698 is fixed as the case
    expression identifiers that are not initialized would now evaluate
    to constant and this check may otherwise be skipped for such cases
    if this compile flag is disabled.

    This compile flag was only visible under Static Elaborator moved it
    to Vhdl static or rtl elaborator product marker. Viper 6231
*/

#define CHECK_CASE_STATEMENT_BEFORE_METALOGICAL_PRUNING

/*-------------------------------------------------------------------------------------
    VHDL_PRUNE_LOGIC_FOR_INTEGER_CONSTRAINTS :
  -------------------------------------------------------------------------------------
    VIPER #6473: Often the widths of expressions involving variables of integer
    subtypes (where integer range bounds are away from power of two) are over-
    estimated resulting bigger operators. e.g. variable v : integer range 0 to 5 ;
    v1 : integer range 0 to 2 ; v is 3 bits and v1 is 2 bits. The expression (v + v1)
    is still 3 bits if we consider the range bounds, otherwise overestimated with
    4 bits wide.
*/

#define VHDL_PRUNE_LOGIC_FOR_INTEGER_CONSTRAINTS

/*-------------------------------------------------------------------------------------
    VHDL_PRESERVE_COMMENTS :
  -------------------------------------------------------------------------------------
    Typically, comments in HDL source code are dropped in token-parsing
    (very early in the reader). For some applications, it is important
    to preserve the comments all the way to the parse-tree or further.

    This compile switch performs comment preservation by attaching the
    comments (as char* inside a VhdlComment class node) into the parse
    tree nodes of Vhdl constructs that are nearest the comment.

    Parse tree classes VhdlDesignUnit, VhdlDeclaration, VhdlStatement
    and VhdlDiscreteRange contains a field accessible with the
    GetComments() routine of the corresponding class (returns an Array
    of VhdlComment nodes applicable for that VHDL item).

    Rules of comment attachment are rather simple :
    A comment will be attached to a Vhdl if :
      - the comment appears before the construct and
        was not attached to any previous construct, or
      - the comment during or after the construct at the same
        line as where the construct ends.

    For example :

       -- something about this entity
       entity ....
       port (a: in std_logic ; -- something about port a
       -- something about port b
       b: out std_logic) ;

    the comments will be attached to the applicable construct nodes
    created for the unit and the port-declarations.

    Note that we do NOT attach comment to identifiers (VhdlIdDef's).
    Only to the declaration construct (VhdlDeclaration) in which they appear.

    Comments attached to each construct are printed ahead of construct
    in the PrettyPrint() routines. The above example will thus be
    pretty_print-ed like this :

       -- something about this entity
       entity ....
       port (
            -- something about port a
            a: in std_logic ;
            -- something about port b
            b: out std_logic) ;

    Downside of using this compile switch is that more memory is needed
    to parse the design (extra field on the constructs and the comment
    char*'s themselves).
*/

// VIPER #7322: VHDL_PRESERVE_COMMENTS is now a run-time flag RuntimeFlags::GetVar("vhdl_preserve_comments") initialized in VhdlRuntimeFlags.h.
//#define VHDL_PRESERVE_COMMENTS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_ID_SCOPE_BACKPOINTER :
  -------------------------------------------------------------------------------------
    Verific's parse tree datastructures are designed to minimize memory usage.
    There are a number of back-pointers and flags on identifiers (VhdlIdDef) and other leaf tree nodes,
    and these are of great help to obtain most important information that applications need.
    However, when writing application software on the Verific parse trees, you might not
    always be able to readily find information that you need.

    For example, a number of customers have asked to be able to access the 'context' in which
    any identifier is declared, if you only know the identifier. The context includes the primary
    or secondary unit or block or subprogram in which an identifier is declared.
    This information is available once we are at the VhdlScope in which the identifier is declared.

    If this compile switch is set, we will add a field to every identifier, which contains a
    pointer to the scope in which the identifier is declared.
    The API routine that will be added under this compile switch is VhdlIdDef::GetOwningScope().
    The owning scope should always be set (for validly declared identifiers), except for the
    design unit identifiers. These have a scope by themselves (VhdlIdDef::LocalScope()), but are
    not contained in a scope.

    The VhdlScope API then provides a number of subroutines that identify the context (see VhdlScope.h),
    including GetContainingDesignUnit(), GetContainingPrimaryUnit() and immediate context GetOwner().

    We will maintain validity of this scope back-pointer through identifier construction, destruction and copying.
    This compile switch creates one more pointer per declared identifier. This means that it causes
  a slight increase in memory usage for parse trees.
*/

#define VHDL_ID_SCOPE_BACKPOINTER  // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_ID_SUBTYPE_BACKPOINTER :
  -------------------------------------------------------------------------------------
    Verific's parse tree datastructures are designed to minimize memory usage.
    There are a number of back-pointers and flags on identifiers (VeriIdDef) and other leaf tree nodes,
    and these are of great help to obtain most important information that applications need.
    However, when writing application software on the Verific parse trees, you might not
    always be able to readily find information that you need.

    Each (typed) VHDL identifier (VhdlIdDef) already has a back-pointer to it's "type" (a type identifier).
    For example, declaration "signal foo : bit_vector(15 downto 0) ;" will create identifier "foo",
    and API routine "VhdlIdDef::Type()" will return the back-pointer to identifier (VhdlIdDef) "bit_vector".

    AFTER static or RTL elaboration, each typed identifier will also contain its elaborated subtype indication,
    in the form of the C++ structures "VhdlConstraint", available via API routine VhdlIdDef::Constraint().
    This structure would contain the information that "foo" is and 1-dimensional array with the index
    range is an integer range 15 downto 0, and the element is a enumeration range (from type 'bit') ranging '0' to '1'.

    However, a number of customers has asked us to provide a way to access the entire subtype indication
    ("bit_vector (15 downto 0)") BEFORE elaboration has run. That information is only available as a
    VHDL tree node (a VhdlExpression) back-pointer to the declaration.

    If this compile switch is set, we will add a field to every identifier, which contains a
    pointer to the "subtype indication" of the identifier's declaration.
    This includes signals, variables, constants, all interface identifiers, subtypes, files, functions,
    record elements, attributes and aliases.
    Note that explicitly full 'type' identifiers do NOT have a subtype indication set. They have
    a 'type definition' which can be oddball structures like a record, a enumeration list etc, expressed
    as a VhdlTypeDef class. So type identifiers themselves have subtype indication fields set to 0.

    The API routine that will be added under this compile switch is VeriIdDef::GetSubtypeIndication().

    NOTE: The subtype indication is NOT elaborated yet, and is a pointer to the original parse tree as
    created during analysis of the VHDL source. If the subtype indication contains parameter references
    or function calls, then user will NOT know their values unless some form of elaboration is performed on the model.

    We will maintain validity of this scope back-pointer through identifier construction, destruction and copying.
    This compile switch creates one more pointer per declared identifier. This means that it causes
    a slight increase in memory usage for parse trees.
*/

#define VHDL_ID_SUBTYPE_BACKPOINTER  // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_UNIQUIFY_ALL_INSTANCES :
  -------------------------------------------------------------------------------------
    This compile flag enables that the VHDL static elaborator will create unique
    entity architecture pair for every component and entity instantiation.  If the
    design instantiates an entity twice with same generic settings, two  copies of
    instantiated entity will be created with same generic settings, but with
    different names and those entities will be bounded with the instantiations.

    Consider the following example :

         entity top is .... end ;
         architecture arch of top is
         begin
            I1 : entity work.child generic map (2) ..... ;
            I2 : entity work.child generic map (3) ..... ;
            I3 : entity work.child generic map (2) ..... ;
            I4 : entity work.child ....... ; -- no generic map
         end ;

    Now when this compile switch is on, new copy of entity child is created for
    every instance and after static elaboration architecture 'arch' of top will
    look like :

         architecture arch of top is
         begin
             I1 : entity work.child_uniq_0(arch);
             I2 : entity work.child_uniq_1(arch);
             I3 : entity work.child_uniq_2(arch);
             I4 : entity work.child_uniq_3(arch);
         end arch ;

    If this compile switch is off, instance I1 and I3 will share same entity  and
    architecture 'arch of top will look like :

          architecture arch of top is
          begin
              I1 : entity work.\child(2)\(arch) port map (i => '0');
              I2 : entity work.\child(3)\(arch) port map (i => '0');
              I3 : entity work.\child(2)\(arch) port map (i => '0'); -- use same entity as I1
              I4 : entity work.child_default(arch) port map (i => '0');
          end arch ;

*/

// VIPER #7352: VHDL_UNIQUIFY_ALL_INSTANCES is now a run-time flag RuntimeFlags::GetVar("vhdl_uniquify_all_instances") initialized in VhdlRuntimeFlags.h.
//#define VHDL_UNIQUIFY_ALL_INSTANCES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_COPY_TOP_BEFORE_STATIC_ELAB :
  -------------------------------------------------------------------------------------
    This compile flag enables that the VHDL static elaborator will copy the
    top level entity first and then statically elaborate the copied entity.
    Static elaborator modifies the parse trees so that all generics become
    locally constant and complex constructs like generates are replaced by
    block statements. So, copy of top level becomes essential to support the
    following features:

    1) Recursion of top level entity as recursive instantiation generally
       resides inside generate and to process that instantiation unchanged
       body of original top level entity is required.

    2) Instantiation of top level vhdl entity from verilog design with different
       sets of generic values during dual language elaboration. If vhdl top
       level is elaborated first and then during verilog elaboration same entity
       instantiation is hit, unmodified top level entity is required to process
       that instance.

    If designs containing above specified features are statically elaborated
    without enabling this flag, incorrect result will occur.
*/

#define VHDL_COPY_TOP_BEFORE_STATIC_ELAB // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
  -------------------------------------------------------------------------------------
    This compile flag enables that the VHDL RTL elaborator annotates the instance
    nets etc created for the body of the ieee package functions (like operators
    "*", "+" etc with pragma label_applies_to) with the line-file from the user
    rtl and not the line-file from the standard packages. This user source relation
    might help the applications with better source correlation and may particularly
    be of help for interactive RTL exploration tools for cross-probing original
    and synthesized designs
*/

//#define VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_PRESERVE_COMPONENT_INSTANCE :
  -------------------------------------------------------------------------------------
    This compile flag enables that the VHDL static elaborator will preserve
    component instantiations.  Normally VHDL static elaborator converts all
    component instantiations to direct entity instantiations.  So, in static
    elaboratation, designs which have only black-box or verilog module
    instantiations are represented as component instantiations. This behavior
    will remain same if this compile switch is off.

    If this compile switch in on, component instantiations (not black-box)
    will not be converted to direct entity instantiations. The instantiated
    entity will be copied with different name (if needed), generics will be
    propagated to copied entity and it will be elaborated properly. Besides,
    instantiated component will be copied (if needed) with a different name
    (same as copied entity) and its generic values will be modified accordingly.
    This copied component will be added to the declarative region of
    instantiating architecture. Current component instantiation will be
    converted to copied component instantiation, so that it can be associated
    with copied entity.

    Consider the following example :

        entity child is
            generic (p : integer ::= 4) ;
        end ;

        entity top is end ;
        architecture arch of top is
            component child is
            ........
            endcomponent
        begin
            I : child generic map (10) port map (...) ;
        end ;

        If this compile switch is off, elaborated design will be

        entity \child(10)\ is
            ........
        end ;

        entity top is end ;
        architecture arch of top is
            component child ...endcomponent ;
        begin
            I : entity work.\child(10)\(... ;
        end ;

        If this compile switch is on, elaborated design will be

        entity \child(10)\ is
            ........
        end ;

        entity top is end ;
        architecture arch of top is
            component child ...endcomponent ;
            component \child(10)\ is ....endcomponent ;
        begin
            I : \child(10)\ port map (... ;
        end ;
*/

//#define VHDL_PRESERVE_COMPONENT_INSTANCE // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_REPLACE_CONST_EXPR :
  -------------------------------------------------------------------------------------
    This compile flag enables the vhdl static elaborator to replace the constant
    expressions used as initial value of signals, variables and constants
    declared within all declarative areas except subprogram. The subtype indication
    of constant, signal and variables will be replaced with simplified subtype i.e.
    for array type bounds will be specified only with literals. This replacement
    by constant value is done only in statically elaborated entities.
    This replacement can generate problem for self-recursive design unit having
    constants with initial value defined by generics and those constants are used
    to set generic values in self-instantiation.

    For example :
    1) If design contains

      constant c : integer := 4+3 ;

    in elaborated parse tree it will be

      constant c : integer := 7 ;

    2) If signal declaration is like

       signal s : bit_vector (p +q -1 downto 0) ;

    in elaborated parse tree it will be

       signal s : bit_vector (7 downto 0) ; -- when p and q are 4
*/

#define VHDL_REPLACE_CONST_EXPR // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_USE_LIB_WIDE_SAVERESTORE :
  -------------------------------------------------------------------------------------
  Normally, binary save/restore of VHDL parse trees to disk (using physical library mapping),
  we save/restore ONE primary unit (with its secondary units) into one binary (vdb) file.
  If an entire library needs to be saved, with hundreds or thousands of design units, then
  the number of binary dumped files can become excessive. For this reason, a customer filed VIPER issue 2944.
  With this compile switch enabled, the routine vhdl_file::SaveLib(char *libname) becomes available,
  which allows all units in one library to be saved to one library-wide binary dump file (an vdbl file).

  This file is created in the normal VHDL physical library mapping search path (using standard vhdl_file
  GetLibraryPath() and GetDefaultLibraryPath() API routines).
  After creation of an library-wide dump file (and if this compile switch is set), routine
  vhdl_file::Restore() will look BOTH in the standard vdb files AND into this library-wide vdbl file
  to find and restore the requested unit from disk (vdb files have higher priority than units in a vdbl file).

  The vdbl file can only be created as a whole, at once, using vhdl_file::SaveLib(). Individual units cannot be replaced.
  However, individual unit saves (using standard vhdl_file::Save() will create a vdb file any way, which
  has higher priority during restore. So, in essence, vdbl files provide a transparent second option
  to store and retrieve VHDL design units to/from disk.
*/

#define VHDL_USE_LIB_WIDE_SAVERESTORE // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_SPECIALIZED_VISITORS (VIPER #4959)
  -------------------------------------------------------------------------------------
    Historically, the visitor class uses the same method name for all the classes.
    This kind of visitor implementation is problematic/error prone in some cases
    because of the C++ inheritance and overload behavior at once. An example:

        Class MyVisitor: public VhdlVisitor
        {
        public:
            virtual void Visit(VhdlTreeNode   &node) ;
            virtual void Visit(VhdlEntityDecl &node) ;
        } ;

        void MyVisitor::Visit(VhdlTreeNode &node)
        {
            cout << "CLASS_ID: " << node.GetClassId() << endl ;
        }

        void MyVisitor::Visit(VhdlEntityDecl &node)
        {
            Visit((VhdlPrimaryUnit &)node) ;
        }

    One would expect the visitor for a 'entity' node to traverse its architectures,
    but it will not. The call to the Visit method for the VhdlPrimaryUnit class actually
    call the Visit method of the MyVisitor class for the nearest parent class of
    the VhdlPrimaryUnit class (which is the Visit method for VhdlTreeNode in MyVisitor).

    One could, then, explicitly call the Visit method of the VhdlVisitor class:

        void MyVisitor::Visit(VhdlEntityDecl &node)
        {
            VhdlVisitor::Visit((VhdlPrimaryUnit &)node) ;
        }

    With this implementation the visitor works fine in the development step. But
    when a Visit method is added in the MyVisitor class for VhdlPrimaryUnit it is not
    called for VhdlEntityDecl nodes, because they still call the method of the
    VhdlVisitor class.

    Finally one could call the Visit method of VhdlVisitor class for the current
    node class:

        void MyVisitor::Visit(VhdlEntityDecl &node)
        {
            VhdlVisitor::Visit(node);
        }

    With this implementation, MyVisitor::Visit(VhdlPrimaryUnit &) is called if it exists
    and VhdlVisitor::Visit(VhdlPrimaryUnit &) is called if it does not. But this method
    may not be used for a class that has children nodes in case user have to do
    some processing after visiting the base class but before visiting the child
    nodes.

    Compared to this, a visitor implementation with different method names for
    each class does not have the pitfalls of the first two methods and allows
    inserting user code in the correct place of the traversal process, which
    the third method cannot.

    So, we now implemented our visitor with different method names. At the same
    time we have to be backward compatible for already written codes that our
    customers may have. Here is a short description about how to use the Visitor:

    With compile flag VHDL_SPECIALIZED_VISITORS turned ON:

        Class VhdlVisitor
        {
            ...

        public:
            virtual void VisitVeriTreeNode(VhdlTreeNode &node) ;
            virtual void VisitVeriModule(VeriModule &node) ;
            ...
        } ;

    So, the user need to override the functions VisitVeriTreeNode(VhdlTreeNode &),
    VisitVeriModule(VeriModule &) etc in his/her derived visitor class.

    And with compile flag VHDL_SPECIALIZED_VISITORS turned OFF:

        Class VhdlVisitor
        {
            ...

        public:
            virtual void Visit(VhdlTreeNode &node) ;
            virtual void Visit(VeriModule &node) ;
            ...
        } ;

    So, the user need to override the functions Visit(VhdlTreeNode &),
    Visit(VeriModule &) etc in his/her derived visitor class as before.

    We have implemented three macros (VHDL_VISIT, VHDL_VISIT_NODE, VHDL_ACCEPT) to handle
    the situation. This macros depend on the compile flag and define the proper names
    that can be used in the code. Please, check the VhdlVisitor.h/VhdlVisitor.cpp file
    for more details of these macros.  Customers are encouraged to implement visitors
    with these macros, then the code becomes independent of this compile switch.
*/

#define VHDL_SPECIALIZED_VISITORS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_FLEX_READ_FROM_STREAM (VIPER #5726)
  -------------------------------------------------------------------------------------
    Historically, flex uses FILE * to read from input files. With this compile flag
    user can make flex read from (C++) streams instead of (C) FILE *.

    Verific has defined a new abstract stream class 'verific_stream'. Flex uses object
    of this class to read from streams. Another class 'verific_istream' is defined and
    it is derived from 'verific_stream'. This class takes a C++ stream object and reads
    from it.

    So, to simply read from an istream (or derived) class, use 'verific_istream' with
    that object and flex will read from that stream.

    To read from other sources, you have to do the following:
      1) Derive your own class from 'verific_stream'
      2) Define a function that takes a const char *file_name and returns an object
         of your derived class.
      3) Register this function with vhdl_file::RegisterFlexStreamCallBack() and you
         are done. Now, flex will read from your stream.

    For an example, we implemented reading from gzipped files as an application.
    Please check example_apps/vhdl_apps/stream_apps directory for more details.
*/

//#define VHDL_FLEX_READ_FROM_STREAM // --- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_FLEX_DECRYPT_PROTECTED_BLOCKS_USING_STREAM (VIPER #6665)
  -------------------------------------------------------------------------------------
    By default Verific decrypts all the protected block string at once
    by calling Protect::decrypt() method on the user specified cipher
    object.

    But decrypting the whole protected block at once may be considered
    to be a large security flaw because it makes hacking the software
    to obtain the decrypted model too easy. It may even become visible
    in crash dumps.

    This compile flag can be used to decrypt the protected string in
    user control with specially coded streams. This stream can decrypt
    the encrypted text block by block or even character by character
    at users will. Verific will not control the decryption method.

    To use it, the decrypt routines that returns verific_stream object
    in Protect class need to be defined in user derived decryption
    object. Along with that user needs to derive a custom stream class
    from verific_stream class that should have the special read() and
    gcount() method defined properly to decrypt the string part by part.
    Object of this special stream class should be returned from the
    overridden decrypt() methods.

    Note that to use this facility, you need to define the compile flag
    VHDL_FLEX_READ_FROM_STREAM above since it makes use of streams.
*/

#ifdef VHDL_FLEX_READ_FROM_STREAM
// This flag can only be used if VHDL_FLEX_READ_FROM_STREAM compile flag is set.
//#define VHDL_FLEX_DECRYPT_PROTECTED_BLOCKS_USING_STREAM // --- Comment this line out to control flag from build environment, or uncomment to force on.
#endif

/*-------------------------------------------------------------------------------------
    VHDL_UNIQUIFY_BLACKBOXES :
  -------------------------------------------------------------------------------------
    This compile flag enables that the VHDL static elaborator will create unique
    component declaration for a specific generic/port constraint setting when
    entity-architecture pair is not defined for the component instance(i.e. black-box).

         entity top is .... end ;
         architecture arch of top is
             component comp is
                generic (p : integer := 4) ;
               .........
             end component ;
         begin
            I1 : comp generic map (2) ..... ;
            I2 : comp generic map (3) ..... ;
            I3 : comp generic map (2) ......;
         end ;

    Now when this compile switch is on and no entity/module is found for component
    'comp', two copies of component 'comp' will be created. One for generic value
    '2' and other for '3'.
    So static elaborated output will be

         architecture arch of top is
             ................
         begin
             I1 : \comp(2)\ ........ ;
             I2 : \comp(3)\ ........ ;
             I3 : \comp(2)\ .........;
         end arch ;

    If this compile switch is off, instances will share same component 'comp'.

*/

//#define VHDL_UNIQUIFY_BLACKBOXES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_PRESERVE_EXTRA_TOKENS :
  -------------------------------------------------------------------------------------
    This compile flag stores some extra tokens in the parse tree. These extra tokens
    may not be needed for normal processing or user application but may be required
    in some cases.

    As of 08/2011 we only store the closing labels for declarations, statements and
    typedefs under this flag. We may add some more later if required.
    If this flag is enabled GetClosingLabel() will return the label specified after
    the constructs for the classes VhdlDeclaration, VhdlTypeDef and VhdlStatement
    and for all the derved classes.
*/

//#define VHDL_PRESERVE_EXTRA_TOKENS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDL_DO_NOT_CREATE_LONG_UNIT_NAMES :
  -------------------------------------------------------------------------------------
    Sometimes long unit names are created during static elaboration. This is because
    we generally append name of the generic and its value in the copied/unique unit
    name.
    This compile flag makes those long names shorter. By default, we treat more than 200
    characters names to be long names.
*/

// VIPER #7853: VHDL_DO_NOT_CREATE_LONG_UNIT_NAMES is now a run-time flag RuntimeFlags::GetVar("vhdl_do_not_create_long_unit_names") initialized in VhdlRuntimeFlags.h.
//#define VHDL_DO_NOT_CREATE_LONG_UNIT_NAMES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

#endif // #ifndef _VERIFIC_VHDL_COMPILEFLAGS_H_

