/*
 *
 * [ File Version : 1.33 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_VARUSAGE_H_
#define _VERIFIC_VHDL_VARUSAGE_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VhdlIdDef;
class BitArray;
class Array;
class Map;

/* -------------------------------------------------------------------------- */

/*
   Keeps a running record of all Signals and Variables that have
   been encountered as well as their current assignment/usage
   status
*/
class VFC_DLL_PORT VhdlVarUsageInfo
{
public:
    VhdlVarUsageInfo() : _indices(0), _is_fully_assigned(0), _is_ever_assigned(0), _is_ever_used(0), _is_clocked(0), _is_in_loop(0), _is_in_condition(0) {}

    explicit VhdlVarUsageInfo(Map* id_table) : _indices(id_table), _is_fully_assigned(0), _is_ever_assigned(0), _is_ever_used(0), _is_clocked(0), _is_in_loop(0), _is_in_condition(0) {}

    ~VhdlVarUsageInfo() ;

private:
    // Prevent the compiler from implementing the following
    VhdlVarUsageInfo(const VhdlVarUsageInfo &ref);            // Purposely leave unimplemented
    VhdlVarUsageInfo& operator=(const VhdlVarUsageInfo &rhs); // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    VhdlVarUsageInfo *  Copy() ;  // creates new storage which is owned by the caller

    // Variable/signal related functions:
    unsigned            AddVariable( VhdlIdDef* id );
    void                AddAliasVariable( const VhdlIdDef* old_id, const VhdlIdDef* new_id );

    // Go through the Usage/Assignment arrays and mark each
    // var/sig as being concurrently used/assigned.  This is
    // used a a post-process for every concurrent statement
    void                UpdateConcurrentVarStatus() const;

    // combine usage/assignment info across multiple branches of
    // IF or CASE regions
    void                Combine(Array& array_of_info);

    // Clear only the FullyAssigned Info -- used in loops
    void                ClearFullyAssignedInfo() ;

    // Methods related to the usage/assignment arrays
    void                SetFullyAssigned(unsigned indx) ;
    void                SetEverAssigned(unsigned indx) ;
    void                SetEverUsed(unsigned indx) ;

    unsigned            GetFullyAssigned(unsigned indx) const ;
    unsigned            GetEverAssigned(unsigned indx) const ;
    unsigned            GetEverUsed(unsigned indx) const ;

    // VIPER 3603 : Flag to check if we are in an clocked area or not.
    unsigned            GetIsClocked() const { return _is_clocked ; }
    void                SetIsClocked() { _is_clocked = 1 ; }

    // VIPER 4008 : Need for the new flag to know if inside the loop
    unsigned            GetIsInForLoop() const { return _is_in_loop ; }
    void                SetIsInForLoop() { _is_in_loop = 1 ; }

    // VIPER 6502 : Need for the new flag to know if inside a (non-constant) condition
    unsigned            GetIsInCondition() const { return _is_in_condition ; }
    void                SetIsInCondition() { _is_in_condition = 1 ; }

private:
    Map        *_indices ;
    BitArray   *_is_fully_assigned ;
    BitArray   *_is_ever_assigned ;
    BitArray   *_is_ever_used ;
// extra bit flags
    unsigned _is_clocked:1 ; // Check if we are in a 'clocked' area.
    unsigned _is_in_loop:1 ; // Check if we are in a for loop : Viper #4008
    unsigned _is_in_condition:1 ; // Check if we are under a (case or if) condition : Viper 6502

} ; // class VhdlVarUsageInfo

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_VARUSAGE_H_

