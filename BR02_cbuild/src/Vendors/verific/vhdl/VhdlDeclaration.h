/*
 *
 * [ File Version : 1.163 - 2014/02/05 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_DECLARATION_H_
#define _VERIFIC_VHDL_DECLARATION_H_

#include "VhdlTreeNode.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Instance ;

class VhdlScope ;
class VhdlIdRef ;
class VhdlIdDef ;
class VhdlName ;
class VhdlExpression ;
class VhdlLiteral ;
class VhdlDiscreteRange ;
class VhdlSubtypeIndication ;
class VhdlSpecification ;
class VhdlFileOpenInfo ;
class VhdlSignature ;
class VhdlBindingIndication ;
class VhdlPhysicalUnitDecl ;
class VhdlDesignUnit ;
class VhdlPrimaryUnit ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Need for vhdl to verilog package conversion
class VeriScope ;
class VeriDataDecl ;
class VeriDataType ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Fillup ranges: Verilog to vhdl package conversion
class VeriConstraint ;
#endif

/* Elaboration */
class VhdlConstraint ;
class VhdlDataFlow ;
class VhdlMapForCopy ;

class VhdlVarUsageInfo ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlDeclaration : public VhdlTreeNode
{
protected: // Abstract class
    VhdlDeclaration() ;

    // Copy tree node
    VhdlDeclaration(const VhdlDeclaration &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlDeclaration(SaveRestore &save_restore, unsigned do_not_restore_members = 0);

public:
    virtual ~VhdlDeclaration() ;

private:
    // Prevent compiler from defining the following
    VhdlDeclaration(const VhdlDeclaration &) ;            // Purposely leave unimplemented
    VhdlDeclaration& operator=(const VhdlDeclaration &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    virtual void                ResolveSpecs(VhdlScope * /*scope*/) { }

    virtual unsigned            IsImplicit() const      { return 0 ; } // Defined for VhdlUseClause/VhdlLibraryClause : Was this clause implicitly declared (VIPER #1994)
    virtual unsigned            IsLibraryClause() const { return 0 ; }
    virtual unsigned            IsConfigurationSpec() const    { return 0 ; } // Returns 1 for comfiguration specification
    virtual unsigned            IsPrimaryUnit() const          { return 0 ; } // Returns 1 for package declaration in declarative region
    virtual unsigned            IsPackageBody() const          { return 0 ; } // Returns 1 for package body in declarative region
    virtual unsigned            IsSubprogramBody() const       { return 0 ; } // Returns 1 for subprogram body (VIPER #7620)
    virtual unsigned            IsProtectedTypeDefBody() const { return 0 ; } // Returns 1 for protected type body (VIPER #7620)
    // VIPER #5918 : Set/get routine for container array for nested package
    virtual Array *             GetContainerArray() const        { return 0 ; }
    virtual void                SetContainerArray(Array * /*arr*/)    { }
    virtual void                SetActualTypes() ; // Defined only for subprogram instantiation decl
    virtual Array *             GetGenericMapAspect() const      { return 0 ; } // Defined only for subprogram instantiation decl
    virtual VhdlIdDef *         GetUninstantiatedSubprogId() const { return 0 ; } // Defined for subprogram instantiation decl/interface subprogram decl
    virtual VhdlIdDef *         GetDefaultSubprogram(VhdlScope * /*assoc_list_scope*/) { return 0 ; }
    virtual void                SetActualId(VhdlIdDef * /*id*/)    { } // Defined for interface subprogram decl and interface package decl
    virtual VhdlIdDef *         GetActualId() const                { return 0 ; } // Defined for interface subprogram decl and interface package decl
    virtual VhdlIdDef *         TakeActualId() { return 0 ; }

    virtual VhdlDesignator *    GetClosingLabel() const ; // VIPER #6666
    virtual void                SetClosingLabel(VhdlDesignator *label) ; // VIPER #6666

    virtual void                CheckLabel(Map* /*all_other_map*/) { } // Viper 4738: Check whether : all or :others is present as the last label

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    virtual Array *             GetIds() const { return 0 ; } // return array of identifiers declared in this declaration.
    virtual VhdlIdDef *         GetTypeId() const  { return 0 ; } // returns _id of full type and sub type and alias type

    virtual VhdlSubtypeIndication * GetSubtypeIndication() const { return 0 ; }
    virtual VhdlExpression *        GetInitAssign() const        { return 0 ; }

    // Copy declaration
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ; // VhdlIdDef's are copied here, but copied id defs are not attached in any scope.
    // Internal routine to copy attribute specification
    virtual void                UpdateDecl(VhdlMapForCopy & /*old2new*/) {} // Set copied back pointers to entity specification

    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl data declaration to verilog data declaration
    virtual VeriDataDecl *      ConvertDataDecl(VeriScope* /*scope*/) const  { return 0 ; }
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges: Verilog to vhdl package conversion:
    virtual void                FillUpRanges(VeriScope* /*veri_scope*/, VeriConstraint* /*constraint*/, Map& /*type_id_map*/) { }
#endif

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildSelectedName(VhdlSelectedName * /*old_node*/, VhdlSelectedName * /*new_node*/)                { return 0 ; } // Delete 'old_node', puts new selected name in its place.
    virtual unsigned            ReplaceChildId(VhdlIdDef * /*old_node*/, VhdlIdDef * /*new_node*/)                                        { return 0 ; } // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication * /*old_node*/, VhdlSubtypeIndication * /*new_node*/) { return 0 ; } // Delete 'old_node', pus new subtype in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/)                            { return 0 ; } // Delete 'old_node', pus new expression in its place.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification * /*old_node*/, VhdlSpecification * /*new_node*/)                      { return 0 ; } // Delete existing specification, puts new specification in its place.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration * /*old_node*/, VhdlDeclaration * /*new_node*/)                          { return 0 ; } // Delete 'old_node', puts new declaration in its place.
    virtual unsigned            ReplaceChildStmt(VhdlStatement * /*old_node*/, VhdlStatement * /*new_node*/)                              { return 0 ; } // Delete 'old_node', puts new statement in its place.
    virtual unsigned            ReplaceChildTypeDef(VhdlTypeDef * /*old_node*/, VhdlTypeDef * /*new_node*/)                               { return 0 ; } // Delete 'old_node', puts new type in its place.
    virtual unsigned            ReplaceChildFileOpenInfo(VhdlFileOpenInfo * /*old_node*/, VhdlFileOpenInfo * /*new_node*/)                { return 0 ; } // Delete 'old_node', puts new file open information in its place.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/)                                        { return 0 ; } // Delete 'old_node', puts new target name in its place.
    virtual unsigned            ReplaceChildBindingIndication(VhdlBindingIndication * /*old_node*/, VhdlBindingIndication * /*new_node*/) { return 0 ; } // Delete 'old_node', puts new binding indication in its place.
    virtual unsigned            ReplaceChildEntityClassEntry(VhdlEntityClassEntry * /*old_node*/, VhdlEntityClassEntry * /*new_node*/)    { return 0 ; } // Delete 'old_node', puts new entity class entry in its place.
    virtual unsigned            InsertBeforeSelectedName(const VhdlSelectedName * /*target_node*/, VhdlSelectedName * /*new_node*/)       { return 0 ; } // Insert new selected name before 'target_node' specified selected name in the name list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterSelectedName(const VhdlSelectedName * /*target_node*/, VhdlSelectedName * /*new_node*/)        { return 0 ; } // Insert new selected name after 'target_node' specified selected name in the name list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertBeforeId(const VhdlIdDef * /*target_node*/, VhdlIdDef * /*new_node*/)                               { return 0 ; } // Insert new identifier before 'target_node' specified identifier in the identifier list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterId(const VhdlIdDef * /*target_node*/, VhdlIdDef * /*new_node*/)                                { return 0 ; } // Insert new identifier after 'target_node' specified identifier in the identifier list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)                 { return 0 ; } // Insert new declaration before the 'target_node' specified declaration.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)                  { return 0 ; } // Insert new declaration after the 'target_node' specified declaration.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)                     { return 0 ; } // Insert new statement before the 'target_node' specified statement.
    virtual unsigned            InsertAfterStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)                      { return 0 ; } // Insert new statement after the 'target_node' specified statement.
    virtual unsigned            InsertBeforeEntityClassEntry(const VhdlEntityClassEntry * /*target_node*/, VhdlEntityClassEntry * /*new_node*/) { return 0 ; }
    virtual unsigned            InsertAfterEntityClassEntry(const VhdlEntityClassEntry * /*target_node*/, VhdlEntityClassEntry * /*new_node*/)  { return 0 ; }

    // Static Elaboration

    // In static elaboration component instantiations are replaced by entity instantiations.
    // So after elaboration the component specifications become useless. This routine deletes
    // those component specifications
    virtual void                DeleteUnusedComSpec(VhdlArchitectureBody * /*parent_arch*/) { }
// retired 3/08  virtual void                InitializeSignals() {} ; // Viper# 3177 : Static elab to evaluate and set the initial values of signals

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) = 0 ;
    virtual VhdlValue *         ElaborateSubprogramInst(Array * /*arguments*/, VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, VhdlTreeNode * /*caller_node*/) { return 0 ; } // Defined for subprogram instantiation and interface subprogram decl
    // APIs to set/get actual package during elaboration
    virtual void                SetElaboratedPackageId(VhdlIdDef * /*actual*/) {}
    virtual VhdlIdDef *         GetElaboratedPackageId() const { return 0 ; }
    virtual VhdlIdDef *         TakeElaboratedPackageId() { return 0 ; }
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // VIPER #7343 : Get dimension from type declaration
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned /*dimension*/) const { return 0 ; }
#endif
protected:
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    VhdlDesignator *_closing_label ; // VIPER #6666
#endif
} ; // class VhdlDeclaration

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlTypeDef : public VhdlTreeNode
{
protected: // Abstract class
    VhdlTypeDef() ;

    // Copy tree node
    VhdlTypeDef(const VhdlTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlTypeDef(SaveRestore &save_restore);

public:
    virtual ~VhdlTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlTypeDef(const VhdlTypeDef &) ;            // Purposely leave unimplemented
    VhdlTypeDef& operator=(const VhdlTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    // All VHDL types are 'declared' before they become known types.
    virtual void                Declare(VhdlIdDef *id) = 0 ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    virtual VhdlIdDef *         GetId() const ; // relevant for protected typedef / typedef body
    virtual unsigned            IsProtectedTypeDefBody() const { return 0 ; }
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope* /*scope*/, Array* /*type_id_arr*/, unsigned /*processing_type*/) const       { return 0 ; }
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges: Verilog to vhdl package conversion:
    virtual void                FillUpRanges(VeriScope* /*veri_scope*/, VeriConstraint* /*constraint*/, Map& /*type_id_map*/) { }
#endif

    // Parse tree copy routines
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ; // Copy type definition
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ; // Set back pointers in type id. It is used in copy routines.

    virtual VhdlDesignator *    GetClosingLabel() const ; // VIPER #6666
    virtual void                SetClosingLabel(VhdlDesignator *label) ; // VIPER #6666

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange * /*old_node*/, VhdlDiscreteRange * /*new_node*/)             { return 0 ; } // Delete 'old_node', puts new range in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication * /*old_node*/, VhdlSubtypeIndication * /*new_node*/) { return 0 ; } // Delete 'old_node', puts new subtype indication in its place.
    virtual unsigned            ReplaceChildElementDecl(VhdlElementDecl * /*old_node*/, VhdlElementDecl * /*new_node*/)                   { return 0 ; } // Delete 'old_node', puts new element declaration in its place.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_name*/, VhdlName * /*new_name*/)                                        { return 0 ; } // Delete 'old_name', puts new name in its place.
    virtual unsigned            ReplaceChildId(VhdlIdDef * /*old_node*/, VhdlIdDef * /*new_node*/)                                        { return 0 ; } // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildPhysicalUnitDecl(VhdlPhysicalUnitDecl * /*old_node*/, VhdlPhysicalUnitDecl * /*new_node*/)    { return 0 ; } // Delete 'old_node', puts new unit declaration in its place.
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)    { return 0 ; } // Insert new range before the 'target_node' specified range in the array of ranges. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)     { return 0 ; }  // Insert new range after the 'target_node' specified range in the array of ranges. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertBeforeElementDecl(const VhdlElementDecl * /*target_node*/, VhdlElementDecl * /*new_node*/)          { return 0 ; } // Insert new element declaration before 'target_node' specified element declaration in the element list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterElementDecl(const VhdlElementDecl * /*target_node*/, VhdlElementDecl * /*new_node*/)           { return 0 ; } // Insert new element declaration after 'target_node' specified element declaration in the element list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertBeforeId(const VhdlIdDef * /*target_node*/, VhdlIdDef * /*new_node*/)                               { return 0 ; } // Insert new enumeration literal before 'target_node' specific literal in the enum literal list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterId(const VhdlIdDef * /*target_node*/, VhdlIdDef * /*new_node*/)                                { return 0 ; } // Insert new enumeration literal after 'target_node' specific literal in the enum literal list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertBeforePhysicalUnitDecl(const VhdlPhysicalUnitDecl * /*target_node*/, VhdlPhysicalUnitDecl * /*new_node*/) { return 0 ; } // Insert new unit declaration before 'target_node' specific unit declaration in the unit declaration list.
    virtual unsigned            InsertAfterPhysicalUnitDecl(const VhdlPhysicalUnitDecl * /*target_node*/, VhdlPhysicalUnitDecl * /*new_node*/)  { return 0 ; } // Insert new unit declaration after 'target_node' specific unit declaration in the unit declaration list.

    // Elaboration
    VhdlConstraint *            EvaluateConstraint(VhdlDataFlow *df, VhdlConstraint *target_constraint = 0) { return EvaluateConstraintInternal(df, target_constraint) ; }
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) = 0 ;
    virtual void                ElaboratePhysicalTypeDef(VhdlIdDef * /*id*/) ; // Added to make Set TimeResolution work post analysis phase
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned /*dimension*/) const { return 0 ; } // VIPER #7343 :
#endif
protected:
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    VhdlDesignator *_closing_label ; // VIPER #6666
#endif
} ; // class VhdlTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlScalarTypeDef : public VhdlTypeDef
{
public:
    explicit VhdlScalarTypeDef(VhdlDiscreteRange *scalar_range) ;

    // Copy tree node
    VhdlScalarTypeDef(const VhdlScalarTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlScalarTypeDef(SaveRestore &save_restore);

    virtual ~VhdlScalarTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlScalarTypeDef() ;                                     // Purposely leave unimplemented
    VhdlScalarTypeDef(const VhdlScalarTypeDef &) ;            // Purposely leave unimplemented
    VhdlScalarTypeDef& operator=(const VhdlScalarTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSCALARTYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const ;
#endif

    // Copy scalar type definition
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range in its place.

    // Accessor method
    VhdlDiscreteRange *         GetScalarRange() const   { return _scalar_range; }

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;

protected:
// Parse tree nodes (owned) :
    VhdlDiscreteRange   *_scalar_range ;
} ; // class VhdlScalarTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlArrayTypeDef : public VhdlTypeDef
{
public:
    VhdlArrayTypeDef(Array *index_constraint, VhdlSubtypeIndication *subtype_indication) ;

    // Copy tree node
    VhdlArrayTypeDef(const VhdlArrayTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlArrayTypeDef(SaveRestore &save_restore);

    virtual ~VhdlArrayTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlArrayTypeDef() ;                                    // Purposely leave unimplemented
    VhdlArrayTypeDef(const VhdlArrayTypeDef &) ;            // Purposely leave unimplemented
    VhdlArrayTypeDef& operator=(const VhdlArrayTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLARRAYTYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges: Verilog to vhdl package conversion:
    virtual void                FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map) ;
#endif

    // Copy array type definition
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ;             // Delete 'old_node', puts new range in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new subtype indication in its place.
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range before the 'target_node' specified range in the array of ranges. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ;  // Insert new range after the 'target_node' specified range in the array of ranges. If 'target_node' is null, nothing is inserted.

    // Accessor methods
    Array *                     GetIndexConstraint() const      { return _index_constraint ; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const    { return _subtype_indication ; }

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // VIPER #7343 : Get dimension from type declaration
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned dimension) const ;
#endif
protected:
// Parse tree nodes (owned) :
    Array                   *_index_constraint ; // Array of VhdlDiscreteRange*
    VhdlSubtypeIndication   *_subtype_indication ;
} ; // class VhdlArrayTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlRecordTypeDef : public VhdlTypeDef
{
public:
    VhdlRecordTypeDef(Array *element_decl_list, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlRecordTypeDef(const VhdlRecordTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlRecordTypeDef(SaveRestore &save_restore);

    virtual ~VhdlRecordTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlRecordTypeDef() ;                                     // Purposely leave unimplemented
    VhdlRecordTypeDef(const VhdlRecordTypeDef &) ;            // Purposely leave unimplemented
    VhdlRecordTypeDef& operator=(const VhdlRecordTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLRECORDTYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

    // Copy record type
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ; // VIPER 2158 : overwrite on this to set type id as _local_scope's owner. Set back pointers in type id. It is used in copy routines.

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildElementDecl(VhdlElementDecl *old_node, VhdlElementDecl *new_node) ; // Delete 'old_node', puts new element declaration in its place.
    virtual unsigned            InsertBeforeElementDecl(const VhdlElementDecl *target_node, VhdlElementDecl *new_node) ; // Insert new element declaration before 'target_node' specified element declaration in the element list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterElementDecl(const VhdlElementDecl *target_node, VhdlElementDecl *new_node) ; // Insert new element declaration after 'target_node' specified element declaration in the element list. If 'target_node' is null, nothing is inserted.

    // Accessor methods
    Array *                     GetElementDeclList() const   { return _element_decl_list ; }
    virtual VhdlScope *         LocalScope() const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges: Verilog to vhdl package conversion:
    virtual void                FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map) ;
#endif

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;

protected:
// Parse tree nodes (owned) :
    Array           *_element_decl_list ; // Array of VhdlElementDecl*
    VhdlScope       *_local_scope ;
} ; // class VhdlRecordTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlProtectedTypeDef : public VhdlTypeDef
{
public:
    VhdlProtectedTypeDef(Array *decl_list, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlProtectedTypeDef(const VhdlProtectedTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlProtectedTypeDef(SaveRestore &save_restore);

    virtual ~VhdlProtectedTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlProtectedTypeDef() ;                                     // Purposely leave unimplemented
    VhdlProtectedTypeDef(const VhdlProtectedTypeDef &) ;            // Purposely leave unimplemented
    VhdlProtectedTypeDef& operator=(const VhdlProtectedTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPROTECTEDTYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    virtual void                ClosingLabel(VhdlDesignator *id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy record type
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ;

    // Specific parse-tree manipulation routines.

    // Accessor methods
    Array *                     GetDeclList() const   { return _decl_list ; }
    virtual VhdlScope *         LocalScope() const ;
    virtual VhdlIdDef *         GetId() const ;

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint *terget_constraint) ;
    virtual void                Elaborate(VhdlDataFlow * /*df*/) ;

protected:
// Parse tree nodes (owned) :
    Array           *_decl_list ; // Array of VhdlDeclaration*
    VhdlScope       *_local_scope ;
} ; // class VhdlProtectedTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlProtectedTypeDefBody : public VhdlTypeDef
{
public:
    VhdlProtectedTypeDefBody(Array *decl_list, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlProtectedTypeDefBody(const VhdlProtectedTypeDefBody &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlProtectedTypeDefBody(SaveRestore &save_restore);

    virtual ~VhdlProtectedTypeDefBody() ;

private:
    // Prevent compiler from defining the following
    VhdlProtectedTypeDefBody() ;                                     // Purposely leave unimplemented
    VhdlProtectedTypeDefBody(const VhdlProtectedTypeDefBody &) ;            // Purposely leave unimplemented
    VhdlProtectedTypeDefBody& operator=(const VhdlProtectedTypeDefBody &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPROTECTEDTYPEDEFBODY; } // Defined in VhdlClassIds.h

    virtual unsigned            IsProtectedTypeDefBody() const { return 1 ; }

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy record type
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ;

    // Specific parse-tree manipulation routines.

    // Accessor methods
    Array *                     GetDeclList() const   { return _decl_list ; }
    virtual VhdlScope *         LocalScope() const ;
    virtual VhdlIdDef *         GetId() const ;

    virtual void                ClosingLabel(VhdlDesignator *id) ;

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint *terget_constraint) ;
    virtual void                Elaborate(VhdlDataFlow * /*df*/) ;

protected:
// Parse tree nodes (owned) :
    Array           *_decl_list ; // Array of VhdlDeclaration*
    VhdlScope       *_local_scope ;
} ; // class VhdlProtectedTypeDefBody

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAccessTypeDef : public VhdlTypeDef
{
public:
    explicit VhdlAccessTypeDef(VhdlSubtypeIndication *subtype_indication) ;

    // Copy tree node
    VhdlAccessTypeDef(const VhdlAccessTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAccessTypeDef(SaveRestore &save_restore);

    virtual ~VhdlAccessTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlAccessTypeDef() ;                                     // Purposely leave unimplemented
    VhdlAccessTypeDef(const VhdlAccessTypeDef &) ;            // Purposely leave unimplemented
    VhdlAccessTypeDef& operator=(const VhdlAccessTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLACCESSTYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy access type definition
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routine
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new subtype in its place.

    // Accessor method
    VhdlSubtypeIndication *     GetSubtypeIndication() const { return _subtype_indication ; }

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;

protected:
// Parse tree nodes (owned) :
    VhdlSubtypeIndication   *_subtype_indication ;
} ; // class VhdlAccessTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlFileTypeDef : public VhdlTypeDef
{
public:
    explicit VhdlFileTypeDef(VhdlName *file_type_mark) ;

    // Copy tree node
    VhdlFileTypeDef(const VhdlFileTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlFileTypeDef(SaveRestore &save_restore);

    virtual ~VhdlFileTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlFileTypeDef() ;                                   // Purposely leave unimplemented
    VhdlFileTypeDef(const VhdlFileTypeDef &) ;            // Purposely leave unimplemented
    VhdlFileTypeDef& operator=(const VhdlFileTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLFILETYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy file type definition
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routine.
    virtual unsigned            ReplaceChildName(VhdlName *old_name, VhdlName *new_name) ; // Delete 'old_name', puts new name in its place.

    // Accessor method
    VhdlName *                  GetFileTypeMark() const { return _file_type_mark ; }

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;

protected:
// Parse tree nodes (owned) :
    VhdlName    *_file_type_mark ;
} ; // class VhdlFileTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlEnumerationTypeDef : public VhdlTypeDef
{
public:
    explicit VhdlEnumerationTypeDef(Array *enumeration_literal_list) ;

    // Copy tree node
    VhdlEnumerationTypeDef(const VhdlEnumerationTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlEnumerationTypeDef(SaveRestore &save_restore);

    virtual ~VhdlEnumerationTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlEnumerationTypeDef() ;                                          // Purposely leave unimplemented
    VhdlEnumerationTypeDef(const VhdlEnumerationTypeDef &) ;            // Purposely leave unimplemented
    VhdlEnumerationTypeDef& operator=(const VhdlEnumerationTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLENUMERATIONTYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const ;
#endif

    // Copy enumeration type
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ; // Set back pointers in type identifier.

    // Specific parse tree manipulation routine
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new enumeration literal before 'target_node' specific literal in the enum literal list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new enumeration literal after 'target_node' specific literal in the enum literal list. If 'target_node' is null, nothing is inserted.

    // Accessor method
    Array *                     GetEnumLiteralList() const { return _enumeration_literal_list ; }

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;

protected:
// Parse tree nodes (owned) :
    Array   *_enumeration_literal_list ; // Array of VhdlEnumerationId* (VhdlIdDef*)
} ; // class VhdlEnumerationTypeDef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPhysicalTypeDef : public VhdlTypeDef
{
public:
    VhdlPhysicalTypeDef(VhdlDiscreteRange *range_constraint, Array *physical_unit_decl_list) ;

    // Copy tree node
    VhdlPhysicalTypeDef(const VhdlPhysicalTypeDef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPhysicalTypeDef(SaveRestore &save_restore);

    virtual ~VhdlPhysicalTypeDef() ;

private:
    // Prevent compiler from defining the following
    VhdlPhysicalTypeDef() ;                                       // Purposely leave unimplemented
    VhdlPhysicalTypeDef(const VhdlPhysicalTypeDef &) ;            // Purposely leave unimplemented
    VhdlPhysicalTypeDef& operator=(const VhdlPhysicalTypeDef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPHYSICALTYPEDEF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Declare(VhdlIdDef *type_id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy physical type definition
    virtual VhdlTypeDef *       CopyTypeDef(VhdlMapForCopy &old2new) const ;
    virtual void                DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new) ; // Set back pointers in type id. This routine is used to copy type declaration.

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ;          // Delete 'old_node', puts new range in its place.
    virtual unsigned            ReplaceChildPhysicalUnitDecl(VhdlPhysicalUnitDecl *old_node, VhdlPhysicalUnitDecl *new_node) ; // Delete 'old_node', puts new unit declaration in its place.
    virtual unsigned            InsertBeforePhysicalUnitDecl(const VhdlPhysicalUnitDecl *target_node, VhdlPhysicalUnitDecl *new_node) ; // Insert new unit declaration before 'target_node' specific unit declaration in the unit declaration list.
    virtual unsigned            InsertAfterPhysicalUnitDecl(const VhdlPhysicalUnitDecl *target_node, VhdlPhysicalUnitDecl *new_node) ; // Insert new unit declaration after 'target_node' specific unit declaration in the unit declaration list.

    // Accessor methods
    VhdlDiscreteRange *         GetRangeConstraint() const          { return _range_constraint ; }
    Array *                     GetPhysicalUnitDeclList() const     { return _physical_unit_decl_list ; }

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;
    virtual void                ElaboratePhysicalTypeDef(VhdlIdDef *id) ; // Added to make SetTimeResolution work post analysis phase. This API sets the Position on the time EnumId appropriately

protected:
// Parse tree nodes (owned) :
    VhdlDiscreteRange   *_range_constraint ;
    Array               *_physical_unit_decl_list ; // Array of VhdlPhysicalUnitDecl*
} ; // class VhdlPhysicalTypeDef

/* -------------------------------------------------------------- */

/*********************** Special classes ***********************/

class VFC_DLL_PORT VhdlElementDecl : public VhdlTreeNode
{
public:
    VhdlElementDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication) ;

    // Copy tree node
    VhdlElementDecl(const VhdlElementDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlElementDecl(SaveRestore &save_restore);

    virtual ~VhdlElementDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlElementDecl() ;                                   // Purposely leave unimplemented
    VhdlElementDecl(const VhdlElementDecl &) ;            // Purposely leave unimplemented
    VhdlElementDecl& operator=(const VhdlElementDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLELEMENTDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges: Verilog to vhdl package conversion:
    virtual void                FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map) ;
#endif

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    unsigned                    NumUnconstrainedRanges() const ;

    // Copy record element declaration
    VhdlElementDecl *           Copy(VhdlMapForCopy &old2new) const ; // Copied element ids are not declared in any scope if this routine is called separately.

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new subtype indication in its place.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new element identifier before 'target_node' specified element identifier in the identifier list.
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new element identifier after 'target_node' specified element identifier in the identifier list.

    // Accessor methods
    virtual Array *             GetIds() const                  { return _id_list; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const    { return _subtype_indication ; }

    // Elaboration
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *terget_constraint) ;

protected:
// Parse tree nodes (owned) :
    Array                   *_id_list ; // Array of VhdlElementId*   (VhdlIdDef*)
    VhdlSubtypeIndication   *_subtype_indication ;
} ; // class VhdlElementDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPhysicalUnitDecl : public VhdlTreeNode
{
public:
    VhdlPhysicalUnitDecl(VhdlIdDef *id, VhdlExpression *physical_literal) ;

    // Copy tree node
    VhdlPhysicalUnitDecl(const VhdlPhysicalUnitDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPhysicalUnitDecl(SaveRestore &save_restore);

    virtual ~VhdlPhysicalUnitDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlPhysicalUnitDecl() ;                                        // Purposely leave unimplemented
    VhdlPhysicalUnitDecl(const VhdlPhysicalUnitDecl &) ;            // Purposely leave unimplemented
    VhdlPhysicalUnitDecl& operator=(const VhdlPhysicalUnitDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPHYSICALUNITDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    verific_uint64              Infer(VhdlIdDef *type) const ; // Test type of id against type of its physical literal. Return position number

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    void                        SetInversePosition(const VhdlIdDef *type) const ;

    // Copy physical unit declaration.
    VhdlPhysicalUnitDecl *      Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ;             // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new unit expression in its place.

    // Accessor methods
    VhdlIdDef *                 GetId() const                   { return _id; } // Get the declared unit identifier
    VhdlExpression *            GetPhysicalLiteral() const      { return _physical_literal ; }

protected:
// Parse tree nodes (owned) :
    VhdlIdDef           *_id ;
    VhdlExpression      *_physical_literal ;
} ; // class VhdlPhysicalUnitDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlUseClause : public VhdlDeclaration
{
public:
    VhdlUseClause(Array *selected_name_list, unsigned is_implicit) ;

    // Copy tree node
    VhdlUseClause(const VhdlUseClause &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlUseClause(SaveRestore &save_restore);

    virtual ~VhdlUseClause() ;

private:
    // Prevent compiler from defining the following
    VhdlUseClause() ;                                 // Purposely leave unimplemented
    VhdlUseClause(const VhdlUseClause &) ;            // Purposely leave unimplemented
    VhdlUseClause& operator=(const VhdlUseClause &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLUSECLAUSE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsImplicit() const { return _is_implicit ; } // Was this use clause implicitly declared

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy use clause
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildSelectedName(VhdlSelectedName *old_node, VhdlSelectedName *new_node) ; // Delete 'old_node', puts new selected name in its place.
    virtual unsigned            InsertBeforeSelectedName(const VhdlSelectedName *target_node, VhdlSelectedName *new_node) ; // Insert new selected name before 'target_node' specified selected name in the name list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterSelectedName(const VhdlSelectedName *target_node, VhdlSelectedName *new_node) ; // Insert new selected name after 'target_node' specified selected name in the name list. If 'target_node' is null, nothing is inserted.

    // Accessor method
    Array *                     GetSelectedNameList() const  { return _selected_name_list ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ; // do nothing

protected:
// Parse tree nodes (owned) :
    Array       *_selected_name_list ; // Array of VhdlSelectedName*
    unsigned     _is_implicit:1 ;      // Was this library clause implicitly declared (VIPER #1994)
} ; // class VhdlUseClause

/* -------------------------------------------------------------- */

// Vhdl 2008 LRM section 13.4
class VFC_DLL_PORT VhdlContextReference : public VhdlDeclaration
{
public:
    explicit VhdlContextReference(Array *selected_name_list) ;

    // Copy tree node
    VhdlContextReference(const VhdlContextReference &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlContextReference(SaveRestore &save_restore);

    virtual ~VhdlContextReference() ;

private:
    // Prevent compiler from defining the following
    VhdlContextReference() ;                                 // Purposely leave unimplemented
    VhdlContextReference(const VhdlContextReference &) ;            // Purposely leave unimplemented
    VhdlContextReference& operator=(const VhdlContextReference &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONTEXTREFERENCE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy use clause
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Accessor method
    Array *                     GetSelectedNameList() const  { return _selected_name_list ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ; // do nothing

protected:
// Parse tree nodes (owned) :
    Array       *_selected_name_list ; // Array of VhdlSelectedName*
    // VIPER #6933: Set of VhdlLibraryIds (owned by this context reference)
    // These ids will be declared in unit where this context reference is used
    Set         *_lib_ids ;
} ; // class VhdlContextReference

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlLibraryClause : public VhdlDeclaration
{
public:
    VhdlLibraryClause(Array *id_list, unsigned is_implicit) ;

    // Copy tree node
    VhdlLibraryClause(const VhdlLibraryClause &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlLibraryClause(SaveRestore &save_restore);

    virtual ~VhdlLibraryClause() ;

private:
    // Prevent compiler from defining the following
    VhdlLibraryClause() ;                                     // Purposely leave unimplemented
    VhdlLibraryClause(const VhdlLibraryClause &) ;            // Purposely leave unimplemented
    VhdlLibraryClause& operator=(const VhdlLibraryClause &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLLIBRARYCLAUSE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsImplicit() const { return _is_implicit ; } // Was this library clause implicitly declared
    virtual unsigned            IsLibraryClause() const { return 1 ; }

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy library clause
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new identifier before 'target_node' specified identifier in the identifier list. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new identifier after 'target_node' specified identifier in the identifier list. If 'target_node' is null, nothing is inserted.

    // Accessor method
    virtual Array *             GetIds() const    { return _id_list ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    Array       *_id_list ;        // Array of VhdlLibraryId*   (VhdlIdDef*)
    unsigned     _is_implicit:1 ;  // Was this library clause implicitly declared (VIPER #1994)
} ; // class VhdlLibraryClause

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlInterfaceDecl : public VhdlDeclaration
{
public:
    VhdlInterfaceDecl(unsigned interface_kind, Array *id_list, unsigned mode, VhdlSubtypeIndication *subtype_indication, unsigned signal_kind, VhdlExpression *init_assign) ;

    // Copy tree node
    VhdlInterfaceDecl(const VhdlInterfaceDecl &node, VhdlMapForCopy &old2new) ;

protected:
    // This constructor is used only for static elaboration. This constructor does not check
    // anything. It should not be used for general purpose.
    VhdlInterfaceDecl(unsigned interface_kind, const VhdlIdDef *id, unsigned mode, VhdlSubtypeIndication *subtype_indication, unsigned signal_kind, VhdlExpression *init_assign) ;

public:

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInterfaceDecl(SaveRestore &save_restore);

    virtual ~VhdlInterfaceDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlInterfaceDecl() ;                                     // Purposely leave unimplemented
    VhdlInterfaceDecl(const VhdlInterfaceDecl &) ;            // Purposely leave unimplemented
    VhdlInterfaceDecl& operator=(const VhdlInterfaceDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLINTERFACEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy interface declaration.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', pus new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', pus new subtype in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', pus new expression in its place.

    // If 'target_node' is null, new node is not inserted
    // in the list.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new identifier before 'target_node' specified identifier.
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ; // Insert new identifier after 'target_node' specified identifier.

    // Static Elaboration

    // Convert declaration with array-of-ids to
    // declarations with single-identifier.
    virtual void                Split(Array *new_decl_list) ;

    // Create range constraint if the range constraint
    // is not specified in this declaration.
    virtual void                CreateRangeConstraint() ;

    // Set/Override initial expression of 'this'
    // declaration
    void                        SetInitAssign(VhdlExpression *expr) ; // Populate '_has_init_assign' field in interface-id.

    // Static elaborator converts component instantiation to direct
    // entity instantiation. This routine consider the initial values
    // of input ports to create new association elements for the
    // terminal list of created entity instantiation.
    virtual void                DefaultValues(const Set *id_considered, Array *port_map, const VhdlIdDef *instantiated_unit, Set *en_id_considered, linefile_type linefile) const ;

    // Accessor methods
    unsigned                    GetInterfaceKind() const            { return _interface_kind ; }
    virtual VhdlIdDef *         GetId() const ; // For interface type/subprogram/package
    virtual Array *             GetIds() const                      { return _id_list; }
    unsigned                    GetMode() const                     { return _mode ; }
    unsigned                    IsInput() const ;
    unsigned                    IsOutput() const ;
    unsigned                    IsInout() const ;
    unsigned                    IsLinkage() const ;
    unsigned                    IsBuffer() const ;
    VhdlSubtypeIndication *     GetSubtypeIndication() const        { return _subtype_indication ; }
// CARBON_BEGIN
    void                        SetSubtypeIndication(VhdlSubtypeIndication* si)              {_subtype_indication = si ; }
// CARBON_END
    unsigned                    GetSignalKind() const               { return _signal_kind ; }
    VhdlExpression *            GetInitAssign() const               { return _init_assign ; }
    virtual unsigned            IsInterfaceTypeDecl() const         { return 0 ; }
    virtual unsigned            IsInterfaceSubprogDecl() const      { return 0 ; }
    virtual unsigned            IsInterfacePackageDecl() const      { return 0 ; }

    // Modifier methods
    void                        SetMode(unsigned mode) ;  // Change the Mode of this IO declaration and all of its ids to 'mode'
    virtual VhdlIdDef *         GetDefaultSubprogram(VhdlScope * /*assoc_list_scope*/) { return 0 ; }

    // Elaboration

    // Before association with actuals :
    virtual void                Initialize() ;                // Set constraints and re-set values to 0.
    virtual void                InitializeConstraints(Map &formal_to_constraint) ; // Create constraints and insert in the Map (id->constraint)

    // After association with actuals :
    // VIPER #7543 : Added argument assoc_list_scope to extract default subprogram in instantiating unit scope
    virtual void                InitialValues(Instance *inst, unsigned has_recursion, VhdlDataFlow *df, VhdlScope *assoc_list_scope) ; // Propagate recursion info so that stack is not used for function

    virtual void                Elaborate(VhdlDataFlow *df) ; // Should be obsolete

protected:
// Parse tree nodes (owned) :
    unsigned               _interface_kind ;       // VHDL_constant, VHDL_signal, VHDL_variable, VHDL_file, 0
    Array                 *_id_list ;              // Array of VhdlInterfaceId*   (VhdlIdDef*)
    unsigned               _mode ;                 // VHDL_in, VHDL_out, VHDL_inout, VHDL_buffer, VHDL_linkage, 0
    VhdlSubtypeIndication *_subtype_indication ;
    unsigned               _signal_kind ;          // VHDL_bus, VHDL_register, 0
    VhdlExpression        *_init_assign ;          // 0/1 flag.
} ; // class VhdlInterfaceDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSubprogramDecl : public VhdlDeclaration
{
public:
    explicit VhdlSubprogramDecl(VhdlSpecification *subprogram_spec) ;

    // Copy tree node
    VhdlSubprogramDecl(const VhdlSubprogramDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSubprogramDecl(SaveRestore &save_restore);

    virtual ~VhdlSubprogramDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlSubprogramDecl() ;                                      // Purposely leave unimplemented
    VhdlSubprogramDecl(const VhdlSubprogramDecl &) ;            // Purposely leave unimplemented
    VhdlSubprogramDecl& operator=(const VhdlSubprogramDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSUBPROGRAMDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual VhdlScope *         LocalScope() const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy subprogram declaration.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routine.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node) ; // Delete existing specification, puts new specification in its place.

    // Accessor method
    VhdlSpecification *         GetSubprogramSpec() const    { return _subprogram_spec ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlSpecification   *_subprogram_spec ;
} ; // class VhdlSubprogramDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSubprogramBody : public VhdlDeclaration
{
public:
    VhdlSubprogramBody(VhdlSpecification *subprogram_spec, Array *decl_part, Array *statement_part) ;

    // Copy tree node
    VhdlSubprogramBody(const VhdlSubprogramBody &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSubprogramBody(SaveRestore &save_restore);

    virtual ~VhdlSubprogramBody() ;

private:
    // Prevent compiler from defining the following
    VhdlSubprogramBody() ;                                      // Purposely leave unimplemented
    VhdlSubprogramBody(const VhdlSubprogramBody &) ;            // Purposely leave unimplemented
    VhdlSubprogramBody& operator=(const VhdlSubprogramBody &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSUBPROGRAMBODY; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    void                        VarUsageStartingPoint() const ;
    virtual void                VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/) ;

    virtual VhdlScope *         LocalScope() const ;

    virtual void                ClosingLabel(VhdlDesignator *id) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    virtual unsigned            IsSubprogramBody() const { return 1 ; } // Returns 1 for subprogram body (VIPER #7620)
    // Copy subprogram body
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node) ; // Delete 'old_node', puts new specification in its place.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Delete 'old_node', puts new declaration in its place.
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ; // Delete 'old_node', puts new statement in its place.

    // If 'target_node' is null, nothing is inserted in corresponding list.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before the 'target_node' specified declaration.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration after the 'target_node' specified declaration.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement before the 'target_node' specified statement.
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement after the 'target_node' specified statement.

    // Accessor methods
    VhdlSpecification *         GetSubprogramSpec() const       { return _subprogram_spec ; }
    Array *                     GetDeclPart() const             { return _decl_part ; }
    Array *                     GetStatementPart() const        { return _statement_part ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;
    VhdlValue *                 ElaborateSubprogram(const Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef *subprog_inst) ;

protected:
// Parse tree nodes (owned) :
    VhdlSpecification   *_subprogram_spec ;
    Array               *_decl_part ;      // Array of VhdlDeclaration*
    Array               *_statement_part ; // Array of VhdlStatement*
} ; // class VhdlSubprogramBody

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSubtypeDecl : public VhdlDeclaration
{
public:
    VhdlSubtypeDecl(VhdlIdDef *id, VhdlSubtypeIndication *subtype_indication) ;

    VhdlSubtypeDecl(const VhdlSubtypeDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSubtypeDecl(SaveRestore &save_restore);

    virtual ~VhdlSubtypeDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlSubtypeDecl() ;                                   // Purposely leave unimplemented
    VhdlSubtypeDecl(const VhdlSubtypeDecl &) ;            // Purposely leave unimplemented
    VhdlSubtypeDecl& operator=(const VhdlSubtypeDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSUBTYPEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                VarUsage(VhdlVarUsageInfo *info, unsigned /*action*/) ;

    // Copy subtype declaration. Copied type id is not declared in any scope
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new type  in its place.

    // Accessor methods
    virtual VhdlIdDef *         GetTypeId() const                   { return _id ; }
    VhdlIdDef *                 GetId() const                       { return _id ; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const        { return _subtype_indication ; }

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl data declaration to verilog data declaration
    virtual VeriDataDecl *      ConvertDataDecl(VeriScope *scope) const ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges: Verilog to vhdl package conversion:
    virtual void                FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map) ;
#endif

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef               *_id ;
    VhdlSubtypeIndication   *_subtype_indication ;
} ; // class VhdlSubtypeDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlFullTypeDecl : public VhdlDeclaration
{
public:
    VhdlFullTypeDecl(VhdlIdDef *id, VhdlTypeDef *type_def) ;

    // Copy tree node
    VhdlFullTypeDecl(const VhdlFullTypeDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary save via SaveRestore class)
    explicit VhdlFullTypeDecl(SaveRestore &save_restore);

    virtual ~VhdlFullTypeDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlFullTypeDecl() ;                                    // Purposely leave unimplemented
    VhdlFullTypeDecl(const VhdlFullTypeDecl &) ;            // Purposely leave unimplemented
    VhdlFullTypeDecl& operator=(const VhdlFullTypeDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLFULLTYPEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsProtectedTypeDefBody() const ; // Returns 1 for protected type body (VIPER #7620)

    // Copy type declaration. Copied type id is not declared in any scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl data declaration to verilog data declaration
    virtual VeriDataDecl *      ConvertDataDecl(VeriScope *scope) const ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges: Verilog to vhdl package conversion:
    virtual void                FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map) ;
#endif

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new type id in its place.
    virtual unsigned            ReplaceChildTypeDef(VhdlTypeDef *old_node, VhdlTypeDef *new_node) ; // Delete 'old_node', puts new type in its place.

    // Accessor methods
    virtual VhdlIdDef *         GetTypeId() const           { return _id ; }
    VhdlIdDef *                 GetId() const               { return _id ; }
    VhdlTypeDef *               GetTypeDef() const          { return _type_def ; }

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809: Check whether element is unconstrained or not

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // VIPER #7343 : Get dimension from type declaration
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned dimension) const ;
#endif
protected:
// Parse tree nodes (owned) :
    VhdlIdDef       *_id ;
    VhdlTypeDef     *_type_def ;
} ; // class VhdlFullTypeDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlIncompleteTypeDecl : public VhdlDeclaration
{
public:
    explicit VhdlIncompleteTypeDecl(VhdlIdDef *id) ;

    // Copy tree node
    VhdlIncompleteTypeDecl(const VhdlIncompleteTypeDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlIncompleteTypeDecl(SaveRestore &save_restore);

    virtual ~VhdlIncompleteTypeDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlIncompleteTypeDecl() ;                                          // Purposely leave unimplemented
    VhdlIncompleteTypeDecl(const VhdlIncompleteTypeDecl &) ;            // Purposely leave unimplemented
    VhdlIncompleteTypeDecl& operator=(const VhdlIncompleteTypeDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLINCOMPLETETYPEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy forward declaration of type id. Copied type id is not declared in any scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;
    virtual void                UpdateDecl(VhdlMapForCopy &old2new) ;

    // Manipulate parse tree.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete existing type id, puts new type id in its place.

    // Accessor method
    VhdlIdDef *                 GetId() const    { return _id ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef   *_id ;
} ; // class VhdlIncompleteTypeDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConstantDecl : public VhdlDeclaration
{
public:
    VhdlConstantDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign) ;

    // Copy tree node
    VhdlConstantDecl(const VhdlConstantDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConstantDecl(SaveRestore &save_restore);

    virtual ~VhdlConstantDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlConstantDecl() ;                                    // Purposely leave unimplemented
    VhdlConstantDecl(const VhdlConstantDecl &) ;            // Purposely leave unimplemented
    VhdlConstantDecl& operator=(const VhdlConstantDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONSTANTDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    virtual unsigned            HasInitAssign() const { return (_init_assign) ? 1 : 0 ; }

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy constant declaration.
    // Constant ids are copied here, but they are not attached to
    // any scope if this routine is called separately.
    // When any scoped object is copied, any identifier declared
    // in that scope is attached to the copied scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new type in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete old initial expression, puts new expression in its place.

    // Insert new identifier before/after any specific constant identifier in the identifier list.
    // If 'target_node' is null, new identifier is not attached.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;

    // Accessor methods
    virtual Array *             GetIds() const                      { return _id_list ; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const        { return _subtype_indication ; }
    VhdlExpression *            GetInitAssign() const               { return _init_assign ; }
    VhdlIdDef *                 GetType() const                     { return _type ; }

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl data declaration to verilog data declaration
    virtual VeriDataDecl *      ConvertDataDecl(VeriScope *scope) const ;
#endif

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    Array                   *_id_list ; // Array of VhdlConstantId*   (VhdlIdDef*)
    VhdlSubtypeIndication   *_subtype_indication ;
    VhdlExpression          *_init_assign ;
// Field set at analysis time :
    VhdlIdDef               *_type ;    // resolved type of the constant decl (can be used during elaboration if needed).
} ; // class VhdlConstantDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSignalDecl : public VhdlDeclaration
{
public:
    VhdlSignalDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication, unsigned signal_kind, VhdlExpression *init_assign) ;

    // Copy tree node
    VhdlSignalDecl(const VhdlSignalDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSignalDecl(SaveRestore &save_restore);

    virtual ~VhdlSignalDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlSignalDecl() ;                                  // Purposely leave unimplemented
    VhdlSignalDecl(const VhdlSignalDecl &) ;            // Purposely leave unimplemented
    VhdlSignalDecl& operator=(const VhdlSignalDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSIGNALDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy signal declaration.
    // Signal ids are copied here, but they are not attached to
    // any scope if this routine is called separately.
    // When any scoped object is copied, any identifier declared
    // in that scope is attached to the copied scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new type information in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new initial expression in its place.

    // Insert new identifier before/after any specific signal identifier in the identifier list.
    // If 'target_node' is null, new identifier is not attached.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;
// retired 3/08   virtual void                InitializeSignals() ; // Viper# 3177 : Static elab to evaluate and set the initial values of signals

    // Accessor methods
    virtual Array *             GetIds() const                          { return _id_list ; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const            { return _subtype_indication ; }
    unsigned                    GetSignalKind() const                   { return _signal_kind ; }
    VhdlExpression *            GetInitAssign() const                   { return _init_assign ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    Array                   *_id_list ;      // Array of VhdlSignalId*    (VhdlIdDef*)
    VhdlSubtypeIndication   *_subtype_indication ;
    unsigned                 _signal_kind ;  // VHDL_bus, VHDL_register, 0
    VhdlExpression          *_init_assign ;
} ; // class VhdlSignalDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlVariableDecl : public VhdlDeclaration
{
public:
    VhdlVariableDecl(unsigned shared, Array *id_list, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign) ;

    // Copy tree node
    VhdlVariableDecl(const VhdlVariableDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlVariableDecl(SaveRestore &save_restore);

    virtual ~VhdlVariableDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlVariableDecl() ;                                    // Purposely leave unimplemented
    VhdlVariableDecl(const VhdlVariableDecl &) ;            // Purposely leave unimplemented
    VhdlVariableDecl& operator=(const VhdlVariableDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLVARIABLEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy variable declaration.
    // variable ids are copied here, but they are not attached to
    // any scope if this routine is called separately.
    // When any scoped object is copied, any identifier declared
    // in that scope is attached to the copied scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new type information in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new initial expression in its place.

    // Insert new identifier before/after any specific identifier in the identifier list.
    // If 'target_node' is null, new identifier is not attached.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;

    // Accessor methods
    unsigned                    IsShared() const                        { return _shared ; }
    virtual Array *             GetIds() const                          { return _id_list ; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const            { return _subtype_indication ; }
    VhdlExpression *            GetInitAssign() const                   { return _init_assign ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    unsigned               _shared ;    // VHDL_shared, 0
    Array                 *_id_list ;   // Array of VhdlVariableId*    (VhdlIdDef*)
    VhdlSubtypeIndication *_subtype_indication ;
    VhdlExpression        *_init_assign ;
} ; // class VhdlVariableDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlFileDecl : public VhdlDeclaration
{
public:
    VhdlFileDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication, VhdlFileOpenInfo *file_open_info) ;

    // Copy tree node
    VhdlFileDecl(const VhdlFileDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlFileDecl(SaveRestore &save_restore);

    virtual ~VhdlFileDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlFileDecl() ;                                // Purposely leave unimplemented
    VhdlFileDecl(const VhdlFileDecl &) ;            // Purposely leave unimplemented
    VhdlFileDecl& operator=(const VhdlFileDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLFILEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy file declaration.
    // declared ids are copied here, but they are not attached to
    // any scope if this routine is called separately.
    // When any scoped object is copied, any identifier declared
    // in that scope is attached to the copied scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new type information in its place.
    virtual unsigned            ReplaceChildFileOpenInfo(VhdlFileOpenInfo *old_node, VhdlFileOpenInfo *new_node) ; // Delete 'old_node', puts new file open information in its place.

    // Insert new identifier before/after any specific identifier in the identifier list.
    // If 'target_node' is null, new identifier is not attached.
    virtual unsigned            InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;
    virtual unsigned            InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node) ;

    // Accessor methods
    virtual Array *             GetIds() const                      { return _id_list ; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const        { return _subtype_indication ; }
    VhdlFileOpenInfo *          GetFileOpenInfo() const             { return _file_open_info ; }

    // ELaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    Array                   *_id_list ;  // Array of  VhdlFileId*    (VhdlIdDef*)
    VhdlSubtypeIndication   *_subtype_indication ;
    VhdlFileOpenInfo        *_file_open_info ;
} ; // class VhdlFileDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAliasDecl : public VhdlDeclaration
{
public:
    VhdlAliasDecl(VhdlIdDef *designator, VhdlSubtypeIndication *subtype_indication, VhdlName *alias_target_name) ;

    // Copy tree node
    VhdlAliasDecl(const VhdlAliasDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAliasDecl(SaveRestore &save_restore);

    virtual ~VhdlAliasDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlAliasDecl() ;                                 // Purposely leave unimplemented
    VhdlAliasDecl(const VhdlAliasDecl &) ;            // Purposely leave unimplemented
    VhdlAliasDecl& operator=(const VhdlAliasDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLALIASDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy alias declaration.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new type information in its place.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new target name in its place.

    // Accessor methods
    VhdlIdDef *                 GetDesignator() const                   { return _designator ; }
    VhdlSubtypeIndication *     GetSubtypeIndication() const            { return _subtype_indication ; }
    VhdlName *                  GetAliasTargetName() const              { return _alias_target_name ; }
    virtual VhdlIdDef *         GetTypeId() const                       { return _designator ; }

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl data declaration to verilog data declaration
    virtual VeriDataDecl *      ConvertDataDecl(VeriScope *scope) const ;
#endif

    // Elaboration
    virtual void Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef             *_designator ;
    VhdlSubtypeIndication *_subtype_indication ;
    VhdlName              *_alias_target_name ;
} ; // class VhdlAliasDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlComponentDecl : public VhdlDeclaration
{
public:
    VhdlComponentDecl(VhdlIdDef *id, Array *generic_clause, Array *port_clause, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlComponentDecl(const VhdlComponentDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlComponentDecl(SaveRestore &save_restore);

    virtual ~VhdlComponentDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlComponentDecl() ;                                     // Purposely leave unimplemented
    VhdlComponentDecl(const VhdlComponentDecl &) ;            // Purposely leave unimplemented
    VhdlComponentDecl& operator=(const VhdlComponentDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCOMPONENTDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy component declaration.
    // Component id is copied while copying this declaration.
    // Copied component id is not attached to any scope by
    // this routine.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;
    // VIPER 2375 : Copy component declaration with argument specific name
    VhdlComponentDecl *         CopyWithName(const char *new_name, VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Find 'old_node' in generic-list and port-list, delete 'old_node' and puts new declaration in its place.

    // Find 'target_node' specific declaration in generic/port list.
    // Insert new declaration before/after the ''target_node' specified
    // declaration. If 'target_node' is null, nothing is inserted.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;

    // Local declared object find routine
    VhdlIdDef *                 FindDeclared(const char *name) const ;

    // Parse-tree port creation/deletion routines
    void                        SetPortClause(Array *port_clause) ;
    VhdlIdDef *                 AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication) ; // Create and add argument specific port to component
    unsigned                    RemovePort(const char *port_name) ; // Remove argument specific port from component

    // Parse-tree generic creation/deletion routines
    void                        SetGenericClause(Array *generic_clause) ; // Set new generic clause
    VhdlIdDef *                 AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign) ; // Create and add argument specific generic to component
    unsigned                    RemoveGeneric(const char *generic_name) ; // Remove argument specific generic from component

    // Static Elaboration
    void                        SplitAndAssociateGenerics(const Array *actual_generics) ; // VIPER 2375 : Set proper default/overridden value to the generic declaration.
    void                        SplitAndAssociatePorts(const Array *actual_ports) ; // VIPER #6281
    const char *                GetOriginalComponentName() const { return _orig_component ; } // Get the component name from which this unit is copied
    void                        SetOriginalComponentName(const char *name) ;

    // Accessor methods
    VhdlIdDef *                 GetId() const                   { return _id ; }
    Array *                     GetGenericClause() const        { return _generic_clause ; }
    Array *                     GetPortClause() const           { return _port_clause ; }
    virtual VhdlScope *         LocalScope() const ;

    virtual void                Elaborate(VhdlDataFlow *df) ;

    void                        AssociateGenerics(const Array *actual_generics) const ;
    void                        AssociatePorts(Instance *inst, const Array *actual_ports) const ;

    unsigned long               TimeStamp() const { return _timestamp ; }  // Time that this component was created

    void                        AddAfter(VhdlComponentDecl *orig_comp_decl, VhdlScope *upper_scope) ; // VIPER #6281 : Add this after 'orig_comp_decl'

private:
    void                        CopyFrom(const VhdlComponentDecl &node, VhdlMapForCopy &old2new) ; // Private routine to copy members of 'this' from 'node' (#2375)

protected:
// Parse tree nodes (owned) :
    VhdlIdDef       *_id ;
    Array           *_generic_clause ;  // (interface list) Array of VhdlInterfaceDecl*
    Array           *_port_clause ;     // (interface list) Array of VhdlInterfaceDecl*
    VhdlScope       *_local_scope ;

// Other fields
    unsigned long    _timestamp ;       // the time that this component was created.
    char            *_orig_component ;  // Name of component from which this component is copied
} ; // class VhdlComponentDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAttributeDecl : public VhdlDeclaration
{
public:
    VhdlAttributeDecl(VhdlIdDef *id, VhdlName *type_mark) ;

    // Copy tree node
    VhdlAttributeDecl(const VhdlAttributeDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAttributeDecl(SaveRestore &save_restore);

    virtual ~VhdlAttributeDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlAttributeDecl() ;                                     // Purposely leave unimplemented
    VhdlAttributeDecl(const VhdlAttributeDecl &) ;            // Purposely leave unimplemented
    VhdlAttributeDecl& operator=(const VhdlAttributeDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLATTRIBUTEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy attribute declaration.
    // Attribute identifier is copied here, but copied
    // identifier is not attached to any scope by this routine.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new type mark in its place.

    // Accessor methods
    VhdlIdDef *                 GetId() const               { return _id ; }
    VhdlName *                  GetTypeMark() const         { return _type_mark ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef *_id ;
    VhdlName  *_type_mark ;
} ; // class VhdlAttributeDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAttributeSpec : public VhdlDeclaration
{
public:
    VhdlAttributeSpec(VhdlName *designator, VhdlSpecification *entity_spec, VhdlExpression *value) ;

    // Copy tree node
    VhdlAttributeSpec(const VhdlAttributeSpec &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAttributeSpec(SaveRestore &save_restore);

    virtual ~VhdlAttributeSpec() ;

private:
    // Prevent compiler from defining the following
    VhdlAttributeSpec() ;                                     // Purposely leave unimplemented
    VhdlAttributeSpec(const VhdlAttributeSpec &) ;            // Purposely leave unimplemented
    VhdlAttributeSpec& operator=(const VhdlAttributeSpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLATTRIBUTESPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Resolve the specification labels after the full scope is analyzed :
    virtual void                ResolveSpecs(VhdlScope *scope) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy attribute specification.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;
    // Internal routine to copy attribute specification
    virtual void                UpdateDecl(VhdlMapForCopy &old2new) ; // Set copied back pointers to entity specification

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new name in its place.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node) ; // Delete 'old_node', puts new entity specification in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts expression in its place.

    // Accessor methods
    VhdlName *                  GetDesignator() const           { return _designator ; }
    VhdlSpecification *         GetEntitySpec() const           { return _entity_spec ; }
    VhdlExpression *            GetValue() const                { return _value ; }
    void                        InvalidateSpec() ; // Viper #5432

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlName            *_designator ;
    VhdlSpecification   *_entity_spec ; // actually a VhdlEntitySpec
    VhdlExpression      *_value ;
} ; // class VhdlAttributeSpec

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConfigurationSpec : public VhdlDeclaration
{
public:
    VhdlConfigurationSpec(VhdlSpecification *component_spec, VhdlBindingIndication *binding) ;

    // Copy tree node
    VhdlConfigurationSpec(const VhdlConfigurationSpec &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConfigurationSpec(SaveRestore &save_restore);

    virtual ~VhdlConfigurationSpec() ;

private:
    // Prevent compiler from defining the following
    VhdlConfigurationSpec() ;                                         // Purposely leave unimplemented
    VhdlConfigurationSpec(const VhdlConfigurationSpec &) ;            // Purposely leave unimplemented
    VhdlConfigurationSpec& operator=(const VhdlConfigurationSpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONFIGURATIONSPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Resolve the specification labels after the full scope is analyzed :
    virtual void                ResolveSpecs(VhdlScope *scope) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            IsConfigurationSpec() const    { return 1 ; }
    virtual void                CheckLabel(Map* all_other_map) ; // Viper 4738: Check whether : all or :others is present as the last label

    // Copy configuration specification.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node) ; // Delete 'old_node', puts new specification in its place.
    virtual unsigned            ReplaceChildBindingIndication(VhdlBindingIndication *old_node, VhdlBindingIndication *new_node) ; // Delete 'old_node', puts new binding indication in its place.

    // Static Elaboration

    // In static elaboration component instantiation is
    // converted to direct entity instantiation according to
    // its configuration specification. So after static
    // elaboration this virtual routine delete 'this'
    virtual void                DeleteUnusedComSpec(VhdlArchitectureBody *parent_arch) ;

    // Accessor methods
    VhdlSpecification *         GetComponentSpec() const        { return _component_spec ; }
    VhdlBindingIndication *     GetBinding() const              { return _binding ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlSpecification       *_component_spec ;
    VhdlBindingIndication   *_binding ;
} ; // class VhdlConfigurationSpec

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlDisconnectionSpec : public VhdlDeclaration
{
public:
    VhdlDisconnectionSpec(VhdlSpecification *guarded_signal_spec, VhdlExpression *after) ;

    // Copy tree node
    VhdlDisconnectionSpec(const VhdlDisconnectionSpec &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlDisconnectionSpec(SaveRestore &save_restore);

    virtual ~VhdlDisconnectionSpec() ;

private:
    // Prevent compiler from defining the following
    VhdlDisconnectionSpec() ;                                         // Purposely leave unimplemented
    VhdlDisconnectionSpec(const VhdlDisconnectionSpec &) ;            // Purposely leave unimplemented
    VhdlDisconnectionSpec& operator=(const VhdlDisconnectionSpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLDISCONNECTIONSPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Resolve the specification labels after the full scope is analyzed :
    virtual void                ResolveSpecs(VhdlScope *scope) ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy disconnection specification.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node) ; // Delete 'old_node', puts new specification in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Accessor methods
    VhdlSpecification *         GetGuardedSignalSpec() const    { return _guarded_signal_spec ; }
    VhdlExpression *            GetAfter() const                { return _after ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlSpecification   *_guarded_signal_spec ;
    VhdlExpression      *_after ;
} ; // class VhdlDisconnectionSpec

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlGroupTemplateDecl : public VhdlDeclaration
{
public:
    VhdlGroupTemplateDecl(VhdlIdDef *id, Array *entity_class_entry_list) ;

    // Copy tree node
    VhdlGroupTemplateDecl(const VhdlGroupTemplateDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlGroupTemplateDecl(SaveRestore &save_restore);

    virtual ~VhdlGroupTemplateDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlGroupTemplateDecl() ;                                         // Purposely leave unimplemented
    VhdlGroupTemplateDecl(const VhdlGroupTemplateDecl &) ;            // Purposely leave unimplemented
    VhdlGroupTemplateDecl& operator=(const VhdlGroupTemplateDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLGROUPTEMPLATEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy group template declaration.
    // Identifier is copied here, but is not attached
    // to any scope by this routine.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.
    virtual unsigned            ReplaceChildEntityClassEntry(VhdlEntityClassEntry *old_node, VhdlEntityClassEntry *new_node) ; // Delete 'old_node', puts new entity class entry in its place.

    // Insert new entity class entry before/after 'target_node'
    // specified entity class entry in the entity class entry
    // list. If 'target_node' is null, nothing is inserted in the list.
    virtual unsigned            InsertBeforeEntityClassEntry(const VhdlEntityClassEntry *target_node, VhdlEntityClassEntry *new_node) ;
    virtual unsigned            InsertAfterEntityClassEntry(const VhdlEntityClassEntry *target_node, VhdlEntityClassEntry *new_node) ;

    // Accessor methods
    VhdlIdDef *                 GetId() const                       { return _id ; }
    Array *                     GetEntityClassEntryList() const     { return _entity_class_entry_list ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef   *_id ;
    Array       *_entity_class_entry_list ;  // Array of VhdlEntityClassEntry*
} ; // class VhdlGroupTemplateDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlGroupDecl : public VhdlDeclaration
{
public:
    VhdlGroupDecl(VhdlIdDef *id, VhdlName *group_template_name) ;

    // Copy tree node
    VhdlGroupDecl(const VhdlGroupDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlGroupDecl(SaveRestore &save_restore);

    virtual ~VhdlGroupDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlGroupDecl() ;                                 // Purposely leave unimplemented
    VhdlGroupDecl(const VhdlGroupDecl &) ;            // Purposely leave unimplemented
    VhdlGroupDecl& operator=(const VhdlGroupDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLGROUPDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy group declaration. Group id is copied by
    // this routine, but is not attached to any scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new group id in its place.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new group template name in its name.

    // Accessor methods
    VhdlIdDef *                 GetId() const                   { return _id ; }
    VhdlName *                  GetGroupTemplateName() const    { return _group_template_name ; }

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef   *_id ;
    VhdlName    *_group_template_name ;
} ; // class VhdlGroupDecl

/* -------------------------------------------------------------- */

// Class to represent VHDL-2008 specific construct subprogram instantiation declaration
// subprogram_kind designator is new uninstantiated_subprogram_name [signature] [generic_map_aspect] ;
class VFC_DLL_PORT VhdlSubprogInstantiationDecl : public VhdlDeclaration
{
public:
    VhdlSubprogInstantiationDecl(VhdlSpecification *inst_spec, VhdlName *uninstantiated_subprog_name, Array *generic_map_aspect) ;

    // Copy tree node
    VhdlSubprogInstantiationDecl(const VhdlSubprogInstantiationDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSubprogInstantiationDecl(SaveRestore &save_restore);

    virtual ~VhdlSubprogInstantiationDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlSubprogInstantiationDecl() ;                                                // Purposely leave unimplemented
    VhdlSubprogInstantiationDecl(const VhdlSubprogInstantiationDecl &) ;            // Purposely leave unimplemented
    VhdlSubprogInstantiationDecl& operator=(const VhdlSubprogInstantiationDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSUBPROGINSTANTIATIONDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy subprogram instantiation declaration. Instance id is copied by
    // this routine, but is not attached to any scope.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new group template name in its name.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node) ; // Delete 'old_node', puts new specification in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Deletes 'old_node', and puts new generic map/port map aspect element in its place

    // Accessor methods
    VhdlIdDef *                 GetId() const ;
    VhdlSpecification *         GetSpecification() const              { return _subprogram_spec ; }
    VhdlName  *                 GetUninstantiatedSubprogName() const  { return _uninstantiated_subprog_name ; }
    virtual Array *             GetGenericMapAspect() const           { return _generic_map_aspect ; }
    virtual VhdlIdDef *         GetUninstantiatedSubprogId() const ;

    // Internal routine :
    virtual void                SetActualTypes() ; // Defined only for subprogram instantiation decl
    // Elaboration
    virtual void                Elaborate(VhdlDataFlow * /*df*/) {} // FIXME
    virtual VhdlValue *         ElaborateSubprogramInst(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode * caller_node) ;

protected:
// Parse tree nodes (owned) :
    VhdlSpecification   *_subprogram_spec ; // subprogram_kind and instance name
    VhdlName            *_uninstantiated_subprog_name ; // uninstantiated subprogram name and signature
    Array               *_generic_map_aspect ; // generic map aspect
    Map                 *_generic_types ; // Map of generic_type_id vs iddef for its actual type (from _generic_map_aspect) : Set during analysis
} ; // class VhdlSubprogInstantiationDecl

/* -------------------------------------------------------------- */
// VIPER #5918 : Class to represent interface type declaration
//  type type_identifier
class VFC_DLL_PORT VhdlInterfaceTypeDecl : public VhdlInterfaceDecl
{
public:
    explicit VhdlInterfaceTypeDecl(VhdlIdDef *type_id) ;

    // Copy tree node
    VhdlInterfaceTypeDecl(const VhdlInterfaceTypeDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInterfaceTypeDecl(SaveRestore &save_restore);

    virtual ~VhdlInterfaceTypeDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlInterfaceTypeDecl() ;                                         // Purposely leave unimplemented
    VhdlInterfaceTypeDecl(const VhdlInterfaceTypeDecl &) ;            // Purposely leave unimplemented
    VhdlInterfaceTypeDecl& operator=(const VhdlInterfaceTypeDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLINTERFACETYPEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy interface declaration.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', pus new identifier in its place.

    // Static Elaboration

    // Convert declaration with array-of-ids to
    // declarations with single-identifier.
    virtual void                Split(Array *new_decl_list) ;

    // Create range constraint if the range constraint
    // is not specified in this declaration.
    virtual void                CreateRangeConstraint() {}

    // Static elaborator converts component instantiation to direct
    // entity instantiation. This routine consider the initial values
    // of input ports to create new association elements for the
    // terminal list of created entity instantiation.
    virtual void                DefaultValues(const Set * /*id_considered*/, Array * /*port_map*/, const VhdlIdDef * /*instantiated_unit*/, Set * /*en_id_considered*/, linefile_type /*linefile*/) const {}

    virtual unsigned            IsInterfaceTypeDecl() const { return 1 ; }

    // Accessor methods
    virtual VhdlIdDef *         GetId() const  { return _type_id ; } // Returns generic type id
    // Elaboration

    // Before association with actuals :
    virtual void                Initialize() ;                // Set constraints and re-set values to 0.
    virtual void                InitializeConstraints(Map &formal_to_constraint) ; // Create constraints and insert in the Map (id->constraint)

    // After association with actuals :
    virtual void                InitialValues(Instance *inst, unsigned has_recursion, VhdlDataFlow *df, VhdlScope *assoc_list_scope) ; // Propagate recursion info so that stack is not used for function

protected:
// Parse tree nodes (owned) :
    VhdlIdDef *_type_id ; // Generic type id
} ; // class VhdlInterfaceTypeDecl

/* -------------------------------------------------------------- */

// VIPER #5918 : Class to represent interface subprogram declaration
//  interface_subprogram_specification [ is interface_subprogram_default ]
// interface_subprogram_specification will be VhdlSpecification and will be owned by
// this class. interface_subprogram_default will be stored as 'VhdlExpression *_init_assign'
// in base class VhdlInterfaceDecl
class VFC_DLL_PORT VhdlInterfaceSubprogDecl : public VhdlInterfaceDecl
{
public:
    VhdlInterfaceSubprogDecl(VhdlSpecification *subprog_specification, VhdlExpression *subprogram_default) ;

    // Copy tree node
    VhdlInterfaceSubprogDecl(const VhdlInterfaceSubprogDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInterfaceSubprogDecl(SaveRestore &save_restore);

    virtual ~VhdlInterfaceSubprogDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlInterfaceSubprogDecl() ;                                            // Purposely leave unimplemented
    VhdlInterfaceSubprogDecl(const VhdlInterfaceSubprogDecl &) ;            // Purposely leave unimplemented
    VhdlInterfaceSubprogDecl& operator=(const VhdlInterfaceSubprogDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLINTERFACESUBPROGDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy interface declaration.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;

    // Specific parse tree manipulation routines.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node) ; // Delete existing specification, puts new specification in its place.

    // Static Elaboration

    // Convert declaration with array-of-ids to
    // declarations with single-identifier.
    virtual void                Split(Array *new_decl_list) ;

    // Create range constraint if the range constraint
    // is not specified in this declaration.
    virtual void                CreateRangeConstraint() {}

    // Static elaborator converts component instantiation to direct
    // entity instantiation. This routine consider the initial values
    // of input ports to create new association elements for the
    // terminal list of created entity instantiation.
    virtual void                DefaultValues(const Set * /*id_considered*/, Array * /*port_map*/, const VhdlIdDef * /*instantiated_unit*/, Set * /*en_id_considered*/, linefile_type /*linefile*/) const {}

    virtual VhdlIdDef *         GetUninstantiatedSubprogId() const { return GetActualId() ; }  // Defined for subprogram instantiation decl/interface subprogram decl
    virtual void                SetActualId(VhdlIdDef *id) ;
    virtual VhdlIdDef *         GetActualId() const ;
    virtual VhdlIdDef *         TakeActualId() ;
    virtual unsigned            IsInterfaceSubprogDecl() const     { return 1 ; }

    // Accessor methods
    virtual VhdlSpecification * GetSubprogramSpec() const { return _subprogram_spec ; }
    virtual VhdlIdDef *         GetId() const ;
    virtual VhdlIdDef *         GetDefaultSubprogram(VhdlScope *assoc_list_scope) ;

    // Elaboration

    // Before association with actuals :
    virtual void                Initialize() ;                // Set constraints and re-set values to 0.
    virtual void                InitializeConstraints(Map &formal_to_constraint) ; // Create constraints and insert in the Map (id->constraint)

    // After association with actuals :
    virtual void                InitialValues(Instance *inst, unsigned has_recursion, VhdlDataFlow *df, VhdlScope *assoc_list_scope) ; // Propagate recursion info so that stack is not used for function
    virtual VhdlValue *         ElaborateSubprogramInst(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode * caller_node) ;

protected:
// Parse tree nodes (owned) :
    VhdlSpecification *_subprogram_spec ; // Interface subprogram name
// Set during analysis/elaboration
    VhdlIdDef         *_actual_subprog ; // Resolved actual subprogram (from generic map aspect or default)

} ; // class VhdlInterfaceSubprogDecl

/* -------------------------------------------------------------- */

// VIPER #5918 : Class to represent interface package declaration
//  package identifier is uninstantiated_package_name interface_package_generic_map_aspect
class VFC_DLL_PORT VhdlInterfacePackageDecl : public VhdlInterfaceDecl
{
public:
    VhdlInterfacePackageDecl(VhdlIdDef *inst_id, VhdlName *uninst_package_name, Array *generic_map_aspect, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlInterfacePackageDecl(const VhdlInterfacePackageDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInterfacePackageDecl(SaveRestore &save_restore);

    virtual ~VhdlInterfacePackageDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlInterfacePackageDecl() ;                                            // Purposely leave unimplemented
    VhdlInterfacePackageDecl(const VhdlInterfacePackageDecl &) ;            // Purposely leave unimplemented
    VhdlInterfacePackageDecl& operator=(const VhdlInterfacePackageDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLINTERFACEPACKAGEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this declaration
    virtual unsigned long       CalculateSignature() const ;

    // Copy interface declaration.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const ;
    void CreateSpecialPackageElementIds() ;

    // Static Elaboration

    // Convert declaration with array-of-ids to
    // declarations with single-identifier.
    virtual void                Split(Array *new_decl_list) ;

    // Create range constraint if the range constraint
    // is not specified in this declaration.
    virtual void                CreateRangeConstraint() {}

    // Static elaborator converts component instantiation to direct
    // entity instantiation. This routine consider the initial values
    // of input ports to create new association elements for the
    // terminal list of created entity instantiation.
    virtual void                DefaultValues(const Set * /*id_considered*/, Array * /*port_map*/, const VhdlIdDef * /*instantiated_unit*/, Set * /*en_id_considered*/, linefile_type /*linefile*/) const {}

    virtual unsigned            IsInterfacePackageDecl() const        { return 1 ; }

    // Accessor methods
    virtual VhdlIdDef *         GetId() const ;  // Returns generic package id
    VhdlName *                  GetUninstantiatedPackageName() const { return _uninst_package_name ; }
    virtual VhdlScope *         LocalScope() const { return _local_scope ; }
    virtual Array *             GetGenericMapAspect() const { return _generic_map_aspect ; }
    virtual void                SetActualId(VhdlIdDef *actual) ;
    virtual VhdlIdDef *         GetActualId() const ;
    virtual VhdlIdDef *         TakeActualId() ;

    // Internal methods :
    void                        CheckGenericMapAspect(const VhdlIdDef *actual_id) const ;
    void                        CheckFormalActualMismatch(VhdlIdDef *actual_id) const ;
    void                        AssociateFormalActualElementIds(VhdlIdDef *actual_id) const ;

    // Elaboration

    // Before association with actuals :
    virtual void                Initialize() ;                // Set constraints and re-set values to 0.
    virtual void                InitializeConstraints(Map &formal_to_constraint) ; // Create constraints and insert in the Map (id->constraint)

    // After association with actuals :
    virtual void                InitialValues(Instance *inst, unsigned has_recursion, VhdlDataFlow *df, VhdlScope *assoc_list_scope) ; // Propagate recursion info so that stack is not used for function

    // APIs to set/get actual package during elaboration
    virtual void                SetElaboratedPackageId(VhdlIdDef *actual) ;
    virtual VhdlIdDef *         GetElaboratedPackageId() const ;
    virtual VhdlIdDef *         TakeElaboratedPackageId() ;
    // VIPER #7479 : Check actual values of generic
    void                        CheckGenericMapAspectValueMismatch(const VhdlIdDef *actual_pkg) const ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef       *_id ; // instance identifier
    VhdlName        *_uninst_package_name ; // Uninstantiated package name
    Array           *_generic_map_aspect ; // Array of VhdlDiscreteRange*
    VhdlScope       *_local_scope ; // Scope of this object
    VhdlPrimaryUnit *_initial_pkg ; // Package specified during declaration
    VhdlIdDef       *_uninst_package_id ; // Uninstantiated package id
    Map             *_generic_types ; // Map of generic_type_id vs iddef for its actual type (from _inst_generic_map_aspect) : Set during analysis
    VhdlIdDef       *_actual_pkg ; // Associated actual package
    VhdlIdDef       *_actual_elab_pkg ; // Associated actual elaborated package
} ; // class VhdlInterfacePackageDecl

/* -------------------------------------------------------------- */
#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_DECLARATION_H_

