/* A Bison parser, made by GNU Bison 1.875c.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 1

/* If NAME_PREFIX is specified substitute the variables and functions
   names.  */
#define yyparse vhdlparse
#define yylex   vhdllex
#define yyerror vhdlerror
#define yylval  vhdllval
#define yychar  vhdlchar
#define yydebug vhdldebug
#define yynerrs vhdlnerrs
#define yylloc vhdllloc

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     VHDL_AMPERSAND = 257,
     VHDL_TICK = 258,
     VHDL_OPAREN = 259,
     VHDL_CPAREN = 260,
     VHDL_STAR = 261,
     VHDL_PLUS = 262,
     VHDL_COMMA = 263,
     VHDL_MINUS = 264,
     VHDL_DOT = 265,
     VHDL_SLASH = 266,
     VHDL_COLON = 267,
     VHDL_SEMI = 268,
     VHDL_STHAN = 269,
     VHDL_EQUAL = 270,
     VHDL_GTHAN = 271,
     VHDL_VERTICAL = 272,
     VHDL_BANG = 273,
     VHDL_OBRACK = 274,
     VHDL_CBRACK = 275,
     VHDL_ARROW = 276,
     VHDL_EXPONENT = 277,
     VHDL_VARASSIGN = 278,
     VHDL_NEQUAL = 279,
     VHDL_GEQUAL = 280,
     VHDL_SEQUAL = 281,
     VHDL_BOX = 282,
     VHDL_abs = 283,
     VHDL_access = 284,
     VHDL_after = 285,
     VHDL_alias = 286,
     VHDL_all = 287,
     VHDL_and = 288,
     VHDL_architecture = 289,
     VHDL_array = 290,
     VHDL_assert = 291,
     VHDL_attribute = 292,
     VHDL_begin = 293,
     VHDL_block = 294,
     VHDL_body = 295,
     VHDL_buffer = 296,
     VHDL_bus = 297,
     VHDL_case = 298,
     VHDL_component = 299,
     VHDL_configuration = 300,
     VHDL_constant = 301,
     VHDL_disconnect = 302,
     VHDL_downto = 303,
     VHDL_else = 304,
     VHDL_elsif = 305,
     VHDL_end = 306,
     VHDL_entity = 307,
     VHDL_exit = 308,
     VHDL_file = 309,
     VHDL_for = 310,
     VHDL_function = 311,
     VHDL_generate = 312,
     VHDL_generic = 313,
     VHDL_group = 314,
     VHDL_guarded = 315,
     VHDL_if = 316,
     VHDL_impure = 317,
     VHDL_in = 318,
     VHDL_inertial = 319,
     VHDL_inout = 320,
     VHDL_is = 321,
     VHDL_label = 322,
     VHDL_library = 323,
     VHDL_linkage = 324,
     VHDL_literal = 325,
     VHDL_loop = 326,
     VHDL_map = 327,
     VHDL_mod = 328,
     VHDL_nand = 329,
     VHDL_new = 330,
     VHDL_next = 331,
     VHDL_nor = 332,
     VHDL_not = 333,
     VHDL_null = 334,
     VHDL_of = 335,
     VHDL_on = 336,
     VHDL_open = 337,
     VHDL_or = 338,
     VHDL_others = 339,
     VHDL_out = 340,
     VHDL_package = 341,
     VHDL_port = 342,
     VHDL_postponed = 343,
     VHDL_procedure = 344,
     VHDL_process = 345,
     VHDL_pure = 346,
     VHDL_range = 347,
     VHDL_record = 348,
     VHDL_register = 349,
     VHDL_reject = 350,
     VHDL_rem = 351,
     VHDL_report = 352,
     VHDL_return = 353,
     VHDL_rol = 354,
     VHDL_ror = 355,
     VHDL_select = 356,
     VHDL_severity = 357,
     VHDL_signal = 358,
     VHDL_shared = 359,
     VHDL_sla = 360,
     VHDL_sll = 361,
     VHDL_sra = 362,
     VHDL_srl = 363,
     VHDL_subtype = 364,
     VHDL_then = 365,
     VHDL_to = 366,
     VHDL_transport = 367,
     VHDL_type = 368,
     VHDL_unaffected = 369,
     VHDL_units = 370,
     VHDL_until = 371,
     VHDL_use = 372,
     VHDL_variable = 373,
     VHDL_wait = 374,
     VHDL_when = 375,
     VHDL_while = 376,
     VHDL_with = 377,
     VHDL_xnor = 378,
     VHDL_xor = 379,
     VHDL_INTEGER = 380,
     VHDL_REAL = 381,
     VHDL_STRING = 382,
     VHDL_BIT_STRING = 383,
     VHDL_ID = 384,
     VHDL_EXTENDED_ID = 385,
     VHDL_CHARACTER = 386,
     VHDL_FATAL_ERROR = 387,
     VHDL_OPT_COMMENT = 484,
     VHDL_NO_COMMENT = 485,
     VHDL_NO_OPAREN = 486,
     VHDL_implicit_guard = 492,
     VHDL_protected = 493,
     VHDL_interfacerange = 494,
     VHDL_interfaceread = 495,
     VHDL_interfaceprefixread = 496,
     VHDL_subtyperange = 497,
     VHDL_subtyperangebound = 498,
     VHDL_minimum = 550,
     VHDL_maximum = 551,
     VHDL_condition = 552,
     VHDL_matching_equal = 553,
     VHDL_matching_nequal = 554,
     VHDL_matching_gthan = 555,
     VHDL_matching_sthan = 556,
     VHDL_matching_gequal = 557,
     VHDL_matching_sequal = 558,
     VHDL_context = 559,
     VHDL_end_generate = 560,
     VHDL_parameter = 561,
     VHDL_default = 562,
     VHDL_matching_op = 563,
     VHDL_to_string = 564,
     VHDL_force = 565,
     VHDL_release = 566,
     VHDL_LANGLEQ = 567,
     VHDL_RANGLEQ = 568,
     VHDL_AT = 569,
     VHDL_ACCENT = 570,
     VHDL_BEGIN_EXPR = 571,
     VHDL_to_bstring = 572,
     VHDL_to_binary_string = 573,
     VHDL_to_ostring = 574,
     VHDL_to_octal_string = 575,
     VHDL_to_hstring = 576,
     VHDL_to_hex_string = 577,
     VHDL_recordrange = 578,
     VHDL_resize = 3000,
     VHDL_uns_resize = 3001,
     VHDL_signed_mult = 3002,
     VHDL_unsigned_mult = 3003,
     VHDL_numeric_signed_mult = 3004,
     VHDL_numeric_unsigned_mult = 3005,
     VHDL_numeric_sign_div = 3006,
     VHDL_numeric_uns_div = 3007,
     VHDL_numeric_minus = 3008,
     VHDL_numeric_unary_minus = 3009,
     VHDL_numeric_plus = 3010,
     VHDL_is_uns_less = 3011,
     VHDL_is_uns_gt = 3012,
     VHDL_is_gt = 3013,
     VHDL_is_less = 3014,
     VHDL_is_uns_gt_or_equal = 3015,
     VHDL_is_uns_less_or_equal = 3016,
     VHDL_is_gt_or_equal = 3017,
     VHDL_is_less_or_equal = 3018,
     VHDL_and_reduce = 3019,
     VHDL_or_reduce = 3020,
     VHDL_xor_reduce = 3021,
     VHDL_numeric_mod = 3022,
     VHDL_i_to_s = 3023,
     VHDL_s_to_u = 3024,
     VHDL_s_to_i = 3025,
     VHDL_u_to_i = 3026,
     VHDL_u_to_u = 3027,
     VHDL_s_xt = 3028
   };
#endif
#define VHDL_AMPERSAND 257
#define VHDL_TICK 258
#define VHDL_OPAREN 259
#define VHDL_CPAREN 260
#define VHDL_STAR 261
#define VHDL_PLUS 262
#define VHDL_COMMA 263
#define VHDL_MINUS 264
#define VHDL_DOT 265
#define VHDL_SLASH 266
#define VHDL_COLON 267
#define VHDL_SEMI 268
#define VHDL_STHAN 269
#define VHDL_EQUAL 270
#define VHDL_GTHAN 271
#define VHDL_VERTICAL 272
#define VHDL_BANG 273
#define VHDL_OBRACK 274
#define VHDL_CBRACK 275
#define VHDL_ARROW 276
#define VHDL_EXPONENT 277
#define VHDL_VARASSIGN 278
#define VHDL_NEQUAL 279
#define VHDL_GEQUAL 280
#define VHDL_SEQUAL 281
#define VHDL_BOX 282
#define VHDL_abs 283
#define VHDL_access 284
#define VHDL_after 285
#define VHDL_alias 286
#define VHDL_all 287
#define VHDL_and 288
#define VHDL_architecture 289
#define VHDL_array 290
#define VHDL_assert 291
#define VHDL_attribute 292
#define VHDL_begin 293
#define VHDL_block 294
#define VHDL_body 295
#define VHDL_buffer 296
#define VHDL_bus 297
#define VHDL_case 298
#define VHDL_component 299
#define VHDL_configuration 300
#define VHDL_constant 301
#define VHDL_disconnect 302
#define VHDL_downto 303
#define VHDL_else 304
#define VHDL_elsif 305
#define VHDL_end 306
#define VHDL_entity 307
#define VHDL_exit 308
#define VHDL_file 309
#define VHDL_for 310
#define VHDL_function 311
#define VHDL_generate 312
#define VHDL_generic 313
#define VHDL_group 314
#define VHDL_guarded 315
#define VHDL_if 316
#define VHDL_impure 317
#define VHDL_in 318
#define VHDL_inertial 319
#define VHDL_inout 320
#define VHDL_is 321
#define VHDL_label 322
#define VHDL_library 323
#define VHDL_linkage 324
#define VHDL_literal 325
#define VHDL_loop 326
#define VHDL_map 327
#define VHDL_mod 328
#define VHDL_nand 329
#define VHDL_new 330
#define VHDL_next 331
#define VHDL_nor 332
#define VHDL_not 333
#define VHDL_null 334
#define VHDL_of 335
#define VHDL_on 336
#define VHDL_open 337
#define VHDL_or 338
#define VHDL_others 339
#define VHDL_out 340
#define VHDL_package 341
#define VHDL_port 342
#define VHDL_postponed 343
#define VHDL_procedure 344
#define VHDL_process 345
#define VHDL_pure 346
#define VHDL_range 347
#define VHDL_record 348
#define VHDL_register 349
#define VHDL_reject 350
#define VHDL_rem 351
#define VHDL_report 352
#define VHDL_return 353
#define VHDL_rol 354
#define VHDL_ror 355
#define VHDL_select 356
#define VHDL_severity 357
#define VHDL_signal 358
#define VHDL_shared 359
#define VHDL_sla 360
#define VHDL_sll 361
#define VHDL_sra 362
#define VHDL_srl 363
#define VHDL_subtype 364
#define VHDL_then 365
#define VHDL_to 366
#define VHDL_transport 367
#define VHDL_type 368
#define VHDL_unaffected 369
#define VHDL_units 370
#define VHDL_until 371
#define VHDL_use 372
#define VHDL_variable 373
#define VHDL_wait 374
#define VHDL_when 375
#define VHDL_while 376
#define VHDL_with 377
#define VHDL_xnor 378
#define VHDL_xor 379
#define VHDL_INTEGER 380
#define VHDL_REAL 381
#define VHDL_STRING 382
#define VHDL_BIT_STRING 383
#define VHDL_ID 384
#define VHDL_EXTENDED_ID 385
#define VHDL_CHARACTER 386
#define VHDL_FATAL_ERROR 387
#define VHDL_OPT_COMMENT 484
#define VHDL_NO_COMMENT 485
#define VHDL_NO_OPAREN 486
#define VHDL_implicit_guard 492
#define VHDL_protected 493
#define VHDL_interfacerange 494
#define VHDL_interfaceread 495
#define VHDL_interfaceprefixread 496
#define VHDL_subtyperange 497
#define VHDL_subtyperangebound 498
#define VHDL_minimum 550
#define VHDL_maximum 551
#define VHDL_condition 552
#define VHDL_matching_equal 553
#define VHDL_matching_nequal 554
#define VHDL_matching_gthan 555
#define VHDL_matching_sthan 556
#define VHDL_matching_gequal 557
#define VHDL_matching_sequal 558
#define VHDL_context 559
#define VHDL_end_generate 560
#define VHDL_parameter 561
#define VHDL_default 562
#define VHDL_matching_op 563
#define VHDL_to_string 564
#define VHDL_force 565
#define VHDL_release 566
#define VHDL_LANGLEQ 567
#define VHDL_RANGLEQ 568
#define VHDL_AT 569
#define VHDL_ACCENT 570
#define VHDL_BEGIN_EXPR 571
#define VHDL_to_bstring 572
#define VHDL_to_binary_string 573
#define VHDL_to_ostring 574
#define VHDL_to_octal_string 575
#define VHDL_to_hstring 576
#define VHDL_to_hex_string 577
#define VHDL_recordrange 578
#define VHDL_resize 3000
#define VHDL_uns_resize 3001
#define VHDL_signed_mult 3002
#define VHDL_unsigned_mult 3003
#define VHDL_numeric_signed_mult 3004
#define VHDL_numeric_unsigned_mult 3005
#define VHDL_numeric_sign_div 3006
#define VHDL_numeric_uns_div 3007
#define VHDL_numeric_minus 3008
#define VHDL_numeric_unary_minus 3009
#define VHDL_numeric_plus 3010
#define VHDL_is_uns_less 3011
#define VHDL_is_uns_gt 3012
#define VHDL_is_gt 3013
#define VHDL_is_less 3014
#define VHDL_is_uns_gt_or_equal 3015
#define VHDL_is_uns_less_or_equal 3016
#define VHDL_is_gt_or_equal 3017
#define VHDL_is_less_or_equal 3018
#define VHDL_and_reduce 3019
#define VHDL_or_reduce 3020
#define VHDL_xor_reduce 3021
#define VHDL_numeric_mod 3022
#define VHDL_i_to_s 3023
#define VHDL_s_to_u 3024
#define VHDL_s_to_i 3025
#define VHDL_u_to_i 3026
#define VHDL_u_to_u 3027
#define VHDL_s_xt 3028




/* Copy the first part of user declarations.  */


/*
 *
 * [ File Version : 1.236 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

////////////////////////////////////////////////////////////////////////////
//
//                        DEVELOPERS NOTE
//
// Verific recommends that you do not change this file, so that you do NOT
// need to re-run bison either. Verific ran bison on it already, which
// resulted in the file vhdl_yacc.cpp, which is included in this directory.
//
// If you need to make a custom change to this .y file, or have another
// reason why you need to re-run bison, then make sure you use bison 1.35
// or later, and note that Verific cannot guarantee that the created vhdl_yacc.cpp
// file will behave properly or even compile on various compilers.
//
// This file contains to %locations directive to keep track
// of accurate line/file info for all bison rules. This is not supported
// in bison 1.28, so use bison 1.35 or later to compile this file.
// Specifically, Verific tested this file only with Bison version 1.35 and 1.875.
//
// Use
//    bison -v -d -l -p vhdl -o vhdl_yacc.cpp vhdl.y
//     mv yeri_yacc.c vhdl_yacc.cpp
//     mv vhdl_yacc.h vhdl_yacc.h
// To create a valid cpp file for compilation with the flex-generated tokenizer
// and the rest of the analyzer code.
//
////////////////////////////////////////////////////////////////////////////

/* Bison forgot to rename this global var */
#define yylloc   vhdllloc
//#define YYDEBUG 1
#define YYLTYPE_IS_TRIVIAL 1 // to fix stack-grow problem in Bison 1.35

#include "VerificSystem.h"

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Strings.h"
#include "Message.h"

#include "VhdlTreeNode.h"
#include "vhdl_file.h"
#include "VhdlScope.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlSpecification.h"
#include "VhdlStatement.h"
#include "VhdlName.h"
#include "VhdlUnits.h"
#include "VhdlMisc.h"
#include "VhdlIdDef.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Control how deep the parser stack can grow before a stack overflow occurs
// (usually for right recursion). Overwrite the default bison value of 10000
// to set a higher depth.  VIPER #5111
#define YYMAXDEPTH 100000

// Declare main parser functions, since yacc forgets that due to renaming
extern void vhdlerror(const char *s) ;
extern int vhdllex(void) ;
extern int vhdlparse(void) ;

 //CARBON_BEGIN
 void* gLastDeclVhdlId; // the last declared vhdl id
 //CARBON_END

/******************************* Column/Line/File Info in rules **********************/

// Since 9/2004, we let flex and bison always work together to process location
// information (column/line/file info) of parsed tokens.
// This is done because of three reasons :
//    (1) in bison, we want accurate line info for the constructs (syntax rules) parsed
//        (not relying on just the last token parsed, but generate info based on the
//        sub-rules per bison rule).
//        The only way to do that is to enable location processing, and use the
//        location range info (yyloc in bison).
//    (2) We have compile switch VERIFIC_LINEFILE_INCLUDES_COLUMNS, which requires
//        us to process location (token/rule start/end column/line) info upon at compile time.
//    (3) Bison 1.375 and later do not allow compile-time control over location processing.

// Need these 4 variables to pick-up rule line/column info from YYLLOC_DEFAULT,
// since the variable that bison uses (yyloc) is a local variable, invisible outside 'parse()'.
static unsigned first_line = 0 ;
static unsigned first_column = 0 ;
static unsigned last_line = 0 ;
static unsigned last_column = 0 ;
static unsigned rhs_0_initialized = 0 ; // VIPER #6608

// Overwrite YYLLOC_DEFAULT (executed just before a rule action executes),
// to calculate and store the rule's column info so it can be
// picked up by constructors in the rule's actions.
// NOTE: This only works for Bison version 1.35 or newer.
// Fix Bison problem for rules with 0 args. Need to take yylloc from flex directly.

// Issue 2216 : do not use line/file info from empty sub-rules.
// We need some line/file info for empty subrules, so pick up next token as normal (yylloc)
// but set their last column/line to first column/line. This way, empty rules will
// never cover text into the next token (which is not part of the subrule).
// Bison 2.00 initializes columns to 0. We initialize to 1. Need to correct this here (test for 0 column).
// VIPER #6608: Update the empty rule line/column/file using Rhs[0] only when it is initialized.
// It is initialized after bison matches any rule, and whenever bison matches a rule it calls this macro.
// So, at the first call of this macro Rhs[0] may be uninitialized but after that it is initialized.
#define YYLLOC_DEFAULT(Current, Rhs, N)                 \
    if ((N)==0) {                                       \
        if (yylloc.first_column==0) { (yylloc.first_column)++ ; (yylloc.last_column)++ ; } \
        Current              = yylloc ;                 \
        if (rhs_0_initialized) {                        \
            Current.last_line    = (Rhs)[0].last_line ; \
            Current.last_column  = (Rhs)[0].last_column ;\
        } else {                                        \
            Current.last_line    = 1 ;                  \
            Current.last_column  = 1 ;                  \
        }                                               \
    } else {                                            \
        Current.first_line   = (Rhs)[1].first_line ;    \
        Current.first_column = (Rhs)[1].first_column ;  \
        Current.last_line    = (Rhs)[N].last_line ;     \
        Current.last_column  = (Rhs)[N].last_column ;   \
    }                                                   \
    first_line   = Current.first_line ;                 \
    first_column = Current.first_column ;               \
    last_line    = Current.last_line ;                  \
    last_column  = Current.last_column ;                \
    rhs_0_initialized  = 1 ; // Now it is initialized
    // set static variables, for pick-up of 'last rule location info'..

/******************************* Routines used locally **********************/

/* present_scope is static in VhdlTreeNode. Bit dirty, but works */

static void
vhdl_declare(VhdlIdDef *id)
{
    if (VhdlTreeNode::_present_scope) (void) VhdlTreeNode::_present_scope->Declare(id) ;
}

static void
vhdl_undeclare(VhdlIdDef *id)
{
    if (!VhdlTreeNode::_present_scope) return ;

    VhdlTreeNode::_present_scope->Undeclare(id) ;
    VhdlTreeNode::_present_scope->ResetPredefinedOpsForType(id) ;
}

// VIPER 2597 : Don't declare label identifiers defined in the design in a
// transparent scope. But we should declare label ids in transparent scope
// for proper implementation of exit, continue.
static void
vhdl_declare_label(VhdlIdDef *id)
{
    if (VhdlTreeNode::_present_scope) (void) VhdlTreeNode::_present_scope->DeclareLabel(id) ;
}
/* Static to get context clause into design unit */
static Array *context_clause = 0 ;

static int inside_design_unit = 0 ;
/* Static to follow VHDL-2008 LRM section 6.5.6.1. Set type of a generic
  just during identifier creation */
static unsigned in_generic_clause = 0 ;
/* Static to find the label of a statement while we are in it */
static VhdlIdDef *last_label = 0 ;
/* Static variables to maintain implicit loop sequence number and process sequence number*/
static unsigned loop_sequence_num = 0 ; // VIPER #7494
static unsigned process_sequence_num = 0 ; // VIPER #7494

void create_context_clause()
{
    if (context_clause) return ;

    VhdlTreeNode::_present_scope = 0 ;
    VhdlScope::Push() ;

    /* Now create a 'context_clause' tree node (an Array of context items). */
    context_clause = new Array() ;

    /* Define library names 'STD' and 'WORK' */
    Array *libs = new Array() ;

    /* Declare library identifier 'std' */
    VhdlIdDef *id = new VhdlLibraryId(Strings::save("std")) ;
    vhdl_declare(id) ;
    libs->InsertLast(id) ;

    /* Declare library identifier 'work' */
    id = new VhdlLibraryId(Strings::save("work")) ;
    vhdl_declare(id) ;
    libs->InsertLast(id) ;

    /* Set them in the parse tree */
    context_clause->InsertLast(new VhdlLibraryClause(libs, 1 /*implicitly declared*/)) ;
    return ;
}

void cleanup_context_clause()
{
    unsigned i ;
    VhdlTreeNode *node ;
    FOREACH_ARRAY_ITEM(context_clause, i, node) delete node ;
    delete context_clause ;
    context_clause = 0 ;
}

void cleanup_treenode_array(Array *tree_node_array)
{
    unsigned i ;
    VhdlTreeNode *node ;
    FOREACH_ARRAY_ITEM(tree_node_array, i, node) delete node ;
    delete tree_node_array ;
}

void cleanup_iddef_array(Array *iddef_node_array)
{
    unsigned i ;
    VhdlIdDef *node ;
    FOREACH_ARRAY_ITEM(iddef_node_array, i, node) {
        vhdl_undeclare(node) ;
        delete node ;
    }
    delete iddef_node_array ;
}
// VIPER #8329 : Delete declaration if id is declared in _present_scope
void cleanup_declnode_array(Array *decl_node_array)
{
    unsigned i, j ;
    VhdlDeclaration *decl ;
    unsigned can_delete_arr = 1 ;
    FOREACH_ARRAY_ITEM(decl_node_array, i, decl) {
        Array *ids = decl ? decl->GetIds(): 0 ;
        VhdlIdDef *id = decl ? decl->GetTypeId(): 0 ;
        unsigned can_delete = 1 ;
        if (ids) {
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (!id) continue ;
                if ((VhdlTreeNode::_present_scope) && !VhdlTreeNode::_present_scope->IsDeclaredHere(id)) {
                    can_delete = 0 ;
                    break ;
                }
            }
        } else if (id) {
            if ((VhdlTreeNode::_present_scope) && !VhdlTreeNode::_present_scope->IsDeclaredHere(id)) {
                can_delete = 0 ;
            }
        }
        if (can_delete) {
            delete decl ;
            decl_node_array->Insert(i, 0) ;
        } else {
            can_delete_arr = 0 ;
        }
    }
    if (can_delete_arr) delete decl_node_array ;
}
// VIPER #8429 : Delete statement is its label can be undeclared
void cleanup_statement_array(Array *stmt_array)
{
    unsigned i ;
    VhdlStatement *stmt ;
    unsigned can_delete_arr = 1 ;
    FOREACH_ARRAY_ITEM(stmt_array, i, stmt) {
        VhdlIdDef *label = stmt ? stmt->GetLabel(): 0 ;
        if (label && (VhdlTreeNode::_present_scope) && !VhdlTreeNode::_present_scope->IsDeclaredHere(label)) {
            can_delete_arr = 0 ;
            continue ;
        }
        delete stmt ;
        stmt_array->Insert(i, 0) ;
    }
    if (can_delete_arr) delete stmt_array ;
}
// VIPER #6229 : Update linefile of the given node to match current rule linefile
// When update_ending_linefile is set, it will update the ending position otherw ise
// it will not update the ending line/column of the linefile. We need this because
// if the last sub-rule is not present or it is comment the linefile is not set.
void update_linefile(VhdlTreeNode *node, unsigned update_ending_linefile = 1)
{
    linefile_type lf = (node) ? node->Linefile() : 0 ;
    if (!lf) return ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    lf->SetLeftCol(first_column) ;
    lf->SetLeftLine(first_line) ;
    if (update_ending_linefile) lf->SetRightCol(last_column) ;
    if (update_ending_linefile) lf->SetRightLine(last_line) ;
#else
    (void) update_ending_linefile; // to circumvent unused parameter warning
#endif
}

static void
add_pre_comments_to_node(VhdlTreeNode *node, Array *comments)
{
    if (!comments) return ; // Nothing to do
    if (node) {
        node->AddComments(comments) ; // Add the comments to the node
    } else {
        VhdlNode::DeleteComments(comments) ; // Delete comments to fix memory leak
    }
}

static void
add_post_comments_to_node(VhdlTreeNode *node, Array *comments)
{
    if (!comments) return ; // Nothing to do
    if (node) {
        vhdl_file::SetComments(node->ProcessPostComments(comments)) ;
    } else {
        VhdlNode::DeleteComments(comments) ; // Delete comments to fix memory leak
    }
}
// VIPER #7494 : Create label for argument specified concurrent statement
static void CreateAndAddImplicitLabel(VhdlStatement *stmt)
{
    if (!RuntimeFlags::GetVar("vhdl_create_implicit_labels")) return ;
    if (!stmt || stmt->GetLabel()) return ; // No need to add label

    // Create implicit label id
    char name[20] ;
    sprintf(name, "_P%d", process_sequence_num) ;

    process_sequence_num++ ;
    VhdlIdDef *implicit_label_id = new VhdlLabelId(Strings::save(name)) ;
    vhdl_declare_label(implicit_label_id) ;
    stmt->SetImplicitLabel(implicit_label_id) ;
}



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)

typedef union YYSTYPE {
    char      *str ;
    unsigned   unsigned_number ;
    double     real_number ;

#undef VN
#ifdef VERIFIC_NAMESPACE
#define VN Verific // Need to define this to get things working with Verific namespace active
#else
#define VN
#endif

    VN::Array     *array ;

    VN::VhdlDesignUnit *library_unit ;
    VN::VhdlDeclaration *declaration ;
    VN::VhdlStatement *statement ;
    VN::VhdlDiscreteRange *discrete_range ;
    VN::VhdlSubtypeIndication *subtype_indication ;
    VN::VhdlExpression *expression ;
    VN::VhdlName *name ;
    VN::VhdlDesignator *designator ;
    VN::VhdlIdRef *id ;
    VN::VhdlIdDef *id_def ;
    VN::VhdlTypeDef *type_def ;
    VN::VhdlSignature *signature ;
    VN::VhdlElementDecl *element_decl ;
    VN::VhdlPhysicalUnitDecl *physical_unit_decl ;
    VN::VhdlConfigurationItem *configuration_item ;
    VN::VhdlBlockConfiguration *block_configuration ;
    VN::VhdlComponentConfiguration *component_configuration ;
    VN::VhdlBindingIndication *binding_indication ;
    VN::VhdlSpecification *specification ;
    VN::VhdlInterfaceDecl *interface_decl ;
    VN::VhdlIterScheme *iter_scheme ;
    VN::VhdlOptions *options ;
    VN::VhdlDelayMechanism *delay_mechanism ;
    VN::VhdlBlockGenerics *block_generics ;
    VN::VhdlBlockPorts *block_ports ;
    VN::VhdlCaseStatementAlternative *case_statement_alternative ;
    VN::VhdlFileOpenInfo *file_open_info ;
    VN::VhdlEntityClassEntry *entity_class_entry ;
    VN::VhdlTreeNode *tree_node;
    struct { VN::Array *decls; VN::Array *stmts ; VN::VhdlScope *scope ; } generate_stmt_body ; // structure with two pointers
    struct {unsigned path_token; unsigned hat_count; VN::VhdlName *path ; } ext_path_name ;
} YYSTYPE;
/* Line 191 of yacc.c.  */

# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */


#if ! defined (yyoverflow) || YYERROR_VERBOSE

# ifndef YYFREE
#  define YYFREE free
# endif
# ifndef YYMALLOC
#  define YYMALLOC malloc
# endif

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   define YYSTACK_ALLOC alloca
#  endif
# else
#  if defined (alloca) || defined (_ALLOCA_H)
#   define YYSTACK_ALLOC alloca
#  else
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYLTYPE_IS_TRIVIAL) && YYLTYPE_IS_TRIVIAL \
             && defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
    YYLTYPE yyls;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  54
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3003

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  202
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  391
/* YYNRULES -- Number of rules. */
#define YYNRULES  886
/* YYNRULES -- Number of states. */
#define YYNSTATES  1456

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   3029

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   134,   135,   136,     2,     2,     2,
       2,     2,   137,   138,   139,   140,   141,   142,   143,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,   191,   192,
     193,   194,   195,   196,   197,   198,   199,   200,   201,     2
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short yyprhs[] =
{
       0,     0,     3,     4,     7,    10,    12,    15,    16,    19,
      23,    24,    27,    29,    31,    35,    39,    43,    47,    49,
      51,    53,    55,    57,    59,    61,    63,    65,    66,    76,
      78,    79,    81,    82,    95,    96,   102,   105,   111,   112,
     115,   117,   121,   123,   125,   127,   129,   131,   133,   135,
     137,   139,   141,   143,   145,   147,   149,   151,   153,   155,
     157,   158,   161,   162,   165,   167,   168,   182,   183,   186,
     188,   192,   194,   196,   198,   200,   202,   204,   206,   208,
     210,   212,   214,   216,   218,   220,   222,   224,   226,   228,
     230,   232,   233,   236,   238,   239,   240,   254,   255,   258,
     260,   264,   266,   268,   270,   274,   275,   284,   286,   287,
     290,   294,   295,   298,   300,   302,   304,   308,   309,   317,
     318,   327,   328,   331,   332,   343,   344,   346,   350,   351,
     354,   355,   358,   360,   364,   366,   368,   370,   372,   374,
     376,   378,   380,   382,   384,   386,   388,   390,   392,   394,
     396,   398,   400,   401,   412,   413,   416,   418,   422,   424,
     426,   428,   430,   432,   434,   436,   438,   440,   442,   444,
     446,   448,   450,   452,   454,   455,   464,   465,   472,   473,
     482,   483,   486,   489,   491,   501,   509,   510,   514,   515,
     517,   518,   521,   523,   527,   529,   531,   533,   535,   537,
     539,   541,   543,   545,   547,   549,   551,   553,   555,   557,
     559,   561,   562,   565,   567,   573,   574,   581,   585,   587,
     589,   591,   593,   595,   597,   599,   601,   603,   605,   607,
     612,   613,   620,   625,   629,   633,   641,   644,   649,   650,
     657,   658,   661,   662,   665,   669,   671,   675,   677,   679,
     681,   683,   691,   693,   695,   697,   699,   701,   703,   705,
     707,   709,   711,   713,   715,   717,   719,   721,   723,   724,
     733,   740,   748,   756,   763,   771,   772,   775,   776,   787,
     793,   801,   806,   812,   820,   823,   825,   827,   829,   831,
     833,   835,   837,   839,   841,   843,   845,   847,   849,   851,
     853,   855,   857,   863,   865,   869,   871,   873,   875,   877,
     885,   886,   888,   890,   892,   894,   895,   897,   899,   901,
     903,   905,   906,   908,   910,   913,   916,   917,   923,   924,
     933,   934,   937,   939,   941,   942,   950,   952,   958,   964,
     968,   972,   976,   980,   982,   985,   989,   994,   996,   998,
    1000,  1002,  1004,  1006,  1008,  1010,  1013,  1017,  1023,  1030,
    1032,  1035,  1038,  1041,  1045,  1047,  1049,  1051,  1059,  1069,
    1070,  1076,  1082,  1091,  1094,  1099,  1103,  1106,  1112,  1114,
    1116,  1120,  1123,  1128,  1132,  1134,  1138,  1147,  1159,  1161,
    1164,  1170,  1172,  1175,  1179,  1185,  1186,  1189,  1191,  1195,
    1197,  1198,  1201,  1202,  1205,  1211,  1217,  1227,  1229,  1231,
    1234,  1236,  1238,  1242,  1248,  1249,  1255,  1256,  1259,  1263,
    1269,  1271,  1273,  1275,  1279,  1281,  1283,  1287,  1288,  1289,
    1306,  1307,  1309,  1313,  1314,  1316,  1320,  1321,  1324,  1326,
    1327,  1330,  1332,  1333,  1337,  1338,  1351,  1352,  1356,  1360,
    1362,  1363,  1366,  1368,  1372,  1374,  1376,  1378,  1380,  1382,
    1384,  1386,  1388,  1390,  1392,  1394,  1396,  1398,  1400,  1402,
    1404,  1405,  1408,  1410,  1414,  1416,  1420,  1422,  1424,  1426,
    1428,  1430,  1432,  1434,  1436,  1438,  1440,  1442,  1444,  1446,
    1452,  1457,  1458,  1461,  1462,  1465,  1471,  1472,  1475,  1476,
    1479,  1480,  1483,  1485,  1487,  1489,  1495,  1502,  1512,  1521,
    1531,  1542,  1548,  1555,  1561,  1563,  1565,  1567,  1572,  1577,
    1583,  1584,  1590,  1591,  1594,  1603,  1611,  1615,  1621,  1624,
    1635,  1636,  1642,  1643,  1646,  1647,  1650,  1652,  1663,  1664,
    1666,  1668,  1671,  1673,  1678,  1679,  1688,  1689,  1692,  1697,
    1700,  1705,  1710,  1714,  1717,  1718,  1721,  1724,  1729,  1732,
    1735,  1737,  1738,  1740,  1741,  1743,  1749,  1755,  1757,  1761,
    1763,  1767,  1769,  1773,  1775,  1777,  1779,  1782,  1784,  1786,
    1791,  1792,  1795,  1800,  1801,  1804,  1806,  1809,  1812,  1816,
    1818,  1820,  1824,  1826,  1828,  1830,  1832,  1834,  1836,  1838,
    1840,  1842,  1844,  1848,  1851,  1854,  1856,  1858,  1860,  1864,
    1868,  1870,  1874,  1877,  1879,  1881,  1883,  1885,  1887,  1891,
    1895,  1899,  1903,  1907,  1911,  1915,  1919,  1923,  1927,  1929,
    1933,  1935,  1937,  1939,  1941,  1943,  1945,  1947,  1949,  1951,
    1953,  1955,  1957,  1959,  1963,  1965,  1967,  1969,  1971,  1973,
    1975,  1977,  1980,  1983,  1987,  1989,  1991,  1993,  1995,  1999,
    2001,  2003,  2005,  2007,  2009,  2011,  2013,  2015,  2017,  2019,
    2021,  2025,  2028,  2031,  2034,  2037,  2039,  2041,  2043,  2045,
    2047,  2049,  2052,  2054,  2056,  2058,  2062,  2065,  2068,  2070,
    2074,  2076,  2080,  2082,  2084,  2088,  2092,  2094,  2096,  2098,
    2100,  2101,  2103,  2104,  2106,  2107,  2109,  2110,  2112,  2113,
    2115,  2116,  2118,  2119,  2121,  2122,  2124,  2125,  2127,  2128,
    2131,  2132,  2134,  2136,  2137,  2139,  2141,  2142,  2144,  2146,
    2147,  2149,  2150,  2152,  2153,  2155,  2156,  2158,  2159,  2161,
    2162,  2164,  2165,  2167,  2168,  2170,  2171,  2173,  2174,  2177,
    2179,  2183,  2185,  2187,  2192,  2194,  2198,  2200,  2201,  2204,
    2206,  2208,  2212,  2214,  2216,  2220,  2222,  2224,  2228,  2230,
    2232,  2236,  2238,  2240,  2243,  2245,  2247,  2251,  2253,  2255,
    2259,  2261,  2263,  2267,  2269,  2273,  2275,  2277,  2281,  2283,
    2285,  2289,  2291,  2293,  2297,  2299,  2301,  2305,  2307,  2310,
    2312,  2314,  2316,  2319,  2321,  2323,  2325,  2327,  2329,  2331,
    2333,  2335,  2337,  2341,  2345,  2352,  2356,  2361,  2368,  2375,
    2382,  2384,  2386,  2388,  2391,  2394,  2397,  2398,  2401,  2403,
    2407,  2409,  2411,  2413,  2415,  2417,  2419,  2422,  2424,  2426,
    2428,  2430,  2432,  2435,  2437,  2439,  2441,  2443,  2445,  2447,
    2449,  2453,  2455,  2457,  2459,  2461,  2463,  2465,  2467,  2469,
    2471,  2473,  2475,  2477,  2479,  2481,  2483,  2485,  2487,  2489,
    2491,  2493,  2495,  2497,  2499,  2501,  2503,  2505,  2507,  2509,
    2511,  2513,  2515,  2517,  2519,  2520,  2521
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short yyrhs[] =
{
     203,     0,    -1,    -1,   203,   204,    -1,   165,   472,    -1,
       1,    -1,   205,   211,    -1,    -1,   205,   206,    -1,   591,
     208,   592,    -1,    -1,   207,   206,    -1,   209,    -1,   210,
      -1,   153,   518,    14,    -1,    69,   529,    14,    -1,   118,
     518,    14,    -1,   591,   212,   592,    -1,   213,    -1,   214,
      -1,   219,    -1,   237,    -1,   256,    -1,   268,    -1,   215,
      -1,   231,    -1,   263,    -1,    -1,   153,   217,    67,   216,
     207,    52,   218,   495,    14,    -1,   130,    -1,    -1,   153,
      -1,    -1,    53,   586,    67,   220,   498,   499,   225,   228,
      52,   509,   495,    14,    -1,    -1,    59,     5,   222,   519,
       6,    -1,   221,    14,    -1,    88,     5,   519,     6,    14,
      -1,    -1,   225,   226,    -1,     1,    -1,   591,   227,   592,
      -1,   274,    -1,   276,    -1,   284,    -1,   283,    -1,   308,
      -1,   309,    -1,   310,    -1,   311,    -1,   312,    -1,   316,
      -1,   317,    -1,   319,    -1,   210,    -1,   320,    -1,   323,
      -1,   268,    -1,   256,    -1,   263,    -1,    -1,    39,   229,
      -1,    -1,   229,   230,    -1,   344,    -1,    -1,    35,   587,
      81,   562,    67,   232,   233,    39,   236,    52,   508,   495,
      14,    -1,    -1,   233,   234,    -1,     1,    -1,   591,   235,
     592,    -1,   274,    -1,   276,    -1,   284,    -1,   283,    -1,
     308,    -1,   309,    -1,   310,    -1,   311,    -1,   312,    -1,
     314,    -1,   316,    -1,   317,    -1,   318,    -1,   319,    -1,
     210,    -1,   320,    -1,   323,    -1,   268,    -1,   256,    -1,
     263,    -1,    -1,   236,   344,    -1,     1,    -1,    -1,    -1,
      46,   588,    81,   238,   562,    67,   239,   240,   243,    52,
     511,   495,    14,    -1,    -1,   240,   241,    -1,     1,    -1,
     591,   242,   592,    -1,   210,    -1,   317,    -1,   323,    -1,
     591,   244,   592,    -1,    -1,    56,   246,   245,   247,   249,
      52,    56,    14,    -1,   541,    -1,    -1,   247,   248,    -1,
     591,   210,   592,    -1,    -1,   249,   250,    -1,     1,    -1,
     243,    -1,   251,    -1,   591,   252,   592,    -1,    -1,    56,
     342,   255,   253,    52,    56,    14,    -1,    -1,    56,   342,
     255,   254,   243,    52,    56,    14,    -1,    -1,   440,    14,
      -1,    -1,    87,   589,    67,   257,   258,   260,    52,   510,
     495,    14,    -1,    -1,   223,    -1,   223,   444,    14,    -1,
      -1,   221,   442,    -1,    -1,   260,   261,    -1,     1,    -1,
     591,   262,   592,    -1,   274,    -1,   284,    -1,   283,    -1,
     308,    -1,   309,    -1,   310,    -1,   311,    -1,   312,    -1,
     314,    -1,   316,    -1,   317,    -1,   319,    -1,   210,    -1,
     320,    -1,   323,    -1,   276,    -1,   268,    -1,   256,    -1,
      -1,    87,    41,   590,    67,   264,   265,    52,   504,   495,
      14,    -1,    -1,   265,   266,    -1,     1,    -1,   591,   267,
     592,    -1,   274,    -1,   276,    -1,   284,    -1,   283,    -1,
     308,    -1,   316,    -1,   317,    -1,   310,    -1,   311,    -1,
     312,    -1,   210,    -1,   320,    -1,   323,    -1,   268,    -1,
     256,    -1,   263,    -1,    -1,    87,   589,    67,    76,   269,
     564,   442,    14,    -1,    -1,    90,   582,   271,   259,   278,
     277,    -1,    -1,   505,    57,   581,   272,   259,   278,   277,
     273,    -1,    -1,    99,   561,    -1,   270,    14,    -1,    39,
      -1,   270,    67,   279,   275,   282,    52,   506,   500,    14,
      -1,   270,    67,    76,   565,   497,   442,    14,    -1,    -1,
       5,   519,     6,    -1,    -1,   155,    -1,    -1,   279,   280,
      -1,     1,    -1,   591,   281,   592,    -1,   274,    -1,   276,
      -1,   284,    -1,   283,    -1,   308,    -1,   309,    -1,   310,
      -1,   311,    -1,   312,    -1,   316,    -1,   317,    -1,   210,
      -1,   320,    -1,   323,    -1,   268,    -1,   256,    -1,   263,
      -1,    -1,   282,   400,    -1,     1,    -1,   110,   570,    67,
     464,    14,    -1,    -1,   114,   571,   285,    67,   286,    14,
      -1,   114,   571,    14,    -1,   287,    -1,   288,    -1,   535,
      -1,   293,    -1,   294,    -1,   457,    -1,   295,    -1,   289,
      -1,   290,    -1,   298,    -1,   306,    -1,    36,   458,    81,
     464,    -1,    -1,    94,   291,   526,    52,    94,   495,    -1,
     528,    13,   464,    14,    -1,    55,    81,   561,    -1,     5,
     520,     6,    -1,   457,   116,   296,   521,    52,   116,   495,
      -1,   566,    14,    -1,   566,    16,   537,    14,    -1,    -1,
     138,   299,   300,    52,   138,   495,    -1,    -1,   300,   302,
      -1,    -1,   301,   303,    -1,   591,   304,   592,    -1,     1,
      -1,   591,   305,   592,    -1,     1,    -1,   274,    -1,   210,
      -1,   317,    -1,   270,    67,    76,   565,   497,   442,    14,
      -1,   274,    -1,   276,    -1,   284,    -1,   283,    -1,   308,
      -1,   310,    -1,   311,    -1,   312,    -1,   316,    -1,   317,
      -1,   210,    -1,   320,    -1,   323,    -1,   268,    -1,   256,
      -1,   263,    -1,    -1,   138,    41,   307,   301,    52,   138,
      41,   495,    -1,    47,   532,    13,   464,   517,    14,    -1,
     104,   531,    13,   464,   507,   517,    14,    -1,   514,   119,
     533,    13,   464,   517,    14,    -1,    55,   534,    13,   464,
     501,    14,    -1,    32,   584,   313,    67,   541,   497,    14,
      -1,    -1,    13,   464,    -1,    -1,    45,   577,   512,   315,
     498,   499,    52,    45,   495,    14,    -1,    38,   578,    13,
     561,    14,    -1,    38,   554,    81,   341,    67,   472,    14,
      -1,    56,   342,   440,    14,    -1,    48,   343,    31,   472,
      14,    -1,    60,   579,    67,     5,   522,     6,    14,    -1,
     322,   513,    -1,    53,    -1,    35,    -1,    46,    -1,    90,
      -1,    57,    -1,    87,    -1,   114,    -1,   110,    -1,    47,
      -1,   104,    -1,   119,    -1,    45,    -1,    68,    -1,    71,
      -1,   116,    -1,    60,    -1,    55,    -1,    60,   580,    13,
     324,    14,    -1,   541,    -1,   592,   326,   592,    -1,   327,
      -1,   331,    -1,   332,    -1,   338,    -1,   328,   530,    13,
     329,   464,   507,   517,    -1,    -1,    47,    -1,   104,    -1,
     119,    -1,    55,    -1,    -1,    64,    -1,    86,    -1,    66,
      -1,    42,    -1,    70,    -1,    -1,    64,    -1,    86,    -1,
     114,   572,    -1,   333,   336,    -1,    -1,    90,   582,   334,
     278,   277,    -1,    -1,   505,    57,   581,   335,   278,   277,
      99,   561,    -1,    -1,    67,   337,    -1,   541,    -1,    28,
      -1,    -1,    87,   589,    67,    76,   339,   541,   340,    -1,
     444,    -1,    59,    73,     5,    28,     6,    -1,    59,    73,
       5,   156,     6,    -1,   523,    13,   322,    -1,   523,    13,
     541,    -1,   523,    13,   561,    -1,   591,   345,   592,    -1,
     346,    -1,    89,   346,    -1,   585,    13,   346,    -1,   585,
      13,    89,   346,    -1,   384,    -1,   348,    -1,   350,    -1,
     347,    -1,   392,    -1,   403,    -1,   372,    -1,   373,    -1,
     541,    14,    -1,    89,   541,    14,    -1,   585,    13,    89,
     541,    14,    -1,   585,    13,   349,   442,   443,    14,    -1,
     541,    -1,    45,   541,    -1,    53,   541,    -1,    46,   541,
      -1,   585,    13,   351,    -1,   352,    -1,   355,    -1,   363,
      -1,   353,    58,   367,    52,    58,   495,    14,    -1,   353,
      58,   367,    52,    14,    52,    58,   495,    14,    -1,    -1,
      56,   354,   563,    64,   462,    -1,   357,    52,    58,   495,
      14,    -1,   357,    52,   495,    14,    52,    58,   495,    14,
      -1,    62,   472,    -1,    62,   563,    13,   472,    -1,   356,
      58,   367,    -1,   357,   358,    -1,   357,    52,   495,    14,
     358,    -1,   359,    -1,   361,    -1,   360,    58,   367,    -1,
      51,   472,    -1,    51,   563,    13,   472,    -1,   362,    58,
     367,    -1,    50,    -1,    50,   563,    13,    -1,    44,   472,
      58,   364,    52,    58,   495,    14,    -1,    44,   472,    58,
     364,    52,   495,    14,    52,    58,   495,    14,    -1,   365,
      -1,   364,   365,    -1,   364,    52,   495,    14,   365,    -1,
       1,    -1,   366,   367,    -1,   121,   493,    22,    -1,   121,
     563,    13,   493,    22,    -1,    -1,   368,   369,    -1,   371,
      -1,   370,    39,   371,    -1,     1,    -1,    -1,   370,   234,
      -1,    -1,   371,   344,    -1,   374,    27,   375,   377,    14,
      -1,   374,    27,   375,   381,    14,    -1,   123,   472,   102,
     429,   374,    27,   375,   380,    14,    -1,   541,    -1,   489,
      -1,   516,   502,    -1,   113,    -1,    65,    -1,    96,   472,
      65,    -1,   381,   121,   472,   378,   379,    -1,    -1,   378,
      50,   381,   121,   472,    -1,    -1,    50,   381,    -1,   381,
     121,   493,    -1,   380,     9,   381,   121,   493,    -1,   382,
      -1,   115,    -1,   383,    -1,   382,     9,   383,    -1,     1,
      -1,   472,    -1,   472,    31,   472,    -1,    -1,    -1,   585,
      13,    40,   391,   512,   385,   387,   388,   386,   389,    39,
     390,    52,    40,   495,    14,    -1,    -1,   223,    -1,   223,
     444,    14,    -1,    -1,   224,    -1,   224,   445,    14,    -1,
      -1,   389,   234,    -1,     1,    -1,    -1,   390,   344,    -1,
       1,    -1,    -1,     5,   472,     6,    -1,    -1,    91,   394,
     512,   393,   396,   275,   399,    52,   515,    91,   495,    14,
      -1,    -1,     5,   395,     6,    -1,     5,    33,     6,    -1,
     524,    -1,    -1,   396,   397,    -1,     1,    -1,   591,   398,
     592,    -1,   274,    -1,   276,    -1,   284,    -1,   283,    -1,
     308,    -1,   310,    -1,   311,    -1,   312,    -1,   316,    -1,
     317,    -1,   210,    -1,   320,    -1,   323,    -1,   256,    -1,
     268,    -1,   263,    -1,    -1,   399,   400,    -1,     1,    -1,
     591,   401,   592,    -1,   402,    -1,   585,    13,   402,    -1,
     407,    -1,   403,    -1,   404,    -1,   411,    -1,   415,    -1,
     423,    -1,   424,    -1,   428,    -1,   432,    -1,   435,    -1,
     436,    -1,   437,    -1,   438,    -1,    37,   472,   405,   406,
      14,    -1,    98,   472,   406,    14,    -1,    -1,    98,   472,
      -1,    -1,   103,   472,    -1,   120,   408,   409,   410,    14,
      -1,    -1,    82,   395,    -1,    -1,   117,   472,    -1,    -1,
      56,   472,    -1,   414,    -1,   412,    -1,   413,    -1,   374,
      27,   502,   377,    14,    -1,   374,    27,   159,   330,   418,
      14,    -1,   123,   472,   102,   157,   374,    27,   502,   380,
      14,    -1,   123,   472,   102,   374,    27,   502,   380,    14,
      -1,   123,   472,   102,   374,    27,   159,   330,   422,    14,
      -1,   123,   472,   102,   157,   374,    27,   159,   330,   422,
      14,    -1,   374,    27,   502,   381,    14,    -1,   374,    27,
     159,   330,   472,    14,    -1,   374,    27,   160,   330,    14,
      -1,   416,    -1,   417,    -1,   421,    -1,   374,    24,   472,
      14,    -1,   374,    24,   418,    14,    -1,   472,   121,   472,
     419,   420,    -1,    -1,   419,    50,   472,   121,   472,    -1,
      -1,    50,   472,    -1,   123,   472,   102,   157,   374,    24,
     422,    14,    -1,   123,   472,   102,   374,    24,   422,    14,
      -1,   472,   121,   493,    -1,   422,     9,   472,   121,   493,
      -1,   541,    14,    -1,    62,   472,   111,   427,   425,   426,
      52,    62,   495,    14,    -1,    -1,   425,    51,   472,   111,
     427,    -1,    -1,    50,   427,    -1,    -1,   427,   400,    -1,
       1,    -1,    44,   429,   472,    67,   430,    52,    44,   429,
     495,    14,    -1,    -1,   157,    -1,   431,    -1,   430,   431,
      -1,     1,    -1,   121,   493,    22,   427,    -1,    -1,   433,
     434,    72,   427,    52,    72,   495,    14,    -1,    -1,   122,
     472,    -1,    56,   563,    64,   462,    -1,    56,     1,    -1,
      77,   495,   439,    14,    -1,    54,   495,   439,    14,    -1,
      99,   496,    14,    -1,    80,    14,    -1,    -1,   121,   472,
      -1,   442,   443,    -1,   118,   441,   442,   443,    -1,    53,
     541,    -1,    46,   541,    -1,    83,    -1,    -1,   444,    -1,
      -1,   445,    -1,    59,    73,     5,   446,     6,    -1,    88,
      73,     5,   446,     6,    -1,   447,    -1,   446,     9,   447,
      -1,     1,    -1,   591,   448,   592,    -1,   450,    -1,   449,
      22,   450,    -1,   463,    -1,   541,    -1,   451,    -1,    65,
     450,    -1,   472,    -1,    83,    -1,    20,   503,   453,    21,
      -1,    -1,    99,   561,    -1,   455,    67,   329,   456,    -1,
      -1,    83,   472,    -1,   472,    -1,    93,   459,    -1,    93,
      28,    -1,     5,   525,     6,    -1,   543,    -1,   460,    -1,
     481,   461,   481,    -1,   112,    -1,    49,    -1,   541,    -1,
     466,    -1,   460,    -1,   466,    -1,   460,    -1,   541,    -1,
     465,    -1,   466,    -1,   467,   561,   457,    -1,   467,   561,
      -1,   561,   457,    -1,   563,    -1,   468,    -1,   469,    -1,
       5,   467,     6,    -1,     5,   470,     6,    -1,   471,    -1,
     470,     9,   471,    -1,   563,   467,    -1,   477,    -1,   473,
      -1,   474,    -1,   475,    -1,   476,    -1,   477,    75,   477,
      -1,   477,    78,   477,    -1,   477,    34,   477,    -1,   473,
      34,   477,    -1,   477,    84,   477,    -1,   474,    84,   477,
      -1,   477,   125,   477,    -1,   475,   125,   477,    -1,   477,
     124,   477,    -1,   476,   124,   477,    -1,   479,    -1,   479,
     478,   479,    -1,    16,    -1,    25,    -1,    17,    -1,    15,
      -1,    26,    -1,    27,    -1,   147,    -1,   148,    -1,   149,
      -1,   150,    -1,   151,    -1,   152,    -1,   481,    -1,   481,
     480,   481,    -1,   107,    -1,   109,    -1,   106,    -1,   108,
      -1,   100,    -1,   101,    -1,   483,    -1,     8,   483,    -1,
      10,   483,    -1,   481,   482,   483,    -1,     8,    -1,    10,
      -1,     3,    -1,   486,    -1,   483,   484,   486,    -1,     7,
      -1,    12,    -1,    74,    -1,    97,    -1,    34,    -1,    84,
      -1,   125,    -1,   124,    -1,    75,    -1,    78,    -1,   487,
      -1,   487,    23,   487,    -1,    29,   487,    -1,    79,   487,
      -1,   146,   487,    -1,   485,   487,    -1,   541,    -1,   488,
      -1,   489,    -1,   490,    -1,   542,    -1,   536,    -1,   536,
     541,    -1,   540,    -1,   538,    -1,    80,    -1,     5,   491,
       6,    -1,    76,   541,    -1,    76,   542,    -1,   492,    -1,
     491,     9,   492,    -1,     1,    -1,   493,    22,   472,    -1,
     472,    -1,   494,    -1,   493,    18,   494,    -1,   493,    19,
     494,    -1,     1,    -1,   481,    -1,   463,    -1,    85,    -1,
      -1,   563,    -1,    -1,   472,    -1,    -1,   452,    -1,    -1,
     223,    -1,    -1,   224,    -1,    -1,   553,    -1,    -1,   454,
      -1,    -1,   376,    -1,    -1,   527,    -1,    -1,    87,    41,
      -1,    -1,    92,    -1,    63,    -1,    -1,    90,    -1,    57,
      -1,    -1,    43,    -1,    95,    -1,    -1,    35,    -1,    -1,
      53,    -1,    -1,    87,    -1,    -1,    46,    -1,    -1,    67,
      -1,    -1,    28,    -1,    -1,   105,    -1,    -1,    89,    -1,
      -1,    61,    -1,    -1,    24,   472,    -1,   544,    -1,   518,
       9,   544,    -1,     1,    -1,   325,    -1,   519,    14,   592,
     325,    -1,   583,    -1,   520,     9,   583,    -1,     1,    -1,
      -1,   521,   297,    -1,     1,    -1,   321,    -1,   522,     9,
     321,    -1,     1,    -1,   555,    -1,   523,     9,   555,    -1,
       1,    -1,   541,    -1,   524,     9,   541,    -1,     1,    -1,
     462,    -1,   525,     9,   462,    -1,     1,    -1,   292,    -1,
     526,   292,    -1,     1,    -1,   561,    -1,   527,     9,   561,
      -1,     1,    -1,   567,    -1,   528,     9,   567,    -1,     1,
      -1,   568,    -1,   529,     9,   568,    -1,   569,    -1,   530,
       9,   569,    -1,     1,    -1,   574,    -1,   531,     9,   574,
      -1,     1,    -1,   573,    -1,   532,     9,   573,    -1,     1,
      -1,   575,    -1,   533,     9,   575,    -1,     1,    -1,   576,
      -1,   534,     9,   576,    -1,     1,    -1,    30,   464,    -1,
     126,    -1,   127,    -1,   541,    -1,   536,   541,    -1,   132,
      -1,   128,    -1,   129,    -1,   563,    -1,   539,    -1,   544,
      -1,   543,    -1,   545,    -1,   546,    -1,   556,     4,   489,
      -1,   556,     4,   554,    -1,   556,     4,   554,     5,   446,
       6,    -1,   557,    11,   559,    -1,   556,     5,   446,     6,
      -1,   161,    47,   547,    13,   464,   162,    -1,   161,   104,
     547,    13,   464,   162,    -1,   161,   119,   547,    13,   464,
     162,    -1,   548,    -1,   549,    -1,   550,    -1,   163,   541,
      -1,    11,   541,    -1,   551,   541,    -1,    -1,   552,    11,
      -1,   164,    -1,   552,    11,   164,    -1,   563,    -1,   560,
      -1,   538,    -1,   563,    -1,    93,    -1,   110,    -1,   541,
     497,    -1,    85,    -1,    33,    -1,   557,    -1,   558,    -1,
     541,    -1,   541,   452,    -1,   563,    -1,   538,    -1,   560,
      -1,    33,    -1,   539,    -1,   541,    -1,   563,    -1,   563,
      11,   563,    -1,   130,    -1,   541,    -1,   563,    -1,   128,
      -1,   130,    -1,   130,    -1,   130,    -1,   130,    -1,   130,
      -1,   130,    -1,   130,    -1,   130,    -1,   130,    -1,   130,
      -1,   130,    -1,   130,    -1,   130,    -1,   130,    -1,   130,
      -1,   130,    -1,   128,    -1,   130,    -1,   130,    -1,   132,
      -1,   130,    -1,   128,    -1,   132,    -1,   130,    -1,   130,
      -1,   130,    -1,   130,    -1,   130,    -1,   130,    -1,    -1,
      -1,   134,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short yyrline[] =
{
       0,  1223,  1223,  1224,  1225,  1230,  1233,  1247,  1248,  1252,
    1260,  1261,  1266,  1267,  1270,  1277,  1281,  1287,  1295,  1296,
    1299,  1300,  1301,  1302,  1303,  1306,  1307,  1313,  1312,  1354,
    1358,  1359,  1365,  1364,  1414,  1414,  1433,  1439,  1461,  1462,
    1464,  1467,  1477,  1478,  1479,  1480,  1481,  1482,  1483,  1484,
    1485,  1486,  1487,  1488,  1489,  1490,  1491,  1492,  1493,  1494,
    1497,  1498,  1503,  1504,  1508,  1516,  1515,  1598,  1599,  1601,
    1604,  1614,  1615,  1616,  1617,  1618,  1619,  1620,  1621,  1622,
    1623,  1624,  1625,  1626,  1627,  1628,  1629,  1630,  1631,  1632,
    1633,  1636,  1637,  1639,  1645,  1667,  1644,  1709,  1710,  1712,
    1715,  1723,  1724,  1725,  1728,  1737,  1736,  1755,  1760,  1761,
    1765,  1773,  1774,  1776,  1779,  1781,  1785,  1795,  1794,  1811,
    1810,  1863,  1864,  1876,  1875,  1977,  1978,  1982,  1989,  1990,
    1998,  1999,  2001,  2004,  2013,  2014,  2015,  2016,  2017,  2018,
    2019,  2020,  2021,  2022,  2023,  2024,  2025,  2026,  2027,  2028,
    2029,  2030,  2036,  2035,  2122,  2123,  2125,  2128,  2138,  2139,
    2140,  2141,  2142,  2146,  2153,  2160,  2161,  2162,  2163,  2164,
    2165,  2166,  2167,  2168,  2172,  2171,  2230,  2230,  2265,  2265,
    2303,  2304,  2307,  2320,  2326,  2353,  2365,  2366,  2384,  2385,
    2388,  2389,  2391,  2394,  2404,  2405,  2406,  2407,  2408,  2409,
    2410,  2411,  2412,  2413,  2414,  2415,  2416,  2417,  2418,  2419,
    2420,  2423,  2424,  2426,  2431,  2436,  2435,  2444,  2448,  2449,
    2450,  2451,  2454,  2455,  2456,  2459,  2460,  2461,  2462,  2465,
    2474,  2474,  2483,  2487,  2491,  2495,  2509,  2515,  2522,  2521,
    2541,  2542,  2546,  2547,  2551,  2557,  2560,  2567,  2571,  2572,
    2573,  2575,  2588,  2589,  2590,  2591,  2592,  2593,  2594,  2595,
    2596,  2597,  2598,  2599,  2600,  2601,  2602,  2603,  2608,  2607,
    2635,  2640,  2645,  2650,  2655,  2665,  2666,  2670,  2670,  2680,
    2684,  2689,  2693,  2699,  2704,  2708,  2709,  2710,  2711,  2712,
    2713,  2714,  2715,  2716,  2717,  2718,  2719,  2720,  2721,  2722,
    2723,  2724,  2727,  2731,  2742,  2749,  2750,  2751,  2752,  2755,
    2760,  2761,  2762,  2763,  2764,  2767,  2768,  2769,  2770,  2771,
    2772,  2775,  2776,  2777,  2780,  2791,  2802,  2802,  2823,  2823,
    2847,  2848,  2851,  2852,  2856,  2855,  2880,  2881,  2883,  2887,
    2891,  2895,  2901,  2909,  2911,  2913,  2915,  2917,  2918,  2919,
    2920,  2923,  2924,  2925,  2926,  2929,  2931,  2933,  2958,  2990,
    2991,  2993,  2995,  2999,  3008,  3009,  3010,  3021,  3027,  3036,
    3036,  3040,  3043,  3060,  3062,  3073,  3075,  3084,  3095,  3096,
    3099,  3103,  3105,  3109,  3113,  3115,  3119,  3123,  3134,  3136,
    3139,  3146,  3150,  3154,  3156,  3161,  3161,  3178,  3180,  3184,
    3190,  3191,  3195,  3196,  3233,  3235,  3246,  3251,  3252,  3255,
    3259,  3261,  3263,  3269,  3278,  3279,  3284,  3285,  3289,  3293,
    3299,  3300,  3304,  3306,  3308,  3311,  3312,  3319,  3345,  3318,
    3370,  3371,  3373,  3377,  3378,  3380,  3384,  3385,  3387,  3390,
    3391,  3393,  3396,  3397,  3404,  3403,  3421,  3422,  3424,  3434,
    3437,  3438,  3440,  3443,  3452,  3453,  3454,  3455,  3456,  3457,
    3458,  3459,  3460,  3461,  3462,  3463,  3464,  3465,  3466,  3467,
    3470,  3471,  3473,  3478,  3486,  3488,  3492,  3493,  3494,  3495,
    3496,  3497,  3498,  3499,  3500,  3501,  3502,  3503,  3504,  3508,
    3512,  3516,  3517,  3520,  3521,  3524,  3529,  3530,  3533,  3534,
    3537,  3538,  3541,  3542,  3547,  3555,  3558,  3562,  3567,  3572,
    3578,  3586,  3589,  3595,  3602,  3603,  3608,  3615,  3619,  3623,
    3631,  3632,  3637,  3638,  3642,  3644,  3649,  3653,  3660,  3664,
    3676,  3677,  3681,  3682,  3686,  3687,  3689,  3692,  3706,  3707,
    3710,  3712,  3714,  3718,  3722,  3722,  3760,  3761,  3763,  3765,
    3769,  3773,  3777,  3781,  3785,  3786,  3792,  3794,  3798,  3801,
    3803,  3807,  3808,  3811,  3812,  3815,  3819,  3823,  3825,  3827,
    3830,  3839,  3840,  3842,  3846,  3850,  3851,  3859,  3860,  3863,
    3867,  3868,  3872,  3880,  3881,  3884,  3889,  3891,  3896,  3900,
    3901,  3904,  3908,  3909,  3918,  3919,  3920,  3923,  3924,  3927,
    3928,  3929,  3932,  3934,  3937,  3948,  3949,  3950,  3953,  3960,
    3966,  3968,  3972,  3980,  3981,  3982,  3983,  3984,  3985,  3987,
    3991,  3993,  3997,  3999,  4003,  4005,  4009,  4011,  4015,  4016,
    4020,  4021,  4022,  4023,  4024,  4025,  4026,  4027,  4028,  4029,
    4030,  4031,  4034,  4035,  4039,  4040,  4041,  4042,  4043,  4044,
    4047,  4048,  4050,  4052,  4056,  4057,  4058,  4061,  4062,  4066,
    4067,  4068,  4069,  4072,  4073,  4074,  4075,  4076,  4077,  4080,
    4081,  4083,  4085,  4087,  4089,  4097,  4098,  4099,  4100,  4101,
    4110,  4111,  4113,  4114,  4115,  4118,  4141,  4143,  4151,  4153,
    4155,  4158,  4160,  4163,  4165,  4167,  4169,  4172,  4173,  4174,
    4180,  4181,  4183,  4184,  4186,  4187,  4189,  4190,  4192,  4193,
    4195,  4196,  4198,  4199,  4201,  4202,  4204,  4205,  4208,  4209,
    4211,  4212,  4213,  4215,  4216,  4217,  4219,  4220,  4221,  4223,
    4224,  4226,  4227,  4229,  4230,  4232,  4233,  4235,  4236,  4238,
    4239,  4241,  4242,  4244,  4245,  4247,  4248,  4250,  4251,  4256,
    4258,  4260,  4263,  4265,  4297,  4299,  4301,  4304,  4305,  4307,
    4310,  4312,  4314,  4317,  4319,  4321,  4324,  4326,  4328,  4331,
    4333,  4335,  4338,  4340,  4342,  4345,  4347,  4349,  4352,  4354,
    4356,  4359,  4361,  4365,  4367,  4369,  4372,  4374,  4376,  4379,
    4381,  4383,  4386,  4388,  4390,  4393,  4395,  4397,  4400,  4406,
    4408,  4412,  4414,  4418,  4422,  4426,  4432,  4433,  4437,  4438,
    4439,  4442,  4446,  4450,  4453,  4474,  4478,  4489,  4492,  4495,
    4499,  4500,  4501,  4504,  4508,  4512,  4516,  4517,  4521,  4523,
    4527,  4528,  4529,  4532,  4533,  4535,  4539,  4547,  4548,  4551,
    4555,  4558,  4561,  4565,  4566,  4567,  4568,  4572,  4575,  4578,
    4580,  4593,  4597,  4600,  4601,  4604,  4607,  4610,  4613,  4617,
    4620,  4633,  4646,  4649,  4655,  4658,  4661,  4664,  4667,  4670,
    4674,  4676,  4680,  4684,  4686,  4690,  4692,  4694,  4698,  4705,
    4712,  4716,  4720,  4724,  4732,  4737,  4738
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "\"&\"", "VHDL_TICK", "\"(\"", "\")\"",
  "VHDL_STAR", "VHDL_PLUS", "\",\"", "VHDL_MINUS", "VHDL_DOT",
  "VHDL_SLASH", "\":\"", "\";\"", "\"<\"", "\"=\"", "\">\"", "\"|\"",
  "\"!\"", "\"[\"", "\"]\"", "VHDL_ARROW", "VHDL_EXPONENT",
  "VHDL_VARASSIGN", "VHDL_NEQUAL", "VHDL_GEQUAL", "VHDL_SEQUAL",
  "VHDL_BOX", "VHDL_abs", "VHDL_access", "VHDL_after", "VHDL_alias",
  "VHDL_all", "\"and\"", "VHDL_architecture", "VHDL_array", "\"assert\"",
  "VHDL_attribute", "VHDL_begin", "VHDL_block", "VHDL_body", "VHDL_buffer",
  "VHDL_bus", "VHDL_case", "VHDL_component", "VHDL_configuration",
  "VHDL_constant", "VHDL_disconnect", "VHDL_downto", "VHDL_else",
  "VHDL_elsif", "VHDL_end", "VHDL_entity", "VHDL_exit", "VHDL_file",
  "VHDL_for", "VHDL_function", "VHDL_generate", "VHDL_generic",
  "VHDL_group", "VHDL_guarded", "VHDL_if", "VHDL_impure", "\"in\"",
  "VHDL_inertial", "VHDL_inout", "\"is\"", "VHDL_label", "VHDL_library",
  "VHDL_linkage", "VHDL_literal", "VHDL_loop", "VHDL_map", "VHDL_mod",
  "\"nand\"", "VHDL_new", "\"next\"", "\"nor\"", "\"not\"", "VHDL_null",
  "VHDL_of", "VHDL_on", "VHDL_open", "\"or\"", "VHDL_others", "VHDL_out",
  "VHDL_package", "VHDL_port", "VHDL_postponed", "VHDL_procedure",
  "VHDL_process", "VHDL_pure", "VHDL_range", "VHDL_record",
  "VHDL_register", "VHDL_reject", "VHDL_rem", "VHDL_report", "VHDL_return",
  "VHDL_rol", "VHDL_ror", "VHDL_select", "VHDL_severity", "VHDL_signal",
  "VHDL_shared", "VHDL_sla", "VHDL_sll", "VHDL_sra", "VHDL_srl",
  "VHDL_subtype", "VHDL_then", "\"to\"", "VHDL_transport", "VHDL_type",
  "VHDL_unaffected", "VHDL_units", "\"until\"", "VHDL_use",
  "VHDL_variable", "VHDL_wait", "VHDL_when", "VHDL_while", "VHDL_with",
  "\"xnor\"", "\"xor\"", "VHDL_INTEGER", "VHDL_REAL", "VHDL_STRING",
  "VHDL_BIT_STRING", "VHDL_ID", "VHDL_EXTENDED_ID", "VHDL_CHARACTER",
  "VHDL_FATAL_ERROR", "VHDL_OPT_COMMENT", "VHDL_NO_COMMENT",
  "VHDL_NO_OPAREN", "VHDL_implicit_guard", "VHDL_protected",
  "VHDL_interfacerange", "VHDL_interfaceread", "VHDL_interfaceprefixread",
  "VHDL_subtyperange", "VHDL_subtyperangebound", "VHDL_minimum",
  "VHDL_maximum", "\"??\"", "\"?=\"", "\"?/=\"", "\"?>\"", "\"?<\"",
  "\"?>=\"", "\"?<=\"", "VHDL_context", "VHDL_end_generate",
  "VHDL_parameter", "VHDL_default", "\"?\"", "VHDL_to_string",
  "VHDL_force", "VHDL_release", "\"<<\"", "\">>\"", "\"@\"", "\"^\"",
  "VHDL_BEGIN_EXPR", "VHDL_to_bstring", "VHDL_to_binary_string",
  "VHDL_to_ostring", "VHDL_to_octal_string", "VHDL_to_hstring",
  "VHDL_to_hex_string", "VHDL_recordrange", "VHDL_resize",
  "VHDL_uns_resize", "VHDL_signed_mult", "VHDL_unsigned_mult",
  "VHDL_numeric_signed_mult", "VHDL_numeric_unsigned_mult",
  "VHDL_numeric_sign_div", "VHDL_numeric_uns_div", "VHDL_numeric_minus",
  "VHDL_numeric_unary_minus", "VHDL_numeric_plus", "VHDL_is_uns_less",
  "VHDL_is_uns_gt", "VHDL_is_gt", "VHDL_is_less",
  "VHDL_is_uns_gt_or_equal", "VHDL_is_uns_less_or_equal",
  "VHDL_is_gt_or_equal", "VHDL_is_less_or_equal", "VHDL_and_reduce",
  "VHDL_or_reduce", "VHDL_xor_reduce", "VHDL_numeric_mod", "VHDL_i_to_s",
  "VHDL_s_to_u", "VHDL_s_to_i", "VHDL_u_to_i", "VHDL_u_to_u", "VHDL_s_xt",
  "$accept", "source_text", "design_unit", "context_clause",
  "context_item", "context_item_list", "plain_context_item",
  "library_clause", "use_clause", "library_unit", "plain_library_unit",
  "primary_unit", "secondary_unit", "context_decl", "@1", "context_id_def",
  "opt_VHDL_context", "entity_decl", "@2", "generic_clause_wo_semi", "@3",
  "generic_clause", "port_clause", "entity_decl_part", "entity_decl_item",
  "plain_entity_decl_item", "opt_entity_statement_part",
  "entity_statement_part", "entity_statement", "architecture_body", "@4",
  "architecture_decl_part", "block_decl_item", "plain_block_decl_item",
  "architecture_statement_part", "configuration_decl", "@5", "@6",
  "configuration_decl_part", "configuration_decl_item",
  "plain_configuration_decl_item", "block_configuration",
  "plain_block_configuration", "@7", "block_spec", "use_clause_list",
  "commented_use_clause", "configuration_item_list", "configuration_item",
  "component_configuration", "plain_component_configuration", "@8", "@9",
  "opt_binding_indication", "package_decl", "@10", "opt_package_header",
  "opt_subprogram_header", "package_decl_part", "package_decl_item",
  "plain_package_decl_item", "package_body", "@11",
  "package_body_decl_part", "package_body_decl_item",
  "plain_package_body_decl_item", "package_instantiation_decl", "@12",
  "subprogram_spec", "@13", "@14", "opt_return", "subprogram_decl",
  "sequential_stmts_begin", "subprogram_body", "opt_formal_parameter_list",
  "opt_parameter_keyword", "subprogram_decl_part", "subprogram_decl_item",
  "plain_subprogram_decl_item", "subprogram_statement_part",
  "subtype_decl", "type_decl", "@15", "type_def", "scalar_type_def",
  "composite_type_def", "array_type_def", "record_type_def", "@16",
  "element_decl", "file_type_def", "enumeration_type_def",
  "physical_type_def", "base_unit_declaration", "physical_unit_decl",
  "protected_type_def", "@17", "protected_type_declarative_part",
  "protected_type_body_declarative_part",
  "protected_type_declarative_item",
  "protected_type_body_declarative_item",
  "plain_protected_type_declarative_item",
  "plain_protected_type_body_declarative_item", "protected_type_def_body",
  "@18", "constant_decl", "signal_decl", "variable_decl", "file_decl",
  "alias_decl", "opt_colon_subtype_indication", "component_decl", "@19",
  "attribute_decl", "attribute_spec", "configuration_spec",
  "disconnection_spec", "group_template_decl", "entity_class_entry",
  "entity_class", "group_decl", "group_template_name", "interface_decl",
  "plain_interface_decl", "interface_object_decl", "opt_interface_kind",
  "opt_mode", "opt_force_mode", "interface_type_decl",
  "interface_subprogram_decl", "interface_subprogram_spec", "@20", "@21",
  "opt_interface_subprogram_default", "interface_subprogram_default",
  "interface_package_decl", "@22", "interface_package_generic_map_aspect",
  "entity_spec", "component_spec", "guarded_signal_spec",
  "concurrent_statement", "plain_concurrent_statement",
  "unlabeled_cc_statement", "concurrent_procedure_call_statement",
  "component_instantiation_statement", "instantiated_unit",
  "generate_statement", "plain_generate_statement",
  "for_generate_statement", "for_scheme", "@23", "if_generate_statement",
  "if_scheme", "if_generate_statement_part",
  "else_elsif_generate_statement", "elsif_generate_statement",
  "elsif_scheme", "else_generate_statement", "else_scheme",
  "case_generate_statement", "case_generate_alternatives",
  "case_generate_alternative", "case_scheme", "generate_statement_body",
  "@24", "generate_statement_body_int", "gen_block_decl_list",
  "gen_block_statement_list", "cc_conditional_signal_assignment",
  "cc_selected_signal_assignment", "target", "options", "delay_mechanism",
  "conditional_waveforms", "else_when_waveform_list", "else_with_waveform",
  "selected_waveforms", "waveform", "waveform_element_list",
  "waveform_element", "block_statement", "@25", "@26", "block_generics",
  "block_ports", "block_decl_part", "block_statement_part",
  "opt_guard_expression", "process_statement", "@27",
  "opt_sensitivity_list", "sensitivity_list", "process_decl_part",
  "process_decl_item", "plain_process_decl_item", "process_statement_part",
  "sequential_statement", "plain_sequential_statement",
  "unlabeled_statement", "assertion_statement", "report_statement",
  "opt_report_expression", "opt_severity_expression", "wait_statement",
  "opt_sensitivity_clause", "opt_condition_clause", "opt_timeout_clause",
  "signal_assignment_statement", "conditional_signal_assignment",
  "selected_signal_assignment", "simple_signal_assignment",
  "variable_assignment_statement", "simple_assignment_statement",
  "conditional_assignment_statement", "conditional_expressions",
  "else_when_list", "else_with_expr", "selected_assignment_statement",
  "selected_expressions", "procedure_call_statement", "if_statement",
  "elsif_list", "else_statements", "sequence_of_statements",
  "case_statement", "opt_matching", "case_statement_alternatives",
  "case_statement_alternative", "loop_statement", "@28",
  "opt_iteration_scheme", "next_statement", "exit_statement",
  "return_statement", "null_statement", "opt_when_condition",
  "binding_indication", "entity_aspect", "opt_generic_map_aspect",
  "opt_port_map_aspect", "generic_map_aspect", "port_map_aspect",
  "assoc_list", "assoc_element", "plain_assoc_element", "formal_part",
  "actual_part", "actual_designator", "signature", "opt_return_type",
  "file_open_info", "opt_file_open", "file_logical_name",
  "range_constraint", "index_constraint", "range", "range_subset1",
  "direction", "discrete_range", "discrete_range_subset1",
  "subtype_indication", "subtype_indication_subset1",
  "subtype_indication_subset2", "resolution_indication",
  "array_resolution_indication", "record_resolution_indication",
  "record_resolution_element_list", "record_resolution_element",
  "expression", "AND_expression", "OR_expression", "XOR_expression",
  "XNOR_expression", "relation", "relational_oper", "shift_expression",
  "shift_operator", "simple_expression", "adding_operator", "term",
  "multiplying_operator", "logical_operator", "factor", "primary",
  "literal", "aggregate", "allocator", "element_assoc_list",
  "element_assoc", "choices", "choice", "opt_id", "opt_expression",
  "opt_signature", "opt_generic_clause", "opt_port_clause",
  "opt_designator", "opt_file_open_info", "opt_delay_mechanism",
  "opt_type_mark_list", "opt_VHDL_package_body", "opt_pure_impure",
  "opt_subprogram_kind", "opt_signal_kind", "opt_VHDL_architecture",
  "opt_VHDL_entity", "opt_VHDL_package", "opt_VHDL_configuration",
  "opt_VHDL_is", "opt_VHDL_BOX", "opt_VHDL_shared", "opt_VHDL_postponed",
  "opt_VHDL_guarded", "opt_init_assign", "selected_name_list",
  "interface_list", "enumeration_literal_list", "physical_unit_decl_list",
  "entity_class_entry_list", "entity_designator_list", "name_list",
  "discrete_range_list", "element_decl_list", "type_mark_list",
  "element_id_def_list", "library_id_def_list", "interface_id_def_list",
  "signal_id_def_list", "constant_id_def_list", "variable_id_def_list",
  "file_id_def_list", "access_type_def", "abstract_literal",
  "physical_literal", "character_literal", "string_literal",
  "bit_string_literal", "name", "qualified_expression", "attribute_name",
  "selected_name", "indexed_name", "external_name", "external_path_name",
  "package_pathname", "absolute_pathname", "relative_pathName",
  "opt_accent_list", "accent_list", "designator", "attribute_designator",
  "entity_designator", "prefix", "simple_prefix", "signatured_prefix",
  "suffix", "operator_symbol", "type_mark", "entity_name", "id",
  "uninstantiated_package_name", "uninstantiated_subprog_name",
  "physical_unit_id_def", "record_element_id_def", "library_id_def",
  "interface_id_def", "subtype_id_def", "type_id_def",
  "generic_type_id_def", "constant_id_def", "signal_id_def",
  "variable_id_def", "file_id_def", "component_id_def", "attribute_id_def",
  "group_template_id_def", "group_id_def", "subprogram_id_def",
  "procedure_id_def", "enumeration_id_def", "alias_id_def", "label",
  "entity_id_def", "architecture_id_def", "configuration_id_def",
  "package_id_def", "package_body_id_def", "pre_comment", "opt_comment", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short yytoknum[] =
{
       0,   256,  3029,   257,   258,   259,   260,   261,   262,   263,
     264,   265,   266,   267,   268,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   293,
     294,   295,   296,   297,   298,   299,   300,   301,   302,   303,
     304,   305,   306,   307,   308,   309,   310,   311,   312,   313,
     314,   315,   316,   317,   318,   319,   320,   321,   322,   323,
     324,   325,   326,   327,   328,   329,   330,   331,   332,   333,
     334,   335,   336,   337,   338,   339,   340,   341,   342,   343,
     344,   345,   346,   347,   348,   349,   350,   351,   352,   353,
     354,   355,   356,   357,   358,   359,   360,   361,   362,   363,
     364,   365,   366,   367,   368,   369,   370,   371,   372,   373,
     374,   375,   376,   377,   378,   379,   380,   381,   382,   383,
     384,   385,   386,   387,   484,   485,   486,   492,   493,   494,
     495,   496,   497,   498,   550,   551,   552,   553,   554,   555,
     556,   557,   558,   559,   560,   561,   562,   563,   564,   565,
     566,   567,   568,   569,   570,   571,   572,   573,   574,   575,
     576,   577,   578,  3000,  3001,  3002,  3003,  3004,  3005,  3006,
    3007,  3008,  3009,  3010,  3011,  3012,  3013,  3014,  3015,  3016,
    3017,  3018,  3019,  3020,  3021,  3022,  3023,  3024,  3025,  3026,
    3027,  3028
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned short yyr1[] =
{
       0,   202,   203,   203,   203,   203,   204,   205,   205,   206,
     207,   207,   208,   208,   208,   209,   210,   211,   212,   212,
     213,   213,   213,   213,   213,   214,   214,   216,   215,   217,
     218,   218,   220,   219,   222,   221,   223,   224,   225,   225,
     225,   226,   227,   227,   227,   227,   227,   227,   227,   227,
     227,   227,   227,   227,   227,   227,   227,   227,   227,   227,
     228,   228,   229,   229,   230,   232,   231,   233,   233,   233,
     234,   235,   235,   235,   235,   235,   235,   235,   235,   235,
     235,   235,   235,   235,   235,   235,   235,   235,   235,   235,
     235,   236,   236,   236,   238,   239,   237,   240,   240,   240,
     241,   242,   242,   242,   243,   245,   244,   246,   247,   247,
     248,   249,   249,   249,   250,   250,   251,   253,   252,   254,
     252,   255,   255,   257,   256,   258,   258,   258,   259,   259,
     260,   260,   260,   261,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   264,   263,   265,   265,   265,   266,   267,   267,
     267,   267,   267,   267,   267,   267,   267,   267,   267,   267,
     267,   267,   267,   267,   269,   268,   271,   270,   272,   270,
     273,   273,   274,   275,   276,   276,   277,   277,   278,   278,
     279,   279,   279,   280,   281,   281,   281,   281,   281,   281,
     281,   281,   281,   281,   281,   281,   281,   281,   281,   281,
     281,   282,   282,   282,   283,   285,   284,   284,   286,   286,
     286,   286,   287,   287,   287,   288,   288,   288,   288,   289,
     291,   290,   292,   293,   294,   295,   296,   297,   299,   298,
     300,   300,   301,   301,   302,   302,   303,   303,   304,   304,
     304,   304,   305,   305,   305,   305,   305,   305,   305,   305,
     305,   305,   305,   305,   305,   305,   305,   305,   307,   306,
     308,   309,   310,   311,   312,   313,   313,   315,   314,   316,
     317,   318,   319,   320,   321,   322,   322,   322,   322,   322,
     322,   322,   322,   322,   322,   322,   322,   322,   322,   322,
     322,   322,   323,   324,   325,   326,   326,   326,   326,   327,
     328,   328,   328,   328,   328,   329,   329,   329,   329,   329,
     329,   330,   330,   330,   331,   332,   334,   333,   335,   333,
     336,   336,   337,   337,   339,   338,   340,   340,   340,   341,
     342,   343,   344,   345,   345,   345,   345,   345,   345,   345,
     345,   346,   346,   346,   346,   347,   347,   347,   348,   349,
     349,   349,   349,   350,   351,   351,   351,   352,   352,   354,
     353,   355,   355,   356,   356,   357,   357,   357,   358,   358,
     359,   360,   360,   361,   362,   362,   363,   363,   364,   364,
     364,   364,   365,   366,   366,   368,   367,   369,   369,   369,
     370,   370,   371,   371,   372,   372,   373,   374,   374,   375,
     376,   376,   376,   377,   378,   378,   379,   379,   380,   380,
     381,   381,   382,   382,   382,   383,   383,   385,   386,   384,
     387,   387,   387,   388,   388,   388,   389,   389,   389,   390,
     390,   390,   391,   391,   393,   392,   394,   394,   394,   395,
     396,   396,   396,   397,   398,   398,   398,   398,   398,   398,
     398,   398,   398,   398,   398,   398,   398,   398,   398,   398,
     399,   399,   399,   400,   401,   401,   402,   402,   402,   402,
     402,   402,   402,   402,   402,   402,   402,   402,   402,   403,
     404,   405,   405,   406,   406,   407,   408,   408,   409,   409,
     410,   410,   411,   411,   411,   412,   412,   413,   413,   413,
     413,   414,   414,   414,   415,   415,   415,   416,   417,   418,
     419,   419,   420,   420,   421,   421,   422,   422,   423,   424,
     425,   425,   426,   426,   427,   427,   427,   428,   429,   429,
     430,   430,   430,   431,   433,   432,   434,   434,   434,   434,
     435,   436,   437,   438,   439,   439,   440,   440,   441,   441,
     441,   442,   442,   443,   443,   444,   445,   446,   446,   446,
     447,   448,   448,   448,   449,   450,   450,   451,   451,   452,
     453,   453,   454,   455,   455,   456,   457,   457,   458,   459,
     459,   460,   461,   461,   462,   462,   462,   463,   463,   464,
     464,   464,   465,   465,   466,   467,   467,   467,   468,   469,
     470,   470,   471,   472,   472,   472,   472,   472,   472,   472,
     473,   473,   474,   474,   475,   475,   476,   476,   477,   477,
     478,   478,   478,   478,   478,   478,   478,   478,   478,   478,
     478,   478,   479,   479,   480,   480,   480,   480,   480,   480,
     481,   481,   481,   481,   482,   482,   482,   483,   483,   484,
     484,   484,   484,   485,   485,   485,   485,   485,   485,   486,
     486,   486,   486,   486,   486,   487,   487,   487,   487,   487,
     488,   488,   488,   488,   488,   489,   490,   490,   491,   491,
     491,   492,   492,   493,   493,   493,   493,   494,   494,   494,
     495,   495,   496,   496,   497,   497,   498,   498,   499,   499,
     500,   500,   501,   501,   502,   502,   503,   503,   504,   504,
     505,   505,   505,   506,   506,   506,   507,   507,   507,   508,
     508,   509,   509,   510,   510,   511,   511,   512,   512,   513,
     513,   514,   514,   515,   515,   516,   516,   517,   517,   518,
     518,   518,   519,   519,   520,   520,   520,   521,   521,   521,
     522,   522,   522,   523,   523,   523,   524,   524,   524,   525,
     525,   525,   526,   526,   526,   527,   527,   527,   528,   528,
     528,   529,   529,   530,   530,   530,   531,   531,   531,   532,
     532,   532,   533,   533,   533,   534,   534,   534,   535,   536,
     536,   537,   537,   538,   539,   540,   541,   541,   541,   541,
     541,   541,   542,   543,   543,   544,   545,   546,   546,   546,
     547,   547,   547,   548,   549,   550,   551,   551,   552,   552,
     553,   553,   553,   554,   554,   554,   555,   555,   555,   556,
     556,   557,   558,   559,   559,   559,   559,   560,   561,   562,
     562,   563,   564,   565,   565,   566,   567,   568,   569,   570,
     571,   572,   573,   574,   575,   576,   577,   578,   579,   580,
     581,   581,   582,   583,   583,   584,   584,   584,   585,   586,
     587,   588,   589,   590,   591,   592,   592
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     0,     2,     2,     1,     2,     0,     2,     3,
       0,     2,     1,     1,     3,     3,     3,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     9,     1,
       0,     1,     0,    12,     0,     5,     2,     5,     0,     2,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     2,     0,     2,     1,     0,    13,     0,     2,     1,
       3,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     2,     1,     0,     0,    13,     0,     2,     1,
       3,     1,     1,     1,     3,     0,     8,     1,     0,     2,
       3,     0,     2,     1,     1,     1,     3,     0,     7,     0,
       8,     0,     2,     0,    10,     0,     1,     3,     0,     2,
       0,     2,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,    10,     0,     2,     1,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     8,     0,     6,     0,     8,
       0,     2,     2,     1,     9,     7,     0,     3,     0,     1,
       0,     2,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     2,     1,     5,     0,     6,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     4,
       0,     6,     4,     3,     3,     7,     2,     4,     0,     6,
       0,     2,     0,     2,     3,     1,     3,     1,     1,     1,
       1,     7,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     8,
       6,     7,     7,     6,     7,     0,     2,     0,    10,     5,
       7,     4,     5,     7,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     5,     1,     3,     1,     1,     1,     1,     7,
       0,     1,     1,     1,     1,     0,     1,     1,     1,     1,
       1,     0,     1,     1,     2,     2,     0,     5,     0,     8,
       0,     2,     1,     1,     0,     7,     1,     5,     5,     3,
       3,     3,     3,     1,     2,     3,     4,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     3,     5,     6,     1,
       2,     2,     2,     3,     1,     1,     1,     7,     9,     0,
       5,     5,     8,     2,     4,     3,     2,     5,     1,     1,
       3,     2,     4,     3,     1,     3,     8,    11,     1,     2,
       5,     1,     2,     3,     5,     0,     2,     1,     3,     1,
       0,     2,     0,     2,     5,     5,     9,     1,     1,     2,
       1,     1,     3,     5,     0,     5,     0,     2,     3,     5,
       1,     1,     1,     3,     1,     1,     3,     0,     0,    16,
       0,     1,     3,     0,     1,     3,     0,     2,     1,     0,
       2,     1,     0,     3,     0,    12,     0,     3,     3,     1,
       0,     2,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     2,     1,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     5,
       4,     0,     2,     0,     2,     5,     0,     2,     0,     2,
       0,     2,     1,     1,     1,     5,     6,     9,     8,     9,
      10,     5,     6,     5,     1,     1,     1,     4,     4,     5,
       0,     5,     0,     2,     8,     7,     3,     5,     2,    10,
       0,     5,     0,     2,     0,     2,     1,    10,     0,     1,
       1,     2,     1,     4,     0,     8,     0,     2,     4,     2,
       4,     4,     3,     2,     0,     2,     2,     4,     2,     2,
       1,     0,     1,     0,     1,     5,     5,     1,     3,     1,
       3,     1,     3,     1,     1,     1,     2,     1,     1,     4,
       0,     2,     4,     0,     2,     1,     2,     2,     3,     1,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     2,     2,     1,     1,     1,     3,     3,
       1,     3,     2,     1,     1,     1,     1,     1,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     2,     2,     3,     1,     1,     1,     1,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     2,     2,     2,     2,     1,     1,     1,     1,     1,
       1,     2,     1,     1,     1,     3,     2,     2,     1,     3,
       1,     3,     1,     1,     3,     3,     1,     1,     1,     1,
       0,     1,     0,     1,     0,     1,     0,     1,     0,     1,
       0,     1,     0,     1,     0,     1,     0,     1,     0,     2,
       0,     1,     1,     0,     1,     1,     0,     1,     1,     0,
       1,     0,     1,     0,     1,     0,     1,     0,     1,     0,
       1,     0,     1,     0,     1,     0,     1,     0,     2,     1,
       3,     1,     1,     4,     1,     3,     1,     0,     2,     1,
       1,     3,     1,     1,     3,     1,     1,     3,     1,     1,
       3,     1,     1,     2,     1,     1,     3,     1,     1,     3,
       1,     1,     3,     1,     3,     1,     1,     3,     1,     1,
       3,     1,     1,     3,     1,     1,     3,     1,     2,     1,
       1,     1,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     3,     6,     3,     4,     6,     6,     6,
       1,     1,     1,     2,     2,     2,     0,     2,     1,     3,
       1,     1,     1,     1,     1,     1,     2,     1,     1,     1,
       1,     1,     2,     1,     1,     1,     1,     1,     1,     1,
       3,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     0,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short yydefact[] =
{
       0,     5,     0,     7,     0,     0,     0,     0,   663,   667,
       0,   668,     0,   684,   664,   666,   665,   799,   800,   804,
     805,   851,   803,     0,     0,     4,   614,   615,   616,   617,
     613,   628,   642,   650,     0,   657,   669,   676,   677,   678,
     680,   683,   807,   682,   675,   679,   809,   808,   810,   811,
       0,   839,   840,   806,     1,     3,   884,   696,   699,   598,
     698,   597,   692,   642,     0,   688,     0,   693,   675,     0,
     651,   652,   671,   686,   687,   672,   673,   826,   826,   826,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     633,   630,   632,   631,   634,   635,   636,   637,   638,   639,
     640,   641,     0,   656,   654,   655,   648,   649,   646,   644,
     647,   645,     0,     0,   659,   660,   661,   662,     0,   674,
       0,   681,     0,     0,   842,     0,     0,     0,     8,     6,
       0,   593,   592,     0,   685,     0,     0,     0,     0,     0,
     604,     0,     0,   828,     0,   820,   821,   822,     0,     0,
       0,     0,   621,   623,   625,   627,   620,   618,   619,   622,
     626,   624,   629,   643,   653,   658,   670,     0,   777,   580,
     717,   848,   775,   834,   835,   812,   813,   833,   569,     0,
     567,     0,   846,   844,   847,   815,   845,   843,     0,     0,
       0,     0,     0,     0,     0,   885,    12,    13,   885,    18,
      19,    24,    20,    25,    21,    22,    26,    23,   591,   696,
     689,   697,   694,   695,   691,   587,   586,   590,     0,   589,
     841,   841,     0,   841,   827,     0,     0,     0,     0,     0,
       0,   816,   884,     0,   578,   885,     0,   571,   575,   573,
     577,   642,   675,   880,     0,   881,     0,   879,     0,   857,
       0,   781,     0,   882,     0,   751,     0,   841,   808,   851,
       0,     0,   886,     9,    17,     0,     0,   600,   601,     0,
     606,   607,   599,   806,   829,     0,     0,   581,   579,   776,
       0,   568,   576,   570,     0,     0,    94,    32,     0,    15,
     883,     0,   123,     0,    16,    27,    14,     0,     0,   610,
     605,   817,   603,   818,   819,   814,   572,     0,   849,     0,
     706,   782,   152,   174,   125,   808,    10,   608,   609,     0,
     612,   605,   602,    65,     0,     0,     0,     0,   707,   708,
       0,     0,   126,     0,   884,   611,     0,     0,   850,    95,
      34,    36,     0,   709,     0,   156,   884,   841,   561,     0,
       0,   132,   884,    30,    11,     0,    69,   884,     0,   885,
     885,    40,   884,   718,   155,   720,     0,   562,     0,   127,
     733,   131,   720,    31,   700,     0,     0,    68,   720,    99,
     884,   752,     0,   310,     0,    62,    39,     0,   720,     0,
     700,     0,     0,     0,     0,     0,   722,     0,   721,   742,
       0,     0,   168,   172,   173,   885,   171,     0,   158,   159,
     161,   160,   162,   165,   166,   167,   163,   164,   169,   170,
       0,     0,   175,     0,   734,   700,     0,     0,     0,     0,
     146,   151,   885,   150,   134,   149,   136,   135,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   147,   148,     0,
     701,    93,   884,     0,    85,   885,    89,    90,    88,    71,
      72,    74,    73,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    86,    87,    98,     0,     0,    35,   885,
     311,   314,     0,     0,   312,     0,   313,   885,   305,     0,
     306,   307,   330,   308,     0,     0,   884,   731,    54,   885,
      58,    59,    57,    42,    43,    45,    44,    46,    47,    48,
      49,    50,    51,    52,    53,    55,    56,   719,     0,   876,
     875,   877,   275,   851,     0,     0,   791,   862,     0,   789,
     797,   865,     0,   795,   868,     0,     0,   872,   176,   859,
       0,   860,   215,   157,   182,     0,     0,     0,     0,     0,
     866,   737,   765,   838,   837,     0,     0,   841,   763,   788,
     863,     0,   786,   133,    28,   729,    92,     0,   561,     0,
      70,   735,     0,     0,     0,   101,   885,   885,   102,   103,
     885,     0,   326,   861,   324,   304,   785,   858,     0,   783,
       0,   325,     0,    37,    63,    64,   732,   700,    41,   153,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     128,     0,   217,     0,   192,     0,   884,   871,   870,   178,
     794,   864,     0,   792,   565,   124,   738,   277,     0,     0,
       0,   705,   836,     0,     0,   730,   700,     0,     0,   446,
       0,   851,   885,   343,   350,   348,   349,   353,   354,     0,
     347,   351,   352,   408,   841,     0,     0,     0,   563,     0,
     736,   700,   105,   107,   869,   100,   104,   753,     0,   188,
       0,   315,   333,   331,   332,   328,     0,   276,     0,     0,
       0,     0,   790,   747,   796,   583,     0,     0,   841,   561,
     188,     0,     0,   854,   853,   704,   183,     0,   191,   720,
     128,     0,     0,   706,     0,   764,   341,   787,   726,     0,
     491,   344,   841,     0,   737,     0,   342,   745,   355,     0,
       0,     0,   560,   561,   281,     0,   556,   564,   340,     0,
     108,   334,   189,   186,   784,   319,   316,   318,   320,   317,
       0,   188,    33,   841,     0,     0,   279,     0,     0,     0,
     713,     0,     0,   762,   286,   296,   287,   293,   285,   301,
     289,   300,   297,   298,   290,   288,   294,   292,   291,   299,
     295,   760,   739,     0,   302,   129,   186,   214,     0,     0,
       0,     0,   230,   238,     0,   218,   219,   225,   226,   221,
     222,   224,   227,   228,   223,   220,   705,   561,   213,   884,
     205,   209,   210,   208,   194,   195,   885,   197,   196,   198,
     199,   200,   201,   202,   203,   204,   206,   207,   188,   793,
     747,   708,   282,   727,   728,   747,    66,     0,   493,   356,
     768,     0,     0,   449,   766,   444,   538,   746,     0,   714,
     442,     0,     0,     0,     0,   369,     0,     0,   345,   561,
     363,   364,     0,   365,     0,     0,   366,   359,   559,   558,
     563,     0,    96,     0,     0,   885,   327,   726,   186,     0,
       0,   339,   748,   270,   584,   315,   273,   740,   284,     0,
       0,   177,   756,   873,   874,     0,   754,   798,     0,     0,
       0,     0,   268,   240,   216,     0,     0,   723,   212,   544,
     193,   186,     0,     0,     0,   492,     0,     0,   448,   447,
       0,     0,   539,     0,   424,   421,     0,     0,   420,   422,
     425,   411,     0,   410,   715,   409,     0,   737,     0,   360,
     362,   361,     0,   373,   806,   346,   841,   563,   395,   395,
     384,     0,   700,   376,   378,     0,   379,     0,   557,     0,
     113,   109,   884,     0,   841,     0,   747,     0,   274,   280,
       0,   283,   761,   234,     0,   771,   596,   769,   595,     0,
     675,     0,   233,   774,   856,   772,     0,     0,   778,   242,
       0,   855,     0,     0,   185,   725,   724,   710,   538,   700,
       0,   700,     0,     0,   702,   496,     0,     0,   885,   474,
     477,   478,   476,   479,   503,   504,   502,   480,   514,   515,
     516,   481,   482,   483,   484,   546,   485,   486,   487,   488,
     841,     0,   180,   272,     0,   271,   494,   489,   767,   452,
     884,     0,   841,   404,   405,     0,     0,     0,     0,     0,
     427,     0,     0,     0,   357,     0,     0,     0,   375,     0,
     381,   806,   700,     0,   395,   395,     0,     0,   114,   112,
     115,     0,   885,     0,   335,   336,   187,   309,     0,   582,
     585,   755,   588,     0,   229,   780,     0,   773,     0,     0,
       0,   245,     0,   241,   720,   759,     0,   236,     0,   832,
     711,   831,   830,     0,   554,     0,   554,   553,   493,   703,
       0,     0,   498,     0,     0,   714,   473,     0,     0,     0,
     528,   544,     0,   179,   700,     0,   451,   720,   745,   414,
     423,   426,   412,   443,   430,   391,     0,     0,   388,   395,
       0,   374,   358,     0,   399,   396,   884,   884,   385,     0,
       0,     0,   380,   383,   566,     0,     0,   885,   110,     0,
     329,   770,   700,   779,     0,   247,     0,   243,   720,   700,
     249,     0,   248,   885,   250,     0,   758,     0,   184,     0,
       0,     0,     0,     0,     0,   552,   497,     0,   500,     0,
       0,     0,   321,   321,     0,   549,     0,   547,     0,   475,
     181,     0,   472,   884,   464,   467,   469,   468,   454,   455,
     457,   456,   458,   459,   460,   461,   462,   463,   465,   466,
     885,     0,   416,   431,   433,     0,   806,   700,   389,   392,
     370,     0,   700,   402,   401,   403,   382,   371,     0,   377,
     106,   121,   107,   116,     0,   231,   232,     0,   262,   266,
     267,   265,   252,   253,   255,   254,   885,   256,   257,   258,
     259,   260,   261,   263,   264,   239,     0,   244,   700,     0,
       0,   555,   551,   536,   884,   550,   490,   499,     0,     0,
       0,     0,   518,   517,     0,   322,   323,     0,     0,     0,
       0,     0,   884,   278,   743,   471,   453,     0,     0,     0,
     413,     0,   434,   428,   393,     0,   700,     0,     0,     0,
     884,   700,   117,     0,     0,     0,   700,   246,     0,   235,
       0,     0,   841,   542,     0,     0,   540,   535,   532,   501,
     495,     0,     0,   714,   520,     0,     0,   513,   505,   511,
     548,     0,   744,     0,     0,   406,     0,   417,   432,     0,
       0,     0,     0,     0,   700,   367,     0,     0,   884,   122,
     337,   338,   269,   704,   841,   237,     0,     0,   541,     0,
       0,     0,     0,   714,     0,     0,   321,     0,   522,   506,
     512,   700,   700,     0,   418,     0,   435,   438,   884,   394,
     386,     0,   390,     0,   372,     0,     0,     0,   561,     0,
     538,   884,     0,     0,     0,   321,     0,     0,   525,     0,
       0,     0,     0,   519,     0,     0,     0,   415,     0,   437,
     700,   368,     0,     0,     0,   884,   700,     0,   700,   524,
       0,     0,     0,   526,     0,   508,   523,   545,   445,   419,
     441,   884,     0,   118,     0,   251,     0,   884,     0,     0,
     507,     0,   509,     0,     0,   440,   387,   120,   537,   529,
     510,   527,   521,   700,     0,   429
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short yydefgoto[] =
{
      -1,     3,    55,    56,   128,   334,   195,   196,   197,   129,
     198,   199,   200,   201,   316,   260,   374,   202,   310,   327,
     359,   328,   343,   362,   386,   499,   387,   496,   594,   203,
     337,   357,   377,   455,   452,   204,   309,   358,   380,   475,
     576,   476,   577,   730,   662,   863,   951,   952,  1059,  1060,
    1147,  1347,  1348,  1302,   205,   314,   333,   690,   352,   371,
     432,   206,   330,   346,   364,   405,   207,   331,   407,   610,
     700,  1113,   408,   697,   409,   866,   733,   616,   698,   806,
     799,   410,   411,   613,   784,   785,   786,   787,   788,   891,
     975,   789,   790,   791,   982,  1166,   792,   893,   980,  1080,
    1083,  1157,  1163,  1246,   793,   979,   412,   439,   413,   414,
     415,   601,   443,   703,   416,   417,   471,   446,   418,   771,
     772,   419,   687,   381,   487,   488,   489,   740,  1277,   490,
     491,   492,   669,   741,   591,   673,   493,   864,  1064,   679,
     568,   555,  1225,   642,   643,   644,   645,   849,   646,   850,
     851,   852,   932,   853,   854,   855,   943,   944,   945,   946,
     947,   856,  1127,  1128,  1129,  1046,  1047,  1135,  1136,  1137,
     647,   648,   649,   838,   924,   916,  1212,  1290,  1287,  1288,
     918,   919,   650,  1124,  1340,  1214,  1293,  1378,  1431,   927,
     651,   911,   714,   832,  1030,  1116,  1210,  1193,  1317,   998,
     999,   652,  1001,   828,   907,  1002,  1102,  1178,  1269,  1003,
    1004,  1005,  1006,  1007,  1008,  1009,  1180,  1368,  1403,  1010,
    1364,  1011,  1012,  1318,  1361,  1264,  1013,   913,  1315,  1316,
    1014,  1015,  1109,  1016,  1017,  1018,  1019,  1171,   657,   723,
     658,   726,   367,   727,   548,   180,   235,   236,   237,   238,
     124,   228,   750,   751,  1069,   140,   889,   216,    59,   133,
     967,    60,   266,   267,   268,   269,   270,   271,   298,   299,
     920,    26,    27,    28,    29,    30,   102,    31,   112,    32,
     113,    33,   118,    34,    35,    36,    37,    38,    39,    64,
      65,    66,    67,   449,  1100,   632,   329,   344,  1088,   752,
     925,   169,   390,   420,   987,   825,   636,   597,   425,   661,
     627,   878,   421,  1333,   839,   748,   261,   382,   885,  1086,
     773,   569,   833,   969,   976,   170,   977,   250,   588,   561,
     528,   622,   532,   795,    40,  1311,    41,    42,    43,    44,
      45,    46,    47,    48,    49,   144,   145,   146,   147,   148,
     149,  1090,   176,   558,    50,    51,    52,   185,   186,    69,
     307,    53,   348,   695,   983,   978,   251,   589,   540,   542,
     584,   529,   562,   623,   533,   551,   525,   535,   536,   619,
     538,   886,   522,   655,   248,   244,   246,   254,   291,   899,
     383
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -1154
static const short yypact[] =
{
     191, -1154,  2346,    77,   458,   938,   938,  1051, -1154, -1154,
     461, -1154,  1051, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154,  1051,   245, -1154,    54,   105,   102,   151,
     728,  1129,   952,   533,  1051, -1154,   312, -1154, -1154, -1154,
     461, -1154, -1154, -1154,   775, -1154, -1154, -1154, -1154, -1154,
     596,   340, -1154, -1154, -1154, -1154, -1154,   344, -1154, -1154,
   -1154, -1154, -1154,  1497,   634, -1154,   878, -1154,   682,   343,
     533,   533, -1154,   775, -1154, -1154, -1154,   113,   113,   113,
    2346,  2346,  2346,  2346,  2346,  2346,  2346,  2346,  2346,  2346,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154,  2346, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154,  2346,   938, -1154, -1154, -1154, -1154,   938, -1154,
    1051,   775,   724,    84, -1154,   218,  1650,   396, -1154, -1154,
     402, -1154, -1154,  2346, -1154,  1739,  2207,  2207,  2346,  2285,
   -1154,   461,   461, -1154,   452, -1154, -1154, -1154,   461,   487,
     562,   579, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154,   806,   533, -1154, -1154,   549, -1154,   517,
     659,   775, -1154, -1154, -1154, -1154,   712, -1154, -1154,   801,
   -1154,  2196, -1154, -1154, -1154, -1154, -1154, -1154,   607,   668,
     683,   696,    74,   186,   194,   625, -1154, -1154,   625, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,   806, -1154,
   -1154,   127, -1154, -1154, -1154, -1154, -1154, -1154,   127,  2161,
     695,   774,    89,   815,   684,    89,    89,   461,   840,   461,
    1650, -1154, -1154,  2196, -1154,   625,   881, -1154, -1154, -1154,
   -1154,  1244,   653, -1154,   837, -1154,   871, -1154,   805, -1154,
     384, -1154,   826, -1154,   890, -1154,   488,   946,   564,   918,
     923,   671, -1154, -1154, -1154,    73,   839, -1154, -1154,   461,
   -1154, -1154,   682,   591, -1154,   848,   853, -1154, -1154, -1154,
     818, -1154, -1154, -1154,  2196,   908, -1154, -1154,   696, -1154,
   -1154,   953,   964,   461, -1154, -1154, -1154,  1040,   854, -1154,
      73, -1154,   343, -1154, -1154, -1154, -1154,   982,  1044,   908,
     992, -1154, -1154, -1154,   992,   718, -1154, -1154, -1154,   908,
   -1154, -1154, -1154, -1154,   908,  1008,  1064,  1067, -1154,   995,
    2613,   461,  1026,  2476,  1046, -1154,    73,  2345, -1154, -1154,
   -1154, -1154,  1098, -1154,  2545, -1154,  1061,   444,  1026,  1048,
    1105, -1154,  1076,   977, -1154,    37, -1154,  1093,   339,   625,
     625, -1154,   302,  1049, -1154,  2858,  1124, -1154,  1135, -1154,
    1065, -1154,  2779, -1154,   908,   186,   242, -1154,  2711, -1154,
   -1154, -1154,   560,  1196,   639, -1154, -1154,  1101,  2790,  1116,
     908,   802,   601,   118,   128,  1028, -1154,  1030, -1154, -1154,
    1031,  1032, -1154, -1154, -1154,   625, -1154,   309, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
    1107,  1053, -1154,  1650, -1154,   908,  1035,   177,  1037,   141,
   -1154, -1154,   625, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,  1155,
   -1154, -1154,  1132,   177, -1154,   625, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154,  1133,   283, -1154,   625,
   -1154, -1154,  1037,  1030, -1154,  1056, -1154,   625, -1154,   168,
   -1154, -1154,  1109, -1154,  1130,  1174,  1137,  1138, -1154,   625,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,  1178, -1154,
   -1154, -1154,  1180,  1181,  1114,  1183, -1154, -1154,   866, -1154,
   -1154, -1154,   876, -1154,  1184,  1131,  1186, -1154, -1154, -1154,
    1136, -1154,  1188, -1154, -1154,  1885,   349,   170,   962,  1191,
   -1154,  1140, -1154, -1154, -1154,  1177,   912,   616, -1154, -1154,
   -1154,   922, -1154, -1154, -1154,  1165, -1154,   416,   326,   928,
   -1154,  1163,   549,   461,  1080, -1154,   625,   625, -1154, -1154,
     625,  1144, -1154, -1154, -1154, -1154, -1154, -1154,   950, -1154,
      52, -1154,   349, -1154, -1154, -1154, -1154,   908, -1154, -1154,
      89,  1147,   177,   461,  1085,    89,  1086,    89,  1212,   461,
     992,    89, -1154,  1151, -1154,   644,  1185, -1154, -1154, -1154,
   -1154, -1154,   968, -1154, -1154, -1154, -1154, -1154,  2346,   319,
     461,  1091, -1154,  1092,    89, -1154,   908,  2346,   490,  1222,
    2346,  1215,   625, -1154, -1154, -1154, -1154, -1154, -1154,  1203,
   -1154, -1154, -1154, -1154,   770,  1219,   661,  1221,  1145,   461,
   -1154,   908, -1154,   775, -1154, -1154, -1154, -1154,  1161,  1083,
    1112,   820, -1154, -1154,   775, -1154,  1231, -1154,   461,  1190,
     969,  1234, -1154,  1226, -1154,   275,  2571,  1241,   501,  1026,
    1083,  1246,   376, -1154, -1154,   946, -1154,  2022, -1154,  1789,
     992,  1128,    89,   992,  1247, -1154, -1154, -1154,   178,  1250,
    1171, -1154,   791,   176,  1140,  1168, -1154,  1210, -1154,   610,
     461,   461, -1154,  1026, -1154,  1201, -1154, -1154,   775,  1268,
   -1154, -1154, -1154,  1280, -1154, -1154, -1154, -1154, -1154, -1154,
      89,  1083, -1154,   689,  2346,  2884, -1154,  2346,  1273,  2346,
   -1154,  1228,  1282, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154,  1275,   978, -1154, -1154,  1280, -1154,    88,    89,
    1296,  1224, -1154,  1265,  1293, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154,  1193, -1154, -1154,  1026, -1154,  1259,
   -1154, -1154, -1154, -1154, -1154, -1154,   625, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,  1083, -1154,
    1226,   995, -1154, -1154, -1154,  1226, -1154,  2346,  1209, -1154,
   -1154,  1308,  1312,  1310,   775, -1154,  1164, -1154,   433,   454,
    1318,  2346,   461,   461,   461, -1154,  2346,   490, -1154,  1026,
   -1154, -1154,  1266, -1154,  1267,  1022, -1154,   765,   775,   775,
    1145,  1321, -1154,   372,   461,   625, -1154,   178,  1280,  1314,
    1315, -1154, -1154, -1154, -1154,   820, -1154, -1154, -1154,  1319,
    2884, -1154, -1154, -1154, -1154,   989, -1154, -1154,  1887,  1251,
     461,   171, -1154, -1154, -1154,  1206,  1323,   404, -1154,   590,
   -1154,  1280,  1324,  1290,  1329, -1154,  2346,  1332, -1154, -1154,
     461,  2678, -1154,   156, -1154, -1154,  1334,   160,  1345, -1154,
    1328, -1154,  2346, -1154, -1154, -1154,  2346,  1140,  1297,   775,
     775,   775,   908, -1154,  1348, -1154,   860,  1145, -1154, -1154,
     908,  2346,   153, -1154, -1154,  1304, -1154,  1305, -1154,  1650,
   -1154, -1154,  1313,  1248,   310,   655,  1226,  1269, -1154, -1154,
    2346, -1154, -1154, -1154,   814, -1154, -1154, -1154, -1154,  1000,
     730,    89, -1154,   984, -1154, -1154,   112,   991, -1154, -1154,
     608, -1154,   133,  1350, -1154, -1154, -1154,   817,  1164,   908,
    2346,   908,  1353,  2346,  2346,  1288,  2346,  1010,   625, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154,    76, -1154, -1154, -1154, -1154,
     527,  1358,  1276, -1154,  1327, -1154, -1154, -1154,   775, -1154,
    1185,  1347,   529, -1154, -1154,  2346,  2346,  2346,  1311,  1371,
   -1154,    80,  1316,  2346, -1154,  1364,  1330,  1434, -1154,  1366,
   -1154,  1368,   908,  1369, -1154, -1154,  1002,  1333, -1154, -1154,
   -1154,  1335,   625,  1317, -1154, -1154, -1154, -1154,   461, -1154,
   -1154, -1154, -1154,  2346, -1154, -1154,  1291, -1154,  1254,    89,
    2690, -1154,  1256, -1154,   586, -1154,    75, -1154,  1372, -1154,
   -1154, -1154, -1154,  2346,  1278,  1277,  1278, -1154,  1209, -1154,
    1382,   199,  1284,  1295,  2346,   439, -1154,   180,  2346,  1331,
   -1154,  1043,   461, -1154,   908,  2031, -1154,  2858,  1210, -1154,
   -1154, -1154, -1154, -1154,   992, -1154,  1739,   263, -1154, -1154,
    2346, -1154, -1154,   239, -1154, -1154,  1365,   632, -1154,  2346,
    1391,  1038, -1154, -1154, -1154,  1392,   177,   625, -1154,  1402,
   -1154, -1154,   908, -1154,  1396, -1154,  1279, -1154,  2858,   908,
   -1154,   418, -1154,   625, -1154,  1298, -1154,  1395, -1154,  1346,
    2346,  1401,  1169,  1408,  1409, -1154, -1154,  2346,  1360,   138,
    1415,   253,   436,   436,   433, -1154,  1367, -1154,  2123, -1154,
   -1154,  1416, -1154,  1380, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
     625,   433,  1383,  1026,   995,   932,  1421,   229, -1154, -1154,
   -1154,  1384,   908, -1154, -1154, -1154, -1154, -1154,  1379, -1154,
   -1154,   413,   906, -1154,  1503, -1154, -1154,  1397, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154,   625, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154,  1370, -1154,   908,   638,
     101, -1154, -1154, -1154,  1057, -1154, -1154, -1154,  2346,  1426,
     156,  1015, -1154, -1154,  2346, -1154, -1154,  2346,  1428,  1429,
     266,  2346,  1393, -1154,  1359, -1154, -1154,   764,  1326,   433,
   -1154,  1435,  1145, -1154, -1154,  1739,   908,  1436,  1394,  1437,
     640,   908,  1399,  1439,  1450,  1451,   908, -1154,   644, -1154,
     461,  1444,   692, -1154,  1739,   314, -1154, -1154,  1050, -1154,
   -1154,  1020,  2346,   292, -1154,  1445,   271, -1154, -1154, -1154,
   -1154,  1388, -1154,  1373,   433, -1154,  1739,  1340, -1154,  1449,
    2455,   951,  1453,   355,   908, -1154,  1455,  1413, -1154, -1154,
   -1154, -1154, -1154,   946,   735, -1154,   957,  1430, -1154,  2123,
    2346,  1418,  2346,   515,   829,  1355,   436,   433,  1427, -1154,
   -1154,   908,   908,  1357,  1099,  2346, -1154, -1154,  1441, -1154,
   -1154,  1425, -1154,  1473, -1154,  1432,  1443,  1440,  1026,  1639,
    1164,  1446,  1390,  1431,   850,   436,   433,  2346, -1154,  1739,
    2346,   862,  2346, -1154,  1485,  1488,  1739, -1154,   441, -1154,
     908, -1154,  1489,  1454,  1492,   361,   908,  1169,   908, -1154,
    2346,   874,  1406,  1099,   898, -1154,  1407, -1154, -1154,  1099,
   -1154,  1457,  1498, -1154,  1500, -1154,  1504,  1060,  1506,   913,
   -1154,  1739, -1154,  2346,  1477, -1154, -1154, -1154, -1154, -1154,
   -1154,  1099, -1154,   908,  1508, -1154
};

/* YYPGOTO[NTERM-NUM].  */
static const short yypgoto[] =
{
   -1154, -1154, -1154, -1154,  1195, -1154, -1154, -1154,  -336, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,  -501,
   -1154,  -303,   320, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1104, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154,  -922, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154,  -328, -1154, -1154,   833, -1154, -1154,
   -1154,  -324, -1154, -1154, -1154, -1154,  -316, -1154,   451, -1154,
   -1154, -1154,  -317,   506,  -302,  -693,  -525, -1154, -1154, -1154,
   -1154,  -288,  -285, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
     565, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154,  -281,  -313,  -280,  -277,
    -260, -1154,  1162, -1154,  -225,  -354, -1154,   243,  -218,   662,
     798,  -309, -1154,   965, -1154, -1154, -1154,   672, -1126, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
     403, -1154,  -448, -1154,  -598, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154,   409, -1154, -1154, -1154,
   -1154, -1154, -1154, -1089, -1154,  -888, -1154, -1154, -1154,   328,
   -1154, -1154,  -854,   438, -1154,   370, -1154, -1154, -1005,  -816,
   -1154,   522, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154,   459, -1154, -1154, -1154, -1154,  -773, -1154,
     448,  -852, -1154, -1154,   465, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154, -1154, -1154,   288, -1154, -1154, -1154,
    -960, -1154, -1154, -1154, -1154, -1153, -1154,  -965, -1154,   251,
   -1154, -1154, -1154, -1154, -1154, -1154, -1154,   471,   341, -1154,
    -343,  -667,  -331,   281,  -116,  1343, -1154, -1154,   200, -1154,
    -536, -1154, -1154, -1154, -1154,  -275, -1154, -1154,  -131, -1154,
   -1020,  1403,  -211, -1154,    21,   150, -1154, -1154, -1154,  1257,
      -2, -1154, -1154, -1154, -1154,   943, -1154,  1475, -1154,    13,
   -1154,   111, -1154, -1154,  1462,   476, -1154,  -122, -1154, -1154,
    1456, -1080,   998,  -294, -1154,  -676,   882,   767, -1154, -1154,
   -1072, -1154, -1154,  1207, -1154,   722, -1154, -1154, -1154, -1154,
    -675, -1154, -1154, -1154, -1154,  -721,  1400,  -340, -1154, -1154,
   -1154,  -369, -1154, -1154, -1154, -1154, -1154, -1154, -1154, -1154,
   -1154, -1154, -1154, -1154,   333, -1154,  -121,  -118, -1154,     3,
    1584,  1460,  -120, -1154, -1154,  1047, -1154, -1154, -1154, -1154,
   -1154, -1154,  -326,   967,  1199, -1154, -1154, -1154,   614,  -107,
    1299,   629, -1154,   294,   521,   532,  1325,   941, -1154, -1154,
   -1154,  1011,   979,   916,  1013, -1154, -1154, -1154, -1154,  1029,
    1139,   650, -1154,   725, -1154, -1154, -1154,  1141, -1154,   -44,
    -167
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -885
static const short yytable[] =
{
      25,   350,    62,   175,   566,   366,   183,    68,   217,   184,
     179,   332,   130,    73,   275,   276,   172,    63,   445,   797,
     384,   631,   917,  1093,   470,    61,   898,   322,   263,   402,
    1058,   264,  1224,  1184,   513,  1282,   430,   403,  1218,   835,
     711,   404,   454,   121,   431,   997,  1215,  1000,   595,   406,
     456,  1048,   498,  1151,   457,   434,   433,  1278,   556,  1031,
     500,   459,   458,   448,   501,   464,   524,   869,   283,   474,
     435,   503,   502,   258,   258,   508,   460,    54,   265,   516,
     672,  1125,   181,   881,   436,   168,   504,   437,    80,   882,
     461,   438,   440,   462,   265,   441,   518,   463,   465,   902,
     505,   466,  1313,   506,   904,  -716,   191,   507,   509,   689,
    1220,   510,   442,  1075,   280,   252,    70,    71,   467,   526,
     277,   848,   279,   578,   141,   163,   171,  1165,   511,   530,
     103,   549,  1107,    62,  1085,   104,   214,   105,    68,    68,
      68,   575,   559,     4,   220,   221,   208,   444,    63,   211,
     211,   223,   218,   469,   447,   193,    61,    61,    61,   796,
     473,     4,   302,   512,  1076,   776,  1142,  1143,   579,   586,
     515,   620,   973,   315,  1034,   957,   131,   830,   552,   240,
      19,  1185,    21,  -716,   242,  -757,   181,   255,   181,    81,
     375,    -2,     1,   948,   241,   255,   257,   257,  1108,   689,
     830,  1126,    61,    21,   253,   981,  1391,   631,  1022,   831,
     553,  1052,    19,    24,    21,  1341,   868,    19,   883,    21,
     884,   823,  1314,     4,   164,   272,    -2,    82,   272,   272,
     171,   240,   171,   680,  1356,  1067,  1415,    -2,   543,   132,
    1400,  1219,   974,   451,    -2,    24,   524,   -91,   527,   935,
      24,  1367,  1040,  1221,  1382,   258,  1374,   997,   531,  1000,
      -2,  1330,   554,  -757,  1437,   563,    19,  1273,    21,  1420,
    1045,   560,   171,   824,  1409,    83,   142,   143,    -2,   -91,
    1329,  1035,   240,    21,    19,  1370,    21,  1296,   570,  -712,
     355,  1396,    77,   901,   -91,  1270,   257,  1222,   587,    24,
     621,   974,   365,   676,    19,    19,    21,    21,   372,    -2,
      21,   173,   580,   378,    19,  1217,    21,    24,   388,  1423,
     585,   572,    19,   544,   259,  1271,  1429,    19,   174,    21,
     123,   -91,   598,   -91,   347,   120,   477,    24,    24,   573,
     379,   385,   709,   574,    -2,   815,   775,    24,    21,    78,
    -690,   127,   553,  -690,   -60,    24,     2,   921,   749,    21,
      24,  1451,  1401,   800,    79,   -91,  1357,   729,  1280,  1063,
     -91,   801,   -91,   950,  1274,   802,   545,   -97,   257,   181,
     860,   778,   804,   803,  1126,   349,   810,  1035,   922,   677,
     817,  1421,  1274,   288,   683,   -97,   685,   805,   289,   -97,
     691,   193,  1394,   -91,   554,   923,   779,  1381,   567,   665,
     666,   807,   780,  -543,   808,   297,  1321,   794,   809,   811,
    1285,     4,   812,   708,  -111,  1416,  1386,  -561,  -111,   182,
     557,   781,   544,   282,   914,  1314,   139,   188,     4,   813,
    1424,     5,  1430,     6,   656,   653,  -439,    19,   189,    21,
     320,  1366,   567,   637,   896,   190,   557,   -97,  -852,    57,
    1439,   985,     7,     4,   123,   222,     5,     8,     6,   139,
     782,   191,   349,  1337,   814,   716,  1126,   617,  -439,   618,
      24,   816,  -543,    72,   306,  1256,   320,     7,    75,   192,
    -884,   820,     8,  -439,   986,     4,   681,   293,   224,    76,
    1275,  -561,   294,  -852,   921,   638,   937,   639,     9,    10,
     119,    11,    12,    13,   783,  -303,   653,    14,  1373,   921,
     193,   123,  1276,   706,    19,   955,    21,   637,    22,   867,
    -439,   656,  -439,     9,    10,   922,    11,    12,    13,   640,
     114,  1110,    14,    58,    19,   115,   641,   123,   915,   123,
     922,  -407,   923,  -407,  -407,   194,  -407,    15,    16,    17,
      18,    19,    20,    21,  -439,    22,   478,   923,   887,  -439,
     654,  -439,   699,  -749,   479,   225,   663,    24,  -749,    23,
     921,   639,    15,    16,    17,    18,    19,    20,    21,    19,
      22,    21,   226,   674,    24,     4,   166,   653,  1182,  1183,
     125,   126,  -439,   272,    23,   557,   171,   116,   272,  1081,
     272,   922,   688,   640,   272,     4,   227,  1062,    19,    24,
      21,   472,    24,  1065,   572,  -704,   704,   637,   923,  -704,
     117,   514,   557,   171,   988,   710,   123,   272,   715,   900,
     134,   712,   173,   135,   989,   495,  -884,   637,  1053,   396,
     840,    24,   990,   479,   841,   842,   843,  -841,  -841,   174,
    1082,  1066,   728,   844,  -841,  -884,   845,   991,   229,   479,
     992,  -884,   846,   123,  1395,  -574,   397,  1388,   398,    21,
     293,   743,  -397,  -397,  -397,   296,  -841,  -841,   993,   994,
    -398,  -398,  -398,  -841,   173,  1094,   631,  1096,  -884,   847,
    -884,   639,   123,  -704,   193,   272,  -801,   720,  -824,   123,
     995,   174,   123,   996,   721,   123,   834,   230,    19,  -605,
     641,  -605,   857,   858,   859,   653,  -884,  -750,   167,   126,
    1164,   523,  -750,   640,  -841,  -841,  -594,   243,    19,  -594,
      21,  -841,   870,   272,   722,   872,  -848,   874,  1160,  -802,
     123,    24,  -605,  -397,   177,   123,   187,   966,  1140,   262,
    1074,  -398,    84,  1207,    17,    18,    19,  1162,    21,  -841,
    -841,    24,   693,  1334,    21,  -848,  -841,   653,  1335,  -841,
    -841,  1194,   272,   972,   718,   123,  -841,  -823,  -594,  1195,
     123,   653,  -407,  1196,   123,   123,   177,  -407,   245,    24,
    1198,  1197,  -594,    85,  1252,   829,    86,   231,  1209,   103,
     232,   123,    87,   247,   104,  1199,   105,   796,  -407,   953,
    1191,  1213,  1238,  -848,   305,   905,   249,   232,  -825,  1200,
    1239,  1106,  1201,  1056,  1240,   123,  1202,  1203,  1397,   928,
    1204,  1242,  1241,  1398,   933,   929,   930,   931,   274,  1254,
     936,   273,    88,    89,   273,   273,  1243,  1205,  1235,  1397,
     318,   278,   735,   319,  1419,  1255,  1089,   954,  1154,   184,
    1244,  1334,   287,  1245,  1044,   604,  1425,  1247,  1248,   605,
     123,  1249,  1291,  1334,   736,   606,   737,  -407,  1440,   607,
     738,   970,  1206,   171,   300,  1148,   136,   137,  1250,  1208,
     138,   218,  1020,   284,  1026,   181,   739,  1397,  1061,   968,
    -841,  -841,  1442,  1028,   308,  -704,  1032,  -841,   285,  -704,
    1038,   629,  1397,  1297,  1039,   630,   123,  1450,  1299,   321,
     519,   633,   520,  1251,   521,   634,  1084,   629,   308,  1050,
    1253,   659,   966,     4,   883,    19,   884,    21,   336,    22,
     136,   137,   286,   338,  1294,   103,   290,   292,  1070,   670,
     104,  1150,   105,   671,  1309,   321,   123,     7,   624,   136,
     137,   232,     8,  1379,   272,   136,   137,   701,   629,  1389,
    1233,   702,   745,  1445,   879,   -29,  1117,   880,  1095,   653,
     295,  1098,  1099,  -780,  1103,   963,  1257,  -780,   964,   966,
    1078,   301,  1342,   450,  1079,  1190,  1072,  1346,  1144,  1073,
     303,   232,  1352,     9,    10,   304,    11,    12,    13,   450,
     312,   177,    14,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,  1119,  1104,  1121,  1158,  1105,    21,  1322,
     313,  1131,  1323,  1286,  1362,  1414,   317,  1363,     4,   323,
    1383,   326,   106,   107,   450,   324,     4,   653,   108,   109,
     110,   111,    15,    16,    17,    18,    19,    20,    21,   340,
      22,   171,   940,   941,   942,   339,   970,  1404,  1405,  1307,
     637,   341,   272,   342,    23,   349,   218,   988,   940,   941,
    1228,  1169,   378,   567,   968,  -842,  -842,   989,   353,    24,
    1359,  1360,  1181,   360,   834,   990,  1187,  -530,  -530,  -530,
    -531,  -531,  -531,   363,  1020,   171,  1432,   136,   137,   369,
     991,   368,  1436,   992,  1438,   150,   151,    10,   370,    68,
     373,    13,   376,   970,   212,   213,   389,  1226,   422,   211,
     423,   993,   994,   218,    90,    91,    92,    61,   653,  1232,
     966,   968,   424,   497,    93,    94,    95,   517,   534,  1454,
     537,   539,   541,   995,   546,   550,   996,   253,  1261,   564,
    1263,    19,   547,    21,  -534,  1267,   590,    17,    18,    19,
      20,    21,  1032,    22,   565,   571,   583,   592,   593,   -61,
     181,   596,   599,   600,  -867,   602,   603,  -869,   608,   609,
     635,   177,   612,   611,    24,   625,  -534,   626,   628,   660,
     664,   668,    24,  -534,   678,   527,   531,   686,   692,  -534,
    -534,  -534,   560,  -534,   696,  -534,   450,   713,  -878,   273,
     717,  -534,   719,   725,   273,   724,   273,   731,   732,   122,
     273,  -534,   587,   480,   694,   742,  -534,   103,   746,  -534,
     747,   481,   104,  -720,   105,   774,   567,   744,   621,   396,
     777,   822,  1312,   273,   826,   450,  1319,  -534,  -534,   827,
     836,   837,  1324,  1032,   861,  1326,    96,    97,    98,    99,
     100,   101,   862,   482,   970,   865,   483,   873,   398,  -534,
     450,  -534,  -534,   131,   218,   875,   876,  -534,    68,  -534,
     484,   888,   968,   877,  1387,   890,   892,   894,   211,   895,
     485,   897,   906,  1354,   908,   486,    61,    68,   909,   910,
    1365,   912,   122,   926,   938,   939,   949,   211,   958,   959,
    -534,   273,   971,   961,   378,    61,   981,   984,  1023,    68,
     122,   122,  1024,  1025,   106,   107,  1027,   122,  1033,   211,
     108,   109,   110,   111,  1036,  1041,   132,    61,  1392,  1037,
    1365,  1043,  1054,  1055,  1087,  1057,   193,  1097,  1068,   273,
    1101,  1111,  1114,  1407,  1118,  1112,  1122,  1123,  1132,  1138,
    1130,  1139,  1133,  1141,   974,  1152,  1168,   567,  1172,  1145,
    1149,  1146,   122,   122,  1159,  1422,  1175,  1179,  1365,  1170,
    1426,  1177,    68,  1188,  1223,  1227,  1230,  1234,   273,    68,
    1236,  1259,   211,  1260,  1258,  1262,  1268,  1237,  1365,   211,
      61,   122,  1265,  1266,   122,   122,   122,    61,   122,  1272,
    1283,  1281,  1284,  1289,  1295,  1134,  1298,  1301,  1306,  -402,
    1320,  1452,  1327,  1328,    68,  1331,  1308,  1336,  1332,  1338,
    1343,  1345,  1344,  1349,   211,  -119,  1350,  1351,  1355,  1369,
    1371,  1375,    61,  1376,  1372,  1385,  -400,  1380,   122,  1384,
    1393,  -402,  -400,  -400,  1390,   934,  1399,  1402,  1406,  -400,
    1408,  -400,  -400,  1410,  -402,  -402,  -402,  1411,  1412,  -400,
    -400,  -400,   122,  1418,  -400,  1413,   573,  -400,  -533,  1427,
     103,  1417,  1428,  1433,   178,   104,  1435,   105,  -884,  1444,
    1434,  -884,  1446,  -884,  1447,  -697,  -697,  1453,  1448,  -697,
    1449,  -400,  1455,  -402,  -400,  -402,  -400,  1441,  1443,   354,
     122,  1304,  -884,   818,  1292,  1161,  1115,  -884,  -400,  -400,
     468,  1077,   962,   871,  -400,   667,   131,   960,  -400,  1231,
    1229,  1300,  -400,  -400,  1279,  -402,  1211,  -402,  1120,  1189,
    1176,  1042,  -402,  1174,  -402,  1325,  1358,  1173,  -884,  1049,
    1051,   450,  1303,  1339,   122,   281,   335,   162,  -884,  -884,
     165,  -884,  -884,  -884,   239,   821,  -884,  -884,   903,   956,
     494,   210,  1310,   256,    74,  -402,   705,   106,   107,   219,
     273,  1091,  1353,   108,   109,   110,   111,  1167,   325,   132,
    1153,   734,   707,   311,  1071,   682,  1092,   819,   450,   684,
     450,   675,   582,   581,  1021,     0,   122,  -884,  -884,  -884,
    -884,  -884,  -884,  -884,     0,  -884,     0,     0,     0,     0,
    1263,     0,     0,     0,  -534,     0,     0,     0,     0,  -884,
       0,   178,   122,     0,     0,  -884,     0,     0,  -884,  1305,
    -884,     0,     0,     0,  -884,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  -534,     0,     0,  -884,
       0,   450,     0,  -534,  -884,     0,     0,     0,     0,     0,
       0,  -534,     0,  -534,     0,  -534,     0,     0,     0,     0,
       0,  -534,     0,     0,     0,     0,     0,     0,   273,     0,
       0,  -534,     0,     0,     0,  -884,  -534,     0,     0,  -534,
       0,     0,     0,     0,     0,  -884,  -884,     0,  -884,  -884,
    -884,     0,     0,  -884,  -884,     0,  1186,  -534,  -534,     0,
     209,     0,     0,   450,     4,     0,     0,     5,     0,     6,
       0,     0,     0,     0,     0,  1216,     0,     0,     0,  -534,
    -534,  -534,  -534,     0,     0,     0,   122,  -534,     7,  -534,
       0,     0,   122,     8,  -884,  -884,  -884,  -884,  -884,  -884,
    -884,   450,  -884,     0,     0,     0,     0,     0,   450,   122,
       0,     0,     0,     0,     0,     0,  -884,     0,     0,   122,
    -534,   122,   122,     0,   122,     0,   122,     0,   122,     0,
     122,  -884,     0,     0,     9,    10,     0,    11,    12,    13,
       0,   391,     0,    14,    58,     0,     0,   392,   122,   122,
       0,     0,     0,   122,     0,     0,   393,   122,     0,     0,
       0,     0,     0,     0,   394,     0,   450,     0,     0,   395,
       0,   450,   396,     0,     0,     0,     0,     0,   122,     0,
       0,     0,     0,    15,    16,    17,    18,    19,    20,    21,
       0,    22,     0,     0,     0,     0,   192,   122,     0,   397,
       0,   398,     0,     0,     0,    23,   614,   450,   965,     0,
       0,     0,     4,   429,   399,     5,     0,     6,     0,   400,
      24,   122,     0,   401,     0,     0,     0,   193,  -741,     0,
       0,     0,   122,     0,     0,     0,     7,  -190,   122,   122,
     122,     8,     0,  -190,  -190,   450,     0,     0,     0,     0,
     450,     0,  -190,     0,     0,   450,     0,   694,     0,   122,
    -190,     0,  -190,     0,     0,  -190,     0,     0,  -190,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   615,     9,    10,     0,    11,    12,    13,     0,     0,
       0,    14,  -190,   450,     0,  -190,     0,  -190,   122,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  -190,
    -190,     0,     0,     0,     0,  -190,     0,     0,     0,  -190,
     450,   450,     0,  -190,  -190,     0,     0,     0,     0,     0,
       0,    15,    16,    17,    18,    19,    20,    21,     0,    22,
       0,     0,     0,   798,     0,     0,     0,  -211,     0,     0,
       0,     0,  1192,    23,     0,     0,  -470,     0,     0,   450,
       0,   122,   122,   122,     0,   450,   122,   450,    24,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  -211,
       0,     0,     0,   122,     0,     0,  -211,     0,  -470,     0,
       0,     0,     0,     0,  -211,  -470,  -211,     0,  -211,     0,
       0,     0,   450,  -470,  -211,  -470,     0,  -470,     0,   122,
       0,     0,     0,  -470,  -211,     0,     0,     0,   122,  -211,
       0,     0,  -211,  -470,     0,     0,     0,     0,  -470,   122,
       0,  -470,   122,     0,     0,     0,     0,     0,     0,     0,
    -211,  -211,     0,     0,  1263,     0,     0,     0,  -534,  -470,
    -470,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  -211,     0,  -211,  -211,     0,     0,     0,     0,
    -211,  -470,  -211,  -470,  -470,     0,     0,     0,     0,  -470,
    -534,  -470,     0,     0,  -809,  -809,  -809,  -534,  -809,  -809,
     122,  -809,  -809,  -809,     0,  -534,     0,  -534,     0,  -534,
       0,  -809,     0,  -211,  -809,  -534,     0,     0,     0,     0,
       0,     0,  -470,     0,     0,  -534,     0,     0,     0,     0,
    -534,     4,     0,  -534,     5,     0,     6,     0,     0,     0,
    -809,     0,     4,     0,     0,     5,     0,     6,     0,     0,
       0,  -534,  -534,     0,     0,     7,     0,     0,     0,     0,
       8,     0,     0,     0,     0,  -809,     7,     0,     0,     0,
       0,     8,     0,  -534,     0,  -534,  -534,     0,     0,     0,
       0,  -534,     0,  -534,     0,     0,     0,     0,  -809,     0,
       0,   233,     0,     0,     0,     0,     0,   122,     0,     0,
       0,     9,    10,  -809,    11,    12,    13,     0,   122,   234,
      14,     0,     9,    10,  -534,    11,    12,    13,     0,     0,
       4,    14,    58,     5,     0,     6,     0,     0,     0,     0,
     122,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     122,   122,     0,   215,     7,     0,     0,     0,     0,     8,
      15,    16,    17,    18,    19,    20,    21,     0,    22,     0,
       0,    15,    16,    17,    18,    19,    20,    21,     0,    22,
       0,     0,    23,     0,     0,   122,   356,     0,     0,     0,
       0,     4,     0,    23,     5,     0,     6,    24,     0,     0,
       9,    10,     0,    11,    12,    13,     0,     0,    24,    14,
       0,     0,     0,     0,     0,     7,     0,   -67,   122,     0,
       8,     0,     0,   -67,   -67,     0,     0,     0,     0,     0,
     -67,     0,   -67,   -67,     0,     0,     0,     0,     0,     0,
     -67,   -67,   -67,     0,     0,   -67,     0,     0,   -67,    15,
      16,    17,    18,    19,    20,    21,     0,    22,     0,     0,
       0,     9,    10,     0,    11,    12,    13,     0,     0,     0,
      14,    23,   -67,     0,     0,   -67,     0,   -67,     0,     0,
       0,     0,     0,     0,     0,     0,    24,     0,     0,   -67,
     -67,     0,     0,     0,     0,   -67,  1377,     0,   122,   -67,
       0,     0,     0,   -67,   -67,     0,     0,     0,     0,   122,
      15,    16,    17,    18,    19,    20,    21,   351,    22,     0,
       0,     0,     0,     0,     0,     0,     0,  -436,     0,     0,
       0,     0,    23,  -436,  -436,     0,     0,     0,     0,     0,
    -436,     0,  -436,  -436,     0,     0,     0,    24,  -130,   122,
    -436,  -436,  -436,     0,  -130,  -436,     0,     0,  -436,     0,
       0,  -130,     0,  -130,  -130,     0,     0,     0,  -130,     0,
       0,  -130,     0,  -130,     0,     0,  -130,     0,     0,  -130,
       0,     0,  -436,     0,     0,  -436,   361,  -436,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  -436,
    -436,     0,     0,  -130,     0,  -436,  -130,     0,  -130,  -436,
       0,     0,   753,  -436,  -436,     0,     0,   -38,     0,     0,
    -130,  -130,     0,   -38,   -38,     0,  -130,     0,     0,     0,
    -130,     0,   -38,   -38,  -130,  -130,     0,   -38,     0,     0,
     -38,     0,   -38,     0,     0,   -38,   754,     0,   -38,     0,
       0,     0,     0,     0,   345,     0,   755,   756,   757,     0,
       0,     0,     0,     0,   758,     0,   759,     0,   760,     0,
       0,   761,   -38,     0,     0,   -38,     0,   -38,     0,   762,
       0,     0,   763,     0,     0,  -154,     0,     0,     0,   -38,
     -38,  -154,     0,     0,     0,   -38,     0,     0,   764,   -38,
    -154,   765,     0,   -38,   -38,  -154,     0,     0,  -154,     0,
    -154,     0,     0,  -154,     0,   766,  -154,     0,     0,  1029,
       0,   767,     0,     0,     0,   768,     0,   769,     0,     0,
     770,  1155,     0,     0,     0,     0,     0,     0,     0,     0,
    -154,     0,     0,  -154,     0,  -154,     0,     0,     0,     0,
    -450,     0,     0,     0,     0,     0,  -450,  -450,  -154,     0,
       0,     0,  -884,  -154,     0,  -450,     0,  -154,  -884,     0,
       0,  -154,  -154,  -450,     0,  -450,     0,  -884,  -450,     0,
       0,  -450,  1156,   391,     0,  -884,     0,  -884,     0,   392,
    -884,     0,     0,  -884,     0,     0,   426,     0,   393,   427,
       0,     0,     0,     0,     0,  -450,   394,   453,  -450,     0,
    -450,   395,     0,     0,   396,     0,     0,  -884,     0,     0,
    -884,     0,  -884,  -450,     0,     0,     0,     0,  -450,     0,
       0,     0,  -450,     0,     0,  -884,  -450,  -450,   192,     0,
    -884,   397,     0,   398,  -884,     0,     0,     0,  -884,  -884,
       0,   391,     0,     0,     0,   429,   399,   392,     0,     0,
       0,   400,   391,     0,   426,   401,   393,   427,   392,   193,
    -741,     0,     0,     0,   394,     0,     0,   393,   427,   395,
       0,     0,   396,     0,     0,   394,     0,     0,     0,     0,
     395,     0,     0,   396,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   428,     0,     0,   397,
       0,   398,     0,     0,     0,     0,     0,   192,     0,     0,
     397,     0,   398,   429,   399,     0,     0,     0,     0,   400,
     391,     0,     0,   401,   429,   399,   392,   193,  -741,     0,
     400,     0,     0,     0,   401,   393,     0,     0,   193,  -741,
       0,     0,     0,   394,     0,     0,     0,     0,   395,   754,
       0,   396,     0,     0,     0,     0,     0,     0,     0,   755,
     756,   757,     0,     0,     0,     0,     0,   758,     0,   759,
       0,   760,     0,     0,   761,   192,     0,     0,   397,     0,
     398,     0,   762,     0,     0,   763,     0,     0,     0,     0,
       0,     0,     0,   399,     0,     0,     0,     0,   400,     0,
       0,   764,   401,     0,   765,     0,   193,  -741,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   766,     0,
       0,     0,     0,     0,   767,     0,     0,     0,   768,     0,
     769,     0,     0,   770
};

static const short yycheck[] =
{
       2,   332,     4,   125,   452,   348,   127,     4,   139,   127,
     126,   314,    56,    10,   225,   226,   123,     4,   372,   695,
     360,   557,   838,   988,   378,     4,   799,   302,   195,   365,
     952,   198,  1136,  1105,   388,  1188,   372,   365,  1127,   714,
     638,   365,   378,    40,   372,   899,  1126,   899,   496,   365,
     378,   939,   388,  1073,   378,   372,   372,  1183,   427,   913,
     388,   378,   378,   372,   388,   378,   392,   743,   235,   378,
     372,   388,   388,   193,   194,   388,   378,     0,     5,   388,
      28,     1,   126,   776,   372,     1,   388,   372,    34,     1,
     378,   372,   372,   378,     5,   372,   390,   378,   378,   820,
     388,   378,     1,   388,   825,    21,    69,   388,   388,   610,
    1130,   388,   372,     1,   230,    41,     5,     6,   378,     1,
     227,   719,   229,   477,    11,   112,   123,    52,   388,     1,
       3,   425,    56,   135,     1,     8,   138,    10,   135,   136,
     137,   477,     1,     5,   141,   142,   133,   372,   135,   136,
     137,   148,   139,   378,   372,   118,   135,   136,   137,   695,
     378,     5,   269,   388,    52,   690,  1054,  1055,   477,     1,
     388,     1,     1,   293,    14,   868,    49,     1,     1,   181,
     128,     1,   130,    99,   181,    52,   230,     1,   232,    84,
     153,     0,     1,   860,   181,     1,   193,   194,   122,   700,
       1,   121,   181,   130,   130,   130,  1359,   743,   901,    33,
      33,    58,   128,   161,   130,  1295,   741,   128,   130,   130,
     132,    43,   121,     5,   113,   222,    35,   125,   225,   226,
     227,   233,   229,   602,  1314,   956,  1389,    46,   405,   112,
    1366,  1129,   130,     1,    53,   161,   572,     5,   130,   847,
     161,  1323,   927,    14,  1343,   375,  1336,  1111,   130,  1111,
      69,  1281,    85,   130,  1417,   432,   128,    14,   130,  1395,
     937,   130,   269,    95,  1378,   124,   163,   164,    87,    37,
      14,   121,   284,   130,   128,    14,   130,    58,   455,    14,
     334,  1363,    47,   818,    52,   157,   293,    58,   130,   161,
     130,   130,   346,   597,   128,   128,   130,   130,   352,   118,
     130,    93,   479,   357,   128,    52,   130,   161,   362,  1399,
     487,    38,   128,    14,   130,  1179,  1406,   128,   110,   130,
      20,    89,   499,    91,   331,    23,   380,   161,   161,    56,
       1,    39,   636,    60,   153,   699,   689,   161,   130,   104,
       6,    11,    33,     9,    52,   161,   165,    65,    83,   130,
     161,  1441,  1367,   699,   119,   123,    52,   661,  1184,    59,
     128,   699,   130,     1,   121,   699,    67,    38,   375,   423,
     723,     5,   699,   699,   121,    59,   699,   121,    96,   600,
     699,  1396,   121,     9,   605,    56,   607,   699,    14,    60,
     611,   118,  1362,   161,    85,   113,    30,    52,   452,   576,
     577,   699,    36,    52,   699,   265,  1270,   692,   699,   699,
    1193,     5,   699,   634,    52,  1390,  1348,    14,    56,    33,
     427,    55,    14,   233,     1,   121,    93,    35,     5,   699,
    1400,     8,     1,    10,   118,   567,     5,   128,    46,   130,
     300,   159,   496,    37,   797,    53,   453,   118,    14,     1,
    1420,    57,    29,     5,    20,    13,     8,    34,    10,    93,
      94,    69,    59,  1289,   699,   642,   121,   128,    37,   130,
     161,   699,   121,     7,   284,    67,   336,    29,    12,    87,
     118,   702,    34,    52,    90,     5,   603,     9,    11,    23,
      64,    88,    14,    59,    65,    89,   849,    91,    75,    76,
      34,    78,    79,    80,   138,    14,   638,    84,  1334,    65,
     118,    20,    86,   630,   128,   865,   130,    37,   132,   740,
      89,   118,    91,    75,    76,    96,    78,    79,    80,   123,
       7,    14,    84,    85,   128,    12,   130,    20,   115,    20,
      96,    24,   113,    24,    27,   153,    27,   124,   125,   126,
     127,   128,   129,   130,   123,   132,     6,   113,   779,   128,
     567,   130,   616,     9,    14,    13,   573,   161,    14,   146,
      65,    91,   124,   125,   126,   127,   128,   129,   130,   128,
     132,   130,    13,   590,   161,     5,   120,   719,   159,   160,
       4,     5,   161,   600,   146,   602,   603,    74,   605,     1,
     607,    96,   609,   123,   611,     5,    99,   953,   128,   161,
     130,   378,   161,   954,    38,     9,   628,    37,   113,    13,
      97,   388,   629,   630,    44,   637,    20,   634,   640,   806,
       6,   638,    93,     9,    54,     6,    38,    37,   942,    63,
      40,   161,    62,    14,    44,    45,    46,     4,     5,   110,
      52,     6,   659,    53,    11,    57,    56,    77,     9,    14,
      80,    63,    62,    20,   159,    22,    90,  1353,    92,   130,
       9,   678,    50,    51,    52,    14,     4,     5,    98,    99,
      50,    51,    52,    11,    93,   989,  1232,   991,    90,    89,
      92,    91,    20,    14,   118,   702,    14,    46,    13,    20,
     120,   110,    20,   123,    53,    20,   713,     5,   128,   128,
     130,   130,   719,   720,   721,   847,   118,     9,     4,     5,
    1084,   130,    14,   123,     4,     5,     6,   130,   128,     9,
     130,    11,   744,   740,    83,   747,    93,   749,  1084,    14,
      20,   161,   161,   121,   125,    20,   127,   888,  1052,   134,
     971,   121,    34,  1117,   126,   127,   128,  1084,   130,     4,
       5,   161,   128,     9,   130,    93,    11,   899,    14,     4,
       5,  1117,   779,   890,    14,    20,    11,    13,    58,  1117,
      20,   913,    27,  1117,    20,    20,   167,    27,   130,   161,
    1117,  1117,    72,    75,  1158,    14,    78,     6,  1117,     3,
       9,    20,    84,   130,     8,  1117,    10,  1353,    27,   863,
    1114,  1124,  1158,    93,     6,   827,   130,     9,    13,  1117,
    1158,   998,  1117,   949,  1158,    20,  1117,  1117,     9,   841,
    1117,  1158,  1158,    14,   846,   842,   843,   844,   164,  1158,
     847,   222,   124,   125,   225,   226,  1158,  1117,  1152,     9,
       6,    21,    42,     9,    14,  1159,   987,   864,  1079,   987,
    1158,     9,    67,  1158,    14,     9,    14,  1158,  1158,    13,
      20,  1158,  1213,     9,    64,     9,    66,    27,    14,    13,
      70,   888,  1117,   890,   265,  1062,    18,    19,  1158,  1117,
      22,   888,   899,    22,   906,   949,    86,     9,   952,   888,
       4,     5,    14,   910,   285,     9,   913,    11,    81,    13,
     922,     9,     9,  1217,   926,    13,    20,    14,  1222,   300,
     128,     9,   130,  1158,   132,    13,   980,     9,   309,   941,
    1158,    13,  1073,     5,   130,   128,   132,   130,   319,   132,
      18,    19,    81,   324,    22,     3,   130,    67,   960,     9,
       8,  1068,    10,    13,  1258,   336,    20,    29,     6,    18,
      19,     9,    34,    22,   971,    18,    19,     9,     9,    22,
    1147,    13,    13,  1431,     6,    67,  1030,     9,   990,  1111,
      67,   993,   994,     9,   996,     6,  1163,    13,     9,  1130,
       9,   162,  1296,   374,    13,  1112,     6,  1301,     6,     9,
     162,     9,  1306,    75,    76,   162,    78,    79,    80,   390,
      67,   392,    84,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,  1035,    24,  1037,  1080,    27,   130,    24,
      76,  1043,    27,  1210,    24,  1388,     6,    27,     5,    67,
    1344,    59,   100,   101,   425,    11,     5,  1179,   106,   107,
     108,   109,   124,   125,   126,   127,   128,   129,   130,     5,
     132,  1068,    50,    51,    52,    67,  1073,  1371,  1372,  1246,
      37,    14,  1079,    88,   146,    59,  1073,    44,    50,    51,
      52,  1093,  1136,  1137,  1073,     4,     5,    54,    52,   161,
      50,    51,  1104,     5,  1101,    62,  1108,    50,    51,    52,
      50,    51,    52,    52,  1111,  1112,  1410,    18,    19,    14,
      77,    73,  1416,    80,  1418,    78,    79,    76,    52,  1126,
     153,    80,    39,  1130,   136,   137,    87,  1139,    14,  1126,
       5,    98,    99,  1130,    15,    16,    17,  1126,  1270,  1146,
    1281,  1130,    87,    52,    25,    26,    27,    41,   130,  1453,
     130,   130,   130,   120,    57,   130,   123,   130,  1170,    14,
       1,   128,   119,   130,     5,  1177,    67,   126,   127,   128,
     129,   130,  1179,   132,    52,    52,   130,    57,    14,    52,
    1234,    53,    14,    13,    13,    81,    13,    13,    67,    13,
      35,   572,    14,    67,   161,    14,    37,    67,    31,    46,
     130,    67,   161,    44,    67,   130,   130,     5,    67,    50,
      51,    52,   130,    54,    39,    56,   597,     5,    13,   600,
      27,    62,    13,    88,   605,    14,   607,    76,   155,    40,
     611,    72,   130,    47,   615,    14,    77,     3,    14,    80,
      24,    55,     8,    57,    10,    14,  1300,    67,   130,    63,
      14,    14,  1259,   634,    14,   636,  1268,    98,    99,    98,
     102,    61,  1274,  1270,    73,  1277,   147,   148,   149,   150,
     151,   152,    14,    87,  1281,     5,    90,    14,    92,   120,
     661,   122,   123,    49,  1281,    67,    14,   128,  1295,   130,
     104,     5,  1281,    28,  1348,    81,    41,    14,  1295,   116,
     114,    52,   103,  1310,     6,   119,  1295,  1314,     6,     9,
    1322,   157,   123,     5,    58,    58,     5,  1314,    14,    14,
     161,   702,    81,    14,  1378,  1314,   130,    14,    14,  1336,
     141,   142,    52,    14,   100,   101,    14,   148,    14,  1336,
     106,   107,   108,   109,     9,    58,   112,  1336,  1360,    31,
    1362,    13,    58,    58,    14,    52,   118,    14,    99,   740,
      82,    13,    45,  1375,    27,    99,    65,     6,    14,    13,
      64,    13,    52,    14,   130,    94,    14,  1431,   111,    56,
      73,    56,   193,   194,   138,  1397,    14,   102,  1400,   121,
    1402,   117,  1399,    72,    39,    14,    14,     5,   779,  1406,
      14,    16,  1399,    67,   116,    14,    56,   138,  1420,  1406,
    1399,   222,    14,    14,   225,   226,   227,  1406,   229,    14,
      14,    64,    52,    50,    13,     1,    52,    58,    41,     5,
      14,  1443,    14,    14,  1441,    52,    76,   121,    89,    14,
      14,    14,    58,    14,  1441,    56,     6,     6,    14,    14,
      72,   121,  1441,    14,    91,    52,    32,    14,   269,    14,
      52,    37,    38,    39,    44,   846,   121,    50,   121,    45,
      39,    47,    48,    58,    50,    51,    52,    14,    56,    55,
      56,    57,   293,    62,    60,    52,    56,    63,    52,    14,
       3,   111,    14,    14,     1,     8,    14,    10,     5,    52,
      56,     8,    14,    10,    14,    18,    19,    40,    14,    22,
      14,    87,    14,    89,    90,    91,    92,   121,   121,   334,
     331,    28,    29,   700,  1214,  1084,  1030,    34,   104,   105,
     378,   976,   880,   745,   110,   580,    49,   875,   114,  1146,
    1141,  1223,   118,   119,  1184,   121,  1118,   123,  1036,  1111,
    1101,   932,   128,  1098,   130,  1277,  1315,  1096,    65,   940,
     941,   942,  1231,  1292,   375,   232,   319,   102,    75,    76,
     118,    78,    79,    80,   181,   703,    83,    84,   821,   867,
     383,   135,  1259,   193,    10,   161,   629,   100,   101,   139,
     971,   987,  1308,   106,   107,   108,   109,  1086,   309,   112,
    1078,   670,   633,   288,   964,   604,   987,   701,   989,   606,
     991,   592,   483,   482,   899,    -1,   427,   124,   125,   126,
     127,   128,   129,   130,    -1,   132,    -1,    -1,    -1,    -1,
       1,    -1,    -1,    -1,     5,    -1,    -1,    -1,    -1,   146,
      -1,     1,   453,    -1,    -1,     5,    -1,    -1,     8,   156,
      10,    -1,    -1,    -1,   161,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    37,    -1,    -1,    29,
      -1,  1052,    -1,    44,    34,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    -1,    54,    -1,    56,    -1,    -1,    -1,    -1,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,  1079,    -1,
      -1,    72,    -1,    -1,    -1,    65,    77,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    75,    76,    -1,    78,    79,
      80,    -1,    -1,    83,    84,    -1,  1107,    98,    99,    -1,
       1,    -1,    -1,  1114,     5,    -1,    -1,     8,    -1,    10,
      -1,    -1,    -1,    -1,    -1,  1126,    -1,    -1,    -1,   120,
     121,   122,   123,    -1,    -1,    -1,   567,   128,    29,   130,
      -1,    -1,   573,    34,   124,   125,   126,   127,   128,   129,
     130,  1152,   132,    -1,    -1,    -1,    -1,    -1,  1159,   590,
      -1,    -1,    -1,    -1,    -1,    -1,   146,    -1,    -1,   600,
     161,   602,   603,    -1,   605,    -1,   607,    -1,   609,    -1,
     611,   161,    -1,    -1,    75,    76,    -1,    78,    79,    80,
      -1,    32,    -1,    84,    85,    -1,    -1,    38,   629,   630,
      -1,    -1,    -1,   634,    -1,    -1,    47,   638,    -1,    -1,
      -1,    -1,    -1,    -1,    55,    -1,  1217,    -1,    -1,    60,
      -1,  1222,    63,    -1,    -1,    -1,    -1,    -1,   659,    -1,
      -1,    -1,    -1,   124,   125,   126,   127,   128,   129,   130,
      -1,   132,    -1,    -1,    -1,    -1,    87,   678,    -1,    90,
      -1,    92,    -1,    -1,    -1,   146,     1,  1258,     1,    -1,
      -1,    -1,     5,   104,   105,     8,    -1,    10,    -1,   110,
     161,   702,    -1,   114,    -1,    -1,    -1,   118,   119,    -1,
      -1,    -1,   713,    -1,    -1,    -1,    29,    32,   719,   720,
     721,    34,    -1,    38,    39,  1296,    -1,    -1,    -1,    -1,
    1301,    -1,    47,    -1,    -1,  1306,    -1,  1308,    -1,   740,
      55,    -1,    57,    -1,    -1,    60,    -1,    -1,    63,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    76,    75,    76,    -1,    78,    79,    80,    -1,    -1,
      -1,    84,    87,  1344,    -1,    90,    -1,    92,   779,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   104,
     105,    -1,    -1,    -1,    -1,   110,    -1,    -1,    -1,   114,
    1371,  1372,    -1,   118,   119,    -1,    -1,    -1,    -1,    -1,
      -1,   124,   125,   126,   127,   128,   129,   130,    -1,   132,
      -1,    -1,    -1,     1,    -1,    -1,    -1,     5,    -1,    -1,
      -1,    -1,     1,   146,    -1,    -1,     5,    -1,    -1,  1410,
      -1,   842,   843,   844,    -1,  1416,   847,  1418,   161,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    37,
      -1,    -1,    -1,   864,    -1,    -1,    44,    -1,    37,    -1,
      -1,    -1,    -1,    -1,    52,    44,    54,    -1,    56,    -1,
      -1,    -1,  1453,    52,    62,    54,    -1,    56,    -1,   890,
      -1,    -1,    -1,    62,    72,    -1,    -1,    -1,   899,    77,
      -1,    -1,    80,    72,    -1,    -1,    -1,    -1,    77,   910,
      -1,    80,   913,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      98,    99,    -1,    -1,     1,    -1,    -1,    -1,     5,    98,
      99,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   120,    -1,   122,   123,    -1,    -1,    -1,    -1,
     128,   120,   130,   122,   123,    -1,    -1,    -1,    -1,   128,
      37,   130,    -1,    -1,     3,     4,     5,    44,     7,     8,
     971,    10,    11,    12,    -1,    52,    -1,    54,    -1,    56,
      -1,    20,    -1,   161,    23,    62,    -1,    -1,    -1,    -1,
      -1,    -1,   161,    -1,    -1,    72,    -1,    -1,    -1,    -1,
      77,     5,    -1,    80,     8,    -1,    10,    -1,    -1,    -1,
      49,    -1,     5,    -1,    -1,     8,    -1,    10,    -1,    -1,
      -1,    98,    99,    -1,    -1,    29,    -1,    -1,    -1,    -1,
      34,    -1,    -1,    -1,    -1,    74,    29,    -1,    -1,    -1,
      -1,    34,    -1,   120,    -1,   122,   123,    -1,    -1,    -1,
      -1,   128,    -1,   130,    -1,    -1,    -1,    -1,    97,    -1,
      -1,    65,    -1,    -1,    -1,    -1,    -1,  1068,    -1,    -1,
      -1,    75,    76,   112,    78,    79,    80,    -1,  1079,    83,
      84,    -1,    75,    76,   161,    78,    79,    80,    -1,    -1,
       5,    84,    85,     8,    -1,    10,    -1,    -1,    -1,    -1,
    1101,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1111,  1112,    -1,    28,    29,    -1,    -1,    -1,    -1,    34,
     124,   125,   126,   127,   128,   129,   130,    -1,   132,    -1,
      -1,   124,   125,   126,   127,   128,   129,   130,    -1,   132,
      -1,    -1,   146,    -1,    -1,  1146,     1,    -1,    -1,    -1,
      -1,     5,    -1,   146,     8,    -1,    10,   161,    -1,    -1,
      75,    76,    -1,    78,    79,    80,    -1,    -1,   161,    84,
      -1,    -1,    -1,    -1,    -1,    29,    -1,    32,  1179,    -1,
      34,    -1,    -1,    38,    39,    -1,    -1,    -1,    -1,    -1,
      45,    -1,    47,    48,    -1,    -1,    -1,    -1,    -1,    -1,
      55,    56,    57,    -1,    -1,    60,    -1,    -1,    63,   124,
     125,   126,   127,   128,   129,   130,    -1,   132,    -1,    -1,
      -1,    75,    76,    -1,    78,    79,    80,    -1,    -1,    -1,
      84,   146,    87,    -1,    -1,    90,    -1,    92,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   161,    -1,    -1,   104,
     105,    -1,    -1,    -1,    -1,   110,     1,    -1,  1259,   114,
      -1,    -1,    -1,   118,   119,    -1,    -1,    -1,    -1,  1270,
     124,   125,   126,   127,   128,   129,   130,     1,   132,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    32,    -1,    -1,
      -1,    -1,   146,    38,    39,    -1,    -1,    -1,    -1,    -1,
      45,    -1,    47,    48,    -1,    -1,    -1,   161,    32,  1310,
      55,    56,    57,    -1,    38,    60,    -1,    -1,    63,    -1,
      -1,    45,    -1,    47,    48,    -1,    -1,    -1,    52,    -1,
      -1,    55,    -1,    57,    -1,    -1,    60,    -1,    -1,    63,
      -1,    -1,    87,    -1,    -1,    90,     1,    92,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   104,
     105,    -1,    -1,    87,    -1,   110,    90,    -1,    92,   114,
      -1,    -1,     1,   118,   119,    -1,    -1,    32,    -1,    -1,
     104,   105,    -1,    38,    39,    -1,   110,    -1,    -1,    -1,
     114,    -1,    47,    48,   118,   119,    -1,    52,    -1,    -1,
      55,    -1,    57,    -1,    -1,    60,    35,    -1,    63,    -1,
      -1,    -1,    -1,    -1,     1,    -1,    45,    46,    47,    -1,
      -1,    -1,    -1,    -1,    53,    -1,    55,    -1,    57,    -1,
      -1,    60,    87,    -1,    -1,    90,    -1,    92,    -1,    68,
      -1,    -1,    71,    -1,    -1,    32,    -1,    -1,    -1,   104,
     105,    38,    -1,    -1,    -1,   110,    -1,    -1,    87,   114,
      47,    90,    -1,   118,   119,    52,    -1,    -1,    55,    -1,
      57,    -1,    -1,    60,    -1,   104,    63,    -1,    -1,     1,
      -1,   110,    -1,    -1,    -1,   114,    -1,   116,    -1,    -1,
     119,     1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    -1,    -1,    90,    -1,    92,    -1,    -1,    -1,    -1,
      32,    -1,    -1,    -1,    -1,    -1,    38,    39,   105,    -1,
      -1,    -1,    32,   110,    -1,    47,    -1,   114,    38,    -1,
      -1,   118,   119,    55,    -1,    57,    -1,    47,    60,    -1,
      -1,    63,    52,    32,    -1,    55,    -1,    57,    -1,    38,
      60,    -1,    -1,    63,    -1,    -1,    45,    -1,    47,    48,
      -1,    -1,    -1,    -1,    -1,    87,    55,    56,    90,    -1,
      92,    60,    -1,    -1,    63,    -1,    -1,    87,    -1,    -1,
      90,    -1,    92,   105,    -1,    -1,    -1,    -1,   110,    -1,
      -1,    -1,   114,    -1,    -1,   105,   118,   119,    87,    -1,
     110,    90,    -1,    92,   114,    -1,    -1,    -1,   118,   119,
      -1,    32,    -1,    -1,    -1,   104,   105,    38,    -1,    -1,
      -1,   110,    32,    -1,    45,   114,    47,    48,    38,   118,
     119,    -1,    -1,    -1,    55,    -1,    -1,    47,    48,    60,
      -1,    -1,    63,    -1,    -1,    55,    -1,    -1,    -1,    -1,
      60,    -1,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    -1,    -1,    90,
      -1,    92,    -1,    -1,    -1,    -1,    -1,    87,    -1,    -1,
      90,    -1,    92,   104,   105,    -1,    -1,    -1,    -1,   110,
      32,    -1,    -1,   114,   104,   105,    38,   118,   119,    -1,
     110,    -1,    -1,    -1,   114,    47,    -1,    -1,   118,   119,
      -1,    -1,    -1,    55,    -1,    -1,    -1,    -1,    60,    35,
      -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    -1,    -1,    -1,    -1,    53,    -1,    55,
      -1,    57,    -1,    -1,    60,    87,    -1,    -1,    90,    -1,
      92,    -1,    68,    -1,    -1,    71,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   105,    -1,    -1,    -1,    -1,   110,    -1,
      -1,    87,   114,    -1,    90,    -1,   118,   119,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   104,    -1,
      -1,    -1,    -1,    -1,   110,    -1,    -1,    -1,   114,    -1,
     116,    -1,    -1,   119
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned short yystos[] =
{
       0,     1,   165,   203,     5,     8,    10,    29,    34,    75,
      76,    78,    79,    80,    84,   124,   125,   126,   127,   128,
     129,   130,   132,   146,   161,   472,   473,   474,   475,   476,
     477,   479,   481,   483,   485,   486,   487,   488,   489,   490,
     536,   538,   539,   540,   541,   542,   543,   544,   545,   546,
     556,   557,   558,   563,     0,   204,   205,     1,    85,   460,
     463,   466,   472,   481,   491,   492,   493,   494,   541,   561,
     483,   483,   487,   541,   542,   487,   487,    47,   104,   119,
      34,    84,   125,   124,    34,    75,    78,    84,   124,   125,
      15,    16,    17,    25,    26,    27,   147,   148,   149,   150,
     151,   152,   478,     3,     8,    10,   100,   101,   106,   107,
     108,   109,   480,   482,     7,    12,    74,    97,   484,   487,
      23,   541,   556,    20,   452,     4,     5,    11,   206,   211,
     591,    49,   112,   461,     6,     9,    18,    19,    22,    93,
     457,    11,   163,   164,   547,   548,   549,   550,   551,   552,
     547,   547,   477,   477,   477,   477,   477,   477,   477,   477,
     477,   477,   479,   481,   483,   486,   487,     4,     1,   503,
     527,   541,   561,    93,   110,   489,   554,   563,     1,   446,
     447,   591,    33,   538,   539,   559,   560,   563,    35,    46,
      53,    69,    87,   118,   153,   208,   209,   210,   212,   213,
     214,   215,   219,   231,   237,   256,   263,   268,   481,     1,
     492,   481,   494,   494,   472,    28,   459,   460,   481,   543,
     541,   541,    13,   541,    11,    13,    13,    99,   453,     9,
       5,     6,     9,    65,    83,   448,   449,   450,   451,   463,
     472,   481,   541,   130,   587,   130,   588,   130,   586,   130,
     529,   568,    41,   130,   589,     1,   518,   541,   544,   130,
     217,   518,   134,   592,   592,     5,   464,   465,   466,   467,
     468,   469,   541,   563,   164,   464,   464,   561,    21,   561,
     446,   447,   450,   592,    22,    81,    81,    67,     9,    14,
     130,   590,    67,     9,    14,    67,    14,   467,   470,   471,
     563,   162,   561,   162,   162,     6,   450,   562,   563,   238,
     220,   568,    67,    76,   257,   544,   216,     6,     6,     9,
     467,   563,   457,    67,    11,   562,    59,   221,   223,   498,
     264,   269,   223,   258,   207,   471,   563,   232,   563,    67,
       5,    14,    88,   224,   499,     1,   265,   541,   564,    59,
     444,     1,   260,    52,   206,   591,     1,   233,   239,   222,
       5,     1,   225,    52,   266,   591,   442,   444,    73,    14,
      52,   261,   591,   153,   218,   153,    39,   234,   591,     1,
     240,   325,   519,   592,   519,    39,   226,   228,   591,    87,
     504,    32,    38,    47,    55,    60,    63,    90,    92,   105,
     110,   114,   210,   256,   263,   267,   268,   270,   274,   276,
     283,   284,   308,   310,   311,   312,   316,   317,   320,   323,
     505,   514,    14,     5,    87,   510,    45,    48,    87,   104,
     210,   256,   262,   268,   274,   276,   283,   284,   308,   309,
     310,   311,   312,   314,   316,   317,   319,   320,   323,   495,
     563,     1,   236,    56,   210,   235,   256,   263,   268,   274,
     276,   283,   284,   308,   309,   310,   311,   312,   314,   316,
     317,   318,   319,   320,   323,   241,   243,   591,     6,    14,
      47,    55,    87,    90,   104,   114,   119,   326,   327,   328,
     331,   332,   333,   338,   505,     6,   229,    52,   210,   227,
     256,   263,   268,   274,   276,   283,   284,   308,   309,   310,
     311,   312,   316,   317,   319,   320,   323,    41,   495,   128,
     130,   132,   584,   130,   554,   578,     1,   130,   532,   573,
       1,   130,   534,   576,   130,   579,   580,   130,   582,   130,
     570,   130,   571,   592,    14,    67,    57,   119,   446,   495,
     130,   577,     1,    33,    85,   343,   523,   541,   555,     1,
     130,   531,   574,   592,    14,    52,   344,   591,   342,   523,
     592,    52,    38,    56,    60,   210,   242,   244,   317,   323,
     592,   589,   582,   130,   572,   592,     1,   130,   530,   569,
      67,   336,    57,    14,   230,   344,    53,   509,   592,    14,
      13,   313,    81,    13,     9,    13,     9,    13,    67,    13,
     271,    67,    14,   285,     1,    76,   279,   128,   130,   581,
       1,   130,   533,   575,     6,    14,    67,   512,    31,     9,
      13,   452,   497,     9,    13,    35,   508,    37,    89,    91,
     123,   130,   345,   346,   347,   348,   350,   372,   373,   374,
     384,   392,   403,   489,   541,   585,   118,   440,   442,    13,
      46,   511,   246,   541,   130,   592,   592,   325,    67,   334,
       9,    13,    28,   337,   541,   581,   495,   464,    67,   341,
     523,   561,   573,   464,   576,   464,     5,   324,   541,   221,
     259,   464,    67,   128,   563,   565,    39,   275,   280,   591,
     272,     9,    13,   315,   472,   555,   561,   574,   464,   495,
     472,   346,   541,     5,   394,   472,   592,    27,    14,    13,
      46,    53,    83,   441,    14,    88,   443,   445,   541,   495,
     245,    76,   155,   278,   569,    42,    64,    66,    70,    86,
     329,   335,    14,   541,    67,    13,    14,    24,   517,    83,
     454,   455,   501,     1,    35,    45,    46,    47,    53,    55,
      57,    60,    68,    71,    87,    90,   104,   110,   114,   116,
     119,   321,   322,   522,    14,   442,   278,    14,     5,    30,
      36,    55,    94,   138,   286,   287,   288,   289,   290,   293,
     294,   295,   298,   306,   457,   535,   452,   497,     1,   282,
     210,   256,   263,   268,   274,   276,   281,   283,   284,   308,
     309,   310,   311,   312,   316,   317,   320,   323,   259,   575,
     464,   498,    14,    43,    95,   507,    14,    98,   405,    14,
       1,    33,   395,   524,   541,   512,   102,    61,   375,   516,
      40,    44,    45,    46,    53,    56,    62,    89,   346,   349,
     351,   352,   353,   355,   356,   357,   363,   541,   541,   541,
     442,    73,    14,   247,   339,     5,   277,   464,   278,   497,
     472,   322,   472,    14,   472,    67,    14,    28,   513,     6,
       9,   277,     1,   130,   132,   520,   583,   464,     5,   458,
      81,   291,    41,   299,    14,   116,   442,    52,   400,   591,
     592,   278,   517,   499,   517,   472,   103,   406,     6,     6,
       9,   393,   157,   429,     1,   115,   377,   381,   382,   383,
     472,    65,    96,   113,   376,   502,     5,   391,   472,   541,
     541,   541,   354,   472,   563,   346,   541,   442,    58,    58,
      50,    51,    52,   358,   359,   360,   361,   362,   443,     5,
       1,   248,   249,   591,   541,   519,   507,   277,    14,    14,
     329,    14,   321,     6,     9,     1,   460,   462,   466,   525,
     541,    81,   561,     1,   130,   292,   526,   528,   567,   307,
     300,   130,   296,   566,    14,    57,    90,   506,    44,    54,
      62,    77,    80,    98,    99,   120,   123,   374,   401,   402,
     403,   404,   407,   411,   412,   413,   414,   415,   416,   417,
     421,   423,   424,   428,   432,   433,   435,   436,   437,   438,
     541,   585,   277,    14,    52,    14,   472,    14,   541,     1,
     396,   374,   541,    14,    14,   121,     9,    31,   472,   472,
     512,    58,   563,    13,    14,   443,   367,   368,   367,   563,
     472,   563,    58,   495,    58,    58,   446,    52,   243,   250,
     251,   591,   210,    59,   340,   444,     6,   517,    99,   456,
     472,   583,     6,     9,   464,     1,    52,   292,     9,    13,
     301,     1,    52,   302,   591,     1,   521,    14,   500,   538,
     553,   560,   563,   429,   495,   472,   495,    14,   472,   472,
     496,    82,   408,   472,    24,    27,   592,    56,   122,   434,
      14,    13,    99,   273,    45,   275,   397,   591,    27,   472,
     383,   472,    65,     6,   385,     1,   121,   364,   365,   366,
      64,   472,    14,    52,     1,   369,   370,   371,    13,    13,
     495,    14,   367,   367,     6,    56,    56,   252,   592,    73,
     561,   462,    94,   567,   464,     1,    52,   303,   591,   138,
     210,   270,   274,   304,   317,    52,   297,   566,    14,   472,
     121,   439,   111,   439,   406,    14,   395,   117,   409,   102,
     418,   472,   159,   160,   502,     1,   563,   472,    72,   402,
     561,   495,     1,   399,   210,   256,   263,   268,   274,   276,
     283,   284,   308,   310,   311,   312,   316,   317,   320,   323,
     398,   375,   378,   223,   387,   493,   563,    52,   365,   367,
     462,    14,    58,    39,   234,   344,   472,    14,    52,   358,
      14,   342,   541,   592,     5,   495,    14,   138,   210,   256,
     263,   268,   274,   276,   283,   284,   305,   308,   310,   311,
     312,   316,   317,   320,   323,   495,    67,   592,   116,    16,
      67,   472,    14,     1,   427,    14,    14,   472,    56,   410,
     157,   374,    14,    14,   121,    64,    86,   330,   330,   377,
     381,    64,   427,    14,    52,   400,   592,   380,   381,    50,
     379,   444,   224,   388,    22,    13,    58,   495,    52,   495,
     371,    58,   255,   440,    28,   156,    41,   592,    76,   495,
     536,   537,   541,     1,   121,   430,   431,   400,   425,   472,
      14,   374,    24,    27,   472,   418,   472,    14,    14,    14,
     462,    52,    89,   515,     9,    14,   121,   381,    14,   445,
     386,   493,   495,    14,    58,    14,   495,   253,   254,    14,
       6,     6,   495,   565,   541,    14,   493,    52,   431,    50,
      51,   426,    24,    27,   422,   472,   159,   502,   419,    14,
      14,    72,    91,   381,   493,   121,    14,     1,   389,    22,
      14,    52,   365,   495,    14,    52,   243,   591,   497,    22,
      44,   427,   472,    52,   422,   159,   502,     9,    14,   121,
     330,   380,    50,   420,   495,   495,   121,   472,    39,   234,
      58,    14,    56,    52,   442,   427,   429,   111,    62,    14,
     330,   380,   472,   493,   422,    14,   472,    14,    14,   493,
       1,   390,   495,    14,    56,    14,   495,   427,   495,   422,
      14,   121,    14,   121,    52,   344,    14,    14,    14,    14,
      14,   493,   472,    40,   495,    14
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)		\
   ((Current).first_line   = (Rhs)[1].first_line,	\
    (Current).first_column = (Rhs)[1].first_column,	\
    (Current).last_line    = (Rhs)[N].last_line,	\
    (Current).last_column  = (Rhs)[N].last_column)
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

# define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value, Location);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short *bottom, short *top)
#else
static void
yy_stack_print (bottom, top)
    short *bottom;
    short *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
# define YYDSYMPRINTF(Title, Token, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if defined (YYMAXDEPTH) && YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yytype, yyvaluep, yylocationp)
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  switch (yytype)
    {
      case 128: /* VHDL_STRING */

        { Strings::free(yyvaluep->str) ; };

        break;
      case 129: /* VHDL_BIT_STRING */

        { Strings::free(yyvaluep->str) ; };

        break;
      case 130: /* VHDL_ID */

        { Strings::free(yyvaluep->str) ; };

        break;
      case 132: /* VHDL_CHARACTER */

        { Strings::free(yyvaluep->str) ; };

        break;
      case 225: /* entity_decl_part */

        { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_declnode_array (yyvaluep->array) ; };

        break;
      case 229: /* entity_statement_part */

        { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_statement_array (yyvaluep->array) ; };

        break;
      case 233: /* architecture_decl_part */

        { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_declnode_array (yyvaluep->array) ; };

        break;
      case 236: /* architecture_statement_part */

        { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_statement_array (yyvaluep->array) ; };

        break;
      case 246: /* block_spec */

        { delete (yyvaluep->name) ; };

        break;
      case 273: /* opt_return */

        { delete (yyvaluep->name) ; };

        break;
      case 282: /* subprogram_statement_part */

        { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_statement_array (yyvaluep->array) ; };

        break;
      case 324: /* group_template_name */

        { delete (yyvaluep->name) ; };

        break;
      case 336: /* opt_interface_subprogram_default */

        { delete (yyvaluep->expression) ; };

        break;
      case 337: /* interface_subprogram_default */

        { delete (yyvaluep->expression) ; };

        break;
      case 349: /* instantiated_unit */

        { delete (yyvaluep->name) ; };

        break;
      case 353: /* for_scheme */

        { delete (yyvaluep->iter_scheme) ; };

        break;
      case 356: /* if_scheme */

        { delete (yyvaluep->iter_scheme) ; };

        break;
      case 360: /* elsif_scheme */

        { delete (yyvaluep->iter_scheme) ; };

        break;
      case 362: /* else_scheme */

        { delete (yyvaluep->iter_scheme) ; };

        break;
      case 366: /* case_scheme */

        { delete (yyvaluep->iter_scheme) ; };

        break;
      case 374: /* target */

        { delete (yyvaluep->expression) ; };

        break;
      case 383: /* waveform_element */

        { delete (yyvaluep->expression) ; };

        break;
      case 391: /* opt_guard_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 399: /* process_statement_part */

        { VhdlTreeNode::_present_scope->ResetPredefinedOps() ; cleanup_statement_array (yyvaluep->array) ; };

        break;
      case 405: /* opt_report_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 406: /* opt_severity_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 409: /* opt_condition_clause */

        { delete (yyvaluep->expression) ; };

        break;
      case 410: /* opt_timeout_clause */

        { delete (yyvaluep->expression) ; };

        break;
      case 434: /* opt_iteration_scheme */

        { delete (yyvaluep->iter_scheme) ; };

        break;
      case 439: /* opt_when_condition */

        { delete (yyvaluep->expression) ; };

        break;
      case 441: /* entity_aspect */

        { delete (yyvaluep->name) ; };

        break;
      case 447: /* assoc_element */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 448: /* plain_assoc_element */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 449: /* formal_part */

        { delete (yyvaluep->name) ; };

        break;
      case 450: /* actual_part */

        { delete (yyvaluep->expression) ; };

        break;
      case 451: /* actual_designator */

        { delete (yyvaluep->expression) ; };

        break;
      case 453: /* opt_return_type */

        { delete (yyvaluep->name) ; };

        break;
      case 455: /* opt_file_open */

        { delete (yyvaluep->expression) ; };

        break;
      case 456: /* file_logical_name */

        { delete (yyvaluep->expression) ; };

        break;
      case 457: /* range_constraint */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 459: /* range */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 460: /* range_subset1 */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 462: /* discrete_range */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 463: /* discrete_range_subset1 */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 467: /* resolution_indication */

        { delete (yyvaluep->name) ; };

        break;
      case 468: /* array_resolution_indication */

        { delete (yyvaluep->name) ; };

        break;
      case 469: /* record_resolution_indication */

        { delete (yyvaluep->name) ; };

        break;
      case 471: /* record_resolution_element */

        { delete (yyvaluep->expression) ; };

        break;
      case 472: /* expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 473: /* AND_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 474: /* OR_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 475: /* XOR_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 476: /* XNOR_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 477: /* relation */

        { delete (yyvaluep->expression) ; };

        break;
      case 479: /* shift_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 481: /* simple_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 483: /* term */

        { delete (yyvaluep->expression) ; };

        break;
      case 486: /* factor */

        { delete (yyvaluep->expression) ; };

        break;
      case 487: /* primary */

        { delete (yyvaluep->expression) ; };

        break;
      case 488: /* literal */

        { delete (yyvaluep->name) ; };

        break;
      case 489: /* aggregate */

        { delete (yyvaluep->expression) ; };

        break;
      case 490: /* allocator */

        { delete (yyvaluep->expression) ; };

        break;
      case 492: /* element_assoc */

        { delete (yyvaluep->expression) ; };

        break;
      case 494: /* choice */

        { delete (yyvaluep->discrete_range) ; };

        break;
      case 495: /* opt_id */

        { delete (yyvaluep->id) ; };

        break;
      case 496: /* opt_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 500: /* opt_designator */

        { delete (yyvaluep->designator) ; };

        break;
      case 517: /* opt_init_assign */

        { delete (yyvaluep->expression) ; };

        break;
      case 520: /* enumeration_literal_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 528: /* element_id_def_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 529: /* library_id_def_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 530: /* interface_id_def_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 531: /* signal_id_def_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 532: /* constant_id_def_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 533: /* variable_id_def_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 534: /* file_id_def_list */

        { cleanup_iddef_array (yyvaluep->array) ; };

        break;
      case 536: /* abstract_literal */

        { delete (yyvaluep->name) ; };

        break;
      case 537: /* physical_literal */

        { delete (yyvaluep->name) ; };

        break;
      case 538: /* character_literal */

        { delete (yyvaluep->designator) ; };

        break;
      case 539: /* string_literal */

        { delete (yyvaluep->designator) ; };

        break;
      case 540: /* bit_string_literal */

        { delete (yyvaluep->name) ; };

        break;
      case 541: /* name */

        { delete (yyvaluep->name) ; };

        break;
      case 542: /* qualified_expression */

        { delete (yyvaluep->expression) ; };

        break;
      case 543: /* attribute_name */

        { delete (yyvaluep->name) ; };

        break;
      case 544: /* selected_name */

        { delete (yyvaluep->name) ; };

        break;
      case 545: /* indexed_name */

        { delete (yyvaluep->name) ; };

        break;
      case 546: /* external_name */

        { delete (yyvaluep->name) ; };

        break;
      case 553: /* designator */

        { delete (yyvaluep->designator) ; };

        break;
      case 554: /* attribute_designator */

        { delete (yyvaluep->id) ; };

        break;
      case 555: /* entity_designator */

        { delete (yyvaluep->name) ; };

        break;
      case 556: /* prefix */

        { delete (yyvaluep->name) ; };

        break;
      case 557: /* simple_prefix */

        { delete (yyvaluep->name) ; };

        break;
      case 558: /* signatured_prefix */

        { delete (yyvaluep->name) ; };

        break;
      case 559: /* suffix */

        { delete (yyvaluep->designator) ; };

        break;
      case 560: /* operator_symbol */

        { delete (yyvaluep->designator) ; };

        break;
      case 561: /* type_mark */

        { delete (yyvaluep->name) ; };

        break;
      case 562: /* entity_name */

        { delete (yyvaluep->id) ; };

        break;
      case 563: /* id */

        { delete (yyvaluep->id) ; };

        break;
      case 564: /* uninstantiated_package_name */

        { delete (yyvaluep->name) ; };

        break;
      case 565: /* uninstantiated_subprog_name */

        { delete (yyvaluep->name) ; };

        break;
      case 570: /* subtype_id_def */

        { if ((yyvaluep->id_def) && (yyvaluep->id_def)->IsIncompleteType()) (yyvaluep->id_def)->Error("missing full type definition for %s", (yyvaluep->id_def)->Name()) ; vhdl_undeclare (yyvaluep->id_def) ; if ((yyvaluep->id_def)==last_label) last_label = 0 ; delete (yyvaluep->id_def) ; };

        break;
      case 571: /* type_id_def */

        { if ((yyvaluep->id_def) && (yyvaluep->id_def)->IsIncompleteType()) (yyvaluep->id_def)->Error("missing full type definition for %s", (yyvaluep->id_def)->Name()) ; vhdl_undeclare (yyvaluep->id_def) ; if ((yyvaluep->id_def)==last_label) last_label = 0 ; delete (yyvaluep->id_def) ; };

        break;
      case 583: /* enumeration_id_def */

        { if ((yyvaluep->id_def) && (yyvaluep->id_def)->IsIncompleteType()) (yyvaluep->id_def)->Error("missing full type definition for %s", (yyvaluep->id_def)->Name()) ; vhdl_undeclare (yyvaluep->id_def) ; if ((yyvaluep->id_def)==last_label) last_label = 0 ; delete (yyvaluep->id_def) ; };

        break;

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;
/* Location data for the lookahead symbol.  */
YYLTYPE yylloc;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
  YYLTYPE *yylerrsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
	YYSTACK_RELOCATE (yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
  *++yylsp = yylloc;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, yylsp - yylen, yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 4:

    {
              // Expression Parsing special token to resolve conflicts
              vhdl_file::SetParsedExpr(yyvsp[0].expression) ;
          }
    break;

  case 7:

    { create_context_clause() ; }
    break;

  case 8:

    { if (context_clause && yyvsp[0].declaration) context_clause->InsertLast(yyvsp[0].declaration) ; }
    break;

  case 9:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 10:

    { yyval.array = 0 ; }
    break;

  case 11:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : ((yyvsp[0].declaration) ? new Array():0) ; if (yyval.array && yyvsp[0].declaration) (yyval.array)->InsertLast(yyvsp[0].declaration) ; }
    break;

  case 14:

    {
              /* VHDL 2008 LRM section 13.4 */
              yyval.declaration = new VhdlContextReference(yyvsp[-1].array) ;
          }
    break;

  case 15:

    { yyval.declaration = new VhdlLibraryClause(yyvsp[-1].array, 0 /*implicitly declared*/) ; }
    break;

  case 16:

    { yyval.declaration = new VhdlUseClause(yyvsp[-1].array, 0 /*implicitly declared*/) ; }
    break;

  case 17:

    {
            yyval.library_unit = yyvsp[-1].library_unit ;
            add_pre_comments_to_node(yyval.library_unit, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.library_unit, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 27:

    {
              /* TODO: This is a DesignUnit. Need to rearrange the rules */
              /* VHDL 2008 LRM section 13.3 */
              if (context_clause && context_clause->Size() > 1/*anything apart from implicit*/) yyvsp[-1].id_def->Error("non empty context clause cannot precede a context declaration") ; // Viper 6757 : LRM 1076-2008 Section : 13.1

              cleanup_context_clause() ; // ignore preceding use clauses, if any
              // VIPER #6341 : Scope of context decl is now created earlier.
              // So we have already created library id 'std' and 'work' there.
              // Undeclare those first and then set owner
              VhdlTreeNode::_present_scope->ResetThisScope() ;
              // Create cross-link between the scope and the id that owns it.
              // Could not do this during construction time.
              VhdlTreeNode::_present_scope->SetOwner(yyvsp[-1].id_def) ;

              //VhdlScope::Push($2) ;
              /* coverity[resource_leak] */
              context_clause = new Array() ; // not so clean
              // Declare the identifier (this makes it visible) :
              vhdl_declare(yyvsp[-1].id_def) ;
          }
    break;

  case 28:

    {
              VhdlContextDecl *context_decl = new VhdlContextDecl(yyvsp[-4].array, yyvsp[-7].id_def, VhdlScope::Pop()) ;
              cleanup_context_clause() ; // not so clean
              context_decl->ClosingLabel(yyvsp[-1].id) ;

              /* Add the unit to the library */
              VhdlLibrary *work = vhdl_file::GetWorkLib() ;

              yyval.library_unit = (work->AddPrimUnit(context_decl)) ? context_decl : 0 ;
          }
    break;

  case 29:

    { yyval.id_def = new VhdlContextId(yyvsp[0].str) ; }
    break;

  case 30:

    { yyval.unsigned_number=0; }
    break;

  case 31:

    { yyval.unsigned_number=VHDL_context ; }
    break;

  case 32:

    {
            inside_design_unit++ ;
            //create_context_clause() ;

            // Create cross-link between the scope and the id that owns it.
            // Could not do this during construction time.
            VhdlTreeNode::_present_scope->SetOwner(yyvsp[-1].id_def) ;

            /* Include std.standard.all ; with a use-clause */
            VhdlName *n = new VhdlSelectedName(
                        new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                        new VhdlIdRef(Strings::save("standard"))),
                        new VhdlAll()) ;
            Array *use_clauses = new Array() ;
            use_clauses->InsertLast(n) ;
            VERIFIC_ASSERT(context_clause) ; // By design
            (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;

            (yyvsp[-1].id_def)->Info("analyzing entity %s",(yyvsp[-1].id_def)->Name()) ;

            // Declare the identifier (this makes it visible) :
            vhdl_declare(yyvsp[-1].id_def) ;
            process_sequence_num = 0 ; // VIPER #7494: Reset process sequence number
        }
    break;

  case 33:

    {
            VhdlEntityDecl *unit = new VhdlEntityDecl(context_clause,yyvsp[-10].id_def,yyvsp[-7].array,yyvsp[-6].array,yyvsp[-5].array,yyvsp[-4].array,VhdlScope::Pop()) ;
            context_clause = 0 ;
            inside_design_unit-- ;
            unit->ClosingLabel(yyvsp[-1].id) ;

            /* Add the unit to the library */
            VhdlLibrary *work = vhdl_file::GetWorkLib() ;

            yyval.library_unit = (work->AddPrimUnit(unit)) ? unit : 0 ;

            // Viper 7168: Do not proceed parsing this file if an error occurred
            // and the primary unit was not inserted in the library
            // Proceeding would only cause avalange of errors in the architecture(s)
            if (!(yyval.library_unit) && Message::ErrorCount()) { // Do not abort if there was no error reported
                YYABORT ; // stop the parser
            }
        }
    break;

  case 34:

    { in_generic_clause = 1 ; }
    break;

  case 35:

    {
            // Set the interface list objects to be 'generics' :
            Array *ids ;
            VhdlInterfaceDecl *decl ;
            VhdlIdDef *id ;
            unsigned i, j ;
            FOREACH_ARRAY_ITEM(yyvsp[-1].array, i, decl) {
                ids = decl->GetIds() ; // collect all identifiers in this list
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    id->SetObjectKind(VHDL_generic) ;
                }
            }
            in_generic_clause = 0 ;
            // Return the interface list itself..
            yyval.array = yyvsp[-1].array ;
        }
    break;

  case 36:

    {
            yyval.array = yyvsp[-1].array ;
        }
    break;

  case 37:

    {
            // Set the interface list objects to be 'ports' :
            Array *ids ;
            VhdlInterfaceDecl *decl ;
            VhdlIdDef *id ;
            unsigned i, j ;
            FOREACH_ARRAY_ITEM(yyvsp[-2].array, i, decl) {
                if (decl->IsInterfaceTypeDecl() || decl->IsInterfaceSubprogDecl() || decl->IsInterfacePackageDecl()) {
                    // Viper 7124 : Vhdl 2008, type, subprogram, package cannot  be used as port
                    decl->Error("%s cannot be used as a port",decl->IsInterfaceTypeDecl() ? "type" : decl->IsInterfaceSubprogDecl() ? "subprogram" : "package" ) ;
                }
                ids = decl->GetIds() ; // collect all identifiers in this list
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    id->SetObjectKind(VHDL_port) ;
                }
            }
            // Return the interface list itself..
            yyval.array = yyvsp[-2].array ;
        }
    break;

  case 38:

    { yyval.array = 0 ; }
    break;

  case 39:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].declaration) (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 40:

    { yyval.array = 0 ; }
    break;

  case 41:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
            if (yyvsp[-1].declaration && yyvsp[-1].declaration->IsPackageBody()) yyval.declaration = 0 ; // Package body will not be added in decl list
        }
    break;

  case 57:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 58:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 59:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 60:

    { yyval.array = 0 ; }
    break;

  case 61:

    { yyval.array = yyvsp[0].array ; }
    break;

  case 62:

    { yyval.array = new Array() ; }
    break;

  case 63:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].statement) (yyval.array)->InsertLast(yyvsp[0].statement) ; }
    break;

  case 64:

    {  CreateAndAddImplicitLabel(yyvsp[0].statement) ;}
    break;

  case 65:

    {
            inside_design_unit++ ;
            //create_context_clause() ;

            // Create cross-link between the scope and the id that owns it.
            // Could not do this during construction time.
            VhdlTreeNode::_present_scope->SetOwner(yyvsp[-3].id_def) ;

            VhdlLibrary *work = vhdl_file::GetWorkLib() ;
            // VIPER #7361: Find unit without modifying the name
            VhdlPrimaryUnit *entity = work->GetPrimUnit((yyvsp[-1].id)->Name(), 1, 0 /* do not modify name*/) ;

            if (!entity) {
                (yyvsp[-1].id)->Error("entity %s is not yet compiled", (yyvsp[-1].id)->Name()) ;
            }

            (yyvsp[-3].id_def)->Info("analyzing architecture %s",(yyvsp[-3].id_def)->Name()) ;

            if (entity) {
                if (!entity->Id()->IsEntity()) {
                    (yyvsp[-1].id)->Error("%s is not an entity", (yyvsp[-1].id)->Name()) ;
                } else {
                    // Store entity in the architecture id
                    (yyvsp[-3].id_def)->DeclareArchitectureId(entity->Id()) ;
                    // And link the declarative region.
                    VhdlTreeNode::_present_scope->AddDeclRegion(entity->LocalScope()) ;
                }
            }

            // Declare the identifier (this makes it visible) :
            vhdl_declare(yyvsp[-3].id_def) ;

            if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                // VIPER #7494: Process sequence number for architecture's concurent
                // statements should be one greater than the last process sequence
                // number used in corresponding entity.
                // So determine the process sequence number of architecture
                process_sequence_num = 0 ;
                VhdlIdDef *existing_implicit_label = 0 ;
                VhdlScope *entity_scope = entity ? entity->LocalScope(): 0 ;
                if (entity_scope) {
                    // Get the last used process sequence number in entity
                    int count = -1 ;
                    do {
                        count++ ;
                        char name[20] ;
                        sprintf(name, "_p%d", count) ;
                        existing_implicit_label = entity_scope->FindSingleObjectLocal(name) ;
                    } while (existing_implicit_label) ;
                    process_sequence_num = count ; // sequence number of architecture's stmts
                }
            }
        }
    break;

  case 66:

    {
            VhdlArchitectureBody *unit = new VhdlArchitectureBody(context_clause,yyvsp[-11].id_def,yyvsp[-9].id,yyvsp[-6].array,yyvsp[-4].array,VhdlScope::Pop()) ;
            context_clause = 0 ;
            inside_design_unit-- ;
            process_sequence_num = 0 ; // VIPER #7494: Reset process sequence number
            unit->ClosingLabel(yyvsp[-1].id) ;

            VhdlLibrary *work = vhdl_file::GetWorkLib() ;
            // VIPER #7361: Find unit without modifying the name
            VhdlPrimaryUnit *entity = work->GetPrimUnit((yyvsp[-9].id)->Name(), 1, 0 /* do not modify name*/) ;
            // Add the unit to the entity
            if (entity) {
                // If addition fails, the unit is now removed. So invalidate the return value.
                unit = (entity->AddSecondaryUnit(unit)) ? unit : 0 ;
                yyval.library_unit = unit ;
            } else {
                // If not there, this unit is unbound.
                // That's dangerous, since it's hanging in space, without an owner.
                // Delete it in that case.
                delete unit ;
                yyval.library_unit = 0 ;
            }
        }
    break;

  case 67:

    { yyval.array = 0 ; }
    break;

  case 68:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].declaration) (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 69:

    { yyval.array = 0 ; }
    break;

  case 70:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
            if (yyvsp[-1].declaration && yyvsp[-1].declaration->IsPackageBody()) yyval.declaration = 0 ; // Package body will not be added in decl list
        }
    break;

  case 88:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 89:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 90:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 91:

    { yyval.array = 0 ; }
    break;

  case 92:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].statement) (yyval.array)->InsertLast(yyvsp[0].statement) ; if (yyvsp[0].statement) CreateAndAddImplicitLabel(yyvsp[0].statement) ; }
    break;

  case 93:

    { yyval.array = 0 ; }
    break;

  case 94:

    {
            inside_design_unit++ ;
            //create_context_clause() ;

                // Create cross-link between the scope and the id that owns it.
                // Could not do this during construction time.
                VhdlTreeNode::_present_scope->SetOwner(yyvsp[-1].id_def) ;

                    /* Include std.standard.all ; with a use-clause */
                VhdlName *n = new VhdlSelectedName(
                            new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                            new VhdlIdRef(Strings::save("standard"))),
                            new VhdlAll()) ;
                Array *use_clauses = new Array() ;
                use_clauses->InsertLast(n) ;
                VERIFIC_ASSERT(context_clause) ; // By design
                (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;

                // Declare the identifier (this makes it visible) :
                vhdl_declare(yyvsp[-1].id_def) ;
        }
    break;

  case 95:

    {
                (yyvsp[-4].id_def)->Info("analyzing configuration %s", (yyvsp[-4].id_def)->Name()) ;

                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                // VIPER #7361: Vhdl does not allow name modification for searching
                VhdlPrimaryUnit *entity = work->GetPrimUnit((yyvsp[-1].id)->Name(), 1, 0 /* do not modify name*/) ;
                if (!entity) {
                    (yyvsp[-1].id)->Error("entity %s is not yet compiled", (yyvsp[-1].id)->Name()) ;
                } else if (!entity->Id()->IsEntity()) {
                    (yyvsp[-1].id)->Error("%s is not an entity", (yyvsp[-1].id)->Name()) ;
                } else {
                    // Store entity in the configuration id
                    (yyvsp[-4].id_def)->DeclareConfigurationId(entity->Id()) ;

                    // Register the dependency of the configuration on the entity :
                    VhdlTreeNode::_present_scope->Using(entity->LocalScope()) ;

                    // Block spec in block configuration will find the architecture
                    // in the entity by scope selection.
                }
        }
    break;

  case 96:

    {
                VhdlConfigurationDecl *unit = new VhdlConfigurationDecl(context_clause,yyvsp[-11].id_def,yyvsp[-8].id,yyvsp[-5].array,yyvsp[-4].block_configuration,VhdlScope::Pop()) ;
                context_clause = 0 ;
                inside_design_unit-- ;
                unit->ClosingLabel(yyvsp[-1].id) ;

                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                yyval.library_unit = (work->AddPrimUnit(unit)) ? unit : 0 ;

                // Viper 7168: Do not proceed parsing this file if an error occurred
                // and the primary unit was not inserted in the library
                // Proceeding would only cause avalange of errors in later units
                if (!(yyval.library_unit) && Message::ErrorCount()) { // Do not abort if there was no error reported
                     YYABORT ; // stop the parser
                }
        }
    break;

  case 97:

    { yyval.array = 0 ; }
    break;

  case 98:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].declaration) ; }
    break;

  case 99:

    { yyval.array = 0 ; }
    break;

  case 100:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 104:

    {
            yyval.block_configuration = yyvsp[-1].block_configuration ;
            add_pre_comments_to_node(yyval.block_configuration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.block_configuration, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 105:

    {
          // LRM 1.3.1 : external/internal block configuration :
          // Need to find the block_spec (architecture or block) first,
          // and extend the present scope with its scope
          VhdlIdDef *owner = (yyvsp[0].name) ? (yyvsp[0].name)->BlockSpec() : 0 ;

          VhdlScope::Push() ;

          // LRM 10.4 : declarations of a configured block are visible in the block configuration.
          if (owner) VhdlTreeNode::_present_scope->AddDeclRegion(owner->LocalScope()) ;
        }
    break;

  case 106:

    {
          yyval.block_configuration = new VhdlBlockConfiguration(yyvsp[-6].name,yyvsp[-4].array,yyvsp[-3].array,VhdlScope::Pop()) ;
        }
    break;

  case 107:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 108:

    { yyval.array = 0 ; }
    break;

  case 109:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].declaration) ; }
    break;

  case 110:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 111:

    { yyval.array = 0 ; }
    break;

  case 112:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].configuration_item) ; }
    break;

  case 113:

    { yyval.array = 0 ; }
    break;

  case 114:

    { yyval.configuration_item = yyvsp[0].block_configuration ; }
    break;

  case 115:

    { yyval.configuration_item = yyvsp[0].component_configuration ; }
    break;

  case 116:

    {
            yyval.component_configuration = yyvsp[-1].component_configuration ;
            add_pre_comments_to_node(yyval.component_configuration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.component_configuration, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 117:

    {
            // Component configuration WITHOUT an inner block_configuration.

            // Create scope for the component configuration (LRM 10.1 tells us one is needed, although no declarations can be in here).
            VhdlScope::Push() ;

            // Resolve the component spec with its binding indication.
            if (yyvsp[-1].specification) (void)(yyvsp[-1].specification)->ResolveSpecs(yyvsp[0].binding_indication,1/*incremental*/) ;
            // No need to make secondary units (of the primary binding) visible, since there is no block-config after this.
        }
    break;

  case 118:

    {
            // plain component configuration without inner block-config.
            yyval.component_configuration = new VhdlComponentConfiguration(yyvsp[-5].specification,yyvsp[-4].binding_indication,0,VhdlScope::Pop()) ;
        }
    break;

  case 119:

    {
            // Component configuration with an inner block_configuration.

            // Create scope for the component configuration (LRM 10.1 tells us one is needed, although no declarations can be in here).
            VhdlScope::Push() ;

            // Resolve the component specification using the (optional) binding indication :
            // We need to do this before we decent into the block configuration, since we
            // need the entity aspect of the primary binding of the component spec's labels
            // so we can make the entity scope visible to the block_configuration below.
            // If we don't do that here, any block configuration will error out (block label (architecture) not found)
            // This solves VIPER 3032/3042.
            // Issue 1957 also needs the full binding (the binding with the entity aspect).
            VhdlIdDef *entity_id = (yyvsp[-1].specification) ? (yyvsp[-1].specification)->ResolveSpecs(yyvsp[0].binding_indication,1/*incremental*/) : 0 ;

            // LRM 1.3.1 : if component is fully bound, we need to make the
            // architecture id (the binding unit) visible for later block config's
            // Get the primary unit from the binding indication around this block,
            // and make all its architectures visible.
            // CHECK ME : Maybe we should only make one architecture visible ? Check 1.3.1.

            // If this is an entity, make its architecture names visible here,
            // so that the block configuration can find them :
            if (entity_id && entity_id->IsEntity()) {
                VhdlPrimaryUnit *entity = entity_id->GetPrimaryUnit() ;

                // Register the dependency of the configuration on the entity :
                VhdlTreeNode::_present_scope->Using(entity->LocalScope()) ;

                // Make the architecture id's of this entity visible in the present scope.
                VhdlSecondaryUnit *arch ;
                Set archs(POINTER_HASH) ;
                MapIter mi ;
                FOREACH_VHDL_SECONDARY_UNIT(entity, mi, arch) {
                    (void) archs.Insert(arch->Id()) ;
                    // Register the dependency of the configuration on the architectures.
                    // CHECK ME : Do we REALLY need to register these dependencies ?
                    // We will only use the architecture name mentioned in the block-config....
                    VhdlTreeNode::_present_scope->Using(arch->LocalScope()) ;
                }
                // Now add these guys as 'use' items, so they become visible in deeper block-configs :
                // So we do something similar to "use lib.prim_name.sec_name"..
                VhdlTreeNode::_present_scope->AddUseClause(archs) ;
            }
        }
    break;

  case 120:

    {
           yyval.component_configuration = new VhdlComponentConfiguration(yyvsp[-6].specification,yyvsp[-5].binding_indication,yyvsp[-3].block_configuration,VhdlScope::Pop()) ;
        }
    break;

  case 121:

    { yyval.binding_indication = 0 ; }
    break;

  case 122:

    { yyval.binding_indication = yyvsp[-1].binding_indication ; }
    break;

  case 123:

    {
            if (inside_design_unit) {
                // Package in other library unit
                VhdlScope::Push() ;
            } else {
                // Package as library unit
                //create_context_clause() ;
            }

                // Create cross-link between the scope and the id that owns it.
                // Could not do this during construction time.
                VhdlTreeNode::_present_scope->SetOwner(yyvsp[-1].id_def) ;

                (yyvsp[-1].id_def)->Info("analyzing package %s", (yyvsp[-1].id_def)->Name()) ;

                // If this is package standard in library std itself,
                // then we should not include std.standard.
                if (!inside_design_unit && (!Strings::compare((yyvsp[-1].id_def)->Name(), "standard") ||
                    !Strings::compare((vhdl_file::GetWorkLib())->Name(), "std"))) {
                    /* Include std.standard.all ; with a use-clause */
                    VhdlName *n = new VhdlSelectedName(
                                new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                                new VhdlIdRef(Strings::save("standard"))),
                                new VhdlAll()) ;
                    Array *use_clauses = new Array() ;
                    use_clauses->InsertLast(n) ;
                    VERIFIC_ASSERT(context_clause) ; // By design
                    (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;
                }
                // Declare the identifier (this makes it visible) :
                if (inside_design_unit) {
                    // This package declaration is in a declarative region of
                    // package/entity/architecture etc. So declare the identifier
                    // in upper scope
                    VhdlScope *old_scope = VhdlTreeNode::_present_scope ;
                    VhdlTreeNode::_present_scope = old_scope->Upper() ;
                    // Viper 6858 : For nested design unit the usual scoping and visibility rules apply
                    VhdlIdDef *exist =  VhdlTreeNode::_present_scope->FindSingleObjectLocal(yyvsp[-1].id_def->Name()) ;
                    VhdlIdDef *scope_owner = VhdlTreeNode::_present_scope->GetOwner() ;
                    // Do not error out for same name package inside a package.
                    // This is checked by $2 ne scope_owner
                    if (exist && exist!=scope_owner && yyvsp[-1].id_def!=scope_owner) {
                        if ((exist->IsSubprogram() || exist->IsEnumerationLiteral()) &&
                            (yyvsp[-1].id_def->IsSubprogram() || yyvsp[-1].id_def->IsEnumerationLiteral())) {
                        } else if (yyvsp[-1].id_def->IsLibrary()) {
                        } else if (exist->IsLibrary()) {
                            // Ignore the existing library identifier. Other declarations always override a library.
                        } else if (exist->IsDesignUnit() || yyvsp[-1].id_def->IsDesignUnit()) {
                            yyvsp[-1].id_def->Error("%s is already declared in this region", exist->Name()) ;
                        }
                    }
                    vhdl_declare(yyvsp[-1].id_def) ;
                    VhdlTreeNode::_present_scope = old_scope ;
                } else {
                    vhdl_declare(yyvsp[-1].id_def) ;
                }
                inside_design_unit++ ;
        }
    break;

  case 124:

    {
                Array *generic_clause = 0 ;
                Array *generic_map_aspect = 0 ;
                if (yyvsp[-5].array) {
                    if ((yyvsp[-5].array)->Size() > 0) generic_clause = (Array *)(yyvsp[-5].array)->At(0) ;
                    if ((yyvsp[-5].array)->Size() > 1) generic_map_aspect = (Array *)(yyvsp[-5].array)->At(1) ;
                    //($2)->Warning("VHDL 1076-2008 construct not yet supported") ;
                    // VIPER #6258 : Produce error if generic list in package appears in
                    // any dialect other than 2008
                    if (!VhdlNode::IsVhdl2008()) {
                        if (yyvsp[-8].id_def) (yyvsp[-8].id_def)->Error("this construct is only supported in VHDL 1076-2008") ;
                    }
                    delete (yyvsp[-5].array) ;
                }
                inside_design_unit-- ;
                VhdlPackageDecl *unit = new VhdlPackageDecl(inside_design_unit ? 0: context_clause, yyvsp[-8].id_def, generic_clause, generic_map_aspect, yyvsp[-4].array, VhdlScope::Pop()) ;
                if (!inside_design_unit) context_clause = 0 ;
                unit->ClosingLabel(yyvsp[-1].id) ;

                if (!inside_design_unit) {
                    // Package is declared as library unit, add it to library
                    VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                    yyval.library_unit = (work->AddPrimUnit(unit)) ? unit : 0 ;
                    // Viper 7168: Do not proceed parsing this file if an error occurred
                    // and the package decl was not inserted in the library
                    // Proceeding would only cause avalange of errors in the package body
                    if (!(yyval.library_unit) && Message::ErrorCount()) { // Do not abort if there was no error reported
                        YYABORT ; // stop the parser
                    }
                } else {
                    yyval.library_unit = unit ;
                    // VIPER #6258 : Produce error if nested package appears in
                    // any dialect other than 2008
                    if (!VhdlNode::IsVhdl2008()) {
                        (yyval.library_unit)->Error("this construct is only supported in VHDL 1076-2008") ;
                    }
                }
        }
    break;

  case 125:

    { yyval.array = 0 ; }
    break;

  case 126:

    {
              yyval.array = new Array() ; if (yyvsp[0].array) (yyval.array)->InsertLast(yyvsp[0].array) ;
          }
    break;

  case 127:

    {
              yyval.array = new Array() ; if (yyvsp[-2].array) (yyval.array)->InsertLast(yyvsp[-2].array) ;
              if (yyvsp[-1].array) (yyval.array)->InsertLast(yyvsp[-1].array) ;
          }
    break;

  case 128:

    { yyval.array = 0 ; }
    break;

  case 129:

    {
              yyval.array = new Array() ;
              if (yyvsp[-1].array) (yyval.array)->InsertLast(yyvsp[-1].array) ;
              if (yyvsp[0].array) (yyval.array)->InsertLast(yyvsp[0].array) ;
          }
    break;

  case 130:

    { yyval.array = 0 ; }
    break;

  case 131:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 132:

    { yyval.array = 0 ; }
    break;

  case 133:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 150:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 151:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 152:

    {
            if (inside_design_unit) {
                VhdlScope::Push() ;
            } else {
                //create_context_clause() ;
            }

            // Create cross-link between the scope and the id that owns it.
            // Could not do this during construction time.
            VhdlTreeNode::_present_scope->SetOwner(yyvsp[-1].id_def) ;

            (yyvsp[-1].id_def)->Info("analyzing package body %s",(yyvsp[-1].id_def)->Name()) ;

            // Find the package declaration :
            // VIPER #6582 : If this declaration is inside any design unit, find the package
            // declaration first in _present_scope
            VhdlPrimaryUnit *entity = 0 ;
            if (inside_design_unit) {
                // Package declaration may be in the declarative part of other construct
                // Find there
                VhdlScope *container = VhdlTreeNode::_present_scope->Upper() ;
                VhdlIdDef *id = container ? container->FindSingleObjectLocal((yyvsp[-1].id_def)->Name()): 0 ;
                entity = id ? id->GetPrimaryUnit(): 0 ;
            } else {
                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                // VIPER #7361: Find package in library without modifying the name
                // 'GetPrimUnit' also searches by unescaping the name if 3 rd argument
                // is 1 (for mixed language design)
                entity = work->GetPrimUnit((yyvsp[-1].id_def)->Name(), 1, 0/* do not modify name for searching*/) ;
            }
            if (!entity) {
                    (yyvsp[-1].id_def)->Error("package declaration %s is not yet compiled", (yyvsp[-1].id_def)->Name()) ;
            } else if (!entity->Id()->IsPackage()) {
                    (yyvsp[-1].id_def)->Error("%s is not a package", (yyvsp[-1].id_def)->Name()) ;
            } else {
                    VhdlTreeNode::_present_scope->AddDeclRegion(entity->LocalScope()) ;
            }

            // VIPER 3080 : Delete the existing package body (if there), or else we cannot link a new body to an unchanged header.
            VhdlSecondaryUnit *exist = (entity) ? entity->GetSecondaryUnit((yyvsp[-1].id_def)->Name()) : 0 ;
            // delete it if it exists. This will uncouple the body-header function links.
            delete exist ;

            // Declare the identifier (this makes it visible) :
            // Never do this for package body ? Only the package decl is visible
            // vhdl_declare($3) ;
            inside_design_unit++ ;
        }
    break;

  case 153:

    {
            inside_design_unit-- ;
            VhdlPackageBody *unit = new VhdlPackageBody(inside_design_unit ? 0: context_clause,yyvsp[-7].id_def,yyvsp[-4].array,VhdlScope::Pop()) ;
            if (!inside_design_unit) context_clause = 0 ;
            unit->ClosingLabel(yyvsp[-1].id) ;

            // VIPER #6582 : If this declaration is inside any design unit, find the package
            // declaration first in _present_scope
            VhdlPrimaryUnit *entity = 0 ;
            if (inside_design_unit){
                // Package declaration may be in the declarative part of other construct
                // Find there
                VhdlScope *container = VhdlTreeNode::_present_scope ;
                VhdlIdDef *id = container ? container->FindSingleObjectLocal((yyvsp[-7].id_def)->Name()): 0 ;
                entity = id ? id->GetPrimaryUnit(): 0 ;
            } else {
                VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                // VIPER #7361: Find package in library without modifying the name
                // 'GetPrimUnit' also searches by unescaping the name if 3 rd argument
                // is 1 (for mixed language design)
                entity = work->GetPrimUnit((yyvsp[-7].id_def)->Name(), 1, 0/* do not modify name for searching*/) ;
            }
            // Add the unit to the package header (entity)
            if (entity) {
                // If addition failed, the unit is now removed. So invalidate the return value.
                yyval.library_unit = (entity->AddSecondaryUnit(unit)) ? unit : 0 ;
            } else {
                // If not there, this unit is unbound.
                // That's dangerous, since it's hanging in space, without an owner.
                // Delete it in that case.
                delete unit ;
                yyval.library_unit = 0 ;
            }
        }
    break;

  case 154:

    { yyval.array = 0 ; }
    break;

  case 155:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].declaration) (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 156:

    { yyval.array = 0 ; }
    break;

  case 157:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
            if (yyvsp[-1].declaration && yyvsp[-1].declaration->IsPackageBody()) yyval.declaration = 0 ; // Package body will not be added in decl list
        }
    break;

  case 163:

    {
             if (!VhdlNode::IsVhdl2008() && yyvsp[0].declaration) {
                 (yyvsp[0].declaration)->Error("this construct is only supported in VHDL 1076-2008") ;
             }
             yyval.declaration = yyvsp[0].declaration ;
          }
    break;

  case 164:

    {
             if (!VhdlNode::IsVhdl2008() && yyvsp[0].declaration) {
                 (yyvsp[0].declaration)->Error("this construct is only supported in VHDL 1076-2008") ;
             }
             yyval.declaration = yyvsp[0].declaration ;
          }
    break;

  case 171:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 172:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 173:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 174:

    {
            if (inside_design_unit) {
                VhdlScope::Push() ;
            } else {
                //create_context_clause() ;
            }
            // Create cross-link between the scope and the id that owns it.
            VhdlTreeNode::_present_scope->SetOwner(yyvsp[-2].id_def) ;

            if (!inside_design_unit) {
                /* Include std.standard.all ; with a use-clause */
                VhdlName *n = new VhdlSelectedName(
                        new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                        new VhdlIdRef(Strings::save("standard"))),
                        new VhdlAll()) ;
                Array *use_clauses = new Array() ;
                use_clauses->InsertLast(n) ;
                VERIFIC_ASSERT(context_clause) ; // By design
                (context_clause)->InsertLast(new VhdlUseClause(use_clauses, 1 /*implicitly declared*/)) ;
            }
            // Declare the identifier (this makes it visible) :
            if (!inside_design_unit) {
                vhdl_declare(yyvsp[-2].id_def) ;
            } else {
                // This package declaration is in a declarative region of
                // package/entity/architecture etc. So declare the identifier
                // in upper scope
                VhdlScope *old_scope = VhdlTreeNode::_present_scope ;
                VhdlTreeNode::_present_scope = old_scope->Upper() ;
                vhdl_declare(yyvsp[-2].id_def) ;
                VhdlTreeNode::_present_scope = old_scope ;
            }
          }
    break;

  case 175:

    {
              VhdlPackageInstantiationDecl *decl ;

              decl = new VhdlPackageInstantiationDecl(inside_design_unit ? 0: context_clause, yyvsp[-6].id_def, yyvsp[-2].name, yyvsp[-1].array, VhdlScope::Pop()) ;
              if (!inside_design_unit) context_clause = 0 ;
              if (!inside_design_unit) {
                  // Package is declared as library unit, add it to library
                  VhdlLibrary *work = vhdl_file::GetWorkLib() ;
                  yyval.library_unit = (work->AddPrimUnit(decl)) ? decl : 0 ;

                  // Viper 7168: Do not proceed parsing this file if an error occurred
                  // and the primary unit was not inserted in the library
                  // Proceeding would only cause avalange of errors in later units
                  if (!(yyval.library_unit) && Message::ErrorCount()) { // Do not abort if there was no error reported
                       YYABORT ; // stop the parser
                  }
               } else {
                  yyval.library_unit = decl ;
               }
          }
    break;

  case 176:

    { VhdlScope::Push(yyvsp[0].id_def) ; (yyvsp[0].id_def)->SetAsProcedure() ; }
    break;

  case 177:

    {
              if (yyvsp[-1].unsigned_number && !yyvsp[0].array) {
                  if (yyvsp[-4].id_def) yyvsp[-4].id_def->Error("parameter should be followed by its list") ;
              }
              Array *generic_clause = 0 ;
              Array *generic_map_aspect = 0 ;
              if (yyvsp[-2].array) { // VHDL-2008 LRM Section 4.2.1
                  if ((yyvsp[-2].array)->Size() > 0) generic_clause = (Array *)(yyvsp[-2].array)->At(0) ;
                  if ((yyvsp[-2].array)->Size() > 1) generic_map_aspect = (Array *)(yyvsp[-2].array)->At(1) ;
                  //($2)->Warning("VHDL 1076-2008 construct not yet supported") ;
                  // VIPER #6258 : Produce error if generic list in subprogram appears in
                  // any dialect other than 2008
                  if (!VhdlNode::IsVhdl2008()) {
                      if (yyvsp[-4].id_def) (yyvsp[-4].id_def)->Error("this construct is only supported in VHDL 1076-2008") ;
                  }
                  delete (yyvsp[-2].array) ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM(yyvsp[0].array, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind(yyvsp[-1].unsigned_number ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }
              yyval.specification = new VhdlProcedureSpec(yyvsp[-4].id_def,generic_clause,generic_map_aspect,yyvsp[0].array,VhdlTreeNode::_present_scope, yyvsp[-1].unsigned_number==VHDL_parameter) ;
          }
    break;

  case 178:

    { VhdlScope::Push(yyvsp[0].id_def) ; }
    break;

  case 179:

    {
              if (yyvsp[-2].unsigned_number && !yyvsp[-1].array) {
                  if (yyvsp[-5].id_def) yyvsp[-5].id_def->Error("parameter should be followed by its list") ;
              }
              Array *generic_clause = 0 ;
              Array *generic_map_aspect = 0 ;
              if (yyvsp[-3].array) { // VHDL-2008 LRM Section 4.2.1
                  if ((yyvsp[-3].array)->Size() > 0) generic_clause = (Array *)(yyvsp[-3].array)->At(0) ;
                  if ((yyvsp[-3].array)->Size() > 1) generic_map_aspect = (Array *)(yyvsp[-3].array)->At(1) ;
                  //($3)->Warning("VHDL 1076-2008 construct not yet supported") ;
                  // VIPER #6258 : Produce error if generic list in subprogram appears in
                  // any dialect other than 2008
                  if (!VhdlNode::IsVhdl2008()) {
                      if (yyvsp[-5].id_def) (yyvsp[-5].id_def)->Error("this construct is only supported in VHDL 1076-2008") ;
                  }
                  delete (yyvsp[-3].array) ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM(yyvsp[-1].array, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind(yyvsp[-2].unsigned_number ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }

              yyval.specification = new VhdlFunctionSpec(yyvsp[-7].unsigned_number,yyvsp[-5].id_def,generic_clause,generic_map_aspect,yyvsp[-1].array,yyvsp[0].name,VhdlTreeNode::_present_scope, yyvsp[-2].unsigned_number==VHDL_parameter) ;
          }
    break;

  case 180:

    { yyval.name = 0 ; }
    break;

  case 181:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 182:

    {
            // Scope is already stored in the subprogram spec. So just pop.
            (void) VhdlScope::Pop() ;
            // Create the tree after we pop scope
            yyval.declaration = new VhdlSubprogramDecl(yyvsp[-1].specification) ;
            // VIPER #6664 : Produce error if function spec does not contain return type
            if (yyvsp[-1].specification && yyvsp[-1].specification->IsFunction() && !yyvsp[-1].specification->GetReturnType()) {
                yyvsp[-1].specification->Error("function return type is not specified") ;
            }
        }
    break;

  case 183:

    {
             loop_sequence_num = 0 ; // Reset loop sequence number (VIPER #7494)
         }
    break;

  case 184:

    {
            // Scope is already stored in the spec
            VhdlTreeNode::_present_scope = yyvsp[-8].specification->LocalScope() ; // VIPER #1976
            (void) VhdlScope::Pop() ;
            // Create the tree after we pop scope
            yyval.declaration = new VhdlSubprogramBody(yyvsp[-8].specification,yyvsp[-6].array,yyvsp[-4].array) ;
            // Check that opt_subprogram_kind closing (if it exists) matches subprogram_spec type
            if (yyvsp[-2].unsigned_number) { // VIPER #2017
                if (yyvsp[-8].specification->IsProcedure() && (yyvsp[-2].unsigned_number != VHDL_procedure)) {
                    yyval.declaration->Error("subprogram type at end of subprogram body does not match specification type '%s'", "procedure");
                } else if (yyvsp[-8].specification->IsFunction() && (yyvsp[-2].unsigned_number != VHDL_function)) {
                    yyval.declaration->Error("subprogram type at end of subprogram body does not match specification type '%s'", "function");
                }
            }
            // VIPER #6664 : Produce error if function spec does not contain return type
            if (yyvsp[-8].specification && yyvsp[-8].specification->IsFunction() && !yyvsp[-8].specification->GetReturnType()) {
                yyvsp[-8].specification->Error("function return type is not specified") ;
            }
            (yyval.declaration)->ClosingLabel(yyvsp[-1].designator) ;
        }
    break;

  case 185:

    {
            (void) VhdlScope::Pop() ; yyval.declaration = new VhdlSubprogInstantiationDecl(yyvsp[-6].specification, (yyvsp[-2].signature) ? new VhdlSignaturedName(yyvsp[-3].name, yyvsp[-2].signature): yyvsp[-3].name, yyvsp[-1].array) ;
            // VIPER #6258 : Produce error if subprogram instantiation declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                (yyval.declaration)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
    break;

  case 186:

    { yyval.array = 0 ; }
    break;

  case 187:

    {
            // Set the interface list objects to be subprogram 'parameters' :
            Array *ids ;
            VhdlInterfaceDecl *decl ;
            VhdlIdDef *id ;
            unsigned i, j ;
            FOREACH_ARRAY_ITEM(yyvsp[-1].array, i, decl) {
                ids = decl->GetIds() ; // collect all identifiers in this list
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    id->SetObjectKind(0) ; // indication that it is a 'parameter'..Viper 6653
                }
            }
            // Return the interface list itself..
            yyval.array = yyvsp[-1].array ;
        }
    break;

  case 188:

    { yyval.unsigned_number = 0 ;}
    break;

  case 189:

    { yyval.unsigned_number = VHDL_parameter ;}
    break;

  case 190:

    { yyval.array = 0 ; }
    break;

  case 191:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].declaration) (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 192:

    { yyval.array = 0 ; }
    break;

  case 193:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
            if (yyvsp[-1].declaration && yyvsp[-1].declaration->IsPackageBody()) yyval.declaration = 0 ; // Package body will not be added in decl list
        }
    break;

  case 208:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 209:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 210:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 211:

    { yyval.array = 0 ; }
    break;

  case 212:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].statement) ; }
    break;

  case 213:

    { yyval.array = 0 ; }
    break;

  case 214:

    { yyval.declaration = new VhdlSubtypeDecl(yyvsp[-3].id_def,yyvsp[-1].subtype_indication) ; }
    break;

  case 215:

    { last_label = yyvsp[0].id_def ; }
    break;

  case 216:

    { // Actually, we use last_label here to have access to the record type id
            // when we create its scope. That's an OK use of last_label, even though its not a label.
            if (!last_label) last_label = (yyvsp[-1].type_def)->GetId() ;
            yyval.declaration = new VhdlFullTypeDecl(last_label,yyvsp[-1].type_def) ;
            last_label = 0 ;
          }
    break;

  case 217:

    { yyval.declaration = new VhdlIncompleteTypeDecl(yyvsp[-1].id_def) ;}
    break;

  case 223:

    { yyval.type_def = new VhdlScalarTypeDef(yyvsp[0].discrete_range) ; }
    break;

  case 229:

    { yyval.type_def = new VhdlArrayTypeDef(yyvsp[-2].array,yyvsp[0].subtype_indication) ; }
    break;

  case 230:

    { VhdlScope::Push(last_label) ; }
    break;

  case 231:

    {
           yyval.type_def = new VhdlRecordTypeDef(yyvsp[-3].array,VhdlScope::Pop()) ;
           (yyval.type_def)->ClosingLabel(yyvsp[0].id) ;
        }
    break;

  case 232:

    { yyval.element_decl = new VhdlElementDecl(yyvsp[-3].array,yyvsp[-1].subtype_indication) ; }
    break;

  case 233:

    { yyval.type_def = new VhdlFileTypeDef(yyvsp[0].name) ; }
    break;

  case 234:

    { yyval.type_def = new VhdlEnumerationTypeDef(yyvsp[-1].array) ; }
    break;

  case 235:

    {
              Array *decl_list = (yyvsp[-3].array) ? yyvsp[-3].array : new Array(1) ;
              if (yyvsp[-4].physical_unit_decl && decl_list) decl_list->InsertFirst(yyvsp[-4].physical_unit_decl) ;
              yyval.type_def = new VhdlPhysicalTypeDef(yyvsp[-6].discrete_range,decl_list) ;
              // No call to ClosingLabel() made, since VhdlPhysicalTypeDef
              // does not have a local scope. Call this separately:
              // VIPER #6662: Only call SetClosingLabel() is the label is specified:
              if (yyvsp[0].id) (yyval.type_def)->SetClosingLabel(yyvsp[0].id) ; // VIPER #6666 ($7 may get deleted here)
          }
    break;

  case 236:

    { yyval.physical_unit_decl = new VhdlPhysicalUnitDecl(yyvsp[-1].id_def,0) ; }
    break;

  case 237:

    { yyval.physical_unit_decl = new VhdlPhysicalUnitDecl(yyvsp[-3].id_def,yyvsp[-1].name) ; }
    break;

  case 238:

    {
               VERIFIC_ASSERT(last_label) ;

               VhdlTreeNode::_present_scope->Undeclare(last_label) ;
               VhdlIdDef *protected_id = new VhdlProtectedTypeId(Strings::save(last_label->Name())) ;

               (void) VhdlTreeNode::_present_scope->Declare(protected_id) ;
               delete last_label ;
               last_label = 0 ;

               VhdlScope::Push(protected_id) ;
           }
    break;

  case 239:

    {
            yyval.type_def = new VhdlProtectedTypeDef(yyvsp[-3].array, VhdlScope::Pop()) ;
            (yyval.type_def)->ClosingLabel(yyvsp[0].id) ;
        }
    break;

  case 240:

    { yyval.array = 0 ; }
    break;

  case 241:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 242:

    { yyval.array = 0 ; }
    break;

  case 243:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].declaration) (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 244:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 245:

    { yyval.declaration = 0 ; }
    break;

  case 246:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
            if (yyvsp[-1].declaration && yyvsp[-1].declaration->IsPackageBody()) yyval.declaration = 0 ; // Package body will not be added in decl list
        }
    break;

  case 247:

    { yyval.declaration = 0 ; }
    break;

  case 251:

    {
            (void) VhdlScope::Pop() ; yyval.declaration = new VhdlSubprogInstantiationDecl(yyvsp[-6].specification, (yyvsp[-2].signature) ? new VhdlSignaturedName(yyvsp[-3].name, yyvsp[-2].signature): yyvsp[-3].name, yyvsp[-1].array) ;
            // VIPER #6258 : Produce error if subprogram instantiation declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                (yyval.declaration)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
    break;

  case 265:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 266:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 267:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 268:

    {
               VERIFIC_ASSERT(last_label) ;
               if (!last_label->IsProtectedBody()) last_label->Error("protected body %s has no protected type defined", last_label->Name()) ;

               VhdlIdDef *header_id = VhdlTreeNode::_present_scope->FindSingleObjectLocal(last_label->Name()) ;
               VhdlScope *header_scope = header_id->LocalScope() ;
               VhdlScope::Push(last_label) ;
               // VIPER #7775 : Produce error if header already contains back pointer to body
               if (header_id && header_id->GetProtectedTypeBodyId()) {
                   last_label->Error("protected type %s body is already defined", last_label->Name()) ;
               } else {
                   if (header_id) header_id->SetProtectedTypeBodyId(last_label) ;
               }

               VhdlTreeNode::_present_scope->AddDeclRegion(header_scope) ;

               last_label = 0 ;
           }
    break;

  case 269:

    {
            yyval.type_def = new VhdlProtectedTypeDefBody(yyvsp[-4].array, VhdlScope::Pop()) ;
            (yyval.type_def)->ClosingLabel(yyvsp[0].id) ;
        }
    break;

  case 270:

    { yyval.declaration = new VhdlConstantDecl(yyvsp[-4].array,yyvsp[-2].subtype_indication,yyvsp[-1].expression) ; }
    break;

  case 271:

    { yyval.declaration = new VhdlSignalDecl(yyvsp[-5].array,yyvsp[-3].subtype_indication,yyvsp[-2].unsigned_number,yyvsp[-1].expression) ; }
    break;

  case 272:

    { yyval.declaration = new VhdlVariableDecl(yyvsp[-6].unsigned_number,yyvsp[-4].array,yyvsp[-2].subtype_indication,yyvsp[-1].expression) ; }
    break;

  case 273:

    { yyval.declaration = new VhdlFileDecl(yyvsp[-4].array,yyvsp[-2].subtype_indication,yyvsp[-1].file_open_info) ; }
    break;

  case 274:

    { yyval.declaration = new VhdlAliasDecl(yyvsp[-5].id_def,yyvsp[-4].subtype_indication,
                        (yyvsp[-1].signature) ? new VhdlSignaturedName(yyvsp[-2].name,yyvsp[-1].signature) : yyvsp[-2].name) ;
            // We cannot declare an alias before we processed what it is.
            // The constructor will have processed this, so now we can declare it.
            vhdl_declare(yyvsp[-5].id_def) ;
          }
    break;

  case 275:

    { yyval.subtype_indication = 0 ; }
    break;

  case 276:

    { yyval.subtype_indication = yyvsp[0].subtype_indication ; }
    break;

  case 277:

    { VhdlScope::Push(yyvsp[-1].id_def) ; }
    break;

  case 278:

    {
           yyval.declaration = new VhdlComponentDecl(yyvsp[-8].id_def,yyvsp[-5].array,yyvsp[-4].array,VhdlScope::Pop()) ;
           (yyval.declaration)->ClosingLabel(yyvsp[-1].id) ;
        }
    break;

  case 279:

    { yyval.declaration = new VhdlAttributeDecl(yyvsp[-3].id_def,yyvsp[-1].name) ; }
    break;

  case 280:

    { yyval.declaration = new VhdlAttributeSpec(yyvsp[-5].id,yyvsp[-3].specification,yyvsp[-1].expression) ; }
    break;

  case 281:

    { yyval.declaration = new VhdlConfigurationSpec(yyvsp[-2].specification, yyvsp[-1].binding_indication) ; }
    break;

  case 282:

    { yyval.declaration = new VhdlDisconnectionSpec(yyvsp[-3].specification,yyvsp[-1].expression) ; }
    break;

  case 283:

    { yyval.declaration = new VhdlGroupTemplateDecl(yyvsp[-5].id_def, yyvsp[-2].array) ; }
    break;

  case 284:

    { yyval.entity_class_entry = new VhdlEntityClassEntry(yyvsp[-1].unsigned_number,yyvsp[0].unsigned_number) ; }
    break;

  case 285:

    { yyval.unsigned_number = VHDL_entity ; }
    break;

  case 286:

    { yyval.unsigned_number = VHDL_architecture ; }
    break;

  case 287:

    { yyval.unsigned_number = VHDL_configuration ; }
    break;

  case 288:

    { yyval.unsigned_number = VHDL_procedure ; }
    break;

  case 289:

    { yyval.unsigned_number = VHDL_function ; }
    break;

  case 290:

    { yyval.unsigned_number = VHDL_package ; }
    break;

  case 291:

    { yyval.unsigned_number = VHDL_type ; }
    break;

  case 292:

    { yyval.unsigned_number = VHDL_subtype ; }
    break;

  case 293:

    { yyval.unsigned_number = VHDL_constant ; }
    break;

  case 294:

    { yyval.unsigned_number = VHDL_signal ; }
    break;

  case 295:

    { yyval.unsigned_number = VHDL_variable ; }
    break;

  case 296:

    { yyval.unsigned_number = VHDL_component ; }
    break;

  case 297:

    { yyval.unsigned_number = VHDL_label ; }
    break;

  case 298:

    { yyval.unsigned_number = VHDL_literal ; }
    break;

  case 299:

    { yyval.unsigned_number = VHDL_units ; }
    break;

  case 300:

    { yyval.unsigned_number = VHDL_group ; }
    break;

  case 301:

    { yyval.unsigned_number = VHDL_file ; }
    break;

  case 302:

    { yyval.declaration = new VhdlGroupDecl(yyvsp[-3].id_def, yyvsp[-1].name) ; }
    break;

  case 304:

    {
            yyval.interface_decl = yyvsp[-1].interface_decl ;
            add_pre_comments_to_node(yyval.interface_decl, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.interface_decl, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 309:

    { yyval.interface_decl = new VhdlInterfaceDecl(yyvsp[-6].unsigned_number,yyvsp[-5].array,yyvsp[-3].unsigned_number,yyvsp[-2].subtype_indication,yyvsp[-1].unsigned_number,yyvsp[0].expression) ; }
    break;

  case 310:

    { yyval.unsigned_number = 0 ; }
    break;

  case 311:

    { yyval.unsigned_number = VHDL_constant ; }
    break;

  case 312:

    { yyval.unsigned_number = VHDL_signal ; }
    break;

  case 313:

    { yyval.unsigned_number = VHDL_variable ; }
    break;

  case 314:

    { yyval.unsigned_number = VHDL_file ; }
    break;

  case 315:

    { yyval.unsigned_number = 0 ; }
    break;

  case 316:

    { yyval.unsigned_number = VHDL_in ; }
    break;

  case 317:

    { yyval.unsigned_number = VHDL_out ; }
    break;

  case 318:

    { yyval.unsigned_number = VHDL_inout ; }
    break;

  case 319:

    { yyval.unsigned_number = VHDL_buffer ; }
    break;

  case 320:

    { yyval.unsigned_number = VHDL_linkage ; }
    break;

  case 321:

    { yyval.unsigned_number = 0 ; }
    break;

  case 322:

    { yyval.unsigned_number = VHDL_in ; }
    break;

  case 323:

    { yyval.unsigned_number = VHDL_out ; }
    break;

  case 324:

    {
            yyval.interface_decl = new VhdlInterfaceTypeDecl(yyvsp[0].id_def) ;
            // VIPER #6258 : Produce error if interface type declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                (yyval.interface_decl)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
    break;

  case 325:

    {
            (void) VhdlScope::Pop() ; yyval.interface_decl = new VhdlInterfaceSubprogDecl(yyvsp[-1].specification, yyvsp[0].expression) ;
            // VIPER #6258 : Produce error if interface subprogram declaration appears in
            // any dialect other than 2008
            if (!VhdlNode::IsVhdl2008()) {
                (yyval.interface_decl)->Error("this construct is only supported in VHDL 1076-2008") ;
            }
        }
    break;

  case 326:

    { VhdlScope::Push(yyvsp[0].id_def) ; (yyvsp[0].id_def)->SetAsProcedure() ; }
    break;

  case 327:

    {
              if (yyvsp[-1].unsigned_number && !yyvsp[0].array) {
                  if (yyvsp[-3].id_def) yyvsp[-3].id_def->Error("parameter should be followed by its list") ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM(yyvsp[0].array, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind(yyvsp[-1].unsigned_number ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }
              yyval.specification = new VhdlProcedureSpec(yyvsp[-3].id_def,0,0,yyvsp[0].array,VhdlTreeNode::_present_scope, yyvsp[-1].unsigned_number==VHDL_parameter) ;
          }
    break;

  case 328:

    { VhdlScope::Push(yyvsp[0].id_def) ; }
    break;

  case 329:

    {
              if (yyvsp[-3].unsigned_number && !yyvsp[-2].array) {
                  if (yyvsp[-5].id_def) yyvsp[-5].id_def->Error("parameter should be followed by its list") ;
              }
              // Set the interface list objects to be subprogram 'parameters' :
              Array *ids ;
              VhdlInterfaceDecl *decl ;
              VhdlIdDef *id ;
              unsigned i, j ;
              FOREACH_ARRAY_ITEM(yyvsp[-2].array, i, decl) {
                  if (!decl) continue ;
                  ids = decl->GetIds() ; // collect all identifiers in this list
                  FOREACH_ARRAY_ITEM(ids, j, id) {
                      if (!id) continue ;
                      id->SetObjectKind(yyvsp[-3].unsigned_number ? VHDL_parameter: 0) ; // indication that it is a 'parameter'..Viper 6653
                  }
              }

              yyval.specification = new VhdlFunctionSpec(yyvsp[-7].unsigned_number,yyvsp[-5].id_def,0,0,yyvsp[-2].array,yyvsp[0].name,VhdlTreeNode::_present_scope, yyvsp[-3].unsigned_number==VHDL_parameter) ;
          }
    break;

  case 330:

    { yyval.expression = 0 ; }
    break;

  case 331:

    { yyval.expression = yyvsp[0].expression ; }
    break;

  case 332:

    { yyval.expression = yyvsp[0].name ; }
    break;

  case 333:

    { yyval.expression = new VhdlBox() ; }
    break;

  case 334:

    {
            VhdlScope::Push() ;
            // Create cross-link between the scope and the id that owns it.
            VhdlTreeNode::_present_scope->SetOwner(yyvsp[-2].id_def) ;

            // This package declaration is in a declarative region of
            // package/entity/architecture etc. So declare the identifier
            // in upper scope
            VhdlScope *old_scope = VhdlTreeNode::_present_scope ;
            VhdlTreeNode::_present_scope = old_scope->Upper() ;
            vhdl_declare(yyvsp[-2].id_def) ;
            VhdlTreeNode::_present_scope = old_scope ;
          }
    break;

  case 335:

    {
              yyval.interface_decl = new VhdlInterfacePackageDecl(yyvsp[-5].id_def, yyvsp[-1].name, yyvsp[0].array, VhdlScope::Pop()) ;
              // VIPER #6258 : Produce error if interface package declaration appears in
              // any dialect other than 2008
              if (!VhdlNode::IsVhdl2008()) {
                  (yyval.interface_decl)->Error("this construct is only supported in VHDL 1076-2008") ;
              }
          }
    break;

  case 337:

    { yyval.array = new Array(1) ; (yyval.array)->InsertLast(new VhdlBox()) ; }
    break;

  case 338:

    { yyval.array = new Array(1) ; (yyval.array)->InsertLast(new VhdlDefault()) ; }
    break;

  case 339:

    { yyval.specification = new VhdlEntitySpec(yyvsp[-2].array,yyvsp[0].unsigned_number) ; }
    break;

  case 340:

    { yyval.specification = new VhdlComponentSpec(yyvsp[-2].array,yyvsp[0].name) ; }
    break;

  case 341:

    { yyval.specification = new VhdlGuardedSignalSpec(yyvsp[-2].array,yyvsp[0].name) ; }
    break;

  case 342:

    {
            yyval.statement = yyvsp[-1].statement ;
            add_pre_comments_to_node(yyval.statement, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.statement, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 343:

    { yyval.statement = yyvsp[0].statement ; }
    break;

  case 344:

    { yyval.statement = yyvsp[0].statement ; if (yyval.statement) (yyval.statement)->SetLabel(0,VHDL_postponed) ; update_linefile(yyval.statement, 0/*not ending linefile*/) ;}
    break;

  case 345:

    { yyval.statement=yyvsp[0].statement ; if (yyval.statement) (yyval.statement)->SetLabel(yyvsp[-2].id_def) ; last_label = 0 ; update_linefile(yyval.statement, 0/*not ending linefile*/) ;}
    break;

  case 346:

    { yyval.statement=yyvsp[0].statement ; if (yyval.statement) (yyval.statement)->SetLabel(yyvsp[-3].id_def,VHDL_postponed) ; last_label = 0 ; update_linefile(yyval.statement, 0/*not ending linefile*/) ;}
    break;

  case 355:

    { yyval.statement = new VhdlProcedureCallStatement(yyvsp[-1].name) ; }
    break;

  case 356:

    { yyval.statement = new VhdlProcedureCallStatement(yyvsp[-1].name) ; (yyval.statement)->SetLabel(0,VHDL_postponed) ; }
    break;

  case 357:

    {
            yyval.statement = new VhdlProcedureCallStatement(yyvsp[-1].name) ;
            (yyval.statement)->SetLabel(yyvsp[-4].id_def, VHDL_postponed) ;
            last_label = 0 ;
          }
    break;

  case 358:

    {
            // There is one corner-case where a labeled procedure call with no arguments could
            // match this rule.  In this case, we want to make sure to instantiated the correct
            // class.  Due to this, the following check is needed. (VIPER #2791)
            VhdlIdDef *unit = (yyvsp[-3].name) ? (yyvsp[-3].name)->EntityAspect() : 0 ;
            if (unit && unit->IsSubprogram()) {
                // This is actually a label procedure call with no arguments! (VIPER #2791)
                //VERIFIC_ASSERT(!$4 && !$5) ;
                // VIPER #4692: Subprogram call with port map or generic map is
                // an error case. Produce error now.
                if (!yyvsp[-2].array && !yyvsp[-1].array) {
                    yyval.statement = new VhdlProcedureCallStatement(yyvsp[-3].name) ;
                } else {
                    yyval.statement = 0 ;
                    if (yyvsp[-3].name) {
                        yyvsp[-3].name->Error("%s is not a %s", unit->Name(), "component") ;
                        VhdlTreeNode::_present_scope->Undeclare(yyvsp[-5].id_def) ;
                        delete yyvsp[-3].name ;
                        delete yyvsp[-5].id_def ;
                        cleanup_treenode_array(yyvsp[-2].array) ;
                        cleanup_treenode_array(yyvsp[-1].array) ;
                    }
                }
            } else {
                yyval.statement = new VhdlComponentInstantiationStatement(yyvsp[-3].name,yyvsp[-2].array,yyvsp[-1].array) ;
            }
            if (yyval.statement) (yyval.statement)->SetLabel(yyvsp[-5].id_def) ;
            last_label = 0 ;
          }
    break;

  case 360:

    { yyval.name = new VhdlInstantiatedUnit(VHDL_component, yyvsp[0].name) ; }
    break;

  case 361:

    { yyval.name = new VhdlInstantiatedUnit(VHDL_entity, yyvsp[0].name) ; }
    break;

  case 362:

    { yyval.name = new VhdlInstantiatedUnit(VHDL_configuration, yyvsp[0].name) ; }
    break;

  case 363:

    {
             yyval.statement = yyvsp[0].statement ;
             if (yyval.statement) (yyval.statement)->SetLabel(yyvsp[-2].id_def) ;
             last_label = 0 ;
             update_linefile(yyval.statement, 0/*not ending linefile*/) ; // VIPER #6229 : Update line-column
           }
    break;

  case 364:

    { yyval.statement = yyvsp[0].statement ; }
    break;

  case 365:

    { yyval.statement = yyvsp[0].statement ; }
    break;

  case 366:

    {
               yyval.statement = yyvsp[0].statement ;
               // VIPER #6258 : Produce error if case generate appears in
               // any dialect other than 2008
               if (!VhdlNode::IsVhdl2008()) {
                    if (yyvsp[0].statement) (yyvsp[0].statement)->Error("this construct is only supported in VHDL 1076-2008") ;
               }
           }
    break;

  case 367:

    {
               yyval.statement = new VhdlGenerateStatement(yyvsp[-6].iter_scheme, yyvsp[-4].generate_stmt_body.decls, yyvsp[-4].generate_stmt_body.stmts, yyvsp[-4].generate_stmt_body.scope) ;
               (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
           }
    break;

  case 368:

    {
               yyval.statement = new VhdlGenerateStatement(yyvsp[-8].iter_scheme, yyvsp[-6].generate_stmt_body.decls, yyvsp[-6].generate_stmt_body.stmts, yyvsp[-6].generate_stmt_body.scope) ;
               (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
           }
    break;

  case 369:

    { VhdlScope::Push(last_label) ; last_label = 0 ; }
    break;

  case 370:

    { yyval.iter_scheme = new VhdlForScheme(yyvsp[-2].id, yyvsp[0].discrete_range, 1) ; }
    break;

  case 371:

    { yyval.statement = yyvsp[-4].statement ; if (yyval.statement) (yyval.statement)->ClosingLabel(yyvsp[-1].id) ; update_linefile(yyval.statement) ; /* VIPER #6229*/}
    break;

  case 372:

    {
               yyval.statement = yyvsp[-7].statement ; if (yyvsp[-7].statement) (yyvsp[-7].statement)->ClosingAlternativeLabel(yyvsp[-5].id) ; update_linefile(yyval.statement) ; /* VIPER #6229*/
               // VIPER #6258 : Produce error if if generate with alternative label appears in
               // any dialect other than 2008
               if (!VhdlNode::IsVhdl2008()) {
                    if (yyvsp[-7].statement) (yyvsp[-7].statement)->Error("this construct is only supported in VHDL 1076-2008") ;
               }
               // No call to ClosingLabel() made, since 'if_scheme' below may not use
               // the matching 'last_label' if alternate label is there. In that case
               // calling ClosingLabel() may incorrectly error out. Call this separately:
               // VIPER #6662: Only call SetClosingLabel() is the label is specified:
               if (yyvsp[-1].id) (yyval.statement)->SetClosingLabel(yyvsp[-1].id) ; // VIPER #6666 ($7 may get deleted here)
            }
    break;

  case 373:

    { yyval.iter_scheme = new VhdlIfScheme(yyvsp[0].expression) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
    break;

  case 374:

    {
               yyval.iter_scheme = new VhdlIfScheme(yyvsp[0].expression, yyvsp[-2].id) ; VhdlScope::Push((yyval.iter_scheme)->GetAlternativeLabelId()) ;  // Push with alternatve_label ?
               // VIPER #6258 : Produce error if alternative label appears in
               // any dialect other than 2008
               if (!VhdlNode::IsVhdl2008()) {
                    (yyval.iter_scheme)->Error("this construct is only supported in VHDL 1076-2008") ;
               }
            }
    break;

  case 375:

    { yyval.statement = new VhdlGenerateStatement(yyvsp[-2].iter_scheme, yyvsp[0].generate_stmt_body.decls, yyvsp[0].generate_stmt_body.stmts, yyvsp[0].generate_stmt_body.scope) ; }
    break;

  case 376:

    {
                yyval.statement = (yyvsp[-1].statement) ? (yyvsp[-1].statement)->AddElsifElse(yyvsp[0].statement): 0 ;
                // VIPER #6258 : Produce error if else-elsif generate appears in
                // any dialect other than 2008
                if (!VhdlNode::IsVhdl2008()) {
                    if (yyval.statement) (yyval.statement)->Error("this construct is only supported in VHDL 1076-2008") ;
                }
            }
    break;

  case 377:

    {
                if (yyvsp[-4].statement) yyvsp[-4].statement->ClosingAlternativeLabel(yyvsp[-2].id) ; yyval.statement = (yyvsp[-4].statement) ? (yyvsp[-4].statement)->AddElsifElse(yyvsp[0].statement): 0 ;
                // VIPER #6258 : Produce error if else-elsif generate appears in
                // any dialect other than 2008
                if (!VhdlNode::IsVhdl2008()) {
                    if (yyval.statement) (yyval.statement)->Error("this construct is only supported in VHDL 1076-2008") ;
                }
            }
    break;

  case 380:

    { yyval.statement = new VhdlGenerateStatement(yyvsp[-2].iter_scheme, yyvsp[0].generate_stmt_body.decls, yyvsp[0].generate_stmt_body.stmts, yyvsp[0].generate_stmt_body.scope) ; }
    break;

  case 381:

    { yyval.iter_scheme = new VhdlElsifElseScheme(yyvsp[0].expression, 0) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
    break;

  case 382:

    { yyval.iter_scheme = new VhdlElsifElseScheme(yyvsp[0].expression, yyvsp[-2].id) ; VhdlScope::Push((yyval.iter_scheme)->GetAlternativeLabelId()) ; }
    break;

  case 383:

    { yyval.statement = new VhdlGenerateStatement(yyvsp[-2].iter_scheme, yyvsp[0].generate_stmt_body.decls, yyvsp[0].generate_stmt_body.stmts, yyvsp[0].generate_stmt_body.scope) ; }
    break;

  case 384:

    { yyval.iter_scheme = new VhdlElsifElseScheme(0, 0) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
    break;

  case 385:

    { yyval.iter_scheme = new VhdlElsifElseScheme(0, yyvsp[-1].id) ; VhdlScope::Push((yyval.iter_scheme)->GetAlternativeLabelId()) ; }
    break;

  case 386:

    { yyval.statement = new VhdlCaseGenerateStatement(yyvsp[-6].expression, yyvsp[-4].array) ; if (yyval.statement) (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;}
    break;

  case 387:

    {
               yyval.statement = new VhdlCaseGenerateStatement(yyvsp[-9].expression, yyvsp[-7].array) ; if (yyval.statement) (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
               VhdlStatement *last_alt = (yyvsp[-7].array) ? (VhdlStatement*)(yyvsp[-7].array)->GetLast(): 0 ;
               if (last_alt) last_alt->ClosingAlternativeLabel(yyvsp[-5].id) ;
           }
    break;

  case 388:

    { yyval.array = new Array(1) ; (yyval.array)->InsertLast(yyvsp[0].statement) ; }
    break;

  case 389:

    { yyval.array = (yyvsp[-1].array) ? yyvsp[-1].array : new Array(1) ; (yyval.array)->InsertLast(yyvsp[0].statement) ; }
    break;

  case 390:

    {
               VhdlStatement *last_alt = (yyvsp[-4].array) ? (VhdlStatement*)(yyvsp[-4].array)->GetLast(): 0 ;
               if (last_alt) last_alt->ClosingAlternativeLabel(yyvsp[-2].id) ;
               // VIPER #6935 : Create new Array if $1 is not present
               yyval.array = (yyvsp[-4].array) ? yyvsp[-4].array : new Array(1) ; (yyval.array)->InsertLast(yyvsp[0].statement) ;
           }
    break;

  case 391:

    { yyval.array = 0 ; }
    break;

  case 392:

    { yyval.statement = new VhdlGenerateStatement(yyvsp[-1].iter_scheme, (yyvsp[0].generate_stmt_body).decls, (yyvsp[0].generate_stmt_body).stmts, (yyvsp[0].generate_stmt_body).scope) ; }
    break;

  case 393:

    { yyval.iter_scheme = new VhdlCaseItemScheme(yyvsp[-1].array, 0) ; VhdlScope::Push(last_label) ; last_label = 0 ; }
    break;

  case 394:

    { yyval.iter_scheme = new VhdlCaseItemScheme(yyvsp[-1].array, yyvsp[-3].id) ; VhdlScope::Push((yyval.iter_scheme)->GetAlternativeLabelId()) ; }
    break;

  case 395:

    {
                 if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                     // VIPER #7494 : Reset process sequence number remembering current one
                     yyval.unsigned_number = process_sequence_num ;
                     process_sequence_num = 0 ;
                 }
             }
    break;

  case 396:

    {
                 yyval.generate_stmt_body = yyvsp[0].generate_stmt_body ;
                 if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                     // Pop process sequence number
                     process_sequence_num = yyvsp[-1].unsigned_number ;
                 }
             }
    break;

  case 397:

    { yyval.generate_stmt_body.decls = 0; yyval.generate_stmt_body.stmts = yyvsp[0].array; yyval.generate_stmt_body.scope = VhdlScope::Pop() ; }
    break;

  case 398:

    { yyval.generate_stmt_body.decls = ((yyvsp[-2].array)?(yyvsp[-2].array):new Array(1)); yyval.generate_stmt_body.stmts = yyvsp[0].array ; yyval.generate_stmt_body.scope = VhdlScope::Pop() ; }
    break;

  case 399:

    { yyval.generate_stmt_body.decls = 0 ; yyval.generate_stmt_body.stmts = 0 ; yyval.generate_stmt_body.scope = 0 ;}
    break;

  case 400:

    { yyval.array = 0 ; }
    break;

  case 401:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 402:

    { yyval.array = 0 ; }
    break;

  case 403:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].statement) (yyval.array)->InsertLast(yyvsp[0].statement) ; if (yyvsp[0].statement) CreateAndAddImplicitLabel(yyvsp[0].statement) ; /* VIPER #7494*/}
    break;

  case 404:

    { yyval.statement = new VhdlConditionalSignalAssignment(yyvsp[-4].expression,yyvsp[-2].options,yyvsp[-1].array) ; }
    break;

  case 405:

    {
            // Added this rule to have this type of assignment
            // which was otherwise included in conditional_waveforms
            // definition
            Array *wv_list = new Array() ;
            wv_list->InsertLast(new VhdlConditionalWaveform(yyvsp[-1].array,0)) ;
            yyval.statement = new VhdlConditionalSignalAssignment(yyvsp[-4].expression,yyvsp[-2].options,wv_list) ;
          }
    break;

  case 406:

    { yyval.statement = new VhdlSelectedSignalAssignment(yyvsp[-7].expression, yyvsp[-4].expression, yyvsp[-2].options, yyvsp[-1].array, 0, yyvsp[-5].unsigned_number ? 1: 0) ; }
    break;

  case 407:

    { yyval.expression = yyvsp[0].name ;}
    break;

  case 408:

    { yyval.expression = yyvsp[0].expression ;}
    break;

  case 409:

    { yyval.options = (yyvsp[-1].unsigned_number || yyvsp[0].delay_mechanism) ? new VhdlOptions(yyvsp[-1].unsigned_number,yyvsp[0].delay_mechanism) : 0 ; }
    break;

  case 410:

    { yyval.delay_mechanism = new VhdlTransport() ; }
    break;

  case 411:

    { yyval.delay_mechanism = new VhdlInertialDelay(0, VHDL_inertial) ; }
    break;

  case 412:

    { yyval.delay_mechanism = new VhdlInertialDelay(yyvsp[-1].expression, VHDL_inertial) ; }
    break;

  case 413:

    {
             yyval.array = (yyvsp[-1].array) ? yyvsp[-1].array : new Array() ;
             (yyval.array)->InsertFirst(new VhdlConditionalWaveform(yyvsp[-4].array, yyvsp[-2].expression)) ;
             if (yyvsp[0].tree_node) (yyval.array)->InsertLast(yyvsp[0].tree_node) ;
           }
    break;

  case 414:

    { yyval.array = 0 ; }
    break;

  case 415:

    { yyval.array = (yyvsp[-4].array) ? yyvsp[-4].array : new Array() ; (yyval.array)->InsertLast(new VhdlConditionalWaveform(yyvsp[-2].array,yyvsp[0].expression)) ; }
    break;

  case 416:

    { yyval.tree_node = 0 ; }
    break;

  case 417:

    { yyval.tree_node = new VhdlConditionalWaveform(yyvsp[0].array,0) ; }
    break;

  case 418:

    { yyval.array = new Array() ;
            (yyval.array)->InsertLast(new VhdlSelectedWaveform(yyvsp[-2].array,yyvsp[0].array)) ;
          }
    break;

  case 419:

    { yyval.array = (yyvsp[-4].array) ? (yyvsp[-4].array) : new Array() ;
            (yyval.array)->InsertLast(new VhdlSelectedWaveform(yyvsp[-2].array,yyvsp[0].array)) ;
          }
    break;

  case 421:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(new VhdlUnaffected()) ; }
    break;

  case 422:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].expression) ; }
    break;

  case 423:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].expression) ; }
    break;

  case 424:

    { yyval.array = 0 ; }
    break;

  case 426:

    { yyval.expression = new VhdlWaveformElement(yyvsp[-2].expression,yyvsp[0].expression) ; }
    break;

  case 427:

    {
            // Need a 'block-label' here. yacc does not accept that, so create one now :
            // Make sure to preserve the original name label.
            VhdlIdDef *block_label = new VhdlBlockId( Strings::save((yyvsp[-4].id_def)->OrigName()) ) ;
            // VIPER #3407: Set the linefile of this block id from the original label:
            block_label->SetLinefile((yyvsp[-4].id_def)->Linefile()) ;
            // Remove the old one from the scope :
            VhdlTreeNode::_present_scope->Undeclare(yyvsp[-4].id_def) ;
            // Declare the new one :
            vhdl_declare(block_label) ;
            // delete the old one.
            delete yyvsp[-4].id_def ;
            // Set the new one as $1 :
            yyvsp[-4].id_def = block_label ;

            /* Start a new scope */
            VhdlScope::Push(yyvsp[-4].id_def) ;
            last_label = 0 ;
            if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                // VIPER #7494 : Store current process sequence number
                yyval.unsigned_number = process_sequence_num ;
                process_sequence_num = 0 ;
            }
        }
    break;

  case 428:

    {
            if (yyvsp[-4].expression) {
                /* There is a guard expression. Declare "guard" (LRM 9.1) here at the beginning of the decl part */
                VhdlIdDef *guard = new VhdlSignalId(Strings::save("guard")) ;
                vhdl_declare(guard) ;

                /* Make it of type Boolean */
                guard->DeclareSignal(VhdlNode::StdType("boolean"),0, 1, 1, VHDL_implicit_guard) ;
            }
        }
    break;

  case 429:

    {
           yyval.statement = new VhdlBlockStatement(yyvsp[-12].expression, yyvsp[-9].block_generics, yyvsp[-8].block_ports, yyvsp[-6].array, yyvsp[-4].array, VhdlScope::Pop()) ;
           (yyval.statement)->SetLabel(yyvsp[-15].id_def) ;
           (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
           if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
               // VIPER #7494 : Reset process sequence number
               process_sequence_num = yyvsp[-10].unsigned_number ;
           }
        }
    break;

  case 430:

    { yyval.block_generics = 0 ; }
    break;

  case 431:

    { yyval.block_generics = new VhdlBlockGenerics(yyvsp[0].array, 0) ; }
    break;

  case 432:

    { yyval.block_generics = new VhdlBlockGenerics(yyvsp[-2].array, yyvsp[-1].array) ; }
    break;

  case 433:

    { yyval.block_ports = 0 ; }
    break;

  case 434:

    { yyval.block_ports = new VhdlBlockPorts(yyvsp[0].array, 0) ; }
    break;

  case 435:

    { yyval.block_ports = new VhdlBlockPorts(yyvsp[-2].array, yyvsp[-1].array) ; }
    break;

  case 436:

    { yyval.array = 0 ; }
    break;

  case 437:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 438:

    { yyval.array = 0 ;}
    break;

  case 439:

    { yyval.array = 0 ; }
    break;

  case 440:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].statement) (yyval.array)->InsertLast(yyvsp[0].statement) ; if (yyvsp[0].statement) CreateAndAddImplicitLabel(yyvsp[0].statement) /* VIPER #7494 */ ;}
    break;

  case 441:

    { yyval.array = 0 ;}
    break;

  case 442:

    { yyval.expression = 0 ; }
    break;

  case 443:

    { yyval.expression = yyvsp[-1].expression ; }
    break;

  case 444:

    {  VhdlScope::Push(last_label) ; last_label = 0 ;
           // Flag the scope as coming from a process
           VhdlTreeNode::_present_scope->SetIsProcessScope() ;
           // Flag that no wait statement is allowed (this process has a sensitivity list)
           if (yyvsp[-1].array) VhdlTreeNode::_present_scope->SetWaitStatementNotAllowed() ;
        }
    break;

  case 445:

    {
           yyval.statement = new VhdlProcessStatement(yyvsp[-10].array, yyvsp[-7].array, yyvsp[-5].array, VhdlScope::Pop()) ;
           (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
        }
    break;

  case 446:

    { yyval.array = 0 ; }
    break;

  case 447:

    { yyval.array = yyvsp[-1].array ; }
    break;

  case 448:

    {   // Viper #5918 for Vhdl 2008 support
              VhdlAll *vhd_all = new VhdlAll() ;
              yyval.array = new Array() ;
              (yyval.array)->InsertLast(vhd_all) ;

              if (!vhdl_file::IsVhdlPsl() && !vhdl_file::IsVhdl2008()) vhd_all->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 450:

    { yyval.array = 0 ; }
    break;

  case 451:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; if (yyvsp[0].declaration) (yyval.array)->InsertLast(yyvsp[0].declaration) ; if (yyvsp[0].declaration && yyvsp[0].declaration->IsPrimaryUnit()) yyvsp[0].declaration->SetContainerArray(yyval.array) ;}
    break;

  case 452:

    { yyval.array = 0 ; }
    break;

  case 453:

    {
            yyval.declaration = yyvsp[-1].declaration ;
            add_pre_comments_to_node(yyval.declaration, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.declaration, yyvsp[0].array) ; // trailing comments
            if (yyvsp[-1].declaration && yyvsp[-1].declaration->IsPackageBody()) yyval.declaration = 0 ; // Package body will not be added in decl list
        }
    break;

  case 467:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 468:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 469:

    { yyval.declaration = yyvsp[0].library_unit ; }
    break;

  case 470:

    { yyval.array = 0 ; }
    break;

  case 471:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].statement) ; }
    break;

  case 472:

    { yyval.array = 0 ; }
    break;

  case 473:

    {
            yyval.statement = yyvsp[-1].statement ;
            add_pre_comments_to_node(yyval.statement, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.statement, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 474:

    { yyval.statement = yyvsp[0].statement ; }
    break;

  case 475:

    { yyval.statement = yyvsp[0].statement ; (yyval.statement)->SetLabel(yyvsp[-2].id_def) ; last_label = 0 ; update_linefile(yyval.statement, 0/*not ending linefile*/) /* VIPER #6274 */; }
    break;

  case 489:

    { yyval.statement = new VhdlAssertionStatement(yyvsp[-3].expression,yyvsp[-2].expression,yyvsp[-1].expression) ; }
    break;

  case 490:

    { yyval.statement = new VhdlReportStatement(yyvsp[-2].expression, yyvsp[-1].expression) ; }
    break;

  case 491:

    { yyval.expression = 0 ; }
    break;

  case 492:

    { yyval.expression = yyvsp[0].expression ; }
    break;

  case 493:

    { yyval.expression = 0 ; }
    break;

  case 494:

    { yyval.expression = yyvsp[0].expression ; }
    break;

  case 495:

    { yyval.statement = new VhdlWaitStatement(yyvsp[-3].array,yyvsp[-2].expression,yyvsp[-1].expression) ; }
    break;

  case 496:

    { yyval.array = 0 ; }
    break;

  case 497:

    { yyval.array = yyvsp[0].array ; }
    break;

  case 498:

    { yyval.expression = 0 ; }
    break;

  case 499:

    { yyval.expression = yyvsp[0].expression ; }
    break;

  case 500:

    { yyval.expression = 0 ; }
    break;

  case 501:

    { yyval.expression = yyvsp[0].expression ; }
    break;

  case 503:

    {
            yyval.statement = yyvsp[0].statement ;
            if (!VhdlNode::IsVhdl2008()) yyvsp[0].statement->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 504:

    {
            yyval.statement = yyvsp[0].statement ;
            if (!VhdlNode::IsVhdl2008()) yyvsp[0].statement->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 505:

    { yyval.statement = new VhdlConditionalSignalAssignment(yyvsp[-4].expression,0,yyvsp[-1].array,yyvsp[-2].delay_mechanism) ; }
    break;

  case 506:

    { yyval.statement = new VhdlConditionalForceAssignmentStatement(yyvsp[-5].expression, yyvsp[-2].unsigned_number, yyvsp[-1].array) ; }
    break;

  case 507:

    {
            yyval.statement = new VhdlSelectedSignalAssignment(yyvsp[-7].expression, yyvsp[-4].expression, 0, yyvsp[-1].array, yyvsp[-2].delay_mechanism, 1) ;
          }
    break;

  case 508:

    { yyval.statement = new VhdlSelectedSignalAssignment(yyvsp[-6].expression, yyvsp[-4].expression, 0, yyvsp[-1].array, yyvsp[-2].delay_mechanism) ; }
    break;

  case 509:

    {
              // VIPER #8152: Create parse tree for selected force assignment
              yyval.statement = new VhdlSelectedForceAssignment(yyvsp[-7].expression, 0, yyvsp[-5].expression, yyvsp[-2].unsigned_number, yyvsp[-1].array) ;
          }
    break;

  case 510:

    {
              // VIPER #8152: Create parse tree for selected force assignment
              yyval.statement = new VhdlSelectedForceAssignment(yyvsp[-8].expression, 1, yyvsp[-5].expression, yyvsp[-2].unsigned_number, yyvsp[-1].array) ;
          }
    break;

  case 511:

    { yyval.statement = new VhdlSignalAssignmentStatement(yyvsp[-4].expression, yyvsp[-2].delay_mechanism, yyvsp[-1].array) ; }
    break;

  case 512:

    {
            yyval.statement = new VhdlForceAssignmentStatement(yyvsp[-5].expression, yyvsp[-2].unsigned_number, yyvsp[-1].expression) ;
            if (!VhdlNode::IsVhdl2008()) yyval.statement->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 513:

    {
            yyval.statement = new VhdlReleaseAssignmentStatement(yyvsp[-4].expression, yyvsp[-1].unsigned_number) ;
            if (!VhdlNode::IsVhdl2008()) yyval.statement->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 515:

    {
            yyval.statement = yyvsp[0].statement ;
            if (!VhdlNode::IsVhdl2008()) yyvsp[0].statement->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 516:

    {
            yyval.statement = yyvsp[0].statement ;
            if (!VhdlNode::IsVhdl2008()) yyvsp[0].statement->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 517:

    { yyval.statement = new VhdlVariableAssignmentStatement(yyvsp[-3].expression, yyvsp[-1].expression) ; }
    break;

  case 518:

    { yyval.statement = new VhdlConditionalVariableAssignmentStatement(yyvsp[-3].expression, yyvsp[-1].array) ; }
    break;

  case 519:

    {
             yyval.array = (yyvsp[-1].array) ? yyvsp[-1].array : new Array() ;
             (yyval.array)->InsertFirst(new VhdlConditionalExpression(yyvsp[-4].expression, yyvsp[-2].expression)) ; // Viper 7460 InsertFirst instead of InsertLast
             if (yyvsp[0].tree_node) (yyval.array)->InsertLast(yyvsp[0].tree_node) ;
           }
    break;

  case 520:

    { yyval.array = 0 ; }
    break;

  case 521:

    { yyval.array = (yyvsp[-4].array) ? yyvsp[-4].array : new Array() ; (yyval.array)->InsertLast(new VhdlConditionalExpression(yyvsp[-2].expression,yyvsp[0].expression)) ; }
    break;

  case 522:

    { yyval.tree_node = 0 ; }
    break;

  case 523:

    { yyval.tree_node = new VhdlConditionalExpression(yyvsp[0].expression,0) ; }
    break;

  case 524:

    { yyval.statement = new VhdlSelectedVariableAssignmentStatement(yyvsp[-6].expression, yyvsp[-3].expression, yyvsp[-1].array, 1/* matching select */) ; }
    break;

  case 525:

    { yyval.statement = new VhdlSelectedVariableAssignmentStatement(yyvsp[-5].expression, yyvsp[-3].expression, yyvsp[-1].array) ; }
    break;

  case 526:

    { yyval.array = new Array() ;
               (yyval.array)->InsertLast(new VhdlSelectedExpression(yyvsp[-2].expression,yyvsp[0].array)) ;
             }
    break;

  case 527:

    {
               yyval.array = (yyvsp[-4].array) ? (yyvsp[-4].array) : new Array() ;
               (yyval.array)->InsertLast(new VhdlSelectedExpression(yyvsp[-2].expression,yyvsp[0].array)) ;
             }
    break;

  case 528:

    { yyval.statement = new VhdlProcedureCallStatement(yyvsp[-1].name) ; }
    break;

  case 529:

    {
                yyval.statement = new VhdlIfStatement(yyvsp[-8].expression, yyvsp[-6].array, yyvsp[-5].array, yyvsp[-4].array) ;
                (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
           }
    break;

  case 530:

    { yyval.array = 0 ; }
    break;

  case 531:

    { yyval.array = (yyvsp[-4].array) ? yyvsp[-4].array : new Array() ; (yyval.array)->InsertLast(new VhdlElsif(yyvsp[-2].expression,yyvsp[0].array)) ; }
    break;

  case 532:

    { yyval.array = 0 ; }
    break;

  case 533:

    { yyval.array = (yyvsp[0].array) ? (yyvsp[0].array) : new Array(1) ; }
    break;

  case 534:

    { yyval.array = 0 ; }
    break;

  case 535:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].statement) ; }
    break;

  case 536:

    { yyval.array = 0 ; }
    break;

  case 537:

    {
                yyval.statement = new VhdlCaseStatement(yyvsp[-7].expression,yyvsp[-5].array, yyvsp[-8].unsigned_number ? 1: 0) ;
                // LRM 1076-2008 : Section 10.6 says question mark delimiter should
                // present either in both places or in neither place.
                if ((yyvsp[-8].unsigned_number && !yyvsp[-2].unsigned_number) || (!yyvsp[-8].unsigned_number && yyvsp[-2].unsigned_number)) {
                    yyval.statement->Error("question mark delimiter should be in both places after keyword case") ;
                }
                (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
           }
    break;

  case 538:

    { yyval.unsigned_number = 0 ; }
    break;

  case 539:

    { yyval.unsigned_number = VHDL_matching_op ; }
    break;

  case 540:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].case_statement_alternative) ; }
    break;

  case 541:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].case_statement_alternative) ; }
    break;

  case 542:

    { yyval.array = 0 ; }
    break;

  case 543:

    { yyval.case_statement_alternative = new VhdlCaseStatementAlternative(yyvsp[-2].array,yyvsp[0].array) ; }
    break;

  case 544:

    {        /* Create a new scope */
                        VhdlIdDef *implicit_label_id = 0 ;
                        if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                            // VIPER #7494: If label is not specified, create implicit
                            // label identifier according to VHDL-2008 (19.4.2.2)
                            if (!last_label) {
                                char name[20] ;
                                sprintf(name, "_L%d", loop_sequence_num) ;
                                implicit_label_id = new VhdlLabelId(Strings::save(name)) ;
                                vhdl_declare_label(implicit_label_id) ;
                                last_label = implicit_label_id ;
                            }
                        }
                        // Viper 2597 : Create a transparent scope for loop, so that
                        // labels of nested statements are declared in a non-transparent scope
                        VhdlScope::Push(last_label, 1) ; last_label = 0 ;
                        if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                            // VIPER #7494: Store implicit label to add in statement
                            yyval.id_def = implicit_label_id ;
                            // Increase implicit loop sequence number
                            loop_sequence_num++ ;
                        }
                }
    break;

  case 545:

    {
                        yyval.statement = new VhdlLoopStatement(yyvsp[-6].iter_scheme, yyvsp[-4].array, VhdlScope::Pop()) ;
                        if (RuntimeFlags::GetVar("vhdl_create_implicit_labels")) {
                            // VIPER #7494: Set implicit label id to loop statement
                            VhdlIdDef *implicit_label_id = yyvsp[-7].id_def ;
                            if (implicit_label_id) (yyval.statement)->SetLabel(implicit_label_id) ;
                        }
                        (yyval.statement)->ClosingLabel(yyvsp[-1].id) ;
                }
    break;

  case 546:

    { /* infinite loop */ yyval.iter_scheme = new VhdlWhileScheme(0) ; }
    break;

  case 547:

    { yyval.iter_scheme = new VhdlWhileScheme(yyvsp[0].expression) ; }
    break;

  case 548:

    { yyval.iter_scheme = new VhdlForScheme(yyvsp[-2].id, yyvsp[0].discrete_range, 0) ; }
    break;

  case 549:

    { yyval.iter_scheme = 0 ; /*VIPER #1900*/ }
    break;

  case 550:

    { yyval.statement = new VhdlNextStatement(yyvsp[-2].id,yyvsp[-1].expression) ; }
    break;

  case 551:

    { yyval.statement = new VhdlExitStatement(yyvsp[-2].id,yyvsp[-1].expression) ; }
    break;

  case 552:

    { yyval.statement = new VhdlReturnStatement(yyvsp[-1].expression,VhdlTreeNode::_present_scope->GetSubprogram()) ; }
    break;

  case 553:

    { yyval.statement = new VhdlNullStatement() ; }
    break;

  case 554:

    { yyval.expression=0; }
    break;

  case 555:

    { yyval.expression = yyvsp[0].expression ; }
    break;

  case 556:

    { yyval.binding_indication = new VhdlBindingIndication(0,yyvsp[-1].array,yyvsp[0].array) ; }
    break;

  case 557:

    { yyval.binding_indication = new VhdlBindingIndication(yyvsp[-2].name,yyvsp[-1].array,yyvsp[0].array) ; }
    break;

  case 558:

    { yyval.name = new VhdlEntityAspect(VHDL_entity, yyvsp[0].name) ; }
    break;

  case 559:

    { yyval.name = new VhdlEntityAspect(VHDL_configuration, yyvsp[0].name) ; }
    break;

  case 560:

    { yyval.name = new VhdlOpen() ; }
    break;

  case 561:

    { yyval.array=0; }
    break;

  case 563:

    { yyval.array=0; }
    break;

  case 565:

    { yyval.array = yyvsp[-1].array ; }
    break;

  case 566:

    { yyval.array = yyvsp[-1].array ; }
    break;

  case 567:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].discrete_range) ; }
    break;

  case 568:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].discrete_range) ; }
    break;

  case 569:

    { yyval.array = 0 ; }
    break;

  case 570:

    {
            // VIPER #6534: Preserve comments around assoc-elements:
            yyval.discrete_range = yyvsp[-1].discrete_range ;
            add_pre_comments_to_node(yyval.discrete_range, yyvsp[-2].array) ; // pre-comments
            add_post_comments_to_node(yyval.discrete_range, yyvsp[0].array) ; // trailing comments
        }
    break;

  case 571:

    { yyval.discrete_range = yyvsp[0].expression ; }
    break;

  case 572:

    { yyval.discrete_range = new VhdlAssocElement(yyvsp[-2].name,yyvsp[0].expression) ; }
    break;

  case 573:

    { yyval.discrete_range = yyvsp[0].discrete_range ; }
    break;

  case 575:

    {yyval.expression = yyvsp[0].expression ; }
    break;

  case 576:

    {
               yyval.expression = new VhdlInertialElement(yyvsp[0].expression) ;
           }
    break;

  case 578:

    { yyval.expression = new VhdlOpen() ; }
    break;

  case 579:

    { yyval.signature = new VhdlSignature(yyvsp[-2].array, yyvsp[-1].name) ; }
    break;

  case 580:

    { yyval.name = 0 ; }
    break;

  case 581:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 582:

    { yyval.file_open_info = new VhdlFileOpenInfo(yyvsp[-3].expression, yyvsp[-1].unsigned_number, yyvsp[0].expression) ; }
    break;

  case 583:

    { yyval.expression = 0 ; }
    break;

  case 584:

    { yyval.expression = yyvsp[0].expression ; }
    break;

  case 586:

    { yyval.discrete_range = yyvsp[0].discrete_range ; }
    break;

  case 587:

    { yyval.discrete_range = new VhdlBox() ; }
    break;

  case 588:

    { yyval.array = yyvsp[-1].array ; }
    break;

  case 589:

    { yyval.discrete_range = yyvsp[0].name ; }
    break;

  case 590:

    { yyval.discrete_range = yyvsp[0].discrete_range ; }
    break;

  case 591:

    { yyval.discrete_range = new VhdlRange(yyvsp[-2].expression, yyvsp[-1].unsigned_number, yyvsp[0].expression) ; }
    break;

  case 592:

    { yyval.unsigned_number = VHDL_to ; }
    break;

  case 593:

    { yyval.unsigned_number = VHDL_downto ; }
    break;

  case 594:

    { yyval.discrete_range = yyvsp[0].name ; }
    break;

  case 595:

    { yyval.discrete_range = yyvsp[0].subtype_indication ; }
    break;

  case 596:

    { yyval.discrete_range = yyvsp[0].discrete_range ; }
    break;

  case 597:

    { yyval.discrete_range = yyvsp[0].subtype_indication ; }
    break;

  case 598:

    { yyval.discrete_range = yyvsp[0].discrete_range ; }
    break;

  case 599:

    { yyval.subtype_indication = yyvsp[0].name ; }
    break;

  case 600:

    { yyval.subtype_indication = yyvsp[0].subtype_indication ; }
    break;

  case 601:

    { yyval.subtype_indication = yyvsp[0].subtype_indication ; }
    break;

  case 602:

    { yyval.subtype_indication = new VhdlExplicitSubtypeIndication(yyvsp[-2].name, yyvsp[-1].name, yyvsp[0].discrete_range) ; }
    break;

  case 603:

    { yyval.subtype_indication = new VhdlExplicitSubtypeIndication(yyvsp[-1].name, yyvsp[0].name, 0) ; }
    break;

  case 604:

    { yyval.subtype_indication = new VhdlExplicitSubtypeIndication(0, yyvsp[-1].name, yyvsp[0].discrete_range) ; }
    break;

  case 605:

    { yyval.name= yyvsp[0].id ; }
    break;

  case 608:

    {
              yyval.name = new VhdlArrayResFunction(yyvsp[-1].name) ;
              if (!vhdl_file::IsVhdl2008()) (yyval.name)->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 609:

    {
              yyval.name = new VhdlRecordResFunction(yyvsp[-1].array) ;
              if (!vhdl_file::IsVhdl2008()) (yyval.name)->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 610:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].expression) ; }
    break;

  case 611:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].expression) ; }
    break;

  case 612:

    { yyval.expression = new VhdlRecResFunctionElement(yyvsp[-1].id, yyvsp[0].name) ; }
    break;

  case 618:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_nand, yyvsp[0].expression) ; }
    break;

  case 619:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_nor, yyvsp[0].expression) ; }
    break;

  case 620:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_and, yyvsp[0].expression) ; }
    break;

  case 621:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_and, yyvsp[0].expression) ; }
    break;

  case 622:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_or, yyvsp[0].expression) ; }
    break;

  case 623:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_or, yyvsp[0].expression) ; }
    break;

  case 624:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_xor, yyvsp[0].expression) ; }
    break;

  case 625:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_xor, yyvsp[0].expression) ; }
    break;

  case 626:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_xnor, yyvsp[0].expression) ; }
    break;

  case 627:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression, VHDL_xnor, yyvsp[0].expression) ; }
    break;

  case 629:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression,yyvsp[-1].unsigned_number,yyvsp[0].expression) ; }
    break;

  case 630:

    { yyval.unsigned_number = VHDL_EQUAL ; }
    break;

  case 631:

    { yyval.unsigned_number = VHDL_NEQUAL ; }
    break;

  case 632:

    { yyval.unsigned_number = VHDL_GTHAN ; }
    break;

  case 633:

    { yyval.unsigned_number = VHDL_STHAN ; }
    break;

  case 634:

    { yyval.unsigned_number = VHDL_GEQUAL ; }
    break;

  case 635:

    { yyval.unsigned_number = VHDL_SEQUAL ; }
    break;

  case 636:

    { yyval.unsigned_number = VHDL_matching_equal ; }
    break;

  case 637:

    { yyval.unsigned_number = VHDL_matching_nequal ; }
    break;

  case 638:

    { yyval.unsigned_number = VHDL_matching_gthan ; }
    break;

  case 639:

    { yyval.unsigned_number = VHDL_matching_sthan ; }
    break;

  case 640:

    { yyval.unsigned_number = VHDL_matching_gequal ; }
    break;

  case 641:

    { yyval.unsigned_number = VHDL_matching_sequal ; }
    break;

  case 643:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression,yyvsp[-1].unsigned_number,yyvsp[0].expression) ; }
    break;

  case 644:

    { yyval.unsigned_number = VHDL_sll ; }
    break;

  case 645:

    { yyval.unsigned_number = VHDL_srl ; }
    break;

  case 646:

    { yyval.unsigned_number = VHDL_sla ; }
    break;

  case 647:

    { yyval.unsigned_number = VHDL_sra ; }
    break;

  case 648:

    { yyval.unsigned_number = VHDL_rol ; }
    break;

  case 649:

    { yyval.unsigned_number = VHDL_ror ; }
    break;

  case 651:

    { yyval.expression = new VhdlOperator(yyvsp[0].expression,VHDL_PLUS,0) ; }
    break;

  case 652:

    { yyval.expression = new VhdlOperator(yyvsp[0].expression,VHDL_MINUS,0) ; }
    break;

  case 653:

    { yyval.expression = new VhdlOperator (yyvsp[-2].expression,yyvsp[-1].unsigned_number,yyvsp[0].expression) ; }
    break;

  case 654:

    { yyval.unsigned_number = VHDL_PLUS ; }
    break;

  case 655:

    { yyval.unsigned_number = VHDL_MINUS ; }
    break;

  case 656:

    { yyval.unsigned_number = VHDL_AMPERSAND ; }
    break;

  case 658:

    { yyval.expression = new VhdlOperator(yyvsp[-2].expression, yyvsp[-1].unsigned_number, yyvsp[0].expression) ; }
    break;

  case 659:

    { yyval.unsigned_number = VHDL_STAR ; }
    break;

  case 660:

    { yyval.unsigned_number = VHDL_SLASH ; }
    break;

  case 661:

    { yyval.unsigned_number = VHDL_mod ; }
    break;

  case 662:

    { yyval.unsigned_number = VHDL_rem ; }
    break;

  case 663:

    { yyval.unsigned_number = VHDL_and ; }
    break;

  case 664:

    { yyval.unsigned_number = VHDL_or ; }
    break;

  case 665:

    { yyval.unsigned_number = VHDL_xor ; }
    break;

  case 666:

    { yyval.unsigned_number = VHDL_xnor ; }
    break;

  case 667:

    { yyval.unsigned_number = VHDL_nand ; }
    break;

  case 668:

    { yyval.unsigned_number = VHDL_nor ; }
    break;

  case 670:

    { yyval.expression = new VhdlOperator(yyvsp[-2].expression, VHDL_EXPONENT, yyvsp[0].expression) ; }
    break;

  case 671:

    { yyval.expression = new VhdlOperator(yyvsp[0].expression,VHDL_abs,0) ; }
    break;

  case 672:

    { yyval.expression = new VhdlOperator(yyvsp[0].expression,VHDL_not,0) ; }
    break;

  case 673:

    { yyval.expression = new VhdlOperator(yyvsp[0].expression,VHDL_condition,0) ; }
    break;

  case 674:

    {
              // Viper #6187: logical reduction operators 1076-2008 LRM 9.2.2
              yyval.expression = new VhdlOperator (yyvsp[0].expression, yyvsp[-1].unsigned_number, 0) ;
              if (!vhdl_file::IsVhdl2008()) (yyval.expression)->Error("this construct is only supported in VHDL 1076-2008") ;
          }
    break;

  case 675:

    { yyval.expression=yyvsp[0].name ;}
    break;

  case 676:

    { yyval.expression=yyvsp[0].name ;}
    break;

  case 677:

    { yyval.expression=yyvsp[0].expression ;}
    break;

  case 678:

    { yyval.expression=yyvsp[0].expression ;}
    break;

  case 679:

    { yyval.expression=yyvsp[0].expression ; }
    break;

  case 680:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 681:

    { yyval.name = new VhdlPhysicalLiteral(yyvsp[-1].name,yyvsp[0].name) ; }
    break;

  case 682:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 683:

    { yyval.name = yyvsp[0].designator ; }
    break;

  case 684:

    { yyval.name = new VhdlNull() ; }
    break;

  case 685:

    {
             if ((yyvsp[-1].array) && (yyvsp[-1].array)->Size()==1) {
                VhdlExpression *elem = (VhdlExpression*)(yyvsp[-1].array)->GetFirst() ;
                if (elem && elem->IsAssoc()) {
                    /* Its an aggregate like (others=>'0') */
                    yyval.expression = new VhdlAggregate(yyvsp[-1].array) ;
                } else {
                    /* It's a parenthesized expression */
                    /* Delete the array, return the expression */
                    delete yyvsp[-1].array ;
                    elem->IncreaseNesting() ;
                    yyval.expression = elem ;
                }
             } else {
                yyval.expression = new VhdlAggregate(yyvsp[-1].array) ;
             }
           }
    break;

  case 686:

    { yyval.expression = new VhdlAllocator(yyvsp[0].name) ; }
    break;

  case 687:

    { yyval.expression = new VhdlAllocator(yyvsp[0].expression) ; }
    break;

  case 688:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].expression) ; }
    break;

  case 689:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].expression) ; }
    break;

  case 690:

    { yyval.array = 0 ; }
    break;

  case 691:

    { yyval.expression = new VhdlElementAssoc(yyvsp[-2].array,yyvsp[0].expression) ; }
    break;

  case 693:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].discrete_range) ; }
    break;

  case 694:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].discrete_range) ; }
    break;

  case 695:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].discrete_range) ; }
    break;

  case 696:

    { yyval.array = 0 ; }
    break;

  case 697:

    { yyval.discrete_range = yyvsp[0].expression ; }
    break;

  case 698:

    { yyval.discrete_range = yyvsp[0].discrete_range ; }
    break;

  case 699:

    { yyval.discrete_range = new VhdlOthers() ; }
    break;

  case 700:

    { yyval.id=0; }
    break;

  case 701:

    { yyval.id=yyvsp[0].id; }
    break;

  case 702:

    { yyval.expression=0; }
    break;

  case 703:

    { yyval.expression=yyvsp[0].expression; }
    break;

  case 704:

    { yyval.signature=0; }
    break;

  case 705:

    { yyval.signature=yyvsp[0].signature; }
    break;

  case 706:

    { yyval.array=0; }
    break;

  case 707:

    { yyval.array=yyvsp[0].array; }
    break;

  case 708:

    { yyval.array=0; }
    break;

  case 709:

    { yyval.array=yyvsp[0].array; }
    break;

  case 710:

    { yyval.designator=0; }
    break;

  case 711:

    { yyval.designator=yyvsp[0].designator; }
    break;

  case 712:

    { yyval.file_open_info=0; }
    break;

  case 713:

    { yyval.file_open_info=yyvsp[0].file_open_info; }
    break;

  case 714:

    { yyval.delay_mechanism=0; }
    break;

  case 715:

    { yyval.delay_mechanism=yyvsp[0].delay_mechanism; }
    break;

  case 716:

    { yyval.array=0; }
    break;

  case 717:

    { yyval.array=yyvsp[0].array; }
    break;

  case 718:

    { yyval.unsigned_number=0; }
    break;

  case 719:

    { yyval.unsigned_number=VHDL_body; }
    break;

  case 720:

    { yyval.unsigned_number=0; }
    break;

  case 721:

    { yyval.unsigned_number=VHDL_pure; }
    break;

  case 722:

    { yyval.unsigned_number=VHDL_impure; }
    break;

  case 723:

    { yyval.unsigned_number=0; }
    break;

  case 724:

    { yyval.unsigned_number=VHDL_procedure; }
    break;

  case 725:

    { yyval.unsigned_number=VHDL_function; }
    break;

  case 726:

    { yyval.unsigned_number=0; }
    break;

  case 727:

    { yyval.unsigned_number=VHDL_bus; }
    break;

  case 728:

    { yyval.unsigned_number=VHDL_register; }
    break;

  case 729:

    { yyval.unsigned_number=0; }
    break;

  case 730:

    { yyval.unsigned_number=VHDL_architecture; }
    break;

  case 731:

    { yyval.unsigned_number=0; }
    break;

  case 732:

    { yyval.unsigned_number=VHDL_entity; }
    break;

  case 733:

    { yyval.unsigned_number=0; }
    break;

  case 734:

    { yyval.unsigned_number=VHDL_package; }
    break;

  case 735:

    { yyval.unsigned_number=0; }
    break;

  case 736:

    { yyval.unsigned_number=VHDL_configuration; }
    break;

  case 737:

    { yyval.unsigned_number=0; }
    break;

  case 738:

    { yyval.unsigned_number=VHDL_is; }
    break;

  case 739:

    { yyval.unsigned_number=0; }
    break;

  case 740:

    { yyval.unsigned_number=VHDL_BOX; }
    break;

  case 741:

    { yyval.unsigned_number=0; }
    break;

  case 742:

    { yyval.unsigned_number=VHDL_shared; }
    break;

  case 743:

    { yyval.unsigned_number=0; }
    break;

  case 744:

    { yyval.unsigned_number=VHDL_postponed; }
    break;

  case 745:

    { yyval.unsigned_number=0; }
    break;

  case 746:

    { yyval.unsigned_number=VHDL_guarded; }
    break;

  case 747:

    { yyval.expression=0 ; }
    break;

  case 748:

    { yyval.expression=yyvsp[0].expression ; }
    break;

  case 749:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 750:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 751:

    { yyval.array = 0 ; }
    break;

  case 752:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].interface_decl) ; }
    break;

  case 753:

    {
              // Attach comments after the semicolon to the previous decl if it is in the same line.
              yyval.array = (yyvsp[-3].array) ? (yyvsp[-3].array) : new Array() ;
              // Get the last interface decl item from the Array.
              VhdlInterfaceDecl *last_decl = (VhdlInterfaceDecl *)(((yyval.array)->Size()) ? (yyval.array)->GetLast() : 0) ;
              if (last_decl && yyvsp[-1].array) { // trailing comments
                  // Take any comments accumulated till now.
                  Array *comments_after = vhdl_file::TakeComments() ;
                  // Set the comments to the previous node that is on the same line.
                  Array *comments_before = last_decl->ProcessPostComments(yyvsp[-1].array) ;
                  // Also, set the comments to the next node that is on its line.
                  comments_before = (yyvsp[0].interface_decl)->ProcessPostComments(comments_before) ;
                  // Append the comment array taken forcefully after the left over comments.
                  if (comments_before) {
                      comments_before->Append(comments_after) ;
                      // Delete this array we have already appended its elements to the other one.
                      delete comments_after ;
                  } else {
                      // We don't want to loose any comments
                      comments_before = comments_after ;
                  }
                  // Store the comment array for future needs.
                  vhdl_file::SetComments(comments_before) ;
              } else {
                  VhdlNode::DeleteComments(yyvsp[-1].array) ; // Delete comments to fix memory leak
              }
              (yyval.array)->InsertLast(yyvsp[0].interface_decl) ;
          }
    break;

  case 754:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 755:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 756:

    { yyval.array = 0 ; }
    break;

  case 757:

    { yyval.array = 0 ; }
    break;

  case 758:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].physical_unit_decl) ; }
    break;

  case 759:

    { yyval.array = 0 ; }
    break;

  case 760:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].entity_class_entry) ; }
    break;

  case 761:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].entity_class_entry) ; }
    break;

  case 762:

    { yyval.array = 0 ; }
    break;

  case 763:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 764:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 765:

    { yyval.array = 0 ; }
    break;

  case 766:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 767:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 768:

    { yyval.array = 0 ; }
    break;

  case 769:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].discrete_range) ; }
    break;

  case 770:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].discrete_range) ; }
    break;

  case 771:

    { yyval.array = 0 ; }
    break;

  case 772:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].element_decl) ; }
    break;

  case 773:

    { yyval.array = (yyvsp[-1].array) ? (yyvsp[-1].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].element_decl) ; }
    break;

  case 774:

    { yyval.array = 0 ; }
    break;

  case 775:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 776:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].name) ; }
    break;

  case 777:

    { yyval.array = 0 ; }
    break;

  case 778:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 779:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 780:

    { yyval.array = 0 ; }
    break;

  case 781:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 782:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 783:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 784:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 785:

    { yyval.array = 0 ; }
    break;

  case 786:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 787:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 788:

    { yyval.array = 0 ; }
    break;

  case 789:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 790:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 791:

    { yyval.array = 0 ; }
    break;

  case 792:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 793:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 794:

    { yyval.array = 0 ; }
    break;

  case 795:

    { yyval.array = new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 796:

    { yyval.array = (yyvsp[-2].array) ? (yyvsp[-2].array) : new Array() ; (yyval.array)->InsertLast(yyvsp[0].id_def) ; }
    break;

  case 797:

    { yyval.array = 0 ; }
    break;

  case 798:

    { yyval.type_def = new VhdlAccessTypeDef(yyvsp[0].subtype_indication) ; }
    break;

  case 799:

    { yyval.name = new VhdlInteger(yyvsp[0].unsigned_number) ; }
    break;

  case 800:

    { yyval.name = new VhdlReal(yyvsp[0].real_number) ; }
    break;

  case 801:

    { yyval.name = new VhdlPhysicalLiteral(0, yyvsp[0].name) ; }
    break;

  case 802:

    { yyval.name = new VhdlPhysicalLiteral(yyvsp[-1].name,yyvsp[0].name) ; }
    break;

  case 803:

    { yyval.designator = new VhdlCharacterLiteral(yyvsp[0].str) ; }
    break;

  case 804:

    { yyval.designator = new VhdlStringLiteral(yyvsp[0].str) ; }
    break;

  case 805:

    { yyval.name = new VhdlBitStringLiteral(yyvsp[0].str) ; }
    break;

  case 806:

    { yyval.name=yyvsp[0].id ; }
    break;

  case 807:

    { yyval.name=yyvsp[0].designator ; }
    break;

  case 812:

    { yyval.expression = new VhdlQualifiedExpression(yyvsp[-2].name,yyvsp[0].expression) ; }
    break;

  case 813:

    { yyval.name = new VhdlAttributeName(yyvsp[-2].name,yyvsp[0].id,0) ; }
    break;

  case 814:

    { // The Attribute index collides with indexed name.
            // This causes a shift-reduce conflict on an VHDL_OPAREN token.
            // This is resolved by setting precedence to stay inside this rule (shift)
            // a bit higher than reducing to indexed name.
            // If it turned out that no argument of this attribute is possible,
            // or there are more than one arguments, then apparently we needed an indexed name after all.
            // Determine that here and create the correct parse tree nodes :
            // VIPER #4305 : Create attribute name only for those attributes which
            // can have one argument
            if ((yyvsp[-3].id) && VhdlAttributeName::IsPredefinedAttributeWithOneArg((yyvsp[-3].id)->Name()) && (yyvsp[-1].array) && (yyvsp[-1].array)->Size()==1) {
                VhdlExpression *expr = (VhdlExpression*) (yyvsp[-1].array)->GetFirst() ;
                delete yyvsp[-1].array ; // Need to delete this array since it doesn't get absorbed!
                yyval.name = new VhdlAttributeName(yyvsp[-5].name,yyvsp[-3].id,expr) ;
            } else {
                yyval.name = new VhdlIndexedName(new VhdlAttributeName(yyvsp[-5].name, yyvsp[-3].id, 0), yyvsp[-1].array) ;
            }
          }
    break;

  case 815:

    { yyval.name = new VhdlSelectedName(yyvsp[-2].name,yyvsp[0].designator) ; }
    break;

  case 816:

    { yyval.name = new VhdlIndexedName(yyvsp[-3].name, yyvsp[-1].array) ; }
    break;

  case 817:

    { yyval.name = new VhdlExternalName(VHDL_constant, yyvsp[-3].ext_path_name.path_token, yyvsp[-3].ext_path_name.hat_count, yyvsp[-3].ext_path_name.path, yyvsp[-1].subtype_indication) ; }
    break;

  case 818:

    { yyval.name = new VhdlExternalName(VHDL_signal, yyvsp[-3].ext_path_name.path_token, yyvsp[-3].ext_path_name.hat_count, yyvsp[-3].ext_path_name.path, yyvsp[-1].subtype_indication) ; }
    break;

  case 819:

    { yyval.name = new VhdlExternalName(VHDL_variable, yyvsp[-3].ext_path_name.path_token, yyvsp[-3].ext_path_name.hat_count, yyvsp[-3].ext_path_name.path, yyvsp[-1].subtype_indication) ; }
    break;

  case 823:

    { yyval.ext_path_name.path_token = VHDL_AT ; yyval.ext_path_name.hat_count = 0; yyval.ext_path_name.path = yyvsp[0].name ; }
    break;

  case 824:

    { yyval.ext_path_name.path_token = VHDL_DOT ; yyval.ext_path_name.hat_count = 0; yyval.ext_path_name.path = yyvsp[0].name ; }
    break;

  case 825:

    { yyval.ext_path_name.path_token = (yyvsp[-1].array) ? VHDL_ACCENT: 0 ; yyval.ext_path_name.hat_count = yyvsp[-1].array ? yyvsp[-1].array->Size(): 0; yyval.ext_path_name.path = yyvsp[0].name ; delete yyvsp[-1].array ; }
    break;

  case 826:

    { yyval.array = 0 ; }
    break;

  case 827:

    { yyval.array = yyvsp[-1].array ; }
    break;

  case 828:

    { yyval.array = new Array() ; yyval.array->InsertLast(0) ; }
    break;

  case 829:

    { yyval.array = yyvsp[-2].array ? yyvsp[-2].array : new Array() ; yyval.array->InsertLast(0) ; }
    break;

  case 830:

    { yyval.designator = yyvsp[0].id ; }
    break;

  case 831:

    { yyval.designator = yyvsp[0].designator ; }
    break;

  case 832:

    { yyval.designator = yyvsp[0].designator ; }
    break;

  case 834:

    { yyval.id = new VhdlIdRef(Strings::save("range")) ; }
    break;

  case 835:

    { yyval.id = new VhdlIdRef(Strings::save("subtype")) ; }
    break;

  case 836:

    { yyval.name = (yyvsp[0].signature) ? new VhdlSignaturedName(yyvsp[-1].name,yyvsp[0].signature) : (yyvsp[-1].name) ; }
    break;

  case 837:

    { yyval.name = new VhdlOthers() ; }
    break;

  case 838:

    { yyval.name = new VhdlAll() ; }
    break;

  case 839:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 840:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 841:

    { yyval.name = yyvsp[0].name ; }
    break;

  case 842:

    { yyval.name = (yyvsp[0].signature) ? new VhdlSignaturedName(yyvsp[-1].name,yyvsp[0].signature) : yyvsp[-1].name ; }
    break;

  case 843:

    { yyval.designator = yyvsp[0].id ; }
    break;

  case 844:

    { yyval.designator = yyvsp[0].designator ; }
    break;

  case 845:

    { yyval.designator = yyvsp[0].designator ; }
    break;

  case 846:

    { yyval.designator = new VhdlAll() ; }
    break;

  case 849:

    { yyval.id = yyvsp[0].id ; }
    break;

  case 850:

    {
              // entity name for architecture or configuration.
              // $1 should denote the name of present work library, or "work"
              if (!Strings::compare(yyvsp[-2].id->Name(),"work") &&
                  !Strings::compare(yyvsp[-2].id->Name(),vhdl_file::GetWorkLib()->Name())) {
                 yyvsp[-2].id->Error("entity should be in present work library") ;
              }
              delete yyvsp[-2].id ;
              yyval.id = yyvsp[0].id ;
          }
    break;

  case 851:

    { yyval.id = new VhdlIdRef(yyvsp[0].str) ; }
    break;

  case 853:

    { yyval.name = yyvsp[0].id ; }
    break;

  case 854:

    { yyval.name = new VhdlStringLiteral(yyvsp[0].str) ; }
    break;

  case 855:

    { yyval.id_def= new VhdlPhysicalUnitId(yyvsp[0].str) ;vhdl_declare(yyval.id_def) ; }
    break;

  case 856:

    { yyval.id_def= new VhdlElementId(yyvsp[0].str) ;vhdl_declare(yyval.id_def) ; }
    break;

  case 857:

    { yyval.id_def= new VhdlLibraryId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 858:

    { yyval.id_def= new VhdlInterfaceId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; if (in_generic_clause && yyval.id_def && VhdlNode::IsVhdl2008()) yyval.id_def->SetObjectKind(VHDL_generic) ;}
    break;

  case 859:

    { yyval.id_def= new VhdlSubtypeId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 860:

    {
            yyval.id_def = new VhdlTypeId(yyvsp[0].str) ;
            VhdlIdDef *exist = VhdlTreeNode::_present_scope->FindSingleObjectLocal(yyval.id_def->Name()) ;
            if (exist && exist->IsProtected()) {
                char *name = Strings::save(yyvsp[0].str) ;
                delete yyval.id_def ;
                yyval.id_def = new VhdlProtectedTypeBodyId(name) ;
            } else {
                vhdl_declare(yyval.id_def) ;
            }
        }
    break;

  case 861:

    {
            yyval.id_def = new VhdlGenericTypeId(yyvsp[0].str) ;
            VhdlIdDef *exist = VhdlTreeNode::_present_scope->FindSingleObjectLocal(yyval.id_def->Name()) ;
            if (exist && exist->IsProtected()) {
                char *name = Strings::save(yyvsp[0].str) ;
                delete yyval.id_def ;
                yyval.id_def = new VhdlProtectedTypeBodyId(name) ;
            } else {
                vhdl_declare(yyval.id_def) ;
            }
        }
    break;

  case 862:

    { yyval.id_def= new VhdlConstantId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 863:

    { yyval.id_def= new VhdlSignalId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; gLastDeclVhdlId = yyval.id_def ; }
    break;

  case 864:

    { yyval.id_def= new VhdlVariableId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 865:

    { yyval.id_def= new VhdlFileId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 866:

    { yyval.id_def= new VhdlComponentId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 867:

    { yyval.id_def= new VhdlAttributeId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 868:

    { yyval.id_def=new VhdlGroupTemplateId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 869:

    { yyval.id_def=new VhdlGroupId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 870:

    { yyval.id_def= new VhdlSubprogramId(yyvsp[0].str) ; gLastDeclVhdlId = yyval.id_def ; vhdl_declare(yyval.id_def) ; }
    break;

  case 871:

    { yyval.id_def = new VhdlSubprogramId(yyvsp[0].str) ; gLastDeclVhdlId = yyval.id_def ; vhdl_declare(yyval.id_def) ; }
    break;

  case 872:

    { yyval.id_def= new VhdlSubprogramId(yyvsp[0].str) ; gLastDeclVhdlId = yyval.id_def ; vhdl_declare(yyval.id_def) ; }
    break;

  case 873:

    { yyval.id_def= new VhdlEnumerationId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 874:

    { yyval.id_def= new VhdlEnumerationId(yyvsp[0].str) ; vhdl_declare(yyval.id_def) ; }
    break;

  case 875:

    { yyval.id_def= new VhdlAliasId(yyvsp[0].str) ; }
    break;

  case 876:

    { yyval.id_def = new VhdlAliasId(yyvsp[0].str) ; }
    break;

  case 877:

    { yyval.id_def= new VhdlAliasId(yyvsp[0].str) ; }
    break;

  case 878:

    { yyval.id_def = new VhdlLabelId(yyvsp[0].str) ; vhdl_declare_label(yyval.id_def) ; last_label = yyval.id_def ; }
    break;

  case 879:

    { yyval.id_def= new VhdlEntityId(yyvsp[0].str) ; gLastDeclVhdlId = yyval.id_def ; }
    break;

  case 880:

    { yyval.id_def= new VhdlArchitectureId(yyvsp[0].str) ; }
    break;

  case 881:

    { yyval.id_def= new VhdlConfigurationId(yyvsp[0].str) ; }
    break;

  case 882:

    { yyval.id_def= new VhdlPackageId(yyvsp[0].str) ; }
    break;

  case 883:

    { yyval.id_def= new VhdlPackageBodyId(yyvsp[0].str) ;  }
    break;

  case 884:

    { yyval.array = vhdl_file::TakeComments() ; }
    break;

  case 885:

    { yyval.array = vhdl_file::TakeComments() ; }
    break;

  case 886:

    { yyval.array = 0 ; VERIFIC_ASSERT(0) ; /* force bison to read next token */ }
    break;


    }

/* Line 1000 of yacc.c.  */


  yyvsp -= yylen;
  yyssp -= yylen;
  yylsp -= yylen;

  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  const char* yyprefix;
	  char *yymsg;
	  int yyx;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 0;

	  yyprefix = ", expecting ";
	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		yysize += yystrlen (yyprefix) + yystrlen (yytname [yyx]);
		yycount += 1;
		if (yycount == 5)
		  {
		    yysize = 0;
		    break;
		  }
	      }
	  yysize += (sizeof ("syntax error, unexpected ")
		     + yystrlen (yytname[yytype]));
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yyprefix = ", expecting ";
		  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			yyp = yystpcpy (yyp, yyprefix);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yyprefix = " or ";
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }

  yylerrsp = yylsp;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* If at end of input, pop the error token,
	     then the rest of the stack, then return failure.  */
	  if (yychar == YYEOF)
	     for (;;)
	       {
		 YYPOPSTACK;
		 if (yyssp == yyss)
		   YYABORT;
		 YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
		 yydestruct (yystos[*yyssp], yyvsp, yylsp);
	       }
        }
      else
	{
	  YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
	  yydestruct (yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	  *++yylerrsp = yylloc;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

#ifdef __GNUC__
  /* Pacify GCC when the user code never invokes YYERROR and the label
     yyerrorlab therefore never appears in user code.  */
  if (0)
     goto yyerrorlab;
#endif

  yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  yylerrsp = yylsp;
  *++yylerrsp = yyloc;
  yylsp -= yylen;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
  YYLLOC_DEFAULT (yyloc, yylsp, yylerrsp - yylsp);
  *++yylsp = yyloc;

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}





void
vhdl_file::StartYacc()
{
    VhdlTreeNode::_present_scope = 0 ; /* Just in case */
    first_line = 0 ;
    first_column = 0 ;
    last_line = 0 ;
    last_column = 0 ;
    context_clause = 0 ;
    inside_design_unit = 0 ;
    in_generic_clause = 0 ;
    //CARBON_BEGIN
    delete last_label ; last_label = 0 ; // VIPER #7714: Reset all labels
    //CARBON_END

// Temporary storage for actual analysis mode; currently needed for embedded psl
    rhs_0_initialized  = 0 ; // VIPER #6608: Start with Rhs[0] as uninitialized
}

linefile_type
vhdl_file::GetRuleLineFile()
{
    // Get the linefile info from the last 'rule' (as opposed to last token)

    // get it for the last token (since that includes the filename-id, which is not available here in bison)
    linefile_type result = GetLineFile() ;
    // VIPER #4002 side effect: Don't try to set line & file info if GetLineFile returns null:
    if (!result) return result ;
    // and adjust it to the info in the last rule :

    // Exact range info from the last rule is available in the bison-generated yylloc variable.
    // However, that one is local to routine 'yyparse', so we export it via
    // static variables, set in the overridden macro YYLLOC_DEFAULT.

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Use all location info to initialize the result to latest bison rule that was executed.
    // GetLineFile() already made a copy.
    result->SetLeftCol(first_column) ;
    result->SetLeftLine(first_line) ;
    result->SetRightCol(last_column) ;
    result->SetRightLine(last_line) ;
#else
    // Use only line info from the current (latest) bison rule executed:
    // Decide here which line number (first or last in the rule) should be set as the rule's line number..
#ifdef VHDL_USE_STARTING_LINEFILE_INFO
    result = LineFile::SetLineNo(result, first_line) ;
#else
    result = LineFile::SetLineNo(result, last_line) ;
#endif

#endif
    return result ;
}

void
vhdl_file::EndYacc()
{
    cleanup_context_clause() ;
    //CARBON_BEGIN
    delete last_label ; last_label = 0 ; // VIPER #7714: Reset all labels
    //CARBON_END
}

int
vhdl_file::Parse()
{
#if YYDEBUG
    yydebug = 1 ;
#endif
    inside_design_unit = 0 ;
    return yyparse() ;
}


