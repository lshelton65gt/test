/*
 *
 * [ File Version : 1.130 - 2014/03/11 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlDataFlow_Elab.h" // Compile flags are defined within here

#include "Set.h"
#include "Array.h"
#include "Message.h"

#include "vhdl_tokens.h"
#include "VhdlIdDef.h"
#include "VhdlExpression.h" // For 'VhdlDiscreteRange'
#include "VhdlValue_Elab.h"
#include "VhdlStatement.h"  // For VhdlWaitStatement
#include "VhdlScope.h"      // Scope access from within a dataflow

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VhdlDataFlow::VhdlDataFlow(VhdlDataFlow *owner, Net *clock, VhdlScope *scope, Set *sensitivity_list)
  : Map(POINTER_HASH),
    _owner(owner),
    _goto_areas(0),
    _sensitivity_list(sensitivity_list),
    _scope(scope),
    _condition_id_value_map(0),
    _conditionids(0),
    _clock(clock),
    _clock_enable(0),
#ifdef VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
    _from_treenode(0),
#endif
    _is_dead_code(0),
    _is_waiting(0)
    , _nonconst_expr(0)
//    , _condition_expr(0) // RD: no longer needed since VIPER 7018 fix.
    , _in_initial(0)
    , _is_forced_initial(0)
    , _in_subprogram_or_process(0)
    , _incomplete_sensitivity_list(0)
    , _sensitivity_list_all(0)
    , _subprog_inst(0)
{
    // If there is no explicit sensitivity list
    // and the dataflow is not clocked
    // and the dataflow is not a separate declarative area (subprogram with a scope)
    // then use the one from above if the dataflow.
    // This speeds up sensitivity list checking when a identifier is used somewhere.
    if (!_sensitivity_list && !IsClocked() && owner && !owner->IsClocked() && !_scope) {
        _sensitivity_list = owner->GetSensitivityList() ;
    }

    // Viper #7413: Mark for process(all)
    if (owner) _sensitivity_list_all = owner->_sensitivity_list_all ;

    // Pass the clock through from the owner :
    if (!_clock && owner) _clock = owner->GetClock() ;
    if (owner) _clock_enable = owner->GetClockEnable() ;

#ifdef VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
    if (owner) _from_treenode = owner->_from_treenode ;
#endif

    if (owner && owner->IsNonconstExpr()) _nonconst_expr = 1 ;

    // Inherit the inside initial flag from the owner:
    if (owner) _in_initial = owner->_in_initial ;
    if (owner) _is_forced_initial = owner->_is_forced_initial ;
    if (owner) _in_subprogram_or_process = owner->_in_subprogram_or_process ;
    if (owner) _subprog_inst = owner->_subprog_inst ; // Inherit from owner
}

VhdlDataFlow::~VhdlDataFlow()
{
    MapIter mi ;
    VhdlValue *val ;

    UpdateInitialValues() ; // Viper #5687

    // Delete actual values
    FOREACH_MAP_ITEM(this, mi, 0, &val) {
        delete val ;
    }

    FOREACH_MAP_ITEM(_condition_id_value_map, mi, 0, &val) delete val ;
    delete _condition_id_value_map ;

    // Delete the goto-areas (if any)
    // CHECK ME : Should all be empty now ?
    VhdlGotoArea *area ;
    FOREACH_MAP_ITEM(_goto_areas, mi, 0, &area) {
        delete area ;
    }
    delete _goto_areas ;

    // Delete assertion identifiers (they are owned by the data flow) :
    SetIter si ;
    VhdlIdDef *assert_id ;
    FOREACH_SET_ITEM(_conditionids, si, &assert_id) {
        delete assert_id ;
    }
    delete _conditionids ;

    // Since VIPER 2304, a sensitivity list can come in from a wait statement.
    // Still, we want children dataflows still to inherit their parent sensitivity lists.
    // So, sensitivity list is only owned if the upper dataflow does not have the same pointer :
    if (_sensitivity_list && (!_owner || (_owner->GetSensitivityList()!=_sensitivity_list))) {
        delete _sensitivity_list ;
    }
}

void VhdlDataFlow::UpdateInitialValues() // Viper #5687
{
    if (!_in_initial || _is_forced_initial) return ;

    MapIter mi ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(this, mi, &id, &val) {
        if (!id || !val) continue ;
        // Viper 7932: Do not assign to a constant value.
        if (id->Value() && !id->Value()->IsConstant() && (id->IsSignal() || IsConditionId(id))) {
            id->Value()->NetAssign(val,id->ResolutionFunction(), id) ;
        } else {
            id->SetValue(val) ;
        }
        (void) Remove(id) ;
    }

    return ;
}

unsigned
VhdlDataFlow::SetConditionIdValueMap(Map *id_value_map)
{
    if (id_value_map && _condition_id_value_map) return 1 ; // overwrite the map ?
    _condition_id_value_map = id_value_map ;
    return 0 ;
}

void
VhdlDataFlow::SetSensitivityListAll()
{
    // Viper #7413: Mark for process(all)
    _sensitivity_list_all = 1 ;
}

void
VhdlDataFlow::SetIncompleteSensList()
{
    if (_sensitivity_list_all) return ; // cannot be incomplete

    _incomplete_sensitivity_list = 1 ;
    if (_owner) _owner->SetIncompleteSensList() ;
}

unsigned
VhdlDataFlow::SensListCheck(const VhdlIdDef *id) const
{
    // Check if the id is on the sensitivity list
    // Return 1 if it is, 0 otherwise
    // Do the most common, obvious checks first, for speed :
    if (!_sensitivity_list) return 1 ; // No sens list : we are in a 'clocked' or subprogram environment
    if (IsClocked()) return 1 ; // This flow is clocked, with a sens-list : must be process with wait statement.

    if (_sensitivity_list_all) return 1 ; // Viper #7413: Mark for process(all)

    // Only signals need to be checked to be on the sens list
    if (!id->IsSignal()) return 1 ;

    // Now hash if it is really there.
    // If needed, we could insert the id, to reduce warnings.
    if (_sensitivity_list->GetItem(id)) return 1 ;

    // FIX ME : presently, subprogram outputs do 'evaluate' the actual output expression
    // so that elaboration can build correct logic in the logic generation strategy that
    // is used. This means that output expressions get 'read' and thus end up here to
    // test if their signals are on the sens list.
    // That is incorrect, since the output actual officially does NOT get 'read'.
    // So, some sens-list warnings could be bogus.
    // To catch some obvious ones : test if this id is an output itself.
    // If it is, it can officially never be read, and error will have been given already.
    // So if this id is an output itself, it means that it is in an actual of a subprogram
    // output expression. So skip it.
    // Viper 7480 7481 : For output check if it is used. In Vhdl2008 outputs can be read.
    // if it is read the IsUsed flag will be set.
    if (id->IsOutput()) {
        if (!id->IsUsed()) return 1 ;
    }
    return 0 ; // Signal is not on the sens-list
}

unsigned
VhdlDataFlow::IsImplicitClock(const VhdlValue *condition) const
{
    // Check if this condition represents an implicit clock
    if (!condition) return 0 ;

    // Code only for RTL elaboration

    return 0 ;
}

VhdlValue *
VhdlDataFlow::IdValue(VhdlIdDef *id) const
{
    if (!id) return 0 ;
    // In non-constant region of static elaboration, do not return value
    // VIPER #7286 : Insert and propagate null value in static elab mode.
    // So do not use '_nonconst_expr' flag.
    //if (_nonconst_expr) return 0 ;

    // Obtain a POINTER to the blocking value of a variable or signal.
    // This is the value is has in this dataflow, considering possible
    // assignments made since the start of its existence.
    // VIPER #7286 : Consider null value too in static elab only
    VhdlValue *res = 0 ;
    MapItem *item = GetItem(id) ;
    if (item) res = (VhdlValue*)item->Value() ;
    if (item && VhdlNode::IsStaticElab()) return res ;
    //VhdlValue *res = (VhdlValue*)GetValue(id) ;
    if (res) return res ;

    // Get it from a higher level
    if (_owner) {
        // Issue 1833 : before we look in upper dataflow,
        // we need to check if the identifier is declared here.
        // If so, the upper dataflow might contain the value of
        // this identifier in a upper call of the same recursive function.
        // In other words, if the lifetime of the identifier starts here,
        // we should NOT go into upper dataflow.
        if (_scope && _scope->IsDeclaredHere(id)) {
            // Return id initial value since its lifetime started here with this dataflow.
            // return id->Value() ; // drop-through to initial value return
        } else {
            // No assignments done in 'this' dataflow area.
            // Search upward for value assignments.
            return _owner->IdValue(id) ;
        }
    }
    // Get it from its initial value
    // VIPER 3789/3795 : for static elab : return valid value if dataflow is in 'initial' state only.
    // VIPER #5698 : If the identifier is not assigned anywhere, we can always
    // pick up its initial value, even inside the process.
    if (VhdlNode::IsStaticElab() && !IsInInitial() && id->IsAssigned()) return 0 ;
    // if (VhdlNode::IsStaticElab() && !IsInInitial() && (id->IsSignal() || id->IsVariable())) return 0 ;

    return id->Value() ;
}

VhdlValue *
VhdlDataFlow::AssignValue(VhdlIdDef *id)
{
    // VIPER #7286 : Consider null value as valid value in static elab mode
    // Obtain a new value to assign to ; for this id
    VhdlValue *res = 0 ;
    MapItem *item = GetItem(id) ;
    if (item) res = (VhdlValue*)item->Value() ;
    if (item && VhdlNode::IsStaticElab()) return res ;

    //VhdlValue *res = (VhdlValue*)GetValue(id) ;
    // In non-constant region of static elaboration (i.e. inside process), do not
    // update identifier/data flow with new value. It can create illegal behavior.
    //if (_nonconst_expr) {
        //if (res) {
            //delete res ;
            //(void) Insert(id, 0, 1/*force insert to overwrite existing entry*/) ;
        //}
        //return 0 ;
    //}
    if (res) return res ; // Use existing one

    // Get a copy from the present blocking value
    res = IdValue(id) ;
    if (VhdlNode::IsStaticElab() && GetItem(id) && !res) { // VIPER #7286 : Consider null value
        (void) Insert(id, 0) ;
        return 0 ;
    }
    if (!res) return 0 ; // Something wrong
    res = res->Copy() ; // Make the copy
    // Install the copy under 'id'
    (void) Insert(id, res) ;
    return res ;
}

VhdlValue *
VhdlDataFlow::SignalValue(VhdlIdDef *id) const
{
    // Viper #5553: This API returns id->Value except when
    // this is a branch dataflow under (id == id_value)
    // condition. In such cases, it returns the id_value
    // from the dataflow
    // VIPER #7286 : Consider null value only in static elaboration
    VhdlValue *id_value = 0 ;
    MapItem *item = _condition_id_value_map ? _condition_id_value_map->GetItem(id): 0 ;
    if (item) id_value = (VhdlValue*)item->Value() ;
    if (item && VhdlNode::IsStaticElab()) return id_value ;
    //VhdlValue *id_value = _condition_id_value_map ? (VhdlValue*)_condition_id_value_map->GetValue(id) : 0 ;
    if (!id_value) id_value = _owner ? _owner->SignalValue(id) : id->Value() ;
    return id_value ;
}

void
VhdlDataFlow::SetAssignValue(const VhdlIdDef *id, const VhdlValue *val)
{
    // VIPER #7286 : Accept 0 as 'val' for static elaboration
    if (!val && VhdlNode::IsRtlElab()) return ; // Don't accept setting 0
    // See if we need to remove the existing one
    VhdlValue *exist = (VhdlValue*)GetValue(id) ;
    if (exist) delete exist ;

    // VIPER #7286 : Donot rely on _nonconst_expr
    //if (_nonconst_expr) {
        //delete val ; // This routine absorbs value
        //val = 0 ;
    //}
    (void) Insert(id, val, 1/*force insert to overwrite existing entry*/) ;
}

void
VhdlDataFlow::InsertIds(Set &list) const
{
    MapIter i ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(this, i, &id, 0) {
        (void) list.Insert(id) ;
    }

    // Also accumulate ids from goto-areas. Needed for PopOutOf routines.
    VhdlGotoArea *area ;
    FOREACH_MAP_ITEM(_goto_areas, i, 0, &area) {
        MapIter ii ;
        FOREACH_MAP_ITEM(area, ii, &id, 0) {
            (void) list.Insert(id) ;
        }
    }
}

void
VhdlDataFlow::InsertLabels(Set &labels) const
{
    if (!_goto_areas) return ;
    MapIter i ;
    VhdlIdDef *label ;
    FOREACH_MAP_ITEM(_goto_areas, i, &label, 0) (void) labels.Insert(label) ;
}

unsigned
VhdlDataFlow::IsTemporaryDataFlow() const
{
    // This subprogram is temporary (contains temporary lifetime variables)
    // if it has a scope, and the owner of the scope is a subprogram.
    if (!_scope) return 0 ;

    VhdlIdDef *scope_owner = _scope->GetOwner() ;
    if (!scope_owner) return 0 ;

    return scope_owner->IsSubprogram() ;
}

// Check if this identifier has been assigned previously (here or in upper flow).
unsigned VhdlDataFlow::HasBeenAssigned(VhdlIdDef *id) const
{
    if (!id) return 0 ;

    // Vars are initially assigned inside subprograms
    if( IsTemporaryDataFlow() ) return 1;

    // check in local data flow :
    if (GetItem(id)) return 1 ;

    // check upper flows :
    if (_owner) return _owner->HasBeenAssigned(id) ;

    // id is never assigned up till now
    return 0 ;
}

VhdlGotoArea *
VhdlDataFlow::GetGotoArea(const VhdlIdDef *label) const
{
    if (!_goto_areas) return 0 ;
    return (VhdlGotoArea*)_goto_areas->GetValue(label) ;
}

void
VhdlDataFlow::AddGotoArea(VhdlGotoArea *area, const VhdlTreeNode *from)
{
    if (!area) return ;
    VERIFIC_ASSERT (from) ; // need this for logic creation below.
    VERIFIC_ASSERT (!area->Owner()) ; // Cant add a goto-area if already attached to some dataflow
    area->SetOwner(this) ;

    // A goto was executed in the last statement.
    // Could be a regular hard 'goto', or a conditional
    // goto, from a resolved conditional area
    // Need to add the goto area to me now.
    //

    // First make sure that we add present values of id's that
    // were not yet assigned (in the goto area).
    // These id's really had the 'present_assign_value' at the time the
    // goto did execute. We have to save that before any later assignment
    // changes the value.
    // Otherwise the following code will not execute correctly :
    //
    //     O <= 1 ;
    //        -- Here no label area exists
    //     if (sel1) return ;
    //        -- Here, label area 'exist' contains cond:sel1 value:1
    //     O <= 2 ;
    //     if (sel2) return ;
    //        -- When we resolve this 'if', 'area' is empty (cond:sel2)
    //        -- Need to insert value:2 for O, and then resolve with the
    //        -- existing goto area.
    //
    MapIter mi ;
    VhdlIdDef *id ;
    VhdlValue *val ;
    FOREACH_MAP_ITEM(this, mi, &id, &val) {
        if (area->GetItem(id)) continue ;
        (void) area->Insert(id, val ? val->Copy() : 0) ;
    }

    //
    // What still might go wrong is variables that are not assigned
    // when the first goto hits :
    //   procedure (c : in boolean ; x : inout integer) is
    //         if (c) return
    //         x <= 1 ;
    //   end
    // Here, we did not yet insert a value for x into dataflow.
    //
    // This just got worse with 'exit' and 'next' statements :
    // it now applies to ANY variable of signal that is not yet assigned
    // earlier when an exit (or next) hits :
    //    for i in 0 to 5 loop
    //        exit when c1
    //        x <= 2
    //    end loop
    // Here, x should hold its value if c1 applies.
    //
    // Any way, We fixed this below, and in VhdlDataFlow::Resolve() :
    // If a 'goto' area exists, and we add one,
    // and there is an id that is in the new one, but not in the existing one,
    // then the id was apparently not assigned when that (existing) goto hit.
    // So, its value is the value it had before it entered this dataflow area.
    //

    if (!_goto_areas) _goto_areas = new Map(POINTER_HASH) ;

    // Issue 2337 (Verilog) and 2190 (VHDL),
    // Also issue 2446 (Verilog).
    // If there are existing goto's to OTHER labels existing here locally,
    // then we need to make sure that the new goto is not activated under these existing jump conditions :
    //
    //    new_cond = NOT exist_cond AND area_cond
    //
    // After all, this area goto will not execute if ANY previous goto is active...
    //
    // So we need to scan conditions in ALL goto areas. Not just the ones that are to 'label'.
    //
    // This will add more reconvergent fanout to the control nets if there are various nested jumps to different labels.
    // But I see no way to avoid that extra (redundancy) in the logic.
    VhdlGotoArea *exist_area ;
    Net *exist_cond ;
    Net *area_cond ;
    FOREACH_MAP_ITEM(_goto_areas, mi, 0, &exist_area) {
        if (exist_area->Label()==area->Label()) continue ; // that is a jump to the same label. Adjust below.

        // OK. another label jump. Take its condition :
        exist_cond = exist_area->Condition() ;

        // And get my area condition :
        area_cond = area->Condition() ;

        // Adjust condition to 'area' :
        if (exist_cond && area_cond) {
            area_cond = from->And(area_cond, from->Inv(exist_cond)) ;
        } else if (exist_cond) {
            area_cond = from->Inv(exist_cond) ;
        }

        area->SetCondition(area_cond) ;
    }

    // See if we have already a 'goto-this-label' area here :
    VhdlGotoArea *exist = GetGotoArea(area->Label()) ;
    if (exist) {
        // Got to work with an existing goto area...
        //
        // We are adding a condition and a list of id-values to
        // the present dataflow, which has a goto condition (with id-values)
        // already, to the same goto label.
        //
        // Happens if multiple jumps occur to same label. For example :
        //    if (exist_cond) return foo ;
        //    if (area_cond) return bar ;
        //    ...
        //
        // The previous goto condition/id-values has higher priority.
        // So, create existing values as follows :
        //
        //     if (exist_cond)
        //          new_val = exist_val
        //     else
        //          new_val = area_goto_val
        //     endif
        //
        // if an id is not in 'exist', just take 'area_goto_val'.
        // if an id is not in 'area', just take 'exist_val'.
        //
        // Since the new condition that ANY goto-this-label occurs is
        // now the OR of both existing and new goto area,
        // adjust condition as follows :
        //
        //    new_cond = exist_cond OR area_cond
        //
        //
        // Normally, direct synthesis create LSB->MSB mux trains since the
        // highest priority condition is always the last one. For goto-conditions
        // however, the first one that hits has highest priority.
        // So this will create a MUX train in opposite-from-normal order
        // (MSB->LSB), with OR-ed conditions to get the
        // priority corrected. That is funky logic which will mainly occur
        // for 'exit' statements (since they hit each time we go through the loop).
        // That is the price we pay for direct-synthesis.
        // There is NO easy way out to invert the MUX train logic.
        // I thought very hard about that.
        //

        exist_cond = exist->Condition() ;
        VhdlValue *exist_val ;
        MapItem *item ;
        FOREACH_MAP_ITEM(area, mi, &id, &val) {
            // Pick up existing value of id (we will override this position below, so we can freely absorb it)
            item = exist->GetItem(id) ;
            exist_val = item ? (VhdlValue*)item->Value(): 0 ;
            if (item && !exist_val) {
                delete val ;
                (void) exist->Insert(id, 0, 1/*force insert*/ ) ;
                continue ; // VIPER #7286: If 'id' has null value in data flow, continue
            }
            //exist_val = (VhdlValue*)exist->GetValue(id) ;
            // If 'exist_val' is not there, there was no assignment to 'id' made
            // when that 'goto' hit. So, the value at that point was the value
            // that id had before it entered this dataflow !. So, get the
            // (existing goto) value from the upper dataflow. If there is no upper
            // dataflow, then use the 'initial' value.
            // NOTE : We also do this check in the 'Resolve()' routine below, for
            // if there is just a single 'goto' before we resolve.
            if (!exist_val) {
                exist_val = (_owner) ? _owner->IdValue(id) : id->Value() ;
                // Copy exist_val, since it is absorbed below.
                exist_val = (exist_val) ? exist_val->Copy() : 0 ;
            }
            if (!exist_val) continue ; // some problem with the value of this id.

            if (exist_cond) {
                VhdlNonconstBit select_bit(exist_cond) ;
                // SelectOn1 absorbs both values.
                val = val ? val->SelectOn1(&select_bit, exist_val, id->Value(), from): 0 ;
            } else {
                // exist_cond always applies. A return after a return ?
                // Should never have gotten here, (unless we did something in dead code), but use exist_val to be consistent.
                delete val ;
                val = exist_val ;
            }
            // force overwrite existing value 'exist_val' (which has been deleted above)
            (void) exist->Insert(id, val, 1/*force insert*/ ) ;
        }

        area->Reset() ; // all values are removed from 'area', so reset it (so we won't access it any more).

        // Adjust 'exist' condition
        if (exist_cond && area->Condition()) {
                // Or the two conditions.
            exist_cond = from->Or(exist_cond,area->Condition()) ;
        } else {
            // condition always applies. No need to OR anything (would result in Pwr() any way.)
            // Just set exist_cond=0, which means the same a 'Pwr()' for goto area conditions.
            exist_cond = 0 ;
        }
        exist->SetCondition(exist_cond) ; // absorbs 'exist_cond'.
        delete area ;
    } else {
        // Simply insert the new entry
        (void) _goto_areas->Insert(area->Label(), area) ;
    }
}

void
VhdlDataFlow::DoGoto(VhdlIdDef *goto_label, const VhdlValue *condition, const VhdlTreeNode *from)
{
    if (!goto_label) return ; // no idea where to go..
    if (!from) return ; // no idea where we came from. Need that for adding a goto area to the data flow.

    // Test that if-condition we pop out is not an edge or so.
    // We just don't support that (see b314.vhd)
    if (condition && (condition->IsEdge() || condition->IsEvent()) && VhdlNode::IsRtlElab()) {
        from->Error("edge or event as condition for a return, exit or next statement is not supported") ;
        return ; // forget it.
    }

    Net *condition_net = (condition) ? condition->GetBit() : 0 ;
    // If condition_net is 0, this means the goto-area always applies (unconditionally)

    // If 'goto'condition is constant '1' (or not there), this dataflow will become 'dead_code'
    if (!condition_net || condition_net==VhdlNode::Pwr()) {
        _is_dead_code = 1 ;
        // Don't forget to bring this back to life when we 'resolve' this label.
    }

    // If 'goto'condition is constant '0' this goto is never doing anything
    if (condition_net && condition_net==VhdlNode::Gnd()) {
        return ;
    }

    // Create a new goto area in this area
    VhdlGotoArea *area = new VhdlGotoArea(goto_label, condition_net) ;

    MapIter mi ;
    VhdlIdDef *id ;
    VhdlValue *val ;
    if (condition) {
        // Present values survive if there is a condition
        // Copy all existing 'id-value' pairs in goto-area
        FOREACH_MAP_ITEM(this, mi, &id, &val) {
            (void) area->Insert(id, val ? val->Copy() : 0) ;
        }
    } else {
        // Present values become don't care (we will hit dead code after this)
        // Move all existing 'id-value' into goto area
        FOREACH_MAP_ITEM(this, mi, &id, &val) {
            (void) area->Insert(id, val) ;
        }
        // And now clear me (values are moved)
        Reset() ;
    }

    // Add the goto area to me
    AddGotoArea(area, from) ;
}

void
VhdlDataFlow::Resolve(const VhdlIdDef *goto_label, const VhdlTreeNode *from)
{
    // Resolve all goto's to this label
    if (!_goto_areas || _goto_areas->Size()==0) return ; // Nothing to do ; no return statement (can be in a procedure)

    // Find the area to resolve
    VhdlGotoArea *area = (VhdlGotoArea*)_goto_areas->GetValue(goto_label) ;
    if (!area) return ; // There was no goto-this-label anywhere..

    // Resolve : Move values back into regular dataflow :
    //  if (area->condition)
    //       present_value = area->value
    //  else
    //       present_value = present_value
    //  endif
    MapIter mi ;
    VhdlIdDef *id ;
    VhdlValue *goto_val, *present_val, *new_val ;
    Net *goto_condition_net = area->Condition() ;
    // Create a temporary condition value, but only if there is a condition net
    // (That means, only if the goto-area executes under some condition).
    VhdlNonconstBit *goto_condition = (goto_condition_net) ? new VhdlNonconstBit(goto_condition_net) : 0 ;

    // First :
    // If there was an assignment to some id AFTER the last 'goto-label' happened,
    // then there will be a value in the present dataflow, and there will NOT be
    // a value in the 'goto'. But in fact the value in the 'goto' should have been
    // stored. It was the value that that id had before it entered this dataflow..
    FOREACH_MAP_ITEM(this, mi, &id, &present_val) {
        if (area->GetItem(id)) continue ; // id already has a value in the goto area.

        // Here, there was no 'goto' value for this id. But there is a present value.
        // So 'goto' value SHOULD have been the value it had when it entered this dataflow.
        goto_val = (_owner) ? _owner->IdValue(id) : id->Value() ;
        if (!goto_val) continue ; // Something wrong.

        // Stuff a copy of this value into the goto 'area',
        // so that it looks like 'id' already had this value at time the 'goto' happened.
        (void) area->Insert(id, goto_val->Copy()) ;
    }

    // Now, scan all id's with a 'goto' value, with or without a present value.
    FOREACH_MAP_ITEM(area, mi, &id, &goto_val) {
        // Get the value of 'id' in the current dataflow
        // This is the value of 'id' if the goto condition does not occur.
        present_val = IdValue(id) ;

        // Now mux-in the goto_value :
        if (goto_condition_net && present_val && !IsDeadCode()) {
            // SelectOn1 absorbs both values. Copy present_val, since that is a pointer to a stored value.
            // There is a value both under 'goto' condition, and under NOT('goto' condition).
            new_val = present_val->Copy()->SelectOn1(goto_condition, goto_val, id->Value(), from) ;
        } else {
            // Could happen if there was 'dead_code' (at end of a function)
            // or if the goto-area is always executed (should also only happen in dead code)
            new_val = goto_val ;
        }
        // Set the current value of id in 'this' dataflow to new_val.
        SetAssignValue(id,new_val) ;
    }
    // All 'goto_val' values are absorbed, so clear the table.
    area->Reset() ;

    // Cleanup.
    delete goto_condition ;

    // Now make sure that we bring the dataflow alive again.
    // Since we resolved a 'goto' here, flow continues from here. !
    _is_dead_code = 0 ;

    // Delete the resolved area
    (void) _goto_areas->Remove(goto_label) ;
    delete area ;

    if (_goto_areas->Size()==0) {
        delete _goto_areas ;
        _goto_areas = 0 ;
    }
}

void
VhdlDataFlow::PopOutOfIf(const Map *condition_to_dataflow, const VhdlDataFlow *else_flow, const VhdlTreeNode *from)
{
    // condition_to_dataflow contains condition -> Dataflow association.
    // condition is a one-bit expression (VhdlNonconstBit) that is true/false for the
    // associated dataflow to execute.
    // condition_to_dataflow is ordered MSB->LSB (first one has highest priority).
    // Now resolve this stuff using priority-decoding with 2-1 muxes.
    // 'this' is the dataflow into which the values get resolved.

    // Assume that if 'else_flow' is 0, it does not ever occur ('if' condition is constant 'true')...

    // Find the goto-labels under the dataflows
    // and the assigned id's
    Set labels ;
    Set ids ;

    VhdlNonconstBit *select ;
    VhdlDataFlow *dataflow ;
    MapIter mi ;
    VhdlIdDef *label ;

    unsigned is_alive = 0 ;
// RD: waiting analysis now done in analyser
//    unsigned is_not_waiting = !else_flow && (!condition_to_dataflow || (condition_to_dataflow->Size() == 0)) ;
    FOREACH_MAP_ITEM(condition_to_dataflow, mi, 0, &dataflow) {
        if (!dataflow) continue ;
        if (dataflow!=this) { // Issue 2051 (VHDL equivalent) : do not collect identifier assignments/labels from this default dataflow.
            dataflow->InsertIds(ids) ;
            dataflow->InsertLabels(labels) ;
        }
        if (!dataflow->IsDeadCode()) is_alive = 1 ;
//        if (!dataflow->IsWaiting()) is_not_waiting = 1 ;
    }
    if (else_flow) {
        if (else_flow!=this) { // Issue 2051 (VHDL equivalent) : do not collect identifier assignments/labels from this default dataflow.
            else_flow->InsertIds(ids) ;
            else_flow->InsertLabels(labels) ;
        }
        if (!else_flow->IsDeadCode()) is_alive = 1 ;
//        if (!else_flow->IsWaiting()) is_not_waiting = 1 ;
    }

#ifdef VHDL_BUILD_PRIO_SELECTORS
    // Declare/Build array of selector nets, MSB->LSB
    VERIFIC_ASSERT(condition_to_dataflow) ;

    // Issue 3317 : only build a prio selector if there is more that one condition involved.
    // Otherwise, use a regular selector, so we avoid problems with clock-enable conditions.
    unsigned use_prio_selector = (condition_to_dataflow->Size()>1) ? 1 : 0 ;
    // Make sure that if one of the conditions is a clock, that we do NOT use a prio selector.
    Array selector_nets(condition_to_dataflow->Size()) ;
    FOREACH_MAP_ITEM(condition_to_dataflow, mi, &select, 0) {
        if (!use_prio_selector) break ; // no use to continue
        if (!select) { // Vipr #6392: Null check
            use_prio_selector = 0 ;
            continue ;
        }

        // If there is a CLOCK signal in the if-then-else control.
        // switch to regular 2-1 resolving, so we don't create
        // ugly PrioDecoders that don't handle clock conditions...
        if (select->IsEdge()) use_prio_selector = 0 ;
        selector_nets.InsertLast(select->GetBit()) ;
    }
    // Declare array of values...
    Array values(condition_to_dataflow->Size()) ;
    // FIX ME : We could also use prio-decoders for label-area resolving...
#endif

    // Resolve goto-labels
    Net *zero = VhdlNode::Gnd() ; // Temporary 'zero' value.
    Net *one = VhdlNode::Pwr() ; // Temporary 'one' value.

    SetIter si, sii ;
    Net *cond, *new_cond ;
    VhdlValue *val, *new_val ;
    VhdlGotoArea *area, *new_goto_area ;
    VhdlDataFlow *sub_df ;
    VhdlIdDef *id ;
    FOREACH_SET_ITEM(&labels, si, &label) {
        // Create the new 'goto' condition when popping out of this area

        // Start with else flow
        area = (else_flow) ? else_flow->GetGotoArea(label) : 0 ;
        cond = (area) ? area->Condition() : 0 ;

        // if there is no else_flow, the area is never executed, so condition is don't care ?? CHECK ME.
        // if there is no (goto)area, the condition is 0 (there is no return there).
        // if the area cond is still 0, then the condition is 1 (there was always a return there).
        // otherwise, use the area's condition.
        new_cond = (!area) ? zero : (!cond) ? one : cond ;
        if (!new_cond) continue ; // something wrong.

        // Start with the else flow condition. If it is 0 now, there was always a return.
        // new_cond = (cond) ? cond->Copy() : 0 ;

        // Now do the if-elsif areas in Low->High priority order
        FOREACH_MAP_ITEM_BACK(condition_to_dataflow, mi, &select, &sub_df) {
            // if there is no data flow, the area is never executed, so condition is don't care ?? CHECK ME.
            if (!select || !sub_df) continue ;

            // Test that if-condition we pop out is not an edge or so.
            // We just don't support that (see b314.vhd)
            if ((select->IsEdge() || select->IsEvent()) && VhdlNode::IsRtlElab()) {
                from->Error("edge or event as condition for a return, exit or next statement is not supported") ;
                break ; // forget it.
            }

            // if 'select' is true, use the dataflows' condition, else use the lower prio (else) condition
            area = sub_df->GetGotoArea(label) ;
            cond = (area) ? area->Condition() : 0 ;

            // if there is no (goto)area, the condition is 0 (there is no return there).
            // if the area cond is still 0, then the condition is 1 (there was always a return there).
            // otherwise, use the area's condition.
            cond = (!area) ? zero : (!cond) ? one : cond ;
            if (!cond) continue ; // something wrong.

            // Now just run these guys through a mux to get the next condition :
            new_cond = from->Mux(new_cond, cond, select->GetBit()) ; //   new_cond->SelectOn1(select, cond, 0, /*from*/from) ;
        }

        // Now set this new condition as the newest 'goto' condition
        new_goto_area = new VhdlGotoArea(label, new_cond) ;

        // Then, update the (goto) values for all ids
        // Now do the new goto values :
        FOREACH_SET_ITEM(&ids, sii, &id) {
            // Start with value in 'else' flow
            area = (else_flow) ? else_flow->GetGotoArea(label) : 0 ;
            val = (area) ? (VhdlValue*)area->GetValue(id) : 0 ;

            // If the value is 0, check if there is a goto-area and a dataflow.
            // If there is one, get the present value from the dataflow.
            // it must have been that the id was not assigned under this goto area.
            // so, take value it had when it entered the dataflow.
            // VIPER #4352 : When 'id' is not assigned in goto-area of else_flow,
            // try to get the value from upper flow
            if (!val) val = (area) ? IdValue(id) : 0 ;
            // if val is still 0, it means that value is don't-care for this dataflow
            // (like if there is no goto-area in this dataflow).

            new_val = (val) ? val->Copy() : 0 ;
            // Now do the if-elsif areas in Low->High priority order
            FOREACH_MAP_ITEM_BACK(condition_to_dataflow, mi, &select, &sub_df) {
                // if 'select' is true, use the dataflows' condition, else use the lower prio one
                area = sub_df->GetGotoArea(label) ;
                val = (area) ? (VhdlValue*)area->GetValue(id) : 0 ;
                // If the value is 0, check if there is a goto-area and a dataflow.
                // If there is one, get the present value from the dataflow.
                // it must have been that the id was not assigned under this goto area.
                // so, take value it had when it entered the dataflow.
                // VIPER #4352 : When 'id' is not assigned in goto-area of sub_df,
                // try to get the value from upper flow
                if (!val) val = (area) ? IdValue(id) : 0 ;

                if (!val) {
                    // 'val' is 0. Remain unchanged.
                } else if (!new_val) {
                    new_val = val->Copy() ;
                } else {
                    // Both val and new_val are set. MUX them :
                    new_val = new_val->SelectOn1(select, val->Copy(), /*init_val*/id->Value(), /*from*/from) ;
                }
            }

            // Now set 'new_val' into the new goto area
            (void) new_goto_area->Insert(id, new_val) ;
        }

        // Now stick goto area in existing area
        AddGotoArea(new_goto_area, from) ;
    }

//    if (!is_not_waiting) {
//        // Every branch is waiting. So this is waiting.
//        _is_waiting = 1 ;
//    }
    if (!is_alive) {
        _is_dead_code = 1 ;
        // We are in dead code. Don't do anything with regular id's.
        return ;
    }

    // Do not build a mux for any Id which matches ALL of these criterion:
    // 1. Is "assigned before use"
    // 2. Has NOT already been assigned in this, or some previous, dataflow
    // 3. Is not assigned in at least one arm of the IF statement
    // Such an ID will clearly NOT be used after this point in the flow.
    // NOTE: this step must be performed after GOTO resolution
    FOREACH_SET_ITEM(&ids, si, &id) {
        if( !id ) continue;

        if (!id->IsAssignedBeforeUsed()) continue ; // cannot discard this guy

        if (HasBeenAssigned( id )) {
            // This id has already been assigned before. So it could be used from now on (after the 'if' is resolved).
            continue ;
        }

        // Check if there is a sub-dataflow which does not assign to this guy.
        if (else_flow && !else_flow->GetItem(id) && !else_flow->IsDeadCode()) {
            (void) ids.Remove(id) ; // Don't create logic for this one.
            continue ;
        }

        FOREACH_MAP_ITEM(condition_to_dataflow, mi, 0, &dataflow) {
            if (dataflow->GetItem(id)) continue ; // it's assigned here
            if (dataflow->IsDeadCode()) continue ; // We don't know if it was assigned here..
            (void) ids.Remove(id) ; // Don't create logic for this one.
            break ;
        }
    }

    // Now resolve the regular ids
    VhdlValue *idval, *sub_val ;
    VhdlValue *else_value = 0 ;
    FOREACH_SET_ITEM(&ids, sii, &id) {
        // Start with a 'dont_care' value
        idval = 0 ;

        // Start with 'else' value :
        if (else_flow && !else_flow->IsDeadCode()) {
            idval = else_flow->IdValue(id) ;
            else_value = idval ;
            // idval can be 0 if id is a function return id, and else part is not dead code.
            // In that case, value is still 0.
            idval = (idval) ? idval->Copy() : 0 ;
        }

#ifdef VHDL_BUILD_PRIO_SELECTORS
        if (use_prio_selector) {
            // Build array of values, MSB->LSB
            // and create a prio-selector
            // Run MSB->LSB (build values priority-cautious).
            values.Reset() ;
            FOREACH_MAP_ITEM(condition_to_dataflow, mi, &select, &sub_df) {
                // Pick-up a pointer to value in the dataflow, assigned or not
                // If in dead code, value is don't care
                sub_val = (sub_df->IsDeadCode()) ? 0 : sub_df->IdValue(id) ;
                // Stack the value into the array, for use in prio decoder
                values.InsertLast(sub_val) ;
            }

            VhdlValue *tmp_value = 0 ;
            unsigned build_prio_selector = 1 ;

            if (build_prio_selector) {
            // Now create a priority-selector with this
                if (!tmp_value) tmp_value = VhdlValue::PrioSelector(idval, &selector_nets, &values, from) ;
                delete idval ; // It was the previously allocated 'else' value
                if (!tmp_value) continue ; // Could happen with all areas areas dead-code

                // Now assign idval back to the present dataflow
                SetAssignValue(id, tmp_value) ;
                continue ; // We are done
            }
        }
        // else : Run through the normal 2-1 mux code, below. clock conditions
        // are not handled correctly by PrioSelector..
#endif
        // Build a chain of 2-1 muxes
        // Run LSB->MSB (build value from back to front, priority-decoded.
        // This is because we create 2-1 muxes from back to front.
        FOREACH_MAP_ITEM_BACK(condition_to_dataflow, mi, &select, &sub_df) {
            // In dead code, value is don't care. Skip it.
            if (sub_df->IsDeadCode()) continue ;

            // Pick-up a pointer to value in the dataflow, assigned or not
            sub_val = sub_df->IdValue(id) ;
            if (!sub_val) continue ; // Something fishy. Ignore this one.

            // Exception case : VIPER issue 1123 and 1473 :
            // If the id is assigned before used, and the condition is a clock-edge,
            // then there is no flip-flop needed.
            // Avoid creating one, because otherwise legal use of the variable (in issue 1123 and 1473) would error out.
            // Restrict (for safety reasons) to variables only :
            if (id->IsAssignedBeforeUsed() && select && select->IsEdge() && id->IsVariable()) {
                // Don't resolve the assignment under the clock-edge :
                continue ;
            }

            VhdlNonconstBit *select_val = select ; // Viper #4958
            // If id is a 'condition' identifier (condition under which a dataflow executes),
            // then we want to ignore clock edges (do not create a flip-flop)
            if (select && select->IsEdge() && IsConditionId(id)) {
                // Viper #7267: ignore for default value
                if (else_value == sub_val) continue ;

                // Set the 'idval' to the value under the clocked area. Ignore 'else' part :
                if (select->GetEnableNet()) { // Viper #4958
                    select_val = new VhdlNonconstBit(select->GetEnableNet()) ;
                } else {
                    delete idval ;
                    idval = sub_val->Copy() ;
                    continue ;
                }
            }

            // If select is an edge, we need to check that the 'else' part 'holds' the initial value.
            // Otherwise this is not a synthesizable if statement.
            // But, we cannot do that here at identifier level, since otherwise this legal process would error out :
            //    process (clk, a2) begin
            //        tmp(2) <= a2;
            //        IF clk'EVENT AND clk = '1' THEN
            //            tmp(1) <= a1;
            //        END IF;
            //    end process
            // So we cannot do this:
            //
            // if (idval && !idval->IsEqual(id->Value())) {
            //    from->Error("%s is not synthesizable since it does not hold its value under NOT(clock-edge) condition", id->Name()) ;
            // }
            //
            // so, we need to rely on the bit-by-bit analysis done inside 'SelectOn1'.

            if (idval) {
                // now mux it with the value in the area
                // Selecton1 absorbs idval and sub_val->Copy(), but does not affect 'select'
                idval = idval->SelectOn1(select_val, sub_val->Copy(), id->Value(), /*from*/from) ;
            } else {
                idval = sub_val->Copy() ; // Can happen if 'else' part is 'dead-code'
            }

            if (select_val != select) delete select_val ; // Viper #4958
        }
        if (!idval) continue ; // Could happen with all areas areas dead-code
        // Now assign idval back to the present dataflow
        SetAssignValue(id, idval) ;
    }
}

void
VhdlDataFlow::PopOutOfCaseWithEquals(const VhdlValue *case_expr, Map *choice_to_dataflow, const VhdlDataFlow *others, const VhdlTreeNode *from, unsigned matching_case /*=0*/)
{
    VERIFIC_ASSERT(choice_to_dataflow) ;
    // VIper 7157: matching_case is for matching case statements in Vhdl2008.

    // choice_to_dataflow contains condition -> Dataflow association.
    // condition is a single-bit expression that indicates the condition
    // to select the dataflow in the value part of the Map.
    // All entries are 'parallel' (no priority ; one-hot).
    // The same Dataflow could be present under multiple conditions.
    // Now resolve this stuff using decoders.

    // Find the goto-labels under the dataflows
    // and the assigned id's
    Set labels ;
    Set ids ;

    VhdlDataFlow *dataflow ;
    VhdlGotoArea *area ;
    MapIter mi ;
    VhdlIdDef *label ;

    unsigned is_alive = 0 ;
//    unsigned is_not_waiting = !others && (!choice_to_dataflow || (choice_to_dataflow->Size() == 0)) ;
    Array selector_nets(choice_to_dataflow->Size()+1) ;
    VhdlNonconstBit *b ;
    Net *net ;
    Net *others_net = 0 ;
    VhdlDiscreteRange *choice ;
    FOREACH_MAP_ITEM(choice_to_dataflow, mi, &choice, &dataflow) {
        VERIFIC_ASSERT(dataflow) ;

        // Build selector nets array, one bit per choice :
        // Choice can be a range or a normal (constant) expression.

        if (choice->IsRange()) {
            // Evaluate as a constraint
            VhdlConstraint *range = choice->EvaluateConstraint(this, 0) ;
            if (!range) continue ;

            if (range->IsNullRange()) {
                // null range warning is already given
                choice->Warning("choice ignored") ;
                // Eliminate the entry from the table, so we don't create value either.
                (void) choice_to_dataflow->Remove(choice) ;
                delete range ;
                continue ; // Never selected
            }

            // A range. Can be implemented in two ways :
            //    - Simply create a value for each range position and treat as normal value
            //    - two < comparators and an AND gate.
            // Area-wise, since we are doing constant compares, a (<) magnitude
            // comparator will be as big as a == comparator.
            // On the other hand, once we use a magnitude comparator, we cannot use
            // a MUX or decoder implementation any more to resolve the values.
            // Can only use comparators with selectors.
            //
            // Lets just always build the magnitude comparators for now
            // That will be clearer to users.
            //

            if (range->High()->HasX() || range->Low()->HasX()) {
                choice->Warning("choice with meta-values (X,Z etc) ignored") ;
                // Eliminate the entry from the table, so we don't create value either.
                (void) choice_to_dataflow->Remove(choice) ;
                delete range ;
                continue ; // never selected
            }

            // result = (expr < low) NOR (high < expr)
            VhdlNonconstBit *c1 = case_expr->Copy()->ToNonconst()->LessThan(range->Low()->Copy()->ToNonconst(), 0, choice) ;
            VhdlNonconstBit *c2 = range->High()->Copy()->ToNonconst()->LessThan(case_expr->Copy()->ToNonconst(), 0, choice) ;
            b = c1->Binary(c2, VHDL_nor, choice) ;

            net = (b) ? b->GetBit() : 0 ;
            delete b ;
            delete range ;

        } else {
            // Simple expression
            // Meta-values and other stuff should have been checked before already.
            VhdlValue *cond = choice->Evaluate(0,this,0) ;
            if (!cond) continue ;

            // Create Equal comparator
            if (matching_case && cond->IsMetalogicalWithDC() && (cond->IsComposite() || cond->IsEnumValue())) {
                if (cond->IsComposite()) {
                    Array *values = cond->GetValues() ;
                    unsigned size = values ? values->Size() : 0 ;
                    // Viper 7157: Need to call ToComposite to case the VhdlValue to VhdlComposite
                    // this is required because Equal is not a virtual function it is
                    // defined for VhdlNonconst and VhdlComposite
                    b = cond->ToComposite(size ? size : 1)->Equal(case_expr->Copy(), 0, choice) ;
                } else {
                    // Viper 7537 : Need to handle metalogical values of Enum
                    VhdlEnum *enum_val = cond->ToEnum() ;
                    if (enum_val) {
                        b = enum_val->Equal(case_expr->Copy(), 0, choice) ;
                    } else {
                        b = cond->ToNonconst()->Equal(case_expr->Copy()->ToNonconst(), 0, choice) ;
                    }
                }
            } else {
                b = cond->ToNonconst()->Equal(case_expr->Copy()->ToNonconst(), 0, choice) ;
            }
            net = b->GetBit() ;
            delete b ;
        }
        if (net==VhdlNode::Gnd()) {
            // choice never selected
            // Eliminate the entry from the table, so we don't create value either.
            (void) choice_to_dataflow->Remove(choice) ;
            continue ; // never selected
        }
        if (net==VhdlNode::Pwr()) {
            // Condition always applies, So Others (if there) does not apply
            others = 0 ;
        }
        selector_nets.InsertLast(net) ;

        // Check flags, and collect labels and identifiers assigned here.
        if (dataflow!=this) { // Issue 2051 (VHDL equivalent) : do not collect identifier assignments/labels from this default dataflow.
            dataflow->InsertIds(ids) ;
            dataflow->InsertLabels(labels) ;
        }
        if (!dataflow->IsDeadCode()) is_alive = 1 ;
//        if (!dataflow->IsWaiting()) is_not_waiting = 1 ;
    }

    if (others) {
        // Need 'others' condition : Not any of the previous
        // Else decoder is NOT-any-of-these : ReduceNor
        if (selector_nets.Size()) {
            VhdlNonconst *s = new VhdlNonconst(new Array(selector_nets)) ;
            b = s->Reduce(VHDL_REDNOR,/*from*/from) ;
            others_net = b->GetBit() ;
            delete b ;
        }
        // If no selector nets are there, 'others' is ALWAYS executed.
        // Happens for constant expressions.
        if (!others_net) others_net = VhdlNode::Pwr() ;

        // Set this as the last bit in the selector nets
        selector_nets.InsertLast(others_net) ;

        // Check flags, and collect labels and identifiers assigned here.
        if (others!=this) { // Issue 2051 (VHDL equivalent) : do not collect identifier assignments/labels from this default dataflow.
            others->InsertIds(ids) ;
            others->InsertLabels(labels) ;
        }
        if (!others->IsDeadCode()) is_alive = 1 ;
//        if (!others->IsWaiting()) is_not_waiting = 1 ;
    }

    SetIter si, sii ;
    Array values(choice_to_dataflow->Size()+1) ;
    VhdlValue *val, *new_val ;
    Net *cond ;
    VhdlValue *cond_value ;
    VhdlIdDef *id ;

    Net *zero = VhdlNode::Gnd() ; // Temporary 'zero' value.
    Net *one = VhdlNode::Pwr() ; // Temporary 'one' value.

    // Resolve goto-areas
    FOREACH_SET_ITEM(&labels, si, &label) {
        // Now set-up the new goto-condition for this label.
        // Its a Mux of the goto-conditions of the mutal exclusive dataflows
        //
        // Run over positions n-1 ... 0, and accumulate the conditions
        values.Reset() ;
        FOREACH_MAP_ITEM(choice_to_dataflow, mi, 0, &dataflow) {
            VERIFIC_ASSERT(dataflow) ;

            // Get the goto-area under the label
            area = (dataflow) ? dataflow->GetGotoArea(label) : 0 ;
            // Get the condition in this area
            cond = (area) ? area->Condition() : 0 ;
            // if there is no data flow, the condition is don't-care.
            // if there is no area, there is no return in this dataflow : condition='0'  cond = &zero
            // if there is no conditin yet, there is always a return : condition='1' cond = &one
            // Otherwise we just take the condition
            cond = (!dataflow) ? 0 : (!area) ? zero : (!cond) ? one : cond ;
            // Store this condition as a NonConstBit
            cond_value = (cond) ? new VhdlNonconstBit(cond) : 0 ;
            values.InsertLast(cond_value) ;
        }
        // Insert 'others' value (condition) if there
        if (others) {
            area = others->GetGotoArea(label) ;
            cond = (area) ? area->Condition() : 0 ;
            // if there is no data flow, the condition is don't-care.
            // if there is no area, there is no return in this dataflow : condition='0'  cond = &zero
            // if there is no conditin yet, there is always a return : condition='1' cond = &one
            // Otherwise we just take the condition
            cond = (!area) ? zero : (!cond) ? one : cond ;
            // Store this condition as a NonConstBit
            cond_value = (cond) ? new VhdlNonconstBit(cond) : 0 ;
            values.InsertLast(cond_value) ;
        }

        // Now resolve a new condition
        cond_value = VhdlValue::Selector(&selector_nets, &values, /*from*/from) ;

        // And set this as the 'goto' condition in the new resulting goto-area
        VhdlGotoArea *new_goto_area = new VhdlGotoArea(label, (cond_value) ? cond_value->GetBit() : 0) ;
        delete cond_value ; cond_value = 0 ; // Memory leak fix

        // Cleanup the values array (CHECK ME : see if there is a way to pass-in the condition bits as Net rather than Values).
        unsigned i ;
        FOREACH_ARRAY_ITEM(&values, i, cond_value) delete cond_value ;

        // Now do the new goto values :
        FOREACH_SET_ITEM(&ids, sii, &id) {
            // Now set-up the new goto-condition for this label
            // Run over positions n-1 ... 0
            values.Reset() ;
            FOREACH_MAP_ITEM(choice_to_dataflow, mi, 0, &dataflow) {
                area = (dataflow) ? dataflow->GetGotoArea(label) : 0 ;
                // Get the value of the id in this goto-area
                val = (area) ? (VhdlValue*)area->GetValue(id) : 0 ;
                // If the value is 0, check if there is a goto-area and a dataflow.
                // If there is one, get the present value from the dataflow.
                // it must have been that the id was not assigned under this goto area.
                // so, take value it had when it entered the dataflow.
                // VIPER #4352 : When 'id' is not assigned in goto-area of dataflow,
                // try to get the value from upper flow
                if (!val) val = (area) ? IdValue(id) : 0 ;
                // if val is still 0, it means that value is don't-care for this dataflow
                // Insert in values
                values.InsertLast(val) ;
            }
            // Add 'others' value
            if (others) {
                area = others->GetGotoArea(label) ;
                // Get the value of the id in this goto-area
                val = (area) ? (VhdlValue*)area->GetValue(id) : 0 ;
                // VIPER #4352 : When 'id' is not assigned in goto-area of others,
                // try to get the value from upper flow
                if (!val) val = (area) ? IdValue(id) : 0 ;
                values.InsertLast(val) ;
            }
            new_val = VhdlValue::Selector(&selector_nets, &values, /*from*/from) ;
            // And set this value in the new goto-area
            (void) new_goto_area->Insert(id, new_val) ;
        }

        // Now stick goto area in existing area
        AddGotoArea(new_goto_area, from) ;
    }

//    if (!is_not_waiting) {
//        // Every branch is waiting. So this is waiting.
//        _is_waiting = 1 ;
//    }
    if (!is_alive) {
        _is_dead_code = 1 ;
        // We are in dead code. Don't do anything with regular id's.
        return ;
    }

    // Do not build a mux for any Id which matches ALL of these criterion:
    // 1. Is "assigned before use"
    // 2. Has NOT already been assigned in this, or some previous, dataflow
    // 3. Is not assigned in at least one arm of the CASE statement
    // Such an ID will clearly NOT be used after this point in the flow!
    // NOTE: this step must be performed after GOTO resolution
    FOREACH_SET_ITEM(&ids, si, &id) {
        if( !id ) continue;
        if (!id->IsAssignedBeforeUsed()) continue ; // cannot discard this guy
        if (HasBeenAssigned( id )) continue ; // This id has already been assigned before. So it could be used from now on.

        // Check if there is a sub-dataflow which does not assign to this guy.
        if (others && !others->GetItem(id) && !others->IsDeadCode()) {
            (void) ids.Remove(id) ; // Don't create logic for this one.
            continue ;
        }

        FOREACH_MAP_ITEM(choice_to_dataflow, mi, &choice, &dataflow) {
            if (dataflow->GetItem(id)) continue ; // it's assigned here
            if (dataflow->IsDeadCode()) continue ; // We don't know if it was assigned here..
            (void) ids.Remove(id) ; // Don't create logic for this one.
            break ;
        }
    }

    // Now resolve the regular ids
    FOREACH_SET_ITEM(&ids, si, &id) {
        // Set up the values
        values.Reset() ;
        // Viper #2101: The case statement should infer a flop when only
        // one of the parallel branches has clocked region and the rest of
        // the branches assign constants. The constant branches should infer
        // asynchronous set reset for the flop.
        unsigned num_clocked = 0 ;
        unsigned rest_constants = 1 ;
        VhdlValue *id_init_value = id->Value() ;
        FOREACH_MAP_ITEM(choice_to_dataflow, mi, 0, &dataflow) {
            val = dataflow->IdValue(id) ;
            // Now, if the regular dataflow is dead, the regular value is don't care
            // (since the 'goto' value will be always be used). So make val==NULL (dont_care).
            if (dataflow->IsDeadCode()) val = 0 ;

            // Insert in values
            values.InsertLast(val) ;
        }
        if (others) {
            // Add others value
            val = others->IdValue(id) ;
            if (others->IsDeadCode()) val = 0 ;
            values.InsertLast(val) ;
        }

        new_val = 0 ;
        (void) num_clocked ;
        (void) rest_constants ;
        (void) id_init_value ;

        if (!new_val) {
            // Create a (onehot) selector with these values :
            new_val = VhdlValue::Selector(&selector_nets, &values, /*from*/from) ;
        }

        // And set it as the new value
        SetAssignValue(id, new_val) ;
    }
}

void
VhdlDataFlow::PopOutOfCaseWithMuxes(const VhdlNonconst *case_expr, const Map *position_to_dataflow, const VhdlDataFlow *others, const VhdlTreeNode *from)
{
    // position_to_dataflow contains (integer)position -> Dataflow association.
    // 'position' is an integer that needs to be compared to 'case_expr'
    // to obtain the condition. Use position to find the position on the mux input.
    // All entries are 'parallel' (no priority).
    // The same Dataflow could be present under multiple conditions.
    // Now resolve this stuff using N->1 Muxes.

    // Find the goto-labels under the dataflows
    // and the assigned id's
    Set labels ;
    Set ids ;

    VhdlDataFlow *dataflow ;
    VhdlGotoArea *area ;
    MapIter mi ;
    VhdlIdDef *label ;

    unsigned i ;
    unsigned is_alive = 0 ;
//    unsigned is_not_waiting =  !others && (!position_to_dataflow || (position_to_dataflow->Size() == 0)) ;
    FOREACH_MAP_ITEM(position_to_dataflow, mi, 0, &dataflow) {
        if (!dataflow) continue ;
        // FIX ME : Speed-up by not re-doing dataflows
        if (dataflow!=this) {  // Issue 2051 (VHDL equivalent) : do not collect identifier assignments/labels from this default dataflow.
            dataflow->InsertIds(ids) ;
            dataflow->InsertLabels(labels) ;
        }
        if (!dataflow->IsDeadCode()) is_alive = 1 ;
//        if (!dataflow->IsWaiting()) is_not_waiting = 1 ;
    }
    if (others) {
        if (others!=this) {  // Issue 2051 (VHDL equivalent) : do not collect identifier assignments/labels from this default dataflow.
            others->InsertIds(ids) ;
            others->InsertLabels(labels) ;
        }
        if (!others->IsDeadCode()) is_alive = 1 ;
//        if (!others->IsWaiting()) is_not_waiting = 1 ;
    }

    // calculate the 'span' (number of input values into this mux)
    // by taking 2**num_bits
    unsigned span = 1 ;
    i = case_expr->Size() ;
    while (i-- != 0) span *= 2 ; // power of two

    // Now resolve per label
    SetIter si, sii ;
    Net *cond, *new_cond ;
    VhdlValue *val, *new_val ;
    VhdlGotoArea *new_goto_area ;
    VhdlIdDef *id ;

    Array values(span) ; // Allocate an array that can hold values
    Array in_nets(span) ; // Allocate an array that can hold nets
    Net *zero = VhdlNode::Gnd() ; // Temporary 'zero' value.
    Net *one = VhdlNode::Pwr() ; // Temporary 'one' value.

    // Set up array of condition nets (the ones from the case expression) :
    Array conditions(case_expr->Size()) ;

    FOREACH_SET_ITEM(&labels, si, &label) {
        // Now set-up the new goto-condition for this label.
        // Its a Mux of the goto-conditions of the mutal exclusive dataflows
        // We will work with array's of nets here, so BuildNto1Mux can be called (without the need for VhdlValue's).
        in_nets.Reset() ;
        // Fill the 'conditions' array again, since they will get messed up in BuildNto1Mux :
        conditions.Reset() ;
        conditions.Append(case_expr->Nets()) ;

        // Fill in_nets with the goto-label conditions.
        area = (others) ? others->GetGotoArea(label) : 0 ;
        cond = (area) ? area->Condition() : 0 ;
        // if there is no data flow, the condition is don't-care.
        // if there is no area, there is no return in this dataflow : condition='0'  cond = &zero
        // if there is no conditin yet, there is always a return : condition='1' cond = &one
        // otherwise we just take the condition
        cond = (!others) ? 0 : (!area) ? zero : (!cond) ? one : cond ;

        // Fill the array with this default value :
        for (i=0; i<span; i++) {
            in_nets.InsertLast(cond) ;
        }
        long pos ;
        // Now run over all positions, and fill (overwrite default) :
        FOREACH_MAP_ITEM(position_to_dataflow, mi, &pos, &dataflow) {
            if (!dataflow) continue ; // ?
            area = dataflow->GetGotoArea(label) ;
            cond = (area) ? area->Condition() : 0 ;
            // if there is no data flow, the condition is don't-care.
            // if there is no area, there is no return in this dataflow : condition='0'  cond = &zero
            // if there is no conditin yet, there is always a return : condition='1' cond = &one
            // otherwise we just take the condition
            cond = (!area) ? zero : (!cond) ? one : cond ;

            // Convert negative (2-complement) values to their unsigned position
            if (pos < 0) pos = pos + (long)span ;
            // If position cannot be reached by the expression (out of span)
            // then it can never happen (values will be skipped too).
            if (pos>=(long)span) continue ;
            // Overwrite the value (MSB first)
            in_nets.Insert((span - (unsigned)pos) - 1, cond) ;
        }

        // Now resolve a new condition net.
        new_cond = VhdlValue::BuildNto1Mux(conditions, in_nets, from) ; // used to be : new_cond = VhdlValue::Nto1Mux(case_expr, &values, /*from*/from)->ToNonconstBit() ;

        // And set this as the 'goto' condition in the new resulting goto-area
        new_goto_area = new VhdlGotoArea(label, new_cond) ;

        // Now do the new goto values :
        FOREACH_SET_ITEM(&ids, sii, &id) {
            // Now set-up the new id values for this label

            values.Reset() ;
            // Fill values with the default
            // CHECK ME : can we simplify this ?
            area = (others) ? others->GetGotoArea(label) : 0 ;
            val = (area) ? (VhdlValue*)area->GetValue(id) : 0 ;
            // VIPER #4352 : When 'id' is not assigned in goto-area of others,
            // try to get the value from upper flow
            if (!val && area) val = IdValue(id) ;
            for (i=0; i<span; i++) {
                values.InsertLast(val) ;
            }
            // Now run over all positions, and fill (overwrite default) :
            FOREACH_MAP_ITEM(position_to_dataflow, mi, &pos, &dataflow) {
                if (!dataflow) continue ; // ?
                area = dataflow->GetGotoArea(label) ;
                val = (area) ? (VhdlValue*)area->GetValue(id) : 0 ;
                // VIPER #4352 : When 'id' is not assigned in goto-area of dataflow,
                // try to get the value from upper flow
                if (!val && area) val = IdValue(id) ;
                // If position cannot be reached by the expression (out of span)
                // then assume the value is don't care (0).
                if (pos >= (long)span) continue ;
                // Convert negative (2-complement) values to their unsigned position
                if (pos < 0) pos = pos + (long)span ;
                // Overwrite the value (MSB first)
                values.Insert((unsigned)((long)span - pos) - 1, val) ;
            }

            // Now resolve a new value
            new_val = VhdlValue::Nto1Mux(case_expr, &values, /*from*/from) ;
            // And set this value in the new goto-area
            (void) new_goto_area->Insert(id, new_val) ;
        }

        // Now stick goto area in existing area
        AddGotoArea(new_goto_area, from) ;
    }

//    if (!is_not_waiting) {
//        // Every branch has a wait-statement.
//        // So 'this' dataflow will always have a waitstatement
//        _is_waiting = 1 ;
//    }
    if (!is_alive) {
        _is_dead_code = 1 ;
        // We are in dead code. Don't do anything with regular id's.
        return ;
    }

    // Do not build a mux for any Id which matches ALL of these criterion:
    // 1. Is "assigned before use"
    // 2. Has NOT already been assigned in this, or some previous, dataflow
    // 3. Is not assigned in at least one arm of the CASE statement
    // Such an ID will clearly NOT be used after this point in the flow!
    // NOTE: this step must be performed after GOTO resolution
    FOREACH_SET_ITEM(&ids, si, &id) {
        if( !id ) continue;
        if (!id->IsAssignedBeforeUsed()) continue ; // cannot discard this guy
        if (HasBeenAssigned( id )) continue ; // This id has already been assigned before. So it could be used from now on.

        // Check if there is a sub-dataflow which does not assign to this guy.
        if (others && !others->GetItem(id) && !others->IsDeadCode()) {
            (void) ids.Remove(id) ; // Don't create logic for this one.
            continue ;
        }

        FOREACH_MAP_ITEM(position_to_dataflow, mi, 0, &dataflow) {
            if (dataflow->GetItem(id)) continue ; // it's assigned here
            if (dataflow->IsDeadCode()) continue ; // We don't know if it was assigned here..
            (void) ids.Remove(id) ; // Don't create logic for this one.
            break ;
        }
    }

    // Now resolve the regular ids
    FOREACH_SET_ITEM(&ids, sii, &id) {
        values.Reset() ;
        // Fill values with the default
        // Now, if the regular dataflow is dead, the regular value is don't care
        // (since the 'goto' value will be always be used). So make val==NULL (dont_care).
        val = (others && !others->IsDeadCode()) ? others->IdValue(id) : 0 ;
        for (i=0; i<span; i++) {
            values.InsertLast(val) ;
        }
        long pos ;
        // Viper #2101: The case statement should infer a flop when only
        // one of the parallel branches has clocked region and the rest of
        // the branches assign constants. The constant branches should infer
        // asynchronous set reset for the flop.
        unsigned num_clocked = 0 ;
        unsigned rest_constants = 1 ;
        VhdlValue *id_init_value = id->Value() ;
        // Now run over all positions, and fill (overwrite default) :
        FOREACH_MAP_ITEM(position_to_dataflow, mi, &pos, &dataflow) {
            if (!dataflow) continue ; // ?
            val = (dataflow && !dataflow->IsDeadCode()) ? dataflow->IdValue(id) : 0 ;
            // Convert negative (2-complement) values to their unsigned position
            if (pos < 0) pos = pos + (long)span ;
            // If position cannot be reached by the expression (out of span)
            // then assume the value is don't care (0) :
            if (pos >= (long)span) continue ;
            // Overwrite the value (MSB first)
            values.Insert((unsigned)((long)span - pos) - 1, val) ;
        }

        new_val = 0 ;
        (void) num_clocked ;
        (void) rest_constants ;
        (void) id_init_value ;

        if (!new_val) {
            // Now resolve a new value
            new_val = VhdlValue::Nto1Mux(case_expr, &values, /*from*/from) ;
        }

        // And set it as the new value
        SetAssignValue(id, new_val) ;
    }
}

/*************************************************************************************/

// Condition identifiers
// This is only applicable for RTL elaboration, with assertion synthesis.
// The condition identifiers are used to create a 'dataflow' condition under which an assertion is tested.

// Generate a 'condition' net, which represents the condition under which 'this' dataflow executes.
Net *
VhdlDataFlow::GenerateCondition(const VhdlTreeNode *from)
{
    if (!from) return 0 ;
    // Create an identifier which will carry the condition net.
    // Install the identifier in the top of the dataflow, so that it can be cleaned up at some point.
    // (and we can test if an encountered id is a condition identifier.
    //
    // Then set the value of 'id' to '1' in the current dataflow.
    // Then set the value of 'id' to '0' in the top dataflow.
    //
    // Let the dataflow resolver create the condition logic for assert_net to drive assert_id
    // which will eventually do a net-assignment to drive the result condition net.

    VhdlIdDef *id = new VhdlVariableId(0) ; // don't give it a name.
    // Note : its only a condition, and linefile info blocks some optimizations. Maybe we should NOT set linefile info
    // VIPER #4002: Do not add linefile info to this variable:
    //id->SetLinefile(from->Linefile()) ; // give it the right linefile info.

    // Flag as assigned and used (in case somebody asks)
    id->SetConcurrentAssigned() ;
    id->SetConcurrentUsed() ;

    // To let the id work properly during elaboration :
    // give it a type
    VhdlIdDef *type = VhdlNode::BooleanType() ; //StdType("boolean") ;
    id->DeclareVariable(type, 1, 1, 1) ;

    // Set constraint on this identifier (false to true) from its type.
    VhdlConstraint *constraint = (type && type->Constraint()) ? type->Constraint()->Copy() : 0 ;
    // Initialize the identifier with a 1-bit net (let it have a madeup name)
    VhdlValue *init_val = (constraint) ? constraint->CreateValue(0,0,id,0) : 0 ;
    delete constraint ; // No longer need this.
    id->SetValue(init_val) ;

    // Find the top of the data flow :
    VhdlDataFlow *top_df = this ;
    while (top_df->Owner()) {
        top_df = top_df->Owner() ;
    }

    // In this top of the flow, insert the id, so it gets cleaned up at some point.
    if (!top_df->_conditionids) top_df->_conditionids = new Set(POINTER_HASH) ;
    (void) top_df->_conditionids->Insert(id) ;

    // Insert value '0' into the top, for this id
    top_df->SetAssignValue(id, new VhdlNonconstBit(VhdlNode::Gnd())) ;

    // Insert value '1' into 'this' dataflow :
    SetAssignValue(id, new VhdlNonconstBit(VhdlNode::Pwr())) ;

    // Viper #7587: Additionally add the value to all the goto_areas
    // of the top_df. Since this identifier is being added on-demand
    // during elaboration, this additional work is needed
    Map *goto_areas = top_df->GotoAreas() ;
    MapIter mi ;
    VhdlGotoArea *goto_area ;
    FOREACH_MAP_ITEM(goto_areas, mi, 0, &goto_area) {
        if (!goto_area) continue ;
        (void) goto_area->Insert(id, new VhdlNonconstBit(VhdlNode::Gnd())) ;
    }

    // return the id's net :
    return (init_val) ? init_val->GetBit(): 0 ; // VIPER 2394 : Check 'init_val' for null value
}

unsigned
VhdlDataFlow::IsConditionId(const VhdlIdDef *id) const
{
    if (!id) return 0 ;
    // Go to the top of dataflow. That's where the condition ids are inserted.
    if (_owner) return _owner->IsConditionId(id) ;
    // Check local here :
    if (!_conditionids) return 0 ;
    if (_conditionids->GetItem(id)) return 1 ;
    return 0 ;
}

#ifdef VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
void
VhdlDataFlow::SetCallerTreeNode(VhdlTreeNode *caller_node)
{
    _from_treenode = caller_node ;
}
#endif // VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS

VhdlTreeNode *
VhdlDataFlow::GetCallerTreeNode(void) const
{
#ifdef VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
    return _from_treenode ;
#endif // VHDL_REPORT_USER_LINEFILE_FOR_PACKAGE_FUNCTIONS
    return 0 ;
}
void
VhdlDataFlow::SetSensitivityList(Set *sens_list)
{
    // Delete old sensitivity list to avoid memory leak. If multiple
    // wait statements present in a process, this API is called multiple times
    delete _sensitivity_list ;
    _sensitivity_list = sens_list ;
}
/*************************************************************************************/

// Goto-areas

VhdlGotoArea::VhdlGotoArea(VhdlIdDef *goto_label, Net *condition) :
    Map(POINTER_HASH),
    _owner(0),
    _goto_label(goto_label),
    _condition(condition)
{
    // condition is 'return' condition : if '1' : always-returning
    // If _condition is 0, we assume that is is always-returning (essentialy '1' without the need for a NonconstVal).
    // if (!_condition) _condition = VhdlNode::Pwr() ; // not needed any more
}

VhdlGotoArea::~VhdlGotoArea()
{
    // Delete any left-over id-value pairs.

    MapIter mi ;
    VhdlIdDef *id ;
    VhdlValue *value ;
    FOREACH_MAP_ITEM(this, mi, &id, &value) {
        delete value ;
    }
}

void
VhdlGotoArea::InsertIds(Set &list) const
{
    MapIter i ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(this, i, &id, 0) (void) list.Insert(id) ;
}

// VIPER #7286 : Invalidate the values of identifier assigned in argument specified
// data flows. We cannot rely on those as we cannot evaluate the condition of
// the conditional statement
void VhdlDataFlow::InvalidateIdValues(const Array *dataflows, const VhdlTreeNode *from)
{
    Set ids ;
    Set labels ;
    unsigned i ;
    VhdlDataFlow *sub_df ;
    SetIter si ;
    VhdlIdDef *id ;
    unsigned is_alive = 0 ;
    FOREACH_ARRAY_ITEM(dataflows, i, sub_df) {
        if (sub_df != this) {
            sub_df->InsertIds(ids) ;
            sub_df->InsertLabels(labels) ;
            sub_df->SetNotInInitial() ; // Mark not initial to not execute VhdlDataFlow::UpdateInitialValue
            if (!sub_df->IsDeadCode()) is_alive = 1 ;
        }
    }
    // Iterate over ids and set null value for each one in upper data flow
    FOREACH_SET_ITEM(&ids, si, &id) {
        if (id) SetAssignValue(id, 0) ;
    }
    SetIter sii ;
    VhdlGotoArea *new_goto_area ;
    VhdlIdDef *label ;
    FOREACH_SET_ITEM(&labels, si, &label) {
        // Create the new 'goto' condition when popping out of this area

        // Now set this new condition as the newest 'goto' condition
        new_goto_area = new VhdlGotoArea(label, 0) ;

        // Then, update the (goto) values for all ids
        // Now do the new goto values :
        FOREACH_SET_ITEM(&ids, sii, &id) {
            // Now set 'new_val' into the new goto area
            (void) new_goto_area->Insert(id, 0) ;
        }

        // Now stick goto area in existing area
        AddGotoArea(new_goto_area, from) ;
    }
    if (dataflows && !is_alive) _is_dead_code = 1 ;
}

