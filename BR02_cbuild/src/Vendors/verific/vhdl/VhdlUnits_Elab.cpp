/*
 *
 * [ File Version : 1.257 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlUnits.h" // Compile flags are defined within here

#include <ctype.h>     // for isdigit etc.
#include <string.h>    // for strchr etc.
#include <stdlib.h>    // strtod, strtol

#include "Map.h"
#include "Strings.h"
#include "Array.h"
#include "Set.h"
#include "Message.h"
#include "RuntimeFlags.h"

#include "vhdl_file.h" // for GetWorkLib
#include "vhdl_tokens.h"
#include "VhdlDeclaration.h"
#include "VhdlName.h"
#include "VhdlConfiguration.h"
#include "VhdlScope.h"
#include "VhdlIdDef.h"
#include "VhdlStatement.h"
#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"
#include "VhdlCopy.h" // for package instance elaboration

#include "../verilog/VeriExpression.h"
#include "../verilog/VeriId.h"
#include "../verilog/VeriLibrary.h"
#include "../verilog/VeriModule.h"
#include "../verilog/VeriTreeNode.h"
#include "../verilog/veri_file.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**********************************************************/
//  user-generics handling elaboration
/**********************************************************/

Array *
VhdlPrimaryUnit::CreateActualGenerics(const Map *user_generics, linefile_type lf, unsigned from_verilog /*= 0*/, unsigned ignore_ifnotexist /*=0*/)
{
    if (!_id || !user_generics) return 0 ;

    // 'user_generics' are from 'outside' applications
    // 'user_generics' come in char*name -> char*value associations in a Map.
    // If 'name' is 0, it is an ordered generic association.
    // Otherwise, it is a named association.

    // This routine turns the user generics into an (internally used) tree
    // representation. The elements of the tree are VhdlTreeNode structures,
    // which are allocated. They need to be removed after usage.
    //
    // The 'actual<->formal' association will be type-inferred here.

    // Set scope correct for 'actual' type inference : (set to entity's scope)
    VhdlScope *save_scope = _present_scope ;
    _present_scope = _local_scope ;

    Array *result = new Array(user_generics->Size()) ;

    MapIter mi ;
    const char *name ;
    const char *value ;
    VhdlExpression *actual ;
    VhdlIdDef *formal_id ;
    VhdlExpression *assoc ;
    unsigned pos = 0 ;
    
    FOREACH_MAP_ITEM(user_generics, mi, &name, &value) {
        pos++ ; // pos is one higher than the generic's position
        // Create a value node. Determine first what kind of value it is :
        if (!value) continue ; // 'OPEN' ?

        // Get the formal (id)  by name or position
        if (name) {
            formal_id = _id->GetGeneric(name) ;
            if (!formal_id) {
                // Issue 1878 : Check if maybe it's requested in mixed case
                if (name[0]!='\\') { // It's NOT an escaped identifier
                    char *downcased = Strings::strtolower(Strings::save(name)) ;
                    formal_id = _id->GetGeneric(downcased) ;
                    Strings::free( downcased ) ;
                }
            }
            if (!formal_id) {
                if (ignore_ifnotexist) {
                    // VIPER #7727 : produce warning if generic does not exist
                    _id->Warning("unit %s does not have a generic named %s to override", _id->Name(), name) ;
                } else {
                    _id->Error("unit %s does not have a generic named %s", _id->Name(), name) ;
                }
            }
        } else {
            formal_id = _id->GetGenericAt(pos-1) ;
            if (!formal_id && !ignore_ifnotexist) {
                // VIPER #3401: Produce a better error message and with the appropriate line-file:
                //_id->Error("unit %s does not have %d generics", _id->Name(), pos) ;
                VhdlTreeNode err_node((lf) ? lf : _id->Linefile()) ;
                err_node.Error("number of generic actuals %d does not match with number of formals %d for unit %s",
                                                      user_generics->Size(), _id->NumOfGenerics(), _id->Name()) ;
            }
        }
        if (!formal_id && ignore_ifnotexist) continue ;
        if (!formal_id) break ; // error occurred

        // Create a Expression tree-node from this string (Pass-in line/file info from 'formal_id')
        actual = CreateGeneric(value, formal_id) ;
        if (actual && lf) actual->SetLinefile(lf) ;

        // Modelsim behavior: 1. If actual(verilog) is integer or vector it matches with any type formal
        //                    2. If formal (vhdl) is integer or real it matches with any formal
        // Viper 6649 and 5889
        if (!actual) {
            if (!ignore_ifnotexist) formal_id->Error("unknown actual generic literal value %s for generic %s ignored", value, formal_id->Name()) ;
            continue ;
        }

        unsigned int_or_vect_actual = from_verilog ? actual->IsInteger() || actual->IsStringLiteral() || actual->IsAggregate() : 0 ;
        VhdlIdDef *formal_type = formal_id->Type() ;
        unsigned int_or_real_formal = from_verilog ? formal_type->IsIntegerType() || formal_type->IsNaturalType() ||
                                                     formal_type->IsPositiveType() || formal_type->IsRealType() : 0 ;

        if (!int_or_real_formal && !int_or_vect_actual && !actual->TypeInfer(formal_id->Type(), 0, VHDL_READ,0)) {
            formal_id->Error("unknown actual generic literal value %s for generic %s ignored", value, formal_id->Name()) ;
            delete actual ;
            continue ;
        } else if (int_or_real_formal || int_or_vect_actual) {
            actual->SetId(formal_id->Type()) ;
        }

        // Create the association :
        assoc = new VhdlAssocElement(new VhdlIdRef(formal_id), actual) ;

        // Since TypeInfer errors out for this assoc it needs to be handeled differently
        // while calling checkAgainst during elaboration
        if (from_verilog) assoc->SetFromVerilog() ;

        // Pass-in the line-file info from the formal_id. That is the best we have..
        assoc->SetLinefile(lf ? lf : formal_id->Linefile()) ;

        // Insert in array :
        result->InsertLast(assoc) ;
    }

    // Pop scope back to where it came from :
    _present_scope = save_scope ;

    return result ;
}

VhdlExpression *
VhdlPrimaryUnit::CreateGeneric(const char *string, const VhdlIdDef *formal, VhdlIdDef *return_type /*= 0*/) const
{
    // Create a generic (treenode) value from a string
    // If the string does not represent a valid VHDL value, return 0.
    // We do not accept multi-dimensional array aggregates,
    // and we don't skip whitespace.
    if (!string) return 0 ;

    unsigned long len = Strings::len(string) ;

    if (!return_type) return_type = formal ? formal->Type() : 0 ;

    // Create the result as a VhdlExpression tree node :
    VhdlExpression *result = 0 ;

    if (len==3 && string[0]=='\'' && string[2]=='\'') {
        // Character Literal ?
        result = new VhdlCharacterLiteral(Strings::save(string)) ;
    } else if (len>1 && string[0]=='"' && string[len-1]=='"') {
        // String Literal ?
        result = new VhdlStringLiteral(Strings::save(string)) ;
    } else if (len>2 && string[1]=='"' && string[len-1]=='"') {
          // Bit String Literal ?
        result = new VhdlBitStringLiteral(Strings::save(string)) ;
    } else if (len>2 && string[0]=='(' && string[len-1]==')') {
        // Viper 8197: Retired use of specific code to convert a string to aggregate
        // and use Expression parser to parse the string and convert it to expression.
        // Aggregate ?
        result = vhdl_file::AnalyzeExpr(string) ;
        // For mixed language we need to keep the array as array
        if (!result->IsAggregate()) {
            // convert it back to aggregate
            Array* assoc_list = new Array(2) ;
            assoc_list->InsertLast(result) ;
            result = new VhdlAggregate(assoc_list) ;
        }
        unsigned environment = (formal && formal->IsInput() && !formal->IsFile()) ? VHDL_READ : 0 ;
        (void) result->TypeInfer(return_type, 0, environment, _present_scope) ;
    } else if (isdigit(string[0]) || (string[0]=='-') || (string[0]=='+')) {
        // A number (integer or real)
        char *remain = 0 ;
        char *number = 0, *full_number = 0 ;
        if (::strchr(string,'.')) {
            // Real Literal ?
            double real_num = ::strtod(string,&remain) ;
            result = new VhdlReal(real_num) ;
        } else {
            // VIPER #7983 : Allow exponent in interger literal
            number = Strings::save(string) ;
            //char *e = ::strchr(number, 'eE') ;
            // Allow both 'e' and 'E'
            char *e = ::strpbrk(number, "eE") ;
            int exp = 0 ;
            if (e) { // Has exponent
                *e = '\0' ; // terminate previous number :
                exp = Strings::atoi(e+1) ; // extract exponent :
                // If there is an exponent, then we will now append that number of 0's to the number.
                if (exp < 0) {
                    if (formal) formal->Error("exponent of integer literal cannot be negative") ;
                } else if (exp) {
                    char *exp_string = Strings::allocate(exp+1) ; // allocate enough space
                    char *tmp = exp_string ; // fill with 'exp' 0's
                    for (int i=0;i<exp;i++) *tmp++ = '0' ;
                    *tmp = '\0' ; // terminate the string
                    full_number = Strings::save(number,exp_string) ;
                    Strings::free(exp_string) ;
                }
            }
            // Integer Literal ?
            verific_int64 int_num = (verific_int64)::strtol(full_number ? full_number: number,&remain,10) ; // FIXME: Use strtoll?

            // VIPER 2758 : If an integer number is given for a enumeration type formal,
            // then we should pick-up the enum value at that integer position of the enum type.
            if (return_type && return_type->IsEnumerationType()) {
                VhdlIdDef *enum_id = return_type->GetEnumAt((unsigned)int_num) ;
                if (enum_id) {
                    result = new VhdlIdRef(enum_id) ;
                }
            }

            // Otherwise, it's just an integer literal :
            if (!result) {
                result = new VhdlInteger(int_num) ;
            }
        }
        // Issue 1878 : Check if there is a physical literal :
        if (remain && (remain[0]!='\0')) {
            // Convert to physical literal (VIPER #2530)
            VhdlIdRef *idref = new VhdlIdRef(Strings::save(remain)) ;
            if (formal) idref->SetLinefile(formal->Linefile()) ;
            result = new VhdlPhysicalLiteral(result, idref) ;
        }
        Strings::free(number) ;
        Strings::free(full_number) ;
    } else if (isalpha(string[0]) || (string[0]=='_')) {
        // Starts with alphanumeric character or _ : identifier
        result = new VhdlIdRef(Strings::save(string)) ;
    }
    // else it is not a valid VHDL value, and we will return 0.

    // Adjust the line/file info (presently NULL) to the line/file info of 'formal' :
    if (formal && result) result->SetLinefile(formal->Linefile()) ;

    // Return 0 if we cannot figure out what kind of value this is.
    return result ;
}

// Viper 3431 : Support for bind construct in Vhdl. The idea is same as Verilog
// bind instance. The verilog instance is converted to a Vhdl Component Instantiation
// statement. This instance is added to the _bind_instances array in the architecture body.
// the instances are picked up during elaboration and elaborated when the top entity is elaborated.
void VhdlPrimaryUnit::ProcessBindInstance(VeriModuleInstantiation *module_instance)
{
    if (!module_instance)  return ;
    VhdlPrimaryUnit *target_unit = 0;
    const char *lib_name = 0;
    VeriLibrary *veri_lib ;
    VhdlLibrary *vhdl_lib ;
    MapIter mi ;
    //VeriModule* module = 0 ;
    FOREACH_VERILOG_LIBRARY(mi, veri_lib) {
        if (!veri_lib) continue ; // Don't find in this library again
        target_unit = vhdl_file::GetVhdlUnitFromVerilog(veri_lib->GetName(), module_instance->GetModuleNameRef()->GetName(), 1) ;
        if (target_unit) {
            lib_name = veri_lib->GetName() ;
            break ;
        }
    }
    if (!target_unit) {
        FOREACH_VHDL_LIBRARY(mi, vhdl_lib) {
            if (!vhdl_lib) continue ;
            target_unit = vhdl_lib->GetPrimUnit(module_instance->GetModuleNameRef()->GetName(), 1) ;
            if (target_unit) {
                lib_name = vhdl_lib->Name() ;
                break ; // Take the first entity, ignore all others for time being
            }
        }
    }
    if (!target_unit) {
        char *escaped_target_name = Strings::save("\\", module_instance->GetModuleNameRef()->GetName(), "\\") ;
        FOREACH_VHDL_LIBRARY(mi, vhdl_lib) {
            if (!vhdl_lib) continue ;
            target_unit = vhdl_lib->GetPrimUnit(escaped_target_name, 1) ;
            if (target_unit) {
                lib_name = vhdl_lib->Name() ;
                break ; // Take the first entity, ignore all others for time being
            }
        }
        Strings::free(escaped_target_name) ;
    }

    VhdlSecondaryUnit *sec_unit = 0 ;
    if (AllSecondaryUnits()->Size() >= 1) {
        unsigned last_idx = AllSecondaryUnits()->Size() - 1 ;
        sec_unit = (VhdlSecondaryUnit*)(AllSecondaryUnits()->GetValueAt(last_idx)) ;
    }
    if (target_unit && sec_unit) {
        VhdlScope *save_scope = VhdlTreeNode::_present_scope ;
        VhdlTreeNode::_present_scope = sec_unit->LocalScope() ;

        Array* inst_array = module_instance->GetInstances() ;
        VeriInstId* module_instId;
        unsigned i ;
        FOREACH_ARRAY_ITEM(inst_array, i, module_instId) {
            if (module_instId == 0) continue ;
            // VIPER #7663 : Create and declare library identifier
            char *lib_name_to_use = 0 ;
            VhdlIdDef *existing_lib_id = _present_scope ? _present_scope->FindSingleObject(lib_name) : 0 ;
            if (existing_lib_id && !existing_lib_id->IsLibrary()) {
                // The library name can be current working library and
                // for that case we can use 'work' instead of proper library name to
                // remove conflict
                VhdlLibrary *work_lib = vhdl_file::GetWorkLib() ;
                if (work_lib && Strings::compare_nocase(work_lib->Name(), lib_name)) {
                    existing_lib_id = _present_scope ? _present_scope->FindSingleObject("work"): 0 ;
                    lib_name_to_use = Strings::save("work") ;
                } else {
                    existing_lib_id = 0 ;
                }
            }
            // Create library id for library name not in using clause of this design unit.
            // The created id is inserted in architecture's scope (_present_scope here).
            // This creats a memory leak in VhdlScope.
            // This library id is created so that we can create instantiated unit like
            // my_lib.entity_name(arch_name) for this instantiation.
            // Library 'my_lib' is not specified as library clause/use clause before this
            // design unit, so constructor of VhdlInstantiatedUnit gives error if we
            // do not create this library id.
            VhdlLibraryId *lib_id = new VhdlLibraryId(lib_name_to_use ? lib_name_to_use: Strings::save(lib_name)) ;
            if (existing_lib_id) {
                if (_present_scope) { _present_scope->DeclarePredefined(lib_id) ; }
            } else {
                if (_present_scope) { (void) _present_scope->Declare(lib_id) ; }
                // Create a library clause and add that in context clause of
                // containing unit
                Array *lib_list = new Array(1) ; lib_list->InsertLast(lib_id) ;
                VhdlLibraryClause *lib_clause = new VhdlLibraryClause(lib_list, 0) ;
                lib_clause->SetLinefile(sec_unit->Linefile()) ;
                sec_unit->AddContextClause(lib_clause) ;
            }
            VhdlName* entity_name =  new VhdlSelectedName(new VhdlIdRef(Strings::save(lib_name)),
                                         new VhdlIdRef(Strings::save(target_unit->Name()))) ;
            VhdlInstantiatedUnit* instantiated_unit = new VhdlInstantiatedUnit(VhdlTreeNode::GetEntityId()/*VHDL_entity*/, entity_name) ;
            instantiated_unit->SetLinefile(module_instId->Linefile()) ;
            // VIPER #7498 : Set working library to library of instantiated unit
            VhdlLibrary *old_work_lib = vhdl_file::GetWorkLib() ;
            vhdl_file::SetWorkLib(target_unit->GetOwningLib()) ;
            // Convert the instance here.
            VhdlComponentInstantiationStatement* entity_instance = module_instance->ConvertInstance(instantiated_unit, module_instId) ;
            sec_unit->AddBindInstance(module_instId->GetName(), entity_instance) ;
            vhdl_file::SetWorkLib(old_work_lib) ;
        }
        VhdlTreeNode::_present_scope = save_scope ;
    }
}
Array *
VhdlPrimaryUnit::GetBindInstances()
{
    VhdlSecondaryUnit *sec_unit = 0 ;
    if (AllSecondaryUnits()->Size() >= 1) {
        unsigned last_idx = AllSecondaryUnits()->Size() - 1 ;
        sec_unit = (VhdlSecondaryUnit*)(AllSecondaryUnits()->GetValueAt(last_idx)) ;
    }
    if (sec_unit) return (sec_unit->BindInstances()) ;
    return 0 ;
}

void VhdlPackageInstantiationDecl::ElaborateInternal()
{
    if (!_id) return ;
    if (_inst_scope) _inst_scope->ElaborateDependencies() ;
    // Elaboration of package instantiation declaration is elaboration of uninstantiated package
    // with instantiation specific generic map setting
    // Following steps are to be performed for this elaboration
    // 1. Copy the _initial_pkg and set that to _elab_pkg (elaboration specific package)
    // 2. Connect the identifiers of elaboration specific package as actual to
    //    alias ids declared in _local_scope. This is needed as package instance can
    //    be connected to interface package instance of itself or other design unit.
    // 3. Elaborate the elaboration specific package (_elab_pkg). After elaboration
    //    of containing design unit this copied '_elab_pkg' should be deleted.
    VhdlMapForCopy old2new ;
    delete _elab_pkg ; // Delete old one
    _elab_pkg = _initial_pkg ? _initial_pkg->CopyPrimaryUnit(_id->Name(), old2new, 0): 0 ;
    SwitchActualIds() ;
}
void VhdlPackageInstantiationDecl::SwitchActualIds() const
{
    // Connect alias identifiers of _local_scope to the elements of _elab_pkg before elaboration
    VhdlScope *actual_scope = _elab_pkg ? _elab_pkg->LocalScope(): (_initial_pkg ? _initial_pkg->LocalScope(): 0) ;
    MapIter mi ;
    VhdlIdDef *formal, *actual ;
    char *name ;
    Map *local_map = _local_scope ? _local_scope->DeclArea(): 0 ;
    Map *actual_map = actual_scope ? actual_scope->DeclArea(): 0 ;
    unsigned idx = 0 ;
    FOREACH_MAP_ITEM(local_map, mi, &name, &formal) { // Iterate over alias ids
        actual = actual_map ? (VhdlIdDef*)actual_map->GetValueAt(idx): 0 ;
        if (actual_scope && (actual == actual_scope->GetOwner())) {
            // We have not created alias id for package identifier, so skip that
            idx++ ;
            actual = actual_map ? (VhdlIdDef*)actual_map->GetValueAt(idx): 0 ;
        }
        idx++ ;
        if (formal && actual) formal->ConnectActualId(actual) ;
    }
}
void
VhdlPackageInstantiationDecl::Elaborate(VhdlDataFlow * /*df*/)
{
    if (IsStaticElab()) (void) StaticElaborateInternal(0, 0, 0, 0, 0) ;
}
// Routines needed in elaboration for push/pop of elaborated package associated
// with package instantiation declaration
VhdlIdDef *
VhdlPackageInstantiationDecl::GetElaboratedPackageId() const
{
    return (_elab_pkg) ? _elab_pkg->Id(): 0 ;
}
void
VhdlPackageInstantiationDecl::SetElaboratedPackageId(VhdlIdDef *id)
{
    delete _elab_pkg ; // delete old one (if exists)
    _elab_pkg = id ? id->GetPrimaryUnit(): 0 ; // Set new one
    SwitchActualIds() ; // Connect the alias ids to new elaborated package elements
}
VhdlIdDef *
VhdlPackageInstantiationDecl::TakeElaboratedPackageId()
{
    VhdlIdDef *r = _elab_pkg ? _elab_pkg->Id(): 0 ; // Store elaborated package
    _elab_pkg = 0 ;
    SwitchActualIds() ; // Connect the alias ids to _initial_pkg (as in analysis)
    return r ; // Return elaborated package id
}

void
VhdlPackageDecl::Elaborate(VhdlDataFlow * /*df*/)
{
    if (IsStaticElab()) {
        (void) StaticElaborate(0, 0, 0, 0, 0) ;
        return ;
    }
}

/*********************************************************************/
//  Association / setting of generics
/*********************************************************************/

void
VhdlEntityDecl::AssociateGenerics(Array *actual_generics, VhdlIdDef *component)
{
    if (!_id) return ;

    // VIPER 2334 : Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // Set generics using actual generics or default generic binding from component

    // VIPER 1863 & 1472 : begin
    // Get last entered value and constraint map from corresponding stacks. Maps
    // can be achieved for recursive instantiation of this entity. If Maps are
    // active formal values and constraints will be set at maps
    Map *id_values = VhdlNode::GetValueMap() ;
    Map *id_constraints = VhdlNode::GetConstraintMap() ;
    Map *id_actuals = VhdlNode::GetActualIdMap() ;
    // VIPER 1863 & 1472 : end

    unsigned i, j ;
    VhdlDiscreteRange *assoc ;
    VhdlIdDef *id ;

    // VHDL-2008 LRM section 6.5.6.1 says 'A name that denotes an interface
    // declaration in a generic interface list may appear in an interface
    // declaration within the interface list containing the denoted interface
    // declaration'
    // To support the above, change the flow of generic association. We need to
    // consider one interface declaration at a time and perform
    // 1) Initialization of constraint
    // 2) Evaluate and associate actual
    // 3) Evaluate initial value if actual not present
    // After the above 3 steps we need to consider next interface declaration.

    // So first create a map containing generic id vs its actual from 'actual_generics'
    Map actuals(POINTER_HASH) ;
    FOREACH_ARRAY_ITEM(actual_generics, i, assoc) {
        if (!assoc) continue ;

        if (assoc->IsAssoc()) {
            id = assoc->FindFormal() ;
        } else {
            // positional association
            id = _id->GetGenericAt(i) ;
        }
        if (!id) continue ; // type-inference checks failed
        (void) actuals.Insert(id, assoc, 0, 1 /* for partial assoc*/) ;
    }

    VhdlDataFlow df(0, 0, _local_scope) ;
    df.SetInInitial() ;
    VhdlInterfaceDecl *decl ;
    VhdlValue *val = 0 ;
    VhdlIdDef *gen_id ;
    VhdlConstraint *constraint ;
    // Set value/constraint of instantiating unit in maps
    if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        // Initialize constraint with current value of generics
        decl->Initialize() ; // Constraints only

        // To evaluate the actual set value/constraint of containing unit in ids
        if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;

        // Get the identifiers from generic decl
        Array *ids = decl->GetIds() ;
        Array tmp_ids(2) ;
        if (ids && ids->Size()) {
            tmp_ids.Append(ids) ;
        } else { // For VHDL-2008 specific type/package/subprogram generic
            gen_id = decl->GetId() ;
            tmp_ids.InsertLast(gen_id) ;
        }
        FOREACH_ARRAY_ITEM(&tmp_ids, j, gen_id) { // Iterate over ids
            if (!gen_id) continue ;
            MapItem *item ;
            FOREACH_SAME_KEY_MAPITEM(&actuals, gen_id, item) { // Get proper actual
                assoc = item ? (VhdlDiscreteRange*)item->Value(): 0 ;
                if (!assoc) continue ;

                if (assoc->IsAssoc()) {
                    id = assoc->FindFullFormal() ;
                    if (!id) { // Partial association
                        // If formal is not constrained, set proper constraint to it
                        VhdlNode::EvaluateConstraintForPartialAssoc(actual_generics, assoc, &df) ;
                        assoc->PartialAssociation(0, 0) ;
                        continue ;
                    }
                } else {
                    // positional association
                    id = gen_id ; //_id->GetGenericAt(i) ;
                }
                if (!id) continue ; // type-inference checks failed

                // VIPER 1863 & 1472
                // For recursive instantiation, get constraint from stack
                constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id): id->Constraint() ; // Formal-constraint

                if (id->IsGenericType()) {
                    // VIPER #5918 : Formal is interface_type, we need to calculate constraint of it from
                    // actual and it cannot have any value
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, 0):0 ;
                    if (constraint) {
                        if (id_constraints && id_values) {
                            (void) id_constraints->Insert(id, constraint, 1) ;
                            (void) id_values->Insert(id, 0, 1) ;
                        } else {
                            id->SetConstraint(constraint) ;
                            id->SetValue(0) ;
                        }
                    }
                    VhdlIdDef *actual_type = actual_part ? actual_part->TypeInfer(0, 0, VHDL_range, 0): 0 ;
                    if (id_actuals) {
                        (void) id_actuals->Insert(id, actual_type, 1) ;
                    } else {
                        id->SetActualId(actual_type, actual_part ? actual_part->GetAssocList() : 0) ;
                    }
                    continue ;
                }
                if (id->IsSubprogram()) {
                    // Formal is generic subprogram, actual can only be subprogram name which
                    // conforms with formal
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_subprogram = actual_part ? actual_part->SubprogramAspect(id): 0 ;
                    if ((!actual_subprogram && actual_part) || (actual_subprogram && !actual_subprogram->IsSubprogram())) {
                        assoc->Error("subprogram name should be specified as actual for formal generic subprogram %s", id->Name()) ;
                    }
                    if (actual_subprogram && !id->IsHomograph(actual_subprogram)) {
                        assoc->Error("subprogram %s does not conform with its declaration", actual_subprogram->Name()) ;
                    }
                    if (!actual_subprogram) continue ;
                    if (id_actuals) {
                        (void) id_actuals->Insert(id, actual_subprogram, 1) ;
                    } else {
                        id->SetActualId(actual_subprogram, 0) ;
                    }
                    continue ;
                }
                if (id->IsPackage()) {
                    // Formal is a generic package, actual can be a package
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_package = actual_part ? actual_part->EntityAspect(): 0 ;
                    if ((!actual_package && actual_part) || (actual_package && !actual_package->IsPackage())) {
                        assoc->Error("package name should be specified as actual for formal generic package %s", id->Name()) ;
                    }
                    if (!actual_package) continue ;
                    _present_scope = old_scope ; // Context of instantiating unit
                    if (id_actuals) {
                        (void) id_actuals->Insert(id, actual_package->GetElaboratedPackageId(), 1) ;
                    } else {
                        id->SetElaboratedPackageId(actual_package) ;
                    }
                    _present_scope = _local_scope ;
                    continue ;
                }
                if (!constraint) continue ; // something bad happened in initialization

                _present_scope = old_scope ;
                val = assoc->Evaluate(constraint, &df, 0) ; // Evaluate actuals
                _present_scope = _local_scope ;
                if (!val) continue ;
                // Generics are constant :
                if (!val->IsConstant()) {
                    assoc->Error("value for generic %s is not constant", id->Name()) ;
                    continue ;
                }
#if 0
                // Commented out for Viper 6853
                // Viper 6649 : We now match scalar vs array or 2 array of different size.
                // adjust the length of the value of the formal to match that of scalar
                if (Strings::compare(id->Type()->Name(), "vl_logic_vector") && !constraint->IsUnconstrained()) {
                    unsigned formal_length = (unsigned)(constraint->Length()) ;
                    val->Adjust(formal_length) ;
                }
#endif

                // Translate back to a constant (get rid of any constant NonConstVal's)
                val = val->ToConst(id->Type()) ;

                // LRM 3.2.1.1
                if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

                // Check actual against constraint
                // Viper 5897: Do not call CheckAgainst if the type is vl_logic_vector as we are matching it with integer
                // Viper 5889 and 5909. Call checkAgainst only when both are vector in mixed language flow as we can
                // have integer vs vhdl vector. There are two cases to it 1) when vhdl in verilog and 2) verilog in vhdl
                VhdlIdDef *id_type = id->Type() ;
                if ((!Strings::compare(id_type->Name(), "vl_logic_vector")) && // For Verilog in Vhdl
                    (!assoc->FromVerilog() || (!id_type->IsArrayType() || val->IsComposite()))) {
                    // For Vhdl in Verilog
                    if (!val->CheckAgainst(constraint,assoc, 0)) {
                        delete val ;
                        continue ;
                    }
                }

                // VIPER 1863 & 1472 : begin
                if (id_values) { // Has recursion
                    // This routine is called twice with same map 'id_values'. So, need to
                    // delete old data from 'id_values' and use force insert
                    VhdlValue *old_val = (VhdlValue*)id_values->GetValue(id) ; // Get old value
                    (void) id_values->Insert(id, val, 1 /* force overwrite */) ; // Insert actual to stack
                    delete old_val ; // Delete old value
                } else { // Set value in id
                // VIPER 1863 & 1472 : end
                    id->SetValue(val) ;
                }
            }

            // Default Generic Binding LRM 5.2.2 :
            // If there is NO generic binding, use the generic values from the component.
            if (!actuals.Size() && component) {
                // Entity contains generics, and there are NO generic bindings. Default binding applies now.

                // Run over the COMPONENT, and bind all its generic values to our values.
                // Have to error out if entity does not have such a generic.
                VhdlIdDef *actual ;
                // Viper 7667: If the gen_id is escaped name in the converted entity the component might
                // have an unescaped name. So search the enescaped name as well.
                actual = component->GetGeneric(gen_id->Name()) ;
                if (!actual && IsVerilogModule()) {
                    char *unescaped_name = VhdlNode::UnescapeVhdlName(gen_id->Name()) ;
                    if (unescaped_name) {
                        actual = component->GetGeneric(Strings::strtolower(unescaped_name)) ;
                        Strings::free(unescaped_name) ;
                    }
                }
                if (!actual) continue ; // something wrong..

                // Check id consistency with entity generic :
                id = _id->GetGeneric(actual->Name()) ;
                if (!id) {
                    actual->Error("binding entity %s does not have generic %s", _id->Name(), actual->Name()) ;
                    _id->Info("%s is declared here", _id->Name()) ;
                    continue ;
                }

                if (!id->IsGenericType() && !id->IsSubprogram() && !id->IsPackage()) {
                    // Check type consistency
                    VERIFIC_ASSERT(id->Type() && actual->Type()) ;
                    VhdlIdDef* actual_type = actual->Type() ;
                    unsigned is_actual_array_type = actual_type->IsArrayType() ;
                    // Viper 8481: Match bit/boolean with integral verilog types in mixed language
                    unsigned is_actual_bit_or_boolean = 0 ;
                    if (_present_scope && _is_verilog_module) {
                        VhdlIdDef* base_type = actual_type->BaseType() ;
                        if (base_type && (base_type == VhdlNode::StdType("boolean") || base_type == VhdlNode::StdType("bit"))) is_actual_bit_or_boolean = 1 ;
                    }
                    // VIPER 5857. the generics of converted module can also be of vl_logic_vector type
                    // VIPER 5909 5889 TypeMatchDual should be called for integer type in verilog and array type in vhdl
                    if (Strings::compare(id->Type()->Name(), "vl_logic_vector") || (_is_verilog_module && id->Type()->IsIntegerType() &&
                                                                                    (is_actual_array_type || is_actual_bit_or_boolean))) {
                        if (!id->Type()->TypeMatchDual(actual->Type(), 1)) {
                            actual->Error("verilog module parameter %s does not match with type %s of component generic", actual->Name(), actual->Type()->Name()) ;
                            continue ;
                        }
                    } else if (!id->Type()->TypeMatch(actual->Type())) {
                        actual->Error("entity generic %s does not match with type %s of component generic", actual->Name(), actual->Type()->Name()) ;
                        id->Info("%s is declared here", id->Name()) ;
                        continue ;
                    }
                }

                // FIX ME : These, and other association mismatches should be done in the analyzer ?

                // VIPER 1863 & 1472
                // If stack is active, get constraint from stack
                constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id) : id->Constraint() ; // constraint of the formal.

                if (!constraint && actual->IsGenericType()) {
                    // VIPER #5918 : This is type generic, need to set constraint from component's generic
                    constraint = actual->Constraint() ;
                    if (!constraint) continue ;
                    if (id_constraints && id_values && id_actuals) {
                        (void) id_constraints->Insert(id, constraint->Copy()) ;
                        (void) id_values->Insert(id, 0) ;
                        (void) id_actuals->Insert(id, actual->Type(), 1) ;
                    } else {
                        id->SetConstraint(constraint->Copy()) ;
                        id->SetValue(0) ;
                        id->SetActualId(actual->Type(), 0) ;
                    }
                    continue ;
                }
                if (id->IsSubprogram()) {
                    // Formal is generic subprogram, actual can only be subprogram name which
                    // conforms with formal
                    VhdlIdDef *actual_subprogram = actual->GetActualId() ;
                    if (!actual_subprogram || !actual_subprogram->IsSubprogram()) {
                        actual->Error("subprogram name should be specified as actual for formal generic subprogram %s", id->Name()) ;
                    }
                    if (actual_subprogram && !id->IsHomograph(actual_subprogram)) {
                        actual->Error("subprogram %s does not conform with its declaration", actual_subprogram->Name()) ;
                    }
                    if (actual_subprogram) {
                        if (id_actuals) {
                            (void) id_actuals->Insert(id, actual_subprogram, 1) ;
                        } else {
                            id->SetActualId(actual_subprogram, 0) ;
                        }
                    }
                    continue ;
                }
                if (id->IsPackage()) {
                    // Formal is a generic package, actual can be a package
                    //VhdlIdDef *actual_package = actual->GetActualId() ;
                    //if (!actual_package || !actual_package->IsPackage()) {
                    if (!actual->IsPackage()) {
                        actual->Error("package name should be specified as actual for formal generic package %s", id->Name()) ;
                    }

                    _present_scope = old_scope ; // Context of instantiating unit
                    if (id_actuals) {
                        (void) id_actuals->Insert(id, actual->GetElaboratedPackageId(), 1) ;
                    } else {
                        id->SetElaboratedPackageId(actual) ;
                    }
                    _present_scope = _local_scope ;
                    continue ;
                }
                if (!constraint) continue ; // something bad happened in initialization

                // Get the value from the actual :
                // CHECK ME : can we 'take' the value ?
                val = (actual->Value()) ? actual->Value()->Copy() : 0 ;

                // VIPER #7665: Translate back to a constant (get rid of any constant NonConstVal's)
                if (val) val = val->ToConst(actual->Type()) ;

#if 0
                // Commented out for Viper 6853
                // Viper 6649 : We now match scalar vs array or 2 array of different size.
                // adjust the length of the value of the formal to match that of scalar
                if (val && Strings::compare(id->Type()->Name(), "vl_logic_vector") && !constraint->IsUnconstrained()) {
                    unsigned formal_length = (unsigned)(constraint->Length()) ;
                    val->Adjust(formal_length) ;
                }
#endif
                // Adjust constraint to the actual constraint (not the value)
                // This is to mimic the implicit association  formal<=actual.
                if (constraint->IsUnconstrained()) constraint->ConstraintBy(actual->Constraint()) ;

                // Check actual against constraint (this should be kind of obsolete here since we already associate formal and actual in full).
                // Viper 5897: Do not call CheckAgainst if the type is vl_logic_vector as we are matching it with integer
                // for dual lang we would match integer with vector types in vhdl. So call CheckAgainst only if actual_type is array type.
                // this is the case of verilog in vhdl only as componet is present.
                if(!_is_verilog_module) {// For pure Vhdl Designs only
                    if (val && !val->CheckAgainst(constraint,actual, 0)) {
                        delete val ;
                        continue ;
                    }
                }

                // Now assign back to formal, regardless of direction :
                // VIPER 1863 & 1472 : begin
                if (id_values) {
                    // If stack is active, set value to value stack, not in id
                    // This routine is called twice with same map 'id_values'. So, need to
                    // delete old data from 'id_values' and use force insert
                    VhdlValue *old_val = (VhdlValue*)id_values->GetValue(id) ; // Get old value
                    (void) id_values->Insert(id, val, 1 /* force overwrite */) ;
                    delete old_val ; // Delete old value
                } else {
                // VIPER 1863 & 1472 : end
                    id->SetValue(val) ;
                }
            }
        }
        // Now evaluate initial expression :
        // To evaluate the initial set value/constraint of instantiated unit in ids
        if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;
        decl->InitialValues(0, 0, &df, old_scope) ;
    }
    // Set value/constraint of instantiated unit in maps and instantiating unit in ids
    if (id_constraints) SwitchValueConstraintOfScope(_local_scope, 1) ;

    // Check id consistency with entity generic :
    if (!actual_generics && component) {
        VhdlIdDef *actual ;
        for (i=0; i<component->NumOfGenerics(); i++) {
            actual = component->GetGenericAt(i) ;
            if (!actual) continue ; // something wrong..

            // Check id consistency with entity generic :
            id = _id->GetGeneric(actual->Name()) ;
            if (!id) {
                actual->Error("binding entity %s does not have generic %s", _id->Name(), actual->Name()) ;
                _id->Info("%s is declared here", _id->Name()) ;
                continue ;
            }
        }
    }

    // VIPER 2334 : Reset the global scope
    _present_scope = old_scope ;
}

void
VhdlConfigurationDecl::AssociateGenerics(Array *actual_generics, VhdlIdDef *component)
{
    // Call the entity that the configuration configures
    VhdlIdDef *entity = (_id) ? _id->GetEntity() : 0 ;
    if (!entity) return ; // error ?
    VhdlPrimaryUnit *the_entity = entity->GetPrimaryUnit() ;
    if (!the_entity) return ; // error ?
    // If the_entity == this return. This can happen if this is a verilog configuration
    if (the_entity == this) return ;
    the_entity->AssociateGenerics(actual_generics, component) ;
}

void
VhdlPackageDecl::AssociateGenerics(Array *actual_generics, VhdlIdDef * /*component*/)
{
    if (!_id) return ;
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0, 0, _local_scope) ;
    df.SetInInitial() ;

    // VHDL-2008 LRM section 6.5.6.1 says 'A name that denotes an interface
    // declaration in a generic interface list may appear in an interface
    // declaration within the interface list containing the denoted interface
    // declaration'
    // To support the above, change the flow of generic association. We need to
    // consider one interface declaration at a time and perform
    // 1) Initialization of constraint
    // 2) Evaluate and associate actual
    // 3) Evaluate initial value if actual not present
    // After the above 3 steps we need to consider next interface declaration.
    // So first create a map containing generic id vs its actual from 'actual_generics'
    Map actuals(POINTER_HASH) ;
    unsigned i, j ;
    VhdlInterfaceDecl *decl ;
    // Set actual generic values
    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id, *gen_id ;
    VhdlConstraint *constraint ;
    Array *generics = (actual_generics && actual_generics->Size()) ? actual_generics : _generic_map_aspect ;
    FOREACH_ARRAY_ITEM(generics, i, assoc) {
        if (!assoc || assoc->IsBox() || assoc->IsDefault()) continue ;

        // Find the formal.
        if (assoc->IsAssoc() ) {
            // Find the formal :
            id = assoc->FindFormal() ;
            if (id && id->IsPackageInstElement()) {
                id = id->GetConnectedActualId() ;
            }
        } else {
            // positional association
            id = _id->GetGenericAt(i) ;
        }
        if (id) (void) actuals.Insert(id, assoc, 0, 1 /* for partial assoc*/) ;
    }
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        decl->Initialize() ; // Constraints only. Set all values to 0.

        // Get generic ids from declaration :
        Array *ids = decl->GetIds() ;
        Array tmp_ids(2) ;
        if (ids && ids->Size()) { // Value generics
            tmp_ids.Append(ids) ;
        } else { // For VHDL-2008 specific type/package/subprogram generic
            gen_id = decl->GetId() ;
            tmp_ids.InsertLast(gen_id) ;
        }
        FOREACH_ARRAY_ITEM(&tmp_ids, j, gen_id) { // Iterate over ids
            if (!gen_id) continue ;
            MapItem *item ;
            FOREACH_SAME_KEY_MAPITEM(&actuals, gen_id, item) { // Get proper actual
                assoc = (VhdlDiscreteRange*)item->Value() ;
                if (!assoc || assoc->IsBox() || assoc->IsDefault()) continue ;

                // Find the formal.
                if (assoc->IsAssoc() ) {
                    // Find the formal :
                    id = assoc->FindFullFormal() ;
                    if (id && id->IsPackageInstElement()) {
                        id = id->GetConnectedActualId() ;
                    }
                    if (!id) {
                        // Partial association....
                        // If formal is not constrained, set proper constraint to it
                        VhdlNode::EvaluateConstraintForPartialAssoc(_generic_map_aspect, assoc, &df) ;
                        assoc->PartialAssociation(0, 0) ;
                        continue ;
                    }
                } else {
                    // positional association
                    id = gen_id ;
                }

                // Standard handling of actual-formal association
                constraint = id->Constraint() ;
                if (id->IsGenericType()) {
                    // VIPER #5918 : Formal is interface_type, we need to calculate constraint of it from
                    // actual and it cannot have any value
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, 0):0 ;
                    if (constraint) {
                        id->SetConstraint(constraint) ;
                        id->SetValue(0) ;
                    }
                    VhdlIdDef *actual_type = actual_part ? actual_part->TypeInfer(0, 0, VHDL_range, 0): 0 ;
                    id->SetActualId(actual_type, 0) ;
                    continue ;
                }
                if (id->IsSubprogram()) {
                    // Formal is generic subprogram, actual can only be subprogram name which
                    // conforms with formal
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_subprogram = actual_part ? actual_part->SubprogramAspect(id): 0 ;
                    if ((!actual_subprogram && actual_part) || (actual_subprogram && !actual_subprogram->IsSubprogram())) {
                        assoc->Error("subprogram name should be specified as actual for formal generic subprogram %s", id->Name()) ;
                    }
                    if (actual_subprogram && !id->IsHomograph(actual_subprogram)) {
                        assoc->Error("subprogram %s does not conform with its declaration", actual_subprogram->Name()) ;
                    }
                    if (actual_subprogram) id->SetActualId(actual_subprogram, 0) ;
                    continue ;
                }
                if (id->IsPackage()) {
                    // Formal is a generic package, actual can be a package
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_package = actual_part ? actual_part->EntityAspect(): 0 ;
                    if ((!actual_package && actual_part) || (actual_package && !actual_package->IsPackage())) {
                        assoc->Error("package name should be specified as actual for formal generic package %s", id->Name()) ;
                    }
                    _present_scope = old_scope ; // Context of instantiating unit
                    if (actual_package) id->SetElaboratedPackageId(actual_package) ;
                    _present_scope = _local_scope ;
                    continue ;
                }
                if (!constraint) continue ; // Something bad happened in initialization

                // Evaluate actual in the context of instantiating unit
                if (actual_generics) _present_scope = old_scope ;
                val = assoc->Evaluate(constraint, &df, 0) ;
                if (actual_generics) _present_scope = _local_scope ;
                if (!val) continue ;

                if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

                if (!val->CheckAgainst(constraint,assoc, 0)) {
                    delete val ;
                    continue ;
                }

                // Set the initial value
                id->SetValue(val) ;
            }
        }
        // Now evaluate initial expression :
        // To evaluate the actual set value/constraint of containing unit in ids
        decl->InitialValues(0, 0, &df, old_scope) ;
    }
    _present_scope = old_scope ;
}

void
VhdlContextDecl::AssociateGenerics(Array * /*actual_ports*/, VhdlIdDef * /*component*/)
{
    // Don't do anything
}

/*********************************************************************/
//  Association / setting of ports
/*********************************************************************/

void
VhdlEntityDecl::AssociatePorts(Instance *inst, Array *actual_ports, VhdlIdDef *component)
{
    VERIFIC_ASSERT(_id) ;

    // VIPER 2334 : Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // Hook-up the portinsts of 'inst' to the actual ports
    // Assume that generics are already set appropriately

    // If there are no 'actual' ports, take the port values from the 'default_ports_component'
    // according to the rules in LRM 5.2.2.

    // VIPER 1863 & 1472 : begin
    // Get last entered value and constraint map from corresponding stacks. Maps
    // can be achieved for recursive instantiation of this entity. If Maps are
    // active formal values and constraints will be set at maps
    Map *id_values = VhdlNode::GetValueMap() ;
    Map *id_constraints = VhdlNode::GetConstraintMap() ;

    // To evaluate constraints of ports we need current value of generic ids
    // evaluated in 'AssociateGenerics'. Current values of generics are in
    // stack, so switch value/constraint between iddefs and stack
    VhdlNode::SwitchValueConstraintOfScope(_local_scope, 1 /* only generic ids*/) ;
    // VIPER 1863 & 1472 : end

    // Initialize the ports
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_port_clause,i,decl) {
        // VIPER 1863 & 1472 : begin
        if (id_constraints) { // Recursion, insert formal constraint in stack
            decl->InitializeConstraints(*id_constraints) ; // Get last entered constraint Map from stack
        } else {
            // VIPER 1863 & 1472 : end
            decl->Initialize() ; // Create only constraints. Set all values to 0.
        }
    }

    VhdlDiscreteRange *assoc ;
    VhdlConstraint *constraint ;
    VhdlIdDef *id ;
    VhdlValue *val ;
    // VIPER 1863 & 1472
    // If value/constraint stack is active for recursion, calculate actuals obtaining formal
    // constraint from constraint stack as we have set formal constraint to stack.
    // Generic ids now contain instantiated unit's value/constraint, set instantiating
    // unit's value/constraint to generic ids so that we can calculate actuals properly.
    VhdlNode::SwitchValueConstraintOfScope(_local_scope, 1/* only generics*/) ;

    VhdlIdDef *prev_formal_id = 0 ;
    Set partial_formals(POINTER_HASH) ;
    // Viper 4395 : Issue error for individually associated formal which is not in contiguous
    // sequence. Here prev_formal_id is the previous formal associated. All partially associated
    // formals are stored in partial_formals.

    FOREACH_ARRAY_ITEM(actual_ports, i, assoc) {
        if (!assoc) continue ;

        // Find the formal.
        if (assoc->IsAssoc() ) {
            // Find the formal :
            id = assoc->FindFullFormal() ;
            if (!id) {
                // Partial association....
                // If formal is not constrained, set proper constraint to it
                VhdlNode::EvaluateConstraintForPartialAssoc(actual_ports, assoc, 0) ;
                assoc->PartialAssociation(inst, 0) ;
                VhdlName *formal_name = assoc->FormalPart() ;
                VhdlName *prefix_name = formal_name ? formal_name->GetPrefix() : 0 ;
                while (prefix_name && prefix_name->GetPrefix()) prefix_name = prefix_name->GetPrefix() ;
                VhdlIdDef *prefix_id = prefix_name ? prefix_name->GetId() : 0 ;
                // Viper 4395 : We issue error if the formal is already associated
                // previously but not in the one before this
                // Viper 6055: The prefix_id must be a port of _id
                if (prefix_id && prev_formal_id != prefix_id && partial_formals.Get(prefix_id) && _id->GetPort(prefix_id->Name())) {
                    assoc->Error("formal %s associated individually was not in contiguous sequence", prefix_id->Name()) ;
                }
                prev_formal_id = prefix_id ;
                (void) partial_formals.Insert(prefix_id, 0) ;
                continue ;
            }
        } else {
            // positional association
            id = _id->GetPortAt(i) ;
        }
        prev_formal_id = 0 ;
        if (!id) continue ; // type-inference checks failed

        VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual

        // Viper 6649 : TypeMatchDual was not called for array vs scalar types during TypeInferAssociationList
        // So Call it now. Need to call typeinfer to  evaluate the type of actual here.
        VhdlIdDef *id_type = id->Type() ;
        unsigned is_vl_type = 0 ;
        if (id_type && (Strings::compare(id_type->Name(), "vl_logic") || Strings::compare(id_type->Name(), "vl_logic_vector"))) is_vl_type = 1 ;

        if (is_vl_type && actual_part) {
            VhdlIdDef *actual_id = actual_part->GetId() ;
            //Formal type is passed as NULL in TypeInfer so that the already resolved type
            //of the actual is inferred. aggregate_type is not supported with vl_logic_vector

            // Viper 5082. For string and character literal we cannot pass NULL to Typeinfer
            // For string literal we pass typeid for std_logic_vector and std_logic for character literal

            VhdlIdDef *actual_type = 0 ;

            if (actual_part->IsStringLiteral() || actual_part->IsCharacterLiteral()) {
                actual_type = actual_id ? actual_id->Type() : 0 ;
            } else {
                // No need to pass environment as the markings are already done during anlysis.
                actual_type = actual_part->TypeInfer(0, 0, 0, 0) ;
            }

            unsigned typematching_reqd = 0 ;
            if (id_type && actual_type) {
                if (id_type->IsArray() && !actual_type->IsArray()) typematching_reqd = 1 ;
                if (!id_type->IsArray() && actual_type->IsArray()) typematching_reqd = 1 ;
            }

            if (typematching_reqd && id_type && actual_id && actual_type) {
                if (id_type->IsArrayType()) {
                    Array* verilog_assoc_list = id->GetVerilogAssocList() ;
                    unsigned dim = verilog_assoc_list ? verilog_assoc_list->Size() : 0 ;
                    if (dim) {
                        actual_type = actual_type->ElementType(dim) ;
                        id_type = id_type->ElementType() ;
                    }
                }
                if (!id_type->TypeMatchDual(actual_type, 0)) {
                    VhdlConstraint *id_constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id) : id->Constraint() ;
                    VhdlConstraint *actual_constraint = actual_part->EvaluateConstraintInternal(0, id_constraint) ;
                    if (id_constraint && actual_constraint && id_constraint->GetSizeForConstraint() == 1 &&
                        actual_constraint->GetSizeForConstraint()==1) {
                        // Viper 6649 do not error out for vectors of length 0 and scalar
                        // Check if BaseType are compatible
                        VhdlIdDef *actual_element_type = actual_constraint->IsArrayConstraint() ? actual_type->ElementType() : actual_type ;
                        VhdlIdDef *id_element_type = id_constraint->IsArrayConstraint() ? id_type->ElementType() : id_type ;
                        if (!id_element_type->TypeMatchDual(actual_element_type, 0))  {
                            // Viper : 4938 (2) Improve message clarity. this here refers to a Verilo module and not an entity in the design
                            char* error_msg = Strings::save("of VHDL port does not match its Verilog connection") ;
                            assoc->Error("type error near %s ; expected type %s", actual_id->Name(), error_msg) ;
                            Strings::free(error_msg) ;
                        }
                    } else {
                        char* error_msg = Strings::save("of VHDL port does not match its Verilog connection") ;
                        assoc->Error("type error near %s ; expected type %s", actual_id->Name(), error_msg) ;
                        Strings::free(error_msg) ;
                    }
                    delete actual_constraint ;
                }
            }
        }

        // VIPER 1863 & 1472
        // For recursive instantiation get constraint from stack, otherwise from iddef
        constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id): id->Constraint() ;

        if (!constraint) continue ; // something bad happened in initialization

        Array* assoc_list = id->GetVerilogAssocList() ;
        if (assoc_list && id_type && IsVerilogModule()) {
            VhdlDiscreteRange *range_item ;
            unsigned k ;
            VhdlConstraint* curr_constr =  0 ;
            if (is_vl_type) {
                curr_constr = constraint->ElementConstraint() ? constraint->ElementConstraint()->Copy() : 0 ;
            } else {
                curr_constr = constraint->Copy() ;
            }
            if (curr_constr) {
                VhdlDataFlow df(0, 0, _local_scope) ;
                df.SetInInitial() ;
                FOREACH_ARRAY_ITEM(assoc_list, k, range_item) {
                    if (!range_item) continue ;
                    (void) range_item->TypeInfer(0, 0, 0, 0) ;
                    VhdlConstraint* range_constraint = range_item->EvaluateConstraintInternal(&df,0) ;
                    curr_constr = new VhdlArrayConstraint(range_constraint, curr_constr) ;
                }
                if (id->Constraint()) {
                    id->SetConstraint(curr_constr) ;
                } else if (id_constraints) {
                    delete constraint ;
                    (void) id_constraints->Insert(id, curr_constr) ;
                }
                constraint = curr_constr ;
            }
        }

        // Create values : evaluate the actual, regardless of direction (so we can support unconstrained ports)
        _present_scope = old_scope ; // Context of instantiating unit
        val = assoc->Evaluate(constraint, 0,0) ;
        _present_scope = _local_scope ;

        if (!val && IsStaticElab()) {
            // VIPER #2651 : Static elaboration does not evaluate non-constant value.
            // If formal is unconstrained, formal should get constrained from
            // actual. So, try to evaluate constraint of actual and adjust target
            // constraint accordingly
            VhdlConstraint *actual_constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, constraint):0 ;
            if (constraint->IsUnconstrained()) {
                constraint->ConstraintBy(actual_constraint) ;
            } else {
                // VIPER #3140 : Check size mismatching in port association
                if (actual_constraint && !actual_constraint->IsUnconstrained()) {
                    if (is_vl_type && constraint->GetSizeForConstraint()==1 && actual_constraint->GetSizeForConstraint()==1) {
                        // This is a case of one scalar and one array constraint
                        // contrain the id_constraint by the actual here (This can happen for mixed language designs only)
                        VhdlConstraint* id_constraint = constraint ;
                        VhdlConstraint* actual_constraint1 = actual_constraint ; //To avoid memory leak
                        if ((id_constraint->IsArrayConstraint() && !actual_constraint1->IsArrayConstraint()) ||
                            (!id_constraint->IsArrayConstraint() && actual_constraint1->IsArrayConstraint())) {
                            if (id_constraint->IsArrayConstraint()) id_constraint = id_constraint->ElementConstraint() ;
                            if (actual_constraint1->IsArrayConstraint()) actual_constraint1 = actual_constraint1->ElementConstraint() ;
                        }
                        (void) id_constraint->CheckAgainst(actual_constraint1, assoc, 0) ;
                    } else {
                        (void) constraint->CheckAgainst(actual_constraint, assoc, 0) ;
                    }
                }
            }
            delete actual_constraint ;
        }
        if (!val) continue ; // can be for OPEN association. Don't set a value, so initial value will be set.

        // Adjust constraint if needed.
        if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

        // Issue 1858 : For inputs, do this check before formal net-assigns.
        // or else we will enter net-assign with illegal values.
        if (id->IsInput() && !val->CheckAgainst(constraint,assoc, 0)) {
            delete val ;
            continue ;
        }

        // Check actual against constraint.
        // Issue 1858 : For outputs, do this check after formal net-assigns.
        // or else we will have cut-off scalar outputs already
        if (!id->IsInput() && !val->CheckAgainst(constraint,assoc, 0)) {
            delete val ;
            continue ;
        }

        // VIPER 1863 & 1472 : begin
        if (id_values) { // Has recursion, set actual value to stack, not in id
            // This routine is called twice with same map 'id_values'. So, need to
            // delete old data from 'id_values' and use force insert
            VhdlValue *old_val = (VhdlValue*)id_values->GetValue(id) ; // Get old value
            (void) id_values->Insert(id, val, 1 /* force overwrite */) ; // Insert actual to stack
            delete old_val ; // Delete old value
        } else {
        // VIPER 1863 & 1472 : end
            // Just plain value setting now.
            if (IsRtlElab()) {
                id->SetValue(val) ;
            } else {
                delete val ;
            }
        }
    }

    // Default Port Binding LRM 5.2.2 :
    // If there is NO port binding, use the port values from the component.
    if (!actual_ports && component) {
        // Entity contains ports, and there are NO port bindings. Default binding applies now.

        // Run over the COMPONENT, and bind all its port values to our values.
        // Have to error out if entity does not have such a port.
        VhdlIdDef *actual ;
        for (i=0; i<component->NumOfPorts(); i++) {
            actual = component->GetPortAt(i) ;
            if (!actual) continue ; // something wrong..

            // We can only do these 'binding' checks now in elaboration, because during
            // analysis binding was not yet defined.
            // So do similar tests as in TypeInferAssociationList(), but now for ports only.

            // Check id consistency with entity port :
            id = _id->GetPort(actual->Name()) ;
            if (!id) {
                actual->Error("binding entity %s does not have port %s", _id->Name(), actual->Name()) ;
                _id->Info("%s is declared here", _id->Name()) ;
                continue ;
            }

            // Check type consistency
            unsigned is_mixed_type_of_unit_length = 0 ;
            VhdlIdDef *id_type = id->Type() ;
            VhdlIdDef *actual_type = actual->Type() ;
            unsigned is_vl_type = (Strings::compare(id_type->Name(), "vl_logic") || Strings::compare(id_type->Name(), "vl_logic_vector")) ;
            if (id_type && actual_type) {
                // Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
                if (is_vl_type) {
                    if (id_type->IsArrayType()) {
                        Array* verilog_assoc_list = id->GetVerilogAssocList() ;
                        unsigned dim = verilog_assoc_list ? verilog_assoc_list->Size() : 0 ;
                        if (dim) {
                            actual_type = actual_type->ElementType(dim) ;
                            id_type = id_type->ElementType() ;
                        }
                    }

                    if (!id_type->TypeMatchDual(actual_type, 0)) {
                        if (_is_verilog_module) {
                            VhdlConstraint *id_constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id) : id->Constraint() ;
                            VhdlConstraint *actual_constraint = actual->Constraint() ;
                            if (id_constraint && actual_constraint &&
                                id_constraint->GetSizeForConstraint() == 1 &&
                                actual_constraint->GetSizeForConstraint()==1) {
                                    is_mixed_type_of_unit_length = 1 ;
                                    // Viper 6649 do not error out for vectors of length 0 and scalar
                                    // Check if BaseType are compatible
                                    VhdlIdDef *actual_element_type = actual_constraint->IsArrayConstraint() ? actual_type->ElementType() : actual_type ;
                                    VhdlIdDef *id_element_type = id_constraint->IsArrayConstraint() ? id_type->ElementType() : id_type ;
                                    if (!id_element_type->TypeMatchDual(actual_element_type, 0))  {
                                        // Viper : 4938 (2) Improve message clarity. this here refers to a Verilo module and not an entity in the design
                                        actual->Error("verilog module port %s does not match with type %s of component port",actual->Name(),actual->Type()->Name());
                                    }
                            } else {
                                // Viper : 4938 (2) Improve message clarity. this here refers to a Verilo module and not an entity in the design
                                actual->Error("verilog module port %s does not match with type %s of component port",actual->Name(),actual->Type()->Name());
                            }
                        } else {
                            actual->Error("entity port %s does not match with type %s of component port", actual->Name(), actual->Type()->Name()) ;
                        }
                        id->Info("%s is declared here", id->Name()) ;
                        //continue ; // Viper 4938. Commented out so that values are set on formal to avoid secondary error,
                    }                // "formal a has no actual or default value VHDL-1081" when the error is downgraded
                // Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
                } else if (!id->Type()->TypeMatch(actual->Type())) {
                    actual->Error("entity port %s does not match with type %s of component port", actual->Name(), actual->Type()->Name()) ;
                    id->Info("%s is declared here", id->Name()) ;
                    //continue ; // Viper 4938. Commented out so that values are set on formal to avoid secondary error,
                }                // "formal a has no actual or default value VHDL-1081" when the error is downgraded
            }

            if (IsVhdl2008()) {
                // Viper 7666: Modify port vs actual direction criteria for match as per Vhdl2008 rules described in section: 6.5.6.3
                if ((id->IsInput()  && !(actual->IsInput() || actual->IsOutput() || actual->IsInout() || actual->IsBuffer())) ||
                    (id->IsOutput() && !(actual->IsInout() || actual->IsOutput() || actual->IsBuffer())) ||
                    (id->IsInout()  && !(actual->IsInout() || actual->IsOutput() || actual->IsBuffer())) ||
                    (id->IsBuffer() && !(actual->IsBuffer() || actual->IsOutput() || actual->IsInout()))) {
                    // 'linkage' can be associated with any other port.

                    // This is officially an error, but too many designs violate it. So make it a warning.
                    actual->Warning("formal port %s of mode %s cannot be associated with actual port %s of mode %s",
                        id->Name(), EntityClassName(id->Mode()),
                        actual->Name(), EntityClassName(actual->Mode())) ;
                    id->Info("%s is declared here", id->Name()) ;
                }
            } else {
                // Check mode consistency (LRM 1.1.1.2) for this component-entity port assoc
                if ((id->IsInput()  && !(actual->IsInput() || actual->IsInout() || actual->IsBuffer())) ||
                    (id->IsOutput() && !(actual->IsInout() || actual->IsOutput())) ||
                    (id->IsInout()  && !(actual->IsInout())) ||
                    (id->IsBuffer() && !(actual->IsBuffer()))) {
                    // 'linkage' can be associated with any other port.

                    // This is officially an error, but too many designs violate it. So make it a warning.
                    actual->Warning("formal port %s of mode %s cannot be associated with actual port %s of mode %s",
                        id->Name(), EntityClassName(id->Mode()),
                        actual->Name(), EntityClassName(actual->Mode())) ;
                    id->Info("%s is declared here", id->Name()) ;
                }
            }

            // VIPER 1863 & 1472
            // For recursive instantiation get constraint from stack, as id has
            // instantiating unit's value/constraint
            constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id) : id->Constraint() ; // constraint of the formal.

            if (!constraint) continue ; // something bad happened in initialization

            Array* assoc_list = id->GetVerilogAssocList() ;
            if (assoc_list && id_type && IsVerilogModule()) {
                VhdlDiscreteRange *range_item ;
                unsigned k ;
                VhdlConstraint* curr_constr =  0 ;
                if (is_vl_type) {
                    curr_constr = constraint->ElementConstraint() ? constraint->ElementConstraint()->Copy() : 0 ;
                } else {
                    curr_constr = constraint->Copy() ;
                }
                if (curr_constr) {
                    VhdlDataFlow df(0, 0, _local_scope) ;
                    df.SetInInitial() ;
                    FOREACH_ARRAY_ITEM(assoc_list, k, range_item) {
                        if (!range_item) continue ;
                        (void) range_item->TypeInfer(0, 0, 0, 0) ;
                        VhdlConstraint* range_constraint = range_item->EvaluateConstraintInternal(&df,0) ;
                        curr_constr = new VhdlArrayConstraint(range_constraint, curr_constr) ;
                    }
                    if (id->Constraint()) {
                        id->SetConstraint(curr_constr) ;
                    } else if (id_constraints) {
                        delete constraint ;
                        (void) id_constraints->Insert(id, curr_constr) ;
                    }
                    constraint = curr_constr ;
                }
            }

            // Adjust constraint to the actual constraint (not the value)
            // This is to mimic the implicit association  formal<=actual.
            if (constraint->IsUnconstrained()) constraint->ConstraintBy(actual->Constraint()) ;

            // This is a case of one scalar and one array constraint
            // contrain the id_constraint by the actual here (This can happen for mixed language designs only)
            VhdlConstraint* actual_constraint = actual->Constraint() ;
            VhdlConstraint* id_constraint = constraint ;
            if (is_mixed_type_of_unit_length && actual_constraint && ((id_constraint->IsArrayConstraint() && !actual_constraint->IsArrayConstraint()) ||
                                                                      (!id_constraint->IsArrayConstraint() && actual_constraint->IsArrayConstraint()))) {
                if (id_constraint->IsArrayConstraint()) id_constraint = id_constraint->ElementConstraint() ;
                if (actual_constraint->IsArrayConstraint()) actual_constraint = actual_constraint->ElementConstraint() ;
            }
            // VIPER 2636 : Check the constraint of component against constraint of entity
            // VIPER #4547 : Check constraint of component's port against constraint of entity port, only when
            // actual is specified for this component port in instantiation :
            // Viper 7690: Modelsim errors out if the components port's size and entity (or module)'s port size
            // mismatch in lenghth. For Viper 4547 Modelsim issued warning, However when we replaced the
            // generic values with constants Modelsim showed the error. The modified testcase is attached to 4547.
            // Thus decided to disable 4547 specific fix.

            // If the user do not want to see the error for size mismatch in un-associated port they
            // can un-comment "actual->IsActualPresent()" condition to achieve that.

            // Now behavior of Verific is to issue error even if the ports are not associated

            if (IsStaticElab() && /*actual->IsActualPresent() &&*/ !id_constraint->CheckAgainst(actual_constraint, actual, 0)) continue ;

            // Get the value from the actual :
            // CHECK ME : can we 'take' the value ?
            // Viper #4457 : Added IsRtlElab check
            val = (IsRtlElab() && actual->Value()) ? actual->Value()->Copy() : 0 ;
            if (!val) continue ; // don't set a value, so initial value will be used.

            VhdlConstraint* actual_constr = actual->Constraint() ;
            if (is_mixed_type_of_unit_length) {
                if (!constraint->IsArrayConstraint() && actual_constr->IsArrayConstraint()) {
                    val = val->ToNonconst() ;
                }
                if (constraint->IsArrayConstraint() && !actual_constr->IsArrayConstraint()) {
                    val = val->ToComposite(1) ;
                }
            }

            // Issue 1858 : For inputs, do this check before formal net-assigns.
            // or else we will enter net-assign with illegal values.
            if (id->IsInput() && !val->CheckAgainst(constraint,actual, 0)) {
                delete val ;
                continue ;
            }

            // Check actual against constraint.
            // Issue 1858 : For outputs, do this check after formal net-assigns.
            // or else we will have cut-off scalar outputs already
            if (!id->IsInput() && !val->CheckAgainst(constraint,actual, 0)) {
                delete val ;
                continue ;
            }

            // VIPER 1863 & 1472 : begin
            if (id_values) { // Have recursion : put value to stack
                // This routine is called twice with same map 'id_values'. So, need to
                // delete old data from 'id_values' and use force insert
                VhdlValue *old_val = (VhdlValue*)id_values->GetValue(id) ; // Get old value
                (void) id_values->Insert(id, val, 1 /* force overwrite */) ;
                delete old_val ;
            } else {
            // VIPER 1863 & 1472 : end
                // Just plain value setting now.
                if (IsRtlElab()) {
                    id->SetValue(val) ;
                } else {
                    delete val ;
                }
            }
        }
    }
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #3368 : Iterate over all ports of entity and try to constrained unconstrained ports
    // from the size of associated actual width :
    unsigned port_count = _id->NumOfPorts() ;
    for(i = 0; i < port_count; i++) {
        id = _id->GetPortAt(i) ;
        if (!id) continue ;
        constraint = (id_constraints) ? (VhdlConstraint*)id_constraints->GetValue(id): id->Constraint() ;

        if (!constraint || !constraint->IsUnconstrained()) continue ; // something bad happened in initialization
        if (id->GetVerilogActualWidth()) constraint->ConstraintBy(id->GetVerilogActualWidth(), id) ;
    }
#endif

    // don't check for unbound ports in RTL elab on a top-level unit.
    // Will create ports when we decent to new netlist.
    // VIPER 1863 & 1472
    //if (!inst && IsRtlElab()) return ;

    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0, 0, _local_scope) ;
    df.SetInInitial() ;

    // Check InitialValues of the ports.
    if (inst || !IsRtlElab()) {
        FOREACH_ARRAY_ITEM(_port_clause,i,decl) {
            decl->InitialValues(inst, id_values ? 1: 0, &df, old_scope) ; // Wire initial values into 'inst' if needed.
        }
    }
    // VIPER 2334 : Reset the global scope
    _present_scope = old_scope ;
}

void
VhdlConfigurationDecl::AssociatePorts(Instance *inst, Array *actual_ports, VhdlIdDef *component)
{
    VERIFIC_ASSERT(_id) ;

    // Call the entity that the configuration configures
    VhdlIdDef *entity = _id->GetEntity() ;
    if (!entity) return ; // error ?
    VhdlPrimaryUnit *the_entity = entity->GetPrimaryUnit() ;
    if (!the_entity) return ; // error ?
    the_entity->AssociatePorts(inst, actual_ports, component) ;
}

void
VhdlPackageDecl::AssociatePorts(Instance * /*inst*/, Array * /*actual_ports*/, VhdlIdDef * /*component*/)
{
    // Don't do anything
}

void
VhdlContextDecl::AssociatePorts(Instance * /*inst*/, Array * /*actual_ports*/, VhdlIdDef * /*component*/)
{
    // Don't do anything
}

void VhdlArchitectureBody::AddVerilogBindInstance(VeriModuleInstantiation *module_instance)
{
    if (!module_instance) return ;
    if (!_bind_verilog_instances) _bind_verilog_instances = new Array(1) ;
    _bind_verilog_instances->InsertLast(module_instance) ;
}
void VhdlArchitectureBody::AddBindInstance(const char *inst_name, VhdlComponentInstantiationStatement *entity_instance)
{
    // VIPER 2678 : check if the instance names are free (in the scope) :
    // Also VIPER 2678 : if there is a an instance in the bind_instance that is already in the bind list,
    // then  we need to error, and replace old with new one.
    if (!entity_instance) return ;

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to Secondary Uhit scope

    // Create label id with 'instance_name'
    VhdlLabelId *label_id = new VhdlLabelId(Strings::save(inst_name)) ;
    // Declare created id to this scope
    if (!_local_scope->Declare(label_id)) { // Identifier 'instance_name' already declared
        delete label_id ; // Delete created label
        return ; // Failed to add instance
    }

    // Set label to newly created instance
    entity_instance->SetLabel(label_id) ;

    // Insert as last back pointer into the array. Use at elaboration time.
    if (IsRtlElab()) {
        if (!_bind_instances) _bind_instances = new Array() ;
        _bind_instances->InsertLast(entity_instance) ;
    } else {
        // Insert created instance to statement part
        if (!_statement_part) _statement_part = new Array(1) ;
        _statement_part->InsertLast(entity_instance) ;
    }
    _present_scope = save_scope ; // Pop present scope
}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
void VhdlEntityDecl::GetParameterFromGeneric(Map& parameters, Array * /*generics*/, VeriModule * /*mod*/, VhdlIdDef * /*component*/)
{
    VhdlIdDef *generic ;
    VhdlIdDef *unit = _id ;
    if (!unit) return ;
    char *image ;
    // Create a Verilog parameter value array from these generic actuals
    for (unsigned i = 0; i < unit->NumOfGenerics(); i++) {
        generic = unit->GetGenericAt(i) ;
        if (!generic) continue ;

        image = (generic->Value()) ? generic->Value()->ImageVerilog() : 0 ;
        if (!image) continue ;

        parameters.Insert(generic->OrigName(), image) ; // free 'value' fields later.
    }
}
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
void VhdlConfigurationDecl::GetParameterFromGeneric(Map& parameters, Array * generics, VeriModule * mod, VhdlIdDef *component)
{
    VhdlDiscreteRange *assoc, *actual_part ;
    char *image ;
    if ((!generics && !component) || !mod || !mod->IsConfiguration()) return ;
    // Create a Verilog parameter value array from these generic actuals
    // Viper 5491. Get the names of generics from the actual verilog module
    // or the vhdl entity
    Set *modules = mod->GetDesignModules() ;
    Set *units = mod->GetDesignUnits() ;
    Array param_names ;
    if (modules->Size() == 1) {
        SetIter si ;
        VeriModule *tmp_mod ;
        FOREACH_SET_ITEM(modules, si, &tmp_mod) {
            if (!tmp_mod) break ;
            Array *tmp_parameters = tmp_mod->GetParameters() ;
            VeriIdDef *veri_id ;
            unsigned i ;
            FOREACH_ARRAY_ITEM(tmp_parameters, i, veri_id) {
                if (!veri_id) continue ;
                param_names.Insert(veri_id->GetName()) ;
            }
        }
    }
    VhdlIdDef *generic = 0 ;
    if (units->Size() == 1) {
        SetIter si ;
        VhdlPrimaryUnit *vhdl_unit ;
        FOREACH_SET_ITEM(units, si, &vhdl_unit) {
            if (!vhdl_unit) break ;
            VhdlIdDef *id = vhdl_unit->Id() ;
            Array *unit_generics = id ? id->GetGenerics() : 0 ;
            unsigned i ;
            FOREACH_ARRAY_ITEM(unit_generics, i, generic) {
                if (!generic) continue ;
                param_names.Insert(generic->Name()) ;
            }
        }
    }
    delete modules ; modules = 0 ;
    delete units ; units = 0 ;
    if (generics) {
        VhdlIdDef *unit = _id ? _id->GetEntity(): 0 ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(generics, i, assoc) {
            if (!assoc || assoc->IsOpen()) continue ;

            const char *generic_name = 0 ;
            if (assoc->IsAssoc() ) {
                actual_part = assoc->ActualPart() ;
                if (!actual_part || actual_part->IsOpen()) continue ; // named assoc
                // Find the formal :
                generic = assoc->FindFormal() ; // named
            } else {
                generic = unit ? unit->GetGenericAt(i): 0 ;
                //generic = assoc->GetId() ;
                generic_name = (const char*)param_names.At(i) ;
            }

            if (!generic) continue ;

            image = (generic->Value()) ? generic->Value()->ImageVerilog() : 0 ;
            if (!image) continue ;
            parameters.Insert(generic_name ? generic_name : generic->OrigName(), image) ; // free 'value' fields later.
        }
    } else if (component) {
        // Create a Verilog parameter value array from these generic actuals
        for (unsigned i = 0; i < component->NumOfGenerics(); i++) {
            generic = component->GetGenericAt(i) ;
            if (!generic) continue ;

            image = (generic->Value()) ? generic->Value()->ImageVerilog() : 0 ;
            if (!image) continue ;

            parameters.Insert(generic->OrigName(), image) ; // free 'value' fields later.
        }
    }
}
#endif

