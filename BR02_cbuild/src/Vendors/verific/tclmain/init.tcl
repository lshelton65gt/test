puts "Hello, init.tcl has been executed"

###############################################################################
#
# Verific Design Automation Inc.
#    Tcl Initialization routines. 
#    This file gets automatically read from within the executable
#    First, Verific specific initializations
#
###############################################################################
# 
#
###############################################################################
#
# Now, normal Tcl initialization. 'unknown' procedure etc.
#
###############################################################################

# Dont allow autoloading for now.
set auto_noload 1 

set errorCode ""
set errorInfo ""

# Define a log command (which can be overwritten to log errors
# differently, specially when stderr is not available)

if {[info commands tclLog] == ""} {
    proc tclLog {string} {
        catch {puts stderr $string}
    }
}

# unknown --
# This procedure is called when a Tcl command is invoked that doesn't
# exist in the interpreter.  It takes the following steps to make the
# command available:
#
#   1. See if the command has the form "namespace inscope ns cmd" and
#      if so, concatenate its arguments onto the end and evaluate it.
#   2. See if the autoload facility can locate the command in a
#      Tcl script file.  If so, load it and execute it.
#   3. If the command was invoked interactively at top-level:
#       (a) see if the command exists as an executable UNIX program.
#       If so, "exec" the command.
#       (b) see if the command requests csh-like history substitution
#       in one of the common forms !!, !<number>, or ^old^new.  If
#       so, emulate csh's history substitution.
#       (c) see if the command is a unique abbreviation for another
#       command.  If so, invoke the command.
#
# Arguments:
# args -    A list whose elements are the words of the original
#       command, including the command name.

proc unknown args {
    global auto_noexec auto_noload env unknown_pending tcl_interactive
    global errorCode errorInfo

    # If the command word has the form "namespace inscope ns cmd"
    # then concatenate its arguments onto the end and evaluate it.

    set cmd [lindex $args 0]
    if {[regexp "^namespace\[ \t\n\]+inscope" $cmd] && [llength $cmd] == 4} {
        set arglist [lrange $args 1 end]
    set ret [catch {uplevel $cmd $arglist} result]
        if {$ret == 0} {
            return $result
        } else {
          return -code $ret -errorcode $errorCode $result
        }
    }

    # Save the values of errorCode and errorInfo variables, since they
    # may get modified if caught errors occur below.  The variables will
    # be restored just before re-executing the missing command.

    set savedErrorCode $errorCode
    set savedErrorInfo $errorInfo
    set name [lindex $args 0]
    if {![info exists auto_noload]} {
    #
    # Make sure we're not trying to load the same proc twice.
    #
    if {[info exists unknown_pending($name)]} {
        return -code error "self-referential recursion in \"unknown\" for command \"$name\"";
    }
    set unknown_pending($name) pending;
    set ret [catch {auto_load $name [uplevel 1 {namespace current}]} msg]
    unset unknown_pending($name);
    if {$ret != 0} {
        return -code $ret -errorcode $errorCode \
        "error while autoloading \"$name\": $msg"
    }
    if {![array size unknown_pending]} {
        unset unknown_pending
    }
    if {$msg} {
        set errorCode $savedErrorCode
        set errorInfo $savedErrorInfo
        set code [catch {uplevel 1 $args} msg]
        if {$code ==  1} {
        #
        # Strip the last five lines off the error stack (they're
        # from the "uplevel" command).
        #

        set new [split $errorInfo \n]
        set new [join [lrange $new 0 [expr {[llength $new] - 6}]] \n]
        return -code error -errorcode $errorCode \
            -errorinfo $new $msg
        } else {
        return -code $code $msg
        }
    }
    }

    if {([info level] == 1) && ([info script] == "") \
        && [info exists tcl_interactive] && $tcl_interactive} {
    if {![info exists auto_noexec]} {
        set new [auto_execok $name]
        if {$new != ""} {
        set errorCode $savedErrorCode
        set errorInfo $savedErrorInfo
        set redir ""
        if {[info commands console] == ""} {
            set redir ">&@stdout <@stdin"
        }
        return [uplevel exec $redir $new [lrange $args 1 end]]
        }
    }
    set errorCode $savedErrorCode
    set errorInfo $savedErrorInfo
    if {$name == "!!"} {
        set newcmd [history event]
    } elseif {[regexp {^!(.+)$} $name dummy event]} {
        set newcmd [history event $event]
    } elseif {[regexp {^\^([^^]*)\^([^^]*)\^?$} $name dummy old new]} {
        set newcmd [history event -1]
        catch {regsub -all -- $old $newcmd $new newcmd}
    }
    if {[info exists newcmd]} {
        tclLog $newcmd
        history change $newcmd 0
        return [uplevel $newcmd]
    }

    set ret [catch {set cmds [info commands $name*]} msg]
    if {[string compare $name "::"] == 0} {
        set name ""
    }
    if {$ret != 0} {
        return -code $ret -errorcode $errorCode \
        "error in unknown while checking if \"$name\" is a unique command abbreviation: $msg"
    }
    if {[llength $cmds] == 1} {
        return [uplevel [lreplace $args 0 0 $cmds]]
    }
    if {[llength $cmds] != 0} {
        if {$name == ""} {
        return -code error "empty command name \"\""
        } else {
        return -code error \
            "ambiguous command name \"$name\": [lsort $cmds]"
        }
    }
    }
    return -code error "invalid command name \"$name\""
}

# auto_execok --
#
# Returns string that indicates name of program to execute if 
# name corresponds to a shell builtin or an executable in the
# Windows search path, or "" otherwise.  Builds an associative 
# array auto_execs that caches information about previous checks, 
# for speed.
#
# Arguments: 
# name -            Name of a command.

if {[string compare $tcl_platform(platform) windows] == 0} {

# Windows version.
#
# Note that info executable doesn't work under Windows, so we have to
# look for files with .exe, .com, or .bat extensions.  Also, the path
# may be in the Path or PATH environment variables, and path
# components are separated with semicolons, not colons as under Unix.
#
proc auto_execok name {
    global auto_execs env tcl_platform

    if {[info exists auto_execs($name)]} {
    return $auto_execs($name)
    }
    set auto_execs($name) ""

    if {[lsearch -exact {cls copy date del erase dir echo mkdir md rename 
        ren rmdir rd time type ver vol} $name] != -1} {
    return [set auto_execs($name) [list $env(COMSPEC) /c $name]]
    }

    if {[llength [file split $name]] != 1} {
    foreach ext {{} .com .exe .bat} {
        set file ${name}${ext}
        if {[file exists $file] && ![file isdirectory $file]} {
        return [set auto_execs($name) [list $file]]
        }
    }
    return ""
    }

    set path "[file dirname [info nameof]];.;"
    if {[info exists env(WINDIR)]} {
    set windir $env(WINDIR) 
    }
    if {[info exists windir]} {
    if {$tcl_platform(os) == "Windows NT"} {
        append path "$windir/system32;"
    }
    append path "$windir/system;$windir;"
    }

    if {[info exists env(PATH)]} {
    append path $env(PATH)
    }

    foreach dir [split $path {;}] {
    if {$dir == ""} {
        set dir .
    }
    foreach ext {{} .com .exe .bat} {
        set file [file join $dir ${name}${ext}]
        if {[file exists $file] && ![file isdirectory $file]} {
        return [set auto_execs($name) [list $file]]
        }
    }
    }
    return ""
}

} else {

# auto_execok --
#
# Returns string that indicates name of program to execute if 
# name corresponds to an executable in the path. Builds an associative 
# array auto_execs that caches information about previous checks, 
# for speed.
#
# Arguments: 
# name -            Name of a command.

# Unix version.
#
proc auto_execok name {
    global auto_execs env

    if {[info exists auto_execs($name)]} {
    return $auto_execs($name)
    }
    set auto_execs($name) ""
    if {[llength [file split $name]] != 1} {
    if {[file executable $name] && ![file isdirectory $name]} {
        set auto_execs($name) [list $name]
    }
    return $auto_execs($name)
    }
    foreach dir [split $env(PATH) :] {
    if {$dir == ""} {
        set dir .
    }
    set file [file join $dir $name]
    if {[file executable $file] && ![file isdirectory $file]} {
        set auto_execs($name) [list $file]
        return $auto_execs($name)
    }
    }
    return ""
}

}
# auto_reset --
# Destroy all cached information for auto-loading and auto-execution,
# so that the information gets recomputed the next time it's needed.
# Also delete any procedures that are listed in the auto-load index
# except those defined in this file.
#
# Arguments: 
# None.

proc auto_reset {} {
    global auto_execs auto_index auto_oldpath
    foreach p [info procs] {
    if {[info exists auto_index($p)] && ![string match auto_* $p]
        && ([lsearch -exact {unknown pkg_mkIndex tclPkgSetup tcl_findLibrary
            tclMacPkgSearch tclPkgUnknown} $p] < 0)} {
        rename $p {}
    }
    }
    catch {unset auto_execs}
    catch {unset auto_index}
    catch {unset auto_oldpath}
}

