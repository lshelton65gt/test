###################################################
# Tcl Script to Create Standard Verilog Package Sdbs #
###################################################

# Have all sdb's, and their corresponding library
# directories, be outputted to ../verilog_packages/sdbs.
# NOTE: The directory ../verilog_packages/sdbs must exist
# prior to executing this script.
setveriloglibrarypath -default ../verilog_packages/sdbs

# Save system verilog's std package
#setveriloglibrarypath -associate std=../vhdl_packages/sdbs/std

# Save verilog std 
analyze -work std ../verilog_packages/std.sv
save -work std -module std 
