###################################################
# Tcl Script to Create Standard Vhdl Package Vdbs #
###################################################

# Have all vdb's, and their corresponding library
# directories, be outputted to ../vhdl_packages/ieee_1993/bin.
# NOTE: The directory ../vhdl_packages/ieee_1993/bin must exist
# prior to executing this script.
setvhdllibrarypath -default ../vhdl_packages/ieee_1993/bin

# Save VHDL standard 
analyze -work std ../vhdl_packages/ieee_1993/src/standard.vhd
save -work std -unit standard 
analyze -w std ../vhdl_packages/ieee_1993/src/textio.vhd
save -w std -u textio

# Save Synopsys attributes (read before ieee packages)
analyze -w synopsys ../vhdl_packages/ieee_1993/src/syn_attr.vhd
save -w synopsys -u ATTRIBUTES 

# Save IEEE packages
analyze -w ieee ../vhdl_packages/ieee_1993/src/std_1164.vhd
save -w ieee -u std_logic_1164 
analyze -w ieee ../vhdl_packages/ieee_1993/src/numeric_std.vhd
save -w ieee -u numeric_std 
analyze -w ieee ../vhdl_packages/ieee_1993/src/numeric_bit.vhd
save -w ieee -u numeric_bit 

# Save IEEE Vital packages
analyze -w ieee ../vhdl_packages/ieee_1993/src/timing_p.vhd
save -w ieee -u VITAL_Timing 
analyze -w ieee ../vhdl_packages/ieee_1993/src/timing_b.vhd
save -w ieee -u VITAL_Timing 
analyze -w ieee ../vhdl_packages/ieee_1993/src/prmtvs_p.vhd 
save -w ieee -u VITAL_Primitives 
analyze -w ieee ../vhdl_packages/ieee_1993/src/prmtvs_b.vhd 
save -w ieee -u VITAL_Primitives 

# Save Synopsys packages
analyze -w ieee ../vhdl_packages/ieee_1993/src/syn_arit.vhd
save -w ieee -u std_logic_arith 
analyze -w ieee ../vhdl_packages/ieee_1993/src/syn_unsi.vhd
save -w ieee -u STD_LOGIC_UNSIGNED 
analyze -w ieee ../vhdl_packages/ieee_1993/src/syn_sign.vhd
save -w ieee -u STD_LOGIC_SIGNED 
analyze -w ieee ../vhdl_packages/ieee_1993/src/syn_misc.vhd
save -w ieee -u std_logic_misc 
analyze -w ieee ../vhdl_packages/ieee_1993/src/syn_textio.vhd
save -w ieee -u std_logic_textio 

# Save Mentor Graphics packages
analyze -w arithmetic ../vhdl_packages/ieee_1993/src/mgc_arit.vhd
save -w arithmetic -u std_logic_arith 
analyze -w qsim_logic ../vhdl_packages/ieee_1993/src/mgc_qsim.vhd
save -w qsim_logic -u QSIM_logic 

# Save Mixed language type vl_types
analyze -work vl ../vhdl_packages/ieee_1993/src/mixed_lang_vltype.vhd
save -work vl -unit vl_types

# now make links for backward compatibility
cd ../vhdl_packages
exec ln -s ieee_1993/bin vdbs

