/*
 *
 * [ File Version : 1.76 - 2014/02/03 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

/* Main for Tcl based interactive console application. Windows version. */

#ifdef WINDOWS
#include <windows.h>
#endif

#include <locale.h>
#include <stdio.h>
#include <time.h>

#include "tcl.h"

#include "Strings.h"
#include "Commands.h"
#include "Message.h"  /* For log file */

#include "veri_file.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

static Commands *MyCommandsInit(Tcl_Interp *interp) ;
static int ComExec _ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc, const char *argv[])) ;
static unsigned check_time_security() ;

#ifdef WINDOWS
static void        setargv _ANSI_ARGS_((int *argcPtr, char ***argvPtr));
#endif

/*
 *----------------------------------------------------------------------
 *
 * main --
 *
 *    This is the main program for the application.
 *
 * Results:
 *    None: Tcl_Main never returns here, so this procedure never
 *    returns either.
 *
 * Side effects:
 *    Whatever the application does.
 *
 *----------------------------------------------------------------------
 */

// Need to make script_file static so that it's visible in ComExec().  This is
// needed for Tcl linefile reporting.
static const char *script_file = 0 ;

int
main(int argc, char **argv)
{
    /*
     * Set up the default locale to be standard "C" locale so parsing
     * is performed correctly.
     */

    setlocale(LC_ALL, "C");

    /* Check time-based security */
    if (!check_time_security()) {
        return 1 ;
    }

#ifdef WINDOWS
    /* Windoze need argument pre-processing */
    setargv(&argc, &argv);

    // (9/8/04) With the introduction of TCL 8.4.7, it was learned that the following
    // routine needs to be called before calling getFileAttributesExProc
    // or createHardLinkProc.  The TCL developers state if you don't call this
    // first, a crash will result.
    Tcl_FindExecutable(argv[0]) ;
#endif /* WINDOWS */

    //fprintf(stdout, "%s\n",argv[0]) ; // absolute path of exec.

    /* Set-up main line command line */
    Command *mainline = new Command(argv[0],0) ;
    mainline->Add(new StringArg("log_file","store messages in log file")) ;
    mainline->Add(new StringArg("script_file","execute Tcl script", 1, 1)) ;
    // VIPER #7772: Use -ve number as group so that both -file and -File can be used at the same time:
    mainline->Add(new ArrayArg("file", "execute Verilog -f command file", 1, -1)) ;
    mainline->Add(new ArrayArg("File", "execute Verilog -f command file", 1, -1)) ;
    mainline->Add(new BoolArg("version","version of Verific code")) ;

    /* Now parse the command line */
    if (!mainline->ParseArgs(argc, (const char**)argv)) { // cast to const char** needed to quiet compilers. Not sure why it's so picky.
        mainline->Usage() ;
        delete mainline ;
        return 2 ; /* Command line failed */
    }
    /* Check if we need to print help (usage) */
    if (mainline->BoolVal("help")) {
        mainline->Usage() ;
        delete mainline ;
        return 0 ;
    }
    unsigned ret_val = 0 ;

    /* Print message IDs */
    Message::PrintIds(1) ; // Default is on (1)

    /* Get version */
    if (mainline->BoolVal("version")) {
        // Report SW Release string and timestamp conversion (VIPER #2125)
        time_t t = Message::ReleaseDate() ;
        const char *release_string = Message::ReleaseString() ;
        if (!release_string) release_string = "N/A" ;
        Message::PrintLine("This code is dated being part of the Verific ", release_string, " release : ", ctime(&t)) ;
    }

    /* Get log file */
    const char *log_file = mainline->StringVal("log_file") ;
    if (log_file) Message::OpenLogFile(log_file) ;

    /* Get script file */
    script_file = mainline->StringVal("script_file") ;
    Array *f_file_names = mainline->ArrayVal("file") ;
    Array *F_file_names = mainline->ArrayVal("File") ;
    if (script_file) {
        /* Run a script file */
        /* Init interpreter with MyCommandsInit to get our commands */
        Tcl_Interp *interp = Tcl_CreateInterp() ;
        Commands *cmds = MyCommandsInit(interp) ;

        /* Now execute the script */
        if (Tcl_EvalFile(interp, script_file) != TCL_OK) {
            if (!Strings::compare(Tcl_GetStringResult(interp), "")) {
                // Need to save error message before calling Command::GetTclLineFile; result string gets overwritten
                char *error = Strings::save(Tcl_GetStringResult(interp)) ;
                Message::Error(Command::GetTclLineFile(interp), script_file, " failed: ", error) ;
                Strings::free(error) ;
            } else {
                Message::Error(Command::GetTclLineFile(interp), script_file, " failed") ;
            }
            ret_val = 2 ;
        }
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        Command::ResetLookupFile(script_file) ;
#endif
        // Memory leak fixes:
        Tcl_DeleteInterp(interp) ;
        delete cmds ;
    } else if (f_file_names || F_file_names) {
        /* Parse the -f file */
        // The tool that supports -f files works in Verilog 95 mode by default:
        //unsigned f_analysis_mode = veri_file::VERILOG_2K ;
        unsigned f_analysis_mode = veri_file::VERILOG_95 ;
        Array *files = 0 ;

        unsigned i ;
        const char *f_file ;
        FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
            Array *tmp = veri_file::ProcessFFile(f_file, veri_file::F_FILE_NONE, f_analysis_mode) ;
            if (!files) {
                files = tmp ;
            } else {
                files->Append(tmp) ;
                delete tmp ;
            }
        }

        FOREACH_ARRAY_ITEM(F_file_names, i, f_file) {
            Array *tmp = veri_file::ProcessFFile(f_file, veri_file::F_FILE_CAPITAL, f_analysis_mode) ;
            if (!files) {
                files = tmp ;
            } else {
                files->Append(tmp) ;
                delete tmp ;
            }
        }

        /* Aanalyze all the accumulated files: */
        unsigned analyzed = veri_file::AnalyzeMultipleFiles(files, f_analysis_mode) ;

        char *file ;
        FOREACH_ARRAY_ITEM(files, i, file) Strings::free(file) ;
        delete files ;

        if (analyzed) {
            /* Static elaborate: */
            Message::PrintLine("Static elaborating design") ;
            (void) veri_file::ElaborateAllStatic() ;
        }
    } else {
        /* Enter interactive loop */
        Tcl_Main(argc, argv, Tcl_AppInit);
    }

    Message::CloseLogFile() ;
    LineFile::ResetFileIdMaps() ; // mem-leaks..
    delete mainline ;

    return ret_val ;
}

/*
 *----------------------------------------------------------------------
 *
 * Tcl_AppInit --
 *
 *    This procedure performs application-specific initialization.
 *    Most applications, especially those that incorporate additional
 *    packages, will have their own version of this procedure.
 *
 * Results:
 *    Returns a standard Tcl completion code, and leaves an error
 *    message in Tcl_GetStringResult(interp) if an error occurs.
 *
 * Side effects:
 *    Depends on the startup script.
 *
 *----------------------------------------------------------------------
 */

/* Find the file init.tcl/history.tcl in the executable directory */
static const char init_script[] = "\n\
    set dir [file dirname [info nameofexecutable]]\n\
    set tclfile [file join $dir init.tcl]\n\
    if {[file exists $tclfile]} {\n\
        source $tclfile \n\
    }\n\
    set historyfile [file join $dir history.tcl]\n\
    if {[file exists $historyfile]} {\n\
        source $historyfile \n\
    }\n\
" ;

int
Tcl_AppInit(Tcl_Interp *interp)
{
    /* This routine called for interactive Tcl processes */

#if 0
    /* Don't want the init.tcl and other files */
    if (Tcl_Init(interp) == TCL_ERROR) {
    return TCL_ERROR;
    }
#endif

    /*
     * Call the init procedures for included packages.  Each call should
     * look like this:
     *
     * if (Mod_Init(interp) == TCL_ERROR) {
     *     return TCL_ERROR;
     * }
     *
     * where "Mod" is the name of the module.
     */

    if (!MyCommandsInit(interp)) {// == TCL_ERROR) {
        return TCL_ERROR ;
    }

    /*
     * Now execute the initialization script (tries to load init.tcl)
     */

    if (Tcl_Eval(interp, init_script) == TCL_ERROR) {
        if (!Strings::compare(Tcl_GetStringResult(interp), "")) {
            // Need to save error message before calling Command::GetTclLineFile; result string gets overwritten
            char *error = Strings::save(Tcl_GetStringResult(interp)) ;
            Message::Error(Command::GetTclLineFile(interp), "init_script failed: ", error) ;
            Strings::free(error) ;
        } else {
            Message::Error(Command::GetTclLineFile(interp), "init_script failed") ;
        }
        return TCL_ERROR ;
    }

    /*
     * Specify a user-specific startup file to invoke if the application
     * is run interactively.  Typically the startup file is "~/.apprc"
     * where "app" is the name of the application.  If this line is deleted
     * then no user-specific startup file will be run under any conditions.
     */

    Tcl_SetVar(interp, "tcl_rcFileName", "~/tclshrc.tcl", TCL_GLOBAL_ONLY);
    return TCL_OK;
}

#ifdef WINDOWS
/*
 *-------------------------------------------------------------------------
 *
 * setargv --
 *
 *    Parse the Windows command line string into argc/argv.  Done here
 *    because we don't trust the builtin argument parser in crt0.
 *    Windows applications are responsible for breaking their command
 *    line into arguments.
 *
 *    2N backslashes + quote -> N backslashes + begin quoted string
 *    2N + 1 backslashes + quote -> literal
 *    N backslashes + non-quote -> literal
 *    quote + quote in a quoted string -> single quote
 *    quote + quote not in quoted string -> empty string
 *    quote -> begin quoted string
 *
 * Results:
 *    Fills argcPtr with the number of arguments and argvPtr with the
 *    array of arguments.
 *
 * Side effects:
 *    Memory allocated.
 *
 *--------------------------------------------------------------------------
 */

static void
setargv(
    int *argcPtr,    /* Filled with number of argument strings. */
    char ***argvPtr  /* Filled with argument strings (malloc'd). */
)
{
    char *cmdLine, *p, *arg, *argSpace;
    char **argv;
    int argc, size, inquote, copy, slashes;

    cmdLine = GetCommandLine();

    /*
     * Precompute an overly pessimistic guess at the number of arguments
     * in the command line by counting non-space spans.
     */

    size = 2;
    for (p = cmdLine; *p != '\0'; p++) {
    if (isspace(*p)) {
        size++;
        while (isspace(*p)) p++;
        if (*p == '\0') break;
    }
    }
    argSpace = (char *) ckalloc((unsigned) (size * sizeof(char *) + Strings::len(cmdLine) + 1));
    argv = (char **) argSpace;
    argSpace += size * sizeof(char *);
    size--;

    p = cmdLine;
    for (argc = 0; argc < size; argc++) {
    argv[argc] = arg = argSpace;
    while (isspace(*p)) {
        p++;
    }
    if (*p == '\0') {
        break;
    }

    inquote = 0;
    slashes = 0;
    while (1) {
        copy = 1;
        while (*p == '\\') {
            slashes++;
            p++;
        }
        if (*p == '"') {
            if ((slashes & 1) == 0) {
                copy = 0;
                if ((inquote) && (p[1] == '"')) {
                    p++;
                    copy = 1;
                } else {
                    inquote = !inquote;
                }
            }
            slashes >>= 1;
        }
        while (slashes) {
            *arg = '\\';
            arg++;
            slashes--;
        }

        if ((*p == '\0') || (!inquote && isspace(*p))) {
            break;
        }
        if (copy != 0) {
            *arg = *p;
            arg++;
        }
        p++;
    }
    *arg = '\0';
    argSpace = arg + 1;
    }
    argv[argc] = NULL;

    *argcPtr = argc;
    *argvPtr = argv;
}
#endif /* WINDOWS */

static Commands *
MyCommandsInit(Tcl_Interp *interp)
{
    if (!interp) return 0 ; //TCL_ERROR ;

    /* Add my commands to the Tcl Interpreter */
    MapIter i ;
    Command *com ;
    Commands * commands = new Commands() ;
    FOREACH_COMMAND(commands,i,com) {
        Tcl_CreateCommand(interp, com->Name(), ComExec, (ClientData)commands, 0);
    }

    // Return the created commands. so that we can clean it up properly:
    return commands ; //TCL_OK ;
}

static int
ComExec(
    ClientData clientData,  /* We store 'interp's 'commands' in here */
    Tcl_Interp *interp,     /* Interpreter for evaluation. */
    int argc,               /* Number of arguments. */
    const char *argv[]      /* String values of arguments. */
)
{
    Commands *coms = (Commands*)clientData ;
    VERIFIC_ASSERT(coms) ;
    Command *com = coms->Get(argv[0]) ;
    VERIFIC_ASSERT(com) ; /* Or else our initialisation was not done correctly */

    // Set Tcl linefile information from interp/filename
    com->SetLineFile(Command::GetTclLineFile(interp, script_file)) ;

    if (!com->ParseArgs(argc,argv)) {
        com->Usage() ;
        return TCL_ERROR ;
    }

    /* Check if we need to print help (usage) */
    if (com->BoolVal("help")) {
        com->Usage() ;
        return TCL_OK ;
    }

    /* Execute the command */
    if (!com->Process(interp)) {
        if (!Strings::compare(Tcl_GetStringResult(interp), "")) {
            com->Error("%s: failed because %s", argv[0], Tcl_GetStringResult(interp)) ;
        } else {
            com->Error("%s: failed", argv[0]) ;
        }
        return TCL_ERROR ;
    }
    return TCL_OK ;
}

unsigned
check_time_security()
{
    Message::PrintLine("(c) Copyright 1999 - 2014 Verific Design Automation Inc. All rights reserved") ;
    /* Security checked and approved */
    return 1 ;
}

