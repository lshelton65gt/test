###################################################
# Tcl Script to Create Standard Vhdl Package Vdbs #
###################################################

# Have all vdb's, and their corresponding library
# directories, be outputted to vdbs_2008.
# NOTE: The directory vdbs_2008 must exist
# prior to executing this script.
setvhdllibrarypath -default ../vhdl_packages/ieee_2008/bin

# Save VHDL 2008 standard 
analyze -vhdl_2008 -work std ../vhdl_packages/ieee_2008/src/standard_2008.vhd
save -work std -unit standard 
analyze -vhdl_2008 -w std ../vhdl_packages/ieee_2008/src/textio_2008.vhd
save -w std -u textio
# Save 2008 'env' package
analyze -vhdl_2008 -w std ../vhdl_packages/ieee_2008/src/env_2008.vhd
save -w std -u env

# Save IEEE packages
################################################################################
# Because of the copyright that the IEEE has placed on these packages, 
# Verific Design Auromation Inc is not allowed to redistribute the source VHDL code.
#
# The source code is available at
# http://standards.ieee.org/downloads/1076/1076-2008/
#
# Verific does distribute VHDL 2008 IEEE packages only in 'vdb' (binary) form.
# You and your customers are allowed to ship and use the 'vdb' form in your product,
# but explicitly only as a 'read-only'. 
#
# IMPORTANT : Decompilation from these 'vdb' files back to human readable VHDL code 
# is not allowed without written permission from the IEEE Standards Department.
# 

# IEEE std_logic_1164 2008 package :
# analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/std_logic_1164.vhdl ../vhdl_packages/ieee_2008/src/std_logic_1164-body.vhdl
# RD: use synthesis-annotated form :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/std_logic_1164_2008.vhd 
save -w ieee -u std_logic_1164 

# IEEE std_logic_textio 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/std_logic_textio.vhdl
save -w ieee -u std_logic_textio 

# IEEE numeric_std 2008 package :
# analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/numeric_std.vhdl ../vhdl_packages/ieee_2008/src/numeric_std-body.vhdl
# RD: use synthesis-annotated form :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/numeric_std_2008.vhd 
save -w ieee -u numeric_std 

# IEEE numeric_std_unsigned 2008 package :
# analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/numeric_std_unsigned.vhdl ../vhdl_packages/ieee_2008/src/numeric_std_unsigned-body.vhdl
# RD: use synthesis-annotated form :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/numeric_std_unsigned_2008.vhd 
save -w ieee -u numeric_std_unsigned 

# IEEE numeric_bit 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/numeric_bit.vhdl ../vhdl_packages/ieee_2008/src/numeric_bit-body.vhdl
save -w ieee -u numeric_bit 

# IEEE numeric_bit_unsigned 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/numeric_bit_unsigned.vhdl ../vhdl_packages/ieee_2008/src/numeric_bit_unsigned-body.vhdl
save -w ieee -u numeric_bit_unsigned 

# IEEE match_real 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/math_real.vhdl ../vhdl_packages/ieee_2008/src/math_real-body.vhdl
save -w ieee -u math_real 

# IEEE match_complex 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/math_complex.vhdl ../vhdl_packages/ieee_2008/src/math_complex-body.vhdl
save -w ieee -u math_complex 

# IEEE fixed_float_types 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/fixed_float_types.vhdl 
save -w ieee -u fixed_float_types 

# IEEE fixed_generic_pkg 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/fixed_generic_pkg.vhdl ../vhdl_packages/ieee_2008/src/fixed_generic_pkg-body.vhdl
save -w ieee -u fixed_generic_pkg 

# IEEE float_generic_pkg 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/float_generic_pkg.vhdl ../vhdl_packages/ieee_2008/src/float_generic_pkg-body.vhdl
save -w ieee -u float_generic_pkg 

# IEEE fixed_pkg 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/fixed_pkg.vhdl
save -w ieee -u fixed_pkg 

# IEEE float_pkg 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/float_pkg.vhdl
save -w ieee -u float_pkg 

################################################################################

# IEEE ieee_bit_context 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/ieee_bit_context.vhd
save -w ieee -u ieee_bit_context

# IEEE ieee_std_context 2008 package :
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/ieee_std_context.vhd
save -w ieee -u ieee_std_context

# Older IEEE packages into library IEEE :
# Save IEEE Vital packages
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_1993/src/timing_p.vhd
save -w ieee -u VITAL_Timing 
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_1993/src/timing_b.vhd
save -w ieee -u VITAL_Timing 
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_1993/src/prmtvs_p.vhd 
save -w ieee -u VITAL_Primitives 
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_1993/src/prmtvs_b.vhd 
save -w ieee -u VITAL_Primitives 


# Save Synopsys packages
analyze -vhdl_2008 -w synopsys ../vhdl_packages/ieee_1993/src/syn_attr.vhd
save -w synopsys -u ATTRIBUTES 

analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_1993/src/syn_arit.vhd
save -w ieee -u std_logic_arith 
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_1993/src/syn_unsi.vhd
save -w ieee -u STD_LOGIC_UNSIGNED 
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_1993/src/syn_sign.vhd
save -w ieee -u STD_LOGIC_SIGNED 
analyze -vhdl_2008 -w ieee ../vhdl_packages/ieee_2008/src/syn_misc_2008.vhd
save -w ieee -u std_logic_misc 

# Save Mentor Graphics packages
analyze -vhdl_2008 -w arithmetic ../vhdl_packages/ieee_2008/src/mgc_arit_2008.vhd
save -w arithmetic -u std_logic_arith 
analyze -vhdl_2008 -w qsim_logic ../vhdl_packages/ieee_1993/src/mgc_qsim.vhd
save -w qsim_logic -u QSIM_logic 

# Save Mixed language type vl_types
analyze -vhdl_2008 -work vl ../vhdl_packages/ieee_1993/src/mixed_lang_vltype.vhd
save -work vl -unit vl_types

# now make links for backward compatibility
cd ../vhdl_packages
exec ln -s ieee_2008/bin vdbs_2008


