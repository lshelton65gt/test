/*
 *
 * [$Revision: 1.40 $ - $Date: 2014/01/02 18:35:01 $]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

/* 
    You need to have SWIG 1.3.25 or greater install in order
    to build swig.cpp correctly.  Please download at
    http://prdownloads.sourceforge.net/swig
*/

%module Tclswigapi
#pragma SWIG nowarn=201,315
%include "cpointer.i"
%pointer_cast(char *, void *, char_to_void);
%include "std_string.i"
   char *to_char(char *value);

%{

/* Include any container headers */

/* Include any util headers */
#include "TextBasedDesignMod.h"
#include "LineFile.h"

/* Include any vhdl headers */

/* Include any verilog headers */
#include "veri_file.h"
#include "VeriModule.h"


char *to_char(char *value)
{
   return (value);
}
%}

/* Ignore */
%ignore Verific::Message::MsgVaList;
%ignore Message::MsgVaList;


/* Include any container headers */

/* Include any util headers */
#include "TextBasedDesignMod.h"
#include "LineFile.h"

/* Include any vhdl headers */

/* Include any verilog headers */
#include "veri_file.h"
#include "VeriModule.h"


