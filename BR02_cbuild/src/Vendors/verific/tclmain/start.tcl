
cd ../vhdl_packages/ieee_1993/src

# VHDL standard 
analyze -w std standard.vhd
analyze -w std textio.vhd

# Synopsys attributes (read before ieee packages)
analyze -w synopsys syn_attr.vhd

# IEEE packages
analyze -w ieee std_1164.vhd
analyze -w ieee numeric_std.vhd
analyze -w ieee numeric_bit.vhd

# IEEE Vital packages
analyze -w ieee timing_p.vhd
analyze -w ieee timing_b.vhd
analyze -w ieee prmtvs_p.vhd 
analyze -w ieee prmtvs_b.vhd 

# Synopsys packages
analyze -w ieee syn_arit.vhd
analyze -w ieee syn_unsi.vhd
analyze -w ieee syn_sign.vhd
analyze -w ieee syn_misc.vhd
analyze -w ieee syn_textio.vhd

# Mentor Graphics packages
analyze -w arithmetic mgc_arit.vhd
analyze -w qsim_logic mgc_qsim.vhd

# Mixed language type vl_types
analyze -w vl mixed_lang_vltype.vhd

cd ..
