// -*-c++-*-
/***************************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONXSTRING_H_
#define _CARBONXSTRING_H_

/*!
  \file 
  Minimal replacement for UtString to be used by PCIE transactor, since
  UtString is not exposed from libcarbon.
*/

#include "carbon/carbon_shelltypes.h"
#include "util/c_memmanager.h"

class CarbonXString
{
public:
  CARBONMEM_OVERRIDES

  CarbonXString();
  CarbonXString(const char* str);
  CarbonXString(const CarbonXString& str);

  ~CarbonXString();

  const char* c_str() const;

  CarbonXString& operator=(const char* str);
  CarbonXString& operator=(const CarbonXString& str);

  CarbonXString& operator+=(const char* str);
  CarbonXString& operator+=(const CarbonXString& str);

private:
  char* mBuf;
  UInt32 mBufAlloc;
};

#endif
