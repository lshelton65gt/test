// -*-c++-*-
/***************************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONXARRAY_H_
#define _CARBONXARRAY_H_

/*!
  \file 
  Mimimal replacement for UtArray to be used by PCIE transactor, since
  UtArray is not exposed from libcarbon.
*/

#include "carbon/carbon_shelltypes.h"
#include "util/c_memmanager.h"
#include <cstring>

template <typename T>
class CarbonXArray
{
public:
  CARBONMEM_OVERRIDES

  //! Construct empty array
  CarbonXArray()
    : mArray(NULL), mSize(0), mCapacity(0) {}

  //! Construct array with size
  CarbonXArray(UInt32 size)
    : mArray(NULL), mSize(0), mCapacity(0)
  {
    resize(size);
  }

  //! Construct array with size and initial values
  CarbonXArray(UInt32 size, const T& val)
    : mArray(NULL), mSize(0), mCapacity(0)
  {
    resize(size, val);
  }

  CarbonXArray(const CarbonXArray& arr)
    : mArray(NULL), mSize(0), mCapacity(0)
  {
    *this = arr;
  }

  ~CarbonXArray()
  {
    if (mCapacity != 0) {
      CARBON_FREE_VEC(mArray, T, mCapacity);
    }
  }

  class iterator
  {
    friend class CarbonXArray;

  public:
    CARBONMEM_OVERRIDES

    iterator() : mPtr(NULL) {}
    iterator(const iterator& other) : mPtr(other.mPtr) {}
    ~iterator() {}

    iterator& operator=(const iterator& other)
    {
      mPtr = other.mPtr;
    }

    T& operator*() const
    {
      return *mPtr;
    }

    T* operator->() const
    {
      return mPtr;
    }

    iterator operator++()
    {
      ++mPtr;
      return *this;
    }

    bool operator==(const iterator& other)
    {
      return (mPtr == other.mPtr);
    }

    bool operator<(const iterator& other)
    {
      return (mPtr < other.mPtr);
    }

    bool operator>=(const iterator& other)
    {
      return (mPtr >= other.mPtr);
    }

    iterator operator+(int x)
    {
      return iterator(mPtr + x);
    }

  private:
    T* mPtr;

    // Only outer class can call this
    iterator(T* ptr) : mPtr(ptr) {}
  };

  CarbonXArray& operator=(const CarbonXArray& arr)
  {
    if (this != &arr) {
      if (mCapacity != 0) {
        CARBON_FREE_VEC(mArray, T, mCapacity);
      }
      mCapacity = arr.mCapacity;
      mSize = arr.mSize;
      mArray = CARBON_ALLOC_VEC(T, mCapacity);
      memcpy(mArray, arr.mArray, mCapacity * sizeof(T));
    }
    return *this;
  }

  void push_back(const T& data)
  {
    maybeResize(mSize + 1);
    mArray[mSize++] = data;
  }

  T& operator[](int index)
  {
    return mArray[index];
  }

  const T& operator[](int index) const
  {
    return mArray[index];
  }

  void clear()
  {
    // Don't free the array
    mSize = 0;
  }

  UInt32 size() const
  {
    return mSize;
  }

  void resize(UInt32 newSize)
  {
    maybeResize(newSize);
    // If size is increased, clear new elements
    if (newSize > mSize) {
      UInt32 numNewElems = newSize - mSize;
      memset(&mArray[mSize], 0, numNewElems * sizeof(T));
    }
    mSize = newSize;
  }

  void resize(UInt32 newSize, const T& val)
  {
    maybeResize(newSize);
    // If size is increased, init new elements
    while (mSize < newSize) {
      mArray[mSize] = val;
      ++mSize;
    }
  }

  iterator begin()
  {
    return iterator(mArray);
  }

  iterator end()
  {
    // Same as begin() if we're empty
    if (mSize == 0) {
      return begin();
    }
    return iterator(&mArray[mSize]);
  }

  iterator erase(iterator first, iterator last)
  {
    // Work with the underlying pointers
    T* writePtr = &(*first);
    T* readPtr = &(*last);
    T* endPtr = &(*end());
    // After collapsing the array, the first element after the deleted
    // sequence will be where the first element of the sequence is
    // now.  Save the pointer, because we need to return an iterator
    // for it.
    T* nextPtr = writePtr;
    if (last >= first) {
      // Update the size.  Remember, this erases [first, last)
      mSize -= (readPtr - writePtr);
      // Shift the remaining entries
      while (readPtr != endPtr) {
        *writePtr = *readPtr;
        ++writePtr;
        ++readPtr;
      }
    }

    return iterator(nextPtr);
  }

  bool operator==(const CarbonXArray& other)
  {
    if (mSize != other.mSize) {
      return false;
    }
    int cmp = memcmp(mArray, other.mArray, mSize * sizeof(T));
    return (cmp == 0);
  }

private:
  T* mArray;
  UInt32 mSize;
  UInt32 mCapacity;

  // Factor by which capacity is increased when needed
  static const UInt32 scCapFactor = 2;

  void maybeResize(UInt32 requestedSize)
  {
    if (requestedSize > mCapacity) {
      // Scale by capacity factor until it's large enough.  If the
      // capacity is currently zero the first attempted capacity will
      // be scCapFactor.
      UInt32 newCapacity = (mCapacity == 0) ? 1 : mCapacity;
      do {
        newCapacity *= scCapFactor;
      } while (requestedSize > newCapacity);

      // Alloc the new array
      T* newArray = CARBON_ALLOC_VEC(T, newCapacity);
      // If necessary, copy the old data and free the old array.
      if (mSize != 0) {
        memcpy(newArray, mArray, mSize * sizeof(T));
      }
      if (mCapacity != 0) {
        CARBON_FREE_VEC(mArray, T, mCapacity);
      }

      mArray = newArray;
      mCapacity = newCapacity;
    }
  }
};

#endif
