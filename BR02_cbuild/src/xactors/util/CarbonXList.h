// -*-c++-*-
/***************************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONXLIST_H_
#define _CARBONXLIST_H_

/*!
  \file 
  Mimimal replacement for UtDLList to be used by PCIE transactor, since
  UtDLList is not exposed from libcarbon.
*/

#include "carbon/carbon_shelltypes.h"
#include "util/c_memmanager.h"

template <typename T>
class CarbonXList
{
public:
  CARBONMEM_OVERRIDES

  CarbonXList()
  {
    init();
  }

  CarbonXList(const CarbonXList& other)
  {
    init();
    for (const_iterator iter = other.begin(); iter != other.end(); ++iter) {
      push_back(*iter);
    }
  }

  ~CarbonXList()
  {
    clear();
    delete mEnd;
    delete mRend;
  }

  class Node
  {
  public:
    CARBONMEM_OVERRIDES

    Node(const T& data)
      : mPrev(NULL), mNext(NULL), mData(data) {}

    // Dummy node
    Node()
      : mPrev(NULL), mNext(NULL) {}

    ~Node() {}

    Node* mPrev;
    Node* mNext;
    T mData;
  };

  class iterator
  {
    friend class CarbonXList;

  public:
    CARBONMEM_OVERRIDES

    iterator() : mPtr(NULL) {}
    iterator(const iterator& other) : mPtr(other.mPtr) {}
    iterator(Node* ptr) : mPtr(ptr) {}
    ~iterator() {}

    iterator& operator=(const iterator& other)
    {
      mPtr = other.mPtr;
      return *this;
    }

    T& operator*()
    {
      return mPtr->mData;
    }

    T* operator->()
    {
      return &(mPtr->mData);
    }

    iterator operator++()
    {
      mPtr = mPtr->mNext;
      return *this;
    }

    iterator operator--()
    {
      mPtr = mPtr->mPrev;
      return *this;
    }

    bool operator==(const iterator& other) const
    {
      return (mPtr == other.mPtr);
    }

    bool operator!=(const iterator& other) const
    {
      return (mPtr != other.mPtr);
    }

    Node* mPtr;
  };

  class const_iterator
  {
    friend class CarbonXList;

  public:
    CARBONMEM_OVERRIDES

    const_iterator() : mPtr(NULL) {}
    const_iterator(const const_iterator& other) : mPtr(other.mPtr) {}
    const_iterator(const iterator& other) : mPtr(other.mPtr) {}
    const_iterator(const Node* ptr) : mPtr(ptr) {}
    ~const_iterator() {}

    const_iterator& operator=(const const_iterator& other)
    {
      mPtr = other.mPtr;
      return *this;
    }

    const T& operator*() const
    {
      return mPtr->mData;
    }

    const T* operator->() const
    {
      return &(mPtr->mData);
    }

    const_iterator operator++()
    {
      mPtr = mPtr->mNext;
      return *this;
    }

    const_iterator operator--()
    {
      mPtr = mPtr->mPrev;
      return *this;
    }

    bool operator==(const const_iterator& other) const
    {
      return (mPtr == other.mPtr);
    }

    bool operator!=(const const_iterator& other) const
    {
      return (mPtr != other.mPtr);
    }

    const Node* mPtr;
  };

  void push_back(const T& data)
  {
    Node* n = new Node(data);
    insertBetween(n, mEnd->mPrev, mEnd);
    ++mSize;
  }

  void pop_back()
  {
    removeNode(mEnd->mPrev);
    --mSize;
  }

  T& back()
  {
    return mEnd->mPrev->mData;
  }

  const T& back() const
  {
    return mEnd->mPrev->mData;
  }

  void push_front(const T& data)
  {
    Node* n = new Node(data);
    insertBetween(n, mRend, mRend->mNext);
    ++mSize;
  }

  void pop_front()
  {
    removeNode(mRend->mNext);
    --mSize;
  }

  T& front()
  {
    return mRend->mNext->mData;
  }

  const T& front() const
  {
    return mRend->mNext->mData;
  }

  UInt32 size() const
  {
    return mSize;
  }

  bool empty() const
  {
    return (mSize == 0);
  }

  void splice(iterator iter, CarbonXList& other)
  {
    if (!other.empty()) {
      // Grab the list to splice
      Node* spliceFirst = other.mRend->mNext;
      Node* spliceLast = other.mEnd->mPrev;
      // Stitch the original list back together.  We can't just call
      // clear() because we're keeping the nodes allocated.
      other.mRend->mNext = other.mEnd;
      other.mEnd->mPrev = other.mRend;
      // Insert into our list before iter
      Node* after = iter.mPtr->mPrev;
      Node* before = iter.mPtr;
      insertAfter(spliceFirst, after);
      insertBefore(spliceLast, before);
      // Update sizes
      mSize += other.mSize;
      other.mSize = 0;
    }
  }

  iterator begin()
  {
    return iterator(mRend->mNext);
  }

  const_iterator begin() const
  {
    return const_iterator(mRend->mNext);
  }

  iterator end()
  {
    return iterator(mEnd);
  }

  const_iterator end() const
  {
    return const_iterator(mEnd);
  }

  void clear()
  {
    Node* p = mRend->mNext;
    while (p != mEnd) {
      Node* pNext = p->mNext;
      delete p;
      p = pNext;
    }
    // Fix pointers
    insertBefore(mRend, mEnd);
    mSize = 0;
  }

  iterator insert(iterator iter, const T& data)
  {
    Node* n = new Node(data);
    insertBetween(n, iter.mPtr->mPrev, iter.mPtr);
    ++mSize;
    return iterator(n);
  }

  iterator erase(iterator iter)
  {
    iterator next(iter.mPtr->mNext);
    removeNode(iter.mPtr);
    --mSize;
    return next;
  }

  iterator erase(iterator first, iterator last)
  {
    iterator iter = first;
    while (iter != last) {
      iter = erase(iter);
    }
    return last;
  }

private:
  Node* mEnd;   //!< Node representing the end() iterator
  Node* mRend;  //!< Node representing the rend() iterator
  UInt32 mSize;

  void init()
  {
    // Dummy end/rend node
    mEnd = new Node();
    mRend = new Node();
    insertBefore(mRend, mEnd);
    mSize = 0;
  }

  void insertBefore(Node* ins, Node* next)
  {
    ins->mNext = next;
    next->mPrev = ins;
  }

  void insertAfter(Node* ins, Node* prev)
  {
    ins->mPrev = prev;
    prev->mNext = ins;
  }

  void insertBetween(Node* ins, Node* prev, Node* next)
  {
    insertBefore(ins, next);
    insertAfter(ins, prev);
  }

  void removeNode(Node* n)
  {
    n->mPrev->mNext = n->mNext;
    n->mNext->mPrev = n->mPrev;
    delete n;
  }

  // Forbid
  CarbonXList& operator=(const CarbonXList& other);
};

#endif
