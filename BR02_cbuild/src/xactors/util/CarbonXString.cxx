// -*-c++-*-
/***************************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "CarbonXString.h"
#include <cstring>

CarbonXString::CarbonXString()
  : mBuf(NULL), mBufAlloc(0)
{
  *this = "";
}

CarbonXString::CarbonXString(const char* str)
  : mBuf(NULL), mBufAlloc(0)
{
  *this = str;
}

CarbonXString::CarbonXString(const CarbonXString& str)
  : mBuf(NULL), mBufAlloc(0)
{
  *this = str;
}

CarbonXString::~CarbonXString()
{
  if (mBufAlloc != 0) {
    CARBON_FREE_VEC(mBuf, char, mBufAlloc);
  }
}

const char* CarbonXString::c_str() const
{
  return mBuf;
}

CarbonXString& CarbonXString::operator=(const char* str)
{
  // For simplicity, always realloc buffer
  if (mBufAlloc != 0) {
    CARBON_FREE_VEC(mBuf, char, mBufAlloc);
  }
  mBufAlloc = strlen(str) + 1;
  mBuf = CARBON_ALLOC_VEC(char, mBufAlloc);
  strcpy(mBuf, str);

  return *this;
}

CarbonXString& CarbonXString::operator=(const CarbonXString& str)
{
  *this = str.c_str();
  return *this;
}

CarbonXString& CarbonXString::operator+=(const char* str)
{
  // For simplicity, always realloc buffer.  Unlike in operator=,
  // which is called from the contructor, we can be certain the buffer
  // has been allocated.
  UInt32 newAlloc = mBufAlloc + strlen(str);
  char* newBuf = CARBON_ALLOC_VEC(char, newAlloc);
  newAlloc = mBufAlloc + strlen(str);
  strcpy(newBuf, mBuf);
  strcat(newBuf, str);
  CARBON_FREE_VEC(mBuf, char, mBufAlloc);

  mBufAlloc = newAlloc;
  mBuf = newBuf;

  return *this;
}

CarbonXString& CarbonXString::operator+=(const CarbonXString& str)
{
  *this += str.c_str();
  return *this;
}
