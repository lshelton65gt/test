//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Transactor interface core module.
//
`timescale 1ns/1ns
//`define XIF_DEBUG 1
//
// Notes:
//	All outputs (put_*) must be combinatorial from the cclock, and
//	to be valid for the simulation version, must be valid before
//	the falling edge of the cclock.
//	All inputs (get_*) are valid from the falling edge of the cclock
// 	during which BFM_xrun deasserts.
//	There is a possible race between Xav deasserting and Xrun
//	asserting.  If xrun asserts after xav deasserts, the code
//	misses the transaction.  We could change this to allow Xav to
// 	stay on longer, perhaps until negedge cclock....
//	This is primarily a simulation only problem.  Under emulation 
//      the race would merely cause a glitch on the clock enable signal
//
//	An assumption was made: Configuration size < 256 bytes
//	   This was done by selecting config data vs status data using 
//	   address bit 8.  Obviously we could pick any other bit.
//	   perhaps 16 would be a better choice?
//
module XIF_core
       (BFM_reset,	// Reset the transactor from the HW side
	XIF_reset,	// Reset the transactor from the SW side (output)

	cclock,
	Interrupt,

        Xav,		// Transaction available
	Xrun,		// Transaction running

        get_operation,	// Transaction output (wrt test) parameters
        get_address,
        get_size,
        get_status,
        get_data,
	get_csdata,
	
        put_operation,	// Transaction input parameters
        put_address,
        put_size,
        put_status,
        put_data,
        put_csdata,

	put_cphwtosw,	// For H-side xtor: copy data from hw side to sw side
	put_cpswtohw,	//	This one is visa-versa above
	
	put_dwe,	// Dataram Write Enable
	gp_daddr	// Address into data memory, both put and get
       );
   
   // Due to limitations in parameter specs and other implementation
   // choices, this code can currently support only 32 or 64 bit data
   // widths as well as only 256 I{G/P}Dwid.  Other values will require
   // changes, mostly to the simulation version of the code.
   parameter DataWidth	= 32;
   parameter DataAdWid	= 14;
   parameter GCSwid    	= 32;		// Get CS Data width 2048 max
   parameter PCSwid     = 32;		// Put CS Data width 2048 max
   
   parameter GCSDefaultData      = 0;		// Default value of Get CS data.
	    
   output                  XIF_reset,
			   Xav;
   input 		   BFM_reset,
			   Xrun,
			   cclock,
			   Interrupt;
   
   output [31:0] 	   get_operation,
			   get_size,
			   get_status;
   output [63:0] 	   get_address;
   output [DataWidth-1:0]  get_data;
   output [GCSwid-1:0] 	   get_csdata;
   
   input [31:0] 	   put_operation,
			   put_status;
   
   input [23:0] 	   put_size;		// Notice that we have squished this down!!!
   
   input [63:0] 	   put_address;
   input [DataWidth-1:0]   put_data;
   input [PCSwid-1:0] 	   put_csdata;
   
   input 		   put_cphwtosw;	// For H-side xtor: copy data from hw side to sw side
   input 		   put_cpswtohw;	//	This one is visa-versa above
   input 		   put_dwe;
   
   input [DataAdWid-1:0]   gp_daddr;
   
`include "dvpf_parms.vh"
   
`protect
   // we do not need a c*rbon license here because this module is included
   // in multiple other transactors that have already checked licensing
   
   
   //      CAUTION: there is a potential race between this core and the
   //      calling BFM:  The BFM must set Xrun before #1 after the clock
   //      so that the code here in the core will catch the xrun on the
   //      first clock after Xav
   //
   //	There is a purposeful "glitch" in XAV every cycle that shows
   //      the arrival of each new opcode, even if it is a sequence of 
   //      NOP's
   //
   
   reg 				   Xav;
   
   reg 				   XIF_reset, resetlast;
   
   reg 				   TBP_reset_toggle;    // toggled by prep to trigger a reset
   reg 				   TBP_reset_r;		// before each test.
   
   reg [31:0] 			   get_operation,  operation,
				   get_size,       size,
				   get_status,     status,     int_status, rst_status,
				   get_int_status, get_rst_status;   
   reg [63:0] 			   get_address,    address,  
				   starttime,
				   endtime;
   
   reg [DataWidth-1:0] 		   get_data,       gword;
   reg [GCSwid-1:0] 		   get_csdata; 
   
   reg [31:0] 			   DVPF_id;
   
   reg [DataAdWid-1:0] 		   gp_daddrint, last_daddr;
   
   // Reset issues:
   //	reset must allow at least one cclock out to allow the BFM to reset itself
   //	  Current implementation enables cclock during reset.
   //	Resets from HW side must enable cclocks, disable transaction communication
   //	What do we have to do about sync'ing with SW on reset?
   // First transaction issues:
   //	s-side initiated transactor assumes BFM comes out of reset waiting for a transaction
   //		xrun resets to INACTIVE, get_operation resets to BFM_NXTREQ in BFM
   //	h-side initiated transactor needs to run until it decides to make a request:
   //		This happens by xrun reseting to ACTIVE
   //
   
   //wire [1023:0] cs_default_data = GCSDefaultData;
   
   initial begin
      // Register my existance
      DVPF_id       = -1;
      Xav           = 0;
      get_operation =  BFM_NOP;
      $tbp_register(DVPF_id, (GCSwid+31)/32, GCSDefaultData);

`ifdef XIF_DEBUG
      $display("Registered module: %m as id: %d", DVPF_id);
`endif
      
      if(DataWidth != 32 && DataWidth != 64) begin
	 tbp_error("Only 32 and 64 bit datawidths are supported.");
      end
   end
   
   always @(negedge cclock) begin
      
      if (put_dwe) begin
`ifdef XIF_DEBUG
	 $display("%m -- %t Writing: index: %h  Data: %h", $time, gp_daddr, put_data);
`endif
         tbp_put_data(gp_daddr, put_data);
      end
      
      if(BFM_reset | TBP_reset_r) begin
	 
	 // Initialize locals
	 TBP_reset_toggle = 0;
	 TBP_reset_r      = 0;
	 XIF_reset	  = 0;
	 starttime        = 0;
	 
	 // Reset CS register bits.
  	 get_csdata = GCSDefaultData;
	 
	 // Transaction bookkeeping.  Since we are reseting the transactor
	 // we do not care about the reset status.
	 resetlast     =  0;
	 endtime       =  $time;
	 starttime     =  $time;
	 
	 // Fake a NOP to allow one cclock cycle to pass such that
	 // the XIF reset is seen by the BFM.
	 get_operation =  BFM_NOP;
	 get_address   =  0;
	 get_size      =  0;
	 get_status    =  0;
	 
	 resetlast     = BFM_reset; // save the state of the reset input;

      end else begin
	 
	 // Wait for a transaction request from the BFM  (~XIF_xrun)
`ifdef XIF_DEBUG
	 $display("%m  %t Transaction loop: BFM_reset: %h  Xav: %h  Xrun: %h", $time, BFM_reset, Xav, Xrun);
`endif
	 if ((BFM_reset | Xrun) && !TBP_reset_r) begin // this will fall thru on zero time ops
	    Xav = 0;		// And provide hold time for Xav
	 
	 end else begin
	    
	    // Clear XIF_reset after finishing with the current transaction.
	    XIF_reset = 0;
	    
	    operation   = put_operation;
	    address     = put_address;
	    size	= put_size;
	    status      = put_status;
	    int_status  = Interrupt;
	    rst_status  = resetlast;
	    
            resetlast   =  0;
            endtime     =  $time;
	    
`ifdef XIF_DEBUG
	    $display("%t getting transaction  %m  %d", $time, DVPF_id);
`endif
            tbp_transport(operation,
      			  address,
      			  size,
      			  status,      // op modifier on the way in
      		          // op results on the way out
      			  int_status,  // Interrupt info to C side
      			  rst_status,  // Reset info to the C side
      			  starttime,
      			  endtime,
			  
      			  get_operation,
      			  get_address,
      			  get_size,
      			  get_status,      // op modifier on the way in
      		          // op results on the way out
      			  get_int_status,  // Interrupt info to C side
      			  get_rst_status, // Reset info to the C side
			  get_csdata);    // Configuration Data
	    
`ifdef XIF_DEBUG
	    $display("%t got transaction: %h %h   %m", $time, get_operation, get_size);
`endif
            starttime     =  $time;
	    
            last_daddr    = 0;   // Force block below to fetch getdata

	    TBP_reset_r   = (get_operation == TBP_RESET);
	    XIF_reset     = TBP_reset_r;
	 end
      end  // if tbp_reset_r
   end
      
   // These make the BFM think the tbp_get/put data bufers are a simple RAM
   //   This is pretty tricky to get the data written to the buffer before
   //   getting the next transaction: we need to do this at the beginning of 
   //   the cycle, not the end!  This is rife with places for failures!
   //
   // Note, for the following two always blocks see the notes on the equivalent
   // blocks in the emulation section regarding 32-bit word swizzling, especially
   // the comments about using get_size.
   //  Moved the contents of the "write" block up to the above
   // always @(negedge cclock) begin
   // end
   // The real ram is clocked on uclock, and followed by
   // a posedge cclock register. I cheated that detail out for this
   always @(posedge cclock) begin
    if(get_operation != BFM_NOP && gp_daddr !== last_daddr | Xav) begin // Only do this if there is some NEW data
       if ((gp_daddr < (get_size-1)/4 + 1) || Xav) begin
	  gp_daddrint = Xav ? 0 : gp_daddr;   // bypass mux for 1st cycle of a write op
	  last_daddr = gp_daddrint;
          tbp_get_data(gp_daddrint, gword);
	  get_data <= gword;
`ifdef XIF_DEBUG
  	  $display("%m -- %t getting data at address: %h -- %h", $time, gp_daddrint, gword);
`endif
       end else begin
	  // Don't whine: we get here by accident sometimes, so handle it silently...
	  // When a new, shorter op arrives, gp_daddr can become illegal until the
	  // BFM resets to the beginning of the next transaction
  	  // $display("%m  Illegal data buffer read: index: %d  size: %d", gp_daddr, size);
       end
    end
   end

   always @(put_csdata or DVPF_id) begin
      tbp_put_csdata(put_csdata);
   end
   
   task tbp_get_data;
      input [31:0] index;
      output[63:0] data;
      begin
	 if(DataWidth == 32)
	   $tbp_get_data32(DVPF_id, index, data[31:0]);
	 else
	   $tbp_get_data64(DVPF_id, index, data);
      end
   endtask

   task tbp_put_data;
      input [31:0]          index;
      input [63:0] data;
      begin
	 if(DataWidth == 32)
	   $tbp_put_data32(DVPF_id, index, data[31:0]);
	 else
	   $tbp_put_data64(DVPF_id, index, data);
      end
   endtask

   task tbp_put_csdata;
      input [2047:0] csdata;
      begin
	 $tbp_put_cs_data(DVPF_id, (PCSwid+31)/32, csdata);
      end
   endtask
   
   task tbp_transport;
      input [31:0]        operation;
      input [63:0]        address;
      input [31:0]        size;
      input [31:0]        status;
      input [31:0]        int_status;
      input [31:0]        rst_status;
      input [63:0]        starttime;
      input [63:0]        endtime;
      
      output[31:0]        get_operation;
      output[63:0]        get_address;
      output[31:0]        get_size;
      output[31:0]        get_status;
      output[31:0]        get_int_status;
      output[31:0]        get_rst_status;
      output [2047:0]     get_csdata;
      begin
         $tbp_transport(DVPF_id,
      			operation,
      			address,
      			size,
      			status,      // op modifier on the way in
      		        // op results on the way out
      			int_status,  // Interrupt info to C side
      			rst_status,  // Reset info to the C side
      			starttime,
      			endtime,
			
      			get_operation,
      			get_address,
      			get_size,
      			get_status,      // op modifier on the way in
      		        // op results on the way out
      			get_int_status,  // Interrupt info to C side
      			get_rst_status); // Reset info to the C side

	 // Get the latest CS Data from the C-side
	 $tbp_get_cs_data(DVPF_id, (GCSwid+31)/32, get_csdata);

	 // Mark that we got a transaction
	 Xav = 1;
	 
      end
   endtask

   task tbp_error;
      input [0:399] msg;
      $tbp_error(msg);
   endtask // tbp_error

`endprotect
   
endmodule

//
// End of file XIF_core.v.
//
