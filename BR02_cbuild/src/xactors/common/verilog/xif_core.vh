//
// Transactor interface include file.
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
`protect

// we do not need a c*rbon license here because this module is included
// in multiple other transactors that have already checked licensing


// Author: Joe Sestrich <sestrich@zaiqtech.com>
//
// Filename: xif_core.vh
//
// Created: Thu Apr 15 18:59:36 EST 2003
//
// $Id$
//

// pull in BFM opcode defines for calling code
`include "dvpf_parms.vh"

// Use `define to define the default GCS data so transactors only have
// to worry about it when they use non-0 default data (which is rare).
`ifdef XIF_GCS_DEFAULT_DATA
`else
`define XIF_GCS_DEFAULT_DATA 0
`endif

parameter [1023:0] GCSDefaultData = `XIF_GCS_DEFAULT_DATA;
 
// Undefine XIF_GCS_DEFAULT_DATA to avoid it carrying over to other
// transactors.
`undef XIF_GCS_DEFAULT_DATA

// BFM stuff
wire                       BFM_clock; 
wire                       BFM_reset; 		// From BFM
wire                       XIF_reset; 		// From XIF
wire                       XIF_xav;		// Handshake
wire                       BFM_xrun;		// Handshake
wire			   BFM_interrupt;

wire [31:0]                XIF_get_operation;
wire [63:0]    		   XIF_get_address;
wire [31:0]                XIF_get_size;
wire [31:0]                XIF_get_status;
wire [DataWidth-1:0]       XIF_get_data;
wire [GCSwid-1:0]          XIF_get_csdata;

wire [31:0]                BFM_put_operation;
wire [63:0]		   BFM_put_address;
wire [23:0]                BFM_put_size;
wire [31:0]                BFM_put_status;
wire [DataWidth-1:0]       BFM_put_data;
wire [PCSwid-1:0]          BFM_put_csdata;
wire			   BFM_put_cphwtosw;
wire			   BFM_put_cpswtohw;
wire[DataAdWid-1:0]	   BFM_gp_daddr;
wire                       BFM_put_dwe;

// Instantiation of the Communication device
XIF_core   #(DataWidth, DataAdWid, GCSwid, PCSwid, GCSDefaultData) XIFcore
       (
	.XIF_reset     (XIF_reset),
        .BFM_reset     (BFM_reset),
	.cclock	       (BFM_clock),
        .Xav	       (XIF_xav),
	.Xrun          (BFM_xrun),
	.Interrupt     (BFM_interrupt),
        
	
        .get_operation (XIF_get_operation),
        .get_address   (XIF_get_address),
        .get_size      (XIF_get_size),
        .get_status    (XIF_get_status),
        .get_data      (XIF_get_data),
	.get_csdata    (XIF_get_csdata),
	
        .put_operation (BFM_put_operation),
        .put_address   (BFM_put_address),
        .put_size      (BFM_put_size),
        .put_status    (BFM_put_status),
        .put_data      (BFM_put_data),
        .put_csdata    (BFM_put_csdata),

	.put_cphwtosw  (BFM_put_cphwtosw),
	.put_cpswtohw  (BFM_put_cpswtohw),

	.put_dwe       (BFM_put_dwe),
	.gp_daddr      (BFM_gp_daddr)
       ) /* synthesis syn_noprune=1 */;

`endprotect

//
// End of file xif_core.vh.
//
