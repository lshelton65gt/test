//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/* Codes determining operations for transactors */
/*   Note that the value 0 is NOT used */

/* 
 *  The values listed here are compiled into the TBP
 *  libraries in both the C-side and sim-side (PLI)
 *  The user can add to this list, but may not change
 *  these entries....
 */


parameter
	// These are values for the operation word
TBP_NOP     	= 1,
TBP_IDLE    	= 2,
TBP_SLEEP   	= 3,
TBP_READ    	= 4,
TBP_WRITE   	= 5,
TBP_CS_READ 	= 6,
TBP_CS_WRITE	= 7,
TBP_NXTREQ      = 8,
TBP_INT_DONE    = 9,
TBP_FREAD    	= 10,
TBP_FWRITE   	= 11,
TBP_WAKEUP      = 12,
TBP_CS_RMW      = 13,
TBP_RESET       = 14,

BFM_NOP      	= 1, 
BFM_IDLE     	= 2, 
BFM_SLEEP    	= 3, 
BFM_READ     	= 4, 
BFM_WRITE    	= 5, 
BFM_CS_READ  	= 6, 
BFM_CS_WRITE 	= 7, 
BFM_NXTREQ   	= 8, 
BFM_INT_DONE 	= 9, 
BFM_FREAD    	= 10,
BFM_FWRITE   	= 11,
BFM_WAKEUP      = 12,
BFM_CS_RMW      = 13,
BFM_RESET       = 14,

	// These are bit positions withing the int_status register
TBP_int_status_bit   = 0,
TBP_reset_status_bit = 1;

