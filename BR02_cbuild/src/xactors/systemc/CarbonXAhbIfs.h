// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxahbifs__
#define __carbonxahbifs__

#include "xactors/systemc/CarbonXIfs.h"
#include "xactors/ahb/carbonXAhbMaster.h"

class CarbonXAhbTransReqT : public CarbonXTransReqT {
 public:

  // Constructor
  CarbonXAhbTransReqT(UInt32 buf_size = 0) : CarbonXTransReqT(buf_size) {
  }
  
  //-/////////////////////////////////////////////////////////////////////////////
  // Set Abort/Continue on error. Based on this setting, a master will abort
  // a burst or continue when error response is received
  // abortOnError=1 =>abort on error, abortOnError=0 => continue on error
  //-/////////////////////////////////////////////////////////////////////////////
  void setAbortOnError(UInt32 abortOnError) {
    csMaskWrite(MASTER_CONFIG_REG, abortOnError<<23, 0x1<<23);
  }

  //-/////////////////////////////////////////////////////////////////////////////
  // Configure busy cycles. The first busy cycle will be inserted at the
  // insertAt cycle number, this will be repeated after 'repeatAfter' arg.
  // e.g. if insertAt=2 and repeatAfter=4, for a 16-beat burst the first
  // busy cycle will appear after the second transfer of the burst, and then
  // after every 4 transfer.
  //-/////////////////////////////////////////////////////////////////////////////
  void setBusyCycles (UInt32 insertAfter, UInt32 repeatAfter) {
    UInt32 busyField;
    
    // Fill in [31:24]={insertAfter,repeatAfter}
    busyField = ((insertAfter << 4) | repeatAfter) << 24;
    csMaskWrite(MASTER_CONFIG_REG, busyField, 0xff000000);
  }

  //-/////////////////////////////////////////////////////////////////////////////
  // Set HPROT value.
  // This affects ALL types of transfers
  // hprot: protection (as defined by AHB spec)         (Config bits [3:0])
  //                           3 2 1 0
  // ---------------+-+-+-----+-------+
  //  config reg    |-|-|- - -| hprot | 
  // ---------------+-+-+-----+-------+
  //-/////////////////////////////////////////////////////////////////////////////
  void setProtection (UInt32 hprot) {
    csMaskWrite(MASTER_CONFIG_REG, hprot & 0xf, 0xf); 
  }
  
  //-/////////////////////////////////////////////////////////////////////////////
  // Set Burst parameters (Use this with carbonXRead/Write)
  //
  // incr:  1: INCR or 0: WRAP burst.                   (Config bit 8)
  // lock:  1: Locked burst requested; 0: Don't Lock    (Config bit 7) 
  // hsize: 0: 8-bit; 1: 16-bit, 2: 32-bit transfer     (Config bits [6:4])
  //
  // This values are ONLY used by carbonXRead/Write.
  //                 8 7 6 5 4 3 2 1 0
  // ---------------+-+-+-----+-------+
  //  config reg    |I|L|size |- - - -|
  // ---------------+-+-+-----+-------+
  //-/////////////////////////////////////////////////////////////////////////////
  void setBurstConfig (UInt32 hsize, UInt32 incr, UInt32 lock) {
    UInt32 configReg, burstcfg;
    burstcfg = ((incr & 0x1) << 8) | ((lock & 0x1) << 7) | ((hsize & 0x7) << 4);
    csMaskWrite(MASTER_CONFIG_REG, burstcfg, 0x1f0);
  }

  //-///////////////////////////////////////////////////////////////////////////
  // N-bit BURST of 'beats' number of beats (transfers)                       //
  //                                                                          //
  // nbits   => Data width in bits per transfer (8-/16-/32-bit supported)     //
  // beats   => Number of beats (transfers) in a burst                        //
  // incr    => 1: INCR burst, 0: WRAP burst                                  //
  // lock    => 1: locked burst, 0: non locked burst                          //
  // address => Start address for the burst read                              //
  //-///////////////////////////////////////////////////////////////////////////
  void burstReadReq (UInt32 nbits, UInt32 beats, UInt32 incr, 
		     UInt32 lock, UInt32 address) {
    transaction(MASTER_AHB_OP, address, NULL, beats<<MGetHSize(nbits), 
		carbonXAhbMasterCtlCmd(MGetHSize(nbits),lock,incr,0), TBP_WIDTH_32);
  }

  //-///////////////////////////////////////////////////////////////////////////
  // N-bit BURST of 'beats' number of beats (transfers)                       //
  //                                                                          //
  // nbits   => Data width in bits per transfer (8-/16-/32-bit supported)     //
  // beats   => Number of beats (transfers) in a burst                        //
  // incr    => 1: INCR burst, 0: WRAP burst                                  //
  // lock    => 1: locked burst, 0: non locked burst                          //
  // address => Start address for the burst read                              //
  // data    => pointer to a block of at least 'beats' number of UInt32 words //
  // Returns number of beats actually written                                 //
  //-///////////////////////////////////////////////////////////////////////////
  void burstWrite (UInt32 nbits, UInt32 beats, UInt32 incr,
		   UInt32 lock, UInt32 address, UInt8 *wdata) {
    // Write to sim side.
    transaction(MASTER_AHB_OP, address, wdata, beats << MGetHSize(nbits), 
		carbonXAhbMasterCtlCmd(MGetHSize(nbits),lock,incr,1), TBP_WIDTH_32);
  }
  void burstWrite (UInt32 nbits, UInt32 beats, UInt32 incr,
		   UInt32 lock, UInt32 address, UInt16 *wdata) {
    // Write to sim side.
    transaction(MASTER_AHB_OP, address, reinterpret_cast<UInt8 *>(wdata), beats << MGetHSize(nbits), 
		carbonXAhbMasterCtlCmd(MGetHSize(nbits),lock,incr,1), TBP_WIDTH_32);
  }
  void burstWrite (UInt32 nbits, UInt32 beats, UInt32 incr,
		   UInt32 lock, UInt32 address, UInt32 *wdata) {
    // Write to sim side.
    transaction(MASTER_AHB_OP, address, reinterpret_cast<UInt8 *>(wdata), beats << MGetHSize(nbits), 
		carbonXAhbMasterCtlCmd(MGetHSize(nbits),lock,incr,1), TBP_WIDTH_32);
  }
 
  
private :
  
  // Provate constants
  static const UInt32 MASTER_CONFIG_REG = 0;
  static const UInt32 MASTER_AHB_OP     = 0x80000001;

  typedef struct {
    UInt32 rsvd1 :16; // 31:16: Reserved
    UInt32 rsvd2 :10; // 15:06: Reserved
    UInt32 size  :03; // 05:03: Transfer size 8-/16-/32-bit (upto 1024 allowed)
    UInt32 lock  :01; // 02   : 1=> locked transfer
    UInt32 incr  :01; // 01   : 1=> INCR burst, 0=> SINGLE or WRAP burst
    UInt32 write :01; // 00   : 1: Write, 0: Read
  } AHB_MasterCtl_T;

  //-/////////////////////////////////////////////////////////////////////////////
  // Local function to calculate AHB HSIZE encoding from nbits
  //-/////////////////////////////////////////////////////////////////////////////
  UInt32 MGetHSize (UInt32 nbits) {
    UInt32 size = UInt32(log(nbits/8)/log(2)); // Log base 2 of (nbits/8)
    return size;
  }
    
};  

#endif
