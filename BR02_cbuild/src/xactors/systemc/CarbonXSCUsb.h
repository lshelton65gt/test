// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __CarbonXSCUsb_h_
#define __CarbonXSCUsb_h_

#include <iostream>
#include <sstream>
#include "systemc.h"
#include "CarbonXSCSignal.h"
#include "xactors/usb/carbonXUsb.h"
#include "xactors/systemc/CarbonXSCUsbIfs.h"

/*!
 * \addtogroup Usb
 * @{
 */

/*!
 * \defgroup UsbSystemC USB  SystemC-Interface
 * \brief User Interface Classes and Types for the USB  transactor
 */

/*!
 * \addtogroup UsbSystemC
 * @{
 */

/*!
 * \file xactors/systemc/CarbonXSCUsb.h
 * \brief Definition of USB  SystemC Transactor Object
 *
 * This file provides the definition of USB  transactor class in 
 * SystemC and related SystemC USB signal class.
 */

//! Explicit template instantiations
template class CarbonXSCSignal<sc_uint<2> >;
template class CarbonXSCSignal<sc_uint<8> >;

//! CarbonXSCUsb class
/*!
 * The CarbonXSCUsb class is a base wrapper class for the C API functionality
 * provided by the Carbon USB transactor.
 *
 * This class is responsible for managing the C instance of the Carbon
 * USB transactor.
 */
class CarbonXSCUsb : public sc_module
{
public:

    //! SystemC Contructor Declaration
    SC_HAS_PROCESS(CarbonXSCUsb);

    //! \name SystemC Transaction Ports
    // @{
    //! Start New Transaction Export
    sc_export<CarbonXSCUsbStartTransactionIF> startTransExport;

    //! Response Port
    sc_port<CarbonXSCUsbReportTransactionIF>  reportTransPort;

    //! Response Port
    sc_port<CarbonXSCUsbResponseIF>           responsePort;

    // @}

    //! \name SystemC Signal Ports
    // @{
    //!Transactor Input SystemClock from System
    sc_in<bool>                  SystemClock;

    // @}

    //! Constructor
    /* param transactorName     Transactor name
     * param xtorType           Transactor type - Host, Device or OtgDevice
     * param config             Transactor configuration
     */
    CarbonXSCUsb(sc_module_name transactorName, CarbonXUsbType xtorType,
            CarbonXUsbInterfaceType interface, CarbonXUsbConfig *config) :
        sc_module(transactorName),
        SystemClock("SystemClock"),
        mConfig(createConfig(config, interface)),
        mXtorInst(carbonXUsbCreate(transactorName,
                                    xtorType,
                                    config,
                                    reportTransactionCB,
                                    setDefaultResponseCB,
                                    setResponseCB,
                                    refreshResponseCB,
                                    reportCommandCB,
                                    notifyCB,
                                    notify2CB,
                                    this)),
        mStartTransactionExport(mXtorInst)
    {
        init();
    }

    //! Destructor
    ~CarbonXSCUsb()
    {
        carbonXUsbDestroy(mXtorInst);
    }

    //! Start bus enumeration
    void enumerate()
    {
        carbonXUsbStartBusEnumeration(mXtorInst);
    }

    //! Create pipe
    CarbonXUsbPipe *createPipe(CarbonUInt7 deviceAddress,
            const CarbonUInt8 *epDescriptor)
    {
        return carbonXUsbCreatePipe(mXtorInst, deviceAddress, epDescriptor);
    }
    //! Get existing pipe count
    CarbonUInt32 getPipeCount() const
    {
        return carbonXUsbGetCurrentPipeCount(mXtorInst);
    }
    //! Get existing pipe array
    CarbonXUsbPipe * const *getPipes()
    {
        return carbonXUsbGetCurrentPipes(mXtorInst);
    }
    //! Destroy existing pipe
    void destroyPipe(CarbonXUsbPipe *pipe)
    {
        carbonXUsbDestroyPipe(mXtorInst, pipe);
    }

protected:

    //! StartTransaction export class
    /*!
     * Used to sc_export the Start Transaction Interface
     */
    class StartTransaction : public CarbonXSCUsbStartTransactionIF
    {
    public:
        //! Constructor
        StartTransaction(CarbonXUsbID xtor) : mXtorInst(xtor) {}

        //! destructor
        ~StartTransaction() {}

        //! Start New Transaction
        CarbonUInt32 startNewTransaction(CarbonXUsbTrans* trans)
        {
            return carbonXUsbStartNewTransaction(mXtorInst, trans);
        }

        //! Start New Command
        CarbonUInt32 startCommand(CarbonXUsbCommand command)
        {
            return carbonXUsbStartCommand(mXtorInst, command);
        }

    private:
        //! USB Transactor Instance Handle
        CarbonXUsbID mXtorInst;
    };

    //! Connect the System C interface signals to the xactor callbacks
    virtual void connect()
    {
        // Bind exports
        startTransExport(mStartTransactionExport);
    }

    //! Set up the sensitivities to run the xactor.
    void init()
    {
        SC_METHOD(evaluate);
#if SYSTEMC_VERSION <= 20050714 // For SystemC version 2.1.v1
        sensitive_pos << SystemClock;
#else // For SystemC version 2.2
        sensitive << SystemClock.pos();
#endif
    }

    //! Run a single cycle
    void evaluate()
    {
        carbonXUsbRefreshInputs(mXtorInst, sc_time_stamp().value());
        carbonXUsbEvaluate(mXtorInst, sc_time_stamp().value());
        carbonXUsbRefreshOutputs(mXtorInst, sc_time_stamp().value());
    }

    //! Set Default Response Callback Function
    static void setDefaultResponseCB(CarbonXUsbTrans* trans,
            void* stimulusInst)
    {
        CarbonXSCUsb* inst = reinterpret_cast<CarbonXSCUsb*>(stimulusInst);
        inst->responsePort->setDefaultResponse(trans);
    }

    //! Set Response Callback Function
    static void setResponseCB(CarbonXUsbTrans* trans, void* stimulusInst)
    {
        CarbonXSCUsb* inst = reinterpret_cast<CarbonXSCUsb*>(stimulusInst);
        inst->responsePort->setResponse(trans);
    }

    //! Refresh Response Callback Function
    static void refreshResponseCB(CarbonXUsbTrans* trans, void* stimulusInst)
    {
        CarbonXSCUsb* inst = reinterpret_cast<CarbonXSCUsb*>(stimulusInst);
        inst->responsePort->refreshResponse(trans);
    }

    //! Report Transaction Callback Function
    static void reportTransactionCB(CarbonXUsbTrans* trans,
            void* stimulusInst)
    {
        CarbonXSCUsb* inst = reinterpret_cast<CarbonXSCUsb*>(stimulusInst);
        inst->reportTransPort->reportTransaction(trans);
    }

    //! Report Command Callback Function
    static void reportCommandCB(CarbonXUsbCommand command, void* stimulusInst)
    {
        CarbonXSCUsb* inst = reinterpret_cast<CarbonXSCUsb*>(stimulusInst);
        inst->reportTransPort->reportCommand(command);
    }

    //! Error Notify Callback Function
    static void notifyCB(CarbonXUsbErrorType type, const char* msg,
            const CarbonXUsbTrans* trans, void* stimulusInst)
    {
        std::stringstream errStr;

        if (msg == NULL)
        {
            switch (type)
            {
                case CarbonXUsbError_NoMemory:
                    errStr << "Memory allocation error" << endl;
                    break;
                case CarbonXUsbError_ConnectionError:
                    errStr << "Carbon model connection error" << endl;
                    break;
                case CarbonXUsbError_ProtocolError:
                    errStr << "Protocol violation error" << endl;
                    break;
                case CarbonXUsbError_DeviceFull:
                    errStr << "All 127 devices are attached" << endl;
                    break;
                case CarbonXUsbError_NoBandwidth:
                    errStr << "No bandwidth is available" << endl;
                    break;
                case CarbonXUsbError_NoPeriodicOutTrans:
                    errStr << "Periodic OUT pipe is empty" << endl;
                    break;
                case CarbonXUsbError_InvalidDescriptor:
                    errStr << "Invalid descriptor provided" << endl;
                    break;
                case CarbonXUsbError_InvalidTrans:
                    errStr << "Invalid transaction provided" << endl;
                    break;
                case CarbonXUsbError_InternalError:
                default:
                    errStr << "Internal code error" << endl;
            }
            SC_REPORT_ERROR("/Carbon/USB", errStr.str().c_str());
        }
        else
            SC_REPORT_ERROR("/Carbon/USB", msg);
    }

    //! Error Notify2 Callback Function
    static void notify2CB(CarbonXUsbErrorType type, const char* msg,
            void* stimulusInst)
    {
        std::stringstream errStr;

        if (msg == NULL)
        {
            switch (type)
            {
                case CarbonXUsbError_NoMemory:
                    errStr << "Memory allocation error" << endl;
                    break;
                case CarbonXUsbError_ConnectionError:
                    errStr << "Carbon model connection error" << endl;
                    break;
                case CarbonXUsbError_ProtocolError:
                    errStr << "Protocol violation error" << endl;
                    break;
                case CarbonXUsbError_DeviceFull:
                    errStr << "All 127 devices are attached" << endl;
                    break;
                case CarbonXUsbError_NoBandwidth:
                    errStr << "No bandwidth is available" << endl;
                    break;
                case CarbonXUsbError_NoPeriodicOutTrans:
                    errStr << "Periodic OUT pipe is empty" << endl;
                    break;
                case CarbonXUsbError_InvalidDescriptor:
                    errStr << "Invalid descriptor provided" << endl;
                    break;
                case CarbonXUsbError_InvalidTrans:
                    errStr << "Invalid transaction provided" << endl;
                    break;
                case CarbonXUsbError_InternalError:
                default:
                    errStr << "Internal code error" << endl;
            }
            SC_REPORT_ERROR("/Carbon/USB", errStr.str().c_str());
        }
        else
            SC_REPORT_ERROR("/Carbon/USB", msg);
    }

    //! Set proper interface in configuration
    static CarbonXUsbConfig *createConfig(CarbonXUsbConfig *config,
            CarbonXUsbInterfaceType interface)
    {
        if (config == NULL)
        {
            config = carbonXUsbConfigCreate();
            carbonXUsbConfigSetIntfType(config, interface);
            return config;
        }
        carbonXUsbConfigSetIntfType(config, interface);
        return NULL;
    }

    //! Configuration
    CarbonXUsbConfig *mConfig;

    //! Transactor instance
    CarbonXUsbID mXtorInst;

    //! Export StartTransaction IF
    StartTransaction mStartTransactionExport;
};

//! CarbonXSCUsbUtmi class
/*!
 * The CarbonXSCUsbUtmi class is a wrapper for the C API functionality
 * provided by the Carbon USB transactor having UTMI interface.
 */
class CarbonXSCUsbUtmi : public CarbonXSCUsb
{
public:

    //! SystemC Contructor Declaration
    SC_HAS_PROCESS(CarbonXSCUsbUtmi);

    //! \name SystemC Signal Ports
    // @{

    //!Transactor signals for UTMI interface

    // UTMI level 0 pins
    CarbonXSCSignal<bool>        Reset;
    CarbonXSCSignal<bool>        DataBus16_8;
    CarbonXSCSignal<bool>        SuspendM;
    CarbonXSCSignal<sc_uint<8> > DataInLo;
    CarbonXSCSignal<sc_uint<8> > DataInHi;
    CarbonXSCSignal<bool>        TXValid;
    CarbonXSCSignal<bool>        TXValidH;
    CarbonXSCSignal<bool>        TXReady;
    CarbonXSCSignal<sc_uint<8> > DataOutLo;
    CarbonXSCSignal<sc_uint<8> > DataOutHi;
    CarbonXSCSignal<bool>        RXValid;
    CarbonXSCSignal<bool>        RXValidH;
    CarbonXSCSignal<bool>        RXActive;
    CarbonXSCSignal<bool>        RXError;
    CarbonXSCSignal<bool>        XcvrSelect;
    CarbonXSCSignal<bool>        TermSelect;
    CarbonXSCSignal<sc_uint<2> > OpMode;
    CarbonXSCSignal<sc_uint<2> > LineState;

    // UTMI Level 1 pins
    CarbonXSCSignal<bool>        DpPulldown;
    CarbonXSCSignal<bool>        DmPulldown;
    CarbonXSCSignal<bool>        HostDisconnect;
    CarbonXSCSignal<bool>        IdPullup;
    CarbonXSCSignal<bool>        IdDig;
    CarbonXSCSignal<bool>        AValid;
    CarbonXSCSignal<bool>        BValid;
    CarbonXSCSignal<bool>        VbusValid;
    CarbonXSCSignal<bool>        SessEnd;
    CarbonXSCSignal<bool>        DrvVbus;
    CarbonXSCSignal<bool>        ChrgVbus;
    CarbonXSCSignal<bool>        DischrgVbus;

    // @}

    //! Constructor
    /* param transactorName     Transactor name
     * param xtorType           Transactor type - Host, Device or OtgDevice
     * param config             Transactor configuration
     */
    CarbonXSCUsbUtmi(sc_module_name transactorName, CarbonXUsbType xtorType,
            CarbonXUsbConfig *config) :
        CarbonXSCUsb(transactorName, xtorType,
                CarbonXUsbInterface_UTMI, config),
        Reset("Reset"),
        DataBus16_8("DataBus16_8"),
        SuspendM("SuspendM"),
        DataInLo("DataInLo"),
        DataInHi("DataInHi"),
        TXValid("TXValid"),
        TXValidH("TXValidH"),
        TXReady("TXReady"),
        DataOutLo("DataOutLo"),
        DataOutHi("DataOutHi"),
        RXValid("RXValid"),
        RXValidH("RXValidH"),
        RXActive("RXActive"),
        RXError("RXError"),
        XcvrSelect("XcvrSelect"),
        TermSelect("TermSelect"),
        OpMode("OpMode"),
        LineState("LineState"),
        DpPulldown("DpPulldown"),
        DmPulldown("DmPulldown"),
        HostDisconnect("HostDisconnect"),
        IdPullup("IdPullup"),
        IdDig("IdDig"),
        AValid("AValid"),
        BValid("BValid"),
        VbusValid("VbusValid"),
        SessEnd("SessEnd"),
        DrvVbus("DrvVbus"),
        ChrgVbus("ChrgVbus"),
        DischrgVbus("DischrgVbus")
    {
        connect();
    }

private:

    //! Connect the System C UTMI signals to the xactor callbacks
    void connect()
    {
        CarbonXInterconnectNameCallbackTrio trio_array[] =
        {
            { "Reset", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &Reset },
            { "DataBus16_8", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &DataBus16_8 },
            { "SuspendM", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &SuspendM },
            { "DataInLo", CarbonXSCSignal<sc_uint<8> >::write_callback,
                CarbonXSCSignal<sc_uint<8> >::read_callback, &DataInLo },
            { "DataInHi", CarbonXSCSignal<sc_uint<8> >::write_callback,
                CarbonXSCSignal<sc_uint<8> >::read_callback, &DataInHi },
            { "TXValid", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &TXValid },
            { "TXValidH", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &TXValidH },
            { "TXReady", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &TXReady },
            { "DataOutLo", CarbonXSCSignal<sc_uint<8> >::write_callback,
                CarbonXSCSignal<sc_uint<8> >::read_callback, &DataOutLo },
            { "DataOutHi", CarbonXSCSignal<sc_uint<8> >::write_callback,
                CarbonXSCSignal<sc_uint<8> >::read_callback, &DataOutHi },
            { "RXValid", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &RXValid },
            { "RXValidH", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &RXValidH },
            { "RXActive", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &RXActive },
            { "RXError", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &RXError },
            { "XcvrSelect", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &XcvrSelect },
            { "TermSelect", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &TermSelect },
            { "OpMode", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &OpMode },
            { "LineState", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &LineState },
            { "DpPulldown", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &DpPulldown },
            { "DmPulldown", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &DmPulldown },
            { "HostDisconnect", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &HostDisconnect },
            { "IdPullup", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &IdPullup },
            { "IdDig", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &IdDig },
            { "AValid", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &AValid },
            { "BValid", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &BValid },
            { "VbusValid", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &VbusValid },
            { "SessEnd", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &SessEnd },
            { "DrvVbus", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &DrvVbus },
            { "ChrgVbus", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &ChrgVbus },
            { "DischrgVbus", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &DischrgVbus },
            { 0, 0, 0, 0 }
        };
        carbonXUsbConnectToLocalSignalMethods(mXtorInst, trio_array);

        CarbonXSCUsb::connect();
    }
};

//! CarbonXSCUsbUlpi class
/*!
 * The CarbonXSCUsbUlpi class is a wrapper for the C API functionality
 * provided by the Carbon USB transactor having ULPI interface.
 */
class CarbonXSCUsbUlpi : public CarbonXSCUsb
{
public:

    //! SystemC Contructor Declaration
    SC_HAS_PROCESS(CarbonXSCUsbUlpi);

    //! \name SystemC Signal Ports
    // @{

    //!Transactor signals for ULPI interface
    CarbonXSCSignal<bool>        dir;
    CarbonXSCSignal<bool>        nxt;
    CarbonXSCSignal<bool>        stp;
    CarbonXSCRvBus<8>      data;

    // @}

    //! Constructor
    /* param transactorName     Transactor name
     * param xtorType           Transactor type - Host, Device or OtgDevice
     * param config             Transactor configuration
     */
    CarbonXSCUsbUlpi(sc_module_name transactorName, CarbonXUsbType xtorType,
            CarbonXUsbConfig *config) :
        CarbonXSCUsb(transactorName, xtorType,
                CarbonXUsbInterface_ULPI, config),
        dir("dir"),
        nxt("nxt"),
        stp("stp"),
        data("data")
    {
        connect();
    }

private:

    //! Connect the System C ULPI signals to the xactor callbacks
    void connect()
    {
        CarbonXInterconnectNameCallbackTrio trio_array[] =
        {
            { "dir", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &dir },
            { "nxt", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &nxt },
            { "stp", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &stp },
            { "data", CarbonXSCRvBus<8>::write_callback,
                CarbonXSCRvBus<8>::read_callback, &data },
            { 0, 0, 0, 0 }
        };
        carbonXUsbConnectToLocalSignalMethods(mXtorInst, trio_array);

        CarbonXSCUsb::connect();
    }
};

#endif
