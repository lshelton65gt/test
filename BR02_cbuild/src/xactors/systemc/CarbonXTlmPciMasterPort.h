// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxtlmpcimasterport__
#define __carbonxtlmpcimasterport__

#include "xactors/systemc/CarbonXTlmMasterPort.h"
#include "xactors/systemc/CarbonXPciIfs.h"

class CarbonXTlmPciMasterPort : public CarbonXTlmMasterPort {
  
public:
  
  CarbonXTlmPciMasterPort(const char* name) : 
    CarbonXTlmMasterPort(name) {
  }
  
  // Configuration space accesses (always 32 bit)
  void pciCfgWrite32(UInt64 address, UInt32 data) {
    mPciReq.pciCfgWriteReq(address, data);
    _pciPut();
    _block();
  }
  UInt32 pciCfgRead32(UInt64 address) {
    mPciReq.pciCfgReadReq(address);    
    _pciPut();
    _block();
    return *(reinterpret_cast<UInt32 *>(mResp.getData()));
  }

  //  Fixed sized memory accesses
  void   pciMemWrite32(UInt64 address, UInt32 data ) {
    mPciReq.pciMemWriteReq(address, &data, 4, false);
    _pciPut();
    _block();
  }  
  void   pciMemWrite16(UInt64 address, UInt16 data ){
    UInt32 data32 = data;
    mPciReq.pciMemWriteReq(address, &data32, 2, false);
    _pciPut();
    _block();
  }
  void   pciMemWrite8(UInt64 address, UInt8  data ){
    UInt32 data32 = data;
    mPciReq.pciMemWriteReq(address, &data32, 1, false);
    _pciPut();
    _block();
  }
  
  // The return values all gets cast to UInt32 independent on
  // the actual return type. This is because the data is
  // represented as UInt32 internally and this will guarantee
  // that there are no endianess problems
  UInt32 pciMemRead32(UInt64 address ){
    mPciReq.pciMemReadReq(address, 4, false);
    _pciPut();
    _block();
    return *(reinterpret_cast<UInt32 *>(mResp.getData()));
  }
  UInt16 pciMemRead16(UInt64 address ){
    mPciReq.pciMemReadReq(address, 2, false);
    _pciPut();
    _block();
    return *(reinterpret_cast<UInt32 *>(mResp.getData()));
  }
  UInt8  pciMemRead8(UInt64 address ){
    mPciReq.pciMemReadReq(address, 1, false);
    _pciPut();
    _block();
    return *(reinterpret_cast<UInt32 *>(mResp.getData()));
  }

  // Variable size 32 bit memory accesses
  void pciMemWrite(UInt64 address, const UInt32 * data, UInt32 size){
    mPciReq.pciMemWriteReq(address, data, size, false);
    _pciPut();
    _block();
  }
  void pciMemRead(UInt64 address, UInt32 * data, UInt32 size){
    mPciReq.pciMemReadReq(address, size, false);
    _pciPut();
    _block();
    UInt32 cpy_size = (size > mResp.getSize()) ? mResp.getSize() : size;
    cpy_size = ( (cpy_size + 3)/4) *4; // Copy full Uint32 words
    memcpy(data, mResp.getData(), cpy_size);    
  }

  // Variable sized 64 bit bus memory accesses
  //    These request 64-bit data width accesses but take 32-bit arrays of data
  void pci64MemWrite(UInt64 address, const UInt32 * data, UInt32 size){
    mPciReq.pciMemWriteReq(address, data, size, true);
    _pciPut();
    _block();
  }
  void pci64MemRead(UInt64 address, UInt32 * data, UInt32 size){
    mPciReq.pciMemReadReq(address, size, true);
    _pciPut();
    _block();
    UInt32 cpy_size = (size > mResp.getSize()) ? mResp.getSize() : size;
    cpy_size = ( (cpy_size + 3)/4) *4; // Copy full Uint32 words
    memcpy(data, mResp.getData(), cpy_size);    
  }

  // IO space accesses (always 32 bit)
  void pciIOWrite32(UInt32 address, UInt32 data, UInt32 req_id  = 0, UInt32 req_tag = 0) {
    mPciReq.pciIOWriteReq(address, data, 4, req_id, req_tag);
    _pciPut();
    _block();
  }
  UInt32 pciIORead32(UInt32 address, UInt32 req_id  = 0, UInt32 req_tag = 0) {
    mPciReq.pciIOReadReq(address, 4, req_id, req_tag);
    _pciPut();
    _block();
    return *(reinterpret_cast<UInt32 *>(mResp.getData()));
  }
    
  // Interrupt acknowledge
  UInt8  carbonXPciIntAck(){
    mPciReq.pciIntAckReq();
    _pciPut();
    _block();
  }
  
private:
  
  // PCI(x) Transaction Request Object
  CarbonXPciTransReqT mPciReq;

  inline void _pciPut()   { 
    mPutTransId = mPciReq.getTransId(); 
    (*this)->put(mPciReq); 
  }

};

#endif
