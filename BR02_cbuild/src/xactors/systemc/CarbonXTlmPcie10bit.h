// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef __carbonxtlmpcie10bit__
#define __carbonxtlmpcie10bit__

#include "xactors/systemc/CarbonXTlmPcieTransactor.h"

/*!
    \addtogroup PcieSystemC
    @{
*/

//! 10 bit Interface SystemC Transactor Module
/*!
  This class defines the SystemC signal ports for the 10 bit interface
  The class is templated with the link width of the PCI Express link.
  Legal link widths are: 1, 2, 4, 8, 12, 16 and 32. The signal ports
  uses the SystemC multi-port feature, so the ports need to be connected
  to the same number of channels that are specified by the template parameter.
  The Transaction side interface for the transactor is common for all signal
  interfaces and is defined by the CarbonXTlmPcieTransactor class which is a
  parent of this class.
*/
template<int LINKWIDTH = 1>
class CarbonXTlmPcie10bit : public CarbonXTlmPcieTransactor {
public:

  //! SystemC Contructor Declaration
  SC_HAS_PROCESS(CarbonXTlmPcie10bit);

  //! \name SystemC Signal Ports
  // @{

  //! Electrical Idle. Output One bit wide. One port per lane on the interace
  /*! 
    This is the Electrical Idle Output. Whether it's active high
    or low can be configured in the transactor.
  */
  sm_port<bool, LINKWIDTH>                           rxElecIdle;

  //! Data Output. Eight bits wide. One port per lane on the interace
  /*
    ! This is the 10bit data output bus from the transactor.
  */
  sm_port<sc_uint<10>, LINKWIDTH>                    rxData;

  //! Electrical Idle. Input One bit wide. One port per lane on the interace
  /*! 
    This is the Electrical Idle Input. Whether it's active high
    or low can be configured in the transactor.
  */
  sc_port<sc_signal_in_if<bool>, LINKWIDTH >         txElecIdle;

  //! Data Input. Eight bits wide. One port per lane on the interace
  /*
    ! This is the 10bit data input bus to the transactor.
  */
  sc_port<sc_signal_in_if<sc_uint<10> >, LINKWIDTH > txData;  

  // @}

  //! Constructor
  CarbonXTlmPcie10bit(sc_module_name instname) : 
    CarbonXTlmPcieTransactor(eCarbonXPcie10BitIntf, LINKWIDTH, instname) ,
    rxElecIdle(eCarbonXPcieRxElecIdle, mPcieInst, "rxElecIdle"),
    rxData(eCarbonXPcieRxData, mPcieInst, "rxData"),
    txElecIdle("txElecIdle"),
    txData("txData")
  {  
    init();
  }

private:
#ifndef CARBON_EXTERNAL_DOC

  //! Initializes SystemC module
  void init() {
    mInitialized = false;
    for(int i = 0; i < LINKWIDTH; i++) {
      mNetTxData[i]     = carbonXPcieGetNet(mPcieInst, eCarbonXPcieTxData, i);
      mNetTxElecIdle[i] = carbonXPcieGetNet(mPcieInst, eCarbonXPcieTxElecIdle, i);
    }

    SC_METHOD(processTransaction);
    sensitive << pcieRx.ok_to_get();

    SC_METHOD(sendTransaction);
    sensitive << pcieTx.ok_to_put();

    SC_METHOD(step);
  }

  //! Read all inputs every cycle
  void step() {
    if(mInitialized) {
      for(int i = 0; i < LINKWIDTH; i++) {
	carbonXPcieNetDeposit(mNetTxElecIdle[i], txElecIdle[i]->read()); 
	carbonXPcieNetDeposit(mNetTxData[i], txData[i]->read()); 
      }
      CarbonXTlmPcieTransactor::step();
    }
    else {
      // Drive Initial values
      for(int i = 0; i < LINKWIDTH; i++) {
        CarbonUInt32 eidle, data;
        carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxData, i), &data);
        carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxElecIdle, i), &eidle);
        rxElecIdle[i]->write(eidle);
        rxData[i]->write(data);
      }
      mInitialized = true;
    }
    next_trigger(mSymbolPeriod);
  }

  CarbonXPcieNet *mNetTxData[LINKWIDTH];
  CarbonXPcieNet *mNetTxElecIdle[LINKWIDTH];

  //! We don't want to call the model the first time around
  // (Can't use dont_initialize() when using next_trigger
  bool            mInitialized;

#endif
};
  
/*!
  @}
*/

#endif
