// -*- C++ -*-
/*******************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#ifndef __CarbonXSCUsbIfs_h_
#define __CarbonXSCUsbIfs_h_

#include "systemc.h"
#include "xactors/usb/carbonXUsb.h"

/*!
 * \addtogroup UsbSystemC
 * @{
 */

/*!
 * \file xactors/systemc/CarbonXSCUsbIfs.h
 * \brief Definition of USB SystemC Transaction Interface
 *
 * This file provides definition of USB transaction interface class
 * for StartTransaction, ReportTransaction and Response interfaces.
 */

//! USB SystemC StartTransaction Interface
class CarbonXSCUsbStartTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCUsbStartTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCUsbStartTransactionIF() {}

  /*!
   * This function can be used by the user to push a new transaction to
   * the transactor for initiation. 
   */
  virtual CarbonUInt32 startNewTransaction(CarbonXUsbTrans* trans) = 0;

  /*!
   * This function can be used by the user to start a timing command to
   * the transactor for initiation.
   */
  virtual CarbonUInt32 startCommand(CarbonXUsbCommand command) = 0;
};

//! USB SystemC Response Interface
class CarbonXSCUsbResponseIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCUsbResponseIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCUsbResponseIF() {}

  /*! 
   * This function is called back by the transactor to set the default 
   * response condition for a transaction. This function will be called
   * back from the receiving side as soon as the transaction is started.
   */
  virtual void setDefaultResponse(CarbonXUsbTrans* trans) = 0;

  /*!
   * This function is called back by the transactor to set the response
   * to a transaction. This is called on the receiving side once the full
   * transaction has been received.
   */
  virtual void setResponse(CarbonXUsbTrans* trans) = 0;

  //! This function will not be called in the USB transactor.
  void refreshResponse(CarbonXUsbTrans* trans) {}
};

//! USB SystemC ReportTransaction Interface
class CarbonXSCUsbReportTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCUsbReportTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCUsbReportTransactionIF() {}

  /*!
   * This function is called back by the transactor when a transaction push
   * is complete from the initiator side.
   */
  virtual void reportTransaction(CarbonXUsbTrans* trans) = 0;

  /*!
   * This function is called back by the transactor to indicate
   * completion of command to host or device transactor
   */
  virtual void reportCommand(CarbonXUsbCommand command) = 0;
};

#endif
