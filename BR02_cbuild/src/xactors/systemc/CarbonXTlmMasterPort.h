// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxtlmmasterport__
#define __carbonxtlmmasterport__

#include "systemc.h"
#include "tlm.h"
#include "CarbonXIfs.h"

class CarbonXTlmMasterPort : public sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT> > {
  
public:
  
  CarbonXTlmMasterPort(const char* name) : 
    sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT> >( name ) {
  }
  
  // Return Status of last Operation
  UInt32 getStatus() {
    return mStatus;
  }

  // Non Blocking
  void csWrite(UInt64 address, UInt64 data) {
    mReq.csWrite(address, data); _put();
  }
  
  // Blocking
  UInt64 csRead(UInt64 address) {
    mReq.csReadReq(address); 
    _put();
    _block();
    return *mResp.getData64();
  }
  void write8(UInt64 address, const UInt8 data) {
    mReq.write8(address, data); _put();    
  }
  void write16(UInt64 address, const UInt16 data) {
    mReq.write16(address, data); _put();        
  }
  void write32(UInt64 address, const UInt32 data) {
    mReq.write32(address, data); _put();        
  }
  void write64(UInt64 address, const UInt64 data) {
    mReq.write64(address, data); _put();        
  }
  void writeArray8(UInt64 address, const UInt8 *data, UInt32 size) {
    mReq.writeArray8(address, data, size); _put();        
  }
  void writeArray16(UInt64 address, const UInt16 *data, UInt32 size) {
    mReq.writeArray16(address, data, size); _put();        
  }
  void writeArray32(UInt64 address, const UInt32 *data, UInt32 size) {
    mReq.writeArray32(address, data, size); _put();        
  }
  void writeArray64(UInt64 address, const UInt64 *data, UInt32 size) {
    mReq.writeArray64(address, data, size); _put();        
  }
  UInt8 read8(UInt64 address) {
    mReq.read8Req(address); 
    _put();
    _block();
    return *mResp.getData8();    
  }
  UInt16 read16(UInt64 address) {
    mReq.read16Req(address); 
    _put();
    _block();
    return *mResp.getData16();        
  }
  UInt32 read32(UInt64 address) {
    mReq.read32Req(address); 
    _put();
    _block();
    return *mResp.getData32();        
  }
  UInt64 read64(UInt64 address) {
    mReq.read64Req(address); 
    _put();
    _block();
    return *mResp.getData64();        
  }
  UInt32 readArray8(UInt64 address, UInt8 *data, UInt32 size) {
    mReq.readArray8Req(address, size);
    _put();
    _block();
    UInt32 cpy_size = (size > mResp.getSize()) ? mResp.getSize() : size;
    memcpy(data, mResp.getData(), cpy_size);
    return mResp.getSize();
  }
  UInt32 readArray16(UInt64 address, UInt16 *data, UInt32 size) {
    mReq.readArray16Req(address, size);
    _put();
    _block();
    UInt32 cpy_size = (size > mResp.getSize()) ? mResp.getSize() : size;
    cpy_size = ( (cpy_size + 1)/2) *2; // Copy full Uint16 words
    memcpy(data, mResp.getData(), cpy_size);
    return mResp.getSize();    
  }
  UInt32 readArray32(UInt64 address, UInt32 *data, UInt32 size) {
    mReq.readArray32Req(address, size);
    _put();
    _block();
    UInt32 cpy_size = (size > mResp.getSize()) ? mResp.getSize() : size;
    cpy_size = ( (cpy_size + 3)/4) *4; // Copy full Uint32 words
    memcpy(data, mResp.getData(), cpy_size);
    return mResp.getSize();        
  }
  UInt32 readArray64(UInt64 address, UInt64 *data, UInt32 size) {
    mReq.readArray64Req(address, size);
    _put();
    _block();
    UInt32 cpy_size = (size > mResp.getSize()) ? mResp.getSize() : size;
    cpy_size = ( (cpy_size + 7)/8) *8; // Copy full Uint64 words
    memcpy(data, mResp.getData(), cpy_size);
    return mResp.getSize();            
  }

protected:
  
  inline void _get()   { 
    mResp       = (*this)->get(); 
    mStatus     = mResp.getStatus(); 
    mGetTransId = mResp.getTransId();
  }
  inline void _put()   { 
    mPutTransId = mReq.getTransId(); 
    (*this)->put(mReq); 
  }
  inline void _block() {
    do { 
      _get(); 
    } while (mGetTransId != mPutTransId);
  }

  CarbonXTransReqT    mReq;
  CarbonXTransRespT   mResp;
  
  UInt32              mStatus;
  UInt32              mPutTransId;
  UInt32              mGetTransId;
};

#endif
