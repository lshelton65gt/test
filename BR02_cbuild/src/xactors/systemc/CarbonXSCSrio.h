// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __CarbonXSCSrio_h_
#define __CarbonXSCSrio_h_

#include "systemc.h"
#include "CarbonXSCSignal.h"
#include "xactors/srio/carbonXSrio.h"
#include "xactors/systemc/CarbonXSCSrioIfs.h"

/*!
 * \addtogroup Srio
 * @{
 */

/*!
 * \defgroup SrioSystemC SRIO  SystemC-Interface
 * \brief User Interface Classes and Types for the SRIO  transactor
 */

/*!
 * \addtogroup SrioSystemC
 * @{
 */

/*!
 * \file xactors/systemc/CarbonXSCSrio.h
 * \brief Definition of SRIO  SystemC Transactor Object
 *
 * This file provides the definition of SRIO  transactor class in 
 * SystemC and related SystemC SRIO signal class.
 */

//! CarbonXSCSrio class
/*!
 * The CarbonXSCSrio class is a wrapper for the C API functionality
 * provided by the Carbon SRIO transactor.
 *
 * This class is responsible for managing the C instance of the Carbon
 * SRIO transactor.
 */
class CarbonXSCSrio : public sc_module
{
public:

    //! SystemC Contructor Declaration
    SC_HAS_PROCESS(CarbonXSCSrio);

    //! \name SystemC Transaction Ports
    // @{
    //! Start New Transaction Export
    sc_export<CarbonXSCSrioStartTransactionIF> startTransExport;

    //! Report Transaction Port
    sc_port<CarbonXSCSrioReportTransactionIF>  reportTransPort;

    //! Response Port
    sc_port<CarbonXSCSrioResponseIF>           responsePort;

    // @}

    //! \name SystemC Signal Ports
    // @{
    //!Transactor Input SystemClock from System
    sc_in<bool> SystemClock;

    //!Transactor Input RLane0 from Carbon Model
    CarbonXSCSignal<sc_uint<10> > RLane0;
    //!Transactor Input RLane1 from Carbon Model
    CarbonXSCSignal<sc_uint<10> > RLane1;
    //!Transactor Input RLane2 from Carbon Model
    CarbonXSCSignal<sc_uint<10> > RLane2;
    //!Transactor Input RLane3 from Carbon Model
    CarbonXSCSignal<sc_uint<10> > RLane3;

    //!Transactor Output Tlane0 to Carbon Model
    CarbonXSCSignal<sc_uint<10> > TLane0;
    //!Transactor Output Tlane1 to Carbon Model
    CarbonXSCSignal<sc_uint<10> > TLane1;
    //!Transactor Output Tlane2 to Carbon Model
    CarbonXSCSignal<sc_uint<10> > TLane2;
    //!Transactor Output Tlane3 to Carbon Model
    CarbonXSCSignal<sc_uint<10> > TLane3;
    // @}

    //! constructor
    CarbonXSCSrio(sc_module_name transactorName,
                  CarbonXSrioConfig *config,
                  CarbonUInt32 maxTransfers) :
        sc_module(transactorName),
        SystemClock("SystemClock"),
        RLane0("RLane0"),
        RLane1("RLane1"),
        RLane2("RLane2"),
        RLane3("RLane3"),
        TLane0("TLane0"),
        TLane1("TLane1"),
        TLane2("TLane2"),
        TLane3("TLane3"),
        mXtorInst(carbonXSrioCreate(transactorName,
                                      config,
                                      maxTransfers,
                                      reportTransactionCB,
                                      setDefaultResponseCB,
                                      setResponseCB,
                                      refreshResponseCB,
                                      notifyCB,
                                      notify2CB,
                                      this)),
        mStartExport(mXtorInst)
    {
        connect();
        init();
    }

    //! destructor
    ~CarbonXSCSrio()
    {
        carbonXSrioDestroy(mXtorInst);
    }

    //! Fills out the given configuration object with the transactor's
    //  current configuration.
    void getConfig(CarbonXSrioConfig *config)
    {
        carbonXSrioGetConfig(mXtorInst, config);
    }
private:

  //! StartTransaction export class
  /*!
   * Used to sc_export the Start Transaction Interface
   */
    class StartTransaction : public CarbonXSCSrioStartTransactionIF
    {
    public:
        //! Constructor
        StartTransaction(CarbonXSrioID xtor) : mXtorInst(xtor){}

        //! destructor
        ~StartTransaction() {}

        //! Start New Transaction
        CarbonUInt32 startNewTransaction(CarbonXSrioTrans* trans)
        {
            return carbonXSrioStartNewTransaction(mXtorInst, trans);
        }

    private:
        //! SRIO Transactor Instance Handle
        CarbonXSrioID mXtorInst;
    };


    //! Connect the System C signals to the xactor callbacks
    void connect()
    {
        CarbonXInterconnectNameCallbackTrio trio_array[] =
        {
            {"RLane0", 0, CarbonXSCSignal<sc_uint<10> >::read_callback, 
                &RLane0},
            {"RLane1", 0, CarbonXSCSignal<sc_uint<10> >::read_callback, 
                &RLane1},
            {"RLane2", 0, CarbonXSCSignal<sc_uint<10> >::read_callback, 
                &RLane2},
            {"RLane3", 0, CarbonXSCSignal<sc_uint<10> >::read_callback, 
                &RLane3},
            {"TLane0", CarbonXSCSignal<sc_uint<10> >::write_callback, 0, 
                &TLane0},
            {"TLane1", CarbonXSCSignal<sc_uint<10> >::write_callback, 0, 
                &TLane1},
            {"TLane2", CarbonXSCSignal<sc_uint<10> >::write_callback, 0, 
                &TLane2},
            {"TLane3", CarbonXSCSignal<sc_uint<10> >::write_callback, 0, 
                &TLane3},
            { 0, 0, 0, 0 }
        };
        carbonXSrioConnectToLocalSignalMethods(mXtorInst, trio_array);

        // Bind exports
        startTransExport(mStartExport);
    }

    //! Set up the sensitivities to run the xactor.
    void init()
    {
        SC_METHOD(evaluate);
        sensitive << SystemClock.pos();

        // NOTE: if there are any async paths through the xactor, then the 
        // xactor needs to set up async sensitivities to the async_evaluate 
        // function here.
        // SC_METHOD(async_evaluate);
        // sensitive << <async signals> ;
    }

    //! Run a single cycle
    void evaluate()
    {
        carbonXSrioRefreshInputs(mXtorInst, sc_time_stamp().value());
        carbonXSrioEvaluate(mXtorInst, sc_time_stamp().value());
        carbonXSrioRefreshOutputs(mXtorInst, sc_time_stamp().value());
    }

    //! Set Default Response Callback Function
    static void setDefaultResponseCB(CarbonXSrioTrans* trans,
          void* stimulusInst)
    {
        CarbonXSCSrio* inst = reinterpret_cast<CarbonXSCSrio*>(stimulusInst);
        inst->responsePort->setDefaultResponse(trans);
    }

    //! Refresh Response Callback Function
    static void refreshResponseCB(CarbonXSrioTrans* trans, void* stimulusInst)
    {
        CarbonXSCSrio* inst = reinterpret_cast<CarbonXSCSrio*>(stimulusInst);
        inst->responsePort->refreshResponse(trans);
    }

    
    //! Set Response Callback Function
    static void setResponseCB(CarbonXSrioTrans* trans, void* stimulusInst)
    {
        CarbonXSCSrio* inst = reinterpret_cast<CarbonXSCSrio*>(stimulusInst);
        inst->responsePort->setResponse(trans);
    }
    
    //! Report Transaction Callback Function
    static void reportTransactionCB(CarbonXSrioTrans* trans, 
            void* stimulusInst)
    {
        CarbonXSCSrio* inst = reinterpret_cast<CarbonXSCSrio*>(stimulusInst);
        inst->reportTransPort->reportTransaction(trans);
    }

    //! Error Notify Callback Function
    static void notifyCB(CarbonXSrioErrorType type, const char* msg, 
            CarbonXSrioTrans* trans, void* stimulusInst)
    {
        // CarbonXSCSrio* inst = reinterpret_cast<CarbonXSCSrio*>(stimulusInst);
        std::stringstream errStr;

        if(msg == NULL)
        {
            switch(type) {

                case CarbonXSrioError_NoMemory :
                    errStr << "An Out of Memory Error occured when processing "
                       << "transaction." << endl;
                    break;

                case CarbonXSrioError_InvalidTrans :
                    errStr << "An Invalid Transaction Error occured when " << 
                        "processing transaction." << endl;
                    
                case CarbonXSrioError_InternalError :
                default:
                    errStr << "**** INTERNAL ERROR ****" << endl;
                    break;
            }
            SC_REPORT_ERROR("/Carbon/SRIO", errStr.str().c_str());
        }
        else
            SC_REPORT_ERROR("/Carbon/SRIO", msg);
        
    }

    //! Error Notify2 Callback Function
    static void notify2CB(CarbonXSrioErrorType type, const char* msg,
            void* stimulusInst)
    {
        // CarbonXSCSrio* inst = reinterpret_cast<CarbonXSCSrio*>(stimulusInst);
        std::stringstream errStr;

        if(msg == NULL)
        {
            switch(type) {

                case CarbonXSrioError_NoMemory :
                    errStr << "An Out of Memory Error occured when processing"
                       << "transaction." << endl;
                    break;

                case CarbonXSrioError_InvalidTrans :
                    errStr << "An Invalid Transaction Error occured when" << 
                        "processing transaction." << endl;
                    
                case CarbonXSrioError_InternalError :
                default:
                    errStr << "**** INTERNAL ERROR ****" << endl;
                    break;
            }
            SC_REPORT_ERROR("/Carbon/SRIO", errStr.str().c_str());
        }
        else
            SC_REPORT_ERROR("/Carbon/SRIO", msg);
    }

    //! Handle evaluation of asynchronous paths
    /*!
    * For asyncs, the xactor will update its internal state immediately when RefreshInputs
    * is called.
    */
    //   void async_evaluate()
    //   {
    //     carbonXSrioRefreshInputs(mXtorInst, sc_time_stamp.value());
    //     carbonXSrioEvaluateAsync(mXtorInst, sc_time_stamp.value());
    //     carbonXSrioRefreshOutputs(mXtorInst, sc_time_stamp.value());
    //   }

    //! Transactor instance
    CarbonXSrioID mXtorInst;

    //! Export StartTransaction IF
    StartTransaction mStartExport;
};

/*!
 * @}
 */

/*!
 * @}
 */

#endif
