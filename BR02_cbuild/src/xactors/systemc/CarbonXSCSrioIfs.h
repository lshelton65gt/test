// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __CarbonXSCSrioIfs_h_
#define __CarbonXSCSrioIfs_h_

#include "systemc.h"
#include "xactors/srio/carbonXSrio.h"

/*!
 * \addtogroup SrioSystemC
 * @{
 */

/*!
 * \file xactors/systemc/CarbonXSCSrioIfs.h
 * \brief Definition of SRIO SystemC Transaction Interface
 *
 * This file provides definition of SRIO transaction interface class
 * for ReportTransaction and Response interfaces
 */

//! SRIO SystemC Response Interface
class CarbonXSCSrioResponseIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCSrioResponseIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCSrioResponseIF() {}

  /*! 
   * This function is called back by the transactor to set the default 
   * response condition for a transaction. This function will be called
   *  back from the receiving side as soon as the transaction is started.
   */
  virtual void setDefaultResponse(CarbonXSrioTrans* trans) = 0;

  /*!
   * This function is called back by the transactor to set the response
   * to a transaction. This is called on the receiving side once the full
   * transaction has been received.
   */
  virtual void setResponse(CarbonXSrioTrans* trans) = 0;

  //! This function will not be called in the SRIO transactor.
  void refreshResponse(CarbonXSrioTrans* trans) {}
};

//! SRIO SystemC StartTransaction Interface
class CarbonXSCSrioStartTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCSrioStartTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCSrioStartTransactionIF() {}

  /*!
   * This function can be used by the user to push a new transaction to
   * the transactor for initiation. 
   */
  virtual CarbonUInt32 startNewTransaction(CarbonXSrioTrans* trans) = 0;
};

//! SRIO SystemC ReportTransaction Interface
class CarbonXSCSrioReportTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCSrioReportTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCSrioReportTransactionIF() {}

  /*!
   * This function is called back by the transactor when a transaction pushed
   * is complete from the initiator side.
   */
  virtual void reportTransaction(CarbonXSrioTrans* trans) = 0;
};

/*!
 * @}
 */

#endif
