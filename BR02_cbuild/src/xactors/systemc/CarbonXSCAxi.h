// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __CarbonXSCAxi_h_
#define __CarbonXSCAxi_h_

#include "systemc.h"
#include "CarbonXSCSignal.h"
#include "xactors/axi/carbonXAxi.h"
#include "xactors/systemc/CarbonXSCAxiIfs.h"

/*!
 * \addtogroup Axi
 * @{
 */

/*!
 * \defgroup AxiSystemC AXI  SystemC-Interface
 * \brief User Interface Classes and Types for the AXI  transactor
 */

/*!
 * \addtogroup AxiSystemC
 * @{
 */

/*!
 * \file xactors/systemc/CarbonXSCAxi.h
 * \brief Definition of AXI  SystemC Transactor Object
 *
 * This file provides the definition of AXI  transactor class in 
 * SystemC and related SystemC AXI signal class.
 */

//! Explicit template instantiations
template class CarbonXSCSignal<bool>;
template class CarbonXSCSignal<sc_uint<2> >;
template class CarbonXSCSignal<sc_uint<3> >;
template class CarbonXSCSignal<sc_uint<4> >;
template class CarbonXSCSignal<sc_uint<32> >;
template class CarbonXSCSignal<sc_biguint<1> >;
template class CarbonXSCSignal<sc_biguint<2> >;
template class CarbonXSCSignal<sc_biguint<4> >;
template class CarbonXSCSignal<sc_biguint<8> >;
template class CarbonXSCSignal<sc_biguint<16> >;
template class CarbonXSCSignal<sc_biguint<32> >;
template class CarbonXSCSignal<sc_biguint<64> >;
template class CarbonXSCSignal<sc_biguint<128> >;
template class CarbonXSCSignal<sc_biguint<256> >;
template class CarbonXSCSignal<sc_biguint<512> >;
template class CarbonXSCSignal<sc_biguint<1024> >;

//! CarbonXSCAxi class
/*!
 * The CarbonXSCAxi class is a wrapper for the C API functionality
 * provided by the Carbon AXI transactor.
 *
 * This class is responsible for managing the C instance of the Carbon
 * AXI transactor.
 */
template<CarbonUInt32 _ID_WIDTH = 4, //! ID bus width
     CarbonXAxiAddressBusWidth _ADDRESS_WIDTH = CarbonXAxiAddressBusWidth_32,
                                     //! Address bus width
     CarbonXAxiDataBusWidth _DATA_WIDTH = CarbonXAxiDataBusWidth_32>
                                     //! Data bus width
class CarbonXSCAxi : public sc_module
{
public:

    //! SystemC Contructor Declaration
    SC_HAS_PROCESS(CarbonXSCAxi);

    //! \name SystemC Transaction Ports
    // @{
    //! Start New Transaction Export
    sc_export<CarbonXSCAxiStartTransactionIF> startTransExport;

    //! Report Transaction Port
    sc_port<CarbonXSCAxiReportTransactionIF>  reportTransPort;

    //! Response Port
    sc_port<CarbonXSCAxiResponseIF>           responsePort;

    // @}

    //! \name SystemC Signal Ports
    // @{
    //!Transactor Input SystemClock from System
    sc_in<bool>                               SystemClock;

    //!Transactor signal of address write(AW) channel
    CarbonXSCSignal<sc_uint<_ID_WIDTH> >      AWID;
    CarbonXSCSignal<sc_uint<_ADDRESS_WIDTH> > AWADDR;
    CarbonXSCSignal<sc_uint<4> >              AWLEN;
    CarbonXSCSignal<sc_uint<3> >              AWSIZE;
    CarbonXSCSignal<sc_uint<2> >              AWBURST;
    CarbonXSCSignal<sc_uint<2> >              AWLOCK;
    CarbonXSCSignal<sc_uint<4> >              AWCACHE;
    CarbonXSCSignal<sc_uint<3> >              AWPROT;
    CarbonXSCSignal<bool>                     AWVALID;
    CarbonXSCSignal<bool>                     AWREADY;
    
    //!Transactor signal of write(W) channel
    CarbonXSCSignal<sc_uint<_ID_WIDTH> >      WID;
    CarbonXSCBus<_DATA_WIDTH>                 WDATA;
    CarbonXSCBus<_DATA_WIDTH / 8 + (_DATA_WIDTH % 8 != 0)>
                                              WSTRB;
    CarbonXSCSignal<bool>                     WLAST;
    CarbonXSCSignal<bool>                     WVALID;
    CarbonXSCSignal<bool>                     WREADY;

    //!Transactor signal of write response(B) channel
    CarbonXSCSignal<sc_uint<_ID_WIDTH> >      BID;
    CarbonXSCSignal<sc_uint<2> >              BRESP;
    CarbonXSCSignal<bool>                     BVALID;
    CarbonXSCSignal<bool>                     BREADY;

    //!Transactor signal of address read(AR) channel
    CarbonXSCSignal<sc_uint<_ID_WIDTH> >      ARID;
    CarbonXSCSignal<sc_uint<_ADDRESS_WIDTH> > ARADDR;
    CarbonXSCSignal<sc_uint<4> >              ARLEN;
    CarbonXSCSignal<sc_uint<3> >              ARSIZE;
    CarbonXSCSignal<sc_uint<2> >              ARBURST;
    CarbonXSCSignal<sc_uint<2> >              ARLOCK;
    CarbonXSCSignal<sc_uint<4> >              ARCACHE;
    CarbonXSCSignal<sc_uint<3> >              ARPROT;
    CarbonXSCSignal<bool>                     ARVALID;
    CarbonXSCSignal<bool>                     ARREADY;

    //!Transactor signal of read(R) channel
    CarbonXSCSignal<sc_uint<_ID_WIDTH> >      RID;
    CarbonXSCBus<_DATA_WIDTH>                 RDATA;
    CarbonXSCSignal<sc_uint<2> >              RRESP;
    CarbonXSCSignal<bool>                     RLAST;
    CarbonXSCSignal<bool>                     RVALID;
    CarbonXSCSignal<bool>                     RREADY;
    // @}

    //! Constructor
    /* param transactorName     Transactor name
     * param xtorType           Transactor type - master or slave
     * param maxTransfers       Maximum number of simultaneous
     *                          transactions it can accept or process.
     */
    CarbonXSCAxi(sc_module_name transactorName,
                  CarbonXAxiType xtorType,
                  CarbonUInt32 maxTransfers) :
        sc_module(transactorName),
        SystemClock("SystemClock"),
        AWID("AWID"),
        AWADDR("AWADDR"),
        AWLEN("AWLEN"),    
        AWSIZE("AWSIZE"),        
        AWBURST("AWBURST"),        
        AWLOCK("AWLOCK"),        
        AWCACHE("AWCACHE"),        
        AWPROT("AWPROT"),        
        AWVALID("AWVALID"),        
        AWREADY("AWREADY"),        
        WID("WID"),
        WDATA("WDATA"),
        WSTRB("WSTRB"),
        WLAST("WLAST"),
        WVALID("WVALID"),
        WREADY("WREADY"),
        BID("BID"),
        BRESP("BRESP"),
        BVALID("BVALID"),
        BREADY("BREADY"),
        ARID("ARID"),
        ARADDR("ARADDR"),
        ARLEN("ARLEN"),
        ARSIZE("ARSIZE"),
        ARBURST("ARBURST"),
        ARLOCK("ARLOCK"),
        ARCACHE("ARCACHE"),
        ARPROT("ARPROT"),
        ARVALID("ARVALID"),
        ARREADY("ARREADY"),
        RID("RID"),
        RDATA("RDATA"),
        RRESP("RRESP"),
        RLAST("RLAST"),
        RVALID("RVALID"),
        RREADY("RREADY"),
        mConfig(createConfig()),
        mXtorInst(carbonXAxiCreate(transactorName,
                                      xtorType,
                                      mConfig,
                                      maxTransfers,
                                      reportTransactionCB,
                                      setDefaultResponseCB,
                                      setResponseCB,
                                      refreshResponseCB,
                                      notifyCB,
                                      notify2CB,
                                      this)),
        mStartExport(mXtorInst)
    {
        connect();
        init();
    }

    //! Destructor
    ~CarbonXSCAxi()
    {
        carbonXAxiConfigDestroy(mConfig);
        carbonXAxiDestroy(mXtorInst);
    }

    //! Fills out the given configuration object with the transactor's
    //  current configuration.
    void getConfig(CarbonXAxiConfig *config)
    {
        carbonXAxiGetConfig(mXtorInst, config);
    }
private:

    //! Creates configutaion object
    CarbonXAxiConfig *createConfig()
    {
        CarbonXAxiConfig *config = carbonXAxiConfigCreate();
        carbonXAxiConfigSetIDBusWidth(config, _ID_WIDTH);
        carbonXAxiConfigSetAddressBusWidth(config, _ADDRESS_WIDTH);
        carbonXAxiConfigSetDataBusWidth(config, _DATA_WIDTH);
        return config;
    }

  //! StartTransaction export class
  /*!
   * Used to sc_export the Start Transaction Interface
   */
    class StartTransaction : public CarbonXSCAxiStartTransactionIF
    {
    public:
        //! Constructor
        StartTransaction(CarbonXAxiID xtor) : mXtorInst(xtor){}

        //! destructor
        ~StartTransaction() {}

        //! Check space in transaction queue
        CarbonUInt32 isSpaceInTransactionBuffer(CarbonXAxiTransType transType)
        {
            return carbonXAxiIsSpaceInTransactionBuffer(mXtorInst, transType);
        }
        
        //! Start New Transaction
        CarbonUInt32 startNewTransaction(CarbonXAxiTrans* trans)
        {
            return carbonXAxiStartNewTransaction(mXtorInst, trans);
        }

    private:
        //! AXI Transactor Instance Handle
        CarbonXAxiID mXtorInst;
    };


    //! Connect the System C signals to the xactor callbacks
    void connect()
    {
        CarbonXInterconnectNameCallbackTrio trio_array[] =
        {
            {"AWID", CarbonXSCSignal<sc_uint<_ID_WIDTH> >::write_callback,
                CarbonXSCSignal<sc_uint<_ID_WIDTH> >::read_callback, &AWID},
            {"AWADDR",
                CarbonXSCSignal<sc_uint<_ADDRESS_WIDTH> >::write_callback,
                CarbonXSCSignal<sc_uint<_ADDRESS_WIDTH> >::read_callback,
                &AWADDR},
            {"AWLEN", CarbonXSCSignal<sc_uint<4> >::write_callback,
                CarbonXSCSignal<sc_uint<4> >::read_callback, &AWLEN},
            {"AWSIZE", CarbonXSCSignal<sc_uint<3> >::write_callback,
                CarbonXSCSignal<sc_uint<3> >::read_callback, &AWSIZE},
            {"AWBURST", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &AWBURST},
            {"AWLOCK", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &AWLOCK},
            {"AWCACHE", CarbonXSCSignal<sc_uint<4> >::write_callback,
                CarbonXSCSignal<sc_uint<4> >::read_callback, &AWCACHE},
            {"AWPROT", CarbonXSCSignal<sc_uint<3> >::write_callback,
                CarbonXSCSignal<sc_uint<3> >::read_callback, &AWPROT},
            {"AWVALID", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &AWVALID},
            {"AWREADY", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &AWREADY},
            {"WID", CarbonXSCSignal<sc_uint<_ID_WIDTH> >::write_callback,
                CarbonXSCSignal<sc_uint<_ID_WIDTH> >::read_callback, &WID},
            {"WDATA", CarbonXSCBus<_DATA_WIDTH>::write_callback,
                CarbonXSCBus<_DATA_WIDTH>::read_callback,
                &WDATA},
            {"WSTRB", CarbonXSCBus<_DATA_WIDTH / 8 +
                    (_DATA_WIDTH % 8 != 0)>::write_callback,
                CarbonXSCBus<_DATA_WIDTH / 8 +
                    (_DATA_WIDTH % 8 != 0)>::read_callback, &WSTRB},
            {"WLAST", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &WLAST},
            {"WVALID", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &WVALID},
            {"WREADY", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &WREADY},
            {"BID", CarbonXSCSignal<sc_uint<_ID_WIDTH> >::write_callback,
                CarbonXSCSignal<sc_uint<_ID_WIDTH> >::read_callback, &BID},
            {"BRESP", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &BRESP},
            {"BVALID", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &BVALID},
            {"BREADY", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &BREADY},
            {"ARID", CarbonXSCSignal<sc_uint<_ID_WIDTH> >::write_callback,
                CarbonXSCSignal<sc_uint<_ID_WIDTH> >::read_callback, &ARID},
            {"ARADDR",
                CarbonXSCSignal<sc_uint<_ADDRESS_WIDTH> >::write_callback,
                CarbonXSCSignal<sc_uint<_ADDRESS_WIDTH> >::read_callback,
                &ARADDR},
            {"ARLEN", CarbonXSCSignal<sc_uint<4> >::write_callback,
                CarbonXSCSignal<sc_uint<4> >::read_callback, &ARLEN},
            {"ARSIZE", CarbonXSCSignal<sc_uint<3> >::write_callback,
                CarbonXSCSignal<sc_uint<3> >::read_callback, &ARSIZE},
            {"ARBURST", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &ARBURST},
            {"ARLOCK", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &ARLOCK},
            {"ARCACHE", CarbonXSCSignal<sc_uint<4> >::write_callback,
                CarbonXSCSignal<sc_uint<4> >::read_callback, &ARCACHE},
            {"ARPROT", CarbonXSCSignal<sc_uint<3> >::write_callback,
                CarbonXSCSignal<sc_uint<3> >::read_callback, &ARPROT},
            {"ARVALID", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &ARVALID},
            {"ARREADY", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &ARREADY},
            {"RID", CarbonXSCSignal<sc_uint<_ID_WIDTH> >::write_callback,
                CarbonXSCSignal<sc_uint<_ID_WIDTH> >::read_callback, &RID},
            {"RDATA", CarbonXSCBus<_DATA_WIDTH>::write_callback,
                CarbonXSCBus<_DATA_WIDTH>::read_callback,
                &RDATA},
            {"RRESP", CarbonXSCSignal<sc_uint<2> >::write_callback,
                CarbonXSCSignal<sc_uint<2> >::read_callback, &RRESP},
            {"RLAST", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &RLAST},
            {"RVALID", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &RVALID},
            {"RREADY", CarbonXSCSignal<bool>::write_callback,
                CarbonXSCSignal<bool>::read_callback, &RREADY},
            { 0, 0, 0, 0 }
        };
        carbonXAxiConnectToLocalSignalMethods(mXtorInst, trio_array);

        // Bind exports
        startTransExport(mStartExport);
    }

    //! Set up the sensitivities to run the xactor.
    void init()
    {
        SC_METHOD(evaluate);
        sensitive << SystemClock.pos();
    }

    //! Run a single cycle
    void evaluate()
    {
        carbonXAxiRefreshInputs(mXtorInst, sc_time_stamp().value());
        carbonXAxiEvaluate(mXtorInst, sc_time_stamp().value());
        carbonXAxiRefreshOutputs(mXtorInst, sc_time_stamp().value());
    }

    //! Set Default Response Callback Function
    static void setDefaultResponseCB(CarbonXAxiTrans* trans,
          void* stimulusInst)
    {
        CarbonXSCAxi* inst = reinterpret_cast<CarbonXSCAxi*>(stimulusInst);
        inst->responsePort->setDefaultResponse(trans);
    }

    //! Refresh Response Callback Function
    static void refreshResponseCB(CarbonXAxiTrans* trans, void* stimulusInst)
    {
        CarbonXSCAxi* inst = reinterpret_cast<CarbonXSCAxi*>(stimulusInst);
        inst->responsePort->refreshResponse(trans);
    }

    
    //! Set Response Callback Function
    static void setResponseCB(CarbonXAxiTrans* trans, void* stimulusInst)
    {
        CarbonXSCAxi* inst = reinterpret_cast<CarbonXSCAxi*>(stimulusInst);
        inst->responsePort->setResponse(trans);
    }
    
    //! Report Transaction Callback Function
    static void reportTransactionCB(CarbonXAxiTrans* trans, 
            void* stimulusInst)
    {
        CarbonXSCAxi* inst = reinterpret_cast<CarbonXSCAxi*>(stimulusInst);
        inst->reportTransPort->reportTransaction(trans);
    }

    //! Error Notify Callback Function
    static void notifyCB(CarbonXAxiErrorType type, const char* msg, 
            const CarbonXAxiTrans* trans, void* stimulusInst)
    {
        // CarbonXSCAxi* inst = reinterpret_cast<CarbonXSCAxi*>(stimulusInst);
        std::stringstream errStr;

        if(msg == NULL)
        {
            switch(type) {
                case CarbonXAxiError_NoMemory :
                    errStr << "An Out of Memory Error occured when processing "
                       << "transaction." << endl;
                    break;

                case CarbonXAxiError_ConnectionError :
                    errStr << "An Connection Error occured when " << 
                        "processing transaction." << endl;
                    break;
                    
                case CarbonXAxiError_ProtocolError :
                    errStr << "An Protocol Error occured when " << 
                        "processing transaction." << endl;
                    break;
                    
                case CarbonXAxiError_InvalidTrans :
                    errStr << "An Invalid Transaction Error occured when " << 
                        "processing transaction." << endl;
                    break;
                    
                case CarbonXAxiError_InternalError :
                default:
                    errStr << "**** INTERNAL ERROR ****" << endl;
                    break;
            }
            SC_REPORT_ERROR("/Carbon/AXI", errStr.str().c_str());
        }
        else
            SC_REPORT_ERROR("/Carbon/AXI", msg);
        
    }

    //! Error Notify2 Callback Function
    static void notify2CB(CarbonXAxiErrorType type, const char* msg,
            void* stimulusInst)
    {
        // CarbonXSCAxi* inst = reinterpret_cast<CarbonXSCAxi*>(stimulusInst);
        std::stringstream errStr;

        if(msg == NULL)
        {
            switch(type) {

                case CarbonXAxiError_NoMemory :
                    errStr << "An Out of Memory Error occured." << endl;
                    break;

                case CarbonXAxiError_ConnectionError :
                    errStr << "An Connection Error occured." << endl;
                    break;
                    
                case CarbonXAxiError_ProtocolError :
                    errStr << "An Protocol Error occured." << endl;
                    break;
                    
                case CarbonXAxiError_InvalidTrans :
                    errStr << "An Invalid Transaction Error occured." << endl;
                     
                case CarbonXAxiError_InternalError :
                default:
                    errStr << "**** INTERNAL ERROR ****" << endl;
                    break;
            }
            SC_REPORT_ERROR("/Carbon/AXI", errStr.str().c_str());
        }
        else
            SC_REPORT_ERROR("/Carbon/AXI", msg);
    }

    //! Transactor configuration
    CarbonXAxiConfig *mConfig;

    //! Transactor instance
    CarbonXAxiID mXtorInst;

    //! Export StartTransaction IF
    StartTransaction mStartExport;
};

/*!
 * @}
 */

/*!
 * @}
 */

#endif
