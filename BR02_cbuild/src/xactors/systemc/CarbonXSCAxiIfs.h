// -*- C++ -*-
/*******************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#ifndef __CarbonXSCAxiIfs_h_
#define __CarbonXSCAxiIfs_h_

#include "systemc.h"
#include "xactors/axi/carbonXAxi.h"

/*!
 * \addtogroup AxiSystemC
 * @{
 */

/*!
 * \file xactors/systemc/CarbonXSCAxiIfs.h
 * \brief Definition of AXI SystemC Transaction Interface
 *
 * This file provides definition of AXI transaction interface class
 * for StartTransaction, ReportTransaction and Response interfaces
 */

//! AXI SystemC Response Interface
class CarbonXSCAxiResponseIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCAxiResponseIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCAxiResponseIF() {}

  /*! 
   * This function is called back by the transactor to set the default 
   * response condition for a transaction. This function will be called
   * back from the receiving side as soon as the transaction is started.
   */
  virtual void setDefaultResponse(CarbonXAxiTrans* trans) = 0;

  /*!
   * This function is called back by the transactor to set the response
   * to a transaction. This is called on the receiving side once the full
   * transaction has been received.
   */
  virtual void setResponse(CarbonXAxiTrans* trans) = 0;

  //! This function will not be called in the AXI transactor.
  void refreshResponse(CarbonXAxiTrans* trans) {}
};

//! AXI SystemC StartTransaction Interface
class CarbonXSCAxiStartTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCAxiStartTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCAxiStartTransactionIF() {}

  /*!
   * This function can be used by the user to check if new transaction
   * can be initiated at transactor side.
   */
  virtual CarbonUInt32 isSpaceInTransactionBuffer(
          CarbonXAxiTransType transType) = 0;

  /*!
   * This function can be used by the user to push a new transaction to
   * the transactor for initiation. 
   */
  virtual CarbonUInt32 startNewTransaction(CarbonXAxiTrans* trans) = 0;
};

//! AXI SystemC ReportTransaction Interface
class CarbonXSCAxiReportTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCAxiReportTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCAxiReportTransactionIF() {}

  /*!
   * This function is called back by the transactor when a transaction push
   * is complete from the initiator side.
   */
  virtual void reportTransaction(CarbonXAxiTrans* trans) = 0;
};

#endif
