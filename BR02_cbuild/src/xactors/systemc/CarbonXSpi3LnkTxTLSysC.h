// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __CarbonXSpi3LnkTxTLSysC__
#define __CarbonXSpi3LnkTxTLSysC__

#include <systemc.h>
#include "xactors/carbonX.h"
#include "xactors/systemc/CarbonXBase.h"
#include "CarbonXSpi3Ifs.h"
#include <string>
#include <vector>
#include <map>

template <int NPORTS = 1> 
class CarbonXSpi3LnkTxTLSysC: 
  public CarbonXBase
{

  typedef std::vector<UInt32>::iterator       PortIter;
  typedef  std::map<UInt32, UInt32>::iterator PortIdIter;

public:
  
  sc_port<CarbonXSlaveIf<CarbonXTransReqT, CarbonXTransRespT > > config_port;
  sc_port<CarbonXGetIf<CarbonXTransReqT>, NPORTS >               tx_port; // NPORT Number of Port Connections

  // constructor
  CarbonXSpi3LnkTxTLSysC(const sc_module_name instname, const char *xtor_path) : 
    CarbonXBase(instname, xtor_path),
    config_port("config_port"),
    tx_port("tx_port") {
  }
  
 private :
  
  // carbonX Attach methods
  void transactionLoop() {
    while (1) {
      bool success;

      // Check for configuration transactions
      processConf();
      
      // Loop through all ports and send available packets
      // Every time that we successfully send data to a port
      // last port is updated, so as long as we have and can send
      // data we will not exit this loop. When we have done one
      // full loop thorugh all the ports without being able to send 
      // any data, we idle once to give the chance for anything else to happen
      // mCurrPort = mLastPort;
      if (!mPortOrder.empty()) do {

	// Try to Send a packet, 
	success = sendPacket();

	// Take care of any configuration transactions that may have arrived
	// while sending packets
	processConf();	
	
	// End loop before we get to the last port that we sent data to.
	// If we loop over only one port, exit loop every time we got an unsuccessful send
      } while( (nextPort() != mLastPort) && (mPortOrder.size() > 1) || success);

      // Idle so we give the interface time to update or
      // to so that we can receive more packets to send
      carbonXIdle(1);
    }
  }
  
  void processConf() {
    
    while(config_port->nb_get(mReq)){
      int j = 0;
      switch(mReq.getOpcode()) {
      case CarbonXSpi3TransReqT::OP_SET_PORT_ID :
      {
	UInt32 index = mReq.getAddress();
	UInt32 id    = *(reinterpret_cast<UInt32 *>(mReq.getData()));
	mPortIds[id]  = index;
	mResp.transaction(CARBONX_OP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
	mResp.setTransId(mReq.getTransId());
	break;
      }
      case CarbonXSpi3TransReqT::OP_SET_PORT_ORDER :
      {
        UInt32 *order = reinterpret_cast<UInt32 *>(mReq.getData());
	mPortOrder.resize(mReq.getSize()/4);
	for(UInt32 i = 0; i < mReq.getSize()/4; i++)
	  mPortOrder[i] = order[i];
	mLastPort = mCurrPort = mPortOrder.begin();
	
	mResp.transaction(CARBONX_OP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
	mResp.setTransId(mReq.getTransId());
	break;
      }
      default :
	carbonXReqRespTransaction(&mReq, &mResp);
      }
      config_port->put(mResp);
    }
  }

  // Help Method to get the next port
  PortIter & nextPort() {
    ++mCurrPort;
    if(mCurrPort == mPortOrder.end()) mCurrPort = mPortOrder.begin();

    return mCurrPort;
  }

  // This method sends a packet if there is one available and
  // returns success if a partial or full packet was successfully
  // sent to the interface.
  bool sendPacket() {
    
    UInt32 bytes_sent = 0;

    // If there are not a Packet for this queue, create an empty one
    if(mTxPkt.find(*mCurrPort) == mTxPkt.end()) {
      mTxPkt[*mCurrPort] = std::vector<UInt8>(0);
    }
     
    // If Packet is empty, see if there is a packet on the queue
    if(mTxPkt[*mCurrPort].empty()) {
      if(tx_port[mPortIds[*mCurrPort]]->nb_get(mReq)) {
	mTxPkt[*mCurrPort].resize(mReq.getSize());
	memcpy(&mTxPkt[*mCurrPort][0], mReq.getData(), mReq.getSize());
	mTxSop[*mCurrPort] = true;
      }
    }
    
    // Coding Aid
    std::vector<UInt8> &pkt = mTxPkt[*mCurrPort];
    bool               &sop = mTxSop[*mCurrPort];

    if(!pkt.empty() && canSend(*mCurrPort)) {
      
      bytes_sent = carbonXSpi3LnkTxPkt(&pkt[0], pkt.size(), *mCurrPort, sop); 
      
      // Update bytes sent
      if(bytes_sent) {

	if(bytes_sent > pkt.size()) {
	  SC_REPORT_ERROR("/Carbon/Transactor/CarbonXSpi3LnkTxTLSysC/", "Sent more bytes that packet is long.");
	  pkt.resize(0);
	}
	else pkt.erase(pkt.begin(), pkt.begin()+bytes_sent);

	// Mark that the current packet has started to be sent
	sop = false;

	// If we successfully sent data, update last port sent too
	mLastPort = mCurrPort;
      }
      
    }

    // Success if any bytes got sent
    return (bytes_sent != 0);
  }

  bool canSend(UInt32 port) {
    bool result;
    // Check if this port has any space left in its fifo
    UInt32 port_stat = carbonXCsRead(CARBONXSPI3LNKTX_PORTSTATBASE);
    if((port_stat>>mPortIds[port])&1)
      result = true;
    else
      result = false;

    return result;
  }
  
  CarbonXTransReqT                      mReq;
  CarbonXTransRespT                     mResp;

  std::map<UInt32, UInt32>              mPortIds;
  std::vector<UInt32>                   mPortOrder;
  PortIter                              mCurrPort;
  PortIter                              mLastPort;
  std::map<UInt32, std::vector<UInt8> > mTxPkt;
  std::map<UInt32, bool >               mTxSop;
};

#endif
