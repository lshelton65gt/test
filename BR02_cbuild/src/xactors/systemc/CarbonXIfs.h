// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxifs__
#define __carbonxifs__

#include <iostream>
#include <iomanip>
#include "carbon/carbon.h"
#include "xactors/carbonX.h"

// Transaction Opcodes
#define CARBONX_OP_NOP     	1
#define CARBONX_OP_IDLE    	2
#define CARBONX_OP_SLEEP   	3
#define CARBONX_OP_READ    	4
#define CARBONX_OP_WRITE   	5
#define CARBONX_OP_CS_READ 	6
#define CARBONX_OP_CS_WRITE	7
#define CARBONX_OP_NXTREQ       8
#define CARBONX_OP_INT_DONE     9
#define CARBONX_OP_FREAD       10
#define CARBONX_OP_FWRITE      11
#define CARBONX_OP_WAKEUP      12
#define CARBONX_OP_CS_RMW      13
#define CARBONX_OP_RESET       14
#define CARBONX_OP_NUM_OPCODES 15

class  CarbonXTransReqT : public carbonXTransactionStruct {
public:
  
  // Default Constructor
  CarbonXTransReqT(UInt32 buf_size = 0) {
    carbonXInitTransactionStruct(this, buf_size);
    //std::cout << "CarbonXTransReqT:  this: "<< std::hex << (int)this << " mData: 0x" << std::hex << (int)mData << " mBufferSize: " << mBufferSize << std::endl; 
  }
      
  // Copy Contructor
  CarbonXTransReqT(const CarbonXTransReqT &other) {
    carbonXInitTransactionStruct(this, 0);
    doCopy(other);
  } 
  
  // Public Query Methods
  UInt32      getTransId()   const {return mTransId;  }
  UInt32      getRepeatCnt() const {return mRepeatCnt; }
  UInt32      getOpcode()    const {return mOpcode;  }
  UInt64      getAddress()   const {return mAddress; }
  UInt8 *     getData()      const {return mData; }
  UInt32      getStatus()    const {return mStatus;  }
  UInt32      getSize()      const {return mSize; }
  UInt32      getWidth()     const {return mWidth; }

  void        setTransId(UInt32 id)    {mTransId   = id;}
  void        setRepeatCnt(UInt32 cnt) {mRepeatCnt = cnt;}
  void        setStatus(UInt32 status) {mStatus    = status;}

  // Generic Transaction, Most users would not use this method
  void transaction(UInt32 opcode, UInt64 address, const void * data, 
		   UInt32 size,   UInt32 status,  UInt32       width) {
    mTransId++;  // Transaction ID is incremented for every transaction 
    mOpcode    = opcode;
    mAddress   = address;
    mStatus    = status;
    mSize      = size;
    mWidth     = width;
    mRepeatCnt = 1;
    mInterrupt = 0;
    mStartTime = 0;
    mEndTime   = 0;
    mHasData   = 0;

    // Allocated memory for the Data buffer
    allocate();
    
    //std::cout << "transaction: after allocate(). this = " << std::hex << (int)this << " mData = " << std::hex << (int)mData << " \n";
    if(mData && data && size > 0) { // Only Copy if both buffers are valid
      memcpy(mData, data, copySize());
      mHasData = 1;
    }
  }
  
  void resetBFM() {
    transaction(CARBONX_OP_RESET, 0, NULL, 4, 0, TBP_WIDTH_32);
  }
  void csWrite(UInt64 address, UInt32 data) {
    transaction(CARBONX_OP_CS_WRITE, address, reinterpret_cast<const void*>(&data), 4, 0, TBP_WIDTH_32);
  }
  void csMaskWrite(UInt64 address, UInt32 data, UInt32 mask) {
    UInt32 wdata[] = {data, mask};
    transaction(CARBONX_OP_CS_RMW, address, reinterpret_cast<const void*>(wdata), 8, 0, TBP_WIDTH_32);
  }
  void csReadReq(UInt64 address) {
    transaction(CARBONX_OP_CS_READ, address, NULL, 4, 0, TBP_WIDTH_32);
  }
  void write8(UInt64 address, const UInt8 data) {
    transaction(CARBONX_OP_WRITE, address, reinterpret_cast<const void*>(&data), 1, 0, TBP_WIDTH_8);
  }
  void write16(UInt64 address, const UInt16 data) {
    transaction(CARBONX_OP_WRITE, address, reinterpret_cast<const void*>(&data), 2, 0, TBP_WIDTH_16);
  }
  void write32(UInt64 address, const UInt32 data) {
    transaction(CARBONX_OP_WRITE, address, reinterpret_cast<const void*>(&data), 4, 0, TBP_WIDTH_32);
  }
  void write64(UInt64 address, const UInt64 data) {
    transaction(CARBONX_OP_WRITE, address, reinterpret_cast<const void*>(&data), 8, 0, TBP_WIDTH_64P);
  }
  void writeArray8(UInt64 address, const UInt8 *data, UInt32 size) {
    transaction(CARBONX_OP_WRITE, address, data, size, 0, TBP_WIDTH_8);
  }
  void writeArray16(UInt64 address, const UInt16 *data, UInt32 size) {
    transaction(CARBONX_OP_WRITE, address, data, size, 0, TBP_WIDTH_16);
  }
  void writeArray32(UInt64 address, const UInt32 *data, UInt32 size) {
    transaction(CARBONX_OP_WRITE, address, data, size, 0, TBP_WIDTH_32);
  }
  void writeArray64(UInt64 address, const UInt64 *data, UInt32 size) {
    transaction(CARBONX_OP_WRITE, address, data, size, 0, TBP_WIDTH_64);
  }
  void read8Req(UInt64 address) {
    transaction(CARBONX_OP_READ, address, NULL, 1, 0, TBP_WIDTH_8);
  }
  void read16Req(UInt64 address) {
    transaction(CARBONX_OP_READ, address, NULL, 2, 0, TBP_WIDTH_16);
  }
  void read32Req(UInt64 address) {
    transaction(CARBONX_OP_READ, address, NULL, 4, 0, TBP_WIDTH_32);
  }
  void read64Req(UInt64 address) {
    transaction(CARBONX_OP_READ, address, NULL, 8, 0, TBP_WIDTH_64P);
  }
  void readArray8Req(UInt64 address, UInt32 size) {
    transaction(CARBONX_OP_READ, address, NULL, size, 0, TBP_WIDTH_8);
  }
  void readArray16Req(UInt64 address, UInt32 size) {
    transaction(CARBONX_OP_READ, address, NULL, size, 0, TBP_WIDTH_16);
  }
  void readArray32Req(UInt64 address, UInt32 size) {
    transaction(CARBONX_OP_READ, address, NULL, size, 0, TBP_WIDTH_32);
  }
  void readArray64Req(UInt64 address, UInt32 size) {
    transaction(CARBONX_OP_READ, address, NULL, size, 0, TBP_WIDTH_64);
  }

  // 
  CarbonXTransReqT & operator=(const CarbonXTransReqT &other) {
    if (&other != this) {
      doCopy(other);
    }
    return *this;
  }

  ~CarbonXTransReqT() {
    //std::cout << "~CarbonXTransReqT:  this: "<< std::hex << (int)this << " mData: 0x" << std::hex << (int)mData << " mBufferSize: " << mBufferSize << std::endl; 
    carbonXDestroyTransactionStruct(this);
  }

  
private:

  void doCopy(const CarbonXTransReqT &other) {
    carbonXCopyTransactionStruct(this, &other);    
  }

  void allocate() {
    carbonXReallocateTransactionBuffer(this);
  }
  UInt32 copySize() const {
    return carbonXCalculateActualTransactionSize(this);
  }
};

inline std::ostream & operator<<(std::ostream &os, const CarbonXTransReqT &obj) {

  const char opstr[][10] = {"ILLEGAL", "NOP", "IDLE", "SLEEP", "READ", "WRITE", 
			    "CS_READ", "CS_WRITE",
			    "NXTREQ", "INT_DONE", "FREAD", "WAKEUP", "CS_RMW"};
  std::ostringstream opcode;
  
  os << "------------------------------------"   << std::endl
     << "Transaction ID: 0x" << std::hex << obj.getTransId()   << std::endl
     << "Repeat Count:   "   << std::dec << obj.getRepeatCnt() << std::endl;
  
  if(obj.getOpcode() < CARBONX_OP_NUM_OPCODES)
    os << "Opcode:         " << opstr[obj.getOpcode()] << std::endl;
  else
    os << "Opcode:         0x" << std::hex << obj.getOpcode() << std::endl;
  
  os << "Address:        0x" << std::hex << obj.getAddress()   << std::endl
     << "Size:           "   << std::dec << obj.getSize()      << std::endl
     << "Status:         0x" << std::hex << obj.getStatus()    << std::endl
     << "Width:          0x" << std::hex << obj.getWidth()     << std::endl
     << "Payload:"                                             << std::endl;
  
  if(obj.getSize() > 0 && obj.getData() != NULL) {

    // Output The Payload Data based on the the Words Size
    for(UInt32 i = 0; i < obj.getSize(); i += obj.getWidth()&0xf) {
      UInt64 odata = 0;         // silence coverity
      switch(obj.getWidth()) {
      case 1 : odata = *(reinterpret_cast<UInt8  *>(obj.getData()+i)); break;
      case 2 : odata = *(reinterpret_cast<UInt16 *>(obj.getData()+i)); break;
      case 4 : odata = *(reinterpret_cast<UInt32 *>(obj.getData()+i)); break;
      case 8 : odata = *(reinterpret_cast<UInt64 *>(obj.getData()+i)); break;
      }
      os << " " << std::dec << i << ": 0x" << std::setw(obj.getWidth()&0xf) << std::setfill('0') << std::hex << odata << std::endl;
    }
  }
  return os;
}

class  CarbonXTransRespT : public carbonXTransactionStruct {
public:
  
  // Default Constructor
  CarbonXTransRespT(UInt32 buf_size = 0) {
    carbonXInitTransactionStruct(this, buf_size);
    //std::cout << "CarbonXTransRespT:  this: "<< std::hex << (int)this << " mData: 0x" << std::hex << (int)mData << " mBufferSize: " << mBufferSize << std::endl; 
  }
      
  // Copy Contructor
  CarbonXTransRespT(const CarbonXTransRespT &other) {
    carbonXInitTransactionStruct(this, 0);
    doCopy(other);
  } 
  
  // Public Query Methods
  UInt32      getTransId()   const {return mTransId;  }
  UInt32      getRepeatCnt() const {return mRepeatCnt; }
  UInt32      getOpcode()    const {return mOpcode;  }
  UInt64      getAddress()   const {return mAddress; }
  UInt8 *     getData()      const {return mData; }
  UInt8 *     getData8()     const {return reinterpret_cast<UInt8 *>(mData); }
  UInt16 *    getData16()    const {return reinterpret_cast<UInt16 *>(mData); }
  UInt32 *    getData32()    const {return reinterpret_cast<UInt32 *>(mData); }
  UInt64 *    getData64()    const {return reinterpret_cast<UInt64 *>(mData); }
  UInt32      getStatus()    const {return mStatus;  }
  UInt32      getSize()      const {return mSize; }
  UInt32      getWidth()     const {return mWidth; }
  UInt64      getStartTime() const {return mStartTime; }
  UInt64      getEndTime()   const {return mEndTime; }
  
  void        setTransId(UInt32 id)    {mTransId   = id;}
  void        setRepeatCnt(UInt32 cnt) {mRepeatCnt = cnt;}

  // Generic Transaction, Most users would not use this method
  void transaction(UInt32 opcode, UInt64 address, const void * data, 
		   UInt32 size,   UInt32 status,  UInt32       width) {
    mTransId++;  // Transaction ID is incremented for every transaction 
    mOpcode    = opcode;
    mAddress   = address;
    mStatus    = status;
    mSize      = size;
    mWidth     = width;
    mRepeatCnt = 1;
    mInterrupt = 0;
    mStartTime = 0;
    mEndTime   = 0;
    mHasData   = 0;

    // Allocated memory for the Data buffer
    allocate();
    
    //std::cout << "transaction: after allocate(). this = " << std::hex << (int)this << " mData = " << std::hex << (int)mData << " \n";
    if(mData && data && size > 0) { // Only Copy if both buffers are valid
      memcpy(mData, data, copySize());
      mHasData = 1;
    }
  }
  
  UInt8 read8() const {
    if(mData != NULL) return *(reinterpret_cast<UInt8 *>(mData));
    else              return 0;
  }
  UInt16 read16() const {
    if(mData != NULL) return *(reinterpret_cast<UInt16 *>(mData));
    else              return 0;
  }
  UInt32 read32() const {
    if(mData != NULL) return *(reinterpret_cast<UInt32 *>(mData));
    else              return 0;
  }
  UInt64 read64() const {
    if(mData != NULL) return *(reinterpret_cast<UInt64 *>(mData));
    else              return 0;
  }
  
  UInt32 readArray8(UInt8 *buffer) const {
    memcpy(buffer, mData, copySize());
    return mSize;
  }
  UInt32 readArray16(UInt16 *buffer) const {
    memcpy(buffer, mData, copySize());
    return mSize;
  }
  UInt32 readArray32(UInt32 *buffer) const {
    memcpy(buffer, mData, copySize());
    return mSize;
  }
  UInt32 readArray64(UInt64 *buffer) const {
    memcpy(buffer, mData, copySize());
    return mSize;
  }

  // 
  CarbonXTransRespT & operator=(const CarbonXTransRespT &other) {
    if (&other != this) {
      doCopy(other);
    }
    return *this;
  }

  ~CarbonXTransRespT() {
    //std::cout << "~CarbonXTransRespT: this: "<< std::hex << (int)this << " mData: 0x" << std::hex << (int)mData << " mBufferSize: " << mBufferSize << std::endl; 
    carbonXDestroyTransactionStruct(this);
  }

private:

  void doCopy(const CarbonXTransRespT &other) {
    carbonXCopyTransactionStruct(this, &other);    
  }

  void allocate() {
    carbonXReallocateTransactionBuffer(this);
  }
  UInt32 copySize() const {
    return carbonXCalculateActualTransactionSize(this);
  }
};

inline std::ostream & operator<<(std::ostream &os, const CarbonXTransRespT &obj) {
  const char opstr[][10] = {"ILLEGAL", "NOP", "IDLE", "SLEEP", "READ", "WRITE", 
			    "CS_READ", "CS_WRITE",
			    "NXTREQ", "INT_DONE", "FREAD", "WAKEUP", "CS_RMW"};
  std::ostringstream opcode;
  
  os << "------------------------------------"   << std::endl
     << "Transaction ID: 0x" << std::hex << obj.getTransId()   << std::endl
     << "Repeat Count:   "   << std::dec << obj.getRepeatCnt() << std::endl;
  
  if(obj.getOpcode() < CARBONX_OP_NUM_OPCODES)
    os << "Opcode:         " << opstr[obj.getOpcode()] << std::endl;
  else
    os << "Opcode:         0x" << std::hex << obj.getOpcode() << std::endl;
  
  os << "Address:        0x" << std::hex << obj.getAddress()   << std::endl
     << "Size:           "   << std::dec << obj.getSize()      << std::endl
     << "Status:         0x" << std::hex << obj.getStatus()    << std::endl
     << "Width:          0x" << std::hex << obj.getWidth()     << std::endl
     << "Payload:"                                             << std::endl;
  
  if(obj.getSize() > 0 && obj.getData() != NULL) {

    // Output The Payload Data based on the the Words Size
    for(UInt32 i = 0; i < obj.getSize(); i += obj.getWidth()&0xf) {
      UInt64 odata = 0;
      switch(obj.getWidth()) {
      case 1 : odata = *(reinterpret_cast<UInt8  *>(obj.getData()+i)); break;
      case 2 : odata = *(reinterpret_cast<UInt16 *>(obj.getData()+i)); break;
      case 4 : odata = *(reinterpret_cast<UInt32 *>(obj.getData()+i)); break;
      case 8 : odata = *(reinterpret_cast<UInt64 *>(obj.getData()+i)); break;
      }
      os << " " << std::dec << i << ": 0x" << std::setw(obj.getWidth()&0xf) << std::setfill('0') << std::hex << odata << std::endl;
    }
  }
  return os;
}

// Interface for the carbonX threads
template <class T>
class CarbonXGetIf : public virtual sc_interface {
public:
  virtual T get() = 0;
  virtual bool nb_get( T& )=0;
  virtual bool nb_can_get() const = 0;
  virtual const std::string &ok_to_get() const = 0;
  virtual int used() const = 0;
  virtual int size() const = 0;
  virtual void debug() const = 0;
};
 
// Interface for the carbonX threads
template <class T>
class CarbonXPutIf : public virtual sc_interface {
public:
  virtual void put( const T & ) = 0;
  virtual bool nb_put( const T& )=0;
  virtual bool nb_can_put() const = 0;
  virtual const std::string &ok_to_put() const = 0;
  virtual int  used() const = 0;
  virtual int  size() const = 0;
  virtual void debug() const = 0;
};

template <typename REQ, typename RSP>
class CarbonXSlaveIf : 
  public virtual CarbonXPutIf<RSP> ,
  public virtual CarbonXGetIf<REQ> {};
	
#endif
