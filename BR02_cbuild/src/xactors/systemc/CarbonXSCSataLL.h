// -*- C++ -*-
/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved. 

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __CarbonXSCSataLL_h_
#define __CarbonXSCSataLL_h_

#include "systemc.h"
#include "xactors/sata/carbonXSataLL.h"
#include "xactors/systemc/CarbonXSCSataLLIfs.h"

/*!
 * \addtogroup SataLL
 * @{
 */

/*!
 * \defgroup SataLLSystemC SATA Link Layer SystemC-Interface
 * \brief User Interface Classes and Types for the SATA Link Layer transactor
 */

/*!
 * \addtogroup SataLLSystemC
 * @{
 */

/*!
 * \file CarbonXSCSataLL.h
 * \brief Definition of SATA Link Layer SystemC Transactor Object
 *
 * This file provides the definition of SATA Link Layer transactor class in 
 * SystemC and related SystemC SATA signal class.
 */

//! CarbonXSCSataSignal class
/*!
 * The CarbonXSCSataSignal class models signal inputs and outputs of the
 * transactor, handling the communication between System C channel and the
 * transactor's callback methods.
 */
template<class _SIG_TYPE>
class CarbonXSCSataSignal : public sc_signal<_SIG_TYPE>
{
public:
  //! constructor
  CarbonXSCSataSignal(const char *name) :
    sc_signal<_SIG_TYPE>(name)
  {}

  //! destructor
  ~CarbonXSCSataSignal()
  {}

  //! Pull the value from the System C signal.
  static CarbonStatus read_callback(CarbonUInt32* data, void* userdata)
  {
    CarbonXSCSataSignal *me =
        reinterpret_cast<CarbonXSCSataSignal<_SIG_TYPE>*>(userdata);
    // NOTE: this only works for 32 bit or less!
    *data = me->read();
    return eCarbon_OK;
  }

  //! Push the value into the System C signal.
  static CarbonStatus write_callback(CarbonUInt32* data, void* userdata)
  {
    CarbonXSCSataSignal *me =
        reinterpret_cast<CarbonXSCSataSignal<_SIG_TYPE>*>(userdata);
    // NOTE: this only works for 32 bit or less!
    me->write(*data);
    return eCarbon_OK;
  }
};

//! CarbonXSCSataLL class
/*!
 * The CarbonXSCSataLL class is a wrapper for the C API functionality
 * provided by the Carbon SATA LL transactor.
 *
 * This class is responsible for managing the C instance of the Carbon
 * SATA LL transactor.
 */
class CarbonXSCSataLL : public sc_module
{
public:

  //! SystemC Contructor Declaration
  SC_HAS_PROCESS(CarbonXSCSataLL);

  //! \name SystemC Transaction Ports
  // @{
  //! Start New Transaction Export
  sc_export<CarbonXSCSataStartTransactionIF> startTransExport;

  //! Report Transaction Port
  sc_port<CarbonXSCSataReportTransactionIF>  reportTransPort;

  //! Response Port
  sc_port<CarbonXSCSataResponseIF>           responsePort;

  // @}

  //! \name SystemC Signal Ports
  // @{
  //!Transactor Input SystemClock from System
  sc_in<bool> SystemClock;

  //!Transactor Input PARTIAL from Carbon Model
  CarbonXSCSataSignal<bool> PARTIAL;
  //!Transactor Input SLUMBER from Carbon Model
  CarbonXSCSataSignal<bool> SLUMBER;
  //!Transactor Input DATAIN from Carbon Model
  CarbonXSCSataSignal<sc_uint<10> > DATAIN;

  //!Transactor Output COMRESET to Carbon Model
  CarbonXSCSataSignal<bool> COMRESET;
  //!Transactor Output COMWAKE to Carbon Model
  CarbonXSCSataSignal<bool> COMWAKE;
  //!Transactor Output PhyRdy to Carbon Model
  CarbonXSCSataSignal<bool> PhyRdy;
  //!Transactor Output DeviceDetect to Carbon Model
  CarbonXSCSataSignal<bool> DeviceDetect;
  //!Transactor Output PhyInternalError to Carbon Model
  CarbonXSCSataSignal<bool> PhyInternalError;
  //!Transactor Output COMMA to Carbon Model
  CarbonXSCSataSignal<bool> COMMA;
  //!Transactor Output DATAOUT to Carbon Model
  CarbonXSCSataSignal<sc_uint<10> > DATAOUT;
  // @}

  //! constructor
  CarbonXSCSataLL(sc_module_name transactorName,
                  CarbonXSataLLType xtorType,
                  CarbonUInt32 maxTransfers) :
    sc_module(transactorName),
    SystemClock("SystemClock"),
    PARTIAL("PARTIAL"),
    SLUMBER("SLUBMER"),
    DATAIN("DATAIN"),
    COMRESET("COMRESET"),
    COMWAKE("COMWAKE"),
    PhyRdy("PhyRdy"),
    DeviceDetect("DeviceDetect"),
    PhyInternalError("PhyInternalError"),
    COMMA("COMMA"),
    DATAOUT("DATAOUT"),
    mXtorInst(carbonXSataLLCreate(name(),
                                  xtorType,
                                  maxTransfers,
                                  reportTransactionCB,
                                  setDefaultResponseCB,
                                  setResponseCB,
                                  refreshResponseCB,
                                  notifyCB,
                                  notify2CB,
                                  this)),
    mStartExport(mXtorInst)
  {
    connect();
    init();
  }

  //! destructor
  ~CarbonXSCSataLL()
  {
    carbonXSataLLDestroy(mXtorInst);
  }

  //! Apply the given configuration to the transactor.
  void setConfig(const CarbonXSataLLConfig *config)
  {
    carbonXSataLLSetConfig(mXtorInst, config);
  }

  //! Fills out the given configuration object with the transactor's
  //  current configuration.
  void getConfig(CarbonXSataLLConfig *config)
  {
    carbonXSataLLGetConfig(mXtorInst, config);
  }

  //! Dump the state of the transactor.
  void dump()
  {
    carbonXSataLLDump(mXtorInst);
  }

private:

  //! StartTransaction export class
  /*!
   * Used to sc_export the Start Transaction Interface
   */
  class StartTransaction : public CarbonXSCSataStartTransactionIF
  {
  public:
    //! Constructor
    StartTransaction(CarbonXSataLLID xtor) : mXtorInst(xtor){}

    //! destructor
    ~StartTransaction() {}

    //! Start New Transaction
    CarbonUInt32 startNewTransaction(CarbonXSataLLTrans* trans)
    {
      return carbonXSataLLStartNewTransaction(mXtorInst, trans);
    }

  private:
    //! SATA Transactor Instance Handle
    CarbonXSataLLID mXtorInst;
  };


  //! Connect the System C signals to the xactor callbacks
  void connect()
  {
    CarbonXInterconnectNameCallbackTrio trio_array[] =
      {
        { "PARTIAL", 0, CarbonXSCSataSignal<bool>::read_callback, &PARTIAL },
        { "SLUMBER", 0, CarbonXSCSataSignal<bool>::read_callback, &SLUMBER },
        { "DATAIN", 0, CarbonXSCSataSignal<sc_uint<10> >::read_callback,
            &DATAIN },
        { "COMRESET", CarbonXSCSataSignal<bool>::write_callback, 0, &COMRESET },
        { "COMWAKE", CarbonXSCSataSignal<bool>::write_callback, 0, &COMWAKE },
        { "PhyRdy", CarbonXSCSataSignal<bool>::write_callback, 0, &PhyRdy },
        { "DeviceDetect", CarbonXSCSataSignal<bool>::write_callback, 0,
            &DeviceDetect },
        { "PhyInternalError", CarbonXSCSataSignal<bool>::write_callback, 0,
            &PhyInternalError },
        { "COMMA", CarbonXSCSataSignal<bool>::write_callback, 0, &COMMA },
        { "DATAOUT", CarbonXSCSataSignal<sc_uint<10> >::write_callback, 0,
            &DATAOUT },
        { 0, 0, 0, 0 }
      };
    carbonXSataLLConnectToLocalSignalMethods(mXtorInst, trio_array);

    // Bind exports
    startTransExport(mStartExport);
  }

  //! Set up the sensitivities to run the xactor.
  void init()
  {
    SC_METHOD(evaluate);
    sensitive << SystemClock.pos();

    // NOTE: if there are any async paths through the xactor, then the xactor
    // needs to set up async sensitivities to the async_evaluate function here.
    // SC_METHOD(async_evaluate);
    // sensitive << <async signals> ;
  }

  //! Run a single cycle
  void evaluate()
  {
    carbonXSataLLRefreshInputs(mXtorInst, sc_time_stamp().value());
    carbonXSataLLEvaluate(mXtorInst, sc_time_stamp().value());
    carbonXSataLLRefreshOutputs(mXtorInst, sc_time_stamp().value());
  }

  //! Set Default Response Callback Function
  static void setDefaultResponseCB(CarbonXSataLLTrans* trans,
          void* stimulusInst)
  {
    CarbonXSCSataLL* inst = reinterpret_cast<CarbonXSCSataLL*>(stimulusInst);
    inst->responsePort->setDefaultResponse(trans);
  }

  //! Set Response Callback Function
  static void setResponseCB(CarbonXSataLLTrans* trans, void* stimulusInst)
  {
    CarbonXSCSataLL* inst = reinterpret_cast<CarbonXSCSataLL*>(stimulusInst);
    inst->responsePort->setResponse(trans);
  }

  //! Refresh Response Callback Function
  static void refreshResponseCB(CarbonXSataLLTrans* trans, void* stimulusInst)
  {
    CarbonXSCSataLL* inst = reinterpret_cast<CarbonXSCSataLL*>(stimulusInst);
    inst->responsePort->refreshResponse(trans);
  }

  //! Report Response Callback Function
  static void reportTransactionCB(CarbonXSataLLTrans* trans, void* stimulusInst)
  {
    CarbonXSCSataLL* inst = reinterpret_cast<CarbonXSCSataLL*>(stimulusInst);
    inst->reportTransPort->reportTransaction(trans);
  }

  //! Error Notify Callback Function
  static void notifyCB(CarbonXSataLLErrorType type, CarbonXSataLLTrans* trans,
          void* stimulusInst)
  {
    // CarbonXSCSataLL* inst = reinterpret_cast<CarbonXSCSataLL*>(stimulusInst);
    std::stringstream errStr;

    switch(type) {

    case CarbonXSataLLError_NoMemory :
      errStr << "An Out of Memory Error occured when processing transaction."
          << endl;
      break;

    case CarbonXSataLLError_FsmInvalidState :
      errStr << "An Invalid FSM State Error occured " <<
          "when processing transaction." << endl;
      break;

    case CarbonXSataLLError_FsmInvalidTransition :
      errStr << "An Invalid FSM State Transition Error occured " <<
          "when processing transaction." << endl;
      break;

    case CarbonXSataLLError_InternalError :
    default:
      errStr << "**** INTERNAL ERROR ****" << endl;
      break;

    }
    SC_REPORT_ERROR("/Carbon/SATA", errStr.str().c_str());
  }

  //! Error Notify2 Callback Function
  static void notify2CB(CarbonXSataLLErrorType type, void* stimulusInst)
  {
    // CarbonXSCSataLL* inst = reinterpret_cast<CarbonXSCSataLL*>(stimulusInst);
    std::stringstream errStr;

    switch(type) {

    case CarbonXSataLLError_NoMemory :
      errStr << "An Out of Memory Error occured in the transactor."
          << endl;
      break;

    case CarbonXSataLLError_FsmInvalidState :
      errStr << "An Invalid FSM State Error occured in the transactor." << endl;
      break;

    case CarbonXSataLLError_FsmInvalidTransition :
      errStr << "An Invalid FSM State Transition Error occured " <<
          "in the transactor." << endl;
      break;

    case CarbonXSataLLError_InternalError :
    default:
      errStr << "**** INTERNAL ERROR ****" << endl;
      break;

    }
    SC_REPORT_ERROR("/Carbon/SATA", errStr.str().c_str());
  }

  //! Handle evaluation of asynchronous paths
  /*!
   * For asyncs, the xactor will update its internal state immediately when RefreshInputs
   * is called.
   */
//   void async_evaluate()
//   {
//     carbonXSataLLRefreshInputs(mXtorInst, sc_time_stamp.value());
//     carbonXSataLLEvaluateAsync(mXtorInst, sc_time_stamp.value());
//     carbonXSataLLRefreshOutputs(mXtorInst, sc_time_stamp.value());
//   }

  //! Transactor instance
  CarbonXSataLLID mXtorInst;

  //! Export StartTransaction IF
  StartTransaction mStartExport;
};

/*!
 * @}
 */

/*!
 * @}
 */

#endif
