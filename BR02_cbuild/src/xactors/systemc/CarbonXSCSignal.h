// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __CarbonXSCSignal_h_
#define __CarbonXSCSignal_h_

#include "systemc.h"

#ifndef __carbon_capi_h_
#include "carbon/carbon_capi.h"
#endif

/*!
 * \file CarbonXSCSignal.h
 * \brief Definition of Carbon SystemC Transactor Signal
 *
 * This file provides the definition of Carbon SystemC transactor's signal
 * class.
 */

//! CarbonXSCSignal class
/*!
 * The CarbonXSCSignal class models signal inputs and outputs of the
 * transactor, handling the communication between System C channel and the
 * transactor's callback methods.
 */
template<class _SIG_TYPE>
class CarbonXSCSignal : public sc_signal<_SIG_TYPE>
{

public:
    //! constructor
    CarbonXSCSignal(const char *name) :
        sc_signal<_SIG_TYPE>(name)
    {}

    //! destructor
    ~CarbonXSCSignal(){}

    //! Pull the value from the System C signal.
    static CarbonStatus read_callback(CarbonUInt32* data, void* userdata)
    {
        CarbonXSCSignal *me =
            reinterpret_cast<CarbonXSCSignal<_SIG_TYPE>*>(userdata);
        //! Get the signal width
        int width = me->read().length();
        if (width <= 32) // For width 32 bit or less
            *data = me->read().to_uint();
        else // For width > 32, extract data array from sc_biguint
        {
            _SIG_TYPE value(me->read());
            for (int i = 0; i < width; i += 32)
            {
                data[i / 32] = value.to_uint();
                value = value >> 32;
            }
        }
        return eCarbon_OK;
    }

    //! Push the value into the System C signal.
    static CarbonStatus write_callback(CarbonUInt32* data, void* userdata)
    {
        CarbonXSCSignal *me =
            reinterpret_cast<CarbonXSCSignal<_SIG_TYPE>*>(userdata);
        //! Get the signal width
        int width = me->read().length();
        if (width <= 32) // For width 32 bit or less
            me->write(*data);
        else // For width > 32, put data array to sc_biguint
        {
            _SIG_TYPE value = 0;
            for (int i = 0; i < width; i += 32)
            {
                value |= _SIG_TYPE(data[i / 32]) << i;
            }
            me->write(value);
        }
        return eCarbon_OK;
    }
};

//! CarbonXSCSignal<bool> class
/*!
 * The CarbonXSCSignal class template specialization for bool type
 */
template<>
class CarbonXSCSignal<bool> : public sc_signal<bool>
{

public:
    //! constructor
    CarbonXSCSignal(const char *name) : sc_signal<bool>(name){}

    //! destructor
    ~CarbonXSCSignal(){}

    //! Pull the value from the System C signal.
    static CarbonStatus read_callback(CarbonUInt32* data, void* userdata)
    {
        CarbonXSCSignal *me =
            reinterpret_cast<CarbonXSCSignal<bool>*>(userdata);
        *data = me->read();
        return eCarbon_OK;
    }

    //! Push the value into the System C signal.
    static CarbonStatus write_callback(CarbonUInt32* data, void* userdata)
    {
        CarbonXSCSignal *me =
            reinterpret_cast<CarbonXSCSignal<bool>*>(userdata);
        me->write(*data);
        return eCarbon_OK;
    }
};

//! CarbonXSCBus class
/*!
 * This class will be used to represent data buses of transactor.
 */
template<int _WIDTH>
class CarbonXSCBus : public CarbonXSCSignal<sc_uint<_WIDTH> >
{
public:
    //! constructor
    CarbonXSCBus(const char *name) : CarbonXSCSignal<sc_uint<_WIDTH> >(name) {}
};
//! CarbonXSCBus<128> class template specialization
template<>
class CarbonXSCBus<128> : public CarbonXSCSignal<sc_biguint<128> >
{
public:
    //! constructor
    CarbonXSCBus(const char *name) : CarbonXSCSignal<sc_biguint<128> >(name) {}
};
//! CarbonXSCBus<256> class template specialization
template<>
class CarbonXSCBus<256> : public CarbonXSCSignal<sc_biguint<256> >
{
public:
    //! constructor
    CarbonXSCBus(const char *name) : CarbonXSCSignal<sc_biguint<256> >(name) {}
};
//! CarbonXSCBus<512> class template specialization
template<>
class CarbonXSCBus<512> : public CarbonXSCSignal<sc_biguint<512> >
{
public:
    //! constructor
    CarbonXSCBus(const char *name) : CarbonXSCSignal<sc_biguint<512> >(name) {}
};
//! CarbonXSCBus<1024> class template specialization
template<>
class CarbonXSCBus<1024> : public CarbonXSCSignal<sc_biguint<1024> >
{
public:
    //! constructor
    CarbonXSCBus(const char *name) : CarbonXSCSignal<sc_biguint<1024> >(name) {}
};

//! CarbonXSCBus class
/*!
 * This class will be used to represent bidirectional data buses of transactor.
 * Such buses can have mutiple driver, with one driving active value while
 * others driving tristate values. Thus such buses contains resovled logic bits.
 */
template<int _WIDTH>
class CarbonXSCRvBus : public sc_signal_rv<_WIDTH>
{

public:
    //! constructor
    CarbonXSCRvBus(const char *name) :
        sc_signal_rv<_WIDTH>(name)
    {}

    //! destructor
    ~CarbonXSCRvBus(){}

    //! Pull the value from the System C signal.
    static CarbonStatus read_callback(CarbonUInt32* data, void* userdata)
    {
        CarbonXSCRvBus *me =
            reinterpret_cast<CarbonXSCRvBus<_WIDTH>*>(userdata);
        //! Get the signal width
        int width = me->read().length();
        if (width <= 32) // For width 32 bit or less
            *data = me->read().to_uint();
        else // For width > 32, extract data array from sc_biguint
        {
            sc_lv<_WIDTH> value(me->read());
            for (int i = 0; i < width; i += 32)
            {
                data[i / 32] = value.to_uint();
                value = value >> 32;
            }
        }
        return eCarbon_OK;
    }

    //! Push the value into the System C signal.
    static CarbonStatus write_callback(CarbonUInt32* data, void* userdata)
    {
        CarbonXSCRvBus *me =
            reinterpret_cast<CarbonXSCRvBus<_WIDTH>*>(userdata);
        //! Get the signal width
        int width = me->read().length();
        if (width <= 32) // For width 32 bit or less
            me->write(*data);
        else // For width > 32, put data array to sc_biguint
        {
            sc_lv<_WIDTH> value;
            for (int i = 0; i < width; i += 32)
            {
                value |= sc_lv<_WIDTH> ((data[i / 32]) << i);
            }
            me->write(value);
        }
        return eCarbon_OK;
    }
};
#endif
