// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef __carbonxtlmpcietransactor__
#define __carbonxtlmpcietransactor__

#include "systemc.h"
#include "tlm.h"
#include "xactors/pcie/carbonXPcie.h"
#include "xactors/systemc/CarbonXPcieIfs.h"

#include <iostream>

/*!
    \addtogroup PcieSystemC
    @{
*/

//! Base Class of the  SystemC PCIE Transactor
/*!
  This class defines the transaction interface that is common between all Signal
  Interfaces of the transactor.
  It also handles the initialization of the PCIE Transactor code.
*/
class CarbonXTlmPcieTransactor : public sc_module {
public:
  
  //! SystemC Contructor Declaration
  SC_HAS_PROCESS(CarbonXTlmPcieTransactor);

#ifndef CARBON_EXTERNAL_DOC

  //! Transaction Slave Port Help Class
  class xs_port :
    public sc_port<tlm::tlm_slave_if<CarbonXPcieTransReqT, CarbonXPcieTransRespT> >
  {
  public:
    //! Typing Aid
    typedef tlm::tlm_nonblocking_get_if<CarbonXPcieTransReqT> get_if_type;
    //! Typing Aid
    typedef tlm::tlm_nonblocking_put_if<CarbonXPcieTransRespT> put_if_type;
    
    //! Contructor
    xs_port( const char *port_name ) :
      sc_port<tlm::tlm_slave_if<CarbonXPcieTransReqT, CarbonXPcieTransRespT> >( port_name ) {}
    
    //! Event Finder for ok_to_get() event
    sc_event_finder& ok_to_get() const {
      return *new tlm::tlm_event_finder_t< get_if_type ,CarbonXPcieTransReqT >(*this,
									  &get_if_type::ok_to_get );
    }
    //! Event Finder for ok_to_put() event
    sc_event_finder& ok_to_put() const {
      return *new tlm::tlm_event_finder_t< put_if_type ,CarbonXPcieTransRespT >(*this,
									  &put_if_type::ok_to_put );
    }
  };
  
  //! Signal Master Port Help Class
  template<class _SIG_TYPE, int _LINKWIDTH = 1>
  class sm_port :
    public sc_port<sc_signal_out_if<_SIG_TYPE>, _LINKWIDTH >
  {
    typedef sc_port<sc_signal_out_if<_SIG_TYPE>, _LINKWIDTH > if_type; 
  public:
    
    //! Constructor
    sm_port(CarbonXPcieSignalType type, CarbonXPcieID * pcie_inst, const char *name) :
      sc_port<sc_signal_out_if<_SIG_TYPE>, _LINKWIDTH >(name) 
    {
      for(int i = 0; i < _LINKWIDTH; i++) {
	mCbData[i].port  = this;
	mCbData[i].index = i;
	carbonXPcieNetSetValueChangeCB(carbonXPcieGetNet(pcie_inst, type, i), netChangeCB, &mCbData[i]);
     }
    }
  private:

    //! Callback Data Structure for PCIE Transactor net changes
    struct CBData {
      //! Pointer to the Port object
      sm_port<_SIG_TYPE, _LINKWIDTH> *port;
      //! Lane Index
      int                 index;
    };

    //! Net Change Callback Function
    static void netChangeCB(CarbonXPcieNetID *net, CarbonClientData me){
      CBData *cb_data = reinterpret_cast<CBData *>(me);
      CarbonUInt32 val;
      carbonXPcieNetExamine(net, &val);
      (*(cb_data->port))[cb_data->index]->write(val);
    }

    //! Callback Data
    CBData mCbData[_LINKWIDTH];
  };

#endif
  
public:
  
  //! \name SystemC Transaction Ports
  // @{

  //! SystemC Transaction Receiver Port, used to send transactions to the transactor
  /*!
    TLP Transactions sent to this port will be processed by the transactor,
    and sent out on the signal pins.
    A transaction can also be a configuration transaction used to
    configure the transactor itself. See \sa class CarbonXPcieTransReqT
    for documentation on how to configure the transactor.
  */
  xs_port                                             pcieRx;

  //! SystemC Transaction Transmit Port, used to receive transaction from the transactor
  /*
    Requested transaction and Comleter transaction received by the transactor can be retreived
    using this port.
  */
  tlm::tlm_nonblocking_put_port<CarbonXPcieTransReqT> pcieTx;
  
  // @}

  //! Constructor
  CarbonXTlmPcieTransactor(CarbonXPcieIntfType iftype, CarbonUInt32 linkwidth, sc_module_name instname) :
    sc_module(instname),
    pcieRx("pcieRx"),
    pcieTx("pcieTx"),
    mPcieInst(carbonXPcieCreate(iftype, linkwidth, name())),
    mTick(0),
    mCurrPeriod(4),
    mSymbolPeriod(4, SC_NS),
    mNeedToReset(true)
  {
    init(iftype, linkwidth);
  }
  

  //! Initializes Transactor
  void init(CarbonXPcieIntfType iftype, CarbonUInt32 linkwidth) {
    mTxCmd = carbonXPcieTLPCreate();
        
    // Setup Callback functions
    carbonXPcieSetTransCallbackFn(mPcieInst, pcieTransCB, this);
    carbonXPcieSetEndTransCallbackFn(mPcieInst, this, pcieEndTransCB);
    carbonXPcieSetRateChangeCallbackFn(mPcieInst, this, pcieRateChangeCB);

    // Set Time Scale
    carbonXPcieSetTimescale(mPcieInst, e1ns);
  }
  
  //! Destructor
  ~CarbonXTlmPcieTransactor() {
    if(mPcieInst != NULL) carbonXPcieDestroy(mPcieInst);
    if(mTxCmd != NULL) carbonXPcieTLPDestroy(mTxCmd);
  }
  
  //! Data Rate Change Callback function
  /*!
    This function gets called when the transactor is requesting a change
    in the transactors physical speed.
  */
  static void pcieRateChangeCB(void *caller, CarbonUInt32 new_rate) {
    CarbonXTlmPcieTransactor *self = reinterpret_cast<CarbonXTlmPcieTransactor *>(caller);
    if(new_rate & 2) {
      self->mCurrPeriod  = 2;
      self->mSymbolPeriod = sc_time(2, SC_NS);
    }
    else if(new_rate & 1) {
      self->mCurrPeriod  = 4;
      self->mSymbolPeriod = sc_time(4, SC_NS);
    }
    carbonXPcieAckDataRateChange(self->mPcieInst);
  }

  //! Training Method
  inline void         startTraining(CarbonUInt64 ticks)     { carbonXPcieStartTraining(mPcieInst, ticks); }
  
  //! Reset Transactor
  inline void         reset()                               { 
    carbonXPcieReset(mPcieInst);
    mSymbolPeriod = sc_time(4, SC_NS);
  }

  //! Set Print Level
  inline void         setPrintLevel(unsigned int val)       { carbonXPcieSetPrintLevel(mPcieInst, val); }
  //! Set Max Payload Size
  inline void         setMaxPayloadSize(unsigned int val)   { carbonXPcieSetMaxPayloadSize(mPcieInst, val); }
  //! Set Read Comletion Boundary
  inline void         setRCB(unsigned int val)              { carbonXPcieSetRCB(mPcieInst, val); }
  //! Set Maximum Number of Transactions In Flight
  inline void         setMaxInFlight(unsigned int val)      { carbonXPcieSetMaxTransInFlight(mPcieInst, val); }
  //! Set Scramble Status
  //! \param val If true turns on Link Scrambling. If false turns off Link Scrambling 
  inline void         setScrambleStatus(bool val)           { carbonXPcieSetScrambleStatus(mPcieInst, val); }
  //! Set Training Bypass
  //! \param val If set to true, the transactor will bypass the training sequence and go straight to Flow Control initialization
  inline void         setTrainingBypass(bool val)           { carbonXPcieSetTrainingBypass(mPcieInst, val); }
  //! Set Electrical Idle Ports to Active High
  inline void         setIdleActiveHigh()                   { carbonXPcieSetElecIdleActiveHigh(mPcieInst); }
  //! Set Electrical Idle Ports to Active Low
  inline void         setIdleActiveLow()                    { carbonXPcieSetElecIdleActiveLow(mPcieInst); }
  //! Set Supported Data Rates (Gen1 or Gen1 and Gen2)
  inline void         setSupportedSpeeds(unsigned int val)  { carbonXPcieSetSupportedSpeeds(mPcieInst, val); }
  //! Setup Flow Control Buffer Sizes
  /*!
    All buffer sizes are specified as Number of Flow Control Credits.
    One FCC = 16 bytes of byffer space.
  */
  inline void         setupBufferSizes(unsigned int vc, 
				       unsigned int posted_hdr,     unsigned int posted_data, 
				       unsigned int non_posted_hdr, unsigned int non_posted_data, 
				       unsigned int cpl_hdr,        unsigned int cpl_data) {
    carbonXPcieSetupBufferSizes(mPcieInst, vc, posted_hdr, posted_data, non_posted_hdr, non_posted_data, cpl_hdr, cpl_data);
  }

  //! Get Max Payload Size
  inline unsigned int getMaxPayloadSize()                   { return carbonXPcieGetMaxPayloadSize(mPcieInst); }
  //! Get Read Completion Boundary
  inline unsigned int getRCB()                              { return carbonXPcieGetRCB(mPcieInst); }
  //! Get Maximum Number of Transactions In Flight
  inline unsigned int getMaxInFlight()                      { return carbonXPcieGetMaxTransInFlight(mPcieInst); }
  //! Get Scramble Status
  inline bool         getScrambleStatus()                   { return carbonXPcieGetScrambleStatus(mPcieInst); }
  //! Get Training Bypass
  inline bool         getTrainingBypass()                   { return carbonXPcieGetTrainingBypass(mPcieInst); }
  
  //! Returns Current Symbol Period, useful for debugging
  sc_time & getSymbolPeriod() {
    return mSymbolPeriod;
  }

protected:
#ifndef CARBON_EXTERNAL_DOC

  //! Step Method
  void step() {
    if (mNeedToReset) {
      reset();
      mNeedToReset = false;
    }

    carbonXPcieStep(mPcieInst, mTick += mCurrPeriod);
  }
  
  //! Transaction Slave Method
  void processTransaction() {
    CarbonXPcieTransReqT trans;
    while(pcieRx->nb_get(trans)) {
      switch(trans.getOpcode()) {
      case CarbonXPcieTransReqT::eSendTLP :
	carbonXPcieAddTransaction(mPcieInst, trans.tlp());
	break;

      case CarbonXPcieTransReqT::eStartTraining :
	startTraining(trans.getOperand());
	break;

      case CarbonXPcieTransReqT::eReset :
        mNeedToReset = true;
	break;

      case CarbonXPcieTransReqT::eSetPrintLevel :
	setPrintLevel(trans.getOperand());
	break;

      case CarbonXPcieTransReqT::eSetMaxPayloadSize :
	setMaxPayloadSize(trans.getOperand());
	break;

      case CarbonXPcieTransReqT::eSetRCB :
	setRCB(trans.getOperand());
	break;
	
      case CarbonXPcieTransReqT::eSetMaxInFlight :
	setMaxInFlight(trans.getOperand());
	break;
	
      case CarbonXPcieTransReqT::eSetScrambleStatus :
	setScrambleStatus(trans.getOperand());
	break;
	
      case CarbonXPcieTransReqT::eSetTrainingBypass :
	setTrainingBypass(trans.getOperand());
	break;
	
      case CarbonXPcieTransReqT::eSetIdleActive :
	if(trans.getOperand()) setIdleActiveHigh();
	else                   setIdleActiveLow();
	break;
	
      case CarbonXPcieTransReqT::eSetSupportedSpeeds :
	setSupportedSpeeds(trans.getOperand());
	break;

      case CarbonXPcieTransReqT::eSetupBufferSizes :
	setupBufferSizes(trans.getPtrOperand()[0],
			 trans.getPtrOperand()[1],
			 trans.getPtrOperand()[2],
			 trans.getPtrOperand()[3],
			 trans.getPtrOperand()[4],
			 trans.getPtrOperand()[5],
			 trans.getPtrOperand()[6]);
	break;
	
      default :
	char errstr[40];
	sprintf(errstr, "Unknown Transaction Opcode %d.\n" , trans.getOpcode());
	SC_REPORT_ERROR("/Carbon/CarbonXTlmPcieTransactor/", errstr);
      }
    }
  }

  //! Transaction Master Method
  void sendTransaction() {
    if(pcieTx->nb_can_put()) {
      while (carbonXPcieGetReceivedTrans(mPcieInst, mTxCmd) ) {
	bool result = pcieTx->nb_put(CarbonXPcieTransReqT(mTxCmd));
	
	// Check that we still can put out a new transaction
	if(!pcieTx->nb_can_put()) break;
      }
    }

    if(pcieTx->nb_can_put()) {
      while (carbonXPcieGetReturnData(mPcieInst, mTxCmd) ) {
	bool result = pcieTx->nb_put(CarbonXPcieTransReqT(mTxCmd));
	
	// Check that we still can put out a new transaction
	if(!pcieTx->nb_can_put()) break;
      }
    }
  }
  
  //! Received Transaction Callback function
  static void pcieTransCB(CarbonXPcieID *pcie, CarbonXPcieCallbackType type, void *info) {
    (void)pcie; (void)type;
    CarbonXTlmPcieTransactor *self = reinterpret_cast<CarbonXTlmPcieTransactor *>(info);
    self->sendTransaction();
  }
  
  //! End Of Transaction Callback Function
  /*!
    Sends Status Information back to the Control Module.
  */
  static void pcieEndTransCB(CarbonXPcieID *xtor, void *caller, CarbonXPcieTLP *cmd) {
    (void)xtor;
    CarbonXTlmPcieTransactor *self = reinterpret_cast<CarbonXTlmPcieTransactor *>(caller);
    if(self->pcieRx->nb_can_put()) {
      self->pcieRx->nb_put(CarbonXPcieTransReqT(cmd));
    }
  }

protected:
  // Transactor Instance
  CarbonXPcieID * mPcieInst;

  // Keep track of tick count for the transactor
  CarbonUInt64    mTick;
  sc_time         mSymbolPeriod;
  CarbonUInt32    mCurrPeriod;

  // Transmit Command
  CarbonXPcieTLP *mTxCmd;

private:
  bool mNeedToReset;

  // User defined parameters: 
  unsigned int p_MaxPayloadSize;
  unsigned int p_ReadCompletionBoundary;
  unsigned int p_MaxInFlight;
  bool         p_ScrambleStatus;
  bool         p_DoTraining;
  bool         p_TrainingBypass;
  bool         p_IdleIsHiTrue;
  bool         p_ForceIdleInactive;
  unsigned int p_DebugPrintLevel;
  unsigned int p_BusNum;
  unsigned int p_DevNum;
  unsigned int p_FuncNum;
#endif
};

/*!
  @}
*/

#endif
