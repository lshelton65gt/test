// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#include "xactors/pcie/carbonXPcie.h"
#include <iomanip>

#ifndef __carbonxtlmpcieifs__
#define __carbonxtlmpcieifs__

/*!
  \defgroup PcieSystemC PCI Express SystemC-Interface
  \brief User Interface Classes and Types for the PCI Express transactor
*/

/*!
    \addtogroup PcieSystemC
    @{
*/

//! PCI Express SystemC Transaction class
class CarbonXPcieTransReqT {
 public:

  //! Type of transaction to send to the transactor.
  /*!
    The eSendTLP type is used to send TLP transaction through the transactor.
    The other types are used to set configuration in the transactor.
  */
  enum OpcodeT {
    eSendTLP,
    eStartTraining,
    eReset,
    eSetPrintLevel,
    eSetMaxPayloadSize,
    eSetRCB,
    eSetMaxInFlight,
    eSetScrambleStatus,
    eSetTrainingBypass,
    eSetIdleActive,
    eSetSupportedSpeeds,
    eSetupBufferSizes
  };

  //! Default Constructor
  CarbonXPcieTransReqT() {
    mTLP        = carbonXPcieTLPCreate();
    mOpcode     = eSendTLP;
    mOperand    = 0;
    mPtrOperand = 0;
  }
  
  //! Copy Constructor
  CarbonXPcieTransReqT(const CarbonXPcieTransReqT &other) {
    mTLP     = carbonXPcieTLPCreate();
    if(other.mPtrOperand) {
      mPtrOperand = new CarbonUInt32[other.mOperand];
      memcpy(mPtrOperand, other.mPtrOperand, sizeof(CarbonUInt32) * other.mOperand);
    }
    else mPtrOperand = 0;
    mOpcode  = other.mOpcode;
    mOperand = other.mOperand;
    carbonXPcieTLPCopy(mTLP, other.mTLP);
  }

  //! Conversion Constructor
  /*!
    This constructor converts from a CarbonXPcieTLP object.
  */
  CarbonXPcieTransReqT(const CarbonXPcieTLP *tlp) {
    mTLP        = carbonXPcieTLPCreate();
    mOpcode     = eSendTLP;
    mOperand    = 0;
    mPtrOperand = 0;
    carbonXPcieTLPCopy(mTLP, tlp);
  }

  // !Destructor
  ~CarbonXPcieTransReqT() {
    carbonXPcieTLPDestroy(mTLP);
    delete [] mPtrOperand;
  }
  
  //! Assignment Operator
  CarbonXPcieTransReqT & operator=(const CarbonXPcieTransReqT &other) {
    if(&other && &other != this) {
    if(other.mPtrOperand) {
      mPtrOperand = new CarbonUInt32[other.mOperand];
      memcpy(mPtrOperand, other.mPtrOperand, sizeof(CarbonUInt32) * other.mOperand);
    }
    else mPtrOperand = 0;
      mOpcode  = other.mOpcode;
      mOperand = other.mOperand;
      carbonXPcieTLPCopy(mTLP, other.mTLP);
    }

    return *this;
  }
  
  //! Comparison Operator
  bool operator==(const CarbonXPcieTransReqT &other) const {
    bool result = true;
    if(mOpcode == eSendTLP) {
      
      result &= (carbonXPcieTLPGetCmd(mTLP)           == carbonXPcieTLPGetCmd(other.mTLP));
      result &= (carbonXPcieTLPGetCmdByName(mTLP)     == carbonXPcieTLPGetCmdByName(other.mTLP));
      result &= (carbonXPcieTLPGetAddr(mTLP)          == carbonXPcieTLPGetAddr(other.mTLP));
      result &= (carbonXPcieTLPGetReqId(mTLP)         == carbonXPcieTLPGetReqId(other.mTLP));
      result &= (carbonXPcieTLPGetTag(mTLP)           == carbonXPcieTLPGetTag(other.mTLP));
      result &= (carbonXPcieTLPGetRegNum(mTLP)        == carbonXPcieTLPGetRegNum(other.mTLP));
      result &= (carbonXPcieTLPGetCompId(mTLP)        == carbonXPcieTLPGetCompId(other.mTLP));
      result &= (carbonXPcieTLPGetMsgRout(mTLP)       == carbonXPcieTLPGetMsgRout(other.mTLP));
      result &= (carbonXPcieTLPGetMsgCode(mTLP)       == carbonXPcieTLPGetMsgCode(other.mTLP));
      result &= (carbonXPcieTLPGetLowAddr(mTLP)       == carbonXPcieTLPGetLowAddr(other.mTLP));
      result &= (carbonXPcieTLPGetByteCount(mTLP)     == carbonXPcieTLPGetByteCount(other.mTLP));
      result &= (carbonXPcieTLPGetFirstBE(mTLP)       == carbonXPcieTLPGetFirstBE(other.mTLP));
      result &= (carbonXPcieTLPGetLastBE(mTLP)        == carbonXPcieTLPGetLastBE(other.mTLP));
      result &= (carbonXPcieTLPGetCplStatus(mTLP)     == carbonXPcieTLPGetCplStatus(other.mTLP));
      result &= (carbonXPcieTLPGetDataByteCount(mTLP) == carbonXPcieTLPGetDataByteCount(other.mTLP));
      result &= (carbonXPcieTLPGetDataLength(mTLP)    == carbonXPcieTLPGetDataLength(other.mTLP));
      result &= (carbonXPcieTLPGetTransId(mTLP)       == carbonXPcieTLPGetTransId(other.mTLP));
      
      for(CarbonUInt32 i = 0; i < carbonXPcieTLPGetDataLength(mTLP); i++) {
	if(carbonXPcieTLPGetDWord(mTLP, i) != carbonXPcieTLPGetDWord(other.mTLP, i)) {
	  result = false;
	}
      }
    }
    else {
      result = (mOpcode == other.mOpcode && mOperand == other.mOperand);
    }
    
    return result;
  }
  
  //! Based On operator==
  bool operator!=(const CarbonXPcieTransReqT &other) const {
    return !(other == *this);
  }
  
  //! Transactor Configuration
  /*! This will setup a configuration transaction, effectively changing the
    this transaction to a config transaction. The transaction can be changed back to a TLP
    transaction by calling any of the TLP field or the High Level TLP methods.
    \param opcode Specifies the paramater to set
    \param operand Value of the parameter if any
  */
  void setConfig(OpcodeT opcode, CarbonUInt64 operand = 0) {
    mOpcode  = opcode;
    mOperand = operand;
    delete mPtrOperand;
    mPtrOperand = 0;
  }
  
  //! Setup Available Buffer Space, in the transactors receiver port
  /*! 
    This will setup a Buffer Space Configuration Transaction, for the receiver
    port of the transactor.
    The buffer sizes are used to advertize the available buffer space
    on the transactors receiving port to the opposite side's transmitting port
    as port of the flow control protocoll, the buffer sizes are specified in FCC
    values each FCC value requires 16 bytes of buffer. I.E an FCC value of 1 
    required 16 bytes of available buffer space. The default FCC value for all 
    buffer types are 0 or Infinate. 
    
    \param vc The virtual channel to setup buffer space for, currently only VC0 is supported.
    \param posted_hdr FCC value for Posted Header Buffer Space
    \param posted_data FCC value for Posted Data Buffer Space
    \param non_posted_hdr FCC value for Non Posted Header Buffer Space
    \param non_posted_data FCC value for Non Data Header Buffer Space
    \param cpl_hdr FCC value for Completion Header Buffer Space
    \param cpl_data FCC value for Completion Data Buffer Space
  */
  void setupBufferSizes(CarbonUInt32 vc, 
			CarbonUInt32 posted_hdr,     CarbonUInt32 posted_data, 
			CarbonUInt32 non_posted_hdr, CarbonUInt32 non_posted_data, 
			CarbonUInt32 cpl_hdr,        CarbonUInt32 cpl_data) {
    mOpcode = eSetupBufferSizes;
    delete [] mPtrOperand; // Delete if it's already used
    mOperand       = 7; // Use Operand to keep track of memory size 
    mPtrOperand    = new CarbonUInt32[7];
    mPtrOperand[0] = vc;
    mPtrOperand[1] = posted_hdr;
    mPtrOperand[2] = posted_data;
    mPtrOperand[3] = non_posted_hdr;
    mPtrOperand[4] = non_posted_data;
    mPtrOperand[5] = cpl_hdr;
    mPtrOperand[6] = cpl_data;
  }

  //! Get Transaction Opcode
  //! \retval CarbonXPcieTransReqT::eSendTLP The transaction is a TLP transaction
  //! \retval other The parameter the transaction is going to set.
  OpcodeT getOpcode() const {
    return mOpcode;
  }

  //! Get Transaction Operand
  //! \return The operand or parameter value of the corresponding opcode 
  CarbonUInt64 getOperand() const {
    return mOperand;
  }
  
  //! Get Transaction Pointer Operand
  //! \return The pointer operand of the corresponding opcode 
  CarbonUInt32 *getPtrOperand() const {
    return mPtrOperand;
  }

  //! Copy Transaction Object
  void copy(const CarbonXPcieTransReqT & in) {
    carbonXPcieTLPCopy(mTLP, in.mTLP);
    mOpcode  = in.mOpcode;
    mOperand = in.mOperand;
  }
  
  //! Sets the command type for the transaction.
  /*!
    \param cmd The 7-bit value to be set.
  */
  void setCmd(CarbonUInt32 cmd) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetCmd(mTLP, cmd);
  }
  
  //! Gets the command type for the transaction.
  /*!
    \return The 7-bit command type value.
  */
  CarbonUInt32 getCmd() const {
    return carbonXPcieTLPGetCmd(mTLP);
  }
  
  //! Sets the command type for the transaction.
  /*!
    \param cmd The CarbonXPcieCmdType value to be set.
  */
  void setCmdByName(CarbonXPcieCmdType cmd) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetCmdByName(mTLP, cmd);
  }
  
  //! Gets the command type for the transaction.
  /*!
    \return The CarbonXPcieCmdType command type value.
  */
  CarbonXPcieCmdType getCmdByName() const{
    return carbonXPcieTLPGetCmdByName(mTLP);
  }
  
  //! Sets the address for the transaction.
  /*!
    If only 32-bits are needed for the transaction then the upper 32-bits
    will be masked out when used.
    \param addr The 64-bit value to be set.
  */
  void setAddr(CarbonUInt64 addr) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetAddr(mTLP, addr);
  }
  
  //! Gets the address for the transaction.
  /*!
    The value will be the same as passed into the setAddr() function,
    even if only 32-bits are used for this transaction.
    \return The 64-bit address value.
  */
  CarbonUInt64 getAddr() const{
    return carbonXPcieTLPGetAddr(mTLP);
  }
    
  //! Sets the requester ID for the transaction.
  /*!
    \param id The 16-bit value to be set.
  */
  void setReqId(CarbonUInt32 id) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetReqId(mTLP, id);
  }
  
  //! Gets the requester ID for the transaction.
  /*!
    \return The 16-bit requester ID.
  */
  CarbonUInt32 getReqId() const{
  return carbonXPcieTLPGetReqId(mTLP);
  }
  
  //! Sets the tag for the transaction.
  /*!
    \param tag The 8-bit value to be set.
  */
  void setTag(CarbonUInt32 tag) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetTag(mTLP, tag);
  }
  
  //! Gets the tag for the transaction.
  /*!
    \return The 8-bit tag value.
  */
  CarbonUInt32 getTag() const {
    return carbonXPcieTLPGetTag(mTLP);
  }
  
  //! Sets the transaction register number and extended register number.
  /*!
    The numeric values are 10-bits wide, corresponding to the concatination
    of the 4-bit extended register number and 6-bit register number.
    \param reg The 10-bit value to be set.
  */
  void setRegNum(CarbonUInt32 reg) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetRegNum(mTLP, reg);
  }
  
  //! Gets the transaction register number and extended register number.
  /*!
    This value is 10-bits wide, corresponding to the concatination of
    the 4-bit extended register number and 6-bit register number.
    \return The 10-bits extended register number and register number.
  */
  CarbonUInt32 getRegNum() const{
    return carbonXPcieTLPGetRegNum(mTLP);
  }
  
  //! Sets the completer ID for the transaction.
  /*!
    \param id The 16-bit value to be set.
  */
  void setCompId(CarbonUInt32 id) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetCompId(mTLP, id);
  }
  
  //! Gets the completer ID for the transaction.
  /*!
    \return The 16-bit completer ID value.
  */
  CarbonUInt32 getCompId() const{
    return carbonXPcieTLPGetCompId(mTLP);
  }
  
  //! Sets the message routing type for the transaction.
  /*!
    \param rout The 3-bit value to be set.
  */
  void setMsgRout(CarbonUInt32 rout) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetMsgRout(mTLP, rout);
  }
  
  //! Gets the message routing type for the transaction.
  /*!
    \return The 3-bit message routing type.
  */
  CarbonUInt32 getMsgRout() const{
    return carbonXPcieTLPGetMsgRout(mTLP);
  }
  
  //! Sets the message code for the transaction.
  /*!
    \param code The 8-bit value to be set.
  */
  void setMsgCode(CarbonUInt32 code) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetMsgCode(mTLP, code);
  }
  
  //! Gets the message code for the transaction.
  /*!
    \return The 8-bit message code.
  */
  CarbonUInt32 getMsgCode() const{
    return carbonXPcieTLPGetMsgCode(mTLP);
  }
  
  //! Sets the lower address for the transaction.
  /*!
    \param addr The 7-bit value to be set.
  */
  void setLowAddr(CarbonUInt32 addr) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetLowAddr(mTLP, addr);
  }
  
  //! Gets the lower address for the transaction.
  /*!
    \return The 7-bit lower address.
  */
  CarbonUInt32 getLowAddr() const{
    return carbonXPcieTLPGetLowAddr(mTLP);
  }
  
  //! Sets the byte count for the transaction.
  /*!
    The Byte Count field in the transaction, which equals the number
    of data bytes sent with a write or requested with a read.  In a
    Completion this may or may not equal the amount of data with the
    transaction.

    \param count The 12-bit value to be set.
  */
  void setByteCount(CarbonUInt32 count) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetByteCount(mTLP, count);
  }
  
  //! Gets the data byte count for the transaction.
  /*!
    \return The 12-bit byte count.
  */
  CarbonUInt32 getByteCount() const{
    return carbonXPcieTLPGetByteCount(mTLP);
  }
  
  //! Sets the first double-word byte enable for the transaction.
  /*!
    \param be The 4-bit value for the FDWBE.
  */
  void setFirstBE(CarbonUInt32 be) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetFirstBE(mTLP, be);
  }
  
  //! Gets the first double-word byte enable value for the transaction.
  /*!
    \return The 4-bit value for the FDWBE.
  */
  CarbonUInt32 getFirstBE() const{
    return carbonXPcieTLPGetFirstBE(mTLP);
  }
  
  //! Sets the last double word byte enable for the transaction.
  /*!
    \param be The 4-bit value for the LDWBE.
  */
  void setLastBE(CarbonUInt32 be) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetLastBE(mTLP, be);
  }
  
  //! Gets the last double-word byte enable value for the transaction.
  /*!
    \return The 4-bit value for the LDWBE.
  */
  CarbonUInt32 getLastBE() const{
    return carbonXPcieTLPGetLastBE(mTLP);
  }
  
  //! Sets the completion status for the transaction.
  /*!
    \param status The 3-bit value to be set. 
  */
  void setCplStatus(CarbonUInt32 status) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetCplStatus(mTLP, status);
  }
  
  //! Gets the completion status for the transaction.
  /*!
    \return The 3-bit completion status.
  */
  CarbonUInt32 getCplStatus() const{
    return carbonXPcieTLPGetCplStatus(mTLP);
  }
    
  //! Sets a Double Word for the transaction.
  /*!
    \param index The index of the DWord being set.
    \param data The value of the DWord being set.
  */
  void setDWord(CarbonUInt32 index, CarbonUInt32 data) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetDWord(mTLP, index, data);
  }
  
  //! Gets the data DWord at the specified index for the transaction.
  /*!
    If the DWord \a index wasn't written then the return data will be 0x00.
     
    \param index The index of the DWord being retrieved.
    \return The DWord at the specified index.
  */
  CarbonUInt32 getDWord(CarbonUInt32 index) const{
    return carbonXPcieTLPGetDWord(mTLP, index);
  }
  
  //! Resizes the byte data buffer
  /*!
    Either extend or truncate the byte data buffer.  If \a start is greater
    than zero then all elements from zero to \a start will be removed.  If
    \a end is before the last element, then all after it will be removed,
    otherwise zero data will be appended to the byte data buffer.

    \param start The index of the first element to keep.
    \param end The index of the last element to keep or create.
  */
  void resizeDataBytes(CarbonUInt32 start, CarbonUInt32 end) {
    carbonXPcieTLPResizeDataBytes(mTLP, start, end);
  }
  
  //! Gets the number of bytes in the data queue
  /*!
    The length of the data queue is from byte 0 through the last byte written.  If only
    byte 11 is written, then the data queue length is 12 bytes.

    \return The number of bytes of data set for the tranaction
  */
  CarbonUInt32 getDataByteCount(void) const{
    return carbonXPcieTLPGetDataByteCount(mTLP);
  }
  
  //! Sets the data length value in double-words
  /*!
    \param len The 10-bit data length for the transaction
  */
  void setDataLength(CarbonUInt32 len) {
    mOpcode = eSendTLP;
    carbonXPcieTLPSetDataLength(mTLP, len);
  }
  
  //! Gets the data length in double-words
  /*!
    \return The 10-bit data length for the transaction
  */
  CarbonUInt32 getDataLength(void) const{
    return carbonXPcieTLPGetDataLength(mTLP);
  }
  
  //! Gets the transaction ID
  /*!
    The transaction ID is a concatination of the Requester ID and the Tag values.  On completion
    transactions this information links the completion to the original request.  For all other
    transactions the return value is undefined.
    \return The transaction ID of the original request on completions.
  */
  CarbonSInt32 getTransId() const{
    return carbonXPcieTLPGetTransId(mTLP);
  }
  
  //! Set the Start Time of the transaction (For Status Purposes Only)  
  /*!
    This Method is meant to be used by the transactor to report the starttime
    of the transaction.
    \param time The start time of the transaction
  */
  void setStartTime(CarbonUInt64 time) {
    carbonXPcieTLPSetStartTick(mTLP, time);
  }
  
  //! Get the Start Time of the transaction
  /*!
    \return The start time of the transaction
    \sa setStartTime
  */
  CarbonUInt64 getStartTime() const {
    return carbonXPcieTLPGetStartTick(mTLP);
  }
  
  //! Set the Start Time of the transaction (For Status Purposes Only)  
  /*!
    This Method is meant to be used by the transactor to report the endtime
    of the transaction.
    \param time The end time of the transaction
  */
  void setEndTime(CarbonUInt64 time) {
    carbonXPcieTLPSetEndTick(mTLP, time);
  }
  
  //! Get the End Time of the transaction
  /*!
    \return The end time of the transaction
    \sa setEndTime
  */
  CarbonUInt64 getEndTime() const {
    return carbonXPcieTLPGetEndTick(mTLP);
  }
  
  // Ease of use functions
  //! Gets whether the transaction is a read
  /*!
    \retval true If the command is a memory, i/o, or config read
    \retval false If the command is not a read
  */
  bool isRead() const{
    return carbonXPcieTLPIsRead(mTLP);
  }
  
  //! Gets whether the transaction is a write
  /*!
    \retval true If the command is a memory, i/o, or config write
    \retval false If the command is not a write
  */
  bool isWrite() const{
    return carbonXPcieTLPIsWrite(mTLP);
  }
  
  //! Returns a Pointer to the CarbonXPcieID Object  
  CarbonXPcieTLP * tlp(){
    return mTLP;
  }
  
  //! Returns a Pointer to the CarbonXPcieID Object (const version)
  const CarbonXPcieTLP * tlp() const {
    return mTLP;
  }

  //! Build a Memory Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param num_dwords Number of DWORDS to read, goes in the Length Field of the TLP
    \param first_be First BE Field
    \param last_be  Last  BE Field
  */
  void memReadReq(CarbonUInt8 tag, 
		  CarbonUInt64 addr, CarbonUInt32 num_dwords, 
		  CarbonUInt32 first_be, CarbonUInt32 last_be) {
    carbonXPcieTLPMemReadReq(mTLP, tag, addr, num_dwords, first_be, last_be);
  }
  
  //! Build Completer TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory Completer Transaction. 
    The function populates the following fields in the supplied TLP:
    Type, Length, Tag, Requester ID HDW Byte Count, Lower Address and Data
    The other fields are left unchanged!
    \param req_id Bus-, Device- and Funcion- Number of the corresponding Requester Transaction 
    \param tag Tag of the corresponding Requester Transaction
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS in the TLP
    \param byte_count Byte Count Field
    \param lower_addr Lower Address Field
  */
  void completer(CarbonUInt32 req_id, CarbonUInt8 tag,
		 const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
		 CarbonUInt8  byte_count, CarbonUInt8 lower_addr) {
    carbonXPcieTLPCompleter(mTLP, req_id, tag, data, num_dwords, byte_count, lower_addr);
  }
  
  //! Build a Memory Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS to write, goes in the Length Field of the TLP
    \param first_be First BE Field
    \param last_be  Last  BE Field
  */
  void memWriteReq(CarbonUInt8 tag,
		   CarbonUInt64 addr, const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
		   CarbonUInt32 first_be, CarbonUInt32 last_be) {
    carbonXPcieTLPMemWriteReq(mTLP, tag, addr, data, num_dwords, first_be, last_be);
  }
  
  //! Build an IO Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a IO
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param first_be First BE Field
  */
  void ioRead32Req(CarbonUInt8 tag, 
		   CarbonUInt64 addr,
		   CarbonUInt32 first_be) {
    carbonXPcieTLPIORead32Req(mTLP, tag, addr, first_be);
  }

  //! Build a IO Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a IO
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void ioWrite32Req(CarbonUInt8 tag, 
		    CarbonUInt64 addr, CarbonUInt32 data, 
		    CarbonUInt32 first_be) {
    carbonXPcieTLPIOWrite32Req(mTLP, tag, addr, data, first_be);
  }
  
  //! Build an Cfg 0 Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr Address HDW
    \param first_be First BE Field
  */
  void cfg0ReadReq(CarbonUInt8 tag, 
		   CarbonUInt32 addr, 
		   CarbonUInt32 first_be) {
    carbonXPcieTLPCfg0ReadReq(mTLP, tag, addr, first_be);
  }
  
  //! Build an Cfg 1 Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 1
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr Address HDW
    \param first_be First BE Field
  */
  void cfg1ReadReq(CarbonUInt8 tag, 
		   CarbonUInt32 addr, 
		   CarbonUInt32 first_be) {
    carbonXPcieTLPCfg1ReadReq(mTLP, tag, addr, first_be);
  }

  //! Build a Cfg 0 Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs.
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void cfg0WriteReq(CarbonUInt8 tag, 
		    CarbonUInt32 addr, CarbonUInt32 data, 
		    CarbonUInt32 first_be) {
    carbonXPcieTLPCfg0WriteReq(mTLP, tag, addr, data, first_be);
  }
  
  //! Build a Cfg 1 Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 1
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs.
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void cfg1WriteReq(CarbonUInt8 tag, 
		    CarbonUInt32 addr, CarbonUInt32 data, 
		    CarbonUInt32 first_be) {
    carbonXPcieTLPCfg1WriteReq(mTLP, tag, addr, data, first_be);
  }
  
  //! Build an Message Base Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, MsgRout and MsgCode
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS in the TLP
    \param rout Msg Rout Field
    \param msg_code Msg Code Field
  */
  void msgBaseReq(CarbonUInt8 tag, 
		  const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
		  CarbonUInt8 rout, CarbonUInt8 msg_code) {
    carbonXPcieTLPMsgBaseReq(mTLP, tag, data, num_dwords, rout, msg_code);
  }

private:
  OpcodeT         mOpcode;
  CarbonXPcieTLP *mTLP;
  CarbonUInt64    mOperand;
  CarbonUInt32   *mPtrOperand;
};

//! Streaming Operator
/*!
  Outputs a \a CarbonXPcieTransReqT Transaction Structure to the given stream
*/
inline std::ostream & operator<<(std::ostream &os, const CarbonXPcieTransReqT &obj) {
  if(obj.getOpcode() == CarbonXPcieTransReqT::eSendTLP) {
    os << "Cmd:        " << carbonXPcieTLPGetStrFromType(carbonXPcieTLPGetCmdByName(obj.tlp())) << std::endl
       << "Addr:       0x" << std::hex << carbonXPcieTLPGetAddr(obj.tlp()) << std::endl
       << "ReqId:      0x" << carbonXPcieTLPGetReqId(obj.tlp()) << std::endl
       << "Tag:        0x" << carbonXPcieTLPGetTag(obj.tlp()) << std::endl
       << "Reg Num:    0x" << carbonXPcieTLPGetRegNum(obj.tlp()) << std::endl
       << "CompId:     0x" << carbonXPcieTLPGetCompId(obj.tlp()) << std::endl
       << "MsgRout:    0x" << carbonXPcieTLPGetMsgRout(obj.tlp()) << std::endl
       << "MsgCode:    0x" << carbonXPcieTLPGetMsgCode(obj.tlp()) << std::endl
       << "LowAddr:    0x" << carbonXPcieTLPGetLowAddr(obj.tlp()) << std::endl
       << "ByteCount:  0x" << carbonXPcieTLPGetByteCount(obj.tlp()) << std::endl
       << "FirstBE:    0x" << carbonXPcieTLPGetFirstBE(obj.tlp()) << std::endl
       << "LastBE:     0x" << carbonXPcieTLPGetLastBE(obj.tlp()) << std::endl
       << "CplStatus:  0x" << carbonXPcieTLPGetCplStatus(obj.tlp()) << std::endl
       << "D ByteCount:0x" << carbonXPcieTLPGetDataByteCount(obj.tlp()) << std::endl
       << "DataLength: 0x" << carbonXPcieTLPGetDataLength(obj.tlp()) << std::endl
       << "TransId:    0x" << carbonXPcieTLPGetTransId(obj.tlp()) << std::endl
       << "Payload:"                                             << std::endl;
    
    for(CarbonUInt32 i = 0; i <carbonXPcieTLPGetDataLength(obj.tlp()); i++)
      os << " " << std::dec << i << ": 0x" << std::setw(4) << std::setfill('0') << std::hex << carbonXPcieTLPGetDWord(obj.tlp(), i) << std::endl;
    
  }

  return os;
}

//! Response Transaction class is same a Request transaction class
typedef CarbonXPcieTransReqT CarbonXPcieTransRespT ;

/*!
  @}
*/

#endif
