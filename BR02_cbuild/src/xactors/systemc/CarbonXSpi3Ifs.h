// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxspi3ifs__
#define __carbonxspi3ifs__

#include "xactors/systemc/CarbonXIfs.h"
#include "xactors/spi3/carbonXSpi3LnkTx.h"
#include "xactors/spi3/carbonXSpi3LnkRx.h"

class CarbonXSpi3TransReqT : public CarbonXTransReqT {
 public:

  // Constructor
  CarbonXSpi3TransReqT(UInt32 buf_size = 0) : CarbonXTransReqT(buf_size) {
  }
  
  void setPortId(UInt32 id, UInt32 index) {
    transaction(OP_SET_PORT_ID, index, &id, 4, 0, TBP_WIDTH_32);
  }
  void setPortOrder(UInt32 *order, UInt32 size) {
    transaction(OP_SET_PORT_ORDER, 0, order, size*4, 0, TBP_WIDTH_32);
  }
  void setPortOrder(std::vector<UInt32> order) {
    transaction(OP_SET_PORT_ORDER, 0, &order[0], order.size()*4, 0, TBP_WIDTH_32);
  }

  static const UInt32 OP_SET_PORT_ID    = 0x80000001;
  static const UInt32 OP_SET_PORT_ORDER = 0x80000002;

};  


#endif
  
    
