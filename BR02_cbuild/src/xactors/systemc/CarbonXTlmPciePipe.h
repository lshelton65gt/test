// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef __carbonxtlmpciePipe__
#define __carbonxtlmpciePipe__

#include "xactors/systemc/CarbonXTlmPcieTransactor.h"

/*!
    \addtogroup PcieSystemC
    @{
*/

//! PIPE Interface SystemC Transactor Module
/*!
  This class defines the SystemC signal ports for the PIPE interface
  The class is templated with the link width of the PCI Express link.
  Legal link widths are: 1, 2, 4, 8, 12, 16 and 32. The signal ports
  uses the SystemC multi-port feature, so the ports need to be connected
  to the same number of channels that are specified by the template parameter.
  The Transaction side interface for the transactor is common for all signal
  interfaces and is defined by the CarbonXTlmPcieTransactor class which is a
  parent of this class.
*/
template<int LINKWIDTH = 1>
class CarbonXTlmPciePipe : public CarbonXTlmPcieTransactor {
public:
  //! SystemC Contructor Declaration
  SC_HAS_PROCESS(CarbonXTlmPciePipe);
 
  //! \name SystemC Signal Ports
  // @{
  
  //! Electrical Idle Output. One bit wide. One port per lane on the interace
  /*! 
    This is the Electrical Idle Output. Whether it's active high
    or low can be configured in the transactor.
  */
  sm_port<bool, LINKWIDTH>                           rxElecIdle;

  //! Data Output Eight bits wide. One port per lane on the interace
  /*
    ! This is the 8 bit data input bus to the transactor.
  */
  sm_port<sc_uint<8>, LINKWIDTH>                     rxData;

  //! Rx Data K Output. One bit wide. One port per lane on the interace
  /*!
    Data/Control for the symbols of rxData.
    A value of zero indicates a data byte, a value of 1
    indicates a control byte.
  */
  sm_port<bool, LINKWIDTH>                           rxDataK;

  sm_port<sc_uint<2>, LINKWIDTH>                     rxStatus;
  
  //! Rx Valid Output. Two bits wide. One port per lane on the interace
  /*!
    Indicates symbol lock and valid data on
    RxData and RxDataK. The transactor currently
    assert this signal when the transmitter is NOT
    in Electrical Idle.
  */
  sm_port<bool, LINKWIDTH>                           rxValid;

  //! PHY Status Output. One bit wide. One port for the entire interface.
  /*!
    Used to communicate completion of several
    PHY functions including power management
    state transitions, and receiver detection.
  */
  sm_port<bool, 1>                                   phyStatus;


  //! Electrical Idle Input. One bit wide. One port per lane on the interace
  /*! 
    This is the Electrical Idle Input. Whether it's active high
    or low can be configured in the transactor.
  */
  sc_port<sc_signal_in_if<bool>, LINKWIDTH >         txElecIdle;

  //! Data Input. Eight bits wide. One port per lane on the interace
  /*
    ! This is the 8 bit data input bus to the transactor.
  */
  sc_port<sc_signal_in_if<sc_uint<8> >, LINKWIDTH >  txData;

  //! Tx Data K Input. One bit wide. One port per lane on the interace
  /*!
    Data/Control for the symbols of txData.
    A value of zero indicates a data byte, a value of 1
    indicates a control byte.
  */
  sc_port<sc_signal_in_if<bool>, LINKWIDTH >         txDataK;
  
  //! Power Down Input. 2 bits wide. One port for the whole interface
  /*!
    Power up or down the transceiver.
    The transactor currently do not use this input.
  */
  sc_port<sc_signal_in_if<bool> >                    powerDown;

  //! Active Low Reset Signal Input. One bit wide. One port for the whole interface
  /*!
    Resets the transmitter and receiver in the PHY interface
    note that this does not reset the transactor since that
    would logically be on the other side of the link from the PHY
  */
  sc_port<sc_signal_in_if<bool> >                    resetN;

  //! TxDetectRx/Loopback. One bit wide. One port for the whole interface
  /*!
    Used to tell the PHY to begin a receiver detection operation 
    or to begin loopback.
    Loopback is not supported by the transactor. This input is only 
    used to create an expected response on the PhyStatus and RxStatus
    ports, to let the DUT function properly. No internal state in the
    transactor is affected.
  */
  sc_port<sc_signal_in_if<bool> >                   txDetectRx;

  //! Rx Polarity Input. One bit wide. One port per lane on the interace
  /*!
    Tells PHY to do a polarity inversion on the received data.
    This function is not supported by the transactor.
  */
  sc_port<sc_signal_in_if<bool>, LINKWIDTH>         rxPolarity;
  
  //! Tx Compliance. One bit wide. One port per lane on the interace
  /*!
    Sets the running disparity to negative. Used when transmitting 
    the compliance pattern.
    This function is not supoprted by the transactor.
  */
  sc_port<sc_signal_in_if<bool>, LINKWIDTH>         txCompliance;

  // @}

  //! Constructor
  CarbonXTlmPciePipe(sc_module_name instname) : 
    CarbonXTlmPcieTransactor(eCarbonXPciePipeIntf, LINKWIDTH, instname) ,
    rxElecIdle(eCarbonXPcieRxElecIdle, mPcieInst, "rxElecIdle"),
    rxData(eCarbonXPcieRxData, mPcieInst, "rxData"),
    rxDataK(eCarbonXPcieRxDataK, mPcieInst, "rxDataK"),
    rxStatus(eCarbonXPcieRxStatus, mPcieInst, "rxStatus"),
    rxValid(eCarbonXPcieRxValid, mPcieInst, "rxValid"),
    phyStatus(eCarbonXPciePhyStatus, mPcieInst, "phyStatus"),
    txElecIdle("txElecIdle"),
    txData("txData"),
    txDataK("txDataK"),
    powerDown("powerDown"),
    resetN("resetN"),
    txDetectRx("txDetectRx"),
    rxPolarity("rxPolarity"),
    txCompliance("txCompliance")
  {  
    init();
  }
  
private:
#ifndef CARBON_EXTERNAL_DOC
  
  void init() {
    mInitialized = false;
    for(int i = 0; i < LINKWIDTH; i++) {
      mNetTxData[i]     = carbonXPcieGetNet(mPcieInst, eCarbonXPcieTxData, i);
      mNetTxDataK[i]    = carbonXPcieGetNet(mPcieInst, eCarbonXPcieTxDataK, i);
      mNetTxElecIdle[i] = carbonXPcieGetNet(mPcieInst, eCarbonXPcieTxElecIdle, i);
      mNetRxPolarity[i]    = carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxPolarity, i);
      mNetTxCompliance[i]  = carbonXPcieGetNet(mPcieInst, eCarbonXPcieTxCompliance, i);
    }

    mNetPowerDown        = carbonXPcieGetNet(mPcieInst, eCarbonXPciePowerDown, 0);
    mNetResetN           = carbonXPcieGetNet(mPcieInst, eCarbonXPcieResetN, 0);
    mNetTxDetectRx       = carbonXPcieGetNet(mPcieInst, eCarbonXPcieTxDetectRx, 0);

    SC_METHOD(processTransaction);
    sensitive << pcieRx.ok_to_get();

    SC_METHOD(sendTransaction);
    sensitive << pcieTx.ok_to_put();

    SC_METHOD(step);
  }

  // Read all inputs every cycle
  void step() {
    if(mInitialized) {
      for(int i = 0; i < LINKWIDTH; i++) {
	carbonXPcieNetDeposit(mNetTxElecIdle[i], txElecIdle[i]->read()); 
	carbonXPcieNetDeposit(mNetTxData[i], txData[i]->read()); 
	carbonXPcieNetDeposit(mNetTxDataK[i], txDataK[i]->read()); 
	carbonXPcieNetDeposit(mNetRxPolarity[i], rxPolarity[i]->read()); 
	carbonXPcieNetDeposit(mNetTxCompliance[i], txCompliance[i]->read()); 
      }
      carbonXPcieNetDeposit(mNetResetN, resetN->read()); 
      carbonXPcieNetDeposit(mNetTxDetectRx, txDetectRx->read()); 
      carbonXPcieNetDeposit(mNetPowerDown, powerDown->read()); 
      CarbonXTlmPcieTransactor::step();
    }
    else {
      // Drive Initial values
      CarbonUInt32 phystatus;
      carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPciePhyStatus, 0), &phystatus);
      phyStatus->write(phystatus);
      for(int i = 0; i < LINKWIDTH; i++) {
        CarbonUInt32 eidle, data, datak, rxstatus, rxvalid;
        carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxData, i), &data);
        carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxDataK, i), &datak);
        carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxElecIdle, i), &eidle);
        carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxStatus, i), &rxstatus);
        carbonXPcieNetExamine(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxValid, i), &rxvalid);
        rxElecIdle[i]->write(eidle);
        rxData[i]->write(data);
        rxDataK[i]->write(datak);
        rxStatus[i]->write(rxstatus);
        rxValid[i]->write(rxvalid);
      }
      mInitialized = true;
    }
    next_trigger(mSymbolPeriod);
  }

  // Input Net Member Variables
  CarbonXPcieNet *mNetTxData[LINKWIDTH];
  CarbonXPcieNet *mNetTxDataK[LINKWIDTH];
  CarbonXPcieNet *mNetTxElecIdle[LINKWIDTH];
  CarbonXPcieNet *mNetRxPolarity[LINKWIDTH];
  CarbonXPcieNet *mNetTxCompliance[LINKWIDTH];
  CarbonXPcieNet *mNetPowerDown;
  CarbonXPcieNet *mNetResetN;
  CarbonXPcieNet *mNetTxDetectRx;
  
  //! We don't want to call the model the first time around
  // (Can't use dont_initialize() when using next_trigger
  bool            mInitialized;

#endif
};
  
/*!
  @}
*/

#endif
