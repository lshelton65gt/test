// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxspi4ifs__
#define __carbonxspi4ifs__

#include "xactors/systemc/CarbonXIfs.h"
#include "xactors/spi4/carbonXSpi4Src.h"
#include "xactors/spi4/carbonXSpi4Snk.h"
#include "xactors/spi4/carbonXSpi4.h"

class CarbonXSpi4TransReqT : public CarbonXTransReqT {
 public:

  // Constructor
  CarbonXSpi4TransReqT(UInt32 buf_size = 0) : CarbonXTransReqT(buf_size) {
  }
  
  void setPortId(UInt32 id, UInt32 index) {
    transaction(OP_SET_PORT_ID, index, &id, 4, 0, TBP_WIDTH_32);
    //std::cout << "setPortId: Opcode = " << hex << getOpcode() << endl; 
  }
  void setPortOrder(UInt32 *order, UInt32 size) {
    transaction(OP_SET_PORT_ORDER, 0, order, size*4, 0, TBP_WIDTH_32);
  }
  void setPortOrder(const std::vector<UInt32> &order) {
    transaction(OP_SET_PORT_ORDER, 0, (&order[0]), order.size()*4, 0, TBP_WIDTH_32);
  }
  void startup() {
    transaction(OP_STARTUP, 0, NULL, 0, 0, TBP_WIDTH_32);
    //std::cout << "Startup, Opcode = " << hex << getOpcode() << std::endl;
  }
  void setCalPortMap(SInt32 *map, UInt32 size ) {
    transaction(OP_SET_CAL_PORTMAP, 0, map, size*4, 0, TBP_WIDTH_32);
  }
  void setCalPortMap(const std::vector<UInt32> &calMap) {
    transaction(OP_SET_CAL_PORTMAP, 0, &calMap[0], 4*calMap.size(), 0, TBP_WIDTH_32);
  }
  void setCalMaxBurst1(SInt32 port, SInt32 maxB1){
    transaction(OP_SET_MAXBURST1, port, &maxB1, 4, 0, TBP_WIDTH_32);
  }
  void setCalMaxBurst2(SInt32 port, SInt32 maxB2){
    transaction(OP_SET_MAXBURST2, port, &maxB2, 4, 0, TBP_WIDTH_32);
  }
  void setWaterMark1(SInt32 port, SInt32 wm1){
    transaction(OP_SET_WATERMARK1, port, &wm1, 4, 0, TBP_WIDTH_32);
  }
  void setWaterMark2(SInt32 port, SInt32 wm2){
    transaction(OP_SET_WATERMARK2, port, &wm2, 4, 0, TBP_WIDTH_32);
  }
  void setDataMaxT(SInt32 dataMaxT ){
    transaction(OP_SET_DATAMAXT, dataMaxT, NULL, 0, 0, TBP_WIDTH_32);
  }
  void setFifoMaxT(SInt32 fifoMaxT ){
    transaction(OP_SET_FIFOMAXT, fifoMaxT, NULL, 0, 0, TBP_WIDTH_32);
  }
  static const UInt32 OP_STARTUP          = CARBONXSPI4_OP_STARTUP;
  static const UInt32 OP_SET_PORT_ID      = CARBONXSPI4_OP_SET_PORT_ID;
  static const UInt32 OP_SET_PORT_ORDER   = CARBONXSPI4_OP_SET_PORT_ORDER;
  static const UInt32 OP_SET_CAL_PORTMAP  = CARBONXSPI4_OP_SET_CAL_PORTMAP;
  static const UInt32 OP_SET_MAXBURST1    = CARBONXSPI4_OP_SET_CAL_MAXB1;
  static const UInt32 OP_SET_MAXBURST2    = CARBONXSPI4_OP_SET_CAL_MAXB2;
  static const UInt32 OP_SET_DATAMAXT     = CARBONXSPI4_OP_SET_DATAMAXT;
  static const UInt32 OP_SET_FIFOMAXT     = CARBONXSPI4_OP_SET_FIFOMAXT;
  static const UInt32 OP_SET_ALPHA        = CARBONXSPI4_OP_SET_ALPHA;
  static const UInt32 OP_SET_GAMMAVALID   = CARBONXSPI4_OP_SET_GAMMAVALID;
  static const UInt32 OP_SET_GAMMAINVALID = CARBONXSPI4_OP_SET_GAMMAINVALID;
  static const UInt32 OP_SET_DELTAVALID   = CARBONXSPI4_OP_SET_DELTAVALID;
  static const UInt32 OP_SET_DELTAINVALID = CARBONXSPI4_OP_SET_DELTAINVALID;
  static const UInt32 OP_SET_WATERMARK1   = CARBONXSPI4_OP_SET_WATERMARK1;
  static const UInt32 OP_SET_WATERMARK2   = CARBONXSPI4_OP_SET_WATERMARK2;

  static const  UInt32  STARVING  = 0;
  static const  UInt32  HUNGRY    = 1;
  static const  UInt32  SATISFIED = 2;
};  


#endif
  
    
