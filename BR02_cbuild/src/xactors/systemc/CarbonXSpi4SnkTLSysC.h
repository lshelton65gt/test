// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __CarbonXSpi4SnkTLSysC__
#define __CarbonXSpi4SnkTLSysC__

#include <systemc.h>
#include "xactors/carbonX.h"
#include "xactors/systemc/CarbonXBase.h"
#include "CarbonXSpi4Ifs.h"
#include <string>
#include <vector>
#include <map>
#include <deque>

template <int NPORTS = 1>
class CarbonXSpi4SnkTLSysC : 
  public CarbonXBase
{

public:
  
  sc_port<CarbonXSlaveIf<CarbonXTransReqT, CarbonXTransRespT > > config_port;
  sc_port<CarbonXPutIf<CarbonXTransRespT>, NPORTS >              rx_port; // NPORTS Number of Port Connections

  // constructor
  CarbonXSpi4SnkTLSysC(const sc_module_name instname, const char *xtor_path) : 
    CarbonXBase(instname, xtor_path),
    config_port("config_port"),
    rx_port("rx_port") {
    mIsRunning = false;
  }
  
 private :
  
  // carbonX Attach methods
  void transactionLoop() {
    CarbonXTransRespT rdResp(MAX_PACKET_SIZE);

    // Write Default Setup to Transactor
    setupDefaults();
    while (1) {
      
      // Check for configuration transactions
      processConf();
      
      if(mIsRunning) {

	// Get Transaction
	SInt32 port, eop, sop, size = MAX_PACKET_SIZE;
	SInt32 cycles = carbonXSpi4SnkPkt(rdResp.getData(), &size, &sop, &eop, &port); 
	
	// If Start of packet, clear before adding data
	if(sop) mPkt[port].resize(0);
	
	// Add the Data payload to the packet
	mPkt[port].insert(mPkt[port].end(),rdResp.getData8(), rdResp.getData8()+size); 
	
	// If End Of Packet, send it off
	if(eop) {
	  mResp.transaction(TBP_WRITE, 0, &mPkt[port][0], mPkt[port].size(), 0, TBP_WIDTH_8);
	  rx_port[mPortIds[port]]->put(mResp);
	  addByteCnt(port, mPkt[port].size());
	  mPkt[port].resize(0);
	}

	updateCalendar();
      }
      else carbonXSpi4SnkIdle(1);
    }
  }
  
  // Iterate over all Ports and update Receiving Fifo Status
  void updateCalendar() {
    for(std::map<UInt32,UInt32>::iterator port = mPortIds.begin(); port != mPortIds.end(); ++port) {
      UInt32 num_bytes  = calcFifoDepth(port->first, rx_port[port->second]->used());
      UInt32 port_state = calcPortStatus(port->first, num_bytes); 
      carbonXSpi4SnkSetCalPortState(port->first, port_state);
    }
  }
  
  void addByteCnt(UInt32 port, UInt32 cnt) {
    mByteCnt[port].push_front(mPkt[port].size());
  }

  UInt32 calcFifoDepth(UInt32 port, UInt32 used) {
    UInt32 num_bytes = 0;
    std::deque<UInt32>::iterator qi = mByteCnt[port].begin();
    for(UInt32 i = 0; i < used; i++, qi++)
      num_bytes += *qi;
    
    // The remainding packets in mByteCnt must already have been taken
    // off the que, so erase them
    mByteCnt[port].erase(qi, mByteCnt[port].end());
    return num_bytes;
  }

  UInt32 calcPortStatus(UInt32 port, UInt32 num_bytes) {
    return ( (num_bytes < mWaterMark1[port]) ? 0 :
	     (num_bytes < mWaterMark2[port]) ? 1 : 2 );
  }

  void processConf() {
    
    while(config_port->nb_get(mReq)){
      UInt32 index, id;
      switch(mReq.getOpcode()) {
      case CarbonXSpi4TransReqT::OP_SET_PORT_ID :
	index = mReq.getAddress();
        id    = *(reinterpret_cast<UInt32 *>(mReq.getData()));
	mPortIds[id]  = index;
	mResp.transaction(CARBONX_OP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
	mResp.setTransId(mReq.getTransId());
	break;
      case CarbonXSpi4TransReqT::OP_SET_PORT_ORDER :
	break;
      case CarbonXSpi4TransReqT::OP_SET_WATERMARK1 :
	index = mReq.getAddress();
	mWaterMark1[index] = *(reinterpret_cast<UInt32 *>(mReq.getData()));
	mResp.transaction(CARBONX_OP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
	break;
      case CarbonXSpi4TransReqT::OP_SET_WATERMARK2 :
	index = mReq.getAddress();
	mWaterMark2[index] = *(reinterpret_cast<UInt32 *>(mReq.getData()));
	mResp.transaction(CARBONX_OP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
	break;
      case CarbonXSpi4TransReqT::OP_STARTUP :
	mIsRunning = true;
      default :
	carbonXSpi4SnkProcessConf(&mReq, &mResp);
	carbonXSpi4SnkGetEnableDip4Check();
	break;
      }
      config_port->put(mResp);
    }
  }

  void setupDefaults() {
    carbonXSpi4SnkSetCalLen(16);
    carbonXSpi4SnkSetCalM(1);
    carbonXSpi4SnkSetDataMaxT(100);
    carbonXSpi4SnkSetFifoMaxT(100);
    carbonXSpi4SnkSetAlpha(1);
    carbonXSpi4SnkSetGammaValid(5);
    carbonXSpi4SnkSetGammaInvalid(1);
  }

  bool                                  mIsRunning;
  CarbonXTransReqT                      mReq;
  CarbonXTransRespT                     mResp;
  
  std::map<UInt32, UInt32>              mPortIds;
  std::map<UInt32, UInt32>              mWaterMark1;
  std::map<UInt32, UInt32>              mWaterMark2;

  std::map<UInt32, std::vector<UInt8> > mPkt;
  
  std::map<UInt32, std::deque<UInt32> > mByteCnt;
  static const UInt32 MAX_PACKET_SIZE   = 1000;
};

#endif
