// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef _carbonx_tlm_req_rsp_channel_h_
#define _carbonx_tlm_req_rsp_channel_h_

#include "tlm.h"
#include "xactors/systemc/CarbonXIfs.h"
#include "xactors/systemc/CarbonXTlmFifo.h"
#include "xactors/systemc/CarbonXTlmMasterSlaveImp.h"

template < typename REQ , typename RSP >
class CarbonXTlmReqRspChannel : public sc_module
{
public:
  // uni-directional slave interface

  sc_export< CarbonXGetIf< REQ > >           get_request_export;
  sc_export< CarbonXPutIf< RSP > >           put_response_export;

  // uni-directional master interface

  sc_export< tlm::tlm_fifo_put_if< REQ > >   put_request_export;
  sc_export< tlm::tlm_fifo_get_if< RSP > >   get_response_export;

  // master / slave interfaces

  sc_export< tlm::tlm_master_if< REQ , RSP > >  master_export;
  sc_export< CarbonXSlaveIf< REQ , RSP > >      slave_export;


  CarbonXTlmReqRspChannel( int req_size = 0 , int rsp_size = 0 ) :
    sc_module( sc_module_name( sc_gen_unique_name("CarbonXTlmReqRspChannel") ) ) ,
    master( request_fifo , response_fifo ) , 
    slave( request_fifo , response_fifo ) ,
    request_fifo( "request_fifo", req_size ) ,
    response_fifo( "response_fifo", rsp_size ) {

    bind_exports();
  }

  CarbonXTlmReqRspChannel( sc_module_name module_name ,
		       int req_size = 0 , int rsp_size = 0 ) :
    sc_module( module_name  ) ,
    master( request_fifo , response_fifo ) , 
    slave( request_fifo , response_fifo ) ,
    request_fifo( "request_fifo", req_size ) ,
    response_fifo( "response_fifo", rsp_size ) {

    bind_exports();
  }

private:

  void bind_exports() {

    put_request_export( request_fifo );
    get_request_export( request_fifo );
    
    put_response_export( response_fifo );
    get_response_export( response_fifo );

    master_export( master );
    slave_export( slave );

  }

protected:

  CarbonXTlmMasterImp< REQ , RSP > master;
  CarbonXTlmSlaveImp< REQ , RSP >  slave;

  CarbonXTlmPutFifo<REQ> request_fifo;
  CarbonXTlmGetFifo<RSP> response_fifo;
};

#endif
