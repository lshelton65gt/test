// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __CarbonXSpi3LnkRxTLSysC__
#define __CarbonXSpi3LnkRxTLSysC__

#include <systemc.h>
#include "xactors/carbonX.h"
#include "xactors/systemc/CarbonXBase.h"
#include "CarbonXSpi3Ifs.h"
#include <string>
#include <vector>
#include <map>

template <int NPORTS = 1>
class CarbonXSpi3LnkRxTLSysC : 
  public CarbonXBase
{

public:
  
  sc_port<CarbonXSlaveIf<CarbonXTransReqT, CarbonXTransRespT > > config_port;
  sc_port<CarbonXPutIf<CarbonXTransRespT>, NPORTS >              rx_port; // NPORTS Number of Port Connections

  // constructor
  CarbonXSpi3LnkRxTLSysC(const sc_module_name instname, const char *xtor_path) : 
    CarbonXBase(instname, xtor_path),
    config_port("config_port"),
    rx_port("rx_port") {
  }
  
 private :
  
  // carbonX Attach methods
  void transactionLoop() {
    // Setup the Request
    CarbonXTransReqT  rdReq;
    CarbonXTransRespT rdResp;
    
    // Setup the Read Request
    rdReq.readArray8Req(0, 0);

    while (1) {
      
      // Check for configuration transactions
      processConf();
      
      // Get Transaction
      carbonXReqRespTransaction(&rdReq, &rdResp);
      UInt32 port = (rdResp.getStatus() >> 2) & 0xff;  
      bool   eop  = (rdResp.getStatus() >> 1) & 0x1;
      bool   sop  = rdResp.getStatus() & 0x1;
      
      // If Start of packet, clear before adding data
      if(sop) mPkt[port].resize(0);

      // Add the Data payload to the packet
      for(UInt32 i = 0; i < rdResp.getSize(); i++)
	mPkt[port].push_back( *(rdResp.getData8()+i));
       
      // If End Of Packet, send it off
      if(eop) {
	mResp.transaction(TBP_WRITE, 0, &mPkt[port][0], mPkt[port].size(), 0, TBP_WIDTH_8);
	rx_port[mPortIds[port]]->put(mResp);
      }
    }
  }
  
  void processConf() {
    
    while(config_port->nb_get(mReq)){
      switch(mReq.getOpcode()) {
      case CarbonXSpi3TransReqT::OP_SET_PORT_ID :
      {
	UInt32 index = mReq.getAddress();
	UInt32 id    = *(reinterpret_cast<UInt32 *>(mReq.getData()));
	mPortIds[id]  = index;
	mResp.transaction(CARBONX_OP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
	mResp.setTransId(mReq.getTransId());
	break;
      }
      default :
	carbonXReqRespTransaction(&mReq, &mResp);
      }
      config_port->put(mResp);
    }
  }

  CarbonXTransReqT                      mReq;
  CarbonXTransRespT                     mResp;

  std::map<UInt32, UInt32>              mPortIds;
  std::map<UInt32, std::vector<UInt8> > mPkt;
  std::map<UInt32, UInt32>              mBytesRecvd;
};

#endif
