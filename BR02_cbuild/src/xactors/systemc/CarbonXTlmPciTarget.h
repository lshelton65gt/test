// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxtlmpcitarget__
#define __carbonxtlmpcitarget__

#include "systemc.h"
#include "tlm.h"
#include "xactors/systemc/CarbonXPciIfs.h"
#include "xactors/systemc/CarbonXPciConfigRegs.h"

class CarbonXTlmPciTarget : public sc_module {  
public:

  SC_HAS_PROCESS(CarbonXTlmPciTarget);
  
  // Ports
  sc_port<tlm::tlm_master_if<CarbonXTransReqT, CarbonXTransRespT> > xaction_port;
  sc_export<CarbonXPciSlaveConfigureIF>                             config;

  // Constructor
  CarbonXTlmPciTarget(sc_module_name _name) : 
    sc_module(_name),
    mCfgSpace(64)
  {
    mMemSpace = carbonXSparseMemCreateNew();
    mIOSpace  = carbonXSparseMemCreateNew();
    
    // Bind Config Interface
    config(mCfgSpace);

    // SC Methods
    SC_METHOD(configureMethod);
    SC_METHOD(slaveMethod);
    sensitive << ok_to_get();
  }
  
  void configureMethod() {

    // Kick Start Transactor
    mResp.csWrite(0, 1<<28);
    xaction_port->put(mResp);
  }
  
  void slaveMethod() {
    
    int status = 0;
    
    // Check if any transaction are available and if so, get them of the queue
    while(xaction_port->nb_get(mReq)) {
      
      // Check if we have CS_WRITE responses to receive
      // We Set the Transaction ID to 0x80000000 below when we write the CS_WRITE transaction so we know how
      // to recognize the responses, if we do generate a response to the CS_WRITE responses we 
      // will get out of sync in the link module, so be careful if editing this code.
      // If it is a CS_WRITE response, we don't want to do anything
      if( !(mReq.getTransId() & 0x80000000) ) {

	// We got here so we have a request. Process it and send back the response
	int op = mReq.getOpcode() & 0x1f;
	
// 	MSG_Milestone("%s Slave:          A:%08llx  OP:%08x SZ:%3d  DT:0x%08x_%08x ST:%08x STARTTIME: %08lld ns ENDTIME: %08lld ns\n",
// 		      name(), mReq.getAddress(), mReq.getOpcode(), mReq.getSize(), mReq.getData32()[1], mReq.getData32()[0], mReq.getStatus(), mReq.getStartTime(), mReq.getEndTime() 
// 		      );
        
	switch(op) {
	case PCI_CFG_WRITE_CMP :
	  carbonXPciSlaveCfgWrite(&mCfgSpace, &mReq, &mResp);
	  break;
	  
	case PCI_CFG_READ :
	  carbonXPciSlaveCfgRead(&mCfgSpace, &mReq, &mResp);
	  break;
	  
	case PCI_MEM_WRITE_CMP  :
	  carbonXPciSlvSparseMemWrite(mMemSpace, &mCfgSpace, &mReq, &mResp);
	  break;
	  
	case PCI_MEM_READ   : 
	  carbonXPciSlvSparseMemRead(mMemSpace, &mCfgSpace, &mReq, &mResp);
	  break;
	  
	default :
	  // Send Empty Response
	  mResp.transaction(TBP_NXTREQ, 0LL, NULL, 0, status, TBP_WIDTH_64P);
	  
	}
	
	mResp.setStatus(mResp.getStatus() | status);
	
// 	MSG_Milestone("%s Slave response: A:%08llx  OP:%08x SZ:%3d  DT:0x%08x_%08x ST:%08x ID:0x%04x\n",
// 		      name(), mResp.getAddress(), mResp.getOpcode(), mResp.getSize(), 
// 		      ((const UInt32 *)mResp.getData())[1], ((const UInt32 *)mResp.getData())[0], mResp.getStatus(), mResp.getTransId());
	
	// Check if we need to configure the Verilog Side
	// if we do send them before the response
	if(mCfgSpace.needUpdate()) {
	  CarbonXPciTransReqT cs_req;
	  
	  // Set Trans ID For identification
	  cs_req.setTransId(0x80000000);
	  
	  CarbonXPciConfigRegs::CsListItem item;
	  while(mCfgSpace.getNextCsListItem(&item)) {
	    cs_req.csWrite(item.first, item.second);
	    if( !xaction_port->nb_put(cs_req)) {
	      SC_REPORT_ERROR("/Carbon/CarbonXTlmPciTarget/", "Failed to put transaction.");
	    }
	  }
	  mCfgSpace.csListClear();
	}
	
	if( !xaction_port->nb_put(mResp)) {
	  SC_REPORT_ERROR("/Carbon/CarbonXTlmPciTarget/", "Failed to put transaction.");
	}
	
      } // if( !(mReq.getTransId() & 0x80000000) )
    }
    
  } // slaveMethod
  
private:
 
  // Transaction Objects 
  // (Since this is a slave ReqT is really a response and RespT is really a request)
  CarbonXTransReqT   mResp;
  CarbonXTransRespT  mReq;

  CarbonXPciConfigRegs mCfgSpace;
  sparseMem_t          mMemSpace;
  sparseMem_t          mIOSpace;

  // Event Finder Method
  typedef tlm::tlm_nonblocking_get_if<CarbonXTransRespT> get_if_type; 
  sc_event_finder &ok_to_get() const {
    return *new tlm::tlm_event_finder_t< get_if_type , CarbonXTransRespT>(
	     xaction_port,
	     &get_if_type::ok_to_get );
  }
};

#endif
