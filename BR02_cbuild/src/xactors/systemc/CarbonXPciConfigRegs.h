// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxpciconfigregs__
#define __carbonxpciconfigregs__

#include "xactors/systemc/CarbonXPciIfs.h"
#include <list>

class CarbonXPciConfigRegs : public carbonXPciConfigRegsS, 
			     public CarbonXPciSlaveConfigureIF 
{
  
 public:
  
  typedef std::list< std::pair<UInt32, UInt32> >::iterator CsListIterator;
  typedef std::pair<UInt32, UInt32>                        CsListItem;

  CarbonXPciConfigRegs(UInt32 size = 64) {
    mSize = size;
    mMem  = new UInt32[size];
    mWe   = new UInt32[size];
    mCe   = new UInt32[size];
    
    memset(mMem, 0, sizeof(UInt32) * size);
    memset(mWe,  0, sizeof(UInt32) * size);
    memset(mCe,  0, sizeof(UInt32) * size);
  }
  
  // Generic Configuration Methods
  void setWriteMask(UInt32 reg_addr, UInt32 mask){
    mWe[reg_addr] = mask;
  }
  void setClearableMask(UInt32 reg_addr, UInt32 mask){
    mCe[reg_addr] = mask;
  }
  UInt32 getRegisterValue(UInt32 reg_addr) const {
    return mMem[reg_addr];
  }
  void setRegisterValue(UInt32 reg_addr, UInt32 reg_data) {
    mMem[reg_addr] = reg_data;
  }

  // Specific field Methods
  void setDeviceID(UInt16 id) {
    mMem[0] = (mMem[0] & 0x0000ffff) | (UInt32)id << 16;  
  }
  void setVendorID(UInt16 id) {
    mMem[0] = (mMem[0] & 0xffff0000) | id;  
  };
  void setStatusReg(UInt16 val){
    mMem[1] = (mMem[1] & 0x0000ffff) | (UInt32)val << 16;  
  }
  void setCommandReg(UInt16 val){
    mMem[1] = (mMem[1] & 0xffff0000) | val;  
  }
  void setClassCode(UInt32 val){
    mMem[2] = (mMem[2] & 0x000000ff) | (val&0xffffff) << 8;  
  }
  void setRevisionID(UInt8 id) {
    mMem[2] = (mMem[2] & 0xffffff00) | id;  
  };
  void setBIST(UInt8 val){
    mMem[3] = (mMem[3] & 0x00ffffff) | (UInt32)val << 24;  
  }
  void setHeaderType(UInt8 val){
    mMem[3] = (mMem[3] & 0xff00ffff) | (UInt32)val << 16;  
  }
  void setLatencyTimer(UInt8 val){
    mMem[3] = (mMem[3] & 0xffff00ff) | (UInt32)val << 8;  
  }
  void setCacheLineSize(UInt8 val){
    mMem[3] = (mMem[3] & 0xffffff00) | val;  
  }
  void setupBaseAddr(UInt32 bar, UInt64 size, MemSpaceT mem_or_io, AddrWidthT width, PrefetchT prefetch) {
    
    // Check arguments
    if(bar > 5) {
      std::cout << "In CarbonXPciConfigRegs::setBaseAddr: Tried to set Base Address register " << dec << bar
		<< ". Only 6 Base Address Registers are available." << std::endl;
      return;
    }

    // Check if size is a power of 2, if it is we should only see
    // one "1" in the binary representaion of size
    bool found_one = false;
    for(int i = 0; i < 64; i++) {
      if( (size >> i) & 1) {
	if(found_one) {
	  std::cout << "In CarbonXPciConfigRegs::setBaseAddr: Size of memory space has to be "
		    << "a power of power of 2. Tried to use a size of 0x" << std::hex << size << std::endl; 
	  return;
	}
	else found_one = true;
      }
    }
    
    // If this is an IO base address, the maximum amount of memory is 256 bytes
    if(mem_or_io == eIOSpace && size > 256) {
      std::cout << "In CarbonXPciConfigRegs::setBaseAddr: IO Space can no exceed 256 "
		<< "bytes. Size requested: " << std::dec << size << std::endl;
      return;
    }

    // Check that for an IO base address the width is specified as 32 bit
    if(mem_or_io == eIOSpace && width == eWidth64b) {
      std::cout << "In CarbonXPciConfigRegs::setBaseAddr: IO Base Address Registers are only 32 bit wide. " 
		<< "Request for 64 bit is ignored." << std::endl;
      width = eWidth32b;
    }
    
    
    // Now Check if size is in Range of the specified address width
    if(width == eWidth32b && size > 0x100000000LL) {
      std::cout << "In CarbonXPciConfigRegs::setBaseAddr: A size of " << std::dec << size 
		<< " is too big for a 32 bit Base Address Register." << std::endl;
      return;
    }

    // Check that for 64 bit Base Address Registers an even register number is given
    if(width == eWidth64b && bar%2) {
      std::cout << "In CarbonXPciConfigRegs::setBaseAddr: 64 bit Base Address Registers has to use "
		<< "Base Address Register, 0, 2 or 4, since two registers are used." << std::endl;
      return;
    }

    // Now Calculate the Write Mask
    UInt64 wren = ~(size-1);
    
    mWe[4+bar]  = wren;
    if(mem_or_io == eIOSpace)
      mMem[4+bar] = 1; // Indicate IO Space
    else
      mMem[4+bar] = width << 1 | prefetch << 3;
    
    // If 64bit register write the upper word
    if(width == eWidth64b) {
      mWe[5+bar]  = wren >> 32;
      mMem[5+bar] = 0;
    }

    // Now We need to tell the HDL side about the size
    CarbonXPciTransReqT req;
    mCsList.push_back(CsListItem(CARBONX_PCI_BAR_SIZE0 + 4*bar, size));
    
    UInt32 bar_val = prefetch << 3 | width << 2 | mem_or_io;
    mCsList.push_back(CsListItem(CARBONX_PCI_BAR0 + 4*bar, bar_val));

    // If 64bit register write the upper word
    if(width == eWidth64b) {
      mCsList.push_back(CsListItem(CARBONX_PCI_BAR_SIZE0 + 4*(bar+1), size>>32));
    }
  }
  void setCardBusCISPointer(UInt32 val){
    mMem[10] = val;
  }
  void setSubSystemID(UInt16 val){
    mMem[11] = (mMem[11] & 0x0000ffff) | (UInt32)val << 16;
  }
  void setSubsystemVendorID(UInt16 val){
    mMem[11] = (mMem[11] & 0xffff0000) | val;  
  }
  void setExpROMBaseAddr(UInt32 val){
    mMem[12] = val;
  }
  void setCapabilitiesPtr(UInt8 val){
    mMem[13] = (mMem[13] & 0xffffff00) | val;  
  }
  void setMaxLat(UInt8 val){
    mMem[15] = (mMem[15] & 0x00ffffff) | (UInt32)val << 24;  
  }
  void setMinGnt(UInt8 val){
    mMem[15] = (mMem[15] & 0xff00ffff) | (UInt32)val << 16;  
  }
  void setInterruptPin(UInt8 val){
    mMem[15] = (mMem[15] & 0xffff00ff) | (UInt32)val << 8;  
  }
  void setInterruptLine(UInt8 val){
    mMem[15] = (mMem[15] & 0xffffff00) | val;  
  }

  // Config Space List Methods
  inline bool needUpdate() {
    return !mCsList.empty();
  }
  
  inline CarbonUInt32 csListGetSize() const {
    return mCsList.size();
  }

  inline bool getNextCsListItem(CsListItem *item) {
    bool success = false;
    if(!mCsList.empty()) {
      *item = mCsList.front();
      mCsList.pop_front();
      success = true;
    }
    return success;
  }
  
  inline CsListIterator csListBegin() {
    return mCsList.begin();
  }

  inline CsListIterator csListEnd() {
    return mCsList.end();
  }
  
  inline void csListClear() {
    mCsList.clear();
  }
  
  // Destructor
  ~CarbonXPciConfigRegs() {
    delete [] mMem;
    delete [] mWe;
    delete [] mCe;
  }

private:

  // List Of Config Space Writes that needs to be
  // done to update the Verilog side
  std::list<CsListItem> mCsList;
  
};

#endif
