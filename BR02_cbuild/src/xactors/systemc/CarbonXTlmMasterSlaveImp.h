// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef _carbonx_tlm_master_slave_imp_h__
#define _carbonx_tlm_master_slave_imp_h__

#include "tlm.h"
#include "xactors/systemc/CarbonXIfs.h"

template < typename REQ , typename RSP >
class CarbonXTlmMasterImp :
  public virtual tlm::tlm_master_if< REQ , RSP >
{
public:
  
  CarbonXTlmMasterImp( CarbonXTlmPutFifo<REQ> &req ,
		       CarbonXTlmGetFifo<RSP> &rsp ) :
    put_fifo(req), get_fifo(rsp) {
  }
  
  // put interface
  
  void put( const REQ &t ) { put_fifo.put( t ); }

  bool nb_put( const REQ &t ) { return put_fifo.nb_put( t ); }
  bool nb_can_put( tlm::tlm_tag<REQ> *t = 0 ) const {
    return put_fifo.nb_can_put( t );
  }
  const sc_event &ok_to_put( tlm::tlm_tag<REQ> *t = 0 ) const {
    return put_fifo.ok_to_put( t );
  }

  // get interface

  RSP get( tlm::tlm_tag<RSP> *t = 0 ) { return get_fifo.get(); }

  bool nb_get( RSP &t ) { return get_fifo.nb_get( t ); }
  
  bool nb_can_get( tlm::tlm_tag<RSP> *t = 0 ) const {
    return get_fifo.nb_can_get( t );
  }

  virtual const sc_event &ok_to_get( tlm::tlm_tag<RSP> *t = 0 ) const {
    return get_fifo.ok_to_get( t );
  }

  // peek interface

  RSP peek( tlm::tlm_tag<RSP> *t = 0 ) const { return get_fifo.peek(); }

  bool nb_peek( RSP &t ) const { return get_fifo.nb_peek( t ); }
  
  bool nb_can_peek( tlm::tlm_tag<RSP> *t = 0 ) const {
    return get_fifo.nb_can_peek( t );
  }

  virtual const sc_event &ok_to_peek( tlm::tlm_tag<RSP> *t = 0 ) const {
    return get_fifo.ok_to_peek( t );
  }

  // debug interface
  virtual int used() const {
  }
  virtual int size() const {
  }
  virtual void debug() const {
  }

private:
  CarbonXTlmPutFifo<REQ> &put_fifo;
  CarbonXTlmGetFifo<RSP> &get_fifo;

};

template < typename REQ , typename RSP >
class CarbonXTlmSlaveImp :
  public virtual CarbonXSlaveIf< REQ , RSP >
{
public:
  
  CarbonXTlmSlaveImp( CarbonXTlmPutFifo<REQ> &req ,
		       CarbonXTlmGetFifo<RSP> &rsp ) :
    put_fifo(rsp), get_fifo(req) {
  }
  
  // put interface
  
  void put( const RSP &t ) { put_fifo.put( t ); }

  bool nb_put( const RSP &t ) { return put_fifo.nb_put( t ); }
  bool nb_can_put() const {
    return put_fifo.nb_can_put( );
  }
  const std::string &ok_to_put() const {
    return put_fifo.ok_to_put( );
  }

  // get interface

  REQ get( ) { return get_fifo.get(); }

  bool nb_get( REQ &t ) { return get_fifo.nb_get( t ); }
  
  bool nb_can_get( ) const {
    return get_fifo.nb_can_get();
  }

  virtual const std::string &ok_to_get( ) const {
    return get_fifo.ok_to_get();
  }

  // debug interface
  virtual int used() const {
  }
  virtual int size() const {
  }
  virtual void debug() const {
  }

private:
  CarbonXTlmPutFifo<REQ> &get_fifo;
  CarbonXTlmGetFifo<RSP> &put_fifo;

};

#endif
