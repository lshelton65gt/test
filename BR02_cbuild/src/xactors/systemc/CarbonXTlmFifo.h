// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef _carbonx_tlm_fifo_h_
#define _carbonx_tlm_fifo_h_

#include <string>
#include <queue>
#include "tlm.h"
#include "xactors/systemc/CarbonXIfs.h"

template <class T>
class CarbonXTlmPutFifo :
  public virtual CarbonXGetIf<T>,
  public virtual tlm::tlm_fifo_put_if<T>,
  public sc_prim_channel
{

public :

  // constructors
  
  explicit CarbonXTlmPutFifo( int size_ = 0 )
    : sc_prim_channel( sc_gen_unique_name( "fifo" ) ),
      mDataWrittenEvent(std::string(name())) {
    mSize  = size_;
    //std::cout << "Created Signal Name: " <<  mDataWrittenEvent << std::endl; 
  }
  
  explicit CarbonXTlmPutFifo( const char* name_ , int size_ = 0 )
		       : sc_prim_channel( name_ ),
			 mDataWrittenEvent(std::string(name())) {
    mSize  = size_;
    //std::cout << "Created Signal Name: " <<  mDataWrittenEvent << std::endl; 
  }
  
  // tlm put interface 
  
  void put( const T& cmd) {
    while(!nb_put(cmd)) {
      wait(ok_to_put());
    }
  }
  
  bool nb_put( const T& cmd);

  bool nb_can_put( tlm::tlm_tag<T> *t = 0 ) const {
    return !isFull();
  };
  
  const sc_event& ok_to_put( tlm::tlm_tag<T> *t = 0 ) const {
    return mDataReadEvent;
  }
  
  
  // tlm get interface (VSPX side)
  
  T get();
  
  bool nb_get( T& );
  bool nb_can_get() const {
    return !mQueue.empty();
  }
  const std::string &ok_to_get() const {
    return mDataWrittenEvent;
  }
  
  // tlm debug interface
  int used() const {
    return mQueue.size();
  };
  int size() const {
    return mSize;
  };
  void debug() const {
    if( mQueue.empty() ) cout << "empty" << endl;
    if( isFull() ) cout << "full" << endl;
    cout << "size " << size() << " - " << used() << " used " << endl;
  };

private :
  bool nb_peek( T & , int n ) const{};
  bool nb_poke( const T & , int n = 0 ){};

protected :
  
  inline bool isFull() const {
    return (mSize > 0 && mQueue.size() >= mSize);
  }
    
  // Member Variables
  std::queue<T>                 mQueue;
  int                           mSize;
  std::string                   mDataWrittenEvent;
  sc_event                      mDataReadEvent;
};

template<class T>
bool CarbonXTlmPutFifo<T>::nb_put(const T & cmd) {
  
  bool success;

  // Check if fifo is full
  if(isFull()) success = false;

  else {

    mQueue.push(cmd);

    // Check if the queue was empty, if it is, we want to notify
    // anyone that might wait, that there is now data on the queue 
    if(mQueue.size() == 1) {
      
      // If someone is waiting for the signal, set a signal
      //std::cout << "Setting Signal " <<  mDataWrittenEvent << std::endl;
      carbonXSetSignal((char *)mDataWrittenEvent.c_str());
    }
    
    success = true;
  }

  return success;
}

template<class T>
T CarbonXTlmPutFifo<T>::get() {
  T data;

  while ( !nb_get(data) ) {
    //std::cout << "Waiting for signal " << mDataWrittenEvent << std::endl;
    carbonXWaitSignal((char *)mDataWrittenEvent.c_str(), 0);
  }

  return data;
}

template<class T>
bool CarbonXTlmPutFifo<T>::nb_get( T & cmd) {
  if(mQueue.empty()) {
    return false;
  }
  else {
    cmd = mQueue.front();
    mQueue.pop();
    mDataReadEvent.notify(SC_ZERO_TIME);
  }

  return true;
}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
template <class T>
class CarbonXTlmGetFifo :
  public virtual tlm::tlm_fifo_get_if<T>,
  public virtual CarbonXPutIf<T>,
  public sc_prim_channel
{

public :

  // constructors
  
  explicit CarbonXTlmGetFifo( int size_ = 0)
    : sc_prim_channel( sc_gen_unique_name( "fifo" ) ),
      mDataReadEvent(std::string(name())) {
    mSize = size_;
    //std::cout << "Created Signal Name: " <<  mDataReadEvent << std::endl; 
  }
  
  explicit CarbonXTlmGetFifo( const char* name_, int size_ = 0)
		       : sc_prim_channel( name_ ),
			 mDataReadEvent(std::string(name())) {
    mSize = size_;
    //std::cout << "Created Signal Name: " <<  mDataReadEvent << std::endl; 
  }
  
  // tlm put interface (VSPX side) 
  
  bool nb_put( const T& );
  
  void put( const T& cmd) {
    while(!nb_put(cmd)) carbonXWaitSignal((char *)ok_to_put().c_str(), 0);
  }

  bool nb_can_put() const {
    return !isFull();
  };
  
  const std::string & ok_to_put() const {
    return mDataReadEvent;
  }
  
  
  // tlm get interface
  
  T get( tlm::tlm_tag<T> *t = 0 );
  
  bool nb_get( T& );
  bool nb_can_get( tlm::tlm_tag<T> *t = 0 ) const {
    return !mQueue.empty();
  }
  const sc_event &ok_to_get( tlm::tlm_tag<T> *t = 0 ) const {
    return mDataWrittenEvent;
  }

  // Has to implement these pure virtual methods
  bool nb_peek( T &t ) const{};
  bool nb_can_peek( tlm::tlm_tag<T> *t = 0 ) const{};
  const sc_event &ok_to_peek( tlm::tlm_tag<T> *t = 0 ) const{};
  T peek( tlm::tlm_tag<T> *t = 0 ) const {};

  // tlm debug interface
  int used() const {
    return mQueue.size();
  };
  int size() const {
    return mSize;
  };
  void debug() const {
    if( mQueue.empty() ) cout << "empty" << endl;
    if( isFull() ) cout << "full" << endl;
    cout << "size " << size() << " - " << used() << " used " << endl;
  };
  
  // tlm peek and poke interface
  bool nb_peek( T & , int n ) const{};
  bool nb_poke( const T & , int n = 0 ){};
  
protected :
  
  inline bool isFull() const {
    return (mSize > 0 && mQueue.size() >= mSize);
  }

  // Member Variables
  std::queue<T>              mQueue;
  int                        mSize;
  sc_event                   mDataWrittenEvent;
  std::string                mDataReadEvent;
};

template<class T>
bool CarbonXTlmGetFifo<T>::nb_put(const T & cmd) {
  
  if(isFull()) return false;

  // Check if queue is empty
  bool empty = mQueue.empty();
  
  // Put command on the queue
  //std::cout << name() << ": Pushing Reponse to queue"  << std::endl;
  mQueue.push(cmd);
  
  // Check if the queue was empty, if it was, we want to notify
  // anyone that might wait, that there is now data on the queue 
  if(empty) {
    //std::cout << name() << ": Notify Written Event." << std::endl;
    mDataWrittenEvent.notify(SC_ZERO_TIME);
  }

  return true;
}

template<class T>
T CarbonXTlmGetFifo<T>::get( tlm::tlm_tag<T> *t ) {
  T data;

  while ( !nb_get(data) ) {
    wait(mDataWrittenEvent);
  }

  return data;
}

template<class T>
bool CarbonXTlmGetFifo<T>::nb_get( T & cmd) {
  //std::cout << name() << "Empty: " << mQueue.empty() << std::endl;
  if(mQueue.empty()) {
    return false;
  }
  else {
    cmd = mQueue.front();
    mQueue.pop();
    carbonXSetSignal((char *)mDataReadEvent.c_str());
  }

  return true;
}

#endif
