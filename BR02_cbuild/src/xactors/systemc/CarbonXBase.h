// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxbase__
#define __carbonxbase__

#include <systemc.h>
#include <string>

class CarbonXBase : 
  public sc_channel
{

public:
  SC_HAS_PROCESS(CarbonXBase);
  
  // constructor
  CarbonXBase(const sc_module_name instname, const char *xtor_path) : 
    sc_channel(instname),
    mXtorPath( std::string(xtor_path) ) {
    
    // This method does the carbonXAttach
    SC_METHOD(init_method);
  }

  const std::string & getXtorPath() const { return mXtorPath; };
        
private:

  // Verilog Path to transactor
  std::string mXtorPath;
  
  // carbonX Attach methods
  virtual void transactionLoop() = 0;

  static void vspx_func(CarbonXBase *this_obj){
    this_obj->transactionLoop();
  }

  void init_method() {
    std::cout << "Running init_method for Xactor." << mXtorPath << std::endl;
    carbonXAttach(mXtorPath.c_str(), 
		  reinterpret_cast<CarbonXAttachFuncT> (vspx_func), 
		  1, 
		  this);
  }
};

#endif
