// -*- C++ -*-
/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/

#ifndef __CarbonXSCSataLLIfs_h_
#define __CarbonXSCSataLLIfs_h_

#include "systemc.h"
#include "xactors/sata/carbonXSataLL.h"

/*!
 * \addtogroup SataLLSystemC
 * @{
 */

/*!
 * \file CarbonXSCSataLLIfs.h
 * \brief Definition of SATA Link Layer SystemC Transaction Interface
 *
 * This file provides definition of SATA Link Layer transaction interface class
 * for ReportTransaction and Response interfaces
 */

//! SATA SystemC Response Interface
class CarbonXSCSataResponseIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCSataResponseIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCSataResponseIF() {}

  /*!
   * This function is called back to set the default response condition for a
   * transaction. For example, this function will be called back from the
   * receiving side as soon as the transaction is started, and can be used
   * to specify the DMAT position.
   */
  virtual void setDefaultResponse(CarbonXSataLLTrans* trans)=0;

  /*!
   * This function is called back by the transactor to set the response
   * to a transaction. This is called on the receiving side once the full
   * transaction has been received, and one can retrieve the frame data here.
   */
  virtual void setResponse(CarbonXSataLLTrans* trans)=0;

  //! This function will not be called in the SATA LL transactor.
  void refreshResponse(CarbonXSataLLTrans* trans) {}
};

//! SATA SystemC StartTransaction Interface
class CarbonXSCSataStartTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCSataStartTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCSataStartTransactionIF() {}

  /*!
   * This function can be used by the user to push a new transaction to
   * the transactor for initiation. For example, one can push a Frame
   * transaction using this function.
   */
  virtual CarbonUInt32 startNewTransaction(CarbonXSataLLTrans* trans)=0;
};

//! SATA SystemC ReportTransaction Interface
class CarbonXSCSataReportTransactionIF : public virtual sc_interface
{
public:

  //! constructor
  CarbonXSCSataReportTransactionIF() : sc_interface() {}

  //! destructor
  ~CarbonXSCSataReportTransactionIF() {}

  /*!
   * This function is called back by the transactor when a transaction pushed
   * is complete from the initiator side.
   */
  virtual void reportTransaction(CarbonXSataLLTrans* trans)=0;
};

/*!
 * @}
 */

#endif
