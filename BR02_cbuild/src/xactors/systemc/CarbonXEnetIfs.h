// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxenetifs__
#define __carbonxenetifs__

#include "xactors/systemc/CarbonXIfs.h"
#include "xactors/enet_mii/carbonXEnet.h"

class CarbonXEnetTransReqT : public CarbonXTransReqT {
 public:

  // Constructor
  CarbonXEnetTransReqT(UInt32 buf_size = 0) : CarbonXTransReqT(buf_size) {
  }
  
  void buildPacket(const ENET_PKT_T &desc) {
    carbonXEnetBuildTransaction(this, const_cast<ENET_PKT_T *>(&desc));
  }

};  

class CarbonXEnetTransRespT : public CarbonXTransRespT {
 public:

  // Constructor
  CarbonXEnetTransRespT(UInt32 buf_size = 0) : CarbonXTransRespT(buf_size) {
  }
  
  void checkPacket(const ENET_PKT_T &desc) {

    carbonXEnetCheckPacket(desc, reinterpret_cast<u_int8 *>(mData), mSize);

  }
};

#endif
  
    
