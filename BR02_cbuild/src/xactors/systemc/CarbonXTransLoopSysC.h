// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxtransloopsysc__
#define __carbonxtransloopsysc__

#include <systemc.h>
#include "xactors/carbonX.h"
#include "xactors/systemc/CarbonXBase.h"
#include "xactors/systemc/CarbonXIfs.h"
#include <string>

class CarbonXTransLoopSysC : 
  public CarbonXBase
{

public:
  
  // constructor
  CarbonXTransLoopSysC(sc_module_name instname, const char *xtor_path) : 
    CarbonXBase(instname, xtor_path){ }
  
  sc_port<CarbonXSlaveIf<CarbonXTransReqT, CarbonXTransRespT > > trans_port;

 private :
  
  // carbonX Attach methods
  void transactionLoop() {
    CarbonXTransReqT  trans_req;
    CarbonXTransRespT trans_resp;

    //std::cout << "Starting Transaction Loop for xactor " << getXtorPath() << std::endl;

    while (1) {
      trans_req = trans_port->get();
      
      //std::cout << getXtorPath() << "TransLoop: Request at time " << sc_time_stamp() << ": " << std::endl << trans_req;
      UInt32 i = 0;
      do {
	carbonXReqRespTransaction(&trans_req, &trans_resp);
	
	trans_port->put(trans_resp);
	//std::cout << getXtorPath() << "TransLoop: Response at time " << sc_time_stamp() << ": " << std::endl << trans_resp;

	i++;
      } while(i < trans_req.getRepeatCnt() || (trans_req.getRepeatCnt() == 0 && !trans_port->nb_can_get()));
      
    }
  }

};

#endif
