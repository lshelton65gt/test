// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxtlmahbmasterport__
#define __carbonxtlmahbmasterport__

#include "xactors/systemc/CarbonXTlmMasterPort.h"
#include "xactors/systemc/CarbonXAhbIfs.h"

class CarbonXTlmAhbMasterPort : public CarbonXTlmMasterPort {
  
public:
  
  CarbonXTlmAhbMasterPort(const char* name) : 
    CarbonXTlmMasterPort(name) {

    // We need to be able to seperate Ahb Transaction from transactions
    // Performed by the Base Port, so set the top bit of the transaction
    // ID
    mAhbReq.setTransId(0x80000000);
  }
  
  ////////////////////////////////////////////////////////////////////////////
  // Master xtor configuration routines
  ////////////////////////////////////////////////////////////////////////////
  void setAbortOnError (UInt32 abortOnError) {
    mAhbReq.setAbortOnError(abortOnError);
    _ahbPut();
  }
  void setBusyCycles   (UInt32 insertAfter, UInt32 repeatAfter) {
    mAhbReq.setBusyCycles(insertAfter, repeatAfter);
    _ahbPut();
  }
  void setProtection   (UInt32 hprot) {
    mAhbReq.setProtection(hprot);
    _ahbPut();
  }
  void setBurstConfig  (UInt32 hsize, UInt32 incr, UInt32 lock) {
    mAhbReq.setBurstConfig(hsize, incr, lock);
    _ahbPut();
  };

  ////////////////////////////////////////////////////////////////////////////
  // READ/WRITE LOCKED/UNLOCKED bursts of different sizes (8-/16-/32-bit)
  // Inputs: 
  // nbits   => Data width in bits (data size) per transfer (8, 16 or 32)
  // beats   => Number of beats (transfers) in the burst.
  // incr    => 1: INCR burst, 0: WRAP burst
  // lock    => 1: locked burst, 0: non locked burst
  // address => Start address of the burst
  // wdata   => Write buffer ptr. Pointer to a array of 'beats' number of elements.
  // rdata   => Pointer to a block capable of holding 'beats' number of elements.
  // elements should be 8-/16-/32-bit wide depending on the value of nbits used.
  // All functions return number of beats actually written/read
  ////////////////////////////////////////////////////////////////////////////
  UInt32 burstRead  (UInt32 nbits, UInt32 beats, UInt32 incr, 
		     UInt32 lock,  UInt32 address, UInt8 *rdata) {
    mAhbReq.burstReadReq(nbits, beats, incr, lock, address);
    _ahbPut();
    _block();
    mResp.readArray8(rdata);
    return mResp.getSize();
  }
  UInt32 burstRead  (UInt32 nbits, UInt32 beats, UInt32 incr, 
		     UInt32 lock,  UInt32 address, UInt16 *rdata) {
    mAhbReq.burstReadReq(nbits, beats, incr, lock, address);
    _ahbPut();
    _block();
    mResp.readArray16(rdata);
    return mResp.getSize();
  }
  UInt32 burstRead  (UInt32 nbits, UInt32 beats, UInt32 incr, 
		     UInt32 lock,  UInt32 address, UInt32 *rdata) {
    mAhbReq.burstReadReq(nbits, beats, incr, lock, address);
    _ahbPut();
    _block();
    mResp.readArray32(rdata);
    return mResp.getSize();
  }
  
  template <typename T>
  UInt32 burstWrite (UInt32 nbits, UInt32 beats,   UInt32 incr,
		     UInt32 lock,  UInt32 address, T *wdata) {
    mAhbReq.burstWrite(nbits, beats, incr, lock, address, wdata);
    _ahbPut();
    _block();
    return mResp.getSize();
  }
  
private:
  
  // AHB(x) Transaction Request Object
  CarbonXAhbTransReqT mAhbReq;

  inline void _ahbPut()   { 
    mPutTransId = mAhbReq.getTransId(); 
    (*this)->put(mAhbReq); 
  }

  //-/////////////////////////////////////////////////////////////////////////////
  // Local function to calculate AHB HSIZE encoding from nbits
  //-/////////////////////////////////////////////////////////////////////////////
  inline UInt32 MGetHSize (UInt32 nbits) {
    return UInt32(log(nbits/8)/log(2)); // Log base 2 of (nbits/8)
  }
};

#endif
