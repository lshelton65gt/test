// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxpciifs__
#define __carbonxpciifs__

#include "xactors/systemc/CarbonXIfs.h"

// Don't want to clobber global namespace
//namespace pcimaster {
#include "xactors/pci/carbonXPciMaster.h"
//}

class CarbonXPciTransReqT : public CarbonXTransReqT {
 public:

  // Constructor
  CarbonXPciTransReqT(UInt32 buf_size = 0) : CarbonXTransReqT(buf_size) {
  }
  
  void pciCfgWriteReq(UInt64 address, UInt32 data) {
    UInt32 opcode = (1<<USER_OP__VALID);
    opcode |= (1<<USER_OP__WRITE);
    opcode |= (PCI_CFG_WRITE<<USER_OP__PCI_CMD);
    transaction(opcode, address, reinterpret_cast<const void*>(&data), 4, 0, TBP_WIDTH_32);
  }

  void pciCfgReadReq(UInt64 address) {
    UInt32 opcode = (1<<USER_OP__VALID);
    opcode |= (PCI_CFG_READ<<USER_OP__PCI_CMD);
    transaction(opcode, address, NULL, 4, 0, TBP_WIDTH_32);
  }

  void pciMemWriteReq(UInt64         address, 
		      const UInt32 * data,
		      UInt32         size,
		      bool           use_64bit = false,
		      UInt32         req_id  = 0, 
		      UInt32         req_tag = 0)
  {
    UInt32 opcode = (1<<USER_OP__VALID);
    UInt32 status = 0;

    opcode |= ((use_64bit&1)<<USER_OP__64BIT);
    opcode |= (PCI_MEM_WRITE<<USER_OP__PCI_CMD);
    opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);
    
    std::cout << "pciMemWriteReq; Writing data " << *data << " Size " << size << std::endl;
    transaction(opcode, address, reinterpret_cast<const void*>(data), size, status, TBP_WIDTH_32);
    
  }

  void pciMemReadReq(UInt64 address, UInt32 size, bool use_64bit = false)
  {
    UInt32 opcode = (1<<USER_OP__VALID);
    UInt32 status = 0;

    opcode |= ((use_64bit&1)<<USER_OP__64BIT);
    opcode |= (PCI_MEM_READ<<USER_OP__PCI_CMD);
    
    transaction(opcode, address, NULL, size, status, TBP_WIDTH_32);
  }
  
  void pcixMemReadReq(UInt64   address, 
		      UInt32   size,
		      bool     use_64bit = false,
		      UInt32   req_id  = 0, 
		      UInt32   req_tag = 0)
  {
    UInt32 opcode = (1<<USER_OP__VALID);
    UInt32 status = 0;

    opcode |= ((use_64bit&1)<<USER_OP__64BIT);
    opcode |= (PCIX_MEM_READ_BLK<<USER_OP__PCI_CMD);
    opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);
    
    transaction(opcode, address, NULL, size, status, TBP_WIDTH_32);
  }
  
  void pciIOWriteReq(UInt32 address, UInt32 data, UInt32 size, UInt32 req_id  = 0, UInt32 req_tag = 0) {
    UInt32 opcode;
    
    opcode  = (1<<USER_OP__VALID) | (PCI_IO_WRITE<<USER_OP__PCI_CMD);
    opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID);
    opcode |= ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);
    
    transaction(opcode, address, &data, size, 0, TBP_WIDTH_32);
  }
  void pciIOReadReq(UInt32 address, UInt32 size, UInt32 req_id  = 0, UInt32 req_tag = 0) {
    UInt32 opcode;

    opcode  = (1<<USER_OP__VALID) | (PCI_IO_READ<<USER_OP__PCI_CMD);
    opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID);
    opcode |= ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

    transaction(opcode, address, NULL, size, 0, TBP_WIDTH_32);
  }

  void pciIntAckReq(UInt32 req_id = 0, UInt32 req_tag = 0){
    UInt32 opcode = (1<<USER_OP__VALID) | (PCI_INTR_ACK<<USER_OP__PCI_CMD);
    opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | 
      ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);
    transaction(opcode, 0LL, NULL, 4, 0, TBP_WIDTH_32);
  }

};  

class CarbonXPciSlaveConfigureIF : public sc_interface {
public:

  // Enums to use with the interface
  enum AddrWidthT {
    eWidth64b = 2,
    eWidth32b = 0
  };
  enum MemSpaceT {
    eMemSpace = 0,
    eIOSpace  = 1
  };
  enum PrefetchT {
    ePrefetch   = 1,
    eNoPrefetch = 0
  };

  // Generic Methods
  virtual void   setWriteMask(UInt32 reg_addr, UInt32 mask)=0;
  virtual void   setClearableMask(UInt32 reg_addr, UInt32 mask)=0;
  virtual void   setRegisterValue(UInt32 reg_addr, UInt32 reg_data)=0;
  virtual UInt32 getRegisterValue(UInt32 reg_addr) const =0;

  // Specific field Methods
  virtual void setDeviceID(UInt16)=0;
  virtual void setVendorID(UInt16)=0;
  virtual void setStatusReg(UInt16)=0;
  virtual void setCommandReg(UInt16)=0;
  virtual void setClassCode(UInt32)=0;
  virtual void setBIST(UInt8)=0;
  virtual void setHeaderType(UInt8)=0;
  virtual void setLatencyTimer(UInt8)=0;
  virtual void setCacheLineSize(UInt8)=0;
  virtual void setupBaseAddr(UInt32 bar, UInt64 size, MemSpaceT mem_or_io, AddrWidthT width, PrefetchT prefetch)=0;
  virtual void setCardBusCISPointer(UInt32)=0;
  virtual void setSubSystemID(UInt16)=0;
  virtual void setSubsystemVendorID(UInt16)=0;
  virtual void setExpROMBaseAddr(UInt32)=0;
  virtual void setCapabilitiesPtr(UInt8)=0;
  virtual void setMaxLat(UInt8)=0;
  virtual void setMinGnt(UInt8)=0;
  virtual void setInterruptPin(UInt8)=0;
  virtual void setInterruptLine(UInt8)=0;

};

#endif
