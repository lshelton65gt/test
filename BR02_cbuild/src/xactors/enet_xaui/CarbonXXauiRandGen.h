// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CarbonXXauiRandGen_h_
#define _CarbonXXauiRandGen_h_
#include "util/c_memmanager.h"

//! Generates a X^7 + X^6 + 1 polnomial PRBS 
class CarbonXXauiRandGen {
public: CARBONMEM_OVERRIDES;

  //! Constructor
  CarbonXXauiRandGen() : mInitialSeed(0x55) {
    reset();
  }

  //! Reset Random Generator
  void reset() {
    mLfsr = mInitialSeed;
  }

  //! Calculate next random value
  int nextVal() {
    // Implements the X^7 + X^6 + 1 polynomial
    int x7   = (mLfsr >> 6) & 1;
    int x6   = (mLfsr >> 5) & 1;
    int feed = x7 ^ x6;
    
    // Shift LFSR and insert result of feedback function into bit 7
    mLfsr = ((mLfsr << 1) | feed) & 0x7f; 
    return mLfsr & 0xF; // Only return the 4 LSB's
  }

private :

  //! Initial seed for the LFSR
  int mInitialSeed;

  //! Holds the current value in the LFSR
  int mLfsr;
};
  
#endif
