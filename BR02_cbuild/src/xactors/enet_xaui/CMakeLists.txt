add_definitions(${DISABLE_DYNAMIC_CAST}
                ${DISABLE_EXCEPTIONS})

include_directories(${CMAKE_CURRENT_SOURCE_DIR}
                    ${CMAKE_SOURCE_DIR}/xactors/pcie
                    ${CMAKE_SOURCE_DIR}/xactors/util
                    )

set(xtor_enet_xaui_sources
    CarbonXXaui.cxx
    )

add_static_library_pure_virtual_hack(xtor_enet_xaui ${CMAKE_CURRENT_BINARY_DIR} ${xtor_enet_xaui_sources})
add_dependencies(xtor_enet_xaui PrepCarbonHome)

add_subdirectory(verilog)
