/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
`timescale 1ns / 100 ps

// XAUI Transactor module
// Combines both RX and TX path of the transactor
module carbonx_enetxaui
  (SCLK,
   DCLK,
   sys_reset_l,

   // XAUI Input Ports
   SL0,
   SL1,
   SL2,
   SL3,

   // XAUI Output Ports
   DL0,
   DL1,
   DL2,
   DL3);

   input SCLK;
   input DCLK;
   input sys_reset_l;

   input [9:0] SL0;
   input [9:0] SL1;
   input [9:0] SL2;
   input [9:0] SL3;

   output [9:0] DL0;
   output [9:0] DL1;
   output [9:0] DL2;
   output [9:0] DL3;

   // XGMII Signals
   wire [31:0] 	RXD;
   wire [3:0] 	RXC;
   wire [31:0] 	TXD;
   wire [3:0] 	TXC;
   reg          TX_CLK;
   wire         rx_clk;

   // Configuration Signals
   wire         rx_loopback;
   wire         rx_bitreversal;
   wire         tx_loopback;
   wire         tx_bitreversal;

   // Status Signals
   wire [3:0] 	xaui_linkstatus;
   reg [1:0] 	xaui_code_error;
   
   // Generate half speed clock for XGMII Receiver
   initial TX_CLK = 0;
   always @(posedge SCLK)
     TX_CLK = ~TX_CLK;
   
   // Instantiate XGMII Transactors
   carbonx_enetxgmii_tx TX_XTOR (.sys_clk(DCLK),
				 .sys_reset_l(sys_reset_l),
				 .RXD(RXD),       
				 .RXC(RXC),     
				 .RX_CLK(rx_clk));

   carbonx_enetxgmii_rx RX_XTOR (.sys_clk(TX_CLK),
				 .sys_reset_l(sys_reset_l),
				 .TXD(TXD),      
				 .TXC(TXC), 
				 .TX_CLK(TX_CLK));

`protect

   // Code Error Signals
   wire         code_error;
   reg          code_error_d1;
   reg          code_error_d2;

   // XAUI Data Lanes
   wire [9:0] 	xaui_rx0;
   wire [9:0] 	xaui_rx1;
   wire [9:0] 	xaui_rx2;
   wire [9:0] 	xaui_rx3;
   wire [9:0] 	xaui_tx0;
   wire [9:0] 	xaui_tx1;
   wire [9:0] 	xaui_tx2;
   wire [9:0] 	xaui_tx3;

   // Delayed Inputs
   reg [9:0] 	SL0_d;
   reg [9:0] 	SL1_d; 
   reg [9:0] 	SL2_d;
   reg [9:0] 	SL3_d;
  
   // Get XAUI Configuration from XGMII Transactor
   assign 	tx_loopback     = TX_XTOR.XIF_get_csdata[96];
   assign 	tx_bitreversal  = TX_XTOR.XIF_get_csdata[97];
   assign 	rx_loopback     = RX_XTOR.rx.XIF_get_csdata[32];
   assign 	rx_bitreversal  = RX_XTOR.rx.XIF_get_csdata[33];

   // Delay Input buses to use for loopback
   // If we don't add en extra cycle when in loopback mode
   // we get combinational cycles if two transactors are connected
   // to each other.
   always @(posedge SCLK) begin
      SL0_d <= SL0;
      SL1_d <= SL1;
      SL2_d <= SL2;
      SL3_d <= SL3;
   end
   
   // Instantiate XAUI Bridge
   carbonx_enetxaui_tx XAUI_TX (.DCLK(DCLK),
				.sys_reset_l(TX_XTOR.reset_l),
				.DL0(xaui_rx0),
				.DL1(xaui_rx1),
				.DL2(xaui_rx2),
				.DL3(xaui_rx3),
				.RXD(RXD),
				.RXC(RXC),
				.bitreversal(tx_bitreversal));

   carbonx_enetxaui_rx XAUI_RX (.SCLK(SCLK),
				.sys_reset_l(TX_XTOR.reset_l),
				.SL0(xaui_tx0),
				.SL1(xaui_tx1),
				.SL2(xaui_tx2),
				.SL3(xaui_tx3),
				.TXD(TXD),
				.TXC(TXC),
				.bitreversal(rx_bitreversal),
				.linkstatus(xaui_linkstatus),
				.code_error(code_error));

   // Stretch code_error so it can be clocked by the half speed TX_CLK
   always @(posedge SCLK) begin
      code_error_d1 <= code_error;
      code_error_d2 <= code_error_d1;
      if(~TX_CLK) xaui_code_error <= {code_error_d1, code_error_d2};
   end
   
   // Link Status and Code Errors
   always @(xaui_linkstatus or xaui_code_error) begin
      RX_XTOR.rx.xaui_linkstatus <= xaui_linkstatus;
      RX_XTOR.rx.xaui_code_error <= xaui_code_error;
   end
   
   // Drive XAUI Outputs
   assign 	DL0 = tx_loopback ? SL0_d : xaui_rx0; 
   assign 	DL1 = tx_loopback ? SL1_d : xaui_rx1; 
   assign 	DL2 = tx_loopback ? SL2_d : xaui_rx2; 
   assign 	DL3 = tx_loopback ? SL3_d : xaui_rx3; 
   
   // Drive XAUI Inputs
   assign 	xaui_tx0 = rx_loopback ? xaui_rx0 : SL0;
   assign 	xaui_tx1 = rx_loopback ? xaui_rx1 : SL1;
   assign 	xaui_tx2 = rx_loopback ? xaui_rx2 : SL2;
   assign 	xaui_tx3 = rx_loopback ? xaui_rx3 : SL3;

`endprotect
endmodule

// This module handles the XAI TX path of the transactor
module carbonx_enetxaui_tx 
  (DCLK,
   sys_reset_l,
   DL0,
   DL1,
   DL2,
   DL3,
   RXD,
   RXC,
   bitreversal);

   input        DCLK;
   input 	sys_reset_l;
   output [9:0] DL0;
   output [9:0] DL1;
   output [9:0] DL2;
   output [9:0] DL3;

   input [31:0] RXD;
   input [3:0] 	RXC;

   input        bitreversal;
   
endmodule

// This module handles the XAI RX path of the transactor
module carbonx_enetxaui_rx
  (SCLK,
   sys_reset_l,
   SL0,
   SL1,
   SL2,
   SL3,
   TXD,
   TXC,
   bitreversal,
   linkstatus,
   code_error);

   input         SCLK;
   input 	 sys_reset_l;
   
   input [9:0] 	 SL0;
   input [9:0] 	 SL1;
   input [9:0] 	 SL2;
   input [9:0] 	 SL3;
   
   output [31:0] TXD;
   output [3:0]  TXC;

   input 	 bitreversal;
   output [3:0]  linkstatus;
   output 	 code_error;
 	 
endmodule
