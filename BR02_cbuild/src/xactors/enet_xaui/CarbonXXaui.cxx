/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "CarbonXString.h"
#include "CarbonXXaui.h"
#include "shell/carbon_capi.h"

#define S_CH 0xFB
#define E_CH 0xFE
#define K_CH 0xBC
#define R_CH 0x1C
#define A_CH 0x7C
#define T_CH 0xFD
#define Q_CH 0xDC

#define IDLE_CH 0x07

// Uncomment to enable debug output
//#define XAUI_DEBUG 1

// 
// CarbonXXgmii2Xaui
//

CarbonXXgmii2Xaui::CarbonXXgmii2Xaui() :
  mTime(0),
  mModel(0),
  mReversed(false)
{  
  // Reset Members
  reset();
}

void CarbonXXgmii2Xaui::reset() {
  mRandGen.reset();
  
  // Clear Input Ports
  mXgmiiRxData = 0;
  mXgmiiRxCtrl = 0;
  
  // Clear Output Ports
  for(int i = 0; i < 4; i++) {
    mXauiDL[i] = m8b10b[i].encoder_lut(0, false);
    m8b10b[i].clearInputDisparity();
  }
  
  // Clear Members
  mIdleCnt = 0;
  mACount  = 0;
  mSendK   = true;
}

void CarbonXXgmii2Xaui::evaluate()
{
  // Update Simulation Time
  mTime = carbonGetSimulationTime(mModel);

  // Check for Idle Symbols
  UInt32 laneData[4];
  bool   laneCtrl[4];
  
  // If there are any control characters
  bool sentA = false;
  for(int i = 0; i < 4; i++) {

    // Default Values for Lane Data and Control is the values directly from XGMII
    laneData[i] = (mXgmiiRxData >> (8*i)) & 0xff;
    laneCtrl[i] = (mXgmiiRxCtrl >> i) & 0x1;

    // Look to see if we need to alter any values
    
    // Check for Terminate
    if(laneCtrl[i] && (laneData[i] == T_CH) )
      mIdleCnt = 0; // IdleCnt == 0 indicated that we saw a ||T|| in this column

    // Now if we see an Idle Character on the XGMII bus, we have to generate
    // the Idle sequence.    
    else if(laneCtrl[i] && (laneData[i] == IDLE_CH)) {
      
      switch(mIdleCnt) {
        // If mIdleCnt == 0, this means that there was a Terminate
        // in this column so we should put a ||K|| as the Idle character.
      case 0 :
        laneData[i] = K_CH;
        break;

        // If mIdleCnt == 1 we should alternate between K and A unless A is not yet
        // scheduled to be sent, in which case we should send a K. 
      case 1 :
        if(mSendK || mACount != 0)
          laneData[i] = K_CH;
        else {
          sentA = true;
          laneData[i] = A_CH;
        }
        break;

        // If mIdleCnt == 2 we should send an R.
      case 2 :
        laneData[i] = R_CH;
        break;
        
        // If mIdldeCnt > 2 we should send either K or R based on the LSB of
        // mRandVal, unless mACount == 0 then we should send an A
      default :
        if(mACount == 0) {
          sentA = true;
          laneData[i] = A_CH;
        }
        else if(mRandVal & 1)
          laneData[i] = R_CH;
        else
          laneData[i] = K_CH;
        break;
      }
    }
  }
  
//   UtIO::cout() << "XAUI TX: "
//                << charStr(laneData[0], laneCtrl[0]) << ", "
//                << charStr(laneData[1], laneCtrl[1]) << ", "
//                << charStr(laneData[2], laneCtrl[2]) << ", "
//                << charStr(laneData[3], laneCtrl[3]) << ", "
//                << UtIO::endl;

  // Toggle mSendK every time mIdleCnt == 1
  if(mIdleCnt == 1) mSendK = ~mSendK;

  // If we just sent an ||A|| we should update mACount
  if(sentA) mACount = mRandVal | 16;
  else if(mACount != 0) --mACount;
  
  // Update Idle Count. If not in idle noone is going to look at mIdleCnt anyway,
  // So always incrementing.
  ++mIdleCnt;

  // Update Random Generator
  mRandVal = mRandGen.nextVal();

  // Drive Outputs
  for(int i = 0; i < 4; i++)
    mXauiDL[i] = m8b10b[i].encoder_lut(laneData[i], laneCtrl[i]);
}

CarbonXString CarbonXXgmii2Xaui::charStr(UInt32 data, bool ctrl) {
  CarbonXString str;

  if(ctrl) {
    switch(data) {
    case S_CH : str = " |S|"; break;
    case E_CH : str = " |E|"; break;
    case K_CH : str = " |K|"; break;
    case R_CH : str = " |R|"; break;
    case A_CH : str = " |A|"; break;
    case T_CH : str = " |T|"; break;
    case Q_CH : str = " |Q|"; break;
    default :   
      char cstr[10];
      sprintf(cstr, "I%x", data & 0xff); 
      str = cstr; 
      break;
    }
  }
  else {
    char cstr[10];
    sprintf(cstr, "0x%02x", data & 0xff);
    str = cstr;
  }

  return str;
}

// 
// CarbonXXaui2Xgmii
//

CarbonXXaui2Xgmii::CarbonXXaui2Xgmii() :
  mTime(0),
  mModel(0)
{  
  // Reset Members
  reset();
}

void CarbonXXaui2Xgmii::reset() {

  // Update Simulation Time
  if(mModel) mTime = carbonGetSimulationTime(mModel);

  // Clear Output Ports
  mXgmiiTxData = 0;
  mXgmiiTxCtrl = 0;
  
  // Clear Input Ports
  for(int i = 0; i < 4; i++) {
    mXauiSL[i] = 0x18b; // D00.0 Positive Disparity
    mLane[i].reset();
  }
}

void CarbonXXaui2Xgmii::evaluate()
{
  // Update Simulation Time
  mTime = carbonGetSimulationTime(mModel);

  // Check for Idle Symbols
  UInt8  laneData[4];
  bool   laneCtrl[4];
  
  for(int i = 0; i < 4; i++) {

    // First Decode Inputs
    laneData[i] = mLane[i].nextData(&laneCtrl[i], mXauiSL[i]);
  
    // Now Check if it's an Idle Character
    if(laneCtrl[i]) {
      if(laneData[i] == K_CH || laneData[i] == R_CH || laneData[i] == A_CH) {
        // Convert to XGMII Idle Character
        laneData[i] = IDLE_CH;
      }
    }
  }

  // Drive Outputs
  mXgmiiTxData = 0; mXgmiiTxCtrl = 0;
  for(int i = 0; i < 4; i++) {
    mXgmiiTxData |= ((UInt32)laneData[i]) << (i*8);
    mXgmiiTxCtrl |= (laneCtrl[i] & 1)     << i;
  }
}

CarbonXXauiLane::CarbonXXauiLane()
{
  reset();
}

void CarbonXXauiLane::reset()
{
  m8b10b.clearInputDisparity();
  mSyncState  = eLossOfSync;
  mSyncStatus = false;
}

UInt32 CarbonXXauiLane::nextData(bool *ctrl, UInt32 inData)
{
  if(mReversed) inData = reverse10bit(inData);
  bool err = false;
  UInt32 laneData = m8b10b.decoder_lut(inData, ctrl, &err);
  
  // If code error, convert to Error Character, ignore disparity errors
  if(err && *ctrl && laneData == 0xff)
    laneData = E_CH;

  // Synchronization State Machine
  // This implements the statemachine described in figure 48-7 in the IEEE 802.3-2005 spec 
  switch(mSyncState) {
  case eSyncAcquired1:
    mSyncStatus = true;
    if(err)
      mSyncState = eSyncAcquired2;      
    break;
  case eLossOfSync:
    mSyncStatus = false;
    if(isCOMMA(*ctrl, laneData))
      mSyncState = eCommaDetect1;
    break;
  case eCommaDetect1:
    if(err) 
      mSyncState = eLossOfSync;
    else if(isCOMMA(*ctrl, laneData))
      mSyncState = eCommaDetect2;
    break;
  case eCommaDetect2:
    if(err) 
      mSyncState = eLossOfSync;
    else if(isCOMMA(*ctrl, laneData))
      mSyncState = eCommaDetect3;
    break;
  case eCommaDetect3:
    if(err) 
      mSyncState = eLossOfSync;
    else if(isCOMMA(*ctrl, laneData)) {
      mSyncState = eSyncAcquired1;
    }
    break;
  case eSyncAcquired2:
    if(err)
      mSyncState = eSyncAcquired3;
    else {
      mSyncState = eSyncAcquired2a;
      mGoodCgs   = 0;
    }
    break;
  case eSyncAcquired3:
    if(err)
      mSyncState = eSyncAcquired4;
    else {
      mSyncState = eSyncAcquired3a;
      mGoodCgs   = 0;
    }
    break;
  case eSyncAcquired4:
    if(err)
      mSyncState = eLossOfSync;
    else {
      mSyncState = eSyncAcquired3a;
      mGoodCgs   = 0;
    }
    break;
  case eSyncAcquired2a:
    ++mGoodCgs;
    if(err)
      mSyncState = eSyncAcquired3;
    else if(mGoodCgs == 3)
      mSyncState = eSyncAcquired1;
    break;
  case eSyncAcquired3a:
    ++mGoodCgs;
    if(err)
      mSyncState = eSyncAcquired4;
    else if(mGoodCgs == 3)
      mSyncState = eSyncAcquired2;
    break;
  case eSyncAcquired4a:
    ++mGoodCgs;
    if(err)
      mSyncState = eLossOfSync;
    else if(mGoodCgs == 3)
      mSyncState = eSyncAcquired3;
    break;
  default:
    break;
  }
  
#ifdef XAUI_DEBUG
   if(lastState != mSyncState) {
     UtOStream* ostr = carbonInterfaceGetCout();
     carbonInterfaceWriteStringToOStream(ostr, "Sync State Changed from ");
     sWriteToOStream(ostr, lastState);
     carbonInterfaceWriteStringToOStream(ostr, " to ");
     sWriteToOStream(ostr, mSyncState);
     carbonInterfaceOStreamEndl(ostr);
   }
#endif

  mCodeError = err;
  return laneData;
}
  
bool CarbonXXauiLane::inSync() const
{
  return mSyncStatus;
}

bool CarbonXXauiLane::codeError() const
{
  return mCodeError;
}


bool CarbonXXauiLane::isCOMMA(bool ctrl, UInt8 data) const
{
  return ctrl && (data == K_CH || data == 0x3C || data == 0xFC);
}


int CarbonXXauiLane::reverse10bit(int inData) {
  int outData = 0;
  outData |= (inData << 9) & 0x200;
  outData |= (inData << 7) & 0x100;
  outData |= (inData << 5) & 0x080;
  outData |= (inData << 3) & 0x040;
  outData |= (inData << 1) & 0x020;
  outData |= (inData >> 1) & 0x010;
  outData |= (inData >> 3) & 0x008;
  outData |= (inData >> 5) & 0x004;
  outData |= (inData >> 7) & 0x002;
  outData |= (inData >> 9) & 0x001;
  return outData;
}

// 
// C-model Functions
//

extern __C__ {

  typedef enum CDScarbonx_enetxaui_txContext{
    eCDScarbonx_enetxaui_txRiseDCLK,
  } CDScarbonx_enetxaui_txContext;
  
  void* cds_carbonx_enetxaui_tx_create(int, CModelParam*, const char*)
  {
    CarbonXXgmii2Xaui* handle = new CarbonXXgmii2Xaui;
    return handle;
  }

  void cds_carbonx_enetxaui_tx_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
  {
    CarbonObjectID*    model;
    CarbonXXgmii2Xaui* xaui;

    switch(reason) {
    case eCarbonCModelStart :
      break;

    case eCarbonCModelSave :
      model = (CarbonObjectID*)cmodelData;
      carbonCheckpointWrite(model, hndl, sizeof(CarbonXXgmii2Xaui));
      break;
      
    case eCarbonCModelRestore :
      model = (CarbonObjectID*)cmodelData;
      carbonCheckpointRead(model, hndl, sizeof(CarbonXXgmii2Xaui));
      break;
      
    case eCarbonCModelID :
      // Get Handle to transactor
      xaui = reinterpret_cast<CarbonXXgmii2Xaui*>(hndl);
      xaui->setModel((CarbonObjectID*)cmodelData);
      break;

    default :
      break;
    }
  }

  void cds_carbonx_enetxaui_tx_run(void* hndl, CDScarbonx_enetxaui_txContext context, 
                                   const CarbonUInt32*, const CarbonUInt32* a_sys_reset_l,
                                   CarbonUInt32* a_DL0, CarbonUInt32* a_DL1,
                                   CarbonUInt32* a_DL2, CarbonUInt32* a_DL3,
                                   const CarbonUInt32* a_RXD, const CarbonUInt32* a_RXC,
                                   const CarbonUInt32* a_bitreversal)
  {
    
    // Get Handle to transactor
    CarbonXXgmii2Xaui* xaui = reinterpret_cast<CarbonXXgmii2Xaui*>(hndl);

    // Check Reset
    if(*a_sys_reset_l == 0)
      xaui->reset();

    // Check that we got called because of a rising edge on SCLK
    else if(context == eCDScarbonx_enetxaui_txRiseDCLK) {
      
      // Drive Data into transactor model
      xaui->setRXD(*a_RXD);
      xaui->setRXC(*a_RXC);
      xaui->setBitReversal(*a_bitreversal);

      // Evaluate transactor model
      xaui->evaluate();
    }

    // Now, get the output back into the HDL world
    *a_DL0 = xaui->getDL(0);
    *a_DL1 = xaui->getDL(1);
    *a_DL2 = xaui->getDL(2);
    *a_DL3 = xaui->getDL(3);
  }

  void cds_carbonx_enetxaui_tx_destroy(void* hndl) {
    CarbonXXgmii2Xaui* handle = reinterpret_cast<CarbonXXgmii2Xaui*>(hndl);
    delete handle;
  }

  typedef enum CDScarbonx_enetxaui_rxContext{
    eCDScarbonx_enetxaui_rxRiseSCLK,
  } CDScarbonx_enetxaui_rxContext;

  void* cds_carbonx_enetxaui_rx_create(int, CModelParam*, const char*)
  {
    CarbonXXaui2Xgmii* handle = new CarbonXXaui2Xgmii;
    return handle;
  }
  void cds_carbonx_enetxaui_rx_misc(void* hndl, CarbonCModelReason reason, void* cmodelData)
  {
    CarbonObjectID*    model;
    CarbonXXaui2Xgmii* xaui;

    switch(reason) {
    case eCarbonCModelStart :
      break;
    case eCarbonCModelSave :
      model = (CarbonObjectID*)cmodelData;
      carbonCheckpointWrite(model, hndl, sizeof(CarbonXXaui2Xgmii));
      break;
      
    case eCarbonCModelRestore :
      model = (CarbonObjectID*)cmodelData;
      carbonCheckpointRead(model, hndl, sizeof(CarbonXXaui2Xgmii));
      break;

    case eCarbonCModelID :
      // Get Handle to transactor
      xaui = reinterpret_cast<CarbonXXaui2Xgmii*>(hndl);
      xaui->setModel((CarbonObjectID*)cmodelData);
      break;

    default :
      break;
    }
  }

  void cds_carbonx_enetxaui_rx_run(void* hndl, CDScarbonx_enetxaui_rxContext context, 
                                   const CarbonUInt32*, 
                                   const CarbonUInt32* a_sys_reset_l, 
                                   const CarbonUInt32* a_SL0, const CarbonUInt32* a_SL1, 
                                   const CarbonUInt32* a_SL2, const CarbonUInt32* a_SL3, 
                                   CarbonUInt32* a_TXD, CarbonUInt32* a_TXC, // Output, size = 1 word(s)
                                   const CarbonUInt32* a_bitreversal, 
                                   CarbonUInt32* a_linkstatus, 
                                   CarbonUInt32* a_code_error
    )
  {

    // Get Handle to transactor
    CarbonXXaui2Xgmii* xaui = reinterpret_cast<CarbonXXaui2Xgmii*>(hndl);

    // Check Reset
    if(*a_sys_reset_l == 0)
      xaui->reset();
      
    // Check that we got called because of a rising edge on SCLK
    else if(context == eCDScarbonx_enetxaui_rxRiseSCLK) {
    
      // Drive Data into transactor model
      xaui->setSL(0, *a_SL0);
      xaui->setSL(1, *a_SL1);
      xaui->setSL(2, *a_SL2);
      xaui->setSL(3, *a_SL3);
      xaui->setBitReversal(*a_bitreversal);
      
      // Evaluate transactor model
      xaui->evaluate();
    }

    // Now, get the output back into the HDL world
    *a_TXD = xaui->getTXD();
    *a_TXC = xaui->getTXC();
    *a_linkstatus = xaui->getLinkStatus();
    *a_code_error = xaui->getCodeError();
  }

  void cds_carbonx_enetxaui_rx_destroy(void* hndl)
  {
    // Get Handle to transactor
    CarbonXXaui2Xgmii* xaui = reinterpret_cast<CarbonXXaui2Xgmii*>(hndl);
    delete xaui;
  }

};

void CarbonXXauiLane::sWriteToOStream(UtOStream *os, const CarbonXXauiLane::StateT& state)
{
  switch(state) {
  case eLossOfSync :     carbonInterfaceWriteStringToOStream(os, "Loss Of Sync");     break;
  case eCommaDetect1 :   carbonInterfaceWriteStringToOStream(os, "Comma Detect 1");   break;
  case eCommaDetect2 :   carbonInterfaceWriteStringToOStream(os, "Comma Detect 2");   break;
  case eCommaDetect3 :   carbonInterfaceWriteStringToOStream(os, "Comma Detect 3");   break;
  case eSyncAcquired1 :  carbonInterfaceWriteStringToOStream(os, "Sync Acquired 1");  break;
  case eSyncAcquired2 :  carbonInterfaceWriteStringToOStream(os, "Sync Acquired 2");  break;
  case eSyncAcquired3 :  carbonInterfaceWriteStringToOStream(os, "Sync Acquired 3");  break;
  case eSyncAcquired4 :  carbonInterfaceWriteStringToOStream(os, "Sync Acquired 4");  break;
  case eSyncAcquired2a : carbonInterfaceWriteStringToOStream(os, "Sync Acquired 2a"); break;
  case eSyncAcquired3a : carbonInterfaceWriteStringToOStream(os, "Sync Acquired 3a"); break;
  case eSyncAcquired4a : carbonInterfaceWriteStringToOStream(os, "Sync Acquired 4a"); break;
  case eILLEGAL :        carbonInterfaceWriteStringToOStream(os, "Illegal State");    break;
  }
}
