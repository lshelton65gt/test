//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/* 
 ***************************************************************************** 
 * 
 * Module Name: pci_master_support.c 
 * This module contains the efficiency routines for the PCI master.
 *
 * Author : Nirmala Paul
 * Date   : 6/23/2001
 * 
 ***************************************************************************** 
 */ 
 
/* 
 * include files 
 */ 
#include "tbp.h" 
#include "carbonXTransaction.h"
#include "carbonXPciMaster.h" 
#include "carbonXPciSlave.h" 
#include "util/CarbonPlatform.h"
 
/* 
 ***************************************************************************** 
 * 
 * Functions defined in this file: 
 * 
 ***************************************************************************** 
 */ 

static u_int32 pci_memwr (u_int64 address, u_int32 use_64bit, u_int32 * data, u_int32 length);
static u_int32 pci_memrd (u_int64 address, u_int32 use_64bit, u_int32 *data, u_int32 length);
static u_int32 pcix_memwr(u_int64 address, u_int32 use_64bit, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length);
static u_int32 pcix_memrd(u_int64 address, u_int32 use_64bit, u_int32 req_id, u_int32 req_tag, u_int32 *data, u_int32 length);
 
 

u_int32 carbonXPcixMemWrite(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length)
{
  return( pcix_memwr( address, 0, req_id, req_tag, data, length ) );
}


void carbonXPcixMemWrite_Status(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length, u_int32 * return_status)
{
  *return_status = carbonXPcixMemWrite( address, req_id, req_tag, data, length );
}



u_int32 carbonXPcixMemRead(u_int64 address,  u_int32 req_id, u_int32 req_tag, u_int32 *data, u_int32 length)
{
  return( pcix_memrd( address, 0, req_id, req_tag, data, length ) );
}


void carbonXPcixMemRead_Status(u_int64 address,  u_int32 req_id, u_int32 req_tag, u_int32 *data, u_int32 length, u_int32 * return_status)
{
  *return_status = carbonXPcixMemRead( address, req_id, req_tag, data, length );
}


/* Interrupt Acknowledge cycle */
  
u_int8 carbonXPcixIntAck(u_int32 req_id, u_int32 req_tag)
{
  u_int32 data = 0;
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);
  u_int64 address = 0;

  opcode |= (PCI_INTR_ACK<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, &data, NULL, &size, &status, 
		  TBP_OP_BIDIR, TBP_WIDTH_32);

  return data;
}   


u_int32 carbonXPcixCfgWrite(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 data)
{
  u_int32 arr[4];
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (PCI_CFG_WRITE<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

  arr[0] = data;

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, arr, NULL, &size, &status, 
		  TBP_OP_WRITE, TBP_WIDTH_32);

  return( status );
}


void carbonXPcixCfgWrite_Status(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 data, u_int32 * return_status)
{
  *return_status = carbonXPcixCfgWrite( address, req_id, req_tag, data );
}


u_int32 carbonXPcixCfgRead(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 *data)
{
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (PCI_CFG_READ<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, data, NULL, &size, &status, 
		  TBP_OP_READ, TBP_WIDTH_32);

  return( status );
}


void carbonXPcixCfgRead_Status(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 *data, u_int32 * return_status)
{
  *return_status = carbonXPcixCfgRead( address, req_id, req_tag, data );
}


u_int32 carbonXPcixIOWrite(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 data)
{
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (PCI_IO_WRITE<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, &data, NULL, &size, &status, 
		  TBP_OP_WRITE, TBP_WIDTH_32);

  return( status );
}


u_int32 carbonXPcixIORead(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 *data)
{
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (PCI_IO_READ<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, data, NULL, &size, &status, 
		  TBP_OP_READ, TBP_WIDTH_32);

  return( status );
}


u_int32 carbonXPcix64MemWrite(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length)
{
  return( pcix_memwr( address, 1, req_id, req_tag, data, length ) );
}


void carbonXPcix64MemWrite_Status(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length, u_int32 * return_status)
{
  *return_status = carbonXPcix64MemWrite( address, req_id, req_tag, data, length );
}


u_int32 carbonXPcix64MemRead(u_int64 address,  u_int32 req_id, u_int32 req_tag, u_int32 *data, u_int32 length)
{
  return( pcix_memrd( address, 1, req_id, req_tag, data, length ) );
}


void carbonXPcix64MemRead_Status(u_int64 address,  u_int32 req_id, u_int32 req_tag, u_int32 *data, u_int32 length, u_int32 * return_status)
{
  *return_status = carbonXPcix64MemRead( address, req_id, req_tag, data, length );
}


/////////////////////////////////////////////////////////////////////////////
// This function is to be called from the main test function.
// This function allocates memory for the split table.
//
// The split table is organized as follows :
// * It has a 32 bit valid_bits register. Each bit corresponds to one entry.
//   A bit when set, indicates that the corresponding entry is
//   an outstanding split transaction.
//
// * There are 32 entries for storing upto 32 outstanding split transactions.
//   Each entry has the following details about the transaction :
//   Command       - bit [0] - write/read ...write - 0, read - 1
//   Num_Bytes     - total byte count
//   Start_address - Starting address of the transaction
//   Req_attribute - Requester attribute
//   ptr_write_data - This is the pointer to the expected data (For reads).
/////////////////////////////////////////////////////////////////////////////

PCIX_MSTR_SPLIT_TABLE * carbonXPcixMasterCreateSplitTable() {

  PCIX_MSTR_SPLIT_TABLE * table_ptr;
  u_int32 i;

  if ((table_ptr = (PCIX_MSTR_SPLIT_TABLE *)tbp_malloc(sizeof(PCIX_MSTR_SPLIT_TABLE))) == NULL) {
    MSG_Error("Failed to allocate memory for Split Table for Initiator\n");
  }
  else {
    // Initialize the split table
    table_ptr->valid_bits = 0x00000000;
    for (i=0; i < 32; i++) {
      table_ptr->split_entry[i].Command = 0;
      table_ptr->split_entry[i].Num_bytes = 0;
      table_ptr->split_entry[i].Req_attribute = 0;
      table_ptr->split_entry[i].Start_address = 0;
      table_ptr->split_entry[i].ptr_write_data = 0;
      table_ptr->split_entry[i].compare_data = 0;
    }
    MSG_Milestone("Initialized valid bits to 0\n");
  }

  return(table_ptr);

}


void carbonXPcixMasterDestroySplitTable( PCIX_MSTR_SPLIT_TABLE * split_table_ptr) {

  tbp_free (split_table_ptr);

}


////////////////////////////////////////////////////////////////////////////
// This function is called from the master's attached function.
// This function adds the attributes and other information of a transaction
// that terminated with split response, to a split table.
// The bit corresponding to the entry in teh valid_bits register is set.
////////////////////////////////////////////////////////////////////////////

//***** check if command is burst write..and issue error

void carbonXPcixMstrAddSplitEntry(u_int32 Command, u_int32 Num_bytes, u_int32 Req_attribute, u_int32 Start_address, u_int32 * ptr_write_data, PCIX_MSTR_SPLIT_TABLE * split_table_ptr, u_int32 compare) {

  u_int32 valid_bits;
  u_int32 free_location;
  u_int32 valid_bit_location;
  u_int32 split_table_full;
   
  valid_bits = split_table_ptr->valid_bits;
  free_location = 0x00000000;
  valid_bit_location = 0x00000001;
  split_table_full = 0;

  //-------------------------------------------------//
  //----- Finding a free location in the table  -----//
  //-------------------------------------------------//
  while (((valid_bits & valid_bit_location) != 0x00000000) && 
         (valid_bit_location != 0x00000000)) {
    valid_bit_location = valid_bit_location << 1;
    free_location++;
  }

  if (valid_bit_location == 0x00000000)
    split_table_full = 1;


  //-------------------------------------//
  //----- No free location in table -----//
  //-------------------------------------//
  if (split_table_full) {
    MSG_Error("@%lld: Initiator can have maximum 32 outstanding split transactions\n", tbp_get_sim_time());
  }
  //-------------------------------------------//
  //----- Found a free location in table -----//
  //----- Add the split response details -----//
  //-------------------------------------------//
  else {
    if ((Command == 0) && (Num_bytes > 4)) {
      MSG_Error("@%lld: IBurst write cannot be terminated with split reponse\n", tbp_get_sim_time());
    }
    else {
      split_table_ptr->split_entry[free_location].Command = Command;
      split_table_ptr->split_entry[free_location].Num_bytes = Num_bytes;
      split_table_ptr->split_entry[free_location].Req_attribute = (Req_attribute & 0x001fffff);
      split_table_ptr->split_entry[free_location].Start_address = Start_address;
      split_table_ptr->split_entry[free_location].ptr_write_data = ptr_write_data;
      split_table_ptr->split_entry[free_location].compare_data = compare;
    }
  }

  //------------------------------------------------------//
  //---- Set the valid bit corresponding to that entry ---//
  //------------------------------------------------------//

  split_table_ptr->valid_bits = valid_bits | valid_bit_location;
  MSG_Milestone("Valid_bits in master table %0x\n", split_table_ptr->valid_bits);

}


////////////////////////////////////////////////////////////////////////////
// This function is called from a master's attached function.
// This function receives a transaction, and compares the attribute with
// that in the split table. Data is compared against expected data.
// If the split completion was ok, the bit corresponding to the 
// transaction in the valid_bit table is reset.
// An error is flagged if data is not match with expected data.
////////////////////////////////////////////////////////////////////////////
   
void carbonXPcixMstrRemoveSplitEntry( PCI_OP_T * transaction, PCIX_MSTR_SPLIT_TABLE * split_table_ptr)
{
  u_int32 size, status, location, valid_bit_location;
  u_int32 command;
  u_int32 requester_attr;
  u_int32 completer_attr;
  u_int32 read_data;
  u_int32 temp, temp1, k, address;
  u_int32 num_bytes_remaining;
  u_int32 * wr_ptr; 
  u_int32 * new_wr_ptr;
  u_int32 num_locations_left;
  u_int8 read_byte, exp_byte;

  location = 0;
  valid_bit_location = 0x00000001;
  size = transaction->size; 
  status = transaction->status;

  if (((status >> STATUS__ACCESS_TYPE) & STATUS__ACCESS_TYPE_MASK) != PCIX_SPLIT_COMP)
    return; // Not a split completion, do nothing
  
  requester_attr = ((u_int32*)transaction->read_data)[0];
  completer_attr = ((u_int32*)transaction->read_data)[1];

  //-------------------------------------------//
  //-------- Find the attrbute in the table ---//
  //-------------------------------------------//

  while (((split_table_ptr->split_entry[location].Req_attribute != requester_attr) || ((split_table_ptr->valid_bits & valid_bit_location) == 0x00000000)) && (valid_bit_location != 0x00000000)) {
    location++;
    valid_bit_location = valid_bit_location << 1;
  }


  //------ Get the transaction details --------// 
  command = split_table_ptr->split_entry[location].Command;
  address = split_table_ptr->split_entry[location].Start_address;
  wr_ptr  = split_table_ptr->split_entry[location].ptr_write_data;
  num_bytes_remaining = split_table_ptr->split_entry[location].Num_bytes;

       
  //-------------------------------------------//
  //-------- Requester attr not in table ------//
  //-------------------------------------------//
  if (valid_bit_location == 0x00000000) {
    MSG_Error("@%lld: Requester attribute not found in the split table !!!\n", tbp_get_sim_time());
  } 

  //-------------------------------------------//
  //-------- Requester attr found in table ----//
  //-------------------------------------------//
  else {

    //----------------------------------//
    //-------- Command is Write --------//
    //----------------------------------//
    if (command == 0x00000000) {

      // Write completion should have BCM=0
      if (status & STATUS_SPLIT_BCM_MASK)
        MSG_Milestone("@%lld: #### WARNING: BCM bit set for Write split Completion !!!\n", tbp_get_sim_time());

      // CASE 1: ---------------//
      //--- Error completion ---//
      if (status & STATUS_SPLIT_SPLIT_ERROR_MASK) {
        MSG_Milestone("@%lld: #### Split completion Error for Req Attr %0x ####\n", tbp_get_sim_time(), requester_attr);
        read_data = ((u_int32*)transaction->read_data)[2];
        MSG_Milestone("@%lld: #### Split completion message %0x\n", tbp_get_sim_time(), read_data);
              
        if ((read_data & 0x000fffff) != 0x00000004) {
          MSG_Milestone("@%lld: #### Wrong Split Completion message\n", tbp_get_sim_time());
        }

      }
      // CASE 2: ---------------//
      //--- Normal Completion --//
      else if (status & STATUS_SPLIT_COMP_MSG_MASK) {
        read_data = ((u_int32*)transaction->read_data)[2];
        MSG_Milestone("@%lld: #### Split completion message %0x for Req Attr %0x\n", tbp_get_sim_time(), read_data, requester_attr);

        if (read_data != 0x00000004) {
          MSG_Error("@%lld: #### Wrong split Completion Message\n", tbp_get_sim_time());
        }
      }
      // CASE 3: ---------------//
      //--- SCM & SCE not set ---//
      else {
        MSG_Error("@%lld: #### Write did not get split completion message!!!\n", tbp_get_sim_time());
      }

      // Freeing the allocated wr_ptr
      tbp_free(split_table_ptr->split_entry[location].ptr_write_data);

      //---- Clear the entry ----//
      temp = split_table_ptr->valid_bits;
      temp1 = temp & ~valid_bit_location;
      split_table_ptr->valid_bits = temp1;
      MSG_Milestone("Cleared entry for attr %0x\n", requester_attr);

    } // Command is Write

    //----------------------------------//
    //--------- Command is Read --------//
    //----------------------------------//

    else {
      //---------------------------//
      // CASE 1: Error completion //
      //---------------------------//
      if (status & STATUS_SPLIT_SPLIT_ERROR_MASK) {

        // Split completion message should have BCM=0
        if (status & STATUS_SPLIT_BCM_MASK)
          MSG_Milestone("@%lld: #### WARNING: BCM bit set for Read Split Completion !!!\n", tbp_get_sim_time());

        MSG_Milestone("@%lld: #### Split completion Error for Req Attr %0x \n", tbp_get_sim_time(), requester_attr);
        read_data = ((u_int32*)transaction->read_data)[2];
        MSG_Milestone("@%lld: #### Split completion message %0x\n", tbp_get_sim_time(), read_data);


        // Checking the split completion message
        temp = read_data & 0x00000fff;
        temp1 = (read_data & 0x0007f000) >> 12;
        address = address & 0x0000007f;

        if (temp != num_bytes_remaining) {
          MSG_Milestone("@%lld: #### Wrong Remaining Byte Count in the message\n", tbp_get_sim_time());
          MSG_Error("@%lld: #### Expected: %0x Received: %0x\n", tbp_get_sim_time(), num_bytes_remaining, temp);
        }

        if (temp1 != address) {
          MSG_Milestone("@%lld: #### Wrong Remaining Lower Address in the message\n", tbp_get_sim_time());
          MSG_Error("@%lld: #### Expected: %0x Received: %0x\n", tbp_get_sim_time(), address, temp1);    
        }

        // Freeing the allocated wr_ptr
        tbp_free(split_table_ptr->split_entry[location].ptr_write_data);

        //---- Clear the entry ----//
        temp = split_table_ptr->valid_bits;
        temp1 = temp & ~valid_bit_location;
        split_table_ptr->valid_bits = temp1;
        MSG_Milestone("Cleared entry for attr %0x\n", requester_attr);

      }
      //----------------------------------//
      // CASE 2: Split completion message //
      //----------------------------------//
      else if (status & STATUS_SPLIT_COMP_MSG_MASK) {
        MSG_Milestone("@%lld: ### Split completion for Read got normal completion message - NOT OK\n", tbp_get_sim_time());
        MSG_Error("@%lld: ### Should terminate with Read Data or Error Split Completion Message\n", tbp_get_sim_time());

        // Freeing the allocated wr_ptr
        tbp_free(split_table_ptr->split_entry[location].ptr_write_data);

        //---- Clear the entry ----//
        temp = split_table_ptr->valid_bits;
        temp1 = temp & ~valid_bit_location;
        split_table_ptr->valid_bits = temp1;
        MSG_Milestone("Cleared entry for attr %0x\n", requester_attr);

      }
      //---------------------------//
      // CASE 3: Normal completion //
      //         with Read data    // 
      //---------------------------//
      else {
        //---------------------------------------------------//
        //-----  byte count < full remaining byte count  ----//
        //---------------------------------------------------//
        if (size < num_bytes_remaining) {

          // If size is < full remaining byte count
          // BCM must be set to 1
          if ((status & STATUS_SPLIT_COMP_MSG_MASK) == 0x00000000) {
            MSG_Milestone("@%lld: #### WARNING: BCM bit not set when Byte Count is less than full remaining byte count for Req Attr %0x!!!\n",
                         tbp_get_sim_time(), requester_attr);
          }

          MSG_Milestone("@%lld: #### Did not receive all remaining bytes for Req Attr %0x\n", tbp_get_sim_time(), requester_attr);
          MSG_Milestone("@%lld: #### Number of bytes received = %0x \n", tbp_get_sim_time(), size);


          //--- If the compare bit in the entry is set  ----//
          //--- Compare the read data with expected data ---//

          if (split_table_ptr->split_entry[location].compare_data) {
/*             for (k=0; k < (size+3)/4; k++) { */
/*               read_data = ((u_int32*)transaction->read_data)[2+k]; */
/*               exp_data  = *(split_table_ptr->split_entry[location].ptr_write_data+k); */
/*               if (read_data == exp_data) { */
/*                 //MSG_Milestone("@%lld: Read %0x from %0x \n", tbp_get_sim_time(), read_data, address); */
/*               } */
/*               else { */
/*                 MSG_Error("@%lld: Expected data %0x Read data %0x from address %0x \n", tbp_get_sim_time(), exp_data, read_data, address); */
/*               } */
/*             } */
            for (k=0; k < size; k++) {
              read_byte = ((u_int32*)transaction->read_data)[2+k/4] >> ((k%4) * 8);
              exp_byte = split_table_ptr->split_entry[location].ptr_write_data[k/4] >> ((k%4) * 8);
              if (read_byte == exp_byte) {
                //MSG_Milestone("@%lld: Read %0x from %0x \n", tbp_get_sim_time(), read_data, address);
              }
              else {
                MSG_Error("@%lld: Expected data %0x Read data %0x from address %0x \n", tbp_get_sim_time(), exp_byte, read_byte, address+k);
              }
            }
          }
          //--- If the compare bit in the entry is 0  ----//
          //--- Write the read data into the table entry ---//

          else {
            for (k=0; k < (size+3)/4; k++) {
              read_data = ((u_int32*)transaction->read_data)[2+k]; 
              *(split_table_ptr->split_entry[location].ptr_write_data+k) = read_data;
            }
          }

          // Updating the table with the remaining bytes & new address
          num_bytes_remaining = num_bytes_remaining - size;
          split_table_ptr->split_entry[location].Num_bytes = num_bytes_remaining;
          split_table_ptr->split_entry[location].Start_address = address + size;

          // Position of next data
          wr_ptr = &(wr_ptr[k/4]);
          num_locations_left = (num_bytes_remaining+3)/4;

          // Assigning a new area for comparing read data
          new_wr_ptr = (u_int32 *)tbp_malloc(num_locations_left*sizeof(u_int32));

          // Copying the reference data into the new area
          for(k=0; k < num_locations_left; k++) {
            new_wr_ptr[k] = wr_ptr[k];
          }

          tbp_free(split_table_ptr->split_entry[location].ptr_write_data);
          split_table_ptr->split_entry[location].ptr_write_data = new_wr_ptr;
        }

        //---------------------------------------------------//
        //--- byte count equals full remaining byte count ---//
        //---------------------------------------------------//
        else {

          // If size is == full remaining byte count
          // BCM should be 0
          if (status & STATUS_SPLIT_COMP_MSG_MASK) {
            MSG_Milestone("@%lld: #### WARNING: BCM bit set when Byte Count equals the full remaining byte count for Req Attr %0x!!!\n",
                         tbp_get_sim_time(), requester_attr);
          }

          if (split_table_ptr->split_entry[location].compare_data) {
/*             for (k=0; k < (size+3)/4; k++) { */
/*               read_data = ((u_int32*)transaction->read_data)[2+k]; */
/*               exp_data  = *(split_table_ptr->split_entry[location].ptr_write_data+k); */
/*               if (read_data == exp_data) { */
/*                 //MSG_Milestone("Read %0x from %0x \n", read_data, address); */
/*               } */
/*               else { */
/*                 MSG_Error("@%lld: Expected data %0x Read data %0x from address %0x \n", tbp_get_sim_time(), exp_data, read_data, address); */
/*               } */
/*             } */
            for (k=0; k < size; k++) {
              read_byte = ((u_int32*)transaction->read_data)[2+k/4] >> ((k%4) * 8);
              exp_byte = split_table_ptr->split_entry[location].ptr_write_data[k/4] >> ((k%4) * 8);
              if (read_byte == exp_byte) {
                //MSG_Milestone("@%lld: Read %0x from %0x \n", tbp_get_sim_time(), read_data, address);
              }
              else {
                MSG_Error("@%lld: Expected data %0x Read data %0x from address %0x \n", tbp_get_sim_time(), exp_byte, read_byte, address+k);
              }
            }
          }
          else {
            for (k=0; k < (size+3)/4; k++) {
              read_data = ((u_int32*)transaction->read_data)[2+k];
              *(split_table_ptr->split_entry[location].ptr_write_data+k) = read_data;
            }

          }

          // Freeing the memory used for read reference data
          tbp_free(split_table_ptr->split_entry[location].ptr_write_data);
                   
          //---- Setting the valid bit to 0 ---//
          temp = split_table_ptr->valid_bits;
          temp1 = temp & ~valid_bit_location;
          split_table_ptr->valid_bits = temp1;
          MSG_Milestone("@%lld: #### Normal completion for Req Attr %0x\n", tbp_get_sim_time(), requester_attr);
          MSG_Milestone("Cleared entry for attr %0x\n", requester_attr);
          MSG_Milestone("Valid bits in master table %0x\n", split_table_ptr->valid_bits);

        } // Byte count == Expected total byte count 

      } // Split completion Read data

    } // Command is Read

  } // Req_attr is in the split table

}
               
               

////////////////////////////////////////////////////////////////////////////
// This function checks if the attribute exists in the split table
// If it exists, the PCIX master's target state machine asserts devsel
// and claims the transaction
////////////////////////////////////////////////////////////////////////////

void carbonXPcixAttributeInRange(PCIX_MSTR_SPLIT_TABLE * split_table_ptr,  PCI_OP_T * transaction)
{
  u_int32 req_attr;
  u_int32 temp, temp1;
  u_int32 valid_bits;
  u_int32 valid_location;
  u_int32 location;
  u_int32 InRange;
  u_int32 cmd;


  cmd = (transaction->status>>STATUS__ACCESS_TYPE) & STATUS__ACCESS_TYPE_MASK;
  if (cmd == PCIX_SPLIT_COMP)
    {
      req_attr = (((u_int32)(transaction->address>>32)) & 0x1fffff00) >> 8;

      InRange = 0;
      location = 0;
      valid_location = 0x00000001;
      valid_bits = split_table_ptr->valid_bits;

      while(valid_location != 0x00000000) {
        temp = split_table_ptr->split_entry[location].Req_attribute;
        temp1 = valid_bits & valid_location;

        if ((req_attr == temp) && (temp1 != 0x00000000)) {
          InRange = 1;
        }
        valid_location = valid_location << 1;
        location++;
      }
    }
  else 
    {
      InRange = 0;
    }


  transaction->status = InRange;
  transaction->size = 0;
  transaction->opcode = TBP_NXTREQ;

}

      
/* 
 ***************************************************************************** 
 * PCI Function calls are listed below. 
 * Each of them return the transaction status to the test.
 * carbonXPciCfgWrite32()
 * carbonXPciCfgRead32()
 * carbonXPciMemWrite()
 * carbonXPciMemWrite32()
 * carbonXPciMemWrite16()
 * carbonXPciMemWrite8()
 * carbonXPciMemRead()
 * carbonXPciMemRead32()
 * carbonXPciMemRead16()
 * carbonXPciMemRead8()
 * carbonXPci64MemWrite()
 * carbonXPci64MemRead()
 *
 ***************************************************************************** 
 */ 
 

void carbonXPciCfgWrite32_Status(u_int64 address, u_int32 data, u_int32 * return_status)
{ 
  u_int32 status = 0;
  u_int32 size = 4;   
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (1<<USER_OP__WRITE);
  opcode |= (PCI_CFG_WRITE<<USER_OP__PCI_CMD);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, &data, NULL, &size, &status, 
		  TBP_OP_WRITE, TBP_WIDTH_32);

  *return_status = status;
} 
 
 
u_int32 carbonXPciCfgRead32_Status(u_int64 address, u_int32 * return_status)
{ 
  u_int32 arr[2];
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (PCI_CFG_READ<<USER_OP__PCI_CMD);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, arr, NULL, &size, &status, 
		  TBP_OP_READ, TBP_WIDTH_32);

  *return_status = status;
  return(arr[0]);
} 
 

void carbonXPciIOWrite_Status(u_int64 address, u_int32 data, u_int32 * return_status)
{
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (1<<USER_OP__WRITE);
  opcode |= (PCI_IO_WRITE<<USER_OP__PCI_CMD);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, &data, NULL, &size, &status, 
		  TBP_OP_WRITE, TBP_WIDTH_32);

  *return_status = status;
}


u_int32 carbonXPciIORead_Status(u_int64 address, u_int32 * return_status)
{
  u_int32 arr[2];
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= (PCI_IO_READ<<USER_OP__PCI_CMD);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, arr, NULL, &size, &status, 
		  TBP_OP_READ, TBP_WIDTH_32);

  *return_status = status;
  return(arr[0]);
}


void carbonXPciMemWrite_Status(u_int64 address, u_int32 * data, u_int32 length, u_int32 * return_status)
{ 
  *return_status = pci_memwr( address, 0, data, length );
} 


void carbonXPciMemWrite32_Status(u_int64 address, u_int32 data, u_int32 * return_status)
{ 
  *return_status = pci_memwr( address, 0, &data, 4 );
} 
 

void carbonXPciMemWrite16_Status(u_int64 address, u_int16 data, u_int32 * return_status)
{
  u_int32 local_data = data;
  *return_status = pci_memwr( address, 0, &local_data, 2 );
} 
 
 
void carbonXPciMemWrite8_Status(u_int64 address, u_int8 data, u_int32 * return_status)
{
  u_int32 local_data = data;
  *return_status = pci_memwr( address, 0, &local_data, 1 );
} 
 

void carbonXPci64MemWrite_Status(u_int64 address, u_int32 * data, u_int32 length, u_int32 * return_status)
{
  *return_status = pci_memwr( address, 1, data, length );
}

 
void carbonXPciMemRead_Status(u_int64 address, u_int32 *data, u_int32 length, u_int32 * return_status)
{
  *return_status = pci_memrd( address, 0, data, length );
} 
 
 
u_int32 carbonXPciMemRead32_Status(u_int64 address, u_int32 * return_status)
{
  u_int32 data = 0;
  *return_status = pci_memrd( address, 0, &data, 4 );
  return( data );
} 
 
 
u_int16 carbonXPciMemRead16_Status(u_int64 address, u_int32 * return_status)
{ 
  u_int32 data = 0;
  *return_status = pci_memrd( address, 0, &data, 2);
  return((u_int16)(data));
} 
 

u_int8 carbonXPciMemRead8_Status(u_int64 address, u_int32 * return_status)
{ 
  u_int32 data = 0;
  *return_status = pci_memrd( address, 0, &data, 1);
  return((u_int8)(data));
} 

 
void carbonXPci64MemRead_Status(u_int64 address, u_int32 *data, u_int32 length, u_int32 * return_status)
{
  *return_status = pci_memrd( address, 1, data, length );
}

 
/* 
 ***************************************************************************** 
 * 
 * The following functions dont return the transaction status to the test.
 * Other than this difference they are identical to the function above.
 * carbonXPciCfgWrite32()
 * carbonXPciCfgRead32()
 * carbonXPciMemWrite()
 * carbonXPciMemWrite32()
 * carbonXPciMemWrite16()
 * carbonXPciMemWrite8()
 * carbonXPciMemRead()
 * carbonXPciMemRead32()
 * carbonXPciMemRead16()
 * carbonXPciMemRead8()
 * carbonXPci64MemWrite()
 * carbonXPci64MemRead()
 *
 * carbonXPciCheckStatus() is called in each of the above functions. This function
 * checks the return status and causes the test to exit on any error.
 ***************************************************************************** 
 */ 
 

void carbonXPciCfgWrite32(u_int64 address, u_int32 data)
{ 
  u_int32 status = 0;
  carbonXPciCfgWrite32_Status( address, data, &status );
  carbonXPciCheckStatus(status, address, 1);
} 
 
 
u_int32 carbonXPciCfgRead32(u_int64 address)
{ 
  u_int32 status = 0;
  u_int32 data;

  data = carbonXPciCfgRead32_Status( address, &status );
  carbonXPciCheckStatus(status, address, 0);

  return( data );
} 
 
 
void carbonXPciIOWrite(u_int64 address, u_int32 data)
{
  u_int32 status = 0;
  carbonXPciIOWrite_Status( address, data, &status );
  carbonXPciCheckStatus(status, address, 1);
}


u_int32 carbonXPciIORead(u_int64 address)
{
  u_int32 status = 0;
  u_int32 data;

  data = carbonXPciIORead_Status( address, &status );
  carbonXPciCheckStatus(status, address, 0);

  return(data);
}


static u_int32 pci_memwr(u_int64 address, u_int32 use_64bit, u_int32 * data, u_int32 length)
{
  u_int32 status = 0;
  u_int32 size = length;
  u_int32 opcode = (1<<USER_OP__VALID);
  opcode |= (1<<USER_OP__WRITE);
  opcode |= ((use_64bit&1)<<USER_OP__64BIT);
  opcode |= (PCI_MEM_WRITE<<USER_OP__PCI_CMD);

  carbonXTransaction(&opcode, &address, data, NULL , &size, &status, TBP_OP_WRITE, TBP_WIDTH_32);
/*   MSG_Milestone("MEM WR-- A:%08llx  SZ: %d   D: ", addr, size); */

/*   for (i=0; i < (length+3)/4; i++) { */
/*     MSG_Printf(" %08x", data[i]); */
/*     if ((i & 7) == 7)MSG_Printf("\n                                                "); */
/*   } */
/*   MSG_Printf("\n"); */

  return( status );
}


u_int32 carbonXPciMemWrite(u_int64 address, u_int32 * data, u_int32 length)
{ 
  u_int32 status = 0;
  status = pci_memwr( address, 0, data, length );
  carbonXPciCheckStatus(status, address, 1);
  return( status );
} 
 
// Request 64 bit transfer
u_int32 carbonXPci64MemWrite(u_int64 address, u_int32 * data, u_int32 length)
{
  u_int32 status = 0;
  status = pci_memwr( address, 1, data, length );
  carbonXPciCheckStatus(status, address, 1);
  return( status );
}

 
void carbonXPciMemWrite32(u_int64 address, u_int32 data)
{ 
  u_int32 status = 0;
  carbonXPciMemWrite32_Status( address, data, &status );
  carbonXPciCheckStatus(status, address, 1);
} 
 

void carbonXPciMemWrite16(u_int64 address, u_int16 data)
{ 
  u_int32 status = 0;
  carbonXPciMemWrite16_Status( address, data, &status );
  carbonXPciCheckStatus(status, address, 1);
} 
 
 
void carbonXPciMemWrite8(u_int64 address, u_int8 data)
{ 
  u_int32 status = 0;
  carbonXPciMemWrite8_Status( address, data, &status );
  carbonXPciCheckStatus(status, address, 1);
} 
 
static u_int32 pci_memrd(u_int64 address, u_int32 use_64bit, u_int32 *data, u_int32 length)
{
  u_int32 status = 0;
  u_int32 size = length;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= ((use_64bit&1)<<USER_OP__64BIT);
  opcode |= (PCI_MEM_READ<<USER_OP__PCI_CMD);

  carbonXTransaction(&opcode, &address, data, NULL , &size, &status, TBP_OP_READ, TBP_WIDTH_32);

/*   MSG_Milestone("MEM RD-- A:%08x  SZ: %d   D: ", addr, size); */

/*   for (i=0; i < (length+3)/4; i++) { */
/*     MSG_Printf(" %08x", data[i]); */
/*     if ((i & 7) == 7)MSG_Printf("\n                                                "); */
/*   } */
/*   MSG_Printf("\n"); */

  return( status );
}


u_int32 carbonXPciMemRead(u_int64 address, u_int32 *data, u_int32 length)
{ 
  u_int32 status = 0;
  status = pci_memrd( address, 0, data, length );
  carbonXPciCheckStatus(status, address, 0);
  return( status );
} 
 
 
u_int32 carbonXPciMemRead32(u_int64 address)
{ 
  u_int32 status = 0;
  u_int32 data;
  data = carbonXPciMemRead32_Status( address, &status );
  carbonXPciCheckStatus(status, address, 0);
  return( data );
} 
 
 
u_int16 carbonXPciMemRead16(u_int64 address)
{
  u_int32 status = 0;
  u_int16 data;
  data = carbonXPciMemRead16_Status( address, &status );
  carbonXPciCheckStatus(status, address, 0);
  return( data );
} 
 

u_int8 carbonXPciMemRead8(u_int64 address)
{ 
  u_int32 status = 0;
  u_int8 data;
  data = carbonXPciMemRead8_Status( address, &status );
  carbonXPciCheckStatus(status, address, 0);
  return( data );
} 

 
u_int32 carbonXPci64MemRead(u_int64 address, u_int32 *data, u_int32 length)
{
  u_int32 status = 0;
  status = pci_memrd( address, 1, data, length );
  carbonXPciCheckStatus(status, address, 0);
  return( status );
}


u_int8 carbonXPciIntAck()
{
  u_int32 arr = 0;
  u_int32 status = 0;
  u_int32 size = 4;
  u_int32 opcode = (1<<USER_OP__VALID);
  u_int64 address = 0;

  opcode |= (PCI_INTR_ACK<<USER_OP__PCI_CMD);
 
  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, &arr, NULL, &size, &status, 
		  TBP_OP_READ, TBP_WIDTH_32);

  return arr;
}
 

// This function checks the status and exits if the transaction
// did not terminate normally, or if there were any data/address
// parity errors.

void carbonXPciCheckStatus(u_int32 status, u_int32 address, u_int32 write) {

  char rd_wr[6];

  if (write == 0x00000001)
    strcpy(rd_wr, "Write");
  else
    strcpy(rd_wr, "Read ");

  //    MSG_Milestone("For Address: %08x   CheckStatus: %08x\n", address, status);

  switch(status) {
  case STATUS__NORMAL : {
    //    MSG_Milestone("@%lld: #### Normal Termination (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__MASTER_ABORT : {
    MSG_Error("@%lld: #### Master Abort (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__TARGET_ABORT : {
    MSG_Error("@%lld: #### Target Abort (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__DATA_DISCONNECT_A : {
    MSG_Error("@%lld: #### Disconnect A (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__DATA_DISCONNECT_B : {
    MSG_Error("@%lld: #### Disconnect B (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__DATA_DISCONNECT_C : {
    MSG_Error("@%lld: #### Disconnect C (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__TARGET_RETRY : {
    MSG_Error("@%lld: #### Target Retry (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__DATA_PARITY_ERROR : {
    MSG_Error("@%lld: #### Data Parity error (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  case STATUS__ADDR_PARITY_ERROR : {
    MSG_Error("@%lld: #### Address Parity error (%08x) for %s starting at address %0x\n", tbp_get_sim_time(), status, rd_wr, address);
    break; }
  }

}
      
u_int32 carbonXPciCmd(u_int32 command,   // PCI command to use; little checking is done vs. other arguments right now
              u_int64 address,   // address (DAC is used if address[63:32] non-zero)
              u_int32 req64data, // request a 64-bit-wide data transfer
              u_int32 * data,    // if write, contains data; if read, must be large enough to hold requested data
              u_int32 size)    // in bytes
{
  u_int32 status = 0;
  u_int32 opcode = 0;

  if ( command == PCI_DAC ) {
    MSG_Milestone("Dual-address cycle is not directly controlled.\n");
    MSG_Error("Use address with upper word non-zero to implicitly cause DAC");
    return 0;
  }

  if ( command == PCI_RESERVED1 ||
       command == PCI_RESERVED2 ||
       command == PCI_RESERVED3 ||
       command == PCI_RESERVED4 ) {
    MSG_Error("Reserved command %f not supported\n",command);
    return 0;
  }


  opcode = (1<<USER_OP__VALID);

  opcode |= ((req64data&1)<<USER_OP__64BIT);

  opcode |= ((command&0xf)<<USER_OP__PCI_CMD);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, data, NULL, &size, &status, 
		  TBP_OP_BIDIR, TBP_WIDTH_32);

  //MSG_Milestone("command=%x,op=%8x,addr=%016llx,size=%3d,status=%08x\n",
  //       command,opcode,address,size,status);

  return( status );
}

u_int32 carbonXPcixCmd(u_int32 command, u_int64 address, u_int32 req64data, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length)
{
  u_int32 status = 0;
  u_int32 size = length;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= ((req64data&1)<<USER_OP__64BIT);
  opcode |= (command<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, data, NULL, &size, &status, 
		  TBP_OP_BIDIR, TBP_WIDTH_32);

  return( status );
}

static u_int32 pcix_memwr(u_int64 address, u_int32 use_64bit, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length)
{
  u_int32 status = 0;
  u_int32 size = length;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= ((use_64bit&1)<<USER_OP__64BIT);
  opcode |= (PCI_MEM_WRITE<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);
  
  
  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, data, NULL, &size, &status, 
		  TBP_OP_WRITE, TBP_WIDTH_32);

  return( status );
}

static u_int32 pcix_memrd(u_int64 address, u_int32 use_64bit, u_int32 req_id, u_int32 req_tag, u_int32 *data, u_int32 length)
{
  u_int32 i;
  u_int32 status = 0;
  u_int32 size = length;
  u_int32 opcode = (1<<USER_OP__VALID);

  opcode |= ((use_64bit&1)<<USER_OP__64BIT);
  opcode |= (PCIX_MEM_READ_BLK<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_MASK)<<USER_OP__REQ_TAG);

  for (i=0; i < (length+3)/4; i++)  // fill the array with junk
    data[i] = 0xfeedbeef;

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &address, data, NULL, &size, &status, 
		  TBP_OP_READ, TBP_WIDTH_32);

  return( status );
}

