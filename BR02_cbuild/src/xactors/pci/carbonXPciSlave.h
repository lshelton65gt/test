
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef PCI_SLAVE_SUPPORT__H
#define PCI_SLAVE_SUPPORT__H

#ifdef __cplusplus
extern "C" {
#endif

#include "xactors/sparse_mem/carbonXSparseMem.h"

/*************************************************
  Defines for Transactor Config registers
*************************************************/ 
#define CARBONX_PCI_CONFIG_REG  0
#define CARBONX_PCI_BAR_SIZE0   4
#define CARBONX_PCI_BAR_SIZE1   8
#define CARBONX_PCI_BAR_SIZE2  12
#define CARBONX_PCI_BAR_SIZE3  16
#define CARBONX_PCI_BAR_SIZE4  20
#define CARBONX_PCI_BAR_SIZE5  24
#define CARBONX_PCI_BAR0       28
#define CARBONX_PCI_BAR1       32
#define CARBONX_PCI_BAR2       36
#define CARBONX_PCI_BAR3       40
#define CARBONX_PCI_BAR4       44
#define CARBONX_PCI_BAR5       48

/*************************************************
  Defines the type of access requested
*************************************************/ 
#define PCI_CFG_ACCESS 0x00000001 
#define PCI_IO_ACCESS  0x00000002 
#define PCI_MEM_ACCESS 0x00000000 

// Macro to mash together the 7 components of the status returned to the slave BFM
// in range, use32bit, cause perr, devsel count,
// resp, num data phases, num wait cycles
#define PCI_STATMERGE(inr,u32,cpe,dsc,rsp,ndp,nwc)   ((nwc)&0x0f)      \
                                                  | (((ndp)&0xff)<<08) \
                                                  | (((rsp)&0x0f)<<16) \
                                                  | (((dsc)&0x0f)<<20) \
                                                  | (((cpe)&0x01)<<29) \
                                                  | (((u32)&0x01)<<30) \
                                                  | (((inr)&0x01)<<31)


/*************************************************
  Target Response types -- part of return status
*************************************************/ 
#define NORMAL 0x0
#define RETRY  0x1
#define TABRT  0x2
#define DISCNT 0x3
#define ADB    0x4
#define SPLIT  0x5

// For backwards compatibility
#define SINGLE_DATA_DISCNT DISCNT
#define DISCONNECT_ADB     ADB
#define SPLIT_RESPONSE     SPLIT
 
#define PCI_MAX_BASE_REGS     32 
#define PCI_MEM_BASE_ADDR     7 
#define PCI_MEM_LIMIT_ADDR    8
#define PCI_MEM_BASE_ADDR_HI  9 
#define PCI_MEM_LIMIT_ADDR_HI 0xa
 
// These modified codes are used by the target to indicate
// that the write data for the bus operation is now available
//  These are in addition to pci command codes
#define PCI_IO_WRITE_CMP       0x13
#define PCI_MEM_WRITE_CMP      0x17
#define PCIX_MEM_WRITE_BLK_CMP 0x19
#define PCI_CFG_WRITE_CMP      0x1b
#define PCI_MEM_WRITE_INV_CMP  0x1f

#define DECODE_A      0x00000000
#define DECODE_B      0x00000001
#define DECODE_C      0x00000002
#define DECODE_S      0x00000003
#define DECODE_FAST   0x00000000
#define DECODE_MEDIUM 0x00000001
#define DECODE_SLOW   0x00000002

#define PCI_BE_MASK        0x000000f0 
#define PCI_BE0_MASK       0x00000001 
#define PCI_BE1_MASK       0x00000002 
#define PCI_BE2_MASK       0x00000004 
#define PCI_BE3_MASK       0x00000008 

#define RET_STATUS__NUM_DATA_PH     8
#define RET_STATUS__NUM_INIT_WAIT   5
#define RET_STATUS__RESP_TYPE       0
#define RET_STATUS__NUM_WAIT        0
#define RET_STATUS__DEC_TIME        0

#define RET_STATUS__NUM_DATA_PH_MASK    0xff
#define RET_STATUS__NUM_INIT_WAIT_MASK     7
#define RET_STATUS__RESP_TYPE_MASK       0xf
#define RET_STATUS__NUM_WAIT_MASK       0xff
#define RET_STATUS__DEC_TIME_MASK        0xf


#define GET_RESP_STATUS__CONFIG_WR_MASK 1
#define GET_RESP_STATUS__CONFIG_RD_MASK 2
#define GET_RESP_STATUS__CONFIG_MASK    3
#define GET_RESP_STATUS__MEM_WR_MASK    4
#define GET_RESP_STATUS__MEM_RD_MASK    8
#define GET_RESP_STATUS__MEM_MASK       0xc

/*************************************************
  Efficieny routines are declared here
*************************************************/ 

typedef struct {
  u_int32 opcode;         // read/write etc..
  u_int64 address;        // 64bit operation address
  void *  read_data;      // data returning from the sim/hdl
  void *  write_data;     // data being written to the hdl
  u_int32 size;           // size in "bytes"
  u_int32 status;         // user/bfm defined
  u_int32 interrupt;      // 32bit interrupt vector !=32'h0 == interrupt
} PCI_OP_T;

typedef struct {
  u_int32  config_array[16];
  PCI_OP_T transaction;
} PCI_SLAVE_INST_DATA;

void carbonXPciSendOperation(PCI_OP_T * pci_trans);
PCI_OP_T * carbonXPciGetOperation(void);

/*  A simple default slave driver function */
void carbonXPciSlaveFunction(u_int32 mem_id, u_int32 *status, u_int32 size  /*, ??? qname */);
void carbonXPciSlvProcessWrite(u_int32 mem_id, PCI_OP_T *transaction); 
void carbonXPciSlvProcessRead(u_int32 mem_id, PCI_OP_T *  transaction);

#define PCIX_SPLIT_COMPLETION 0x00000008
#define PCIX_INIT_64          0x00000010
 
#define STATUS__REQ_ATTR    8
#define STATUS__CBE         4
#define STATUS__SPLIT_COMP  4
#define STATUS__ACCESS_TYPE 0

#define STATUS__REQ_ATTR_MASK    0x1fffff
#define STATUS__CBE_MASK         0xf
#define STATUS__SPLIT_COMP_MASK  1
#define STATUS__ACCESS_TYPE_MASK 0xf

//#define PCI_IO_STATUS      0x00000004 
//#define PCI_MEM_STATUS     0x00000002 
//#define PCI_CFG_STATUS     0x00000000 
//#define PCI_STATUS_MASK    0x0000000e 

#define STATUS_SPLIT_BCM_MASK         0x40
#define STATUS_SPLIT_SPLIT_ERROR_MASK 0x30
#define STATUS_SPLIT_COMP_MSG_MASK    0x10

// Slave structure
typedef struct { 
  u_int32 base_reg[PCI_MAX_BASE_REGS]; 
} PCI_SLAVE_T; 


PCI_SLAVE_T * PCI_CreateSlave(u_int32 mem_size); 
typedef struct {
  u_int32 Command;
  u_int32 Num_bytes;
  u_int32 Req_attribute;
  u_int64 Start_address;
  u_int32 bcm;
} SLV_SPLIT_ENTRY;


typedef struct {
  u_int32 valid_bits;
  SLV_SPLIT_ENTRY split_entry[32];
} PCIX_SLV_SPLIT_TABLE;

struct carbonXPciConfigRegsS {
#ifdef __cplusplus
  protected:
#endif
  UInt32  mSize;  // Size of PCI Config Space
  UInt32 *mMem;   // PCI Config Space Memory
  UInt32 *mWe;    // PCI Config Space Write Enable. Sets what bits are Writable.
  UInt32 *mCe;    // PCI Config Space Clear Enable. Sets what bits can be cleared by writing oned to them
};

typedef struct carbonXPciConfigRegsS * carbonXPciConfigRegsT;

PCIX_SLV_SPLIT_TABLE * carbonXPcixSlaveCreateSplitTable();

void carbonXPcixSlvAddSplitEntry(u_int32 Command, u_int32 Num_bytes, u_int32 Req_attribute, u_int64 Start_address, PCIX_SLV_SPLIT_TABLE * split_table_ptr);

void carbonXPcixSlvRemoveSplitEntry(PCIX_SLV_SPLIT_TABLE * split_table_ptr, PCI_SLAVE_T * slave_ptr, u_int32 completer_id, u_int32 split_error, u_int32 split_bcm);
void carbonXPcix64SlvRemoveSplitEntry(PCIX_SLV_SPLIT_TABLE * split_table_ptr, PCI_SLAVE_T * slave_ptr, u_int32 completer_id, u_int32 split_error, u_int32 split_bcm);

void carbonXPcixSplitComplete(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length, u_int32 * return_status, u_int32 req64);

void carbonXPcixSlaveDestroySplitTable(PCIX_SLV_SPLIT_TABLE * split_table_ptr);

void carbonXPciSlvSparseMemWrite(sparseMem_t mem, const carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *request, struct carbonXTransactionStruct *response);
void carbonXPciSlvSparseMemRead(sparseMem_t mem, const carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *request, struct carbonXTransactionStruct *response);
void carbonXPciSlaveCfgWrite(carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *req, struct carbonXTransactionStruct *resp);
void carbonXPciSlaveCfgRead(const carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *req, struct carbonXTransactionStruct *resp);
carbonXPciConfigRegsT carbonXCreateConfigRegs(UInt32 size);
void carbonXPciInitConfigRegs(carbonXPciConfigRegsT cfg, UInt32 size);
void carbonXDestroyConfigRegs(carbonXPciConfigRegsT cfg);

#ifdef __cplusplus
}
#endif

#endif
