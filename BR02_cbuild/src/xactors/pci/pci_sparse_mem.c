//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/* This is "good" file. 7 breaks in case statements were missing
  they are on lines: 328, 330, 332, 334, 336, 344, 347 of this file. */
/*******************************************************
 *
 * Hash function adapted from PJW generic hash
 *
 * Accepts a pointer to a datum to be hashed and returns
 * an unsigned integer key
 *
 * This implementation will use linear rehashing for
 * collisions with a step function of 5.  Since memory is
 * never deleted, the linear rehash will not be an issue
 *
 * A single hash table will be used for all memories.  Class,
 * id, and address will all be used with the hash function

*******************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

/* #ifdef IN_EC */
/* #include "string.h" */
/* #include "sched.h" */
/* #include "machine/kprintf.h" */
/* #endif */

#include "tbp.h" 
#include "carbonXPciSparseMem.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"

#define MEM_DATA_BLOCK_SIZE    32
#define HASH_ADDR_MASK     0xffffffe0
#define HASH_ADDR_MODULO   0x1f
#define HASH_STRIDE        0x7
#define HASH_TABLE_SIZE         517717

#define ulong  unsigned long

struct  mem_data_struct {
  ulong data[MEM_DATA_BLOCK_SIZE];
  int   class, id;
  ulong address_hi, address;
};

/* #ifdef IN_EC */
/* static struct mem_data_struct **mem_data_table; */
/* #else */

static struct mem_data_struct *mem_data_table[HASH_TABLE_SIZE];

/* #endif */

/* the following variables are for statistics on the hash table
   effectiveness */
static int    hashstat_filled_locations;
#define LEVEL_KEEP	4
static int    hashstat_level_hits[LEVEL_KEEP];
static int    hashstat_level_coll[LEVEL_KEEP];
static int    hashstat_max_coll;
static int    print_warning_once;

/* Forward declaration */
int print_hash_statistics(void);

/* #ifdef IN_EC */
/* void */
/* init_sparse_mem(void) */
/* { */
/*     mem_data_table = (struct mem_data_struct **) */
/* 	malloc(HASH_TABLE_SIZE*sizeof(*mem_data_table)); */
/*     bzero(mem_data_table,HASH_TABLE_SIZE*sizeof(*mem_data_table)); */
/* } */
/* #endif */

static u_int32 elfhash (const unsigned char *name)
{
  u_int32 h = 0, g;
  while (*name) {
    h = (h << 4) + *name++;
    if ((g = (h & 0xf0000000)) != 0)
      h ^= g >> 24;
    g &= ~g;
  }
  return h;
}

/***********************************************************
  get_hash_entry

  performs all the hash table lookups, linear
  rehashing, and memory allocation functions.  The memory
  routines simply call this function with the class, id, and
  address.  This routine will always return to an allocated
  mem_data_struct pointer.

  Note that this function is reentrant to handle the linear
  rehashing
  **********************************************************/

static struct mem_data_struct * get_hash_entry(int class,
                                               int id,
                                               u_int64 addr,
                                               int stride)
{
  ulong hash_key;
  int   i;
  unsigned char  word[17];
  int   mask  = 0xff;
  int   coll_lev = 0;
  u_int32 addr_lo = addr;
  u_int32 addr_hi = addr >> 32;

  while (stride < HASH_TABLE_SIZE * HASH_STRIDE) {
    coll_lev = stride / HASH_STRIDE;
    if (coll_lev > hashstat_max_coll) {
	  hashstat_max_coll = coll_lev;
    }

    for (i = 0 ; i < 16; i++) word[i] = (char) 0;
    for (i = 0 ; i < 4; i++) {
	  word[i]    = (char) (mask << i) & addr_lo;
	  word[4+i]  = (char) (mask << i) & addr_hi;
	  word[8+i]  = (char) (mask << i) & class;
	  word[12+i] = (char) (mask << i) & id;
    }

    /* Andy Meyer Modified 10/5/98.  Add a trailer word
       to indicate end of table to elfhash routine. */
    word[16] = '\0';

    hash_key = (ulong ) elfhash (word) + stride;
    hash_key %= HASH_TABLE_SIZE;

/*     printf("BZZT: addr=%x, addr_hi=%x, class=%x, id=%x, key=%x\n", addr, addr_hi, class, id, hash_key); */

    /* we have a hash key.  First check if it has data */

    /* Look to see if an entry exists */
    if (mem_data_table[hash_key] == NULL) {
	  /* this entry is empty.  We must allocate data
	     and fill the table */
	  mem_data_table[hash_key] =
        (struct mem_data_struct *) carbonmem_malloc(sizeof(struct mem_data_struct));
	  if (mem_data_table[hash_key] == NULL) {
        /* #ifdef IN_EC */
        /* 	      kprintf("SIM:  error in malloc; sparse memory full!\n"); */
        /* 	      panic("malloc"); */
        /* 	      return NULL; */
        /* #else */
        printf("Error in malloc\n");
        exit(1);
        /* #endif */
	  }

	  /* enter initial data into the structure */
	  mem_data_table[hash_key]->class   = class;
	  mem_data_table[hash_key]->id      = id;
	  mem_data_table[hash_key]->address = addr & HASH_ADDR_MASK;
	  mem_data_table[hash_key]->address_hi = addr_hi;
	  for (i = 0; i < MEM_DATA_BLOCK_SIZE; i++) {
        mem_data_table[hash_key]->data[i] = 0xdeadbeef;
	  }
	  hashstat_filled_locations++;
	  if (coll_lev < LEVEL_KEEP)
        hashstat_level_hits[coll_lev]++;
	  return (mem_data_table[hash_key]);
    }

    /* look to see if this entry is the right one */
    if ((mem_data_table[hash_key]->class   == class) &&
        (mem_data_table[hash_key]->id      == id) &&
        (mem_data_table[hash_key]->address == (addr & HASH_ADDR_MASK)) &&
        (mem_data_table[hash_key]->address_hi == addr_hi)) {

	  /* update hash statistics */
	  if (coll_lev < LEVEL_KEEP)
        hashstat_level_coll[coll_lev]++;

	  /* We have found the entry in the table, so return it */
	  return (mem_data_table[hash_key]);
    }

    /* since we are here, we have found a hash_table collision.  We
       implement linear rehashing using a prime number stride, and
       retrying the algorithm

       Since the stride must also be re-entrant, we must simple add
       to the current stride */

    /* If we have wrapped around the table size, then
       things are getting bleak, issue a warning */

    if (stride > HASH_TABLE_SIZE/5) {
	  if (print_warning_once == 0) {
        print_warning_once++;
        /* #ifdef IN_EC */
        /* 	      kprintf("SIM:  Warning; sparse memory table size is too small.\n"); */
        /* 	      print_hash_statistics(); */
        /* #else */

        printf("*********************************************************\n");
        printf(" WARNING: The Sparse Memory Model Table size is too small\n");
        printf("   Increase the table size and re-compile\n");
        print_hash_statistics();

        printf("*********************************************************\n");
        /* #endif */
	  }
    }
    stride += HASH_STRIDE;
  }

  /* If we have wrapped hash_stride times around the table size,
     then it is over - we have filled the table */

  /* #ifdef IN_EC */
  /*   kprintf("SIM:  Fatal error; sparse memory table size is too small.\n"); */
  /*   panic("sparse mem"); */
  /*   return NULL; */
  /* #else */
  printf(" ERROR: The Sparse Memory Model Table size is too small\n");
  printf("   Increase the table size and re-compile\n");
  print_hash_statistics();
  exit(1);
  return (0);  /* Just to shut up the compiler */
  /* #endif */
}

/*******************************************************

  mem_read,  mem_write
     Note that these screwy routines only work for sizes 1,2,4!!!!
     and natually aligned addresses
  *****************************************************/

u_int32  mem_read(int class, int id, u_int64 addr, int size) {
  struct mem_data_struct *data_ptr;
  u_int32 longdata, data=0;
  u_int64 longaddr;
  
  longaddr = addr >> 2;  // I store all data at word boundaries

  data_ptr = get_hash_entry(class, id, longaddr, 0);
  longdata = data_ptr->data[longaddr & HASH_ADDR_MODULO];

  switch (size) {
  case 1:
    /* we have byte addressing, so we will need to get the appropriate
       byte in to the lsb. */
    switch (addr & 0x3) {
    case 0: data = ((longdata >>  0) & 0xff);
      break;
    case 1: data = ((longdata >>  8) & 0xff);
      break;
    case 2: data = ((longdata >> 16) & 0xff);
      break;
    case 3: data = ((longdata >> 24) & 0xff);
      break;
    }
    break;
  case 2:
    if ((addr & 2) == 0) {
      data = (longdata & 0xffff);
    }
    else {
      data = ((longdata >> 16) & 0xffff);
    }
    break;
  case 4:
    data = longdata;
    break;
  }
  return (data);
}

void mem_write(int class, int id, u_int64 addr, u_int32 data, int size) {
  struct mem_data_struct *data_ptr;
  u_int64 longaddr;
  u_int32 tempdata;

  longaddr = addr >> 2;

  data_ptr = get_hash_entry(class, id, longaddr, 0);

  switch (size) {
  case 1:
    /* we have byte addressing, so we will need to get the appropriate
       byte in to the lsb. */
    switch (addr & 0x3) {
    case 0:
      tempdata = data_ptr->data[longaddr & HASH_ADDR_MODULO];
      tempdata = (tempdata & 0xffffff00) |  (data & 0x000000ff);
      data_ptr->data[longaddr & HASH_ADDR_MODULO] = tempdata;
      break;
    case 1:
      tempdata = data_ptr->data[longaddr & HASH_ADDR_MODULO];
      tempdata = (tempdata & 0xffff00ff) |  ((data << 8) & 0x0000ff00);
      data_ptr->data[longaddr & HASH_ADDR_MODULO] = tempdata;
      break;
    case 2:
      tempdata = data_ptr->data[longaddr & HASH_ADDR_MODULO];
      tempdata = (tempdata & 0xff00ffff) |  ((data << 16) & 0x00ff0000);
      data_ptr->data[longaddr & HASH_ADDR_MODULO] = tempdata;
      break;
    case 3:
      tempdata = data_ptr->data[longaddr & HASH_ADDR_MODULO];
      tempdata = (tempdata & 0x00ffffff) | ((data << 24)& 0xff000000);
      data_ptr->data[longaddr & HASH_ADDR_MODULO] = tempdata;
      break;
    }
    break;
  case 2:
    switch (addr & 2) {
    case 0:
      tempdata = data_ptr->data[longaddr & HASH_ADDR_MODULO];
      tempdata = (tempdata & 0xffff0000) |  (data & 0x0000ffff);
      data_ptr->data[longaddr & HASH_ADDR_MODULO] = tempdata;
      break;
    case 2:
      tempdata = data_ptr->data[longaddr & HASH_ADDR_MODULO];
      tempdata = (tempdata & 0x0000ffff) |  ((data << 16) & 0xffff0000);
      data_ptr->data[longaddr & HASH_ADDR_MODULO] = tempdata;
      break;
    }
    break;
  case 4:
    data_ptr->data[longaddr & HASH_ADDR_MODULO] = data;
    break;
  }
}


/*
 *
 *  These two routines are used by the slave control function
 *  The data has already been rotated to the proper byte lanes
 *
 */
/* take in a 64 bit *ALIGNED* byte address & return 64 bits of data */
u_int64  mem_read64(u_int32 mem_id, u_int64 addr) {
  struct mem_data_struct *data_ptr;
  u_int64 longaddr, longdata; 

  if (addr & 7) {
#if pfLP64
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %lx\n", addr);
#else
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %llx\n", addr);
#endif
      exit(-1);
  }

  longaddr = addr >>  2;

  data_ptr  = get_hash_entry(mem_class, mem_id, longaddr, 0);

  // Notice that this is stored LITTLE-ENDIAN!!!!
  longdata  = data_ptr->data[(longaddr+1) & HASH_ADDR_MODULO];
  longdata  = longdata << 32;
  longdata |= data_ptr->data[ longaddr    & HASH_ADDR_MODULO];
  // printf("Read  data: %016llx\n", longdata);
  return (longdata);
}


void mem_write64(u_int32 mem_id, u_int64 addr, u_int64 data)  {
  struct mem_data_struct *data_ptr;
  u_int64 longaddr;

  if (addr & 7) {
#if pfLP64
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %lx\n", addr);
#else
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %llx\n", addr);
#endif
      exit(-1);
  }

  longaddr = addr >>  2;

  data_ptr = get_hash_entry(mem_class, mem_id, longaddr, 0);

  // Notice that this is stored LITTLE-ENDIAN!!!!
  data_ptr->data[ longaddr    & HASH_ADDR_MODULO] = data;
  data_ptr->data[(longaddr+1) & HASH_ADDR_MODULO] = data >> 32;
  // printf("Wrote data: %016llx\n", data);
}



/*
 *  These routines are intended for backdoor access to the memory data
 */
void   mem_write32(u_int32 mem_id, u_int64 addr, u_int32 data ) {
 mem_write( mem_class, mem_id, addr, data, 4);
}


void   mem_write16(u_int32 mem_id, u_int64 addr, u_int16 data ) {
  u_int32 temp = data;
  mem_write( mem_class, mem_id, addr, temp, 2);
}


void   mem_write8(u_int32 mem_id, u_int64 addr, u_int8 data ) {
  u_int32 temp = data;

  mem_write( mem_class, mem_id, addr, temp, 1);
}


u_int32 mem_read32(u_int32 mem_id, u_int64 addr) {
  u_int32 data;
  data = mem_read( mem_class, mem_id, addr, 4);
  return(data);
}


u_int16 mem_read16(u_int32 mem_id, u_int64 addr) {
  u_int16 data;
  data = mem_read( mem_class, mem_id, addr, 2);
  return(data);
}


u_int8 mem_read8(u_int32 mem_id, u_int64 addr) {
  u_int8 data;
  data = mem_read( mem_class, mem_id, addr, 1);
  return(data);
}




/*
 *  I/O Accesses are limited to 32 bits, so that's all we provide
 */
void    io_write(u_int32 mem_id, u_int64 addr, u_int32 data) {
  mem_write( io_class, mem_id, addr, data, 4);
}


u_int32 io_read(u_int32 mem_id, u_int64 addr) {
  u_int32 data;
  data = mem_read( io_class, mem_id, addr, 4);
  return(data);
}




/*
 *
 *  These two routines are used by the slave control function
 *  The data has already been rotated to the proper byte lanes
 *
 */
/* take in a 64 bit *ALIGNED* byte address & return 64 bits of data */
u_int64  io_read64(u_int32 mem_id, u_int64 addr) {
  struct mem_data_struct *data_ptr;
  u_int64 longaddr, longdata; 

  if (addr & 7) {
#if pfLP64
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %lx\n", addr);
#else
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %llx\n", addr);
#endif
      exit(-1);
  }

  longaddr = addr >>  2;

  data_ptr  = get_hash_entry(io_class, mem_id, longaddr, 0);

  // Notice that this is stored LITTLE-ENDIAN!!!!
  longdata  = data_ptr->data[(longaddr+1) & HASH_ADDR_MODULO];
  longdata  = longdata << 32;
  longdata |= data_ptr->data[ longaddr    & HASH_ADDR_MODULO];
  // printf("Read  data: %016llx\n", longdata);
  return (longdata);
}


void io_write64(u_int32 mem_id, u_int64 addr, u_int64 data)  {
  struct mem_data_struct *data_ptr;
  u_int64 longaddr;

  if (addr & 7) {
#if pfLP64
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %lx\n", addr);
#else
      printf("Error calling sparse memory write 64:  Address size must be QWORD aligned: %llx\n", addr);
#endif
      exit(-1);
  }

  longaddr = addr >>  2;

  data_ptr = get_hash_entry(io_class, mem_id, longaddr, 0);

  // Notice that this is stored LITTLE-ENDIAN!!!!
  data_ptr->data[ longaddr    & HASH_ADDR_MODULO] = data;
  data_ptr->data[(longaddr+1) & HASH_ADDR_MODULO] = data >> 32;
  // printf("Wrote data: %016llx\n", data);
}

/* #ifdef IN_EC */
/* #define printf kprintf */
/* #endif */

int print_hash_statistics(void) {
  int i;
  printf ("Hash Table Statistics:\n");
  printf ("Table Size: %d  Stride: %d\n", HASH_TABLE_SIZE, HASH_STRIDE);
  printf ("Filled locations : %d\n", hashstat_filled_locations);
  for (i = 0; i < LEVEL_KEEP; i++)
    printf ("Level %d      Hits: %d  Collisions: %d\n",i+1,
            hashstat_level_hits[i],hashstat_level_coll[i]);
  printf ("Max Collision Lev: %d\n", hashstat_max_coll);
  return 0;
}

