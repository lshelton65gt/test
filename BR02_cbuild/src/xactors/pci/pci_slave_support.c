//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/* 
 ***************************************************************************** 
 * 
 * Module Name: pci_support.c 
 * This module has the efficiency routines for the PCI slave.
 * It has functions to set up a slave structure, read/write to the slave,
 * and provide response to the slave for a transaction 
 *
 ***************************************************************************** 
 */ 
 
/* 
 * include files 
 */ 
#include "tbp.h"
#include "carbonXTransaction.h"
#include "carbonXPciMaster.h"
#include "carbonXPciSlave.h"
#include "carbonXPciSparseMem.h"
#include "carbon/c_memmanager.h"
 
static void g_PCIX_Slv_Remove_split_entry(PCIX_SLV_SPLIT_TABLE * split_table_ptr, PCI_SLAVE_T * slave_ptr, u_int32 completer_id, u_int32 split_error, u_int32 split_bcm, u_int32 mem_64_access);
  
static void merge_write(u_int32 mclass, u_int32 mem_id, u_int64 addr, u_int64 data, u_int32 be) {
    u_int64 newd, oldd, wr_mask;

    wr_mask =   ((be & 0x01) ? 0x00000000000000ffLL : 0)
              | ((be & 0x02) ? 0x000000000000ff00LL : 0)
              | ((be & 0x04) ? 0x0000000000ff0000LL : 0)
              | ((be & 0x08) ? 0x00000000ff000000LL : 0)
              | ((be & 0x10) ? 0x000000ff00000000LL : 0)
              | ((be & 0x20) ? 0x0000ff0000000000LL : 0)
              | ((be & 0x40) ? 0x00ff000000000000LL : 0)
              | ((be & 0x80) ? 0xff00000000000000LL : 0);
 
    if (mclass) {
        oldd = io_read64(mem_id, addr) & ~wr_mask;
	newd = (data & wr_mask) | oldd;
	io_write64(mem_id,  addr, newd );
    } else {
        oldd = mem_read64(mem_id, addr) & ~wr_mask;
	newd = (data & wr_mask) | oldd;
	mem_write64(mem_id,  addr, newd );
    }
    //    MSG_Milestone("mem merge write: A: %08llx  D: %016llx  E:%02x\n", addr, newd, be & 0xff);
}

#if 0
static void io_merge_write(u_int32 mem_id, u_int64 addr, u_int64 data, u_int32 be) {
    u_int64 newd, oldd, wr_mask;

    wr_mask =   (be & 0x01) ? 0x00000000000000ffLL : 0
              | (be & 0x02) ? 0x000000000000ff00LL : 0
              | (be & 0x04) ? 0x0000000000ff0000LL : 0
              | (be & 0x08) ? 0x00000000ff000000LL : 0
              | (be & 0x10) ? 0x000000ff00000000LL : 0
              | (be & 0x20) ? 0x0000ff0000000000LL : 0
              | (be & 0x40) ? 0x00ff000000000000LL : 0
              | (be & 0x80) ? 0xff00000000000000LL : 0;
 
    oldd = io_read64(mem_id, addr) & ~wr_mask;
    newd = (data & wr_mask) | oldd;
    io_write64(mem_id, addr, newd );
 
}
#endif

void carbonXPciSendOperation(PCI_OP_T *pci_trans) {
  TBP_OP_T * tbp_trans = tbp_get_operation();

  tbp_trans->opcode     = TBP_NXTREQ; 
  tbp_trans->width      = TBP_WIDTH_64P;
  tbp_trans->direction  = TBP_OP_BIDIR;
  tbp_trans->address    = pci_trans->address;
  tbp_trans->read_data  = pci_trans->read_data;
  tbp_trans->write_data = pci_trans->write_data;
  tbp_trans->size       = pci_trans->size;
  tbp_trans->status     = pci_trans->status;
  tbp_trans->interrupt  = pci_trans->interrupt;
  
  carbonXSendOperation(tbp_trans);
}

PCI_OP_T * carbonXPciGetOperation (void) {
  PCI_OP_T * pci_trans;
  TBP_OP_T * tbp_trans  = tbp_get_operation();
  PCI_SLAVE_INST_DATA * myInst = (PCI_SLAVE_INST_DATA*) carbonXGetInstData();
  if (NULL == myInst) {
    myInst = (PCI_SLAVE_INST_DATA*) tbp_calloc(1, sizeof(PCI_SLAVE_INST_DATA));
    if (NULL == myInst)
      MSG_Panic("tbp_calloc failed to allocate memory\n");
    else
      carbonXSetInstData(myInst);
  }
  pci_trans = &(myInst->transaction);
  pci_trans->opcode     = tbp_trans->opcode;
  pci_trans->address    = tbp_trans->address;
  pci_trans->read_data  = tbp_trans->read_data;
  pci_trans->write_data = tbp_trans->write_data;
  pci_trans->size       = tbp_trans->size;
  pci_trans->status     = tbp_trans->status;
  pci_trans->interrupt  = tbp_trans->interrupt;
  return pci_trans;
}

/* 
 * Function to process a write transaction to a PCI slave 
 */ 
void carbonXPciSlvProcessWrite(u_int32 mem_id, PCI_OP_T *transaction) { 
    u_int32 byte_enables = transaction->status; 
// convert byte count to quad words
    int count = (transaction->size+7)/8;  
    int i, mclass = (transaction->opcode & 0xf) == PCI_IO_WRITE;
    u_int64 addr;


    //
    // There is an issue with calculating the size of a pci bus request:
    // The slave transactor counts words transferred,
    // and translates that to bytes.  But due to unset byte-enables,
    // more words may have to be processed.
    // We need to do an extra iteration through the loop if
    // the size of the transaction is > # bytes to be written
    // in the first word.
    //    if ( (byte_enables & 0xf) == 0) count++;
    addr = transaction->address & ~(7LL);
    //    MSG_Milestone("MEM WR:  A: %08llx  ST: %08x  SZ: %04x  BE:%08x\n",
    //		 addr, transaction->status, transaction->size, byte_enables);

    for(i=0; byte_enables; i++) {
        merge_write  (mclass, mem_id, addr + i*8, ((u_int64 *)transaction->data_from_sim)[i],   byte_enables);
	// the byte enables have the last 8 and first 24, shift
	// on the first 2 and the last 2
	if (i<2  ||  i>(count-3)) byte_enables = byte_enables >> 8;
    }
    transaction->size      = 0; 
} 
 


/* 
 * Function to process a read transaction from a PCI slave 
 */ 
void carbonXPciSlvProcessRead(u_int32 mem_id, PCI_OP_T *  transaction) { 
// convert to words but round up to nearest quad word
    int count = ((transaction->size+7)/8);  
    int i;
    u_int64 addr;

    addr = transaction->address & ~(7LL);

    for(i=0; i<count; i++) {
	if ((transaction->opcode & 0xf) == PCI_IO_READ) { 
	    ((u_int64 *)transaction->data_to_sim)[i] = io_read64 (mem_id, addr + i*8 );
	} else {
	    ((u_int64 *)transaction->data_to_sim)[i] = mem_read64(mem_id, addr + i*8 );
	} 
    }
    //MSG_Milestone("MEM RD:  A: %08llx  SZ: %04x  D: %016llx\n",
    //	  addr, transaction->size, ((u_int64 *)transaction->data_to_sim)[0]);
 
} 

/* 
 * Function to process a configuration write transaction to a PCI slave 
 */ 
void carbonXPciSlvProcessCfgWrite(u_int32 mem_id, PCI_OP_T *transaction) { 
    u_int32 addr;
    u_int32 *data = (u_int32 *)(transaction->data_from_sim);
    u_int32 *cfg_arr;
    PCI_SLAVE_INST_DATA * myInst = (PCI_SLAVE_INST_DATA*) carbonXGetInstData();
    (void)mem_id;
    if (NULL == myInst) {
      myInst = (PCI_SLAVE_INST_DATA*) tbp_calloc(1, sizeof(PCI_SLAVE_INST_DATA));
      if (NULL == myInst) MSG_Panic("tbp_calloc failed to allocate memory\n");
      else carbonXSetInstData(myInst);
    }
    cfg_arr = myInst->config_array;

    addr = (transaction->address>>2) & 0xf;  // Only implement 16 words of cfg space
    cfg_arr[addr] = data[0];

    //    MSG_Milestone("CFG WR:  A: %08x  D: %08x  SZ: %04x\n",
    //		 addr, cfg_arr[addr], transaction->size);

    transaction->size      = 0; 
} 
/* 
 * Function to process a config read transaction from a PCI slave 
 */ 
void carbonXPciSlvProcessCfgRead(u_int32 mem_id, PCI_OP_T *  transaction) { 
    u_int32 addr;
    u_int32 *data = (u_int32 *)(transaction->data_to_sim);
    u_int32 *cfg_arr;
    (void)mem_id;
    PCI_SLAVE_INST_DATA * myInst = (PCI_SLAVE_INST_DATA*) carbonXGetInstData();
    if (NULL == myInst) {
      myInst = (PCI_SLAVE_INST_DATA*) tbp_calloc(1, sizeof(PCI_SLAVE_INST_DATA));
      if (NULL == myInst) MSG_Panic("tbp_calloc failed to allocate memory\n");
      else carbonXSetInstData(myInst);
    }
    cfg_arr = myInst->config_array;

    addr = (transaction->address>>2) & 0xf;  // Only implement 16 words of cfg space

    data[0] = cfg_arr[addr];
    data[1] = cfg_arr[addr];

    //    MSG_Milestone("CFG RD:  A: %08x  D: %08x  SZ: %04x\n",
    //		 addr, cfg_arr[addr], transaction->size);
 
} 

/* 
 * Function to process a config read transaction from a PCI slave 
 */ 
void carbonXPciSlvRangeCheck(u_int32 mem_id, PCI_OP_T *  transaction) { 
    u_int32 inrange;
    (void)mem_id;
    // Perhaps one might see something like: inrange = addr >= cfg_arr[base]  && addr <cfg_arr[limit];
    inrange = 1;  //!!! Set this based on the address!!!

    transaction->status &= 0x7fffffff;
    transaction->status |= inrange <<31; 	// set MSB of status if in range
    
} 

/*
 *   This is basically the "default" slave function
 *
 * The status returned for each transaction is given by the
 * status parameter.  The user can use this in three different
 * ways:
 *  1) set size to 0 or 1, and set the u_int32 pointed to by status
 *     to a single fixed value.
 *  2) Set size to 0 or 1, and change the value pointed to by status
 *     as required by test needs.
 *  3) Set size to the number of values in an array, and set status
 *     to point to that array.  This function will then walk through
 *     the array for each transaction, wrapping around after size
 *     transactions.
 */




void carbonXPciSlaveFunction(u_int32 mem_id, u_int32 *status, u_int32 size /*, ??? qname */) {
    PCI_OP_T   *transaction;
    int op, index, range;
    // Define the response
    /*******************************************
	 addr_in_range   = XIF_get_csdata[31   ] | XIF_get_status[31];   
         use_32bit       = XIF_get_csdata[30   ] | XIF_get_status[30],
         cause_perr      = XIF_get_csdata[29   ] | XIF_get_status[29],
         cside_wait      = XIF_get_csdata[28   ] | XIF_get_status[28],
         DevselCount     = XIF_get_csdata[23:20] | XIF_get_status[23:20],
	 resp_type       = XIF_get_csdata[19:16] | XIF_get_status[19:16],
	 num_data_ph     = XIF_get_csdata[15: 8] | XIF_get_status[15:8], 
	 num_wait_cycles = XIF_get_csdata[ 7: 0] | XIF_get_status[7:0],  

      Responses for PCI_Slv_resp_req could be:  RETRY, TABRT, NORMAL, DISCNT
    *******************************************/
    if (size==0)size=1; // prevent divide by zero errors
    index = 0;

    carbonXCsWrite(0, 0x0 | 1<<28);  // clear status reg to allow us to override it

    while(TRUE) {
        transaction = carbonXPciGetOperation(); // Wait for a request

	/*
	MSG_Milestone("Operation: %08x  SZ: %08x  ST: %08x\n",
		      transaction->opcode, transaction->size, transaction->status);
	*/

	/*
	  need code to check command queue and process the command
	 */
	op = transaction->opcode & 0x1f;
	/****************************************
	MSG_Milestone("Slave:      A:%08x  OP:%08x SZ:%3d  ST:%08x  D:%08x %08x\n",
	transaction->addr_lo, transaction->operation, transaction->byte_count, transaction->ret_status,
	transaction->ret_data[0], transaction->ret_data[1]);
	****************************************/

	// Notice that the below processing routines can alter the status in the transaction
	//  This is of particular interest for range check

	switch(op) {
	case PCI_MEM_WRITE_INV_CMP  :
	case PCIX_MEM_WRITE_BLK_CMP :
	case PCI_MEM_WRITE_CMP      :
	case PCI_IO_WRITE_CMP       :
	  carbonXPciSlvProcessWrite (mem_id, transaction); break;

	case PCIX_MEM_READ_BLK_A :
	case PCI_MEM_READ_LINE   :
	case PCI_MEM_READ        :
	case PCI_IO_READ         :
	  carbonXPciSlvProcessRead  (mem_id, transaction); break;

	case PCIX_SPLIT_COMP  : 
	  break;

	  // Handle configuration operations separately
	case PCI_CFG_READ     :
	  carbonXPciSlvProcessCfgRead  (mem_id, transaction); break;
	case PCI_CFG_WRITE_CMP:
	  carbonXPciSlvProcessCfgWrite (mem_id, transaction); break;

	  // This batch simply checks the address range
	case PCI_IO_WRITE     :
	case PCI_MEM_WRITE_INV:
	case PCI_MEM_WRITE    :
	case PCIX_MEM_WRITE_BLK_A:
	  carbonXPciSlvRangeCheck (mem_id, transaction); break;

	  // None of these codes have much to do....
	case PCI_CFG_WRITE    :// cfg accesses check range in V code
	case PCI_INTR_ACK     :
	case PCI_SPECIAL_CYCLE:
	case PCI_RESERVED1    :
	case PCI_RESERVED2    :
	case PCI_DAC          :
	  //MSG_Milestone("Slave received PCI cmd: %02x (Ignored)\n", op);
	  range = 0;
	  break;

	}
	// Note that this overwrites any status from the above routines!!
	transaction->status    = status[index];
	transaction->opcode    = TBP_NXTREQ; 

	//MSG_Milestone("Slave resp: A:%08llx  OP:%08x SZ:%3d  ST:%08x\n",
	//transaction->address, transaction->opcode, transaction->size, transaction->status);

        carbonXPciSendOperation(transaction);
	index = (index+1) % size;
    }

}




void carbonXPciSlvDisplayError(PCI_OP_T *transaction)
{
  u_int32 parity_status;

  parity_status = transaction->status;

  if (parity_status & 0x00000001) MSG_Milestone("@%lld: ###### Write data parity error detected ######\n", tbp_get_sim_time());

  if (parity_status & 0x00000002) MSG_Milestone("@%lld: ###### Address parity error detected ######\n", tbp_get_sim_time());

  transaction->status = 0;
  transaction->size   = 0;
  transaction->opcode = TBP_NXTREQ;
}


////////////////////////////////////////////////////////////////////////////
// This function is called from the slave transctor's initiator to send
// the read data/split completion message back to the requestor
///////////////////////////////////////////////////////////////////////////   

void carbonXPcixSplitComplete(u_int64 address, u_int32 req_id, u_int32 req_tag, u_int32 * data, u_int32 length, u_int32 * return_status, u_int32 req64)
{
  u_int32 * arr;
  u_int32 temp, temp1, num_phases;
  u_int32 status = 0;
  u_int32 depth;
  u_int32 size;
  u_int32 byte_cnt_altered;
  u_int32 opcode = (1<<USER_OP__VALID);
  u_int64 addr = address;

  temp = length + (address & 0x0000000000000003);
  temp1 = (temp & 0x00000001) | (temp & 0x00000002);
  num_phases = ((temp & 0xfffffffc) >> 2) + temp1;

  if (num_phases > ((length+3)/4) ) {
    size = length + 4;
    byte_cnt_altered = 1;
  }
  else {
    size = length;
    byte_cnt_altered = 0;
  }

  depth = num_phases + 3;

  opcode |= (byte_cnt_altered<<USER_OP__BCM);
  opcode |= ((req64&1)<<USER_OP__64BIT);
  opcode |= (PCIX_SPLIT_COMP<<USER_OP__PCI_CMD);

  opcode |= ((req_id&USER_OP__REQ_ID_MASK)<<USER_OP__REQ_ID) | ((req_tag&USER_OP__REQ_TAG_SC_MASK)<<USER_OP__REQ_TAG);

  arr = (u_int32 *)tbp_malloc(depth*sizeof(u_int32));

  memcpy(arr, data, num_phases*sizeof(u_int32));

  tbp_transaction(tbp_get_my_trans_id(), &opcode, &addr, arr, NULL, &size, &status, 
		  TBP_OP_WRITE, TBP_WIDTH_32);

  *return_status = status;
}


/////////////////////////////////////////////////////////////////////////////
// This function is to be called from the main test function.
// This function allocates memory for the split table.
//
// The split table is organized as follows :
// * It has a 32 bit valid_bits register. Each bit corresponds to one entry.
//   A bit when set, indicates that the corresponding entry is
//   an outstanding split transaction.
//
// * There are 32 entries for storing upto 32 outstanding split transactions.
//   Each entry has the following details about the transaction :
//   Command       - bit [0] - write/read ...write - 0, read - 1
//                   bit [1] - byte count modified
//   Num_Bytes     - total byte count
//   Start_address - Starting address of the transaction
//   Req_attribute - Requester attribute 
/////////////////////////////////////////////////////////////////////////////

PCIX_SLV_SPLIT_TABLE * carbonXPcixSlaveCreateSplitTable()
{
  PCIX_SLV_SPLIT_TABLE * table_ptr;
  u_int32 i;

  if ((table_ptr = (PCIX_SLV_SPLIT_TABLE *)tbp_malloc(sizeof(PCIX_SLV_SPLIT_TABLE))) == NULL) {
    MSG_Error("Failed to allocate memory for Split Table for Initiator\n");
  }
  else {
    // Initialize the split table
    table_ptr->valid_bits = 0x00000000;
    for (i=0; i < 32; i++) {
      table_ptr->split_entry[i].Command = 0;
      table_ptr->split_entry[i].Num_bytes = 0;
      table_ptr->split_entry[i].Req_attribute = 0;
      table_ptr->split_entry[i].Start_address = 0;
      table_ptr->split_entry[i].bcm = 0;
    }
    MSG_Milestone("@%lld: in create table function %0x\n", tbp_get_sim_time(), table_ptr);
  }

  return(table_ptr);
}

void carbonXPcixSlaveDestroySplitTable(PCIX_SLV_SPLIT_TABLE * split_table_ptr)
{
  void* ptr = split_table_ptr;
  tbp_free (ptr);
}


/////////////////////////////////////////////////////////////////////////////
// This function is called from a slave's attached function.
// This function adds the attribute, address and other information
// about the transaction it ended with Split Response, to a split
// table. 
/////////////////////////////////////////////////////////////////////////////

void carbonXPcixSlvAddSplitEntry(u_int32 Command, u_int32 Num_bytes, u_int32 Req_attribute, u_int64 Start_address, PCIX_SLV_SPLIT_TABLE * split_table_ptr)
{
  u_int32 valid_bits;
  u_int32 free_location;
  u_int32 valid_bit_location;
  u_int32 split_table_full;

  valid_bits = split_table_ptr->valid_bits;
  free_location = 0x00000000;
  valid_bit_location = 0x00000001;
  split_table_full = 0;


  //-------------------------------------------------//
  //----- Finding a free location in the table  -----//
  //-------------------------------------------------//

  while (((valid_bits & valid_bit_location) != 0x00000000) &&
         (valid_bit_location != 0x00000000)) {
    valid_bit_location = valid_bit_location << 1;
    free_location++;
  }

  if (valid_bit_location == 0x00000000)
    split_table_full = 1;


  //-------------------------------------//
  //----- No free location in table -----//
  //-------------------------------------//
  if (split_table_full) {
    MSG_Error("@%lld: Initiator can have maximum 32 outstanding split transactions\n", tbp_get_sim_time());
  }
  //-------------------------------------------//
  //----- Found a free location in table -----//
  //----- Add the split response details -----//
  //-------------------------------------------//
  else {
    if ((Command == 0) && (Num_bytes > 4)) {
      MSG_Error("@%lld: #### Cannot terminate burst write with split response\n", tbp_get_sim_time());
    }
    else {
      split_table_ptr->split_entry[free_location].Command = Command;
      split_table_ptr->split_entry[free_location].Num_bytes = Num_bytes;
      split_table_ptr->split_entry[free_location].Req_attribute = (Req_attribute & 0x001fffff);
      split_table_ptr->split_entry[free_location].Start_address = Start_address;
      split_table_ptr->split_entry[free_location].bcm = 0;
    }
  }

  //------------------------------------------------------//
  //---- Set the valid bit corresponding to that entry ---//
  //------------------------------------------------------//
  split_table_ptr->valid_bits = valid_bits | valid_bit_location;
}


/////////////////////////////////////////////////////////////////////////////
// This function is called from the slave's attached function.
// This function reads the split table, and initiates a split
// completion. If the split completion terminated normally, that
// entry is deleted from teh split table. Else the entry is 
// modified with the number of bytes transferred. And the split
// completion is again initiated at a later time.
/////////////////////////////////////////////////////////////////////////////

void carbonXPcixSlvRemoveSplitEntry(PCIX_SLV_SPLIT_TABLE * split_table_ptr,
                                 PCI_SLAVE_T * slave_ptr,
                                 u_int32 completor_id,
                                 u_int32 split_error,
                                 u_int32 split_bcm)
{
  g_PCIX_Slv_Remove_split_entry( split_table_ptr, slave_ptr, completor_id, split_error, split_bcm, 0 );
}

void carbonXPcix64SlvRemoveSplitEntry(PCIX_SLV_SPLIT_TABLE * split_table_ptr,
                                   PCI_SLAVE_T * slave_ptr,
                                   u_int32 completor_id,
                                   u_int32 split_error,
                                   u_int32 split_bcm)
{
  g_PCIX_Slv_Remove_split_entry( split_table_ptr, slave_ptr, completor_id, split_error, split_bcm, 1 );
}

static void g_PCIX_Slv_Remove_split_entry(PCIX_SLV_SPLIT_TABLE * split_table_ptr,
                                   PCI_SLAVE_T * slave_ptr,
                                   u_int32 completor_id,
                                   u_int32 split_error,
                                   u_int32 split_bcm,
                                   u_int32 mem_64_access)
{
  u_int32 size, command, req_attr, status, k;
  u_int64 address, address_hi, mem_addr, mem_addr_hi, mem_addr_lo;
  u_int32 split_address, scm;
  u_int32 * data;
  u_int32 temp=0, temp1;
  u_int32 split_completion_message;
  u_int32 location, valid_bit_location;
  u_int32 completer_tag;
  u_int32 loop_max;
  u_int32 bcm_applied, disconnect_adb, error_completion=0, adb_completion=0;

  (void)slave_ptr;
  //   while (1) {

  if(split_table_ptr->valid_bits) {

    carbonXIdle(20);

    location = 0;
    valid_bit_location = 0x00000001;

    //----------------------------------------------------//
    //----- Find a valid entry in the table  -------------//
    //----------------------------------------------------//

    while(((split_table_ptr->valid_bits & valid_bit_location) == 0x00000000) && (valid_bit_location != 0x00000000)) {
      location++;
      valid_bit_location = valid_bit_location << 1;
    }


    //----------------------------------------------------//
    //---- Get the split completion attributes from ------//
    //---- the split table                      ----------//
    //----------------------------------------------------//
    command = (split_table_ptr->split_entry[location].Command & 0x00000001);
    size  = split_table_ptr->split_entry[location].Num_bytes;
    req_attr = split_table_ptr->split_entry[location].Req_attribute;
    address = split_table_ptr->split_entry[location].Start_address;
    address_hi = split_table_ptr->split_entry[location].Start_address;
    address_hi >>= 32;
    bcm_applied = split_table_ptr->split_entry[location].bcm;


    //------ If command is read, and split completion ----//
    //------ is to be disconnected at ADB, only if -------//
    //------ burst crosses ADB, disconnect is signalled --// 

    if (split_bcm && command)  {
      temp = 0x00000080 - (address & 0x0000007f);
      if (size > temp)  // Burst crosses ADB
        disconnect_adb = 1;
      else              // Does not cross ADB
        disconnect_adb = 0;
    }
    else {
      disconnect_adb = 0;
    }
           

    //----------------------------------------------------//
    //------- Cases for {disconnect_ADB, split_error} ----//
    //  00 : Normal completion
    //  01 : Error completion
    //  10 : Disconnect at ADB, & set bcm bit in attribute
    //  11 : If this is not the first completion of the
    //       sequence, then send Error Completion.
    //       Else if this is the first split completion of
    //       the sequence, then disconnect at ADB, and 
    //       set the bcm bit in the attribute
    //----------------------------------------------------//
    temp1 = (disconnect_adb << 1) | split_error;

    switch(temp1) {
    case 0x00000000 : 
      MSG_Milestone("Normal completion %0x\n", req_attr);
      adb_completion = 0;
      error_completion = 0; break;
    case 0x00000001 : 
      MSG_Milestone("Split Error completion %0x\n", req_attr);
      adb_completion = 0;
      error_completion = 1; break;
    case 0x00000002 : 
      MSG_Milestone("Disconnect at ADB %0x\n", req_attr);
      adb_completion = 1;
      size = temp; 
      error_completion = 0; break;

    case 0x00000003 : 
      if (bcm_applied) {
        MSG_Milestone("Split Error completion after prev ADB %0x\n", req_attr);
        adb_completion = 0;
        error_completion = 1;
      }
      else {
        MSG_Milestone("Disconnect at ADB (split error set) %0x\n", req_attr);
        adb_completion = 1;
        size = temp;
        error_completion = 0; 
      }
      break;
    }

        

    //----------------------------------------------------//
    //------ Generation the split completion Message -----//
    //----------------------------------------------------//

    if (error_completion) {
      if (command == 0) {
        split_completion_message = 0x20800000 | 0x00000004;
      }
      else {
        split_completion_message = 0x20800000 | ((address & 0x0000007f) << 12) | size ;
      }
    }
    else {
      if (command == 0) {
        split_completion_message = 0x00000004;
      }
      else {
        split_completion_message = 0x00000000;
      }
    }
           

    //----------------------------------------------------//
    //--------------- Filling the data array -------------//
    //------- For write - split completion message -------//
    //------- For read  - read data                -------//
    //----------------------------------------------------//

    mem_addr = address_hi;
    mem_addr <<= 32;
    mem_addr |= address;
    if (address & 0x00000003)
      loop_max = (size+3)/4 + 1;
    else
      loop_max = (size+3)/4;

    data = (u_int32 *)(tbp_malloc(loop_max*(sizeof(u_int32))));

    if ((command == 0) || error_completion) {
      *data = split_completion_message;
      scm = 0x00000001;
    }
    else {
      scm = 0x00000000;
      for (k=0; k < loop_max; k++) {
        mem_addr_hi = mem_addr >> 32;
        mem_addr_lo = mem_addr & 0xffffffff;
//!!!        *(data+k) = pci_mem_rd32( mem_addr_hi, mem_addr_lo );
        mem_addr += 4;
        //MSG_Milestone("data [%0x] = %0x address %0x\n", k, *(data+k), (mem_addr+k));
      }
    }

    //----- Setting the address for the split completion -----//
    split_address = ((req_attr & 0x001fffff) << 8) | (address & 0x0000007f);

    //---- Encoding the bcm, error, and scm bits -------//
    completer_tag =  (adb_completion << 2) | (error_completion << 1) | scm;
       

    //----------------------------------------------------//
    //--------- Initiate the split completion ------------//
    //----------------------------------------------------//
    if (scm == 0x00000001) {
      carbonXPcixSplitComplete(split_address, completor_id, completer_tag, data, 4, &status, mem_64_access);
    }
    else {
      carbonXPcixSplitComplete(split_address, completor_id, completer_tag, data, size, &status, mem_64_access);
    }

    // freeing the read data area
    tbp_free(data);

    //----------------------------------------------------//
    //----- If normal termination, if it was an error ----//
    //----- completion or all bytes were sent, CLEAR the -//
    //----- corresponding valid bit.        --------------//
    //----- If it was a disconnect at ADB, then update ---//
    //----- the fields in the split table..and DO NOT ----//
    //----- clear the entry.                --------------//
    //----------------------------------------------------//

    if ((status & 0x0000ffff) == 0x00000000) {
      if (adb_completion) {
        // Updating with number of bytes to be sent
        temp = split_table_ptr->split_entry[location].Num_bytes;
        split_table_ptr->split_entry[location].Num_bytes = temp - size;

        // Updating the address
        split_table_ptr->split_entry[location].Start_address += size;

        // Setting the bcm bit
        split_table_ptr->split_entry[location].bcm = 0x00000001;
      }
      else {
        // Clear the entry
        temp = split_table_ptr->valid_bits;
        temp1 = temp & ~valid_bit_location;
        split_table_ptr->valid_bits = temp1;
        //MSG_Milestone("Cleared the split entry for req_attribute %0x\n", req_attr);
        MSG_Milestone("Valid bits in slave table %0x\n", split_table_ptr->valid_bits);
      }
    }
    //---------------------------------------------------//
    //---- If the status is not normal termination ------//
    //---- display an error                       -------//
    //---------------------------------------------------//
    else {
      MSG_Milestone("@%lld: Termination status %0x\n", tbp_get_sim_time(), (status & 0x0000ffff));
      MSG_Error("@%lld: #### Requestor did not terminate split completion normally for attr %0x\n", tbp_get_sim_time(), req_attr);
    }
           
  } // While valid_bits != 0

  //  } // While(1)

}


void carbonXPciSlvSparseMemWrite(sparseMem_t mem, carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *request, struct carbonXTransactionStruct *response) {
  (void)cfg;
  u_int32 be = request->mStatus; 

  // convert byte count to quad words
  int     count        = ((request->mSize+7)/8) * 2;  
  int     i;
  u_int64 addr;
  u_int32 wr_data, wr_mask;
  u_int64 wr_data64 = 0;

  //
  // There is an issue with calculating the size of a pci bus request:
  // The slave transactor counts words transferred,
  // and translates that to bytes.  But due to unset byte-enables,
  // more words may have to be processed.
  // We need to do an extra iteration through the loop if
  // the size of the transaction is > # bytes to be written
  // in the first word.
  //    if ( (byte_enables & 0xf) == 0) count++;
  addr = request->mAddress & ~(3LL);

  MSG_Milestone("MEM WR:  A: %08llx  ST: %08x  SZ: %04x  BE:%08x D: %016llx\n",
  	addr, request->mStatus, request->mSize, be, ((u_int64 *)request->mData)[0]);
  
  for(i=0; be; i++) {
    wr_mask =   
      ((be & 0x01) ? 0x000000ff : 0) | 
      ((be & 0x02) ? 0x0000ff00 : 0) | 
      ((be & 0x04) ? 0x00ff0000 : 0) | 
      ((be & 0x08) ? 0xff000000 : 0);
    
    wr_data64 = ((u_int64*)request->mData)[i/2];
    wr_data = (carbonXSparseMemReadWord32(mem, addr+(i<<2)) & (~wr_mask)) | ((wr_data64 >> ((i&1)*32)) & wr_mask);
    MSG_Milestone("Writing %4x to Address %4llx. wr_mask: %8x wr_data64 = %llx\n", wr_data, addr+(i<<2), wr_mask, wr_data64);
    carbonXSparseMemWriteWord32(mem, addr+(i<<2), wr_data);

    // the byte enables have the last 8 and first 24, shift
    // on the first 2 and the last 2
    if (i<4  ||  i>(count-5)) be = be >> 4;
  }
  
  response->mOpcode    = TBP_NXTREQ;
  response->mAddress   = request->mAddress;
  response->mSize      = 0;
  response->mStatus    = 0;
  response->mWidth     = TBP_WIDTH_64P;
  response->mInterrupt = 0;
  response->mHasData   = 0;
}

static u_int64 sparse_read64(sparseMem_t mem, u_int64 addr);

void carbonXPciSlvSparseMemRead(sparseMem_t mem, carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *request, struct carbonXTransactionStruct *response) {

  // convert to words but round up to nearest quad word
  int     count = ((request->mSize+7)/8);  
  int     i;
  u_int64 addr;
  (void) cfg;
  addr = request->mAddress & ~7LL;
  
  response->mTransId++;
  response->mRepeatCnt = 1;
  response->mOpcode    = TBP_NXTREQ;
  response->mAddress   = request->mAddress;
  response->mSize      = request->mSize;
  response->mStatus    = 0;
  response->mWidth     = TBP_WIDTH_64P;
  response->mInterrupt = 0;
  response->mHasData   = (count > 0);
  // Make Sure that the transaction buffer is large enough for the return data
  carbonXReallocateTransactionBuffer(response);

  for(i=0; i<count; i++) {
    ((u_int64 *)response->mData)[i] = sparse_read64(mem, addr + (i<<3));
    MSG_Milestone("Read %8llx from Address %4x\n", ((u_int64 *)response->mData)[i], addr+(i<<3));
  }

  MSG_Milestone("MEM RD:  A: %08llx  SZ: %04x  D: %016x\n",
  	addr, response->mSize, ((u_int64 *)response->mData)[0]);
}

static u_int64 sparse_read64(sparseMem_t mem, u_int64 addr) {

  u_int64 data_l, data_h;

  data_l = carbonXSparseMemReadWord32(mem, addr);
  data_h = carbonXSparseMemReadWord32(mem, addr+4);

  return (data_h<<32) | data_l;
} 

void carbonXPciSlaveCfgWrite(carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *req, struct carbonXTransactionStruct *resp) {
  
  UInt64 data64;
  UInt32 addr;
  int    i;
  UInt64 write_mask = 0;

  // Address should be even quad words, and make sure 
  // that we don't overflow memory
  addr   = ((req->mAddress>>2)%cfg->mSize) & 0xfffffffe;

  // Create the Write Mask
  for(i = 0; i < 8; i++) {
    if((req->mStatus >> i) &1) write_mask |= (0xffLL << (i*8));
  }

  // The Write Mask depends both on the user write mask and the byte enable from the hardware
  // Hardware is sending data on even quad words, so for odd dword addresses only the upper
  // 32 bits of data are enabled
  write_mask &= ((UInt64)cfg->mWe[addr+1]) << 32 | cfg->mWe[addr];

  // Get the old data from the memory, odd address in upper part of word
  data64 = ((UInt64)cfg->mMem[addr+1]) << 32 | cfg->mMem[addr];

  //MSG_Milestone("Wrote %llx to Config Register %d BE: 0x%08x WM: 0x%08llx SIZE: %d\n", *(UInt64 *)req->mData, addr, req->mStatus, write_mask, cfg->mSize);
  
  data64 = (data64 & ~write_mask) | ((*(UInt64 *)req->mData) & write_mask);

  //MSG_Milestone("Resulting data = %08llx\n", data64);

  cfg->mMem[addr]   = data64;
  cfg->mMem[addr+1] = (data64 >> 32);

  resp->mOpcode      = TBP_NXTREQ;
  resp->mAddress     = req->mAddress;
  resp->mWidth       = TBP_WIDTH_64P;
  resp->mSize        = 8;
  resp->mHasData     = 1;

  // The resulting memory data is sent back down to the HDL side
  // This is needed so that the HDL side transactor can have a shadow
  // register of the base address registers, and that it can figure out
  // it's memory sizes during the initilization of the PCI bus
  carbonXReallocateTransactionBuffer(resp);
  *((UInt64 *)resp->mData) = data64;
  
}
  
void carbonXPciSlaveCfgRead(const carbonXPciConfigRegsT cfg, struct carbonXTransactionStruct *req, struct carbonXTransactionStruct *resp) {  
  UInt32 addr;

  // Address should be even quad words, and make sure 
  // that we don't overflow memory
  addr   = ((req->mAddress>>2)%cfg->mSize) & 0xfffffffe;
  
  resp->mOpcode = TBP_NXTREQ;
  resp->mWidth  = TBP_WIDTH_64P;
  resp->mSize   = 8;
  resp->mHasData     = 1;
  carbonXReallocateTransactionBuffer(resp);
  *((UInt64 *)resp->mData) = (UInt64)cfg->mMem[addr+1] << 32 | cfg->mMem[addr];

  //MSG_Milestone("Read %llx from Config Register %d\n", *((UInt64 *)resp->mData), addr);
  
}

void carbonXPciInitConfigRegs(carbonXPciConfigRegsT cfg, UInt32 size) {
  cfg->mSize = size;
  cfg->mMem  = carbonmem_malloc(sizeof(UInt32) * size);
  cfg->mWe   = carbonmem_malloc(sizeof(UInt32) * size);
  cfg->mCe   = carbonmem_malloc(sizeof(UInt32) * size);

  memset(cfg->mMem, 0, sizeof(UInt32) * size);
  memset(cfg->mWe,  0, sizeof(UInt32) * size);
  memset(cfg->mCe,  0, sizeof(UInt32) * size);
}

carbonXPciConfigRegsT carbonXCreateConfigRegs(UInt32 size) {  
  carbonXPciConfigRegsT cfg = carbonmem_malloc(sizeof(struct carbonXPciConfigRegsS));
  carbonXPciInitConfigRegs(cfg, size);
  return cfg;
}

void carbonXDestroyConfigRegs(carbonXPciConfigRegsT cfg) {
  carbonmem_free(cfg->mMem);
  carbonmem_free(cfg->mWe);
  carbonmem_free(cfg->mCe);
  if(cfg != NULL) carbonmem_free(cfg);
}
