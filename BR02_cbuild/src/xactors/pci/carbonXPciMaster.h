//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef PCI_MASTER_SUPPORT__H
#define PCI_MASTER_SUPPORT__H

#include <xactors/pci/carbonXPciSlave.h>

#ifdef __cplusplus
extern "C" {
#endif
 
//  Fixed sized memory accesses
void    carbonXPciMemWrite32       (u_int64 address, 
				    u_int32 data );
void    carbonXPciMemWrite16       (u_int64 address, 
				    u_int16 data );
void    carbonXPciMemWrite8        (u_int64 address, 
				    u_int8  data );

u_int32 carbonXPciMemRead32        (u_int64 address );
u_int16 carbonXPciMemRead16        (u_int64 address );
u_int8  carbonXPciMemRead8         (u_int64 address );

// Variable size 32 bit memory accesses
u_int32 carbonXPciMemWrite         (u_int64 address, 
				    u_int32 *data, 
				    u_int32 length); // length is in bytes
u_int32 carbonXPciMemRead          (u_int64 address, 
				    u_int32 *data, 
				    u_int32 length); // length is in bytes

// Variable sized 64 bit bus memory accesses
//    These request 64-bit data width accesses but take 32-bit arrays of data
#define REQ_32BIT 0
#define REQ_64BIT 1
u_int32 carbonXPci64MemWrite       (u_int64 address, 
				    u_int32 *data, 
				    u_int32 length); // length is in bytes
u_int32 carbonXPci64MemRead        (u_int64 address, 
				    u_int32 *data, 
				    u_int32 length); // length is in bytes

// Configuration space accesses (always 32 bit)
void    carbonXPciCfgWrite32       (u_int64 address, 
				    u_int32 data);
u_int32 carbonXPciCfgRead32        (u_int64 address);

// IO space accesses (always 32 bit)
void    carbonXPciIOWrite          (u_int64 address, 
				    u_int32 data);
u_int32 carbonXPciIORead           (u_int64 address);

// Interrupt acknowledge
u_int8  carbonXPciIntAck();

/*
 *  This similar set of routines also returns the status of the PCI operation
 *  These are useful for when an unusual response is expected
 *  carbonXPciCheckStatus may be called to report an error on status
 *  other than normal.
 */

void    carbonXPciCheckStatus(u_int32 status, 
			      u_int32 address, 
			      u_int32 write);

void    carbonXPciMemWrite32_Status(u_int64 address, 
				    u_int32  data,
				    u_int32 *return_status);
void    carbonXPciMemWrite16_Status(u_int64 address, 
				    u_int16  data,              
				    u_int32 *return_status);
void    carbonXPciMemWrite8_Status (u_int64 address, 
				    u_int8   data,              
				    u_int32 *return_status);

u_int32 carbonXPciMemRead32_Status (u_int64 address,                          
				    u_int32 *return_status);
u_int16 carbonXPciMemRead16_Status (u_int64 address,
				    u_int32 *return_status);
u_int8  carbonXPciMemRead8_Status  (u_int64 address,  
				    u_int32 *return_status);

void    carbonXPciMemWrite_Status  (u_int64 address, 
				    u_int32 *data, 
				    u_int32 length,             // length is in bytes
				    u_int32 *return_status);
void    carbonXPciMemRead_Status   (u_int64 address, 
				    u_int32 *data, 
				    u_int32 length,             // length is in bytes
				    u_int32 *return_status);

void    carbonXPci64MemWrite_Status(u_int64 address, 
				    u_int32 *data, 
				    u_int32 length,             // length is in bytes
				    u_int32 *return_status);
void    carbonXPci64MemRead_Status (u_int64 address, 
				    u_int32 *data, 
				    u_int32 length,             // length is in bytes
				    u_int32 *return_status);

void    carbonXPciCfgWrite32_Status(u_int64 address, 
				    u_int32 data,
				    u_int32 *return_status);
u_int32 carbonXPciCfgRead32_Status (u_int64 address,
				    u_int32 *return_status);

void    carbonXPciIOWrite_Status   (u_int64 address, 
				    u_int32 data, 
				    u_int32 *return_status);
u_int32 carbonXPciIORead_Status    (u_int64 address,  
				    u_int32 *return_status);

/*
** PCI/PCIX Initiator return values
*/
#define STATUS__NORMAL                    0x0000
#define STATUS__MASTER_ABORT              0x0001
#define STATUS__TARGET_RETRY              0x0002
#define STATUS__TARGET_ABORT              0x0004
#define STATUS__TARGET_SPLIT_RESPONSE     0x0008 // PCIX only
#define STATUS__DATA_DISCONNECT_A         0x0008 // PCI only
#define STATUS__SINGLE_DATA_DISCONNECT    0x0010 // PCIX only
#define STATUS__DATA_DISCONNECT_B         0x0010 // PCI only
#define STATUS__TARGET_DISCONNECT_ADB     0x0020 // PCIX only
#define STATUS__DATA_DISCONNECT_C         0x0020 // PCI only
#define STATUS__ILLEGAL_TARGET_RETRY      0x0040 // PCIX only
#define STATUS__ILLEGAL_TARGET_WAIT       0x0080 // PCIX only
#define STATUS__ILLEGAL_TARGET_SDD        0x0100 // PCIX only
#define STATUS__ILLEGAL_TARGET_SPLIT_RESP 0x0200 // PCIX only
#define STATUS__ILLEGAL_TARGET_RESPONSE   0x0400 // PCIX only
#define STATUS__ILLEGAL_BURST_AFTER_ADB   0x0800 // PCIX only
#define STATUS__MASTER_LATENCY_EXP        0x1000 // PCIX only
#define STATUS__DATA_PARITY_ERROR         0x10000 
#define STATUS__ADDR_PARITY_ERROR         0x20000 
#define STATUS__TARGET_LATENCY_EXP        0x40000 // PCIX only

/* 
 * Issue an arbitrary PCI command
 */
u_int32 carbonXPciCmd(u_int32 command, 
		      u_int64 address, 
		      u_int32 req64data, 
		      u_int32 * data, 
		      u_int32 length);  // length is in bytes
/*
** PCI command types
*/
#define PCI_INTR_ACK           0x0
#define PCI_SPECIAL_CYCLE      0x1
#define PCI_IO_READ            0x2
#define PCI_IO_WRITE           0x3
#define PCI_RESERVED1          0x4
#define PCI_RESERVED2          0x5
#define PCI_MEM_READ           0x6
#define PCI_MEM_WRITE          0x7
#define PCI_RESERVED3          0x8
#define PCI_RESERVED4          0x9
#define PCI_CFG_READ           0xa
#define PCI_CFG_WRITE          0xb
#define PCI_MEM_READ_MULT      0xc
#define PCI_DAC                0xd
#define PCI_MEM_READ_LINE      0xe
#define PCI_MEM_WRITE_INV      0xf







/* 
 * PCI-X Specific Routines
 * 
 *   All the standard read/write functions return the status.  Defines
 *   for these are above.
 */

u_int32 carbonXPcixMemWrite(u_int64 address, 
			    u_int32 req_id, 
			    u_int32 req_tag, 
			    u_int32 * data, 
			    u_int32 length);  // length is in bytes
u_int32 carbonXPcixMemRead(u_int64 address,  
			   u_int32 req_id, 
			   u_int32 req_tag, 
			   u_int32 *data, 
			   u_int32 length);  // length is in bytes
u_int32 carbonXPcixCfgWrite(u_int64 address, 
			    u_int32 req_id, 
			    u_int32 req_tag, 
			    u_int32 data);
u_int32 carbonXPcixCfgRead(u_int64 address, 
			   u_int32 req_id, 
			   u_int32 req_tag, 
			   u_int32 *data);
u_int32 carbonXPcixIOWrite(u_int64 address, 
			   u_int32 req_id, 
			   u_int32 req_tag, 
			   u_int32 data);
u_int32 carbonXPcixIORead(u_int64 address, 
			  u_int32 req_id, 
			  u_int32 req_tag, 
			  u_int32 *data);
u_int32 carbonXPcix64MemWrite(u_int64 address, 
			      u_int32 req_id, 
			      u_int32 req_tag, 
			      u_int32 * data, 
			      u_int32 length); // length is in bytes
u_int32 carbonXPcix64MemRead(u_int64 address,  
			     u_int32 req_id, 
			     u_int32 req_tag, 
			     u_int32 *data, 
			     u_int32 length); // length is in bytes

  // The following calls place the status in an output parameter.
void    carbonXPcixMemWrite_Status(u_int64 address, 
				   u_int32 req_id, 
				   u_int32 req_tag, 
				   u_int32 * data, 
				   u_int32 length, // length is in bytes
				   u_int32 * return_status);
void    carbonXPcixMemRead_Status(u_int64 address,  
				  u_int32 req_id, 
				  u_int32 req_tag, 
				  u_int32 *data, 
				  u_int32 length, // length is in bytes
				  u_int32 * return_status);
void    carbonXPcixCfgWrite_Status(u_int64 address, 
				   u_int32 req_id, 
				   u_int32 req_tag, 
				   u_int32 data, 
				   u_int32 * return_status);
void    carbonXPcixCfgRead_Status(u_int64 address, 
				  u_int32 req_id, 
				  u_int32 req_tag, 
				  u_int32 *data, 
				  u_int32 * return_status);
void    carbonXPcix64MemWrite_Status(u_int64 address, 
				     u_int32 req_id, 
				     u_int32 req_tag, 
				     u_int32 * data, 
				     u_int32 length, // length is in bytes
				     u_int32 * return_status);
void    carbonXPcix64MemRead_Status(u_int64 address,  
				    u_int32 req_id, 
				    u_int32 req_tag, 
				    u_int32 *data, 
				    u_int32 length, // length is in bytes
				    u_int32 * return_status);

u_int32 carbonXPcixCmd(u_int32 command, 
		       u_int64 address, 
		       u_int32 req64data, 
		       u_int32 req_id, 
		       u_int32 req_tag, 
		       u_int32 * data, 
		       u_int32 length);  // length is in bytes

/*
** PCI-X command types
*/
#define PCIX_INTR_ACK           0x0
#define PCIX_SPECIAL_CYCLE      0x1
#define PCIX_IO_READ            0x2
#define PCIX_IO_WRITE           0x3
#define PCIX_RESERVED1          0x4
#define PCIX_RESERVED2          0x5
#define PCIX_MEM_READ           0x6
#define PCIX_MEM_WRITE          0x7
#define PCIX_MEM_READ_BLK_A     0x8
#define PCIX_MEM_WRITE_BLK_A    0x9
#define PCIX_CFG_READ           0xa
#define PCIX_CFG_WRITE          0xb
#define PCIX_SPLIT_COMP         0xc
#define PCIX_DAC                0xd
#define PCIX_MEM_READ_BLK       0xe
#define PCIX_MEM_WRITE_BLK      0xf


u_int8  carbonXPcixIntAck(u_int32 req_id, 
			  u_int32 req_tag);


/*
** Special Cycle Message Encodings
*/
#define SPECIAL_SHUTDOWN 0x0000
#define SPECIAL_HALT     0x0001
#define SPECIAL_X86      0x0002

#define SPECIAL_MESSAGE_MASK 0xffff

/*
** For backwards compatibility in the PCI tests
*/
#define PCI_GOOD_STATUS  STATUS__NORMAL
#define PCI_MASTER_ABORT STATUS__MASTER_ABORT
#define PCI_TARGET_ABORT STATUS__TARGET_ABORT
#define PCI_DISCONNECT_A STATUS__DATA_DISCONNECT_A
#define PCI_DISCONNECT_B STATUS__DATA_DISCONNECT_B
#define PCI_DISCONNECT_C STATUS__DATA_DISCONNECT_C
#define PCI_RETRY        STATUS__TARGET_RETRY
#define PCI_PARITY_ERROR STATUS__DATA_PARITY_ERROR
#define PCI_SYSTEM_ERROR STATUS__ADDR_PARITY_ERROR

/*
** For backwards compatibility in the PCIX tests
*/
#define PCIX_TARGET_GOOD_RESPONSE      STATUS__NORMAL
#define PCIX_MASTER_ABORT              STATUS__MASTER_ABORT
#define PCIX_TARGET_RETRY              STATUS__TARGET_RETRY 
#define PCIX_TARGET_ABORT              STATUS__TARGET_ABORT 
#define PCIX_TARGET_SPLIT_RESPONSE     STATUS__TARGET_SPLIT_RESPONSE
#define PCIX_SINGLE_DATA_DISCONNECT    STATUS__SINGLE_DATA_DISCONNECT
#define PCIX_TARGET_DISCONNECT_ADB     STATUS__TARGET_DISCONNECT_ADB 
#define PCIX_ILLEGAL_TARGET_RETRY      STATUS__ILLEGAL_TARGET_RETRY
#define PCIX_ILLEGAL_TARGET_WAIT       STATUS__ILLEGAL_TARGET_WAIT
#define PCIX_ILLEGAL_TARGET_SDD        STATUS__ILLEGAL_TARGET_SDD
#define PCIX_ILLEGAL_TARGET_SPLIT_RESP STATUS__ILLEGAL_TARGET_SPLIT_RESP
#define PCIX_ILLEGAL_TARGET_RESPONSE   STATUS__ILLEGAL_TARGET_RESPONSE
#define PCIX_ILLEGAL_BURST_AFTER_ADB   STATUS__ILLEGAL_BURST_AFTER_ADB
#define PCIX_MASTER_LATENCY_EXP        STATUS__MASTER_LATENCY_EXP
#define PCIX_DATA_PARITY_ERROR         STATUS__DATA_PARITY_ERROR 
#define PCIX_ADDR_PARITY_ERROR         STATUS__ADDR_PARITY_ERROR
#define PCIX_TARGET_LATENCY_EXP        STATUS__TARGET_LATENCY_EXP

/*
** Transactor definitions
*/
//  3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 
//  1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 
//  _ _____________________________________________________________
// | |   |         |                               |   | | |       |
// |V|   | REQ_TAG |            REQ_ID             | B |W|6| PCI   |
// |A|   |         |                               | C |R|4| CMD   |
// |L|   |         |                               | M |I|B|       |
// |I|   |         |                               |   |T|I|       |
// |D|   |         |                               |   |E|T|       |
// |_|___|_________|_______________________________|___|_|_|_______|

#define USER_OP__VALID    31
#define USER_OP__REQ_TAG  24 
#define USER_OP__REQ_ID    8
#define USER_OP__BCM       6
#define USER_OP__WRITE     5
#define USER_OP__64BIT     4
#define USER_OP__PCI_CMD   0

#define USER_OP__VALID_MASK       0x1
#define USER_OP__REQ_TAG_SC_MASK  0x7
#define USER_OP__REQ_TAG_MASK     0x1f
#define USER_OP__REQ_ID_MASK      0xffff
#define USER_OP__BCM_MASK         0x2
#define USER_OP__WRITE_MASK       0x1
#define USER_OP__64BIT_MASK       0x1
#define USER_OP__PCI_CMD_MASK     0xf

#define PCI_CONFIG_REG    0
#define PCI_CDP_REG       8

#define DEFAULT_CMD       4

typedef struct {
  u_int32 Command;
  u_int32 Num_bytes;
  u_int32 Req_attribute;
  u_int32 Start_address;
  u_int32 * ptr_write_data;
  u_int32 compare_data;
} SPLIT_ENTRY;


typedef struct {
  u_int32 valid_bits;
  SPLIT_ENTRY  split_entry[32];
} PCIX_MSTR_SPLIT_TABLE;

/* 
 * PCI-X Split Transaction Routines
 */

PCIX_MSTR_SPLIT_TABLE * carbonXPcixMasterCreateSplitTable();
void carbonXPcixMasterDestroySplitTable( PCIX_MSTR_SPLIT_TABLE * split_table_ptr);
void carbonXPcixMstrAddSplitEntry(u_int32 Command, u_int32 Num_bytes, u_int32 Req_attribute, u_int32 Start_address, u_int32 * ptr_write_data, PCIX_MSTR_SPLIT_TABLE * split_table_ptr, u_int32 compare);
void carbonXPcixMstrRemoveSplitEntry( PCI_OP_T * transaction, PCIX_MSTR_SPLIT_TABLE * split_table_ptr);
void carbonXPcixAttributeInRange(PCIX_MSTR_SPLIT_TABLE * split_table_ptr,  PCI_OP_T * transaction);

#ifdef __cplusplus
}
#endif

#endif
