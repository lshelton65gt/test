/****************************************************************
 PCI command types
 ****************************************************************/
parameter PCI_INTR_ACK            = 4'b0000;
parameter PCI_SPECIAL_CYCLE       = 4'b0001;
parameter PCI_IO_READ             = 4'b0010;
parameter PCI_IO_WRITE            = 4'b0011;
parameter PCI_RESERVED1           = 4'b0100;
parameter PCI_RESERVED2           = 4'b0101;
parameter PCI_MEM_READ            = 4'b0110;
parameter PCI_MEM_WRITE           = 4'b0111;
parameter PCI_RESERVED3           = 4'b1000;
parameter PCI_RESERVED4           = 4'b1001;
parameter PCI_CFG_READ            = 4'b1010;
parameter PCI_CFG_WRITE           = 4'b1011;
parameter PCI_MEM_READ_MULT       = 4'b1100;
parameter PCI_DAC                 = 4'b1101;
parameter PCI_MEM_READ_LINE       = 4'b1110;
parameter PCI_MEM_WRITE_INV       = 4'b1111;

/****************************************************************
 Internal register addresses
 ****************************************************************/
parameter PCI_CS_REG_ZERO    = 0;
parameter PCI_CS_REG_ONE     = 1;
parameter PCIX_CS_REG_ONE    = 2;
parameter PCIX_CS_REG_TWO    = 3;
parameter DEFAULT_64BIT      = 4;
parameter DEFAULT_WR_CMD     = 5;
parameter DEFAULT_RD_CMD     = 6;
parameter DEFAULT_REQ_TAG    = 7;
parameter DEFAULT_REQ_ID     = 8;

/****************************************************************
 Transaction definitions
 ****************************************************************/
parameter USER_OP__VALID        = 31;

parameter USER_OP__64BIT        = 4;
// define USER_OP__PCI_CMD     3:0
parameter USER_OP__PCI_CMD_H    = 3;
parameter USER_OP__PCI_CMD_L    = 0;
parameter USER_OP__WRITE        = 0;

//------------------------------------------//
//---- Termination types in Pcix_Status ----//
//------------------------------------------//
parameter STATUS__NORMAL                     = 16'h0000;
parameter STATUS__MASTER_ABORT               = 16'h0001;
parameter STATUS__TARGET_RETRY               = 16'h0002;
parameter STATUS__TARGET_ABORT               = 16'h0004;
parameter STATUS__TARGET_SPLIT_RESPONSE      = 16'h0008; // PCIX only;
parameter STATUS__DATA_DISCONNECT_A          = 16'h0008; // PCI only;
parameter STATUS__SINGLE_DATA_DISCONNECT     = 16'h0010; // PCIX only;
parameter STATUS__DATA_DISCONNECT_B          = 16'h0010; // PCI only;
parameter STATUS__TARGET_DISCONNECT_ADB      = 16'h0020; // PCIX only;
parameter STATUS__DATA_DISCONNECT_C          = 16'h0020; // PCI only;
parameter STATUS__ILLEGAL_TARGET_RETRY       = 16'h0040; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_WAIT        = 16'h0080; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_SDD         = 16'h0100; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_SPLIT_RESP  = 16'h0200; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_RESPONSE    = 16'h0400; // PCIX only;
parameter STATUS__ILLEGAL_BURST_AFTER_ADB    = 16'h0800; // PCIX only;
parameter STATUS__MASTER_LATENCY_EXP         = 16'h1000; // PCIX only;

//------------------------------------------//
//--- Error bit positions in Pcix_Status ---//
//------------------------------------------//

parameter DATA_PARITY_ERROR          = 16;
parameter ADDR_PARITY_ERROR          = 17;
parameter TARGET_LATENCY_EXP         = 18;  // PCIX only;


//------------------------------------------//
//--- V->C defines -------------------------//
//------------------------------------------//
parameter PCI_RANGE_CHECK      = 32'h000000ff;
parameter PCI_GET_RESPONSE     = 32'h000000fe;
parameter PCI_GET_WAIT         = 32'h000000fd;
parameter PCIX_GET_RESPONSE    = 32'h000000fc;
parameter PCI_PARITY_ERR       = 32'h000000fb;
parameter PCIX_DECODE_TIME     = 32'h000000f0;
parameter PCI_DECODE_TIME      = 32'h000000f1;

parameter PCI_RANGE_OK         = 32'h00000001;
parameter PCI_RANGE_OK_32      = 32'h00000003;
parameter PCI_RANGE_OK_BIT     = 0;
parameter PCI_RANGE_32_BIT     = 1;

parameter DECODE_A        = 32'h00000000;
parameter DECODE_B        = 32'h00000001;
parameter DECODE_C        = 32'h00000002;
parameter DECODE_S        = 32'h00000003;
parameter DECODE_FAST     = 32'h00000000;
parameter DECODE_MEDIUM   = 32'h00000001;
parameter DECODE_SLOW     = 32'h00000002;

// define STATUS__REQ_ATTR    28:8
parameter STATUS__REQ_ATTR_H = 28;
parameter STATUS__REQ_ATTR_L =  8;

// define STATUS__CBE         7:4
parameter STATUS__CBE_H               = 7;
parameter STATUS__CBE_L               = 4;

// define STATUS__SPLIT_COMP  4
parameter STATUS__SPLIT_COMP          = 4;

// define STATUS__ACCESS_TYPE 3:0
parameter STATUS__ACCESS_TYPE_H       = 3;
parameter STATUS__ACCESS_TYPE_L       = 0;

// define RET_STATUS__NUM_DATA_PH    15:8
parameter RET_STATUS__NUM_DATA_PH_H   =  15;
parameter RET_STATUS__NUM_DATA_PH_L   =  8;

// define RET_STATUS__NUM_INIT_WAIT   7:5
parameter RET_STATUS__NUM_INIT_WAIT_H = 7;
parameter RET_STATUS__NUM_INIT_WAIT_L = 5;

// define RET_STATUS__RESP_TYPE       3:0
parameter RET_STATUS__RESP_TYPE_H     =  3;
parameter RET_STATUS__RESP_TYPE_L     =  0;

// define RET_STATUS__NUM_WAIT        7:0
parameter RET_STATUS__NUM_WAIT_H      =  7;
parameter RET_STATUS__NUM_WAIT_L      =  0;

// define RET_STATUS__DEC_TIME        3:0
parameter RET_STATUS__DEC_TIME_H      =  3;
parameter RET_STATUS__DEC_TIME_L      =  0;

parameter NORMAL   = 0;
parameter RETRY    = 1;
parameter TABRT    = 2;
parameter DISCNT   = 3;
parameter ADB      = 4;
parameter SPLIT    = 5;

