
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Description
//
// PCI Master transactor

`timescale 1 ns / 100 ps



module carbonx_pci_master (
		    clk,
		    reset_l,
		    req_l,
		    gnt_l,
		    req64_l,
		    ack64_l,
		    frame_l,
		    devSel_l,
		    irdy_l,
		    trdy_l,
		    ad,
		    cbe_l,
		    stop_l,
		    par,
		    par64,
		    perr_l,
		    serr_l,
		    int_l,

		    operation,  // op & address are in only
		    address,
		    get_size,	// rest are i/o
		    get_status,
		    get_data,
		    get_csdata,
		    put_size,
		    put_status,
		    put_data,
		    Xrun,
		    put_dwe,
		    gp_daddr
		    
		    );

   
   /**********************************
    ** List all Input/Output signals **
    **********************************/
   input        clk,
		reset_l,
		gnt_l,
		devSel_l,
		trdy_l,
		stop_l,
		ack64_l,
		serr_l;

   input [3:0] 	int_l; 

   inout 	frame_l,
		par,
		par64,
		perr_l;
   
   inout	irdy_l;

   inout [7:0] 	cbe_l;

   inout [63:0] ad;

   output 	req_l;
   output 	req64_l;
   
   input[31:0]	operation;  // op & address are in only
   input[63:0]	address;
   input[31:0]	get_size;	// rest are i/o
   input[31:0]	get_status;
   input[63:0]	get_data;
   input[63:0]	get_csdata;
   output[31:0]	put_size;
   output[31:0]	put_status;
   output[63:0]	put_data;
   output	Xrun;
   output	put_dwe;
   output[13:0]	gp_daddr;

`protect
// carbon license crbn_vsp_exec_xtor_pci

`include "carbonx_pci_defines.vh"

   
/**************************************************************
 ** PCI Master transactor
 ** Taken from VIP library and modified to make it a transactor
 **
 ** Modified by      : Nirmala Paul
 ** Last modified on : 7/3/2001
 **************************************************************
 ** This file has the PciControltasks.vh included. 
 **
 ** Features : 
 ** 1. There are 2 configuration registers configRegZero and
 ** configRegOne, which can be written using a CS_Write. 
 ** These registers are used to corrupt address and data parity,
 ** and enable auto retry.
 ** For address parity corruption, set bit 8 of register 0.
 ** For data parity corruption...set bit 9 of register 0. Then
 ** Setting bits in register 1 will corrupt the data parity of 
 ** of the corresponding data phases.
 ** eg 0x00000005 - data parity for data phases 1 and 3 will be wrong
 **
 ** 2. pciStatus register reports the status of the transaction
 ** This is transported back to C-side using TBP.
 ** 
 ** 3. Byte accesses can be done to any address
 **    16 bit write/read address has to be halfword aligned (addr[0] is ignored)
 **    32 bit write/read address has to be word aligned (addr[1:0] is ignored)
 **
 ** 4. Transaction is terminated on system error (address parity err)
 ****************************************************************/
   reg[31:0]	put_size;
   reg		Xrun;
   reg[13:0]	rdat_gp_daddr;
   wire[63:0]	wdat_data;
   reg[63:0]	wdat_data_reg;
   reg[63:0]	wdat_last_data;
   wire[63:0]	wdat_rotator;
   reg		ld_getdata, ld_getdata_d, ld_getdata_i;
   
   /**********************************
    ** Internal registers and wires  **
    **********************************/
   reg 		master,
		master_1,
		req,
		req64,
		frame,
		irdy,
		addrParity,
		addrParity_1,
		//dataParity,
		dataChkParity,
		dataChkParity_1,
		checkRdParity,
		parity,
		parity64,
		readParErr,
		readParEnable,
		readParEnable_1,
		dataPhase,
		doingIt,
		doneIt,
		retryAccess,
		writing,
		writing_1,
		reading,
		doSomething,
		burstAccess,
		use64bitaccess,
		use_DAC,
		dac,
		dataPhase_1; 

   wire 	dataParity;
   wire 	dataParity64;
   
   reg [2:0] 	devSelTimer;

   reg [3:0] 	curState,
		transferSize;
   
   reg [7:0] 	latencyTimer,
		burstDataCount,
		startBurstDataCount;
   
   reg [31:0] 	burstCount;

   wire [31:0] 	configRegOne = get_csdata[63:32];
   
   reg [31:0] 	pciStatus;
   wire[31:0] 	put_status = pciStatus;
   
   reg [63:0] 	accessAddress;
   reg [63:0] 	startAddress;
      
//   parameter 	DATA_BUFFER_SIZE = 128;
//   reg [31:0] 	wrData [0:DATA_BUFFER_SIZE-1];

   wire [63:0] 	ad;
   
   wire 	devSelTimeout,
		parErr,
		sysErr,
		lastData,
		lastWord,
		secondLastWord,
		grant,
		devSel,
		trdy,
		stop,
		corruptAddrParity,
		corruptDataParity,
		enableAutoRetry;

   wire		ack64;
   reg		ack64_d;
   reg 		devSel_d;

   reg [3:0] 	cmd, intStatus;
   wire [3:0] 	DAC_cmd;

   wire [7:0] 	mstrLatencyTime,
		burstCount_w;

   wire [31:0] 	wr_data_lo;
   wire         first_data;
   wire [3:0]   EndAddress;
   wire 	block_last_terms;
   reg [3:0] 	byte_en1;
   reg [3:0] 	byte_en2;
   reg [3:0] 	byte_en_l;		// Lower byte enables
   reg [3:0] 	byte_en_h;		// Upper byte enables

   integer 	i;

   
   /*************************************
    * State machine parameters         **
    *************************************/
   parameter 	PCI_IDLE     = 4'h0,
		PCI_REQ      = 4'h1,
		PCI_GNT      = 4'h2,
		PCI_SEND_DAC = 4'h3,
		PCI_DATA0    = 4'h4,
		PCI_BURST    = 4'h5,
		PCI_MABRT    = 4'h6,
		PCI_RETRY    = 4'h7,
		PCI_TABRT    = 4'h8,
		PCI_CYCABRT  = 4'h9;

   
   /**********************************
    ** Continuous Assignments        **
    **********************************/

   assign DAC_cmd = 4'b1101;

   // Assign all PCI signals

   assign #1 req_l = ~req;
   assign #1 frame_l = master ? ~frame : 1'bz;
   assign #1 irdy_l = (master || master_1) ? ~irdy : 1'bz;

   assign wr_data_lo = (first_data & accessAddress[2] & ack64) ? 32'hdeadbeef : 
				  (burstDataCount[0] & ~ack64) ? wdat_rotator[63:32] :
								 wdat_rotator[31:0];
   assign #1 ad[31:0] = (master & (~dataPhase | writing)) ?
					 (~dataPhase ? (dac ? accessAddress[63:32] :
						             {accessAddress[31:2],2'b00}) : 
				                              wr_data_lo) :
							      32'bz;

   assign #1 cbe_l[3:0] = master ? (~dataPhase ? (use_DAC ? (dac ? cmd : DAC_cmd) : cmd) : byte_en_l) : 4'bz;

   assign #1 par = master_1 ? ((~dataPhase_1 || writing_1) ? parity : 1'bz) : 1'bz; 

   // 64 bit data path signals

   assign #1 req64_l = master ? ~req64 : 1'bz;

   assign #1 ad[63:32] = (master & use64bitaccess & ((~dataPhase & use_DAC) | writing)) ?
			  (~dataPhase ? accessAddress[63:32] :
			                wdat_rotator[63:32]) :
					32'bz ;

   assign #1 cbe_l[7:4] = (master & use64bitaccess) ?
			  (~dataPhase ? (use_DAC ? cmd : 4'bz) :
			   ( (curState == PCI_DATA0) ? byte_en_h :
			     (ack64 ? byte_en_h : 4'bz) ) ) : 4'bz;

   assign #1 par64 = master_1 ? (use64bitaccess ? ((~dataPhase_1 || writing) ? parity64 : 1'bz) : 1'bz) : 1'bz;


   // Internal signals
   assign devSelTimeout = burstAccess ? (devSelTimer==3'b101) : 
			  (devSelTimer==3'b100);

   assign perr_l = readParEnable_1 ? ~readParErr : 1'bz;
   
   assign devSel = ~devSel_l;
   assign grant = ~gnt_l;
   assign trdy = ~trdy_l;
   assign stop = ~stop_l;
   assign parErr = ~perr_l;
   assign sysErr = ~serr_l;


   assign ack64 = ~ack64_l;

   assign block_last_terms = use64bitaccess & accessAddress[2] & ~ack64 & first_data;
   
   assign lastData = ~block_last_terms & (burstCount==8'b0000);
   assign lastWord = ~block_last_terms & ( (burstCount < 8'h4) ||
					   ((burstCount < 8'h8) & ack64 & (!accessAddress[2] || (curState != PCI_DATA0))) || ((mstrLatencyTime != 0) & (latencyTimer == 0) & !grant));

   assign secondLastWord = ( ((burstCount < 8) & !ack64) ||
			     ((curState == PCI_DATA0) & !accessAddress[2] & (burstCount < 16) & ack64) ||
			     ((curState == PCI_DATA0) & accessAddress[2] & (burstCount < 12) & ack64) ||
			     ((curState == PCI_BURST) & (burstCount < 16) & ack64) );


   assign mstrLatencyTime   = get_csdata[7:0];
   assign corruptAddrParity = get_csdata[8];
   assign corruptDataParity = get_csdata[9];
   assign enableAutoRetry   = get_csdata[10];

   assign dataParity = ^{wr_data_lo,byte_en_l};

   assign dataParity64 = (curState == PCI_DATA0) ? (accessAddress[2] ?
						    ^{wdat_rotator[63:32],byte_en_h} :
						    ^{wdat_rotator[63:32],byte_en_h}) :
			 (ack64 ? ^{wdat_rotator[63:32],byte_en_h} :
			  ^{36{1'b1}}) ;

//  ****************************************
//
//  This is a passle of combinatorial logic to handle rotation and assembly
//  of read data from PCI to the XIF_core data memory.
//  This replaces all the rotate logic into the rddata array as well as packing
//  logic in the pci_bfm_read tasks
//  
//  ****************************************
   reg [31:0]    actual_size;
   wire[63:0]    rdat_newbytes;

	// Read data valid this cycle
   wire rdat_valid = ((curState==PCI_DATA0) || (curState==PCI_BURST)) && trdy && reading;

	// Read data bus normalized for 64/32 bit operation
   wire[63:0] rdat_normdata = ack64 ? ad : {ad[31:0], ad[31:0]};

	// Rotate normalized data appropriately
   wire[63:0] rdat_rotator  = rotate(rdat_normdata, ack64 ? accessAddress[2:0] : {burstDataCount[0],accessAddress[1:0]});

	// Byte enables for save register and put data
   wire[63:0] rdat_be  = ack64 ?        64'hffffffffffffffff >> {accessAddress[2:0],3'b0} 
			      : rotate(64'h00000000ffffffff, {burstDataCount[0], accessAddress[1:0]});

   wire[2:0] rdat_seldist = curState == PCI_DATA0 ? 0 : 8 - accessAddress[2:0];


	// This represents how many bytes on the bus are valid
	// This gets messy because of the case where we need 3 cycles to fetch data to fill a dpram word (6-8 bytes)
	// eg: read 6 bytes from address 3; cy1: get byte 1 cy2: get bytes 2-5 cyc3: get byte 6
	// During that last cycle we have to be careful to NOT select new data for bytes 7,8,9
	// of ccourse none of this applies to cases < 6, so that case is excluded
   wire[1:0] rdat_nbd = 3'h4 - accessAddress[1:0];
   assign    rdat_newbytes[63:32] = 0;
   assign    rdat_newbytes[31:0]  = (burstDataCount[0] || get_size<4)? 32'hffffffff : 32'hffffffff >> {rdat_nbd, 3'b0};
	// Select which bytes come from live bus vs save reg
   wire[63:0] rdat_sel = ack64_d ?        64'hffffffffffffffff << {rdat_seldist,3'b0} 
			      : rotate(rdat_newbytes, {burstDataCount[0], accessAddress[1:0]});

	// output to XIF_core RAM assemble together saved & live bus data
   reg [63:0] rdat_save;
   wire[63:0] put_data = (rdat_rotator & rdat_sel) | (rdat_save & ~rdat_sel);

   reg        rdat_newbc;  // record that burstDataCount has changed, to write possible tail data
   wire       put_dwe  = rdat_valid | rdat_newbc;

	// use register directly for reading, bumped burstcount for writing
   assign  gp_daddr = reading ? rdat_gp_daddr : burstDataCount[7:1]+1;

	//For writing we do the opposite rotation
   assign wdat_data    = ld_getdata & ld_getdata_d ? get_data : wdat_data_reg;  // bypass mux
   assign wdat_rotator = shiftleft(wdat_data, wdat_last_data, {accessAddress[2]&ack64, accessAddress[1:0]});



//  Rotate right by dist BYTES (dist*8 bits)

function[63:0] rotate;
input[63:0] a;
input[2:0] dist;
begin
   rotate = a >> (dist*8) | a << ((8-dist)*8);
end
endfunction

function[63:0] shiftleft;
input[63:0] a;
input[63:0] b;
input[2:0] dist;
begin
   shiftleft = (a << (dist*8)) | (b >> (64-dist*8));
end
endfunction



   always @ (posedge clk)
      intStatus = ~int_l;
   

   // calculate parity for address
   always @ (use64bitaccess or use_DAC or accessAddress or cmd or dac or DAC_cmd)
      begin
	 if (use64bitaccess)
	    begin
	       if (use_DAC)
		  begin
		     if (!dac)
			addrParity <= ^{accessAddress[31:2],DAC_cmd};
		     else
			addrParity <= ^{accessAddress[63:32],cmd};
		  end
	       else
		  addrParity <= ^{accessAddress[31:2],cmd};
	    end
	 else
	    begin
	       if (use_DAC)
		  begin
		     if (!dac)
			addrParity <= ^{accessAddress[31:2],DAC_cmd};
		     else
			addrParity <= ^{accessAddress[63:32],cmd};
		  end
	       else
		  addrParity <= ^{accessAddress[31:2],cmd};
	    end
      end


   
   
  assign first_data = (curState == PCI_DATA0);
  assign EndAddress = startAddress[3:0] + transferSize - 1;

  always @ (first_data or lastWord or accessAddress or EndAddress)
     begin
	if (first_data) begin
	   case (accessAddress[1:0])
	     0 : byte_en1 <= 4'h0;
	     1 : byte_en1 <= 4'h1;
	     2 : byte_en1 <= 4'h3;
	     3 : byte_en1 <= 4'h7;
	   endcase 
	end
	else
	   byte_en1 <= 4'h0;
	// For the last data phase, all bytes after the ending
	// address are disabled (byte enable is high)
	
	if (lastWord) begin
	   case (EndAddress[1:0])
	     0 : byte_en2 <= 4'he;
	     1 : byte_en2 <= 4'hc;
	     2 : byte_en2 <= 4'h8;
	     3 : byte_en2 <= 4'h0;
	   endcase 
	end 
	else byte_en2 <= 4'h0;
     end // always @ (first_data or lastWord or accessAddress or EndAddress)
   
   always @ (use64bitaccess or byte_en1 or byte_en2 or  EndAddress or accessAddress 
	     or first_data or lastData or lastWord or reading or curState)
      begin
	 if(reading)
	    begin
	       byte_en_l <= 4'h0;
	       byte_en_h <= 4'h0;
	    end
	 else if(curState!=PCI_IDLE)begin
	    if(use64bitaccess) begin
	       case ({first_data, lastWord})
		 2'b00	: begin
		    byte_en_l <= 4'h0;
		    byte_en_h <= 4'h0;
		 end	
		 2'b01 : begin
		    if(EndAddress[2]) begin
		       byte_en_l <= 4'h0;
		       byte_en_h <= byte_en2;
		    end
		    else begin
		       byte_en_l <= byte_en2;
		       byte_en_h <= 4'hf;
		    end
		 end	
		 2'b10 : begin
		    if (accessAddress[2]) begin
		       byte_en_l <= 4'hf;
		       byte_en_h <= byte_en1;
		    end
		    else begin
		       byte_en_l <= byte_en1;
		       byte_en_h <= 4'h0;
		    end
		 end	
		 2'b11 : begin
		    if (accessAddress[2] == EndAddress[2])begin
		       if (accessAddress[2]) begin
			  byte_en_h <= byte_en1 | byte_en2;
			  byte_en_l <= 4'hf;
		       end
		       else begin
			  byte_en_l <= byte_en1 | byte_en2;
			  byte_en_h <= 4'hf;
		       end
		    end
		    else begin
		       byte_en_l <= byte_en1;
		       byte_en_h <= byte_en2;
		    end
		 end	
	       endcase 
	    end // .......if(use64bitaccess)
	    else begin // ......... if 32 bit
	       byte_en_h <= 4'h0;
	       if(lastWord) 
		  byte_en_l <= byte_en1 | byte_en2;  
	       else if(first_data)
		  byte_en_l <= byte_en1;
	       else
		  byte_en_l <= 4'h0;
	    end
	 end else begin
	    byte_en_l <= 4'h0;
	    byte_en_h <= 4'h0;
	 end
      end
   

   /**********************************
    ** State machine                 **
    **********************************/
   always @(posedge clk or negedge reset_l)
      begin


   /**********************************
    ** Synchronous Logic             **
    **********************************/
	 if (~reset_l)
	    begin
	       master      <= 1'b0;
	       req64       <= 1'b0;
	       master_1    <= 1'b0;
	       frame       <= 1'b0;
	       irdy        <= 1'b0;
	       dac         <= 4'b0;
	       addrParity_1<= 1'b0;
	       dataChkParity <= 1'b0;
	       dataChkParity_1 <= 1'b0;
	       parity       <= 1'b0;
	       parity64     <= 1'b0;
	       readParErr   <= 1'b0;
	       readParEnable<= 1'b0;
	       readParEnable_1 <= 1'b0;
	       dataPhase    <= 1'b0;
	       dataPhase_1  <= 1'b0; 
	       curState     <= PCI_IDLE;
	       devSelTimer  <= 3'b0;
	       use_DAC      <= 1'b0;
	       burstCount   <= 8'b0;
	       burstDataCount <= 8'b0;
	       startBurstDataCount <= 8'b0;
	       doSomething  <= 1'b0;
	       doingIt      <= 1'b1;
	       doneIt       <= 1'b1;
	       writing_1    <= 1'b0;
	       transferSize <= 4'b0;
	       latencyTimer <= 8'b0;
	       retryAccess  <= 1'b0;
	       devSel_d     <= 0;
	       rdat_newbc   <= 0;
	       ld_getdata_d <= 0;
	       rdat_save    <= 0;
	       rdat_gp_daddr<= 0;
	       checkRdParity<= 1'b0;
	       ack64_d      <= 0;
	       wdat_data_reg <= 0;
	       wdat_last_data<= 0;

	       req           <= 1'b0;
	       pciStatus     <= STATUS__NORMAL;
	       curState      <= PCI_IDLE;
	       Xrun          <= 0;
	       startAddress  <= 0;
	       ld_getdata    <= 0;
	       ld_getdata_i  <= 0;
	       use64bitaccess = 1'b0;
	       reading       <= 1'b0;
	       writing       <= 1'b0;
	       burstAccess   <= 1'b0;
	       cmd           <= 4'h0;
	       put_size      <= 0;
	    end
	 else
	    begin
	       cmd <= operation[3:0];
	       ack64_d <= ~ack64_l; // this needs to hang around a cycle after the bus completes

	       ld_getdata_d <= ld_getdata;

	       if (ld_getdata_i | (ld_getdata & ld_getdata_d)) begin  // Initial data or bypass load
		  wdat_data_reg <= get_data;
	       end

	       if (ld_getdata_i) begin  // Initial data only
		  wdat_last_data<= get_data;
	       end

	       devSel_d <= devSel;
	       
	       dataPhase_1 <= dataPhase; // added to fix parity output, mark 6/1/01
	       
	       // register bus mastership
	       if ((curState==PCI_REQ) && grant && frame_l && irdy_l)
		  master <= 1;
	       else if (( ((curState==PCI_DATA0)||(curState==PCI_BURST)) &&
			  (lastWord && trdy) ) || (curState==PCI_TABRT) || (curState==PCI_CYCABRT) || (curState==PCI_IDLE) )
//			(curState==PCI_MABRT) )
		  master <= 0;


	       // delayed master used to control irdy lines
	       // irdy should be tristated one clock after last data
	       master_1 <= master;

	       // delayed writing used to control par line
		writing_1 <= writing;
	

	       
	       // drive frame
		// One must be careful about de-asserting frame before we know if the
		// target is 64 bit or not
	       if ((curState==PCI_REQ) && grant && frame_l && irdy_l)
		  frame <= 1;
	       else if ( ( ((curState==PCI_BURST) || (curState==PCI_DATA0)) && secondLastWord
			   && trdy) || stop || (curState==PCI_MABRT) ||
			 ((curState==PCI_GNT) && lastWord && !use64bitaccess) ||
//			 (use64bitaccess && devSel && ack64 && lastWord && first_data) )
			 (use64bitaccess && ack64 && lastWord && first_data) )
		  frame <= 0;


	       // drive the req64 lines

	       if ((curState==PCI_REQ) && grant && frame_l && irdy_l && use64bitaccess)
		  req64 <= 1;
	       else if ( ( ((curState==PCI_BURST) || (curState==PCI_DATA0)) && secondLastWord
			   && trdy) || stop || (curState==PCI_MABRT) ||
			 ((curState==PCI_GNT) && lastWord && !use64bitaccess) ||
//			 (use64bitaccess && devSel && ack64 && lastWord && first_data) )
			 (use64bitaccess && ack64 && lastWord && first_data) )
		  req64 <= 0;



	       // drive irdy

	       if ( (((curState==PCI_DATA0)||(curState==PCI_BURST)) && (lastWord && trdy) && irdy) || 
		    (curState==PCI_TABRT) || 
		    (curState==PCI_CYCABRT) ||
		    ((curState==PCI_MABRT) && frame_l) || 
		    (curState==PCI_IDLE) ||
		    (((curState==PCI_DATA0)||(curState==PCI_BURST)) && stop && frame_l))
		  irdy <= 1'b0;
	       else if ((curState==PCI_GNT) && !use64bitaccess) 
		       irdy <= 1;
		    else if ((curState==PCI_DATA0) && use64bitaccess  && devSel) // - AG 10/10/02
		       irdy <= 1;

	       
	       // select data phase

	       if (curState==PCI_REQ)
		  dataPhase <= 1'b0;
	       else begin
		  if (use_DAC) begin
		     if (curState==PCI_SEND_DAC)
			dataPhase <= 1;
		  end
		  else
		     dataPhase <= 1'b1;
	       end

	       
	       // decrement burst cycle counter  

	       if ( ((curState==PCI_DATA0) || (curState==PCI_BURST)) && trdy)
		  begin
		     if (lastWord)
			burstCount <= 0;
		     else
			begin
			   if (!ack64)
			      begin
				 if ((curState==PCI_DATA0) && accessAddress[2] && use64bitaccess)
				    burstCount <= burstCount;
				 else
				    burstCount <= burstCount - 4;
			      end
			   else
			      begin
				 if ((curState==PCI_DATA0) && accessAddress[2])
				    burstCount <= burstCount - 4;
				 else
				    burstCount <= burstCount - 8;
			      end
			end
		  end



	       // increment burst data counter

	       if (curState == PCI_IDLE) begin
		  burstDataCount      <= 8'b0;
		  startBurstDataCount <= 8'b0;
		  wdat_last_data      <= wdat_data;
		  wdat_data_reg       <= get_data;

	       end else if (((curState==PCI_BURST)||(curState == PCI_DATA0)) && trdy) begin
// change to:           if (((curState==PCI_BURST)||(curState == PCI_DATA0) || 
//                          || ((curState==PCI_BURST) || (curState==PCI_BURST) && early_irdy)) && trdy) - AG
		  if (!ack64) begin
		     if ((curState == PCI_DATA0) && accessAddress[2] && use64bitaccess) begin
		        burstDataCount <= burstDataCount;
		     end else begin
		        if (burstDataCount[0]) begin
			   wdat_last_data<= wdat_data;
			   wdat_data_reg <= get_data;
			end
		        burstDataCount <= burstDataCount + 1;
		     end
//		  end else if ((curState == PCI_DATA0) && accessAddress[2]) begin
//		     burstDataCount <= burstDataCount + 1;
		  end else begin
		     burstDataCount <= burstDataCount + 2;
		     wdat_last_data <= wdat_data;
		     wdat_data_reg  <= get_data;   // Get next data word
		  end
               end else begin

	       end

	       // flag tasks that operation is underway
	       if ((curState==PCI_REQ) && grant && frame_l && irdy_l)
		  doingIt <= 1'b1;
	       else
		  doingIt <= 1'b0;

	       
	       // flag tasks that operation is complete
	       if ( (((curState==PCI_BURST) || (curState==PCI_DATA0)) && 
		     lastWord && trdy)  || (curState==PCI_MABRT) || 
		    ((curState==PCI_CYCABRT) & (~enableAutoRetry || pciStatus[ADDR_PARITY_ERROR] || pciStatus[DATA_PARITY_ERROR] ))
		     || (curState==PCI_TABRT))
		  doneIt <= 1'b1;
	       else
		  doneIt <= 1'b0;

	       
	       // generate put_data control signals
	       if (rdat_valid) begin
//
// Addressing & writing get tricky because of unaligned accesses
// We have to figure out what address to write to next cycle, based on
// the accessaddress, size & op-mode (64/32).  An unaligned access causes 
// us to have to do an "extra" write to the xif_core memory, since the initial
// write doesn't fill the first word, then after the second write, there could be 
// additional data that overflows into the second RAM word.  I hope that is clear....
//
		    rdat_gp_daddr <= (burstDataCount + rdat_be[63] + ack64) >> 1;  // hang onto gp_daddr until after this cycle
		    rdat_newbc <= burstDataCount[7:1] != rdat_gp_daddr;
//
			// Load save register according to be (byte enable) bits
		    rdat_save <= (rdat_rotator & rdat_be) | (rdat_save & ~rdat_be);
		    
	       end else begin
		    rdat_newbc <= 0;
	       end

	       
	       if (curState==PCI_IDLE)
		  rdat_gp_daddr <= 0;

	       // devSel timeout monitor
	       if ((curState==PCI_IDLE) || devSel)
		  devSelTimer <= 3'b0;

	       else if (((curState==PCI_REQ) && grant && frame_l && irdy_l) ||
			(curState==PCI_GNT) ||
			(curState==PCI_DATA0) ||
			(curState==PCI_BURST) )
		  devSelTimer <= devSelTimer + 1;
	       

	       if (^{accessAddress[63:32], cmd}==1)
		  addrParity_1 <= 1'b1;
	       else
		  addrParity_1 <= 1'b0;

	       
	       // register output parity to appear on bus 1 cycle later
	       if (dataPhase)
		  parity <= dataParity^(configRegOne[burstDataCount]&corruptDataParity);
	       else
		  parity <= addrParity^corruptAddrParity;
	       

	       if (dataPhase)
		  parity64 <= dataParity64;
	       else
		  begin
		     if (use_DAC & use64bitaccess)
			parity64 <= addrParity_1;
		     else
			parity64 <= ^{36{1'b1}};
		  end


	       // register data parity during reads
	       dataChkParity <= ^{ad[31:0],cbe_l[3:0]};

	       dataChkParity_1 <= (^{ad[63:32],cbe_l[7:4]}) & req64;

	       
	       // indicate when to check read data parity
	       if (trdy && irdy && devSel && reading)
		  checkRdParity <= 1'b1;
	       else
		  checkRdParity <= 1'b0;
	       

	       // register parity error during a read
	       if (checkRdParity && (dataChkParity != par) && (dataChkParity_1 != par64))
		  readParErr <= 1'b1;
	       else
		  readParErr <= 1'b0;

	       

	       if (master && reading && dataPhase)
		  readParEnable <= 1'b1;
	       else 
		  readParEnable <= 1'b0;

	       readParEnable_1 <= readParEnable;



	       if ((curState == PCI_GNT) && use_DAC)
		  dac <= 1;
	       else
		  dac <= 0;


	       
	       // bus master latency timer
	       if (grant && frame_l && irdy_l)
		  latencyTimer <= mstrLatencyTime;
	       else if (master && (latencyTimer>0))
		  latencyTimer <= latencyTimer-1;
	       

	       // retry indicator
	       if ((curState == PCI_DATA0) && stop && devSel && enableAutoRetry)
		  retryAccess <= 1'b1;
	       else
		  retryAccess <= 1'b0;


	       case (curState)

		 PCI_IDLE : begin
		    ld_getdata    <= 0;
		    if (operation != 0) begin
		          use_DAC       <= (address[63:32] != 0);
			  use64bitaccess =  operation[USER_OP__64BIT];
			  writing	<=  operation[USER_OP__WRITE];  //!!!
			  reading	<= ~operation[USER_OP__WRITE];  //!!!
			  burstAccess	<= ((get_size + address[1:0])/4 + (((get_size + address[1:0])%4)? 1:0)) > 1;
//  too simple:		  burstAccess	<= get_size > (use64bitaccess ? 8 : 4);
			  accessAddress	<= address[63:0];
	 // The actual number of bytes to be transferred
	    		  actual_size    = get_size;

			  burstCount	<= (actual_size - 1) + address[1:0];
			  transferSize	<= actual_size;
			  ld_getdata_i  <= 1;
			  pciStatus	<= STATUS__NORMAL;
			  req		<= 1'b1;
			  curState	<= PCI_REQ;
			  Xrun		<= 1'b1;
			  startAddress	<= address;  //!!! redundancy !!!
			  put_size      <= get_size;
			  //$display("%m burst count %h accessAddress %h", burstCount,accessAddress,$time);
		    end else begin
			  curState <= PCI_IDLE;
			  Xrun     <= 1'b0;
		    end
		 end
		 
		 PCI_REQ : begin
		    ld_getdata_i   <= 0;
		    if (grant && frame_l && irdy_l) begin
			  curState <= PCI_GNT;
		    end else begin
			  req <= 1'b1;
			  curState <= PCI_REQ;
		    end
		 end
		 
		 PCI_GNT : begin
		    if (use_DAC)
		       begin
			  curState <= PCI_SEND_DAC;
		       end
		    else
		       begin
			  req <= 1'b1;
			  curState <= PCI_DATA0;
		       end
		 end


		 PCI_SEND_DAC : begin
		    curState <= PCI_DATA0;
		 end

		 
		 PCI_DATA0 : begin
		    req <= 1'b0;
		    if (devSel_d)
		       use64bitaccess = ack64;   // what the heck is this about!!!!
		    
		    if (sysErr)
		       begin
			  pciStatus[ADDR_PARITY_ERROR] <= 1;
			  curState <= PCI_MABRT;
		          ld_getdata  <= 0;
		       end
		    else if(stop)
		       begin
			  // determine type of termination
			  if (stop && trdy && devSel && ~irdy) 
			     begin
				pciStatus <= STATUS__DATA_DISCONNECT_A;
				curState <= PCI_CYCABRT;
			     end
			  else if (stop && trdy && devSel && irdy)
			     begin
				curState <= PCI_CYCABRT;
				pciStatus <= STATUS__DATA_DISCONNECT_A;
			     end
			  else if (stop && devSel)
			     begin
				pciStatus <= STATUS__TARGET_RETRY;
				curState <= PCI_CYCABRT;
			     end
			  else if(stop && ~devSel)
			     begin
				pciStatus <= STATUS__TARGET_ABORT;
				curState <= PCI_TABRT;
			     end
			  if (parErr)
			     begin
				pciStatus[DATA_PARITY_ERROR] <= 1;
				curState <= PCI_CYCABRT;
			     end
		          ld_getdata  <= 0;
		       end
		    else if(devSelTimeout)
		       begin
			  pciStatus <= STATUS__MASTER_ABORT;
			  curState <= PCI_MABRT;
		          ld_getdata  <= 0;
		       end
		    else if (trdy & devSel & irdy)
		       begin
		          ld_getdata  <= ack64 | burstDataCount[0];
			  if ((burstAccess && !lastWord) ||
			      (use64bitaccess && !ack64 && accessAddress[2]))  //!!! did they mean this as temp or reg? !!!
			     curState <= PCI_BURST;
			  else begin
			     curState <= PCI_IDLE;
			     Xrun     <= 1'b0;
			  end
			  if (parErr) pciStatus[DATA_PARITY_ERROR] <= 1;
		       end
		    else
		       begin
		          ld_getdata  <= 0;
			  curState <= PCI_DATA0;
			  // if (parErr) pciStatus <= `PCI_PARITY_ERROR;
		       end
		 end
		 
		 PCI_BURST : begin
		    if (sysErr) begin
			  pciStatus[ADDR_PARITY_ERROR] <= 1;
			  curState <= PCI_MABRT;
		          ld_getdata  <= 0;
		    end else if(stop ) begin
			  // determine type of target abort
			  if (stop && trdy && devSel && ~irdy) begin
				pciStatus <= STATUS__DATA_DISCONNECT_A;
				curState <= PCI_CYCABRT;
			  end else if (stop && trdy && devSel && irdy) begin
				pciStatus <= STATUS__DATA_DISCONNECT_B;
				curState <= PCI_CYCABRT;
			  end else if (stop && devSel) begin
				pciStatus <= STATUS__DATA_DISCONNECT_C;
				curState <= PCI_CYCABRT;
			  end else if(stop && ~devSel) begin
				pciStatus <= STATUS__TARGET_ABORT;
				curState <= PCI_TABRT;
			  end
			  if (parErr) begin
				pciStatus[DATA_PARITY_ERROR] <= 1;
				curState <= PCI_CYCABRT;
			  end
		          ld_getdata  <= 0;
		    end else if (trdy & devSel) begin
		          ld_getdata  <= ack64 | burstDataCount[0];
			  if (lastWord) begin
			     curState <= PCI_IDLE;
			     Xrun     <= 1'b0;
			  end else begin
			     curState <= PCI_BURST;
			  end
			  if (parErr) pciStatus[DATA_PARITY_ERROR] <= 1;
		    end else begin
		          ld_getdata  <= 0;
			  curState <= PCI_BURST;
			  if (parErr) pciStatus[DATA_PARITY_ERROR] <= 1;
		    end
		 end
		 
		 PCI_MABRT : begin
		    //$display("In %m: PCI Master Abort Detected!");
		    curState <= PCI_IDLE;
		    Xrun     <= 1'b0;
		 end
		 
		 PCI_TABRT : begin
		    if (retryAccess && (pciStatus==STATUS__TARGET_RETRY))
		       begin
			  curState <= PCI_REQ;
			  pciStatus <= STATUS__NORMAL;
			  req <= 1;
		       end
		    else begin
		       curState <= PCI_IDLE;
		       Xrun     <= 1'b0;
		    end
		 end
		 
		 PCI_CYCABRT : begin
		    //$display("In %m: PCI Cycle Abort Detected!");
		    if (retryAccess && (pciStatus==STATUS__TARGET_RETRY))
		       begin
			  curState <= PCI_REQ;
			  pciStatus <= STATUS__NORMAL;
			  req <= 1;
		       end // if (retryAccess && (pciStatus==`PCI_RETRY))
		    else if (enableAutoRetry && ((pciStatus == STATUS__DATA_DISCONNECT_B) || (pciStatus == STATUS__DATA_DISCONNECT_C))) begin
		       curState <= PCI_REQ;
//!!!  Need to make this work somehow  it sure looks broken
		       accessAddress <= {accessAddress[63:2],2'b00} + ((burstDataCount - startBurstDataCount) * 4);
		       pciStatus <= STATUS__NORMAL;
		       req <= 1;
		       startBurstDataCount <= burstDataCount;
		    end
		    else begin
		       curState <= PCI_IDLE;
		       Xrun     <= 1'b0;
		    end
		 end

		 default : curState <= PCI_IDLE;
		 
	       endcase
	    end
      end // always @ (curState or doSomething or grant or reset_l or trdy or...

`endprotect
endmodule
