
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`timescale 1 ns / 100 ps 

module carbonx_pci_target ( 
		  clk, 
		  reset_l, 
		  frame_l, 
		  irdy_l, 
		  devsel_l, 
		  trdy_l, 
		  stop_l,
		  idsel,
		  par,
		  par64,
		  ad,
		  cbe,
		  perr_l,
		  serr_l,
		  req64_l,
		  ack64_l
		  ); 

   
   input clk,         // PCI bus clock 
	 reset_l;     // system reset 

   inout
	 frame_l,     // PCI frame flag, driven by master device 
	 irdy_l;      // PCI irdy flag, driven by master device to acknowledge data 
   
   
   inout [7:0]  cbe;   // PCI command/byte enables, driven by master 
   
   inout [63:0] ad;   // PCI address/data 
   inout 	par;  // PCI parity
   inout 	par64;
   
   input	idsel;       // device select flag for configuration access 

   output 	devsel_l,   // PCI devsel, driven by slave to claim address 
		trdy_l,     // PCI trdy, driven by slave to acknowledge data 
		stop_l;     // PCI stop, driven by slave to stop data trf

   output 	perr_l;     // Data parity error
   output 	serr_l;     // Address parity error

   input 	req64_l;
   output 	ack64_l;

   

`protect
// carbon license crbn_vsp_exec_xtor_pci

`include "carbonx_pci_defines.vh"

  // We need this handful of signals to properly configure the
  // transactor interface.
  parameter    
               ClockId   =   1,
               DataWidth =  64,
               DataAdWid =   9, // Max is 15
               GCSwid    =  32, // Minimum is 1.
               PCSwid    =  32; // Minimum is 1.

`define XIF_GCS_DEFAULT_DATA 'h10ff00
  //
  // Include XIF.
  //

`include "xif_core.vh"

`define XIF_GCS_DEFAULT_DATA 0

   /************************** 
    ** Internal registers    ** 
    **************************/ 
   reg 		devsel,       // address claim 
		trdy,         // data cycle ack 
		stop,         // stop data trf
		mem_rd,       // indicates access is a read 
		mem_rd_1,     // indicates access is a read , used for driving AD lines
		mem_wr,       // indicates access is a write 
		cfg_wr,       // indicates access is configuration cycle write 
		cfg_rd,       // indicates access is configuration cycle read 
		simside_in_range; // indicates access in range in C-based transactor mode 
   
   reg [7:0] 	word_count;  
   reg [3:0] 	pci_slv_state; // state machine register 
   
   reg [63:0] 	start_address;  // original PCI address
		// actually the offset from the beginning of the burst...
   reg [20:0] 	current_address;  // incrementing version through burst (2M max burst??)
   
   reg [7:0] 	cntr;

   reg 		irdy_l_del, trdy_del, mem_wr_del;
   reg [63:0] 	ad_del;
   reg [7:0] 	cbe_del;
   reg 		perr, serr;
   reg 		frame_del, frame_dd, frame_ddd;
   
   reg 		par_en, par_en_d;
   reg 		rd_par;
   reg 		addr_in_range_del;
   reg 		addr_in_range_del1;

   reg 		check_addr_par;
   reg 		check_addr_par_del;

   reg 		ack64, ack64_del;
   reg 		req64;
   reg 		req64_del;
   reg 		rd_par_1;
   
   wire [7:0] 	num_wait_cycles;
   wire [3:0] 	resp_type;
   wire [7:0] 	num_data_ph;
   wire 	insert_wait, cause_perr, use_32bit, cside_waits;
   wire [3:0] 	DevselCount;

   reg [31:0] 	last_op;        
   wire         wait_for_cside = (cside_waits && XIF_get_operation == BFM_NOP);
   wire [31:0] 	simside_rd_data;  // return data from C when operating as transactor 
   wire [31:0] 	simside_rd_data_1;

   reg [3:0] 	devsel_cntr;
   wire 	addr_in_range; // indicates access is to this memory 
   
   wire [31:0] 	cfg_access_addr = {start_address[11:3], current_address[2]};   // address for configuration access 
   reg [31:0] 	cfg_base_reg;
   reg [31:0] 	cfg_limit_reg;
   reg [31:0] 	cfg_misc_reg;
   reg [31:0] 	slavestatus;
   reg [31:0] 	wbe, ldwbe, last_ad;
   reg [3:0]    command;	// hold command through an entire bus cycle   
   wire         wrt_cmplt;      // indicate a write completion transaction
   reg          xrun;
   parameter 	PCI_IDLE     = 4'h0, 
		PCI_GNT      = 4'h1, 
		PCI_RECV_DAC = 4'h2, 
		PCI_ADDR     = 4'h3, 
		PCI_CLAIM    = 4'h4, 
		PCI_DATA     = 4'h5, 
		PCI_BURST    = 4'h6, 
		PCI_MABRT    = 4'h7, 
		PCI_WAIT     = 4'h8,
		PCI_WTST     = 4'h9,
		PCI_TABRT    = 4'ha,
		PCI_TABRT_1  = 4'hb,
                DEVSEL_WAIT  = 4'hc; 
      

   /************************** 
    ** Initialize registers ** 
    **************************/
   initial begin
      last_op = BFM_NXTREQ;
   end
   
   /************************** 
    ** continuous assigns    ** 
    **************************/ 
		//!!! fix this obtuse equation
   assign #1 ack64_l = (req64) ? ( ((mem_rd || mem_wr) & addr_in_range) ? !ack64 : 1'bz) : 1'bz;
   assign #1 devsel_l = (((mem_rd || mem_wr) & addr_in_range) || cfg_wr || cfg_rd) ? !devsel : 1'bz; 
   assign #1 trdy_l = (((mem_rd || mem_wr) & addr_in_range) || cfg_wr || cfg_rd) ? !trdy : 1'bz; 
   assign #1 stop_l = (((mem_rd || mem_wr) & addr_in_range) || cfg_wr || cfg_rd) ? !stop : 1'bz; 
   assign #1 par = par_en_d ? rd_par : 1'bz;
   assign #1 perr_l = (mem_wr_del & (addr_in_range_del | addr_in_range_del1))  ? !perr : 1'bz;
   assign #1 serr_l = ((frame_ddd & !frame_dd) || check_addr_par_del)  ? !serr : 1'bz;

   assign addr_in_range = simside_in_range; 

   assign #1 ad[31:0] = (devsel & (cfg_rd | (mem_rd_1 & addr_in_range))) ? simside_rd_data : 32'hzzzzzzzz; 

   assign #1 ad[63:32] = (devsel & req64) ? ((devsel & mem_rd_1 & addr_in_range) ? simside_rd_data_1 : 32'bz) : 32'bz;

   assign #1 par64 = (par_en_d & req64_del) ? rd_par_1 : 1'bz;

   assign #1 cbe     = reset_l ? 8'hzz : 8'hff;
   assign #1 irdy_l  = reset_l ? 1'bz  : 1'b1;
   assign #1 frame_l = reset_l ? 1'bz  : 1'b1;


   // Note : ad is driven one clock earlier than it should be. But it dosent cause
   // any simulation problem, as trdy is not asserted during that clock.
   
   
   always @ (posedge clk) begin

	 irdy_l_del <= irdy_l;
	 trdy_del   <= trdy;
	 mem_wr_del <= mem_wr;
	 ad_del     <= ad;
	 cbe_del    <= cbe;
	 frame_del  <= frame_l;
	 frame_dd   <= frame_del;
	 frame_ddd  <= frame_dd;
	 addr_in_range_del1 <= addr_in_range_del;
	 addr_in_range_del <= addr_in_range;
	 check_addr_par_del <= check_addr_par;
	 req64_del  <= req64;
   end

/*******************************
 *
 *  RAM Array code
 *
 *******************************/
wire[7:0]  bemux1 = ~{ack64 ? cbe[7:4]:cbe[3:0], cbe[3:0]}; // Handle 32/64 bit cases
wire[31:0] bemux  = {4{bemux1}};                          // just replicate above 4 times
wire[63:0] wdat_data, wdat_mdata, rdat_data;
wire[8:0]  wdat_addr, rdat_addr;
wire[3:0]  wdat_bump;
wire wdat_we;

assign
    BFM_clock         = clk;   

assign #1
    BFM_reset         = ~reset_l,

//  Note the extra bit indicating write completion transaction
    BFM_put_operation = {27'h4000000,  (frame_l & irdy_l & mem_wr),  command},
    BFM_put_address   = mem_wr     ? start_address : start_address + current_address + wdat_bump,
    BFM_put_size      = mem_rd     ? 32'h20  : 
                        ack64_del  ? {21'h0, word_count, 3'b0} : // Reads are always 32 bytes; cvt wrts to bytes
                                     {22'h0, word_count, 2'b0}+4,

// For write data transfers the status is the corresponding BE's
// Since there is space for only 32 bits, the first 24 bytes are reigsters in bits 23:0
// bits 31:24 have the LAST 8 BE's.  Thus, any arbitrary set ob BE's can be conveyed for transfers
// up to 32 bytes long.  For longer transfers, only the first 24 and last 8 can be disabled.  This
// accomodates arbitrarily aligned contiguous transfers.
    BFM_put_status    = mem_wr ? wbe : slavestatus,
    BFM_put_data      = wdat_data,

// Deasserting BFM_xrun initiates a transaction;
// Transactions are initiated at the beginning of every transaction (not DAC's -- the address is accumulated internally)
// as well as at the end of write operations, in order to send the write data up to the C-side
		// & (mem_rd | cfg_wr | cfg_rd)  // Read transaction
    BFM_xrun          = ~((  pci_slv_state == PCI_CLAIM & addr_in_range
	// This second term is a bit tricky: it is looking for the case where a read
	// is transferring the last word of data of a 32 byte block, since a transaction can hold only 32 bytes
			  | ~frame_l & ~irdy_l & trdy & mem_rd & (current_address[4:3]==2'B11) & (current_address[2] | req64)
			  | frame_l & irdy_l & mem_wr) ||  			 // tail end of Write
			  (last_op == BFM_NOP && cside_waits)),
    BFM_put_csdata    = slavestatus,
    BFM_put_cphwtosw  = mem_wr | cfg_wr,   // This will be true for writes
    BFM_put_cpswtohw  = pci_slv_state == PCI_CLAIM,  // This will be true for reads
    BFM_gp_daddr      = mem_wr ? wdat_addr : rdat_addr,
    BFM_put_dwe       = wdat_we,
    BFM_interrupt     = 0,

    wdat_we = !irdy_l & trdy & mem_wr,

// This hack calculates whether to bump current_address.
// this happens whenever we are requesting another block of data
// for a burst read
    wdat_bump         = (pci_slv_state == PCI_CLAIM) ? 4'h0 : 4'h8,

//    This address is used to address the XIF data ram for writes
    wdat_addr = current_address[11:3],

    wdat_data = ack64 ? ad : {ad[31:0], current_address[2] ? last_ad : ad[31:0]},  // handle 32 bit writes by replication

    // This does some cheating if we are inc'ing the address this cycle...
    rdat_addr = (trdy ? current_address[11:3]+(current_address[2] |req64) : current_address[11:3]) & 'h3,
    rdat_data = XIF_get_data;



assign
  simside_rd_data   = mem_rd & ~req64 & current_address[2] ? rdat_data[63:32] :
		      mem_rd                              ? rdat_data[31: 0]  :
		      cfg_access_addr=='h7                ? cfg_base_reg      :
		      cfg_access_addr=='h8                ? cfg_limit_reg     :
		      cfg_access_addr=='h9                ? xrun              :   // Ensure xrun not optimized away
							    cfg_misc_reg       ,  // All other addresses get this loc
// This is ONLY used for 64 bit memory reads -- cfg accesses are always 32 bit
  simside_rd_data_1 = rdat_data[63:32];


// Tear apart the bits of the cs register & get_status word & OR together
assign
	 num_wait_cycles = XIF_get_csdata[7:0]   | XIF_get_status[7:0],  
	 num_data_ph     = XIF_get_csdata[15:8]  | XIF_get_status[15:8], 
	 resp_type       = XIF_get_csdata[19:16] | XIF_get_status[19:16],
         DevselCount     = XIF_get_csdata[23:20] | XIF_get_status[23:20],
         cside_waits     = XIF_get_csdata[28]    | XIF_get_status[28],
         cause_perr      = XIF_get_csdata[29]    | XIF_get_status[29],
         use_32bit       = XIF_get_csdata[30]    | XIF_get_status[30],
	 insert_wait     = XIF_get_csdata[31]    | XIF_get_status[31];   


   // This task is called to store a configuration-space write access 
   // to the C model
always @(posedge clk) begin
	 if (!reset_l) begin
	    cfg_base_reg  = 0;
	    cfg_limit_reg = 'h00200000;

	 end else if (trdy & cfg_wr) begin	    
`ifdef PCISLVDEBUG
$display("%m  Writing -- A: %h   D: %h", cfg_access_addr, ad);
`endif
	    case (cfg_access_addr)
	    'h7: cfg_base_reg  = ad[31:0];
	    'h8: cfg_limit_reg = ad[31:0];
	    default: cfg_misc_reg = ad[31:0];
	    endcase
	 end

	
end // always @ (posedge clk)
   

  
   /************************** 
    ** PCI slave state m/c   ** 
    **************************/ 
   always @(posedge clk) begin 
	 par_en_d <= par_en;
	 
	 if (!reset_l) begin 
	       req64            <= 0;
	       ack64            <= 0;
	       ack64_del        <= 0;
	       devsel           <= 0; 
	       trdy             <= 0; 
	       stop             <= 0; 
	       mem_rd           <= 0; 
	       mem_rd_1         <= 0; 
	       mem_wr           <= 0; 
	       cfg_wr           <= 0; 
	       cfg_rd           <= 0; 
	       word_count       <= 0;
	       check_addr_par   <= 0;
	       simside_in_range <= 1;
	       current_address  <= 0;
	       start_address    <= 0;
	       pci_slv_state    <= PCI_IDLE;
//	       simside_rd_data  <= 0;
//	       simside_rd_data_1<= 0;
               devsel_cntr      <= 0;
	       cntr             <= 0;
	       slavestatus      <= 0;
	       ldwbe            <= 0;
	       wbe              <= 0;
	       last_op          <= BFM_NXTREQ;
	 end else begin 
		xrun     <= BFM_xrun;
                last_op  <= XIF_get_operation;

	       ack64_del <= ack64;  // just for size computation cycle after last trdy

		// wbe, ldwbe is a mechanism to save away the first batch of byte enables
		// to be sent up to the C-side for writes.  We save the first 24 (28) byte enables
		// and the last 8 (4) by copying the live cbe into wbe in the appropriate
		// bit positions.  ldwbe tracks which bits are to be loaded during trdys		
	       if (pci_slv_state == PCI_CLAIM) begin
		  ldwbe <= (req64 & ~use_32bit) ? 8'hff :   // 64 bit
		            current_address[2]  ? 8'hf0 :   // Unaligned 32 bit
						  8'h0f ;   // Normal 32 bit
		  wbe   <= 0;
	       end else if (trdy & (word_count < (ack64 ? 4 : 8))) begin
		  ldwbe <= ldwbe << (ack64 ? 8 : 4);
	       end

	       if (trdy) begin
		  wbe <= (bemux & ldwbe)  | (wbe & ~ldwbe);
		  last_ad <= ad[31:0];
	       end

		// set on an error, clear on report to C-side
	       slavestatus  <= {30'h0, ({serr,perr} | (BFM_xrun ? slavestatus[1:0] : 2'h0))};

	       if ((cfg_rd && (pci_slv_state != PCI_IDLE)) || (mem_rd_1 && addr_in_range)) begin
		     par_en <= 1'b1;
		     rd_par <= ^{ad[31:0],cbe[3:0]};
		     rd_par_1 <= ^{ad[63:32],cbe[7:4]};
	       end else begin
		  par_en <= 1'b0;
	       end
	       
	       case (pci_slv_state) 
		 
	       PCI_IDLE : begin 
		       if (!frame_l) begin 
			     current_address <= {10'b0, ad[2], 2'b0}; // just keeps track of H/L word
			     start_address   <= {ad[31:3], 3'b000};
			     req64           <= ~req64_l;
			     command         <= cbe[3:0];

			     if (cbe[3:0] == 4'hd)                                //Dual address cycle
				pci_slv_state <= PCI_RECV_DAC;
			     else if ((cbe[3:0] == 4'hb) && idsel) begin          // Config Write
				   cfg_wr <= 1;
				   pci_slv_state <= PCI_CLAIM;
		             end else if ((cbe[3:0] == 4'ha) && idsel) begin      // Config Read
				   cfg_rd <= 1;
				   pci_slv_state <= PCI_CLAIM;
			     end else if ((cbe[3:0] == 4'h6) || (cbe[3:0] == 4'hC)
				       || (cbe[3:0] == 4'hE)) begin               // Memory Read
				   mem_rd   <= 1;
				   mem_rd_1 <= 1;
				   pci_slv_state <= PCI_CLAIM;
			     end else if ((cbe[3:0] == 4'h7) || (cbe[3:0] == 4'hF)) begin  // Memory Write
				   mem_wr <= 1;
				   pci_slv_state <= PCI_CLAIM;
			     end

			     simside_in_range <= (cfg_base_reg[31:0] <= ad[31:0]) && (ad[31:0] <= cfg_limit_reg[31:0]);

		       end else begin
			     devsel           <= 0;
			     req64            <= 0;
			     ack64            <= 0;
			     trdy             <= 0;
			     stop             <= 0;
			     mem_rd           <= 0;
			     mem_rd_1         <= 0;
			     mem_wr           <= 0;
			     cfg_wr           <= 0;
			     cfg_rd           <= 0;
			     word_count       <= 0;
			     check_addr_par   <= 0;
			     simside_in_range <= 0;
			     current_address  <= 0;
			     pci_slv_state    <= PCI_IDLE;
		       end
		 end // case: PCI_IDLE

		 PCI_RECV_DAC :
		    begin
 		       simside_in_range <= ({ad[31:0],start_address[31:0]} >= cfg_base_reg)
                                        && ({ad[31:0],start_address[31:0]} <= cfg_limit_reg);
		       start_address[63:32]   <= ad[31:0];
		       check_addr_par <= 1;
		       pci_slv_state <= PCI_CLAIM;

		       if ((cbe[3:0] == 4'h6) || (cbe[3:0] == 4'hC) || (cbe[3:0] == 4'hE))  begin
			  mem_rd   <= 1;
			  mem_rd_1 <= 1;
		       end else if ((cbe[3:0] == 4'h7) || (cbe[3:0] == 4'hF)) begin // Memory Write
			  mem_wr   <= 1;
		       end
		    end


		 
		 PCI_CLAIM : 
		    if (addr_in_range || cfg_wr || cfg_rd)  begin 
		       check_addr_par <= 0;

			if (DevselCount < 2) begin
		       	   devsel <= 1; 
		           ack64  <= req64 & ~use_32bit;
		       	   case(resp_type)
			 	RETRY  : pci_slv_state <= PCI_TABRT;
			 	TABRT, 
			 	DISCNT : begin
			    		    if (insert_wait || wait_for_cside)
					    	 pci_slv_state <= PCI_WAIT; 
			                    else if (word_count == num_data_ph) 
			       			   pci_slv_state <= PCI_TABRT_1;
			    		    else
						    pci_slv_state <= PCI_DATA;
			 		  end
			 	NORMAL : begin 
					    if (insert_wait || wait_for_cside) pci_slv_state <= PCI_WAIT;
			 		    else  pci_slv_state <= PCI_DATA;
					  end
			 	default : pci_slv_state <= PCI_DATA;
		           endcase
		       

		       	   // Assert stop for RETRY
		           stop <= (resp_type == RETRY) ? 1 : 0;
		       
		           // Assert trdy for data transfer
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				   (word_count != num_data_ph))) && !irdy_l && !(insert_wait || wait_for_cside))
			      trdy <= 1;
		       
		       	   // Increment word count if data transfer takes place
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				    (word_count != num_data_ph)) ) && !irdy_l && !(insert_wait || wait_for_cside))
			      word_count <= word_count + 1;
		       
		           // Load counter with the num of initial wait states reqd
		           cntr <= num_wait_cycles;
			  end // if (DevselCount==1)
		        else  
			  begin
				devsel <= 0;
				devsel_cntr <= DevselCount;
				pci_slv_state <= DEVSEL_WAIT;
			  end // else: !if(DevselCount == 1)
		    end // if (addr_in_range) 
		 else 
		    begin 
		       ack64          <= 0;
		       devsel         <= 0; 
		       mem_rd         <= 0; 
		       mem_rd_1       <= 0; 
		       mem_wr         <= 0; 
		       check_addr_par <= 0;
		       pci_slv_state  <= PCI_MABRT; 
		    end // else: !if(addr_in_range) 
		 

		DEVSEL_WAIT : begin
			devsel_cntr <= devsel_cntr - 1;
			if (devsel_cntr == 1)
                          begin
		       	   devsel <= 1; 
		           ack64  <= req64 & ~use_32bit;
		       	   case(resp_type)
			 	RETRY  : pci_slv_state <= PCI_TABRT;
			 	TABRT, 
			 	DISCNT : begin
			    		    if (insert_wait) pci_slv_state <= PCI_WAIT; 
			                    else if (word_count == num_data_ph) 
			       			   pci_slv_state <= PCI_TABRT_1;
			    		    else pci_slv_state <= PCI_DATA;
`ifdef PCISLVDEBUG
					    $display("Next pci_slave_state = %x\n", pci_slv_state);
`endif
			 		  end
			 	NORMAL : begin 
					    if (insert_wait) pci_slv_state <= PCI_WAIT;
			 		    else  pci_slv_state <= PCI_DATA;
`ifdef PCISLVDEBUG
				            $display("Current pci_slave_state=PCI_CLAIM, next=%x\n", 
						      pci_slv_state); 
`endif
					  end
			 	default : pci_slv_state <= PCI_DATA;
		           endcase
		       

		       	   // Assert stop for RETRY
		           stop <= (resp_type == RETRY) ? 1 : 0;
		       
		           // Assert trdy for data transfer
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				   (word_count != num_data_ph))) && !irdy_l && !insert_wait)
			      trdy <= 1;
		       
		       	   // Increment word count if data transfer takes place
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				    (word_count != num_data_ph)) ) && !irdy_l && !insert_wait)
			      word_count <= word_count + 1;
		       
		           // Load counter with the num of initial wait states reqd
		           cntr <= num_wait_cycles;
			  end // if (devsel_cntr==1)
		        else  
			  pci_slv_state <= DEVSEL_WAIT;
		    end // DEVSEL_WAIT


		 PCI_WAIT : begin
		    cntr <= cntr - 1;

		    if (frame_l && irdy_l)
		       begin
			  req64    <= 0;
			  ack64    <= 0;
			  trdy     <= 0;
			  devsel   <= 0;
			  stop     <= 0;
			  mem_rd_1 <= 0;
			  pci_slv_state <= PCI_IDLE;
		       end
		    else if ( (insert_wait && cntr == 1) || (!insert_wait && XIF_get_operation == BFM_NXTREQ))     
		       begin
			  case(resp_type)
			  TABRT, 
			  DISCNT   : begin
			     if (word_count == num_data_ph)
				  pci_slv_state <= PCI_TABRT;
			     else pci_slv_state <= PCI_DATA;
			  end
			  NORMAL   : pci_slv_state <= PCI_DATA;
			   default  : pci_slv_state <= PCI_DATA;
			  endcase
			  
			  // Assert trdy if next state is data transfer
			  trdy <= ( (resp_type == NORMAL) | 
				   ((resp_type != NORMAL) && (word_count != num_data_ph)) );

			  // Deassert devsel if target abort and byte count is satisfied
			  if ((resp_type == TABRT) && (word_count == num_data_ph)) begin
			     devsel <= 0;
			     ack64  <= 0;
			  end

			  // Assert stop if targetabort/retry/discnt and byte count is satisfied
			  stop <= ((resp_type != NORMAL) && (word_count == num_data_ph));
		       end
		    else // (cntr != 1)
		       pci_slv_state <= PCI_WAIT;
		 end
		 
		 PCI_DATA : begin 
		    if (!frame_l ) begin
	       	       if (insert_wait || irdy_l) begin
		          cntr <= num_wait_cycles;
			  if(insert_wait)
			    pci_slv_state <= PCI_WAIT;
			  else
			    pci_slv_state <= PCI_WTST;
			     trdy <= 0;
			     if ( (((resp_type != NORMAL) && (word_count != num_data_ph)) || 
				   (resp_type == NORMAL)) && !irdy_l & ((trdy & mem_wr) | mem_rd)) begin
				      if (req64) 
					 current_address <= current_address + 8;
				      else       
					 current_address <= current_address + 4;
				      word_count <= word_count + 1;
			     end // if ( (((resp_type != NORMAL) && (word_count != num_data_ph)) ||...

		       end else begin
			     case(resp_type)
			     TABRT,   
			     DISCNT  : begin
				if (word_count == num_data_ph)
				   pci_slv_state <= PCI_TABRT;
				else
				   pci_slv_state <= PCI_DATA;
			     end
			     NORMAL    : begin
				pci_slv_state <= PCI_DATA;
			     end
			     default  : pci_slv_state <= PCI_DATA;
			     endcase
			     

			     // If normal data trf, or (abort and data count not satisfied)
			     if ( (((resp_type != NORMAL) && (word_count != num_data_ph)) || 
				   (resp_type == NORMAL)) && !irdy_l & ((trdy & mem_wr) | mem_rd)) begin 
				   trdy <= 1;
				   pci_slv_state <= PCI_DATA;
				   if (req64) 
				      current_address <= current_address + 8;
				   else       
				      current_address <= current_address + 4;
				   
				   word_count <= word_count + 1;
			     end else begin
				trdy <= 0;
				if ((resp_type == TABRT) && (word_count == num_data_ph)) begin
				   devsel <= 0;
				   ack64  <= 0;
				end
				stop   <= ((resp_type != NORMAL) && (word_count == num_data_ph)) ? 1 : 0;
			     end
		       end
		    end else if (frame_l) begin // This is bogus!!!
			  if (irdy_l) 
			     $display(" Frame and Irdy deasserted before Trdy is asserted");
			  req64 <= 0;
			  ack64 <= 0;
			  trdy <= 0;
			  devsel <= 0;
			  stop <= 0;
			  mem_rd_1 <= 0;
			  par_en <= 0;
			  check_addr_par <= 0;
			  pci_slv_state <= PCI_IDLE; 
		    end else begin  // if (frame_l & !irdy_l) //!!! old comment
			  pci_slv_state <= PCI_DATA; 
		    end
		 end // case: PCI_DATA 
		 
		 
		 PCI_WTST : begin
		    
	    	    // pci_get_wait_state(1); // NEW-AG

		    if (frame_l && irdy_l) begin
			  pci_slv_state <= PCI_IDLE;
			  req64 <= 0;
			  ack64 <= 0;
			  trdy <= 0;
			  devsel <= 0;
			  stop <= 0;
			  mem_rd_1 <= 0;
			  par_en <= 0;
			  check_addr_par <= 0;
		    end else if (insert_wait || irdy_l) begin
		       pci_slv_state <= PCI_WTST;
		       trdy <= 0;
		    end else begin
			  
			  case(resp_type)
			    TABRT,
			    DISCNT  : begin
			       if (word_count == num_data_ph)
				  pci_slv_state <= PCI_TABRT;
			       else pci_slv_state <= PCI_DATA;
			    end
			    NORMAL    : pci_slv_state <= PCI_DATA;
			    default  : pci_slv_state <= PCI_DATA;
			  endcase
			  
			  
			  trdy <= ( ((resp_type != NORMAL) && (word_count != num_data_ph)) || 
				    (resp_type == NORMAL)) ? 1 : 0;
			  if ((resp_type == TABRT) && (word_count == num_data_ph)) begin
			     devsel <= 0;
			     ack64  <= 0;
			  end
			  stop   <= ((resp_type != NORMAL) && (word_count == num_data_ph)) ? 1 : 0;
//!!!
			  if (trdy) begin
			     current_address <= ( ((resp_type != NORMAL) && (word_count != num_data_ph))
					        || (resp_type == NORMAL) ) ? ((req64 & !use_32bit) ? current_address + 8 :
									   current_address + 4) : current_address;
			  end
			     word_count  <= ( ((resp_type != NORMAL) && (word_count != num_data_ph)) || 
					       (resp_type == NORMAL) ) ? word_count  + 1 : word_count ;
		    end
		 end
		 
		 
		 PCI_MABRT : begin
		    if (frame_l) 
		       begin 
			  par_en <= 0;
			  check_addr_par <= 0;
			  pci_slv_state <= PCI_IDLE; 
			  req64 <= 0;
			  mem_rd_1 <= 0;
		    end else begin 
			  pci_slv_state <= PCI_MABRT; 
		    end // else: !if(frame_l) 
		 end
		 
		 PCI_TABRT : begin
		    if (frame_l) begin
			  ack64 <= 0;
			  req64 <= 0;
			  trdy <= 0;
			  stop <= 0;
			  devsel <= 0;
			  mem_rd_1 <= 0;
			  par_en <= 0;
			  check_addr_par <= 0;
			  pci_slv_state <= PCI_IDLE;
		    end else 
		       pci_slv_state <= PCI_TABRT;
		 end
		 
		 PCI_TABRT_1:begin
		    stop <= 1;
		    if (resp_type == TABRT) begin
		       devsel <= 0;
		       ack64  <= 0;
		    end
		    pci_slv_state <= PCI_TABRT;
		 end
		 
		 
		 default : begin
		    par_en <= 0;
		    check_addr_par <= 0;
		    pci_slv_state <= PCI_IDLE; 
		 end
	       endcase // case(pci_slv_state) 

	    end // else: !if(!reset_l) 

	 if (!irdy_l_del & trdy_del & mem_wr_del)
	    perr <= (^{ad_del[31:0] , cbe_del[3:0] , par}) |
		     (^{ad_del[63:32] , cbe_del[7:4] , par64} && req64_del);
	 else perr <= 0;
	 if ((!frame_del & frame_dd) || check_addr_par)
	    serr <= ^{ad_del[31:0] ,par, cbe_del[3:0]};
	 else
	    serr <= 0;
      end // always @ (posedge clk) 
   
   


`endprotect
endmodule // PciSlave
