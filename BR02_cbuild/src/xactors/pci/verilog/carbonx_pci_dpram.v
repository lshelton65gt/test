
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module  carbonx_pci_dpram (
	Aaddress,
	Adatain,
	Adataout, 
	Awe,
	Aen,
	Ack,
	
	Baddress,
	Bdatain,
	Bdataout,
	Bwe,
	Ben,
	Bck
    );

parameter
   DataWidth = 32,
   DataAdWid =  9,
   DataDepth = 512;



input[DataAdWid-1:0]
	Aaddress;
input[DataWidth-1:0]
	Adatain;
output[DataWidth-1:0]
	Adataout;
input
	Awe,
	Aen,
	Ack;
	
input[DataAdWid-1:0]
	Baddress;
input[DataWidth-1:0]
	Bdatain;
output[DataWidth-1:0]
	Bdataout;
input
	Bwe,
	Ben,
	Bck;

   reg [DataWidth-1:0] ramarray[0:DataDepth-1];

`protect
 // carbon license crbn_vsp_exec_xtor_pci

   reg [DataWidth-1:0] 
		       Adataout,
		       Bdataout;

always @(posedge Ack) begin
   if(Aen) begin
      if (Awe) begin
         ramarray[Aaddress] <= Adatain;
         Adataout           <= Adatain;
      end else begin
         Adataout           <= ramarray[Aaddress];
      end
   end
end


always @(posedge Bck) begin
   if(Ben) begin
      if (Bwe) begin
         ramarray[Baddress] <= Bdatain;
         Bdataout           <= Bdatain;
      end else begin
         Bdataout           <= ramarray[Baddress];
      end
   end
end

`endprotect

endmodule
