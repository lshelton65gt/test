//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
      There are two arbitration methods here.
      Variable fairness controls the priority on the bus.
      fairness = FALSE  => Fixed priority;
      fairness = TRUE   => Fairness: Bus masters get control 
                               on a cycle to cycle basis.

      Fixed priority dictates that agent 0 has the highest priority
      while agent 7 has the lowest. During the arbitration cycle, 
      the agent with the highest priority will always win the bus.
      This scheme may lock out certain low priority agents on the
      PCI bus.
*/


/******
  Delay values are meaningless due to registered output on grant_out_l, so 
  the original intent was lost.  However, this is OK since Carbon simulations
   do not support delays. I have left the delays in place to show the original intent
  and for use in other simulators
 ******/

`timescale 1ns/1ns

module carbonx_pci_arbiter (
		    grant_out_l,
		    req_ ,
		    frame_  ,    // PCI Bus frame, indicates 
		                 // bus being driven.
		    irdy_  ,     // PCI Bus master ready
		    reset_l ,
		    clock,
		    trdy_l,
		    devsel_l,
		    stop_l
		    );

   parameter TRUE = 1;
   parameter FALSE = 0;

   
   output [7:0]    grant_out_l;
   input  [7:0]    req_;
   input 	   frame_, irdy_;
   input 	   reset_l, clock ;
   input 	   trdy_l, devsel_l, stop_l;


`protect
// carbon license crbn_vsp_exec_xtor_pci

   parameter 	   nobody   = 99 ;  // ID of requestor when no request pending
   //                park_id  =  1  ;   // Responsible for drive when
                                        // no requests are pending.

   reg      [7:0]   grant_;
   reg 		    static_bus ;

   reg 		    fairness ;
   reg 		    pending;
   reg 		    one_shot;
   reg 		    last_frame_;
   reg 		    frame_dd_;
   reg 		    last_irdy_;
   reg 		    last_bus_idle;
   reg [7:0] 	    last_req_in;
   reg [31:0] 	    park_id;
   integer 	    i, j;
   integer 	    prev_line;
   integer 	    cycle_start_counter;

   wire 	    bus_idle ;
   wire [7:0] 	    req_;
   reg [7:0] 	    req_in;

   integer 	    bus_timer, noGrantClocks;
   integer 	    bus_timeout;    // max no of bus cycles
   integer 	    curr_owner, highest_req, gntAssertDelay ;
   integer 	    gntDeassertTime;

   wire [7:0] 	    grant_out_l;
   reg [7:0] 	    r_grant_out_l;
   reg 		    pcix_mode;

   reg [2:0] grantState;


   //  G r a n t   O u t p u t
   // -------------------------
   //
   // The following code makes the delays on grant meaningless,
   //  since only the registered (r_grant_out_l) values are driven out.
   // To make delays meaningful change this to a delayed assignment
   // such as:  assign #5 r_grant_out_l = grant_
   //
   always @(posedge clock)
      r_grant_out_l <= grant_;

   //  assign #1 grant_out_l = (~reset_l)  ? 8'bz : r_grant_out_l ;
   //
   assign grant_out_l = (~reset_l)  ? 8'hff : r_grant_out_l ;
      
   always @ (posedge reset_l)
      begin
	 pcix_mode <= ~(trdy_l & devsel_l & stop_l);
      end 

   initial begin
      pcix_mode = 1;
      park_id = 3;
      fairness = TRUE;
      grant_  =  8'hff  ;
      curr_owner  = park_id ;
      highest_req = nobody ;
      prev_line      = park_id;
      pending = FALSE;
      one_shot = FALSE;
      cycle_start_counter = 0;

      bus_timer   = 512 ;      // maximum of 512 cycles for master.
      bus_timeout = 256;
      noGrantClocks = 1;
      
      `ifdef PCI66     
      gntAssertDelay  = 3;
      gntDeassertTime = 3;
      `else
      gntAssertDelay  = 5;
      gntDeassertTime = 5;
      `endif
	 
      static_bus = TRUE  ;
      grantState = 0;
   end



   /* 
    We capture the Current bus owner ID at the negedge of FRAME_
    */
   
   always @(negedge last_frame_) begin
      if      (~grant_[0])  curr_owner = 0 ;
      else if (~grant_[1])  curr_owner = 1 ;
      else if (~grant_[2])  curr_owner = 2 ;
      else if (~grant_[3])  curr_owner = 3 ;
      else if (~grant_[4])  curr_owner = 4 ;
      else if (~grant_[5])  curr_owner = 5 ;
      else if (~grant_[6])  curr_owner = 6 ;
      else if (~grant_[7])  curr_owner = 7 ;
      
      cycle_start_counter = 0;
      end  // capture the current bus owner's ID

   /* 
    Detecting bus idle if
    1. frame not active and both irdy and trdy are active.
    This implies last data word being transferred.
    
    2. Bus is completely free.
    */
   
   //   assign bus_idle =  (frame_ && irdy_ ) ;
   assign bus_idle =  last_frame_ ;


   reg    cycle_started;
   reg    multi_reqs;
   reg    noGrantsPending;
   
always @(posedge clock or negedge reset_l) 
  begin
  //  #1;

  if (~reset_l)
    begin
     grant_      =  8'hff  ;
     curr_owner  =  nobody ;
     highest_req =  nobody ;
     bus_timer   =  bus_timeout ;  // maximum of 512 cycles for master.
    end
  else
    begin
      /*
       * if bus idle or cycle started or 
       * (multiple reqs pending and cycle started)
       * and not any pending grants
       * Determine the highest level of request that is pending.
       */


      if (   (~last_frame_ & (frame_dd_ != last_frame_))
           | (cycle_start_counter > 5)
           | (~req_in[curr_owner])
          )
          begin 
	     pending = FALSE;
	  end

         
      if (bus_idle && pending)
        begin
	   cycle_start_counter = cycle_start_counter + 1;   
        end

       cycle_started   =  ~bus_idle && last_bus_idle;
       multi_reqs      =  req_in != last_req_in;
       noGrantsPending = ~pending || bus_idle && one_shot ; 
   
       if (noGrantsPending && (bus_idle || cycle_started || multi_reqs ))
        begin
           if (fairness) ARBfairness ;
	   else          NOTfairness ;
		
           grantState = 0;  // start new grant in state 0
    
	   if (static_bus) static_grant ;
           else	           pulse_grant ;             
        end    // noGrantsPending ...       

   end         // (else reset_l)
       
 end           // always @(posedge clock)



   
   always @(posedge clock) begin
      req_in  <= ~req_ ;
      last_bus_idle <= bus_idle;
      last_req_in <= req_in;
      last_frame_ <= frame_;
      last_irdy_ <= irdy_;
      frame_dd_ <= last_frame_;
   end


   
   /* ================================================================
    
    There are three cases as follows :-
    
    1.  Bus Free and no request pending. In this case, the bus
    will be parked on parameter park ID.

    2.  Bus free and new requests exist.
    
    1. Bus was granted to a lower priority agent, which has
    not yet driven the bus. In this case, the higher priority
    agent may be granted the bus. One clock delay is
    required between the de-assertion of a grant and 
    assertion of a new one.
    
    3. Bus Busy but it was granted in the previous cycle only,
    in this case we have to wait because there must be atleast
    one cycle delay between the de-assertion of a grant and
    assertion of a new one.
    
    ================================================================ */


   // ------------------------- static_grant ---------------------------
     
   integer grantCounter;
   initial grantCounter = 0; 

   parameter sgINIT     = 0,
             sgCLKDLY   = 1,
             sgGRANTBUS = 2;
  
   task static_grant ;
    begin
     if (bus_idle)           bus_timer = bus_timeout ;
     else if (bus_timer > 0) bus_timer = bus_timer - 1;
      
       
     case (grantState) 

     sgINIT: begin  // 0       
        if  ((highest_req == nobody) || ~reset_l )
          begin
	     highest_req = park_id ;   // no request pending, park at park_id
	     cycle_start_counter = 0;
	  end
 
        if (bus_idle || (bus_timer != 0))  // idle or (busy and not timed-out)
          begin
             grantState = 2;
             grant_bus ;
          end
        else    //  ~bus_idle && (bus_timer == 0) 
          begin
	     grant_ = 8'hff ;
  	     // repeat (noGrantClocks) @(posedge clock);
             // bus_timer = bus_timeout;          
             grantState = 1;
             grantCounter = noGrantClocks-1;  // -1 to count clock for state transition
	  end
       end       // sgINIT
                   
        
      sgCLKDLY: // 1 
         if (grantCounter <= 0)  // WAIT here noGrantClocks-1 cycles
            begin
              grant_ = 8'hff ;
              bus_timer = bus_timeout;
              grantState = 2; 
              grant_bus ;
           end 
        else  
              grantCounter = grantCounter - 1; 
        
      sgGRANTBUS:  // 2
         begin
          grant_bus ;
         end
      
     endcase

     end 
   endtask		// end of static_grant 




// ---------------------------- grant_bus ----------------------------------   

reg [1:0] gbState;
initial gbState = 0;

task grant_bus ;

case (gbState)
  0: begin
     if (    (curr_owner != highest_req) 
     	  && (    pcix_mode 
               || (bus_idle && ~(&grant_)) )
        )     
       begin
          // #gntDeassertTime;
	grant_ = 8'hff ;                 // Remove ALL grants
          // repeat (1) @(posedge clock);
        gbState = 1;                     // Wait 1 clock
       end
              
     else
       begin
        if (curr_owner != highest_req) ;  // #gntDeassertTime;
	grant_ = 8'hff ;
	  //  #gntAssertDelay;	        
        grant_ = prioEncode(highest_req) ;

	curr_owner = highest_req ;
	if (~bus_idle) 
  	    if (bus_timer > 0) bus_timer = bus_timer - 1 ;

        gbState    = 0; // already 0
        grantState = 0;
       end     // else !(pcix_mode |( bus_idle & ~&grant_))              
        
     end     // gbState == 0

  1: begin       // arrive here after a posedge clk
           
      grant_ = prioEncode(highest_req);
      curr_owner = highest_req ;
      if (~bus_idle) 
	if (bus_timer > 0) bus_timer = bus_timer - 1 ;

      gbState    = 0;
      grantState = 0;
     end                 // gbState == 1

endcase 
endtask		// end of grant_bus.



// ---- Priority Encoder

 function [7:0] prioEncode ;
   input  priority;
   integer priority;
      
   case (priority)       // assert NEW grant
     0 : prioEncode = 8'hfe;
     1 : prioEncode = 8'hfd; 
     2 : prioEncode = 8'hfb; 
     3 : prioEncode = 8'hf7; 
     4 : prioEncode = 8'hef;
     5 : prioEncode = 8'hdf; 
     6 : prioEncode = 8'hbf; 
     7 : prioEncode = 8'h7f; 
     default :	$display (
         "%t %m: ...Going into default .possibly error in Arbiter. hi_req = 0x%h",
			  $realtime, highest_req);
   endcase
 endfunction

 


// ------------------------- pulse_grant ----------------   

   reg [1:0] pgState;
   initial pgState = 0;
   

task pulse_grant ;
  begin
    case (pgState)
    0: begin   
        // #15.0 ;
       if (highest_req == nobody)  highest_req = park_id ;

       if ( (highest_req != park_id) && (highest_req != curr_owner) && bus_idle) 
	 begin
	    curr_owner = highest_req ;
            pgState = 1;
 	    grant_bus ;
	     // repeat (3) @(posedge clock);
	     // #gntDeassertTime;
	 end 
       else  
             if (highest_req == park_id) grant_bus ;
       end  // case 0
   
      1: pgState = 2;     // Cycle 1
      2: pgState = 3;     // cycle 2
      
      3: begin            // cycle 3 -- remove grant (deassert all grants)
           grant_ = 8'hff;
           pgState = 0;
         end
    endcase

   end  
endtask		// end of task pulse_grant

   

   // --------------------------- Highest Priority  ------------------------------
   
   task NOTfairness ;
      begin
	 if (cycle_start_counter > 15) begin  
	    if (~one_shot)
	      $display(
                 "ERROR! %t   Master  %d  might have broken, grant has been given to another agent",
                       $realtime, highest_req);
	    highest_req = nobody;
	    cycle_start_counter = 0;
	 end else begin
	    if (req_in[0]) begin
	       highest_req = 0 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else if (req_in[1]) begin
	       highest_req = 1 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else if (req_in[2]) begin
	       highest_req = 2 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else if (req_in[3]) begin
	       highest_req = 3 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else if (req_in[4]) begin
	       highest_req = 4 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else if (req_in[5]) begin
	       highest_req = 5 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else if (req_in[6]) begin
	       highest_req = 6 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else if (req_in[7]) begin
	       highest_req = 7 ;
	       pending = TRUE;
	       cycle_start_counter = 0;
	    end
	    else
	      highest_req = nobody ;
            
	 end
      end
   endtask   // NOTfairness   


   // --------------------   ARB with FAIRNESS ------------------------------

  //  

   
   task ARBfairness;
      begin
	 if (cycle_start_counter > 15)
           begin
	      if (~one_shot)  $display(
                   "%t   Master  %d  might have broken, grant has been given to another agent",
                         $realtime, highest_req);
	      highest_req = nobody;
	      prev_line = prev_line + 1;
	      cycle_start_counter = 0;
	   end else
             begin

		if (curr_owner == prev_line)  j = prev_line +1;
		else      		      j = prev_line;

		if (|req_in )
                  begin
		  for (i=0; i<8; i=i+1) 
                     begin :next_agent
		        if (j > 7) j = 0;
                        
		        if (grant_[j] && req_in[j]) 
                          begin
			     highest_req = j;
			     prev_line = j;
			     pending = TRUE;
			     cycle_start_counter = 0;
                             // $display("ARBfairness: highest_req = %0h", j);
			     disable next_agent;          // does disable work for Carbon ?
		          end 
                        else
			    j = j + 1;
		     end
		  end else 
		    highest_req = nobody;
	     end
      end
   endtask   

   
`endprotect
endmodule          // end of bus_arbitration module
