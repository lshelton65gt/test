
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`timescale 1 ns / 100 ps 

module carbonx_pci_slave_mem ( 
		  clk, 
		  reset_l, 
		  frame_l, 
		  irdy_l, 
		  devsel_l, 
		  trdy_l, 
		  stop_l,
		  idsel,
		  par,
		  par64,
		  ad,
		  cbe,
		  perr_l,
		  serr_l,
		  req64_l,
		  ack64_l
		  );

   
   input clk,         // PCI bus clock 
	 reset_l;     // system reset 

   inout
	 frame_l,     // PCI frame flag, driven by master device 
	 irdy_l;      // PCI irdy flag, driven by master device to acknowledge data 
   
   
   inout [7:0]  cbe;   // PCI command/byte enables, driven by master 
   
   inout [63:0] ad;   // PCI address/data 
   inout 	par;  // PCI parity
   inout 	par64;
   
   input	idsel;       // device select flag for configuration access 

   output 	devsel_l,   // PCI devsel, driven by slave to claim address 
		trdy_l,     // PCI trdy, driven by slave to acknowledge data 
		stop_l;     // PCI stop, driven by slave to stop data trf

   output 	perr_l;     // Data parity error
   output 	serr_l;     // Address parity error

   input 	req64_l;
   output 	ack64_l;

`protect
// carbon license crbn_vsp_exec_xtor_pci

`include "carbonx_pci_defines.vh"

   /************************** 
    ** Internal registers    ** 
    **************************/ 
   reg 		devsel,       // address claim 
		trdy,         // data cycle ack 
		stop,         // stop data trf
		mem_rd,       // indicates access is a read 
		mem_rd_1,     // indicates access is a read , used for driving AD lines
		mem_wr,       // indicates access is a write 
		cfg_wr,       // indicates access is configuration cycle write 
		cfg_rd,       // indicates access is configuration cycle read 
		simside_in_range; // indicates access in range in C-based transactor mode 
   
   reg [7:0] 	word_count;  
   reg [3:0] 	pci_slv_state; // state machine register 
   
   reg [63:0] 	access_address;  // address of current access 
   reg [1:0] 	byte_address;  // address of current access 
   
   reg [7:0] 	cntr;
   reg [7:0] 	num_wait_cycles;
   reg [3:0] 	resp_type;
   reg [7:0] 	num_data_ph;
   reg 		insert_wait;

   reg 		irdy_l_del, trdy_del, mem_wr_del;
   reg [63:0] 	ad_del;
   reg [7:0] 	cbe_del;
   reg 		perr, serr;
   reg 		frame_del, frame_dd, frame_ddd;
   
   reg 		par_en, par_en_d;
   reg 		rd_par;
   reg 		addr_in_range_del;
   reg 		addr_in_range_del1;

   reg 		check_addr_par;
   reg 		check_addr_par_del;

   reg 		ack64;
   reg 		req64;
   reg 		req64_del;
   reg 		rd_par_1;
   
//   reg [31:0] 	temp1;
   
   wire [31:0] 	simside_rd_data;  // return data from C when operating as transactor 
   wire [31:0] 	simside_rd_data_1;

   reg [3:0] 	DevselCount;
   reg [3:0] 	devsel_cntr;
   wire 	addr_in_range; // indicates access is to this memory 
   
   wire [31:0] 	cfg_access_addr;   // address for configuration access 
   reg [31:0] 	cfg_base_reg;
   reg [31:0] 	cfg_limit_reg;
   reg [31:0] 	cfg_misc_reg;
   
   
   parameter 	PCI_IDLE     = 4'h0, 
		PCI_GNT      = 4'h1, 
		PCI_RECV_DAC = 4'h2, 
		PCI_ADDR     = 4'h3, 
		PCI_CLAIM    = 4'h4, 
		PCI_DATA     = 4'h5, 
		PCI_BURST    = 4'h6, 
		PCI_MABRT    = 4'h7, 
		PCI_WAIT     = 4'h8,
		PCI_WTST     = 4'h9,
		PCI_TABRT    = 4'ha,
		PCI_TABRT_1  = 4'hb,
                DEVSEL_WAIT  = 4'hc; 
      


   /************************** 
    ** continuous assigns    ** 
    **************************/ 
//   assign #1 ack64_l = (req64) ? ( ((mem_rd || mem_wr) & addr_in_range) ? !ack64 : 1'bz) : 1'bz;
   assign #1 ack64_l = ~req64;
   assign #1 devsel_l = (((mem_rd || mem_wr) & addr_in_range) || cfg_wr || cfg_rd) ? !devsel : 1'bz; 
   assign #1 trdy_l = (((mem_rd || mem_wr) & addr_in_range) || cfg_wr || cfg_rd) ? !trdy : 1'bz; 
   assign #1 stop_l = (((mem_rd || mem_wr) & addr_in_range) || cfg_wr || cfg_rd) ? !stop : 1'bz; 
   assign #1 par = par_en_d ? rd_par : 1'bz;
   assign #1 perr_l = (mem_wr_del & (addr_in_range_del | addr_in_range_del1))  ? !perr : 1'bz;
   assign #1 serr_l = ((frame_ddd & !frame_dd) || check_addr_par_del)  ? !serr : 1'bz;

   assign cfg_access_addr = {26'b0,access_address[5:0]}; 
   
   assign addr_in_range = simside_in_range; 

   assign #1 ad[31:0] = (devsel & (cfg_rd | (mem_rd_1 & addr_in_range))) ? simside_rd_data : 32'hzzzzzzzz; 

   assign #1 ad[63:32] = (devsel & req64) ? ((devsel & mem_rd_1 & addr_in_range) ? simside_rd_data_1 : 32'bz) : 32'bz;

   assign #1 par64 = (par_en_d & req64_del) ? rd_par_1 : 1'bz;

   assign #1 cbe     = reset_l ? 8'hzz : 8'hff;
   assign #1 irdy_l  = reset_l ? 1'bz  : 1'b1;
   assign #1 frame_l = reset_l ? 1'bz  : 1'b1;


   // Note : ad is driven one clock earlier than it should be. But it dosent cause
   // any simulation problem, as trdy is not asserted during that clock.
   
   
   always @ (posedge clk)
      begin

	 irdy_l_del <= irdy_l;
	 trdy_del   <= trdy;
	 mem_wr_del <= mem_wr;
	 ad_del     <= ad;
	 cbe_del    <= cbe;
	 frame_del  <= frame_l;
	 frame_dd   <= frame_del;
	 frame_ddd  <= frame_dd;
	 addr_in_range_del1 <= addr_in_range_del;
	 addr_in_range_del <= addr_in_range;
	 check_addr_par_del <= check_addr_par;
	 req64_del  <= req64;
      end

/*******************************
 *
 *  RAM Array code
 *
 *******************************/
wire[7:0] enb;
wire[63:0] wdat_data, wdat_mdata, admux, rdat_data;
wire[8:0] wdat_addr, rdat_addr;
wire wdat_we;

// dpram_core_64_9_512
   `endprotect
    carbonx_pci_dpram #(64, 25, 1<<25) dataram (
	.Aaddress(wdat_addr),
	.Adatain (wdat_data),
	.Adataout(wdat_mdata), 
	.Awe     (wdat_we),
	.Aen     (1'b1),
	.Ack     (clk),

	.Baddress(rdat_addr),
	.Bdatain (64'h0),
	.Bdataout(rdat_data),
	.Bwe     (1'b0),
	.Ben     (1'b1),
	.Bck     (clk)
    );
   `protect
   
assign #1
    wdat_we = !irdy_l & trdy & mem_wr & simside_in_range,

    enb  = {req64                      ? cbe[7:4] :   // 64 bit access
		access_address[0]      ? cbe[3:0] :   // unaligned 32 bit access
				             4'hF,	 // aligned 32 bit access
            req64 | ~access_address[0] ? cbe[3:0] : // 64 bit or aligned 32 bit
					     4'hF},       // unaligned 32 bit access

    wdat_addr = (access_address >> 1),

    // This does some cheating if we are inc'ing the address this cycle...
    rdat_addr = trdy ? (access_address >> 1) +(access_address[0] |req64) : (access_address >> 1),

    admux = req64 ? ad : {ad[31:0], ad[31:0]},  // handle 32 bit writes by replication

    wdat_data[63:56] = enb[7] ? wdat_mdata[63:56] : admux[63:56],
    wdat_data[55:48] = enb[6] ? wdat_mdata[55:48] : admux[55:48],
    wdat_data[47:40] = enb[5] ? wdat_mdata[47:40] : admux[47:40],
    wdat_data[39:32] = enb[4] ? wdat_mdata[39:32] : admux[39:32],
    wdat_data[31:24] = enb[3] ? wdat_mdata[31:24] : admux[31:24],
    wdat_data[23:16] = enb[2] ? wdat_mdata[23:16] : admux[23:16],
    wdat_data[15: 8] = enb[1] ? wdat_mdata[15: 8] : admux[15: 8],
    wdat_data[ 7: 0] = enb[0] ? wdat_mdata[ 7: 0] : admux[ 7: 0];


assign
  simside_rd_data   = mem_rd & ~req64 & access_address[0] ? rdat_data[63:32] :
		      mem_rd                              ? rdat_data[31: 0] :
		      cfg_access_addr=='h7                ? cfg_base_reg     :
		      cfg_access_addr=='h8                ? cfg_limit_reg    :
							    cfg_misc_reg       ,
// This is ONLY used for 64 bit memory reads -- cfg accesses are always 32 bit
  simside_rd_data_1 = rdat_data[63:32];



   // This task is called to store a configuration-space write access 
   // to the C model
always @(posedge clk) begin
	 if (!reset_l) begin
	    cfg_base_reg  = 'h80000000;
	    cfg_limit_reg = 'h80200000;
	    cfg_misc_reg  = 'h0010ff00;

	 end else if (trdy & cfg_wr) begin	    
`ifdef PCISLVDEBUG
$display("%m  Writing -- A: %h   D: %h", cfg_access_addr, ad);
`endif
	    case (cfg_access_addr)
	    'h7: cfg_base_reg  = ad[31:0];
	    'h8: cfg_limit_reg = ad[31:0];
	    'h9: cfg_misc_reg  = ad[31:0];
	    endcase
	 end

	 num_wait_cycles <= cfg_misc_reg[7:0];
	 num_data_ph     <= cfg_misc_reg[15:8];
	 resp_type       <= cfg_misc_reg[19:16];
         DevselCount     <= cfg_misc_reg[23:20];
	 insert_wait     <= cfg_misc_reg[31];
	
end // always @ (posedge clk)
   

  
   /************************** 
    ** PCI slave state m/c   ** 
    **************************/ 
   always @(posedge clk) 
      begin 
	 par_en_d <= par_en;
	 
	 if (!reset_l) 
	    begin 
	       req64 <= 0;
	       ack64 <= 0;
	       devsel <= 0; 
	       trdy <= 0; 
	       stop <= 0; 
	       mem_rd <= 0; 
	       mem_rd_1 <= 0; 
	       mem_wr <= 0; 
	       cfg_wr <= 0; 
	       cfg_rd <= 0; 
	       word_count <= 0;
	       check_addr_par <= 0;
	       simside_in_range <= 1;
	       access_address[63:0] <= 0;
	       byte_address[1:0] <= 0;
	       pci_slv_state <= PCI_IDLE;
//	       simside_rd_data <= 0;
//	       simside_rd_data_1 <= 0;
               devsel_cntr <= 0;
	       cntr <= 0;
	    end // if (!reset_l) 
	 else 
	    begin 


	       if ((cfg_rd && (pci_slv_state != PCI_IDLE)) || (mem_rd_1 && addr_in_range))
		  begin
		     par_en <= 1'b1;
		     rd_par <= ^{ad[31:0],cbe[3:0]};
		     rd_par_1 <= ^{ad[63:32],cbe[7:4]};
		  end
	       else
		  par_en <= 1'b0;
	       
	       
	       case (pci_slv_state) 
		 
	       PCI_IDLE : begin 
		       if (!frame_l) begin 
			     if (req64)
				access_address[29:0] <= {ad[31:3],1'b0};
			     else
				access_address[29:0] <= ad[31:2];
				
			     byte_address[1:0] <= ad[1:0]; 
			     req64 <= ~req64_l;

			     if (cbe[3:0] == 4'hd)                                //Dual address cycle
				pci_slv_state <= PCI_RECV_DAC;
			     else if ((cbe[3:0] == 4'hb) && idsel) begin          // Config Write
				   cfg_wr <= 1;
				   pci_slv_state <= PCI_CLAIM;
		             end else if ((cbe[3:0] == 4'ha) && idsel) begin      // Config Read
				   cfg_rd <= 1;
				   pci_slv_state <= PCI_CLAIM;
			     end else if ((cbe[3:0] == 4'h6) || (cbe[3:0] == 4'hC)
				       || (cbe[3:0] == 4'hE)) begin               // Memory Read
				   mem_rd   <= 1;
				   mem_rd_1 <= 1;
				   pci_slv_state <= PCI_CLAIM;
			     end else if ((cbe[3:0] == 4'h7) || (cbe[3:0] == 4'hF)) begin  // Memory Write
				   mem_wr <= 1;
				   pci_slv_state <= PCI_CLAIM;
			     end

			  simside_in_range <= (ad[31:0] >= cfg_base_reg[31:0]) && (ad[31:0] <= cfg_limit_reg[31:0]);

		       end else begin
			     devsel <= 0;
			     req64 <= 0;
			     ack64 <= 0;
			     trdy <= 0;
			     stop <= 0;
			     mem_rd <= 0;
			     mem_rd_1 <= 0;
			     mem_wr <= 0;
			     cfg_wr <= 0;
			     cfg_rd <= 0;
			     word_count <= 0;
			     check_addr_par <= 0;
			     simside_in_range <= 1;
			     access_address[63:0] <= 0;
			     pci_slv_state <= PCI_IDLE;
		       end
		 end // case: PCI_IDLE

		 PCI_RECV_DAC :
		    begin
 		       simside_in_range <= {ad[31:0],access_address[29:0], 2'b0} >= cfg_base_reg
                                           && {ad[31:0],access_address[29:0], 2'b0} <= cfg_limit_reg;
		       access_address[61:30] <= ad[31:0];
		       check_addr_par <= 1;
		       pci_slv_state <= PCI_CLAIM;

		       if ((cbe[3:0] == 4'hb) && idsel)
			  cfg_wr   <= 1;
		       else if ((cbe[3:0] == 4'ha) && idsel)
			  cfg_rd   <= 1;
		       else if ((cbe[3:0] == 4'h6) || (cbe[3:0] == 4'hC) || (cbe[3:0] == 4'hE))  begin
			  mem_rd   <= 1;
			  mem_rd_1 <= 1;
		       end else if ((cbe[3:0] == 4'h7) || (cbe[3:0] == 4'hF)) begin // Memory Write
			  mem_wr   <= 1;
		       end
		    end


		 
		 PCI_CLAIM : if (addr_in_range || cfg_wr || cfg_rd) 
		    begin 
`ifdef PCISLVDEBUG
		       $display("Address in range %x, cfg_wr %x, cfg_rd %x\n",
				addr_in_range,cfg_wr,cfg_rd); 
`endif
		       check_addr_par <= 0;
		       ack64 <= req64;

//	    	       pci_get_wait_state(0);  //NEW-AG

			if (DevselCount < 2)
                          begin
		       	   devsel <= 1; 
		       	   case(resp_type)
			 	RETRY  : pci_slv_state <= PCI_TABRT;
			 	TABRT, 
			 	DISCNT : begin
			    		    if (insert_wait)
					    	   pci_slv_state <= PCI_WAIT; 
			                    else if (word_count == num_data_ph) 
			       			   pci_slv_state <= PCI_TABRT_1;
			    		    else
						    pci_slv_state <= PCI_DATA;
`ifdef PCISLVDEBUG
					    $display("Next pci_slave_state = %x\n", pci_slv_state);
`endif
			 		  end
			 	NORMAL : begin 
					    if (insert_wait) pci_slv_state <= PCI_WAIT;
			 		    else  pci_slv_state <= PCI_DATA;
`ifdef PCISLVDEBUG
				            $display("Current pci_slave_state=PCI_CLAIM, next=%x\n", 
						      pci_slv_state); 
`endif
					  end
			 	default : pci_slv_state <= PCI_DATA;
		           endcase
		       

		       	   // Assert stop for RETRY
		           stop <= (resp_type == RETRY) ? 1 : 0;
		       
		           // Assert trdy for data transfer
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				   (word_count != num_data_ph))) && !irdy_l && !insert_wait)
			      trdy <= 1;
		       
		       	   // Increment word count if data transfer takes place
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				    (word_count != num_data_ph)) ) && !irdy_l && !insert_wait)
			      word_count <= word_count + 1;
		       
		           // Load counter with the num of initial wait states reqd
		           cntr <= num_wait_cycles;
			  end // if (DevselCount==1)
		        else  
			  begin
				devsel <= 0;
				devsel_cntr <= DevselCount;
				pci_slv_state <= DEVSEL_WAIT;
			  end // else: !if(DevselCount == 1)
		    end // if (addr_in_range) 
		 else 
		    begin 
		       ack64 <= 0;
		       devsel <= 0; 
		       mem_rd <= 0; 
		       mem_rd_1 <= 0; 
		       mem_wr <= 0; 
		       check_addr_par <= 0;
		       pci_slv_state <= PCI_MABRT; 
		    end // else: !if(addr_in_range) 
		 

		DEVSEL_WAIT : begin
			devsel_cntr <= devsel_cntr - 1;
			if (devsel_cntr == 1)
                          begin
		       	   devsel <= 1; 
		       	   case(resp_type)
			 	RETRY  : pci_slv_state <= PCI_TABRT;
			 	TABRT, 
			 	DISCNT : begin
			    		    if (insert_wait) pci_slv_state <= PCI_WAIT; 
			                    else if (word_count == num_data_ph) 
			       			   pci_slv_state <= PCI_TABRT_1;
			    		    else pci_slv_state <= PCI_DATA;
`ifdef PCISLVDEBUG
					    $display("Next pci_slave_state = %x\n", pci_slv_state);
`endif
			 		  end
			 	NORMAL : begin 
					    if (insert_wait) pci_slv_state <= PCI_WAIT;
			 		    else  pci_slv_state <= PCI_DATA;
`ifdef PCISLVDEBUG
				            $display("Current pci_slave_state=PCI_CLAIM, next=%x\n", 
						      pci_slv_state); 
`endif
					  end
			 	default : pci_slv_state <= PCI_DATA;
		           endcase
		       

		       	   // Assert stop for RETRY
		           stop <= (resp_type == RETRY) ? 1 : 0;
		       
		           // Assert trdy for data transfer
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				   (word_count != num_data_ph))) && !irdy_l && !insert_wait)
			      trdy <= 1;
		       
		       	   // Increment word count if data transfer takes place
		           if (((resp_type == NORMAL) || ((resp_type != RETRY) && 
				    (word_count != num_data_ph)) ) && !irdy_l && !insert_wait)
			      word_count <= word_count + 1;
		       
		           // Load counter with the num of initial wait states reqd
		           cntr <= num_wait_cycles;
			  end // if (devsel_cntr==1)
		        else  
			  pci_slv_state <= DEVSEL_WAIT;
		    end // DEVSEL_WAIT


		 PCI_WAIT : begin
		    cntr <= cntr - 1;

		    if (frame_l && irdy_l)
		       begin
			  req64 <= 0;
			  ack64 <= 0;
			  trdy <= 0;
			  devsel <= 0;
			  stop <= 0;
			  mem_rd_1 <= 0;
			  pci_slv_state <= PCI_IDLE;
		       end
		    else if (cntr == 1)     
		       begin
			  case(resp_type)
			    TABRT, 
			  DISCNT : begin
			     if (word_count == num_data_ph)
				pci_slv_state <= PCI_TABRT;
			     else pci_slv_state <= PCI_DATA;
			  end
			    NORMAL   : pci_slv_state <= PCI_DATA;
			    default : pci_slv_state <= PCI_DATA;
			  endcase
			  
			  // Assert trdy if next state is data transfer
			  trdy <= ((resp_type == NORMAL) | 
				   ((resp_type != NORMAL) && (word_count != num_data_ph)) );
			  
			  // Increment word count if next state is data transfer
			  word_count <= ((resp_type == NORMAL) | ((resp_type != NORMAL) 
								 && (word_count != num_data_ph)) ) ? word_count + 1 :
					word_count;

			  // Deassert devsel if target abort and byte count is satisfied
			  devsel <= ((resp_type == TABRT) && (word_count == num_data_ph)) ? 0 : 1;
			  ack64  <= ((resp_type == TABRT) && (word_count == num_data_ph)) ? 0 : 1;

			  // Assert stop if targetabort/retry/discnt and byte count is satisfied
			  stop <= ((resp_type != NORMAL) && (word_count == num_data_ph));
		       end
		    else // (cntr != 1)
		       pci_slv_state <= PCI_WAIT;
		 end
		 
		 PCI_DATA : begin 
		    
		    if (!frame_l ) begin
	    		// pci_get_wait_state(1); // NEW-AG
		       
`ifdef PCISLVDEBUG
	$display("Response type = %x\n", resp_type);
`endif
	       if (insert_wait || irdy_l)
			  begin
			     pci_slv_state <= PCI_WTST;
`ifdef PCISLVDEBUG
		$display("Current pci_slave_state=PCI_DATA, next=PCI_WTST, irdy_l=%x, insert_wait=%x\n", irdy_l, insert_wait); 
`endif
			     trdy <= 0;
			     if ( (((resp_type != NORMAL) && (word_count != num_data_ph)) || 
				   (resp_type == NORMAL)) && !irdy_l & ((trdy & mem_wr) | mem_rd)) begin
				      if (req64) 
					 access_address <= access_address + 2;
				      else       
					 access_address <= access_address + 1;
				      word_count <= word_count + 1;
				   end // if ( (((resp_type != NORMAL) && (word_count != num_data_ph)) ||...
			  end
		       else
			  begin
			     case(resp_type)
			     TABRT,   
			     DISCNT  : begin
				if (word_count == num_data_ph)
				   pci_slv_state <= PCI_TABRT;
				else
				   pci_slv_state <= PCI_DATA;
			     end
			     NORMAL    : begin
				pci_slv_state <= PCI_DATA;
`ifdef PCISLVDEBUG
			        $display("Current pci_slave_state=PCI_DATA, next=%x, irdy_l=%x, insert_wait=%x\n",
					 pci_slv_state, irdy_l, insert_wait);
`endif
			     end
			     default  : pci_slv_state <= PCI_DATA;
			     endcase
			     

			     // If normal data trf, or (abort and data count not satisfied)
			     if ( (((resp_type != NORMAL) && (word_count != num_data_ph)) || 
				   (resp_type == NORMAL)) && !irdy_l & ((trdy & mem_wr) | mem_rd))
				begin 
				   trdy <= 1;
				   pci_slv_state <= PCI_DATA;
				   if (req64) 
				      access_address <= access_address + 2;
				   else       
				      access_address <= access_address + 1;
				   
				   word_count <= word_count + 1;
`ifdef PCISLVDEBUG
				$display("Normal operation\n");
`endif
				end // if (!frame_l & !irdy_l) 
			     else begin
				trdy <= 0;
				devsel <= ((resp_type == TABRT) && (word_count == num_data_ph))  ? 0 : 1;
				ack64 <= ((resp_type == TABRT) && (word_count == num_data_ph))   ? 0 : 1;
				stop   <= ((resp_type != NORMAL) && (word_count == num_data_ph)) ? 1 : 0;
`ifdef PCISLVDEBUG
				$display("Normal termination\n");
`endif
			     end
			  end
		    end
		    //		    else if (frame_l & !irdy_l) 
		    else if (frame_l) 
		       begin 
`ifdef PCISLVDEBUG
			     $display(" Frame deasserted\n");
`endif
			  if (irdy_l) 
`ifdef PCISLVDEBUG
			     $display(" Frame and Irdy deasserted before Trdy is asserted");
`endif

			  req64 <= 0;
			  ack64 <= 0;
			  trdy <= 0;
			  devsel <= 0;
			  stop <= 0;
			  mem_rd_1 <= 0;
			  par_en <= 0;
			  check_addr_par <= 0;
			  pci_slv_state <= PCI_IDLE; 
		       end // if (frame_l & !irdy_l) 
		    else 
		       begin
			  pci_slv_state <= PCI_DATA; 
		       end
		 end // case: PCI_DATA 
		 
		 
		 PCI_WTST : begin
		    
	    	    // pci_get_wait_state(1); // NEW-AG

		    if (frame_l && irdy_l)
		       begin
			  pci_slv_state <= PCI_IDLE;
			  req64 <= 0;
			  ack64 <= 0;
			  trdy <= 0;
			  devsel <= 0;
			  stop <= 0;
			  mem_rd_1 <= 0;
			  par_en <= 0;
			  check_addr_par <= 0;
		       end
		    else if (insert_wait || irdy_l) begin
		       pci_slv_state <= PCI_WTST;
		       trdy <= 0;
		    end
		    else 
		       begin
			  
			  case(resp_type)
			    TABRT,
			    DISCNT  : begin
			       if (word_count == num_data_ph)
				  pci_slv_state <= PCI_TABRT;
			       else pci_slv_state <= PCI_DATA;
			    end
			    NORMAL    : pci_slv_state <= PCI_DATA;
			    default  : pci_slv_state <= PCI_DATA;
			  endcase
			  
			  
			  trdy <= ( ((resp_type != NORMAL) && (word_count != num_data_ph)) || 
				    (resp_type == NORMAL)) ? 1 : 0;
			  devsel <= ((resp_type == TABRT) && (word_count == num_data_ph))  ? 0 : 1;
			  ack64 <= ((resp_type == TABRT) && (word_count == num_data_ph))   ? 0 : 1;
			  stop   <= ((resp_type != NORMAL) && (word_count == num_data_ph)) ? 1 : 0;

/* -----\/----- EXCLUDED -----\/-----
			  access_address <= ( ((resp_type != NORMAL) && (word_count != num_data_ph))
					      || (resp_type == NORMAL) ) ? ((req64) ? access_address + 2 :
									   access_address + 1) : access_address;
			  word_count  <= ( ((resp_type != NORMAL) && (word_count != num_data_ph)) || 
					   (resp_type == NORMAL) ) ? word_count  + 1 : word_count ;
			  
 -----/\----- EXCLUDED -----/\----- */
		       end
		 end
		 
		 
		 PCI_MABRT : begin
		    if (frame_l) 
		       begin 
			  par_en <= 0;
			  check_addr_par <= 0;
			  pci_slv_state <= PCI_IDLE; 
			  req64 <= 0;
			  mem_rd_1 <= 0;
		       end // if (frame_l) 
		    else 
		       begin 
			  pci_slv_state <= PCI_MABRT; 
		       end // else: !if(frame_l) 
		 end
		 
		 PCI_TABRT : begin
		    if (frame_l) 
		       begin
			  ack64 <= 0;
			  req64 <= 0;
			  trdy <= 0;
			  stop <= 0;
			  devsel <= 0;
			  mem_rd_1 <= 0;
			  par_en <= 0;
			  check_addr_par <= 0;
			  pci_slv_state <= PCI_IDLE;
		       end
		    else 
		       pci_slv_state <= PCI_TABRT;
		 end
		 
		 PCI_TABRT_1:begin
		    stop <= 1;
		    devsel <= (resp_type == TABRT) ? 0 : 1;
		    ack64 <= (resp_type == TABRT)  ? 0 : 1;
		    pci_slv_state <= PCI_TABRT;
		 end
		 
		 
		 default : begin
		    par_en <= 0;
		    check_addr_par <= 0;
		    pci_slv_state <= PCI_IDLE; 
		 end
	       endcase // case(pci_slv_state) 

	    end // else: !if(!reset_l) 
      end // always @ (posedge clk) 
   
   

   always @ (posedge clk)
      begin
	 if (!irdy_l_del & trdy_del & mem_wr_del)
	    perr <= (^{ad_del[31:0] , cbe_del[3:0] , par}) |
		     (^{ad_del[63:32] , cbe_del[7:4] , par64, ~req64_del});
	 else perr <= 0;
      end

   always @ (posedge clk)
      begin
	 if ((!frame_del & frame_dd) || check_addr_par)
	    serr <= ^{ad_del[31:0] ,par, cbe_del[3:0]};
	 else
	    serr <= 0;
      end // always @ (posedge clk)

`endprotect
endmodule // PciSlave
