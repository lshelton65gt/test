
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// *****************************************************************************
// Description:
//
// This module is the top level of a PCI initiator. It has the PCI state 
// machine instantiated. 
// *****************************************************************************

`timescale 1 ns / 100 ps


module carbonx_pci_initiator
   (
    frame_l,
    irdy_l,
    trdy_l,
    devsel_l,
    stop_l,
    req_l,
    gnt_l,
    req64_l,
    ack64_l,
    ad,
    cbe,
    par,
    par64,
    perr_l,
    serr_l,
    int_l,
    clk,
    rst_l
    );

   
   
   /**********************************
    ** List all Input/Output signals **
    **********************************/
   
   inout frame_l;
   inout irdy_l;
   input trdy_l;
   input stop_l;
   input devsel_l;
   inout perr_l;
   
   input serr_l;
   input [3:0] int_l;
   
   output      req_l;
   input       gnt_l;
   
   output      req64_l;
   input       ack64_l;

   inout       par;
   inout       par64;
   inout [63:0] ad;
   inout [7:0] 	cbe;
   
   input 	clk;
   input 	rst_l;
      
`protect
// carbon license crbn_vsp_exec_xtor_pci

`include "carbonx_pci_defines.vh"

  // We need this handful of signals to properly configure the
  // transactor interface.
  parameter    
               ClockId   =   1,
               DataWidth =  64,
               DataAdWid =   9, // Max is 15
               GCSwid    = 128, // Minimum is 1.
               PCSwid    =  32; // Minimum is 1.

// *****************************************************************************
// Module Name: carbonx_pci_initiator.v
//
// Author : Nirmala Paul
// Date   : 7/2/2001
//
// Module Description:
//
// This module is the top level of a PCI initiator. It has the PCI state 
// machine instantiated. 
//
//
// The following features are supported :
// * Configuration accesses
// * Write/Read accesses (single and burst)
// * 64-bit data support
// * Dual Address cycle support
// * Parity generation and error detection
// * Termination types supported : Retry, Target Abort, Disconnect at ADB
//   Single Data phase disconnect, Split response, Master Abort (some of these
//   are supported by PCI-X only).
// * Target inserted Initial wait state is also supported 
// * A status register is updated with the status of the transaction 
//   ( type of termination, parity errors etc), and is passed to the C-side
//
// *****************************************************************************

  //
  // Include XIF.
  //

`include "xif_core.vh"


  //
  // Internal registers and wires.
  //

  wire 			 reset;

// These are all outputs from the two state machines that get muxed
// on their way up to the C side
  wire [63:0]		 pci_data;
  wire [DataAdWid-1:0] 	 pci_daddr;
  wire [DataAdWid+2:0] 	 pci_size;
  wire [31:0]		 pci_status;
  wire [31:0]		 pci_opcode;
  wire			 pci_xrun,
			 pci_dwe;

   /**********************************
    ** Internal wires/state
    **********************************/

   wire 	req_pci;
   wire 	TBP_reset;

   wire 	default_64bit   =  0;
   wire [3:0] 	default_wr_cmd  =  7;
   wire [3:0] 	default_rd_cmd  =  6;


   //
   // Continuous assignments.
   //

   assign 		 BFM_clock = clk;

   assign 		 BFM_reset = ~rst_l;

   // Reset internal logic from external input or from SW
   assign 		 reset     = ~rst_l | XIF_reset;


   assign 		 BFM_put_operation = BFM_NXTREQ;
   assign 		 BFM_put_data      = pci_data;
   assign 		 BFM_put_dwe       = pci_dwe;
   assign 		 BFM_put_status    = pci_status;
   assign 		 BFM_xrun          = pci_xrun;
   assign 		 BFM_gp_daddr      = pci_daddr;
   assign 		 BFM_put_size      = pci_size;
   assign                BFM_put_address   = 0;
   assign                BFM_put_csdata    = 0;
   assign 		 BFM_interrupt = 0;
   
 //  Screwy decode to translate TBP operation to appropriate bits to
 //    pci/pcix SM's
   assign  pci_opcode = XIF_get_operation == BFM_WRITE ? {26'h0, 1'b1, default_64bit, default_wr_cmd} 
                      : XIF_get_operation == BFM_READ  ? {26'h0, 1'b0, default_64bit, default_rd_cmd}
                      : XIF_get_operation == BFM_NOP   ? 0
		      : XIF_get_operation;
   
   assign req_l = !rst_l ? 1'bz : req_pci;
   
   
   
   /******************************************
    ** Instantiation of the PCI initiator **
    ******************************************/
   
   carbonx_pci_master PCI_INIT_SM(
	// PCI bus pins
			   .clk(clk),
			   .reset_l(rst_l),
			   .req_l(req_pci),
			   .gnt_l(gnt_l),
			   .req64_l(req64_l),
			   .ack64_l(ack64_l),
			   .frame_l(frame_l),
			   .devSel_l(devsel_l),
			   .irdy_l(irdy_l),
			   .trdy_l(trdy_l),
			   .ad(ad),
			   .cbe_l(cbe),
			   .stop_l(stop_l),
			   .par(par),
			   .par64(par64),
			   .perr_l(perr_l),
			   .serr_l(serr_l),
			   .int_l(int_l),

	// Transactor interface communications
			   .operation(pci_opcode),  // op & address are in only
			   .address(XIF_get_address),
			   .get_size(XIF_get_size),	// rest have GET & PUT
			   .get_status(XIF_get_status),
			   .get_data(XIF_get_data),
			   .get_csdata(XIF_get_csdata[63:0]),
			   .put_size(pci_size),
			   .put_status(pci_status),
			   .put_data(pci_data),
			   .Xrun(pci_xrun),
			   .put_dwe(pci_dwe),
			   .gp_daddr(pci_daddr)

			   );


`endprotect
endmodule // carbonx_pci_initiator
