# Don't do protected verilog on Windows - it should have already been
# done on Linux
if(${WIN32})
  return()
endif()

generate_xtor_vp(pci/verilog
                 carbonx_pci_initiator.v
                 carbonx_pci_master.v
                 carbonx_pci_target.v
                 carbonx_pci_target2.v
                 carbonx_pci_slave_mem.v
                 carbonx_pci_arbiter.v
                 carbonx_pci_dpram.v
                 carbonx_pci_defines.vh
                 )
