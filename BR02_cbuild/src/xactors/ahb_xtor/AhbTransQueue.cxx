// -*- mode: c++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stddef.h>
#include <stdio.h>

#include "AhbTransQueue.h"


AhbTransQueueItem::AhbTransQueueItem(AhbTransaction * trans) :
  _trans (trans),
  _prev (NULL),
  _next (NULL)
{}

AhbTransQueueItem::~AhbTransQueueItem()
{}

AhbTransaction * AhbTransQueueItem::trans()
{
  return _trans;
}

AhbTransaction * AhbTransQueueItem::next_trans()
{
  if (_next == NULL)
    return NULL;
  else
    return _next->_trans;
}



AhbTransQueue::AhbTransQueue(CarbonUInt32 size):
  _size(size),
  _count(0),
  _head(NULL),
  _tail(NULL)
{}

AhbTransQueue::~AhbTransQueue()
{
  // free up all nodes
  while (_head != NULL)
    {
      AhbTransQueueItem * next_head = _head->_next;
      delete _head;
      _head = next_head;
    }
}

bool AhbTransQueue::push(AhbTransaction * trans)
{
  if (_size > 0 && _count >= _size) // if no more items allowed
    {
      sprintf(get_error_message(), "ERROR: Push() overflowed on AHB Transaction Queue of size %d!\n", _size);

      return false;
    }

  // create an item
  AhbTransQueueItem * item = new AhbTransQueueItem(trans);

  if (item == NULL)
    return false;

  if (isEmpty())
    {
      _head = _tail = item;
    }
  else
    {
      _tail->_next = item;
      item->_prev = _tail;

      _tail = item;
    }

  _count ++;

  return true;
}

AhbTransaction * AhbTransQueue::pop()
{
  if (_count == 0)
    {
      sprintf(get_error_message(), "ERROR: Empty AHB Transaction Queue accessed with pop()!\n");

      return NULL;
    }

  // items are always poped from the head of the queue 
  AhbTransQueueItem * item = _head;
  _count--;

  if (_count == 0) // if queue becomes empty after pop
    _head = _tail = NULL;
  else // if queue has some item after pop
    _head = item->_next;

  // retrieve the trans and free the memory for queue node
  AhbTransaction * trans = item->_trans;
  delete item;

  return trans;
}

AhbTransQueueItem * AhbTransQueue::top()
{
  if (_count == 0) // queue is empty
    {
      sprintf(get_error_message(), "ERROR: Empty AHB Transaction Queue accessed with top()!\n");

      return NULL;
    }

  // get the trans corresponding to head
  return _head;
}
/*
AhbTransaction * AhbTransQueue::next()
{
  return _head->_next->_trans;
}
*/

// Deletes a particular transaction if exists in queue
bool AhbTransQueue::remove(AhbTransaction * trans)
{
  // search for the transaction in queue
  AhbTransQueueItem * item = _head;
  while (item != NULL && item->_trans != trans)
    item = item->_next;

  // if not found
  if (item == NULL)
    return false;

  // delete node containing the transaction
  _count--;
  if (_count == 0) // if queue becomes empty after delete
    _head = _tail = NULL;
  else if (item == _head) // if it's at head
    {
      _head = _head->_next;
    }
  else if (item == _tail) // if it's at tail
    {
      _tail = _tail->_prev;
      _tail->_next = NULL;
    }
  else // when transaction is in the middle
    {
      item->_next->_prev = item->_prev;
      item->_prev->_next = item->_next;
      item->_next = NULL;
    }

  delete item;

  return true;
}
