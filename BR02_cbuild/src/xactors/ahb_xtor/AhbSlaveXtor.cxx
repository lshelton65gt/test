// -*- mode: c++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stddef.h>
#include <string.h>

#include "AhbSlaveXtor.h"

AhbSlaveXtor::AhbSlaveXtor(const AhbConfig_t & ahb_cfg)
  : AhbAbsXtor(ahb_cfg.xtor_name, CarbonXAhb_Slave),
    _state (SLAVE_IDLE),
    _prevTransType (CarbonXAhbTrans_IDLE),
    _waitCount (0),
    _splitWaitCount (0),
    _splitMaster (0),
    _split (false),
    _addr (0),
    _writeAddr (0),
    // ahb slave signals
    _hReset_n("hReset_n", CarbonXPin_Input),
    _hClk("hClk", CarbonXPin_Input),
    _hSel("hSel", CarbonXPin_Input),
    _hAddr("hAddr", CarbonXPin_Input, ahb_cfg.addr_width),
    _hWrite("hWrite", CarbonXPin_Input),
    _hTrans("hTrans", CarbonXPin_Input, 2),
    _hSize("hSize", CarbonXPin_Input, 3),
    _hBurst("hBurst", CarbonXPin_Input, 3),
    _hWData("hWData", CarbonXPin_Input, ahb_cfg.data_width),
    _hReadyIn("hReadyIn", CarbonXPin_Input),
    _hMaster("hMaster", CarbonXPin_Input, 4),
    _hMastLock("hMastLock", CarbonXPin_Input),
    _hReadyOut("hReadyOut", CarbonXPin_Output),
    _hResp("hResp", CarbonXPin_Output, 2),
    _hRData("hRData", CarbonXPin_Output, ahb_cfg.data_width),
    _hSplitx("hSplitx", CarbonXPin_Output, 16)
{ }

AhbSlaveXtor::~AhbSlaveXtor()
{ }

bool AhbSlaveXtor::bind(CarbonObjectID * carbonModelHandle)
{
  bool bound = true;

  // Ahb slave input signals
  bound &= _hReset_n.bind( carbonModelHandle );
  bound &= _hClk.bind( carbonModelHandle );
  bound &= _hSel.bind( carbonModelHandle );
  bound &= _hAddr.bind( carbonModelHandle );
  bound &= _hWrite.bind( carbonModelHandle );
  bound &= _hTrans.bind( carbonModelHandle );
  bound &= _hSize.bind( carbonModelHandle );
  bound &= _hBurst.bind( carbonModelHandle );
  bound &= _hWData.bind( carbonModelHandle );
  bound &= _hReadyIn.bind( carbonModelHandle );
  bound &= _hMaster.bind( carbonModelHandle );
  bound &= _hMastLock.bind( carbonModelHandle );

  // Ahb slave output signals
  bound &= _hReadyOut.bind( carbonModelHandle );
  bound &= _hResp.bind( carbonModelHandle );
  bound &= _hRData.bind( carbonModelHandle );
  bound &= _hSplitx.bind( carbonModelHandle );

  return bound;
}

void AhbSlaveXtor::registerResponseCB(callback_t setDefaultResponseCB,
				      callback_t setResponseCB,
				      void * userInstance)
{
  _setDefaultResponseCB = setDefaultResponseCB;
  _setResponseCB = setResponseCB;

  _userInstance = userInstance;
}


bool AhbSlaveXtor::evaluate()
{
  switch(_state)
    {
    case SLAVE_IDLE:
      if (_hReadyIn.read() && _hSel.read() && _hTrans.read() == CarbonXAhbTrans_NONSEQ)
	{
          // a transaction initiated on AHB bus, a new _trans is created to "describe" it
          _trans = new AhbTransaction(
                                      (AhbAccessType_t) _hWrite.read(), // access type - read/write
                                      _hAddr.read(),                    // addr
                                      (AhbBurstType_t) _hBurst.read(),  // burst type
                                      (AhbSizeType_t) _hSize.read()     // transfer size
                                      );

	  // we then call setDefaultResponse() since this is the beginning of a transaction
	  _setDefaultResponseCB(_trans, _userInstance);

	  _waitCount = _trans->get_slave_wait_states();

	  if (_waitCount != 0)
	    _waitCount --;

	  // initially _addr & __writeAddr are the same
	  _addr = _writeAddr = _hAddr.read();

	  if (_trans->get_resp_type() != CarbonXAhbTrans_OKAY)
	    {
	      _state = SLAVE_NOT_OK;

	      // two-cycle response, 1st cycle below
	      _hReadyOut.write32(0);
	      _hResp.write32(_trans->get_resp_type());
	      _hSplitx.write32(0);

	      // we record the master that is split
	      if (_trans->get_resp_type() == CarbonXAhbTrans_SPLIT)
		_splitMaster = _hMaster.read();
	    }
	  else if (_hWrite.read())
	    {
	      _state = SLAVE_WRITE;

	      if (_waitCount != 0)
		{
		  _hReadyOut.write32(0);
		  _hResp.write32(CarbonXAhbTrans_OKAY);
		  _hRData.writeBig(NULL);
		  _hSplitx.write32(0);
		}
	      else
		{
		  _hReadyOut.write32(1);
		  _hResp.write32(CarbonXAhbTrans_OKAY);
		  _hRData.writeBig(NULL);
		  _hSplitx.write32(0);
		}
	    }
	  else
	    {
	      _state = SLAVE_READ;

	      if (_waitCount != 0)
		{
		  _hReadyOut.write32(0);
		  _hResp.write32(CarbonXAhbTrans_OKAY);
		  _hRData.writeBig(NULL);
		  _hSplitx.write32(0);
		}
	      else
		{
		  _hReadyOut.write32(1);
		  _hResp.write32(CarbonXAhbTrans_OKAY);
		  prepareHRData();
		  _hSplitx.write32(0);
		}

	      // we alway drive read data even during wait
	      //prepareHRData();
	    }
	}
      else
	{
	  // idle the bus slave xtor is driving
	  _hReadyOut.write32(1);
	  _hResp.write32(CarbonXAhbTrans_OKAY);
	  _hRData.writeBig(NULL);
	  _hSplitx.write32(0);
	}

      break;

    case SLAVE_READ:
      if (_hReadyIn.read())
	{
	  if (_hSel.read())
	    {
	      if (_hTrans.read() == CarbonXAhbTrans_NONSEQ)
		// start of a new transaction
		{
		  // current transaction ended
		  _setResponseCB(_trans, _userInstance);
		  delete _trans;

		  // a new transaction initiated on AHB bus, a new _trans is created to "describe" it
		  _trans = new AhbTransaction(
					      (AhbAccessType_t) _hWrite.read(), // access type - read/write
					      _hAddr.read(),                    // addr
					      (AhbBurstType_t) _hBurst.read(),  // burst type
					      (AhbSizeType_t) _hSize.read()     // transfer size
					      );

		  // we then call setDefaultResponse() since this is the beginning of a transaction
		  _setDefaultResponseCB(_trans, _userInstance);

		  _waitCount = _trans->get_slave_wait_states();

		  if (_waitCount != 0)
		    _waitCount --;

		  // initially _addr & __writeAddr are the same
		  _addr = _writeAddr = _hAddr.read();

		  if (_trans->get_resp_type() != CarbonXAhbTrans_OKAY)
		    {
		      _state = SLAVE_NOT_OK;

		      // two-cycle response, 1st cycle below
		      _hReadyOut.write32(0);
		      _hResp.write32(_trans->get_resp_type());
		      _hSplitx.write32(0);

		      // we record the master that is split
		      if (_trans->get_resp_type() == CarbonXAhbTrans_SPLIT)
			_splitMaster = _hMaster.read();
		    }
		  else if (_hWrite.read())
		    {
		      _state = SLAVE_WRITE;

		      if (_waitCount != 0)
			{
			  _hReadyOut.write32(0);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  _hRData.writeBig(NULL);
			  _hSplitx.write32(0);
			}
		      else
			{
			  _hReadyOut.write32(1);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  _hRData.writeBig(NULL);
			  _hSplitx.write32(0);
			}
		    }
		  else
		    {
		      _state = SLAVE_READ;

		      if (_waitCount != 0)
			{
			  _hReadyOut.write32(0);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  _hRData.writeBig(NULL);
			  _hSplitx.write32(0);
			}
		      else
			{
			  _hReadyOut.write32(1);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  //prepareHRData();
			  _hSplitx.write32(0);
			}
		    }
		}
	      else if (_hTrans.read() == CarbonXAhbTrans_SEQ)
		{
		  // start of a new data phase, restart wait count
		  _waitCount = _trans->get_slave_wait_states();
		}
	      else if (_hTrans.read() == CarbonXAhbTrans_BUSY)
		{
		  // we don't wait for busy access
		  _waitCount = 0;
		}
	      else
		// CarbonXAhbTrans_IDLE
		{
		  // end of this transaction, call setResponse()
		  _setResponseCB(_trans, _userInstance);
		  delete _trans;

		  _state = SLAVE_IDLE;

		  _hReadyOut.write32(1);
		  _hResp.write32(CarbonXAhbTrans_OKAY);
		  _hRData.writeBig(NULL);
		}

              // update the read addr
              _addr = _hAddr.read();
	    }
	  else
	    // hSel == 0, no more access to us
	    {
	      _setResponseCB(_trans, _userInstance);
	      delete _trans;

	      _state = SLAVE_IDLE;

	      // idle the bus slave xtor is driving
	      _hReadyOut.write32(1);
	      _hResp.write32(CarbonXAhbTrans_OKAY);
	      _hRData.writeBig(NULL);
	      _hSplitx.write32(0);
	    }
	}

      // hReadyOut is controlled here out side of hReadyIn
      if (_waitCount != 0)
	{
	  _hReadyOut.write32(0);
	  _hResp.write32(CarbonXAhbTrans_OKAY);
	  _hRData.writeBig(NULL);

	  _waitCount --;
	}
      else
	{
	  if ((((_hTrans.read() != CarbonXAhbTrans_IDLE) && (_hTrans.read() != CarbonXAhbTrans_BUSY))
	       || ((_hTrans.read() == CarbonXAhbTrans_IDLE || _hTrans.read() == CarbonXAhbTrans_BUSY) && (! _hReadyOut.read()))) 
	      && (_hResp.read() == CarbonXAhbTrans_OKAY) && (! _hWrite.read()))
	    prepareHRData();
	  else
	    _hRData.writeBig(NULL);

	  _hReadyOut.write32(1);
	  _hResp.write32(CarbonXAhbTrans_OKAY);
	}

      break;

    case SLAVE_WRITE:
      if (_hReadyIn.read())
	{
	  if (((_prevTransType == CarbonXAhbTrans_NONSEQ) || (_prevTransType == CarbonXAhbTrans_SEQ))
	      && (_hResp.read() == CarbonXAhbTrans_OKAY))
	    extractHWData();

	  if (_hSel.read())
	    {
	      // update new write addr
	      _writeAddr = _hAddr.read();

	      if (_hTrans.read() == CarbonXAhbTrans_NONSEQ)
		// start of a new transaction
		{
		  // current transaction ended
		  _setResponseCB(_trans, _userInstance);
		  delete _trans;

		  // a new transaction initiated on AHB bus, a new _trans is created to "describe" it
		  _trans = new AhbTransaction(
					      (AhbAccessType_t) _hWrite.read(), // access type - read/write
					      _hAddr.read(),                    // addr
					      (AhbBurstType_t) _hBurst.read(),  // burst type
					      (AhbSizeType_t) _hSize.read()     // transfer size
					      );

		  // we then call setDefaultResponse() since this is the beginning of a transaction
		  _setDefaultResponseCB(_trans, _userInstance);

		  _waitCount = _trans->get_slave_wait_states();

		  if (_waitCount != 0)
		    _waitCount --;

		  // initially _addr & __writeAddr are the same
		  _addr = _writeAddr = _hAddr.read();

		  if (_trans->get_resp_type() != CarbonXAhbTrans_OKAY)
		    {
		      _state = SLAVE_NOT_OK;

		      // two-cycle response, 1st cycle below
		      _hReadyOut.write32(0);
		      _hResp.write32(_trans->get_resp_type());
		      _hSplitx.write32(0);

		      // we record the master that is split
		      if (_trans->get_resp_type() == CarbonXAhbTrans_SPLIT)
			_splitMaster = _hMaster.read();
		    }
		  else if (_hWrite.read())
		    {
		      _state = SLAVE_WRITE;

		      if (_waitCount != 0)
			{
			  _hReadyOut.write32(0);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  _hRData.writeBig(NULL);
			  _hSplitx.write32(0);
			}
		      else
			{
			  _hReadyOut.write32(1);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  _hRData.writeBig(NULL);
			  _hSplitx.write32(0);
			}
		    }
		  else
		    {
		      _state = SLAVE_READ;

		      if (_waitCount != 0)
			{
			  _hReadyOut.write32(0);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  _hRData.writeBig(NULL);
			  _hSplitx.write32(0);
			}
		      else
			{
			  _hReadyOut.write32(1);
			  _hResp.write32(CarbonXAhbTrans_OKAY);
			  prepareHRData();
			  _hSplitx.write32(0);
			}
		    }
		}
	      else if (_hTrans.read() == CarbonXAhbTrans_SEQ)
		{
		  // start of a new data phase, restart wait count
		  _waitCount = _trans->get_slave_wait_states();
		}
	      else if (_hTrans.read() == CarbonXAhbTrans_BUSY)
		{
		  // we don't wait for busy access
		  _waitCount = 0;
		}
	      else
		// CarbonXAhbTrans_IDLE
		{
		  // end of this transaction, call setResponse()
		  _setResponseCB(_trans, _userInstance);
		  delete _trans;

		  _state = SLAVE_IDLE;

		  _hReadyOut.write32(1);
		  _hResp.write32(CarbonXAhbTrans_OKAY);
		  _hRData.writeBig(NULL);
		}
	    }
	  else
	    // hSel == 0, no more access to us
	    {
	      _setResponseCB(_trans, _userInstance);
	      delete _trans;

	      _state = SLAVE_IDLE;

	      // idle the bus slave xtor is driving
	      _hReadyOut.write32(1);
	      _hResp.write32(CarbonXAhbTrans_OKAY);
	      _hRData.writeBig(NULL);
	      _hSplitx.write32(0);
	    }
	}

      if (_waitCount != 0)
	{
	  _hReadyOut.write32(0);
	  _hResp.write32(CarbonXAhbTrans_OKAY);
	  _hRData.writeBig(NULL);

	  -- _waitCount;
	}
      else
	{
	  _hReadyOut.write32(1);
	  _hResp.write32(CarbonXAhbTrans_OKAY);
	}

      break;

    case SLAVE_NOT_OK:
      if (_hReadyIn.read())
	{
	  if (_trans->get_resp_type() == CarbonXAhbTrans_SPLIT)
	    // if split, need to send "split req" to arbiter
	    {
	      _splitWaitCount = _trans->get_slave_wait_states();
	      _split = true;
	    }

	  _state = SLAVE_IDLE;

	  _hResp.write32(CarbonXAhbTrans_OKAY);
	}

      _hReadyOut.write32(1);

      break;

    case SLAVE_SPLIT_REQ:
      if (_waitCount != 0)
	{
	  _hReadyOut.write32(0);
	  _hResp.write32(CarbonXAhbTrans_OKAY);
	  _hRData.writeBig(NULL);

	  -- _waitCount;
	}
      else
	{
	  // signal split req to arbiter for one clock cycle
	  _hSplitx.write32(to_splitx());

	  // reset split master
	  _splitMaster = 0;
	}

      break;

    default:
      _state = SLAVE_IDLE;
    }

  if (_hReadyIn.read() && _hSel.read())
    {
      _prevTransType = (AhbTransType_t) _hTrans.read();
      _prevBurstType = (AhbBurstType_t) _hBurst.read();
    }

  if (_split) {
    if (_splitWaitCount != 0)
      {
	_splitWaitCount --;
      }
    else
      {
	// when split wait count reaches 0, we simply "split req" the bus
	_hSplitx.write32(to_splitx());
	_splitMaster = 0;

	_split = false;
      }
  }

  return true;
}

void AhbSlaveXtor::refreshInputs()
{
  // the following pins are inputs to this xtor from Carbon model
  // clock & reset
  _hClk.examine();
  _hReset_n.examine();

  _hSel.examine();
  _hAddr.examine();
  _hWrite.examine();
  _hTrans.examine();
  _hSize.examine();
  _hBurst.examine();
  _hWData.examine();
  _hReadyIn.examine();
  _hMaster.examine();
  _hMastLock.examine();
}

void AhbSlaveXtor::refreshOutputs()
{
  // the following pins are driven from this xtor to Carbon model
  _hReadyOut.deposit();
  _hResp.deposit();
  _hRData.deposit();
  _hSplitx.deposit();
}

void AhbSlaveXtor::prepareHRData()
{
  CarbonUInt32 bus_byte_size  = (_hRData.get_width() >> 3);
  CarbonUInt32 xfer_byte_size = _trans->get_xfer_byte_size();

  CarbonUInt32 bus_alignment_bits  = AhbTransaction::get_alignment_bits(bus_byte_size);
  CarbonUInt32 xfer_alignment_bits = AhbTransaction::get_alignment_bits(xfer_byte_size);

  CarbonUInt8 * data_ptr = _trans->get_data_ptr() + _trans->get_data_length();
  //cout << "AhbSlaveXtor::prepareHRData() 0x" << hex << ((CarbonUInt32 *) data_ptr)[0] << " at 0x" << _addr << dec << endl;
  //cout << "\tdata_length = 0x" << hex << _trans->get_data_length() << dec << endl;

  CarbonUInt64 bus_alignment_mask = (1 << bus_alignment_bits) - 1;

  CarbonUInt32 lane_index = ((_addr & bus_alignment_mask) >> xfer_alignment_bits);

  CarbonUInt32 start_byte_index = lane_index * xfer_byte_size;
  CarbonUInt32 end_byte_index = start_byte_index + xfer_byte_size - 1;

  for(CarbonUInt32 i = 0; i < bus_byte_size; i++)
    reinterpret_cast<CarbonUInt8 *> (_hRData.get_value())[i] = ((start_byte_index <= i) && (i <= end_byte_index)) ?
      data_ptr[i - start_byte_index] : 0;

  // adjust data length
  _trans->set_data_length(_trans->get_data_length() + xfer_byte_size);
}

void AhbSlaveXtor::extractHWData()
{
  CarbonUInt32 bus_byte_size  = (_hRData.get_width() >> 3);
  CarbonUInt32 xfer_byte_size = _trans->get_xfer_byte_size();

  CarbonUInt32 bus_alignment_bits  = AhbTransaction::get_alignment_bits(bus_byte_size);
  CarbonUInt32 xfer_alignment_bits = AhbTransaction::get_alignment_bits(xfer_byte_size);

  CarbonUInt32 offset = _trans->get_data_length();
  CarbonUInt8 * data_ptr = _trans->get_data_ptr() + offset;

  //cout << "AhbSlaveXtor::extractHWData() 0x" << hex << _hWData.read() << " at 0x" << _writeAddr << dec << endl;
  //cout << "\toffset = 0x" << hex << offset << dec << endl;

  CarbonUInt64 bus_alignment_mask = (1 << bus_alignment_bits) - 1;

  CarbonUInt32 lane_index = ((_writeAddr & bus_alignment_mask) >> xfer_alignment_bits);

  CarbonUInt32 start_byte_index = lane_index * xfer_byte_size;
  CarbonUInt32 end_byte_index = start_byte_index + xfer_byte_size;

  for(CarbonUInt32 i = start_byte_index; i < end_byte_index; i++)
    data_ptr[i - start_byte_index] = reinterpret_cast<CarbonUInt8 *> (_hWData.get_value())[i];

  // adjust data length
  _trans->set_data_length(_trans->get_data_length() + xfer_byte_size);
}

bool AhbSlaveXtor::bind(CarbonXInterconnectNameNamePair * name_list,
			CarbonObjectID * model_handle)
{
  bool bound = true;

  // first bind TransactorSignalName<-> ModelSignalName for all pins
  for (CarbonUInt32 i = 0; name_list[i].TransactorSignalName != NULL; i++)
    {
      CarbonXInterconnectNameNamePair pin_name_pair = name_list[i];

      if (! bind(& pin_name_pair))
	bound = false;
    }

  // then bind to carbon model
  return (bound && bind(model_handle));
}

bool AhbSlaveXtor::bind(CarbonXInterconnectNameNamePair * pin_name_pair)
{
  bool bound;

  if (! strcmp(pin_name_pair->TransactorSignalName, "hReset_n"))
    {
      setModelSignalNamehReset_n(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hClk"))
    {
      setModelSignalNamehClk(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hSel"))
    {
      setModelSignalNamehSel(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hAddr"))
    {
      setModelSignalNamehAddr(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hWrite"))
    {
      setModelSignalNamehWrite(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hTrans"))
    {
      setModelSignalNamehTrans(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hSize"))
    {
      setModelSignalNamehSize(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hBurst"))
    {
      setModelSignalNamehBurst(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hWData"))
    {
      setModelSignalNamehWData(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hReadyIn"))
    {
      setModelSignalNamehReadyIn(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hMaster"))
    {
      setModelSignalNamehMaster(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hMastLock"))
    {
      setModelSignalNamehMastLock(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hReadyOut"))
    {
      setModelSignalNamehReadyOut(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hResp"))
    {
      setModelSignalNamehResp(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hRData"))
    {
      setModelSignalNamehRData(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hSplitx"))
    {
      setModelSignalNamehSplitx(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else
    bound = false;

  return bound;
}

CarbonUInt32 AhbSlaveXtor::to_splitx() const
{
  CarbonUInt32 splitx = 0;

  switch (_splitMaster)
    {
    case 0:
      splitx = 0x1;
      break;
    case 1:
      splitx = 0x2;
      break;
    case 2:
      splitx = 0x4;
      break;
    case 3:
      splitx = 0x8;
      break;
    case 4:
      splitx = 0x10;
      break;
    case 5:
      splitx = 0x20;
      break;
    case 6:
      splitx = 0x40;
      break;
    case 7:
      splitx = 0x80;
      break;
    case 8:
      splitx = 0x100;
      break;
    case 9:
      splitx = 0x200;
      break;
    case 10:
      splitx = 0x400;
      break;
    case 11:
      splitx = 0x800;
      break;
    case 12:
      splitx = 0x1000;
      break;
    case 13:
      splitx = 0x2000;
      break;
    case 14:
      splitx = 0x4000;
      break;
    case 15:
      splitx = 0x8000;
      break;
    default:
      break;
    }

  return splitx;
}
