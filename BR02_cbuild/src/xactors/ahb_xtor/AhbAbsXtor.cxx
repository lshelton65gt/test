//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "AhbAbsXtor.h"


AhbAbsXtor::~AhbAbsXtor()
{
}

void AhbAbsXtor::registerReportTransCB(callback_t ,
			   void * )
{
}

void AhbAbsXtor::registerResponseCB(callback_t ,
			callback_t ,
			void * )
{
}

bool AhbAbsXtor::startTransaction(AhbTransaction * )
{
  return true;
}
