// -*- mode: c++ -*-

#ifndef AHBSLAVEXTOR_H
#define AHBSLAVEXTOR_H

#include "carbon/c_memmanager.h"
#include "carbon/carbon_shelltypes.h"
#include "CarbonXPin.h"
#include "AhbTransaction.h"
#include "AhbAbsXtor.h"
#include "carbonXAhb.h"


class AhbSlaveXtor : public AhbAbsXtor
{
public: CARBONMEM_OVERRIDES

public:
  AhbSlaveXtor(const AhbConfig_t & ahb_cfg);
  // Constructor from AhbConfig

  ~AhbSlaveXtor();
  // Destructor

  bool bind(CarbonXInterconnectNameNamePair * name_list, 
	    CarbonObjectID * model_handle);
  // Bind xtor pins according to name_list. If succeeded return true, otherwise return false.

  void registerResponseCB(callback_t setDefaultResponseCB,
			  callback_t setResponseCB,
			  void * userInstance);
  // callback registration for slave xtor


  /* Evaluation and refresh functions */
  virtual bool evaluate();
  virtual void refreshInputs();
  virtual void refreshOutputs();


  /* Callback registation function */
  void setDefaultResponse(AhbTransaction *trans);
  void setResponse(AhbTransaction *trans);
  void reportTransaction(AhbTransaction *trans);


private:
  bool bind(CarbonXInterconnectNameNamePair * pin_name_pair);
  // bind the xtor pin designated by pin_name_pair

  bool bind(CarbonObjectID * carbonModelHandle);
  // Bind this Xtor to a Carbon model. If succeeded return true, otherwise return false.

  /* Functions to set cabon model signals for this Xtor. */
  void setModelSignalNamehReset_n(const char * name)  { _hReset_n.setModelSignalName(name); }
  void setModelSignalNamehClk(const char * name)      { _hClk.setModelSignalName(name); }
  void setModelSignalNamehSel(const char * name)      { _hSel.setModelSignalName(name); }
  void setModelSignalNamehAddr(const char * name)     { _hAddr.setModelSignalName(name); }
  void setModelSignalNamehWrite(const char * name)    { _hWrite.setModelSignalName(name); }
  void setModelSignalNamehTrans(const char * name)    { _hTrans.setModelSignalName(name); }
  void setModelSignalNamehSize(const char * name)     { _hSize.setModelSignalName(name); }
  void setModelSignalNamehBurst(const char * name)    { _hBurst.setModelSignalName(name); }
  void setModelSignalNamehWData(const char * name)    { _hWData.setModelSignalName(name); }
  void setModelSignalNamehReadyIn(const char * name)  { _hReadyIn.setModelSignalName(name); }
  void setModelSignalNamehMaster(const char * name)   { _hMaster.setModelSignalName(name); }
  void setModelSignalNamehMastLock(const char * name) { _hMastLock.setModelSignalName(name); }
  void setModelSignalNamehReadyOut(const char * name) { _hReadyOut.setModelSignalName(name); }
  void setModelSignalNamehResp(const char * name)     { _hResp.setModelSignalName(name); }
  void setModelSignalNamehRData(const char * name)    { _hRData.setModelSignalName(name); }
  void setModelSignalNamehSplitx(const char * name)   { _hSplitx.setModelSignalName(name); }

  AhbAddressMode_t get_addr_mode() const;
  // get address mode of this xtor

  CarbonUInt32 to_splitx() const;
  // from _splitMaster to calculate hSplitx value


  // Transactor state
  enum XtorState {
    SLAVE_IDLE,
    SLAVE_NOT_OK,
    SLAVE_SPLIT_REQ,
    SLAVE_READ,
    SLAVE_WRITE
  };

  XtorState _state;
  // xtor state

  AhbTransType_t _prevTransType;
  AhbBurstType_t _prevBurstType;
  // previous trans type on the bus

  CarbonUInt32 _waitCount;
  // slave wait count

  CarbonUInt32 _splitWaitCount;
  // split wait count

  CarbonUInt32 _splitMaster;
  // split master if any

  bool _split;
  // split issued

  AhbTransaction * _trans;
  // the trans this xtor is working on

  CarbonUInt64 _addr;
  // current access address recorded on the bus

  CarbonUInt64 _writeAddr;
  // 

  bool _refreshInputs;
  // any time this xtor write to carbon model, this variable is set to true.

  // Callback function handles
  callback_t _setDefaultResponseCB;
  callback_t _setResponseCB;


  void prepareHRData();
  // based on _tran info (_addr, _size) and current bus activity calculate _hRData

  void extractHWData();
  // based on _tran info (_addr, _size) and current bus activity read _hWData


  void * _userInstance;
  // User instance

  CarbonUInt8 data[128];

  // Ahb slave input signals for Carbon model
  CarbonXPin  _hReset_n;
  CarbonXPin  _hClk;
  CarbonXPin  _hSel;
  CarbonXPin  _hAddr;
  CarbonXPin  _hWrite;
  CarbonXPin  _hTrans;
  CarbonXPin  _hSize;
  CarbonXPin  _hBurst;
  CarbonXPin  _hWData;
  CarbonXPin  _hReadyIn;
  CarbonXPin  _hMaster;
  CarbonXPin  _hMastLock;

  // Ahb slave output signals
  CarbonXPin  _hReadyOut;
  CarbonXPin  _hResp;
  CarbonXPin  _hRData;
  CarbonXPin  _hSplitx;
};


#endif
