// -*- mode: c++ -*-

#ifndef CARBONXPIN_H
#define CARBONXPIN_H

#include "carbon/c_memmanager.h"
#include "xactors/carbonX.h"


enum CarbonXPinDirection {
    CarbonXPin_Unknown,
    CarbonXPin_Input,
    CarbonXPin_Output,
    CarbonXPin_Inout
};


class CarbonXPin
{
public: CARBONMEM_OVERRIDES

public:
  // Constructor and destructor
  CarbonXPin(const char * transactorSignalName, const char * modelSignalName, CarbonXPinDirection dir, const CarbonUInt32 width = 1);
  CarbonXPin(const char * transactorSignalName, CarbonXPinDirection dir, const CarbonUInt32 width = 1);
  ~CarbonXPin();


  bool bind(CarbonObjectID * carbonModelHandle);
  // Environment binding - establish the association between xctor pin and carbon model net

  CarbonUInt32 read() const;
  // Read carbon model net's value and return it. This only works for nets less than 32-bit.


  void write32(CarbonUInt32 value);
  void write64(CarbonUInt64 value);
  void writeBig(CarbonUInt32 * value);
  // write value to the xctor side, caller is responsible for width match

  void deposit();
  // deposit xctor side _value to carbon model net, valid only for input


  void examine();
  // read carbon model net's value into this pin

  void examine(unsigned char * value) const;
  // read HDL value into this pin and store it in place pointed to by value



  void setModelSignalName(const char * modelSignalName);
  const char * getModelSignalName() { return _modelSignalName; }

  CarbonUInt32 get_width() const { return _width; }
  CarbonUInt32 width() const { return _width; }

  CarbonUInt32 * get_value() const { return _value; }

private:
  char * _transactorSignalName;
  // this pin's associated xctor (side) signal name

  char * _modelSignalName;
  // this pin's associated carbon model net name

  CarbonXPinDirection  _dir;
  // direction of its Carbon model net

  const CarbonUInt32 _width;
  // pin width

  CarbonUInt32 * _value;
  // xctor (side) value to be written to carbon model net for deposit() - write port
  // or for examine() - read port

  /* Carbon variables */
  CarbonObjectID * _carbonModelHandle;
  CarbonNetID *    _carbonNetID;
};


#endif
