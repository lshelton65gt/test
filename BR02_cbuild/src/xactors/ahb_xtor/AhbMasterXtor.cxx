// -*- mode: c++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stddef.h>
#include <string.h>
#include <stdio.h>

#include "AhbMasterXtor.h"

AhbMasterXtor::AhbMasterXtor(const AhbConfig_t & ahb_cfg)
  : AhbAbsXtor(ahb_cfg.xtor_name, CarbonXAhb_Master),
    _state (MASTER_IDLE),
    _prevState (MASTER_IDLE),
    _terminated (false),
    _waitCount (0),
    _waitRData (false),
    _transQueue (ahb_cfg.trans_queue_size),
    _currTransItem (NULL),
    _addrPhaseCount(0),
    _xferByteCount (0),
    _lastData (false),
    _readAddr (0),
    // ahb master signals
    _hReset_n("hReset_n", CarbonXPin_Input),
    _hClk("hClk", CarbonXPin_Input),

    _hReady("hReady", CarbonXPin_Input),
    _hResp("hResp", CarbonXPin_Input, 2),
    _hRData("hRData", CarbonXPin_Input, ahb_cfg.data_width),
    // ahb master port request
    _hBusReq("hBusReq", CarbonXPin_Output),
    _hGrant("hGrant", CarbonXPin_Input),

    _hLock("hLock", CarbonXPin_Input),
    _hTrans("hTrans", CarbonXPin_Input, 2),
    _hAddr("hAddr", CarbonXPin_Input, ahb_cfg.addr_width),
    _hWrite("hWrite", CarbonXPin_Input),
    _hSize("hSize", CarbonXPin_Input, 3),
    _hBurst("hBurst", CarbonXPin_Input, 3),
    _hProt("hProt", CarbonXPin_Input),
    _hWData("hWData", CarbonXPin_Input, ahb_cfg.data_width),
    _ahbLite (ahb_cfg.ahb_lite)
{ }

AhbMasterXtor::~AhbMasterXtor()
{ }

bool AhbMasterXtor::bind(CarbonXInterconnectNameNamePair * name_list,
                        CarbonObjectID * model_handle)
{
  bool bound = true;

  // first bind TransactorSignalName<-> ModelSignalName for all pins
  for (CarbonUInt32 i = 0; name_list[i].TransactorSignalName != NULL; i++)
    {
      CarbonXInterconnectNameNamePair pin_name_pair = name_list[i];

      if (! bind(& pin_name_pair))
        bound = false;
    }

  // then bind to carbon model
  return (bound && bind(model_handle));
}

bool AhbMasterXtor::bind(CarbonObjectID * carbonModelHandle)
{
  bool bound = true;

  // ahb master bus
  bound &= _hReset_n.bind( carbonModelHandle );
  bound &= _hClk.bind( carbonModelHandle );
  bound &= _hReady.bind( carbonModelHandle );
  bound &= _hResp.bind( carbonModelHandle );
  bound &= _hRData.bind( carbonModelHandle );
  // ahb master port request
  bound &= _hBusReq.bind( carbonModelHandle );
  bound &= _hGrant.bind( carbonModelHandle );
  bound &= _hLock.bind( carbonModelHandle );
  bound &= _hTrans.bind( carbonModelHandle );
  bound &= _hAddr.bind( carbonModelHandle );
  bound &= _hWrite.bind( carbonModelHandle );
  bound &= _hSize.bind( carbonModelHandle );
  bound &= _hBurst.bind( carbonModelHandle );
  bound &= _hProt.bind( carbonModelHandle );
  bound &= _hWData.bind( carbonModelHandle );

  return bound;
}

bool AhbMasterXtor::startTransaction(AhbTransaction * trans)
{
  // push the tran to its queue
  return _transQueue.push(trans);
}

void AhbMasterXtor::registerReportTransCB(void (*reportTransCB)(AhbTransaction*, void*),
					  void * userInstance)
{
  _reportTransactionCB = reportTransCB;
  _userInstance = userInstance;
}

bool AhbMasterXtor::evaluate()
{
  switch(_state)
    {
    case MASTER_IDLE:
      if (! _transQueue.isEmpty())
	{
	  // we always process the top (oldest) of the queue
	  _currTransItem = _transQueue.top();

	  if (_addrPhaseCount == 0)
	    {
	      // we only update the counts when we know that the prev trans has finished
	      // otherwise we resume from where we stopped
	      _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();
	      _xferByteCount = 0;
	      _terminated = false;
	    }

	  if (_hGrant.read())
	    // we already have the bus
	    {
	      if (_hReady.read())
		// and bus happen to be ready
		{
		  _state = MASTER_ADDR_PHASE;
		  _prevState = MASTER_IDLE;

		  // drives NONSEQ address phase, not necessarily the first
		  {
		    _waitCount = _currTransItem->trans()->get_master_wait_states();

		    _hWrite.write32( _currTransItem->trans()->get_access_type() );

		    if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		      _hAddr.write32(calHAddr());
		    else
		      _hAddr.write64(calHAddr());

		    _hTrans.write32( CarbonXAhbTrans_NONSEQ );
		    if (_xferByteCount == 0)
		      // beginning of a new trans
		      _hBurst.write32( _currTransItem->trans()->get_burst_type() );
		    else
		      // this is a terminated xfer
		      _hBurst.write32 (CarbonXAhbTrans_INCR);
		    _hSize.write32( _currTransItem->trans()->get_size() );
		    _hProt.write32 (_currTransItem->trans()->get_prot_type() );
		  }

		  if (_addrPhaseCount > 1)
		    // current trans has not been finished yet - more addr phases to come
		    {
		      _hBusReq.write32 (1);
		      _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
		    }
		  else if (_currTransItem->next_trans() != NULL)
		    // we request for the next trans since currTransItem is done
		    {
		      _hBusReq.write32 (1);
		      _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
		    }
		  // no need to reset req/lock, since we are already in reset state
		}
	      else
		// but the bus is not hReady, we request the bus for currTransItem
		{
		  if (! _ahbLite)
		    {
		      _state = MASTER_BUSREQ;
		      _prevState = MASTER_IDLE;

		      // we are requesting the bus for currTrans
		      _hBusReq.write32(1);
		      _hLock.write32(_currTransItem->trans()->get_lock_type());
		    }
		}
	    }
	  else
	    {
	      // when code reaches here, it must not be ahbLite, 
	      _state = MASTER_BUSREQ;
	      _prevState = MASTER_IDLE;

	      // we are requesting the bus for currTrans
	      _hBusReq.write32(1);
	      _hLock.write32(_currTransItem->trans()->get_lock_type());
	    }
	}
      else
	{
	  // idle the bus
	  _hTrans.write32(CarbonXAhbTrans_IDLE);

	  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
	    _hAddr.write32(0);
	  else
	    _hAddr.write64(0);

	  _hWrite.write32(0);
	  _hSize.write32(CarbonXAhbTrans_SIZE8);
	  _hBurst.write32(CarbonXAhbTrans_SINGLE);
	  _hProt.write32(0);
	  _hWData.writeBig(NULL);

	  _hBusReq.write32(0);
	  _hLock.write32(CarbonXAhbTrans_UNLOCKED);
	}

      break;

    case MASTER_BUSREQ:
      if (_hGrant.read() && _hReady.read())
	{
	  _state = MASTER_ADDR_PHASE;
	  _prevState = MASTER_BUSREQ;

	  // drives NONSEQ address phase, not necessarily the first
	  {
	    _waitCount = _currTransItem->trans()->get_master_wait_states();

	    _hWrite.write32( _currTransItem->trans()->get_access_type() );

	    if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
	      _hAddr.write32(calHAddr());
	    else
	      _hAddr.write64(calHAddr());

	    _hTrans.write32( CarbonXAhbTrans_NONSEQ );
	    if (_xferByteCount == 0)
	      _hBurst.write32( _currTransItem->trans()->get_burst_type() );
	    else
	      // this is a terminated xfer
	      _hBurst.write32 (CarbonXAhbTrans_INCR);
	    _hSize.write32( _currTransItem->trans()->get_size() );
	    _hProt.write32 (_currTransItem->trans()->get_prot_type() );

	    if (_addrPhaseCount > 1)
	      // current trans has not been finished yet - more addr phases to come
	      {
		_hBusReq.write32 (1);
		_hLock.write32 ( _currTransItem->trans()->get_lock_type() );
	      }
	    else if (_currTransItem->next_trans() != NULL)
	      // we request for the next trans since currTransItem is done
	      {
		_hBusReq.write32 (1);
		_hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
	      }
	    else
	      // we are done, stop requesting the bus
	      {
		_hBusReq.write32 (0);
		_hLock.write32 (CarbonXAhbTrans_UNLOCKED);
	      }
	  }
	}

      break;

    case MASTER_ADDR_PHASE:
      if (_hReady.read())
	// NONSEQ address phase is done, not necessarily the first of a trans
	{
	  if (_currTransItem->trans()->get_access_type() == CarbonXAhbTrans_WRITE)
	    // put write data on the bus
	    writeHWData();
	  else
	    {
	      // we record a read address
	      _readAddr = _hAddr.read();
	      _waitRData = true;

	      _hWData.writeBig(NULL);
	    }

	  if (_hGrant.read())
	    // we still have the bus
	    {
	      if (_addrPhaseCount > 1)
		// currTrans is a burst transfer and is allowed to continue
		{
		  _waitCount = _currTransItem->trans()->get_master_wait_states();

		  // continuing the burst on the bus
		  if (_waitCount == 0)
		    {
		      _state = MASTER_ADDR_DATA_PHASE;
		      _prevState = MASTER_ADDR_PHASE;

		      if (isWrapAddr() && _terminated)
			_hTrans.write32( CarbonXAhbTrans_NONSEQ );
		      else
			_hTrans.write32( CarbonXAhbTrans_SEQ );
		    }
		  else if (isWrapAddr() && _terminated)
		    {
		      _state = MASTER_ADDR_DATA_PHASE;
		      _prevState = MASTER_ADDR_PHASE;

		      _hTrans.write32( CarbonXAhbTrans_NONSEQ );
		    }
		  else
		    {
		      _state = MASTER_BUSY;
		      _prevState = MASTER_ADDR_PHASE;

		      _hTrans.write32( CarbonXAhbTrans_BUSY );
		    }

		  // continuing the access, we don't need to update control signals except addr
		  _addrPhaseCount --;
		  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		    _hAddr.write32(calHAddr());
		  else
		    _hAddr.write64(calHAddr());

		  // keep requesting the bus for currTrans by default
		}
	      else
		// currTrans is in its last addr phase
		{
		  if (_currTransItem->next_trans() != NULL)
		    // we've got more transactions, we go ahead with the next one
		    {
		      _state = MASTER_DATA_ADDR_PHASE;
		      _prevState = MASTER_ADDR_PHASE;

		      // we move to the next trans item
		      _currTransItem = _currTransItem->_next;
		      _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();
		      _xferByteCount = 0;

		      // drives the 1st addr phase of the new trans
		      {
			_waitCount = _currTransItem->trans()->get_master_wait_states();

			_hWrite.write32( _currTransItem->trans()->get_access_type() );

			if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
			  _hAddr.write32(calHAddr());
			else
			  _hAddr.write64(calHAddr());

			_hTrans.write32( CarbonXAhbTrans_NONSEQ );
			_hBurst.write32( _currTransItem->trans()->get_burst_type() );
			_hSize.write32( _currTransItem->trans()->get_size() );
			_hProt.write32 (_currTransItem->trans()->get_prot_type() );

			if (_addrPhaseCount > 1)
			  // current trans is a burst xfer - more addr phases to come
			  {
			    _hBusReq.write32 (1);
			    _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
			  }
			else if (_currTransItem->next_trans() != NULL)
			  // we request for the next trans since request for currTransItem is done
			  {
			    _hBusReq.write32 (1);
			    _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
			  }
			else
			  // we are done, stop requesting the bus
			  {
			    _hBusReq.write32 (0);
			    _hLock.write32 (CarbonXAhbTrans_UNLOCKED);
			  }
		      }
		    }
		  else
		    // we are done with transactions, we go to data phase and stop requesting the bus
		    {
		      _state = MASTER_DATA_PHASE;
		      _prevState = MASTER_ADDR_PHASE;

		      // idle the bus except _hWData
		      _hWrite.write32(0);
		      _hTrans.write32(CarbonXAhbTrans_IDLE);

		      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
			_hAddr.write32(0);
		      else
			_hAddr.write64(0);

		      _hSize.write32(CarbonXAhbTrans_SIZE8);
		      _hBurst.write32(CarbonXAhbTrans_SINGLE);
		      _hProt.write32(0);

		      // deassert bus req if not ahb lite mode
		      _hBusReq.write32(0);
		      _hLock.write32(CarbonXAhbTrans_UNLOCKED);

		      _addrPhaseCount --;
		    }
		}
	    }
	  else
	    // we lost the bus
	    {
	      // we can only go to data phase
	      _state = MASTER_DATA_PHASE;
	      _prevState = MASTER_ADDR_PHASE;

	      _terminated = true;

	      _hTrans.write32(CarbonXAhbTrans_IDLE);

	      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		_hAddr.write32(0);
	      else
		_hAddr.write64(0);

	      _hWrite.write32(CarbonXAhbTrans_READ);
	      _hSize.write32(CarbonXAhbTrans_SIZE8);
	      _hBurst.write32(CarbonXAhbTrans_SINGLE);
	      _hProt.write32(0);

	      // request the bus appropriately
	      if (_addrPhaseCount > 1)
		// current trans has not been finished yet
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
		}
	      else if (_currTransItem->next_trans() != NULL)
		// we request for the next trans since currTrans is done
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
		}
	      else
		// we are done, stop requesting the bus
		{
		  _hBusReq.write32 (0);
		  _hLock.write32 (CarbonXAhbTrans_UNLOCKED);
		}

	      _addrPhaseCount --;
	    }
	}

      break;

    case MASTER_BUSY:
      if (_waitCount != 0)
	_waitCount --;

      if (_hResp.read() != CarbonXAhbTrans_OKAY)
	// error, retry or split response from slave
	{
	  _state = MASTER_SLV_NOT_OKAY;
	  _prevState = MASTER_BUSY;

	  // we drive the bus to idle
	  _hWrite.write32(0);
	  _hTrans.write32(CarbonXAhbTrans_IDLE);

	  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
	    _hAddr.write32(0);
	  else
	    _hAddr.write64(0);

	  _hSize.write32(CarbonXAhbTrans_SIZE8);
	  _hBurst.write32(CarbonXAhbTrans_SINGLE);
	  _hProt.write32(0);
	  _hWData.writeBig(NULL);

	  // we then move back to the trans which received error, retry or split response 
	  _currTransItem = _transQueue.top();
	  _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();

	  // and re-request the bus for it
	  _hBusReq.write32(1);
	  _hLock.write32(_currTransItem->trans()->get_lock_type());
	}
      else if (_hReady.read())
	{
	  // if a read access, read data is ready
	  if (_waitRData)
	    {
	      readHRData();

	      _waitRData = false;
	    }

	  if (_hGrant.read())
	    // we still have the bus
	    {
	      if (_waitCount == 0)
		// we wait until waitCount equals 0
		{
		  _state = MASTER_ADDR_DATA_PHASE;
		  _prevState = MASTER_BUSY;

		  // drive the bus - only _hTrans
		  _hTrans.write32(CarbonXAhbTrans_SEQ);

		  // we also drive 0 on hWData
		  _hWData.writeBig(NULL);
		}
	      else
		// we stay in busy but drive 0 to hWData
		{
		  _state = MASTER_BUSY;
		  _prevState = MASTER_BUSY;

		  _hWData.writeBig(NULL);
		}
	    }
	  else
	    // we lost the bus, by default we keep requesting it
	    {
	      _state = MASTER_IDLE;
	      _prevState = MASTER_BUSY;

	      _terminated = true;

	      // idle the bus
	      {
		if (! _ahbLite)
		  _hBusReq.write32(0);

		_hLock.write32(CarbonXAhbTrans_UNLOCKED);
		_hTrans.write32(CarbonXAhbTrans_IDLE);

		if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		  _hAddr.write32(0);
		else
		  _hAddr.write64(0);

		_hWrite.write32(0);
		_hSize.write32(CarbonXAhbTrans_SIZE8);
		_hBurst.write32(CarbonXAhbTrans_SINGLE);
		_hProt.write32(0);
		_hWData.writeBig(NULL);
	      }
	    }
	}

      break;

    case MASTER_DATA_ADDR_PHASE:
      // last (or 1st for single xfer) data phase of one trans and 1st addr phase of the next one
      if (_hResp.read() != CarbonXAhbTrans_OKAY)
	// error, retry or split response from slave
	{
	  _state = MASTER_SLV_NOT_OKAY;
	  _prevState = MASTER_DATA_ADDR_PHASE;

	  // we drive the bus to idle
	  _hWrite.write32(0);
	  _hTrans.write32(CarbonXAhbTrans_IDLE);

	  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
	    _hAddr.write32(0);
	  else
	    _hAddr.write64(0);

	  _hSize.write32(CarbonXAhbTrans_SIZE8);
	  _hBurst.write32(CarbonXAhbTrans_SINGLE);
	  _hProt.write32(0);
	  _hWData.writeBig(NULL);

	  // we then move back to the trans which received error, retry or split response 
	  _currTransItem = _transQueue.top();
	  _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();

	  // and re-request the bus for it
	  _hBusReq.write32(1);
	  _hLock.write32(_currTransItem->trans()->get_lock_type());
	}
      else if (_hReady.read())
	// okay response and bus ready
	{
	  /* data phase handling */
	  if (_waitRData)
	    // a read trans data phase finishes
	    {
	      readHRData();
	      _waitRData = false;
	    }

	  // we report a trans is done
	  (*_reportTransactionCB)(_transQueue.pop(), _userInstance);

	  // in this state, we are guaranted we have more transactions
	  _currTransItem = _transQueue.top();
	  _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();
	  _xferByteCount = 0;
	  _terminated = false;

	  /* addr phase */
	  if (_hGrant.read())
	    // we still have the bus
	    {
	      if (_currTransItem->trans()->get_access_type() == CarbonXAhbTrans_WRITE)
		// put write data on the bus ignoring waitCount
		writeHWData();
	      else
		{
		  // we record a read access
		  _waitRData = true;
		  _readAddr = _hAddr.read();

		  _hWData.writeBig(NULL);
		}

	      if (_addrPhaseCount > 1)
		// currTrans is a burst transfer and is allowed to continue
		{
		  _waitCount = _currTransItem->trans()->get_master_wait_states();

		  // continuing the burst on the bus
		  if (_waitCount == 0)
		    {
		      _state = MASTER_ADDR_DATA_PHASE;
		      _prevState = MASTER_DATA_ADDR_PHASE;

		      _hTrans.write32( CarbonXAhbTrans_SEQ );
		    }
		  else
		    {
		      _state = MASTER_BUSY;
		      _prevState = MASTER_DATA_ADDR_PHASE;

		      _hTrans.write32( CarbonXAhbTrans_BUSY );
		    }

		  // continuing the access, we don't need to update control signals except addr
		  _addrPhaseCount --;
		  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		    _hAddr.write32(calHAddr());
		  else
		    _hAddr.write64(calHAddr());

		  // keep requesting the bus for currTrans by default

		  // put write data on the bus or record the read addr
		  //if (_currTransItem->trans()->get_access_type() == CarbonXAhbTrans_WRITE)
		  //  writeHWData();
		  //else
		  if (_currTransItem->trans()->get_access_type() == CarbonXAhbTrans_READ)
		    _readAddr = _hAddr.read();
		}
	      else
		// currTrans is in its last addr phase
		{
		  if (_currTransItem->next_trans() != NULL)
		    // we've got more transactions, we go ahead with the next one
		    {
		      _state = MASTER_DATA_ADDR_PHASE;
		      _prevState = MASTER_DATA_ADDR_PHASE;

		      // we move to the next trans item
		      _currTransItem = _currTransItem->_next;
		      _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();
		      _xferByteCount = 0;

		      // drives the 1st addr phase of the new trans
		      {
			_waitCount = _currTransItem->trans()->get_master_wait_states();

			_hWrite.write32( _currTransItem->trans()->get_access_type() );

			if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
			  _hAddr.write32(calHAddr());
			else
			  _hAddr.write64(calHAddr());

			_hTrans.write32( CarbonXAhbTrans_NONSEQ );
			_hBurst.write32( _currTransItem->trans()->get_burst_type() );
			_hSize.write32( _currTransItem->trans()->get_size() );
			_hProt.write32 (_currTransItem->trans()->get_prot_type() );

			if (_addrPhaseCount > 1)
			  // current trans is a burst xfer - more addr phases to come
			  {
			    _hBusReq.write32 (1);
			    _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
			  }
			else if (_currTransItem->next_trans() != NULL)
			  // we request for the next trans since request for currTransItem is done
			  {
			    _hBusReq.write32 (1);
			    _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
			  }
			else
			  // we are done, stop requesting the bus
			  {
			    _hBusReq.write32 (0);
			    _hLock.write32 (CarbonXAhbTrans_UNLOCKED);
			  }
		      }
		    }
		  else
		    // we are done with transactions, we go to data phase and stop requesting the bus
		    {
		      _state = MASTER_DATA_PHASE;
		      _prevState = MASTER_DATA_ADDR_PHASE;

		      _addrPhaseCount --;
		      // put write data on the bus or record the read addr
		      if (_currTransItem->trans()->get_access_type() == CarbonXAhbTrans_WRITE)
			writeHWData();
		      else
			{
			  _readAddr = _hAddr.read();
			  _waitRData = true;
			  _hWData.writeBig(NULL);
			}

		      // idle the bus except _hWData
		      _hWrite.write32(0);
		      _hTrans.write32(CarbonXAhbTrans_IDLE);

		      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
			_hAddr.write32(0);
		      else
			_hAddr.write64(0);

		      _hSize.write32(CarbonXAhbTrans_SIZE8);
		      _hBurst.write32(CarbonXAhbTrans_SINGLE);
		      _hProt.write32(0);

		      // deassert bus req if not ahb lite mode
		      _hBusReq.write32(0);
		      _hLock.write32(CarbonXAhbTrans_UNLOCKED);
		    }
		}
	    }
	  else
	    // we lost the bus
	    {
	      // we can only go to data phase
	      _state = MASTER_DATA_PHASE;
	      _prevState = MASTER_DATA_ADDR_PHASE;

	      _terminated = true;

	      if (_currTransItem->trans()->get_access_type() == CarbonXAhbTrans_WRITE)
		writeHWData();
	      else
		{
		  _readAddr = _hAddr.read();
		  _waitRData = true;
		  _hWData.writeBig(NULL);
		}

	      _hTrans.write32(CarbonXAhbTrans_IDLE);

	      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		_hAddr.write32(0);
	      else
		_hAddr.write64(0);

	      _hWrite.write32(CarbonXAhbTrans_READ);
	      _hSize.write32(CarbonXAhbTrans_SIZE8);
	      _hBurst.write32(CarbonXAhbTrans_SINGLE);
	      _hProt.write32(0);

	      // request the bus appropriately
	      if (_addrPhaseCount > 1)
		// current trans has not been finished yet
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
		}
	      else if (_currTransItem->next_trans() != NULL)
		// we request for the next trans since currTrans is done
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
		}
	      else
		// we are done, stop requesting the bus
		{
		  _hBusReq.write32 (0);
		  _hLock.write32 (CarbonXAhbTrans_UNLOCKED);
		}

	      _addrPhaseCount --;
	    }
	}

      break;

    case MASTER_SLV_NOT_OKAY:
      if (_hReady.read())
	// we wait for bus ready, in theory we stay here for only one clock cycle.
	{
	  // we only report error trans
	  if (_hResp.read() == CarbonXAhbTrans_ERROR)
	    {
	      _transQueue.top()->trans()->set_resp_type((AhbRespType_t) _hResp.read());

	      (*_reportTransactionCB)(_transQueue.pop(), _userInstance);

	      // we move to the next trans
	      if (_transQueue.top() != NULL)
		{
		  _currTransItem = _transQueue.top();
		  _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();
		}
	      else
		{
		  _currTransItem = NULL;
		  _addrPhaseCount = 0;
		}

	      // in case we removed an read transaction
	      _waitRData = false;
	    }

	  // we reset xferByteCount
	  _xferByteCount = 0;

	  if (_hGrant.read())
	    // we still have the bus
	    {
	      if (_addrPhaseCount > 0)
		// and we have more transfers
		{
		  // we retry currTransItem - drives 1st address phase
		  _state = MASTER_ADDR_PHASE;
		  _prevState = MASTER_SLV_NOT_OKAY;

		  {
		    _waitCount = _currTransItem->trans()->get_master_wait_states();

		    _hWrite.write32( _currTransItem->trans()->get_access_type() );

		    if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		      _hAddr.write32(calHAddr());
		    else
		      _hAddr.write64(calHAddr());

		    _hTrans.write32( CarbonXAhbTrans_NONSEQ );
		    _hBurst.write32( _currTransItem->trans()->get_burst_type() );
		    _hSize.write32( _currTransItem->trans()->get_size() );
		    _hProt.write32 (_currTransItem->trans()->get_prot_type() );
		  }
		}
	      else
		// we are done
		{
		  _state = MASTER_IDLE;
		  _prevState = MASTER_SLV_NOT_OKAY;

		  // we idle the bus
		  _hTrans.write32(CarbonXAhbTrans_IDLE);

		  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		    _hAddr.write32(0);
		  else
		    _hAddr.write64(0);

		  _hWrite.write32(CarbonXAhbTrans_READ);
		  _hSize.write32(CarbonXAhbTrans_SIZE8);
		  _hBurst.write32(CarbonXAhbTrans_SINGLE);
		  _hProt.write32(0);
		}

	      if (_addrPhaseCount > 1)
		// current trans has not been finished yet - more addr phases to come
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
		}
	      else if (_currTransItem != NULL && _currTransItem->next_trans() != NULL)
		// we request for the next trans since currTransItem is done
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
		}
	      else
		// we are done, stop requesting the bus
		{
		  _hBusReq.write32 (0);
		  _hLock.write32 (CarbonXAhbTrans_UNLOCKED);
		}
	    }
	  else
	    // we lost the bus, we have to request it
	    {
	      if (_addrPhaseCount > 0)
		{
		  _state = MASTER_BUSREQ;
		  _prevState = MASTER_SLV_NOT_OKAY;

		  // but request the bus appropriately
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
		}
	      else
		// we are done
		{
		  _state = MASTER_IDLE;
		  _prevState = MASTER_SLV_NOT_OKAY;
		}

	      // we idle the bus
	      _hTrans.write32(CarbonXAhbTrans_IDLE);

	      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		_hAddr.write32(0);
	      else
		_hAddr.write64(0);

	      _hWrite.write32(CarbonXAhbTrans_READ);
	      _hSize.write32(CarbonXAhbTrans_SIZE8);
	      _hBurst.write32(CarbonXAhbTrans_SINGLE);
	      _hProt.write32(0);
	    }
	}

      break;

    case MASTER_ADDR_DATA_PHASE:
      // this is within a burst xfer
      if (_hResp.read() != CarbonXAhbTrans_OKAY)
	// error, retry or split response from slave - this happens in the 1st data phase of a burst xfer
	{
	  _state = MASTER_SLV_NOT_OKAY;
	  _prevState = MASTER_ADDR_DATA_PHASE;

	  // we drive the bus to idle
	  _hWrite.write32(0);
	  _hTrans.write32(CarbonXAhbTrans_IDLE);

	  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
	    _hAddr.write32(0);
	  else
	    _hAddr.write64(0);

	  _hSize.write32(CarbonXAhbTrans_SIZE8);
	  _hBurst.write32(CarbonXAhbTrans_SINGLE);
	  _hProt.write32(0);
	  _hWData.writeBig(NULL);

	  // we then move back to the trans which received error, retry or split response 
	  _currTransItem = _transQueue.top();
	  _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();

	  // and re-request the bus for it
	  _hBusReq.write32(1);
	  _hLock.write32(_currTransItem->trans()->get_lock_type());
	}
      else if (_hReady.read())
	// one more addr and data phase are doneand phase 
	{
	  /* data phase handling */
	  if (_waitRData)
	    // store read back data if we are waiting for read data
	    {
	      readHRData();
	      _waitRData = false;
	    }

	  if (_currTransItem->trans()->get_access_type() == CarbonXAhbTrans_WRITE)
	    // put write data on the bus ignoring waitCount
	    writeHWData();
	  else
	    {
	      // we record a read access
	      _waitRData = true;
	      _readAddr = _hAddr.read();
	    }

	  /* next state - similar to MASTER_ADDR_PHASE */
	  if (_hGrant.read())
	    // we still have the bus
	    {
	      if (_addrPhaseCount > 1)
		// currTrans is a burst transfer and is allowed to continue
		{
		  _waitCount = _currTransItem->trans()->get_master_wait_states();

		  // continuing the burst on the bus
		  if (_waitCount == 0)
		    {
		      _state = MASTER_ADDR_DATA_PHASE;
		      _prevState = MASTER_ADDR_DATA_PHASE;

		      if (isWrapAddr() && _terminated)
			_hTrans.write32( CarbonXAhbTrans_NONSEQ );
		      else
			_hTrans.write32( CarbonXAhbTrans_SEQ );
		    }
		  else if (isWrapAddr() && _terminated)
		    {
		      _state = MASTER_ADDR_DATA_PHASE;
		      _prevState = MASTER_ADDR_DATA_PHASE;

		      _hTrans.write32( CarbonXAhbTrans_NONSEQ );
		    }
		  else
		    {
		      _state = MASTER_BUSY;
		      _prevState = MASTER_ADDR_DATA_PHASE;

		      _hTrans.write32( CarbonXAhbTrans_BUSY );
		    }

		  // continuing the access, we don't need to update control signals except addr
		  _addrPhaseCount --;
		  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		    _hAddr.write32(calHAddr());
		  else
		    _hAddr.write64(calHAddr());

		  // keep requesting the bus for currTrans by default
		}
	      else
		// currTrans is in its last addr phase
		{
		  if (_currTransItem->next_trans() != NULL)
		    // we've got more transactions, we go ahead with the next one
		    {
		      _state = MASTER_DATA_ADDR_PHASE;
		      _prevState = MASTER_ADDR_DATA_PHASE;

		      // we move to the next trans item
		      _currTransItem = _currTransItem->_next;
		      _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();

		      // drives the 1st addr phase of the new trans
		      {
			_waitCount = _currTransItem->trans()->get_master_wait_states();

			_hWrite.write32( _currTransItem->trans()->get_access_type() );

			if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
			  _hAddr.write32(calHAddr());
			else
			  _hAddr.write64(calHAddr());

			_hTrans.write32( CarbonXAhbTrans_NONSEQ );
			_hBurst.write32( _currTransItem->trans()->get_burst_type() );
			_hSize.write32( _currTransItem->trans()->get_size() );
			_hProt.write32 (_currTransItem->trans()->get_prot_type() );

			if (_addrPhaseCount > 1)
			  // current trans is a burst xfer - more addr phases to come
			  {
			    _hBusReq.write32 (1);
			    _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
			  }
			else if (_currTransItem->next_trans() != NULL)
			  // we request for the next trans since request for currTransItem is done
			  {
			    _hBusReq.write32 (1);
			    _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
			  }
			else
			  // we are done, stop requesting the bus
			  {
			    _hBusReq.write32 (0);
			    _hLock.write32 (CarbonXAhbTrans_UNLOCKED);
			  }
		      }
		    }
		  else
		    // we are done with transactions, we go to data phase and stop requesting the bus
		    {
		      _state = MASTER_DATA_PHASE;
		      _prevState = MASTER_ADDR_PHASE;

		      // idle the bus except _hWData
		      _hWrite.write32(0);
		      _hTrans.write32(CarbonXAhbTrans_IDLE);

		      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
			_hAddr.write32(0);
		      else
			_hAddr.write64(0);

		      _hSize.write32(CarbonXAhbTrans_SIZE8);
		      _hBurst.write32(CarbonXAhbTrans_SINGLE);
		      _hProt.write32(0);

		      // deassert bus req if not ahb lite mode
		      _hBusReq.write32(0);
		      _hLock.write32(CarbonXAhbTrans_UNLOCKED);

		      _addrPhaseCount --;
		    }
		}
	    }
	  else
	    // we lost the bus
	    {
	      // we can only go to data phase
	      _state = MASTER_DATA_PHASE;
	      _prevState = MASTER_ADDR_PHASE;

	      _terminated = true;

	      _hTrans.write32(CarbonXAhbTrans_IDLE);

	      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		_hAddr.write32(0);
	      else
		_hAddr.write64(0);

	      _hWrite.write32(CarbonXAhbTrans_READ);
	      _hSize.write32(CarbonXAhbTrans_SIZE8);
	      _hBurst.write32(CarbonXAhbTrans_SINGLE);
	      _hProt.write32(0);

	      // request the bus appropriately
	      if (_addrPhaseCount > 1)
		// current trans has not been finished yet
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->trans()->get_lock_type() );

		  _terminated = true;
		}
	      else if (_currTransItem->next_trans() != NULL)
		// we request for the next trans since currTrans is done
		{
		  _hBusReq.write32 (1);
		  _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
		}
	      else
		// we are done, stop requesting the bus
		{
		  _hBusReq.write32 (0);
		  _hLock.write32 (CarbonXAhbTrans_UNLOCKED);
		}

	      _addrPhaseCount --;
	    }
	}

      break;

    case MASTER_DATA_PHASE:
      // last data phase of a trans or because master lost of bus
      if (_hResp.read() != CarbonXAhbTrans_OKAY)
	// error, retry or split response from slave
	{
	  _state = MASTER_SLV_NOT_OKAY;
	  _prevState = MASTER_DATA_PHASE;

	  // we drive the bus to idle
	  _hWrite.write32(0);
	  _hTrans.write32(CarbonXAhbTrans_IDLE);

	  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
	    _hAddr.write32(0);
	  else
	    _hAddr.write64(0);

	  _hSize.write32(CarbonXAhbTrans_SIZE8);
	  _hBurst.write32(CarbonXAhbTrans_SINGLE);
	  _hProt.write32(0);
	  _hWData.writeBig(NULL);

	  // we then move back to the trans which received error, retry or split response 
	  _currTransItem = _transQueue.top();
	  _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();

	  // and re-request the bus for it
	  _hBusReq.write32(1);
	  _hLock.write32(_currTransItem->trans()->get_lock_type());
	}
      else if (_hReady.read())
	// okay response and bus ready
	{
	  /* data phase */
	  // read the data back first
	  if (_waitRData)
	    {
	      readHRData(); // read should always work with top trans
	      _waitRData = false;
	    }

	  if (_addrPhaseCount == 0)
	    {
	      // we report finished trans at the end of its last data phase
	      (*_reportTransactionCB)(_transQueue.pop(), _userInstance);

	      _terminated = false;

	      if (! _transQueue.isEmpty())
		// and we got more transaction
		{
		  _currTransItem = _transQueue.top();

		  // we only update the counts when we know that the prev trans has finished
		  // otherwise we resume from where we stopped
		  _addrPhaseCount = _currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size();
		  _xferByteCount = 0;
		}
	    }

	  if (_hGrant.read())
	    // we again have the bus
	    {
	      if (_addrPhaseCount != 0)
		// we have more xfers
		{
		  _state = MASTER_ADDR_PHASE;
		  _prevState = MASTER_DATA_PHASE;

		  // drives NONSEQ address phase, not necessarily the first
		  {
		    _waitCount = _currTransItem->trans()->get_master_wait_states();

		    _hWrite.write32( _currTransItem->trans()->get_access_type() );
		    _hWData.writeBig(NULL);

		    if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		      _hAddr.write32(calHAddr());
		    else
		      _hAddr.write64(calHAddr());

		    _hTrans.write32( CarbonXAhbTrans_NONSEQ );
		    if (_xferByteCount == 0)
		      // beginning of a new trans
		      _hBurst.write32( _currTransItem->trans()->get_burst_type() );
		    else
		      // this is a terminated xfer
		      _hBurst.write32 (CarbonXAhbTrans_INCR);
		    _hSize.write32( _currTransItem->trans()->get_size() );
		    _hProt.write32 (_currTransItem->trans()->get_prot_type() );
		  }

		  // we request the bus appropriately
		  if (_addrPhaseCount > 1)
		    // current trans has not been finished yet - more addr phases to come
		    {
		      _hBusReq.write32 (1);
		      _hLock.write32 ( _currTransItem->trans()->get_lock_type() );
		    }
		  else if (_currTransItem->next_trans() != NULL)
		    // we request for the next trans since currTransItem is done
		    {
		      _hBusReq.write32 (1);
		      _hLock.write32 ( _currTransItem->next_trans()->get_lock_type() );
		    }
		  else
		    {
		      _hBusReq.write32 (0);
		      _hLock.write32 (0);
		    }
		}
	      else
		// we are done, no more xfers to 
		{
		  _state = MASTER_IDLE;
		  _prevState = MASTER_DATA_PHASE;

		  // idle the bus
		  _hWrite.write32(0);
		  _hTrans.write32(CarbonXAhbTrans_IDLE);

		  if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		    _hAddr.write32(0);
		  else
		    _hAddr.write64(0);

		  _hSize.write32(CarbonXAhbTrans_SIZE8);
		  _hBurst.write32(CarbonXAhbTrans_SINGLE);
		  _hProt.write32(0);
		  _hWData.writeBig(NULL);

		  _hBusReq.write32(0);
		  _hLock.write32(CarbonXAhbTrans_UNLOCKED);
		}
	    }
	  else if (_addrPhaseCount != 0)
	    // we got more xfers but don't have the bus
	    {
	      // request the bus
	      _hBusReq.write32(1);
	      _hLock.write32(_currTransItem->trans()->get_lock_type());
	    }
	  else
	    {
	      _state = MASTER_IDLE;
	      _prevState = MASTER_DATA_PHASE;

	      // idle the bus
	      _hTrans.write32(CarbonXAhbTrans_IDLE);

	      if (get_addr_mode() == CarbonXAhbTrans_ADDR32)
		_hAddr.write32(0);
	      else
		_hAddr.write64(0);
	      _hWrite.write32(0);
	      _hSize.write32(CarbonXAhbTrans_SIZE8);
	      _hBurst.write32(CarbonXAhbTrans_SINGLE);
	      _hProt.write32(0);
	      _hWData.writeBig(NULL);

	      _hBusReq.write32(0);
	      _hLock.write32(CarbonXAhbTrans_UNLOCKED);
	    }
	}

      break;

    default:
      _state = MASTER_IDLE;

      break;
    }

  return true;
}

void AhbMasterXtor::refreshInputs()
{
  // the following pins are inputs to this xtor from Carbon model
  // clock & reset
  _hClk.examine();
  _hReset_n.examine();

  // grant
  if (! _ahbLite)
    _hGrant.examine();
  else
    _hGrant.write32(1);  // hGrant is always one in lite mode

  // bus signals
  _hReady.examine();
  _hResp.examine();
  _hRData.examine();
}

void AhbMasterXtor::refreshOutputs()
{
  // the following pins are driven from this xtor to Carbon model
  if (! _ahbLite)
    _hBusReq.deposit();
  _hLock.deposit();
  _hTrans.deposit();
  _hAddr.deposit();
  _hWrite.deposit();
  _hSize.deposit();
  _hBurst.deposit();
  _hProt.deposit();
  _hWData.deposit();
}

bool AhbMasterXtor::bind(CarbonXInterconnectNameNamePair * pin_name_pair)
{
  bool bound;

  if (! strcmp(pin_name_pair->TransactorSignalName, "hReset_n"))
    {
      setModelSignalNamehReset_n(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hClk"))
    {
      setModelSignalNamehClk(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hReady"))
    {
      setModelSignalNamehReady(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hResp"))
    {
      setModelSignalNamehResp(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hRData"))
    {
      setModelSignalNamehRData(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if ((! _ahbLite) && (! strcmp(pin_name_pair->TransactorSignalName, "hBusReq")))
    {
      setModelSignalNamehBusReq(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if ((! _ahbLite) && (! strcmp(pin_name_pair->TransactorSignalName, "hGrant")))
    {
      setModelSignalNamehGrant(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hLock"))
    {
      setModelSignalNamehLock(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hTrans"))
    {
      setModelSignalNamehTrans(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hAddr"))
    {
      setModelSignalNamehAddr(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hWrite"))
    {
      setModelSignalNamehWrite(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hSize"))
    {
      setModelSignalNamehSize(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hBurst"))
    {
      setModelSignalNamehBurst(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hProt"))
    {
      setModelSignalNamehProt(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else if (! strcmp(pin_name_pair->TransactorSignalName, "hWData"))
    {
      setModelSignalNamehWData(pin_name_pair->ModelSignalName);
      bound = true;
    }
  else
    {
      sprintf(get_error_message(), "ERROR: Unknown Transactor Signal Name in pin-name pair - %s!\n",
	      pin_name_pair->TransactorSignalName);

      bound = false;
    }

  return bound;
}

AhbAddressMode_t AhbMasterXtor::get_addr_mode() const
{
  if (_hAddr.width() == 32)
    return CarbonXAhbTrans_ADDR32;
  else
    return CarbonXAhbTrans_ADDR64;
}

void AhbMasterXtor::writeHWData()
{
  CarbonUInt32 bus_byte_size  = (_hWData.get_width() >> 3);
  CarbonUInt32 xfer_byte_size = _currTransItem->trans()->get_xfer_byte_size();

  CarbonUInt32 bus_alignment_bits  = AhbTransaction::get_alignment_bits(bus_byte_size);
  CarbonUInt32 xfer_alignment_bits = AhbTransaction::get_alignment_bits(xfer_byte_size);

  CarbonUInt64 addr = _hAddr.read();

  CarbonUInt8 * data_ptr = _currTransItem->trans()->get_data_ptr() + _xferByteCount;

  CarbonUInt64 bus_alignment_mask = (1 << bus_alignment_bits) - 1;

  CarbonUInt32 lane_index = ((addr & bus_alignment_mask) >> xfer_alignment_bits);

  CarbonUInt32 start_byte_index = lane_index * xfer_byte_size;
  CarbonUInt32 end_byte_index = start_byte_index + xfer_byte_size - 1;

  for(CarbonUInt32 i = 0; i < bus_byte_size; i++)
    (reinterpret_cast<CarbonUInt8 *> (_hWData.get_value()))[i] = ((start_byte_index <= i) && (i <= end_byte_index)) ?
      data_ptr[i - start_byte_index] : 0;

  //cout << "AhbMasterXtor::writeHWData()" << endl;
  //cout << "\taddr: 0x" << hex << addr << dec << endl;
  //cout << "\tdata: 0x" << hex << ((CarbonUInt32 *) data_ptr)[0] << dec << endl;
  //cout << "\txferByteCount: 0x" << hex << _xferByteCount << dec << endl;

  // adjust counts
  _xferByteCount += xfer_byte_size;
}

void AhbMasterXtor::readHRData()
{
  CarbonUInt32 bus_byte_size  = (_hRData.get_width() >> 3);
  CarbonUInt32 xfer_byte_size = _transQueue.top()->trans()->get_xfer_byte_size();

  CarbonUInt32 bus_alignment_bits  = AhbTransaction::get_alignment_bits(bus_byte_size);
  CarbonUInt32 xfer_alignment_bits = AhbTransaction::get_alignment_bits(xfer_byte_size);

  CarbonUInt8 * data_ptr = _transQueue.top()->trans()->get_data_ptr() + _xferByteCount;

  CarbonUInt64 bus_alignment_mask = (1 << bus_alignment_bits) - 1;

  CarbonUInt32 lane_index = ((_readAddr & bus_alignment_mask) >> xfer_alignment_bits);

  CarbonUInt32 start_byte_index = lane_index * xfer_byte_size;
  CarbonUInt32 end_byte_index = start_byte_index + xfer_byte_size;

  for(CarbonUInt32 i = start_byte_index; i < end_byte_index; i++)
    data_ptr[i - start_byte_index] = reinterpret_cast<CarbonUInt8 *> (_hRData.get_value())[i];

  //cout << "\tAhbMasterXtor::readHRData() - 0x" << hex << _hRData.read() << " at 0x" << _readAddr << dec << endl;
  //cout << "\txferByteCount: 0x" << hex << _xferByteCount << dec << endl;

  // adjust counts
  _xferByteCount += xfer_byte_size;
}

CarbonUInt64 AhbMasterXtor::calHAddr() const
{
  //cout << "AhbMasterXtor::calHAddr() - _addrPhaseCount = " << _addrPhaseCount 
  //     << " at 0x" << hex << _currTransItem->trans()->get_address() << dec << endl;
  CarbonUInt64 base_addr = _currTransItem->trans()->get_address() & (~(CarbonUInt64)_currTransItem->trans()->get_wrap_mask());
  CarbonUInt64 offset = _currTransItem->trans()->get_address() & _currTransItem->trans()->get_wrap_mask();

  offset += (_currTransItem->trans()->get_data_length() / _currTransItem->trans()->get_byte_size() - _addrPhaseCount)
    * _currTransItem->trans()->get_xfer_byte_size();

  offset &= _currTransItem->trans()->get_wrap_mask();

  return base_addr + offset;
}

bool AhbMasterXtor::isWrapAddr() const
{
  bool wrap = false;
  CarbonUInt64 burst_mask = _currTransItem->trans()->get_exp_data_length() - 1;
  CarbonUInt64 xfer_mask = _currTransItem->trans()->get_xfer_byte_size() - 1;

  switch (_currTransItem->trans()->get_burst_type())
    {
    case CarbonXAhbTrans_WRAP4:
    case CarbonXAhbTrans_WRAP8:
    case CarbonXAhbTrans_WRAP16:
      wrap = ((calHAddr() & burst_mask & (~ xfer_mask)) == (burst_mask & (~ xfer_mask)));
      break;

    case CarbonXAhbTrans_SINGLE:
    case CarbonXAhbTrans_INCR:
    case CarbonXAhbTrans_INCR4:
    case CarbonXAhbTrans_INCR8:
    case CarbonXAhbTrans_INCR16:
      wrap = false;
      break;

    default:
      wrap = false;
      break;
    }

  return wrap;
}

CarbonUInt32 AhbMasterXtor::hasMoreXfers() const
{
  bool more_xfers = 0;

  if (_transQueue.hasMoreItems() || (_addrPhaseCount > 1))
    more_xfers = 1;

  return more_xfers;
}
