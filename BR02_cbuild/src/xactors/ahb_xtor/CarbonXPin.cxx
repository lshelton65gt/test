// -*- mode: c++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <string.h>
#include <stdio.h>

#include "CarbonXPin.h"
#include "AhbTransaction.h"
#include "carbon/carbon_capi.h"


CarbonXPin::CarbonXPin(const char * transactorSignalName,
		       const char * modelSignalName,
		       CarbonXPinDirection dir,
		       const CarbonUInt32 width)
  : _transactorSignalName( NULL ),
    _modelSignalName( NULL ),
    _dir( dir ),
    _width( width ),
    _carbonModelHandle( NULL ),
    _carbonNetID( NULL )
{
  _value = CARBON_ALLOC_VEC(CarbonUInt32, ((_width + 31) >> 5));

  _transactorSignalName = (char *) carbonmem_malloc(strlen(transactorSignalName) + 1);
  strcpy(_transactorSignalName, transactorSignalName);

  _modelSignalName = (char *) carbonmem_malloc(strlen(modelSignalName) + 1);
  strcpy(_modelSignalName, modelSignalName);
}

CarbonXPin::CarbonXPin(const char * transactorSignalName,
		       CarbonXPinDirection dir,
		       const CarbonUInt32 width)
  : _transactorSignalName( NULL ),
    _modelSignalName( NULL ),
    _dir( dir ),
    _width( width ),
    _carbonModelHandle( NULL ),
    _carbonNetID( NULL )
{
  _value = CARBON_ALLOC_VEC(CarbonUInt32, ((_width + 31) >> 5));

  _transactorSignalName = (char *) carbonmem_malloc(strlen(transactorSignalName) + 1);
  strcpy(_transactorSignalName, transactorSignalName);
}

CarbonXPin::~CarbonXPin()
{
  if (_transactorSignalName != NULL)
    carbonmem_free(_transactorSignalName);
  if (_modelSignalName != NULL)
    carbonmem_free(_modelSignalName);
  CARBON_FREE_VEC(_value, CarbonUInt32, (_width + 31) >> 5);
}

bool CarbonXPin::bind(CarbonObjectID * carbonModelHandle)
{
  if (_transactorSignalName == NULL)
    sprintf(get_error_message(), "ERROR: NULL transactor signal name supplied during pin binding!\n");
  else if (_modelSignalName == NULL)
    sprintf(get_error_message(), "ERROR: NULL model signal name supplied during pin binding!\n");
  else if (carbonModelHandle == NULL)
    sprintf(get_error_message(), "ERROR: NULL Carbon Model Handle supplied during pin binding\n");

  if(carbonModelHandle == NULL || _transactorSignalName == NULL
     || _modelSignalName == NULL)
    return false;

  _carbonModelHandle = carbonModelHandle;

  // Get the Net Handle from the Carbon model
  _carbonNetID = carbonFindNet(_carbonModelHandle, _modelSignalName);

  // Check that we got a valid net id handle
  if(_carbonNetID == NULL)
    {
      sprintf(get_error_message(), "ERROR: Can not find net %s in Carbon Model during pin binding!\n", _modelSignalName);

      return false;
    }

  return true;
}

CarbonUInt32 CarbonXPin::read() const
{
  return _value[0];
}

void CarbonXPin::write32(CarbonUInt32 value)
{
  _value[0] = value;
}

void CarbonXPin::write64(CarbonUInt64 value)
{
  _value[0] = static_cast<CarbonUInt32>(value & 0xFFFFFFFF);
  _value[1] = static_cast<CarbonUInt32>(value >> 32);
}

void CarbonXPin::writeBig(CarbonUInt32 * value)
{
  int count = ((_width + 31) >> 5);

  if (value == NULL)
    {
      // write all zeros to _value
      for(int i = 0; i < count; i++)
	_value[i] = 0;
    }
  else
    {
      for(int i = 0; i < count; i++)
	_value[i] = value[i];
    }
}

void CarbonXPin::deposit()
{
  carbonDeposit(_carbonModelHandle, _carbonNetID, _value, NULL);
}

void CarbonXPin::examine()
{
  carbonExamine(_carbonModelHandle, _carbonNetID, _value, NULL);
}

void CarbonXPin::examine(unsigned char * value) const
{
  // read HDL value from Carbon model
  carbonExamine(_carbonModelHandle, _carbonNetID, _value, NULL);

  // copy read value to external storage place
  int word_count = ((_width + 7) >> 5);

  for(int i = 0; i < word_count; i++)
    {
      // a word has 4 bytes
      value[i + 0] = (_value[i]) & 0xFF;
      value[i + 0] = (_value[i] >> 8) & 0xFF;
      value[i + 0] = (_value[i] >> 16) & 0xFF;
      value[i + 0] = (_value[i] >> 24) & 0xFF;
    }
}

void CarbonXPin::setModelSignalName(const char * modelSignalName)
{
  _modelSignalName = (char *) carbonmem_malloc(strlen(modelSignalName) + 1);
  strcpy(_modelSignalName, modelSignalName);
}
