// -*- mode: c++ -*-

#ifndef AHBMASTERXTOR_H
#define AHBMASTERXTOR_H

#include "carbon/c_memmanager.h"
#include "carbon/carbon_shelltypes.h"
#include "CarbonXPin.h"
#include "AhbTransaction.h"
#include "AhbTransQueue.h"
#include "AhbAbsXtor.h"
#include "carbonXAhb.h"


class AhbMasterXtor : public AhbAbsXtor
{
public: CARBONMEM_OVERRIDES

public:
  AhbMasterXtor(const AhbConfig_t & ahb_cfg);
  // Constructor from AhbConfig

  ~AhbMasterXtor();
  // Destructor

  void registerReportTransCB(callback_t reportTransCB, void * userInstance);
  // register report cb for master xtor

  bool bind(CarbonXInterconnectNameNamePair * name_list, 
	    CarbonObjectID * model_handle);
  // Bind xtor pins according to name_list. If succeeded return true, otherwise return false.


  bool startTransaction(AhbTransaction * trans);
  // 

  /* Evaluation and refresh functions */
  virtual bool evaluate();
  virtual void refreshInputs();
  virtual void refreshOutputs();

  /* Callback functions */
  void setDefaultResponse(AhbTransaction *trans);
  void setResponse(AhbTransaction *trans);
  void reportTransaction(AhbTransaction *trans);


private:
  bool bind(CarbonXInterconnectNameNamePair * pin_name_pair);
  // bind the xtor pin designated by pin_name_pair

  bool bind(CarbonObjectID * carbonModelHandle);
  // Bind this Xtor to a Carbon model. If succeeded return true, otherwise return false.

  /* Functions to set cabon model signals for this Xtor. */
  void setModelSignalNamehReset_n(const char * name) { _hReset_n.setModelSignalName(name); }
  void setModelSignalNamehClk(const char * name)     { _hClk.setModelSignalName(name); }
  void setModelSignalNamehReady(const char * name)   { _hReady.setModelSignalName(name); }
  void setModelSignalNamehResp(const char * name)    { _hResp.setModelSignalName(name); }
  void setModelSignalNamehRData(const char * name)   { _hRData.setModelSignalName(name); }
  void setModelSignalNamehBusReq(const char * name)  { _hBusReq.setModelSignalName(name); }
  void setModelSignalNamehGrant(const char * name)   { _hGrant.setModelSignalName(name); }
  void setModelSignalNamehLock(const char * name)    { _hLock.setModelSignalName(name); }
  void setModelSignalNamehTrans(const char * name)   { _hTrans.setModelSignalName(name); }
  void setModelSignalNamehAddr(const char * name)    { _hAddr.setModelSignalName(name); }
  void setModelSignalNamehWrite(const char * name)   { _hWrite.setModelSignalName(name); }
  void setModelSignalNamehSize(const char * name)    { _hSize.setModelSignalName(name); }
  void setModelSignalNamehBurst(const char * name)   { _hBurst.setModelSignalName(name); }
  void setModelSignalNamehProt(const char * name)    { _hProt.setModelSignalName(name); }
  void setModelSignalNamehWData(const char * name)   { _hWData.setModelSignalName(name); }

  AhbAddressMode_t get_addr_mode() const;
  // get address mode of this xtor

  void writeHWData();
  void readHRData();

  CarbonUInt64 calHAddr() const;
  // calculate hAddr

  bool isWrapAddr() const;
  // next addr is wrap-around addr

  CarbonUInt32 hasMoreXfers() const;
  // This xtor has more than one xfers pending

  enum XtorState {
    MASTER_IDLE,
    MASTER_BUSREQ,
    MASTER_ADDR_PHASE,
    MASTER_SLV_NOT_OKAY,
    MASTER_BUSY,
    MASTER_ADDR_DATA_PHASE,
    MASTER_DATA_ADDR_PHASE,
    MASTER_DATA_PHASE
  };
  /*
  enum XtorState {
    MASTER_IDLE = 0x0,
    MASTER_BUSY,
    MASTER_NONSEQ,
    MASTER_SEQ
  };
  */  
  XtorState _state;
  // updated state

  XtorState _prevState;
  // xtor state representing what's going on

  bool _terminated;
  // is _currTransItem terminated?

  CarbonUInt32 _waitCount;
  // wait count for BUSY state for burst transfer

  bool _waitRData;
  // master is waiting read data back

  AhbTransQueue _transQueue;
  // Transaction queue

  AhbTransQueueItem * _currTransItem;
  // current transaction

  //CarbonUInt32 _addrReqCount;
  // the number of remaining addr request for currTrans

  CarbonUInt32 _addrPhaseCount;
  // the number of remaining ADDR phases for currTrans

  CarbonUInt32 _xferByteCount;
  // _byteCount is the number of transferred data in bytes for _currTrans

  bool _lastData;

  CarbonUInt64 _readAddr;
  // current read data's access address recorded on the bus


  //  bool _refreshInputs;
  // any time this xtor write to carbon model, this variable is set to true.

  callback_t _reportTransactionCB;
  // Callback function handles

  void * _userInstance;
  // User instance

  // ahb master bus
  CarbonXPin  _hReset_n;
  CarbonXPin  _hClk;
  CarbonXPin  _hReady;
  CarbonXPin  _hResp;
  CarbonXPin  _hRData;
  // ahb master port request
  CarbonXPin  _hBusReq;
  CarbonXPin  _hGrant;
  CarbonXPin  _hLock;
  CarbonXPin  _hTrans;
  CarbonXPin  _hAddr;
  CarbonXPin  _hWrite;
  CarbonXPin  _hSize;
  CarbonXPin  _hBurst;
  CarbonXPin  _hProt;
  CarbonXPin  _hWData;

  bool _ahbLite;
  // ahb lite mode?
};


#endif
