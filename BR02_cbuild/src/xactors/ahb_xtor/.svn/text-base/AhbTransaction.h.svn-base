// -*- mode: c++ -*-

#ifndef AHBTRANSACTION_H
#define AHBTRANSACTION_H

#include "carbon/c_memmanager.h"
#include "carbon/carbon_shelltypes.h"
#include "carbonXAhb.h"


char * get_error_message();


class AhbTransaction
{
public: CARBONMEM_OVERRIDES


public:
  static CarbonUInt32 get_alignment_bits(CarbonUInt32 bytes);
  // This func returns the alignment addr bits for the number of bytes. Return value example:
  //   return_value,     bytes
  //         0             1
  //         1             2
  //         2             4
  //         3             8
  //         etc.

  AhbTransaction();
  // Default constructor

  AhbTransaction(AhbAccessType_t access, CarbonUInt64 addr,
		 AhbBurstType_t burst, AhbSizeType_t size);
  // Construct a transaction based on info from the initial addr phase on AHB bus

  AhbTransaction(AhbTransConfig_t & ahb_trans_cfg);
  // Construct a transaction from a trans config structure

  ~AhbTransaction();
  // Destructor.

  const AhbTransaction & operator = (const AhbTransaction & rhs);
  // Assignment operator.

  bool check() const;
  // Check consistency for this transaction



  void print() const;
  // print out trans info

  /* Access functions */
  AhbAccessType_t  get_access_type()     { return _accessType; }
  CarbonUInt64     get_address()         { return _addr; }
  CarbonUInt8 *    get_data_ptr()        { return _data; }
  CarbonUInt32     get_data_length()     { return _length; }
  CarbonUInt32     get_exp_data_length() const;
  AhbBurstType_t   get_burst_type()      { return _burstType; }
  AhbSizeType_t    get_size()            { return _size; }
  AhbSizeType_t    get_xfer_size()       { return _size; }
  CarbonUInt32     get_byte_size() const;
  CarbonUInt32     get_xfer_byte_size() const;
  AhbLockType_t    get_lock_type() const { return _locked; }
  AhbRespType_t    get_resp_type() const { return _respType; }
  CarbonUInt32     get_prot_type() const { return _protType; }
  CarbonUInt64     get_alignment_mask() const;

  void set_data_length(CarbonUInt32 length) { _length = length; }
  void set_resp_type(AhbRespType_t resp) { _respType = resp; }

  CarbonUInt32 get_master_wait_states() const { return _masterWaitStates; }
  CarbonUInt32 get_slave_wait_states() const { return _slaveWaitStates; }

  CarbonUInt32 get_wrap_mask() const { return _wrapAddrMask; }

  void set_master_wait_states(CarbonUInt32 wait_states) { _masterWaitStates = wait_states; }
  void set_slave_wait_states(CarbonUInt32 wait_states) { _slaveWaitStates = wait_states; }

private:
  AhbAccessType_t       _accessType;
  // type of this trans - Read or Write?

  CarbonUInt64          _addr;
  // address of this trans

  CarbonUInt8 *         _data;
  // data pointer of this trans - for READ, trans allocates its own 

  CarbonUInt32          _length;
  // length of this trans

  AhbBurstType_t        _burstType;
  // burst type of this trans - Single, Incr, Wrap4, etc.

  AhbSizeType_t         _size;
  // size of this trans on the AHB bus

  AhbLockType_t         _locked;
  // split capable here?

  CarbonUInt32          _protType;
  // prot type for this transsaction for master xtor only

  AhbRespType_t         _respType;
  // response type of this trans for slave xtor

  CarbonUInt32          _masterWaitStates;
  // Number of master waitstates, used with master transactor

  CarbonUInt32          _slaveWaitStates;
  // Number of slave waitstates, used with slave transactor

  CarbonUInt32 _wrapAddrMask;
  // addr bits to be wrapped in WRAP burst mode, internal use only
};


#endif
