// -*- mode: c++ -*-

#ifndef CARBONXAHB_H
#define CARBONXAHB_H

#include "xactors/carbonX.h"

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
#define STRUCT struct
#endif

  /*!
    User can querry global error message buffer for more info about the error.
    If there is no appropriate message for the error, a NULL pointer is returned.
    It is the user's responsibility to check that a NULL pointer is returned.
  */
  const char * carbonXAhbGetErrorMsg();

  //! data types and enum constants which user can use.
  enum AhbAddressMode_t { CarbonXAhbTrans_ADDR32 = 0x0, CarbonXAhbTrans_ADDR64 };
  enum AhbAccessType_t  { CarbonXAhbTrans_READ = 0x0, CarbonXAhbTrans_WRITE };
  enum AhbTransType_t   { CarbonXAhbTrans_IDLE = 0x0, CarbonXAhbTrans_BUSY, CarbonXAhbTrans_NONSEQ, CarbonXAhbTrans_SEQ };
  enum AhbBurstType_t   { CarbonXAhbTrans_SINGLE = 0x0, CarbonXAhbTrans_INCR, CarbonXAhbTrans_WRAP4, CarbonXAhbTrans_INCR4,
			  CarbonXAhbTrans_WRAP8, CarbonXAhbTrans_INCR8, CarbonXAhbTrans_WRAP16, CarbonXAhbTrans_INCR16 };
  enum AhbSizeType_t    { CarbonXAhbTrans_SIZE8 = 0x0, CarbonXAhbTrans_SIZE16, CarbonXAhbTrans_SIZE32, CarbonXAhbTrans_SIZE64,
			  CarbonXAhbTrans_SIZE128, CarbonXAhbTrans_SIZE256, CarbonXAhbTrans_SIZE512, CarbonXAhbTrans_SIZE1024 };
  enum AhbRespType_t    { CarbonXAhbTrans_OKAY = 0x0, CarbonXAhbTrans_ERROR, CarbonXAhbTrans_RETRY, CarbonXAhbTrans_SPLIT };
  enum AhbLockType_t    { CarbonXAhbTrans_UNLOCKED = 0x0, CarbonXAhbTrans_LOCKED = 0x1 };

  STRUCT AhbAbsXtor;
  STRUCT AhbTransaction;

  typedef struct {
    AhbAccessType_t  access;
    CarbonUInt64     addr;
    CarbonUInt8 *    data_ptr;
    CarbonUInt32     length;
    AhbBurstType_t   burstType;
    AhbSizeType_t    size;
    AhbLockType_t    locked;
    CarbonUInt32     protType;
    AhbRespType_t    respType;
    CarbonUInt32     master_wait_states;
    CarbonUInt32     slave_wait_states;
  } AhbTransConfig_t;

  /*!
    Create an AhbTransaction from ahb_trans_cfg, user must specify the length of a xfer.
    For write, user has to provide valid data_ptr. For read, user must set data_ptr to NULL.
    The requested trans has to be consistent with the requested xfer. For example,
      length = xfer_byte_size * BURSTS
  */
  AhbTransaction * carbonXAhbTransCreate(AhbTransConfig_t & ahb_trans_cfg);

  //! create a copy of a transaction
  AhbTransaction * carbonXAhbTransCreateCopy(AhbTransaction * trans);

  /*!
    Destroy the transaction pointed to by trans. This function needs to be called on
    every object created by both of the above two create functions.
  */
  void carbonXAhbTransDestroy(AhbTransaction * trans);

  //! access functions for an AhbTransaction object
  AhbAccessType_t  carbonXAhbTransGetAccessType(AhbTransaction *);
  CarbonUInt64     carbonXAhbTransGetAddress(AhbTransaction *);
  CarbonUInt8 *    carbonXAhbTransGetDataPtr(AhbTransaction *);
  CarbonUInt32     carbonXAhbTransGetDataLength(AhbTransaction *);
  CarbonUInt32     carbonXAhbTransGetExpDataLength(AhbTransaction *);
  AhbBurstType_t   carbonXAhbTransGetBurstType(AhbTransaction *);
  AhbSizeType_t    carbonXAhbTransGetSize(AhbTransaction *);
  CarbonUInt32     carbonXAhbTransGetByteSize(AhbTransaction *);
  AhbSizeType_t    carbonXAhbTransGetXferSize(AhbTransaction *);
  CarbonUInt32     carbonXAhbTransGetXferByteSize(AhbTransaction *);
  CarbonUInt32     carbonXAhbTransGetLockType(AhbTransaction *);
  AhbRespType_t    carbonXAhbTransGetRespType(AhbTransaction *);
  CarbonUInt64     carbonXAhbTransGetAlignmentMask(AhbTransaction *);

  void carbonXAhbTransSetSlaveResp(AhbTransaction * trans, AhbRespType_t resp);
  void carbonXAhbTransSetSlaveWaitStates(AhbTransaction * trans, CarbonUInt32 wait_states);


  //! Ahb xtor type - slave or master?
  typedef enum {
    CarbonXAhb_Master = 0,
    CarbonXAhb_Slave = 1
  } CarbonXAhbType_t;


  //! Ahb transactor config struct used when creating an Ahb transactor.
  typedef struct {
    //! xtor name of this config
    const char *      xtor_name;

    //! xtor type of this config - master or slave?
    CarbonXAhbType_t  xtor_type;

    //! width of the addr bus of the transactor - 32-bit or 64-bit?
    CarbonUInt32      addr_width;

    //! width of the data bus, which ca be one of 8-bit, 16-bit, ..., or 1024-bit.
    CarbonUInt32      data_width;

    //! transaction queue size in master xtor when created. Not used by slave xtor.
    CarbonUInt32      trans_queue_size;

    /*!
      Number of addr bits used to decode a byte location accessed through a slave 
      transactor. They are the lsbs from the address bus.
    */
    CarbonUInt32      dcd_addr_width;

    //! ahb lite mode for master xtor use only, not used by slave xtor.
    bool              ahb_lite;
  } AhbConfig_t;


  //! error report - TBD
  typedef enum {
    CarbonXAhbError_NoError = 0,
    CarbonXAxiError_InvalidTrans
  } CarbonXAhbErrorType;


  /*!
    Create an Ahb master or slave xtor based on parameters passed to this function.
    Callbacks shall also be passed. They are registered with the created xtor.
  */
  AhbAbsXtor * carbonXAhbCreate(AhbConfig_t ahb_config,
				void (*reportTransaction)(AhbTransaction *, void *),
				void (*setDefaultResponse)(AhbTransaction *, void *),
				void (*setResponse)(AhbTransaction *, void *),
				void * stimulusInstance);

  //! Created xtors need to be destroyed before program ends.
  void carbonXAhbDestroy(AhbAbsXtor * xtor);

  //! Get the name of the xtor.
  const char *carbonXAxiGetName(AhbAbsXtor * xtor);

  //! Get the type of the xtor - master or slave?
  CarbonXAhbType_t carbonXAxiGetType(AhbAbsXtor * xtor);

  //! Connect xtor to Carbon model according to nameList.
  CarbonUInt32 carbonXAhbConnectByModelSignalName(AhbAbsXtor * xtor,
						  CarbonXInterconnectNameNamePair * nameList,
						  CarbonObjectID * carbonModelHandle);

  /*!
    Start transaction to be initiated by an Ahb master xtor.
    If succeeded, return 1; otherwise return 0.
  */
  CarbonUInt32 carbonXAhbStartNewTransaction(AhbAbsXtor * xtor, 
					     AhbTransaction *trans);

  //! Evaluate a xtor at currentTime
  CarbonUInt32 carbonXAhbEvaluate(AhbAbsXtor * xtor);

  //! Refresh xtor's inputs
  void carbonXAhbRefreshInputs(AhbAbsXtor * xtor);

  //! Refresh xtor's outputs - update all output signal ports of the transactor
  void carbonXAhbRefreshOutputs(AhbAbsXtor * xtor);


#ifdef __cplusplus
}
#endif

#endif
