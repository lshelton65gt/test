// -*- mode: c++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stddef.h>
#include <stdio.h>

#include "carbonXAhb.h"
#include "AhbTransaction.h"
#include "AhbSlaveXtor.h"
#include "AhbMasterXtor.h"


static char error_msg[1024] = "";


char * get_error_message()
{
  return error_msg;
}

const char * carbonXAhbGetErrorMsg()
{
  return error_msg;
}

AhbTransaction * carbonXAhbTransCreate(AhbTransConfig_t & trans_cfg)
{
  if (trans_cfg.length == 0)
    {
      sprintf(error_msg, "ERROR: zero length transaction requested!\n");

      return NULL;
    }

  if (trans_cfg.data_ptr == NULL && trans_cfg.access == CarbonXAhbTrans_WRITE)
    {
      sprintf(error_msg, "ERROR: NULL data_ptr when requesting a write transaction!\n");

      return NULL;
    }

  bool consistent = true;

  CarbonUInt32 xfer_byte_size = 0;

  switch(trans_cfg.size)
    {
    case CarbonXAhbTrans_SIZE8:
      xfer_byte_size = 1;
      break;
    case CarbonXAhbTrans_SIZE16:
      xfer_byte_size = 2;
      break;
    case CarbonXAhbTrans_SIZE32:
      xfer_byte_size = 4;
      break;
    case CarbonXAhbTrans_SIZE64:
      xfer_byte_size = 8;
      break;
    case CarbonXAhbTrans_SIZE128:
      xfer_byte_size = 16;
      break;
    case CarbonXAhbTrans_SIZE256:
      xfer_byte_size = 32;
      break;
    case CarbonXAhbTrans_SIZE512:
      xfer_byte_size = 64;
      break;
    case CarbonXAhbTrans_SIZE1024:
      xfer_byte_size = 128;
      break;
    default:
      xfer_byte_size = 4;
      break;
    }


  // check for consistency of the ahb_trans_cfg
  CarbonUInt32 exp_data_length = 0;

  switch (trans_cfg.burstType)
    {
    case CarbonXAhbTrans_SINGLE:
      exp_data_length = xfer_byte_size;

      if (trans_cfg.length != exp_data_length)
	consistent = false;

      break;

    case CarbonXAhbTrans_INCR:
      // use max data length which is calculated based on "Burst must not cross 1KB address 
      // boundary" requirement from AMBA 2.0 spec. p.3-11
      exp_data_length = 1024 - (trans_cfg.addr & 0x3FF);

      if (trans_cfg.length > exp_data_length)
	consistent = false;

      break;

    case CarbonXAhbTrans_INCR4:
    case CarbonXAhbTrans_WRAP4:
      exp_data_length = 4 * xfer_byte_size;

      if (trans_cfg.length != exp_data_length)
	consistent = false;

      break;

    case CarbonXAhbTrans_INCR8:
    case CarbonXAhbTrans_WRAP8:
      exp_data_length = 8 * xfer_byte_size;

      if (trans_cfg.length != exp_data_length)
	consistent = false;

      break;

    case CarbonXAhbTrans_INCR16:
    case CarbonXAhbTrans_WRAP16:
      exp_data_length = 16 * xfer_byte_size;

      if (trans_cfg.length != exp_data_length)
	consistent = false;

      break;

    default:
      // shouldn't get here, if gets here, it is wrong
      consistent = false;

      break;
    }

  if (! consistent)
    {
      sprintf(error_msg,
	      "ERROR: Requested transaction length %d dose not match expected transaction length %d!\n",
	      trans_cfg.length, exp_data_length);

      return NULL;
    }

  // for write trans, user needs to set data_ptr and length.
  // for read trans, user must NOT provide a data_ptr. But length is necessary.
  // The trans created here takes care of memory allocation for read
  if ((trans_cfg.access == CarbonXAhbTrans_READ) && (trans_cfg.data_ptr != NULL))
    {
      sprintf(error_msg, "ERROR: data_ptr must be NULL for a read trans!\n");

      return NULL;
    }

  return new AhbTransaction(trans_cfg);
}

AhbTransaction * carbonXAhbTransCreateCopy(AhbTransaction * trans)
{
  AhbTransConfig_t trans_cfg;

  trans_cfg.access    = trans->get_access_type();
  trans_cfg.addr      = trans->get_address();
  trans_cfg.data_ptr  = trans->get_data_ptr();
  trans_cfg.length    = trans->get_data_length();
  trans_cfg.burstType = trans->get_burst_type();
  trans_cfg.size      = trans->get_size();
  trans_cfg.locked    = trans->get_lock_type();
  trans_cfg.protType  = trans->get_prot_type();
  trans_cfg.respType  = trans->get_resp_type();

  trans_cfg.master_wait_states = trans->get_master_wait_states();
  trans_cfg.slave_wait_states  = trans->get_slave_wait_states();

  AhbTransaction * trans_copy = new AhbTransaction(trans_cfg);

  if (trans_cfg.access == CarbonXAhbTrans_READ)
    {
      for(CarbonUInt32 i = 0; i < trans_cfg.length; i++)
	((CarbonUInt8 *)(trans_copy->get_data_ptr()))[i] = (trans_cfg.data_ptr)[i];
    }

  return trans_copy;
}

void carbonXAhbTransDestroy(AhbTransaction * trans)
{
  delete trans;
}

AhbAccessType_t carbonXAhbTransGetAccessType(AhbTransaction * trans)
{
  return trans->get_access_type();
}

CarbonUInt64 carbonXAhbTransGetAddress(AhbTransaction * trans)
{
  return trans->get_address();
}

CarbonUInt8 * carbonXAhbTransGetDataPtr(AhbTransaction * trans)
{
  return trans->get_data_ptr();
}

CarbonUInt32 carbonXAhbTransGetDataLength(AhbTransaction * trans)
{
  return trans->get_data_length();
}

CarbonUInt32 carbonXAhbTransGetExpDataLength(AhbTransaction * trans)
{
  return trans->get_exp_data_length();
}

AhbBurstType_t carbonXAhbTransGetBurstType(AhbTransaction * trans)
{
  return trans->get_burst_type();
}

AhbSizeType_t carbonXAhbTransGetSize(AhbTransaction * trans)
{
  return trans->get_size();
}

CarbonUInt32 carbonXAhbTransGetByteSize(AhbTransaction * trans)
{
  return trans->get_byte_size();
}

AhbSizeType_t carbonXAhbTransGetXferSize(AhbTransaction * trans)
{
  return trans->get_xfer_size();
}

CarbonUInt32 carbonXAhbTransGetXferByteSize(AhbTransaction * trans)
{
  return trans-> get_xfer_byte_size();
}

CarbonUInt32 carbonXAhbTransGetLockType(AhbTransaction * trans)
{
  return trans->get_lock_type();
}

AhbRespType_t carbonXAhbTransGetRespType(AhbTransaction * trans)
{
  return trans->get_resp_type();
}

CarbonUInt64 carbonXAhbTransGetAlignmentMask(AhbTransaction * trans)
{
  return trans->get_alignment_mask();
}

void carbonXAhbTransSetSlaveResp(AhbTransaction * trans, AhbRespType_t resp)
{
  trans->set_resp_type(resp);
}

/*
void carbonXAhbTransSetMasterWaitStates(AhbTransaction * trans, CarbonUInt32 wait_states)
{
  trans-> set_master_wait_states(wait_states);
}
*/

void carbonXAhbTransSetSlaveWaitStates(AhbTransaction * trans, CarbonUInt32 wait_states)
{
  trans-> set_slave_wait_states(wait_states);
}

AhbAbsXtor * carbonXAhbCreate(AhbConfig_t ahb_config,
			      void (*reportTransaction)(AhbTransaction *, void *),
			      void (*setDefaultResponse)(AhbTransaction *, void *),
			      void (*setResponse)(AhbTransaction *, void *),
			      void * stimulusInstance)
{
  AhbAbsXtor * xtor = NULL;

  if (ahb_config.xtor_type == CarbonXAhb_Master)
    {
      if (reportTransaction == NULL)
	sprintf(error_msg, "ERROR: NULL Report Transaction Callback supplied when creating an AHB master transactor!\n");
      else
	{
	  xtor = new AhbMasterXtor(ahb_config);
	  xtor->registerReportTransCB(reportTransaction, stimulusInstance);
	}
    }
  else
    {
      if (setDefaultResponse == NULL)
	sprintf(error_msg, "ERROR: NULL Set Default Response Callback supplied when creating an AHB slave transactor!\n");
      else if (setResponse == NULL)
	sprintf(error_msg, "ERROR: NULL Set Response Callback supplied when creating an AHB slave transactor!\n");
      else
	{
	  xtor = new AhbSlaveXtor(ahb_config);
	  xtor->registerResponseCB(setDefaultResponse, setResponse, stimulusInstance);
	}
    }

  return xtor;
}

void carbonXAhbDestroy(AhbAbsXtor * xtor)
{
  if (xtor != NULL)
    delete xtor;
}

CarbonUInt32 carbonXAhbConnectByModelSignalName(AhbAbsXtor * xtor,
						CarbonXInterconnectNameNamePair * nameList,
						CarbonObjectID * carbonModelHandle)
{
  if (carbonModelHandle == NULL)
    {
      sprintf(error_msg, "ERROR: NULL Carbon Model Handle supplied when calling carbonXAhbConnectByModelSignalName()!\n");

      return 0;
    }
  else
    return xtor->bind(nameList, carbonModelHandle);
}

const char *carbonXAhbGetName(AhbAbsXtor * xtor)
{
  return xtor->get_name();
}

CarbonXAhbType_t carbonXAhbGetType(AhbAbsXtor * xtor)
{
  return xtor->get_type();
}

CarbonUInt32 carbonXAhbStartNewTransaction(AhbAbsXtor * xtor, 
					   AhbTransaction * trans)
{
  if (xtor->get_type() == CarbonXAhb_Master)
    return ((AhbMasterXtor *)xtor)->startTransaction(trans);
  else
    {
      sprintf(error_msg, "ERROR: carbonXAhbStartNewTransaction() called on a slave transactor!\n");

      // slave xtor cannot start a transaction
      return 0;
    }
}

CarbonUInt32 carbonXAhbEvaluate(AhbAbsXtor * xtor)
{
  return xtor->evaluate();
}

void carbonXAhbRefreshInputs(AhbAbsXtor * xtor)
{
  xtor->refreshInputs();
}

void carbonXAhbRefreshOutputs(AhbAbsXtor * xtor)
{
  xtor->refreshOutputs();
}
