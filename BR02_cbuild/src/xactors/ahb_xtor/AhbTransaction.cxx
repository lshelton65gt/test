// -*- mode: c++ -*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stddef.h>

#include "AhbTransaction.h"


CarbonUInt32 AhbTransaction::get_alignment_bits(CarbonUInt32 bytes)
{
  CarbonUInt32 alignment = 0;

  while (bytes != (CarbonUInt32)(1 << alignment))
    alignment ++;

  return alignment;
}

AhbTransaction::AhbTransaction(AhbAccessType_t access, 
			       CarbonUInt64 addr,
			       AhbBurstType_t burst,
			       AhbSizeType_t size) :
  _accessType (access),
  _addr (addr),
  _burstType (burst),
  _size (size),
  _respType (CarbonXAhbTrans_OKAY),
  _masterWaitStates (0),
  _slaveWaitStates (0)
{
  CarbonUInt32 data_length = 0;

  switch(_burstType)
    {
    case CarbonXAhbTrans_SINGLE:
      data_length = get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR:
      // use max data length which is calculated based on "Burst must not cross 1KB address 
      // boundary" requirement from AMBA 2.0 spec. p.3-11
      data_length = 1024 - (_addr & 0x3FF);
      break;

    case CarbonXAhbTrans_INCR4:
    case CarbonXAhbTrans_WRAP4:
      data_length = 4 * get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR8:
    case CarbonXAhbTrans_WRAP8:
      data_length = 8 * get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR16:
    case CarbonXAhbTrans_WRAP16:
      data_length = 16 * get_xfer_byte_size();
      break;

    default:
      // shouldn't get here, even if it gets, we assign a default value
      data_length = 4;
      break;
    }

  // allocate data storage
  _data = (CarbonUInt8 *) carbonmem_malloc(data_length);

  // set length to 0 since this constructor is called by slave xtor
  _length = 0;
}

AhbTransaction::AhbTransaction(AhbTransConfig_t & ahb_trans_cfg)
  : _accessType (ahb_trans_cfg.access),
    _addr (ahb_trans_cfg.addr),
    _length (ahb_trans_cfg.length),
    _burstType (ahb_trans_cfg.burstType),
    _size (ahb_trans_cfg.size),
    _locked (ahb_trans_cfg.locked),
    _protType (ahb_trans_cfg.protType),
    _respType (ahb_trans_cfg.respType),
    _masterWaitStates (ahb_trans_cfg.master_wait_states),
    _slaveWaitStates (ahb_trans_cfg.slave_wait_states)
{
  // allocate data storage & set up length for this transaction
  _data = (CarbonUInt8 *) carbonmem_malloc(_length);

  if (_accessType == CarbonXAhbTrans_WRITE)
    {
      for(CarbonUInt32 i = 0; i < _length; i++)
	_data[i] = ahb_trans_cfg.data_ptr[i];
    }

  CarbonUInt32 wrap_length;

  switch (_burstType)
    {
    case CarbonXAhbTrans_WRAP4:
    case CarbonXAhbTrans_WRAP8:
    case CarbonXAhbTrans_WRAP16:
      wrap_length = get_exp_data_length();
      _wrapAddrMask = wrap_length - 1;
      break;

    default:
      _wrapAddrMask = 0xFFFFFFFF;
      break;
    }
}

AhbTransaction::~AhbTransaction()
{
  if (_data != NULL)
    carbonmem_free(_data);
}

CarbonUInt32 AhbTransaction::get_exp_data_length() const
{
  CarbonUInt32 data_length = 0;

  switch(_burstType)
    {
    case CarbonXAhbTrans_SINGLE:
      data_length = get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR:
      // use max data length which is calculated based on "Burst must not cross 1KB address 
      // boundary" requirement from AMBA 2.0 spec. p.3-11
      data_length = 1024 - (_addr & 0x3FF);
      break;

    case CarbonXAhbTrans_INCR4:
    case CarbonXAhbTrans_WRAP4:
      data_length = 4 * get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR8:
    case CarbonXAhbTrans_WRAP8:
      data_length = 8 * get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR16:
    case CarbonXAhbTrans_WRAP16:
      data_length = 16 * get_xfer_byte_size();
      break;

    default:
      // shouldn't get here, even if it gets, we assign a default value
      data_length = 4;
      break;
    }

  return data_length;
}

CarbonUInt32 AhbTransaction::get_byte_size() const
{
  switch(_size)
    {
    case CarbonXAhbTrans_SIZE8:
      return 1;
    case CarbonXAhbTrans_SIZE16:
      return 2;
    case CarbonXAhbTrans_SIZE32:
      return 4;
    case CarbonXAhbTrans_SIZE64:
      return 8;
    case CarbonXAhbTrans_SIZE128:
      return 16;
    case CarbonXAhbTrans_SIZE256:
      return 32;
    case CarbonXAhbTrans_SIZE512:
      return 64;
    case CarbonXAhbTrans_SIZE1024:
      return 128;
    default:
      return 4;
    }
}

CarbonUInt32 AhbTransaction::get_xfer_byte_size() const
{
  switch(_size)
    {
    case CarbonXAhbTrans_SIZE8:
      return 1;
    case CarbonXAhbTrans_SIZE16:
      return 2;
    case CarbonXAhbTrans_SIZE32:
      return 4;
    case CarbonXAhbTrans_SIZE64:
      return 8;
    case CarbonXAhbTrans_SIZE128:
      return 16;
    case CarbonXAhbTrans_SIZE256:
      return 32;
    case CarbonXAhbTrans_SIZE512:
      return 64;
    case CarbonXAhbTrans_SIZE1024:
      return 128;
    default:
      return 4;
    }
}

CarbonUInt64 AhbTransaction::get_alignment_mask() const
{
  CarbonUInt64 alignment_mask = 0xFFFFFFFFFFFFFFFFULL; //???

  CarbonUInt32 data_length = 0;

  switch (_burstType)
    {
    case CarbonXAhbTrans_WRAP4:
    case CarbonXAhbTrans_WRAP8:
    case CarbonXAhbTrans_WRAP16:
      data_length = get_exp_data_length();
      break;

    default:
      data_length = get_xfer_byte_size();
      break;
    }

  while (data_length >>= 1)
    alignment_mask <<= 1;

  return alignment_mask;
}

bool AhbTransaction::check() const
{
  CarbonUInt32 data_length = 0;

  switch(_burstType)
    {
    case CarbonXAhbTrans_SINGLE:
    case CarbonXAhbTrans_INCR:
      data_length = _length;
      break;

    case CarbonXAhbTrans_INCR4:
    case CarbonXAhbTrans_WRAP4:
      data_length = 4 * get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR8:
    case CarbonXAhbTrans_WRAP8:
      data_length = 8 * get_xfer_byte_size();
      break;

    case CarbonXAhbTrans_INCR16:
    case CarbonXAhbTrans_WRAP16:
      data_length = 16 * get_xfer_byte_size();
      break;

    default:
      // shouldn't get here, even if it gets, we assign a default value
      data_length = 4;
      break;
    }

  return data_length == _length;
}

void AhbTransaction::print() const
{
}

const AhbTransaction & AhbTransaction::operator = (const AhbTransaction & rhs)
{
  _accessType = rhs._accessType;
  _addr = rhs._addr;
  _length = rhs._length;
  _burstType = rhs._burstType;
  _size = rhs._size;

  if (_data != NULL)
    carbonmem_free(_data);

  _data = (CarbonUInt8 *) carbonmem_malloc(_length);
  for(CarbonUInt32 i = 0; i < _length; i++)
    _data[i] = rhs._data[i];

  return * this;
}
