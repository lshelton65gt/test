// -*- mode: c++ -*-

#ifndef AHBTRANSQUEUE_H
#define AHBTRANSQUEUE_H

#include "carbon/c_memmanager.h"
#include "AhbTransaction.h"


class AhbTransQueueItem
{
public: CARBONMEM_OVERRIDES

  AhbTransaction * const _trans; // Data item *
  AhbTransQueueItem * _prev;     // Link to previous node *
  AhbTransQueueItem * _next;     // Link to next node *

  AhbTransQueueItem(AhbTransaction * trans);
  ~AhbTransQueueItem();

  AhbTransaction * trans();
  AhbTransaction * next_trans();
};

class AhbTransQueue
{
public: CARBONMEM_OVERRIDES

private:
  CarbonUInt32 _size;
  // Max. no of items in queue

  CarbonUInt32 _count;
  // No of items in queue

  AhbTransQueueItem * _head;
  // Head of queue

  AhbTransQueueItem *_tail;
  // Tail of queue

public:
  AhbTransQueue(CarbonUInt32 size);
  ~AhbTransQueue();

  CarbonUInt32 get_size() const { return _size; }
  // get max size of the queue

  CarbonUInt32 get_count() const { return _count; }
  // gets the queue fill level

  bool isEmpty() const { return (_count == 0); }
  bool hasLastItem() const { return (_count == 1); }
  bool hasMoreItems() const { return (_count > 1); }
  bool isFull() const { return (_count == _size); }

  bool push(AhbTransaction *trans);
  // Push a new item

  AhbTransaction * pop();
  // Pop the oldest item

  AhbTransQueueItem * top();
  // Get the oldest item without poping

  //  AhbTransaction * next();
  // Get the 2nd oldest item, next of top item


  bool remove(AhbTransaction *trans);
  // Remove a particular item from the queue
};


#endif
