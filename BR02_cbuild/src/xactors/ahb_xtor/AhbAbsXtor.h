// -*- mode: c++ -*-

#ifndef AHBABSXTOR_H
#define AHBABSXTOR_H

#include "carbon/c_memmanager.h"
#include "carbonXAhb.h"


typedef void (* callback_t) (AhbTransaction*, void*);


class AhbAbsXtor
{
public: CARBONMEM_OVERRIDES

public:
  AhbAbsXtor(const char * name, CarbonXAhbType_t type) : _name(name), _type(type) {}
  virtual ~AhbAbsXtor();

  virtual void registerReportTransCB(callback_t reportTransCB,
				     void * userInstance);

  virtual void registerResponseCB(callback_t setDefaultResponseCB,
				  callback_t setResponseCB,
				  void * userInstance);

  virtual bool bind(CarbonXInterconnectNameNamePair * name_list, 
		    CarbonObjectID * model_handle) = 0;

  virtual bool startTransaction(AhbTransaction * trans);

  virtual void refreshInputs() = 0;
  virtual void refreshOutputs() = 0;
  virtual bool evaluate() = 0;

  /* access functions */
  const char * get_name() const { return _name; }
  CarbonXAhbType_t get_type() const { return _type; }

protected:
  const char *     _name;
  // name of this xtor

  CarbonXAhbType_t _type;
  // type of this xtor
};


#endif
