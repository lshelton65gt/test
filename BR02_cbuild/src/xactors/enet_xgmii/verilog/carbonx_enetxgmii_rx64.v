//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Description:  Synthesizable Ethernet Receiver BFM and attaches to the XGMII Tx
//		  interface of the DUT.
//                 This transactor module is a C-side initiated transactor and consist  
//		   of XIF_core and BFM. The rx transactor receives configuration data
//		   from XIF_core to set up modes.
//                 The only valid command is BFM_Read, to read a packet



`timescale 1ns / 100 ps

module  carbonx_enetxgmii_rx64
  (sys_clk,   
   sys_reset_l,
   TXD,      
   TXC, 
   TX_CLK 
   );

   
   // XGMII signals
   input        sys_clk, sys_reset_l;
   input [63:0] TXD;
   input [7:0] 	TXC;
   input        TX_CLK;

   // XAUI Configurations and Status Signals
   // These signals are only used when using the XAUI transactor
   reg [31:0] 	xaui_linkstatus; 		
   reg [1:0] 	xaui_code_error;

`protect
   // carbon license crbn_vsp_exec_xtor_e10g

   //**************************************************************************
   //                      Parameter Definition
   //**************************************************************************

   // State machine states
   parameter    IDLE         = 2'h0;
   parameter    RAW_DATA     = 2'h1;
   parameter    PREAMBLE     = 2'h2;
   parameter    DATA         = 2'h3;

   // Misc constants
   parameter    SQ_DATA      = 24'hF10000;  // !!! check value
   parameter    MIN_IPG      = 16'h3;

   parameter    IDL_CH         =  8'h07; // idle
   parameter    STR_CH         =  8'hFB; // start
   parameter    ERR_CH         =  8'hFE; // error
   parameter    TRM_CH         =  8'hFD; // terminate (efd)
   parameter    SFD_CH         =  8'hD5; // start frame delim (sfd)
   
   parameter    _PREAMBLE_     = 24'h555555; 

   parameter    PREAMBLE_L     = {_PREAMBLE_,STR_CH};           // 32'h5555_55FB - start char & preamble
   parameter    PREAMBLE_H     = {SFD_CH,_PREAMBLE_};           // 32'hD555_5555 - preamble & sfd char

   parameter    IDLE_DATA      = {IDL_CH,IDL_CH,IDL_CH,IDL_CH}; // 32'h0707_0707 - idle chars
   parameter    END_DATA       = {TRM_CH,TRM_CH,TRM_CH,TRM_CH}; // 32'hFDFD_FDFD - efd ... efd
   parameter    ERR_DATA       = {ERR_CH,TRM_CH,ERR_CH,ERR_CH}; // 32'hFEFE_FEFE - err ... err

   parameter    FCSCHECK     = 32'hE320BBDE;


   //***************************************************************************
   //                      Include C Interface File
   //***************************************************************************  
   parameter    
     BFMid     =  1,
       ClockId   =  1,	 // Which clock I am connected to.
       DataWidth = 64,
       DataAdWid = 11,   // Max is 15
       GCSwid    = 64,   // Max is 256 bytes
       PCSwid    = 64;   // Max is 256 bytes

`include "xif_core.vh"           // Use the transactor template

   //  File Name - carbonx_enetxgmii_rx64.v 
   //  Author    - Joe Sestrich
   //  Date      - 12/1/2004
   // 
   //  Description:  Synthesizable Ethernet Receiver BFM and attaches to the XGMII Tx
   //		  interface of the DUT.
   //                 This transactor module is a C-side initiated transactor and consist  
   //		   of XIF_core and BFM. The rx transactor receives configuration data
   //		   from XIF_core to set up modes.
   //                 The only valid command is BFM_Read, to read a packet
   //*******************************************************************************

   //**************************************************************************
   //                      Internal Reg/Wire Signals 
   //************************************************************************** 
   reg [12:0] 	dcount, dcount_del, pkt_size;	// size of packet
   wire [12:0] 	raw_pkt_size;
   reg [DataAdWid -1:0] daddr;    	// addr to dpram
   reg [1:0] 		state;

   reg [15:0] 		ipg_cnt;	// ipg counter
   reg [31:0] 		fcs; 		// Current fcs
   reg [63:0] 		cbits,   // Accumulated TXC bits
		        cbitw;   // bits to write
   wire [31:0] 		TXCx;
   reg [3:0] 		raw_align;
   reg [31:0] 		TXDh_del;
   
   reg 			xrun, dwe;

   reg 			ipg_err, def_ipg_err;
   reg 			pre_err;
   reg 			pre_err_d;
   reg 			tx_err;  
   reg 			fcs_err;
   reg                  code_err;
   
   //  These are partial fcs results, that include the 
   //  indicated byte lane (plus the lower lanes)  
   reg [31:0] 		fcs7, fcs6, fcs5, fcs4, fcs3, fcs2, fcs1, fcs0;

   wire [31:0] 		rx_config;
   wire 		raw_mode, fcs_enable, tx_err_enable;
   wire 		reset_i;
   wire                 reset_l;
   wire [15:0] 		rx_err_status; // report back err status to C-side
			
   // Wait to do something until we have seen idles on the INPUT line
   // That way we can avoid spurious errors after reset.
   reg 			dut_resetted;
   
   // ----------------------------------------------------------------------
   //  Register definition
   //  get_csdata[31:0]   -> rx_config
   //  get_csdata[63:32]  -> err_insertion
   //  put_csdata[31:0]   <- rx_err_status
   //
   //
   //  rx_config[0]     -> sel_clk_10     
   //  rx_config[1]     -> sel_clk_100 --> (_config[1:0]==00) -> gigmode    
   //  rx_config[2]     -> half_duplex    
   //  rx_config[3]     -> fcs_enable
   //  rx_config[4]     -> rmii_mode 	// Reserved for future  
   //  rx_config[5]     -> rxgmii_mode	// Reserved for future   
   //  rx_config[6]     -> tbi_en   	// Reserved for future 
   //  rx_config[7]     -> rtbi_en		// Reserved for future       

   // 
   // ----------------------------------------------------------------------

   //***************************************************************************
   //                      Continuous Assignments
   //***************************************************************************    
   assign 		reset_i   = ~sys_reset_l | XIF_reset;
   assign 		reset_l   = ~reset_i;

   assign 		rx_config     = XIF_get_csdata[31:0];
   assign 		raw_mode      = rx_config[0];
   assign 		fcs_enable    = rx_config[1];
   assign 		tx_err_enable = rx_config[2];

   // Outputs to the XIF interface
   assign 		BFM_put_operation= BFM_NXTREQ;
   assign 		BFM_put_status   = raw_mode? (cbits | (TXC<<dcount)) : rx_err_status;
   assign 		BFM_put_size     = fcs_enable ? ((state==RAW_DATA ? raw_pkt_size : pkt_size)-4) : (state==RAW_DATA ? raw_pkt_size : pkt_size);
   assign 		BFM_put_address  = cbits | (TXC<<dcount);
   assign 		BFM_put_csdata   = {xaui_linkstatus, 16'd0, rx_err_status};
   assign 		BFM_put_data     = dcount[2] ? {TXD[31:0],TXDh_del[31:0]} : TXD[63:0];
   assign 		BFM_put_cphwtosw = 0; 
   assign 		BFM_put_cpswtohw = 0; 
   assign 		BFM_interrupt    = 0;
   assign 		BFM_xrun         = xrun & ~(state==RAW_DATA && TXD[63:32]==IDLE_DATA && TXC[7:4]==4'b1111);
   assign 		BFM_put_dwe      = dwe;
   assign 		BFM_gp_daddr     = daddr;

   assign 		BFM_reset        = ~sys_reset_l;
   assign 		BFM_clock        =  TX_CLK; 


   //  The rx error status register note the bit ordering better correlate
   //  to the C_side xgmii_defines.h....
   //  They look a bit goofy, but the "force" and "detect" bits are the same
   assign 		rx_err_status = {8'b0, code_err, pre_err_d, ipg_err, fcs_err, tx_err, 2'b00}; 

   assign 		TXCx = {8{TXC}} << raw_align;  // just 8 copies accross a bus

   // Calculate packet size for Raw Data Mode
   assign 		raw_pkt_size = dcount + count_non_idle(TXD,TXC);

   // Initialize XAUI Status to 0, so when not running in XAUI mode
   // These status signals will always be cleared
   initial begin
      xaui_linkstatus = 0;
      xaui_code_error = 0;
   end
   
   //***************************************************************************
   //                      Always Loops
   //***************************************************************************

   //----------------------------------------------------------------------
   // main state machine to recive TX_signals from G/MAC
   //
   //----------------------------------------------------------------------
   always@(posedge TX_CLK or negedge reset_l) begin
      if (!reset_l) begin 
	 state  <= IDLE; 
	 xrun   <= 0;
	 dcount <= 0;
	 daddr  <= 0;
	 dwe    <= 0;
	 cbits  <= 0;
	 fcs    <= 32'hffffffff;
	 ipg_cnt <= 0;
	 def_ipg_err <= 0;
	 ipg_err <= 0;
	 fcs_err <= 0;
	 pre_err <= 0;
	 pre_err_d <= 0;
	 tx_err  <= 0;
	 cbitw   <= 0;
	 dut_resetted <= 0;
	 pkt_size <= 4;
	 TXDh_del <= 0;
	 raw_align <= 0;
      end else begin

	 // Pipeline pre_err, so we can get the status right, even if it goes away
	 // right when xrun goes low
	 pre_err_d <= pre_err;
	 
	 // Pipeline the uper part of the bus, so it can be delayed to the next cycle if necessary
	 TXDh_del <= TXD[63:32];
	 
	 // Wait to do something until we have seen idles on the INPUT line
	 // That way we can avoid spurious errors after reset.
	 if(TXD=={IDLE_DATA,IDLE_DATA} && TXC==8'b11111111) dut_resetted <= 1;

	 // Pipeline dcount, so that we can use it a cycle later for raw_pkt_size
	 dcount_del <= dcount;


	 // Default to set code_err whenever xaui_code_error is high
	 // When state is in Idle, code_err will be cleared if there is no code error during
	 // the first cycle, overriding this assignment
	 if(xaui_code_error) code_err <= 1;
	 
	 // Don't forget that there is a cycle of delay on dwe!!!

	 if (XIF_get_operation == TBP_READ) begin	    
	    case (state)

	      IDLE: begin
		 dwe         <= 1;  // always asserting write during idle
		 xrun        <= 1;
		 cbits[7:0]  <= TXC;
		 fcs         <= 32'hffffffff;
		 fcs_err     <= 0;
		 tx_err      <= 0;
		 pre_err     <= 0;
		 def_ipg_err <= 0;
		 code_err    <= |xaui_code_error;
		 
		 if(TXD=={IDLE_DATA,IDLE_DATA} && TXC==8'b11111111 || !dut_resetted) begin	// Idling
		    //$display("Detected IDLE at tim %d TXD=%h, TXC=%h", $time, TXD, TXC);
		    dcount  <= 0;
		    daddr   <= 0;
		    ipg_cnt <= ipg_cnt=='hFF ? 8'hFF : ipg_cnt + 2;
		    pre_err <= 0;
		    ipg_err <= 0;

		 end else if(TXD[31:0]==IDLE_DATA && TXC[3:0]==4'b1111) // Lower word Idle, Upper word not
		   if(raw_mode) begin
		      state     <= RAW_DATA;
		      dcount    <= 4;
		      daddr     <= 0;
		      ipg_err   <= 0;
		      pre_err   <= 0;
		      cbits     <= TXC[7:4];
		      cbitw     <= 32'h00000ff0;
		      raw_align <= 4;
		   end else if (TXD[63:40]==SQ_DATA   && TXC[7:4]==4'b0001) begin // Seq pkt
 		      state   <= RAW_DATA;
		      dcount  <= 4;
		      daddr   <= 1;
		      ipg_err <= 0;
		      pre_err <= 0;
		   end else begin   // Start normal frame
		      pre_err <= !(TXD[63:32]==PREAMBLE_L && TXC[7:4]==4'b0001);
		      state   <= PREAMBLE;
		      dcount  <= 8;	// just to make logic simpler: dont care
		      daddr   <= 1;
		      ipg_err <= (ipg_cnt+1)<MIN_IPG;
		   end // else: !if(TXD[63:40]==SQ_DATA   && TXC[7:4]==4'b0001)
		 
		 else begin // Lower and upper word both has data
		    if(raw_mode) begin
		       state     <= RAW_DATA;
		       dcount    <= 8;
		       daddr     <= 1;
		       ipg_err   <= 0;
		       pre_err   <= 0;
		       cbits     <= TXC;
		       cbitw     <= 32'h0000ff00;
		       raw_align <= 0;
		    end else if (TXD[31:8]==SQ_DATA && TXC[3:0]==4'b0001) begin	// Seq pkt
 		       state   <= RAW_DATA;
		       dcount  <= 4;
		       daddr   <= 1;
		       ipg_err <= 0;
		       pre_err <= 0;
		    end else begin		// Start normal frame
		       pre_err <= !(TXD=={PREAMBLE_H,PREAMBLE_L} && TXC==8'b00000001);	// record error
		       ipg_err <= ipg_cnt<MIN_IPG; // check for ipg error
		       state   <= DATA;
		       dcount  <= 0;
		       daddr   <= 0;
		    end // else: !if(TXD[31:8]==SQ_DATA && TXC[3:0]==4'b0001)
		 end // else: !if(TXD[63:40]==SQ_DATA   && TXC[7:4]==4'b0001)
	      end // case: IDLE

	      RAW_DATA:  begin					// Grab everything up til idle or 32 bytes
		 cbitw <= cbitw << 8;
		 if (TXD=={IDLE_DATA,IDLE_DATA} && TXC==8'b11111111) begin
		    xrun     <= 1;	// xrun gets deasserted combinatorially elsewhere
		    dwe      <= 1;
		    state    <= IDLE;
		    daddr    <= 0;	// Hold dcount
		    ipg_cnt  <= 2;
		    pkt_size <= dcount;
		    dcount   <= 0;
		 end else if (TXD[63:32]==IDLE_DATA && TXC[7:4]==4'b1111) begin
		    xrun     <= 1;	// xrun gets deasserted combinatorially elsewhere
		    dwe      <= 1;
		    state    <= IDLE;
		    daddr    <= 0;	// Hold dcount
		    ipg_cnt  <= 1;
		    pkt_size <= dcount + 4;
		    cbits    <= cbits | (cbitw & TXCx);
		    dcount   <= 0;
		 end else begin
		    xrun    <= 1;
		    dwe     <= 1;
		    state   <= RAW_DATA;
		    dcount  <= dcount + 8;
		    daddr   <= daddr  + 1;
		    cbits   <= cbits | (cbitw & TXCx);
		 end
	      end // RAW_DATA

	      PREAMBLE: begin
		 pre_err <= pre_err || TXD[31:0]!=PREAMBLE_H || TXC[3:0]!=0;	// record error
		 if ( (TXC[7] && TXD[63:56] == ERR_CH) ||  
	              (TXC[6] && TXD[55:48] == ERR_CH) ||
	              (TXC[5] && TXD[47:40] == ERR_CH) ||
	              (TXC[4] && TXD[39:32] == ERR_CH)) tx_err <= 1;   // check tx Error
		 state   <= DATA;
		 dwe     <= 1;	// start writing next cycle
		 xrun    <= 1;
		 daddr   <= 0;
		 dcount  <= 4;
		 fcs     <= calc_word_fcs(32'hffffffff, TXD, 8'b11110000);
		 ipg_err <= ipg_err | def_ipg_err;
	      end // PREAMBLE

	      DATA: begin

		 //if(TXC) $display("TXC[7:4] == %h, TXD[31:24] = %h", TXC[7:4] , TXD[31:24]);
		 
		 if (TXC == 0) begin							// Full word of data
		    dwe     <= 1;
		    state   <= DATA;
		    dcount  <= dcount + 8;
		    daddr   <= daddr  + 1;
		    ipg_cnt <= 0;
		    fcs     <= calc_word_fcs(fcs, TXD, 8'b11111111);
		    
		 end else if ( (TXC[7] && TXD[63:56] == ERR_CH) ||  
	                       (TXC[6] && TXD[55:48] == ERR_CH) ||
	                       (TXC[5] && TXD[47:40] == ERR_CH) ||
	                       (TXC[4] && TXD[39:32] == ERR_CH) ||
			       (TXC[3] && TXD[31:24] == ERR_CH) ||  
	                       (TXC[2] && TXD[23:16] == ERR_CH) ||
	                       (TXC[1] && TXD[15:8]  == ERR_CH) ||
	                       (TXC[0] && TXD[7:0]   == ERR_CH))  begin	// got TX Error
		    dwe     <= 1;
		    state   <= DATA;
		    dcount  <= dcount + 8;
		    daddr   <= daddr  + 1;
		    ipg_cnt <= 0;
		    fcs     <= calc_word_fcs(fcs, TXD, 8'b11111111);
		    tx_err  <= 1;

		 end else if (TXC[3:0]  == 4'b1111 && 
			      TXD[31:0] == {IDL_CH,IDL_CH,IDL_CH,TRM_CH}) begin	// just terminate, no data
		    //$display("0 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    
		    xrun     <= 0;
		    dwe      <= 0;
		    daddr    <= 0;
		    ipg_cnt  <= 1;
		    pkt_size <= dcount;
		    fcs_err <= fcs_enable && fcs != FCSCHECK;

		    // Now we need to check the next word to see where we are heading next
		    if (TXD[63:32]==PREAMBLE_L && TXC[7:4]==4'b0001) begin
		       state       <= PREAMBLE;
		       dcount      <= 4;
		       def_ipg_err <= 1; // If we're going straight for the packet, the ipg is too short
		       pre_err     <= 0; // We just checked PREAMBLE_L, so no error yet
		    end
		    else begin
		       state   <= IDLE;
		    end
		    
		 end else if (TXC[3:0] == 4'b1110 && TXD[31:8] == {IDL_CH,IDL_CH,TRM_CH}) begin	// 1 byte of data
		    //$display("1 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun    <= 0;
		    dwe     <= 0;
		    pkt_size  <= dcount + 1;
		    daddr   <= 0;
		    ipg_cnt <= 1;
		    fcs     <= calc_word_fcs(fcs, TXD, 8'b00000001);
		    fcs_err <= fcs_enable && calc_word_fcs(fcs, TXD, 8'b00000001) != FCSCHECK;

		    // Now we need to check the next word to see where we are heading next
		    if (TXD[63:32]==PREAMBLE_L && TXC[7:4]==4'b0001) begin
		       state       <= PREAMBLE;
		       dcount      <= 4;
		       def_ipg_err <= 1; // If we're going straight for the packet, the ipg is too short
		       pre_err     <= 0; // We just checked PREAMBLE_L, so no error yet
		    end
		    else begin
		       state   <= IDLE;
		    end

		 end else if (TXC[3:0] == 4'b1100 && TXD[31:16] == {IDL_CH,TRM_CH}) begin		// 2 bytes of data
		    //$display("2 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun     <= 0;
		    dwe      <= 0;
		    pkt_size <= dcount + 2;
		    daddr    <= 0;
		    ipg_cnt  <= 1;
		    fcs      <= calc_word_fcs(fcs, TXD, 8'b00000011);
		    fcs_err  <= fcs_enable && calc_word_fcs(fcs, TXD, 8'b00000011) != FCSCHECK;

		    // Now we need to check the next word to see where we are heading next
		    if (TXD[63:32]==PREAMBLE_L && TXC[7:4]==4'b0001) begin
		       state       <= PREAMBLE;
		       dcount      <= 4;
		       def_ipg_err <= 1; // If we're going straight for the packet, the ipg is too short
		       pre_err     <= 0; // We just checked PREAMBLE_L, so no error yet
		    end
		    else begin
		       state   <= IDLE;
		    end

		 end else if (TXC[3:0]==4'b1000 && TXD[31:24]==TRM_CH) begin			// 3 bytes of data
		    //$display("3 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun     <= 0;
		    dwe      <= 0;
		    state    <= IDLE;
		    pkt_size <= dcount + 3;
		    daddr    <= 0;
		    ipg_cnt  <= 1;
		    fcs      <= calc_word_fcs(fcs, TXD, 8'b00000111);
		    fcs_err  <= fcs_enable && calc_word_fcs(fcs, TXD, 8'b00000111) != FCSCHECK;

		    // Now we need to check the next word to see where we are heading next
		    if (TXD[63:32]==PREAMBLE_L && TXC[7:4]==4'b0001) begin
		       state       <= PREAMBLE;
		       dcount      <= 4;
		       def_ipg_err <= 1; // If we're going straight for the packet, the ipg is too short
		       pre_err     <= 0; // We just checked PREAMBLE_L, so no error yet
		    end
		    else begin
		       state   <= IDLE;
		    end

		 end else if (TXC == 8'b11110000 && TXD[63:32] == {IDL_CH,IDL_CH,IDL_CH,TRM_CH}) begin			// 4 bytes of data
		    //$display("4 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun    <= 0;
		    dwe     <= 0;
		    state   <= IDLE;
		    pkt_size <= dcount + 4;
		    daddr   <= 0;
		    ipg_cnt <= 0;
		    fcs     <= calc_word_fcs(fcs, TXD, 8'b00001111);
		    fcs_err <= fcs_enable && calc_word_fcs(fcs, TXD, 8'b00001111) != FCSCHECK;

		 end else if (TXC == 8'b11100000 && TXD[63:40] == {IDL_CH,IDL_CH,TRM_CH}) begin			// 5 bytes of data
		    //$display("5 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun    <= 0;
		    dwe     <= 0;
		    state   <= IDLE;
		    pkt_size  <= dcount + 5;
		    daddr   <= 0;
		    ipg_cnt <= 0;
		    fcs     <= calc_word_fcs(fcs, TXD, 8'b00011111);
		    fcs_err <= fcs_enable && calc_word_fcs(fcs, TXD, 8'b00011111) != FCSCHECK;

		 end else if (TXC == 8'b11000000 && TXD[63:48] == {IDL_CH,TRM_CH}) begin			// 6 bytes of data
		    //$display("6 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun    <= 0;
		    dwe     <= 0;
		    state   <= IDLE;
		    pkt_size  <= dcount + 6;
		    daddr   <= 0;
		    ipg_cnt <= 0;
		    fcs     <= calc_word_fcs(fcs, TXD, 8'b00111111);
		    fcs_err <= fcs_enable && calc_word_fcs(fcs, TXD, 8'b00111111) != FCSCHECK;

		 end else if (TXC == 8'b10000000 && TXD[63:56] == TRM_CH) begin			// 7 bytes of data
		    //$display("7 - bytes. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun    <= 0;
		    dwe     <= 0;
		    state   <= IDLE;
		    pkt_size  <= dcount + 7;
		    daddr   <= 0;
		    ipg_cnt <= 0;
		    fcs     <= calc_word_fcs(fcs, TXD, 8'b01111111);
		    fcs_err <= fcs_enable && calc_word_fcs(fcs, TXD, 8'b01111111) != FCSCHECK;

		    //  This case happens on receipt of any error character
		 end else begin							// Bogus case?
		    //$display("Bogus Case:. TXD = %h, TXC = %h\n", TXD,TXC);
		    xrun    <= 0;
		    dwe     <= 0;
		    state   <= IDLE;
		    pkt_size  <= dcount + 8;
		    daddr   <= 0;
		    ipg_cnt <= 0;	    
		 end

	      end // DATA

	    endcase

	 end else begin   // Ignore any opcode but Read
            state  <= IDLE; 
            xrun   <= 0;
            dcount <= 0;
            daddr  <= 0;
	 end
      end
   end
   
   //----------------------------------------------------------------------
   // -- Calculate FCS For the whole word. Mask can be used to indicate
   // -- that bytes in the beginning or end of the word should not be used in
   // -- the calculation.
   //----------------------------------------------------------------------
   function [31:0] calc_word_fcs;
      input [31:0] fcs;
      input [63:0] word;
      input [7:0]  byte_mask;

      integer byte_idx;
      
      begin
	 calc_word_fcs = fcs;
	 
	 for(byte_idx=0; byte_idx<8; byte_idx = byte_idx + 1)
	   if(byte_mask[byte_idx]) 
	     calc_word_fcs = calculate_fcs(calc_word_fcs, (word>>(byte_idx*8)) & 8'hff);
      end
   endfunction // calc_word_fcs
   
   //----------------------------------------------------------------------
   // -- Packet FCS Calculation
   //----------------------------------------------------------------------
   function [31:0] calculate_fcs;
      input [31:0] fcs;
      input [7:0]  byte;
      
      integer 	   index;
      reg 	   feedback;
      
      begin
	 calculate_fcs = fcs;
	 
	 for (index=0; index<=7; index=index+1)
	   begin
	      feedback = calculate_fcs[24] ^ byte[index];
	      
	      calculate_fcs[24] = calculate_fcs[25];
	      calculate_fcs[25] = calculate_fcs[26];
	      calculate_fcs[26] = calculate_fcs[27];
	      calculate_fcs[27] = calculate_fcs[28];
	      calculate_fcs[28] = calculate_fcs[29];
	      calculate_fcs[29] = calculate_fcs[30]  ^ feedback;
	      calculate_fcs[30] = calculate_fcs[31];
	      calculate_fcs[31] = calculate_fcs[16];
	      
	      calculate_fcs[16] = calculate_fcs[17]  ^ feedback;
	      calculate_fcs[17] = calculate_fcs[18]  ^ feedback;
	      calculate_fcs[18] = calculate_fcs[19];
	      calculate_fcs[19] = calculate_fcs[20];
	      calculate_fcs[20] = calculate_fcs[21];
	      calculate_fcs[21] = calculate_fcs[22];
	      calculate_fcs[22] = calculate_fcs[23];
	      calculate_fcs[23] = calculate_fcs[8]   ^ feedback;
	      
	      calculate_fcs[8]  = calculate_fcs[9];
	      calculate_fcs[9]  = calculate_fcs[10];
	      calculate_fcs[10] = calculate_fcs[11];
	      calculate_fcs[11] = calculate_fcs[12]  ^ feedback;
	      calculate_fcs[12] = calculate_fcs[13]  ^ feedback;
	      calculate_fcs[13] = calculate_fcs[14]  ^ feedback;
	      calculate_fcs[14] = calculate_fcs[15];
	      calculate_fcs[15] = calculate_fcs[0]   ^ feedback;
	      
	      calculate_fcs[0]  = calculate_fcs[1]   ^ feedback;
	      calculate_fcs[1]  = calculate_fcs[2];
	      calculate_fcs[2]  = calculate_fcs[3]   ^ feedback;
	      calculate_fcs[3]  = calculate_fcs[4]   ^ feedback;
	      calculate_fcs[4]  = calculate_fcs[5];
	      calculate_fcs[5]  = calculate_fcs[6]   ^ feedback;
	      calculate_fcs[6]  = calculate_fcs[7]   ^ feedback;
	      calculate_fcs[7]  =                      feedback;
	   end // for (index=0; index<=7; index=index+1)
	 //$display("RX FCS: Prev fcs: %h, new fcs: %h, byte: %h", fcs, calculate_fcs, byte); 
      end
   endfunction

   // Check if a byte is an idle
   function is_byte_idle;
      input [63:0] txd;
      input [7:0]  txc;
      input [2:0]  i;

      is_byte_idle = ((((txd>>(i*8)) & 8'hff) == IDL_CH) || (((txd>>(i*8)) & 8'hff) == TRM_CH)) && txc[i]==1'b1;
   endfunction // is_byte_idle

   // Count number of bytes that are not IDLE characters in a word
   // This is used when calculating the received packet size
   function [2:0] count_non_idle;
      input [63:0] txd;
      input [7:0]  txc;

      integer     i;
      reg         idle_found;
      
      begin

	 idle_found = 0; 
	 for(i = 0; i < 8; i = i + 1) begin
	    if( is_byte_idle(txd,txc,i) && !idle_found) begin
	       idle_found     = 1;
	       count_non_idle = i;
	    end
	 end
      end
   endfunction // count_non_idle
   
   /*
    // SS - begin watching RX state machine  
    integer rx_k; 

    initial
    begin
    rx_k = 0;
    $display("%m: state IDLE          = %x", IDLE);
    $display("%m: state PREAMBLE      = %x", PREAMBLE);
    $display("%m: state DATA          = %x", DATA);
    $display("%m: state RAW_DATA      = %x", RAW_DATA);

    case(state) 
    IDLE:          $display("%m: init: RX state is IDLE");
    PREAMBLE:      $display("%m: init: RX state is PREAMBLE");
    DATA:          $display("%m: init: RX state is DATA");
    RAW_DATA:      $display("%m: init: RX state is RAW_DATA");
    default:       $display("%m: init: unknown RX state <%x>", state);
       endcase

     end

    //always @(state) 
    always @(posedge TX_CLK)
    begin
    # 0;       
    $display("\n");

    rx_k = rx_k+1;
    $display("%m: rx_k = %d", rx_k);

    case(state) 
    IDLE:          begin
    $display("%m: RX state is IDLE");
                        end
    PREAMBLE:      begin
    $display("%m: RX state is PREAMBLE");
                        end
    DATA:          begin
    $display("%m: RX state is DATA");
                        end
    RAW_DATA:      begin
    $display("%m: RX state is RAW_DATA");
                        end
    default:        $display("%m: unknown RX state <%x>", state);
       endcase

    @ (negedge TX_CLK);
    $display("%m: XIF_get_operation = %x", XIF_get_operation);
    $display("%m: rx_config         = %x", rx_config);
    $display("%m: raw_mode          = %x", raw_mode);
    $display("%m: fcs_enable        = %x", fcs_enable);
    $display("%m: tx_err_enable     = %x", tx_err_enable);

    $display("%m: xrun         = %x", xrun);
    $display("%m: fcs          = %x", fcs);

    $display("%m: daddr        = %d", daddr);
    $display("%m: dcount       = %d", dcount);
    $display("%m: dwe          = %x", dwe);
    $display("%m: BFM_put_data = %x", BFM_put_data);

    $display("%m: TXD          = %x", TXD);
    $display("%m: TXC          = %x", TXC);
    $display("%m: TXCx         = %x", TXCx);
    $display("%m: cbits        = %x", cbits);
    $display("%m: cbitw        = %x", cbitw);

    $display("%m: ipg_cnt      = %d", ipg_cnt);
    $display("%m: ipg_err      = %x", ipg_err);
    $display("%m: fcs_err      = %x", fcs_err);
    $display("%m: pre_err      = %x", pre_err);
    $display("%m: tx_err       = %x", tx_err);
     end
    // SS - end watching RX state machine
    */

`endprotect
endmodule
// -- eof --
