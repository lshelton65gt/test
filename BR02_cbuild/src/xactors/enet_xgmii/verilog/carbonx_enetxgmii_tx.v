//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//  Description - Synthesizable Ethernet Transmitter BFM. Attaches to the Rx 
//                interface of the DUT

`timescale 1ns / 100 ps

module  carbonx_enetxgmii_tx
  (sys_clk,   
   sys_reset_l,
   RXD,       
   RXC,     
   RX_CLK    
   );

   // Primary IO
   input         sys_clk, sys_reset_l;
   output        RX_CLK;   // Receive clock - 125MHz
   output [3:0]  RXC;      // Receive control
   output [31:0] RXD;      // Receive data

`protect
   // carbon license crbn_vsp_exec_xtor_e10g

   reg [3:0] 	 RXCr;      // Receive control
   reg [31:0] 	 RXDr;      // Receive data

   //**************************************************************************
   //                      Parameter Defines
   //**************************************************************************
   parameter 	 
     BFMid     = 1,
       ClockId   = 1,
       DataWidth = 32,
       DataAdWid = 11,	    // Max is 15  accommodate 2K pkt
       GCSwid    = 32*4  ,  // Max is 256 bytes
       // debug we don't need pcswid  set it to very small value but non-Zero
       PCSwid    = 32;  // Max is 256 bytes
   
`define	       XIF_GCS_DEFAULT_DATA 96'hD555_5555_5555_55FB_0000_0000

`include "xif_core.vh"   // Use the transactor template

   // std frame: <idle> <s> <preamble> <sfd> <data> <efd> <idle>
   // err frame: <idle> <s> <preamble> <sfd> <data> <err> <data> <efd> <idle>
   // std data:  <da> <sa> <len> <payload> <pad> ... <fcs>

   //==========================================================================
   //  File Name - carbonx_enetxgmii_tx.v 
   //  Author    - Joe Sestrich
   //  Date      - 12/1/2004
   // 
   //  Description - Synthesizable Ethernet Transmitter BFM. Attaches to the Rx 
   //                interface of the DUT
   //  Revision History
   //*******************************************************************************

   // State machine state names
   
   parameter 	 IDLE         	=  8'h00;
   parameter 	 SEND_IPG     	=  8'h01;
   parameter 	 SEND_PREAMBLE 	=  8'h02;
   parameter 	 SEND_DATA    	=  8'h04;
   parameter 	 SEND_FCS     	=  8'h08;
   parameter 	 SEND_END       =  8'h10;
   parameter 	 SEND_RAW       =  8'h20;
   parameter 	 SEND_ERROR     =  8'h40;

   // Misc constants

   parameter 	 IDL_CH         =  8'h07; // idle
   parameter 	 STR_CH         =  8'hFB; // start
   parameter 	 ERR_CH         =  8'hFE; // error
   parameter 	 TRM_CH         =  8'hFD; // terminate (efd)
   parameter 	 SFD_CH         =  8'hD5; // start frame delim (sfd)
   
   parameter 	 _PREAMBLE_     = 24'h555555; 

   parameter 	 PREAMBLE_L     = {_PREAMBLE_,STR_CH};           // 32'h5555_55FB - start char & preamble
   parameter 	 PREAMBLE_H     = {SFD_CH,_PREAMBLE_};           // 32'hD555_5555 - preamble & sfd char
   parameter 	 IDLE_DATA      = {IDL_CH,IDL_CH,IDL_CH,IDL_CH}; // 32'h0707_0707 - idle chars
   parameter 	 END_DATA       = {TRM_CH,TRM_CH,TRM_CH,TRM_CH}; // 32'hFDFD_FDFD - efd ... efd
   parameter 	 ERR_DATA       = {ERR_CH,TRM_CH,ERR_CH,ERR_CH}; // 32'hFEFE_FEFE - err ... err

   wire [31:0] 	 tx_config;
   wire [31:0] 	 preamble_h, preamble_l; 

   wire [31:0]   data = XIF_get_data[31:0];

   // Break out the bits from tx_config
   wire 	 raw_mode, fcs_enable, tx_err_enable;
   wire [7:0] 	 fcs_err;
   wire 	 reset_i;
   wire          reset_l;
   
   wire [15:0] 	 err_baddr;
   wire [7:0] 	 err_char; 

   
   // -- register definition
   reg [7:0] 	 state;     
   reg [31:0] 	 fcs;	// Holds fcs during transmission
   reg [31:0] 	 cbits;	// shift register used during raw mode

   reg [11:0] 	 dcount;	// Counter to hold data byte count

   reg [7:0] 	 ipg_count;			// Current gap since last pkt
   wire [7:0] 	 ipg = XIF_get_address[7:0];	// desired gap from pkt
   wire 	 ipg_done = !(ipg_count < ipg);   // count >= desiged gap;
   
   reg 		 xrun, raw_data;
   
   reg [DataAdWid -1:0] daddr;

   //  These are partial fcs results, that include the 
   //  indicated byte lane (plus the lower lanes)  
   wire [31:0] 		fcs3, fcs2, fcs1, fcs0;

   reg 			RX_CLK;
   
   // ----------------------------------------------------------------------
   //  Register definition
   //  get_csdata[31:0]    -> tx_config
   //  get_csdata[63:32]   -> preamble_l
   //  get_csdata[95:64]   -> preamble_h
   //
   //  tx_config[0]     -> raw_mode
   //  tx_config[1]     -> fcs_enable     
   //  tx_config[2]     -> inject TX error
   //  tx_config[3]     -> force fcs error
   //  tx_config[7:4]   -> reserved
   //  tx_config[23:8]  -> TX error delay
   //  tx_config[31:24] -> TX error value
   // 
   // ----------------------------------------------------------------------

   // ------------------------------
   // XIF_core interface 
   // ------------------------------
   assign 		BFM_clock          = sys_clk; 
   assign 		BFM_put_operation  = BFM_NXTREQ;
   assign 		BFM_interrupt      = 0;
   assign 		BFM_put_status     = 0;
   assign 		BFM_put_size       = 0;
   assign 		BFM_put_address    = 0;
   assign 		BFM_put_data       = 0;
   assign 		BFM_put_cphwtosw   = 0; 
   assign 		BFM_put_cpswtohw   = 0; 
   assign 		BFM_put_dwe        = 0;
   assign 		BFM_reset          = ~sys_reset_l;
   assign 		BFM_put_csdata[31:0]   = 0;  // Could pass status?

   //----------------------------------------------------------------------
   // -- CS_Write --
   //----------------------------------------------------------------------
   assign 		tx_config  = XIF_get_csdata[31:0];
   assign 		preamble_l = XIF_get_csdata[63:32];
   assign 		preamble_h = XIF_get_csdata[95:64]; 

   //  assign 	       preamble_l = PREAMBLE_L;
   //  assign 	       preamble_h = PREAMBLE_H;


   //  Internal reset is or of external reset OR testbench reset
   assign 		reset_i     = ~sys_reset_l | XIF_reset;
   assign 		reset_l     = ~reset_i;
   
   assign 		RXC        = raw_data ? cbits : RXCr;
   assign 		RXD        = raw_data ? data  : RXDr;

   assign 		raw_mode      = tx_config[0];
   assign 		fcs_enable    = tx_config[1];  
   assign 		tx_err_enable = tx_config[2]; 
   assign 		fcs_err       = {3'b000, tx_config[3], 4'b0000};    

   assign 		err_baddr     = tx_config[23:8];
   assign 		err_char      = tx_config[31:24];

   assign 		BFM_gp_daddr = daddr;  // only for get data
   assign               BFM_xrun = xrun;

   // Calculate the FCS from incoming data
   assign 		fcs0 = calculate_fcs(fcs , data[ 7: 0]);
   assign 		fcs1 = calculate_fcs(fcs0, data[15: 8]);
   assign 		fcs2 = calculate_fcs(fcs1, data[23:16]);
   assign 		fcs3 = calculate_fcs(fcs2, data[31:24]);
   

   reg 			tx_err_inject;

   //-------------------------------------------------------------------
   // Output Clock Divider  
   //-------------------------------------------------------------------
   initial                   RX_CLK = 0;
   always @(posedge sys_clk) RX_CLK = ~RX_CLK;
   
   //-------------------------------------------------------------------
   // Enet TX-Xactor 
   //-------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l) begin
      if (!reset_l) begin 
	 state     <= IDLE; 
	 RXDr      <= IDLE_DATA;
	 RXCr      <= 4'b1111;
	 xrun      <= 1'b0;
	 raw_data  <= 1'b0;
	 ipg_count <= 0;
	 dcount    <= XIF_get_size;
	 daddr     <= 1;
	 fcs       <= 32'hffffffff;
	 tx_err_inject <= 1'b0;
      end else begin 

	 case(state) 

	   IDLE : begin
	      // decode the transaction opcode
              fcs    <= 32'hffffffff;
	      dcount <= XIF_get_size;
              tx_err_inject <= 1'b0;
              if (XIF_get_operation==BFM_WRITE) begin // write cmd
	         if(raw_mode) begin     // RAW Mode
	            RXDr      <= data;
	            cbits     <= XIF_get_address[31:0];
	            ipg_count <= 0;
	            raw_data  <= 1;
	            if (XIF_get_size > 4) begin  // Raw packet
                       state    <= SEND_RAW;		  
	               xrun     <= 1'b1;
		       daddr    <= 1;
	            end else begin  			       // single cycle raw packet, right back to idle
                       state    <= IDLE;
	               xrun     <= 1'b0;
		       daddr    <= 1;
		    end
		 end else begin          // Normal mode: <ipg, preamble, sfd, data, efd>
	            ipg_count <= (ipg_count == 8'hff) ? 8'hff : ipg_count + 1;  // Stop counting at 255
	            raw_data  <= 0;
	            if (ipg_done) begin           // Normal packet, no IPG
		       state    <= SEND_PREAMBLE;
	 	       // send the preamble
		       RXDr     <= preamble_l;		
		       RXCr     <= 4'b0001;
	               xrun     <= 1'b1;
		       daddr    <= 0;
	            end else begin                // IPG
		       state    <= SEND_IPG;		
		       RXDr     <= IDLE_DATA;
		       RXCr     <= 4'b1111;
	               xrun     <= 1'b1;
		       daddr    <= 0;
	            end
		 end
              end else begin                          // Ignore any opcode other than write
		 state     <= IDLE;
	         RXDr      <= IDLE_DATA;
	         RXCr      <= 4'b1111;
		 xrun      <= 1'b0;
	         daddr     <= 1;
	         ipg_count <= (ipg_count == 8'hff) ? 8'hff : ipg_count + 1;  // Stop counting at 255
	         raw_data  <= 0;
              end
	   end // IDLE

	   SEND_RAW:  begin
	      RXDr       <= data;
	      RXCr       <= cbits[3:0];
	      cbits      <= {4'b0000, cbits[31:4]};
	      ipg_count  <= 0;
	      if (dcount > 8) begin  // screwy constant due to pipeline delays
		 state    <= SEND_RAW;
	         xrun     <= 1'b1;
	         dcount   <= dcount - 4;
	         daddr    <= daddr  + 1;
	      end else begin
		 state    <= IDLE;
	         xrun     <= 1'b0;
	         dcount   <= 4;
	         daddr    <= 0;
	      end
	   end // SEND_RAW

	   SEND_IPG : begin 				// Wait for IPG to expire
              if (ipg_done) begin
		 state    <= SEND_PREAMBLE;
	         // send the preamble
	         RXDr     <= preamble_l;		// Send 1st half of preamble
	         RXCr     <= 4'b0001;
	         xrun     <= 1'b1;
	         daddr    <= 0;
	      end else begin
		 state    <= SEND_IPG;		// IPG
	         RXDr     <= IDLE_DATA;
	         RXCr     <= 4'b1111;
	         xrun     <= 1'b1;
	         daddr    <= 0;
              end

	      ipg_count   <= (ipg_count == 8'hff) ? 8'hff : ipg_count + 1;  // Stop counting at 255
	   end // SEND_IPG
	   
	   SEND_PREAMBLE: begin	 		// Send 2nd half of preamble
              state    <= SEND_DATA;
	      // send the preamble
	      RXDr     <= preamble_h;
	      RXCr     <= 4'b0000;
	      xrun     <= 1'b1;
	      daddr    <= 1;
	      ipg_count<= 0;
	   end // SEND_PREAMBLE

	   SEND_ERROR: begin
              state    <= SEND_DATA;
	      RXDr     <= data;
	      RXCr     <= 4'b0000;
	      xrun     <= 1'b1;
	      fcs      <= fcs3;
              tx_err_inject <= 1'b0;

	      ipg_count <= 0;
	      dcount    <= dcount - 4;
	      daddr     <= daddr  + 1;
	   end // SEND_ERROR

	   SEND_DATA: begin
	      if ( tx_err_enable & (tx_err_inject == 1'b0) & (dcount > 4) ) begin // inj errors
		 if (err_baddr/4 == (daddr-1)) begin 
		    state    <= SEND_ERROR;
	            case (err_baddr-4*(err_baddr/4))
	              0: begin
	                 RXDr <= { data[31:8],  err_char};              
	                 RXCr <= 4'b0001;
	              end
	              1:	begin  
	                 RXDr <= { data[31:16], err_char, data[7:0]  }; 
	                 RXCr <= 4'b0010;
	              end
	              2:	begin  
	                 RXDr <= { data[31:24], err_char, data[15:0] }; 
	                 RXCr <= 4'b0100;
	              end
	              3: begin  
	                 RXDr <= {              err_char, data[23:0] }; 
	                 RXCr <= 4'b1000;
	              end
	            endcase	         
	            xrun     <= 1'b1;
	            fcs      <= fcs3;
		    tx_err_inject <= 1'b1;
		 end else begin
		    state    <= SEND_DATA;
	            RXDr     <= data;
	            RXCr     <= 4'b0000;
	            xrun     <= 1'b1;
	            fcs      <= fcs3;
		 end

	      end else if (dcount > 4) begin	// The simple case, no errors
		 state    <= SEND_DATA;
	         RXDr     <= data;
	         RXCr     <= 4'b0000;
	         xrun     <= 1'b1;
	         fcs      <= fcs3;

		 // These are the tail cases
	      end else if (fcs_enable & dcount == 4) begin  // bytes of data left
		 state    <= SEND_FCS;
	         RXDr     <= data;
	         RXCr     <= 4'b0000;
	         xrun     <= 1'b1;
	         fcs      <= fcs3;

	      end else if (fcs_enable & dcount == 3) begin  // bytes of data left
		 state    <= SEND_FCS;
	         RXDr     <= {~fcs2[31:24], data[23:0]};
	         RXCr     <= 4'b0000;
	         xrun     <= 1'b1;
	         fcs      <= fcs2;

	      end else if (fcs_enable & dcount == 2) begin  // bytes of data left
		 state    <= SEND_FCS;
	         RXDr     <= {~fcs1[23:16], ~fcs1[31:24], data[15:0]};
	         RXCr     <= 4'b0000;
	         xrun     <= 1'b1;
	         fcs      <= fcs1;

	      end else if (fcs_enable & dcount == 1) begin  // byte of data left
		 state    <= SEND_FCS;
	         RXDr     <= {~fcs0[15:8], ~fcs0[23:16], ~fcs0[31:24], data[7:0]};
	         RXCr     <= 4'b0000;
	         xrun     <= 1'b1;
	         fcs      <= fcs0;

		 // More tails, no FCS appending
	      end else if (dcount == 4) begin
		 state    <= SEND_END;
	         RXDr     <= data;
	         RXCr     <= 4'b0000;
	         xrun     <= 1'b1;
	         fcs      <= fcs3;

	      end else if (dcount == 3) begin
		 state    <= IDLE;
	         RXDr     <= {TRM_CH, data[23:0]};
	         RXCr     <= 4'b1000;
	         xrun     <= 1'b0;
	         daddr    <= 1;
	         fcs      <= fcs2;

	      end else if (dcount == 2) begin
		 state    <= IDLE;
	         RXDr     <= {IDL_CH, TRM_CH, data[15:0]};
	         RXCr     <= 4'b1100;
	         xrun     <= 1'b0;
	         daddr    <= 1;
	         fcs      <= fcs1;

	      end else if (dcount == 1) begin
		 state    <= IDLE;
	         RXDr     <= {IDL_CH, IDL_CH, TRM_CH, data[7:0]};
	         RXCr     <= 4'b1110;
	         xrun     <= 1'b0;
	         daddr    <= 1;
	         fcs      <= fcs0;
	      end

	      ipg_count <= 0;
	      dcount    <= dcount - 4;
	      daddr     <= daddr  + 1;
	   end  // SEND_DATA
	   

	   // Send out FCS after all data is gone
	   SEND_FCS: begin	 
	      if (dcount == 'hfff) begin  		// -1; 3 bytes of fcs left
	         RXDr     <= {TRM_CH, ~fcs[7:0], ~fcs[15:8], ~fcs[23:16]^fcs_err};
	         RXCr     <= 4'b1000;
		 state    <= IDLE;
	         xrun     <= 1'b0;
	      end else if (dcount == 'hffe) begin  	// -2; 2 bytes of fcs left
	         RXDr     <= {IDL_CH, TRM_CH, ~fcs[7:0], ~fcs[15:8]^fcs_err};
	         RXCr     <= 4'b1100;
		 state    <= IDLE;
	         xrun     <= 1'b0;
	      end else if (dcount == 'hffd) begin  	// -3; 1 bytes of fcs left
	         RXDr     <= {IDL_CH, IDL_CH, TRM_CH, ~fcs[7:0]^fcs_err};
	         RXCr     <= 4'b1110;
		 state    <= IDLE;
	         xrun     <= 1'b0;
	      end else begin				// 4 bytes of fcs left
	         RXDr     <= {~fcs[7:0], ~fcs[15:8], ~fcs[23:16], ~fcs[31:24]^fcs_err};
	         RXCr     <= 4'b0000;
		 state    <= SEND_END;
	         xrun     <= 1'b1;
	      end
	      daddr    <= 1;
	      ipg_count<= 0;
	   end // SEND_FCS

	   SEND_END:  begin
	      RXDr     <= {IDL_CH, IDL_CH, IDL_CH, TRM_CH};
	      RXCr     <= 4'b1111;
              state    <= IDLE;
	      xrun     <= 1'b0;
	      daddr    <= 1;
	      ipg_count<= 0;
	   end // SEND_END

	 endcase // case(state)
      end  // if (!reset_l)...
   end // always
   
   //----------------------------------------------------------------------
   // -- CRC gen --
   // crc_calculated is inverted before sending out on the FCS-field
   //----------------------------------------------------------------------

   // synthesis tool must expand and optimise this crc-calculator
   function [31:0] calculate_fcs;
      input [31:0] fcs;
      input [7:0]  byte;
      integer 	   index;
      reg 	   feedback;

      begin
	 calculate_fcs = fcs;

	 for (index=0; index<=7; index=index+1)
	   begin
	      feedback = calculate_fcs[24] ^ byte[index];

	      calculate_fcs[24] = calculate_fcs[25];
	      calculate_fcs[25] = calculate_fcs[26];
	      calculate_fcs[26] = calculate_fcs[27];
	      calculate_fcs[27] = calculate_fcs[28];
	      calculate_fcs[28] = calculate_fcs[29];
	      calculate_fcs[29] = calculate_fcs[30]  ^ feedback;
	      calculate_fcs[30] = calculate_fcs[31];
	      calculate_fcs[31] = calculate_fcs[16];

	      calculate_fcs[16] = calculate_fcs[17]  ^ feedback;
	      calculate_fcs[17] = calculate_fcs[18]  ^ feedback;
	      calculate_fcs[18] = calculate_fcs[19];
	      calculate_fcs[19] = calculate_fcs[20];
	      calculate_fcs[20] = calculate_fcs[21];
	      calculate_fcs[21] = calculate_fcs[22];
	      calculate_fcs[22] = calculate_fcs[23];
	      calculate_fcs[23] = calculate_fcs[8]   ^ feedback;

	      calculate_fcs[8]  = calculate_fcs[9];
	      calculate_fcs[9]  = calculate_fcs[10];
	      calculate_fcs[10] = calculate_fcs[11];
	      calculate_fcs[11] = calculate_fcs[12]  ^ feedback;
	      calculate_fcs[12] = calculate_fcs[13]  ^ feedback;
	      calculate_fcs[13] = calculate_fcs[14]  ^ feedback;
	      calculate_fcs[14] = calculate_fcs[15];
	      calculate_fcs[15] = calculate_fcs[0]   ^ feedback;

	      calculate_fcs[0]  = calculate_fcs[1]   ^ feedback;
	      calculate_fcs[1]  = calculate_fcs[2];
	      calculate_fcs[2]  = calculate_fcs[3]   ^ feedback;
	      calculate_fcs[3]  = calculate_fcs[4]   ^ feedback;
	      calculate_fcs[4]  = calculate_fcs[5];
	      calculate_fcs[5]  = calculate_fcs[6]   ^ feedback;
	      calculate_fcs[6]  = calculate_fcs[7]   ^ feedback;
	      calculate_fcs[7]  =                      feedback;
	   end
      end
   endfunction

   /*
    // SS - begin watching TX state machine
    integer tx_k; 

    initial
    begin
    tx_k = 0;
    $display("%m: state IDLE          = %x", IDLE);
    $display("%m: state SEND_IPG      = %x", SEND_IPG);
    $display("%m: state SEND_PREAMBLE = %x", SEND_PREAMBLE);
    $display("%m: state SEND_DATA     = %x", SEND_DATA);
    $display("%m: state SEND_FCS      = %x", SEND_FCS);
    $display("%m: state SEND_END      = %x", SEND_END);
    $display("%m: state SEND_RAW      = %x", SEND_RAW);
    $display("%m: state SEND_ERROR    = %x", SEND_ERROR);

    case(state) 
    IDLE:          $display("%m: init: TX state is IDLE");
    SEND_IPG:      $display("%m: init: TX state is SEND_IPG");
    SEND_PREAMBLE: $display("%m: init: TX state is SEND_PREAMBLE");
    SEND_DATA:     $display("%m: init: TX state is SEND_DATA");
    SEND_FCS:      $display("%m: init: TX state is SEND_FCS");
    SEND_END:      $display("%m: init: TX state is SEND_END");
    SEND_RAW:      $display("%m: init: TX state is SEND_RAW");
    SEND_ERROR:    $display("%m: init: TX state is SEND_ERROR");
    default:       $display("%m: init: unknown TX state <%x>", state);
       endcase

     end

    //always @(state) 
    always @(posedge sys_clk)
    begin
    # 0;       
    $display("\n");

    tx_k = tx_k+1;
    $display("%m: tx_k = %d", tx_k);

    case(state) 
    IDLE:          begin
    $display("%m: TX state is IDLE");
                        end
    SEND_IPG:      begin
    $display("%m: TX state is SEND_IPG");
                        end
    SEND_PREAMBLE: begin
    $display("%m: TX state is SEND_PREAMBLE");
                        end
    SEND_DATA:     begin
    $display("%m: TX state is SEND_DATA");
                        end
    SEND_FCS:      begin
    $display("%m: TX state is SEND_FCS");
                        end
    SEND_END:      begin
    $display("%m: TX state is SEND_END");
                        end
    SEND_RAW:      begin
    $display("%m: TX state is SEND_RAW");
                        end
    SEND_ERROR:    begin
    $display("%m: TX state is SEND_ERROR");
                        end
    default:        $display("%m: unknown TX state <%x>", state);
       endcase

    @ (negedge sys_clk);
    $display("%m: XIF_get_operation = %x", XIF_get_operation);
    $display("%m: tx_config         = %x", tx_config);
    $display("%m: raw_mode          = %x", raw_mode);
    $display("%m: fcs_enable        = %x", fcs_enable);
    $display("%m: tx_err_enable     = %x", tx_err_enable);
    $display("%m: tx_err_inject     = %x", tx_err_inject);
    $display("%m: fcs_err           = %x", fcs_err);

    $display("%m: xrun        = %x", xrun);
    $display("%m: fcs         = %x", fcs);
    $display("%m: raw_data    = %x", raw_data);

    $display("%m: RXD       = %x", RXD);
    $display("%m: RXDr      = %x", RXDr);
    $display("%m: data      = %x", data);

    $display("%m: RXC       = %x", RXC);
    $display("%m: RXCr      = %x", RXCr);
    $display("%m: cbits     = %x", cbits);

    $display("%m: ipg_count = %d", ipg_count);
    $display("%m: err_baddr = %d, err_baddr/4 = %d", err_baddr, err_baddr/4);
    $display("%m: daddr     = %d", daddr);
    $display("%m: dcount    = %d", dcount);
     end
    // SS - end watching TX state machine
    */
   
`endprotect
endmodule

// -- eof --
