//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Description:  Synthesizable Ethernet Receiver BFM and attaches to the XGMII Tx
//		  interface of the DUT.
//                 This transactor module is a C-side initiated transactor and consist  
//		   of XIF_core and BFM. The rx transactor receives configuration data
//		   from XIF_core to set up modes.
//                 The only valid command is BFM_Read, to read a packet



`timescale 1ns / 100 ps

module  carbonx_enetxgmii_rx
  (sys_clk,   
   sys_reset_l,
   TXD,      
   TXC, 
   TX_CLK 
   );

   
   // XGMII signals
   input        sys_clk, sys_reset_l;
   input [31:0] TXD;
   input [3:0] 	TXC;
   input        TX_CLK;

`protect
   // carbon license crbn_vsp_exec_xtor_e10g

   reg [63:0] 	txd_64, txd_64r;
   reg [7:0] 	txc_8,  txc_8r;

   // Capture Positive Edge Data
   always @(posedge TX_CLK) begin
      txd_64[31:0]  <= TXD;
      txc_8[3:0]    <= TXC;
   end

   // Capture Negative Edge Data
   always @(negedge TX_CLK) begin
      txd_64[63:32] <= TXD;
      txc_8[7:4]    <= TXC;
   end

   // Pipeline Data to get rid of half cycle Changes (XIF_core clocks data on negative edge)
   always @(posedge TX_CLK) begin
      txd_64r       <= txd_64;
      txc_8r        <= txc_8;
   end
   
   carbonx_enetxgmii_rx64 rx(.sys_clk(sys_clk),
			     .sys_reset_l(sys_reset_l),
			     .TXD(txd_64r),
			     .TXC(txc_8r),
			     .TX_CLK(TX_CLK));
`endprotect
endmodule // carbonx_enetxgmii_rx

   
     