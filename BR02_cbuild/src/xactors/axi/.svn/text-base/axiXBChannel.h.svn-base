/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_BCHANNELINTF_H
#define AXIX_BCHANNELINTF_H

#include "axiXChannel.h"
#include "axiXWriteTrans.h"

/*!
 * \file axiXBChannel.h
 * \brief Header file for write response (B) channel interface definition.
 */

/*!
 * \brief Class for Write Response i.e. B Channel
 *
 * This includes BID and BRESP pins. BVALID and BREADY pins are inherited
 * from base class AxixChannel.
 */
class AxixBChannel: public AxixChannel
{
public: CARBONMEM_OVERRIDES

public:

    AxixPin * const _respPin;                 //!< BRESP pin

public:

    // Constructor and destructor
    AxixBChannel(AxiXtor *xtor, const AxixDirection channelDir,
            const CarbonUInt32 idBusWidth);
    ~AxixBChannel();

    // Evaluation to transfer transaction to/from channel interface pins.
    AxixBool transfer(AxixWriteTrans *trans, CarbonTime currentTime);

    // Refresh
    AxixBool refresh(CarbonTime currentTime);
};

#endif
