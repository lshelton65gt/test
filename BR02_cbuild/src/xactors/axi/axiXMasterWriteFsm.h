/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_MASTERWRITEFSM_H
#define AXIX_MASTERWRITEFSM_H

#include "axiXFsm.h"
#include "axiXTransQueue.h"

/*!
 * \file axiXMasterWriteFsm.h
 * \brief Header file for master write FSM class.
 */

/*!
 * \brief Class to represent master write FSM
 *
 * This FSM will internally have three FSMs - address, data and response.
 * Updation of this FSM will update three states for these three internal FSMs
 * and access the channels accordingly. Channels will be updated using
 * transaction structure, while READY and VALID signals will be
 * accessed explicitly.
 */
class AxixMasterWriteFsm: public AxixFsm
{
public: CARBONMEM_OVERRIDES

private:
    
    // Transaction queues in user interface
    AxixTransQueue _addressTransQueue;       //!< Address cycle queue. FSM pops
                                             //   from this queue and starts
                                             //   address cycle for that.
    AxixTransQueue _dataTransQueue;          //!< Data cycle queue. FSM pops
                                             //   from this queue and starts
                                             //   data cycle for that
                                             //   independently with address
                                             //   cycle.
    UInt _queueSpace;                        //!< Transaction queue space. This
                                             //   is used to track simultaneous
                                             //   transactions that user has
                                             //   pushed. Thus it will decrement
                                             //   when user pushes a new write
                                             //   transaction. This will
                                             //   increment when response
                                             //   for a transaction arrives.

    // Internal queues and tables
    AxixTransTable _respTransTable;          //!< Response cycle table of size
                                             //   equal to maximum simultaneous
                                             //   transactions allowed.
                                             //   After a data cycle
                                             //   is complete, transaction will
                                             //   be pushed into this table and
                                             //   that transaction will wait for
                                             //   response.

public:

    // Constructor and destructor
    AxixMasterWriteFsm(AxiXtor *xtor, const CarbonUInt32 maxTransfers);
    ~AxixMasterWriteFsm() {}

    //! Starts a new transaction
    AxixBool canAcceptNewTrans() const;
    AxixBool startNewTransaction(CarbonXAxiTrans *trans);

    // Evaluation routine
    AxixBool update(CarbonTime currentTime);
    // Print routine
    void print() const;
};

#endif
