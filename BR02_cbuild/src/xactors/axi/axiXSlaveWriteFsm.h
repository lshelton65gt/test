/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_SLAVEWRITEFSM_H
#define AXIX_SLAVEWRITEFSM_H

#include "axiXFsm.h"
#include "axiXTransQueue.h"

/*!
 * \file axiXSlaveWriteFsm.h
 * \brief Header file for slave write FSM class.
 */

/*!
 * \brief Class to represent slave write FSM
 *
 * This FSM will internally have three FSMs - address, data and response.
 * Updation of this FSM will update three states for these three internal FSMs
 * and access the channels accordingly. Channels will be updated using
 * transaction structure, while READY and VALID signals will be
 * accessed explicitly.
 */
class AxixSlaveWriteFsm: public AxixFsm
{
public: CARBONMEM_OVERRIDES

private:
    
    // Data waits
    UInt _dataWait;                          //!< Waits in data cycle - WREADY
                                             //   will be made low for this
                                             //   much clocks for a transfer.
                                             //   This variable is assigned
                                             //   at the end of each transfer
                                             //   for controlling WREADY in next
                                             //   transfer.

    // Internal queues and tables
    AxixTransTable _dataTransTable;          //!< Transaction table of size
                                             //   equal to maximum simultaneous
                                             //   transactions allowed.
                                             //   When a new address comes,
                                             //   new transaction is created and
                                             //   pushed into this table. When
                                             //   data cycle for that
                                             //   transaction gets over, FSM
                                             //   pops from this table and
                                             //   puts the transaction into
                                             //   response transaction queue.

    AxixTransQueue _dataWaitQueue;           //!< Queue to maintain ordered
                                             //   transactions to get wait
                                             //   for first transfer in data
                                             //   cycle. This will maintain
                                             //   proper wait states for
                                             //   ordered but overlapping writes

    AxixTransTable _respTransTable;          //!< Transaction table of size
                                             //   equal to maximum simultaneous
                                             //   transactions allowed.
                                             //   Transtable to containing
                                             //   transactions for which data
                                             //   cycle is over, but response
                                             //   is yet to send

public:

    // Constructor and destructor
    AxixSlaveWriteFsm(AxiXtor *xtor, const CarbonUInt32 maxTransfers);
    ~AxixSlaveWriteFsm();

    // Evaluation routine
    AxixBool update(CarbonTime currentTime);
    // Print routine
    void print() const;

};

#endif
