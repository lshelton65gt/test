/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/* 
    File:    axiXBChannel.cpp
    Purpose: This file contains the definition of the functions for 
             accessing write response(B) channel.
*/

#include "axiXBChannel.h"
#include "axiXtor.h"

// Constructor
AxixBChannel::AxixBChannel(AxiXtor *xtor,
        const AxixDirection channelDir, const CarbonUInt32 idBusWidth):
    AxixChannel(xtor, "B", channelDir, idBusWidth),
    _respPin(new AxixPin(xtor, "BRESP", channelDir, 2))
{
    // Check for memory allocation error
    if (_respPin == NULL)
    {
        xtor->errorForNoMemory("BRESP");
        return;
    }
}

AxixBChannel::~AxixBChannel()
{
    delete _respPin;
}

// Transfers the transaction info to and from channel
AxixBool AxixBChannel::transfer(AxixWriteTrans *trans, CarbonTime currentTime)
{
    ASSERT(trans, _xtor);
    if (_channelDir == AxixDirection_Input)
    {
        ASSERT(trans->getId() == _idPin->read(currentTime), _xtor);
        trans->setResponse(
                static_cast<CarbonXAxiResponse>(_respPin->read(currentTime)));
    }
    else // output
    {
        _idPin->write(trans->getId(), currentTime);
        _respPin->write(
                static_cast<CarbonUInt32>(trans->getResponse()), currentTime);
    }
    return AxixTrue;
}

// Refresh channel pins
AxixBool AxixBChannel::refresh(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    retValue &= _respPin->refresh(currentTime);
    return retValue;
}

