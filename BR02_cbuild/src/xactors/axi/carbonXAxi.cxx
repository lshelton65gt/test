/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    carbonXAxi.cpp
    Purpose: This file provides definition of creation and connection APIs for
             AXI transactor object. 
*/

#include "axiXtor.h"
#include "axiXMasterXtor.h"
#include "axiXSlaveXtor.h"

// Create transactor
CarbonXAxiID carbonXAxiCreate(const char* transactorName,
        CarbonXAxiType xtorType, const CarbonXAxiConfig *config, 
        const CarbonUInt32 maxTransfers,
        void (*reportTransaction)(CarbonXAxiTrans*, void*),
        void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
        void (*setResponse)(CarbonXAxiTrans*, void*),
        void (*refreshResponse)(CarbonXAxiTrans*, void*),
        void (*notify)(CarbonXAxiErrorType, const char*,
            const CarbonXAxiTrans*, void*),
        void (*notify2)(CarbonXAxiErrorType, const char*, void*),
        void *stimulusInstance)
{
    CarbonXAxiClass *xtor;
    if (xtorType == CarbonXAxi_Master)
        xtor = new AxixMasterXtor(transactorName, config,
                maxTransfers == 0 ? 1 : maxTransfers); // min is 1
    else
        xtor = new AxixSlaveXtor(transactorName, config,
                maxTransfers == 0 ? 1 : maxTransfers); // min is 1
    // Check for memory allocation error
    if (xtor == NULL)
    {
        AxiXtor::error(xtor, CarbonXAxiError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    // Check if transactor creation got some error internally
    CarbonXAxiErrorType error = xtor->getError();
    if (error != CarbonXAxiError_NoError)
    {
        AxixConstString errorMesg = xtor->getErrorMessage();
        AxiXtor::error(xtor, error, errorMesg);
        delete xtor;
        xtor = NULL;
        return NULL;
    }

#ifndef _NO_LICENSE
    /* scramble feature name and checkout license */
    CARBON_LICENSE_FEATURE(featureName, "crbn_vsp_exec_xtor_axi", 22);
    xtor->_carbonLicense = UtLicenseWrapperCheckout(featureName,
            "AMBA 3 AXI Transactor");
#endif

    // binding
    xtor->registerCallbacks(stimulusInstance, reportTransaction, 
            setDefaultResponse, setResponse, notify, notify2);
    return xtor;

    // to remove compile time warning
    refreshResponse = NULL;
    notify = NULL;
}

CarbonUInt32 carbonXAxiDestroy(CarbonXAxiID xtor)
{
    if (xtor == NULL)
        return 0;
#ifndef _NO_LICENSE
    /* release the license */
    UtLicenseWrapperRelease(&(xtor->_carbonLicense));
#endif

    delete xtor;
    xtor = NULL;
    return 1;
}

// Set get function wrappers
const char *carbonXAxiGetName(CarbonXAxiID xtor)
{
    if (xtor == NULL)
        return NULL;
    return xtor->getName();
}

CarbonXAxiType carbonXAxiGetType(CarbonXAxiID xtor)
{
    if (xtor == NULL)
        return CarbonXAxi_Master;
    return xtor->getType();
}

void carbonXAxiGetConfig(CarbonXAxiID xtor, CarbonXAxiConfig *config)
{ 
    if (config != NULL)
        *config = *(xtor->getConfig());
}

CarbonUInt32 carbonXAxiIsSpaceInTransactionBuffer(CarbonXAxiID xtor,
        CarbonXAxiTransType transType)
{
    if (xtor == NULL)
        return 0;
    return (xtor->canAcceptNewTrans(transType) ? 1 : 0);
}

CarbonUInt32 carbonXAxiStartNewTransaction(CarbonXAxiID xtor,
        CarbonXAxiTrans *trans)
{
    if (xtor == NULL || trans == NULL)
        return 0;
    if (!xtor->startNewTransaction(trans))
        return 0;
    return 1;
}

CarbonUInt32 carbonXAxiConnectToLocalSignalMethods(CarbonXAxiID xtor,
        CarbonXInterconnectNameCallbackTrio *nameList)
{
    if (xtor == NULL || nameList == NULL)
        return 0;

    /* Iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (UInt i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXSignalChangeCB changeCB = NULL;
        CarbonXInterconnectNameCallbackTrio pair = nameList[i];
        /* get the transactor signal wrapper */
        AxixPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        if (pin == NULL)
            return 0;
        /* for output signal use models input callback */
        if (pin->getDirection() == AxixDirection_Output)
            changeCB = pair.ModelInputChangeCB;
        /* for input signal use models output callback */
        else if (pin->getDirection() == AxixDirection_Input)
            changeCB = pair.ModelOutputChangeCB;
        else
            ASSERT(0, xtor);

        if (pin->bind(changeCB, pair.UserPointer) != AxixTrue)
            return 0;
    }
    return 1;
}

CarbonUInt32 carbonXAxiConnectByModelSignalHandle(CarbonXAxiID xtor,
        CarbonXInterconnectNameHandlePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    if (xtor == NULL || nameList == NULL ||
            carbonModelName == NULL || carbonModelHandle == NULL)
        return 0;

    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameHandlePair pair = nameList[i];
        /* get the transactor signal wrapper */
        AxixPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        /* connect the net handle with transactor's respective net */
        if ( pin->bind(carbonModelHandle, pair.ModelSignalHandle) != AxixTrue)
            return 0; /* Failure in connection */
    }
    return 1;
    carbonModelName = NULL; /* remove warning */
}

CarbonUInt32 carbonXAxiConnectByModelSignalName(CarbonXAxiID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    if (xtor == NULL || nameList == NULL ||
            carbonModelName == NULL || carbonModelHandle == NULL)
        return 0;

    static char hdlPath[1024];
    CarbonNetID *net;
    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameNamePair pair = nameList[i];
        sprintf(hdlPath, "%s.%s", carbonModelName, pair.ModelSignalName);
        /* get the net handle */
        net = carbonFindNet(carbonModelHandle, hdlPath);
        if (net == NULL)
            return 0;
        /* get the transactor signal wrapper */
        AxixPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        /* connect the net handle with transactor's respective net */
        if ( pin->bind(carbonModelHandle, net) != AxixTrue)
            return 0;
    }
    return 1;
}

CarbonUInt32 carbonXAxiEvaluate(CarbonXAxiID xtor, CarbonTime currentTime)
{
    if (xtor == NULL)
        return 0;

#ifndef _NO_LICENSE
    /* tell the license server we are still using the license */
    UtLicenseWrapperHeartbeat(xtor->_carbonLicense);
#endif

    return (xtor->evaluate(currentTime) ? 1 : 0); 
}

CarbonUInt32 carbonXAxiRefreshInputs(CarbonXAxiID xtor,
       CarbonTime currentTime)
{
    if (xtor == NULL)
        return 0;

    return (xtor->refreshInputs(currentTime) ? 1 : 0);
}

CarbonUInt32 carbonXAxiRefreshOutputs(CarbonXAxiID xtor,
        CarbonTime currentTime)
{
    if (xtor == NULL)
        return 0;

    return (xtor->refreshOutputs(currentTime) ? 1 : 0);
}

void carbonXAxiDump(CarbonXAxiID xtor)
{
    if (xtor == NULL)
        printf("NULL TRANSACTOR\n");
    else
        xtor->print();
}
