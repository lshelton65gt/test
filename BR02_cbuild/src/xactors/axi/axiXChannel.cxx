/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXChannel.cpp
    Purpose: This file contains the definition of the base class of 
             axi channels.
*/


#include "axiXChannel.h"
#include "axiXtor.h"

// Constructor
AxixChannel::AxixChannel(AxiXtor *xtor,
        AxixConstString channelName, const AxixDirection channelDir,
        const CarbonUInt32 idBusWidth):
    _xtor(xtor), _channelDir(channelDir)
{
    ASSERT(_channelDir == AxixDirection_Input ||
            _channelDir == AxixDirection_Output, _xtor);

    // Create the pin name from channel name and create ID pin
    static char pinName[10];
    sprintf(pinName, "%s%s", channelName, "ID");
    _idPin = new AxixPin(xtor, pinName, _channelDir, idBusWidth);
    // Check for memory allocation error
    if (_idPin == NULL)
    {
        xtor->errorForNoMemory(pinName);
        return;
    }

    // Create valid pin
    sprintf(pinName, "%s%s", channelName, "VALID");
    _validPin = new AxixPin(xtor, pinName, _channelDir);
    // Check for memory allocation error
    if (_validPin == NULL)
    {
        xtor->errorForNoMemory(pinName);
        return;
    }

    // Create ready pin
    sprintf(pinName, "%s%s", channelName, "READY");
    _readyPin = new AxixPin(xtor, pinName,
            (_channelDir == AxixDirection_Input) ?
            AxixDirection_Output : AxixDirection_Input);
    // Check for memory allocation error
    if (_readyPin == NULL)
    {
        xtor->errorForNoMemory(pinName);
        return;
    }
}

AxixChannel::~AxixChannel()
{
    delete _idPin;
    delete _validPin;
    delete _readyPin;
}

// Refresh inputs
AxixBool AxixChannel::refreshInputs(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    if (_channelDir == AxixDirection_Input)
    {
        retValue &= _idPin->refresh(currentTime); // refresh id pin
        retValue &= refresh(currentTime); // refresh other channel pins
        retValue &= _validPin->refresh(currentTime); // refresh valid pin
    }
    else // output
        retValue &= _readyPin->refresh(currentTime); // refresh ready pin
    return retValue;
}

// Refresh outputs
AxixBool AxixChannel::refreshOutputs(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    if (_channelDir == AxixDirection_Output)
    {
        retValue &= _idPin->refresh(currentTime);
        retValue &= refresh(currentTime); // refresh other channel pins
        retValue &= _validPin->refresh(currentTime);
    }
    else
    {
        retValue &= _readyPin->refresh(currentTime);
    }
    return retValue;
}

