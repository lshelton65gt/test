/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXSlaveReadFsm.cpp 
    Purpose: This file contains the definition of the functions for read
             fsm for slave axi transactor.
*/

#include "axiXSlaveReadFsm.h"
#include "axiXtor.h"

#define ALLOW_READ_DATA_INTERLEAVING 0

AxixSlaveReadFsm::AxixSlaveReadFsm(AxiXtor *xtor,
        const CarbonUInt32 maxTransfers):
    AxixFsm(xtor),
    _dataWait(0),
    _scheduledTrans(NULL),
    _dataTransTable(maxTransfers) 
{
    // Remove compiler warnings
    ASSERT(maxTransfers, _xtor);
}

// FSM
AxixBool AxixSlaveReadFsm::update(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;

    //**** Address logic ****
    {
    CarbonUInt32 id = _xtor->getARChannel()->getIdPin()->read(currentTime);
    AxixBool valid = _xtor->getARChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getARChannel()->getReadyPin()->read(currentTime);
    // Transfer should happen at this clock
    if (valid && ready)
    {
        // Create new transaction.
        AxixReadTrans *trans = new AxixReadTrans(id);
        if (trans == NULL) // if no memory is allocated
        {
            AxiXtor::error(_xtor, CarbonXAxiError_NoMemory,
                    "Can't allocate memory for received read transaction.");
            return AxixFalse;
        }
        // Transfer new address and control
        retValue &= _xtor->getARChannel()->transfer(trans, currentTime);
        // Push the transaction for data cycle
        ASSERT(_dataTransTable.push(trans), _xtor);
        if (DEBUG)
            printf("# SLAVE[%u]:READ: ADDRESS transferred.\n",
                    static_cast<UInt>(currentTime));
        _xtor->setDefaultResponse(trans);
    }
    // Ready logic for next transfer
    AxixBool nextReady = 1; // default value of READY
    // Make READY low when transactor can not accept any more transactions
    // Note, transactor can't accept a new incoming transaction if
    // the transaction table is full.
    if (_dataTransTable.isFull())
    {
        nextReady = 0;
#if ALLOW_SAME_ID_FOR_FULL_TABLE
        // One exception to recover ready high from low is: when current ready
        // is low and ID for a valid incoming transaction has an entry in table
        if (!ready && valid && _dataTransTable.get(id) != NULL)
            nextReady = 1;
#endif
    }
    _xtor->getARChannel()->getReadyPin()->write(nextReady, currentTime);
    }

    // **** Data logic ****
    {
    AxixBool valid = _xtor->getRChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getRChannel()->getReadyPin()->read(currentTime);
    // !valid : Bus is idle
    // valid && ready : Transfer is getting complete at master side
    if (!valid || (valid && ready))
    {
        AxixBool nextValid = 0;
        // Check if any transactions are pending and decide for next transfer
        if (!_dataTransTable.isEmpty())
        {
            // Loop exactly the current parrallel queue count times
            for (UInt index = 0; index < _dataTransTable.getCount(); index++)
            {
                // Get a handle to the next queue
                AxixTransQueue *transQueue = _dataTransTable.getQueue(index);
                if (transQueue == NULL)
                {
                    // Internal code error
                    _xtor->error(_xtor, CarbonXAxiError_InternalError,
                        "INTERNAL ERROR - "
                        "invalid queue in data transaction table in "
                        "slave read FSM.");
                    return AxixFalse;
                }
                // Get handle to the oldest transaction pending in the queue
                AxixReadTrans *trans = static_cast<AxixReadTrans*>
                    (transQueue->top());
                ASSERT(trans, _xtor);
                // Decrease transfer wait count and check if wait is zero
                if (trans->getWaitIndefinite())
                { /* Do nothing -- Do not call any callbacks here */ }
                else if (trans->getTransferWait(AxixTrue) == 0)
                {
                    // Schedule this transaction if no other is scheduled
                    if (!_scheduledTrans)
                        _scheduledTrans = trans;
                }
            }

            // Check if any transaction is scheduled
            if (_scheduledTrans != NULL)
            {
                // Transfer new data
                AxixReadTrans *trans = static_cast<AxixReadTrans*>
                    (_dataTransTable.get(_scheduledTrans->getId()));
                ASSERT(trans, _xtor);
                // Only check for wait state here, do not decrement
                if (trans->getTransferWait(AxixFalse) == 0)
                {
                    if (trans->getTransferIndex() == 0)
                        _xtor->setResponse(trans);
                    // Transfer when there is no more wait
                    retValue &= _xtor->getRChannel()->transfer(trans,
                            currentTime);
                    if (DEBUG)
                        printf("# SLAVE[%u]:READ: DATA BEAT %u transferred\n",
                                static_cast<UInt>(currentTime),
                                trans->getTransferIndex() - 1);
                    // For last data that has been transferred, pop from queue
                    if (trans->getLast())
                    {
                        delete _dataTransTable.pop(_scheduledTrans->getId());
                        _scheduledTrans = NULL;
                    }
#if ALLOW_READ_DATA_INTERLEAVING
                    _scheduledTrans = NULL;
#endif
                    nextValid = 1;
                }
            }
        }
        _xtor->getRChannel()->getValidPin()->write(nextValid, currentTime);
    }
    // Call setDefaultResponse for those who have indefinite wait flag set
    for (UInt index = 0; index < _dataTransTable.getCount(); index++)
    {
        AxixReadTrans *trans = static_cast<AxixReadTrans*>
            (_dataTransTable.getQueue(index)->top());
        ASSERT(trans, _xtor);
        if (trans->getWaitIndefinite())
            _xtor->setDefaultResponse(trans);
    }
    }
    return retValue;
}

void AxixSlaveReadFsm::print() const
{
    printf("Read transactions pending for:\n"
           "  Data transfer     = %u\n",
            _dataTransTable.getCount());
}
