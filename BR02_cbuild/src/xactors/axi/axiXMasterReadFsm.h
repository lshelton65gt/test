/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_MASTERREADFSM_H
#define AXIX_MASTERREADFSM_H

#include "axiXFsm.h"
#include "axiXTransQueue.h"

/*!
 * \file axiXMasterReadFsm.h
 * \brief Header file for master read FSM class.
 */

/*!
 * \brief Class to represent master read FSM
 *
 * This FSM will internally have two FSMs - address and data.
 * Updation of this FSM will update three states for these two internal FSMs
 * and access the channels accordingly. Channels will be updated using
 * transaction structure, while READY and VALID signals will be
 * accessed explicitly.
 */
class AxixMasterReadFsm: public AxixFsm
{
public: CARBONMEM_OVERRIDES

private:
    
    // Transaction Queues
    AxixTransQueue _addressTransQueue;       //!< Address cycle queue. 
                                             //   FSM pops from this queue and
                                             //   starts address cycle for 
                                             //   that. 
    UInt _queueSpace;                        //!< Queue space. This will
                                             //   decrement when we push a
                                             //   transaction. This will
                                             //   increment when reportTrans()
                                             //   for a transaction is called.

    UInt _dataWait;                          //   Data Wait state. Stores 
                                             //   the wait cycle for the 
                                             //   current transfer.
    
    // Internal Queues
    AxixTransQueue _dataWaitQueue;            //!< Queue to maintain ordered
                                              //   transactions to get wait
                                              //   for first transfer in data
                                              //   cycle. This will maintain
                                              //   proper wait states for
                                              //   ordered transaction.
    AxixTransTable _dataTransTable;           //!< Data cycle table of size
                                              //   equal to maxTransfer
                                              //   allowed. After an address
                                              //   cycle is complete, 
                                              //   transaction will be pushed
                                              //   into this table and will 
                                              //   wait for the corresponding 
                                              //   data and response.

public:

    // Constructor and destructor
    AxixMasterReadFsm(AxiXtor *xtor, const CarbonUInt32 maxTransfers);
    ~AxixMasterReadFsm();

    //! Starts a new transaction
    AxixBool canAcceptNewTrans() const;
    AxixBool startNewTransaction(CarbonXAxiTrans *trans);

    // Evaluation routine
    AxixBool update(CarbonTime currentTime);
    // Print routine
    void print() const;
};

#endif
