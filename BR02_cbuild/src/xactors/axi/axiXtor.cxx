/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXtor.cpp 
    Purpose: This file contains the definition of the base class of 
             axi transactor.
*/

#include <stdlib.h>
#include "axiXtor.h"
#include "axiXTrans.h"

// Constructor
CarbonXAxiClass::CarbonXAxiClass(AxixConstString name, 
        const CarbonXAxiConfig *config, CarbonXAxiType xtorType):
    _name(AxixStrdup(name)), _config(NULL),
    _writeFsm(NULL), _readFsm(NULL),
    _awChannel(NULL), _wChannel(NULL), _bChannel(NULL),
    _arChannel(NULL), _rChannel(NULL),
    _error(CarbonXAxiError_NoError), _errorMessage(NULL), 
    _errorCallback(NULL), _userContext(NULL)
{
    // Copy the name
    if (_name == NULL)
    {
        errorForNoMemory("transactor name");
        return;
    }

    // Create default configuration
    if (config == NULL)
        _config = new CarbonXAxiConfig();
    else
        _config = new CarbonXAxiConfig(*config);
    if (_config == NULL)
    {
        errorForNoMemory("transactor configuration");
        return;
    }

    // Create channels - do it after configuration
    const AxixDirection writeDir = (xtorType == CarbonXAxi_Master) ?
        AxixDirection_Output : AxixDirection_Input;
    const AxixDirection readDir = (xtorType == CarbonXAxi_Slave) ?
        AxixDirection_Output : AxixDirection_Input;

    _awChannel = new AxixAWChannel(this, writeDir, _config->getIDBusWidth(),
            _config->getAddressBusWidth());
    if (_awChannel == NULL)
    {
        errorForNoMemory("write address channel");
        return;
    }

    _wChannel = new AxixWChannel(this, writeDir, _config->getIDBusWidth(),
            _config->getDataBusWidth());
    if (_wChannel == NULL)
    {
        errorForNoMemory("write data channel");
        return;
    }

    _bChannel = new AxixBChannel(this, readDir, _config->getIDBusWidth());
    if (_bChannel == NULL)
    {
        errorForNoMemory("write response channel");
        return;
    }

    _arChannel = new AxixARChannel(this, writeDir, _config->getIDBusWidth(),
            _config->getAddressBusWidth());
    if (_awChannel == NULL)
    {
        errorForNoMemory("read address channel");
        return;
    }

    _rChannel = new AxixRChannel(this, readDir, _config->getIDBusWidth(),
            _config->getDataBusWidth());
    if (_rChannel == NULL)
    {
        errorForNoMemory("read data channel");
        return;
    }
}

CarbonXAxiClass::~CarbonXAxiClass()
{
    DELETE(_name);
    delete _config;
    delete _writeFsm;
    delete _readFsm;
    delete _awChannel;
    delete _wChannel;
    delete _bChannel;
    delete _arChannel;
    delete _rChannel;
}

void CarbonXAxiClass::errorForNoMemory(AxixConstString objectName)
{
    ASSERT(objectName, this);
    _error = CarbonXAxiError_NoMemory;
    static char errorMessage[1024];
    sprintf(errorMessage, "Can't allocate memory for %s.", objectName);
    _errorMessage = errorMessage;
}

// Get pin pointer from pin name
AxixPin* CarbonXAxiClass::getPinByName(AxixConstString netName)
{
    if (strcmp(netName, "AWID") == 0)
        return _awChannel->_idPin;
    else if (strcmp(netName, "AWADDR") == 0)
        return _awChannel->_addrPin;    
    else if (strcmp(netName, "AWLEN") == 0)
        return _awChannel->_lenPin;    
    else if (strcmp(netName, "AWSIZE") == 0)
        return _awChannel->_sizePin;    
    else if (strcmp(netName, "AWBURST") == 0)
        return _awChannel->_burstPin;    
    else if (strcmp(netName, "AWLOCK") == 0)
        return _awChannel->_lockPin;    
    else if (strcmp(netName, "AWCACHE") == 0)
        return _awChannel->_cachePin;    
    else if (strcmp(netName, "AWPROT") == 0)
        return _awChannel->_protPin;    
    else if (strcmp(netName, "AWVALID") == 0)
        return _awChannel->_validPin;    
    else if (strcmp(netName, "AWREADY") == 0)
        return _awChannel->_readyPin;    
    else if (strcmp(netName, "WID") == 0)
        return _wChannel->_idPin;    
    else if (strcmp(netName, "WDATA") == 0)
        return _wChannel->_dataPin;    
    else if (strcmp(netName, "WSTRB") == 0)
        return _wChannel->_strbPin;    
    else if (strcmp(netName, "WLAST") == 0)
        return _wChannel->_lastPin;    
    else if (strcmp(netName, "WVALID") == 0)
        return _wChannel->_validPin;    
    else if (strcmp(netName, "WREADY") == 0)
        return _wChannel->_readyPin;    
    else if (strcmp(netName, "BID") == 0)
        return _bChannel->_idPin;    
    else if (strcmp(netName, "BRESP") == 0)
        return _bChannel->_respPin;    
    else if (strcmp(netName, "BVALID") == 0)
        return _bChannel->_validPin;    
    else if (strcmp(netName, "BREADY") == 0)
        return _bChannel->_readyPin;    
    else if (strcmp(netName, "ARID") == 0)
        return _arChannel->_idPin;    
    else if (strcmp(netName, "ARADDR") == 0)
        return _arChannel->_addrPin;    
    else if (strcmp(netName, "ARLEN") == 0)
        return _arChannel->_lenPin;    
    else if (strcmp(netName, "ARSIZE") == 0)
        return _arChannel->_sizePin;    
    else if (strcmp(netName, "ARBURST") == 0)
        return _arChannel->_burstPin;    
    else if (strcmp(netName, "ARLOCK") == 0)
        return _arChannel->_lockPin;    
    else if (strcmp(netName, "ARCACHE") == 0)
        return _arChannel->_cachePin;    
    else if (strcmp(netName, "ARPROT") == 0)
        return _arChannel->_protPin;    
    else if (strcmp(netName, "ARVALID") == 0)
        return _arChannel->_validPin;    
    else if (strcmp(netName, "ARREADY") == 0)
        return _arChannel->_readyPin;    
    else if (strcmp(netName, "RID") == 0)
        return _rChannel->_idPin;    
    else if (strcmp(netName, "RDATA") == 0)
        return _rChannel->_dataPin;    
    else if (strcmp(netName, "RRESP") == 0)
        return _rChannel->_respPin;    
    else if (strcmp(netName, "RLAST") == 0)
        return _rChannel->_lastPin;    
    else if (strcmp(netName, "RVALID") == 0)
        return _rChannel->_validPin;    
    else if (strcmp(netName, "RREADY") == 0)
        return _rChannel->_readyPin;    
    return NULL;
}

// Evaluate FSMs
AxixBool CarbonXAxiClass::evaluate(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    retValue &= _writeFsm->update(currentTime);
    retValue &= _readFsm->update(currentTime);
    return retValue;
}

// Refresh channels
AxixBool CarbonXAxiClass::refreshInputs(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    retValue &= _awChannel->refreshInputs(currentTime);
    retValue &= _wChannel->refreshInputs(currentTime);
    retValue &= _bChannel->refreshInputs(currentTime);
    retValue &= _arChannel->refreshInputs(currentTime);
    retValue &= _rChannel->refreshInputs(currentTime);
    return retValue;
}

// Refresh channels
AxixBool CarbonXAxiClass::refreshOutputs(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    retValue &= _awChannel->refreshOutputs(currentTime);
    retValue &= _wChannel->refreshOutputs(currentTime);
    retValue &= _bChannel->refreshOutputs(currentTime);
    retValue &= _arChannel->refreshOutputs(currentTime);
    retValue &= _rChannel->refreshOutputs(currentTime);
    return retValue;
}

// Register callbacks
void CarbonXAxiClass::registerCallbacks(void *userContext,
        void (*reportTransaction)(CarbonXAxiTrans*, void*),
        void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
        void (*setResponse)(CarbonXAxiTrans*, void*),
        void (*errorTransCallback)(CarbonXAxiErrorType, const char*,
            const CarbonXAxiTrans*, void*),
        void (*errorCallback)(CarbonXAxiErrorType, const char*, void*))
{   
    _userContext = userContext;
    _errorTransCallback = errorTransCallback;
    _errorCallback = errorCallback;

    return;
    // remove compiler warnings
    ASSERT(reportTransaction, this);
    ASSERT(setDefaultResponse, this);
    ASSERT(setResponse, this);
}

// Error callback wrapper
void CarbonXAxiClass::error(const CarbonXAxiClass *xtor,
        CarbonXAxiErrorType errorType, AxixConstString errorMessage,
        const CarbonXAxiTrans *trans)
{
    if (xtor != NULL)
    {
        // if transaction for which error happened is available and
        // notify callback is registered
        if (trans != NULL && xtor->_errorTransCallback != NULL)
            (*(xtor->_errorTransCallback))(errorType, errorMessage,
                                        trans, xtor->_userContext);
        // if notify2 callback is registered
        else if (xtor->_errorCallback != NULL)
            (*(xtor->_errorCallback))(errorType, errorMessage,
                                      xtor->_userContext);
        // when notify and notify2 callbacks are not available
        // show the error message in console
        else if (xtor->_name != NULL)
        {
            printf("CarbonXAxi: ERROR[%u]: Transactor %s: %s\n",
                    (UInt) errorType, xtor->_name, errorMessage);
            exit(1);
        }
        else
        {
            printf("CarbonXAxi: ERROR[%u]: %s\n",
                    (UInt) errorType, errorMessage);
            exit(1);
        }
    }
    else
    {
        printf("CarbonXAxi: ERROR[%u]: %s\n", (UInt) errorType, errorMessage);
        exit(1);
    }
}

// Debug routine
void CarbonXAxiClass::print() const
{
    if (getType() == CarbonXAxi_Master)
        printf("AMBA AXI Master Transactor\n");
    else
        printf("AMBA AXI Slave Transactor\n");
    printf("Name           = %s\n", _name);
    printf("Data Bus Width = %u bits\n", _config->getDataBusWidth());
    printf("ID Bus Width   = %u bits\n", _config->getIDBusWidth());
    _writeFsm->print();
    _readFsm->print();
}

// Definition of AxixAssertFail used as a replacement of assert.
UInt AxixAssertFail(const AxiXtor *xtor, AxixConstString failExpr,
        AxixConstString file, UInt line)
{
    AxixString msg = AxixGetStringBuffer();
    sprintf(msg, "INTERNAL ERROR - assertion failed on %s at file %s line %u.",
            failExpr, file, line);
    CarbonXAxiClass::error(xtor, CarbonXAxiError_InternalError, msg);
    return 1;
}
