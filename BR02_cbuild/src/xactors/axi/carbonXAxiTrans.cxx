/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    carbonXAxiTrans.cpp
    Purpose: This file provides definition of APIs for transaction object.
*/

#include "xactors/axi/carbonXAxiTrans.h"
#include "axiXReadTrans.h"
#include "axiXWriteTrans.h"
#include "axiXtor.h"

// Create and destroy functions
CarbonXAxiTrans* carbonXAxiTransCreate(CarbonUInt32 transId,
        CarbonXAxiTransType transType)
{
    CarbonXAxiTrans *trans;
    if (transType == CarbonXAxiTrans_WRITE)
        trans = new AxixWriteTrans(transId);
    else
        trans = new AxixReadTrans(transId);
#if TRANS_NOMEMORY_ERROR
    if (trans == NULL)
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't allocate memory for Transaction Object");
#endif
    return trans;
}

CarbonUInt32 carbonXAxiTransDestroy(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    delete trans;
    return 1;
}

// Access function wrappers
CarbonUInt32 carbonXAxiTransGetId(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->getId();
}

CarbonXAxiTransType carbonXAxiTransGetType(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return CarbonXAxiTrans_WRITE;
    }
    return trans->getType();
}

CarbonUInt32 carbonXAxiTransSetStartAddress(CarbonXAxiTrans *trans,
        CarbonUInt64 startAddress)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setStartAddress(startAddress);
    return 1;
}

CarbonUInt64 carbonXAxiTransGetStartAddress(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->getStartAddress(); 
}

CarbonUInt32 carbonXAxiTransSetBurstLength(CarbonXAxiTrans *trans,
        CarbonUInt32 burstLength)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    if (burstLength > 16)
        return 0;
    else if (burstLength > 0)
        trans->setBurstLength(burstLength - 1);
    // else no burst length is set
    return 1;
}

CarbonUInt32 carbonXAxiTransGetBurstLength(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return (trans->getBurstLength() + 1);
}

CarbonUInt32 carbonXAxiTransSetBurstSize(CarbonXAxiTrans *trans,
        CarbonXAxiBurstSize burstSize)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setBurstSize(burstSize);
    return 1;
}

CarbonXAxiBurstSize carbonXAxiTransGetBurstSize(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return CarbonXAxiBurstSize_DEFAULT;
    }
    return trans->getBurstSize();
}

CarbonUInt32 carbonXAxiTransSetBurstType(CarbonXAxiTrans *trans,
        CarbonXAxiBurstType burstType)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setBurstType(burstType);
    return 1;
}

CarbonXAxiBurstType carbonXAxiTransGetBurstType(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return CarbonXAxiBurst_FIXED;
    }
    return trans->getBurstType();
}

CarbonUInt32 carbonXAxiTransSetLockType(CarbonXAxiTrans *trans,
        CarbonXAxiAccessType lock)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setAccessType(lock);
    return 1;
}

CarbonXAxiAccessType carbonXAxiTransGetLockType(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return CarbonXAxiAccess_NORMAL;
    }
    return trans->getAccessType();
}

CarbonUInt32 carbonXAxiTransSetCacheType(CarbonXAxiTrans *trans,
        CarbonUInt4 cache)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setCacheType(cache);
    return 1;
}

CarbonUInt4 carbonXAxiTransGetCacheType(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->getCacheType();
}

CarbonUInt32 carbonXAxiTransSetProtType(CarbonXAxiTrans *trans,
        CarbonUInt3 prot)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setProtType(prot);
    return 1;
}

CarbonUInt3 carbonXAxiTransGetProtType(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->getProtType();
}

CarbonUInt32 carbonXAxiTransSetData(CarbonXAxiTrans *trans,
        CarbonUInt32 dataSize, const CarbonUInt8 *data)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setData(dataSize, data);
    return 1;
}

const CarbonUInt8* carbonXAxiTransGetData(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->getData();
}

CarbonUInt32 carbonXAxiTransGetDataSize(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->getDataSize();
}

CarbonUInt32 carbonXAxiTransSetWriteStrobe(CarbonXAxiTrans *trans,
        CarbonUInt32 strobeSize, const CarbonUInt1 *strobe)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    if (trans->getType() != CarbonXAxiTrans_WRITE)
        return 0;
    return static_cast<AxixWriteTrans*>(trans)->setStrobe(strobeSize, strobe);
}

const CarbonUInt1* carbonXAxiTransGetWriteStrobe(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    if (trans->getType() != CarbonXAxiTrans_WRITE)
        return NULL;
    return static_cast<AxixWriteTrans*>(trans)->getStrobe();
}

CarbonUInt32 carbonXAxiTransSetWriteResponse(CarbonXAxiTrans *trans,
        CarbonXAxiResponse resp)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    if (trans->getType() != CarbonXAxiTrans_WRITE)
        return 0;
    return static_cast<AxixWriteTrans*>(trans)->setResponse(resp);
}

CarbonXAxiResponse carbonXAxiTransGetWriteResponse(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return CarbonXAxiResponse_OKAY;
    }
    if (trans->getType() != CarbonXAxiTrans_WRITE)
        return CarbonXAxiResponse_SLVERR;
    return static_cast<AxixWriteTrans*>(trans)->getResponse();
}

CarbonUInt32 carbonXAxiTransSetReadResponse(CarbonXAxiTrans *trans,
        CarbonUInt32 respSize, const CarbonXAxiResponse *resp)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    if (trans->getType() != CarbonXAxiTrans_READ)
        return 0;
    return static_cast<AxixReadTrans*>(trans)->setResponse(respSize, resp);
}

const CarbonXAxiResponse* carbonXAxiTransGetReadResponse(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    if (trans->getType() != CarbonXAxiTrans_READ)
        return NULL;
    return static_cast<AxixReadTrans*>(trans)->getResponse();
}

CarbonUInt32 carbonXAxiTransSetDataWaitStates(CarbonXAxiTrans *trans,
        CarbonUInt32 waitSize, const CarbonUInt32 *dataWaits)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->setDataWaits(waitSize, dataWaits);
}

CarbonUInt32 carbonXAxiTransSetResponseWait(CarbonXAxiTrans *trans,
        CarbonUInt32 wait)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setResponseWait(wait);
    return 1;
}

CarbonUInt32 carbonXAxiTransWaitIndefinitely(CarbonXAxiTrans *trans,
        CarbonUInt32 isOn)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    trans->setWaitIndefinite(isOn);
    return 1;
}

CarbonUInt32 carbonXAxiTransGetWaitIndefiniteStatus(CarbonXAxiTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_InvalidTrans,
                "Invalid transaction - NULL transaction passed.");
#endif
        return 0;
    }
    return trans->getWaitIndefinite();
}

void carbonXAxiTransDump(const CarbonXAxiTrans *trans)
{
    if (trans == NULL)
        printf("NULL TRANSACTION\n");
    else
        trans->print();
}
