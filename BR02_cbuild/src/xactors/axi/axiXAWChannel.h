/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_AWCHANNELINTF_H
#define AXIX_AWCHANNELINTF_H

#include "axiXConfig.h"
#include "axiXChannel.h"
#include "axiXWriteTrans.h"

/*!
 * \file axiXAWChannel.h
 * \brief Header file for write address (AW) channel interface definition.
 */

/*!
 * \brief Class for write address i.e. AW Channel
 *
 * This includes AWID, AWADDR, AWLEN, AWSIZE, AWBURST, AWLOCK, AWCACHE and
 * AWPROT pins. AWVALID and AWREADY pins are inherited from base class
 * AxixChannel.
 */
class AxixAWChannel: public AxixChannel
{
public: CARBONMEM_OVERRIDES

public:

    AxixPin * const _addrPin;                 //!< AWADDR pin
    AxixPin * const _lenPin;                  //!< AWLEN pin
    AxixPin * const _sizePin;                 //!< AWSIZE pin
    AxixPin * const _burstPin;                //!< AWBURST pin
    AxixPin * const _lockPin;                 //!< AWLOCK pin
    AxixPin * const _cachePin;                //!< AWCACHE pin
    AxixPin * const _protPin;                 //!< AWPROT pin

public:

    // Constructor and destructor
    AxixAWChannel(AxiXtor *xtor, const AxixDirection channelDir,
            const CarbonUInt32 idBusWidth,
            const CarbonXAxiAddressBusWidth addressBusWidth =
                CarbonXAxiAddressBusWidth_32);
    ~AxixAWChannel();

    // Evaluation to transfer transaction to/from channel interface pins.
    AxixBool transfer(AxixWriteTrans *trans, CarbonTime currentTime);

    // Refresh
    AxixBool refresh(CarbonTime currentTime);
};

#endif
