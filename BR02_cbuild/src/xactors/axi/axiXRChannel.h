/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_RCHANNELINTF_H
#define AXIX_RCHANNELINTF_H

#include "xactors/axi/carbonXAxiConfig.h"
#include "axiXChannel.h"
#include "axiXReadTrans.h"

/*!
 * \file axiXRChannel.h
 * \brief Header file for read data (R) channel interface definition.
 */

/*!
 * \brief Class for read data i.e. R Channel
 *
 * This includes RID, RDATA, RRESP and RLAST pins. RVALID and RREADY pins are
 * inherited from base class AxixChannel.
 */
class AxixRChannel: public AxixChannel
{
public: CARBONMEM_OVERRIDES

private:

    const CarbonUInt32 _dataBusWidth;         //!< Data bus width

public:

    AxixPin * const _dataPin;                 //!< RDATA pin
    AxixPin * const _respPin;                 //!< RRESP pin
    AxixPin * const _lastPin;                 //!< RLAST pin

public:

    // Constructor and destructor
    AxixRChannel(AxiXtor *xtor, const AxixDirection channelDir,
            const CarbonUInt32 idBusWidth,
            const CarbonXAxiDataBusWidth dataBusWidth);
    ~AxixRChannel();

    // Evaluation to transfer transaction to/from channel interface pins.
    // Note, LAST in transaction will denote that data cycle transfers for
    // that particular transaction has been completed. FSM should check LAST
    // on transferred transaction after each call of this API and end data
    // cycle for that transaction.
    AxixBool transfer(AxixReadTrans *trans, CarbonTime currentTime);

    // Refresh
    AxixBool refresh(CarbonTime currentTime);
};

#endif
