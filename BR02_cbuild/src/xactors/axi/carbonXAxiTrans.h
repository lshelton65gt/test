// -*-C++-*-
/*****************************************************************************
 
 Copyright (c) 2008 - 2009 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.
 
******************************************************************************/

#ifndef __carbonXAxiTrans_h_
#define __carbonXAxiTrans_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \addtogroup Axi
 * @{
 */

/*!
 * \file xactors/axi/carbonXAxiTrans.h
 * \brief Definition of Carbon AMBA AXI Transaction Object.
 *
 * This file provides definition of transaction object.
 */

/*! Forward declaration */
STRUCT CarbonXAxiTransClass;

/*!
 * \brief Carbon AMBA AXI Transaction Object
 */
typedef STRUCT CarbonXAxiTransClass CarbonXAxiTrans;

/*!
 * \brief Transaction Types
 *
 * This specifies different types of transactions.
 */
typedef enum {
    CarbonXAxiTrans_WRITE,             /*!< Write transaction */
    CarbonXAxiTrans_READ               /*!< Read transaction */
} CarbonXAxiTransType;

/*!
 * \brief Creates Transaction.
 *
 * This API will create a new transaction with given transaction ID
 * and transaction type( READ or WRITE).
 *
 * \param transId Transaction ID
 * \param transType Transaction type.
 *
 * \returns Transaction object created.
 */
CarbonXAxiTrans* carbonXAxiTransCreate(CarbonUInt32 transId,
        CarbonXAxiTransType transType);

/*!
 * \brief Destroy a transaction object
 *
 * One should use this API to destroy the created transactions from
 * request or response at reportTransaction callback
 *
 * \param trans Pointer to transaction object.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXAxiTransDestroy(CarbonXAxiTrans *trans);

/*!
 * \brief Returns transaction ID.
 *
 * This API will return ID of the received transaction.
 *
 * \param trans Pointer to transaction object.
 *
 * \return Transaction ID.
 */
CarbonUInt32 carbonXAxiTransGetId(CarbonXAxiTrans *trans);

/*!
 * \brief Gets the transaction type.
 *
 * This API will return the received transaction type i.e READ or WRITE.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Transaction type.
 */
CarbonXAxiTransType carbonXAxiTransGetType(CarbonXAxiTrans *trans);

/*!
 * \brief Sets the Starting address of the transaction.
 *
 * This API will set the starting address of the transaction. The address
 * can be unaligned. Default value would be 0x0 if this API is not called.
 *
 * \param trans Pointer to transaction object.
 * \param startAddress Starting address of the transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetStartAddress(CarbonXAxiTrans *trans,
        CarbonUInt64 startAddress);

/*!
 * \brief Gets the Starting address of the transaction.
 *
 * This API will return the starting address of the received transaction.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Starting address of the transaction.
 */
CarbonUInt64 carbonXAxiTransGetStartAddress(CarbonXAxiTrans *trans);

/*!
 * \brief Sets burst length for the current transaction.
 *
 * This API will set the burst length for transaction.
 * This value can range from 1 to 16. Default value is 0 if this
 * API is not called, which means transactor is going to calculate
 * the length from data size and burst size. This is true for write
 * transaction, for read transaction call of this API is must.
 *
 * \param trans Pointer to transaction object.
 * \param length Burst length. Value in LEN bus will be one less from this.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetBurstLength(CarbonXAxiTrans *trans,
        CarbonUInt32 length);

/*!
 * \brief Gets burst length for the received transaction.
 *
 * This API will return the burst length for the received transaction.
 * One can get this at setDefaultResponse() callback at slave for write
 * and read transactions.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Burst length. Value in LEN bus will be one less from this.
 */
CarbonUInt32 carbonXAxiTransGetBurstLength(CarbonXAxiTrans *trans);


/*!
 * \brief Write or read burst size
 *
 * This specifies possible sizes of read and write bursts
 */
typedef enum {
    CarbonXAxiBurstSize_1 = 0,         /*!< Burst size   1 byte  */
    CarbonXAxiBurstSize_2,             /*!< Burst size   2 bytes */
    CarbonXAxiBurstSize_4,             /*!< Burst size   4 bytes */
    CarbonXAxiBurstSize_8,             /*!< Burst size   8 bytes */
    CarbonXAxiBurstSize_16,            /*!< Burst size  16 bytes */
    CarbonXAxiBurstSize_32,            /*!< Burst size  32 bytes */
    CarbonXAxiBurstSize_64,            /*!< Burst size  64 bytes */
    CarbonXAxiBurstSize_128,           /*!< Burst size 128 bytes */
    CarbonXAxiBurstSize_DEFAULT,       /*!< Default burst size, which will be
                                            set to the data bus width by the
                                            transactor. */
} CarbonXAxiBurstSize;

/*!
 * \brief Sets burst size for the current transaction.
 *
 * This API will set the burst size per transfer in bytes for transaction.
 * Default value is CarbonXAxiBurstSize_DEFAULT if this API is not called,
 * which means transactor is going to put data bus width as burst size.
 *
 * \param trans Pointer to transaction object.
 * \param burstSize Burst size.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetBurstSize(CarbonXAxiTrans *trans,
        CarbonXAxiBurstSize burstSize);

/*!
 * \brief Gets burst size for the received transaction.
 *
 * This API will return the burst size per transfer in bytes
 * for the received transaction. One can get this at setDefaultResponse()
 * callback at slave for write and read transactions.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Burst size.
 */
CarbonXAxiBurstSize carbonXAxiTransGetBurstSize(CarbonXAxiTrans *trans);

/*!
 * \brief Write or read burst types
 *
 * This specifies possible types of read and write bursts
 */
typedef enum {
    CarbonXAxiBurst_FIXED = 0,          /*!< Fixed-address burst */
    CarbonXAxiBurst_INCR,               /*!< Incrementing-address burst */
    CarbonXAxiBurst_WRAP                /*!< Incrementing-address burst that
                                             wraps to a lower address at the
                                             wrap boundary */
} CarbonXAxiBurstType;

/*!
 * \brief Sets type of the burst for current transaction.
 *
 * This API will set the burst type for transaction.
 * Default value is CarbonXAxiBurst_INCR, if this API is not called.
 *
 * \param trans Pointer to transaction object.
 * \param burstType Burst type.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetBurstType(CarbonXAxiTrans *trans,
        CarbonXAxiBurstType burstType);

/*!
 * \brief Gets type of the burst for received transaction.
 *
 * This API will return the burst type for the received transaction.
 * One can get this at setDefaultResponse() callback at slave for
 * write and read transactions.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Burst type.
 */
CarbonXAxiBurstType carbonXAxiTransGetBurstType(CarbonXAxiTrans *trans);


/*!
 * \brief Write or read access types
 *
 * This specifies different types of access types supported by
 * the transactor.
 */
typedef enum {
    CarbonXAxiAccess_NORMAL = 0,        /*!< Normal access */
    CarbonXAxiAccess_EXCLUSIVE,         /*!< Exclusive access */
    CarbonXAxiAccess_LOCKED             /*!< Locked access */
} CarbonXAxiAccessType;

/*!
 * \brief Sets atomic access type for the transaction.
 *
 * This API will set the lock type for transaction.
 * Default value is CarbonXAxiAccess_NORMAL, if this API is not called.
 *
 * \param trans Pointer to transaction object.
 * \param lock Atomic access type.
 *
 * \retval 1 For Success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXAxiTransSetLockType(CarbonXAxiTrans *trans,
        CarbonXAxiAccessType lock);

/*!
 * \brief Gets atomic access type for the transaction.
 *
 * This API will return the lock type for the received transaction.
 * One can get this at setDefaultResponse() callback at slave for
 * write and read transactions.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Atomic access type.
 */
CarbonXAxiAccessType carbonXAxiTransGetLockType(CarbonXAxiTrans *trans);

/*!
 * \brief Sets cache support type for the transaction.
 *
 * This API will set the cache type for transaction. This value
 * can range from 0 to 15. Default value is 0 (noncacheable and
 * nonbufferable), if this API is not called.
 *
 * \param trans Pointer to transaction object.
 * \param cacheType Cache support type.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetCacheType(CarbonXAxiTrans *trans,
        CarbonUInt4 cacheType);

/*!
 * \brief Gets cache support type for the transaction.
 *
 * This API will return the cache type for the received transaction.
 * One can get this at setDefaultResponse() callback at slave for write
 * and read transactions.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Cache support type.
 */
CarbonUInt4 carbonXAxiTransGetCacheType(CarbonXAxiTrans *trans);

/*!
 * \brief Sets protection type for the transaction.
 *
 * This API will set the protection type for transaction.
 * This value can range from 0 to 7. Default value is 0 for write
 * transaction, if this API is not called. For read transaction,
 * this API call is must.
 *
 * \param trans Pointer to transaction object.
 * \param prot Protection type.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetProtType(CarbonXAxiTrans *trans,
        CarbonUInt3 prot);

/*!
 * \brief Gets protection type for the received transaction.
 *
 * This API will return the protection type for the received
 * transaction. One can get this at setDefaultResponse() callback
 * at slave for write and read transactions.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Protection type.
 */
CarbonUInt3 carbonXAxiTransGetProtType(CarbonXAxiTrans *trans);

/*!
 * \brief Sets data byte array for transaction
 *
 * This API will set the write data in byte array (size of the array
 * is provided by dataSize) for entire burst of transaction. Data size
 * (i.e. dataSize) can range from 1 to 1024*16. Default value of data
 * will be null array, which will be signaled as error from master
 * transactor for write transaction and from slave transactor for read
 * transaction. So calling this API for transaction is must before master's
 * carbonXAxiStartNewTransaction and at slave's setResponse. Also note, larger
 * data byte array will be truncated.
 *
 * The array is copied, that means memory ownership is to the user.
 *
 * \param trans Pointer to transaction object.
 * \param dataSize Size of the data byte array.
 * \param data Pointer to the data byte array.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetData(CarbonXAxiTrans *trans,
        CarbonUInt32 dataSize, const CarbonUInt8 *data);

/*!
 * \brief Gets data byte array size for received write transaction.
 *
 * This API will return the write data array size received at
 * setResponse() callback at slave. The memory ownership of the returned
 * data array is to the transactor.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Data byte array size.
 */
CarbonUInt32 carbonXAxiTransGetDataSize(CarbonXAxiTrans *trans);

/*!
 * \brief Gets data byte array pointer for received write transaction.
 *
 * This API will return the data array pointer received at
 * setResponse() callback at slave.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Data byte array pointer for received write transaction.
 */
const CarbonUInt8* carbonXAxiTransGetData(CarbonXAxiTrans *trans);

/*!
 * \brief Sets write strobe array for the burst.
 *
 * This API will set the strobe (byte enable) array (size of the
 * array is provided by strobeSize) for write transaction at master.
 * Default is null array which means all Strobes are 1 for all data.
 * Also note, larger byte strobe array will be truncated.
 *
 * The array is copied, that means memory ownership is to the user.
 *
 * \param trans Pointer to transaction object.
 * \param strobeSize Strobe array size.
 * \param strobe Strobe array.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetWriteStrobe(CarbonXAxiTrans *trans,
        CarbonUInt32 strobeSize, const CarbonUInt1 *strobe);

/*!
 * \brief Gets the write data strobe array pointer.
 *
 * This API will return the write data strobe array pointer received at
 * setResponse() callback at slave.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns write data strobe array pointer.
 */
const CarbonUInt1* carbonXAxiTransGetWriteStrobe(CarbonXAxiTrans *trans);


/*!
 * \brief Write or read response status
 *
 * This specifies types of response supported by the transactor.
 */
typedef enum {
    CarbonXAxiResponse_OKAY = 0,      /*!< Ok status */
    CarbonXAxiResponse_EXOKAY,        /*!< Exclusive ok status */
    CarbonXAxiResponse_SLVERR,        /*!< Slave error */
    CarbonXAxiResponse_DECERR         /*!< Decoder error */
} CarbonXAxiResponse;

/*!
 * \brief Sets write response for the received transaction.
 *
 * This API will set the response for write transaction. One need
 * to call this from setResponse() callback of slave for write transaction.
 * Default value is CarbonXAxiResponse_OKAY if this API is not called.
 *
 * \param trans Pointer to transaction object.
 * \param resp Response for the receivesd transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetWriteResponse(
        CarbonXAxiTrans *trans, CarbonXAxiResponse resp);

/*!
 * \brief Gets write response.
 *
 * This API will return the write response. One can call this
 * API at master's reportTransaction() for write transaction.
 *
 * \param trans Pointer to transaction object.
 *
 * \return Response of the receivesd transaction.
 */
CarbonXAxiResponse carbonXAxiTransGetWriteResponse(CarbonXAxiTrans *trans);

/*!
 * \brief Sets response for read transaction.
 *
 * This API will set the response for write transaction - one need to call
 * this from setResponse() callback of slave for write transaction. If less
 * sized array is provided, default value of CarbonXAxiResponse_OKAY will be
 * considered for rest. Same when this API is not called. Also note, larger
 * response array will be truncated.
 *
 * The array is copied, that means memory ownership is to the user.
 *
 * \param trans Pointer to transaction object.
 * \param respSize Size of the response array for read transaction.
 * \param resp Pointer to the response array for read transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetReadResponse(CarbonXAxiTrans *trans,
        CarbonUInt32 respSize, const CarbonXAxiResponse *resp);

/*!
 * \brief Gets response for read transaction.
 *
 * This API will get the response array (size of the array is
 * specified by carbonXAxiTransGetBurstLength()) for read transaction
 * at master i.e. at reportTransaction(). The memory ownership of the
 * returned response array is to the transactor.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns response for write transaction.
 */
const CarbonXAxiResponse* carbonXAxiTransGetReadResponse(
        CarbonXAxiTrans *trans);

/*!
 * \brief Sets wait state for read or write data.
 *
 * This API will set the data wait state array (size of array is
 * denoted by waitSize). This will be set from master as well as slave.
 * For write transaction, master sets this to control WVALID, and
 * slave to control WREADY. For read transaction, slave sets this to
 * control RVALID, and master to control RREADY. Wait states can be
 * provided by master on transaction before calling
 * carbonXAxiStartNewTransaction() and at setDefaultResponse() from
 * slave.
 *
 * The array is copied, that means memory ownership is to the user.
 * Default is null array when this API is not called, which means no
 * waits at VALID or READY. Less sized wait array will be extended with
 * zero waits. Also note, larger wait array will be truncated.
 *
 * \param trans Pointer to transaction object.
 * \param waitSize Size of the wait state array.
 * \param dataWaits pointer to the wait state array.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetDataWaitStates(CarbonXAxiTrans *trans,
        CarbonUInt32 waitSize, const CarbonUInt32 *dataWaits);

/*!
 * \brief Sets wait state for write response in slave.
 *
 * This API will set the write response wait state. This will be set from the
 * slave for write transactions only. Slave sets this to control BVALID.
 * This delay is introduced between last write data transfer and response.
 * This wait state can be provided by the slave on a transaction at the
 * setResponse() callback.
 *
 * This is for support of out-of-order write transactions. In slave side,
 * one can expect completion of a number of write data stages, which results
 * in scheduling a number of responses in the response channel. The slave will
 * monitor such responses queued for their delays and the delay value for the
 * oldest response, queued for each ID, will be decremented per evaluation
 * clock. A response with delay evaluated to zero will be popped out and sent
 * via the response channel.
 *
 * Default is zero delay when this API is not called, which means no
 * waits at BVALID.
 *
 * \param trans Pointer to transaction object.
 * \param wait Wait count.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransSetResponseWait(CarbonXAxiTrans *trans,
        CarbonUInt32 wait);

/*!
 * \brief Marks/unmarks a transaction for indefinite wait in slave.
 *
 * This API can be used either to mark or unmark a transaction for indefinite
 * wait. This will be set from the slave for read or write transactions.
 * For write transaction, such wait will affect the response delay, while
 * for read transaction it will be the first wait of the data cycle. A non-zero
 * value of isOn will set indefinite wait and zero value will make it recover.
 *
 * To enable slave to recover from indefinite wait state for the particular
 * transaction, slave transactor will keep on calling the appropriate callback
 * function with the transaction pointer. For write transaction, the callback
 * function will be setResponse and for read transaction, it will be
 * setDefaultResponse. Once a transaction is unmarked at the callback, the
 * slave recovers from the indefinite wait state and the callback will not be
 * called again for the transaction.
 *
 * \param trans Pointer to transaction object.
 * \param isOn Indefinite wait status to be set.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXAxiTransWaitIndefinitely(CarbonXAxiTrans *trans,
        CarbonUInt32 isOn);

/*!
 * \brief Gets status of a transaction for indefinite wait.
 *
 * This API can be used to know whether indefinite wait is
 * set on the transaction
 *
 * \param trans Pointer to transaction object.
 *
 * \retval 1 If indefinite wait is set.
 * \retval 0 If indefinite wait is not set.
 */
CarbonUInt32 carbonXAxiTransGetWaitIndefiniteStatus(CarbonXAxiTrans *trans);

/*!
 * \brief Dumps the transaction information
 *
 * This routine dumps the transaction info in console.
 * One can use this routine for debug purpose.
 *
 * \param trans Transaction object.
 */
void carbonXAxiTransDump(const CarbonXAxiTrans *trans);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
