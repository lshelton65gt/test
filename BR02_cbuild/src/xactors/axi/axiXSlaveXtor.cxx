/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXSlaveXtor.cpp 
    Purpose: This file contains the definition of the functions for
             slave axi transactor.
*/

#include "axiXSlaveXtor.h"
#include "axiXSlaveWriteFsm.h"
#include "axiXSlaveReadFsm.h"

// Constructor
AxixSlaveXtor::AxixSlaveXtor(AxixConstString name,
        const CarbonXAxiConfig *config, const CarbonUInt32 maxTransfers):
    CarbonXAxiClass(name, config, CarbonXAxi_Slave),
    _setDefaultResponse(NULL), _setResponse(NULL)
{
    // Create Slave Write FSM
    _writeFsm = new AxixSlaveWriteFsm(this, maxTransfers);
    // Check for memory allocation error
    if (_writeFsm == NULL)
    {
        errorForNoMemory("slave write FSM");
        return;
    }
    // Create Slave Read FSM
    _readFsm = new AxixSlaveReadFsm(this, maxTransfers);
    // Check for memory allocation error
    if (_readFsm == NULL)
    {
        errorForNoMemory("slave read FSM");
        return;
    }
}

// Callback registration
void AxixSlaveXtor::registerCallbacks(void *userContext,
        void (*reportTransaction)(CarbonXAxiTrans*, void*),
        void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
        void (*setResponse)(CarbonXAxiTrans*, void*),
        void (*errorTransCallback)(CarbonXAxiErrorType, const char*,
            const CarbonXAxiTrans*, void*),
        void (*errorCallback)(CarbonXAxiErrorType, const char*, void*))
{   
    CarbonXAxiClass::registerCallbacks(userContext, reportTransaction,
            setDefaultResponse, setResponse, errorTransCallback, errorCallback);
    // reportTransaction is not registered
    _setDefaultResponse = setDefaultResponse;
    _setResponse = setResponse;
}

void AxixSlaveXtor::print() const
{
    AxiXtor::print();
}

// Callback wrappers
void AxixSlaveXtor::setDefaultResponse(CarbonXAxiTrans *trans)
{
    if (_setDefaultResponse != NULL)
        (*_setDefaultResponse)(trans, _userContext);
}

void AxixSlaveXtor::setResponse(CarbonXAxiTrans *trans)
{
    if (_setResponse != NULL)
        (*_setResponse)(trans, _userContext);
}

