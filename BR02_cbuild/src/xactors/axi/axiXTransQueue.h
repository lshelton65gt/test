/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_TRANSQUEUE_H
#define AXIX_TRANSQUEUE_H

#include "axiXDefines.h"

/*!
 * \file axiXTransQueue.h
 * \brief Header file for transaction queue implementation.
 *
 * This file provides the class definition for transaction queue.
 */

/*!
 * \brief Queue class
 *
 * The queue is implemented using doubly linked list.
 */
class AxixTransQueue
{
public: CARBONMEM_OVERRIDES
    
    /*;
    * \brief Queue node class
    *
    * This type defines each node of queue.
    */
    class AxixTransQueueItem
    {
    public: CARBONMEM_OVERRIDES

    public:

        AxixTrans * const _trans;    //!< Data item *
        AxixTransQueueItem *_prev;   //!< Link to previous node *
        AxixTransQueueItem *_next;   //!< Link to next node *

        AxixTransQueueItem(AxixTransQueueItem* prev, AxixTrans *trans);
        ~AxixTransQueueItem();
    };

private:

    UInt _count;                     //!< No of items in queue
    UInt _size;                      //!< Max. no of items in queue
    AxixTransQueueItem *_head;       //!< Head of queue
    AxixTransQueueItem *_tail;       //!< Tail of queue
    
public:

    AxixTransQueue(UInt size); // make size = 0 for infinite queue
    ~AxixTransQueue();

    // Utility functions
    // Set get size
    UInt getSize() const { return _size; }  
    void setSize(const UInt size) { _size = size; }
    // Gets fill level
    UInt getCount() const { return _count; }
    AxixBool isEmpty() const { return (_count == 0); }
    AxixBool isFull() const { return (_count == _size); }
    // Pushes new item
    AxixBool push(AxixTrans *trans);
    // Pops oldest item
    AxixTrans *pop();
    // Gets the oldest item without poping
    AxixTrans *top();
    // Removes a particular item from queue
    AxixBool remove(AxixTrans *trans);
};

#endif
