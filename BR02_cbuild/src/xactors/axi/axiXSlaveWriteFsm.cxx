/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXSlaveWriteFsm.cpp
    Purpose: This file contains the definition of the functions for write
             fsm for slave axi transactor.
*/

#include "axiXSlaveWriteFsm.h"
#include "axiXtor.h"

// Config to allow incoming transactions of same ID
// even when transaction table is full
#define ALLOW_SAME_ID_FOR_FULL_TABLE 1
#define NO_WAIT_AFTER_INDEFINITE_WAIT 0

AxixSlaveWriteFsm::AxixSlaveWriteFsm(AxiXtor *xtor,
        const CarbonUInt32 maxTransfers):
    AxixFsm(xtor),
    _dataWait(0),
    _dataTransTable(maxTransfers),
    _dataWaitQueue(0),
    _respTransTable(maxTransfers)
{}

AxixSlaveWriteFsm::~AxixSlaveWriteFsm()
{
    _dataTransTable.deleteAll();
    _respTransTable.deleteAll();
}

// FSM
AxixBool AxixSlaveWriteFsm::update(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;

    //**** Address logic ****
    {
    CarbonUInt32 id = _xtor->getAWChannel()->getIdPin()->read(currentTime);
    AxixBool valid = _xtor->getAWChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getAWChannel()->getReadyPin()->read(currentTime);
    // Transfer should happen at this clock
    if (valid && ready)
    {
        // Create new transaction. You have to delete this after response sent.
        AxixWriteTrans *trans = new AxixWriteTrans(id);
        if (trans == NULL) // if no memory is allocated
        {
            AxiXtor::error(_xtor, CarbonXAxiError_NoMemory,
                    "Can't allocate memory for received write transaction.");
            return AxixFalse;
        }
        // Transfer new address and control
        retValue &= _xtor->getAWChannel()->transfer(trans, currentTime);
        if (DEBUG)
            printf("# SLAVE[%u]:WRITE: ADDRESS transferred\n",
                    static_cast<UInt>(currentTime));
        _xtor->setDefaultResponse(trans);
        // Get the wait of first beat, if not already in wait state
        if (_dataTransTable.isEmpty()) // No transactions pending
            _dataWait = trans->getTransferWait();
        else
            // Push into data wait queue, when data cycle is ongoing
            ASSERT(_dataWaitQueue.push(trans), _xtor);
        // Push the transaction for data cycle retrieval
        if (!_dataTransTable.push(trans))
        {
            // No memory error
             _xtor->errorForNoMemory("Can't push trans in data trans table "
                        "in Slave Write Fsm");
            return AxixFalse;
        }
    }
    // Ready logic for next transfer
    AxixBool nextReady = 1; // default value of READY
    // Make READY low when transactor can not accept any more transactions
    // Note, transactor can't accept a new incoming transaction if
    // the transaction table is full.
    if (_dataTransTable.isFull())
    {
        nextReady = 0;
#if ALLOW_SAME_ID_FOR_FULL_TABLE
        // One exception to recover ready high from low is: when current ready
        // is low and ID for a valid incoming transaction has an entry in table
        if (!ready && valid && _dataTransTable.get(id) != NULL)
            nextReady = 1;
#endif
    }
    _xtor->getAWChannel()->getReadyPin()->write(nextReady, currentTime);
    }

    //**** Data logic ****
    {
    CarbonUInt32 id = _xtor->getWChannel()->getIdPin()->read(currentTime);
    AxixBool valid = _xtor->getWChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getWChannel()->getReadyPin()->read(currentTime);
    // Transfer should happen at this clock
    if (valid && ready)
    {
        // Get the transaction
        AxixWriteTrans *trans =
            static_cast<AxixWriteTrans*>(_dataTransTable.get(id));
        // Check if write data came earlier than write address
        if (trans == NULL)
        {
            AxixString buffer = AxixGetStringBuffer();
            sprintf(buffer, "Protocol error at time %u - "
                    "Write data (ID = %u) came earlier than write address.",
                    static_cast<UInt>(currentTime), id);
            AxiXtor::error(_xtor, CarbonXAxiError_ProtocolError, buffer);
            return AxixFalse;
        }
        // Transfer new data
        retValue &= _xtor->getWChannel()->transfer(trans, currentTime);
        if (DEBUG)
            printf("# SLAVE[%u]:WRITE: DATA BEAT %u transferred\n",
                    static_cast<UInt>(currentTime),
                    trans->getTransferIndex() - 1);
        // For last transfer, end the corresponding data cycle and push
        // it into response table for sending response
        if (trans->getLast())
        {
            _xtor->setResponse(trans);
            ASSERT(_respTransTable.push(_dataTransTable.pop(id)), _xtor);
            // For immediate data transfer after a complete data cycle,
            // wait for first transfer will be obtained from wait queue,
            // which was populated during address cycle
            if (!_dataWaitQueue.isEmpty())
            {
                _dataWait = _dataWaitQueue.pop()->getTransferWait();
                _dataWaitQueue.remove(trans); // remove from wait 0 queue
            }
            else // when no transfer is pending, data wait queue will be empty
                _dataWait = 0;
        }
        else // Get the wait of next beat
            _dataWait = trans->getTransferWait();
    }
    // Ready logic for next transfer
    AxixBool nextReady = 1; // default value of READY
    if (_dataWait > 0)
    {
        nextReady = 0; // make READY low for wait states
        _dataWait--;
    }
    _xtor->getWChannel()->getReadyPin()->write(nextReady, currentTime);
    }

    // **** Response logic ****
    {
    AxixBool valid = _xtor->getBChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getBChannel()->getReadyPin()->read(currentTime);
    // !valid : Bus is idle
    // valid && ready : Transfer is getting complete at slave side
    if (!valid || (valid && ready))
    {
        // Decide for next transfer
        AxixWriteTrans *scheduledTrans = NULL;
        // Loop exactly the current parrallel queue count times
        for (UInt index = 0; index < _respTransTable.getCount(); index++)
        {
            // Get a handle to the next queue
            AxixTransQueue *transQueue = _respTransTable.getQueue(index);
            // Check if the queue is invalid.
            if (transQueue == NULL)
            {
                // Internal code error
                _xtor->error(_xtor, CarbonXAxiError_InternalError,
                        "INTERNAL ERROR - "
                        "invalid queue in response transaction table in "
                        "slave write FSM.");
                return AxixFalse;
            }
            // Get handle to the oldest transaction pending in the queue
            AxixWriteTrans *trans = static_cast<AxixWriteTrans*>
                (transQueue->top());
            ASSERT(trans, _xtor);

            // Decrease response wait count of trans and check if wait is zero
            if (trans->getWaitIndefinite())
            { /* Do nothing -- Do not call any callbacks here */ }
            else if (trans->getResponseWait(AxixTrue) == 0)
            {
                // Schedule this transaction for response transfer if no
                // other transaction is scheduled previously in this clock
                if (!scheduledTrans)
                    scheduledTrans = trans;
            }
        }

        AxixBool nextValid = 0;
        if (scheduledTrans != NULL) // More transactions pending
        {
            // Pop out the transction from the response transTable
            AxixWriteTrans *trans = static_cast<AxixWriteTrans*>
                (_respTransTable.pop(scheduledTrans->getId()));
            ASSERT(trans, _xtor);
            // Transfer new response
            retValue &= _xtor->getBChannel()->transfer(trans, currentTime);
            if (DEBUG)
                printf("# SLAVE[%u]: RESPONSE transferred\n",
                        static_cast<UInt>(currentTime));
            // Delete created transaction - transactor has the memory ownership
            delete trans;
            scheduledTrans = NULL;
            // Making VALID high
            nextValid = 1;
        }
        else // No more transactions pending - so make VALID low
            nextValid = 0;
        _xtor->getBChannel()->getValidPin()->write(nextValid, currentTime);
    }
    // Call setResponse for all those transactions that have indefinite wait
    // flag set to true
    for (UInt index = 0; index < _respTransTable.getCount(); index++)
    {
        AxixWriteTrans *trans = static_cast<AxixWriteTrans*>
            (_respTransTable.getQueue(index)->top());
        ASSERT(trans, _xtor);
        if (trans->getWaitIndefinite())
            _xtor->setResponse(trans);
#if NO_WAIT_AFTER_INDEFINITE_WAIT
        if (!trans->getWaitIndefinite())
            trans->setResponseWait(0);
#endif
    }
    }

    return retValue;
}

void AxixSlaveWriteFsm::print() const
{
    printf("Write transactions pending for:\n"
           "  Data transfer     = %u\n"
           "  Response transfer = %u\n",
            _dataTransTable.getTransCount(),
            _respTransTable.getTransCount());
}
