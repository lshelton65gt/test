// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXAxiConfig_h_
#define __carbonXAxiConfig_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \addtogroup Axi
 * @{
 */

/*!
 * \file xactors/axi/carbonXAxiConfig.h
 * \brief Definition of Carbon AMBA AXI Configuration Object Routines.  
 *
 * User need to create configuration object and set the configuration 
 * parameters on the object. User then need to pass the configuration object 
 * to the transactor's create API. The memory ownership will remain to 
 * the user - transactor will copy the configuration parameters.
 * 
 * Configuration support manipulation allowed are:
 * - ID bus width - 32 (default). 
 * - Address bus width - 32 (default) or 64 bit.
 * - Data bus width - 8, 16, 32 (default), 64, 128, 512 or 1024 bit.
 */

/*! Forward declaration */
STRUCT CarbonXAxiConfigClass;

/*!
 * \brief Carbon AMBA AXI Configuration Object declaration.
 */
typedef STRUCT CarbonXAxiConfigClass CarbonXAxiConfig;

/*!
 * \brief Creates AXI configuration object.
 *
 * \returns Pointer to the created configuration object.
 */
CarbonXAxiConfig *carbonXAxiConfigCreate(void);

/*!
 * \brief Destroys AXI configuration object.
 *
 * \param config Pointer to the configuration object to be destroyed.
 *
 * \retval 1 For success.
 * \retval 0 For failure. 
 */
CarbonUInt32 carbonXAxiConfigDestroy(CarbonXAxiConfig *config);

/*!
 * \brief Sets AXI configuration object ID width.
 *
 * This API will set width of the ID field. Default value is 32.
 *
 * \param config Pointer to the configuration object.
 * \param width Width of the ID field.
 */
void carbonXAxiConfigSetIDBusWidth(CarbonXAxiConfig *config, 
        CarbonUInt32 width);

/*!
 * \brief Get AXI configuration object ID bus width.
 *
 * This API will return width of the ID field.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns width of the ID field.
 */
CarbonUInt32 carbonXAxiConfigGetIDBusWidth(const CarbonXAxiConfig *config);

/*!
 * \brief Address bus width.
 *
 * This specifies address bus width for the transactor.
 */
typedef enum {
    CarbonXAxiAddressBusWidth_32   = 32,    /*!< 32 bit address bus */
    CarbonXAxiAddressBusWidth_64   = 64     /*!< 64 bit address bus */
} CarbonXAxiAddressBusWidth;

/*!
 * \brief Sets AXI configuration object address bus width.
 *
 * This API will set width of the address bus field. Default value is 32.
 *
 * \param config Pointer to the configuration object.
 * \param width Width of the address bus width.
 */
void carbonXAxiConfigSetAddressBusWidth(CarbonXAxiConfig *config, 
        CarbonXAxiAddressBusWidth width);

/*!
 * \brief Get AXI configuration object address bus width.
 *
 * This API will return width of the address bus field.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns width of the address bus width.
 */
CarbonXAxiAddressBusWidth carbonXAxiConfigGetAddressBusWidth(
        const CarbonXAxiConfig *config);

/*!
 * \brief Data bus width.
 *
 * This specifies data bus width for the transactor.
 */
typedef enum {
    CarbonXAxiDataBusWidth_8    = 8,     /*!< 8  bit data bus   */
    CarbonXAxiDataBusWidth_16   = 16,    /*!< 16 bit data bus   */
    CarbonXAxiDataBusWidth_32   = 32,    /*!< 32 bit data bus   */
    CarbonXAxiDataBusWidth_64   = 64,    /*!< 64 bit data bus   */
    CarbonXAxiDataBusWidth_128  = 128,   /*!< 128 bit data bus  */
    CarbonXAxiDataBusWidth_256  = 256,   /*!< 256 bit data bus  */
    CarbonXAxiDataBusWidth_512  = 512,   /*!< 512 bit data bus  */
    CarbonXAxiDataBusWidth_1024 = 1024   /*!< 1024 bit data bus */
} CarbonXAxiDataBusWidth;

/*!
 * \brief Sets AXI configuration object data bus width.
 *
 * This API will set data bus width. Default value is 32.
 * 
 * \param config Pointer to the configuration object.
 * \param width  Data bus width.
 */
void carbonXAxiConfigSetDataBusWidth(CarbonXAxiConfig *config,
        CarbonXAxiDataBusWidth width);

/*!
 * \brief Get AXI configuration object data bus width.
 *
 * This API will return data bus width.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns Data bus width.
 */
CarbonXAxiDataBusWidth carbonXAxiConfigGetDataBusWidth(
        const CarbonXAxiConfig *config);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif

