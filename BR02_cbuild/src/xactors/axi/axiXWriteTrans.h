/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_WRITETRANS_H
#define AXIX_WRITETRANS_H

#include "axiXTrans.h"

/*!
 * \file axiXWriteTrans.h
 * \brief Header file for Write Transaction Class.
 */

/*!
 * \brief Class to represent Write Transaction.
 */
class AxixWriteTrans : public CarbonXAxiTransClass
{
public: CARBONMEM_OVERRIDES

private:

    CarbonUInt32 _strobeSize;         //!< Size of strobe array
    CarbonUInt1 *_strobe;             //!< Write strobe (byte enable) array
    CarbonXAxiResponse _response;     //!< Write response

public:

    // Constructor and destructor
    AxixWriteTrans(CarbonUInt32 id);
    ~AxixWriteTrans();

    // Access functions
    CarbonXAxiTransType getType() const
    { return CarbonXAxiTrans_WRITE; }

    // Creates arrays to hold data and strobe for incoming write transaction
    AxixBool createData();

    // Set get for strobe
    AxixBool setStrobe(CarbonUInt32 strobeSize, const CarbonUInt1 *strobe);
    CarbonUInt32 getStrobeSize() const 
    { return _strobeSize; }
    CarbonUInt1 *getStrobe() const 
    { return _strobe; }

    // Set get for response
    AxixBool setResponse(const CarbonXAxiResponse response)
    { _response = response; return AxixTrue; }
    CarbonXAxiResponse getResponse() const
    { return _response; }

    // Validity check function
    AxixBool checkValidity(const AxiXtor *xtor) const;

    // Debugging function
    void print() const;
};

#endif
