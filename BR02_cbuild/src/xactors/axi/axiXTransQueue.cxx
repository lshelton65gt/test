/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXTransQueue.cpp
    Purpose: This file implements the member functions of transaction
             queue for axi transactor.
*/

#include "axiXtor.h"
#include "axiXTransQueue.h"

AxixTransQueue::AxixTransQueueItem::AxixTransQueueItem(AxixTransQueueItem* prev,
        AxixTrans *trans): _trans(trans), _prev(prev), _next(NULL)
{
    ASSERT(_trans, NULL);
    if (_prev != NULL)
        _prev->_next = this;
}

AxixTransQueue::AxixTransQueueItem::~AxixTransQueueItem()
{
    if (_next != NULL)
        _next->_prev = NULL;
}

AxixTransQueue::AxixTransQueue(UInt size): _count(0), _size(size),
    _head(NULL), _tail(NULL)
{}

AxixTransQueue::~AxixTransQueue()
{
    // free up all nodes
    while (_head != NULL)
    {
        AxixTransQueueItem *nextHead = _head->_next;
        delete _head;
        _head = nextHead;
    }
}

AxixBool AxixTransQueue::push(AxixTrans *trans)
{
    if (_size > 0 && _count >= _size) // if no more items allowed 
        return AxixFalse;

    // create the node 
    _tail = new AxixTransQueueItem(_tail, trans);
    if (_tail == NULL)
        return AxixFalse;
    if (_count == 0) // when queue is empty
        _head = _tail;
    
    _count++;
    return AxixTrue;
}

AxixTrans* AxixTransQueue::pop()
{
    if (_count == 0) // queue is empty
        return NULL;

    // items are always poped from the head of the queue 
    AxixTransQueueItem *item = _head;
    _count--;

    if (_count == 0) // if queue becomes empty after pop
        _head = _tail = NULL;
    else // if queue has some item after pop
        _head = item->_next;

    // retrieve the trans and free the memory for queue node
    AxixTrans *trans = item->_trans;
    delete item;
    return trans;
}

AxixTrans* AxixTransQueue::top()
{
    if (_count == 0) // queue is empty
        return NULL;

    // get the trans corresponding to head
    return _head->_trans;
}

// Deletes a particular transaction if exists in queue
AxixBool AxixTransQueue::remove(AxixTrans *trans)
{
    // search for the transaction in queue
    AxixTransQueueItem *item = _head;
    while (item != NULL && item->_trans != trans)
        item = item->_next;

    // if not found
    if (item == NULL)
        return AxixFalse;

    // delete node containing the transaction
    _count--;
    if (_count == 0) // if queue becomes empty after delete
        _head = _tail = NULL;
    else if (item == _head) // if its at head
    {
        _head = _head->_next;
    }
    else if (item == _tail) // if its at tail
    {
        _tail = _tail->_prev;
        _tail->_next = NULL;
    }
    else // when transaction is at middle
    {
        item->_next->_prev = item->_prev;
        item->_prev->_next = item->_next;
        item->_next = NULL;
    }
    delete item;
    return AxixTrue;
}
