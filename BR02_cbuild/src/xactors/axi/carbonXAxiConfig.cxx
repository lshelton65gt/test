/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:      carbonXAxiConfig.cpp 
    Purpose:   This file provides definition of AXI Configuration 
               Object Routines.
*/

#include "xactors/axi/carbonXAxiConfig.h"
#include "axiXtor.h"
#include "axiXConfig.h"

// Create and destroy functions
CarbonXAxiConfig *carbonXAxiConfigCreate(void)
{
    CarbonXAxiConfig *configObj = new CarbonXAxiConfig();
    return configObj;
}

CarbonUInt32 carbonXAxiConfigDestroy(CarbonXAxiConfig *config)
{
    delete config;
    config = NULL;
    return 1;
}

// ID bus width access functions
void carbonXAxiConfigSetIDBusWidth(CarbonXAxiConfig *config, 
        CarbonUInt32 width)
{
    if (config == NULL)
        return;
    config->setIDBusWidth(width);
}

CarbonUInt32 carbonXAxiConfigGetIDBusWidth(const CarbonXAxiConfig *config)
{
    if (config == NULL)
        return 0;
    return config->getIDBusWidth();
}

// Address bus width access functions
void carbonXAxiConfigSetAddressBusWidth(CarbonXAxiConfig *config, 
        CarbonXAxiAddressBusWidth width)
{
    if (config == NULL)
        return;
    config->setAddressBusWidth(width);
}

CarbonXAxiAddressBusWidth carbonXAxiConfigGetAddressBusWidth(
        const CarbonXAxiConfig *config)
{
    if (config == NULL)
        return CarbonXAxiAddressBusWidth_32;
    return config->getAddressBusWidth();
}

// Data bus width access functions
void carbonXAxiConfigSetDataBusWidth(CarbonXAxiConfig *config,
        CarbonXAxiDataBusWidth width)
{
    if (config == NULL)
        return;
    config->setDataBusWidth(width);
}

CarbonXAxiDataBusWidth carbonXAxiConfigGetDataBusWidth(
        const CarbonXAxiConfig *config)
{
    if (config == NULL)
        return CarbonXAxiDataBusWidth_8;
    return config->getDataBusWidth();
}

