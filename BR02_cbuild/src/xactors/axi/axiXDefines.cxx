/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
      File    :  axiXDefines.cpp
      Purpose :  Provides some utility function for axi transactor.
*/

#define DEF_BOOL_CONST /* This file actually defines AxixTrue/False */
#include "axiXDefines.h"

/* This routine is used to check if memory is available - used for emulation
 * of no memory situation
 * AxixMemoryIndex: Memory allocated till now thru NEW macro
 * CarbonXAxiMaxMemory: Max memory can be allocated - This can be
 *                      modified to emulate no memory situation */
UInt AxixMemoryIndex = 0;
CarbonUInt32 CarbonXAxiMaxMemory = 0;
UInt AxixIsMemAvail(size_t size)
{
    if (CarbonXAxiMaxMemory <= 0) // Infinite memory - allocation can
                                  // fail depending on system
        return 1;
    // say no memory when CarbonXAxiMaxMemory is reached
    if (AxixMemoryIndex + size > CarbonXAxiMaxMemory)
        return 0;
    AxixMemoryIndex += static_cast<UInt>(size); // Increment memory count
    return 1;
}

// Project specific strdup
AxixString AxixStrdup(AxixConstString str)
{
    AxixString dup = NEWARRAY(char, strlen(str) + 1);
    if (dup == NULL)
        return NULL;
    strcpy(dup, str);
    return dup;
}

// Gets a temporary string buffer for creating name, user messages etc.
AxixString AxixGetStringBuffer()
{
    static char buffer[1024];
    *buffer = 0;
    return buffer;
}

