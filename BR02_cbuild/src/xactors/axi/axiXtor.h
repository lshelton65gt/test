/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_XTOR_H
#define AXIX_XTOR_H

#include "xactors/axi/carbonXAxi.h"
#include "axiXConfig.h"
#include "axiXAWChannel.h"
#include "axiXARChannel.h"
#include "axiXBChannel.h"
#include "axiXRChannel.h"
#include "axiXWChannel.h"
#include "axiXTrans.h"
#include "axiXFsm.h"

#ifndef _NO_LICENSE
#include "util/UtCLicense.h"
#endif

/*!
 * \file axiXtor.h
 * \brief Header file for transactor definition
 * 
 * This file provides the definition of the class of transactor and 
 * declares the functions used to update the FSM and to make a transaction.
 */

/*!
 * \brief This class defines the transactor.
 *
 * This class contains the functions for initializing and updating the 
 * transactor and for compliting a transaction.
 */
class CarbonXAxiClass
{
public: CARBONMEM_OVERRIDES

private:

    AxixConstString _name;                 //!< Name of the transactor
    CarbonXAxiConfig *_config;             //!< Pointer to the configuration 
                                           //   object for this transactor
protected:

    // FSMs
    AxixFsm *_writeFsm;                      //!< Write FSM unit
    AxixFsm *_readFsm;                       //!< Read FSM unit
   
private:

    /* Building units */
    AxixAWChannel *_awChannel;             //!< Address read channel
    AxixWChannel  *_wChannel;              //!< write data channel
    AxixBChannel  *_bChannel;              //!< Write response channel
    AxixARChannel *_arChannel;             //!< Address read channel
    AxixRChannel  *_rChannel;              //!< Read data channel
   
private:

    CarbonXAxiErrorType _error;            //!< Holds the type of error 
                                           //   encountered
    AxixConstString _errorMessage;         //!< Corresponding error message
    
    //! Callback when error happened for a transaction
    void (*_errorTransCallback)(CarbonXAxiErrorType, const char*,
            const CarbonXAxiTrans*, void*);
    //! Callback when error happened otherwise
    void (*_errorCallback)(CarbonXAxiErrorType, const char*, void*);

protected:

    //! Some user data to provide the callback functions as argument
    void *_userContext;
          
public:
#ifndef _NO_LICENSE
    /* Carbon licensing parameter */
    CarbonLicenseData *_carbonLicense;
#endif

public:

    //! Constructor
    CarbonXAxiClass(AxixConstString name, const CarbonXAxiConfig *config,
            CarbonXAxiType xtorType);
    //! Destructor
    virtual ~CarbonXAxiClass();
    
    //! This function returns transactor's name
    AxixConstString getName() const { return _name;}
    //! Returns the current configuration
    CarbonXAxiConfig* getConfig() const {return _config;}
    //! This function returns transactor's type
    virtual CarbonXAxiType getType() const = 0;
    //! This function prints the current status of the transactor
    virtual void print() const;    
    //! Returns the current error encountered
    CarbonXAxiErrorType getError() const { return _error; }
    //! Returns the string message for current error encountered
    AxixConstString getErrorMessage() const { return _errorMessage; }
    //! Sets error and error message for no memory error
    void errorForNoMemory(AxixConstString objectName);

    //! Given the pin name this function returns the pin handle
    AxixPin* getPinByName(AxixConstString netName);

    //! Channel access
    AxixAWChannel *getAWChannel() { return _awChannel; }
    AxixWChannel  *getWChannel() { return _wChannel; }
    AxixBChannel  *getBChannel() { return _bChannel; }
    AxixARChannel *getARChannel() { return _arChannel; }
    AxixRChannel  *getRChannel() { return _rChannel; }

    //! Evaluation and refresh functions
    AxixBool evaluate(CarbonTime currentTime);
    AxixBool refreshInputs(CarbonTime currentTime);
    AxixBool refreshOutputs(CarbonTime currentTime);
    
    //! Callback registation function
    virtual void registerCallbacks(void *userContext,
            void (*reportTransaction)(CarbonXAxiTrans*, void*),
            void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
            void (*setResponse)(CarbonXAxiTrans*, void*),
            void (*errorTransCallback)(CarbonXAxiErrorType, const char*,
                const CarbonXAxiTrans*, void*),
            void (*errorCallback)(CarbonXAxiErrorType, const char*, void*));
    virtual void setDefaultResponse(CarbonXAxiTrans *trans)
    { ASSERT(trans, this); ASSERT(0, this); }
    virtual void setResponse(CarbonXAxiTrans *trans)
    { ASSERT(trans, this); ASSERT(0, this); }
    virtual void reportTransaction(CarbonXAxiTrans *trans)
    { ASSERT(trans, this); ASSERT(0, this); }

    //! Function for setting the error type encountered
    static void error(const CarbonXAxiClass *xtor,
            CarbonXAxiErrorType errorType, AxixConstString errorMessage = NULL,
            const CarbonXAxiTrans *trans = NULL);

    //! Starts a new transaction
    virtual AxixBool canAcceptNewTrans(CarbonXAxiTransType transType) const = 0;
    virtual AxixBool startNewTransaction(CarbonXAxiTrans *trans) = 0;
};

#endif
