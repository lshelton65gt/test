/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_TRANS_H
#define AXIX_TRANS_H

#include "axiXDefines.h"
#include "xactors/axi/carbonXAxiTrans.h"

/*!
 * \file axiXTrans.h
 * \brief Header file for transaction base class.
 */

#define TRANS_NOMEMORY_ERROR 0
#define NULL_TRANS_ERROR 1

/*!
 * \brief Class to represent basic transaction object.
 */
class CarbonXAxiTransClass
{
public: CARBONMEM_OVERRIDES

private:

    // Common for all channels
    const CarbonUInt32 _id;                //!< Transaction ID, corresponds to
                                           //   ID buses of all channels
    // Address information
    CarbonUInt64 _startAddress;            //!< Start address 
    CarbonXAxiBurstType _burstType;        //!< Burst type   
    CarbonXAxiBurstSize _burstSize;        //!< Burst size
    AxixBool _noBurstLengthSet;            //!< No burst length set
    CarbonUInt4 _burstLength;              //!< Encoded burst length, 0 - 15
    CarbonXAxiAccessType _lock;            //!< Access type
    CarbonUInt4 _cache;                    //!< Cache type
    CarbonUInt3 _prot;                     //!< Protection type
    // Data information
    CarbonUInt32 _dataSize;                //!< Data size provided by user
    CarbonUInt8 *_data;                    //!< Data byte array
    static AxixBool _useStrobeForLessData; //!< This will allow less amount of
                                           //   of data to be specified.
                                           //   That is, for less data
                                           //   zero strobe will be used.
    // Data transfer tracking
    UInt _byteIndexToTransfer;             //!< Index of data byte to be
                                           //   tranferred in next beat
    UInt _transferIndex;                   //!< Transfer index. Data channels
                                           //   will increment for each transfer
    CarbonUInt1 _last;                     //!< Last which will be asserted
                                           //   for last transfer
    // Data wait states to emulate delays in data transfers
    CarbonUInt32 _dataWaitSize;            //!< Data wait array size
    CarbonUInt32 *_dataWaits;              //!< Data wait array

    // Response wait states to emulate delays in response transfers
    CarbonUInt32 _responseWait;            //!< Resopnse wait count

    // Transfer status flag to emulate indefinite delays in data or response
    // transfers in read or write transactions
    AxixBool _waitIndefinite;              //!< Wait indefinitely if true

public:

    // Constructor and destructor
    CarbonXAxiTransClass(CarbonUInt32 id);
    virtual ~CarbonXAxiTransClass();

    // Access routines
    virtual CarbonXAxiTransType getType() const = 0;
    CarbonUInt32 getId() const
    { return _id; }

    // Set get for start address
    void setStartAddress(CarbonUInt64 startAddress)
    { _startAddress = startAddress; }
    CarbonUInt64 getStartAddress() const
    { return _startAddress; }

    // Set get for burst type
    void setBurstType(CarbonXAxiBurstType burstType)
    { _burstType = burstType; }
    CarbonXAxiBurstType getBurstType() const
    { return _burstType; }

    // Set get for burst size
    void setBurstSize(CarbonXAxiBurstSize burstSize)
    { _burstSize = burstSize; }
    CarbonXAxiBurstSize getBurstSize() const
    { return _burstSize; }

    // Set get for burst length
    void setBurstLength(UInt burstLength)
    { _noBurstLengthSet = AxixFalse; _burstLength = burstLength; }
    UInt isBurstLengthSet() const
    { return _noBurstLengthSet; }
    UInt getBurstLength() const
    { return _burstLength; }

    // Set get for lock type
    void setAccessType(CarbonXAxiAccessType accessType)
    { _lock = accessType; }
    CarbonXAxiAccessType getAccessType() const
    { return _lock; }

    // Set get for cache type
    void setCacheType(CarbonUInt4 cache)
    { _cache = cache; }
    CarbonUInt4 getCacheType() const
    { return _cache; }

    // Set get for protection type
    void setProtType(CarbonUInt3 prot) 
    { _prot = prot; }
    CarbonUInt3 getProtType() const
    { return _prot; }
    
    // Set get for data - data gets copied
    AxixBool setData(CarbonUInt32 dataSize, const CarbonUInt8 *data);
    virtual AxixBool createData(); // creates empty data array and strobe or
                                   // response array for write data cycle in
                                   // slave and read data cycle in master
    CarbonUInt32 getDataSize() const 
    {return _dataSize; }
    CarbonUInt8 *getData() const
    { return _data; }
    // Check config - if FSM can accept less data using strobes
    AxixBool useStrobeForLessData() const
    { return _useStrobeForLessData; }

    // Resolve default values of burst length and burst width
    virtual AxixBool resolveDefaults(const AxiXtor *xtor);

    // Transfer routines
    CarbonUInt32 getActualDataSize() const; // actual data size, calculated
                                            // from start address and control
                                            // parameters
    // Gets lower and upper byte lanes for given data bus width
    void getByteLanes(CarbonUInt32 dataBusWidth,
            UInt &lowerByteLane, UInt &upperByteLane) const;

    // Gets next byte index to be transferred
    UInt getByteIndexToTransfer() const
    { return _byteIndexToTransfer; }
    // Uodates byte index
    void updateByteIndexToTransfer()
    { _byteIndexToTransfer++; }

    // Get update transfer index
    UInt getTransferIndex() const
    { return _transferIndex; }
    void updateTransferIndex()
    { _transferIndex++; }

    // Get set for last
    void setLast(CarbonUInt1 last)
    { _last = last; }
    CarbonUInt1 getLast() const
    { return _last; }

    // Set get for wait states - waits get copied
    AxixBool setDataWaits(CarbonUInt32 waitSize, const CarbonUInt32 *dataWaits);
    CarbonUInt32 getDataWaitSize() const
    { return _dataWaitSize; }
    const CarbonUInt32* getDataWaits() const
    { return _dataWaits; }
    void setResponseWait(CarbonUInt32 wait)
    { _responseWait = wait; }
    CarbonUInt32 getResponseWait(AxixBool update = AxixFalse);
    // Set get for indefinite wait status
    void setWaitIndefinite(CarbonUInt32 isOn)
    { _waitIndefinite = (isOn != 0); }
    AxixBool getWaitIndefinite() const
    { return _waitIndefinite; }
    //! Check for wait for a particular transfer index
    //  If update is true and wait > 0, decrement the wait
    CarbonUInt32 getTransferWait(AxixBool update = AxixFalse);

    // Validity check function
    virtual AxixBool checkValidity(const AxiXtor *xtor) const;

    // Debugging function
    virtual void print() const;

    // Utility functions
    static CarbonUInt8* copyData(CarbonUInt32 size, const CarbonUInt8 *data);
    static CarbonUInt32* copyData(CarbonUInt32 size, const CarbonUInt32 *data);
};

#endif
