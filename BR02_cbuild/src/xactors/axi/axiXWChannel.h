/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_WCHANNELINTF_H
#define AXIX_WCHANNELINTF_H

#include "xactors/axi/carbonXAxiConfig.h"
#include "axiXChannel.h"
#include "axiXWriteTrans.h"

/*!
 * \file axiXWChannel.h
 * \brief Header file for write data (W) channel interface definition.
 */

/*!
 * \brief Class for write data i.e. W Channel
 *
 * This includes WID, WDATA, WSTRB and WLAST pins. WVALID and WREADY pins are
 * inherited from base class AxixChannel.
 */
class AxixWChannel: public AxixChannel
{
public: CARBONMEM_OVERRIDES

private:

    const CarbonUInt32 _dataBusWidth;         //!< Data bus width

public:

    AxixPin * const _dataPin;                 //!< WDATA pin
    AxixPin * const _strbPin;                 //!< WSTRB pin
    AxixPin * const _lastPin;                 //!< WLAST pin

public:

    // Constructor and destructor
    AxixWChannel(AxiXtor *xtor, const AxixDirection channelDir,
            const CarbonUInt32 idBusWidth,
            const CarbonXAxiDataBusWidth dataBusWidth);
    ~AxixWChannel();

    // Evaluation to transfer transaction to/from channel interface pins.
    // Note, LAST in transaction will denote that data cycle transfers for
    // that particular transaction has been completed. FSM should check LAST
    // on transferred transaction after each call of this API and end data
    // cycle for that transaction.
    AxixBool transfer(AxixWriteTrans *trans, CarbonTime currentTime);

    // Refresh
    AxixBool refresh(CarbonTime currentTime);
};

#endif
