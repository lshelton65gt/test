/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/* 
    File:    axiXRChannel.cpp
    Purpose: This file contains the definition of the functions for 
             accessing read(R) channel.
*/

#include "axiXRChannel.h"
#include "axiXtor.h"

// Constructor
AxixRChannel::AxixRChannel(AxiXtor *xtor,
        const AxixDirection channelDir, const CarbonUInt32 idBusWidth,
        const CarbonXAxiDataBusWidth dataBusWidth):
    AxixChannel(xtor, "R", channelDir, idBusWidth),
    _dataBusWidth(dataBusWidth),
    _dataPin(new AxixPin(xtor, "RDATA", channelDir, dataBusWidth)),
    _respPin(new AxixPin(xtor, "RRESP", channelDir, 2)),
    _lastPin(new AxixPin(xtor, "RLAST", channelDir, 1))
{
    // Check for allocation error
    if (_dataPin == NULL)
    {
        xtor->errorForNoMemory("RDATA");
        return;
    }
    if (_respPin == NULL)
    {
        xtor->errorForNoMemory("RRESP");
        return;
    }
    if (_lastPin == NULL)
    {
        xtor->errorForNoMemory("RLAST");
        return;
    }
}

AxixRChannel::~AxixRChannel()
{
    delete _dataPin;
    delete _respPin;
    delete _lastPin;
}

// Transfers the transaction info to and from channel
AxixBool AxixRChannel::transfer(AxixReadTrans *trans, CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;

    ASSERT(trans, _xtor);
    // Error when transaction burst size is larger than data bus width
    if (static_cast<UInt>(1 << trans->getBurstSize()) * 8 > _dataBusWidth)
    {
        AxixString buffer = AxixGetStringBuffer();
        sprintf(buffer, "Invalid transaction at time %u - "
                "burst size %u bytes is more than data bus width %u bits.",
                static_cast<UInt>(currentTime),
                1 << trans->getBurstSize(), _dataBusWidth);
        AxiXtor::error(_xtor, CarbonXAxiError_InvalidTrans, buffer, trans);
        return AxixFalse;
    }
    ASSERT(trans->getLast() == 0, _xtor); // last == 1 means data transfer
                                          // is complete in earlier cycle

    // Get lower and upper byte lanes
    UInt lowerByteLane, upperByteLane;
    trans->getByteLanes(_dataBusWidth, lowerByteLane, upperByteLane);

    CarbonUInt32 data[32];
    CarbonUInt8 *dataBytes = trans->getData();
    CarbonXAxiResponse *response = trans->getResponse();
    if (_channelDir == AxixDirection_Input)
    {
        if (dataBytes == NULL)
        {
            // Create data and response array for incoming read data at master
            if (!trans->createData()) // create data and response array
            {
                AxiXtor::error(_xtor, CarbonXAxiError_NoMemory,
                        "Can't allocate memory for "
                        "received read transaction data.", trans);
                return AxixFalse;
            }
            dataBytes = trans->getData(); // get pointer to create data array
            response = trans->getResponse();
        }
        ASSERT(trans->getId() == _idPin->read(currentTime), _xtor);
        _dataPin->read(data, currentTime);
        // 32 bit array to byte array convertion for data
        for (UInt byteLane = 0; byteLane < _dataBusWidth / 8; byteLane++)
        {
            CarbonUInt32 &data32 = data[byteLane / 4];
            if (byteLane >= lowerByteLane && byteLane <= upperByteLane)
            {
                // For valid byte lane, transfer byte
                UInt byteIndex = trans->getByteIndexToTransfer();
                dataBytes[byteIndex] = data32 & 0xFF;
                trans->updateByteIndexToTransfer();
            }
            data32 = data32 >> 8;
        }
        ASSERT(trans->getTransferIndex() < trans->getResponseSize(), _xtor);
        response[trans->getTransferIndex()] =
            static_cast<CarbonXAxiResponse>(_respPin->read(currentTime));
        trans->setLast(_lastPin->read(currentTime));

        // Check protocol error i.e. last should be high iff last data
        // transfer of transfer index equal to burst length
        if ((trans->getTransferIndex() == trans->getBurstLength()) ^
                (trans->getLast()))
        {
            AxixString buffer = AxixGetStringBuffer();
            sprintf(buffer, "Protocol error at time %u - "
                "RLAST should be high only for last transfer (%u), "
                "while transfer %u is ongoing.",
                static_cast<UInt>(currentTime),
                trans->getBurstLength(), trans->getTransferIndex());
            AxiXtor::error(_xtor, CarbonXAxiError_ProtocolError, buffer, trans);
            return AxixFalse;
        }
    }
    else
    {
        // Error when less number of data provided than required
        if (dataBytes == NULL || (!trans->useStrobeForLessData() &&
                trans->getDataSize() < trans->getActualDataSize()))
        {
            AxixString buffer = AxixGetStringBuffer();
            sprintf(buffer, "Invalid transaction at time %u - "
                    "less number (%u) of data provided.",
                    static_cast<UInt>(currentTime),
                    (dataBytes == NULL) ? 0 : trans->getDataSize());
            AxiXtor::error(_xtor, CarbonXAxiError_InvalidTrans, buffer, trans);
            return AxixFalse;
        }
        _idPin->write(trans->getId(), currentTime);
        // byte array to 32 bit array convertion for data
        for (UInt byteLane = 0; byteLane < _dataBusWidth / 8; byteLane++)
        {
            CarbonUInt32 &data32 = data[byteLane / 4];
            if (byteLane % 4 == 0)
                data32 = 0; // Initalize 32 bit data item
            if (byteLane >= lowerByteLane && byteLane <= upperByteLane)
            {
                UInt byteIndex = trans->getByteIndexToTransfer();
                if (byteIndex < trans->getDataSize())
                    data32 |= (dataBytes[byteIndex] & 0xFF) <<
                        (byteLane % 4) * 8;
                trans->updateByteIndexToTransfer();
            }
        }
        _dataPin->write(data, currentTime);
        // Transfer response
        CarbonXAxiResponse resp = CarbonXAxiResponse_OKAY;
        if (trans->getTransferIndex() < trans->getResponseSize())
            resp = response[trans->getTransferIndex()];
        _respPin->write(static_cast<CarbonUInt32>(resp), currentTime);
        UInt introError = 0; // You can introduce error by making this > 0
        // Last assertion
        if (trans->getTransferIndex() == trans->getBurstLength() - introError)
            trans->setLast(1);
        _lastPin->write(trans->getLast(), currentTime);
    }
    trans->updateTransferIndex();

    return retValue;
}

// Refresh channel pins
AxixBool AxixRChannel::refresh(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    retValue &= _dataPin->refresh(currentTime);
    retValue &= _respPin->refresh(currentTime);
    retValue &= _lastPin->refresh(currentTime);
    return retValue;
}
