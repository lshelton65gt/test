/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXTrans.cpp 
    Purpose: This file contains the definition of the base class of 
             axi transaction.
*/

#include "axiXTrans.h"
#include "axiXtor.h"

// configuration init
AxixBool CarbonXAxiTransClass::_useStrobeForLessData = AxixTrue;

CarbonXAxiTransClass::CarbonXAxiTransClass(CarbonUInt32 id):
    _id(id), _startAddress(0), _burstType(CarbonXAxiBurst_INCR),
    _burstSize(CarbonXAxiBurstSize_DEFAULT),
    _noBurstLengthSet(AxixTrue), _burstLength(0),
    _lock(CarbonXAxiAccess_NORMAL), _cache(0), _prot(0),
    _dataSize(0), _data(NULL),
    _byteIndexToTransfer(0), _transferIndex(0), _last(0),
    _dataWaitSize(0), _dataWaits(NULL), _responseWait(0),
    _waitIndefinite(AxixFalse)
{}

CarbonXAxiTransClass::~CarbonXAxiTransClass()
{
    if (_data != NULL)
        DELETE(_data);
    if (_dataWaits != NULL)
        DELETE(_dataWaits);
}

AxixBool CarbonXAxiTransClass::createData()
{
    _dataSize = getActualDataSize();
    _data = NEWARRAY(CarbonUInt8, _dataSize);
    if (_data == NULL)
    {
        _dataSize = 0;
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't allocate memory for Data Array");
#endif
        return AxixFalse;
    }
    return AxixTrue;
}

AxixBool CarbonXAxiTransClass::setData(CarbonUInt32 dataSize, 
        const CarbonUInt8 *data)
{
    _dataSize = dataSize;
    _data = copyData(_dataSize, data);
    if (_data == NULL)
    {
        _dataSize = 0;
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't set Data");
#endif
        return AxixFalse;
    }
    return AxixTrue;
}

AxixBool CarbonXAxiTransClass::setDataWaits(CarbonUInt32 dataWaitSize, 
        const CarbonUInt32 *dataWaits)
{
    _dataWaitSize = dataWaitSize;
    _dataWaits = copyData(_dataWaitSize, dataWaits);
    if (_dataWaits == NULL)
    {
        _dataWaitSize = 0;
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't set Data Waits");
#endif
        return AxixFalse;
    }
    return AxixTrue;
}

/* 
   This routine calculates the data size in bytes to transfer in a burst
   transaction depending on the type, size, length and start address of the
   burst.
*/
CarbonUInt32 CarbonXAxiTransClass::getActualDataSize() const
{   
    CarbonUInt32 dataSize = 0;
    CarbonUInt32 transferSizeInBytes = 1 << _burstSize; // 2 ^ _burstSize
    CarbonUInt32 burstLength = _burstLength + 1; // convert from encoded value
    UInt firstTransferSize = transferSizeInBytes -
        static_cast<UInt>(_startAddress) % transferSizeInBytes;
    if (_burstType == CarbonXAxiBurst_FIXED)
    {
        dataSize = firstTransferSize * burstLength;
    }
    else // for incremental or wrapping burst
    {
        dataSize = firstTransferSize + transferSizeInBytes * (burstLength - 1);
    }
    return dataSize;
}

/* 
   This routine calculates the byte lanes of transfer within a burst.
   Formulas are taken from Section 4.5 of AMBA AXI Protocol 
   - v1.0 specification.
*/  
void CarbonXAxiTransClass::getByteLanes(CarbonUInt32 dataBusWidth,
        UInt &lowerByteLane, UInt &upperByteLane) const
{
    // The number of byte lanes in the data transfer
    const UInt dataBusByte = (dataBusWidth / 8);

    // The alligned version of the start address
    const UInt transferSizeInBytes = 1 << _burstSize; // 2 ^ _burstSize
    const UInt allignedAddress =
        static_cast<UInt>(_startAddress/transferSizeInBytes) *
        transferSizeInBytes;
    
    // The address of transfer N within a burst; N is a UInteger from 2-16
    UInt addressN = allignedAddress + _transferIndex * transferSizeInBytes;

    // For the first transfer in a burst or if the transaction is fixed 
    // burst type
    if (_transferIndex == 0 || _burstType == CarbonXAxiBurst_FIXED)
    {
        lowerByteLane = static_cast<UInt>(_startAddress) -
            static_cast<UInt>(_startAddress/dataBusByte) * dataBusByte;
        upperByteLane = allignedAddress + (transferSizeInBytes - 1) -
            static_cast<UInt>(_startAddress/dataBusByte) * dataBusByte;
    }
    else // for incremental or wrapping burst
    {
        lowerByteLane = addressN - addressN/dataBusByte * dataBusByte;
        upperByteLane = lowerByteLane + transferSizeInBytes - 1;
    }
}

CarbonUInt32 CarbonXAxiTransClass::getResponseWait(AxixBool update)
{
    if (update && !getWaitIndefinite() && _responseWait > 0)
        _responseWait--;
    return _responseWait;
}

CarbonUInt32 CarbonXAxiTransClass::getTransferWait(AxixBool update)
{
    if (_dataWaitSize == 0 || _dataWaits == NULL || // if no waits
            _transferIndex >= _dataWaitSize) // no wait for specified index
        return 0; // No wait
    CarbonUInt32 returnWait = _dataWaits[_transferIndex];
    if (update && _dataWaits[_transferIndex] > 0)
        _dataWaits[_transferIndex]--; // Decrement wait
    return returnWait;
}

AxixBool CarbonXAxiTransClass::resolveDefaults(const AxiXtor *xtor)
{
    ASSERT(xtor, xtor);
    // Resolve default burst size - make it same to data bus width
    if (_burstSize == CarbonXAxiBurstSize_DEFAULT)
    {
        UInt dataBusWidthInBytes = xtor->getConfig()->getDataBusWidth() / 16;
        UInt burstSize = 0;
        while (dataBusWidthInBytes > 0)
        {
            burstSize++;
            dataBusWidthInBytes = dataBusWidthInBytes >> 1;
        }
        _burstSize = static_cast<CarbonXAxiBurstSize>(burstSize);
    }
    // Resolve default burst length - calculate from burst size and data size
    if (_noBurstLengthSet)
    {
    _noBurstLengthSet = AxixFalse;

        if (getType() == CarbonXAxiTrans_READ)
        {
            AxiXtor::error(xtor, CarbonXAxiError_InvalidTrans,
                    "Invalid transaction - "
                    "no burst length set for READ transaction.", this);
            return AxixFalse;
        }

        CarbonUInt32 transferSizeInBytes = 1 << _burstSize; // 2 ^ _burstSize
        UInt firstTransferSize = transferSizeInBytes -
            static_cast<UInt>(_startAddress) % transferSizeInBytes;
        if (_dataSize < firstTransferSize)
        {
            if (_useStrobeForLessData && _dataSize > 0 && _data != NULL)
            {
                _burstLength = 0; // take minimal burst size
                if (_burstType == CarbonXAxiBurst_WRAP)
                    _burstLength = 1; // for wrapping burst set length to 2
            }
            else
            {
                AxixString buffer = AxixGetStringBuffer();
                sprintf(buffer, "Invalid transaction - "
                        "less number (%u) of data provided.", _dataSize);
                AxiXtor::error(xtor, CarbonXAxiError_InvalidTrans,
                        buffer, this);
                return AxixFalse;
            }
        }
        else
        {
            UInt length; // no upper limit
            if (_burstType == CarbonXAxiBurst_FIXED)
            {
                length = (_dataSize / firstTransferSize) - 1 +
                    (_useStrobeForLessData ?
                     (_dataSize % firstTransferSize != 0) : 0);
            }
            else // for incremental or wrapping burst
            {
                length = (_dataSize - firstTransferSize) /
                    transferSizeInBytes + (_useStrobeForLessData ?
                    ((_dataSize - firstTransferSize) % transferSizeInBytes != 0)
                    : 0);
            }
            // Limit the max value
            if (_burstType == CarbonXAxiBurst_WRAP) // for wrapping burst
            {
                UInt burstLength = 2;
                while (burstLength < length + 1)
                    burstLength *= 2;
                _burstLength = burstLength - 1;
            }
            else
                _burstLength = (length < 16) ? length : 15;
        }
    }
    return AxixTrue;
}

AxixBool CarbonXAxiTransClass::checkValidity(const AxiXtor *xtor) const
{
    ASSERT(xtor, xtor);
    // Burst size is greater than data bus width
    if ((1 << _burstSize) * 8 > xtor->getConfig()->getDataBusWidth())
    {
        AxixString buffer = AxixGetStringBuffer();
        sprintf(buffer, "Invalid transaction - "
                "Burst size %u bytes is greater than data bus width %u bits.",
                1 << _burstSize, xtor->getConfig()->getDataBusWidth());
        xtor->error(xtor, CarbonXAxiError_InvalidTrans, buffer, this);
        return AxixFalse;
    }
    // For wrapping burst type, burst length is outside its permissible values
    // (2, 4, 8 and 16).
    if ((_burstType == CarbonXAxiBurst_WRAP) &&
        (_burstLength != 1 && _burstLength != 3 &&
         _burstLength != 7 && _burstLength != 15))
    {
        AxixString buffer = AxixGetStringBuffer();
        sprintf(buffer, "Invalid transaction - "
                "For wrapping burst, burst length (%u) is outside its "
                "permissible values (2, 4, 8 and 16).", _burstLength + 1);
        xtor->error(xtor, CarbonXAxiError_InvalidTrans, buffer, this);
        return AxixFalse;
    }
    // For wrapping burst type, the start address is not aligned to burst size.
    if ((_burstType == CarbonXAxiBurst_WRAP) &&
        (_startAddress % (1 << _burstSize)))
    {
        AxixString buffer = AxixGetStringBuffer();
        sprintf(buffer, "Invalid transaction - "
                "For wrapping burst, the start address ("Format64(u)") is not "
                "aligned to burst size (%u bytes).", _startAddress,
                 1 << _burstSize);
        xtor->error(xtor, CarbonXAxiError_InvalidTrans, buffer, this);
        return AxixFalse;
    }
    return AxixTrue;
}

void CarbonXAxiTransClass::print() const
{
    AxixConstString transType =
        (getType() == CarbonXAxiTrans_WRITE) ?  "Write" : "Read";
    printf("AXI %s Transaction %u\n", transType, _id);
    printf("ID            = %u\n", _id);
    printf("Start Address = " Format64(u) "\n", _startAddress);
    AxixConstString burstType = NULL;
    switch (_burstType)
    {
        case CarbonXAxiBurst_FIXED: burstType = "Fixed"; break;
        case CarbonXAxiBurst_INCR: burstType = "Incremental"; break;
        case CarbonXAxiBurst_WRAP: burstType = "Wrapping"; break;
        default: ASSERT(0, NULL);
    }
    printf("Burst Type    = %s Burst\n", burstType);
    printf("Burst Size    = %u Bytes\n", 1 << _burstSize);
    printf("Burst Length  = %u\n", _burstLength + 1);
    AxixConstString lockType = NULL;
    switch (_lock)
    {
        case CarbonXAxiAccess_NORMAL:    lockType = "Normal"; break;
        case CarbonXAxiAccess_EXCLUSIVE: lockType = "Exclusive"; break;
        case CarbonXAxiAccess_LOCKED:    lockType = "Locked"; break;
        default: ASSERT(0, NULL);
    }
    printf("Lock          = %s Access\n", lockType);
    printf("Cache         = %u\n", _cache);
    printf("Protection    = %u\n", _prot);
}

CarbonUInt8* CarbonXAxiTransClass::copyData(CarbonUInt32 size,
        const CarbonUInt8 *data)
{
    if (size == 0 || data == NULL)
        return NULL;

    CarbonUInt8 *newData = NEWARRAY(CarbonUInt8, size);
    if (newData == NULL)
    {
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't copy Data Array");
#endif
        return NULL;
    }

    for (UInt i = 0; i < size; i++)
        newData[i] = data[i];
    return newData;
}

CarbonUInt32* CarbonXAxiTransClass::copyData(CarbonUInt32 size,
        const CarbonUInt32 *data)
{
    if (size == 0 || data == NULL)
        return NULL;

    CarbonUInt32 *newData = NEWARRAY(CarbonUInt32, size);
    if (newData == NULL)
    {
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't copy Data Array");
#endif
        return NULL;
    }

    for (UInt i = 0; i < size; i++)
        newData[i] = data[i];
    return newData;
}
