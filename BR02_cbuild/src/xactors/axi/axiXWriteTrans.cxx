/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXiWriteTrans.cpp 
    Purpose: This file contains the definition of the function for 
             write transaction class.
*/

#include "axiXtor.h"
#include "axiXWriteTrans.h"

AxixWriteTrans::AxixWriteTrans(CarbonUInt32 id):
    CarbonXAxiTransClass(id),
    _strobeSize(0), _strobe(NULL),
    _response(CarbonXAxiResponse_OKAY)
{}

AxixWriteTrans::~AxixWriteTrans()
{
    if (_strobe != NULL)
        DELETE(_strobe);
}

// Creates data and strobe array for incoming write transaction
AxixBool AxixWriteTrans::createData()
{
    if (!CarbonXAxiTransClass::createData())
    {    
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't allocate memory for Data Array for Write Transaction", this);
#endif
        return AxixFalse;
    }
    _strobeSize = getActualDataSize();
    _strobe = NEWARRAY(CarbonUInt1, _strobeSize);
    if (_strobe == NULL)
    {
        _strobeSize = 0;
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't allocate memory for Strobe Array", this);
#endif
        return AxixFalse;
    }
    return AxixTrue;
}

// Sets strobe by copying
AxixBool AxixWriteTrans::setStrobe(CarbonUInt32 strobeSize, 
        const CarbonUInt1 *strobe)
{
    _strobeSize = strobeSize;
    _strobe = copyData(_strobeSize, strobe);
    if (_strobe == NULL)
    {
        _strobeSize = 0;
        return AxixFalse;
    }
    return AxixTrue;
}

AxixBool AxixWriteTrans::checkValidity(const AxiXtor *xtor) const
{
    if (!CarbonXAxiTransClass::checkValidity(xtor))
        return AxixFalse;
    return AxixTrue;
}

// Debug
void AxixWriteTrans::print() const
{
    CarbonXAxiTransClass::print();
    CarbonUInt32 dataSize = getDataSize();
    const CarbonUInt8 *data = getData();
    printf("Data Size     = %u\n", dataSize);
    for (UInt i = 0; i < dataSize; i++)
    {
        printf("%5u: Data   = %3u, Strobe = %u\n", i, data[i],
                (i < _strobeSize) ? _strobe[i] : 1);
    }
    AxixConstString resp = NULL;
    switch (_response)
    {
        case CarbonXAxiResponse_OKAY:   resp = "OKAY"; break;
        case CarbonXAxiResponse_EXOKAY: resp = "EXOKAY"; break;
        case CarbonXAxiResponse_SLVERR: resp = "SLVERR"; break;
        case CarbonXAxiResponse_DECERR: resp = "DECERR"; break;
        default: ASSERT(0, NULL);
    }
    printf("Response      = %s\n", resp);
}
