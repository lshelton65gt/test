/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_DEFINES_H
#define AXIX_DEFINES_H

#include <stdio.h>
#include <string.h>

/*!
 * \file axiXDefines.h
 * \brief Header file for project specific type definitions.
 *
 * This file provides the type definitions to be used globally in the project.
 * Define all local types in this file.
 */

/* Unified type definition for integer and unsigned integer */
/*! \brief Type for integer */
typedef int           Int;
/*! \brief Type of unsigned integer */
typedef unsigned int  UInt;

/* Macros for memory management */
#include "carbon/c_memmanager.h"
/*! \brief Checks if memory is available
 *
 * This routine is provided for emulation of no memory case.
 * \param size Memory need to be allocated
 * \retval 1 If memory can be allocated.
 * \retval 0 Otherwise
 */
UInt AxixIsMemAvail(size_t size);
/*! \brief Macro to allocate memory */
#define NEW(type) static_cast<type*>(AxixIsMemAvail(sizeof(type))?carbonmem_malloc(sizeof(type)):NULL)
/*! \brief Macro to allocate memory for array */
#define NEWARRAY(type, size) static_cast<type*>(AxixIsMemAvail(sizeof(type)*(size))?carbonmem_malloc(sizeof(type)*(size)):NULL)
/*! \brief Macro to free memory */
#define DELETE(ptr) carbonmem_free(const_cast<void*>(static_cast<const void*>(ptr)))

/* Macros for debug purpose */
/*! \brief Macro to print a variable value */
#define PRINT(var) printf(#var" = %u\n",var)

/*! Printf format modifier for 64 bit arguments vs. 32 bit args
 * Usage:: // Print a 64 bit variable
 * printf ("value is: " Format64(u) "\n", var64bit); */
/* Note: This code is copied from inc/util/OSWrapper.h. */
#if pfLP64
#  define Format64(x) "%l" #x
#  define Formatw64(w,x) "%" #w "l" #x
#  define FormatSize_t "%lu"
#else
#  if pfWINDOWS || pfMSVC
#    define Format64(x) "%I64" #x
#    define Formatw64(w,x) "%" #w "I64" #x
#  else
#    define Format64(x) "%ll" #x
#    define Formatw64(w,x) "%" #w "ll" #x
#  endif
#  define FormatSize_t "%u"
#endif

/* Data types */
/*! \brief String type */
typedef char*         AxixString;
/*! \brief Constant string type */
typedef const char*   AxixConstString;
/*! \brief Bool type */
typedef char          AxixBool;

#include "carbon/carbon_shelltypes.h"
/*! \brief Bit type */
typedef CarbonUInt1   AxixBit;

/* Definition of true and false
 * Note, macro 'DEF_BOOL_CONST' is defined only in axiLlXDefines.c so that
 * axiLlXDefines.cpp can have the definition while other files will use the
 * extern forward declaration. */
/* Declaration of true and false */
/*! \brief Definition of False */
extern const AxixBool AxixFalse;
/*! \brief Definition of True */
extern const AxixBool AxixTrue;
#ifdef DEF_BOOL_CONST
/*! \brief Definition of false */
const AxixBool AxixFalse = 0;
/*! \brief Definition of true */
const AxixBool AxixTrue  = 1;
#endif

/* Declaration for AxiXtor */
class CarbonXAxiClass;
class CarbonXAxiTransClass;
/*! \brief Type for Carbon AXI Transactor */
typedef class CarbonXAxiClass AxiXtor;
typedef class CarbonXAxiTransClass AxixTrans;

/*! \brief Function to string duplication */
AxixString AxixStrdup(AxixConstString str);
/*! \brief Function to get a large buffer for string message creation */
AxixString AxixGetStringBuffer();
/*! \brief Assert function specific to this project */
UInt AxixAssertFail(const AxiXtor *xtor, AxixConstString failExpr,
        AxixConstString file, UInt line);
/*! \brief Assert Macro */
#define ASSERT(x,y) ((x)?0:AxixAssertFail((y),#x,__FILE__,__LINE__))

#endif
