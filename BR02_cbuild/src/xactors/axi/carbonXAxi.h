// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbonXAxi_h_
#define __carbonXAxi_h_

#ifndef __carbonXAxiConfig_h_
#include "xactors/axi/carbonXAxiConfig.h"
#endif

#ifndef __carbonXAxiTrans_h_
#include "xactors/axi/carbonXAxiTrans.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \defgroup Axi AXI C-Interface
 * \brief C-Linkable Interface to the AXI Transactor
 */

/*!
 * \addtogroup Axi
 * @{
 */

/*!
 * \file xactors/axi/carbonXAxi.h
 * \brief Definition of Carbon AMBA AXI Transactor Object.
 *
 * This file provides definition of AXI transactor object and related APIs.
 *
 * Transactor update semantics: Following sequence of operation will be
 * followed to run the transactor for each clock cycle
 *    -# Evaluate all transactors and the model. This should call
 *       carbonXAxiEvaluate().
 *    -# Call carbonXAxiRefreshInputs() and carbonXAxiRefreshOutputs()
 *       for every transactor to update the input and output connections
 *       between the model and the transactors.
 */

/*!
 * \brief Carbon AMBA AXI Transactor Class */
STRUCT CarbonXAxiClass;

/*!
 * \brief Carbon AXI Transactor Handle
 *
 * This type provides a handle or pointer to transactor object. All transactor
 * APIs need this handle to be passed as its argument.
 */
typedef STRUCT CarbonXAxiClass* CarbonXAxiID;

/*!
 * \brief Carbon AXI transactor type
 *
 * This type specifies the transctor type i.e. master or slave.
 */
typedef enum {
    CarbonXAxi_Master = 0,              /*!< Master transactor */
    CarbonXAxi_Slave = 1                /*!< Slave  transactor */
} CarbonXAxiType;


/*!
 * \brief Carbon AXI transactor error notification type
 *
 * This type provides the error type that can be signaled by the transactor
 * for any unmanagable case, that it might see.
 */
typedef enum {
    CarbonXAxiError_NoError = 0,        /*!< No error */
    CarbonXAxiError_NoMemory,           /*!< Memory allocation error */
    CarbonXAxiError_ConnectionError,    /*!< Pin connection error */
    CarbonXAxiError_ProtocolError,      /*!< Protocol assumption failed */
    CarbonXAxiError_InvalidTrans,       /*!< Invalid transaction encountered */
    CarbonXAxiError_InternalError       /*!< Internal error in code */
} CarbonXAxiErrorType;

/*!
 * \brief Creates a transactor Transactor Object
 *
 * \param transactorName     Instance name of this transactor object.
 *
 * \param xtorType           Specifies transactor type i.e. master or slave.
 *
 * \param config             Specifies transactor configuration parameters.
 *                           User need to populate this beforehand.
 *                           Transactor will copy the configuration object.
 *                           If config is NULL, default values of configuration
 *                           parameters will be used.
 *
 * \param maxTransfers       Maximum number of simultaneous transactions that
 *                           can be pushed. This actually sets the size of two
 *                           transaction queues of the master transactor - one
 *                           for READ and other for WRITE transactions.
 *                           Note, this also specifies the read readordering
 *                           and write interleaving depth of the transactors.
 *                           Its minimum value is 1, which is set if zero is
 *                           passed as argument.
 *
 * \param reportTransaction  Callback to indicate completion of transaction to
 *                           master transactor. For write transaction, user
 *                           need to retrieve response and for read transaction,
 *                           user will get read data at this point. Irrelevant
 *                           for slave.
 *
 * \param setDefaultResponse Callback function used by transactors to set
 *                           the default conditions of a transaction. This
 *                           indicates slave, when an incoming transaction is
 *                           being received i.e. address cycle is complete.
 *                           Slave needs to provide the data wait states here.
 *                           Irrelevant for master.
 *
 * \param setResponse        Callback function used by transactors to set the
 *                           response to be provided to a transction, i.e. when
 *                           a transaction need to be responded by the slave.
 *                           User need to retrieve the data for write 
 *                           transaction and set the response back. For read 
 *                           transaction, this callback will be called to 
 *                           provide all data and response for the entire 
 *                           burst. Irrelevant for master.
 *
 * \param refreshResponse    This callback can be used to revise return values
 *                           or latency. It is not presently used in the AXI
 *                           transactors.
 *
 * \param notify             Callback function used to report a transactor
 *                           fault that is associated with a particular
 *                           transction.
 *
 * \param notify2            Callback function used to report a transactor
 *                           fault that cannot be associated with a
 *                           particular transction.
 *
 * \param stimulusInstance   Provided as a convenience to the user. It is
 *                           stored as a void pointer by the transactor
 *                           and returned as a parameter of the callback
 *                           functions to help identify the generator
 *                           instance that should service the callback
 *                           where more than one interface is in use.
 *
 * \returns Created transactor handle
 */

CarbonXAxiID carbonXAxiCreate(const char* transactorName,
        CarbonXAxiType xtorType, const CarbonXAxiConfig *config,
        CarbonUInt32 maxTransfers,
        void (*reportTransaction)(CarbonXAxiTrans*, void*),
        void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
        void (*setResponse)(CarbonXAxiTrans*, void*),
        void (*refreshResponse)(CarbonXAxiTrans*, void*),
        void (*notify)(CarbonXAxiErrorType, const char*,
            const CarbonXAxiTrans*, void*),
        void (*notify2)(CarbonXAxiErrorType, const char*, void*),
        void *stimulusInstance);

/*!
 * \brief Destroys a transactor object.
 *
 * \param xtor Transactor object.
 *
 * \retval 1 For successful destruction.
 * \retval 0 For failure in destruction.
 */
CarbonUInt32 carbonXAxiDestroy(CarbonXAxiID xtor);

/*!
 * \brief Gets transactor name.
 *
 * \param xtor Transactor object.
 *
 * \returns Name of the transactor.
 */
const char *carbonXAxiGetName(CarbonXAxiID xtor);

/*!
 * \brief Gets transactor type i.e. master or slave.
 *
 * \param xtor Transactor object.
 *
 * \returns Type of the transactor.
 */
CarbonXAxiType carbonXAxiGetType(CarbonXAxiID xtor);

/*!
 * \brief Gets the configuration object of the transactor.
 *
 * This API will copy the configuration parameters from transactor to the
 * configuration object passed.
 *
 * \param xtor Transactor object.
 * \param config Configuration object to be updated.
 */
void carbonXAxiGetConfig(CarbonXAxiID xtor, CarbonXAxiConfig *config);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by name.
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName   Carbon Model name.
 * \param carbonModelHandle Carbon Model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXAxiConnectByModelSignalName(CarbonXAxiID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by
 *        CarbonNetID.
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName   Carbon Model name.
 * \param carbonModelHandle Carbon Model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXAxiConnectByModelSignalHandle(CarbonXAxiID xtor,
        CarbonXInterconnectNameHandlePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by
 *        input/output change callbacks.
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXAxiConnectToLocalSignalMethods(
        CarbonXAxiID xtor, CarbonXInterconnectNameCallbackTrio *nameList);

/*!
 * \brief Checks if transactor can accept additional transactions.
 * 
 * This API will check the space availability of transaction queues. Transactor
 * maintains two separate queues - one for WRITE and other for READ
 * transactions. So this API will take the type of transaction also as an
 * argument , that user want to push.
 *
 * \param xtor Transactor object.
 * \param transType Transaction type to be pushed.
 *
 * \retval 1 If transactor can accept additional transactions.
 * \retval 0 Otherwise.
*/
CarbonUInt32 carbonXAxiIsSpaceInTransactionBuffer(CarbonXAxiID xtor,
        CarbonXAxiTransType transType);

/*!
 * \brief Starts a new transaction
 *
 * This API will push a created transaction into appropriate transaction queue
 * of the transactor.
 *
 * \param xtor Transactor object.
 * \param trans Transaction to be pushed in the transaction queue.
 *
 * \retval 1 If the transaction is successfully added to the input buffer.
 * \retval 0 Otherwise.
 */
CarbonUInt32 carbonXAxiStartNewTransaction(CarbonXAxiID xtor, 
        CarbonXAxiTrans *trans);

/*!
 * \brief Evaluates transactor
 * 
 * This API evaluates the transactor and updates all the functionality
 * for a clock. Note, in this API call transactor state is updated
 * but i/o of the transactor are not changed.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which evaluation is happening.
 *
 * \retval 1 For successful evaluation.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXAxiEvaluate(CarbonXAxiID xtor, CarbonTime currentTime);

/*!
 * \brief Refreshes transactor input signals
 *
 * In this API, all input signal ports of the transactor
 * are updated but transactor state remains unchanged. Thus at this point
 * transactor will communicate with the model for reading signal values.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which refreshing is happening.
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXAxiRefreshInputs(CarbonXAxiID xtor, 
        CarbonTime currentTime);

/*!
 * \brief Refreshes transactor output signals.
 *
 * In this API, all output signal ports of the transactor
 * are updated but transactor state remains unchanged. Thus at this point
 * transactor will communicate with the model for writing signal values.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which refreshing is happening.
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXAxiRefreshOutputs(CarbonXAxiID xtor,
        CarbonTime currentTime);

/*!
 * \brief Dumps the state of transactor
 *
 * This routine dumps the transactor's internal state in console.
 * One can use this routine for debug purpose.
 *
 * \param xtor Transactor object.
 */
void carbonXAxiDump(CarbonXAxiID xtor);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif

