/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_CHANNELINTF_H
#define AXIX_CHANNELINTF_H

#include "axiXPin.h"

/*!
 * \file axiXChannel.h
 * \brief Header file for channel interface definition.
 *
 * This file provides the base class definition channel interface.
 */

/*!
 * \brief Base class for transactor's channel interface
 *
 * This includes VALID and READY pins and provides base class for all
 * 5 channel interfaces.
 */
class AxixChannel
{
public: CARBONMEM_OVERRIDES

protected:

    const AxiXtor *_xtor;                 //!< Transactor reference
    const AxixDirection _channelDir;      //!< Channel direction

public:

    AxixPin *_idPin;                      //!< ARID pin
    AxixPin *_validPin;                   //!< Valid pin
    AxixPin *_readyPin;                   //!< Ready pin

public:

    // Constructor and destructor
    // channelName should be one of { AW, W, B, AR, R }
    AxixChannel(AxiXtor *xtor, AxixConstString channelName,
            const AxixDirection channelDir, const CarbonUInt32 idBusWidth);
    virtual ~AxixChannel();

    // Pin access
    AxixPin *getIdPin() { return _idPin; }
    AxixPin *getValidPin() { return _validPin; }
    AxixPin *getReadyPin() { return _readyPin; }
    
    // Refresh
    AxixBool refreshInputs(CarbonTime currentTime);
    AxixBool refreshOutputs(CarbonTime currentTime);
    // Refresh routine to be implemented by derived channel interface classes
    // which will just call refresh on each pins
    virtual AxixBool refresh(CarbonTime currentTime) = 0;
};

#endif
