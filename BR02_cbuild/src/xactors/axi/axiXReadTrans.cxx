/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXiReadTrans.cpp 
    Purpose: This file contains the definition of the function for 
             read transaction class.
*/

#include "axiXtor.h"
#include "axiXReadTrans.h"

AxixReadTrans::AxixReadTrans(CarbonUInt32 id):
    CarbonXAxiTransClass(id),
    _responseSize(0), _response(NULL)
{}

AxixReadTrans::~AxixReadTrans()
{
    if (_response != NULL)
        DELETE(_response);
}

// Creates data and response array for incomminbg read transaction
AxixBool AxixReadTrans::createData()
{
    if (!CarbonXAxiTransClass::createData())
    {
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't allocate memory for Data for Read Transaction", this);
#endif
        return AxixFalse;
    }
    _responseSize = (getBurstLength() + 1);
    _response = NEWARRAY(CarbonXAxiResponse, _responseSize);
    if (_response == NULL)
    {
        _responseSize = 0;
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't allocate memory for Response Array", this);
#endif
        return AxixFalse;
    }
    return AxixTrue;
}

// Copies response
AxixBool AxixReadTrans::setResponse(CarbonUInt32 responseSize, 
        const CarbonXAxiResponse *response)
{
    if (responseSize == 0 || response == NULL)
        return AxixTrue;
    
    _responseSize = responseSize;
    _response = NEWARRAY(CarbonXAxiResponse, _responseSize);
    if (_response == NULL)
    {
        _responseSize = 0;
#if TRANS_NOMEMORY_ERROR
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't copy Response Array", this);
#endif
        return AxixFalse;
    }

    for (UInt i = 0; i < _responseSize; i++)
        _response[i] = response[i];
    return AxixTrue;    
}

AxixBool AxixReadTrans::checkValidity(const AxiXtor *xtor) const
{
    if (!CarbonXAxiTransClass::checkValidity(xtor))
        return AxixFalse;
    return AxixTrue;
}

void AxixReadTrans::print() const
{
    CarbonXAxiTransClass::print();
    CarbonUInt32 dataSize = getDataSize();
    const CarbonUInt8 *data = getData();
    printf("Data Size     = %u\n", dataSize);
    CarbonUInt32 transferSizeInBytes = 1 << getBurstSize();
    UInt firstTransferSize = transferSizeInBytes -
        static_cast<UInt>(getStartAddress()) % transferSizeInBytes;
    for (UInt i = 0, j = 0; i < dataSize; i++)
    {
        printf("%5u: Data   = %3u", i, data[i]);

        // Logic to print response per burst
        AxixBool isLastByteInBurst = AxixFalse;
        if (getBurstType() == CarbonXAxiBurst_FIXED || i < firstTransferSize)
            isLastByteInBurst = (((i + 1) % firstTransferSize) == 0);
        else
            isLastByteInBurst = (((i - firstTransferSize + 1) %
                    transferSizeInBytes) == 0);
        if (isLastByteInBurst)
        {
            AxixConstString resp = "OKAY";
            if (j < _responseSize)
            {
                switch (_response[j])
                {
                    case CarbonXAxiResponse_OKAY:   resp = "OKAY";   break;
                    case CarbonXAxiResponse_EXOKAY: resp = "EXOKAY"; break;
                    case CarbonXAxiResponse_SLVERR: resp = "SLVERR"; break;
                    case CarbonXAxiResponse_DECERR: resp = "DECERR"; break;
                    default: ASSERT(0, NULL);
                }
                j++;
            }
            printf(", Response = %s\n", resp);
        }
        else
            printf("\n");
    }
}
