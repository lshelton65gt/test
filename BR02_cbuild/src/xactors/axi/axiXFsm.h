/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_FSM_H
#define AXIX_FSM_H

#include "axiXTransTable.h"

/*!
 * \file axiXFsm.h
 * \brief Header file for FSM base class.
 */

#define DEBUG 0

/*!
 * \brief Class to represent FSM base class
 */
class AxixFsm
{
public: CARBONMEM_OVERRIDES

protected:
    
    AxiXtor * const _xtor;                //!< Transactor reference

public:

    // Constructor and destructor
    AxixFsm(AxiXtor *xtor): _xtor(xtor) { }
    virtual ~AxixFsm() {}

    //! Starts a new transaction
    virtual AxixBool canAcceptNewTrans() const
    { ASSERT(0, _xtor); return AxixFalse; }
    virtual AxixBool startNewTransaction(CarbonXAxiTrans *trans)
    { ASSERT(0, _xtor); ASSERT(trans, _xtor); return AxixFalse; }

    // Evaluation routine
    virtual AxixBool update(CarbonTime currentTime) = 0;
    // Print routine
    virtual void print() const = 0;
};

#endif
