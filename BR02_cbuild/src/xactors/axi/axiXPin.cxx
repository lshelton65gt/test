/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXPin.cpp
    Purpose: This file provides the member function definition for pins. 
*/

#include "axiXPin.h"
#include "axiXtor.h"

#define ENABLE_MONITOR 0

// Constructor
AxixPin::AxixPin(AxiXtor *xtor, AxixConstString name,
        const AxixDirection direction, const CarbonUInt32 size):
    _xtor(xtor), _name(AxixStrdup(name)), _direction(direction), _size(size),
    _size32(_size / 32 + (_size % 32 != 0)),
    _currValue(NEWARRAY(CarbonUInt32, _size32)),
    _module(NULL), _net(NULL),
    _changeCallback(NULL), _callbackContext(NULL)
{
    // Check for memory allocation error
    if (_name == NULL)
    {
        xtor->errorForNoMemory(name);
        return;
    }
    if (_currValue == NULL)
    {
        xtor->errorForNoMemory(name);
        return;
    }

    // Initializes current values
    for (UInt i = 0; i < _size32; i++)
        _currValue[i] = 0;
}

AxixPin::~AxixPin()
{
    DELETE(_name);
    DELETE(_currValue);
}

// Bind functions
AxixBool AxixPin::bind(CarbonObjectID *module, CarbonNetID *net)
{
    _module = module;
    _net = net;
    return AxixTrue;
}
 
AxixBool AxixPin::bind(CarbonXSignalChangeCB changeCallback,
                                void *callbackContext)
{
    _changeCallback = changeCallback;
    _callbackContext = callbackContext;
    return AxixTrue;
}

// Scalar write
AxixBool AxixPin::write(const CarbonUInt32 data, CarbonTime currentTime)
{
    ASSERT(_size <= 32, _xtor);
    *_currValue = data & (static_cast<UInt>(-1) >> (32 - _size));
    return AxixTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

// Vector write
AxixBool AxixPin::write(const CarbonUInt32 *data, CarbonTime currentTime)
{
    for (UInt i = 0; i < _size32; i++)
        _currValue[i] = data[i];
    return AxixTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

// Scalar read - short form
CarbonUInt32 AxixPin::read(CarbonTime currentTime) const
{
    CarbonUInt32 data;
    ASSERT(read(data, currentTime), _xtor);
    return data;
}

// Scalar read
AxixBool AxixPin::read(CarbonUInt32 &data, CarbonTime currentTime) const
{
    ASSERT(_size <= 32, _xtor);
    data = *_currValue & (static_cast<UInt>(-1) >> (32 - _size));
    return AxixTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

// Vector read
AxixBool AxixPin::read(CarbonUInt32 *data, CarbonTime currentTime) const
{
    for (UInt i = 0; i < _size32; i++)
        data[i] = _currValue[i];
    return AxixTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

// Refresh function to transfer data between transactor pin and Carbon Model pin
AxixBool AxixPin::refresh(CarbonTime currentTime)
{
    AxixBool retValue = AxixFalse;
    if (_direction == AxixDirection_Input) // Read
    {
        if (_module && _net) // Use Carbon Model functions
        {
            if (carbonExamine(_module, _net, _currValue, NULL) == eCarbon_OK)
                retValue = AxixTrue;
        }
        else if (_changeCallback) // Use user callbacks
        {
            if ((*_changeCallback)(_currValue, _callbackContext) == eCarbon_OK)
                retValue = AxixTrue;
        }
#if ENABLE_MONITOR
        printf("# READING: %s.", _xtor->getName()); print(currentTime);
#endif
    }
    else if (_direction == AxixDirection_Output) // Write
    {
        if (_module && _net) // Use Carbon Model functions
        {
            if (carbonDeposit(_module, _net, _currValue, NULL) == eCarbon_OK)
                return AxixFalse;
        }
        else if (_changeCallback) // Use user callbacks
        {
            if ((*_changeCallback)(_currValue, _callbackContext) == eCarbon_OK)
                retValue = AxixTrue;
        }
#if ENABLE_MONITOR
        printf("# WRITING: %s.", _xtor->getName()); print(currentTime);
#endif
    }
    if (!retValue)
    {
        // Error in connection
        AxixString buffer = AxixGetStringBuffer();
        sprintf(buffer, "Connection error "
                "during refresh for '%s' pin at time %u.", _name,
                static_cast<UInt>(currentTime));
        AxiXtor::error(_xtor, CarbonXAxiError_ConnectionError, buffer);
    }
    return retValue;
}

/* This routine returns a bit from the index of a specified register */
AxixBit AxixPin::getBit(CarbonUInt32 reg, UInt index)
{
    CarbonUInt32 mask = 0x1 << index;
    return (reg & mask) >> index;
}

/* This routines prints the pin value */
void AxixPin::printData() const
{
    for (Int i = _size - 1; i >= 0; i--) /* Don't make 'i' UInt */
    {
        printf("%u", getBit(_currValue[i / 32], i % 32));
        if (i % 8 == 0 && i > 0)
            printf(".");
    }
}

/* This routines prints the pin details */
void AxixPin::print(CarbonTime currentTime) const
{
    printf("%s[%u] = ", _name, static_cast<UInt>(currentTime));
    printData();
    printf("\n");
}

