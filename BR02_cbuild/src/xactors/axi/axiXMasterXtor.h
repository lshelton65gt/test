/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_MASTERXTOR_H
#define AXIX_MASTERXTOR_H

#include "axiXtor.h"

/*!
 * \file axiXMasterXtor.h
 * \brief Header file for master transactor definition
 */

/*!
 * \brief This class defines the master transactor.
 */
class AxixMasterXtor: public CarbonXAxiClass
{
public: CARBONMEM_OVERRIDES

private:

    //! Report transaction callback
    void (*_reportTransaction)(CarbonXAxiTrans*, void*);
          
public:

    //! Constructor
    AxixMasterXtor(AxixConstString name, const CarbonXAxiConfig *config,
            const CarbonUInt32 maxTransfers);
    //! Destructor
    ~AxixMasterXtor() {}
    
    //! This function returns transactor's type
    CarbonXAxiType getType() const
    { return CarbonXAxi_Master; }
    //! This function prints the current status of the transactor
    void print() const;

    //! Callback registation function
    void registerCallbacks(void *userContext,
            void (*reportTransaction)(CarbonXAxiTrans*, void*),
            void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
            void (*setResponse)(CarbonXAxiTrans*, void*),
            void (*errorTransCallback)(CarbonXAxiErrorType, const char*,
                const CarbonXAxiTrans*, void*),
            void (*errorCallback)(CarbonXAxiErrorType, const char*, void*));
    void reportTransaction(CarbonXAxiTrans *trans);

    //! Starts a new transaction
    AxixBool canAcceptNewTrans(CarbonXAxiTransType transType) const;
    AxixBool startNewTransaction(CarbonXAxiTrans *trans);
};

#endif
