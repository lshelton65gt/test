/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_ARCHANNELINTF_H
#define AXIX_ARCHANNELINTF_H

#include "axiXChannel.h"
#include "axiXReadTrans.h"

/*!
 * \file axiXARChannel.h
 * \brief Header file for read address (AR) channel interface definition.
 */

/*!
 * \brief Class for read address i.e. AR Channel
 *
 * This includes ARID, ARADDR, ARLEN, ARSIZE, ARBURST, ARLOCK, ARCACHE and
 * ARPROT pins. ARVALID and ARREADY pins are inherited from base class
 * AxixChannel.
 */
class AxixARChannel: public AxixChannel
{
public: CARBONMEM_OVERRIDES

public:

    AxixPin * const _addrPin;                 //!< ARADDR pin
    AxixPin * const _lenPin;                  //!< ARLEN pin
    AxixPin * const _sizePin;                 //!< ARSIZE pin
    AxixPin * const _burstPin;                //!< ARBURST pin
    AxixPin * const _lockPin;                 //!< ARLOCK pin
    AxixPin * const _cachePin;                //!< ARCACHE pin
    AxixPin * const _protPin;                 //!< ARPROT pin

public:

    // Constructor and destructor
    AxixARChannel(AxiXtor *xtor, const AxixDirection channelDir,
            const CarbonUInt32 idBusWidth,
            const CarbonXAxiAddressBusWidth addressBusWidth =
                CarbonXAxiAddressBusWidth_32);
    ~AxixARChannel();

    // Evaluation to transfer transaction to/from channel interface pins.
    AxixBool transfer(AxixReadTrans *trans, CarbonTime currentTime);

    // Refresh
    AxixBool refresh(CarbonTime currentTime);
};

#endif
