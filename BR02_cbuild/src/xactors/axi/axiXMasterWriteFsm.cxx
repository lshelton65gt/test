/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXMasterWriteFsm.cpp 
    Purpose: This file contains the definition of the functions for write 
             fsm for master axi transactor.
*/

#include "axiXMasterWriteFsm.h"
#include "axiXtor.h"

AxixMasterWriteFsm::AxixMasterWriteFsm(AxiXtor *xtor,
        const CarbonUInt32 maxTransfers):
    AxixFsm(xtor),
    _addressTransQueue(maxTransfers),
    _dataTransQueue(maxTransfers),
    _queueSpace(maxTransfers),
    _respTransTable(maxTransfers)
{}

AxixBool AxixMasterWriteFsm::canAcceptNewTrans() const
{
    // Note, transaction will be considered to be complete only after
    // response is obtained for that transaction. But, we can't stop FSM from
    // poping from the address and data queues. That's why extra queue size
    // tracking variable _queueSpace is introduced.
    if (_queueSpace == 0 || _addressTransQueue.isFull())
        return AxixFalse;
    return AxixTrue;
}

AxixBool AxixMasterWriteFsm::startNewTransaction(CarbonXAxiTrans *trans)
{
    ASSERT(trans, _xtor);
    if (!canAcceptNewTrans()) // if queue space is available
        return AxixFalse;
    if (_addressTransQueue.push(trans) && _dataTransQueue.push(trans))
    {
        _queueSpace--;
        return AxixTrue;
    }
    // Push error here can only happen if malloc fails, since we already have
    // checked for queue availability using canAcceptNewTrans()
    CarbonXAxiClass::error(_xtor, CarbonXAxiError_NoMemory,
            "Can't allocate memory for transaction in queue.");
    return AxixFalse;
}

// FSM
AxixBool AxixMasterWriteFsm::update(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;

    //**** Address logic ****
    {
    AxixBool valid = _xtor->getAWChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getAWChannel()->getReadyPin()->read(currentTime);
    // !valid : Bus is idle
    // valid && ready : Transfer is getting complete at slave side
    if (!valid || (valid && ready))
    {
        AxixBool nextValid = 0;
        // Decide for next transfer
        if (!_addressTransQueue.isEmpty()) // More transactions pending
        {
            // Transfer new address and control
            AxixWriteTrans *trans =
                static_cast<AxixWriteTrans*>(_addressTransQueue.pop());
            ASSERT(trans, _xtor);
            retValue &= _xtor->getAWChannel()->transfer(trans, currentTime);
            if (DEBUG)
                printf("# MASTER[%u]: ADDRESS transferred\n",
                        static_cast<UInt>(currentTime));
            // Making VALID high
            nextValid = 1;
        }
        else
            // No more transactions pending - so make VALID low
            nextValid = 0;
        _xtor->getAWChannel()->getValidPin()->write(nextValid, currentTime);
    }
    }

    // **** Data logic ****
    {
    AxixBool valid = _xtor->getWChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getWChannel()->getReadyPin()->read(currentTime);
    // !valid : Bus is idle
    // valid && ready : Transfer is getting complete at slave side
    if (!valid || (valid && ready))
    {
        AxixBool nextValid = 0;
        // Decide for next transfer
        if (!_dataTransQueue.isEmpty())
        {
            // Transfer new data
            AxixWriteTrans *trans =
                static_cast<AxixWriteTrans*>(_dataTransQueue.top());
            ASSERT(trans, _xtor);
            // Check for wait state, decrement wait if wait is present
            if (trans->getTransferWait(AxixTrue) == 0)
            {
                // Transfer when there is no more wait
                retValue &= _xtor->getWChannel()->transfer(trans, currentTime);
                if (DEBUG)
                    printf("# MASTER[%u]: DATA BEAT %u transferred\n",
                            static_cast<UInt>(currentTime),
                            trans->getTransferIndex() - 1);
                // For last data that has been transferred, pop from queue and
                // push into response table, where it will wait for response
                if (trans->getLast())
                {
                    if (_respTransTable.isFull())
                    {
                        // Internal code error
                        _xtor->error(_xtor, CarbonXAxiError_InternalError,
                                "INTERNAL ERROR - response transaction table "
                                " in master write FSM is full.");
                        return AxixFalse;
                    }
                    else if (!_respTransTable.push(_dataTransQueue.pop()))
                    {
                        // No memory error
                        _xtor->errorForNoMemory("Can't allocate memory for "
                               " transaction table.");
                        return AxixFalse;
                    }
                }
                nextValid = 1;
            }
        }
        _xtor->getWChannel()->getValidPin()->write(nextValid, currentTime);
    }
    }

    // **** Response logic *****
    {
    AxixBool valid = _xtor->getBChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getBChannel()->getReadyPin()->read(currentTime);
    // Transfer should happen at this clock
    if (valid && ready)
    {
        // Transfer new response
        CarbonUInt32 id = _xtor->getBChannel()->getIdPin()->read(currentTime);
        AxixWriteTrans *trans =
            static_cast<AxixWriteTrans*>(_respTransTable.pop(id));
        // If a response comes which has no transaction with same ID
        if (trans == NULL)
        {
            AxixString buffer = AxixGetStringBuffer();
            sprintf(buffer, "Protocol error - no such transaction pending "
                    "for incoming response with ID %u.", id);
            AxiXtor::error(_xtor, CarbonXAxiError_ProtocolError, buffer);
            return AxixFalse;
        }
        retValue &= _xtor->getBChannel()->transfer(trans, currentTime);
        if (DEBUG)
            printf("# MASTER[%u]: RESPONSE transferred\n",
                    static_cast<UInt>(currentTime));
        _xtor->reportTransaction(trans);
        // Increment transaction queue space allowing more transactions
        _queueSpace++;  
    }
    // Ready logic, should be always high
    _xtor->getBChannel()->getReadyPin()->write(1, currentTime);
    }

    return retValue;
}

void AxixMasterWriteFsm::print() const
{
    printf("Write transactions pending for:\n"
           "  Address transfer  = %u\n"
           "  Data transfer     = %u\n"
           "  Response transfer = %u\n",
           _addressTransQueue.getCount(),
           _dataTransQueue.getCount(),
           _respTransTable.getTransCount());
}
