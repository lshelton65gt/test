/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_SLAVEREADFSM_H
#define AXIX_SLAVEREADFSM_H

#include "axiXFsm.h"
#include "axiXTransQueue.h"
#include "axiXReadTrans.h"

/*!
 * \file axiXSlaveReadFsm.h
 * \brief Header file for slave read FSM class.
 */

/*!
 * \brief Class to represent slave read FSM
 *
 * This FSM will internally have two FSMs - address and data.
 * Updation of this FSM will update three states for these two internal FSMs
 * and access the channels accordingly. Channels will be updated using
 * transaction structure, while READY and VALID signals will be
 * accessed explicitly.
 */
class AxixSlaveReadFsm: public AxixFsm
{
public: CARBONMEM_OVERRIDES

private:
    
    UInt _dataWait;                          //!< Data wait. Stores the wait
                                             //   cycle for the current 
                                             //   transfer.

    AxixReadTrans *_scheduledTrans;          //!< Handle to the presently
                                             //   scheduled Transaction object

    // Internal Queues and tables
    AxixTransTable _dataTransTable;          //!< Transaction table for 
                                             //   data cycle. Transaction 
                                             //   is pushed into the table
                                             //   after receiving the address.
                                             //   Required data and response is
                                             //   obtained from the
                                             //   setResponse() callback.
                                             //   Then the transaction is sent 
                                             //   back to the master.
                                             //   transaction will be popped
                                             //   after getting the last signal.

public:

    // Constructor and destructor
    AxixSlaveReadFsm(AxiXtor *xtor, const CarbonUInt32 maxTransfers);
    ~AxixSlaveReadFsm() {}

    // Evaluation routine
    AxixBool update(CarbonTime currentTime);
    // Print routine
    void print() const;
};

#endif
