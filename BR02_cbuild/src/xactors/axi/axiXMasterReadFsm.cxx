/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXMasterReadFsm.cpp 
    Purpose: This file contains the definition of the functions for read 
             fsm for master axi transactor.
*/

#include "axiXMasterReadFsm.h"
#include "axiXtor.h"

AxixMasterReadFsm::AxixMasterReadFsm(AxiXtor *xtor,
        const CarbonUInt32 maxTransfers):
    AxixFsm(xtor),
    _addressTransQueue(maxTransfers),
    _queueSpace(maxTransfers),
    _dataWait(0), _dataWaitQueue(0), 
    _dataTransTable(maxTransfers)
{}

AxixMasterReadFsm::~AxixMasterReadFsm()
{
    _dataTransTable.deleteAll();
#if 0
    AxixTrans *trans;
    // deletes all pending transactions
    while ((trans = _addressTransQueue.pop()) != NULL)
        delete trans;
#endif
}

AxixBool AxixMasterReadFsm::canAcceptNewTrans() const
{
    // Note, transaction will be considered to be complete only after
    // response is obtained for that transaction. But, we can't stop FSM from
    // poping from the address queue. That's why extra queue size
    // tracking variable _queueSpace is introduced.
    if (_queueSpace == 0 || _addressTransQueue.isFull())
        return AxixFalse;
    return AxixTrue;
}

AxixBool AxixMasterReadFsm::startNewTransaction(CarbonXAxiTrans *trans)
{
    ASSERT(trans, _xtor);
    if (!canAcceptNewTrans()) // if queue space is available
        return AxixFalse;
    if (_addressTransQueue.push(trans))
    {
        _queueSpace--;
        return AxixTrue;
    }
    // Push error here can only happen if malloc fails, since we already have
    // checked for queue availability using canAcceptNewTrans()
    AxiXtor::error(_xtor, CarbonXAxiError_NoMemory,
            "Can't allocate memory for transaction in buffer.");
    return AxixFalse;
}

// FSM
AxixBool AxixMasterReadFsm::update(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;

    //**** Address logic ****
    {
    AxixBool valid = _xtor->getARChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getARChannel()->getReadyPin()->read(currentTime);
    // !valid : Bus is idle
    // valid && ready : Transfer is getting complete at slave side
    if (!valid || (valid && ready))
    {
        AxixBool nextValid = 0;
        // Decide for next transfer
        if (!_addressTransQueue.isEmpty()) // More transactions pending
        {
            // Transfer new address and control
            AxixReadTrans *trans = 
                static_cast<AxixReadTrans*>(_addressTransQueue.pop());
            ASSERT(trans, _xtor);
            retValue &= _xtor->getARChannel()->transfer(trans, currentTime);
            // Get the wait of first beat, if not already in wait state
            if (_dataTransTable.isEmpty()) // No transactions pending
                _dataWait = trans->getTransferWait();
            else // Push into data wait queue, when data cycle is ongoing
                ASSERT(_dataWaitQueue.push(trans), _xtor);
            // Push the transaction for data
            if (_dataTransTable.isFull())
            {
                // Internal code error
                _xtor->error(_xtor, CarbonXAxiError_InternalError,
                        "INTERNAL ERROR - "
                        "data transaction table in master read FSM is full.");
                return AxixFalse;
            }
            else if (!_dataTransTable.push(trans))
            {
                // No memory error
                _xtor->errorForNoMemory("Can't allocate memory for "
                        "transaction table.");
                return AxixFalse;
            }
            if (DEBUG)
                printf("# MASTER[%u]:READ: ADDRESS transferred.\n", 
                        static_cast<UInt>(currentTime));
            // Making VALID high
            nextValid = 1;
        }
        else // No more transactions pending - so make VALID low
            nextValid = 0;
        _xtor->getARChannel()->getValidPin()->write(nextValid, currentTime);
    }
    }

    //**** Data logic ****
    {
    CarbonUInt32 id = _xtor->getRChannel()->getIdPin()->read(currentTime);
    AxixBool valid = _xtor->getRChannel()->getValidPin()->read(currentTime);
    AxixBool ready = _xtor->getRChannel()->getReadyPin()->read(currentTime);
    // Transfer should happen at this clock
    if (valid && ready)
    {
        // The transaction is retreived from the data Table, which is stored
        // during address cycle. 
        AxixReadTrans *trans = 
            static_cast<AxixReadTrans*>(_dataTransTable.get(id));
        // If the received transaction ID is not in the table then 
        // it must generate an error.
        if (!trans)
        {
            AxixString buffer = AxixGetStringBuffer();
            sprintf(buffer, "Protocol error at time %u - "
                    "Read data (ID = %u) received for which no address "
                    "was sent.", static_cast<UInt>(currentTime), id);
            AxiXtor::error(_xtor, CarbonXAxiError_ProtocolError, buffer);
            return AxixFalse;
        }
        // Transfer to get received data and address
        retValue &= _xtor->getRChannel()->transfer(trans, currentTime);
        if (DEBUG)
            printf("# MASTER[%u]:READ: DATA BEAT transferred %u.\n", 
                    static_cast<UInt>(currentTime),
                    trans->getTransferIndex() - 1);
        if (trans->getLast())
        {
            if (!_dataWaitQueue.isEmpty())
                _dataWait = _dataWaitQueue.pop()->getTransferWait();
            else // when no transfer is pending, data wait queue will be empty
                _dataWait = 0;

            _dataWaitQueue.remove(trans);
            _xtor->reportTransaction(_dataTransTable.pop(id));
            _queueSpace++;
        }
        else // Get the wait of next beat
            _dataWait = trans->getTransferWait();
    }
    // Ready logic for next transfer
    AxixBool nextReady = 1; // default value of READY
    if (_dataWait > 0)
    {
        nextReady = 0; // make READY low for wait states
        _dataWait--;
    }
    _xtor->getRChannel()->getReadyPin()->write(nextReady, currentTime);
    }
    return retValue;
}

void AxixMasterReadFsm::print() const
{
    printf("Read transactions pending for:\n"
           "  Address transfer  = %u\n"
           "  Data transfer     = %u\n",
           _addressTransQueue.getCount(),
           _dataTransTable.getTransCount());
}
