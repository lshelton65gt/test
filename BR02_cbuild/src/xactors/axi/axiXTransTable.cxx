/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXTransTable.cpp
    Purpose: This file provides the definition for outstanding transaction
             table implementation.
*/

#include "axiXtor.h"
#include "axiXTransTable.h"

AxixTransTable::AxixTransTable(UInt size):
    _size(size), _count(0), _transCount(0),
    _transTable(NEWARRAY(AxixTransQueue*, _size))
{
    if (_transTable == NULL)
    {
        // No memory error
        AxiXtor::error(NULL, CarbonXAxiError_NoMemory,
                "Can't allocate memory for Transaction table");
        return;
    }
    for (UInt tableIndex = 0; tableIndex < _size; tableIndex++)
        _transTable[tableIndex] = NULL;
}

AxixTransTable::~AxixTransTable()
{
    if (_transTable == NULL)
        return;
    for (UInt tableIndex = 0; tableIndex < _size; tableIndex++)
    {
        // deletes queues
        if (_transTable[tableIndex] != NULL)
            delete _transTable[tableIndex];
    }
    // delete table
    DELETE(_transTable);
}

AxixBool AxixTransTable::checkAndCreateTable()
{
    if (_transTable == NULL)
    {
        // create table
        _transTable = NEWARRAY(AxixTransQueue*, _size);
        if (_transTable == NULL)
            return AxixFalse;
        for (UInt tableIndex = 0; tableIndex < _size; tableIndex++)
            _transTable[tableIndex] = NULL;
    }
    return AxixTrue;
}

AxixBool AxixTransTable::push(AxixTrans *trans)
{
    ASSERT(trans, NULL);
    CarbonUInt32 tableIndex;
    AxixTransQueue *transQueue = getQueue(trans->getId(), tableIndex);
    // If queue for ID exists push to that directly
    if (transQueue != NULL)
    {
        if (!transQueue->push(trans))
            return AxixFalse;
        _transCount++;
        return AxixTrue;
    }

    // Check and create table
    if (!checkAndCreateTable())
        return AxixFalse;

    for (tableIndex = 0; tableIndex < _size; tableIndex++)
    {
        if (_transTable[tableIndex] == NULL)
        {
            // Create queue for ID
            _transTable[tableIndex] = new AxixTransQueue(0);
            if (_transTable[tableIndex] == NULL)
                return AxixFalse;
            _count++;
            // Push to that queue
            if (!_transTable[tableIndex]->push(trans))
                return AxixFalse;
            _transCount++;
            return AxixTrue;
        }
    }

    return AxixFalse;
}

AxixTrans* AxixTransTable::pop(CarbonUInt32 transId)
{
    if (_transTable == NULL)
        return NULL;

    CarbonUInt32 tableIndex;
    AxixTransQueue *transQueue = getQueue(transId, tableIndex);
    // If queue is NULL, return NULL
    if (transQueue == NULL)
        return NULL;

    // Pops from queue
    AxixTrans *trans = transQueue->pop();
    // If queue for that ID is empty, delete it
    if (transQueue->getCount() == 0)
    {
        delete transQueue;
        _count--;
        for (; tableIndex < _count; tableIndex++)
            _transTable[tableIndex] = _transTable[tableIndex + 1];
        _transTable[tableIndex] = NULL;
    }
    _transCount--;
    return trans;
}

AxixTrans* AxixTransTable::get(CarbonUInt32 transId)
{
    if (_transTable == NULL)
        return NULL;

    CarbonUInt32 tableIndex;
    AxixTransQueue *transQueue = getQueue(transId, tableIndex);
    if (transQueue == NULL)
        return NULL;

    return (transQueue->top());
}

AxixTransQueue* AxixTransTable::getQueue(CarbonUInt32 tableIndex)
{
    return (tableIndex < _count && _transTable) ? _transTable[tableIndex] :
        NULL;
}

// It deletes transaction as well
void AxixTransTable::deleteAll()
{
    if (_transTable == NULL)
        return;

    AxixTransQueue *transQueue;
    for (UInt tableIndex = 0; tableIndex < _size; tableIndex++)
    {
        transQueue = _transTable[tableIndex];
        if (transQueue == NULL)
            continue;
        AxixTrans *trans;
        while ((trans = transQueue->pop()) != NULL)
            delete trans;
        delete _transTable[tableIndex];
        _transTable[tableIndex] = NULL;
    }
}

// Returns queue for ID
AxixTransQueue* AxixTransTable::getQueue(CarbonUInt32 transId,
        CarbonUInt32 &tableIndex)
{
    if (_transTable == NULL)
        return NULL;

    AxixTransQueue *transQueue;
    // For each index in table, check queue's first item for ID - if matches
    // returns that queue
    for (tableIndex = 0; tableIndex < _size; tableIndex++)
    {
        transQueue = _transTable[tableIndex];
        if (transQueue != NULL &&
                (transQueue->top())->getId() == transId)
            return transQueue;
    }
    return NULL;
}

