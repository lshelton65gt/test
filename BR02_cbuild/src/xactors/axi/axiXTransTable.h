/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_TRANSTABLE_H
#define AXIX_TRANSTABLE_H

#include "axiXTrans.h"
#include "axiXTransQueue.h"

/*!
 * \file axiXTransTable.h
 * \brief Header file for outstanding transaction table implementation.
 */

/*!
 * \brief TransTable class
 *
 * The transaction table is an array of queues holding transaction with same ID.
 * Thus one can think of that the table is indexed by ID's.
 */
class AxixTransTable
{
public: CARBONMEM_OVERRIDES

private:

    const UInt _size;                //!< Max. no of outstanding transactions
                                     //   with different IDs
    UInt _count;                     //!< Table fill count
    UInt _transCount;                //!< Outstanding transaction count
    AxixTransQueue **_transTable;    //!< Transaction table of queues. Each
                                     //   queue holds an ordered transactions
                                     //   of same ID. The order is in the manner
                                     //   the transactions came.

public:

    // Constructor and destructor
    AxixTransTable(UInt size);
    ~AxixTransTable();

    // Access functions
    // Get size of table i.e. number of different IDs it can hold
    UInt getSize() const { return _size; }
    // Gets the table fill level
    UInt getCount() const { return _count; }
    // Gets the number of transactions it is holding currently
    UInt getTransCount() const { return _transCount; }
    // Checks empty
    AxixBool isEmpty() const { return (_count == 0); }
    // Checks if full i.e. no new ID can't be pushed
    AxixBool isFull() const { return (_count == _size); }

    // Push a new transacton
    AxixBool push(AxixTrans *trans);
    // Pops the oldest transaction for a given ID
    AxixTrans* pop(CarbonUInt32 transId);
    // Gets the oldest transaction for a given ID, without poping it
    AxixTrans* get(CarbonUInt32 transId);

    // Get transaction queue corresponding to table index
    AxixTransQueue* getQueue(CarbonUInt32 tableIndex);

    // Deletes all transactions in the table
    void deleteAll();

private:

    // Check the table and create if not created already
    AxixBool checkAndCreateTable();
    // Get transaction queue corresponding to ID
    AxixTransQueue* getQueue(CarbonUInt32 transId, CarbonUInt32 &tableIndex);
};

#endif
