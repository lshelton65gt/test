/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_CONFIG_H
#define AXIX_CONFIG_H

#include "axiXDefines.h"
#include "xactors/axi/carbonXAxiConfig.h"

/*!
 * \file axiXConfig.h
 * \brief Header file for configuration class.
 *
 * This file provides the class definition for configuration object.
 */

/*!
 * \brief Class to represent configuration object.
 */
class CarbonXAxiConfigClass
{
public: CARBONMEM_OVERRIDES

private:

    CarbonUInt32 _idBusWidth;                   //!< Width of ID field
    CarbonXAxiAddressBusWidth _addressBusWidth; //!< Width of address bus
    CarbonXAxiDataBusWidth _dataBusWidth;       //!< Width of data bus
    
public:

    // Constructor and destructor
    CarbonXAxiConfigClass():
        _idBusWidth(4), _addressBusWidth(CarbonXAxiAddressBusWidth_32),
        _dataBusWidth(CarbonXAxiDataBusWidth_32) {}
    ~CarbonXAxiConfigClass() {}

    // Set get functions for ID bus width
    void setIDBusWidth(CarbonUInt32 width) 
    { _idBusWidth = width; }
    CarbonUInt32 getIDBusWidth() const { return _idBusWidth; }
    // Set get functions for address bus width
    void setAddressBusWidth(CarbonXAxiAddressBusWidth width)
    { _addressBusWidth = width; }
    CarbonXAxiAddressBusWidth getAddressBusWidth() const
    { return _addressBusWidth; }
    // Set get functions for data bus width
    void setDataBusWidth(CarbonXAxiDataBusWidth width)
    { _dataBusWidth = width; }
    CarbonXAxiDataBusWidth getDataBusWidth() const { return _dataBusWidth; }
};

#endif
