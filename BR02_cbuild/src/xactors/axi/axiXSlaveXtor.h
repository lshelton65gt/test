/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_SLAVEXTOR_H
#define AXIX_SLAVEXTOR_H

#include "axiXtor.h"
#include "axiXTransQueue.h"

/*!
 * \file axiXSlaveXtor.h
 * \brief Header file for slave transactor definition
 */

/*!
 * \brief This class defines the slave transactor.
 */
class AxixSlaveXtor: public CarbonXAxiClass
{
public: CARBONMEM_OVERRIDES

private:

    //! Response transaction callbacks
    void (*_setDefaultResponse)(CarbonXAxiTrans*, void*);
    void (*_setResponse)(CarbonXAxiTrans*, void*);
          
public:

    //! Constructor
    AxixSlaveXtor(AxixConstString name, const CarbonXAxiConfig *config,
            const CarbonUInt32 maxTransfers);
    //! Destructor
    ~AxixSlaveXtor() {}
    
    //! This function returns transactor's type
    CarbonXAxiType getType() const
    { return CarbonXAxi_Slave; }
    //! This function prints the current status of the transactor
    void print() const;

    //! Callback registation function
    void registerCallbacks(void *userContext,
            void (*reportTransaction)(CarbonXAxiTrans*, void*),
            void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
            void (*setResponse)(CarbonXAxiTrans*, void*),
            void (*errorTransCallback)(CarbonXAxiErrorType, const char*,
                const CarbonXAxiTrans*, void*),
            void (*errorCallback)(CarbonXAxiErrorType, const char*, void*));
    void setDefaultResponse(CarbonXAxiTrans *trans);
    void setResponse(CarbonXAxiTrans *trans);

    // Invalid transaction routines
    AxixBool canAcceptNewTrans(CarbonXAxiTransType transType) const
    { return AxixFalse; transType = CarbonXAxiTrans_WRITE; }
    AxixBool startNewTransaction(CarbonXAxiTrans *trans)
    { return AxixFalse; trans = NULL; }
};

#endif
