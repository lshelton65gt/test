/*******************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_PIN_H
#define AXIX_PIN_H

#include "axiXDefines.h"
#include "xactors/carbonX.h"

/*!
 * \file axiXPin.h
 * \brief Header file for pin definition.
 *
 * This file provides the class definition for generic address, control and
 * data pins.
 */

/*!
 * \brief Direction type
 *
 * This type defines the direction of pin. This will control the direction
 * of data flow to and from the transactor during refresh. Normal read and
 * write will return or update an internal value, which actually becomes
 * operational during refresh cycle.
 */
enum AxixDirection {
    AxixDirection_Unknown,
    AxixDirection_Input,
    AxixDirection_Output,
    AxixDirection_Inout
};

/*!
 * \brief Class to represent transactor's generic pin.
 *
 * This represents address, control and data pin, which can be connected
 * with Carbon Model by a CarbonNet or through read/write callback.
 */
class AxixPin
{
public: CARBONMEM_OVERRIDES

private:

    const AxiXtor *_xtor;                 //!< Transactor reference
    AxixConstString _name;                //!< Pin name
    const AxixDirection _direction;       //!< Pin direction
    const CarbonUInt32 _size;             //!< Pin width
    const CarbonUInt32 _size32;           //!< Pin width in 32 bits
    CarbonUInt32 *_currValue;             //!< Buffered value of net, which
                                          //   updated during referesh cycle.
                                          //   This is an array of 32 bit data
                                          //   of size _size32.
 
    /* Carbon Model connection mode */
    CarbonObjectID *_module;               //!< Connected Carbon Model module
    CarbonNetID *_net;                     //!< Connected Carbon Model net

    /* Callback connection mode */
    CarbonXSignalChangeCB _changeCallback; //!< Callback functions to
                                           //   to refresh the currValue
                                           //   accessimng the net
    void *_callbackContext;                //!< Callback context

public:

    // Constructor and destructor
    AxixPin(AxiXtor *xtor, AxixConstString name,
            const AxixDirection direction, const CarbonUInt32 size = 1);
    ~AxixPin();

    // Get functions
    AxixConstString getName() const { return _name; }
    AxixDirection getDirection() const { return _direction; }
    CarbonUInt32 getSize() const { return _size; }
    CarbonUInt32 getSize32() const { return _size32; }

    // Environment binding
    AxixBool bind(CarbonObjectID *module, CarbonNetID *net);
    AxixBool bind(CarbonXSignalChangeCB changeCallback,
            void *callbackContext);

    // Read/Write access during evaluation
    AxixBool write(const CarbonUInt32 data, CarbonTime currentTime);
    AxixBool write(const CarbonUInt32 *data, CarbonTime currentTime);
    AxixBool read(CarbonUInt32 &data, CarbonTime currentTime) const;
    CarbonUInt32 read(CarbonTime currentTime) const; // same as above
    AxixBool read(CarbonUInt32 *data, CarbonTime currentTime) const;

    // Refresh
    AxixBool refresh(CarbonTime currentTime);

    // Utility functions
    // Gets a bit from 'reg' indexed by 'index'
    static AxixBit getBit(CarbonUInt32 reg, UInt index);
    // Prints data
    void printData() const;
    // Dumps the pin state
    void print(CarbonTime currentTime) const;
};

#endif
