/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXMasterXtor.cpp 
    Purpose: This file contains the definition of the functions for
             master axi transactor.
*/

#include "axiXMasterXtor.h"
#include "axiXMasterWriteFsm.h"
#include "axiXMasterReadFsm.h"

// Constructor
AxixMasterXtor::AxixMasterXtor(AxixConstString name,
        const CarbonXAxiConfig *config, const CarbonUInt32 maxTransfers):
    CarbonXAxiClass(name, config, CarbonXAxi_Master),
    _reportTransaction(NULL)
{
    ASSERT(maxTransfers > 0, this);
    // Creates Master Write FSM
    _writeFsm = new AxixMasterWriteFsm(this, maxTransfers);
    // Check for memory allocation error
    if (_writeFsm == NULL)
    {
        errorForNoMemory("master write FSM");
        return;
    }
    // Creates Master Read FSM
    _readFsm = new AxixMasterReadFsm(this, maxTransfers);
    // Check for memory allocation error
    if (_readFsm == NULL)
    {
        errorForNoMemory("master read FSM");
        return;
    }
}

// Register callbacks
void AxixMasterXtor::registerCallbacks(void *userContext,
        void (*reportTransaction)(CarbonXAxiTrans*, void*),
        void (*setDefaultResponse)(CarbonXAxiTrans*, void*),
        void (*setResponse)(CarbonXAxiTrans*, void*),
        void (*errorTransCallback)(CarbonXAxiErrorType, const char*,
            const CarbonXAxiTrans*, void*),
        void (*errorCallback)(CarbonXAxiErrorType, const char*, void*))
{   
    CarbonXAxiClass::registerCallbacks(userContext, reportTransaction,
           setDefaultResponse, setResponse, errorTransCallback, errorCallback);
    _reportTransaction = reportTransaction;
}

// Report trabsaction wrapper
void AxixMasterXtor::reportTransaction(CarbonXAxiTrans *trans)
{
    if (_reportTransaction != NULL)
        (*_reportTransaction)(trans, _userContext);
}

void AxixMasterXtor::print() const
{
    AxiXtor::print();
}

AxixBool AxixMasterXtor::canAcceptNewTrans(CarbonXAxiTransType transType) const
{
    // Depending on write or read transaction checks transaction buffer fill
    // level of corresponding FSM
    if (transType == CarbonXAxiTrans_WRITE)
        return _writeFsm->canAcceptNewTrans();
    else
        return _readFsm->canAcceptNewTrans();
    return AxixFalse;
}

// Start new transaction
AxixBool AxixMasterXtor::startNewTransaction(CarbonXAxiTrans *trans)
{
    ASSERT(trans, this);
    // First resolve default burst width and burst sizes
    if (!trans->resolveDefaults(this))
        return AxixFalse;
    // Check for trasaction validity i.e. info provided are as per protocol
    if (!trans->checkValidity(this))
        return AxixFalse;
    // Depending on write or read transaction, pushes to proper FSMs queue
    if (trans->getType() == CarbonXAxiTrans_WRITE)
        return _writeFsm->startNewTransaction(trans);
    else
        return _readFsm->startNewTransaction(trans);
    return AxixFalse;
}
