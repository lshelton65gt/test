/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    axiXAWChannel.cpp
    Purpose: This file contains the definition of the functions for
             accessing address write(AW) channel.
*/

#include "axiXAWChannel.h"
#include "axiXtor.h"

// Constructor
AxixAWChannel::AxixAWChannel(AxiXtor *xtor,
        const AxixDirection channelDir, const CarbonUInt32 idBusWidth,
        const CarbonXAxiAddressBusWidth addressBusWidth):
    AxixChannel(xtor, "AW", channelDir, idBusWidth),
    _addrPin(new AxixPin(xtor, "AWADDR", channelDir, addressBusWidth)),
    _lenPin(new AxixPin(xtor, "AWLEN", channelDir, 4)),
    _sizePin(new AxixPin(xtor, "AWSIZE", channelDir, 3)),
    _burstPin(new AxixPin(xtor, "AWBURST", channelDir, 2)),
    _lockPin(new AxixPin(xtor, "AWLOCK", channelDir, 2)),
    _cachePin(new AxixPin(xtor, "AWCACHE", channelDir, 4)),
    _protPin(new AxixPin(xtor, "AWPROT", channelDir, 3))
{
    // Check for memory allocation error
    if (_addrPin == NULL)
    {
        xtor->errorForNoMemory("AWADDR");
        return;
    }
    if (_lenPin == NULL)
    {
        xtor->errorForNoMemory("AWLEN");
        return;
    }
    if (_sizePin == NULL)
    {
        xtor->errorForNoMemory("AWSIZE");
        return;
    }
    if (_burstPin == NULL)
    {
        xtor->errorForNoMemory("AWBURST");
        return;
    }
    if (_lockPin == NULL)
    {
        xtor->errorForNoMemory("AWLOCK");
        return;
    }
    if (_cachePin == NULL)
    {
        xtor->errorForNoMemory("AWCACHE");
        return;
    }
    if (_protPin == NULL)
    {
        xtor->errorForNoMemory("AWPROT");
        return;
    }
}

AxixAWChannel::~AxixAWChannel()
{
    delete _addrPin;
    delete _lenPin;
    delete _sizePin;
    delete _burstPin;
    delete _lockPin;
    delete _cachePin;
    delete _protPin;
}

// Transfers the transaction info to and from channel
AxixBool AxixAWChannel::transfer(AxixWriteTrans *trans, CarbonTime currentTime)
{
    ASSERT(trans, _xtor);
    if (_channelDir == AxixDirection_Input)
    {
        ASSERT(trans->getId() == _idPin->read(currentTime), _xtor);
        CarbonUInt32 address[2];
        _addrPin->read(address, currentTime);
        CarbonUInt64 address64 = address[0];
        if (_addrPin->getSize() > 32)
            address64 |= (static_cast<CarbonUInt64>(address[1]) << 32);
        trans->setStartAddress(address64);
        trans->setBurstLength(_lenPin->read(currentTime));
        trans->setBurstSize(
                static_cast<CarbonXAxiBurstSize>(_sizePin->read(currentTime)));
        trans->setBurstType(
                static_cast<CarbonXAxiBurstType>(_burstPin->read(currentTime)));
        trans->setAccessType(
                static_cast<CarbonXAxiAccessType>(_lockPin->read(currentTime)));
        trans->setCacheType(_cachePin->read(currentTime));
        trans->setProtType(_protPin->read(currentTime));
    }
    else // output
    {
        _idPin->write(trans->getId(), currentTime);
        CarbonUInt32 address[2];
        CarbonUInt64 address64 = trans->getStartAddress();
        address[0] = static_cast<CarbonUInt32>(address64 & 0xFFFFFFFF);
        address[1] = static_cast<CarbonUInt32>(address64 >> 32);
        _addrPin->write(address, currentTime);
        _lenPin->write(trans->getBurstLength(), currentTime);
        _sizePin->write(
                static_cast<CarbonUInt32>(trans->getBurstSize()), currentTime);
        _burstPin->write(
                static_cast<CarbonUInt32>(trans->getBurstType()), currentTime);
        _lockPin->write(
                static_cast<CarbonUInt32>(trans->getAccessType()), currentTime);
        _cachePin->write(trans->getCacheType(), currentTime);
        _protPin->write(trans->getProtType(), currentTime);
    }
    return AxixTrue;
}

// Refresh each pins
AxixBool AxixAWChannel::refresh(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    retValue &= _addrPin->refresh(currentTime);
    retValue &= _lenPin->refresh(currentTime);
    retValue &= _sizePin->refresh(currentTime);
    retValue &= _burstPin->refresh(currentTime);
    retValue &= _lockPin->refresh(currentTime);
    retValue &= _cachePin->refresh(currentTime);
    retValue &= _protPin->refresh(currentTime);
    return retValue;
}
