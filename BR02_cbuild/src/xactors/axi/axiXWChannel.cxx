/*******************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/* 
    File:    axiXWChannel.cpp
    Purpose: This file contains the definition of the functions for 
             accessing write(W) channel.
*/

#include "axiXWChannel.h"
#include "axiXtor.h"

// Constructor
AxixWChannel::AxixWChannel(AxiXtor *xtor,
        const AxixDirection channelDir, const CarbonUInt32 idBusWidth,
        const CarbonXAxiDataBusWidth dataBusWidth):
    AxixChannel(xtor, "W", channelDir, idBusWidth),
    _dataBusWidth(dataBusWidth),
    _dataPin(new AxixPin(xtor, "WDATA", channelDir, dataBusWidth)),
    _strbPin(new AxixPin(xtor, "WSTRB", channelDir, dataBusWidth / 8)),
    _lastPin(new AxixPin(xtor, "WLAST", channelDir, 1))
{
    // Check for memory allocation error
    if (_dataPin == NULL)
    {
        xtor->errorForNoMemory("WDATA");
        return;
    }
    if (_strbPin == NULL)
    {
        xtor->errorForNoMemory("WSTRB");
        return;
    }
    if (_lastPin == NULL)
    {
        xtor->errorForNoMemory("WLAST");
        return;
    }
}

AxixWChannel::~AxixWChannel()
{
    delete _dataPin;
    delete _strbPin;
    delete _lastPin;
}

// Transfers the transaction info to and from channel
AxixBool AxixWChannel::transfer(AxixWriteTrans *trans, CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;

    ASSERT(trans, _xtor);
    // Error when transaction burst size is larger than data bus width
    if (static_cast<UInt>(1 << trans->getBurstSize()) * 8 > _dataBusWidth)
    {
        AxixString buffer = AxixGetStringBuffer();
        sprintf(buffer, "Invalid transaction at time %u - "
                "burst size %u bytes is more than data bus width %u bits.",
                static_cast<UInt>(currentTime),
                1 << trans->getBurstSize(), _dataBusWidth);
        AxiXtor::error(_xtor, CarbonXAxiError_InvalidTrans, buffer, trans);
        return AxixFalse;
    }
    ASSERT(trans->getLast() == 0, _xtor); // last == 1 means data transfer
                                   // is complete in earlier cycle

    // Gets lower and upper bytelanes
    UInt lowerByteLane, upperByteLane;
    trans->getByteLanes(_dataBusWidth, lowerByteLane, upperByteLane);

    CarbonUInt32 data[32];
    CarbonUInt32 strb[4];
    CarbonUInt8 *dataBytes = trans->getData();
    CarbonUInt1 *byteStrobes = trans->getStrobe();
    if (_channelDir == AxixDirection_Input)
    {
        // When you need to create data array within transaction for
        // storing incoming data and stobe
        if (dataBytes == NULL)
        {
            if (!trans->createData()) // create data and response array
            {
                AxiXtor::error(_xtor, CarbonXAxiError_NoMemory,
                        "Can't allocate memory for "
                        "received write transaction data.", trans);
                return AxixFalse;
            }
            dataBytes = trans->getData(); // get pointer of created data array
            byteStrobes = trans->getStrobe();
        }
        ASSERT(trans->getId() == _idPin->read(currentTime), _xtor);
        _dataPin->read(data, currentTime);
        _strbPin->read(strb, currentTime);
        // 32 bit array to byte array conversion
        for (UInt byteLane = 0; byteLane < _dataBusWidth / 8; byteLane++)
        {
            CarbonUInt32 &data32 = data[byteLane / 4];
            CarbonUInt32 &strb32 = strb[byteLane / 32];
            if (byteLane >= lowerByteLane && byteLane <= upperByteLane)
            {
                // Transfer for valid byte lanes
                UInt byteIndex = trans->getByteIndexToTransfer();
                dataBytes[byteIndex] = data32 & 0xFF;
                byteStrobes[byteIndex] = strb32 & 0x1;
                trans->updateByteIndexToTransfer();
            }
            // Error if strobe is high for invalid byte lanes
            else if (strb32 & 0x1)
            {
                AxixString buffer = AxixGetStringBuffer();
                sprintf(buffer, "Protocol error at time %u - "
                        "strobe is found to be asserted for invalid %u byte "
                        "lane in %u transfer.",
                        static_cast<UInt>(currentTime),
                        byteLane, trans->getTransferIndex());
                AxiXtor::error(_xtor, CarbonXAxiError_InvalidTrans, buffer, trans);
                return AxixFalse;
            }
            data32 = data32 >> 8;
            strb32 = strb32 >> 1;
        }
        trans->setLast(_lastPin->read(currentTime));

        // Check protocol error i.e. last should be high iff last data
        // transfer of transfer index equal to burst length
        if ((trans->getTransferIndex() == trans->getBurstLength()) ^
                (trans->getLast()))
        {
            AxixString buffer = AxixGetStringBuffer();
            sprintf(buffer, "Protocol error at time %u - "
                    "WLAST should be high only for last transfer (%u), "
                    "while transfer %u is ongoing.",
                    static_cast<UInt>(currentTime),
                    trans->getBurstLength(), trans->getTransferIndex());
            AxiXtor::error(_xtor, CarbonXAxiError_ProtocolError, buffer, trans);
            return AxixFalse;
        }
    }
    else // for output pin
    {
        // Error when less number of data provided than required
        if (dataBytes == NULL || (!trans->useStrobeForLessData() &&
                trans->getDataSize() < trans->getActualDataSize()))
        {
            AxixString buffer = AxixGetStringBuffer();
            sprintf(buffer, "Invalid transaction at time %u - "
                    "less number (%u) of data provided.",
                    static_cast<UInt>(currentTime),
                    (dataBytes == NULL) ? 0 : trans->getDataSize());
            AxiXtor::error(_xtor, CarbonXAxiError_InvalidTrans, buffer, trans);
            return AxixFalse;
        }
        _idPin->write(trans->getId(), currentTime);
        // Byte array to 32 bit array conversion
        for (UInt byteLane = 0; byteLane < _dataBusWidth / 8; byteLane++)
        {
            CarbonUInt32 &data32 = data[byteLane / 4];
            if (byteLane % 4 == 0)
                data32 = 0;
            CarbonUInt32 &strb32 = strb[byteLane / 32];
            if (byteLane % 32 == 0)
                strb32 = 0;
            if (byteLane >= lowerByteLane && byteLane <= upperByteLane)
            {
                UInt byteIndex = trans->getByteIndexToTransfer();
                if (byteIndex < trans->getDataSize())
                {
                    data32 |= (dataBytes[byteIndex] & 0xFF)
                        << (byteLane % 4) * 8;
                    strb32 |= (byteIndex < trans->getStrobeSize() ?
                        (byteStrobes[byteIndex] & 0x1) : 1) << (byteLane % 32);
                }
                trans->updateByteIndexToTransfer();
            }
        }
        _dataPin->write(data, currentTime);
        _strbPin->write(strb, currentTime);
        UInt introError = 0; // You can introduce error by making this > 0
        if (trans->getTransferIndex() == trans->getBurstLength() - introError)
            trans->setLast(1);
        _lastPin->write(trans->getLast(), currentTime);
    }
    trans->updateTransferIndex();
    return retValue;
}

// Refresh channel pins
AxixBool AxixWChannel::refresh(CarbonTime currentTime)
{
    AxixBool retValue = AxixTrue;
    retValue &= _dataPin->refresh(currentTime);
    retValue &= _strbPin->refresh(currentTime);
    retValue &= _lastPin->refresh(currentTime);
    return retValue;
}
