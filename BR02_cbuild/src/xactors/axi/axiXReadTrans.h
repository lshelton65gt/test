/*******************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef AXIX_READ_TRANS_H
#define AXIX_READ_TRANS_H

#include "axiXDefines.h"
#include "axiXTrans.h"
#include "xactors/axi/carbonXAxi.h"
#include "xactors/axi/carbonXAxiTrans.h"

/*!
 * \file axiXReadTrans.h
 * \brief Header file for read Transaction Class.
 */

/*!
 * \brief Class to represent read Transaction.
 */
class AxixReadTrans : public CarbonXAxiTransClass
{
public: CARBONMEM_OVERRIDES

private:

    CarbonUInt32 _responseSize;            //!< Size of the response array
    CarbonXAxiResponse *_response;         //!< Response array

public:

    // Constructor and destructor
    AxixReadTrans(CarbonUInt32 id);
    ~AxixReadTrans();

    // Access routines
    CarbonXAxiTransType getType() const
    { return CarbonXAxiTrans_READ; }

    // Creates array for holding data and response for incoming read transaction
    AxixBool createData();

    // Set get for response
    AxixBool setResponse(CarbonUInt32 responseSize, 
            const CarbonXAxiResponse *response);
    CarbonUInt32 getResponseSize() const
    { return _responseSize; }
    CarbonXAxiResponse *getResponse() const
    { return _response; }
    
    // Validity check function
    AxixBool checkValidity(const AxiXtor *xtor) const;

    // Debugging function
    void print() const;
};

#endif
