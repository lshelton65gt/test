//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifdef __cplusplus
extern "C" {
#endif

#include "xactors/sparse_mem/carbonXSparseMem.h"

  void    carbonXDdrsdram16Megx16MemFcn(sparseMem_t mem);
  u_int16 carbonXDdrsdram16Megx16Read(sparseMem_t mem, u_int32 ba, u_int32 ra, u_int32 ca);
  void    carbonXDdrsdram16Megx16Write(sparseMem_t mem, u_int32 ba, u_int32 ra, u_int32 ca, u_int16 data);

#ifdef __cplusplus
}
#endif



