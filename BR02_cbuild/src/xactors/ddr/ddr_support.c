/*
 *****************************************************************************
 *
 * Module Name: ddr_support.c
 *
 * Module Description:
 *      The "ddr_support.c" file contains the common project support routines.
 * 
 *
 * Author  : Andy Zhang
 *
 * Created : Nov 3, 2003
 *
 *
 *****************************************************************************
 */


/*
 * include files
 */
#include "tbp.h"
#include "carbonXDdrsdramCtrl.h"

//*****************************************************************************
//*      Set DDR Mode Register
//*****************************************************************************
void carbonXDdrsdramCtrlSetModeReg (u_int32 mode)
{
  carbonXIdle(10);
  carbonXWrite32(DDR_MODE_REG_ADDR, mode);
  carbonXIdle(20);
}

//*****************************************************************************
//*      DDR Write Function
//*****************************************************************************
void carbonXDdrsdramCtrlWrite (u_int32 addr, u_int64 wdata)
{
  carbonXWrite64 (addr, wdata);
}

//*****************************************************************************
//*      DDR Read Function
//*****************************************************************************
u_int64 carbonXDdrsdramCtrlRead (u_int32 addr)
{
  return carbonXRead64 (addr);
}






