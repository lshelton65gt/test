
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
 *****************************************************************************
 *
 * File Name: ddr_support.h
 *
 * This file serves as the toplevel include file for the project source.
 * It should be included by all project source and hold all #includes
 * which are common to all source.
 *
 *****************************************************************************
 */
#ifndef DDR_SUPPORT_H
#define DDR_SUPPORT_H

//#include "tbp.h"
//#include "project.h"

#define DDR_MODE_REG_ADDR   0x80000000


void DDR_SetModeReg (u_int32 mode);
void DDR_Write (u_int32 addr, u_int64 wdata);
u_int64 DDR_Read (u_int32 addr);
void print_rdata64 (u_int32 addr, u_int64 rdata);
void print_wdata64 (u_int32 addr, u_int64 wdata);
void Check_Results (u_int32 addr, u_int64 wdata, u_int64 rdata);

#endif
