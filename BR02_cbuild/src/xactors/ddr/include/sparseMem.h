#ifndef SPARSE_MEM_H
#define SPARSE_MEM_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

// These defines may be modified for performance tuning.
#define WORDS32_PER_PAGE 64
#define PAGE_ADDR_BITS (WORDS32_PER_PAGE/8)
#define ADDR_MASK ((1<<(WORDS32_PER_PAGE/8)) -1)
#define HASH_BINS 100000

#define NegInfinity (-10000)

#define Error( Str )        FatalError( Str )
#define FatalError( Str )   fprintf( stderr, "%s\n", Str ), exit( 1 )

typedef  u_int32 redBlackKey_t;
//typedef  int redBlackKey_t;

typedef struct info_t {
  redBlackKey_t  addr;  // used as key
  u_int32         data[WORDS32_PER_PAGE];
  //  u_int32     dataValid[WORDS32_PER_PAGE];
} info_t, *infoRef_t;

struct redBlackNode_t;

typedef struct redBlackNode_t *nodeRef_t;
typedef struct redBlackNode_t *redBlackTree_t;

typedef redBlackTree_t *sparseMem_t;

sparseMem_t SparseMemCreateNew();
u_int32     SparseMemReadWord32( sparseMem_t mem, u_int32 addr );
void        SparseMemWriteWord32( sparseMem_t mem, u_int32 addr, u_int32 data );

#endif  // SPARSE_MEM_H
