#ifdef __cplusplus
extern "C" {
#endif

#define mem_class 0
#define io_class  1
/*
 *  General purpose access routines, with screwy restrictions:
 *    Size is 1,2,4  *ONLY*
 *    Adresses must be aligned to data size -- 4 byte op to addr 1,2,3 is illegal
 */
  extern u_int32 mem_read   (int class_id, int id, u_int64 addr, int size);
  extern void    mem_write  (int class_id, int id, u_int64 addr, u_int32 data, int size);

/*
 *  These routines are intended for backdoor access to the memory data
 */
  extern void    mem_write64(u_int32 mem_id,  u_int64 addr, u_int64 data);
  extern void    mem_write32(u_int32 mem_id,  u_int64 addr, u_int32 data );
  extern void    mem_write16(u_int32 mem_id,  u_int64 addr, u_int16 data );
  extern void    mem_write8 (u_int32 mem_id,  u_int64 addr, u_int8  data );

  extern u_int64 mem_read64 (u_int32 mem_id,  u_int64 addr);
  extern u_int32 mem_read32 (u_int32 mem_id,  u_int64 addr );
  extern u_int16 mem_read16 (u_int32 mem_id,  u_int64 addr );
  extern u_int8  mem_read8  (u_int32 mem_id,  u_int64 addr );

/*
 *  I/O Accesses are limited to 32 bits, so that's all we provide
 */
  extern void    io_write   (u_int32 mem_id,  u_int64 addr, u_int32 data );
  extern u_int32 io_read    (u_int32 mem_id,  u_int64 addr );

/*
 *  I/O ops for obtuse reasons in target transactor control function
 */
  extern u_int64 io_read64  (u_int32 mem_id,  u_int64 addr);
  extern void    io_write64 (u_int32 mem_id,  u_int64 addr, u_int64 data);

#ifdef __cplusplus
}
#endif
