//typedef unsigned int sU32;



#ifdef __cplusplus
extern "C" {
#endif

#include "sparseMem.h"
//#include "sparse_mem.h" 
 
//int ddr_array [0x1000000];



//extern unsigned long  mem_read(int class_id, int id, unsigned long addr, int byteaddr);
//extern int mem_write(int class_id, int id, unsigned long addr, unsigned long data, int byteaddr);
//extern void cache_16x16_mem_read(unsigned *read_array, int class, int id, unsigned long addr);
//extern void cache_16x16_mem_write(unsigned *write_array, int class, int id, unsigned long addr);
extern void cache_16x16_mem_read(sparseMem_t mem, unsigned *read_array, unsigned long addr);
//extern void cache_16x16_mem_read(u_int32 mem, unsigned *read_array, unsigned long addr);
extern void cache_16x16_mem_write(sparseMem_t mem, unsigned *write_array, unsigned long addr);
//extern void cache_16x16_mem_write(u_int32 mem, unsigned *write_array, unsigned long addr);

extern void ddr_16megx16_mem_fcn(void);
extern void ddr_16megx16_mem_errors(int class, int id, int error1, int error2);
extern void ddr_16megx16_time_monitor(int class, int id, unsigned long cycle_count, int rdwr);


#ifdef __cplusplus
}
#endif



