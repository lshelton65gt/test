//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 
//  Description:  Parameterizable SDRAM DDR Model
//  Legal Densities in Megabits are: 64, 128, 256, 512 and 1024
//  Legal Data Width in bits are   : x4, x8, x16
//
//  Any other configuration would be considered illegal and behaviour
//  is undefined.                    
//

`timescale 1ns/1ns 

module carbonx_ddrsdram (dq, 
			 dqs, 
			 addr, 
			 ba, 
			 clk, 
			 clk_n, 
			 cke, 
			 cs_n, 
			 cas_n, 
			 ras_n, 
			 we_n, 
			 dm);

   // Memory Density in Megabits
   parameter DENSITY    = 256;

   // Data Width in bits
   parameter DATA_WIDTH = 16;

   // These parameters are used a a coding aid and should
   // not be specified by instantiation of the module
   parameter ADDR_WIDTH = (DENSITY ==  64 || DENSITY == 128) ? 12 :
			  (DENSITY == 256 || DENSITY == 512) ? 13 :
			  (DENSITY == 1024) ? 14 : -1;
   parameter DQS_WIDTH  = (DATA_WIDTH == 16) ? 2 : 1;
   
   inout [DATA_WIDTH-1:0] dq;
   inout [DQS_WIDTH-1:0]  dqs;
   input [ADDR_WIDTH-1:0] addr;
   input [1:0] 		  ba;
   input 		  clk;
   input 		  clk_n;
   input 		  cke;
   input 		  cs_n;
   input 		  cas_n;
   input 		  ras_n;
   input 		  we_n;
   input [DQS_WIDTH-1:0]  dm;

   // Protected Parameters, The user should not set these parameters directly
   parameter 		  ROW_ADDR_WIDTH = ADDR_WIDTH;
   parameter              COL_ADDR_WIDTH = (DENSITY ==   64 && DATA_WIDTH ==  4) ? 10 :
					   (DENSITY ==  128 && DATA_WIDTH ==  4) ? 11 :
					   (DENSITY ==  256 && DATA_WIDTH ==  4) ? 11 :
					   (DENSITY ==  512 && DATA_WIDTH ==  4) ? 12 :
					   (DENSITY == 1024 && DATA_WIDTH ==  4) ? 12 :

					   (DENSITY ==   64 && DATA_WIDTH ==  8) ?  9 :
					   (DENSITY ==  128 && DATA_WIDTH ==  8) ? 10 :
					   (DENSITY ==  256 && DATA_WIDTH ==  8) ? 10 :
					   (DENSITY ==  512 && DATA_WIDTH ==  8) ? 11 :
					   (DENSITY == 1024 && DATA_WIDTH ==  8) ? 11 :

					   (DENSITY ==   64 && DATA_WIDTH == 16) ?  8 :
					   (DENSITY ==  128 && DATA_WIDTH == 16) ?  9 :
					   (DENSITY ==  256 && DATA_WIDTH == 16) ?  9 :
					   (DENSITY ==  512 && DATA_WIDTH == 16) ? 10 :
					   (DENSITY == 1024 && DATA_WIDTH == 16) ? 10 : -1;
   parameter 		  TOT_ADDR_WIDTH = ROW_ADDR_WIDTH + COL_ADDR_WIDTH + 2;
   parameter 		  RAM_SIZE       = (1<<TOT_ADDR_WIDTH);
					   
/****************************************************************************************/

   // This is the Memory Array for the Model. The user can access this memory through Carbon's
   // memory interface. The address for the memory is orginized as follows:
   // {bank_address, row_address, column_address} The sizes of the row address and column address
   // is dependent on model configuration. See ROW_ADDR_WIDTH and COL_ADDR_WIDTH parameters above.
   reg [DATA_WIDTH-1:0]   ram_array[0:RAM_SIZE-1]; // carbon observeSignal

`protect
   // carbon license crbn_vsp_exec_xtor_ddrsdram 

   // When Data Width is 16, low data is 7:0, otherwise the low data holds all the bits
   parameter 		  LOW_DATA_WIDTH = (DATA_WIDTH == 16) ? 8 : DATA_WIDTH;
   
   reg [ROW_ADDR_WIDTH-1:0]   bank0_row_addr;
   reg [ROW_ADDR_WIDTH-1:0]   bank1_row_addr;
   reg [ROW_ADDR_WIDTH-1:0]   bank2_row_addr;
   reg [ROW_ADDR_WIDTH-1:0]   bank3_row_addr;
   reg [COL_ADDR_WIDTH-1:0]   col_addr;
   
   reg [13:0] 		      mode_register;
   reg [DATA_WIDTH-1:0]       Dq_dm, Dq_out;
   
   reg 			      precharged_bank0;
   reg 			      precharged_bank1;
   reg 			      precharged_bank2;
   reg 			      precharged_bank3;
   
   wire [TOT_ADDR_WIDTH-1:0]  enable_addr;
   
   reg [15:0] 		      access_errors;   // This is pulsed!
   // bit 0 - illegal cas latency in mode register
   // bit 1 - illegal burst length in mode register
   // bit 2 - access of mode (or extended mode) register when all banks not precharged
   // bit 3 - activate to a bank that is not precharged.
   // bit 4 - burst terminate of an access with auto precharge.
   // bit 5 - access to an inactive bank.
   // bit 6 - interrupt of access with auto precharge.
   // bit 7 - activate occurred before mode register written.
   // bit 8 - activate occurred before extended mode register written.
   
   reg [7:0] 		      reset_counter;
   reg 			      reset;
   
   reg [LOW_DATA_WIDTH-1:0] 		      first_data_low;
   reg 			      first_byte_enable_low;
   reg [7:0] 		      first_data_high;
   reg 			      first_byte_enable_high;
   reg [LOW_DATA_WIDTH-1:0] 		      write_data_second_low;
   reg [LOW_DATA_WIDTH-1:0] 		      write_data_first_low;
   reg 			      write_first_enable_low;
   reg 			      write_second_enable_low;
   reg [7:0] 		      write_data_second_high;
   reg [7:0] 		      write_data_first_high;
   reg 			      write_first_enable_high;
   reg 			      write_second_enable_high;
   wire [DATA_WIDTH-1:0]      write_data;
   wire [DATA_WIDTH-1:0]      write_data_next;
   wire [3:0] 		      write_byte_enable;
   reg  		      wr_data_avail;
   reg  		      wr_data_detect;
   
   wire 		      active_enable;  
   wire 		      write_enable;
   wire 		      read_enable;
   wire [ADDR_WIDTH-1:0]      row_addr;
   
   wire 		      set_read_data_enable;
   wire 		      tristate_reset;
   wire 		      dqs_enable;
   
   wire [TOT_ADDR_WIDTH-1:0]  wr_address0;
   wire [TOT_ADDR_WIDTH-1:0]  wr_address1;
   reg [TOT_ADDR_WIDTH-1:0]   start_addr;
   
   wire 		      set_read_dqs_enable;
   
   reg 			      mode_register_written;
   reg 			      extended_mode_register_written;
   
   // Commands Decode
   wire 		      ACTIVE_ENABLE        =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h13);
   wire 		      AUTOREF_ENABLE       =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h11);   // Ignored
   wire 		      BURST_TERMINATE      =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h16);
   wire 		      EXTENDED_MODE_ENABLE = (({cke,cs_n,ras_n,cas_n,we_n} == 5'h10) && (ba[1:0] == 2'b1));
   wire 		      MODE_REGISTER_ENABLE = (({cke,cs_n,ras_n,cas_n,we_n} == 5'h10) && (ba[1:0] == 2'b0));
   wire 		      PRECHARGE_ENABLE     =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h12);
   wire 		      READ_ENABLE          =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h15);
   wire 		      WRITE_ENABLE         =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h14);
   
   // Burst Length Decode
   wire 		      BURST_LENGTH_2 = (mode_register[2:0] == 3'b001);
   wire 		      BURST_LENGTH_4 = (mode_register[2:0] == 3'b010);
   wire 		      BURST_LENGTH_8 = (mode_register[2:0] == 3'b011);
   
   // CAS Latency Decode
   wire 		      CAS_LATENCY_2  = (mode_register[6:4] == 3'b010);
   wire 		      CAS_LATENCY_25 = (mode_register[6:4] == 3'b110);
   wire 		      CAS_LATENCY_3  = (mode_register[6:4] == 3'b011);

   // Burst Type Decode
   wire                       SEQUENTIAL_BURST  = (mode_register[3] == 1'b0);
   wire                       INTERLEAVED_BURST = (mode_register[3] == 1'b1);

   wire 		      cmd_valid;
   wire 		      rd_cmd;
   wire 		      wr_cmd;
   wire [DATA_WIDTH-1:0]      read_data;
   wire [DATA_WIDTH-1:0]      read_data_next;
   
   wire [TOT_ADDR_WIDTH-1:0]  cmd_wr_addr0;
   wire [TOT_ADDR_WIDTH-1:0]  cmd_wr_addr1;
   wire 		      cmd_wr_en;
   reg 			      wr_active;
   reg 			      rd_active;
   reg [3:0] 		      burst_cnt;
   reg 			      auto_precharge;
   
   // Calculate Column Address
   integer 		  i;
   initial col_addr <= 0;
   always @(addr)
   begin
      // Address 10 is never used for columns address, so skip.
      for(i = 0; i < COL_ADDR_WIDTH; i = i + 1)
	col_addr[i] <= (i < 10) ? addr[i] : addr[i+1];
   end

   // Reset Logic
   initial begin
      reset_counter <= 0;
   end
   assign tristate_reset = (reset_counter != 8'hff);

   always @ (posedge clk) begin
      begin
         if(reset_counter != 8'hff) reset_counter <= reset_counter + 8'h1;

         if(reset_counter == 8'hff) reset <= 1'b0;
         else if(reset_counter <= 8'h01) reset <= 1'b0;
         else reset <= 1'b1;
      end
   end


   /* The following latches the data, byte_enables, and address for writes  */
   /* This is lined up for the next clock edge */
   /* If the dqs is being driven by a read the results are ignored!  */

   always @ (posedge dqs[0]) begin
      first_data_low <= dq[DATA_WIDTH-1:0];
      first_byte_enable_low <= ~dm[0];
   end

   // Not used unless datawidth = 16 
   always @ (posedge dqs[DQS_WIDTH-1]) begin
      if(DATA_WIDTH == 16) begin
	 first_data_high <= dq[DATA_WIDTH-1:DATA_WIDTH/2];
	 first_byte_enable_high <= ~dm[DQS_WIDTH-1];
      end
   end
   
   always @ (negedge dqs[0]) begin
      write_data_second_low <= dq[DATA_WIDTH-1:0];
      write_data_first_low <= first_data_low;
      write_first_enable_low <= first_byte_enable_low;
      write_second_enable_low <= ~dm[0];
   end

   // Not used unless datawidth = 16 
   always @ (negedge dqs[DQS_WIDTH-1]) begin
      if(DATA_WIDTH == 16) begin
	 write_data_second_high <= dq[DATA_WIDTH-1:DATA_WIDTH/2];
	 write_data_first_high <= first_data_high;
	 write_first_enable_high <= first_byte_enable_high;
	 write_second_enable_high <= ~dm[DQS_WIDTH-1];
      end
   end

   // Detect Writes. wr_data_avail is not a level sensitive variable, but change sensitive
   // Every time wr_data_avail changes we have a new write data available 
   always @ (negedge dqs[0] or posedge reset) begin
     if(reset) wr_data_avail <= 0;
     else if(!dqs_enable && cmd_valid && wr_cmd) wr_data_avail <= ~wr_data_avail;
   end

   // For Data Widths other than 16 the "low" data hold all the data bits
   // write_data and write_data_next is sized to the correct data width, so
   // the high bits are thrown away, when data_width is not 16.
   assign write_byte_enable = {write_second_enable_high,write_second_enable_low,
                               write_first_enable_high,write_first_enable_low};
   assign write_data = {write_data_first_high,write_data_first_low};
   assign write_data_next = {write_data_second_high,write_data_second_low};

   /* The following are the signals for the first stuff sent to the RAM  */
   /* This activates the cache */
   assign enable_addr = {ba,row_addr,col_addr};  // 2+13+9 = 24
   assign active_enable = (ACTIVE_ENABLE) ? 1'b1 : 1'b0;
   assign write_enable = (WRITE_ENABLE) ? 1'b1 : 1'b0;
   assign read_enable = (READ_ENABLE) ? 1'b1 : 1'b0;
   assign row_addr = (ba == 2'b00) ? bank0_row_addr : 
		     (ba == 2'b01) ? bank1_row_addr : 
		     (ba == 2'b10) ? bank2_row_addr : 
		     (ba == 2'b11) ? bank3_row_addr : 0;
      
   /* 
    * Read/Write Statmachine 
    */

   assign 		     cmd_wr_en    = wr_active || rd_active;
   assign 		     cmd_wr_addr0 = (wr_active || rd_active) ? calc_addr(start_addr, burst_cnt) : 0;
   assign 		     cmd_wr_addr1 = (wr_active || rd_active) ? calc_addr(start_addr, burst_cnt+1) : 0;
   
   assign set_read_data_enable = (rd_cmd && cmd_valid)  && !reset && !tristate_reset;
   assign set_read_dqs_enable  = (read_enable || (rd_cmd && cmd_valid)) && !reset && !tristate_reset;

   // Auto Precharge Help Variables
   wire   bank0_tobe_precharged = auto_precharge && (start_addr[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2] == 2'b00);
   wire   bank1_tobe_precharged = auto_precharge && (start_addr[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2] == 2'b01);
   wire   bank2_tobe_precharged = auto_precharge && (start_addr[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2] == 2'b10);
   wire   bank3_tobe_precharged = auto_precharge && (start_addr[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2] == 2'b11);
   
   always @(posedge clk) begin
      if(reset) begin
	 burst_cnt   <= 0;
	 wr_active      <= 0;
	 rd_active      <= 0;
	 auto_precharge <= 0;

         access_errors <= 16'b0;
	 
         mode_register <= 14'b0;
         mode_register_written <= 1'b0;
         extended_mode_register_written <= 1'b0;

         bank0_row_addr <= 14'b0;
         bank1_row_addr <= 14'b0;
         bank2_row_addr <= 14'b0;
         bank3_row_addr <= 14'b0;
	 
      end
      else begin
	 if(WRITE_ENABLE || READ_ENABLE) begin
	    burst_cnt   <= 0;
	    wr_active      <= WRITE_ENABLE;
	    rd_active      <= READ_ENABLE;
	    start_addr  <= enable_addr;
	    auto_precharge <= addr[10];
	 end
	 else if( (wr_active || rd_active) &&
		  ((BURST_LENGTH_2 && burst_cnt == 0) ||
		   (BURST_LENGTH_4 && burst_cnt == 2) ||
		   (BURST_LENGTH_8 && burst_cnt == 6) ||
		   ((PRECHARGE_ENABLE & addr[10]) ||
		    (PRECHARGE_ENABLE && start_addr[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2] == ba) || 
		    BURST_TERMINATE)))
	   begin
	      wr_active      <= 0;
	      rd_active      <= 0;
	      burst_cnt   <= 0;
	      auto_precharge <= 0;

	      // If Auto Precharge is desired perform that now
	      case(start_addr[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2])
		2'b00 : precharged_bank0 <= 1'b1;
		2'b01 : precharged_bank1 <= 1'b1;
		2'b10 : precharged_bank2 <= 1'b1;
		2'b11 : precharged_bank3 <= 1'b1;
	      endcase // case(start_addr[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2])
	 
	   end
	 else if(wr_active || rd_active) begin
	    // Increment Burst Count
	    burst_cnt <= burst_cnt + 2;
	 end	 

	 /* Handle auto precharge reads and writes */
         if(read_enable || write_enable) begin
	    /* Can't interrupt an autoprecharge read or write */
            if((ba == 2'b00) && (bank0_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
            if((ba == 2'b01) && (bank1_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
            if((ba == 2'b10) && (bank2_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
            if((ba == 2'b11) && (bank3_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
         end

         if(BURST_TERMINATE) begin
	    /* Can't interrupt an autoprecharge read or write */
            if((ba == 2'b00) && (bank0_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
            if((ba == 2'b01) && (bank1_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
            if((ba == 2'b10) && (bank2_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
            if((ba == 2'b11) && (bank3_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
         end

         if(PRECHARGE_ENABLE) begin
            if((ba == 2'b00) && (bank0_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
            if((ba == 2'b01) && (bank1_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
            if((ba == 2'b10) && (bank2_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
            if((ba == 2'b11) && (bank3_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
            if(addr[10] && ((bank0_tobe_precharged == 1'b1) ||
                            (bank1_tobe_precharged == 1'b1) || 
                            (bank2_tobe_precharged == 1'b1) ||
                            (bank3_tobe_precharged == 1'b1))) access_errors[6] <= 1'b1;
            if(addr[10] == 1'b0) begin
               if(ba == 2'b00) precharged_bank0 <= 1'b1; 
               else if(ba == 2'b01) precharged_bank1 <= 1'b1; 
               else if(ba == 2'b10) precharged_bank2 <= 1'b1; 
               else precharged_bank3 <= 1'b1;
            end
            else begin  // precharge them all
               precharged_bank0 <= 1'b1;
               precharged_bank1 <= 1'b1;
               precharged_bank2 <= 1'b1;
               precharged_bank3 <= 1'b1;
            end
         end

         // Activate Bank - Catch the bank's row address
         if (ACTIVE_ENABLE) begin

	    /* Detect an illegal cas latency  */
            if(!(CAS_LATENCY_2) && !(CAS_LATENCY_25) && !(CAS_LATENCY_3)) begin
               access_errors[0] <= 1'b1;
            end

            if(!(BURST_LENGTH_2) && !(BURST_LENGTH_4) && !(BURST_LENGTH_8)) ////begin
            access_errors[1] <= 1'b1;

            if(mode_register_written != 1'b1) access_errors[7] <= 1'b1;

            if(extended_mode_register_written != 1'b1) access_errors[8] <= 1'b1;

            if (ba == 2'b00 && precharged_bank0 == 1'b1) begin
               precharged_bank0 <= 1'b0;
               bank0_row_addr <= addr;
            end else if (ba == 2'b01 && precharged_bank1 == 1'b1) begin
               precharged_bank1 <= 1'b0;
               bank1_row_addr <= addr;
            end else if (ba == 2'b10 && precharged_bank2 == 1'b1) begin
               precharged_bank2 <= 1'b0;
               bank2_row_addr <= addr;
            end else if (ba == 2'b11 && precharged_bank3 == 1'b1) begin
               precharged_bank3 <= 1'b0;
               bank3_row_addr <= addr;
            end
	    /* Must not be precharged! This is an error. */
            else access_errors[3] <= 1'b1;
	 end // if (ACTIVE_ENABLE)
	 
         if (read_enable || write_enable) begin
            if ((ba == 2'b00 && precharged_bank0 == 1'b1) ||
                (ba == 2'b01 && precharged_bank1 == 1'b1) ||
                (ba == 2'b10 && precharged_bank2 == 1'b1) ||
                (ba == 2'b11 && precharged_bank3 == 1'b1)) begin
               // Can't access an inactive bank.
               access_errors[5] <= 1'b1;
            end
         end

         // Extended Mode Register
         if (EXTENDED_MODE_ENABLE) begin
            if (precharged_bank0 != 1'b1 || 
                precharged_bank1 != 1'b1 || 
                precharged_bank2 != 1'b1 ||
                precharged_bank3 != 1'b1) begin
               //  All banks must be precharged prior to this.
               access_errors[2] <= 1'b1;
            end
            else extended_mode_register_written <= 1'b1;
         end
         
         // Load Mode Register
         if (MODE_REGISTER_ENABLE) begin
            // Decode DLL, CAS Latency, Burst Type, and Burst Length
            if (precharged_bank0 == 1'b1 &&
                precharged_bank1 == 1'b1 &&
                precharged_bank2 == 1'b1 &&
                precharged_bank3 == 1'b1) begin
               mode_register <= addr;
               mode_register_written <= 1'b1;
            end else begin
               //  All banks must be precharged prior to this.
               access_errors[2] <= 1'b1;
            end
	 end

      end // else: !if(reset)
   end // always @ (posedge clk)

   // Access the Ram Array
   always @(posedge clk or posedge reset)
     if(reset) begin
	wr_data_detect <= 0;
     end
     else if(cmd_valid) begin
	
	// Write to the Ram Array. A new write data is available when wr_data_avail is different that wr_data_detect
	// Once we have registered the new data we update wr_data_detect, so we can detect when wr_data_avail changes again.
	if( (wr_data_avail ^ wr_data_detect) && wr_cmd) begin
	   wr_data_detect <= wr_data_avail;
	   write_ram(wr_address0, write_data, write_byte_enable[1:0]);
	   write_ram(wr_address1, write_data_next, write_byte_enable[3:2]);
	end
     end

   // Read From Ram Array
   assign read_data      = (cmd_valid && rd_cmd) ? ram_array[wr_address0] : 0;
   assign read_data_next = (cmd_valid && rd_cmd) ? ram_array[wr_address1] : 0;
   
   // Command Fifo
   carbonx_ddrsdram_simple_fifo #(TOT_ADDR_WIDTH*2+2) 
     wr_cmd_fifo (.reset(reset),
		  .wclk(clk),
		  .we(cmd_wr_en),
		  .datain({rd_active, wr_active, cmd_wr_addr1, cmd_wr_addr0}),
		  .rclk(clk),
		  .re( (wr_data_avail ^ wr_data_detect) || rd_cmd),
		  .dataout({rd_cmd, wr_cmd, wr_address1, wr_address0}),
		  .valid_out(cmd_valid));
   
   // ------------------------------------------------
   	 

   carbonx_ddr_data_out_control #(DATA_WIDTH) ddr_data_out_control
     (
      .clk(clk),  // actual ddr RAM input
      .clk_n(clk_n),  // actual ddr RAM input
      .mode_register(mode_register),
      .read_data(read_data),
      .read_data_next(read_data_next),
      .data_bidirect_1(dq),   // actual ddr RAM output - for 2   cycle latency
      .data_bidirect_2(dq),   // actual ddr RAM output - for 2.5 cycle latency
      .data_bidirect_3(dq),   // actual ddr RAM output - for 3   cycle latency
      .dqs_bidirect(dqs),     // actual ddr RAM output
      .set_read_data_enable(set_read_data_enable),
      .set_read_dqs_enable(set_read_dqs_enable),
      .reset(reset),
      .tristate_reset(tristate_reset),
      .dqs_enable(dqs_enable)
      );

   task write_ram;
      input [TOT_ADDR_WIDTH-1:0] addr;
      input [15:0]               data;
      input [1:0]                be;

      reg [15:0] wr_data;
      reg [15:0] wr_mask;
      begin
	 wr_mask[15:8] = be[1] ? 'hff : 'h00;
	 wr_mask[ 7:0] = be[0] ? 'hff : 'h00;
	 wr_data = (ram_array[addr] & ~wr_mask) | (data & wr_mask);

	 // Write Data to Ram Array
	 //$display("Writing to addr %h, data %h. old data = %h, be = %h, wr_mask = %h", addr, wr_data, ram_array[addr], be, wr_mask);
	 ram_array[addr] = wr_data;
	 //$display("After writing: ram_array[%h] = %h", addr, ram_array[addr]);
      end
   endtask // write_ram
   
   function [TOT_ADDR_WIDTH-1:0] calc_addr;
      input [TOT_ADDR_WIDTH-1:0] start;
      input [2:0]                burst_cnt;
      
      reg [TOT_ADDR_WIDTH-1:0]   mask;
      reg [2:0]                  offset;
      
      begin
	 // Calculate how many address bits there are in the block
	 if(BURST_LENGTH_2)      mask = 'b001;
	 else if(BURST_LENGTH_4) mask = 'b011;
	 else if(BURST_LENGTH_8) mask = 'b111;
	 else                    mask = 'b000;
	 
	 // Now Calculate the block address
	 if(SEQUENTIAL_BURST)       offset = (start + burst_cnt) & mask;
	 else if(INTERLEAVED_BURST) offset = (start ^ burst_cnt) & mask;
	 else                       offset = 0;
	 
	 // Construct the final address
	 calc_addr = (start & ~mask) | offset;
      end
   endfunction // calc_addr
   
endmodule

/* The following module is used to control the data and dqs outputs during reads */
/* Much of the logic is only used during 2.5 cycle latency */
module carbonx_ddr_data_out_control (
				     clk,
				     clk_n,
				     mode_register,
				     read_data,
				     read_data_next,
				     data_bidirect_1,
				     data_bidirect_2,
				     data_bidirect_3,
				     dqs_bidirect,
				     set_read_data_enable,
				     set_read_dqs_enable,
				     reset,
				     tristate_reset,
				     dqs_enable);

   parameter DATA_WIDTH = -1;
   parameter DQS_WIDTH = (DATA_WIDTH == 16) ? 2 : 1;
   
   input clk;
   input clk_n;
   input [13:0] mode_register;
   input [DATA_WIDTH-1:0] read_data;
   input [DATA_WIDTH-1:0] read_data_next;
   input 	set_read_data_enable;
   input 	set_read_dqs_enable;
   input 	reset;
   input 	tristate_reset;
   output [DATA_WIDTH-1:0] data_bidirect_1;
   output [DATA_WIDTH-1:0] data_bidirect_2;
   output [DATA_WIDTH-1:0] data_bidirect_3;
   output [DQS_WIDTH-1:0]  dqs_bidirect;
   output        dqs_enable;

   reg 		 read_data_enable;
   reg 		 delayed_read_data_enable;  
   reg 		 read_dqs_enable;
   reg 		 delayed_read_dqs_enable;
   reg [DATA_WIDTH-1:0] 	 delayed_read_data;
   reg [DATA_WIDTH-1:0] 	 delayed_read_data_next; 
   reg [DATA_WIDTH-1:0] 	 read_data_1;
   reg [DATA_WIDTH-1:0] 	 read_data_2;
   reg [DATA_WIDTH-1:0] 	 read_data_next_1;
   reg [DATA_WIDTH-1:0] 	 read_data_next_2;
   reg 		 was_set_read_data_enable;
   reg 		 was_set_read_dqs_enable;
   reg 		 rd_out;
   reg 		 rd_out_next;
   reg 		 delayed_rd_out;
   reg 		 delayed_rd_out_next;

   wire 	 dqs_internal;
   wire 	 dqs_enable;
   wire 	 two_point_five_cycle;
   wire 	 two_cycle;
   wire 	 three_cycle;
   
   
   assign 	 #1 data_bidirect_1 = (two_cycle && read_data_enable) ? ( !(rd_out^rd_out_next) ? read_data_1 : read_data_next_1) : 16'hzzzz;
   assign 	 #1 data_bidirect_2 = delayed_read_data_enable ? ( !(delayed_rd_out^delayed_rd_out_next) ? delayed_read_data : delayed_read_data_next) : 16'hzzzz;
   assign 	 #1 data_bidirect_3 = (three_cycle && read_data_enable) ? ( !(rd_out^rd_out_next) ? read_data_2 : read_data_next_2) : 16'hzzzz;
   assign 	 #1 dqs_bidirect = dqs_enable ? {dqs_internal,dqs_internal} : 2'bzz;

   assign 	 two_cycle = (mode_register[6:4] == 3'b010) ? 1'b1 : 1'b0;

   assign 	 two_point_five_cycle = (mode_register[6:4] == 3'b110) ? 1'b1 : 1'b0;

   assign 	 three_cycle = (mode_register[6:4] == 3'b011) ? 1'b1 : 1'b0;

   assign 	 dqs_enable = (two_cycle || three_cycle) ? read_dqs_enable : delayed_read_dqs_enable;


   always @ (posedge clk)
     if (reset)
       rd_out <= 0;
     else if (rd_out)
       rd_out <= 0;  
     else if (read_data_enable)
       rd_out <= 1;
   

   always @ (negedge clk)
     if (reset)
       rd_out_next <= 0;
     else if (rd_out_next)
       rd_out_next <= 0;  
     else if (read_data_enable)
       rd_out_next <= 1;


   always @ (negedge clk)
     if (reset)
       delayed_rd_out_next <= 0;
     else if (delayed_rd_out_next)
       delayed_rd_out_next <= 0;  
     else if (delayed_read_data_enable)
       delayed_rd_out_next <= 1;

   
   always @ (posedge clk)
     if (reset)
       delayed_rd_out <= 0;
     else if (delayed_rd_out)
       delayed_rd_out <= 0;  
     else if (delayed_read_data_enable)
       delayed_rd_out <= 1;
   

   
   always @ (negedge clk or posedge tristate_reset) begin
      if(tristate_reset) begin
	 delayed_read_data_enable <= 1'b0;
	 delayed_read_dqs_enable <= 1'b0;
	 
	 delayed_read_data <= 0;
	 delayed_read_data_next <= 0;    
      end
      else begin
	 delayed_read_data_enable <= two_point_five_cycle && was_set_read_data_enable;
	 delayed_read_dqs_enable <= was_set_read_dqs_enable;
	 
	 delayed_read_data <= read_data_1;
	 delayed_read_data_next <= read_data_next_1;
      end
   end

   
   always @ (posedge clk or posedge tristate_reset) begin
      if(tristate_reset) begin
	 read_data_enable <= 1'b0;
	 read_dqs_enable <= 1'b0;
	 
	 read_data_1 <= 0;
	 read_data_2 <= 0;
	 read_data_next_1 <= 0;
	 read_data_next_2 <= 0;
	 was_set_read_data_enable <= 0;    
      end
      else begin
	 read_data_enable <= (two_cycle && set_read_data_enable) || (three_cycle && was_set_read_data_enable);
	 read_dqs_enable <= (two_cycle && set_read_dqs_enable) || (three_cycle && was_set_read_dqs_enable);
	 
	 read_data_1 <= read_data;
	 read_data_2 <= read_data_1;
	 read_data_next_1 <= read_data_next;
	 read_data_next_2 <= read_data_next_1;
	 was_set_read_data_enable <= set_read_data_enable;
	 was_set_read_dqs_enable <= set_read_dqs_enable;
      end
   end
   

   assign dqs_internal = (two_cycle || three_cycle) ? (read_data_enable && !(rd_out^rd_out_next) ? 1 : 0 ) :
                         (delayed_read_data_enable && !(delayed_rd_out^delayed_rd_out_next) ? 1 : 0);

endmodule

module carbonx_ddrsdram_simple_fifo 
  (
   reset,
   wclk,
   datain,
   we,
   rclk,
   dataout,
   re,
   valid_out);

   parameter               DATA_WIDTH = 32;

   input 		   reset;

   // Write Port
   input 		   wclk;
   input [DATA_WIDTH-1:0]  datain;
   input                   we;

   // Read Port
   input                   rclk;
   input                   re;
   output [DATA_WIDTH-1:0] dataout;
   output                  valid_out;
   
   // Write Command Fifo Signals
   reg [DATA_WIDTH-1:0]    fifo[0:3];
   reg [1:0] 		   wr_ptr;
   reg [1:0] 		   rd_ptr;
   
   // Write Command Fifo
   always @(posedge wclk or posedge reset)
      if(reset) wr_ptr <= 0;
      else if(we) begin 
	 fifo[wr_ptr] <= datain;
	 wr_ptr = wr_ptr + 1;
      end

   // Increment Read Pointer on every posedge of rclk unless fifo is empty
   always @(posedge rclk or posedge reset)
      if(reset) rd_ptr <= 0;
      else if(wr_ptr != rd_ptr && re) rd_ptr = rd_ptr + 1;

   assign dataout   = fifo[rd_ptr];
   assign valid_out = (wr_ptr != rd_ptr);
   
endmodule // carbonx_ddrsdram_simple_fifo

`endprotect
