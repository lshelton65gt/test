//*****************************************************************************
// DDR SDRAM Controller
//*****************************************************************************
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


module carbonx_ddrsdram_ctrl (
		              clk,            // System signals
		              reset_n,
		              
		              dq,             // DDR SDRAM interface
		              dqs, 
		              addr, 
		              ba, 
		              cke, 
		              cs_n,           // COMMAND[3] 
		              cas_n,          // COMMAND[1]
		              ras_n,          // COMMAND[2]
		              we_n,           // COMMAND[0]
		              dm);
   
   
   
   
   //**************************************************************************
   //                      Parameter Defines
   //**************************************************************************
   
   // Memory Density in Megabits
   parameter DENSITY    = 256;

   // Data Width in bits
   parameter DATA_WIDTH = 16;

   // These parameters are used a a coding aid and should
   // not be specified by instantiation of the module
   parameter ADDR_WIDTH = (DENSITY ==  64 || DENSITY == 128) ? 12 :
			  (DENSITY == 256 || DENSITY == 512) ? 13 :
			  (DENSITY == 1024) ? 14 : -1;
   parameter DQS_WIDTH  = (DATA_WIDTH == 16) ? 2 : 1;
   
   // Protected Parameters, The user should not set these parameters directly
   parameter 		  ROW_ADDR_WIDTH = ADDR_WIDTH;
   parameter              COL_ADDR_WIDTH = (DENSITY ==   64 && DATA_WIDTH ==  4) ? 10 :
					   (DENSITY ==  128 && DATA_WIDTH ==  4) ? 11 :
					   (DENSITY ==  256 && DATA_WIDTH ==  4) ? 11 :
					   (DENSITY ==  512 && DATA_WIDTH ==  4) ? 12 :
					   (DENSITY == 1024 && DATA_WIDTH ==  4) ? 12 :

					   (DENSITY ==   64 && DATA_WIDTH ==  8) ?  9 :
					   (DENSITY ==  128 && DATA_WIDTH ==  8) ? 10 :
					   (DENSITY ==  256 && DATA_WIDTH ==  8) ? 10 :
					   (DENSITY ==  512 && DATA_WIDTH ==  8) ? 11 :
					   (DENSITY == 1024 && DATA_WIDTH ==  8) ? 11 :

					   (DENSITY ==   64 && DATA_WIDTH == 16) ?  8 :
					   (DENSITY ==  128 && DATA_WIDTH == 16) ?  9 :
					   (DENSITY ==  256 && DATA_WIDTH == 16) ?  9 :
					   (DENSITY ==  512 && DATA_WIDTH == 16) ? 10 :
					   (DENSITY == 1024 && DATA_WIDTH == 16) ? 10 : -1;
   parameter 		  TOT_ADDR_WIDTH = ROW_ADDR_WIDTH + COL_ADDR_WIDTH + 2;

   parameter     UD        = 1;
   parameter     ND        = 0;
   parameter     HI        = 1'b1;
   parameter     LO        = 1'b0;
   
   parameter     BFMid     = 1;
   parameter     ClockId   = 1;
   parameter     DataWidth = 64;
   parameter     DataAdWid = 10;
   parameter     CSDWidth  = 32;
   parameter     GCSwid    = 32;
   parameter     PCSwid    = 32;
   
   //define      COMMAND     {cs_n, ras_n, cas_n, we_n}
   
   parameter     INACTIVE    = {HI,   HI,   HI,   HI};
   parameter     NOP         = {LO,   HI,   HI,   HI};
   parameter     ACTIVE      = {LO,   LO,   HI,   HI};
   parameter     READ        = {LO,   HI,   LO,   HI};
   parameter     WRITE       = {LO,   HI,   LO,   LO};
   parameter     PRECHARGE   = {LO,   LO,   HI,   LO};
   parameter     AUTOREFRESH = {LO,   LO,   LO,   HI};
   parameter     LDMODE      = {LO,   LO,   LO,   LO};
   
   
   
   //***************************************************************************
   //                      Ports Definitions
   //***************************************************************************  
   
   output        clk; 
   input         reset_n;
   
   inout [DATA_WIDTH-1:0]  dq;
   inout [DQS_WIDTH-1:0]   dqs;
   
   output [ADDR_WIDTH-1:0] addr;
   output [1:0] 	   ba;
   output 		   cke;
   output 		   cs_n;
   output 		   cas_n;
   output 		   ras_n;
   output 		   we_n;
   output [DQS_WIDTH-1:0]  dm;
   
   //***************************************************************************
   //                      Clocking
   //  ddr_clk is twice the speed of the clock that runs from controller to
   //    memory (clk).
   //***************************************************************************  
   
   wire          ddr_clk;             // carbon depositSignal
   reg           clk;           
   initial clk = 0;
   always @(posedge ddr_clk)
     clk = ~clk;
     
`protect
   // carbon license crbn_vsp_exec_xtor_ddrsdram
   
   //
   // Maintainer: Andy Zhang <zhang@zaiqtech.com>
   //
   // Filename: ddr_ctlr.v
   //
   // $Id$
   //
   // Revision 1.2  2006/01/17 03:37:33  jstein
   // Reviewed by:  John Hoglund
   // Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
   // Bugs Affected: DW02_prod_sum1 sign extension
   // Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
   // removed all `defines from xactor verilog (mostly usb)
   // added test/xactors/usb20
   //
   // Revision 1.1  2005/11/22 16:49:06  knutson
   // Reviewed by: Jim Stein
   // Tests Done: checkin
   // Bugs Affected:
   //
   // Added Source Code for DDR SDRAM transactor
   // Renamed API functions for DDR SDRAM according to decided upon naming convention.
   // Updated DDR SDRAM example.
   //
   // Updated API for PCI slave, to allow for user written slave function.
   // Update PCI example to use the carbonx_pci_target transactor as slave.
   //
   // Revision 1.3  2005/10/20 18:23:28  jstein
   // Reviewed by: Dylan Dobbyn
   // Tests Done: scripts/checkin + xactor regression suite
   // Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
   // removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
   // removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
   // added html/xactors dir, and html/xactors/adding_xactors.txt doc
   //
   // Revision 1.2  2005/09/29 23:53:43  jstein
   // Reviewed by: Dylan Dobbyn
   // Tests Done:  checkin, xactor regression
   // Bugs Affected: none
   //  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
   //  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
   //  so they will not be seen by the customer
   //  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
   //
   // Revision 1.1  2005/09/25 16:58:31  jstein
   // Reviewed by: none
   // Tests Done: usb2.0 xactor test
   // Bugs Affected: Adding Verilog unprotected source code to sandbox
   //
   // Revision 1.2  2005/07/01 14:07:12  colby_w
   // ddr_16megx16.v
   //
   // Revision 1.1  2003/12/04 17:06:19  zhang
   // initial revision.
   //
   //
   //*****************************************************************************

   //***************************************************************************
   //                      Include C Interface File
   //***************************************************************************  
   
`include "xif_core.vh"
   
   
   
   //**************************************************************************
   //                      Internal Reg/Wire Signals 
   //**************************************************************************
   
   reg [ADDR_WIDTH-1:0]    addr;
   reg [1:0]     ba;
   
   reg [3:0]     COMMAND;
   
   wire          cs_n   = COMMAND[3];
   wire          ras_n  = COMMAND[2];
   wire          cas_n  = COMMAND[1];
   wire          we_n   = COMMAND[0];
   
   reg [DQS_WIDTH-1:0] dm;
   wire          cke;
   
   
   reg           xrun;
   reg [127:0]   wr_data;
   reg [15:0]    cyc_end;
   reg [15:0]    cyc_cnt_pos;
   reg [15:0]    cyc_cnt_neg;
   reg [31:0]    address;
   reg           ddr_wr;
   reg           ddr_rd;  
   reg           ddr_init;
   reg           ddr_rdy;  
   reg           wr_drv_data;
   reg [15:0]    rd_da_0;
   reg [15:0]    rd_da_1;
   reg [15:0]    rd_da_2;
   reg [15:0]    rd_da_3;
   reg [15:0]    rd_da_4;
   reg [15:0]    rd_da_5;
   reg [15:0]    rd_da_6;
   reg [15:0]    rd_da_7;
   reg           wr_pos_valid;
   reg           wr_neg_valid;
   reg [15:0]    wr_pos_cnt;
   reg [15:0]    wr_neg_cnt;
   reg           read_data_ready;
   reg [3:0]     burst_len;
   reg [9:0]     gp_daddr;
   reg [1:0]     dqs_reg;
   reg [DQS_WIDTH-1:0]     dqs_phase;
   reg [3:0]     dqs_cnt;
   
   
   wire          reset;
   wire [127:0]  rd_data;

   // Cycle times for write data to go on DQ for minimum Command to DQS timing.
   //   FIXME -- add a CS register to change the timing to maximum.
   wire [3:0]    WR_DATA_T0 = 7;
   wire [3:0]    WR_DATA_T1 = 8;
   wire [3:0]    WR_DATA_T2 = 9;
   wire [3:0]    WR_DATA_T3 = 10;
   wire [3:0]    WR_DATA_T4 = 11;
   wire [3:0]    WR_DATA_T5 = 12;
   wire [3:0]    WR_DATA_T6 = 13;
   wire [3:0]    WR_DATA_T7 = 14;
   
   // Decode Row and column Address
   wire [ADDR_WIDTH-1:0] row_address = address[ROW_ADDR_WIDTH+COL_ADDR_WIDTH-1:COL_ADDR_WIDTH];
   reg [ADDR_WIDTH-1:0]  col_address;
   integer 		 i;

   // Clear all bits so unused bits in column address is 0
   initial col_address <= 0;

   // Address bit 10 is skipped in column address
   always @(address) for(i = 0; i < COL_ADDR_WIDTH; i = i + 1)
     if(i < 10) col_address[i]   <= address[i];
     else       col_address[i+1] <= address[i];
   
   //***************************************************************************
   //                      Continuous Assignments
   //***************************************************************************
   
   assign        BFM_clock         = ddr_clk;
   assign        BFM_reset         = ~reset_n;
   assign        reset             = BFM_reset | XIF_reset;
   assign        BFM_put_operation = BFM_NXTREQ;               // Always getting next request
   assign        BFM_put_data      = gp_daddr ? rd_data[127:64] : rd_data[63:0];           
   assign        BFM_put_status    = 0;		               // Status always OK
   assign        BFM_put_size      = burst_len * 2;              // In bytes
   assign        BFM_put_dwe       = read_data_ready & ddr_rd;
   assign        BFM_gp_daddr      = gp_daddr;
   assign        BFM_xrun          = xrun;  
   assign        BFM_interrupt     = 0;
   assign        BFM_put_csdata    = 0;
   assign        BFM_put_address   = 0;

   
   assign        cke               = HI;
   assign        rd_data           = {rd_da_7, rd_da_6, rd_da_5, rd_da_4, rd_da_3, rd_da_2, rd_da_1, rd_da_0};
   
   assign        dq                = (!wr_drv_data) ? 16'bz : 
		 (cyc_cnt_neg == WR_DATA_T0) ? wr_data[ 15:  0] : 
		 (cyc_cnt_neg == WR_DATA_T1) ? wr_data[ 31: 16] : 
		 (cyc_cnt_neg == WR_DATA_T2) ? wr_data[ 47: 32] : 
		 (cyc_cnt_neg == WR_DATA_T3) ? wr_data[ 63: 48] : 
		 (cyc_cnt_neg == WR_DATA_T4) ? wr_data[ 79: 64] : 
		 (cyc_cnt_neg == WR_DATA_T5) ? wr_data[ 95: 80] : 
		 (cyc_cnt_neg == WR_DATA_T6) ? wr_data[111: 96] : 
		 (cyc_cnt_neg == WR_DATA_T7) ? wr_data[127:112] : 
                 0 ;
   
   assign        dqs               = (!ddr_wr) ? 2'bzz : dqs_reg;
   
   
   
   
   //***************************************************************************
   //                      Always Loops
   //***************************************************************************
   
   //---------------------------------------------------------------------------
   //                      State Machine for XIF transactor
   //---------------------------------------------------------------------------
   
   always @(posedge ddr_clk or posedge reset) 
     begin
        if (reset)
	  begin
	     ddr_wr    <=  0;
	     ddr_rd    <=  0;
	     ddr_init  <=  0;	  
	     address   <=  0;
	     xrun      <=  0;
	     cyc_end   <=  0;
             cyc_cnt_pos   <=  0;
             burst_len <= 0;
             gp_daddr <= 0;
             read_data_ready <= 0;
	  end
        else
	  begin
	     // --- Communicate With XIF
	     if (XIF_xav) 
	       begin
                  cyc_cnt_pos <= 0;
                  gp_daddr <= 0;
	          cyc_end  <=  (XIF_get_operation == BFM_WRITE) && (XIF_get_address[31:0] != 32'h8000_0000)? (burst_len + 10) :
			        (XIF_get_operation == BFM_WRITE) && (XIF_get_address[31:0] == 32'h8000_0000)? 205*2 :
			        (XIF_get_operation == BFM_READ)                                             ? (burst_len + 12) : 2;
	          address   <=  (XIF_get_operation == BFM_WRITE || XIF_get_operation == BFM_READ) ? XIF_get_address[31:0] : 0; 

	          
	          if (XIF_get_operation == BFM_WRITE) 
		    begin
		       if (XIF_get_address[31:0] == 32'h8000_0000)
		         begin
		            xrun       <=  1;   
		            ddr_rd     <=  0;
		            ddr_wr     <=  0;
		            ddr_init   <=  1;
		         end 
		       else
		         begin
		            xrun       <=  1;   
		            ddr_rd     <=  0;
		            ddr_wr     <=  1;
		            ddr_init   <=  0;
		         end 
		    end
	          else if (XIF_get_operation == BFM_READ)  
		    begin
		       xrun       <=  1;
		       ddr_rd     <=  1;
		       ddr_wr     <=  0;
		       ddr_init   <=  0;		      
		    end 	     
	          else 	     
		    begin	     
		       ddr_wr     <=  0;
		       ddr_rd     <=  0;
		       xrun       <=  0;
		       ddr_init   <=  0;		      
		    end
	       end // if (XIF_xav)
	     else if (~xrun)                                            // --- zero if not running
	       begin
	          ddr_wr         <=  0;
	          ddr_rd         <=  0;
	          ddr_init       <=  0;		      
	          cyc_cnt_pos        <=  0;
	          cyc_end        <=  0;
	          xrun           <=  0;
	       end 
	     else if (ddr_wr || ddr_init)                     // --- Middle cycles
	       begin
	          cyc_cnt_pos        <=  cyc_cnt_pos + 1;
                  if (cyc_cnt_pos == 1)
                    begin
	               wr_data[63:0] <=  XIF_get_data;
                       gp_daddr <= 1;
                    end
                  if (cyc_cnt_pos == 3)
                    begin
                       wr_data[127:64] = XIF_get_data;
                       gp_daddr <= 0;
                    end
                  if ((cyc_cnt_pos != (cyc_end - 1)))
                    begin
                       xrun <= 1;
                    end 
                  else 
                    begin
                       xrun <= 0;
                    end
	       end 
	     else if (ddr_rd)                     // --- Middle cycles
	       begin
	          cyc_cnt_pos        <=  cyc_cnt_pos + 1;
                  if ((dqs_cnt == 4) || (dqs_cnt == 8) || ((burst_len == 2) && (dqs_cnt == 2)))
                    begin
                       read_data_ready <= 1;
                    end
                  else
                    begin
                       read_data_ready <= 0;
                    end
                  if (read_data_ready && ~gp_daddr)
                    begin
                       gp_daddr <= 1;
                    end
                  if (read_data_ready && gp_daddr)
                    begin
                       gp_daddr <= 0;
                    end
                  if ((cyc_cnt_pos != (cyc_end - 1)))
                    begin
                       xrun <= 1;
                    end 
                  else 
                    begin
                       xrun <= 0;
                    end
	       end 
	     
	  end // else: !if(reset)
     end // always @(posedge clk)
   
   
   
   //---------------------------------------------------------------------------
   //                      State Machine for DDR SDRAM TRANSACTOR
   //---------------------------------------------------------------------------
   
   always @(negedge ddr_clk or posedge reset) 
     begin
        if (reset)
	  begin
	     addr       <=  0;
	     ba         <=  0;
             COMMAND    <=  INACTIVE;
	     // cs_n       <=  HI;
	     // ras_n      <=  HI;
	     // cas_n      <=  HI;
	     // we_n       <=  HI;
	     dm         <=  {HI, HI};	  
	     ddr_rdy    <=  LO;	  
	     wr_drv_data    <=  LO;
             cyc_cnt_neg   <=  0;
	  end
        else	
	  begin
	     if (xrun && (ddr_wr || ddr_rd || ddr_init))                     // --- Middle cycles
	       begin
	          cyc_cnt_neg        <=  cyc_cnt_neg + 1;
               end
             else
               begin
	          cyc_cnt_neg        <=  0;
               end
	     // --- DDR Commands
	     if (ddr_init && !ddr_rdy)                            
	       begin
	          if ((cyc_cnt_pos == 1) || (cyc_cnt_pos == 2))
		    begin
		       COMMAND <=  PRECHARGE;		  
		       addr     <=  {2'h0, HI, 10'h000};               
		       ba       <=  0;
		       dm       <=  {HI, HI};
		    end 
	          else if ((cyc_cnt_pos == 7) || (cyc_cnt_pos == 8))
		    begin
		       COMMAND  <=  LDMODE;		  
		       addr     <=  0;       
		       ba       <=  2'b01;
		       dm       <=  {HI, HI};
		    end 
	          else if ((cyc_cnt_pos == 9) || (cyc_cnt_pos == 10))
		    begin
		       COMMAND  <=  LDMODE;		  
		       addr     <=  wr_data[12:0];  // wr_data = {OP_MODE, CAS_LAT, BT, BURST_LEN};
                       case (wr_data[1:0])
                         2'b00: burst_len <= 0;
                         2'b01: burst_len <= 2;
                         2'b10: burst_len <= 4;
                         2'b11: burst_len <= 8;
                       endcase
		       ba       <=  wr_data[14:13];
		       dm       <=  {HI, HI};
		    end 
	          else if ((cyc_cnt_pos == 11) || (cyc_cnt_pos == 12))
		    begin
		       COMMAND  <=  PRECHARGE;		  
		       addr     <=  {2'h0, HI, 10'h000};                
		       ba       <=  0;
		       dm       <=  {HI, HI};
		    end
	          else if ((cyc_cnt_pos == 17) || (cyc_cnt_pos == 18) || (cyc_cnt_pos == 37) || (cyc_cnt_pos == 38))
		    begin
		       COMMAND  <=  AUTOREFRESH;		  
		       addr     <=  0;                
		       ba       <=  0;
		       dm       <=  {HI, HI};
		    end
	          else 
		    begin
		       COMMAND  <=  NOP;		  
		       addr     <=  0;                
		       ba       <=  0;
		       dm       <=  {HI, HI};
		    end
	          if (cyc_cnt_pos == 205*2)
		    ddr_rdy <=  HI;	      
	       end 
	     else if (ddr_rdy) 
	       if ((ddr_wr || ddr_rd) && ((cyc_cnt_pos == 1) || (cyc_cnt_pos == 2)))            
	         begin
		    COMMAND  <=  ACTIVE;
		    addr     <=  row_address;  
		    ba       <=  address[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2];		
		    dm       <=  {HI, HI};
	         end
	       else if (ddr_wr && ((cyc_cnt_pos == 3) || (cyc_cnt_pos == 4)))   
	         begin
		    COMMAND  <=  WRITE;		
		    addr     <=  col_address | {2'h0, HI, 10'h0}; // Auto-Precharge
		    ba       <=  address[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2];		
		    dm       <=  {HI, HI};
	         end	 
	       else if (ddr_rd && ((cyc_cnt_pos == 3) || (cyc_cnt_pos == 4)))    
	         begin
		    COMMAND  <=  READ;		
		    addr     <=  col_address | {2'h0, HI, 10'h0}; // Auto-Precharge
		    ba       <=  address[TOT_ADDR_WIDTH-1:TOT_ADDR_WIDTH-2];
		    dm       <=  {HI, HI};
	         end
               else                                                     
	         begin
		    COMMAND  <=  NOP;		
		    addr     <=  0;
		    ba       <=  0;		
		    dm       <=  ddr_wr ? {LO, LO} : {HI, HI};
	         end

	     // --- When to drive data for writes
	     if (ddr_wr && (cyc_cnt_pos >= 6) && (cyc_cnt_pos <= (5 + burst_len)))
	       wr_drv_data <=  HI;
	     else
	       wr_drv_data <=  LO;
	  end // else: !if(reset)
        
     end // always @ (posedge clk)
   
   
   
   
   //---------------------------------------------------------------------------
   //                      DQS Gen 
   //---------------------------------------------------------------------------
   
   always @(posedge ddr_clk or posedge reset) 
     begin
        if (reset)
	  begin
             dqs_reg <= 0;
	  end
        else
	  begin
             if (wr_drv_data)
               begin
                  if ((cyc_cnt_pos == 6) || (cyc_cnt_pos == 8) || (cyc_cnt_pos == 10) || (cyc_cnt_pos == 12))
                    dqs_reg <= 2'b11;
                  else
                    dqs_reg <= 0;
               end
	  end
     end // always @ (negedge clk)
   
   
   
   //---------------------------------------------------------------------------
   //                      Read data registration
   //---------------------------------------------------------------------------
   
   always @(negedge ddr_clk or posedge reset)
     begin
        if (reset)
	  begin
	     rd_da_0 <=  0;
	     rd_da_1 <=  0;
	     rd_da_2 <=  0;
	     rd_da_3 <=  0;
	     rd_da_4 <=  0;
	     rd_da_5 <=  0;
	     rd_da_6 <=  0;
	     rd_da_7 <=  0;
             dqs_cnt <= 0;
             dqs_phase <= 3;
	  end 
        else
	  begin
	     if (ddr_rd)
	       begin
                  if (dqs == dqs_phase)
                    begin
                       if (dqs_phase[0] == 1'b1) 
                         dqs_phase <= 2'b00;
                       else
                         dqs_phase <= 2'b11;
                       dqs_cnt <= dqs_cnt + 1;
	               if (dqs_cnt == 0) rd_da_0 <=  dq;
	               if (dqs_cnt == 1) rd_da_1 <=  dq;
	               if (dqs_cnt == 2) rd_da_2 <=  dq; 
	               if (dqs_cnt == 3) rd_da_3 <=  dq;
	               if (dqs_cnt == 4) rd_da_4 <=  dq;
	               if (dqs_cnt == 5) rd_da_5 <=  dq;
	               if (dqs_cnt == 6) rd_da_6 <=  dq; 
	               if (dqs_cnt == 7) rd_da_7 <=  dq;
                    end else
                      begin
                         if (dqs_cnt == burst_len) dqs_cnt <= 0;
                      end
	       end else
                 begin
                    dqs_cnt <= 0;
                    dqs_phase <= 3;
                 end
	  end
     end
   
`endprotect
endmodule
