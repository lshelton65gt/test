//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
//  Description:   256Mbit SDRAM DDR (Double Data Rate) as 16Meg X 16

`timescale 1ns/1ns 

module carbonx_ddrsdram_16megx16 (
dq, dqs, addr, ba, clk, clk_n, cke, cs_n, cas_n, ras_n, we_n, dm
		     );


 
  parameter IMsgWid = 448;
  parameter OMsgWid = 448;


  
  inout [15:0]        dq;
  inout [1:0] 	      dqs;
  input [13:0] 	      addr;
  input [1:0] 	      ba;
  input 	      clk;
  input 	      clk_n;
  input 	      cke;
  input 	      cs_n;
  input 	      cas_n;
  input 	      ras_n;
  input 	      we_n;
  input [1:0] 	      dm;

`protect
// carbon license crbn_vsp_exec_xtor_ddrsdram

/***************************************************************************************
*
*    File Name:  carbonx_ddrsdram_16meqx16.v
*         Date:  Jan 1, 2003
*        Model:  DDR SDRAM 16MX16 Synthesizable Transactor 
*
* Dependencies:  Needs SPARSE model on the C side
*
*       Author:  Robert Fredieu
*        Email:  fredieu@world.std.com
*
*****************************************************************************************
*   Description:   256Mbit SDRAM DDR (Double Data Rate) as 16Meg X 16
*
*   Limitations: - Does not check timing
*                - Does not check for refresh
*                - Does not check for proper powerup initialization.
*
*
*   Disclaimer:  THESE DESIGNS ARE PROVIDED "AS IS" WITH NO WARRANTY 
*                WHATSOEVER AND THE AUTHOR AND/OR SELLER SPECIFICALLY
*                DISCLAIMS ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
*                FITNESS FOR A PARTICULAR PURPOSE, OR AGAINST INFRINGEMENT.
*
*   Usage:       Intended to be used to aid in the verification of the design
*                of some device that uses these RAMs.  The assumption is that
*                the actual proper usage of this type of RAM has been verified
*                using some other method than this particular model.  This model
*                is simply intended to aid in the high level system verification
*                of the device using these RAMs.
*
*  Modifications:  Any modifications by the user/purchaser will violate any
*                  maintainance agreement covering this model.
*
*  License:      The user/purchaser agrees to the licensing agreement provided
*                with this model.  The license extends all usage of this model
*                even when it is modified or synthesized.
*
*     See furthur info at end of this file.

$Header$
 
Revision 1.2  2006/01/17 03:37:33  jstein
Reviewed by:  John Hoglund
Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
Bugs Affected: DW02_prod_sum1 sign extension
Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
removed all `defines from xactor verilog (mostly usb)
added test/xactors/usb20

Revision 1.1  2005/11/22 16:49:06  knutson
Reviewed by: Jim Stein
Tests Done: checkin
Bugs Affected:

Added Source Code for DDR SDRAM transactor
Renamed API functions for DDR SDRAM according to decided upon naming convention.
Updated DDR SDRAM example.

Updated API for PCI slave, to allow for user written slave function.
Update PCI example to use the carbonx_pci_target transactor as slave.

Revision 1.3  2005/10/20 18:23:28  jstein
Reviewed by: Dylan Dobbyn
Tests Done: scripts/checkin + xactor regression suite
Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
added html/xactors dir, and html/xactors/adding_xactors.txt doc

Revision 1.2  2005/09/29 23:53:43  jstein
Reviewed by: Dylan Dobbyn
Tests Done:  checkin, xactor regression
Bugs Affected: none
 Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
 moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
 so they will not be seen by the customer
 removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include

Revision 1.1  2005/09/25 16:58:31  jstein
Reviewed by: none
Tests Done: usb2.0 xactor test
Bugs Affected: Adding Verilog unprotected source code to sandbox

Revision 1.4  2005/07/01 15:01:21  colby_w
ddr_16megx16.v

Revision 1.3  2005/07/01 14:15:21  colby_w
ddr_16megx16.v

Revision 1.2  2005/07/01 14:07:19  colby_w
ddr_16megx16.v

Revision 1.1  2003/12/04 17:06:19  zhang
initial revision.

  
****************************************************************************************/

    reg       [13:0] bank0_row_addr;
    reg       [13:0] bank1_row_addr;
    reg       [13:0] bank2_row_addr;
    reg       [13:0] bank3_row_addr;

    reg       [13:0] mode_register;
    reg       [15:0] Dq_dm, Dq_out;

    reg        [2:0] burst_count;

    reg              precharged_bank0;
    reg              precharged_bank1;
    reg              precharged_bank2;
    reg              precharged_bank3;

    reg       [23:0] enable_addr_1;
    reg       [23:0] enable_addr_0;
    wire      [23:0] enable_addr;

    reg   [15:0]  access_errors;   // This is pulsed!
// bit 0 - illegal cas latency in mode register
// bit 1 - illegal burst length in mode register
// bit 2 - access of mode (or extended mode) register when all banks not precharged
// bit 3 - activate to a bank that is not precharged.
// bit 4 - burst terminate of an access with auto precharge.
// bit 5 - access to an inactive bank.
// bit 6 - interrupt of access with auto precharge.
// bit 7 - activate occurred before mode register written.
// bit 8 - activate occurred before extended mode register written.

    reg [31:0] reset_descriptor;  // used to detect a "powerup"
    reg [7:0]  reset_counter;
    reg        reset;

    reg    [7:0] first_data_low;
    reg          first_byte_enable_low;
    reg    [7:0] first_data_high;
    reg          first_byte_enable_high;
    reg    [7:0] write_data_second_low;
    reg    [7:0] write_data_first_low;
    reg          write_first_enable_low;
    reg          write_second_enable_low;
    reg    [7:0] write_data_second_high;
    reg    [7:0] write_data_first_high;
    reg          write_first_enable_high;
    reg          write_second_enable_high;
    wire  [15:0] write_data;
    wire  [15:0] write_data_next;
    wire   [3:0] write_byte_enable;

  wire 		 active_enable;  
    wire         write_enable;
    wire         read_enable;
    wire  [13:0] row_addr;

    wire         set_read_data_enable;
  wire 		 tristate_reset;
  

    reg   [23:0] addr_continue;
    reg   [23:0] addr_continue_next;
   // addr_continue_next[23:3] will always equal addr_continue[23:3]

    reg          write_enable_1;
    reg          write_enable_2;
    reg          write_enable_3;
    reg          write_enable_4;
    reg          write_continue;
    reg          write_continue_clear;
    reg          read_enable_1;
    reg          read_enable_2;
    reg          read_enable_3;
    reg          read_enable_4;
    reg          read_continue;
    reg          read_continue_clear;
    wire         set_read_dqs_enable;

    reg          bank0_tobe_precharged;
    reg          bank1_tobe_precharged;
    reg          bank2_tobe_precharged;
    reg          bank3_tobe_precharged;
  reg 		 mode_register_written;
  reg 		 extended_mode_register_written;
  

   parameter     ADDR_BITS      = 13;
   
  
   // Commands Decode
   wire          ACTIVE_ENABLE        =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h13);
   wire          AUTOREF_ENABLE       =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h11);   // Ignored
   wire          BURST_TERMINATE      =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h16);
   wire          EXTENDED_MODE_ENABLE = (({cke,cs_n,ras_n,cas_n,we_n} == 5'h10) && (ba[1:0] == 2'b1));
   wire          MODE_REGISTER_ENABLE = (({cke,cs_n,ras_n,cas_n,we_n} == 5'h10) && (ba[1:0] == 2'b0));
   wire          PRECHARGE_ENABLE     =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h12);
   wire          READ_ENABLE          =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h15);
   wire          WRITE_ENABLE         =  ({cke,cs_n,ras_n,cas_n,we_n} == 5'h14);

   // Burst Length Decode
   wire          BURST_LENGTH_2 = (mode_register[2:0] == 3'b001);
   wire          BURST_LENGTH_4 = (mode_register[2:0] == 3'b010);
   wire          BURST_LENGTH_8 = (mode_register[2:0] == 3'b011);
   
   // CAS Latency Decode
   wire          CAS_LATENCY_2  = (mode_register[6:4] == 3'b010);
   wire          CAS_LATENCY_25 = (mode_register[6:4] == 3'b110);
   wire          CAS_LATENCY_3  = (mode_register[6:4] == 3'b011);




  
//synopsys translate_off
    initial begin
        reset_descriptor <= 32'hffffffff;
    end
//synopsys translate_on


/* Look for a specific "random" number in reset_descriptor.  If it is not that */
/* then go into the reset mode for 256 clocks. cke ignored */

assign tristate_reset = (reset_descriptor != 32'h5ac3942) || (reset_counter != 8'hff);

    always @ (posedge clk) begin
        if(reset_descriptor != 32'h5ac3942) begin
             reset_descriptor <= 32'h5ac3942;
             reset_counter <= 8'h0;
             reset <= 1'b0;
        end
        else begin
             if(reset_counter != 8'hff) reset_counter <= reset_counter + 8'h1;

             if(reset_counter == 8'hff) reset <= 1'b0;
             else if(reset_counter <= 8'h01) reset <= 1'b0;
             else reset <= 1'b1;
        end
    end


  
/* The following latches the data, byte_enables, and address for writes  */
/* This is lined up for the next clock edge */
/* If the dqs is being driven by a read the results are ignored!  */

  always @ (posedge dqs[0]) begin
    first_data_low <= dq[7:0];
    first_byte_enable_low <= ~dm[0];
  end

  always @ (posedge dqs[1]) begin
    first_data_high <= dq[15:8];
    first_byte_enable_high <= ~dm[1];
  end

  always @ (negedge dqs[0]) begin
    write_data_second_low <= dq[7:0];
    write_data_first_low <= first_data_low;
    write_first_enable_low <= first_byte_enable_low;
    write_second_enable_low <= ~dm[0];
  end
 
  always @ (negedge dqs[1]) begin
    write_data_second_high <= dq[15:8];
    write_data_first_high <= first_data_high;
    write_first_enable_high <= first_byte_enable_high;
    write_second_enable_high <= ~dm[1];
  end

assign write_byte_enable = {write_second_enable_high,write_second_enable_low,
                               write_first_enable_high,write_first_enable_low};
assign write_data = {write_data_first_high,write_data_first_low};
assign write_data_next = {write_data_second_high,write_data_second_low};

  
/* The following are the signals for the first stuff sent to the RAM  */
/* This activates the cache */

assign enable_addr = {ba,row_addr[12:0],addr[8:0]};  // 2+13+9 = 24
assign active_enable = (ACTIVE_ENABLE) ? 1'b1 : 1'b0;
assign write_enable = (WRITE_ENABLE) ? 1'b1 : 1'b0;
assign read_enable = (READ_ENABLE) ? 1'b1 : 1'b0;
assign row_addr = (ba == 2'b00) ? bank0_row_addr : (
                  (ba == 2'b01) ? bank1_row_addr : (
                  (ba == 2'b10) ? bank2_row_addr : bank3_row_addr));

assign set_read_data_enable = read_continue && !reset && !tristate_reset;
assign set_read_dqs_enable  = (read_enable || read_continue) && !reset && !tristate_reset;

/*  The following is for the actual reads and writes  */

    always @ (posedge clk) begin

        if(reset) begin
           burst_count <= 3'b0;
           addr_continue <= 24'b0;
           addr_continue_next <= 24'b0;
           write_enable_1 <= 1'b0;
           write_enable_2 <= 1'b0;
           write_enable_3 <= 1'b0;
           write_enable_4 <= 1'b0;
           write_continue <= 1'b0;
           write_continue_clear <= 1'b0;
           read_enable_1 <= 1'b0;
           read_enable_2 <= 1'b0;
           read_enable_3 <= 1'b0;
           read_enable_4 <= 1'b0;
           read_continue <= 1'b0;
           read_continue_clear <= 1'b0;
           access_errors <= 16'b0;

           mode_register <= 14'b0;

           mode_register_written <= 1'b0;
           extended_mode_register_written <= 1'b0;

           bank0_row_addr <= 14'b0;
           bank1_row_addr <= 14'b0;
           bank2_row_addr <= 14'b0;
           bank3_row_addr <= 14'b0;

           enable_addr_0 <= 24'b0;
           enable_addr_1 <= 24'b0;

           bank0_tobe_precharged <= 1'b0;
           bank1_tobe_precharged <= 1'b0;
           bank2_tobe_precharged <= 1'b0;
           bank3_tobe_precharged <= 1'b0;
        end
        else begin
	  access_errors <= 16'b0;

	  /* Handle the addresses  */
	  enable_addr_1 <= enable_addr;
	  if(write_enable_1 || read_enable_1) 
	    begin
	      burst_count <= 3'b0;
	      addr_continue <= enable_addr_1;
	      addr_continue_next[23:3] <= enable_addr_1[23:3];  ////
	      if(BURST_LENGTH_2)
                begin
                   addr_continue_next[2:0] <= {enable_addr_1[2:1], enable_addr_1[0]+1'b1};
                end
	      if(mode_register[3] == 1'b0) 
		begin // sequential
		   if(BURST_LENGTH_4)
                     begin
                        addr_continue_next[2:0] <= {enable_addr_1[2], enable_addr_1[1:0]+1'b1};
                     end
		   if(BURST_LENGTH_8)
                     begin
                        addr_continue_next[2:0] <= enable_addr_1[2:0]+1'b1;
                     end
		end
	      else 
		begin  // interleaved
		  if(enable_addr_1[0] == 1'b0) 
		    begin
		      addr_continue_next[0] <= 1'b1;
		    end
		  else 
		    begin
		      addr_continue_next[0] <= 1'b0;
		    end
		end
	    end
	  else 
	    begin
              burst_count <= burst_count + 3'h2;
	      if(mode_register[3] == 1'b0) 
		begin // sequential
		  if(BURST_LENGTH_4) begin
		    addr_continue[1:0] <= addr_continue_next[1:0]+2'h1;
		    addr_continue_next[1:0] <= addr_continue_next[1:0]+2'h2;
		  end
		  if(BURST_LENGTH_8) begin
		    addr_continue[2:0] <= addr_continue_next[2:0]+2'h1;
		    addr_continue_next[2:0] <= addr_continue_next[2:0]+2'h2;
		  end
		end
	      else 
		begin  // interleaved
		  if(burst_count == 3'h0) 
		    begin
		      if(BURST_LENGTH_4) 
			begin
			  addr_continue[1:0] <= addr_continue[1:0] ^ 2'h2;
			  addr_continue_next[1:0] <= addr_continue_next[1:0] ^ 2'h2;
			end
		      if(BURST_LENGTH_8) 
			begin
			  addr_continue[2:0] <= addr_continue[2:0] ^ 3'h2;
			  addr_continue_next[2:0] <= addr_continue_next[2:0] ^ 3'h2;
			end
		    end
		  else if(burst_count == 3'h2)
		    begin
		      if(BURST_LENGTH_8) 
			begin
			  addr_continue[2:0] <= addr_continue[2:0] ^ 3'h6;
			  addr_continue_next[2:0] <= addr_continue_next[2:0] ^ 3'h6;
			end
		    end
		       else if(burst_count == 3'h4) 
			 begin
			   if(BURST_LENGTH_8) 
			     begin
			       addr_continue[2:0] <= addr_continue[2:0] ^ 3'h2;
			       addr_continue_next[2:0] <= addr_continue_next[2:0] ^ 3'h2;
			     end
			 end
		end
	    end // else: !if(write_enable_1 || read_enable_1)

	  

/* do the actual writes */
	   write_enable_1 <= write_enable;
	   if(write_continue_clear || WRITE_ENABLE || READ_ENABLE) begin
	     write_continue <= 1'b0;
	     write_enable_2 <= 1'b0;
	     write_enable_3 <= 1'b0;
	     write_enable_4 <= 1'b0;
	   end else
             begin
	        write_continue <= write_enable_1 || write_enable_2 || write_enable_3 || write_enable_4;
                if (BURST_LENGTH_4 || BURST_LENGTH_8)
                  begin
                     write_enable_2 <= write_enable_1;
                  end
                if (BURST_LENGTH_8)
                  begin
                     write_enable_3 <= write_enable_2;
                     write_enable_4 <= write_enable_3;
                  end
             end
           if(BURST_TERMINATE || PRECHARGE_ENABLE) 
             write_continue_clear <= 1'b1;
           else
             write_continue_clear <= 1'b0;

/* do the actual reads */
	   read_enable_1 <= read_enable;
	   if(read_continue_clear || WRITE_ENABLE || READ_ENABLE) begin
	     read_continue <= 1'b0;
	     read_enable_2 <= 1'b0;
	     read_enable_3 <= 1'b0;
	     read_enable_4 <= 1'b0;
	   end else
             begin
	        read_continue <= read_enable_1 || read_enable_2 || read_enable_3 || read_enable_4;
                if (BURST_LENGTH_4 || BURST_LENGTH_8)
                  begin
                     read_enable_2 <= read_enable_1;
                  end
                if (BURST_LENGTH_8)
                  begin
                     read_enable_3 <= read_enable_2;
                     read_enable_4 <= read_enable_3;
                  end
             end
           if(BURST_TERMINATE || PRECHARGE_ENABLE) 
             read_continue_clear <= 1'b1;
           else
             read_continue_clear <= 1'b0;

/* Auto precharge the bank - addr_continue[23:22] is the bank */
           if((BURST_LENGTH_8 && (read_enable_4 || write_enable_4)) ||
                 (BURST_LENGTH_4 && (read_enable_2 || write_enable_2)) ||
                 (BURST_LENGTH_2 && (read_enable_1 || write_enable_1))) begin
              if((addr_continue[23:22] == 2'b00) && bank0_tobe_precharged) begin
                  bank0_tobe_precharged <= 1'b0;
                  precharged_bank0 <= 1'b1;
              end
              else if((addr_continue[23:22] == 2'b01) && bank1_tobe_precharged) begin
                  bank1_tobe_precharged <= 1'b0;
                  precharged_bank1 <= 1'b1;
              end
              else if((addr_continue[23:22] == 2'b10) && bank2_tobe_precharged) begin
                  bank2_tobe_precharged <= 1'b0;
                  precharged_bank2 <= 1'b1;
              end
              else if((addr_continue[23:22] == 2'b11) && bank3_tobe_precharged) begin
                  bank3_tobe_precharged <= 1'b0;
                  precharged_bank3 <= 1'b1;
              end
           end


/* Handle auto precharge reads and writes */
           if(read_enable || write_enable) begin
/* Can't interrupt an autoprecharge read or write */
               if((ba == 2'b00) && (bank0_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
               if((ba == 2'b01) && (bank1_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
               if((ba == 2'b10) && (bank2_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
               if((ba == 2'b11) && (bank3_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
/* addr[10] indicates the autoprecharge  */
               if(addr[10] == 1'b1) begin
                  if(ba == 2'b00) bank0_tobe_precharged <= 1'b1; 
                  else if(ba == 2'b01) bank1_tobe_precharged <= 1'b1; 
                  else if(ba == 2'b10) bank2_tobe_precharged <= 1'b1; 
                  else bank3_tobe_precharged <= 1'b1;
               end
            end

           if(BURST_TERMINATE) begin
/* Can't interrupt an autoprecharge read or write */
               if((ba == 2'b00) && (bank0_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
               if((ba == 2'b01) && (bank1_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
               if((ba == 2'b10) && (bank2_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
               if((ba == 2'b11) && (bank3_tobe_precharged == 1'b1)) access_errors[4] <= 1'b1;
           end

           if(PRECHARGE_ENABLE) begin
               if((ba == 2'b00) && (bank0_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
               if((ba == 2'b01) && (bank1_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
               if((ba == 2'b10) && (bank2_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
               if((ba == 2'b11) && (bank3_tobe_precharged == 1'b1)) access_errors[6] <= 1'b1;
               if(addr[10] && ((bank0_tobe_precharged == 1'b1) ||
                               (bank1_tobe_precharged == 1'b1) || 
                               (bank2_tobe_precharged == 1'b1) ||
                               (bank3_tobe_precharged == 1'b1))) access_errors[6] <= 1'b1;
               if(addr[10] == 1'b0) begin
                  if(ba == 2'b00) precharged_bank0 <= 1'b1; 
                  else if(ba == 2'b01) precharged_bank1 <= 1'b1; 
                  else if(ba == 2'b10) precharged_bank2 <= 1'b1; 
                  else precharged_bank3 <= 1'b1;
               end
               else begin  // precharge them all
                  precharged_bank0 <= 1'b1;
                  precharged_bank1 <= 1'b1;
                  precharged_bank2 <= 1'b1;
                  precharged_bank3 <= 1'b1;
               end
            end

            // Activate Bank - Catch the bank's row address
           if (ACTIVE_ENABLE) begin

/* Detect an illegal cas latency  */
                if(!(CAS_LATENCY_2) && !(CAS_LATENCY_25) && !(CAS_LATENCY_3)) begin
                      access_errors[0] <= 1'b1;
                end

                if(!(BURST_LENGTH_2) && !(BURST_LENGTH_4) && !(BURST_LENGTH_8)) ////begin
                      access_errors[1] <= 1'b1;

               if(mode_register_written != 1'b1) access_errors[7] <= 1'b1;

               if(extended_mode_register_written != 1'b1) access_errors[8] <= 1'b1;

               if (ba == 2'b00 && precharged_bank0 == 1'b1) begin
                   precharged_bank0 <= 1'b0;
                   bank0_row_addr <= addr [ADDR_BITS - 1 : 0];
               end else if (ba == 2'b01 && precharged_bank1 == 1'b1) begin
                   precharged_bank1 <= 1'b0;
                   bank1_row_addr <= addr [ADDR_BITS - 1 : 0];
               end else if (ba == 2'b10 && precharged_bank2 == 1'b1) begin
                   precharged_bank2 <= 1'b0;
                   bank2_row_addr <= addr [ADDR_BITS - 1 : 0];
               end else if (ba == 2'b11 && precharged_bank3 == 1'b1) begin
                   precharged_bank3 <= 1'b0;
                   bank3_row_addr <= addr [ADDR_BITS - 1 : 0];
               end
/* Must not be precharged! This is an error. */
               else access_errors[3] <= 1'b1;
	   end // if (ACTIVE_ENABLE)
	  


           if (read_enable || write_enable) begin
                if ((ba == 2'b00 && precharged_bank0 == 1'b1) ||
                    (ba == 2'b01 && precharged_bank1 == 1'b1) ||
                    (ba == 2'b10 && precharged_bank2 == 1'b1) ||
                    (ba == 2'b11 && precharged_bank3 == 1'b1)) begin
                    // Can't access an inactive bank.
                         access_errors[5] <= 1'b1;
                end
            end

            // Extended Mode Register
            if (EXTENDED_MODE_ENABLE) begin
                if (precharged_bank0 != 1'b1 || 
                    precharged_bank1 != 1'b1 || 
                    precharged_bank2 != 1'b1 ||
                    precharged_bank3 != 1'b1) begin
                    //  All banks must be precharged prior to this.
                    access_errors[2] <= 1'b1;
                end
                else extended_mode_register_written <= 1'b1;
            end
        
            // Load Mode Register
            if (MODE_REGISTER_ENABLE) begin
                // Decode DLL, CAS Latency, Burst Type, and Burst Length
                if (precharged_bank0 == 1'b1 &&
                    precharged_bank1 == 1'b1 &&
                    precharged_bank2 == 1'b1 &&
                    precharged_bank3 == 1'b1) begin
                    mode_register <= addr;
                    mode_register_written <= 1'b1;
                end else begin
                    //  All banks must be precharged prior to this.
                    access_errors[2] <= 1'b1;
                end
	    end


	   ////end // if (cke)	  
	end // else: !if(reset)      
    end // always @ (posedge clk)
  
      
  wire [15:0] read_data;
  wire [15:0] read_data_next;
  

  carbonx_ddr_data_out_control ddr_data_out_control (
					     .clk(clk),  // actual ddr RAM input
					     .clk_n(clk_n),  // actual ddr RAM input
					     .mode_register(mode_register),
					     .read_data(read_data),
					     .read_data_next(read_data_next),
					     .data_bidirect_1(dq),   // actual ddr RAM output - for 2   cycle latency
					     .data_bidirect_2(dq),   // actual ddr RAM output - for 2.5 cycle latency
					     .data_bidirect_3(dq),   // actual ddr RAM output - for 3   cycle latency
					     .dqs_bidirect(dqs),     // actual ddr RAM output
					     .set_read_data_enable(set_read_data_enable),
					     .set_read_dqs_enable(set_read_dqs_enable),
					     .reset(reset),
					     .tristate_reset(tristate_reset)
					     );


   carbonx_ddr_ram_array ddr_ram_array (
			                .clock(clk),
			                .clock_n(clk_n),
			                .addr(addr_continue),
			                .next_addr(addr_continue_next),
			                .wr_data(write_data),
			                .wr_data_next(write_data_next),
			                .rd_data(read_data),
			                .rd_data_next(read_data_next),
			                .we(write_continue),
			                .re(read_continue),
			                .enable_addr(enable_addr),
			                .active_enable(active_enable),
			                .read_enable(read_enable),
			                .write_enable(write_enable),
			                .errors(access_errors),
			                .byte_enable(write_byte_enable),
			                .reset_ram(reset),
                                        .burst_len({BURST_LENGTH_8, BURST_LENGTH_4, BURST_LENGTH_2, 1'b0})
			                );





endmodule




/* The following module is used to control the data and dqs outputs during reads */
/* Much of the logic is only used during 2.5 cycle latency */


module carbonx_ddr_data_out_control (
    clk,
    clk_n,
    mode_register,
    read_data,
    read_data_next,
    data_bidirect_1,
    data_bidirect_2,
    data_bidirect_3,
    dqs_bidirect,
    set_read_data_enable,
    set_read_dqs_enable,
    reset,
    tristate_reset);

input clk;
input clk_n;
input [13:0] mode_register;
input [15:0] read_data;
input [15:0] read_data_next;
input set_read_data_enable;
input set_read_dqs_enable;
input reset;
input tristate_reset;
output [15:0] data_bidirect_1;
output [15:0] data_bidirect_2;
output [15:0] data_bidirect_3;
output [1:0] dqs_bidirect;

  reg 	     read_data_enable;
  reg 	     delayed_read_data_enable;  
  reg 	     read_dqs_enable;
  reg 	     delayed_read_dqs_enable;
  reg [15:0] delayed_read_data;
  reg [15:0] delayed_read_data_next; 
  reg [15:0] read_data_1;
  reg [15:0] read_data_2;
  reg [15:0] read_data_next_1;
  reg [15:0] read_data_next_2;
  reg 	     was_set_read_data_enable;
  reg 	     was_set_read_dqs_enable;
  reg 	     rd_out;
  reg 	     rd_out_next;
  reg 	     delayed_rd_out;
  reg 	     delayed_rd_out_next;

  wire 	     dqs_internal;
  wire 	     dqs_enable;
  wire 	     two_point_five_cycle;
  wire 	     two_cycle;
  wire 	     three_cycle;
  
  
assign #1 data_bidirect_1 = (two_cycle && read_data_enable) ? ( !(rd_out^rd_out_next) ? read_data_1 : read_data_next_1) : 16'hzzzz;
assign #1 data_bidirect_2 = delayed_read_data_enable ? ( !(delayed_rd_out^delayed_rd_out_next) ? delayed_read_data : delayed_read_data_next) : 16'hzzzz;
assign #1 data_bidirect_3 = (three_cycle && read_data_enable) ? ( !(rd_out^rd_out_next) ? read_data_2 : read_data_next_2) : 16'hzzzz;
assign #1 dqs_bidirect = dqs_enable ? {dqs_internal,dqs_internal} : 2'bzz;

assign two_cycle = (mode_register[6:4] == 3'b010) ? 1'b1 : 1'b0;

assign two_point_five_cycle = (mode_register[6:4] == 3'b110) ? 1'b1 : 1'b0;

assign three_cycle = (mode_register[6:4] == 3'b011) ? 1'b1 : 1'b0;

assign dqs_enable = (two_cycle || three_cycle) ? read_dqs_enable : delayed_read_dqs_enable;


always @ (posedge clk)
  if (reset)
    rd_out <= 0;
  else if (rd_out)
    rd_out <= 0;  
  else if (read_data_enable)
    rd_out <= 1;
  

always @ (negedge clk)
  if (reset)
    rd_out_next <= 0;
  else if (rd_out_next)
    rd_out_next <= 0;  
  else if (read_data_enable)
    rd_out_next <= 1;


always @ (negedge clk)
  if (reset)
    delayed_rd_out_next <= 0;
  else if (delayed_rd_out_next)
    delayed_rd_out_next <= 0;  
  else if (delayed_read_data_enable)
    delayed_rd_out_next <= 1;

  
always @ (posedge clk)
  if (reset)
    delayed_rd_out <= 0;
  else if (delayed_rd_out)
    delayed_rd_out <= 0;  
  else if (delayed_read_data_enable)
    delayed_rd_out <= 1;
  

  
always @ (negedge clk or posedge tristate_reset) begin
  if(tristate_reset) begin
    delayed_read_data_enable <= 1'b0;
    delayed_read_dqs_enable <= 1'b0;
    
    delayed_read_data <= 0;
    delayed_read_data_next <= 0;    
   end
   else begin
     delayed_read_data_enable <= two_point_five_cycle && was_set_read_data_enable;
     delayed_read_dqs_enable <= was_set_read_dqs_enable;
     
     delayed_read_data <= read_data_1;
     delayed_read_data_next <= read_data_next_1;
   end
end

  
always @ (posedge clk or posedge tristate_reset) begin
  if(tristate_reset) begin
    read_data_enable <= 1'b0;
    read_dqs_enable <= 1'b0;
    
    read_data_1 <= 0;
    read_data_2 <= 0;
    read_data_next_1 <= 0;
    read_data_next_2 <= 0;
    was_set_read_data_enable <= 0;    
  end
  else begin
    read_data_enable <= (two_cycle && set_read_data_enable) || (three_cycle && was_set_read_data_enable);
    read_dqs_enable <= (two_cycle && set_read_dqs_enable) || (three_cycle && was_set_read_dqs_enable);
    
    read_data_1 <= read_data;
    read_data_2 <= read_data_1;
    read_data_next_1 <= read_data_next;
    read_data_next_2 <= read_data_next_1;
    was_set_read_data_enable <= set_read_data_enable;
    was_set_read_dqs_enable <= set_read_dqs_enable;
  end
end
  

assign dqs_internal = (two_cycle || three_cycle) ? (read_data_enable && !(rd_out^rd_out_next) ? 1 : 0 ) :
                                                   (delayed_read_data_enable && !(delayed_rd_out^delayed_rd_out_next) ? 1 : 0);

endmodule
/* provides data whenever re goes high or when clock goes high and re already high */
/* writes data whenever we is high when clock goes high  */
/* simultaneous write and read is illegal */
/* cache is sixteen 16 bit words */
/* clock is the input clock */
/* read data is available at the next clock */

module carbonx_ddr_ram_array (
		              clock,
		              clock_n,
		              addr,
		              next_addr,
		              wr_data,
		              wr_data_next,
		              rd_data,
		              rd_data_next,
		              we,
		              re,
		              enable_addr,
		              active_enable,
		              read_enable,
		              write_enable,
		              errors,
		              byte_enable,
		              reset_ram,
                              burst_len);
   
   
   input               clock;
   input               clock_n;
   input [23:0]        addr;
   input [23:0]        next_addr;
   
   input [15:0]        wr_data;
   input [15:0]        wr_data_next;
   
   input               we;
   input               re;
   output [15:0]       rd_data;
   output [15:0]       rd_data_next;
   
   input [23:0]        enable_addr;
   input               active_enable;
   input               read_enable;
   input               write_enable;
   
   input [15:0]        errors;
   input               reset_ram;
   input [3:0]         byte_enable;
   input [3:0]         burst_len;         
   
   
   
   //***************************************************************************
   //            XIF interface
   //***************************************************************************
   parameter           BFMid     =  1,
		       ClockId   =  1,
		       DataWidth = 32,
		       DataAdWid = 10,
		       CSDWidth  = 32,
		       GCSwid    = 32,
		       PCSwid    = 32;
   
   
   /*
    * Include C interface file
    */
`include "xif_core.vh"
   
   
   reg [15:0]          internal_errors;
   reg [15:0]          errors_hold;
   
   reg [31:0]          cycle_count;
   reg                 write_active;
   reg [1:0]           write_read_word_addr;
   reg [1:0]           write_read_word_addr_r;
   reg [3:0]           write_word_count;
   reg                 write_is_active;
   reg                 waiting_for_command;
   reg [15:0]          active_byte_enables;
   reg [23:0]          active_enable_address;
   reg                 we_r;
   reg                 read_enable_r;
   
   
   //***************************************************************************
   //                      XIF interface
   //***************************************************************************
   
   assign              BFM_clock         = clock;
   assign              BFM_reset         = reset_ram;
   assign              BFM_xrun          = ~((~waiting_for_command && read_enable_r) || (write_word_count == (burst_len/2)));
   assign              BFM_put_operation = (write_is_active) ? BFM_WRITE : BFM_READ;
   assign              BFM_put_address   = {24'h0, active_byte_enables[15:0], active_enable_address[23:0]};
   assign              BFM_put_data      = {wr_data_next, wr_data};
   assign              BFM_put_size      = write_is_active ? write_word_count * 4 : burst_len * 2;
   assign              BFM_put_status    = {16'd0, errors};
   assign              BFM_put_csdata    = 0;
   assign              BFM_put_cphwtosw  = ~write_is_active;
   assign              BFM_put_cpswtohw  = write_is_active;
   assign              BFM_interrupt     = 0;
   assign              BFM_put_dwe       = we_r;
   assign              BFM_gp_daddr      = write_is_active ? {8'h0, write_read_word_addr_r} : {8'h0, write_read_word_addr + (re?1:0)};

   assign              rd_data = XIF_get_data[15:0];
   assign              rd_data_next = XIF_get_data[31:16];

   initial
     begin
        waiting_for_command <= 1;
        active_enable_address <= 0;
        write_is_active <= 0;
        write_read_word_addr <= 0;
        write_read_word_addr_r <= 0;
        write_word_count <= 0;
        active_byte_enables <= 0;
        we_r <= 0;
        read_enable_r <= 0;
     end
   always @(posedge clock or posedge reset_ram)
     begin
        if (reset_ram == 1)
          begin
             waiting_for_command <= 1;
             active_enable_address <= 0;
             write_is_active <= 0;
             write_read_word_addr <= 0;
             write_read_word_addr_r <= 0;
             write_word_count <= 0;
             active_byte_enables <= 0;
             we_r <= 0;
             read_enable_r <= 0;
          end 
        else
          begin
             we_r <= we;
             read_enable_r <= read_enable;
             write_read_word_addr_r <= write_read_word_addr;
             if (~waiting_for_command)
               begin
                  // Actively running a write or read
                  if (write_enable || read_enable)
                    begin
                       // initialize registers to move the data
                       write_is_active <= write_enable;
                       active_byte_enables <= 0;
                       active_enable_address <= enable_addr[23:0];
                       write_read_word_addr <= 2'b00;
                       write_word_count <= 0;
// FIXME only aligned writes for now 
//                       case (burst_len)
//                         4'h2:
//                           begin 
//                              active_enable_address <= enable_addr[23:0];
//                              write_read_word_addr <= 2'b00;
//                          end
//                         4'h4:
//                           begin 
//                              active_enable_address <= {enable_addr[23:1], 1'b0};
//                              write_read_word_addr <= {1'b0, enable_addr[0]};
//                          end
//                         4'h8:
//                           begin 
//                              active_enable_address <= {enable_addr[23:2], 2'b00};
//                              write_read_word_addr <= enable_addr[1:0];
//                           end
//                         default:
//                           begin
//                              active_enable_address <= 0;
//                              write_read_word_addr <= 0;
//                           end
//                         endcase
                    end
                  if (we || re)
                    begin
                       // new data just arrived (write) or was just sent (read)
                       if (we)
                         write_word_count <= write_word_count + 1;
                       case (burst_len)
                         4'h2:
                           begin 
                              active_byte_enables[3:0] <= byte_enable[3:0];
                              active_byte_enables[15:4] <= 0;
                              waiting_for_command <= 1;
                          end
                         4'h4:
                           begin 
                              write_read_word_addr[0] <= write_read_word_addr[0] + 1;
                              active_byte_enables[7:0] <= write_read_word_addr[0] ? {byte_enable[3:0], active_byte_enables[3:0]} : {active_byte_enables[7:4], byte_enable[3:0]};
                              active_byte_enables[15:8] <= 0;
                              if (write_read_word_addr[0] == 1)
                                begin
                                   waiting_for_command <= 1;
                                end
                          end
                         4'h8:
                           begin 
                              active_byte_enables[15:0] <= (write_read_word_addr[1:0] == 2'b00) ? {active_byte_enables[15:4], byte_enable[3:0]} :
                                                           (write_read_word_addr[1:0] == 2'b01) ? {active_byte_enables[15:8], byte_enable[3:0], active_byte_enables[3:0]} :
                                                           (write_read_word_addr[1:0] == 2'b10) ? {active_byte_enables[15:12], byte_enable[3:0], active_byte_enables[7:0]} :
                                                           (write_read_word_addr[1:0] == 2'b11) ? {byte_enable[3:0], active_byte_enables[11:0]} : 0;
                              write_read_word_addr[1:0] <= write_read_word_addr[1:0] + 1;
                              if (write_read_word_addr[1:0] == 3)
                                begin
                                   waiting_for_command <= 1;
                                end
                           end
                         default:
                           begin
                              write_read_word_addr[1:0] <= 0;
                              active_byte_enables[15:0] <= 0;
                           end
                         endcase
                    end
               end
             else
               begin
                  // Waiting for a new command
                  if (active_enable)
                    waiting_for_command <= 0;
                  write_read_word_addr <= 0;
                  write_word_count <= 0;
                  write_is_active <= 0;
                  active_byte_enables <= 0;
               end
          end
     end
   
   
endmodule


`endprotect
