# Don't do protected verilog on Windows - it should have already been
# done on Linux
if(${WIN32})
  return()
endif()

generate_xtor_vp(ddr/verilog
                 carbonx_ddrsdram_16megx16.v
                 carbonx_ddrsdram.v
                 carbonx_ddrsdram_ctrl.v
                 )
