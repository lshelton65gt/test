/*********************************************************************************
*
*    File Name:  ddr_16megx16.c
*      Version:  0.1
*         Date:  January 1, 2003
*        Model:  Xactor wrapper for the Zaiq SPARSE MEMORY Model 
*
* Dependencies:  Need a DRIVER in THE V SIDE (dr_16megx16.v) TO ATTACH  
*                TBP FUNCTION.
*
*       Author:  Robert Fredieu
*        Email:  fredieu@world.std.com
*
*  Description:  Wrapper around SPARSE memory model to build C xactor function 
*                for attaching with the V side Driver xactor using 3.2.1 TBP. 
*
**********************************************************************************
*  V SIDE-TRANSACTOR IS BUILD TO DRIVE SDRAM FILE WITH THE TBP FUNCTIONS
*   ATTACHED WITH SDRAM CLASS = 2 AND ID IS FUNCTION OF INDIVIDUAL SDRAM AND CARD.
**********************************************************************************/
#include "tbp.h"
#include "carbonXDdrsdram16Megx16.h"
#include "carbonXSparseMem.h"
#include <stdio.h>

void carbonXDdrsdram16Megx16MemFcn(sparseMem_t sparseMem) 
{

  SS_OP_T *trans;
  u_int32 wdata0, wdata1;
  u_int32 rdata0, rdata1;
  u_int32 i, j;
  u_int32 index;
  u_int32 byte_enable;
  u_int32 write_enable;
  u_int32 read_enable;

  // Check if NULL 
  if (sparseMem==NULL)  
  {
    //MSG_Printf("carbonXSparseMemCreateNew\n");
    sparseMem = carbonXSparseMemCreateNew();
  }

  while (1) {

    // Get a transaction from the transactor
    trans = carbonXGetOperation(); // Wait for a request
    
    // Respond to the transaction
    switch(trans->opcode) {

    case BFM_Write:

        // translate the byte address to a memory index
        index = trans->address & 0xffffff;
	//MSG_Printf("address = 0x%llx\n",trans->address);
        byte_enable = (trans->address>>24) & 0xffff;
        
        // Write Data
        for (i = 0; i < trans->size/2; i=i+2)
        {
          rdata0 = carbonXSparseMemReadWord32(sparseMem, (index+i)<<2); 
          rdata1 = carbonXSparseMemReadWord32(sparseMem, (index+i+1)<<2); 
          wdata0 = (((u_int32 *) (trans->read_data))[i/2]) & 0xffff;
          wdata1 = ((((u_int32 *) (trans->read_data))[i/2]) >> 16) & 0xffff;
	  // Figure out byte access
          write_enable = 0x0;
          read_enable  = 0x0;
          for (j=0; j<4; j++) {
            if (((byte_enable >> ((2*i) + j)) & 0x1) == 0x1) {
              write_enable = write_enable | (0xff << (8*j));
            } else {
              read_enable = read_enable | (0xff << (8*j));
            }
          }
          wdata0 = (wdata0 & (write_enable & 0xffff)) | (rdata0 & (read_enable & 0xffff));
          wdata1 = (wdata1 & ((write_enable >> 16) & 0xffff)) | (rdata1 & ((read_enable >> 16) & 0xffff));
          carbonXSparseMemWriteWord32(sparseMem, (index+i)<<2, wdata0 & 0xffff); 
          carbonXSparseMemWriteWord32(sparseMem, (index+i+1)<<2, wdata1 & 0xffff); 
	  //MSG_Printf("write addr=0x%0x, data=0x%0x, 0x%0x, wr_en=0x%0x, rd_en=0x%0x, by_en=0x%0x\n", 
          //           index+i, wdata1, wdata0, write_enable, read_enable, byte_enable);
        }
	
	break;

  case BFM_Read:

        index = trans->address & 0xffffff;
        //MSG_Printf("address = 0x%0x\n",trans->address);
	
        for (i = 0; i < trans->size/2; i=i+2)
        {
          ((u_int32 *) (trans->write_data))[i/2] = (carbonXSparseMemReadWord32(sparseMem, (index+i)<<2) & 0xffff) | ((carbonXSparseMemReadWord32(sparseMem, (index+i+1)<<2) << 16) & 0xffff0000); 
	  //MSG_Printf("read addr=0x%0x, data=0x%0x\n", 
          //           index+i, ((u_int32 *) (trans->write_data))[i/2]);
        }
	
	break;
  default:
    MSG_Warn("Opcode = 0x%0x for DDR SDRAM SVM. OK before init.\n", trans->opcode);
  
    }

    /* return to the sim-side to complete the transaction */
    carbonXSendOperation(trans);
  }
}


u_int16 carbonXDdrsdram16Megx16Read(sparseMem_t mem, u_int32 ba, u_int32 ra, u_int32 ca)
{
  
  u_int32 addr = ( (ba & 0x3) << 22) | ( (ra & 0x1fff) << 9) | ( ca & 0x1ff);
  u_int64 data;
  
  data = carbonXSparseMemReadWord32(mem, addr<<2);
  return data;
}

void carbonXDdrsdram16Megx16Write(sparseMem_t mem, u_int32 ba, u_int32 ra, u_int32 ca, u_int16 data) {
  
  u_int32 addr = ( (ba & 0x3) << 22) | ( (ra & 0x1fff) << 9) | ( ca & 0x1ff);

  carbonXSparseMemWriteWord32(mem, addr<<2, data);
}

void ddr_16megx16_mem_errors(int class, int id, int error1, int error2) {
  struct queue_transaction_struct *trans = NULL ;
  (void) class; (void)id;

  trans = tbp_attach_buffer();
        if((error1 & 0xffff) != 0) {
// bit 0 - illegal cas latency in mode register
// bit 1 - illegal burst length in mode register
// bit 2 - access of mode (or extended mode) register when all banks not precharged
// bit 3 - activate to a bank that is not precharged.
// bit 4 - burst terminate of an access with auto precharge.
// bit 5 - access to an inactive bank.
// bit 6 - interrupt of access with auto precharge.
// bit 7 - activate occurred before mode register written.
// bit 8 - activate occurred before extended mode register written.
            if((error1 & 0x1) != 0)
                   MSG_Error("Illegal Cas Latency in Mode Register for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x2) != 0)
                   MSG_Error("Illegal Burst Length in Mode Register for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x4) != 0)
                   MSG_Error("Access of Mode (or Extended Mode) Register when banks not precharged for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x8) != 0)
                   MSG_Error("Activate of unprecharged bank for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x10) != 0)
                   MSG_Error("Burst Terminate of Auto Precharge Access for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x20) != 0)
                   MSG_Error("Access to Inactive Bank for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x40) != 0)
                   MSG_Error("Interrupt of Auto Precharge Access for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x80) != 0)
                   MSG_Error("Activate before Mode Register Written for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error1 & 0x100) != 0)
                   MSG_Error("Activate before Extended Mode Register Written for DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
        }

        if((error2 & 0xffff) != 0) {
            if((error2 & 0x1) != 0)
                   MSG_Error("Write to uncached location at DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
            if((error2 & 0x2) != 0)
                   MSG_Error("Read to uncached location at DDR RAM at Class/ID %d/%d",
                          trans->class, trans->transactor_id);
        }

}

void ddr_16megx16_time_monitor(int class, int id, unsigned long cycle_count, int rdwr) {

static int first_read = 1;
static int first_write = 1;

 if (rdwr == 1) {
   if(first_read) {
     MSG_Printf("A read of a DDR SDRAM has occured for the first time!\n");
     MSG_Printf("This occurred at cycle count %d for class/id %d/%d\n",
		   cycle_count,class,id);
     first_read = 0;
   }
 }
 else {
   if(first_write) {
     MSG_Printf("A write of a DDR SDRAM has occured for the first time!\n");
     MSG_Printf("This occurred at cycle count %d for class/id %d/%d\n",
		   cycle_count,class,id);
     first_write = 0;
   }
 }

// nothing else to do as yet

}

