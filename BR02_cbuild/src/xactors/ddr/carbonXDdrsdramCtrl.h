
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
 *****************************************************************************
 *
 * File Name: ddr_support.h
 *
 * This file serves as the toplevel include file for the project source.
 * It should be included by all project source and hold all #includes
 * which are common to all source.
 *
 *****************************************************************************
 */
#ifndef DDR_SUPPORT_H
#define DDR_SUPPORT_H

#define DDR_MODE_REG_ADDR   0x80000000

#define CARBON_DDR_BURST_LEN_2 1
#define CARBON_DDR_BURST_LEN_4 2
#define CARBON_DDR_BURST_LEN_8 3

#define CARBON_DDR_CAS_LATENCY_2 (2<<4)
#define CARBON_DDR_CAS_LATENCY_2_5 (6<<4)
#define CARBON_DDR_CAS_LATENCY_3 (3<<4)

#define CARBON_DDR_BURST_TYPE_S (0<<3)
#define CARBON_DDR_BURST_TYPE_I (1<<3)

void carbonXDdrsdramCtrlSetModeReg (u_int32 mode);
void carbonXDdrsdramCtrlWrite (u_int32 addr, u_int64 wdata);
u_int64 carbonXDdrsdramCtrlRead (u_int32 addr);

#endif
