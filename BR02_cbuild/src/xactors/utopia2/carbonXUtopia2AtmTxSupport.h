//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2AtmTxSupport.h
 *
 * This file contains private definitions used for the C-API of the Utopia 2 
 * ATM Tx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2ATMTXSUPPORT_H
#define  UTOPIA2ATMTXSUPPORT_H

// CS Write Address Map
#define UTOPIA2ATMTX_FLOWCTRLMODE  (0<<2)
#define UTOPIA2ATMTX_CLAVMODE      (1<<2)
#define UTOPIA2ATMTX_POLLSTARTADDR (2<<2)
#define UTOPIA2ATMTX_POLLENDADDR   (3<<2)

// CS Read Address Map
#define UTOPIA2ATMTX_DATAW        (256 + (0<<2))


typedef struct {
  u_int32 flow_ctrl_mode;
  u_int32 cell_size;

} UTOPIA2ATMTX_INST_DATA_T;


UTOPIA2ATMTX_INST_DATA_T * carbonXUtopia2AtmTxGetInstData (void);
void carbonXUtopia2AtmTxInit(void);

#endif
