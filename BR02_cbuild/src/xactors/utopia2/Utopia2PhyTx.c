//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2PhyTx.c
 *
 * This file contains the C-API of the Utopia 2 PHY Tx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */

#include "tbp.h"
#include "carbonXUtopia2PhyTxSupport.h"
#include "carbonXUtopia2PhyTx.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"

// Function: carbonXUtopia2PhyTxSetPortAddr
// Description: Defines the port address for the Attached PHY Tx Transactor. 
//              port_idx specifies what port on the attached transactor should be 
//              specified. portIdx 0 is the port corresponding to the oTxClav[0] port,
//              1 is the port corresponding to oTxClav[1] and so on. The port_addr 
//              parameter is the port address between 0 and 30 that the port should
//              repond to.
// Parameters: port_idx  -- what port on the attached transactor should be specified
//             port_addr -- Port address of the port
// Return: Nothing
void carbonXUtopia2PhyTxSetPortAddr (u_int32 port_idx, u_int32 port_addr){

  UTOPIA2PHYTX_INST_DATA_T * instData;

  
  if(port_idx > 3) {
    MSG_Error("port_idx has to be less that 4\n");
    return;
  }
  
  if(port_addr > 31) {
    MSG_Error("port_addr has to be less than or equal to 31\n");
    return;
  }
    
  instData = carbonXUtopia2PhyTxGetInstData();

  switch(port_idx) {
  case 0 :
    carbonXCsWrite(UTOPIA2PHYTX_PORTADDR0, port_addr);
    instData->port_addr[0] = port_addr;
    break;
  case 1 :
    carbonXCsWrite(UTOPIA2PHYTX_PORTADDR1, port_addr);
    instData->port_addr[1] = port_addr;
    break;
  case 2 :
    carbonXCsWrite(UTOPIA2PHYTX_PORTADDR2, port_addr);
    instData->port_addr[2] = port_addr;
    break;
  case 3 :
    carbonXCsWrite(UTOPIA2PHYTX_PORTADDR3, port_addr);
    instData->port_addr[3] = port_addr;
    break;
  }
}

// Function: carbonXUtopia2PhyTxSetStatus
// Description: Sets the Cell available status for the port indicated by port_addr. 
//              The status parameter should be set to 1 if a cell is available to be 
//              sent or to 0 if there are no cells to be sent on this port. If the port 
//              does not exist for the PHY that's attached to the function calling this 
//              API, the call will be ignored.
// Parameters: port_addr  -- The port in which to set the status
//             port_addr  -- The status of the port 
// Return: Nothing
void carbonXUtopia2PhyTxSetStatus (u_int32 port_addr, u_int32 status){

  UTOPIA2PHYTX_INST_DATA_T * instData;
  u_int32 port_status;
  u_int32 i;

  if(port_addr > 30) {
    MSG_Error("port_addr has to be less that 31\n");
    return;
  }
  
  instData    = carbonXUtopia2PhyTxGetInstData();

  // Save Status before update, so we can decide not to write if there is no change
  port_status = instData->port_status;

  for(i = 0; i < 4; i++) {
    if(instData->port_addr[i] == port_addr) {
      port_status = (port_status & ~(1<<i)) | ((status&0x1) << i);
      break;
    }
  }

  // If status is updated write it to the verilog side
  if(instData->port_status != port_status) {
    carbonXCsWrite(UTOPIA2PHYTX_STATUS, port_status);
    instData->port_status = port_status;
  }
}

// Function: carbonXUtopia2PhyTxSPhyFlowCtrlMode
// Description: Sets the Flow Control Mode that the attached PHY Tx transactor should 
//              operate in. Set mode to OCTET_FLOW_CTRL if octet (cycle level) flow 
//              control is desired, to CELL_FLOW_CTRL if cell level flow control is 
//              desired. Octet level flow control can only be set when operating in a 
//              single PHY configuration.
// Parameters: mode  -- Flow Control Mode
// Return: Nothing
void carbonXUtopia2PhyTxSetSPhyFlowCtrlMode(u_int32 mode){

  UTOPIA2PHYTX_INST_DATA_T * instData;
  instData = carbonXUtopia2PhyTxGetInstData();

  if(mode != 0 && mode != 1){
    MSG_Error("mode must be either 1 or 0\n");
    return;
  }

  instData->flow_ctrl_mode = mode;
  carbonXCsWrite(UTOPIA2PHYTX_FLOWCTRLMODE, mode);
}

// Function: carbonXUtopia2PhyTxSPhyFlowCtrlMode
// Description: Sets the Flow Control Mode that the attached PHY Tx transactor should 
//              operate in. Set mode to OCTET_FLOW_CTRL if octet (cycle level) flow 
//              control is desired, to CELL_FLOW_CTRL if cell level flow control is 
//              desired. Octet level flow control can only be set when operating in a 
//              single PHY configuration.
// Parameters: mode  -- Clav Mode
// Return: Nothing
void carbonXUtopia2PhyTxSetMPhyClavMode(u_int32 mode){

  if(mode != 0 && mode != 1){
    MSG_Error("mode must be either 1 or 0\n");
    return;
  }

  carbonXCsWrite(UTOPIA2PHYTX_CLAVMODE, mode);
}

// Function: carbonXUtopia2PhyTxReceiveCell
// Description: Receives a cell from the attached PHY Tx Transactor and put it in the 
//              space pointed to by the cell_buf parameter. cell_buf has to point to 
//              a user allocated memory at least 53 bytes long.
// Parameters: cell_buf  -- Pointer to buffer where cell should get stored.
// Return: The port address that the cell was received on. If the function returns 31, 
//         an error occurred and the cell should be considered invalid data.
u_int32 carbonXUtopia2PhyTxReceiveCell(u_int8 *cell_buf){

  u_int32   size;
  TBP_OP_T *trans;
  u_int32   port;

  size = carbonXReadArray8(0, cell_buf, carbonXUtopia2PhyTxGetCellSize());

  if (size == 0) { 
    // When size comes back as zero, the transaction timed out
    // and so the null port is returned.
    return 31;
  }

  if (size != carbonXUtopia2PhyTxGetCellSize()) {
    MSG_Error("Receive data (%d bytes) was different than expected %d bytes. Possible Array overflow\n",
	      size, carbonXUtopia2PhyTxGetCellSize());
    return 31; // Null Port. Indicates error.
  }
  
  // The port is returned in the address field
  trans = carbonXGetOperation();
  port  = trans->address;
  
  if(port > 30) {
    MSG_Error("Illegal port address %d recieved. Port has to be less than 31.\n", port);
    port = 31;  // Null Port. Indicates error.
  }
  
  return port;
    
}


UTOPIA2PHYTX_INST_DATA_T * carbonXUtopia2PhyTxGetInstData (void) {

  UTOPIA2PHYTX_INST_DATA_T * instData = 
    (UTOPIA2PHYTX_INST_DATA_T *) carbonXGetInstData();

  if (NULL == instData) {
    carbonXUtopia2PhyTxInit();
    instData = (UTOPIA2PHYTX_INST_DATA_T *) carbonXGetInstData();
  }

  return instData;
}

u_int32 carbonXUtopia2PhyTxGetCellSize (){
  return carbonXUtopia2PhyTxGetInstData()->cell_size;
}

void carbonXUtopia2PhyTxInit(void) {
  UTOPIA2PHYTX_INST_DATA_T * instData;

  if( (instData = carbonXGetInstData() ) == NULL) {
    instData = carbonmem_malloc(sizeof(UTOPIA2PHYTX_INST_DATA_T));
  }

  if (instData != NULL) {
    carbonXSetInstData(instData);
    instData->port_addr[0]   = 0x1f;
    instData->port_addr[1]   = 0x1f;
    instData->port_addr[2]   = 0x1f;
    instData->port_addr[3]   = 0x1f;
    instData->port_status    = 0;
    instData->flow_ctrl_mode = CELL_FLOW_CTRL;
    
    // In 16 bit data bus mode, the cell size is 54 bytes
    if(carbonXCsRead(UTOPIA2PHYTX_DATAW) == 16)
      instData->cell_size = 54;
    // In 8 bit data bus mode, the cell size is 53 bytes
    else
      instData->cell_size = 53;
    
  }
  

  else MSG_Panic("Failed, allocating memory for instance data.\n");
}
