//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2AtmRxSupport.h
 *
 * This file contains private definitions used for the C-API of the Utopia 2 
 * ATM Rx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2ATMRXSUPPORT_H

// CS Write Address Map
#define UTOPIA2ATMRX_FLOWCTRLMODE  (0<<2)
#define UTOPIA2ATMRX_CLAVMODE      (1<<2)
#define UTOPIA2ATMRX_POLLSTARTADDR (2<<2)
#define UTOPIA2ATMRX_POLLENDADDR   (3<<2)

// CS Read Address Map
#define UTOPIA2ATMRX_DATAW        (256 + (0<<2))


typedef struct {
  u_int32 flow_ctrl_mode;
  u_int32 cell_size;

} UTOPIA2ATMRX_INST_DATA_T;


UTOPIA2ATMRX_INST_DATA_T * carbonXUtopia2AtmRxGetInstData (void);
void carbonXUtopia2AtmRxInit(void);

#define  UTOPIA2ATMRXSUPPORT_H

#endif
