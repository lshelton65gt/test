//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2PhyTx.h
 *
 * This file contains public definitions used for the C-API of the Utopia 2 
 * PHY Tx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2PHYTX_H
#define  UTOPIA2PHYTX_H

#include <xactors/utopia2/carbonXUtopia2Support.h>

void carbonXUtopia2PhyTxSetPortAddr (u_int32 port_idx, u_int32 port_addr);
void carbonXUtopia2PhyTxSetStatus (u_int32 port_addr, u_int32 status);
void carbonXUtopia2PhyTxSetSPhyFlowCtrlMode(u_int32 mode);
void carbonXUtopia2PhyTxSetMPhyClavMode(u_int32 mode);
u_int32 carbonXUtopia2PhyTxReceiveCell(u_int8 *cell_buf);
u_int32 carbonXUtopia2PhyTxGetCellSize ();

#endif
