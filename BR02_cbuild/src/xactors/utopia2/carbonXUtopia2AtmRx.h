//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2AtmRx.h
 *
 * This file contains public definitions used for the C-API of the Utopia 2 
 * ATM Rx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2ATMRX_H
#define  UTOPIA2ATMRX_H

#include <xactors/utopia2/carbonXUtopia2Support.h>

void carbonXUtopia2AtmRxSetupPolling (u_int32 start_addr, 
				   u_int32 end_addr,
				   u_int32 mphy_clav_mode);
u_int32 carbonXUtopia2AtmRxGetStatus (u_int32 port_addr);
void carbonXUtopia2AtmRxSetSPhyFlowCtrlMode(u_int32 mode);
u_int32 carbonXUtopia2AtmRxReceiveCell(u_int32 port_addr, u_int8 *cell_buf);
u_int32 carbonXUtopia2AtmRxGetCellSize ();
void carbonXUtopia2AtmRxPrintCell(u_int8 *cell);
#endif
