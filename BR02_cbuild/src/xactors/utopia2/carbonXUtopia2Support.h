//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: Utopia2Support.h
 *
 * This file contains common definitions used for the the C-API of all 
 * Utopia 2 Transactors
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2SUPPORT_H
#define  UTOPIA2SUPPORT_H

// Flow Control Mode
#define CELL_FLOW_CTRL 1
#define OCTET_FLOW_CTRL 0

// Clav Mode
#define FOUR_CLAV_MODE 1
#define ONE_CLAV_MODE  0
 
#endif
