//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2PhyRx.h
 *
 * This file contains public definitions used for the C-API of the Utopia 2 
 * PHY Rx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2PHYRX_H
#define  UTOPIA2PHYRX_H

#include <xactors/utopia2/carbonXUtopia2Support.h>

void carbonXUtopia2PhyRxSetPortAddr (u_int32 port_idx, u_int32 port_addr);
u_int32 carbonXUtopia2PhyRxGetStatus (u_int32 port_addr);
void carbonXUtopia2PhyRxSetSPhyFlowCtrlMode(u_int32 mode);
u_int32 carbonXUtopia2PhyRxGetNextPort();
void carbonXUtopia2PhyRxSetMPhyClavMode(u_int32 mode);
void carbonXUtopia2PhyRxQueueCell(u_int32 port, u_int8 *cell_buf);
u_int32 carbonXUtopia2PhyRxSendCell();
u_int32 carbonXUtopia2PhyRxGetCellSize ();

#endif
