//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2PhyRxSupport.h
 *
 * This file contains private definitions used for the C-API of the Utopia 2 
 * PHY Rx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2PHYRXSUPPORT_H
#define  UTOPIA2PHYRXSUPPORT_H

// CS Write Address Map
#define UTOPIA2PHYRX_FLOWCTRLMODE (0<<2)
#define UTOPIA2PHYRX_CLAVMODE     (1<<2)
#define UTOPIA2PHYRX_PORTADDR0    (2<<2)
#define UTOPIA2PHYRX_PORTADDR1    (3<<2)
#define UTOPIA2PHYRX_PORTADDR2    (4<<2)
#define UTOPIA2PHYRX_PORTADDR3    (5<<2)
#define UTOPIA2PHYRX_STATUS       (6<<2)

// CS Read Address Map
#define UTOPIA2PHYRX_DATAW        (256 + (0<<2))
#define UTOPIA2PHYRX_PORTADDR     (256 + (1<<2))

typedef struct {
  u_int32  port_addr[4];
  u_int32  port_status;
  u_int32  flow_ctrl_mode;
  u_int32  cell_size;
  u_int32  send_size;
  u_int8 * cell_buf;
  
} UTOPIA2PHYRX_INST_DATA_T;


UTOPIA2PHYRX_INST_DATA_T * carbonXUtopia2PhyRxGetInstData (void);
void carbonXUtopia2PhyRxInit(void);

#endif
