//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2PhyRx.c
 *
 * This file contains the C-API of the Utopia 2 PHY Rx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */

#include "tbp.h"
#include "carbonXUtopia2PhyRxSupport.h"
#include "carbonXUtopia2PhyRx.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"

// Function: carbonXUtopia2PhyRxSetPortAddr
// Description: Defines the port address for the Attached PHY Rx Transactor. 
//              port_idx specifies what port on the attached transactor should be 
//              specified. portIdx 0 is the port corresponding to the oRxClav[0] port,
//              1 is the port corresponding to oRxClav[1] and so on. The port_addr 
//              parameter is the port address between 0 and 30 that the port should
//              repond to.
// Parameters: port_idx  -- what port on the attached transactor should be specified
//             port_addr -- Port address of the port
// Return: Nothing
void carbonXUtopia2PhyRxSetPortAddr (u_int32 port_idx, u_int32 port_addr){

  UTOPIA2PHYRX_INST_DATA_T * instData;

  
  if(port_idx > 3) {
    MSG_Error("port_idx has to be less that 4\n");
    return;
  }
  
  if(port_addr > 31) {
    MSG_Error("port_addr has to be less than or equal to 31\n");
    return;
  }
    
  instData = carbonXUtopia2PhyRxGetInstData();

  switch(port_idx) {
  case 0 :
    carbonXCsWrite(UTOPIA2PHYRX_PORTADDR0, port_addr);
    instData->port_addr[0] = port_addr;
    break;
  case 1 :
    carbonXCsWrite(UTOPIA2PHYRX_PORTADDR1, port_addr);
    instData->port_addr[1] = port_addr;
    break;
  case 2 :
    carbonXCsWrite(UTOPIA2PHYRX_PORTADDR2, port_addr);
    instData->port_addr[2] = port_addr;
    break;
  case 3 :
    carbonXCsWrite(UTOPIA2PHYRX_PORTADDR3, port_addr);
    instData->port_addr[3] = port_addr;
    break;
  }
}

// Function: carbonXUtopia2PhyRxGetStatus
// Description: Gets the Cell available status for the port indicated by port_addr. 
//              If the port does not exist for the PHY that's attached to the function 
//              calling this API, the call will be ignored.
// Parameters: port_addr  -- The port in which to set the status
// Return: status for the specified port. 0 means no cell is queued, 1 means 1 cell is queued
u_int32 carbonXUtopia2PhyRxGetStatus (u_int32 port_addr){

  UTOPIA2PHYRX_INST_DATA_T * instData;
  u_int32 port_status=0;
  u_int32 i;

  if(port_addr > 30) {
    MSG_Error("port_addr has to be less that 31. port_addr = %d\n", port_addr);
    return 0;
  }
  
  instData    = carbonXUtopia2PhyRxGetInstData();

  for(i = 0; i < 4; i++) {
    if(instData->port_addr[i] == port_addr) {
      port_status = (instData->port_status >> i) & 0x1;
      break;
    }
  }
  
  return port_status;
}

// Function: carbonXUtopia2PhyRxSPhyFlowCtrlMode
// Description: Sets the Flow Control Mode that the attached PHY Rx transactor should 
//              operate in. Set mode to OCTET_FLOW_CTRL if octet (cycle level) flow 
//              control is desired, to CELL_FLOW_CTRL if cell level flow control is 
//              desired. Octet level flow control can only be set when operating in a 
//              single PHY configuration.
// Parameters: mode  -- Flow Control Mode
// Return: Nothing
void carbonXUtopia2PhyRxSetSPhyFlowCtrlMode(u_int32 mode){

  UTOPIA2PHYRX_INST_DATA_T * instData;
  instData = carbonXUtopia2PhyRxGetInstData();

  if(mode != 0 && mode != 1){
    MSG_Error("mode must be either 1 or 0\n");
    return;
  }

  instData->flow_ctrl_mode = mode;
  carbonXCsWrite(UTOPIA2PHYRX_FLOWCTRLMODE, mode);
}

// Function: Utopa2PhyRxGetNextPort
// Description: The function returns the next port address to be sent 
//              as requested from the AtmRx device.
//
// Parameters: None
// Return: The next port address to be sent as requested from the AtmRx device.
u_int32 carbonXUtopia2PhyRxGetNextPort(){
  return carbonXCsRead(UTOPIA2PHYRX_PORTADDR);
}

// Function: carbonXUtopia2PhyRxSPhyFlowCtrlMode
// Description: Sets the Flow Control Mode that the attached PHY Rx transactor should 
//              operate in. Set mode to OCTET_FLOW_CTRL if octet (cycle level) flow 
//              control is desired, to CELL_FLOW_CTRL if cell level flow control is 
//              desired. Octet level flow control can only be set when operating in a 
//              single PHY configuration.
// Parameters: mode  -- Clav Mode
// Return: Nothing
void carbonXUtopia2PhyRxSetMPhyClavMode(u_int32 mode){

  if(mode != 0 && mode != 1){
    MSG_Error("mode must be either 1 or 0\n");
    return;
  }

  carbonXCsWrite(UTOPIA2PHYRX_CLAVMODE, mode);
}

// Function: carbonXUtopia2PhyRxQueueCell
// Description: Queues a cell of data from the attached ATM Rx Transactor to the port 
//              specified by the port_addr parameter. The Cell is always 53 bytes long
//              in 8 bit databus mode and 54 bytes long in 16 bit databus mode and the 
//              data is pointed to by the cell_buf parameter.
//
// Parameters: cell_buf  -- Pointer to cell buffer.
// Return: Nothing
void carbonXUtopia2PhyRxQueueCell(u_int32 port, u_int8 *cell_buf){

  u_int32 i;
  u_int32 port_idx=0;
  UTOPIA2PHYRX_INST_DATA_T * instData;

  instData = carbonXUtopia2PhyRxGetInstData();

  // Find Port Index for this port
  for(i = 0; i < 4; i++) {
    if(instData->port_addr[i] == port) {
      port_idx = i;
      break;
    }
  }
  
  if(i == 4) {
    MSG_Error("Port %d does not exist in this transactor.\n", port);
    return;
  }
  
  // Check if we already have a cell available for this port
  // If we do, then we have to keep sending cells until the cell on this
  // port has been sent, and we can put the new cell into the buffer
  if(instData->port_status & (1 << port_idx)){
    MSG_Error("Tried to queue cell to port %d when a cell was already queued. The new cell will be ignored.\n");
    return;
  }
    
  // Now Put the cell into the send buffer
  if(instData->cell_size == 54) {
    for(i = 0; i < 54; i += 2){
      instData->cell_buf[i*4+2*port_idx+0] = cell_buf[i+1];
      instData->cell_buf[i*4+2*port_idx+1] = cell_buf[i];
    }
  }
  else {
    for(i = 0; i < 53; i ++){
      instData->cell_buf[i*4+port_idx] = cell_buf[i];
    }
  }

  // Update port_status
  instData->port_status |= (1 << port_idx); 
  
}

// Function: carbonXUtopia2PhyRxSendCell
// Description: Sends one of the already queued cells of from the 
//              attached PHY Rx Transactor to the port requested by the 
//              ATM Rx device over the RxAddr bus. The port to which the 
//              cell was sent is returned by the function.
// Parameters: None
// Return: port on which a cell was sent
u_int32 carbonXUtopia2PhyRxSendCell() {

  u_int32 i;
  UTOPIA2PHYRX_INST_DATA_T * instData;
  u_int32 sent_port_idx=0;
  u_int32 sent_port;
  
  instData = carbonXUtopia2PhyRxGetInstData();
  
  // Check Port Status, if there are no cells available to send, wait for a while and 
  // return 31
  if(instData->port_status == 0) {
    if(instData->cell_size == 54) carbonXIdle(27);
    else carbonXIdle(53);
    return 31;
  }
  
  carbonXWriteArray8(instData->port_status, instData->cell_buf, instData->cell_size*4);    
  sent_port = carbonXGetOperation()->address & 0x1f;
  for(i = 0; i < 4; i++) {
    if(instData->port_addr[i] == sent_port) {
      sent_port_idx = i;
      break;
    }
  }
  
  // The port that was just sent is no longer available
  instData->port_status &= ~(1 << sent_port_idx);
  
  return sent_port;

}

// Function: carbonXUtopia2PhyGetCellSize
// Description: Returns 54 if 
// Parameters: None
// Return: port on which a cell was sent
u_int32 carbonXUtopia2PhyRxGetCellSize (){
  return carbonXUtopia2PhyRxGetInstData()->cell_size;
}

// Private functions
UTOPIA2PHYRX_INST_DATA_T * carbonXUtopia2PhyRxGetInstData (void) {

  UTOPIA2PHYRX_INST_DATA_T * instData = 
    (UTOPIA2PHYRX_INST_DATA_T *) carbonXGetInstData();

  if (NULL == instData) {
    carbonXUtopia2PhyRxInit();
    instData = (UTOPIA2PHYRX_INST_DATA_T *) carbonXGetInstData();
  }

  return instData;
}

void carbonXUtopia2PhyRxInit(void) {
  UTOPIA2PHYRX_INST_DATA_T * instData;

  if( (instData = carbonXGetInstData() ) == NULL) {
    instData = carbonmem_malloc(sizeof(UTOPIA2PHYRX_INST_DATA_T));
  }

  if (instData != NULL) {
    carbonXSetInstData(instData);
    instData->port_addr[0]   = 0x1f;
    instData->port_addr[1]   = 0x1f;
    instData->port_addr[2]   = 0x1f;
    instData->port_addr[3]   = 0x1f;
    instData->port_status    = 0;
    instData->flow_ctrl_mode = CELL_FLOW_CTRL;

    // In 16 bit data bus mode, the cell size is 54 bytes
    if(carbonXCsRead(UTOPIA2PHYRX_DATAW) == 16) {
      instData->cell_size = 54;
    }
    // In 8 bit data bus mode, the cell size is 53 bytes
    else {
      instData->cell_size = 53;
    }
    
    // Allocate memory for cell_buf
    instData->cell_buf = carbonmem_malloc(sizeof(u_int8)*instData->cell_size*4);
    if(instData->cell_buf == NULL) MSG_Panic("Failed, allocating memory for instance data.\n");
    else
      memset(instData->cell_buf, 0, sizeof(u_int8)*instData->cell_size*4);

  }
 

 
  else MSG_Panic("Failed, allocating memory for instance data.\n");
}
