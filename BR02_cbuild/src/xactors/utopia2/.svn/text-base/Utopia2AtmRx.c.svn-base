//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2AtmRx.c
 *
 * This file contains the C-API of the Utopia 2 ATM Rx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */

#include "tbp.h"
#include "carbonXUtopia2AtmRxSupport.h"
#include "carbonXUtopia2AtmRx.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"

// Function: carbonXUtopia2AtmRxSetupPolling
// Description: Sets up the status polling mechanism for the Attached ATM Rx Transactor. 
//              Status Polling is done in a round robin scheme where the status for 
//              each port is read and put into a PHY status register. The start_addr 
//              parameter specifies the port where polling should start. The end_addr 
//              parameter specifies the port where polling should end. After polling 
//              the end port, the transactor will continue with the starting one again. 
//              The mphy_clav mode parameter specifies whether one clav or four clav 
//              signals are used. Set mphy_clav_mode to ONE_CLAV_MODE to specify that 
//              one clav signals should be used. Set it to FOUR_CLAV_MODE to specify 
//              that four clav signals should be used. When the transactor is operating 
//              in a single PHY configuration, start- and end_addr should be set to 0, 
//              and mphy_clav_mode should be set to ONE_CLAV_MODE
//
// Parameters: start_addr     -- Port address where to start polling
//             end_addr       -- Port address where to end polling
//             mphy_clav_mode -- How many clav signals to look at 1 or 4
// Return: Nothing
void carbonXUtopia2AtmRxSetupPolling (u_int32 start_addr, 
				   u_int32 end_addr,
				   u_int32 mphy_clav_mode){

  carbonXCsWrite(UTOPIA2ATMRX_POLLSTARTADDR, start_addr);
  carbonXCsWrite(UTOPIA2ATMRX_POLLENDADDR, end_addr);
  carbonXCsWrite(UTOPIA2ATMRX_CLAVMODE, mphy_clav_mode);
}

// Function: carbonXUtopia2AtmRxGetStatus
// Description: Returns the cell available status as read from the RxClav signals 
//              from the PHY.  A one means that the PHY corresponding to port_addr 
//              can accept a cell. A zero means that the PHY corresponding to 
//              port_addr cannot accept any more cells at the moment. When the 
//              transactor is polling for status, the status returned corresponds 
//              to what the status was last time the port, indicated by port_addr, 
//              was polled. In direct status indication mode, the status returned 
//              corresponds to the current value of the port's RxClav signal. 
//
// Parameters: port_addr -- Port address to get status from
// Return: status of selected port. 1 means that the port can accept data. 0 means that
//         it's rate matching fifo is full.
u_int32 carbonXUtopia2AtmRxGetStatus (u_int32 port_addr) {
  u_int32 status = carbonXGetStatus();
  return ( status >> port_addr ) & 0x1;
}

// Function: carbonXUtopia2AtmRxSPhyFlowCtrlMode
// Description: Sets the Flow Control Mode that the attached ATM Rx transactor should 
//              operate in. Set mode to OCTET_FLOW_CTRL if octet (cycle level) flow 
//              control is desired, to CELL_FLOW_CTRL if cell level flow control is 
//              desired. Octet level flow control can only be set when operating in a 
//              single PHY configuration.
// Parameters: mode  -- Flow Control Mode
// Return: Nothing
void carbonXUtopia2AtmRxSetSPhyFlowCtrlMode(u_int32 mode) {

  if(mode != 0 && mode != 1){
    MSG_Error("mode must be either 1 or 0\n");
    return;
  }
  
  carbonXUtopia2AtmRxGetInstData()->flow_ctrl_mode = mode;
  carbonXCsWrite(UTOPIA2ATMRX_FLOWCTRLMODE, mode);
}

// Function: carbonXUtopia2AtmRxSendCell
// Description: Receives a cell of data from the attached ATM Rx Transactor from the port 
//              specified by the port_addr parameter. The Cell is always 53 bytes long
//              in 8 bit databus mode and 54 bytes long in 16 bit databus mode and the 
//              data is pointed to by the cell_buf parameter. When operating in a single 
//              PHY configuration port_addr is ignored. If port_addr is a larger number 
//              than 30 en error will be created. The function first checks if the port
//              requested can accept more data. If it cannot, an error is generated and
//              the function returns without sending any data.
//
// Parameters: port_addr -- Port address to receive the cell from
//             cell_buf  -- Pointer to the cell getting send
// Return: Nothing
u_int32 carbonXUtopia2AtmRxReceiveCell(u_int32 port_addr, u_int8 *cell_buf){

  u_int32 size=0;

  // Check if the port is available to receive from
  if( carbonXUtopia2AtmRxGetStatus(port_addr) ) {
    size = carbonXReadArray8(port_addr, cell_buf, carbonXUtopia2AtmRxGetInstData()->cell_size);
  }
  else {
    MSG_Error("Trying to receive a cell from a port(%d) that has no data available.\n", port_addr);
  }

  return size;
  
}

UTOPIA2ATMRX_INST_DATA_T * carbonXUtopia2AtmRxGetInstData (void) {

  UTOPIA2ATMRX_INST_DATA_T * instData = 
    (UTOPIA2ATMRX_INST_DATA_T *) carbonXGetInstData();

  if (NULL == instData) {
    carbonXUtopia2AtmRxInit();
    instData = (UTOPIA2ATMRX_INST_DATA_T *) carbonXGetInstData();
  }

  return instData;
}

u_int32 carbonXUtopia2AtmRxGetCellSize (){
  return carbonXUtopia2AtmRxGetInstData()->cell_size;
}

void carbonXUtopia2AtmRxPrintCell(u_int8 *cell){
  u_int32 i;
  for (i=0; i<carbonXUtopia2AtmRxGetCellSize(); i++) {
    if(i % 8 == 0) {
      MSG_Printf("\n");
      MSG_Milestone("data = ");
    }
    MSG_Printf("%02x ", cell[i]);
  }
  MSG_Printf("\n");
}

void carbonXUtopia2AtmRxInit(void) {
  UTOPIA2ATMRX_INST_DATA_T * instData;

  if( (instData = carbonXGetInstData() ) == NULL) {
    instData = carbonmem_malloc(sizeof(UTOPIA2ATMRX_INST_DATA_T));
  }

  if (instData != NULL) {
    carbonXSetInstData(instData);

    instData->flow_ctrl_mode = CELL_FLOW_CTRL;
    carbonXCsWrite(UTOPIA2ATMRX_FLOWCTRLMODE,CELL_FLOW_CTRL);

    // In 16 bit data bus mode, the cell size is 54 bytes
    if(carbonXCsRead(UTOPIA2ATMRX_DATAW) == 16)
      instData->cell_size = 54;
    // In 8 bit data bus mode, the cell size is 53 bytes
    else
      instData->cell_size = 53;

  }
  
  else MSG_Panic("Failed, allocating memory for instance data.\n");
}


