//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXUtopia2PhyTxSupport.h
 *
 * This file contains private definitions used for the C-API of the Utopia 2 
 * PHY Tx Transactor.
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Oct 4, 2005
 *
 *****************************************************************************
 */
#ifndef UTOPIA2PHYTXSUPPORT_H
#define  UTOPIA2PHYTXSUPPORT_H

// CS Write Address Map
#define UTOPIA2PHYTX_FLOWCTRLMODE (0<<2)
#define UTOPIA2PHYTX_CLAVMODE     (1<<2)
#define UTOPIA2PHYTX_PORTADDR0    (2<<2)
#define UTOPIA2PHYTX_PORTADDR1    (3<<2)
#define UTOPIA2PHYTX_PORTADDR2    (4<<2)
#define UTOPIA2PHYTX_PORTADDR3    (5<<2)
#define UTOPIA2PHYTX_STATUS       (6<<2)

// CS Read Address Map
#define UTOPIA2PHYTX_DATAW        (256 + (0<<2))


typedef struct {
  u_int32 port_addr[4];
  u_int32 port_status;
  u_int32 flow_ctrl_mode;
  u_int32 cell_size;

} UTOPIA2PHYTX_INST_DATA_T;


UTOPIA2PHYTX_INST_DATA_T * carbonXUtopia2PhyTxGetInstData (void);
void carbonXUtopia2PhyTxInit(void);

#endif
