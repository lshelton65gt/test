//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**********************************************************
 ** Title      : Utopia 2 ATM TX Transactor              **
 ** Created    : October 4, 2005        	         **
 ** Author     : 
 ** References : The ATM Forum Utopia 2, Version 1.0     **
 **                  af-phy-0039.000			 **
 **              The ATM Forum Utopia Specification      **
 **                Level 1, Version 2.01                 **
 **                  af-phy-0017.000			 **
 **                                                      **
 **********************************************************/

/*********************************************************************
 ** Utopia 2 ATM Side TX Transactor
 ** This transactor drives ATM cells out.  Flow control is done with
 ** txenb and txfulln/txclav.  It may be instantiated and connected
 ** to single or multiple PHY devices.  Data bus width may be 8 or
 ** 16 bits.  
 **********************************************************************/ 

`timescale 1 ns / 1 ps

module carbonx_utopia2_atmtx (
  iResetn,
  oTxClk,
  oTxData,
  oTxPrty,
  oTxSOC,
  oTxEnbn,
  oTxAddr,
  iTxFulln_TxClav0,
  iTxClav3_1
  );

  parameter          DATAW        = 16;
   
  input              iResetn;
  output             oTxClk;
  output [DATAW-1:0] oTxData;
  output             oTxPrty;
  output             oTxSOC;
  output             oTxEnbn;
  output [4:0]       oTxAddr;
  input              iTxFulln_TxClav0;
  input  [3:1]       iTxClav3_1;

`protect
  // carbon license crbn_vsp_exec_xtor_utopia2
  
  /******************************************************
   ** Clock Period Parameter for non-Carbon Simulation **
   ******************************************************/
`ifdef CARBONX_NOT_RUNNING_CARBON
   parameter 	      TXCLK_PERIOD     = 20;
`endif

  /*****************************************
   ** XIF Core Parameters                 **
   *****************************************/
  parameter 		     BFMid     = 1;
  parameter 		     ClockId   = 1;
  parameter 		     DataWidth = 32;
  parameter 		     DataAdWid = 11;	// Max is 15
  parameter 		     GCSwid    = 32*4;  // Max is 256 bytes
  parameter 		     PCSwid    = 32*1;  // Max is 256 bytes
  
  /*****************************************
   * State Parameters
   *****************************************/
  parameter                  IDLE            = 0;
  parameter                  PORT_SELECT     = 1;
  parameter                  TRANSMIT_WINDOW = 2;
  parameter                  OCTET_FLOW_STOP = 3;
  
  /*****************************************
   * Config Register Parameters
   *****************************************/
  parameter
    OCTET_FLOW_CTRL = 0,
    CELL_FLOW_CTRL  = 1,
    ONE_CLAV        = 0,
    FOUR_CLAV       = 1;

`include "xif_core.vh"   // Use the transactor template
   
  /*************************************
   ** Variables needed for transactor **
   ************************************/
  reg        data_width;
  reg [31:0] size_of_cell;
  reg        xrun;
  reg [7:0]  state;
  reg [30:0] clav_status_reg;
  reg [7:0]  octet_count;
  reg [7:0]  octet_flow_ctrl_cnt;
  reg [4:0]  poll_addr;
  reg [4:0]  poll_txaddr;
  reg [4:0]  poll_txaddr_r1;
  reg        poll_cycle;
  reg [4:0]  curr_port;
  reg        disable_polling;
  wire       flow_ctrl_mode;
  wire       clav_mode;
  wire [4:0] polling_start;
  wire [4:0] polling_end;
  wire [7:0] curr_8bit_byte0;
  wire [7:0] curr_8bit_byte1;
  wire [7:0] curr_8bit_byte2;
  wire [7:0] curr_8bit_byte3;
  wire [15:0] curr_16bit_byte01;
  wire [15:0] curr_16bit_byte23;
  wire [DATAW-1:0] curr_output_data;

  /*************************************
   ** Initialize the outputs          **
   ************************************/
  wire [DATAW-1:0] oTxData;
  wire             oTxPrty;
  reg              oTxSOC;
  reg              oTxEnbn;
  wire [4:0]       oTxAddr;

  initial begin 
    data_width <= (DATAW == 16) ? 1 : 0;
    // The number of octets in a cell is 53 in 8-bit mode and 54 in 16-bit mode
    size_of_cell <= (DATAW == 16) ? 32'd54 : 32'd53;
    oTxSOC  <= 1'b0;
    oTxEnbn <= 1'b1;
    xrun <= 0;
    state <= IDLE;
    clav_status_reg <= 30'b0;
    octet_count <= 0;
    octet_flow_ctrl_cnt <= 0;
    poll_addr <= 0;
    curr_port <= 5'h1F;
    disable_polling <= 0;
    poll_cycle <= 0;
    poll_txaddr <= 5'h0;
    poll_txaddr_r1 <= 5'h0;
 end

  /*************************************
   ** Generate Transmit Clock         **
   ************************************/
`ifdef CARBONX_NOT_RUNNING_CARBON
   reg 	oTxClk;
   initial oTxClk = 0;
   always #(TXCLK_PERIOD/2) oTxClk = !oTxClk;
`else
`endprotect   
   wire oTxClk; // carbon depositSignal
`protect   
`endif	 

 /*************************************
  ** XIF_core interface
  ************************************/
  assign 			   BFM_xrun           = xrun; 
  assign 			   BFM_clock          = oTxClk; 
  assign 			   BFM_put_operation  = BFM_NXTREQ;
  assign 			   BFM_interrupt      = 0;
  assign 			   BFM_put_size       = 0;
  assign 			   BFM_put_address    = 0;
  assign 			   BFM_put_data       = 0;
  assign 			   BFM_put_cphwtosw   = 0; 
  assign 			   BFM_put_cpswtohw   = 0; 
  assign 			   BFM_put_dwe        = 0;
  assign 			   BFM_reset          = !iResetn;
  wire TBP_reset;
  assign TBP_reset = !iResetn;
 
 /*********************************
  ** Continuous assignments      **
  *********************************/ 
  // Address the XIF core by incrementing every 4 octets
  assign BFM_gp_daddr = (octet_count + 1 + data_width) >> 2;

  // The following wires do byte swizzling on the 32-bits that come from the
  // XIF_core into 8 or 16 bit output data.
  assign curr_8bit_byte0 = XIF_get_data[7:0];
  assign curr_8bit_byte1 = XIF_get_data[15:8];
  assign curr_8bit_byte2 = XIF_get_data[23:16];
  assign curr_8bit_byte3 = XIF_get_data[31:24];
  assign curr_16bit_byte01 = {XIF_get_data[7:0], XIF_get_data[15:8]};
  assign curr_16bit_byte23 = {XIF_get_data[23:16], XIF_get_data[31:24]};

  assign curr_output_data = data_width ? (// 16-bit mode
                                          (octet_count[1] == 0) ?   curr_16bit_byte01 :
                                                                    curr_16bit_byte23) :
                                         (// 8-bit mode
                                          (octet_count[1:0] == 0) ? curr_8bit_byte0 :
                                          (octet_count[1:0] == 1) ? curr_8bit_byte1 :
                                          (octet_count[1:0] == 2) ? curr_8bit_byte2 :
                                                                    curr_8bit_byte3 );

  assign oTxData = curr_output_data[DATAW-1:0];
  assign oTxPrty = ^curr_output_data[DATAW-1:0];
  assign oTxAddr = (state == PORT_SELECT) ? XIF_get_address[4:0] :
                                            poll_txaddr;


  // Receiving CLAV status
  //   When there is a single phy, clav_mode is set to ONE_CLAV and both
  //   polling_start and polling_end should be set to zero.
  //   Then bit zero of the status word has the clav info for the phy.
  //
  //   When there are multi-phys with one clav coming to the ATM side, 
  //   clav_mode is set to ONE_CLAV and polling_start/polling_end are set
  //   as needed by the number of phy devices/ports connected to the one 
  //   clav signal.  All the status bits between polling_start and polling_end
  //   contain valid clav status for the ports.
  //
  //   When there are multi-phys with four clavs coming to the ATM side,
  //   clav_mode is set to FOUR_CLAV and polling_start/polling_end must
  //   be set to a group address (0, 4, 8, ...).  

  always @(posedge oTxClk or negedge iResetn) begin
    if(iResetn == 0) begin
      clav_status_reg <= 30'b0;
      poll_addr <= 5'h0;
      poll_txaddr <= 5'h0;
      poll_txaddr_r1 <= 5'h0;
      poll_cycle <= 0;
    end
    else begin
       
      // Pipeline poll_txaddr to use
      poll_txaddr_r1 <= poll_txaddr;
      poll_cycle <= !poll_cycle;
 
      if (disable_polling == 0) begin
        if (clav_mode == ONE_CLAV) begin
          clav_status_reg <= (clav_status_reg & ~(30'h1 << poll_txaddr_r1)) | 
                             ({29'h0, iTxFulln_TxClav0} << poll_txaddr_r1);
          // Only increment every other cycle and also only drive the
          // output every other cycle
          if (poll_addr == polling_end) 
            poll_addr <= poll_cycle ? poll_addr : polling_start;
          else
            poll_addr <= poll_cycle ? poll_addr : poll_addr + 1;
        end 
        else begin // clav mode is FOUR_CLAV
          clav_status_reg <= (clav_status_reg & ~(30'hF << poll_txaddr_r1)) | 
                             ({26'h0, 
                               iTxClav3_1[3:1],
                               iTxFulln_TxClav0} << poll_txaddr_r1);
          // When in four clav mode, always poll by groups
          // Only increment every other cycle and also only drive the
          // output every other cycle
          if (poll_addr == polling_end) 
            poll_addr <= poll_cycle ? poll_addr : polling_start;
          else
            poll_addr <= poll_cycle ? poll_addr : poll_addr + 4;
        end
        // When only one address or group is being polled don't put
        //   the NULL cycle inbetween address changes.
        if (polling_start != polling_end)
          poll_txaddr <= poll_cycle ? poll_addr : 5'h1F;
        else
          poll_txaddr <= polling_start; 
      end
    end
  end

  always @(posedge oTxClk or negedge iResetn) begin
    if(iResetn == 0) begin
      xrun <= 0;
      state <= IDLE;
      octet_count <= 0;
      octet_flow_ctrl_cnt <= 0;
      curr_port <= 5'h1F;
      disable_polling <= 0;
      oTxSOC  <= 1'b0;
      oTxEnbn <= 1'b1;
    end
    else begin
      case (state)
        IDLE : begin
          // Idle state waits for a transaction to start
          if((XIF_xav == 1'b1) && 
             (XIF_get_operation == BFM_WRITE)) begin
            if (XIF_get_size != size_of_cell) begin
              $display("ERROR -- Write operation size = %d is not %d bytes\n",
                        XIF_get_size, size_of_cell);
            end
            xrun <= 1'b1;
            octet_count <= 0;
            if (curr_port == XIF_get_address[4:0]) begin
              oTxEnbn <= 1'b0;
              oTxSOC <= 1'b1;
              state <= TRANSMIT_WINDOW;
            end
            else begin
              oTxEnbn <= 1'b1;
              oTxSOC <= 1'b0;
              disable_polling <= 1;
              curr_port <= XIF_get_address[4:0];
              state <= PORT_SELECT;
            end
          end else begin
            state <= IDLE;
          end
        end
        PORT_SELECT : begin
          // poll_cycle == 1 means that we are in the non-NULL address output
          //   cycle and can continue with the cell transmit on the next cycle.
          //   If we are in the NULL address output cycle then we must continue
          //   in port select for one more cycle.
          if (poll_cycle == 1) begin
            oTxEnbn <= 1'b0;
            oTxSOC <= 1'b1;
            disable_polling <= 0;
            state <= TRANSMIT_WINDOW;
          end
          else begin
            oTxEnbn <= 1'b1;
            oTxSOC <= 1'b0;
            disable_polling <= 1;
            state <= PORT_SELECT;
          end
        end
        TRANSMIT_WINDOW : begin
          oTxEnbn <= 1'b0;
          oTxSOC <= 1'b0;
          // Look for the last cycle.  The last cycle depends on whether
          // we are in 8 or 16 bit data bus mode.  
          if((data_width == 1) && (octet_count == (size_of_cell - 4)) ||
             (data_width == 0) && (octet_count == (size_of_cell - 2))) begin
            xrun        <= 1'b0;
            octet_count <= data_width ? octet_count+2 : octet_count+1;
            octet_flow_ctrl_cnt <= 0;
            state <= IDLE;
          end
          else begin
            // In the middle of outputing a cell
            octet_count <= data_width ? octet_count+2 : octet_count+1;
            // Check for octet flow control to stop the output
            if (flow_ctrl_mode == OCTET_FLOW_CTRL) begin
              if (octet_flow_ctrl_cnt == 8'h4) begin
                octet_flow_ctrl_cnt <= 0;
                state <= OCTET_FLOW_STOP;
              end 
              else if (iTxFulln_TxClav0 == 0) begin // TxFulln is asserted for less than 4 cycles
                octet_flow_ctrl_cnt <= octet_flow_ctrl_cnt + 1;
                state <= TRANSMIT_WINDOW;
              end
              else begin // TxFulln is not asserted
                octet_flow_ctrl_cnt <= 0;
                state <= TRANSMIT_WINDOW;
              end
            end
            else begin // In Cell flow control
              state <= TRANSMIT_WINDOW;
            end
          end
        end
        OCTET_FLOW_STOP : begin
          // The only way we get here is when in single phy mode and octet flow control mode
          if (iTxFulln_TxClav0 == 0) state <= OCTET_FLOW_STOP;
          else                       state <= TRANSMIT_WINDOW;
        end
      endcase
    end
  end

 /********************************************
  **  ControlStatus Write Task              **
  **  Update Programmable Register Values   ** 
  *******************************************/
  //  flow_ctrl_mode can be OCTET (0) or CELL (1) 
  assign flow_ctrl_mode    = XIF_get_csdata[ 0 ];       // Addr  0
  //  clav_mode can be ONECLAV (0) or FOURCLAV (1)
  assign clav_mode         = XIF_get_csdata[ 32 ];      // Addr  1
  assign polling_start     = XIF_get_csdata[  68:64 ];  // Addr  2
  assign polling_end       = XIF_get_csdata[ 100:96 ];  // Addr  3

  /*************************************************
  ** CS_Read Task, reading current value of the  **
  ** programmable registers                      **
  *************************************************/
  // send the polled clav status back on the status wires
  assign BFM_put_status = {1'b0, clav_status_reg};

  assign BFM_put_csdata[  31:  0] = DATAW;       // Addr 0

`endprotect  
endmodule
