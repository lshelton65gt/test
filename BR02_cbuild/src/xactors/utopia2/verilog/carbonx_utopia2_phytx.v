//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**********************************************************
 ** Title      : Utopia 2 PHY TX Transactor              **
 ** Created    : October 4, 2005        	         **
 ** Author     : 
 ** References : The ATM Forum Utopia 2, Version 1.0     **
 **                  af-phy-0039.000			 **
 **              The ATM Forum Utopia Specification      **
 **                Level 1, Version 2.01                 **
 **                  af-phy-0017.000			 **
 **                                                      **
 **********************************************************/

/*********************************************************************
 ** Utopia 2 PHY Side TX Transactor
 ** This transactor receives ATM cells.  Flow control is done with
 ** txenb and txfulln/txclav.  
 **********************************************************************/ 

`timescale 1 ns / 1 ps

module carbonx_utopia2_phytx (
  iResetn,
  iTxClk,
  iTxData,
  iTxPrty,
  iTxSOC,
  iTxEnbn,
  iTxAddr,
  oTxFulln_TxClav0,
  oTxClav3_1
  );

  parameter          DATAW = 16;
   
  input               iResetn;
  input               iTxClk;
  input  [DATAW-1:0]  iTxData;
  input               iTxPrty;
  input               iTxSOC;
  input               iTxEnbn;
  input  [4:0]        iTxAddr;
  output              oTxFulln_TxClav0;
  output [3:1]        oTxClav3_1;

`protect
  // carbon license crbn_vsp_exec_xtor_utopia2
  
  /*****************************************
   ** XIF Core Parameters                 **
   *****************************************/
  parameter 		     BFMid     = 1;
  parameter 		     ClockId   = 1;
  parameter 		     DataWidth = 32;
  parameter 		     DataAdWid = 11;	   // Max is 15
  parameter 		     GCSwid    = 32*7;  // Max is 256 bytes
  parameter 		     PCSwid    = 32*1;  // Max is 256 bytes
  
  /*****************************************
   * State Parameters
   *****************************************/
  parameter                  IDLE            = 0;
  parameter                  TRANSMIT_WINDOW = 1;
  
  /*****************************************
   * Config Register Parameters
   *****************************************/
  parameter
    ONE_CLAV        = 0,
    FOUR_CLAV       = 1;
  
`include "xif_core.vh"   // Use the transactor template
   
  /*************************************
   ** Variables needed for transactor **
   ************************************/
  reg        data_width;
  reg [31:0] size_of_cell;
  reg        xrun;
  reg [7:0]  state;
  reg [7:0]  octet_count;
  reg [4:0]  curr_port;
  reg  [3:0] clav_reg;
  reg  [3:0] clav_enable;
  reg [31:0] put_data;
  reg        put_dwe;
  reg [31:0] time_out_cnt;
  wire       flow_ctrl_mode;
  wire       clav_mode;
  wire [4:0] port1_addr;
  wire [4:0] port2_addr;
  wire [4:0] port3_addr;
  wire [4:0] port4_addr;
  wire [3:0] port_status;

  /*************************************
   ** Drive outputs                   **
   ************************************/
  wire       oTxFulln_TxClav0;
  wire [3:1] oTxClav3_1;
  assign oTxFulln_TxClav0 = (clav_enable[0]) ? clav_reg[0] : 1'bz;
  assign oTxClav3_1[1]    = (clav_enable[1]) ? clav_reg[1] : 1'bz;
  assign oTxClav3_1[2]    = (clav_enable[2]) ? clav_reg[2] : 1'bz;
  assign oTxClav3_1[3]    = (clav_enable[3]) ? clav_reg[3] : 1'bz;


  /*************************************
   ** Initialize the registers        **
   ************************************/
  initial begin
    clav_reg <= 4'b0;
    clav_enable <= 4'b0;
    data_width <= (DATAW == 16) ? 1 : 0;
    // The number of octets in a cell is 53 in 8-bit mode and 54 in 16-bit mode
    size_of_cell <= (DATAW == 16) ? 32'd54 : 32'd53;
    xrun <= 0;
    state <= IDLE;
    time_out_cnt <= 0;
    octet_count <= 0;
    curr_port <= 5'h1F;
    put_data <= 0;
    put_dwe  <= 0;
  end

 /*************************************
  ** XIF_core interface
  ************************************/
  assign 			   BFM_xrun           = xrun; 
  assign 			   BFM_clock          = iTxClk; 
  assign 			   BFM_put_operation  = BFM_NXTREQ;
  assign 			   BFM_interrupt      = 0;
  assign 			   BFM_put_status     = 0;
  assign 			   BFM_put_size       = octet_count;
  assign 			   BFM_put_address    = curr_port;
  assign 			   BFM_put_data       = put_data;
  assign 			   BFM_put_cphwtosw   = 0; 
  assign 			   BFM_put_cpswtohw   = 0; 
  assign 			   BFM_put_dwe        = put_dwe;
  assign 			   BFM_reset          = !iResetn;
  wire TBP_reset;
  assign TBP_reset = !iResetn;
 
 /*********************************
  ** Continuous assignments      **
  *********************************/ 
  // Address the XIF core by incrementing every 4 octets
  assign BFM_gp_daddr = (octet_count == 0) ? 0 :
                                             (octet_count - 1 - data_width) >> 2;

  always @(posedge iTxClk or negedge iResetn) begin
    if(iResetn == 0) begin
      curr_port <= 5'h1F;
    end
    else begin
      if (iTxEnbn == 1)
        curr_port <= iTxAddr;
    end
  end

  always @(posedge iTxClk or negedge iResetn) begin
    if(iResetn == 0) begin
      clav_enable[3:0] <= 4'h0;
      clav_reg[3:0] <= 4'h0;
    end
    else begin
      if (iTxAddr == 5'h1F) begin
        clav_enable[3:0] <= 4'h0;
      end
      else begin
        if (clav_mode == ONE_CLAV) begin
          clav_enable[0] <= (iTxAddr == port1_addr) |
                            (iTxAddr == port2_addr) |
                            (iTxAddr == port3_addr) |
                            (iTxAddr == port4_addr);
          clav_enable[3:1] <= 3'b0;
        end
        else begin // FOUR_CLAV mode
          clav_enable[0] <= (iTxAddr[4:2] == port1_addr[4:2]);
          clav_enable[1] <= (iTxAddr[4:2] == port2_addr[4:2]);
          clav_enable[2] <= (iTxAddr[4:2] == port3_addr[4:2]);
          clav_enable[3] <= (iTxAddr[4:2] == port4_addr[4:2]);
        end
      end
      clav_reg[3:0] <= port_status[3:0];
    end
  end

  always @(posedge iTxClk or negedge iResetn) begin
    if(iResetn == 0) begin
      data_width <= (DATAW == 16) ? 1 : 0;
      // The number of octets in a cell is 53 in 8-bit mode and 54 in 16-bit mode
      size_of_cell <= (DATAW == 16) ? 32'd54 : 32'd53;
      xrun <= 0;
      state <= IDLE;
      octet_count <= 0;
      time_out_cnt <= 0;
      put_data <= 0;
      put_dwe  <= 0;
    end
    else begin
      case (state)
        IDLE : begin
          if(((XIF_xav == 1'b1) && 
              (XIF_get_operation == BFM_READ)) || 
               xrun) begin
            if (XIF_get_size != size_of_cell) begin
              $display("ERROR -- Read operation size = %d is not %d bytes\n",
                        XIF_get_size, size_of_cell);
            end
            xrun <= 1'b1;
            // To start receiving a cell we must get a SOC with the port equal
            //   to one of the port addresses.  If the port is null then we
            //   don't respond.
            if ((iTxSOC == 1) && 
                (curr_port != 5'h1F) &&
                ((curr_port == port1_addr) ||
                 (curr_port == port2_addr) ||
                 (curr_port == port3_addr) ||
                 (curr_port == port4_addr))) begin
              octet_count <= data_width ? 2 : 1;
              put_dwe  <= 1;
              if (data_width == 1) begin // 16-bit mode
                if (iTxSOC == 1) begin // When SOC is 1 the first two bytes of data 
                                       // get stuffed into the bottom of put_data;
                  put_data <= {16'h0, iTxData[(DATAW/2)-1:0], iTxData[DATAW-1:DATAW/2]};
                end
                else begin
                  put_data <= (BFM_put_data & ~(16'hFFFF << (octet_count[1] * 16))) |
                              ({iTxData[(DATAW/2)-1:0], iTxData[DATAW-1:DATAW/2]} << (octet_count[1] * 16));
                end
              end
              else begin // 8-bit mode
                if (iTxSOC == 1) begin // When SOC is 1 the first byte of data 
                                       // get stuffed into the bottom of put_data;
                  put_data <= {24'h0, iTxData[7:0]};
                end
                else begin
                  put_data <= (BFM_put_data & ~(8'hFF << (octet_count[1:0] * 8))) |
                              (iTxData << (octet_count[1:0] * 8));
                end
              end
              state <= TRANSMIT_WINDOW;
            end
            else begin
              if (time_out_cnt >= size_of_cell) begin
                xrun <= 0;
                time_out_cnt <= 0;
              end else begin
                // Increment time out count to time out the transaction
                //   after one cell time.
                time_out_cnt <= time_out_cnt + ((data_width == 1) ? 2 : 1);
              end
              octet_count <= 0;
              put_dwe  <= 0;
              state <= IDLE;
            end
          end
          else begin
            put_dwe  <= 0;
            state <= IDLE;
          end
        end
        TRANSMIT_WINDOW : begin
          // Look for the last cycle.  The last cycle depends on whether
          // we are in 8 or 16 bit data bus mode.  
          if((data_width == 1) && (octet_count == (size_of_cell - 2)) ||
             (data_width == 0) && (octet_count == (size_of_cell - 1))) begin
            time_out_cnt <= 0;
            xrun        <= 1'b0;
            state <= IDLE;
          end
          else begin
            state <= TRANSMIT_WINDOW;
          end
          if (iTxEnbn == 0) begin
            octet_count <= data_width ? octet_count+2 : octet_count+1;
            put_dwe  <= 1;
            if (data_width == 1) begin // 16-bit mode
              put_data <= (BFM_put_data & ~(16'hFFFF << (octet_count[1] * 16))) |
                          ({iTxData[(DATAW/2)-1:0], iTxData[DATAW-1:DATAW/2]} << (octet_count[1] * 16));
            end
            else begin // 8-bit mode
              put_data <= (BFM_put_data & ~(8'hFF << (octet_count[1:0] * 8))) |
                          (iTxData << (octet_count[1:0] * 8));
            end
          end 
          else begin
            put_dwe  <= 0;
          end
        end
      endcase
    end
  end

 /********************************************
  **  ControlStatus Write Task              **
  **  Update Programmable Register Values   ** 
  *******************************************/
  //  flow_ctrl_mode can be OCTET (0) or CELL (1) 
  assign flow_ctrl_mode    = XIF_get_csdata[ 0 ];       // Addr  0
  //  clav_mode can be ONE_CLAV (0) or FOUR_CLAV (1)
  assign clav_mode         = XIF_get_csdata[ 32 ];      // Addr  1
  //  Port Address registers -- tells the transactor
  //    which address to respond to when a port
  //    is selected.
  assign port1_addr        = XIF_get_csdata[ 68 :  64]; // Addr  2
  assign port2_addr        = XIF_get_csdata[100 :  96]; // Addr  3
  assign port3_addr        = XIF_get_csdata[132 : 128]; // Addr  4
  assign port4_addr        = XIF_get_csdata[164 : 160]; // Addr  5
  //  Port Status has one bit per port address (0-30)
  //    0->Port Full, ATM side is not permitted to send a cell
  //    1->Port Not Full, ATM side not permitted to send a cell
  assign port_status       = XIF_get_csdata[196 : 192]; // Addr  6

  /*************************************************
  ** CS_Read Task, reading current value of the  **
  ** programmable registers                      **
  *************************************************/
  assign BFM_put_csdata[  31:  0] = DATAW;       // Addr 0

`endprotect
endmodule
