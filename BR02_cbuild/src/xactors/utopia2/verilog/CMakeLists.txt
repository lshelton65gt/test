# Don't do protected verilog on Windows - it should have already been
# done on Linux
if(${WIN32})
  return()
endif()

generate_xtor_vp(utopia2/verilog
                 carbonx_utopia2_atmtx.v
                 carbonx_utopia2_atmrx.v
                 carbonx_utopia2_phytx.v
                 carbonx_utopia2_phyrx.v
                 )
