//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**********************************************************
 ** Title      : Utopia 2 PHY RX Transactor              **
 ** Created    : October 4, 2005        	         **
 ** Author     : 
 ** References : The ATM Forum Utopia 2, Version 1.0     **
 **                  af-phy-0039.000			 **
 **              The ATM Forum Utopia Specification      **
 **                Level 1, Version 2.01                 **
 **                  af-phy-0017.000			 **
 **                                                      **
 **********************************************************/

/*********************************************************************
 ** Utopia 2 ATM Side RX Transactor
 ** This transactor outputs ATM cells.  Flow control is done with
 ** rxenb and rxemptyn/rxclav.
 **********************************************************************/ 

`timescale 1 ns / 1 ps

module carbonx_utopia2_phyrx (
  iResetn,
  iRxClk,
  oRxData,
  oRxPrty,
  oRxSOC,
  iRxEnbn,
  iRxAddr,
  oRxEmptyn_RxClav0,
  oRxClav3_1
  );
   
   parameter                  DATAW = 16;

   input 		     iResetn;
   input 		     iRxClk;
   output [DATAW-1:0] 	     oRxData;
   output 		     oRxPrty;
   output 		     oRxSOC;
   input 		     iRxEnbn;
   input [4:0] 		     iRxAddr;
   output 		     oRxEmptyn_RxClav0;
   output [3:1] 	     oRxClav3_1;
   
   
`protect
  // carbon license crbn_vsp_exec_xtor_utopia2
  
   /*****************************************
    ** XIF Core Parameters                 **
    *****************************************/
   parameter 		     BFMid     = 1;
   parameter 		     ClockId   = 1;
   parameter 		     DataWidth = DATAW*4;
   parameter 		     DataAdWid = 11;	// Max is 15
   parameter 		     GCSwid    = 32*6;  // Max is 256 bytes
   parameter 		     PCSwid    = 32*2;  // Max is 256 bytes
   defparam 		     XIFcore.GCSDefaultData = 192'h0000001f_0000001f_0000001f_0000001f_00000000_00000000;
   
   /*****************************************
    * State Parameters
    *****************************************/
   parameter 		     PRIME           = 0;
   parameter 		     IDLE            = 1;
   parameter 		     RECEIVE_WINDOW  = 2;
   
   /*****************************************
    * Config Register Parameters
    *****************************************/
   parameter 		     
     OCTET_FLOW_CTRL = 0,
       CELL_FLOW_CTRL  = 1,
       ONE_CLAV        = 0,
       FOUR_CLAV       = 1;
   
`include "xif_core.vh"   // Use the transactor template
   
   /*************************************
    ** Variables needed for transactor **
    ************************************/
   reg 			     data_width;
   reg [31:0] 		     size_of_cell;
   reg 			     xrun;
   reg [7:0] 		     state;
   reg [7:0] 		     octet_count;
   reg [3:0] 		     clav_reg;
   reg [3:0] 		     clav_enable;
   reg [4:0] 		     port_request_addr;
   reg 			     curr_soc;
   wire 		     out_enable;
   wire 		     flow_ctrl_mode;
   wire 		     clav_mode;
   wire [7:0] 		     curr_8bit_byte0;
   wire [7:0] 		     curr_8bit_byte1;
   wire [7:0] 		     curr_8bit_byte2;
   wire [7:0] 		     curr_8bit_byte3;
   wire [15:0] 		     curr_16bit_byte01;
   wire [15:0] 		     curr_16bit_byte23;
   wire [DATAW-1:0] 	     curr_output_data;
   wire [4:0] 		     port1_addr;
   wire [4:0] 		     port2_addr;
   wire [4:0] 		     port3_addr;
   wire [4:0] 		     port4_addr;
   reg [3:0] 		     port_status;
   wire                      port_avail;
   reg 			     curr_port_avail;
   wire [3:0] 		     port_mask;
   reg 			     RxEnbn_dly;
			     
   /*************************************
    ** XIF Interface Signals           **
    ************************************/
   wire 		     fifo_re;
   reg [DataAdWid-1:0] 	     gp_daddr;
   reg [31:0] 		     put_status;
   reg [DATAW*4-1:0] 	     xif_fifo_out;
   reg [3:0] 		     xif_port_status;
   
   reg [3:0]  wr_count,		// fifo write index
	      rd_count,		// fifo read index
	      rd_addr,		// fifo core read address
	      depth;		// current fifo depth
   
   reg [DATAW*4-1:0] ram_core[0:15];	// fifo store
   wire       fifo_af, full, empty;
   reg        fifo_we;
   reg        fifo_we_rp1;

   /*************************************
   ** Drive outputs                   **
    ************************************/
   wire 		     oRxEmptyn_RxClav0;
   wire [3:1] 		     oRxClav3_1;
   assign 		     oRxEmptyn_RxClav0 = (clav_enable[0]) ? clav_reg[0] : 1'bz;
   assign 		     oRxClav3_1[1]     = (clav_enable[1]) ? clav_reg[1] : 1'bz;
   assign 		     oRxClav3_1[2]     = (clav_enable[2]) ? clav_reg[2] : 1'bz;
   assign 		     oRxClav3_1[3]     = (clav_enable[3]) ? clav_reg[3] : 1'bz;
   

   /*************************************
    ** Initialize the registers        **
    ************************************/
   initial begin
      data_width <= (DATAW == 16) ? 1 : 0;
      // The number of octets in a cell is 53 in 8-bit mode and 54 in 16-bit mode
      size_of_cell <= (DATAW == 16) ? 32'd54 : 32'd53;
      xrun <= 0;
      state <= PRIME;
      octet_count <= 0;
      clav_reg <= 4'b0;
      clav_enable <= 4'b0;
      port_request_addr <= 5'h1F;
      curr_soc <= 0;
   end
   
   /*************************************
    ** XIF_core interface
    ************************************/
   assign BFM_xrun           = xrun; 
   assign BFM_clock          = iRxClk; 
   assign BFM_put_operation  = BFM_NXTREQ;
   assign BFM_interrupt      = 0;
   assign BFM_put_size       = 0;
   assign BFM_put_status     = put_status;
   assign BFM_put_address    = {60'd0, port_request_addr};
   assign BFM_put_data       = 0;
   assign BFM_put_cphwtosw   = 0; 
   assign BFM_put_cpswtohw   = 0; 
   assign BFM_put_dwe        = 0;
   assign BFM_reset          = !iResetn;
   assign BFM_gp_daddr       = gp_daddr;
   wire   TBP_reset;
   assign TBP_reset          = !iResetn;
   
   /*********************************
    ** Continuous assignments      **
    *********************************/ 
   // Address the XIF core by incrementing every 4 octets
   assign curr_output_data = (port_request_addr == port1_addr) ? xif_fifo_out[(DATAW*1)-1:(DATAW*0)] : 
			     (port_request_addr == port2_addr) ? xif_fifo_out[(DATAW*2)-1:(DATAW*1)] :
			     (port_request_addr == port3_addr) ? xif_fifo_out[(DATAW*3)-1:(DATAW*2)] :
			     (port_request_addr == port4_addr) ? xif_fifo_out[(DATAW*4)-1:(DATAW*3)] :
			     0;
   
   wire [DATAW-1:0] oRxData;
   wire 	    oRxPrty;
   wire 	    oRxSOC;
   assign 	    oRxData = (out_enable == 1) ? curr_output_data[DATAW-1:0] :   'hz;
   assign 	    oRxPrty = (out_enable == 1) ? ^curr_output_data[DATAW-1:0] : 1'bz;
   assign 	    oRxSOC  = (out_enable == 1) ? curr_soc : 1'bz;
   
   // Update port_request_addr register with iRxAddr when RxEnb is deasserted
   always @(posedge iRxClk or negedge iResetn) begin
      if(iResetn == 0) begin
	 port_request_addr <= 5'h1F;
      end
      else begin
	 if (iRxEnbn == 1)
           port_request_addr <= iRxAddr;
      end
   end
   
   always @(posedge iRxClk or negedge iResetn) begin
      if(iResetn == 0) begin
	 clav_enable[3:0] <= 4'h0;
	 clav_reg[3:0] <= 4'h0;
      end
      else begin
      if (iRxAddr == 5'h1F) begin
         clav_enable[3:0] <= 4'h0;
      end
      else begin
         if (clav_mode == ONE_CLAV) begin
            clav_enable[0] <= (iRxAddr == port1_addr) |
                              (iRxAddr == port2_addr) |
                              (iRxAddr == port3_addr) |
                              (iRxAddr == port4_addr);
            clav_enable[3:1] <= 3'b0;
         end
         else begin // FOUR_CLAV mode
            clav_enable[0] <= (iRxAddr[4:2] == port1_addr[4:2]);
            clav_enable[1] <= (iRxAddr[4:2] == port2_addr[4:2]);
            clav_enable[2] <= (iRxAddr[4:2] == port3_addr[4:2]);
            clav_enable[3] <= (iRxAddr[4:2] == port4_addr[4:2]);
         end
      end
	 clav_reg[3:0] <= port_status[3:0];
      end
   end
   
   assign port_avail  = (port_request_addr == port1_addr && (port_status & 1) ) ||
			(port_request_addr == port2_addr && (port_status & 2) ) ||
			(port_request_addr == port3_addr && (port_status & 4) ) ||
			(port_request_addr == port4_addr && (port_status & 8) );
   assign port_mask   = {(port_request_addr == port4_addr),
			 (port_request_addr == port3_addr),
			 (port_request_addr == port2_addr),
			 (port_request_addr == port1_addr)};

   // Output enable when any of our ports are selected
   assign out_enable  = ((port_request_addr == port1_addr) ||
			 (port_request_addr == port2_addr) ||
			 (port_request_addr == port3_addr) ||
			 (port_request_addr == port4_addr)) && 
			(RxEnbn_dly == 0) && (port_request_addr != 5'h1f);

   assign fifo_re     = (curr_port_avail || port_avail) && !iRxEnbn;
   
   /*********************************
    ** Main State Machine          **
    *********************************/ 
   always @(posedge iRxClk or negedge iResetn) begin
      if(iResetn == 0) begin
	 xrun        <= 0;
	 state       <= PRIME;
	 octet_count <= 0;
	 curr_soc    <= 1'b0;
	 RxEnbn_dly  <= 0;
      end
      else begin       

	 // Pipeline RxEnbn
	 RxEnbn_dly <= iRxEnbn;
	 
	 case (state)
	   
	   // If we have no cells available on any port, keep reading from the xif_core until we do
	   PRIME : begin
	      port_status <= 4'b0000;

	      // Wait until FIFO is almost full
	      if(fifo_af) state <= IDLE;
	   end
           IDLE : begin
	      port_status <= xif_port_status;
	      
	      if(iRxEnbn == 0) begin
		 octet_count <= 0;
		 //fifo_re     <= 0;
		 
		 // Wait until we have available data on the selected port
		 if(port_avail) begin
		    state      <= RECEIVE_WINDOW;
		    curr_soc   <= 1'b1;
		    curr_port_avail <= 1;
		    //fifo_re    <= 1'b1;
		 end
		 else begin
		    state <= IDLE;
		 end // else: !if( (port_request_addr == port1_addr) ||...
	      end // if (iRxEnbn == 0)
           end // case: IDLE
	   
           RECEIVE_WINDOW : begin
	      port_status <= xif_port_status;

	      if(iRxEnbn == 0) begin
		 //fifo_re  <= 1'b1;
		 curr_soc <= 1'b0;
		 // Look for the last cycle.  The last cycle depends on whether
		 // we are in 8 or 16 bit data bus mode.  
		 if((data_width == 1) && (octet_count == (size_of_cell - 4)) ||
		    (data_width == 0) && (octet_count == (size_of_cell - 2))) begin
		    octet_count <= data_width ? octet_count+2 : octet_count+1;
		    if(xrun == 1)
		      state <= IDLE;
		    else
		      state <= PRIME;
		    curr_port_avail <= 0;
		 end
		 else begin
		    // In the middle of outputing a cell
		    octet_count <= data_width ? octet_count+2 : octet_count+1;
		    state <= RECEIVE_WINDOW;
		 end
	      end // if (iRxEnbn == 0)
	      else begin
		 state <= state;
	      end // else: !if(iRxEnbn == 0)
	      
           end // case: RECEIVE_WINDOW

	   //default : state <= PRIME;
	     
	 endcase // case(state)
	 
	 if(iRxEnbn == 1'b1 && state != PRIME) begin // if RxEnb is deasserted then turn off the data, parity 
            //   and soc drivers and let the cycle pass
	 end
      end // else: !if(iResetn == 0)
   end // always @ (posedge iRxClk or negedge iResetn)

   /*********************************
    ** XIF Core Interface          **
    *********************************/ 
   always @(posedge iRxClk or negedge iResetn) begin
      if(iResetn == 0) begin
	 xif_fifo_out    <= 0;
	 gp_daddr        <= 0;
	 xif_port_status <= 0;
	 fifo_we_rp1     <= 0;
	 fifo_we         <= 0;
      end
      else begin

	 // wait for a transaction to start
	 if((XIF_xav == 1'b1) && (XIF_get_operation == BFM_WRITE)) begin
            if (XIF_get_size != (size_of_cell*4)) begin
               $display("ERROR -- Write operation size = %d is not %d bytes\n",
			XIF_get_size, size_of_cell);
            end
            xrun            <= 1'b1;
	    xif_port_status <= XIF_get_address[3:0];
	 end
	 else if(state == IDLE &&
		 port_avail    &&
		 iRxEnbn == 0) begin
	    // Clear port status for the current port, when start transmitting
	    xif_port_status <= xif_port_status & ~(port_mask);
	 end
	 
	 // Clear gp_daddr on the last cycle of the transaction
	 if(XIF_xav && XIF_get_operation == BFM_WRITE) begin
	    gp_daddr <= 1;
	    fifo_we  <= 1;
	 end
	 else if(!fifo_af && xrun) begin
	    gp_daddr <= gp_daddr + 1;
	    fifo_we  <= 1;
	 end
	 else
	   fifo_we   <= 0;

	 // fifo_we <= fifo_we_rp1;
	 
	 if(!fifo_af) begin
	    // Signal End of Transaction
	    if(gp_daddr == (data_width ? 26 : 52) ) begin
	       xrun        <= 0;
	       put_status  <= port_request_addr;
	    end
	 end
      end // else: !if(iResetn == 0)
   end // always @ (posedge iRxClk or negedge iResetn)

   /*********************************************
    * Fifo Logic Between XIF Core and Output    *
    *********************************************/
   assign full    = (wr_count >= rd_count) ? ((wr_count-rd_count)==4'hf) : ((rd_count-wr_count)<=1);
   assign fifo_af = (wr_count >= rd_count) ? ((wr_count-rd_count)>=4'h5) : ((rd_count-wr_count)<=4'd12);
   assign empty   = (wr_count==rd_count);

   /*****************************
    * FIFO   *
    ****************************/
   always @(posedge iRxClk or negedge iResetn) begin
	if (~iResetn)
	  begin
	     wr_count <= 4'b0;
	     rd_count <= 4'b0;
	     rd_addr  <= 4'b0;
	  end // if (~iResetn)
	else
	  begin
	     rd_count <= rd_addr;

	     // Write Logic
	     if (fifo_we & ~full)
	       begin
		  ram_core[wr_count] <= XIF_get_data;
		  wr_count <= wr_count+1;
	       end // if (buf_we)

	     // Read Logic
	     if (fifo_re & ~empty)
	       begin
		  rd_addr      <= rd_addr + 1;
		  xif_fifo_out <= ram_core[rd_addr];
	       end // if (re & ~empty)
	     
	  end // else: !if(~iResetn)
     end // always @ (posedge clk or negedge iResetn)
      
   /********************************************
    **  ControlStatus Write Task              **
    **  Update Programmable Register Values   ** 
    *******************************************/
   //  flow_ctrl_mode can be OCTET (0) or CELL (1) 
   assign flow_ctrl_mode    = XIF_get_csdata[ 0 ];       // Addr  0
   //  clav_mode can be ONE_CLAV (0) or FOUR_CLAV (1)
   assign clav_mode         = XIF_get_csdata[ 32 ];      // Addr  1
   //  Port Address registers -- tells the transactor
   //    which address to respond to when a port
   //    is selected.
   assign port1_addr        = XIF_get_csdata[ 68 :  64]; // Addr  2
   assign port2_addr        = XIF_get_csdata[100 :  96]; // Addr  3
   assign port3_addr        = XIF_get_csdata[132 : 128]; // Addr  4
   assign port4_addr        = XIF_get_csdata[164 : 160]; // Addr  5
   //  Port Status has one bit per port address (0-30)
   //    0->Port Not Empty, ATM side is not permitted to request a cell
   //    1->Port Empty, ATM side not permitted to request a cell

   /*************************************************
    ** CS_Read Task, reading current value of the  **
    ** programmable registers                      **
    *************************************************/
   assign BFM_put_csdata[  31:  0] = DATAW;               // Addr 0
   assign BFM_put_csdata[  63: 32] = port_request_addr;   // Addr 1

`endprotect
endmodule
