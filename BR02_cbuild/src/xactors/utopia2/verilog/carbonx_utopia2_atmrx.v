//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**********************************************************
 ** Title      : Utopia 2 ARM RX Transactor              **
 ** Created    : October 4, 2005        	         **
 ** Author     : 
 ** References : The ATM Forum Utopia 2, Version 1.0     **
 **                  af-phy-0039.000			 **
 **              The ATM Forum Utopia Specification      **
 **                Level 1, Version 2.01                 **
 **                  af-phy-0017.000			 **
 **                                                      **
 **********************************************************/

/*********************************************************************
 ** Utopia 2 ATM Side RX Transactor
 ** This transactor receives ATM cells.  Flow control is done with
 ** rxenb and rxemptyn/rxclav.  It may be instantiated and connected
 ** to single or multiple PHY devices.  Data bus width may be 8 or
 ** 16 bits.  
 **********************************************************************/ 

`timescale 1 ns / 1 ps

module carbonx_utopia2_atmrx (
			 iResetn,
			 oRxClk,
			 iRxData,
			 iRxPrty,
			 iRxSOC,
			 oRxEnbn,
			 oRxAddr,
			 iRxEmptyn_RxClav0,
			 iRxClav3_1
			 );
   
   parameter          DATAW = 16;

   input              iResetn;
   output             oRxClk;
   input [DATAW-1:0]  iRxData;
   input              iRxPrty;
   input              iRxSOC;
   output             oRxEnbn;
   output [4:0]       oRxAddr;
   input              iRxEmptyn_RxClav0;
   input [3:1] 	      iRxClav3_1;


`protect
  // carbon license crbn_vsp_exec_xtor_utopia2
  
   /******************************************************
    ** Clock Period Parameter for non-Carbon Simulation **
    ******************************************************/

`ifdef CARBONX_NOT_RUNNING_CARBON
   parameter 	      RXCLK_PERIOD     = 20;
`endif

   /*****************************************
    ** XIF Core Parameters                 **
    *****************************************/
   parameter 	      BFMid     = 1;
   parameter 	      ClockId   = 1;
   parameter 	      DataWidth = 32;
   parameter 	      DataAdWid = 11;	   // Max is 15
   parameter 	      GCSwid    = 32*4;  // Max is 256 bytes
   parameter 	      PCSwid    = 32*1;  // Max is 256 bytes
   
   /*****************************************
    * State Parameters
    *****************************************/
   parameter 	      IDLE            = 0;
   parameter 	      PORT_SELECT     = 1;
   parameter 	      PORT_SELECT2    = 2;
   parameter 	      WAIT_SOC        = 3;
   parameter 	      CHECK_SOC1      = 4;
   parameter 	      CHECK_SOC2      = 5;
   parameter 	      RECEIVE_WINDOW  = 6;
   parameter 	      OCTET_FLOW_STOP = 7;
   
   /*****************************************
    * Config Register Parameters
    *****************************************/
   parameter 	      
     OCTET_FLOW_CTRL = 0,
       CELL_FLOW_CTRL  = 1,
       ONE_CLAV        = 0,
       FOUR_CLAV       = 1;
   
   /*****************************************
    * Other Parameters
    *****************************************/
   parameter 	      SIZE_OF_CELL = (DATAW == 16) ? 54 : 53;
   
`include "xif_core.vh"   // Use the transactor template
   
   /*************************************
    ** Variables needed for transactor **
    ************************************/
   reg 		      data_width;
   reg 		      xrun;
   reg [7:0] 	      state;
   reg [30:0] 	      clav_status_reg;
   reg [7:0] 	      octet_count;
   reg [7:0] 	      octet_flow_ctrl_cnt;
   reg [4:0] 	      poll_addr;
   reg [4:0] 	      poll_rxaddr;
   reg [4:0] 	      poll_rxaddr_r1;
   reg 		      poll_cycle;
   reg [4:0] 	      curr_port;
   reg 		      disable_polling;
   reg 		      receiving_cell;
   reg [31:0] 	      put_data;
   reg 		      put_dwe;
   wire 	      flow_ctrl_mode;
   wire 	      clav_mode;
   wire [4:0] 	      polling_start;
   wire [4:0] 	      polling_end;
   reg 		      RxEnbn_dly;
   
   /*************************************
    ** Initialize the outputs          **
    ************************************/
   wire [4:0] 	      oRxAddr;
   reg 		      oRxEnbn;
   initial begin
      oRxEnbn <= 1'b1;
      xrun <= 0;
      state <= IDLE;
      clav_status_reg <= 30'b0;
      octet_count <= 0;
      octet_flow_ctrl_cnt <= 0;
      poll_addr <= 0;
      curr_port <= 5'h1F;
      disable_polling <= 0;
      poll_cycle <= 0;
      poll_rxaddr <= 5'h0;
      poll_rxaddr_r1 <= 5'h0;
      receiving_cell <= 0;
      put_data <= 32'h0;
      put_dwe <= 0;
   end

`ifdef CARBONX_NOT_RUNNING_CARBON
   reg 	oRxClk;
   initial oRxClk = 0;
   always #(RXCLK_PERIOD/2) oRxClk = !oRxClk;
`else
`endprotect
   wire oRxClk; // carbon depositSignal
`protect
`endif	 

   /*************************************
    ** XIF_core interface
    ************************************/
   assign BFM_xrun           = xrun; 
   assign BFM_clock          = oRxClk; 
   assign BFM_put_operation  = BFM_NXTREQ;
   assign BFM_interrupt      = 0;
   assign BFM_put_size       = octet_count;
   assign BFM_put_address    = curr_port;
   assign BFM_put_data       = put_data;
   assign BFM_put_cphwtosw   = 0; 
   assign BFM_put_cpswtohw   = 0; 
   assign BFM_put_dwe        = put_dwe;
   assign BFM_reset          = !iResetn;
   wire   TBP_reset;
   assign TBP_reset = !iResetn;
   
   /*********************************
    ** Continuous assignments      **
    *********************************/ 
   // Address the XIF core by incrementing every 4 octets
   assign BFM_gp_daddr = (octet_count == 0) ? 0 :
                         (octet_count - 1 - data_width) >> 2;
   assign oRxAddr = (disable_polling) ? XIF_get_address[4:0] :
                    poll_rxaddr;

   // Receiving CLAV status
   //   When there is a single phy, clav_mode is set to ONE_CLAV and both
   //   polling_start and polling_end should be set to zero.
   //   Then bit zero of the status word has the clav info for the phy.
   //
   //   When there are multi-phys with one clav coming to the ATM side, 
   //   clav_mode is set to ONE_CLAV and polling_start/polling_end are set
   //   as needed by the number of phy devices/ports connected to the one 
   //   clav signal.  All the status bits between polling_start and polling_end
   //   contain valid clav status for the ports.
   //
   //   When there are multi-phys with four clavs coming to the ATM side,
   //   clav_mode is set to FOUR_CLAV and polling_start/polling_end must
   //   be set to a group address (0, 4, 8, ...).  

   always @(posedge oRxClk or negedge iResetn) begin
      if(iResetn == 0) begin
	 clav_status_reg <= 30'b0;
	 poll_addr <= 5'h0;
	 poll_rxaddr <= 5'h0;
	 poll_rxaddr_r1 <= 5'h0;
	 poll_cycle <= 0;
      end
      else begin
	 
	 // Pipeline poll_rxaddr to use
	 poll_rxaddr_r1 <= oRxAddr;

	 if (disable_polling == 0) begin

	    poll_cycle <= !poll_cycle;
	    
            if (clav_mode == ONE_CLAV) begin
               clav_status_reg <= (clav_status_reg & ~(30'h1 << poll_rxaddr_r1)) | 
				  ({29'h0, iRxEmptyn_RxClav0} << poll_rxaddr_r1);
               // Only increment every other cycle and also only drive the
               // output every other cycle
               if (poll_addr == polling_end) 
		 poll_addr <= poll_cycle ? poll_addr : polling_start;
               else
		 poll_addr <= poll_cycle ? poll_addr : poll_addr + 1;
            end 
            else begin // clav mode is FOUR_CLAV
               clav_status_reg <= (clav_status_reg & ~(30'hF << poll_rxaddr_r1)) | 
				  ({26'h0, 
				    iRxClav3_1[3:1],
				    iRxEmptyn_RxClav0} << poll_rxaddr_r1);
               // When in four clav mode, always poll by groups
               // Only increment every other cycle and also only drive the
               // output every other cycle
               if (poll_addr == polling_end) 
		 poll_addr <= poll_cycle ? poll_addr : polling_start;
               else
		 poll_addr <= poll_cycle ? poll_addr : poll_addr + 4;
            end
            // When only one address or group is being polled don't put
            //   the NULL cycle inbetween address changes.
            if (polling_start != polling_end)
              poll_rxaddr <= poll_cycle ? poll_addr : 5'h1F;
            else
              poll_rxaddr <= polling_start; 
	 end // if (disable_polling == 0)

	 // Keep the current port status to no available data
	 //if(state == RECEIVE_WINDOW && octet_count < 49)
	   //clav_status_reg <= (clav_status_reg & ~(30'h1 << curr_port));
      end
   end

   always @(posedge oRxClk or negedge iResetn) begin
      if(iResetn == 0) begin
	 data_width      <= (DATAW == 16) ? 1 : 0;
	 xrun            <= 0;
	 state           <= IDLE;
	 octet_count     <= 0;
	 put_data        <= 0;
	 put_dwe         <= 0;
	 curr_port       <= 5'h1F;
	 disable_polling <= 0;
	 receiving_cell  <= 0;
	 RxEnbn_dly      <= 0;
      end
      else begin

	 // Pipeline RxEnbn
	 RxEnbn_dly      <= oRxEnbn;
	 
	 case (state)
           IDLE : begin
              if(((XIF_xav == 1'b1) && 
		  (XIF_get_operation == BFM_READ)) || 
		 xrun) begin
		 if (XIF_get_size != SIZE_OF_CELL) begin
		    $display("ERROR -- Read operation size = %d is not %d bytes\n",
                             XIF_get_size, SIZE_OF_CELL);
		 end
		 xrun <= 1'b1;
		 octet_count <= 0;
		 oRxEnbn <= 1'b1;
		 disable_polling <= 1;
		 state <= PORT_SELECT;
              end
              else begin
		 put_dwe  <= 0;
		 state <= IDLE;
              end
           end
           PORT_SELECT : begin
              // poll_cycle == 1 means that we are in the non-NULL address output
              //   cycle and can continue with the cell receive on the next cycle.
              //   If we are in the NULL address output cycle then we must continue
              //   in port select for one more cycle.
              if (poll_cycle == 1) begin
		 oRxEnbn <= 1'b0;
		 curr_port <= XIF_get_address[4:0];
		 state <= WAIT_SOC;
              end
              else begin
		 oRxEnbn <= 1'b1;
		 disable_polling <= 1;
		 state <= PORT_SELECT2;
              end
           end // case: PORT_SELECT

           PORT_SELECT2 : begin
	      oRxEnbn <= 1'b0;
	      curr_port <= XIF_get_address[4:0];
	      state <= WAIT_SOC;
           end
	   
	   // It takes a cycle before SOC can come after RxEnbn is assered
	   WAIT_SOC : begin
	      state <= CHECK_SOC1;
	   end

	   // Now Check to see if SOC comes active
	   CHECK_SOC1 : begin
	      if(iRxSOC == 1) begin
		 state           <= RECEIVE_WINDOW;
		 disable_polling <= 0;
		 octet_count     <= data_width ? octet_count+2 : octet_count+1;
		 put_dwe         <= 1;
		 
		 // Put data into the XIF Core
		 if (data_width == 1) begin // 16-bit mode
		    put_data <= (BFM_put_data & ~(16'hFFFF << (octet_count[1] * 16))) |
				({iRxData[(DATAW/2-1):0], iRxData[(DATAW-1):(DATAW/2)]} << (octet_count[1] * 16));
		 end
		 else begin // 8-bit mode
		    put_data <= (BFM_put_data & ~(8'hFF << (octet_count[1:0] * 8))) |
				(iRxData << (octet_count[1:0] * 8));
		 end
	      end // if (iRxSOC == 1)

	      // If we didn't get a SOC, deassert RxEnbn and check again for an SOC
	      else begin
		 oRxEnbn <= 1'b1;
		 state   <= CHECK_SOC2;
	      end // else: !if(iRxSOC == 1)
	      
	   end // case: CHECK_SOC1
	   
	   CHECK_SOC2 : begin

	      // If we again didn't get a SOC, the port has no cells available
	      // return to the c-side to choose another port
	      if(iRxSOC != 1) begin
		 state           <= IDLE;
		 disable_polling <= 0;
		 xrun            <= 0;
		 oRxEnbn         <= 1;
	      end

	      // We did get a SOC, assert RxEnbn again and continue to receive the cell  
	      else begin
		 state           <= RECEIVE_WINDOW;
		 disable_polling <= 0;
		 oRxEnbn         <= 0;
		 octet_count     <= data_width ? octet_count+2 : octet_count+1;
		 put_dwe         <= 1;
		 
		 // Put data into the XIF Core
		 if (data_width == 1) begin // 16-bit mode
		    put_data <= (BFM_put_data & ~(16'hFFFF << (octet_count[1] * 16))) |
				({iRxData[(DATAW/2-1):0], iRxData[(DATAW-1):(DATAW/2)]} << (octet_count[1] * 16));
		 end
		 else begin // 8-bit mode
		    put_data <= (BFM_put_data & ~(8'hFF << (octet_count[1:0] * 8))) |
				(iRxData << (octet_count[1:0] * 8));
		 end
	      end // else: !if(iRxSOC != 1)
	   end // case: CHECK_SOC2
	   
           RECEIVE_WINDOW : begin

	      if(RxEnbn_dly == 0) begin
		 octet_count <= data_width ? octet_count+2 : octet_count+1;
		 put_dwe     <= 1;
		 
		 // Put data into the XIF Core
		 if (data_width == 1) begin // 16-bit mode
		    put_data <= (BFM_put_data & ~(16'hFFFF << (octet_count[1] * 16))) |
				({iRxData[(DATAW/2-1):0], iRxData[(DATAW-1):(DATAW/2)]} << (octet_count[1] * 16));
		 end
		 else begin // 8-bit mode
		    put_data <= (BFM_put_data & ~(8'hFF << (octet_count[1:0] * 8))) |
				(iRxData << (octet_count[1:0] * 8));
		 end
		 
		 // On the second to last cycle deassert RxEnbn so the PHY does
		 //   not start sending another cell from the current port.
		 if((data_width == 1) && (octet_count == (SIZE_OF_CELL - 4)) ||
		    (data_width == 0) && (octet_count == (SIZE_OF_CELL - 2))) begin
		    oRxEnbn <= 1;
		 end
		 // Look for the last cycle.  The last cycle depends on whether
		 // we are in 8 or 16 bit data bus mode.  
		 if((data_width == 1) && (octet_count == (SIZE_OF_CELL - 2)) ||
		    (data_width == 0) && (octet_count == (SIZE_OF_CELL - 1))) begin
		    xrun        <= 1'b0;
		    oRxEnbn     <= 1;
		    state       <= IDLE;
		 end
		 else begin
		    state <= RECEIVE_WINDOW;
		 end
	      end // if (RxEnbn_dly == 0)
	      
           end // case: RECEIVE_WINDOW
	   
	 endcase
      end
   end

   /********************************************
    **  ControlStatus Write Task              **
    **  Update Programmable Register Values   ** 
    *******************************************/
   //  flow_ctrl_mode can be OCTET (0) or CELL (1) 
   assign flow_ctrl_mode    = XIF_get_csdata[ 0 ];       // Addr  0
   //  clav_mode can be ONE_CLAV (0) or FOUR_CLAV (1)
   assign clav_mode         = XIF_get_csdata[ 32 ];      // Addr  1
   assign polling_start     = XIF_get_csdata[  68:64 ];  // Addr  2
   assign polling_end       = XIF_get_csdata[ 100:96 ];  // Addr  3

   /*************************************************
    ** CS_Read Task, reading current value of the  **
    ** programmable registers                      **
    *************************************************/
   // send the polled clav status back on the status wires
   assign BFM_put_status = {1'b0, clav_status_reg};

   assign BFM_put_csdata[  31:  0] = DATAW;       // Addr 0

`endprotect
endmodule
