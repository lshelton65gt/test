//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONXSPI3LNKRX_H
#define CARBONXSPI3LNKRX_H

#ifdef __cplusplus
extern "C" {
#endif

s_int32 carbonXSpi3LnkRxPkt(u_int8 * pkt, 
			 s_int32 * size, 
			 s_int32 * sopt,
			 s_int32 * eop, 
			 s_int32 * port);

void    carbonXSpi3LnkRxWrConfig ( u_int32 writeData );

// Config Space Register Address Map
#define CARBONXSPI3LNKRX_CONFIG_REG    (0x0<<2)

// Bit position definitions for LnkTxConfig register
//   Must be corrsonding definitions in Verilog
#define CARBONXSPI3LNKRX_BUSWIDTH_32    (0<<0)
#define CARBONXSPI3LNKRX_BUSWIDTH_8     (1<<0)
#define CARBONXSPI3LNKRX_INBANDADDR     (0<<1)
#define CARBONXSPI3LNKRX_OUTBANDADDR    (1<<1)
#define CARBONXSPI3LNKRX_RENBENABLE     (0<<2)
#define CARBONXSPI3LNKRX_RENBDISABLE    (1<<2)
#define CARBONXSPI3LNKRX_PRTYCHKENABLE  (0<<3)
#define CARBONXSPI3LNKRX_PRTYCHKDISABLE (1<<3)

#ifdef __cplusplus
}
#endif

#endif
