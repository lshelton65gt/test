//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module carbonx_spi3_lnkrx (
                       reset_l,
                       rdat,
                       rprty,
                       rsop,
                       renbn,
                       rval,
                       rfclk,
                       rmod,
                       reop,
                       rerr,
                       rsx
);


input          reset_l, 
               rprty,
               rsop, 
               rval, 
               reop, 
               rerr, 
               rfclk, 
               rsx;
input [1:0]    rmod;
input [31:0]   rdat;

output         renbn;

`protect
// carbon license crbn_vsp_exec_xtor_spi3

//**************************************************************************
//                      Parameter Defines
//**************************************************************************
parameter      BFMid     = 1;
parameter      ClockId   = 1;
parameter      DataWidth = 32;
parameter      DataAdWid = 11;
parameter      GCSwid    = 32*8;
parameter      PCSwid    = 32*19;


//**************************************************************************
// Include XIF core
//**************************************************************************
`include "xif_core.vh"


reg            	renbn;
reg            	renbn_r1;
reg            	rval_r1;
`endprotect   
wire    	end_of_trans; // carbon observeSignal
`protect   
reg     	end_of_trans_r;
reg            	sop_found;
reg            	renb_stop;
reg   	[1:0]	rmod_r1;
reg   	[7:0]   rx_port_id;
reg	[7:0]   renbn_cnt1; 
reg   	[7:0]   renbn_cnt2;
reg   	[31:0]  rbyte_count;
reg   	[31:0]  rdat_r1;
reg   	[31:0]  parity_error_count;

wire            bus_width;
wire            addr_mode;
wire            renb_dis;
wire            rx_par_err;
wire            rx_par_check;
wire           	reop_valid;
wire            rval_valid;
wire           	stop_cnt1;
wire		stop_cnt2;
wire	[7:0]	renbn_cycles_cnt1; 
wire	[7:0]   renbn_cycles_cnt2; 

reg            trans_in_progress;
reg [23:0]     put_size;
reg [31:0]     put_status;
reg [63:0]     put_address;


/*************************************
 ** XIF_core interface
 ************************************/
assign reop_valid         = reop & !renbn_r1 & rval;
assign rval_valid         = rval | renbn_r1;

assign BFM_xrun           = !end_of_trans;
assign BFM_clock          = rfclk; 
assign BFM_put_operation  = BFM_NXTREQ;
assign BFM_interrupt      = 0;
assign BFM_put_status     = {22'h0, rx_port_id, reop, (sop_found || (!BFM_xrun && rsop))};
assign BFM_put_size       = reop_valid ? !bus_width ? ((rbyte_count + 4) - rmod) :
                                                       (rbyte_count + 1) :
                                         rbyte_count;
assign BFM_put_address    = put_address;
assign BFM_put_data       = (!bus_width) ? 
       {rdat[7:0], rdat[15:8], rdat[23:16], rdat[31:24]} :
       (rbyte_count[1:0] == 2'b00) ? {rdat_r1[31:8],  rdat[7:0]} : 
       (rbyte_count[1:0] == 2'b01) ? {rdat_r1[31:16], rdat[7:0], rdat_r1[7:0]} : 
       (rbyte_count[1:0] == 2'b10) ? {rdat_r1[31:23], rdat[7:0], rdat_r1[15:0]} : 
       (rbyte_count[1:0] == 2'b11) ? {                rdat[7:0], rdat_r1[23:0]} : 32'h0;
assign BFM_put_csdata     = 0;
assign BFM_put_cphwtosw   = 0; 
assign BFM_put_cpswtohw   = 0; 
assign BFM_put_dwe        = rval & !renbn_r1;
assign BFM_reset          = !reset_l;
assign BFM_gp_daddr       = rbyte_count >> 2;


//**************************************************************************
// Config Reg Definitions and Reading/Writing
//**************************************************************************

parameter // Bits in config reg

BUS_WIDTH          = 0,  // 0 - 32 bits, 1 - 8 bits 
IN_OUT_ADDR        = 1,  // 0 - In band, 1 - Out of band
RENB_DISABLE       = 2,  // 0 - Enabled, 1 - Disabled
RX_PAR_CHECK       = 3;  // 0 - Parity check enabled, 1 - Parity check disabled

assign bus_width           = XIF_get_csdata[BUS_WIDTH];
assign addr_mode           = XIF_get_csdata[IN_OUT_ADDR];
assign renb_dis            = XIF_get_csdata[RENB_DISABLE];
assign rx_par_check  	   = XIF_get_csdata[RX_PAR_CHECK];

assign BFM_put_csdata[31:0] = parity_error_count[31:0];

assign rx_par_err          = (~^rdat) ^ rprty;
assign renbn_cycles_cnt1  = 8'h8;
assign renbn_cycles_cnt2  = 8'h6;

initial begin
    trans_in_progress <= 0;
    put_address       <= 0;
    put_size          <= 0;
    rval_r1           <= 0;
    rmod_r1           <= 0;
    rx_port_id        <= 0;
    sop_found         <= 0;
    rbyte_count       <= 0;
    rdat_r1           <= 0;
    parity_error_count<= 0;
end

//**************************************************************************
//  Main XIF Core Interface Control
//**************************************************************************
assign end_of_trans = (reop_valid || (!rval_valid && rval_r1 && !end_of_trans_r)) || !trans_in_progress;  
always @(posedge rfclk or negedge reset_l)
begin
  if(reset_l == 1'b0) begin
    trans_in_progress <= 0;
    put_address       <= 0;
    put_size          <= 0;
    rval_r1           <= 0;
    rmod_r1           <= 0;
    rx_port_id        <= 0;
    sop_found         <= 0;
    rbyte_count       <= 0;
    rdat_r1           <= 0;
    end_of_trans_r    <= 0;
  end
  else begin
    // Start the read transaction
    if((XIF_xav == 1'b1) && (XIF_get_operation == BFM_READ)) begin
      trans_in_progress <= 1'b1;
      rbyte_count <= 0;
    end
    end_of_trans_r    <= end_of_trans;

    if (renbn_r1 == 1'b0) begin  // Signals from PHY are valid
                                 //   PHY responds to renbn one cycle after
                                 //   it is deasserted.

      // Pipeline the required signals
      rval_r1 <= rval;
      rmod_r1 <= rval;

      if ((rsx == 1'b1) && (rval == 1'b0)) begin  // PHY is choosing a port
        rx_port_id <= rdat[7:0];
      end                                         // PHY is choosing a port

      if (rval == 1'b1) begin // Data is valid from PHY
        if (bus_width == 1'b1) begin // 8-bit
          case (rbyte_count[1:0])
            0: rdat_r1[7:0]    <= rdat[7:0];
            1: rdat_r1[15:8]   <= rdat[7:0];
            2: rdat_r1[23:16]  <= rdat[7:0];
            3: rdat_r1[31:24]  <= rdat[7:0];
          endcase
          rbyte_count <= (BFM_xrun == 1'b1) ? rbyte_count + 1 : 0;
        end        // 8-bit
        else begin // 32-bit
                   //    All the data is written to the XIF_core even if 
                   //    it is end of packet with rmod indicating less than
                   //    4-bytes valid.  rmod is taken into account by
                   //    setting put_size properly.
          rbyte_count <= (BFM_xrun == 1'b1) ? rbyte_count + 4 : 0;
        end        // 32-bit
      end                    // Data is valid from PHY

      if (((rval == 1'b0) && (rval_r1 == 1'b1)) ||
           (reop == 1'b1)) begin // end of transaction cases
        sop_found       <= 0;
      end // Transaction end

      if (rsop == 1'b1) begin  // start of packet is signaled
        sop_found   <= 1'b1; // rsop
      end

    end                       // Signals from PHY are valid
    else begin                // renbn == 1 -- signals from PHY are not valid
    end                       // renbn == 1 -- signals from PHY are not valid
  end // non-reset 
end   // always


//**************************************************************************
// Renbn Signal Control
//**************************************************************************

assign stop_cnt1 = renbn_cnt1==renbn_cycles_cnt1;
assign stop_cnt2 = renbn_cnt2==renbn_cycles_cnt2;

always @(posedge rfclk or negedge reset_l) begin
  if(reset_l == 1'b0) begin
    renbn_cnt1 <= 8'h0;
    renbn_cnt2 <= 8'h0;
    renb_stop  <= 1'b0;
    renbn      <= 1'b0;
    renbn_r1   <= 1'b0;
  end
  else begin
    renbn_r1   <= renbn;
//    if (renb_dis == 1'b0) begin
//      if (renbn_cnt1 == renbn_cycles_cnt1) begin
//        renbn <= !renbn;
//        renbn_cnt1 <= 0;
//      end
//      else begin
//        renbn_cnt1 <= renbn_cnt1 + 1;
//      end
//    end
  end
end  

//**************************************************************************
// Parity Error Indication
//**************************************************************************
always @(posedge rfclk) begin
  begin
    // Send display to log on parity error
    if (rx_par_check && rx_par_err)
      $display("ERROR: Parity incorrect at time %t", $time);
    if (rx_par_err)
      parity_error_count <= parity_error_count + 1;
  end
end

`endprotect
endmodule
