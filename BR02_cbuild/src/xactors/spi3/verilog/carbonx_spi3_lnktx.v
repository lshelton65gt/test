//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module carbonx_spi3_lnktx (
			   reset_l,
			   tadr,
			   tdat,
			   tsop, 
			   tenbn,
			   tfclk, 
			   dtpa,
			   stpa,
			   ptpa,
			   tprty, 
			   tmod, 
			   teop, 
			   terr,
			   tsx
			   );

   parameter DTPA_WIDTH = 4;
   parameter TADR_WIDTH = 8;
   
   input     reset_l,
	     stpa,
	     ptpa, 
	     tfclk;
   input [DTPA_WIDTH-1:0] dtpa;
   
   output 		  tsop, 
			  tenbn,
			  tprty, 
			  teop, 
			  terr,
			  tsx;
   output [1:0] 	  tmod;
   output [TADR_WIDTH-1:0] tadr;
   output [31:0] 	   tdat;
   
`protect
   // carbon license crbn_vsp_exec_xtor_spi3

   //**************************************************************************
   //                      Parameter Defines
   //************************************************************************** 
   parameter 		   BFMid     = 1;
   parameter 		   ClockId   = 1;       		// Which cclock I am connected to	     
   parameter 		   DataWidth = 32;
   parameter 		   DataAdWid = 11;	     		// Max is 15
   parameter 		   GCSwid    = 32*4;   		// Get CS Data width Max is 512 bytes
   parameter 		   PCSwid    = 32*8;   		// Put CS Data width Max is 512 bytes

   // define TX_CONFIG_REG 0
   parameter 		   TX_CONFIG_REG = 0;

   // States
   parameter               IDLE      = 0;
   parameter               TSX       = 1;
   parameter               DATA      = 2;
   
   //**************************************************************************
   // Include XIF core
   //**************************************************************************
`include "xif_core.vh"


   reg 			   xrun;
   reg 			   stop;
   reg 			   tsop;
   reg 			   teop;
   reg 			   tsx;
   reg 			   tenbn;
   wire [1:0] 		   tmod;
   wire [7:0] 		   tsx_addr;
   reg [TADR_WIDTH-1:0]    tadr;
   reg [7:0] 		   dtpa_byte;
   reg [7:0] 		   dtpa_byte_cnt;
   reg [7:0] 		   pa;
   wire [31:0] 		   tdat_out;
   reg [255:0] 		   port_avail_reg;

   wire [31:0] 		   data_word;
   reg [7:0] 		   cycle_cnt;
   reg [3:0] 		   state;
   reg [31:0] 		   pkt_size;
   reg 			   ins_par_err;
   reg 			   ins_data_err;
   reg [7:0] 		   error_count;

   wire 		   tprty;
   wire 		   terr;
   wire 		   tpa;
   wire 		   dtpa_inv;
   wire [31:0] 		   segment_words;

   // Config reg flags
   wire 		   bus_width;
   wire 		   packet_mode;
`endprotect
   wire 		   addr_mode;
`protect
   wire 		   tpa_control;
   wire 		   tenb_dis;
   wire 		   tsop_mask;
   wire 		   teop_mask;
   wire 		   par_data_err;
   wire 		   packet_pause;
   wire 		   set_transmit_error;
`endprotect   
   wire 		   one_port;
`protect   
   wire 		   small_fifo;
   wire [7:0] 		   err_cyc_cnt;

   wire [7:0] 		   poll_start_addr; 
   wire [7:0] 		   poll_end_addr; 
   wire [7:0] 		   poll_bit_reg;
   wire [7:0] 		   port_status_addr;
   wire [31:0] 		   tx_config;
   wire [31:0] 		   tdat;


   /*************************************
    ** TX Output Signals
    ************************************/
   assign 		   tmod       = bus_width ? 0 : teop ? (4-(pkt_size % 4)) : 0; 
   // Invert bit 0 to create opposite parity
   assign 		   tdat[0]    = (par_data_err && ins_data_err && !tsx) ? !tdat_out[0] : tdat_out[0];
   assign 		   tdat[31:1] = tdat_out[31:1];
   assign 		   tprty      = (!par_data_err && ins_par_err) ? ^tdat_out : ~^tdat_out;
   assign 		   terr       = teop && set_transmit_error;

   /*************************************
    ** XIF_core interface
    ************************************/
   assign 		   BFM_xrun              = xrun; 
   assign 		   BFM_clock             = tfclk; 
   assign 		   BFM_put_operation     = BFM_NXTREQ;
   assign 		   BFM_interrupt         = 0;
   assign 		   BFM_put_status        = bus_width ? cycle_cnt : (cycle_cnt*4 - tmod);
   assign 		   BFM_put_size          = 0;
   assign 		   BFM_put_address       = 0;
   assign 		   BFM_put_data          = 0;
   assign 		   BFM_put_cphwtosw      = 0; 
   assign 		   BFM_put_cpswtohw      = 0; 
   assign 		   BFM_put_dwe           = 0;
   assign 		   BFM_reset             = !reset_l;
   assign 		   BFM_put_csdata[255:0] = packet_mode ? port_avail_reg[255:0] : dtpa;
   assign 		   BFM_gp_daddr          = !bus_width ? cycle_cnt : cycle_cnt >> 2; 


   //**************************************************************************
   // Config register definitions and reading/writing
   //**************************************************************************

   // Bits in config reg
   parameter 		   BUS_WIDTH          = 0; 	// 0 - 32 bits,		1 - 8 bits 
   parameter 		   BYTE_PACKET        = 1; 	// 0 - Byte mode,	1 - Packet mode
   parameter 		   IN_OUT             = 2; 	// 0 - In Band,   	1 - Out of band
   parameter 		   DTPA_STPA          = 3; 	// 0 - DTPA,     	1 - STPA
   parameter 		   TENB_DISABLE       = 4; 	// 0 - Enable,   	1 - Disable
   parameter 		   TSOP_MASK          = 5; 	// 0 - No Mask,  	1 - Mask
   parameter 		   TEOP_MASK          = 6; 	// 0 - No Mask,  	1 - Mask
   parameter 		   PAR_DATA_ERR       = 7; 	// 0 - Par error, 	1 - Data error
   parameter 		   RENB_DISABLE       = 8; 	// 0 - No pause, 	1 - Insert Pause
   parameter 		   SET_TERR           = 9;   	// 0 - No error, 	1 - Insert Error 
   parameter 		   ONE_PORT_OPER      = 10;  	// 0 - 4 ports,   	1 - 1 port
   parameter 		   SMALL_FIFO         = 11;  	// 0 - 256 words, 	1 - 8 words

   assign 		   bus_width           = XIF_get_csdata[BUS_WIDTH];
   assign 		   packet_mode         = XIF_get_csdata[BYTE_PACKET];
   assign 		   addr_mode           = XIF_get_csdata[IN_OUT];
   assign 		   tpa_control         = XIF_get_csdata[DTPA_STPA];
   assign 		   tenb_dis            = XIF_get_csdata[TENB_DISABLE];
   assign 		   tsop_mask           = XIF_get_csdata[TSOP_MASK];
   assign 		   teop_mask           = XIF_get_csdata[TEOP_MASK];
   assign 		   par_data_err        = XIF_get_csdata[PAR_DATA_ERR];
   assign 		   set_transmit_error  = XIF_get_csdata[SET_TERR];
   assign 		   one_port            = XIF_get_csdata[ONE_PORT_OPER];
   assign 		   small_fifo          = XIF_get_csdata[SMALL_FIFO];
   assign 		   err_cyc_cnt	       = XIF_get_csdata[19:12];

   assign 		   tx_config           = XIF_get_csdata[31:0];
   assign 		   poll_start_addr     = XIF_get_csdata[39:32];
   assign 		   poll_end_addr       = XIF_get_csdata[71:64];
   assign 		   port_status_addr    = XIF_get_csdata[111:104];

   assign 		   segment_words = bus_width ? XIF_get_size : ((XIF_get_size+3)/4);

   //**************************************************************************
   // Parity and data error insertion
   //**************************************************************************
   always @(posedge tfclk)
     begin
	if (!reset_l)
	  begin
	     ins_par_err <= 0;
	     ins_data_err <= 0;
	     error_count <= 0;
	  end
	else 
	  begin 
	     if ((err_cyc_cnt > 0)&&(err_cyc_cnt == error_count))
	       begin
		  if (!par_data_err)
		    ins_par_err <= 1;
		  else
		    ins_data_err <= 1;

		  error_count <= 0;
	       end
	     else
	       begin
		  ins_par_err <= 0;
		  ins_data_err <= 0;

		  if (!tenbn)
		    error_count <= error_count + 1;
	       end
	  end
     end 


   //**************************************************************************
   // Extract in-band address
   //**************************************************************************
   assign tsx_addr = !addr_mode ? XIF_get_address[8:1] : 0;
   
   //**************************************************************************
   // Polling PTPA  
   //**************************************************************************
   always @(posedge tfclk) 
     begin      
	if(!reset_l) begin
	   port_avail_reg <= 256'h0;
	   pa             <= 0;
	   tadr           <= 0;
	end else if (packet_mode) begin 
	   // In packet mode and the polling end address has been set.
	   //   This means enable polling mode.
	   // Update the tadr output
	   tadr <= pa;
	   // pa is the counts through the polling addresses in a round
	   //   robin manner.  
	   if (pa == poll_end_addr) begin
	      pa <= poll_start_addr;
	   end else begin
	      pa <= pa + 1;
	   end
	   // The ptpa input has the status of the port on tadr one
	   //   cycle ago.
	   if (pa == poll_start_addr) begin
	      port_avail_reg[poll_end_addr] <= ptpa;
	   end else begin
	      port_avail_reg[tadr] <= ptpa;
	   end
	   // When tsx goes active we have begun a write to this port
	   //   Force this port's status to zero.
	   if (tsx) port_avail_reg[tdat_out[7:0]] <= 0;
	end // if (packet_mode && (poll_end_addr != 8'h0))
	
     end

   
   //**************************************************************************
   // Tx signal control
   //**************************************************************************
   // TBP data is little endian, tdat is big endian, so invert
   assign      data_word = !bus_width ? {XIF_get_data[7:0], XIF_get_data[15:8], XIF_get_data[23:16], XIF_get_data[31:24]} :
			   cycle_cnt[1:0] == 1 ? XIF_get_data[ 7: 0] : 
			   cycle_cnt[1:0] == 2 ? XIF_get_data[15: 8] :
			   cycle_cnt[1:0] == 3 ? XIF_get_data[23:16] :
			   cycle_cnt[1:0] == 0 ? XIF_get_data[31:24] : 0;
   assign      tdat_out = (state == TSX) ? tsx_addr : data_word; 
   
   always @(posedge tfclk) 
     begin      
	if(!reset_l) 
	  begin
	     state              <= IDLE;
	     xrun     		<= 0;
	     tsop     		<= 0;
	     teop     		<= 0;
	     tenbn  		<= 1;
	     tsx      		<= 0;
	     pkt_size           <= 0;
	  end    
	else 
	  begin

	     // Start Transaction
	     if(XIF_xav && XIF_get_operation == BFM_WRITE) 
	       begin
		  // Latch Size
		  pkt_size <= XIF_get_size;
		  
		  // If Inband Address goto address state
		  if(!addr_mode && tpa) begin
		     state     <= TSX;
		     cycle_cnt <= 0;
		     tenbn     <= 1'b1;
		     tsx       <= 1'b1;
		     tsop      <= 1'b0;
		     teop      <= 1'b0;
		     xrun      <= 1'b1;
		  end
		  
		  // Else if the PHY can receive Data on the current port
		  else if (tpa) begin
		     if(segment_words > 1) begin 
			state <= DATA;
			xrun  <= 1'b1;
		     end
		     else begin
			state <= IDLE;
			xrun  <= 1'b0;
		     end
		     
		     cycle_cnt <= 1;
		     tenbn     <= 1'b0;
		     tsx       <= 1'b0;
		     tsop      <= XIF_get_address[0];
		     teop      <= (segment_words == 1);
		  end
		  
		  // Else keep on Idling
		  else begin
		     state     <= IDLE;
		     cycle_cnt <= 0;
		     tenbn     <= 1'b1;
		     tsx       <= 1'b0;
		     tsop      <= 1'b0;
		     teop      <= 1'b0;
		     xrun      <= 1'b0;
		  end // else: !if(tpa)
	       end // if (XIF_xav && XIF_get_operation == BFM_WRITE)

	     else 
	       case(state)
		 IDLE : begin
		    state     <= IDLE;
		    cycle_cnt <= 0;
		    tenbn     <= 1'b1;
		    tsx       <= 1'b0;
		    tsop      <= 1'b0;
		    teop      <= 1'b0;
		 end // case: IDLE
		 
		 TSX : begin
		    state     <= DATA;
		    cycle_cnt <= 1;
		    tenbn     <= 1'b0;
		    tsx       <= 1'b0;
		    tsop      <= XIF_get_address[0];
		    teop      <= (segment_words == 1);

		    // End Of Packet Return To IDLE
		    if(cycle_cnt == segment_words-1) begin
		       state <= IDLE;
		       teop  <= 1'b1;
		       xrun  <= 1'b0;
		    end
		 end
		 
		 DATA : begin
		    tsop      <= 1'b0;
		    teop      <= 1'b0;
		    cycle_cnt <= cycle_cnt + 1;
		    tenbn     <= 1'b0;
		    
		    // End Of Packet Return To IDLE
		    if(cycle_cnt == segment_words-1) begin
		       state <= IDLE;
		       teop  <= 1'b1;
		       xrun  <= 1'b0;
		    end

		    // PHY Can't receive more data at this point, end transaction
		    else if(!stpa) begin
		       state     <= IDLE;
		       xrun      <= 1'b0;
		    end
		 end // case: DATA
	       endcase // case(state)
	  end // else: !if(!reset_l)
     end // always @ (posedge tfclk)

   assign tpa = !packet_mode ? dtpa[tsx_addr[1:0]] : port_avail_reg[tsx_addr[1:0]];
   
   //**************************************************************************
   // DTPA and STPA stop control
   //**************************************************************************
   reg 	  dtpa_latch;
   always @(posedge tfclk) begin

      // Latch DTPA or PTPA in the beginning of the transfer
      // Use STPA for Flow Control During the packet
      if( XIF_xav && XIF_get_operation == BFM_WRITE)
	dtpa_latch <= tpa;
   end
   
   assign dtpa_inv = (cycle_cnt < 3) ? !dtpa_latch : !stpa;
   
   always @(posedge tfclk)
     begin
	if (!reset_l)
	  begin
	     dtpa_byte <= 0;
	     dtpa_byte_cnt <= 0;
	     stop <= 0;
	  end
	else if (dtpa_inv)
	  begin
	     if (dtpa_byte == dtpa_byte_cnt)
	       begin
		  dtpa_byte <= 0;
		  stop <= 1;
	       end
	     else
	       begin
		  dtpa_byte <= dtpa_byte + 1;
		  stop <= stop;
	       end
	  end
	else
	  begin
	     dtpa_byte <= dtpa_byte;
	     stop <= 0;
	  end
     end

`endprotect
endmodule
