//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "tbp.h"
#include "carbonXSpi3LnkTx.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"

s_int32 carbonXSpi3LnkTxPkt(u_int8 *pkt, 
			    s_int32 size, 
			    s_int32 port, 
			    s_int32 sop) {
  
  carbonXWriteArray8((port<<1) | sop, pkt, size);
  return carbonXGetStatus();     
}

void    carbonXSpi3LnkTxWrConfig ( u_int32 writeData ) {
  carbonXCsWrite ( CARBONXSPI3LNKTX_CONFIG_REG, writeData );
}

// Function: carbonXSpi3LnkTxSetPollStartAddr
// Description: Sets the beginning port number for the round-robin
//              polling of port status using TADR[7:0] and PTPA.
//              Number must be 0 - 255.
void    carbonXSpi3LnkTxSetPollStartAddr ( s_int32 startPollPortNum ) {
  if ((startPollPortNum >= 0) && (startPollPortNum <= 255)) {
    carbonXCsWrite(CARBONXSPI3LNKTX_PORTSTATSTART, startPollPortNum);
  } else {
    MSG_Error("Start Port Number = %d out of range (0-255) in carbonXSpi3LnkTxSetPollStartAddr\n",
	      startPollPortNum);
  }
}
// Function: carbonXSpi3LnkTxSetPollEndAddr
// Description: Sets the ending port number for the round-robin
//              polling of port status using TADR[7:0] and PTPA.
//              Number must be 0 - 255 and greater than the starting
//              port number.
void    carbonXSpi3LnkTxSetPollEndAddr   ( s_int32 endPollPortNum ) {
  if ((endPollPortNum >= 1) && (endPollPortNum <= 255)) {
    carbonXCsWrite(CARBONXSPI3LNKTX_PORTSTATEND, endPollPortNum);
  } else {
    MSG_Error("End Port Number = %d out of range (1-255) in carbonXSpi3LnkTxSetPollEndAddr\n",
	      endPollPortNum);
  }
}

// Function: carbonXSpi3LnkTxGetPollStatus
// Description: Read the latest received port status for the requested 
//              port number.  Return of zero means portNum does not 
//              have room for a new packet.  Return of one means portNum
//              does have room for a new packet.  
// Return: 0 or 1 return values are the only valid values.  Return of -1
//         indicates an error.
u_int32 carbonXSpi3LnkTxGetPollStatus      ( s_int32 portNum ) {
  u_int32 readData;
  u_int32 ret;
  if ((portNum >= 0) && (portNum <= 255)) {
    // First find the CS register address that has the portNum.
    //   Ports start at PORTSTATBASE and each register is (#<<2) greater.
    //   (ie. BASE+0, BASE+4, BASE+8, etc.)
    readData = carbonXCsRead(CARBONXSPI3LNKTX_PORTSTATBASE + ((portNum / 32) << 2));
    ret = (readData >> (portNum % 32)) & 0x1;
  } else {
    MSG_Error("Port Number = %d out of range (0-255) in carbonXSpi3LnkTxGetPollStatus\n",
	      portNum);
    ret = -1;
  }
  return ret;
}

// Function: carbonXSpi3LnkTxGetPollStatusRange
// Description: Similar to GetPollStatus but returns status for 32 ports
//              at a time.  Each bit of the return value is the status
//              of one port.  The mapping of portRange to port numbers is:
//                      portRange           Port Numbers
//                          0                  31 :   0
//                          1                  63 :  32
//                          2                  95 :  64
//                          3                 127 :  96
//                          4                 159 : 128
//                          5                 191 : 160
//                          6                 223 : 192
//                          7                 255 : 224

u_int32 carbonXSpi3LnkTxGetPollStatusRange ( s_int32 portRange ) {
  u_int32 ret;
  if ((portRange >= 0) && (portRange <= 7)) {
    ret = carbonXCsRead(CARBONXSPI3LNKTX_PORTSTATBASE + (portRange << 2));
  } else {
    MSG_Error("Port Range = %d out of range (0-7) in carbonXSpi3LnkTxGetPollStatusRange\n",
	      portRange);
    ret = -1;
  }
  return ret;
}

