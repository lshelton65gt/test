//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONXSPI3LNKTX_H
#define CARBONXSPI3LNKTX_H

#ifdef __cplusplus
extern "C" {
#endif

s_int32 carbonXSpi3LnkTxPkt(u_int8 *pkt, 
			 s_int32 size, 
			 s_int32 port, 
			 s_int32 sop);

void    carbonXSpi3LnkTxWrConfig ( u_int32 writeData );

// Function: carbonXSpi3LnkTxSetPollStartAddr
// Description: Sets the beginning port number for the round-robin
//              polling of port status using TADR[7:0] and PTPA.
//              Number must be 0 - 255.
void    carbonXSpi3LnkTxSetPollStartAddr ( s_int32 startPollPortNum );
// Function: carbonXSpi3LnkTxSetPollEndAddr
// Description: Sets the ending port number for the round-robin
//              polling of port status using TADR[7:0] and PTPA.
//              Number must be 0 - 255 and greater than the starting
//              port number.
void    carbonXSpi3LnkTxSetPollEndAddr   ( s_int32 endPollPortNum );

// Function: carbonXSpi3LnkTxGetPollStatus
// Description: Read the latest received port status for the requested 
//              port number.  Return of zero means portNum does not 
//              have room for a new packet.  Return of one means portNum
//              does have room for a new packet.  
// Return: 0 or 1 return values are the only valid values.
u_int32 carbonXSpi3LnkTxGetPollStatus      ( s_int32 portNum );

// Function: carbonXSpi3LnkTxGetPollStatusRange
// Description: Similar to GetPollStatus but returns status for 32 ports
//              at a time.  Each bit of the return value is the status
//              of one port.  The mapping of portRange to port numbers is:
//                      portRange           Port Numbers
//                          0                  31 :   0
//                          1                  63 :  32
//                          2                  95 :  64
//                          3                 127 :  96
//                          4                 159 : 128
//                          5                 191 : 160
//                          6                 223 : 192
//                          7                 255 : 224
u_int32 carbonXSpi3LnkTxGetPollStatusRange ( s_int32 portRange );

// Config Space Register Address Map
#define CARBONXSPI3LNKTX_CONFIG_REG    (0x0<<2)
#define CARBONXSPI3LNKTX_PORTSTATSTART (0x1<<2)
#define CARBONXSPI3LNKTX_PORTSTATEND   (0x2<<2)
#define CARBONXSPI3LNKTX_PORTSTATBASE  ((0<<2)+256)

// Bit position definitions for LnkTxConfig register
//   Must be corrsonding definitions in Verilog
#define CARBONXSPI3LNKTX_BUSWIDTH_32    (0<<0)
#define CARBONXSPI3LNKTX_BUSWIDTH_8     (1<<0)
#define CARBONXSPI3LNKTX_BYTEMODE       (0<<1)
#define CARBONXSPI3LNKTX_PACKETMODE     (1<<1)
#define CARBONXSPI3LNKTX_INBANDADDR     (0<<2)
#define CARBONXSPI3LNKTX_OUTBANDADDR    (1<<2)
#define CARBONXSPI3LNKTX_DTPAMODE       (0<<3)
#define CARBONXSPI3LNKTX_STPAMODE       (1<<3)
#define CARBONXSPI3LNKTX_TENBENABLE     (0<<4)
#define CARBONXSPI3LNKTX_TENBDISABLE    (1<<4)
#define CARBONXSPI3LNKTX_TSOPNORMAL     (0<<5)
#define CARBONXSPI3LNKTX_TSOPMASKED     (1<<5)
#define CARBONXSPI3LNKTX_TEOPNORMAL     (0<<6)
#define CARBONXSPI3LNKTX_TEOPMASKED     (1<<6)
#define CARBONXSPI3LNKTX_FRCPRTYERR     (0<<7)
#define CARBONXSPI3LNKTX_FRCDATAERR     (1<<7)
#define CARBONXSPI3LNKTX_NOTERR         (0<<9)
#define CARBONXSPI3LNKTX_FRCTERR        (1<<9)
#define CARBONXSPI3LNKTX_MULTIPORT      (0<<10)
#define CARBONXSPI3LNKTX_ONEPORT        (1<<10)
#define CARBONXSPI3LNKTX_NORMALFIFO     (0<<11)
#define CARBONXSPI3LNKTX_SMALLFIFO      (1<<11)

#ifdef __cplusplus
}
#endif

#endif



