//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

enum Response {OKAY,ERROR,RETRY,SPLIT};

// Configuration Register structure
typedef struct {
  u_int32  rsvd          :2; // 31:30
  u_int32  en_usr_resume :1; // 29
  u_int32  split_count   :8; // 28:21
  u_int32  split_m_id    :5; // 20:16
  u_int32  slave_resp    :2; // 15:14  
  u_int32  resp_count    :8; // 13:6
  u_int32  wait_count    :6; // 5:0   
} AHB_SlaveCfg_T;

