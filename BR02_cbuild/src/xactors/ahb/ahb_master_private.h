//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define MASTER_CONFIG_REG 0
#define MASTER_AHB_OP 0x80000001

// TCMD
typedef struct {
  u_int32 rsvd1 :16; // 31:16: Reserved
  u_int32 rsvd2 :10; // 15:06: Reserved
  u_int32 size  :03; // 05:03: Transfer size 8-/16-/32-bit (upto 1024 allowed)
  u_int32 lock  :01; // 02   : 1=> locked transfer
  u_int32 incr  :01; // 01   : 1=> INCR burst, 0=> SINGLE or WRAP burst
  u_int32 write :01; // 00   : 1: Write, 0: Read
} AHB_MasterCtl_T;

