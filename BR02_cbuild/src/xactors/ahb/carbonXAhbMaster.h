/*
 *
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *****************************************************************************
 *
 * Module Name: master_support.h
 *
 * Module Description:
 *
 * Headers for AHB Master support routines.
 *
 * Author  : Kiran Maheriya
 *
 * Created : 02/23/2002
 *
 * $Id$
 *****************************************************************************
 */

#ifndef MASTER_SUPPORT_H
#define MASTER_SUPPORT_H

#ifdef __cplusplus
extern "C" {
#endif

// pack the control feilds into the command
#define carbonXAhbMasterCtlCmd(_size_,_lock_,_incr_,_write_) \
(((_size_ & 0x7) << 3) | ((_lock_ & 0x1) << 2) | ((_incr_ & 0x1) << 1) | (_write_ & 0x1))


////////////////////////////////////////////////////////////////////////////
// Master xtor configuration routines
////////////////////////////////////////////////////////////////////////////
void carbonXAhbSetAbortOnError (u_int32 abortOnError);
void carbonXAhbSetBusyCycles   (u_int32 insertAfter, u_int32 repeatAfter);
void carbonXAhbSetProtection   (u_int32 hprot);
void carbonXAhbSetBurstConfig  (u_int32 hsize, u_int32 incr, u_int32 lock);


////////////////////////////////////////////////////////////////////////////
// READ/WRITE LOCKED/UNLOCKED bursts of different sizes (8-/16-/32-bit)
// Inputs: 
// nbits   => Data width in bits (data size) per transfer (8, 16 or 32)
// beats   => Number of beats (transfers) in the burst.
// incr    => 1: INCR burst, 0: WRAP burst
// lock    => 1: locked burst, 0: non locked burst
// address => Start address of the burst
// wdata   => Write buffer ptr. Pointer to a array of 'beats' number of elements.
// rdata   => Pointer to a block capable of holding 'beats' number of elements.
// elements should be 8-/16-/32-bit wide depending on the value of nbits used.
// All functions return number of beats actually written/read
////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbBurstRead  (u_int32 nbits, u_int32 beats, u_int32 incr, 
		        u_int32 lock,  u_int32 address, u_int32 *rdata);
u_int32 carbonXAhbBurstWrite (u_int32 nbits, u_int32 beats, u_int32 incr,
			u_int32 lock,  u_int32 address, u_int32 *wdata);
#ifdef __cplusplus
}
#endif

#endif
