//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Description: AMBA AHB Bus SLAVE.
// 

`timescale 1 ns / 1 ns

module carbonx_ahb_slave
  (
  // Outputs
  HREADY, HRESP, HRDATA, HSPLITx, 
  // Inputs
  HSELx, HADDR, HWDATA, HWRITE, HTRANS, HSIZE, HBURST, HRESETn, HCLK, 
  HMASTER, HMASTLOCK, HPROT
  );

   parameter   NUM_MASTERS = 4;  // Number of masters. Include dummy master
   
  //-//////////////////////////////////////////////////////////////////////////
  // Slave Memory Config Parameters
  //parameter ADDR_WIDTH = 10;            // Slave memory address width. Default 1Kx8bit
  parameter ADDR_WIDTH = 12;            // Slave memory address width. Default 4Kx8bit

  //-////////////////////////////////////////////////////////////////////////////
  // A H B   I n t e r f a c e
  //-////////////////////////////////////////////////////////////////////////////
  input                    HSELx;    // slave selected
  input [31:0] 		   HADDR;    // Address Bus
  input [31:0] 		   HWDATA;   // Write data bus
  input 		   HWRITE;   // Read or write transfer
  input [1:0] 		   HTRANS;   // Type of transer
  input [2:0] 		   HSIZE;    // Size of a transfer in each beat
  input [2:0] 		   HBURST;   // Burst type
  input 		   HRESETn;  // Reset   
  input 		   HCLK;     // System clock
  input [3:0] 		   HMASTER;  // Active master which owns the BUS
  input                    HMASTLOCK; // Locked transfer
  input [3:0]              HPROT;     // Protection Control

  //
  output 		   HREADY;   // Slave ready for transfer/ insert wait cycles.
  output [1:0] 		   HRESP;    // Slave response (OKAY,ERROR,RETRY,SPLIT) 
  output [31:0] 	   HRDATA;   // Read data bus
  output [NUM_MASTERS-1:0] HSPLITx;  //  Split Resume, driven by test side  
  
`protect
  // carbon license crbn_vsp_exec_xtor_ahb

  //-//////////////////////////////////////////////////////////////////////////
  // Global Parameters
`include "carbonx_ahb_params.vh"

  // Configure the transactor interface
  parameter 		   BFMid     =  1, // Emulator interface (-1 for software interface)
			   ClockId   =  1, // ?
			   DataWidth = 32, // Data width
			   DataAdWid =  4, // Data memory index width, 4-bit (for gp_daddr)
			   GCSwid    = 32, // config reg width, 32-bit (for get_csdata)
			   PCSwid    = 32; // config reg width, 32-bit (for put_csdata)
//-//////////////////////////////////////////////////////////////////////////
// File Name  : carbonx_ahb_slave.v
// 
// Author     : Pala
// Created    : 2002/02/16
//
// Description: AMBA AHB Bus SLAVE.
//
// Revisions  :
//
//  08/29/2005 Jim Stein
//   Added c*rbon licensing features (protect endprotect)
//
//
//-//////////////////////////////////////////////////////////////////////////

  // Include template
`include "xif_core.vh"
  
  //-//////////////////////////////////////////////////////////////////////////
  // State machine states
  parameter 		   BURST_ACTIVE  = 4'd0,
			   ERROR_STATE_0 = 4'd1, 
			   RETRY_STATE_0 = 4'd2,
			   SPLIT_STATE_0 = 4'd3,
			   WAIT_STATE    = 4'd4;
  
  
  //-//////////////////////////////////////////////////////////////////////////
  // L o c a l   S i g n a l s
  //-//////////////////////////////////////////////////////////////////////////
  reg [NUM_MASTERS-1:0]    HSPLITx;
  reg 			   HREADY;
  reg [1:0] 		   HRESP; 
  reg 			   start_split_count;

  reg [3:0] 		   slave_state, next_slave_state;
  reg [1:0] 		   next_HRESP;
  reg 			   next_HREADY;  
  reg [4:0] 		   burst_count;
  reg [4:0] 		   burst_size;   // generated internally from HBURST
  reg [5:0] 		   burst_cycle_count;
  reg [3:0] 		   master_num;
  reg 			   burst_complete_1, burst_complete_2;
  wire 			   burst_complete;

  reg [15:0] 		   split_masters; // Each bit indicates when 1: master is split; 0: master is not split
  reg [5:0] 		   insert_wait_count,next_insert_wait_count;
  reg [21:0] 		   addrs_range; 
  reg [7:0] 		   resume_delay;
  reg [1:0] 		   htrans_d;
  reg [2:0] 		   hsize_d;
  wire [2:0] 		   read_size;
  reg [1:0] 		   hresp_d;
  reg [2:0] 		   hburst_d;
  reg [4:0] 		   index;
  reg 			   hready_d;
  reg 			   hwrite_d;
  reg 			   hsel_d;
  reg 			   burst_type;
  
  // Slave configuration register
  wire [31:0] 		   slave_config; // driven by the interface module. 
  // Config register fields
  wire 			   ERROR_RESP_EN;     // Enable ERROR response insertion
  wire 			   RETRY_RESP_EN;     // Enable RETRY response insertion
  wire 			   SPLIT_RESP_EN;     // Enable SPLIT response insertion
  wire [4:0] 		   SPLIT_RESUME_MID;  // Master to be split or resumed
  wire [5:0] 		   WAIT_COUNT;        // Number of wait cycles to insert
  wire [7:0] 		   USER_RESP_DELAY;   // User specified delay after which to insert specified response.
  wire [7:0] 		   USER_RESUME_DELAY; // User specified dealy after which specified master should be resumed from SPLIT.
  wire 			   USER_RESUME_DELAY_EN; // Enable use of USER_RESUME_DELAY. If this is zero, default of 10 is used.
  //
  wire 			   split_resume;      // Generated when a master is to be resumed.
  wire 			   early_termination; // Indicates that a burst was terminated early, before completion
  wire 			   wait_insertion_en; // 1 when WAIT_COUNT>0
  // Slave memory signals
  wire [31:0] 		   dataout_A;   // Port-A read data (HRDATA)
  wire [ADDR_WIDTH-3:0]    addr_A;      // Port-A address
  reg [3:0] 		   read_enA, read_enA_d;    // Port-A read lane enables (mem enables, in fact)
  reg [ADDR_WIDTH-1:0] 	   HADDR_d;     // Port-B clocked address
  wire [ADDR_WIDTH-3:0]    addr_B;      // Port-B address
  wire 			   mem_wr, mem_rd;// Mem write/read enable
  wire [7:0] 		   dataout_B0, dataout_B1, dataout_B2, dataout_B3;
  wire [7:0] 		   datain_B0,  datain_B1,  datain_B2,  datain_B3;
  reg [3:0] 		   write_enB;   // Port-B Write enables for bytes
  wire 			   enable = 1;  // Port-B enable
  wire [1:0] 		   byte_addrA, byte_addrB;
  
  
  
  // Global reset is combination of hardware and S-side resets 
  wire resetn = (HRESETn==0 || XIF_reset==1) ? 0 : 1;
  wire reset  = ~resetn;


  
  //-//////////////////////////////////////////////////////////////////////////
  // C o n f i g u r a t i o n   r e g i s t e r   f i e l d s
  assign USER_RESUME_DELAY_EN = slave_config[29];
  assign USER_RESUME_DELAY    = slave_config[28:21];
  assign SPLIT_RESUME_MID     = slave_config[20:16];
  assign ERROR_RESP_EN        = (slave_config[15:14]==2'b01)?1:0;
  assign RETRY_RESP_EN        = (slave_config[15:14]==2'b10)?1:0;
  assign SPLIT_RESP_EN        = (slave_config[15:14]==2'b11)?1:0;
  assign USER_RESP_DELAY      = slave_config[13:6];
  assign WAIT_COUNT           = slave_config[5:0]; 

  assign wait_insertion_en    = (WAIT_COUNT>0) ? 1 : 0;
  
  assign early_termination = (((HTRANS==TRANS_NSEQ) || (HTRANS==TRANS_IDLE))  && 
			      (hburst_d != BURST_SINGLE) && (burst_count != 0)) ? 1'b1 : 1'b0;
  
  assign burst_complete = ((HBURST==BURST_SINGLE && HTRANS==TRANS_NSEQ) || 
			   (HTRANS==TRANS_SEQ && burst_count==1)) ? 1'b1 : 1'b0;
  
  // Mimic real slave behavior by allowing mem_wr only when HREADY is high, and
  // response is not RETRY/SPLIT. This should catch more bugs.
  assign mem_wr = (hwrite_d  && hsel_d &&
		   HREADY && HRESP==RESP_OKAY &&
		   (htrans_d==TRANS_NSEQ || htrans_d==TRANS_SEQ)) ? 1 : 0;
  // Mimic real slave behavior by allowing mem_rd only when HREADY is going to be high
  // AND response is not going to be RETRY/SPLIT. This should catch more bugs
  assign mem_rd = (!HWRITE && HSELx &&
		   next_HREADY && next_HRESP==RESP_OKAY &&
		   HTRANS!=TRANS_IDLE) ? 1 : 0;
  
  assign split_resume = (resume_delay== 8'h0) ? 1:0;


  //-//////////////////////////////////////////////////////////////////////////
  // AHB Slave state machine
  //-//////////////////////////////////////////////////////////////////////////
  always @(posedge HCLK or negedge resetn) begin
    if (!resetn) begin 
      slave_state       <= BURST_ACTIVE;
      insert_wait_count <= 0;
      HRESP             <= RESP_OKAY;
      HREADY            <= 1;
    end
    else begin
      slave_state       <= next_slave_state;
      insert_wait_count <= next_insert_wait_count;
      HRESP             <= next_HRESP;
      HREADY            <= next_HREADY;
    end
  end


  //-//////////////////////////////////////////////////////////////////////////
  // Slave State machine.
  // BURST_ACTIVE is the main state where we always stay except for special
  // conditions. Special conditions are second cycle of ERROR/RETRY or SPLIT and
  // for wait cycle insertion when wait cycles are two or more.
  // ERROR/RETRY/SPLIT_STATE_0 state are for the second cycle of these two-cycle
  // responses.
  // WAIT_STATE is reached when number of wait cycles to be inserted is more than
  // one.
  //-//////////////////////////////////////////////////////////////////////////  
  always @(HADDR or HMASTER or HREADY or HRESP or HSELx
	   or HTRANS or addrs_range or burst_cycle_count
	   or ERROR_RESP_EN or insert_wait_count or USER_RESP_DELAY
	   or RETRY_RESP_EN or slave_state or SPLIT_RESP_EN
	   or SPLIT_RESUME_MID or WAIT_COUNT
	   or split_masters or wait_insertion_en) begin

    next_HRESP             = HRESP;  
    next_HREADY            = HREADY;  
    next_slave_state       = slave_state;  
    next_insert_wait_count = insert_wait_count; 
    
    case (slave_state)
      BURST_ACTIVE : begin  // Normal burst or start of burst
        if (HSELx==1) begin // Slave is selected
          if (HTRANS==TRANS_SEQ && HADDR[31:10]!=addrs_range) begin
	    // Burst has crossed 1KB boundry. Give ERROR response
            next_HRESP       = RESP_ERROR;
            next_HREADY      = 0;
            next_slave_state = ERROR_STATE_0;
          end
	  else begin
	    // Check if it is time to insert special cycles (make sure a normal transfer is going on)
	    if ((burst_cycle_count==USER_RESP_DELAY) && (HTRANS==TRANS_NSEQ || HTRANS==TRANS_SEQ)) begin 
	      if (ERROR_RESP_EN==1) begin                        // ERROR insertion is enabled
		next_HRESP       = RESP_ERROR;
		next_HREADY      = 0;
		next_slave_state = ERROR_STATE_0;
	      end
	      else if (RETRY_RESP_EN==1) begin                   // RETRY insertion is enabled
		next_HRESP       = RESP_RETRY;
		next_HREADY      = 0;
		next_slave_state = RETRY_STATE_0; 
	      end
	      else if (SPLIT_RESP_EN==1 &&                       // SPLIT insertion is enabled.
		       HMASTER==SPLIT_RESUME_MID &&              // current master is the one to be split.
		       split_masters[SPLIT_RESUME_MID]==0) begin // And it is not already split.
                next_HRESP       = RESP_SPLIT;
                next_HREADY      = 0;
                next_slave_state = SPLIT_STATE_0;
	      end
	      else if (wait_insertion_en==1)  begin              // Wait insertion is enabled
		next_HRESP             = RESP_OKAY;
		next_HREADY            = 0;
                next_insert_wait_count = insert_wait_count + 1;
                if (WAIT_COUNT==1) begin
		  next_slave_state       = BURST_ACTIVE;
		  next_insert_wait_count = 0;                    // Reset the wait cycle counter
		end
                else
		  next_slave_state = WAIT_STATE;
	      end
	      // Insertion of special cycles is not enabled. Give normal response.
	      else begin
		next_HRESP  = RESP_OKAY;
		next_HREADY = 1;
	      end
	    end
	    // Either special cycle insertion is not enabled or we haven't reached required cycle count yet,
	    // so give normal response.
            else begin
	      next_HRESP = RESP_OKAY;
	      next_HREADY = 1;
            end
          end // else: !if(HTRANS==TRANS_SEQ && HADDR[31:10]!=addrs_range)
        end // if (HSELx==1)
      end // case: BURST_ACTIVE
      
      ERROR_STATE_0 : begin  // Second cycle of ERROR response
        next_HRESP       = RESP_ERROR;
        next_HREADY      = 1;
        next_slave_state = BURST_ACTIVE;
      end

      RETRY_STATE_0 : begin  // Second cycle of RETRY response
        next_HRESP       = RESP_RETRY;
        next_HREADY      = 1;
        next_slave_state = BURST_ACTIVE; 
      end

      SPLIT_STATE_0: begin  // Second cycle of SPLIT response
        next_HRESP       = RESP_SPLIT;
        next_HREADY      = 1;
        next_slave_state = BURST_ACTIVE;
      end
      
      WAIT_STATE   : begin  // inserting wait cycles
	next_HRESP             = RESP_OKAY;
	next_HREADY            = 0;
        if(insert_wait_count==(WAIT_COUNT -1)) begin // Last Wait cycle
          next_slave_state       = BURST_ACTIVE ; 
          next_insert_wait_count = 0;
        end
        else begin                                     // Still more wait cycles to insert
          next_slave_state       = WAIT_STATE;
          next_insert_wait_count = insert_wait_count + 1;  
        end
      end

      default : begin
	// Shouldn't be here. Drive some valid conditions, though.
        next_HRESP       = HRESP;
        next_HREADY      = HREADY;
        next_slave_state = BURST_ACTIVE;
      end
    endcase 
  end // always @ ()


  //-//////////////////////////////////////////////////////////////////////////
  // Clock the current burst master into master_num
  always @ (negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      master_num  <= 0;
      addrs_range <= 0;
    end
    else begin
      if (HSELx==1 && HTRANS==TRANS_NSEQ) begin
	master_num  <= HMASTER;
	addrs_range <= HADDR[31:10];
      end
    end
  end
  
  //-//////////////////////////////////////////////////////////////////////////
  // Keep track of cycle number within a burst
  always @(posedge HCLK or negedge resetn) begin
    if (!resetn) begin
      burst_cycle_count <= 0;
    end
    else begin
      if (HSELx==1 && HTRANS==TRANS_NSEQ) begin
	burst_cycle_count <= 0;
      end
      else if (HSELx==1 && HTRANS==TRANS_SEQ && HREADY==1 && HMASTER==master_num)
	burst_cycle_count <= burst_cycle_count + 1;
    end
  end

  //-//////////////////////////////////////////////////////////////////////////
  // Buffer the bus input signals for next phase
  //-//////////////////////////////////////////////////////////////////////////
  always @(posedge HCLK or negedge resetn) begin
    if (!resetn) begin
      htrans_d         <= 0;
      hsize_d          <= 0;
      hready_d         <= 1;
      hresp_d          <= 0;
      hwrite_d         <= 0;
      hsel_d           <= 0;
      hburst_d         <= 0;
      burst_complete_1 <= 0;
      burst_complete_2 <= 0;
    end
    else begin
      if (HREADY==1) begin
	hwrite_d <= HWRITE;
	htrans_d <= HTRANS;
	hsize_d  <= HSIZE;
	hsel_d   <= HSELx;
	hburst_d <= HBURST; 
	hresp_d  <= HRESP;
      end
      hready_d <= HREADY;
      burst_complete_1 <= burst_complete;
      burst_complete_2 <= burst_complete_1; 
    end   
  end // always @ (posedge HCLK or negedge resetn)
  
  //-//////////////////////////////////////////////////////////////////////////
  // Resume a split master *if* it is  split.
  //-//////////////////////////////////////////////////////////////////////////
  always @(posedge HCLK or negedge resetn) begin
    if (!resetn)
      HSPLITx <= 0;
    else
      // If it is time to resume from split, and the master is really split, resume it.
      if (split_resume==1 && split_masters[SPLIT_RESUME_MID]==1)
        HSPLITx <= 1 << SPLIT_RESUME_MID; // split_resume_vector[NUM_MASTERS-1:0];
      else     
        HSPLITx <= 0;
  end // always


  //-//////////////////////////////////////////////////////////////////////////
  // resume_delay = number of cycles after which to resume a split master
  //-//////////////////////////////////////////////////////////////////////////
  always @(posedge HCLK or negedge resetn) begin
    if (!resetn) begin
      start_split_count <= 0;
      resume_delay      <= 8'ha;
    end
    else begin
      if (slave_state==SPLIT_STATE_0) begin
        start_split_count <= 1;
	if (USER_RESUME_DELAY_EN==1)  // Use user specified delay.
	  resume_delay <= USER_RESUME_DELAY;
      end
      else if (start_split_count==1 && resume_delay != 0)
	resume_delay   <= resume_delay - 1;
      else if (resume_delay==0) begin
        start_split_count <= 0; 
	resume_delay      <= 8'ha;    // Load default delay of 10 cycles
      end
    end
  end // always

  //-//////////////////////////////////////////////////////////////////////////
  // split_masters records split masters
  // burst_count is the current number of beats in a burst
  //-//////////////////////////////////////////////////////////////////////////
  always @ (negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      burst_count <= 0;
      for (index=0; index<16; index=index+1) begin
	split_masters[index] <= 0;
      end
    end
    else begin
      if (HSELx==1 && HTRANS==TRANS_NSEQ)
	// New burst started
	burst_count <= burst_size - 1;
      else if (HTRANS==TRANS_SEQ && HREADY==1 && HMASTER==master_num)
	// Burst continued
	burst_count <= burst_count - 1;

      // Record the current master if we split it. Clear when we resume it.
      if (hsel_d==1 && HREADY==1 && HRESP==RESP_SPLIT)
	split_masters[SPLIT_RESUME_MID] <= 1;
      else if (split_resume==1)
	split_masters[SPLIT_RESUME_MID] <= 0;
    end
  end


  always @ (HBURST) begin
    case (HBURST) 
      BURST_INCR4:begin
        burst_size = 4;
        burst_type = 0;
      end
      BURST_WRAP4: begin
	burst_size = 4;
        burst_type = 1;
      end
      BURST_INCR8: begin
	burst_size = 8;
        burst_type = 0;
      end
      BURST_WRAP8: begin 
        burst_size = 8;
        burst_type = 1;
      end
      BURST_INCR16: begin
        burst_size = 16;
        burst_type = 0;
      end
      BURST_WRAP16:begin
        burst_size = 16;
        burst_type = 1;
      end
      default:     begin
        burst_size = 1;
        burst_type = 0;
      end
    endcase // case(HBURST)
  end

  
  // use clkd address for Write for AHB addr/data timing compliance
  always @(posedge HCLK or negedge resetn) begin
    if (!resetn) begin
      HADDR_d    <= 0;
      read_enA_d <= 0;
    end
    else begin
      if (HREADY==1)      HADDR_d    <= HADDR[ADDR_WIDTH-1:0];
      if (next_HREADY==1) read_enA_d <= read_enA; // For putting zeroes during and in unneeded cycles and lanes.
    end
  end

  
  //-/////////////////////////////////////////////////////////////////////////////
  //
  //                          S L A V E   M E MO R Y
  //
  // dual_port RAM MEM_DEPTHx32
  //   Port A is read-only;  for RDATA
  //   Port B is read/write; for BFM (WDATA) and XIF_core (in future) read
  //-/////////////////////////////////////////////////////////////////////////////

  carbonx_ahb_dpram #(8, ADDR_WIDTH-2, 1<<(ADDR_WIDTH-2)) slave_mem0
    (.clka   (HCLK),      // Port-A read_only 
     .ena    (read_enA[0]),
     .addra  (addr_A),
     .douta  (dataout_A[7:0]),
     .clkb   (HCLK),      // Port-B r/w
     .addrb  (addr_B),
     .dinb   (datain_B0),
     .doutb  (dataout_B0),
     .web    (write_enB[0]),
     .enb    (enable),
     .sinita (reset),
     .sinitb (reset));

  carbonx_ahb_dpram #(8, ADDR_WIDTH-2, 1<<(ADDR_WIDTH-2)) slave_mem1
    (.clka   (HCLK),      // Port-A read_only 
     .ena    (read_enA[1]),
     .addra  (addr_A),
     .douta  (dataout_A[15:8]),
     .clkb   (HCLK),      // Port-B r/w
     .addrb  (addr_B),
     .dinb   (datain_B1),
     .doutb  (dataout_B1),
     .web    (write_enB[1]),
     .enb    (enable),
     .sinita (reset),
     .sinitb (reset));

  carbonx_ahb_dpram #(8, ADDR_WIDTH-2, 1<<(ADDR_WIDTH-2)) slave_mem2
    (.clka   (HCLK),      // Port-A read_only 
     .ena    (read_enA[2]),
     .addra  (addr_A),
     .douta  (dataout_A[23:16]),
     .clkb   (HCLK),      // Port-B r/w
     .addrb  (addr_B),
     .dinb   (datain_B2),
     .doutb  (dataout_B2),
     .web    (write_enB[2]),
     .enb    (enable),
     .sinita (reset),
     .sinitb (reset));

  carbonx_ahb_dpram #(8, ADDR_WIDTH-2, 1<<(ADDR_WIDTH-2)) slave_mem3
    (.clka   (HCLK),      // Port-A read_only 
     .ena    (read_enA[3]),
     .addra  (addr_A),
     .douta  (dataout_A[31:24]),
     .clkb   (HCLK),      // Port-B r/w
     .addrb  (addr_B),
     .dinb   (datain_B3),
     .doutb  (dataout_B3),
     .web    (write_enB[3]),
     .enb    (enable),
     .sinita (reset),
     .sinitb (reset));


  //-//////////////////////////////////////////////////////////////////////////
  // Memory is ALWAYS arranged in little endian order.
  // If AHB is BIG_ENDIAN, we re-order to make it little endian in the memory
  // This also means that the S-Side will get consistantly little endian data
  // irrespective of Verilog side endianness.
  //-//////////////////////////////////////////////////////////////////////////
  // Port-B address
  assign     addr_B     = HADDR_d[ADDR_WIDTH-1:2];
  assign     byte_addrB = HADDR_d[1:0];
  // Port-A address
  assign     addr_A     = (HREADY) ? HADDR[ADDR_WIDTH-1:2] : HADDR_d[ADDR_WIDTH-1:2];
  assign     byte_addrA = (HREADY) ? HADDR[1:0]            : HADDR_d[1:0];
  assign     read_size  = (HREADY) ? HSIZE                 : hsize_d;
  // Enable Port-B Write lanes based on 8/16/32 transfer size
  always @ (hsize_d or byte_addrB or mem_wr) begin
    if (mem_wr) begin             // Write operation
      write_enB = 0;
      case (hsize_d)                // handle 8-/16-/32-bit operations
`ifdef BIG_ENDIAN
	SIZE_8:   write_enB[~byte_addrB] = 1;
	SIZE_16:  begin
	  if (byte_addrB[1]==0) write_enB[3:2] = 2'b11;
	  else                  write_enB[1:0] = 2'b11;
	end
`else
	SIZE_8:   write_enB[byte_addrB] = 1;
	SIZE_16:  begin
	  if (byte_addrB[1]==1) write_enB[3:2] = 2'b11;
	  else                  write_enB[1:0] = 2'b11;
	end
`endif
	SIZE_32: write_enB = 4'b1111;
	default: write_enB = 4'b1111;
      endcase
    end
    else
      write_enB = 0;
  end
  // Enable Port-A Read lanes based on 8/16/32 transfer size
  always @ (byte_addrA or read_size or mem_rd) begin
    if (mem_rd) begin         // Read operation
      read_enA = 0;
      case (read_size)            // handle 8-/16-/32-bit operations
`ifdef BIG_ENDIAN
	SIZE_8:   read_enA[~byte_addrA] = 1;
	SIZE_16:  begin
	  if (byte_addrA[1]==0) read_enA[3:2] = 2'b11;
	  else                  read_enA[1:0] = 2'b11;
	end
`else
	SIZE_8:   read_enA[byte_addrA] = 1;
	SIZE_16:  begin
	  if (byte_addrA[1]==1) read_enA[3:2] = 2'b11;
	  else                  read_enA[1:0] = 2'b11;
	end
`endif
	SIZE_32: read_enA = 4'b1111;
	default: read_enA = 4'b1111;
      endcase
    end
    else begin
      read_enA = 0;
    end
  end
  //-//////////////////////////////////////////////////////////////////////////
  // Assign mem input and mem outputs
  //-////////////////////////////////////////////////////////////////////////// 
  assign     {datain_B3, datain_B2, datain_B1, datain_B0} = HWDATA;
  assign     HRDATA[31:24] = (read_enA_d[3]) ? dataout_A[31:24] : 0;
  assign     HRDATA[23:16] = (read_enA_d[2]) ? dataout_A[23:16] : 0;
  assign     HRDATA[15:08] = (read_enA_d[1]) ? dataout_A[15:08] : 0;
  assign     HRDATA[07:00] = (read_enA_d[0]) ? dataout_A[07:00] : 0;




  
  //-/////////////////////////////////////////////////////////////////////////////
  //
  //                               XIF_core interface
  //
  //-/////////////////////////////////////////////////////////////////////////////
  
  
  //-/////////////////////////////////////////////////////////////////////////////
  //            Config Register
  //-/////////////////////////////////////////////////////////////////////////////
  assign 	slave_config = XIF_get_csdata;

  // Drive XIF_core inputs
  assign 	BFM_reset         = ~HRESETn;
  assign 	BFM_clock         = HCLK;
  assign 	BFM_xrun          = 0; // NA (Not Applicable)
  assign 	BFM_interrupt     = 0; // NA
  //
  assign 	BFM_put_operation = BFM_NXTREQ; // NA
  assign 	BFM_put_address   = 0; // NA
  assign 	BFM_put_size      = 0; 
  assign 	BFM_put_status    = 0; // NA
  assign 	BFM_put_data      = 0;
  assign 	BFM_put_csdata    = 0; // Loop back
  assign 	BFM_put_cphwtosw  = 0; // NA
  assign 	BFM_put_cpswtohw  = 0; // NA
  assign 	BFM_gp_daddr      = 0;
  assign 	BFM_put_dwe       = 0;
  
`endprotect  
endmodule

// EOF

