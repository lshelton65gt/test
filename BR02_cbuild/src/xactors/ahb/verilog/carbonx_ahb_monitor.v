//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Description: AHB Bus protocol monitor transactor. Monitors the bus and checks
//              protocol on AHB bus.  This monitor considers 8 masters and 8
//              slaves, and maintains hardware registers for them. If there are
//              more masters or slaves in the systems, then individual activity
//              of those devices is ignored. The monitor will still monitor
//              the bus for protocol violations.
//              There are 40 registers currently supported.
//
//              1K burst boundary check and locked transfer check are not implemented.
`timescale 1 ns / 1 ns

module carbonx_ahb_monitor
  (
   // Inputs
   HWRITE, HTRANS, HSIZE, HPROT, HADDR, HWDATA, HRDATA, HBUSREQ, 
   HLOCK, HSPLIT, HREADY, HRESP, HBURST, HGRANT, HMASTER, HMASTLOCK, 
   HSEL, HRESETn, HCLK
   );

   parameter   NUM_MASTERS = 4;  // Number of masters. Include dummy master
   parameter   NUM_SLAVES  = 4;  // Number of slaves. Include default and APB bridge as slaves.

   //-////////////////////////////////////////////////////////////////////////////
   // A H B   I n t e r f a c e
   //-////////////////////////////////////////////////////////////////////////////
   input       HWRITE;    // Read or Write transfer
   input [1:0] HTRANS;    // Type of transer
   input [2:0] HSIZE;
   input [3:0] HPROT;
   input [31:0] HADDR;
   input [31:0] HWDATA;
   input [31:0] HRDATA;
   input [NUM_MASTERS-1:0] HBUSREQ;
   input [NUM_MASTERS-1:0] HLOCK;
   input [NUM_MASTERS-1:0] HSPLIT;
   input 		   HREADY;
   input [1:0] 		   HRESP;
   input [2:0] 		   HBURST;
   input [NUM_MASTERS-1:0] HGRANT;
   input [3:0] 		   HMASTER;
   input 		   HMASTLOCK;
   input [NUM_SLAVES-1:0]  HSEL;

   input 		   HRESETn;
   input 		   HCLK;
   
`protect
   // carbon license crbn_vsp_exec_xtor_ahb

   //-//////////////////////////////////////////////////////////////////////////
   // Global Parameters
`include "carbonx_ahb_params.vh"  
   
   // Configure the transactor interface
   parameter 		   BFMid     =  1, // Emulator interface (-1 for software interface)
			     ClockId   =  1, // ?
			     DataWidth = 32, // Data width
			     DataAdWid =  4, // Data memory index width, 4-bit (for gp_daddr)
			     GCSwid    = 32, // config reg width, 32-bit (for get_csdata)
			     PCSwid    = 32; // config reg width, 32-bit (for put_csdata)

   //-//////////////////////////////////////////////////////////////////////////
   // 
   // File Name  : carbonx_ahb_monitor.v
   //
   // Author     : Kiran Maheriya
   // Created    : 07/01/2002
   //
   // Description: AHB Bus protocol monitor transactor. Monitors the bus and checks
   //              protocol on AHB bus.  This monitor considers 8 masters and 8
   //              slaves, and maintains hardware registers for them. If there are
   //              more masters or slaves in the systems, then individual activity
   //              of those devices is ignored. The monitor will still monitor
   //              the bus for protocol violations.
   //              There are 40 registers currently supported.
   //
   //              1K burst boundary check and locked transfer check are not implemented.
   // Revisions  :
   // Kiran Maheriya 05/05/2003
   //    Added software reset. A test would write 1'b1 into config reg bit 0 and then
   //    write 1'b0 after at least one clock cycle. This will act as synchronous software
   //    reset.
   //
   //  08/29/2005 Jim Stein
   //   Added c*rbon licensing features (protect endprotect)
   //
   //
   // $Id$
   //
   //-//////////////////////////////////////////////////////////////////////////

   // Include xif_core template
`include "xif_core.vh"



   //-//////////////////////////////////////////////////////////////////////////
   // L o c a l   S i g n a l s
   //-//////////////////////////////////////////////////////////////////////////
   reg [1:0] 		   htrans_d;
   reg [3:0] 		   hmaster_d;
   reg [2:0] 		   hburst_d;
   reg 			   hwrite_d;
   reg [1:0] 		   hresp_d;
   reg 			   hready_d;
   
   // Internal HSEL, for fixed logic, it is always 16-bit wide unlike HSEL, whose
   // width depends on NUM_SLAVES
   reg [15:0] 		   iHSEL;
   
   // Interface signals
   reg [31:0] 		   regData;
   wire [7:0] 		   regAddr;   // Enough room to handle 256 regs (currently 40 supported)
   wire 		   sReset;
   
   reg [31:0] 		   globalReadCounter;
   reg [31:0] 		   globalWriteCounter;
   reg [31:0] 		   readCounterMaster0;
   reg [31:0] 		   readCounterMaster1;
   reg [31:0] 		   readCounterMaster2;
   reg [31:0] 		   readCounterMaster3;
   reg [31:0] 		   readCounterMaster4;
   reg [31:0] 		   readCounterMaster5;
   reg [31:0] 		   readCounterMaster6;
   reg [31:0] 		   readCounterMaster7;
   
   reg [31:0] 		   writeCounterMaster0;
   reg [31:0] 		   writeCounterMaster1;
   reg [31:0] 		   writeCounterMaster2;
   reg [31:0] 		   writeCounterMaster3;
   reg [31:0] 		   writeCounterMaster4;
   reg [31:0] 		   writeCounterMaster5;
   reg [31:0] 		   writeCounterMaster6;
   reg [31:0] 		   writeCounterMaster7;
   
   reg [31:0] 		   readCounterSlave0;
   reg [31:0] 		   readCounterSlave1;
   reg [31:0] 		   readCounterSlave2;
   reg [31:0] 		   readCounterSlave3;
   reg [31:0] 		   readCounterSlave4;
   reg [31:0] 		   readCounterSlave5;
   reg [31:0] 		   readCounterSlave6;
   reg [31:0] 		   readCounterSlave7;
   
   reg [31:0] 		   writeCounterSlave0;
   reg [31:0] 		   writeCounterSlave1;
   reg [31:0] 		   writeCounterSlave2;
   reg [31:0] 		   writeCounterSlave3;
   reg [31:0] 		   writeCounterSlave4;
   reg [31:0] 		   writeCounterSlave5;
   reg [31:0] 		   writeCounterSlave6;
   reg [31:0] 		   writeCounterSlave7;
   
   reg [31:0] 		   errorHtransCounter;
   reg [31:0] 		   errorHgrantCounter;
   reg [31:0] 		   errorTwoCycleCounter;
   
   reg [31:0] 		   hrespErrorCounter;
   reg [31:0] 		   hrespRetryCounter;
   reg [31:0] 		   hrespSplitCounter;
   
   wire [3:0] 		   nofMastersGranted;
   
   wire 		   resetn;


   assign 		   resetn = HRESETn; // Hardware reset.
   
   
   //-//////////////////////////////////////////////////////////////////////////////
   // regData : register read back. Data out to S-Side
   //-//////////////////////////////////////////////////////////////////////////////
   always @(negedge resetn or posedge HCLK) begin
      if (!resetn)
	regData <= 0;
      else if (sReset)
	regData <= 0;
      else begin
	 case (regAddr[7:0])
           0: regData <= globalReadCounter;
           1: regData <= globalWriteCounter;
           2: regData <= readCounterMaster0;
           3: regData <= readCounterMaster1;
           4: regData <= readCounterMaster2;
           5: regData <= readCounterMaster3;
           6: regData <= readCounterMaster4;
           7: regData <= readCounterMaster5;
           8: regData <= readCounterMaster6;
           9: regData <= readCounterMaster7;
	   
           10: regData <= writeCounterMaster0;
           11: regData <= writeCounterMaster1;
           12: regData <= writeCounterMaster2;
           13: regData <= writeCounterMaster3;
           14: regData <= writeCounterMaster4;
           15: regData <= writeCounterMaster5;
           16: regData <= writeCounterMaster6;
           17: regData <= writeCounterMaster7;
	   
           18: regData <= readCounterSlave0;
           19: regData <= readCounterSlave1;
           20: regData <= readCounterSlave2;
           21: regData <= readCounterSlave3;
           22: regData <= readCounterSlave4;
           23: regData <= readCounterSlave5;
           24: regData <= readCounterSlave6;
           25: regData <= readCounterSlave7;
	   
           26: regData <= writeCounterSlave0;
           27: regData <= writeCounterSlave1;
           28: regData <= writeCounterSlave2;
           29: regData <= writeCounterSlave3;
           30: regData <= writeCounterSlave4;
           31: regData <= writeCounterSlave5;
           32: regData <= writeCounterSlave6;
           33: regData <= writeCounterSlave7;
	   
           34: regData <= errorHtransCounter;
           35: regData <= errorHgrantCounter;
           36: regData <= errorTwoCycleCounter;
	   
           37: regData <= hrespErrorCounter;
           38: regData <= hrespRetryCounter;
           39: regData <= hrespSplitCounter;
	   default regData <= 0;
	 endcase // case(XIF_get_address[7:0])
      end // else: !if(!resetn)
   end // always @ (...
   
   //-//////////////////////////////////////////////////////////////////////////////
   // htrans_d:  Delayed HTRANS. HTRANS represents the address phase, whereas
   //            htrans_d represents the data phase.
   // hmaster_d: Delayed HMASTER. HMASTER represents the address phase for a master,
   //            whereas hmaster_d represents data phase for a master.
   // Don't use software reset here
   //-//////////////////////////////////////////////////////////////////////////////
   always @ (negedge resetn or posedge HCLK) begin
      if (!resetn) begin
	 hready_d  <= 1;
	 hresp_d   <= 0;
	 htrans_d  <= 0;
	 hmaster_d <= 0;
	 hburst_d  <= 0;
	 hwrite_d  <= 0;
	 iHSEL     <= 0;
      end
      else if (sReset) begin
	 hready_d  <= 1;
	 hresp_d   <= 0;
	 htrans_d  <= 0;
	 hmaster_d <= 0;
	 hburst_d  <= 0;
	 hwrite_d  <= 0;
	 iHSEL     <= 0;
      end
      else begin
	 hready_d <= HREADY;   // Updated every cycle
	 hresp_d  <= HRESP;    //    --same--
	 if (HREADY == 1'b1) begin
	    htrans_d  <= HTRANS;     // Updated when HREAD==1
	    hmaster_d <= HMASTER;    //    --same--
            hburst_d  <= HBURST;     //    --same--
	    hwrite_d  <= HWRITE;     //    --same--
            iHSEL[NUM_SLAVES-1:0] <= HSEL[NUM_SLAVES-1:0]; // Only these bits are used
            iHSEL[15:NUM_SLAVES]  <= 0; // Drive the rest with zeros.
	 end
      end
   end
   
   //-//////////////////////////////////////////////////////////////////////////////
   // Global and individual READ/WRITE counters for masters
   //-//////////////////////////////////////////////////////////////////////////////
   always @(negedge resetn or posedge HCLK) begin
      if (!resetn) begin
	 globalReadCounter  <= 0;
	 globalWriteCounter <= 0;
	 
	 readCounterMaster0 <= 0;
	 readCounterMaster1 <= 0;
	 readCounterMaster2 <= 0;
	 readCounterMaster3 <= 0;
	 readCounterMaster4 <= 0;
	 readCounterMaster5 <= 0;
	 readCounterMaster6 <= 0;
	 readCounterMaster7 <= 0;
	 
	 writeCounterMaster0 <= 0;
	 writeCounterMaster1 <= 0;
	 writeCounterMaster2 <= 0;
	 writeCounterMaster3 <= 0;
	 writeCounterMaster4 <= 0;
	 writeCounterMaster5 <= 0;
	 writeCounterMaster6 <= 0;
	 writeCounterMaster7 <= 0;
      end
      else if (sReset) begin // Synchronous, software reset
	 globalReadCounter  <= 0;
	 globalWriteCounter <= 0;
	 
	 readCounterMaster0 <= 0;
	 readCounterMaster1 <= 0;
	 readCounterMaster2 <= 0;
	 readCounterMaster3 <= 0;
	 readCounterMaster4 <= 0;
	 readCounterMaster5 <= 0;
	 readCounterMaster6 <= 0;
	 readCounterMaster7 <= 0;
	 
	 writeCounterMaster0 <= 0;
	 writeCounterMaster1 <= 0;
	 writeCounterMaster2 <= 0;
	 writeCounterMaster3 <= 0;
	 writeCounterMaster4 <= 0;
	 writeCounterMaster5 <= 0;
	 writeCounterMaster6 <= 0;
	 writeCounterMaster7 <= 0;
      end
      else begin
	 if (HREADY==1'b1 && HRESP==RESP_OKAY &&                // Okay response from slave
	     (htrans_d==TRANS_NSEQ || htrans_d==TRANS_SEQ)) begin   // Valid transaction done
	    if (hwrite_d==1) begin                           // Write transfer done
	       globalWriteCounter <= globalWriteCounter + 1;
	       case (hmaster_d) 
		 0: writeCounterMaster0 <= writeCounterMaster0 + 1;
		 1: writeCounterMaster1 <= writeCounterMaster1 + 1;
		 2: writeCounterMaster2 <= writeCounterMaster2 + 1;
		 3: writeCounterMaster3 <= writeCounterMaster3 + 1;
		 4: writeCounterMaster4 <= writeCounterMaster4 + 1;
		 5: writeCounterMaster5 <= writeCounterMaster5 + 1;
		 6: writeCounterMaster6 <= writeCounterMaster6 + 1;
		 7: writeCounterMaster7 <= writeCounterMaster7 + 1;
		 default : begin
		    // synthesis translate_off
		    $display("Monitor: Master %d is not supported", hmaster_d);
		    // synthesis translate_on
		 end	    
	       endcase // case(hmaster_d)
	    end
	    else if (hwrite_d==0) begin                    // Read transfer done
	       globalReadCounter <= globalReadCounter + 1;
	       case (hmaster_d) 
		 0: readCounterMaster0 <= readCounterMaster0 + 1;
		 1: readCounterMaster1 <= readCounterMaster1 + 1;
		 2: readCounterMaster2 <= readCounterMaster2 + 1;
		 3: readCounterMaster3 <= readCounterMaster3 + 1;
		 4: readCounterMaster4 <= readCounterMaster4 + 1;
		 5: readCounterMaster5 <= readCounterMaster5 + 1;
		 6: readCounterMaster6 <= readCounterMaster6 + 1;
		 7: readCounterMaster7 <= readCounterMaster7 + 1;
		 default : begin
		    // synthesis translate_off
		    $display("Monitor: Master %d is not supported", hmaster_d);
		    // synthesis translate_on
		 end
	       endcase // case(hmaster_d)
	    end // if (hwrite_d...
	 end // if (HREADY...
      end // !if (!resetn...
   end // always @(...
   
   
   //-//////////////////////////////////////////////////////////////////////////////
   // Global and individual READ/WRITE counters for slaves
   //-//////////////////////////////////////////////////////////////////////////////
   always @(negedge resetn or posedge HCLK) begin
      if (!resetn) begin
	 readCounterSlave0 <= 0;
	 readCounterSlave1 <= 0;
	 readCounterSlave2 <= 0;
	 readCounterSlave3 <= 0;
	 readCounterSlave4 <= 0;
	 readCounterSlave5 <= 0;
	 readCounterSlave6 <= 0;
	 readCounterSlave7 <= 0;
	 
	 writeCounterSlave0 <= 0;
	 writeCounterSlave1 <= 0;
	 writeCounterSlave2 <= 0;
	 writeCounterSlave3 <= 0;
	 writeCounterSlave4 <= 0;
	 writeCounterSlave5 <= 0;
	 writeCounterSlave6 <= 0;
	 writeCounterSlave7 <= 0;
      end
      else if (sReset) begin // Synchronous, software reset
	 readCounterSlave0 <= 0;
	 readCounterSlave1 <= 0;
	 readCounterSlave2 <= 0;
	 readCounterSlave3 <= 0;
	 readCounterSlave4 <= 0;
	 readCounterSlave5 <= 0;
	 readCounterSlave6 <= 0;
	 readCounterSlave7 <= 0;
	 
	 writeCounterSlave0 <= 0;
	 writeCounterSlave1 <= 0;
	 writeCounterSlave2 <= 0;
	 writeCounterSlave3 <= 0;
	 writeCounterSlave4 <= 0;
	 writeCounterSlave5 <= 0;
	 writeCounterSlave6 <= 0;
	 writeCounterSlave7 <= 0;
      end
      else begin
	 if (HREADY==1'b1 && HRESP==RESP_OKAY &&              // Okay response from slave
	     (htrans_d==TRANS_NSEQ || htrans_d==TRANS_SEQ)) begin // Valid transaction done
	    if (hwrite_d==1) begin                           // Write transfer done
	       case (1'b1) 
		 iHSEL[0]: writeCounterSlave0 <= writeCounterSlave0 + 1;
		 iHSEL[1]: writeCounterSlave1 <= writeCounterSlave1 + 1;
		 iHSEL[2]: writeCounterSlave2 <= writeCounterSlave2 + 1;
		 iHSEL[3]: writeCounterSlave3 <= writeCounterSlave3 + 1;
		 iHSEL[4]: writeCounterSlave4 <= writeCounterSlave4 + 1;
		 iHSEL[5]: writeCounterSlave5 <= writeCounterSlave5 + 1;
		 iHSEL[6]: writeCounterSlave6 <= writeCounterSlave6 + 1;
		 iHSEL[7]: writeCounterSlave7 <= writeCounterSlave7 + 1;
	       endcase // case(1'b1)
	    end
	    else begin
	       case (1'b1) 
		 iHSEL[0]: readCounterSlave0 <= readCounterSlave0 + 1;
		 iHSEL[1]: readCounterSlave1 <= readCounterSlave1 + 1;
		 iHSEL[2]: readCounterSlave2 <= readCounterSlave2 + 1;
		 iHSEL[3]: readCounterSlave3 <= readCounterSlave3 + 1;
		 iHSEL[4]: readCounterSlave4 <= readCounterSlave4 + 1;
		 iHSEL[5]: readCounterSlave5 <= readCounterSlave5 + 1;
		 iHSEL[6]: readCounterSlave6 <= readCounterSlave6 + 1;
		 iHSEL[7]: readCounterSlave7 <= readCounterSlave7 + 1;
	       endcase // case(1'b1)
	    end // else: if (hwrite_d...
	 end // if (HREADY...
      end // else: if (!resetn
   end // always @(...
   
   //-//////////////////////////////////////////////////////////////////////////////
   // HGRANT monitoring
   // Make sure that HGRANT is given to one and only one master.
   // errorHgrantCounter is incremented in case of simultaneous grant to more than
   // one masters.
   //-//////////////////////////////////////////////////////////////////////////////
   always @(negedge resetn or posedge HCLK) begin
      if (!resetn) begin
	 errorHgrantCounter <= 0;
      end
      else if (sReset) begin // Synchronous, software reset
	 errorHgrantCounter <= 0;
      end
      else begin
	 if (|HGRANT == 0) begin // No master is given grant
            // synthesis translate_off 
            $display("%14d: Monitor: No master is given grant!", $time);
            // synthesis translate_on
            errorHgrantCounter <= errorHgrantCounter + 1;
	 end
	 else if (nofMastersGranted > 1) begin // More than one master granted
            // synthesis translate_off
            $display("%14d: Monitor: %d masters are given HGRANT simultaneously (%b)!",
                     $time, nofMastersGranted, HGRANT);
            // synthesis translate_on
            errorHgrantCounter <= errorHgrantCounter + 1;
	 end
      end
   end // always @(...

   assign nofMastersGranted = addbits(HGRANT);

   //-////////////////////////////////////////////////////////////////////////////// 
   // HTRANS monitoring. Check for invalid transitions
   //-////////////////////////////////////////////////////////////////////////////// 
   always @(negedge resetn or posedge HCLK) begin
      if (!resetn) begin
	 errorHtransCounter <= 0;
      end
      else if (sReset) begin // Synchronous, software reset
	 errorHtransCounter <= 0;
      end
      else begin
	 case (HTRANS)
	   // Current HTRANS==TRANS_NSEQ
	   TRANS_NSEQ: begin
              case (htrans_d)
		TRANS_NSEQ: begin
		   if (hburst_d!=BURST_SINGLE && hburst_d!=BURST_INCR && HMASTER==hmaster_d) begin
		      // previous transfer was not a single cycle burst, and was from the same master.
                      // NONSEQ->NONSEQ is invalid
                      // synthesis translate_off
		      $display("%14d: Monitor: Invalid NONSEQ->NONSEQ transition on bus", $time);
                      // synthesis translate_on
		      errorHtransCounter <= errorHtransCounter + 1;
		   end
		end
	      endcase // case(htrans_d)
	   end

	   // Current  HTRANS==TRANS_SEQ
	   TRANS_SEQ: begin
	      case (htrans_d)
		TRANS_IDLE: begin   // Previous HTRANS==TRANS_IDLE
		   if (hresp_d!=RESP_ERROR) begin  // It is okay to have IDLE->SEQ when there is error response. Not otherwise.
		      // Cannot have IDLE->SEQ!
                      // synthesis translate_off
		      $display("%14d: Monitor: Invalid IDLE->SEQ transition on bus", $time);
                      // synthesis translate_on
		      errorHtransCounter <= errorHtransCounter + 1;
		   end
		end
		TRANS_NSEQ: begin // Previous HTRANS==TRANS_NSEQ
		   // Transition from TRANS_NSEQ->TRANS_SEQ is not valid for BURST_SINGLE burst
		   if (hburst_d==BURST_SINGLE) begin
                      // synthesis translate_off
		      $display("%14d: Monitor: Invalid NONSEQ->SEQ transition on bus (SINGLE burst)", $time);
                      // synthesis translate_on
		      errorHtransCounter <= errorHtransCounter + 1;
		   end
		end
	      endcase // case(htrans_d)
	   end

	   // Current  HTRANS==TRANS_BUSY
	   TRANS_BUSY: begin
	      case (htrans_d)
		TRANS_IDLE: begin   // Previous HTRANS==TRANS_IDLE
		   // Cannot have IDLE->BUSY!
		   // synthesis translate_off
		   $display("%14d: Monitor: Invalid IDLE->BUSY transition on bus", $time);
		   // synthesis translate_on
		   errorHtransCounter <= errorHtransCounter + 1;
		end
	      endcase // case(htrans_d)
	   end
	 endcase // case(HTRANS)

      end
   end // always @(...

   //-//////////////////////////////////////////////////////////////////////////////
   // Check two cycle responses (ERROR/RETRY/SPLIT)
   //-//////////////////////////////////////////////////////////////////////////////
   always @(negedge resetn or posedge HCLK) begin
      if (!resetn) begin
	 errorTwoCycleCounter <= 0;
	 hrespErrorCounter    <= 0;
	 hrespRetryCounter    <= 0;
	 hrespSplitCounter    <= 0;
      end
      else if (sReset) begin // Synchronous, software reset
	 errorTwoCycleCounter <= 0;
	 hrespErrorCounter    <= 0;
	 hrespRetryCounter    <= 0;
	 hrespSplitCounter    <= 0;
      end
      else begin
	 case (HRESP)

	   // Current resp is RESP_OKAY
	   RESP_OKAY: begin
	      // Check for the case when a two cycle ERROR/RETRY/SPLIT is not complete
	      if (hready_d==0 && hresp_d!=RESP_OKAY) begin
		 // synthesis translate_off
		 $display("%14d: Monitor: Expected ERROR/RETRY/SPLIT second cycle", $time);
		 // synthesis translate_on
		 errorTwoCycleCounter <= errorTwoCycleCounter + 1;
	      end
	   end

	   // Current resp is RESP_ERROR
	   RESP_ERROR: begin 
	      // Check if previous resp was RESP_ERROR and hready was 0, if not, it's an error
	      if (HREADY==1 && (hready_d==1 || hresp_d!=RESP_ERROR)) begin
		 // synthesis translate_off
		 $display("%14d: Monitor: Received second cycle of ERROR response without first cycle", $time);
		 // synthesis translate_on
		 errorTwoCycleCounter <= errorTwoCycleCounter + 1;
	      end
	      else
		hrespErrorCounter <= hrespErrorCounter + 1;
	   end

	   // Current resp is RESP_RETRY
	   RESP_RETRY: begin
	      // Check if previous resp was RESP_RETRY and hready was 0, if not, it's an error
	      if (HREADY==1 && (hready_d==1 || hresp_d!=RESP_RETRY)) begin
		 // synthesis translate_off
		 $display("%14d: Monitor: Received second cycle of RETRY response without first cycle", $time);
		 // synthesis translate_on
		 errorTwoCycleCounter <= errorTwoCycleCounter + 1;
	      end
	      else
		hrespRetryCounter <= hrespRetryCounter + 1;
	   end

	   RESP_SPLIT: begin  // Current resp is RESP_SPLIT
	      // Check if previous resp was RESP_SPLIT and hready was 0, if not, it's an error
	      if (HREADY==1 && (hready_d==1 || hresp_d!=RESP_SPLIT)) begin
		 // synthesis translate_off
		 $display("%14d: Monitor: Received second cycle of SPLIT response without first cycle", $time);
		 // synthesis translate_on
		 errorTwoCycleCounter <= errorTwoCycleCounter + 1;
	      end
	      else
		hrespSplitCounter <= hrespSplitCounter + 1;
	   end
	 endcase // case(HRESP)  
      end // else: !if(!resetn)
   end // always @ (negedge resetn or posedge HCLK)

   
   


   //-//////////////////////////////////////////////////////////////////////////////
   //
   //                               XIF_core interface
   //
   //-//////////////////////////////////////////////////////////////////////////////

   assign      sReset       = (XIF_get_csdata[0] | XIF_reset);
   assign      regAddr[7:0] = XIF_get_address[7:0]; // Send reg address to the BFM
   
   
   // Drive XIF_core inputs
   assign      BFM_reset         = ~HRESETn;      // Input to XIF_core 
   assign      BFM_clock         = HCLK;          // Input clock to the interface core
   assign      BFM_xrun          = 0; // NA (Not Applicable)
   assign      BFM_interrupt     = 0; // Have to implement this yet.
   //
   assign      BFM_put_operation = BFM_NXTREQ; // NA
   assign      BFM_put_address   = 0; // NA
   assign      BFM_put_size      = 4; // Always send 4 bytes
   assign      BFM_put_status    = 0; // NA
   assign      BFM_put_data      = regData;	   // Data from BFM to S-Side
   assign      BFM_put_csdata    = 0;
   assign      BFM_put_cphwtosw  = 0; // NA
   assign      BFM_put_cpswtohw  = 0; // NA
   assign      BFM_gp_daddr      = 0;
   assign      BFM_put_dwe       = (XIF_get_operation==BFM_READ) ? 1 : 0;  // Write enable
   
   
   
   //-////////////////////////////////////////////////////////////////////////////// 
   // Function to add bits of a vector
   //-////////////////////////////////////////////////////////////////////////////// 
   function [3:0] addbits;
      input [NUM_MASTERS-1:0] invector;
      integer i;
      integer tmp;
      begin
	 tmp = 0;
	 for (i=0; i<NUM_MASTERS; i=i+1) begin
	    tmp = tmp + invector[i];
	 end
	 addbits = tmp;
      end
   endfunction // addbits

`endprotect  
endmodule

// EOF
