//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////
//   Dual Port Synchronus S-ram:  Port A Read Only,  Port B Read/Write
//-//////////////////////////////////////////////////////////////////////////

`timescale 1 ns / 1 ns

module carbonx_ahb_dpram
  (addra,
   addrb,
   clka,
   clkb,
   dinb,
   douta,
   doutb,
   ena,
   enb,
   sinita,
   sinitb,
   web)    /* synthesis syn_black_box */;

  parameter DATA_WIDTH = 8;
  parameter ADDR_WIDTH = 8;
  parameter MEM_DEPTH  = 2<<ADDR_WIDTH;
  parameter TOP_ADDR = "256";
  
  input [ADDR_WIDTH-1:0]  addra;
  input [ADDR_WIDTH-1: 0] addrb;
  input 		  clka;
  input 		  clkb;
  input [DATA_WIDTH-1:0]  dinb;
  output [DATA_WIDTH-1:0] douta;
  output [DATA_WIDTH-1:0] doutb;
  input 		  ena;
  input 		  enb;
  input 		  sinita;
  input 		  sinitb;
  input 		  web;

`protect
// carbon license crbn_vsp_exec_xtor_ahb
  
  // synopsys translate_off

   wire                   ReadOnly = 0;
   
              // ADDRESS_WIDTH, DATA_WIDTH, RAM_SIZE
dnp_synch_ram  #(ADDR_WIDTH, DATA_WIDTH, MEM_DEPTH)
   dnp_synch_ram
   (
    // Port A.
    .SSRA(  sinita   ),        // Active high reset for port A.
    .CLKA(  clka     ),
   
    .ENA(   ena      ),
    .WEA(   ReadOnly ),      // = 0:  Read Only for port A
    .ADDRA( addra    ),
    .DIA( 8'd0 ),
    .DOA(   douta    ),

    // Port B.
    .SSRB(  sinitb   ),     // Active high reset for port B
    .CLKB(  clkb     ),
    
    .ENB(   enb      ),
    .WEB(   web      ),
    .ADDRB( addrb    ),
    .DIB(   dinb     ),
    .DOB(   doutb    )
    );
   
    // synopsys translate_on

endmodule

//
// Two-port synchronous generic RAM module.
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//


module dnp_synch_ram
  (
    // Port A.
    SSRA,
    CLKA,

    ENA,
    WEA,
    ADDRA,
    DIA,
    DOA,

    // Port B.
    SSRB,
    CLKB,

    ENB,
    WEB,
    ADDRB,
    DIB,
    DOB
  );

  // Port parameters.
  parameter ADDRESS_WIDTH = 8;
  parameter DATA_WIDTH    = 8;
  parameter RAM_SIZE      = 256; // Should be >= 2**ADDRESS_WIDTH.

  // Interface, port A.
  input     		      SSRA; // Active high reset for port A.
  input 		      CLKA;
  
  input 		      ENA;
  input 		      WEA;
  input [ADDRESS_WIDTH - 1:0] ADDRA;
  input [DATA_WIDTH - 1:0]    DIA;
  output [DATA_WIDTH - 1:0]   DOA;

  // Interface, port B.
  input     		      SSRB; // Active high reset for port B.
  input 		      CLKB;
  
  input 		      ENB;
  input 		      WEB;
  input [ADDRESS_WIDTH - 1:0] ADDRB;
  input [DATA_WIDTH - 1:0]    DIB;
  output [DATA_WIDTH - 1:0]   DOB;


// Author: Per Bojsen <bojsen@zaiqtech.com>
//
// Filename: carbonx_ahb_dpram.v
//
// Created: Sat Nov 23 15:48:23 EST 2002
//
// Note: This module is a simulation model for a parameterized
//   synchronous dual ported RAM similar to Xilinx Virtex II block RAMs.
//
 
  reg [DATA_WIDTH - 1:0]      DOA;
  reg [DATA_WIDTH - 1:0]      DOB;
  
  // The memory array.
  reg [DATA_WIDTH - 1:0]      ram_core [0:RAM_SIZE - 1];

  // Note, no data collision modelling is done in this simple model.
  // It is assumed that the user is nice and will avoid accessing the
  // same location from both ports simultaneously.

  // Port A process.
  always @(posedge CLKA or posedge SSRA)
    begin
      if (SSRA == 1'b1)
	begin
	  DOA <= {DATA_WIDTH{1'b0}};
	end
      else
	begin
	  // Read.
	  if (ENA)
	    DOA <= ram_core[ADDRA];

	  // Write.
	  if (ENA && WEA)
	    ram_core[ADDRA] <= DIA;
	end
    end
  
  // Port B process.
  always @(posedge CLKB or posedge SSRB)
    begin
      if (SSRB == 1'b1)
	begin
	  DOB <= {DATA_WIDTH{1'b0}};
	end
      else
	begin
	  // Read.
	  if (ENB)
	    DOB <= ram_core[ADDRB];

	  // Write.
	  if (ENB && WEB)
	    ram_core[ADDRB] <= DIB;
	end
    end
   
endmodule // dnp_synch_ram

`endprotect  
// End of file slave_dpram.v
