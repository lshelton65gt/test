//-///////////////////////////-*-verilog-mode-*-/////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////


//-//////////////////////////////////////////////////////////////////////////
// AHB Transfer types (HTRANS)
parameter   TRANS_IDLE = 2'b00,
	    TRANS_BUSY = 2'b01,
	    TRANS_NSEQ = 2'b10,
	    TRANS_SEQ  = 2'b11;

//-//////////////////////////////////////////////////////////////////////////
// AHB Burst Types (HBURST)
parameter   BURST_SINGLE = 3'b000, // Single transfer
	    BURST_INCR   = 3'b001, // Incremental burst of unspecified length
	    BURST_WRAP4  = 3'b010, // 4-beat wrapping burst
	    BURST_INCR4  = 3'b011, // 4-beat incremental burst
	    BURST_WRAP8  = 3'b100, // 8-beat wrapping burst
	    BURST_INCR8  = 3'b101, // 8-beat incremental burst
	    BURST_WRAP16 = 3'b110, // 16-beat wrapping burst
	    BURST_INCR16 = 3'b111; // 16-beat incremental burst

//-//////////////////////////////////////////////////////////////////////////
// AHB Response Types (HRESP)
parameter   RESP_OKAY  = 2'b00,
	    RESP_ERROR = 2'b01,
	    RESP_RETRY = 2'b10,
	    RESP_SPLIT = 2'b11;

//-//////////////////////////////////////////////////////////////////////////
// AHB Transfer Sizes (HSIZE)
parameter   SIZE_8    = 0,   // 8-bit transfer (beat)
	    SIZE_16   = 1,   // 16-bit transfer
	    SIZE_32   = 2,   // 32-bit transfer
	    SIZE_64   = 3,   // 64-bit transfer
	    SIZE_128  = 4,   // 128-bit transfer
	    SIZE_256  = 5,   // 256-bit transfer
	    SIZE_512  = 6,   // 512-bit transfer
	    SIZE_1K   = 7,   // 1024-bit transfer
	    SIZE_1024 = 7;   // 1024-bit transfer

