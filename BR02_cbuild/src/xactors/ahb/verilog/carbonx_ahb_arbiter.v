//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Description: Arbiter for use within AMBA SVC. Supports up to 16 masters 
//              and 16 slaves

module carbonx_ahb_arbiter (HCLK,		
			 HRESETn,	       
			 HBUSREQ, 
  			 HLOCK,
			 HSPLIT,
			 HREADY,
			 HRESP,
			 HBURST,
			 HTRANS,
			 HGRANT, 
			 HMASTER,
			 HMASTLOCK,
			 HMaster_d );

   parameter   NUM_MASTERS = 4;  // Number of masters. Include dummy master

  // -- Port Declarations
  input                       HCLK;
  input 		      HRESETn;	       
  input [NUM_MASTERS-1:0]     HBUSREQ;  // from Master 
  input [NUM_MASTERS-1:0]     HLOCK;    // from Master
  input [NUM_MASTERS-1:0]     HSPLIT;   // from Slave
  input 		      HREADY;   // from Slave
  input [1:0] 		      HRESP;    // from Slave
  input [2:0] 		      HBURST;   // from Master
  input [1:0] 		      HTRANS;   // from Master
  
  output [NUM_MASTERS-1:0]    HGRANT;
  output [3:0] 		      HMASTER;
  output 		      HMASTLOCK;
  output [3:0] 		      HMaster_d; // to monitor		   

`protect
  // carbon license crbn_vsp_exec_xtor_ahb

`include "carbonx_ahb_params.vh"

  // Main configuration parameters. Select one of the algorithms.
  parameter 		      EARLY_TERMINATE = 1'b0;   // was config bit
  parameter 		      MASK_DUMMY      = 1'b1;   // was config bit
  parameter 		      DEFAULT_MASTER  = 4'b1;   // was config bit
  parameter 		      DUMMY_MASTER    = 4'b0;   // was config bit
  
  // state machine
  parameter 		      IDLE        = 2'b00,
			      GRANT       = 2'b01,
   			      BURSTACTIVE = 2'b10;
  // HRESP codes
  parameter 		      OK          = RESP_OKAY,
   			      ERROR       = RESP_ERROR,
   			      RETRY       = RESP_RETRY,
   			      SPLIT       = RESP_SPLIT;
  
  parameter 		      HTRANS_IDLE = TRANS_IDLE,
			      HTRANS_BUSY = TRANS_BUSY,
			      HTRANS_NSEQ = TRANS_NSEQ,
			      HTRANS_SEQ  = TRANS_SEQ;
  parameter 		      HBURST_SINGLE = BURST_SINGLE;
  
//-//////////////////////////////////////////////////////////////////////////
// 
//
// File Name  : carbonx_ahb_arbiter.v
// 
//
// Author     : Mark Rigby
// Created    : 31 January 2002
//
// Description: Arbiter for use within AMBA SVC. Supports up to 16 masters 
//              and 16 slaves
//
// // Revisions  :
//
//  08/29/2005 Jim Stein 
//   Added c*rbon licensing features (protect endprotect)
//
// $Id$
//
//-//////////////////////////////////////////////////////////////////////////

/*

Revision 1.1  2005/11/08 21:38:38  knutson
Reviewed by: John Hoglund
Tests Done: checkin, xactors
Bugs Affected:

Added Source Code and Tests for. Ethernet MII, Ethernet GMII, Ethernet XGMII,
AHB and PCI.
Also renamed C header files and verilog module names and filenames
with the new naming conventions for transactors.
The AHB now has a full testsuite, ported from Zaiq tests. The other transactors has
one test each that is the same as in the example directory.
The example tests are broken right now, they will be fixed in a later commit.

A new testgroup xactors was added to runlist and alltests.xml.
To run the xactor testsuite run runlist -xactors

The new transactors library files was added to runlist.bom

Revision 1.3  2005/10/20 18:23:28  jstein
Reviewed by: Dylan Dobbyn
Tests Done: scripts/checkin + xactor regression suite
Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
added html/xactors dir, and html/xactors/adding_xactors.txt doc

Revision 1.2  2005/09/29 23:53:43  jstein
Reviewed by: Dylan Dobbyn
Tests Done:  checkin, xactor regression
Bugs Affected: none
 Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
 moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
 so they will not be seen by the customer
 removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include

Revision 1.1  2005/09/25 16:58:31  jstein
Reviewed by: none
Tests Done: usb2.0 xactor test
Bugs Affected: Adding Verilog unprotected source code to sandbox

Revision 1.11  2005/06/03 18:07:38  colby_w
carbon updates

Revision 1.9  2004/05/24 22:45:31  fredieu
Fix for proper error message!


*/

  // Priority mux signals
  wire [NUM_MASTERS-1:0]      hreq_masked;

  reg [NUM_MASTERS-1:0]       round_robin_grant;
  reg [NUM_MASTERS-1:0]       round_robin_result;
  wire [NUM_MASTERS-1:0]      fixed_pri_grant;
  wire [NUM_MASTERS-1:0]      priority;
  reg [3:0] 		      rrCount, rrCountNext;
  
  wire 			      req;
  reg [NUM_MASTERS-1:0]       hgrant_i, hgrant_d;    // internal (final) hgrant and its clock delayed version
  reg [NUM_MASTERS-1:0]       hgrant_tmp;            // temp (immidiate) grant. Combinatorial.
  reg [NUM_MASTERS-1:0]       split_mask, split_mask_tmp;
  reg [1:0] 		      state;
  reg [3:0] 		      burst_count;
  reg 			      last_beat;
  reg 			      hmastlock_i;
  reg [3:0] 		      hmaster_i;
  reg [3:0] 		      HMaster_d;
  reg [NUM_MASTERS-1:0]       current_fixed_pri_grant;
  reg 			      new_burst; 
  reg 			      set_def_master;
  reg 			      set_hmaster;
  reg 			      grant_not_changed;
  integer 		      y1,y3, y4;         // indexes for for loops
  reg 			      early_termination; // Flag early termination condition
  reg 			      done;
  wire 			      sys_clk;
  
  assign sys_clk     = HCLK;
  //  assign HGRANT      = (HREADY==1 && HTRANS==HTRANS_NSEQ && HBURST==HBURST_SINGLE && |(priority)) ? priority : hgrant_i;
  assign HGRANT      = hgrant_i;
  assign HMASTLOCK   = hmastlock_i;
  assign HMASTER     = hmaster_i;
  
  
  // MASK hreq for split transfers
  assign hreq_masked = HBUSREQ & (~split_mask_tmp);

  // Flag that a req is received
  assign req = |hreq_masked;
  


  always @(HBURST or HMASTER or HMaster_d or HREADY
	   or HRESP or HTRANS or fixed_pri_grant or hreq_masked
	   or rrCount or rrCountNext or state) begin
    // Rotate right the request. Also, rotate left the received grant to get final grant.
    if ((HREADY==1 &&  HTRANS==HTRANS_NSEQ && HBURST==HBURST_SINGLE) || // Single transfer OR
	(HREADY==0 && (HRESP==RETRY || HRESP==SPLIT) &&  // First cycle of SPLIT/RETRY
	 HMASTER==HMaster_d &&                           // And Grant has not changed
	 state!=GRANT)) begin                            // And Grant is not going to change
	// Give grant to next master in priority
      round_robin_result = RotateRight(hreq_masked, rrCountNext);
      round_robin_grant  = RotateLeft(fixed_pri_grant, rrCountNext);
    end
    else begin
      // Normal assignment of grant, use rrCount to rotate
      round_robin_result = RotateRight(hreq_masked, rrCount);
      round_robin_grant  = RotateLeft(fixed_pri_grant, rrCount);
    end
  end
  // Get the grant for rotated request
  assign fixed_pri_grant    = getGrant(round_robin_result);
  assign priority           = round_robin_grant;

  
  
  //-/////////////////////////////////////////////////////////////////////////////
  // Arbiter State Machine
  //-/////////////////////////////////////////////////////////////////////////////
  always @(negedge HRESETn or posedge sys_clk) begin
    if (!HRESETn)
      state <= IDLE;
    else begin
      case (state) 
	IDLE: if (HREADY == 1 && req == 1) state <= GRANT;
	
	GRANT: begin 
	  if (HREADY == 1) begin  // Make sure bus is ready
	    if (priority==0) // No master is set to get grant.
	      state <= IDLE;
	    else begin
	      if (HTRANS == HTRANS_NSEQ)  begin
		if (HBURST == HBURST_SINGLE) // single access already active, can stay in grant
		  state <= GRANT;
		else
		  state <= BURSTACTIVE;      // go to burst active state
	      end
	      else // no transfer has started
		state <= BURSTACTIVE;        // go to burst active state
	    end
	  end
	end // case: GRANT
	
	BURSTACTIVE: begin 
	  if (HREADY == 1) begin
	    if (early_termination)
	      state <= GRANT;                // next state is grant for new higher priority burst
	    else begin 
	      // check for normal transfer response
	      if (priority==0)
		state <= IDLE;
	      else if (HRESP == OK && (HTRANS == HTRANS_NSEQ || HTRANS == HTRANS_SEQ)) begin
		casez (HBURST) // check length of burst
		  3'b000 : begin // SINGLE transfer
		    state <= GRANT;
		  end
		  3'b001 : begin // INCR transfer of unspecified length.
		    if (~|(HBUSREQ & hgrant_i)) // if master with priority no longer requesting.
		      state <= GRANT;
		  end
		  3'b01? : begin // 4 beat burst transfer (either inc or wrap)
		    if ((burst_count == 4'b0001 && hmastlock_i == 0) ||
			(burst_count == 4'b0010)) begin // check for cycle before penultimate transfer
		      state <= GRANT;
		    end
		  end
		  3'b10? : begin // 8 beat burst transfer (either inc or wrap)
		    if ((burst_count == 4'b0101 && hmastlock_i == 0) ||
			(burst_count == 4'b0110)) begin
		      state <= GRANT;
		    end
		  end
		  3'b11? : begin // 16 beat burst transfer (either inc or wrap)
		    if ((burst_count == 4'b1101 && hmastlock_i == 0) ||
			(burst_count == 4'b1110)) begin
		      state <= GRANT;
		    end
		  end
		  default : begin
		    state <= IDLE;  // Shouldn't be here
                  end
		endcase // casez(HBURST)
	      end
	      else if ((HTRANS == HTRANS_IDLE) && // if trans = idle, false request return to grant
		       ((grant_not_changed == 1 && new_burst == 1) || new_burst != 1))
		state <= GRANT;
	    end // else: !if(early_termination)
	  end // if (HREADY == 1)
	  else begin
	    if (HRESP==RETRY || HRESP==SPLIT) // For okay and ERROR, don't do anything.
	      state <= GRANT;
	  end // else: !if(HREADY == 1)
	end // case: BURSTACTIVE	
      endcase // case(state)
    end // else: !if(!HRESETn)
  end // @always

  //-/////////////////////////////////////////////////////////////////////////////
  // Some flag signals assignments. They indicate as follows:
  // new_burst         : Start of new burst.
  // set_def_master    : Default master should be given grant.
  // set_hmaster       : The hmaster_i should be updated.
  // grant_not_changed : Grant has not changed since last arbitration
  //-/////////////////////////////////////////////////////////////////////////////
  always @(posedge sys_clk or negedge HRESETn) begin    
    if (!HRESETn) begin
      new_burst          <= 0;
      set_def_master     <= 0;
      set_hmaster        <= 0;
      grant_not_changed  <= 0;
    end
    else begin
      case (state) 	
	IDLE: begin 
	  if (HREADY == 1) begin      // only change signals when not wait cycle
	    if (req == 1)             // if request and slave ready go to grant state
	      set_def_master <= 0;
	    else                      // if no request set default conditions
	      set_def_master <= 1;
	  end
	end
	
        GRANT: begin 
	  if (HREADY == 1) begin // go to burst active if slave ready
	    // check to see if master is requesting
	    if (|priority) begin
	      // do not set new priority if existing master has started transfer and no burst termination set
	      if (HTRANS == HTRANS_NSEQ)  begin
		if (HBURST == HBURST_SINGLE) begin // single access already active, can stay in grant
		  set_hmaster <= 1; // set flag to update hmaster next cycle
		end
		else begin
		  set_hmaster <= 0;
		  if (EARLY_TERMINATE == 1) begin // do not update grant if burst and not terminate
		    if (hgrant_i == priority)
		      grant_not_changed <= 1;
		  end
		end
	      end
	      else begin // no transfer has started
		set_hmaster <= 0;
		if (hgrant_i == priority)
		  grant_not_changed <= 1;
	      end
	      new_burst <= 1; // set new burst flag so HMASTER can be set.
	    end
	  end
	end
	
	BURSTACTIVE: begin 
	  if (HREADY == 1 && new_burst==1) begin
	    new_burst <= 0; // clear new burst flag
	    grant_not_changed <= 0; // clear grant_not_changed flag
	  end
	end
      endcase // case(state)
    end // else: !if(!HRESETn)
  end // always @ (...

  
  //-/////////////////////////////////////////////////////////////////////////////
  // Save current priority in current_fixed_pri_grant
  //-/////////////////////////////////////////////////////////////////////////////
  always @(posedge sys_clk or negedge HRESETn) begin    
    if (!HRESETn)
      current_fixed_pri_grant <= 0;
    else begin
      if (state==GRANT && HREADY==1 && |priority)
	// Save current priority so that we can use it later to decide priority
	current_fixed_pri_grant <= round_robin_grant;
    end
  end
  
  //-/////////////////////////////////////////////////////////////////////////////
  // hgrant_i, hmastlock_i, hmaster_i
  //-/////////////////////////////////////////////////////////////////////////////
  always @(posedge sys_clk or negedge HRESETn) begin    
    if (!HRESETn) begin
      hgrant_i <= 1;    // give default after reset to master0
      hgrant_d <= 1;
    end
    else begin
      hgrant_i <= hgrant_tmp;
      hgrant_d <= hgrant_i; // Clock delayed hgrant.
    end
  end
  always @(HREADY or HRESP or hgrant_i or hgrant_d or 
	   priority or split_mask_tmp or state or
	   HTRANS or HBURST) begin
    if ((HREADY==1 && state==GRANT) ||                  // scheduled grant change OR
        (HREADY==1 && HTRANS==HTRANS_NSEQ && HBURST==HBURST_SINGLE) ||
	(HREADY==0 && (HRESP==SPLIT || HRESP==RETRY) && // 1st cycle of SPLIT/RETRY
	 hgrant_i==hgrant_d)) begin                     // make sure that grant was same in the last cycle
      // Scheduled grant change (state==GRANT), or first cycle of 2-cycle SPLIT/RETRY
      // We made sure that grant was same in the previous cycle so that the master who was given grant would
      // have had chance to use it. If not, we don't want to change the grant.
      if (priority == 0) begin
	hgrant_tmp = 0;
	// No requests OR all masters are split, set default conditions
	if (split_mask_tmp[DEFAULT_MASTER] != 1) // If default master is not split (have to use split_mask_tmp)
	  hgrant_tmp[DEFAULT_MASTER] = 1;        // give grant to defualt master.
	else
	  hgrant_tmp[DUMMY_MASTER] = 1;          // else, give grant to dummy master.	  
      end
      else begin
	// There is/are requests AND at least one master is not split. Update the grant
	hgrant_tmp = priority;
      end
    end
    else
      hgrant_tmp = hgrant_i;  // Hold the old value if none of above
  end
  
  always @(posedge sys_clk or negedge HRESETn) begin    
    if (!HRESETn) begin
      hmastlock_i             <= 0;
      hmaster_i               <= 4'b0;
    end
    else begin
      if (HREADY==1) begin
	// Following logic will lag by one cycle after hgrant_i changes
	for (y1=0; y1<NUM_MASTERS; y1=y1+1) begin // loop to shift each bit
	  if (HGRANT[y1] == 1) begin         // check to see if priority bit active
	    hmastlock_i <= HLOCK[y1];
	    hmaster_i[3:0] <= y1;
	  end		 
	end
      end
    end
  end // always @ (...

  //-/////////////////////////////////////////////////////////////////////////////
  // split_mask
  //-/////////////////////////////////////////////////////////////////////////////
  always @(posedge sys_clk or negedge HRESETn) begin    
    if (!HRESETn) split_mask <= 0;
    else          split_mask <= split_mask_tmp;
    // synthesis translate_off
    //if (HSPLIT[y4] == 1 && split_mask[y4] != 1) begin
		    //$tbp_error("SPLIT resume for master%d, which was NOT split! at %t with split_mask[y4] = %d and HSPLIT[y4] = %d\n",
	            //         y4,$time,split_mask[y4],HSPLIT[y4]);
	//	    $display("SPLIT resume for master%d, which was NOT split! at %t with split_mask[y4] = %d and HSPLIT[y4] = %d\n",
	 //                     y4,$time,split_mask[y4],HSPLIT[y4]);
    //end
    // synthesis translate_on
	
  end

  always @(HREADY or HRESP or HSPLIT or hmaster_i or split_mask) begin
    split_mask_tmp = split_mask;
      
    // check split mask each cycle against HSPLIT
    for (y4=0; y4<NUM_MASTERS; y4=y4+1) begin
      if (HSPLIT[y4] == 1 && split_mask[y4] == 1)
	// If split resume (HSPLIT[y4]) is detected for master y4, and it was
	// split earlier (its split mask set to 1), clear the split mask of master.
	split_mask_tmp[y4] = 0;
    end
    if (HREADY==0 && HRESP == SPLIT) begin
      split_mask_tmp[hmaster_i] = 1;
    end
  end

  //-/////////////////////////////////////////////////////////////////////////////
  // last_beat, rrCount
  //-/////////////////////////////////////////////////////////////////////////////
  always @(posedge sys_clk or negedge HRESETn) begin
    if (!HRESETn) begin
      rrCount       <= 4'h1; //
      rrCountNext   <= 4'h2; // It is critical for rrCountNext come out of reset as rrCount+1
      last_beat     <= 0;
    end
    else begin
      if ((HREADY==1 && HTRANS==HTRANS_NSEQ && HBURST==HBURST_SINGLE) ||  // Single transfer
	  (HREADY==0 && (HRESP==RETRY || HRESP==SPLIT) &&   // First cycle of SPLIT/RETRY
	   HMASTER==HMaster_d &&                            // And Grant has not changed
	   state!=GRANT)) begin                             // And Grant is not going to change
	// Single Transfer. Make sure another master gets grant next time.
	last_beat <= 1;
	rrCount <= rrInc(rrCount); rrCountNext <= rrInc(rrCountNext);
      end
      else if (state == BURSTACTIVE && HREADY == 1) begin
	if (last_beat == 1) begin
	  last_beat <= 0;
	end
	// check to see if higher priority request is active and current transfer not locked
	else if (early_termination) begin
	  last_beat <= 1;          // terminate current burst and tidy up.
	  rrCount <= rrInc(rrCount); rrCountNext <= rrInc(rrCountNext);
	end
	else begin     // If not early termination
	  // check for normal transfer response
	  if (HRESP == OK && (HTRANS == HTRANS_NSEQ || HTRANS == HTRANS_SEQ)) begin
	    casez (HBURST) // check length of burst
	      3'b000 : begin 
		last_beat <= 1;     // SINGLE transfer, this results in the master being 
		// given an additional grant cycle at the end of the burst
		rrCount <= rrInc(rrCount); rrCountNext <= rrInc(rrCountNext);       // increment or clear RR counter
	      end
	      3'b001 : begin           // Inc transfer of unspecified length.
		if (~|(HBUSREQ & hgrant_i)) begin // if master with priority no longer requesting.
		  last_beat <= 1;      
		  rrCount <= rrInc(rrCount); rrCountNext <= rrInc(rrCountNext);       // increment or clear RR counter
		end
	      end
	      3'b01? : begin // 4 beat burst transfer (either inc or wrap)
		if ((burst_count == 4'b0001 && hmastlock_i == 0) ||
		    (burst_count == 4'b0010)) begin // check for cycle before penultimate transfer
		  last_beat <= 1;
		  rrCount <= rrInc(rrCount); rrCountNext <= rrInc(rrCountNext); // increment or clear RR counter
		end
	      end
	      3'b10? : begin // 8 beat burst transfer (either inc or wrap)
		if ((burst_count == 4'b0101 && hmastlock_i == 0) ||
		    (burst_count == 4'b0110)) begin
		  last_beat <= 1;
		  rrCount <= rrInc(rrCount); rrCountNext <= rrInc(rrCountNext); // increment or clear RR counter
		end
	      end
	      3'b11? : begin // 16 beat burst transfer (either inc or wrap)
		if ((burst_count == 4'b1101 && hmastlock_i == 0) ||
		    (burst_count == 4'b1110)) begin
		  last_beat <= 1;
		  rrCount <= rrInc(rrCount); rrCountNext <= rrInc(rrCountNext); // increment or clear RR counter
		end
	      end
	      default : begin
              end
	    endcase // casez(HBURST)
	  end
	  else begin
	    if (HTRANS == HTRANS_IDLE) begin // if trans = idle, false request return to grant or idle
	      if  ((grant_not_changed == 1 && new_burst == 1) || new_burst != 1) begin
		last_beat <= 1;
	      end
	    end
	  end // else: !if(HRESP == OK && (HTRANS == HTRANS_NSEQ || HTRANS == HTRANS_SEQ))
	end // else: !if(early_termination)
      end // if (state==BURSTACTIVE && HREADY == 1)
    end // else: !if(!HRESETn)
  end // always @ (posedge sys_clk or negedge HRESETn)
  
  //-/////////////////////////////////////////////////////////////////////////////
  // Burst Count
  //-/////////////////////////////////////////////////////////////////////////////
  always @(posedge sys_clk or negedge HRESETn) begin    
    if (!HRESETn)
      burst_count <= 4'b0;
    else begin
      // check last beat each cycle
      if (last_beat == 1) begin
	if (HREADY == 1 && hgrant_i == priority && HTRANS == HTRANS_NSEQ)
	  // new burst started already !!!need to check this??? orig != nonseq
	  burst_count <= 4'b1; // set counter to 1
	else // no new burst yet, clear counter
	  burst_count <= 4'b0; // clear burst counter
      end
      else if (state==BURSTACTIVE && early_termination==0) begin
	// check for normal transfer response
	if (HREADY==1 && HRESP == OK && (HTRANS == HTRANS_NSEQ || HTRANS == HTRANS_SEQ)) begin
	  // don't increment burst count when previouse access still finishing
	  if (new_burst == 1 && HTRANS == HTRANS_SEQ)
	    burst_count <= 0;
	  else
	    burst_count <= burst_count + 1; // increment burst count
	end
      end
    end
  end
  

  //-/////////////////////////////////////////////////////////////////////////////
  // Generate EARLY_TERMINATION flag so that a burst for a master can be
  // terminated.
  //-/////////////////////////////////////////////////////////////////////////////
  always @(negedge HRESETn or posedge sys_clk) begin
    if (!HRESETn)
      early_termination <= 0;
    else begin
      if (early_termination)
	early_termination <= 0; // Reset the bit
      else begin
	done = 0;
	for (y3=0; y3<NUM_MASTERS; y3=y3+1) begin // loop to check each bit
	  if (!done && current_fixed_pri_grant[y3] == 1) // check current priority
	    done = 1;        // no higher priority request, exit loop.
	  else if (round_robin_grant[y3] == 1 && hmastlock_i == 0 && // check if higher priority exists
		   EARLY_TERMINATE == 1) begin // software prog to disable early terminate.
	    early_termination <= 1;    // next state is grant for new higher priority burst
	    done = 1;          // higher priority exists, exit loop.
	  end
	end
      end
    end
  end
  
  // implement muxing for hwdata (clock the hmaster signal into HMaster_d)
  always @ (negedge HRESETn or posedge HCLK) begin
    if (!HRESETn)
      HMaster_d <= 0;
    else
      if (HREADY == 1)
	HMaster_d <= hmaster_i;
  end

  // Rotate left a vector
  function [NUM_MASTERS-1:0] RotateLeft;
    input [NUM_MASTERS-1:0] invec;
    input [3:0] shiftcnt;
    integer j;
    begin
      //RotateLeft[NUM_MASTERS-1] = invec[NUM_MASTERS-1];
      for(j=0; j<NUM_MASTERS; j=j+1) begin
	if((j+shiftcnt) < NUM_MASTERS) begin
	  RotateLeft[j+shiftcnt] = invec[j];
	end
	else begin // wrap around bits overflowing vector
	  RotateLeft[j+shiftcnt-NUM_MASTERS] = invec[j];
	end
      end
    end
  endfunction // RotateLeft

  // Rotate right a vector
  function [NUM_MASTERS-1:0] RotateRight;
    input [NUM_MASTERS-1:0] invec;
    input [3:0] shiftcnt;
    integer j;
    begin
      //RotateRight[NUM_MASTERS-1]  = invec[NUM_MASTERS-1];
      for(j=0; j<NUM_MASTERS; j=j+1) begin
	if((j+shiftcnt) < NUM_MASTERS) begin
	  RotateRight[j] = invec[j+shiftcnt];
	end
	else begin // wrap around bits overflowing vector
	  RotateRight[j] = invec[j+shiftcnt-NUM_MASTERS];
	end
      end
    end
  endfunction // RotateRight
  
  // Priority decoder. reqest[0] has highest priority
  // Highest priority active request receives the grant
  function [NUM_MASTERS-1:0] getGrant;
    input [NUM_MASTERS-1:0] request;
    integer i;
    reg done;
    begin
      done=0;
      getGrant = 0;
      for (i=0; i<NUM_MASTERS; i=i+1) begin
        if (done==0 && request[i]) begin
          getGrant[i] = 1;
          done = 1; // End loop
        end
      end  
    end  
  endfunction // getGrant

  
  function [3:0] rrInc;
    input [3:0] inCount;
    begin
      if (inCount >= (NUM_MASTERS-1)) rrInc = 1;
      else                            rrInc = inCount + 1;
    end
  endfunction // round_robin_inc
  
`endprotect  
endmodule // ahb_arbiter_xtor

// EOF

