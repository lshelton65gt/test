//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Description: AMBA Bus master transactor BFM part.
//              This transactor currently supports 32-bit data bus, and hence
//              transfer sizes (as in HSIZE) of up to 32 bits per transaction.
//              This is the synthesizable part. Interface module is separate.
//
`timescale 1 ns / 1 ns

module carbonx_ahb_master
  (
  // Outputs
  oHBUSREQ, oHLOCK, oHWRITE, oHTRANS, oHSIZE, oHBURST, oHPROT, 
  oHADDR, oHWDATA, 
  // Inputs
  HGRANT, HREADY, HRESETn, HCLK, HRESP, HRDATA, HINTERRUPT
  );


  //-////////////////////////////////////////////////////////////////////////////
  // A H B   I n t e r f a c e
  //-////////////////////////////////////////////////////////////////////////////
  output 	          oHBUSREQ,
	  		  oHLOCK,
			  oHWRITE;
  output [1:0] 		  oHTRANS;
  output [2:0] 		  oHSIZE,
			  oHBURST;
  output [3:0] 		  oHPROT;
  output [31:0] 	  oHADDR;
  output [31:0] 	  oHWDATA;
  //
  input 		  HGRANT,
			  HREADY,
			  HRESETn,
			  HCLK;
  input [1:0] 		  HRESP;
  input [31:0] 		  HRDATA;
  input 		  HINTERRUPT;
  
  
`protect
  // carbon license crbn_vsp_exec_xtor_ahb

  //-//////////////////////////////////////////////////////////////////////////
  // Global Parameters
`include "carbonx_ahb_params.vh"

// Configure the transactor interface
  parameter BFMid     =  1, // Emulator interface (-1 for software interface)
            ClockId   =  1, // ?
            DataWidth = 32, // Data width
            DataAdWid =  8, // Data memory index width, 8-bit (for gp_daddr)
            GCSwid    = 32, // config reg width, 32-bit (for get_csdata)
            PCSwid    = 32; // config reg width, 32-bit (for put_csdata)

//-/////////////////////////////////////////////////////////////////////////
// 
//
// File Name  : carbonx_ahb_master.v
// 
//
// Author     : Kiran Maheriya
// Created    : 2002/02/16
//
// Description: AMBA Bus master transactor BFM part.
//              This transactor currently supports 32-bit data bus, and hence
//              transfer sizes (as in HSIZE) of up to 32 bits per transaction.
//              This is the synthesizable part. Interface module is separate.
//
// Revisions:
// Kiran Maheriya 03/27/2003
//   Added Address generation logic. Now the interface only gives start address.
// Kiran Maheriya 04/09/2003
//   Changed the HDL interface ("Test Interface") to reflect XIF_core interface.
//   Moved logic from master_if module. Now master_if doesn't exist.

/*
Revision 1.11  2005/06/02 21:50:21  colby_w
added scemi ifdef

Revision 1.10  2004/05/24 22:40:32  fredieu
Fixed a problem with early termination due to HGRANT loss.

*/

//
//  08/29/2005 Jim Stein
//   Added c*rbon licensing features (protect endprotect)
//
//-//////////////////////////////////////////////////////////////////////////

`undef  XIF_GCS_DEFAULT_DATA
`define XIF_GCS_DEFAULT_DATA 32'h00000123

// Default values to be driven on the bus by master

  // Include the xif_core template
`include "xif_core.vh"
  

  //-//////////////////////////////////////////////////////////////////////////
  // L o c a l   p a r a m e t e r s
  //-//////////////////////////////////////////////////////////////////////////
  
  // MASTER control State machine states
  parameter 		  CTL_IDLE = 3'b00,  // Do nothing
			  CTL_REQ  = 3'b01,  // Request grant to bus
			  CTL_ACT  = 3'b10;  // Got the grant, now do the transaction
  
  // Defines for add/subtract 
  parameter 		  INCREMENT = 1, 
			  DECREMENT = 0;
  parameter 		  BYONE = 0,  // to say increment/decrement by one
			  BYTWO = 1;  // to say increment/decrement by two
  
  parameter 		  MASTER_AHB_OP = 32'h80000001;  // User Opcode
  
  
  //-//////////////////////////////////////////////////////////////////////////
  // L o c a l   S i g n a l s
  //-//////////////////////////////////////////////////////////////////////////
  reg [7:0] 		  bfmAddrIndex, bfmWDataIndex, bfmRDataIndex, gp_daddr;
  wire [7:0] 		  dataIndex;
  reg 			  bfmRDataEnable;
  wire 			  bfmDone;
  // Inputs from Interface module
  wire [31:0] 		  imAddrIn,     // AHB Address
 			  imDataIn,     // AHB WDATA; data to be written to slave
			  imConfigIn;   // Config register.
  reg [31:0] 		  imDataInAligned,
			  imWDataLanes; // intermediate wdata signals for bit-packing
  reg [31:0] 		  imRDataAligned, imRDataPackedComb,
			  imRDataPacked; // intermediate rdata signals for bit unpacking
  
  wire 			  cmdReceived;
  
  // Other Local signals
  reg 			  oHBUSREQ;
  wire 			  oHLOCK, oHWRITE;
  wire [1:0] 		  oHTRANS;
  wire [2:0] 		  oHSIZE, oHBURST;
  wire [3:0] 		  oHPROT;
  reg [31:0] 		  oHADDR, oHWDATA;
  

  // State variables
  reg [1:0] 		  ctl_cs, ctl_ns, master_cs, master_ns;

  // Flags used throughout the code. For readability as well as brevity in code.
  // All are combinatorial
  reg 			  AllBeatsCompleted;     // All the beats completed!!
  wire 			  TransferDone;          // Current beat completed
  wire 			  TransferDoneWithError; // Current beat completed, with error
  wire 			  OkayTransfer;          // Okay response from slave
  wire 			  RetryTransfer;         // Retry/Split response from slave
  wire 			  ErrorTransfer;         // Error response from slave
  reg 			  ErrorTransfer_d;       // Clock delayed ErrorTransfer. Used to report aborted burst size back to S-side
  reg 			  SplitOrRetryPending;   // Flag keeps track of pending split/retry
  reg 			  SplitOrRetryPendingComb;// Combinatorial part of above
  reg 			  earlyTerminated, earlyTerminatedComb, earlyTerminated_1; // Early termination flag (and its combinatorial part)
  
  //-//////////////////////////////////////////////////////////////////////////
  // Config reg fields
  //-//////////////////////////////////////////////////////////////////////////
  reg 			  AbortOnError;   // imConfigIn[23]
  reg [3:0] 		  BusyInsertAfter, BusyRepeatAfter; // imConfigIn[31:28] and [27:24]
  //
  reg 			  BusyEnabled;     // When one of the above two Busy* are >0, busy insertion is enabled
  reg [3:0] 		  BusyRepeatCount; // Counter to count till BusyRepeatAfter
  wire 			  BusyInsertCondition; // A flag to pinpoint cycles where BUSY is to be inserted
  
  // Signals making up the Command Word
  reg 			  cHLOCK,
			  cHWRITE;
  reg [2:0] 		  cHSIZE, cHSIZE_d;
  reg [2:0] 		  cHBURST;
  wire [3:0] 		  cHPROT;
  wire [7:0] 		  cBeats,         // Number of beats in a burst
			  cBeatsMinusOne; // cBeats-1 (or zero whichever is greater)
  reg 			  cINCR;          // 1=> INCR burst, 0=> SINGLE or WRAP burst
  reg 			  cHWRITE_d;      // delayed cHWRITE
  
  reg 			  hgrant_d;       // For oHWDATA and HRDATA, clock delayed grant
  reg 			  hgrant_d2;      // For oHWDATA and HRDATA, double clock delayed grant
  reg [1:0] 		  master_csd;     // for oHWDATA delayed HTRANS/master_cs
  
  wire 			  BurstInProgress;// Indicates that regular burst is in progress
  
  // Signals used for address increment
  reg [31:0] 		  incAddress, incAddress_d, incAddress_d2,
 			  imAddrClocked;
  reg 		          xrun;
  wire 		          reset_xrun, set_xrun;
  wire 			  validOpcode;

  // Global reset is combination of hardware and S-side resets 
  wire 		resetn   = (HRESETn==0 || XIF_reset==1) ? 0:1;

  assign        bfmDone = AllBeatsCompleted;
  
  //-//////////////////////////////////////////////////////////////////////////////
  // Some flags used throughout the code. Interpretations of HREADY and HRESP.
  //-//////////////////////////////////////////////////////////////////////////////
  // Okay response
  assign 	OkayTransfer  = (HREADY==1 && HRESP==RESP_OKAY) ? 1 : 0;
  // Retry/Split response from slave (first phase)
  assign 	RetryTransfer = (HREADY==0 && (HRESP==RESP_RETRY || HRESP==RESP_SPLIT) && hgrant_d2==1) ? 1 : 0;
  // Error response from slave (first phase)
  assign 	ErrorTransfer = (HREADY==0 && HRESP==RESP_ERROR && hgrant_d2==1) ? 1 : 0;
  // Error response from slave (second phase)
  assign 	TransferDoneWithError = (HREADY==1 && HRESP==RESP_ERROR && hgrant_d2==1) ? 1 : 0;
  // Successful transfer completion
  assign 	TransferDone = (OkayTransfer==1 && hgrant_d2==1) ? 1 : 0;

  assign 	BusyInsertCondition = (BusyEnabled == 1 &&
				       ((bfmAddrIndex==BusyInsertAfter && BusyInsertAfter != 0) ||
					(BusyRepeatCount >= BusyRepeatAfter && BusyRepeatCount != 0))) ? 1 : 0;

  //-//////////////////////////////////////////////////////////////////////////////
  // hgrant_d: Delayed HGRANT. This will be disabled only when OkayTransfer==1,
  //           making sure current transfer finishes
  // master_csd: Generated for HWDATA timing. HWDATA must be put on bus when
  //             when master_cs WAS NSEQ or SEQ in previous cycle. master_csd
  //             stores this, and keeps it until current transfer is done
  //-//////////////////////////////////////////////////////////////////////////////
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      hgrant_d      <= 0;
      hgrant_d2     <= 0;
      master_csd    <= 0;
      cHWRITE_d     <= 0;
      cHSIZE_d      <= 0;
      imAddrClocked <= 0;
      ErrorTransfer_d <= 0;
    end
    else begin
      hgrant_d2    <= hgrant_d;
      ErrorTransfer_d <= ErrorTransfer; // To report aborted bursts back to S-Side
      //if (OkayTransfer == 1) begin
      if (HREADY == 1) begin
	hgrant_d   <= HGRANT;
	master_csd <= master_cs;
	cHWRITE_d  <= cHWRITE;
	cHSIZE_d   <= cHSIZE;
	imAddrClocked <= imAddrIn;
      end
    end
  end
  
  // Configuration
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      BusyInsertAfter <= 0;
      BusyRepeatAfter <= 0;
      BusyEnabled     <= 0;
      AbortOnError    <= 0;
    end
    else begin
      // BusyInsertAfter: Insert busy cycle after this many cycles,
      // BusyRepeatAfter: Repeat the same after this many cycles 
      // (i.e., insert a busy cycle every 'BusyRepeatAfter' cycle, starting at the cycle after 'BusyInsertAfter' cycles
      BusyInsertAfter <= imConfigIn[31:28];
      BusyRepeatAfter <= imConfigIn[27:24];
      if (imConfigIn[31:28] != 0 || imConfigIn[27:24] != 0)
	BusyEnabled   <= 1;
      else
	BusyEnabled   <= 0;
      // Error response... AbortOnError=1: duh!, AbortOnError=0: continue on error
      AbortOnError <= imConfigIn[23];
    end
  end

  //-///////////////////////////////////////////////////////////////////////////////////////////////////
  // Busy insertion logic. This counter counts the cycles between busy cycles
  // i.e., once a busy cycle is inserted, it starts counting until it reaches BusyRepeatAfter; repeats
  // the count from zero again and so on...
  // The counter advances every time a valid transfer is completed.
  //-///////////////////////////////////////////////////////////////////////////////////////////////////
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn)
      BusyRepeatCount <= 0;
    else
      if (BusyRepeatCount >= BusyRepeatAfter || master_ns == TRANS_NSEQ)
	BusyRepeatCount <= 0;
      else if ((master_cs == TRANS_NSEQ || master_cs == TRANS_SEQ) && (TransferDone == 1 || TransferDoneWithError == 1))
	BusyRepeatCount <= BusyRepeatCount + 1;
      else
	BusyRepeatCount <= BusyRepeatCount;
  end

  

  //-//////////////////////////////////////////////////////////////////////////
  // The control signals: oHLOCK, oHTRANS, oHSIZE, oHBURST, oHPROT
  //-//////////////////////////////////////////////////////////////////////////
  // Drive oHTRANS based on C-side command
  assign oHLOCK  = cHLOCK;
  assign oHTRANS = master_cs;
  assign oHSIZE  = cHSIZE;
  assign oHBURST = cHBURST;
  assign oHPROT  = cHPROT;
  assign oHWRITE = cHWRITE; // Check the timing sync
  
  //-//////////////////////////////////////////////////////////////////////////
  // The addr/data signals: oHADDR, oHWDATA
  //-//////////////////////////////////////////////////////////////////////////
  always @ (TransferDoneWithError or bfmAddrIndex
	    or cBeats or hgrant_d or imAddrClocked or incAddress_d
	    or oHTRANS) begin
    if (hgrant_d==1 &&                                                 // Got grant
	(oHTRANS == TRANS_NSEQ || oHTRANS == TRANS_SEQ || oHTRANS == TRANS_BUSY || // Normal read/write states
	 (oHTRANS == TRANS_IDLE && TransferDoneWithError==1))) begin // Entered idle because of Error response
      if (cBeats == 1 || bfmAddrIndex == 1) // Single burst or first beat of bigger burst
	oHADDR <= imAddrClocked; // For single bursts use the incoming address
      else
	oHADDR <= incAddress_d;  // Otherwise use computed address
    end
    else
      oHADDR <= 0;   // Drive zeros on bus. Helps in debugging.
  end
    
  always @ (cHWRITE_d or imWDataLanes or master_csd) begin
    if (cHWRITE_d == 1 &&   // && OkayTransfer==1 && 
	(master_csd == TRANS_NSEQ || master_csd == TRANS_SEQ || master_csd == TRANS_BUSY))
      oHWDATA <= imWDataLanes;
    else   // Just to make bus look clean!
      oHWDATA <= 0;
  end
  

  //-//////////////////////////////////////////////////////////////////////////
  // bfmRDataEnable : Make sure that memory write of HRDATA occurs only during
  // valid clock periods
  //-//////////////////////////////////////////////////////////////////////////
  always @ (OkayTransfer or cHWRITE_d or master_csd) begin
    if ((master_csd==TRANS_NSEQ || master_csd==TRANS_SEQ) &&
	cHWRITE_d==0 && OkayTransfer==1)
      // If current state is a valid read transfer, assert enable.
      // HRDATA will be sampled by test side interface (XIF_core)
      // in the cycle when bfmRDataEnable is asserted.
      bfmRDataEnable <= 1;
    else  // if (OkayTransfer==1)
      // Reset it when an OKAY response and HREADY=1 condition are met
      bfmRDataEnable <= 0;
  end
  
  //-//////////////////////////////////////////////////////////////////////////
  // The bus request : oHBUSREQ
  //-//////////////////////////////////////////////////////////////////////////
  always @ (negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      oHBUSREQ <= 0;
    end
    else begin
      if ((OkayTransfer == 1 && (bfmAddrIndex == cBeatsMinusOne && (master_ns==TRANS_NSEQ || master_ns==TRANS_SEQ))) || // Penultimate Cycle
	  (ErrorTransfer == 1 && AbortOnError == 1))  // Aborting on error
	   oHBUSREQ <= 0;
      else if (cmdReceived == 1 ||       // New request from test side
	       ctl_ns == CTL_REQ ||      // Still in request phase
	       ctl_ns == CTL_ACT ||      // Continue reqest till transfer goes on
	       ctl_cs == CTL_ACT)        // same. This may redundant, though.
	oHBUSREQ <= 1;
      else
	oHBUSREQ <= 0;
    end
  end
  
  //-//////////////////////////////////////////////////////////////////////////
  // Control State machine  State assignment - next state logic
  //-//////////////////////////////////////////////////////////////////////////
  always @ (AllBeatsCompleted or HGRANT or RetryTransfer
	    or SplitOrRetryPending or cmdReceived or ctl_cs) begin
    case (ctl_cs) 
      CTL_IDLE: begin
	if (HGRANT == 1 && (cmdReceived == 1 || SplitOrRetryPending == 1)) begin
	  ctl_ns = CTL_ACT;
	end
	else if (cmdReceived == 1 && HGRANT == 0) begin
	  ctl_ns = CTL_REQ;
	end
 	else begin
	  ctl_ns = CTL_IDLE;
	end
      end
      CTL_REQ: begin
	if (HGRANT == 1) begin
	  ctl_ns = CTL_ACT;
	end
	else begin
	  ctl_ns = CTL_REQ;
	end
      end
      CTL_ACT: begin
	if (AllBeatsCompleted == 1 && cmdReceived == 0) begin
	  ctl_ns = CTL_IDLE;
	end
	else if ((AllBeatsCompleted == 1  && cmdReceived == 1 && HGRANT == 0) ||
		 (RetryTransfer == 1)) begin
	  ctl_ns = CTL_REQ;
	end
	else
	  ctl_ns = CTL_ACT;
      end
      default: begin
	ctl_ns = CTL_IDLE;
      end
    endcase // case(ctl_cs)
  end
  // State sync for ctl state
  always @ (negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      ctl_cs <= CTL_IDLE;
    end
    else begin
      ctl_cs <= ctl_ns;
    end
  end
  //-//////////////////////////////////////////////////////////////////////////
  // Master state machine (for bus transaction)
  //-//////////////////////////////////////////////////////////////////////////
  always @ (BusyInsertCondition or ErrorTransfer
	    or HGRANT or HREADY or OkayTransfer or RetryTransfer
	    or SplitOrRetryPending or TransferDoneWithError
	    or bfmAddrIndex or cBeats or cHBURST or cINCR
	    or cmdReceived or ctl_ns or earlyTerminated or hgrant_d
	    or master_cs or oHBUSREQ) begin
    master_ns = master_cs; // Default condition.
    case (master_cs) 
      TRANS_IDLE: begin
        if (ctl_ns==CTL_ACT && HGRANT==1 && HREADY==1) begin   // Control in act state and grant available
          if (oHBUSREQ==1 &&                   // longer burst, request already raised and ...
	      (bfmAddrIndex==0 || SplitOrRetryPending==1 || earlyTerminated==1)) // first cycle of the burst, or reconstructed split/retry burst
	    master_ns = TRANS_NSEQ;
	  else if (TransferDoneWithError == 1)
	    // This is essentially for error response (after one required IDLE is inserted). 
	    // If error was on last cycle of the burst, we will automatically, correctly go to IDLE
	    // as ctl_ns won't be in CTL_ACT in that case.
	    master_ns = TRANS_SEQ;
	end
	else
	  master_ns = TRANS_IDLE;
      end

      TRANS_NSEQ: begin
	if (hgrant_d==1 && HREADY==0)
	  master_ns = TRANS_NSEQ; // Stay here it seems slave is not ready.
	//-//////////////////////////////////////////////////////////////////////////
	// Handle burst completion of single transfers and 
	// two cycle responses (SPLIT/RETRY/ERROR)
	else if (bfmAddrIndex == cBeats && OkayTransfer == 1 || // Handle single transfer burst
		 RetryTransfer == 1 || ErrorTransfer == 1)      // Handle retry/split and error response
	  master_ns = TRANS_IDLE;
	//-//////////////////////////////////////////////////////////////////////////
	// Burst Continuation (grant available, and it's not a single beat burst
	else if (HGRANT == 1 && HREADY == 1 && cBeats > 1) begin
	  //-////////////////////////////////////////////////////////////////////////
	  // Busy Insertion
	  //    For SINGLE bursts, busy cannot be inserted.
	  if (BusyInsertCondition == 1 && cHBURST != BURST_SINGLE)
	    master_ns = TRANS_BUSY;
	  //-////////////////////////////////////////////////////////////////////////
	  // Rebuild burst after SPLIT/Retry
	  //  For wrapping bursts, rebuild remaining beats as SINGLE bursts
	  else if (SplitOrRetryPending == 1 && cINCR==0)
	    master_ns = TRANS_NSEQ;
	  //-////////////////////////////////////////////////////////////////////////
	  // Continue the burst. SEQ cycles.
	  //    This also works for rebuilding of burst after SPLIT/RETRY (cHBURST=BURST_INCR)
	  // This handles both standard (4/8/16 beats) as well as nonstandard bursts
	  // The cHBURST changes according to the situation. See cHBURST logic.
	  else
	    master_ns = TRANS_SEQ;
	end
	else // Default, unknown condition
	  master_ns = TRANS_IDLE;
      end

      TRANS_SEQ: begin
	//-//////////////////////////////////////////////////////////////////////////
	// Handle retry/split and error two cycle response by inserting IDLE
	if (RetryTransfer == 1 || ErrorTransfer == 1 || HGRANT == 0)
	  master_ns = TRANS_IDLE;
	//-//////////////////////////////////////////////////////////////////////////
	// Busy insertion
	else if (BusyInsertCondition == 1)
	  // Don't insert BUSY if burst is completed and is NOT if BURST_INCR type
	  if (bfmAddrIndex == cBeats && OkayTransfer == 1 && cHBURST != BURST_INCR)
	    master_ns = TRANS_IDLE;
	  else  // insert BUSY cycle
	    master_ns = TRANS_BUSY;
	//-//////////////////////////////////////////////////////////////////////////
	// Burst completion
	// Back to Back pipeline is NOT supported yet, so going to idle is fine.
	else if (bfmAddrIndex == cBeats && OkayTransfer == 1)
	  master_ns = TRANS_IDLE;
	//-//////////////////////////////////////////////////////////////////////////
	// Burst continuation
	else
	  master_ns = TRANS_SEQ;
      end
      
      TRANS_BUSY: begin
	if (ctl_ns == CTL_ACT && cmdReceived == 1)
	  master_ns = TRANS_NSEQ;
	else if (ctl_ns == CTL_ACT && cmdReceived == 0)
	  master_ns = TRANS_SEQ;
	else
	  master_ns = TRANS_IDLE;
      end

      default: begin
	master_ns = TRANS_IDLE;  // Shouldn't be here in normal circumstances
      end
    endcase // case(master_cs)
  end
  // State sync for master state
  always @ (negedge resetn or posedge HCLK) begin
    if (!resetn)
      master_cs <= TRANS_IDLE;
    else
      master_cs <= master_ns;
  end

  //-//////////////////////////////////////////////////////////////////////////////
  // Track pending RETRY/SPLIT
  //-//////////////////////////////////////////////////////////////////////////////
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn) SplitOrRetryPending <= 0;
    else         SplitOrRetryPending <= SplitOrRetryPendingComb;
  end
  always @ (AllBeatsCompleted or RetryTransfer
	    or SplitOrRetryPending or hgrant_d2 or master_csd) begin
    if (RetryTransfer==1 && hgrant_d2==1 && master_csd!=TRANS_IDLE)
      SplitOrRetryPendingComb = 1;
    else if (AllBeatsCompleted == 1)
      // Reset pending split/retry set earlier.
      // master_cs in TRANS_NSEQ means we got the grant AGAIN ... not using that logic right now
      // SplitOrRetryPending has to remain high until burst is completed
      SplitOrRetryPendingComb = 0;
    else
      SplitOrRetryPendingComb = SplitOrRetryPending;
  end
  
  //-//////////////////////////////////////////////////////////////////////////////
  // Encode burst type (this is priority encoded logic)
  //-//////////////////////////////////////////////////////////////////////////////
  always @(RetryTransfer or SplitOrRetryPending or cBeats or cINCR) begin
    // First handle re-building of burst for Retry/Split.
    if (RetryTransfer == 1 || SplitOrRetryPending == 1) begin
      if (cINCR==1) // Switch to INCR for rest of the burst
	cHBURST = BURST_INCR;
      else
	cHBURST = BURST_SINGLE;
    end
    //-////////////////////////////////////////////////////////////////////////////
    // INCR type bursts
    else if (cINCR==1) begin
      if      (cBeats==4)  cHBURST = BURST_INCR4;
      else if (cBeats==8)  cHBURST = BURST_INCR8;
      else if (cBeats==16) cHBURST = BURST_INCR16;
      else                 cHBURST = BURST_INCR; // Single as well as non-standard bursts
    end
    //-////////////////////////////////////////////////////////////////////////////
    // WRAP type bursts
    else begin
      if      (cBeats==4)  cHBURST = BURST_WRAP4;
      else if (cBeats==8)  cHBURST = BURST_WRAP8;
      else if (cBeats==16) cHBURST = BURST_WRAP16;
      //-////////////////////////////////////////////////////////////////////////////
      // Non standard wrapping bursts are NOT supported. Defaulting to BURST_SINGLE
      // may just 
      else                 cHBURST = BURST_SINGLE; // Single as well as non-standard bursts
      
    end
  end

  // Flage early termination of the burst. This is used while calculating indices and address
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn) earlyTerminated <= 0;
    else         earlyTerminated <= earlyTerminatedComb;
  end
  always @(HGRANT or bfmAddrIndex or cBeats or earlyTerminated or master_cs or master_ns) begin
    // Haven't completed the burst, still in a valid state but grant was taken away :(
    if ((bfmAddrIndex < cBeats) && HGRANT==0 && (master_cs!=TRANS_IDLE))
      earlyTerminatedComb = 1;
    // Finally, got the grant, and going to finish the burst :)
    else if (master_ns==TRANS_NSEQ)
      earlyTerminatedComb = 0;
    // Hold
    else
      earlyTerminatedComb = earlyTerminated;
  end

  //-////////////////////////////////////////////////////////////////////////////
  // Beats accounting. Keep track of current beat number.
  // bfmAddrIndex  => Used for address memory arrays
  // bfmWAddrIndex =>      for WDATA  --- it's basically bfmAddrIndex - 1, starting at zero
  // bfmRDataIndex =>      for RDATA  --- it's basically bfmAddrIndex - 2, starting at zero
  // The latter two provide pipelined timing required for oHWDATA and HRDATA buses.
  //-////////////////////////////////////////////////////////////////////////////
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      bfmAddrIndex  <= 0;
      bfmWDataIndex <= 0;
      bfmRDataIndex <= 0;
    end
    else begin
      // Reset indices
      if ((AbortOnError==1 && ErrorTransfer==1) ||    // If there is an Error response, and AbortOnError is asserted, OR
	  (HREADY==1 && master_ns==TRANS_IDLE &&   // We are going to IDLE
	   (earlyTerminated==0 && earlyTerminatedComb==0) && // Burst in not early terminated
	   SplitOrRetryPendingComb==0 && RetryTransfer==0 && ErrorTransfer==0)) begin // no split/retry pending and not an error resp
	bfmAddrIndex  <= 0;
	bfmWDataIndex <= bfmAddrIndex;
	bfmRDataIndex <= bfmWDataIndex;
      end
      // Increment indices
      else if (HREADY == 1 &&                          // ready
	       (master_cs != TRANS_IDLE || master_ns == TRANS_NSEQ) && HRESP!=RESP_ERROR) begin // Valid states
	if (hgrant_d == 1)
	  bfmRDataIndex <= bfmWDataIndex;
	if (HGRANT == 1 && master_ns != TRANS_BUSY)  begin            // Not going to be in BUSY state
          //cHBURST != BURST_SINGLE) begin  // Not a single transfer (in which case, no sense in incrementing)
	  bfmAddrIndex   <= bfmAddrIndex + 1;
	  bfmWDataIndex  <= bfmAddrIndex;
	end
      end
      // Decrement indices by two when there is a retry response
      else if (RetryTransfer == 1) begin
	if (master_cs == TRANS_BUSY) begin
	  bfmAddrIndex  <= bfmWDataIndex;
	  bfmWDataIndex <= bfmAddrIndex;
	  bfmRDataIndex <= bfmWDataIndex;
	end
	else begin
	  bfmAddrIndex <= bfmRDataIndex;
	  // Now take the indices back 2 times. This makes address to fall back to the one before retry
	  if (bfmWDataIndex > 2) bfmWDataIndex <= bfmWDataIndex - 2;
	  else                   bfmWDataIndex <= 0;
	  if (bfmRDataIndex > 2) bfmRDataIndex <= bfmRDataIndex - 2;
	  else                   bfmRDataIndex <= 0;
	end
      end
    end // else: !if(!resetn)
  end // always @ (negedge resetn or posedge HCLK)

  //-/////////////////////////////////////////////////////////////////////////////
  // Address generation
  //-/////////////////////////////////////////////////////////////////////////////
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      incAddress    <= 0;
      incAddress_d  <= 0;
      incAddress_d2 <= 0;
    end
    else begin
      if ((AbortOnError==1 && ErrorTransfer==1) ||    // If there is an Error response, and AbortOnError is asserted, OR
	  (master_ns==TRANS_IDLE &&   // We are going to IDLE
	   (earlyTerminated==0 && earlyTerminatedComb==0) && // Burst in not early terminated
	   SplitOrRetryPendingComb==0 && RetryTransfer==0 && ErrorTransfer==0)) begin // no split/retry pending and not an error resp
	incAddress    <= imAddrIn;
	incAddress_d  <= incAddress;
	incAddress_d2 <= incAddress_d;
      end
      else if (cmdReceived == 1 && master_ns == TRANS_NSEQ) begin  // New command received, and already the next cycle is NSEQ
	incAddress <= incrDecrAddress(INCREMENT, BYONE, imAddrIn, cBeats, cHSIZE, cINCR);
	incAddress_d <= imAddrIn;
	incAddress_d2 <= incAddress_d;
      end
      else if (HGRANT == 1 && HREADY == 1 &&                           // Bus is granted and ready
               (master_cs != TRANS_IDLE || master_ns == TRANS_NSEQ) && HRESP!=RESP_ERROR) begin // Valid states
	incAddress_d2 <= incAddress_d;
	if (master_ns != TRANS_BUSY)  begin                        // Not going to be in BUSY state
	  incAddress    <= incrDecrAddress(INCREMENT, BYONE, incAddress, cBeats, cHSIZE, cINCR);
	  incAddress_d  <= incAddress;
	end
      end
      else if (RetryTransfer == 1) begin // decrement indices by two when there is a retry response
	if (master_cs == TRANS_BUSY) begin
	  incAddress    <= incAddress_d;
	  incAddress_d  <= incAddress;
	  incAddress_d2 <= incAddress_d;
	end
	else begin
	  incAddress <= incAddress_d2;
	  // Now take the indices back 2 times. This makes address to fall back to the one before retry
	  if (bfmWDataIndex > 2) begin
	    incAddress_d  <= incrDecrAddress(DECREMENT, BYTWO, incAddress_d, cBeats, cHSIZE, cINCR);
	  end
	  else begin
	    incAddress_d <= imAddrIn;
	  end
	  if (bfmRDataIndex > 2) begin
	    incAddress_d2 <= incrDecrAddress(DECREMENT, BYTWO, incAddress_d2, cBeats, cHSIZE, cINCR);
	  end
	  else begin
	    incAddress_d2 <= imAddrIn;
	  end
	end
      end
    end // else: !if(!resetn)
  end // always @ (negedge resetn or posedge HCLK)

  
  //-/////////////////////////////////////////////////////////////////////////////////
  // Flag when all the beats (transfers) are completed
  //-/////////////////////////////////////////////////////////////////////////////////
  always @(AbortOnError or ErrorTransfer or OkayTransfer
	   or bfmRDataIndex or cBeatsMinusOne or master_csd) begin
    //-/////////////////////////////////////////////////////////////////////////////
    // If OkayTransfer (irrespective of HGRANT), and all beats done, and in NSEQ/SEQ,
    //    flag completion of transaction.
    // In case of ErrorTransfer with AbortOnError=1, flag completion immediately
    if ((ErrorTransfer == 1 && AbortOnError == 1) ||
	(bfmRDataIndex == cBeatsMinusOne && OkayTransfer == 1 && (master_csd == TRANS_NSEQ || master_csd == TRANS_SEQ)))
      AllBeatsCompleted = 1;
    else
      AllBeatsCompleted = 0;
  end

  //-//////////////////////////////////////////////////////////////////////////////
  // Stage-1 of bit Unpacking from AHB RDATA lanes based on 8/16/32-bit mode.
  // Unpacking is done in two stages. Below is stage one where a lane is extracted
  // from RDATA into imRDataAligned. In stage two, imRDataAligned will be shifted
  // to pack final data for XIF_core.
  //-//////////////////////////////////////////////////////////////////////////////
  always @(HRDATA or cHSIZE_d or incAddress_d2) begin
    imRDataAligned = 0;
    case (cHSIZE_d)
      SIZE_8: begin
	case (incAddress_d2[1:0])  // This has valid addr during write
`ifdef BIG_ENDIAN     // LSB of incoming data becomes MSB on AHB
	  2'b00:   imRDataAligned[7:0] = HRDATA[31:24];
	  2'b01:   imRDataAligned[7:0] = HRDATA[23:16];
	  2'b10:   imRDataAligned[7:0] = HRDATA[15:08];
	  2'b11:   imRDataAligned[7:0] = HRDATA[07:00];
	  default: imRDataAligned[7:0] = HRDATA[31:24];
`else                 // LSB of incoming data remains LSB on AHB
	  2'b00:   imRDataAligned[7:0] = HRDATA[07:00];
	  2'b01:   imRDataAligned[7:0] = HRDATA[15:08];
	  2'b10:   imRDataAligned[7:0] = HRDATA[23:16];
	  2'b11:   imRDataAligned[7:0] = HRDATA[31:24];
	  default: imRDataAligned[7:0] = HRDATA[07:00];
`endif
	endcase
      end
      SIZE_16: begin
	case (incAddress_d2[1])
`ifdef BIG_ENDIAN    // LSB/MSB sense reversed
	  1'b0:    imRDataAligned[15:00] = HRDATA[31:16];
	  1'b1:    imRDataAligned[15:00] = HRDATA[15:00];
	  default: imRDataAligned[15:00] = HRDATA[31:16];
`else                // LSB/MSB sense not changed
	  1'b0:    imRDataAligned[15:00] = HRDATA[15:00];
	  1'b1:    imRDataAligned[15:00] = HRDATA[31:16];
	  default: imRDataAligned[15:00] = HRDATA[15:00];
`endif
	endcase
      end
      default: begin
	imRDataAligned[31:00] = HRDATA[31:00];
      end
    endcase
  end

  //-/////////////////////////////////////////////////////////////////////////////
  // Stage-2 of bit unpacking from RDATA.
  // Shift LSB aligned lanes (imRDataAligned) to create packed final output data
  // imRDataPacked for S-Side.
  //-/////////////////////////////////////////////////////////////////////////////
  always @(bfmRDataIndex or cHSIZE_d or imRDataAligned) begin
    imRDataPackedComb = 0;
    case (cHSIZE_d)
      SIZE_8: begin
	case (bfmRDataIndex[1:0])
	  2'b00:   imRDataPackedComb[07:00] = imRDataAligned[7:0];
	  2'b01:   imRDataPackedComb[15:08] = imRDataAligned[7:0];
	  2'b10:   imRDataPackedComb[23:16] = imRDataAligned[7:0];
	  2'b11:   imRDataPackedComb[31:24] = imRDataAligned[7:0];
	  default: imRDataPackedComb[07:00] = imRDataAligned[7:0];
	endcase
      end
      SIZE_16: begin
	case (bfmRDataIndex[0])
	  1'b0:    imRDataPackedComb[15:00] = imRDataAligned[15:00];
	  1'b1:    imRDataPackedComb[31:16] = imRDataAligned[15:00];
	  default: imRDataPackedComb[15:00] = imRDataAligned[15:00];
	endcase
      end
      default: begin // 32-bit or others
	imRDataPackedComb[31:00] = imRDataAligned[31:00];
      end
    endcase
  end
  //-/////////////////////////////////////////////////////////////////////////////
  // Stage-2 of bit unpacking continued. Clock the shifted data to pack it.
  // Problem with this is that the last transfer will not be clocked in time for
  // it to be sampled by XIF_core. As a result, the data sent to the XIF_core
  // should be imRDataPacked | imRDataPackedComb.
  //-/////////////////////////////////////////////////////////////////////////////
  always @(negedge resetn or posedge HCLK) begin
    if (!resetn)
      imRDataPacked <= 0;
    else
      if (cmdReceived) // New burst
	imRDataPacked <= 0;
      else if (OkayTransfer &&  (master_csd == TRANS_NSEQ || master_csd == TRANS_SEQ)) begin
        if ((cHSIZE_d==SIZE_8  && bfmRDataIndex[1:0]==2'b11) || // 8-bit; Last byte of word
	    (cHSIZE_d==SIZE_16 && bfmRDataIndex[0]==1)       || // 16-bit; Second half-word of word
	    (cHSIZE_d==SIZE_32))                                // 32-bit; imRDataPacked not used
          imRDataPacked <= 0;
        else
	  // Rest of the bytes (when SIZE_8) or second half-word (when SIZE_16). Combine.
	  imRDataPacked <= (imRDataPacked | imRDataPackedComb);
      end
  end

  //-//////////////////////////////////////////////////////////////////////////////
  // Bit Packing for in AHB WDATA lanes based on 8/16/32-bit mode
  // imWDataLanes is the final wdata that goes on the bus.
  //-//////////////////////////////////////////////////////////////////////////////
  always @(cHSIZE_d or imDataInAligned or incAddress_d2) begin
    imWDataLanes = 0;
    case (cHSIZE_d)
      SIZE_8: begin
	case (incAddress_d2[1:0])  // This has valid addr during write
`ifdef BIG_ENDIAN     // LSB of incoming data becomes MSB on AHB
	  2'b00:   imWDataLanes[31:24] = imDataInAligned[7:0];
	  2'b01:   imWDataLanes[23:16] = imDataInAligned[7:0];
	  2'b10:   imWDataLanes[15:08] = imDataInAligned[7:0];
	  2'b11:   imWDataLanes[07:00] = imDataInAligned[7:0];
	  default: imWDataLanes[31:24] = imDataInAligned[7:0];
`else                 // LSB of incoming data remains LSB on AHB
	  2'b00:   imWDataLanes[07:00] = imDataInAligned[7:0];
	  2'b01:   imWDataLanes[15:08] = imDataInAligned[7:0];
	  2'b10:   imWDataLanes[23:16] = imDataInAligned[7:0];
	  2'b11:   imWDataLanes[31:24] = imDataInAligned[7:0];
	  default: imWDataLanes[07:00] = imDataInAligned[7:0];
`endif
	endcase
      end
      SIZE_16: begin
	case (incAddress_d2[1])
`ifdef BIG_ENDIAN    // LSB/MSB sense reversed
	  1'b0:    imWDataLanes[31:16] = imDataInAligned[15:00];
	  1'b1:    imWDataLanes[15:00] = imDataInAligned[15:00];
	  default: imWDataLanes[31:24] = imDataInAligned[15:00];
`else                // LSB/MSB sense not changed
	  1'b0:    imWDataLanes[15:00] = imDataInAligned[15:00];
	  1'b1:    imWDataLanes[31:16] = imDataInAligned[15:00];
	  default: imWDataLanes[15:00] = imDataInAligned[15:00];
`endif
	endcase
      end
      default: begin
	imWDataLanes[31:00] = imDataInAligned[31:00];
      end
    endcase
  end
  
  //-//////////////////////////////////////////////////////////////////////////////
  // Shift and align the incoming data from XIF_core so that the bit packing logic
  // can handle trailing bytes. The times when trailing bytes need to be handled
  // are when 1, 2 or 3 beat 8-bit transfers or a single 16-bit transfer are made.
  // This happens when 8-bit or 16-bit burst lengths (beats) are not 32-bit word
  // aligned. The logic below will enable bit-packing logic to handle this by
  // always feeding it the next available data. Note that flip-flips are not used.
  // The timing of XIF_core and AHB doesn't allow us that. This may create
  // long combinatorial path combined with bit-packing.
  // XIF_core always packs data starting from LSB. This is the basis of this logic.
  //-//////////////////////////////////////////////////////////////////////////////
  always @(bfmRDataIndex or cHSIZE_d or imDataIn) begin
    imDataInAligned = 0;
    case (cHSIZE_d)
      SIZE_8: begin
	case (bfmRDataIndex[1:0])
	  // RDataIndex is used because it is valid DURING the write. WDataIndex
	  // would be valid BEFORE the write cycle
	  2'b00:   imDataInAligned[7:0] = imDataIn[07:00];
	  2'b01:   imDataInAligned[7:0] = imDataIn[15:08];
	  2'b10:   imDataInAligned[7:0] = imDataIn[23:16];
	  2'b11:   imDataInAligned[7:0] = imDataIn[31:24];
	  default: imDataInAligned[7:0] = imDataIn[07:00];
        endcase
      end
      SIZE_16: begin
	case (bfmRDataIndex[0])
	  1'b0:    imDataInAligned[15:00] = imDataIn[15:00];
	  1'b1:    imDataInAligned[15:00] = imDataIn[31:16];
	  default: imDataInAligned[15:00] = imDataIn[15:00];
	endcase
      end
      default: begin
	imDataInAligned[31:00] = imDataIn[31:00];
      end
    endcase
  end

  //-//////////////////////////////////////////////////////////////////////////////
  //
  //                               XIF_core interface
  //
  //-//////////////////////////////////////////////////////////////////////////////



  //-//////////////////////////////////////////////////////////////////////////////
  //                                                            8 7 6 5 4 3 2 1 0
  //                                            ---------------+-+-+-----+-------+
  //  config reg                                               |I|L| size|  hprot|
  //                                            ---------------+-+-+-----+-------+
  assign imConfigIn = XIF_get_csdata[31:0]; // Config register
  // Incoming start address and data
  assign imDataIn   = XIF_get_data;
  assign imAddrIn   = XIF_get_address[31:0];

  assign validOpcode = (XIF_get_operation==BFM_READ ||
			XIF_get_operation==BFM_WRITE ||
			XIF_get_operation==MASTER_AHB_OP) ? 1 : 0;
  
  // Signal command/transaction received
  assign cmdReceived  = (XIF_xav & validOpcode) ? 1 : 0;
  

  // Drive XIF_core inputs
  assign BFM_reset         = ~HRESETn;
  assign BFM_clock         = HCLK;
  assign BFM_xrun          = xrun;
  assign BFM_interrupt     = HINTERRUPT;          // Combine this with RESP_ERROR condition or others
  //
  assign BFM_put_operation = BFM_NXTREQ;          // NA
  assign BFM_put_address   = 0;                   // NA
  // Number of bytes being sent
  // This could have been simple bfmRDataIndex<<cHSIZE_d, but because of lots of corner cases,
  // it is safer to send cBeats back for normal transfers. Former is sent for aborted bursts.
  assign BFM_put_size      = (ErrorTransfer_d) ? (bfmRDataIndex<<cHSIZE_d) : (cBeats<<cHSIZE_d);
  assign BFM_put_status    = 0;                   // NA
  assign BFM_put_data      = (imRDataPacked | imRDataPackedComb);// Data from BFM to S-Side (HRDATA, packed)
  assign BFM_put_csdata    = 0;
  assign BFM_put_cphwtosw  = 0;                   // NA
  assign BFM_put_cpswtohw  = 0;                   // NA
  assign BFM_gp_daddr      = gp_daddr;            // index into XIF_core memory
  assign BFM_put_dwe       = bfmRDataEnable & ~earlyTerminated_1;      // Write enable for XIF_core memory (RDATA valid)

  // The HREADY is added actually to avoid putting next data during wait cycles. XIF_core
  // doesn't have enable for get_data, hence the need for work around.
  // For get data, during HREADY==0 we use previous address index (which is in RDataIndex) and fetch
  // HWDATA (get_data) for corresponding address only when HREADY==0.
  assign dataIndex = (cHWRITE==0 || HREADY==0) ? bfmRDataIndex : bfmWDataIndex; // WDATA or RDATA index for mem in XIF_Core
  // Handle different sizes. 8, 16 and 32 are supported.
  always @ (cHSIZE or dataIndex) begin
    case (cHSIZE)
      SIZE_8:  gp_daddr = dataIndex >> 2; // For 8-bit access, discard last 2 bits
      SIZE_16: gp_daddr = dataIndex >> 1; // For 16-bit access, discard last one bit.
      default: gp_daddr = dataIndex;      // For 32-bit and other sizes, use as it is.
    endcase
  end
  
  //-/////////////////////////////////////////////////////////////////////////////
  // xrun. 
  //-/////////////////////////////////////////////////////////////////////////////
  always @ (negedge resetn or posedge HCLK) begin
    if (!resetn) begin
      xrun <= 0;
      earlyTerminated_1 <= 0;
    end
    else begin
      if (cmdReceived)  // Always need minimum of two clocks
        xrun <= 1;
      else if (bfmDone)
        xrun <= 0;
      earlyTerminated_1 <= earlyTerminated;
    end
  end

  //-//////////////////////////////////////////////////////////////////////////
  // When UserOp (MASTER_AHB_OP) is received, cHSIZE, cHLOCK, cINCR and cHWRITE
  // are sent in status work.
  // For BFM_WRITE and BFM_READ, config reg has to be used. This is an indirect
  // method, but at least allows the BFM to work with BFM_READ/WRITE ops. 
  // For non standard bursts (cBeats != 4|8|16), HBURST=BURST_INCR is automatically
  // selected irrespective of cINCR.
  //-//////////////////////////////////////////////////////////////////////////
  always @(XIF_get_csdata or XIF_get_operation or XIF_get_status or XIF_get_size) begin
    case (XIF_get_operation)
      MASTER_AHB_OP: begin  // UserOp; Config comes with status word
	cHSIZE          = XIF_get_status[5:3];
	cHLOCK          = XIF_get_status[2];
	cINCR           = XIF_get_status[1];
	cHWRITE         = XIF_get_status[0];
      end
      default: begin       // Other ops; Take from config register.
        if (XIF_get_size == 1 || XIF_get_size == 3)
          cHSIZE = 0; // Force 8-bit transfer mode
        else if (XIF_get_size == 2 && XIF_get_csdata[6:4]>1)
          cHSIZE = 1; // Force 16-bit transfer mode
        else
	  cHSIZE          = XIF_get_csdata[6:4];
	cHLOCK          = XIF_get_csdata[7];
	cINCR           = XIF_get_csdata[8]; 
	cHWRITE         = (XIF_get_operation==BFM_WRITE) ? 1 : 0;
      end
    endcase
  end
  // HPROT is always taken from config reg
  assign cHPROT          = XIF_get_csdata[3:0];
  
  // Calculate beats based on XIF_get_size and hsize
  // This works because cHSIZE=0,1&2 for 8-,16- & 32-bit width
  assign cBeats = (XIF_get_size >> cHSIZE);

  // some convenience signals
  assign cBeatsMinusOne  = (cBeats>1) ? (cBeats-1) : 0;
  assign BurstInProgress = (cBeats==4 || cBeats==8 || cBeats==16) ? 1:0; // Indicate that a standard burst is in progress.

  


  
  //-/////////////////////////////////////////////////////////////////////////////
  // Function to increment the address based on WRAP/INCR
  //-/////////////////////////////////////////////////////////////////////////////
  function [31:0] incrDecrAddress;
    input        incdec;  // 1: increment, 0: decrement
    input        cnt;       // 0: incr/decr by 1, 1: incr/decr by 2  
    
    input [31:0] address;   // address to be incremented
    input [4:0]  beats;     // number of beats in the burst
    input [2:0]  hsize;     // size (width) of data
    input        incr;      // INCR or WRAP type of burst

    reg [31:0]   bitmask;   // beats to be masked for WRAPping burst
    begin
      bitmask = getBitMask(beats, hsize);
      case (incr)
	1'b0: begin  // Wrapping address
	  case (hsize) 
	    SIZE_8:  incrDecrAddress = (address & ~bitmask) | 
				       (((incdec==INCREMENT) ? address+(1<<cnt) : address-(1<<cnt)) & bitmask);
	    SIZE_16: incrDecrAddress = (address & ~bitmask) |
				       (((incdec==INCREMENT) ? address+(2<<cnt) : address-(2<<cnt)) & bitmask);
	    SIZE_32: incrDecrAddress = (address & ~bitmask) |
				       (((incdec==INCREMENT) ? address+(4<<cnt) : address-(4<<cnt)) & bitmask);
	    default: incrDecrAddress = ((incdec==INCREMENT) ? address+(1<<cnt) : address-(1<<cnt));
	  endcase // case(hsize)
	end
	1'b1: begin  // Incrementing address
	  case (hsize) 
	    SIZE_8:  incrDecrAddress = (incdec==INCREMENT) ? address+(1<<cnt) : address-(1<<cnt);
	    SIZE_16: incrDecrAddress = (incdec==INCREMENT) ? address+(2<<cnt) : address-(2<<cnt);
	    SIZE_32: incrDecrAddress = (incdec==INCREMENT) ? address+(4<<cnt) : address-(4<<cnt);
	    default: incrDecrAddress = (incdec==INCREMENT) ? address+(1<<cnt) : address-(1<<cnt);
	  endcase // case(hsize)
	end
	default:     incrDecrAddress = (incdec==INCREMENT) ? address+(1<<cnt) : address-(1<<cnt);
      endcase // case(incr)
    end
  endfunction // incrDecrAddress
  

  //-/////////////////////////////////////////////////////////////////////////////
  // getBitMask: mask for generating wrapping address = beats*size/8 - 1
  //-/////////////////////////////////////////////////////////////////////////////
  function [31:0] getBitMask;
    input [4:0] beats;
    input [2:0] hsize;
    begin
      case (hsize) 
	SIZE_8: begin
	  getBitMask = beats - 1;
	end
	SIZE_16: begin
	  getBitMask = (beats << 1) - 1;
	end
	SIZE_32: begin
	  getBitMask = (beats << 2) - 1;
	end
	default: begin
	  getBitMask = 32'hffffffff;
	end
      endcase // case(hsize)
    end
  endfunction // getBitMask
  
`endprotect
endmodule // ahb_master_bfm
`undef  XIF_GCS_DEFAULT_DATA

// EOF

