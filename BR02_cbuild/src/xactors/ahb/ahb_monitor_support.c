//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*****************************************************************************
 * Module Name: monitor_support.c
 *
 * Module Description:
 *
 * Support routines for AHB Monitor xtor.
 *
 * Author  : Kiran Maheriya
 *
 * Created : 09/01/2002
 *
 * Revisions:
 *
 * $Id$
 *****************************************************************************
 */

#include "tbp.h"
#include "math.h"
#include "carbonXAhbMonitor.h"

////////////////////////////////////////////////////////////////////////////////
// Static global structure for monitor registers
////////////////////////////////////////////////////////////////////////////////
static carbonXAhbMonRegs_T gMonRegs;


////////////////////////////////////////////////////////////////////////////////
// Return a pointer to the global monitor registers structure
////////////////////////////////////////////////////////////////////////////////
carbonXAhbMonRegs_T * getMonRegs(void) {
  return &gMonRegs;
}

////////////////////////////////////////////////////////////////////////////////
// Read monitor registers. The global structure carbonXAhbMonRegs_T is filled up.
////////////////////////////////////////////////////////////////////////////////
carbonXAhbMonRegs_T * carbonXAhbreadMonCounters(void) {
  u_int32 *regptr;
  int i;

  // Make regptr an array pointer (instead of struct pointer), to ease access
  regptr = (u_int32 *) getMonRegs();

  for (i=0; i<NUM_MONREGS; i++) {
    regptr[i] = carbonXRead32(i);
  }
  return (carbonXAhbMonRegs_T *) regptr;
}

////////////////////////////////////////////////////////////////////////////////
// Send reset command to the monitor.
////////////////////////////////////////////////////////////////////////////////
void carbonXAhbresetMonCounters(void) {
  carbonXCsWrite(MONITOR_CFG_REG, 0);        // Enable normal operation
  carbonXCsWrite(MONITOR_CFG_REG, 1);        // Reset counters (synchronous)
  carbonXIdle(1);                            // Let a clock pass.
  carbonXCsWrite(MONITOR_CFG_REG, 0);        // Enable normal operation
  carbonXIdle(5);                            // Let a clock pass.
}

////////////////////////////////////////////////////////////////////////////////
// Print the current values of the Monitor registers. Doesn't read from DUT
////////////////////////////////////////////////////////////////////////////////
void carbonXAhbprintMonCounters(carbonXAhbMonRegs_T *monRegs) {
  int i;

  MSG_Printf("=========================================================================\n");
  MSG_Printf("#                ### Monitor Counters Status Dump ###                   #\n");
  MSG_Printf("=========================================================================\n");
  MSG_Printf("\n");
  MSG_Printf("\tTotal Number of  Read Transactions : %5ld\n", monRegs->globalReadCounter);
  MSG_Printf("\tTotal Number of Write Transactions : %5ld\n", monRegs->globalWriteCounter);
  for (i=0; i<NUM_MASTERS; i++) {
    MSG_Printf("\tMaster #%d  Read Transactions   : %5ld\n", i, monRegs->readCounterMaster[i]);
    MSG_Printf("\tMaster #%d Write Transactions   : %5ld\n", i, monRegs->writeCounterMaster[i]);
  }
  for (i=0; i<NUM_SLAVES; i++) {
    MSG_Printf("\tSlave  #%d  Read Transactions   : %5ld\n", i, monRegs->readCounterSlave[i]);
    MSG_Printf("\tSlave  #%d Write Transactions   : %5ld\n", i, monRegs->writeCounterSlave[i]);
  }
  MSG_Printf("\n");
  MSG_Printf(" Special Conditions Status\n");
  MSG_Printf("\tNumber of 'ERROR' responses : %3ld\n", monRegs->hrespErrorCounter);
  MSG_Printf("\tNumber of 'RETRY' responses : %3ld\n", monRegs->hrespRetryCounter);
  MSG_Printf("\tNumber of 'SPLIT' responses : %3ld\n", monRegs->hrespSplitCounter);
  MSG_Printf("\n");
  MSG_Printf(" Protocol Violations Report\n");
  MSG_Printf("\tHTRANS Errors       : %3ld\n", monRegs->errorHtransCounter);
  MSG_Printf("\tHGRANT Errors       : %3ld\n", monRegs->errorHgrantCounter);
  MSG_Printf("\t2-cycle Resp Errors : %3ld\n", monRegs->errorTwoCycleCounter);

  MSG_Printf("=========================================================================\n");
}

// EOF
