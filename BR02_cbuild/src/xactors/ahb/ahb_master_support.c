//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*****************************************************************************
 *
 * Module Name: master_support.c
 *
 * Module Description:
 *
 * Support routines for AHB Master xtor.
 *
 * Author  : Kiran Maheriya
 *
 * Created : 02/23/2002
 *
 * $Id$
 *****************************************************************************
 */

#include "tbp.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"
#include <math.h>
#include "carbonXAhbMaster.h"
#include "ahb_master_private.h"

// Local Functions
static u_int32 MGetHSize            (u_int32 nbits);

//-/////////////////////////////////////////////////////////////////////////////
// Set Abort/Continue on error. Based on this setting, a master will abort
// a burst or continue when error response is received
// abortOnError=1 =>abort on error, abortOnError=0 => continue on error
//-/////////////////////////////////////////////////////////////////////////////
void carbonXAhbSetAbortOnError(u_int32 abortOnError) {
  u_int32 configReg;
  configReg = ((carbonXCsRead(MASTER_CONFIG_REG) & ~(0x1<<23)) |
	       ((abortOnError & 0x1)<<23));

  carbonXCsWrite(MASTER_CONFIG_REG, configReg);
}

//-/////////////////////////////////////////////////////////////////////////////
// Configure busy cycles. The first busy cycle will be inserted at the
// insertAt cycle number, this will be repeated after 'repeatAfter' arg.
// e.g. if insertAt=2 and repeatAfter=4, for a 16-beat burst the first
// busy cycle will appear after the second transfer of the burst, and then
// after every 4 transfer.
//-/////////////////////////////////////////////////////////////////////////////
void carbonXAhbSetBusyCycles (u_int32 insertAfter, u_int32 repeatAfter) {
  u_int32 configReg, busyField;

  // Fill in [31:24]={insertAfter,repeatAfter}
  busyField = ((insertAfter << 4) | repeatAfter) << 24;
  configReg = ((carbonXCsRead(MASTER_CONFIG_REG) & 0x00ffffff) | busyField);
  carbonXCsWrite(MASTER_CONFIG_REG, configReg);
}


//-/////////////////////////////////////////////////////////////////////////////
// Set HPROT value.
// This affects ALL types of transfers
// hprot: protection (as defined by AHB spec)         (Config bits [3:0])
//                           3 2 1 0
// ---------------+-+-+-----+-------+
//  config reg    |-|-|- - -| hprot | 
// ---------------+-+-+-----+-------+
//-/////////////////////////////////////////////////////////////////////////////
void carbonXAhbSetProtection (u_int32 hprot) {
  carbonXCsWrite(MASTER_CONFIG_REG, 
	      ((carbonXCsRead(MASTER_CONFIG_REG) & 0xfffffff0) | (hprot & 0xf)));
}

//-/////////////////////////////////////////////////////////////////////////////
// Set Burst parameters (Use this with carbonXRead/Write)
//
// incr:  1: INCR or 0: WRAP burst.                   (Config bit 8)
// lock:  1: Locked burst requested; 0: Don't Lock    (Config bit 7) 
// hsize: 0: 8-bit; 1: 16-bit, 2: 32-bit transfer     (Config bits [6:4])
//
// This values are ONLY used by carbonXRead/Write.
//                 8 7 6 5 4 3 2 1 0
// ---------------+-+-+-----+-------+
//  config reg    |I|L|size |- - - -|
// ---------------+-+-+-----+-------+
//-/////////////////////////////////////////////////////////////////////////////
void carbonXAhbSetBurstConfig (u_int32 hsize, u_int32 incr, u_int32 lock) {
  u_int32 configReg, burstcfg;
  burstcfg = ((incr & 0x1) << 8) | ((lock & 0x1) << 7) | ((hsize & 0x7) << 4);
  configReg = (carbonXCsRead(MASTER_CONFIG_REG) & 0xfffffe0f) | burstcfg;
  carbonXCsWrite(MASTER_CONFIG_REG, configReg);
}




//-///////////////////////////////////////////////////////////////////////////
// N-bit BURST of 'beats' number of beats (transfers)                       //
//                                                                          //
// nbits   => Data width in bits per transfer (8-/16-/32-bit supported)     //
// beats   => Number of beats (transfers) in a burst                        //
// incr    => 1: INCR burst, 0: WRAP burst                                  //
// lock    => 1: locked burst, 0: non locked burst                          //
// address => Start address for the burst read                              //
// data    => pointer to a block of at least 'beats' number of u_int32 words//
// Returns number of beats actually read                                    //
//-///////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbBurstRead (u_int32 nbits, u_int32 beats, u_int32 incr, 
		       u_int32 lock, u_int32 address, u_int32 *rdata) {
  AHB_MasterCtl_T ctl;
  u_int32 control, nbytes;
  u_int32 *wptr;
  u_int32 i;

  memset(&ctl, 0, sizeof(ctl));

  // Fill up the control word struct
  ctl.size  = MGetHSize(nbits);
  ctl.lock  = lock;
  ctl.incr  = incr;
  ctl.write = 0;
  // pack struct into a single word for transfer
  control = carbonXAhbMasterCtlCmd(ctl.size,ctl.lock,ctl.incr,ctl.write);

  // Compute number of bytes to be sent.
  // 8-bit => size=0, 16-bit => size=1 and 32-bit => size=2.
  // So, shifting 1 'size' times gives number of bytes.
  nbytes = beats << ctl.size;
  wptr = (u_int32 *) carbonmem_malloc( (nbytes+3) * sizeof(u_int8)); // allocate enough space for full words
  memset(wptr, 0, (nbytes+3)*sizeof(u_int8)); 

  // Read back the data from the transactor.
  carbonXUserOp(MASTER_AHB_OP, 0, address, wptr, NULL, &nbytes, &control);

  for (i=0; i<beats; i++)
    if      (ctl.size==0) *(((u_int8  *)rdata)+i) = *(((u_int8  *)wptr)+i);
    else if (ctl.size==1) *(((u_int16 *)rdata)+i) = *(((u_int16 *)wptr)+i);
    else                  rdata[i] = wptr[i];
  carbonmem_free(wptr);

  return (nbytes>>ctl.size); // This would have been adjusted by carbonXUserOp
}


//-///////////////////////////////////////////////////////////////////////////
// N-bit BURST of 'beats' number of beats (transfers)                       //
//                                                                          //
// nbits   => Data width in bits per transfer (8-/16-/32-bit supported)     //
// beats   => Number of beats (transfers) in a burst                        //
// incr    => 1: INCR burst, 0: WRAP burst                                  //
// lock    => 1: locked burst, 0: non locked burst                          //
// address => Start address for the burst read                              //
// data    => pointer to a block of at least 'beats' number of u_int32 words//
// Returns number of beats actually written                                 //
//-///////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbBurstWrite (u_int32 nbits, u_int32 beats, u_int32 incr,
			u_int32 lock, u_int32 address, u_int32 *wdata) {
  AHB_MasterCtl_T ctl;
  u_int32 control, nbytes;
  u_int32 *wptr;
  u_int32 i;

  memset(&ctl, 0, sizeof(ctl));

  // Fill up the control word struct
  ctl.size  = MGetHSize(nbits);
  ctl.lock  = lock;
  ctl.incr  = incr;
  ctl.write = 1;
  // pack struct into a single word for transfer
  control = carbonXAhbMasterCtlCmd(ctl.size,ctl.lock,ctl.incr,ctl.write);

  // Compute number of bytes to be sent.
  // 8-bit => size=0, 16-bit => size=1 and 32-bit => size=2.
  // So, shifting 1 'size' times gives number of bytes.
  nbytes = beats << ctl.size;

  wptr = (u_int32 *) carbonmem_malloc( (nbytes+3) * sizeof(u_int8)); // allocate enough space for full words
  memset(wptr, 0, (nbytes+3)*sizeof(u_int8)); 

  for (i=0; i<beats; i++) {
    if      (ctl.size==0) *(((u_int8  *)wptr)+i) = *(((u_int8  *)wdata)+i);
    else if (ctl.size==1) *(((u_int16 *)wptr)+i) = *(((u_int16 *)wdata)+i);
    else                  wptr[i] = wdata[i];
  }
  // Write to sim side.
  carbonXUserOp(MASTER_AHB_OP, 0, address, wptr, NULL, &nbytes, &control);
  carbonmem_free(wptr);
  return (nbytes>>ctl.size);  // This would have been adjusted by carbonXUserOp
}


//-/////////////////////////////////////////////////////////////////////////////
// Local function to calculate AHB HSIZE encoding from nbits
//-/////////////////////////////////////////////////////////////////////////////
static u_int32 MGetHSize (u_int32 nbits) {
  return log(nbits/8)/log(2); // Log base 2 of (nbits/8)
}

