//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*****************************************************************************
 * Module Name: slave_support.c
 *
 * Module Description:
 *    Support routines for AHB Slave xtor.
 *
 * Author  : Rajat Mohan
 * Created : 08/12/2002
 *
 * Revisions:
 *
 *****************************************************************************
 */


#include "tbp.h"
#include "carbonXAhbSlave.h"
#include "ahb_slave_private.h"

// Address of the slave configuration register
#define SLAVE_CONFIG_REG 0

// Local functions
static inline u_int32 carbonXAhbSetResponse(enum Response res, int respDelay, 
					 int enSplitResume, int splitResumeCount,
					 int splitResumeMId);
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                                                                            //
//                C o n f i g u r a t i o n   R o u t i n e s                 //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
//  Configure slave to insert wait cycles (HREADY=0) for 'wait_count'  number of
// cycles after 'delay' number of cycles from start of a transaction
////////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbInsertWaitCycles(int wait_count, int delay) {
  return carbonXAhbSlaveCfgCmd(0,0,0,0,delay,wait_count);
}

////////////////////////////////////////////////////////////////////////////////
// Configure slave to insert ERROR response in a transaction after 'respDelay'
// number of cycles from start of a transaction.
////////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbSetErrorResponse(int respDelay) {
  return carbonXAhbSetResponse(ERROR, respDelay, 0, 0, 0);
}

////////////////////////////////////////////////////////////////////////////////
// Configure slave to insert RETRY response in a transaction after 'respDelay'
// number of cycles from start of a transaction.
////////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbSetRetryResponse(int respDelay) {
  return carbonXAhbSetResponse(RETRY, respDelay, 0, 0, 0);
}

////////////////////////////////////////////////////////////////////////////////
// Configure slave to SPLIT the transaction after 'respDelay' number of cycles
// from start of a transaction. 
// Once slave has SPLIT a transaction, the same routine should be called by
// enabling 'enSplitResume' to instruct slave to resume split for master number
// given by 'splitResumeId' after 'splitResumeCount' number of cycles.
////////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbSetSplitResponse(int respDelay, int enSplitResume, 
			    int splitResumeCount, int splitResumeMId) { 

  return carbonXAhbSetResponse(SPLIT, respDelay, enSplitResume,
			splitResumeCount, splitResumeMId);
}


////////////////////////////////////////////////////////////////////////////////
// Configure slave to resume OKAY response. This is useful if slave was earlier
// configure to insert other (ERROR/RETRY/SPLIT) responses. This routine will
// bring slave to normal operation.
////////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbSetOkResponse(int respDelay) {
  return carbonXAhbSetResponse(OKAY, respDelay, 0, 0, 0);
}



////////////////////////////////////////////////////////////////////////////////
// This function writes response parameters into the slave configuration reg.
// res             : Response to be inserted (OKAY/ERROR/RETRY/SPLIT)
// respDelay       : Number of cycles after which the response should be
//                   inserted from start of a transaction
// REMAINING FIELDS ARE ONLY FOR res=SPLIT
// enSplitResume   : If slave has split a transaction, this field will enable it
//                   to resume a master ('splitResumeId') from split.
// splitResumeCount: Along with enSplitResume, instructs slave to resume split
//                   after 'splitResumeCount' number of cycles.
// splitResumeId   : The master that should be allowed to resume from split.
//                   HSPLITx will be asserted for appropriate master.
////////////////////////////////////////////////////////////////////////////////
static inline u_int32 carbonXAhbSetResponse(enum Response res, int respDelay, 
		                      int enSplitResume, int splitResumeCount,
                                      int splitResumeMId) {
  AHB_SlaveCfg_T cfgStruct;

  memset(&cfgStruct, 0, sizeof(cfgStruct));       // clear the data_struct

  cfgStruct.resp_count    = respDelay;

  if      (res == ERROR) cfgStruct.slave_resp  = 0x1;
  else if (res == RETRY) cfgStruct.slave_resp  = 0x2;
  else if (res == SPLIT) cfgStruct.slave_resp  = 0x3;
   
  cfgStruct.split_count   = splitResumeCount;
  cfgStruct.split_m_id    = splitResumeMId;
  cfgStruct.en_usr_resume = enSplitResume;

  return carbonXAhbSlaveCfgCmd(cfgStruct.en_usr_resume,cfgStruct.split_count,
			 cfgStruct.split_m_id,cfgStruct.slave_resp,
			 cfgStruct.resp_count,0);
}

/* Default Slave State Machine
 * This function executes whenever the Slave transactor
 * makes a request for service.
 */
void carbonXAhbDefaultSlaveSm(void) {

  int i;
  u_int32 terminate_operation = 0;
  //ECTL_CMD_T current_cmd;
  u_int32 cq_depth;
  // char my_handle[100];
  //TRANS_HANDLE_T handle_ptr;

  /*
   * Get the thread handle and copy it to a local variable
   * so that it remains on the thread stack
   */
  //handle_ptr = PLAT_GetHandleFromTransTable();
  //strcpy(my_handle,handle_ptr);

  /*
   * Do this until we find a command that tells us to finish,
   * at which time we exit and terminate the thread
   */
  while(terminate_operation==0) {

    i = 0;
    cq_depth = 0;

    /*
     * Read next command from command queue
     */
    //current_cmd = PLAT_GetCmdFromQueue(my_handle);

    /* Decide what to do based on the current command op_code */
    //switch(current_cmd.op_code) {

    /* Set the event signal indicated by current_cmd.ptr */
    //case SET_SIGNAL_CMD : { PLAT_SetSignal(current_cmd.ptr);
    //MSG_Milestone("setting signal %s with timeout %d\n",current_cmd.ptr,current_cmd.operand1); } break;

    /* Set the event indicated by current_cmd.ptr to the state indicated by current_cmd.operand1 */
    //case SET_SIGNALSTATE_CMD : PLAT_SetSignalState(current_cmd.ptr, current_cmd.operand1); break;

    /* Wait for the event indicated by current_cmd.ptr to reach the state indicated by current_cmd.operand1 */
    //case WAIT_SIGNALSTATE_CMD : PLAT_WaitSignalState(current_cmd.ptr, current_cmd.operand1, current_cmd.operand2); break;

    /* Perform a config-space write to the transactor at address cmd.operand1 with data cmd.operand2 */
    //case WRITE_TRANSACTOR_CS_CMD : tbp_cpf_cs_write(current_cmd.operand1, current_cmd.operand2); break;

    /* Break out of the command interpretation loop and terminate the thread */
    //case FINISH_CMD : terminate_operation = 1; break;

    /* If the command is NULL, do nothing */
    //case NULL_CMD : tbp_cpf_idle(1); break;

    /* We should always get a valid command, even if it says to do nothing. Error if command is un-recognised */
    //default: MSG_Error("In DefaultSlaveSm %s: Invalid command detected (opcode = 0x%08x)\n",my_handle,current_cmd.op_code);
    //}
  }
}

// --- eof ---
