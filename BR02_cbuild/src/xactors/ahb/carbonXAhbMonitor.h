/*
 *
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *****************************************************************************
 *
 * Module Name: arbiter_support.h
 *
 * Module Description:
 *
 * The "monitor_support.h" file contains definitions CSide  functions for reading back monirot data
 *
 * Author  : Kiran Maheriya
 *
 * Created : 04/01/02
 *
 * Revisions:
 *
 * $Id$
 *****************************************************************************
 */


#ifndef MONITOR_SUPPORT_H
#define MONITOR_SUPPORT_H

#ifdef __cplusplus
extern "C" {
#endif

#define MONITOR_CFG_REG 0

//-/////////////////////////////////////////////////////////////////////////////
// Number of masters and slaves on the AHB
//-/////////////////////////////////////////////////////////////////////////////
#define NUM_MASTERS  4   // Including dummy master
#define NUM_SLAVES   4   // Including default slave

#define NUM_MONREGS  40

// Read struct for Monitor
typedef struct {
  u_int32 globalReadCounter;
  u_int32 globalWriteCounter;
  u_int32 readCounterMaster[8];
  u_int32 writeCounterMaster[8];
  u_int32 readCounterSlave[8];
  u_int32 writeCounterSlave[8];
  u_int32 errorHtransCounter;
  u_int32 errorHgrantCounter;
  u_int32 errorTwoCycleCounter;
  u_int32 hrespErrorCounter;
  u_int32 hrespRetryCounter;
  u_int32 hrespSplitCounter;
} carbonXAhbMonRegs_T;


// Reset the counters
void carbonXAhbresetMonCounters(void);

// Read the counter values from hardware.
// Returned as a pointer to a structure holding the values
carbonXAhbMonRegs_T * carbonXAhbreadMonCounters(void);

// Pretty print the counter values. Doesnn't read from DUT.
// Must call carbonXAhbreadMonCounters at least once before calling this routine
void carbonXAhbprintMonCounters(carbonXAhbMonRegs_T *monRegs);

#ifdef __cplusplus
}
#endif

#endif
