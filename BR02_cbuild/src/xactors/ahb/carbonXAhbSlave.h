/*
 *
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *****************************************************************************
 *
 * Module Name: slave_support.h
 *
 * Module Description:
 *
 * Headers for AHB Slave support routines.
 *
 * Author  : Pala
 *
 * Created : 03/02/2002
 *
 * Revisions:
 * Kiran Maheriya 03/25/2003
 *   Cleaned up unwanted code. Removed slave number from arguments of functions.
 *
 * $Id$
 *****************************************************************************
 */


#ifndef SLAVE_SUPPORT_H
#define SLAVE_SUPPORT_H

#ifdef __cplusplus
extern "C" {
#endif

// Slave config reg address
#define SLAVE_CFG_REG 0

#define carbonXAhbSlaveCfgCmd(_en_usr_resume_,_split_count_,_split_m_id_,_slave_resp_,_resp_count_,_wait_count_) \
(((_en_usr_resume_ & 0x1)<<29) | ((_split_count_ & 0xff)<<21) | ((_split_m_id_ & 0x1f)<<16) | \
 ((_slave_resp_ & 0x3)<<14) | ((_resp_count_ & 0xff)<<6) | (_wait_count_ & 0x3f))


/////////////////////////////////////////////////////////////////////////////
// Insert waits cycles (HREADY=0) for a given slave (returns required config
// reg value)
// carbonXCsWrite(0) must be used to actually write into slave config reg
/////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbInsertWaitCycles(int wait_count, int delay);

/////////////////////////////////////////////////////////////////////////////
// Sets the responses ERROR or RETRY or SPLIT from a given slave
// Each returns required config reg value. carbonXCsWrite(0) must be used after
// this to actually write into slave configuration register
/////////////////////////////////////////////////////////////////////////////
u_int32 carbonXAhbSetOkResponse   (int respDelay);
u_int32 carbonXAhbSetErrorResponse(int respDelay);
u_int32 carbonXAhbSetRetryResponse(int respDelay);
u_int32 carbonXAhbSetSplitResponse(int respDelay, int enSplitResume, 
			    int splitResumeCount, int splitResumeMId); 

// Default Slave State Machine
// This function should be attached to the slave transactor
void carbonXAhbDefaultSlaveSm(void);

#ifdef __cplusplus
}
#endif

#endif
