/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXPin.cpp
    Purpose: This file provides the member function definition for using 10 bit
             encoded data pins.
*/

#include "srioXPin.h"

#define ENABLE_MONITOR 0

SrioxPin::SrioxPin(const SrioXtor *xtor, SrioxConstString name,
        const SrioxPinDirection direction):
    _xtor(xtor), _direction(direction), _currValue(0),
    _module(NULL), _net(NULL),
    _changeCallback(NULL), _callbackContext(NULL)
{
    _name = NEWARRAY(char, strlen(name) + 1);
    strcpy(_name, name);
}

SrioxBool SrioxPin::isOutput()
{
    if (_direction == SrioxDirOutput)
        return SrioxTrue;
    else
        return SrioxFalse;
}

SrioxBool SrioxPin::bind(CarbonObjectID *module, CarbonNetID *net)
{
    _module = module;
    _net = net;
    return SrioxTrue;
}
 
SrioxBool SrioxPin::bind(CarbonXSignalChangeCB changeCallback,
                                void *callbackContext)
{
    _changeCallback = changeCallback;
    _callbackContext = callbackContext;
    return SrioxTrue;
}

SrioxBool SrioxPin::write(const SrioxData10b data, CarbonTime currentTime)
{
    _currValue = data;
    return SrioxTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

SrioxBool SrioxPin::read(SrioxData10b &data, CarbonTime currentTime) const
{
    data = _currValue;
    return SrioxTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

SrioxBool SrioxPin::refresh(CarbonTime currentTime)
{
    CarbonUInt32 value;
    if (_direction == SrioxDirInput)
    {
        if (_module && _net)
        {
            if (carbonExamine(_module, _net, &value, NULL) != eCarbon_OK)
                return SrioxFalse;
        }
        else if (_changeCallback)
        {
            if ((*_changeCallback)(&value, _callbackContext) != eCarbon_OK)
                return SrioxFalse;
        }
        else
        {
            return SrioxFalse;
        }
        _currValue = value & 0x3FF;
#if ENABLE_MONITOR
        printf("%s[%u]: ", _name, (UInt) currentTime);
        printData(_currValue, 9);
        printf("\n");
#endif
    }
    else if (_direction == SrioxDirOutput)
    {
        value = _currValue;
        if (_module && _net)
        {
            if (carbonDeposit(_module, _net, &value, NULL) != eCarbon_OK)
                return SrioxFalse;
        }
        else if (_changeCallback)
        {
            if ((*_changeCallback)(&value, _callbackContext) != eCarbon_OK)
                return SrioxFalse;
        }
        else
        {
            return SrioxFalse;
        }
#if ENABLE_MONITOR
        printf("%s[%u]: ", _name, (UInt) currentTime);
        printData(_currValue, 9);
        printf("\n");
#endif
    }
    else
    {
        return SrioxFalse;
    }
    return SrioxTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

/* This routine returns a bit from the index of a specified register */
SrioxBit SrioxPin::getBit(CarbonUInt32 reg, UInt index)
{
    CarbonUInt32 mask = 0x1 << index;
    return (reg & mask) >> index;
}

/* This routines prints the register value starting from the MSB */
void SrioxPin::printData(CarbonUInt32 reg, UInt startMsb)
{
    for (Int i = startMsb; i >= 0; i--) /* Don't make 'i' UInt */
        printf("%d", getBit(reg, i));
}

