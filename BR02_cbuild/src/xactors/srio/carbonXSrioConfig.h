// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXSrioConfig_h_
#define __carbonXSrioConfig_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \addtogroup Srio
 * @{
 */

/*!
 * \file xactors/srio/carbonXSrioConfig.h
 * \brief Definition of SRIO Configuration Object Routines.  
 *
 * User need to create configuration object and set the configuration parameters
 * on the object. User then need to pass the configuration object to the
 * transactor's create API. The memory ownership will remain to the user -
 * transactor will copy the configuration parameters.
 *
 * Configuration support manipulation allowed are:
 * - Addressing - 34 (default), 50 and 66 bit
 * - Device ID size - 8 (default) or 16
 * - 1x mode - default is 4x mode
 * - Force 4x to 1x mode - default is false
 * - Force 4x to 1x mode to lane 2 - default is false
 */

/*! Forward declaration */
STRUCT CarbonXSrioConfigClass;

/*!
 * \brief Carbon SRIO Configuration Object declaration
 */
typedef STRUCT CarbonXSrioConfigClass CarbonXSrioConfig;

/*!
 * \brief Creates SRIO configuration object
 *
 * \returns Pointer to the created configuration object
 */
CarbonXSrioConfig *carbonXSrioConfigCreate(void);

/*!
 * \brief Destroys SRIO configuration object
 *
 * \param config Pointer to the configuration object to be destroyed.
 *
 * \retval 1 For success.
 * \retval 0 For failure. 
 */
CarbonUInt32 carbonXSrioConfigDestroy(CarbonXSrioConfig *config);

/*!
 * \brief Addressing types
 */
typedef enum {
    CarbonXSrioAddressing_34, /*!< 34 bit addressing */
    CarbonXSrioAddressing_50, /*!< 50 bit addressing */
    CarbonXSrioAddressing_66  /*!< 66 bit addressing */
} CarbonXSrioAddressingType;

/*!
 * \brief Sets SRIO configuration object addressing
 *
 * \param config Pointer to the configuration object.
 * \param supportParam Addressing type.
 */
void carbonXSrioConfigSetAddressing(CarbonXSrioConfig *config,
        CarbonXSrioAddressingType supportParam);

/*!
 * \brief Gets SRIO configuration object addressing
 *
 * \param config Pointer to the configuration object.
 *
 * \returns Addressing type
 */
CarbonXSrioAddressingType carbonXSrioConfigGetAddressing(
        const CarbonXSrioConfig *config);

/*!
 * \brief Transactor Device ID size types
 */
typedef enum {
    CarbonXSrioDeviceIdSize_8, /*!< 8 bit device ID */
    CarbonXSrioDeviceIdSize_16 /*!< 16 bit device ID */
} CarbonXSrioDeviceIdSizeType;

/*!
 * \brief Sets Device ID Size configuration of a transactor
 *
 * This API will set Device ID Size configuration of a transactor. Default 
 * would be 8 bit Device ID.
 *
 * \param config Pointer to the configuration object.
 * \param supportParam Device ID Size type.
 */
void carbonXSrioConfigSetDeviceIdSizeType(
        CarbonXSrioConfig *config, CarbonXSrioDeviceIdSizeType supportParam);

/*!
 * \brief Gets Device ID Size configuration of a transactor
 * 
 * \param config Pointer to the configuration object.
 *
 * \returns Device ID Size type
 */
CarbonXSrioDeviceIdSizeType carbonXSrioConfigGetDeviceIdSize(
        CarbonXSrioConfig *config);

/*!
 * \brief Transactor Link Type
 */
typedef enum {
    CarbonXSrioLinkType_4x,                 /*!< 4x link  */
    CarbonXSrioLinkType_1x,                 /*!< 1x link */
    CarbonXSrioLinkType_4xForcedTo1xLane0,  /*!< Transactor 4x link working 
                                                 in 1x mode is forced to 
                                                 lane0 */
    CarbonXSrioLinkType_4xForcedTo1xLane2,  /*!< Transactor 4x link working 
                                                 in 1x mode is forced to 
                                                 lane 2 */
} CarbonXSrioLinkType;

/*!
 * \brief Sets transactors link type
 *
 * \param config Pointer to the configuration object.
 * \param supportParam Link type
 */
void carbonXSrioConfigSetLinkType(
        CarbonXSrioConfig *config, CarbonXSrioLinkType supportParam);

/*!
 * \brief Gets transactors link type
 *
 * \param config Pointer to the configuration object.
 *
 * \returns Link type
 */
CarbonXSrioLinkType carbonXSrioConfigGetLinkType(CarbonXSrioConfig *config);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
