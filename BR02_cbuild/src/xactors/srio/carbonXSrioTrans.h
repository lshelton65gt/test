// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXSrioTrans_h_
#define __carbonXSrioTrans_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \addtogroup Srio
 * @{
 */

/*!
 * \file xactors/srio/carbonXSrioTrans.h
 * \brief Definition of Carbon SRIO Transaction Object.
 *
 * This file provides definition of transaction object.
 */

/*! Forward declaration */
STRUCT CarbonXSrioTransClass;

/*!
 * \brief Carbon SRIO Transaction Object
 */
typedef STRUCT CarbonXSrioTransClass CarbonXSrioTrans;

/*!
 * \brief Transaction Types
 *
 * This specifies different types of transactions.
 */
typedef enum {
    CarbonXSrioTrans_IO_REQUEST,        /*!< IO Request */
    CarbonXSrioTrans_MESSAGE,           /*!< Message Request */
    CarbonXSrioTrans_DOORBELL,          /*!< Doorbell Request */
    CarbonXSrioTrans_RESPONSE           /*!< Response */
} CarbonXSrioTransType;

/*!
 * \brief DWORD structure
 */
typedef struct {
    CarbonUInt32 msWord;                /*!< Most Significant Word */
    CarbonUInt32 lsWord;                /*!< Least Significant Word */
} CarbonXSrioDword;

/*!
 * \brief IO Request Types
 *  
 * This specifies different types of request created by IO Logical layer.
 */
typedef enum {
    CarbonXSrioIoRequest_NREAD,         /*!< NREAD Request */
    CarbonXSrioIoRequest_NWRITE,        /*!< NWRITE Request */
    CarbonXSrioIoRequest_SWRITE,        /*!< SWRITE Request */
    CarbonXSrioIoRequest_NWRITE_R,      /*!< NWRITE_R Request */
    CarbonXSrioIoRequest_ATOMIC_INC,    /*!< Atomic Increment Request */
    CarbonXSrioIoRequest_ATOMIC_DEC,    /*!< Atomic Decrement Request */
    CarbonXSrioIoRequest_ATOMIC_TSWP,   /*!< Atomic Test-and-swap Request */
    CarbonXSrioIoRequest_ATOMIC_SET,    /*!< Atomic Set Request */
    CarbonXSrioIoRequest_ATOMIC_CLR,    /*!< Atomic Clear Request */
    CarbonXSrioIoRequest_ATOMIC_SWAP    /*!< Atomic Swap Request */

} CarbonXSrioIoRequestType;

/*!
 * \brief Response Status Types
 *   
 * This specifies status of the response packet.
 */
typedef enum {
    CarbonXResponseStatus_DONE,     /*!< Response Status is DONE */
    CarbonXResponseStatus_ERROR,    /*!< Response Status is ERROR */
    CarbonXResponseStatus_RETRY     /*!< Response Status is RETRY */

} CarbonXSrioResponseStatusType;

/*!
 * \brief Gets transaction object Type
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Transaction type.
 */
CarbonXSrioTransType carbonXSrioGetTransType( const CarbonXSrioTrans *trans);

/*!
 * \brief IO Request Structure
 *
 */
typedef struct { 
    CarbonXSrioIoRequestType reqType;   /*!< IO Request type */
    CarbonUInt32 address;               /*!< Byte Address 
                                             (3 bit from LSB are 0)*/
    CarbonUInt32 extAddress;            /*!< Extended Address (16 or 32 bit) */
    CarbonUInt2 xamsbs;                 /*!< Extended Address MSBs */
    CarbonUInt1 wdptr;                  /*!< Word pointer */
    CarbonUInt4 rdSize;                 /*!< rdSize for read request */
    CarbonUInt4 wrSize;                 /*!< wrSize for write request */
    CarbonUInt32 dataSize;              /*!< DWORD data size, should match
                                             with wdptr and rdsize/wrsize,
                                             except SWRITE */
    CarbonXSrioDword *data;             /*!< DWORD data */
    CarbonUInt8 transId;                /*!< Transacion ID (srcTID) */
    CarbonUInt16 srcId;                 /*!< Source device ID */
    CarbonUInt16 destId;                /*!< Destination device ID */
    CarbonUInt2 prio;                   /*!< Priority of transaction */

} CarbonXSrioIoRequest;

/*! 
 * \brief Creates Transaction from IO Request Structure
 *
 * This routine can be called by the user or test layer to create a transaction
 * from request structure provided. Thus memory ownership of the transaction
 * object goes to the user and after transaction is complete, \a
 * carbonXSrioTransDestroy needs to be called to destroy the transaction.
 *
 * \param req IO Request Structure
 *
 * \returns Transaction object created
 */
CarbonXSrioTrans *carbonXSrioCreateIoRequestTrans(
                            const CarbonXSrioIoRequest *req);

/*!
 * \brief Gets IO Request Structure from received transaction
 *
 * This routine can be called by the user or test layer to retrieve request
 * structure from received transaction. Memory ownership of the returned
 * structure is on user and user need to call \a carbonXSrioDestroyIoRequest to
 * deallocate the memory consumed by this.
 *
 * \param trans Transaction object
 *
 * \returns IO Request Structure
 */
CarbonXSrioIoRequest *carbonXSrioGetIoRequest(
                            const CarbonXSrioTrans *trans);

/*!
 * \brief Gets data size from wdptr and wrSize / rdSize of IO Request
 * Structure in DWORDs
 *
 * \param req IO Request structure
 *
 * \returns IO Request data size in DWORDs
 */
CarbonUInt6 carbonXSrioGetIoRequestDataSize(const CarbonXSrioIoRequest *req);

/*!
 * \brief Destroy a IO Request structure
 *
 * One should use this API to destroy the obtained IO request from
 * \a carbonXSrioGetIoRequest API call.
 *
 * \param iorequest IO request to be destroyed
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioDestroyIoRequest(CarbonXSrioIoRequest *iorequest);

/*!
 * \brief Data Message structure (from Message Passing Logical layer)
 *       
 * Note, xmbox and msgseg are mapped to same packet bit fields.
 *
 * Thus, for single packet message where msglen is equal to 0, mailbox
 * number is specified by { xmbox, mbox } which can range to 64 mailboxes.
 * Here msgseg is ignored.
 *
 * For multiple packet message where msglen is greater than 0, msgseg indicates
 * the packet index within a message. Here xmbox is ignored and mailbox
 * number is specified only by mbox, which provides only 4 mailboxes.
 */
typedef struct {
    CarbonUInt2 mbox;            /*!< 2 bit LSB for mailbox number */
    CarbonUInt4 xmbox;           /*!< 4 bit MSB for mailbox number */
    CarbonUInt2 letter;          /*!< Letter number */
    CarbonUInt4 msglen;          /*!< Number of Packets in a message */
    CarbonUInt4 msgseg;          /*!< Current Packet number */
    CarbonUInt4 ssize;           /*!< Standard size */
    CarbonXSrioDword *data;      /*!< DWORD data array of size ssize */
    CarbonUInt16 srcId;          /*!< Source device ID */
    CarbonUInt16 destId;         /*!< Destination device ID */
    CarbonUInt2 prio;            /*!< Priority of transaction*/

} CarbonXSrioMessage;

/*! 
 * \brief Creates Transaction from Message Structure
 *
 * This routine can be called by the user or test layer to create a transaction
 * from request structure provided. Thus memory ownership of the transaction
 * object goes to the user and after transaction is complete, \a
 * carbonXSrioTransDestroy needs to be called to destroy the transaction.
 *
 * \param req Message Structure
 *
 * \returns Transaction object created
 */
CarbonXSrioTrans *carbonXSrioCreateMessageTrans(
                            const CarbonXSrioMessage *req);

/*!
 * \brief Gets Message Structure from received transaction
 *
 * This routine can be called by the user or test layer to retrieve request
 * structure from received transaction. Memory ownership of the returned
 * structure is on user and user need to call \a carbonXSrioDestroyMessage to
 * deallocate the memory consumed by this.
 *
 * \param trans Transaction object
 *
 * \returns Message Structure
 */
CarbonXSrioMessage *carbonXSrioGetMessage(
                            const CarbonXSrioTrans *trans);

/*!
 * \brief Destroy a message structure
 *
 * One should use this API to destroy the obtained message from
 * \a carbonXSrioGetMessage API call.
 *
 * \param message Message to be destroyed
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioDestroyMessage(CarbonXSrioMessage *message);


/*!
 * \brief Doorbell request structure (from Message Passing Logical layer)
 */
typedef struct {
    CarbonUInt8 transId;         /*!< Transacion ID (srcTID).
                                      Note, this field is ignored by internal
                                      BFM for transmitted doorbell, but
                                      is assigned to some value by BFM at
                                      receive side. */
    CarbonUInt16 info;           /*!< Doorbell information */
    CarbonUInt16 srcId;          /*!< Source device ID */
    CarbonUInt16 destId;         /*!< Destination device ID */
    CarbonUInt2 prio;            /*!< Priority of transaction*/

} CarbonXSrioDoorbell;

/*! 
 * \brief Creates Transaction from Doorbell Structure
 * 
 * This routine can be called by the user or test layer to create a transaction
 * from request structure provided. Thus memory ownership of the transaction
 * object goes to the user and after transaction is complete, \a
 * carbonXSrioTransDestroy needs to be called to destroy the transaction.
 *
 * \param req Doorbell Structure
 *
 * \returns Transaction object created
 */
CarbonXSrioTrans *carbonXSrioCreateDoorbellTrans(
                            const CarbonXSrioDoorbell *req);

/*!
 * \brief Gets Doorbell Structure from received transaction
 *
 * This routine can be called by the user or test layer to retrieve request
 * structure from received transaction. Memory ownership of the returned
 * structure is on user and user need to call \a carbonXSrioDestroyDoorbell to
 * deallocate the memory consumed by this.
 *
 * \param trans Transaction object
 *
 * \returns Doorbell Structure
 */
CarbonXSrioDoorbell *carbonXSrioGetDoorbell(
                            const CarbonXSrioTrans *trans);

/*!
 * \brief Destroy a doorbell structure
 *
 * One should use this API to destroy the obtained doorbell from
 * \a carbonXSrioGetDoorbell API call.
 *
 * \param doorbell Doorbell to be destroyed
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioDestroyDoorbell(CarbonXSrioDoorbell *doorbell);

/*!
 * \brief Response structure
 *
 * SRIO response packet can only be sent in response of some request received
 * by the transactor - the request should expect some response.
 */
typedef struct {
    CarbonUInt4 transaction;             /*!< Transaction -
                                           0x0 for no payload
                                           0x8 for payload
                                           0x1 for message request only */
    CarbonXSrioResponseStatusType status;/*!< Status of the request */
    CarbonUInt5 dataSize;                /*!< Data size in DWORDs */
    CarbonXSrioDword *data;              /*!< Data array requested */
    CarbonUInt16 srcId;                  /*!< Source device ID */
    CarbonUInt16 destId;                 /*!< Destination device ID */
    CarbonUInt2 prio;                    /*!< Priority of transaction*/
    CarbonUInt8 transId;                 /*!< Transacion ID.
                                              Note, this field should be same
                                              as \a transId of corresponding
                                              request packet.*/
} CarbonXSrioResponse;

/*! 
 * \brief Creates Transaction from Response Structure
 * 
 * This routine can be called by the user or test layer to create a transaction
 * from response  structure provided. Thus memory ownership of the transaction
 * object goes to the user and after transaction is complete, \a
 * carbonXSrioTransDestroy needs to be called to destroy the transaction.
 *
 * \param resp Response Structure
 *
 * \returns Transaction object created
 */
CarbonXSrioTrans *carbonXSrioCreateResponseTrans(
                            const CarbonXSrioResponse *resp);

/*!
 * \brief Gets Response Structure from received transaction
 *
 * This routine can be called by the user or test layer to retrieve response
 * structure from received transaction. Memory ownership of the returned
 * structure is on user and user need to call \a carbonXSrioDestroyResponse to
 * deallocate the memory consumed by this.
 *
 * \param trans Transaction object
 *
 * \returns Response Structure
 */
CarbonXSrioResponse *carbonXSrioGetResponse(
                            const CarbonXSrioTrans *trans);

/*!
 * \brief Destroy a response structure
 *
 * One should use this API to destroy the obtained response from
 * \a carbonXSrioGetResponse API call.
 *
 * \param resp Response to be destroyed
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioDestroyResponse(CarbonXSrioResponse *resp);

/*!
 * \brief Destroy a transaction object
 *
 * One should use this API to destroy the created transactions from
 * request or response at reportTransaction callback
 *
 * \param trans Pointer to transaction object.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioTransDestroy(CarbonXSrioTrans *trans);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
