// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_SERDES_H
#define SRIOX_SERDES_H

#include "srioXPin.h"

/*!
 * \file srioXSerDes.h
 * \brief Header file for serializer and deserializer.
 *
 * This file provides the class definition serializer and deserializer to be
 * used at the bottom of the BFM.
 */

/*!
 * \brief Class to represent serializer.
 * 
 *
 */
class SrioxSerializer {
public: CARBONMEM_OVERRIDES

private:

    const SrioXtor *_xtor;                 //!< Transactor reference
    SrioxPin * const _inputPort;           //!< Input port of 10 bit data
    SrioxData10b _shiftReg;                //!< Shift register
    UInt _bitCounter;                      //!< Bit counter
    
public:

    // Constructor and destructor
    SrioxSerializer(const SrioXtor *xtor, SrioxPin * const inputPin):
        _xtor(xtor), _inputPort(inputPin), _shiftReg(0), _bitCounter(0) {}
    ~SrioxSerializer() {}

    // Evaluate
    SrioxBool evaluate(SrioxBit &serialOut, CarbonTime currentTime);
};

/*!
 * \brief Class to represent deserializer.
 * 
 *
 */
class SrioxDeserializer {
public: CARBONMEM_OVERRIDES

private:

    const SrioXtor *_xtor;                 //!< Transactor reference
    SrioxPin * const _outputPort;          //!< Output port of 10 bit data
    SrioxData10b _shiftReg;                //!< Shift register
    UInt _bitCounter;                      //!< Bit counter
    SrioxBool _gotSync;                    //!< Flag to hold sync status
    
public:

    // Constructor and destructor
    SrioxDeserializer(const SrioXtor *xtor, SrioxPin * const outputPin):
        _xtor(xtor), _outputPort(outputPin), _shiftReg(0), _bitCounter(0),
        _gotSync(SrioxFalse) {}
    ~SrioxDeserializer() {}

    // Evaluate
    SrioxBool evaluate(SrioxBit serialIn, CarbonTime currentTime);
};

#endif

