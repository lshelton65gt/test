// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_PIN_H
#define SRIOX_PIN_H

#include "srioXDefines.h"
#include "xactors/carbonX.h"

/*!
 * \file srioXPin.h
 * \brief Header file for pin definition.
 *
 * This file provides the class definition for 10 bit encoded data pins
 * to be used for each lane in pair.
 */

/*!
 * \brief Direction type
 *
 * This type defines the direction of pin.
 */
enum SrioxPinDirection {
    SrioxDirUnknown,
    SrioxDirInput,
    SrioxDirOutput,
    SrioxDirInout
};

/*!
 * \brief Class to represent 10 bit transactor encoded data pin.
 *
 * This represents 10 bit data pin which can be connected with Carbon Model
 * by a CarbonNet or through read/write callback.
 */
class SrioxPin {
public: CARBONMEM_OVERRIDES

private:

    const SrioXtor *_xtor;                 //!< Transactor reference
    SrioxString _name;                     //!< Pin name
    const SrioxPinDirection _direction;    //!< Pin direction
    SrioxData10b _currValue;               //!< Buffered value of net.
                                           //   Current value to be used
                                           //   during transactor and
                                           //   SERDES evaluation. This
                                           //   value is updated during
                                           //   referesh cycle.
    
    /* Carbon Model connection mode */
    CarbonObjectID *_module;               //!< Connected Carbon Model module
    CarbonNetID *_net;                     //!< Connected Carbon Model net

    /* Callback connection mode */
    CarbonXSignalChangeCB _changeCallback; //!< Callback functions to
                                           //   to refresh the currValue
                                           //   accessimng the net
    void *_callbackContext;                //!< Callback context

public:

    // Constructor and destructor
    SrioxPin(const SrioXtor *xtor, SrioxConstString name,
            const SrioxPinDirection direction);
    ~SrioxPin() { DELETE(_name); }

    SrioxBool isOutput();
    // Environment binding
    SrioxBool bind(CarbonObjectID *module, CarbonNetID *net);
    SrioxBool bind(CarbonXSignalChangeCB changeCallback,
            void *callbackContext);

    // Read/Write access
    SrioxBool write(const SrioxData10b data, CarbonTime currentTime);
    SrioxBool read(SrioxData10b &data, CarbonTime currentTime) const;

    // Refresh
    SrioxBool refresh(CarbonTime currentTime);

    // Utility functions
    SrioxBit getBit(CarbonUInt32 reg, UInt index);
    void printData(CarbonUInt32 reg, UInt startMsb);
};

#endif
