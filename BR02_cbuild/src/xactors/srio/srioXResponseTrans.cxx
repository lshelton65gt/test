/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXResponseTrans.cpp
    Purpose: This file provides the member function definitions for Response 
             Transaction objects.
*/

#include "srioXResponseTrans.h"
#include "srioXtor.h"

CarbonXSrioResponseTrans::CarbonXSrioResponseTrans(
        const CarbonXSrioResponse *resp):
    _resp(*resp), _error(CarbonXSrioError_NoError)
{   
    if (_resp.dataSize)
    {
        _dwData = copyToBfmDw(_resp.dataSize, resp->data);
        if(_dwData == NULL)
            _error = CarbonXSrioError_NoMemory;
    }
    else
        _dwData = NULL;
}

CarbonXSrioResponseTrans::CarbonXSrioResponseTrans(RIO_RESPONSE_T *response):
    _error(CarbonXSrioError_NoError)
{
    assert(response);

    _resp.transaction = response->transaction;
    if (response->status == 0)
        _resp.status = CarbonXResponseStatus_DONE;
    else if (response->status == 7)
        _resp.status = CarbonXResponseStatus_ERROR;
    else if (response->status == 3)
        _resp.status = CarbonXResponseStatus_RETRY;
    else
    {
        assert(0);
    }
    _resp.srcId = response->src_ID;
    if (response->transp_Type == RIO_PL_TRANSP_16)
        _resp.destId = ((response->tr_Info >> 8) & 0xFF);
    else
         _resp.destId = ((response->tr_Info >> 16) & 0xFFFF);
    _resp.prio = response->prio;
    _resp.transId = response->target_TID;

    _resp.dataSize = response->dw_Num;
    if (_resp.dataSize > 0)
    {
        _dwData = copyBfmDw(response->dw_Num, response->data); 
        if (_dwData == NULL)
        {
            _error = CarbonXSrioError_NoMemory;
            return;
        }
    }
    else
        _dwData = NULL;
}

CarbonXSrioResponseTrans::~CarbonXSrioResponseTrans()
{
    if (_dwData != NULL)
        DELETE(_dwData);
}

CarbonXSrioResponse* CarbonXSrioResponseTrans::getResponse() const
{
    CarbonXSrioResponse *resp = NEW(CarbonXSrioResponse);
    if (resp == NULL)
        return NULL;
    *resp = _resp;
    
    if (_resp.dataSize)
    {
        resp->data = copyFromBfmDw(_resp.dataSize, _dwData);
        if (resp->data == NULL)
        {
            DELETE(resp);
            return NULL;
        }
    }
    else
        resp->data = NULL;
    return resp;
}

SrioxBool CarbonXSrioResponseTrans::start(CarbonXSrioClass *xtor,
        UInt tag) const
{
    // BFM respuest structure 
    RIO_RESPONSE_T bfmResp;
    bfmResp.prio = _resp.prio;
    bfmResp.ftype = 13;          // Packet type for Response transaction
    bfmResp.transaction = _resp.transaction;

    if (_resp.status == CarbonXResponseStatus_DONE)
        bfmResp.status = 0x0;
    else if (_resp.status == CarbonXResponseStatus_ERROR)
        bfmResp.status = 0x7;     
    else 
        bfmResp.status = 0x3;    // RETRY, only for message passing

    bfmResp.tr_Info = (xtor->getConfig()->getDeviceIdSize() ==  
            CarbonXSrioDeviceIdSize_8) ? (_resp.destId << 8 | _resp.srcId) : 
                                         (_resp.destId << 16 | _resp.srcId);
    bfmResp.src_ID = _resp.srcId;
    bfmResp.target_TID = _resp.transId;
    bfmResp.sec_ID = bfmResp.sec_TID = 0;
    bfmResp.dw_Num = _resp.dataSize;
    bfmResp.data = _dwData;
    if (xtor->getConfig()->getDeviceIdSize() ==  CarbonXSrioDeviceIdSize_8)
        bfmResp.transp_Type = RIO_PL_TRANSP_16;
    else
        bfmResp.transp_Type = RIO_PL_TRANSP_32;

    return ((xtor->getBfmFtray()).rio_PL_Send_Response(
                xtor->getBfm(), tag, &bfmResp) == RIO_OK);
}


