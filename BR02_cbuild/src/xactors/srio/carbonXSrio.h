// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbonXSrio_h_
#define __carbonXSrio_h_

#ifndef __carbonXSrioConfig_h_
#include "xactors/srio/carbonXSrioConfig.h"
#endif

#ifndef __carbonXSrioTrans_h_
#include "xactors/srio/carbonXSrioTrans.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \defgroup Srio SRIO C-Interface
 * \brief C-Linkable Interface to the SRIO Transactor
 */

/*!
 * \addtogroup Srio
 * @{
 */

/*!
 * \file xactors/srio/carbonXSrio.h
 * \brief Definition of SRIO Transactor Object
 *
 * This file provides definition of SRIO transactor object and 
 * related APIs.
 *
 * Transactor update semantics: Following sequence of operation will be
 * followed to run the thransactor for each clock cycle
 *     -# Evaluate all transactors and the model. This should call
 *        carbonXSrioEvaluate().
 *     -# Call carbonXSrioRefreshInputs() and carbonXSrioRefreshOutputs()
 *        for every transactor to update the input and output connections
 *        between the model and the transactors.
 */

/*!
 * \brief Carbon SRIO Transactor Class */
STRUCT CarbonXSrioClass;

/*!
 * \brief Carbon SRIO Transactor Handle
 *
 * This type provides a handle or pointer to transactor object. All transactor
 * APIs need this handle to be passed as its argument.
 */
typedef STRUCT CarbonXSrioClass* CarbonXSrioID;

/*!
 * \brief Carbon SRIO transactor error notification type
 *
 * This type provides the error type that can be signaled by the transactor
 * for any unmanagable case, that it might see.
 */
typedef enum {
    CarbonXSrioError_NoError = 0,         /*!< No error */
    CarbonXSrioError_NoMemory,            /*!< Memory allocation error */
    CarbonXSrioError_InvalidTrans,        /*!< Invalid transaction happended */
    CarbonXSrioError_InternalError        /*!< Internal error in code */
} CarbonXSrioErrorType;

/*!
 * \brief Creates a transactor Transactor Object
 *
 * \param transactorName     Instance name of this transactor object.
 *
 * \param config             Specifies transactor configuration parameters.
 *                           Transactor will copy the configuration object.
 *
 * \param maxTransfers       Maximum number of transactions that can be queued.
 *                           This value can range from 1 to 127. Note, this
 *                           indicates maximum number of transactions that user
 *                           can start simulaneously. Once the queue is full,
 *                           the transactor will still take one more transaction
 *                           in an additional waiting buffer and after that
 *                           inform user unavailability of buffer space. This
 *                           additional transaction will not be placed for
 *                           transaction start at that time.
 *
 * \param reportTransaction  Callback function to be called when a transaction
 *                           is complete. This is relevant only for the
 *                           intiatior of the transaction.
 *
 * \param setDefaultResponse Callback function used by transactors to set
 *                           the default conditions of a transaction. This is
 *                           redundent in SRIO transactor context.
 *
 * \param setResponse        Callback function used by transactors to set the
 *                           response to be provided to a transction. This is
 *                           called on the receiving side once the full
 *                           transaction has been received. One can check
 *                           a request received and initiate a response
 *                           transaction from this callback.
 *
 * \param refreshResponse    This callback can be used to revise return values
 *                           or latency. It is not presently used in the SRIO
 *                           transactors.
 *
 * \param notify             Callback function used to report a transactor
 *                           fault that is associated with a particular
 *                           transaction. It is not presently used in the SRIO
 *                           transactor context.
 *
 * \param notify2            Callback function used to report a transactor
 *                           fault that cannot be associated with a
 *                           particular transaction.
 *
 * \param stimulusInstance   Provided as a convenience to the user. It is
 *                           stored as a void pointer by the transactor
 *                           and returned as a parameter of the callback
 *                           functions to help identify the generator
 *                           instance that should service the callback
 *                           where more than one intrerface is in use.
 *
 * \returns Created transactor handle
 */
CarbonXSrioID carbonXSrioCreate(const char* transactorName,
        const CarbonXSrioConfig *config, const CarbonUInt32 maxTransfers,
        void (*reportTransaction)(CarbonXSrioTrans*, void*),
        void (*setDefaultResponse)(CarbonXSrioTrans*, void*),
        void (*setResponse)(CarbonXSrioTrans*, void*),
        void (*refreshResponse)(CarbonXSrioTrans*, void*),
        void (*notify)(CarbonXSrioErrorType, const char*,
            CarbonXSrioTrans*, void*),
        void (*notify2)(CarbonXSrioErrorType, const char*, void*),
        void *stimulusInstance);

/*!
 * \brief Destroys a transactor object
 *
 * \param xtor Transactor object.
 *
 * \retval 1 For successful destruction.
 * \retval 0 For failure in destruction.
 */
CarbonUInt32 carbonXSrioDestroy(CarbonXSrioID xtor);

/*!
 * \brief Gets transactor name
 *
 * \param xtor Transactor object.
 *
 * \returns Name of the transactor
 */
const char *carbonXSrioGetName(CarbonXSrioID xtor);

/*!
 * \brief Gets the current configuration object of the transactor
 *
 * This API will copy the configuration parameters from transactor to the
 * configuration object passed.
 *
 * \param xtor Transactor object.
 * \param config Current configuration object.
 */
void carbonXSrioGetConfig(CarbonXSrioID xtor, CarbonXSrioConfig *config);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by name
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName   Carbon Model name.
 * \param carbonModelHandle Carbon Model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXSrioConnectByModelSignalName(CarbonXSrioID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by
 *        CarbonNetID
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName   Carbon Model name.
 * \param carbonModelHandle Carbon Model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXSrioConnectByModelSignalHandle(CarbonXSrioID xtor,
        CarbonXInterconnectNameHandlePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by
 *        input/output change callbacks
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXSrioConnectToLocalSignalMethods(CarbonXSrioID xtor,
        CarbonXInterconnectNameCallbackTrio *nameList);

/*!
 * \brief Checks if transactor can accept additional transactions.
 * 
 * This API will check if more transactions can be pushed into the
 * transaction queue. Note, this will return 0 to user, once the user
 * has pushed maxTransfers + 1 transactions. Transactor can hold the additional
 * transaction in temporary waiting location, and start once buffer becomes
 * available.
 *
 * \param xtor Transactor object.
 *
 * \retval 1 If transactor can accept additional transactions.
 * \retval 0 Otherwise.
*/
CarbonUInt32 carbonXSrioIsSpaceInTransactionBuffer(CarbonXSrioID xtor);

/*!
 * \brief Resets transactor.
 *
 * This API resets and initialise the transactor.
 *
 * \param xtor Transactor object.
 * 
 * \retval 1 If transactor is successfully resetted.
 * \retval 0 Otherwise.
 */
CarbonUInt32 carbonXSrioReset(CarbonXSrioID xtor);

/*!
 * \brief Starts a new transaction
 *
 * This API will push a created transaction into transaction queue of
 * the transactor.
 *
 * \param xtor Transactor object.
 * \param trans Transaction to be pushed in the transaction queue.
 *
 * \retval 1 If the transaction is successfully added to the input buffer.
 * \retval 0 Otherwise.
 */
CarbonUInt32 carbonXSrioStartNewTransaction(CarbonXSrioID xtor,
        CarbonXSrioTrans *trans);

/*!
 * \brief Evaluates transactor
 * 
 * This API evaluates the transactor and updates all the functionality
 * for a clock. Note, in this API call transactor state is updated
 * but i/o of the transactor are not changed.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which evaluation is happening.
 *
 * \retval 1 For successful evaluation.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioEvaluate(CarbonXSrioID xtor, CarbonTime currentTime);

/*!
 * \brief Refreshes transactor input signals
 *
 * In this API, all input signal ports of the transactor
 * are updated but transactor state remains unchanged. Thus at this point
 * transactor will communicate with the model for reading signal values.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which refreshing is happening.
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioRefreshInputs(CarbonXSrioID xtor,
        CarbonTime currentTime);

/*!
 * \brief Refreshes transactor output signals
 *
 * In this API, all output signal ports of the transactor
 * are updated but transactor state remains unchanged. Thus at this point
 * transactor will communicate with the model for writing signal values.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which refreshing is happening.
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSrioRefreshOutputs(CarbonXSrioID xtor,
        CarbonTime currentTime);


/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
