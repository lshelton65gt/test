/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXConfig.cpp
    Purpose: This file provides the member function definition for 
             configuration object.
*/

#include "srioXConfig.h"

CarbonXSrioConfigClass::CarbonXSrioConfigClass()
{
    // Setting the default values of the instantion parameters
    _instParam.ext_Address_Support = 0x7;    // 66 bit addressing
    _instParam.is_Bridge = RIO_FALSE;
    _instParam.has_Memory = RIO_FALSE;
    _instParam.has_Processor = RIO_FALSE;
    _instParam.has_Fcp = RIO_FALSE;
    _instParam.coh_Granule_Size_32 = RIO_FALSE;
    _instParam.coh_Domain_Size = 0;
    _instParam.device_Identity = 0;
    _instParam.device_Vendor_Identity = 0;
    _instParam.device_Rev = 0;
    _instParam.device_Minor_Rev = 0;
    _instParam.device_Major_Rev = 1;
    _instParam.assy_Identity = 0;
    _instParam.assy_Vendor_Identity = 0;
    _instParam.assy_Rev = 0;
    _instParam.entry_Extended_Features_Ptr = 0x0100; 
    _instParam.next_Extended_Features_Ptr = 0;
    _instParam.source_Trx = 0x00F3FC;        // all the operations are enabled
    _instParam.dest_Trx = 0x00F3FC;          // all the operations are enabled
    _instParam.has_Doorbell = RIO_TRUE;
    _instParam.pl_Inbound_Buffer_Size = 0;
    _instParam.pl_Outbound_Buffer_Size = 0;
    _instParam.ll_Inbound_Buffer_Size = 0;
    _instParam.ll_Outbound_Buffer_Size = 0;
    _instParam.ack_Delay_Enable = RIO_FALSE;
    _instParam.ack_Delay_Min = 0;
    _instParam.ack_Delay_Max = 0;
    _instParam.gda_Tx_Illegal_Packet = RIO_FALSE;
    _instParam.gda_Rx_Illegal_Packet = RIO_FALSE;
    _instParam.tx_Illegal_Pnacc_Resend_Cnt = 0;
    _instParam.RIO_Link_Valid_To1_I = 0;
    _instParam.resp_Packet_Delay_Enable = RIO_FALSE;
    _instParam.resp_Packet_Delay_Cntr = 0;
    _instParam.resp_Stomp_Needed = RIO_FALSE;
    // PE supports message box 1 to 64 by default
    _instParam.mbox1  = RIO_TRUE;
    _instParam.mbox2  = RIO_TRUE;
    _instParam.mbox3  = RIO_TRUE;
    _instParam.mbox4  = RIO_TRUE;
    _instParam.mbox5  = RIO_TRUE;
    _instParam.mbox6  = RIO_TRUE;
    _instParam.mbox7  = RIO_TRUE;
    _instParam.mbox8  = RIO_TRUE;
    _instParam.mbox9  = RIO_TRUE;
    _instParam.mbox10 = RIO_TRUE;
    _instParam.mbox11 = RIO_TRUE;
    _instParam.mbox12 = RIO_TRUE;
    _instParam.mbox13 = RIO_TRUE;
    _instParam.mbox14 = RIO_TRUE;
    _instParam.mbox15 = RIO_TRUE;
    _instParam.mbox16 = RIO_TRUE;
    _instParam.mbox17 = RIO_TRUE;
    _instParam.mbox18 = RIO_TRUE;
    _instParam.mbox19 = RIO_TRUE;
    _instParam.mbox20 = RIO_TRUE;
    _instParam.mbox21 = RIO_TRUE;
    _instParam.mbox22 = RIO_TRUE;
    _instParam.mbox23 = RIO_TRUE;
    _instParam.mbox24 = RIO_TRUE;
    _instParam.mbox25 = RIO_TRUE;
    _instParam.mbox26 = RIO_TRUE;
    _instParam.mbox27 = RIO_TRUE;
    _instParam.mbox28 = RIO_TRUE;
    _instParam.mbox29 = RIO_TRUE;
    _instParam.mbox30 = RIO_TRUE;
    _instParam.mbox31 = RIO_TRUE;
    _instParam.mbox32 = RIO_TRUE;
    _instParam.mbox33 = RIO_TRUE;
    _instParam.mbox34 = RIO_TRUE;
    _instParam.mbox35 = RIO_TRUE;
    _instParam.mbox36 = RIO_TRUE;
    _instParam.mbox37 = RIO_TRUE;
    _instParam.mbox38 = RIO_TRUE;
    _instParam.mbox39 = RIO_TRUE;
    _instParam.mbox40 = RIO_TRUE;
    _instParam.mbox41 = RIO_TRUE;
    _instParam.mbox42 = RIO_TRUE;
    _instParam.mbox43 = RIO_TRUE;
    _instParam.mbox44 = RIO_TRUE;
    _instParam.mbox45 = RIO_TRUE;
    _instParam.mbox46 = RIO_TRUE;
    _instParam.mbox47 = RIO_TRUE;
    _instParam.mbox48 = RIO_TRUE;
    _instParam.mbox49 = RIO_TRUE;
    _instParam.mbox50 = RIO_TRUE;
    _instParam.mbox51 = RIO_TRUE;
    _instParam.mbox52 = RIO_TRUE;
    _instParam.mbox53 = RIO_TRUE;
    _instParam.mbox54 = RIO_TRUE;
    _instParam.mbox55 = RIO_TRUE;
    _instParam.mbox56 = RIO_TRUE;
    _instParam.mbox57 = RIO_TRUE;
    _instParam.mbox58 = RIO_TRUE;
    _instParam.mbox59 = RIO_TRUE;
    _instParam.mbox60 = RIO_TRUE;
    _instParam.mbox61 = RIO_TRUE;
    _instParam.mbox62 = RIO_TRUE;
    _instParam.mbox63 = RIO_TRUE;
    _instParam.mbox64 = RIO_TRUE;

    // Setting the default values of the initialization parameters
    _initParam.transp_Type = RIO_PL_TRANSP_16;
    _initParam.dev_ID = 0;
    _initParam.lcshbar = 0;
    _initParam.lcsbar = 0;
    _initParam.is_Host = RIO_TRUE;
    _initParam.is_Master_Enable = RIO_TRUE;
    _initParam.is_Discovered = RIO_FALSE;
    _initParam.ext_Address = RIO_TRUE;
    _initParam.ext_Address_16 = RIO_FALSE;
    _initParam.ext_Mailbox_Support = RIO_TRUE;
    _initParam.lp_Serial_Is_1x = RIO_FALSE;
    _initParam.is_Force_1x_Mode = RIO_FALSE;
    _initParam.is_Force_1x_Mode_Lane_0 = RIO_TRUE;
    _initParam.input_Is_Enable = RIO_TRUE;
    _initParam.output_Is_Enable = RIO_TRUE;
    _initParam.discovery_Period = 1000; // Essential to enable 4x mode
    _initParam.silence_Period = 0;
    _initParam.comp_Seq_Rate = 5000;
    _initParam.status_Sym_Rate = 132;
    _initParam.comp_Seq_Rate_Check = 0;
    _initParam.comp_Seq_Size = 0;
    _initParam.res_For_3_Out = 0;
    _initParam.res_For_2_Out = 0;
    _initParam.res_For_1_Out = 0;
    _initParam.res_For_3_In  = 0;
    _initParam.res_For_2_In = 0;
    _initParam.res_For_1_In = 0;
    _initParam.pass_By_Prio = RIO_FALSE;
    _initParam.transmit_Flow_Control_Support = RIO_TRUE;
    _initParam.enable_Retry_Return_Code = RIO_FALSE;
    _initParam.enable_LRQ_Resending = RIO_TRUE;
    _initParam.icounter_Max = 2;
    _initParam.data_Corrupted = RIO_FALSE;
}

void CarbonXSrioConfigClass::setMaxTransSize(UInt maxTransSize)
{
    // Minimum size of Inbound PL buffer should be 7 - found by trial and error
    static const UInt _bfmMinInBuffSize = 7;
    _instParam.pl_Inbound_Buffer_Size =
        (maxTransSize < _bfmMinInBuffSize) ? _bfmMinInBuffSize : maxTransSize;
    _instParam.pl_Outbound_Buffer_Size = maxTransSize;
}

/*
   This function sets the addressing mode parameters of the SRIO Configuration
   Structure.
*/
void CarbonXSrioConfigClass::setAddressing(
        const CarbonXSrioAddressingType addressing)
{
    if (addressing == CarbonXSrioAddressing_66)
    {
        _instParam.ext_Address_Support = 0x7;
        _initParam.ext_Address = RIO_TRUE;
        _initParam.ext_Address_16 = RIO_FALSE;
    }
    else if (addressing == CarbonXSrioAddressing_50)
    {
        _instParam.ext_Address_Support = 0x3;
        _initParam.ext_Address = RIO_TRUE;
        _initParam.ext_Address_16 = RIO_TRUE;
    }
    else
    {
        _instParam.ext_Address_Support = 0x1;
        _initParam.ext_Address = RIO_FALSE;
        _initParam.ext_Address_16 = RIO_FALSE;
    }
}

/*
   This function returns the addressing mode parameter from the SRIO 
   Configuration Structure.
*/
CarbonXSrioAddressingType CarbonXSrioConfigClass::getAddressing() const
{
    if (_instParam.ext_Address_Support == 0x7)
    {
        return CarbonXSrioAddressing_66;
    }
    else if (_instParam.ext_Address_Support == 0x3)
    {
        return CarbonXSrioAddressing_50;
    }
    else
    {
        return CarbonXSrioAddressing_34;
    }
}

/*
   This function sets the device ID size type, either 8 bit or 16 bit,
   depending upon idSize, passed as argument.
*/
void CarbonXSrioConfigClass::setDeviceIdSize(
        const CarbonXSrioDeviceIdSizeType idSize)
{
    if (idSize == CarbonXSrioDeviceIdSize_16)
    {
        _initParam.transp_Type = RIO_PL_TRANSP_32;
    }
    else
    {
        _initParam.transp_Type = RIO_PL_TRANSP_16;
    }
}

/*
   This function returns the device ID size type, either 8 bit or 16 bit,
   from the configuration structure.
*/
CarbonXSrioDeviceIdSizeType CarbonXSrioConfigClass::getDeviceIdSize() const
{
    if (_initParam.transp_Type == RIO_PL_TRANSP_16)
    {
        return CarbonXSrioDeviceIdSize_8;
    }
    else
    {
        return CarbonXSrioDeviceIdSize_16;
    }
}

/*
   This function sets the Link type, as defined in the enum 
   CarbonXSrioLinkType, passed as argument.
*/
void CarbonXSrioConfigClass::setLinkType(const CarbonXSrioLinkType linkType)
{
    switch (linkType)
    {
        case CarbonXSrioLinkType_4x:
        {
            _initParam.lp_Serial_Is_1x = RIO_FALSE;
            _initParam.is_Force_1x_Mode = RIO_FALSE;
            _initParam.is_Force_1x_Mode_Lane_0 = RIO_FALSE;
            break;
        }
        case CarbonXSrioLinkType_1x:
        {
            _initParam.lp_Serial_Is_1x = RIO_TRUE;
            break;
        }
        case CarbonXSrioLinkType_4xForcedTo1xLane0:
        {
            _initParam.lp_Serial_Is_1x = RIO_FALSE;
            _initParam.is_Force_1x_Mode = RIO_TRUE;
            _initParam.is_Force_1x_Mode_Lane_0 = RIO_TRUE;
            break;
        }
        case CarbonXSrioLinkType_4xForcedTo1xLane2:
        {
            _initParam.lp_Serial_Is_1x = RIO_FALSE;
            _initParam.is_Force_1x_Mode = RIO_TRUE;
            _initParam.is_Force_1x_Mode_Lane_0 = RIO_FALSE;
            break;
        }
    }
}

/*
   This function returns the Link type, as defined in the enum 
   CarbonXSrioLinkType, from the configuration structure.
*/
CarbonXSrioLinkType CarbonXSrioConfigClass::getLinkType()
{
    if (_initParam.lp_Serial_Is_1x == RIO_TRUE)
        return CarbonXSrioLinkType_1x;
    else 
    {
        if (_initParam.is_Force_1x_Mode == RIO_FALSE)
            return CarbonXSrioLinkType_4x;
        else
        {
            if (_initParam.is_Force_1x_Mode_Lane_0 == RIO_TRUE)
                return CarbonXSrioLinkType_4xForcedTo1xLane0;
            else 
                return CarbonXSrioLinkType_4xForcedTo1xLane2;
        }
    }
}
