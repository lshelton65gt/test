// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_MESSAGE_TRANS_H
#define SRIOX_MESSAGE_TRANS_H

#include "srioXDefines.h"
#include "srioXTrans.h"

/*!
 * \file srioXMessageTrans.h
 * \brief Header file for Data Message Transaction Class
 *
 * This file provides the class definition for Message Transaction objects.
 */

/*!
 * \brief Class to represent Message Transaction.
 *
 */
class CarbonXSrioMessageTrans: public CarbonXSrioTransClass
{
public: CARBONMEM_OVERRIDES

    CarbonXSrioMessage _msg;        //!< Message Request info
    RIO_DW_T *_dwData;              //!< BFM DWORD data
    CarbonXSrioErrorType _error;    //!< Error in constructor

public:

    // Constructor and destructor
    CarbonXSrioMessageTrans(const CarbonXSrioMessage *req);
    CarbonXSrioMessageTrans(RIO_REMOTE_REQUEST_T *request, 
            RIO_TR_INFO_T transportInfo);
    ~CarbonXSrioMessageTrans();

    CarbonXSrioTransType getType() const
    { return CarbonXSrioTrans_MESSAGE; }
    CarbonXSrioErrorType getError() const
    { return _error; }
    
    // Conversion functions
    CarbonXSrioMessage *getMessage() const;

    // Start transation
    SrioxBool start(CarbonXSrioClass *xtor, UInt tag) const;

    // Check if needs response
    SrioxBool needResponse() const { return SrioxTrue; }
};

#endif
