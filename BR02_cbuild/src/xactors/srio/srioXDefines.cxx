/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
 *     File    :  srioXDefines.cpp
 *     Purpose :  Provides implementation of SrioxTrue and SrioxFalse.
 */

#define DEF_BOOL_CONST /* This file actually defines SriollxTrue/False */
#include "srioXDefines.h"

