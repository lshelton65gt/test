// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_DOORBELL_TRANS_H
#define SRIOX_DOORBELL_TRANS_H

#include "srioXDefines.h"
#include "srioXTrans.h"

/*!
 * \file srioXDoorbellTrans.h
 * \brief Header file for Data Doorbell Transaction Class
 *
 * This file provides the class definition for Doorbell Transaction objects.
 */

/*!
 * \brief Class to represent Doorbell Transaction.
 *
 */
class CarbonXSrioDoorbellTrans: public CarbonXSrioTransClass
{
public: CARBONMEM_OVERRIDES

    CarbonXSrioDoorbell _doorbell;      //!< Doorbell Request info
    CarbonXSrioErrorType _error;        //!< Error in constructor

public:

    // Constructor and destructor
    CarbonXSrioDoorbellTrans(const CarbonXSrioDoorbell *req):
        _doorbell(*req), _error(CarbonXSrioError_NoError) {}
    CarbonXSrioDoorbellTrans(RIO_REMOTE_REQUEST_T *request,
            RIO_TR_INFO_T transportInfo);
    ~CarbonXSrioDoorbellTrans() {}

    CarbonXSrioTransType getType() const
    { return CarbonXSrioTrans_DOORBELL; }
    CarbonXSrioErrorType getError() const
    { return _error; }
    
    // Conversion functions
    CarbonXSrioDoorbell *getDoorbell() const;

    // Start transation
    SrioxBool start(CarbonXSrioClass *xtor, UInt tag) const;

    // Check if needs response
    SrioxBool needResponse() const { return SrioxTrue; }
};

#endif
