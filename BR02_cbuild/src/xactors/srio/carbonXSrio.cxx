/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    carbonXSrio.cpp
    Purpose: This file provides definition of creation and connection APIs for
             SRIO transactor object. 
*/

#include "srioXtor.h"

CarbonXSrioID carbonXSrioCreate(const char* transactorName,
        const CarbonXSrioConfig *config, const CarbonUInt32 maxTransfers,
        void (*reportTransaction)(CarbonXSrioTrans*, void*),
        void (*setDefaultResponse)(CarbonXSrioTrans*, void*),
        void (*setResponse)(CarbonXSrioTrans*, void*),
        void (*refreshResponse)(CarbonXSrioTrans*, void*),
        void (*notify)(CarbonXSrioErrorType, const char*,
            CarbonXSrioTrans*, void*),
        void (*notify2)(CarbonXSrioErrorType, const char*, void*),
        void *stimulusInstance)
{
    CarbonXSrioClass *xtor = new CarbonXSrioClass(transactorName, 
            config, maxTransfers);
    if (xtor == NULL)
    {
        CarbonXSrioClass::error(xtor, CarbonXSrioError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    // Check if transactor creation got some error
    CarbonXSrioErrorType error = xtor->getError();
    if (error != CarbonXSrioError_NoError)
    {
        SrioxConstString errorMesg = xtor->getErrorMessage();
        CarbonXSrioClass::error(xtor, error, errorMesg);
        delete xtor;
        xtor = NULL;
        return NULL;
    }

#ifndef _NO_LICENSE
    /* scramble feature name and checkout license */
    CARBON_LICENSE_FEATURE(featureName, "crbn_vsp_exec_xtor_srio", 23);
    xtor->_carbonLicense = UtLicenseWrapperCheckout(featureName,
            "Serial RapidIO Transactor");
#endif

    xtor->registerCallbacks(stimulusInstance, reportTransaction, setResponse, 
            notify2);
    return xtor;

    // to remove compile time warning
    setDefaultResponse = NULL;
    refreshResponse = NULL;
    notify = NULL;
}

CarbonUInt32 carbonXSrioDestroy(CarbonXSrioID xtor)
{
#ifndef _NO_LICENSE
    /* release the license */
    UtLicenseWrapperRelease(&(xtor->_carbonLicense));
#endif

    delete xtor;
    xtor = NULL;
    return 1;
}

const char *carbonXSrioGetName(CarbonXSrioID xtor)
{
    return xtor->getName();
}

void carbonXSrioGetConfig(CarbonXSrioID xtor, CarbonXSrioConfig *config)
{ 
    if (config != NULL)
        *config = *(xtor->getConfig());
}

CarbonUInt32 carbonXSrioReset(CarbonXSrioID xtor)
{
    if (!xtor->reset())
    {
        CarbonXSrioClass::error(xtor, CarbonXSrioError_InternalError,
                "Can't reset transactor.");
        return 0;
    }
    return 1;
}

CarbonUInt32 carbonXSrioIsSpaceInTransactionBuffer(CarbonXSrioID xtor)
{
    return (xtor->canAcceptNewTrans() ? 1 : 0);
}

CarbonUInt32 carbonXSrioStartNewTransaction(CarbonXSrioID xtor,
        CarbonXSrioTrans *trans)
{
    if (!xtor->startNewTransaction(trans))
        return 0;
    return 1;
}

CarbonUInt32 carbonXSrioConnectToLocalSignalMethods(CarbonXSrioID xtor,
        CarbonXInterconnectNameCallbackTrio *nameList)
{
    /* Iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (UInt i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXSignalChangeCB changeCB;
        CarbonXInterconnectNameCallbackTrio pair = nameList[i];
        /* get the transactor signal wrapper */
        SrioxPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        if (pin == NULL)
            return 0;
        if (pin->isOutput()) /* for output signal use models input callback */
            changeCB = pair.ModelInputChangeCB;
        else /* for input signal use models output callback */
            changeCB = pair.ModelOutputChangeCB;

        if (pin->bind(changeCB, pair.UserPointer) != SrioxTrue)
            return 0;
    }
    return 1;
}

CarbonUInt32 carbonXSrioConnectByModelSignalHandle(CarbonXSrioID xtor,
        CarbonXInterconnectNameHandlePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameHandlePair pair = nameList[i];
        /* get the transactor signal wrapper */
        SrioxPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        /* connect the net handle with transactor's respective net */
        if ( pin->bind(carbonModelHandle, pair.ModelSignalHandle) != SrioxTrue)
            return 0; /* Failure in connection */
    }
    return 1;
    carbonModelName = NULL; /* remove warning */
}

CarbonUInt32 carbonXSrioConnectByModelSignalName(CarbonXSrioID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    static char hdlPath[1024];
    CarbonNetID *net;
    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameNamePair pair = nameList[i];
        sprintf(hdlPath, "%s.%s", carbonModelName, pair.ModelSignalName);
        /* get the net handle */
        net = carbonFindNet(carbonModelHandle, hdlPath);
        if (net == NULL)
            return 0;
        /* get the transactor signal wrapper */
        SrioxPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        /* connect the net handle with transactor's respective net */
        if ( pin->bind(carbonModelHandle, net) != SrioxTrue)
            return 0;
    }
    return 1;
}

CarbonUInt32 carbonXSrioEvaluate(CarbonXSrioID xtor, CarbonTime currentTime)
{
#ifndef _NO_LICENSE
    /* tell the license server we are still using the license */
    UtLicenseWrapperHeartbeat(xtor->_carbonLicense);
#endif

    return (xtor->evaluate(currentTime) ? 1 : 0); 
}

CarbonUInt32 carbonXSrioRefreshInputs(CarbonXSrioID xtor,
       CarbonTime currentTime)
{
    return (xtor->refreshInputs(currentTime) ? 1 : 0);
}

CarbonUInt32 carbonXSrioRefreshOutputs(CarbonXSrioID xtor,
        CarbonTime currentTime)
{
    return (xtor->refreshOutputs(currentTime) ? 1 : 0);
}

