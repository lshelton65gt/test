// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_CONFIG_H
#define SRIOX_CONFIG_H

#include "srioXDefines.h"
#include "xactors/srio/carbonXSrioConfig.h"
extern "C" {
#include "rio_serial_model.h"
}

/*!
 * \file srioXConfig.h
 * \brief Header file for configuration class.
 *
 * This file provides the class definition for configuration object.
 */

/*!
 * \brief Class to represent configuration object.
 */
class CarbonXSrioConfigClass {
public: CARBONMEM_OVERRIDES

private:

    RIO_MODEL_INST_PARAM_T _instParam;      //!< BFM instantiation parameter
    RIO_PL_SERIAL_PARAM_SET_T _initParam;   //!< BFM initialization param

public:

    // Constructor and destructor
    CarbonXSrioConfigClass();
    ~CarbonXSrioConfigClass() {}

    // BFM parameter functions
    const RIO_MODEL_INST_PARAM_T *getBfmInstConfig() const
    { return &_instParam; }
    const RIO_PL_SERIAL_PARAM_SET_T *getBfmInitConfig() const
    { return &_initParam; }

    // Member functions
    void setMaxTransSize(UInt maxTransSize);
    void setAddressing(const CarbonXSrioAddressingType addressing);
    CarbonXSrioAddressingType getAddressing() const;
    void setDeviceIdSize(const CarbonXSrioDeviceIdSizeType idSize);
    CarbonXSrioDeviceIdSizeType getDeviceIdSize() const;
    void setLinkType(const CarbonXSrioLinkType linkType);
    CarbonXSrioLinkType getLinkType();
};

#endif
