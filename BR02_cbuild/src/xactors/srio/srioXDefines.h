// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_DEFINES_H
#define SRIOX_DEFINES_H

#include <stdio.h>
#include <string.h>
#include <assert.h>

/*!
 * \file srioXDefines.h
 * \brief Header file for project specific type definitions.
 *
 * This file provides the type definitions to be used globally in the project.
 * Define all local types in this file.
 */

/* Unified type definition for integer and unsigned integer */
/*! \brief Type for integer */
typedef int           Int;
/*! \brief Type of unsigned integer */
typedef unsigned int  UInt;

/* Macros for memory management */
#include "carbon/c_memmanager.h"
/*! \brief Macro to allocate memory */
#define NEW(type) ((type*)carbonmem_malloc(sizeof(type)))
/*! \brief Macro to allocate memory for array */
#define NEWARRAY(type, size) ((type*)carbonmem_malloc(sizeof(type)*(size)))
/*! \brief Macro to free memory */
#define DELETE(ptr) carbonmem_free(ptr)

/* Macros for debug purpose */
/*! \brief Macro to print a variable value */
#define PRINT(var) printf(#var" = %d\n",var)

/* Data types */
/*! \brief String type */
typedef char*         SrioxString;
/*! \brief Constant string type */
typedef const char*   SrioxConstString;
/*! \brief Bool type */
typedef char          SrioxBool;

#include "carbon/carbon_shelltypes.h"
/*! \brief Bit type */
typedef CarbonUInt1   SrioxBit;
/*! \brief 4 bit data type */
typedef CarbonUInt4   SrioxData4b;
/*! \brief 10 bit data type */
typedef CarbonUInt10  SrioxData10b;

/* Definition of true and false
 * Note, macro 'DEF_BOOL_CONST' is defined only in srioLlXDefines.c so that
 * srioLlXDefines.cpp can have the definition while other files will use the
 * extern forward declaration. */
/* Declaration of true and false */
/*! \brief Definition of False */
extern const SrioxBool SrioxFalse;
/*! \brief Definition of True */
extern const SrioxBool SrioxTrue;
#ifdef DEF_BOOL_CONST
/*! \brief Definition of false */
const SrioxBool SrioxFalse = 0;
/*! \brief Definition of true */
const SrioxBool SrioxTrue  = 1;
#endif

/* Declaration for SrioXtor */
class CarbonXSrioClass;
/*! \brief Type for Carbon SRIO Transactor */
typedef class CarbonXSrioClass SrioXtor;

#endif
