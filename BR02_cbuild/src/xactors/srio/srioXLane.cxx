/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXLane.cpp
    Purpose: This file contains the definition of the functions for accessing
             Lane.
*/

#include "srioXLane.h"
#include "srioXtor.h"

SrioxLane::SrioxLane(const SrioXtor *xtor, const UInt id):
    _xtor(xtor), _id(id)
{
    SrioxConstString xtorName = xtor->getName();
    SrioxString laneName = NEWARRAY(char, strlen(xtorName) + 40);
    sprintf(laneName, "%s.%s%d", xtorName, "TLane", id);
    _tlane = new SrioxPin(xtor, laneName, SrioxDirOutput);
    sprintf(laneName, "%s.%s%d", xtorName, "RLane", id);
    _rlane = new SrioxPin(xtor, laneName, SrioxDirInput);
    DELETE(laneName);

    _serializer = new SrioxSerializer(xtor, _rlane);
    _deserializer = new SrioxDeserializer(xtor, _tlane);
}

SrioxLane::~SrioxLane()
{
    delete _tlane;
    _tlane = NULL;
    delete _rlane;
    _rlane = NULL;
    delete _serializer;
    _serializer = NULL;
    delete _deserializer;
    _deserializer = NULL;
}

// Returns output pin handle
SrioxPin* SrioxLane::getTlane()
{
    return _tlane;
}

// Returns input pin handle
SrioxPin* SrioxLane::getRlane()
{
    return _rlane;
}

// Evaluation of deserializer
SrioxBool SrioxLane::transmit(SrioxBit dataBit, CarbonTime currentTime)
{
    return _deserializer->evaluate(dataBit, currentTime);
}

// Evaluation of serializer
SrioxBool SrioxLane::receive(SrioxBit &dataBit, CarbonTime currentTime)
{
    return _serializer->evaluate(dataBit, currentTime);
}

