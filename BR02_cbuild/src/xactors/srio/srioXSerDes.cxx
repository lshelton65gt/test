/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXSerDes.cpp
    Purpose: This file provides the member function definition serializer and 
             deserializer to be used at the bottom of the BFM.
*/

#include "srioXSerDes.h"

SrioxBool SrioxSerializer::evaluate(SrioxBit &serialOut, CarbonTime currentTime)
{
    if (_bitCounter == 0)
    {
        if (_inputPort->read(_shiftReg, currentTime) != SrioxTrue )
            return SrioxFalse;
        _shiftReg &= 0x3FF;
    }
    _bitCounter = (_bitCounter + 1) % 10;
    // MSB to LSB serialization ordering
    serialOut = ((_shiftReg & 0x200) == 0) ? 0x0 : 0x1;
    _shiftReg = (_shiftReg << 1) & 0x3FF;
    return SrioxTrue;
}

SrioxBool SrioxDeserializer::evaluate(SrioxBit serialIn, CarbonTime currentTime)
{
    static const UInt srioxK28p5 = 0x305; // Encoded K28.5 for rd+

    // MSB to LSB deserialization ordering
    _shiftReg = ((_shiftReg << 1) | (serialIn & 0x1)) & 0x3FF;
    _bitCounter = (_bitCounter + 1) % 10;

    // Alignment of deserializer checking K28.5
    if ((_shiftReg == srioxK28p5) || (_shiftReg == ((~srioxK28p5) & 0x3FF)))
    {
        _gotSync = SrioxTrue;
        _bitCounter = 0; //resetting the counter
    }

    // Output the shift register value as parallel data when bit counter is
    // zero and we have already got the sync or align
    if ((_bitCounter == 0) && (_gotSync == SrioxTrue))
    {
        if (_outputPort->write(_shiftReg, currentTime) != SrioxTrue)
            return SrioxFalse;
    }
    return SrioxTrue;
}
