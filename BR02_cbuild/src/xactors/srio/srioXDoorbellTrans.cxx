/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXDoorbellTrans.cpp
    Purpose: This file provides the definition for Doorbell Transaction 
             routines.
*/

#include "srioXDoorbellTrans.h"
#include "srioXtor.h"

CarbonXSrioDoorbellTrans::CarbonXSrioDoorbellTrans(
        RIO_REMOTE_REQUEST_T *request,
        RIO_TR_INFO_T transportInfo):
    _error(CarbonXSrioError_NoError)
{
    assert(request);
    _doorbell.transId = request->header->target_TID;
    _doorbell.info = request->header->doorbell_Info;
    _doorbell.srcId = request->header->src_ID;

    if (request->transp_Type == RIO_PL_TRANSP_16)
        _doorbell.destId = ((transportInfo >> 8) & 0xFF);
    else
        _doorbell.destId = ((transportInfo >> 16) & 0xFFFF);

    _doorbell.prio = request->header->prio;
}

CarbonXSrioDoorbell* CarbonXSrioDoorbellTrans::getDoorbell() const
{
    CarbonXSrioDoorbell *doorbell = NEW(CarbonXSrioDoorbell);
    if (doorbell == NULL)
        return NULL;
    *doorbell = _doorbell;
    
    return doorbell;
}

SrioxBool CarbonXSrioDoorbellTrans::start(CarbonXSrioClass *xtor,
        UInt tag) const
{
    // BFM Doorbell request structure
    RIO_DOORBELL_REQ_T bfmDoorbell;
    bfmDoorbell.prio = _doorbell.prio;
    bfmDoorbell.doorbell_Info = _doorbell.info;
       
    if (xtor->getConfig()->getDeviceIdSize() 
            == CarbonXSrioDeviceIdSize_8)
        bfmDoorbell.transp_Type = RIO_PL_TRANSP_16;
    else
        bfmDoorbell.transp_Type = RIO_PL_TRANSP_32;
    bfmDoorbell.transport_Info = (bfmDoorbell.transp_Type == RIO_PL_TRANSP_16)
        ? ((_doorbell.destId << 8) | _doorbell.srcId)
        : ((_doorbell.destId << 16) | _doorbell.srcId);
    
    return ((xtor->getBfmFtray()).rio_PL_Doorbell_Request
            (xtor->getBfm(), &bfmDoorbell, tag) == RIO_OK);
}


