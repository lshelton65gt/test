/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXIoRequestTrans.cpp
    Purpose: This file provides the class definition for IO Request Transaction
             routines.
*/

#include "srioXIoRequestTrans.h"
#include "srioXtor.h"

CarbonXSrioIoRequestTrans::CarbonXSrioIoRequestTrans(
        const CarbonXSrioIoRequest *req):
    _ioReq(*req), _error(CarbonXSrioError_NoError)
{
    SrioxBool isWriteTrans = isWriteType(_ioReq);
    if (!isValid(_ioReq))
    {
        _error = CarbonXSrioError_InvalidTrans;
        return;
    }

    if (isWriteTrans)
    {
        _dwData = copyToBfmDw(_ioReq.dataSize, req->data);
        if (_dwData == NULL)
        {
            _error = CarbonXSrioError_NoMemory;
            return;
        }
    }
    else
        _dwData = NULL;
}

CarbonXSrioIoRequestTrans::CarbonXSrioIoRequestTrans(
        RIO_REMOTE_REQUEST_T *request, RIO_TR_INFO_T transportInfo):
    _error(CarbonXSrioError_NoError)
{
    assert(request);
    if (request->packet_Type == 2)
    {
        if(request->header->ttype == 4)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_NREAD;
        }
        else if (request->header->ttype == 12)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_ATOMIC_INC;
        }
        else if (request->header->ttype == 13)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_ATOMIC_DEC;
        }
        else if (request->header->ttype == 14)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_ATOMIC_SET;
        }
        else if (request->header->ttype == 15)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_ATOMIC_CLR;
        }
    }
    else if (request->packet_Type == 5)
    {
        if (request->header->ttype == 4)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_NWRITE;
        }
        if (request->header->ttype == 5)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_NWRITE_R;
        }
        if (request->header->ttype == 12)
        {
            _ioReq.reqType = CarbonXSrioIoRequest_ATOMIC_SWAP;
        }
        if (request->header->ttype == 14)
        {
             _ioReq.reqType = CarbonXSrioIoRequest_ATOMIC_TSWP;
        }
    }
    else if (request->packet_Type == 6)
    {
        _ioReq.reqType = CarbonXSrioIoRequest_SWRITE;
    }
    else
    {
        assert(0);
    }

    _ioReq.address = request->header->address;
    _ioReq.extAddress = request->header->extended_Address;
    _ioReq.xamsbs = request->header->xamsbs;
    _ioReq.wdptr = request->header->wdptr;
    _ioReq.transId = request->header->target_TID;
    _ioReq.srcId = request->header->src_ID;
    _ioReq.prio = request->header->prio;
    if (request->transp_Type == RIO_PL_TRANSP_16)
        _ioReq.destId = ((transportInfo >> 8) & 0xFF);
    else
        _ioReq.destId = ((transportInfo >> 16) & 0xFFFF);

    SrioxBool isWriteTrans = isWriteType(_ioReq);
    if (isWriteTrans)
    {
        _ioReq.wrSize = request->header->rdsize;
        _ioReq.rdSize = 0;
    }
    else
    {
        _ioReq.rdSize = request->header->rdsize;
        _ioReq.wrSize = 0;
    }
    _ioReq.dataSize = request->dw_Num;

    if (!isValid(_ioReq))
    {
        _error = CarbonXSrioError_InvalidTrans;
        return;
    }

    if (_ioReq.dataSize > 0)
    {
        _dwData = copyBfmDw(_ioReq.dataSize, request->data);
        if (_dwData == NULL)
        {
            _error = CarbonXSrioError_NoMemory;
            return;
        }
    }
    else
        _dwData = NULL;
}

CarbonXSrioIoRequestTrans::~CarbonXSrioIoRequestTrans()
{
    if (isWriteType(_ioReq) && _dwData != NULL)
        DELETE(_dwData);
}

CarbonXSrioIoRequest* CarbonXSrioIoRequestTrans::getIoRequest() const
{
    CarbonXSrioIoRequest *ioReq = NEW(CarbonXSrioIoRequest);
    if (ioReq == NULL)
        return NULL;
    *ioReq = _ioReq;
    
    SrioxBool isWriteTrans = isWriteType(_ioReq);
    if (isWriteTrans)
    {
        ioReq->data = copyFromBfmDw(_ioReq.dataSize, _dwData);
        if (ioReq->data == NULL)
        {
            DELETE(ioReq);
            return NULL;
        }
    }
    else
        ioReq->data = NULL;
    return ioReq;
}

SrioxBool CarbonXSrioIoRequestTrans::start(CarbonXSrioClass *xtor,
        UInt tag) const
{
    // BFM request structure
    RIO_IO_REQ_T bfmReq;
    switch (_ioReq.reqType)
    {
        case CarbonXSrioIoRequest_NREAD :
        {
            bfmReq.ttype = RIO_IO_NREAD;
            break;
        }
        case CarbonXSrioIoRequest_NWRITE :
        {
            bfmReq.ttype = RIO_IO_NWRITE;
            break;
        }
        case CarbonXSrioIoRequest_SWRITE :
        {
            bfmReq.ttype = RIO_IO_SWRITE;
            break;
        }
        case CarbonXSrioIoRequest_NWRITE_R :
        {
            bfmReq.ttype = RIO_IO_NWRITE_R;
            break;
        }
        case CarbonXSrioIoRequest_ATOMIC_INC :
        {
            bfmReq.ttype = RIO_IO_ATOMIC_INC;
            break;
        }
        case CarbonXSrioIoRequest_ATOMIC_DEC :
        {
            bfmReq.ttype = RIO_IO_ATOMIC_DEC;
            break;
        }
        case CarbonXSrioIoRequest_ATOMIC_TSWP :
        {
            bfmReq.ttype = RIO_IO_ATOMIC_TSWAP;
            break;
        }
        case CarbonXSrioIoRequest_ATOMIC_SET :
        {
            bfmReq.ttype = RIO_IO_ATOMIC_SET;
            break;
        }
        case CarbonXSrioIoRequest_ATOMIC_CLR :
        {
            bfmReq.ttype = RIO_IO_ATOMIC_CLEAR;
            break;
        }
        case CarbonXSrioIoRequest_ATOMIC_SWAP :
        {
            bfmReq.ttype = RIO_IO_ATOMIC_SWAP;
            break;
        }
        default:
        {
            assert(0);
        }
    }

    // conversion from wdptr & rdSize/wrSize to byte_Num/offset
    SrioxBool isWriteTrans = isWriteType(_ioReq);
    if (_ioReq.reqType != CarbonXSrioIoRequest_SWRITE)
    {
        UInt byteSize = getByteNum(_ioReq.wdptr,
                isWriteTrans ? _ioReq.wrSize : _ioReq.rdSize);
        bfmReq.offset = getByteOffset(_ioReq.wdptr,
                isWriteTrans ? _ioReq.wrSize : _ioReq.rdSize);
        if (byteSize <= 8)
        {
            bfmReq.dw_Size = 1;
            bfmReq.byte_Num = byteSize;
        }
        else
        {
            bfmReq.dw_Size = byteSize / 8;
            bfmReq.byte_Num = 0;
        }
    }
    else
    {
        bfmReq.byte_Num = (_ioReq.dataSize == 1) ? 8 : 0;
        bfmReq.offset = 0;
        bfmReq.dw_Size = _ioReq.dataSize;
    }

    bfmReq.address = _ioReq.address;
    bfmReq.extended_Address = _ioReq.extAddress;
    bfmReq.xamsbs = _ioReq.xamsbs;
    bfmReq.data = isWriteTrans ? _dwData : NULL;
    bfmReq.prio = _ioReq.prio;

    if (xtor->getConfig()->getDeviceIdSize() ==  CarbonXSrioDeviceIdSize_8)
        bfmReq.transp_Type = RIO_PL_TRANSP_16;
    else
        bfmReq.transp_Type = RIO_PL_TRANSP_32;

    bfmReq.destId = _ioReq.destId;
    bfmReq.trans_Id = _ioReq.transId;

    RIO_TR_INFO_T transportInfo = (bfmReq.transp_Type == RIO_PL_TRANSP_16)
        ? (_ioReq.destId << 8 | _ioReq.srcId) 
        : (_ioReq.destId << 16 | _ioReq.srcId);

    return ((xtor->getBfmFtray()).rio_PL_IO_Request(
                xtor->getBfm(), &bfmReq, tag, transportInfo) == RIO_OK);
}

SrioxBool CarbonXSrioIoRequestTrans::isWriteType(
        const CarbonXSrioIoRequest &ioReq)
{
    return (ioReq.reqType == CarbonXSrioIoRequest_NWRITE ||
            ioReq.reqType == CarbonXSrioIoRequest_SWRITE ||
            ioReq.reqType == CarbonXSrioIoRequest_NWRITE_R ||
            ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_TSWP ||
            ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_SWAP);
}

SrioxBool CarbonXSrioIoRequestTrans::needResponse() const
{
    // this type of transactions require a response in return 
    return (_ioReq.reqType == CarbonXSrioIoRequest_NREAD ||
            _ioReq.reqType == CarbonXSrioIoRequest_NWRITE_R ||
            _ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_INC ||
            _ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_DEC ||
            _ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_TSWP ||
            _ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_SET ||
            _ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_CLR ||
            _ioReq.reqType == CarbonXSrioIoRequest_ATOMIC_SWAP);
}

SrioxBool CarbonXSrioIoRequestTrans::isValid(const CarbonXSrioIoRequest &ioReq)
{
    // For read transactions dataSize should be 0
    if (!isWriteType(ioReq))
        return (ioReq.dataSize == 0);

    // For write transactions where dataSize > 0
    UInt maxDataSize = 32;
    // Only for SWRITE transaction, Max data size should be 32
    if (ioReq.reqType != CarbonXSrioIoRequest_SWRITE)
        maxDataSize = getDwordNum(ioReq.wdptr, ioReq.wrSize);
    if (ioReq.dataSize == 0)
        return SrioxFalse;
    if (ioReq.dataSize > maxDataSize)
        return SrioxFalse;
    return SrioxTrue;
}

/*
   This routine returns the number of DWORDs if wdptr and rdSize/wrSize
   passed as argument.
*/
UInt CarbonXSrioIoRequestTrans::getDwordNum(SrioxBit wdptr, SrioxData4b rdSize)
{
    UInt dwNum = getByteNum(wdptr, rdSize);
    if (dwNum <= 8) // conevrting from byte to dword
        dwNum = 1;
    else
        dwNum /= 8;
    return dwNum;
}

/*
   This routine returns the number of bytes that will be read/written, 
   from the wdptr and rdSize/wrSize value, passed as argument.
   This table can be found in the SRIO IO Logical Specification.
*/
UInt CarbonXSrioIoRequestTrans::getByteNum(SrioxBit wdptr, SrioxData4b rdSize)
{
    static const UInt srioxConvByteNumArray[32] = {
        1, 1, 1, 1, 2, 3, 2, 5, 4, 6, 7, 8, 32, 96, 160, 224,
        1, 1, 1, 1, 2, 3, 2, 5, 4, 6, 7, 16, 64, 128, 192, 256
    };
    return srioxConvByteNumArray[((wdptr << 4) | rdSize) & 0x1F];
}

/*
   This routine returns the Byte offset value from the wdptr and 
   rdSize/wrSize value, passed as argument.This table can be found in
   the SRIO IO Logical Specification.
*/
UInt CarbonXSrioIoRequestTrans::getByteOffset(
        SrioxBit wdptr, SrioxData4b rdSize)
{
    static const UInt srioxConvByteOffsetArray[32] = {
        0, 1, 2, 3, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        4, 5, 6, 7, 4, 5, 6, 3, 4, 2, 1, 0, 0, 0, 0, 0
    };
    return srioxConvByteOffsetArray[((wdptr << 4) | rdSize) & 0x1F];
}

