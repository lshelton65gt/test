// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_XTOR_H
#define SRIOX_XTOR_H

#include "srioXDefines.h"
#include "srioXLane.h"
#include "srioXConfig.h"
#include "srioXTrans.h"
#include "xactors/srio/carbonXSrio.h"
extern "C" {
#include "rio_serial_model.h"
}
#ifndef _NO_LICENSE
#include "util/UtCLicense.h"
#endif

/*!
 * \file srioXtor.h
 * \brief Header file for transactor definition
 * 
 * This file provides the definition of the class of transactor and 
 * declares the functions used to update the BFM and to make a transaction.
 */

/*!
 * \brief This class defines the transactor.
 *
 * This class contains the functions for initializing and updating the 
 * transactor and for compliting a transaction.
 *
 */
class CarbonXSrioClass {
public: CARBONMEM_OVERRIDES

private:

    SrioxString _name;                      //!< Name of the transactor
    const UInt _id;                         //!< Identity no. of the transactor
    static UInt _xtorCount;                 //!< Count of transactor object
    CarbonXSrioConfig *_config;             //!< Pointer to the configuration 
                                            //   object for this transactor
    SrioxLane *_lanes[4];                   //!< Container of SERDES and pins
    RIO_HANDLE_T _bfm;                      //!< BFM
    RIO_PL_SERIAL_MODEL_FTRAY_T _bfmFtray;  //!< BFM Function Tray
    CarbonTime _currentTime;                //!< Current simulation time
    UInt _nextTransTag;                     //!< Tag of next transaction
    CarbonXSrioTrans *_lastTransToAck;      //!< Last transaction to acknowledge
    CarbonXSrioErrorType _error;            //!< Holds the type of error 
                                            //   encountered
    SrioxConstString _errorMessage;         //!< Corresponding error message

    //! Callback functions
    //! Callback to acknowledge an initiated transaction
    void (*_acknowledgeTransCallback)(CarbonXSrioTrans*, void*);
    //! Callback when a transaction has been received
    void (*_remoteTransCallback)(CarbonXSrioTrans*, void*);
    //! Callback when error happened for a transaction
    void (*_errorCallback)(CarbonXSrioErrorType, const char*, void*);
    //! Some user data to provide the callback functions as argument
    void *_userContext;

public:
#ifndef _NO_LICENSE
    /* Carbon licensing parameter */
    CarbonLicenseData *_carbonLicense;
#endif

public:

    //! Constructor
    CarbonXSrioClass(SrioxConstString name, const CarbonXSrioConfig *config,
            CarbonUInt32 transQueueSize);
    //! Destructor
    ~CarbonXSrioClass();
    
    //! This function returns transactor's name
    SrioxConstString getName() const { return _name;}
    //! This function returns BFM
    RIO_HANDLE_T getBfm() { return _bfm;}
    //! This function returns BFM Ftray
    RIO_PL_SERIAL_MODEL_FTRAY_T &getBfmFtray() { return _bfmFtray;}
    //! Returns the current error encountered
    CarbonXSrioErrorType getError() const { return _error; }
    //! Returns the string message for current error encountered
    SrioxConstString getErrorMessage() const { return _errorMessage; }
    //! Returns if transactor can accept new transaction
    SrioxBool canAcceptNewTrans() const { return (_lastTransToAck == NULL); }

    //! Returns the current configuration
    CarbonXSrioConfig* getConfig() const {return _config;}

    //! Given the pin name this function returns the pin handle
    SrioxPin* getPinByName(SrioxConstString netName);

    //! Callback registation function
    void registerCallbacks(void *userContext,
            void (*acknowledgeTransCallback)(CarbonXSrioTrans*, void*),
            void (*remoteTransCallback)(CarbonXSrioTrans*, void*),
            void (*errorCallback)(CarbonXSrioErrorType, const char*, void*));

    //! Resets the transactor
    CarbonUInt32 reset();
    
    //! This routine evaluate the serializers in each lane and update the 
    //  BFM by setting the rlane values of BFM and providing it a clock edge
    SrioxBool evaluate(CarbonTime currentTime);
    
    //! updates the rlane values in each lane  
    SrioxBool refreshInputs(CarbonTime currentTime);
    //! updates the tlane values in each lane
    SrioxBool refreshOutputs(CarbonTime currentTime);
    
    //! Function for setting the error type encountered
    static void error(const CarbonXSrioClass *xtor,
            CarbonXSrioErrorType errorType, 
            SrioxConstString errorMessage = NULL);

    //! Starts a new transaction
    CarbonUInt32 startNewTransaction(CarbonXSrioTrans *trans);

    // ****** CALLBACK ROUTINES FOR BFM *******

    //! Function for binding BFM with callbacks
    SrioxBool bindBfmCallbackTray();

    //! This function is called by BFM via rio_PL_Ack_Request callback and it
    //  invokes the registred callback function _acknowledgeTransCallback
    static int bfmCallbackAckRequest(RIO_CONTEXT_T context, RIO_TAG_T tag);
    
    //! BFM calls this function via rio_PL_Ack_Response callback and it
    //  invokes the registered callback function _acknowledgeTransCallback
    static int bfmCallbackAckResponse(RIO_CONTEXT_T context, RIO_TAG_T tag);

    //! BFM calls this function via rio_PL_Remote_Request callback and this 
    //  function in turn invokes _remoteTransCallback and also call the
    //  rio_PL_Ack_Remote_Req function    
    static int bfmCallbackRemoteRequest(RIO_CONTEXT_T context, RIO_TAG_T tag,
            RIO_REMOTE_REQUEST_T *request, RIO_TR_INFO_T transportInfo);

    //! This function is called by BFM via rio_PL_Remote_Response callback and
    //  it in turn calls the registered callback function _remoteTransCallback
    static int bfmCallbackRemoteResponse(RIO_CONTEXT_T context, RIO_TAG_T tag,
                    RIO_RESPONSE_T *response);

    //! This function is called via RIO_PL_Set_Pins callback and it evaluates 
    //  the deserializers in each lane by providing the values of tlane of BFM
    static int bfmCallbackSetPins(RIO_CONTEXT_T context,
            RIO_SIGNAL_T tlane0, RIO_SIGNAL_T tlane1,
            RIO_SIGNAL_T tlane2, RIO_SIGNAL_T tlane3);
    
    /* Redundant callbacks */
    static int bfmCallbackGetReqDataB(RIO_CONTEXT_T context, RIO_TAG_T tag,
            RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data);
    
    static int bfmCallbackGetReqDataHwB(RIO_CONTEXT_T context, RIO_TAG_T tag,
            RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_HW_T *data);    

    static int bfmCallbackGetResponseDataB(RIO_CONTEXT_T context,
            RIO_TAG_T tag, RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data);

    static int bfmCallbackRequestTimeOutB(RIO_CONTEXT_T context, RIO_TAG_T tag);

    static int bfmCallbackUserMessageB(RIO_CONTEXT_T  context, char *user_Msg);

    static int bfmCallbackExtendedRegReadB(RIO_CONTEXT_T context,
            RIO_CONF_OFFS_T config_Offset, unsigned long *data);

    static int bfmCallbackExtendedRegWriteB(RIO_CONTEXT_T context,
            RIO_CONF_OFFS_T config_Offset, unsigned long data);

    static int bfmCallbackRequestReceivedB(RIO_CONTEXT_T context,
            RIO_REMOTE_REQUEST_T *packet_Struct);

    static int bfmCallbackResponseReceivedB(RIO_CONTEXT_T context,
            RIO_RESPONSE_T *resp);
};

#endif

