// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_IO_REQUEST_TRANS_H
#define SRIOX_IO_REQUEST_TRANS_H

#include "srioXDefines.h"
#include "srioXTrans.h"

/*!
 * \file srioXIoRequestTrans.h
 * \brief Header file for IO Request Transaction Class.
 *
 * This file provides the class definition for IO Request Transaction objects.
 */

/*!
 * \brief Class to represent IO Request Transaction.
 *
 */
class CarbonXSrioIoRequestTrans: public CarbonXSrioTransClass
{
public: CARBONMEM_OVERRIDES

    CarbonXSrioIoRequest _ioReq;    //!< IO Request info
    RIO_DW_T *_dwData;              //!< BFM DWORD data
    CarbonXSrioErrorType _error;    //!< Error in constructor

public:

    // Constructor and destructor
    CarbonXSrioIoRequestTrans(const CarbonXSrioIoRequest *req);
    CarbonXSrioIoRequestTrans(RIO_REMOTE_REQUEST_T *request,
            RIO_TR_INFO_T transportInfo);
    ~CarbonXSrioIoRequestTrans();

    CarbonXSrioTransType getType() const
    { return CarbonXSrioTrans_IO_REQUEST; }
    CarbonXSrioErrorType getError() const
    { return _error; }
    
    // Conversion functions
    CarbonXSrioIoRequest *getIoRequest() const;

    // Start transation
    SrioxBool start(CarbonXSrioClass *xtor, UInt tag) const;

    // Check if needs response
    SrioxBool needResponse() const;

    // Utility functions
    static SrioxBool isWriteType(const CarbonXSrioIoRequest &ioReq);
    static SrioxBool isValid(const CarbonXSrioIoRequest &ioReq);
    static UInt getDwordNum(SrioxBit wdptr, SrioxData4b rdSize);
    static UInt getByteNum(SrioxBit wdptr, SrioxData4b rdSize);
    static UInt getByteOffset(SrioxBit wdptr, SrioxData4b rdSize);
};

#endif
