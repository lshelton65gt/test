/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    carbonXSrioTrans.cpp
    Purpose: This file provides definition of APIs for transaction object.
*/

#include "xactors/srio/carbonXSrioTrans.h"
#include "srioXIoRequestTrans.h"
#include "srioXMessageTrans.h"
#include "srioXDoorbellTrans.h"
#include "srioXResponseTrans.h"
#include "srioXtor.h"

CarbonXSrioTransType carbonXSrioGetTransType(const CarbonXSrioTrans *trans)
{
    return trans->getType();
}

CarbonXSrioTrans *carbonXSrioCreateIoRequestTrans(
                            const CarbonXSrioIoRequest *req)
{
    CarbonXSrioTrans *trans = new CarbonXSrioIoRequestTrans(req);
    return (CarbonXSrioTrans::checkError(trans) ? NULL : trans);
}

CarbonXSrioIoRequest *carbonXSrioGetIoRequest(const CarbonXSrioTrans *trans)
{
    CarbonXSrioIoRequest *req = trans->getIoRequest();
    if (req == NULL)
    {
        CarbonXSrioClass::error(NULL, CarbonXSrioError_NoMemory, 
              "Can't allocate memory for request structure.");
        return NULL;
    }
    return req;
}

CarbonUInt6 carbonXSrioGetIoRequestDataSize(const CarbonXSrioIoRequest *req)
{
    return (CarbonUInt6) CarbonXSrioIoRequestTrans::getDwordNum(req->wdptr,
            CarbonXSrioIoRequestTrans::isWriteType(*req) ?
            req->wrSize : req->rdSize);
}

CarbonUInt32 carbonXSrioDestroyIoRequest(CarbonXSrioIoRequest *iorequest)
{
    if (iorequest == NULL)
        return 0;
    if (iorequest->dataSize > 0 && iorequest->data != NULL)
        DELETE(iorequest->data);
    DELETE(iorequest);
    return 1;
}

CarbonXSrioTrans *carbonXSrioCreateMessageTrans(
                            const CarbonXSrioMessage *req)
{
    CarbonXSrioTrans *trans = new CarbonXSrioMessageTrans(req);
    return (CarbonXSrioTrans::checkError(trans) ? NULL : trans);
}

CarbonXSrioMessage *carbonXSrioGetMessage(const CarbonXSrioTrans *trans)
{
    CarbonXSrioMessage *req = trans->getMessage();
    if (req == NULL)
    {
        CarbonXSrioClass::error(NULL, CarbonXSrioError_NoMemory,
              "Can't allocate memory for request structure.");
        return NULL;
    }
    return req;
}

CarbonUInt32 carbonXSrioDestroyMessage(CarbonXSrioMessage *message)
{
    if (message == NULL)
        return 0;
    if (message->ssize > 0 && message->data != NULL)
        DELETE(message->data);
    DELETE(message);
    return 1;
}

CarbonXSrioTrans *carbonXSrioCreateDoorbellTrans(
                            const CarbonXSrioDoorbell *req)
{
    CarbonXSrioTrans *trans = new CarbonXSrioDoorbellTrans(req);
    return (CarbonXSrioTrans::checkError(trans) ? NULL : trans);
}

CarbonXSrioDoorbell *carbonXSrioGetDoorbell(const CarbonXSrioTrans *trans)
{
    CarbonXSrioDoorbell *req = trans->getDoorbell();
    if (req == NULL)
    {
        CarbonXSrioClass::error(NULL, CarbonXSrioError_NoMemory,
              "Can't allocate memory for request structure.");
        return NULL;
    }
    return req;
}

CarbonUInt32 carbonXSrioDestroyDoorbell(CarbonXSrioDoorbell *doorbell)
{
    if (doorbell == NULL)
        return 0;
    DELETE(doorbell);
    return 1;
}

CarbonXSrioTrans *carbonXSrioCreateResponseTrans(
                            const CarbonXSrioResponse *resp)
{
    CarbonXSrioTrans *trans = new CarbonXSrioResponseTrans(resp);
    return (CarbonXSrioTrans::checkError(trans) ? NULL : trans);
}

CarbonXSrioResponse *carbonXSrioGetResponse(const CarbonXSrioTrans *trans)
{
    CarbonXSrioResponse *resp = trans->getResponse();
    if (resp == NULL)
    {
        CarbonXSrioClass::error(NULL, CarbonXSrioError_NoMemory, 
              "Can't allocate memory for request structure.");
        return NULL;
    }
    return resp;
}

CarbonUInt32 carbonXSrioDestroyResponse(CarbonXSrioResponse *resp)
{
    if (resp == NULL)
        return 0;
    if (resp->dataSize > 0 && resp->data != NULL)
        DELETE(resp->data);
    DELETE(resp);
    return 1;
}

CarbonUInt32 carbonXSrioTransDestroy(CarbonXSrioTrans *trans)
{
    delete trans;
    trans = NULL;
    return 1;
}

