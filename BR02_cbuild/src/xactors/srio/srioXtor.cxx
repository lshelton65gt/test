/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXtor.cpp
    Purpose: This file provides the definition of the class methods of 
             transactor that are used to update the BFM and to make 
             a transaction.
*/

#include <stdlib.h>
#include "srioXIoRequestTrans.h"
#include "srioXMessageTrans.h"
#include "srioXDoorbellTrans.h"
#include "srioXResponseTrans.h"
#include "srioXtor.h"

UInt CarbonXSrioClass::_xtorCount = 0;

CarbonXSrioClass::CarbonXSrioClass(SrioxConstString name,
        const CarbonXSrioConfig *config, CarbonUInt32 transQueueSize):
    _name(NULL), _id(_xtorCount++), _config(NULL), _bfm(NULL),
    _currentTime(0), _nextTransTag(0), _lastTransToAck(NULL),
    _error(CarbonXSrioError_NoError), _errorMessage(NULL),
    _acknowledgeTransCallback(NULL), _remoteTransCallback(NULL),
    _errorCallback(NULL), _userContext(NULL)
{
    UInt i;
    for (i = 0; i < 4; i++)
        _lanes[i] = NULL;

    // Copy the name
    _name = NEWARRAY(char, strlen(name) + 1);
    if (_name == NULL)
    {
        _error = CarbonXSrioError_NoMemory;
        _errorMessage = "Can't allocate memory for transactor name.";
        return;
    }
    strcpy(_name, name);

    for (i = 0; i < 4; i++)
    {
        _lanes[i] = new SrioxLane(this, i);
        if (_lanes[i] == NULL)
        {
            _error = CarbonXSrioError_NoMemory;
            _errorMessage = "Can't allocate memory for transactor lanes.";
            return;
        }
    }

    // Create default configuration
    if (config == NULL)
        _config = new CarbonXSrioConfig();
    else
        _config = new CarbonXSrioConfig(*config);
    if (_config == NULL)
    {
        _error = CarbonXSrioError_NoMemory;
        _errorMessage = "Can't allocate memory for transactor configuration.";
        return;
    }
    if (transQueueSize < 1)
        transQueueSize = 1;
    else if (transQueueSize > 127)
        transQueueSize = 127;
    _config->setMaxTransSize(transQueueSize);

    // Create BFM
    if (RIO_PL_Serial_Model_Create_Instance(&_bfm,
                const_cast<RIO_MODEL_INST_PARAM_T*>(
                    _config->getBfmInstConfig())) != RIO_OK)
    {
        _error = CarbonXSrioError_InternalError;
        _errorMessage = "Error in BFM instantiation.";
        return;
    }

    // Get BFM function tray
    if (RIO_PL_Serial_Model_Get_Function_Tray(&_bfmFtray) != RIO_OK)
    {
        _error = CarbonXSrioError_InternalError;
        _errorMessage = "Error in BFM function tray retrieval.";
        return;
    }

    // Bind with callback tray
    if (!bindBfmCallbackTray())
    {
        _error = CarbonXSrioError_InternalError;
        _errorMessage = "Error in BFM callback tray binding.";
        return;
    }

    // Initialize BFM
    if (_bfmFtray.rio_PL_Serial_Model_Initialize(_bfm,
                const_cast<RIO_PL_SERIAL_PARAM_SET_T*>(
                    _config->getBfmInitConfig())) != RIO_OK)
    {
        _error = CarbonXSrioError_InternalError;
        _errorMessage = "Error in BFM initialization.";
        return;
    }
}

CarbonXSrioClass::~CarbonXSrioClass()
{
    DELETE(_name);
    delete _config;
    _config = NULL;
    for (UInt i = 0; i < 4; i++)
    {
        delete _lanes[i];
        _lanes[i] = NULL;
    }

    if (_bfm != NULL)
        _bfmFtray.rio_PL_Delete_Instance(_bfm);
}

CarbonUInt32 CarbonXSrioClass::reset()
{
    // Resetting the BFM
    if (_bfmFtray.rio_PL_Model_Start_Reset(_bfm) != RIO_OK)
    {
        error(this, CarbonXSrioError_InternalError, 
            "Error in BFM reset process");
        return 0;
    }
    else
    {
        printf("***** CarbonXSrio Transactor %s resetted\n", _name);
    }
    // Initialization of the BFM after reset
    if (_bfmFtray.rio_PL_Serial_Model_Initialize(_bfm,
                const_cast<RIO_PL_SERIAL_PARAM_SET_T*>(
                    _config->getBfmInitConfig())) != RIO_OK)
    {
        error(this, CarbonXSrioError_InternalError,
            "Error in BFM initialization.");
        return 0;
    }
    return 1; 
}

CarbonUInt32 CarbonXSrioClass::startNewTransaction(CarbonXSrioTrans *trans)
{
    // Check if transaction is pushable
    if (_lastTransToAck != NULL)
        return 0;

    // Send the transaction
    _lastTransToAck = trans;
    if (trans->start(this, _nextTransTag))
    {
        _nextTransTag = (_nextTransTag + 1) % 32;
        return 1;
    }

    // For unsuccessful transmission
    _lastTransToAck = NULL;
    error(this, CarbonXSrioError_InternalError,
        "Error in starting transaction.");
    return 0;
}

/*!
 * 1. Evaluate Serializers to get rlane bits
 * 2. BFM::RIO_PL_Serial_Get_Pins - Sets BFM's rlaneX bits
 * 3. BFM::RIO_PL_Clock_Edge - Evalutes BFM
 *   - This will call BFM::RIO_PL_Set_Pins callback
 *
 * We need to execute above steps 10 times for each transactor evaluation.
 */
SrioxBool CarbonXSrioClass::evaluate(CarbonTime currentTime)
{
    _currentTime = currentTime;
    SrioxBool status = SrioxTrue;
    for (UInt clockTick = 0; clockTick < 10; clockTick++)
    {
        RIO_SIGNAL_T rlanes[4];
        for (UInt i = 0; i < 4; i++)
        {
            SrioxBit rlaneX;
            status &= _lanes[i]->receive(rlaneX, currentTime);
            rlanes[i] = rlaneX ? RIO_SIGNAL_1 : RIO_SIGNAL_0;
        }
        status &= (_bfmFtray.rio_PL_Serial_Get_Pins(_bfm,
                rlanes[0], rlanes[1], rlanes[2], rlanes[3]) == RIO_OK);
        status &= (_bfmFtray.rio_PL_Clock_Edge(_bfm) == RIO_OK);
    }
    return status;
}

SrioxBool CarbonXSrioClass::refreshInputs(CarbonTime currentTime)
{
    SrioxBool status = SrioxTrue;
    for (UInt i = 0; i < 4; i++)
        status &= _lanes[i]->getRlane()->refresh(currentTime);
    return status;
}

SrioxBool CarbonXSrioClass::refreshOutputs(CarbonTime currentTime)
{
    SrioxBool status = SrioxTrue;
    for (UInt i = 0; i < 4; i++)
        status &= _lanes[i]->getTlane()->refresh(currentTime);
    return status;
}

SrioxPin* CarbonXSrioClass::getPinByName(SrioxConstString netName)
{
    if (strcmp(netName, "TLane0") == 0)
        return _lanes[0]->getTlane();
    else if (strcmp(netName, "TLane1") == 0)
        return _lanes[1]->getTlane();
    else if (strcmp(netName, "TLane2") == 0)
        return _lanes[2]->getTlane();
    else if (strcmp(netName, "TLane3") == 0)
        return _lanes[3]->getTlane();
    else if (strcmp(netName, "RLane0") == 0)
        return _lanes[0]->getRlane();
    else if (strcmp(netName, "RLane1") == 0)
        return _lanes[1]->getRlane();
    else if (strcmp(netName, "RLane2") == 0)
        return _lanes[2]->getRlane();
    else if (strcmp(netName, "RLane3") == 0)
        return _lanes[3]->getRlane();
    else
        return NULL;
}

void CarbonXSrioClass::registerCallbacks(void *userContext,
        void (*acknowledgeTransCallback)(CarbonXSrioTrans*, void*),
        void (*remoteTransCallback)(CarbonXSrioTrans*, void*),
        void (*errorCallback)(CarbonXSrioErrorType, const char*, void*))
{
    _userContext = userContext;
    _acknowledgeTransCallback = acknowledgeTransCallback;
    _remoteTransCallback = remoteTransCallback;
    _errorCallback = errorCallback;
}

void CarbonXSrioClass::error(const CarbonXSrioClass *xtor,
        CarbonXSrioErrorType errorType, SrioxConstString errorMessage)
{
    if (xtor != NULL)
    {
        if (xtor->_errorCallback)
            xtor->_errorCallback(errorType, errorMessage, xtor->_userContext);
        else if (xtor->_name != NULL)
        {
            printf("CarbonXSrio: ERROR[%d]: Transactor %s: %s\n",
                    (UInt) errorType, xtor->_name, errorMessage);
            exit(1);
        }
        else
        {
            printf("CarbonXSrio: ERROR[%d]: %s\n",
                    (UInt) errorType, errorMessage);
            exit(1);
        }
    }
    else
    {
        printf("CarbonXSrio: ERROR[%d]: %s\n",
                (UInt) errorType, errorMessage);
        exit(1);
    }
}

SrioxBool CarbonXSrioClass::bindBfmCallbackTray()
{
    // Bind BFM with callbacks
    RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T ctray;
    ctray.rio_PL_Ack_Request = bfmCallbackAckRequest;
    ctray.rio_PL_Get_Req_Data = bfmCallbackGetReqDataB;
    ctray.rio_PL_Get_Req_Data_Hw = bfmCallbackGetReqDataHwB;
    ctray.rio_PL_Ack_Response = bfmCallbackAckResponse;
    ctray.rio_PL_Get_Resp_Data = bfmCallbackGetResponseDataB;
    ctray.rio_PL_Req_Time_Out = bfmCallbackRequestTimeOutB;
    ctray.rio_PL_Remote_Request = bfmCallbackRemoteRequest;
    ctray.rio_PL_Remote_Response = bfmCallbackRemoteResponse;
    ctray.pl_Interface_Context = this;
    ctray.rio_PL_Set_Pins = bfmCallbackSetPins;
    ctray.lp_Serial_Interface_Context = this;
    ctray.rio_User_Msg = bfmCallbackUserMessageB;;
    ctray.rio_Msg = this;
    ctray.extend_Reg_Read = bfmCallbackExtendedRegReadB;
    ctray.extend_Reg_Write = bfmCallbackExtendedRegWriteB;
    ctray.extend_Reg_Context = this;
    ctray.rio_Serial_Granule_To_Send = NULL;
    ctray.rio_Serial_Granule_Received = NULL;
    ctray.rio_Serial_Character_Column_To_Send = NULL;
    ctray.rio_Serial_Character_Column_Received = NULL;
    ctray.rio_Serial_Codegroup_Column_To_Send = NULL;
    ctray.rio_Serial_Codegroup_Received = NULL;
    ctray.rio_Serial_Hooks_Context = this;
    ctray.rio_Request_Received = bfmCallbackRequestReceivedB;
    ctray.rio_Response_Received = bfmCallbackResponseReceivedB;
    ctray.rio_Symbol_To_Send = NULL;
    ctray.rio_Packet_To_Send = NULL;
    ctray.rio_Hooks_Context = this;
    ctray.rio_PL_FC_To_Send = NULL;

    if (RIO_PL_Serial_Model_Bind_Instance(_bfm, &ctray) != RIO_OK)
    {
        return SrioxFalse;
    }
    return SrioxTrue;
}

int CarbonXSrioClass::bfmCallbackAckRequest(RIO_CONTEXT_T context,
        RIO_TAG_T tag)
{
    CarbonXSrioClass *xtor = static_cast<CarbonXSrioClass*>(context);
    assert(xtor);
    if (xtor->_lastTransToAck == NULL)
    {
        CarbonXSrioClass::error(xtor, CarbonXSrioError_InvalidTrans);
        return RIO_ERROR;
    }
    if (xtor->_acknowledgeTransCallback)
        xtor->_acknowledgeTransCallback(xtor->_lastTransToAck,
                xtor->_userContext);
    xtor->_lastTransToAck = NULL;
    return RIO_OK;
    // to remove compile time warning
    tag = 0;
}

int CarbonXSrioClass::bfmCallbackAckResponse(RIO_CONTEXT_T context,
        RIO_TAG_T tag)
{
    CarbonXSrioClass *xtor = static_cast<CarbonXSrioClass*>(context);
    assert(xtor);
    if (xtor->_lastTransToAck == NULL)
    {
        CarbonXSrioClass::error(xtor, CarbonXSrioError_InvalidTrans);
        return RIO_ERROR;
    }
    if (xtor->_acknowledgeTransCallback)
        xtor->_acknowledgeTransCallback(xtor->_lastTransToAck,
                xtor->_userContext);
    xtor->_lastTransToAck = NULL;
    return RIO_OK;
    // to remove compile time warning
    tag = 0;
}

int CarbonXSrioClass::bfmCallbackRemoteRequest(RIO_CONTEXT_T context,
        RIO_TAG_T tag, RIO_REMOTE_REQUEST_T *request,
        RIO_TR_INFO_T transportInfo)
{
    CarbonXSrioTrans *trans;
    CarbonXSrioClass *xtor = static_cast<CarbonXSrioClass*>(context);
    assert(xtor);

    if (request->packet_Type == 11)
    {
        trans = new CarbonXSrioMessageTrans(request, transportInfo);
        if (CarbonXSrioTrans::checkError(trans))
            return RIO_OK;
    }
    else if (request->packet_Type == 10)
    {
        trans = new CarbonXSrioDoorbellTrans(request, transportInfo);
        if (CarbonXSrioTrans::checkError(trans))
            return RIO_OK;
    }
    else if (request->packet_Type == 2 || request->packet_Type == 5 ||
            request->packet_Type == 6)
    {
        trans = new CarbonXSrioIoRequestTrans(request, transportInfo);
        if (CarbonXSrioTrans::checkError(trans))
            return RIO_OK;
    }
    else
    {
        CarbonXSrioClass::error(xtor, CarbonXSrioError_InvalidTrans,
                "Invalid request received.");
        return RIO_ERROR;
    }

    if (xtor->_remoteTransCallback)
        xtor->_remoteTransCallback(trans, xtor->_userContext);

    if (trans != NULL)
    {
        delete trans;
        trans = NULL;
    }

    if (xtor->_bfmFtray.rio_PL_Ack_Remote_Req(xtor->_bfm, tag) != RIO_OK)
    {
        CarbonXSrioClass::error(xtor, CarbonXSrioError_InternalError,
                "Error in transaction acknowledgement.");
        return RIO_ERROR;
    }

    return RIO_OK;
    // to remove compile time warning
    tag = 0;
}

int CarbonXSrioClass::bfmCallbackRemoteResponse(RIO_CONTEXT_T context,
        RIO_TAG_T tag, RIO_RESPONSE_T *response)
{
    CarbonXSrioClass *xtor = static_cast<CarbonXSrioClass*>(context);
    assert(xtor);
    // PRINT(tag);

    CarbonXSrioTrans *trans = new CarbonXSrioResponseTrans(response);
    if (CarbonXSrioTrans::checkError(trans))
        return RIO_OK;

    if (xtor->_remoteTransCallback)
        xtor->_remoteTransCallback(trans, xtor->_userContext);

    if (trans != NULL)
    {
        delete trans;
        trans = NULL;
    }

    return RIO_OK;
    // to remove compile time warning
    tag = 0;
}

/*!
 */
int CarbonXSrioClass::bfmCallbackSetPins(RIO_CONTEXT_T context,
        RIO_SIGNAL_T tlane0, RIO_SIGNAL_T tlane1,
        RIO_SIGNAL_T tlane2, RIO_SIGNAL_T tlane3)
{
    SrioxBool status = SrioxTrue;
    RIO_SIGNAL_T tlanes[4] = { tlane0, tlane1, tlane2, tlane3 };
    CarbonXSrioClass *xtor = static_cast<CarbonXSrioClass*>(context);
    assert(xtor);
    for (UInt i = 0; i < 4; i++)
    {
        SrioxBit tlaneX = (tlanes[i] == RIO_SIGNAL_1) ? 1 : 0;
        status &= xtor->_lanes[i]->transmit(tlaneX, xtor->_currentTime);
    }
    return (status ? RIO_OK : RIO_ERROR);
}

int CarbonXSrioClass::bfmCallbackGetReqDataB(RIO_CONTEXT_T context,
        RIO_TAG_T tag, RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    tag = 0;
    offset = 0;
    cnt = 0;
    data = NULL;
}

int CarbonXSrioClass::bfmCallbackGetReqDataHwB(RIO_CONTEXT_T context,
        RIO_TAG_T tag, RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_HW_T *data)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    tag = 0;
    offset = 0;
    cnt = 0;
    data = NULL;
}

int CarbonXSrioClass::bfmCallbackGetResponseDataB(RIO_CONTEXT_T context,
        RIO_TAG_T tag, RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    tag = 0;
    offset = 0;
    cnt = 0;
    data = NULL;
}

int CarbonXSrioClass::bfmCallbackRequestTimeOutB(RIO_CONTEXT_T context,
        RIO_TAG_T tag)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    tag = 0;
}

int CarbonXSrioClass::bfmCallbackUserMessageB(RIO_CONTEXT_T context,
        char *user_Msg)
{
    return RIO_OK;
    CarbonXSrioClass *xtor = static_cast<CarbonXSrioClass*>(context);
    if (strncmp(user_Msg, "Error", 5) == 0)
    {
        CarbonXSrioClass::error(xtor, CarbonXSrioError_InternalError,
                user_Msg + 5);
    }
}

int CarbonXSrioClass::bfmCallbackExtendedRegReadB(RIO_CONTEXT_T context,
        RIO_CONF_OFFS_T config_Offset, unsigned long *data)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    config_Offset = 0;
    data = NULL;
}

int CarbonXSrioClass::bfmCallbackExtendedRegWriteB(RIO_CONTEXT_T context,
        RIO_CONF_OFFS_T config_Offset, unsigned long data)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    config_Offset = 0;
    data = 0;
}

int CarbonXSrioClass::bfmCallbackRequestReceivedB(RIO_CONTEXT_T context,
        RIO_REMOTE_REQUEST_T *packet_Struct)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    packet_Struct = NULL;
}

int CarbonXSrioClass::bfmCallbackResponseReceivedB(RIO_CONTEXT_T context,
        RIO_RESPONSE_T *resp)
{
    return RIO_OK;
    // to remove compile time warning
    context = NULL;
    resp = NULL;
}

