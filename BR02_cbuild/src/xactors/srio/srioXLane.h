// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_LANE_H
#define SRIOX_LANE_H

#include "srioXSerDes.h"

/*!
 * \file srioXLane.h
 * \brief Header file for transactor definition
 *
 * This file contains the definition of Lane and the functions for accessing
 * the Lane.
 */

/*!
 * \brief This class defines the Lane of a transactor.
 *
 * Each lane consist of --
 *        1. Input Pin that communicate with model for reading 10 bit value
 *        2. Output Pin that communicate with model for writing 10 bit value
 *        3. Serializer for serializing 10 bit value of Input Pin into 1 bit 
 *           for receiving
 *        4. Deserializer for accumulating 1 bit output from BFM and writing
 *           the 10 bit value into Output pin.    
 */
class SrioxLane {
public: CARBONMEM_OVERRIDES

private:

    const SrioXtor *_xtor;             //!< Transactor Handle
    const UInt _id;                    //!< Lane id
    SrioxPin *_tlane;                  //!< Output pin handle
    SrioxPin *_rlane;                  //!< Input pin handle
    SrioxSerializer *_serializer;      //!< Serializer handle
    SrioxDeserializer *_deserializer;  //!< Deserializer handle

public:

    //! Constructor
    SrioxLane(const SrioXtor *xtor, const UInt id);
    //! Destructor
    ~SrioxLane();

    //! Returns output pin handle
    SrioxPin *getTlane();
    //! Returns input pin handle
    SrioxPin *getRlane();

    //! Evaluation of deserializer
    SrioxBool transmit(SrioxBit dataBit, CarbonTime currentTime);
    //! Evaluation of serializer
    SrioxBool receive(SrioxBit &dataBit, CarbonTime currentTime);
};

#endif
