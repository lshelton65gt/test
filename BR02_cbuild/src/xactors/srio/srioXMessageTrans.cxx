/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/* 
    File:    srioXMessageTrans.cpp
    Purpose: This file provides the class definition for Message Transaction 
             routines.
*/
#include "srioXMessageTrans.h"
#include "srioXtor.h"

CarbonXSrioMessageTrans::CarbonXSrioMessageTrans(const CarbonXSrioMessage *req):
    _msg(*req), _error(CarbonXSrioError_NoError)
{     
    if (req->ssize >= 9 && req->ssize <= 14)/* valid ssize value checking */
    {
        _dwData = copyToBfmDw((0x1 << (req->ssize - 9)), req->data);
    }
    else
    {
        _dwData = NULL;
        return;
    }
    if (_dwData == NULL)
        _error = CarbonXSrioError_NoMemory;
}

CarbonXSrioMessageTrans::CarbonXSrioMessageTrans(RIO_REMOTE_REQUEST_T *request,
        RIO_TR_INFO_T transportInfo):
    _error(CarbonXSrioError_NoError)
{
    assert(request);
    _msg.mbox = request->header->mbox;
    _msg.msglen = request->header->msglen;
    if (_msg.msglen == 0)
    {
        _msg.xmbox = request->header->msgseg;
        _msg.msgseg = 0;
    }
    else
    {
        _msg.xmbox = 0;
        _msg.msgseg = request->header->msgseg;
    }
    _msg.letter = request->header->letter;
    _msg.ssize = request->header->ssize;
    _msg.srcId = request->header->src_ID;
    _msg.prio = request->header->prio;

    if (request->transp_Type == RIO_PL_TRANSP_16)
        _msg.destId = ((transportInfo >> 8) & 0xFF);
    else
        _msg.destId = ((transportInfo >> 16) & 0xFFFF);

    if (_msg.ssize >= 9 && _msg.ssize <= 14)/* valid ssize value checking */
    {
        _dwData = copyBfmDw((0x1 << (_msg.ssize - 9)), request->data);
        if (_dwData == NULL)
        { 
            _error = CarbonXSrioError_NoMemory;
            return;
        }
    }
    else
        _dwData = NULL;
}

CarbonXSrioMessageTrans::~CarbonXSrioMessageTrans()
{
    if (_dwData != NULL)
        DELETE(_dwData);
}

CarbonXSrioMessage* CarbonXSrioMessageTrans::getMessage() const
{
    CarbonXSrioMessage *msg = NEW(CarbonXSrioMessage);
    if (msg == NULL)
        return NULL;
    *msg = _msg;
    
    if (_msg.ssize >= 9 && _msg.ssize <= 14)/* valid ssize value checking */
    {
        msg->data = copyFromBfmDw((0x1 << (_msg.ssize - 9)), _dwData);
        if (msg->data == NULL)
        {
            DELETE(msg);
            return NULL;
        }
    }
    else
        msg->data = NULL;
    return msg;
}

SrioxBool CarbonXSrioMessageTrans::start(CarbonXSrioClass *xtor,
        UInt tag) const
{
    /* BFM Message request structure */
    RIO_MESSAGE_REQ_T bfmMsg;
    bfmMsg.prio = _msg.prio;
    bfmMsg.msglen = _msg.msglen;
    bfmMsg.letter = _msg.letter;

    /* checking for single packet message */
    bfmMsg.msgseg = (_msg.msglen) ? _msg.msgseg : _msg.xmbox;

    /* bfmMsg.mbox should be equal to _msg.ssize whether for single packet
       message or multiple packet message as opposed to RapidIo Models ver 2.2 
       user manual $4.2.2.6 from Motorolla */
    bfmMsg.mbox = _msg.mbox;

    /* valid ssize value checking */
    if (_msg.ssize >= 9 && _msg.ssize <= 14)
        bfmMsg.ssize = 0x1 << (_msg.ssize - 9);
    else
        return 0;

    bfmMsg.dw_Size = bfmMsg.ssize;
    bfmMsg.data = _dwData;
    if (xtor->getConfig()->getDeviceIdSize() 
            == CarbonXSrioDeviceIdSize_8)
        bfmMsg.transp_Type = RIO_PL_TRANSP_16;
    else
        bfmMsg.transp_Type = RIO_PL_TRANSP_32;
    
    bfmMsg.transport_Info = (bfmMsg.transp_Type == RIO_PL_TRANSP_16)
        ? ((_msg.destId << 8) | _msg.srcId) 
        : ((_msg.destId << 16) | _msg.srcId);
    
    return ((xtor->getBfmFtray()).rio_PL_Message_Request
            (xtor->getBfm(), &bfmMsg, tag) == RIO_OK);
}
