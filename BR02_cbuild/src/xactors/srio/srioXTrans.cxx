/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:    srioXTrans.cpp
    Purpose: This file provides the class method definition for Common 
             Transaction objects.
*/

#include "srioXTrans.h"
#include "srioXtor.h"

SrioxBool CarbonXSrioTrans::checkError(const CarbonXSrioTrans *trans)
{
    if (trans == NULL)
    {
        CarbonXSrioClass::error(NULL, CarbonXSrioError_NoMemory,
                "Can't allocate memory for transaction.");
        return SrioxTrue;
    }
    CarbonXSrioErrorType error = trans->getError();
    if (error != CarbonXSrioError_NoError)
    {
        SrioxConstString errorMesg = (error == CarbonXSrioError_NoMemory) ?
            "Can't allocate memory for transaction." :
            "Invalid transaction encountered.";
        CarbonXSrioClass::error(NULL, error, errorMesg);
        delete trans;
        return SrioxTrue;
    }
    return SrioxFalse;
}

RIO_DW_T* CarbonXSrioTransClass::copyBfmDw(const UInt size,
        const RIO_DW_T *data)
{
    RIO_DW_T *copyData = NEWARRAY(RIO_DW_T, size);
    if (copyData == NULL)
        return NULL;

    for (UInt i = 0; i < size; i++)
    {
        copyData[i] = data[i];
    }
    return copyData;
}

RIO_DW_T* CarbonXSrioTransClass::copyToBfmDw(const UInt size,
        const CarbonXSrioDword *data)
{
    RIO_DW_T *copyData = NEWARRAY(RIO_DW_T, size);
    if (copyData == NULL)
        return NULL;

    for (UInt i = 0; i < size; i++)
    {
        copyData[i].ms_Word = data[i].msWord;
        copyData[i].ls_Word = data[i].lsWord;
    }
    return copyData;
}

CarbonXSrioDword* CarbonXSrioTransClass::copyFromBfmDw(const UInt size,
        const RIO_DW_T *data)
{
    CarbonXSrioDword *copyData = NEWARRAY(CarbonXSrioDword, size);
    if (copyData == NULL)
        return NULL;

    for (UInt i = 0; i < size; i++)
    {
        copyData[i].msWord = data[i].ms_Word;
        copyData[i].lsWord = data[i].ls_Word;
    }
    return copyData;
}

