/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
    File:      carbonXSrioConfig.cpp 
    Purpose:   This file provides definition of SRIO Configuration 
               Object Routines.
*/

#include "xactors/srio/carbonXSrioConfig.h"
#include "srioXConfig.h"

CarbonXSrioConfig *carbonXSrioConfigCreate(void)
{
    CarbonXSrioConfig *configObj = new CarbonXSrioConfig();
    return configObj;
}

CarbonUInt32 carbonXSrioConfigDestroy(CarbonXSrioConfig *config)
{
    delete config;
    config = NULL;
    return 1;
}

void carbonXSrioConfigSetAddressing(CarbonXSrioConfig *config,
        CarbonXSrioAddressingType supportParam)
{
    config->setAddressing(supportParam);
}

CarbonXSrioAddressingType carbonXSrioConfigGetAddressing(
        const CarbonXSrioConfig *config)
{
    return config->getAddressing();
}

void carbonXSrioConfigSetDeviceIdSizeType(
        CarbonXSrioConfig *config, CarbonXSrioDeviceIdSizeType supportParam)
{
    config->setDeviceIdSize(supportParam);
}

CarbonXSrioDeviceIdSizeType carbonXSrioConfigGetDeviceIdSize(
        CarbonXSrioConfig *config)
{
    return config->getDeviceIdSize();
}

void carbonXSrioConfigSetLinkType(
        CarbonXSrioConfig *config, CarbonXSrioLinkType supportParam)
{
    config->setLinkType(supportParam);
}

CarbonXSrioLinkType carbonXSrioConfigGetLinkType(CarbonXSrioConfig *config)
{
    return config->getLinkType();
}

