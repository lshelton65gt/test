// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_RESPONSE_TRANS_H
#define SRIOX_RESPONSE_TRANS_H

#include "srioXDefines.h"
#include "srioXTrans.h"

/*!
 * \file srioXResponseTrans.h
 * \brief Header file for Response Transaction Class.
 *
 * This file provides the class definition for Response Transaction objects.
 */

/*!
 * \brief Class to represent Response Transaction.
 *
 */
class CarbonXSrioResponseTrans: public CarbonXSrioTransClass
{
public: CARBONMEM_OVERRIDES

    CarbonXSrioResponse _resp;      //!< Response packet structure
    RIO_DW_T *_dwData;              //!< BFM DWORD data
    CarbonXSrioErrorType _error;    //!< Error in constructor

public:

    // Constructor and destructor
    CarbonXSrioResponseTrans(const CarbonXSrioResponse *req);
    CarbonXSrioResponseTrans(RIO_RESPONSE_T *response);
    ~CarbonXSrioResponseTrans();

    CarbonXSrioTransType getType() const
    { return CarbonXSrioTrans_RESPONSE; }
    CarbonXSrioErrorType getError() const
    { return _error; }
    
    // Conversion functions
    CarbonXSrioResponse *getResponse() const;

    // Start transation
    SrioxBool start(CarbonXSrioClass *xtor, UInt tag) const;
    
    // Check if needs response
    virtual SrioxBool needResponse() const { return SrioxFalse; }
};

#endif
