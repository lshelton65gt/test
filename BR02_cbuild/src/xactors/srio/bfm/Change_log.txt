RapidIOBFM
----------
File: Change_log.txt
Initial release of this document: 24/08/2006
Second release: 27/12/2006


This document contains the information about the latest 
fixes/enhancements made in BFM Source.


Bug Fixes/Enhancements detail as follows:
-----------------------------------------

   Bug#43  - Support for more than 32 dwords with allowing/
	     rejecting the illegal packets (more than 32 dwords).

   Bug#117 - Callback Implementation.

   Bug#129 - Port width auto detection feature implementation
	     as per RapidIO Spec 1.3.

   Bug#86  - Ability to postpone acknowledgement Control Symbols
	     in PE & PL BFM.

   Bug#130 - BFM should be able to respond with the PNACC or 
	     Packet retry before packet is completed.

   Bug#116 - Cannot generate 'Multicast Event' control symbol
	     with PE BFM.

   Bug#133 - Support for Type 5 ttypes C and E.

   Bug#131 - Link timeout counter.  Request for user 
             programmability & bug fixing during random
             LOLS activity.

   Bug#132 - PE BFM "RIO_SYMBOL_TO_SEND" callback cannot replace
             "Status CS".

   Bug#124 - Inclusion of Type 9 Data Streaming packet.

   Bug#125 - Inclusion of Type 7 Flow Control packet.

   Bug#101 - Serial PL Link-Request, Reset-Device callback,
	     ackID reset.

   Bug#102 - add crc corruption feature to RIO_SYMBOL_TO_SEND.

   Bug#103 - Add an option to display sent/recived data in 
             a user defined radix of decimal or hexadecimal.

   Bug#104 - Add the target address field value (currently in:
             dest_id_reg) as a field in the $HW_Rio_IO_Request.

   Bug#105 - Add a 'read expected' feature to the Nread requests.

   Bug#106 - Add prints of the BFM requests, reponses, accepted
	     reuests and reponses.

   Bug#111 - Priority of response packets check required.

   Bug#123 - Support for 1.3 Specification.

   Bug#131 - link timeout counter - request for user 
             programmability, and bug fix during random 
             LOLS activity.

   Bug#132 - PE BFM "RIO_SYMBOL_TO_SEND" callback cannot replace
	     "status CS"

   Bug#134 - compensation sequence is not inserted if BFM is
	     kept busy.

   Bug#135 - Request for RapidIO 1.3 1x/4x/ Initilization 
             state machine.

   Bug#136 - TA BFM should allow injection of arbitrary packet
	     types.

   Bug#137 - BFM should use user specified transaction ID for IO
	     request in PE mode.

   Bug#138 - How can user specify  that the BFM not issue 
             response automatically?

   Bug#139 - SW_Rio_Packet_To_Send  in PE mode should allow 
             user to specify recompute of CRC

-------------------------------------------------------------------------
