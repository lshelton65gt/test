#ifndef RIO_LL_ERRORS_H
#define RIO_LL_ERRORS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/include/rio_ll_errors.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  logical layer error/warning message handler prototpe
*               and error codes.
* Notes:
*
******************************************************************************/

#ifndef RIO_LL_DS_H
#include "rio_ll_ds.h"
#endif

/*
 * this type syblically represent error codes which are used as
 * indexes in error messages array
 */
typedef enum {
    RIO_LL_ERROR_INVALID_RECURSIVE_CALL = 0,
    RIO_LL_ERROR_WRONG_PTR, 
    RIO_LL_ERROR_WRONG_PRIO, 
    RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
    RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, 
    RIO_LL_ERROR_CROSS_BOUND, 
    RIO_LL_ERROR_NULL_SUB_DW_DATA_PAYLOAD, 
    RIO_LL_ERROR_NO_DATA,
    RIO_LL_ERROR_ATOMIC_WRONG_SIZE,
    RIO_LL_ERROR_SWRITE_SUB_DW,
    RIO_LL_ERROR_CANNOT_GET_BUFFER,
    RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY,
    RIO_LL_ERROR_WRONG_LOCAL_DATA_PAYLOAD,

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_LL_ERROR_WRONG_LOCAL_DATA_STREAM_PAYLOAD,
    RIO_LL_ERROR_WRONG_LOCAL_DATA_STREAM_MTU,
    RIO_LL_ERROR_WRONG_LOCAL_DATA_STREAM_PACKET,
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    RIO_LL_ERROR_WRONG_GSM_DATA_PAYLOAD,
    RIO_LL_ERROR_GSM_CROSS_GRANULE_BOUND,
    RIO_LL_ERROR_GSM_FLUSH_MISALIGN,
    RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD,
    RIO_LL_ERROR_WRONG_ACTUAL_REMOTE_DATA_PAYLOAD,
    RIO_LL_ERROR_LOCAL_NO_HANDLER,
    RIO_LL_ERROR_REMOTE_NO_HANDLER,
    RIO_LL_ERROR_MESSAGE_WRONG_PARAM,
    RIO_LL_ERROR_REMOTE_MESSAGE_WRONG_PARAM,
    RIO_LL_ERROR_MESSAGE_WRONG_DATA_PAYLOAD,
    RIO_LL_ERROR_WRONG_LOCAL_RESULT,
    RIO_LL_ERROR_TRX_NOT_FOUND,
    RIO_LL_ERROR_QUEUE_ERROR,
    RIO_LL_ERROR_TRX_HANDLER_ERROR,
    RIO_LL_ERROR_NO_CALLBACK,
    RIO_LL_ERROR_INIT_OUTSIDE_RESET,
    RIO_LL_ERROR_NONPARTICIPANT_GSM,
    RIO_LL_ERROR_NO_DATA_PAYLOAD,

    RIO_LL_WARN_NO_MEMORY,
    RIO_LL_WARN_NO_PROCESSOR,
    RIO_LL_WARN_NO_DOORBELL,
    RIO_LL_WARN_NO_MAILBOX,
    RIO_LL_WARN_IN_RESET,
    RIO_LL_WARN_CANNOT_ISSUE,
    RIO_LL_WARN_CANNOT_SERVICE,
    RIO_LL_WARN_INVALID_RESPONSE_FTYPE,
    RIO_LL_WARN_INVALID_RESPONSE_TYPE,
    RIO_LL_WARN_WRONG_RESPONSE_DATA_PAYLOAD,
    RIO_LL_WARN_WRONG_TRX_COMPLETION,
    RIO_LL_WARN_WRONG_ADDR_ONLY_TRANSACTION,
    RIO_LL_WARN_WRONG_TRANS_ONLY_TRANSACTION

} RIO_LL_MESSAGES_T;

typedef enum {
    RIO_LL_INT_ARG = 0,
    RIO_LL_CHAR_ARG,
    RIO_LL_NO_ARG
} RIO_LL_ARG_TYPE_T;

/*
 * this function write an warning message with corresponding explaning
 * based on instanse's state and a set of additional parameters
 */
int RIO_LL_Message(
    RIO_LL_DS_T       *handle, 
    RIO_LL_MESSAGES_T message_Type, 
    ...
);


/****************************************************************************/
#endif /* RIO_LL_ERRORS_H */
