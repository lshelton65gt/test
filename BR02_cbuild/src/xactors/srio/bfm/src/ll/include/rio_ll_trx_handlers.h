#ifndef RIO_LL_TRX_HANDLERS_H
#define RIO_LL_TRX_HANDLERS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/include/rio_ll_trx_handlers.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  fine contains prototypes of transaction handlers implemented 
*               by the logical layer
*
* Notes:        
*
******************************************************************************/

#include "rio_types.h"
#include "rio_ll_ds.h"



/* locally requested IO transaction handlers */
int RIO_LL_Local_IO_SWrite_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_IO_NWrite_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_IO_NWrite_R_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_IO_NRead_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_IO_Atomic_Set_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_IO_Atomic_Clear_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_IO_Atomic_Inc_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_IO_Atomic_Dec_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* GDA: Bug#133 - Support for ttype C for type 5 packet */
int RIO_LL_Local_IO_Atomic_Swap_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);
/* GDA: End */

int RIO_LL_Local_IO_Atomic_TSwap_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* remotely requested IO transaction handlers */
int RIO_LL_Remote_IO_SWrite_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_IO_NWrite_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_IO_NWrite_R_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_IO_NRead_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_IO_Atomic_Set_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_IO_Atomic_Clear_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_IO_Atomic_Inc_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_IO_Atomic_Dec_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* GDA: Bug#133 - Included feature for TType C for Type 5 packet */
int RIO_LL_Remote_IO_Atomic_Swap_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);
/* GDA: End */

int RIO_LL_Remote_IO_Atomic_TSwap_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* locally requested MAINTENANCE transactions handlers */
int RIO_LL_Local_Conf_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_Conf_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_Conf_Port_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* remotely requested MAINTENANCE transaction handlers */
int RIO_LL_Remote_Conf_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_Conf_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_Conf_Port_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* locally requested MESSAGE PASSING transactions handlers */
int RIO_LL_Local_Message_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_Doorbell_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* remotely requested MESSAGE PASSING transactions handlers */
int RIO_LL_Remote_Message_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_Doorbell_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* errorneous remote transaction handlers */
int RIO_LL_Remote_Conf_Read_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_Conf_Write_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_General_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_Message_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);



/* locally requested GSM transaction handlers */
int RIO_LL_Local_GSM_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_I_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_Read_To_Own_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_DCI_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_IO_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_IKILL_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_TLBIE_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_TLBSYNC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_Castout_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Local_GSM_Flush_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* external (remote) requests handlers prototypes */

int RIO_LL_Remote_GSM_Read_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_Read_Owner_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);


int RIO_LL_Remote_GSM_I_Read_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_Read_To_Own_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_Read_To_Own_Owner_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_DKILL_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_DKILL_Sharer_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_IO_Read_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_IO_Read_Owner_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_IKILL_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_IKILL_Sharer_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);


int RIO_LL_Remote_GSM_TLBIE_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_TLBSYNC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_Castout_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

int RIO_LL_Remote_GSM_Flush_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);

/* GDA: Bug#124 - Support for Data Streaming packet */
int RIO_LL_Local_DS_Request_H(
    void               *handle,
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state,
    RIO_LL_TRX_EVENT_T event
);
 
int RIO_LL_Remote_DS_Request_H(
    void               *handle,
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state,
    RIO_LL_TRX_EVENT_T event
);
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
int RIO_LL_Local_FC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);
int RIO_LL_Remote_FC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
);
/* GDA: Bug#125 */

#endif /* RIO_LL_TRX_HANDLERS_H */
