#ifndef RIO_LL_KERNEL_H
#define RIO_LL_KERNEL_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/include/rio_ll_kernel.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains internal kernel interface which is visible and 
*               used by the transaction handlers. 
*
* Notes:        
*
******************************************************************************/

#include <stdlib.h>

#include "rio_ll_ds.h"

/*
 * kernel interface provides following services for transaction handlers:
 * - writing message to error or warning log
 * - configuration and layer parameter set access
 * - UDFs (except address translations?)
 */

/* dummy functions which defines if we have memory, processor and so on */
RIO_BOOL_T RIO_LL_Kernel_Have_Memory( RIO_LL_DS_T *handle );
RIO_BOOL_T RIO_LL_Kernel_Have_Processor( RIO_LL_DS_T *handle );
RIO_BOOL_T RIO_LL_Kernel_Have_Mailbox( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag );
RIO_BOOL_T RIO_LL_Kernel_Have_Doorbell( RIO_LL_DS_T *handle );
RIO_BOOL_T RIO_LL_Kernel_Have_Directory( RIO_LL_DS_T *handle );

/* adds request to various target queue */
int RIO_LL_Kernel_Memory_Request(
    RIO_LL_DS_T           *handle, 
    RIO_LL_BUF_TAG_T      tag,  
    RIO_MEMORY_REQ_TYPE_T req_Type
);

int RIO_LL_Kernel_Snoop_Request(
    RIO_LL_DS_T       *handle, 
    RIO_LL_BUF_TAG_T  tag,  
    RIO_LOCAL_TTYPE_T lttype,
    RIO_SNOOP_TTYPE_T ttype
);

int RIO_LL_Kernel_Send_Responce(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T tag, 
    RIO_UCHAR_T      ftype, 
    RIO_UCHAR_T      transaction, 
    RIO_UCHAR_T      status
);

int RIO_LL_Kernel_Send_Request(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T tag
);

    

/* wrappers around various done callbacks */
int RIO_LL_Kernel_Local_Responce(
    RIO_LL_DS_T       *handle, 
    RIO_LL_BUF_TAG_T  buf_Tag, 
    RIO_LOCAL_RTYPE_T response
);

int RIO_LL_Kernel_GSM_Request_Done(
    RIO_LL_DS_T      *handle,   
    RIO_LL_BUF_TAG_T buf_Tag,
    RIO_BOOL_T       supply_Data, 
    RIO_REQ_RESULT_T result 
);

int RIO_LL_Kernel_IO_Request_Done(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_BOOL_T       supply_Data, 
    RIO_REQ_RESULT_T result 
);

int RIO_LL_Kernel_MP_Request_Done(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_REQ_RESULT_T result 
);

int RIO_LL_Kernel_Doorbell_Request_Done(
    RIO_LL_DS_T      *handle,   
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_REQ_RESULT_T result
);

int RIO_LL_Kernel_Config_Request_Done(
    RIO_LL_DS_T      *handle,   
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_BOOL_T       supply_Data, 
    RIO_REQ_RESULT_T result
);


/* GDA: Bug#124 - Support for Data Streaming packet */
int RIO_LL_Kernel_DS_Request_Done(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T buf_Tag,
    RIO_BOOL_T       supply_Data,
    RIO_REQ_RESULT_T result
);
/* GDA: Bug#124 - Support for Data Streaming packet */


/* GDA: Bug#125 - Support for Flow control packet */
int RIO_LL_Kernel_FC_Request_Done(
    RIO_LL_DS_T      *handle,   
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_REQ_RESULT_T result
);
/* GDA: Bug#125 - Support for Flow control packet */


/* wrappers around simple remote requests notifiers such as doorbel and message */
int RIO_LL_Kernel_MP_Remote_Request(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag);
int RIO_LL_Kernel_Doorbell_Remote_Request(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag);

/* configuration space access operations wrappers */
int RIO_LL_Kernel_Set_Config_Reg( 
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);

int RIO_LL_Kernel_Get_Config_Reg( 
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);

int RIO_LL_Kernel_Port_Write(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);

RIO_BOOL_T RIO_LL_Kernel_Is_To_Config_Space(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);

/* function for the transaction state setting */
int RIO_LL_Kernel_Set_Trx_State( 
    RIO_LL_DS_T        *handle,
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state 
);

/* snoop data operation functions */

/* function merges data from snoop with data 
 * in packet to perform memory write operation */
int RIO_LL_Kernel_Merge_Snoop_Data(  
    RIO_LL_DS_T      *handle,         
    RIO_LL_BUF_TAG_T buf_Tag          
);

/* function places data from snoop buffer into transaction buffer */
int RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(
    RIO_LL_DS_T      *handle,         
    RIO_LL_BUF_TAG_T buf_Tag,
    RIO_BOOL_T       wrapped
);


/*
 * Kernel functions below are used by GSM handlers 
 * to implement their protocols
 *
 */
 
RIO_DIRECTORY_STATE_T RIO_LL_Kernel_Directory_State(
    RIO_LL_DS_T     *handle,
    RIO_LL_BUF_TAG_T buf_Tag
);

int RIO_LL_Kernel_Set_Received_Done_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);

int RIO_LL_Kernel_Set_Received_Data_Only_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);

RIO_BOOL_T RIO_LL_Kernel_Received_Data_Only_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);

RIO_BOOL_T RIO_LL_Kernel_Received_Done_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
);


int RIO_LL_Kernel_Send_Altered_Request(
    RIO_LL_DS_T         *handle,
    RIO_LL_BUF_TAG_T    buf_Tag,
    RIO_DEST_MASK_T     dest,
    RIO_PL_GSM_TTYPE_T  ttype,
    RIO_BOOL_T          is_SECID_EQ_MYID  /* else secid is determined from external request packet */
);    

int RIO_LL_Kernel_Update_Directory_State(
    RIO_LL_DS_T             *handle, 
    RIO_LL_BUF_TAG_T        buf_Tag, 
    RIO_DIRECTORY_UPDATE_T  update
);    


/* mask_id ~== received_srcid; function returns true if the conditional is satisfied */
RIO_BOOL_T RIO_LL_Kernel_Is_Intervention_Case(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
);

/* received_srcid == received_secid */
RIO_BOOL_T RIO_LL_Kernel_Is_RCVSRCID_EQ_RCVSECID(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
);

/* mask == received_srcid; returns true if the requestor is the only remote sharer */
RIO_BOOL_T RIO_LL_Kernel_Is_Requestor_The_Only_Remote_Sharer(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
);

/* checks that data is received (used by FLUSH transaction handlers) */
RIO_BOOL_T RIO_LL_Kernel_Received_Data(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
);

int RIO_LL_Kernel_Coh_Domain_Size(
    RIO_LL_DS_T      *handle
);    


#endif /* RIO_LL_KERNEL_H */
