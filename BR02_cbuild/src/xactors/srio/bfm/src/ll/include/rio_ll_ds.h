#ifndef RIO_LL_DS_H
#define RIO_LL_DS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\ll\include\rio_ll_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains logical layer module interfaces to the transport 
*               layer, external request/response interface, memory interfaces 
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif /* RIO_TYPES_H */

#ifndef RIO_LL_INTERFACE_H
#include "rio_ll_interface.h"
#endif

#ifndef RIO_PL_INTERFACE_H
#include "rio_pl_interface.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h" 	/* GDA  - GDA_PKT_SIZE is defined */
#endif 


/* typedefs and defines used by the logical layer submodules */

typedef unsigned int RIO_LL_BUF_TAG_T;

/* states for transactions processing */
typedef enum {
    RIO_LL_STATE_INIT         = 0,  /* initial state of the transaction       */
    RIO_LL_STATE_SEND_REQUEST,      /* transaction is sending request packet  */
    RIO_LL_STATE_SEND_RESPONSE,     /* transaction is sending response packet */
    RIO_LL_STATE_SNOOP,             /* transaction waits for snoop completion */
    RIO_LL_STATE_MEMORY_ACCESS,     /* transaction waits for memory access completion */
    RIO_LL_STATE_SEND_REQUEST_TO_OWNER,  /* my_id == mem_id == original_srcid (GSM handlers) */
    RIO_LL_STATE_SEND_REQUEST_TO_SHARER, /* same as above, we are home memory and original
                                           requestor and have sent multicast to sharers */
    RIO_LL_STATE_SEND_REQUEST_TO_HOME,   /* my_id ~= mem_id and i'm original requestor (GSM handlers) */
    RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY, /* my_id == mem_id ~== original_srcid (GSM handlers) */

    RIO_LL_STATE_MEMORY_UPDATE_ACCESS,   /* memory update before returning data to local device */
    RIO_LL_STATE_WAIT_SNOOP_COMPLETE,
    RIO_LL_STATE_SEND_REQUEST_TO_SHARERS,
    RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED,
    RIO_LL_STATE_PARADOX_SEND_REQUEST_FOR_THIRD_PARTY /* used in IREAD external request state machine */
} RIO_LL_TRX_STATE_T;


/* events values from the transactions processing */
typedef enum {
    RIO_EVENT_START_TO_LOCAL  = 0, /* start processing to local space  */
    RIO_EVENT_START_TO_REMOTE,     /* start processing to remote space */

    RIO_EVENT_REQUEST_SENT,        /* request sent */
    RIO_EVENT_RESPONSE_SENT,       /* response sent */

    RIO_EVENT_SNOOP_HIT,           /* snoop hit for whole data was received */
    RIO_EVENT_SNOOP_MISS,          /* snoop miss for the whole data was received */

    RIO_EVENT_MEMORY_COMPLETE,     /* memory access complete */

    RIO_EVENT_RESPONSE_DONE,              /* DONE response arrived      */
    RIO_EVENT_RESPONSE_NOT_OWNER,         /* NOT_OWNER response arrived */
    RIO_EVENT_RESPONSE_RETRY,             /* RETRY response arrived     */
    RIO_EVENT_RESPONSE_ERROR,             /* ERROR response arrived     */
    RIO_EVENT_RESPONSE_NO_MEMORY,         /* NO_MEMORY response arrived */
    RIO_EVENT_RESPONSE_CANNOT_SERVICE,    /* CANNOT SERVICE response arrived */
    RIO_EVENT_RESPONSE_INTERVENTION,      /* INTERVENTION response arrived   */
    RIO_EVENT_RESPONSE_DATA_ONLY,         /* DATA_ONLY response arrived      */
    RIO_EVENT_RESPONSE_DONE_INTERVENTION, /* DONE_INTERVENTION response arrived */

    /* these two handlers are for servicing of the erroneous requets */
    RIO_EVENT_ERROR_GENERAL,        /* request contains some general error */
    RIO_EVENT_ERROR_CANNOT_SERVICE, /* request cannot be serviced          */

    RIO_EVENT_REQUEST_TIMEOUT,      /* request is timed out */

    /* states below are used only by GSM remote handlers to deal with possible collisions */    
    RIO_EVENT_START_NO_COLLISION,      /* no collision was detected by the kernel */
    RIO_EVENT_START_COLLISION_STALLED, /* collision is detected and transaction is stalled */
    RIO_EVENT_END_COLLISION_NORMAL,    /* transaction is unstalled and should start processing 
                                          in the normal way  */
    RIO_EVENT_END_COLLISION_ERROR,     /* transaction is unstalled and should send ERROR response
                                          packet to the originator of the request */
    RIO_EVENT_END_COLLISION_DONE,      /* transaction is unstalled and should send DONE response
                                          packet to the originator of the request */
    RIO_EVENT_START_COLLISION_RETRY,   /* collision is detected and RETRY response shall be sent
                                          per collision resolution tables; transaction isn't stalled */
    RIO_EVENT_START_COLLISION_ERROR,
    RIO_EVENT_START_COLLISION_NOT_OWNER

} RIO_LL_TRX_EVENT_T;


#define RIO_LL_MAX_PACKET_DW_SIZE       GDA_PKT_SIZE /* GDA - maximum size of packet data is 256 bytes */
#define RIO_LL_MAX_CONF_PACKET_DW_SIZE  8    /* maximum size of packet data is 256 bytes      */
#define RIO_LL_MAX_ATOMIC_BYTE_SIZE     4    /* maximum size of the ATOMIC request is 4 bytes */
#define RIO_LL_DW_BYTE_SIZE             8    /* size of doubleword is 8 bytes                 */
#define RIO_LL_WORD_BYTE_SIZE           4    /* size of word is 4 bytes                       */
#define RIO_LL_BYTE_BIT_SIZE            8    /* size of the byte in bits                      */


/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_LL_HW_BYTE_SIZE             2         /* size of half word is 2 bytes              */
#define RIO_LL_MAX_PACKET_DS_SIZE       0xFFFF    /*Data Streaming packet max size is 64k bytes */
/* GDA: Bug#124 - Support for Data Streaming packet */

/* maximum allowed size of the message transaction */
#define RIO_LL_MAX_MESSAGE_BYTE_SIZE 	256
/* maximum length of message in segments */
#define RIO_LL_MAX_MSG_SEGMENTS_NUM 15
/* maximum size of port write data payload */
#define RIO_LL_MAX_PORT_WRITE_BYTE_SIZE 64


#define RIO_MAX_REQUEST_PRIORITY        3    /* the maximal piority for the request packet is 3  */
#define RIO_MAX_RESPONSE_PRIORITY       3    /* the maximal piority for the response packet is 3 */
#define RIO_MAX_MBOX_PER_PE             3    /* the maximal mbox quantity per PE is 4            */
#define RIO_MAX_EXT_MBOX_PER_PE         63   /* the maximum extended mbox quantity per PE is 64  */
#define RIO_MAX_LETTER_PER_MBOX         3    /* There cannot be stored more than 4 letters in one */
                                             /* message box                                       */
/* directory state used by GSM handlers */
typedef enum {
    RIO_LL_LOCAL_SHARED = 0,
    RIO_LL_LOCAL_MODIFIED,
    RIO_LL_SHARED,
    RIO_LL_REMOTE_MODIFIED,
    RIO_LL_ADDR_OUT_OF_RANGE
} RIO_DIRECTORY_STATE_T;

/* constants tell kernel how to determine destination devices of the transaction */
typedef enum {
    RIO_MASK_ID = 0,   /* mask_id is device id of the device which remotely modified cacheline */
    RIO_MASK_WO_MY_ID, /* mask ~= my_id - sharers without my id */
    RIO_MASK_WO_RECEIVED_SRCID, /* mask ~= received_srcid, i.e. all sharers except my_id and received_srcid */
    RIO_MASK_RECEIVED_SRCID,    /* mask only includes received srcid (i.e. source id of response packet received) */
    RIO_MASK_PARTICIPANT_ID,    /* all coherence domain participants (without my_id) */
    RIO_MASK_PARTICIPANT_WO_RECEIVED_SRCID /* sends to all participants except my_id and received_src_id */
                                           /* necessary for servicing remote IKILL_HOME request */
} RIO_DEST_MASK_T;

/* constants are used to let kernel know how to modify memory directory for the cacheline */
typedef enum {
    RIO_SET_SHARED_MY_ID = 0,        /* set shared by local device only */
    RIO_LOCAL_MODIFIED,              /* set cacheline state as locally modified */
    RIO_ADD_SHARED_REQUESTOR_ID,     /* add received_srcid to the sharing list */
    RIO_SET_SHARED_REQUESTOR_ID,     /* set cacheline as shared by local device and requestor srcid */
    RIO_REMOTE_MODIFIED              /* set cacheline as remote modified by external requestor */
} RIO_DIRECTORY_UPDATE_T;


/* conatants define type of the GSM collision, minding its resolution */
typedef enum {
    RIO_LL_COLLISION_NO_COLLISION = 0,
    RIO_LL_COLLISION_STALL,
    RIO_LL_COLLISION_RETRY_RESPONSE,
    RIO_LL_COLLISION_ERROR_RESPONSE,
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE
} RIO_LL_COLLISION_T;

/* fields of configuration registers which are used by ll during its operation */
typedef enum {
    RIO_LL_EXT_ADDRESSING_SUPPORT = 0,
    RIO_LL_MY_DEVICE_ID,
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_LL_DSTREAMING_SUPPORT,		
    RIO_LL_DSTREAMING_SUPPORT_INFO
    /* GDA: Bug#124 - Support for Data Streaming packet */
	    
} RIO_LL_CONFIG_FIELD_T;

/* results for the return value if the transaction handlers */
#define RIO_LL_PROGRESS 0            /* processing of the transaction is in progress */
#define RIO_LL_COMPLETE 1            /* processing of the transaction is complete */
#define RIO_LL_ERROR    2            /* error during transaction servicing        */

/* type for the internal semaphro */
typedef unsigned char RIO_LL_SEMAPHOR_T;

/* possible states for RIO semaphor */
#define RIO_LL_SEMAPHOR_INIT  0
#define RIO_LL_SEMAPHOR_ACK   1
#define RIO_LL_SEMAPHOR_MEM   2
#define RIO_LL_SEMAPHOR_SNOOP 4
#define RIO_LL_SEMAPHOR_DONE  8

typedef int (*RIO_LL_TRX_HANDLER_T)(
    void               *handle,
    RIO_LL_BUF_TAG_T   tag,
    RIO_LL_TRX_STATE_T state,
    RIO_LL_TRX_EVENT_T event     
);


typedef unsigned int RIO_LL_Q_EL_STATE_T;

/* possible states for queue elements */
#define RIO_LL_WAIT_FOR_SERVICE   0
#define RIO_LL_WAIT_FOR_ACK       1
#define RIO_LL_WAIT_FOR_RESPONCE  2


/* element of queue used to build snoop, memory, rio response and request target queues */
typedef struct RIO_LL_Queue_Element {
    RIO_LL_BUF_TAG_T            buf_Tag;       /* buffer tag of the corresponding request */
    RIO_UCHAR_T                 prio;          /* priority of the request */
    RIO_LL_Q_EL_STATE_T         current_State; /* current state of the request */
    struct RIO_LL_Queue_Element *next;         /* next element pointer */

} RIO_LL_QUEUE_T;


/* pointers to queue free elements pools and queue heads (memory is allocated during instantiation) */
typedef struct {

    RIO_LL_QUEUE_T *snoop_Queue_Pool;
    RIO_LL_QUEUE_T *memory_Queue_Pool;
    RIO_LL_QUEUE_T *rio_Req_Queue_Pool;
    RIO_LL_QUEUE_T *rio_Resp_Queue_Pool;

    RIO_LL_QUEUE_T *snoop_Queue_Head;
    RIO_LL_QUEUE_T *memory_Queue_Head;
    RIO_LL_QUEUE_T *rio_Req_Queue_Head;
    RIO_LL_QUEUE_T *rio_Resp_Queue_Head;

} RIO_LL_TARGET_QUEUES_T;

/* type of locally requested transaction */
typedef enum {
    RIO_LL_GSM = 0, 
    RIO_LL_IO, 
    RIO_LL_CONF,
    RIO_LL_MESSAGE,
    RIO_LL_DOORBELL,

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_LL_FC	= 7,

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_LL_DS	= 9   	 
   
} RIO_LL_FTYPE_T;

/* reserved or allowed combination of wdptr and rdsize (wrsize) */
typedef enum {
    RIO_LL_RESERVED = 0,
    RIO_LL_ALLOWED
} RIO_COMBINATION_T;

/* data structure to calculate byte_num, offset and dw_num from wdptr and rdsize */
typedef struct {
    /* reserved combination of wdptr and wrsize or not (rdsize and wdptr are no reserved) */
    RIO_COMBINATION_T  is_Allowed_Combination;
    /* corresponding byte_num, offset or dw_num */
    RIO_UCHAR_T offset;     /* meaningful is dw_Size = 1 */
    RIO_UCHAR_T byte_Num;   /* meaningful is dw_Size = 1 */
    RIO_UCHAR_T dw_Size;
} RIO_LL_WDPTR_TO_SIZE_T;
    

/* RIO logical layer buffer for both local and remote requests */
typedef struct {
    RIO_UCHAR_T          prio;         /* priority of the transaction */

    RIO_TAG_T            user_Trx_Tag; /* tag of the transaction supported by the user */

    RIO_FTYPE_T          req_FType;    /* ftype as defined in the specification */
    RIO_UCHAR_T          req_TType;    /* ttype as defined in the specification */         

                                       /* request as it supported by environment */
    RIO_LL_FTYPE_T       req_Type;     /* type of the request */
    void                 *req;         /* it is nesseccary to use type cast to access request fields */
    RIO_TR_INFO_T        tr_Info[RIO_MAX_COH_DOMAIN_SIZE];    /* this field is used */
                                       /* only for GSM, IO and message transactions. In case of the GSM transactions */
                                       /* there maybe more than one transport info (multicast )             */
    RIO_UCHAR_T          multicast_Size; /* number of the destinations of the request. Corresponds to the   */
                                       /* of the values in the previous array. 1 in case of the IO or the   */
                                       /* nonmulticast GSM request. More than 1 in case of the multicast    */
                                       /* GSM request                                                       */

    RIO_PL_GSM_TTYPE_T   gsm_TType;    /* this field is used only for GSM transactions               */

                                       /* data related fields, common for both request and responce */
    RIO_ADDRESS_T        address;      /* double-word aligned address */
    RIO_ADDRESS_T        extended_Address;
    RIO_UCHAR_T          xamsbs;
    RIO_UCHAR_T          dw_Size;      /* size of transaction in double words */
    RIO_UCHAR_T          offset;       /* offest of the data; meaningful only if dw_Size == 1 */
    RIO_UCHAR_T          byte_Num;     /* number of bytes starting from the offset, should not cross dw boundary */
    RIO_DW_T             data[RIO_LL_MAX_PACKET_DW_SIZE]; /* data itself */
    RIO_UCHAR_T          dw_Num;       /* actual data payload size arrived with the remotely requested packet */

                                       /* types of the neseccary snoop requesi */
    RIO_LOCAL_TTYPE_T    snoop_LTType; /* local type                           */
    RIO_SNOOP_TTYPE_T    snoop_TType;  /* type supported by the transaction    */
                                       
    RIO_MEMORY_REQ_TYPE_T mem_Type;    /* type of the local memory request needed by the transaction */

    RIO_TR_INFO_T        sec_ID;       /* fields for intervention GSM request servicing */
    RIO_UCHAR_T          sec_TID;

    RIO_UCHAR_T          mbox;         /* message packet related fields */
    RIO_UCHAR_T          letter;
    RIO_UCHAR_T          msgseg;
    RIO_UCHAR_T          ssize;
    RIO_UCHAR_T          msglen;

    RIO_DB_INFO_T        doorbell_Info; /* doorbell packet related fields */

    RIO_CONF_OFFS_T      conf_Offset;  /* 
                                        * configuration offset
                                        * in case of MAINTENANCE request data field is used for data 
                                        * storage 
                                        */               
    RIO_UCHAR_T          sub_Dw_Pos;   /* 
                                        * sub doubleword position for maintenance transactions 
                                        * in case of sub-doubleword access, the data position inside dw:
                                        * 0 - first half-dw, 1 - second, else - whole dw is being accessed 
                                        */
    RIO_TR_INFO_T        src_ID;   /* device ID of the requestor */
    

    RIO_TR_INFO_T        resp_Tr_Info;    /* transport info for the response sending */
    RIO_PL_TRANSP_TYPE_T resp_Transp_Type; /*size of the transport for the responce sending info*/

    RIO_TR_INFO_T        resp_Src_ID;     /* device ID of the response originator */
    RIO_UCHAR_T          resp_Target_TID; /* this field is mbox_letter_msgseg for message responses */

                                           /* necessary response packet fields */
                                           /* these fields common for both received responce and responce to send */
    RIO_UCHAR_T          resp_FType;       /* type of the responce packet */
    RIO_UCHAR_T          resp_Transaction; /* transaction of the responce packet */
    RIO_UCHAR_T          resp_Status;      /* status of the responce packet */
    RIO_UCHAR_T          resp_DW_Size;     /* number of the doublewords in the responce */

    RIO_BOOL_T           get_Data_From_Snoop;   /* this flag shows that data for the memory operation */
                                                /* shall come from the snoop buffer                   */
    RIO_UCHAR_T          data_Offset;           /* offset of the data to be written or read drom the  */
                                                /* snoop buffer start                                 */
    RIO_UCHAR_T          data_DW_Size;          /* size of the data to be written or read from the    */
                                                /* snoop buffer                                       */
    RIO_UCHAR_T          count_From_Snoop;      /* counter of the memory operation from snoop buffer  */
    RIO_UCHAR_T          current_From_Snoop;    /* number of the current operation from the snoop buffer */

    RIO_DW_T             *snoop_Data;           /* snoop data temporary buffer                        */ 
    RIO_BOOL_T           *snoop_Mask;           /* mask of results of the snoop for the individual    */
                                                /* cache granules                                     */
    RIO_ADDRESS_T        snoop_Start_Address;   /* start address for the snoop operation              */
    RIO_ADDRESS_T        snoop_Finish_Address;  /* end address for the snoop operation                */
    RIO_ADDRESS_T        snoop_Current_Address; /* address of the current cache granule to be snooped */
    RIO_UCHAR_T          snoop_DW_Size;         /* size of the snooped data in doublewords            */

    RIO_BOOL_T   gsm_Received_Done_Flag; /* TRUE if DONE response was received for this transaction */
    RIO_BOOL_T   gsm_Received_Data_Only_Flag; /* TRUE if DATA_ONLY response was received            */

    RIO_BOOL_T   is_Outstanding;  /* this flag is set in case the transaction is the GSM outstanding transaction */
    
                                          /* state information recorded by the transaction handler */
    RIO_LL_TRX_STATE_T   state;           /* on first invocation is set to RIO_INIT_STATE by the kernel */

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_UCHAR_T     cos;           /*class of service*/
    RIO_BOOL_T      s_Ds;          /*indicates start of segment*/
    RIO_BOOL_T      e_Ds;          /*end of segment*/
    RIO_BOOL_T      o;             /* If set payload has an odd no.of halfwords */
    RIO_BOOL_T      p;             /* If set pad byte was used to pad to a half word boundary*/
    RIO_DS_LENGTH_T length;        /* length in bytes of the segmented PDU */
    RIO_UCHAR_T     mtu ;          /* Maximun transmission unit for segments */
    RIO_DS_INFO_T   stream_ID;     /* traffic stream identifier*/
    RIO_HW_T        data_hw[0xFFFF];/* data itself */								     
    RIO_HW_SIZE     hw_Size ;
    RIO_UCHAR_T     hw_Num;         /* actual data payload size arrived with the remotely requested packet */
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_UCHAR_T		x_On_Off;	/* Stop/Start issuing req for specified & lower priority 
					   transaction request flowP) 				*/
    RIO_UCHAR_T		flow_ID;	/* Highest priority affected transaction request flow 	*/
    RIO_ADDRESS_T	dest_ID;	/* Indicates which end pt the CCP is destined for (srcID
					   of the packet that caused the generation of the CCP) */
    RIO_ADDRESS_T	tgtdest_ID;	/* Combined with the flowID field, indicates which 
					   transaction request flows need to be acted upon 
					   (destinationID field of the packet which caused the 
					   generation of the CCP)				*/
    RIO_UCHAR_T		soc;		/* Source Of Congestion from Switch / End point	*/
    /* GDA: Bug#125 */
     /*GDA: Bug#123 */
    RIO_UCHAR_T		mbox_Xmbox;   /* GDA: Bug #123 Support for extended mailbox ( xmbox) feature */	
  /*   GDA: Bug#123 */
 
    
} RIO_LL_BUFFER_TRX_INFO_T;

typedef struct {
    
    RIO_BOOL_T           valid;           /* buffer entry valid flag */
    RIO_UCHAR_T          for_Prio;        /* defines priority which is necessary to occupy buffer entry */
    RIO_LL_TRX_HANDLER_T handler;         /* pointer to the handler of the transaction */
    RIO_BOOL_T           stall_Tag_Valid; /* true if this transaction stalls some other (later) transaction */
    RIO_LL_BUF_TAG_T     stall_Tag;       /* buffer tag of the transaction which is */
                                          /* stalled against this transaction */
    RIO_BOOL_T           is_Stalled;      /* the flag indicates that current transaction is stalled */
    RIO_BOOL_T           is_Local;        /* the flag indicates that transaction shall be dirested to the */
                                          /* the local device                                             */
} RIO_LL_BUFFER_KERNEL_INFO_T;

typedef struct {
    RIO_LL_BUFFER_TRX_INFO_T    trx_Info;
    RIO_LL_BUFFER_KERNEL_INFO_T kernel_Info;
} RIO_LL_BUFFER_T;

typedef struct {
    RIO_BOOL_T       has_Transaction; /* this field is RIO_TRUE if buffer has transaction */

    RIO_TAG_T        tag; 
    RIO_TR_INFO_T    transport_Info;
    RIO_PL_TRANSP_TYPE_T transp_Type; /*size of the transport info*/

    RIO_UCHAR_T      packet_Type; /* this field selects packet type - from 1 to 12 and allows to understand which
                                   header data structure fields are valid for this packet */
    RIO_UCHAR_T      target_TID;
    RIO_TR_INFO_T    src_ID;
    RIO_UCHAR_T      prio;
    RIO_UCHAR_T      wdptr;
    RIO_UCHAR_T      rdsize;
    RIO_UCHAR_T      format_Type;
    RIO_UCHAR_T      ttype;
    RIO_ADDRESS_T    address;
    RIO_ADDRESS_T    extended_Address;
    RIO_UCHAR_T      xamsbs;
    RIO_TR_INFO_T    sec_ID;
    RIO_UCHAR_T      sec_TID;
    RIO_UCHAR_T      mbox;
    RIO_UCHAR_T      letter;
    RIO_UCHAR_T      msgseg;
    RIO_UCHAR_T      ssize;
    RIO_UCHAR_T      msglen;
    RIO_DB_INFO_T    doorbell_Info;
    RIO_CONF_OFFS_T  offset;

    RIO_UCHAR_T      dw_Num;                          /* number of received double words for the transaction   */
    RIO_DW_T         data[RIO_LL_MAX_PACKET_DW_SIZE]; /* data is supplied only if required by the request type */

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_UCHAR_T     cos;                  /*class of service*/
    RIO_BOOL_T      s_Ds;                 /*indicates start of segment*/
    RIO_BOOL_T      e_Ds;                 /*end of segment*/
    RIO_BOOL_T      o;                    /* If set payload has an odd no.of halfwords */
    RIO_BOOL_T      p;                    /* If set pad byte was used to pad to a half word boundary*/
    RIO_DS_LENGTH_T length;               /* length in bytes of the segmented PDU */
    RIO_UCHAR_T     mtu ;                 /* Maximun transmission unit for segments */
    RIO_UCHAR_T     hw_Num;               /* number of received half words for the transaction   */
    RIO_DS_INFO_T   stream_ID;            /* traffic stream identifier*/
    RIO_HW_T        data_hw[DS_PKT_SIZE]; /* data is supplied only if required by the request type */
    RIO_HW_T        data_ds[DS_SIZE];     /* data is supplied only if required by the request type */
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_UCHAR_T		x_On_Off;	/* Stop/Start issuing req for specified & lower priority 
					   transaction request flowP) 				*/
    RIO_UCHAR_T		flow_ID;	/* Highest priority affected transaction request flow 	*/
    RIO_ADDRESS_T	dest_ID;	/* Indicates which end pt the CCP is destined for (srcID
					   of the packet that caused the generation of the CCP) */
    RIO_ADDRESS_T	tgtdest_ID;	/* Combined with the flowID field, indicates which 
					   transaction request flows need to be acted upon 
					   (destinationID field of the packet which caused the 
					   generation of the CCP)				*/
    RIO_UCHAR_T		soc;		/* Source Of Congestion from Switch / End point	*/
    RIO_BOOL_T		is_congestion;
    /* GDA: Bug#125 */

} RIO_LL_TEMP_BUFFER_T;

/* constansts to be used when accessing transaction handlers pointers array */
typedef enum {

    RIO_LL_LOCAL_GSM_READ_HOME = 0,
    RIO_LL_LOCAL_GSM_READ_TO_OWN_HOME,
    RIO_LL_LOCAL_GSM_IO_READ_HOME,
    RIO_LL_LOCAL_GSM_DKILL_HOME,
    RIO_LL_LOCAL_GSM_IREAD_HOME,
    RIO_LL_LOCAL_GSM_IKILL,
    RIO_LL_LOCAL_GSM_TLBIE,
    RIO_LL_LOCAL_GSM_TLBSYNC,
    RIO_LL_LOCAL_GSM_CASTOUT,
    RIO_LL_LOCAL_GSM_FLUSH,

    RIO_LL_LOCAL_IO_NREAD,
    RIO_LL_LOCAL_IO_NWRITE,
    RIO_LL_LOCAL_IO_NWRITE_R,
    RIO_LL_LOCAL_IO_SWRITE,
    RIO_LL_LOCAL_IO_ATOMIC_SET,
    RIO_LL_LOCAL_IO_ATOMIC_CLEAR,
    RIO_LL_LOCAL_IO_ATOMIC_INC,
    RIO_LL_LOCAL_IO_ATOMIC_DEC,
    RIO_LL_LOCAL_IO_ATOMIC_SWAP,	/* GDA: Bug#133 - Support for ttype C for type 5 packet */
    RIO_LL_LOCAL_IO_ATOMIC_TSWAP,

    RIO_LL_LOCAL_CONF_READ,
    RIO_LL_LOCAL_CONF_WRITE,
    RIO_LL_LOCAL_CONF_PORT_WRITE,

    RIO_LL_LOCAL_MP,
    RIO_LL_LOCAL_DOORBELL,

    RIO_LL_REMOTE_GSM_READ_OWNER,
    RIO_LL_REMOTE_GSM_READ_TO_OWN_OWNER,
    RIO_LL_REMOTE_GSM_IO_READ_OWNER,
    RIO_LL_REMOTE_GSM_READ_HOME,
    RIO_LL_REMOTE_GSM_READ_TO_OWN_HOME,
    RIO_LL_REMOTE_GSM_IO_READ_HOME,
    RIO_LL_REMOTE_GSM_DKILL_HOME,
    RIO_LL_REMOTE_GSM_IREAD_HOME,
    RIO_LL_REMOTE_GSM_DKILL_SHARER,
    RIO_LL_REMOTE_GSM_IKILL_HOME,
    RIO_LL_REMOTE_GSM_IKILL_SHARER,
    RIO_LL_REMOTE_GSM_TLBIE,
    RIO_LL_REMOTE_GSM_TLBSYNC,
    RIO_LL_REMOTE_GSM_CASTOUT,
    RIO_LL_REMOTE_GSM_FLUSH,

    RIO_LL_REMOTE_IO_NREAD,
    RIO_LL_REMOTE_IO_NWRITE,
    RIO_LL_REMOTE_IO_NWRITE_R,
    RIO_LL_REMOTE_IO_SWRITE,
    RIO_LL_REMOTE_IO_ATOMIC_SET,
    RIO_LL_REMOTE_IO_ATOMIC_CLEAR,
    RIO_LL_REMOTE_IO_ATOMIC_INC,
    RIO_LL_REMOTE_IO_ATOMIC_DEC,
    RIO_LL_REMOTE_IO_ATOMIC_SWAP,	/* GDA: Bug#133 - Support for ttype C for type 5 packet */
    RIO_LL_REMOTE_IO_ATOMIC_TSWAP,
    
    RIO_LL_REMOTE_CONF_READ,
    RIO_LL_REMOTE_CONF_WRITE,
    RIO_LL_REMOTE_CONF_PORT_WRITE,
    
    RIO_LL_REMOTE_MP,
    RIO_LL_REMOTE_DOORBELL,

    RIO_LL_REMOTE_GENERAL_ERROR,
    RIO_LL_REMOTE_CONF_READ_ERROR,
    RIO_LL_REMOTE_CONF_WRITE_ERROR,
    RIO_LL_REMOTE_MESSAGE_ERROR,

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_LL_LOCAL_DS, 		
    RIO_LL_REMOTE_DS, 
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_LL_LOCAL_FC,
    RIO_LL_REMOTE_FC,
    /* GDA: Bug#125 */

    RIO_LL_END_TABLE

} RIO_LL_HANDLER_NAMES_T;

typedef struct {
    RIO_LL_HANDLER_NAMES_T name;
    RIO_LL_TRX_HANDLER_T   func;
} RIO_LL_HANDLER_TABLE_T;


/* the data type represents logical layer parameters set */
typedef struct {
    RIO_WORD_T    config_Space_Size;   /* this parameter defins size of the memory window */
                                       /* mapped to the configuration space               */
                                                                             

    RIO_TR_INFO_T remote_Dev_ID[RIO_MAX_COH_DOMAIN_SIZE];  /* array of deviceIDs for coherence domain participants */
    /*RIO_TR_INFO_T dev_ID; */
    RIO_PL_TRANSP_TYPE_T transp_Type;
    

/*    RIO_BOOL_T    ext_Address;    */ /* extended address support     */
/*    RIO_BOOL_T    ext_Address_16; */ /* size of the extended address */
/*    RIO_BOOL_T    xamsbs;        */  /* xamsbs support               */

    RIO_BOOL_T  ext_Mailbox_Support; /* PE supports extended mailbox (msglen=0, mailbox_number=msgseg||mbox) */

    /*
     * these parameters define reservation police for outbound buffer
     */
    RIO_UCHAR_T res_For_3_Out;
    RIO_UCHAR_T res_For_2_Out;
    RIO_UCHAR_T res_For_1_Out;

    /*
     * these parameters define reservation police for inbound buffer
     */
    RIO_UCHAR_T res_For_3_In;
    RIO_UCHAR_T res_For_2_In;
    RIO_UCHAR_T res_For_1_In;

    /*
     * order in that inbound packets are passed to the environment
     * pass_By_Prio     if TRUE the most priority packet goes first
     *                      else the oldest
     */
    RIO_BOOL_T pass_By_Prio;
} RIO_LL_PARAM_SET_T;


typedef struct {
    RIO_BOOL_T    coh_Granule_Size_32; /* size of coherence granule - 32 or 64 bytes */
    RIO_UCHAR_T   coh_Domain_Size;     /* size of coherence domain - 2, 4, 16 participants allowed */

    RIO_WORD_T    source_Trx; /* source transactions support      */
    RIO_WORD_T    dest_Trx;   /* destination transactions support */

    RIO_BOOL_T    has_Memory;
    RIO_BOOL_T    has_Processor;
    RIO_BOOL_T    has_Doorbell;
    RIO_BOOL_T    mbox1;
    RIO_BOOL_T    mbox2;
    RIO_BOOL_T    mbox3;
    RIO_BOOL_T    mbox4;
      /*GDA : Bug #123 Support for extended mailbox(xmbox) feature */
    RIO_BOOL_T    mbox5;
    RIO_BOOL_T    mbox6;
    RIO_BOOL_T    mbox7;
    RIO_BOOL_T    mbox8;
    RIO_BOOL_T    mbox9;
    RIO_BOOL_T    mbox10;
    RIO_BOOL_T    mbox11;
    RIO_BOOL_T    mbox12;
    RIO_BOOL_T    mbox13;
    RIO_BOOL_T    mbox14;
    RIO_BOOL_T    mbox15;
    RIO_BOOL_T    mbox16;
    RIO_BOOL_T    mbox17;
    RIO_BOOL_T    mbox18;
    RIO_BOOL_T    mbox19;
    RIO_BOOL_T    mbox20;
    RIO_BOOL_T    mbox21;
    RIO_BOOL_T    mbox22;
    RIO_BOOL_T    mbox23;
    RIO_BOOL_T    mbox24;
    RIO_BOOL_T    mbox25;
    RIO_BOOL_T    mbox26;
    RIO_BOOL_T    mbox27;
    RIO_BOOL_T    mbox28;
    RIO_BOOL_T    mbox29;
    RIO_BOOL_T    mbox30;
    RIO_BOOL_T    mbox31;
    RIO_BOOL_T    mbox32;
    RIO_BOOL_T    mbox33;
    RIO_BOOL_T    mbox34;
    RIO_BOOL_T    mbox35;
    RIO_BOOL_T    mbox36;
    RIO_BOOL_T    mbox37;
    RIO_BOOL_T    mbox38;
    RIO_BOOL_T    mbox39;
    RIO_BOOL_T    mbox40;
    RIO_BOOL_T    mbox41;
    RIO_BOOL_T    mbox42;
    RIO_BOOL_T    mbox43;
    RIO_BOOL_T    mbox44;
    RIO_BOOL_T    mbox45;
    RIO_BOOL_T    mbox46;
    RIO_BOOL_T    mbox47;
    RIO_BOOL_T    mbox48;
    RIO_BOOL_T    mbox49;
    RIO_BOOL_T    mbox50;
    RIO_BOOL_T    mbox51;
    RIO_BOOL_T    mbox52;
    RIO_BOOL_T    mbox53;
    RIO_BOOL_T    mbox54;
    RIO_BOOL_T    mbox55;
    RIO_BOOL_T    mbox56;
    RIO_BOOL_T    mbox57;
    RIO_BOOL_T    mbox58;
    RIO_BOOL_T    mbox59;
    RIO_BOOL_T    mbox60;
    RIO_BOOL_T    mbox61;
    RIO_BOOL_T    mbox62;
    RIO_BOOL_T    mbox63;
    RIO_BOOL_T    mbox64;
    /* GDA : Bug#123 Support for extneded mailbox(xmbox) feature */

  

    RIO_UCHAR_T   inbound_Buffer_Size;
    RIO_UCHAR_T   outbound_Buffer_Size;

    /*GDA: Bug#43 Fix */
    RIO_BOOL_T    gda_Tx_Illegal_Packet;
    RIO_BOOL_T    gda_Rx_Illegal_Packet;
    /*GDA: Bug#43 Fix */

} RIO_LL_INST_PARAM_T;

/* callback tray to be used by the logical layer model */
typedef struct {
    /* callbacks provided by the local device model (environment) */
    RIO_LOCAL_RESPONSE            rio_Local_Response;
    RIO_GSM_REQUEST_DONE          rio_GSM_Request_Done;
    RIO_IO_REQUEST_DONE           rio_IO_Request_Done;
    RIO_MP_REQUEST_DONE           rio_MP_Request_Done;
    RIO_DOORBELL_REQUEST_DONE     rio_Doorbell_Request_Done;
    RIO_CONFIG_REQUEST_DONE       rio_Config_Request_Done;
    RIO_CONTEXT_T                 local_Device_Context;

    RIO_SNOOP_REQUEST             rio_Snoop_Request;
    RIO_CONTEXT_T                 snoop_Context;

    RIO_MP_REMOTE_REQUEST         rio_MP_Remote_Request;
    RIO_CONTEXT_T                 mailbox_Context;

    RIO_DOORBELL_REMOTE_REQUEST   rio_Doorbell_Remote_Request;
    RIO_CONTEXT_T                 doorbell_Context;

    RIO_PORT_WRITE_REMOTE_REQUEST rio_Port_Write_Remote_Request;
    RIO_CONTEXT_T                 port_Context;

    RIO_MEMORY_REQUEST            rio_Memory_Request;
    RIO_CONTEXT_T                 memory_Context;

    RIO_READ_DIRECTORY            rio_Read_Dir;
    RIO_WRITE_DIRECTORY           rio_Write_Dir;
    RIO_CONTEXT_T                 directory_Context;

    /* callbacks provided by the transport layer */
    RIO_SET_CONFIG_REG            rio_Set_Config_Reg;
    RIO_GET_CONFIG_REG            rio_Get_Config_Reg;
    RIO_CONTEXT_T                 rio_Config_Context;

    RIO_PL_GSM_REQUEST            rio_TL_GSM_Request;
    RIO_PL_IO_REQUEST             rio_TL_IO_Request;
    RIO_PL_MESSAGE_REQUEST        rio_TL_Message_Request;
    RIO_PL_DOORBELL_REQUEST       rio_TL_Doorbell_Request;
    RIO_PL_CONFIGURATION_REQUEST  rio_TL_Configuration_Request;
    RIO_PL_SEND_RESPONSE          rio_TL_Send_Response;
    RIO_CONTEXT_T                 rio_TL_Outbound_Context;

    RIO_PL_ACK_REMOTE_REQ         rio_TL_Ack_Remote_Req;
    RIO_CONTEXT_T                 rio_TL_Inbound_Context;

    /* UDFs */
    RIO_TRANSLATE_LOCAL_IO_REQ    rio_Tr_Local_IO;
    RIO_TRANSLATE_REMOTE_IO_REQ   rio_Tr_Remote_IO;
    RIO_ROUTE_GSM_REQ             rio_Route_GSM;
    RIO_CONTEXT_T                 address_Translation_Context;

    RIO_USER_MSG                  rio_User_Msg;
    RIO_CONTEXT_T                 rio_Msg;

    RIO_REMOTE_CONFIG_READ        rio_Remote_Conf_Read;
    RIO_REMOTE_CONFIG_WRITE       rio_Remote_Conf_Write;
    RIO_CONTEXT_T                 extended_Features_Context;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_DS_REQUEST_DONE           rio_DS_Request_Done; 
    RIO_PL_DS_REQUEST             rio_TL_DS_Request;  
    RIO_TRANSLATE_LOCAL_DS_REQ    rio_Tr_Local_DS;   
    RIO_TRANSLATE_REMOTE_DS_REQ   rio_Tr_Remote_DS;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_FC_REQUEST_DONE           rio_FC_Request_Done;
    RIO_PL_FC_REQUEST             rio_TL_FC_Request;
    RIO_PL_CONGESTION             rio_TL_Congestion;
    RIO_TRANSLATE_LOCAL_FC_REQ    rio_Tr_Local_FC;
    RIO_TRANSLATE_REMOTE_FC_REQ   rio_Tr_Remote_FC;
    /* GDA: Bug#125 */

} RIO_LL_CALLBACK_TRAY_T;


/* logical layer data structure */

typedef struct {

    /* array of pointers to transaction handlers */
    RIO_LL_HANDLER_TABLE_T *handler_Table; /* null-terminated array */

    /* tray of callbacks used to access environment or transport layer */
    RIO_LL_CALLBACK_TRAY_T  callback_Tray;

    /* local requests buffer */
    RIO_LL_BUFFER_T         *loc_Buffer;
    unsigned int            locb_Size;

    /* remote requests buffer */
    RIO_LL_BUFFER_T         *remote_Buffer;
    unsigned int            remoteb_Size;


    /* temporary buffer */
    RIO_LL_TEMP_BUFFER_T    temp_Buffer;

    /* target queues */
    RIO_LL_TARGET_QUEUES_T  target_Queues;

    /* instance's parameters */
    RIO_LL_PARAM_SET_T      init_Params;  /* parameters set during initialization */
    RIO_LL_INST_PARAM_T     inst_Params;  /* parameters set during instantiation */

    /* reset flag */
    RIO_BOOL_T              in_Reset;

    /* recursion semaphor */
    RIO_LL_SEMAPHOR_T       semaphor;

    /* GDA: Bug#125 - Support for Flow control packet */
    unsigned int            congestion_Counter;
    /* GDA: Bug#125 */

} RIO_LL_DS_T;

#endif /* RIO_LL_DS_H */
