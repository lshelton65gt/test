#ifndef RIO_LL_MODULE_H
#define RIO_LL_MODULE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/include/rio_ll_module.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains logical layer module interfaces to the transport 
*               layer, external request/response interface, memory interfaces 
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif /* RIO_TYPES_H */

/* functions to be called by environment to initialize instance */
int RIO_LL_Initialize(
    RIO_HANDLE_T              handle,
    RIO_LL_PARAM_SET_T        *param_Set
);

int RIO_LL_Start_Reset(
    RIO_HANDLE_T handle
);

/* functions allowing environment to request various transactions */
int RIO_LL_IO_Request(
    RIO_HANDLE_T handle, 
    RIO_IO_REQ_T *rq, 
    RIO_TAG_T    trx_Tag
);

int RIO_LL_GSM_Request(
    RIO_HANDLE_T    handle, 
    RIO_GSM_TTYPE_T ttype,
    RIO_GSM_REQ_T   *rq, 
    RIO_TAG_T       trx_Tag
);

int RIO_LL_Config_Request(
    RIO_HANDLE_T     handle, 
    RIO_CONFIG_REQ_T *rq, 
    RIO_TAG_T        trx_Tag
);

int RIO_LL_Message_Request(
    RIO_HANDLE_T      handle, 
    RIO_MESSAGE_REQ_T *rq, 
    RIO_TAG_T         trx_Tag
);

int RIO_LL_Doorbell_Request(
    RIO_HANDLE_T       handle, 
    RIO_DOORBELL_REQ_T *rq, 
    RIO_TAG_T          trx_Tag
);

/* GDA: Bug#124 - Support for Data Streaming packet */
int RIO_LL_DS_Request(
    RIO_HANDLE_T  handle,
    RIO_DS_REQ_T  *rq,
    RIO_TAG_T     trx_Tag
);

int RIO_LL_Get_Request_Data_Hw(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_HW_T      *data_hw
);
/* GDA: Bug#124 - Support for Data Streaming packet */


/* GDA: Bug#125 - Support for Flow control packet */
int RIO_LL_FC_Request(
    RIO_HANDLE_T       handle, 
    RIO_FC_REQ_T       *rq, 
    RIO_TAG_T          trx_Tag
);

int RIO_LL_Congestion(
    RIO_HANDLE_T       handle, 
    RIO_TAG_T          trx_Tag
);

int RIO_LL_FC_To_Send(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);
/* GDA: Bug#125 */


/* functions notifying the LL about event in the environment, such as memory or snoop responce */
int RIO_LL_Ack_Request(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);

int RIO_LL_Ack_Responce(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);

int RIO_LL_Request_Timeout(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);

int RIO_LL_Snoop_Response(
    RIO_HANDLE_T       handle, 
    RIO_SNOOP_RESULT_T snoop_Result, 
    RIO_DW_T           *data
);

int RIO_LL_Set_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *data
);

int RIO_LL_Get_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *data
);

int RIO_LL_Remote_Request(
    RIO_HANDLE_T         handle, 
    RIO_TAG_T            tag, 
    RIO_REMOTE_REQUEST_T *req, 
    RIO_TR_INFO_T        transport_Info
);

int RIO_LL_Remote_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
);

/* functions to be called by the transport layer to get data*/
int RIO_LL_Get_Request_Responce_Data(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
);

/* functions to be called by the transport level to get access to the extended features space */
int RIO_LL_Ext_Features_Read( 
    RIO_CONTEXT_T   handle, 
    RIO_CONF_OFFS_T config_Offset, 
    unsigned long   *data 
);

int RIO_LL_Ext_Features_Write( 
    RIO_CONTEXT_T   handle, 
    RIO_CONF_OFFS_T config_Offset, 
    unsigned long   data 
);


int RIO_LL_Delete_Instance(
    RIO_HANDLE_T    handle
);

#endif /* RIO_LL_MODULE_H */
