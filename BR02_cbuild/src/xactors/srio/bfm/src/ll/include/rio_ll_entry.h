#ifndef RIO_LL_ENTRY_H
#define RIO_LL_ENTRY_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/include/rio_ll_entry.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains logical layer module create functions which are 
*               entry points to the layer and allow to instantiate the layer
*               inside particular implementation of the RIO model configuration 
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_LL_INTERFACE_H
#include "rio_ll_interface.h"
#endif

#ifndef RIO_PL_INTERFACE_H
#include "rio_pl_interface.h"
#endif

#ifndef RIO_LL_DS_H
#include "rio_ll_ds.h"
#endif

#ifndef RIO_LL_MODULE_H
#include "rio_ll_module.h"
#endif

#ifndef RIO_LL_TRX_HANDLERS_H
#include "rio_ll_trx_handlers.h"
#endif



typedef int (*RIO_LL_INITIALIZE)(
    RIO_HANDLE_T              handle,
    RIO_LL_PARAM_SET_T        *param_Set
    );

typedef int (*RIO_LL_START_RESET)(
    RIO_HANDLE_T handle
    );


/* function tray to be passed to the environment by the logical layer model */
typedef struct {

    /* functions to access logical layer from local device */
    RIO_GSM_REQUEST             rio_GSM_Request;
    RIO_IO_REQUEST              rio_IO_Request;

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_FC_REQUEST              rio_FC_Request;
    RIO_CONGESTION              rio_Congestion;
    /* GDA: Bug#125 */

    RIO_MESSAGE_REQUEST         rio_Message_Request;
    RIO_DOORBELL_REQUEST        rio_Doorbell_Request;
    RIO_CONFIGURATION_REQUEST   rio_Conf_Request;
    RIO_SNOOP_RESPONSE          rio_Snoop_Response;
    RIO_SET_MEMORY_DATA         rio_Set_Mem_Data;
    RIO_GET_MEMORY_DATA         rio_Get_Mem_Data;
    
    RIO_LL_START_RESET          rio_Start_Reset;
    RIO_LL_INITIALIZE           rio_Init;

    /* functions to access logical layer from transport layer */
    RIO_PL_REMOTE_REQUEST       rio_Remote_Request;
    RIO_PL_REMOTE_RESPONSE      rio_Remote_Responce;

    RIO_REMOTE_CONFIG_READ      rio_Remote_Conf_Read;
    RIO_REMOTE_CONFIG_WRITE     rio_Remote_Conf_Write;

    RIO_PL_ACK_REQUEST          rio_Ack_Request;
    RIO_PL_GET_REQ_DATA         rio_Get_Req_Data;
    RIO_PL_GET_RESP_DATA        rio_Get_Resp_Data;
    RIO_PL_ACK_RESPONSE         rio_Ack_Response;
    RIO_PL_REQ_TIME_OUT         rio_Req_Time_Out;
    
    RIO_DELETE_INSTANCE         rio_LL_Delete_Instance;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_GET_REQ_DATA_HW      rio_Get_Req_Data_Hw; 
    RIO_DS_REQUEST              rio_DS_Request;      
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_PL_FC_TO_SEND           rio_FC_To_Send;
    /* GDA: Bug#125 */

} RIO_LL_FTRAY_T;


/* the function creates instance of RIO model and returns handle to its data structure */
int RIO_LL_Create_Instance(
    RIO_HANDLE_T               *handle,
    RIO_LL_INST_PARAM_T        *param   
    );


/* the function returns RIO model interface functions to be used for model access */
int RIO_LL_Get_Function_Tray(
    RIO_LL_FTRAY_T    *ftray             /* pointer to the RIO model function tray */
    );


/* the function binds RIO model instance to the environment */
int RIO_LL_Bind_Instance(
    RIO_HANDLE_T                handle,
    RIO_LL_CALLBACK_TRAY_T      *ctray   /* environment callbacks to be registered inside RIO model instance */
    );


#endif /* RIO_LL_ENTRY_H */
