/*****************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_kernel.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains implementation of logical layer kernel 
*               functionality, which includes logical layer interfaces
*               and internal kernel interface used by the transaction 
*               handlers
*
* Notes:        
*
******************************************************************************/

#include <stdio.h>

#include "rio_ll_ds.h"
#include "rio_ll_module.h"
#include "rio_ll_kernel.h"
#include "rio_ll_errors.h"

#include "rio_common.h"	/* GDA  - GDA_PKT_SIZE is defined */
#include <assert.h>

/* definitions for buffer tags */
#define RIO_LL_LOCAL_BUFFER_START  0x1000
#define RIO_LL_REMOTE_BUFFER_START 0x8000

/* address masks for the addresses correction */
#define RIO_LL_ADDRESS_WORD_ALIGN    0xFFFFFFFC
#define RIO_LL_ADDRESS_DW_ALIGN      0xFFFFFFF8
#define RIO_LL_ADDRESS_GRAN_32_ALIGN 0xFFFFFFE0
#define RIO_LL_ADDRESS_GRAN_64_ALIGN 0xFFFFFFC0

/* masks for the destination transaction register and source transaction register */
#define RIO_MASK_READ          0x80000000
#define RIO_MASK_IREAD         0x40000000
#define RIO_MASK_READ_TO_OWN   0x20000000
#define RIO_MASK_DKILL         0x10000000
#define RIO_MASK_CASTOUT       0x08000000
#define RIO_MASK_FLUSH         0x04000000
#define RIO_MASK_IO_READ       0x02000000
#define RIO_MASK_IKILL         0x01000000
#define RIO_MASK_TLBIE         0x00800000
#define RIO_MASK_TLBSYNC       0x00400000

#define RIO_MASK_NREAD         0x00008000
#define RIO_MASK_NWRITE        0x00004000
#define RIO_MASK_SWRITE        0x00002000
#define RIO_MASK_NWRITE_R      0x00001000

#define RIO_MASK_MESSAGE       0x00000800
#define RIO_MASK_DOORBELL      0x00000400

#define RIO_MASK_ATOMIC_TSWAP  0x00000100
#define RIO_MASK_ATOMIC_INC    0x00000080
#define RIO_MASK_ATOMIC_DEC    0x00000040
#define RIO_MASK_ATOMIC_SET    0x00000020
#define RIO_MASK_ATOMIC_CLEAR  0x00000010
#define RIO_MASK_ATOMIC_SWAP   0x00000008  /* GDA: Bug#133 - Support for ttype C for type 5 packet 
						   Refer part1:I/O Logical Spec, Rev. 1.3 (pg 50) */

/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_MASK_DS            0x00040000 
/* GDA: Bug#124 */

/* GDA: Bug#125 - Support for Flow control packet */
#define RIO_MASK_FC            0x00000080
/* GDA: Bug#125 */

#define RIO_MASK_PORT_WRITE    0x00000004


/* source message CSR masks */
#define RIO_MESSAGE_CSR_PAYLOAD_SIZE_MASK 0xF0000000
#define RIO_MESSAGE_CSR_SEGMENT_SIZE_MASK 0x0000F000
#define RIO_DEST_PACKET_CSR_SIZE_MASK     0xF0000000
#define RIO_DEST_RESP_CSR_SIZE_MASK       0x0F000000
#define RIO_SOURCE_RESP_CSR_SIZE_MASK     0x0F000000
#define RIO_DEST_PORT_WRITE_CSR_SIZE_MASK 0x00F00000

/* source message CSR offsets */
#define RIO_SOURCE_OPS_CAR_OFFSET          0x18
#define RIO_DEST_OPS_CAR_OFFSET            0x1C
#define RIO_SOURCE_PACKET_CONF_CSR_OFFSET  0x48
#define RIO_DEST_PACKET_CONF_CSR_OFFSET    0x4C
#define RIO_SOURCE_MESSAGE_CONF_CSR_OFFSET 0x50
#define RIO_DEST_MESSAGE_CONF_CSR_OFFSET   0x54
#define RIO_LCSHBAR_OFFSET                 0x58
#define RIO_LCSBAR_OFFSET                  0x5C


/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_DS_OPS_CAR_OFFSET              0x3C  
#define RIO_LL_PE_LL_CSR_DSTREAM_SUPPORT_MASK  0xFF    
#define RIO_LL_PE_LL_CSR_DSTREAM_INFO_MASK     0xFFFFFFFF  
/* GDA: Bug#124 - Support for Data Streaming packet */

/* mask to clear msbs in extended address if it is 16 bit */
#define RIO_EXT_ADDRESS_16_MASK            0xFFFF

/* WORD offset of Processing Element Logical Layer CSR */
#define RIO_LL_PE_LL_CSR 0x4C

/* word address of the Base Device ID CSR */
#define RIO_LL_BASE_ADDRESS_CSR 0x60

/* mask to retrieve extended addressing support bits */
#define RIO_LL_PE_LL_CSR_EXT_ADDR_SUPPORT_MASK 0x7

/* masks for the transport info processing in case of the TABLE BASED routing */
#define RIO_LL_TR_INFO_16_SRC_MASK  0x000000FF
#define RIO_LL_TR_INFO_32_SRC_MASK  0x0000FFFF

/* extended addressing support encondigs of PE LL CSR */
#define RIO_LL_PE_LL_CSR_34 0x00000001
#define RIO_LL_PE_LL_CSR_50 0x00000002
#define RIO_LL_PE_LL_CSR_66 0x00000004



/*                                                                                        */
/* Below are collision resolution tables defined accordingly to the "RIO GSM Logical ..." */
/* The index in each table is the RIO_PL_GSM_TTYPE_T value for the incoming transaction   */
/* There are no tables for the TLBIE, TLBSYNC, IKILL because of these transactions are    */
/* never makes a collision                                                                */
/*                                                                                        */

/* outstanding READ_HOME */
static RIO_LL_COLLISION_T cr_Table_READ_HOME[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming READ_OWNER        */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_STALL,               /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

/* outstanding IREAD_HOME */
static RIO_LL_COLLISION_T cr_Table_IREAD_HOME[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming READ_OWNER        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

/* outstanding READ_OWNER */
static RIO_LL_COLLISION_T cr_Table_READ_OWNER[] = {
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming CASTOUT           */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_ERROR_RESPONSE       /* incoming IKILL_SHARER      */
};

/* outstanding READ_TO_OWN_HOME */
static RIO_LL_COLLISION_T cr_Table_READ_TO_OWN_HOME[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_STALL,               /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_STALL,               /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_STALL,               /* incoming READ_OWNER        */
    RIO_LL_COLLISION_STALL,               /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_STALL,               /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_STALL,               /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

/* outstanding READ_TO_OWN_OWNER */
static RIO_LL_COLLISION_T cr_Table_READ_TO_OWN_OWNER[] = {
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming CASTOUT           */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_ERROR_RESPONSE       /* incoming IKILL_SHARER      */
};

/* outstanding DKILL_HOME */
static RIO_LL_COLLISION_T cr_Table_DKILL_HOME[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_STALL,               /* incoming READ_OWNER        */
    RIO_LL_COLLISION_STALL,               /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_STALL,               /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_STALL,               /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

/* outstanding DKILL_SHARER */
static RIO_LL_COLLISION_T cr_Table_DKILL_SHARER[] = {
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_ERROR_RESPONSE       /* incoming IKILL_SHARER      */
};

/* outstanding IKILL_HOME */
static RIO_LL_COLLISION_T cr_Table_IKILL_HOME[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming READ_OWNER        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

/* outstanding IKILL_SHARER */
static RIO_LL_COLLISION_T cr_Table_IKILL_SHARER[] = {
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming READ_HOME         */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming CASTOUT           */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_ERROR_RESPONSE       /* incoming IKILL_SHARER      */
};


/* outstanding CASTOUT */
static RIO_LL_COLLISION_T cr_Table_CASTOUT[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,       /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

/* outstanding participant FLUSH */
static RIO_LL_COLLISION_T cr_Table_FLUSH_Participant[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming READ_OWNER        */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_STALL,               /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

#ifndef _CARBON_SRIO_XTOR_FIX
/* outstanding non-participant FLUSH */
static RIO_LL_COLLISION_T cr_Table_FLUSH_Non_Participant[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};
#endif

/* outstanding participant IO_READ_HOME */
static RIO_LL_COLLISION_T cr_Table_IO_READ_HOME_Participant[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming READ_OWNER        */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_NOT_OWNER_RESPONSE,  /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_STALL,               /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};

#ifndef _CARBON_SRIO_XTOR_FIX
/* outstanding non-participant IO_READ_HOME */
static RIO_LL_COLLISION_T cr_Table_IO_READ_HOME_Non_Participant[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_NO_COLLISION         /* incoming IKILL_SHARER      */
};
#endif

/* outstanding participant IO_READ_OWNER */
static RIO_LL_COLLISION_T cr_Table_IO_READ_OWNER_Participant[] = {
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming CASTOUT           */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_RETRY_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_ERROR_RESPONSE       /* incoming IKILL_SHARER      */
};

#ifndef _CARBON_SRIO_XTOR_FIX
/* outstanding non-participant IO_READ_OWNER */
static RIO_LL_COLLISION_T cr_Table_IO_READ_OWNER_Non_Participant[] = {
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_HOME         */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IREAD_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_HOME  */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming CASTOUT           */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_HOME        */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBIE             */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming TLBSYNC           */
    RIO_LL_COLLISION_NO_COLLISION,        /* incoming IKILL_HOME        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_HOME      */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WO_DATA     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming FLUSH_WITH_DATA   */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_OWNER        */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming READ_TO_OWN_OWNER */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming IO_READ_OWNER     */
    RIO_LL_COLLISION_ERROR_RESPONSE,      /* incoming DKILL_SHARER      */
    RIO_LL_COLLISION_ERROR_RESPONSE       /* incoming IKILL_SHARER      */
};
#endif

/* End of the collision resolution tables */
/* NOTE: tables for the non-participant transactions are defined but aren't */
/* used at this time                                                        */


/* rows - wdptr, columns - rdsize, cells - byte_num, offset, dw_Size */
static RIO_LL_WDPTR_TO_SIZE_T wdptr_To_Read_Size_Translation_Table[2][16] = {
    {
        {RIO_LL_ALLOWED, 0, 1, 1},    /* 1000_0000 */
        {RIO_LL_ALLOWED, 1, 1, 1},    /* 0100_0000 */
        {RIO_LL_ALLOWED, 2, 1, 1},    /* 0010_0000 */
        {RIO_LL_ALLOWED, 3, 1, 1},    /* 0001_0000 */
        {RIO_LL_ALLOWED, 0, 2, 1},    /* 1100_0000 */
        {RIO_LL_ALLOWED, 0, 3, 1},    /* 1110_0000 */
        {RIO_LL_ALLOWED, 2, 2, 1},    /* 0011_0000 */
        {RIO_LL_ALLOWED, 0, 5, 1},    /* 1111_1000 */
        {RIO_LL_ALLOWED, 0, 4, 1},    /* 1111_0000 */
        {RIO_LL_ALLOWED, 0, 6, 1},    /* 1111_1100 */
        {RIO_LL_ALLOWED, 0, 7, 1},    /* 1111_1110 */
        {RIO_LL_ALLOWED, 0, 8, 1},    /* 1111_1111 */
        {RIO_LL_ALLOWED, 0, 0, 4},    /* 32 bytes  */
        {RIO_LL_ALLOWED, 0, 0, 12},   /* 96 bytes  */
        {RIO_LL_ALLOWED, 0, 0, 20},   /* 160 bytes */
        {RIO_LL_ALLOWED, 0, 0, 28}    /* 224 bytes */
    },
    {
        {RIO_LL_ALLOWED, 4, 1, 1},    /* 0000_1000 */
        {RIO_LL_ALLOWED, 5, 1, 1},    /* 0000_0100 */
        {RIO_LL_ALLOWED, 6, 1, 1},    /* 0000_0010 */
        {RIO_LL_ALLOWED, 7, 1, 1},    /* 0000_0001 */
        {RIO_LL_ALLOWED, 4, 2, 1},    /* 0000_1100 */
        {RIO_LL_ALLOWED, 5, 3, 1},    /* 0000_0111 */
        {RIO_LL_ALLOWED, 6, 2, 1},    /* 0000_0011 */
        {RIO_LL_ALLOWED, 3, 5, 1},    /* 0001_1111 */
        {RIO_LL_ALLOWED, 4, 4, 1},    /* 0000_1111 */
        {RIO_LL_ALLOWED, 2, 6, 1},    /* 0011_1111 */
        {RIO_LL_ALLOWED, 1, 7, 1},    /* 0111_1111 */
        {RIO_LL_ALLOWED, 0, 0, 2},    /* 16 bytes  */
        {RIO_LL_ALLOWED, 0, 0, 8},    /* 64 bytes  */
        {RIO_LL_ALLOWED, 0, 0, 16},   /* 128 bytes  */
        {RIO_LL_ALLOWED, 0, 0, 24},   /* 192 bytes */
        {RIO_LL_ALLOWED, 0, 0, GDA_PKT_SIZE}    /* GDA */
    }    
};

/* rows - wdptr, columns - rdsize, cells - byte_num, offset, dw_Size */
static RIO_LL_WDPTR_TO_SIZE_T wdptr_To_Write_Size_Translation_Table[2][16] = {
    {
        {RIO_LL_ALLOWED, 0, 1, 1},    /* 1000_0000 */
        {RIO_LL_ALLOWED, 1, 1, 1},    /* 0100_0000 */
        {RIO_LL_ALLOWED, 2, 1, 1},    /* 0010_0000 */
        {RIO_LL_ALLOWED, 3, 1, 1},    /* 0001_0000 */
        {RIO_LL_ALLOWED, 0, 2, 1},    /* 1100_0000 */
        {RIO_LL_ALLOWED, 0, 3, 1},    /* 1110_0000 */
        {RIO_LL_ALLOWED, 2, 2, 1},    /* 0011_0000 */
        {RIO_LL_ALLOWED, 0, 5, 1},    /* 1111_1000 */
        {RIO_LL_ALLOWED, 0, 4, 1},    /* 1111_0000 */
        {RIO_LL_ALLOWED, 0, 6, 1},    /* 1111_1100 */
        {RIO_LL_ALLOWED, 0, 7, 1},    /* 1111_1110 */
        {RIO_LL_ALLOWED, 0, 8, 1},    /* 1111_1111 */
        {RIO_LL_ALLOWED, 0, 0, 4},    /* 32 bytes  */
        {RIO_LL_RESERVED, 0, 0, 0},   /* 96 bytes is reserved */
        {RIO_LL_RESERVED, 0, 0, 0},   /* 160 bytes is reserved */
        {RIO_LL_RESERVED, 0, 0, 0}    /* 224 bytes is reserved */
    },
    {
        {RIO_LL_ALLOWED, 4, 1, 1},    /* 0000_1000 */
        {RIO_LL_ALLOWED, 5, 1, 1},    /* 0000_0100 */
        {RIO_LL_ALLOWED, 6, 1, 1},    /* 0000_0010 */
        {RIO_LL_ALLOWED, 7, 1, 1},    /* 0000_0001 */
        {RIO_LL_ALLOWED, 4, 2, 1},    /* 0000_1100 */
        {RIO_LL_ALLOWED, 5, 3, 1},    /* 0000_0111 */
        {RIO_LL_ALLOWED, 6, 2, 1},    /* 0000_0011 */
        {RIO_LL_ALLOWED, 3, 5, 1},    /* 0001_1111 */
        {RIO_LL_ALLOWED, 4, 4, 1},    /* 0000_1111 */
        {RIO_LL_ALLOWED, 2, 6, 1},    /* 0011_1111 */
        {RIO_LL_ALLOWED, 1, 7, 1},    /* 0111_1111 */
        {RIO_LL_ALLOWED, 0, 0, 2},    /* 16 bytes  */
        {RIO_LL_ALLOWED, 0, 0, 8},    /* 64 bytes  */
        {RIO_LL_ALLOWED, 0, 0, 16},   /* 128 bytes  */
        {RIO_LL_RESERVED, 0, 0, 0},   /* 192 bytes is reserved */
        {RIO_LL_ALLOWED, 0, 0, GDA_PKT_SIZE} /* GDA */
    }    
};


/* function which find transaction handler for the transaction of given type */
static RIO_LL_TRX_HANDLER_T find_Handler( 
    RIO_LL_DS_T *handle, 
    RIO_BOOL_T  is_Local, 
    RIO_LL_FTYPE_T ftype, 
    RIO_CONF_TTYPE_T ttype
);

/* function finds error handler for the transaction of the given ttype and ftype */
static RIO_LL_TRX_HANDLER_T find_Error_Handler( 
    RIO_LL_DS_T *handle, 
    RIO_UCHAR_T ttype, 
    RIO_UCHAR_T ftype
);

/* functions which works with queues (perform abstract operations) */
static RIO_LL_QUEUE_T* get_Queue_Element_From_Pool( RIO_LL_QUEUE_T **pool);
static int return_Queue_Element_To_Pool( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **pool);
static int change_Queue_Element_State( RIO_LL_QUEUE_T *element, RIO_LL_Q_EL_STATE_T new_State);
static int insert_Element_To_Queue_Prio( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **head);
static int insert_Element_To_Queue_End( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **head);
static int remove_Element_From_Queue( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **head);
static RIO_LL_QUEUE_T* find_Queue_Element_With_State( RIO_LL_QUEUE_T *start, RIO_LL_Q_EL_STATE_T state);

/* functions for target queues servicing */
static int service_Target_Queues( RIO_LL_DS_T *handle );
static int service_Memory_Queue( RIO_LL_DS_T *handle );
static int service_Snoop_Queue( RIO_LL_DS_T *handle );
static int service_Request_Responce_Queues( RIO_LL_DS_T *handle );

/* functions which works with buffers */
static RIO_LL_BUF_TAG_T allocate_Buffer_Entry( RIO_LL_DS_T *handle, RIO_UCHAR_T prio, RIO_BOOL_T is_Local );
static int              deallocate_Buffer_Entry(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag);

/* returns pointer to the buffer entry which corresponds to tag */
static RIO_LL_BUFFER_T* get_Buffer_Entry(const RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T tag);

/* function to check reset state */
static RIO_BOOL_T is_In_Reset(const RIO_LL_DS_T *handle);

/* functions working with semaphor */
static void add_Semaphor_State( RIO_LL_DS_T *handle, RIO_LL_SEMAPHOR_T state);
static void remove_Semaphor_State( RIO_LL_DS_T *handle, RIO_LL_SEMAPHOR_T state);
static RIO_BOOL_T check_Semaphor_State( RIO_LL_DS_T *handle, RIO_LL_SEMAPHOR_T state);

/* functions which check the various parameters such as serviced transactions and data payload sizes*/
static RIO_BOOL_T can_Service( RIO_LL_DS_T *handle, RIO_FTYPE_T ftype, RIO_UCHAR_T ttype );
static RIO_BOOL_T can_Issue( RIO_LL_DS_T *handle, RIO_LL_FTYPE_T ftype, RIO_CONF_TTYPE_T ttype );

/* functions return values of the various configuration registers */
static RIO_WORD_T max_Source_Message_Size( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox );
static RIO_WORD_T max_Source_Message_Seg( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox );
static RIO_WORD_T max_Dest_Message_Size( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox );
static RIO_WORD_T max_Dest_Message_Seg( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox );
static RIO_WORD_T max_Dest_Packet_Size( RIO_LL_DS_T *handle );
static RIO_WORD_T max_Source_Resp_Size( RIO_LL_DS_T *handle );
static RIO_WORD_T max_Dest_Port_Write_Size( RIO_LL_DS_T *handle );
static RIO_WORD_T get_LCSBAR( RIO_LL_DS_T *handle );
static RIO_WORD_T get_LCSHBAR( RIO_LL_DS_T *handle );


/* function tryes to process some transactions or get incoming transaction from temporary buffer */
static int process_Transaction( 
    RIO_LL_DS_T        *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_EVENT_T event
);

/* functions defines if there is collision with some other transactions */
static RIO_BOOL_T is_IO_Local_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag ); 
static RIO_BOOL_T is_IO_Remote_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag ); 
static RIO_BOOL_T is_GSM_Local_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag ); 
static RIO_BOOL_T is_GSM_Remote_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag, RIO_LL_TRX_EVENT_T *event); 

/* function takes transaction from the temporary buffer */
static int get_Transaction_From_Temp_Buffer( 
    RIO_LL_DS_T *handle, 
    RIO_LL_BUF_TAG_T *buf_Tag, 
    RIO_LL_TRX_EVENT_T *event 
);

/* functions translating various values */
static int translate_LL_FType_To_Spec_FType(
    RIO_LL_FTYPE_T ll_FType, 
    RIO_IO_TTYPE_T ll_TType, 
    RIO_FTYPE_T *spec_FType, 
    RIO_UCHAR_T *spec_TType
);

/*                                                                            */
/* this function translates wdptr & wrsize/rdsize values into the size of the */
/* transactions data payload in bytes and doublewords                         */
/* all constants are used in this function have their values accordingly to   */
/* the RIO interconnect specification                                         */
/*                                                                            */
static int translate_WDPTR_To_DW_Size(
    RIO_UCHAR_T wdptr,
    RIO_UCHAR_T rdsize,
    RIO_BOOL_T  is_RDSIZE,  /* tells whether read or write request */
    RIO_UCHAR_T *offset,
    RIO_UCHAR_T *byte_Num,
    RIO_UCHAR_T *dw_Size
);

/* translates ssize to dw_Size for message transactions */
static int translate_SSIZE_To_DW_Size(
    RIO_UCHAR_T ssize,
    RIO_UCHAR_T *dw_Size
);

/* functions checking various values */
static int check_Packet_Data_Size(
    RIO_LL_DS_T *handle,
    RIO_UCHAR_T ftype,
    RIO_UCHAR_T ttype,
    RIO_UCHAR_T wdptr,
    RIO_UCHAR_T rdsize,
    RIO_UCHAR_T mbox
);

static RIO_BOOL_T is_GSM_Transaction( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag);
static RIO_BOOL_T is_IO_Transaction( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag);

static unsigned long get_Config_Reg_Field(RIO_LL_DS_T *handle, RIO_LL_CONFIG_FIELD_T field);

/* GDA: Bug#124 - Support for Data Streaming packet */
static RIO_BOOL_T is_DS_Transaction( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag);

static int RIO_LL_Make_Segment(  
    RIO_HANDLE_T   handle,
    RIO_DS_REQ_T   *rq,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
);
/* GDA: Bug#124 */

/* GDA: Bug#125 - Support for Flow control packet */
static RIO_BOOL_T is_FC_Transaction(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag);
/* GDA: Bug#125 */


/***************************************************************************
 * Function :    RIO_LL_IO_Request
 *
 * Description:  function receives data structure describing necessary 
 *               IO. If data is correct buffer is assigned
 *               and request is processed by its transaction handler.
 *
 * Returns:      RIO_OK in case of successfully accepted request.
 *               RIO_PARAM_INVALID in case of incorrect data.
 *               RIO_RETRY in case of unavailable buffer entry.
 *               RIO_ERROR in case of error
 *               
 * Notes:
 *
 **************************************************************************/

int RIO_LL_IO_Request( 
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    )
{
    RIO_LL_DS_T      *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T  *buf_Entry;  /* pointer to the buffer entry to store the transaction */
    RIO_LL_BUF_TAG_T buf_Tag;     /* buffer tag of the buffer entry                       */
    RIO_IO_REQ_T     *buf_Req;    /* copy of structure to place it to buffer              */
    RIO_DW_T         *data;       /* copy of the request data to place it to buffer       */
    int              i;
    static int        j;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;


    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "IO request");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "IO request");
        return RIO_OK;
    }

    /* check that user has requested transaction with correct attributes */
    if( rq == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "IO request" );
        return RIO_PARAM_INVALID;
    }

    /* check priority of the request */
    if( rq->prio > RIO_MAX_REQUEST_PRIORITY )
    {
        /* priority of the request packet can not be larger than 2 */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "IO", RIO_MAX_REQUEST_PRIORITY );
        return RIO_PARAM_INVALID;
    }

    /*printf("\ngda_Tx_Illegal_Packet:%u  gda_Rx_Illegal_Packet:%u\n",
                ll_Data->inst_Params.gda_Tx_Illegal_Packet,
       		ll_Data->inst_Params.gda_Rx_Illegal_Packet);*/

     /* check size of the packet */
     if((rq->dw_Size > 32) && (!(ll_Data->inst_Params.gda_Tx_Illegal_Packet)))/* GDA - Global Variable
							       			 to allow/reject data if 
							       			 greater than 32
    if( rq->dw_Size > RIO_LL_MAX_PACKET_DW_SIZE )*/
    {
        /* the size of the transaction is larger than allowed 256 bytes */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, "IO", 32 );
        return RIO_PARAM_INVALID;
    }

    if (rq->dw_Size == 0)
    {
        /* request shall have size bigger than null */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, "IO" );
        return RIO_PARAM_INVALID;
    }

    if (rq->dw_Size != 1 &&             /* one doubleword data payload         */
        rq->dw_Size != 2 &&             /* two doublewords data payload        */
        rq->dw_Size != 4 &&             /* four doublewords data payload       */
        rq->dw_Size != 8 &&             /* eight doublewords data payload      */
        rq->dw_Size != 12 &&            /* twelve doublewords data payload     */
        rq->dw_Size != 16 &&            /* sixteen doublewords data payload    */
        rq->dw_Size != 20 &&            /* twenty doublewords data payload     */
        rq->dw_Size != 24 &&            /* twenty two doublewords data payload */
        rq->dw_Size != 28 &&            /* twenty eight doublewords data payload */
        rq->dw_Size != 32 &&  		/* thirty two doublewords data payload */
        rq->ttype != RIO_IO_NWRITE &&
        rq->ttype != RIO_IO_NWRITE_R &&
        rq->ttype != RIO_IO_SWRITE
        )
    {
        /* illegal request size */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_LOCAL_DATA_PAYLOAD, 
            "IO read", "1, 2, 4, 8, 12, 16, 20, 24, 28, 32", rq->dw_Size);
        return RIO_PARAM_INVALID;
    }

    if( (rq->dw_Size == 1) && 
        (rq->byte_Num == 0) )
    {
        /* user has requested subdoubleword transaction without data */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_SUB_DW_DATA_PAYLOAD, "IO");
        return RIO_PARAM_INVALID;
    }

    if( (rq->dw_Size == 1) && 
        (rq->offset + rq->byte_Num > RIO_LL_DW_BYTE_SIZE) )
    {
        /* user has requested transaction which crosses double-word boundary */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CROSS_BOUND, "IO" );
        return RIO_PARAM_INVALID;
    }

    /* check data presence in the request structure */
    if( (rq->ttype == RIO_IO_NWRITE     || 
        rq->ttype == RIO_IO_SWRITE      ||
        rq->ttype == RIO_IO_NWRITE_R    ||
	rq->ttype == RIO_IO_ATOMIC_SWAP || /* GDA: Bug#133 - Support for ttype C for type 5 packet */
        rq->ttype == RIO_IO_ATOMIC_TSWAP) &&
        rq->data == NULL
    )
    {
        /* these transactions should supply data */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, "IO" );
        return RIO_PARAM_INVALID;
    }

    /* check data size for the atomic transactions */
    if((rq->ttype == RIO_IO_ATOMIC_INC 	 ||
        rq->ttype == RIO_IO_ATOMIC_DEC   ||
        rq->ttype == RIO_IO_ATOMIC_SWAP  || /* GDA: Bug#133 - Support for ttype C for type 5 packet */
        rq->ttype == RIO_IO_ATOMIC_TSWAP ||
        rq->ttype == RIO_IO_ATOMIC_SET   ||
        rq->ttype == RIO_IO_ATOMIC_CLEAR)&&
        (rq->dw_Size != 1 || rq->byte_Num > RIO_LL_MAX_ATOMIC_BYTE_SIZE || rq->byte_Num == 3 )
        )
    {
        /* atomic operations can not be larger than 4 bytes and can not be 3 bytes */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_ATOMIC_WRONG_SIZE );
        return RIO_PARAM_INVALID;
    }
    

    /* check data size for the SWrite transaction */
    if ( (rq->ttype == RIO_IO_SWRITE) && (rq->dw_Size == 1) && (rq->byte_Num < RIO_LL_DW_BYTE_SIZE) )
    {
        /* SWrite transaction doesn't support subdoubleword data payload */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_SWRITE_SUB_DW );
        return RIO_PARAM_INVALID;
    }

    if( get_Config_Reg_Field( ll_Data, RIO_LL_EXT_ADDRESSING_SUPPORT ) == RIO_LL_PE_LL_CSR_50 ) 
        /* 16-bit extended address; clear significant bytes in the extended address */
        rq->extended_Address = rq->extended_Address & RIO_EXT_ADDRESS_16_MASK;
    else if ( get_Config_Reg_Field( ll_Data, RIO_LL_EXT_ADDRESSING_SUPPORT ) == RIO_LL_PE_LL_CSR_34 )
        /* clear entire extended address */
        rq->extended_Address = 0;

    /* data is correct. Check if PE can issue such a transaction */
    if ( !can_Issue( ll_Data, RIO_LL_IO, rq->ttype) )
        RIO_LL_Message( ll_Data, RIO_LL_WARN_CANNOT_ISSUE, "IO", rq->ttype );

/* GDA: Bug#125 - Support for Flow control packet 
printf("\n xonoff at iorequwest is %d %d  \n",ll_Data->temp_Buffer.x_On_Off,ll_Data->congestion_Counter); */
   if(ll_Data->remote_Buffer->trx_Info.x_On_Off ==1 || ll_Data->temp_Buffer.x_On_Off==1)
   {
         printf("\n At congestion \n"); 
          j++; 
	  ll_Data->congestion_Counter++;
/*printf("\nll_Data->congestion_Counter is  %d  \n",ll_Data->congestion_Counter);*/
          if(ll_Data->congestion_Counter > RIO_MAX_DELAY_ORPHAND_XON)  /*RIO_MAX_CONGESTION_COUNTER_SIZE*/   
	  {
          ll_Data->remote_Buffer->trx_Info.x_On_Off=0;
	  ll_Data->temp_Buffer.x_On_Off=0;
	  }
          else 
           return  RIO_CONGES;   /*RIO_CONGESTION;*/
   }
  
   ll_Data->congestion_Counter=0;

/* GDA: Bug#125 - Support for Flow control packet */


    /* data is correct try to allocate buffer for it */
    if ( (buf_Tag = allocate_Buffer_Entry( ll_Data, rq->prio, RIO_TRUE)) == RIO_ERROR)
        return RIO_RETRY;

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "IO request" );
        return RIO_ERROR;
    }

    /* do filling buffer structure */

    /* find handler for the transation */
    buf_Entry->kernel_Info.handler = find_Handler(
        ll_Data,
        RIO_TRUE,
        RIO_LL_IO,
        rq->ttype
    );
    if (buf_Entry->kernel_Info.handler == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_LOCAL_NO_HANDLER, "IO", rq->ttype );
        return RIO_ERROR;
    }

    /* perform address translation and set local/remote attribute in the buffer */
    if( ll_Data->callback_Tray.rio_Tr_Local_IO == NULL )
    /* no address translation UDF, make local request by default, perform no address translation */
        buf_Entry->kernel_Info.is_Local = RIO_TRUE;
    else
        buf_Entry->kernel_Info.is_Local = (ll_Data->callback_Tray.rio_Tr_Local_IO(
            ll_Data->callback_Tray.address_Translation_Context,
            &(rq->address), 
            &(rq->extended_Address), 
            &(rq->xamsbs),
            &(buf_Entry->trx_Info.tr_Info[0])) == RIO_ROUTE_TO_LOCAL ) ? RIO_TRUE : RIO_FALSE;


    /*Bug#104*/
    buf_Entry->trx_Info.tr_Info[0]= rq->destId;
    /*Bug#104*/

    buf_Entry->trx_Info.multicast_Size = 1; /* there is no mulaticast ability for the IO requests */

    /* create copy of the request structure           */
    buf_Req = ( RIO_IO_REQ_T* )malloc( sizeof( RIO_IO_REQ_T ) );
    if ( buf_Req == NULL )
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, sizeof( RIO_IO_REQ_T), "IO request structure");
        return RIO_ERROR;
    }
    *buf_Req = *rq;

    /* do proper aligment of the offset in the copy of the request */
    buf_Req->address = buf_Req->address & RIO_LL_ADDRESS_DW_ALIGN;

    /* create copy of the data in the request structure */
    if ( rq->ttype == RIO_IO_NWRITE 	||
        rq->ttype == RIO_IO_SWRITE 	||
        rq->ttype == RIO_IO_NWRITE_R 	||
        rq->ttype == RIO_IO_ATOMIC_SWAP	|| /* GDA: Bug#133 - Support for ttype C for type 5 packet */
        rq->ttype == RIO_IO_ATOMIC_TSWAP)
    {
        data = ( RIO_DW_T* )malloc( rq->dw_Size * sizeof( RIO_DW_T ) );
        if ( data == NULL )
        {
            deallocate_Buffer_Entry( ll_Data, buf_Tag );    
            free( buf_Req );
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
                rq->dw_Size * sizeof( RIO_DW_T), "IO request data payload");
            return RIO_ERROR;
        }

        /* fill in copy of data */
        for ( i = 0; i < rq->dw_Size; i++ )
            data[i] = rq->data[i];

    }
    else 
        data = NULL;
    buf_Req->data = data;

    /* fill in transaction info buffer structure */ 
    buf_Entry->trx_Info.prio         = buf_Req->prio;
    buf_Entry->trx_Info.user_Trx_Tag = trx_Tag;
    translate_LL_FType_To_Spec_FType( 
        RIO_LL_IO, 
        rq->ttype, 
        &(buf_Entry->trx_Info.req_FType),
        &(buf_Entry->trx_Info.req_TType)
    );
    buf_Entry->trx_Info.req_Type    = RIO_LL_IO;
    buf_Entry->trx_Info.req         = buf_Req;
    buf_Entry->trx_Info.address     = buf_Req->address;
    buf_Entry->trx_Info.extended_Address = buf_Req->extended_Address;
    buf_Entry->trx_Info.xamsbs      = buf_Req->xamsbs;
    buf_Entry->trx_Info.offset      = buf_Req->offset;
    buf_Entry->trx_Info.byte_Num    = buf_Req->byte_Num;
    buf_Entry->trx_Info.dw_Size     = buf_Req->dw_Size;

    if ( rq->data != NULL )
    {
        for ( i = 0; i < rq->dw_Size; i++ )
            buf_Entry->trx_Info.data[i] = rq->data[i];
    }

    /* cancel transaction if there is a collision with some earlier requested transaction */
    if ( is_IO_Local_Collision( ll_Data, buf_Tag ) )
    {
        /* free allocated memory */
        if ( buf_Req->data != NULL)
            free( buf_Req->data );
        free( buf_Req );
        buf_Entry->trx_Info.req = NULL;

        /* deallocate buffer entry */
        deallocate_Buffer_Entry( ll_Data, buf_Tag );        
        
        /* return value */
        return RIO_RETRY;   
    }

    /* process transaction */
    buf_Entry->trx_Info.state       = RIO_LL_STATE_INIT;

    if ( buf_Entry->kernel_Info.is_Local )
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_LOCAL );
    else 
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_REMOTE );
}

/***************************************************************************
 * Function :    RIO_LL_Config_Request
 *
 * Description:  function receives data structure describing necessary 
 *               maintenance request. If data is correct buffer is assigned
 *               and request is processed by its transaction handler.
 *
 * Returns:      RIO_OK in case of successfully accepted request.
 *               RIO_PARAM_INVALID in case of incorrect data.
 *               RIO_RETRY in case of unavailable buffer entry.
 *               RIO_ERROR in case of error
 *               
 *
 * Notes:        
 *
 **************************************************************************/
int RIO_LL_Config_Request(
    RIO_HANDLE_T     handle, 
    RIO_CONFIG_REQ_T *rq, 
    RIO_TAG_T        trx_Tag
)
{
    RIO_LL_DS_T      *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T  *buf_Entry;  /* pointer to the buffer entry to store the transaction */
    RIO_LL_BUF_TAG_T buf_Tag;     /* buffer tag of the buffer entry                       */
    RIO_CONFIG_REQ_T *buf_Req;    /* copy of structure to place it to buffer              */
    RIO_DW_T         *data;       /* copy of the request data to place it to buffer       */
    int              i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Maintenance request");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Maintenance request");
        return RIO_OK;
    }

    /* check that user has requested transaction with correct attributes */
    if( rq == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "Maintenance request" );
        return RIO_PARAM_INVALID;
    }

    /* check priority of the request */
    if( rq->prio > RIO_MAX_REQUEST_PRIORITY )
    {
        /* priority of the request packet can not be larger than 2 */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "Maintenance", RIO_MAX_REQUEST_PRIORITY );
        return RIO_PARAM_INVALID;
    }

    /* check packet data size */
    if( rq->dw_Size > RIO_LL_MAX_CONF_PACKET_DW_SIZE )
    {
        /* the size of the transaction is larger than allowed 64 bytes */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, "Maintenance", RIO_LL_MAX_CONF_PACKET_DW_SIZE );
        return RIO_PARAM_INVALID;
    }

    if (rq->dw_Size == 0)
    {
        /* request shall have size bigger than null */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, "Maintenance" );
        return RIO_PARAM_INVALID;
    }

    if (rq->dw_Size != 1 &&     /* one doubleword data payload         */     
        rq->dw_Size != 2 &&     /* two doublewords data payload        */     
        rq->dw_Size != 4 &&     /* four doublewords data payload       */     
        rq->dw_Size != 8 &&     /* eight doublewords data payload      */     
        rq->rw == RIO_CONF_READ      
        )                            
    {
        /* request shall have data siz equal to power of two */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_LOCAL_DATA_PAYLOAD, "Maintenance read", "1, 2, 4, 8, 16", rq->dw_Size);
        return RIO_PARAM_INVALID;
    }

    /* check data presence in the transaction structure */
    if( (rq->rw == RIO_CONF_WRITE) && (rq->data == NULL) )
    {
        /* this transactions should supply data */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, "Maintenance" );
        return RIO_PARAM_INVALID;
    }

    /* data is correct. Check if PE can issue such a transaction */
    if ( !can_Issue( ll_Data, RIO_LL_CONF, rq->rw) )
        RIO_LL_Message( ll_Data, RIO_LL_WARN_CANNOT_ISSUE, "Maintenance", rq->rw );


/* GDA: Bug#125 - Support for Flow control packet 
printf("\n xonoff at iorequwest is %d %d  \n",ll_Data->temp_Buffer.x_On_Off,ll_Data->congestion_Counter); */
   if(ll_Data->remote_Buffer->trx_Info.x_On_Off ==1 || ll_Data->temp_Buffer.x_On_Off==1)
   {
         printf("\n At congestion \n"); 
	  ll_Data->congestion_Counter++;
/*printf("\nll_Data->congestion_Counter is  %d  \n",ll_Data->congestion_Counter);*/
          if(ll_Data->congestion_Counter > RIO_MAX_DELAY_ORPHAND_XON)  /*RIO_MAX_CONGESTION_COUNTER_SIZE*/   
	  {
          ll_Data->remote_Buffer->trx_Info.x_On_Off=0;
	  ll_Data->temp_Buffer.x_On_Off=0;
	  }
          else 
           return  RIO_CONGES;   /*RIO_CONGESTION;*/
   }
  
   ll_Data->congestion_Counter=0;

/* GDA: Bug#125 - Support for Flow control packet */



    /* data is correct try to allocate buffer for it */
    if ( (buf_Tag = allocate_Buffer_Entry( ll_Data, rq->prio, RIO_TRUE)) == RIO_ERROR)
        return RIO_RETRY;

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "IO request" );
        return RIO_ERROR;
    }

    /* buffer is allocated, do filling buffer sructure */

    /* find handler for the transation */
    buf_Entry->kernel_Info.handler = find_Handler(
        ll_Data,
        RIO_TRUE,
        RIO_LL_CONF,
        rq->rw
    );
    if (buf_Entry->kernel_Info.handler == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_LOCAL_NO_HANDLER, "Maintenance", rq->rw );
        return RIO_ERROR;
    }

    /* filling in buffer kernel fields */
    buf_Entry->kernel_Info.is_Stalled = RIO_FALSE;
    buf_Entry->kernel_Info.is_Local   = RIO_FALSE;

    /* create copy of the request structure           */
    buf_Req = ( RIO_CONFIG_REQ_T* )malloc( sizeof( RIO_CONFIG_REQ_T ) );
    if ( buf_Req == NULL )
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
            sizeof( RIO_CONFIG_REQ_T), "Maintenance request structure");
        return RIO_ERROR;
    }
    *buf_Req = *rq;

    /* do proper aligment of the offset in the copy of the request */
    buf_Req->config_Offset = buf_Req->config_Offset & RIO_LL_ADDRESS_DW_ALIGN;

    /* create copy of the data in the request structure */
    if ( rq->rw == RIO_CONF_WRITE )
    {
        data = ( RIO_DW_T* )malloc( rq->dw_Size * sizeof( RIO_DW_T ) );
        if ( data == NULL )
        {
            deallocate_Buffer_Entry( ll_Data, buf_Tag );    
            free( buf_Req );
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
                rq->dw_Size * sizeof( RIO_DW_T), "Maintenance request data payload");
            return RIO_ERROR;
        }

        /* fill in copy of data */
        for ( i = 0; i < rq->dw_Size; i++ )
            data[i] = rq->data[i];
    }
    else 
        data = NULL;
    buf_Req->data = data;

    /* fill in transaction info buffer structure */ 
    buf_Entry->trx_Info.prio         = rq->prio;
    buf_Entry->trx_Info.user_Trx_Tag = trx_Tag;
    translate_LL_FType_To_Spec_FType( 
        RIO_LL_CONF, 
        rq->rw, 
        &(buf_Entry->trx_Info.req_FType),
        &(buf_Entry->trx_Info.req_TType)
    );
    buf_Entry->trx_Info.req_Type    = RIO_LL_CONF;
    buf_Entry->trx_Info.req         = buf_Req;
    buf_Entry->trx_Info.dw_Size     = rq->dw_Size;
    buf_Entry->trx_Info.conf_Offset = rq->config_Offset;
    buf_Entry->trx_Info.sub_Dw_Pos  = rq->sub_Dw_Pos;

    if ( rq->data != NULL )
    {
        for ( i = 0; i < rq->dw_Size; i++ )
            buf_Entry->trx_Info.data[i] = rq->data[i];
    }

    /* process transaction */
    buf_Entry->trx_Info.state       = RIO_LL_STATE_INIT;

    return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_REMOTE );
}

/***************************************************************************
 * Function :    RIO_LL_GSM_Request
 *
 * Description:  function receives data structure describing necessary 
 *               GSM request. If data is correct buffer is assigned
 *               and request is processed by its transaction handler.
 *
 * Returns:      RIO_OK in case of successfully accepted request.
 *               RIO_PARAM_INVALID in case of incorrect data.
 *               RIO_RETRY in case of unavailable buffer entry.
 *               RIO_ERROR in case of error
 *               
 *
 * Notes:        
 *
 **************************************************************************/
int RIO_LL_GSM_Request(
    RIO_HANDLE_T    handle, 
    RIO_GSM_TTYPE_T ttype,
    RIO_GSM_REQ_T   *rq, 
    RIO_TAG_T       trx_Tag
)
{
    RIO_LL_DS_T      *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T  *buf_Entry;  /* pointer to the buffer entry to store the transaction */
    RIO_LL_BUF_TAG_T buf_Tag;     /* buffer tag of the buffer entry                       */
    RIO_GSM_REQ_T    *buf_Req;    /* copy of structure to place it to buffer              */
    RIO_DW_T         *data;       /* copy of the request data to place it to buffer       */
    RIO_UCHAR_T      granule_Size; /* size of the cache coherence granule                 */
    RIO_WORD_T       gran_Align_Mask; /* cache granule boundary address align mask        */
    int              i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "GSM request");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "GSM request");
        return RIO_OK;
    }

    /* check that user has requested transaction with correct attributes */
    if( rq == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "GSM request" );
        return RIO_PARAM_INVALID;
    }

    /* check priority of the request */
    if( rq->prio > RIO_MAX_REQUEST_PRIORITY )
    {
        /* priority of the request packet can not be larger than 2 */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "GSM", RIO_MAX_REQUEST_PRIORITY );
        return RIO_PARAM_INVALID;
    }

    /* find out size of the cache coherence granule size */
    if ( ll_Data->inst_Params.coh_Granule_Size_32 )
    {
        granule_Size    = 32;    /* thirty two byte coherency granule size */
        gran_Align_Mask = RIO_LL_ADDRESS_GRAN_32_ALIGN;
    }
    else
    {
        granule_Size    = 64;    /* sixty four byte coherency granule size */
        gran_Align_Mask = RIO_LL_ADDRESS_GRAN_64_ALIGN;
    }

    /* check size of the packet */
    if( rq->dw_Size > granule_Size / RIO_LL_DW_BYTE_SIZE )
    {
        /* the size of the transaction is larger than cache coherence granule size */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, "GSM", granule_Size / RIO_LL_DW_BYTE_SIZE );
        return RIO_PARAM_INVALID;
    }

    /* DKILL_HOME, IKILL, TLBIE and TLB_SYNC transactions shall always have */
    /* data payload size equal to 0                                                      */
    if ( ( ttype == RIO_GSM_DC_I ||
        ttype == RIO_GSM_IC_I ||
        ttype == RIO_GSM_TLB_IE ||
        ttype == RIO_GSM_TLB_SYNC ) &&
        rq->dw_Size != 0 )
    {
        /* the transaction requests not NULL data payload size */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_GSM_DATA_PAYLOAD, ttype, 0);
        return RIO_PARAM_INVALID;
    }

    /* all read transactions and CASTOUT shall always request cache coherence granule sized data payload */
    if ( ( ttype == RIO_GSM_READ_SHARED ||
        ttype == RIO_GSM_INSTR_READ ||
        ttype == RIO_GSM_READ_TO_OWN ||
        ttype == RIO_GSM_CASTOUT) &&
        rq->dw_Size != granule_Size / RIO_LL_DW_BYTE_SIZE )
    {
        /* the transaction requests has data payload size not equal to the cache coherence granule size */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_GSM_DATA_PAYLOAD, ttype, granule_Size / RIO_LL_DW_BYTE_SIZE );
        return RIO_PARAM_INVALID;
    }

    /* flush TRANSACTION may have any data payload size including NULL data payload size */
    /* but data payload for this transaction shall nt cross cache granule boundary      */
    /* check that FLUSH or CASTOUT transactions don't cross cache granule boundary      */
    if ( ( ttype == RIO_GSM_FLUSH || ttype == RIO_GSM_CASTOUT ) && 
        rq->address + rq->dw_Size > ( rq->address & gran_Align_Mask ) + granule_Size)
    {
        /* issue error message */
        if ( ttype == RIO_GSM_CASTOUT )
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_GSM_CROSS_GRANULE_BOUND, "GSM CASTOUT");

        if ( ttype == RIO_GSM_FLUSH )
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_GSM_CROSS_GRANULE_BOUND, "GSM FLUSH");

        return RIO_PARAM_INVALID;
    }

    /* check that SUB DW FLUSH transaction doesn't request NULL data payload */
    if( ( ttype == RIO_GSM_FLUSH ) && ( rq->dw_Size == 1 ) && 
        ( rq->byte_Num == 0 ) )
    {
        /* user has requested subdoubleword transaction without data */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_SUB_DW_DATA_PAYLOAD, "GSM FLUSH");
        return RIO_PARAM_INVALID;
    }

    /* check that sub DW FLUSH transaction doesn't cross DW boundary */
    if( ( ttype == RIO_GSM_FLUSH ) && ( rq->dw_Size == 1 ) && 
        ( rq->offset + rq->byte_Num > RIO_LL_DW_BYTE_SIZE ) )
    {
        /* user has requested transaction which crosses double-word boundary */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CROSS_BOUND, "GSM FLUSH" );
        return RIO_PARAM_INVALID;
    }

    /* check that sub DW FLUSH transaction doesn't have misaligned data payload */
    if ( (ttype == RIO_GSM_IO_READ || ttype == RIO_GSM_FLUSH) 
         && rq->dw_Size == 1 &&
        ( ( rq->byte_Num == 2 && rq->offset % 2 != 0 ) ||   /* two byte subdw access */
        ( rq->byte_Num == 4 && rq->offset % 4 != 0 ) ||
        ( rq->byte_Num == 3 && !(rq->offset == 0 || rq->offset == 5)) ||
        ( rq->byte_Num == 5 && !(rq->offset == 0 || rq->offset == 3)) ||
        ( rq->byte_Num == 6 && !(rq->offset == 0 || rq->offset == 2)) ||
        ( rq->byte_Num == 7 && !(rq->offset == 0 || rq->offset == 1))) )  /* four byte subdw access */
    {
        /* issue error message */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_GSM_FLUSH_MISALIGN );

        return RIO_PARAM_INVALID;
    }

    /* CASTOUT transaction shall always supply data payload */
    /* FLUSH with data shall always supply data payload     */
    if ( ( ttype == RIO_GSM_CASTOUT && rq->data == NULL ) ||
        ( ttype == RIO_GSM_FLUSH && rq->dw_Size > 0 && rq->data == NULL ) )
    {
        /* these transactions should supply data */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, "GSM CASTOUT" );
        return RIO_PARAM_INVALID;
    }

    if( get_Config_Reg_Field( ll_Data, RIO_LL_EXT_ADDRESSING_SUPPORT ) == RIO_LL_PE_LL_CSR_50 ) 
        /* 16-bit extended address; clear significant bytes in the extended address */
        rq->extended_Address = rq->extended_Address & RIO_EXT_ADDRESS_16_MASK;
    else if ( get_Config_Reg_Field( ll_Data, RIO_LL_EXT_ADDRESSING_SUPPORT ) == RIO_LL_PE_LL_CSR_34 )
        /* clear entire extended address */
        rq->extended_Address = 0;


    /* data is correct. Check if PE can issue such a transaction */
    if ( !can_Issue( ll_Data, RIO_LL_GSM, ttype) )
        RIO_LL_Message( ll_Data, RIO_LL_WARN_CANNOT_ISSUE, "GSM", ttype );

/* GDA: Bug#125 - Support for Flow control packet 
printf("\n xonoff at iorequwest is %d %d  \n",ll_Data->temp_Buffer.x_On_Off,ll_Data->congestion_Counter); */
   if(ll_Data->remote_Buffer->trx_Info.x_On_Off ==1 || ll_Data->temp_Buffer.x_On_Off==1)
   {
         printf("\n At congestion \n"); 
	  ll_Data->congestion_Counter++;
/*printf("\nll_Data->congestion_Counter is  %d  \n",ll_Data->congestion_Counter);*/
          if(ll_Data->congestion_Counter > RIO_MAX_DELAY_ORPHAND_XON)  /*RIO_MAX_CONGESTION_COUNTER_SIZE*/   
	  {
          ll_Data->remote_Buffer->trx_Info.x_On_Off=0;
	  ll_Data->temp_Buffer.x_On_Off=0;
	  }
          else 
           return  RIO_CONGES;   /*RIO_CONGESTION;*/
   }
  
   ll_Data->congestion_Counter=0;

/* GDA: Bug#125 - Support for Flow control packet */





    /* data is correct try to allocate buffer for it */
    if ( (buf_Tag = allocate_Buffer_Entry( ll_Data, rq->prio, RIO_TRUE)) == RIO_ERROR)
        return RIO_RETRY;

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "GSM request" );
        return RIO_ERROR;
    }

    /* do filling buffer structure */

    /* find handler for the transation */
    buf_Entry->kernel_Info.handler = find_Handler(
        ll_Data,
        RIO_TRUE,
        RIO_LL_GSM,
        ttype
    );
    if (buf_Entry->kernel_Info.handler == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_LOCAL_NO_HANDLER, "GSM", ttype );
        return RIO_ERROR;
    }

    /* perform address translation and set local/remote attribute in the buffer */
    if( ll_Data->callback_Tray.rio_Route_GSM == NULL )
        /* no address translation UDF, make local request by default, perform no address translation */
        buf_Entry->kernel_Info.is_Local = RIO_TRUE;
    else
        buf_Entry->kernel_Info.is_Local = (ll_Data->callback_Tray.rio_Route_GSM(
            ll_Data->callback_Tray.address_Translation_Context,
            rq->address, 
            rq->extended_Address, 
            rq->xamsbs,
            &(buf_Entry->trx_Info.tr_Info[0])) == RIO_ROUTE_TO_LOCAL ) ? RIO_TRUE : RIO_FALSE;

    buf_Entry->trx_Info.is_Outstanding = !buf_Entry->kernel_Info.is_Local;

    buf_Entry->trx_Info.multicast_Size = 1; /* To make GSM multicast request it is necessary to use */
                                            /* kernel function RIO_LL_Kernel_Send_Altered_Request   */

    /* create copy of the request structure           */
    buf_Req = ( RIO_GSM_REQ_T* )malloc( sizeof( RIO_GSM_REQ_T ) );
    if ( buf_Req == NULL )
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, sizeof( RIO_GSM_REQ_T), "GSM request structure");
        return RIO_ERROR;
    }
    *buf_Req = *rq;

    /* do proper aligment of the address in the copy of the request */
    buf_Req->address = buf_Req->address & RIO_LL_ADDRESS_DW_ALIGN;

    /* create copy of the data in the request structure */
    if ( ttype == RIO_GSM_CASTOUT || ( ttype == RIO_GSM_FLUSH && rq->data != NULL ) )
    {
        data = ( RIO_DW_T* )malloc( rq->dw_Size * sizeof( RIO_DW_T ) );
        if ( data == NULL )
        {
            deallocate_Buffer_Entry( ll_Data, buf_Tag );    
            free( buf_Req );
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
                rq->dw_Size * sizeof( RIO_DW_T), "GSM request data payload");
            return RIO_ERROR;
        }

        /* fill in copy of data */
        for ( i = 0; i < rq->dw_Size; i++ )
            data[i] = rq->data[i];

    }
    else 
        data = NULL;
    buf_Req->data = data;

    /* fill in transaction info buffer structure */ 
    buf_Entry->trx_Info.prio         = buf_Req->prio;
    buf_Entry->trx_Info.user_Trx_Tag = trx_Tag;
    translate_LL_FType_To_Spec_FType( 
        RIO_LL_GSM, 
        ttype, 
        &(buf_Entry->trx_Info.req_FType),
        &(buf_Entry->trx_Info.req_TType)
    );
    buf_Entry->trx_Info.req_Type    = RIO_LL_GSM;
    buf_Entry->trx_Info.gsm_TType   = ttype;
    if ( ttype == RIO_GSM_FLUSH && rq->dw_Size > 0 )
    {
        buf_Entry->trx_Info.req_FType = RIO_WRITE;
        buf_Entry->trx_Info.req_TType = RIO_WRITE_FLUSH_WITH_DATA;
        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_FLUSH_WITH_DATA;
    }

    if( ttype == RIO_GSM_TLB_SYNC )
    {
        /* this is a transaction-only transaction */
        buf_Req->address = 0;
        buf_Req->extended_Address = 0;
        buf_Req->xamsbs = 0;
    }
    buf_Entry->trx_Info.req         = buf_Req;
    buf_Entry->trx_Info.address     = buf_Req->address;
    buf_Entry->trx_Info.extended_Address = buf_Req->extended_Address;
    buf_Entry->trx_Info.xamsbs      = buf_Req->xamsbs;
    buf_Entry->trx_Info.offset      = buf_Req->offset;
    buf_Entry->trx_Info.byte_Num    = buf_Req->byte_Num;
    buf_Entry->trx_Info.dw_Size     = buf_Req->dw_Size;

    if ( rq->data != NULL )
    {
        for ( i = 0; i < rq->dw_Size; i++ )
            buf_Entry->trx_Info.data[i] = rq->data[i];
    }

    /* cancel transaction if there is a collision with some earlier requested transaction */
    if ( is_GSM_Local_Collision( ll_Data, buf_Tag ) )
    {
        /* free allocated memory */
        if ( buf_Req->data != NULL)
            free( buf_Req->data );
        free( buf_Req );
        buf_Entry->trx_Info.req = NULL;

        /* deallocate buffer entry */
        deallocate_Buffer_Entry( ll_Data, buf_Tag );        
        
        /* return value */
        return RIO_RETRY;   
    }

    /* process transaction */
    buf_Entry->trx_Info.state       = RIO_LL_STATE_INIT;

    if ( buf_Entry->kernel_Info.is_Local )
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_LOCAL );
    else 
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_REMOTE );
}

/***************************************************************************
 * Function :    RIO_LL_Message_Request
 *
 * Description:  function receives data structure describing necessary 
 *               message passing request. If data is correct buffer 
 *               is assigned and request is processed by its transaction 
 *               handler.
 *
 * Returns:      RIO_OK in case of successfully accepted request.
 *               RIO_PARAM_INVALID in case of incorrect data.
 *               RIO_RETRY in case of unavailable buffer entry.
 *               RIO_ERROR in case of error
 *               
 *
 * Notes:        
 *
 **************************************************************************/
int RIO_LL_Message_Request(
    RIO_HANDLE_T      handle, 
    RIO_MESSAGE_REQ_T *rq, 
    RIO_TAG_T         trx_Tag
)
{
    RIO_LL_DS_T        *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T    *buf_Entry;  /* pointer to the buffer entry to store the transaction */
    RIO_LL_BUF_TAG_T   buf_Tag;     /* buffer tag of the buffer entry                       */
    RIO_MESSAGE_REQ_T  *buf_Req;    /* copy of structure to place it to buffer              */
    RIO_DW_T           *data;       /* copy of data from the request structure              */
    unsigned int       i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Message request");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Message request");
        return RIO_OK;
    }

    /* check that user has requested transaction with correct attributes */
    if( rq == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "Message request" );
        return RIO_PARAM_INVALID;
    }

    /* check priority of the request */
    if( rq->prio > RIO_MAX_REQUEST_PRIORITY )
    {
        /* priority of the request packet can not be larger than 2 */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "Message", RIO_MAX_REQUEST_PRIORITY );
        return RIO_PARAM_INVALID;
    }

    /* check number of the destination message box */
    if ( ((rq->msglen > 0) && (rq->mbox > RIO_MAX_MBOX_PER_PE)) ||
        ((rq->msglen == 0) && (ll_Data->init_Params.ext_Mailbox_Support != RIO_TRUE) && (rq->mbox > RIO_MAX_MBOX_PER_PE)) ||
        ((rq->msglen == 0) && (ll_Data->init_Params.ext_Mailbox_Support == RIO_TRUE) && (rq->mbox > RIO_MAX_EXT_MBOX_PER_PE)) )
    {
        /* number of the mbox cannot be larger than the constant */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_MESSAGE_WRONG_PARAM, "message box", RIO_MAX_MBOX_PER_PE );
        return RIO_PARAM_INVALID;
    }

    /* check number of the destination letter */
    if( rq->letter > RIO_MAX_LETTER_PER_MBOX )
    {
        /* number of the letter cannot be larger than the constant */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_MESSAGE_WRONG_PARAM, "letter", RIO_MAX_LETTER_PER_MBOX );
        return RIO_PARAM_INVALID;
    }

    /* check data payload presence */
    if ( (rq->data == NULL) || (rq->dw_Size == 0) || (rq->ssize == 0 ) )
    {
        /* this transaction should supply data */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, "Message" );
        return RIO_PARAM_INVALID;
    }

    /* check data payload size     */
    if ( rq->ssize > max_Source_Message_Size( ll_Data, rq->mbox ) )
    {
        /* the size of the data payload is larger than allowed by the configuration */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
            "Message", max_Source_Message_Size( ll_Data, rq->mbox ) );
        return RIO_PARAM_INVALID;        
    }

    if (rq->ssize != 1 &&   /* one doubleword data payload         */
        rq->ssize != 2 &&   /* two doublewords data payload        */
        rq->ssize != 4 &&   /* four doublewords data payload       */
        rq->ssize != 8 &&   /* eight doublewords data payload      */
        rq->ssize != 16 &&  /* sixteen doublewords data payload    */
        rq->ssize != 32     /* thirty two doublewords data payload */
        )
    {
        /* request shall have data size equal to power of two */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_LOCAL_DATA_PAYLOAD, "Message", "1, 2, 4, 8, 16, 32", rq->ssize);
        return RIO_PARAM_INVALID;
    }

    /* check that data payload size is equal to stated in the packet header */
    if ( ((rq->dw_Size != rq->ssize) && (rq->msgseg < rq->msglen)) || 
        ((rq->dw_Size > rq->ssize) && (rq->msgseg == rq->msglen)) )
    {
        /* only last packet in message may have data payload smaller than stated in its header */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_MESSAGE_WRONG_DATA_PAYLOAD );
        return RIO_PARAM_INVALID;        
    }

    /* check message length        */
    if ( rq->msglen > max_Source_Message_Seg( ll_Data, rq->mbox ) )
    {
        /* the number of the segments in message is larger than allowed by the configuration */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_MESSAGE_WRONG_PARAM, "segment", max_Source_Message_Seg( ll_Data, rq->mbox ) );
        return RIO_PARAM_INVALID;        
    }

    /* check packet number */
     if ( (rq->msgseg > rq->msglen)  && (rq->msglen >0 ))  /* GDA : Bug #123 Support for extended mailbox (xmbox) feature */
    
    {
        /* the number of the current message segment is larger than size of the message */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_MESSAGE_WRONG_PARAM, "segment", rq->msglen );
        return RIO_PARAM_INVALID;        
    }

    /* for the case of message packet targeted to the extended Mailbox in another PE */
    /* compose msgseg/xmbox field from the mbox field of the original request        */
    if ( (ll_Data->init_Params.ext_Mailbox_Support == RIO_TRUE) && (rq->msglen == 0) )
    {
	   rq->msgseg &= 0xF; /* GDA: Bug# 123 Support for xmbox */
  
/*        rq->msgseg = (rq->mbox & 0x3C) >> 2;
        rq->mbox   &= 0x03; */
          rq->mbox &= 0x0F; /* GDA: Bug# 123 Support for xmbox */

    }

    /* data is correct. Check if PE can issue such a transaction */
    if ( !can_Issue( ll_Data, RIO_LL_MESSAGE, 0) )
        RIO_LL_Message( ll_Data, RIO_LL_WARN_CANNOT_ISSUE, "Message", 0 );


/* GDA: Bug#125 - Support for Flow control packet 
printf("\n xonoff at iorequwest is %d %d  \n",ll_Data->temp_Buffer.x_On_Off,ll_Data->congestion_Counter); */
   if(ll_Data->remote_Buffer->trx_Info.x_On_Off ==1 || ll_Data->temp_Buffer.x_On_Off==1)
   {
         printf("\n At congestion \n"); 
	  ll_Data->congestion_Counter++;
/*printf("\nll_Data->congestion_Counter is  %d  \n",ll_Data->congestion_Counter);*/
          if(ll_Data->congestion_Counter > RIO_MAX_DELAY_ORPHAND_XON)  /*RIO_MAX_CONGESTION_COUNTER_SIZE*/   
	  {
          ll_Data->remote_Buffer->trx_Info.x_On_Off=0;
	  ll_Data->temp_Buffer.x_On_Off=0;
	  }
          else 
           return  RIO_CONGES;   /*RIO_CONGESTION;*/
   }
  
   ll_Data->congestion_Counter=0;

/* GDA: Bug#125 - Support for Flow control packet */


    /* data is correct try to allocate buffer for it */
    if ( (buf_Tag = allocate_Buffer_Entry( ll_Data, rq->prio, RIO_TRUE)) == RIO_ERROR)
        return RIO_RETRY;

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "Message request" );
        return RIO_ERROR;
    }

    /* buffer is allocated, do filling buffer sructure */

    /* find handler for the transation */
    buf_Entry->kernel_Info.handler = find_Handler(
        ll_Data,
        RIO_TRUE,
        RIO_LL_MESSAGE,
        0
    );
    if (buf_Entry->kernel_Info.handler == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_LOCAL_NO_HANDLER, "Message", 0 );
        return RIO_ERROR;
    }

    /* filling in buffer kernel fields */
    buf_Entry->kernel_Info.is_Stalled = RIO_FALSE;
    buf_Entry->kernel_Info.is_Local   = RIO_FALSE;

    /* create copy of the request structure           */
    buf_Req = ( RIO_MESSAGE_REQ_T* )malloc( sizeof( RIO_MESSAGE_REQ_T ) );
    if ( buf_Req == NULL )
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
            sizeof( RIO_MESSAGE_REQ_T), "Message request structure");
        return RIO_ERROR;
    }
    *buf_Req = *rq;

    /* create copy of the data in the request structure */
    if ( rq->data != NULL)
    {
        data = ( RIO_DW_T* )malloc( rq->dw_Size * sizeof( RIO_DW_T ) );
        if ( data == NULL )
        {
            deallocate_Buffer_Entry( ll_Data, buf_Tag );    
            free( buf_Req );
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
                rq->dw_Size * sizeof( RIO_DW_T), "Message request data payload");
            return RIO_ERROR;
        }

        /* fill in copy of data */
        for ( i = 0; i < rq->dw_Size; i++ )
            data[i] = rq->data[i];
    }
    else 
        data = NULL;
    buf_Req->data = data;

    /* fill in transaction info buffer structure */ 
    buf_Entry->trx_Info.prio         = rq->prio;
    buf_Entry->trx_Info.user_Trx_Tag = trx_Tag;
    translate_LL_FType_To_Spec_FType( 
        RIO_LL_MESSAGE, 
        0, 
        &(buf_Entry->trx_Info.req_FType),
        &(buf_Entry->trx_Info.req_TType)
    );
    buf_Entry->trx_Info.req_Type    = RIO_LL_MESSAGE;
    buf_Entry->trx_Info.req         = buf_Req;

    buf_Entry->trx_Info.mbox        = rq->mbox; 
    buf_Entry->trx_Info.letter      = rq->letter;
    buf_Entry->trx_Info.msgseg      = rq->msgseg;
    buf_Entry->trx_Info.ssize       = rq->ssize;
    buf_Entry->trx_Info.msglen      = rq->msglen;
    
    /* store transport info to re-write it in rq structure when message request
       is being re-issued due to retry (tl changes transport info when first
       message packet is being sent, thus it is corrupted for re-sent packet */
    buf_Entry->trx_Info.tr_Info[0]  = rq->transport_Info;

    buf_Entry->trx_Info.dw_Size     = rq->dw_Size;
    for ( i = 0; i < rq->dw_Size; i++ )
        buf_Entry->trx_Info.data[i] = rq->data[i];

    /* process transaction */
    buf_Entry->trx_Info.state       = RIO_LL_STATE_INIT;

    return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_REMOTE );
}

/***************************************************************************
 * Function :    RIO_LL_Doorbell_Request
 *
 * Description:  function receives data structure describing necessary 
 *               doorbell request. If data is correct buffer is assigned
 *               and request is processed by its transaction handler.
 *
 * Returns:      RIO_OK in case of successfully accepted request.
 *               RIO_PARAM_INVALID in case of incorrect data.
 *               RIO_RETRY in case of unavailable buffer entry.
 *               RIO_ERROR in case of error
 *               
 *
 * Notes:        
 *
 **************************************************************************/
int RIO_LL_Doorbell_Request(
    RIO_HANDLE_T       handle, 
    RIO_DOORBELL_REQ_T *rq, 
    RIO_TAG_T          trx_Tag
)
{
    RIO_LL_DS_T        *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T    *buf_Entry;  /* pointer to the buffer entry to store the transaction */
    RIO_LL_BUF_TAG_T   buf_Tag;     /* buffer tag of the buffer entry                       */
    RIO_DOORBELL_REQ_T *buf_Req;    /* copy of structure to place it to buffer              */

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Doorbell request");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Doorbell request");
        return RIO_OK;
    }

    /* check that user has requested transaction with correct attributes */
    if( rq == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "Doorbell request" );
        return RIO_PARAM_INVALID;
    }

    /* check priority of the request */
    if( rq->prio > RIO_MAX_REQUEST_PRIORITY )
    {
        /* priority of the request packet can not be larger than 2 */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "Doorbell", RIO_MAX_REQUEST_PRIORITY );
        return RIO_PARAM_INVALID;
    }

    /* data is correct. Check if PE can issue such a transaction */
    if ( !can_Issue( ll_Data, RIO_LL_DOORBELL, 0) )
        RIO_LL_Message( ll_Data, RIO_LL_WARN_CANNOT_ISSUE, "Doorbell", 0 );

    /* data is correct try to allocate buffer for it */
    if ( (buf_Tag = allocate_Buffer_Entry( ll_Data, rq->prio, RIO_TRUE)) == RIO_ERROR)
        return RIO_RETRY;

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "Doorbell request" );
        return RIO_ERROR;
    }

    /* buffer is allocated, do filling buffer sructure */

    /* find handler for the transation */
    buf_Entry->kernel_Info.handler = find_Handler(
        ll_Data,
        RIO_TRUE,
        RIO_LL_DOORBELL,
        0
    );
    if (buf_Entry->kernel_Info.handler == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_LOCAL_NO_HANDLER, "Doorbell", 0 );
        return RIO_ERROR;
    }

    /* filling in buffer kernel fields */
    buf_Entry->kernel_Info.is_Stalled = RIO_FALSE;
    buf_Entry->kernel_Info.is_Local   = RIO_FALSE;

    /* create copy of the request structure           */
    buf_Req = ( RIO_DOORBELL_REQ_T* )malloc( sizeof( RIO_DOORBELL_REQ_T ) );
    if ( buf_Req == NULL )
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
            sizeof( RIO_DOORBELL_REQ_T), "Doorbell request structure");
        return RIO_ERROR;
    }
    *buf_Req = *rq;


    /* store transport info to re-write it in rq structure when doorbell request
       is being re-issued due to retry (tl changes transport info when first
       doorbell packet is being sent, thus it is corrupted for re-sent packet */
    buf_Entry->trx_Info.tr_Info[0]  = rq->transport_Info;

    /* fill in transaction info buffer structure */ 
    buf_Entry->trx_Info.prio         = rq->prio;
    buf_Entry->trx_Info.user_Trx_Tag = trx_Tag;
    translate_LL_FType_To_Spec_FType( 
        RIO_LL_DOORBELL, 
        0, 
        &(buf_Entry->trx_Info.req_FType),
        &(buf_Entry->trx_Info.req_TType)
    );
    buf_Entry->trx_Info.req_Type    = RIO_LL_DOORBELL;
    buf_Entry->trx_Info.req         = buf_Req;
    buf_Entry->trx_Info.doorbell_Info = rq->doorbell_Info;

    /* process transaction */
    buf_Entry->trx_Info.state       = RIO_LL_STATE_INIT;

    return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_REMOTE );
}

/***************************************************************************
 * Function :    RIO_LL_Remote_Request
 *
 * Description:  functions is to be invoked when Transport Layer has incoming
 *               RIO request to be serviced. 
 *
 * Returns:      RIO_OK or RIO_ERROR or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_LL_Remote_Request(
    RIO_CONTEXT_T        handle, 
    RIO_TAG_T            tag, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_TR_INFO_T        transport_Info
)
{
    RIO_LL_DS_T        *ll_Data; /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T   buf_Tag;  /* buffer tag of the request        */
    RIO_LL_TRX_EVENT_T event;    /* event for the transaction processing */
    int                i;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    static int         ds;
    /* GDA: Bug#124 - Support for Data Streaming packet */


    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer remote request");
        return RIO_ERROR;
    }

    /* check pointers */
    if ( req == NULL )
    {
        /* NULL pointer to the request structure */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "Logical Layer remote request" );
        return RIO_PARAM_INVALID;
    }

    if ( req->header == NULL )
    {
        /* NULL pointer to the request header structure */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request header structure", "Logical Layer remote request" );
        return RIO_PARAM_INVALID;
    }

    /* check priority of the request */
    if( req->header->prio > RIO_MAX_REQUEST_PRIORITY )
    {
        /* priority of the request packet can not be larger than 2 */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "remote", RIO_MAX_REQUEST_PRIORITY );
        return RIO_PARAM_INVALID;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "remote request");
        return RIO_OK;
    }

    /* copying data into the temporary buffer */
    ll_Data->temp_Buffer.tag            = tag;
    ll_Data->temp_Buffer.transport_Info = transport_Info;
    ll_Data->temp_Buffer.transp_Type    = req->transp_Type;

    ll_Data->temp_Buffer.packet_Type    = req->packet_Type;
    ll_Data->temp_Buffer.format_Type    = req->header->format_Type; 
    ll_Data->temp_Buffer.ttype          = req->header->ttype;   

    ll_Data->temp_Buffer.target_TID     = req->header->target_TID;
    ll_Data->temp_Buffer.src_ID         = req->header->src_ID;
    ll_Data->temp_Buffer.prio           = req->header->prio;
    ll_Data->temp_Buffer.wdptr          = req->header->wdptr;
    ll_Data->temp_Buffer.rdsize         = req->header->rdsize;
    ll_Data->temp_Buffer.address        = req->header->address;
    ll_Data->temp_Buffer.extended_Address = req->header->extended_Address;
    ll_Data->temp_Buffer.xamsbs         = req->header->xamsbs;
    ll_Data->temp_Buffer.sec_ID         = req->header->sec_ID;
    ll_Data->temp_Buffer.sec_TID        = req->header->sec_TID;
    ll_Data->temp_Buffer.mbox           = req->header->mbox;
    ll_Data->temp_Buffer.letter         = req->header->letter;
    ll_Data->temp_Buffer.msgseg         = req->header->msgseg;
    ll_Data->temp_Buffer.ssize          = req->header->ssize;
    ll_Data->temp_Buffer.msglen         = req->header->msglen;
    ll_Data->temp_Buffer.doorbell_Info  = req->header->doorbell_Info;
    ll_Data->temp_Buffer.offset         = req->header->offset & RIO_LL_ADDRESS_WORD_ALIGN;
    ll_Data->temp_Buffer.dw_Num         = req->dw_Num;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ll_Data->temp_Buffer.cos            = req->header->cos;
    ll_Data->temp_Buffer.length         = req->header->length;
    ll_Data->temp_Buffer.s_Ds           = req->header->s_Ds;
    ll_Data->temp_Buffer.e_Ds           = req->header->e_Ds;
    ll_Data->temp_Buffer.o              = req->header->o;
    ll_Data->temp_Buffer.p              = req->header->p;
    ll_Data->temp_Buffer.stream_ID      = req->header->stream_ID;
    ll_Data->temp_Buffer.hw_Num         = req->hw_Num;
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    ll_Data->temp_Buffer.x_On_Off       = req->header->x_On_Off;
    ll_Data->temp_Buffer.flow_ID        = req->header->flow_ID;
    ll_Data->temp_Buffer.tgtdest_ID     = req->header->tgtdest_ID;
    ll_Data->temp_Buffer.soc            = req->header->soc;
    ll_Data->temp_Buffer.dest_ID        = req->header->dest_ID;
    ll_Data->temp_Buffer.is_congestion  = req->header->is_congestion;
    ll_Data->remote_Buffer->trx_Info.x_On_Off=req->header->x_On_Off;
    /* GDA: Bug#125 */
 
    for ( i = 0; i < ll_Data->temp_Buffer.dw_Num; i++)
        ll_Data->temp_Buffer.data[i]    = req->data[i];

    /* GDA: Bug#124 - Support for Data Streaming packet */
    for ( i = 0; i < ll_Data->temp_Buffer.hw_Num; i++)
    {
        ll_Data->temp_Buffer.data_hw[i]    = req->data_hw[i];
 	ll_Data->temp_Buffer.data_ds[ds]    = req->data_hw[i];
	ds++;
    }
    if(req->header->e_Ds)
	    ds=0;
    /* GDA: Bug#124 */
    
    ll_Data->temp_Buffer.has_Transaction = RIO_TRUE;

    switch ( get_Transaction_From_Temp_Buffer( ll_Data, &buf_Tag, &event) )
    {
        case RIO_OK:
            return process_Transaction( ll_Data, buf_Tag, event );

        case RIO_RETRY:
            return RIO_OK;

        case RIO_ERROR:
            return RIO_ERROR;
    }

    return RIO_ERROR;
}


/***************************************************************************
 * Function :    RIO_LL_Remote_Response
 *
 * Description:  function is to be invoked when the TL has remote responce
 *               for processing.
 *
 * Returns:      RIO_OK in case of successfully accepted responce.
 *               RIO_PARAM_INVALID in case of incorrect data.
 *               RIO_ERROR in case of error
 *               
 * Notes:        may issue error or warning message.
 *               may modify request queue. 
 *
 **************************************************************************/
int RIO_LL_Remote_Response(
    RIO_CONTEXT_T  handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
)
{
    RIO_LL_DS_T        *ll_Data;     /* pointer to the LL data structure              */
    RIO_LL_BUF_TAG_T   buf_Tag;      /* buffer tag of the request                     */
    RIO_LL_BUFFER_T    *buf_Entry;   /* buffer entry pointer                          */

    RIO_LL_TRX_EVENT_T new_Event;    /* event for the stalled transaction processing  */
    RIO_BOOL_T         need_Kill;    /* this flag shows that the outstanding GSM      */
                                     /* transaction received response shall be killed */
    RIO_BOOL_T         was_Processed; /* flag is set if the outstanding transaction   */
                                      /* was processed accordingly to the collision   */
                                      /* resolution tables                            */

    RIO_LL_BUF_TAG_T   stalled_Buf_Tag;    /* buffer tag of the stalled transaction    */
    RIO_LL_BUFFER_T    *stalled_Buf_Entry; /* buffer entry for the stalled transaction */

    RIO_WORD_T         mask = 0x00000000;
    RIO_WORD_T         granule_Size;
    int                i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check pointers */
    if ( resp == NULL )
    {
        /* NULL pointer to the request structure */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "response structure", "Logical Layer remote response" );
        return RIO_PARAM_INVALID;
    }

    /* check semaphor*/
    if ( !check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer remote response");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "remote response servicing");
        return RIO_OK;
    }

    /* find out cache coherency granule size */
    if ( ll_Data->inst_Params.coh_Granule_Size_32 )
        granule_Size = 32; /* thirty two byte coherency granule size */
    else
        granule_Size = 64; /* sixty four byte coherency granule size */

    /* find buffer entry of the corresponding request and fill in its fields */
    buf_Tag = tag;

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "remote response" );
        return RIO_ERROR;
    }

    /* check if the buffer with the supplied tag contains request */
    if ( !buf_Entry->kernel_Info.valid )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_TRX_NOT_FOUND, "remote response");
        return RIO_ERROR;
    }

    /* fill in buffer entry fields */
    buf_Entry->trx_Info.resp_FType        = resp->ftype;      
    buf_Entry->trx_Info.resp_Src_ID       = resp->src_ID;
    buf_Entry->trx_Info.resp_Transaction  = resp->transaction;
    buf_Entry->trx_Info.resp_Status       = resp->status;     
    buf_Entry->trx_Info.resp_DW_Size      = resp->dw_Num;    

    /* check if the response has correct combination of the ftype */
    /* transaction and the status fields                          */
    if ( ( resp->ftype == RIO_RESPONCE && resp->transaction != RIO_RESPONCE_WITH_DATA &&
            resp->transaction != RIO_RESPONCE_WO_DATA && resp->transaction != RIO_RESPONCE_MESSAGE ) ||
        ( resp->ftype == RIO_MAINTENANCE && resp->transaction != RIO_MAINTENANCE_READ_RESPONCE && 
            resp->transaction != RIO_MAINTENANCE_WRITE_RESPONCE) ||
        ( resp->ftype == RIO_RESPONCE && resp->transaction == RIO_RESPONCE_WITH_DATA &&
            resp->status != RIO_RESPONCE_STATUS_DONE && resp->status != RIO_RESPONCE_STATUS_DATA_ONLY &&
            resp->status != RIO_RESPONCE_STATUS_INTERV && resp->status != RIO_RESPONCE_STATUS_ERROR) || 
        ( resp->ftype == RIO_RESPONCE && resp->transaction == RIO_RESPONCE_WO_DATA &&
            resp->status != RIO_RESPONCE_STATUS_DONE && resp->status != RIO_RESPONCE_STATUS_NOT_OWNER &&
            resp->status != RIO_RESPONCE_STATUS_RETRY && resp->status != RIO_RESPONCE_STATUS_DONE_INTERV &&
            resp->status != RIO_RESPONCE_STATUS_ERROR && resp->status != RIO_RESPONCE_STATUS_NO_MEMORY &&
            resp->status != RIO_RESPONCE_STATUS_CANNOT_SERVICE && resp->status != RIO_RESPONCE_STATUS_INTERV) ||
        (resp->ftype == RIO_RESPONCE && resp->transaction == RIO_RESPONCE_MESSAGE &&
            resp->status != RIO_RESPONCE_STATUS_DONE && resp->status != RIO_RESPONCE_STATUS_ERROR &&
            resp->status != RIO_RESPONCE_STATUS_RETRY && resp->status != RIO_RESPONCE_STATUS_CANNOT_SERVICE) ||
        (resp->ftype == RIO_MAINTENANCE && resp->transaction == RIO_MAINTENANCE_READ_RESPONCE &&
            resp->status != RIO_RESPONCE_STATUS_DONE && resp->status != RIO_RESPONCE_STATUS_ERROR &&
            resp->status != RIO_RESPONCE_STATUS_CANNOT_SERVICE) ||
        (resp->ftype == RIO_MAINTENANCE && resp->transaction == RIO_MAINTENANCE_WRITE_RESPONCE &&
            resp->status != RIO_RESPONCE_STATUS_DONE && resp->status != RIO_RESPONCE_STATUS_ERROR &&
            resp->status != RIO_RESPONCE_STATUS_CANNOT_SERVICE))
    {
        /* issue warning message */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_INVALID_RESPONSE_TYPE, resp->ftype, resp->transaction, resp->status );

        /* process transaction in such a way as for an ERROR response */
        process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_ERROR );

        /* return OK code because transaction was processed in a normal way */
        return RIO_OK;
    }


    /* check if response has the correct data payload size */
    /* regarding its ftype and transaction                 */
    if ( ( resp->ftype == RIO_RESPONCE && ( resp->transaction == RIO_RESPONCE_WO_DATA || 
        resp->transaction == RIO_RESPONCE_MESSAGE ) && resp->dw_Num != 0 ) ||
        ( resp->ftype == RIO_RESPONCE && resp->transaction == RIO_RESPONCE_WITH_DATA && resp->dw_Num == 0 ) ||
        ( resp->ftype == RIO_MAINTENANCE && resp->transaction == RIO_MAINTENANCE_WRITE_RESPONCE && resp->dw_Num != 0 ) ||
        ( resp->ftype == RIO_MAINTENANCE && resp->transaction == RIO_MAINTENANCE_READ_RESPONCE &&
        resp->status == RIO_RESPONCE_STATUS_DONE && resp->dw_Num == 0) )
    {
        /* issue warning message */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_WRONG_RESPONSE_DATA_PAYLOAD );

        /* process transaction in such a way as for an ERROR response */
        process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_ERROR );

        /* return OK code because transaction was processed in a normal way */
        return RIO_OK;        
    }

    /* check correctness of the response ftype and transaction regarding */
    /* type of the request                                               */
    if ( ( buf_Entry->trx_Info.req_FType == RIO_MESSAGE && ( resp->ftype != RIO_RESPONCE || 
            resp->transaction != RIO_RESPONCE_MESSAGE) ) ||
        ( buf_Entry->trx_Info.req_FType == RIO_MAINTENANCE && buf_Entry->trx_Info.req_TType == RIO_MAINTENANCE_CONF_READ &&
            (resp->ftype != RIO_MAINTENANCE || resp->transaction != RIO_MAINTENANCE_READ_RESPONCE ) ) || 
        ( buf_Entry->trx_Info.req_FType == RIO_MAINTENANCE && buf_Entry->trx_Info.req_TType == RIO_MAINTENANCE_CONF_WRITE &&
            (resp->ftype != RIO_MAINTENANCE || resp->transaction != RIO_MAINTENANCE_WRITE_RESPONCE) ) || 
        ( buf_Entry->trx_Info.req_FType != RIO_MESSAGE && buf_Entry->trx_Info.req_FType != RIO_MAINTENANCE &&
            ( resp->ftype != RIO_RESPONCE || resp->transaction == RIO_RESPONCE_MESSAGE ) ) )
    {
        /* issue warning message */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_INVALID_RESPONSE_FTYPE, resp->ftype, buf_Entry->trx_Info.req_FType );

        /* process transaction in such a way as for an ERROR response */
        process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_ERROR );

        /* return OK code because transaction was processed in a normal way */
        return RIO_OK;        
    }

    /* if transaction is FLUSH and received INTERV response with data it means that     */
    /* intervention request READ_TO_OWN_OWNER was issued so size of the data in buffer  */
    /* needs to be corrected. And data payload from the response packet shall be placed */
    /* into the transactions buffer                                                     */
    if ( ( ( buf_Entry->trx_Info.req_FType == RIO_WRITE && 
        buf_Entry->trx_Info.req_TType == RIO_WRITE_FLUSH_WITH_DATA ) ||
        ( buf_Entry->trx_Info.req_FType == RIO_NONINTERV_REQUEST && 
        buf_Entry->trx_Info.req_TType == RIO_NONINTERV_FLUSH_WO_DATA ) ) &&
        resp->status == RIO_RESPONCE_STATUS_INTERV )
    {
        /* place the data into the transactions buffer */
        if ( buf_Entry->trx_Info.dw_Size > 1 || 
            ( buf_Entry->trx_Info.dw_Size == 1 && buf_Entry->trx_Info.byte_Num == RIO_LL_DW_BYTE_SIZE ) )
        /* transaction requires whole DW memory access */
        {
            for ( i = 0; i < buf_Entry->trx_Info.dw_Size; i++ )
                resp->data[i] = buf_Entry->trx_Info.data[i];
        }
        else
        /* transaction requires sub doubleword memory access */
        {
            /* copy most significant word of the data */
            if ( buf_Entry->trx_Info.offset + buf_Entry->trx_Info.byte_Num <= RIO_LL_WORD_BYTE_SIZE )
            {
                for ( i = 0; i < buf_Entry->trx_Info.byte_Num; i++ )
                {
                    mask = mask >> RIO_LL_BYTE_BIT_SIZE;
                    mask = mask | 0xFF000000;    /* set most significant byte bits */        
                }

                mask = mask >> buf_Entry->trx_Info.offset * RIO_LL_BYTE_BIT_SIZE;

                resp->data[0].ms_Word = ( resp->data[i].ms_Word & (~mask) ) |
                    ( buf_Entry->trx_Info.data[0].ms_Word & mask );
            }

            /* copy least significant word of the data */
            if ( buf_Entry->trx_Info.offset + buf_Entry->trx_Info.byte_Num > RIO_LL_WORD_BYTE_SIZE )
            {
                for ( i = 0; i < buf_Entry->trx_Info.byte_Num; i++ )
                {
                    mask = mask >> RIO_LL_BYTE_BIT_SIZE;
                    mask = mask | 0xFF000000;    /* set most significant byte bits */        
                }

                mask = mask >> ( buf_Entry->trx_Info.offset - RIO_LL_WORD_BYTE_SIZE ) * RIO_LL_BYTE_BIT_SIZE;

                resp->data[0].ls_Word = ( resp->data[0].ls_Word & (~mask) ) |
                    ( buf_Entry->trx_Info.data[0].ls_Word & mask );
            }
        }

        /* adjust data size in the transactions buffer */
        buf_Entry->trx_Info.dw_Size = (unsigned char)(granule_Size / RIO_LL_DW_BYTE_SIZE);
    }

    /* check if the response has correct data payload size */
    /* regarding the data size requested                  */
    if ( ( resp->ftype == RIO_RESPONCE && resp->transaction == RIO_RESPONCE_WITH_DATA && 
        resp->dw_Num != buf_Entry->trx_Info.dw_Size) ||
        ( resp->ftype == RIO_MAINTENANCE && resp->transaction == RIO_MAINTENANCE_READ_RESPONCE && 
        resp->status == RIO_RESPONCE_STATUS_DONE && resp->dw_Num != buf_Entry->trx_Info.dw_Size ) )
    {
        /* issue warning message */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_WRONG_RESPONSE_DATA_PAYLOAD );

        /* process transaction in such a way as for an ERROR response */
        process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_ERROR );

        /* return OK code because transaction was processed in a normal way */
        return RIO_OK;        
    }

    /* copy response data payload into transaction buffer */
    for ( i = 0; i < resp->dw_Num; i++ )
        buf_Entry->trx_Info.data[i]       = resp->data[i];

    /* check if there is transaction stalled against the current transaction */
    /* this check shall be conducted only in case of the GSM requests        */
    if ( is_GSM_Transaction( handle, buf_Tag ) && 
        buf_Entry->trx_Info.is_Outstanding &&
        buf_Entry->kernel_Info.stall_Tag_Valid )
    {
        /* set flags */
        need_Kill     = RIO_FALSE;
        was_Processed = RIO_FALSE;
        new_Event     = RIO_EVENT_END_COLLISION_NORMAL;

        /* reset stall_Tag_Valid flag in the outstanding transaction buffer  */
        /* to not to let the process_Transaction function to unstall stalled */
        /* transaction incorrectly minding collision resolution tables       */
        buf_Entry->kernel_Info.stall_Tag_Valid = RIO_FALSE;

        /* get buffer entry for the stalled transaction */
        stalled_Buf_Tag   = buf_Entry->kernel_Info.stall_Tag;
        stalled_Buf_Entry = get_Buffer_Entry( ll_Data, stalled_Buf_Tag );
        if ( stalled_Buf_Entry == NULL )
        {
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                stalled_Buf_Tag, "remote response" );
            return RIO_ERROR;
        }

        /* find out if the outstanding transaction shall be killed   */
        /* or process it accordingly to the receibed response packet */
        /* status field                                              */
        /* also event for the stalled transaction processing will be */
        /* defined                                                   */
        switch ( buf_Entry->trx_Info.gsm_TType )
        {
            case RIO_PL_GSM_READ_HOME:
            {
                switch ( stalled_Buf_Entry->trx_Info.gsm_TType )
                {
                    case RIO_PL_GSM_DKILL_SHARER:
                    {
                        switch ( resp->status )
                        {
                            case RIO_RESPONCE_STATUS_DONE:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }

                            case RIO_RESPONCE_STATUS_RETRY:
                            {
                                need_Kill = RIO_TRUE;
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }
                        }
                        break;
                    }

                    default: {}
                }
                break;
            }

            case RIO_PL_GSM_READ_TO_OWN_HOME:
            {
                switch ( stalled_Buf_Entry->trx_Info.gsm_TType )
                {
                    case RIO_PL_GSM_FLUSH_WITH_DATA:
                    case RIO_PL_GSM_FLUSH_WO_DATA:
                    case RIO_PL_GSM_READ_TO_OWN_OWNER:
                    case RIO_PL_GSM_READ_OWNER:
                    case RIO_PL_GSM_IO_READ_OWNER:
                    {
                        switch ( resp->status )
                        {
                            case RIO_RESPONCE_STATUS_DONE:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }

                            case RIO_RESPONCE_STATUS_RETRY:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_RETRY );
                                new_Event = RIO_EVENT_END_COLLISION_ERROR;
                                was_Processed = RIO_TRUE;
                                break;
                            }
                        }
                        break;
                    }
                    
                    case RIO_PL_GSM_DKILL_SHARER:
                    {
                        switch ( resp->status )
                        {
                            case RIO_RESPONCE_STATUS_DONE:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );
                                new_Event = RIO_EVENT_END_COLLISION_ERROR;
                                was_Processed = RIO_TRUE;
                                break;
                            }

                            case RIO_RESPONCE_STATUS_RETRY:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_RETRY );
                                new_Event = RIO_EVENT_END_COLLISION_DONE;
                                was_Processed = RIO_TRUE;
                                break;
                            }
                        }
                        break;
                    }

                    default: {}
                }
                break;

                default: {}
            }

            case RIO_PL_GSM_DKILL_HOME:
            {
                switch ( stalled_Buf_Entry->trx_Info.gsm_TType )
                {
                    case RIO_PL_GSM_READ_TO_OWN_OWNER:
                    case RIO_PL_GSM_READ_OWNER:
                    case RIO_PL_GSM_IO_READ_OWNER:
                    {
                        switch ( resp->status )
                        {
                            case RIO_RESPONCE_STATUS_DONE:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }

                            case RIO_RESPONCE_STATUS_RETRY:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_RETRY );
                                new_Event = RIO_EVENT_END_COLLISION_ERROR;
                                was_Processed = RIO_TRUE;
                                break;
                            }
                        }
                        break;
                    }
                    
                    case RIO_PL_GSM_DKILL_SHARER:
                    {
                        switch ( resp->status )
                        {
                            case RIO_RESPONCE_STATUS_DONE:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );
                                new_Event = RIO_EVENT_END_COLLISION_ERROR;
                                was_Processed = RIO_TRUE;
                                break;
                            }

                            case RIO_RESPONCE_STATUS_RETRY:
                            {
                                need_Kill = RIO_TRUE;
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }
                        }
                        break;
                    }

                    default: {}
                }
                break;
            }

            case RIO_PL_GSM_FLUSH_WITH_DATA:
            case RIO_PL_GSM_FLUSH_WO_DATA:
            {
                switch ( stalled_Buf_Entry->trx_Info.gsm_TType )
                {
                    case RIO_PL_GSM_DKILL_SHARER:
                    {
                        switch ( resp->status )
                        {
                            case RIO_RESPONCE_STATUS_DONE:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );
                                new_Event = RIO_EVENT_END_COLLISION_ERROR;
                                was_Processed = RIO_TRUE;
                                break;
                            }

                            case RIO_RESPONCE_STATUS_RETRY:
                            {
                                need_Kill = RIO_TRUE;
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }
                        }
                        break;
                    }

                    default: {}
                }
                break;
            }

            case RIO_PL_GSM_IO_READ_HOME:
            {
                switch ( stalled_Buf_Entry->trx_Info.gsm_TType )
                {
                    case RIO_PL_GSM_DKILL_SHARER:
                    {
                        switch ( resp->status )
                        {
                            case RIO_RESPONCE_STATUS_DONE:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }

                            case RIO_RESPONCE_STATUS_RETRY:
                            {
                                process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_RETRY );
                                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
                                was_Processed = RIO_TRUE;
                                break;
                            }
                        }
                        break;
                    }

                    default: {}
                }
                break;
            }
        }
        
        /* if outstanding tarnsaction shall be killed - kill it */
        if ( need_Kill )
        {
            /* if it is locally requested transaction - invoke GSM request done callback */
            if ( buf_Tag < RIO_LL_REMOTE_BUFFER_START )
                ll_Data->callback_Tray.rio_GSM_Request_Done(
                    ll_Data->callback_Tray.local_Device_Context, 
                    buf_Entry->trx_Info.user_Trx_Tag, 
                    RIO_REQ_CANCEL, 
                    resp->status, 
                    0,
                    NULL
                );

            /* deallocate buffer entry */
            deallocate_Buffer_Entry( ll_Data, buf_Tag );
        }

        /* if the outstanding trasnaction wasn't processed due to unexpected */
        /* response packet status field, set stall_Tag_Valid flag and not    */
        /* exit function                                                     */
        if ( !was_Processed)
            buf_Entry->kernel_Info.stall_Tag_Valid = RIO_TRUE;
        else
        {
            /* if the outstanding transaction was processed - process the */
            /* stalled transaction and exit function                      */
            return process_Transaction( ll_Data, stalled_Buf_Tag, new_Event );
        }        
    }


    switch ( resp->status )
    {
        case RIO_RESPONCE_STATUS_DONE:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE );

        case RIO_RESPONCE_STATUS_NOT_OWNER:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_NOT_OWNER );

        case RIO_RESPONCE_STATUS_RETRY:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_RETRY );

        case RIO_RESPONCE_STATUS_ERROR:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_ERROR );

        case RIO_RESPONCE_STATUS_NO_MEMORY:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_NO_MEMORY );

        case RIO_RESPONCE_STATUS_CANNOT_SERVICE:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_CANNOT_SERVICE );

        case RIO_RESPONCE_STATUS_INTERV:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_INTERVENTION );

        case RIO_RESPONCE_STATUS_DONE_INTERV:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DONE_INTERVENTION );

        case RIO_RESPONCE_STATUS_DATA_ONLY:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_DATA_ONLY );

        default:
            return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_ERROR );
    }

    return RIO_ERROR;
}

/***************************************************************************
 * Function :    RIO_LL_Get_Request_Responce_Data
 *
 * Description:  function is invoked by the Transport Layer to get request 
 *               or responce data.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        
 *
 **************************************************************************/
int RIO_LL_Get_Request_Responce_Data(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
)
{
    RIO_LL_DS_T      *ll_Data;   /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T buf_Tag;    /* buffer tag of the request        */
    RIO_LL_BUFFER_T  *buf_Entry; /* buffer entry pointer             */
    int              i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check pointers */
    if ( data == NULL )
    {
        /* NULL pointer to the data structure */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "data structure", "Logical Layer get request or response data" );
        return RIO_PARAM_INVALID;
    }

    if ( cnt + offset > RIO_LL_MAX_PACKET_DW_SIZE )
    {
        return RIO_PARAM_INVALID;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Logical Layer get request response data");
        return RIO_OK;
    }

    /* find buffer entry of the corresponding request and fill in its fields */
    buf_Tag = tag;

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "Logical Layer get request or response data" );
        return RIO_ERROR;
    }

    /* check if the required data amount is available in the buffer entry */
    if ( offset + cnt > buf_Entry->trx_Info.dw_Size )
        return RIO_PARAM_INVALID;

    /* fill in suplyed data */
    for ( i = offset; i < offset + cnt; i++ )
        data[i] = buf_Entry->trx_Info.data[i];

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Ack_Request
 *
 * Description:  this function is to be called by the Transport Layer when 
 *               request from the LL to send packet is accepted.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        may issue error messages.
 *               may modify request queue.
 *
 **************************************************************************/
int RIO_LL_Ack_Request(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_LL_DS_T    *ll_Data;  /* pointer to the LL data structure */
    RIO_LL_QUEUE_T *req_Head; /* head of the responce queue */
    RIO_LL_QUEUE_T *req_Pool; /* pool of the responce queue elements */
    RIO_LL_QUEUE_T *element;  /* queue element pointer      */  

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphor*/
    if ( (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_INIT )) 
        && (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_ACK )))
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer request acknoledgement");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "request acknoledgement");
        return RIO_OK;
    }

    /* find element in the requests queue waiting for acknowledge*/
    req_Head = ll_Data->target_Queues.rio_Req_Queue_Head;
    req_Pool = ll_Data->target_Queues.rio_Req_Queue_Pool;

    element = req_Head;
    do 
        element = find_Queue_Element_With_State( req_Head, RIO_LL_WAIT_FOR_ACK );
    while ( ( element != NULL ) && ( element->buf_Tag != tag ) );

    if (element == NULL)
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_TRX_NOT_FOUND, "request acknoledgement");
        return RIO_ERROR;
    }

    /* remove element from the request queue */
    remove_Element_From_Queue(element, &req_Head);
    return_Queue_Element_To_Pool(element, &req_Pool);

    ll_Data->target_Queues.rio_Req_Queue_Head = req_Head;
    ll_Data->target_Queues.rio_Req_Queue_Pool = req_Pool;

    /* process transaction */
    return process_Transaction( ll_Data, tag, RIO_EVENT_REQUEST_SENT );
}

/***************************************************************************
 * Function :    RIO_LL_Ack_Responce
 *
 * Description:  this function is to be called by the Transport Layer when 
 *               responce from the LL is accepted.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        may issue error messages.
 *               may modify responce queue.
 *
 **************************************************************************/
int RIO_LL_Ack_Responce(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_LL_DS_T      *ll_Data;  /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T buf_Tag;   /* buffer tag of the request        */
    RIO_LL_QUEUE_T   *resp_Head;
    RIO_LL_QUEUE_T   *resp_Pool, *element;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphor*/
    if ( (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_INIT ))
        && (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_ACK )))
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer response acknoledgement");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "request acknoledgement");
        return RIO_OK;
    }

    /* find element in the requests queue waiting for this responce */
    resp_Head = ll_Data->target_Queues.rio_Resp_Queue_Head;
    resp_Pool = ll_Data->target_Queues.rio_Resp_Queue_Pool;

    element = resp_Head;
    do 
        element = find_Queue_Element_With_State( resp_Head, RIO_LL_WAIT_FOR_ACK );
    while ( ( element != NULL ) && ( element->buf_Tag != tag ) );

    if (element == NULL)
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_TRX_NOT_FOUND, "response acknoledgement");
        return RIO_ERROR;
    }

    /* remove element from the response queue */
    remove_Element_From_Queue(element, &resp_Head);
    return_Queue_Element_To_Pool(element, &resp_Pool);

    ll_Data->target_Queues.rio_Resp_Queue_Head = resp_Head;
    ll_Data->target_Queues.rio_Resp_Queue_Pool = resp_Pool;

    /* process transaction */
    buf_Tag = tag;
    return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_RESPONSE_SENT );
}

/***************************************************************************
 * Function :    RIO_LL_Request_Timeout
 *
 * Description:  this function is to be called by the Transport Layer when 
 *               packet requested by the LL has timed out.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        may issue error messages.
 *               may modify responce queue.
 *
 **************************************************************************/
int RIO_LL_Request_Timeout(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_LL_DS_T      *ll_Data;  /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T buf_Tag;   /* buffer tag of the request        */
    RIO_LL_BUFFER_T  *buf_Entry;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check semaphor*/
    if ( !check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer request timeout");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Logical Layer request timeout");
        return RIO_OK;
    }

    /* find buffer entry of the corresponding request */
    buf_Tag = tag;

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "remote response" );
        return RIO_ERROR;
    }

    /* check if the buffer with the supplied tag contains request */
    if ( !buf_Entry->kernel_Info.valid )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_TRX_NOT_FOUND, "remote response");
        return RIO_ERROR;
    }

    /* process transaction */
    return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_REQUEST_TIMEOUT );
}

/***************************************************************************
 * Function :    RIO_LL_Snoop_Response
 *
 * Description:  this function is to be called by the enviroment when 
 *               requested snoop operation has completed.
 *               Function fills in buffer entry for the transaction and
 *               starts transaction processing with its transaction 
 *               handler.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        may modify snoop queue
 *
 **************************************************************************/

int RIO_LL_Snoop_Response(
    RIO_CONTEXT_T      handle, 
    RIO_SNOOP_RESULT_T snoop_Result, 
    RIO_DW_T           *data
)
{
    RIO_LL_DS_T        *ll_Data;    /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T   buf_Tag;     /* buffer tag of the request        */
    RIO_LL_BUFFER_T    *buf_Entry;  /* buffer entry for the transaction */
    RIO_LL_QUEUE_T     *snoop_Head;
    RIO_LL_QUEUE_T     *snoop_Pool, *element;
    RIO_SNOOP_REQ_T    snoop_Req;
    RIO_WORD_T         granule_Size;
    RIO_WORD_T         offset;      /* offset in the snoop buffer */
    RIO_LL_TRX_EVENT_T event;       /* event for the transaction processing */
    unsigned int       i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    if ( snoop_Result != RIO_SNOOP_HIT && snoop_Result != RIO_SNOOP_MISS )
        return RIO_PARAM_INVALID;

    /* check semaphor*/
    if ( (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_INIT ))
        && (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_SNOOP )))
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer snoop response");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Logical Layer snoop response");
        return RIO_OK;
    }

    /* find element in the requests queue waiting for this responce */
    snoop_Head = ll_Data->target_Queues.snoop_Queue_Head;
    snoop_Pool = ll_Data->target_Queues.snoop_Queue_Pool;

    element = snoop_Head;
    element = find_Queue_Element_With_State( snoop_Head, RIO_LL_WAIT_FOR_RESPONCE );

    if ( element == NULL )
    {
        /* unknown buffer element */        
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_TRX_NOT_FOUND, "Logical Layer snoop response");
        return RIO_ERROR;
    }

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, element->buf_Tag );
    if ( buf_Entry == NULL)
    {
        /* issue error message */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, element->buf_Tag, "Logical Layer snoop response" );
        return RIO_ERROR;
    }

    /* fill in buffer entries */
    /* define granule size */
    if ( ll_Data->inst_Params.coh_Granule_Size_32 )
        granule_Size = 32;  /* thirty two byte coherency granule size */
    else
        granule_Size = 64;  /* sizty four byte coherency granule size */

    /* define offset */
    offset = ( buf_Entry->trx_Info.snoop_Current_Address - buf_Entry->trx_Info.snoop_Start_Address) / granule_Size;

    /* set snoop mask flag */
    if ( snoop_Result == RIO_SNOOP_HIT )
        buf_Entry->trx_Info.snoop_Mask[offset] = RIO_TRUE;
    else if ( snoop_Result == RIO_SNOOP_MISS )
        buf_Entry->trx_Info.snoop_Mask[offset] = RIO_FALSE;

    /* copy snooped data into snoop buffer */
    if ( snoop_Result == RIO_SNOOP_HIT && data != NULL )
        for ( i = 0; i < granule_Size / RIO_LL_DW_BYTE_SIZE; i++ )
            buf_Entry->trx_Info.snoop_Data[(offset * granule_Size) / RIO_LL_DW_BYTE_SIZE + i] = data[i];

    if ( buf_Entry->trx_Info.snoop_Current_Address != buf_Entry->trx_Info.snoop_Finish_Address)
    /* all data isn't snooped yet */
    {
        /* increment current snoop address */
        buf_Entry->trx_Info.snoop_Current_Address += granule_Size;

        /* fill in snoop request data structure */
        snoop_Req.address          = buf_Entry->trx_Info.snoop_Current_Address;
        snoop_Req.extended_Address = buf_Entry->trx_Info.extended_Address;
        snoop_Req.xamsbs           = buf_Entry->trx_Info.xamsbs;
        snoop_Req.lttype           = buf_Entry->trx_Info.snoop_LTType;
        snoop_Req.ttype            = buf_Entry->trx_Info.snoop_TType;

        /* perform snoop request via callback invocation */
        /* set semaphot state */
        add_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_SNOOP);        

        /* invoke callback */
        ll_Data->callback_Tray.rio_Snoop_Request(
            ll_Data->callback_Tray.snoop_Context,
            snoop_Req
        );

        /* unset semaphor state */
        remove_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_SNOOP);     

        return RIO_OK;
    }
    else
    /* whole portion of data was snooped */
    {
        /* remove element from the snoop queue */
        remove_Element_From_Queue(element, &snoop_Head);
        return_Queue_Element_To_Pool(element, &snoop_Pool);

        ll_Data->target_Queues.snoop_Queue_Head = snoop_Head;
        ll_Data->target_Queues.snoop_Queue_Pool = snoop_Pool;

        /* define the event for the futher transaction processing */
        if ( buf_Entry->trx_Info.snoop_Mask[0] )
            event = RIO_EVENT_SNOOP_HIT;
        else 
            event = RIO_EVENT_SNOOP_MISS;

        for ( i = 1; i < (buf_Entry->trx_Info.snoop_Finish_Address - buf_Entry->trx_Info.snoop_Start_Address 
            + granule_Size) / granule_Size; i++ )
            if ( buf_Entry->trx_Info.snoop_Mask[i] )
                event = RIO_EVENT_SNOOP_HIT;

        /* process transaction */
        buf_Tag = element->buf_Tag;

        return process_Transaction( ll_Data, buf_Tag, event );
    }

    return RIO_OK;        
}

/***************************************************************************
 * Function :    RIO_LL_Get_Memory_Data
 *
 * Description:  this function is to be used when memory controller ready
 *               to get data from packet after memory request.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_LL_Get_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *data
)
{
    RIO_LL_DS_T      *ll_Data;   /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T buf_Tag;    /* buffer tag of the request        */
    RIO_LL_BUFFER_T  *buf_Entry; /* buffer entry of the corresponding request */
    RIO_LL_QUEUE_T   *memory_Head;
    RIO_LL_QUEUE_T   *memory_Pool, *element;
    RIO_UCHAR_T      be = 0x00;    /* byte enable field */

    RIO_BOOL_T       snoop_Result; /* result of the previous snoop oeration */
    RIO_UCHAR_T      granule_Size; /* size of the cache coherence granule */
    RIO_WORD_T       gran_Align_Mask; /* mask for the address aligment */

    RIO_UCHAR_T      mem_Piece_Number; /* number of the memory piece to be writen from the snoop buffer */ 
    RIO_UCHAR_T      mem_Piece_Size;   /* size of the memory piece to be writen from the snoop buffer   */
    RIO_UCHAR_T      mem_Piece_Offset; /* offset in snoop_Granules of the memory piece to be writen     */
                                       /* from the snoop buffer start                                   */

    RIO_ADDRESS_T    req_Address;      /* address of the yet another WRITE from snoop buffer memory request */
    int              i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check parameters */
    if ( data == NULL )
    {
        /* issue error message */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "memory data", "get data memory response" );

        return RIO_PARAM_INVALID;
    }

    /* check semaphor*/
    if ( (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_INIT ))
        && (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_MEM )))
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer get data memory response");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Logical Layer get data memory response");
        return RIO_OK;
    }

    /* find element in the requests queue waiting for this responce */
    memory_Head = ll_Data->target_Queues.memory_Queue_Head;
    memory_Pool = ll_Data->target_Queues.memory_Queue_Pool;

    element = memory_Head;
    element = find_Queue_Element_With_State( element, RIO_LL_WAIT_FOR_RESPONCE );

    if ( element == NULL )
    {
        /* unknown buffer element */        
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_TRX_NOT_FOUND, "Logical Layer get data memory response");
        return RIO_ERROR;
    }

    /* find out cache granule size */
    if ( ll_Data->inst_Params.coh_Granule_Size_32 )
    {
        granule_Size    = 32;   /* thirty two byte coherency granule size */
        gran_Align_Mask = RIO_LL_ADDRESS_GRAN_32_ALIGN;
    }
    else
    {
        granule_Size = 64;      /* sizty four byte coherency granule size */
        gran_Align_Mask = RIO_LL_ADDRESS_GRAN_64_ALIGN;
    }

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, element->buf_Tag );
    if ( buf_Entry == NULL)
    {
        /* issue error message */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, element->buf_Tag, "Logical Layer get data memory response" );
        return RIO_ERROR;
    }

    /* fill in data supplied by the request */
    if ( buf_Entry->trx_Info.get_Data_From_Snoop )
    {
        /* fill in supported data field */
        for ( i = 0; i < buf_Entry->trx_Info.data_DW_Size; i++ )        
            data[i] = buf_Entry->trx_Info.snoop_Data[buf_Entry->trx_Info.data_Offset + i];

        /* increment snoop ops counter */
        buf_Entry->trx_Info.current_From_Snoop++;

        /* check if we need to perform yet another memory op from snoop buffer */
        if ( buf_Entry->trx_Info.current_From_Snoop < buf_Entry->trx_Info.count_From_Snoop )
        {
            /* perform yet another memory op from snoop buffer */

            /* find out memory piece offset for the memory WRITE op from snoop buffer */
            mem_Piece_Number = 0;
            i = 0;
            do
            {               
                if ( ( i == 0 && buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE ) ||
                    ( i > 0 && buf_Entry->trx_Info.snoop_Mask[i - 1] == RIO_FALSE && 
                    buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE ) )
                mem_Piece_Number++;
                i++;
            }
            while ( i < ( buf_Entry->trx_Info.snoop_DW_Size * RIO_LL_DW_BYTE_SIZE / granule_Size ) 
                && ( mem_Piece_Number - 1 ) != buf_Entry->trx_Info.current_From_Snoop);

            i--;
            mem_Piece_Offset = i;

            /* find out size in granules of the current WRITE from snoop memory piece */
            mem_Piece_Size = 0;
            do
            {
                mem_Piece_Size++;
                i++;
            }
            while ( i < ( buf_Entry->trx_Info.snoop_DW_Size * RIO_LL_DW_BYTE_SIZE / granule_Size ) 
                && buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE);

            /* find out data offset for the WRITE from snoop memory operation */
            buf_Entry->trx_Info.data_Offset = mem_Piece_Offset * granule_Size / RIO_LL_DW_BYTE_SIZE;

            /* find out data size for the WRITE from snoop memory operation */
            buf_Entry->trx_Info.data_DW_Size = mem_Piece_Size * granule_Size / RIO_LL_DW_BYTE_SIZE;

            /* find out address for the request */
            req_Address = ( buf_Entry->trx_Info.address & gran_Align_Mask ) + mem_Piece_Offset * granule_Size;

            /* perform WRITE memory request */
            /* set semaphor */
            add_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);       

            /* invoke callback */
            ll_Data->callback_Tray.rio_Memory_Request(
                ll_Data->callback_Tray.memory_Context,
                req_Address,
                buf_Entry->trx_Info.extended_Address,
                buf_Entry->trx_Info.xamsbs,
                buf_Entry->trx_Info.data_DW_Size,
                RIO_MEM_WRITE,
                is_GSM_Transaction( ll_Data, element->buf_Tag ),
                be          
            );

            /* unset semaphor state */
            remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);        

            return RIO_OK;
        }
        else
        {
            /* 
             * if it is needed to make a memory access in a subdoubleword quantity
             * convert offset and byte_Num into be 
             */
            if (buf_Entry->trx_Info.dw_Size == 1)
            {
                for (i = 0; i < buf_Entry->trx_Info.byte_Num; i++)                      
                {
                    be = be >> 1;
                    be = be | 0x80; /* set most significant bit */
                }
                be = be >> buf_Entry->trx_Info.offset;
            }

            switch ( buf_Entry->trx_Info.mem_Type )
            {
                case RIO_MEM_WRITE:
                {
                    /* for the write operation it is no need to perform futher operation      */
                    /* because in this case it is only needed to write data from snoop buffer */
                    break;
                }

                case RIO_MEM_READ:
                case RIO_MEM_WRAP_READ: 
                {
                    /* it is needed to find out if it is necessary to perform read */
                    /* request if not whole data was snooped                       */
                    snoop_Result = RIO_TRUE;
                    for ( i = 0; i < buf_Entry->trx_Info.snoop_DW_Size * RIO_LL_DW_BYTE_SIZE / granule_Size; i++)
                        snoop_Result &= buf_Entry->trx_Info.snoop_Mask[i];

                    /* if there wasn't complete snoop hit memory READ shall be performed */
                    if ( !snoop_Result )
                    {
                        /* perform original operation */

                        /* set snoop data buffer flag */
                        buf_Entry->trx_Info.get_Data_From_Snoop = RIO_FALSE;

                        /* set semaphor */
                        add_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);       

                        /* invoke callback */
                        ll_Data->callback_Tray.rio_Memory_Request(
                            ll_Data->callback_Tray.memory_Context,
                            buf_Entry->trx_Info.address,
                            buf_Entry->trx_Info.extended_Address,
                            buf_Entry->trx_Info.xamsbs,
                            buf_Entry->trx_Info.dw_Size,
                            buf_Entry->trx_Info.mem_Type,
                            is_GSM_Transaction( ll_Data, element->buf_Tag ),
                            be          
                        );

                        /* unset semaphor state */
                        remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);        

                        return RIO_OK;
                    }
                    else
                    {
                        /* place snooped data to the transaction buffer */
                        if ( buf_Entry->trx_Info.mem_Type == RIO_MEM_READ )
                            RIO_LL_Kernel_Place_Snoop_Data_To_Buffer( ll_Data, element->buf_Tag, RIO_FALSE );
                        else
                            RIO_LL_Kernel_Place_Snoop_Data_To_Buffer( ll_Data, element->buf_Tag, RIO_TRUE );

                    }
                    
                    break;
                }   
                
                default:
                {
                    /* all operations but WRITE and READ require processing of the original  */
                    /* request after write of the snooped data                               */

                    /* set snoop data buffer flag */
                    buf_Entry->trx_Info.get_Data_From_Snoop = RIO_FALSE;

                    /* set semaphor */
                    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);       

                    /* invoke callback */
                    ll_Data->callback_Tray.rio_Memory_Request(
                        ll_Data->callback_Tray.memory_Context,
                        buf_Entry->trx_Info.address,
                        buf_Entry->trx_Info.extended_Address,
                        buf_Entry->trx_Info.xamsbs,
                        buf_Entry->trx_Info.dw_Size,
                        buf_Entry->trx_Info.mem_Type,
                        is_GSM_Transaction( ll_Data, element->buf_Tag ),
                        be          
                    );

                    /* unset semaphor state */
                    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);        

                    return RIO_OK;
                }                
            }
        }
    }
    else
    {
        for ( i = 0; i < buf_Entry->trx_Info.dw_Size; i++ )
            data[i] = buf_Entry->trx_Info.data[i];
    }

    /*
     * Atomic TSWAP transaction has its own unique way of servicing by the memory.
     * At first Set_Memory_Data may invoked if it is needed to write data to memory.
     * But if it is no need to write data to memory Set_Memory_Data won't be invoked.
     * Get_Memory_Data is always invoked coz it is needed to read data from memory.
     */
    if ( buf_Entry->trx_Info.mem_Type != RIO_MEM_ATOMIC_TSWAP  &&
	/* GDA: Bug#133 - Support for ttype C for type 5 packet */
	buf_Entry->trx_Info.mem_Type != RIO_MEM_ATOMIC_SWAP )
    {
        /* remove element from the response queue */
        remove_Element_From_Queue(element, &memory_Head);
        return_Queue_Element_To_Pool(element, &memory_Pool);

        ll_Data->target_Queues.memory_Queue_Head = memory_Head;
        ll_Data->target_Queues.memory_Queue_Pool = memory_Pool;

        /* processing transaction */
        buf_Tag = element->buf_Tag;
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_MEMORY_COMPLETE );
    }
    else 
        return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_LL_Set_Memory_Data
 *
 * Description:  this function is to be used when memory controller ready
 *               to provide data to LL after memory request.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Set_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *data
)
{
    RIO_LL_DS_T      *ll_Data;   /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T buf_Tag;    /* buffer tag of the request        */
    RIO_LL_BUFFER_T  *buf_Entry; /* buffer entry of the corresponding request */
    RIO_LL_QUEUE_T   *memory_Head;
    RIO_LL_QUEUE_T   *memory_Pool, *element;
    int              i;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check parameters */
    if ( data == NULL )
    {
        /* issue error message */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "memory data", "set data memory response" );
        return RIO_PARAM_INVALID;
    }

    /* check semaphor*/
    if ( (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_INIT ))
        && (!check_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_MEM )))
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "Logical Layer set data memory response");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Logical Layer set data memory response");
        return RIO_OK;
    }

    /* find element in the requests queue waiting for this responce */
    memory_Head = ll_Data->target_Queues.memory_Queue_Head;
    memory_Pool = ll_Data->target_Queues.memory_Queue_Pool;

    element = memory_Head;
    element = find_Queue_Element_With_State( element, RIO_LL_WAIT_FOR_RESPONCE );

    if ( element == NULL )
    {
        /* unknown buffer element */        
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_TRX_NOT_FOUND, "Logical Layer set data memory response");
        return RIO_ERROR;
    }

    /* remove element from the response queue */
    remove_Element_From_Queue(element, &memory_Head);
    return_Queue_Element_To_Pool(element, &memory_Pool);

    ll_Data->target_Queues.memory_Queue_Head = memory_Head;
    ll_Data->target_Queues.memory_Queue_Pool = memory_Pool;

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, element->buf_Tag );
    if ( buf_Entry == NULL)
    {
        /* issue error message */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, element->buf_Tag, "Logical Layer set data memory response" );
        return RIO_ERROR;
    }

    /* fill in buffer data */
    for ( i = 0; i < buf_Entry->trx_Info.dw_Size; i++ )
        buf_Entry->trx_Info.data[i] = data[i];

    /* processing transaction */
    buf_Tag = element->buf_Tag;
    return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_MEMORY_COMPLETE );
}

/***************************************************************************
 * Function :    RIO_LL_Ext_Features_Read
 *
 * Description:  function invokes read extended features space callback
 *
 * Returns:
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Ext_Features_Read( 
    RIO_CONTEXT_T   handle,
    RIO_CONF_OFFS_T config_Offset,  
    unsigned long   *data 
)
{
    RIO_LL_DS_T *ll_Data;
    int         result;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    if ( data == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "data structure", "extended features read");
        return RIO_PARAM_INVALID;
    }

    if ( ll_Data->callback_Tray.rio_Remote_Conf_Read == NULL )
        return RIO_ERROR;
    
    add_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_DONE );

    result = ll_Data->callback_Tray.rio_Remote_Conf_Read(
        ll_Data->callback_Tray.extended_Features_Context,
        config_Offset,
        data
    );
    
    remove_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_DONE );
    return result;
}

/***************************************************************************
 * Function :    RIO_LL_Ext_Features_Write
 *
 * Description:  function invokes read extended features space callback
 *
 * Returns:
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Ext_Features_Write( 
    RIO_CONTEXT_T   handle, 
    RIO_CONF_OFFS_T config_Offset, 
    unsigned long   data 
)
{
    RIO_LL_DS_T *ll_Data;
    int         result;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;
    
    if ( ll_Data->callback_Tray.rio_Remote_Conf_Write == NULL )
        return RIO_ERROR;
    
    add_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_DONE );

    result = ll_Data->callback_Tray.rio_Remote_Conf_Write(
        ll_Data->callback_Tray.extended_Features_Context, 
        config_Offset,
        data
    );
    
    remove_Semaphor_State( ll_Data, RIO_LL_SEMAPHOR_DONE );
    return result;
}

/*****************************************************************************
 *
 * 
 *INTERNAL KERNEL INTERFACE routines (used by trx. handlers) are defined below
 * (implementation of routines declared in rio_ll_kernel.h)
 *
 */

/***************************************************************************
 * Function :    RIO_LL_Kernel_Have_Memory
 *
 * Description:  function defines if we have memory in PE
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Have_Memory( RIO_LL_DS_T *handle )
{
#ifdef _DEBUG
    assert( handle );
#endif

    return handle->inst_Params.has_Memory;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Have_Processor
 *
 * Description:  function defines if we have processor in PE
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Have_Processor( RIO_LL_DS_T *handle )
{
#ifdef _DEBUG
    assert( handle );
#endif

    return handle->inst_Params.has_Processor;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Have_Mailbox
 *
 * Description:  function defines if there is message box for request 
 *               identified by the buf_Tag in PE
 *
 * Returns:      RIO_TRUE, RIO_FALSE or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Have_Mailbox( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag )
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert( handle );
#endif

    /* get buffer entry for the transaction */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );
    /* GDA : Bug #123 Support for extended mailbox (xmbox) feature */
     if (( handle->init_Params.ext_Mailbox_Support == RIO_TRUE) && ( handle->temp_Buffer.msglen == 0) )
     {
     buf_Entry->trx_Info.mbox_Xmbox = (RIO_UCHAR_T)((buf_Entry->trx_Info.msgseg & 0x0f) << 2);
     buf_Entry->trx_Info.mbox_Xmbox |= (RIO_UCHAR_T)( buf_Entry->trx_Info.mbox & 0x03);
     
    printf("\n Message stored in mailbox number  %d  \n", buf_Entry->trx_Info.mbox_Xmbox);
     }
 if((handle->init_Params.ext_Mailbox_Support == RIO_TRUE) && (handle->temp_Buffer.msglen ==0) )
  
 {
    switch (buf_Entry->trx_Info.mbox_Xmbox)
    {
        case 0: /* mailbox 1 */
           return handle->inst_Params.mbox1;

        case 1: /* mailbox 2 */
        return handle->inst_Params.mbox2;

        case 2: /* mailbox 3 */
            return handle->inst_Params.mbox3;

        case 3: /* mailbox 4 */
           return handle->inst_Params.mbox4;
        case 4: /* mailbox 5 */
           return handle->inst_Params.mbox5;

        case 5: /* mailbox 6 */
        return handle->inst_Params.mbox6;

        case 6: /* mailbox 7 */
            return handle->inst_Params.mbox7;

        case 7: /* mailbox 8  */
           return handle->inst_Params.mbox8;
        case 8: /* mailbox 9 */
           return handle->inst_Params.mbox9;

        case 9: /* mailbox 10 */
        return handle->inst_Params.mbox10;

        case 10: /* mailbox 11 */
            return handle->inst_Params.mbox11;

        case 11: /* mailbox 12 */
           return handle->inst_Params.mbox12;
        case 12: /* mailbox 13 */
           return handle->inst_Params.mbox13;

        case 13: /* mailbox 14 */
        return handle->inst_Params.mbox14;

        case 14: /* mailbox 15 */
            return handle->inst_Params.mbox15;

        case 15: /* mailbox 16 */
           return handle->inst_Params.mbox16;
        case 16: /* mailbox 17 */
           return handle->inst_Params.mbox17;

        case 17: /* mailbox 18 */
        return handle->inst_Params.mbox18;

        case 18: /* mailbox 19 */
            return handle->inst_Params.mbox19;

        case 19: /* mailbox 20 */
           return handle->inst_Params.mbox20;
        case 20: /* mailbox 21 */
           return handle->inst_Params.mbox21;

        case 21: /* mailbox 22 */
        return handle->inst_Params.mbox22;

        case 22: /* mailbox 23 */
            return handle->inst_Params.mbox23;

        case 23: /* mailbox 24 */
           return handle->inst_Params.mbox24;
        case 24: /* mailbox 25 */
           return handle->inst_Params.mbox25;

        case 25: /* mailbox 26 */
        return handle->inst_Params.mbox26;

        case 26: /* mailbox 27 */
            return handle->inst_Params.mbox27;

        case 27: /* mailbox 28 */
           return handle->inst_Params.mbox28;
        case 28: /* mailbox 29 */
           return handle->inst_Params.mbox29;

        case 29: /* mailbox 30 */
        return handle->inst_Params.mbox30;

        case 30: /* mailbox 31 */
            return handle->inst_Params.mbox31;

        case 31: /* mailbox 32 */
           return handle->inst_Params.mbox32;
        case 32: /* mailbox 33 */
           return handle->inst_Params.mbox33;

        case 33: /* mailbox 34 */
        return handle->inst_Params.mbox34;

        case 34: /* mailbox 35 */
            return handle->inst_Params.mbox35;

        case 35: /* mailbox 36 */
           return handle->inst_Params.mbox36;
	   
        case 36: /* mailbox 37 */
           return handle->inst_Params.mbox37;

        case 37: /* mailbox 38 */
        return handle->inst_Params.mbox38;

        case 38: /* mailbox 39 */
            return handle->inst_Params.mbox39;

        case 39: /* mailbox 40 */
           return handle->inst_Params.mbox40;
        case 40: /* mailbox 41 */
           return handle->inst_Params.mbox41;

        case 41: /* mailbox 42 */
        return handle->inst_Params.mbox42;

        case 42: /* mailbox 43 */
            return handle->inst_Params.mbox43;

        case 43: /* mailbox 44 */
           return handle->inst_Params.mbox44;
        case 44: /* mailbox 45 */
           return handle->inst_Params.mbox45;

        case 45: /* mailbox 46 */
        return handle->inst_Params.mbox46;

        case 46: /* mailbox 47 */
            return handle->inst_Params.mbox47;

        case 47: /* mailbox 48 */
           return handle->inst_Params.mbox48;
        case 48: /* mailbox 49 */
           return handle->inst_Params.mbox49;

        case 49: /* mailbox 50 */
        return handle->inst_Params.mbox50;

        case 50: /* mailbox 51 */
            return handle->inst_Params.mbox51;

        case 51: /* mailbox 52 */
           return handle->inst_Params.mbox52;

        
        case 52: /* mailbox 53 */
           return handle->inst_Params.mbox53;
	
	   
        case 53: /* mailbox 54 */
           return handle->inst_Params.mbox54;
	 
	   
        case 54: /* mailbox 55 */
           return handle->inst_Params.mbox55;

	   
        case 55: /* mailbox 56 */
           return handle->inst_Params.mbox56;

	   
        case 56: /* mailbox 57 */
	    return handle->inst_Params.mbox57;

	    
        case 57: /* mailbox 58 */
           return handle->inst_Params.mbox58;

	
        case 58: /* mailbox 59 */
           return handle->inst_Params.mbox59;

        
        case 59: /* mailbox 60 */
           return handle->inst_Params.mbox60;

	
        case 60: /* mailbox 61 */
           return handle->inst_Params.mbox61;

	
        case 61: /* mailbox 62 */
           return handle->inst_Params.mbox62;

	
        case 62: /* mailbox 63 */
           return handle->inst_Params.mbox63;

	
        case 63: /* mailbox 64 */
           return handle->inst_Params.mbox64;

        
    }  
 }
 
 else
  {


    switch ( buf_Entry->trx_Info.mbox )
    {
        case 0: /* mailbox 1 */
            return handle->inst_Params.mbox1;

        case 1: /* mailbox 2 */
            return handle->inst_Params.mbox2;

        case 2: /* mailbox 3 */
            return handle->inst_Params.mbox3;

        case 3: /* mailbox 4 */
            return handle->inst_Params.mbox4;

    }
 }
	     
  /* GDA : Bug #123 Support for extended mailbox(xmbox) feature */


    return RIO_FALSE;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Have_Doorbell
 *
 * Description:  function defines if we have doorbell reception hardware in PE
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Have_Doorbell( RIO_LL_DS_T *handle )
{
#ifdef _DEBUG
    assert( handle );
#endif

    return handle->inst_Params.has_Doorbell;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Have_Directory
 *
 * Description:  function defines if we have cache directory hardware in PE
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Have_Directory( RIO_LL_DS_T *handle )
{
#ifdef _DEBUG
    assert( handle );
#endif

    if ( (handle->callback_Tray.rio_Read_Dir != NULL)
        && (handle->callback_Tray.rio_Write_Dir != NULL) )
        return RIO_TRUE;
    else
        return RIO_FALSE;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Local_Responce
 *
 * Description:  function invokes local responce call   back
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Local_Responce(
    RIO_LL_DS_T       *handle, 
    RIO_LL_BUF_TAG_T  buf_Tag, 
    RIO_LOCAL_RTYPE_T responce
)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle);
#endif
    
    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel local response function");
        return RIO_ERROR;
    }
    
    /* set semaphor state */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* invoke callback */
    handle->callback_Tray.rio_Local_Response(
        handle->callback_Tray.local_Device_Context, 
        buf_Entry->trx_Info.user_Trx_Tag, 
        responce
    );

    /* unset semaphor state */  
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_GSM_Request_Done
 *
 * Description:  function invokes GSM done callback
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_GSM_Request_Done(
    RIO_LL_DS_T      *handle,   
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_BOOL_T       supply_Data, 
    RIO_REQ_RESULT_T result
)
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_DW_T        *data;
    RIO_UCHAR_T     dw_Size;

#ifdef _DEBUG
    assert(handle);
#endif

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel GSM request done function");
        return RIO_ERROR;
    }
    
    /* set semaphor state */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* check if it is needed to support the data in responce */
    if ( supply_Data )
    {
        data    = buf_Entry->trx_Info.data;
        dw_Size = buf_Entry->trx_Info.dw_Size;
    }
    else 
    {
        data    = NULL;
        dw_Size = 0;
    }

    /* invoke callback */
    handle->callback_Tray.rio_GSM_Request_Done(
        handle->callback_Tray.local_Device_Context, 
        buf_Entry->trx_Info.user_Trx_Tag, 
        result, 
        buf_Entry->trx_Info.resp_Status, 
        dw_Size,
        data
    );
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_IO_Request_Done
 *
 * Description:  function invokes IO done callback
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_IO_Request_Done(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_BOOL_T       supply_Data, 
    RIO_REQ_RESULT_T result
)
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_DW_T        *data;
    RIO_UCHAR_T     dw_Size;

#ifdef _DEBUG
    assert(handle);
#endif
    
    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel IO request done function");
        return RIO_ERROR;
    }
    
    /* set semaphor state */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* check if it is needed to support the data in responce */
    if ( supply_Data )
    {
        data    = buf_Entry->trx_Info.data;
        dw_Size = buf_Entry->trx_Info.dw_Size;
    }
    else 
    {
        data    = NULL;
        dw_Size = 0;
    }

    /* invoke callback */
    handle->callback_Tray.rio_IO_Request_Done(
        handle->callback_Tray.local_Device_Context, 
        buf_Entry->trx_Info.user_Trx_Tag, 
        result, 
        buf_Entry->trx_Info.resp_Status, 
        dw_Size,
        data
    );
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_MP_Request_Done
 *
 * Description:  function invokes message passing done callback
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_MP_Request_Done(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_REQ_RESULT_T result
)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle);
#endif
    
    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel Message Passing request done function");
        return RIO_ERROR;
    }

    /* set semaphor state */    
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* invoke callback */
    handle->callback_Tray.rio_MP_Request_Done(
        handle->callback_Tray.local_Device_Context, 
        buf_Entry->trx_Info.user_Trx_Tag, 
        result, 
        buf_Entry->trx_Info.resp_Status
    );
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Doorbell_Request_Done
 *
 * Description:  function invokes doorbell done callback
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Doorbell_Request_Done(
    RIO_LL_DS_T      *handle,   
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_REQ_RESULT_T result 
)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle);
#endif
    
    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel Doorbell request done function");
        return RIO_ERROR;
    }

    /* set semaphor state */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* invoke callback */
    handle->callback_Tray.rio_Doorbell_Request_Done(
        handle->callback_Tray.local_Device_Context, 
        buf_Entry->trx_Info.user_Trx_Tag, 
        result, 
        buf_Entry->trx_Info.resp_Status
    );
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Config_Request_Done
 *
 * Description:  function invokes config done callback
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Config_Request_Done(
    RIO_LL_DS_T      *handle,   
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_BOOL_T       supply_Data, 
    RIO_REQ_RESULT_T result
)
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_DW_T        *data;
    RIO_UCHAR_T     dw_Size;

#ifdef _DEBUG
    assert(handle);
#endif
    
    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel Maintenance request done function");
        return RIO_ERROR;
    }

    /* set semaphor state */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* check if it is needed to support the data in responce */
    if ( supply_Data )
    {
        data    = buf_Entry->trx_Info.data;
        dw_Size = buf_Entry->trx_Info.dw_Size;
    }
    else 
    {
        data    = NULL;
        dw_Size = 0;
    }

    /* invoke callback */
    handle->callback_Tray.rio_Config_Request_Done(
        handle->callback_Tray.local_Device_Context, 
        buf_Entry->trx_Info.user_Trx_Tag, 
        result, 
        buf_Entry->trx_Info.resp_Status, 
        dw_Size,
        data
    );

    /* unset semaphor state */  
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_MP_Remote_Request
 *
 * Description:  function invokes MP remote request callback accordingly 
 *               to data supported in buffer entry with tag buf_Tag
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_MP_Remote_Request(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag)
{
    RIO_LL_DS_T         *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T     *buf_Entry;
    RIO_REMOTE_MP_REQ_T message;
    RIO_UCHAR_T         dw_Size = 0;
    int                 result;

#ifdef _DEBUG
    assert(handle);
#endif
    
    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check if there is mailbox hardware in PE */
    if ( !RIO_LL_Kernel_Have_Mailbox( handle, buf_Tag ) )
    {
        /* issue warning message */
        RIO_LL_Message( handle, RIO_LL_WARN_NO_MAILBOX );

        /* return error code */
        return RIO_ERROR;
    }

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel Message Passing remote request function");
        return RIO_ERROR;
    }

    translate_SSIZE_To_DW_Size( buf_Entry->trx_Info.ssize, &dw_Size );

    /* compose message structure */
    message.msglen  = buf_Entry->trx_Info.msglen;
    message.ssize   = dw_Size;
    message.letter  = buf_Entry->trx_Info.letter;
    message.mbox    = buf_Entry->trx_Info.mbox; 
    message.msgseg  = buf_Entry->trx_Info.msgseg;
    message.dw_Size = buf_Entry->trx_Info.dw_Size;                      
    message.data    = buf_Entry->trx_Info.data;

    /* in case message is targeted to the extended mailbox compose mbox number */
    /* from the msgseg and mbox packet fields                                  */
    if ((ll_Data->init_Params.ext_Mailbox_Support == RIO_TRUE) && (message.msglen == 0))
    {
        message.mbox    &= 0x03;
        message.mbox    |= (message.msgseg << 2) & 0x3C;
        message.msgseg  =  0x00;
    }

    /* set proper semaphor state and invoke callback */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    result = handle->callback_Tray.rio_MP_Remote_Request(
        handle->callback_Tray.mailbox_Context, 
        message
    );

    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return result;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Doorbell_Remote_Request
 *
 * Description:  function invokes doorbell remote request callback accordingly
 *               to data supported in buffer entry with tag buf_Tag
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Doorbell_Remote_Request(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag)
{
    RIO_LL_BUFFER_T *buf_Entry;
    int             result;

#ifdef _DEBUG
    assert(handle);
#endif

    /* check if there is doorbell hardware in PE */
    if ( !RIO_LL_Kernel_Have_Doorbell( handle ) )
    {
        /* issue warning message */
        RIO_LL_Message( handle, RIO_LL_WARN_NO_DOORBELL );

        /* return error code */
        return RIO_ERROR;
    }

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel Doorbell remote request function");
        return RIO_ERROR;
    }

    /* set semaphor state and invoke callback */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    result = handle->callback_Tray.rio_Doorbell_Remote_Request(
        handle->callback_Tray.doorbell_Context, 
        buf_Entry->trx_Info.doorbell_Info
    );
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return result;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Set_Config_Reg
 *
 * Description:  function invokes serie of the write configuration register
 *               callbacks accordingly to data supporetd in the buffer entry 
 *               with tag buf_Tag.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Set_Config_Reg( 
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;   /* buffer entry pointer              */
    RIO_CONF_OFFS_T conf_Address; /* offset inside the config space    */
    RIO_BOOL_T      write_MSW = RIO_FALSE;    /* most significant word write flag  */
    RIO_BOOL_T      write_LSW = RIO_FALSE;    /* least significant word write flag */
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_BOOL_T      write_HW = RIO_FALSE;      /* half word write flag  */
    /* GDA: Bug#124 - Support for Data Streaming packet */

    int             result;
    int             i;

#ifdef _DEBUG
    assert(handle);
#endif

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel set configuration register function");
        return RIO_ERROR;
    }

    /* if it is IO transaction in the buffer entry compose config_Offset from the address */
    if ( is_IO_Transaction( handle, buf_Tag ) )
        buf_Entry->trx_Info.conf_Offset = ( buf_Entry->trx_Info.address >= ((get_LCSBAR( handle ) << 3) & ~0x7 )) ?
            (buf_Entry->trx_Info.address - ((get_LCSBAR( handle ) << 3) & ~0x7) ) :
            (0xFFFFFFF8 - ((get_LCSBAR( handle) << 3) & ~0x7) + buf_Entry->trx_Info.address + 8);

    /* define which words of the doublewords should be read */
    if ( (buf_Entry->trx_Info.dw_Size > 1) 
        || ((buf_Entry->trx_Info.dw_Size == 1) && (buf_Entry->trx_Info.byte_Num == RIO_LL_DW_BYTE_SIZE)))
    {
        write_LSW = RIO_TRUE;
        write_MSW = RIO_TRUE;
    }

    if ( (buf_Entry->trx_Info.dw_Size == 1) && (buf_Entry->trx_Info.byte_Num < RIO_LL_DW_BYTE_SIZE) )
    {
        write_MSW = ( (buf_Entry->trx_Info.offset / RIO_LL_WORD_BYTE_SIZE) == 0 );
        write_LSW = ( (buf_Entry->trx_Info.offset / RIO_LL_WORD_BYTE_SIZE) == 1 );
    }

    /* GDA: Bug#124 - Support for Data Streaming packet */
     if ( (buf_Entry->trx_Info.length > 1 )
		              || ((buf_Entry->trx_Info.mtu > 32) || (buf_Entry->trx_Info.mtu== 32)))
		        write_HW = RIO_TRUE;
		 /*bug-124*/
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* set result default value */
    result = RIO_OK;

    /* set semaphor state and invoke callback */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

/* GDA: Bug#124 - Support for Data Streaming packet added below IF loop */

if( buf_Entry->trx_Info.req_FType != RIO_DS)
{	
    for (i = 0; i < buf_Entry->trx_Info.dw_Size; i++)
    {
        if ( write_MSW )
        {
            conf_Address = buf_Entry->trx_Info.conf_Offset + i * RIO_LL_DW_BYTE_SIZE;       
            if ( handle->callback_Tray.rio_Set_Config_Reg(
                handle->callback_Tray.rio_Config_Context, 
                conf_Address, 
                buf_Entry->trx_Info.data[i].ms_Word
            ) != RIO_OK )
            {
                result = RIO_ERROR;
                break;
            }
        }

        if ( write_LSW )
        {
            conf_Address = buf_Entry->trx_Info.conf_Offset + i * RIO_LL_DW_BYTE_SIZE + RIO_LL_WORD_BYTE_SIZE;       
            if ( handle->callback_Tray.rio_Set_Config_Reg(
                handle->callback_Tray.rio_Config_Context, 
                conf_Address, 
                buf_Entry->trx_Info.data[i].ls_Word
            ) != RIO_OK )
            {
                result = RIO_ERROR;
                break;
            }
        }
    }
}
else
{
	if(write_HW)
	result = RIO_OK;
}
/* GDA: Bug#124 - Support for Data Streaming packet */

    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return result;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Get_Config_Reg
 *
 * Description:  function invokes serie of the read configuration register 
 *               callbacks accordingly to data supporetd in the buffer entry 
 *               with tag buf_Tag.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Get_Config_Reg( 
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;   /* buffer entry pointer             */
    RIO_CONF_OFFS_T conf_Address; /* offset inside the config space   */
    RIO_BOOL_T      read_MSW = RIO_FALSE;     /* most significant word read flag  */
    RIO_BOOL_T      read_LSW = RIO_FALSE;     /* least significant word read flag */
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_BOOL_T      read_HW;      /* half word read flag */
    /* GDA: Bug#124 - Support for Data Streaming packet */

    int             result;
    int             i;

#ifdef _DEBUG
    assert(handle);
#endif

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel get configuration register function");
        return RIO_ERROR;
    }

    /* if it is IO transaction in the buffer entry compose config_Offset from the address */
    if ( is_IO_Transaction( handle, buf_Tag ) )
        buf_Entry->trx_Info.conf_Offset = ( buf_Entry->trx_Info.address >= ((get_LCSBAR( handle ) << 3) & ~0x7 )) ?
            (buf_Entry->trx_Info.address - ((get_LCSBAR( handle ) << 3) & ~0x7) ) :
            (0xFFFFFFF8 - ((get_LCSBAR( handle) << 3) & ~0x7) + buf_Entry->trx_Info.address + 8);

    /* define which words of the doublewords should be read */
    if ( (buf_Entry->trx_Info.dw_Size > 1) 
        || ((buf_Entry->trx_Info.dw_Size == 1) && (buf_Entry->trx_Info.byte_Num == RIO_LL_DW_BYTE_SIZE)))
    {
        read_LSW = RIO_TRUE;
        read_MSW = RIO_TRUE;
    }

    if ( (buf_Entry->trx_Info.dw_Size == 1) && (buf_Entry->trx_Info.byte_Num < RIO_LL_DW_BYTE_SIZE) )
    {
        read_MSW = ( (buf_Entry->trx_Info.offset / RIO_LL_WORD_BYTE_SIZE) == 0 );
        read_LSW = ( (buf_Entry->trx_Info.offset / RIO_LL_WORD_BYTE_SIZE) == 1 );
    }
   
    /* GDA: Bug#124 - Support for Data Streaming packet */
     if ( (buf_Entry->trx_Info.length > 0)
		             || ((buf_Entry->trx_Info.mtu > 32) || (buf_Entry->trx_Info.mtu== 32)))
		        read_HW = RIO_TRUE;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* set result default value */
    result = RIO_OK;

    /* set semaphor state and invoke callback */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );


/* GDA: Bug#124 - Support for Data Streaming packet added below IF loop */
if( buf_Entry->trx_Info.req_FType != RIO_DS)
{	
    for (i = 0; i < buf_Entry->trx_Info.dw_Size; i++)
    {
        if ( read_MSW )
        {
            conf_Address = buf_Entry->trx_Info.conf_Offset + i * RIO_LL_DW_BYTE_SIZE;       
            if ( handle->callback_Tray.rio_Get_Config_Reg(
                handle->callback_Tray.rio_Config_Context, 
                conf_Address, 
                &(buf_Entry->trx_Info.data[i].ms_Word)
            ) != RIO_OK )
            {
                result = RIO_ERROR;
                break;
            }
        }

        if ( read_LSW )
        {
            conf_Address = buf_Entry->trx_Info.conf_Offset + i * RIO_LL_DW_BYTE_SIZE + RIO_LL_WORD_BYTE_SIZE;       
            if ( handle->callback_Tray.rio_Get_Config_Reg(
                handle->callback_Tray.rio_Config_Context, 
                conf_Address, 
                &(buf_Entry->trx_Info.data[i].ls_Word)
            ) != RIO_OK )
            {
                result = RIO_ERROR;
                break;
            }
        }
    }
}
else
{
	if(read_HW)
	result = RIO_OK;
}
/* GDA: Bug#124 - Support for Data Streaming packet */
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return result;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Port_Write
 *
 * Description:  function makes congiguration port write operation 
 *               accordingly to the transaction data stored in the buffer 
 *               entry with tag buf_Tag
 *               
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Port_Write(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;   /* buffer entry pointer             */
    RIO_UCHAR_T     sub_DW_Pos;   /* subdoubleword position of the port write operation   */
                                  /* meaningfull only in case of the dw == 1              */
                                  /* 0 - most significant word; 1- least significant word */
                                  /* else - whole doubleword                              */
    int             result;

#ifdef _DEBUG
    assert(handle);
#endif

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel port write function");
        return RIO_ERROR;
    }

    /* find out the value of the sub_DW_Pos flag */
    if ( (buf_Entry->trx_Info.dw_Size == 1) && (buf_Entry->trx_Info.byte_Num < RIO_LL_DW_BYTE_SIZE) )
        sub_DW_Pos = (buf_Entry->trx_Info.offset / RIO_LL_WORD_BYTE_SIZE);
    else /* if ( buf_Entry->trx_Info.dw_Size > 1 ) */
        sub_DW_Pos = 2; /* subdw flag value to make whole doubleword access */

    /* set semaphor state and invoke callback */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    result = handle->callback_Tray.rio_Port_Write_Remote_Request(
        handle->callback_Tray.port_Context,
        buf_Entry->trx_Info.dw_Size,
        sub_DW_Pos,
        buf_Entry->trx_Info.data
    );
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return result;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Is_To_Config_Space
 *
 * Description:  function returns RIO_TRUE in case of the transaction stored 
 *               in buffer entry with tag buf_Tag is an IO transaction routed
 *               to the configuration space by the LCSBAR and LCSHBAR 
 *               registers.
 *               
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Is_To_Config_Space(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;   /* buffer entry pointer             */
    RIO_WORD_T      lcsbar;       /* value of the LCSBAR register     */
    RIO_WORD_T      lcshbar;      /* value of the LCSHBAR register    */

    unsigned long   ext_Address_Support;
    RIO_BOOL_T      ext_Address;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    unsigned short dstream_Support;
    /* GDA: Bug#124 - Support for Data Streaming packet */


    /* 
       words which are formed in order to compare them to LCSBAR (ls_Word) and LCSHBAR (ms_Word)
       These words are formed from address of the transaction using following
       rules:

       34-bit addressing:
        -                         reserved      lcshbar[ 0:31]
        -                         reserved      lcsbar [    0]
        - xamsbs          [ 0: 1] compared with lcsbar [ 1: 2]
        - address         [ 0:28] compared with lcsbar [ 3:31]

       50-bit addressing:
        -                         reserved      lcshbar[ 0:16]
        - xamsbs          [ 0: 1] compared with lcshbar[17:18]
        - extended address[ 0:12] compared with lcshbar[19:31]
        - extended address[13:15] compared with lcsbar [ 0: 2]
        - address         [ 0:28] compared with lcsbar [ 3:31]

       66-bit addressing:
        -                         reserved      lcshbar[    0]
        - xamsbs          [ 0: 1] compared with lcshbar[ 1: 2]
        - extended address[ 0:28] compared with lcshbar[ 3:31]
        - extended address[29:31] compared with lcsbar [ 0: 2]
        - address         [ 0:28] compared with lcsbar [ 3:31]
    */

    RIO_WORD_T      ms_Word   = 0;      
    RIO_WORD_T      ls_Word   = 0;      
    RIO_WORD_T      temp_Word = 0;      

#ifdef _DEBUG
    assert(handle);
#endif

    buf_Entry = get_Buffer_Entry( handle, buf_Tag );
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel function");
        return RIO_FALSE;
    }

/* GDA: Bug#124 - Support for Data Streaming packet added below IF Loop */
    
    if (buf_Entry->trx_Info.req_FType != RIO_DS)	/* Altered for Bug-124 */
    {
	ext_Address_Support = get_Config_Reg_Field( handle, RIO_LL_EXT_ADDRESSING_SUPPORT );
	ext_Address = (ext_Address_Support == RIO_LL_PE_LL_CSR_50 || ext_Address_Support == RIO_LL_PE_LL_CSR_66 ) ? 
        RIO_TRUE : RIO_FALSE;
	
        /* check if the supplied transaction is IO transaction */
        if ( !is_IO_Transaction( handle, buf_Tag ) )
        	return RIO_FALSE;
     }
     else
     {
     dstream_Support =  get_Config_Reg_Field( handle,  RIO_LL_DSTREAMING_SUPPORT);
    /* check if the supplied transaction is DataStream transaction */
    if ( !is_DS_Transaction( handle, buf_Tag ) )
        return RIO_FALSE;
     return RIO_TRUE;    
     }    
/* GDA: Bug#124 - Support for Data Streaming packet */
     
    /* get registers values */
    lcsbar  = get_LCSBAR( handle );
    lcshbar = get_LCSHBAR( handle );

    /* address field of the transaction as it is stored in the model LL has 3 
       less significant bits zeroed */

    /* the hard-coded constants below are in line with the comments which explain
       how comparison takes place and are hard-coded intentionally to make code 
       more understandable; this is the only place (except RIO_LL_Get_Config_Reg
       in the model whether such a comparison takes place 
       For clarity, all shifts are done separately for each branch, although 
       some of them are identical for different branches
    */

    if( ext_Address_Support == RIO_LL_PE_LL_CSR_50 )
    {
        /* 
        50-bit addressing:
         -                         reserved      lcshbar[ 0:16]
         - xamsbs          [ 0: 1] compared with lcshbar[17:18]
         - extended address[ 0:12] compared with lcshbar[19:31]
         - extended address[13:15] compared with lcsbar [ 0: 2]
         - address         [ 0:28] compared with lcsbar [ 3:31]
        */

        temp_Word = (buf_Entry->trx_Info.address >> 3) & 0x1FFFFFFF;
        ls_Word  = ((buf_Entry->trx_Info.extended_Address & 0x7) << 29) & 0xE0000000;
        ls_Word |= temp_Word;

        temp_Word = ((buf_Entry->trx_Info.xamsbs & 0x3) << 13)  & 0x6000;
        ms_Word   = (buf_Entry->trx_Info.extended_Address >> 3) & 0x1FFF;
        ms_Word  |= temp_Word;

        lcshbar &= 0x00007FFF;
    }
    else if( ext_Address_Support == RIO_LL_PE_LL_CSR_66 )
    {
       
        /*
        66-bit addressing:
         -                         reserved      lcshbar[    0]
         - xamsbs          [ 0: 1] compared with lcshbar[ 1: 2]
         - extended address[ 0:28] compared with lcshbar[ 3:31]
         - extended address[29:31] compared with lcsbar [ 0: 2]
         - address         [ 0:28] compared with lcsbar [ 3:31]
        */

        temp_Word = (buf_Entry->trx_Info.address >> 3) & 0x1FFFFFFF;
        ls_Word  = ((buf_Entry->trx_Info.extended_Address & 0x7) << 29) & 0xE0000000;
        ls_Word |= temp_Word;


        temp_Word = ((buf_Entry->trx_Info.xamsbs & 0x3) << 29)  & 0x60000000;
        ms_Word   = (buf_Entry->trx_Info.extended_Address >> 3) & 0x1FFFFFFF;
        ms_Word  |= temp_Word;

        lcshbar &= 0x7FFFFFFF; 
    }
    else
    {
        /*
        34-bit addressing:
         -                         reserved      lcshbar[ 0:31]
         -                         reserved      lcsbar [    0]
         - xamsbs          [ 0: 1] compared with lcsbar [ 1: 2]
         - address         [ 0:28] compared with lcsbar [ 3:31]
        */  
        temp_Word = (buf_Entry->trx_Info.address >> 3) & 0x1FFFFFFF;
        ls_Word   = ((buf_Entry->trx_Info.xamsbs & 0x3) << 29)  & 0x60000000;
        ls_Word  |= temp_Word;

        ms_Word = 0;
        lcshbar = 0;
        lcsbar &= 0x7FFFFFFF;
    }

    /* check if the IO transaction shall go to the local configuration space */


    if( ~0x0 - lcsbar >= (handle->init_Params.config_Space_Size >> 3) )
    {

        if ( ms_Word == lcshbar && 
        ( ls_Word >= lcsbar && ls_Word < lcsbar + (handle->init_Params.config_Space_Size >> 3) ) )
            return RIO_TRUE;
        else
            return RIO_FALSE;
    }
    else
    {
        if ( ( ms_Word == lcshbar && ls_Word >= lcsbar ) ||
            ( ms_Word == lcshbar + 1 && ls_Word < ((handle->init_Params.config_Space_Size >> 3) - (~0x0 - lcsbar)))) 
            return RIO_TRUE;
        else
            return RIO_FALSE;
    }

    return RIO_FALSE;

}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Memory_Request
 *
 * Description:  function adds a request for transaction identified by the
 *               buffer tag into the memory queue
 *               
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Memory_Request(
    RIO_LL_DS_T           *handle, 
    RIO_LL_BUF_TAG_T      tag,
    RIO_MEMORY_REQ_TYPE_T req_Type
)
{
    RIO_LL_QUEUE_T  *mem_Head;
    RIO_LL_QUEUE_T  *mem_Pool, *element;
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle); 
#endif

    /* check if there is memory in PE */
    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
    {
        /* issue warning message */
        RIO_LL_Message( handle, RIO_LL_WARN_NO_MEMORY );

        /* return error code */
        return RIO_ERROR;
    }

    /* get element from the memory queue pool */
    mem_Head = handle->target_Queues.memory_Queue_Head;
    mem_Pool = handle->target_Queues.memory_Queue_Pool;
    element = get_Queue_Element_From_Pool( &mem_Pool );

    /* set queue element state and buffer tag */
    element->current_State = RIO_LL_WAIT_FOR_SERVICE;
    element->buf_Tag = tag;
            
    /* find appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, tag, "kernel memory request function");
        return RIO_ERROR;
    }

    /* set necessary buffer fields related to memory request */
    buf_Entry->trx_Info.mem_Type = req_Type;

    /* set queue element priority */
    element->prio = buf_Entry->trx_Info.prio;

    /* insert element into the end of the memeory requests queue */
    insert_Element_To_Queue_End( element, &mem_Head );

    handle->target_Queues.memory_Queue_Head = mem_Head;
    handle->target_Queues.memory_Queue_Pool = mem_Pool;    

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Snoop_Request
 *
 * Description:  function adds a request for transaction identified by the
 *               buffer tag into the snoop queue
 *               
 *
 * Returns:      RIO_OK or RIO_ERROR (RIO_ERROR also in case of local request)
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Snoop_Request(
    RIO_LL_DS_T       *handle, 
    RIO_LL_BUF_TAG_T  tag,
    RIO_LOCAL_TTYPE_T lttype,
    RIO_SNOOP_TTYPE_T ttype
)
{
    RIO_LL_QUEUE_T  *snoop_Head;
    RIO_LL_QUEUE_T  *snoop_Pool, *element;
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle); 
#endif

    /* check if there is processor in PE */
    if ( !RIO_LL_Kernel_Have_Processor( handle ) )
    {
        /* issue warning message */
        RIO_LL_Message( handle, RIO_LL_WARN_NO_PROCESSOR );

        /* return error code */
        return RIO_ERROR;
    }

    /* get element from the snoop queue pool */
    snoop_Head = handle->target_Queues.snoop_Queue_Head;
    snoop_Pool = handle->target_Queues.snoop_Queue_Pool;
    element = get_Queue_Element_From_Pool( &snoop_Pool );

    /* set element's state and buffer tag */
    element->current_State = RIO_LL_WAIT_FOR_SERVICE;
    element->buf_Tag = tag;
            
    /* find appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, tag, "kernel snoop request function");
        return RIO_ERROR;
    }

    /* fill in buffer entries related to the snoop request */
    buf_Entry->trx_Info.snoop_LTType = lttype;
    buf_Entry->trx_Info.snoop_TType = ttype;

    /* set queue element's priority */
    element->prio = buf_Entry->trx_Info.prio;

    /* insert element into the end of the snoop requests queue */
    insert_Element_To_Queue_End( element, &snoop_Head );

    handle->target_Queues.snoop_Queue_Head = snoop_Head;
    handle->target_Queues.snoop_Queue_Pool = snoop_Pool;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Merge_Snoop_Data
 *
 * Description:  function merges data in the snoop buffer with the data in 
 *               in the transaction buffer. 
 *               
 *
 * Returns:      RIO_OK or RIO_ERROR 
 *
 * Notes:        This function is to be used only in case of the write 
 *               requests.
 *
 **************************************************************************/
int RIO_LL_Kernel_Merge_Snoop_Data(  
    RIO_LL_DS_T      *handle,         
    RIO_LL_BUF_TAG_T buf_Tag          
)
{
    RIO_LL_BUFFER_T *buf_Entry; /* buffer entry pointer */
    RIO_UCHAR_T     offset;     /* offset of the data   */
    RIO_ADDRESS_T   address;    /* address of the snoop area start */
    RIO_WORD_T      mask = 0x00000000; /* mask for the data copying */
    int             i;

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel merge snoop data function");
        return RIO_ERROR;
    }
   
    /* do address aligment */
    if ( handle->inst_Params.coh_Granule_Size_32)
        address = buf_Entry->trx_Info.address & RIO_LL_ADDRESS_GRAN_32_ALIGN;    
    else
        address = buf_Entry->trx_Info.address & RIO_LL_ADDRESS_GRAN_64_ALIGN;    

    /* do offset calculation */
    offset = (RIO_UCHAR_T)(( buf_Entry->trx_Info.address - address ) / RIO_LL_DW_BYTE_SIZE);

    /* copy data */

    if ( buf_Entry->trx_Info.dw_Size != 1 || 
        ( buf_Entry->trx_Info.dw_Size == 1 && buf_Entry->trx_Info.byte_Num == RIO_LL_DW_BYTE_SIZE ) )
    /* transaction requires whole DW memory access */
    {
        for ( i = 0; i < buf_Entry->trx_Info.dw_Size; i++ )
            buf_Entry->trx_Info.snoop_Data[i + offset] = buf_Entry->trx_Info.data[i];
    }
    else
    /* transaction requires sub doubleword memory access */
    {
        /* copy most significant word of the data */
        if ( buf_Entry->trx_Info.offset + buf_Entry->trx_Info.byte_Num <= RIO_LL_WORD_BYTE_SIZE )
        {
            for ( i = 0; i < buf_Entry->trx_Info.byte_Num; i++ )
            {
                mask = mask >> RIO_LL_BYTE_BIT_SIZE;
                mask = mask | 0xFF000000;    /* set most significant byte bits */        
            }

            mask = mask >> buf_Entry->trx_Info.offset * RIO_LL_BYTE_BIT_SIZE;

            buf_Entry->trx_Info.snoop_Data[offset].ms_Word = ( buf_Entry->trx_Info.snoop_Data[offset].ms_Word & (~mask) ) |
                ( buf_Entry->trx_Info.data[0].ms_Word & mask );
        }

        /* copy least significant word of the data */
        if ( buf_Entry->trx_Info.offset + buf_Entry->trx_Info.byte_Num > RIO_LL_WORD_BYTE_SIZE )
        {
            for ( i = 0; i < buf_Entry->trx_Info.byte_Num; i++ )
            {
                mask = mask >> RIO_LL_BYTE_BIT_SIZE;
                mask = mask | 0xFF000000;    /* set most significant byte bits */        
            }

            mask = mask >> ( buf_Entry->trx_Info.offset - RIO_LL_WORD_BYTE_SIZE ) * RIO_LL_BYTE_BIT_SIZE;

            buf_Entry->trx_Info.snoop_Data[offset].ls_Word = ( buf_Entry->trx_Info.snoop_Data[offset].ls_Word & (~mask) ) |
                ( buf_Entry->trx_Info.data[0].ls_Word & mask );
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer
 *
 * Description:  function places snoop data from the special snoop buffer 
 *               into usual transaction data buffer accordingly to 
 *               transaction parameters.
 *               
 *
 * Returns:      RIO_OK or RIO_ERROR 
 *
 * Notes:        wrapped flag defines if it is needed to wrap the data 
 *               around cache granule boundary during writ into the 
 *               transaction buffer. It is only needed in case of the GSM
 *               request.
 *
 **************************************************************************/
int RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(
    RIO_LL_DS_T      *handle,         
    RIO_LL_BUF_TAG_T buf_Tag,
    RIO_BOOL_T       wrapped
)
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_UCHAR_T     offset;
    RIO_ADDRESS_T   address;
    int             i;

#ifdef _DEBUG
    assert( handle );
#endif

    /* find appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel place snoop data to buffer function");
        return RIO_ERROR;
    }

    /* do address aligment */
    if ( handle->inst_Params.coh_Granule_Size_32)
        address = buf_Entry->trx_Info.address & RIO_LL_ADDRESS_GRAN_32_ALIGN;    
    else
        address = buf_Entry->trx_Info.address & RIO_LL_ADDRESS_GRAN_64_ALIGN;    

    /* do offset calculation */
    offset = (RIO_UCHAR_T)(( buf_Entry->trx_Info.address - address ) / RIO_LL_DW_BYTE_SIZE);

    /* copy data */
    if ( wrapped )
    {
        /* if it is wrapped operation we shall copy whole snoop buffer data */
        for ( i = 0; i < buf_Entry->trx_Info.dw_Size - offset; i++ )
            buf_Entry->trx_Info.data[i] = buf_Entry->trx_Info.snoop_Data[i + offset];

        for ( i = 0; i < offset; i++ )
            buf_Entry->trx_Info.data[buf_Entry->trx_Info.dw_Size - offset + i] = buf_Entry->trx_Info.snoop_Data[i];
    } 
    else
    {
        /* if it is nonwrapped operation only needed data shall be copied */
        for ( i = 0; i < buf_Entry->trx_Info.dw_Size; i++ )
            buf_Entry->trx_Info.data[i] = buf_Entry->trx_Info.snoop_Data[i + offset];
    }

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Send_Responce
 *
 * Description:  function adds a request for transaction identified by the
 *               buffer tag into the send responce queue
 *               
 *
 * Returns:      RIO_OK or RIO_ERROR 
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Send_Responce(
    RIO_LL_DS_T *handle, 
    RIO_LL_BUF_TAG_T tag,
    RIO_UCHAR_T ftype, 
    RIO_UCHAR_T transaction, 
    RIO_UCHAR_T status
)
{
    RIO_LL_QUEUE_T  *resp_Head;
    RIO_LL_QUEUE_T  *resp_Pool, *element;
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle); 
#endif

    /* get element from the responces queue pool */
    resp_Head = handle->target_Queues.rio_Resp_Queue_Head;
    resp_Pool = handle->target_Queues.rio_Resp_Queue_Pool;
    element = get_Queue_Element_From_Pool( &resp_Pool );

    /* set element's state and buffer tag */
    element->current_State = RIO_LL_WAIT_FOR_SERVICE;
    element->buf_Tag = tag;
            
    /* find appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, tag, "kernel send response function");
        return RIO_ERROR;
    }

    /* set neseccary buffer fields related to responce */
    buf_Entry->trx_Info.resp_FType       = ftype;
    buf_Entry->trx_Info.resp_Transaction = transaction;
    buf_Entry->trx_Info.resp_Status      = status;

    /* set element's priority */
    if ( buf_Entry->trx_Info.prio < RIO_MAX_RESPONSE_PRIORITY )
        element->prio = buf_Entry->trx_Info.prio + 1;
    else
        element->prio = buf_Entry->trx_Info.prio;

    /* insert element into the responce queue accordingly to the elements priority */
    if ( handle->init_Params.pass_By_Prio == RIO_TRUE )
        insert_Element_To_Queue_Prio( element, &resp_Head );
    else
        insert_Element_To_Queue_End( element, &resp_Head );

    handle->target_Queues.rio_Resp_Queue_Head = resp_Head;
    handle->target_Queues.rio_Resp_Queue_Pool = resp_Pool;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Send_Request
 *
 * Description:  function adds a request for transaction identified by the
 *               buffer tag into the send request queue
 *               
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Send_Request(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T tag)
{
    RIO_LL_QUEUE_T  *req_Head;
    RIO_LL_QUEUE_T  *req_Pool, *element;
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle); 
#endif

    /* get element from the requests queue pool */
    req_Head = handle->target_Queues.rio_Req_Queue_Head;
    req_Pool = handle->target_Queues.rio_Req_Queue_Pool;
    element = get_Queue_Element_From_Pool( &req_Pool );

    /* set element's state and associated buffer tag */
    element->current_State = RIO_LL_WAIT_FOR_SERVICE;
    element->buf_Tag = tag;
            
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, tag, "kernel send request function");
        return RIO_ERROR;
    }

    /* set element's priority */
    element->prio = buf_Entry->trx_Info.prio;

    /* insert element into the request queue accordingly to its priority */
    if ( handle->init_Params.pass_By_Prio == RIO_TRUE )
        insert_Element_To_Queue_Prio( element, &req_Head );
    else
        insert_Element_To_Queue_End( element, &req_Head );

    handle->target_Queues.rio_Req_Queue_Head = req_Head;
    handle->target_Queues.rio_Req_Queue_Pool = req_Pool;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Set_Trx_State
 *
 * Description:  function sets given state for the transaction identified 
 *               by the buffer tag buf_Tag.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Set_Trx_State( 
    RIO_LL_DS_T        *handle,
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state 
)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle); 
#endif

    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel set transaction state function");
        return RIO_ERROR;
    }

    /* set state */
    buf_Entry->trx_Info.state = state;

    return RIO_OK;
}

/* below are kernel functions for the GSM transactions servicing */

/***************************************************************************
 * Function :    RIO_LL_Kernel_Directory_State
 *
 * Description:  function returns current memory directory state for the 
 *               address supplied in the transaction buffer.
 *
 * Returns:      directory state accordingly to the RIO_DIRECFTORY_STATE_T
 *               enumeration values.
 *
 * Notes:
 *
 **************************************************************************/
RIO_DIRECTORY_STATE_T RIO_LL_Kernel_Directory_State(
    RIO_LL_DS_T     *handle,
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;   /* buffer entry for the transaction       */
    RIO_SH_MASK_T   sharing_Mask; /* sharing mask from the memory directory */
    unsigned int    result;

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    /* set semaphor state */    
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    result = handle->callback_Tray.rio_Read_Dir(
        handle->callback_Tray.directory_Context, 
        buf_Entry->trx_Info.address, 
        buf_Entry->trx_Info.extended_Address, 
        buf_Entry->trx_Info.xamsbs, 
        &sharing_Mask
    ); 
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* if there was incorrect address return corresponding value */
    if ( result != RIO_OK )
        return RIO_LL_ADDR_OUT_OF_RANGE;

    /* if this cache line shared locally only the value of the memory directory equal to 0 */
    if ( sharing_Mask == RIO_LL_LOCAL_SHARED ) 
        return RIO_LL_LOCAL_SHARED;

    /* if this cache line locally modified the value of the memory directory equal to 1 */
    if ( sharing_Mask == RIO_LL_LOCAL_MODIFIED )
        return RIO_LL_LOCAL_MODIFIED;

    /* if there are remote sharers for this cacheline the value of the memory */
    /*  directory shall be power of 2                                         */
    if ( sharing_Mask % 2 == 0 )
        return RIO_LL_SHARED;

    /* if the cache line remote modified teh value of the memory directory shall be */
    /* equal to the power of two plus one                                           */
    if ( sharing_Mask % 2 == RIO_LL_LOCAL_MODIFIED )
        return RIO_LL_REMOTE_MODIFIED;

    return RIO_LL_ADDR_OUT_OF_RANGE;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Set_Received_Done_Message
 *
 * Description:  function sets Received_Done_Flag in the transaction
 *               buffer entry.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Set_Received_Done_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry; /* buffer entry for the transaction */

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    buf_Entry->trx_Info.gsm_Received_Done_Flag = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Set_Received_Data_Only_Message
 *
 * Description:  function sets Received_Data_Only_Flag in the transaction
 *               buffer entry.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Set_Received_Data_Only_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry; /* buffer entry for the transaction */

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    buf_Entry->trx_Info.gsm_Received_Data_Only_Flag = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Received_Done_Message
 *
 * Description:  function returns vaue of the  Received_Done_Flag 
 *               from the transaction buffer entry.
 *
 * Returns:      value of the flag
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Received_Done_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry; /* buffer entry for the transaction */

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    return buf_Entry->trx_Info.gsm_Received_Done_Flag;
}


/***************************************************************************
 * Function :    RIO_LL_Kernel_Received_Data_Only_Message
 *
 * Description:  function returns vaue of the  Received_Data_Only_Flag 
 *               from the transaction buffer entry.
 *
 * Returns:      value of the flag
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Received_Data_Only_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_BUF_TAG_T buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry; /* buffer entry for the transaction */

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    return buf_Entry->trx_Info.gsm_Received_Data_Only_Flag;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Send_Altered_Request
 *
 * Description:  function composes secondary or intervention GSM request 
 *               accordingly to buffer entry contents for the transaction
 *               and function parameters.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Send_Altered_Request(
    RIO_LL_DS_T         *handle,
    RIO_LL_BUF_TAG_T    buf_Tag,
    RIO_DEST_MASK_T     dest,
    RIO_PL_GSM_TTYPE_T  ttype,
    RIO_BOOL_T          is_SECID_EQ_MYID  /* else secid is determined from external request packet */
)
{
    RIO_LL_QUEUE_T  *req_Head;
    RIO_LL_QUEUE_T  *req_Pool, *element;
    RIO_LL_BUFFER_T *buf_Entry;   /* buffer entry for the transaction       */
    RIO_GSM_REQ_T   *buf_Req;
    RIO_SH_MASK_T   sharing_Mask;
    RIO_UCHAR_T      granule_Size;
    unsigned int    j;
    int             i;

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    /* find out cache coherency granule size */
    if ( handle->inst_Params.coh_Granule_Size_32 )
        granule_Size = 32; /* thirty two byte cache granule size */
    else
        granule_Size = 64; /* sixty four byte cache granule size */

    /* find out the targets for the request */
    switch ( dest )
    {
        /* send request to the PE owning cache line */
        case RIO_MASK_ID:
        {
            /* read directory state */
            /* set semaphor state */    
            add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* read directory state */
            handle->callback_Tray.rio_Read_Dir(
                handle->callback_Tray.directory_Context, 
                buf_Entry->trx_Info.address, 
                buf_Entry->trx_Info.extended_Address, 
                buf_Entry->trx_Info.xamsbs, 
                &sharing_Mask
            ); 

            /* unset semaphor state */
            remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* check that the cache line is remotely modified */
            if ( ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED) != RIO_LL_LOCAL_MODIFIED ) ||
                ( sharing_Mask == RIO_LL_LOCAL_MODIFIED ) )
                return RIO_ERROR;

            /* find out the number of the modifier in the remote devices IDs array */
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )
            {
                sharing_Mask = sharing_Mask >> 1;
                if ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED ) == RIO_LL_LOCAL_MODIFIED )
                    break;
            }

            buf_Entry->trx_Info.tr_Info[0] = handle->init_Params.remote_Dev_ID[i];
            buf_Entry->trx_Info.multicast_Size = 1;

            break;
        }

        /* send request to all sharers of the cache line excluding local PE */
        case RIO_MASK_WO_MY_ID:
        {
            /* read directory state */
            /* set semaphor state */    
            add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* read directory state */
            handle->callback_Tray.rio_Read_Dir(
                handle->callback_Tray.directory_Context, 
                buf_Entry->trx_Info.address, 
                buf_Entry->trx_Info.extended_Address, 
                buf_Entry->trx_Info.xamsbs, 
                &sharing_Mask
            ); 

            /* unset semaphor state */
            remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* check that the cache line is remotely shared */
            if ( ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED) == RIO_LL_LOCAL_MODIFIED ) ||
                ( sharing_Mask == RIO_LL_LOCAL_SHARED ) )
                return RIO_ERROR;

            /* find out all the sharers of the cache line, their quantity and copy */
            /* their IDs into the tr_Info array in the transaction buffer          */
            j = 0;
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )
            {
                sharing_Mask = sharing_Mask >> 1;
                if ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED ) == RIO_LL_LOCAL_MODIFIED )
                {
                    buf_Entry->trx_Info.tr_Info[j] = handle->init_Params.remote_Dev_ID[i];
                    j++;
                }
            }

            buf_Entry->trx_Info.multicast_Size = j;
            
            break;
        }

        /* send request to all sharers but requestor and local PE */
        case RIO_MASK_WO_RECEIVED_SRCID:
        {
            /* read directory state */
            /* set semaphor state */    
            add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* read directory state */
            handle->callback_Tray.rio_Read_Dir(
                handle->callback_Tray.directory_Context, 
                buf_Entry->trx_Info.address, 
                buf_Entry->trx_Info.extended_Address, 
                buf_Entry->trx_Info.xamsbs, 
                &sharing_Mask
            ); 

            /* unset semaphor state */
            remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* check that the cache line is remotely shared */
            if ( ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED) == RIO_LL_LOCAL_MODIFIED ) ||
                ( sharing_Mask == RIO_LL_LOCAL_SHARED ) )
                return RIO_ERROR;

            /* find out all the sharers of the cache line, their quantity and copy */
            /* their IDs into the tr_Info array in the transaction buffer          */
            j = 0;
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )
            {
                sharing_Mask = sharing_Mask >> 1;
                if ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED ) == RIO_LL_LOCAL_MODIFIED &&
                    handle->init_Params.remote_Dev_ID[i] != buf_Entry->trx_Info.src_ID )
                {
                    buf_Entry->trx_Info.tr_Info[j] = handle->init_Params.remote_Dev_ID[i];
                    j++;
                }
            }

            buf_Entry->trx_Info.multicast_Size = j;
            
            break;
        }

        /* send to all coherence domain participants excluding received src_id */
        /* this is necessary for servicing of remotely requested IKILL_HOME */        
        case RIO_MASK_PARTICIPANT_WO_RECEIVED_SRCID:
        {
            /* create multicast array not including received src_id */
            j = 0;
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )
            {
                /* check that we don't copy received_src_id */
                if ( handle->init_Params.remote_Dev_ID[i] != buf_Entry->trx_Info.src_ID )
                {
                    buf_Entry->trx_Info.tr_Info[j] = handle->init_Params.remote_Dev_ID[i];
                    j++;
                }
            }

            buf_Entry->trx_Info.multicast_Size = j;
            
            break;
        }
        

        /* send request to the originator of the response */
        case RIO_MASK_RECEIVED_SRCID:
        {
            buf_Entry->trx_Info.tr_Info[0] = buf_Entry->trx_Info.resp_Src_ID;
            buf_Entry->trx_Info.multicast_Size = 1;

            break;
        }

        /* send request to the all participants of the cache coherence domain */
        case RIO_MASK_PARTICIPANT_ID:
        {
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )
                buf_Entry->trx_Info.tr_Info[i] = handle->init_Params.remote_Dev_ID[i];
            buf_Entry->trx_Info.multicast_Size = handle->inst_Params.coh_Domain_Size - 1;
    
            break;
        }
    }

    /* if function invoked due to the remote request servicing there is */
    /* no copy of the request structure in buffer and pointer to it is  */
    /* NULL. If so request structure needs to be created                */
    if ( buf_Entry->trx_Info.req == NULL )
    {
        /* allocate memory for the request structure */
        buf_Req = ( RIO_GSM_REQ_T* )malloc( sizeof( RIO_GSM_REQ_T ) );
        if ( buf_Req == NULL )
        {
            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, sizeof( RIO_GSM_REQ_T), "GSM request structure");
            return RIO_ERROR;
        }

        /* fill in request structure fields */
        buf_Req->prio              = buf_Entry->trx_Info.prio;
        buf_Req->address           = buf_Entry->trx_Info.address;
        buf_Req->extended_Address  = buf_Entry->trx_Info.extended_Address;
        buf_Req->xamsbs            = buf_Entry->trx_Info.xamsbs;
        buf_Req->transp_Type       = buf_Entry->trx_Info.resp_Transp_Type;
        buf_Entry->trx_Info.req    = buf_Req;
    }

    /* data sizes for the secondary or intervention request may need to be corrected */

    buf_Req = ( RIO_GSM_REQ_T *)buf_Entry->trx_Info.req;

    if ( ( buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_FLUSH_WITH_DATA ||
        buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_FLUSH_WO_DATA) && 
        ttype == RIO_PL_GSM_READ_TO_OWN_OWNER )
    /* for flushes data sizes for the intervention request are different     */
    /* than for original request because of flush may has subdw data payload */
    /* but for intervention request data size shall always be cache granule  */
    /* sized                                                                 */
    {
        buf_Req->dw_Size  = granule_Size / RIO_LL_DW_BYTE_SIZE;
        buf_Req->offset   = 0;
        buf_Req->byte_Num = 0;
        buf_Req->data     = NULL; /* secondary or intervention requests */
                                  /* never contains data payload        */
    }
    else
    /* for all requests but flushes data sizes for secondary or intervention */
    /* request are the same with the original request                        */
    {
        buf_Req->dw_Size  = buf_Entry->trx_Info.dw_Size;
        buf_Req->offset   = buf_Entry->trx_Info.offset;
        buf_Req->byte_Num = buf_Entry->trx_Info.byte_Num;
        buf_Req->data     = NULL; /* secondary or intervention requests */
                                  /* never contains data payload        */
    }

    /* fill in ttype field in the buffer entry */
    buf_Entry->trx_Info.gsm_TType = ttype;

    /* fill in secondary TID buffer entry */
    buf_Entry->trx_Info.sec_TID   = buf_Entry->trx_Info.resp_Target_TID;

    /* fill in sec_ID field */
    if ( is_SECID_EQ_MYID )
        buf_Entry->trx_Info.sec_ID = (RIO_UCHAR_T)get_Config_Reg_Field(handle, RIO_LL_MY_DEVICE_ID);
    else 
        buf_Entry->trx_Info.sec_ID = buf_Entry->trx_Info.src_ID;

    /* set kernel flag */
    buf_Entry->kernel_Info.is_Local    = RIO_FALSE;
    buf_Entry->trx_Info.is_Outstanding = RIO_TRUE;

    /* add request into the requests queue */

    /* get element from the requests queue pool */
    req_Head = handle->target_Queues.rio_Req_Queue_Head;
    req_Pool = handle->target_Queues.rio_Req_Queue_Pool;
    element  = get_Queue_Element_From_Pool( &req_Pool );

    /* set element's state and associated buffer tag */
    element->current_State = RIO_LL_WAIT_FOR_SERVICE;
    element->buf_Tag = buf_Tag;
            
    /* set element's priority */
    element->prio = buf_Entry->trx_Info.prio;

    /* insert element into the request queue accordingly to its priority */
    if ( handle->init_Params.pass_By_Prio == RIO_TRUE )
        insert_Element_To_Queue_Prio( element, &req_Head );
    else
        insert_Element_To_Queue_End( element, &req_Head );

    handle->target_Queues.rio_Req_Queue_Head = req_Head;
    handle->target_Queues.rio_Req_Queue_Pool = req_Pool;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Update_Directory_State
 *
 * Description:  function updates memory directory state for the address
 *               supplied in the request accordingly to the value supplied
 *               in the parameter update
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_Update_Directory_State(
    RIO_LL_DS_T             *handle, 
    RIO_LL_BUF_TAG_T        buf_Tag, 
    RIO_DIRECTORY_UPDATE_T  update
)
{
    RIO_LL_BUFFER_T *buf_Entry;            /* buffer entry for the transaction       */
    RIO_SH_MASK_T   sharing_Mask = 0;      /* sharing mask from the memory directory */
    RIO_SH_MASK_T   temp_Sharing_Mask = 0; /* temporary variable for the sharing mask storing */
    int             i;

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    /* calculate the value of the sharing_Mask */
    switch ( update )
    {
        /* set as shared by the local processor */
        case RIO_SET_SHARED_MY_ID:
            sharing_Mask = RIO_SET_SHARED_MY_ID;
        break;

        /* set as modified by the local processor */
        case RIO_LOCAL_MODIFIED:
            sharing_Mask = RIO_LOCAL_MODIFIED;
        break;

        /* add requestor ID into the sharers list */
        case RIO_ADD_SHARED_REQUESTOR_ID:
        {
            /* it is needed to read current state of the memory directory */

            /* set semaphor state */    
            add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* read directory state */
            handle->callback_Tray.rio_Read_Dir(
                handle->callback_Tray.directory_Context, 
                buf_Entry->trx_Info.address, 
                buf_Entry->trx_Info.extended_Address, 
                buf_Entry->trx_Info.xamsbs, 
                &temp_Sharing_Mask
            ); 
            
            /* unset semaphor state */
            remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

            /* compose sharing mask */
            sharing_Mask = RIO_LL_LOCAL_MODIFIED;
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )            
            {
                sharing_Mask = sharing_Mask << 1;
                if ( handle->init_Params.remote_Dev_ID[i] == buf_Entry->trx_Info.src_ID )
                    break;
            }

            if ( i == handle->inst_Params.coh_Domain_Size - 1)
            /* the requestor is not from our cache coherency domain */
                return RIO_ERROR;

            sharing_Mask = sharing_Mask | temp_Sharing_Mask;

            break;
        }

        /* set as shared by the requestor and local processor */
        case RIO_SET_SHARED_REQUESTOR_ID:
        {
            /* compose sharing mask */
            sharing_Mask = RIO_LL_LOCAL_MODIFIED;
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )            
            {
                sharing_Mask = sharing_Mask << 1;
                if ( handle->init_Params.remote_Dev_ID[i] == buf_Entry->trx_Info.src_ID )
                    break;
            }

            if ( i == handle->inst_Params.coh_Domain_Size - 1)
            /* the requestor is not from our cache coherency domain */
                return RIO_ERROR;

            break;
        }

        /* set as modified by the remote processor */
        case RIO_REMOTE_MODIFIED:
        {
            /* compose sharing mask */
            sharing_Mask = RIO_LL_LOCAL_MODIFIED;
            for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )            
            {
                sharing_Mask = sharing_Mask << 1;
                if ( handle->init_Params.remote_Dev_ID[i] == buf_Entry->trx_Info.src_ID )
                    break;
            }

            if ( i == handle->inst_Params.coh_Domain_Size - 1 )
            /* the requestor is not from our cache coherency domain */
                return RIO_ERROR;

            sharing_Mask = sharing_Mask | RIO_LL_LOCAL_MODIFIED;

            break;
        }
        
    }

    /* set semaphor state */    
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* write directory state */
    handle->callback_Tray.rio_Write_Dir(
        handle->callback_Tray.directory_Context, 
        buf_Entry->trx_Info.address, 
        buf_Entry->trx_Info.extended_Address, 
        buf_Entry->trx_Info.xamsbs, 
        sharing_Mask
    ); 
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_LL_Kernel_Is_Intervention_Case
 *
 * Description:  function returns RIO_TRUE if the requestor doesn't own the 
 *               cache line
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Is_Intervention_Case(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_SH_MASK_T   sharing_Mask;
    int             i;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    /* read directory state */
    /* set semaphor state */    
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* read directory state */
    handle->callback_Tray.rio_Read_Dir(
        handle->callback_Tray.directory_Context, 
        buf_Entry->trx_Info.address, 
        buf_Entry->trx_Info.extended_Address, 
        buf_Entry->trx_Info.xamsbs, 
        &sharing_Mask
    ); 

    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* if the cache line is not modified at all return RIO_FALSE */
    if ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED ) != RIO_LL_LOCAL_MODIFIED )
        return RIO_FALSE;

    /* if the cache line is modified by the local processor return RIO_FALSE */
    if ( sharing_Mask == RIO_LL_LOCAL_MODIFIED )
        return RIO_FALSE;

    /* find out the cache line owner device ID number in the remote device IDs array */
    for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )
    {
        sharing_Mask = sharing_Mask >> 1;
        if ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED ) == RIO_LL_LOCAL_MODIFIED )
            break;
    }
    
    /* if the found owner device ID isn't equal to the requestor device ID return RIO_TRUE */
    if ( handle->init_Params.remote_Dev_ID[i] != buf_Entry->trx_Info.src_ID )
        return RIO_TRUE;
    else
        return RIO_FALSE;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Is_RCVSRCID_EQ_RCVSECID
 *
 * Description:  function returns RIO_TRUE if the requestor device ID is 
 *               equal to the secondary ID
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Is_RCVSRCID_EQ_RCVSECID(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    if ( buf_Entry->trx_Info.src_ID == buf_Entry->trx_Info.sec_ID )
        return RIO_TRUE;
    else
        return RIO_FALSE;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Is_Requestor_The_Only_Remote_Sharer
 *
 * Description:  function returns RIO_TRUE if the requestor is the only 
 *               remote sharer of the cache line
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/
RIO_BOOL_T RIO_LL_Kernel_Is_Requestor_The_Only_Remote_Sharer(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_SH_MASK_T   sharing_Mask;
    int             i;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    /* read directory state */
    /* set semaphor state */    
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* read directory state */
    handle->callback_Tray.rio_Read_Dir(
        handle->callback_Tray.directory_Context, 
        buf_Entry->trx_Info.address, 
        buf_Entry->trx_Info.extended_Address, 
        buf_Entry->trx_Info.xamsbs, 
        &sharing_Mask
    ); 

    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* if cache line is modified return RIO_FALSE */
    if ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED ) == RIO_LL_LOCAL_MODIFIED )
        return RIO_FALSE;

    /* if the cache line is shared by the local processor only return RIO_FALSE */
    if ( sharing_Mask == RIO_LL_LOCAL_SHARED )
        return RIO_FALSE;

    /* find out the first sharer device ID number in the remote device IDs array */
    for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )
    {
        sharing_Mask = sharing_Mask >> 1;
        if ( ( sharing_Mask & RIO_LL_LOCAL_MODIFIED ) == RIO_LL_LOCAL_MODIFIED )
            break;
    }
    
    /* if the found sharer is not the only sharer return RIO_FALSE */
    if ( sharing_Mask != RIO_LL_LOCAL_MODIFIED )
        return RIO_FALSE;

    /* if the only sharer device ID is equal to the requestor device ID return RIO_TRUE */
    if ( handle->init_Params.remote_Dev_ID[i] == buf_Entry->trx_Info.src_ID )
        return RIO_TRUE;
    else
        return RIO_FALSE;
}

/***************************************************************************
 * Function :    RIO_LL_Kernel_Coh_Domain_Size
 *
 * Description:  function is used by IKILL_HOME transaction handlers
 *               (both internal and remote) to see whether they should
 *               send IKILL_SHARERS or not
 *
 *
 * Returns:      value of coh_Domain_Size instantiation parameter
 *
 * Notes:
 *
 **************************************************************************/

int RIO_LL_Kernel_Coh_Domain_Size(
    RIO_LL_DS_T      *handle
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    return handle->inst_Params.coh_Domain_Size;
}


/***************************************************************************
 * Function :    RIO_LL_Kernel_Received_Data
 *
 * Description:  function returns RIO_TRUE if there is data received
 *               (with local request or response or remote request)
 *               to write to memory; is used only by FLUSH handlers
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:
 *
 **************************************************************************/

RIO_BOOL_T RIO_LL_Kernel_Received_Data(
    RIO_LL_DS_T      *handle,
    RIO_LL_BUF_TAG_T  buf_Tag
)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* find buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );    
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_ERROR;
    }

    return ( buf_Entry->trx_Info.dw_Size != 0 );
}



/*****************************************************************************
 *
 * 
 *  STATIC functions (kernel helpers) are defined below
 *
 *
 */

 

/***************************************************************************
 * Function :    allocate_Buffer_Entry
 *
 * Description:  tries to allocate a buffer for requested transaction
 *               This function should implement buffer allocation policy
 *               based on the requested transaction priority
 *               (the policy may include reserving buffer entries for 
 *               high-priority local requests, or can be user-customizable?)
 *
 * Returns:      RIO_ERROR, if no buffer is available for transaction of 
 *               specified priority, or buffet tag if ok 
 *
 * Notes:
 *
 **************************************************************************/

static RIO_LL_BUF_TAG_T allocate_Buffer_Entry( RIO_LL_DS_T *handle, RIO_UCHAR_T prio, RIO_BOOL_T is_Local)
{
    RIO_LL_BUFFER_T *buffer;
    unsigned int    buf_Size;
    unsigned int    i, j;

#ifdef _DEBUG
    assert( handle );
#endif

    if (is_Local)
    {
        buffer = handle->loc_Buffer;
        buf_Size = handle->locb_Size;
    }
    else
    {
        buffer = handle->remote_Buffer;
        buf_Size = handle->remoteb_Size;
    }

    /* find free buffer entry */
    for (i = 0; (i < buf_Size) && (buffer[i].kernel_Info.valid != RIO_FALSE); i++ )
        ;

    if (i == buf_Size)
        return RIO_ERROR; /* buffer is completely full */

    if ( handle->init_Params.pass_By_Prio )
        if (buffer[i].kernel_Info.for_Prio > prio)
            return RIO_ERROR; /* no entry for transaction of given priority */

    /* mark buffer entry as allocated */
    buffer[i].kernel_Info.valid = RIO_TRUE;

    /* clean buffer entry fields */
    buffer[i].trx_Info.prio         = 0;
    buffer[i].trx_Info.user_Trx_Tag = 0;
    buffer[i].trx_Info.req_FType    = 0;
    buffer[i].trx_Info.req_TType    = 0;
    buffer[i].trx_Info.req_Type     = 0;

    if ( buffer[i].trx_Info.req != NULL )
    {
        free( buffer[i].trx_Info.req );
        buffer[i].trx_Info.req = NULL;
    }

    for ( j = 0; j < RIO_MAX_COH_DOMAIN_SIZE; j++ )
        buffer[i].trx_Info.tr_Info[j]   = 0;

    buffer[i].trx_Info.multicast_Size   = 0;
    buffer[i].trx_Info.gsm_TType        = 0;
    buffer[i].trx_Info.address          = 0;
    buffer[i].trx_Info.extended_Address = 0;
    buffer[i].trx_Info.xamsbs           = 0;
    buffer[i].trx_Info.dw_Size          = 0;
    buffer[i].trx_Info.offset           = 0;
    buffer[i].trx_Info.byte_Num         = 0;
    buffer[i].trx_Info.dw_Num           = 0;

    for ( j = 0; j < RIO_LL_MAX_PACKET_DW_SIZE; j++ )
    {
        buffer[i].trx_Info.data[j].ms_Word = 0;
        buffer[i].trx_Info.data[j].ls_Word = 0;
    }
    

    /* GDA: Bug#124 - Support for Data Streaming packet */
    buffer[i].trx_Info.cos              = 0; 
    buffer[i].trx_Info.stream_ID.src_ID = 0;
    buffer[i].trx_Info.stream_ID.flow_ID = 0;
    buffer[i].trx_Info.s_Ds             = 0;
    buffer[i].trx_Info.e_Ds             = 0;
    buffer[i].trx_Info.mtu              = 0;
    buffer[i].trx_Info.o                = 0;
    buffer[i].trx_Info.p                = 0;
    buffer[i].trx_Info.length           = 0;
    buffer[i].trx_Info.hw_Num           = 0;
    for(j=0 ; j< RIO_LL_MAX_PACKET_DS_SIZE ; j++)
        buffer[i].trx_Info.data_hw[j] = 0;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    /* GDA: Bug#125 - Support for Flow control packet */
    buffer[i].trx_Info.flow_ID         = 0;
    buffer[i].trx_Info.tgtdest_ID      = 0;
    buffer[i].trx_Info.soc             = 0;
    buffer[i].trx_Info.dest_ID         = 0;
    /* buffer[i].trx_Info.x_On_Off     = 0;*/
    /* GDA: Bug#125 - Support for Flow control packet */

    buffer[i].trx_Info.snoop_LTType        = 0;
    buffer[i].trx_Info.snoop_TType         = 0;
    buffer[i].trx_Info.mem_Type            = 0;
    buffer[i].trx_Info.sec_ID              = 0;
    buffer[i].trx_Info.sec_TID             = 0;
    buffer[i].trx_Info.mbox                = 0;
    buffer[i].trx_Info.letter              = 0;
    buffer[i].trx_Info.msgseg              = 0;
    buffer[i].trx_Info.ssize               = 0;
    buffer[i].trx_Info.msglen              = 0;
    buffer[i].trx_Info.doorbell_Info       = 0;
    buffer[i].trx_Info.conf_Offset         = 0;
    buffer[i].trx_Info.sub_Dw_Pos          = 0;
    buffer[i].trx_Info.src_ID              = 0;
    buffer[i].trx_Info.resp_Tr_Info        = 0;
    buffer[i].trx_Info.resp_Src_ID         = 0;
    buffer[i].trx_Info.resp_Target_TID     = 0;
    buffer[i].trx_Info.resp_FType          = 0;
    buffer[i].trx_Info.resp_Transaction    = 0;
    buffer[i].trx_Info.resp_Status         = 0;
    buffer[i].trx_Info.resp_DW_Size        = 0;
    buffer[i].trx_Info.get_Data_From_Snoop = 0;
    buffer[i].trx_Info.data_Offset         = 0;
    buffer[i].trx_Info.data_DW_Size        = 0;
    buffer[i].trx_Info.count_From_Snoop    = 0;      
    buffer[i].trx_Info.current_From_Snoop  = 0;

    if ( buffer[i].trx_Info.snoop_Data != NULL )
    {
        free( buffer[i].trx_Info.snoop_Data );
        buffer[i].trx_Info.snoop_Data = NULL;
    }

    if ( buffer[i].trx_Info.snoop_Mask != NULL )
    {
        free( buffer[i].trx_Info.snoop_Mask );
        buffer[i].trx_Info.snoop_Mask = NULL;
    }

    buffer[i].trx_Info.snoop_Start_Address         = 0;
    buffer[i].trx_Info.snoop_Finish_Address        = 0;
    buffer[i].trx_Info.snoop_Current_Address       = 0;
    buffer[i].trx_Info.snoop_DW_Size               = 0;
    buffer[i].trx_Info.gsm_Received_Done_Flag      = RIO_FALSE;
    buffer[i].trx_Info.gsm_Received_Data_Only_Flag = RIO_FALSE;
    buffer[i].trx_Info.is_Outstanding              = RIO_FALSE;
    buffer[i].trx_Info.state                       = 0;

    /* clean kernel info fields */
    buffer[i].kernel_Info.handler         = NULL;
    buffer[i].kernel_Info.stall_Tag_Valid = RIO_FALSE;
    buffer[i].kernel_Info.stall_Tag       = 0;
    buffer[i].kernel_Info.is_Stalled      = RIO_FALSE;
    buffer[i].kernel_Info.stall_Tag       = 0;
    
    /* return buffer tag */
    if (is_Local)
        return (RIO_LL_LOCAL_BUFFER_START + i);
    else 
        return (RIO_LL_REMOTE_BUFFER_START + i);
}

/***************************************************************************
 * Function :    deallocate_Buffer_Entry
 *
 * Description:  the function is used to resume resources assigned to 
 *               servicing the transaction. It deallocates buffer entry
 *               for the transaction and also looks in the target queues
 *               in case transaction is terminated by the kernel due 
 *               to some error
 *
 * Returns:      nothing
 *
 * Notes:
 *
 **************************************************************************/

static int deallocate_Buffer_Entry(RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert( handle );   
#endif

    /* find neseccary buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag );

    /* mark it as free */
    buf_Entry->kernel_Info.valid = RIO_FALSE;

    return 0;
}

/***************************************************************************
 * Function :    get_Buffer_Entry
 *
 * Description:  function returns pointer to the transaction info
 *               of the buffer entry with tag "tag".
 *
 * Returns:      NULL if entry with given tag wasn't found, valid pointer 
 *               overwise.
 *
 * Notes:
 *
 **************************************************************************/

static RIO_LL_BUFFER_T* get_Buffer_Entry(const RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T tag)
{
    unsigned int position;

#ifdef _DEBUG
    assert( handle );
#endif

    if (tag >= RIO_LL_REMOTE_BUFFER_START) 
    {
        position = tag - RIO_LL_REMOTE_BUFFER_START;
        if ( position >= handle->remoteb_Size )
            return NULL;
        else 
            return &(handle->remote_Buffer[position]);
    }
    else
    {
        position = tag - RIO_LL_LOCAL_BUFFER_START;
        if ( position >= handle->locb_Size )
            return NULL;
        else 
            return &(handle->loc_Buffer[position]);
    }
}

/***************************************************************************
 * Function :    service_Target_Queues
 *
 * Description:  this function is a "read-only" function of ll target queues
 *               It is called to check the status of the queues after 
 *               transaction handlers' work and issues necessary requests
 *               or events to the environment. 
 *               It is possible that function is called recursively.
 *               If we ever want to detect recursion, we can embrace the 
 *               body of this function with semaphores, or introduce 
 *               semaphores for separate queues and check it here to take
 *               appropriate actions.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int service_Target_Queues( RIO_LL_DS_T *handle )
{
    RIO_BOOL_T error_Occured = RIO_FALSE;

#ifdef _DEBUG
    assert( handle );
#endif

    /* service memory queue */
    if (service_Memory_Queue( handle ) == RIO_ERROR)
    {   
        error_Occured = RIO_TRUE;
        RIO_LL_Message( handle, RIO_LL_ERROR_QUEUE_ERROR, "memory");
    }

    /* service snoop queue */
    if (service_Snoop_Queue( handle ) == RIO_ERROR)
    {   
        error_Occured = RIO_TRUE;
        RIO_LL_Message( handle, RIO_LL_ERROR_QUEUE_ERROR, "snoop");
    }

    /* service request and responce queues */
    if (service_Request_Responce_Queues( handle ) == RIO_ERROR)
    {   
        error_Occured = RIO_TRUE;
        RIO_LL_Message( handle, RIO_LL_ERROR_QUEUE_ERROR, "request response");
    }

    if ( error_Occured)
        return RIO_ERROR;
    else
        return RIO_OK;
}


/***************************************************************************
 * Function :    service_Memory_Queue
 *
 * Description:  this function is a "read-only" function of ll target memory 
 *               queue. It is called to check the status of the memory queue. 
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
static int service_Memory_Queue( RIO_LL_DS_T *handle )
{
    RIO_LL_QUEUE_T     *head, *current; /* queue elements pointers */
    RIO_LL_BUFFER_T    *buf_Entry;
    RIO_UCHAR_T        be = 0x00;
    RIO_SNOOP_RESULT_T snoop_Result; /* result of the snoop operation */
    RIO_UCHAR_T        granule_Size; /* size of the cahche granule */
    RIO_WORD_T         gran_Align_Mask; /* mask for the granule boundary address aligment */
    RIO_ADDRESS_T      req_Address = 0;  /* starting address for the memory operation */
    RIO_WORD_T         req_DW_Size = 0;  /* size of the data for the memory request in DWs */
    RIO_UCHAR_T        mem_Piece_Number;  /* number of the memory pieces to be writen */
    RIO_UCHAR_T        mem_Piece_DW_Size; /* size in DWs of the current memory piece */
    int                i;

    /* get memory queue head */
    head = handle->target_Queues.memory_Queue_Head;

    /* if queue is empty do nothing */
    if (head == NULL)
        return RIO_OK;

    /* find out cache coherence granule size and aligment mask */
    if ( handle->inst_Params.coh_Granule_Size_32 )
    {
        granule_Size    = 32;   /* thirty two byte coherency granule size */
        gran_Align_Mask = RIO_LL_ADDRESS_GRAN_32_ALIGN;
    }
    else
    {
        granule_Size    = 64;   /* sixty four byte coherency granule size */
        gran_Align_Mask = RIO_LL_ADDRESS_GRAN_64_ALIGN;
    }

    /* find element waiting for responce from memory */
    /* if there is no such an element service first element in queue */
    /* overwise do nothing                                            */
    current = find_Queue_Element_With_State(head, RIO_LL_WAIT_FOR_RESPONCE);
    if (current == NULL)
    {
        /* get appropriate buffer entry */
        buf_Entry = get_Buffer_Entry( handle, head->buf_Tag );
        if (buf_Entry == NULL)
        {
            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, head->buf_Tag, "internal LL memory queue servicing");
            return RIO_ERROR;
        }

        /* 
         * if it is needed to make a memory access in a subdoubleword quantity
         * convert offset and byte_Num into be 
         */
        if (buf_Entry->trx_Info.dw_Size == 1)
        {
            for (i = 0; i < buf_Entry->trx_Info.byte_Num; i++)                      
            {
                be = be >> 1;
                be = be | 0x80;  /* set most significant bit */
            }
            be = be >> buf_Entry->trx_Info.offset;
        }

        /* 
         * if there was snoop request before the memory request 
         * check snoop_Mask and find out if it is needed to exchange 
         * data beetwen buffers and process memory request from the appropriate
         * buffer 
         */
        snoop_Result = RIO_SNOOP_MISS;

        if ( buf_Entry->trx_Info.snoop_Mask != NULL)
        {
            for ( i = 0; i < buf_Entry->trx_Info.snoop_DW_Size * RIO_LL_DW_BYTE_SIZE / granule_Size; i++ )
                if ( buf_Entry->trx_Info.snoop_Mask[i] )
                    snoop_Result = RIO_SNOOP_HIT;
        }

        if ( snoop_Result == RIO_SNOOP_HIT )
        /* there was snoop hit before memory operation */
        /* so it is needed to perform WRITE of the snooped data to the memory */
        {
            switch ( buf_Entry->trx_Info.mem_Type )
            {
                case RIO_MEM_WRITE:
                {
                    /* merge data */
                    RIO_LL_Kernel_Merge_Snoop_Data( handle, head->buf_Tag );

                    /* set data buffer flag */
                    buf_Entry->trx_Info.get_Data_From_Snoop = RIO_TRUE;
                    buf_Entry->trx_Info.count_From_Snoop    = 1;
                    buf_Entry->trx_Info.current_From_Snoop  = 0;

                    /* find out starting address for the memory operation */
                    if ( buf_Entry->trx_Info.snoop_Mask[0] )
                        req_Address = buf_Entry->trx_Info.address & gran_Align_Mask;
                    else
                        req_Address = buf_Entry->trx_Info.address;

                    /* find out data offset for the memory operation */
                    buf_Entry->trx_Info.data_Offset = (RIO_UCHAR_T)(( req_Address - 
                        ( buf_Entry->trx_Info.address & gran_Align_Mask ) ) / RIO_LL_DW_BYTE_SIZE);

                    /* find out data size for the memory operation */
                    req_DW_Size = buf_Entry->trx_Info.snoop_DW_Size;

                    if ( !buf_Entry->trx_Info.snoop_Mask[0] )
                        req_DW_Size -= buf_Entry->trx_Info.data_Offset;

                    if ( !buf_Entry->trx_Info.snoop_Mask[buf_Entry->trx_Info.snoop_DW_Size * 
                        RIO_LL_DW_BYTE_SIZE / granule_Size - 1] ) 
                        req_DW_Size -= ( granule_Size - ( buf_Entry->trx_Info.address + 
                            buf_Entry->trx_Info.dw_Size * RIO_LL_DW_BYTE_SIZE - 
                            ( ( buf_Entry->trx_Info.address + buf_Entry->trx_Info.dw_Size * RIO_LL_DW_BYTE_SIZE) 
                            & gran_Align_Mask ) ) ) / RIO_LL_DW_BYTE_SIZE;

                    buf_Entry->trx_Info.data_DW_Size = (RIO_UCHAR_T)req_DW_Size;
                    
                    break;
                }

                case RIO_MEM_READ:
                case RIO_MEM_WRAP_READ:
                case RIO_MEM_ATOMIC_INC:
                case RIO_MEM_ATOMIC_DEC:
                case RIO_MEM_ATOMIC_SET:
                case RIO_MEM_ATOMIC_CLEAR:
                case RIO_MEM_ATOMIC_SWAP: /* GDA: Bug#133 - Support for ttype C for type 5 packet */
                case RIO_MEM_ATOMIC_TSWAP:
                {
                    /* set data buffer flag */
                    buf_Entry->trx_Info.get_Data_From_Snoop = RIO_TRUE;

                    /* set current WRITE from snoop operation number */
                    buf_Entry->trx_Info.current_From_Snoop  = 0;

                    /* find out quantity of the WRITE from snoop operations */
                    mem_Piece_Number = 0;
                    for ( i = 0; i < buf_Entry->trx_Info.snoop_DW_Size * RIO_LL_DW_BYTE_SIZE / granule_Size; i++ )
                    {
                        if ( ( i == 0 && buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE ) ||                        
                            ( i != 0 && buf_Entry->trx_Info.snoop_Mask[i - 1] == RIO_FALSE && 
                            buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE ) )
                            mem_Piece_Number++;
                    }

                    buf_Entry->trx_Info.count_From_Snoop = mem_Piece_Number;

                    /* find out size in DWs of the first WRITE from snoop memory piece */
                    mem_Piece_DW_Size = 0;
                    for ( i = 0; i < buf_Entry->trx_Info.snoop_DW_Size * RIO_LL_DW_BYTE_SIZE / granule_Size; i++ )
                    {
                        /* find out start of the memory piece */
                        /* also find out start address for this memory piece */
                        if ( ( i == 0 && buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE ) ||
                            ( i != 0 && buf_Entry->trx_Info.snoop_Mask[i - 1] == RIO_FALSE && 
                            buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE ) )
                        {
                            mem_Piece_DW_Size = granule_Size / RIO_LL_DW_BYTE_SIZE;
                            req_Address = ( buf_Entry->trx_Info.address & gran_Align_Mask ) + i * granule_Size;
                        }

                        /* add size of the next granule */
                        if ( i > 0 && buf_Entry->trx_Info.snoop_Mask[i - 1] == RIO_TRUE && 
                            buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE )
                            mem_Piece_DW_Size += granule_Size / RIO_LL_DW_BYTE_SIZE;

                        /* find out end of the memory piece */
                        if ( i < (buf_Entry->trx_Info.snoop_DW_Size * RIO_LL_DW_BYTE_SIZE / granule_Size - 1) &&
                            buf_Entry->trx_Info.snoop_Mask[i] == RIO_TRUE && buf_Entry->trx_Info.snoop_Mask[i + 1] == RIO_FALSE )
                            break;
                    }

                    /* find out data offset for the WRITE from snoop memory operation */
                    buf_Entry->trx_Info.data_Offset = (RIO_UCHAR_T)(( req_Address - 
                        ( buf_Entry->trx_Info.address & gran_Align_Mask ) ) 
                        / RIO_LL_DW_BYTE_SIZE);

                    /* find out data size for the WRITE from snoop memory operation */
                    req_DW_Size = mem_Piece_DW_Size;
                    buf_Entry->trx_Info.data_DW_Size = (RIO_UCHAR_T)req_DW_Size;

                    break;
                }
            }

            /* change queue element state and set semaphor state*/      
            change_Queue_Element_State(head, RIO_LL_WAIT_FOR_RESPONCE);         
            add_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);       

            /* invoke callback */
            handle->callback_Tray.rio_Memory_Request(
                handle->callback_Tray.memory_Context,
                req_Address,
                buf_Entry->trx_Info.extended_Address,
                buf_Entry->trx_Info.xamsbs,
                (RIO_UCHAR_T)req_DW_Size,
                RIO_MEM_WRITE,
                is_GSM_Transaction( handle, head->buf_Tag ),
                be          
            );

            /* unset semaphor state */
            remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);        

            return RIO_OK;
        }
        else
        /* there was no snoop request or there was SNOOP_MISS */
        /* original memory request shall be serviced          */
        {
            /* set snoop data buffer flag */
            buf_Entry->trx_Info.get_Data_From_Snoop = RIO_FALSE;

            /* change queue element state and set semaphor state*/      
            change_Queue_Element_State(head, RIO_LL_WAIT_FOR_RESPONCE);         
            add_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);       

            /* invoke callback */
            handle->callback_Tray.rio_Memory_Request(
                handle->callback_Tray.memory_Context,
                buf_Entry->trx_Info.address,
                buf_Entry->trx_Info.extended_Address,
                buf_Entry->trx_Info.xamsbs,
                buf_Entry->trx_Info.dw_Size,
                buf_Entry->trx_Info.mem_Type,
                is_GSM_Transaction( handle, head->buf_Tag ),
                be          
            );

            /* unset semaphor state */
            remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_MEM);        

            return RIO_OK;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function :    service_Snoop_Queue
 *
 * Description:  this function is a "read-only" function of ll target snoop 
 *               queue. It is called to check the status of the snoop queue. 
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
static int service_Snoop_Queue( RIO_LL_DS_T *handle )
{
    RIO_LL_QUEUE_T  *head, *current;
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_SNOOP_REQ_T snoop_Req;
    RIO_UCHAR_T     granule_Size;  /* size of the cache granule defined by the environment */
    RIO_WORD_T      aligment_Mask; /* aligment mask used to align addresses on the */
                                   /* cache granule boundaries                     */
    RIO_WORD_T      snoop_Data_Size; /* size of the buffer to store snoop data */

    head = handle->target_Queues.snoop_Queue_Head;

    /* if queue is empty do nothing */
    if (head == NULL)
        return RIO_OK;

    /* find an element waiting for snoop responce */
    /* if there is such ana element do nothing */
    current = find_Queue_Element_With_State(head, RIO_LL_WAIT_FOR_RESPONCE);
    /* if there is no such an element service first element in queue */
    if (current == NULL)
    {
        /* get appropriate buffer entry */
        buf_Entry = get_Buffer_Entry( handle, head->buf_Tag );
        if (buf_Entry == NULL)
        {
            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, head->buf_Tag, "internal LL snoop queue servicing");
            return RIO_ERROR;
        }

        /* define cahce granule size and the aligment mask */
        if ( handle->inst_Params.coh_Granule_Size_32 )
        {
            granule_Size  = 32;  /* thirty two byte coherency granule size */
            aligment_Mask = RIO_LL_ADDRESS_GRAN_32_ALIGN;
        }
        else 
        {
            granule_Size  = 64;  /* sixty four byte coherency granule size */
            aligment_Mask = RIO_LL_ADDRESS_GRAN_64_ALIGN;
        }

        /* define start and finish addresses for the snoop operation */
        buf_Entry->trx_Info.snoop_Start_Address   = buf_Entry->trx_Info.address & aligment_Mask;
        buf_Entry->trx_Info.snoop_Current_Address = buf_Entry->trx_Info.snoop_Start_Address;
        if ( !is_GSM_Transaction( handle, head->buf_Tag ) )
            buf_Entry->trx_Info.snoop_Finish_Address  = ( buf_Entry->trx_Info.address + 
                ( buf_Entry->trx_Info.dw_Size - 1 ) * RIO_LL_DW_BYTE_SIZE ) & aligment_Mask;
        else 
            buf_Entry->trx_Info.snoop_Finish_Address = buf_Entry->trx_Info.snoop_Start_Address;
            

        /* define snoop data size */
        snoop_Data_Size = (buf_Entry->trx_Info.snoop_Finish_Address - 
            buf_Entry->trx_Info.snoop_Start_Address + granule_Size) / RIO_LL_DW_BYTE_SIZE;

        /* if memory for the snoop buffer is already allocated - return it */
        if ( buf_Entry->trx_Info.snoop_Data != NULL )
        {
            free( buf_Entry->trx_Info.snoop_Data );
            buf_Entry->trx_Info.snoop_Data = NULL;                
        }
        if ( buf_Entry->trx_Info.snoop_Mask != NULL )
        {
            free( buf_Entry->trx_Info.snoop_Mask );
            buf_Entry->trx_Info.snoop_Mask = NULL;
        }

        /* allocate memory for the temporary snoop data buffer */
        /* this memory is to be returned after snoop completion */
        /* or during initialization                            */
        buf_Entry->trx_Info.snoop_Data = (RIO_DW_T *)malloc( snoop_Data_Size * sizeof( RIO_DW_T ));
        buf_Entry->trx_Info.snoop_Mask = (RIO_BOOL_T *)malloc( ((snoop_Data_Size * RIO_LL_DW_BYTE_SIZE) / 
            granule_Size) * sizeof( RIO_BOOL_T ));

        /* set snoop size in the transaction buffer */
        buf_Entry->trx_Info.snoop_DW_Size = (RIO_UCHAR_T)snoop_Data_Size;

        /* fill in snoop request data structure */
        snoop_Req.address          = buf_Entry->trx_Info.snoop_Start_Address;
        snoop_Req.extended_Address = buf_Entry->trx_Info.extended_Address;
        snoop_Req.xamsbs           = buf_Entry->trx_Info.xamsbs;
        snoop_Req.lttype           = buf_Entry->trx_Info.snoop_LTType;
        snoop_Req.ttype            = buf_Entry->trx_Info.snoop_TType;

        /* change element's state and set semaphor state */
        change_Queue_Element_State(head, RIO_LL_WAIT_FOR_RESPONCE); 
        add_Semaphor_State( handle, RIO_LL_SEMAPHOR_SNOOP);     

        /* invoke callback */
        handle->callback_Tray.rio_Snoop_Request(
            handle->callback_Tray.snoop_Context,
            snoop_Req
        );

        /* unset semaphor state */
        remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_SNOOP);      
    }

    return RIO_OK;
}

/***************************************************************************
 * Function :    service_Request_Responce_Queues
 *
 * Description:  this function is a "read-only" function of ll target request
 *               and responce queues. It is called to check the status of the
 *               request and responce queues. 
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
static int service_Request_Responce_Queues( RIO_LL_DS_T *handle )
{
    RIO_LL_QUEUE_T  *req_Current, *resp_Current;
    RIO_LL_QUEUE_T  *req_To_Send = NULL, *resp_To_Send = NULL;
    RIO_LL_QUEUE_T  *req_Head, *resp_Head;

    RIO_LL_BUFFER_T *buf_Entry;
    RIO_RESPONSE_T  responce;


    req_Head = handle->target_Queues.rio_Req_Queue_Head;
    resp_Head = handle->target_Queues.rio_Resp_Queue_Head;
    
    /* if queues are empty do nothing */
    if ((req_Head == NULL) && (resp_Head == NULL))
        return RIO_OK;

    /* find request and responces waiting for acknowledge */
    req_Current = find_Queue_Element_With_State(req_Head, RIO_LL_WAIT_FOR_ACK);
    resp_Current = find_Queue_Element_With_State(resp_Head, RIO_LL_WAIT_FOR_ACK);

    /* responce queue is empty*/
    if (resp_Head == NULL)  
    {
        if ( req_Current != req_Head)
        /* more priorital request is present - send it, in other case do nothing */
        {
            if (req_Current != NULL)
                change_Queue_Element_State( req_Current, RIO_LL_WAIT_FOR_SERVICE);  
            req_To_Send = req_Head;
        }
    }

    /* request queue is empty*/
    if (req_Head == NULL)   
    {
        if ( resp_Current != resp_Head)
        /* more priorital responce is present - send it, in other case do nothing */
        {
            if ( resp_Current != NULL )
                change_Queue_Element_State( resp_Current, RIO_LL_WAIT_FOR_SERVICE); 
            resp_To_Send = resp_Head;
        }
    }

    /* both request and responce queues aren't empty */
    if ((req_Head != NULL) && (resp_Head != NULL))
    {
        /* if more priorital responce is present - send it */
        if ( resp_Head->prio >= req_Head->prio)
        {
            if (resp_Current != resp_Head)
            {
                if (resp_Current != NULL)
                    change_Queue_Element_State( resp_Current, RIO_LL_WAIT_FOR_SERVICE);
                if (req_Current != NULL)
                    change_Queue_Element_State( req_Current, RIO_LL_WAIT_FOR_SERVICE);
                resp_To_Send = resp_Head;
            }
        }
        /* if more priorital request is present - send it */
        else if ( req_Head->prio > resp_Head->prio)
        {
            if (req_Current != req_Head)
            {
                if (resp_Current != NULL)
                    change_Queue_Element_State( resp_Current, RIO_LL_WAIT_FOR_SERVICE);
                if (req_Current != NULL)
                    change_Queue_Element_State( req_Current, RIO_LL_WAIT_FOR_SERVICE);
                req_To_Send = req_Head;
            }
        }
    }

    if ( resp_To_Send != NULL)
    {
        /* get appropriate buffer entry */
        buf_Entry = get_Buffer_Entry( handle, resp_To_Send->buf_Tag );
        if (buf_Entry == NULL)
        {
            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                resp_To_Send->buf_Tag, "internal LL request response queue servicing");
            return RIO_ERROR;
        }

        /* fill in responce data structure */
        responce.prio        = resp_To_Send->prio;
        responce.ftype       = buf_Entry->trx_Info.resp_FType;              
        responce.transaction = buf_Entry->trx_Info.resp_Transaction;
        responce.tr_Info     = buf_Entry->trx_Info.resp_Tr_Info;
        responce.transp_Type = buf_Entry->trx_Info.resp_Transp_Type;
        responce.status      = buf_Entry->trx_Info.resp_Status;
        responce.sec_ID      = buf_Entry->trx_Info.sec_ID;
        responce.sec_TID     = buf_Entry->trx_Info.sec_TID;

        /* fill in Target_TID field*/
        if (buf_Entry->trx_Info.resp_Transaction == RIO_RESPONCE_MESSAGE)
        /* if it is message responce */
        {
            /* insert letter number into TID accordingly to RIO interconnect spec */
            responce.target_TID = (buf_Entry->trx_Info.letter & 0x3) << 6;  
            /* insert message box number into TID accordingly to RIO interconnect spec */
            responce.target_TID = responce.target_TID | ((buf_Entry->trx_Info.mbox & 0x3) << 4);
            /* insert message segment number into TID accordingly to RIO interconnect spec */
            responce.target_TID = responce.target_TID | (buf_Entry->trx_Info.msgseg & 0xF);
        }
        else
            /* if it is other responce */
            responce.target_TID = buf_Entry->trx_Info.resp_Target_TID; 

        /* fill in responce data fields */
        switch ( responce.transaction )   
        {
            case RIO_RESPONCE_WO_DATA:
            case RIO_RESPONCE_MESSAGE:
            case RIO_MAINTENANCE_WRITE_RESPONCE:
            {
                responce.dw_Num = 0;     
                responce.data   = NULL;                     
                break;
            }

            case RIO_RESPONCE_WITH_DATA:
            case RIO_RESPONCE_INTERVENTION:
            case RIO_RESPONCE_INTERVENTION_WO_DATA:
            case RIO_MAINTENANCE_READ_RESPONCE:
            {
                responce.dw_Num = buf_Entry->trx_Info.dw_Size;     
                responce.data   = buf_Entry->trx_Info.data;      
                break;
            }
        }

        /* set element's state and set semaphor state */
        change_Queue_Element_State( resp_To_Send, RIO_LL_WAIT_FOR_ACK );
        add_Semaphor_State( handle, RIO_LL_SEMAPHOR_ACK );
    
        /* invoke nesseccary callback */
        handle->callback_Tray.rio_TL_Send_Response(
            handle->callback_Tray.rio_TL_Outbound_Context,
            resp_To_Send->buf_Tag,
            &responce
        );

        /* unset semaphor state */
        remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_ACK );
    }
    else if (req_To_Send != NULL)
    {
        /* get appropriate buffer entry */
        buf_Entry = get_Buffer_Entry( handle, req_To_Send->buf_Tag );
        if (buf_Entry == NULL)
        {
            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                req_To_Send->buf_Tag, "internal LL request response queue servicing");
            return RIO_ERROR;
        }

        /* clear some buffer entrues */
        buf_Entry->trx_Info.resp_Status = 0;


        /* change element's state and set semaphor state */
        change_Queue_Element_State( req_To_Send, RIO_LL_WAIT_FOR_ACK );
        add_Semaphor_State( handle, RIO_LL_SEMAPHOR_ACK );

        /* invoke nesseccary callback */
        switch ( buf_Entry->trx_Info.req_Type )
        {
            case RIO_LL_IO:
                handle->callback_Tray.rio_TL_IO_Request(
                    handle->callback_Tray.rio_TL_Outbound_Context,
                    ( RIO_IO_REQ_T* )(buf_Entry->trx_Info.req),
                    req_To_Send->buf_Tag,
                    buf_Entry->trx_Info.tr_Info[0]
                );
            break;

            case RIO_LL_GSM:
                handle->callback_Tray.rio_TL_GSM_Request(
                    handle->callback_Tray.rio_TL_Outbound_Context,
                    buf_Entry->trx_Info.gsm_TType,                  
                    ( RIO_GSM_REQ_T* )(buf_Entry->trx_Info.req),
                    buf_Entry->trx_Info.sec_ID,
                    buf_Entry->trx_Info.sec_TID,
                    req_To_Send->buf_Tag,
                    buf_Entry->trx_Info.tr_Info,
                    buf_Entry->trx_Info.multicast_Size
                );
            break;

            case RIO_LL_CONF:
                {
                    RIO_CONFIG_REQ_T conf_Req;
                    conf_Req = *(( RIO_CONFIG_REQ_T* )buf_Entry->trx_Info.req);
                    /*the payload is not copied and will be used from the 
                    ll buffer directly. This is not a problem because it 
                    isn't change*/
                    handle->callback_Tray.rio_TL_Configuration_Request(
                        handle->callback_Tray.rio_TL_Outbound_Context,
                        &conf_Req,
                        req_To_Send->buf_Tag
                    );
                }
            break;

            case RIO_LL_MESSAGE:
                (( RIO_MESSAGE_REQ_T* )(buf_Entry->trx_Info.req))->transport_Info = buf_Entry->trx_Info.tr_Info[0];
                handle->callback_Tray.rio_TL_Message_Request(
                    handle->callback_Tray.rio_TL_Outbound_Context,
                    ( RIO_MESSAGE_REQ_T* )(buf_Entry->trx_Info.req),
                    req_To_Send->buf_Tag
                );
            break;

            case RIO_LL_DOORBELL:
                ((RIO_DOORBELL_REQ_T*)(buf_Entry->trx_Info.req))->transport_Info = buf_Entry->trx_Info.tr_Info[0];
                handle->callback_Tray.rio_TL_Doorbell_Request(
                    handle->callback_Tray.rio_TL_Outbound_Context,
                    ( RIO_DOORBELL_REQ_T* )(buf_Entry->trx_Info.req),
                    req_To_Send->buf_Tag
                );
            break;

	    /* GDA: Bug#124 - Support for Data Streaming packet */
	    case RIO_LL_DS:
	            handle->callback_Tray.rio_TL_DS_Request(
	            handle->callback_Tray.rio_TL_Outbound_Context,
	            ( RIO_DS_REQ_T* )(buf_Entry->trx_Info.req),
		    req_To_Send->buf_Tag,
		    buf_Entry->trx_Info.tr_Info[0]
				 );
	    break;
	    /* GDA: Bug#124 */

	    /* GDA: Bug#125 - Support for Flow control packet */
            case RIO_LL_FC:
                handle->callback_Tray.rio_TL_FC_Request(
                    handle->callback_Tray.rio_TL_Outbound_Context,
                    ( RIO_FC_REQ_T* )(buf_Entry->trx_Info.req),
                    req_To_Send->buf_Tag,
                    buf_Entry->trx_Info.tr_Info[0]
                );
            break;
	    /* GDA: Bug#125 */

        }

        /* unset semaphor state */
        remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_ACK );
    }

    return RIO_OK;
}

/***************************************************************************
 * Function :    process_Transaction
 *
 * Description:  function tryes to process transactions stalled against 
 *               transaction defined by completed_Transaction or get 
 *               new transaction from the temporary buffer                    
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        
 *
 **************************************************************************/
static int process_Transaction( 
    RIO_LL_DS_T        *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_EVENT_T event
)
{
    RIO_LL_BUFFER_T       *buf_Entry;
    RIO_LL_BUF_TAG_T      new_Tag = 0;
    RIO_LL_TRX_EVENT_T    new_Event;
    unsigned int          result;
    RIO_LL_TRX_STATE_T    state;

#ifdef _DEBUG
    assert( handle );
#endif
    
    /* get buffer entry for the transaction */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);

    /* process transaction */
    state  = buf_Entry->trx_Info.state;
    result = buf_Entry->kernel_Info.handler( handle, buf_Tag, state, event );

    /* if the error occured during transaction handler execution */
    if ( result == RIO_LL_ERROR )
    {
        /* issue error message */
        RIO_LL_Message( handle, RIO_LL_ERROR_TRX_HANDLER_ERROR, "", 
            buf_Entry->trx_Info.req_FType, buf_Entry->trx_Info.req_TType );
    }

    /* check transaction handler completion code */
    if ( result != RIO_LL_COMPLETE && result != RIO_LL_PROGRESS && result != RIO_LL_ERROR )
        /* unexpected transaction handler completion code */
        /* issue warning message */
        RIO_LL_Message( handle, RIO_LL_WARN_WRONG_TRX_COMPLETION, result );

    if ( result == RIO_LL_COMPLETE )
    {
        /* check if some transaction is stalled by the completed transaction */
        if ( buf_Entry->kernel_Info.stall_Tag_Valid )
            new_Tag = buf_Entry->kernel_Info.stall_Tag;
        else 
            new_Tag = 0;

        /* clean its buffer entry if necessary */
        if ( buf_Entry->trx_Info.req != NULL )
        {
            /* clean data field */
            switch ( buf_Entry->trx_Info.req_Type )         
            {
                case RIO_LL_IO:
                {
                    if ( (( RIO_IO_REQ_T* )(buf_Entry->trx_Info.req))->data != NULL )
                        free( (( RIO_IO_REQ_T* )(buf_Entry->trx_Info.req))->data );
                    break;
                }

                case RIO_LL_GSM:
                    if ( (( RIO_GSM_REQ_T* )(buf_Entry->trx_Info.req))->data != NULL )
                        free( (( RIO_GSM_REQ_T* )(buf_Entry->trx_Info.req))->data );
                break;

                case RIO_LL_CONF:
                    if ( (( RIO_CONFIG_REQ_T* )(buf_Entry->trx_Info.req))->data != NULL )
                        free( (( RIO_CONFIG_REQ_T* )(buf_Entry->trx_Info.req))->data );
                break;

                case RIO_LL_MESSAGE:
                    if ( (( RIO_MESSAGE_REQ_T* )(buf_Entry->trx_Info.req))->data != NULL )
                        free( (( RIO_MESSAGE_REQ_T* )(buf_Entry->trx_Info.req))->data );
                break;

                case RIO_LL_DOORBELL:
                break;

		/* GDA: Bug#124 - Support for Data Streaming packet */
                case RIO_LL_DS:
		    if ( (( RIO_DS_REQ_T* )(buf_Entry->trx_Info.req))->data_hw != NULL )
		         free( (( RIO_DS_REQ_T* )(buf_Entry->trx_Info.req))->data_hw );
		break;
		/* GDA: Bug#124 */

		/* GDA: Bug#125 - Support for Flow control packet */
                case RIO_LL_FC:
                break;
		/* GDA: Bug#125 */

            }
            free( buf_Entry->trx_Info.req );
            buf_Entry->trx_Info.req = NULL;
        }

        if ( buf_Entry->trx_Info.snoop_Data != NULL )
        {
           free( buf_Entry->trx_Info.snoop_Data );
            buf_Entry->trx_Info.snoop_Data = NULL;
        }

        if ( buf_Entry->trx_Info.snoop_Mask != NULL )
        {
           free( buf_Entry->trx_Info.snoop_Mask );
            buf_Entry->trx_Info.snoop_Mask = NULL;
        }
        
        /* deallocate buffer entry */
        deallocate_Buffer_Entry( handle, buf_Tag );

        /* if there are stalled transaction */
        if ( new_Tag != 0 )
        /* process stalled transaction */
        {
            /* get buffer entry for the new transaction */
            buf_Entry = get_Buffer_Entry( handle, new_Tag );

            /* find out event for the futher transaction processing */
            if ( is_IO_Transaction( handle, new_Tag ) )
                new_Event = RIO_EVENT_START_NO_COLLISION;
            
            if ( is_GSM_Transaction( handle, new_Tag ) )
                new_Event = RIO_EVENT_END_COLLISION_NORMAL;

	    /* GDA: Bug#124 - Support for Data Streaming packet */
	     if ( is_DS_Transaction( handle, new_Tag ) )
	        new_Event = RIO_EVENT_START_NO_COLLISION;
	    /* GDA: Bug#124 */

	    /* GDA: Bug#125 - Support for Flow control packet */	    
            if ( is_FC_Transaction( handle, new_Tag ) )
                new_Event = RIO_EVENT_END_COLLISION_NORMAL;
	    /* GDA: Bug#125 */

            process_Transaction( handle, new_Tag, new_Event );
        }

        /* try to get transaction from the temporary buffer */
        if ( get_Transaction_From_Temp_Buffer( handle, &new_Tag, &new_Event) == RIO_OK)
            /* process transaction */
            return process_Transaction( handle, new_Tag, new_Event );
    }

    /* service target queues */
    return service_Target_Queues( handle );
}

/***************************************************************************
 * Function :    is_IO_Local_Collision
 *
 * Description:  function checks if there is a collision for the  
 *               locally requested IO transaction identified by buf_Tag 
 *               with some other transactions
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:        The collision means that two transactions trying to perform 
 *               some memory operation with same memory area. In case of the
 *               collision for the local transaction it shall be retried to
 *               the enviroment 
 *
 **************************************************************************/
static RIO_BOOL_T is_IO_Local_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag )
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_ADDRESS_T   start_Address_New, finish_Address_New;
    RIO_ADDRESS_T   start_Address_Old, finish_Address_Old;
    unsigned int    i;
    
    unsigned long ext_Address_Support;
    RIO_BOOL_T    ext_Address;
    
    
#ifdef _DEBUG
    assert( handle );
#endif

    ext_Address_Support = get_Config_Reg_Field( handle, RIO_LL_EXT_ADDRESSING_SUPPORT );
    ext_Address = (ext_Address_Support == RIO_LL_PE_LL_CSR_50 || ext_Address_Support == RIO_LL_PE_LL_CSR_66 ) ? 
        RIO_TRUE : RIO_FALSE;


    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_TRUE;
    }

    /* check if the transaction routed to the local memory space         */
    /* if so transaction will not participate in the collision detection */
    if ( !buf_Entry->kernel_Info.is_Local )
        return RIO_FALSE;

    /* find out the memory area for the incoming (new) transaction */
    start_Address_New  = buf_Entry->trx_Info.address & RIO_LL_ADDRESS_DW_ALIGN;
    finish_Address_New = start_Address_New + buf_Entry->trx_Info.dw_Size * RIO_LL_DW_BYTE_SIZE - 1;

    /* for all transaction in the outbound buffer check                */
    /* that there is no collision with the transactions identified by  */
    /* the buf_Tag                                                     */
    for ( i = 0; i < handle->inst_Params.outbound_Buffer_Size; i++ )
        if ( handle->loc_Buffer[i].kernel_Info.valid &&
            is_IO_Transaction( handle, RIO_LL_LOCAL_BUFFER_START + i ) &&
            buf_Tag != RIO_LL_LOCAL_BUFFER_START + i &&
            handle->loc_Buffer[i].kernel_Info.is_Local )
        {
            /* find out memory area for the transaction */
            start_Address_Old  = handle->loc_Buffer[i].trx_Info.address & RIO_LL_ADDRESS_DW_ALIGN;
            finish_Address_Old = start_Address_Old + handle->loc_Buffer[i].trx_Info.dw_Size * RIO_LL_DW_BYTE_SIZE - 1;

            /* check if there is a collision */
            if ( ext_Address && 
                buf_Entry->trx_Info.extended_Address != handle->loc_Buffer[i].trx_Info.extended_Address )
                continue;

            if ( /* handle->init_Params.xamsbs && */
                buf_Entry->trx_Info.xamsbs != handle->loc_Buffer[i].trx_Info.xamsbs )
                continue;            

            if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New) ) )
                return RIO_TRUE;
        }

    /* for all transaction in the inbound buffer check                 */
    /* that there is no collision with the transactions identified by  */
    /* the buf_Tag                                                     */
    for ( i = 0; i < handle->inst_Params.inbound_Buffer_Size; i++ )
        if ( handle->remote_Buffer[i].kernel_Info.valid &&
            is_IO_Transaction( handle, RIO_LL_REMOTE_BUFFER_START + i ) )
        {
            /* find out memory area for the transaction */
            start_Address_Old  = handle->remote_Buffer[i].trx_Info.address & RIO_LL_ADDRESS_DW_ALIGN;
            finish_Address_Old = start_Address_Old + handle->remote_Buffer[i].trx_Info.dw_Size * RIO_LL_DW_BYTE_SIZE - 1;

            /* check if there is a collision */
            if ( ext_Address && 
                buf_Entry->trx_Info.extended_Address != handle->remote_Buffer[i].trx_Info.extended_Address )
                continue;

            if ( /* handle->init_Params.xamsbs && */
                buf_Entry->trx_Info.xamsbs != handle->remote_Buffer[i].trx_Info.xamsbs )
                continue;            

            if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New ) ) )
                return RIO_TRUE;
        }

    return RIO_FALSE;
}

/***************************************************************************
 * Function :    is_IO_Remote_Collision
 *
 * Description:  function checks if there is a collision for the  
 *               remotely requested IO transaction identified by buf_Tag 
 *               with some other transactions
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:        The collision means that two transactions trying to perform 
 *               some memory operation with same memory area. In case of the
 *               collision for the remote transaction it will be stalled and
 *               wait for the previous transactions completion 
 *
 **************************************************************************/
static RIO_BOOL_T is_IO_Remote_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag )
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_LL_BUFFER_T *stalled_Buf_Entry;
    RIO_ADDRESS_T   start_Address_New, finish_Address_New;
    RIO_ADDRESS_T   start_Address_Old, finish_Address_Old;
    unsigned int    i;
    
    unsigned long ext_Address_Support;
    RIO_BOOL_T    ext_Address;
    
    
#ifdef _DEBUG
    assert( handle );
#endif

    ext_Address_Support = get_Config_Reg_Field( handle, RIO_LL_EXT_ADDRESSING_SUPPORT );
    ext_Address = (ext_Address_Support == RIO_LL_PE_LL_CSR_50 || ext_Address_Support == RIO_LL_PE_LL_CSR_66 ) ? 
        RIO_TRUE : RIO_FALSE;
    
    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_FALSE;
    }

    /* if the transaction is to config space excluding ATOMIC transactions */
    /* it should not take part in the collision detection                  */
    if ( RIO_LL_Kernel_Is_To_Config_Space( handle, buf_Tag) &&
        buf_Entry->trx_Info.req_TType != RIO_NONINTERV_ATOMIC_INC &&
        buf_Entry->trx_Info.req_TType != RIO_NONINTERV_ATOMIC_DEC &&
        buf_Entry->trx_Info.req_TType != RIO_NONINTERV_ATOMIC_SET &&
        buf_Entry->trx_Info.req_TType != RIO_NONINTERV_ATOMIC_CLEAR &&
        buf_Entry->trx_Info.req_TType != RIO_WRITE_ATOMIC_TSWAP &&
	/* GDA: Bug#133 - Support for ttype C for type 5 packet */
        buf_Entry->trx_Info.req_TType != RIO_WRITE_ATOMIC_SWAP
        )
        return RIO_FALSE;

    /* find out the memory area for the incoming (new) transaction */
    start_Address_New  = buf_Entry->trx_Info.address & RIO_LL_ADDRESS_DW_ALIGN;
    finish_Address_New = start_Address_New + buf_Entry->trx_Info.dw_Size * RIO_LL_DW_BYTE_SIZE - 1;

    /*
     * for all transaction in the outbound buffer check                
     * that there is no collision with the transactions identified by  
     * the buf_Tag. And if there is a collision incoming transaction will be stalled
     * against transaction in the outbound buffer.
     */
    for ( i = 0; i < handle->inst_Params.outbound_Buffer_Size; i++ )
        if ( handle->loc_Buffer[i].kernel_Info.valid &&
            is_IO_Transaction( handle, RIO_LL_LOCAL_BUFFER_START + i ) &&
            handle->loc_Buffer[i].kernel_Info.is_Local )
        {
            /* find out memory area for the transaction */
            start_Address_Old  = handle->loc_Buffer[i].trx_Info.address & RIO_LL_ADDRESS_DW_ALIGN;
            finish_Address_Old = start_Address_Old + handle->loc_Buffer[i].trx_Info.dw_Size * RIO_LL_DW_BYTE_SIZE - 1;

            /* check if there is a collision */
            if ( ext_Address && 
                buf_Entry->trx_Info.extended_Address != handle->loc_Buffer[i].trx_Info.extended_Address )
                continue;

            if ( /* handle->init_Params.xamsbs && */
                buf_Entry->trx_Info.xamsbs != handle->loc_Buffer[i].trx_Info.xamsbs )
                continue;            

            if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New) ) )
            {
                /* add transaction to the end of the chain of the stalled transactions */
                if ( handle->loc_Buffer[i].kernel_Info.stall_Tag_Valid )       
                /* there is stalled transactions */                         
                {
                    /* get buffer entry for the transactions stalling others */
                    stalled_Buf_Entry = get_Buffer_Entry( handle, RIO_LL_LOCAL_BUFFER_START + i );
                    if ( stalled_Buf_Entry == NULL)
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                            RIO_LL_LOCAL_BUFFER_START + i, "internal kernel function");
                        return RIO_FALSE;
                    }

                    /* find the end of the chain of the stalled transactions */
                    do 
                    {
                        stalled_Buf_Entry = get_Buffer_Entry( handle, stalled_Buf_Entry->kernel_Info.stall_Tag );
                        if ( stalled_Buf_Entry == NULL)
                        {
                            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                                RIO_LL_LOCAL_BUFFER_START + i, "internal kernel function");
                            return RIO_FALSE;
                        }
                    }
                    while ( !stalled_Buf_Entry->kernel_Info.stall_Tag_Valid );

                    stalled_Buf_Entry->kernel_Info.stall_Tag_Valid = RIO_TRUE;
                    stalled_Buf_Entry->kernel_Info.stall_Tag       = buf_Tag;
                }
                else
                /* there is no stalled transactions */
                {
                    handle->loc_Buffer[i].kernel_Info.stall_Tag_Valid = RIO_TRUE;
                    handle->loc_Buffer[i].kernel_Info.stall_Tag       = buf_Tag;
                }

                /* fill in buffer entries */
                buf_Entry->kernel_Info.is_Stalled = RIO_TRUE;

                /* return value */
                return RIO_TRUE;
            }
        }

    /*                                             
     * There is no need to check if there is a collision with some inbound transaction 
     * because of the strong ordering of the incoming transactions in this 
     * RIO implementation. IT IS IMPLEMENTATION SPECIFIC FEATURE.
     */

    return RIO_FALSE;
}

/***************************************************************************
 * Function :    is_GSM_Local_Collision
 *
 * Description:  function checks if there is a collision for the  
 *               locally requested GSM transaction identified by buf_Tag 
 *               with some other transactions
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:        The collision means that two transactions trying to perform 
 *               some memory operation with same memory area. In case of the
 *               collision for the local transaction it shall be retried to
 *               the enviroment 
 *
 **************************************************************************/
static RIO_BOOL_T is_GSM_Local_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag )
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_ADDRESS_T   start_Address_New, finish_Address_New;
    RIO_ADDRESS_T   start_Address_Old, finish_Address_Old;
    RIO_WORD_T      granule_Size;
    RIO_WORD_T      granule_Align_Mask;
    unsigned int    i;
    
    unsigned long ext_Address_Support;
    RIO_BOOL_T    ext_Address;
    
    
#ifdef _DEBUG
    assert( handle );
#endif

    ext_Address_Support = get_Config_Reg_Field( handle, RIO_LL_EXT_ADDRESSING_SUPPORT );
    ext_Address = (ext_Address_Support == RIO_LL_PE_LL_CSR_50 || ext_Address_Support == RIO_LL_PE_LL_CSR_66 ) ? 
        RIO_TRUE : RIO_FALSE;

    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        return RIO_TRUE;
    }

    /* three transactions: CASTOUT, TLBIE, TLBSYNC don't take part in the */
    /* collision resolution for the locally requested transactions                 */
    if ( buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_TLBIE || 
        buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_TLBSYNC ||
        buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_CASTOUT )
            return RIO_FALSE;

    /* find out cache granule size and aligment mask */
    if ( handle->inst_Params.coh_Granule_Size_32 )
    {
        granule_Size       = 32; /* thirty two byte coheerency granule size */
        granule_Align_Mask = RIO_LL_ADDRESS_GRAN_32_ALIGN;
    }
    else
    {
        granule_Size       = 64; /* sixty four byte coheerency granule size */
        granule_Align_Mask = RIO_LL_ADDRESS_GRAN_64_ALIGN;
    }

    /* find out the memory area for the incoming (new) transaction */
    start_Address_New  = buf_Entry->trx_Info.address & granule_Align_Mask;
    finish_Address_New = start_Address_New + granule_Size - 1;

    /* for all transaction in the outbound buffer check                */
    /* that there is no collision with the transactions identified by  */
    /* the buf_Tag                                                     */
    for ( i = 0; i < handle->inst_Params.outbound_Buffer_Size; i++ )
        if ( handle->loc_Buffer[i].kernel_Info.valid &&
            is_GSM_Transaction( handle, RIO_LL_LOCAL_BUFFER_START + i ) &&
            buf_Tag != RIO_LL_LOCAL_BUFFER_START + i )
        {
            /* find out memory area for the transaction */
            start_Address_Old  = handle->loc_Buffer[i].trx_Info.address & granule_Align_Mask;
            finish_Address_Old = start_Address_Old + granule_Size - 1;

            /* check if there is a collision */
            if ( ext_Address && 
                buf_Entry->trx_Info.extended_Address != handle->loc_Buffer[i].trx_Info.extended_Address )
                continue;

            if ( /* handle->init_Params.xamsbs && */
                buf_Entry->trx_Info.xamsbs != handle->loc_Buffer[i].trx_Info.xamsbs )
                continue;            

            if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New) ) )
                return RIO_TRUE;
        }

    /* for all transaction in the inbound buffer check                 */
    /* that there is no collision with the transactions identified by  */
    /* the buf_Tag                                                     */
    for ( i = 0; i < handle->inst_Params.inbound_Buffer_Size; i++ )
        if ( handle->remote_Buffer[i].kernel_Info.valid &&
            is_GSM_Transaction( handle, RIO_LL_REMOTE_BUFFER_START + i ) )
        {
            /* find out memory area for the transaction */
            start_Address_Old  = handle->remote_Buffer[i].trx_Info.address & granule_Align_Mask;
            finish_Address_Old = start_Address_Old + granule_Size - 1;

            /* check if there is a collision */
            if ( ext_Address && 
                buf_Entry->trx_Info.extended_Address != handle->remote_Buffer[i].trx_Info.extended_Address )
                continue;

            if ( /* handle->init_Params.xamsbs && */
                buf_Entry->trx_Info.xamsbs != handle->remote_Buffer[i].trx_Info.xamsbs )
                continue;            

            if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New ) ) )
                return RIO_TRUE;
        }

    return RIO_FALSE;
}

/***************************************************************************
 * Function :    is_GSM_Remote_Collision
 *
 * Description:  function checks if there is a collision for the  
 *               remotely requested GSM transaction identified by buf_Tag 
 *               with some other transactions
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:        The collision means that two transactions trying to perform 
 *               some memory operation with same memory area. In case of the
 *               collision for the remote transaction collision shall be 
 *               resolved accordingly tp the collision resolution tables 
 *               defined in the "RIO GSM Logical ..."
 *
 *               Event for the futher transaction processing is returned
 *               through the event parameter.
 *
 **************************************************************************/
static RIO_BOOL_T is_GSM_Remote_Collision( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag, RIO_LL_TRX_EVENT_T *event )
{
    RIO_LL_BUFFER_T    *buf_Entry;
    RIO_LL_BUFFER_T    *stalled_Buf_Entry;
    RIO_ADDRESS_T      start_Address_New, finish_Address_New;
    RIO_ADDRESS_T      start_Address_Old, finish_Address_Old;
    RIO_WORD_T         granule_Size;
    RIO_WORD_T         granule_Align_Mask;
    RIO_LL_COLLISION_T result = 0;
    RIO_LL_BUF_TAG_T   collides_Buf_Tag;
    RIO_LL_BUFFER_T    *collides_Buf_Entry;
    unsigned int    i;
    
    unsigned long ext_Address_Support;
    RIO_BOOL_T    ext_Address;
    
    
#ifdef _DEBUG
    assert( handle );
#endif

    ext_Address_Support = get_Config_Reg_Field( handle, RIO_LL_EXT_ADDRESSING_SUPPORT );
    ext_Address = (ext_Address_Support == RIO_LL_PE_LL_CSR_50 || ext_Address_Support == RIO_LL_PE_LL_CSR_66 ) ? 
        RIO_TRUE : RIO_FALSE;

    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal kernel function");
        *event = RIO_EVENT_START_NO_COLLISION;
        return RIO_FALSE;
    }

    /* two transactions: TLBIE, TLBSYNC  don't take part in the  */
    /* collision resolution for the remotely requested transactions         */
    if ( buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_TLBIE || 
        buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_TLBSYNC  )
    {
        *event = RIO_EVENT_START_NO_COLLISION;
        return RIO_FALSE;
    }

    /* find out cache granule size and aligment mask */
    if ( handle->inst_Params.coh_Granule_Size_32 )
    {
        granule_Size       = 32; /* thirty two byte coheerency granule size */
        granule_Align_Mask = RIO_LL_ADDRESS_GRAN_32_ALIGN;
    }
    else
    {
        granule_Size       = 64; /* sixty four byte coheerency granule size */
        granule_Align_Mask = RIO_LL_ADDRESS_GRAN_64_ALIGN;
    }

    /* find out the memory area for the incoming (new) transaction */
    start_Address_New  = buf_Entry->trx_Info.address & granule_Align_Mask;
    finish_Address_New = start_Address_New + granule_Size - 1;

    /*
     * for all outstanding transactions in the outbound and the inbound buffers check                
     * that there is no collision with the transactions identified by  
     * the buf_Tag. And if there is a collision it will be resolved 
     * accordingly to the collision resolution tables
     */

    collides_Buf_Tag = 0;

    /* find colliding outstanding transactions in the local buffer */
    for ( i = 0; i < handle->inst_Params.outbound_Buffer_Size; i++ )
        if ( handle->loc_Buffer[i].kernel_Info.valid &&
            is_GSM_Transaction( handle, RIO_LL_LOCAL_BUFFER_START + i ) &&
            handle->loc_Buffer[i].trx_Info.is_Outstanding )
        {
            /* find out memory area for the transaction */
            start_Address_Old  = handle->loc_Buffer[i].trx_Info.address & granule_Align_Mask;
            finish_Address_Old = start_Address_Old + granule_Size - 1;

            /* check if there is a collision */
            if ( ext_Address && 
                buf_Entry->trx_Info.extended_Address != handle->loc_Buffer[i].trx_Info.extended_Address )
                continue;

            if ( /* handle->init_Params.xamsbs && */
                buf_Entry->trx_Info.xamsbs != handle->loc_Buffer[i].trx_Info.xamsbs )
                continue;            

            if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New) ) )
            {
                collides_Buf_Tag = RIO_LL_LOCAL_BUFFER_START + i;
                break;
            }
        }

    /* if colliding outstanding transactions was not found in the local buffer try to find */
    /* such a transactions in the remote buffer                                            */
    if ( collides_Buf_Tag == 0 )
        for ( i = 0; i < handle->inst_Params.inbound_Buffer_Size; i++ )
            if ( handle->remote_Buffer[i].kernel_Info.valid &&
                is_GSM_Transaction( handle, RIO_LL_REMOTE_BUFFER_START + i ) &&
                handle->remote_Buffer[i].trx_Info.is_Outstanding &&
                RIO_LL_REMOTE_BUFFER_START + i != buf_Tag )
            {
                /* find out memory area for the transaction */
                start_Address_Old  = handle->remote_Buffer[i].trx_Info.address & granule_Align_Mask;
                finish_Address_Old = start_Address_Old + granule_Size - 1;

                /* check if there is a collision */
                if ( ext_Address && 
                    buf_Entry->trx_Info.extended_Address != handle->remote_Buffer[i].trx_Info.extended_Address )
                    continue;

                if ( /* handle->init_Params.xamsbs && */ 
                    buf_Entry->trx_Info.xamsbs != handle->remote_Buffer[i].trx_Info.xamsbs )
                    continue;            

                if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New) ) )
                {
                    collides_Buf_Tag = RIO_LL_REMOTE_BUFFER_START + i;
                    break;
                }
            }

    /* if there is such a transaction resolve collision */
    if ( collides_Buf_Tag != 0 )
    {
        /* get buffer entry for the colliding transaction */
        if ( (collides_Buf_Entry = get_Buffer_Entry( handle, collides_Buf_Tag )) == NULL )
        {
            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, collides_Buf_Tag, "internal kernel function");
            *event = RIO_EVENT_START_NO_COLLISION;
            return RIO_FALSE;
        }

        /* find out the collision resolution accordingly to the collision resolution tables */
        switch ( collides_Buf_Entry->trx_Info.gsm_TType )
        {
            case RIO_PL_GSM_READ_HOME:
                result = cr_Table_READ_HOME[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_IREAD_HOME:
                result = cr_Table_IREAD_HOME[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_READ_TO_OWN_HOME:
                result = cr_Table_READ_TO_OWN_HOME[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_CASTOUT:
                result = cr_Table_CASTOUT[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_DKILL_HOME:
                result = cr_Table_DKILL_HOME[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_IO_READ_HOME:
                result = cr_Table_IO_READ_HOME_Participant[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_FLUSH_WO_DATA:
            case RIO_PL_GSM_FLUSH_WITH_DATA:
                result = cr_Table_FLUSH_Participant[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_READ_OWNER:
                result = cr_Table_READ_OWNER[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_READ_TO_OWN_OWNER:
                result = cr_Table_READ_TO_OWN_OWNER[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_IO_READ_OWNER:
                result = cr_Table_IO_READ_OWNER_Participant[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_DKILL_SHARER:
                result = cr_Table_DKILL_SHARER[buf_Entry->trx_Info.gsm_TType];
            break;

            case RIO_PL_GSM_IKILL_HOME:
                result = cr_Table_IKILL_HOME[buf_Entry->trx_Info.gsm_TType];
            break;
            
            case RIO_PL_GSM_IKILL_SHARER:
                result = cr_Table_IKILL_SHARER[buf_Entry->trx_Info.gsm_TType];
            break;
            
            case RIO_PL_GSM_TLBIE:
            case RIO_PL_GSM_TLBSYNC:
                result = RIO_LL_COLLISION_NO_COLLISION;
        }


        if ( result == RIO_LL_COLLISION_STALL )
        {
            /* add transaction to the end of the chain of the stalled transactions */
            if ( collides_Buf_Entry->kernel_Info.stall_Tag_Valid )       
            /* there is stalled transactions */                         
            {
                /* get buffer entry for the transactions stalling others */
                stalled_Buf_Entry = get_Buffer_Entry( handle, collides_Buf_Tag );
                if ( stalled_Buf_Entry == NULL)
                {
                    RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                        collides_Buf_Entry, "internal kernel function");
                    *event = RIO_EVENT_START_NO_COLLISION;
                    return RIO_FALSE;
                }

                /* find the end of the chain of the stalled transactions */
                do 
                {
                    stalled_Buf_Entry = get_Buffer_Entry( handle, stalled_Buf_Entry->kernel_Info.stall_Tag );
                    if ( stalled_Buf_Entry == NULL)
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                            collides_Buf_Tag, "internal kernel function");
                        *event = RIO_EVENT_START_NO_COLLISION;
                        return RIO_FALSE;
                    }
                }
                while ( !stalled_Buf_Entry->kernel_Info.stall_Tag_Valid );

                stalled_Buf_Entry->kernel_Info.stall_Tag_Valid = RIO_TRUE;
                stalled_Buf_Entry->kernel_Info.stall_Tag       = buf_Tag;
            }
            else
            /* there is no stalled transactions */
            {
                collides_Buf_Entry->kernel_Info.stall_Tag_Valid = RIO_TRUE;
                collides_Buf_Entry->kernel_Info.stall_Tag       = buf_Tag;
            }

            /* fill in buffer entries */
            buf_Entry->kernel_Info.is_Stalled = RIO_TRUE;

            /* set event for the futher processing */
            *event = RIO_EVENT_START_COLLISION_STALLED;
        }
        else
        /* there is no need to stall transaction because of the resolution */
        /* is to process transaction or to send specific response packet   */
        {
            /* find out event value for the futher transaction processing */
            switch ( result )
            {
                case RIO_LL_COLLISION_ERROR_RESPONSE:
                    *event = RIO_EVENT_START_COLLISION_ERROR;
                break;

                case RIO_LL_COLLISION_RETRY_RESPONSE:
                    *event = RIO_EVENT_START_COLLISION_RETRY;
                break;

                case RIO_LL_COLLISION_NOT_OWNER_RESPONSE:
                    *event = RIO_EVENT_START_COLLISION_NOT_OWNER;
                break;

                case RIO_LL_COLLISION_NO_COLLISION:
                    *event = RIO_EVENT_START_NO_COLLISION;
                break;

                default: {}
            }
        }

        /* return value */
        return RIO_TRUE;
    }

    /* there is no need to check collision between incoming CASTOUT and the */
    /* earlier requested not outstanding transaction requests               */
    if ( buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_CASTOUT )
    {
        *event = RIO_EVENT_START_NO_COLLISION;
        return RIO_TRUE;
    }

    /* colliding outstanding transaction was not found in the inbound and outbound buffers */

    /* find colliding nonoutstanding transactions in the local buffer */
    for ( i = 0; i < handle->inst_Params.outbound_Buffer_Size; i++ )
        if ( handle->loc_Buffer[i].kernel_Info.valid &&
            is_GSM_Transaction( handle, RIO_LL_LOCAL_BUFFER_START + i ) &&
            !handle->loc_Buffer[i].trx_Info.is_Outstanding )
        {
            /* find out memory area for the transaction */
            start_Address_Old  = handle->loc_Buffer[i].trx_Info.address & granule_Align_Mask;
            finish_Address_Old = start_Address_Old + granule_Size - 1;

            /* check if there is a collision */
            if ( ext_Address && 
                buf_Entry->trx_Info.extended_Address != handle->loc_Buffer[i].trx_Info.extended_Address )
                continue;

            if ( /* handle->init_Params.xamsbs && */
                buf_Entry->trx_Info.xamsbs != handle->loc_Buffer[i].trx_Info.xamsbs )
                continue;            

            if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New) ) )
            {
                collides_Buf_Tag = RIO_LL_LOCAL_BUFFER_START + i;
                break;
            }
        }

    /* if colliding nonoutstanding transactions was not found in the local buffer try to find */
    /* such a transactions in the remote buffer                                               */
    if ( collides_Buf_Tag == 0 )
        for ( i = 0; i < handle->inst_Params.inbound_Buffer_Size; i++ )
            if ( handle->remote_Buffer[i].kernel_Info.valid &&
                is_GSM_Transaction( handle, RIO_LL_REMOTE_BUFFER_START + i ) &&
                !handle->remote_Buffer[i].trx_Info.is_Outstanding &&
                RIO_LL_REMOTE_BUFFER_START + i != buf_Tag )
            {
                /* find out memory area for the transaction */
                start_Address_Old  = handle->remote_Buffer[i].trx_Info.address & granule_Align_Mask;
                finish_Address_Old = start_Address_Old + granule_Size - 1;

                /* check if there is a collision */
                if ( ext_Address && 
                    buf_Entry->trx_Info.extended_Address != handle->remote_Buffer[i].trx_Info.extended_Address )
                    continue;

                if ( /* handle->init_Params.xamsbs && */
                    buf_Entry->trx_Info.xamsbs != handle->remote_Buffer[i].trx_Info.xamsbs )
                    continue;            

                if ( !( ( start_Address_New > finish_Address_Old ) || ( start_Address_Old > finish_Address_New) ) )
                {
                    collides_Buf_Tag = RIO_LL_REMOTE_BUFFER_START + i;
                    break;
                }
            }

    /* if colliding nonoutstanding transaction was found */
    if ( collides_Buf_Tag != 0 )
    {
        /* get buffer entry for the colliding transaction */
        if ( (collides_Buf_Entry = get_Buffer_Entry( handle, collides_Buf_Tag )) == NULL )
        {
            RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, collides_Buf_Tag, "internal kernel function");
            *event = RIO_EVENT_START_NO_COLLISION;
            return RIO_FALSE;
        }

        /* if collidiong incoming transaction TLBIE or TLBSYNC */
        /* there is no collision                               */
        if ( collides_Buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_TLBIE   ||
            collides_Buf_Entry->trx_Info.gsm_TType == RIO_PL_GSM_TLBSYNC )
        {
            *event = RIO_EVENT_START_NO_COLLISION;
            return RIO_FALSE;            
        }

        /* add transaction to the end of the chain of the stalled transactions */
        if ( collides_Buf_Entry->kernel_Info.stall_Tag_Valid )       
        /* there is stalled transactions */                         
        {
            /* get buffer entry for the transactions stalling others */
            stalled_Buf_Entry = get_Buffer_Entry( handle, collides_Buf_Tag );
            if ( stalled_Buf_Entry == NULL)
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                    collides_Buf_Tag, "internal kernel function");
                *event = RIO_EVENT_START_NO_COLLISION;
                return RIO_FALSE;
            }

            /* find the end of the chain of the stalled transactions */
            do 
            {
                stalled_Buf_Entry = get_Buffer_Entry( handle, stalled_Buf_Entry->kernel_Info.stall_Tag );
                if ( stalled_Buf_Entry == NULL)
                {
                    RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, 
                        collides_Buf_Tag, "internal kernel function");
                    *event = RIO_EVENT_START_NO_COLLISION;
                    return RIO_FALSE;
                }
            }
            while ( !stalled_Buf_Entry->kernel_Info.stall_Tag_Valid );

            stalled_Buf_Entry->kernel_Info.stall_Tag_Valid = RIO_TRUE;
            stalled_Buf_Entry->kernel_Info.stall_Tag       = buf_Tag;
        }
        else
        /* there is no stalled transactions */
        {
            collides_Buf_Entry->kernel_Info.stall_Tag_Valid = RIO_TRUE;
            collides_Buf_Entry->kernel_Info.stall_Tag       = buf_Tag;
        }

        /* fill in buffer entries */
        buf_Entry->kernel_Info.is_Stalled = RIO_TRUE;

        /* set event for the futher processing */
        *event = RIO_EVENT_START_COLLISION_STALLED;

        /* return value */
        return RIO_TRUE;                
    }

    /* there is no collision. Normal transaction processing */
    *event = RIO_EVENT_START_NO_COLLISION;
    return RIO_FALSE;
}


/***************************************************************************
 * Function :    get_Transaction_From_Temp_Buffer
 *
 * Description:  function tryes to allocate buffer for the transaction in 
 *               the temporary buffer. If it is possible function fills in
 *               all buffer fields and finds appropariate transaction 
 *               handler.
 *
 * Returns:      RIO_OK in case of success
 *               RIO_ERROR in case of error
 *               RIO_RETRY in case of anavailable buffer entry of empty 
 *                         temporary buffer.
 *
 * Notes:        in case of successfull buffer alolocation buffer tag is
 *               returned in *buf_Tag and necessary event value is returned 
 *               in *event.
 *               This function may issue error or warning messages.
 *
 **************************************************************************/
static int get_Transaction_From_Temp_Buffer( 
    RIO_LL_DS_T        *handle,
    RIO_LL_BUF_TAG_T   *buf_Tag,
    RIO_LL_TRX_EVENT_T *event
)
{
    RIO_LL_BUFFER_T *buf_Entry;
    RIO_UCHAR_T     offset = 0, byte_Num = 0, dw_Size = 0;
    RIO_BOOL_T      was_Error = RIO_FALSE;
    int             tr_Result;         /* result of the address translation for the request */
    int             i;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    int             length,check_length;
    static int      ds=0;  
    /* GDA: Bug#124 - Support for Data Streaming packet */



#ifdef _DEBUG
    assert( handle );
    assert( buf_Tag);
    assert( event );
#endif 

    /* check if the transaction present in the temporary buffer */
    if ( !handle->temp_Buffer.has_Transaction )
        return RIO_RETRY;

    /* trying to allocate buffer for transaction */
    if ( (*buf_Tag = allocate_Buffer_Entry( handle, handle->temp_Buffer.prio, RIO_FALSE )) == RIO_ERROR )
        /* there is no free entry at this time. Keep transaction in the internal buffer */
        return RIO_RETRY;

    /* buffer entry is available */
    if ( (buf_Entry = get_Buffer_Entry( handle, *buf_Tag )) == NULL )
    {
        deallocate_Buffer_Entry( handle, *buf_Tag );
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, *buf_Tag, "getting transaction from the temporary buffer");
        return RIO_ERROR;
    }

    /* acknowledge transaction to the Transport Layer */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* invoke callback */
    handle->callback_Tray.rio_TL_Ack_Remote_Req(
        handle->callback_Tray.rio_TL_Inbound_Context,
        handle->temp_Buffer.tag     
    );

    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    /* fill in _some_ transaction info buffer structure */  
    buf_Entry->trx_Info.prio        = handle->temp_Buffer.prio;
    buf_Entry->trx_Info.req_FType   = handle->temp_Buffer.format_Type;
    buf_Entry->trx_Info.req_TType   = handle->temp_Buffer.ttype;

    buf_Entry->trx_Info.sec_ID      = handle->temp_Buffer.sec_ID;
    buf_Entry->trx_Info.sec_TID     = handle->temp_Buffer.sec_TID;

    buf_Entry->trx_Info.resp_Tr_Info    = handle->temp_Buffer.transport_Info;
    buf_Entry->trx_Info.resp_Transp_Type = handle->temp_Buffer.transp_Type;

    buf_Entry->trx_Info.resp_Target_TID = handle->temp_Buffer.target_TID;
    buf_Entry->trx_Info.src_ID          = handle->temp_Buffer.src_ID;

    buf_Entry->trx_Info.letter = handle->temp_Buffer.letter;
    buf_Entry->trx_Info.mbox   = handle->temp_Buffer.mbox;
    buf_Entry->trx_Info.msgseg = handle->temp_Buffer.msgseg;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    handle->temp_Buffer.stream_ID.src_ID          = handle->temp_Buffer.src_ID;
    handle->temp_Buffer.stream_ID.flow_ID          = handle->temp_Buffer.target_TID;
    buf_Entry->trx_Info.stream_ID = handle->temp_Buffer.stream_ID;    
    /* GDA: Bug#124 - Support for Data Streaming packet */


    /* check if this transaction can be serviced */
    if ( !can_Service( handle, handle->temp_Buffer.format_Type, handle->temp_Buffer.ttype) )
    {
        RIO_LL_Message( handle, RIO_LL_WARN_CANNOT_SERVICE, handle->temp_Buffer.format_Type, handle->temp_Buffer.ttype );

        /* find error handler for the transaction */
        buf_Entry->kernel_Info.handler = find_Error_Handler(
            handle,
            handle->temp_Buffer.format_Type,
            handle->temp_Buffer.ttype
        );

        buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
        buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
        buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;

        /* mark temporary buffer as clear */
        handle->temp_Buffer.has_Transaction = RIO_FALSE;

        if (buf_Entry->kernel_Info.handler == NULL)
        {
            /* deallocate buffer entry */
            deallocate_Buffer_Entry( handle, *buf_Tag );   

            /* return value */
            return RIO_RETRY;
        }

        /* set event value */
        *event = RIO_EVENT_ERROR_CANNOT_SERVICE;
        return RIO_OK;            
    }
    
    /* check correctness of the message transaction attributes */
    if ( handle->temp_Buffer.format_Type == RIO_MESSAGE && 
        ( handle->temp_Buffer.mbox > RIO_MAX_MBOX_PER_PE ||
        handle->temp_Buffer.letter > RIO_MAX_LETTER_PER_MBOX ||
        ((handle->temp_Buffer.msgseg > handle->temp_Buffer.msglen) && (handle->temp_Buffer.msglen > 0)) ||
        ((handle->temp_Buffer.msgseg > handle->temp_Buffer.msglen) && (handle->temp_Buffer.msglen == 0) 
            && (handle->init_Params.ext_Mailbox_Support != RIO_TRUE)) ||
        handle->temp_Buffer.msglen > max_Dest_Message_Seg( handle, handle->temp_Buffer.mbox)))
    {
        /* issue error message */
        if ( handle->temp_Buffer.mbox   > RIO_MAX_MBOX_PER_PE )
            RIO_LL_Message( handle, RIO_LL_ERROR_REMOTE_MESSAGE_WRONG_PARAM, "mbox", RIO_MAX_MBOX_PER_PE );
        else if ( handle->temp_Buffer.letter > RIO_MAX_LETTER_PER_MBOX )
            RIO_LL_Message( handle, RIO_LL_ERROR_REMOTE_MESSAGE_WRONG_PARAM, "letter", RIO_MAX_LETTER_PER_MBOX );
        else if ( handle->temp_Buffer.msgseg > handle->temp_Buffer.msglen )
            RIO_LL_Message( handle, RIO_LL_ERROR_REMOTE_MESSAGE_WRONG_PARAM, "message segment", handle->temp_Buffer.msglen );
        else if ( handle->temp_Buffer.msglen > max_Dest_Message_Seg( handle, handle->temp_Buffer.mbox) )
            RIO_LL_Message( handle, RIO_LL_ERROR_REMOTE_MESSAGE_WRONG_PARAM, 
                "segments in the message", max_Dest_Message_Seg( handle, handle->temp_Buffer.mbox) );

        /* find error handler */
        buf_Entry->kernel_Info.handler = find_Error_Handler(
            handle,
            handle->temp_Buffer.format_Type,
            handle->temp_Buffer.ttype
        );

        buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
        buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
        buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;

        /* mark temporary buffer as clear */
        handle->temp_Buffer.has_Transaction = RIO_FALSE;

        /* set event value */
        *event = RIO_EVENT_ERROR_GENERAL;
        return RIO_OK;            
    }

    /* check that GSM transaction requested by the coherency domain participant */
    /* IO_READ_HOME and FLUSH_WITH_DATA are allowed from non-participant device */
    if ( is_GSM_Transaction( handle, *buf_Tag ) && 
        !( handle->temp_Buffer.format_Type == RIO_NONINTERV_REQUEST && handle->temp_Buffer.ttype == RIO_NONINTERV_IO_READ_HOME) &&
        !( handle->temp_Buffer.format_Type == RIO_WRITE && handle->temp_Buffer.ttype == RIO_WRITE_FLUSH_WITH_DATA))
    {
        for ( i = 0; i < handle->inst_Params.coh_Domain_Size - 1; i++ )        
            if ( buf_Entry->trx_Info.src_ID == handle->init_Params.remote_Dev_ID[i] )
                break;

        if ( i == handle->inst_Params.coh_Domain_Size - 1 )
        {
            /* issue error message */
            RIO_LL_Message( handle, RIO_LL_ERROR_NONPARTICIPANT_GSM, 
                buf_Entry->trx_Info.req_FType, buf_Entry->trx_Info.req_TType );

            /* find error handler */
            buf_Entry->kernel_Info.handler = find_Error_Handler(
                handle,
                handle->temp_Buffer.format_Type,
                handle->temp_Buffer.ttype
            );

            buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
            buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
            buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;

            /* mark temporary buffer as clear */
            handle->temp_Buffer.has_Transaction = RIO_FALSE;

            /* set event value */
            *event = RIO_EVENT_ERROR_GENERAL;
            return RIO_OK;            
        }
    }

    /* check if the transaction requested has correct data payload or data requested size */
    if ( (handle->temp_Buffer.format_Type == RIO_MESSAGE && check_Packet_Data_Size( 
            handle, 
            handle->temp_Buffer.format_Type,
            handle->temp_Buffer.ttype,
            handle->temp_Buffer.wdptr,
            handle->temp_Buffer.ssize,
            handle->temp_Buffer.mbox) == RIO_ERROR) ||
        (handle->temp_Buffer.format_Type != RIO_MESSAGE && check_Packet_Data_Size( 
            handle, 
            handle->temp_Buffer.format_Type,
            handle->temp_Buffer.ttype,
            handle->temp_Buffer.wdptr,
            handle->temp_Buffer.rdsize,
            handle->temp_Buffer.mbox) == RIO_ERROR) )
    {
        /* find error handler for the transaction */
        buf_Entry->kernel_Info.handler = find_Error_Handler(
            handle,
            handle->temp_Buffer.format_Type,
            handle->temp_Buffer.ttype
        );

        buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
        buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
        buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;

        /* mark temporary buffer as clear */
        handle->temp_Buffer.has_Transaction = RIO_FALSE;

        if (buf_Entry->kernel_Info.handler == NULL)
        {
            /* deallocate buffer entry */
            deallocate_Buffer_Entry( handle, *buf_Tag );   

            /* return value */
            return RIO_RETRY;
        }

        /* set event value */
        *event = RIO_EVENT_ERROR_GENERAL;
        return RIO_OK;            
    }

    /* check data payload size in the request structure */
    switch ( handle->temp_Buffer.format_Type )
    {
        case RIO_INTERV_REQUEST:
        case RIO_NONINTERV_REQUEST:
            /* nonintervention or intervention class packet arrived */
            /* translate values from the packet                     */
            translate_WDPTR_To_DW_Size( 
                handle->temp_Buffer.wdptr, 
                handle->temp_Buffer.rdsize, 
                RIO_TRUE,
                &offset, 
                &byte_Num, 
                &dw_Size 
            );

            /* for address only transactions and transaction only transactions */
            /* correct data payload size. These transactions are DKILL_HOME,   */
            /* DKILL_SHARER, IKILL_HOME, IKILL_SHARER, TLBIE, TLBSYNC and FLUSH without data */
            if ( handle->temp_Buffer.format_Type == RIO_NONINTERV_REQUEST &&
                ( handle->temp_Buffer.ttype == RIO_NONINTERV_DKILL_HOME ||
                handle->temp_Buffer.ttype == RIO_NONINTERV_DKILL_SHARER ||
                handle->temp_Buffer.ttype == RIO_NONINTERV_IKILL_HOME   ||
                handle->temp_Buffer.ttype == RIO_NONINTERV_IKILL_SHARER ||
                handle->temp_Buffer.ttype == RIO_NONINTERV_TLBIE        ||
                handle->temp_Buffer.ttype == RIO_NONINTERV_TLBSYNC      ||
                handle->temp_Buffer.ttype == RIO_NONINTERV_FLUSH_WO_DATA )
            )
            {
                /* address-only and transaction-only operations shall have 
                   rdsize and wdptr set to logic zeros */
                if(handle->temp_Buffer.wdptr != 0 || handle->temp_Buffer.rdsize != 0)
                    /* issue warning message */
                    RIO_LL_Message(handle, RIO_LL_WARN_WRONG_TRANS_ONLY_TRANSACTION);

                /* transaction-only transaction shall have address fields set to logic zeros */
                if ( (handle->temp_Buffer.ttype == RIO_NONINTERV_TLBSYNC) &&
                    ( (handle->temp_Buffer.address != 0) || (handle->temp_Buffer.xamsbs != 0)  ||
                    ( (get_Config_Reg_Field( handle, RIO_LL_EXT_ADDRESSING_SUPPORT ) == RIO_LL_PE_LL_CSR_50) &&
                        ((handle->temp_Buffer.extended_Address & RIO_EXT_ADDRESS_16_MASK) != 0)
                    ) ||
                    ( (get_Config_Reg_Field( handle, RIO_LL_EXT_ADDRESSING_SUPPORT ) == RIO_LL_PE_LL_CSR_66) &&
                        (handle->temp_Buffer.extended_Address != 0))))
                    /* issue warning message */
                    RIO_LL_Message(handle, RIO_LL_WARN_WRONG_TRANS_ONLY_TRANSACTION);
                
                offset   = 0;
                byte_Num = 0;
                dw_Size  = 0;
            }
    
            /* this type of packet shall never include data payload */
            if ( handle->temp_Buffer.dw_Num > 0 )        
            {
                /* issue error message */
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_ACTUAL_REMOTE_DATA_PAYLOAD );

                /* set error flag */
                was_Error = RIO_TRUE;
            }
        break;

        case RIO_WRITE:
            /* write  class packet arrived      */
            /* translate values from the packet */
            translate_WDPTR_To_DW_Size( 
                handle->temp_Buffer.wdptr, 
                handle->temp_Buffer.rdsize, 
                RIO_FALSE,
                &offset, 
                &byte_Num, 
                &dw_Size 
            );
            
            /* the size of the data payload shall never be bigger than */
            /* stated in the packet header                             */
            if ( handle->temp_Buffer.dw_Num > dw_Size)         
            {
                /* issue error message */
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_ACTUAL_REMOTE_DATA_PAYLOAD );
                /* set error flag */
                was_Error = RIO_TRUE;
            }

	  #ifndef POFF
	    /* GDA - capture our own data */ 
	    printf("\nFrom BFM: LL Layer");
	    for(i = 0; i < handle->temp_Buffer.dw_Num; i++){
                printf("\n\t[%d]LS_Word: D`%d;   \tMS_Word: D`%d",i, handle->temp_Buffer.data[i].ls_Word, \
                                                     	     handle->temp_Buffer.data[i].ms_Word);
            }
            printf("\n\n");
            /* GDA - capture our own data */
	  #endif

            /* transaction shall always include data payload */
            if ((handle->temp_Buffer.dw_Num == 0)  || 
		(handle->temp_Buffer.dw_Num > GDA_PKT_SIZE)) 
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_NO_DATA_PAYLOAD);
                was_Error = RIO_TRUE;
            }

            /* correct data payload size */
            dw_Size = handle->temp_Buffer.dw_Num;
        break;

        case RIO_STREAM_WRITE:
            dw_Size  = handle->temp_Buffer.dw_Num;
            offset   = 0;
            byte_Num = RIO_LL_DW_BYTE_SIZE;
            /* check that payload is present and does not exceed 32 doublewords */
            if( handle->temp_Buffer.dw_Num == 0 || handle->temp_Buffer.dw_Num > RIO_LL_MAX_PACKET_DW_SIZE )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_NO_DATA_PAYLOAD);
                was_Error = RIO_TRUE;
            }
            
        break;

        case RIO_MAINTENANCE:
            /* maintenance packet arrived       */
            /* translate values from the packet */
            translate_WDPTR_To_DW_Size( 
                handle->temp_Buffer.wdptr, 
                handle->temp_Buffer.rdsize, 
                RIO_TRUE, /* don't care since sizes are checked */
                &offset, 
                &byte_Num, 
                &dw_Size 
            );

            /* maintenance write packet data payload shall be never bigger */
            /* than stated in header but may be smaller. Maintenance read  */
            /* packet shall never has data payload                         */
            if ( ( ( (handle->temp_Buffer.ttype == RIO_MAINTENANCE_CONF_WRITE) || 
                    (handle->temp_Buffer.ttype == RIO_MAINTENANCE_PORT_WRITE) )
                && (handle->temp_Buffer.dw_Num > dw_Size))
                || ((handle->temp_Buffer.ttype == RIO_MAINTENANCE_CONF_READ)
                &&(handle->temp_Buffer.dw_Num > 0)) )
            {
                /* issue error message */
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_ACTUAL_REMOTE_DATA_PAYLOAD );

                /* set error flag */
                was_Error = RIO_TRUE;
            }

            /* correct data payload size */
            if ( ( handle->temp_Buffer.ttype == RIO_MAINTENANCE_CONF_WRITE ) ||
                ( handle->temp_Buffer.ttype == RIO_MAINTENANCE_PORT_WRITE ) )
                dw_Size = handle->temp_Buffer.dw_Num;
        break;

        case RIO_DOORBELL:
            /* doorbell packet arrived              */
            /* this packet shall never include data */
            offset   = 0;
            byte_Num = 0;
            dw_Size  = 0;

            if ( handle->temp_Buffer.dw_Num != 0 )
            {
                /* issue error message */
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_ACTUAL_REMOTE_DATA_PAYLOAD );

                /* set error flag */
                was_Error = RIO_TRUE;
            }
        break;


	/* GDA: Bug#125 - Support for Flow control packet */
        case RIO_FC:
            /* Flow Control packet arrived              */
            /* this packet shall never include data */
            offset   = 0;
            byte_Num = 0;
            dw_Size  = 0;

            if ( handle->temp_Buffer.dw_Num != 0 )
            {
                /* issue error message */
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_ACTUAL_REMOTE_DATA_PAYLOAD );

                /* set error flag */
                was_Error = RIO_TRUE;
            }
        break;
        /* GDA: Bug#125 */


        case RIO_MESSAGE:
            /* message packet arrived                */
            /* such a packet shall ever include data */
            translate_SSIZE_To_DW_Size( handle->temp_Buffer.ssize, &dw_Size );

            /* all message segments excluding last shall has data payload equal to ssize */
            /* only last segment may has smaller payload                                 */
            if ( ((handle->temp_Buffer.dw_Num != dw_Size)
                && (handle->temp_Buffer.msgseg != handle->temp_Buffer.msglen) && 
                !((handle->temp_Buffer.msglen == 0) && (handle->init_Params.ext_Mailbox_Support == RIO_TRUE)))
                || ((handle->temp_Buffer.dw_Num > dw_Size)
                && (handle->temp_Buffer.msgseg == handle->temp_Buffer.msglen)))
            {
                /* issue error message */
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_ACTUAL_REMOTE_DATA_PAYLOAD );

                /* set error flag */
                was_Error = RIO_TRUE;
            }

            /* correct data size */
            dw_Size = handle->temp_Buffer.dw_Num;
        break;
	
       /* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
		length  = handle->temp_Buffer.length;
		offset   = 0;
		/* check that payload is present and does not exceed 64k bytes */
	  #ifndef POFF
	    printf("\nFrom BFM: LL Layer (PE)");
	    for(i = 0; i < handle->temp_Buffer.hw_Num; i++)
	    {
                printf("\n\t[%d]DataStreaming data is : D`%d",i, handle->temp_Buffer.data_hw[i]);
    	       /* handle->temp_Buffer.data_ds[ds]    = handle->temp_Buffer.data_hw[i];*/
		ds++;
            }

            printf("\n\n");
	  #endif

	    if(handle->temp_Buffer.e_Ds)
	    {
		    if(handle->temp_Buffer.o !=0)
			    check_length=(ds*2)-1;
		    else
			    check_length=(ds*2);
		    if(length)
		    {
			    if(length == check_length)
				    printf("\n Length Was Matched \n");
			    else
	      	               RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_LOCAL_DATA_STREAM_PACKET, 
			            "Was not matched at reciever", length, check_length );
		    }
		    
			  
	  #ifndef POFF
		    printf("\n Reassembling the Segments \n");
	    for(i = 0; i <ds ; i++)
	    {
                printf("\n\t[%d]DataStreaming data is : D`%d",i, handle->temp_Buffer.data_ds[i]);
		handle->temp_Buffer.data_ds[i]=0;
            }
	    ds=0;

            printf("\n\n");
	  #endif
	    }

#ifndef _CARBON_SRIO_XTOR_FIX
	    // This can never happen due to limited data size of handle->temp_Buffer.length
		if( handle->temp_Buffer.length > RIO_LL_MAX_PACKET_DS_SIZE)
		{
			RIO_LL_Message( handle, RIO_LL_ERROR_NO_DATA_PAYLOAD);
			was_Error = RIO_TRUE;
		}
#endif
        break;

	/* GDA: Bug#124 - Support for Data Streaming packet */
	
    }

    if ( was_Error )
    {
        /* find error handler for the transaction */
        buf_Entry->kernel_Info.handler = find_Error_Handler(
            handle,
            handle->temp_Buffer.format_Type,
            handle->temp_Buffer.ttype
        );

        buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
        buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
        buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;

        /* mark temporary buffer as clear */
        handle->temp_Buffer.has_Transaction = RIO_FALSE;

        if (buf_Entry->kernel_Info.handler == NULL)
        {
            /* deallocate buffer entry */
            deallocate_Buffer_Entry( handle, *buf_Tag );   

            /* return value */
            return RIO_RETRY;
        }

        /* set event value */
        *event = RIO_EVENT_ERROR_GENERAL;
        return RIO_OK;            
    }


    /* transaction is correct. Normal procesing */

    /* find handler for the transation */
    buf_Entry->kernel_Info.handler = find_Handler(
        handle,
        RIO_FALSE,
        handle->temp_Buffer.format_Type,
        handle->temp_Buffer.ttype
    );

    if (buf_Entry->kernel_Info.handler == NULL)
    {
        /* issue error message */
        RIO_LL_Message( handle, RIO_LL_ERROR_REMOTE_NO_HANDLER, 
            handle->temp_Buffer.format_Type, handle->temp_Buffer.ttype);

        /* find error handler for the transaction */
        buf_Entry->kernel_Info.handler = find_Error_Handler(
            handle,
            handle->temp_Buffer.format_Type,
            handle->temp_Buffer.ttype
        );

        buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
        buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
        buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;

        /* mark temporary buffer as clear */
        handle->temp_Buffer.has_Transaction = RIO_FALSE;

        if (buf_Entry->kernel_Info.handler == NULL)
        {
            /* deallocate buffer entry */
            deallocate_Buffer_Entry( handle, *buf_Tag );   

            /* return value */
            return RIO_RETRY;
        }

        /* set event value */
        *event = RIO_EVENT_ERROR_GENERAL;
        return RIO_OK;            
    }

    /* filling in buffer kernel fields */
    buf_Entry->kernel_Info.is_Local = RIO_TRUE;

    /* fill in transaction info buffer structure */ 
    buf_Entry->trx_Info.prio        = handle->temp_Buffer.prio;
    buf_Entry->trx_Info.req_FType   = handle->temp_Buffer.format_Type;
    buf_Entry->trx_Info.req_TType   = handle->temp_Buffer.ttype;

    buf_Entry->trx_Info.address     = handle->temp_Buffer.address & RIO_LL_ADDRESS_DW_ALIGN;
    buf_Entry->trx_Info.extended_Address = handle->temp_Buffer.extended_Address;
    buf_Entry->trx_Info.xamsbs      = handle->temp_Buffer.xamsbs;
    
   /* GDA: Bug#124 - Support for Data Streaming packet added below IF Loop */
   if(buf_Entry->trx_Info.req_FType == RIO_DS)
   {		
    /* perform address translation if necessary */
    	if ( handle->callback_Tray.rio_Tr_Remote_DS != NULL && is_DS_Transaction( handle, *buf_Tag ) )
    	{
        tr_Result = handle->callback_Tray.rio_Tr_Remote_DS(
            handle->callback_Tray.address_Translation_Context
        );

        /* if the translation callback return not OK code               */
        /* so it is needed to send ERROR response                       */
        	if ( tr_Result != RIO_OK )
        	{
            /* find error handler for the transaction */
            	buf_Entry->kernel_Info.handler = find_Error_Handler(
                	handle,
                	handle->temp_Buffer.format_Type,
                	handle->temp_Buffer.ttype
            		);

            	buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
            	buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
            	buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;
            /* mark temporary buffer as clear */
            	handle->temp_Buffer.has_Transaction = RIO_FALSE;

            	if (buf_Entry->kernel_Info.handler == NULL)
            	{
                /* deallocate buffer entry */
               	deallocate_Buffer_Entry( handle, *buf_Tag );   
                /* return value */
               	return RIO_RETRY;
            	}
            /* set event value */
            	*event = RIO_EVENT_ERROR_GENERAL;
            	return RIO_OK;            
          	}
       	}
  }
   else
  {  
    /* perform address translation if necessary */
    if ( handle->callback_Tray.rio_Tr_Remote_IO != NULL && is_IO_Transaction( handle, *buf_Tag ) )
    {
        tr_Result = handle->callback_Tray.rio_Tr_Remote_IO(
            handle->callback_Tray.address_Translation_Context,
            &(buf_Entry->trx_Info.address),
            &(buf_Entry->trx_Info.extended_Address),
            &(buf_Entry->trx_Info.xamsbs)
        );

        /* if the translation callback return not OK code               */
        /* it means that there is no memory for the specified addresses */
        /* so it is needed to send ERROR response                       */
        if ( tr_Result != RIO_OK )
        {
            /* find error handler for the transaction */
            buf_Entry->kernel_Info.handler = find_Error_Handler(
                handle,
                handle->temp_Buffer.format_Type,
                handle->temp_Buffer.ttype
            );

            buf_Entry->kernel_Info.is_Stalled  = RIO_FALSE;
            buf_Entry->kernel_Info.is_Local    = RIO_TRUE;
            buf_Entry->trx_Info.state          = RIO_LL_STATE_INIT;

            /* mark temporary buffer as clear */
            handle->temp_Buffer.has_Transaction = RIO_FALSE;

            if (buf_Entry->kernel_Info.handler == NULL)
            {
                /* deallocate buffer entry */
                deallocate_Buffer_Entry( handle, *buf_Tag );   

                /* return value */
                return RIO_RETRY;
            }

            /* set event value */
            *event = RIO_EVENT_ERROR_GENERAL;
            return RIO_OK;            
        }

    }
}
/* GDA: Bug#124 - Support for Data Streaming packet */


    buf_Entry->trx_Info.dw_Size     = dw_Size;      
    buf_Entry->trx_Info.offset      = offset;   
    buf_Entry->trx_Info.byte_Num    = byte_Num; 
    /* GDA: Bug#124 - Support for Data Streaming packet */
    /* Bcoz of this it is giving segmentation problems at snoop_data ..*/
    /*  for ( i = 0; i < dw_Size; i++)
        buf_Entry->trx_Info.data[i] = handle->temp_Buffer.data[i];*/
    /* GDA: Bug#124 - Support for Data Streaming packet */
	
    buf_Entry->trx_Info.dw_Num      = handle->temp_Buffer.dw_Num;
 
    buf_Entry->trx_Info.sec_ID      = handle->temp_Buffer.sec_ID;
    buf_Entry->trx_Info.sec_TID     = handle->temp_Buffer.sec_TID;

    buf_Entry->trx_Info.mbox        = handle->temp_Buffer.mbox;       
    buf_Entry->trx_Info.letter      = handle->temp_Buffer.letter;
    buf_Entry->trx_Info.msgseg      = handle->temp_Buffer.msgseg;
    buf_Entry->trx_Info.ssize       = handle->temp_Buffer.ssize;
    buf_Entry->trx_Info.msglen      = handle->temp_Buffer.msglen;

    buf_Entry->trx_Info.doorbell_Info = handle->temp_Buffer.doorbell_Info;
    buf_Entry->trx_Info.conf_Offset = handle->temp_Buffer.offset;  

    buf_Entry->trx_Info.resp_Tr_Info    = handle->temp_Buffer.transport_Info;
    buf_Entry->trx_Info.resp_Transp_Type = handle->temp_Buffer.transp_Type;
    
    buf_Entry->trx_Info.resp_Target_TID = handle->temp_Buffer.target_TID;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    buf_Entry->trx_Info.cos      = handle->temp_Buffer.cos;       
    buf_Entry->trx_Info.s_Ds     = handle->temp_Buffer.s_Ds;       
    buf_Entry->trx_Info.e_Ds     = handle->temp_Buffer.e_Ds;       
    buf_Entry->trx_Info.length   = handle->temp_Buffer.length;       
    buf_Entry->trx_Info.o        = handle->temp_Buffer.o;       
    buf_Entry->trx_Info.p        = handle->temp_Buffer.p;       
    handle->temp_Buffer.stream_ID.src_ID   = handle->temp_Buffer.src_ID;
    handle->temp_Buffer.stream_ID.flow_ID  = handle->temp_Buffer.target_TID;
    buf_Entry->trx_Info.stream_ID          = handle->temp_Buffer.stream_ID;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    buf_Entry->trx_Info.x_On_Off    = handle->temp_Buffer.x_On_Off;
    buf_Entry->trx_Info.flow_ID     = handle->temp_Buffer.flow_ID;
    buf_Entry->trx_Info.tgtdest_ID  = handle->temp_Buffer.tgtdest_ID;
    buf_Entry->trx_Info.soc         = handle->temp_Buffer.soc;
    /* GDA: Bug#125
    printf("\n xonoff is %d\n",handle->temp_Buffer.x_On_Off);*/

    /* convert FType and TType into the RIO_PL_GSM_TTYPE_T in case of the GSM */
    /* transactions                                                           */
    if ( is_GSM_Transaction( handle, *buf_Tag ) )
    {
        switch( buf_Entry->trx_Info.req_FType )
        {
            case RIO_INTERV_REQUEST:
            {
                switch ( buf_Entry->trx_Info.req_TType)
                {
                    case RIO_INTERV_READ_OWNER:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_READ_OWNER;
                    break;

                    case RIO_INTERV_READ_TO_OWN_OWNER:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_READ_TO_OWN_OWNER;
                    break;

                    case RIO_INTERV_IO_READ_OWNER:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_IO_READ_OWNER;
                    break;
                }
                break;
            }

            case RIO_NONINTERV_REQUEST:
            {
                switch ( buf_Entry->trx_Info.req_TType)
                {
                    case RIO_NONINTERV_READ_HOME:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_READ_HOME;
                    break;

                    case RIO_NONINTERV_READ_TO_OWN_HOME:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_READ_TO_OWN_HOME;
                    break;

                    case RIO_NONINTERV_IO_READ_HOME:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_IO_READ_HOME;
                    break;

                    case RIO_NONINTERV_DKILL_HOME:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_DKILL_HOME;
                    break;

                    case RIO_NONINTERV_IKILL_HOME:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_IKILL_HOME;
                    break;

                    case RIO_NONINTERV_TLBIE:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_TLBIE;
                    break;

                    case RIO_NONINTERV_TLBSYNC:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_TLBSYNC;
                    break;

                    case RIO_NONINTERV_IREAD_HOME:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_IREAD_HOME;
                    break;

                    case RIO_NONINTERV_FLUSH_WO_DATA:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_FLUSH_WO_DATA;
                    break;

                    case RIO_NONINTERV_IKILL_SHARER:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_IKILL_SHARER;
                    break;

                    case RIO_NONINTERV_DKILL_SHARER:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_DKILL_SHARER;
                    break;
                }
                break;
            }

            case RIO_WRITE:
            {
                switch( buf_Entry->trx_Info.req_TType ) 
                {
                    case RIO_WRITE_CASTOUT:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_CASTOUT;
                    break;

                    case RIO_WRITE_FLUSH_WITH_DATA:
                        buf_Entry->trx_Info.gsm_TType = RIO_PL_GSM_FLUSH_WITH_DATA;
                    break;
                }
                break;
            }

            default: {}
        }
    }

    /* set transaction state */
    buf_Entry->trx_Info.state       = RIO_LL_STATE_INIT;

    /* mark temporary buffer as clear */
    handle->temp_Buffer.has_Transaction = RIO_FALSE;

    /* check if the transaction shall be stalled */
    if ( is_IO_Transaction( handle, *buf_Tag ) )
        if ( is_IO_Remote_Collision( handle, *buf_Tag ) )
            *event = RIO_EVENT_START_COLLISION_STALLED;
        else 
            *event = RIO_EVENT_START_NO_COLLISION;

    else if ( is_GSM_Transaction( handle, *buf_Tag ) )
        is_GSM_Remote_Collision( handle, *buf_Tag, event );    

    else
        *event = RIO_EVENT_START_NO_COLLISION;

    return RIO_OK;
}


/***************************************************************************
 * Function :    get_Queue_Element_From_Pool
 *
 * Description:  this function returns pointer to the queue element from the 
 *               needed pool of the queue elements                    
 *
 * Returns:      pointer to the queue.
 *               NUL pointer can not be returned because it is error to try 
 *               to get element from empty pool.
 *
 * Notes:        can modify contents of the pool head pointer supported in the
 *               pool argument by reference
 *
 **************************************************************************/
static RIO_LL_QUEUE_T* get_Queue_Element_From_Pool( RIO_LL_QUEUE_T **pool)
{
    RIO_LL_QUEUE_T *element;

#ifdef _DEBUG
    assert(pool);
    assert(*pool); /* because it is error to try to get element from empty pool */
#endif

    element = *pool;
    *pool = element->next;

    return element;
}

/***************************************************************************
 * Function :    return_Queue_Element_To_Pool
 *
 * Description:  this function returns queue element to the appropriate pool 
 *               identified by the head pointer                       
 *
 * Returns:      RIO_OK in case of success or RIO_ERROR in another case
 *
 * Notes:        can modify contents of the pool head pointer supported by 
 *               reference in the head argument
 *
 **************************************************************************/
static int return_Queue_Element_To_Pool( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **pool)
{
#ifdef _DEBUG
    assert (element);
    assert (pool);
#endif

    element->next = *pool;
    *pool = element;

    return RIO_OK;
}

/***************************************************************************
 * Function :    change_Queue_Elemen_State
 *
 * Description:  this function changes queue element state to the new one 
 *
 * Returns:      RIO_OK in case of success or RIO_ERROR in another case
 *
 * Notes:
 *
 **************************************************************************/
static int change_Queue_Element_State( RIO_LL_QUEUE_T *element, RIO_LL_Q_EL_STATE_T new_State)
{
#ifdef _DEBUG
    assert (element);
#endif

    element->current_State = new_State;

    return RIO_OK;
}

/***************************************************************************
 * Function :    insert_Element_To_Queue_Prio
 *
 * Description:  this function insertss queue element into the appropriate 
 *               queue identified by the head pointer supported by reference 
 *               according to element's priority
 *
 * Returns:      RIO_OK in case of success or RIO_ERROR in another case
 *
 * Notes:        can modify contents of the queue head pointer supported by 
 *               reference in the head argument
 *
 **************************************************************************/
static int insert_Element_To_Queue_Prio( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **head)
{
    RIO_LL_QUEUE_T **current;

#ifdef _DEBUG
    assert (element);
    assert (head);
#endif

    for (current = head; (*current != NULL) && ((*current)->prio >= element->prio);
        current = &((*current)->next))
        ;

    element->next = *current;
    *current = element;

    return RIO_OK;
}

/***************************************************************************
 * Function :    insert_Element_To_Queue_End
 *
 * Description:  this function inserts queue element into end of the 
 *               appropriate queue identified by the head pointer supported 
 *               by reference regardless of element's priority
 *
 * Returns:      RIO_OK in case of success or RIO_ERROR in another case
 *
 * Notes:        can modify contents of the queue head pointer supported by 
 *               reference in the head argument
 *
 **************************************************************************/
static int insert_Element_To_Queue_End( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **head)
{
    RIO_LL_QUEUE_T **current;

#ifdef _DEBUG
    assert (element);
    assert (head);
#endif

    for (current = head; *current != NULL; current = &((*current)->next))
        ;

    element->next = *current;
    *current = element;

    return RIO_OK;
}

/***************************************************************************
 * Function :    remove_Element_From_Queue
 *
 * Description:  this function removes queue element from the appropriate 
 *               queue identified by the head pointer supported by reference 
 *
 * Returns:      RIO_OK in case of success or RIO_ERROR in another case
 *
 * Notes:        can modify contents of the queue head pointer supported by 
 *               reference in the head argument
 *
 **************************************************************************/
static int remove_Element_From_Queue( RIO_LL_QUEUE_T *element, RIO_LL_QUEUE_T **head)
{
    RIO_LL_QUEUE_T **current;

#ifdef _DEBUG
    assert (element);
    assert (head);
#endif

    for (current = head; (*current != NULL) && (*current != element); current = &((*current)->next))
        ;

    if (*current == NULL)
        return RIO_ERROR;

    *current = element->next;

    return RIO_OK;
}

/***************************************************************************
 * Function :    find_Queue_Element_With_State
 *
 * Description:  this function returns pointer to the queue element having  
 *               given state. The queue in which the element is searched 
 *               is defined by the start pointer.                                          
 *
 * Returns:      pointer to the queue element or NULL pointer
 *
 * Notes:        The queue may be empty
 *
 **************************************************************************/
static RIO_LL_QUEUE_T* find_Queue_Element_With_State( RIO_LL_QUEUE_T *start, RIO_LL_Q_EL_STATE_T state)
{
    RIO_LL_QUEUE_T *current;

    for (current = start; (current != NULL) && (current->current_State != state); current = current->next)
        ;

    return current;
}

/***************************************************************************
 * Function :    is_In_Reset
 *
 * Description:  this function returns RIO_TRUE if reset in progress, 
 *               RIO_FALSE overwise 
 *
 * Returns:      
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_BOOL_T is_In_Reset(const RIO_LL_DS_T *handle)
{
#ifdef _DEBUG
    assert(handle);
#endif
    
    return handle->in_Reset;
}


/***************************************************************************
 * Function :    add_Semaphor_State
 *
 * Description:  adds supplied state mask to the semaphor state
 *
 * Returns:      nothing
 *
 * Notes:       
 *
 **************************************************************************/
static void add_Semaphor_State( RIO_LL_DS_T *handle, RIO_LL_SEMAPHOR_T state)
{
#ifdef _DEBUG
    assert(handle);
#endif

    handle->semaphor = handle->semaphor | state;
}

/***************************************************************************
 * Function :    remove_Semaphor_State
 *
 * Description:  removes supplied state mask from the semaphor state
 *
 * Returns:      nothing
 *
 * Notes:       
 *
 **************************************************************************/
static void remove_Semaphor_State( RIO_LL_DS_T *handle, RIO_LL_SEMAPHOR_T state)
{
#ifdef _DEBUG
    assert(handle);
#endif

    handle->semaphor = ~( ~handle->semaphor | state);
}

/***************************************************************************
 * Function :    check_Semaphor_State
 *
 * Description:  checks if the semaphor has supplied state added
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_BOOL_T check_Semaphor_State( RIO_LL_DS_T *handle, RIO_LL_SEMAPHOR_T state)
{
#ifdef _DEBUG
    assert(handle);
#endif

    if ((state == RIO_LL_SEMAPHOR_INIT) && (handle->semaphor == RIO_LL_SEMAPHOR_INIT))
        return RIO_TRUE;
    else if ( (handle->semaphor & state) == state)
        return RIO_TRUE;
    else 
        return RIO_FALSE;       
}

/***************************************************************************
 * Function :    find_Handler
 *
 * Description:  tries to found handler for transaction specified by ttype 
 *               and ftype. In case of locally requested transaction 
 *               (is_Local == RIO_TRUE) ftype must be from RIO_FTYPE_T and
 *               ttype from RIO_*_TTYPE_T. In case of remote transaction 
 *               ftype and ttype are defined accordingly to "RIO interconnect"
 *               specification.
 *
 * Returns:      pointer to the appropriate handler if found and NULL if  
 *               not
 *
 * Notes:
 *
 **************************************************************************/
static RIO_LL_TRX_HANDLER_T find_Handler( RIO_LL_DS_T *handle, RIO_BOOL_T is_Local, RIO_LL_FTYPE_T ftype, RIO_CONF_TTYPE_T ttype)
{
    RIO_LL_HANDLER_TABLE_T *handlers;
    RIO_LL_HANDLER_NAMES_T needed_Name;
    int i;

#ifdef _DEBUG
    assert( handle );
#endif

    handlers = handle->handler_Table;

    switch ( is_Local )
    {
        case RIO_TRUE:
        {
            switch ( ftype )
            {
                case RIO_LL_GSM:
                {
                    switch ( (RIO_GSM_TTYPE_T)ttype )
                    {
                        case RIO_GSM_READ_SHARED:
                            needed_Name = RIO_LL_LOCAL_GSM_READ_HOME;
                        break;
                        case RIO_GSM_READ_TO_OWN:
                            needed_Name = RIO_LL_LOCAL_GSM_READ_TO_OWN_HOME;
                        break;
                        case RIO_GSM_IO_READ:
                            needed_Name = RIO_LL_LOCAL_GSM_IO_READ_HOME;
                        break;
                        case RIO_GSM_DC_I:
                            needed_Name = RIO_LL_LOCAL_GSM_DKILL_HOME;
                        break;
                        case RIO_GSM_INSTR_READ:
                            needed_Name = RIO_LL_LOCAL_GSM_IREAD_HOME;
                        break;
                        case RIO_GSM_IC_I:
                            needed_Name = RIO_LL_LOCAL_GSM_IKILL;
                        break;
                        case RIO_GSM_TLB_IE:
                            needed_Name = RIO_LL_LOCAL_GSM_TLBIE;
                        break;
                        case RIO_GSM_TLB_SYNC:
                            needed_Name = RIO_LL_LOCAL_GSM_TLBSYNC;
                        break;
                        case RIO_GSM_CASTOUT:
                            needed_Name = RIO_LL_LOCAL_GSM_CASTOUT;
                        break;
                        case RIO_GSM_FLUSH:
                            needed_Name = RIO_LL_LOCAL_GSM_FLUSH;
                        break;
                        default:
                            needed_Name = RIO_LL_END_TABLE;
                        break;
                    }
                    break;
                } 
                case RIO_LL_IO:
                {
                    switch ( ttype )
                    {
                        case RIO_IO_NREAD:
                            needed_Name = RIO_LL_LOCAL_IO_NREAD;
                        break;
                        case RIO_IO_NWRITE:
                            needed_Name = RIO_LL_LOCAL_IO_NWRITE;
                        break;
                        case RIO_IO_SWRITE:
                            needed_Name = RIO_LL_LOCAL_IO_SWRITE;
                        break;
                        case RIO_IO_NWRITE_R:
                            needed_Name = RIO_LL_LOCAL_IO_NWRITE_R;
                        break;
                        case RIO_IO_ATOMIC_INC:
                            needed_Name = RIO_LL_LOCAL_IO_ATOMIC_INC;
                        break;
                        case RIO_IO_ATOMIC_DEC:
                            needed_Name = RIO_LL_LOCAL_IO_ATOMIC_DEC;
                        break;
                        case RIO_IO_ATOMIC_SET:
                            needed_Name = RIO_LL_LOCAL_IO_ATOMIC_SET;
                        break;
                        case RIO_IO_ATOMIC_CLEAR:
                            needed_Name = RIO_LL_LOCAL_IO_ATOMIC_CLEAR;
                        break;
                        case RIO_IO_ATOMIC_TSWAP:
                            needed_Name = RIO_LL_LOCAL_IO_ATOMIC_TSWAP;
                        break;
			/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                        case RIO_IO_ATOMIC_SWAP:
                            needed_Name = RIO_LL_LOCAL_IO_ATOMIC_SWAP;
                        break;


                        default:
                            needed_Name = RIO_LL_END_TABLE;
                        break;
                    }
                    break;
                }
                case RIO_LL_CONF:
                {
                    switch ( (RIO_CONF_TTYPE_T)ttype )
                    {
                        case RIO_CONF_READ:
                            needed_Name = RIO_LL_LOCAL_CONF_READ;
                        break;
                        case RIO_CONF_WRITE:
                            needed_Name = RIO_LL_LOCAL_CONF_WRITE;
                        break;
                        case RIO_CONF_PORT_WRITE:
                            needed_Name = RIO_LL_LOCAL_CONF_PORT_WRITE;
                        break;
                        default:
                            needed_Name = RIO_LL_END_TABLE;
                        break;
                    }
                    break;
                }
                case RIO_LL_MESSAGE:
                    needed_Name = RIO_LL_LOCAL_MP;
                break;
                case RIO_LL_DOORBELL:
                    needed_Name = RIO_LL_LOCAL_DOORBELL;
                break;
		
		/* GDA: Bug#124 - Support for Data Streaming packet */
	        case RIO_LL_DS:
		   needed_Name = RIO_LL_LOCAL_DS;  /*25th location in the table*/
		break;
		/* GDA: Bug#124 */

		/* GDA: Bug#125 - Support for Flow control packet */
                case RIO_LL_FC:
                    needed_Name = RIO_LL_LOCAL_FC;
                break;
		/* GDA: Bug#125 */
	
                default:
                    needed_Name = RIO_LL_END_TABLE;
                break;
            }
            break;
        }
        case RIO_FALSE:
        {
            switch ( (RIO_FTYPE_T)ftype )
            {
                case RIO_INTERV_REQUEST:
                {
                    switch ( (RIO_INTERV_TTYPE_T)ttype )
                    {
                        case RIO_INTERV_READ_OWNER:
                            needed_Name = RIO_LL_REMOTE_GSM_READ_OWNER; 
                        break;
                        case RIO_INTERV_READ_TO_OWN_OWNER:
                            needed_Name = RIO_LL_REMOTE_GSM_READ_TO_OWN_OWNER;
                        break;
                        case RIO_INTERV_IO_READ_OWNER:
                            needed_Name = RIO_LL_REMOTE_GSM_IO_READ_OWNER;
                        break;
                        default:
                            needed_Name = RIO_LL_END_TABLE;
                        break;
                    }
                    break;
                }
                case RIO_NONINTERV_REQUEST:
                {
                    switch ( (RIO_NONINTERV_TTYPE_T)ttype )
                    {
                        case RIO_NONINTERV_READ_HOME:
                            needed_Name = RIO_LL_REMOTE_GSM_READ_HOME;
                        break;
                        case RIO_NONINTERV_READ_TO_OWN_HOME:
                            needed_Name = RIO_LL_REMOTE_GSM_READ_TO_OWN_HOME;
                        break;
                        case RIO_NONINTERV_IO_READ_HOME:
                            needed_Name = RIO_LL_REMOTE_GSM_IO_READ_HOME;
                        break;
                        case RIO_NONINTERV_DKILL_HOME:
                            needed_Name = RIO_LL_REMOTE_GSM_DKILL_HOME;
                        break;
                        case RIO_NONINTERV_NREAD:
                            needed_Name = RIO_LL_REMOTE_IO_NREAD;
                        break;
                        case RIO_NONINTERV_IKILL_HOME:
                            needed_Name = RIO_LL_REMOTE_GSM_IKILL_HOME;
                        break;
                        case RIO_NONINTERV_TLBIE:
                            needed_Name = RIO_LL_REMOTE_GSM_TLBIE;
                        break;
                        case RIO_NONINTERV_TLBSYNC:
                            needed_Name = RIO_LL_REMOTE_GSM_TLBSYNC;
                        break;
                        case RIO_NONINTERV_IREAD_HOME:
                            needed_Name = RIO_LL_REMOTE_GSM_IREAD_HOME;
                        break;
                        case RIO_NONINTERV_FLUSH_WO_DATA:
                            needed_Name = RIO_LL_REMOTE_GSM_FLUSH;
                        break;
                        case RIO_NONINTERV_IKILL_SHARER:
                            needed_Name = RIO_LL_REMOTE_GSM_IKILL_SHARER;
                        break;
                        case RIO_NONINTERV_DKILL_SHARER:
                            needed_Name = RIO_LL_REMOTE_GSM_DKILL_SHARER;
                        break;
                        case RIO_NONINTERV_ATOMIC_INC:
                            needed_Name = RIO_LL_REMOTE_IO_ATOMIC_INC;
                        break;
                        case RIO_NONINTERV_ATOMIC_DEC:
                            needed_Name = RIO_LL_REMOTE_IO_ATOMIC_DEC;
                        break;
                        case RIO_NONINTERV_ATOMIC_SET:
                            needed_Name = RIO_LL_REMOTE_IO_ATOMIC_SET;
                        break;
                        case RIO_NONINTERV_ATOMIC_CLEAR:
                            needed_Name = RIO_LL_REMOTE_IO_ATOMIC_CLEAR;
                        break;
                        default:
                            needed_Name = RIO_LL_END_TABLE;
                        break;
                    }
                    break;
                }
                case RIO_WRITE:
                {
                    switch ( (RIO_WRITE_TTYPE_T)ttype )
                    {
                        case RIO_WRITE_CASTOUT:
                            needed_Name = RIO_LL_REMOTE_GSM_CASTOUT;
                        break;
                        case RIO_WRITE_FLUSH_WITH_DATA:
                            needed_Name = RIO_LL_REMOTE_GSM_FLUSH;
                        break;
                        case RIO_WRITE_NWRITE:
                            needed_Name = RIO_LL_REMOTE_IO_NWRITE;
                        break;
                        case RIO_WRITE_NWRITE_R:
                            needed_Name = RIO_LL_REMOTE_IO_NWRITE_R;
                        break;
                        case RIO_WRITE_ATOMIC_TSWAP:
                            needed_Name = RIO_LL_REMOTE_IO_ATOMIC_TSWAP;
                        break;
 			/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                        case RIO_WRITE_ATOMIC_SWAP:
                            needed_Name = RIO_LL_REMOTE_IO_ATOMIC_SWAP;
                        break;
                       default:
                            needed_Name = RIO_LL_END_TABLE;
                        break;
                    }
                    break;
                }
                case RIO_STREAM_WRITE:
                    needed_Name = RIO_LL_REMOTE_IO_SWRITE;
                break;
		/* GDA: Bug#124 - Support for Data Streaming packet */
                case RIO_DS:
                    needed_Name = RIO_LL_REMOTE_DS;
                break;
		/* GDA: Bug#124 */

		/* GDA: Bug#125 - Support for Flow control packet */
                case RIO_FC:
                    needed_Name = RIO_LL_REMOTE_FC;
                break;
		/* GDA: Bug#125 */

                case RIO_MAINTENANCE:
                {
                    switch ( (RIO_MAINTENANCE_TTYPE_T)ttype )
                    {
                        case RIO_MAINTENANCE_CONF_READ:
                            needed_Name = RIO_LL_REMOTE_CONF_READ;
                        break;
                        case RIO_MAINTENANCE_CONF_WRITE:
                            needed_Name = RIO_LL_REMOTE_CONF_WRITE;
                        break;
                        case RIO_MAINTENANCE_PORT_WRITE:
                            needed_Name = RIO_LL_REMOTE_CONF_PORT_WRITE;
                        break;
                        default:
                            needed_Name = RIO_LL_END_TABLE;
                        break;
                    }
                    break;
                }
                case RIO_DOORBELL:
                    needed_Name = RIO_LL_REMOTE_DOORBELL;
                break;
                case RIO_MESSAGE:
                    needed_Name = RIO_LL_REMOTE_MP;
                break;
                default:
                    needed_Name = RIO_LL_END_TABLE;
                break;
            }
            break;
        }
        default:
            needed_Name = RIO_LL_END_TABLE;
        break;
    }

    if ( needed_Name == RIO_LL_END_TABLE )
        return NULL;

    for (i = 0; handlers[i].name != needed_Name; i++) 
        ;

    if ( handlers[i].name == RIO_LL_END_TABLE )
        return NULL;

    return handlers[i].func;
}

/***************************************************************************
 * Function :    find_Error_Handler
 *
 * Description:  function finds handler for the errorneous remotely 
 *               requested transaction. If transaction is of the unknown
 *               ftype and ttype the general error handler will be assigned
 *
 * Returns:      pointer to the appropriate handler if found (also in case
 *               of the unknown transaction), NULL if not (in case of the 
 *               IO SWrite or the IO NWrite transactions because these 
 *               transactions can be dropped in case of the errorneous 
 *               request)
 *
 * Notes:
 *
 **************************************************************************/
static RIO_LL_TRX_HANDLER_T find_Error_Handler( 
    RIO_LL_DS_T *handle, 
    RIO_UCHAR_T ftype, 
    RIO_UCHAR_T ttype
)
{
    RIO_LL_HANDLER_TABLE_T *handlers;
    RIO_LL_HANDLER_NAMES_T needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
    int i;

#ifdef _DEBUG
    assert( handle );
#endif

    handlers = handle->handler_Table;

    switch ( (RIO_FTYPE_T)ftype )
    {
        case RIO_INTERV_REQUEST:
            needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
        break;
        case RIO_NONINTERV_REQUEST:
            needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
        break;
        case RIO_WRITE:
        {
            switch ( (RIO_WRITE_TTYPE_T)ttype )
            {
                case RIO_WRITE_CASTOUT:
                case RIO_WRITE_FLUSH_WITH_DATA:
                    needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
                break;
                case RIO_WRITE_NWRITE:
                    needed_Name = RIO_LL_END_TABLE;
                break;
                case RIO_WRITE_NWRITE_R:
		/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                case RIO_WRITE_ATOMIC_SWAP:
                case RIO_WRITE_ATOMIC_TSWAP:
                    needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
                break;
            }
            break;
        }
        case RIO_STREAM_WRITE:
            needed_Name = RIO_LL_END_TABLE;
        break;
	/* GDA: Bug#124 - Support for Data Streaming packet */
   	case RIO_DS:
	    needed_Name = RIO_LL_END_TABLE;
	break;
	/* GDA: Bug#124 */

	/* GDA: Bug#125 - Support for Flow control packet */
        case RIO_FC:
            needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
        break;
	/* GDA: Bug#125 */

        case RIO_MAINTENANCE:
        {
            switch ( (RIO_MAINTENANCE_TTYPE_T)ttype )
            {
                case RIO_MAINTENANCE_CONF_READ:
                    needed_Name = RIO_LL_REMOTE_CONF_READ_ERROR;
                break;
                case RIO_MAINTENANCE_CONF_WRITE:
                    needed_Name = RIO_LL_REMOTE_CONF_WRITE_ERROR;
                break;
                case RIO_MAINTENANCE_PORT_WRITE:
                    needed_Name = RIO_LL_END_TABLE;
                break;

                default: {}
            }
            break;
        }

        case RIO_DOORBELL:
            needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
        break;
        case RIO_MESSAGE:
            needed_Name = RIO_LL_REMOTE_MESSAGE_ERROR;
        break;
        default:
            needed_Name = RIO_LL_REMOTE_GENERAL_ERROR;
    }

    if ( needed_Name == RIO_LL_END_TABLE )
        return NULL;

    for (i = 0; handlers[i].name != needed_Name; i++) 
        ;

    if ( handlers[i].name == RIO_LL_END_TABLE )
        return NULL;

    return handlers[i].func;
}

/***************************************************************************
 * Function :    can_Service
 *
 * Description:  function checks if LL can service remotely requested 
 *               transaction with given ftype and ttype. ftype and ttype
 *               shall be defined accordingly to the "RIO Interconnect"
 *               specification.
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_BOOL_T can_Service( RIO_LL_DS_T *handle, RIO_FTYPE_T ftype, RIO_UCHAR_T ttype )
{
    RIO_WORD_T mask;
    RIO_WORD_T dest_Trx;

#ifdef _DEBUG
    assert( handle ); 
#endif

    switch ( ftype )
    {
        case RIO_INTERV_REQUEST:
        {
            switch ( (RIO_INTERV_TTYPE_T)ttype )
            {
                case RIO_INTERV_READ_OWNER:
                    return RIO_TRUE;
                break;
                case RIO_INTERV_READ_TO_OWN_OWNER:
                    return RIO_TRUE;
                break;
                case RIO_INTERV_IO_READ_OWNER:
                    return RIO_TRUE;
                break;
                default:
                    mask = 0;
                break;
            }
            break;
        }
        case RIO_NONINTERV_REQUEST:
        {
            switch ( (RIO_NONINTERV_TTYPE_T)ttype )
            {
                case RIO_NONINTERV_READ_HOME:
                    mask = RIO_MASK_READ;
                break;
                case RIO_NONINTERV_READ_TO_OWN_HOME:
                    mask = RIO_MASK_READ_TO_OWN;
                break;
                case RIO_NONINTERV_IO_READ_HOME:
                    mask = RIO_MASK_IO_READ;
                break;
                case RIO_NONINTERV_DKILL_HOME:
                    mask = RIO_MASK_DKILL;
                break;
                case RIO_NONINTERV_NREAD:
                    mask = RIO_MASK_NREAD;
                break;
                case RIO_NONINTERV_IKILL_HOME:
                    mask = RIO_MASK_IKILL;
                break;
                case RIO_NONINTERV_TLBIE:
                    mask = RIO_MASK_TLBIE;
                break;
                case RIO_NONINTERV_TLBSYNC:
                    mask = RIO_MASK_TLBSYNC;
                break;
                case RIO_NONINTERV_IREAD_HOME:
                    mask = RIO_MASK_IREAD;
                break;
                case RIO_NONINTERV_FLUSH_WO_DATA:
                    mask = RIO_MASK_FLUSH;
                break;
                case RIO_NONINTERV_DKILL_SHARER:
                    return RIO_TRUE;
                break;
                case RIO_NONINTERV_IKILL_SHARER:
                    return RIO_TRUE;
                break;
                case RIO_NONINTERV_ATOMIC_INC:
                    mask = RIO_MASK_ATOMIC_INC;
                break;
                case RIO_NONINTERV_ATOMIC_DEC:
                    mask = RIO_MASK_ATOMIC_DEC;
                break;
                case RIO_NONINTERV_ATOMIC_SET:
                    mask = RIO_MASK_ATOMIC_SET;
                break;
                case RIO_NONINTERV_ATOMIC_CLEAR:
                    mask = RIO_MASK_ATOMIC_CLEAR;
                break;
                default:
                    mask = 0;
                break;
            }
            break;
        }
        case RIO_WRITE:
        {
            switch ( (RIO_WRITE_TTYPE_T)ttype )
            {
                case RIO_WRITE_CASTOUT:
                    mask = RIO_MASK_CASTOUT;
                break;
                case RIO_WRITE_FLUSH_WITH_DATA:
                    mask = RIO_MASK_FLUSH;
                break;
                case RIO_WRITE_NWRITE:
                    mask = RIO_MASK_NWRITE;
                break;
                case RIO_WRITE_NWRITE_R:
                    mask = RIO_MASK_NWRITE_R;
                break;
		/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                case RIO_WRITE_ATOMIC_SWAP:
                    mask = RIO_MASK_ATOMIC_SWAP;
                break;
                case RIO_WRITE_ATOMIC_TSWAP:
                    mask = RIO_MASK_ATOMIC_TSWAP;
                break;
                default:
                    mask = 0;
                break;
            }
            break;
        }
        case RIO_STREAM_WRITE:
            mask = RIO_MASK_SWRITE;
        break;

	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	    mask = RIO_MASK_DS;
	break;
	/* GDA: Bug#124 */

	/* GDA: Bug#125 - Support for Flow control packet */
        case RIO_FC:
            mask = RIO_MASK_FC;
        break;
	/* GDA: Bug#125 */

        case RIO_MAINTENANCE:
        {
            switch ( (RIO_MAINTENANCE_TTYPE_T)ttype )
            {
                case RIO_MAINTENANCE_CONF_READ:
                    return RIO_TRUE;
                break;
                case RIO_MAINTENANCE_CONF_WRITE:
                    return RIO_TRUE;
                break;
                case RIO_MAINTENANCE_PORT_WRITE:
                    mask = RIO_MASK_PORT_WRITE;
                break;
                default:
                    mask = 0;
                break;
            }
            break;
        }
        case RIO_DOORBELL:
            mask = RIO_MASK_DOORBELL;
        break;
        case RIO_MESSAGE:
            mask = RIO_MASK_MESSAGE;
        break;
        default:
            mask = 0;
        break;
    }

    if (mask == 0)
        return RIO_FALSE;

    handle->callback_Tray.rio_Get_Config_Reg(
        handle->callback_Tray.rio_Config_Context,
        RIO_DEST_OPS_CAR_OFFSET,
        &dest_Trx
    );

    if ( (mask & dest_Trx) == mask)
        return RIO_TRUE;
    else 
        return RIO_FALSE;

}

/***************************************************************************
 * Function :    can_Issue
 *
 * Description:  function checks if LL can issue locally requested 
 *               transaction with given ftype and ttype. ftype and ttype
 *               shall be defined accordingly to the RIO_LL_FTYPE_T and 
 *               RIO_*_TTYPE_T.
 *
 * Returns:      RIO_TRUE or RIO_FALSE
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_BOOL_T can_Issue( RIO_LL_DS_T *handle, RIO_LL_FTYPE_T ftype, RIO_CONF_TTYPE_T ttype )
{
    RIO_WORD_T mask;
    RIO_WORD_T source_Trx;

#ifdef _DEBUG
    assert( handle ); 
#endif

    switch ( ftype )
    {
        case RIO_LL_GSM:
        {
            switch ( ttype )
            {
                case RIO_GSM_READ_SHARED:
                    mask = RIO_MASK_READ;
                break;
                case RIO_GSM_READ_TO_OWN:
                    mask = RIO_MASK_READ_TO_OWN;
                break;
                case RIO_GSM_IO_READ:
                    mask = RIO_MASK_IO_READ;
                break;
                case RIO_GSM_DC_I:
                    mask = RIO_MASK_DKILL;
                break;
                case RIO_GSM_INSTR_READ:
                    mask = RIO_MASK_IREAD;
                break;
                case RIO_GSM_IC_I:
                    mask = RIO_MASK_IKILL;
                break;
                case RIO_GSM_TLB_IE:
                    mask = RIO_MASK_TLBIE;
                break;
                case RIO_GSM_TLB_SYNC:
                    mask = RIO_MASK_TLBSYNC;
                break;
                case RIO_GSM_CASTOUT:
                    mask = RIO_MASK_CASTOUT;
                break;
                case RIO_GSM_FLUSH:
                    mask = RIO_MASK_FLUSH;
                break;
                default:
                    mask = 0;
                break;
            }
            break;
        } 
        case RIO_LL_IO:
        {
            switch ( (RIO_IO_TTYPE_T)ttype )
            {
                case RIO_IO_NREAD:
                    mask = RIO_MASK_NREAD;
                break;
                case RIO_IO_NWRITE:
                    mask = RIO_MASK_NWRITE;
                break;
                case RIO_IO_SWRITE:
                    mask = RIO_MASK_SWRITE;
                break;
                case RIO_IO_NWRITE_R:
                    mask = RIO_MASK_NWRITE_R;
                break;
                case RIO_IO_ATOMIC_INC:
                    mask = RIO_MASK_ATOMIC_INC;
                break;
                case RIO_IO_ATOMIC_DEC:
                    mask = RIO_MASK_ATOMIC_DEC;
                break;
                case RIO_IO_ATOMIC_SET:
                    mask = RIO_MASK_ATOMIC_SET;
                case RIO_IO_ATOMIC_CLEAR:
                    mask = RIO_MASK_ATOMIC_CLEAR;
                break;
                case RIO_IO_ATOMIC_TSWAP:
                    mask = RIO_MASK_ATOMIC_TSWAP;
                break;
		/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                case RIO_IO_ATOMIC_SWAP:
		   mask = RIO_MASK_ATOMIC_SWAP;
                break;

		default:
                    mask = 0;
                break;
            }
            break;
        }
        case RIO_LL_CONF:
        {
            switch ( (RIO_CONF_TTYPE_T)ttype )
            {
                case RIO_CONF_READ:
                    return RIO_TRUE;
                break;
                case RIO_CONF_WRITE:
                    return RIO_TRUE;
                break;
                case RIO_CONF_PORT_WRITE:
                    mask = RIO_MASK_PORT_WRITE;
                break;
                default:
                    mask = 0;
                break;
            }
            break;
        }
        case RIO_LL_MESSAGE:
            mask = RIO_MASK_MESSAGE;
        break;
        case RIO_LL_DOORBELL:
            mask = RIO_MASK_DOORBELL;
        break;

	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_LL_DS:
	     mask = RIO_MASK_DS;
	break;
	/* GDA: Bug#124 */

	/* GDA: Bug#125 - Support for Flow control packet */
        case RIO_LL_FC:
            mask = RIO_MASK_FC;
        break;
	/* GDA: Bug#125 */
	
        default:
            mask = 0;
        break;
    }

    handle->callback_Tray.rio_Get_Config_Reg(
        handle->callback_Tray.rio_Config_Context,
        RIO_SOURCE_OPS_CAR_OFFSET,
        &source_Trx
    );

    if ( (mask & source_Trx) == mask)
        return RIO_TRUE;
    else 
        return RIO_FALSE;

}

/***************************************************************************
 * Function :    max_Source_Message_Size
 *
 * Description:  function returns maximum size in doublewords of message 
 *               segment data payload which can be issued by PE.
 *
 * Returns:      number of bytes, 0 in case of error
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T max_Source_Message_Size( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox )
{
    (void)handle; (void)mailbox;
#ifdef _DEBUG
    assert( handle );
#endif
    

    return RIO_LL_MAX_MESSAGE_BYTE_SIZE / RIO_LL_DW_BYTE_SIZE;
    
}

/***************************************************************************
 * Function :    max_Source_Message_Seg
 *
 * Description:  function returns maximum number of segments which can form
 *               the message issued by PE.
 *
 * Returns:      number of segments
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T max_Source_Message_Seg( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox )
{
    (void)handle; (void)mailbox;
#ifdef _DEBUG
    assert( handle );
#endif

    return RIO_LL_MAX_MSG_SEGMENTS_NUM;
    
}

/***************************************************************************
 * Function :    max_Dest_Message_Size
 *
 * Description:  function returns maximum size in doublewords of message 
 *               segment data payload which can be serviced by PE.
 *
 * Returns:      number of bytes, 0 in case of error
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T max_Dest_Message_Size( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox )
{
    (void)handle; (void)mailbox;
#ifdef _DEBUG
    assert( handle );
#endif

    return RIO_LL_MAX_MESSAGE_BYTE_SIZE / RIO_LL_DW_BYTE_SIZE;

}

/***************************************************************************
 * Function :    max_Dest_Message_Seg
 *
 * Description:  function returns maximum number of segments which can form
 *               the message serviced by PE.
 *
 * Returns:      number of segments
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T max_Dest_Message_Seg( RIO_LL_DS_T *handle, RIO_UCHAR_T mailbox )
{
    (void)handle; (void)mailbox;
#ifdef _DEBUG
    assert( handle );
#endif

    return RIO_LL_MAX_MSG_SEGMENTS_NUM;

}

/***************************************************************************
 * Function :    max_Source_Resp_Size
 *
 * Description:  function returns maximum size in doublewords of the 
 *               source response data payload which can be issued by PE.
 *
 * Returns:      number of bytes, 0 in case of error
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T max_Source_Resp_Size( RIO_LL_DS_T *handle )
{
  (void)handle;
#ifdef _DEBUG
    assert( handle );
#endif

    return RIO_LL_MAX_PACKET_DW_SIZE; 

}

/***************************************************************************
 * Function :    max_Dest_Packet_Size
 *
 * Description:  function returns maximum size in doublewords of the 
 *               destination packet data payload which can be serviced by PE.
 *
 * Returns:      number of bytes, 0 in case of error
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T max_Dest_Packet_Size( RIO_LL_DS_T *handle )
{
    (void)handle;
#ifdef _DEBUG
    assert( handle );
#endif

    return RIO_LL_MAX_PACKET_DW_SIZE; 

}

/***************************************************************************
 * Function :    max_Dest_Port_Write_Size
 *
 * Description:  function returns maximum size in doublewords of the 
 *               destination PORT WRITE packet data payload which can be 
 *               serviced by PE.
 *
 * Returns:      number of bytes, 0 in case of error
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T max_Dest_Port_Write_Size( RIO_LL_DS_T *handle )
{
    (void)handle;
#ifdef _DEBUG
    assert( handle );
#endif

    return RIO_LL_MAX_PORT_WRITE_BYTE_SIZE / RIO_LL_DW_BYTE_SIZE;

}

/***************************************************************************
 * Function :    get_LCSBAR
 *
 * Description:  function returns value of the LCSBAR register
 *
 * Returns:      value of the LCSBAR register
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T get_LCSBAR( RIO_LL_DS_T *handle )
{
    RIO_WORD_T  lcsbar;

#ifdef _DEBUG
    assert( handle );
#endif

    handle->callback_Tray.rio_Get_Config_Reg(
        handle->callback_Tray.rio_Config_Context,
        RIO_LCSBAR_OFFSET,
        &lcsbar
    );

    return lcsbar;
}

/***************************************************************************
 * Function :    get_LCSHBAR
 *
 * Description:  function returns value of the LCSHBAR register
 *
 * Returns:      value of the LCSHBAR register
 *
 * Notes:       
 *
 **************************************************************************/
static RIO_WORD_T get_LCSHBAR( RIO_LL_DS_T *handle )
{
    RIO_WORD_T  lcshbar;

#ifdef _DEBUG
    assert( handle );
#endif

    handle->callback_Tray.rio_Get_Config_Reg(
        handle->callback_Tray.rio_Config_Context,
        RIO_LCSHBAR_OFFSET,
        &lcshbar
    );

    return lcshbar;
}

/***************************************************************************
 * Function :    translate_LL_FType_To_Spec_FType
 *
 * Description:  function translates logical layer ftype and ttype 
 *               (RIO_LL_FTYPE_T, RIO_*_TTYPE_T) into spec's representation
 *               (RIO_FTYPE_T, RIO_*_TTYPE_T).
 *
 * Returns:      RIO_OK in case of successfull translation, RIO_ERROR overwise
 *
 * Notes:        FType and TType accordingly to specification is returned in
 *               *spec_FType and *spec_TType in case of the successfull 
 *               translation. In case of error values of these variables 
 *               are undefined.
 *
 **************************************************************************/
static int translate_LL_FType_To_Spec_FType(
    RIO_LL_FTYPE_T ll_FType, 
    RIO_IO_TTYPE_T ll_TType, 
    RIO_FTYPE_T *spec_FType, 
    RIO_UCHAR_T *spec_TType
)
{
    switch ( ll_FType )
    {
        case RIO_LL_GSM:
        {
            switch ( ll_TType )
            {
                case RIO_GSM_READ_SHARED:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_READ_HOME;
                    return RIO_OK;
                }
                case RIO_GSM_READ_TO_OWN:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_READ_TO_OWN_HOME;
                    return RIO_OK;
                }
                case RIO_GSM_IO_READ:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_IO_READ_HOME;
                    return RIO_OK;
                }
                case RIO_GSM_DC_I:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_DKILL_HOME;
                    return RIO_OK;
                }
                case RIO_GSM_INSTR_READ:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_IREAD_HOME;
                    return RIO_OK;
                }
                case RIO_GSM_IC_I:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_IKILL_HOME;
                    return RIO_OK;
                }
                case RIO_GSM_TLB_IE:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_TLBIE;
                    return RIO_OK;
                }
                case RIO_GSM_TLB_SYNC:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_TLBSYNC;
                    return RIO_OK;
                }
                case RIO_GSM_CASTOUT:
                {
                    *spec_FType = RIO_WRITE;
                    *spec_TType = RIO_WRITE_CASTOUT;
                    return RIO_OK;
                }
                case RIO_GSM_FLUSH:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_FLUSH_WO_DATA;
                    return RIO_OK;
                }
                default:
                {
                    *spec_FType = 0;
                    *spec_TType = 0;
                    return RIO_ERROR;
                }
            }
            break;
        } 
        case RIO_LL_IO:
        {
            switch ( ll_TType )
            {
                case RIO_IO_NREAD:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_NREAD;
                    return RIO_OK;
                }
                case RIO_IO_NWRITE:
                {
                    *spec_FType = RIO_WRITE;
                    *spec_TType = RIO_WRITE_NWRITE;
                    return RIO_OK;
                }
                case RIO_IO_SWRITE:
                {
                    *spec_FType = RIO_STREAM_WRITE;
                    *spec_TType = 0;
                    return RIO_OK;
                }
                case RIO_IO_NWRITE_R:
                {
                    *spec_FType = RIO_WRITE;
                    *spec_TType = RIO_WRITE_NWRITE_R;
                    return RIO_OK;
                }
                case RIO_IO_ATOMIC_INC:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_ATOMIC_INC;
                    return RIO_OK;
                }
                case RIO_IO_ATOMIC_DEC:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_ATOMIC_DEC;
                    return RIO_OK;
                }
                case RIO_IO_ATOMIC_SET:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_ATOMIC_SET;
                    return RIO_OK;
                }
                case RIO_IO_ATOMIC_CLEAR:
                {
                    *spec_FType = RIO_NONINTERV_REQUEST;
                    *spec_TType = RIO_NONINTERV_ATOMIC_CLEAR;
                    return RIO_OK;
                }
		/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                case RIO_IO_ATOMIC_SWAP:
                {
                    *spec_FType = RIO_WRITE;
                    *spec_TType = RIO_WRITE_ATOMIC_SWAP;
                    return RIO_OK;
                }
                case RIO_IO_ATOMIC_TSWAP:
                {
                    *spec_FType = RIO_WRITE;
                    *spec_TType = RIO_WRITE_ATOMIC_TSWAP;
                    return RIO_OK;
                }
                default:
                {
                    *spec_FType = 0;
                    *spec_TType = 0;
                    return RIO_ERROR;
                }
            }
            break;
        }
        case RIO_LL_CONF:
        {
            switch ( (RIO_CONF_TTYPE_T)ll_TType )
            {
                case RIO_CONF_READ:
                {
                    *spec_FType = RIO_MAINTENANCE;
                    *spec_TType = RIO_MAINTENANCE_CONF_READ;
                    return RIO_OK;
                }
                case RIO_CONF_WRITE:
                {
                    *spec_FType = RIO_MAINTENANCE;
                    *spec_TType = RIO_MAINTENANCE_CONF_WRITE;
                    return RIO_OK;
                }
                default:
                {
                    *spec_FType = 0;
                    *spec_TType = 0;
                    return RIO_ERROR;
                }
            }
            break;
        }
        case RIO_LL_MESSAGE:
        {
            *spec_FType = RIO_MESSAGE;
            *spec_TType = 0;
            return RIO_OK;
        }
        case RIO_LL_DOORBELL:
        {
            *spec_FType = RIO_DOORBELL;
            *spec_TType = 0;
            return RIO_OK;
        }
	/* GDA: Bug#124 - Support for Data Streaming packet */
        case RIO_LL_DS:
        {
            *spec_FType = RIO_DS;
            *spec_TType = 0;
            return RIO_OK;
        }
	/* GDA: Bug#124 */

	/* GDA: Bug#125 - Support for Flow control packet */
        case RIO_LL_FC:
        {
            *spec_FType = RIO_FC;
            *spec_TType = 0;
            return RIO_OK;
        }
	/* GDA: Bug#125 */

        default:
        {
            *spec_FType = 0;
            *spec_TType = 0;
            return RIO_ERROR;
        }
    }
}

/***************************************************************************
 * Function :    translate_WDPTR_To_DW_Size
 *
 * Description:  function translates wdprt and {wrsize|rdsize} into offset,
 *               byte_Num. dw_Size
 *
 * Returns:      RIO_OK in case of successfull translation, RIO_ERROR overwise
 *
 * Notes:        In case of successfull translation, values is returned in 
 *               *offset, *byte_Num and *dw_Size. In case of error values 
 *               of these variables are undefined.
 *               In case of dw_Size > 1, values of *offset and *byte_Num are 
 *               undefined.
 *
 **************************************************************************/
static int translate_WDPTR_To_DW_Size(
    RIO_UCHAR_T wdptr,
    RIO_UCHAR_T rdsize,
    RIO_BOOL_T  is_RDSIZE,
    RIO_UCHAR_T *offset,
    RIO_UCHAR_T *byte_Num,
    RIO_UCHAR_T *dw_Size
)
{
    /* the constants are maximum values of appropriate packet fields defined by
       RIO spec. */
    if( wdptr > 1 || rdsize > 15 )
        return RIO_ERROR;
        
    if( is_RDSIZE == RIO_TRUE )
    {
        /* there are no reserved combinations here */
        *offset = wdptr_To_Read_Size_Translation_Table[wdptr][rdsize].offset;
        *byte_Num = wdptr_To_Read_Size_Translation_Table[wdptr][rdsize].byte_Num;
        *dw_Size = wdptr_To_Read_Size_Translation_Table[wdptr][rdsize].dw_Size;
    }
    else 
    {
        /* rdsize parameter contains actually wrsize */
        if( wdptr_To_Write_Size_Translation_Table[wdptr][rdsize].is_Allowed_Combination == RIO_LL_RESERVED )
            return RIO_ERROR;
        else
        {
            *offset = wdptr_To_Write_Size_Translation_Table[wdptr][rdsize].offset;
            *byte_Num = wdptr_To_Write_Size_Translation_Table[wdptr][rdsize].byte_Num;
            *dw_Size = wdptr_To_Write_Size_Translation_Table[wdptr][rdsize].dw_Size;
        }
    }   
      
    return RIO_OK;
}

/***************************************************************************
 * Function :    translate_SSIZE_To_DW_Size
 *
 * Description:  translates ssize encodings to doublewords
 *
 * Returns:      RIO_OK in case of the correct values. RIO_ERROR overwise
 *
 * Notes:        
 *
 **************************************************************************/

int translate_SSIZE_To_DW_Size(
    RIO_UCHAR_T ssize,
    RIO_UCHAR_T *dw_Size
)
{
    /* constants are in accordance with RapidIO Spec ssize encodings */
    if( ssize == 9 )    /* 1001 */
        *dw_Size = 1;
    else if( ssize == 10 )   /* 1010 */
        *dw_Size = 2;
    else if( ssize == 11 )   /* 1011 */
        *dw_Size = 4;
    else if( ssize == 12 )   /* 1100 */
        *dw_Size = 8;
    else if( ssize == 13 )   /* 1101 */
        *dw_Size = 16;
    else if( ssize == 14 )   /* 1110 */
        *dw_Size = 32;
    else
        return RIO_ERROR;
        
    return RIO_OK;
}


/***************************************************************************
 * Function :    check_Packet_Data_Size
 *
 * Description:  function checks if the supplied wdptr and rdsize is 
 *               specified correctly for the packet of the given ftype 
 *               and ttype
 *
 * Returns:      RIO_OK in case of the correct values. RIO_ERROR overwise
 *
 * Notes:        
 *
 **************************************************************************/
static int check_Packet_Data_Size(
    RIO_LL_DS_T *handle,
    RIO_UCHAR_T ftype,
    RIO_UCHAR_T ttype,
    RIO_UCHAR_T wdptr,
    RIO_UCHAR_T rdsize,
    RIO_UCHAR_T mbox
)
{
    RIO_UCHAR_T dw_Size;
    RIO_UCHAR_T offset;
    RIO_UCHAR_T byte_Num;   
    RIO_UCHAR_T granule_Size; 
    RIO_UCHAR_T max_Dest_Packet;
    RIO_UCHAR_T max_Source_Resp;
    RIO_UCHAR_T max_Dest_Port_Write;

#ifdef _DEBUG
    assert( handle );
#endif

    /* define granule size */
    if ( handle->inst_Params.coh_Granule_Size_32 )
        granule_Size = 32; /* thirty two byte coherency granule size */
    else
        granule_Size = 64; /* sixty four byte coherency granule size */

    /* define maximum destination packet size */
    max_Dest_Packet = (RIO_UCHAR_T)max_Dest_Packet_Size( handle );

    /* define maximum source response size */
    max_Source_Resp = (RIO_UCHAR_T)max_Source_Resp_Size( handle );

    /* define maximum destination Port Write packet size */
    max_Dest_Port_Write = (RIO_UCHAR_T)max_Dest_Port_Write_Size( handle );

    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
        {  
            if ( translate_WDPTR_To_DW_Size( wdptr, rdsize, RIO_TRUE, &offset, &byte_Num, &dw_Size) == RIO_ERROR )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                return RIO_ERROR;

            }

            /* these transactions shall always have data size equal to cache granule size */
            if ( dw_Size != granule_Size / RIO_LL_DW_BYTE_SIZE )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                return RIO_ERROR;
            }

            return RIO_OK;
        }            

        case RIO_NONINTERV_REQUEST:
        {
            /* this transactions is address only or transaction only transactions */
            /* so data payload encoding is don't care in these cases              */
            if ( ttype == RIO_NONINTERV_DKILL_HOME || ttype == RIO_NONINTERV_DKILL_SHARER ||
                ttype == RIO_NONINTERV_IKILL_HOME || ttype == RIO_NONINTERV_IKILL_SHARER || 
                ttype == RIO_NONINTERV_TLBIE || ttype == RIO_NONINTERV_TLBSYNC )
                return RIO_OK;
    
            if ( translate_WDPTR_To_DW_Size( wdptr, rdsize, RIO_TRUE, &offset, &byte_Num, &dw_Size) == RIO_ERROR )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                return RIO_ERROR;
            }

            switch (ttype) 
            {
                /* these transactions shall always request data size equal to the */
                /* cache granule size                                             */
                case RIO_NONINTERV_READ_HOME:
                case RIO_NONINTERV_READ_TO_OWN_HOME:
                case RIO_NONINTERV_IREAD_HOME:  
                {
                    if ( dw_Size != granule_Size / RIO_LL_DW_BYTE_SIZE )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                        return RIO_ERROR;
                    }
                        
                    break;
                }

                case RIO_NONINTERV_IO_READ_HOME:    
                case RIO_NONINTERV_FLUSH_WO_DATA:   
                {
                    /* this transaction shall never include data payload bigger than the */
                    /* cache granule size                                                */
                    if ( dw_Size > granule_Size / RIO_LL_DW_BYTE_SIZE )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                            "remote FLUSH without data", granule_Size );
                        return RIO_ERROR;
                    }
                        
                    break;
                }
                   
                case RIO_NONINTERV_NREAD:     
                {
                    /* this transaction shall never request data size bigger than */
                    /* maximal source response size                               */
                    if ( dw_Size > max_Source_Resp )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                            "remote IO NREAD", max_Source_Resp );
                        return RIO_ERROR;
                    }

                    break;
                }

                case RIO_NONINTERV_ATOMIC_INC:      
                case RIO_NONINTERV_ATOMIC_DEC:      
                case RIO_NONINTERV_ATOMIC_SET:      
                case RIO_NONINTERV_ATOMIC_CLEAR:
                {
                    /* ATOMIC transaction shall never have data size bigger than four bytes */
                    if ( byte_Num > RIO_LL_MAX_ATOMIC_BYTE_SIZE || byte_Num == 3 || dw_Size > 1 )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, "remote IO ATOMIC", 1 );
                        return RIO_ERROR;
                    }

                    break;
                }

                default:
                    return RIO_ERROR;
            }

            return RIO_OK;
        }

        case RIO_WRITE:
        {
            if ( translate_WDPTR_To_DW_Size( wdptr, rdsize, RIO_FALSE, &offset, &byte_Num, &dw_Size) == RIO_ERROR )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                return RIO_ERROR;
            }

            switch ( ttype )
            {
                case RIO_WRITE_CASTOUT:
                {
                    /* this transaction shall always include data payload equal to the size */
                    /* of the cache granule                                                 */
                    if ( dw_Size != granule_Size / RIO_LL_DW_BYTE_SIZE )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                        return RIO_ERROR;
                    }
                        
                    break;
                }

                case RIO_WRITE_FLUSH_WITH_DATA: 
                {
                    /* this transaction shall never include data payload bigger than the */
                    /* cache granule size                                                */
                    if ( dw_Size > granule_Size / RIO_LL_DW_BYTE_SIZE )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                            "remote FLUSH with data", granule_Size );
                        return RIO_ERROR;
                    }
                        
                    break;
                }

                case RIO_WRITE_NWRITE:
                case RIO_WRITE_NWRITE_R:
                {
                    /* these transactions shall never supply data payload with size bigger */
                    /* than the maximal deatination packet size                            */
                    if ( dw_Size > max_Dest_Packet )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                            "remote IO NWRITE or NWRITE_R", max_Dest_Packet );
                        return RIO_ERROR;
                    }

                    break;
                }

                case RIO_WRITE_ATOMIC_SWAP:/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                case RIO_WRITE_ATOMIC_TSWAP:
                {
                    /* ATOMIC transaction shall never have data size bigger than four bytes */
                    if ( byte_Num > RIO_LL_MAX_ATOMIC_BYTE_SIZE || byte_Num == 3 || dw_Size > 1 )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, "remote IO ATOMIC TSWAP", 1 );
                        return RIO_ERROR;
                    }

                    break;
                }

                default:
                    return RIO_ERROR;
            }

            return RIO_OK;
        }

        case RIO_STREAM_WRITE:
            /* this transaction hasn't data size encoding in the packet */
            return RIO_OK;

	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	    return RIO_OK;
	/* GDA: Bug#124 */

	/* GDA: Bug#125 - Support for Flow control packet */
        case RIO_FC:
            return RIO_OK;
	/* GDA: Bug#125 */

        case RIO_MAINTENANCE:
        {
            if ( translate_WDPTR_To_DW_Size( wdptr, rdsize, RIO_FALSE, &offset, &byte_Num, &dw_Size) == RIO_ERROR )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                return RIO_ERROR;
            }

            if (dw_Size > 8)
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                return RIO_ERROR;
            }

            switch (ttype)
            {
                case RIO_MAINTENANCE_CONF_READ:
                {
                    /* this trnasction shall always request size of data not bigger than */
                    /* maximal source response size                                      */
                    if ( dw_Size > max_Source_Resp )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                            "remote MAINTENANCE READ", max_Source_Resp );
                        return RIO_ERROR;
                    }

                    break;
                }

                case RIO_MAINTENANCE_CONF_WRITE:
                {
                    /* this trnasction shall always supply size of data not bigger than */
                    /* maximal destination packet size                                  */
                    if ( dw_Size > max_Dest_Packet )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                            "remote MAINTENANCE WRITE", (unsigned long)max_Dest_Packet );
                        return RIO_ERROR;
                    }

                    break;
                }

                case RIO_MAINTENANCE_PORT_WRITE:
                {
                    /* this trnasction shall always supply size of data not bigger than */
                    /* maximal destination port write packet size                       */
                    if ( dw_Size > max_Dest_Port_Write )
                    {
                        RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                            "remote MAINTENANCE PORT WRITE", max_Dest_Port_Write );
                        return RIO_ERROR;
                    }

                    break;
                }

                default:
                    return RIO_ERROR;
            }

            return RIO_OK;
        }

        case RIO_DOORBELL:
            /* this transaction doesn't have data in packet */
            return RIO_OK;

        case RIO_MESSAGE:
        {
            if ( translate_SSIZE_To_DW_Size( rdsize, &dw_Size) == RIO_ERROR )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_REMOTE_DATA_PAYLOAD );
                return RIO_ERROR;
            }

            /* this transaction shall never supply data payload bigger than maximal */
            /* destination message size                                             */
            if ( dw_Size > max_Dest_Message_Size( handle, mbox ) )
            {
                RIO_LL_Message( handle, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, 
                    "remote message", max_Dest_Message_Size( handle, mbox ) );
                return RIO_ERROR;                
            }

            return RIO_OK;
        }

        default:
            return RIO_ERROR;

    }
}

/***************************************************************************
 * Function :    is_GSM_Transaction
 *
 * Description:  function checks if the transaction in the buffer entry
 *               with tag buf_Tag is GSM transaction or not
 *
 * Returns:      RIO_TRUE or RIO_FALSE;
 *
 * Notes:        
 *
 **************************************************************************/
static RIO_BOOL_T is_GSM_Transaction( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert( handle );
#endif

    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal Logical Layer function");
        return RIO_FALSE;
    }

    switch ( buf_Entry->trx_Info.req_FType )
    {
        case RIO_INTERV_REQUEST:
            return RIO_TRUE;

        case RIO_NONINTERV_REQUEST:
        {
            switch ( buf_Entry->trx_Info.req_TType )
            {
                case RIO_NONINTERV_READ_HOME:
                case RIO_NONINTERV_READ_TO_OWN_HOME:
                case RIO_NONINTERV_IO_READ_HOME:
                case RIO_NONINTERV_DKILL_HOME:
                case RIO_NONINTERV_IKILL_HOME:
                case RIO_NONINTERV_TLBIE:
                case RIO_NONINTERV_TLBSYNC:
                case RIO_NONINTERV_IREAD_HOME:
                case RIO_NONINTERV_FLUSH_WO_DATA:
                case RIO_NONINTERV_IKILL_SHARER:
                case RIO_NONINTERV_DKILL_SHARER:
                    return RIO_TRUE;

                default:
                    return RIO_FALSE;
            }

            break;
        }

        case RIO_WRITE:
        {
            switch ( buf_Entry->trx_Info.req_TType )
            {
                case RIO_WRITE_CASTOUT:
                case RIO_WRITE_FLUSH_WITH_DATA:
                    return RIO_TRUE;

                default:
                    return RIO_FALSE;
            }
            break;
        }

        default:
            return RIO_FALSE;
    }
    
}

/***************************************************************************
 * Function :    is_IO_Transaction
 *
 * Description:  function checks if the transaction in the buffer entry
 *               with tag buf_Tag is IO transaction or not
 *
 * Returns:      RIO_TRUE or RIO_FALSE;
 *
 * Notes:        
 *
 **************************************************************************/
static RIO_BOOL_T is_IO_Transaction( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert( handle );
#endif

    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal Logical Layer function");
        return RIO_FALSE;
    }

    switch ( buf_Entry->trx_Info.req_FType )
    {
        case RIO_NONINTERV_REQUEST:
        {
            switch ( buf_Entry->trx_Info.req_TType )
            {
                case RIO_NONINTERV_NREAD:
                case RIO_NONINTERV_ATOMIC_INC:
                case RIO_NONINTERV_ATOMIC_DEC:
                case RIO_NONINTERV_ATOMIC_SET:
                case RIO_NONINTERV_ATOMIC_CLEAR:
                    return RIO_TRUE;

                default:
                    return RIO_FALSE;
            }

            break;
        }

        case RIO_WRITE:
        {
            switch ( buf_Entry->trx_Info.req_TType )
            {
                case RIO_WRITE_NWRITE:
                case RIO_WRITE_NWRITE_R:
                case RIO_WRITE_ATOMIC_SWAP:/* GDA: Bug#133 - Support for ttype C for type 5 packet */
                case RIO_WRITE_ATOMIC_TSWAP:
                    return RIO_TRUE;

                default:
                    return RIO_FALSE;
            }
            break;
        }

        case RIO_STREAM_WRITE:
            return RIO_TRUE;

        default:
            return RIO_FALSE;
    }
    
}

/***************************************************************************
 * Function :    get_Config_Reg_Field
 *
 * Description:  calls PL to obtain the value of the specified configuration
 *               register field
 *
 * Returns:      field of the configuration register
 *
 * Notes:        
 *
 **************************************************************************/

static unsigned long get_Config_Reg_Field(RIO_LL_DS_T *handle, RIO_LL_CONFIG_FIELD_T field)
{
    unsigned long reg_Value;
    unsigned long field_Value;
    
    RIO_CONF_OFFS_T   config_Offset;
    
    /* read the register */
    switch ( field )
    {
        case RIO_LL_EXT_ADDRESSING_SUPPORT:
        {
            config_Offset = RIO_LL_PE_LL_CSR;
            handle->callback_Tray.rio_Get_Config_Reg( 
                handle->callback_Tray.rio_Config_Context, 
                config_Offset, 
                &reg_Value);
            field_Value = reg_Value & RIO_LL_PE_LL_CSR_EXT_ADDR_SUPPORT_MASK;
            break;
        }
        case RIO_LL_MY_DEVICE_ID:
        {
            config_Offset = RIO_LL_BASE_ADDRESS_CSR;
            handle->callback_Tray.rio_Get_Config_Reg( 
                handle->callback_Tray.rio_Config_Context, 
                config_Offset, 
                &reg_Value);
            if( handle->init_Params.transp_Type == RIO_PL_TRANSP_16 )
                field_Value = (reg_Value >> 16) & RIO_LL_TR_INFO_16_SRC_MASK;
                
            else if ( handle->init_Params.transp_Type == RIO_PL_TRANSP_32 )
                field_Value = reg_Value & RIO_LL_TR_INFO_32_SRC_MASK;
                
            else
                /* impossible case */
                field_Value = 0;
                                                      
            break;
        }
	
	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_LL_DSTREAMING_SUPPORT:
	{
            config_Offset = RIO_SOURCE_PACKET_CONF_CSR_OFFSET;	/* 0x48 */
            handle->callback_Tray.rio_Get_Config_Reg( 
                handle->callback_Tray.rio_Config_Context, 
                config_Offset, 
                &reg_Value);

            field_Value = reg_Value & RIO_LL_PE_LL_CSR_DSTREAM_SUPPORT_MASK;  /* 0xFF */
            break;
	}
	case RIO_LL_DSTREAMING_SUPPORT_INFO:
	{
            config_Offset =RIO_DS_OPS_CAR_OFFSET;	/* 0x3C */
            handle->callback_Tray.rio_Get_Config_Reg( 
                handle->callback_Tray.rio_Config_Context, 
                config_Offset, 
                &reg_Value);

            field_Value = reg_Value & RIO_LL_PE_LL_CSR_DSTREAM_INFO_MASK;  /* 0xFFFF_FFFF */
            break;
	}
	/* GDA: Bug#124 - Support for Data Streaming packet */

        default:
        {
            field_Value = 0;
            break;
        }
    }
    
    return field_Value;
}   

/* GDA: Bug#124 - Support for Data Streaming packet */

/***************************************************************************
 *  Function :    RIO_LL_DS_Request
 *  
 *  Description:  function receives data structure describing necessary 
 *                DS. If data is correct buffer is assigned
 *                and request is processed by its transaction handler.
 *
 *  Returns:      RIO_OK in case of successfully accepted request.
 *                RIO_PARAM_INVALID in case of incorrect data.
 *                RIO_RETRY in case of unavailable buffer entry.
 *                RIO_ERROR in case of error
 *                          
 *           Notes:
 *          
 ***************************************************************************/

int RIO_LL_DS_Request(
         RIO_HANDLE_T   handle,
         RIO_DS_REQ_T   *rq,
         RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    )
{
    RIO_LL_DS_T      *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T  *buf_Entry;  /* pointer to the buffer entry to store the transaction */
    RIO_LL_BUF_TAG_T buf_Tag;     /* buffer tag of the buffer entry                       */
    RIO_DS_REQ_T     *buf_Req;    /* copy of structure to place it to buffer              */
    RIO_HW_T         *data_hw;       /* copy of the request data to place it to buffer       */

   unsigned int   i,dstream_Support,check;
   static int j;


   ++j;
    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
          /* can not generate error message, because don't have any error function bind */
           return RIO_PARAM_INVALID;


    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
           /* this function can not be called recursively from any previously invoked LL callback */
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "DataStream request");
                return RIO_ERROR;
     }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
	              /* if in reset state do nothing */
	RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "DataStream request");
	return RIO_OK;
     }


	   /* check that user has requested transaction with correct attributes */
    if( rq == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "DataStream request" );
        return RIO_PARAM_INVALID;
    }


	     /* check priority of the request */
        if( rq->prio > RIO_MAX_REQUEST_PRIORITY )
		    {
	            /* priority of the request packet can not be larger than 2 */
           RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "DataStream", RIO_MAX_REQUEST_PRIORITY );
		            return RIO_PARAM_INVALID;
	        }



          /* check size of the packet */ /*65536 is 64k*/
#ifndef _CARBON_SRIO_XTOR_FIX
	// This can never happen due to limited data size of rq->length
    if((rq->length > 0xFFFF) )
     {
        /* the size of the transaction is larger than allowed 64k bytes */
           RIO_LL_Message( ll_Data, RIO_LL_ERROR_EXCEEDING_DATA_PAYLOAD, "DataStream", 65536);
           return RIO_PARAM_INVALID;
     }
#endif
    if (rq->length == 0)
     {
              /* request shall have size bigger than null */
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_NULL_DW_DATA_PAYLOAD, "DataStream" );
            return RIO_PARAM_INVALID;
     }
     /*printf("\nmtu size is %u \n",rq->mtu);*/
    check = rq->mtu / 4;
    dstream_Support =  get_Config_Reg_Field( handle,  RIO_LL_DSTREAMING_SUPPORT);
     check = dstream_Support & check ;
     if ((check * 4)==(rq->mtu))
	     printf("\n correct mtu  D`%u \n",check * 4);
     else
     {
		/* illegal request size */
	      RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_LOCAL_DATA_STREAM_MTU, 
		            "DataStream read", " 32,36,40...........256", rq->mtu);
			        return RIO_PARAM_INVALID;
	}
     


/* data is correct. Check if PE can issue such a transaction */
    if ( !can_Issue( ll_Data, RIO_LL_DS, 0 ))
           RIO_LL_Message( ll_Data, RIO_LL_WARN_CANNOT_ISSUE, "DS", 0 );

/* GDA: Bug#125 - Support for Flow control packet 
printf("\n xonoff at iorequwest is %d %d  \n",ll_Data->temp_Buffer.x_On_Off,ll_Data->congestion_Counter); */
   if(ll_Data->remote_Buffer->trx_Info.x_On_Off ==1 || ll_Data->temp_Buffer.x_On_Off==1)
   {
         printf("\n At congestion \n"); 
	  ll_Data->congestion_Counter++;
/*printf("\nll_Data->congestion_Counter is  %d  \n",ll_Data->congestion_Counter);*/
          if(ll_Data->congestion_Counter > RIO_MAX_DELAY_ORPHAND_XON)  /*RIO_MAX_CONGESTION_COUNTER_SIZE*/   
	  {
          ll_Data->remote_Buffer->trx_Info.x_On_Off=0;
	  ll_Data->temp_Buffer.x_On_Off=0;
	  }
          else 
           return  RIO_CONGES;   /*RIO_CONGESTION;*/
   }
  
   ll_Data->congestion_Counter=0;

/* GDA: Bug#125 - Support for Flow control packet */



/*calling segments */
         if(j==1)
	 {
           if (RIO_LL_Make_Segment( handle,rq, trx_Tag) == RIO_OK)
             return RIO_OK;
           else
	     return RIO_ERROR;	   
	 }				       
	


 /* data is correct try to allocate buffer for it */
         if ( (buf_Tag = allocate_Buffer_Entry( ll_Data, rq->prio, RIO_TRUE)) == RIO_ERROR)
		         return RIO_RETRY;

	     /* get buffer entry */
	     buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
	    if ( buf_Entry == NULL)
	     {
	         deallocate_Buffer_Entry( ll_Data, buf_Tag );
	         RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "DataStream request" );
		             return RIO_ERROR;
	     }



	        /* find handler for the transation */
	       buf_Entry->kernel_Info.handler = find_Handler(
				        ll_Data,
          			        RIO_TRUE,
					RIO_LL_DS,
			                0
					   );


               if (buf_Entry->kernel_Info.handler == NULL)
	     {
             deallocate_Buffer_Entry( ll_Data, buf_Tag );
	           RIO_LL_Message( ll_Data, RIO_LL_ERROR_LOCAL_NO_HANDLER, "DS",0  );
	             return RIO_ERROR;
	     }


	       
    /* perform address translation and set local/remote attribute in the buffer */
    if( ll_Data->callback_Tray.rio_Tr_Local_DS == NULL )
    /* no address translation UDF, make local request by default, perform no address translation */
        buf_Entry->kernel_Info.is_Local = RIO_TRUE;
    else
        buf_Entry->kernel_Info.is_Local = (ll_Data->callback_Tray.rio_Tr_Local_DS(
            ll_Data->callback_Tray.address_Translation_Context,
            &(buf_Entry->trx_Info.tr_Info[0])) == RIO_ROUTE_TO_LOCAL ) ? RIO_TRUE : RIO_FALSE;


    buf_Entry->trx_Info.multicast_Size = 1; /* there is no mulaticast ability for the DS  requests */

   /* create copy of the request structure */
   buf_Req = ( RIO_DS_REQ_T* )malloc( sizeof( RIO_DS_REQ_T ) );
   if ( buf_Req == NULL )
   {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, sizeof( RIO_DS_REQ_T), "DataStream request structure");
	 return RIO_ERROR;
     }
 
    *buf_Req = *rq;


    /* create copy of the data in the request structure */

        data_hw = ( RIO_HW_T* )malloc( rq->hw_Size * sizeof( RIO_HW_T ) );
        if ( data_hw == NULL )
        {
            deallocate_Buffer_Entry( ll_Data, buf_Tag );    
            free( buf_Req );
            RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, 
                rq->hw_Size * sizeof( RIO_HW_T), "DS request data payload");
            return RIO_ERROR;
        }

        /* fill in copy of data */
        for ( i = 0; i < rq->hw_Size; i++ )
            data_hw[i] = rq->data_hw[i];
    
    /* do proper aligment of the offset in the copy of the request */

    buf_Req->data_hw = data_hw;
    /* fill in transaction info buffer structure */
    buf_Entry->trx_Info.prio         = buf_Req->prio;
    buf_Entry->trx_Info.user_Trx_Tag = trx_Tag;
    translate_LL_FType_To_Spec_FType(
		RIO_LL_DS,
		0,
		&(buf_Entry->trx_Info.req_FType),
		 &(buf_Entry->trx_Info.req_TType)
		);

   buf_Entry->trx_Info.req_Type    = RIO_LL_DS;
   buf_Entry->trx_Info.req         = buf_Req;
   buf_Entry->trx_Info.offset      = buf_Req->offset;
   buf_Entry->trx_Info.length      = buf_Req->length;
   buf_Entry->trx_Info.s_Ds        = buf_Req->s_Ds;
   buf_Entry->trx_Info.e_Ds        = buf_Req->e_Ds;
   buf_Entry->trx_Info.mtu         = buf_Req->mtu;
   buf_Entry->trx_Info.cos         = buf_Req->cos;
   buf_Entry->trx_Info.o           = buf_Req->o;
   buf_Entry->trx_Info.p           = buf_Req->p;
       

   if ( rq->data_hw != NULL )
    {
            for ( i = 0; i < rq->hw_Size; i++ )
	            buf_Entry->trx_Info.data_hw[i] = rq->data_hw[i];  
    }	    

    buf_Entry->trx_Info.state       = RIO_LL_STATE_INIT;


    if ( buf_Entry->kernel_Info.is_Local )
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_LOCAL );
    else
	 return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_REMOTE );

}

/***************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */


/***************************************************************************
 *   Function :    is_DS_Transaction
 *  
 *   Description:  function checks if the transaction in the buffer entry
 *                 with tag buf_Tag is DataStreaming transaction or not
 *  
 *   Returns:      RIO_TRUE or RIO_FALSE;
 *  
 *   Notes:        
 *          
 **************************************************************************/

static RIO_BOOL_T is_DS_Transaction( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
       assert( handle );
#endif

	    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
        {
          RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal Logical Layer function");
          return RIO_FALSE;
	}

     switch ( buf_Entry->trx_Info.req_FType )
	         {
		 case RIO_DS:
	              return RIO_TRUE;
		      
                  default:
		       return RIO_FALSE;
		 }
}


/***************************************************************************/

/* GDA: Bug#124 - Support for Data Streaming packet */


/***************************************************************************
 *  Function :    RIO_LL_Kernel_DS_Request_Done
 * 
 * Description:  function invokes DataStreaming done callback
 * 
 * Returns:      RIO_OK or RIO_ERROR
 * 
 *  Notes:
 ****************************************************************************/
int RIO_LL_Kernel_DS_Request_Done(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_BOOL_T       supply_Data, 
    RIO_REQ_RESULT_T result
)
{
   RIO_LL_BUFFER_T *buf_Entry;
   RIO_HW_T        *data_hw;
   RIO_HW_SIZE     hw_Size;

#ifdef _DEBUG
		        assert(handle);
#endif

	 /* get appropriate buffer entry */
        buf_Entry = get_Buffer_Entry( handle, buf_Tag);
        if (buf_Entry == NULL)
   	{
	   RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel DataStreaming request done function");
	   return RIO_ERROR;
	}
       		/* set semaphor state */
	    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

		/* check if it is needed to support the data in responce */
       	   if ( supply_Data )
	   {
	      data_hw = buf_Entry->trx_Info.data_hw;
	      hw_Size = buf_Entry->trx_Info.hw_Size;
           }
	   else 
	   {
	       data_hw    = NULL;
	       hw_Size = 0;
	   }
       	    /* invoke callback */
	       handle->callback_Tray.rio_DS_Request_Done(
				       handle->callback_Tray.local_Device_Context, 
				       buf_Entry->trx_Info.user_Trx_Tag, 
				       result, 
				       buf_Entry->trx_Info.resp_Status, 
				       hw_Size,
				       data_hw
				   );

	    /* unset semaphor state */
	    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

		        return RIO_OK;
}


/***************************************************************************/

/* GDA: Bug#124 - Support for Data Streaming packet */


/***************************************************************************
 * Function :    RIO_LL_Get_Request_Data_Hw
 *
 * Description:  function is invoked by the Transport Layer to get request data
 *               
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        
 *
 **************************************************************************/
int RIO_LL_Get_Request_Data_Hw(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_HW_T      *data_hw
)
{
    RIO_LL_DS_T      *ll_Data;   /* pointer to the LL data structure */
    RIO_LL_BUF_TAG_T buf_Tag;    /* buffer tag of the request        */
    RIO_LL_BUFFER_T  *buf_Entry; /* buffer entry pointer             */

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

    /* check pointers */
    if ( data_hw == NULL )
    {
        /* NULL pointer to the data structure */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "data structure", "Logical Layer get request  data" );
        return RIO_PARAM_INVALID;
    }

    if ( cnt + offset > RIO_LL_MAX_PACKET_DS_SIZE )
    {
        return RIO_PARAM_INVALID;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Logical Layer get request  data");
        return RIO_OK;
    }

    /* find buffer entry of the corresponding request and fill in its fields */
    buf_Tag = tag;

    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "Logical Layer get request data" );
        return RIO_ERROR;
    }

    /* check if the required data amount is available in the buffer entry */

#ifdef _CARBON_SRIO_XTOR_FIX
    return RIO_PARAM_INVALID;
#endif

}

/***************************************************************************/

/* GDA: Bug#124 - Support for Data Streaming packet */

/* EOF */






/***************************************************************************
 * Function :    RIO_LL_Make_Segments
 *
 * Description:  function is invoked by the Transport Layer to get request data
 *               
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        
 *
 **************************************************************************/
int RIO_LL_Make_Segment(  
         RIO_HANDLE_T   handle,
         RIO_DS_REQ_T   *rq,
         RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    )
{
       unsigned short length,mtu,seg,endseg_length,seg_incr;
       unsigned int   check_info;
       unsigned short seg_support,pdu_support;	
       unsigned short i,j,k;
       
#ifdef _CARBON_SRIO_XTOR_FIX
	RIO_HW_T *data = (RIO_HW_T*) malloc(sizeof(RIO_HW_T) * (rq->length/2 +1));
	if (data == NULL)
		return RIO_ERROR;
#else
	RIO_HW_T data[rq->length/2 +1];
#endif
        
	length= rq->length;
	mtu=rq->mtu;
	seg_incr=mtu /2;

	
		rq->s_Ds = 0;
		rq->e_Ds = 0;
		rq->o = 0;
		rq->p = 0;
		
	check_info =  get_Config_Reg_Field( handle,  RIO_LL_DSTREAMING_SUPPORT_INFO);
	seg_support = check_info;
	check_info=check_info >> 16;
	pdu_support= check_info;

          pdu_support = pdu_support & length;
	  if(pdu_support != length)
     		{
			/* illegal request size */
	      		RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_LOCAL_DATA_STREAM_PAYLOAD, 
		        	    "DataStream read", " 1,2,3,4...........65536", length);
#ifdef _CARBON_SRIO_XTOR
				free(data);
#endif
			        	return RIO_PARAM_INVALID;
		}

	if(length < mtu || length == mtu)
	{
		rq->s_Ds = 1;
		rq->e_Ds = 1;
		
		       if(length % 2)
			{
				printf("\nPadding at Start Segment \n");
			rq->hw_Size =(length/2)+1;
			rq->o=1;
			}
		       else
				rq->hw_Size = length/2;
			for(k=0; k <rq->hw_Size; k++)
			printf("\ndata[%d] is %d \n",k,rq->data_hw[k]);
            if(RIO_LL_DS_Request(handle,
				    rq,
				    trx_Tag) == RIO_OK)
		    printf("\nSingle segment was sent\n ");
	    else
		    printf("\nError while the time of sending segments \n");

	}
	else
	{
	     if(length%2)
			data[length/2]=rq->data_hw[length/2];
	     
			
	 	for(j=0;j<length/2 ; j++)
			data[j]=rq->data_hw[j];

		seg = (length / mtu) -1 ;

                      
		if(length % mtu)
		{
			endseg_length = length % mtu;
		}
		else
		{
			seg=seg-1;
			endseg_length = mtu;
		}
		printf("\nNo.of segments are %u; End segment length is %u \n", seg+2, endseg_length); 

            seg_support = seg_support & seg;
	   if(seg_support != seg)
     		{
		/* illegal request size */
	      	RIO_LL_Message( handle, RIO_LL_ERROR_WRONG_LOCAL_DATA_STREAM_PAYLOAD, 
			            "DataStream read", "1,2,4 ........65356", seg);
#ifdef _CARBON_SRIO_XTOR
				free(data);
#endif
				        return RIO_PARAM_INVALID;
		}

		

		/*Sending Start Segment */
		printf("\nStart Segment: \n");

		rq->hw_Size = seg_incr;
		rq->s_Ds = 1;
			for(k=0; k < seg_incr ; k++)
			printf("\n data[%d]  is D`%d \n",k,rq->data_hw[k]);
                	if(  RIO_LL_DS_Request(handle, rq, trx_Tag) == RIO_OK)
		       		printf("\nStart segment sent successfully\n");
	        	else
		       		printf("\n error while the time of sending start segment \n");
		
		

		/*Sending Continuous Segment*/
		rq->s_Ds =0;

		for(i=0 ;i < seg ; i++)
		{
			printf("\n Continuous segment : D`%d\n ",i);
				
			for(j=0 ; j<seg_incr ;k++,j++)
			{
			rq->data_hw[j]= data[k];
			printf("\n data[%d]  is D`%d \n",j,rq->data_hw[j]);
			}



                 	if(  RIO_LL_DS_Request(handle, rq, trx_Tag) == RIO_OK)
		       		printf("\n Continuous segment : D`%d  sent successfully\n ",i);
	           	else
		      		printf("\n error while the time of sending Continuous  segments \n");

		}

		

		/*Sending End Segment*/
			rq->e_Ds=1;
                    printf("\n End segment : \n"); 
	
		if( endseg_length == mtu)
		{
			for(j=0 ; j<seg_incr ;k++,j++)
			{
			rq->data_hw[j]= data[k];
			printf("\n data[%d]  is D`%d \n",j,rq->data_hw[j]);
			}
                	if(  RIO_LL_DS_Request(handle, rq, trx_Tag) == RIO_OK)
		       		printf("\n End segment was sent\n ");
	        	else
		       		printf("\n error while the time of sending End segment \n");
		}
		else
		{
			
			if(endseg_length % 2)
			{
				printf("\n padding at End Segment \n");
			rq->hw_Size =(endseg_length/2)+1;
			rq->o=1;
			}
			else
				rq->hw_Size= endseg_length/2;
			for( j=0; j<rq->hw_Size ;k++,j++)
			{
			rq->data_hw[j]= data[k];
			printf("\n data[%d]  is D`%d \n",j,rq->data_hw[j]);
			}
			
                	if(  RIO_LL_DS_Request(handle, rq, trx_Tag) == RIO_OK)
		       		printf("\n End segment sent successfully\n ");
	        	else
		       		printf("\n error while the time of sending End segment \n");


		}


	}

#ifdef _CARBON_SRIO_XTOR
	free(data);
#endif
	return RIO_OK;
			

}
/* GDA: Bug#124 */
/**************************************************************************/



/* GDA: Bug#125 - Support for Flow control packet */
/***************************************************************************
 * Function :    is_FC_Transaction
 *
 * Description:  function checks if the transaction in the buffer entry
 *               with tag buf_Tag is FC transaction or not
 *
 * Returns:      RIO_TRUE or RIO_FALSE;
 *
 * Notes:        
 *
 **************************************************************************/
static RIO_BOOL_T is_FC_Transaction( RIO_LL_DS_T *handle, RIO_LL_BUF_TAG_T buf_Tag)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert( handle );
#endif

    /* get buffer entry */
    if ( (buf_Entry = get_Buffer_Entry( handle, buf_Tag )) == NULL )
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "internal Logical Layer function");
        return RIO_FALSE;
    }

    switch ( buf_Entry->trx_Info.req_FType )
    {
        case RIO_FC:
        {
                    return RIO_TRUE;

                default:
                    return RIO_FALSE;
            }

            break;
        }

}


/* GDA: Bug#125 - Support for Flow control packet */
/***************************************************************************
 * Function :    RIO_LL_Kernel_FC_Request_Done
 *
 * Description:  function invokes FC done callback
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Kernel_FC_Request_Done(
    RIO_LL_DS_T      *handle, 
    RIO_LL_BUF_TAG_T buf_Tag, 
    RIO_REQ_RESULT_T result
)
{
    RIO_LL_BUFFER_T *buf_Entry;

#ifdef _DEBUG
    assert(handle);
#endif
    
    /* get appropriate buffer entry */
    buf_Entry = get_Buffer_Entry( handle, buf_Tag);
    if (buf_Entry == NULL)
    {
        RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "kernel Flow Control request done function");
        return RIO_ERROR;
    }
    
    /* set semaphor state */
    add_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );


    /* invoke callback */
    handle->callback_Tray.rio_FC_Request_Done(
        handle->callback_Tray.local_Device_Context, 
        buf_Entry->trx_Info.user_Trx_Tag, 
        result, 
        buf_Entry->trx_Info.resp_Status 
    );
    
    /* unset semaphor state */
    remove_Semaphor_State( handle, RIO_LL_SEMAPHOR_DONE );

    return RIO_OK;
}


/* GDA: Bug#125 - Support for Flow control packet */
/***************************************************************************
 * Function :    RIO_LL_FC_Request
 *
 * Description:  function receives data structure describing necessary 
 *               
 *               FC. If data is correct buffer is assigned
 *               and request is processed by its transaction handler.
 *
 * Returns:      RIO_OK in case of successfully accepted request.
 *               RIO_PARAM_INVALID in case of incorrect data.
 *               RIO_RETRY in case of unavailable buffer entry.
 *               RIO_ERROR in case of error
 *               
 * Notes:
 *
 **************************************************************************/

int RIO_LL_FC_Request( 
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag        /* user-assigned tag of the request */
    )
{
    RIO_LL_DS_T      *ll_Data;    /* pointer to the data structure                        */
    RIO_LL_BUFFER_T  *buf_Entry;  /* pointer to the buffer entry to store the transaction */
    RIO_LL_BUF_TAG_T buf_Tag;     /* buffer tag of the buffer entry                       */
    RIO_FC_REQ_T     *buf_Req;    /* copy of structure to place it to buffer              */


    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;


    /* check semaphore*/
    if ( !check_Semaphor_State ( ll_Data, RIO_LL_SEMAPHOR_INIT ) )
    {
        /* this function can not be called recursively from any previously invoked LL callback */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INVALID_RECURSIVE_CALL, "FC request");
        return RIO_ERROR;
    }

    /* check reset condition */
    if ( is_In_Reset(ll_Data) )
    {
        /* if in reset state do nothing */
        RIO_LL_Message( ll_Data, RIO_LL_WARN_IN_RESET, "Flow Control request");
        return RIO_OK;
    }

    /* check that user has requested transaction with correct attributes */
    if( rq == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PTR, "request structure", "Flow Control request" );
        return RIO_PARAM_INVALID;
    }
    rq->soc = 1;	/* SOC = 1; mean, it is an end point */
    rq->prio= 2;

    /* check priority of the request */
    if( rq->prio > RIO_MAX_REQUEST_PRIORITY )
    {
        /* priority of the request packet can not be larger than 2 */
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_WRONG_PRIO, "Flow Control", RIO_MAX_REQUEST_PRIORITY );
        return RIO_PARAM_INVALID;
    }

    /* data is correct. Check if PE can issue such a transaction */
    if ( !can_Issue( ll_Data, RIO_LL_FC,0 ) )
        RIO_LL_Message( ll_Data, RIO_LL_WARN_CANNOT_ISSUE, "Flow Conrtol", 0 );

    /* data is correct try to allocate buffer for it */
    if ( (buf_Tag = allocate_Buffer_Entry( ll_Data, rq->prio, RIO_TRUE)) == RIO_ERROR)
        return RIO_RETRY;

    /* get buffer entry */
    buf_Entry = get_Buffer_Entry( ll_Data, buf_Tag );
    if ( buf_Entry == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_GET_BUFFER, buf_Tag, "Flow Control request" );
        return RIO_ERROR;
    }

    /* do filling buffer structure */

    /* find handler for the transation */
    buf_Entry->kernel_Info.handler = find_Handler(
        ll_Data,
        RIO_TRUE,
        RIO_LL_FC,
        0
    );
    if (buf_Entry->kernel_Info.handler == NULL)
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_LOCAL_NO_HANDLER, "FC", 0 );
        return RIO_ERROR;
    }

    /* perform address translation and set local/remote attribute in the buffer */
    /* if( ll_Data->callback_Tray.rio_Tr_Local_FC == NULL )
        buf_Entry->kernel_Info.is_Local = RIO_TRUE;
    else*/
       buf_Entry->kernel_Info.is_Local = RIO_FALSE;/* (ll_Data->callback_Tray.rio_Tr_Local_FC(
            ll_Data->callback_Tray.address_Translation_Context,
            &(buf_Entry->trx_Info.tr_Info[0])) == RIO_ROUTE_TO_LOCAL ) ? RIO_TRUE : RIO_FALSE;*/

    buf_Entry->trx_Info.multicast_Size = 1;/* there is no mulaticast ability for the IO requests */

    /* create copy of the request structure */
    buf_Req = ( RIO_FC_REQ_T* )malloc( sizeof( RIO_FC_REQ_T ) );
    if ( buf_Req == NULL )
    {
        deallocate_Buffer_Entry( ll_Data, buf_Tag );    
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, sizeof( RIO_FC_REQ_T), "Flow Control request structure");
        return RIO_ERROR;
    }
    *buf_Req = *rq;

    /* fill in transaction info buffer structure */ 
    buf_Entry->trx_Info.prio         = buf_Req->prio;
    buf_Entry->trx_Info.user_Trx_Tag = trx_Tag;
    translate_LL_FType_To_Spec_FType( 
        RIO_LL_FC, 
        0, 
        &(buf_Entry->trx_Info.req_FType),
        &(buf_Entry->trx_Info.req_TType)
    );
    buf_Entry->trx_Info.req_Type    = RIO_LL_FC;
    buf_Entry->trx_Info.req         = buf_Req;
    buf_Entry->trx_Info.offset      = buf_Req->offset;
    buf_Entry->trx_Info.x_On_Off      = buf_Req->x_On_Off;
    buf_Entry->trx_Info.soc      = buf_Req->soc;

    /* process transaction */
    buf_Entry->trx_Info.state   = RIO_LL_STATE_INIT;

    if ( buf_Entry->kernel_Info.is_Local )
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_LOCAL );
    else 
        return process_Transaction( ll_Data, buf_Tag, RIO_EVENT_START_TO_REMOTE );
}


/* GDA: Bug#125 - Support for Flow control packet */
/***************************************************************************
 * Function :    RIO_LL_Congestion
 *
 * Description:  this function  call's  the Transport Layer when 
 *               congestion occurs from test_bench.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        may issue error messages.
 *               may modify request queue.
 *
***************************************************************************/
int RIO_LL_Congestion(RIO_HANDLE_T   handle,
                       RIO_TAG_T      trx_Tag)
		           
{
    RIO_LL_DS_T      *ll_Data;    /* pointer to the data structure */
    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;
    if( ll_Data == NULL )
       /* can not generate error message, because don't have any error function bind */
      return RIO_PARAM_INVALID;

     ll_Data->callback_Tray.rio_TL_Congestion(
                  ll_Data->callback_Tray.rio_TL_Outbound_Context,
                  trx_Tag
                   );

         return RIO_OK;
}


/* GDA: Bug#125 - Support for Flow control packet */
/***************************************************************************
 * Function :    RIO_LL_FC_TO_Send
 *
 * Description:  this function is to be called by the Transport Layer when 
 *               request from the LL to send packet is accepted.
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:        may issue error messages.
 *               may modify request queue.
 *
 **************************************************************************/
int RIO_LL_FC_To_Send(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_LL_DS_T    *ll_Data;  /* pointer to the LL data structure */
    RIO_FC_REQ_T   *rq;
    static int k = 1;

    /* convert handle to point to correct memory type */
    ll_Data = (RIO_LL_DS_T*)handle;

    if( ll_Data == NULL )
        /* can not generate error message, because don't have any error function bind */
        return RIO_PARAM_INVALID;

        rq = ( RIO_FC_REQ_T* )malloc( sizeof( RIO_FC_REQ_T ) );
         if ( rq == NULL )
        {
           RIO_LL_Message( handle, RIO_LL_ERROR_CANNOT_ALLOCATE_MEMORY, sizeof( RIO_FC_REQ_T), "FC request structure");
           return RIO_ERROR;
        }
	  
	 if(k < 3)	/* Count no. of times the fc invoked */
	 {
	   if(k==1)
	 	rq->x_On_Off=1;
	   else
	 	rq->x_On_Off=0;

	   if(RIO_LL_FC_Request(handle,rq, tag) == RIO_OK)            
		printf("\nCalling FC packet from LL Layer\n"); 
	 }
	 k++;		/* Increament the fc count */

    return RIO_OK;

}

/* EOF */
