/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_gsm_local_handlers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains handlers which implement state machines for
*               servicing of locally requested gsm transactions
*
* Notes:        
*
******************************************************************************/

#include "rio_ll_trx_handlers.h"
#include "rio_ll_kernel.h"

#include <assert.h>


/***************************************************************************
 * Function :    RIO_LL_Local_GSM_Read_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested GSM read transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* remote - we've got to go to another PE which home memory for that cacheline*/
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_TO_LOCAL:
                {
                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    /* we are home memory for the cacheline */
                    switch ( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_SHARED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_OWNER, RIO_TRUE);    
                            /*    RIO_LL_Kernel_Send_Altered_Request(handle, READ_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        } 
        case RIO_LL_STATE_SEND_REQUEST_TO_HOME:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                    /* return data */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_DONE_INTERVENTION:
                {
                    RIO_LL_Kernel_Set_Received_Done_Message(handle, buf_Tag);
                    if( RIO_LL_Kernel_Received_Data_Only_Message(handle, buf_Tag) == RIO_TRUE )
                        /* data is already returned to the local device */
                        return RIO_LL_COMPLETE;
                    else 
                    {
                        /* wait for a data only message; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_DATA_ONLY:
                {
                    /* this is due to an intervention, DONE_INTERVENTION should come separately */
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                    RIO_LL_Kernel_Set_Received_Data_Only_Message(handle, buf_Tag);
                    /* return data to the local device */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    /* check whether DONE_INTERVENTION has been received */
                    if( RIO_LL_Kernel_Received_Done_Message(handle, buf_Tag) == RIO_TRUE )
                        return RIO_LL_COMPLETE;
                    else
                    {
                        /* wait for the DONE_INTERVENTION; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* retry READ_HOME request */
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_OWNER:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_OWNER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    /* intervention data is written to memory; update directory and return data to local device */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }   /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}   /* end of RIO_LL_Local_GSM_Read_Request_H */




/***************************************************************************
 * Function :    RIO_LL_Local_GSM_I_Read_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested GSM instruction read transaction 
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_I_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* remote - we've got to go to another PE which home memory for that cacheline*/
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_TO_LOCAL:
                {
                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE 
                    )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    /* we are home memory for the cacheline */
                    switch ( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_SHARED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_OWNER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* other events are erroneous in this state */
                default:
                    return RIO_LL_ERROR;
            }
            break;
        } 
        case RIO_LL_STATE_SEND_REQUEST_TO_OWNER:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_OWNER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_HOME:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                    /* return data */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_DONE_INTERVENTION:
                {
                    RIO_LL_Kernel_Set_Received_Done_Message(handle, buf_Tag);
                    if( RIO_LL_Kernel_Received_Data_Only_Message(handle, buf_Tag) == RIO_TRUE )
                        /* data is already returned to the local device */
                        return RIO_LL_COMPLETE;
                    else 
                    {
                        /* wait for a data only message; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_DATA_ONLY:
                {
                    /* this is due to an intervention, DONE_INTERVENTION should come separately */
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_SHARED);
                    RIO_LL_Kernel_Set_Received_Data_Only_Message(handle, buf_Tag);
                    /* return data to the local device */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    /* check whether DONE_INTERVENTION has been received */
                    if( RIO_LL_Kernel_Received_Done_Message(handle, buf_Tag) == RIO_TRUE )
                        return RIO_LL_COMPLETE;
                    else
                    {
                        /* wait for the DONE_INTERVENTION; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* retry READ_HOME request */
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    /* intervention data is written to memory; update directory and return data to local device */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        default: {}
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}




/***************************************************************************
 * Function :    RIO_LL_Local_GSM_Read_To_Own_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested GSM read transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_Read_To_Own_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* remote - we've got to go to another PE which home memory for that cacheline*/
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_TO_LOCAL:
                {
                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    /* we are home memory for the cacheline */
                    switch ( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_TO_OWN_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_SHARED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, (mask ~= my_id), my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_WO_MY_ID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        } 
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_LOCAL_MODIFIED);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch (event)
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* final response for DKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* return data */
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }               
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_OWNER:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* remotely owned */
                    /* return data */
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                {
                    /* due to address collision with CASTOUT or FLUSH */
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_TO_OWN_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* due to address collision with CASTOUT or FLUSH */
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        {
                            /* return data */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* mask_id must match received_srcid or error */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_TO_OWN_OWNER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_SHARED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_HOME:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                    /* return data */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_DONE_INTERVENTION:
                {
                    RIO_LL_Kernel_Set_Received_Done_Message(handle, buf_Tag);
                    if( RIO_LL_Kernel_Received_Data_Only_Message(handle, buf_Tag) == RIO_TRUE )
                        /* data is already returned to the local device */
                        return RIO_LL_COMPLETE;
                    else 
                    {
                        /* wait for a data only message; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_DATA_ONLY:
                {
                    /* this is due to an intervention, DONE_INTERVENTION should come separately */
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                    RIO_LL_Kernel_Set_Received_Data_Only_Message(handle, buf_Tag);
                    /* return data to the local device */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    /* check whether DONE_INTERVENTION has been received */
                    if( RIO_LL_Kernel_Received_Done_Message(handle, buf_Tag) == RIO_TRUE )
                        return RIO_LL_COMPLETE;
                    else
                    {
                        /* wait for the DONE_INTERVENTION; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* retry READ_HOME request */
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        default:
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Local_GSM_DCI_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested data cache invalidate request
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_DCI_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* remote - we've got to go to another PE which home memory for that cacheline*/
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_TO_LOCAL:
                {

                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    /* we are home memory for the cacheline */
                    switch ( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_LOCAL_MODIFIED);
                            RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                            return RIO_LL_COMPLETE;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* cache paradox; DKILL is write-hit-on-shared */
                            RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                            return RIO_LL_COMPLETE;
                        }
                        case RIO_LL_SHARED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_WO_MY_ID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, (mask ~= my_id), my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        } 
        case RIO_LL_STATE_SEND_REQUEST_TO_HOME:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* final response for DKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE); */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_LOCAL_MODIFIED);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }               
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        default:
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}


/***************************************************************************
 * Function :    RIO_LL_Local_GSM_IKILL_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested IKILL request
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_IKILL_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_LOCAL:
                {
                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    /* we are home memory; send multicast request to all participants in the coherence domain */
                    if( RIO_LL_Kernel_Coh_Domain_Size(handle) < 2 )
                    {
                        /* don't send IKILL_SHARERS */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                        return RIO_LL_COMPLETE;
                    }
                    else
                    {
                        RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                            RIO_MASK_PARTICIPANT_ID, RIO_PL_GSM_IKILL_SHARER, RIO_TRUE);    
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                        return RIO_LL_PROGRESS;
                    }
                    
                    break;
                }
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* Send IKILL_HOME to home memory */
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);    
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* final response for IKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }               
                /* any other response event isn't defined by the protocol and is erroneous */
                /* RETRY responses have been handled by PL multicast engine */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_HOME:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_EXCLUSIVE);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        default: 
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}


/***************************************************************************
 * Function :    RIO_LL_Local_GSM_IO_Read_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested IO Read request
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_IO_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* remote - we've got to go to another PE which home memory for that cacheline*/
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_TO_LOCAL:
                {
                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE
                    )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                    /* we are home memory for the cacheline */
                    switch ( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ_LATEST, RIO_SNOOP_IO_READ_HOME);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_SHARED:
                        {
                            /* go to local memory */
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, IO_READ_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_IO_READ_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        } 
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* return data to requesting device */
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    /* go to local memory */
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch (event)
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_OWNER:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            /* return data from memory */
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* mask_id must match received_srcid */
                            /* send IO_READ_OWNER */
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, IO_READ_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_IO_READ_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                        {
                            /* cache paradox */
                            RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                            return RIO_LL_COMPLETE;
                        }                   
                    }
                    break;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_HOME:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_DONE_INTERVENTION:
                {
                    RIO_LL_Kernel_Set_Received_Done_Message(handle, buf_Tag);
                    if( RIO_LL_Kernel_Received_Data_Only_Message(handle, buf_Tag) == RIO_TRUE )
                        /* data is already returned to the local device */
                        return RIO_LL_COMPLETE;
                    else 
                    {
                        /* wait for a data only message; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_DATA_ONLY:
                {
                    /* this is due to an intervention, DONE_INTERVENTION should come separately */
                    RIO_LL_Kernel_Set_Received_Data_Only_Message(handle, buf_Tag);
                    /* return data to the local device */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_TRUE, RIO_REQ_DONE);
                    /* check whether DONE_INTERVENTION has been received */
                    if( RIO_LL_Kernel_Received_Done_Message(handle, buf_Tag) == RIO_TRUE )
                        return RIO_LL_COMPLETE;
                    else
                    {
                        /* wait for the DONE_INTERVENTION; stay in the same state */
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                        return RIO_LL_PROGRESS;
                    }
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* retry READ_HOME request */
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        default:
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}


/***************************************************************************
 * Function :    RIO_LL_Local_GSM_TLBIE_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested TLBIE request
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_TLBIE_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_LOCAL:
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* send multicast request to all participants in the coherence domain */
                    /* RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, TLBIE, participant_id); */
                    RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                        RIO_MASK_PARTICIPANT_ID, RIO_PL_GSM_TLBIE, RIO_TRUE);    
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                    return RIO_LL_PROGRESS;

                case RIO_EVENT_RESPONSE_DONE:            
                {
                    /* invoke done function */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        default: 
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Local_GSM_TLBSYNC_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested IKILL request
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_TLBSYNC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_LOCAL:
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* send request to all participants in the coherence domain */
                    /* RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, TLBSYNC, participant_id); */
                    RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                        RIO_MASK_PARTICIPANT_ID, RIO_PL_GSM_TLBSYNC, RIO_TRUE);    
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                    return RIO_LL_PROGRESS;

                case RIO_EVENT_RESPONSE_DONE:            
                {
                    /* invoke done function */
                    /* can not receive retry, because multicast engine will handle this automatically */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        default: 
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Local_GSM_Castout_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested CASTOUT request
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_Castout_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_LOCAL:
                {
                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE 
                    )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    switch ( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            /* if processor is doing a CASTOUT this is the only legal state */
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_SHARED:
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* error */
                            RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                            return RIO_LL_COMPLETE;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_START_TO_REMOTE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                    return RIO_LL_PROGRESS;                                  
                case RIO_EVENT_RESPONSE_DONE:            
                {
                    /* invoke done function */
                    /* can not receive retry, because multicast engine will handle this automatically */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        default: 
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}            


/***************************************************************************
 * Function :    RIO_LL_Local_GSM_Flush_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested FLUSH transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_GSM_Flush_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* remote - we've got to go to another PE which home memory for that cacheline*/
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_TO_LOCAL:
                {
                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE )
                    {
                        /* invoke done function with error */
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                        return RIO_LL_COMPLETE;
                    }

                    /* we are home memory for the cacheline */
                    switch ( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                            if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                            {
                                /* update memory */
                                RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                                return RIO_LL_PROGRESS;
                            }
                            else
                            {
                                RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                                return RIO_LL_COMPLETE;
                            }
                            break;                            
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_TO_OWN_OWNER, mask_id, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_SHARED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, (mask ~= my_id), my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_WO_MY_ID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        } 
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch (event)
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* final response for DKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                    
                    if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                    {
                        RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                        return RIO_LL_PROGRESS;
                    }
                    else
                    {
                        RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                        RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                        return RIO_LL_COMPLETE;
                    }                
                }               
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_OWNER:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* remotely owned */
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                {
                    /* due to address collision with CASTOUT or FLUSH */
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                            if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                            {
                                /* update memory */
                                RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                                return RIO_LL_PROGRESS;
                            }
                            else
                            {
                                RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                                return RIO_LL_COMPLETE;
                            }
                            break;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                            if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                            {
                                /* update memory */
                                RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                                return RIO_LL_PROGRESS;
                            }
                            else
                            {
                                RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                                return RIO_LL_COMPLETE;
                            }
                            break;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* mask_id must match received_srcid or error */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, READ_TO_OWN_OWNER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_SHARED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_HOME:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Local_Responce(handle, buf_Tag, RIO_LR_OK);
                    /* return data */
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_DONE);
                    return RIO_LL_COMPLETE;
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* retry READ_HOME request */
                    RIO_LL_Kernel_Send_Request(handle, buf_Tag);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_HOME);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event isn't defined by the protocol and is erroneous */
                default:
                {
                    RIO_LL_Kernel_GSM_Request_Done(handle, buf_Tag, RIO_FALSE, RIO_REQ_ERROR);
                    return RIO_LL_COMPLETE;
                }                   
            }
            break;
        }
        default:
            return RIO_LL_ERROR;
    }

    /* transaction handler doesn't handle all possible states - trx handler error */
    return RIO_LL_ERROR;
}



/* EOF */

