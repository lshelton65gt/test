/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_errors.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  logical layer errors/warnings messages handlers source
*
* Notes:
*
******************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "rio_ll_ds.h"
#include "rio_ll_errors.h"


typedef enum {
    RIO_LL_MSG_WARNING_MESSAGE = 0,
    RIO_LL_MSG_ERROR_MESSAGE,
    RIO_LL_MSG_NOTE_MESSAGE
} RIO_LL_MSG_TYPES_T;

typedef struct {
    char              *string;
    int               count_Of_Add_Args;
    RIO_LL_ARG_TYPE_T arg_Types[3]; /* the maximum count of the additional arguments is 3 */
    RIO_LL_MSG_TYPES_T msg_Type;
} LL_ERROR_WARNING_ENTRY_T;

/*
 * array of error strings
 */
static LL_ERROR_WARNING_ENTRY_T ll_Message_Strings[] = {
    {"Invalid recursive call of the function %s.\n", 1, {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"NULL pointer to the %s supplied in the %s.\n", 2, 
        {RIO_LL_CHAR_ARG, RIO_LL_CHAR_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Priority of the %s request may not be higher than %d.\n", 2, 
        {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Data payload for the %s request may not exceed %d doublewords.\n", 2, 
        {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"%s transaction request has null data payload size or size of the data requested.\n", 1, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"%s transaction request has subdoubleword data payload crossing doubleword boundary.\n", 1, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Subdoubleword %s transaction with null bytes number requested.\n", 1, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"%s transaction of the LL ttype %d shall supply data.\n", 2, 
        {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Data payload for the ATOMIC transactions is illegal (can be 1,2,4 bytes only).\n", 0, 
        {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"SWRITE transaction may not specify subdoubleword data payload.\n", 0, 
        {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Cannot get buffer entry for tag %d during %s.\n", 2, 
        {RIO_LL_INT_ARG, RIO_LL_CHAR_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Cannot allocate %d bytes of memory for the %s.\n", 2, 
        {RIO_LL_INT_ARG, RIO_LL_CHAR_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Size of the data payload for the %s transaction may only be %s doublewords. But %d was requested\n", 3, 

	    /* GDA: Bug#124 - Support for Data Streaming packet */
        {RIO_LL_INT_ARG, RIO_LL_CHAR_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Size of the data payload for Data streaming %s transaction may only be %s acceptable. But %d was requested\n", 3, 
        {RIO_LL_INT_ARG, RIO_LL_CHAR_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Size of the MTU  for Data streaming  %s transaction may only be %s half words. But %d was requested\n", 3, 
        {RIO_LL_INT_ARG, RIO_LL_CHAR_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Size of the payload for Data streaming  %s transaction is  %d bytes. But %d was recieved\n", 3, 
	    /* GDA: Bug#124 - Support for Data Streaming packet */
	    
        {RIO_LL_CHAR_ARG, RIO_LL_CHAR_ARG, RIO_LL_INT_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Size of the data payload for the locally requested GSM transaction of ttype %d may only be %d doublewords.\n", 2, 
        {RIO_LL_INT_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Data payload for the %s transaction may not cross cache granule boundary.\n", 1, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Misaligned data payload for the GSM FLUSH request.\n", 0, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Received remote transaction with the invalid data payload encoding.\n", 
        0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Received remote transaction with data payload different from that stated in header.\n", 
        0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Transaction handler for the local %s transaction of LL ttype %d cannot be found"
        "(i.e. handling of the transaction is not supported).\n", 2, 
        {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Transaction handler for the remote transaction of the ftype %d and ttype %d cannot be found.\n", 2, 
        {RIO_LL_INT_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Number of %s may not exceed %d for the local message request.\n", 2, 
        {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Number of %s may not exceed %d for the remote message request.\n", 
        2, {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Nonlast message segment shall supply data payload equal to stated in header.\n", 0, 
        {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Unknown local request result %d.\n", 1, {RIO_LL_INT_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Transaction for the %s cannot be found.\n", 1, {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Error during internal Logical Layer %s queue servicing.\n", 1, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Transaction handler for the %s transaction of ftype %d and ttype %d completed with error.\n", 
        3, {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_INT_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Logical Layer does not have pointer to necessary %s callback.\n", 1, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Logical Layer initialize function was invoked outside reset state.\n", 
        0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Logical Layer received nonparticipant GSM transaction ftype %d ttype %d.\n", 
        2, {RIO_LL_INT_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
    {"Logical layer has received write request with incorrect actual payload. \n",
        0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE },
        
    {"PE has no memory.\n", 0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE },
    {"PE has no processor.\n", 0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE },
    {"PE has no Doorbell hardware.\n", 0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE },
    {"PE has no Mailbox hardware.\n", 0, {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE },
    {"Logical Layer is in reset state. But %s was invoked.\n", 1, 
        {RIO_LL_CHAR_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE },
    {"PE is not configured to issue locally requested %s transaction of LL ttype %d.\n", 2, 
        {RIO_LL_CHAR_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE },
    {"Incoming transaction of ftype %d ttype %d cannot be serviced.\n", 2, 
        {RIO_LL_INT_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE },
    {"Response of ftype %d arrived for request of ftype %d.\n", 2, 
        {RIO_LL_INT_ARG, RIO_LL_INT_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE  },
    {"Remote response has invalid combination of the ftype %d transaction %d and status %d fields.\n", 3, 
        {RIO_LL_INT_ARG, RIO_LL_INT_ARG, RIO_LL_INT_ARG}, RIO_LL_MSG_WARNING_MESSAGE  },
    {"Logical Layer received response with the wrong data payload size.\n", 0, 
        {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE  },
    {"Logical Layer has got unrecognized transaction handler completion code %d.\n", 1, 
        {RIO_LL_INT_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_WARNING_MESSAGE  },
    {"Address-only and transaction-only operations shall have wdptr and rdsize set to logic zeros.\n", 0, 
        {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE  },
    {"TLBSYNC shall have address and xamsbs fields set to logic zeros %d.\n", 0, 
        {RIO_LL_NO_ARG, RIO_LL_NO_ARG, RIO_LL_NO_ARG}, RIO_LL_MSG_ERROR_MESSAGE  }

};

static const int ll_Message_Strings_Count = sizeof(ll_Message_Strings) / sizeof(ll_Message_Strings[0]);


static char ll_Error_Prefix_Str[]   = "Error{#%lu}: ";
static char ll_Warning_Prefix_Str[] = "Warning{#%lu}: ";
static char ll_Note_Prefix_Str[]    = "Info{#%lu}: ";


/***************************************************************************
 * Function :   RIO_LL_Message
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns:     RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Message(
    RIO_LL_DS_T     *handle, 
    RIO_LL_MESSAGES_T message_Type, 
    ...
)
{
    /* 
     * buffer to store error message and pointer to char which is
     * necessary for argument of error type 1 because it's of this
     * type
     */
    char message_Buffer[255];
    char temp_Buffer[255];

    /* 
     * additional information which shall present in error message
     * is passing through additional (optional) parameters:
     * add_Args[0]...[2]. 
     * i - loop counter
     */
    unsigned long add_Args[3] = {0, 0, 0}; /* additional arguments */
    int i;

    /* this is necessary to detect additional parameters */
    va_list pointer_To_Next_Arg;

#ifdef _DEBUG
    assert( handle );
#endif 

    /*
     * check all using pointers to be not NULL, check
     * that type of error is within corresponding array
     * and when invoke callback with actual parameters.
     * actual error message obtain using error type as an index
     */
    if (handle == NULL || 
        handle->callback_Tray.rio_User_Msg == NULL ||
        (int)message_Type < 0 || 
        (int)message_Type > (ll_Message_Strings_Count - 1))
        return RIO_ERROR;

    /* 
     * get additional parameters, accordingly to arg_Types array 
     * in the structure decribing message
     */
    va_start(pointer_To_Next_Arg, message_Type);

    for (i = 0; i < ll_Message_Strings[(int)message_Type].count_Of_Add_Args; i++)
    {
        if ( ll_Message_Strings[(int)message_Type].arg_Types[i] == RIO_LL_CHAR_ARG )
        {
            add_Args[i] = (unsigned long)va_arg(pointer_To_Next_Arg, char*);
        }
        else if ( ll_Message_Strings[(int)message_Type].arg_Types[i] == RIO_LL_INT_ARG )
        {
            add_Args[i] = va_arg(pointer_To_Next_Arg, unsigned long);     
        }
    }

    va_end(pointer_To_Next_Arg);

    /* detect the type of message */
    switch (ll_Message_Strings[(int)message_Type].msg_Type)
    {
        case RIO_LL_MSG_WARNING_MESSAGE:
            sprintf(message_Buffer, ll_Warning_Prefix_Str, (int)message_Type);
            break;
        
        case RIO_LL_MSG_ERROR_MESSAGE:
            sprintf(message_Buffer, ll_Error_Prefix_Str, (int)message_Type);
            break;
        
        case RIO_LL_MSG_NOTE_MESSAGE:
            sprintf(message_Buffer, ll_Note_Prefix_Str, (int)message_Type);
            break;
        
        default:
            return RIO_ERROR;
    
    } 

    /*obtain actual message*/
    switch (ll_Message_Strings[(int)message_Type].count_Of_Add_Args)
    {
        case 0: /* message has no arguments */
            sprintf(temp_Buffer, ll_Message_Strings[(int)message_Type].string);
            break;

        case 1: /* message has one argument */
            sprintf(temp_Buffer, ll_Message_Strings[(int)message_Type].string,
                add_Args[0]);
            break;
 
        case 2: /* message has two arguments */
            sprintf(temp_Buffer, ll_Message_Strings[(int)message_Type].string,
                add_Args[0], add_Args[1]);
            break;

        case 3: /* message has three arguments */
            sprintf(temp_Buffer, ll_Message_Strings[(int)message_Type].string,
                add_Args[0], add_Args[1], add_Args[2]);
            break;

        default:
            return RIO_ERROR;
        
    }

    strcat(message_Buffer, temp_Buffer);

    /* print result error message*/
    handle->callback_Tray.rio_User_Msg(
        handle->callback_Tray.rio_Msg, 
        message_Buffer);

    return RIO_OK;
  
}


/**************************************************************************/
