/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_entry.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains layer entry functions, such as instantiation
*               initialization and callback tray exchange routines
*
* Notes:        
*
******************************************************************************/


#include "rio_ll_entry.h"
#include "rio_ll_errors.h"

#include <stdlib.h>

/* transactions masks definitions to find out necessary callbacks set */
#define RIO_LL_GSM_TRX_MASK        0xFFC00000

/*
#define RIO_LL_IO_TRX_MASK         0x0000F1F0 */
#define RIO_LL_IO_TRX_MASK         0x0000F1F8 /* GDA: Bug#133 - Support for ttype C for type 5 packet 
						      Refer part1:I/O Logical Spec, Rev1.3 (pg 50) */
#define RIO_LL_MP_TRX_MASK         0x00000800
#define RIO_LL_DOORBELL_TRX_MASK   0x00000400
#define RIO_LL_PORT_WRITE_TRX_MASK 0x00000004

/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_LL_DS_TRX_MASK         0x00040000 
/* GDA: Bug#124 */

/* GDA: Bug#125 - Support for Flow control packet */
#define RIO_LL_FC_TRX_MASK   0x00000080
/* GDA: Bug#125 */

/***************************************************************************
 * Function :    RIO_LL_Create_Instance
 *
 * Description:  this function creates instance of the LL.
 *
 * Returns:      RIO_OK, RIO_ERROR or RIO_PARAM_INVALID
 *
 * Notes:        this is the only function in the layer which allocates
 *               memory for the pointer passed as function parameter
 *
 **************************************************************************/
int RIO_LL_Create_Instance(
    RIO_HANDLE_T        *handle, 
    RIO_LL_INST_PARAM_T *param
)
{
    /* pointer to the data of the newly created instance */
    RIO_LL_DS_T        *ll_Data = NULL;
    RIO_LL_QUEUE_T     *element = NULL, *prev = NULL;
    unsigned int       i;

    /* check parameters */
    if ( handle == NULL)
        return RIO_PARAM_INVALID;

    if ( param == NULL )
        return RIO_PARAM_INVALID;

    /* allocate memory for the instance's data and initialize it */
    ll_Data = (RIO_LL_DS_T *)malloc(sizeof( RIO_LL_DS_T ));
    /* can't allocate memory */
    if ( ll_Data == NULL )
        return RIO_ERROR;

    /* check buffer sizes */
    if ( param->inbound_Buffer_Size == 0 || param->outbound_Buffer_Size == 0 )
        return RIO_PARAM_INVALID;

    /* check coherency domain size */
    if ( param->coh_Domain_Size > RIO_MAX_COH_DOMAIN_SIZE )
        return RIO_PARAM_INVALID;
    
    /* set instance parameters */
    ll_Data->inst_Params.coh_Granule_Size_32 = param->coh_Granule_Size_32;
    ll_Data->inst_Params.coh_Domain_Size     = param->coh_Domain_Size;

    ll_Data->inst_Params.source_Trx = param->source_Trx;
    ll_Data->inst_Params.dest_Trx   = param->dest_Trx;

    ll_Data->inst_Params.has_Memory             = param->has_Memory;
    ll_Data->inst_Params.has_Processor          = param->has_Processor;
    ll_Data->inst_Params.has_Doorbell           = param->has_Doorbell;
    ll_Data->inst_Params.mbox1                  = param->mbox1;
    ll_Data->inst_Params.mbox2                  = param->mbox2;
    ll_Data->inst_Params.mbox3                  = param->mbox3;
    ll_Data->inst_Params.mbox4                  = param->mbox4;
     /* GDA : Bug #123 Support for extended mailbox (xmbox) feature */
    ll_Data->inst_Params.mbox5                  = param->mbox5;
    ll_Data->inst_Params.mbox6                  = param->mbox6;
    ll_Data->inst_Params.mbox7                  = param->mbox7;
    ll_Data->inst_Params.mbox8                  = param->mbox8;
    ll_Data->inst_Params.mbox9                  = param->mbox9;
    ll_Data->inst_Params.mbox10                 = param->mbox10;
    ll_Data->inst_Params.mbox11                 = param->mbox11;
    ll_Data->inst_Params.mbox12                 = param->mbox12;
    ll_Data->inst_Params.mbox13                 = param->mbox13;
    ll_Data->inst_Params.mbox14                 = param->mbox14;
    ll_Data->inst_Params.mbox15                 = param->mbox15;
    ll_Data->inst_Params.mbox16                 = param->mbox16;
    ll_Data->inst_Params.mbox17                 = param->mbox17;
    ll_Data->inst_Params.mbox18                 = param->mbox18;
    ll_Data->inst_Params.mbox19                 = param->mbox19;
    ll_Data->inst_Params.mbox20                 = param->mbox20;
    ll_Data->inst_Params.mbox21                 = param->mbox21;
    ll_Data->inst_Params.mbox22                 = param->mbox22;
    ll_Data->inst_Params.mbox23                 = param->mbox23;
    ll_Data->inst_Params.mbox24                 = param->mbox24;
    ll_Data->inst_Params.mbox25                 = param->mbox25;
    ll_Data->inst_Params.mbox26                 = param->mbox26;
    ll_Data->inst_Params.mbox27                 = param->mbox27;
    ll_Data->inst_Params.mbox28                 = param->mbox28;
    ll_Data->inst_Params.mbox29                 = param->mbox29;
    ll_Data->inst_Params.mbox30                 = param->mbox30;
    ll_Data->inst_Params.mbox31                 = param->mbox31;
    ll_Data->inst_Params.mbox32                 = param->mbox32;
    ll_Data->inst_Params.mbox33                 = param->mbox33;
    ll_Data->inst_Params.mbox34                 = param->mbox34;
    ll_Data->inst_Params.mbox35                 = param->mbox35;
    ll_Data->inst_Params.mbox36                 = param->mbox36;
    ll_Data->inst_Params.mbox37                 = param->mbox37;
    ll_Data->inst_Params.mbox38                 = param->mbox38;
    ll_Data->inst_Params.mbox39                 = param->mbox39;
    ll_Data->inst_Params.mbox40                 = param->mbox40;
    ll_Data->inst_Params.mbox41                 = param->mbox41;
    ll_Data->inst_Params.mbox42                 = param->mbox42;
    ll_Data->inst_Params.mbox43                 = param->mbox43;
    ll_Data->inst_Params.mbox44                 = param->mbox44;
    ll_Data->inst_Params.mbox45                 = param->mbox45;
    ll_Data->inst_Params.mbox46                 = param->mbox46;
    ll_Data->inst_Params.mbox47                 = param->mbox47;
    ll_Data->inst_Params.mbox48                 = param->mbox48;
    ll_Data->inst_Params.mbox49                 = param->mbox49;
    ll_Data->inst_Params.mbox50                 = param->mbox50;
    ll_Data->inst_Params.mbox51                 = param->mbox51;
    ll_Data->inst_Params.mbox52                 = param->mbox52;
    ll_Data->inst_Params.mbox53                 = param->mbox53;
    ll_Data->inst_Params.mbox54                 = param->mbox54;
    ll_Data->inst_Params.mbox55                 = param->mbox55;
    ll_Data->inst_Params.mbox56                 = param->mbox56;
    ll_Data->inst_Params.mbox57                 = param->mbox57;
    ll_Data->inst_Params.mbox58                 = param->mbox58;
    ll_Data->inst_Params.mbox59                 = param->mbox59;
    ll_Data->inst_Params.mbox60                 = param->mbox60;
    ll_Data->inst_Params.mbox61                 = param->mbox61;
    ll_Data->inst_Params.mbox62                 = param->mbox62;
    ll_Data->inst_Params.mbox63                 = param->mbox63;
    ll_Data->inst_Params.mbox64                 = param->mbox64;
  /* GDA : Bug#123 Support for extended mailbox (xmbox) feature */  

    ll_Data->inst_Params.inbound_Buffer_Size    = param->inbound_Buffer_Size;
    ll_Data->inst_Params.outbound_Buffer_Size   = param->outbound_Buffer_Size;

    /*GDA: Bug#43 Fix*/
    ll_Data->inst_Params.gda_Tx_Illegal_Packet  = param->gda_Tx_Illegal_Packet;
    ll_Data->inst_Params.gda_Rx_Illegal_Packet  = param->gda_Rx_Illegal_Packet;
    /*GDA: Bug#43 Fix*/

    /* initialize some instance parameters */
    ll_Data->callback_Tray.rio_User_Msg      = NULL;
    ll_Data->callback_Tray.rio_Msg              = NULL;

    /* allocate inbound buffer */
    ll_Data->remoteb_Size  = param->inbound_Buffer_Size;
    ll_Data->remote_Buffer = ( RIO_LL_BUFFER_T* )malloc( sizeof( RIO_LL_BUFFER_T ) * ll_Data->remoteb_Size);

    /* initialize inbound buffer entrues */
    for ( i = 0; i < ll_Data->remoteb_Size; i++ )
    {
        ll_Data->remote_Buffer[i].trx_Info.req        = NULL;
        ll_Data->remote_Buffer[i].trx_Info.snoop_Data = NULL;
        ll_Data->remote_Buffer[i].trx_Info.snoop_Mask = NULL;
    }

    /* allocate outbound buffer */
    ll_Data->locb_Size  = param->outbound_Buffer_Size;
    ll_Data->loc_Buffer = ( RIO_LL_BUFFER_T* )malloc( sizeof( RIO_LL_BUFFER_T ) * ll_Data->locb_Size);

    /* initialize outbound buffer entrues */
    for ( i = 0; i < ll_Data->locb_Size; i++ )
    {
        ll_Data->loc_Buffer[i].trx_Info.req        = NULL;
        ll_Data->loc_Buffer[i].trx_Info.snoop_Data = NULL;
        ll_Data->loc_Buffer[i].trx_Info.snoop_Mask = NULL;
    }

    /* allocate memory queue pool */
    ll_Data->target_Queues.memory_Queue_Head = NULL;
    prev = NULL;
    for ( i = 0; i < (ll_Data->remoteb_Size + ll_Data->locb_Size); i++)
    {
        element = ( RIO_LL_QUEUE_T* )malloc( sizeof( RIO_LL_QUEUE_T ) );
        element->next = prev;
        prev = element;
    }
    ll_Data->target_Queues.memory_Queue_Pool = element;

    /* allocate snoop queue pool */
    ll_Data->target_Queues.snoop_Queue_Head = NULL;
    prev = NULL;
    for ( i = 0; i < (ll_Data->remoteb_Size + ll_Data->locb_Size ); i++)
    {
        element = ( RIO_LL_QUEUE_T* )malloc( sizeof( RIO_LL_QUEUE_T ) );
        element->next = prev;
        prev = element;
    }
    ll_Data->target_Queues.snoop_Queue_Pool = element;

    /* allocate responce queue pool */
    ll_Data->target_Queues.rio_Resp_Queue_Head = NULL;
    prev = NULL;
    for ( i = 0; i < ll_Data->remoteb_Size; i++)
    {
        element = ( RIO_LL_QUEUE_T* )malloc( sizeof( RIO_LL_QUEUE_T ) );
        element->next = prev;
        prev = element;
    }
    ll_Data->target_Queues.rio_Resp_Queue_Pool = element;

    /* allocate request queue pool */
    ll_Data->target_Queues.rio_Req_Queue_Head = NULL;
    prev = NULL;
    for ( i = 0; i < (ll_Data->remoteb_Size + ll_Data->locb_Size); i++)
    {
        element = ( RIO_LL_QUEUE_T* )malloc( sizeof( RIO_LL_QUEUE_T ) );
        element->next = prev;
        prev = element;
    }
    ll_Data->target_Queues.rio_Req_Queue_Pool = element;

    /* allocate memory for the transaction handlers table */
    ll_Data->handler_Table = ( RIO_LL_HANDLER_TABLE_T* )malloc( sizeof( RIO_LL_HANDLER_TABLE_T ) * ( RIO_LL_END_TABLE + 1));
    if ( ll_Data->handler_Table == NULL )
    {
        free( ll_Data );
        return RIO_ERROR;
    }

    /* set transaction handlers pointers array (null-terminated) */
    for ( i = 0; i <= RIO_LL_END_TABLE; i++ )
    {
        ll_Data->handler_Table[i].name = i;
        switch ( i )
        {
            case RIO_LL_LOCAL_GSM_READ_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_Read_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_READ_TO_OWN_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_Read_To_Own_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_IO_READ_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_IO_Read_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_DKILL_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_DCI_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_IREAD_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_I_Read_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_IKILL:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_IKILL_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_TLBIE:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_TLBIE_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_TLBSYNC:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_TLBSYNC_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_CASTOUT:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_Castout_Request_H;
            break;

            case RIO_LL_LOCAL_GSM_FLUSH:
                ll_Data->handler_Table[i].func = RIO_LL_Local_GSM_Flush_Request_H;
            break;

            case RIO_LL_LOCAL_IO_NREAD:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_NRead_Request_H;
            break;

            case RIO_LL_LOCAL_IO_NWRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_NWrite_Request_H;
            break;

            case RIO_LL_LOCAL_IO_NWRITE_R:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_NWrite_R_Request_H;
            break;

            case RIO_LL_LOCAL_IO_SWRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_SWrite_Request_H;
            break;

            case RIO_LL_LOCAL_IO_ATOMIC_SET:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_Atomic_Set_Request_H;
            break;

            case RIO_LL_LOCAL_IO_ATOMIC_CLEAR:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_Atomic_Clear_Request_H;
            break;

            case RIO_LL_LOCAL_IO_ATOMIC_INC:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_Atomic_Inc_Request_H;
            break;

            case RIO_LL_LOCAL_IO_ATOMIC_DEC:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_Atomic_Dec_Request_H;
            break;

	    /* GDA: Added for Bug#133. To support the ttype C for type 5 packets */
            case RIO_LL_LOCAL_IO_ATOMIC_SWAP:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_Atomic_Swap_Request_H;
            break;
	    /* GDA: Added for Bug#133 */

            case RIO_LL_LOCAL_IO_ATOMIC_TSWAP:
                ll_Data->handler_Table[i].func = RIO_LL_Local_IO_Atomic_TSwap_Request_H;
            break;

            case RIO_LL_LOCAL_CONF_READ:
                ll_Data->handler_Table[i].func = RIO_LL_Local_Conf_Read_Request_H;
            break;

            case RIO_LL_LOCAL_CONF_WRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Local_Conf_Write_Request_H;
            break;

            case RIO_LL_LOCAL_CONF_PORT_WRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Local_Conf_Port_Write_Request_H;
            break;

            case RIO_LL_LOCAL_MP:
                ll_Data->handler_Table[i].func = RIO_LL_Local_Message_Request_H;
            break;

            case RIO_LL_LOCAL_DOORBELL:
                ll_Data->handler_Table[i].func = RIO_LL_Local_Doorbell_Request_H;
            break;

	    /* GDA: Bug#124 - Support for Data Streaming packet */
	     case RIO_LL_LOCAL_DS:
                  ll_Data->handler_Table[i].func = RIO_LL_Local_DS_Request_H;
             break;
	    /* GDA: Bug#124 - Support for Data Streaming packet */
	    
	    /* GDA: Bug#125 - Support for Flow control packet */
            case RIO_LL_LOCAL_FC:
                ll_Data->handler_Table[i].func = RIO_LL_Local_FC_Request_H;
            break;
	    /* GDA: Bug#125 - Support for Flow control packet */

            case RIO_LL_REMOTE_GSM_READ_OWNER:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_Read_Owner_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_READ_TO_OWN_OWNER:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_Read_To_Own_Owner_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_IO_READ_OWNER:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_IO_Read_Owner_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_READ_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_Read_Home_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_READ_TO_OWN_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_Read_To_Own_Home_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_IO_READ_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_IO_Read_Home_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_DKILL_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_DKILL_Home_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_IREAD_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_I_Read_Home_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_DKILL_SHARER:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_DKILL_Sharer_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_IKILL_HOME:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_IKILL_Home_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_IKILL_SHARER:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_IKILL_Sharer_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_TLBIE:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_TLBIE_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_TLBSYNC:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_TLBSYNC_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_CASTOUT:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_Castout_Request_H;
            break;

            case RIO_LL_REMOTE_GSM_FLUSH:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_GSM_Flush_Request_H;
            break;

            case RIO_LL_REMOTE_IO_NREAD:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_NRead_Request_H;
            break;

            case RIO_LL_REMOTE_IO_NWRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_NWrite_Request_H;
            break;

            case RIO_LL_REMOTE_IO_NWRITE_R:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_NWrite_R_Request_H;
            break;

            case RIO_LL_REMOTE_IO_SWRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_SWrite_Request_H;
            break;

            case RIO_LL_REMOTE_IO_ATOMIC_SET:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_Atomic_Set_Request_H;
            break;

            case RIO_LL_REMOTE_IO_ATOMIC_CLEAR:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_Atomic_Clear_Request_H;
            break;

            case RIO_LL_REMOTE_IO_ATOMIC_INC:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_Atomic_Inc_Request_H;
            break;

            case RIO_LL_REMOTE_IO_ATOMIC_DEC:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_Atomic_Dec_Request_H;
            break;

	    /* GDA: Added for Bug#133. To support the ttype C for type 5 packets */
            case RIO_LL_REMOTE_IO_ATOMIC_SWAP:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_Atomic_Swap_Request_H;
            break;
	    /* GDA: Added for Bug#133 */

            case RIO_LL_REMOTE_IO_ATOMIC_TSWAP:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_IO_Atomic_TSwap_Request_H;
            break;

            case RIO_LL_REMOTE_CONF_READ:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Conf_Read_Request_H;
            break;

            case RIO_LL_REMOTE_CONF_WRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Conf_Write_Request_H;
            break;

            case RIO_LL_REMOTE_CONF_PORT_WRITE:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Conf_Port_Write_Request_H;
            break;

            case RIO_LL_REMOTE_MP:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Message_Request_H;
            break;

            case RIO_LL_REMOTE_DOORBELL:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Doorbell_Request_H;
            break;

	    /* GDA: Bug#124 - Support for Data Streaming packet */
	    case RIO_LL_REMOTE_DS:
                   ll_Data->handler_Table[i].func = RIO_LL_Remote_DS_Request_H;
            break;
	    /* GDA: Bug#124 - Support for Data Streaming packet */
	    
	    /* GDA: Bug#125 - Support for Flow control packet */
            case RIO_LL_REMOTE_FC:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_FC_Request_H;
            break;
	    /* GDA: Bug#125 - Support for Flow control packet */
	    
            case RIO_LL_REMOTE_GENERAL_ERROR:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_General_Error_Request_H;
            break;

            case RIO_LL_REMOTE_CONF_READ_ERROR:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Conf_Read_Error_Request_H;
            break;

            case RIO_LL_REMOTE_CONF_WRITE_ERROR:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Conf_Write_Error_Request_H;
            break;

            case RIO_LL_REMOTE_MESSAGE_ERROR:
                ll_Data->handler_Table[i].func = RIO_LL_Remote_Message_Error_Request_H;
            break;
            
            case RIO_LL_END_TABLE:
                ll_Data->handler_Table[i].func = NULL;
            break;
        }       
    }

    /* reset LL instance */
    RIO_LL_Start_Reset( ll_Data );
    
    /* return value */
    *handle = (RIO_HANDLE_T)ll_Data;
    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_LL_Get_Function_Tray
 *
 * Description:  this function places pointers to the functions into 
 *               supplied ftray structure.
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Get_Function_Tray(
    RIO_LL_FTRAY_T *ftray
)
{
    /* check parameter */
    if ( ftray == NULL )
        return RIO_PARAM_INVALID;

    /* functions to access logical layer from local device */
    ftray->rio_GSM_Request      = RIO_LL_GSM_Request;
    ftray->rio_IO_Request       = RIO_LL_IO_Request;

    ftray->rio_Message_Request  = RIO_LL_Message_Request;
    ftray->rio_Doorbell_Request = RIO_LL_Doorbell_Request;
    ftray->rio_Conf_Request     = RIO_LL_Config_Request;
    ftray->rio_Snoop_Response   = RIO_LL_Snoop_Response;
    ftray->rio_Set_Mem_Data     = RIO_LL_Set_Memory_Data;
    ftray->rio_Get_Mem_Data     = RIO_LL_Get_Memory_Data; 
    
    ftray->rio_Start_Reset      = RIO_LL_Start_Reset; 
    ftray->rio_Init             = RIO_LL_Initialize;

    /* functions to access logical layer from transport layer */
    ftray->rio_Remote_Request   = RIO_LL_Remote_Request;
    ftray->rio_Remote_Responce  = RIO_LL_Remote_Response;

    ftray->rio_Remote_Conf_Read  = RIO_LL_Ext_Features_Read;
    ftray->rio_Remote_Conf_Write = RIO_LL_Ext_Features_Write;

    ftray->rio_Ack_Request   = RIO_LL_Ack_Request;
    ftray->rio_Get_Req_Data  = RIO_LL_Get_Request_Responce_Data;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    ftray->rio_Get_Req_Data_Hw  = RIO_LL_Get_Request_Data_Hw; 
    /* GDA: Bug#124 - Support for Data Streaming packet */

    ftray->rio_Get_Resp_Data = RIO_LL_Get_Request_Responce_Data;
    ftray->rio_Ack_Response  = RIO_LL_Ack_Responce;
    ftray->rio_Req_Time_Out  = RIO_LL_Request_Timeout;
    
    ftray->rio_LL_Delete_Instance = RIO_LL_Delete_Instance;
 
   /* GDA: Bug#124 - Support for Data Streaming packet */
    ftray->rio_Get_Req_Data_Hw  = RIO_LL_Get_Request_Data_Hw; 
    ftray->rio_DS_Request       = RIO_LL_DS_Request; 
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    ftray->rio_FC_Request       = RIO_LL_FC_Request;
    ftray->rio_Congestion       = RIO_LL_Congestion;
    ftray->rio_FC_To_Send  	= RIO_LL_FC_To_Send;
    /* GDA: Bug#125 */

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_LL_Bind_Instance
 *
 * Description:  this function places supplied callback tray into interbal 
 *               LL data structure.
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Bind_Instance(
    RIO_HANDLE_T           handle, 
    RIO_LL_CALLBACK_TRAY_T *ctray
)
{
    RIO_LL_DS_T *ll_Data;

    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        ll_Data = (RIO_LL_DS_T*)handle;

    if ( ctray == NULL )
        return RIO_PARAM_INVALID;

    /* these parameters are stored at first to provide ability for the */
    /* layer to report about errors during instance binding            */
    ll_Data->callback_Tray.rio_User_Msg          = ctray->rio_User_Msg;
    ll_Data->callback_Tray.rio_Msg               = ctray->rio_Msg;

    /* store callback tray in instance's data structure */

    /* callbacks providede by environment */

    /* check and copy local response callbacks */
    if ( ctray->rio_Local_Response == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "local response");
        return RIO_PARAM_INVALID;                
    }
    else
        ll_Data->callback_Tray.rio_Local_Response = ctray->rio_Local_Response;

    /* check and copy GSM request done callback */
    if ( ( ( ll_Data->inst_Params.source_Trx & RIO_LL_GSM_TRX_MASK ) != 0 ) &&
        ctray->rio_GSM_Request_Done == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "GSM request done notification");
        return RIO_PARAM_INVALID;                
    }
    else
        ll_Data->callback_Tray.rio_GSM_Request_Done = ctray->rio_GSM_Request_Done;

    /* check and copy IO request done callback */
    if ( ( ( ll_Data->inst_Params.source_Trx & RIO_LL_IO_TRX_MASK ) != 0 ) &&
        ctray->rio_IO_Request_Done == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "IO request done notification");
        return RIO_PARAM_INVALID;                
    }
    else
        ll_Data->callback_Tray.rio_IO_Request_Done = ctray->rio_IO_Request_Done;

    /* check and copy message request done callback */
    if ( ( ( ll_Data->inst_Params.source_Trx & RIO_LL_MP_TRX_MASK ) != 0 ) &&
        ctray->rio_MP_Request_Done == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "message request done notification");
        return RIO_PARAM_INVALID;                
    }
    else
        ll_Data->callback_Tray.rio_MP_Request_Done = ctray->rio_MP_Request_Done;

    /* check and copy doorbell request done callback */
    if ( ( ( ll_Data->inst_Params.source_Trx & RIO_LL_DOORBELL_TRX_MASK ) != 0 ) &&
        ctray->rio_Doorbell_Request_Done == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "doorbell request done notification");
        return RIO_PARAM_INVALID;                
    }
    else
        ll_Data->callback_Tray.rio_Doorbell_Request_Done = ctray->rio_Doorbell_Request_Done;


    /* GDA: Bug#124 - Support for Data Streaming packet */
    if ( ( ( ll_Data->inst_Params.source_Trx & RIO_LL_DS_TRX_MASK ) != 0 ) &&
        ctray->rio_DS_Request_Done == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "DS request done notification");
        return RIO_PARAM_INVALID;
    }
    else
       ll_Data->callback_Tray.rio_DS_Request_Done = ctray->rio_DS_Request_Done;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */ 
    if ( ( ( ll_Data->inst_Params.source_Trx & RIO_LL_FC_TRX_MASK ) != 0 ) &&
        ctray->rio_FC_Request_Done == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "FC request done notification");
        return RIO_PARAM_INVALID;                
    }
    else
        ll_Data->callback_Tray.rio_FC_Request_Done = ctray->rio_FC_Request_Done;
    /* GDA: Bug#125 - Support for Flow control packet */
    
    /* check and copy maintenance request done callback */
    if ( ctray->rio_Config_Request_Done == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "maintenance request done notification");
        return RIO_PARAM_INVALID;                
    }
    else
        ll_Data->callback_Tray.rio_Config_Request_Done   = ctray->rio_Config_Request_Done;

    ll_Data->callback_Tray.local_Device_Context = ctray->local_Device_Context;

    /* check and copy snoop request callback */
    if ( ll_Data->inst_Params.has_Processor && ctray->rio_Snoop_Request == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "snoop request");
        return RIO_PARAM_INVALID;                
    }
    else
    {
        ll_Data->callback_Tray.rio_Snoop_Request     = ctray->rio_Snoop_Request;
        ll_Data->callback_Tray.snoop_Context         = ctray->snoop_Context;
    }

    /* check and copy remote message request callback */
    if ( ( ll_Data->inst_Params.mbox1 || ll_Data->inst_Params.mbox1 ||
        ll_Data->inst_Params.mbox1 || ll_Data->inst_Params.mbox1 )  && 
        ctray->rio_MP_Remote_Request == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "remote message request notification");
        return RIO_PARAM_INVALID;                
    }
    else
    {
        ll_Data->callback_Tray.rio_MP_Remote_Request   = ctray->rio_MP_Remote_Request;
        ll_Data->callback_Tray.mailbox_Context         = ctray->mailbox_Context;
    }

    /* check and copy remote doorbell requst callback */
    if ( ll_Data->inst_Params.has_Doorbell && ctray->rio_Doorbell_Remote_Request == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "remote doorbell request notification");
        return RIO_PARAM_INVALID;                
    }
    else
    {
        ll_Data->callback_Tray.rio_Doorbell_Remote_Request = ctray->rio_Doorbell_Remote_Request;
        ll_Data->callback_Tray.doorbell_Context            = ctray->doorbell_Context;
    }

    /* check and copy memory request callback */
    if ( ll_Data->inst_Params.has_Memory && ctray->rio_Memory_Request == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "memory request");
        return RIO_PARAM_INVALID;                
    }
    else
    {
        ll_Data->callback_Tray.rio_Memory_Request          = ctray->rio_Memory_Request;
        ll_Data->callback_Tray.memory_Context              = ctray->memory_Context;
    }

    /* check and copy remote port write request callback */
    if ( ( ( ll_Data->inst_Params.dest_Trx & RIO_LL_PORT_WRITE_TRX_MASK ) != 0) &&
        ctray->rio_Port_Write_Remote_Request == NULL )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_NO_CALLBACK, "remote port write notification");
        return RIO_PARAM_INVALID;                
    }
    else
    {
        ll_Data->callback_Tray.rio_Port_Write_Remote_Request = ctray->rio_Port_Write_Remote_Request;
        ll_Data->callback_Tray.port_Context                  = ctray->port_Context;
    }

    /* it is no need to check memory directory access callbacks */
    /* because they are checked inside logical layer kernel     */
    ll_Data->callback_Tray.rio_Read_Dir                = ctray->rio_Read_Dir;
    ll_Data->callback_Tray.rio_Write_Dir               = ctray->rio_Write_Dir;
    ll_Data->callback_Tray.directory_Context           = ctray->directory_Context;
                                                      
    /* it is no need to check extended features access callbacks */
    /* because they are checked inside logical layer kernel      */
    ll_Data->callback_Tray.rio_Remote_Conf_Read         = ctray->rio_Remote_Conf_Read;
    ll_Data->callback_Tray.rio_Remote_Conf_Write        = ctray->rio_Remote_Conf_Write;
    ll_Data->callback_Tray.extended_Features_Context    = ctray->extended_Features_Context;

    /* it is no need to check address translation callbacks      */
    /* because NULL values of these callbacks are proper handled */
    /* inside logical layer                                      */
    ll_Data->callback_Tray.rio_Tr_Local_IO              = ctray->rio_Tr_Local_IO;
    ll_Data->callback_Tray.rio_Tr_Remote_IO             = ctray->rio_Tr_Remote_IO;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ll_Data->callback_Tray.rio_Tr_Local_DS              = ctray->rio_Tr_Local_DS;
    ll_Data->callback_Tray.rio_Tr_Remote_DS             = ctray->rio_Tr_Remote_DS; 
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */    
    ll_Data->callback_Tray.rio_Tr_Local_FC              = ctray->rio_Tr_Local_FC;
    ll_Data->callback_Tray.rio_Tr_Remote_FC             = ctray->rio_Tr_Remote_FC;
    ll_Data->callback_Tray.rio_TL_FC_Request            = ctray->rio_TL_FC_Request;
    ll_Data->callback_Tray.rio_TL_Congestion            = ctray->rio_TL_Congestion;
    /* GDA: Bug#125 */
    
    ll_Data->callback_Tray.rio_Route_GSM                = ctray->rio_Route_GSM;
    ll_Data->callback_Tray.address_Translation_Context  = ctray->address_Translation_Context;

    /* callbacks provided by the transport layer */
    ll_Data->callback_Tray.rio_Set_Config_Reg           = ctray->rio_Set_Config_Reg; 
    ll_Data->callback_Tray.rio_Get_Config_Reg           = ctray->rio_Get_Config_Reg; 
    ll_Data->callback_Tray.rio_Config_Context           = ctray->rio_Config_Context; 

    ll_Data->callback_Tray.rio_TL_GSM_Request           = ctray->rio_TL_GSM_Request;
    ll_Data->callback_Tray.rio_TL_IO_Request            = ctray->rio_TL_IO_Request;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    ll_Data->callback_Tray.rio_TL_DS_Request            = ctray->rio_TL_DS_Request;  
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    ll_Data->callback_Tray.rio_TL_Message_Request       = ctray->rio_TL_Message_Request;
    ll_Data->callback_Tray.rio_TL_Doorbell_Request      = ctray->rio_TL_Doorbell_Request;
    ll_Data->callback_Tray.rio_TL_Configuration_Request = ctray->rio_TL_Configuration_Request;
    ll_Data->callback_Tray.rio_TL_Send_Response         = ctray->rio_TL_Send_Response;
    ll_Data->callback_Tray.rio_TL_Outbound_Context      = ctray->rio_TL_Outbound_Context;

    ll_Data->callback_Tray.rio_TL_Ack_Remote_Req        = ctray->rio_TL_Ack_Remote_Req;
    ll_Data->callback_Tray.rio_TL_Inbound_Context       = ctray->rio_TL_Inbound_Context;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_LL_Initialize
 *
 * Description:  function initializes LL instance accordingly to supplied 
 *               parameters set
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Initialize(
    RIO_HANDLE_T              handle,
    RIO_LL_PARAM_SET_T        *param_Set
)
{
    RIO_LL_DS_T    *ll_Data;
    RIO_LL_QUEUE_T *element;
    int            i;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        ll_Data = (RIO_LL_DS_T*)handle;

    if ( param_Set == NULL )
        return RIO_PARAM_INVALID;

    /* check that LL was reseted before initialization */
    if ( !ll_Data->in_Reset )
    {
        RIO_LL_Message( ll_Data, RIO_LL_ERROR_INIT_OUTSIDE_RESET );
        return RIO_ERROR;
    }
    
    /* copy parameters into instance data structure */
    ll_Data->init_Params.config_Space_Size = param_Set->config_Space_Size;

/*    ll_Data->init_Params.dev_ID        = param_Set->dev_ID; */

    /*ll_Data->init_Params.ext_Address    = param_Set->ext_Address;
    ll_Data->init_Params.ext_Address_16 = param_Set->ext_Address_16;
    ll_Data->init_Params.xamsbs         = param_Set->xamsbs;*/

    ll_Data->init_Params.ext_Mailbox_Support = param_Set->ext_Mailbox_Support;

    ll_Data->init_Params.res_For_3_Out = param_Set->res_For_3_Out;
    ll_Data->init_Params.res_For_2_Out = param_Set->res_For_2_Out;
    ll_Data->init_Params.res_For_1_Out = param_Set->res_For_1_Out;
    ll_Data->init_Params.res_For_3_In  = param_Set->res_For_3_In;
    ll_Data->init_Params.res_For_2_In  = param_Set->res_For_2_In;
    ll_Data->init_Params.res_For_1_In  = param_Set->res_For_1_In;
    ll_Data->init_Params.pass_By_Prio  = param_Set->pass_By_Prio;
    ll_Data->init_Params.transp_Type    = param_Set->transp_Type;
   

    /* check that remote device id's array pointer isn't NULL */
    if ( ll_Data->inst_Params.coh_Domain_Size >= 2 && param_Set->remote_Dev_ID == NULL )
        return RIO_PARAM_INVALID;

    /* copy remote devices ID array */
    if ( ll_Data->inst_Params.coh_Domain_Size >= 2 ) /* the minimal allowed coherency domain size is 2 */
        for ( i = 0; i < ll_Data->inst_Params.coh_Domain_Size - 1; i++ )
            ll_Data->init_Params.remote_Dev_ID[i] = param_Set->remote_Dev_ID[i];

    /* clear inbound buffer */
    for (i = 0; i < (int)(ll_Data->remoteb_Size); i++)
    {
        if ( ll_Data->remote_Buffer[i].trx_Info.req != NULL )
        {
            /* clean data field */
            switch ( ll_Data->remote_Buffer[i].trx_Info.req_Type )          
            {
                case RIO_LL_IO:
                    if ( (( RIO_IO_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_IO_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data );
                break;

                case RIO_LL_GSM:
                    if ( (( RIO_GSM_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_GSM_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data );
                break;

                case RIO_LL_CONF:
                    if ( (( RIO_CONFIG_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_CONFIG_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data );
                break;

                case RIO_LL_MESSAGE:
                    if ( (( RIO_MESSAGE_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_MESSAGE_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data );
                break;

		/* GDA: Bug#124 - Support for Data Streaming packet */
                case RIO_LL_DS:
                    if ( (( RIO_DS_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data_hw != NULL )
                        free( (( RIO_DS_REQ_T* )(ll_Data->remote_Buffer[i].trx_Info.req))->data_hw );
                break;
		/* GDA: Bug#124 - Support for Data Streaming packet */

		/* GDA: Bug#125 - Support for Flow control packet */
                case RIO_LL_FC:
                break;
		/* GDA: Bug#125 - Support for Flow control packet */

                case RIO_LL_DOORBELL:
                break;
            }
            free( ll_Data->remote_Buffer[i].trx_Info.req );
            ll_Data->remote_Buffer[i].trx_Info.req = NULL;
        }

        if ( ll_Data->remote_Buffer[i].trx_Info.snoop_Data != NULL)
        {
            free( ll_Data->remote_Buffer[i].trx_Info.snoop_Data );
            ll_Data->remote_Buffer[i].trx_Info.snoop_Data = NULL;
        }

        if ( ll_Data->remote_Buffer[i].trx_Info.snoop_Mask != NULL)
        {
            free( ll_Data->remote_Buffer[i].trx_Info.snoop_Mask );
            ll_Data->remote_Buffer[i].trx_Info.snoop_Mask = NULL;
        }

        ll_Data->remote_Buffer[i].kernel_Info.valid = RIO_FALSE;
    }

    /* clear outbound buffer */
    for (i = 0; i < (int)(ll_Data->locb_Size); i++)
    {
        if ( ll_Data->loc_Buffer[i].trx_Info.req != NULL )
        {
            /* clean data field */
            switch ( ll_Data->loc_Buffer[i].trx_Info.req_Type )         
            {
                case RIO_LL_IO:
                    if ( (( RIO_IO_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_IO_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data );
                break;

		/* GDA: Bug#124 - Support for Data Streaming packet */
                case RIO_LL_DS:
                    if ( (( RIO_DS_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data_hw != NULL )
                        free( (( RIO_DS_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data_hw );
                break;
		/* GDA: Bug#124 */

		/* GDA: Bug#125 - Support for Flow control packet */
                case RIO_LL_FC:
                break;
		/* GDA: Bug#125 */
	
                case RIO_LL_GSM:
                    if ( (( RIO_GSM_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_GSM_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data );
                break;

                case RIO_LL_CONF:
                    if ( (( RIO_CONFIG_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_CONFIG_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data );
                break;

                case RIO_LL_MESSAGE:
                    if ( (( RIO_MESSAGE_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data != NULL )
                        free( (( RIO_MESSAGE_REQ_T* )(ll_Data->loc_Buffer[i].trx_Info.req))->data );
                break;

                case RIO_LL_DOORBELL:
                break;
            }
            free( ll_Data->loc_Buffer[i].trx_Info.req );
            ll_Data->loc_Buffer[i].trx_Info.req = NULL;
        }

        if ( ll_Data->loc_Buffer[i].trx_Info.snoop_Data != NULL)
        {
            free( ll_Data->loc_Buffer[i].trx_Info.snoop_Data );
            ll_Data->loc_Buffer[i].trx_Info.snoop_Data = NULL;
        }

        if ( ll_Data->loc_Buffer[i].trx_Info.snoop_Mask != NULL)
        {
            free( ll_Data->loc_Buffer[i].trx_Info.snoop_Mask );
            ll_Data->loc_Buffer[i].trx_Info.snoop_Mask = NULL;
        }

        ll_Data->loc_Buffer[i].kernel_Info.valid = RIO_FALSE;
    }

    /* mark inbound buffer piorities as supposed by param set */
    for (i = ll_Data->remoteb_Size - 1; i >= (int)(ll_Data->remoteb_Size - param_Set->res_For_3_In); i--)
        ll_Data->remote_Buffer[i].kernel_Info.for_Prio = 3; /* priority level 3 */
    for (; i >= (int)(ll_Data->remoteb_Size - param_Set->res_For_3_In - param_Set->res_For_2_In); i--)
        ll_Data->remote_Buffer[i].kernel_Info.for_Prio = 2; /* priority level 2 */
    for (; i >= (int)(ll_Data->remoteb_Size - param_Set->res_For_3_In - param_Set->res_For_2_In - param_Set->res_For_1_In); i--)
        ll_Data->remote_Buffer[i].kernel_Info.for_Prio = 1; /* priority level 1 */
    for (; i >= 0; i--)
        ll_Data->remote_Buffer[i].kernel_Info.for_Prio = 0; /* priority level 0 */

    /* mark outbound buffer piorities as supposed by param set */
    for (i = ll_Data->locb_Size - 1; i >= (int)(ll_Data->locb_Size - param_Set->res_For_3_Out); i--)
        ll_Data->loc_Buffer[i].kernel_Info.for_Prio = 3; /* priority level 3 */
    for (; i >= (int)(ll_Data->locb_Size - param_Set->res_For_3_Out - param_Set->res_For_2_Out); i--)
        ll_Data->loc_Buffer[i].kernel_Info.for_Prio = 2; /* priority level 2 */
    for (; i >= (int)(ll_Data->locb_Size - param_Set->res_For_3_Out - param_Set->res_For_2_Out - param_Set->res_For_1_Out); i--)
        ll_Data->loc_Buffer[i].kernel_Info.for_Prio = 1; /* priority level 1 */
    for (; i >= 0; i--)
        ll_Data->loc_Buffer[i].kernel_Info.for_Prio = 0; /* priority level 0 */

    /* return memory queue elements to pool */
    while (ll_Data->target_Queues.memory_Queue_Head != NULL)
    {
        element = ll_Data->target_Queues.memory_Queue_Head;
        ll_Data->target_Queues.memory_Queue_Head = ll_Data->target_Queues.memory_Queue_Head->next;
        element->next = ll_Data->target_Queues.memory_Queue_Pool;
        ll_Data->target_Queues.memory_Queue_Pool = element;
    }

    /* return snoop queue elements to pool */
    while (ll_Data->target_Queues.snoop_Queue_Head != NULL)
    {
        element = ll_Data->target_Queues.snoop_Queue_Head;
        ll_Data->target_Queues.snoop_Queue_Head = ll_Data->target_Queues.snoop_Queue_Head->next;
        element->next = ll_Data->target_Queues.snoop_Queue_Pool;
        ll_Data->target_Queues.snoop_Queue_Pool = element;
    }

    /* return request queue elements to pool */
    while (ll_Data->target_Queues.rio_Req_Queue_Head != NULL)
    {
        element = ll_Data->target_Queues.rio_Req_Queue_Head;
        ll_Data->target_Queues.rio_Req_Queue_Head = ll_Data->target_Queues.rio_Req_Queue_Head->next;
        element->next = ll_Data->target_Queues.rio_Req_Queue_Pool;
        ll_Data->target_Queues.rio_Req_Queue_Pool = element;
    }

    /* return response queue elements to pool */
    while (ll_Data->target_Queues.rio_Resp_Queue_Head != NULL)
    {
        element = ll_Data->target_Queues.rio_Resp_Queue_Head;
        ll_Data->target_Queues.rio_Resp_Queue_Head = ll_Data->target_Queues.rio_Resp_Queue_Head->next;
        element->next = ll_Data->target_Queues.rio_Resp_Queue_Pool;
        ll_Data->target_Queues.rio_Resp_Queue_Pool = element;
    }

    /* initialize temporary buffer */
    ll_Data->temp_Buffer.has_Transaction = RIO_FALSE;

    /* reset semaphor */
    ll_Data->semaphor = RIO_LL_SEMAPHOR_INIT;

    /* reset reset flag */
    ll_Data->in_Reset = RIO_FALSE;

    return RIO_OK;

}

/***************************************************************************
 * Function :    RIO_LL_Start_Reset
 *
 * Description:  this function simply sets reset flag, so nothing can occurs
 *               afer this before initialization
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_LL_Start_Reset(
    RIO_HANDLE_T handle
)
{
    RIO_LL_DS_T *ll_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        ll_Data = (RIO_LL_DS_T*)handle;

    ll_Data->in_Reset = RIO_TRUE;

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_LL_Delete_Instance
 *
 * Description:  
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_LL_Delete_Instance(
    RIO_HANDLE_T    handle
)
{
    RIO_LL_QUEUE_T *q;
    RIO_LL_DS_T *ll_Data = (RIO_LL_DS_T *)handle;
    
    if( ll_Data == NULL )
        return RIO_ERROR;
    
    
    if( ll_Data->handler_Table != NULL )
        free( ll_Data->handler_Table );


    /* local requests buffer */
    if( ll_Data->loc_Buffer != NULL )
        /* rq and snoop_data are allocated and freed during model work */
        free( ll_Data->loc_Buffer );
        

    /* remote requests buffer */
    if( ll_Data->remote_Buffer != NULL )
        /* rq and snoop_data are allocated and freed during model work */
        free( ll_Data->remote_Buffer );


    /* target queues */
    while( ll_Data->target_Queues.snoop_Queue_Pool != NULL )
    {
        q = ll_Data->target_Queues.snoop_Queue_Pool->next;
        free( ll_Data->target_Queues.snoop_Queue_Pool );
        ll_Data->target_Queues.snoop_Queue_Pool = q;
    }
    while( ll_Data->target_Queues.memory_Queue_Pool != NULL )
    {
        q = ll_Data->target_Queues.memory_Queue_Pool->next;
        free( ll_Data->target_Queues.memory_Queue_Pool );
        ll_Data->target_Queues.memory_Queue_Pool = q;
    }
    while( ll_Data->target_Queues.rio_Req_Queue_Pool != NULL )
    {
        q = ll_Data->target_Queues.rio_Req_Queue_Pool->next;
        free( ll_Data->target_Queues.rio_Req_Queue_Pool );
        ll_Data->target_Queues.rio_Req_Queue_Pool = q;
    }
    while( ll_Data->target_Queues.rio_Resp_Queue_Pool != NULL )
    {
        q = ll_Data->target_Queues.rio_Resp_Queue_Pool->next;
        free( ll_Data->target_Queues.rio_Resp_Queue_Pool );
        ll_Data->target_Queues.rio_Resp_Queue_Pool = q;
    }
    while( ll_Data->target_Queues.snoop_Queue_Head != NULL )
    {
        q = ll_Data->target_Queues.snoop_Queue_Head->next;
        free( ll_Data->target_Queues.snoop_Queue_Head );
        ll_Data->target_Queues.snoop_Queue_Head = q;
    }
    while( ll_Data->target_Queues.memory_Queue_Head != NULL )
    {
        q = ll_Data->target_Queues.memory_Queue_Head->next;
        free( ll_Data->target_Queues.memory_Queue_Head );
        ll_Data->target_Queues.memory_Queue_Head = q;
    }
    while( ll_Data->target_Queues.rio_Req_Queue_Head != NULL )
    {
        q = ll_Data->target_Queues.rio_Req_Queue_Head->next;
        free( ll_Data->target_Queues.rio_Req_Queue_Head );
        ll_Data->target_Queues.rio_Req_Queue_Head = q;
    }
    while( ll_Data->target_Queues.rio_Resp_Queue_Head != NULL )
    {
        q = ll_Data->target_Queues.rio_Resp_Queue_Head->next;
        free( ll_Data->target_Queues.rio_Resp_Queue_Head );
        ll_Data->target_Queues.rio_Resp_Queue_Head = q;
    }

    free( ll_Data );

    return RIO_OK;   
}

