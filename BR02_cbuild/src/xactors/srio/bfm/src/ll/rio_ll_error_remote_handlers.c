/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_error_remote_handlers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains handlers which implement state machines for
*               servicing of remotely requested transactions containing errors
*
* Notes:        
*
******************************************************************************/

#include "rio_ll_trx_handlers.h"
#include "rio_ll_kernel.h"

#include <assert.h>


/***************************************************************************
 * Function :    RIO_LL_Remote_Conf_Read_Error_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested erroneous maintenance read transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Conf_Read_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is geberal error */
                case RIO_EVENT_ERROR_GENERAL:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_MAINTENANCE, 
                        RIO_MAINTENANCE_READ_RESPONCE, 
                        RIO_RESPONCE_STATUS_ERROR
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* if transaction cannot be serviced */
                case RIO_EVENT_ERROR_CANNOT_SERVICE:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_MAINTENANCE, 
                        RIO_MAINTENANCE_READ_RESPONCE, 
                        RIO_RESPONCE_STATUS_CANNOT_SERVICE
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}


/***************************************************************************
 * Function :    RIO_LL_Remote_Conf_Write_Error_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested erroneous maintenance write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Conf_Write_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is geberal error */
                case RIO_EVENT_ERROR_GENERAL:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_MAINTENANCE, 
                        RIO_MAINTENANCE_WRITE_RESPONCE, 
                        RIO_RESPONCE_STATUS_ERROR
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* if transaction cannot be serviced */
                case RIO_EVENT_ERROR_CANNOT_SERVICE:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_MAINTENANCE, 
                        RIO_MAINTENANCE_WRITE_RESPONCE, 
                        RIO_RESPONCE_STATUS_CANNOT_SERVICE
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_General_Error_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested erroneous transaction (IO, Doorbell, GSM and 
 *               transaction of the unknown format type or transaction type)
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_General_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is geberal error */
                case RIO_EVENT_ERROR_GENERAL:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, 
                        RIO_RESPONCE_STATUS_ERROR
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* if transaction cannot be serviced */
                case RIO_EVENT_ERROR_CANNOT_SERVICE:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, 
                        RIO_RESPONCE_STATUS_CANNOT_SERVICE
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_Message_Error_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested erroneous message transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Message_Error_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is geberal error */
                case RIO_EVENT_ERROR_GENERAL:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_MESSAGE, 
                        RIO_RESPONCE_STATUS_ERROR
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* if transaction cannot be serviced */
                case RIO_EVENT_ERROR_CANNOT_SERVICE:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_MESSAGE, 
                        RIO_RESPONCE_STATUS_CANNOT_SERVICE
                    );
    
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}



