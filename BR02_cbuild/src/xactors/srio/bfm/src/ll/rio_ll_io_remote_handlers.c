/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_io_remote_handlers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains handlers which implement state machines for
*               servicing of remotely requested io transactions
*
* Notes:        
*
******************************************************************************/


#include "rio_ll_trx_handlers.h"
#include "rio_ll_kernel.h"

#include <assert.h>

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_SWrite_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Stream Write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               Transaction handlers for the Stream Write and NWrite 
 *               transactions are now exactly equal. Byt there are made two
 *               transaction handlers for the protocol change case to 
 *               easely modify transaction handler for the individual 
 *               transaction.
 *
 **************************************************************************/
int RIO_LL_Remote_IO_SWrite_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if the transaction shall go to the local config space */
                    if ( RIO_LL_Kernel_Is_To_Config_Space( handle, buf_Tag ) )
                    {
                        /* do configuration registers write */
                        RIO_LL_Kernel_Set_Config_Reg( 
                            handle, 
                            buf_Tag
                        );

                        return RIO_LL_COMPLETE;
                    }

                    /* if there is no memory in PE complete transaction */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                        return RIO_LL_COMPLETE;

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_SWRITE
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_WRITE
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_WRITE
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_NWrite_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO NWrite transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               Transaction handlers for the Stream Write and NWrite 
 *               transactions are now exactly equal. Byt there are made two
 *               transaction handlers for the protocol change case to 
 *               easely modify transaction handler for the individual 
 *               transaction.
 *
 **************************************************************************/
int RIO_LL_Remote_IO_NWrite_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if the transaction shall go to the local config space */
                    if ( RIO_LL_Kernel_Is_To_Config_Space( handle, buf_Tag ) )
                    {
                        /* do configuration registers write */
                        RIO_LL_Kernel_Set_Config_Reg( 
                            handle, 
                            buf_Tag
                        );

                        return RIO_LL_COMPLETE;
                    }

                    /* if there is no memory in PE complete transaction */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                        return RIO_LL_COMPLETE;

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_NWRITE
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_WRITE
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_WRITE
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_NWrite_R_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO NWrite with response transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_NWrite_R_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
    int result;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if the transaction shall go to the local config space */
                    if ( RIO_LL_Kernel_Is_To_Config_Space( handle, buf_Tag ) )
                    {
                        /* do configuration registers write */
                        result = RIO_LL_Kernel_Set_Config_Reg( 
                            handle, 
                            buf_Tag
                        );

                        switch ( result ) {
                            
                            case RIO_OK:
                                /* send response */
                                RIO_LL_Kernel_Send_Responce(
                                    handle, 
                                    buf_Tag, 
                                    RIO_RESPONCE, 
                                    RIO_RESPONCE_WO_DATA, 
                                    RIO_RESPONCE_STATUS_DONE
                                );
                            break;

                            case RIO_ERROR:
                                /* send response */
                                RIO_LL_Kernel_Send_Responce(
                                    handle, 
                                    buf_Tag, 
                                    RIO_RESPONCE, 
                                    RIO_RESPONCE_WO_DATA, 
                                    RIO_RESPONCE_STATUS_ERROR
                                );
                            break;

                        }

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_NWRITE_R
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_WRITE
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_WRITE
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_NRead_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO NRead transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_NRead_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
    int result;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if the transaction shall go to the local config space */
                    if ( RIO_LL_Kernel_Is_To_Config_Space( handle, buf_Tag ) )
                    {
                        /* do configuration registers read */
                        result = RIO_LL_Kernel_Get_Config_Reg( 
                            handle, 
                            buf_Tag
                        );

                        switch ( result ) {
                            
                            case RIO_OK:
                                /* send response */
                                RIO_LL_Kernel_Send_Responce(
                                    handle, 
                                    buf_Tag, 
                                    RIO_RESPONCE, 
                                    RIO_RESPONCE_WITH_DATA, 
                                    RIO_RESPONCE_STATUS_DONE
                                );
                            break;

                            case RIO_ERROR:
                                /* send response */
                                RIO_LL_Kernel_Send_Responce(
                                    handle, 
                                    buf_Tag, 
                                    RIO_RESPONCE, 
                                    RIO_RESPONCE_WO_DATA, 
                                    RIO_RESPONCE_STATUS_ERROR
                                );
                            break;

                        }

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_NREAD
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_READ
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_READ
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WITH_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_Atomic_Set_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Atomic Set transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_Atomic_Set_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_ATOMIC    
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_ATOMIC_SET
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_ATOMIC_SET
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WITH_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_Atomic_Clear_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Atomic Clear transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_Atomic_Clear_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_ATOMIC    
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_ATOMIC_CLEAR
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_ATOMIC_CLEAR
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WITH_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_Atomic_Inc_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Atomic Inc transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_Atomic_Inc_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_ATOMIC    
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_ATOMIC_INC
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_ATOMIC_INC
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WITH_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_Atomic_Dec_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Atomic Dec transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_Atomic_Dec_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_ATOMIC    
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_ATOMIC_DEC
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_ATOMIC_DEC
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WITH_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_IO_Atomic_TSwap_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Atomic TSwap transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_Atomic_TSwap_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_ATOMIC    
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_ATOMIC_TSWAP
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_ATOMIC_TSWAP
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WITH_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}




/* GDA: Bug#133 */
/***************************************************************************
 * Function :    RIO_LL_Remote_IO_Atomic_Swap_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Atomic Swap transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_IO_Atomic_Swap_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if there is no memory in PE send ERROR response */
                    if ( !RIO_LL_Kernel_Have_Memory( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_NO_MEMORY
                        );

                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );

                        return RIO_LL_PROGRESS;
                    }

                    if ( RIO_LL_Kernel_Have_Processor( handle ))
                    {
                        /* do snoop request */
                        RIO_LL_Kernel_Snoop_Request(
                            handle,
                            buf_Tag,
                            RIO_LOCAL_NONCOHERENT,
                            RIO_SNOOP_ATOMIC    
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SNOOP );
                    }
                    else
                    {
                        /* do memory request */
                        RIO_LL_Kernel_Memory_Request(
                            handle, 
                            buf_Tag,    
                            RIO_MEM_ATOMIC_SWAP
                        );
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );
                    }

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */
        case RIO_LL_STATE_SNOOP:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* do memory request */                    
                    RIO_LL_Kernel_Memory_Request(
                        handle, 
                        buf_Tag,    
                        RIO_MEM_ATOMIC_SWAP
                    );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS );

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing memory access */
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            switch ( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Send_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_RESPONCE, 
                        RIO_RESPONCE_WITH_DATA, 
                        RIO_RESPONCE_STATUS_DONE
                    );

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}
