/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_conf_mp_remote_handlers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains handlers which implement state machines for
*               servicing of remotely requested maintenance and message passing
*               transactions
*
* Notes:        
*
******************************************************************************/

#include "rio_ll_trx_handlers.h"
#include "rio_ll_kernel.h"

#include <assert.h>

/***************************************************************************
 * Function :    RIO_LL_Remote_Conf_Read_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested maintenance read transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Conf_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
    int result;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* read configuration registers */
                    result = RIO_LL_Kernel_Get_Config_Reg( 
                        handle, 
                        buf_Tag
                    );

                    switch ( result )
                    {
                        case RIO_OK:
                            /* send OK response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_MAINTENANCE, 
                                RIO_MAINTENANCE_READ_RESPONCE, 
                                RIO_RESPONCE_STATUS_DONE
                            );
                        break;

                        case RIO_ERROR:
                            /* send ERROR response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_MAINTENANCE, 
                                RIO_MAINTENANCE_READ_RESPONCE, 
                                RIO_RESPONCE_STATUS_ERROR
                            );
                        break;
                    }

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_Conf_Write_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested maintenance write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Conf_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
    int result;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* write configuration registers */
                    result = RIO_LL_Kernel_Set_Config_Reg( 
                        handle, 
                        buf_Tag
                    );

                    switch ( result )
                    {
                        case RIO_OK:
                            /* send OK response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_MAINTENANCE, 
                                RIO_MAINTENANCE_WRITE_RESPONCE, 
                                RIO_RESPONCE_STATUS_DONE
                            );
                        break;

                        case RIO_ERROR:
                            /* send ERROR response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_MAINTENANCE, 
                                RIO_MAINTENANCE_WRITE_RESPONCE, 
                                RIO_RESPONCE_STATUS_ERROR
                            );
                        break;
                    }

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_Conf_Port_Write_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested maintenance port write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Conf_Port_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* write data to the port */
                    RIO_LL_Kernel_Port_Write( 
                        handle, 
                        buf_Tag
                    );

                    return RIO_LL_COMPLETE;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_Message_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested message passing transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Message_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
    int result;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* check if we have message hardware in PE */
                    if ( !RIO_LL_Kernel_Have_Mailbox( handle, buf_Tag ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_MESSAGE, 
                            RIO_RESPONCE_STATUS_ERROR
                        );
    
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                        return RIO_LL_PROGRESS;
                    }
                    
                    /* notify local device about remote message packet */
                    result = RIO_LL_Kernel_MP_Remote_Request(
                        handle, 
                        buf_Tag
                    );

                    switch ( result )
                    {
                        case RIO_OK:
                            /* send OK response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_RESPONCE, 
                                RIO_RESPONCE_MESSAGE, 
                                RIO_RESPONCE_STATUS_DONE
                            );
                        break;

                        case RIO_RETRY:
                            /* send RETRY response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_RESPONCE, 
                                RIO_RESPONCE_MESSAGE, 
                                RIO_RESPONCE_STATUS_RETRY
                            );
                        break;

                        case RIO_ERROR:
                            /* send ERROR response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_RESPONCE, 
                                RIO_RESPONCE_MESSAGE, 
                                RIO_RESPONCE_STATUS_ERROR
                            );
                        break;

                        /* any other result is impossible for this state */
                        default:
                            /* send ERROR response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_RESPONCE, 
                                RIO_RESPONCE_MESSAGE, 
                                RIO_RESPONCE_STATUS_ERROR
                            );

                    }

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Remote_Doorbell_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested doorbell transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_Doorbell_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
    int result;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* check if we have doorbell hardware in PE */
                    if ( !RIO_LL_Kernel_Have_Doorbell( handle ) )
                    {
                        /* send ERROR response */
                        RIO_LL_Kernel_Send_Responce(
                            handle, 
                            buf_Tag, 
                            RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, 
                            RIO_RESPONCE_STATUS_ERROR
                        );
    
                        RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                        return RIO_LL_PROGRESS;
                    }

                    /* notify local device about remote message packet */
                    result = RIO_LL_Kernel_Doorbell_Remote_Request(
                        handle, 
                        buf_Tag
                    );

                    switch ( result )
                    {
                        case RIO_OK:
                            /* send OK response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_RESPONCE, 
                                RIO_RESPONCE_WO_DATA, 
                                RIO_RESPONCE_STATUS_DONE
                            );
                        break;

                        case RIO_ERROR:
                            /* send ERROR response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_RESPONCE, 
                                RIO_RESPONCE_WO_DATA, 
                                RIO_RESPONCE_STATUS_ERROR
                            );
                        break;

                        case RIO_RETRY:
                            /* send RETRY response */
                            RIO_LL_Kernel_Send_Responce(
                                handle, 
                                buf_Tag, 
                                RIO_RESPONCE, 
                                RIO_RESPONCE_WO_DATA, 
                                RIO_RESPONCE_STATUS_RETRY
                            );
                        break;

                    }

                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending response */
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch ( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}


