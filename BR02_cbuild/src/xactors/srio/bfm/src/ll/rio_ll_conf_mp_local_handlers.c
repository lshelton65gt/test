/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_conf_mp_local_handlers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains handlers which implement state machines for
*               servicing of locally requested maintenence and message
*               passing transactions (handlers implement both local request 
*               and remote response state machines)
*
* Notes:        
*
******************************************************************************/

#include "rio_ll_trx_handlers.h"
#include "rio_ll_kernel.h"

#include <assert.h>

/***************************************************************************
 * Function :    RIO_LL_Local_Conf_Read_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested maintenance read transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_Conf_Read_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Local_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_LR_OK
                    );

                    /* send request */
                    RIO_LL_Kernel_Send_Request( handle, buf_Tag );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending request */
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                    return RIO_LL_PROGRESS;

                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Config_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_TRUE, 
                        RIO_REQ_DONE
                    );
                    return RIO_LL_COMPLETE;
                }

                case RIO_EVENT_RESPONSE_ERROR:
                case RIO_EVENT_RESPONSE_RETRY:
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_REQUEST_TIMEOUT:
                case RIO_EVENT_RESPONSE_CANNOT_SERVICE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Config_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_FALSE, 
                        RIO_REQ_ERROR
                    );
                    return RIO_LL_COMPLETE;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of ttransaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Local_Conf_Write_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested maintenance write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_Conf_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Local_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_LR_OK
                    );

                    /* send request */
                    RIO_LL_Kernel_Send_Request( handle, buf_Tag );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending request */
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                    return RIO_LL_PROGRESS;

                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Config_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_FALSE, 
                        RIO_REQ_DONE
                    );
                    return RIO_LL_COMPLETE;
                }

                case RIO_EVENT_RESPONSE_ERROR:
                case RIO_EVENT_RESPONSE_RETRY:
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_REQUEST_TIMEOUT:
                case RIO_EVENT_RESPONSE_CANNOT_SERVICE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Config_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_FALSE, 
                        RIO_REQ_ERROR
                    );
                    return RIO_LL_COMPLETE;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Local_Conf_Port_Write_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested maintenance port write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_Conf_Port_Write_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Local_Responce(
                        handle, 
                        buf_Tag, 
                        RIO_LR_OK
                    );

                    /* send request */
                    RIO_LL_Kernel_Send_Request( handle, buf_Tag );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending request */
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Config_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_FALSE, 
                        RIO_REQ_DONE
                    );
                    return RIO_LL_COMPLETE;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Local_Message_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested message passing transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_Message_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* send request */
                    RIO_LL_Kernel_Send_Request( handle, buf_Tag );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending request */
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                    return RIO_LL_PROGRESS;

                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_MP_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_REQ_DONE 
                    );
                    return RIO_LL_COMPLETE;
                }

                case RIO_EVENT_RESPONSE_ERROR:
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_REQUEST_TIMEOUT:
                case RIO_EVENT_RESPONSE_CANNOT_SERVICE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_MP_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_REQ_ERROR
                    );
                    return RIO_LL_COMPLETE;
                }

                /* any other event is impossible for this state */
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* resend request */
                    RIO_LL_Kernel_Send_Request( handle, buf_Tag );
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

/***************************************************************************
 * Function :    RIO_LL_Local_Doorbell_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested doorbell transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_Doorbell_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag, 
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is no collisions */
                case RIO_EVENT_START_TO_REMOTE:
                {
                    /* send request */
                    RIO_LL_Kernel_Send_Request( handle, buf_Tag );
                    RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is sending request */
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                    return RIO_LL_PROGRESS;

                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Doorbell_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_REQ_DONE
                    );
                    return RIO_LL_COMPLETE;
                }

                case RIO_EVENT_RESPONSE_ERROR:
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_REQUEST_TIMEOUT:
                case RIO_EVENT_RESPONSE_CANNOT_SERVICE:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Doorbell_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_REQ_ERROR
                    );
                    return RIO_LL_COMPLETE;
                }

                /* resend request in case of RETRY response */
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    /* resend request */
                    RIO_LL_Kernel_Send_Request( handle, buf_Tag );
                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                {
                    /* respond to the local device */
                    RIO_LL_Kernel_Doorbell_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_REQ_ERROR
                    );
                    return RIO_LL_COMPLETE;
                }
            }
            break;
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}

