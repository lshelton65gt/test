#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/ll/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds relocatable object file for logical layer model
#

include ../../verilog_env/demo/debug.inc


CC              =   gcc
TARGET          =   ll.o
PROFTARGET	=   ll_prof.o

DEBUG		=   -DNDEBUG

DEFINES         =   $(_DEBUG)

LIBS            =

INCLUDES        =   -I./include -I../interfaces/common

CFLAGS          =   -c $(DEFINES) -elf -ansi -pedantic -Wall
LFLAGS          =   -r

OBJS		=	rio_ll_entry.o rio_ll_kernel.o rio_ll_errors.o\
			rio_ll_conf_mp_local_handlers.o rio_ll_conf_mp_remote_handlers.o\
			rio_ll_io_local_handlers.o rio_ll_io_remote_handlers.o\
			rio_ll_error_remote_handlers.o\
			rio_ll_gsm_local_handlers.o rio_ll_gsm_remote_handlers.o rio_ll_ds_handlers.o rio_ll_fc_handlers.o

###############################################################################
# make default target
all :   ll

# make target
$(TARGET) :	$(OBJS)
		ld  $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

#clean data
clean :		
		rm -f *.o
		rm -f $(TARGET)

#make ll relocatable object
ll :		$(TARGET)

#make ll relocatable object with the profiler support
prof :
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_entry.o rio_ll_entry.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_kernel.o rio_ll_kernel.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_errors.o rio_ll_errors.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_conf_mp_local_handlers.o rio_ll_conf_mp_local_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_conf_mp_remote_handlers.o rio_ll_conf_mp_remote_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_io_local_handlers.o rio_ll_io_local_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_io_remote_handlers.o rio_ll_io_remote_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_error_remote_handlers.o rio_ll_error_remote_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_gsm_local_handlers.o rio_ll_gsm_local_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_gsm_remote_handlers.o rio_ll_gsm_remote_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_ds_handlers.o rio_ll_ds_handlers.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_ll_fc_handlers.o rio_ll_fc_handlers.c
		ld  $(LFLAGS) $(OBJS) $(LIBS) -o $(PROFTARGET)

%.o :		%.c
		$(CC) $(CFLAGS) $(INCLUDES) -o $*.o $<

###############################################################################
             
