/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_ds_handlers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains handlers which implement state machines for
*               servicing of locally requested io transactions
*               (handlers implement both local request and remote response
*               state machines)
*
* Notes:        
*
******************************************************************************/

#include "rio_ll_trx_handlers.h"
#include "rio_ll_kernel.h"

#include <assert.h>

/***************************************************************************
 * Function :    RIO_LL_Local_FC_Request_H
 *
 * Description:  function implements state machine for servicing locally 
 *               requested Data Stream Write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Local_FC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
    int result;

#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {

		     /* if this request should go to remote space */
	       case RIO_EVENT_START_TO_REMOTE:
	           {
	                  /* respond to the local device */
	                  RIO_LL_Kernel_Local_Responce(
	                    handle, 
	                    buf_Tag, 
	                    RIO_LR_EXCLUSIVE
	                     );
                   /* send request */
	                 RIO_LL_Kernel_Send_Request( handle, buf_Tag );
	                 RIO_LL_Kernel_Set_Trx_State( handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST );

																                    return RIO_LL_PROGRESS;
		   }

                /* if this request should go to local space */
                case RIO_EVENT_START_TO_LOCAL:
                {
			
	                  /* respond to the local device */
	                  RIO_LL_Kernel_Local_Responce(
	                    handle, 
	                    buf_Tag, 
	                    RIO_LR_EXCLUSIVE
	                     );

                    /* if the transaction shall go to the local config space */
                    if ( RIO_LL_Kernel_Is_To_Config_Space( handle, buf_Tag ) )
                    {
                        /* do configuration registers write */
                        result = RIO_LL_Kernel_Set_Config_Reg( 
                            handle, 
                            buf_Tag
                        );

                        switch ( result ) {
                            
                            case RIO_OK:
                                /* invoke done function */
                                RIO_LL_Kernel_FC_Request_Done(
                                    handle, 
                                    buf_Tag, 
                                    RIO_REQ_DONE 
                                );
                            break;

                            case RIO_ERROR:
                                /* invoke done function */
                                RIO_LL_Kernel_FC_Request_Done(
                                    handle, 
                                    buf_Tag, 
                                    RIO_REQ_ERROR
                                );
                            break;

                        }
                            
                        return RIO_LL_COMPLETE;
		    }
                    
		}
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        
	}
        case RIO_LL_STATE_SEND_REQUEST:
        {
            switch (event)
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* invoke done function */
                    RIO_LL_Kernel_FC_Request_Done(
                        handle, 
                        buf_Tag, 
                        RIO_REQ_DONE 
                    );

                    return RIO_LL_COMPLETE;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
        }

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}




/***************************************************************************
 * Function :    RIO_LL_Remote_FC_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested IO Stream Write transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               Transaction handlers for the Stream Write and NWrite 
 *               transactions are now exactly equal. Byt there are made two
 *               transaction handlers for the protocol change case to 
 *               easely modify transaction handler for the individual 
 *               transaction.
 *
 **************************************************************************/
int RIO_LL_Remote_FC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    /* check transaction state */
    switch ( state )
    {
        /* transaction in its initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                /* if there is collision do nothing */
                case RIO_EVENT_START_COLLISION_STALLED:
                    return RIO_LL_PROGRESS;

                /* if this request should go to local space */
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* if the transaction shall go to the local config space */
                    if ( RIO_LL_Kernel_Is_To_Config_Space( handle, buf_Tag ) )
                    {
                        /* do configuration registers write */
                        RIO_LL_Kernel_Set_Config_Reg( 
                            handle, 
                            buf_Tag
                        );

                        return RIO_LL_COMPLETE;
                    }

                    /* if there is no memory in PE complete transaction */

                    else

                    return RIO_LL_PROGRESS;
                }

                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }

        /* transaction is performing snoop request */

        /* transaction is performing memory access */

        /* any other states are impossible for this type of transaction */
        default:
            return RIO_LL_ERROR;
    }
}


