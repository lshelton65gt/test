/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/ll/rio_ll_gsm_remote_handlers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains handlers which implement state machines for
*               servicing of remotely requested gsm transactions
*
* Notes:        
*
******************************************************************************/

#include "rio_ll_trx_handlers.h"
#include "rio_ll_kernel.h"

#include <assert.h>

/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_Read_Home_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM read home transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               RIO spec doesn't divide remote read request to two state
 *               machines, i.e. READ_HOME and READ_OWNER, but we do this
 *               for clarity
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_Read_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */

                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ, RIO_SNOOP_READ_HOME);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_SHARED:
                        {
                            /* request memory location */
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_ADD_SHARED_REQUESTOR_ID);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            if( RIO_LL_Kernel_Is_Intervention_Case(handle, buf_Tag) == RIO_TRUE ) 
                            {
                                /* mask_id ~== received_srcid */
                                RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                    RIO_MASK_ID, RIO_PL_GSM_READ_OWNER, RIO_FALSE);    
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, 
                                    RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                                return RIO_LL_PROGRESS;
                            }
                            else
                                return RIO_LL_ERROR;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }

                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* write data to memory; response will be sent when memory is updated */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    /* read from memory */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            /* memory access is finished; update directory state and send response packet */
            switch( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                { 
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_DONE);              
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY:
        {
            /* response to request has been received */
            switch( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* write data to memory */
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* castout hasn't been received yet; spin with request */
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, READ_OWNER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                /* send done intervention to original requestor and update directory */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE_INTERV);             
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }

            break;
        }
        default:
            return RIO_LL_ERROR;
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}


/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_Read_Owner_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM read owner transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               RIO spec doesn't divide remote read request to two state
 *               machines, i.e. READ_HOME and READ_OWNER, but we do this
 *               for clarity
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_Read_Owner_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */

                    /* check that PE is configured to have processor */
                    if ( RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                            RIO_LOCAL_READ, RIO_SNOOP_READ_OWNER);
                    /* wait for snoop to complete */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_NOT_OWNER:
                {
                    /* send NOT_OWNER response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* send response with data provided by processor's caches */
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);

                    if( RIO_LL_Kernel_Is_RCVSRCID_EQ_RCVSECID(handle, buf_Tag) == RIO_TRUE )
                    {
                        /* received_srcid == received_secid */
                        /* original requestor is also home */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_INTERV);                
                    }
                    else
                    {
                        /* force PL's intervention engine to send INTERV and DATA_ONLY */   
                        RIO_LL_Kernel_Send_Responce( handle, buf_Tag, RIO_RESPONCE,
                            RIO_RESPONCE_INTERVENTION, RIO_RESPONCE_STATUS_DONE );
                    }
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);               
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
            return RIO_LL_ERROR;
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_I_Read_Home_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM instruction read home
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_I_Read_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                {
                    /* normal branch */

                    /* check that PE is configured to have memory and memory directory */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ, RIO_SNOOP_READ_HOME);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_SHARED:
                        {
                            /* request memory location */
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_ADD_SHARED_REQUESTOR_ID);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            if( RIO_LL_Kernel_Is_Intervention_Case(handle, buf_Tag) == RIO_TRUE )
                            {
                                /* mask_id ~== received srcid intervention case */
                                RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                    RIO_MASK_ID, RIO_PL_GSM_READ_OWNER, RIO_FALSE);    
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                                return RIO_LL_PROGRESS;
                            }
                            else
                            {
                                /* data cache paradox case: he already owned it */
                                RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                    RIO_MASK_ID, RIO_PL_GSM_READ_OWNER, RIO_TRUE);    
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_PARADOX_SEND_REQUEST_FOR_THIRD_PARTY);
                                return RIO_LL_PROGRESS;
                            }
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default: 
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* write data to memory; response will be sent when memory is updated */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    /* read from memory */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            /* memory access is finished; update directory state and send response packet */
            switch( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                { 
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_DONE);              
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY:
        {
            /* response to request has been received */
            switch( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* write data to memory */
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* castout hasn't been received yet; spin with request */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_OWNER, RIO_FALSE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                /* send done intervention to original requestor and update directory */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE_INTERV);             
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_PARADOX_SEND_REQUEST_FOR_THIRD_PARTY:
        {
            /* response to request has been received */
            /* this is cache paradox case */
            switch( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_PARADOX_SEND_REQUEST_FOR_THIRD_PARTY);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* write data to memory */
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_PARADOX_SEND_REQUEST_FOR_THIRD_PARTY);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* castout hasn't been received yet; spin with request */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_PARADOX_SEND_REQUEST_FOR_THIRD_PARTY);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* send done and update directory */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_REQUESTOR_ID);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_DONE);              
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        default:
            return RIO_LL_ERROR;
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_Read_To_Own_Home_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM read home transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_Read_To_Own_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */

                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE || 
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ_TO_OWN, RIO_SNOOP_READ_TO_OWN_HOME);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            if( RIO_LL_Kernel_Is_Intervention_Case(handle, buf_Tag) == RIO_TRUE )
                            {
                                /* mask_id ~== received srcid intervention case */
                                RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                    RIO_MASK_ID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_FALSE);    
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                                return RIO_LL_PROGRESS;
                            }
                            else
                                return RIO_LL_ERROR;
                        }
                        case RIO_LL_SHARED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ_TO_OWN, RIO_SNOOP_READ_TO_OWN_HOME);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED:
        {
            /* processor has invalidated shared copy of the cacheline */
            /* send done if he was the only sharer or dkill_sharer to other participants */
            switch ( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                case RIO_EVENT_SNOOP_MISS:
                {
                    if( RIO_LL_Kernel_Is_Requestor_The_Only_Remote_Sharer(handle, buf_Tag) == RIO_TRUE )
                    {
                        /* mask == received_srcid */
                        /* requestor is the only remote sharer */
                        RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                        RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                        return RIO_LL_PROGRESS;
                    }
                    else
                    {
                        /* there are other remote sharers */
                        /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, mask~=received_srcid, my_id, my_id); */
                        RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                            RIO_MASK_WO_RECEIVED_SRCID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                        return RIO_LL_PROGRESS;
                    }
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* write data to memory; response will be sent when memory is updated */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    /* read from memory */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            /* memory access is finished; update directory state and send response packet */
            switch( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                { 
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_DONE);              
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_OWNER:
        {
            /* response to request has been received */
            switch( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* write data to memory */
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* castout hasn't been received yet; spin with request */
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                               READ_TO_OWN_OWNER, received_srcid, my_id, original_srcid); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_FALSE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* castout hasn't been received yet; spin with request */
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                               READ_TO_OWN_OWNER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_SHARED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* final response for DKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* return data to original requestor */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }               
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                /* send done intervention to original requestor and update directory */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE_INTERV);             
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }

            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}




/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_Read_To_Own_Owner_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM read owner transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               RIO spec doesn't divide remote read request to two state
 *               machines, i.e. READ_HOME and READ_OWNER, but we do this
 *               for clarity
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_Read_To_Own_Owner_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */

                    if ( RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                            RIO_LOCAL_READ_TO_OWN, RIO_SNOOP_READ_TO_OWN_OWNER);
                    /* wait for snoop to complete */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_NOT_OWNER:
                {
                    /* send NOT_OWNER response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* send response with data provided by processor's caches */
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);

                    if( RIO_LL_Kernel_Is_RCVSRCID_EQ_RCVSECID(handle, buf_Tag) == RIO_TRUE )
                    {                   
                        /* received_srcid == received_secid */
                        /* original requestor is also home */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_INTERV);                
                    }
                    else
                    {
                        /* force PL's intervention engine to send INTERV and DATA_ONLY */   
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE,
                            RIO_RESPONCE_INTERVENTION, RIO_RESPONCE_STATUS_DONE);
                    }
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);               
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_DKILL_Home_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM read home transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               RIO spec doesn't divide remote read request to two state
 *               machines, i.e. DKILL_HOME and DKILL_SHARER, but we do this
 *               for clarity
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_DKILL_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* cache paradoxes; DKILL is write-hit-on-shared */
                            return RIO_LL_ERROR;
                        }
                        case RIO_LL_SHARED:
                        {
                            /* this is the right case, send invalidates to the sharing list */
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_DKILL, RIO_SNOOP_DKILL_HOME);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED:
        {
            /* processor has invalidated shared copy of the cacheline */
            /* send done if he was the only sharer or dkill_sharer to other participants */
            switch ( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                case RIO_EVENT_SNOOP_MISS:
                {
                    if( RIO_LL_Kernel_Is_Requestor_The_Only_Remote_Sharer(handle, buf_Tag) == RIO_TRUE )
                    {
                        /* mask == received_srcid */
                        /* requestor is the only remote sharer */
                        RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }
                    else
                    {
                        /* there are other remote sharers */
                        /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, mask~=received_srcid, my_id, NULL); */
                        RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                            RIO_MASK_WO_RECEIVED_SRCID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                        return RIO_LL_PROGRESS;
                    }
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* i'm home module working for a third party */
            /* final response for DKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* return data to original requestor */
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_REMOTE_MODIFIED);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }               
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_DKILL_Sharer_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM DKILL owner
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               RIO spec doesn't divide remote read request to two state
 *               machines, i.e. DKILL_HOME and DKILL_SHARER, but we do this
 *               for clarity
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_DKILL_Sharer_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                            RIO_LOCAL_READ_TO_OWN, RIO_SNOOP_DKILL_SHARER);
                    /* wait for snoop to complete */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_END_COLLISION_DONE:
                {
                   /* due to address collision with outstanding READ_TO_OWN_HOME 
                      which got RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_NOT_OWNER:
                {
                    /* send NOT_OWNER response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                case RIO_EVENT_SNOOP_MISS:
                {
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}

/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_IKILL_Home_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM IKILL HOME transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_IKILL_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    /* send invalidates to all domain participants w/o received src_id list */
                    if( RIO_LL_Kernel_Coh_Domain_Size(handle) <= 2 )
                    {
                        /* don't send IKILL_SHARERS if coherence domain is 2 */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }
                    else
                    {
                        RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                            RIO_MASK_PARTICIPANT_WO_RECEIVED_SRCID, RIO_PL_GSM_IKILL_SHARER, RIO_TRUE);    
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                        return RIO_LL_PROGRESS;
                    }

                    break;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* i'm home module working for a third party */
            /* final response for DKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    /* return data to original requestor */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }               
                /* any other response event is erroneous; send error to original requestor */
                /* RETRY responses from coherence domain participants have been handled by multicast engine */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}


/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_IKILL_Sharer_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM IKILL SHARER transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/

int RIO_LL_Remote_GSM_IKILL_Sharer_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                            RIO_LOCAL_IKILL, RIO_SNOOP_IKILL);
                    /* wait for snoop to complete */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_END_COLLISION_DONE:
                {
                   /* due to address collision */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_NOT_OWNER:
                {
                    /* send NOT_OWNER response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                case RIO_EVENT_SNOOP_MISS:
                {
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_IO_Read_Home_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM read home transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               RIO spec doesn't divide remote read request to two state
 *               machines, i.e. IO_READ_HOME and IO_READ_OWNER, but we do this
 *               for clarity
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_IO_Read_Home_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ_LATEST, RIO_SNOOP_IO_READ_HOME);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_SHARED:
                        {
                            /* request memory location */
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                               IO_READ_OWNER, mask_id, my_id, received_srcid); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_ID, RIO_PL_GSM_IO_READ_OWNER, RIO_FALSE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_DONE);              
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    /* snoop miss is erroneous in this situation; send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            /* memory access is finished; update directory state and send response packet */
            switch( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                { 
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_DONE);              
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY:
        {
            /* response to request has been received */
            switch( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* responce doesn't include any data because owner preserved the ownership of cacheline */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE_INTERV);             
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            /* data comes from memory */
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRAP_READ);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            /* castout hasn't been received yet; spin with request */
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, IO_READ_OWNER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_IO_READ_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_FOR_THIRD_PARTY);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        default:
            return RIO_LL_ERROR;
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_IO_Read_Owner_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM read owner transaction for reading shared copy
 *               of cacheline
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *               RIO spec doesn't divide remote read request to two state
 *               machines, i.e. IO_READ_HOME and IO_READ_OWNER, but we do this
 *               for clarity
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_IO_Read_Owner_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                            RIO_LOCAL_READ_LATEST, RIO_SNOOP_IO_READ_OWNER);
                    /* wait for snoop to complete */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_NOT_OWNER:
                {
                    /* send NOT_OWNER response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    /* send response with data provided by processor's caches */
                    RIO_LL_Kernel_Place_Snoop_Data_To_Buffer(handle, buf_Tag, RIO_TRUE);
                    if( RIO_LL_Kernel_Is_RCVSRCID_EQ_RCVSECID(handle, buf_Tag) == RIO_TRUE )
                    {
                        /* received_srcid == received_secid */
                        /* original requestor is also home */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WITH_DATA, RIO_RESPONCE_STATUS_INTERV);                
                    }
                    else
                    {
                        /* force PL's intervention engine to send INTERV and DATA_ONLY */   
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE,
                            RIO_RESPONCE_INTERVENTION_WO_DATA, RIO_RESPONCE_STATUS_DONE);
                    }
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    /* must have cast it out during an address collision */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_NOT_OWNER);               
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_TLBIE_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM TLBIE transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_TLBIE_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                            RIO_LOCAL_TLBIE, RIO_SNOOP_TLBIE);
                    /* wait for snoop to complete */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_TLBSYNC_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM TLBSYNC transaction
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_TLBSYNC_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */
                    if ( RIO_LL_Kernel_Have_Processor( handle ) != RIO_TRUE )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                            RIO_LOCAL_TLBSYNC, RIO_SNOOP_TLBSYNC);
                    /* wait for snoop to complete */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            switch( event )
            {
                case RIO_EVENT_SNOOP_MISS:
                case RIO_EVENT_SNOOP_HIT:
                {
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_Castout_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM castout transaction 
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_Castout_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                {
                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            /* memory access is finished; update directory state and send response packet */
            switch( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                { 
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
} 



/***************************************************************************
 * Function :    RIO_LL_Remote_GSM_Flush_Request_H
 *
 * Description:  function implements state machine for servicing remotely 
 *               requested GSM flush transaction 
 *
 * Returns:      RIO_LL_PROGRESS, RIO_LL_COMPLETE or RIO_LL_ERROR
 *
 * Notes:        this function uses only interface defined in the 
 *               rio_ll_kernel.h. 
 *
 **************************************************************************/
int RIO_LL_Remote_GSM_Flush_Request_H(
    void               *handle, 
    RIO_LL_BUF_TAG_T   buf_Tag,
    RIO_LL_TRX_STATE_T state, 
    RIO_LL_TRX_EVENT_T event
)
{
#ifdef _DEBUG
    assert( handle );
#endif 

    switch ( state )
    {
        /* transaction in initial state */
        case RIO_LL_STATE_INIT:
        {
            switch ( event )
            {
                case RIO_EVENT_START_NO_COLLISION:
                case RIO_EVENT_END_COLLISION_NORMAL:
                {
                    /* normal branch */

                    if ( RIO_LL_Kernel_Have_Memory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Have_Directory( handle ) != RIO_TRUE ||
                        RIO_LL_Kernel_Directory_State(handle, buf_Tag) == RIO_LL_ADDR_OUT_OF_RANGE
                    )
                    {
                        /* send error response */
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }

                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_MODIFIED:
                        case RIO_LL_LOCAL_SHARED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ_TO_OWN, RIO_SNOOP_FLUSH);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_SNOOP_COMPLETE);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            if( RIO_LL_Kernel_Is_Intervention_Case(handle, buf_Tag) == RIO_TRUE )
                            {
                                /* mask_id ~== received srcid intervention case */
                                RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                    RIO_MASK_ID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                                return RIO_LL_PROGRESS;
                            }
                            else
                            {
                                /* shouldn't generate a FLUSH; send ERROR response */
                                RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                    RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                                return RIO_LL_PROGRESS;
                            }
                        }
                        case RIO_LL_SHARED:
                        {
                            RIO_LL_Kernel_Snoop_Request(handle, buf_Tag, 
                                    RIO_LOCAL_READ_TO_OWN, RIO_SNOOP_FLUSH);
                            /* wait for snoop to complete */
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_START_COLLISION_STALLED:
                {
                    /* transaction is stalled; preserve the state and wait for end stall event */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_INIT);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_RETRY:
                {
                    /* send RETRY response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_RETRY);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_START_COLLISION_ERROR:
                case RIO_EVENT_END_COLLISION_ERROR:
                {
                    /* send ERROR response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_RESPONSE:
        {
            switch( event )
            {
                /* transaction has completed successfully */
                case RIO_EVENT_RESPONSE_SENT:
                    return RIO_LL_COMPLETE;
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }
            break;
        }
        case RIO_LL_STATE_WAIT_CACHELINE_INVALIDATED:
        {
            switch ( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                case RIO_EVENT_SNOOP_MISS:
                {
                    if( RIO_LL_Kernel_Is_Requestor_The_Only_Remote_Sharer(handle, buf_Tag) == RIO_TRUE )
                    {
                        /* mask == received_srcid */
                        /* requestor is the only remote sharer */
                        RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                        if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                        {
                            RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                            return RIO_LL_PROGRESS;
                        }
                        else
                        {
                            RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                    RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                            return RIO_LL_PROGRESS;
                        }
                    }
                    else
                    {
                        /* there are other remote sharers */
                        /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, mask~=received_srcid, my_id, my_id); */
                        RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                            RIO_MASK_WO_RECEIVED_SRCID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                        return RIO_LL_PROGRESS;
                    }
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_WAIT_SNOOP_COMPLETE:
        {
            RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);

            switch( event )
            {
                case RIO_EVENT_SNOOP_HIT:
                {
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_SNOOP_MISS:
                {
                    /* write data to memory; response will be sent when memory is updated */
                    if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                    {
                        RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                        return RIO_LL_PROGRESS;
                    }
                    else
                    {
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_ACCESS:
        {
            /* memory access is finished; update directory state and send response packet */
            switch( event )
            {
                case RIO_EVENT_MEMORY_COMPLETE:
                { 
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                default:
                    return RIO_LL_ERROR;
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_OWNER:
        {
            /* response to request has been received */
            switch( event )
            {
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_INTERVENTION:
                {
                    /* write data to memory */
                    RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_UPDATE_ACCESS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_NOT_OWNER:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                            {
                                RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                                return RIO_LL_PROGRESS;
                            }
                            else
                            { 
                                RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                                return RIO_LL_PROGRESS;
                            }
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                case RIO_EVENT_RESPONSE_RETRY:
                {
                    switch( RIO_LL_Kernel_Directory_State(handle, buf_Tag) )
                    {
                        case RIO_LL_LOCAL_SHARED:
                        case RIO_LL_LOCAL_MODIFIED:
                        {
                            if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                            {
                                RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                                return RIO_LL_PROGRESS;
                            }
                            else
                            { 
                                RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                                RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                                return RIO_LL_PROGRESS;
                            }
                        }
                        case RIO_LL_REMOTE_MODIFIED:
                        {
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_READ_TO_OWN_OWNER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_OWNER);
                            return RIO_LL_PROGRESS;
                        }
                        case RIO_LL_SHARED:
                        {
                            /* RIO_LL_Kernel_Send_Altered_Request(handle, DKILL_SHARER, received_srcid, my_id, my_id); */
                            RIO_LL_Kernel_Send_Altered_Request(handle, buf_Tag, 
                                RIO_MASK_RECEIVED_SRCID, RIO_PL_GSM_DKILL_SHARER, RIO_TRUE);    
                            RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                            return RIO_LL_PROGRESS;
                        }
                        default:
                            /* error */
                            return RIO_LL_ERROR;
                    }
                    break;
                }
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        case RIO_LL_STATE_SEND_REQUEST_TO_SHARERS:
        {
            /* final response for DKILL_SHARERS multicast has been received */
            switch ( event )
            {
                /* request sent */
                case RIO_EVENT_REQUEST_SENT:
                {
                    /* stay in the same state and wait for the response packet */
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_REQUEST_TO_SHARERS);
                    return RIO_LL_PROGRESS;
                }
                case RIO_EVENT_RESPONSE_DONE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                    if( RIO_LL_Kernel_Received_Data(handle, buf_Tag) == RIO_TRUE )
                    {
                        RIO_LL_Kernel_Memory_Request(handle, buf_Tag, RIO_MEM_WRITE);
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_MEMORY_ACCESS);
                        return RIO_LL_PROGRESS;
                    }
                    else
                    { 
                        RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                                RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                        RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                        return RIO_LL_PROGRESS;
                    }
                    break;
                }               
                /* any other response event is erroneous; send error to original requestor */
                default:
                {
                    /* send error response */
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                        RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_ERROR);
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
            }
            break;
        }
        case RIO_LL_STATE_MEMORY_UPDATE_ACCESS:
        {
            switch (event)
            {
                /* send done intervention to original requestor and update directory */
                case RIO_EVENT_MEMORY_COMPLETE:
                {
                    RIO_LL_Kernel_Update_Directory_State(handle, buf_Tag, RIO_SET_SHARED_MY_ID);
                    RIO_LL_Kernel_Send_Responce(handle, buf_Tag, RIO_RESPONCE, 
                            RIO_RESPONCE_WO_DATA, RIO_RESPONCE_STATUS_DONE);                
                    RIO_LL_Kernel_Set_Trx_State(handle, buf_Tag, RIO_LL_STATE_SEND_RESPONSE);
                    return RIO_LL_PROGRESS;
                }
                /* any other event is impossible for this state */
                default:
                    return RIO_LL_ERROR;                    
            }

            break;
        }
        default:
        {
            return RIO_LL_ERROR;
        }
    }       /* end of state switch */

    /* should never come here (if we do, handler was modified inappropriately */
    return RIO_LL_ERROR;
}


/* EOF */ 

