#ifndef RIO_PCS_PMA_DP_DS_H
#define RIO_PCS_PMA_DP_DS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_dp_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer data structure declaration file
*
* Notes:
*
******************************************************************************/

#ifndef RIO_PCS_PMA_DP_H
#include "rio_pcs_pma_dp.h"
#endif /* RIO_PCS_PMA_DP_H */


#define RIO_PL_SERIAL_CODE_GROUP_BIT_SIZE    10
#define RIO_PL_SERIAL_ALLOWED_ALIGN_GAP        32


#define RIO_PCS_PMA_DP_PD_LANE_BUF_TOP        0
#define RIO_PCS_PMA_DP_PD_LANE_BUF_BOTTOM    9


typedef enum {
    RIO_PCS_PMA_DP_FLAG_IDLES_INSIDE_PACKET = 0,

    /* total amount */
    RIO_PCS_PMA_DP_FLAGS_AMOUNT
} RIO_PCS_PMA_DP_FLAGS_T;


typedef struct {
    RIO_SIGNAL_T    lane_Buf[RIO_PL_SERIAL_CODE_GROUP_BIT_SIZE];
    RIO_UCHAR_T        buf_Pointer;
    RIO_BOOL_T        is_Full;
    RIO_UCHAR_T        self_Number; /*this is the number of lane to which it belongs*/
    RIO_BOOL_T        is_Out_Lane; /*tells if this is in or out lane*/
    RIO_HANDLE_T    mum_Instanse; /*the pointer to the upper instanse where it stored*/
}RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T; 


typedef struct {
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T    out_Lanes[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T    in_Lanes[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_PCS_PMA_CODE_GROUP_T            out_Code_Group_Column;
    RIO_HANDLE_T    mum_Instanse; /*the pointer to the upper instanse where it stored*/
} RIO_PL_PCS_PMA_DP_PD_DS_T; 

typedef struct {
    RIO_UCHAR_T                character_Data[RIO_PL_SERIAL_ALLOWED_ALIGN_GAP];
    RIO_CHARACTER_TYPE_T    character_Type[RIO_PL_SERIAL_ALLOWED_ALIGN_GAP];
    RIO_UCHAR_T                head_Pointer;
    RIO_UCHAR_T                tail_Pointer;
    RIO_BOOL_T                is_Full;
    RIO_BOOL_T                is_Empty;
    RIO_BOOL_T                is_Running_Disparity_Positive;
}RIO_PL_PCS_PMA_DP_CL_LANE_CLASS_T; 


typedef struct {
    /* RIO_HANDLE_T mum_Instanse; */ /*the pointer to the upper instanse where it stored*/

    RIO_PL_PCS_PMA_DP_CL_LANE_CLASS_T  in_Lanes[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_BOOL_T                         is_Out_Running_Disparity_Positive[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_HANDLE_T                       mum_Instanse; /*the pointer to the upper instanse where it stored*/
} RIO_PL_PCS_PMA_DP_CL_DS_T; 

typedef struct {
    RIO_PCS_PMA_CHARACTER_T  out_Granule;
    RIO_PCS_PMA_CHARACTER_T  in_Granule;
    RIO_HANDLE_T             mum_Instanse; /*the pointer to the upper instanse where it stored*/
} RIO_PL_PCS_PMA_DP_SB_DS_T; 

typedef struct {
    unsigned long                            instance_Number;
    RIO_PL_PCS_PMA_DP_PD_DS_T                pins_Driver_Data;
    RIO_PL_PCS_PMA_DP_CL_DS_T                coding_Logic_Data;
    RIO_PL_PCS_PMA_DP_SB_DS_T                streaming_Block_Data;
        
    RIO_PCS_PMA_DP_CALLBACK_TRAY_T           interfaces;
    RIO_BOOL_T                               is_Ctray_Registered;

    RIO_PCS_PMA_INITIALIZE                   restart_Function;
    RIO_PCS_PMA_START_RESET                  reset_Function;

    RIO_BOOL_T                               is_In_Reset;

    RIO_BOOL_T                               is_Flag_Asserted[RIO_PCS_PMA_DP_FLAGS_AMOUNT];

    RIO_HANDLE_T                             mum_Instanse;
} RIO_PL_PCS_PMA_DP_DS_T;

/***************************************************************************************/
#endif /* RIO_PCS_PMA_DP_DS_H */
