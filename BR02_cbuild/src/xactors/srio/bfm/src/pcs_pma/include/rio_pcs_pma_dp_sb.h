#ifndef RIO_PCS_PMA_DP_SB_H
#define RIO_PCS_PMA_DP_SB_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_dp_sb.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    internal PCS/PMA Data Processor: Streaming Block
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_dp_ds.h"


#define RIO_BYTE1_SHIFT     16
#define RIO_BYTE2_SHIFT      8


int rio_PCS_PMA_DP_Get_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

int rio_PCS_PMA_DP_Put_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

int rio_PCS_PMA_DP_Encode_Granule(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_GRANULE_T     *granule,         /* pointer to the granule data structure */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to the character data structure */
);

int rio_PCS_PMA_DP_Decode_Granule(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T      *character,       /* pointer to the character data structure */
    RIO_PCS_PMA_GRANULE_T     *granule          /* pointer to the granule data structure */
);

int rio_PCS_PMA_DP_Set_Character_Byte_0(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T      *in_Character,
    RIO_UCHAR_T               byte_Number,      /* byte number in the in_Character */
    RIO_PCS_PMA_CHARACTER_T      *out_Character
);

int rio_PCS_PMA_DP_Get_Character_Byte_0(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T      *in_Character,
    RIO_PCS_PMA_CHARACTER_T      *out_Character,
    RIO_UCHAR_T               byte_Number       /* byte number in the in_Character */
);

int rio_PCS_PMA_DP_Calculate_CRC(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_GRANULE_T     *granule          /* pointer to the granule data structure */
);

int rio_PCS_PMA_DP_Check_CRC(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_GRANULE_T     *granule          /* pointer to the granule data structure */
);

int RIO_PCS_PMA_DP_SB_Initialize(
    RIO_PL_PCS_PMA_DP_SB_DS_T          *dp_SB_Instance
);


#endif /* RIO_PCS_PMA_DP_SB_H */
