#ifndef RIO_PCS_PMA_PM_INTERFACE_H
#define RIO_PCS_PMA_PM_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_pm_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA Protocol Manager interface.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_interface.h"


/*
 * the data type represents PM parameters set
 */
/*
typedef struct RIO_PCS_PMA_PM_PARAM_SET_TAG{
    unsigned char dummy;
} RIO_PCS_PMA_PM_PARAM_SET_T;
*/
typedef RIO_PCS_PMA_PARAM_SET_T RIO_PCS_PMA_PM_PARAM_SET_T;


/* 
 * the function is used to initialize and re-initialize the PCS/PMA PM
 */
typedef int (*RIO_PCS_PMA_PM_INITIALIZE)(
    RIO_HANDLE_T                handle,
    RIO_PCS_PMA_PM_PARAM_SET_T  *param_Set
    );

#endif /* RIO_PCS_PMA_PM_INTERFACE_H */
