#ifndef RIO_SERIAL_GRAN_TYPE_H
#define RIO_SERIAL_GRAN_TYPE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_serial_gran_type.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
* 
* Description:  This file contains granule data types,
*                
* Notes:      
* 
******************************************************************************/
#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif /* RIO_TYPES_H */

#ifndef RIO_SERIAL_TYPES_H
#include "rio_serial_types.h"
#endif /* RIO_SERIAL_TYPES_H */

/* cmd field in LINK_REQUEST */
typedef enum {
    RIO_PL_SERIAL_LREQ_RESET = 3,
    RIO_PL_SERIAL_LREQ_INPUT_STATUS = 4
} RIO_PL_SERIAL_LINK_REQUEST_CMD_T;

typedef enum {
    RIO_PL_SERIAL_GR_PACKET_ACCEPTED_STYPE0 = 0 ,
    RIO_PL_SERIAL_GR_PACKET_RETRY_STYPE0 = 1,
    RIO_PL_SERIAL_GR_PACKET_NOT_ACCEPTED_STYPE0 = 2 ,
    RIO_PL_SERIAL_GR_IDLE_STYPE0 = 4,
    RIO_PL_SERIAL_GR_LINK_RESPONSE_STYPE0 = 6
} RIO_PL_SERIAL_SYMBOL_STYPE0_T;


typedef enum {
    RIO_PL_SERIAL_GR_START_OF_PACKET_STYPE1 = 0 ,
    RIO_PL_SERIAL_GR_STOMP_STYPE1 = 1,
    RIO_PL_SERIAL_GR_EOP_STYPE1 = 2 ,
    RIO_PL_SERIAL_GR_RFR_STYPE1 = 3,
    RIO_PL_SERIAL_GR_LINK_REQUEST_STYPE1 = 4,
    RIO_PL_SERIAL_GR_MULTICAST_EVENT_STYPE1 = 5,
    RIO_PL_SERIAL_GR_NOP_STYPE1 = 7
} RIO_PL_SERIAL_SYMBOL_STYPE1_T;

typedef enum {
    RIO_PL_SERIAL_GR_COMMON_STYPE1_CMD = 0
} RIO_PL_SERIAL_SYMBOL_STYPE1_CMD_T;

#endif /* RIO_SERIAL_GRAN_TYPE_H*/
