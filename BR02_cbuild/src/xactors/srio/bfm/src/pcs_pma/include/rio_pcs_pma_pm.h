#ifndef RIO_PCS_PMA_PM_H
#define RIO_PCS_PMA_PM_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_pm.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_pm_interface.h"
#include "rio_pcs_pma_inside_interface.h"


/* 
 * function tray to be passed by the PCS/PMA protocol manager
 */
typedef struct {
    RIO_PCS_PMA_START_RESET         start_Reset;
    RIO_PCS_PMA_PM_INITIALIZE       initialize;
    RIO_PCS_PMA_MACHINE_MANAGEMENT    machine_Management;
    RIO_DELETE_INSTANCE                delete_Instance;

    RIO_PCS_PMA_TRANSFER_GRANULE    put_Granule;
    RIO_PCS_PMA_TRANSFER_GRANULE    get_Granule;

    RIO_PCS_PMA_TRANSFER_CHARACTER    put_Character_Column;
    RIO_PCS_PMA_TRANSFER_CHARACTER    get_Character_Column;

    RIO_PCS_PMA_TRANSFER_CODEGROUP    put_Codegroup;
    RIO_PCS_PMA_TRANSFER_CODEGROUP    get_Codegroup_Column;

} RIO_PCS_PMA_PM_FTRAY_T;

/* 
 * callback tray to be provided for the PCS/PMA protocol manager
 */
typedef struct RIO_PCS_PMA_PM_CALLBACK_TRAY_TAG{
    RIO_PCS_PMA_MSG                 rio_User_Msg;
    RIO_CONTEXT_T                   user_Msg_Context;

    RIO_SET_STATE                   rio_Set_State_Flag;
    RIO_GET_STATE                   rio_Get_State_Flag;
    RIO_CONTEXT_T                   rio_Regs_Context;

    RIO_PCS_PMA_TRANSFER_GRANULE    get_Granule;
    RIO_PCS_PMA_TRANSFER_GRANULE    put_Granule;
    RIO_CONTEXT_T                    granule_Level_Context;

    RIO_PCS_PMA_TRANSFER_CHARACTER    put_Character_Column;
    RIO_PCS_PMA_TRANSFER_CHARACTER    get_Character_Column;
    RIO_PCS_PMA_POP_CHAR_BUFFER        pop_Char_Buffer;
    RIO_CONTEXT_T                    character_Level_Context;

    RIO_PCS_PMA_TRANSFER_CODEGROUP        put_Codegroup;
    RIO_PCS_PMA_TRANSFER_CODEGROUP        get_Codegroup_Column;
    RIO_PCS_PMA_POP_CODEGROUP_BUFFER    pop_Codegroup_Buffer;
    RIO_CONTEXT_T                        codegroup_Level_Context;
    RIO_SYMBOL_TO_SEND              rio_Symbol_To_Send;   /*bug-132*/
} RIO_PCS_PMA_PM_CALLBACK_TRAY_T;

/* 
 * the function creates instance of PCS/PMA protocol manager and returns handle
 * to its data structure. There are no create parameters.
 */
int RIO_PCS_PMA_PM_Create_Instance(
    RIO_HANDLE_T                     *handle,
    RIO_HANDLE_T                     mum_Instanse
    );

/* 
 * the function returns interface functions to be used for PCS/PMA protocol manager access.
 * function tray is allocated by invokating side and pointer to tray is passed.
 * link only stores actual entry points there.
 */
int RIO_PCS_PMA_PM_Get_Function_Tray(
    RIO_PCS_PMA_PM_FTRAY_T           *ftray
    );

/* 
 * the function binds link PCA/PMA protocol manager instance to the environment.
 * Callback tray is allocated outside, stored with actual entry points and pointer
 * to it is passed here. Link copies these pointers to its internal data.
 */
int RIO_PCS_PMA_PM_Bind_Instance(
    RIO_HANDLE_T                     handle,
    RIO_PCS_PMA_PM_CALLBACK_TRAY_T   *ctray
    );

int rio_PCS_PMA_PM_Display_Msg(
    RIO_HANDLE_T                handle,
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
);


#endif /* RIO_PCS_PMA_PM_H */
