#ifndef RIO_PCS_PMA_INSIDE_INTERFACE_H
#define RIO_PCS_PMA_INSIDE_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_inside_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_LP_SERIAL_INTERFACE_H
#include "rio_lp_serial_interfaces.h"
#endif /* RIO_LP_SERIAL_INTERFACE_H */

/* 
 * this callback is invoked to clean the upper element in the specified lane
 * in the characters buffer
 */ 
typedef int (*RIO_PCS_PMA_POP_CHAR_BUFFER)(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_UCHAR_T                  lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
);

/* 
 * this callback is invoked to clean the codegroup bits storage
 * (it's 10-bit long FIFO)
 */ 
typedef int (*RIO_PCS_PMA_POP_CODEGROUP_BUFFER)(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_UCHAR_T                  lane_ID            /* 0 - for lane 0, 1 - for lane 1, etc. */
);


#endif /* RIO_PCS_PMA_INSIDE_INTERFACE_H */
