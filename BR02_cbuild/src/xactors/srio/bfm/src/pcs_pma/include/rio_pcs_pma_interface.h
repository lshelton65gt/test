#ifndef RIO_PCS_PMA_INTERFACE_H
#define RIO_PCS_PMA_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\pcs_pma\include\rio_pcs_pma_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes external TxRxM interface.
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_LP_SERIAL_INTERFACE_H
#include "rio_lp_serial_interface.h"
#endif /* RIO_LP_SERIAL_INTERFACE_H */

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif /* RIO_COMMON_H */

#ifndef RIO_SERIAL_COMMON_H
#include "rio_serial_common.h"
#endif /* RIO_SERIAL_COMMON_H */

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif /* RIO_TYPES_H */

#ifndef RIO_SERIAL_TYPES_H
#include "rio_serial_types.h"
#endif /* RIO_SERIAL_TYPES_H */

typedef enum {
    RIO_PL_SERIAL_MSG_WARNING_MSG,
    RIO_PL_SERIAL_MSG_ERROR_MSG,
    RIO_PL_SERIAL_MSG_NOTE_MSG
} RIO_PL_SERIAL_MSG_TYPE_T;


typedef int (*RIO_PCS_PMA_MSG) (
    RIO_CONTEXT_T                context,    /* callback context */
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
    );

/*to provide clock*/
typedef int (*RIO_PCS_PMA_CLOCK_EDGE)(
    RIO_HANDLE_T handle      /* instance's handle */
    ); 

/*to start reset*/
typedef int (*RIO_PCS_PMA_START_RESET)(
    RIO_HANDLE_T handle       /* instance's handle */
    );

/*
 * to switch on/off sync, align, init, seq_gen machines. 
 */
typedef int (*RIO_PCS_PMA_MACHINE_MANAGEMENT)(
    RIO_HANDLE_T                handle,
    RIO_BOOL_T                    machine_Is_Switched_On, /*RIO_TRUE if seq must be sent*/
    RIO_PCS_PMA_MACHINE_T       machine_Type /*size is 4*/
    );

typedef struct {
    /*RIO_TRUE if init seq shall be performed by txrx model itself.
    In this case the sync and align management functions don't work (?)*/
    RIO_BOOL_T      is_Init_Proc_On;
    unsigned int    silence_Period;
    unsigned int    discovery_Period;

    RIO_BOOL_T      is_Comp_Seq_Gen_On;
    unsigned int    comp_Seq_Rate; /*in spec - 5000*/

    RIO_BOOL_T      is_Status_Sym_Gen_On;
    unsigned int    status_Sym_Rate; /*in spec - 1024 code groups*/

    unsigned int    comp_Seq_Size; /*in spec - 5000*/
    unsigned int    comp_Seq_Rate_Check; /*in spec - 5000*/

    unsigned int    icounter_Max;  /*max value of Icounter 
                                    for switching to the NO_SYNC in spec - 2*/
    unsigned int    status_Symbols_To_Send;
    unsigned int    status_Symbols_To_Receive;

} RIO_PCS_PMA_PARAM_SET_T;
/* 
 * the function is used to initialize and re-initialize the RIO model. It set 
 * TXRXM_SERIAL model instance parameters to requested values and turn the TXRXM_SERIAL model
 * instance to work mode
 */
typedef int (*RIO_PCS_PMA_INITIALIZE)(
    RIO_HANDLE_T                handle,
    RIO_PCS_PMA_PARAM_SET_T        *param_Set
    );





/*for Bus Checker*/

typedef enum {
    RIO_PCS_PMA_IC_IN_GRANULE,
    RIO_PCS_PMA_CC_NOT_IN_FIRST_GRANULE_BYTE,
    RIO_PCS_PMA_SC_GOT_INSTEAD_PD,
    RIO_PCS_PMA_PD_GOT_INSTEAD_SC,
    RIO_PCS_PMA_WRONG_SYMBOL_CRC,
    RIO_PCS_PMA_IDLE_SEQ_IN_PACKET,
    RIO_PCS_PMA_NON_IDLE_IN_IDLE_SEQ
} RIO_PCS_PMA_GRANULE_VIOLATION_T;

typedef int (*RIO_PCS_PMA_PUT_CORRUPTED_GRANULE)(
    RIO_CONTEXT_T                   handle_context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T           *granule,          /* pointer to granule data structure */
    RIO_PCS_PMA_CHARACTER_T         *character,        /* pointer to character data structure */
    RIO_PCS_PMA_GRANULE_VIOLATION_T violation
);

#endif /* RIO_PCS_PMA_INTERFACE_H */


