#ifndef RIO_PCS_PMA_PM_INIT_H
#define RIO_PCS_PMA_PM_INIT_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_pm_init.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface init part.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_pm_ds.h"


int rio_PCS_PMA_PM_Get_Granule(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
);

int rio_PCS_PMA_PM_Put_Granule(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
);

int rio_PCS_PMA_PM_Exec_Init_1x_FSM(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance
);

int rio_PCS_PMA_PM_Exec_Init_1x4x_FSM(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_BOOL_T              lp_Serial_Is_1x,
    RIO_BOOL_T              is_Force_1x_Mode,
    RIO_BOOL_T              is_Force_1x_Mode_Lane_0
);


#endif /* RIO_PCS_PMA_PM_INIT_H */
