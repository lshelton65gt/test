#ifndef RIO_PCS_PMA_DP_H
#define RIO_PCS_PMA_DP_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_dp.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_LP_SERIAL_INTERFACE_H
#include "rio_lp_serial_interface.h"
#endif /* RIO_LP_SERIAL_INTERFACE_H */

#ifndef RIO_PCS_PMA_INTERFACE_H
#include "rio_pcs_pma_interface.h"
#endif /* RIO_PCS_PMA_INTERFACE_H */

#ifndef RIO_PCS_PMA_INSIDE_INTERFACE_H
#include "rio_pcs_pma_inside_interface.h"
#endif /* RIO_PCS_PMA_INSIDE_INTERFACE_H */

/* 
 * function tray to be passed by the PCS/PMA data processor
 */
typedef struct {
    RIO_PCS_PMA_START_RESET                start_Reset;
    RIO_PCS_PMA_INITIALIZE                initialize;
    
    RIO_PCS_PMA_CLOCK_EDGE                clock_Edge;
    RIO_DELETE_INSTANCE                    rio_Link_Delete_Instance;

    RIO_SERIAL_GET_PINS                    rio_Serial_Get_Pins; 
   
    RIO_PCS_PMA_TRANSFER_CHARACTER        put_Character_Column;
    RIO_PCS_PMA_TRANSFER_CHARACTER        get_Character_Column;

    RIO_PCS_PMA_TRANSFER_CODEGROUP        put_Codegroup;
    RIO_PCS_PMA_TRANSFER_CODEGROUP        get_Codegroup_Column;

    RIO_PCS_PMA_POP_CHAR_BUFFER            pop_Char_Buffer;
    RIO_PCS_PMA_POP_CODEGROUP_BUFFER    pop_Codegroup_Buffer;

} RIO_PCS_PMA_DP_FTRAY_T;


/* 
 * callback tray to be provided for the PCS/PMA data processor
 */
typedef struct {
    RIO_SERIAL_SET_PINS                rio_Serial_Set_Pins;
    RIO_CONTEXT_T                    lp_Serial_Interface_Context;

    RIO_PCS_PMA_MSG                 rio_User_Msg;
    RIO_CONTEXT_T                   user_Msg_Context;

    RIO_SET_STATE                   rio_Set_State_Flag;
    RIO_GET_STATE                   rio_Get_State_Flag;
    RIO_CONTEXT_T                   rio_Regs_Context;

    RIO_PCS_PMA_TRANSFER_GRANULE        get_Granule;
    RIO_PCS_PMA_TRANSFER_GRANULE        put_Granule;
    RIO_PCS_PMA_PUT_CORRUPTED_GRANULE   put_Corrupted_Granule;
    RIO_CONTEXT_T                       granule_Level_Context;

    RIO_PCS_PMA_TRANSFER_CHARACTER    put_Character_Column;
    RIO_PCS_PMA_TRANSFER_CHARACTER    get_Character_Column;
    RIO_CONTEXT_T                    character_Level_Context;

    RIO_PCS_PMA_TRANSFER_CODEGROUP    put_Codegroup;
    RIO_PCS_PMA_TRANSFER_CODEGROUP    get_Codegroup_Column;
    RIO_CONTEXT_T                    codegroup_Level_Context;
    RIO_SYMBOL_TO_SEND              rio_Symbol_To_Send;   /*bug-132*/


} RIO_PCS_PMA_DP_CALLBACK_TRAY_T;

/* 
 * the function creates instance of PCS/PMA data processor and returns handle
 * to its data structure. There are no create parameters.
 */
int RIO_PCS_PMA_DP_Create_Instance(
    RIO_HANDLE_T                     *handle,
    RIO_HANDLE_T                     mum_Instanse
    );
    
/* 
 * the function returns interface functions to be used for PCS/PMA data processor access.
 * function tray is allocated by invokating side and pointer to tray is passed.
 * link only stores actual entry points there.
 */
int RIO_PCS_PMA_DP_Get_Function_Tray(
    RIO_PCS_PMA_DP_FTRAY_T           *ftray
    );


/* 
 * the function binds link PCA/PMA data processor instance to the environment.
 * Callback tray is allocated outside, stored with actual entry points and pointer
 * to it is passed here. Link copies these pointers to its internal data.
 */
int RIO_PCS_PMA_DP_Bind_Instance(
    RIO_HANDLE_T                     handle,
    RIO_PCS_PMA_DP_CALLBACK_TRAY_T   *ctray
    );


int rio_PCS_PMA_DP_Display_Msg(
    RIO_HANDLE_T                handle,
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
);

/*for internal use*/
int RIO_Pcs_Pma_Dp_Set_Pins(
    RIO_HANDLE_T handle, /* enviroment's context */
    RIO_SIGNAL_T tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3       /* transmit data on lane 3*/
    );

int RIO_Pcs_Pma_Dp_Put_Codegroup(
    RIO_HANDLE_T handle, /* enviroment's context */
    RIO_PCS_PMA_CODE_GROUP_T *code_Group
    );

int RIO_Pcs_Pma_Dp_Get_Codegroup_Column(
    RIO_HANDLE_T handle, /* enviroment's context */
    RIO_PCS_PMA_CODE_GROUP_T *code_Group_Column
    );

#endif /* RIO_PCS_PMA_DP_H */
