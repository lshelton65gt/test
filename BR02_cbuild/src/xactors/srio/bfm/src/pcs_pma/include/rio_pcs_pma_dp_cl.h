#ifndef RIO_PCS_PMA_DP_CL_H
#define RIO_PCS_PMA_DP_CL_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_dp_cl.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    internal PCS/PMA Data Processor: Coding Logic block
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_dp_ds.h"

/* 0b0011 */
#define RIO_RD_POSITIVE_4BIT_SHIFT      0x3
/* 0b1100 */
#define RIO_RD_NEGATIVE_4BIT_SHIFT      0xC
/* 0b000111 */
#define RIO_RD_POSITIVE_6BIT_SHIFT      0x7
/* 0b111000 */
#define RIO_RD_NEGATIVE_6BIT_SHIFT      0x38


int rio_PCS_PMA_DP_Get_Codegroup_Column(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

int rio_PCS_PMA_DP_Put_Codegroup(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

int rio_PCS_PMA_DP_Pop_Char_Buffer(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_UCHAR_T                        lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
);

int rio_PCS_PMA_DP_Encode(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T            *character,       /* pointer to character data structure */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

int rio_PCS_PMA_DP_Decode(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup,       /* pointer to codegroup data structure */
    RIO_PCS_PMA_CHARACTER_T            *character        /* pointer to character data structure */
);

int rio_PCS_PMA_DP_Encode_Single_Character(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    RIO_BOOL_T                      is_Data_Character,
    RIO_UCHAR_T                     character_Data,
    RIO_CHARACTER_TYPE_T            character_Type,
    unsigned int                    *code_Group_Data,
    RIO_BOOL_T                      *is_Running_Disparity_Positive
);

int rio_PCS_PMA_DP_Decode_Single_Character(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    unsigned int                    code_Group_Data,
    RIO_UCHAR_T                     *character_Data,
    RIO_CHARACTER_TYPE_T            *character_Type,
    RIO_BOOL_T                      *is_Running_Disparity_Positive
);

int rio_PCS_PMA_DP_Calculate_Running_Disparity(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    unsigned int                    code_Group_Data,
    RIO_BOOL_T                      *is_Running_Disparity_Positive
);

int rio_PCS_PMA_DP_Count_Ones_And_Zeros(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    RIO_UCHAR_T                     sub_Block,
    RIO_UCHAR_T                     length,
    RIO_UCHAR_T                     *ones_Counter,
    RIO_UCHAR_T                     *zeros_Counter
);

int RIO_PCS_PMA_DP_CL_Reset_Queues(
    RIO_PL_PCS_PMA_DP_CL_DS_T          *dp_CL_Instance
);

int RIO_PCS_PMA_DP_CL_Initialize(
    RIO_PL_PCS_PMA_DP_CL_DS_T          *dp_CL_Instance
);


#endif /* RIO_PCS_PMA_DP_CL_H */
