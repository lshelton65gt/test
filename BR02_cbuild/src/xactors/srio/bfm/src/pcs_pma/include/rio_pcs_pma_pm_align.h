#ifndef RIO_PCS_PMA_PM_ALIGN_H
#define RIO_PCS_PMA_PM_ALIGN_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\pcs_pma\include\rio_pcs_pma_pm_align.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_pm_ds.h"


int rio_PCS_PMA_PM_Get_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

int rio_PCS_PMA_PM_Put_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

int rio_PCS_PMA_PM_Exec_Align_FSM(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_CHARACTER_TYPE_T  character_Type[RIO_PL_SERIAL_NUMBER_OF_LANES]
);

RIO_BOOL_T are_All_Lanes_Sync(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance
);

RIO_BOOL_T is_Align_Column(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,
    RIO_CHARACTER_TYPE_T    character_Type[RIO_PL_SERIAL_NUMBER_OF_LANES]
);

RIO_BOOL_T is_Align_Error(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,
    RIO_CHARACTER_TYPE_T    character_Type[RIO_PL_SERIAL_NUMBER_OF_LANES]
);

void rio_PCS_PMA_PM_Check_Comp_Seq(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character,       /* pointer to character data structure */
    RIO_BOOL_T is_4x_Mode
);

int rio_PCS_PMA_PM_Generate_Comp_Seq(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_CHARACTER_TYPE_T  *character_Type
);

int rio_PCS_PMA_PM_Get_Next_Idle(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_CHARACTER_TYPE_T  *character_Type
);

/* 
 * the below functions are placed in rio_pcs_pma_pm.c
 */
int rio_PCS_PMA_PM_Get_Port_Control(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,
    RIO_BOOL_T              *lp_Serial_Is_1x,
    RIO_BOOL_T              *is_Force_1x_Mode,
    RIO_BOOL_T              *is_Force_1x_Mode_Lane_0
);

int rio_PCS_PMA_PM_Reset_Port_Regs(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance
);

int rio_PCS_PMA_PM_Correct_A_Against_Comp_Seq(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance
);

int rio_PCS_PMA_PM_Correct_A_Against_Status_Sym(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_BOOL_T            is_True_4x
);

#endif /* RIO_PCS_PMA_PM_ALIGN_H */
