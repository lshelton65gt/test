#ifndef RIO_PCS_PMA_DS_H
#define RIO_PCS_PMA_DS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include <malloc.h>
#include <stdio.h>

#include "rio_serial_types.h"


typedef struct {
    unsigned long                           instance_Number;
    struct RIO_PCS_PMA_MODEL_CALLBACK_TRAY_TAG  *ctray;
    RIO_BOOL_T                              is_Ctray_Registered;
    RIO_PCS_PMA_PARAM_SET_T                 *parameters_Set;
    RIO_BOOL_T                              is_In_Reset;

    RIO_HANDLE_T                            pm_Handle;
    RIO_PCS_PMA_PM_FTRAY_T                  pm_Ftray;
    RIO_HANDLE_T                            dp_Handle;
    RIO_PCS_PMA_DP_FTRAY_T                  dp_Ftray;
} RIO_PCS_PMA_DS_T;

#endif /* RIO_PCS_PMA_DS_H */
