#ifndef RIO_PCS_PMA_DP_PD_H
#define RIO_PCS_PMA_DP_PD_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_dp_pd.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_LP_SERIAL_INTERFACE_H
#include "rio_lp_serial_interface.h"
#endif /* RIO_LP_SERIAL_INTERFACE_H */

#ifndef RIO_PCS_PMA_INTERFACE_H
#include "rio_pcs_pma_interface.h"
#endif /* RIO_PCS_PMA_INTERFACE_H */

#ifndef RIO_PCS_PMA_INSIDE_INTERFACE_H
#include "rio_pcs_pma_inside_interface.h"
#endif /* RIO_PCS_PMA_INSIDE_INTERFACE_H */

#include "rio_pcs_pma_dp_ds.h"


int Dp_Pd_Pop_Code_Group_Buffer(
    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse,  /* callback handle/context */
    RIO_UCHAR_T        lane_ID            /* 0 - for lane 0, 1 - for lane 1, etc. */
    );

int RIO_Pcs_Pma_Dp_Pd_Tx_Clock(RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse); /*pd handle*/

int RIO_Pcs_Pma_Dp_Pd_Get_Pins(
    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse, /* pd instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
    );

int RIO_Pcs_Pma_Dp_Pd_Initialize(
    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse
    );


#endif /* RIO_PCS_PMA_DP_PD_H */
