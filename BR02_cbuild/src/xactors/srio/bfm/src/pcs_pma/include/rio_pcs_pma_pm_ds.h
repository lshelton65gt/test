#ifndef RIO_PCS_PMA_PM_DS_H
#define RIO_PCS_PMA_PM_DS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\pcs_pma\include\rio_pcs_pma_pm_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include <malloc.h>
#include <stdio.h>

#include "rio_serial_types.h"
#include "rio_pcs_pma_pm_interface.h"


/* FOR DEBUG */
#ifdef DEBUG_ALL_GENERATION
#define DEBUG_A_GENERATION
#define _CUSTOM_DEBUG
#endif /* DEBUG_ALL_GENERATION */



#define PCS_PMA_PM__SYNC_NEXT_STATE(param_state, param_lane_id)    pm_Instance->sync_State[param_lane_id] = param_state
#define PCS_PMA_PM__ALIGN_NEXT_STATE(param_state)                  pm_Instance->align_State = param_state
#define PCS_PMA_PM__INIT_NEXT_STATE(param_state)                   pm_Instance->init_State = param_state



#define RIO__SYNC_FSM__COMMA_CONST_1      127
#define RIO__SYNC_FSM__COMMA_CONST_2        2
#define RIO__SYNC_FSM__COMMA_CONST_3      255

#define RIO__SYNC_FSM__ALIGN_FSM_CONST_1        4
#define RIO__SYNC_FSM__ALIGN_FSM_CONST_2        2
#define RIO__SYNC_FSM__ALIGN_FSM_CONST_3        4

/*#define PCS_PMA_PM__DISCOVERY_PERIOD           10*/

/* in codegroups for 1x link and in columns for 4x link */
#define PCS_PMA_PM__COMPENSATION_SEQUENCE_SIZE  4

/* in codegroups  */
#define PCS_PMA_PM__STATUS_SYMBOL_SIZE          4


/* specifies number of characters (columns) before 5000 to start issuing next compensation sequience */
#define PCS_PMA_PM__COMPENSATION_DELTA          4 /* for status symbol */

#define PCS_PMA_PM__CHARACTER_A_COUNTER_MASK   0x0F
#define PCS_PMA_PM__CHARACTER_A_COUNTER_BASE   (0x10 + 1)
    /* '+1' is because the actual pminimum period for A is 16 (16 non-A between two A) */

/* compensation sequence generation rate boundaries */
#define PCS_PMA_PM__COMP_SEQ_RATE_MAX       5000
#define PCS_PMA_PM__COMP_SEQ_RATE_MIN        300

/* status symbol generation rate boundaries */
#define PCS_PMA_PM__STATUS_SYMBOL_RATE_MAX  1024
#define PCS_PMA_PM__STATUS_SYMBOL_RATE_MIN   132

/* status symbol init amount boundaries */
#define PCS_PMA_PM__STATUS_SYM_TO_SEND_MAX     100
#define PCS_PMA_PM__STATUS_SYM_TO_SEND_MIN       0
#define PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MAX  100
#define PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MIN    0


typedef enum {
    PCS_PMA_PM__SYNC_FSM__NO_SYNC = 0,
    PCS_PMA_PM__SYNC_FSM__NO_SYNC_PART_2,
    PCS_PMA_PM__SYNC_FSM__NO_SYNC_1,
    PCS_PMA_PM__SYNC_FSM__NO_SYNC_2,
    PCS_PMA_PM__SYNC_FSM__SYNC,
    PCS_PMA_PM__SYNC_FSM__SYNC_PART_2,
    PCS_PMA_PM__SYNC_FSM__SYNC_1,
    PCS_PMA_PM__SYNC_FSM__SYNC_2_3,
    PCS_PMA_PM__SYNC_FSM__SYNC_4
} RIO_PCS_PMA_PM__SYNC_FSM_STATES_T;

typedef enum {
    PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED = 0,
    PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_PART_2,
    PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_1,
    PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_2,
    PCS_PMA_PM__ALIGN_FSM__ALIGNED,
    PCS_PMA_PM__ALIGN_FSM__ALIGNED_PART_2,
    PCS_PMA_PM__ALIGN_FSM__ALIGNED_1,
    PCS_PMA_PM__ALIGN_FSM__ALIGNED_2,
    PCS_PMA_PM__ALIGN_FSM__ALIGNED_3
} RIO_PCS_PMA_PM__ALIGN_FSM_STATES_T;

typedef enum {
    PCS_PMA_PM__INIT_FSM__SILENT = 0,
    PCS_PMA_PM__INIT_FSM__SILENT_PART_2,
    PCS_PMA_PM__INIT_FSM__SEEK,
    PCS_PMA_PM__INIT_FSM__1X_MODE,          /* 1x link ONLY */

    PCS_PMA_PM__INIT_FSM__DISCOVERY,        /* 1x4x link ONLY */
    PCS_PMA_PM__INIT_FSM__DISCOVERY_PART_2, /* 1x4x link ONLY */
    PCS_PMA_PM__INIT_FSM__4X_MODE,          /* 1x4x link ONLY */
    PCS_PMA_PM__INIT_FSM__1X_MODE_LANE0,    /* 1x4x link ONLY */
    PCS_PMA_PM__INIT_FSM__1X_MODE_LANE2     /* 1x4x link ONLY */
} RIO_PCS_PMA_PM__INIT_FSM_STATES_T;

typedef struct {
    unsigned long                           instance_Number;
    struct RIO_PCS_PMA_PM_CALLBACK_TRAY_TAG *ctray;
    RIO_BOOL_T                              is_Ctray_Registered;
    RIO_PCS_PMA_PM_PARAM_SET_T              *parameters_Set;
    RIO_BOOL_T                              is_In_Reset;

    RIO_BOOL_T                              sync_Status[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_PCS_PMA_PM__SYNC_FSM_STATES_T       sync_State[RIO_PL_SERIAL_NUMBER_OF_LANES];
    unsigned int                            comma_Counter[RIO_PL_SERIAL_NUMBER_OF_LANES];
    unsigned int                            invalids_Counter[RIO_PL_SERIAL_NUMBER_OF_LANES];
    unsigned int                            valids_Counter[RIO_PL_SERIAL_NUMBER_OF_LANES];

    RIO_BOOL_T                              align_Status;
    RIO_PCS_PMA_PM__ALIGN_FSM_STATES_T      align_State;
    unsigned int                            align_Counter;
    unsigned int                            misalign_Counter;
    RIO_BOOL_T                              is_Alignment_Lost;

    RIO_BOOL_T                              is_Init_FSM_On;
    RIO_BOOL_T                              init_Status;
    RIO_PCS_PMA_PM__INIT_FSM_STATES_T       init_State;
    RIO_BOOL_T                              is_Lane_OE[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_UCHAR_T                             lane_Driving_Init_FSM;
    unsigned int                            silence_Timer;
    unsigned int                            discovery_Timer;
    unsigned long                           rcv_Status_Symbols_Counter;
    unsigned long                           snd_Status_Symbols_Counter;
    RIO_BOOL_T                              trx_Requests_Enabled;
    RIO_BOOL_T                              is_Packet_Streaming_Broken;
    RIO_BOOL_T                              is_Comp_Seq_Delayed_For_Status;

    RIO_BOOL_T                              is_Status_Sym_Gen_On;
    RIO_BOOL_T                              is_Comp_Seq_Gen_On;
    unsigned int                            before_Comp_Seq_Counter;
    unsigned int                            comp_Seq_Counter;
    unsigned int                            comp_Seq_Size_Next;
    unsigned int                            before_Status_Symbol_Counter;
    RIO_BOOL_T                              idle_Delayed;
    unsigned int                            idle_Non_A_Counter;

    unsigned int                            comp_Seq_Check_Counter;
    RIO_CHARACTER_TYPE_T                    comp_Seq_Check_Buf[10];

    /* FOR DEBUG */
#ifdef _CUSTOM_DEBUG
    unsigned long                           debug_Iteration_Counter;
#endif /* _CUSTOM_DEBUG */

    RIO_HANDLE_T                            mum_Instanse;

} RIO_PCS_PMA_PM_DS_T;

#endif /* RIO_PCS_PMA_PM_DS_H */
