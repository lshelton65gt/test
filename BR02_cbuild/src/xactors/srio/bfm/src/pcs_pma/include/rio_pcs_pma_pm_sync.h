#ifndef RIO_PCS_PMA_PM_SYNC_H
#define RIO_PCS_PMA_PM_SYNC_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_pm_sync.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_pm_ds.h"


#define RIO_COMMA_SHIFT_LENGTH      3
#define RIO_NEG_COMMA_SHIFTED    0x1F
#define RIO_POS_COMMA_SHIFTED    0x60


int rio_PCS_PMA_PM_Get_Codegroup_Column(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

int rio_PCS_PMA_PM_Put_Codegroup(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

int rio_PCS_PMA_PM_Exec_Sync_FSM(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance,
    unsigned char        lane_ID,
    unsigned int         codegroup
);

RIO_BOOL_T is_Comma(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance,
    unsigned int         codegroup
);

RIO_BOOL_T is_Pos_Comma(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance,
    unsigned int         codegroup
);


#endif /* RIO_PCS_PMA_PM_SYNC_H */
