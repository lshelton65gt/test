#ifndef RIO_PCS_PMA_MODEL_H
#define RIO_PCS_PMA_MODEL_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/include/rio_pcs_pma_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes external TxRxM interface.
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_LP_SERIAL_INTERFACE_H
#include "rio_lp_serial_interface.h"
#endif /* RIO_LP_SERIAL_INTERFACE_H */

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif /* RIO_COMMON_H */

#ifndef RIO_SERIAL_COMMON_H
#include "rio_serial_common.h"
#endif /* RIO_SERIAL_COMMON_H */

#ifndef RIO_PCS_PMA_INTERFACE_H
#include "rio_pcs_pma_interface.h"
#endif /* RIO_PCS_PMA_INTERFACE_H */

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif /* RIO_TYPES_H */

#ifndef RIO_SERIAL_TYPES_H
#include "rio_serial_types.h"
#endif /* RIO_SERIAL_TYPES_H */

/* 
 * function tray to be passed to the environment by the 32 TxRx model 
 */
typedef struct {
    RIO_SERIAL_GET_PINS                rio_Serial_Get_Pins;
    RIO_PCS_PMA_CLOCK_EDGE            clock_Edge;
    RIO_PCS_PMA_START_RESET            start_Reset;
    RIO_PCS_PMA_INITIALIZE            initialize;
    RIO_DELETE_INSTANCE                delete_Instance;
    
    RIO_PCS_PMA_MACHINE_MANAGEMENT    machine_Management;

} RIO_PCS_PMA_MODEL_FTRAY_T;

/* 
 * callback tray to be used by the link (supplied by the model environment). Also
 * includes context of 8LP/EP interface and context of PAL/PDL interface. These
 * contexts using to run corresponding callbacks in proper environment 
 */
typedef struct RIO_PCS_PMA_MODEL_CALLBACK_TRAY_TAG {
    RIO_PCS_PMA_MSG                        rio_User_Msg;
    RIO_CONTEXT_T                        user_Msg_Context;


    RIO_SET_STATE                        rio_Set_State_Flag;
    RIO_GET_STATE                        rio_Get_State_Flag;
    RIO_CONTEXT_T                        rio_Regs_Context;

    RIO_SERIAL_SET_PINS                    rio_Serial_Set_Pins;
    RIO_CONTEXT_T                        lp_Serial_Interface_Context;


    RIO_PCS_PMA_TRANSFER_GRANULE        get_Granule;
    RIO_PCS_PMA_TRANSFER_GRANULE        put_Granule;
    RIO_PCS_PMA_PUT_CORRUPTED_GRANULE   put_Corrupted_Granule;
    RIO_CONTEXT_T                       granule_Level_Context;


    RIO_PCS_PMA_TRANSFER_CHARACTER        put_Character_Column;
    RIO_PCS_PMA_TRANSFER_CHARACTER        get_Character_Column;
    RIO_CONTEXT_T                        character_Level_Context;

    RIO_PCS_PMA_TRANSFER_CODEGROUP        put_Codegroup;
    RIO_PCS_PMA_TRANSFER_CODEGROUP        get_Codegroup_Column;
    RIO_CONTEXT_T                        codegroup_Level_Context;
    RIO_SYMBOL_TO_SEND                    rio_Symbol_To_Send;  /*bug-132*/


} RIO_PCS_PMA_MODEL_CALLBACK_TRAY_T;

/* 
 * the function creates instance of PL model and returns handle to its 
 * data structure. There are not any create parameters for 32 bit link
 */
int RIO_PCS_PMA_Model_Create_Instance(
    RIO_HANDLE_T                        *handle
    );

/* 
 * the function returns link model interface functions to be used for model access.
 * function tray is allocated by invokating side and pointer to tray is passed.
 * link only stores actual entry points there.
 */
int RIO_PCS_PMA_Model_Get_Function_Tray(
    RIO_PCS_PMA_MODEL_FTRAY_T           *ftray
    );

/* 
 * the function binds link model instance to the environment. Callback tray is allocated
 * outside, stored with actual entry points and pointer to it is passed here. Link
 * copies these pointers to its internal data.
 */
int RIO_PCS_PMA_Model_Bind_Instance(
    RIO_HANDLE_T                        handle,
    RIO_PCS_PMA_MODEL_CALLBACK_TRAY_T   *ctray   
    );

#endif /* RIO_PCS_PMA_MODEL_H */


