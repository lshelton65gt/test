/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_dp_cl.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    PCS/PMA Data Processor: Coding Logic block.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_dp_cl.h"
#include "rio_pcs_pma_dp_ds.h"
#include "rio_pcs_pma_dp.h"

#include "rio_gran_type.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

RIO_UCHAR_T RIO_Log2_Values[] =
{
    5,  /* error: log2(0) */
    0,  /* log2(1) */
    1,  /* log2(2) */
    5,  /* error: log2(3) */
    2,  /* log2(4) */
    5,  /* error: log2(5) */
    5,  /* error: log2(6) */
    5,  /* error: log2(7) */
    3,  /* log2(8) */
    5,  /* error: log2(9) */
    5,  /* error: log2(A) */
    5,  /* error: log2(B) */
    5,  /* error: log2(C) */
    5,  /* error: log2(D) */
    5,  /* error: log2(E) */
    5   /* error: log2(F) */
};


/***************************************************************************
 * Function : rio_PCS_PMA_DP_Get_Codegroup_Column
 *
 * Description: encodes character column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Get_Codegroup_Column(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T *)handle_Context;
    RIO_PCS_PMA_CHARACTER_T    character;
    
    if (dp_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Get_Codegroup_Column: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Get_Codegroup_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    dp_Instance->interfaces.get_Character_Column(
        dp_Instance->interfaces.character_Level_Context,
        &character
    );
    codegroup->lane_Mask = character.lane_Mask;

    rio_PCS_PMA_DP_Encode(dp_Instance, &character, codegroup);

    /* encoded codegroup is returned */
    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Put_Codegroup
 *
 * Description: decodes character column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Put_Codegroup(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T *)handle_Context;
    RIO_PCS_PMA_CHARACTER_T    character;
    RIO_PCS_PMA_CHARACTER_T    out_Character;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;
    RIO_UCHAR_T i;
    
    if (dp_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Put_Codegroup: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Put_Codegroup: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    /* 
     * checking if the alignment was lost in the meantime
     */
    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_IS_ALIGNMENT_LOST,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    /* reset buffers upon loosing alignment */
        RIO_PCS_PMA_DP_CL_Reset_Queues(&dp_Instance->coding_Logic_Data);

    /* 
     * handling a case when the very first comma on a lane has a positive running disparity
     */
    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_INITIAL_RUN_DISP,
        &indicator_Value,
        &indicator_Parameter
    );
    if (
        PCS_PMA__RUN_DISP_NEG == indicator_Value ||
        PCS_PMA__RUN_DISP_POS == indicator_Value
    )
    /* reset buffers upon loosing alignment */
    {
        /* RIO_Log2_Values[codegroup->lane_Mask] is lane_ID */
        if (RIO_Log2_Values[codegroup->lane_Mask] >= RIO_PL_SERIAL_NUMBER_OF_LANES)
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Put_Codegroup: unexpected lane_Mask; only one active (masked) lane is expected here"
            );

        if (PCS_PMA__RUN_DISP_NEG == indicator_Value)
            dp_Instance->coding_Logic_Data.in_Lanes[RIO_Log2_Values[codegroup->lane_Mask]].is_Running_Disparity_Positive =
                RIO_FALSE;
        else
            dp_Instance->coding_Logic_Data.in_Lanes[RIO_Log2_Values[codegroup->lane_Mask]].is_Running_Disparity_Positive =
                RIO_TRUE;

        dp_Instance->interfaces.rio_Set_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_INITIAL_RUN_DISP,
            PCS_PMA__RUN_DISP_NOT_CHANGE,
            0 /* don't care */
        );
    }

    /* check port width/mode */
    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_IS_PORT_TRUE_4X,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    /* true 4x link */
    {
        /* check if this is just a call to drive the INIT FSM */
        dp_Instance->interfaces.rio_Get_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_SUPPRESS_HOOK,
            &indicator_Value,
            &indicator_Parameter
        );
        if (RIO_TRUE == indicator_Value)
        {
            /* issue false column to drive the INIT FSM */
            character.character_Type[0] = RIO_R;
            character.character_Type[1] = RIO_R;
            character.character_Type[2] = RIO_R;
            character.character_Type[3] = RIO_R;
            character.lane_Mask = RIO_PL_ALL_LANES_MASK;
            return
                dp_Instance->interfaces.put_Character_Column(
                    dp_Instance->interfaces.character_Level_Context,
                    &character
                );
        }

        /* decode the code-groups */
        rio_PCS_PMA_DP_Decode(dp_Instance, codegroup, &character);
        
        /* 
         * store them in the buffer
         */
        for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        {
            if ((codegroup->lane_Mask >> i) & 1)
            {
                if (dp_Instance->coding_Logic_Data.in_Lanes[i].is_Full)
                {
                    /* pop the top element in the buffer to give place for the incoming one */
                    rio_PCS_PMA_DP_Pop_Char_Buffer(
                        dp_Instance,
                        codegroup->lane_Mask
                    );
                }

                /* place the single character to the buffer */
                dp_Instance->coding_Logic_Data.in_Lanes[i].is_Empty = RIO_FALSE;
                dp_Instance->coding_Logic_Data.in_Lanes[i].character_Type[
                    dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer
                ] = character.character_Type[i];
                if (RIO_D == character.character_Type[i])
                    dp_Instance->coding_Logic_Data.in_Lanes[i].character_Data[
                        dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer
                    ] = character.character_Data[i];
                dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer++;
                if (dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer >= RIO_PL_SERIAL_ALLOWED_ALIGN_GAP)
                {
                    dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer = 0;
                }
                if (dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer == 
                    dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer
                )
                    dp_Instance->coding_Logic_Data.in_Lanes[i].is_Full = RIO_TRUE;
            }
        }

        /* 
         * upload the column at the buffer head to the upper level
         */
        out_Character.lane_Mask = 0;
        for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        {
            if (!dp_Instance->coding_Logic_Data.in_Lanes[i].is_Empty)
            {
                out_Character.character_Type[i] = 
                    dp_Instance->coding_Logic_Data.in_Lanes[i].character_Type[
                        dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer
                    ];
                out_Character.character_Data[i] = 
                    dp_Instance->coding_Logic_Data.in_Lanes[i].character_Data[
                        dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer
                    ];
                out_Character.lane_Mask |= 1 << i;
            }
        }
        dp_Instance->interfaces.put_Character_Column(
            dp_Instance->interfaces.character_Level_Context,
            &out_Character
        );

        /* additional check for internal discrepancies (in the char buffer) */
        for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        {
            if (
                !dp_Instance->coding_Logic_Data.in_Lanes[i].is_Full &&
                !dp_Instance->coding_Logic_Data.in_Lanes[i].is_Empty &&
                dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer ==
                dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer
            )
            {
                char temp_String[256];
                sprintf(
                    temp_String,
                    "rio_PCS_PMA_DP_Put_Codegroup: Empty/Full buffer discrepancy on lane %i; first char: type=%i, value=%X\n",
                    i,
                    dp_Instance->coding_Logic_Data.in_Lanes[i].character_Type[
                        dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer
                    ],
                    dp_Instance->coding_Logic_Data.in_Lanes[i].character_Data[
                        dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer
                    ]
                );
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    temp_String
                );
            }
        }
    }
    else
    /* 1x link */
    {
        /* check if this is just a call to drive the INIT FSM */
        dp_Instance->interfaces.rio_Get_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_SUPPRESS_HOOK,
            &indicator_Value,
            &indicator_Parameter
        );
        if (RIO_TRUE == indicator_Value)
        {
            /* issue false column to drive the INIT FSM */
            character.character_Type[0] = RIO_R;
            character.character_Type[1] = RIO_I;
            character.character_Type[2] = RIO_I;
            character.character_Type[3] = RIO_I;
            character.lane_Mask = 1;
            return
                dp_Instance->interfaces.put_Character_Column(
                    dp_Instance->interfaces.character_Level_Context,
                    &character
                );
        }

        if (1 != codegroup->lane_Mask)
        /* error: unexpected lane_Mask (1 is expected) */
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Put_Codegroup: unexpected lane_Mask (1 is expected)"
            );

        /* decode the code-group */
        rio_PCS_PMA_DP_Decode(dp_Instance, codegroup, &character);
        /* upload it to the upper level */
        dp_Instance->interfaces.put_Character_Column(
            dp_Instance->interfaces.character_Level_Context,
            &character
        );
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Pop_Char_Buffer
 *
 * Description: pops the coulmn at the head of the buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Pop_Char_Buffer(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_UCHAR_T                        lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
)
{
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T *)handle_Context;
    RIO_UCHAR_T i;

    if (dp_Instance == NULL)
        return RIO_ERROR;
    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Pop_Char_Buffer: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Pop_Char_Buffer: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        if ((lane_Mask >> i) & 1)
        {
            if (dp_Instance->coding_Logic_Data.in_Lanes[i].is_Empty)
            {
                /* error: an attempt to pop the buffer on the empty lane (lane #i) */
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_DP_Pop_Char_Buffer: an attempt to pop the buffer on the empty lane"
                );
                continue;
            }

            if (dp_Instance->coding_Logic_Data.in_Lanes[i].is_Full)
            {
                dp_Instance->coding_Logic_Data.in_Lanes[i].is_Full = RIO_FALSE;
            }

            dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer++;
            if (dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer >= RIO_PL_SERIAL_ALLOWED_ALIGN_GAP)
            {
                dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer = 0;
            }
            if (dp_Instance->coding_Logic_Data.in_Lanes[i].head_Pointer == 
                dp_Instance->coding_Logic_Data.in_Lanes[i].tail_Pointer
            )
                dp_Instance->coding_Logic_Data.in_Lanes[i].is_Empty = RIO_TRUE;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Encode
 *
 * Description: encodes character column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Encode(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T         *character,       /* pointer to character data structure */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    unsigned char i;
    RIO_BOOL_T result;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;

    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_LANE2_JUST_FORCED_BY_DISCOVERY,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    {
        dp_Instance->coding_Logic_Data.is_Out_Running_Disparity_Positive[0] = 
            dp_Instance->coding_Logic_Data.is_Out_Running_Disparity_Positive[2];
        /* it is assumed here that RIO_PCS_PMA_DP_CL_Reset_Queues doesn't reset the Run.Disp. */

        dp_Instance->interfaces.rio_Set_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_LANE2_JUST_FORCED_BY_DISCOVERY,
            RIO_FALSE,
            0 /* don't care */
        );
    }
    
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        if ((character->lane_Mask >> i) & 1)
        {
            result = 
                rio_PCS_PMA_DP_Encode_Single_Character(
                    dp_Instance, 
                    RIO_D == character->character_Type[i], /* is_Data_Character */
                    (RIO_UCHAR_T)(
                        RIO_D == character->character_Type[i] ?
                            character->character_Data[i] : 
                            0
                    ),
                    character->character_Type[i],
                    &codegroup->code_Group_Data[i],
                    &dp_Instance->coding_Logic_Data.is_Out_Running_Disparity_Positive[i]
                );
            codegroup->is_Running_Disparity_Positive[i] = 
                dp_Instance->coding_Logic_Data.is_Out_Running_Disparity_Positive[i];
            if (RIO_OK != result)
            /* error: character is not coded successfully => invalid */
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_DP_Encode: character is not coded successfully => invalid"
                );
        }
        else
        {
            codegroup->code_Group_Data[i] = 0;
            codegroup->is_Running_Disparity_Positive[i] = 0;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Decode
 *
 * Description: decodes character column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Decode(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup,       /* pointer to codegroup data structure */
    RIO_PCS_PMA_CHARACTER_T         *character        /* pointer to character data structure */
)
{
    unsigned char i;
    RIO_BOOL_T result;
    RIO_BOOL_T result2;
    unsigned long lanes_With_Invalid_Character;
    unsigned long lanes_With_Any_Runn_Disp_Allowed;
    unsigned long updated_Lanes_With_Any_Runn_Disp_Allowed;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;

    character->lane_Mask = codegroup->lane_Mask;

    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_DECODE_ANY_RUN_DISP,
        &lanes_With_Any_Runn_Disp_Allowed,
        &indicator_Parameter
    );
    updated_Lanes_With_Any_Runn_Disp_Allowed = lanes_With_Any_Runn_Disp_Allowed;
    result2 = RIO_ERROR;
    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_LANE_WITH_INVALID_CHARACTER,
        &lanes_With_Invalid_Character,
        &indicator_Parameter
    );

    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_LANE2_JUST_FORCED_BY_DISCOVERY,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    {
        dp_Instance->coding_Logic_Data.in_Lanes[0].is_Running_Disparity_Positive = 
            dp_Instance->coding_Logic_Data.in_Lanes[2].is_Running_Disparity_Positive;
        RIO_PCS_PMA_DP_CL_Reset_Queues(&dp_Instance->coding_Logic_Data);
        /* it is assumed here that RIO_PCS_PMA_DP_CL_Reset_Queues doesn't reset the Run.Disp. */
    }

    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        if ((codegroup->lane_Mask >> i) & 1)
        {
            result = 
                rio_PCS_PMA_DP_Decode_Single_Character(
                    dp_Instance, 
                    codegroup->code_Group_Data[i],
                    &(character->character_Data[i]),
                    &(character->character_Type[i]),
                    &dp_Instance->coding_Logic_Data.in_Lanes[i].is_Running_Disparity_Positive
                );
            if (
                ((lanes_With_Any_Runn_Disp_Allowed >> i) & 1) &&
                RIO_OK != result
            )
            /*
             * second attempt to decode character: with RunningDisparity
             * that was not originally expected;
             * the second attempt is allowed only if commanded during 
             * processing of the previous codegroup for this lane
             */
            {
                dp_Instance->coding_Logic_Data.in_Lanes[i].is_Running_Disparity_Positive = 
                    1 - dp_Instance->coding_Logic_Data.in_Lanes[i].is_Running_Disparity_Positive;
                result2 = 
                    rio_PCS_PMA_DP_Decode_Single_Character(
                        dp_Instance, 
                        codegroup->code_Group_Data[i],
                        &(character->character_Data[i]),
                        &(character->character_Type[i]),
                        &dp_Instance->coding_Logic_Data.in_Lanes[i].is_Running_Disparity_Positive
                    );
            }

            if (
                RIO_OK != result &&
                RIO_OK != result2
            )
            /*
             * character is not decoded successfully
             * (from the second attempt also - if it was allowed) => invalid
             */
            {
                char *out_Message;
                char *in_Message = "rio_PCS_PMA_DP_Decode: codegroup is not decoded successfully " \
                    "(lane_ID=%i, CG=%i, is_Pos_RD=%i) => INVALID";

                out_Message = (char *)malloc(strlen(in_Message) + 50 /* length of the 3 parameters + extra */);
                if (out_Message == NULL)
                {
                    rio_PCS_PMA_DP_Display_Msg(
                        dp_Instance,
                        RIO_PL_SERIAL_MSG_ERROR_MSG,
                        "rio_PCS_PMA_DP_Decode: cannot allocate memory"
                    );
                }
                else
                {
                    RIO_UCHAR_T lane_ID;

                    dp_Instance->interfaces.rio_Get_State_Flag(
                        dp_Instance->interfaces.rio_Regs_Context,
                        dp_Instance->mum_Instanse,
                        RIO_COPY_TO_LANE2,
                        &indicator_Value,
                        &indicator_Parameter
                    );
                    if (RIO_TRUE == indicator_Value)
                        lane_ID = 2; /* lane 2 */
                    else
                        lane_ID = RIO_Log2_Values[codegroup->lane_Mask];
                    
                    sprintf(
                        out_Message,
                        in_Message,
                        lane_ID,
                        codegroup->code_Group_Data[i],
                        dp_Instance->coding_Logic_Data.in_Lanes[i].is_Running_Disparity_Positive
                    );
                    /* error: codegroup is not decoded successfully => invalid */
                    rio_PCS_PMA_DP_Display_Msg(
                        dp_Instance,
                        RIO_PL_SERIAL_MSG_ERROR_MSG,
                        out_Message
                    );
                    free(out_Message);
                }
                
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_TRUE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_IS_CHARACTER_INVALID,
                    RIO_TRUE,
                    0 /* don't care */
                );
                lanes_With_Invalid_Character |= 1 << i;

                /* allow to search both in positive and negative
                 * Running Disparity tables during the decoding 
                 * of the next codegroup for this lane
                 */
                updated_Lanes_With_Any_Runn_Disp_Allowed |= (1 << i);

                character->character_Type[i] = RIO_I;
                character->character_Data[i] = 0;
            }
            else
            /* character is decoded successfully but from the second attempt */
            {
                updated_Lanes_With_Any_Runn_Disp_Allowed &= ~(1 << i);
            }
        }
        else
        {
            character->character_Data[i] = 0;
            character->character_Type[i] = RIO_I;
        }
    }

    if (lanes_With_Invalid_Character)
        dp_Instance->interfaces.rio_Set_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_LANE_WITH_INVALID_CHARACTER,
            lanes_With_Invalid_Character, /* lane mask */
            0 /* don't care */
        );
    if (updated_Lanes_With_Any_Runn_Disp_Allowed != lanes_With_Any_Runn_Disp_Allowed)
        dp_Instance->interfaces.rio_Set_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_DECODE_ANY_RUN_DISP,
            updated_Lanes_With_Any_Runn_Disp_Allowed,
            0 /* don't care */
        );

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Calculate_Running_Disparity
 *
 * Description: calculates Running Disparity
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Calculate_Running_Disparity(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    unsigned int                    code_Group_Data,
    RIO_BOOL_T                      *is_Running_Disparity_Positive
)
{
    RIO_UCHAR_T left_Part; /* 6-bit part */
    RIO_UCHAR_T right_Part; /* 4-bit part */
    RIO_UCHAR_T ones_Counter;
    RIO_UCHAR_T zeros_Counter;
    
    left_Part = (RIO_UCHAR_T )((code_Group_Data >> 4) & 0x3F);  /* first (left) 6-bit sub-code-group is taken */
    right_Part = (RIO_UCHAR_T )(code_Group_Data & 0xF);         /* second (right) 4-bit sub-code-group is taken */

    /* check right_Part first */
    rio_PCS_PMA_DP_Count_Ones_And_Zeros(
        dp_Instance,
        right_Part,
        4, /* 4 bits in this sub-code-group */
        &ones_Counter,
        &zeros_Counter
    );
    if (ones_Counter > zeros_Counter ||
        RIO_RD_POSITIVE_4BIT_SHIFT == right_Part
    )
    {
        *is_Running_Disparity_Positive = RIO_TRUE;
        return RIO_OK;
    }
    else
    if (zeros_Counter > ones_Counter ||
        RIO_RD_NEGATIVE_4BIT_SHIFT == right_Part
    )
    {
        *is_Running_Disparity_Positive = RIO_FALSE;
        return RIO_OK;
    }
    
    /* check left_Part */
    rio_PCS_PMA_DP_Count_Ones_And_Zeros(
        dp_Instance,
        left_Part,
        6, /* 6 bits in this sub-code-group */
        &ones_Counter,
        &zeros_Counter
    );
    if (ones_Counter > zeros_Counter ||
        RIO_RD_POSITIVE_6BIT_SHIFT == left_Part
    )
    {
        *is_Running_Disparity_Positive = RIO_TRUE;
        return RIO_OK;
    }
    else
    if (zeros_Counter > ones_Counter ||
        RIO_RD_NEGATIVE_6BIT_SHIFT == left_Part
    )
    {
        *is_Running_Disparity_Positive = RIO_FALSE;
        return RIO_OK;
    }

    /* running disparity is left unchanged */
    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Count_Ones_And_Zeros
 *
 * Description: calculates 1s and 0s in the sub-block
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Count_Ones_And_Zeros(
    RIO_PL_PCS_PMA_DP_DS_T          *dp_Instance,
    RIO_UCHAR_T                     sub_Block,
    RIO_UCHAR_T                     length,
    RIO_UCHAR_T                     *ones_Counter,
    RIO_UCHAR_T                     *zeros_Counter
)
{
    RIO_UCHAR_T  i;

    if (
        ones_Counter == NULL ||
        zeros_Counter == NULL
    )
        rio_PCS_PMA_DP_Display_Msg(
            dp_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_DP_Count_Ones_And_Zeros: input parameter is a NULL pointer"
        );

    
    *ones_Counter = 0;
    *zeros_Counter = 0;
    for (i = 0; i < length; i++)
    {
        if (1 == (sub_Block & 1))
            (*ones_Counter)++;
        else
            (*zeros_Counter)++;
        sub_Block = (unsigned char)(sub_Block >> 1);
    }
    if (*ones_Counter > *zeros_Counter)
        return RIO_TRUE;
    else
    {
        return RIO_FALSE;
    }
}

/***************************************************************************
 * Function : RIO_PCS_PMA_DP_CL_Reset_Queues
 *
 * Description: reset CL queues
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PCS_PMA_DP_CL_Reset_Queues(
    RIO_PL_PCS_PMA_DP_CL_DS_T          *dp_CL_Instance
)
{
    RIO_UCHAR_T i;

    /*
     * check handle isn't NULL, check parameter set is valid,
     * check that the instance is in reset mode,
     * set link parameters to necessary values and turn the
     * instance to work mode.
     */
    if (dp_CL_Instance == NULL)
        return RIO_ERROR;

    ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_CL_Instance->mum_Instanse))->interfaces.rio_Set_State_Flag(
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_CL_Instance->mum_Instanse))->interfaces.rio_Regs_Context,
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_CL_Instance->mum_Instanse))->mum_Instanse,
        RIO_IS_ALIGNMENT_LOST,
        RIO_FALSE,
        0 /* don't care */
    );

    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        dp_CL_Instance->in_Lanes[i].head_Pointer = 0;
        dp_CL_Instance->in_Lanes[i].tail_Pointer = 0;
        dp_CL_Instance->in_Lanes[i].is_Empty = RIO_TRUE;
        dp_CL_Instance->in_Lanes[i].is_Full = RIO_FALSE;
    }
    
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PCS_PMA_DP_CL_Initialize
 *
 * Description: initialize CL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PCS_PMA_DP_CL_Initialize(
    RIO_PL_PCS_PMA_DP_CL_DS_T          *dp_CL_Instance
)
{
    RIO_UCHAR_T i;

    /*
     * check handle isn't NULL, check parameter set is valid,
     * check that the instance is in reset mode,
     * set link parameters to necessary values and turn the
     * instance to work mode.
     */
    if (dp_CL_Instance == NULL)
        return RIO_ERROR;

    RIO_PCS_PMA_DP_CL_Reset_Queues(dp_CL_Instance);

    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        dp_CL_Instance->in_Lanes[i].is_Running_Disparity_Positive = RIO_FALSE;
        dp_CL_Instance->is_Out_Running_Disparity_Positive[i] = RIO_FALSE;
    }
    
    return RIO_OK;
}

/*****************************************************************************/
