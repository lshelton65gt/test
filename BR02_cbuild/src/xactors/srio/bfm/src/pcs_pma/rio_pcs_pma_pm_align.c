/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_pm_align.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include <stdlib.h>

#include "rio_pcs_pma_pm_align.h"
#include "rio_pcs_pma_pm_ds.h"
#include "rio_pcs_pma_pm.h"

/* FOR DEBUG */
/*#define DEBUG_ELIMINATE_RAND*/
/*#define ENABLE_DISCREPANCY*/
/*#define DEBUG_ALL_GENERATION*/

/* FOR DEBUG */
#ifdef DEBUG_ALL_GENERATION
#define DEBUG_A_GENERATION
#define _CUSTOM_DEBUG
#endif /* DEBUG_ALL_GENERATION */



/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Character_Column
 *
 * Description: requests a next codegroup for transmission through the link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Get_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle_Context;
    int result;
    RIO_BOOL_T    lp_Serial_Is_1x;
    RIO_BOOL_T    is_Force_1x_Mode;
    RIO_BOOL_T    is_Force_1x_Mode_Lane_0;
    unsigned long indicator_Value;
    unsigned int  indicator_Parameter;
#ifdef DEBUG_A_GENERATION
    char s[50];
#endif /* DEBUG_A_GENERATION */
    
    if (pm_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Get_Character_Column: is called while in reset\n");
#else
	if(pm_Instance->ctray)
		pm_Instance->ctray->rio_User_Msg(pm_Instance->ctray->user_Msg_Context,
				RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Get_Character_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    rio_PCS_PMA_PM_Get_Port_Control(
        pm_Instance,
        &lp_Serial_Is_1x,
        &is_Force_1x_Mode,
        &is_Force_1x_Mode_Lane_0
    );

    result = 
        pm_Instance->ctray->get_Character_Column(
            pm_Instance->ctray->character_Level_Context,
            character
        );

    if (RIO_NO_DATA == result)
    /* generate COMPENSATION & IDLE sequences */
    {
        /* generate COMPENSATION sequence */
        if (
            !lp_Serial_Is_1x &&
            !is_Force_1x_Mode
        )
        /* true 4x link */
        {
            character->lane_Mask = RIO_PL_ALL_LANES_MASK;
            if (
                !pm_Instance->is_Comp_Seq_Gen_On ||
                RIO_NO_DATA == rio_PCS_PMA_PM_Generate_Comp_Seq(pm_Instance, &character->character_Type[0])
            )
            /* it's not a time for compensation */
            {
                /* generate IDLSs */
                rio_PCS_PMA_PM_Get_Next_Idle(pm_Instance, &character->character_Type[0]);
                character->character_Type[1] = character->character_Type[0];
                character->character_Type[2] = character->character_Type[0];
                character->character_Type[3] = character->character_Type[0];
            }
            else
            {
                character->character_Type[1] = character->character_Type[0];
                character->character_Type[2] = character->character_Type[0];
                character->character_Type[3] = character->character_Type[0];
            }
        }
        else
        /* 1x link */
        {
            character->lane_Mask = 1;
            if (
                !pm_Instance->is_Comp_Seq_Gen_On ||
                RIO_NO_DATA == rio_PCS_PMA_PM_Generate_Comp_Seq(pm_Instance, &character->character_Type[0])
            )
            /* it's not a time for compensation */
            {
                /* generate IDLEs */
                rio_PCS_PMA_PM_Get_Next_Idle(pm_Instance, &character->character_Type[0]);
            }
        }
    }
    else
    {
        /* for the IDLE sequence */
        pm_Instance->idle_Delayed = RIO_TRUE;

        /* for the COMPENSATION sequence */
        pm_Instance->idle_Non_A_Counter = PCS_PMA_PM__CHARACTER_A_COUNTER_BASE + 
            PCS_PMA_PM__CHARACTER_A_COUNTER_MASK;
        if (
            0 == pm_Instance->before_Comp_Seq_Counter &&
            pm_Instance->is_Comp_Seq_Gen_On &&
            RIO_TRUE != pm_Instance->is_Packet_Streaming_Broken
        )
        {
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_TX_DATA_IN_PROGRESS,
                &indicator_Value,
                &indicator_Parameter
            );
            if (RIO_TRUE == indicator_Value)
            {
                /* break streaming of packets in PL (it is resumed afterwards by PL) */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_TX_DATA_IN_PROGRESS,
                    RIO_FALSE, /* buf_status */
                    0 /* don't care */
                );
                pm_Instance->is_Packet_Streaming_Broken = RIO_FALSE;
            }
            else
                pm_Instance->is_Packet_Streaming_Broken = RIO_TRUE;
        }
    }

    /* decrease counters for STATUS & COMPENSATION */
    if (pm_Instance->before_Comp_Seq_Counter > 0)
        pm_Instance->before_Comp_Seq_Counter -= 1;
#ifdef DEBUG_A_GENERATION
    /* this is to trace decreasing of the counter */
    if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
    {
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_NOTE_MSG,
            "PM_INSTANCE->BEFORE_COMP_SEQ_COUNTER-- "
        );
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_NOTE_MSG,
            itoa(pm_Instance->before_Comp_Seq_Counter, s, 10)  /* decimal number */
        );
    }
#endif /* DEBUG_A_GENERATION */

    if (
        pm_Instance->is_Status_Sym_Gen_On &&
        (
            !pm_Instance->is_Init_FSM_On ||
            RIO_TRUE == pm_Instance->init_Status
        )
    )
    {
        if (
            !lp_Serial_Is_1x &&
            !is_Force_1x_Mode
        )
        /* true 4x link */
        {
            if (pm_Instance->before_Status_Symbol_Counter > 0)
                pm_Instance->before_Status_Symbol_Counter -= RIO_PL_SERIAL_NUMBER_OF_LANES;
        }
        else
        /* 1x link */
        {
            if (pm_Instance->before_Status_Symbol_Counter > 0)
                pm_Instance->before_Status_Symbol_Counter -= 1;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Put_Character_Column
 *
 * Description: notifies on reception of character
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Put_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle_Context;
    unsigned char i;
    RIO_BOOL_T    lp_Serial_Is_1x;
    RIO_BOOL_T    is_Force_1x_Mode;
    RIO_BOOL_T    is_Force_1x_Mode_Lane_0;
    RIO_UCHAR_T   pop_Lane_Mask;
    RIO_UCHAR_T   aligned_Lanes_Mask;
    unsigned long indicator_Value;
    unsigned int  indicator_Parameter;

    
    if (pm_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Put_Character_Column: is called while in reset\n");
#else
	if(pm_Instance->ctray)
		pm_Instance->ctray->rio_User_Msg(pm_Instance->ctray->user_Msg_Context,
				RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Put_Character_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    rio_PCS_PMA_PM_Get_Port_Control(
        pm_Instance,
        &lp_Serial_Is_1x,
        &is_Force_1x_Mode,
        &is_Force_1x_Mode_Lane_0
    );
    
    rio_PCS_PMA_PM_Check_Comp_Seq(
        pm_Instance,
        character,
        (!lp_Serial_Is_1x && !is_Force_1x_Mode)
    );

	if (
        !lp_Serial_Is_1x &&
        !is_Force_1x_Mode
    )
    /* true 4x link */
    {
        if (pm_Instance->is_Init_FSM_On)
        /* INIT FSM is turned ON */
        {
            /* check if this is just a call to drive the INIT FSM */
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_SUPPRESS_HOOK,
                &indicator_Value,
                &indicator_Parameter
            );
			
            if (RIO_TRUE == indicator_Value)
            {
                pm_Instance->ctray->rio_Get_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_IS_SYNC_LOST,
                    &indicator_Value,
                    &indicator_Parameter
                );
                if (RIO_TRUE == indicator_Value)
                /* synchronization is lost => restart the ALIGN FSM */
                    rio_PCS_PMA_PM_Exec_Align_FSM(
                        pm_Instance,
                        character->character_Type
                    );

                /* issue false column to drive the INIT FSM */
                return
                    pm_Instance->ctray->put_Character_Column(
                        pm_Instance->ctray->character_Level_Context,
                        character
                    );
            }

            if (
                RIO_PL_ALL_LANES_MASK == character->lane_Mask &&
                (
                    pm_Instance->align_State > PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_PART_2 ||
                    is_Align_Column(pm_Instance, character->character_Type)
                )
            )
                rio_PCS_PMA_PM_Exec_Align_FSM(
                    pm_Instance,
                    character->character_Type
                );
            else
                pm_Instance->is_Alignment_Lost = 0;

            if (pm_Instance->align_State <= PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_PART_2)
            {
                aligned_Lanes_Mask = 0;
                for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
                    if ((character->lane_Mask >> i) & 1)
                    {
                        if (RIO_A == character->character_Type[i])
                        {
                            aligned_Lanes_Mask |= 1 << i;
                        }
                    }
                pop_Lane_Mask = (unsigned char)((~aligned_Lanes_Mask) & character->lane_Mask);

                if (0 != pop_Lane_Mask)
                /* poping (removing) unnecessary non-A characters that have just been got */
                    pm_Instance->ctray->pop_Char_Buffer(
                        pm_Instance->ctray->character_Level_Context,
                        pop_Lane_Mask
                    );
                else
                if (RIO_PL_ALL_LANES_MASK == character->lane_Mask)
                {
                    /* poping (removing) unnecessary non-A characters that have just been got */
                    pm_Instance->ctray->pop_Char_Buffer(
                        pm_Instance->ctray->character_Level_Context,
                        character->lane_Mask
                    );

                    if (pm_Instance->is_Alignment_Lost)
                    {
                        rio_PCS_PMA_PM_Display_Msg(
                            pm_Instance,
                            RIO_PL_SERIAL_MSG_ERROR_MSG,
                            "rio_PCS_PMA_PM_Put_Character_Column: discrepancy: alignment_Lost flag is asserted inappropriately"
                        );
                        return RIO_ERROR;
                    }
                    
                    /* uploading the character */
/*
                    / * this is an /A/ column that is got before all lanes are synchronized * /
                    pm_Instance->ctray->put_Character_Column(
                        pm_Instance->ctray->character_Level_Context,
                        character
                    );
*/
                }

                if (pm_Instance->is_Alignment_Lost)
                {
                    pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_SUPPRESS_HOOK,
                        RIO_TRUE,
                        0 /* don't care */
                    );
                    /* issue false column to force the INIT FSM to reset */
                    character->character_Type[0] = RIO_R;
                    character->character_Type[1] = RIO_R;
                    character->character_Type[2] = RIO_R;
                    character->character_Type[3] = RIO_R;
                    character->lane_Mask = RIO_PL_ALL_LANES_MASK;
                    pm_Instance->ctray->put_Character_Column(
                        pm_Instance->ctray->character_Level_Context,
                        character
                    );
                    pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_SUPPRESS_HOOK,
                        RIO_FALSE,
                        0 /* don't care */
                    );
                    
                    /*
                     * command resetting character buffers
                     * this flag is also set inside the ALIGN FSM
                     * but it may be cleared by the INIT FSM invoked in the
                     * fictive call just above
                     */
                    pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_IS_ALIGNMENT_LOST,
                        RIO_TRUE,
                        0 /* don't care */
                    );
                }
            }
            else
            if (RIO_PL_ALL_LANES_MASK == character->lane_Mask)
            {
                /* poping (removing) unnecessary non-A characters that have just been got */
                pm_Instance->ctray->pop_Char_Buffer(
                    pm_Instance->ctray->character_Level_Context,
                    character->lane_Mask
                );
                /* uploading the character */
                pm_Instance->ctray->put_Character_Column(
                    pm_Instance->ctray->character_Level_Context,
                    character
                );
            }
        }
        else
        if (RIO_PL_ALL_LANES_MASK == character->lane_Mask)
        {
            /* poping (removing) unnecessary non-A characters that have just been got */
            pm_Instance->ctray->pop_Char_Buffer(
                pm_Instance->ctray->character_Level_Context,
                character->lane_Mask
            );
            /* uploading the character */
            pm_Instance->ctray->put_Character_Column(
                pm_Instance->ctray->character_Level_Context,
                character
            );
        }
    }
    else
    /* 1x link */
    {
        return
            pm_Instance->ctray->put_Character_Column(
                pm_Instance->ctray->character_Level_Context,
                character
            );
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Exec_Align_FSM
 *
 * Description: executes the PCS/PMA PM align FSM
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Exec_Align_FSM(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_CHARACTER_TYPE_T  character_Type[RIO_PL_SERIAL_NUMBER_OF_LANES]
)
{
    pm_Instance->is_Alignment_Lost = RIO_FALSE;
    while (1)
    {
        if (!are_All_Lanes_Sync(pm_Instance))
            PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED);

        if (PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED == pm_Instance->align_State)
        {
            if (pm_Instance->align_Counter > 0)
            {
                if (!pm_Instance->align_Status)
                    rio_PCS_PMA_PM_Display_Msg(
                        pm_Instance,
                        RIO_PL_SERIAL_MSG_NOTE_MSG,
                        "rio_PCS_PMA_PM_Exec_Align_FSM: ALIGNMENT process has been restarted after some /A/ columns had been found {Info #13}"
                    );
                pm_Instance->align_Counter = 0;
            }
            if (pm_Instance->align_Status)
            {
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Align_FSM: port has lost ALIGNMENT {Info #12}"
                );
                pm_Instance->align_Status = RIO_FALSE;
                pm_Instance->is_Alignment_Lost = RIO_TRUE;

                /*in case when lost aligment is occured during packet transmission
                it is necessary reset "rx packet ongoing" flag
                this is necessary to prevent "idle inside packet" errors
                mantis #94 implementation */
                pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_RX_IS_PD,
                        (int)RIO_FALSE,
                        0 /* don't care */
                    );
            }

            /* command resetting character buffers */
            pm_Instance->ctray->rio_Set_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_IS_ALIGNMENT_LOST,
                RIO_TRUE,
                0 /* don't care */
            );

            PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_PART_2);
            break;
        }

        if (PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_PART_2 == pm_Instance->align_State)
        {
            if (
                are_All_Lanes_Sync(pm_Instance) &&
                is_Align_Column(pm_Instance, character_Type)
            )
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_1);
            else
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED);
        }
        
        if (PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_1 == pm_Instance->align_State)
        {
            pm_Instance->align_Counter++;
            if (pm_Instance->align_Counter < RIO__SYNC_FSM__ALIGN_FSM_CONST_1)
            {
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_2);
                break;
            }
            else
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED);
        }

        if (PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_2 == pm_Instance->align_State)
        {
            if (is_Align_Error(pm_Instance, character_Type))
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED);
            else
            if (is_Align_Column(pm_Instance, character_Type))
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_1);
            else
                /* stay in the same state */
                break;
        }

        if (PCS_PMA_PM__ALIGN_FSM__ALIGNED == pm_Instance->align_State)
        {
            if (!pm_Instance->align_Status)
            {
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Align_FSM: port has got ALIGNMENT {Info #11}"
                );
                pm_Instance->align_Status = RIO_TRUE;
            }
            pm_Instance->misalign_Counter = 0;
            PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED_PART_2);
            break;
        }

        if (PCS_PMA_PM__ALIGN_FSM__ALIGNED_PART_2 == pm_Instance->align_State)
        {
            if (is_Align_Error(pm_Instance, character_Type))
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED_1);
            else
            {
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED);
            }
        }

        if (PCS_PMA_PM__ALIGN_FSM__ALIGNED_1 == pm_Instance->align_State)
        {
            pm_Instance->align_Counter = 0;
            pm_Instance->misalign_Counter++;
            if (pm_Instance->misalign_Counter < RIO__SYNC_FSM__ALIGN_FSM_CONST_2)
            {
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED_2);
                break;
            }
            else
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED);
        }

        if (PCS_PMA_PM__ALIGN_FSM__ALIGNED_2 == pm_Instance->align_State)
        {
            if (is_Align_Error(pm_Instance, character_Type))
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED_1);
            else
            if (is_Align_Column(pm_Instance, character_Type))
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED_3);
            else
                /* stay in the same state */
                break;
        }

        if (PCS_PMA_PM__ALIGN_FSM__ALIGNED_3 == pm_Instance->align_State)
        {
            pm_Instance->align_Counter++;
            if (pm_Instance->align_Counter < RIO__SYNC_FSM__ALIGN_FSM_CONST_3)
            {
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED_2);
                break;
            }
            else
                PCS_PMA_PM__ALIGN_NEXT_STATE(PCS_PMA_PM__ALIGN_FSM__ALIGNED);
        }

        if (pm_Instance->align_State > PCS_PMA_PM__ALIGN_FSM__ALIGNED_3)
        {
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Exec_Align_FSM: unknown state for ALIGN FSM"
            );
            return RIO_ERROR;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : are_All_Lanes_Sync
 *
 * Description: checks if all the lanes are syncronized
 *
 * Returns: bool
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T are_All_Lanes_Sync(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance
)
{
    unsigned char i;

    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        if (RIO_TRUE != pm_Instance->sync_Status[i])
            return RIO_FALSE;

    return RIO_TRUE;
}

/***************************************************************************
 * Function : is_Align_Column
 *
 * Description: checks if this is an ||A|| column
 *
 * Returns: bool
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T is_Align_Column(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,
    RIO_CHARACTER_TYPE_T    character_Type[RIO_PL_SERIAL_NUMBER_OF_LANES]
)
{
    unsigned char i;

    if (character_Type == NULL)
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "is_Align_Column: input parameter is a NULL pointer"
        );

    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        if (RIO_A != character_Type[i])
            return RIO_FALSE;

    return RIO_TRUE;
}

/***************************************************************************
 * Function : is_Align_Error
 *
 * Description: checks if this is a column with align error
 *
 * Returns: bool
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T is_Align_Error(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,
    RIO_CHARACTER_TYPE_T    character_Type[RIO_PL_SERIAL_NUMBER_OF_LANES]
)
{
    unsigned char i;
    unsigned char num_Of_A;

    if (character_Type == NULL)
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "is_Align_Error: input parameter is a NULL pointer"
        );

    num_Of_A = 0;
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        if (RIO_A == character_Type[i])
            num_Of_A++;

    if (
        0 == num_Of_A ||
        RIO_PL_SERIAL_NUMBER_OF_LANES == num_Of_A
    )
        return RIO_FALSE;
    else
        return RIO_TRUE;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Check_Comp_Seq
 *
 * Description: returns the next IDLE character
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
void rio_PCS_PMA_PM_Check_Comp_Seq(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character,       /* pointer to character data structure */
    RIO_BOOL_T is_4x_Mode
)
{
    unsigned int i;

	if (pm_Instance->parameters_Set->comp_Seq_Rate_Check == 0)
		return;

	if (is_4x_Mode)
	{
        if (character->lane_Mask != 15)
			return;

        if ((character->character_Type[0] == RIO_K) &&
            (character->character_Type[1] == RIO_K) &&
			(character->character_Type[2] == RIO_K) &&
			(character->character_Type[3] == RIO_K))
		{
            pm_Instance->comp_Seq_Check_Buf[9] = RIO_K;
		}
		else if ((character->character_Type[0] == RIO_R) &&
            (character->character_Type[1] == RIO_R) &&
			(character->character_Type[2] == RIO_R) &&
			(character->character_Type[3] == RIO_R))
		{
            pm_Instance->comp_Seq_Check_Buf[9] = RIO_R;
		}
	    else
		    pm_Instance->comp_Seq_Check_Buf[9] = RIO_I;
    }
	else
	{
        pm_Instance->comp_Seq_Check_Buf[9] = character->character_Type[0];
	}

    if ((pm_Instance->comp_Seq_Check_Buf[0] == RIO_K) && 
        (pm_Instance->comp_Seq_Check_Buf[1] == RIO_R) && 
        (pm_Instance->comp_Seq_Check_Buf[2] == RIO_R) && 
        (pm_Instance->comp_Seq_Check_Buf[3] == RIO_R))
        pm_Instance->comp_Seq_Check_Counter = 0;
    else
		pm_Instance->comp_Seq_Check_Counter++;

    for (i = 0; i < 9; i++)
        pm_Instance->comp_Seq_Check_Buf[i] = pm_Instance->comp_Seq_Check_Buf[i + 1];

    if (pm_Instance->comp_Seq_Check_Counter > 
		pm_Instance->parameters_Set->comp_Seq_Rate_Check)
	{
        char str[256];

        sprintf(str, "compensation sequence is not generated after %d codegroups",
            pm_Instance->parameters_Set->comp_Seq_Rate_Check);

        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            str
            );

        pm_Instance->comp_Seq_Check_Counter = 0;
	}

    return;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Generate_Comp_Seq
 *
 * Description: returns the next IDLE character
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Generate_Comp_Seq(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_CHARACTER_TYPE_T  *character_Type
)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Parameter;
#ifdef DEBUG_A_GENERATION
    char s[50];
#endif /* DEBUG_A_GENERATION */

    if (pm_Instance->comp_Seq_Counter > 0)
    {
		if (pm_Instance->before_Comp_Seq_Counter > 0)
        /* error: discrepancy in the compensation sequence counter */
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Generate_Comp_Seq: discrepancy in the compensation sequence counter"
            );

        pm_Instance->comp_Seq_Counter++;
        *character_Type = RIO_R;
        if (pm_Instance->comp_Seq_Size_Next == pm_Instance->comp_Seq_Counter )
        {
            unsigned long is_True_4x;
            unsigned int indicator_Parameter;

            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_IS_PORT_TRUE_4X,
                &is_True_4x,
                &indicator_Parameter
            );

            pm_Instance->is_Packet_Streaming_Broken = RIO_FALSE;
            pm_Instance->comp_Seq_Counter = 0;

            /* setup the size of the next comp sequence */
            if (pm_Instance->parameters_Set->comp_Seq_Size != 0)
            {
                double rand_Res;
                int expr1, expr2, expr3;
 
                rand_Res = rand() / (double)RAND_MAX;

                /*  Konstantin suggestion  with 7-29-04  */
                expr1 =  PCS_PMA_PM__COMPENSATION_SEQUENCE_SIZE - pm_Instance->parameters_Set->comp_Seq_Size;
                expr2 = 2 * pm_Instance->parameters_Set->comp_Seq_Size;
                expr3 = (int)( rand_Res * 2);
      
                if (expr3 == 0)
                    pm_Instance->comp_Seq_Size_Next = expr1;
                else
                    pm_Instance->comp_Seq_Size_Next = expr1 + expr2;


                /* DAA 7-27-04 ORG
                pm_Instance->comp_Seq_Size_Next = 
                PCS_PMA_PM__COMPENSATION_SEQUENCE_SIZE - pm_Instance->parameters_Set->comp_Seq_Size +
                2 * pm_Instance->parameters_Set->comp_Seq_Size * ((int)( rand_Res * 2));
                */
            }
            else
                pm_Instance->comp_Seq_Size_Next = PCS_PMA_PM__COMPENSATION_SEQUENCE_SIZE;

            pm_Instance->before_Comp_Seq_Counter =
                pm_Instance->parameters_Set->comp_Seq_Rate -
                RIO_PL_MAX_PACKET_SIZE -
                pm_Instance->comp_Seq_Size_Next -
                PCS_PMA_PM__COMPENSATION_DELTA;
            /* align to 4 */
            pm_Instance->before_Comp_Seq_Counter =
                ((unsigned long)pm_Instance->before_Comp_Seq_Counter / RIO_PL_SERIAL_NUMBER_OF_LANES) *
                RIO_PL_SERIAL_NUMBER_OF_LANES;




#ifdef DEBUG_ALL_GENERATION
            if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
                printf("(%i)ITERATION: %i:    A: %i, !C: %i, S: %i\n",
                    pm_Instance->instance_Number,
                    pm_Instance->debug_Iteration_Counter,
                    pm_Instance->idle_Non_A_Counter,
                    pm_Instance->before_Comp_Seq_Counter,
                    pm_Instance->before_Status_Symbol_Counter
                );
#endif /* DEBUG_ALL_GENERATION */
            /* checking overlapping with A */
            rio_PCS_PMA_PM_Correct_A_Against_Comp_Seq(pm_Instance);
#ifdef DEBUG_ALL_GENERATION
            if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
                printf("(%i)ITERATION: %i:    A: %i, !!C: %i, S: %i\n",
                    pm_Instance->instance_Number,
                    pm_Instance->debug_Iteration_Counter,
                    pm_Instance->idle_Non_A_Counter,
                    pm_Instance->before_Comp_Seq_Counter,
                    pm_Instance->before_Status_Symbol_Counter
                );
#endif /* DEBUG_ALL_GENERATION */
            /* checking overlapping with status symbol */
            rio_PCS_PMA_PM_Correct_A_Against_Status_Sym(pm_Instance, is_True_4x);
#ifdef DEBUG_ALL_GENERATION
            if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
                printf("(%i)ITERATION: %i:    A: %i, !!!C: %i, S: %i\n",
                    pm_Instance->instance_Number,
                    pm_Instance->debug_Iteration_Counter,
                    pm_Instance->idle_Non_A_Counter,
                    pm_Instance->before_Comp_Seq_Counter,
                    pm_Instance->before_Status_Symbol_Counter
                );
#endif /* DEBUG_ALL_GENERATION */
            /*
             * checking overlapping with A again
             * (it may be cause by the above correction of overlapping with status symbol)
             */
            rio_PCS_PMA_PM_Correct_A_Against_Comp_Seq(pm_Instance);
#ifdef DEBUG_ALL_GENERATION
            if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
                printf("(%i)ITERATION: %i:    A: %i, !!!!C: %i, S: %i\n",
                    pm_Instance->instance_Number,
                    pm_Instance->debug_Iteration_Counter,
                    pm_Instance->idle_Non_A_Counter,
                    pm_Instance->before_Comp_Seq_Counter,
                    pm_Instance->before_Status_Symbol_Counter
                );
#endif /* DEBUG_ALL_GENERATION */

		}
#ifdef DEBUG_A_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        {
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                "idle_Non_A_Counter-- comp_seq_1 before"
            );
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                itoa(pm_Instance->idle_Non_A_Counter, s, 10)  /* decimal number */
            );
        }
#endif /* DEBUG_A_GENERATION */
        /* this is for the uninterruption of the /A/ sequence */
        if (pm_Instance->idle_Non_A_Counter > 0)
            pm_Instance->idle_Non_A_Counter--;
        else
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Generate_Comp_Seq: discrepancy: overlapping of /A/ and compensation sequence: /A/ is delayed"
            );

#ifdef DEBUG_A_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                itoa(pm_Instance->idle_Non_A_Counter, s, 10)  /* decimal number */
            );
#endif /* DEBUG_A_GENERATION */

        return RIO_OK;
    }
    else
    if (0 == pm_Instance->before_Comp_Seq_Counter)
    /* it's time for the next compensation sequence */
    {
        unsigned long is_True_4x;

        pm_Instance->ctray->rio_Get_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_IS_PORT_TRUE_4X,
            &is_True_4x,
            &indicator_Parameter
        );
#ifdef DEBUG_ALL_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            printf("(%i)ITERATION: %i:    A: %i, 2!C: %i, S: %i,  PSB: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter,
                pm_Instance->is_Packet_Streaming_Broken
            );
#endif /* DEBUG_ALL_GENERATION */
        if (
            (
                is_True_4x &&
                pm_Instance->before_Status_Symbol_Counter < pm_Instance->comp_Seq_Size_Next * PCS_PMA_PM__STATUS_SYMBOL_SIZE
            ) ||
            (
                !is_True_4x &&
                pm_Instance->before_Status_Symbol_Counter < pm_Instance->comp_Seq_Size_Next
            )
        )
        /* Status symbol is overlapping with comp. seq. => delay the comp. seq. */
        {
#ifdef DEBUG_ALL_GENERATION
            if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
                printf("(%i)ITERATION: %i:    comp.seq. is delayed for status\n",
                    pm_Instance->instance_Number,
                    pm_Instance->debug_Iteration_Counter
                );
#endif /* DEBUG_ALL_GENERATION */
            pm_Instance->is_Comp_Seq_Delayed_For_Status = RIO_TRUE;
            return RIO_NO_DATA;
        }

        if (pm_Instance->idle_Non_A_Counter < pm_Instance->comp_Seq_Size_Next - 1)
        /* ||A|| is overlapping with comp. seq. => delay the comp. seq. */
        {
#ifdef DEBUG_ALL_GENERATION
            if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
                printf("(%i)ITERATION: %i:    comp.seq. is delayed for A\n",
                    pm_Instance->instance_Number,
                    pm_Instance->debug_Iteration_Counter
                );
#endif /* DEBUG_ALL_GENERATION */
            return RIO_NO_DATA;
        }

        if (
            RIO_TRUE != pm_Instance->is_Packet_Streaming_Broken &&
            pm_Instance->trx_Requests_Enabled
        )
        {
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_TX_DATA_IN_PROGRESS,
                &indicator_Value,
                &indicator_Parameter
            );
            if (RIO_TRUE == indicator_Value)
            {
#ifdef ENABLE_DISCREPANCY
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_PM_Generate_Comp_Seq: discrepancy: " \
                    "TX data is in progress while starting sending compensation sequence"
                );
#endif /*ENABLE_DISCREPANCY*/
                pm_Instance->is_Packet_Streaming_Broken = RIO_FALSE;
                return RIO_NO_DATA;
            }
            else
                pm_Instance->is_Packet_Streaming_Broken = RIO_TRUE;
        }
        else
            pm_Instance->is_Packet_Streaming_Broken = RIO_TRUE;

        pm_Instance->comp_Seq_Counter = 1;
        *character_Type = RIO_K;

#ifdef DEBUG_A_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        {
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                "idle_Non_A_Counter-- comp_seq_2 before"
            );
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                itoa(pm_Instance->idle_Non_A_Counter, s, 10)  /* decimal number */
            );
        }
#endif /* DEBUG_A_GENERATION */
        /* this is for the uninterruption of the /A/ sequence */
        if (pm_Instance->idle_Non_A_Counter > 0)
            pm_Instance->idle_Non_A_Counter--;
        else
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Generate_Comp_Seq: discrepancy: overlapping of /A/ and compensation sequence: /A/ is delayed"
            );

#ifdef DEBUG_A_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                itoa(pm_Instance->idle_Non_A_Counter, s, 10)  /* decimal number */
            );
#endif /* DEBUG_A_GENERATION */

        return RIO_OK;
    }

    return RIO_NO_DATA;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Next_Idle
 *
 * Description: returns the next IDLE character
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Get_Next_Idle(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_CHARACTER_TYPE_T  *character_Type
)
{
    RIO_UCHAR_T random_Value;
#ifdef DEBUG_A_GENERATION
    char s[50];
#endif /* DEBUG_A_GENERATION */

    /* getting next value for the random generator */
    random_Value = (RIO_UCHAR_T)(rand() / (float) RAND_MAX * 0xFF); /* max value is 0xFF - all bits of a byte are asserted */

#ifdef DEBUG_ELIMINATE_RAND
    /* !!! FOR DEBUG */
    random_Value = 1;
#endif /* DEBUG_ELIMINATE_RAND */

    if (pm_Instance->idle_Delayed)
    {
        pm_Instance->idle_Delayed = RIO_FALSE;
        pm_Instance->idle_Non_A_Counter = 
            PCS_PMA_PM__CHARACTER_A_COUNTER_BASE + 
            (RIO_UCHAR_T)(random_Value & PCS_PMA_PM__CHARACTER_A_COUNTER_MASK);
#ifdef DEBUG_ELIMINATE_RAND
        /* !!! FOR DEBUG */
        pm_Instance->idle_Non_A_Counter = PCS_PMA_PM__CHARACTER_A_COUNTER_BASE;
#endif /* DEBUG_ELIMINATE_RAND */

#ifdef DEBUG_ALL_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            printf("(%i)ITERATION: %i:    1!A: %i, C: %i, S: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter
            );
#endif /* DEBUG_ALL_GENERATION */
        /* check overlapping with comp.seq. */
        if (
            pm_Instance->idle_Non_A_Counter >= pm_Instance->before_Comp_Seq_Counter &&
            pm_Instance->idle_Non_A_Counter <= pm_Instance->before_Comp_Seq_Counter +
                pm_Instance->comp_Seq_Size_Next - 1
        )
        {
            if (pm_Instance->before_Comp_Seq_Counter - 1 >= PCS_PMA_PM__CHARACTER_A_COUNTER_BASE)
                pm_Instance->idle_Non_A_Counter = pm_Instance->before_Comp_Seq_Counter - 1;
            else
                pm_Instance->idle_Non_A_Counter += pm_Instance->comp_Seq_Size_Next;
        }
        pm_Instance->idle_Non_A_Counter--;
#ifdef DEBUG_A_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        {
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                "idle_Non_A_Counter-- first time"
            );
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_NOTE_MSG,
                itoa(pm_Instance->idle_Non_A_Counter, s, 10)  /* decimal number */
            );
        }
#endif /* DEBUG_A_GENERATION */
        *character_Type = RIO_K;

#ifdef DEBUG_ALL_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            printf("(%i)ITERATION: %i:    1!!A: %i, C: %i, S: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter
            );
#endif /* DEBUG_ALL_GENERATION */
#ifdef DEBUG_ELIMINATE_RAND
/*        pm_Instance->idle_Non_A_Counter = 32; */
#endif /* DEBUG_ELIMINATE_RAND */
#ifdef DEBUG_A_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        {
            printf("(%i)ITERATION: %i:    alignment set after not-IDLEs: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter
            );
            printf("(%i)ITERATION: %i:    before_Comp_Seq_Counter: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->before_Comp_Seq_Counter
            );
        }
#endif /* DEBUG_A_GENERATION */
        return RIO_OK;
    }

    if (pm_Instance->idle_Non_A_Counter > 0)
        pm_Instance->idle_Non_A_Counter--;
#ifdef DEBUG_A_GENERATION
    if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
    {
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_NOTE_MSG,
            "idle_Non_A_Counter--"
        );
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_NOTE_MSG,
            itoa(pm_Instance->idle_Non_A_Counter, s, 10)  /* decimal number */
        );
    }
#endif /* DEBUG_A_GENERATION */

    if (0 == pm_Instance->idle_Non_A_Counter)
    /* set new counter value in this case */
    {
        *character_Type = RIO_A;
        pm_Instance->idle_Non_A_Counter = 
            PCS_PMA_PM__CHARACTER_A_COUNTER_BASE + 
            (random_Value & PCS_PMA_PM__CHARACTER_A_COUNTER_MASK);
#ifdef DEBUG_ELIMINATE_RAND
        /* !!! FOR DEBUG */
        pm_Instance->idle_Non_A_Counter = PCS_PMA_PM__CHARACTER_A_COUNTER_BASE;
#endif /* DEBUG_ELIMINATE_RAND */

#ifdef DEBUG_ALL_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            printf("(%i)ITERATION: %i:    2!A: %i, C: %i, S: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter
            );
#endif /* DEBUG_ALL_GENERATION */
        /* check overlapping with comp.seq. */
        if (
            pm_Instance->idle_Non_A_Counter >= pm_Instance->before_Comp_Seq_Counter &&
            pm_Instance->idle_Non_A_Counter <= pm_Instance->before_Comp_Seq_Counter +
                pm_Instance->comp_Seq_Size_Next - 1
        )
        {
            if (pm_Instance->before_Comp_Seq_Counter - 1 >= PCS_PMA_PM__CHARACTER_A_COUNTER_BASE)
                pm_Instance->idle_Non_A_Counter = pm_Instance->before_Comp_Seq_Counter - 1;
            else
                pm_Instance->idle_Non_A_Counter += pm_Instance->comp_Seq_Size_Next;
        }
#ifdef DEBUG_ALL_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            printf("(%i)ITERATION: %i:    2!!A: %i, C: %i, S: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter
            );
#endif /* DEBUG_ALL_GENERATION */
#ifdef DEBUG_ELIMINATE_RAND
/*        pm_Instance->idle_Non_A_Counter = 33; */
#endif /* DEBUG_ELIMINATE_RAND */
#ifdef DEBUG_A_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        {
            printf("(%i)ITERATION: %i:    alignment set after IDLEs: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter
            ); 
            printf("(%i)ITERATION: %i:    before_Comp_Seq_Counter: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->before_Comp_Seq_Counter
            );
        }
#endif /* DEBUG_A_GENERATION */
    }
    else
    {
        *character_Type = (random_Value & 1) ? RIO_K : RIO_R;
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Correct_A_Against_Comp_Seq
 *
 * Description: correctes generated A against the compensation sequence
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Correct_A_Against_Comp_Seq(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance
)
{
    /* checking overlapping with A */
    if (
        pm_Instance->idle_Non_A_Counter >= pm_Instance->before_Comp_Seq_Counter &&
        pm_Instance->idle_Non_A_Counter <= pm_Instance->before_Comp_Seq_Counter +
            pm_Instance->comp_Seq_Size_Next - 1
    )
    {
        /* this time a comp.seq. counter shal be adjusted (decreased) */
        if (pm_Instance->idle_Non_A_Counter >= pm_Instance->comp_Seq_Size_Next)
        {
            pm_Instance->before_Comp_Seq_Counter =
                pm_Instance->idle_Non_A_Counter - pm_Instance->comp_Seq_Size_Next;
        }
        else
        {
            pm_Instance->before_Comp_Seq_Counter = 0;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Correct_A_Against_Status_Sym
 *
 * Description: correctes generated A against the status symbol
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Correct_A_Against_Status_Sym(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance,
    RIO_BOOL_T            is_True_4x
)
{
    if (pm_Instance->is_Status_Sym_Gen_On)
    /* checking overlapping with status symbol */
    {
        if (
            is_True_4x &&
            (unsigned int)(pm_Instance->before_Status_Symbol_Counter / PCS_PMA_PM__STATUS_SYMBOL_SIZE) >=
                pm_Instance->before_Comp_Seq_Counter &&
            (unsigned int)(pm_Instance->before_Status_Symbol_Counter / PCS_PMA_PM__STATUS_SYMBOL_SIZE) <=
                pm_Instance->before_Comp_Seq_Counter + pm_Instance->comp_Seq_Size_Next - 1
        )
        {
            if (
                (unsigned int)(pm_Instance->before_Status_Symbol_Counter / PCS_PMA_PM__STATUS_SYMBOL_SIZE) >=
                    pm_Instance->comp_Seq_Size_Next
            )
            {
                pm_Instance->before_Comp_Seq_Counter =
                    pm_Instance->before_Status_Symbol_Counter * PCS_PMA_PM__STATUS_SYMBOL_SIZE -
                    pm_Instance->comp_Seq_Size_Next;
            }
            else
            {
                pm_Instance->before_Comp_Seq_Counter = 0;
            }
        }
        else
        if (
            !is_True_4x &&
            pm_Instance->before_Status_Symbol_Counter + PCS_PMA_PM__STATUS_SYMBOL_SIZE - 1 >= 
                pm_Instance->before_Comp_Seq_Counter &&
            pm_Instance->before_Status_Symbol_Counter <= pm_Instance->before_Comp_Seq_Counter +
                pm_Instance->comp_Seq_Size_Next - 1
        )
        {
            if (pm_Instance->before_Status_Symbol_Counter >= pm_Instance->comp_Seq_Size_Next)
            {
                pm_Instance->before_Comp_Seq_Counter =
                    pm_Instance->before_Status_Symbol_Counter - pm_Instance->comp_Seq_Size_Next;
            }
            else
            {
                pm_Instance->before_Comp_Seq_Counter = 0;
            }
        }
    }

    return RIO_OK;
}

/*****************************************************************************/
