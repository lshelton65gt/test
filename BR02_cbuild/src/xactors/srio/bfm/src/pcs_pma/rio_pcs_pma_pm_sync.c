/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_pm_sync.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include <string.h>

#include "rio_pcs_pma_pm_sync.h"
#include "rio_pcs_pma_pm_align.h"
#include "rio_pcs_pma_pm_ds.h"
#include "rio_pcs_pma_pm.h"

#include "rio_gran_type.h"

/* FOR DEBUG */
/*#define CG_RECEIVED_DEBUG*/

/* FOR DEBUG */
#ifdef DEBUG_ALL_GENERATION
#define DEBUG_A_GENERATION
#define _CUSTOM_DEBUG
#endif /* DEBUG_ALL_GENERATION */



/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Codegroup_Column
 *
 * Description: requests a next codegroup for transmission through the link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Get_Codegroup_Column(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle_Context;
    RIO_BOOL_T    lp_Serial_Is_1x;
    RIO_BOOL_T    is_Force_1x_Mode;
    RIO_BOOL_T    is_Force_1x_Mode_Lane_0;
    
    if (pm_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Get_Codegroup_Column: is called while in reset\n");
#else
	if(pm_Instance->ctray)
        pm_Instance->ctray->rio_User_Msg(
                pm_Instance->ctray->user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Get_Codegroup_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    rio_PCS_PMA_PM_Get_Port_Control(
        pm_Instance,
        &lp_Serial_Is_1x,
        &is_Force_1x_Mode,
        &is_Force_1x_Mode_Lane_0
    );

    if (
        pm_Instance->silence_Timer > 0 &&
        pm_Instance->is_Init_FSM_On
    )
    /* silence period */
    {
        pm_Instance->silence_Timer--;
        if (0 == pm_Instance->silence_Timer)
            pm_Instance->init_State = PCS_PMA_PM__INIT_FSM__SILENT_PART_2;

        codegroup->code_Group_Data[0] = 0;
        codegroup->code_Group_Data[1] = 0;
        codegroup->code_Group_Data[2] = 0;
        codegroup->code_Group_Data[3] = 0;
        codegroup->is_Running_Disparity_Positive[0] = RIO_FALSE;
        codegroup->is_Running_Disparity_Positive[1] = RIO_FALSE;
        codegroup->is_Running_Disparity_Positive[2] = RIO_FALSE;
        codegroup->is_Running_Disparity_Positive[3] = RIO_FALSE;

        /* adjust lane_Mask */
        if (lp_Serial_Is_1x)
        /* true 1x link */
            codegroup->lane_Mask = 1;
        else
        if (!lp_Serial_Is_1x &&
            is_Force_1x_Mode
        )
        /* 1x4x link in 1x mode */
            codegroup->lane_Mask = RIO_LANE_MASK_LANE0_LANE2;
        else
        /* true 4x link */
            codegroup->lane_Mask = RIO_PL_ALL_LANES_MASK;

        return RIO_OK;
    }

    pm_Instance->ctray->get_Codegroup_Column(
        pm_Instance->ctray->codegroup_Level_Context,
        codegroup
    );

#ifdef _CUSTOM_DEBUG
    pm_Instance->debug_Iteration_Counter++;
#endif /* _CUSTOM_DEBUG */
#ifdef CG_TO_SEND_DEBUG
    if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        printf("(%i)ITERATION: %i:    cg column to send: %X, %X, %X, %X\n",
            pm_Instance->instance_Number,
            pm_Instance->debug_Iteration_Counter,
            codegroup->code_Group_Data[0],
            codegroup->code_Group_Data[1],
            codegroup->code_Group_Data[2],
            codegroup->code_Group_Data[3]
        );
#endif /* CG_TO_SEND_DEBUG */
#ifdef DEBUG_ALL_GENERATION
    if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        printf("(%i)ITERATION: %i:   cur: A: %i, C: %i, S: %i,  PSB: %i\n",
            pm_Instance->instance_Number,
            pm_Instance->debug_Iteration_Counter,
            pm_Instance->idle_Non_A_Counter,
            pm_Instance->before_Comp_Seq_Counter,
            pm_Instance->before_Status_Symbol_Counter,
            pm_Instance->is_Packet_Streaming_Broken
        );
#endif /* DEBUG_ALL_GENERATION */

    if (lp_Serial_Is_1x)
    /* true 1x link */
    {
        codegroup->lane_Mask = 1;
        codegroup->code_Group_Data[1] = 0;
        codegroup->is_Running_Disparity_Positive[1] = 0;
        codegroup->code_Group_Data[2] = 0;
        codegroup->is_Running_Disparity_Positive[2] = 0;
        codegroup->code_Group_Data[3] = 0;
        codegroup->is_Running_Disparity_Positive[3] = 0;
    }
    else
    if (is_Force_1x_Mode)
    /* 1x4x link in 1x mode */
    {
        codegroup->lane_Mask = RIO_LANE_MASK_LANE0_LANE2; /* transmission through lane0 & lane 2 */
        codegroup->code_Group_Data[1] = 0;
        codegroup->is_Running_Disparity_Positive[1] = 0;
        codegroup->code_Group_Data[2] = codegroup->code_Group_Data[0];
        codegroup->is_Running_Disparity_Positive[2] = codegroup->is_Running_Disparity_Positive[0];
        codegroup->code_Group_Data[3] = 0;
        codegroup->is_Running_Disparity_Positive[3] = 0;
    }
    else
    if (pm_Instance->is_Init_FSM_On)
    /* true 4x mode && FSMs are turned ON */
    {
        /* 
         * apply lanes' output enables
         */
        if (!pm_Instance->is_Lane_OE[1])
        /* lane 1 OE is OFF */
        {
            codegroup->code_Group_Data[1] = 0;
            codegroup->is_Running_Disparity_Positive[1] = 0;
            codegroup->lane_Mask &= ~(1 << 1);
        }

        if (!pm_Instance->is_Lane_OE[3])
        /* lane 3 OE is OFF */
        {
            codegroup->code_Group_Data[3] = 0;
            codegroup->is_Running_Disparity_Positive[3] = 0;
            codegroup->lane_Mask &= ~(1 << 3);
        }
    }

    if (0 == codegroup->lane_Mask)
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Get_Codegroup: error: lane_Mask = 0 at transmission"
        );

#ifdef DEBUG_A_GENERATION
    if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
    {
        if (
            pm_Instance->before_Comp_Seq_Counter <= 5 ||  /* critical area */
            pm_Instance->before_Comp_Seq_Counter >= 4700  /* => needs close attention */
        )
        {
            char s[100];

            sprintf(s, "CG (mask = %X): %i, %i, %i, %i \n",
                codegroup->lane_Mask,
                codegroup->code_Group_Data[0],
                codegroup->code_Group_Data[1],
                codegroup->code_Group_Data[2],
                codegroup->code_Group_Data[3]
            );
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                s
            );
        }
    }
#endif /* DEBUG_A_GENERATION */

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Put_Codegroup
 *
 * Description: notifies on reception of codegroup on a link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Put_Codegroup(
    RIO_CONTEXT_T                    handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle_Context;
    unsigned char i;
    RIO_BOOL_T    lp_Serial_Is_1x;
    RIO_BOOL_T    is_Force_1x_Mode;
    RIO_BOOL_T    is_Force_1x_Mode_Lane_0;
    RIO_UCHAR_T old_Lane_Mask;
    
    if (pm_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Put_Codegroup: is called while in reset\n");
#else
	if(pm_Instance->ctray)
        pm_Instance->ctray->rio_User_Msg(
                pm_Instance->ctray->user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Put_Codegroup: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

#ifdef _CUSTOM_DEBUG
    pm_Instance->debug_Iteration_Counter++;
#endif /* _CUSTOM_DEBUG */
#ifdef CG_RECEIVED_DEBUG
    if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        printf("(%i)ITERATION: %i:    cg column received: %X, %X, %X, %X\n",
            pm_Instance->instance_Number,
            pm_Instance->debug_Iteration_Counter,
            codegroup->code_Group_Data[0],
            codegroup->code_Group_Data[1],
            codegroup->code_Group_Data[2],
            codegroup->code_Group_Data[3]
        );
#endif /* CG_RECEIVED_DEBUG */
    
    rio_PCS_PMA_PM_Get_Port_Control(
        pm_Instance,
        &lp_Serial_Is_1x,
        &is_Force_1x_Mode,
        &is_Force_1x_Mode_Lane_0
    );

    if (lp_Serial_Is_1x)
    /* true 1x link */
    {
        if (1 != (codegroup->lane_Mask & 1))
        {
            /* lane 0 is not specified for 1x link */
            return
                pm_Instance->ctray->pop_Codegroup_Buffer(
                    pm_Instance->ctray->codegroup_Level_Context,
                    /* lane_ID - is got as log2(lane_Mask) */
                    RIO_Log2_Values[codegroup->lane_Mask]
                );
        }
        else
        if (1 != codegroup->lane_Mask)
        {
            /* error: odd lanes are used in lane_Mask */
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Put_Codegroup: error: odd lanes are used in lane_Mask"
            );
            codegroup->lane_Mask = 1;
        }

        if (!pm_Instance->is_Init_FSM_On)
        /* INIT FSM is turned OFF */
        {
            pm_Instance->ctray->put_Codegroup(
                pm_Instance->ctray->codegroup_Level_Context,
                codegroup
            );
            return
                pm_Instance->ctray->pop_Codegroup_Buffer(
                    pm_Instance->ctray->codegroup_Level_Context,
                    0 /* lane_ID */
                );
        }

        rio_PCS_PMA_PM_Exec_Sync_FSM(
            pm_Instance,
            0 /* lane_ID */,
            codegroup->code_Group_Data[0]
        );
        
        if (pm_Instance->sync_State[0] >= PCS_PMA_PM__SYNC_FSM__NO_SYNC_1)
            pm_Instance->ctray->put_Codegroup(
                pm_Instance->ctray->codegroup_Level_Context,
                codegroup
            );
    }
    else
    if (!lp_Serial_Is_1x &&
        is_Force_1x_Mode
    )
    /* 4x link in 1x mode */
    {
        if (is_Force_1x_Mode_Lane_0)
        {
            if (1 != (codegroup->lane_Mask & 1)) /*  for lane 0 (1 << 0 == 1) */
            {
                /* lane 0 is not specified for 1x link */
                return
                    pm_Instance->ctray->pop_Codegroup_Buffer(
                        pm_Instance->ctray->codegroup_Level_Context,
                        /* lane_ID - is got as log2(lane_Mask) */
                        RIO_Log2_Values[codegroup->lane_Mask]
                    );
            }
            else
            if (1 != codegroup->lane_Mask)
            {
                /* error: odd lanes are used in lane_Mask */
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_PM_Put_Codegroup: error: odd lanes are used in lane_Mask"
                );
                codegroup->lane_Mask = 1;
            }
            old_Lane_Mask = 1;
        }
        else
        /* !is_Force_1x_Mode_Lane_0 */
        {
            if (RIO_LANE_MASK_LANE2 != (codegroup->lane_Mask & RIO_LANE_MASK_LANE2)) /*  for lane 2 (1 << 2 == 4) */
            {
                /* lane 2 is not specified for 1x link */
                return
                    pm_Instance->ctray->pop_Codegroup_Buffer(
                        pm_Instance->ctray->codegroup_Level_Context,
                        /* lane_ID - is got as log2(lane_Mask) */
                        RIO_Log2_Values[codegroup->lane_Mask]
                    );
            }
            else
            if (RIO_LANE_MASK_LANE2 != codegroup->lane_Mask)
            {
                /* error: odd lanes are used in lane_Mask */
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_PM_Put_Codegroup: error: odd lanes are used in lane_Mask"
                );
                codegroup->lane_Mask = RIO_LANE_MASK_LANE2;
            }

            /* further on inside the PM the work is been done on the lane 0 */
            codegroup->code_Group_Data[0] = codegroup->code_Group_Data[2];
            codegroup->is_Running_Disparity_Positive[0] = codegroup->is_Running_Disparity_Positive[2];
            codegroup->lane_Mask = 1;
            old_Lane_Mask = RIO_LANE_MASK_LANE2;
        }

        if (!pm_Instance->is_Init_FSM_On)
        /* INIT FSM is turned OFF */
        {
            pm_Instance->ctray->put_Codegroup(
                pm_Instance->ctray->codegroup_Level_Context,
                codegroup
            );
            return
                pm_Instance->ctray->pop_Codegroup_Buffer(
                    pm_Instance->ctray->codegroup_Level_Context,
                    RIO_Log2_Values[old_Lane_Mask] /* lane_ID */
                );
        }

        rio_PCS_PMA_PM_Exec_Sync_FSM(
            pm_Instance,
            RIO_Log2_Values[old_Lane_Mask] /* lane_ID */,
            codegroup->code_Group_Data[0]
        );
        if (pm_Instance->sync_State[RIO_Log2_Values[old_Lane_Mask]] >= PCS_PMA_PM__SYNC_FSM__NO_SYNC_1)
            pm_Instance->ctray->put_Codegroup(
                pm_Instance->ctray->codegroup_Level_Context,
                codegroup
            );
    }
    else
    /* true 4x link */
    {
        for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
            if ((codegroup->lane_Mask >> i) & 1)
            {
                if (!pm_Instance->is_Init_FSM_On)
                /* INIT FSM is turned OFF */
                {
                    pm_Instance->ctray->put_Codegroup(
                        pm_Instance->ctray->codegroup_Level_Context,
                        codegroup
                    );
                    pm_Instance->ctray->pop_Codegroup_Buffer(
                        pm_Instance->ctray->codegroup_Level_Context,
                        i /* lane_ID */
                    );
                }
                else
                {
                    if (
                        (
                            PCS_PMA_PM__INIT_FSM__1X_MODE_LANE2 == pm_Instance->init_State &&
                            RIO_TRUE == pm_Instance->init_Status &&
                            2 != i  /* NOT lane 2 */
                        ) ||
                        (
                            PCS_PMA_PM__INIT_FSM__1X_MODE_LANE0 == pm_Instance->init_State &&
                            RIO_TRUE == pm_Instance->init_Status &&
                            0 != i  /* NOT lane 0 */
                        )
                    )
                    /* just shifted to the 1x mode by DISCOVERY timer outage */
                        break;

                    rio_PCS_PMA_PM_Exec_Sync_FSM(
                        pm_Instance,
                        i,
                        codegroup->code_Group_Data[i]
                    );
                    if (
                        (
                            PCS_PMA_PM__INIT_FSM__1X_MODE_LANE2 == pm_Instance->init_State &&
                            RIO_TRUE == pm_Instance->init_Status &&
                            2 != i  /* NOT lane 2 */
                        ) ||
                        (
                            PCS_PMA_PM__INIT_FSM__1X_MODE_LANE0 == pm_Instance->init_State &&
                            RIO_TRUE == pm_Instance->init_Status &&
                            0 != i  /* NOT lane 0 */
                        )
                    )
                    /* just shifted to the 1x mode by DISCOVERY timer outage */
                        break;

                    if (pm_Instance->sync_State[i] >= PCS_PMA_PM__SYNC_FSM__NO_SYNC_1)
                        pm_Instance->ctray->put_Codegroup(
                            pm_Instance->ctray->codegroup_Level_Context,
                            codegroup
                        );
                }
            }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Exec_Sync_FSM
 *
 * Description: executes the PCS/PMA PM sync FSM                
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Exec_Sync_FSM(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance,
    unsigned char        lane_ID,
    unsigned int         codegroup
)
{
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;
    unsigned long is_True_4x;
    RIO_UCHAR_T i;
    RIO_BOOL_T urgent_Fictive_Call_Enable = RIO_FALSE;
    RIO_BOOL_T fictive_Call_Blocked = RIO_FALSE;

    /* was previous codegroup valid for this lane? */
    pm_Instance->ctray->rio_Get_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_IS_PORT_TRUE_4X,
        &is_True_4x,
        &indicator_Parameter
    );

    /* was previous codegroup valid for this lane? */
    pm_Instance->ctray->rio_Get_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_IS_CHARACTER_INVALID,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    /* INVALID character on one of lanes */
    {
        indicator_Value = 0;
        if (is_True_4x)
        {
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_LANE_WITH_INVALID_CHARACTER,
                &indicator_Value,
                &indicator_Parameter
            );
        }
        else
        /* !is_True_4x */
        {
            pm_Instance->ctray->rio_Set_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_IS_CHARACTER_INVALID,
                RIO_FALSE,
                0 /* don't care */
            );
            pm_Instance->ctray->rio_Set_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_LANE_WITH_INVALID_CHARACTER,
                0,
                0 /* don't care */
            );
        }

        if (
            !is_True_4x ||
            (indicator_Value & (1 << lane_ID))
        )
        /* error on this particular lane */
        {
            if (PCS_PMA_PM__SYNC_FSM__SYNC_PART_2 == pm_Instance->sync_State[lane_ID])
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC_1, lane_ID);
            else
            if (PCS_PMA_PM__SYNC_FSM__SYNC_2_3 == pm_Instance->sync_State[lane_ID])
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC_1, lane_ID);
            else
            if (PCS_PMA_PM__SYNC_FSM__NO_SYNC_2 == pm_Instance->sync_State[lane_ID])
            {
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__NO_SYNC, lane_ID);

                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );
            }

            if (is_True_4x)
            {
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_LANE_WITH_INVALID_CHARACTER,
                    indicator_Value & (~(1 << lane_ID) & RIO_PL_ALL_LANES_MASK),
                    0 /* don't care */
                );
                if (0 == (indicator_Value & (~(1 << lane_ID) & RIO_PL_ALL_LANES_MASK)))
                    pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_IS_CHARACTER_INVALID,
                        RIO_FALSE,
                        0 /* don't care */
                    );
            }
        }
    }

    while (1)
    {
        if (PCS_PMA_PM__SYNC_FSM__NO_SYNC == pm_Instance->sync_State[lane_ID])
        {
            /*
             * redefine RIO_LANE_DRIVING_INIT_FSM
             */
            if (
                is_True_4x &&
                lane_ID == pm_Instance->lane_Driving_Init_FSM
            )
            {
                RIO_UCHAR_T new_Lane_Driving_Init_FSM;

                new_Lane_Driving_Init_FSM = 0;
                for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
                {
                    if (i == lane_ID)
                        continue;
                    if (pm_Instance->sync_State[i] >= PCS_PMA_PM__SYNC_FSM__NO_SYNC_1)
                    {
                        new_Lane_Driving_Init_FSM = i;
                        break;
                    }
                }
                if (
                    lane_ID == pm_Instance->lane_Driving_Init_FSM &&
                    new_Lane_Driving_Init_FSM < lane_ID
                )
                /* this is a case when otherwise fictive call is not assigned to any of the lanes on this clock */
                    urgent_Fictive_Call_Enable = RIO_TRUE;
                
                pm_Instance->lane_Driving_Init_FSM = new_Lane_Driving_Init_FSM;
            }

            if (pm_Instance->comma_Counter[lane_ID] > 0)
            {
                if (!pm_Instance->sync_Status[lane_ID])
                {
                    char *out_Message;
                    char *in_Message = "rio_PCS_PMA_PM_Exec_Sync_FSM: SYNCHRONIZATION process on lane %i " \
                        "has been restarted after some commas had been found {Info #03}";

                    out_Message = (char *)malloc(strlen(in_Message) + 20 /* length of lane_ID */);
                    if (out_Message == NULL)
                    {
                        rio_PCS_PMA_PM_Display_Msg(
                            pm_Instance,
                            RIO_PL_SERIAL_MSG_ERROR_MSG,
                            "rio_PCS_PMA_PM_Exec_Sync_FSM: cannot allocate memory"
                        );
                    }
                    else
                    {
                        sprintf(out_Message, in_Message, lane_ID);
                        rio_PCS_PMA_PM_Display_Msg(
                            pm_Instance,
                            RIO_PL_SERIAL_MSG_NOTE_MSG,
                            out_Message
                        );
                        free(out_Message);
                    }
                }

                pm_Instance->comma_Counter[lane_ID] = 0;
            }

            if (pm_Instance->sync_Status[lane_ID])
            {
                char *out_Message;
                char *in_Message = "rio_PCS_PMA_PM_Exec_Sync_FSM: port has lost SYNCHRONIZATION on lane %i {Info #02}";

                out_Message = (char *)malloc(strlen(in_Message) + 20 /* length of lane_ID */);
                if (out_Message == NULL)
                {
                    rio_PCS_PMA_PM_Display_Msg(
                        pm_Instance,
                        RIO_PL_SERIAL_MSG_ERROR_MSG,
                        "rio_PCS_PMA_PM_Exec_Sync_FSM: cannot allocate memory"
                    );
                }
                else
                {
                    sprintf(out_Message, in_Message, lane_ID);
                    rio_PCS_PMA_PM_Display_Msg(
                        pm_Instance,
                        RIO_PL_SERIAL_MSG_NOTE_MSG,
                        out_Message
                    );
                    free(out_Message);
                }

                pm_Instance->ctray->rio_Get_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_DECODE_ANY_RUN_DISP,
                    &indicator_Value,
                    &indicator_Parameter
                );
                if (pm_Instance->is_Init_FSM_On)
                    pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_DECODE_ANY_RUN_DISP,
                        indicator_Value & ~(1 << lane_ID),
                        0 /* don't care */
                    );
                else
                /* 
                 * if Init FSM is OFF it is always needed to allow
                 * any RunningDisparity because it cannot shift the 10 bit
                 * window, i.e. ossibly cannot acquire correct RunningDisparity
                 * again or at least will do it longer which is not desirable
                 */
                    pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_DECODE_ANY_RUN_DISP,
                        indicator_Value | (1 << lane_ID),
                        0 /* don't care */
                    );

                if (!is_True_4x)
                    urgent_Fictive_Call_Enable = RIO_TRUE;

                pm_Instance->sync_Status[lane_ID] = RIO_FALSE;
            }
            
            PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__NO_SYNC_PART_2, lane_ID);
        }

        if (PCS_PMA_PM__SYNC_FSM__NO_SYNC_PART_2 == pm_Instance->sync_State[lane_ID])
        {
            if (is_Comma(pm_Instance, codegroup))
            {
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__NO_SYNC_1, lane_ID);
                if (0 == pm_Instance->comma_Counter[lane_ID])
                {
                    if (is_Pos_Comma(pm_Instance, codegroup))
                    /* handling a case when the very first comma on a lane has a positive running disparity */
                        pm_Instance->ctray->rio_Set_State_Flag(
                            pm_Instance->ctray->rio_Regs_Context,
                            pm_Instance->mum_Instanse,
                            RIO_INITIAL_RUN_DISP,
                            PCS_PMA__RUN_DISP_POS,
                            0 /* don't care */
                        );
                    else
                        pm_Instance->ctray->rio_Set_State_Flag(
                            pm_Instance->ctray->rio_Regs_Context,
                            pm_Instance->mum_Instanse,
                            RIO_INITIAL_RUN_DISP,
                            PCS_PMA__RUN_DISP_NEG,
                            0 /* don't care */
                        );
                }

                /*
                 * redefine RIO_LANE_DRIVING_INIT_FSM
                 */
                if (
                    is_True_4x &&
                    0 != lane_ID &&
                    0 == pm_Instance->lane_Driving_Init_FSM &&
                    pm_Instance->sync_State[0] < PCS_PMA_PM__SYNC_FSM__NO_SYNC_1
                )
                {
                    pm_Instance->lane_Driving_Init_FSM = lane_ID;
                    if (!urgent_Fictive_Call_Enable)
                        fictive_Call_Blocked = RIO_TRUE;
                }
            }
            else
            {
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__NO_SYNC, lane_ID);
                break;
            }
        }
        
        if (PCS_PMA_PM__SYNC_FSM__NO_SYNC_1 == pm_Instance->sync_State[lane_ID])
        {
            pm_Instance->comma_Counter[lane_ID]++;
            if (pm_Instance->comma_Counter[lane_ID] < RIO__SYNC_FSM__COMMA_CONST_1)
            {
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__NO_SYNC_2, lane_ID);
                break;
            }
            else
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC, lane_ID);
        }

        if (PCS_PMA_PM__SYNC_FSM__NO_SYNC_2 == pm_Instance->sync_State[lane_ID])
        {
            if (is_Comma(pm_Instance, codegroup)) /* it cannot be invalid in this case */
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__NO_SYNC_1, lane_ID);
            else
                /* stay in the same state */
                break;
        }

        if (PCS_PMA_PM__SYNC_FSM__SYNC == pm_Instance->sync_State[lane_ID])
        {
            if (!pm_Instance->sync_Status[lane_ID])
            {
                char *out_Message;
                char *in_Message = "rio_PCS_PMA_PM_Exec_Sync_FSM: SYNCHRONIZATION is got on lane %i {Info #01}";

                out_Message = (char *)malloc(strlen(in_Message) + 20 /* length of lane_ID */);
                if (out_Message == NULL)
                {
                    rio_PCS_PMA_PM_Display_Msg(
                        pm_Instance,
                        RIO_PL_SERIAL_MSG_NOTE_MSG,
                        "rio_PCS_PMA_PM_Exec_Sync_FSM: cannot allocate memory"
                    );
                }
                else
                {
                    sprintf(out_Message, in_Message, lane_ID);
                    rio_PCS_PMA_PM_Display_Msg(
                        pm_Instance,
                        RIO_PL_SERIAL_MSG_NOTE_MSG,
                        out_Message
                    );
                    free(out_Message);
                }

                pm_Instance->sync_Status[lane_ID] = RIO_TRUE;
                pm_Instance->comma_Counter[lane_ID] = 0;
            }

            pm_Instance->invalids_Counter[lane_ID] = 0;
            PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC_PART_2, lane_ID);
            break;
        }

        if (PCS_PMA_PM__SYNC_FSM__SYNC_PART_2 == pm_Instance->sync_State[lane_ID])
        {
            PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC, lane_ID);
        }

        if (PCS_PMA_PM__SYNC_FSM__SYNC_1 == pm_Instance->sync_State[lane_ID])
        {
            pm_Instance->invalids_Counter[lane_ID]++;
            pm_Instance->valids_Counter[lane_ID] = 0;
            if (pm_Instance->invalids_Counter[lane_ID] < 
                pm_Instance->parameters_Set->icounter_Max /*RIO__SYNC_FSM__COMMA_CONST_2*/)
            {
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC_2_3, lane_ID);
                break;
            }
            else
            {
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__NO_SYNC, lane_ID);
                /* break; */
            }
        }

        if (PCS_PMA_PM__SYNC_FSM__SYNC_2_3 == pm_Instance->sync_State[lane_ID])
        {
            pm_Instance->valids_Counter[lane_ID]++;
            if (pm_Instance->valids_Counter[lane_ID] < RIO__SYNC_FSM__COMMA_CONST_3)
            {
                /* stay in the same joint state (SYNC_2 & SYNC_3) */
                break;
            }
            else
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC_4, lane_ID);
        }

        if (PCS_PMA_PM__SYNC_FSM__SYNC_4 == pm_Instance->sync_State[lane_ID])
        {
            pm_Instance->invalids_Counter[lane_ID]--;
            pm_Instance->valids_Counter[lane_ID] = 0;
            if (0 == pm_Instance->invalids_Counter[lane_ID])
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC, lane_ID);
            else
            {
                PCS_PMA_PM__SYNC_NEXT_STATE(PCS_PMA_PM__SYNC_FSM__SYNC_2_3, lane_ID);
                break;
            }
        }

        if (
            PCS_PMA_PM__SYNC_FSM__NO_SYNC == pm_Instance->sync_State[lane_ID] &&
            RIO_TRUE == pm_Instance->sync_Status[lane_ID]
        )
        {
            pm_Instance->ctray->rio_Set_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_IS_SYNC_LOST,
                RIO_TRUE,
                0 /* don't care */
            );
        }

        if (pm_Instance->sync_State[lane_ID] > PCS_PMA_PM__SYNC_FSM__SYNC_4)
        {
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Exec_Sync_FSM: unknown state for SYNC FSM"
            );
            return RIO_ERROR;
        }
    }

    if (PCS_PMA_PM__SYNC_FSM__NO_SYNC != pm_Instance->sync_State[lane_ID])
    /* the codegroup buffer shall be popped */
        pm_Instance->ctray->pop_Codegroup_Buffer(
            pm_Instance->ctray->codegroup_Level_Context,
            lane_ID /* lane_ID */
        );

    if (
        (
            !fictive_Call_Blocked &&
            (
                lane_ID == pm_Instance->lane_Driving_Init_FSM ||
                urgent_Fictive_Call_Enable
            ) &&
                /* this is a lane that is currently used to communicat with INIT FSM */
            (
                pm_Instance->align_State <= PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_PART_2 ||
                !are_All_Lanes_Sync(pm_Instance)
            ) &&
                /* 
                 * calls are stiil being blocked in the upper levels
                 * (not a single /A/ column is received so far)
                 */
            is_True_4x
                /* this is only for 4x mode */
        ) ||
        (
            !is_True_4x &&
            urgent_Fictive_Call_Enable
                /* this is only for 1x mode  - when synchronization just have been lost */
        )
    )
    {
        RIO_PCS_PMA_CODE_GROUP_T  codegroup;

        /* 
         * this is to drive the INIT FSM => hook shall be suppressed
         */
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_SUPPRESS_HOOK,
            RIO_TRUE,
            0 /* don't care */
        );
        codegroup.code_Group_Data[lane_ID] = 0;
        codegroup.is_Running_Disparity_Positive[lane_ID] = RIO_FALSE;
        codegroup.lane_Mask = (unsigned char)(1 << lane_ID);
        pm_Instance->ctray->put_Codegroup(
            pm_Instance->ctray->codegroup_Level_Context,
            &codegroup
        );
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_SUPPRESS_HOOK,
            RIO_FALSE,
            0 /* don't care */
        );
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : is_Comma
 *
 * Description: checks if the codegroup contains comma
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T is_Comma(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance,
    unsigned int         codegroup
)
{
    (void)pm_Instance;
    if (
        RIO_NEG_COMMA_SHIFTED == (codegroup >> RIO_COMMA_SHIFT_LENGTH) ||
        RIO_POS_COMMA_SHIFTED == (codegroup >> RIO_COMMA_SHIFT_LENGTH)
    )
        return RIO_TRUE;
    else
        return RIO_FALSE;
}

/***************************************************************************
 * Function : is_Pos_Comma
 *
 * Description: checks if the codegroup contains a comma
 *              with a positive running disparity
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T is_Pos_Comma(
    RIO_PCS_PMA_PM_DS_T  *pm_Instance,
    unsigned int         codegroup
)
{
    (void)pm_Instance;
    if (RIO_POS_COMMA_SHIFTED == (codegroup >> RIO_COMMA_SHIFT_LENGTH))
        return RIO_TRUE;
    else
        return RIO_FALSE;
}


/*****************************************************************************/

