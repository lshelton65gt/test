/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_pm.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "rio_pcs_pma_pm.h"
#include "rio_pcs_pma_pm_ds.h"
#include "rio_pcs_pma_pm_sync.h"
#include "rio_pcs_pma_pm_align.h"
#include "rio_pcs_pma_pm_init.h"


/* FOR DEBUG */
/*#define DEBUG_MESSAGING*/

/*
 * Global variable for counting number of PCS/PMA Protocol Manager
 * instantiations. It is used for proper instance number assignement
 */
static unsigned long rio_PCS_PMA_PM_Inst_Count = 0;



static int rio_PCS_PMA_PM_Initialize(
    RIO_HANDLE_T                handle,
    RIO_PCS_PMA_PARAM_SET_T     *param_Set
    );

static int rio_PCS_PMA_PM_Start_Reset(
    RIO_HANDLE_T                handle
    );

static int rio_PCS_PMA_PM_Delete_Instance(
    RIO_HANDLE_T                handle
    );

static int rio_PCS_PMA_PM_Machine_Management(
    RIO_HANDLE_T                handle,
    RIO_BOOL_T                  machine_Is_Switched_On, /*RIO_TRUE if seq must be sent*/
    RIO_PCS_PMA_MACHINE_T       machine_Type /*size is 4*/
);


/***************************************************************************
 * Function : RIO_PCS_PMA_PM_Create_Instance
 *
 * Description: Create new PCS/PMA PM instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_PM_Create_Instance(
    RIO_HANDLE_T            *handle,
    RIO_HANDLE_T            mum_Instanse
    )
{
    /* handle to structure will allocated*/
    RIO_PCS_PMA_PM_DS_T *pm_Instance = NULL;

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for physical layer 
     * data structure
     */
    if (handle == NULL )
        return RIO_ERROR;

    /* clear handle */
    *handle = NULL;

    /* allocate instan�e */
    if ((pm_Instance = malloc(sizeof(RIO_PCS_PMA_PM_DS_T))) == NULL)
        return RIO_ERROR;

    /* copy the pointer to the upper block */
    pm_Instance->mum_Instanse = mum_Instanse;

    if ((pm_Instance->ctray = malloc(sizeof(RIO_PCS_PMA_PM_CALLBACK_TRAY_T))) == NULL)
        return RIO_ERROR;
    /*
     * clear all ctray pointers
     */
    pm_Instance->ctray->rio_User_Msg = NULL;
    pm_Instance->ctray->user_Msg_Context = NULL;
    pm_Instance->ctray->rio_Set_State_Flag = NULL;
    pm_Instance->ctray->rio_Get_State_Flag = NULL;
    pm_Instance->ctray->rio_Regs_Context = NULL;
    pm_Instance->ctray->get_Granule = NULL;
    pm_Instance->ctray->put_Granule = NULL;
    pm_Instance->ctray->granule_Level_Context = NULL;
    pm_Instance->ctray->put_Character_Column = NULL;
    pm_Instance->ctray->get_Character_Column = NULL;
    pm_Instance->ctray->pop_Char_Buffer = NULL;
    pm_Instance->ctray->character_Level_Context = NULL;
    pm_Instance->ctray->put_Codegroup = NULL;
    pm_Instance->ctray->get_Codegroup_Column = NULL;
    pm_Instance->ctray->pop_Codegroup_Buffer = NULL;
    pm_Instance->ctray->codegroup_Level_Context = NULL;

    /*
     * increase count of TxRx instances and set
     * up the number of instance, turn link into reset
     */
    *handle = pm_Instance;
    pm_Instance->instance_Number = ++rio_PCS_PMA_PM_Inst_Count;

    /* TBD: initialize the randome generator from timer */
/*    srand((unsigned)time(NULL)); */
    /* srand(0); */

    pm_Instance->is_Ctray_Registered = RIO_FALSE;
    pm_Instance->is_In_Reset = RIO_FALSE;
    rio_PCS_PMA_PM_Start_Reset(pm_Instance);

    return RIO_OK;

}

/***************************************************************************
 * Function : RIO_PCS_PMA_PM_Get_Function_Tray
 *
 * Description: Export function tray, set pointers to corresponding 
 *              functions entry point
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_PM_Get_Function_Tray(
    RIO_PCS_PMA_PM_FTRAY_T *ftray             
    )
{
    /*
     * check pointers to not NULL values and complete function tray
     * with valid entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->start_Reset = rio_PCS_PMA_PM_Start_Reset;
    ftray->initialize = rio_PCS_PMA_PM_Initialize;
    ftray->machine_Management = rio_PCS_PMA_PM_Machine_Management;
    ftray->delete_Instance = rio_PCS_PMA_PM_Delete_Instance;

    ftray->put_Granule = rio_PCS_PMA_PM_Put_Granule;
    ftray->get_Granule = rio_PCS_PMA_PM_Get_Granule;

    ftray->put_Character_Column = rio_PCS_PMA_PM_Put_Character_Column;
    ftray->get_Character_Column = rio_PCS_PMA_PM_Get_Character_Column;

    ftray->put_Codegroup = rio_PCS_PMA_PM_Put_Codegroup;
    ftray->get_Codegroup_Column = rio_PCS_PMA_PM_Get_Codegroup_Column;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PCS_PMA_PM_Bind_Instance
 *
 * Description: bind the instance with the environment, load entry point from
 *              callback tray to internal structure
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_PM_Bind_Instance(
    RIO_HANDLE_T                     handle,
    RIO_PCS_PMA_PM_CALLBACK_TRAY_T   *ctray   
    )
{
    /* 
     * convert void pointer to pointer to instance structure and 
     * check all callbacks pointers to be not NULL and 
     * store external callbacks with environment's context
     * from callback tray in instance's data structure
     */
    RIO_PCS_PMA_PM_DS_T* pm_Instance = (RIO_PCS_PMA_PM_DS_T*)handle;

    if (ctray == NULL || 
        handle == NULL ||
        ctray->rio_User_Msg == NULL ||
        ctray->rio_Set_State_Flag == NULL ||
        ctray->rio_Get_State_Flag == NULL ||
        ctray->get_Granule == NULL ||
        ctray->put_Granule == NULL ||
        ctray->put_Character_Column == NULL ||
        ctray->get_Character_Column == NULL ||
        ctray->pop_Char_Buffer == NULL ||
        ctray->put_Codegroup == NULL ||
        ctray->get_Codegroup_Column == NULL ||
        ctray->pop_Codegroup_Buffer == NULL
    )
        return RIO_ERROR;
    
    pm_Instance->ctray = (RIO_PCS_PMA_PM_CALLBACK_TRAY_T *)malloc(sizeof(RIO_PCS_PMA_PM_CALLBACK_TRAY_T));
    if (NULL == pm_Instance->ctray)
    {
        /* error: unsuccessful malloc */
#ifndef _CARBON_SRIO_XTOR
        printf("PM: ERROR/WARNING: unsuccessful malloc");
#endif
        return RIO_ERROR;
    }
    *pm_Instance->ctray = *ctray;
    pm_Instance->is_Ctray_Registered = RIO_TRUE;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Delete_Instance
 *
 * Description: deletes instance of PCS/PMA PM
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_PCS_PMA_PM_Delete_Instance(
    RIO_HANDLE_T          handle
    )
{ 
    RIO_PCS_PMA_PM_DS_T *rio_PCS_PMA_PM_Instance = (RIO_HANDLE_T)handle;

    /* check pointers */
    if (rio_PCS_PMA_PM_Instance == NULL) 
        return RIO_ERROR;

    if (rio_PCS_PMA_PM_Instance != NULL)
        free(rio_PCS_PMA_PM_Instance);
   
    return RIO_OK; 
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Initialize
 *
 * Description: initialize the PCS/PMA PM
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Initialize(
    RIO_HANDLE_T                handle,
    RIO_PCS_PMA_PM_PARAM_SET_T  *param_Set
)
{
    /* instanse handle and link's parameters set */
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T*)handle;
    RIO_UCHAR_T i;

    /* check pointers */
    if (pm_Instance == NULL)
        return RIO_ERROR;

    if (param_Set == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (!pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Initialize: is called while not in reset\n");
#else
	if(pm_Instance->ctray)
		pm_Instance->ctray->rio_User_Msg(
				pm_Instance->ctray->user_Msg_Context,
				RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Initialize: is called while not in reset\n");
#endif
        return RIO_ERROR;
    }

    if (!pm_Instance->is_Ctray_Registered)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Initialize: is called while ctray is not registered\n");
#endif
        return RIO_ERROR;
    }
    /*set off the reset flag*/
    pm_Instance->is_In_Reset = RIO_FALSE;

    
    pm_Instance->parameters_Set = (RIO_PCS_PMA_PM_PARAM_SET_T *)malloc(sizeof (RIO_PCS_PMA_PM_PARAM_SET_T));
    /* this structure is simple and thus such resssignment OK */
    *(pm_Instance->parameters_Set) = *param_Set;

    
#ifdef _CUSTOM_DEBUG
    pm_Instance->debug_Iteration_Counter = 0;
#endif /* _CUSTOM_DEBUG */

    
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        pm_Instance->sync_Status[i] = RIO_FALSE;
        pm_Instance->sync_State[i] = PCS_PMA_PM__SYNC_FSM__NO_SYNC_PART_2;
        pm_Instance->comma_Counter[i] = 0;
        pm_Instance->invalids_Counter[i] = 0;
        pm_Instance->valids_Counter[i] = 0;
        /* this is needed to initialize the SYNC FSM */
    }

    pm_Instance->align_Status = RIO_FALSE;
    pm_Instance->align_State = PCS_PMA_PM__ALIGN_FSM__NOT_ALIGNED_PART_2;
    pm_Instance->align_Counter = 0;
    pm_Instance->misalign_Counter = 0;

    if (pm_Instance->parameters_Set->is_Init_Proc_On)
        pm_Instance->is_Init_FSM_On = RIO_TRUE;
    else
        pm_Instance->is_Init_FSM_On = RIO_FALSE;

    if (pm_Instance->parameters_Set->is_Status_Sym_Gen_On)
        pm_Instance->is_Status_Sym_Gen_On = RIO_TRUE;
    else
        pm_Instance->is_Status_Sym_Gen_On = RIO_FALSE;

    if (pm_Instance->parameters_Set->is_Comp_Seq_Gen_On)
        pm_Instance->is_Comp_Seq_Gen_On = RIO_TRUE;
    else
        pm_Instance->is_Comp_Seq_Gen_On = RIO_FALSE;

    pm_Instance->init_Status = RIO_FALSE;
    pm_Instance->init_State = PCS_PMA_PM__INIT_FSM__SILENT_PART_2;
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        pm_Instance->is_Lane_OE[i] = RIO_FALSE;
    }
    pm_Instance->lane_Driving_Init_FSM = 0;

    pm_Instance->silence_Timer = pm_Instance->parameters_Set->silence_Period;
    pm_Instance->discovery_Timer = 0;
    pm_Instance->rcv_Status_Symbols_Counter = 0;
    pm_Instance->snd_Status_Symbols_Counter = 0;
    pm_Instance->trx_Requests_Enabled = RIO_FALSE;
    pm_Instance->is_Packet_Streaming_Broken = RIO_FALSE;
    pm_Instance->is_Comp_Seq_Delayed_For_Status = RIO_FALSE;

    /* 
     * sequence generation
     */
    /* check pm_Instance->parameters_Set->comp_Seq_Rate for the valid range */
    if (pm_Instance->parameters_Set->comp_Seq_Rate > PCS_PMA_PM__COMP_SEQ_RATE_MAX)
    {
        pm_Instance->parameters_Set->comp_Seq_Rate = PCS_PMA_PM__COMP_SEQ_RATE_MAX;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: comp_Seq_Rate parameter is out of range (too big); is set to the highest allowable level"
        );
    }
    else
    if (pm_Instance->parameters_Set->comp_Seq_Rate < PCS_PMA_PM__COMP_SEQ_RATE_MIN)
    {
        pm_Instance->parameters_Set->comp_Seq_Rate = PCS_PMA_PM__COMP_SEQ_RATE_MIN;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: comp_Seq_Rate parameter is out of range (too small); is set to the lowest allowable level"
        );
    }

    /* setup the size of the next comp sequence */
    if (pm_Instance->parameters_Set->comp_Seq_Size != 0)
    {
        double rand_Res;
        int expr1, expr2, expr3;

        rand_Res = rand() / (double)RAND_MAX;

        /*  Konstantin suggestion  with 7-29-04  */
        expr1 =  PCS_PMA_PM__COMPENSATION_SEQUENCE_SIZE - pm_Instance->parameters_Set->comp_Seq_Size;
        expr2 = 2 * pm_Instance->parameters_Set->comp_Seq_Size;
        expr3 = (int)( rand_Res * 2);

        if (expr3 == 0)
            pm_Instance->comp_Seq_Size_Next = expr1;
        else
            pm_Instance->comp_Seq_Size_Next = expr1 + expr2;


        /* DAA 7-27-04 ORG
        pm_Instance->comp_Seq_Size_Next = 
        PCS_PMA_PM__COMPENSATION_SEQUENCE_SIZE - pm_Instance->parameters_Set->comp_Seq_Size +
        2 * pm_Instance->parameters_Set->comp_Seq_Size * ((int)( rand_Res * 2));
        */
    }
    else
        pm_Instance->comp_Seq_Size_Next = PCS_PMA_PM__COMPENSATION_SEQUENCE_SIZE;


    pm_Instance->before_Comp_Seq_Counter =
            pm_Instance->parameters_Set->comp_Seq_Rate -
            RIO_PL_MAX_PACKET_SIZE -
            pm_Instance->comp_Seq_Size_Next -
            PCS_PMA_PM__COMPENSATION_DELTA;
    /* align to 4 */
    pm_Instance->before_Comp_Seq_Counter =
        ((unsigned long)pm_Instance->before_Comp_Seq_Counter / RIO_PL_SERIAL_NUMBER_OF_LANES) *
        RIO_PL_SERIAL_NUMBER_OF_LANES;
    pm_Instance->comp_Seq_Counter = 0;

    /* 
     * sequence check
     */
    /* check pm_Instance->parameters_Set->comp_Seq_Rate_Check for the valid range */
    if ((pm_Instance->parameters_Set->comp_Seq_Rate_Check < PCS_PMA_PM__COMP_SEQ_RATE_MIN)
		&& (pm_Instance->parameters_Set->comp_Seq_Rate_Check != 0))
    {
        pm_Instance->parameters_Set->comp_Seq_Rate_Check = PCS_PMA_PM__COMP_SEQ_RATE_MIN;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: comp_Seq_Rate_Check parameter is out of range (too small); is set to the lowest allowable level"
        );
    }

    /* check pm_Instance->parameters_Set->comp_Seq_Size for the valid range */
    if (pm_Instance->parameters_Set->comp_Seq_Size > 2)
    {
        pm_Instance->parameters_Set->comp_Seq_Size = 0;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: comp_Seq_Size parameter is out of range (too big); is set to zero"
        );
    }
	
	pm_Instance->comp_Seq_Check_Counter = 0;
    for (i = 0; i < 10; i++)
    {
        pm_Instance->comp_Seq_Check_Buf[i] = RIO_I;
    }

    /* check pm_Instance->parameters_Set->status_Sym_Rate for the valid range */
    if (pm_Instance->parameters_Set->status_Sym_Rate > PCS_PMA_PM__STATUS_SYMBOL_RATE_MAX)
    {
        pm_Instance->parameters_Set->status_Sym_Rate = PCS_PMA_PM__STATUS_SYMBOL_RATE_MAX;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: status_Sym_Rate parameter is out of range (too big); is set to the highest allowable level"
        );
    }
    else
    if (pm_Instance->parameters_Set->status_Sym_Rate < PCS_PMA_PM__STATUS_SYMBOL_RATE_MIN)
    {
        pm_Instance->parameters_Set->status_Sym_Rate = PCS_PMA_PM__STATUS_SYMBOL_RATE_MIN;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: status_Sym_Rate parameter is out of range (too small); is set to the lowest allowable level"
        );
    }

    if (pm_Instance->parameters_Set->icounter_Max < RIO__SYNC_FSM__COMMA_CONST_2)
    {
        pm_Instance->parameters_Set->icounter_Max = RIO__SYNC_FSM__COMMA_CONST_2;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: icounter_Max parameter is out of range (too small); is set to the lowest allowable level"
        );
    }
    
    /* align to 4 */
    pm_Instance->parameters_Set->status_Sym_Rate =
        ((unsigned long)pm_Instance->parameters_Set->status_Sym_Rate / RIO_PL_SERIAL_NUMBER_OF_LANES) *
        RIO_PL_SERIAL_NUMBER_OF_LANES;
    pm_Instance->before_Status_Symbol_Counter =
        pm_Instance->parameters_Set->status_Sym_Rate - PCS_PMA_PM__STATUS_SYMBOL_SIZE;
    pm_Instance->idle_Delayed = RIO_TRUE;
    pm_Instance->idle_Non_A_Counter = 0;

    /* check pm_Instance->parameters_Set->status_Symbols_To_Send for the valid range */
    if (pm_Instance->parameters_Set->status_Symbols_To_Send > PCS_PMA_PM__STATUS_SYM_TO_SEND_MAX)
    {
        pm_Instance->parameters_Set->status_Symbols_To_Send = PCS_PMA_PM__STATUS_SYM_TO_SEND_MAX;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: status_Symbols_To_Send parameter is out of range (too big); is set to the highest allowable level"
        );
    }
    // This is generating a warning if PCS_PMA_PM__STATUS_SYM_TO_SEND_MIN == 0 (which it is)
#if PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MIN > 0
    else
    if (pm_Instance->parameters_Set->status_Symbols_To_Send < PCS_PMA_PM__STATUS_SYM_TO_SEND_MIN)
    {
        pm_Instance->parameters_Set->status_Symbols_To_Send = PCS_PMA_PM__STATUS_SYM_TO_SEND_MIN;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: status_Symbols_To_Send parameter is out of range (too small); is set to the lowest allowable level"
        );
    }
#endif
    /* check pm_Instance->parameters_Set->status_Symbols_To_Receive for the valid range */
    if (pm_Instance->parameters_Set->status_Symbols_To_Receive > PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MAX)
    {
        pm_Instance->parameters_Set->status_Symbols_To_Receive = PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MAX;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: status_Symbols_To_Receive parameter is out of range (too big); is set to the highest allowable level"
        );
    }

    // This is generating a warning if PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MIN == 0 (which it is)
#if PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MIN > 0
    else
    if (pm_Instance->parameters_Set->status_Symbols_To_Receive < PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MIN)
    {
        pm_Instance->parameters_Set->status_Symbols_To_Receive = PCS_PMA_PM__STATUS_SYM_TO_RECEIVE_MIN;
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Initialize: status_Symbols_To_Receive parameter is out of range (too small); is set to the lowest allowable level"
        );
    }
#endif

    /* 
     * setting flag RIO_IS_PORT_TRUE_4X
     */
    rio_PCS_PMA_PM_Reset_Port_Regs(pm_Instance);

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Start_Reset(
    RIO_HANDLE_T handle
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle;

    /* check pointers */
    if (pm_Instance == NULL)
        return RIO_ERROR;

    /* check work state */
    if (pm_Instance->is_In_Reset == RIO_TRUE)
        return RIO_OK;
    
    /* modify state to be _in_reset_ */
    pm_Instance->is_In_Reset = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Display_Msg
 *
 * Description: deletes instance of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Display_Msg(
    RIO_HANDLE_T                handle,
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
)
{ 
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_HANDLE_T)handle;

    /* check pointers */
    if (pm_Instance == NULL) 
        return RIO_ERROR;

    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Display_Msg: is called while in reset\n");
#else
	if(pm_Instance->ctray)
        pm_Instance->ctray->rio_User_Msg(
                pm_Instance->ctray->user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Display_Msg: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

#ifdef DEBUG_MESSAGING
    pm_Instance->ctray->rio_User_Msg(
        pm_Instance->ctray->user_Msg_Context,
        msg_Type,
        string
    );
#endif /* DEBUG_MESSAGING */
#ifndef DEBUG_MESSAGING
    {
        char *sub_String;
        char str_PCS_PMA[40]; /* size of the PCS/PMA prefix (including instance_Number) */
        char *tmp_String;

        sprintf(str_PCS_PMA, "PCS_PMA #%lu: ", pm_Instance->instance_Number);
        sub_String = (char *)malloc(strlen(string) + strlen(str_PCS_PMA) + 10); /* the extra space is reserved */
        strcpy(sub_String, str_PCS_PMA);
        tmp_String = strchr(string, ':');
        if (NULL == tmp_String)
            strcpy(sub_String + strlen(sub_String), string);  /* the whole string is copied */
        else
            strcpy(sub_String + strlen(sub_String), strchr(string, ':') + 2);  /* pointer to the character */
                                                                               /* next to ':' (a space-char is also skipped) */
        /* error messages printing via user_Msg callback */
        pm_Instance->ctray->rio_User_Msg(
            pm_Instance->ctray->user_Msg_Context,
            msg_Type,
            sub_String
        );
    }
#endif /* !DEBUG_MESSAGING */

/*    printf("PM: ERROR/WARNING: %s\n", string); */

    /* for DEBUGGING needs */
    if (RIO_PL_SERIAL_MSG_ERROR_MSG == msg_Type)
    {
        int i;

        i = 0;
    }

    return RIO_OK; 
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Machine_Management
 *
 * Description: turns ON/OFF FSMs
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Machine_Management(
    RIO_HANDLE_T                handle,
    RIO_BOOL_T                    machine_Is_Switched_On, /*RIO_TRUE if seq must be sent*/
    RIO_PCS_PMA_MACHINE_T       machine_Type /*size is 4*/
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle;
    RIO_BOOL_T  *is_FSM_On;

    /* check pointers */
    if (pm_Instance == NULL)
        return RIO_ERROR;
    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {  
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Machine_Management: is called while in reset\n");
#else
	if(pm_Instance->ctray)
        pm_Instance->ctray->rio_User_Msg(
                pm_Instance->ctray->user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Machine_Management: is called while in reset\n");
#endif
        return RIO_ERROR;
    }
        
    /* 
     * !!!!!!!!!!!!!!!!!!!!!!!!!
     * it is implemented now that machine_Type parameter is disregarded
     * and all FSMs are turned ON/OFF synchroneously (all together)
     */

    /* chose which FSM to affect */
    switch (machine_Type)
    {
    case RIO_PCS_PMA_MACHINE_INIT:
        is_FSM_On = &pm_Instance->is_Init_FSM_On;
        break;

    case RIO_PCS_PMA_MACHINE_COMP_SEQ_GEN:
        is_FSM_On = &pm_Instance->is_Comp_Seq_Gen_On;
        break;

    case RIO_PCS_PMA_MACHINE_STATUS_SYM_GEN:
        is_FSM_On = &pm_Instance->is_Status_Sym_Gen_On;
        break;

    default:
        /* error: wrong machine_Type */
        rio_PCS_PMA_PM_Display_Msg(
            pm_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_PM_Machine_Management: wrong machine_Type parameter"
        );
        return RIO_ERROR;
    }

    /* setting the corresponding FSM */
    if (machine_Is_Switched_On)
        *is_FSM_On = RIO_TRUE;
    else
        *is_FSM_On = RIO_FALSE;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Port_Control
 *
 * Description: gets port control settings
 *
 * Returns: error code
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Get_Port_Control(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,
    RIO_BOOL_T              *lp_Serial_Is_1x,
    RIO_BOOL_T              *is_Force_1x_Mode,
    RIO_BOOL_T              *is_Force_1x_Mode_Lane_0
)
{
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;

    /* initial default settings */
    *lp_Serial_Is_1x = RIO_FALSE;
    *is_Force_1x_Mode = RIO_FALSE;
    *is_Force_1x_Mode_Lane_0 = RIO_TRUE;
    
    pm_Instance->ctray->rio_Get_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_PORT_WIDTH,
        &indicator_Value,
        &indicator_Parameter
    );

    if (RIO_PORT_WIDTH__SINGLE_LANE == indicator_Value)
    {
        *lp_Serial_Is_1x = RIO_TRUE;
        return RIO_OK;
    }
    else
    if (RIO_PORT_WIDTH__FOUR_LANE == indicator_Value)
    {
        *lp_Serial_Is_1x = RIO_FALSE;
    
        if (pm_Instance->init_Status)
        {
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_INITIALIZED_PORT_WIDTH,
                &indicator_Value,
                &indicator_Parameter
            );
            if (RIO_INITIALIZED_PORT_WIDTH__SINGLE_LANE_0 == indicator_Value)
            {
                *is_Force_1x_Mode = RIO_TRUE;
                *is_Force_1x_Mode_Lane_0 = RIO_TRUE;
                return RIO_OK;
            }
            else
            if (RIO_INITIALIZED_PORT_WIDTH__SINGLE_LANE_2 == indicator_Value)
            {
                *is_Force_1x_Mode = RIO_TRUE;
                *is_Force_1x_Mode_Lane_0 = RIO_FALSE;
                return RIO_OK;
            }
            else
            if (RIO_INITIALIZED_PORT_WIDTH__FOUR_LANE == indicator_Value)
            {
                *is_Force_1x_Mode = RIO_FALSE;
                return RIO_OK;
            }
        }
        else
        {
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_PORT_WIDTH_OVERRIDE,
                &indicator_Value,
                &indicator_Parameter
            );
            if (RIO_PORT_WIDTH_OVERRIDE__SINGLE_LANE_0 == indicator_Value)
            {
                *is_Force_1x_Mode = RIO_TRUE;
                *is_Force_1x_Mode_Lane_0 = RIO_TRUE;
                return RIO_OK;
            }
            else
            if (RIO_PORT_WIDTH_OVERRIDE__SINGLE_LANE_2 == indicator_Value)
            {
                *is_Force_1x_Mode = RIO_TRUE;
                *is_Force_1x_Mode_Lane_0 = RIO_FALSE;
                return RIO_OK;
            }
            else
            if (RIO_PORT_WIDTH_OVERRIDE__NO_OVERRIDE == indicator_Value)
            {
                *is_Force_1x_Mode = RIO_FALSE;
                return RIO_OK;
            }
        }
    }

    /* reserved value returned in some of the parameters */
    return RIO_ERROR;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Reset_Port_Regs
 *
 * Description: reset all regs values 
 *              (usually when entering the SILENT state)
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Reset_Port_Regs(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance
)
{ 
    RIO_BOOL_T              lp_Serial_Is_1x;
    RIO_BOOL_T              is_Force_1x_Mode;
    RIO_BOOL_T              is_Force_1x_Mode_Lane_0;

    pm_Instance->ctray->rio_Set_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_INITIALIZED,
        RIO_FALSE,
        0 /* don't care */
    );

    /*in case when lost sync is occured during packet transmission
    it is necessary reset "rx packet ongoing" flag
    this is necessary to prevent "idle inside packet" errors*/
    pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_RX_IS_PD,
            (int)RIO_FALSE,
            0 /* don't care */
        );

    rio_PCS_PMA_PM_Get_Port_Control(
        pm_Instance,
        &lp_Serial_Is_1x,
        &is_Force_1x_Mode,
        &is_Force_1x_Mode_Lane_0
    );

    if (
        lp_Serial_Is_1x ||
        is_Force_1x_Mode
    )
    /* 1x mode */
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_IS_PORT_TRUE_4X,
            RIO_FALSE,
            0 /* don't care */
        );
    else
    /* true 4x mode */
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_IS_PORT_TRUE_4X,
            RIO_TRUE,
            0 /* don't care */
        );

    pm_Instance->ctray->rio_Set_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_LANE2_JUST_FORCED_BY_DISCOVERY,
        RIO_FALSE,
        0 /* don't care */
    );
    if (
        !lp_Serial_Is_1x &&
        is_Force_1x_Mode &&
        !is_Force_1x_Mode_Lane_0
    )
    /* force lane2 mode */
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_COPY_TO_LANE2,
            RIO_TRUE,
            0 /* don't care */
        );
    else
    /* NOT force lane2 mode */
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_COPY_TO_LANE2,
            RIO_FALSE,
            0 /* don't care */
        );

    /* init flags for reporting loss of alignment/synchronization */
    pm_Instance->ctray->rio_Set_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_IS_ALIGNMENT_LOST,
        RIO_FALSE,
        0 /* don't care */
    );
    pm_Instance->ctray->rio_Set_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_IS_SYNC_LOST,
        RIO_FALSE,
        0 /* don't care */
    );

    pm_Instance->ctray->rio_Set_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_INITIAL_RUN_DISP,
        PCS_PMA__RUN_DISP_NOT_CHANGE,
        0 /* don't care */
    );
    if (pm_Instance->parameters_Set->is_Init_Proc_On)
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_DECODE_ANY_RUN_DISP,
            0, /* NOT applicable for all lanes */
            0 /* don't care */
        );
    else
        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_DECODE_ANY_RUN_DISP,
            RIO_PL_ALL_LANES_MASK, /* applicable for all lanes */
            0 /* don't care */
        );

    pm_Instance->ctray->rio_Set_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_IS_CHARACTER_INVALID,
        RIO_FALSE,
        0 /* don'tcare */
    );
    pm_Instance->ctray->rio_Set_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_LANE_WITH_INVALID_CHARACTER,
        0, /* lane_Mask */
        0 /* don't care */
    );
    
    return RIO_OK; 
}

/*****************************************************************************/
