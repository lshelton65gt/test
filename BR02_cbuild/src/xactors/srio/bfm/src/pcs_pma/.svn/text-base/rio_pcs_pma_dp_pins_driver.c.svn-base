/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_dp_pins_driver.c $ 
* $Author: markk $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  32-Bit Transmitter/Receiver source
*
* Notes:
*
******************************************************************************/

#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>      
#include <stdarg.h>

#include "rio_pcs_pma_dp.h"
#include "rio_pcs_pma_dp_ds.h"
#include "rio_pcs_pma_dp_pd.h"


static int pd_Lane_Pass_Bit_Value(RIO_HANDLE_T  handle, RIO_SIGNAL_T* value);

static int pd_Lane_Class_Get_Code_Group_Value(
    RIO_HANDLE_T  handle,
    unsigned int* value
);

static int pd_Lane_Class_Clean_Buffer(
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse
);

static int pd_Lane_Class_Set_Code_Group_Value(
    RIO_HANDLE_T  handle,
    unsigned int value
);

static int pd_Lane_Add_Bit_Value(
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse,
    RIO_SIGNAL_T value
);

static int pd_Buffer_Is_Full(
    RIO_HANDLE_T handle,
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse
);

static int pd_Buffer_Is_Empty(
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse
);

static int pd_Get_Codegroup_Value(
    RIO_HANDLE_T handle,
    unsigned int *value,
    unsigned int lane_Num
);

static int pd_Lane_Initialize(
    RIO_HANDLE_T  handle,
    RIO_HANDLE_T mum_Instance,
    RIO_BOOL_T is_Out,
    unsigned int self_Number
);


/***************************************************************************
 * Function : pd_Lane_Pass_Bit_Value 
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Lane_Pass_Bit_Value(
    RIO_HANDLE_T  handle,
    RIO_SIGNAL_T* value
)
{
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse;
    unsigned int i;

    if (handle == NULL)
        return RIO_ERROR;

    pd_Lane_Instanse = (RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T*)handle; 

    if (pd_Lane_Instanse->buf_Pointer == RIO_PCS_PMA_DP_PD_LANE_BUF_TOP)
    {
        /*tell the mummy about empty buffer*/
        pd_Buffer_Is_Empty(pd_Lane_Instanse);
    }

    if (pd_Lane_Instanse->buf_Pointer == RIO_PCS_PMA_DP_PD_LANE_BUF_TOP)
        return RIO_NO_DATA;

    *value = pd_Lane_Instanse->lane_Buf[RIO_PCS_PMA_DP_PD_LANE_BUF_TOP];

    for (i = RIO_PCS_PMA_DP_PD_LANE_BUF_TOP; i < pd_Lane_Instanse->buf_Pointer; i++)
    {
        pd_Lane_Instanse->lane_Buf[i] = pd_Lane_Instanse->lane_Buf[i+1];
    }
        
    pd_Lane_Instanse->buf_Pointer--;

    /*in txrx model it is necessary to check for nesisity to invoke
    callback which notifies about packet finishing
    mantis. This is 71 implementation*/
    if (pd_Lane_Instanse->buf_Pointer == RIO_PCS_PMA_DP_PD_LANE_BUF_TOP)
    {
        RIO_PL_PCS_PMA_DP_PD_DS_T * mum_Inst;
        RIO_PL_PCS_PMA_DP_DS_T * pl_Pcs_Pma_Dp_Instance;
        unsigned long cnt_Val;
        unsigned int stub;

        stub = 0;
        cnt_Val = 0; /*initialize cnt_Val for case when pcs_PMA is 
        attached to the pl or pe model, and RIO_PACKET_FINISHING_CNT
        register is not supported. 
        In this case get_state_flag routine doesnt change the cnt_Val value
        and decrement will work with the initialized value.
        then after call for set_state_flag pe or pl will do nothing
        
        
        in case when txrx model is attached to pcs/pma then to the 
        cnt_Val correct value of sent codegroups will be placed in the
        from the txrx register block. After that this value will be decre-
        ased and will written back. In this case after value will be decreased
        it will be write back and in case when 0 will be wirite back 
        the register block will analyse it and invoke the callback*/

        /*accept handle of the parent instance*/

        mum_Inst = (RIO_PL_PCS_PMA_DP_PD_DS_T*)pd_Lane_Instanse->mum_Instanse;
        pl_Pcs_Pma_Dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)mum_Inst->mum_Instanse;
        /*perform decrement of sent codegroups number*/

        pl_Pcs_Pma_Dp_Instance->interfaces.rio_Get_State_Flag(
            pl_Pcs_Pma_Dp_Instance->interfaces.rio_Regs_Context,
            pd_Lane_Instanse, RIO_PACKET_FINISHING_CNT, &cnt_Val, &stub);

        if (cnt_Val != 0)
        {
            cnt_Val--;
            pl_Pcs_Pma_Dp_Instance->interfaces.rio_Set_State_Flag(
                pl_Pcs_Pma_Dp_Instance->interfaces.rio_Regs_Context,
                pd_Lane_Instanse, RIO_PACKET_FINISHING_CNT, cnt_Val, stub);
        }
    }

    pd_Lane_Instanse->is_Full = RIO_FALSE;


    return RIO_OK;
}

/***************************************************************************
 * Function : pd_Lane_Class_Get_Code_Group_Value 
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Lane_Class_Get_Code_Group_Value(
    RIO_HANDLE_T  handle,
    unsigned int* value
)
{
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse;
    unsigned int i;

    if (handle == NULL)
        return RIO_ERROR;

    pd_Lane_Instanse = (RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T*)handle; 

    if (pd_Lane_Instanse->is_Full == RIO_FALSE)
        return RIO_NO_DATA;

    *value = 0;

    for (i = RIO_PCS_PMA_DP_PD_LANE_BUF_TOP; i <= RIO_PCS_PMA_DP_PD_LANE_BUF_BOTTOM; i++)
    {    
        *value <<= 1;
        *value |= (pd_Lane_Instanse->lane_Buf[i] & 1);
    }

    return RIO_OK;
}
/***************************************************************************
 * Function : pd_Lane_Class_Clean_Buffer
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Lane_Class_Clean_Buffer(
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse
)
{
    if (pd_Lane_Instanse == NULL)
        return RIO_ERROR;

    pd_Lane_Instanse->is_Full = RIO_FALSE;
    pd_Lane_Instanse->buf_Pointer = RIO_PCS_PMA_DP_PD_LANE_BUF_TOP;
    return RIO_OK;
}
/***************************************************************************
 * Function : pd_Lane_Class_Set_Code_Group_Value 
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Lane_Class_Set_Code_Group_Value(
    RIO_HANDLE_T  handle,
    unsigned int value
)
{
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse;
    unsigned int i;

    if (handle == NULL)
        return RIO_ERROR;

    pd_Lane_Instanse = (RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T*)handle; 

    /*may be not necessary*/
    if (pd_Lane_Instanse->is_Full == RIO_TRUE || 
        pd_Lane_Instanse->buf_Pointer > RIO_PCS_PMA_DP_PD_LANE_BUF_TOP)
        return RIO_ERROR;

    pd_Lane_Instanse->buf_Pointer = RIO_PCS_PMA_DP_PD_LANE_BUF_TOP;

    for (i = RIO_PCS_PMA_DP_PD_LANE_BUF_TOP; i <= RIO_PCS_PMA_DP_PD_LANE_BUF_BOTTOM; i++)
    {    
        pd_Lane_Instanse->lane_Buf[i] = (value >> (RIO_PCS_PMA_DP_PD_LANE_BUF_BOTTOM - i)) & 1;
        pd_Lane_Instanse->buf_Pointer++;
    }

    pd_Lane_Instanse->is_Full = RIO_TRUE;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pd_Lane_Class_Add_Bit_Value 
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Lane_Add_Bit_Value(
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse,
    RIO_SIGNAL_T value
)
{
    RIO_SIGNAL_T val;

    if (pd_Lane_Instanse == NULL)
        return RIO_ERROR;

    /*if the buffer is full delete top bit firstly*/
    if (pd_Lane_Instanse->is_Full == RIO_TRUE)
    {
        pd_Lane_Pass_Bit_Value(pd_Lane_Instanse, &val);
    }
    
    /*put the new received bit into FIFO*/
    pd_Lane_Instanse->lane_Buf[pd_Lane_Instanse->buf_Pointer] = value;

    pd_Lane_Instanse->buf_Pointer++;
    
    if (pd_Lane_Instanse->buf_Pointer > RIO_PCS_PMA_DP_PD_LANE_BUF_BOTTOM)
    {
        pd_Lane_Instanse->is_Full = RIO_TRUE;

        /*tell the mummy about full buffer*/
        pd_Buffer_Is_Full(pd_Lane_Instanse->mum_Instanse, pd_Lane_Instanse);
    };
    
    return RIO_OK;
}


/***************************************************************************
 * Function : pd_Buffer_Is_Full
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Buffer_Is_Full(
    RIO_HANDLE_T handle,
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse
)
{
    unsigned int code_Group_Value = 0;
    RIO_PCS_PMA_CODE_GROUP_T code_Group;


    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse;

    if (handle == NULL || pd_Lane_Instanse == NULL)
        return RIO_ERROR;


    pd_Instanse = (RIO_PL_PCS_PMA_DP_PD_DS_T*)handle;

     /*get lane num and in/out flag*/
    if (pd_Lane_Instanse->self_Number >= RIO_PL_SERIAL_NUMBER_OF_LANES)
        return RIO_ERROR;

    if (pd_Lane_Instanse->is_Out_Lane == RIO_TRUE)
        return RIO_OK;
    else
    {
        pd_Lane_Class_Get_Code_Group_Value(pd_Lane_Instanse, &code_Group_Value);

/*        printf("instance %i   lane: %i   data: %i \n", 
        pd_Instanse->mum_Instanse, pd_Lane_Instanse->self_Number, code_Group_Value);*/

        code_Group.code_Group_Data[pd_Lane_Instanse->self_Number] = code_Group_Value;
        code_Group.is_Running_Disparity_Positive[pd_Lane_Instanse->self_Number] = RIO_TRUE; /*!!!!!!!!!!!!!!!!!!!!*/
        code_Group.lane_Mask = (unsigned char)(1 << pd_Lane_Instanse->self_Number);
 
/*        printf("         instance %i   lane mask: %i   data: %i \n", 
        pd_Instanse->mum_Instanse, code_Group.lane_Mask, code_Group.code_Group_Data[pd_Lane_Instanse->self_Number]);*/

        RIO_Pcs_Pma_Dp_Put_Codegroup(pd_Instanse->mum_Instanse, &code_Group);
    }
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pd_Buffer_Is_Empty
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Buffer_Is_Empty(
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse
)
{
    unsigned int code_Group_Value;
        
    /*check the pointers*/
    if (pd_Lane_Instanse == NULL)
        return RIO_ERROR;

     /*get lane num and in/out flag*/
    if (pd_Lane_Instanse->self_Number >= RIO_PL_SERIAL_NUMBER_OF_LANES)
        return RIO_ERROR;

    /*the lane buffers only can be filled for out site*/
    if (pd_Lane_Instanse->is_Out_Lane == RIO_FALSE)
        return RIO_OK;
    else
    {
        /*get codegroup value*/
        if (pd_Get_Codegroup_Value(pd_Lane_Instanse->mum_Instanse, &code_Group_Value, pd_Lane_Instanse->self_Number) == RIO_ERROR)
            return RIO_ERROR;
        /*put the got codegroup value into buffer*/
        pd_Lane_Class_Set_Code_Group_Value(pd_Lane_Instanse, code_Group_Value);
    }
    return RIO_OK;
}
/***************************************************************************
 * Function : pd_Get_Codegroup_Value
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pd_Get_Codegroup_Value(
    RIO_HANDLE_T handle,
    unsigned int *value,
    unsigned int lane_Num
)
{

    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse;
    unsigned int i;

    if (handle == NULL)
        return RIO_ERROR;


    pd_Instanse = (RIO_PL_PCS_PMA_DP_PD_DS_T*)handle;

    /*check the pointers*/
    if (value == NULL)
        return RIO_ERROR;

    if (pd_Instanse->out_Code_Group_Column.lane_Mask == 0)/*no data ia available*/
    {
        RIO_Pcs_Pma_Dp_Get_Codegroup_Column(pd_Instanse->mum_Instanse, &(pd_Instanse->out_Code_Group_Column));

        for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        {
            /*if there are no data to send (in case of 1x link)    set code group value to 0*/
            if (((pd_Instanse->out_Code_Group_Column.lane_Mask >> i) & 1) == 0)
            {
                pd_Instanse->out_Code_Group_Column.code_Group_Data[i] = 0;
            }
        }
        /*set lane mask to all lanes*/
        pd_Instanse->out_Code_Group_Column.lane_Mask = RIO_PL_ALL_LANES_MASK;
    }

    /*if the data for this lane was already got - this is an error case*/
    if (((pd_Instanse->out_Code_Group_Column.lane_Mask >> lane_Num) & 1) == 0)
        return RIO_ERROR;

    /*pass the codegroup value*/
    *value =  pd_Instanse->out_Code_Group_Column.code_Group_Data[lane_Num];

    /*clean the appropriate bit in the lane mask*/
    pd_Instanse->out_Code_Group_Column.lane_Mask &= ((~(1 << lane_Num)) & RIO_PL_ALL_LANES_MASK);
    return RIO_OK;
}

/***************************************************************************
 * Function : Dp_Pd_Pop_Code_Group_Buffer
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int Dp_Pd_Pop_Code_Group_Buffer(
    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse,  /* callback handle/context */
    RIO_UCHAR_T        lane_ID            /* 0 - for lane 0, 1 - for lane 1, etc. */
)
{
    if (pd_Instanse == NULL)
        return RIO_ERROR;


    if (lane_ID >= RIO_PL_SERIAL_NUMBER_OF_LANES)
        return RIO_ERROR;

    /*clean buffer of appropriate inbound lane*/
    return pd_Lane_Class_Clean_Buffer(&(pd_Instanse->in_Lanes[lane_ID]));
}
/***************************************************************************
 * Function : Dp_Pd_Tx_Clock
 *
 * Description: transmit clock
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Pcs_Pma_Dp_Pd_Tx_Clock(
    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse
)
{
    RIO_SIGNAL_T lanes[RIO_PL_SERIAL_NUMBER_OF_LANES];
    unsigned int i;

    /*check the handle*/
    if (pd_Instanse == NULL)
        return RIO_ERROR;

    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        if (pd_Lane_Pass_Bit_Value(&(pd_Instanse->out_Lanes[i]), &(lanes[i])) == RIO_NO_DATA)
            lanes[i] = RIO_SIGNAL_NO_SIGNAL;
    }

/*    printf("pd_Instanse = %i, tlane 0 = %i\n", pd_Instanse, lanes[0]);*/

    return RIO_Pcs_Pma_Dp_Set_Pins(pd_Instanse->mum_Instanse, lanes[0], lanes[1],    lanes[2], lanes[3]);
}

/***************************************************************************
 * Function : Dp_Pd_Get_Pins
 *
 * Description: receiver clock and pins values accepting
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Pcs_Pma_Dp_Pd_Get_Pins(
    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse, /* pd instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
)
{
    /*check the handle*/
    if ( pd_Instanse == NULL)
        return RIO_ERROR;

/*    printf("pd_Instanse = %i, rlane 0 = %i\n", pd_Instanse, rlane0);*/
    /*put the newly received data to buffers*/
    /*all hardcoded numbers are used only here to indicated the array element*/
    pd_Lane_Add_Bit_Value(&(pd_Instanse->in_Lanes[0]), rlane0);
    pd_Lane_Add_Bit_Value(&(pd_Instanse->in_Lanes[1]), rlane1);
    pd_Lane_Add_Bit_Value(&(pd_Instanse->in_Lanes[2]), rlane2);
    pd_Lane_Add_Bit_Value(&(pd_Instanse->in_Lanes[3]), rlane3);

    return RIO_OK;
}
/***************************************************************************
 * Function : RIO_Pcs_Pma_Dp_Pd_Initialize
 *
 * Description: initialize pd
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Pcs_Pma_Dp_Pd_Initialize(
    RIO_PL_PCS_PMA_DP_PD_DS_T* pd_Instanse
)
{
    
    unsigned int i;
    /*
     * check handle isn't NULL, check parameter set is valid,
     * check that the instance is in reset mode,
     * set link parameters to necessary values and turn the
     * instance to work mode.
     */
    if (pd_Instanse == NULL)
        return RIO_ERROR;


    /*set the mask of out_code group to 0x0 - no data is got yet*/
    pd_Instanse->out_Code_Group_Column.lane_Mask = 0;

    for (i = 0 ; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        pd_Lane_Initialize(&(pd_Instanse->out_Lanes[i]),
            pd_Instanse,
            RIO_TRUE,
            i
        );
        pd_Lane_Initialize(&(pd_Instanse->in_Lanes[i]),
            pd_Instanse,
            RIO_FALSE,
            i
        );
    }
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pd_Lane_Initialize
 *
 * Description: initialize pd
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pd_Lane_Initialize(
    RIO_HANDLE_T  handle,
    RIO_HANDLE_T mum_Instance,
    RIO_BOOL_T is_Out,
    unsigned int self_Number
)
{
    RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T *pd_Lane_Instanse;

    if (handle == NULL)
        return RIO_ERROR;

    pd_Lane_Instanse = (RIO_PL_PCS_PMA_DP_PD_LANE_CLASS_T*)handle; 

    pd_Lane_Instanse->buf_Pointer = RIO_PCS_PMA_DP_PD_LANE_BUF_TOP;
    pd_Lane_Instanse->is_Full = RIO_FALSE;
    pd_Lane_Instanse->is_Out_Lane = is_Out;
    pd_Lane_Instanse->self_Number = (unsigned char)self_Number;
    pd_Lane_Instanse->mum_Instanse = mum_Instance;
    
    return RIO_OK;
}
