/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_dp.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Physical layer transmitter
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include <string.h>

#include "rio_pcs_pma_dp.h"
#include "rio_pcs_pma_dp_pd.h"
#include "rio_pcs_pma_dp_cl.h"
#include "rio_pcs_pma_dp_sb.h"

/* FOR DEBUG */
/*#define DEBUG_MESSAGING*/

/*
 * Global variable for counting number of 32-Bit Transmitter/Receiver 
 * instantiations. Using for proper instance number assignement
 */
static unsigned long rio_Pl_Pcs_Pma_Dp_Inst_Count = 0;

static int dp_Delete_Instance(
    RIO_HANDLE_T          handle
    );

static int dp_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
    );

static int dp_Clock_Edge(RIO_HANDLE_T handle);

static int dp_Start_Reset(
    RIO_HANDLE_T handle
    );

static int dp_Initialize(
    RIO_HANDLE_T         handle,
    RIO_PCS_PMA_PARAM_SET_T        *param_Set
    );

static int dp_Pop_Codegroup(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T    lane_ID
    );

static int dp_Get_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

static int dp_Put_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

static int dp_Get_Codegroup_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T  *codegroup        /* pointer to codegroup data structure */
);

static int dp_Put_Codegroup(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T  *codegroup        /* pointer to codegroup data structure */
);

static int dp_Pop_Char_Buffer(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_UCHAR_T                  lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
);



/***************************************************************************
 * Function : RIO_PCS_PMA_DP_Create_Instance
 *
 * Description: Create new physical layer instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_DP_Create_Instance(
    RIO_HANDLE_T            *handle,
    RIO_HANDLE_T            mum_Instanse
    )
{
    unsigned int i;
    
    /* handle to structure will allocated*/
    RIO_PL_PCS_PMA_DP_DS_T *pl_Pcs_Pma_Dp_Instance = NULL;

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for physical layer 
     * data structure
     */
    if (handle == NULL )
        return RIO_ERROR;

    /* clear handle */
    *handle = NULL;

    /* allocate instanse */
    if ((pl_Pcs_Pma_Dp_Instance = malloc(sizeof(RIO_PL_PCS_PMA_DP_DS_T))) == NULL)
        return RIO_ERROR;

    /* copy the pointer to the upper block */
    pl_Pcs_Pma_Dp_Instance->mum_Instanse = mum_Instanse;

    /*
     * clear all pointers
     */
    pl_Pcs_Pma_Dp_Instance->interfaces.get_Character_Column = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.get_Codegroup_Column = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.get_Granule = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.put_Character_Column = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.put_Codegroup = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.put_Granule = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.rio_Serial_Set_Pins = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.rio_User_Msg = NULL;

    pl_Pcs_Pma_Dp_Instance->interfaces.character_Level_Context = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.codegroup_Level_Context = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.granule_Level_Context = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.lp_Serial_Interface_Context = NULL;
    pl_Pcs_Pma_Dp_Instance->interfaces.user_Msg_Context = NULL;

    /*set all internal pointers*/
    pl_Pcs_Pma_Dp_Instance->pins_Driver_Data.mum_Instanse = pl_Pcs_Pma_Dp_Instance;
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {        
        pl_Pcs_Pma_Dp_Instance->pins_Driver_Data.in_Lanes[i].mum_Instanse = &(pl_Pcs_Pma_Dp_Instance->pins_Driver_Data);
        pl_Pcs_Pma_Dp_Instance->pins_Driver_Data.out_Lanes[i].mum_Instanse = &(pl_Pcs_Pma_Dp_Instance->pins_Driver_Data);
    }
    pl_Pcs_Pma_Dp_Instance->coding_Logic_Data.mum_Instanse = pl_Pcs_Pma_Dp_Instance;
    pl_Pcs_Pma_Dp_Instance->streaming_Block_Data.mum_Instanse = pl_Pcs_Pma_Dp_Instance;
    
    /*
     * increase count of TxRx instances and set
     * up the number of instance, turn link into reset
     */
    *handle = pl_Pcs_Pma_Dp_Instance;
    pl_Pcs_Pma_Dp_Instance->instance_Number = ++rio_Pl_Pcs_Pma_Dp_Inst_Count;
    
    pl_Pcs_Pma_Dp_Instance->is_Ctray_Registered = RIO_FALSE;
    pl_Pcs_Pma_Dp_Instance->is_In_Reset = RIO_FALSE;
    dp_Start_Reset(pl_Pcs_Pma_Dp_Instance);

    return RIO_OK;

}

/***************************************************************************
 * Function : RIO_PCS_PMA_DP_Get_Function_Tray
 *
 * Description: Export function tray, set pointers to corresponding 
 *              functions entry point
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_DP_Get_Function_Tray(
    RIO_PCS_PMA_DP_FTRAY_T *ftray             
    )
{
    /*
     * check pointers to not NULL values and complete function tray
     * with valid entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->rio_Link_Delete_Instance = dp_Delete_Instance;
    ftray->initialize = dp_Initialize;
    ftray->start_Reset = dp_Start_Reset;
    ftray->get_Character_Column = dp_Get_Character_Column;
    ftray->get_Codegroup_Column = dp_Get_Codegroup_Column;

    ftray->pop_Char_Buffer = dp_Pop_Char_Buffer;
    ftray->pop_Codegroup_Buffer = dp_Pop_Codegroup;

    ftray->put_Character_Column = dp_Put_Character_Column;
    ftray->put_Codegroup = dp_Put_Codegroup;
    ftray->rio_Serial_Get_Pins = dp_Get_Pins;

    ftray->rio_Link_Delete_Instance = dp_Delete_Instance;
    ftray->clock_Edge = dp_Clock_Edge;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PCS_PMA_DP_Bind_Instance
 *
 * Description: bind the instance with the environment, load entry point from
 *              callback tray to internal structure
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_DP_Bind_Instance(
    RIO_HANDLE_T                        handle,
    RIO_PCS_PMA_DP_CALLBACK_TRAY_T        *ctray   
    )
{
    /* 
     * convert void pointer to pointer to instance structure and 
     * check all callbacks pointers to be not NULL and 
     * store external callbacks with environment's context
     * from callback tray in instance's data structure
     */
    RIO_PL_PCS_PMA_DP_DS_T* instanse = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    if (ctray == NULL || 
        handle == NULL)
        return RIO_ERROR;
    
    instanse->interfaces = *ctray;
    instanse->is_Ctray_Registered = RIO_TRUE;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : dp_Delete_Instance
 *
 * Description: deletes instance of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int dp_Delete_Instance(
    RIO_HANDLE_T          handle
    )
{ 
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance = (RIO_HANDLE_T)handle;

    /* check pointers */
    if (dp_Instance == NULL) 
        return RIO_ERROR;

    if (dp_Instance != NULL)
        free(dp_Instance);
   
    return RIO_OK; 
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Display_Msg
 *
 * Description: deletes instance of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Display_Msg(
    RIO_HANDLE_T                handle,
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
)
{ 
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance = (RIO_HANDLE_T)handle;

    /* check pointers */
    if (dp_Instance == NULL) 
        return RIO_ERROR;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Display_Msg: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
               dp_Instance->interfaces.user_Msg_Context,
               RIO_PL_SERIAL_MSG_ERROR_MSG, "PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Display_Msg: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

#ifdef DEBUG_MESSAGING
    dp_Instance->interfaces.rio_User_Msg(
        dp_Instance->interfaces.user_Msg_Context,
        msg_Type,
        string
    );
#endif /* DEBUG_MESSAGING */
#ifndef DEBUG_MESSAGING
    {
        char *sub_String;
        char str_PCS_PMA[40]; /* size of the PCS/PMA prefix (including instance_Number) */

        sprintf(str_PCS_PMA, "PCS_PMA #%lu: ", dp_Instance->instance_Number);
        sub_String = (char *)malloc(strlen(string) + strlen(str_PCS_PMA) + 10); /* the extra space is reserved */
        strcpy(sub_String, str_PCS_PMA);
        strcpy(sub_String + strlen(sub_String), strchr(string, ':') + 2);  /* +2 means that pointer to the character */
                                                                           /* next to ':' (a space-char is also skipped) */
        /* error messages printing via user_Msg callback */
        dp_Instance->interfaces.rio_User_Msg(
            dp_Instance->interfaces.user_Msg_Context,
                msg_Type,
                sub_String
            );
    }
#endif /* !DEBUG_MESSAGING */
/*    printf("DP: ERROR/WARNING: %s\n", string); */
    
    /* for DEBUGGING needs */
    if (RIO_PL_SERIAL_MSG_ERROR_MSG == msg_Type)
    {
        int i;

        i = 0;
    }

    return RIO_OK; 
}

/***************************************************************************
 * Function : set_Pins
 *
 * Description: stab to safly invoke rio_Ph_Set_Pins callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_Pcs_Pma_Dp_Set_Pins(
    RIO_HANDLE_T handle, /* enviroment's context */
    RIO_SIGNAL_T tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3       /* transmit data on lane 3*/
    )
{
    RIO_PL_PCS_PMA_DP_DS_T* dp_Instance;

    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /*
     * check that using pointers is not NULL and invoke 
     * the callback
     */
    if (dp_Instance->interfaces.rio_Serial_Set_Pins == NULL)
        return RIO_ERROR;

    return dp_Instance->interfaces.rio_Serial_Set_Pins(
        dp_Instance->interfaces.lp_Serial_Interface_Context,
        tlane0,
        tlane1,
        tlane2,
        tlane3);

}
/***************************************************************************
 * Function : dp_Get_Pins
 *
 * Description: stab to safly invoke rio_Ph_Set_Pins callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
    )
{
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;

    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
        return RIO_OK;
    }

    return RIO_Pcs_Pma_Dp_Pd_Get_Pins(&(dp_Instance->pins_Driver_Data), rlane0, rlane1, rlane2, rlane3);
}

/***************************************************************************
 * Function :dp_Clock_Edge
 *
 * Description: transmit clock
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Clock_Edge(RIO_HANDLE_T handle)
{
    RIO_PL_PCS_PMA_DP_DS_T* dp_Instance;

    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
        return RIO_OK;
    }

    return RIO_Pcs_Pma_Dp_Pd_Tx_Clock(&(dp_Instance->pins_Driver_Data));
}
/***************************************************************************
 * Function : RIO_Pcs_Pma_Dp_Put_Codegroup
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_Pcs_Pma_Dp_Put_Codegroup(
    RIO_HANDLE_T handle, /* enviroment's context */
    RIO_PCS_PMA_CODE_GROUP_T *code_Group
    )
{
    RIO_PL_PCS_PMA_DP_DS_T* dp_Instance;
    RIO_UCHAR_T i;

    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    if (dp_Instance->interfaces.put_Codegroup == NULL)
        return RIO_ERROR;

    /* 
     * setting running disparity with which this codegroup
     * will be further decoded in Coding Logic block
     * (this is needed for the corresponding user hook in PCS/PMA)
     */
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
    {
        if ((code_Group->lane_Mask >> i) & 1)
        {
            code_Group->is_Running_Disparity_Positive[i] =
                dp_Instance->coding_Logic_Data.in_Lanes[i].is_Running_Disparity_Positive;
        }
        else
        {
            code_Group->code_Group_Data[i] = 0;
            code_Group->is_Running_Disparity_Positive[i] = 0;
        }
    }

    return dp_Instance->interfaces.put_Codegroup(
        dp_Instance->interfaces.codegroup_Level_Context,
        code_Group);

}

/***************************************************************************
 * Function : RIO_Pcs_Pma_Dp_Get_Codegroup_Column
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_Pcs_Pma_Dp_Get_Codegroup_Column(
    RIO_HANDLE_T handle, /* enviroment's context */
    RIO_PCS_PMA_CODE_GROUP_T *code_Group_Column
    )
{
    RIO_PL_PCS_PMA_DP_DS_T* dp_Instance;

    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    if (dp_Instance->interfaces.get_Codegroup_Column == NULL)
        return RIO_ERROR;

    return dp_Instance->interfaces.get_Codegroup_Column(
        dp_Instance->interfaces.codegroup_Level_Context,
        code_Group_Column);

}

/***************************************************************************
 * Function : dp_Initialize
 *
 * Description: initialize the transmitter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Initialize(
    RIO_HANDLE_T         handle,
    RIO_PCS_PMA_PARAM_SET_T        *param_Set
    )
{
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance; /* instanse handle*/
    int i;
    
    /* check pointers */
    if (handle == NULL || param_Set == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (!dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Initialize: is called while not in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: dp_Initialize: is called while not in reset\n");
#endif
        return RIO_ERROR;
    }

    if (!dp_Instance->is_Ctray_Registered)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Initialize: is called while ctray is not registered\n");
#endif
        return RIO_ERROR;
    }

    for (i = 0; i < RIO_PCS_PMA_DP_FLAGS_AMOUNT; i++)
        dp_Instance->is_Flag_Asserted[i] = RIO_FALSE;


    /* initialize Pins Driver */
    RIO_Pcs_Pma_Dp_Pd_Initialize(&(dp_Instance->pins_Driver_Data));
    
    /* initialize Coding Logic */
    RIO_PCS_PMA_DP_CL_Initialize(&dp_Instance->coding_Logic_Data);

    /* initialize Coding Logic */
    RIO_PCS_PMA_DP_SB_Initialize(&dp_Instance->streaming_Block_Data);

    /*set off the reset flag*/
    dp_Instance->is_In_Reset = RIO_FALSE;
    return RIO_OK;
}

/***************************************************************************
 * Function : dp_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Start_Reset(
    RIO_HANDLE_T handle
    )
{
    /* instanse handle*/
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check work state */
    if (dp_Instance->is_In_Reset == RIO_TRUE)
        return RIO_OK;
    
    /* modify state to be _in_reset_ */
    dp_Instance->is_In_Reset = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : dp_Pop_Codegroup
 *
 * Description: pop the godegroup buffer for appropriate lane
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Pop_Codegroup(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T    lane_ID
    )
{
    /* instanse handle*/
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Pop_Codegroup: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: dp_Pop_Codegroup: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    /*call the pins driver*/
    return Dp_Pd_Pop_Code_Group_Buffer(&(dp_Instance->pins_Driver_Data), lane_ID);
}

/***************************************************************************
 * Function : dp_Get_Character_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Get_Character_Column(
    RIO_CONTEXT_T              handle,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    /* instanse handle*/
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Get_Character_Column: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: dp_Get_Character_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }
    
    return rio_PCS_PMA_DP_Get_Character_Column(dp_Instance, character);
}

/***************************************************************************
 * Function : dp_Put_Character_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Put_Character_Column(
    RIO_CONTEXT_T              handle,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    /* instanse handle*/
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Put_Character_Column: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: dp_Put_Character_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }
    
    /*call the pins driver*/
    return rio_PCS_PMA_DP_Put_Character_Column(dp_Instance, character);
}

/***************************************************************************
 * Function : dp_Get_Codegroup_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Get_Codegroup_Column(
    RIO_CONTEXT_T              handle,
    RIO_PCS_PMA_CODE_GROUP_T  *codegroup        /* pointer to codegroup data structure */
)
{
    /* instanse handle*/
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Get_Codegroup_Column: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: dp_Get_Codegroup_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }
    
    return rio_PCS_PMA_DP_Get_Codegroup_Column(dp_Instance, codegroup);
}

/***************************************************************************
 * Function : dp_Put_Codegroup
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Put_Codegroup(
    RIO_CONTEXT_T              handle,
    RIO_PCS_PMA_CODE_GROUP_T  *codegroup        /* pointer to codegroup data structure */
)
{
    /* instanse handle*/
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Put_Codegroup: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: dp_Put_Codegroup: is called while in reset\n");
#endif
        return RIO_ERROR;
    }
    
    return rio_PCS_PMA_DP_Put_Codegroup(dp_Instance, codegroup);
}

/***************************************************************************
 * Function : dp_Pop_Char_Buffer
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int dp_Pop_Char_Buffer(
    RIO_CONTEXT_T              handle,
    RIO_UCHAR_T                  lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
)
{
    /* instanse handle*/
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T*)handle;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA DP: ERROR: dp_Pop_Char_Buffer: is called while in reset\n");
#else
        dp_Instance->interfaces.rio_User_Msg(
                dp_Instance->interfaces.user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: dp_Pop_Char_Buffer: is called while in reset\n");
#endif
        return RIO_ERROR;
    }
    
    return rio_PCS_PMA_DP_Pop_Char_Buffer(dp_Instance, lane_Mask);
}


/*****************************************************************************/
