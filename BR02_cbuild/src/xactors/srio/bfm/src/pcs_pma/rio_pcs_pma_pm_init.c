/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_pm_init.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_pm_init.h"
#include "rio_pcs_pma_pm_align.h"
#include "rio_pcs_pma_pm_ds.h"
#include "rio_pcs_pma_pm.h"
#include "rio_pcs_pma_pm_interface.h"

#include "rio_gran_type.h"
#include "rio_serial_gran_type.h"

#include <string.h>
#include <stdio.h>

/* FOR DEBUG */
/*#define DEBUG_ALL_GENERATION*/

/* FOR DEBUG */
#ifdef DEBUG_ALL_GENERATION
#define DEBUG_A_GENERATION
#define _CUSTOM_DEBUG
#endif /* DEBUG_ALL_GENERATION */

#define GO_TO_NORMAL_OPERATION_STATE \
{ \
    pm_Instance->trx_Requests_Enabled = RIO_TRUE; \
    rio_PCS_PMA_PM_Display_Msg( \
        pm_Instance, \
        RIO_PL_SERIAL_MSG_NOTE_MSG, \
        "rio_PCS_PMA_PM_Put_Codegroup: port is in the normal operation state (ready to process transmit requests)" \
    ); \
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Granule
 *
 * Description: requests a next granule for transmission through the link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Get_Granule(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle_Context;
    int result;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;
    unsigned long is_True_4x;
    RIO_UCHAR_T tmp_Buf[4]; /*bug-132*/
    
    if (pm_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Get_Granule: is called while in reset\n");
#else
	if(pm_Instance->ctray)
		pm_Instance->ctray->rio_User_Msg(pm_Instance->ctray->user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG, "PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Get_Granule: is called while in reset\n");
#endif
        return RIO_ERROR;
    }
    

    pm_Instance->ctray->rio_Get_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_IS_PORT_TRUE_4X,
        &is_True_4x,
        &indicator_Parameter
    );

#ifdef DEBUG_ALL_GENERATION
    if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
        if (pm_Instance->before_Status_Symbol_Counter <= pm_Instance->comp_Seq_Size_Next * PCS_PMA_PM__STATUS_SYMBOL_SIZE)
            printf("(%i)ITERATION: %i:    A: %i, C: %i, 2!S: %i,  PSB: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter,
                pm_Instance->is_Packet_Streaming_Broken
            );
#endif /* DEBUG_ALL_GENERATION */


    
    /* set normal op. state in case of status_sim-gen disabled */
    if (
        pm_Instance->trx_Requests_Enabled != RIO_TRUE &&
        (
            pm_Instance->init_Status &&
            pm_Instance->is_Status_Sym_Gen_On == RIO_FALSE
        )
    )
        GO_TO_NORMAL_OPERATION_STATE;

    /* 
     * check for the necessity of sending the STATUS symbol
     * (possible overlapping with comp.seq is also resolved here)
     */
    if (
        pm_Instance->is_Status_Sym_Gen_On &&
        (
            pm_Instance->init_Status ||
            !pm_Instance->is_Init_FSM_On
        ) &&
        (
            (
                0 == pm_Instance->before_Status_Symbol_Counter &&
                !(
                    pm_Instance->is_Comp_Seq_Gen_On &&
                    0 == pm_Instance->before_Comp_Seq_Counter &&
                    pm_Instance->is_Packet_Streaming_Broken
                )
            ) ||
            (
                (
                    pm_Instance->is_Comp_Seq_Gen_On &&
                    0 == pm_Instance->before_Comp_Seq_Counter &&
                    (
                        !pm_Instance->is_Packet_Streaming_Broken ||
                        pm_Instance->is_Comp_Seq_Delayed_For_Status
                    )
                ) &&
                (
                    (
                        is_True_4x &&
                        pm_Instance->before_Status_Symbol_Counter <
                        pm_Instance->comp_Seq_Size_Next * PCS_PMA_PM__STATUS_SYMBOL_SIZE
                    ) ||
                    (
                        !is_True_4x &&
                        pm_Instance->before_Status_Symbol_Counter < pm_Instance->comp_Seq_Size_Next
                    )
                )
            )
        )
    )
    /* this is done only if the Status Symbol Generator is turned ON */
    {
        /* reset the counter */
        pm_Instance->before_Status_Symbol_Counter =
            pm_Instance->parameters_Set->status_Sym_Rate;
        
        pm_Instance->is_Comp_Seq_Delayed_For_Status = RIO_FALSE;
        
#ifdef DEBUG_ALL_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            printf("(%i)ITERATION: %i:    A: %i, C: %i, !S: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter
            );
#endif /* DEBUG_ALL_GENERATION */
        /* checking overlapping with comp.seq. */
        if (
            is_True_4x &&                
            (unsigned int)(pm_Instance->before_Status_Symbol_Counter / PCS_PMA_PM__STATUS_SYMBOL_SIZE) >=
                pm_Instance->before_Comp_Seq_Counter &&
            (unsigned int)(pm_Instance->before_Status_Symbol_Counter / PCS_PMA_PM__STATUS_SYMBOL_SIZE) <=
                pm_Instance->before_Comp_Seq_Counter + pm_Instance->comp_Seq_Size_Next - 1
        )
        {
            if (pm_Instance->before_Comp_Seq_Counter >= PCS_PMA_PM__STATUS_SYMBOL_SIZE)
            {
                pm_Instance->before_Status_Symbol_Counter =
                    (pm_Instance->before_Comp_Seq_Counter - PCS_PMA_PM__STATUS_SYMBOL_SIZE) *
                    PCS_PMA_PM__STATUS_SYMBOL_SIZE;
                pm_Instance->before_Status_Symbol_Counter =
                    ((unsigned int)(pm_Instance->before_Status_Symbol_Counter / PCS_PMA_PM__STATUS_SYMBOL_SIZE)) *
                    PCS_PMA_PM__STATUS_SYMBOL_SIZE;
            }
            else
                pm_Instance->before_Status_Symbol_Counter = 0;
        }
        if (
            !is_True_4x &&
            pm_Instance->before_Status_Symbol_Counter + PCS_PMA_PM__STATUS_SYMBOL_SIZE - 1 >=
                pm_Instance->before_Comp_Seq_Counter &&
            pm_Instance->before_Status_Symbol_Counter <= pm_Instance->before_Comp_Seq_Counter +
                pm_Instance->comp_Seq_Size_Next - 1
        )
        {
            if (pm_Instance->before_Comp_Seq_Counter >= PCS_PMA_PM__STATUS_SYMBOL_SIZE)
            {
                pm_Instance->before_Status_Symbol_Counter = pm_Instance->before_Comp_Seq_Counter - PCS_PMA_PM__STATUS_SYMBOL_SIZE;
                pm_Instance->before_Status_Symbol_Counter =
                    ((unsigned int)(pm_Instance->before_Status_Symbol_Counter / PCS_PMA_PM__STATUS_SYMBOL_SIZE)) *
                    PCS_PMA_PM__STATUS_SYMBOL_SIZE;
            }
            else
                pm_Instance->before_Status_Symbol_Counter = 0;
        }
#ifdef DEBUG_ALL_GENERATION
        if (DEBUG_INSTANCE_NUMBER == pm_Instance->instance_Number)
            printf("(%i)ITERATION: %i:    A: %i, C: %i, !!S: %i\n",
                pm_Instance->instance_Number,
                pm_Instance->debug_Iteration_Counter,
                pm_Instance->idle_Non_A_Counter,
                pm_Instance->before_Comp_Seq_Counter,
                pm_Instance->before_Status_Symbol_Counter
            );
#endif /* DEBUG_ALL_GENERATION */

        /* 
         * form the symbol
         */
        granule->is_Data = RIO_FALSE;
        granule->granule_Struct.stype0 = RIO_PL_SERIAL_GR_IDLE_STYPE0; /* status symbol */

        /* read the ackID status */
        pm_Instance->ctray->rio_Get_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_INBOUND_ACKID,
            &indicator_Value,
            &indicator_Parameter
        );
        granule->granule_Struct.parameter0 = (RIO_UCHAR_T)(indicator_Value & RIO_SYMBOL_PARAMETER0_MASK);

        /* read the buf_status */
        pm_Instance->ctray->rio_Get_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_FLOW_CONTROL_MODE,
            &indicator_Value,
            &indicator_Parameter
        );
        if (RIO_FALSE == indicator_Value)
            granule->granule_Struct.parameter1 = (RIO_UCHAR_T)(RIO_PL_BUF_STATUS_MAX);
        else
        {
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_OUR_BUF_STATUS,
                &indicator_Value,
                &indicator_Parameter
            );
            granule->granule_Struct.parameter1 = (RIO_UCHAR_T)(indicator_Value & RIO_SYMBOL_PARAMETER1_MASK);
        }

/*bug-132*/
 	tmp_Buf[0] = 0;
	tmp_Buf[1] = ((granule->granule_Struct.stype0 & 7) << 5) |
	                              (granule->granule_Struct.parameter0 & 0x1F);
	tmp_Buf[2] = ((granule->granule_Struct.parameter1 & 0x1F) << 3) ;
	tmp_Buf[3] = (granule->granule_Struct.cmd & 7) << 5;



          pm_Instance->ctray->rio_Symbol_To_Send(
                        pm_Instance->ctray->rio_Regs_Context,
                        tmp_Buf,
                        PCS_PMA_PM__STATUS_SYMBOL_SIZE);


	   /*put data after user change back to structure*/
                granule->granule_Struct.stype0 = tmp_Buf[1] >> 5;
                granule->granule_Struct.parameter0 = tmp_Buf[1] & 0x1F;
                granule->granule_Struct.parameter1 = tmp_Buf[2] >> 3;
                granule->granule_Struct.cmd = tmp_Buf[3] >> 5;

/*bug-132*/

        granule->granule_Struct.stype1 = RIO_PL_SERIAL_GR_NOP_STYPE1;
/*        granule->granule_Struct.cmd = 0;*/
        granule->granule_Struct.is_Crc_Valid = RIO_FALSE;

        result = RIO_OK;

        /* going to the normal operation mode */
        if (pm_Instance->snd_Status_Symbols_Counter < pm_Instance->parameters_Set->status_Symbols_To_Send)
        /* necessary amount of sent status symbols after initialization have already passed */
            pm_Instance->snd_Status_Symbols_Counter++;

        if (
            pm_Instance->trx_Requests_Enabled != RIO_TRUE &&
            (
                pm_Instance->rcv_Status_Symbols_Counter >= pm_Instance->parameters_Set->status_Symbols_To_Receive &&
                pm_Instance->snd_Status_Symbols_Counter >= pm_Instance->parameters_Set->status_Symbols_To_Send
            )
        )
        {
            GO_TO_NORMAL_OPERATION_STATE;
        }
    }
    else
    if (
        0 == pm_Instance->before_Comp_Seq_Counter &&
        pm_Instance->is_Packet_Streaming_Broken &&
        pm_Instance->is_Comp_Seq_Gen_On
    )
    {
        result = RIO_NO_DATA;
    }
    else
    {
        if (
            pm_Instance->trx_Requests_Enabled ||
            !pm_Instance->is_Init_FSM_On
        )
        /* if INIT FSM is OFF this is done anyway */
        {
            result = pm_Instance->ctray->get_Granule(
                pm_Instance->ctray->granule_Level_Context,
                granule
            );
        }
        else
            result = RIO_NO_DATA;
    }

    if (RIO_NO_DATA == result)
    /* PL returned nothing */
        return RIO_NO_DATA;
    else
    /* granule is also returned to the DP Streaming Block */
        return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Put_Granule
 *
 * Description: notifies on reception of granule
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Put_Granule(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
)
{
    RIO_PCS_PMA_PM_DS_T *pm_Instance = (RIO_PCS_PMA_PM_DS_T *)handle_Context;
    RIO_BOOL_T    lp_Serial_Is_1x;
    RIO_BOOL_T    is_Force_1x_Mode;
    RIO_BOOL_T    is_Force_1x_Mode_Lane_0;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;
    int result;
    
    if (pm_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pm_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Put_Granule: is called while in reset\n");
#else
		pm_Instance->ctray->rio_User_Msg(pm_Instance->ctray->user_Msg_Context,
                RIO_PL_SERIAL_MSG_ERROR_MSG, "PCS/PMA PM: ERROR: rio_PCS_PMA_PM_Put_Granule: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    rio_PCS_PMA_PM_Get_Port_Control(
        pm_Instance,
        &lp_Serial_Is_1x,
        &is_Force_1x_Mode,
        &is_Force_1x_Mode_Lane_0
    );

    if (pm_Instance->is_Init_FSM_On)
    /* INIT FSM is turned ON */
    {
        if (
            !lp_Serial_Is_1x
        )
        /* 1x4x link */
        {
            rio_PCS_PMA_PM_Exec_Init_1x4x_FSM(
                pm_Instance,
                lp_Serial_Is_1x,
                is_Force_1x_Mode,
                is_Force_1x_Mode_Lane_0
            );

            /* check if this is just a call to drive the INIT FSM */
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_SUPPRESS_HOOK,
                &indicator_Value,
                &indicator_Parameter
            );
            if (RIO_TRUE == indicator_Value)
                return RIO_OK;
        }
        else
        {
            rio_PCS_PMA_PM_Exec_Init_1x_FSM(
                pm_Instance
            );

            /* check if this is just a call to drive the INIT FSM */
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_SUPPRESS_HOOK,
                &indicator_Value,
                &indicator_Parameter
            );
            if (RIO_TRUE == indicator_Value)
                return RIO_OK;
        }

        if (!pm_Instance->init_Status)
        /* module is NOT initialized yet =>  granule is not sent to PL */
            return RIO_OK;
    }

    /* 
     * module is assumed to be initialized here
     * (in case INIT FSM is turned ON)
     */

    pm_Instance->ctray->rio_Get_State_Flag(
        pm_Instance->ctray->rio_Regs_Context,
        pm_Instance->mum_Instanse,
        RIO_ARE_ALL_IDLES_IN_GRANULE,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
        return RIO_NO_DATA;
    
    /* granule is sent to PL anyway (even if it's corrupted) */
    result = pm_Instance->ctray->put_Granule(
        pm_Instance->ctray->granule_Level_Context,
        granule
    );
    if (!pm_Instance->is_Init_FSM_On)
    /* INIT FSM is turned OFF */
    {
        return result;
    }

    if (
        !granule->is_Data &&
        !granule->granule_Struct.is_Crc_Valid
    )
    /* CRC error occured in the current control symbol */
        if (pm_Instance->rcv_Status_Symbols_Counter < pm_Instance->parameters_Set->status_Symbols_To_Receive)
        {
            pm_Instance->rcv_Status_Symbols_Counter = 0;
            pm_Instance->snd_Status_Symbols_Counter = 0;
            pm_Instance->trx_Requests_Enabled = RIO_FALSE;
            return RIO_OK;
        }

    /* set normal op. state in case of status_sim-gen disabled */
    if (
        pm_Instance->trx_Requests_Enabled != RIO_TRUE &&
        (
            pm_Instance->init_Status &&
            pm_Instance->is_Status_Sym_Gen_On == RIO_FALSE
        )
    )
        GO_TO_NORMAL_OPERATION_STATE;
        
    if (
        !granule->is_Data &&
        RIO_PL_SERIAL_GR_IDLE_STYPE0 == granule->granule_Struct.stype0 /* status symbol */
    )
    /* status control symbol */
    {
        if (pm_Instance->rcv_Status_Symbols_Counter >= pm_Instance->parameters_Set->status_Symbols_To_Receive)
        /* necessary amount of status symbols after initialization have already passed */
            return RIO_OK;
        pm_Instance->rcv_Status_Symbols_Counter++;

        pm_Instance->ctray->rio_Set_State_Flag(
            pm_Instance->ctray->rio_Regs_Context,
            pm_Instance->mum_Instanse,
            RIO_PARTNER_BUF_STATUS,
            granule->granule_Struct.parameter1, /* buf_status */
            0 /* don't care */
        );

        if (1 == pm_Instance->rcv_Status_Symbols_Counter)
        /* first status symbol after the initialization */
        {
            /* it is necessary to determine flow control mode here           */
            /* in case device is set to receiver control buf_Status of first */
            /* status symbol does not matter in other case transmitter flow  */
            /* control may be downgraded to receiver flow control            */
            pm_Instance->ctray->rio_Get_State_Flag(
                pm_Instance->ctray->rio_Regs_Context,
                pm_Instance->mum_Instanse,
                RIO_FLOW_CONTROL_MODE,
                &indicator_Value,
                &indicator_Parameter
            );

            if ( RIO_TRUE == indicator_Value && RIO_PL_BUF_STATUS_MAX == granule->granule_Struct.parameter1 )
                /* instance was set to transmitter control originally */
                /* downgrade the model to receiver control                          */
                /* set parameters to allow PL determining control-flow control mode */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_FLOW_CONTROL_MODE,
                    RIO_FALSE,  /* receiver-controlled flow control mode */
                    0 /* don't care */
                );
        }
        else
        if (
            pm_Instance->trx_Requests_Enabled != RIO_TRUE &&
            (
                pm_Instance->rcv_Status_Symbols_Counter >= pm_Instance->parameters_Set->status_Symbols_To_Receive &&
                pm_Instance->snd_Status_Symbols_Counter >= pm_Instance->parameters_Set->status_Symbols_To_Send
            )
        )
        {
            GO_TO_NORMAL_OPERATION_STATE;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Exec_Init_1x_FSM
 *
 * Description: executes the PCS/PMA PM init FSM for 1x link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Exec_Init_1x_FSM(
    RIO_PCS_PMA_PM_DS_T   *pm_Instance
)
{
    while (1)
    {
        if (PCS_PMA_PM__INIT_FSM__SILENT == pm_Instance->init_State)
        {
            if (pm_Instance->init_Status)
            {
                pm_Instance->init_Status = RIO_FALSE;

                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Init_1x_FSM: INITIALIZATION is lost {Info #08}"
                );
                rio_PCS_PMA_PM_Reset_Port_Regs(pm_Instance);
            }
            pm_Instance->is_Lane_OE[0] = RIO_FALSE;
            pm_Instance->rcv_Status_Symbols_Counter = 0;
            pm_Instance->snd_Status_Symbols_Counter = 0;
            pm_Instance->trx_Requests_Enabled = RIO_FALSE;
            pm_Instance->silence_Timer = pm_Instance->parameters_Set->silence_Period;
            pm_Instance->before_Status_Symbol_Counter =
                pm_Instance->parameters_Set->status_Sym_Rate - PCS_PMA_PM__STATUS_SYMBOL_SIZE;
            PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT_PART_2);
        }

        if (PCS_PMA_PM__INIT_FSM__SILENT_PART_2 == pm_Instance->init_State)
        {
            if (0 == pm_Instance->silence_Timer)
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SEEK);
            else
                break;
        }

        if (PCS_PMA_PM__INIT_FSM__SEEK == pm_Instance->init_State)
        {
            pm_Instance->is_Lane_OE[0] = RIO_TRUE;
            if (RIO_TRUE == pm_Instance->sync_Status[0])
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__1X_MODE);
            else
                break;
        }

        if (PCS_PMA_PM__INIT_FSM__1X_MODE == pm_Instance->init_State)
        {
            if (!pm_Instance->init_Status)
            {
                pm_Instance->init_Status = RIO_TRUE;
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    RIO_TRUE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_IS_PORT_TRUE_4X,
                    RIO_FALSE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_COPY_TO_LANE2,
                    RIO_FALSE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED_PORT_WIDTH,
                    RIO_INITIALIZED_PORT_WIDTH__SINGLE_LANE_0,
                    0 /* don't care */
                );
                /* reset "INVALID character" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );
                /* reset "CRC Error" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_BAD_CONTROL
                );
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Init_1x_FSM: 1x port has been INITIALIZED {Info #04}"
                );
                break;
            }
            else
            if (RIO_TRUE != pm_Instance->sync_Status[0])
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
            else
            {
                unsigned long indicator_Value;
                unsigned int indicator_Parameter;

                pm_Instance->ctray->rio_Get_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    &indicator_Value,
                    &indicator_Parameter
                );
                if (RIO_TRUE != indicator_Value)
                /* "force_reinit" siganl is asserted */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
                else
                    break;
            }
        }

        if (pm_Instance->init_State > PCS_PMA_PM__INIT_FSM__1X_MODE)
        {
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Exec_Init_1x_FSM: unknown state for 1x INIT FSM"
            );
            return RIO_ERROR;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Exec_Init_1x4x_FSM
 *
 * Description: executes the PCS/PMA PM init FSM for 1x/4x link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_PM_Exec_Init_1x4x_FSM(
    RIO_PCS_PMA_PM_DS_T     *pm_Instance,
    RIO_BOOL_T              lp_Serial_Is_1x,
    RIO_BOOL_T              is_Force_1x_Mode,
    RIO_BOOL_T              is_Force_1x_Mode_Lane_0
)
{
    (void)lp_Serial_Is_1x;
    while (1)
    {
        if (PCS_PMA_PM__INIT_FSM__SILENT == pm_Instance->init_State)
        {
            if (pm_Instance->init_Status)
            {
                pm_Instance->init_Status = RIO_FALSE;

                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Init_1x4x_FSM: INITIALIZATION is lost {Info #08}"
                );
                rio_PCS_PMA_PM_Reset_Port_Regs(pm_Instance);
            }
	    /* SILENT STATE - GDA */
    	    /* printf("\n\n\tIam in SILENT state now\n"); */
            pm_Instance->is_Lane_OE[0] = RIO_FALSE;
            pm_Instance->is_Lane_OE[1] = RIO_FALSE;
            pm_Instance->is_Lane_OE[2] = RIO_FALSE;
            pm_Instance->is_Lane_OE[3] = RIO_FALSE;
            pm_Instance->rcv_Status_Symbols_Counter = 0;
            pm_Instance->snd_Status_Symbols_Counter = 0;
            pm_Instance->trx_Requests_Enabled = RIO_FALSE;
            pm_Instance->silence_Timer = pm_Instance->parameters_Set->silence_Period;
            pm_Instance->before_Status_Symbol_Counter =
                pm_Instance->parameters_Set->status_Sym_Rate - PCS_PMA_PM__STATUS_SYMBOL_SIZE;

            PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT_PART_2);
        }

        if (PCS_PMA_PM__INIT_FSM__SILENT_PART_2 == pm_Instance->init_State)
        {   /* SILENCE_TIMER_DONE, so goto SEEK state - GDA */
            if (0 == pm_Instance->silence_Timer){
    	    /* printf("\n\n\tIam going to SEEK state now\n\n"); */
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SEEK);
           }else
                break;
        }

        if (PCS_PMA_PM__INIT_FSM__SEEK == pm_Instance->init_State)
        {   /* In SEEK State - GDA */
            /* printf("\n\n\tIam in SEEK state now\n\n"); */
            pm_Instance->is_Lane_OE[0] = RIO_TRUE;
            pm_Instance->is_Lane_OE[2] = RIO_TRUE;
            if (
	        (!is_Force_1x_Mode) &&	/* GDA */
                (RIO_TRUE == pm_Instance->sync_Status[0] ||
                 RIO_TRUE == pm_Instance->sync_Status[2])
                ){/* Enter DISCOVERY mode - GDA */
		/* printf("\n\n\tIam going to DISCOVERY mode now\n\n"); */
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__DISCOVERY);}

	    /* GDA Implementation for rest of 2 conditions according to Spec Ver 1.3 */
            else
	    if (/* Enter the 1X_MODE_LANE0 */
		is_Force_1x_Mode && is_Force_1x_Mode_Lane_0 && 
                 (RIO_TRUE == pm_Instance->sync_Status[0])
		)
		{
		    /* printf("\n\n\tIam going to 1X_MODE_LANE0 mode now\n\n"); */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__1X_MODE_LANE0);
                    break;
                }
            else
	    if (/* Enter the 1X_MODE_LANE2 */
		 is_Force_1x_Mode && (!(RIO_TRUE == pm_Instance->sync_Status[0]) || 
		 !is_Force_1x_Mode_Lane_0) && 
                 (RIO_TRUE == pm_Instance->sync_Status[2])
		)
                {
		    /* printf("\n\n\tIam going to 1X_MODE_LANE2 mode now\n\n"); */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__1X_MODE_LANE2);
                    break;
                }
	    else
                break;
        }

/* End - GDA */ 


        if (PCS_PMA_PM__INIT_FSM__DISCOVERY == pm_Instance->init_State)
        {   /* In DISCOVERY state - GDA */
	    /* printf("\n\n\tIm inside DISCOVERY state now\n\n"); */
            pm_Instance->is_Lane_OE[1] = RIO_TRUE;
            pm_Instance->is_Lane_OE[3] = RIO_TRUE;
            pm_Instance->discovery_Timer = pm_Instance->parameters_Set->discovery_Period;
            PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__DISCOVERY_PART_2);
        }

        if (PCS_PMA_PM__INIT_FSM__DISCOVERY_PART_2 == pm_Instance->init_State)
        {
            if (/* Go back to previous SILENT state - GDA */
                RIO_TRUE != pm_Instance->sync_Status[0] &&
                RIO_TRUE != pm_Instance->sync_Status[2]
            )
            {
		/* printf("\n\n\tIm going to SILENT state now from discovery...\n\n"); */
		PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);	/* GDA */
                /* PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SEEK);	- Rev 1.2 */
		break;
            }
	    

            if (pm_Instance->discovery_Timer > 0)
            /* this check is to prevent the failure in case the timer was originally 0 */
                pm_Instance->discovery_Timer--;

            if (0 == pm_Instance->discovery_Timer)
            /* discovery timer has finished */
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Init_1x4x_FSM: discovery timer has finished {Info #09}"
                );
	/* 
	 *
	 * DISCOVERY STATE TO ALL OTHER STATE STATE 
	 *
	 */
            
            if (/* 4X_MODE condition from DISCOVERY - GDA */
                RIO_TRUE == pm_Instance->align_Status/* &&
                !is_Force_1x_Mode - spec ver1.2 */
            )
            {   /* Enter 4X_MODE */
		/* printf("\n\n\tIam going to 4X_MODE mode now from Discovery\n\n"); */
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__4X_MODE);
                break;
            }
            else 
            /* is_Force_1x_Mode || !pm_Instance->align_Status */
            if (/* is_Force_1x_Mode || - Spec ver 1.2, GDA */
                    0 == pm_Instance->discovery_Timer &&
                    RIO_TRUE != pm_Instance->align_Status
            )
            {
                if (/* Enter 1X_MODE_LANE0 - GDA */
                    RIO_TRUE == pm_Instance->sync_Status[0]/* &&
                    (
                        !is_Force_1x_Mode ||		//spec ver 1.2
                        is_Force_1x_Mode_Lane_0
                    )*/
                )
                {/* Enter the 1X_MODE_LANE0 - GDA */
		    /* printf("\n\n\tIam going to 1X_MODE_LANE0 mode now from Discovery\n\n"); */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__1X_MODE_LANE0);
                    break;
                }
                else
                /*if ( RIO_TRUE == pm_Instance->sync_Status[2] &&
                     ( RIO_TRUE != pm_Instance->sync_Status[0] ||
                     (!is_Force_1x_Mode_Lane_0 && is_Force_1x_Mode)))*/
		/* GDA */
		if ( RIO_TRUE == pm_Instance->sync_Status[2] &&
                     RIO_TRUE != pm_Instance->sync_Status[0]) 
                {
		    /* printf("\n\n\tIam going to 1X_MODE_LANE2 mode now from Discovery\n\n"); */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__1X_MODE_LANE2);
                    break;
                }
                /*
                else
                    pm_Instance->sync_Status[0] &&
                        is_Force_1x_Mode !is_Force_1x_Mode_Lane_0 &&

                => do not change the FSM state (wait when lane 2 will be fixed)
                */
            }
            else
                break;
        }

        if (PCS_PMA_PM__INIT_FSM__4X_MODE == pm_Instance->init_State)
        {
            if (!pm_Instance->init_Status)
            {
                pm_Instance->init_Status = RIO_TRUE;
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    RIO_TRUE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_IS_PORT_TRUE_4X,
                    RIO_TRUE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_COPY_TO_LANE2,
                    RIO_FALSE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED_PORT_WIDTH,
                    RIO_INITIALIZED_PORT_WIDTH__FOUR_LANE,
                    0 /* don't care */
                );
                /* reset "INVALID character" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );
                /* reset "CRC Error" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_BAD_CONTROL
                );
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Init_1x4x_FSM: 4x port has been INITIALIZED in 4x mode {Info #05}"
                );
                break;
            }
            else
            if (RIO_TRUE != pm_Instance->align_Status)
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
            else
            {
                unsigned long indicator_Value;
                unsigned int indicator_Parameter;

                pm_Instance->ctray->rio_Get_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    &indicator_Value,
                    &indicator_Parameter
                );
                if (RIO_TRUE != indicator_Value)
                /* "force_reinit" siganl is asserted */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
                else
                    break;
            }
        }

        if (PCS_PMA_PM__INIT_FSM__1X_MODE_LANE0 == pm_Instance->init_State)
        {
            if (!pm_Instance->init_Status)
            {
                pm_Instance->init_Status = RIO_TRUE;
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    RIO_TRUE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_IS_PORT_TRUE_4X,
                    RIO_FALSE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_COPY_TO_LANE2,
                    RIO_FALSE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED_PORT_WIDTH,
                    RIO_INITIALIZED_PORT_WIDTH__SINGLE_LANE_0,
                    0 /* don't care */
                );
                /* reset "INVALID character" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );
                /* reset "CRC Error" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_BAD_CONTROL
                );
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Init_1x4x_FSM: 4x port has been INITIALIZED in 1x mode (lane 0) {Info #06}"
                );
                break;
            }
            else
            if (RIO_TRUE != pm_Instance->sync_Status[0])
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
            else
            {
                unsigned long indicator_Value;
                unsigned int indicator_Parameter;

                pm_Instance->ctray->rio_Get_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    &indicator_Value,
                    &indicator_Parameter
                );
                if (RIO_TRUE != indicator_Value)
                /* "force_reinit" siganl is asserted */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
                else
                    break;
            }
        }

        if (PCS_PMA_PM__INIT_FSM__1X_MODE_LANE2 == pm_Instance->init_State)
        {
            if (!pm_Instance->init_Status)
            {
                pm_Instance->init_Status = RIO_TRUE;
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    RIO_TRUE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_IS_PORT_TRUE_4X,
                    RIO_FALSE,
                    0
                );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_COPY_TO_LANE2,
                    RIO_TRUE,
                    0
                );
                if (
                    !is_Force_1x_Mode ||
                    is_Force_1x_Mode_Lane_0

                )
                    pm_Instance->ctray->rio_Set_State_Flag(
                        pm_Instance->ctray->rio_Regs_Context,
                        pm_Instance->mum_Instanse,
                        RIO_LANE2_JUST_FORCED_BY_DISCOVERY,
                        RIO_TRUE,
                        0 /* don't care */
                    );
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED_PORT_WIDTH,
                    RIO_INITIALIZED_PORT_WIDTH__SINGLE_LANE_2,
                    0 /* don't care */
                );
                /* reset "INVALID character" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );
                /* reset "CRC Error" flag */
                pm_Instance->ctray->rio_Set_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_FALSE,
                    RIO_PL_PNACC_BAD_CONTROL
                );
                rio_PCS_PMA_PM_Display_Msg(
                    pm_Instance,
                    RIO_PL_SERIAL_MSG_NOTE_MSG,
                    "rio_PCS_PMA_PM_Exec_Init_1x4x_FSM: 4x port has been INITIALIZED in 1x mode (lane 2) {Info #07}"
                );
                break;
            }
            else
            if (RIO_TRUE != pm_Instance->sync_Status[2])
                PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
            else
            {
                unsigned long indicator_Value;
                unsigned int indicator_Parameter;

                pm_Instance->ctray->rio_Get_State_Flag(
                    pm_Instance->ctray->rio_Regs_Context,
                    pm_Instance->mum_Instanse,
                    RIO_INITIALIZED,
                    &indicator_Value,
                    &indicator_Parameter
                );
                if (RIO_TRUE != indicator_Value)
                /* "force_reinit" siganl is asserted */
                    PCS_PMA_PM__INIT_NEXT_STATE(PCS_PMA_PM__INIT_FSM__SILENT);
                else
                    break;
            }
        }

        if (
            pm_Instance->init_State > PCS_PMA_PM__INIT_FSM__1X_MODE_LANE2 ||
            PCS_PMA_PM__INIT_FSM__1X_MODE == pm_Instance->init_State
        )
        {
            rio_PCS_PMA_PM_Display_Msg(
                pm_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_PM_Exec_Init_1x_FSM: unknown state for 1x4x INIT FSM"
            );
            return RIO_ERROR;
        }
    }

    return RIO_OK;
}


/*****************************************************************************/
