/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    PCS/PMA main file.
*
* Notes:        
*
******************************************************************************/

#include <malloc.h>
#include <stdio.h>

#include "rio_pcs_pma_pm.h"
#include "rio_pcs_pma_dp.h"
#include "rio_pcs_pma_model.h"
#include "rio_pcs_pma_ds.h"

/*
 * Global variable for counting number of PCS/PMA Protocol Manager
 * instantiations. It is used for proper instance number assignement
 */
static unsigned long rio_PCS_PMA_Inst_Count = 0;



static int rio_PCS_PMA_Initialize(
    RIO_HANDLE_T                  handle,
    RIO_PCS_PMA_PARAM_SET_T       *param_Set
);

static int rio_PCS_PMA_Start_Reset(
    RIO_HANDLE_T handle
);

static int rio_PCS_PMA_Delete_Instance(
    RIO_HANDLE_T          handle
);

static int rio_PCS_PMA_Serial_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
);

static int rio_PCS_PMA_Clock_Edge(
    RIO_HANDLE_T handle  /* instance's handle */
);

static int rio_PCS_PMA_Machine_Management(
    RIO_HANDLE_T                handle,
    RIO_BOOL_T                    machine_Is_Switched_On, /*RIO_TRUE if seq must be sent*/
    RIO_PCS_PMA_MACHINE_T       machine_Type /*size is 4*/
);

static int rio_PCS_PMA_User_Msg(
    RIO_CONTEXT_T                context,    /* callback context */
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
);

static int rio_PCS_PMA_Set_State_Flag(
    RIO_CONTEXT_T  context,
    RIO_HANDLE_T   handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Parameter
);

static int rio_PCS_PMA_Get_State_Flag(
    RIO_CONTEXT_T  context,
    RIO_HANDLE_T   handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Parameter
);

static int rio_PCS_PMA_DP_Get_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
);

static int rio_PCS_PMA_DP_Put_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
);

static int rio_PCS_PMA_DP_Put_Corrupted_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T           *granule,          /* pointer to granule data structure */
    RIO_PCS_PMA_CHARACTER_T         *character,        /* pointer to character data structure */
    RIO_PCS_PMA_GRANULE_VIOLATION_T violation
);

static int rio_PCS_PMA_PM_Get_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
);

static int rio_PCS_PMA_PM_Put_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
);

static int rio_PCS_PMA_DP_Put_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

static int rio_PCS_PMA_DP_Get_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

static int rio_PCS_PMA_PM_Put_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

static int rio_PCS_PMA_PM_Get_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

static int rio_PCS_PMA_Pop_Char_Buffer(
    RIO_CONTEXT_T  context,
    RIO_UCHAR_T    lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
);

static int rio_PCS_PMA_DP_Put_Codegroup(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

static int rio_PCS_PMA_DP_Get_Codegroup_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

static int rio_PCS_PMA_PM_Put_Codegroup(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

static int rio_PCS_PMA_PM_Get_Codegroup_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

static int rio_PCS_PMA_Pop_Codegroup_Buffer(
    RIO_CONTEXT_T  context,
    RIO_UCHAR_T    lane_ID            /* 0 - for lane 0, 1 - for lane 1, etc. */
);

static int rio_PCS_PMA_Serial_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T  tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T  tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T  tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T  tlane3       /* transmit data on lane 3*/
);

/*bug-132*/
static int rio_PCS_PMA_Symbol_To_Send(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_UCHAR_T*          symbol_Buffer,
    RIO_UCHAR_T           buffer_Size
            );

/*bug-132*/

/***************************************************************************
 * Function : RIO_PCS_PMA_Model_Create_Instance
 *
 * Description: Create new PCS/PMA instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_Model_Create_Instance(
    RIO_HANDLE_T            *handle
    )
{
    /* handle to structure will allocated*/
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = NULL;
    RIO_PCS_PMA_PM_CALLBACK_TRAY_T  pm_Ctray;
    RIO_PCS_PMA_DP_CALLBACK_TRAY_T  dp_Ctray;

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for physical layer 
     * data structure
     */
    if (handle == NULL )
        return RIO_ERROR;

    /* clear handle */
    *handle = NULL;

    /* allocate instan�e */
    if ((pcs_PMA_Instance = malloc(sizeof(RIO_PCS_PMA_DS_T))) == NULL)
        return RIO_ERROR;

    /*
     * clear all ctray pointers
     */
    if ((pcs_PMA_Instance->ctray = malloc(sizeof(RIO_PCS_PMA_MODEL_CALLBACK_TRAY_T))) == NULL)
        return RIO_ERROR;
    pcs_PMA_Instance->ctray->rio_User_Msg = NULL;
    pcs_PMA_Instance->ctray->user_Msg_Context = NULL;
    pcs_PMA_Instance->ctray->rio_Set_State_Flag = NULL;
    pcs_PMA_Instance->ctray->rio_Get_State_Flag = NULL;
    pcs_PMA_Instance->ctray->rio_Regs_Context = NULL;
    pcs_PMA_Instance->ctray->rio_Serial_Set_Pins = NULL;
    pcs_PMA_Instance->ctray->lp_Serial_Interface_Context = NULL;
    pcs_PMA_Instance->ctray->get_Granule = NULL;
    pcs_PMA_Instance->ctray->put_Granule = NULL;
    pcs_PMA_Instance->ctray->put_Corrupted_Granule = NULL;
    pcs_PMA_Instance->ctray->granule_Level_Context = NULL;
    pcs_PMA_Instance->ctray->put_Character_Column = NULL;
    pcs_PMA_Instance->ctray->get_Character_Column = NULL;
    pcs_PMA_Instance->ctray->character_Level_Context = NULL;
    pcs_PMA_Instance->ctray->put_Codegroup = NULL;
    pcs_PMA_Instance->ctray->get_Codegroup_Column = NULL;
    pcs_PMA_Instance->ctray->codegroup_Level_Context = NULL;
    pcs_PMA_Instance->ctray->rio_Symbol_To_Send = NULL; /*bug-132*/

    /*
     * increase count of TxRx instances and set
     * up the number of instance, turn link into reset
     */
    *handle = pcs_PMA_Instance;
    pcs_PMA_Instance->instance_Number = ++rio_PCS_PMA_Inst_Count;

#ifndef _CARBON_SRIO_XTOR
    RIO_PCS_PMA_PM_Create_Instance(&pcs_PMA_Instance->pm_Handle, pcs_PMA_Instance);
#else
    if (RIO_PCS_PMA_PM_Create_Instance(&pcs_PMA_Instance->pm_Handle, pcs_PMA_Instance) == RIO_ERROR)
		return RIO_ERROR;
#endif
    RIO_PCS_PMA_PM_Get_Function_Tray(&pcs_PMA_Instance->pm_Ftray);
    pm_Ctray.rio_User_Msg = rio_PCS_PMA_User_Msg;
    pm_Ctray.user_Msg_Context = pcs_PMA_Instance;
    pm_Ctray.rio_Set_State_Flag = rio_PCS_PMA_Set_State_Flag;
    pm_Ctray.rio_Get_State_Flag = rio_PCS_PMA_Get_State_Flag;
    pm_Ctray.rio_Regs_Context = pcs_PMA_Instance;
    pm_Ctray.get_Granule = rio_PCS_PMA_PM_Get_Granule;
    pm_Ctray.put_Granule = rio_PCS_PMA_PM_Put_Granule;
    pm_Ctray.granule_Level_Context = pcs_PMA_Instance;
    pm_Ctray.put_Character_Column = rio_PCS_PMA_PM_Put_Character_Column;
    pm_Ctray.get_Character_Column = rio_PCS_PMA_PM_Get_Character_Column;
    pm_Ctray.pop_Char_Buffer = rio_PCS_PMA_Pop_Char_Buffer;
    pm_Ctray.character_Level_Context = pcs_PMA_Instance;
    pm_Ctray.put_Codegroup = rio_PCS_PMA_PM_Put_Codegroup;
    pm_Ctray.get_Codegroup_Column = rio_PCS_PMA_PM_Get_Codegroup_Column;
    pm_Ctray.pop_Codegroup_Buffer = rio_PCS_PMA_Pop_Codegroup_Buffer;
    pm_Ctray.codegroup_Level_Context = pcs_PMA_Instance;
    pm_Ctray.rio_Symbol_To_Send =  rio_PCS_PMA_Symbol_To_Send;  /*bug-132*/
#ifndef _CARBON_SRIO_XTOR
    RIO_PCS_PMA_PM_Bind_Instance(pcs_PMA_Instance->pm_Handle, &pm_Ctray);
#else
    if (RIO_PCS_PMA_PM_Bind_Instance(pcs_PMA_Instance->pm_Handle, &pm_Ctray)
			== RIO_ERROR)
		return RIO_ERROR;
#endif

#ifndef _CARBON_SRIO_XTOR
    RIO_PCS_PMA_DP_Create_Instance(&pcs_PMA_Instance->dp_Handle, pcs_PMA_Instance);    
#else
    if (RIO_PCS_PMA_DP_Create_Instance(&pcs_PMA_Instance->dp_Handle, pcs_PMA_Instance) == RIO_ERROR)
		return RIO_ERROR;
#endif
    RIO_PCS_PMA_DP_Get_Function_Tray(&pcs_PMA_Instance->dp_Ftray);
    dp_Ctray.rio_Serial_Set_Pins = rio_PCS_PMA_Serial_Set_Pins;
    dp_Ctray.lp_Serial_Interface_Context = pcs_PMA_Instance;
    dp_Ctray.rio_User_Msg = rio_PCS_PMA_User_Msg;
    dp_Ctray.user_Msg_Context = pcs_PMA_Instance;
    dp_Ctray.rio_Set_State_Flag = rio_PCS_PMA_Set_State_Flag;
    dp_Ctray.rio_Get_State_Flag = rio_PCS_PMA_Get_State_Flag;
    dp_Ctray.rio_Regs_Context = pcs_PMA_Instance;
    dp_Ctray.get_Granule = rio_PCS_PMA_DP_Get_Granule;
    dp_Ctray.put_Granule = rio_PCS_PMA_DP_Put_Granule;
    dp_Ctray.put_Corrupted_Granule = rio_PCS_PMA_DP_Put_Corrupted_Granule;
    dp_Ctray.granule_Level_Context = pcs_PMA_Instance;
    dp_Ctray.put_Character_Column = rio_PCS_PMA_DP_Put_Character_Column;
    dp_Ctray.get_Character_Column = rio_PCS_PMA_DP_Get_Character_Column;
    dp_Ctray.character_Level_Context = pcs_PMA_Instance;
    dp_Ctray.put_Codegroup = rio_PCS_PMA_DP_Put_Codegroup;
    dp_Ctray.get_Codegroup_Column = rio_PCS_PMA_DP_Get_Codegroup_Column;
    dp_Ctray.codegroup_Level_Context = pcs_PMA_Instance;
    dp_Ctray.rio_Symbol_To_Send =  rio_PCS_PMA_Symbol_To_Send;   /*bug-132*/
    RIO_PCS_PMA_DP_Bind_Instance(pcs_PMA_Instance->dp_Handle, &dp_Ctray);

    pcs_PMA_Instance->is_Ctray_Registered = RIO_FALSE;
    pcs_PMA_Instance->is_In_Reset = RIO_FALSE;
    rio_PCS_PMA_Start_Reset(pcs_PMA_Instance);

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PCS_PMA_Model_Get_Function_Tray
 *
 * Description: Export function tray, set pointers to corresponding 
 *              functions entry point
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_Model_Get_Function_Tray(
    RIO_PCS_PMA_MODEL_FTRAY_T *ftray             
)
{
    /*
     * check pointers to not NULL values and complete function tray
     * with valid entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->start_Reset = rio_PCS_PMA_Start_Reset;
    ftray->initialize = rio_PCS_PMA_Initialize;
    ftray->delete_Instance = rio_PCS_PMA_Delete_Instance;

    ftray->rio_Serial_Get_Pins = rio_PCS_PMA_Serial_Get_Pins;
    ftray->clock_Edge = rio_PCS_PMA_Clock_Edge;

    ftray->machine_Management = rio_PCS_PMA_Machine_Management;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PCS_PMA_Model_Bind_Instance
 *
 * Description: bind the instance with the environment, load entry point from
 *              callback tray to internal structure
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PCS_PMA_Model_Bind_Instance(
    RIO_HANDLE_T                       handle,
    RIO_PCS_PMA_MODEL_CALLBACK_TRAY_T  *ctray
    )
{
    /* 
     * convert void pointer to pointer to instance structure and 
     * check all callbacks pointers to be not NULL and 
     * store external callbacks with environment's context
     * from callback tray in instance's data structure
     */
    RIO_PCS_PMA_DS_T* pcs_PMA_Instance = (RIO_PCS_PMA_DS_T*)handle;

    if (ctray == NULL || 
        handle == NULL ||
        ctray->rio_User_Msg == NULL ||
        ctray->rio_Set_State_Flag == NULL ||
        ctray->rio_Get_State_Flag == NULL ||
        ctray->rio_Serial_Set_Pins == NULL ||
        ctray->get_Granule == NULL ||
        ctray->put_Granule == NULL ||
        ctray->put_Character_Column == NULL ||
        ctray->get_Character_Column == NULL ||
        ctray->put_Codegroup == NULL ||
        ctray->get_Codegroup_Column == NULL
    )
        return RIO_ERROR;
    
    pcs_PMA_Instance->ctray = (RIO_PCS_PMA_MODEL_CALLBACK_TRAY_T *)malloc(sizeof(RIO_PCS_PMA_MODEL_CALLBACK_TRAY_T));
    if (NULL == pcs_PMA_Instance->ctray)
    {
        /* error: unsuccessful malloc */
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA: ERROR/WARNING: RIO_PCS_PMA_Model_Bind_Instance: unsuccessful malloc");
#endif
        return RIO_ERROR;
    }

    *pcs_PMA_Instance->ctray = *ctray;
    pcs_PMA_Instance->is_Ctray_Registered = RIO_TRUE;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_Delete_Instance
 *
 * Description: deletes instance of PCS/PMA PM
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_PCS_PMA_Delete_Instance(
    RIO_HANDLE_T          handle
    )
{ 
    RIO_PCS_PMA_DS_T *rio_PCS_PMA_Instance = (RIO_HANDLE_T)handle;

    /* check pointers */
    if (rio_PCS_PMA_Instance == NULL)
        return RIO_ERROR;

    if (rio_PCS_PMA_Instance != NULL)
        free(rio_PCS_PMA_Instance);
   
    return RIO_OK; 
}

/***************************************************************************
 * Function : rio_PCS_PMA_Initialize
 *
 * Description: initialize the PCS/PMA PM
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Initialize(
    RIO_HANDLE_T                handle,
    RIO_PCS_PMA_PARAM_SET_T  *param_Set
)
{
    /* instanse handle and link's parameters set */
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T*)handle;
   
    /* check pointers */
    if (pcs_PMA_Instance == NULL)
        return RIO_ERROR;

    if (param_Set == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (!pcs_PMA_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA: ERROR: rio_PCS_PMA_Initialize: is called while not in reset\n");
#else
	if(pcs_PMA_Instance->ctray)
		pcs_PMA_Instance->ctray->rio_User_Msg(
				pcs_PMA_Instance->ctray->user_Msg_Context,
				RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA: ERROR: rio_PCS_PMA_Initialize: is called while not in reset\n");
#endif
        return RIO_ERROR;
    }
    
    if (!pcs_PMA_Instance->is_Ctray_Registered)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA: ERROR: rio_PCS_PMA_Initialize: is called while ctray is not registered\n");
#endif
        return RIO_ERROR;
    }

    pcs_PMA_Instance->parameters_Set = (RIO_PCS_PMA_PARAM_SET_T *)malloc(sizeof (RIO_PCS_PMA_PARAM_SET_T));
    /* this structure is simple and thus such resssignment OK */
    *(pcs_PMA_Instance->parameters_Set) = *param_Set;

    pcs_PMA_Instance->pm_Ftray.initialize(pcs_PMA_Instance->pm_Handle, pcs_PMA_Instance->parameters_Set);
    pcs_PMA_Instance->dp_Ftray.initialize(pcs_PMA_Instance->dp_Handle, pcs_PMA_Instance->parameters_Set);
    
    /*set off the reset flag*/
    pcs_PMA_Instance->is_In_Reset = RIO_FALSE;
    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Start_Reset(
    RIO_HANDLE_T handle
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)handle;

    /* check pointers */
    if (pcs_PMA_Instance == NULL)
        return RIO_ERROR;

    /* check work state */
    if (pcs_PMA_Instance->is_In_Reset == RIO_TRUE)
        return RIO_OK;
    
    /* modify state to be _in_reset_ */
    pcs_PMA_Instance->is_In_Reset = RIO_TRUE;

    pcs_PMA_Instance->pm_Ftray.start_Reset(pcs_PMA_Instance->pm_Handle);
    pcs_PMA_Instance->dp_Ftray.start_Reset(pcs_PMA_Instance->dp_Handle);

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_Serial_Get_Pins
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Serial_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)handle;

    /* check pointers */
    if (pcs_PMA_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pcs_PMA_Instance->is_In_Reset)
    {
        return RIO_OK;
    }
    
    return
        pcs_PMA_Instance->dp_Ftray.rio_Serial_Get_Pins(
            pcs_PMA_Instance->dp_Handle,
            rlane0,
            rlane1,
            rlane2,
            rlane3
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_Clock_Edge
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Clock_Edge(
    RIO_HANDLE_T handle  /* instance's handle */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)handle;

    /* check pointers */
    if (pcs_PMA_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (pcs_PMA_Instance->is_In_Reset)
    {
        return RIO_OK;
    }

    return
        pcs_PMA_Instance->dp_Ftray.clock_Edge(pcs_PMA_Instance->dp_Handle);
}

/***************************************************************************
 * Function : rio_PCS_PMA_Machine_Management
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Machine_Management(
    RIO_HANDLE_T                handle,
    RIO_BOOL_T                  machine_Is_Switched_On, /*RIO_TRUE if seq must be sent*/
    RIO_PCS_PMA_MACHINE_T       machine_Type /*size is 4*/
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)handle;

    /* check pointers */
    if (pcs_PMA_Instance == NULL)
        return RIO_ERROR;
	
    /* check reset state */
    if (pcs_PMA_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
		printf("PCS/PMA: ERROR: rio_PCS_PMA_Machine_Management: is called while in reset\n");
#else
	if(pcs_PMA_Instance->ctray)
		pcs_PMA_Instance->ctray->rio_User_Msg(
				pcs_PMA_Instance->ctray->user_Msg_Context,
				RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA: ERROR: rio_PCS_PMA_Machine_Management: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    return
        pcs_PMA_Instance->pm_Ftray.machine_Management(
            pcs_PMA_Instance->pm_Handle,
            machine_Is_Switched_On,
            machine_Type
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_User_Msg
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_User_Msg(
    RIO_CONTEXT_T               context,    /* callback context */
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
    unsigned long            indicator_Value;
    unsigned int             indicator_Parameter;

    indicator_Value = RIO_TRUE;
    if (msg_Type == RIO_PL_SERIAL_MSG_ERROR_MSG)
    {
        pcs_PMA_Instance->ctray->rio_Get_State_Flag(
            pcs_PMA_Instance->ctray->rio_Regs_Context,
            pcs_PMA_Instance,
            RIO_INITIALIZED,
            &indicator_Value,
            &indicator_Parameter
        );

        if (indicator_Value != RIO_TRUE)
        /* port is not in the initialized state => prohibit reporting any error message */
            msg_Type = RIO_PL_SERIAL_MSG_WARNING_MSG;
    }

    return
        pcs_PMA_Instance->ctray->rio_User_Msg(
            pcs_PMA_Instance->ctray->user_Msg_Context,
            msg_Type,
            string
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_Set_State_Flag
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Set_State_Flag(
    RIO_CONTEXT_T  context,
    RIO_HANDLE_T   handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Parameter
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
    unsigned long            indicator_Value_2;
    unsigned int             indicator_Parameter_2;

    /* check pointers */
    if (pcs_PMA_Instance == NULL)
        return RIO_ERROR;

    indicator_Value_2 = RIO_TRUE;
    if (
        indicator_ID == RIO_OUTPUT_ERROR ||
        indicator_ID == RIO_INPUT_ERROR
    )
        pcs_PMA_Instance->ctray->rio_Get_State_Flag(
            pcs_PMA_Instance->ctray->rio_Regs_Context,
            handle,
            RIO_INITIALIZED,
            &indicator_Value_2,
            &indicator_Parameter_2
        );

    if (indicator_Value_2 == RIO_TRUE)
    /* port is not in the initialized state => prohibit setting any error */
    {
        return
            pcs_PMA_Instance->ctray->rio_Set_State_Flag(
                pcs_PMA_Instance->ctray->rio_Regs_Context,
                handle,
                indicator_ID,
                indicator_Value,
                indicator_Parameter
            );
    }
    else
        return 0;
}

/***************************************************************************
 * Function : rio_PCS_PMA_Get_State_Flag
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Get_State_Flag(
    RIO_CONTEXT_T  context,
    RIO_HANDLE_T   handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Parameter
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    /* check pointers */
    if (pcs_PMA_Instance == NULL)
        return RIO_ERROR;

    return
        pcs_PMA_Instance->ctray->rio_Get_State_Flag(
            pcs_PMA_Instance->ctray->rio_Regs_Context,
            handle,
            indicator_ID,
            indicator_Value,
            indicator_Parameter
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Get_Granule
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_DP_Get_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->pm_Ftray.get_Granule(
            pcs_PMA_Instance->pm_Handle,
            granule
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Put_Granule
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_DP_Put_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->pm_Ftray.put_Granule(
            pcs_PMA_Instance->pm_Handle,
            granule
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Put_Corrupted_Granule
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_DP_Put_Corrupted_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T           *granule,          /* pointer to granule data structure */
    RIO_PCS_PMA_CHARACTER_T         *character,        /* pointer to character data structure */
    RIO_PCS_PMA_GRANULE_VIOLATION_T violation
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
    unsigned long            indicator_Value;
    unsigned int             indicator_Parameter;

    indicator_Value = RIO_TRUE;
    pcs_PMA_Instance->ctray->rio_Get_State_Flag(
        pcs_PMA_Instance->ctray->rio_Regs_Context,
        pcs_PMA_Instance,
        RIO_INITIALIZED,
        &indicator_Value,
        &indicator_Parameter
    );

    if (indicator_Value == RIO_TRUE)
    /* port is not in the initialized state => prohibit reporting any error */
    {
        if (pcs_PMA_Instance->ctray->put_Corrupted_Granule != NULL)
        return
            pcs_PMA_Instance->ctray->put_Corrupted_Granule(
                pcs_PMA_Instance->ctray->granule_Level_Context,
                granule,
                character,
                violation
            );
        else 
            return 0;
    }
    else
        return 0;
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Granule
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Get_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->ctray->get_Granule(
            pcs_PMA_Instance->ctray->granule_Level_Context,
            granule
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Put_Granule
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Put_Granule(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->ctray->put_Granule(
            pcs_PMA_Instance->ctray->granule_Level_Context,
            granule
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Put_Character_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_DP_Put_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->pm_Ftray.put_Character_Column(
            pcs_PMA_Instance->pm_Handle,
            character
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Get_Character_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_DP_Get_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->pm_Ftray.get_Character_Column(
            pcs_PMA_Instance->pm_Handle,
            character
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Put_Character_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Put_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;

    /* 
     * hook
     */
    pcs_PMA_Instance->ctray->rio_Get_State_Flag(
        pcs_PMA_Instance->ctray->rio_Regs_Context,
        pcs_PMA_Instance,
        RIO_SUPPRESS_HOOK,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE != indicator_Value)
    {
        pcs_PMA_Instance->ctray->rio_Get_State_Flag(
            pcs_PMA_Instance->ctray->rio_Regs_Context,
            pcs_PMA_Instance,
            RIO_COPY_TO_LANE2,
            &indicator_Value,
            &indicator_Parameter
        );
        if (RIO_TRUE == indicator_Value)
        {
            if (1 != character->lane_Mask)
            {
                pcs_PMA_Instance->ctray->rio_User_Msg(
                    pcs_PMA_Instance->ctray->user_Msg_Context,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_PM_Put_Character_Column: error: lane mask is not 0 in the internal interface in the 1x mode"
                );
                return RIO_ERROR;
            }

            character->character_Type[2] = character->character_Type[0];
            character->character_Data[2] = character->character_Data[0];
            character->lane_Mask = RIO_LANE_MASK_LANE2;
        }

        pcs_PMA_Instance->ctray->put_Character_Column(
            pcs_PMA_Instance->ctray->character_Level_Context,
            character
        );

        if (RIO_TRUE == indicator_Value)
        /* correct the codegroup back */
        {
            character->character_Data[0] = character->character_Data[2];
            character->character_Type[0] = character->character_Type[2];
            character->lane_Mask = 1;
        }
    }

    return
        pcs_PMA_Instance->dp_Ftray.put_Character_Column(
            pcs_PMA_Instance->dp_Handle,
            character
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Character_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Get_Character_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
    int result;

    result = 
        pcs_PMA_Instance->dp_Ftray.get_Character_Column(
            pcs_PMA_Instance->dp_Handle,
            character
        );

    /* hook */
    if (RIO_OK == result)
        pcs_PMA_Instance->ctray->get_Character_Column(
            pcs_PMA_Instance->ctray->character_Level_Context,
            character
        );

    return result;
}

/***************************************************************************
 * Function : rio_PCS_PMA_Pop_Char_Buffer
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Pop_Char_Buffer(
    RIO_CONTEXT_T  context,
    RIO_UCHAR_T    lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->dp_Ftray.pop_Char_Buffer(
            pcs_PMA_Instance->dp_Handle,
            lane_Mask
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Put_Codegroup
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_DP_Put_Codegroup(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->pm_Ftray.put_Codegroup(
            pcs_PMA_Instance->pm_Handle,
            codegroup
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Get_Codegroup_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_DP_Get_Codegroup_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->pm_Ftray.get_Codegroup_Column(
            pcs_PMA_Instance->pm_Handle,
            codegroup
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Put_Codegroup
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Put_Codegroup(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;

    /* 
     * hook
     */
    pcs_PMA_Instance->ctray->rio_Get_State_Flag(
        pcs_PMA_Instance->ctray->rio_Regs_Context,
        pcs_PMA_Instance,
        RIO_SUPPRESS_HOOK,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE != indicator_Value)
    {
        pcs_PMA_Instance->ctray->rio_Get_State_Flag(
            pcs_PMA_Instance->ctray->rio_Regs_Context,
            pcs_PMA_Instance,
            RIO_COPY_TO_LANE2,
            &indicator_Value,
            &indicator_Parameter
        );
        if (RIO_TRUE == indicator_Value)
        {
            if (1 != codegroup->lane_Mask)
            {
                if (RIO_LANE_MASK_LANE2 == codegroup->lane_Mask)
                    pcs_PMA_Instance->ctray->rio_Get_State_Flag(
                        pcs_PMA_Instance->ctray->rio_Regs_Context,
                        pcs_PMA_Instance,
                        RIO_LANE2_JUST_FORCED_BY_DISCOVERY,
                        &indicator_Value,
                        &indicator_Parameter
                    );
                else
                    indicator_Value = RIO_FALSE;
                if (RIO_TRUE != indicator_Value)
                {
                    pcs_PMA_Instance->ctray->rio_User_Msg(
                        pcs_PMA_Instance->ctray->user_Msg_Context,
                        RIO_PL_SERIAL_MSG_ERROR_MSG,
                        "rio_PCS_PMA_PM_Get_Codegroup_Column: error: lane mask is not 0 in the internal interface in the 1x mode"
                    );
                    return RIO_ERROR;
                }

                codegroup->code_Group_Data[0] = codegroup->code_Group_Data[2];
                codegroup->is_Running_Disparity_Positive[0] = codegroup->is_Running_Disparity_Positive[2];
            }

            codegroup->code_Group_Data[2] = codegroup->code_Group_Data[0];
            codegroup->is_Running_Disparity_Positive[2] = codegroup->is_Running_Disparity_Positive[0];
            codegroup->lane_Mask = RIO_LANE_MASK_LANE2;
        }

        pcs_PMA_Instance->ctray->put_Codegroup(
            pcs_PMA_Instance->ctray->codegroup_Level_Context,
            codegroup
        );

        if (RIO_TRUE == indicator_Value)
        /* correct the codegroup back */
        {
            codegroup->code_Group_Data[0] = codegroup->code_Group_Data[2];
            codegroup->is_Running_Disparity_Positive[0] = codegroup->is_Running_Disparity_Positive[2];
            codegroup->lane_Mask = 1;
        }
    }

    return
        pcs_PMA_Instance->dp_Ftray.put_Codegroup(
            pcs_PMA_Instance->dp_Handle,
            codegroup
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_PM_Get_Codegroup_Column
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_PM_Get_Codegroup_Column(
    RIO_CONTEXT_T  context,
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
    int result;

    result = 
        pcs_PMA_Instance->dp_Ftray.get_Codegroup_Column(
            pcs_PMA_Instance->dp_Handle,
            codegroup
        );

    /* hook */
    if (RIO_OK == result)
    {
        pcs_PMA_Instance->ctray->get_Codegroup_Column(
            pcs_PMA_Instance->ctray->codegroup_Level_Context,
            codegroup
        );
    }

    return result;
}

/***************************************************************************
 * Function : rio_PCS_PMA_Pop_Codegroup_Buffer
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Pop_Codegroup_Buffer(
    RIO_CONTEXT_T  context,
    RIO_UCHAR_T    lane_ID            /* 0 - for lane 0, 1 - for lane 1, etc. */
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->dp_Ftray.pop_Codegroup_Buffer(
            pcs_PMA_Instance->dp_Handle,
            lane_ID
        );
}

/***************************************************************************
 * Function : rio_PCS_PMA_Serial_Set_Pins
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Serial_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T  tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T  tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T  tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T  tlane3       /* transmit data on lane 3*/
)
{
    RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;

    return
        pcs_PMA_Instance->ctray->rio_Serial_Set_Pins(
            pcs_PMA_Instance->ctray->lp_Serial_Interface_Context,
            tlane0,
            tlane1,
            tlane2,
            tlane3
        );
}


/*****************************************************************************/

/*bug-132*/
/***************************************************************************
 * Function : rio_PCS_PMA_Symbol_To_Send
 *
 * Description: just wrapper
 *
 * Returns: error code
 *
 * Notes: none
 *
**************************************************************************/
#pragma check_stack(on)
static int rio_PCS_PMA_Symbol_To_Send(
                         RIO_CONTEXT_T context,
                         RIO_UCHAR_T*          symbol_Buffer,
                         RIO_UCHAR_T           buffer_Size
                               )
{
            RIO_PCS_PMA_DS_T *pcs_PMA_Instance = (RIO_PCS_PMA_DS_T *)context;
                return
         pcs_PMA_Instance->ctray->rio_Symbol_To_Send(
                       pcs_PMA_Instance->ctray->codegroup_Level_Context,
                                      symbol_Buffer,
                                        buffer_Size
                         );
}
/*****************************************************************************/


