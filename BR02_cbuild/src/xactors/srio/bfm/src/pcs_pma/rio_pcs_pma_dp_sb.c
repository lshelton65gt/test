/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pcs_pma/rio_pcs_pma_dp_sb.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    PCS/PMA Data Processor: Streaming Block.
*
* Notes:        
*
******************************************************************************/

#include "rio_pcs_pma_dp_sb.h"
#include "rio_pcs_pma_dp.h"

#include "rio_gran_type.h"
#include "rio_serial_gran_type.h"

#include <stdio.h>


/*#define DEBUG_SC_PD_CHOOSING*/


/* Table 3-8 of the spec is coded here */
#define RIO_SYMBOL_CRC_CODING_TABLE_SIZE   19
static char symbol_CRC_Coding_Table[RIO_SYMBOL_CRC_CODING_TABLE_SIZE][RIO_SYMBOL_CRC_SIZE] =
{
    { 1, 1, 0, 0, 1 },
    { 1, 0, 1, 1, 0 },
    { 0, 1, 0, 1, 1 },
    { 1, 1, 1, 1, 1 },
    { 1, 0, 1, 0, 1 },
    { 1, 0, 0, 0, 0 },
    { 0, 1, 0, 0, 0 },
    { 0, 0, 1, 0, 0 },
    { 0, 0, 0, 1, 0 },
    { 0, 0, 0, 0, 1 },
    { 1, 1, 0, 1, 0 },
    { 0, 1, 1, 0, 1 },
    { 1, 1, 1, 0, 0 },
    { 0, 1, 1, 1, 0 },
    { 0, 0, 1, 1, 1 },
    { 1, 1, 0, 0, 1 },
    { 1, 0, 1, 1, 0 },
    { 0, 1, 0, 1, 1 },
    { 1, -1, -1, -1, 1 }
};

#define CHECK_IDLES_INSIDE_PACKET \
    if (!dp_Instance->is_Flag_Asserted[RIO_PCS_PMA_DP_FLAG_IDLES_INSIDE_PACKET]) \
    { \
        dp_Instance->interfaces.rio_Get_State_Flag( \
            dp_Instance->interfaces.rio_Regs_Context, \
            dp_Instance->mum_Instanse, \
            RIO_RX_IS_PD, /*RIO_RX_DATA_IN_PROGRESS*/ \
            &indicator_Value, \
            &indicator_Parameter \
        ); \
        if (RIO_TRUE == indicator_Value) \
        /* IDLE granule inside a packet */ \
        { \
            rio_PCS_PMA_DP_Display_Msg( \
                dp_Instance, \
                RIO_PL_SERIAL_MSG_ERROR_MSG, \
                "rio_PCS_PMA_DP_Decode_Granule: IDLE granule(s) inside a packet" \
            ); \
            dp_Instance->interfaces.rio_Set_State_Flag( \
                dp_Instance->interfaces.rio_Regs_Context, \
                dp_Instance->mum_Instanse, \
                RIO_INPUT_ERROR, \
                RIO_TRUE, \
                RIO_PL_PNACC_INVALID_CHARACTER \
            ); \
\
            /*put corrupted granule for BC event*/ \
            dp_Instance->interfaces.put_Corrupted_Granule( \
                dp_Instance->interfaces.granule_Level_Context, \
                NULL,          /* pointer to granule data structure */ \
                character,        /* pointer to character data structure */ \
                RIO_PCS_PMA_IDLE_SEQ_IN_PACKET \
            ); \
\
            /* raising the flag to not report numerous sabsequent errors of this kind */ \
            /* (to dostinguish only truly separate ones) */ \
            dp_Instance->is_Flag_Asserted[RIO_PCS_PMA_DP_FLAG_IDLES_INSIDE_PACKET] = RIO_TRUE; \
        } \
    }


/***************************************************************************
 * Function : rio_PCS_PMA_DP_Get_Character_Column
 *
 * Description: requests a next codegroup for transmission through the link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Get_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T    *character        /* pointer to character data structure */
)
{
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T *)handle_Context;
    RIO_PCS_PMA_GRANULE_T granule;
    unsigned int result;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;
 /*   GDA: int i=0;	

 for(i=0;i<4;i++){*/
    /* RIO_SC;RIO_PD;RIO_K;RIO_A; 
    printf("\n\t(DP_Get_Character_Column)granule->character_Type[%d]=%d", \
				i,character->character_Type[i]);

comment: 0x1;0x2;0x3;0x4; 
    printf("\n\t(DP_Get_Character_Column)granule->character_Data[%d]=%d", \
				i,character->character_Data[i]);
    }
    printf("\n\t(DP_Get_Character_Column)granule->lane_Mask=%x",\
				character->lane_Mask);    */
    if (dp_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Get_Character_Column: is called while in reset\n");
#else
		dp_Instance->interfaces.rio_User_Msg(
				dp_Instance->interfaces.user_Msg_Context,
				RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Get_Character_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_IS_PORT_TRUE_4X,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    /* true 4x link */
    {
        result = dp_Instance->interfaces.get_Granule(
            dp_Instance->interfaces.granule_Level_Context,
            &granule
        );

        if (RIO_NO_DATA == result)
            return RIO_NO_DATA;

        /* encode full granule */
        rio_PCS_PMA_DP_Encode_Granule(
            dp_Instance,
            &granule,
            character
        );
        character->lane_Mask = RIO_PL_ALL_LANES_MASK;
    }
    else
    /* 1x link */
    {
        dp_Instance->interfaces.rio_Get_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE,
            &indicator_Value,
            &indicator_Parameter
        );
        character->lane_Mask = 1; /* only character_Data[0] is valid */
        if (RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1 == indicator_Value)
        {
            /* get next granule */
            result = dp_Instance->interfaces.get_Granule(
                dp_Instance->interfaces.granule_Level_Context,
                &granule
            );

            if (RIO_NO_DATA == result)
            {
                return RIO_NO_DATA;
            }

            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE,
                0, /* byte 0 is being processed now */
                0 /* don't care */
            );

            /* encode full granule */
            rio_PCS_PMA_DP_Encode_Granule(
                dp_Instance,
                &granule,
                &dp_Instance->streaming_Block_Data.out_Granule
            );

            /* upload it to the lower level */
            rio_PCS_PMA_DP_Set_Character_Byte_0(
                dp_Instance,
                &dp_Instance->streaming_Block_Data.out_Granule,
                0, /* byte num */
                character
            );
        }
        else
        if (indicator_Value < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1)
        {
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE,
                indicator_Value + 1, /* number of the byte which is being processed now */
                0 /* don't care */
            );

            /* upload it to the lower level */
            rio_PCS_PMA_DP_Set_Character_Byte_0(
                dp_Instance,
                &dp_Instance->streaming_Block_Data.out_Granule,
                (RIO_UCHAR_T)(indicator_Value + 1),
                character
            );
        }
        else
        /* error: incorrect number of the current byte */
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Get_Character_Column: discrepancy: incorrect number of the current byte"
            );
    }

    /* encoded character column is returned */
    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Put_Character_Column
 *
 * Description: notifies on reception of character
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Put_Character_Column(
    RIO_CONTEXT_T              handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
)
{
    RIO_PL_PCS_PMA_DP_DS_T *dp_Instance = (RIO_PL_PCS_PMA_DP_DS_T *)handle_Context;
    RIO_PCS_PMA_GRANULE_T granule;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;
    int result;
    
    if (dp_Instance == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (dp_Instance->is_In_Reset)
    {
#ifndef _CARBON_SRIO_XTOR
        printf("PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Put_Character_Column: is called while in reset\n");
#else
		dp_Instance->interfaces.rio_User_Msg(
				dp_Instance->interfaces.user_Msg_Context,
				RIO_PL_SERIAL_MSG_ERROR_MSG,"PCS/PMA DP: ERROR: rio_PCS_PMA_DP_Put_Character_Column: is called while in reset\n");
#endif
        return RIO_ERROR;
    }

    /* 
     * checking if the alignment was lost in the meantime
     */
    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_IS_SYNC_LOST,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    /* reset buffers upon loosing alignment */
        RIO_PCS_PMA_DP_SB_Initialize(&dp_Instance->streaming_Block_Data);

    dp_Instance->interfaces.rio_Get_State_Flag(
        dp_Instance->interfaces.rio_Regs_Context,
        dp_Instance->mum_Instanse,
        RIO_IS_PORT_TRUE_4X,
        &indicator_Value,
        &indicator_Parameter
    );
    if (RIO_TRUE == indicator_Value)
    /* true 4x link */
    {
        /* decode full granule */
        result = rio_PCS_PMA_DP_Decode_Granule(
            dp_Instance,
            character,
            &granule
        );
        if (RIO_NO_DATA == result)
        {
            /* IDLEs - not to be reported to the upper levels (PL, TxRxM) */
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_ARE_ALL_IDLES_IN_GRANULE,
                RIO_TRUE,
                0 /* don't care */
            );
        }
        dp_Instance->interfaces.put_Granule(
            dp_Instance->interfaces.granule_Level_Context,
            &granule
        );
        if (RIO_NO_DATA == result)
        {
            /* reset flag */
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_ARE_ALL_IDLES_IN_GRANULE,
                RIO_FALSE,
                0 /* don't care */
            );
        }
    }
    else
    /* 1x link */
    {
        if (1 != character->lane_Mask)
        /* error: unexpected lane_Mask (1 is expected) */
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Put_Character_Column: unexpected lane_Mask (1 is expected)"
            );

        dp_Instance->interfaces.rio_Get_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_SUPPRESS_HOOK,
            &indicator_Value,
            &indicator_Parameter
        );
        if (RIO_TRUE == indicator_Value)
            return
                dp_Instance->interfaces.put_Granule(
                    dp_Instance->interfaces.granule_Level_Context,
                    &granule
                );

        dp_Instance->interfaces.rio_Get_State_Flag(
            dp_Instance->interfaces.rio_Regs_Context,
            dp_Instance->mum_Instanse,
            RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE,
            &indicator_Value,
            &indicator_Parameter
        );

        if (0 == indicator_Value)
        {
            if (
                RIO_A == character->character_Type[0] ||
                RIO_K == character->character_Type[0] ||
                RIO_R == character->character_Type[0]
            )
            {
                CHECK_IDLES_INSIDE_PACKET;

                /* IDLEs - not to be reported to the upper levels (PL, TxRxM) */
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_ARE_ALL_IDLES_IN_GRANULE,
                    RIO_TRUE,
                    0 /* don't care */
                );
                dp_Instance->interfaces.put_Granule(
                    dp_Instance->interfaces.granule_Level_Context,
                    &granule
                );
                /* reset flag */
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_ARE_ALL_IDLES_IN_GRANULE,
                    RIO_FALSE,
                    0 /* don't care */
                );

                return RIO_OK;
            }
        }

        if (RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1 == indicator_Value)
        {
            /* place the byte to the granule */
            rio_PCS_PMA_DP_Get_Character_Byte_0(
                dp_Instance,
                character,
                &dp_Instance->streaming_Block_Data.in_Granule,
                (RIO_UCHAR_T)indicator_Value
            );
            /* decode full granule */
            result = rio_PCS_PMA_DP_Decode_Granule(
                dp_Instance,
                &dp_Instance->streaming_Block_Data.in_Granule,
                &granule
            );
            if (RIO_NO_DATA == result)
            {
                /* IDLEs - not to be reported to the upper levels (PL, TxRxM) */
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_ARE_ALL_IDLES_IN_GRANULE,
                    RIO_TRUE,
                    0 /* don't care */
                );
            }
            dp_Instance->interfaces.put_Granule(
                dp_Instance->interfaces.granule_Level_Context,
                &granule
            );
            if (RIO_NO_DATA == result)
            {
                /* reset flag */
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_ARE_ALL_IDLES_IN_GRANULE,
                    RIO_FALSE,
                    0 /* don't care */
                );
            }
            /* reset the byte counter */
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE,
                0, /* byte 0 is being processed now */
                0 /* don't care */
            );
        }
        else
        if (indicator_Value < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1)
        {
            /* place the byte to the granule */
            rio_PCS_PMA_DP_Get_Character_Byte_0(
                dp_Instance,
                character,
                &dp_Instance->streaming_Block_Data.in_Granule,
                (RIO_UCHAR_T)indicator_Value
            );
            /* increment the byte counter */
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE,
                indicator_Value + 1, /* number of the byte which is being processed now */
                0 /* don't care */
            );
        }
        else
        /* error: incorrect number of the current byte */
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Put_Character_Column: discrepancy: incorrect number of the current byte"
            );
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Encode_Granule
 *
 * Description: encodes granule to the character column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Encode_Granule(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_GRANULE_T     *granule,         /* pointer to the granule data structure */
    RIO_PCS_PMA_CHARACTER_T   *character        /* pointer to the character data structure */
)
{
    RIO_UCHAR_T i;
    RIO_WORD_T symbol;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;

    if (granule->is_Data)
    {
        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            character->character_Data[i] = granule->granule_Date[i];
            character->character_Type[i] = RIO_D;
            character->lane_Mask = RIO_PL_ALL_LANES_MASK;
	    /*printf("\n\n\tgranule->granule_Date[i]: %x\n",granule->granule_Date[i]);*/
        }
    }
    else
    {
        RIO_BOOL_T is_Crc_Valid;
        RIO_UCHAR_T crc;

        is_Crc_Valid = granule->granule_Struct.is_Crc_Valid;
        crc = granule->granule_Struct.crc;
	/*printf("\n\t(before)granule->granule_Struct.crc: %x\n",\
				granule->granule_Struct.crc);			GDA */
        /* we need to calculate CRC ourselves */
        rio_PCS_PMA_DP_Calculate_CRC(dp_Instance, granule);
	/* printf("\n\t(after)granule->granule_Struct.crc: %x\n",\
				granule->granule_Struct.crc);			GDA */
        /*if hecessary then corrupt CRC*/
        if (is_Crc_Valid == RIO_TRUE){
            granule->granule_Struct.crc = granule->granule_Struct.crc ^ crc;
	    /*printf("\n\t(inside if condi)granule->granule_Struct.crc: %x\n",\
				granule->granule_Struct.crc);			GDA */
	}
        symbol = granule->granule_Struct.stype0 & RIO_SYMBOL_STYPE0_MASK;
        symbol = (symbol << RIO_SYMBOL_PARAMETER0_SIZE) | (granule->granule_Struct.parameter0 & RIO_SYMBOL_PARAMETER0_MASK);
        symbol = (symbol << RIO_SYMBOL_PARAMETER1_SIZE) | (granule->granule_Struct.parameter1 & RIO_SYMBOL_PARAMETER1_MASK);
        symbol = (symbol << RIO_SYMBOL_STYPE1_SIZE) | (granule->granule_Struct.stype1 & RIO_SYMBOL_STYPE1_MASK);
        symbol = (symbol << RIO_SYMBOL_CMD_SIZE) | (granule->granule_Struct.cmd & RIO_SYMBOL_CMD_MASK);
        symbol = (symbol << RIO_SYMBOL_CRC_SIZE) | (granule->granule_Struct.crc & RIO_SYMBOL_CRC_MASK);

        /* choose control symbol */
        if (RIO_PL_SERIAL_GR_START_OF_PACKET_STYPE1 == granule->granule_Struct.stype1 ||
            RIO_PL_SERIAL_GR_STOMP_STYPE1 == granule->granule_Struct.stype1 ||
            RIO_PL_SERIAL_GR_EOP_STYPE1 == granule->granule_Struct.stype1
        )
            character->character_Type[0] = RIO_PD;
        else
        if (RIO_PL_SERIAL_GR_MULTICAST_EVENT_STYPE1 == granule->granule_Struct.stype1 ||
            RIO_PL_SERIAL_GR_NOP_STYPE1 == granule->granule_Struct.stype1
        )
            character->character_Type[0] = RIO_SC;
        else
        if (RIO_PL_SERIAL_GR_RFR_STYPE1 == granule->granule_Struct.stype1 ||
            RIO_PL_SERIAL_GR_LINK_REQUEST_STYPE1 == granule->granule_Struct.stype1
        )
        /* this case requires additional investigation of the status of transmitted packet */
        {
            dp_Instance->interfaces.rio_Get_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_SYMBOL_IS_PD,
                &indicator_Value,
                &indicator_Parameter
            );
            if (RIO_TRUE == indicator_Value)
            {
                character->character_Type[0] = RIO_PD;
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_SYMBOL_IS_PD,
                    RIO_FALSE, /* reset to be /SC/ */
                    0 /* don't care */
                );
            }
            else
                character->character_Type[0] = RIO_SC;
        }
        else
        /* error: unknown stype1 */
        {
            /*TBD: implement error for PL model*/
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_WARNING_MSG,
                "rio_PCS_PMA_DP_Encode_Granule: unknown stype1"
            );
            /*in case of reserved stype1 the first byte should be SC. 
            See serial spec chapter 3.4
            table 3-6 "stype1 control symbol encoding"*/
            character->character_Type[0] = RIO_SC;
        }

        character->character_Type[1] = RIO_D;
        character->character_Data[1] = (RIO_UCHAR_T)(symbol >> RIO_BYTE1_SHIFT);
        character->character_Type[2] = RIO_D;
        character->character_Data[2] = (RIO_UCHAR_T)(symbol >> RIO_BYTE2_SHIFT);
        character->character_Type[3] = RIO_D;
        character->character_Data[3] = (RIO_UCHAR_T)(symbol);
        character->lane_Mask = RIO_PL_ALL_LANES_MASK;

#ifdef DEBUG_SC_PD_CHOOSING
        if (DEBUG_INSTANCE_NUMBER == dp_Instance->instance_Number)
            printf("TRX(%i): CONTROL: %i, %i, %i, %i\n", dp_Instance->instance_Number,
                character->character_Type[0],
                character->character_Data[1],
                character->character_Data[2],
                character->character_Data[3]
            );
#endif  /* DEBUG_SC_PD_CHOOSING */

    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Decode_Granule
 *
 * Description: decodes character column to granule
 *
 * Returns: none
 *
 * Notes: this function is expected to be called only when all four
 *        characters of the column are collected
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Decode_Granule(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T   *character,       /* pointer to the character data structure */
    RIO_PCS_PMA_GRANULE_T     *granule          /* pointer to the granule data structure */
)
{
    RIO_UCHAR_T i;
    RIO_WORD_T symbol;
    unsigned long indicator_Value;
    unsigned int indicator_Parameter;
    RIO_UCHAR_T invalids_Counter;
    RIO_BOOL_T is_Wrong_SC_PD;

    invalids_Counter = 0;
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
    {
        if (RIO_I == character->character_Type[i])
        /* this is an INVALID granule */
        {
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Decode_Granule: granule has INVALID character"
            );

            /*put corrupted granule for BC event*/
            dp_Instance->interfaces.put_Corrupted_Granule(
                dp_Instance->interfaces.granule_Level_Context,
                NULL,          /* pointer to granule data structure */
                character,        /* pointer to character data structure */
                RIO_PCS_PMA_IC_IN_GRANULE
            );

            invalids_Counter++;
        }
        if (invalids_Counter > 0)
            return RIO_NO_DATA;
    }

    if (RIO_D == character->character_Type[0])
    /* this is a data granule */
    {
        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            if (RIO_D != character->character_Type[i])
            /* error: there are control characters not in the first byte of the granule */
            {
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_DP_Decode_Granule: there are control characters not in the first byte of the granule"
                );
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_TRUE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );

                /*put corrupted granule for BC event*/
                dp_Instance->interfaces.put_Corrupted_Granule(
                    dp_Instance->interfaces.granule_Level_Context,
                    NULL,          /* pointer to granule data structure */
                    character,        /* pointer to character data structure */
                    RIO_PCS_PMA_CC_NOT_IN_FIRST_GRANULE_BYTE
                );
            }

            granule->granule_Date[i] = character->character_Data[i];
            granule->is_Data = RIO_TRUE;
        }
    }
    else
    if (
        RIO_SC == character->character_Type[0] ||
        RIO_PD == character->character_Type[0]
    )
    /* control symbol */
    {

        if (
            RIO_PD == character->character_Type[0] &&
            RIO_TRUE == dp_Instance->is_Flag_Asserted[RIO_PCS_PMA_DP_FLAG_IDLES_INSIDE_PACKET]
        )
            /* clearing the flag to get it ready for next such error (a truly separate one) */
            dp_Instance->is_Flag_Asserted[RIO_PCS_PMA_DP_FLAG_IDLES_INSIDE_PACKET] = RIO_FALSE;

#ifdef DEBUG_SC_PD_CHOOSING
        if (DEBUG_INSTANCE_NUMBER == dp_Instance->instance_Number)
            printf("RCV(%i): CONTROL: %i, %i, %i, %i\n", dp_Instance->instance_Number,
                character->character_Type[0],
                character->character_Data[1],
                character->character_Data[2],
                character->character_Data[3]
            );
#endif  /* DEBUG_SC_PD_CHOOSING */

        /* check that bytes 1-3 are data */
        for (i = 1; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            if (RIO_D != character->character_Type[i])
            /* error: there are control characters not in the first byte of the granule */
            {
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_DP_Decode_Granule: there are control characters not in the first byte of the granule"
                );
                /* set "CRC Error" flag */
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_TRUE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );

                /*put corrupted granule for BC event*/
                dp_Instance->interfaces.put_Corrupted_Granule(
                    dp_Instance->interfaces.granule_Level_Context,
                    NULL,          /* pointer to granule data structure */
                    character,        /* pointer to character data structure */
                    RIO_PCS_PMA_CC_NOT_IN_FIRST_GRANULE_BYTE
                );
                
                return RIO_NO_DATA;
            }
        }

        symbol =
            (character->character_Data[1] << RIO_BYTE1_SHIFT) | 
            (character->character_Data[2] << RIO_BYTE2_SHIFT) |
            (character->character_Data[3]);

        granule->granule_Struct.crc = (RIO_UCHAR_T)(symbol & RIO_SYMBOL_CRC_MASK);
        symbol = symbol >> RIO_SYMBOL_CRC_SIZE;
        granule->granule_Struct.cmd =  (RIO_UCHAR_T)(symbol & RIO_SYMBOL_CMD_MASK);
        symbol = symbol >> RIO_SYMBOL_CMD_SIZE;
        granule->granule_Struct.stype1 =  (RIO_UCHAR_T)(symbol & RIO_SYMBOL_STYPE1_MASK);
        symbol = symbol >> RIO_SYMBOL_STYPE1_SIZE;
        granule->granule_Struct.parameter1 = (RIO_UCHAR_T)(symbol & RIO_SYMBOL_PARAMETER1_MASK);
        symbol = symbol >> RIO_SYMBOL_PARAMETER1_SIZE;
        granule->granule_Struct.parameter0 = (RIO_UCHAR_T)(symbol & RIO_SYMBOL_PARAMETER0_MASK);
        symbol = symbol >> RIO_SYMBOL_PARAMETER0_SIZE;
        granule->granule_Struct.stype0 =  (RIO_UCHAR_T)(symbol & RIO_SYMBOL_STYPE0_MASK);
        granule->is_Data = RIO_FALSE;

         for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            granule->granule_Date[i] = character->character_Data[i];


        /* 
         * check correctness of the SC/PD character
         */
        is_Wrong_SC_PD = RIO_FALSE;
        /* choose control symbol */
        if (
            (
                RIO_PL_SERIAL_GR_START_OF_PACKET_STYPE1 == granule->granule_Struct.stype1 ||
                RIO_PL_SERIAL_GR_STOMP_STYPE1 == granule->granule_Struct.stype1 ||
                RIO_PL_SERIAL_GR_EOP_STYPE1 == granule->granule_Struct.stype1
            ) &&
            RIO_PD != character->character_Type[0]
        )
        /* error: wrong control character: /PD/ is expected */
        {
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Decode_Granule: wrong control character: /PD/ is expected"
            );

            /*put corrupted granule for BC event*/
            dp_Instance->interfaces.put_Corrupted_Granule(
                dp_Instance->interfaces.granule_Level_Context,
                granule,          /* pointer to granule data structure */
                character,        /* pointer to character data structure */
                RIO_PCS_PMA_SC_GOT_INSTEAD_PD
            );

            is_Wrong_SC_PD = RIO_TRUE;
        }
        else
        if (
            (
                RIO_PL_SERIAL_GR_MULTICAST_EVENT_STYPE1 == granule->granule_Struct.stype1 ||
                RIO_PL_SERIAL_GR_NOP_STYPE1 == granule->granule_Struct.stype1
            ) &&
            RIO_SC != character->character_Type[0]
        )
        /* error: wrong control character: /SC/ is expected */
        {
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Decode_Granule: wrong control character: /SC/ is expected"
            );

            /*put corrupted granule for BC event*/
            dp_Instance->interfaces.put_Corrupted_Granule(
                dp_Instance->interfaces.granule_Level_Context,
                granule,          /* pointer to granule data structure */
                character,        /* pointer to character data structure */
                RIO_PCS_PMA_PD_GOT_INSTEAD_SC
            );

            is_Wrong_SC_PD = RIO_TRUE;
        }
        else
        if (RIO_PL_SERIAL_GR_RFR_STYPE1 == granule->granule_Struct.stype1 ||
            RIO_PL_SERIAL_GR_LINK_REQUEST_STYPE1 == granule->granule_Struct.stype1
        )
        /* this case requires additional investigation of the status of transmitted packet */
        {
            dp_Instance->interfaces.rio_Get_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_RX_IS_PD, /*RIO_RX_DATA_IN_PROGRESS*/
                &indicator_Value,
                &indicator_Parameter
            );
            if (
                RIO_TRUE == indicator_Value &&
                RIO_PD != character->character_Type[0]
            )
            /* error: wrong control character: /PD/ is expected */
            {
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_DP_Decode_Granule: wrong control character: /PD/ is expected"
                );
            
                /*put corrupted granule for BC event*/
                dp_Instance->interfaces.put_Corrupted_Granule(
                    dp_Instance->interfaces.granule_Level_Context,
                    granule,          /* pointer to granule data structure */
                    character,        /* pointer to character data structure */
                    RIO_PCS_PMA_SC_GOT_INSTEAD_PD
                );

                is_Wrong_SC_PD = RIO_TRUE;
            }
            else
            if (
                RIO_FALSE == indicator_Value &&
                RIO_SC != character->character_Type[0]
            )
            /* error: wrong control character: /SC/ is expected */
            {
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_DP_Decode_Granule: wrong control character: /SC/ is expected"
                );

                /*put corrupted granule for BC event*/
                dp_Instance->interfaces.put_Corrupted_Granule(
                    dp_Instance->interfaces.granule_Level_Context,
                    granule,          /* pointer to granule data structure */
                    character,        /* pointer to character data structure */
                    RIO_PCS_PMA_PD_GOT_INSTEAD_SC
                );

                is_Wrong_SC_PD = RIO_TRUE;
            }
        }

        if (is_Wrong_SC_PD)
        {
            /* for TxRx */
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_IS_SC_PD_INCORRECT,
                RIO_TRUE,
                0 /* don't care */
            );
            /* for PL */
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_INPUT_ERROR,
                RIO_TRUE,
                RIO_PL_PNACC_INVALID_CHARACTER
            );
        }

        rio_PCS_PMA_DP_Check_CRC(dp_Instance, granule);
        if (!granule->granule_Struct.is_Crc_Valid)
        {
            rio_PCS_PMA_DP_Display_Msg(
                dp_Instance,
                RIO_PL_SERIAL_MSG_ERROR_MSG,
                "rio_PCS_PMA_DP_Decode_Granule: wrong CRC on symbol"
            );
            /* set "CRC Error" flag */
            dp_Instance->interfaces.rio_Set_State_Flag(
                dp_Instance->interfaces.rio_Regs_Context,
                dp_Instance->mum_Instanse,
                RIO_INPUT_ERROR,
                RIO_TRUE,
                RIO_PL_PNACC_BAD_CONTROL
            );

            /*put corrupted granule for BC event*/
            dp_Instance->interfaces.put_Corrupted_Granule(
                dp_Instance->interfaces.granule_Level_Context,
                granule,          /* pointer to granule data structure */
                character,        /* pointer to character data structure */
                RIO_PCS_PMA_WRONG_SYMBOL_CRC
            );
        }
    }
    else
    if (
        RIO_K == character->character_Type[0] ||
        RIO_R == character->character_Type[0] ||
        RIO_A == character->character_Type[0]
    )
    /* idles */
    {
        
        CHECK_IDLES_INSIDE_PACKET;
        
        /* check that bytes 1-3 are data */
        for (i = 1; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
                
            if (
                RIO_K != character->character_Type[i] &&
                RIO_R != character->character_Type[i] &&
                RIO_A != character->character_Type[i]
            )
            /* error: there are non idles here */
            {
                rio_PCS_PMA_DP_Display_Msg(
                    dp_Instance,
                    RIO_PL_SERIAL_MSG_ERROR_MSG,
                    "rio_PCS_PMA_DP_Decode_Granule: there is a non idle in an idle graule"
                );
                dp_Instance->interfaces.rio_Set_State_Flag(
                    dp_Instance->interfaces.rio_Regs_Context,
                    dp_Instance->mum_Instanse,
                    RIO_INPUT_ERROR,
                    RIO_TRUE,
                    RIO_PL_PNACC_INVALID_CHARACTER
                );

                /*put corrupted granule for BC event*/
                dp_Instance->interfaces.put_Corrupted_Granule(
                    dp_Instance->interfaces.granule_Level_Context,
                    NULL,          /* pointer to granule data structure */
                    character,        /* pointer to character data structure */
                    RIO_PCS_PMA_NON_IDLE_IN_IDLE_SEQ
                );
            }
        }
        return RIO_NO_DATA;
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Set_Character_Byte_0
 *
 * Description: sets the character byte 0 from the supplied character column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Set_Character_Byte_0(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T   *in_Character,
    RIO_UCHAR_T               byte_Number,      /* byte number in the in_Character */
    RIO_PCS_PMA_CHARACTER_T   *out_Character
)
{
    if (
        in_Character == NULL ||
        out_Character == NULL
    )
        rio_PCS_PMA_DP_Display_Msg(
            dp_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_DP_Set_Character_Byte_0: input parameter is a NULL pointer"
        );

    out_Character->character_Type[0] = in_Character->character_Type[byte_Number];
    out_Character->character_Data[0] = in_Character->character_Data[byte_Number];
    out_Character->lane_Mask = 1; /* byte 0 is the only active */

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Get_Character_Byte_0
 *
 * Description: gets the character byte 0 from the supplied character column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Get_Character_Byte_0(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_CHARACTER_T   *in_Character,
    RIO_PCS_PMA_CHARACTER_T   *out_Character,
    RIO_UCHAR_T               byte_Number       /* byte number in the in_Character */
)
{
    if (
        in_Character == NULL ||
        out_Character == NULL
    )
        rio_PCS_PMA_DP_Display_Msg(
            dp_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_DP_Get_Character_Byte_0: input parameter is a NULL pointer"
        );

    out_Character->character_Type[byte_Number] = in_Character->character_Type[0];
    out_Character->character_Data[byte_Number] = in_Character->character_Data[0];
    out_Character->lane_Mask = RIO_PL_ALL_LANES_MASK;

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Calculate_CRC
 *
 * Description: calculates CRC for the control symbol
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Calculate_CRC(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_GRANULE_T     *granule          /* pointer to the granule data structure */
)
{
    unsigned char i;
    unsigned char j;
    RIO_BOOL_T temp_CRC[RIO_SYMBOL_CRC_SIZE];
    RIO_WORD_T symbol;
    RIO_BOOL_T temp_Symbol[RIO_SYMBOL_CRC_CODING_TABLE_SIZE];

    if (granule == NULL)
        rio_PCS_PMA_DP_Display_Msg(
            dp_Instance,
            RIO_PL_SERIAL_MSG_ERROR_MSG,
            "rio_PCS_PMA_DP_Set_Character_Byte_0: input parameter is a NULL pointer"
        );

    symbol = granule->granule_Struct.stype0 & RIO_SYMBOL_STYPE0_MASK;
    symbol = (symbol << RIO_SYMBOL_PARAMETER0_SIZE) | (granule->granule_Struct.parameter0 & RIO_SYMBOL_PARAMETER0_MASK);
    symbol = (symbol << RIO_SYMBOL_PARAMETER1_SIZE) | (granule->granule_Struct.parameter1 & RIO_SYMBOL_PARAMETER1_MASK);
    symbol = (symbol << RIO_SYMBOL_STYPE1_SIZE) | (granule->granule_Struct.stype1 & RIO_SYMBOL_STYPE1_MASK);
    symbol = (symbol << RIO_SYMBOL_CMD_SIZE) | (granule->granule_Struct.cmd & RIO_SYMBOL_CMD_MASK);

    for (i = 0; i < RIO_SYMBOL_CRC_CODING_TABLE_SIZE; i++)
    {
        temp_Symbol[RIO_SYMBOL_CRC_CODING_TABLE_SIZE - 1 - i] = symbol & 1;
        symbol = symbol >> 1;
    }
    
    for (j = 0; j < RIO_SYMBOL_CRC_SIZE; j++)
        temp_CRC[j] = RIO_FALSE;

    for (i = 0; i < RIO_SYMBOL_CRC_CODING_TABLE_SIZE; i++)
    {
        for (j = 0; j < RIO_SYMBOL_CRC_SIZE; j++)
        {
            if ((char)(-1) == symbol_CRC_Coding_Table[i][j])
                temp_CRC[j] ^= !temp_Symbol[i];
            if (1 == symbol_CRC_Coding_Table[i][j])
                temp_CRC[j] ^= temp_Symbol[i];
            //else
            /* XOR with 0 doesn't change the value */
                ;
        }
    }

    granule->granule_Struct.crc = 0;
    for (j = 0; j < RIO_SYMBOL_CRC_SIZE; j++)
    {
        granule->granule_Struct.crc |= temp_CRC[j];
        if (j < RIO_SYMBOL_CRC_SIZE - 1)
            granule->granule_Struct.crc = (unsigned char)(granule->granule_Struct.crc << 1);
    }
    granule->granule_Struct.is_Crc_Valid = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_PCS_PMA_DP_Check_CRC
 *
 * Description: checks CRC for the control symbol
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int rio_PCS_PMA_DP_Check_CRC(
    RIO_PL_PCS_PMA_DP_DS_T    *dp_Instance,
    RIO_PCS_PMA_GRANULE_T     *granule          /* pointer to the granule data structure */
)
{
    RIO_UCHAR_T crc;

    crc = granule->granule_Struct.crc;
    /* re-calculate granule->granule_Struct.crc */
    rio_PCS_PMA_DP_Calculate_CRC(dp_Instance, granule);
    if (crc != granule->granule_Struct.crc)
        granule->granule_Struct.is_Crc_Valid = RIO_FALSE;
    else
        granule->granule_Struct.is_Crc_Valid = RIO_TRUE;
    /* place the original CRC back */
    granule->granule_Struct.crc = crc;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PCS_PMA_DP_SB_Initialize
 *
 * Description: initialize SB
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PCS_PMA_DP_SB_Initialize(
    RIO_PL_PCS_PMA_DP_SB_DS_T          *dp_SB_Instance
)
{
    /*
     * check handle isn't NULL, check parameter set is valid,
     * check that the instance is in reset mode,
     * set link parameters to necessary values and turn the
     * instance to work mode.
     */
    if (dp_SB_Instance == NULL)
        return RIO_ERROR;

    ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Set_State_Flag(
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Regs_Context,
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->mum_Instanse,
        RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE,
        RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1,
        0 /* don't care */
    );
    ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Set_State_Flag(
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Regs_Context,
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->mum_Instanse,
        RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE,
        0, /* byte #0 */
        0 /* don't care */
    );
    ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Set_State_Flag(
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Regs_Context,
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->mum_Instanse,
        RIO_ARE_ALL_IDLES_IN_GRANULE,
        RIO_FALSE,
        0 /* don't care */
    );

    ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Set_State_Flag(
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->interfaces.rio_Regs_Context,
        ((RIO_PL_PCS_PMA_DP_DS_T *)(dp_SB_Instance->mum_Instanse))->mum_Instanse,
        RIO_IS_SYNC_LOST,
        RIO_FALSE,
        0 /* don't care */
    );

    dp_SB_Instance->out_Granule.lane_Mask = 0;
    dp_SB_Instance->in_Granule.lane_Mask = 0;

    return RIO_OK;
}


/*****************************************************************************/
