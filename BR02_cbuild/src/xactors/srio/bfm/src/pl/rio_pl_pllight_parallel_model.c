/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/rio_pl_pllight_parallel_model.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  RapidIO PL parallel model 
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "rio_pl_pllight_parallel_model.h"
#include "rio_pl_pllight_model_common.h"

#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_rx.h"
#include "rio_pl_tx.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"

#include "rio_32_txrx_model.h"
#include "rio_parallel_init.h"

/* count of LP-EP interfaces*/
#define RIO_PL_COUNT_OF_LPEP 1

/* count of LP-EP interfaces*/
#define RIO_PL_PARALLEL_ACKID_QUEUE_SIZE 8 

/* variable for number of created instanses counting */
static unsigned long pl_Instances_Count = 0;

/*
 * prototypes for static functions to be passed via ftray/ctray
 */

static int pl_Model_Start_Reset(
    RIO_HANDLE_T handle
    );

static void pl_Commands_Handler(
    RIO_HANDLE_T                handle,
    RIO_COMMAND_PL_WRAP_T       cmd
    );

static int pl_Model_Initialize(
    RIO_HANDLE_T                handle,
    RIO_PL_PARALLEL_PARAM_SET_T                 *param_Set
    );

static int pl_Clock_Edge(RIO_HANDLE_T handle);

static int pl_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_BYTE_T frame,    /* receive frame */
    RIO_BYTE_T rd,       /* receive data */
    RIO_BYTE_T rdl,      /* receive data low part */
    RIO_BYTE_T rclk      /* receive clock */
    );

static int pl_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_BYTE_T tframe,     /* transmit frame */
    RIO_BYTE_T td,         /* transmit data */
    RIO_BYTE_T tdl,        /* transmit data low part */
    RIO_BYTE_T tclk        /* transmit clock */
    );

static int pl_Input_Present(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_BOOL_T    input_Present /* pinter to granule data structure */
    );

static int pl_Granule_Received(
    RIO_CONTEXT_T         context,
    RIO_GRANULE_STRUCT_T  *symbol_Struct      /* struct, containing symbol's fields */
    );

static int pl_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
    );

static int pl_Delete_Instance(
    RIO_HANDLE_T          handle
    );

#ifdef _CARBON_SRIO_XTOR_FIX
void multicast_Delay_Time_Out(RIO_PL_DS_T *handle);
#endif

/*
 * Prototypes for static functions used inside the file
 */

static int pl_Create_Bind_Link(RIO_PL_DS_T *handle);
static int pl_Initialize_Instance(RIO_PL_DS_T *instanse, RIO_PL_PARAM_SET_T *param_Set);
static int check_Init_Param_Set(RIO_PL_DS_T *instanse, RIO_PL_PARALLEL_PARAM_SET_T *param_Set);
static int check_Inst_Param_Set(RIO_MODEL_INST_PARAM_T *param_Set);
static void count_Time_Outs(RIO_PL_DS_T *handle);
static int pl_Lost_Sync_Command(RIO_HANDLE_T handle);

static int pl_Force_EOP_Insertion(
    RIO_HANDLE_T handle,  
    RIO_BOOL_T enable_Force);




/* 
 * External Physical Layer and Physical Layer Light functions implementation
 */

/***************************************************************************
 * Function : RIO_PL_PLLight_Parallel_Model_Create_Instance
 *
 * Description: Create new physical layer instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_PL_PLLight_Parallel_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_MODEL_INST_PARAM_T  *param_Set
    )
{
    /* handle to structure will allocated*/
    RIO_PL_DS_T *pl_Instanse = NULL;

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for physical layer 
     * data structure
     */
    if (handle == NULL || 
        param_Set == NULL || 
        check_Inst_Param_Set(param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* clear handle */
    *handle = NULL;

    /* allocate instanse */
    if ((pl_Instanse = malloc(sizeof(RIO_PL_DS_T))) == NULL)
        return RIO_ERROR;

    /* mark the instance as the parallel one */
    pl_Instanse->is_Parallel_Model = RIO_TRUE;

    /* mark the instance as not bound one */
    pl_Instanse->was_Bound = RIO_FALSE;

    /* allocate mamory for LP-EP registers */
    pl_Instanse->reg_Set.lpep_Count  = RIO_PL_COUNT_OF_LPEP;
        pl_Instanse->reg_Set.lpep_Size   = RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP; 
    if ((pl_Instanse->reg_Set.lpep_Regs = malloc(sizeof(RIO_PL_LPEP_REGS_T) * pl_Instanse->reg_Set.lpep_Count)) == NULL)
    {
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for outboud buffer */
    if ((pl_Instanse->outbound_Buf.buffer = 
        malloc(sizeof(RIO_PL_OUT_ENTRY_T) * param_Set->pl_Outbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for sending queue */
    if ((pl_Instanse->outbound_Buf.send_Queue = 
        malloc(sizeof(RIO_PL_QUEUE_ELEM_T) * param_Set->pl_Outbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for inbound buffer */
    if ((pl_Instanse->inbound_Buf.buffer = 
        malloc(sizeof(RIO_PL_IN_ENTRY_T) * param_Set->pl_Inbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for inbound queue */
    if ((pl_Instanse->inbound_Buf.rec_Queue = 
        malloc(sizeof(RIO_PL_QUEUE_ELEM_T) * param_Set->pl_Inbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /*set size of ackID queues*/
    pl_Instanse->ack_Buffer_Size = RIO_PL_PARALLEL_ACKID_QUEUE_SIZE;
    
    /*allocate memory for ackID queues*/
    if ((pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer = 
        malloc(sizeof(RIO_PL_OUT_T) * pl_Instanse->ack_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    if ((pl_Instanse->tx_Data.ack_Out_Buffer.ack_Buffer = 
        malloc(sizeof(RIO_GRANULE_T) * pl_Instanse->ack_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer);
        free(pl_Instanse);
        return RIO_ERROR;
    }


    if ((pl_Instanse->specific_Param_Set = 
        malloc(sizeof(RIO_PARALLEL_PARAM_SET_T))) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer);
        free(pl_Instanse->tx_Data.ack_Out_Buffer.ack_Buffer);
        free(pl_Instanse);
        return RIO_ERROR;
    }    
    /*
     * clear pointers to external callbacks 
     */
    pl_Instanse->ext_Interfaces.rio_PL_Ack_Request     = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Get_Req_Data    = NULL;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    pl_Instanse->ext_Interfaces.rio_PL_Get_Req_Data_Hw = NULL; 
    /* GDA: Bug#124 */

    pl_Instanse->ext_Interfaces.rio_PL_Ack_Response    = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Get_Resp_Data   = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Req_Time_Out    = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Remote_Request  = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Remote_Response = NULL;
    pl_Instanse->ext_Interfaces.pl_Interface_Context   = NULL; 

    pl_Instanse->ext_Interfaces.rio_User_Msg = NULL;
    pl_Instanse->ext_Interfaces.rio_Msg      = NULL;

    pl_Instanse->ext_Interfaces.extend_Reg_Read    = NULL;
    pl_Instanse->ext_Interfaces.extend_Reg_Write   = NULL;
    pl_Instanse->ext_Interfaces.extend_Reg_Context = NULL;

    pl_Instanse->ext_Interfaces.rio_Request_Received  = NULL;
    pl_Instanse->ext_Interfaces.rio_Response_Received = NULL;
    pl_Instanse->ext_Interfaces.rio_Symbol_To_Send    = NULL;
    pl_Instanse->ext_Interfaces.rio_Packet_To_Send    = NULL;
    pl_Instanse->ext_Interfaces.rio_Hooks_Context     = NULL;

    /* GDA: Bug#125 - Support for Flow control packet */
    pl_Instanse->ext_Interfaces.rio_PL_FC_To_Send     = NULL;
    /* GDA: Bug#125 */
    
    /* allocate memory for serial callback tray */
    pl_Instanse->ext_Specific_Interfaces = (RIO_HANDLE_T)malloc(sizeof(RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T));
    memset((void*)(pl_Instanse->ext_Specific_Interfaces), 0 , sizeof(RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T)); 

    /*
     * Allocate memory for 32-bit TXRX FTray and clear it
     */
    pl_Instanse->link_Instanse  = NULL;
    pl_Instanse->link_Interface = (RIO_HANDLE_T)malloc(sizeof(RIO_LINK_MODEL_FTRAY_T));
    ((RIO_LINK_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Link_Get_Pins    = NULL;   
    ((RIO_LINK_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Link_Clock_Edge  = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Link_Start_Reset = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Link_Initialize  = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Link_Tx_Disabling = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Link_Rx_Disabling = NULL;

    /*
     *clear parallel init block pointers
     */
    pl_Instanse->mid_Block_Instanse  = NULL;
    pl_Instanse->mid_Block_Interface = (RIO_HANDLE_T)malloc(sizeof(RIO_PL_PAR_INIT_BLOCK_FTRAY_T));
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Start_Reset = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Initialize  = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Get_Granule = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Put_Granule = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Delete_Instance = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Clock_Edge = NULL;

    /* try create and bind a link */
    if (pl_Create_Bind_Link(pl_Instanse) == RIO_ERROR)
    {
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer);
        free(pl_Instanse->tx_Data.ack_Out_Buffer.ack_Buffer);
        free(pl_Instanse->specific_Param_Set);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /*allocation was successful */
    pl_Instanse->instance_Number = ++pl_Instances_Count;
    pl_Instanse->in_Reset = RIO_TRUE;
    pl_Instanse->inst_Param_Set = *param_Set;
    pl_Instanse->tid = 0;
    pl_Instanse->restart_Function = (RIO_PL_II_MODEL_INITIALIZE)pl_Model_Initialize;
    pl_Instanse->reset_Function   = pl_Model_Start_Reset;
    pl_Instanse->cmd_To_Wrap      = pl_Commands_Handler;

    /* set handle for the environment */
    *handle = (RIO_HANDLE_T)pl_Instanse;

    /*initialize the random number generator*/
    /* srand(0); */
    /*srand( (unsigned)time( NULL ) );*/

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_PLLight_Parallel_Model_Get_Function_Tray
 *
 * Description: export physical layer instance function tray
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_PLLight_Parallel_Model_Get_Function_Tray(
    RIO_PL_PARALLEL_MODEL_FTRAY_T *ftray             
    )
{
    /*
     * check passed poinetrs and if they are valid compete functional
     * tray with actual entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->rio_PL_Clock_Edge            = pl_Clock_Edge;
    ftray->rio_PL_Model_Start_Reset     = pl_Model_Start_Reset;
    ftray->rio_PL_Model_Initialize      = pl_Model_Initialize;
    ftray->rio_PL_Get_Pins              = pl_Get_Pins;

    ftray->rio_PL_Print_Version         = RIO_PL_Print_Version;

    ftray->rio_PL_Lost_Sync_Command     = pl_Lost_Sync_Command;
    
    ftray->rio_PL_Delete_Instance       = pl_Delete_Instance;
    ftray->rio_PL_Enable_Rnd_Idle_Generator = pl_Enable_Rnd_Idle_Generator;
    ftray->rio_PL_Force_EOP_Insertion = pl_Force_EOP_Insertion;

    ftray->rio_PL_Set_Retry_Generator = RIO_PL_Set_Retry_Generator;
    ftray->rio_PL_Set_PNACC_Generator = RIO_PL_Set_PNACC_Generator;
    ftray->rio_PL_Set_Stomp_Generator = RIO_PL_Set_Stomp_Generator;
    ftray->rio_PL_Set_LRQR_Generator  = RIO_PL_Set_LRQR_Generator;

    /*GDA: Rnd Generator (PNACC/RETRY) */
    ftray->rio_PL_Enable_RND_Pnacc_Generator_Init =  RIO_PL_Enable_RND_Pnacc_Generator_Init;
    ftray->rio_PL_Enable_RND_Retry_Generator_Init =  RIO_PL_Enable_RND_Retry_Generator_Init;
    /*GDA: Rnd Generator (PNACC/RETRY) */

    /*GDA: Multicast Contro Symbol Bug#116*/
    ftray->rio_PL_Generate_Multicast =  RIO_PL_Generate_Multicast;
    /*GDA: Multicast Contro Symbol Bug#116*/

    return RIO_OK;
}


/***************************************************************************
  Static functions to be passed via ftray/ctray implementation
***************************************************************************/

/***************************************************************************
 * Function : pl_Commands_Handler
 *
 * Description: hadler of commands from the pl core
 *
 * Returns: 
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Commands_Handler(
    RIO_HANDLE_T                handle,
    RIO_COMMAND_PL_WRAP_T       cmd)
{
    (void)cmd;
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL) return;
}

/***************************************************************************
 * Function : pl_Model_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Model_Start_Reset(
    RIO_HANDLE_T handle
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

    /* check that the instance is the parallel model */
    if ( !instanse->is_Parallel_Model )
    {
        if ( instanse->was_Bound != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_PL_Msg(
                instanse,
                RIO_PL_MSG_ERROR_MSG,
                "Error: It is not allowed to call 8/16 LP-LVDS specific API"
                " functions while the model is configured to work with the 1x/4x LP-Serial link. \n"
            );
            return RIO_ERROR;
        }
    }

    /* check pointers */
    if (instanse == NULL ||
        ((RIO_LINK_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Link_Start_Reset == NULL)
        return RIO_ERROR;

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;

    /* send reset to the link */
    if (((RIO_LINK_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Link_Start_Reset(
        instanse->link_Instanse) == RIO_ERROR
        )
        return RIO_ERROR;

    if (((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Start_Reset(
        instanse->mid_Block_Instanse) == RIO_ERROR)
        return RIO_ERROR;

    /* modify state to be _in_reset_ */
    instanse->in_Reset = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Model_Initialize
 *
 * Description: turn the instanse to work mode and initialize
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Model_Initialize(
    RIO_HANDLE_T                handle,
    RIO_PL_PARALLEL_PARAM_SET_T *param_Set
    )
{
    /* instanse handle and link's parameters set */
    RIO_PL_DS_T          *instanse = (RIO_PL_DS_T*)handle;
    RIO_LINK_PARAM_SET_T link_Param_Set;
    RIO_PAR_INIT_BLOCK_PARAM_SET_T init_Param_Set;

    unsigned int i;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

    /* check that the instance is the parallel model */
    if ( !instanse->is_Parallel_Model )
    {
        if ( instanse->was_Bound != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_PL_Msg(
                instanse,
                RIO_PL_MSG_ERROR_MSG,
                "Error: It is not allowed to call 8/16 LP-LVDS specific API"
                " functions while the model is configured to work with the 1x/4x LP-Serial link. \n"
            );
            return RIO_ERROR;
        }
    }

    /* check pointers */
    if (instanse == NULL || param_Set == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (instanse->in_Reset == RIO_FALSE)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_3);
        return RIO_ERROR;
    }

    /* check parameters set */
    if (check_Init_Param_Set(instanse, param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* prepare link's parameters set */
    link_Param_Set.lp_Ep_Is_8     = param_Set->lp_Ep_Is_8;
    link_Param_Set.work_Mode_Is_8 = param_Set->work_Mode_Is_8;

    /* link_Param_Set.single_Data_Rate = param_Set->single_Data_Rate;*/
    link_Param_Set.single_Data_Rate     = RIO_FALSE;
    link_Param_Set.check_SECDED         = RIO_FALSE;
    link_Param_Set.correct_Using_SECDED = RIO_FALSE;

    /*necessary to detect if to toggle frame automatically or not*/
    link_Param_Set.is_TxRx_Model = RIO_FALSE;

    if (param_Set->custom_Training_Pattern != NULL)
        for (i = 0; i < RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE; i++)
                link_Param_Set.training_Pattern[i]    = param_Set->custom_Training_Pattern[i];      
    else
    {
        /*the constants are used only here and protocol specification dependent*/
        if (link_Param_Set.work_Mode_Is_8 == RIO_TRUE)
        {
            link_Param_Set.training_Pattern[0]    = 0xffffffff;     
            link_Param_Set.training_Pattern[1]    = 0x00;
        }
        else
        {

            link_Param_Set.training_Pattern[0]    = 0xffffffff;     
            link_Param_Set.training_Pattern[1]    = 0xffffffff;     
            link_Param_Set.training_Pattern[2]    = 0x0;     
            link_Param_Set.training_Pattern[3]    = 0x0;     
        }
    }

    /* try to initialize the link */
    if (((RIO_LINK_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Link_Initialize(
        instanse->link_Instanse,
        &link_Param_Set) == RIO_ERROR)
        return RIO_ERROR;

        init_Param_Set.is_Init_Proc_On = RIO_TRUE;
        init_Param_Set.num_Trainings = param_Set->num_Trainings;
        init_Param_Set.requires_Window_Alignment = param_Set->requires_Window_Alignment;
        init_Param_Set.transmit_Flow_Control_Support = param_Set->transmit_Flow_Control_Support;

    if (((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(instanse->mid_Block_Interface))->rio_PL_Par_Init_Block_Initialize(
        instanse->mid_Block_Instanse,
        &init_Param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* save parameters and initialize the instanse */
    instanse->parameters_Set.transp_Type                   = param_Set->transp_Type;
    instanse->parameters_Set.dev_ID                        = param_Set->dev_ID;                       
    instanse->parameters_Set.lcshbar                       = param_Set->lcshbar;                      
    instanse->parameters_Set.lcsbar                        = param_Set->lcsbar;                       
    instanse->parameters_Set.is_Host                       = param_Set->is_Host;                      
    instanse->parameters_Set.is_Master_Enable              = param_Set->is_Master_Enable;             
    instanse->parameters_Set.is_Discovered                 = param_Set->is_Discovered;                
    instanse->parameters_Set.ext_Address                   = param_Set->ext_Address;                  
    instanse->parameters_Set.ext_Address_16                = param_Set->ext_Address_16;               
    instanse->parameters_Set.ext_Mailbox_Support           = param_Set->ext_Mailbox_Support;
    instanse->parameters_Set.input_Is_Enable               = param_Set->input_Is_Enable;              
    instanse->parameters_Set.output_Is_Enable              = param_Set->output_Is_Enable;             
    instanse->parameters_Set.res_For_3_Out                 = param_Set->res_For_3_Out;                
    instanse->parameters_Set.res_For_2_Out                 = param_Set->res_For_2_Out;                
    instanse->parameters_Set.res_For_1_Out                 = param_Set->res_For_1_Out;                
    instanse->parameters_Set.res_For_3_In                  = param_Set->res_For_3_In;                 
    instanse->parameters_Set.res_For_2_In                  = param_Set->res_For_2_In;                 
    instanse->parameters_Set.res_For_1_In                  = param_Set->res_For_1_In;                 
    instanse->parameters_Set.pass_By_Prio                  = param_Set->pass_By_Prio;                 
    instanse->parameters_Set.transmit_Flow_Control_Support = param_Set->transmit_Flow_Control_Support;
    instanse->parameters_Set.lp_Ep_Is_8                    = param_Set->lp_Ep_Is_8;                   
    instanse->parameters_Set.work_Mode_Is_8                = param_Set->work_Mode_Is_8;               
    instanse->parameters_Set.num_Trainings                 = param_Set->num_Trainings;                
    instanse->parameters_Set.requires_Window_Alignment     = param_Set->requires_Window_Alignment;    
    instanse->parameters_Set.enable_Retry_Return_Code      = param_Set->enable_Retry_Return_Code;
    instanse->parameters_Set.enable_LRQ_Resending          = param_Set->enable_LRQ_Resending;
     /*GDA:Bug#139 remove the need to recompute crc */
    instanse->data_Corrupted                 = param_Set->data_Corrupted;
     /*GDA:Bug#139 remove the need to recompute crc */

        for (i = 0; i < RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE; i++)
                instanse->parameters_Set.custom_Training_Pattern[i]    = param_Set->custom_Training_Pattern[i];      
    instanse->parameters_Set.receive_Idle_Lim              = param_Set->receive_Idle_Lim;             
    instanse->parameters_Set.throttle_Idle_Lim             = param_Set->throttle_Idle_Lim;             
 
    *((RIO_PL_PARALLEL_PARAM_SET_T *)(instanse->specific_Param_Set)) = *param_Set;
   
    for (i = 0; i < RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE; i++)
        ((RIO_PL_PARALLEL_PARAM_SET_T *)instanse->specific_Param_Set)->custom_Training_Pattern[i] 
            = param_Set->custom_Training_Pattern[i];

    instanse->in_Reset = RIO_FALSE;

    if (pl_Initialize_Instance(instanse, &(instanse->parameters_Set)) == RIO_ERROR) 
        return RIO_ERROR;

    return RIO_OK;
}
/***************************************************************************
 * Function : pl_Clock_Edge
 *
 * Description: physical layer clock
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Clock_Edge(RIO_HANDLE_T handle)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    int clock_Edge_Result;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    /*
     * check handle and callback then return and check the result
     */
    if (instanse == NULL ||
        ((RIO_LINK_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Link_Clock_Edge == NULL ||
        instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if ((clock_Edge_Result = ((RIO_LINK_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Link_Clock_Edge(
        instanse->link_Instanse)) == RIO_ERROR)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_2);
    }
    
    /* count all pl time outs */
    count_Time_Outs(instanse);
     
    /* try unloading external request */
    RIO_PL_In_Not_About_Request(instanse);

    return clock_Edge_Result;
}

/***************************************************************************
 * Function : pl_Set_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_BYTE_T tframe,     /* transmit frame */
    RIO_BYTE_T td,         /* transmit data */
    RIO_BYTE_T tdl,        /* transmit data low part */
    RIO_BYTE_T tclk        /* transmit clock */
    )
{
    RIO_PL_DS_T *instanse;
    int         set_Pins_Result;

    /* check that not NULL instance handle is suplied */
    if ( context == NULL )
        return RIO_ERROR;

    instanse = (RIO_PL_DS_T*)context;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

    /* check that the instance is the parallel model */
    if ( !instanse->is_Parallel_Model )
    {
        if ( instanse->was_Bound != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_PL_Msg(
                instanse,
                RIO_PL_MSG_ERROR_MSG,
                "Error: It is not allowed to call 8/16 LP-LVDS specific API"
                " functions while the model is configured to work with the 1x/4x LP-Serial link. \n"
            );
            return RIO_ERROR;
        }
    }

    /* check pointers */
    if (instanse == NULL || 
        ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_PL_Set_Pins == NULL)
        return RIO_ERROR;

    /* invoke environments callback */
    if ((set_Pins_Result = ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_PL_Set_Pins(
        ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->lpep_Interface_Context,
        tframe,
        td,
        tdl,
        tclk)) == RIO_ERROR)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_5);
    }

    return set_Pins_Result;
}

/***************************************************************************
 * Function : pl_Get_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_BYTE_T frame,    /* receive frame */
    RIO_BYTE_T rd,       /* receive data */
    RIO_BYTE_T rdl,      /* receive data low part */
    RIO_BYTE_T rclk      /* receive clock */
    )
{
    RIO_PL_DS_T *instanse;
    int         get_Pins_Result;

    /* check that not NULL instance handle is supplied */
    if ( handle == NULL )
        return RIO_ERROR;

    instanse = (RIO_PL_DS_T*)handle;

    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    if (instanse == NULL ||
        ((RIO_LINK_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Link_Get_Pins == NULL)
        return RIO_ERROR;

    /* invoke link's function */
    if ((get_Pins_Result = ((RIO_LINK_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Link_Get_Pins(
        instanse->link_Instanse,
        frame,
        rd,
        rdl,
        rclk)) == RIO_ERROR)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_8);
    }
     
    return get_Pins_Result;
}

/***************************************************************************
 * Function : pl_Input_Present
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Input_Present(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_BOOL_T    input_Present /* pinter to granule data structure */
    )
{
    unsigned long rec_State;

    /* instanse handler */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T *)context;

    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    if (instanse == NULL || handle == NULL)
        return RIO_ERROR;

    /* check if call comes from proper link */
    if (instanse->link_Instanse != handle)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_7);
        return RIO_ERROR;
    }

    /* change receiver state in the register */
    RIO_PL_Read_Register(instanse, RIO_LINK_ERROR_A + instanse->inst_Param_Set.entry_Extended_Features_Ptr,
                        &rec_State, RIO_TRUE);
    
    if (instanse->parameters_Set.requires_Window_Alignment == RIO_TRUE)
    {
        /* clear receiver bits*/
        rec_State &= ~RIO_PL_ERROR_STATE_PRESENT;
        /* set stopped due to error bit */
        rec_State |= (input_Present == RIO_TRUE ? RIO_PL_ERROR_STATE_PRESENT : 0x0);
    }
    else
    {
        rec_State &= ~RIO_PL_ERROR_STATE_PRESENT;
        rec_State &= ~RIO_PL_ERROR_STATE_UNINIT;
        rec_State |= (input_Present == RIO_TRUE ? RIO_PL_ERROR_STATE_PRESENT : 0x0);
        rec_State |= (input_Present == RIO_TRUE ? RIO_PL_ERROR_STATE_OK : 0x0);
    }
        
    RIO_PL_Write_Register(instanse, RIO_LINK_ERROR_A + instanse->inst_Param_Set.entry_Extended_Features_Ptr,
                        rec_State, RIO_TRUE);

    
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Symbol_To_Send
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
    )
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL)
        return RIO_ERROR;

    /* invoke environments callback */
    if(((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Symbol_To_Send != NULL)
        ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Symbol_To_Send(
            ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Hooks_Context,
            symbol_Buffer,
            buffer_Size
        );
    

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Granule_Received
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Granule_Received(
    RIO_CONTEXT_T         context,
    RIO_GRANULE_STRUCT_T  *symbol_Struct      /* struct, containing symbol's fields */
    )
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL) 
        return RIO_ERROR;

    /* invoke environments callback */
    if (((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Granule_Received != NULL)
        ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Granule_Received(
            ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Hooks_Context,
            symbol_Struct
        );
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Delete_Instance
 *
 * Description: deletes instance of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/

static int pl_Delete_Instance(
    RIO_HANDLE_T          handle
    )
{ 
    RIO_PL_DS_T *pl_Instanse = (RIO_HANDLE_T)handle;

    /* check pointers */
    if (pl_Instanse == NULL) 
        return RIO_ERROR;

        /* check that the instance was created as parallel one */
    if (!pl_Instanse->is_Parallel_Model)
        return RIO_ERROR;

    ((RIO_LINK_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Link_Delete_Instanse(pl_Instanse->link_Instanse);

    if (pl_Instanse->inbound_Buf.rec_Queue != NULL)
        free(pl_Instanse->inbound_Buf.rec_Queue);

    if (pl_Instanse->inbound_Buf.buffer != NULL)
        free(pl_Instanse->inbound_Buf.buffer);

    if (pl_Instanse->outbound_Buf.send_Queue != NULL)
        free(pl_Instanse->outbound_Buf.send_Queue);

    if (pl_Instanse->outbound_Buf.buffer != NULL)
        free(pl_Instanse->outbound_Buf.buffer);

    if (pl_Instanse->reg_Set.lpep_Regs != NULL)
        free(pl_Instanse->reg_Set.lpep_Regs);

    if (pl_Instanse != NULL)
        free(pl_Instanse);

    return RIO_OK; 

}


/***************************************************************************
  Static functions and macros which are used in this file only
***************************************************************************/


/***************************************************************************
 * Function : pl_Create_Bind_Link
 *
 * Description: instantiate 32-Bit Transmitter/Receiver into physical layer
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Create_Bind_Link(RIO_PL_DS_T *handle)
{
    unsigned int                          flag = 0; /* flag for loop*/
    RIO_LINK_MODEL_CALLBACK_TRAY_T        link_Ctray;
    RIO_PL_PAR_INIT_BLOCK_CALLBACK_TRAY_T par_Init_Block_Ctray;

    /* check that the instance was created as parallel one */
    if ( !handle->is_Parallel_Model )
        return RIO_ERROR;

    /* 
     * create an instanse, export its function tray and bind it to
     * physical layer
     * if an error happens - memory will be deallocated and 
     * an error code will be returned
     */
    if (RIO_Link_Model_Create_Instance(&handle->link_Instanse) == RIO_ERROR ||
        handle->link_Instanse == NULL)
        return RIO_ERROR;

    if (RIO_PL_Par_Init_Block_Create_Instance(&handle->mid_Block_Instanse) == RIO_ERROR ||
        handle->mid_Block_Instanse == NULL)
        return RIO_ERROR;

    do
    {

        /*get function trays from 32-bit txrx and parallel init block*/
        if (RIO_Link_Model_Get_Function_Tray(
            (RIO_LINK_MODEL_FTRAY_T*)handle->link_Interface) == RIO_ERROR)
            break;

        if (RIO_PL_Par_Init_Block_Get_Function_Tray(
            (RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)handle->mid_Block_Interface) == RIO_ERROR)
            break;

        /* fill internal function tray */
        handle->internal_Ftray.rio_Link_Tx_Disabling = ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Interface))->rio_Link_Tx_Disabling;
        handle->internal_Ftray.rio_Link_Rx_Disabling = ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Interface))->rio_Link_Rx_Disabling;

        handle->internal_Ftray.rio_Init_Block_Machine_Change_State =
            ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Interface))->rio_PL_Par_Init_Block_Machine_Change_State;

        /*fill ctray for 32-bit txrx*/
        link_Ctray.rio_Link_Get_Granule     
            = ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Interface))->rio_PL_Par_Init_Block_Get_Granule;
        link_Ctray.rio_Link_Put_Granule     
            = ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Interface))->rio_PL_Par_Init_Block_Put_Granule;
        link_Ctray.palpdl_Interface_Context = (RIO_CONTEXT_T)handle->mid_Block_Instanse;

        link_Ctray.rio_Ph_Set_Pins        = pl_Set_Pins;
        link_Ctray.lpep_Interface_Context = (RIO_CONTEXT_T)handle;

        link_Ctray.rio_Link_Input_Present = pl_Input_Present;
        link_Ctray.rio_Granule_Received   = pl_Granule_Received;
        link_Ctray.rio_Symbol_To_Send     = pl_Symbol_To_Send;
        link_Ctray.rio_Hooks_Context      = (RIO_CONTEXT_T)handle;

        link_Ctray.rio_User_Msg          = RIO_PL_Msg;
        link_Ctray.error_Warning_Context = (RIO_CONTEXT_T)handle;

        /*fill ctray for parallel init block*/
        par_Init_Block_Ctray.rio_Link_Get_Granule     = RIO_PL_Get_Granule;
        par_Init_Block_Ctray.rio_Link_Put_Granule     = RIO_PL_Put_Granule;
        par_Init_Block_Ctray.rio_PL_Interface_Context = (RIO_CONTEXT_T)handle;

        par_Init_Block_Ctray.rio_Set_State_Flag = RIO_PL_Set_State_Flag;
        par_Init_Block_Ctray.rio_Get_State_Flag = RIO_PL_Get_State_Flag;
        par_Init_Block_Ctray.rio_Regs_Context   = (RIO_CONTEXT_T)handle;

        par_Init_Block_Ctray.rio_User_Msg = RIO_PL_Msg;
        par_Init_Block_Ctray.rio_Msg      = (RIO_CONTEXT_T)handle;


        if (RIO_Link_Model_Bind_Instance(
            handle->link_Instanse, 
            &link_Ctray) == RIO_ERROR)
            break;
            
        if (RIO_PL_Par_Init_Block_Bind_Instance(
            handle->mid_Block_Instanse, 
            &par_Init_Block_Ctray) == RIO_ERROR)
            break;

        return RIO_OK;

    }while (flag);

    /* creating and binding wasn't successful */
    free(handle->link_Instanse);
    free(handle->mid_Block_Instanse);

    return RIO_ERROR;
}

/***************************************************************************
 * Function : pl_Initialize_Instanse
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Initialize_Instance(
    RIO_PL_DS_T *instanse, 
    RIO_PL_PARAM_SET_T *param_Set
    )
{
    /*
     * check handle
     */
    if (instanse == NULL)
        return RIO_ERROR;
    
    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    /*
     * initialize components of the instanse
     */
    if (RIO_PL_Init_Regs(instanse) == RIO_ERROR)
        return RIO_ERROR;

    RIO_PL_Rx_Init(instanse);
    RIO_PL_Tx_Init(instanse);
    RIO_PL_Out_Init(instanse, param_Set);
    RIO_PL_In_Init(instanse, param_Set);

    instanse->initialized = RIO_FALSE;


    return RIO_OK;
}

/***************************************************************************
 * Function : check_Inst_Param_Set
 *
 * Description: check is instantiation physical layer parameters set is valid
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Inst_Param_Set(RIO_MODEL_INST_PARAM_T *param_Set)
{
    if (param_Set == NULL || 
        IS_NOT_A_BOOL(param_Set->mbox1) ||
        IS_NOT_A_BOOL(param_Set->mbox2) ||
        IS_NOT_A_BOOL(param_Set->mbox3) ||
        IS_NOT_A_BOOL(param_Set->mbox4) ||
        IS_NOT_A_BOOL(param_Set->has_Memory) ||
        IS_NOT_A_BOOL(param_Set->has_Processor) ||
        IS_NOT_A_BOOL(param_Set->has_Doorbell) ||
        IS_NOT_A_BOOL(param_Set->is_Bridge))
        return RIO_ERROR;

        /*check if parameter value is correct to reserved value */

/*      temp_1=(param_Set->max_Source_Op_Sizes>> 20);
        if (((temp_1 & 0xf)  == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 &0xf) == 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;

        temp_1=(param_Set->max_Dest_Op_Sizes>> 20);
        if (((temp_1 & 0xf)  == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 &0xf) == 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;
*/

/*    temp_1=(param_Set->max_Source_MP_Op_Sizes >> 16);
        if (((temp_1 & 0xf)  == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 &0xf) == 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;


        temp_1=(param_Set->max_Dest_MP_Op_Sizes>> 16);
        if (((temp_1 & 0xf)  == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 &0xf) == 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;
        temp_1 = temp_1 >> 4;
        if (((temp_1 & 0xf) == 0x5) || ((temp_1 & 0xf) == 0x7) || ((temp_1 & 0xf)== 0xf) ) return RIO_ERROR;

*/
        /*check extended features ptr for lieing in the extended features space with its block */

        if ((param_Set->entry_Extended_Features_Ptr < RIO_EF_SPACE_BOTTOM) ||
                (param_Set->entry_Extended_Features_Ptr > 
                ((RIO_EF_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD) - 
                (RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP))) ||
                (param_Set->entry_Extended_Features_Ptr % RIO_PL_BYTE_CNT_IN_DWORD))
                return RIO_ERROR;

        /*check next extended features ptr for lieing in the extended features space,
        but not in the first extended features block*/
        if (((param_Set->next_Extended_Features_Ptr != 0) && (param_Set->next_Extended_Features_Ptr < RIO_EF_SPACE_BOTTOM)) ||
                (param_Set->next_Extended_Features_Ptr > 
                ((RIO_EF_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD) - 
                (RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP))) ||
                (param_Set->next_Extended_Features_Ptr % RIO_PL_BYTE_CNT_IN_DWORD) ||
                ((param_Set->next_Extended_Features_Ptr >= param_Set->entry_Extended_Features_Ptr) && 
                (param_Set->next_Extended_Features_Ptr < (param_Set->entry_Extended_Features_Ptr + 
                RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP))))
                return RIO_ERROR;

    /* check sizes of inbound and outbound buffers*/
    if (!param_Set->pl_Outbound_Buffer_Size || param_Set->pl_Outbound_Buffer_Size > RIO_PL_MAX_BUFFER_SIZE ||
        !param_Set->pl_Inbound_Buffer_Size || param_Set->pl_Inbound_Buffer_Size > RIO_PL_MAX_BUFFER_SIZE)
        return RIO_ERROR;

    /*if (param_Set->ext_Address_Support > 7)*/
    if (param_Set->ext_Address_Support != 0x01 &&
        param_Set->ext_Address_Support != 0x03 &&
        param_Set->ext_Address_Support != 0x05 &&
        param_Set->ext_Address_Support != 0x07)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Init_Param_Set
 *
 * Description: check is initialization physical layer 
 *              parameters set is valid
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Init_Param_Set(
    RIO_PL_DS_T *instanse, 
    RIO_PL_PARALLEL_PARAM_SET_T *param_Set
    )
{
    if (param_Set == NULL || instanse == NULL)
        return RIO_ERROR;
        
    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->pass_By_Prio))
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_4, (unsigned long)param_Set->pass_By_Prio);
        return RIO_ERROR;
    }
    if (IS_NOT_A_BOOL(param_Set->ext_Address) ||
        IS_NOT_A_BOOL(param_Set->ext_Address_16)/* ||
        IS_NOT_A_BOOL(param_Set->xamsbs)*/)
        return RIO_ERROR;

    if (param_Set->transp_Type != RIO_PL_TRANSP_32 &&
        param_Set->transp_Type != RIO_PL_TRANSP_16)
        return RIO_ERROR;

/*    if (param_Set->ext_Address == RIO_FALSE &&
        param_Set->ext_Address_16 == RIO_TRUE)
        return RIO_ERROR;
*/
        if (IS_NOT_A_BOOL(param_Set->lp_Ep_Is_8))
                return RIO_ERROR;

        if (IS_NOT_A_BOOL(param_Set->work_Mode_Is_8))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->input_Is_Enable))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->is_Discovered))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->is_Host))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->is_Master_Enable))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->output_Is_Enable))
                return RIO_ERROR;

    if (param_Set->lp_Ep_Is_8 == RIO_TRUE && param_Set->work_Mode_Is_8 == RIO_FALSE)
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->requires_Window_Alignment))
                return RIO_ERROR;

    if (param_Set->num_Trainings < 1)
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->enable_LRQ_Resending))
        return RIO_ERROR;

     /*   if (param_Set->custom_Training_Pattern == NULL)
                return RIO_ERROR;*/

    /* 
     * check correspondense between supported address  - the constant in case 
     * labels mean specification defined encoding for supported address range.
     * they are used anywhere else. Checking supported addresses with initialization parameters 
     * is checking. Change constant here in case of specification changing
     */
    switch (instanse->inst_Param_Set.ext_Address_Support)
    {
        /* 34 bit address only */
        case 1:
            if (/*param_Set->xamsbs == RIO_FALSE ||*/
                param_Set->ext_Address == RIO_TRUE)
                return RIO_ERROR;
            break;

        /* 50 and 34 */
        case 3:
            if (/*param_Set->xamsbs == RIO_FALSE ||*/
                (param_Set->ext_Address == RIO_TRUE && param_Set->ext_Address_16 == RIO_FALSE))
                return RIO_ERROR;
            break;

        /* 66 and 34 */
        case 5:
            if (/*param_Set->xamsbs == RIO_FALSE ||*/
                (param_Set->ext_Address == RIO_TRUE && param_Set->ext_Address_16 == RIO_TRUE))
                return RIO_ERROR;
            break;

        /* 66, 50 and 34 */
        case 7:
/*            if (param_Set->xamsbs == RIO_FALSE)
                return RIO_ERROR;
*/
            break;

        default:
            return RIO_ERROR;
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : count_Time_Outs
 *
 * Description: count time after sendimg for instanses parts
 *              
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void count_Time_Outs(RIO_PL_DS_T *handle)
{
    if (handle == NULL)
        return;

    /* check that the instance was created as parallel one */
    if ( !handle->is_Parallel_Model )
        return;

    /*GDA: ack delay time-out proceding */	
      ack_Delay_Time_Out(handle);

    /*GDA: multicast delay time-out proceding */	
      multicast_Delay_Time_Out(handle);  /*GDA: Description=> Supporting Random Delay feature */

    /*GDA: Rnd PNACC Generator*/
      rio_Enable_RND_Pnacc_Generator(handle);

    /*GDA: Rnd RETRY Generator*/
      rio_Enable_RND_Retry_Generator(handle);

    /* link request time-out proceding */
    RIO_PL_Tx_Link_Req_Time_Out(handle);

    /*sending packet time-out proceding */
    RIO_PL_Tx_Phys_Time_Out(handle);

    /* logical layer time-out*/
    /*  RIO_PL_Out_Log_Time_Out(handle);*/

    /* logical layer time-out*/
	if (handle->is_PLLight_Model == RIO_FALSE)
		RIO_PL_Out_Log_Time_Out_Errata_14_0503(handle);
}

/***************************************************************************
 * Function : pl_Lost_Sync_Command
 *
 * Description: instantiate 32-Bit Transmitter/Receiver into physical layer
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Lost_Sync_Command(RIO_HANDLE_T handle)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse->is_Parallel_Model == RIO_FALSE)
        return RIO_ERROR;

    return (instanse->internal_Ftray.rio_Init_Block_Machine_Change_State
        (instanse->mid_Block_Instanse, RIO_PAR_INIT_BLOCK_LOST_SYNC));
}


/***************************************************************************
 * Function : pl_Force_EOP_Insertion
 *
 * Description: Forces insertion of eop at the end of each packet
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Force_EOP_Insertion(
    RIO_HANDLE_T handle,  
    RIO_BOOL_T enable_Force)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse->is_Parallel_Model != RIO_TRUE)
        return RIO_ERROR;
    /*check parameters*/
    if (IS_NOT_A_BOOL(enable_Force))
        return RIO_PARAM_INVALID;

    /*enable idle generator*/
    instanse->tx_Data.force_EOP = enable_Force;
    

    return RIO_OK;
}

/****************************************************************************/
