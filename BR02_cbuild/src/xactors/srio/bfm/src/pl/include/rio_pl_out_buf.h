#ifndef RIO_PL_OUT_BUF_H
#define RIO_PL_OUT_BUF_H
/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_out_buf.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer outbound buffer header
*               
* Notes:
*
******************************************************************************/

#ifndef RIO_PL_DS_H
#include "rio_pl_ds.h"
#endif

/* try to load current request to outbound buffer */
int RIO_PL_Out_Load_Entry(RIO_PL_DS_T *handle);

/* start new [acket transmission and get the first granule */
int RIO_PL_Out_Start_New_Packet(RIO_PL_DS_T  *handle, RIO_GRANULE_T *granule, unsigned int *packet_Handle, unsigned int ack_Id);

/* obtain new data for transmitting packet */
int RIO_PL_Out_Get_Data(RIO_PL_DS_T  *handle, RIO_GRANULE_T *granule, RIO_BOOL_T *is_Last);

/* notify the buffer about physical layer acknowledge */
void RIO_PL_Out_Packet_Ack(RIO_PL_DS_T  *handle, unsigned int packet_Handle, RIO_BOOL_T is_Positive);

/* notify the buffer about physical layer time-out*/
void RIO_PL_Out_Packet_Time_Out(RIO_PL_DS_T *handle, unsigned int packet_Handle, unsigned long time);

/* notify the buffer about discarding packet at receiver side */
void RIO_PL_Out_Packet_Discarded(RIO_PL_DS_T *handle, unsigned int packet_Handle, RIO_BOOL_T is_PNA);

/* initialize outbound buffer */
void RIO_PL_Out_Init(
    RIO_PL_DS_T        *handle,
    RIO_PL_PARAM_SET_T *param_Set
    );

/* return size of outbound buffer */
unsigned int RIO_PL_Out_Get_Buf_Size(RIO_PL_DS_T *handle);

/* returns count of available cells in the outbound buffer for prio */
unsigned int RIO_PL_Out_Get_Buf_Free(RIO_PL_DS_T *handle, unsigned int value);

/* convert double world to the stream */
void RIO_PL_Out_Conv_Dw_Char(RIO_DW_T *dw, RIO_UCHAR_T *stream);

/* GDA: Bug#124 - Support for Data Streaming packet */
/* convert half world to the stream */
void RIO_PL_Out_Conv_Hw_Char(RIO_HW_T *hw, RIO_UCHAR_T *stream);
/* GDA: Bug#124 - Support for Data Streaming packet */

/* count CRC */
void RIO_PL_Out_Compute_CRC(unsigned short *crc, RIO_UCHAR_T stream[], unsigned int i_Max);

/* detect if ttype is a request ttype */
RIO_BOOL_T RIO_PL_Is_Req(unsigned int ftype, unsigned int ttype);

/* count logical layer time-out*/
void RIO_PL_Out_Log_Time_Out(RIO_PL_DS_T *handle);

/* count logical layer time-out according Errata*/
void RIO_PL_Out_Log_Time_Out_Errata_14_0503(RIO_PL_DS_T *handle);


/* notify the buffer about arriving response */
void RIO_PL_Out_Rec_Resp(
    RIO_PL_DS_T    *handle, 
    RIO_RESPONSE_T *ext_Resp);

/* detect new TID for a packet */
unsigned char RIO_PL_Out_Assert_TID(RIO_PL_DS_T *handle);

/* initialize the arbiter */
void RIO_PL_Out_Init_Arbiter(RIO_PL_OUT_ARB_T *element);

/* detect size and wdptr for packet */
int RIO_PL_Detect_IO_Size_Wdprt(
    unsigned int *size, 
    unsigned int *wdptr, 
    unsigned int dw_Size, 
    unsigned int byte_Num,
    unsigned int offset);


/****************************************************************************/
#endif /* RIO_PL_OUT_BUF_H */

