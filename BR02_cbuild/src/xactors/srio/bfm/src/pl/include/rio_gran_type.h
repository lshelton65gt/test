#ifndef RIO_GRAN_TYPE_H
#define RIO_GRAN_TYPE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_gran_type.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
* 
* Description:  This file contains granule data types,
*                
* Notes:      
* 
******************************************************************************/
#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

typedef enum {
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_NOTE_MSG
} RIO_PL_MSG_TYPE_T;

/*
 * this data type represent one part of specefied RIO control
 * symbol
 */
typedef struct {
    unsigned int s;
    unsigned int ack_ID;
    unsigned int secded; /* for packet here is prio value */
    unsigned int buf_Status;
    unsigned int stype;
} RIO_PL_16_DATA_T;

/*
 * The structure represented data granule consisting from early defined 
 * control data types and type of data granule
 */
#define RIO_PL_CNT_BYTE_IN_GRAN       4 /* count of bytes in granule */

typedef struct {
    RIO_UCHAR_T        granule_Date[RIO_PL_CNT_BYTE_IN_GRAN];
    RIO_PL_16_DATA_T   granule_Struct;
    RIO_GRANULE_TYPE_T granule_Type;
    RIO_BOOL_T         granule_Frame;
} RIO_GRANULE_T;

/* cmd field in LINK_REQUEST */
typedef enum {
    RIO_PL_LREQ_RESET = 3,
    RIO_LP_LREQ_INPUT_STATUS = 4,
    RIO_PL_LREQ_SEND_TRAINING = 0
/*    RIO_PL_LREQ_OVERRIDE_WIDTH = 2  not implemented*/ 

} RIO_PL_LINK_REQUEST_CMD_T;

/* link status in LINK RESPONSE */
typedef enum {
    RIO_PL_LRESP_UNINIT = 0,
    RIO_PL_LRESP_INITIALIZING = 1,
    RIO_PL_LRESP_ERROR = 2,
    RIO_PL_LRESP_STOPPED = 3,
    RIO_PL_LRESP_RETRY_STOPPED = 4,
    RIO_PL_LRESP_ERROR_STOPPED = 5,
    RIO_PL_LRESP_GENERAL_ERROR = 7,
    RIO_PL_LRESP_OK = 8,
    RIO_PL_LRESP_SERIAL_OK = 16
} RIO_PL_LRESP_LINK_STATUS_T;

/* stype values for granules */
typedef enum {
    RIO_GR_PACKET_ACCEPTED_STYPE = 0 ,
    RIO_GR_PACKET_RETRY_STYPE = 1,
    RIO_GR_PACKET_NOT_ACCEPTED_STYPE = 2 ,
    RIO_GR_PACKET_CONTROL_STYPE = 4,
    RIO_GR_LINK_REQUEST_STYPE = 5,
    RIO_GR_LINK_RESPONSE_STYPE = 6,
    RIO_GR_STATUS_STYPE = 8,
    RIO_GR_LINK_TRAINING_STYPE = 15
} RIO_PL_GRANULE_STYPE_T;

/* packet control sub type field encoding */
typedef enum {
    RIO_GR_IDLE_SUB_TYPE = 0,
    RIO_GR_THROTTLE_SUB_TYPE = 4, 
    RIO_GR_RESTART_FROM_RETRY_SUB_TYPE = 3,
    RIO_GR_TOD_SYNC_SUB_TYPE = 5, 
    RIO_GR_EOP_SUB_TYPE = 2,
    RIO_GR_STOMP_SUB_TYPE = 1
} RIO_PL_PACKET_CONTROL_ENC_T;

/* packet not accepted cause field*/ 
typedef enum {
    RIO_PL_PNACC_INTERNAL_ERROR = 8,
    RIO_PL_PNACC_UNEXPECTED_ACKID = 9,
    RIO_PL_PNACC_BAD_CONTROL = 10,
    RIO_PL_PNACC_INPUT_STOPPED = 11,
    RIO_PL_PNACC_BAD_CRC = 12,
    RIO_PL_PNACC_GENERAL_ERROR = 15,
    RIO_PL_PNACC_OK = 16,
    RIO_PL_PNACC_BAD_S_BIT_PARITY = 13 /* RIO_PL_PNACC_INVALID_CHARACTER for the serial spec */
} RIO_PL_PACKET_NOT_ACCEPTED_CAUSE_T;

#define RIO_PL_PNACC_INVALID_CHARACTER  RIO_PL_PNACC_BAD_S_BIT_PARITY

/*
 * this type represents receiver training machine state
 */
typedef enum {
    RIO_PAR_INIT_BLOCK_LOST_SYNC = 0, /*the block must send lrq/st followed by trn. pttn*/
    RIO_PAR_INIT_BLOCK_LOST_RECAL     /*the block must send just lrq/st*/
} RIO_PAR_INIT_BLOCK_COMMANDS_T;



#endif /* RIO_GRAN_TYPE_H*/


