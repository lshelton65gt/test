#ifndef RIO_PL_ERRORS_H
#define RIO_PL_ERRORS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
* $Element: Y:\riosuite\src\riom\pl\include\rio_pl_errors.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer errors/warnings handlers prototypes
*               and data 
* Notes:
*
******************************************************************************/

#ifndef RIO_PL_DS_H
#include "rio_pl_ds.h"
#endif

/*
 * this type syblically represent error codes which are used as
 * indexes in error messages array
 */
typedef enum {
    RIO_PL_ERROR_1 = 0,    /* 32 bit txrx errors */
    RIO_PL_ERROR_2, 
    RIO_PL_ERROR_3, 
    RIO_PL_ERROR_4, 
    RIO_PL_ERROR_5, 
    RIO_PL_ERROR_6, 
    RIO_PL_ERROR_7, 
    RIO_PL_ERROR_8,
    RIO_PL_ERROR_9,
    RIO_PL_ERROR_10,
    RIO_PL_ERROR_11,
    RIO_PL_ERROR_12,
    RIO_PL_ERROR_13,
    RIO_PL_ERROR_14,
    RIO_PL_ERROR_15,
    RIO_PL_ERROR_16,
    RIO_PL_ERROR_17,
    RIO_PL_ERROR_18,
    RIO_PL_ERROR_19,
    RIO_PL_ERROR_20,
    RIO_PL_ERROR_21,
    RIO_PL_ERROR_22,
    RIO_PL_ERROR_23,
    RIO_PL_ERROR_24,
    RIO_PL_ERROR_25,
    RIO_PL_ERROR_26,
    RIO_PL_ERROR_27,
    RIO_PL_ERROR_28,
    RIO_PL_ERROR_29,
    RIO_PL_ERROR_30,
    RIO_PL_ERROR_31,
    RIO_PL_ERROR_32,
    RIO_PL_ERROR_33,
    RIO_PL_ERROR_34,
    RIO_PL_ERROR_35,
    RIO_PL_ERROR_36,
    RIO_PL_ERROR_37,
    RIO_PL_ERROR_38,
    RIO_PL_ERROR_39,
    RIO_PL_ERROR_40,
    RIO_PL_ERROR_41,
    RIO_PL_ERROR_42,
    RIO_PL_ERROR_43,
    RIO_PL_ERROR_44,
    RIO_PL_ERROR_45,
    RIO_PL_ERROR_46,
    RIO_PL_ERROR_47,
    RIO_PL_ERROR_48,
    RIO_PL_ERROR_49,
    RIO_PL_ERROR_50,
    RIO_PL_ERROR_51,
    RIO_PL_ERROR_52,
    RIO_PL_ERROR_53,
    RIO_PL_ERROR_54,
    RIO_PL_ERROR_55,
    RIO_PL_ERROR_56,
    RIO_PL_ERROR_57,
    RIO_PL_ERROR_58,
    RIO_PL_ERROR_59,
    RIO_PL_ERROR_60,
    RIO_PL_ERROR_61,
    RIO_PL_ERROR_62,
    RIO_PL_ERROR_63,

    RIO_PL_WARNING_1,  /* 32 bit tx/rx warnings */
    RIO_PL_WARNING_2,
    RIO_PL_WARNING_3,
    RIO_PL_WARNING_4,
    RIO_PL_WARNING_5,
    RIO_PL_WARNING_6,
    RIO_PL_WARNING_7,
    RIO_PL_WARNING_8,
    RIO_PL_WARNING_9,
    RIO_PL_WARNING_10,
    RIO_PL_WARNING_11,
    
    RIO_PL_WARNING_12, /*added entry*/

    RIO_PL_ERROR_64,

    RIO_PL_NOTE_1,     /* 32 bit txrx notes */

    RIO_PL_ERROR_65, /*unexpected ackID received in output error stopped state*/
    RIO_PL_ERROR_66, /*unsolicited acknowledge*/
    RIO_PL_ERROR_67, /*received more than 7(31) unacknowleged packet*/
    RIO_PL_WARNING_13, /*incorrect link responce is accepted*/
    RIO_PL_WARNING_14, /*timeout for link responce is accepted - LRQ will be resend*/
    RIO_PL_WARNING_15  /*received PA for stomped packet*/
} RIO_PL_MESSAGES_T;

/*
 * this function write an warning message with corresponding explaning
 * based on instanse's state and a set of additional parameters
 */
int RIO_PL_Print_Message(RIO_PL_DS_T* handle, RIO_PL_MESSAGES_T warning_Type, ...);


/****************************************************************************/
#endif /* RIO_PL_ERRORS_H */

