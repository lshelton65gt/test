#ifndef RIO_PL_RX_H
#define RIO_PL_RX_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_rx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Functions:
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  physical layer receiver header
*               
* Notes:
*
******************************************************************************/

#ifndef RIO_PL_DS_H
#include "rio_pl_ds.h"
#endif

/* main receiver loop */
void RIO_PL_Granule_Received(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/*initialize receiver */
void RIO_PL_Rx_Init(RIO_PL_DS_T *handle);

/* detect if a THROTTLE shall be sent */
int RIO_PL_Throttle_Shall_Be_Sent
    (RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/*GDA: generates PNACC control on the fly*/
void rio_Enable_RND_Pnacc_Generator(RIO_PL_DS_T *handle);

/*GDA: generates Retry control on the fly*/
void rio_Enable_RND_Retry_Generator(RIO_PL_DS_T *handle);

/****************************************************************************/
#endif /* RIO_PL_RX_H */

