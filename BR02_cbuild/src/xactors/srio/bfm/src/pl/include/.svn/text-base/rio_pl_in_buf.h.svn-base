#ifndef RIO_PL_IN_BUF_H
#define RIO_PL_IN_BUF_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_in_buf.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer inbound buffer header
*               
* Notes:  
*
******************************************************************************/

#ifndef RIO_PL_DS_H
#include "rio_pl_ds.h"
#endif

/* next portion of packet data */
int RIO_PL_In_Load_Data(
    RIO_PL_DS_T *handle, 
    RIO_GRANULE_T *granule
    );

/* check current packet for error detection */
int RIO_PL_In_Check_Current(RIO_PL_DS_T *handle);

/* commit accumulating packet */
void RIO_PL_In_Commit_Current(RIO_PL_DS_T *handle);

/* try to allocate new entry for packet */
int RIO_PL_In_Alloc_Entry(RIO_PL_DS_T   *handle, unsigned int  prio);

/* return count of free entries in inbound buffer*/
int RIO_PL_In_Buf_Status(RIO_PL_DS_T *handle);

/* initialize the inbound buffer */
void RIO_PL_In_Init(
    RIO_PL_DS_T        *handle,
    RIO_PL_PARAM_SET_T *param_Set
    );

/* return the size of inbound buffer */
unsigned int RIO_PL_In_Get_Buf_Size(RIO_PL_DS_T *handle);

/* return the number of available free entries in the inbound buffer */
unsigned int RIO_PL_In_Get_Buf_Free(RIO_PL_DS_T *handle, unsigned int value);

/* form response structure */
int RIO_PL_In_Form_Resp(
    RIO_PL_DS_T    *handle, 
    RIO_RESPONSE_T *ext_Resp);

/* load buffer entry from the stream */
void RIO_PL_In_Load_From_Stream(RIO_PL_DS_T *handle, unsigned int i);

/* convert byte stream to double words array */
void RIO_In_Stream_To_Dw(RIO_DW_T *dws, RIO_UCHAR_T *stream, unsigned int size);

/* notify about accepting external request */
void RIO_PL_In_Ack_Ext_Req(RIO_PL_DS_T *handle, RIO_TAG_T    tag);

/* detect if the packet has a payload */
int RIO_PL_Is_Payload(unsigned int ftype, unsigned int ttype);

/* notify the environment about external request */
void RIO_PL_In_Not_About_Request(RIO_PL_DS_T * handle);


/****************************************************************************/
#endif /* RIO_PL_IN_BUF_H */


