#ifndef RIO_PL_PLLIGHT_SER_MODEL_H
#define RIO_PL_PLLIGHT_SER_MODEL_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_pllight_ser_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains declaration of the common RapidIO PL model
*               function interface
*
* Notes:        
*
******************************************************************************/

#include "rio_pl_pllight_serial_model.h"

int RIO_PL_PLLight_Serial_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_MODEL_INST_PARAM_T  *param_Set
    );

int RIO_PL_PLLight_Serial_Model_Get_Function_Tray(
    RIO_PL_SERIAL_MODEL_FTRAY_T *ftray             
    );

#endif /* RIO_PL_PLLIGHT_SER_MODEL_H */


