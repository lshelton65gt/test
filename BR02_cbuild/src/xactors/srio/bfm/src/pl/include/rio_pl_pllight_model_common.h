#ifndef RIO_PL_PLLIGHT_MODEL_COMMON_H
#define RIO_PL_PLLIGHT_MODEL_COMMON_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_pllight_model_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains declaration of the common RapidIO PL model
*               function interface
*
* Notes:        
*
******************************************************************************/

#include "rio_types.h"
#include "rio_gran_type.h"



int RIO_PL_Print_Version(
    RIO_HANDLE_T    handle
    );

int RIO_PL_Detect_IO_Size_Wdprt(
    unsigned int *size, 
    unsigned int *wdptr, 
    unsigned int dw_Size, 
    unsigned int byte_Num,
    unsigned int offset
    );

int RIO_PL_Get_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset, 
    unsigned long     *data
    );

int RIO_PL_Set_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset, 
    unsigned long     data
    );

int RIO_PL_Set_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            indicator_Value,
    unsigned int             indicator_Param
    );

int RIO_PL_Get_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param
    );

int RIO_PL_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    );

int RIO_PL_Get_Granule(
    RIO_CONTEXT_T context,   /* callback context */
    RIO_HANDLE_T  handle,    /* instance's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    );

int RIO_PL_Msg(
    RIO_CONTEXT_T     context, 
    RIO_PL_MSG_TYPE_T msg_Type,
    char              *string
    );


int RIO_PL_Set_Retry_Generator(
    RIO_HANDLE_T handle,  
    unsigned int    retry_Prob_Prio0,
    unsigned int    retry_Prob_Prio1,
    unsigned int    retry_Prob_Prio2,
    unsigned int    retry_Prob_Prio3
    );

int RIO_PL_Set_PNACC_Generator(
    RIO_HANDLE_T handle,  
    unsigned int pnacc_Factor
    );

int RIO_PL_Set_Stomp_Generator(
    RIO_HANDLE_T handle,  
    unsigned int stomp_Factor
    );

int RIO_PL_Set_LRQR_Generator(
    RIO_HANDLE_T handle,  
    unsigned int lrqr_Factor
    );

int RIO_PL_Model_Set_LRQR_Generator(
    RIO_HANDLE_T     handle,
    unsigned int     lrqr_Factor
    );

int pl_Enable_Rnd_Idle_Generator(
    RIO_HANDLE_T handle,  
    unsigned int low_Boundary,
    unsigned int high_Boundary,
    RIO_BOOL_T enable
);


/*GDA: Rnd Generator (PNACC)*/
int RIO_PL_Enable_RND_Pnacc_Generator_Init(
    RIO_HANDLE_T handle,
    RIO_WORD_T pnacc_Low_Boundary,
    RIO_WORD_T pnacc_High_Boundary,
    RIO_BYTE_T pnacc_Probability
    );


/*GDA: Rnd Generator (RETRY)*/
int RIO_PL_Enable_RND_Retry_Generator_Init(
    RIO_HANDLE_T handle,
    RIO_WORD_T retry_Low_Boundary,
    RIO_WORD_T retry_High_Boundary,
    RIO_BYTE_T retry_Probability_Prio0,
    RIO_BYTE_T retry_Probability_Prio1,
    RIO_BYTE_T retry_Probability_Prio2,
    RIO_BYTE_T retry_Probability_Prio3
    );

/*GDA: Enable Multicast Bug#116 Implementation*/
int RIO_PL_Generate_Multicast(
			     RIO_HANDLE_T handle,
			     RIO_WORD_T   low_Boundary,
                             RIO_WORD_T   high_Boundary
			   );

/***************************************************************************
 * Function : IS_NOT_A_BOOL
 *
 * Description: detect if a variable is RIO_BOOL_T type
 *
 * Returns: bool
 *
 * Notes: none
 *
 **************************************************************************/
#define IS_NOT_A_BOOL(bool_Value) ((bool_Value) != RIO_TRUE && (bool_Value) != RIO_FALSE)

/***************************************************************************
 * Function : RIO_PL_GET_TEMP
 *
 * Description: detect outbound temporary
 *
 * Returns: variable
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_PL_GET_TEMP(handle) (handle)->outbound_Buf.temp

#endif /* RIO_PL_PLLIGHT_MODEL_COMMON_H */


