#ifndef RIO_PL_INSIDE_INTERFACE_H
#define RIO_PL_INSIDE_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_inside_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the logical layer
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

typedef enum
{
    REINIT_ADAPTER
}RIO_COMMAND_PL_WRAP_T;

/* 
 * the function is used to enabling/disabling the link Transmitter.
 * The function invokation set tx mode - 
 * if transmitter drives the pins or is switched off.
 */
typedef int (*RIO_PL_II_LINK_TX_DISABLING)(
    RIO_HANDLE_T           handle,     /* handle of instance*/
    RIO_BOOL_T               tx_Disable /* parameters */
    );
/* 
 * the function is used to enabling/disabling the link Receiver.
 * The function invokation set rx mode - 
 * if receiver drives the pins or is switched off.
 */
typedef int (*RIO_PL_II_LINK_RX_DISABLING)(
    RIO_HANDLE_T           handle,     /* handle of instance*/
    RIO_BOOL_T               rx_Disable /* parameters */
    );


/* 
 * the function is used to initialize and re-initialize the RIO model. It set 
 * PL model instance parameters to requested values and turn the PL model
 * instance to work mode
 */
typedef int (*RIO_PL_II_MODEL_INITIALIZE)(
    RIO_HANDLE_T         handle,
    void                 *param_Set
    );
/*
 * to switch on/off init machine. 
 */
typedef int (*RIO_PL_II_INIT_MACHINE_CHANGE_STATE)(
    RIO_HANDLE_T                handle,
    unsigned int                command /*the new state of machine*/
    );

/*
    function to pass commands to the wrappers 
    this command is implemented in serial 
    and parallel wrapper.
*/
typedef void (*RIO_PL_MODEL_COMMAND_TO_WRAP)(
    RIO_HANDLE_T                handle,
    RIO_COMMAND_PL_WRAP_T       cmd);


#endif /* RIO_PL_INSIDE_INTERFACE_H */

