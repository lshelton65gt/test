#ifndef RIO_PL_DS_H
#define RIO_PL_DS_H

#include "rio_common.h"		/* GDA  - GDA_PKT_SIZE is defined */

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                       to display revision history information.
*
* Description:  physical layer data structure declaration file
*
* Notes:
*
******************************************************************************/

/*#ifndef RIO_32_TXRX_MODEL_H
#include "rio_32_txrx_model.h"
#endif
*/
/*#ifndef RIO_PL_MODEL_H
#include "rio_pl_model.h"
#endif
*/
/*
#ifndef RIO_PL_PAR_INIT_BLOCK_H
#include "rio_parallel_init.h"
#endif
*/

#ifndef RIO_PL_INSIDE_INTERFACE_H
#include "rio_pl_inside_interface.h"
#endif

#ifndef RIO_GRAN_TYPE_H
#include "rio_gran_type.h"
#endif

#ifndef RIO_PL_INTERFACE_H
#include "rio_pl_interface.h"
#endif

#ifndef RIO_PLLIGHT_INTERFACE_H
#include "rio_pllight_interface.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif

/*
 * this register describe type of allowed access to register
 */
typedef enum {
    RIO_REG_READ_ONLY = 0,
    RIO_REG_WRITE_ONLY,
    RIO_REG_READ_WRITE,
    RIO_REG_READ_RESET,
    RIO_REG_INTERNAL_ONLY
} RIO_REG_TYPE_T;

/*
 * this structure represents a register as an entity
 */
typedef struct{
    unsigned long     reg_Value;
    unsigned long     reg_Mask;
    unsigned long     reg_Offset;
    RIO_REG_TYPE_T    access_Type;
} RIO_PL_REG_T;

/*
 * this is a set of common register 
 */
typedef struct {
    RIO_PL_REG_T device_Ids;
    RIO_PL_REG_T device_Info;

    RIO_PL_REG_T assembly_Ids;
    RIO_PL_REG_T assembly_Info;

    RIO_PL_REG_T pe_Features;
    RIO_PL_REG_T switch_Info;

    RIO_PL_REG_T source_Ops;
    RIO_PL_REG_T dest_Ops;

/*    RIO_PL_REG_T max_Source_Size;
    RIO_PL_REG_T max_Dest_Size;

    RIO_PL_REG_T max_Source_Mes_Size;
    RIO_PL_REG_T max_Dest_Mes_Size;
*/
    RIO_PL_REG_T mailbox_Status;
    RIO_PL_REG_T write_Port_Status;
/*
    RIO_PL_REG_T conf_Source_Size;
    RIO_PL_REG_T conf_Dest_Size;

    RIO_PL_REG_T conf_Source_Mes_Size;
    RIO_PL_REG_T conf_Dest_Mes_Size;
*/
    RIO_PL_REG_T pe_Ll_Control;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_REG_T pe_Ll_DS_Control;  
    RIO_PL_REG_T pe_Ll_DS_Info;  
    /* GDA: Bug#124 - Support for Data Streaming packet */
    

    RIO_PL_REG_T lcshbar;
    RIO_PL_REG_T lcsbar;

        RIO_PL_REG_T base_Dev_Id;
        RIO_PL_REG_T host_Base_Dev_Id_Lock;
        RIO_PL_REG_T component_Tag;

    RIO_PL_REG_T port_Maint_Header_0;
    RIO_PL_REG_T port_Maint_Header_1;
    RIO_PL_REG_T extend_Feat_Header_0;
    RIO_PL_REG_T extend_Feat_Header_1;

        RIO_PL_REG_T valid_Time_Out_1; /*link req timeout*/
        RIO_PL_REG_T valid_Time_Out_2; /*packet phy timeout*/
        RIO_PL_REG_T port_General_Control; /*new*/
    

} RIO_PL_COMMON_REGS_T;

/*
 * this is a set of LP-EP specific registers
 */
typedef struct {
    RIO_PL_REG_T link_Valid_Req;
    RIO_PL_REG_T link_Valid_Resp;
    RIO_PL_REG_T link_AckID_Status; /*new*/ 


    RIO_PL_REG_T port_Error_Status;
    RIO_PL_REG_T port_Control; /*new*/ 
    RIO_PL_REG_T port_Buf_Status; /*for buf_Status passing*/

} RIO_PL_LPEP_REGS_T;

/*
 * this type defines the state indicator structure 
 */

typedef struct {
    unsigned int         output_Error_Type;
    unsigned int         input_Error_Type;
    unsigned int         lane_With_Invalid_Character;  /* number of the lane where invalid character occured */
    RIO_BOOL_T           is_Port_Mode_4x; /* RIO_TRUE if port is working in 4x mode */
    unsigned int         byte_In_Rcv_Granule_Cur_Num;
    unsigned int         byte_In_Trx_Granule_Cur_Num;
    unsigned int         are_All_Idles_In_Granule;
    RIO_BOOL_T           is_Packet_Delimiter;
    RIO_BOOL_T           is_Align_Lost;
    RIO_BOOL_T           is_Sync_Lost;
    RIO_BOOL_T           is_Character_Invalid;
    RIO_BOOL_T           copy_To_Lane2;
    RIO_BOOL_T           suppress_Hook;
    RIO_UCHAR_T          initial_Run_Disp;
    unsigned int         decode_Any_Run_Disp;
    RIO_BOOL_T           lane2_Just_Forced_By_Discovery;
    RIO_BOOL_T           rx_Packet_Is_Ongoing;
} RIO_PL_STATE_INDICATORS_PARAMS_T;



/*
 * this type defines all PE registers
 */

typedef struct {
    unsigned int         lpep_Count;
        unsigned int             lpep_Size;
        RIO_PL_COMMON_REGS_T common_Regs;
    RIO_PL_LPEP_REGS_T   *lpep_Regs;
} RIO_PL_REGS_T;


/*
 * this types represent different types for internal packet representation
 */

#define RIO_PL_MAX_PAYLOAD_SIZE	GDA_PKT_SIZE  /* GDA */

/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_PL_MAX_DS_PAYLOAD_SIZE  DS_PKT_SIZE  
/* GDA: Bug#124 - Support for Data Streaming packet */


/*
 * this type represents general request/response for outbound buffer
 */
typedef struct {
    unsigned int  prio;
    unsigned int  tt;
    unsigned int  ftype;
    unsigned long tranp_Info;
    unsigned int  ttype;
    unsigned int  size;
    unsigned int  src_TID;
    unsigned int  hop_Count;
    unsigned int  config_Off;
    unsigned int  wdptr;
    unsigned int  status;
    unsigned int  doorbell_Info;
    unsigned int  msglen;
    unsigned int  letter;
    unsigned int  mbox;
    unsigned int  msgseg;
    unsigned long  address;            /* double-word aligned address */
    unsigned long  extended_Address;
    unsigned int   xamsbs;
    RIO_BOOL_T     ext_Address_Valid; /* extended address fields flags for the sophisticated devices */
    RIO_BOOL_T     ext_Address_16;
    RIO_TR_INFO_T  sec_ID;
    unsigned int sec_TID;

    RIO_BOOL_T   additional_Fields_Valid;
    /*additional fields that are used for the packet struct request*/
    RIO_UCHAR_T     s; /* for parallel model, pl light model only */
    RIO_UCHAR_T     s_inv; /* for parallel model, pl light model only */
    RIO_UCHAR_T     rsrv_Phy_1;
    RIO_UCHAR_T     rsrv_Phy_2;
    RIO_UCHAR_T     rsrv_Ftype6;
    unsigned long   rsrv_Ftype8;
    RIO_UCHAR_T     rsrv_Ftype10;
    /*RIO_UCHAR_T     transaction;*/
    unsigned long   intermediate_Crc;
    RIO_BOOL_T      is_Int_Crc_Valid;     /*if RIO_TRUE model used intermediate crc value
                                            from the intermediate_Crc field instead of calculated one*/
    unsigned long   crc;
    RIO_BOOL_T      is_Crc_Valid;     /*if RIO_TRUE model used crc value from the crc field 
                                        instead of calculated one*/
    RIO_BOOL_T      is_PLLight_Model;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_UCHAR_T     rsrv_Ftype9; 
    RIO_UCHAR_T     cos;
    RIO_BOOL_T      s_Ds;
    RIO_BOOL_T      e_Ds;
    RIO_BOOL_T      o; 
    RIO_BOOL_T      p; 
    unsigned short  length;
    RIO_DS_INFO_T   stream_ID; 
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_UCHAR_T     rsrv_Ftype7; 
    RIO_UCHAR_T		x_On_Off;	/* Stop/Start issuing req for specified & lower priority 
					   transaction request flowP) 				*/
    RIO_UCHAR_T		flow_ID;	/* Highest priority affected transaction request flow 	*/
    RIO_ADDRESS_T	dest_ID;	/* Indicates which end pt the CCP is destined for (srcID
					   of the packet that caused the generation of the CCP) */
    RIO_ADDRESS_T	tgtdest_ID;	/* Combined with the flowID field, indicates which 
					   transaction request flows need to be acted upon 
					   (destinationID field of the packet which caused the 
					   generation of the CCP)				*/
    RIO_UCHAR_T		soc;		/* Source Of Congestion from Switch / End point	*/
    RIO_BOOL_T        	congestion;
    /* GDA: Bug#125 */
    RIO_BOOL_T        resp_Delay; /* GDA : Bug # 138 */

} RIO_PL_OUTBOUND_RQ_T;


/* this type represent entry state*/
typedef enum {
    RIO_PL_OUT_INVALID = 0,
    RIO_PL_OUT_READY,
    RIO_PL_OUT_WAIT_SPLIT,
    RIO_PL_OUT_WAIT_MULTICAST,
    RIO_PL_OUT_WAIT_INTERVEN,
    RIO_PL_OUT_SENDING,
    RIO_PL_OUT_SENT,
    RIO_PL_OUT_TIMEOUTED,
    RIO_PL_OUT_ACKED,
    RIO_PL_OUT_RESPONSED
} RIO_PL_OUT_STATE_T;

/* count of recipients of multicast request */
#define RIO_PL_OUT_MULTICAST_ENGINE_LENGHT 32

#define RIO_ALL_PACKETS 100 /* all received packets  (100 percents) */
#define RIO_PL_LRQ_RESET_CNT 4 /* four rest command must be issued to force reset */

/* the maximum length of packet stream */
#define RIO_PL_OUT_MAX_PACK_STREAM_LEN  ((GDA_PKT_SIZE*8)+20)  /* GDA 276 */

/* this structure represent unified type for outbound buffer entry*/
typedef struct {
    RIO_TAG_T            trx_Tag;           /* user-assigned tag of the request */
    RIO_PL_OUTBOUND_RQ_T entry;
    RIO_PL_OUT_STATE_T   state;
    unsigned int         pl_Retry_Cnt;
    unsigned int         ll_Retry_Cnt;
    RIO_DW_T             payload[RIO_PL_MAX_PAYLOAD_SIZE];
    unsigned int         payload_Size;
    RIO_DW_T             *payload_Source;
    unsigned int         res_For_Prio;
    unsigned int         time_After_Acked;
    unsigned int         time_After_Got_From_LL;
    unsigned int         resp_Cnt;
    unsigned int         max_Size; /* maximum size (for split) engine */
    unsigned char        offset; /* offset in DW  */
    unsigned char        byte_Offset;
    unsigned int         byte_Num;
    unsigned char        multicast_Size;
    unsigned long        tr_Infos[RIO_PL_OUT_MULTICAST_ENGINE_LENGHT];

    /* Only for PLLight model, stream request */
    RIO_UCHAR_T          packet_Stream[RIO_PL_OUT_MAX_PACK_STREAM_LEN];
    unsigned int         packet_Len;
    RIO_BOOL_T           is_AckID_Valid;
    RIO_BOOL_T		 is_Padded;

    /* Only for PLLight model */
    RIO_BOOL_T           is_Stream;
    int                  packet_Resend_Count;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_HW_T             payload_hw[RIO_PL_MAX_DS_PAYLOAD_SIZE]; 
    RIO_HW_T             *payload_Source_hw; 
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_BOOL_T           data_Corrupted; /*GDA:Bug#139 remove the need to recompute crc */

} RIO_PL_OUT_ENTRY_T;

/* state of queue's element */
typedef enum {
    RIO_PL_QUEUE_STATE_READY = 0,
    RIO_PL_QUEUE_STATE_SENT
} RIO_PL_QUEUE_ELEM_STATE_T;

/* the structure for queue value -1 entry free, index refer to the buffer */
typedef struct {
    int                       value;
    unsigned int              index;
    RIO_PL_QUEUE_ELEM_STATE_T state;
} RIO_PL_QUEUE_ELEM_T;

/* length of the intervention engine is always two */
#define RIO_PL_OUT_INTRV_ENGINE_LENGHT 2

/* the structure of intervention engine */
typedef struct{
    RIO_PL_OUT_ENTRY_T entries[RIO_PL_OUT_INTRV_ENGINE_LENGHT];
    RIO_BOOL_T         valid;
    unsigned int       source_Index;
} RIO_PL_OUT_INTERV_ENG_T;

/* the structure of multicast engine */
typedef struct{
    RIO_PL_OUT_ENTRY_T entries[RIO_PL_OUT_MULTICAST_ENGINE_LENGHT];
    RIO_BOOL_T         valid;
    unsigned int       source_Index;
    unsigned int       entries_Cnt;
} RIO_PL_OUT_MULTICAST_ENG_T;

/* the lenght of split engine */
#define RIO_PL_OUT_SPLIT_ENGINE_LENGHT 32

/* GDA: Bug#125 - Support for Flow control packet */
/* Watermark level for congestion */
#define  RIO_CONGESTION_BUF_SIZE 6
/* GDA: Bug#125 */

/* the structure of split engine */
typedef struct{
    RIO_PL_OUT_ENTRY_T entries[RIO_PL_OUT_SPLIT_ENGINE_LENGHT];
    RIO_BOOL_T         valid;
    unsigned int       source_Index;
    unsigned int       entries_Cnt;
} RIO_PL_OUT_SPLIT_ENG_T;

/* this structure represents additional out_buf parameteres */
typedef struct {
	unsigned long     logical_Time_Out;
        RIO_BOOL_T        only_Responses;
        RIO_BOOL_T        only_Maint_Responses;
        RIO_BOOL_T        ext_Address_Valid;
        RIO_BOOL_T        ext_Address_16;
} RIO_PL_OUT_ADD_PARAM_SET_T;

/* the structure of outbound buffer */
typedef struct {
    RIO_PL_OUT_ENTRY_T         temp;                            /* temporary buffer */
    RIO_PL_OUT_ENTRY_T         *buffer;                         /* buffer itself */
    RIO_PL_QUEUE_ELEM_T        *send_Queue;                     /* queue for saving order*/
    RIO_PL_OUT_INTERV_ENG_T    int_Eng;                         /* intervention engine*/
    RIO_PL_OUT_MULTICAST_ENG_T mult_Eng;                        /* multicast engine */ 
    RIO_PL_OUT_SPLIT_ENG_T     split_Eng;                       /* split engine */
    RIO_PL_OUT_ADD_PARAM_SET_T out_Buf_Param_Set;   /* add param set*/
} RIO_PL_OUT_BUFFER_T;

/* this type represents tha arbiter with current data stream */
typedef struct {
    RIO_UCHAR_T packet_Stream[RIO_PL_OUT_MAX_PACK_STREAM_LEN];
    unsigned int index;
    unsigned int packet_Len;
    unsigned int cur_Pos;
} RIO_PL_OUT_ARB_T;

/* the state of inbound buffer entry*/
typedef enum {
    RIO_PL_IN_INVALID = 0,
    RIO_PL_IN_READY
} RIO_PL_IN_STATE_T;

/* the entry of inbound buffer */
typedef struct {
    RIO_PL_OUTBOUND_RQ_T entry;
    RIO_PL_IN_STATE_T    state;
    RIO_DW_T             payload[RIO_PL_MAX_PAYLOAD_SIZE];
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_HW_T             payload_hw[RIO_PL_MAX_PAYLOAD_SIZE]; 
    /* GDA: Bug#124 - Support for Data Streaming packet */

    unsigned int         payload_Size;
    unsigned int         res_For_Prio;
} RIO_PL_IN_ENTRY_T;

typedef struct {
    RIO_REQ_HEADER_T     header;
    RIO_REMOTE_REQUEST_T request;
    RIO_DW_T             payload[RIO_PL_MAX_PAYLOAD_SIZE];
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_HW_T             payload_hw[RIO_PL_MAX_DS_PAYLOAD_SIZE];  
    /* GDA: Bug#124 - Support for Data Streaming packet */

} RIO_PL_IN_REQ_TO_LL_T;

/* this structure represents additional in_buf parameteres */
typedef struct {
    RIO_BOOL_T    only_Maint_Requests;
} RIO_PL_IN_ADD_PARAM_SET_T;

/* inbound buffer structure */
typedef struct {
    RIO_PL_IN_REQ_TO_LL_T               temp;       /* temporary buffer */
    RIO_PL_IN_ENTRY_T                   *buffer;    /* buffer itself */
    RIO_PL_QUEUE_ELEM_T                 *rec_Queue; /* */
    RIO_BOOL_T                          req_Wait;  /* there is a request which is wait */
    RIO_PL_IN_ADD_PARAM_SET_T       in_Buf_Param_Set;       /* add param set*/
} RIO_PL_IN_BUFFER_T;

/*
 * this type represents transmitter training machine state
 */
/*typedef enum {
        RIO_PL_TX_RESET = 0,
        RIO_PL_TX_SEND_TRN_REQ,
        RIO_PL_TX_SEND_TRN_PTTN,
        RIO_PL_TX_SEND_IDLES,
        RIO_PL_TX_OK,
        RIO_PL_TX_OK_SEND_TRN_PTTN,
        RIO_PL_TX_OK_SEND_IDLES,
        RIO_PL_TX_SEND_IDLES_REQ
} RIO_PL_TX_TRAIN_MACHINE_STATE_T;
*/
/*
 * this type represents receiver training machine state
 */

typedef enum {
    RIO_PL_RESET_RESET = 0,
    RIO_PL_RESET_OK,
    RIO_PL_RESET_PRE_RESET
} RIO_PL_RESET_MACHINE_STATE_T;


/*
 * this type represents transmitter or receiver state
 */
typedef enum {
    RIO_PL_OK = 0,
    RIO_PL_STOPPED_DUE_TO_ERROR,
    RIO_PL_STOPPED_DUE_TO_RETRY,
    RIO_PL_ERROR,
    RIO_PL_UNINIT,
    RIO_PL_INITIALIZING
} RIO_PL_TXRX_STATE_T;

/*
 * a control for representation RESET latch
 */
typedef struct {
        unsigned int   reset_Num; 
    RIO_BOOL_T     valid;
} RIO_PL_RESET_REQ_LATCH_T;
			
/* a packet retry generator */
typedef struct {
    unsigned int prob_Prio0; /* Probability of retry for packets w/ prio=0 */
    unsigned int prob_Prio1; /* Probability of retry for packets w/ prio=1 */
    unsigned int prob_Prio2; /* Probability of retry for packets w/ prio=2 */
    unsigned int prob_Prio3; /* Probability of retry for packets w/ prio=3 */
    unsigned int retry_Cnt; 
    RIO_BOOL_T   is_Retry_Sent;
} RIO_PL_RETRY_LATCH_T;

/* a packet-not-accepted generator */
typedef struct {
    unsigned int factor; /* factor for packet-not-accepted generator */
    unsigned int pnacc_Cnt; 
} RIO_PL_PNACC_LATCH_T;

/*
 * this type represents receiver date
 */
typedef struct {
    unsigned int                        expected_Ack_Id;
    unsigned int                        receiving_Ack_Id;
    RIO_PL_TXRX_STATE_T                 state;
    RIO_PL_RESET_MACHINE_STATE_T        train_State;
    RIO_BOOL_T                          in_Progress; /* packet receiving is in progress */
    unsigned int                        count_Idle_In_Packet;
    RIO_BOOL_T                          idle_Watch; /*watch idle in  input dur init*/
    RIO_PL_RESET_REQ_LATCH_T            reset_Req_Counter;
    RIO_BOOL_T                          pins_Disabled;
    RIO_PL_RETRY_LATCH_T                retry_Generator; /* packet retry generator */
    RIO_PL_PNACC_LATCH_T                pnacc_Generator; /* packet-not-accepted generator */
} RIO_PL_RX_DS_T;

/*
 * a control for representation TRAINING latch
 */
typedef struct {
        unsigned int    training_Cnt; 
    RIO_BOOL_T          valid;
    unsigned long       time_After_Sending;
} RIO_PL_TRAINING_LATCH_T;

/*
 * a control for representation link request latch
 */
typedef struct {
    RIO_GRANULE_T                   control; 
    RIO_BOOL_T                      valid;
    RIO_BOOL_T                      is_Sent;
    unsigned long                   time_After_Sending;
    unsigned int                    retry_Cnt;
    RIO_PL_TRAINING_LATCH_T         training_Counter;
} RIO_PL_LINK_REQUEST_LATCH_T;

/*
 * a control for representation link response latch
 */
typedef struct {
    RIO_GRANULE_T  control; 
    RIO_BOOL_T     valid;
    unsigned int   last_Ack; /* last ack shell ce sent before */
} RIO_PL_LINK_RESPONSE_LATCH_T;

/*
 * a control for representation THROTTLE  latch
 */
typedef struct {
    RIO_GRANULE_T  control; 
    RIO_BOOL_T     valid;
} RIO_PL_THROT_FOR_SEND_LATCH_T;


/*
 * a control for representation THROTTLE latch
 */
typedef struct {
    unsigned int   idle_Cnt; 
    RIO_BOOL_T     valid;
} RIO_PL_THROTTLE_LATCH_T;


/*
 * a control for representation idle latch
 */
typedef struct {
    unsigned int   idle_Cnt; 
    RIO_BOOL_T     valid;
} RIO_PL_IDLE_LATCH_T;


/*a random idle generator*/
typedef struct {
    unsigned int idle_Cnt;
    unsigned int gr_Num;    /*number of granule after wjich is necessary to insert */
    RIO_BOOL_T   valid;
    RIO_BOOL_T   packet_Ongoing; /*true during packet issuing to the link*/
    unsigned int lb, hb;    /*low boundary and high boundary for random generator*/
} RIO_PL_RND_IDLE_LATCH_T;

/* a LRQ-Reset generator */
typedef struct {
    unsigned int factor; /* factor for LRQ-Reset generator */
    unsigned int lrqr_Cnt; 
    RIO_BOOL_T   need_RESET;
}RIO_PL_LRQR_LATCH_T;

/* a packet stomp generator */
typedef struct {
    unsigned int factor; /* factor for packet stomp generator */
    unsigned int stomp_Cnt; 
    RIO_BOOL_T   sent_STOMP;      /* stomp is sent */
    unsigned int sent_STOMP_ack_ID;       /* stomp ackID */
}RIO_PL_STOMP_LATCH_T;

/*  the buffer for heading out acknowledgments*/
#define RIO_PL_ACK_BUF_SIZE 8

typedef struct {
    /*RIO_GRANULE_T  ack_Buffer[RIO_PL_ACK_BUF_SIZE];*/
    RIO_GRANULE_T  *ack_Buffer;
    unsigned int   buffer_Head; /* the ack for sending */
    unsigned int   buffer_Top;  /* the free entry for new ack*/
    RIO_BOOL_T     is_Full;     /* buffer is full*/
} RIO_PL_OUT_ACK_BUF_T;

/* this type represent an entry for outbound packet handling*/
typedef struct {
    unsigned int           packet_Handle;
    unsigned long          time_After_Sending;
    RIO_PL_OUT_STATE_T     state;
} RIO_PL_OUT_T;

typedef struct {
/*    RIO_PL_OUT_T          ack_Buffer[RIO_PL_ACK_BUF_SIZE];*/
    RIO_PL_OUT_T          *ack_Buffer;
    unsigned int          buffer_Head; /* the ack for handling */
    unsigned int          buffer_Top;  /* the free entry for new ack*/
    RIO_BOOL_T            is_Full;     /* buffer is full*/
} RIO_PL_IN_ACK_BUF_T;

/* this structure represents tx parameters */
typedef struct {
    unsigned long lresp_Time_Out;
/*  unsigned int  lresp_Retry_Cnt;*/
} RIO_PL_TX_ADD_PARAM_SET_T;

/* latch to store restart from retry */
typedef struct {
    RIO_GRANULE_T control;
    RIO_BOOL_T    valid;
} RIO_PL_REST_F_RETRY_LATCH_T;

/*
 * this type represents transmitter date
 */
typedef struct {
    unsigned int                                expected_Ack_Id;
    RIO_PL_TXRX_STATE_T                         state;       /* state of transmitter */
    RIO_BOOL_T                                  in_Progress; /* packet transmitting is in progress */
    RIO_PL_OUT_ACK_BUF_T                        ack_Out_Buffer; /* heading out acks*/
    RIO_PL_IN_ACK_BUF_T                         ack_In_Buffer;  /* coming in acks*/
    RIO_PL_LINK_REQUEST_LATCH_T                 link_Request;   /* link request for sending*/
    RIO_PL_LINK_RESPONSE_LATCH_T                link_Response;  /* link response for sending */
    RIO_PL_REST_F_RETRY_LATCH_T                 restart_From_Retry; /* restart from retry latch */
    RIO_PL_THROTTLE_LATCH_T                     throttle_Generator;
    RIO_PL_IDLE_LATCH_T                         idle_Generator;
    RIO_PL_RND_IDLE_LATCH_T                     rnd_Idle_Generator;
    RIO_PL_THROT_FOR_SEND_LATCH_T               throttle_For_Send;
    RIO_PL_TRAINING_LATCH_T                     training_Generator;
    RIO_BOOL_T                                  idle_Watch; /*watch idle in  output dur init*/  
    RIO_PL_TX_ADD_PARAM_SET_T                   tx_Param_Set;
    RIO_BOOL_T                                  need_STOMP;      /* packet shall be canceled by STOMP*/
    RIO_BOOL_T                                  need_STOMP_Due_Gen;      /* packet shall be canceled by STOMP*/
    RIO_BOOL_T                                  pins_Disabled;
    RIO_BOOL_T                                  force_EOP; /*enable regime with the forced eop*/
    RIO_PL_STOMP_LATCH_T                        stomp_Generator; /* packet stomp generator */
    RIO_PL_LRQR_LATCH_T                         lrqr_Generator; /* packet stomp generator */
} RIO_PL_TX_DS_T;


/* 
 * function tray to be used internally 
 */
typedef struct {
    RIO_PL_II_LINK_TX_DISABLING         rio_Link_Tx_Disabling;
    RIO_PL_II_LINK_RX_DISABLING         rio_Link_Rx_Disabling;
    RIO_PL_II_INIT_MACHINE_CHANGE_STATE rio_Init_Block_Machine_Change_State;
} RIO_INTERNAL_FTRAY_T;

#define RIO_PL_DS_TRN_ARRAY_SIZE    4
/* 
 * This set of data stores all the parameters which may be specified 
 * to PL in either serial and arallel modes
 */
typedef struct {

/* Parameters common for both serial and parallel modes */

    RIO_PL_TRANSP_TYPE_T transp_Type;
    RIO_TR_INFO_T dev_ID;

    /* values of corresponding registers */
    RIO_WORD_T    lcshbar;
    RIO_WORD_T    lcsbar;
        
    /*These fields define the initial value  of General Control CSR (offset 0x038 lower word(EF)*/
    RIO_BOOL_T    is_Host;
    RIO_BOOL_T    is_Master_Enable;
    RIO_BOOL_T    is_Discovered;

    /*
     * These fields define structure of the address information 
     * for the incoming packets recived by this PE
     */
    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */
        
    RIO_BOOL_T  ext_Mailbox_Support; /* PE supports extended mailbox (msglen=0, mailbox_number=msgseg||mbox) */

    /*define if output and input are enable after reset(enable to send and receive all kinds
    of packets (if False - only MAINTENANCE requests and responds are processed))*/
    RIO_BOOL_T    input_Is_Enable;
    RIO_BOOL_T    output_Is_Enable;

    /*
     * these parameters define reservation police for outbound buffer
     */
    RIO_UCHAR_T res_For_3_Out;
    RIO_UCHAR_T res_For_2_Out;
    RIO_UCHAR_T res_For_1_Out;

    /*
     * these parameters define reservation police for inbound buffer
     */
    RIO_UCHAR_T res_For_3_In;
    RIO_UCHAR_T res_For_2_In;
    RIO_UCHAR_T res_For_1_In;

    /*
     * order in that inbound packets are passed to the environment
     * pass_By_Prio     if TRUE the most priority packet goes first
     *                      else the oldest
     */
    RIO_BOOL_T pass_By_Prio;

    RIO_BOOL_T transmit_Flow_Control_Support; /*RIO_TRUE - flow control is detected automaticly,
                                                RIO_FALSE - only rcv flow control is supported*/
    RIO_BOOL_T enable_Retry_Return_Code;   /*if true pl will return RIO_RETRY code*/
    RIO_BOOL_T enable_LRQ_Resending;       /*if true the models will resend the link request
                                                symbol if link responce is not accepted or
                                                accepted lresponce with incorrect ackid
                                                if false then model will fail to the unrec. error*/

/* Parameters specific for serial mode */

    /*TRUE in case os 1x serial link*/
    RIO_BOOL_T    lp_Serial_Is_1x; 

    /*TRUE in case of 4x link used in 1x mode (valid if lp_Serial_Is_1x == RIO_FALSE)*/
    RIO_BOOL_T    is_Force_1x_Mode; 

    /*TRUE in case of 4x link used in 1x mode on lane 0
    (valid if lp_Serial_Is_1x == RIO_FALSE, is_Force_1x_Mode == RIO_TRUE)*/
    RIO_BOOL_T    is_Force_1x_Mode_Lane_0; 
        
    unsigned int  silence_Period;   
    unsigned int  comp_Seq_Rate; /*in spec - 5000*/
    unsigned int  status_Sym_Rate; /*in spec - 1024 code groups*/
    unsigned int  discovery_Period;

    unsigned int  comp_Seq_Size; 
    unsigned int  comp_Seq_Rate_Check; /*in spec - 5000*/


/* Parameters specific for parallel mode */

    /*
     * lp_Ep_Is_8       if set to TRUE make 8LP/EP interface,
     *                      else 16LP/EP.
     */
    RIO_BOOL_T   lp_Ep_Is_8; 

    /*defines the current working mode : RIO_TRUE if operating mode is 8 - bit*/
    RIO_BOOL_T   work_Mode_Is_8;
        
    /*
     * this parameter define limit number of sent 
     * Training patterns (actually symbols pair)
     * Should be 1 or more.
     */
    unsigned int num_Trainings;

    /*defines if the model needs training sequence*/
    RIO_BOOL_T   requires_Window_Alignment;

    /*defines the training pattern type to be used by the model*/
    unsigned int custom_Training_Pattern[RIO_PL_DS_TRN_ARRAY_SIZE];
        
    /*
     * this parameter define limit number of continiously received 
     * IDLE symbols embedded in a packet if this value is oversteped
     * packet acknowledge by RETRY
     */
    unsigned int receive_Idle_Lim;

    /*
     * this parameter define limit number of IDLE, which is set in received
     * THROTTLE - if this value is overstepped packet transmittion terminated
     * STOMP sending
     */
    unsigned int throttle_Idle_Lim;

} RIO_PL_PARAM_SET_T;

/*
 * This set represents callbacks which are supplide to PL 
 * THis composes both serial and parallel modes callbacks
 */
typedef struct {   

    RIO_PL_ACK_REQUEST      rio_PL_Ack_Request;
    RIO_PL_GET_REQ_DATA     rio_PL_Get_Req_Data;
 
    RIO_PL_ACK_RESPONSE     rio_PL_Ack_Response;
    RIO_PL_GET_RESP_DATA    rio_PL_Get_Resp_Data;
    RIO_PL_REQ_TIME_OUT     rio_PL_Req_Time_Out;
    RIO_PL_REMOTE_REQUEST   rio_PL_Remote_Request;
    RIO_PL_REMOTE_RESPONSE  rio_PL_Remote_Response;
    RIO_CONTEXT_T           pl_Interface_Context; 
    
    RIO_USER_MSG            rio_User_Msg;
    RIO_CONTEXT_T           rio_Msg;

    RIO_REMOTE_CONFIG_READ  extend_Reg_Read;
    RIO_REMOTE_CONFIG_WRITE extend_Reg_Write;
    RIO_CONTEXT_T           extend_Reg_Context;

    RIO_REQUEST_RECEIVED    rio_Request_Received;
    RIO_RESPONSE_RECEIVED   rio_Response_Received;
    RIO_SYMBOL_TO_SEND      rio_Symbol_To_Send;
    RIO_PACKET_TO_SEND      rio_Packet_To_Send;
    RIO_CONTEXT_T           rio_Hooks_Context;

    RIO_PLLIGHT_PACKET_RECEIVED rio_PLLight_Packet_Received;
    RIO_PLLIGHT_PACKET_ACKNOWLEDGED rio_PLLight_Packet_Acknowledged;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_GET_REQ_DATA_HW  rio_PL_Get_Req_Data_Hw;    
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_PL_FC_TO_SEND      rio_PL_FC_To_Send;
    /* GDA: Bug#125 */

} RIO_PL_MODEL_CALLBACK_TRAY_T;


/*
 * this type represent physical layer data structure, which contains its
 * internal state, register pool, buffers, callback tray, etc
 */
typedef struct {
    RIO_BOOL_T                   is_Parallel_Model; /*shall be RIO_TRUE in case of parallel model instantiation*/
    unsigned int                 ack_Buffer_Size; /*8 or 32 depending on mode*/

    unsigned long                instance_Number;

    unsigned char                tid;
    RIO_BOOL_T                   in_Reset;
    RIO_BOOL_T                   initialized; /* used for show state in reg*/
    RIO_BOOL_T                   was_Bound;
  
    RIO_PL_REGS_T                       reg_Set;
    RIO_PL_STATE_INDICATORS_PARAMS_T    state_Indicators_Params;

    RIO_PL_RX_DS_T               rx_Data;
    RIO_PL_TX_DS_T               tx_Data;
    RIO_PL_OUT_BUFFER_T          outbound_Buf; /*outbound buffer */
    RIO_PL_OUT_ARB_T             outbound_Arb; /* current transmitting packet */
    RIO_PL_OUT_ARB_T             inbound_Arb;  /* current receiving packet */
    RIO_PL_IN_BUFFER_T           inbound_Buf;  /* inbound buffer */
   
    RIO_BOOL_T                   error_Checking_Disabled;
    
    /*external data*/
    RIO_PL_PARAM_SET_T           parameters_Set;
    RIO_HANDLE_T                 specific_Param_Set;
    RIO_MODEL_INST_PARAM_T       inst_Param_Set;
    RIO_PL_II_MODEL_INITIALIZE   restart_Function;
    RIO_PL_MODEL_START_RESET     reset_Function;
    RIO_PL_MODEL_COMMAND_TO_WRAP cmd_To_Wrap;
                                 
    RIO_PL_MODEL_CALLBACK_TRAY_T ext_Interfaces;
    RIO_HANDLE_T                 ext_Specific_Interfaces;

    RIO_INTERNAL_FTRAY_T         internal_Ftray;

    RIO_HANDLE_T                 mid_Block_Interface; /*medium block*/
    RIO_HANDLE_T                 mid_Block_Instanse;

    RIO_HANDLE_T                 link_Interface; /*lower block*/
    RIO_HANDLE_T                 link_Instanse;

    RIO_BOOL_T                  is_PLLight_Model;/*shall be RIO_TRUE in case of pl light model instantiation*/

    RIO_WORD_T ack_Cycle_Delay_Counter; /* GDA: Description- Cycle Delay Counter */
    RIO_WORD_T ack_Delay_Period;        /* GDA: Description- Set delay period */
    RIO_BOOL_T set_Random;              /* GDA: Description- It should be '1' always */
    RIO_BOOL_T response_Packet_Delay_Flag; /*GDA: Description- Response Packet Flag */

    RIO_UCHAR_T pe_Pl_Tx_Illegal_Pnacc_Resend_Cnt;/*GDA:Tx Illegal Pkt PNACC Resend Counter(PE/PL)Bug#43 Fix*/

  /* GDA:PNACC RND GENERATOR */
    RIO_WORD_T			 SOP_Count;    /*GDA: Description - Start of Packet(SOP) Counter */
    RIO_WORD_T 			 granule_Count;/*GDA: Description - Granule Counter for each SOP*/

    RIO_BOOL_T 			 is_Rnd_Generator_Invoked;      /*Status Flag - Rnd Generator*/
    RIO_BYTE_T       		 pnacc_Probability;             /* PNACC Prob*/
    RIO_BOOL_T			 set_Pnacc_Rnd_Generator_Flag;
    RIO_BOOL_T                   set_Pnacc_Flag;							          RIO_WORD_T		       pnacc_Clock_Cycle_Counter;
    RIO_WORD_T	         	 pnacc_Clock_Cycle_Period;
    RIO_WORD_T			 pnacc_Low_Boundary;
    RIO_WORD_T                   pnacc_High_Boundary;
  /* GDA:PNACC RND GENERATOR */


  /* GDA:RETRY RND GENERATOR */
    RIO_BYTE_T       		 retry_Probability_Prio0;
    RIO_BYTE_T       		 retry_Probability_Prio1;
    RIO_BYTE_T       		 retry_Probability_Prio2;
    RIO_BYTE_T       		 retry_Probability_Prio3;
    RIO_BOOL_T			 set_Retry_Rnd_Generator_Flag;
    RIO_BOOL_T                   set_Retry_Flag;							
    RIO_WORD_T			 retry_Clock_Cycle_Counter;
    RIO_WORD_T			 retry_Clock_Cycle_Period;
    RIO_WORD_T			 retry_Low_Boundary;
    RIO_WORD_T                   retry_High_Boundary;
 /* GDA:RETRY RND GENERATOR */

    /* GDA: Bug#116 Implementation */
    RIO_BOOL_T			 multicast_Enable_Flag;  /*GDA Implemented Bug#116 */
    RIO_WORD_T			 multicast_Low_Boundary;
    RIO_WORD_T                   multicast_High_Boundary;
    RIO_WORD_T			 multicast_Clock_Cycle_Counter;
    RIO_BOOL_T			 set_Multicast_Rnd_Generator_Flag;
    RIO_WORD_T			 multicast_Clock_Cycle_Period;
    RIO_BOOL_T                   set_Multicast_Flag;
    /* GDA: Bug#116 Implementation */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_BOOL_T                   is_congestion; 
    /* GDA: Bug#125 - Support for Flow control packet */
    unsigned  char               trans_Id; /*GDA:Bug#137 -passing transaction-id from the testbench*/
    RIO_BOOL_T                   data_Corrupted; /*GDA:Bug#139 remove the need to recompute crc */
       /* GDA: Bug#138 */
    RIO_BOOL_T	  resp_Packet_Delay_Enable; /* To delay the res pkt.  RIO_FALSE - default */
    RIO_BOOL_T	  is_resp_Packet;	    /* 1st time to identify the res pkt. RIO_FALSE - default */
    RIO_WORD_T    resp_Packet_Delay_Cntr;   /* Delay Counter.  '0' - default */
    RIO_BOOL_T	  resp_Packet_Send; /* To delay the res pkt.  RIO_FALSE - default */
    unsigned int  check_Ack_Id;
    RIO_BOOL_T    resp_Stomp_Needed; /* To Discard a packet */
    /* GDA: Bug#138 */
 
} RIO_PL_DS_T;

/* hardcore constants */
#define RIO_PL_MAX_PRIO_VALUE         3 /* maximum priority value */
#define RIO_PL_MAX_PAYLOAD_SIZE       GDA_PKT_SIZE /* GDA - size of maximum payload */
#define RIO_PL_BYTE_CNT_IN_DWORD      8 /* count of bytes in double word */
#define RIO_PL_MAX_OFFSET_IN_DWORD    7 /* max offest within double word */
#define RIO_PL_THE_SECOND_WORD_OFFSET 4 /* offest of the second word in dword */
#define RIO_PL_MAX_MBOX_VALUE         3 /* maximum message box number */
#define RIO_PL_MAX_MSGLEN_VALUE       15 /* maximum length of message */
#define RIO_PL_MAX_LETTER_VALUE       3  /* maximum letter number */
#define RIO_PL_BYTE_CNT_IN_WORD       4 /* byte count in word */
#define RIO_PL_MAX_BUFFER_SIZE        0x7f /* GDA 3f maximum count of entry in a buffer */

/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_PL_BYTE_CNT_IN_HALF_WORD  2   
/* GDA: Bug#124 - Support for Data Streaming packet */

#define RIO_PL_INIT_CRC_VALUE         0xffff
#define RIO_PL_PACKET_LEN_NEED_INT_CRC 84
#define RIO_PL_INT_CRC_START           80
#define RIO_PL_BITS_IN_BYTE            8
#define RIO_PL_QUEUE_ELEM_UNUSED       -1
#define RIO_PL_FTYPE_POS               1
#define RIO_PL_TTYPE_POS_LOW           4
#define RIO_PL_TTYPE_POS_HIGH          6
#define RIO_PL_TTYPE_MASK              0xf0
#define RIO_PL_TTYPE_SHIFT             4
#define RIO_PL_FTYPE_MASK              0x0f
#define RIO_PL_TT_POS                  1
#define RIO_PL_TT_MASK                 0x30
#define RIO_PL_TT_SHIFT                4
#define RIO_PL_STATUS_MASK             0x0f
#define RIO_PL_BYTES_IN_HALF_WORD      2
#define RIO_PL_PRIO_POS                1
#define RIO_PL_PRIO_MASK                0xc0
#define RIO_PL_PRIO_SHIFT               6
#define RIO_PL_TRANSP_TYPE_POS          2
#define RIO_PL_SIZE_MASK                0x0f
#define RIO_PL_CNT_OF_INT_RESP          2
#define RIO_PL_MAX_TID_PLUS_ONE         256
#define RIO_PL_REG_MISALIGN_MASK        0x03
#define RIO_PL_REG_HEADER_SHIFT         16
#define RIO_PL_TRAINING_NUM             256 /*number of training simbols in training pattern*/

/*registers constants*/
/*error and status register*/
#define RIO_PL_ERROR_STATE_CLEAR_MASK       0x00120204 /* mask for clearing state in register */
#define RIO_PL_ERROR_PORT_STATE_CLEAR_MASK  0xfffffffc /* mask for clearing common state in register(without present bit)*/
#define RIO_PL_ERROR_STATE_UNINIT           0x00000001 /* port is uninitialized */
#define RIO_PL_ERROR_STATE_OK               0x00000002 /* port is initialized */
#define RIO_PL_ERROR_STATE_UNREC_ERROR      0x00000004 /* port is is in unrecoverable error*/
#define RIO_PL_ERROR_STATE_PRESENT          0x00000008 /* port is present */

#define RIO_PL_ERROR_TX_STATE_CLEAR_MASK        0xfff2ffff /* mask for clearing tx state in register */
#define RIO_PL_ERROR_RX_STATE_CLEAR_MASK        0xfffffaff /* mask for clearing tx state in register */

#define RIO_PL_ERROR_TX_ERROR                           0x00030000 /*recoverable error was detected*/
#define RIO_PL_ERROR_TX_RETRY                           0x001c0000 /*retry state was detected*/

#define RIO_PL_ERROR_RX_ERROR                           0x00000300 /*recoverable error was detected*/
#define RIO_PL_ERROR_RX_RETRY                           0x00000400 /*retry state was detected*/

/*port n control CSR*/

/*these constants are necessary to check packet header size - in bits count*/
#define RIO_PL_FIRST_16_BIT_SIZE       16
#define RIO_PL_TR_INFO_SIZE_16         16
#define RIO_PL_TR_INFO_SIZE_32         32
#define RIO_PL_EXT_ADDR_SIZE_16        16
#define RIO_PL_EXT_ADDR_SIZE_32        32
#define RIO_PL_CRC_FIELD_SIZE          16 /*size of one CRC field*/


/*all packet header sizes not includes first 16 packet bits,
 *size of transport info, extended address fields
 */
#define RIO_PL_PACK_8_HEADER_SIZE      48 /*include hop count*/
#define RIO_PL_PACK_13_HEADER_SIZE     16
#define RIO_PL_PACK_2_HEADER_SIZE      48
#define RIO_PL_PACK_5_HEADER_SIZE      48
#define RIO_PL_PACK_6_HEADER_SIZE      32
#define RIO_PL_PACK_10_HEADER_SIZE     32
#define RIO_PL_PACK_11_HEADER_SIZE     16
#define RIO_PL_PACK_1_HEADER_SIZE      64
/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_PL_PACK_9_HEADER_SIZE      32 
#define RIO_PL_PACK_9_RSRV_FIELD_SIZE  4 
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
#define RIO_PL_PACK_7_HEADER_SIZE      32
#define RIO_PL_PACK_7_RSRV_FIELD_SIZE  7
/* GDA: Bug#125 */

/*bit size of reserved packet fields*/
#define RIO_PL_PACK_6_RSRV_FIELD_SIZE          1       
#define RIO_PL_PACK_8_REQ_RSRV_FIELD_SIZE      2    
#define RIO_PL_PACK_8_RESP_RSRV_FIELD_SIZE     24
#define RIO_PL_PACK_10_RSRV_FIELD_SIZE         8

/*free buffers watemarks*/
#define RIO_PL_WM0      4
#define RIO_PL_WM1      3
#define RIO_PL_WM2      2

/*constant for source ID extraction*/
#define RIO_PL_8_BIT_DEV_ID_MASK    0xFF
#define RIO_PL_16_BIT_DEV_ID_MASK   0xFFFF

/*buffer mask*/
#define RIO_PL_ALL_BUFS_FREE    0xF

/****************************************************************************/
#endif /* RIO_PL_DS_H */

