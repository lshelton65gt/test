#ifndef RIO_PL_REGISTERS_H
#define RIO_PL_REGISTERS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_registers.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer errors/warnings handlers prototypes
*               and data 
* Notes:
*
******************************************************************************/

/*register map boundaries*/
#define RIO_EF_SPACE_BOTTOM         (unsigned long) 0x0100
#define RIO_EF_SPACE_TOP            (unsigned long) 0xFFF8

#define RIO_ID_SPACE_BOTTOM         (unsigned long) 0x010000
#define RIO_ID_SPACE_TOP            (unsigned long) 0xFFFFF8

/*
 * rio registers addresses
 */
#define RIO_DEV_IDS_A              0x00
#define RIO_DEV_INFO_A             0x04

#define RIO_ASS_IDS_A              0x08
#define RIO_ASS_INFO_A             0x0c

#define RIO_PE_FEAT_A              0x10
#define RIO_SWITCH_INFO_A          0x14

#define RIO_SOURCE_OPS_A           0x18
#define RIO_DEST_OPS_A             0x1c

/*
#define RIO_MAX_SOURCE_SIZE_A      0x20
#define RIO_MAX_DEST_SIZE_A        0x24

#define RIO_MAX_SOURCE_MES_SIZE_A  0x28
#define RIO_MAX_DEST_MES_SIZE_A    0x2c
*/

#define RIO_MAILBOX_STATUS_A       0x40
#define RIO_WRITE_PORT_STATUS_A    0x44
/*
#define RIO_CONF_SOURCE_SIZE_A     0x48
#define RIO_CONF_DEST_SIZE_A       0x4c
*/

#define RIO_PE_LL_CONTROL_A           0x4c

/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_PE_LL_DS_CONTROL_A        0x48  
#define RIO_PE_LL_DS_INFO_A           0x3c  
/* GDA: Bug#124 - Support for Data Streaming packet */


/*
#define RIO_CONF_SOURCE_MES_SIZE_A 0x50
#define RIO_CONF_DEST_MES_SIZE_A   0x54
*/
#define RIO_LCSHBAR_A               0x58
#define RIO_LCSBAR_A                0x5c

/*transport registers*/
#define RIO_BASE_DEV_ID_A           0x60
#define RIO_HOST_BASE_DEV_ID_LOCK_A 0x68
#define RIO_COMPONENT_TAG_A         0x6c



#define RIO_PORT_MAINT_HEADER_0_A  0x0000
#define RIO_PORT_MAINT_HEADER_1_A  0x0004

#define RIO_EXTEND_FEAT_HEADER_0_A  0x0300
#define RIO_EXTEND_FEAT_HEADER_1_A  0x0304


/* LP-EP registers */
/*#define RIO_LPEP_REG_BASE        0x0100 */
#define RIO_LPEP_REG_GEN_SIZE      0x40
#define RIO_LPEP_REG_INDEX         0x20

/*
#define RIO_LINK_VALID_REQ_A       0x0120
#define RIO_LINK_VALID_RESP_A      0x0124


#define RIO_LINK_VALID_TO1_A       0x0130
#define RIO_LINK_VALID_TO2_A       0x0134

#define RIO_LINK_ERROR_A           0x0138
*/

/*common*/
#define RIO_LINK_VALID_TO1_A       0x0020
#define RIO_LINK_VALID_TO2_A       0x0024

#define RIO_LINK_GEN_CONTROL_A     0x003c

#define RIO_LINK_ERROR_A      0x0058
#define RIO_LINK_CONTROL_A    0x005c

/*constant for setting pending bit in the port n error and status CSR*/
#define RIO_SET_PENDING_BIT_MASK    0x10
/* for software assistant error recovery*/
#define RIO_LINK_VALID_REQ_A        0x0040
#define RIO_LINK_VALID_RESP_A       0x0044
#define RIO_LINK_ACKID_STATUS_A     0x0048
#define RIO_LINK_BUF_STATUS_A       0x004c /*for internal purpose - buf_status indicator: input - 12-15 bits, output - 28-31 bits*/

int RIO_PL_Write_Register(
    RIO_PL_DS_T       *handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     data,
    RIO_BOOL_T        internal
    );

int RIO_PL_Read_Register(
    RIO_PL_DS_T       *handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     *data,
    RIO_BOOL_T        internal
    );

int RIO_PL_Set_State(
    RIO_PL_DS_T              *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Param
    );

int RIO_PL_Get_State(
    RIO_PL_DS_T              *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param
    );


int RIO_PL_Init_Regs(RIO_PL_DS_T *handle);

int RIO_PL_Get_Max_Resp_Dem_Size(RIO_PL_DS_T *handle);

int RIO_PL_Get_Max_Port_Wr_Size(RIO_PL_DS_T *handle);

int RIO_PL_Get_Max_Resp_Size(RIO_PL_DS_T *handle);

unsigned int RIO_PL_Conv_Size_To_Dw_Cnt(unsigned int size);

/* GDA: Bug#124 - Support for Data Streaming packet */
unsigned int RIO_PL_Conv_Size_To_Hw_Cnt(unsigned int size);  
/* GDA: Bug#124 - Support for Data Streaming packet */


unsigned int RIO_PL_Get_Max_Req_Size(RIO_PL_DS_T *handle);

int RIO_PL_Get_Max_Mes_Size(RIO_PL_DS_T *handle, unsigned int mbox);

int RIO_PL_Get_Max_Mes_Cnt(RIO_PL_DS_T *handle, unsigned int mbox);

int RIO_PL_Get_Max_Gr_Size(RIO_PL_DS_T *handle);

int RIO_PL_Get_Max_Dem_Gr_Size(RIO_PL_DS_T *handle);

/*****************************************************************************/
#endif /* RIO_PL_REGISTERS_H*/
