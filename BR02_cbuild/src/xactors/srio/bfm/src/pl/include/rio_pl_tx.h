#ifndef RIO_PL_TX_H
#define RIO_PL_TX_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/include/rio_pl_tx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer transmitter header
*               
* Notes:
*
******************************************************************************/

#ifndef RIO_PL_DS_H
#include "rio_pl_ds.h"
#endif

/* main transmitter loop*/
void RIO_PL_Granule_For_Transmitting(RIO_PL_DS_T *instanse, RIO_GRANULE_T *granule);

/* store an acknowledgment for heading out */
void RIO_PL_Tx_Send_Ack(RIO_PL_DS_T *handle, RIO_GRANULE_T *ack_Granule);

/* an acknowledgment to headed out packet has been received*/
void RIO_PL_Tx_Rec_Ack(RIO_PL_DS_T *handle, RIO_GRANULE_T *ack_Granule);

/* a link response has been received*/
void RIO_PL_Tx_Rec_Link_Resp(RIO_PL_DS_T *handle, RIO_GRANULE_T *ack_Granule);

/* a THROTTLE has been received */
void RIO_PL_Tx_Rec_Throttle(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/* a unexpected packet control has been received */
void RIO_PL_Tx_Rec_Unexp_Packet_Control(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/* a Training sequence is under processing and symbol has been received*/
void RIO_PL_Tx_Rec_Training_Seq(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/* a link request/send_training has been received*/
void RIO_PL_Tx_Rec_Link_Req(RIO_PL_DS_T *handle, RIO_GRANULE_T *link_Request);

/*detect, if received control symbol is expected (now for TRAININGs only))*/
RIO_BOOL_T RIO_PL_Tx_Control_Is_Expected(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/* a THROTTLE for sending */
void RIO_PL_Tx_Send_Throttle(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/* initialize the transmitter*/
void RIO_PL_Tx_Init(RIO_PL_DS_T *handle);

/* a LINK_REQUEST shall be sent*/
void RIO_PL_Tx_Send_Link_Req(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/* a Training pattern shall be sent*/
void RIO_PL_Tx_Send_Training(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule, unsigned int num_Trainings);

/* a link response shall be sent */
void RIO_PL_Tx_Send_Link_Resp(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

void RIO_PL_Tx_Get_Link_Request(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);

/* perform LINK REQUEST time out proceding */
void RIO_PL_Tx_Link_Req_Time_Out(RIO_PL_DS_T *handle);

/* perform LINK REQUEST/send_Training time out proceding */
void RIO_PL_Tx_Link_Req_Training_Time_Out(RIO_PL_DS_T *handle);

/* perform physical layer time-out */
void RIO_PL_Tx_Phys_Time_Out(RIO_PL_DS_T *handle);

/*get inbound free buf size*/
int RIO_PL_Get_Our_Buf_Status(RIO_PL_DS_T *handle);
/*get partner buf status*/
int RIO_PL_Get_Partner_Buf_Status(RIO_PL_DS_T *handle);

/* GDA: perform ack delay time-out */
void ack_Delay_Time_Out(RIO_PL_DS_T *handle);

/* GDA: generates random delay */
unsigned long random_Delay_Period(RIO_WORD_T min_Delay,RIO_WORD_T max_Delay);


/****************************************************************************/
#endif /* RIO_PL_TX_H */

