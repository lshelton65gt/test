/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/rio_pl_in_buf.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Physical layer inbound buffer source
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "rio_pl_in_buf.h"
#include "rio_pl_ds.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"


/***************************************************************************
 * Function : RIO_IN_RES_FOR_3
 *
 * Description: return count of reserved for 3 prio packets in inbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_IN_RES_FOR_3(handle) (handle)->parameters_Set.res_For_3_In

/***************************************************************************
 * Function : RIO_IN_RES_FOR_2
 *
 * Description: return count of reserved for 2 prio packets in inbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_IN_RES_FOR_2(handle) (handle)->parameters_Set.res_For_2_In

/***************************************************************************
 * Function : RIO_IN_RES_FOR_1
 *
 * Description: return count of reserved for 1 prio packets in inbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_IN_RES_FOR_1(handle) (handle)->parameters_Set.res_For_1_In

/***************************************************************************
 * Function : RIO_IN_GET_ENTRY
 *
 * Description: return address an entry in inbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_IN_GET_ENTRY(handle, i) ((handle)->inbound_Buf.buffer + (i))

/***************************************************************************
 * Function : RIO_IN_GET_QUEUE_ELEM
 *
 * Description: return address of an element in inbound queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_IN_GET_QUEUE_ELEM(handle, i) ((handle)->inbound_Buf.rec_Queue + (i))

/***************************************************************************
 * Function : RIO_IN_GET_TEMP
 *
 * Description: return temporary buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_IN_GET_TEMP(handle) ((handle)->inbound_Buf.temp)

/***************************************************************************
 * Function : RIO_IN_GET_ARB
 *
 * Description: return inbound arbiter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_IN_GET_ARB(handle) ((handle)->inbound_Arb)


/* initialize one inbound buffer entry */
static void init_Buffer_Entry(RIO_PL_IN_ENTRY_T *element, unsigned int prio);

static void init_In_Temp(RIO_PL_IN_REQ_TO_LL_T *element);

/* returns the start porition of the end CRC */
static unsigned int get_CRC_Start(RIO_PL_DS_T *handle);

/* detect corresponding fields from packet stream */
static unsigned int get_Ftype(RIO_PL_OUT_ARB_T *arbiter);
static unsigned int get_Ttype(RIO_PL_OUT_ARB_T *arbiter);
static unsigned int get_Tt(RIO_PL_OUT_ARB_T *arbiter);
static unsigned int get_Prio(RIO_PL_OUT_ARB_T *arbiter);
static unsigned long get_Transp_Info(RIO_PL_OUT_ARB_T *arbiter);

/* parse request packets*/
static void prod_Req_8(RIO_PL_DS_T *handle, unsigned int i);
static void prod_Req_10(RIO_PL_DS_T *handle, unsigned int i);
static void prod_Req_11(RIO_PL_DS_T *handle, unsigned int j);
static void prod_Req_2(RIO_PL_DS_T *handle, unsigned int j);
static void prod_Req_1(RIO_PL_DS_T *handle, unsigned int j);
static void prod_Req_5(RIO_PL_DS_T *handle, unsigned int j);
static void prod_Req_6(RIO_PL_DS_T *handle, unsigned int j);

/* GDA: Bug#124 - Support for Data Streaming packet */
static void prod_Req_9(RIO_PL_DS_T *handle, unsigned int j);  
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
static void prod_Req_7(RIO_PL_DS_T *handle, unsigned int j);
/* GDA: Bug#125 */

/* queue management */
static void in_Queue_Shift(RIO_PL_DS_T *handle, unsigned int pos);
static unsigned int in_Queue_Add(RIO_PL_DS_T *handle, int prio, unsigned int index);
static void conv_To_Ext_Req(RIO_PL_DS_T *handle);
static void conv_To_Hook_Req(RIO_PL_DS_T *handle, unsigned int j);

/*packet check function*/
static int in_Check_Current(RIO_PL_DS_T *handle,
                            RIO_BOOL_T* error_Flag,
                            RIO_PACKET_VIOLATION_T *violation_Type);
/*check if ftype is correct in received packet*/
static unsigned int check_Ftype(RIO_PL_OUT_ARB_T *arbiter);
/*check if tt is correct in received packet*/
static unsigned int check_Tt(RIO_PL_OUT_ARB_T *arbiter);
/*check CRC*/
static unsigned int check_CRC(RIO_PL_DS_T * handle, RIO_PL_OUT_ARB_T *arbiter);
/*get header size depending of packet params*/
static unsigned int get_Header_Size(RIO_PL_DS_T * handle, RIO_PL_OUT_ARB_T *arbiter);
/*check header */
static unsigned int check_Header_Size(RIO_PL_DS_T * handle, RIO_PL_OUT_ARB_T *arbiter);
/*check if payload is aligned to dw size*/
static unsigned int check_Payload_Align(RIO_PL_DS_T * handle, RIO_PL_OUT_ARB_T *arbiter);
/*check if reserved fields are equal to zero*/
static unsigned int check_Reserved_Fields(RIO_PL_DS_T *handle, RIO_PL_OUT_ARB_T *arbiter);
/*check Ttype*/
static unsigned int check_Ttype(RIO_PL_OUT_ARB_T *arbiter);
/*check reserved field*/
static unsigned int check_Reserved_Field(RIO_PL_OUT_ARB_T *arbiter, unsigned int field_Pos_Start, unsigned int field_Size);
/*check if it is request*/
static RIO_BOOL_T is_Req(unsigned int ftype, unsigned int ttype);

/* GDA: Bug#124 - Support for Data Streaming packet */
static void RIO_In_Stream_To_Hw(RIO_HW_T *hws, RIO_UCHAR_T *stream, unsigned int size);
/* GDA: Bug#124 - Support for Data Streaming packet */

/***************************************************************************
 * Function : RIO_PL_In_Init
 *
 * Description: initialize the inbound buffer
 *
 * Returns:  none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_In_Init(
    RIO_PL_DS_T        *handle,
    RIO_PL_PARAM_SET_T *param_Set
    )
{
    unsigned int i, init_Prio = 3, count = 0; /* loop counter */

    if (handle == NULL || param_Set == NULL)
        return;

    /* clear buffer state */
    init_In_Temp(&RIO_IN_GET_TEMP(handle));

    /* 
     * initialize buffer with proper reservation for prio 
     * the constants mean only prio values itself
     */
    for (i = 0; i < handle->inst_Param_Set.pl_Inbound_Buffer_Size; i++, count++)
    {
        if (init_Prio == 3 && count >= RIO_IN_RES_FOR_3(handle))
        {
            init_Prio = 2;
            count = 0;
        }
        if (init_Prio == 2 && count >= RIO_IN_RES_FOR_2(handle))
        {
            init_Prio = 1;
            count = 0;
        }
        if (init_Prio == 1 && count >= RIO_IN_RES_FOR_1(handle))
            init_Prio = 0;

        init_Buffer_Entry(RIO_IN_GET_ENTRY(handle, i), init_Prio);

        /* initialize sending queue */
        RIO_IN_GET_QUEUE_ELEM(handle, i)->value = RIO_PL_QUEUE_ELEM_UNUSED;
        RIO_IN_GET_QUEUE_ELEM(handle, i)->index = 0;
    }

    /* initialize arbiter */
    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
    handle->inbound_Buf.req_Wait = RIO_FALSE;
}

/***************************************************************************
 * Function : init_In_Temp
 *
 * Description: initialize temporary
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_In_Temp(RIO_PL_IN_REQ_TO_LL_T *element)
{
    if (element == NULL)
        return;

    /* initialize header*/
    memset(&element->header, 0, sizeof(RIO_REQ_HEADER_T));
    /* initialize request */
    element->request.data = element->payload;
    element->request.dw_Num = 0;
    element->request.header = &element->header;
    element->request.packet_Type = 0;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    element->request.data_hw = element->payload_hw;
    element->request.hw_Num = 0;
    /* GDA: Bug#124 - Support for Data Streaming packet */

}

/***************************************************************************
 * Function : init_Buffer_Entry
 *
 * Description: initialize one inbound buffer entry
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Buffer_Entry(RIO_PL_IN_ENTRY_T *element, unsigned int prio)
{
    if (element == NULL)
        return;

    /* initialize general parameters */
    element->state = RIO_PL_IN_INVALID;
    element->payload_Size = 0;
    element->res_For_Prio = prio;

    /* initialize entry parameters */
    memset(&element->entry, '\0', sizeof(RIO_PL_OUTBOUND_RQ_T));
    
}

/***************************************************************************
 * Function : RIO_PL_In_Load_Data
 *
 * Description: next portion of data
 *
 * Returns:  8 - OK, else code specefied for packet not accepted
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_In_Load_Data(
    RIO_PL_DS_T *handle, 
    RIO_GRANULE_T *granule
    )
{
    unsigned int i; /* loop counter */
    static int count=0; /*GDA */
   

    /*GDA Included Bug:43  (Start) Status:Debug*/ 
   if(granule->granule_Type==RIO_GR_NEW_PACKET)
     {
       count=0;
     } 
    /*GDA Included Bug:43  (End)   Status:Debug*/ 


   if (handle == NULL || granule == NULL)
        return RIO_PL_PNACC_INTERNAL_ERROR; /* internal error */

    /* check if receiving payload does not overstepped the boundary */
    if (RIO_IN_GET_ARB(handle).cur_Pos > (RIO_PL_OUT_MAX_PACK_STREAM_LEN - RIO_PL_CNT_BYTE_IN_GRAN))
    {
         
        RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));  
        return RIO_PL_PNACC_GENERAL_ERROR; /* internal error */
    }

/*    printf("\nInstance No:%u   GDA_RX_Illegal_Packet:%u \n",
						handle->instance_Number,
						handle->inst_Param_Set.gda_Rx_Illegal_Packet);*/
   

    /*GDA: Bug#43 Fix*/ 
    if ((RIO_IN_GET_ARB(handle).cur_Pos >272)&&(!handle->inst_Param_Set.gda_Rx_Illegal_Packet))
       {
#ifndef _CARBON_SRIO_XTOR
         printf("\n<!Warning at Instance D`%u> Payload size exceeds more than 32 DWORD\n",
                                                                handle->instance_Number);
#endif
        
         RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle)); 
      
         return RIO_PL_PNACC_GENERAL_ERROR;
      }
    /*GDA: Bug#43 Fix*/
          


    /* add payload to the stream */
    for (i = 0; i < RIO_PL_CNT_BYTE_IN_GRAN; i++)
    {
        RIO_IN_GET_ARB(handle).packet_Stream[RIO_IN_GET_ARB(handle).cur_Pos + i] = 
            granule->granule_Date[i];
    }

    /* update arbiter state */
    RIO_IN_GET_ARB(handle).cur_Pos += RIO_PL_CNT_BYTE_IN_GRAN;

    return RIO_PL_PNACC_OK; 
}

/***************************************************************************
 * Function : RIO_PL_In_Check_Current
 *
 * Description: check receiving packet for error
 *
 * Returns: 8 - ok, else error code specefied in packet not accepted 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_In_Check_Current(RIO_PL_DS_T *handle)
{
    unsigned int ftype;
    RIO_BOOL_T error_Flag;
    RIO_PACKET_VIOLATION_T violation_Type;

    if (handle == NULL)
        return RIO_PL_PNACC_INTERNAL_ERROR; /*internal error*/

    /* check if packet isn't word aligned */
    if (RIO_IN_GET_ARB(handle).cur_Pos % RIO_PL_BYTE_CNT_IN_WORD)
    {
        RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
        return RIO_PL_PNACC_GENERAL_ERROR;
    }

    ftype = get_Ftype(&RIO_IN_GET_ARB(handle));
    if ( (handle->inbound_Buf.in_Buf_Param_Set.only_Maint_Requests == RIO_TRUE) &&
        (ftype != RIO_MAINTENANCE)  && (ftype != RIO_RESPONCE))
    {
        RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
        return RIO_PL_PNACC_INPUT_STOPPED;
    }

    /*check other packet errors*/
    if (in_Check_Current(handle, &error_Flag, &violation_Type) != RIO_ERROR)
        if (error_Flag == RIO_TRUE)
        {
            RIO_PL_Print_Message(handle, RIO_PL_WARNING_11, violation_Type);
            switch (violation_Type)
            {
                case CRC_ERROR:
                    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
                    return RIO_PL_PNACC_BAD_CRC;
                
                case FTYPE_RESERVED:
                    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
                    return RIO_PL_PNACC_GENERAL_ERROR;
                
                case TTYPE_RESERVED:
                    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
                    return RIO_PL_PNACC_GENERAL_ERROR;
                
                case TT_RESERVED:
                    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
                    return RIO_PL_PNACC_GENERAL_ERROR;
                
                case NON_ZERO_RESERVED_PACKET_BITS_VALUE:
                    return RIO_PL_PNACC_OK;
                
                case INCORRECT_PACKET_HEADER:
                    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
                    return RIO_PL_PNACC_GENERAL_ERROR;

                case PAYLOAD_NOT_DW_ALIGNED:
                    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
                    return RIO_PL_PNACC_GENERAL_ERROR;

                default:
                    break;
            }
        }
        
    return RIO_PL_PNACC_OK;
}
/***************************************************************************
 * Function : in_Check_Current
 *
 * Description: check receiving packet stream if it could be parsered
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int in_Check_Current(RIO_PL_DS_T *handle,
                            RIO_BOOL_T* error_Flag,
                            RIO_PACKET_VIOLATION_T *violation_Type)
{
    int check_Result;

    RIO_PL_OUT_ARB_T* arbiter;

    if (handle == NULL || violation_Type == NULL || error_Flag == NULL)
        return RIO_ERROR; /*internal error*/

    arbiter = &RIO_IN_GET_ARB(handle);

    /*check if packet is greater than 276 bytes*/
 /*   if (arbiter->cur_Pos > RIO_BC_PL_MAX_PROTOCOL_PACKET_LEN)
    {
        *error_Flag = RIO_BC_TRUE;
        *violation_Type = RIO_BC_PACKET_IS_LONGER_276;
        return RIO_BC_OK;
    }
*/

    if ((check_Result = check_Ftype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = FTYPE_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = check_Tt(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TT_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = check_CRC(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = CRC_ERROR;
            return RIO_OK;
    }

    if ((check_Result = check_Header_Size(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = INCORRECT_PACKET_HEADER;
            return RIO_OK;
    }

    if ((check_Result = check_Payload_Align(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = PAYLOAD_NOT_DW_ALIGNED;
            return RIO_OK;
    }
   
    if ((check_Result = check_Ttype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TTYPE_RESERVED;
            return RIO_OK;
    }
    
    /*check reserved field value*/
    if ((check_Result = check_Reserved_Fields(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = NON_ZERO_RESERVED_PACKET_BITS_VALUE;
            return RIO_OK;            
 
    }
    

    *error_Flag = RIO_FALSE;
    return RIO_OK;
}
/***************************************************************************
 * Function : RIO_PL_In_Commit_Current
 *
 * Description: commit current packet for passing to LL
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_In_Commit_Current(RIO_PL_DS_T *handle)
{
    unsigned int ftype, ttype, prio;

    if (handle == NULL)
        return;

    ftype = get_Ftype(&RIO_IN_GET_ARB(handle));
    ttype = get_Ttype(&RIO_IN_GET_ARB(handle));
    prio = get_Prio(&RIO_IN_GET_ARB(handle));

    if (handle->is_PLLight_Model)
    {
		RIO_PLLIGHT_PACKET_T packet;
	    	RIO_DW_T payload[RIO_PL_MAX_PAYLOAD_SIZE];
		/* GDA: Bug#124 - Support for Data Streaming packet */
	    	RIO_HW_T payload_hw[RIO_PL_MAX_DS_PAYLOAD_SIZE];  
		/* GDA: Bug#124 - Support for Data Streaming packet */

		unsigned int i;

		if (handle->is_Parallel_Model)
		{
			packet.s = (RIO_IN_GET_ARB(handle).packet_Stream[0] >> 7) & 1; /* for parallel mode only */
			packet.s_inv = (RIO_IN_GET_ARB(handle).packet_Stream[0] >> 2) & 1; /* for parallel mode only */
			packet.ack_ID = (RIO_IN_GET_ARB(handle).packet_Stream[0] >> 4) & 7;
			packet.rsrv_Phy_1 = (RIO_IN_GET_ARB(handle).packet_Stream[0] >> 3) & 1;
			packet.rsrv_Phy_2 = (RIO_IN_GET_ARB(handle).packet_Stream[0] >> 0) & 3; /* for parallel mode only */
		}
		else
		{
			packet.s = 0; 
			packet.s_inv = 0;
			packet.ack_ID = (RIO_IN_GET_ARB(handle).packet_Stream[0] >> 3) & 0x1F;
			packet.rsrv_Phy_1 = (RIO_IN_GET_ARB(handle).packet_Stream[0] >> 0) & 7;
			packet.rsrv_Phy_2 = 0;
		}

		packet.prio = get_Prio(&RIO_IN_GET_ARB(handle));
		packet.tt = get_Tt(&RIO_IN_GET_ARB(handle));
		packet.ftype = get_Ftype(&RIO_IN_GET_ARB(handle));
		packet.transport_Info = get_Transp_Info(&RIO_IN_GET_ARB(handle));
		packet.transport_Info_Size = get_Tt(&RIO_IN_GET_ARB(handle));

		packet.ttype = get_Ttype(&RIO_IN_GET_ARB(handle));

	    /* locate first unparsed byte */
		if (packet.tt == RIO_PL_TRANSP_16)
			i = RIO_PL_TTYPE_POS_LOW;
	    else
		    i = RIO_PL_TTYPE_POS_HIGH;

       /* if (packet.ftype != RIO_STREAM_WRITE)*/
	    /* GDA: Bug#124 - Support for Data Streaming packet */
        if (packet.ftype != RIO_STREAM_WRITE && packet.ftype != RIO_DS)
		{
			/* rdwr_Size parameter is used for all transactions except stream_write. 
			For doorbell transactions rdwr_Size parameter is used to fill doorbell_reserved field (lower bits).
			For response transactions rdwr_Size parameter is used to fill status field.*/
			packet.rdwr_Size = RIO_IN_GET_ARB(handle).packet_Stream[i++] & RIO_PL_SIZE_MASK;

			/* srtr_TID (source or target TID) parameter is used for all transactions except stream_write. 
			For message transactions srtr_TID parameter is used to fill letter-mbox-msgseg fields. */
			packet.srtr_TID = RIO_IN_GET_ARB(handle).packet_Stream[i++];
		}


		if (packet.ftype == RIO_INTERV_REQUEST)
		{
			/* gsm_Specific parameter is used only for intervention requests,
			these parameters set up fields sec_domain, secID and secTID 
			the size of all fields is 16 bits */
			packet.gsm_Specific = (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << 8;
			packet.gsm_Specific |= (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++];
		}
		
		if (packet.ftype == RIO_DOORBELL)
		{
			/* address parameter is used for all transactions except response, message.
			For doorbell transactions address parameter is used to fill doorbell_info field. */
			packet.address = (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << 8;
			packet.address |= (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++];
		} 
		else if ((packet.ftype != RIO_MESSAGE) && (packet.ftype != RIO_RESPONCE))
		{

			/* extended_Address parameter is used only for intervention requests, 
			non_intervention requests, writes, stream write requests depending on
			ext_addr_Size field */
			packet.extended_Address = 0;

			if ((handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE) &&
				(packet.ftype != RIO_MAINTENANCE))
			{
				if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_FALSE)
				{
					packet.extended_Address = (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << (RIO_PL_BITS_IN_BYTE * 3);
					packet.extended_Address |= (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << (RIO_PL_BITS_IN_BYTE * 2);
					packet.ext_addr_Size = RIO_EXT_ADDR_VALID_32;
				}
				else
					packet.ext_addr_Size = RIO_EXT_ADDR_VALID_16;

				packet.extended_Address |= (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << RIO_PL_BITS_IN_BYTE;
				packet.extended_Address |= (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++];
			}
			else
				packet.ext_addr_Size = RIO_EXT_ADDR_NOT_VALID;

			/* hop_Count parameter is used only for maintenance transactions */
			packet.hop_Count = RIO_IN_GET_ARB(handle).packet_Stream[i];

			/* address parameter is used for all transactions except response, message.
			For doorbell transactions address parameter is used to fill doorbell_info field.
			For maintenance transactions address parameter is used to fill config_offset field. */
			packet.address = (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << 21;
			packet.address |= (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << 13;
			packet.address |= (unsigned long)RIO_IN_GET_ARB(handle).packet_Stream[i++] << 5;

			/* address, wdptr, xambs */
			packet.address |= (RIO_IN_GET_ARB(handle).packet_Stream[i] & 0xf8) >> 3;

			/* address has been shifted previously*/
			packet.address <<= 3;

			/* for maintenance transactions clear the upper address bits
			hop_count parameter will contain this information */
			if (packet.ftype == RIO_MAINTENANCE)
			{
				packet.address &= 0x00FFFFFF;
			}

			/* wdptr parameter is used for all transactions where address field is filled.*/
			packet.wdptr = (RIO_IN_GET_ARB(handle).packet_Stream[i] & 0x04) >> 2;

			/* xamsbs parameter is used for all transactions where address field is filled. */
			packet.xamsbs = RIO_IN_GET_ARB(handle).packet_Stream[i++] & 0x03;
		}

		packet.crc_Valid_Setting = RIO_SET_BOTH_CRC_CORRECT;

		/* payload is used only for responses, messages, writes, stream writes */

		/* detect actual payload so that if it is present logical layer will report an error */
		if (RIO_IN_GET_ARB(handle).cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
		{
		   unsigned int k; /* loop counter */

		   if( RIO_IN_GET_ARB(handle).cur_Pos < (i + RIO_PL_BYTES_IN_HALF_WORD + RIO_PL_BYTES_IN_HALF_WORD) )
			{
			packet.dw_Size = 0;
			/* GDA: Bug#124 - Support for Data Streaming packet */
			packet.hw_Size = 0; 
			/* GDA: Bug#124 - Support for Data Streaming packet */
			}
		   else
		   {
			/* GDA: Bug#124 - Support for Data Streaming packet added below IF-ELSE Condition */
			   
	     		if (packet.ftype != RIO_DS)
			{
			packet.dw_Size = 
			(RIO_IN_GET_ARB(handle).cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) / 	RIO_PL_BYTE_CNT_IN_DWORD;
			}
			else
			{
			packet.hw_Size = 
			(RIO_IN_GET_ARB(handle).cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) / 	RIO_PL_BYTE_CNT_IN_HALF_WORD;
			}
			for (k = RIO_PL_INT_CRC_START; k < (RIO_IN_GET_ARB(handle).cur_Pos - RIO_PL_BYTES_IN_HALF_WORD); k++)
			    RIO_IN_GET_ARB(handle).packet_Stream[k] = RIO_IN_GET_ARB(handle).packet_Stream[k + RIO_PL_BYTES_IN_HALF_WORD];
		   }
		}
		else if ( RIO_IN_GET_ARB(handle).cur_Pos >= (i + RIO_PL_BYTES_IN_HALF_WORD) )
		{
			/* GDA: Bug#124 - Support for Data Streaming packet added below IF-ELSE Condition */
        	        if (packet.ftype != RIO_DS)
		        packet.dw_Size = (RIO_IN_GET_ARB(handle).cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD;
		        else
		        packet.hw_Size = (RIO_IN_GET_ARB(handle).cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_HALF_WORD;
		}
		else
		{
		     packet.dw_Size = 0;
		     /* GDA: Bug#124 - Support for Data Streaming packet */
		     packet.hw_Size = 0;
		     /* GDA: Bug#124 - Support for Data Streaming packet */
		}
		
		/* GDA: Bug#124 - Support for Data Streaming packet added below IF-ELSE Condition */
        	if (packet.ftype != RIO_DS)
		{
		RIO_In_Stream_To_Dw(payload,RIO_IN_GET_ARB(handle).packet_Stream + i,packet.dw_Size);
		packet.data = payload;
		}
		else		
		{
		RIO_In_Stream_To_Hw(payload_hw,RIO_IN_GET_ARB(handle).packet_Stream + i,packet.hw_Size);
		packet.data_hw = payload_hw;
		}

		/* invoke callback */
		handle->ext_Interfaces.rio_PLLight_Packet_Received(
			handle->ext_Interfaces.pl_Interface_Context,
			RIO_IN_GET_ARB(handle).packet_Stream,
			RIO_IN_GET_ARB(handle).cur_Pos,
			&packet);

	    RIO_PL_Set_State(handle, RIO_OUR_BUF_STATUS, RIO_PL_In_Buf_Status(handle), 0);
            RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));

	    /*GDA: Bug#43 Fix*/ 
            if ((packet.dw_Size>32) && (handle->inst_Param_Set.gda_Rx_Illegal_Packet))
            {
#ifndef _CARBON_SRIO_XTOR
                   printf("\n<!Warning at Instance D`%u> Payload size exceeds more than 32 DWORD\n",
                                                                         handle->instance_Number);
#endif
            }

	  #ifndef POFF
	    /* GDA - capture our own data*/ 
#ifndef _CARBON_SRIO_XTOR
            printf("\nFrom BFM:PL Layer(PLL) (Instance No:D`%u)",handle->instance_Number);
#endif
	/* GDA: Bug#124 - Support for Data Streaming packet added below IF-ELSE Condition */
        if (packet.ftype != RIO_DS)
	{    
            for(i=0; i < packet.dw_Size; i++){
#ifndef _CARBON_SRIO_XTOR
               printf("\n\t[%d]LS_Word: D`%d;   \tMS_Word: D`%d", i ,payload[i].ls_Word, \
							      payload[i].ms_Word);
#endif
	       }
#ifndef _CARBON_SRIO_XTOR
	    printf("\n");
#endif
	}
	else
	{
           for(i=0; i < packet.hw_Size; i++){
#ifndef _CARBON_SRIO_XTOR
               printf("\n\t[%d]Datastreaming data is D`%d", i ,payload_hw[i]);
#endif
	   }
#ifndef _CARBON_SRIO_XTOR
	    printf("\n");
#endif
	}  
	    /* GDA - capture our own data */
	  #endif

        return;
    }


	if (RIO_PL_Is_Req(ftype, ttype) == RIO_OK)
    {
        unsigned int i, i_Max; /* loop counter and limit value */

        /* look through the buffer for available place */
        i_Max = RIO_PL_In_Get_Buf_Size(handle);
        for (i = 0; i < i_Max; i++)
        {
            /* try to load */
            if (RIO_IN_GET_ENTRY(handle, i)->state == RIO_PL_IN_INVALID &&
                prio >= RIO_IN_GET_ENTRY(handle, i)->res_For_Prio)
            {
                RIO_PL_In_Load_From_Stream(handle, i);
                break;
            }
        }
    }
    else
    {
        /* here structures to pass to the environment */
        RIO_RESPONSE_T ext_Resp;
        RIO_DW_T       payload[RIO_PL_MAX_PAYLOAD_SIZE];
        ext_Resp.data = payload;
	  
        if (RIO_PL_In_Form_Resp(handle, &ext_Resp) == RIO_OK)
        {
            RIO_PL_Out_Rec_Resp(handle, &ext_Resp);
            /*call hook callback*/
            if (handle->ext_Interfaces.rio_Response_Received != NULL)
            handle->ext_Interfaces.rio_Response_Received(
                handle->ext_Interfaces.rio_Hooks_Context,
                &ext_Resp);
        }
    }

   
    RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));
}

/***************************************************************************
 * Function : RIO_PL_In_Allocate_Entry
 *
 * Description: allocate an entry for new packet based on packet prio
 *
 * Returns: error 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_In_Alloc_Entry(RIO_PL_DS_T   *handle, unsigned int  prio)
{
    if (handle == NULL || prio > RIO_PL_MAX_PRIO_VALUE)
        return RIO_ERROR;

    if (handle->is_PLLight_Model)
        return RIO_OK;

    /* check if we can load packet to inbound buffer */
    if (RIO_PL_In_Get_Buf_Free(handle, prio) > 0)
        return RIO_OK;
    else 
        return RIO_ERROR;
}

/***************************************************************************
 * Function : RIO_PL_In_Buf_Status
 *
 * Description: return status of inbound buffer
 *
 * Returns: number of free entry
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_In_Buf_Status(RIO_PL_DS_T *handle)
{
    if (handle == NULL)
        return 0;

    return RIO_PL_In_Get_Buf_Free(handle, RIO_PL_MAX_PRIO_VALUE);
}

/***************************************************************************
 * Function : RIO_PL_In_Get_Buf_Size
 *
 * Description: return size of inbound buffer
 *
 * Returns: number
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_PL_In_Get_Buf_Size(RIO_PL_DS_T *handle)
{
    if (handle == NULL)
        return 0;
    else 
        return handle->inst_Param_Set.pl_Inbound_Buffer_Size;
}

/***************************************************************************
 * Function : RIO_PL_In_Get_Buf_Free
 *
 * Description: return count of free entries in the buffer
 *
 * Returns: number
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_PL_In_Get_Buf_Free(RIO_PL_DS_T *handle, unsigned int value)
{
    unsigned int i, i_Max, count = 0; /* loop */
    static int congestion_count=1;

    if (handle == NULL)
        return 0;
    /* count the number of available entries in inbound buffer */
    i_Max = RIO_PL_In_Get_Buf_Size(handle);
    for (i = 0; i < i_Max; i++)
        if (value >= RIO_IN_GET_ENTRY(handle, i)->res_For_Prio &&
            RIO_IN_GET_ENTRY(handle, i)->state == RIO_PL_IN_INVALID)
            count++;

 /* GDA: Bug#125 - Support for Flow control packet */
       
 if(handle->instance_Number==2)
 { 
  	 if(count <= RIO_CONGESTION_BUF_SIZE)
   	{
	   	if(congestion_count==1)
	   	{
			   congestion_count++;
#ifndef _CARBON_SRIO_XTOR
		   	   printf("\n calling fc packet from RIO_PL_In_Get_Buf_Free \n");
#endif
	                    handle->ext_Interfaces.rio_PL_FC_To_Send(
        		   	  handle->ext_Interfaces.rio_Hooks_Context,
			     	  count
		 	 );
	   	}
	   	else congestion_count++;
   	}
	   else if(congestion_count>1)
	   {
		   congestion_count=1;
#ifndef _CARBON_SRIO_XTOR
		   printf("\n calling fc packet from RIO_PL_In_Get_Buf_Free \n");
#endif
	           handle->ext_Interfaces.rio_PL_FC_To_Send(
        	     	handle->ext_Interfaces.rio_Hooks_Context,
	      		count
		  );
   }
 }
/* GDA: Bug#125 - Support for Flow control packet */
 


    return count;
}

/***************************************************************************
 * Function : in_Queue_Add
 *
 * Description: add element to receiving queue
 *
 * Returns: 0 - insert in the top, else not
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int in_Queue_Add(RIO_PL_DS_T *handle, int prio, unsigned int index)
{
    unsigned int i, i_Max; /* loop  counter and limit value */

    if (handle == NULL)
        return 1;

    /* size of inbound queue */
    i_Max = RIO_PL_In_Get_Buf_Size(handle);

    /* check the sort of insertion by prio or by time */
    if (handle->parameters_Set.pass_By_Prio == RIO_TRUE)
    {
        for (i = 0; i < i_Max; i++)
        {
            if (prio > RIO_IN_GET_QUEUE_ELEM(handle, i)->value)
            {
                /* check if we need shift other elements*/
                if (RIO_IN_GET_QUEUE_ELEM(handle, i)->value != RIO_PL_QUEUE_ELEM_UNUSED)
                {
                    /* temporary */
                    RIO_PL_QUEUE_ELEM_T temp, temp2;
                    unsigned int j;
                    
                    temp = *RIO_IN_GET_QUEUE_ELEM(handle, i);
                    RIO_IN_GET_QUEUE_ELEM(handle, i)->value = prio;
                    RIO_IN_GET_QUEUE_ELEM(handle, i)->index = index;
                    
                    for (j = i + 1; j < i_Max; j++)
                    {
                        temp2 = *RIO_IN_GET_QUEUE_ELEM(handle, j);
                        *RIO_IN_GET_QUEUE_ELEM(handle, j) = temp;
                        temp = temp2;
                    }
                }
                else
                {
                    RIO_IN_GET_QUEUE_ELEM(handle, i)->value = prio;
                    RIO_IN_GET_QUEUE_ELEM(handle, i)->index = index;
                }
                return i;
            }
        }
    } /* pass by prio */
    else
    {
        for (i = 0; i < i_Max; i++)
        {
            if (RIO_IN_GET_QUEUE_ELEM(handle, i)->value == RIO_PL_QUEUE_ELEM_UNUSED)
            {
                RIO_IN_GET_QUEUE_ELEM(handle, i)->value = prio;
                RIO_IN_GET_QUEUE_ELEM(handle, i)->index = index;
                return i;
            }
        }
    }

    return 1;
}

/***************************************************************************
 * Function : in_Queue_Shift
 *
 * Description: shift queue to the left 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void in_Queue_Shift(RIO_PL_DS_T *handle, unsigned int pos)
{
    unsigned int i, i_Max; /* loop  counter and limit value */

    if (handle == NULL)
        return;

    i_Max = RIO_PL_In_Get_Buf_Size(handle) - 1;

    /* shift queue */
    for (i = pos; i < i_Max; i++)
        *RIO_IN_GET_QUEUE_ELEM(handle, i) = *RIO_IN_GET_QUEUE_ELEM(handle, i + 1);

    /* clear the last element */
    RIO_IN_GET_QUEUE_ELEM(handle, i)->value = RIO_PL_QUEUE_ELEM_UNUSED;
    RIO_IN_GET_QUEUE_ELEM(handle, i)->index = 0;
}

/***************************************************************************
 * Function : get_CRC_Start
 *
 * Description: returns the start position of the end CRC in the stream 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_CRC_Start(RIO_PL_DS_T *handle)
{
    unsigned int ttype, ftype, tt;
    RIO_PL_OUT_ARB_T *arbiter;

    if (handle == NULL)
        return 1;

    /* get inbound arbiter */
    arbiter = &handle->inbound_Arb;

    ftype = get_Ftype(arbiter);
    ttype = get_Ttype(arbiter);
    tt = get_Tt(arbiter);

    /*
     * this logic depends on: packet size, existion extended address field
     * and its length, packet type. All used hardcore numbers are unique for
     * each case (each packet types and other conditions). In case of changing 
     * packet format make all changing here.
     */
    switch (ftype)
    {
        /* maintanence */
        case RIO_MAINTENANCE:
            if (tt == RIO_PL_TRANSP_16)
                return arbiter->cur_Pos - 2;
            else
                return arbiter->cur_Pos - 4;

        /* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	    if (tt == RIO_PL_TRANSP_16)
	        return arbiter->cur_Pos - 2;
            else
                return arbiter->cur_Pos - 2;
	/* GDA: Bug#124 - Support for Data Streaming packet */
	    
	/* GDA: Bug#125 - Support for Flow control packet */ 
	case RIO_FC:
            if (tt == RIO_PL_TRANSP_16)
		return arbiter->cur_Pos - 4;
	    else
		return arbiter->cur_Pos - 2;
	/* GDA: Bug#125 */ 


        /* doorbell request */
        case RIO_DOORBELL:
            if (tt == RIO_PL_TRANSP_16)
                return arbiter->cur_Pos - 4;
            else
                return arbiter->cur_Pos - 2;
        
        /* response packet */
        case RIO_RESPONCE:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 4;
                else
                    return arbiter->cur_Pos - 2;
            }
            else
            {
                if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 2;
                else
                    return arbiter->cur_Pos - 4;
            }

        /* message request */
        case RIO_MESSAGE:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 4;
                else
                    return arbiter->cur_Pos - 2;
            }
            else
            {
                if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 2;
                else 
                    return arbiter->cur_Pos - 4;
            }

        /* nonintervention request */
        case RIO_NONINTERV_REQUEST:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 4;
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                    return arbiter->cur_Pos - 2;
            }
            else
            {
                if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 2;
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                    return arbiter->cur_Pos - 4;
            }

        /* write class */
        case RIO_WRITE:
            if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
            }
            else
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
            }

        /* stream write */
        case RIO_STREAM_WRITE:
            if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
            }
            else
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                    {
                        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
            }

            /* intervention request */
        case RIO_INTERV_REQUEST:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 2;
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                    return arbiter->cur_Pos - 4;
            }
            else
            {
                if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
                {
                    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 4;
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                    return arbiter->cur_Pos - 2;
            }

        /* other not-implemented types */
        default :
            return 1;
    }
}

/***************************************************************************
 * Function : get_Ftype
 *
 * Description: detect FTYPE
 *
 * Returns: FTYPE
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Ftype(RIO_PL_OUT_ARB_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return arbiter->packet_Stream[RIO_PL_FTYPE_POS] & RIO_PL_FTYPE_MASK;

}

/***************************************************************************
 * Function : get_Ttype
 *
 * Description: detect TTYPE field
 *
 * Returns: ttype
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Ttype(RIO_PL_OUT_ARB_T *arbiter)
{
    unsigned int tt;

    if (arbiter == NULL)
        return 0;

    tt = get_Tt(arbiter);
    if (tt == RIO_PL_TRANSP_16)
        return (arbiter->packet_Stream[RIO_PL_TTYPE_POS_LOW] & RIO_PL_TTYPE_MASK) >> RIO_PL_TTYPE_SHIFT;
    else
        return (arbiter->packet_Stream[RIO_PL_TTYPE_POS_HIGH] & RIO_PL_TTYPE_MASK) >> RIO_PL_TTYPE_SHIFT;
}

/***************************************************************************
 * Function : get_Tt
 *
 * Description: detect TT field
 *
 * Returns: TT
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Tt(RIO_PL_OUT_ARB_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return (arbiter->packet_Stream[RIO_PL_TT_POS] & RIO_PL_TT_MASK) >> RIO_PL_TT_SHIFT;
}

/***************************************************************************
 * Function : RIO_PL_In_Form_Resp
 *
 * Description: form response structure
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_In_Form_Resp(
    RIO_PL_DS_T    *handle, 
    RIO_RESPONSE_T *ext_Resp)
{
    unsigned int ftype, ttype; 
    RIO_PL_OUT_ARB_T  *arbiter;
    unsigned int i;


    if (handle == NULL || ext_Resp == NULL || ext_Resp->data == NULL)
        return RIO_ERROR;

    /* form response structure */
    arbiter = &RIO_IN_GET_ARB(handle);

    ext_Resp->transp_Type = get_Tt(arbiter);
    if (ext_Resp->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_PL_TTYPE_POS_LOW;
    else
        i = RIO_PL_TTYPE_POS_HIGH;

    /* form fields */
    ext_Resp->tr_Info = get_Transp_Info(arbiter);
    ext_Resp->ftype = (RIO_UCHAR_T)(ftype = get_Ftype(arbiter));
    ext_Resp->transaction = (RIO_UCHAR_T)(ttype = get_Ttype(arbiter));
    ext_Resp->prio = (RIO_UCHAR_T)get_Prio(arbiter);
    ext_Resp->status = (RIO_UCHAR_T)(arbiter->packet_Stream[i] & RIO_PL_STATUS_MASK);
    ext_Resp->target_TID = arbiter->packet_Stream[++i];
    ext_Resp->sec_ID = 0;
    ext_Resp->sec_TID = 0;
    /*src_Id field calculation*/
    ext_Resp->src_ID = ext_Resp->transp_Type == RIO_PL_TRANSP_16 ?
        ext_Resp->tr_Info & RIO_PL_8_BIT_DEV_ID_MASK : ext_Resp->tr_Info & RIO_PL_16_BIT_DEV_ID_MASK;

    /* 
     * different responses proceding depends on response packet format. All hardcore numbers defines
     * offsets some fields in certain type of packet and are unique. If some packets 
     * formats are changed - change code here.
     */
    switch(ftype)
    {
        case RIO_MAINTENANCE:
            if (RIO_PL_Is_Payload(ftype, ttype) == RIO_OK)
            {
                i += 5;
                /* count payload size */
                ext_Resp->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD);
                
                /* convert the stream to payload */
                RIO_In_Stream_To_Dw(ext_Resp->data, arbiter->packet_Stream + i, ext_Resp->dw_Num);
            }
            else
                ext_Resp->dw_Num = 0;
            break;

        case RIO_RESPONCE:
            if (RIO_PL_Is_Payload(ftype, ttype) == RIO_OK)
            {
                i++;

                /* there is embedded CRC */
                if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
                {
                    unsigned int j; 

                    for (j = RIO_PL_INT_CRC_START; j < arbiter->cur_Pos - RIO_PL_BYTES_IN_HALF_WORD; j++)
                        arbiter->packet_Stream[j] = arbiter->packet_Stream[j + 2];
                    arbiter->cur_Pos -= RIO_PL_BYTES_IN_HALF_WORD;
                }
                /* count payload size */
                ext_Resp->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD);
                
                /* convert the stream to payload */
                RIO_In_Stream_To_Dw(ext_Resp->data, arbiter->packet_Stream + i, ext_Resp->dw_Num);
            }
            else
            {
                ext_Resp->dw_Num = 0;
                ext_Resp->data = NULL;
            }
            break;

        default:
            return RIO_ERROR;

    }
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_In_Load_From_Stream
 *
 * Description: load inbound buffer entry from the packet stream
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_In_Load_From_Stream(RIO_PL_DS_T *handle, unsigned int i)
{
    RIO_PL_IN_ENTRY_T *entry;

    if (handle == NULL)
        return;

    /* locate entry */
    entry = RIO_IN_GET_ENTRY(handle, i);

    /* form entry fields */
    entry->entry.ftype = get_Ftype(&RIO_IN_GET_ARB(handle));
    entry->entry.prio = get_Prio(&RIO_IN_GET_ARB(handle));
    entry->entry.ttype = get_Ttype(&RIO_IN_GET_ARB(handle));
    entry->entry.tranp_Info = get_Transp_Info(&RIO_IN_GET_ARB(handle));
    entry->entry.tt = get_Tt(&RIO_IN_GET_ARB(handle));

    /* parse the stream to the structure */
    switch(entry->entry.ftype)
    {
        case RIO_MAINTENANCE:
            prod_Req_8(handle, i);
            break;

        case RIO_DOORBELL:
            prod_Req_10(handle, i);
            break;

        case RIO_MESSAGE:
            prod_Req_11(handle, i);
            break;

        case RIO_NONINTERV_REQUEST:
            prod_Req_2(handle, i);
            break;

        case RIO_WRITE:
            prod_Req_5(handle, i);
            break;

        case RIO_STREAM_WRITE:
            prod_Req_6(handle, i);
            break;

        case RIO_INTERV_REQUEST:
            prod_Req_1(handle, i);
            break;

    /* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
           prod_Req_9(handle, i);
           break;		
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
	case RIO_FC:
	    prod_Req_7(handle, i);
	    break;
    /* GDA: Bug#125 */

	    
    }

    /* change state of entry*/
    entry->state = RIO_PL_IN_READY;

    conv_To_Hook_Req(handle, i);
     /* load to receiving queue and notify logical layer */
    if (!in_Queue_Add(handle, entry->entry.prio, i))
    {
        conv_To_Ext_Req(handle);
    }
 
    /*change buf_status*/
    RIO_PL_Set_State(handle, RIO_OUR_BUF_STATUS, RIO_PL_In_Buf_Status(handle), 0);
}

/***************************************************************************
 * Function : get_Prio
 *
 * Description: detect prio field
 *
 * Returns: prio
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Prio(RIO_PL_OUT_ARB_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return (arbiter->packet_Stream[RIO_PL_PRIO_POS] & RIO_PL_PRIO_MASK) >> RIO_PL_PRIO_SHIFT;
}

/***************************************************************************
 * Function : get_Transp_Info
 *
 * Description: detect transport info field
 *
 * Returns: transport info
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned long get_Transp_Info(RIO_PL_OUT_ARB_T *arbiter)
{
    unsigned int tt; /* size of transport information */

    if (arbiter == NULL)
        return 0;

    /*
     * this code obtaint transport info from the packet. It does it in 
     * according with the specification. And all hardcore numbers are unique.
     * Change code here if specification changes.
     */
    if ((tt = get_Tt(arbiter)) == RIO_PL_TRANSP_16)
        return (((unsigned long)arbiter->packet_Stream[RIO_PL_TRANSP_TYPE_POS]) << RIO_PL_BITS_IN_BYTE) |
            ((unsigned long)arbiter->packet_Stream[RIO_PL_TRANSP_TYPE_POS + 1]);
    else
        return (((unsigned long)arbiter->packet_Stream[RIO_PL_TRANSP_TYPE_POS]) << (RIO_PL_BITS_IN_BYTE * 3)) |
            (((unsigned long)arbiter->packet_Stream[RIO_PL_TRANSP_TYPE_POS + 1]) << (RIO_PL_BITS_IN_BYTE * 2)) |
            (((unsigned long)arbiter->packet_Stream[RIO_PL_TRANSP_TYPE_POS + 2]) << RIO_PL_BITS_IN_BYTE) |
            ((unsigned long)arbiter->packet_Stream[RIO_PL_TRANSP_TYPE_POS + 3]);
}

/***************************************************************************
 * Function : prod_Req_8
 *
 * Description: parse maintenance request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_8(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (handle == NULL)
        return;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = RIO_PL_TTYPE_POS_LOW;
    else
        i = RIO_PL_TTYPE_POS_HIGH;

    /* complete fields */
    entry->entry.size = arbiter->packet_Stream[i] & RIO_PL_SIZE_MASK;
    entry->entry.src_TID = arbiter->packet_Stream[++i];
    entry->entry.hop_Count = arbiter->packet_Stream[++i];
    entry->entry.config_Off = ((unsigned long)arbiter->packet_Stream[++i]) << 13;
    entry->entry.config_Off |= ((unsigned long)arbiter->packet_Stream[++i]) << 5;
    entry->entry.config_Off |= ((unsigned long)arbiter->packet_Stream[++i] & 0xf8) >> 3;
    entry->entry.wdptr = ((unsigned long)arbiter->packet_Stream[i++] & 0x04) >> 2;

    /* shift offset again */
    entry->entry.config_Off <<= 3;
    
    /* check data payload */
    /* (RIO_PL_Is_Payload(entry->entry.ftype, entry->entry.ttype) == RIO_OK) */
    if (arbiter->cur_Pos >= (i + 2))
    {
        /* count payload size */
        entry->payload_Size = (arbiter->cur_Pos - i - 2) / RIO_PL_BYTE_CNT_IN_DWORD;

        /* convert the stream to payload */
        RIO_In_Stream_To_Dw(entry->payload, arbiter->packet_Stream + i, entry->payload_Size);
    }
    else
        entry->payload_Size = 0;
}

/***************************************************************************
 * Function : prod_Req_11
 *
 * Description: parse message request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_11(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    if (handle == NULL)
        return;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = RIO_PL_TTYPE_POS_LOW;
    else
        i = RIO_PL_TTYPE_POS_HIGH;

    /* complete fields */
    entry->entry.ttype = 0;
    entry->entry.size = arbiter->packet_Stream[i] & 0x0f;
    entry->entry.msglen = (arbiter->packet_Stream[i] & 0xf0) >> 4;

    /* letter, mbox & msgseg */
    entry->entry.letter = (arbiter->packet_Stream[++i] & 0xc0) >> 6;
    entry->entry.mbox = (arbiter->packet_Stream[i] & 0x30) >> 4;
    entry->entry.msgseg = arbiter->packet_Stream[i++] & 0x0f;

   
    /* payload is always */
    if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_PL_BYTES_IN_HALF_WORD + RIO_PL_BYTES_IN_HALF_WORD) )
        {
            entry->payload_Size = 
                (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) / 
                RIO_PL_BYTE_CNT_IN_DWORD;
            for (k = RIO_PL_INT_CRC_START; k < (arbiter->cur_Pos - RIO_PL_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_PL_BYTES_IN_HALF_WORD];
        }
        else
            entry->payload_Size = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_PL_BYTES_IN_HALF_WORD) )
    {
        entry->payload_Size = (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD;
    }
    else
        entry->payload_Size = 0;
    
    /* convert the stream to payload */
    RIO_In_Stream_To_Dw(entry->payload, arbiter->packet_Stream + i, entry->payload_Size);
}

/***************************************************************************
 * Function : prod_Req_10
 *
 * Description: parse doorbell request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_10(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (handle == NULL)
        return;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = 5;
    else
        i = 7;

    /* complete fields */
    entry->entry.ttype = 0;
    entry->entry.src_TID = arbiter->packet_Stream[i];
    entry->entry.doorbell_Info = ((unsigned int)arbiter->packet_Stream[++i]) << RIO_PL_BITS_IN_BYTE;
    entry->entry.doorbell_Info |= (unsigned int)arbiter->packet_Stream[++i];
}

/***************************************************************************
 * Function : RIO_In_Stream_To_Dw
 *
 * Description: convert bytes to dws
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_In_Stream_To_Dw(RIO_DW_T *dws, RIO_UCHAR_T *stream, unsigned int size)
{
    unsigned int i; /* loop counter */

    if (stream == NULL)
        return;
    /* 
     * this code converts bytes stream to double-words stream
     * hardcore constants are use here aren't used in the same sence everywhere else
     */
    for (i = 0; i < size; i++)
    {
        dws[i].ms_Word = ((unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD] << (RIO_PL_BITS_IN_BYTE * 3));
        dws[i].ms_Word |= ((unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD + 1] << (RIO_PL_BITS_IN_BYTE * 2));
        dws[i].ms_Word |= ((unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD + 2] << RIO_PL_BITS_IN_BYTE);
        dws[i].ms_Word |= (unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD + 3];

        dws[i].ls_Word = ((unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD + 4] << (RIO_PL_BITS_IN_BYTE * 3));
        dws[i].ls_Word |= ((unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD + 5] << (RIO_PL_BITS_IN_BYTE * 2));
        dws[i].ls_Word |= ((unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD + 6] << RIO_PL_BITS_IN_BYTE);
        dws[i].ls_Word |= (unsigned long)stream[i * RIO_PL_BYTE_CNT_IN_DWORD + 7];
    }
}

/***************************************************************************
 * Function : conv_To_Ext_Req
 *
 * Description: convert queue heap to external request for the environment
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void conv_To_Ext_Req(RIO_PL_DS_T *handle)
{
    /* pointers to temp and target entry */
    RIO_PL_IN_ENTRY_T     *entry;
    RIO_PL_IN_REQ_TO_LL_T *tmp;

    unsigned int i; /* loop counter */

    if (handle == NULL)
        return;

    /* obtain pointers */
    entry = RIO_IN_GET_ENTRY(handle, handle->inbound_Buf.rec_Queue[0].index);
    tmp = &RIO_IN_GET_TEMP(handle);

    /* load payload*/ 
    if (entry->payload_Size > 0)
    {
	    /* GDA: Bug#124 - Support for Data Streaming packet added IF loop */
      if(entry->entry.ftype != RIO_DS)
      {
        for (i = 0; i < entry->payload_Size; i++)
            tmp->payload[i] = entry->payload[i];

        /* actual values and data */
        tmp->request.data = tmp->payload;
       }
       else
       {
           for (i = 0; i < entry->payload_Size; i++)
              tmp->payload_hw[i] = entry->payload_hw[i];
            /* actual values and data */
            tmp->request.data_hw = tmp->payload_hw;
        }
    }
    else
    {
        tmp->request.data = NULL;
        tmp->request.data_hw = NULL;
    }
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* load header */
    tmp->header.doorbell_Info = entry->entry.doorbell_Info;
    tmp->header.format_Type = (RIO_UCHAR_T)entry->entry.ftype;
    tmp->header.ttype = (RIO_UCHAR_T)entry->entry.ttype;
    tmp->header.prio = (RIO_UCHAR_T)entry->entry.prio;
    tmp->header.offset = entry->entry.config_Off;
    tmp->header.wdptr = (RIO_UCHAR_T)entry->entry.wdptr;
    tmp->header.target_TID = (RIO_UCHAR_T)entry->entry.src_TID;
    tmp->header.ssize = tmp->header.rdsize = (RIO_UCHAR_T)entry->entry.size;
    tmp->header.mbox = (RIO_UCHAR_T)entry->entry.mbox;
    tmp->header.msglen = (RIO_UCHAR_T)entry->entry.msglen;
    tmp->header.letter = (RIO_UCHAR_T)entry->entry.letter;
    tmp->header.msgseg = (RIO_UCHAR_T)entry->entry.msgseg;
    tmp->header.address = entry->entry.address;
    tmp->header.extended_Address = entry->entry.extended_Address;
    tmp->header.xamsbs = (RIO_UCHAR_T)entry->entry.xamsbs;
    tmp->header.sec_ID = (RIO_UCHAR_T)entry->entry.sec_ID;
    tmp->header.sec_TID = (RIO_UCHAR_T)entry->entry.sec_TID;
    tmp->header.src_ID =  (entry->entry.tt == RIO_PL_TRANSP_16) ?
        entry->entry.tranp_Info & RIO_PL_8_BIT_DEV_ID_MASK : entry->entry.tranp_Info & RIO_PL_16_BIT_DEV_ID_MASK;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    tmp->header.stream_ID.src_ID=tmp->header.src_ID;
    tmp->header.stream_ID.flow_ID=  tmp->header.target_TID;
    tmp->header.cos = entry->entry.cos;
    tmp->header.s_Ds = entry->entry.s_Ds;
    tmp->header.e_Ds = entry->entry.e_Ds;
    tmp->header.length = entry->entry.length;
    tmp->header.o = entry->entry.o;
    tmp->header.p = entry->entry.p;
    tmp->request.hw_Num = (RIO_UCHAR_T)entry->payload_Size;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    tmp->header.x_On_Off= entry->entry.x_On_Off;
    tmp->header.flow_ID = entry->entry.flow_ID;
    tmp->header.tgtdest_ID = entry->entry.tgtdest_ID;
    tmp->header.soc = entry->entry.soc; 
    /* GDA: Bug#125 */


    /* load request */
    tmp->request.dw_Num = (RIO_UCHAR_T)entry->payload_Size;
    tmp->request.packet_Type = (RIO_UCHAR_T)entry->entry.ftype;
    tmp->request.transp_Type = (entry->entry.tt == RIO_PL_TRANSP_16) ? RIO_PL_TRANSP_16 : RIO_PL_TRANSP_32;
    tmp->request.header = &tmp->header;

    /* invoke callback */
    handle->ext_Interfaces.rio_PL_Remote_Request(
        handle->ext_Interfaces.pl_Interface_Context,
        handle->inbound_Buf.rec_Queue[0].index,
        &tmp->request,
        entry->entry.tranp_Info);


}

/***************************************************************************
 * Function : RIO_PL_In_Ack_Ext_Req
 *
 * Description: notify about accepting external request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_In_Ack_Ext_Req(RIO_PL_DS_T *handle, RIO_TAG_T    tag)
{
    unsigned int i, i_Max; /*loop counter */
    
    if (handle == NULL)
        return;

    i_Max = RIO_PL_In_Get_Buf_Size(handle);
    for (i = 0; i < i_Max; i++)
    {
        /* find accepted request */
        if (tag == handle->inbound_Buf.rec_Queue[i].index)
        {
            /* delete queue entry */
            in_Queue_Shift(handle, i);

            /* clean tmp */
            init_In_Temp(&RIO_IN_GET_TEMP(handle));
            
            /* clean entry */
            init_Buffer_Entry(RIO_IN_GET_ENTRY(handle, tag), RIO_IN_GET_ENTRY(handle, tag)->res_For_Prio);
            
            /* check if we need notify again */
            if (handle->inbound_Buf.rec_Queue[0].value != RIO_PL_QUEUE_ELEM_UNUSED)
            {
                handle->inbound_Buf.req_Wait = RIO_TRUE;
/*                conv_To_Ext_Req(handle); */
            }
            break;
        }
    }
    /* change state in reg*/
    RIO_PL_Set_State(handle, RIO_OUR_BUF_STATUS, RIO_PL_In_Buf_Status(handle), 0);
    
    return;
        
}

/***************************************************************************
 * Function : RIO_PL_In_Not_About_Request
 *
 * Description: notify the environment about external request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_In_Not_About_Request(RIO_PL_DS_T * handle)
{
    if (handle == NULL)
        return;

    if (handle->inbound_Buf.req_Wait == RIO_TRUE)
    {
        handle->inbound_Buf.req_Wait = RIO_FALSE;
        conv_To_Ext_Req(handle);
    }
}

/***************************************************************************
 * Function : RIO_PL_Is_Payload
 *
 * Description: detect if packet is with payload
 *
 * Returns: RIO_OK - yes, RIO_ERROR - no
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Is_Payload(unsigned int ftype, unsigned int ttype)
{
    switch (ftype)
    {
        case RIO_MAINTENANCE:
            if (ttype == RIO_MAINTENANCE_CONF_WRITE ||
                ttype == RIO_MAINTENANCE_READ_RESPONCE ||
                ttype == RIO_MAINTENANCE_PORT_WRITE)
                return RIO_OK;
            else
                return RIO_ERROR;

        case RIO_DOORBELL:
            return RIO_ERROR;

        case RIO_RESPONCE:
            if (ttype == RIO_RESPONCE_WITH_DATA)
                return RIO_OK;
            else
                return RIO_ERROR;

        case RIO_MESSAGE:
            return RIO_OK;

        case RIO_STREAM_WRITE:
            return RIO_OK;

	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	    return RIO_OK;
	/* GDA: Bug#124 - Support for Data Streaming packet */

        /* GDA: Bug#125 - Support for Flow control packet */
        case RIO_FC:
            return RIO_ERROR;
        /* GDA: Bug#125 */

        case RIO_WRITE:
            /*if (ttype != RIO_PL_IO_ATOMIC_TSWAP)
                return RIO_OK;
            else 
                return RIO_ERROR; */
            return RIO_OK;

        default:
            return RIO_ERROR;
    }
}

/***************************************************************************
 * Function : prod_Req_5
 *
 * Description: parse write request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_5(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    /* 
     * the routine stripps packets of type 5 to structure fields
     * the offsets, masks, shift constants are unique for this type
     * of packets. Change them here in case of specification changing 
     */
    if (handle == NULL)
        return;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = RIO_PL_TTYPE_POS_LOW;
    else
        i = RIO_PL_TTYPE_POS_HIGH;

    /* complete fields */
    entry->entry.ttype = (arbiter->packet_Stream[i] & RIO_PL_TTYPE_MASK) >> RIO_PL_TTYPE_SHIFT;
    entry->entry.size = arbiter->packet_Stream[i] & RIO_PL_SIZE_MASK;

    /* src_TID */
    entry->entry.src_TID = arbiter->packet_Stream[++i];

    /* extended address */
    entry->entry.extended_Address = 0;

    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
    {
        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_FALSE)
        {
            entry->entry.extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 3);
            entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 2);
        }
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_PL_BITS_IN_BYTE;
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    entry->entry.address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    entry->entry.address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    entry->entry.address <<= 3;

    entry->entry.wdptr = (arbiter->packet_Stream[i] & 0x04) >> 2;
    entry->entry.xamsbs = arbiter->packet_Stream[i] & 0x03;
    i++;
   
    /* payload is always */
    if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos < (i + RIO_PL_BYTES_IN_HALF_WORD + RIO_PL_BYTES_IN_HALF_WORD) )
            entry->payload_Size = 0;
        else
        {
            entry->payload_Size = 
                (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) / 
                RIO_PL_BYTE_CNT_IN_DWORD;
            for (k = RIO_PL_INT_CRC_START; k < (arbiter->cur_Pos - RIO_PL_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_PL_BYTES_IN_HALF_WORD];
        }
    }
    else if ( arbiter->cur_Pos >= (i + RIO_PL_BYTES_IN_HALF_WORD) )
    {
        entry->payload_Size = (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD;
    }
    else
        entry->payload_Size = 0;
    
    /* convert the stream to payload */
    RIO_In_Stream_To_Dw(entry->payload, arbiter->packet_Stream + i, entry->payload_Size);
}

/***************************************************************************
 * Function : prod_Req_6
 *
 * Description: parse streaming write request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_6(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    /* 
     * the routine stripps packets of type 6 to structure fields
     * the offsets, masks, shift constants are unique for this type
     * of packets. Change them here in case of specification changing 
     */
    if (handle == NULL)
        return;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = 3;
    else
        i = 5;

    /* complete fields */
    entry->entry.ttype = 0;

    /* extended address */
    entry->entry.extended_Address = 0;

    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
    {
        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_FALSE)
        {
            entry->entry.extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 3);
            entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 2);
        }
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_PL_BITS_IN_BYTE;
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    entry->entry.address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    entry->entry.address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    entry->entry.address <<= 3;

    entry->entry.wdptr = (arbiter->packet_Stream[i] & 0x04) >> 2;
    entry->entry.xamsbs = arbiter->packet_Stream[i] & 0x03;
    i++;
   
    /* payload is always */
    
    if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */
        if (arbiter->cur_Pos >=    (i + RIO_PL_BYTES_IN_HALF_WORD + RIO_PL_BYTES_IN_HALF_WORD) )
        {
            entry->payload_Size = 
                (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) / 
                RIO_PL_BYTE_CNT_IN_DWORD;
            for (k = RIO_PL_INT_CRC_START; k < (arbiter->cur_Pos - RIO_PL_BYTES_IN_HALF_WORD); k++)
            arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_PL_BYTES_IN_HALF_WORD];
        }
        else entry->payload_Size = 0;
    }
    else
    {
        if (arbiter->cur_Pos >=    (i + RIO_PL_BYTES_IN_HALF_WORD) )
        {
            entry->payload_Size = (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD;
        }
        else entry->payload_Size = 0;
    }
    
    /* convert the stream to payload */
    RIO_In_Stream_To_Dw(entry->payload, arbiter->packet_Stream + i, entry->payload_Size);
}

/***************************************************************************
 * Function : prod_Req_2
 *
 * Description: parse noninterventing request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_2(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (handle == NULL)
        return;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = RIO_PL_TTYPE_POS_LOW;
    else
        i = RIO_PL_TTYPE_POS_HIGH;

    /* complete fields */
    entry->entry.ttype = (arbiter->packet_Stream[i] & RIO_PL_TTYPE_MASK) >> RIO_PL_TTYPE_SHIFT;
    entry->entry.size = arbiter->packet_Stream[i] & RIO_PL_SIZE_MASK;

    /* src_TID */
    entry->entry.src_TID = arbiter->packet_Stream[++i];

    /* extended address */
    entry->entry.extended_Address = 0;

    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
    {
        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_FALSE)
        {
            entry->entry.extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 3);
            entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 2);
        }
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_PL_BITS_IN_BYTE;
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    entry->entry.address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    entry->entry.address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    entry->entry.address <<= 3;

    entry->entry.wdptr = (arbiter->packet_Stream[i] & 0x04) >> 2;
    entry->entry.xamsbs = arbiter->packet_Stream[i] & 0x03;
   
    /* payload is never */
    /* entry->payload_Size = 0; */

    /* detect actual payload so that if it is present logical layer will report an error */
    if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos < (i + RIO_PL_BYTES_IN_HALF_WORD + RIO_PL_BYTES_IN_HALF_WORD) )
            entry->payload_Size = 0;
        else
        {
            entry->payload_Size = 
                (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) / 
                RIO_PL_BYTE_CNT_IN_DWORD;
            for (k = RIO_PL_INT_CRC_START; k < (arbiter->cur_Pos - RIO_PL_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_PL_BYTES_IN_HALF_WORD];
        }
    }
    else if ( arbiter->cur_Pos >= (i + RIO_PL_BYTES_IN_HALF_WORD) )
    {
        entry->payload_Size = (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD;
    }
    else
        entry->payload_Size = 0;


    RIO_In_Stream_To_Dw(entry->payload, arbiter->packet_Stream + i, entry->payload_Size);
}

/***************************************************************************
 * Function : prod_Req_1
 *
 * Description: parse interventing request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_1(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    /* 
     * the routine stripps packets of type 1 to structure fields
     * the offsets, masks, shift constants are unique for this type
     * of packets. Change them here in case of specification changing 
     */
    if (handle == NULL)
        return;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = RIO_PL_TTYPE_POS_LOW;
    else
        i = RIO_PL_TTYPE_POS_HIGH;

    /* complete fields */
    entry->entry.ttype = (arbiter->packet_Stream[i] & RIO_PL_TTYPE_MASK) >> RIO_PL_TTYPE_SHIFT;
    entry->entry.size = arbiter->packet_Stream[i] & RIO_PL_SIZE_MASK;

    /* src_TID */
    entry->entry.src_TID = arbiter->packet_Stream[++i];

    /* secondary ID*/
    entry->entry.sec_ID = arbiter->packet_Stream[++i];

    /* secondary TID*/
    entry->entry.sec_TID = arbiter->packet_Stream[++i];

    /* extended address */
    entry->entry.extended_Address = 0;

    if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid == RIO_TRUE)
    {
        if (handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 == RIO_FALSE)
        {
            entry->entry.extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 3);
            entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_PL_BITS_IN_BYTE * 2);
        }
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_PL_BITS_IN_BYTE;
        entry->entry.extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    entry->entry.address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    entry->entry.address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    entry->entry.address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    entry->entry.address <<= 3;

    entry->entry.wdptr = (arbiter->packet_Stream[i] & 0x04) >> 2;
    entry->entry.xamsbs = arbiter->packet_Stream[i] & 0x03;
   
    /* payload is never */
    /* entry->payload_Size = 0; */

    /* detect actual payload so that if it is present logical layer will report an error */
    if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos < (i + RIO_PL_BYTES_IN_HALF_WORD + RIO_PL_BYTES_IN_HALF_WORD) )
            entry->payload_Size = 0;
        else
        {
            entry->payload_Size = 
                (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) / 
                RIO_PL_BYTE_CNT_IN_DWORD;
            for (k = RIO_PL_INT_CRC_START; k < (arbiter->cur_Pos - RIO_PL_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_PL_BYTES_IN_HALF_WORD];
        }
    }
    else if ( arbiter->cur_Pos >= (i + RIO_PL_BYTES_IN_HALF_WORD) )
    {
        entry->payload_Size = (arbiter->cur_Pos - i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_DWORD;
    }
    else
        entry->payload_Size = 0;


    RIO_In_Stream_To_Dw(entry->payload, arbiter->packet_Stream + i, entry->payload_Size);

}
/***************************************************************************
 * Function : conv_To_Hook_Req
 *
 * Description: convert queue heap to external request for the environment
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void conv_To_Hook_Req(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointers to temp and target entry */
    RIO_PL_IN_ENTRY_T     *entry;
    RIO_PL_IN_REQ_TO_LL_T  hook_Request;
    unsigned int i; /* loop counter */

    if (handle == NULL)
        return;

    /* obtain pointers */
    entry = RIO_IN_GET_ENTRY(handle, j);   

    /* load payload*/
    if (entry->payload_Size > 0)
    {
	    /* GDA: Bug#124 - Support for Data Streaming packet added below IF Loop */
      if(entry->entry.ftype == RIO_DS)
      {
	for (i = 0; i < entry->payload_Size; i++)
            hook_Request.payload_hw[i] = entry->payload_hw[i];
        /* actual values and data */
        hook_Request.request.data_hw = hook_Request.payload_hw;
      }
      else
      {
	for (i = 0; i < entry->payload_Size; i++)
            hook_Request.payload[i] = entry->payload[i];
        /* actual values and data */
        hook_Request.request.data = hook_Request.payload;
      }
    }
    else
    {
        hook_Request.request.data = NULL;
        hook_Request.request.data_hw = NULL;
    }
	    /* GDA: Bug#124 - Support for Data Streaming packet */
	    
    /* load header */
    hook_Request.header.doorbell_Info = entry->entry.doorbell_Info;
    hook_Request.header.format_Type  = (RIO_UCHAR_T)entry->entry.ftype;
    hook_Request.header.ttype = (RIO_UCHAR_T)entry->entry.ttype;
    hook_Request.header.prio = (RIO_UCHAR_T)entry->entry.prio;
    hook_Request.header.offset = entry->entry.config_Off;
    hook_Request.header.wdptr = (RIO_UCHAR_T)entry->entry.wdptr;
    hook_Request.header.target_TID = (RIO_UCHAR_T)entry->entry.src_TID;
    hook_Request.header.ssize = hook_Request.header.rdsize = (RIO_UCHAR_T)entry->entry.size;
    hook_Request.header.mbox = (RIO_UCHAR_T)entry->entry.mbox;
    hook_Request.header.msglen = (RIO_UCHAR_T)entry->entry.msglen;
    hook_Request.header.letter = (RIO_UCHAR_T)entry->entry.letter;
    hook_Request.header.msgseg = (RIO_UCHAR_T)entry->entry.msgseg;
    hook_Request.header.address = entry->entry.address;
    hook_Request.header.extended_Address = entry->entry.extended_Address;
    hook_Request.header.xamsbs = (RIO_UCHAR_T)entry->entry.xamsbs;
    hook_Request.header.sec_ID = (RIO_UCHAR_T)entry->entry.sec_ID;
    hook_Request.header.sec_TID = (RIO_UCHAR_T)entry->entry.sec_TID;
    hook_Request.header.src_ID = (RIO_UCHAR_T)((entry->entry.tt == RIO_PL_TRANSP_16) ? (entry->entry.tranp_Info & 0xff) :
        (entry->entry.tranp_Info & 0xffff)); /*????*/

    /* GDA: Bug#124 - Support for Data Streaming packet */
    hook_Request.header.stream_ID.src_ID=hook_Request.header.src_ID;
    hook_Request.header.stream_ID.flow_ID=  hook_Request.header.target_TID;
    hook_Request.header.cos = entry->entry.cos;
    hook_Request.header.length = entry->entry.length;
    hook_Request.header.s_Ds = entry->entry.s_Ds;
    hook_Request.header.e_Ds = entry->entry.e_Ds;
    hook_Request.header.o = entry->entry.o;
    hook_Request.header.p = entry->entry.p;
    hook_Request.request.hw_Num = (RIO_UCHAR_T)entry->payload_Size;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    hook_Request.header.x_On_Off = entry->entry.x_On_Off;
    hook_Request.header.flow_ID = entry->entry.flow_ID;
    hook_Request.header.tgtdest_ID = entry->entry.tgtdest_ID;
    hook_Request.header.soc = entry->entry.soc;
    /* GDA: Bug#125 - Support for Flow control packet */


    /* load request */
    hook_Request.request.dw_Num = (RIO_UCHAR_T)entry->payload_Size;
    hook_Request.request.packet_Type = (RIO_UCHAR_T)entry->entry.ftype;
    hook_Request.request.transp_Type = (entry->entry.tt == RIO_PL_TRANSP_16) ? RIO_PL_TRANSP_16 : RIO_PL_TRANSP_32;
    hook_Request.request.header = &hook_Request.header;

    /*GDA Bug#43 Fix*/
     if ((RIO_IN_GET_ARB(handle).cur_Pos >272)&&(handle->inst_Param_Set.gda_Rx_Illegal_Packet))
      {
#ifndef _CARBON_SRIO_XTOR
         printf("\n<!Warning at Instance D`%u> Payload size exceeds more than 32 DWORD\n",
                                                                handle->instance_Number);
#endif
      }
    /*GDA Bug#43 Fix*/

  #ifndef POFF
#ifndef _CARBON_SRIO_XTOR
    /* GDA - capture our own data */ 
    printf("\nFrom BFM: PL Layer (Instance No:D`%u)",handle->instance_Number);
#endif
    
	/* GDA: Bug#124 - Support for Data Streaming packet added below IF-ELSE Condition */
     if(entry->entry.ftype == RIO_DS)
     {
        for(i=0; i < entry->payload_Size; i++){
#ifndef _CARBON_SRIO_XTOR
        printf("\n\t[%d]DataStreaming Data is : D`%d ",i, entry->payload_hw[i]);
#endif
        }
#ifndef _CARBON_SRIO_XTOR
        printf("\n\n");
#endif
     }
    else
    {	
        for(i=0; i < entry->payload_Size; i++){
#ifndef _CARBON_SRIO_XTOR
        printf("\n\t[%d]LS_Word: D`%d;   \tMS_Word: D`%d",i, entry->payload[i].ls_Word, \
						      entry->payload[i].ms_Word);
#endif
        }
#ifndef _CARBON_SRIO_XTOR
        printf("\n\n");
#endif
    }

    /* GDA - capture our own data */
  #endif

    /*invoke hook callback*/
    if (handle->ext_Interfaces.rio_Request_Received != NULL)
    handle->ext_Interfaces.rio_Request_Received(
        handle->ext_Interfaces.rio_Hooks_Context,
        &hook_Request.request);

}

/***************************************************************************
 * Function : check_Ftype
 *
 * Description: check if ftype is correct to reserved value
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Ftype(RIO_PL_OUT_ARB_T *arbiter)
{
    unsigned int ftype;

    if (arbiter == NULL)
        return RIO_ERROR;

    ftype = get_Ftype(arbiter); 

    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
        case RIO_NONINTERV_REQUEST:
        case RIO_WRITE:
        case RIO_STREAM_WRITE:
        case RIO_MAINTENANCE:
        case RIO_DOORBELL:
        case RIO_MESSAGE:
        case RIO_RESPONCE:
        /* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:  
	/* GDA: Bug#124 */

	/* GDA: Bug#125 - Support for Flow control packet */ 
        case RIO_FC:
	/* GDA: Bug#125  */ 

		
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
}
/***************************************************************************
 * Function : check_Tt
 *
 * Description: check if tt is correct to reserved values
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Tt(RIO_PL_OUT_ARB_T *arbiter)
{
    unsigned int tt;

    if (arbiter == NULL)
        return RIO_ERROR;

    tt = get_Tt(arbiter); 

    switch (tt)
    {
        case RIO_PL_TRANSP_16:
        case RIO_PL_TRANSP_32:
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
}
/***************************************************************************
 * Function : check_Ttype
 *
 * Description: check if ttype is correct to reserved value
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Ttype(RIO_PL_OUT_ARB_T *arbiter)
{
    unsigned int ftype, ttype;

    if (arbiter == NULL)
        return RIO_ERROR;

    ftype = get_Ftype(arbiter);
    ttype = get_Ttype(arbiter);

    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            if (ttype != RIO_INTERV_READ_OWNER &&
                ttype != RIO_INTERV_READ_TO_OWN_OWNER &&
                ttype != RIO_INTERV_IO_READ_OWNER)
                return RIO_ERROR;
            break;
        case RIO_NONINTERV_REQUEST:
            break;/*all transactions are allowed*/
        case RIO_WRITE:
            if (ttype != RIO_WRITE_CASTOUT &&
                ttype != RIO_WRITE_FLUSH_WITH_DATA &&
                ttype != RIO_WRITE_NWRITE &&
                ttype != RIO_WRITE_NWRITE_R &&
                ttype != RIO_WRITE_ATOMIC_TSWAP &&
		/* GDA: Added for Bug#133. Included support for TType C for Type 5 packet */
                ttype != RIO_WRITE_ATOMIC_SWAP )
                return RIO_ERROR;
            break;
        case RIO_STREAM_WRITE:
            break; /*there is no transaction field*/

	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	    break;  /*there is no transaction field*/
	/* GDA: Bug#124 - Support for Data Streaming packet */

        /* GDA: Bug#125 - Support for Flow control packet */
   	case RIO_FC:
            break; /*there is no transaction field*/
        /* GDA: Bug#125 */

        case RIO_MAINTENANCE:
            if (ttype != RIO_MAINTENANCE_CONF_READ &&
                ttype != RIO_MAINTENANCE_CONF_WRITE &&
                ttype != RIO_MAINTENANCE_READ_RESPONCE &&
                ttype != RIO_MAINTENANCE_WRITE_RESPONCE &&
                ttype != RIO_MAINTENANCE_PORT_WRITE)
                return RIO_ERROR;
            break;
        case RIO_DOORBELL:
        case RIO_MESSAGE:
            break;/*there is no transaction field*/
        case RIO_RESPONCE:
            if (ttype != RIO_RESPONCE_WO_DATA &&
                ttype != RIO_RESPONCE_MESSAGE &&
                ttype != RIO_RESPONCE_WITH_DATA)
                return RIO_ERROR;
            break;
        default:
            return RIO_ERROR;
    }
    return RIO_OK;
}
/***************************************************************************
 * Function : check_CRC
 *
 * Description: check if CRC is correct
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_CRC(RIO_PL_DS_T *handle, RIO_PL_OUT_ARB_T *arbiter)
{
    RIO_UCHAR_T first_Byte;
    unsigned short crc = RIO_PL_INIT_CRC_VALUE, src_Crc;
    unsigned int first_Crc_Pos, start_For_Cnt = 0;

    if (arbiter == NULL || handle == NULL)
        return RIO_ERROR;

    /* CRC checking*/
    first_Byte = arbiter->packet_Stream[0];
    arbiter->packet_Stream[0] = first_Byte & 0x03;

    /* check if CRC was countered twice */
    if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
    {
        src_Crc = arbiter->packet_Stream[RIO_PL_INT_CRC_START];
        src_Crc <<= RIO_PL_BITS_IN_BYTE;
        src_Crc |= (unsigned short)(arbiter->packet_Stream[RIO_PL_INT_CRC_START + 1]);

        start_For_Cnt = RIO_PL_INT_CRC_START;
	 /*GDA:Bug#139 remove the need to recompute crc */
        if (src_Crc != 0)
	{
        RIO_PL_Out_Compute_CRC(&crc, 
        arbiter->packet_Stream, RIO_PL_INT_CRC_START);

        if (src_Crc != crc)
        {
            /*set first byte value back*/
            arbiter->packet_Stream[0] = first_Byte;
            return RIO_ERROR;
        }
	 /*GDA:Bug#139 remove the need to recompute crc */
	}
    }
    /* here we check the next CRC */
    first_Crc_Pos = get_CRC_Start(handle);

    /* check if CRC is 16-bit aligned */
    if (first_Crc_Pos % RIO_PL_BYTES_IN_HALF_WORD)
        return RIO_ERROR;

    if (first_Crc_Pos <= start_For_Cnt)
    {
        /*set first byte value back*/
        arbiter->packet_Stream[0] = first_Byte;
        return RIO_ERROR;
    }

    /* count CRC */

    src_Crc = arbiter->packet_Stream[first_Crc_Pos];
    src_Crc <<= RIO_PL_BITS_IN_BYTE;
    src_Crc |= arbiter->packet_Stream[first_Crc_Pos + 1];

    if(src_Crc ==0)
#ifndef _CARBON_SRIO_XTOR
	    printf("\n check in rx : src crs ic set to zero \n");
#endif

  /*GDA:Bug#139 remove the need to recompute crc */
  if (src_Crc != 0)
  {
    RIO_PL_Out_Compute_CRC(&crc, 
        arbiter->packet_Stream + start_For_Cnt, 
        first_Crc_Pos - start_For_Cnt);
    /* check and return an error if incorrect*/
    if (src_Crc != crc)
    {
        /*set first byte value back*/
        arbiter->packet_Stream[0] = first_Byte;
        return RIO_ERROR;
    }
  /*GDA:Bug#139 remove the need to recompute crc */
  }
    /*set first byte value back*/
    arbiter->packet_Stream[0] = first_Byte;
    return RIO_OK;

}
/***************************************************************************
 * Function : get_Header_Size
 *
 * Description: calculate required packet header size depending on ftype,tt 
 *              and internal parameters value
 *
 * Returns: header size in bits   
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Header_Size(RIO_PL_DS_T * handle, RIO_PL_OUT_ARB_T *arbiter)
{
    /*temporary*/
    unsigned int tt, ftype;
    /*length of headers and required length of fields - in bits*/
    unsigned int required_Length, ext_Addr_Size, tr_Info_Size;
    /*unsigned long ext_Addr_Control;*/
    RIO_BOOL_T ext_Address_Valid, ext_Address_16;

    if (arbiter == NULL || handle == NULL)
        return 0;

    /*get extended address params*/
    

    ext_Address_Valid = handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid;
    ext_Address_16 = handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16;

    ftype = get_Ftype(arbiter);
    if ((tt = get_Tt(arbiter)) == RIO_PL_TRANSP_16)
        tr_Info_Size = RIO_PL_TR_INFO_SIZE_16; /*16 bit transport info is supposed*/
    else
        tr_Info_Size = RIO_PL_TR_INFO_SIZE_32; /*32 bit transport info is supposed*/

    if (ext_Address_Valid == RIO_TRUE)
    {
        if(ext_Address_16 == RIO_TRUE)
            ext_Addr_Size = RIO_PL_EXT_ADDR_SIZE_16; /*16 bit extended address is supposed*/
        else
            ext_Addr_Size = RIO_PL_EXT_ADDR_SIZE_32; /*32 bit extended address is supposed*/
    }
    else
        ext_Addr_Size = 0; /*no extended address is supposed*/
    
    /*necessary part of all packets*/
    required_Length = RIO_PL_FIRST_16_BIT_SIZE + tr_Info_Size;

    /*add size of packet depended headers*/
    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            required_Length += ext_Addr_Size + RIO_PL_PACK_1_HEADER_SIZE;
            break;

        case RIO_NONINTERV_REQUEST:
            required_Length += ext_Addr_Size + RIO_PL_PACK_2_HEADER_SIZE;
            break;
        case RIO_WRITE:
            required_Length += ext_Addr_Size + RIO_PL_PACK_5_HEADER_SIZE;
            break;
        case RIO_STREAM_WRITE:
            required_Length += ext_Addr_Size + RIO_PL_PACK_6_HEADER_SIZE;
            break;
        case RIO_MAINTENANCE:
            required_Length += RIO_PL_PACK_8_HEADER_SIZE;
            break;
        case RIO_DOORBELL:
            required_Length += RIO_PL_PACK_10_HEADER_SIZE;
            break;

        case RIO_MESSAGE:
            required_Length += RIO_PL_PACK_11_HEADER_SIZE;
            break;

        case RIO_RESPONCE:
            required_Length += RIO_PL_PACK_13_HEADER_SIZE;
            break;

        /* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	     required_Length += RIO_PL_PACK_9_HEADER_SIZE;
	     break;
	/* GDA: Bug#124 - Support for Data Streaming packet */

       /* GDA: Bug#125 - Support for Flow control packet */
        case RIO_FC:
            required_Length += RIO_PL_PACK_7_HEADER_SIZE;
            break;
       /* GDA: Bug#125  */

	    
        default:
            break; /*impossible*/
    }
    
    return required_Length;

}
/***************************************************************************
 * Function : check_Header_Size
 *
 * Description: check if header length is correct
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Header_Size(RIO_PL_DS_T * handle, RIO_PL_OUT_ARB_T *arbiter)
{
    /*length of headers*/
    unsigned int required_Length;
    if (arbiter == NULL || handle == NULL)
        return RIO_ERROR;

    /*get required header size in bits */
    required_Length = get_Header_Size(handle, arbiter) + RIO_PL_CRC_FIELD_SIZE;
    /*convert from bit count to byte count*/
    required_Length = (unsigned int)(required_Length / RIO_PL_BITS_IN_BYTE) + 
        ((required_Length % RIO_PL_BITS_IN_BYTE) == 0 ? 0 : 1); 

    /* arbiter data is always aligned to 32 bit boundary,
        so required_Length align is unnecessary*/
    if (arbiter->cur_Pos < required_Length)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Payload_Align
 *
 * Description: check if payload is aligned to dw boundary
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Payload_Align(RIO_PL_DS_T * handle, RIO_PL_OUT_ARB_T *arbiter)
{
    /*length of headers*/
    unsigned int required_Length, first_Crc_Pos;
    unsigned int ftype;

    if (arbiter == NULL || handle == NULL)
        return RIO_ERROR;

    /*get first CRC Pos - packet end pointer without CRC and padding*/
    first_Crc_Pos = get_CRC_Start(handle);
    /*get required header size in bits */
    required_Length = get_Header_Size(handle, arbiter);
    /*if CRC is single, decrease header lenght to it -neccesary to calculate payload size*/
    if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
        required_Length += RIO_PL_CRC_FIELD_SIZE;  
    

    /*convert from bit count to byte count*/
    required_Length = (unsigned int)(required_Length / RIO_PL_BITS_IN_BYTE) + 
        ((required_Length % RIO_PL_BITS_IN_BYTE) == 0 ? 0 : 1); 

    /* calculate payload size and compare it to DW boundary*/
    /* GDA: Bug#124 - Support for Data Streaming packet added below IF-ELSE Condition */
     ftype=get_Ftype(arbiter);
    if(ftype!=RIO_DS)
    {
    if (((first_Crc_Pos - required_Length) % RIO_PL_BYTE_CNT_IN_DWORD) != 0)
        return RIO_ERROR;
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Reserved_Fields
 *
 * Description: check if reserved fields are equal to zero
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Reserved_Fields( RIO_PL_DS_T *handle, RIO_PL_OUT_ARB_T *arbiter)
{
    /*all used numbers are individualy and can be changed only in ths part of spec*/
    /*length of headers*/
    unsigned int ftype, head_Size, field_Pos_Start;
    int field_Size;

    if (arbiter == NULL ||
        handle == NULL)
        return RIO_ERROR;

    /*check physical fields, common to all, in first byte */
    /*first reserved field*/
    /*depending on parallel or serial mode*/
    if (handle->is_Parallel_Model == RIO_TRUE)
    {
        if ((arbiter->packet_Stream[0] & 0x08) != 0)
            return RIO_ERROR;
        /*second reserved bit*/
        if ((arbiter->packet_Stream[0] & 0x03) != 0)
            return RIO_ERROR;
    }
    else
    {
        if ((arbiter->packet_Stream[0] & 0x07) != 0)
            return RIO_ERROR;
    }

    /*check reserved fields depending on ftype*/
        /*get required header size in bits */
    head_Size = get_Header_Size(handle, arbiter);
    ftype = get_Ftype(arbiter);

    /*here the the place of reserved field will be detected 
    by calculating it from the end of appropriate packet header*/
    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_NONINTERV_REQUEST:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_WRITE:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_STREAM_WRITE:
            /*pos in bits count*/
            field_Pos_Start = head_Size - 3;
            field_Size = RIO_PL_PACK_6_RSRV_FIELD_SIZE;
            return check_Reserved_Field(arbiter, field_Pos_Start, field_Size);
        
        case RIO_MAINTENANCE:
            if (is_Req(ftype, get_Ttype(arbiter)) == RIO_TRUE)
            {
                field_Pos_Start = head_Size - 2;
                field_Size = RIO_PL_PACK_8_REQ_RSRV_FIELD_SIZE;
            }
            else
            {
                field_Pos_Start = head_Size - 24;
                field_Size = RIO_PL_PACK_8_RESP_RSRV_FIELD_SIZE;
            }
            return check_Reserved_Field(arbiter, field_Pos_Start, field_Size);
        
        case RIO_DOORBELL:
            field_Pos_Start = head_Size - 32;
            field_Size = RIO_PL_PACK_10_RSRV_FIELD_SIZE;
            return check_Reserved_Field(arbiter, field_Pos_Start, field_Size);

        case RIO_MESSAGE:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_RESPONCE:
            return RIO_OK;
	/* GDA: Bug#124 - Support for Data Streaming packet */
   	case RIO_DS:
	    field_Pos_Start = head_Size - 3;
	    field_Size = RIO_PL_PACK_9_RSRV_FIELD_SIZE;
	    return RIO_OK;
	/* GDA: Bug#124 - Support for Data Streaming packet */
	 
	/* GDA: Bug#125 - Support for Flow control packet */ 
	case RIO_FC:
	    return RIO_OK;
	/* GDA: Bug#125  */ 
   
        default:
            return RIO_ERROR;
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : check_Reserved_Field
 *
 * Description: check if reserved field is equal to zero
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Reserved_Field(RIO_PL_OUT_ARB_T *arbiter, unsigned int field_Pos_Start, unsigned int field_Size)
{
    unsigned int byte_Num, mask;
    
    byte_Num = field_Pos_Start / RIO_PL_BITS_IN_BYTE;
    /*start pos in byte*/
    field_Pos_Start = field_Pos_Start % RIO_PL_BITS_IN_BYTE;
    while (field_Size > 0)
    {
        if (field_Size > (RIO_PL_BITS_IN_BYTE - field_Pos_Start))
        {
            mask = ((char) (0x1) << (RIO_PL_BITS_IN_BYTE - field_Pos_Start)) - 1;
            field_Size -= (RIO_PL_BITS_IN_BYTE - field_Pos_Start);
            field_Pos_Start = 0;
            if ((arbiter->packet_Stream[byte_Num] & mask) != 0)
                return RIO_ERROR;
            byte_Num++;
        }
        else
        {
            mask = ( ( (char) (0x1) << (RIO_PL_BITS_IN_BYTE - field_Pos_Start)) - 1) -
                (( (char) (0x1) << (RIO_PL_BITS_IN_BYTE - (field_Pos_Start + field_Size))) - 1);
            field_Size = 0;
            if ((arbiter->packet_Stream[byte_Num] & mask) != 0)
                return RIO_ERROR;
        }
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : is_Req
 *
 * Description: detect if ttype is request ttype
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T is_Req(unsigned int ftype, unsigned int ttype)
{
    switch(ftype)
    {
        case RIO_MAINTENANCE:
            if (ttype == RIO_MAINTENANCE_CONF_READ ||
                ttype == RIO_MAINTENANCE_CONF_WRITE ||
                ttype == RIO_MAINTENANCE_PORT_WRITE)
                return RIO_TRUE;
            break;

        /* one break because of one result */
        case RIO_DOORBELL:
        case RIO_MESSAGE:
        case RIO_NONINTERV_REQUEST:
        case RIO_WRITE:
        case RIO_STREAM_WRITE:
        case RIO_INTERV_REQUEST:
	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS: 
	/* GDA: Bug#124 - Support for Data Streaming packet */

        /* GDA: Bug#125 - Support for Flow control packet */
	case RIO_FC:
        /* GDA: Bug#125  */


            return RIO_TRUE;

        default:
            ;
    }

    return RIO_FALSE;
}

/*****************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */
/***************************************************************************
 *   Function : prod_Req_9
 *   
 *     Description: parse write request
 *     
 *      Returns: none
 *   Notes: none
 *
 ***************************************************************************/

static void prod_Req_9(RIO_PL_DS_T *handle, unsigned int j)
{
	    /* pointer to the entry */
	    RIO_PL_IN_ENTRY_T *entry;
	    RIO_PL_OUT_ARB_T  *arbiter;
		    /* index in the steram */
	   unsigned int i;

		        /* 
			 *  the routine stripps packets of type 9 to structure fields
			 *  the offsets, masks, shift constants are unique for this type
			 *  of packets. Change them here in case of specification changing 
			 *                     */
          if (handle == NULL)
		        return;

	    /* locate entry and arbiter */
	    entry = RIO_IN_GET_ENTRY(handle, j);
            arbiter = &RIO_IN_GET_ARB(handle);

		    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = 3;
    else
        i = 5;

				    /* complete fields */
	        entry->entry.ttype = 0;

		entry->entry.cos =  arbiter->packet_Stream[++i]; /*cos field*/
		
		entry->entry.s_Ds = ((arbiter->packet_Stream[++i] & 0x80) >> 7);
		

		entry->entry.e_Ds = ((arbiter->packet_Stream[i] & 0x40) >> 6);

	    		
		
		 if(entry->entry.e_Ds != 0 && entry->entry.s_Ds != 0)
		 {
			 /* Start  Segment */
#ifndef _CARBON_SRIO_XTOR
			 printf("\n Single  Segment \n");
#endif
		entry->entry.o = ((arbiter->packet_Stream[i] & 0x02) >>1);
		entry->entry.p = (arbiter->packet_Stream[i] & 0x01);
                entry->entry.stream_ID.flow_ID = arbiter->packet_Stream[++i];
		entry->entry.stream_ID.src_ID = arbiter->packet_Stream[++i];
		 }
		 
		 
		 else if(entry->entry.e_Ds)
		 {
			 /* End Segment */
#ifndef _CARBON_SRIO_XTOR
			 printf("\n End Segment \n");
#endif
		entry->entry.o = ((arbiter->packet_Stream[i] & 0x02) >> 1);
		entry->entry.p = (arbiter->packet_Stream[i] & 0x01);
		entry->entry.length = (arbiter->packet_Stream[++i] << 8);
		entry->entry.length |= (arbiter->packet_Stream[++i]);
		 }

		 else if(entry->entry.s_Ds)
		 {
			/*Start segment */
			 /* stream id */
#ifndef _CARBON_SRIO_XTOR
			 printf("\n....Start Segment\n");
#endif
                entry->entry.stream_ID.flow_ID = arbiter->packet_Stream[++i];
		entry->entry.stream_ID.src_ID = arbiter->packet_Stream[++i];
		 }

		 
		 else
		 {
			 /*conitunous Segment*/
#ifndef _CARBON_SRIO_XTOR
			 printf("\n Continuous Segment \n ");
#endif
		 }	




	     /* payload is always */
		i++;
	        
        if (arbiter->cur_Pos > RIO_PL_PACKET_LEN_NEED_INT_CRC)
	   {
	            unsigned int k; /* loop counter */
              if (arbiter->cur_Pos >= (i + RIO_PL_BYTES_IN_HALF_WORD + RIO_PL_BYTES_IN_HALF_WORD))
	            {
			     entry->payload_Size = 
	               (arbiter->cur_Pos -  i - RIO_PL_BYTES_IN_HALF_WORD - RIO_PL_BYTES_IN_HALF_WORD) /  RIO_PL_BYTE_CNT_IN_HALF_WORD;
					
		      if(entry->entry.e_Ds == 0 && entry->entry.s_Ds == 0)
			    entry->payload_Size= entry->payload_Size -1;
		            for (k = RIO_PL_INT_CRC_START; k < (arbiter->cur_Pos - 1); k++)
		            arbiter->packet_Stream[k] = arbiter->packet_Stream[k + 2];
		    }
		    else entry->payload_Size = 0;
	   }
	   else
	    {
		 if (arbiter->cur_Pos >=   (i + RIO_PL_BYTES_IN_HALF_WORD) )
		       {
				   entry->payload_Size = (arbiter->cur_Pos -  i - RIO_PL_BYTES_IN_HALF_WORD) / RIO_PL_BYTE_CNT_IN_HALF_WORD;
		          if(entry->entry.e_Ds == 0 && entry->entry.s_Ds != 0 && entry->entry.tt==0)
			   entry->payload_Size= entry->payload_Size-1 ;
		          if(entry->entry.e_Ds == 0 && entry->entry.s_Ds == 0 && entry->entry.tt!=0)
			   entry->payload_Size= entry->payload_Size-1 ;
			}
		      	  else entry->payload_Size = 0;
            } 

	   
		 if(entry->entry.o != 0 )   
		 {    
#ifndef _CARBON_SRIO_XTOR
			 printf("\n at reciever because of odd length of payload \n");
#endif
			 entry->payload_Size= entry->payload_Size -1;
		 }
		 if( entry->entry.p != 0)  
		 {    
#ifndef _CARBON_SRIO_XTOR
			 printf("\n Depadding at reciever because of  payload allignment \n");
#endif
			 entry->payload_Size= entry->payload_Size -1;
		 }

		 if(entry->entry.e_Ds != 0)
			 entry->payload_Size= entry->payload_Size ;
			 
		        /* convert the stream to payload */
    RIO_In_Stream_To_Hw(entry->payload_hw, arbiter->packet_Stream +i , entry->payload_Size);

		      
}
	
/*******************************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */

/***************************************************************************
 *  Function : RIO_In_Stream_To_Hw
 * 
 * Description: convert bytes to halfwords
 *  Returns: none
 * 
 * Notes: none
 ****************************************************************************/
void RIO_In_Stream_To_Hw(RIO_HW_T *hws, RIO_UCHAR_T *stream, unsigned int size)
{
	    unsigned int i; /* loop counter */

	        if (stream == NULL)
		      return;
		    /* 
		     *   this code converts bytes stream to half-words stream
		     *   hardcore constants are use here aren't used in the same sence everywhere else
		     *                */
		    for (i = 0; i < size; i++)
	 {
     hws[i] = ((unsigned short)stream[i * RIO_PL_BYTE_CNT_IN_HALF_WORD] << (RIO_PL_BITS_IN_BYTE ));
     hws[i]|= ((unsigned short)stream[i * RIO_PL_BYTE_CNT_IN_HALF_WORD + 1] );
	 }
} 

/*************************************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */







/* GDA: Bug#125 - Support for Flow control packet */ 

/***************************************************************************
 * Function : prod_Req_7
 *
 * Description: parse Flow Control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prod_Req_7(RIO_PL_DS_T *handle, unsigned int j)
{
    /* pointer to the entry */
    RIO_PL_IN_ENTRY_T *entry;
    RIO_PL_OUT_ARB_T  *arbiter;
    /* index in the steram */
    unsigned int i;

    /* 
     * the routine stripps packets of type 7 to structure fields
     * the offsets, masks, shift constants are unique for this type
     * of packets. Change them here in case of specification changing 
     */
    if (handle == NULL)
        return;

    handle->is_congestion =1;

    /* locate entry and arbiter */
    entry = RIO_IN_GET_ENTRY(handle, j);
    arbiter = &RIO_IN_GET_ARB(handle);

    /* locate first unparsed byte */
    if (entry->entry.tt == RIO_PL_TRANSP_16)
        i = 3;
    else
        i = 5;

    /* complete fields */
    entry->entry.ttype = (arbiter->packet_Stream[i] & RIO_PL_TTYPE_MASK) >> RIO_PL_TTYPE_SHIFT;
    entry->entry.size = arbiter->packet_Stream[i] & RIO_PL_SIZE_MASK;
    
    if(entry->entry.tt)
    {
    entry->entry.tgtdest_ID =(unsigned short) arbiter->packet_Stream[++i] << 8 ;
    entry->entry.tgtdest_ID |=(unsigned short) arbiter->packet_Stream[++i] ;
    }
    else
    entry->entry.tgtdest_ID = arbiter->packet_Stream[++i];

    entry->entry.x_On_Off = (arbiter->packet_Stream[++i] & 0x80) >> 7;

    entry->entry.flow_ID =  (arbiter->packet_Stream[++i] & 0xfe) << 1;

    entry->entry.soc = arbiter->packet_Stream[i] & 0x01;
                 
    
    i++;
   
        entry->payload_Size = 0;
    
}





/* GDA: Bug#125 - Support for Flow control packet */

/***************************************************************************
 * Function : RIO_PL_Congestion
 *
 * Description: parse Flow Control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/

int RIO_PL_Congestion( RIO_HANDLE_T   handle,
		        RIO_TAG_T      tag
		       )
{

  RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
  unsigned int i, i_Max, count = 0;


     if (instanse == NULL || instanse->in_Reset == RIO_TRUE)
           return RIO_ERROR;
    

    i_Max = RIO_PL_In_Get_Buf_Size(instanse);
    for (i = 0; i < i_Max-2; i++)
    {       if(tag==1)
            RIO_IN_GET_ENTRY(instanse, i)->state = RIO_PL_IN_READY ;
	    else
            RIO_IN_GET_ENTRY(instanse, i)->state = RIO_PL_IN_INVALID;
            count++;
    }
/*  instanse->ext_Interfaces.rio_PL_FC_To_Send(
             instanse->ext_Interfaces.rio_Hooks_Context,
	      count
		  );*/
            return count;


}


/**********************************************************************************/
