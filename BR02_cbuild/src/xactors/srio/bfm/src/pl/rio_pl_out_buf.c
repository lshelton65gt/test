/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/rio_pl_out_buf.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Physical layer outbound buffer source
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "rio_pl_out_buf.h"
#include "rio_pl_ds.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"

/***************************************************************************
 * Function : RIO_OUT_RES_FOR_3
 *
 * Description: detect count of entries reserved in outbound buffer for 3
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_RES_FOR_3(handle) (handle)->parameters_Set.res_For_3_Out

/***************************************************************************
 * Function : RIO_OUT_RES_FOR_2
 *
 * Description: detect count of entries reserved in outbound buffer for 2
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_RES_FOR_2(handle) (handle)->parameters_Set.res_For_2_Out

/***************************************************************************
 * Function : RIO_OUT_RES_FOR_1
 *
 * Description: detect count of entries reserved in outbound buffer for 1 prio
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_RES_FOR_1(handle) (handle)->parameters_Set.res_For_1_Out

/***************************************************************************
 * Function : RIO_OUT_GET_ENTRY
 *
 * Description: return address of an outbound buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_ENTRY(handle, i) ((handle)->outbound_Buf.buffer + (i))

/***************************************************************************
 * Function : RIO_OUT_GET_QUEUE_ELEM
 *
 * Description: return queue element
 *
 * Returns: queue element
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_QUEUE_ELEM(handle, i) ((handle)->outbound_Buf.send_Queue + (i))

/***************************************************************************
 * Function : RIO_PL_GET_TEMP
 *
 * Description: return outbound temporary
 *
 * Returns: outbound temporary
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_PL_GET_TEMP(handle) ((handle)->outbound_Buf.temp)

/***************************************************************************
 * Function : RIO_OUT_GET_ARB
 *
 * Description: return outbound arbiter
 *
 * Returns: outbound arbiter
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_ARB(handle) ((handle)->outbound_Arb)

/***************************************************************************
 * Function : RIO_OUT_GET_INT_ENG
 *
 * Description: return intervention engine
 *
 * Returns: the engine
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_INT_ENG(handle) ((handle)->outbound_Buf.int_Eng)

/***************************************************************************
 * Function : RIO_OUT_GET_INT_ENG_ELEM
 *
 * Description: return intervention engine element
 *
 * Returns: the element's address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_INT_ENG_ELEM(handle, i) ((handle)->outbound_Buf.int_Eng.entries + i)

/***************************************************************************
 * Function : RIO_OUT_GET_MULT_ENG
 *
 * Description: return multicast engine
 *
 * Returns: the engine
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_MULT_ENG(handle) ((handle)->outbound_Buf.mult_Eng)

/***************************************************************************
 * Function : RIO_OUT_GET_MULT_ENG_ELEM
 *
 * Description: return multicast engine element 
 *
 * Returns: the element's address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_MULT_ENG_ELEM(handle, i) ((handle)->outbound_Buf.mult_Eng.entries + i)

/***************************************************************************
 * Function : RIO_OUT_GET_SPLIT_ENG
 *
 * Description: return multicast engine
 *
 * Returns: the engine
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_SPLIT_ENG(handle) ((handle)->outbound_Buf.split_Eng)

/***************************************************************************
 * Function : RIO_OUT_GET_SPLIT_ENG_ELEM
 *
 * Description: return multicast engine element 
 *
 * Returns: the element's address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i) ((handle)->outbound_Buf.split_Eng.entries + i)


/***************************************************************************
 * Function : SUBSTITUTE_CRC
 *
 * Description: replace calculated CRC with the user defined crc
 *
 * Returns: none
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define SUBSTITUTE_CRC\
    if (entry->additional_Fields_Valid == RIO_TRUE) \
        if (entry->is_Crc_Valid == RIO_TRUE) \
            crc = (unsigned short)(crc ^ entry->crc);


/***************************************************************************
 * Function : SUBSTITUTE_INTERMEDIATE_CRC
 *
 * Description: replace calculated intermediate CRC with the user defined crc
 *
 * Returns: none
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define SUBSTITUTE_INTERMEDIATE_CRC\
   if (entry->additional_Fields_Valid == RIO_TRUE) \
        if(entry->is_Int_Crc_Valid == RIO_TRUE) \
            crc = (unsigned short)(crc ^ entry->intermediate_Crc);


/* sources of packets */
#define RIO_PL_OUT_QUEUE  0x0100
#define RIO_PL_OUT_INTERV 0x0200
#define RIO_PL_OUT_MULTI  0x0300
#define RIO_PL_OUT_SPLIT  0x0400

#define RIO_PL_OUT_INDEX_MASK 0x00ff
#define RIO_PL_OUT_SRC_MASK   0xff00

/* initialize one buffer entry */
static void init_Buffer_Entry(RIO_PL_OUT_ENTRY_T *element, unsigned int prio);
/* add element to sending queue */
static void out_Queue_Add(RIO_PL_DS_T *handle, int prio, unsigned int index);
/* shift the queue */
static void out_Queue_Shift(RIO_PL_DS_T *handle, unsigned int pos);
/* make packet */
static void pl_Out_Buf_Make_Stream(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry, 
    RIO_PL_OUT_ARB_T             *arbiter
    );

/* GDA: Bug#124 - Support for Data Streaming packet */
static void pl_Out_Buf_Make_9(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray,
    RIO_PL_OUT_ENTRY_T           *entry_Out,
    RIO_PL_OUT_ARB_T             *arbiter
    );
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
/* convert Flow Control  packet to the stream */
static void pl_Out_Buf_Make_7(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    );
/* GDA: Bug#125 - Support for Flow control packet */

/* make maintanance stream */
static void pl_Out_Buf_Make_8(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T *entry_Out, 
    RIO_PL_OUT_ARB_T *arbiter
    );
/* make response stream */
static void pl_Out_Buf_Make_13(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T *entry_Out, 
    RIO_PL_OUT_ARB_T *arbiter
    );
/* make doorbell stream */
static void pl_Out_Buf_Make_10(
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    );

/* check if response is waited for the packet */
static int packet_Need_Response(unsigned int ftype, unsigned int ttype);

/* convert message to stream */
static void pl_Out_Buf_Make_11(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    );

/* convert nonintervention packet to the stream */
static void pl_Out_Buf_Make_2(
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    );

/* convert intervention packet to the stream */
static void pl_Out_Buf_Make_1(
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    );

/* convert write packet to the stream */
static void pl_Out_Buf_Make_5(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    );

/* convert stream write packet to the stream */
static void pl_Out_Buf_Make_6(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    );

/* initialize the intervention engine  */
static void init_Int_Engine(RIO_PL_OUT_INTERV_ENG_T *engine);

/* initialize multicast engine */
static void init_Mult_Engine(RIO_PL_OUT_MULTICAST_ENG_T *engine);

/* initialize split engine */
static void init_Split_Engine(RIO_PL_OUT_SPLIT_ENG_T *engine);

/* load intervention engine */
static void out_Load_Int_Eng(RIO_PL_DS_T *handle, unsigned int src_Index);

/* load split/multicast engine */
static void out_Load_Split_Eng(RIO_PL_DS_T *handle, unsigned int src_Index);

/* load multicast engine */
static void out_Load_Multi_Eng(RIO_PL_DS_T *handle, unsigned int src_Index);

/* perform logical time-out in the outbound buffer */
static void out_Time_Out_In_Buffer(RIO_PL_DS_T *handle, unsigned int index);

/* perform logycal time-out for split engine participants */
static void out_Time_Out_Split(RIO_PL_DS_T *handle, unsigned int index);

/* time-out proceding in muticast engine */
static void out_Time_Out_Mult(RIO_PL_DS_T *handle, unsigned int index);

/* load response come to outbound buffer request*/
static void out_Rec_Buffer_Resp(RIO_PL_DS_T *handle, RIO_RESPONSE_T *ext_Resp, unsigned int index);

/* load response come to split engine request*/
static void out_Rec_Split_Resp(RIO_PL_DS_T *handle, RIO_RESPONSE_T *ext_Resp, unsigned int index);

/* load response come to multicast engine request*/
static void out_Rec_Multi_Resp(RIO_PL_DS_T *handle, RIO_RESPONSE_T *ext_Resp, unsigned int index);



static void out_Time_Out_In_Buffer_Errata_14_0503(RIO_PL_DS_T *handle, unsigned int index);



/***************************************************************************
 * Function : RIO_PL_Out_Load_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, RETRY or ERROR - not loaded
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Out_Load_Entry(RIO_PL_DS_T *handle)
{
    unsigned int i, i_Max; /* loop counter and limit value*/
    RIO_BOOL_T additional_Fields_Valid; /*tmp storage for buffer field*/

    if (handle == NULL)
        return RIO_ERROR;

    /* check if the temp is valid for downloading */
    if (RIO_PL_GET_TEMP(handle).state == RIO_PL_OUT_INVALID)
        return RIO_OK;

    /* check if we can load new entry to the outbound buffer immediately */
    if (!RIO_PL_Out_Get_Buf_Free(handle, RIO_PL_GET_TEMP(handle).entry.prio))
    {
        if (handle->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
            RIO_PL_GET_TEMP(handle).state = RIO_PL_OUT_INVALID;
        return RIO_RETRY;
    }

    i_Max = RIO_PL_Out_Get_Buf_Size(handle);

    additional_Fields_Valid = RIO_PL_GET_TEMP(handle).entry.additional_Fields_Valid;

    for (i = 0; i < i_Max; i++)
    {
        /* check if entry is free and we can use it for our priority */
        if (((RIO_PL_GET_TEMP(handle).entry.prio >= RIO_OUT_GET_ENTRY(handle, i)->res_For_Prio) ||
			(handle->is_PLLight_Model)) &&
            RIO_OUT_GET_ENTRY(handle, i)->state == RIO_PL_OUT_INVALID)
        {
            unsigned int tag;
            
            /* load new entry to the buffer */
            RIO_PL_GET_TEMP(handle).res_For_Prio = RIO_OUT_GET_ENTRY(handle, i)->res_For_Prio;
            *RIO_OUT_GET_ENTRY(handle, i) = RIO_PL_GET_TEMP(handle);
            RIO_OUT_GET_ENTRY(handle, i)->pl_Retry_Cnt = 0;
            RIO_OUT_GET_ENTRY(handle, i)->ll_Retry_Cnt = 0;

            /* assign trID */
            if ((handle->is_PLLight_Model == RIO_FALSE) &&
				RIO_PL_GET_TEMP(handle).entry.additional_Fields_Valid == RIO_FALSE &&
                RIO_PL_Is_Req(
                RIO_OUT_GET_ENTRY(handle, i)->entry.ftype,
                RIO_OUT_GET_ENTRY(handle, i)->entry.ttype) == RIO_OK)
            {
                /* multicast and split head record doesn't need in TID */
                if (RIO_OUT_GET_ENTRY(handle, i)->state == RIO_PL_OUT_READY)
                {
                    /* not all requests needs in TID fields */
                    if (RIO_OUT_GET_ENTRY(handle, i)->entry.ftype == RIO_MESSAGE ||
                        RIO_OUT_GET_ENTRY(handle, i)->entry.ftype == RIO_STREAM_WRITE ||
                        (RIO_OUT_GET_ENTRY(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                        RIO_OUT_GET_ENTRY(handle, i)->entry.ttype == RIO_MAINTENANCE_PORT_WRITE))
                    {
                    }
                    else
                        RIO_OUT_GET_ENTRY(handle, i)->entry.src_TID = RIO_PL_Out_Assert_TID(handle);
                }
            }
            
            /* update sending queue */
            out_Queue_Add(handle, RIO_OUT_GET_ENTRY(handle, i)->entry.prio, i);

            
            /* change state of temp buffer */
            tag = RIO_PL_GET_TEMP(handle).trx_Tag;
            init_Buffer_Entry(&RIO_PL_GET_TEMP(handle), 0);

            /*it is necessary to set pending bit 
            if requested transaction is port write*/
            if (RIO_OUT_GET_ENTRY(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                RIO_OUT_GET_ENTRY(handle, i)->entry.ttype == RIO_MAINTENANCE_PORT_WRITE)
            {
                unsigned long reg_Data;
                /*the pending bit is set by 0 writing*/
                RIO_PL_Read_Register(handle, 
                    handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A, 
                    &reg_Data, RIO_TRUE);
                
                reg_Data |= RIO_SET_PENDING_BIT_MASK;

                RIO_PL_Write_Register(handle, 
                    handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
                    reg_Data, RIO_TRUE);

            }
            
            if ( (handle->parameters_Set.enable_Retry_Return_Code != RIO_TRUE) &&
                (additional_Fields_Valid != RIO_TRUE) &&
				(handle->is_PLLight_Model == RIO_FALSE))
            {
                /* packet has just been accepted */
                if (RIO_PL_Is_Req(
                    RIO_OUT_GET_ENTRY(handle, i)->entry.ftype, 
                    RIO_OUT_GET_ENTRY(handle, i)->entry.ttype) == RIO_OK)
                {
                    handle->ext_Interfaces.rio_PL_Ack_Request(
                        handle->ext_Interfaces.pl_Interface_Context,
                        tag);
                }
                else
                {
                    handle->ext_Interfaces.rio_PL_Ack_Response(
                        handle->ext_Interfaces.pl_Interface_Context,
                        tag);
                
                }
            }

            RIO_OUT_GET_ENTRY(handle, i)->time_After_Got_From_LL = 0;
            break;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Out_Start_New_Packet
 *
 * Description: start new packet transmission and get the first granule
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Out_Start_New_Packet(
    RIO_PL_DS_T  *handle, 
    RIO_GRANULE_T *granule, 
    unsigned int *packet_Handle,
    unsigned int ack_Id
    )
{
    unsigned int src_Index = 0, index = 0;
    unsigned int cur_Prio = 0;
    unsigned int queue_Index = 0;
    RIO_PL_OUT_ENTRY_T *entry_For_Tr = NULL;
    unsigned int i, i_Max;
    RIO_UCHAR_T packet_Len_In_Granules = 0;
    RIO_UCHAR_T *point_To_Pack_Len_In_Granules = NULL;

    if (handle == NULL || granule == NULL || packet_Handle == NULL)
        return RIO_ERROR;
    
    /* detect the most priority source for transmitting */
    /* go through outbound queue - the lowest priority */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);
      
    

    for (i = 0; i < i_Max; i++)
    {
        if ((RIO_OUT_GET_QUEUE_ELEM(handle, i)->state == RIO_PL_QUEUE_STATE_READY &&
            RIO_OUT_GET_QUEUE_ELEM(handle, i)->value >= (int)cur_Prio) && 
            ((handle->is_PLLight_Model) ||
			((handle->outbound_Buf.out_Buf_Param_Set.only_Responses == RIO_FALSE || 
            (RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->entry.ftype == RIO_RESPONCE || 
            (RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->entry.ftype == RIO_MAINTENANCE && 
            (RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
            RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->entry.ttype ==
                RIO_MAINTENANCE_WRITE_RESPONCE)))) && 
            (handle->outbound_Buf.out_Buf_Param_Set.only_Maint_Responses == RIO_FALSE || 
            (RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->entry.ftype == RIO_MAINTENANCE && 
            (RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
            RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->entry.ttype == RIO_MAINTENANCE_WRITE_RESPONCE))))))
        {
            /* if transaction is waiting for intervention engine check if it is ready*/
            if (RIO_OUT_GET_ENTRY(handle, 
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->state == RIO_PL_OUT_WAIT_INTERVEN)
            {
                /* engine is not busy */
                if (RIO_OUT_GET_INT_ENG(handle).valid == RIO_FALSE)
                {
                    /* load the engine and change queue element's state */
                    out_Load_Int_Eng(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index);
                    RIO_OUT_GET_QUEUE_ELEM(handle, i)->state = RIO_PL_QUEUE_STATE_SENT; 
                }
                break;
            }
            /* check if it needs in multicast engine */
            if (RIO_OUT_GET_ENTRY(handle, 
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->state == RIO_PL_OUT_WAIT_MULTICAST)
            {
                /* engine is not busy */
                if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_FALSE)
                {
                    /* load the engine and change queue element's state */
                    out_Load_Multi_Eng(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index);
                    RIO_OUT_GET_QUEUE_ELEM(handle, i)->state = RIO_PL_QUEUE_STATE_SENT; 
                }
                break;
            }

            /* check if it needs in split engine */
            if (RIO_OUT_GET_ENTRY(handle, 
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->index)->state == RIO_PL_OUT_WAIT_SPLIT)
            {
                /* engine is not busy */
                if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_FALSE)
                {
                    /* load the engine and change queue element's state */
                    out_Load_Split_Eng(handle, RIO_OUT_GET_QUEUE_ELEM(handle, i)->index);
                    RIO_OUT_GET_QUEUE_ELEM(handle, i)->state = RIO_PL_QUEUE_STATE_SENT; 
                }
                break;
            }
            /* there are not any necessary engines, go directly from the buffer */
            index = RIO_OUT_GET_QUEUE_ELEM(handle, i)->index;
	      /* GDA : Bug # 138 : Support for response delay */ 
	/*   printf("\n RIO_OUT_GET_EN  entry ftype is %d and index is %d",RIO_OUT_GET_ENTRY(handle, index)->entry.ftype,index);
	   printf("\n resp_delay is %d delay enable is %d\n",(RIO_OUT_GET_ENTRY(handle, index))->entry.resp_Delay,handle->resp_Packet_Delay_Enable);*/

 if ((!(handle->is_resp_Packet) &&RIO_OUT_GET_ENTRY(handle, index)->entry.ftype  == RIO_RESPONCE) &&
        handle->inst_Param_Set.resp_Packet_Delay_Enable == RIO_TRUE){
        handle->is_resp_Packet = RIO_TRUE;
	handle->resp_Packet_Delay_Enable = handle->inst_Param_Set.resp_Packet_Delay_Enable;
        handle->resp_Packet_Delay_Cntr   = handle->inst_Param_Set.resp_Packet_Delay_Cntr;
 } 

	   
	   if((RIO_OUT_GET_ENTRY(handle, index)->entry.ftype == RIO_RESPONCE) &&
	      ( handle->resp_Packet_Delay_Enable         == RIO_TRUE) &&
	      ((RIO_OUT_GET_ENTRY(handle, index))->entry.resp_Delay == RIO_FALSE))
		 {
#ifndef _CARBON_SRIO_XTOR
			 printf("\n[Inst %d]1->res_Pkt_Delay_Cntr: %d ", handle->instance_Number, \
					                 handle->resp_Packet_Delay_Cntr);
#endif
		/* handle->check_Ack_Id=ack_Id;*/
			 
		 			 

			 break;
		 }

	        /* GDA : Bug # 138 : Support for response delay */  
            queue_Index = i;
            src_Index = RIO_PL_OUT_QUEUE;
            cur_Prio = RIO_OUT_GET_QUEUE_ELEM(handle, i)->value;
            entry_For_Tr = RIO_OUT_GET_ENTRY(handle, index);
            break;
        }
    } /* end of loop through outbound buffer */

   
    /* look through split engine - medium priority*/
    if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_TRUE)
    {
        for (i = 0; i < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt; i++)
        {
            if ((RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->state == RIO_PL_OUT_READY &&
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.prio >= cur_Prio) && 
                (handle->outbound_Buf.out_Buf_Param_Set.only_Responses == RIO_FALSE ||
                (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.ftype == RIO_RESPONCE || 
                (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_WRITE_RESPONCE)))) && 
                (handle->outbound_Buf.out_Buf_Param_Set.only_Maint_Responses == RIO_FALSE ||
                (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_WRITE_RESPONCE))))
            {
                src_Index = RIO_PL_OUT_SPLIT;
                cur_Prio = RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.prio;
                index = i & RIO_PL_OUT_INDEX_MASK;
                entry_For_Tr = RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i);
                break;
            }
        }
    }

    /* look through multicast engine - high priority */
    if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_TRUE)
    {
        for (i = 0; i < RIO_OUT_GET_MULT_ENG(handle).entries_Cnt; i++)
        {
            if ((RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->state == RIO_PL_OUT_READY &&
                RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.prio >= cur_Prio) &&
                (handle->outbound_Buf.out_Buf_Param_Set.only_Responses == RIO_FALSE ||
                (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.ftype == RIO_RESPONCE || 
                (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
                RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_WRITE_RESPONCE)))) && 
                (handle->outbound_Buf.out_Buf_Param_Set.only_Maint_Responses == RIO_FALSE ||
                (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
                RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_WRITE_RESPONCE))))
            {
                src_Index = RIO_PL_OUT_MULTI;
                cur_Prio = RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.prio;
                index = i & RIO_PL_OUT_INDEX_MASK;
                entry_For_Tr = RIO_OUT_GET_MULT_ENG_ELEM(handle, i);
                break;
            }
        }
    }
    
    /* look in intervention engine - the highest priority */
    if (RIO_OUT_GET_INT_ENG(handle).valid == RIO_TRUE)
    {
        for (i = 0; i < RIO_PL_OUT_INTRV_ENGINE_LENGHT; i++)
        {
            if ((RIO_OUT_GET_INT_ENG_ELEM(handle, i)->state == RIO_PL_OUT_READY &&
                RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.prio >= cur_Prio) &&
                (handle->outbound_Buf.out_Buf_Param_Set.only_Responses == RIO_FALSE ||
                (RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.ftype == RIO_RESPONCE || 
                (RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                (RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
                RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_WRITE_RESPONCE)))) && 
                (handle->outbound_Buf.out_Buf_Param_Set.only_Maint_Responses == RIO_FALSE ||
                (RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.ftype == RIO_MAINTENANCE &&
                (RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_READ_RESPONCE ||
                RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.ttype == RIO_MAINTENANCE_WRITE_RESPONCE))))
            {
                src_Index = RIO_PL_OUT_INTERV;
                cur_Prio = RIO_OUT_GET_INT_ENG_ELEM(handle, i)->entry.prio;
                index = i & RIO_PL_OUT_INDEX_MASK;
                entry_For_Tr = RIO_OUT_GET_INT_ENG_ELEM(handle, i);
                break;
            }
        }
    }

    
    /* make index with mark where the packet is from */
    index |= src_Index;

    /* check if we have a packet for transmitting */
    switch (src_Index)
    {
        /* packet will load from the outbound buffer */
        case RIO_PL_OUT_QUEUE:

           /* GDA Included: Description=> Delaying Response Packet */
	   if ((entry_For_Tr->entry.ftype==13)&&(handle->response_Packet_Delay_Flag==RIO_FALSE)&&
					        (handle->inst_Param_Set.ack_Delay_Enable==RIO_TRUE))
              {
         	return RIO_ERROR;
      	      }
 	   else if ((entry_For_Tr->entry.ftype==13)&&(handle->response_Packet_Delay_Flag==RIO_TRUE)&&
					        (handle->inst_Param_Set.ack_Delay_Enable==RIO_TRUE))
      	      {
       	  	 handle->response_Packet_Delay_Flag=RIO_FALSE;
      	      }  
          /* GDA Included: Description-Delaying Response Packet */

	    /* update the entry state */
	    RIO_OUT_GET_QUEUE_ELEM(handle, queue_Index)->state = RIO_PL_QUEUE_STATE_SENT;
            break;
        /* packet will load from intervention engine */
        case RIO_PL_OUT_INTERV:
            break;

        /* multicast engine */
        case RIO_PL_OUT_MULTI:
            break;

        /* split engine*/
        case RIO_PL_OUT_SPLIT:
            break;

        /* there are not any packets for transmitting */
        default:
	    return RIO_ERROR;
    }

    /* update the packet state*/   
    entry_For_Tr->state = RIO_PL_OUT_SENDING;

    if ((handle->is_PLLight_Model) && (entry_For_Tr->is_Stream == RIO_TRUE))
    {
		unsigned short crc_Start;
		unsigned short crc, crc_Unprotected;

		crc = RIO_PL_INIT_CRC_VALUE;

		RIO_OUT_GET_ARB(handle).packet_Len = entry_For_Tr->packet_Len;

		for (i = 0; i < entry_For_Tr->packet_Len; i++)
			RIO_OUT_GET_ARB(handle).packet_Stream[i] = entry_For_Tr->packet_Stream[i];

		if (entry_For_Tr->is_Padded == RIO_TRUE)
		{
			crc_Start = entry_For_Tr->packet_Len - 4;
		}
		else
		{
			crc_Start = entry_For_Tr->packet_Len - 2;
		}

		/* if packet_Len is small setup the crc location */
		if (crc_Start > entry_For_Tr->packet_Len)
			crc_Start = 0;

        if ((entry_For_Tr->entry.is_Int_Crc_Valid == RIO_FALSE) && (crc_Start > 80))
		{
			/* clear fields that are not protected by crc */
			crc_Unprotected = (RIO_OUT_GET_ARB(handle).packet_Stream[0]) && 0xFC;
			RIO_OUT_GET_ARB(handle).packet_Stream[0] &= 0x3;

            /* compute intermadiate CRC */
            RIO_PL_Out_Compute_CRC(&crc, RIO_OUT_GET_ARB(handle).packet_Stream, 80);

			/* restore fields that are not protected by crc */
			RIO_OUT_GET_ARB(handle).packet_Stream[0] |= crc_Unprotected;

            RIO_OUT_GET_ARB(handle).packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
            RIO_OUT_GET_ARB(handle).packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
		}

        if ((entry_For_Tr->entry.is_Crc_Valid == RIO_FALSE))
		{
            /* compute CRC */
			if (entry_For_Tr->packet_Len > 82)
	            RIO_PL_Out_Compute_CRC(&crc, RIO_OUT_GET_ARB(handle).packet_Stream + 80, crc_Start);
			else
			{
				unsigned int crc_Unprotected;
				/* clear fields that are not protected by crc */
				crc_Unprotected = (RIO_OUT_GET_ARB(handle).packet_Stream[0]) && 0xFC;
				RIO_OUT_GET_ARB(handle).packet_Stream[0] &= 0x3;

	            RIO_PL_Out_Compute_CRC(&crc, RIO_OUT_GET_ARB(handle).packet_Stream, crc_Start);

				/* restore fields that are not protected by crc */
				RIO_OUT_GET_ARB(handle).packet_Stream[0] |= crc_Unprotected;
			}

            RIO_OUT_GET_ARB(handle).packet_Stream[crc_Start] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
            RIO_OUT_GET_ARB(handle).packet_Stream[crc_Start + 1] = (RIO_UCHAR_T)(crc & 0x00ff);
		}

		/* add proper value of ackID to the stream */
		if (entry_For_Tr->is_AckID_Valid == RIO_TRUE)
		{
			if (handle->is_Parallel_Model == RIO_TRUE)
			{
			RIO_OUT_GET_ARB(handle).packet_Stream[0] = (RIO_UCHAR_T)((ack_Id & 0x07) << 4);
			}
			else
			{
			RIO_OUT_GET_ARB(handle).packet_Stream[0] = (RIO_UCHAR_T)((ack_Id & 0x1f) << 3);
			}
		}
		else
			entry_For_Tr->is_AckID_Valid = RIO_TRUE;
	}
	else
	{
		/* load the packet to stream */
		RIO_OUT_GET_ARB(handle).packet_Stream[0] = 0;
		/*it is necessary initialize fields that are protected by crc*/
		if (entry_For_Tr->entry.additional_Fields_Valid == RIO_TRUE)
		{
			if (handle->is_Parallel_Model == RIO_TRUE)
			{
				RIO_OUT_GET_ARB(handle).packet_Stream[0] |= (entry_For_Tr->entry.rsrv_Phy_2 & 3);
			}
			else
			{
				/*protected only 2 bits of reserved field*/
				RIO_OUT_GET_ARB(handle).packet_Stream[0] |= (entry_For_Tr->entry.rsrv_Phy_1 & 3);
			}

		}
		/*GDA:Bug#139 remove the need to recompute crc */
		 entry_For_Tr->data_Corrupted =handle->data_Corrupted; 
		/*GDA:Bug#139 remove the need to recompute crc */
		pl_Out_Buf_Make_Stream(&handle->ext_Interfaces, entry_For_Tr, &RIO_OUT_GET_ARB(handle));
	}

    /* modify arbiter*/
    RIO_OUT_GET_ARB(handle).cur_Pos = 0;
    RIO_OUT_GET_ARB(handle).index = index;
    *packet_Handle = index;

    
    /* 
     * complete the first byte with ackID  -depending on mode - serial or parallel - 
     * in according with the specification. 
     * ackID mask and offest are used only here has't to be defined as macroses
     */
    if (!((handle->is_PLLight_Model) && (entry_For_Tr->is_Stream == RIO_TRUE)))
	{
		if (handle->is_Parallel_Model == RIO_TRUE)
		{
			RIO_OUT_GET_ARB(handle).packet_Stream[0] = 0;
			RIO_OUT_GET_ARB(handle).packet_Stream[0] = (RIO_UCHAR_T)((ack_Id & 0x07) << 4);
        
			/*add s-parity bit*/
			if (handle->is_PLLight_Model == RIO_FALSE)
    			RIO_OUT_GET_ARB(handle).packet_Stream[0] |= (RIO_UCHAR_T)(0x1 << 2);
			else
			{
    			RIO_OUT_GET_ARB(handle).packet_Stream[0] |= (RIO_UCHAR_T)((entry_For_Tr->entry.s & 1) << 7);
    			RIO_OUT_GET_ARB(handle).packet_Stream[0] |= (RIO_UCHAR_T)((entry_For_Tr->entry.s_inv & 1) << 2);
			}

			if (entry_For_Tr->entry.additional_Fields_Valid == RIO_TRUE)
			{
				RIO_OUT_GET_ARB(handle).packet_Stream[0] |= ((entry_For_Tr->entry.rsrv_Phy_1 & 1) << 3);
				RIO_OUT_GET_ARB(handle).packet_Stream[0] |= (entry_For_Tr->entry.rsrv_Phy_2 & 3);
			}
		}
		else
		{
			/*in case of serial model the ackID is 5-bit size*/
			RIO_OUT_GET_ARB(handle).packet_Stream[0] = 0;
			RIO_OUT_GET_ARB(handle).packet_Stream[0] = (RIO_UCHAR_T)((ack_Id & 0x1f) << 3);
			if (entry_For_Tr->entry.additional_Fields_Valid == RIO_TRUE)
			{
				RIO_OUT_GET_ARB(handle).packet_Stream[0] |= (entry_For_Tr->entry.rsrv_Phy_1 & 7);
			}
		}
	}

    /*add user's hook to send packet*/
    packet_Len_In_Granules = RIO_OUT_GET_ARB(handle).packet_Len / 4;
    point_To_Pack_Len_In_Granules = &packet_Len_In_Granules;        

  
    if ((handle->ext_Interfaces.rio_Packet_To_Send != NULL))
    {
          handle->ext_Interfaces.rio_Packet_To_Send(
          handle->ext_Interfaces.rio_Hooks_Context,
          RIO_OUT_GET_ARB(handle).packet_Stream,
          RIO_PL_OUT_MAX_PACK_STREAM_LEN,
          point_To_Pack_Len_In_Granules,
          entry_For_Tr->trx_Tag);

          RIO_OUT_GET_ARB(handle).packet_Len = (*point_To_Pack_Len_In_Granules) * 4;
    };

    /* prepare the first granule  -consist from 4 bytes with corresponding offsets from 0 to 3*/
    granule->granule_Type = RIO_GR_NEW_PACKET;
    {
        unsigned int i; /* loop counter */

        for (i = 0; i < RIO_PL_BYTE_CNT_IN_WORD; i++)
            granule->granule_Date[i] = RIO_OUT_GET_ARB(handle).packet_Stream[i];
    }

    /* update the arbiter state*/
    RIO_OUT_GET_ARB(handle).cur_Pos = RIO_PL_BYTE_CNT_IN_WORD;
     /* GDA: Bug#138 - Repeat the delay for rest of the response pkts */
       /* handle->resp_Packet_Delay_Cntr = handle->inst_Param_Set.resp_Packet_Delay_Cntr; */
    if (  entry_For_Tr->entry.ftype == RIO_RESPONCE){	
    
#ifndef _CARBON_SRIO_XTOR
	printf("\n([Inst: %d] out_buf.c) Set Resp Delay cntr\n", handle->instance_Number);
#endif
	handle->resp_Packet_Delay_Cntr = handle->inst_Param_Set.resp_Packet_Delay_Cntr;
	handle->is_resp_Packet = RIO_FALSE;
    }
    /* GDA: Bug#138 */

  
    return RIO_OK;
}

          
			
        
            


/***************************************************************************
 * Function : out_Load_Multi_Eng
 *
 * Description: load multicast engine
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Load_Multi_Eng(RIO_PL_DS_T *handle, unsigned int src_Index)
{
    unsigned int i; /* loop counter */

    RIO_PL_OUT_ENTRY_T *element = NULL; /* holds buffer's entry address*/

    if (handle == NULL || src_Index >= handle->inst_Param_Set.pl_Outbound_Buffer_Size)
        return;

    element = RIO_OUT_GET_ENTRY(handle, src_Index);

    /* set multicast engine valid */
    RIO_OUT_GET_MULT_ENG(handle).valid = RIO_TRUE;
    RIO_OUT_GET_MULT_ENG(handle).entries_Cnt = element->multicast_Size;
    RIO_OUT_GET_MULT_ENG(handle).source_Index = src_Index;

    /* 
     * load multicast request, correct transport info,
     * correct state and assign TID
     */
    for (i = 0; i < element->multicast_Size; i++)
    {
        *RIO_OUT_GET_MULT_ENG_ELEM(handle, i) = *RIO_OUT_GET_ENTRY(handle, src_Index);
        RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->state = RIO_PL_OUT_READY;
        RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.tranp_Info = RIO_OUT_GET_ENTRY(handle, src_Index)->tr_Infos[i];
        RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.src_TID = RIO_PL_Out_Assert_TID(handle);
        /*for hook purposes*/
     }
}

/***************************************************************************
 * Function : out_Load_Split_Eng
 *
 * Description: load split engine
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Load_Split_Eng(RIO_PL_DS_T *handle, unsigned int src_Index)
{
    /* the flag that tha last request has been formed and count*/
    unsigned int req_Cnt = 0,
        dw_Size,
        curr_Size,
        curr_Addr,
        curr_Off = 0,
        size, wdptr;

    if (handle == NULL || src_Index >= handle->inst_Param_Set.pl_Outbound_Buffer_Size)
        return;

    /* prepare data */
    dw_Size = RIO_OUT_GET_ENTRY(handle, src_Index)->payload_Size;
    curr_Size = RIO_OUT_GET_ENTRY(handle, src_Index)->max_Size;
    if (RIO_OUT_GET_ENTRY(handle, src_Index)->entry.ftype == RIO_MAINTENANCE)
    {
        curr_Addr = RIO_OUT_GET_ENTRY(handle, src_Index)->entry.config_Off;
    }
    else
    {
        curr_Addr = RIO_OUT_GET_ENTRY(handle, src_Index)->entry.address;
    }


    /*try to load split engine */
    RIO_OUT_GET_SPLIT_ENG(handle).valid = RIO_TRUE;
    RIO_OUT_GET_SPLIT_ENG(handle).source_Index = src_Index;

    /* split engine loading itself */
    if (dw_Size > 1)
    {
        while (dw_Size)
        {
            
            /* try assign max possible payload */
            while (dw_Size < curr_Size)
                curr_Size >>= 1;
            
            /* detect rsize/wsize and wdptr*/
            RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, curr_Size, 8, 0);
            
            /* allocate entry */
            *RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt) = *RIO_OUT_GET_ENTRY(handle, src_Index);
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.size = size;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.wdptr = wdptr;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->state = RIO_PL_OUT_READY;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->pl_Retry_Cnt = 0;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->ll_Retry_Cnt = 0;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->time_After_Acked = 0;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->offset = (RIO_UCHAR_T)curr_Off;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->payload_Size = curr_Size;

            /* assign TID if it's necessary */
            if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.ftype == RIO_STREAM_WRITE ||
                ( RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.ftype == RIO_MAINTENANCE &&
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.ttype == RIO_MAINTENANCE_PORT_WRITE) )
            {
            }
            else
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.src_TID = RIO_PL_Out_Assert_TID(handle);

            if (RIO_OUT_GET_ENTRY(handle, src_Index)->entry.ftype == RIO_MAINTENANCE)
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.config_Off = curr_Addr;
            else
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.address = curr_Addr;
            
            
            /* increase requests count */
            req_Cnt++;
            
            /* decrease double-word count */
            dw_Size -= ((dw_Size <= curr_Size) ? dw_Size : curr_Size);
            
            /* increase address */
            curr_Addr += curr_Size;
            curr_Off += curr_Size;
        }
    }
    else
    {
        /* misaligned request */
        unsigned int byte_Num = RIO_OUT_GET_ENTRY(handle, src_Index)->byte_Num;
            
        curr_Off = RIO_OUT_GET_ENTRY(handle, src_Index)->byte_Offset;

        while (byte_Num)
        {
            if (curr_Off % 2)
                curr_Size = 1;
            else if (curr_Off % 4)
                curr_Size = 2;
            else
                curr_Size = 4;

            /* check possible size */
            while (byte_Num < curr_Size)
                curr_Size >>= 1;

            /* detect rsize/wsize and wdptr*/
            RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, dw_Size, curr_Size, curr_Off);

            /* allocate entry */
            *RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt) = *RIO_OUT_GET_ENTRY(handle, src_Index);
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.size = size;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.wdptr = wdptr;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->state = RIO_PL_OUT_READY;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->pl_Retry_Cnt = 0;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->ll_Retry_Cnt = 0;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->time_After_Acked = 0;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->offset = (RIO_UCHAR_T)0;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->byte_Num = curr_Size;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->payload_Size = 1;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->byte_Offset = (RIO_UCHAR_T)curr_Off;

            /* assign TID if it's necessary */
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.src_TID = RIO_PL_Out_Assert_TID(handle);
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, req_Cnt)->entry.address = curr_Addr;

            /* increase requests count */
            req_Cnt++;

            /* decrease byte_NUm */
            byte_Num -= curr_Size;

            curr_Off += curr_Size;
        }
    }

    /* update count of request in split engine */
    RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt = req_Cnt;

}

/***************************************************************************
 * Function : out_Load_Int_Eng
 *
 * Description: load intervention engine
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Load_Int_Eng(RIO_PL_DS_T *handle, unsigned int src_Index)
{
    if (handle == NULL || src_Index >= RIO_PL_Out_Get_Buf_Size(handle))
        return;
    
    /* change engine state*/
    RIO_OUT_GET_INT_ENG(handle).valid = RIO_TRUE;
    RIO_OUT_GET_INT_ENG(handle).source_Index = src_Index;

    /* load INTERV - the first entry has to be intervention response*/
    *RIO_OUT_GET_INT_ENG_ELEM(handle, 0) = *RIO_OUT_GET_ENTRY(handle, src_Index);
    RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->state = RIO_PL_OUT_READY;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->pl_Retry_Cnt = 0;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->ll_Retry_Cnt = 0;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->time_After_Acked = 0;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->entry.ftype = RIO_RESPONCE;

    /* intervention can be without data */
    if (RIO_OUT_GET_ENTRY(handle, src_Index)->entry.ttype == RIO_RESPONCE_INTERVENTION_WO_DATA)
        RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->entry.ttype = RIO_RESPONCE_WO_DATA;
    else
        RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->entry.ttype = RIO_RESPONCE_WITH_DATA;

    RIO_OUT_GET_INT_ENG_ELEM(handle, 0)->entry.status = RIO_RESPONCE_STATUS_INTERV;

    /* load DATA ONLY - the second one has to be DATA ONLY*/
    *RIO_OUT_GET_INT_ENG_ELEM(handle, 1) = *RIO_OUT_GET_ENTRY(handle, src_Index);
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->state = RIO_PL_OUT_READY;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->pl_Retry_Cnt = 0;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->ll_Retry_Cnt = 0;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->time_After_Acked = 0;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->entry.ftype = RIO_RESPONCE;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->entry.ttype = RIO_RESPONCE_WITH_DATA;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->entry.status = RIO_RESPONCE_STATUS_DATA_ONLY;

    /* we shall sent to secondary target in the second entry*/
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->entry.src_TID = 
        RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->entry.sec_TID;
    RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->entry.tranp_Info = 
        RIO_OUT_GET_INT_ENG_ELEM(handle, 1)->entry.sec_ID;
}

/***************************************************************************
 * Function : RIO_PL_Out_Get_Data
 *
 * Description: obtain new data for transmitting packet
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Out_Get_Data(
    RIO_PL_DS_T  *handle, 
    RIO_GRANULE_T *granule, 
    RIO_BOOL_T *is_Last
    )
{
    if (handle == NULL || granule == NULL || is_Last == NULL)
        return RIO_ERROR;

    /* check if packet is in transmission*/
    if (RIO_OUT_GET_ARB(handle).index == (unsigned int)RIO_PL_QUEUE_ELEM_UNUSED)
        return RIO_ERROR;

    *is_Last = RIO_FALSE;

    /* check if the last portion of packet has already been sent */
    if (RIO_OUT_GET_ARB(handle).cur_Pos < RIO_OUT_GET_ARB(handle).packet_Len)
    {
        unsigned int i; /* loop counter */

        /* load new data to the granule and update position */
        granule->granule_Type = RIO_GR_DATA;

        for (i = 0; i < RIO_PL_BYTE_CNT_IN_WORD; i++)
            granule->granule_Date[i] = 
                RIO_OUT_GET_ARB(handle).packet_Stream[RIO_OUT_GET_ARB(handle).cur_Pos + i];

        RIO_OUT_GET_ARB(handle).cur_Pos += RIO_PL_BYTE_CNT_IN_WORD;
    }
    else
    {
        /* source of transaction and the index there */
        unsigned int index, src_Index;

        index = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_INDEX_MASK;
        src_Index = (RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK);

        /* clear arbiter state */
        RIO_PL_Out_Init_Arbiter(&RIO_OUT_GET_ARB(handle));
        *is_Last = RIO_TRUE;

        /* update packet state in the source */
        switch(src_Index)
        {
            /* outbound buffer */
            case RIO_PL_OUT_QUEUE:
                RIO_OUT_GET_ENTRY(handle, index)->state = RIO_PL_OUT_SENT;
                break;

            /* intervention engine */
            case RIO_PL_OUT_INTERV:
                RIO_OUT_GET_INT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_SENT;
                break;

            /* multicast engine  */
            case RIO_PL_OUT_MULTI:
                RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_SENT;
                break;

            /* split engine */
            case RIO_PL_OUT_SPLIT:
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_SENT;
                break;

            /* an error */
            default:
                ;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Out_Packet_Ack
 *
 * Description: notify the buffer about physical layer acknowledge
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Packet_Ack(
    RIO_PL_DS_T  *handle, 
    unsigned int packet_Handle,
	RIO_BOOL_T is_Positive
    )
{
    /* source and the index */
    unsigned int index, src_Index;
    unsigned int i, i_Max;

    if (handle == NULL)
        return;
 
    src_Index = packet_Handle & RIO_PL_OUT_SRC_MASK;
    index = packet_Handle & RIO_PL_OUT_INDEX_MASK;

    switch (src_Index)
    {
        /* outbound queue */
        case RIO_PL_OUT_QUEUE:

            /* change state */
            RIO_OUT_GET_ENTRY(handle, index)->state = RIO_PL_OUT_ACKED;
            RIO_OUT_GET_ENTRY(handle, index)->pl_Retry_Cnt = 0;
            RIO_OUT_GET_ENTRY(handle, index)->time_After_Acked = 0;

            /* check if it's a response and we shall deallocate space */
            if ((packet_Need_Response(
                RIO_OUT_GET_ENTRY(handle, index)->entry.ftype,
                RIO_OUT_GET_ENTRY(handle, index)->entry.ttype
                ) == RIO_ERROR) || (handle->is_PLLight_Model))
            {
                /* delete from queue*/
                i_Max = RIO_PL_Out_Get_Buf_Size(handle);
                for (i = 0; i < i_Max; i++)
                    if (RIO_OUT_GET_QUEUE_ELEM(handle, i)->index == index)
                    {
                        out_Queue_Shift(handle, i);
                        break;
                    }

				if (handle->is_PLLight_Model)
				{
					handle->ext_Interfaces.rio_PLLight_Packet_Acknowledged(
						handle->ext_Interfaces.pl_Interface_Context,
						RIO_OUT_GET_ENTRY(handle, index)->trx_Tag,
						is_Positive);

				}

         init_Buffer_Entry(RIO_OUT_GET_ENTRY(handle, index), RIO_OUT_GET_ENTRY(handle, index)->res_For_Prio);
          
           /*GDA: Bug#43 Fix */ 
           if (handle->is_PLLight_Model==RIO_FALSE)
	      {
               handle->pe_Pl_Tx_Illegal_Pnacc_Resend_Cnt=handle->inst_Param_Set.tx_Illegal_Pnacc_Resend_Cnt;
              }	
          /*GDA: Bug#43 Fix */
 
		/* try to load new entry */
                RIO_PL_Out_Load_Entry(handle);
            }
            break;

        /* intervention engine */
        case RIO_PL_OUT_INTERV:
            /*
             * intervention engine manage responses sending so we needn't
             * wait for a response, only deallocate entry and check 
             * if the last response has been acknowledged then
             * clear engine and speak outbound buffer
             */
            if (RIO_OUT_GET_INT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_PL_OUT_INTRV_ENGINE_LENGHT)
            {
                init_Buffer_Entry(RIO_OUT_GET_INT_ENG_ELEM(handle, index), 0);
                i_Max = 0;
                for (i = 0; i < RIO_PL_OUT_INTRV_ENGINE_LENGHT; i++)
                    if (RIO_OUT_GET_INT_ENG_ELEM(handle, i)->state != RIO_PL_OUT_INVALID)
                        i_Max = 1;
                    
                    if (!i_Max)
                    {
                        index = RIO_PL_OUT_QUEUE | RIO_OUT_GET_INT_ENG(handle).source_Index;
                        init_Int_Engine(&RIO_OUT_GET_INT_ENG(handle));
                        RIO_PL_Out_Packet_Ack(handle, index, RIO_TRUE);
                    }
            }
            break;

        /* split engine */
        case RIO_PL_OUT_SPLIT:
            /* change state */
            if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt)
            {
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_ACKED;
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->pl_Retry_Cnt = 0;
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->time_After_Acked = 0;
                
                /* requests which doesn't demant any responses can be splitted as well*/
                if (packet_Need_Response(
                    RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ftype,
                    RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ttype
                    ) == RIO_ERROR)
                {
                    /* check if all requests has been acknowledged */
                    i_Max = 0;
                    for (i = 0; i < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt; i++)
                        if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->state != RIO_PL_OUT_ACKED)
                        {
                            i_Max = 1;
                            break;
                        }
                        /* all requests have been acked */
                        if (!i_Max)
                        {
                            index = RIO_PL_OUT_QUEUE | RIO_OUT_GET_SPLIT_ENG(handle).source_Index;
                            init_Split_Engine(&RIO_OUT_GET_SPLIT_ENG(handle));
                            RIO_PL_Out_Packet_Ack(handle, index, RIO_TRUE);
                        }
                }
            }
            break;

        /* multicast engine */
        case RIO_PL_OUT_MULTI:
            /* change state */
            if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_OUT_GET_MULT_ENG(handle).entries_Cnt)
            {
                RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_ACKED;
                RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->pl_Retry_Cnt = 0;
                RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->time_After_Acked = 0;
            }
            break;

        default:
            ;
    }
}

/***************************************************************************
 * Function : packet_Need_Response
 *
 * Description: check if a response is waited for certain packet
 *
 * Returns: RIO_OK - yes, RIO_ERROR - no
 *
 * Notes: none
 *
 **************************************************************************/
static int packet_Need_Response(unsigned int ftype, unsigned int ttype)
{
    /* responses do not need in responses */
    if (RIO_PL_Is_Req(ftype, ttype) == RIO_ERROR)
        return RIO_ERROR;

    /* port write doesn't need in response */
    if (ftype == RIO_MAINTENANCE && ttype == RIO_MAINTENANCE_PORT_WRITE)
        return RIO_ERROR;

    if (ftype == RIO_STREAM_WRITE)
        return RIO_ERROR;

    if (ftype == RIO_WRITE && ttype == RIO_WRITE_NWRITE)
        return RIO_ERROR;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    if (ftype == RIO_DS)
        return RIO_ERROR;
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    if (ftype == RIO_FC)
        return RIO_ERROR;
    /* GDA: Bug#125 */

    /* else the response is waited for */
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Out_Packet_Time_Out
 *
 * Description: notify the buffer about physical layer time-out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Packet_Time_Out(
    RIO_PL_DS_T *handle, 
    unsigned int packet_Handle,
    unsigned long time
    )
{
    /* source and the index */
    unsigned int index, src_Index;
    /* tag of request or response */
    unsigned int tag, arbiter_Src;

    if (handle == NULL)
        return;

    /* initialize variables */
    src_Index = packet_Handle & RIO_PL_OUT_SRC_MASK;
    index = packet_Handle & RIO_PL_OUT_INDEX_MASK;

    switch (src_Index)
    {
        /* outbound queue */
        case RIO_PL_OUT_QUEUE:
        {
            unsigned int queue_Ind = 0, i, i_Max;

            /* detect corresponding queue element*/
            i_Max = RIO_PL_Out_Get_Buf_Size(handle);
            for (i = 0; i < i_Max; i++)
                if (RIO_OUT_GET_QUEUE_ELEM(handle, i)->index == index)
                {
                    queue_Ind = i;
                    break;
                }

            tag = RIO_OUT_GET_ENTRY(handle, index)->trx_Tag;
            /* print warning */
            if (RIO_PL_Is_Req(
                RIO_OUT_GET_ENTRY(handle, index)->entry.ftype,
                RIO_OUT_GET_ENTRY(handle, index)->entry.ttype
                ) == RIO_OK)
                RIO_PL_Print_Message(handle, RIO_PL_ERROR_56, (unsigned long)tag, time);
            else
                RIO_PL_Print_Message(handle, RIO_PL_ERROR_57, (unsigned long)tag, time);

            /*delete rtry count from the model*/
            /* check if we overstepped the limit */
/*            if (RIO_OUT_GET_ENTRY(handle, index)->pl_Retry_Cnt >= 
                handle->parameters_Set.physical_Retry_Count)
            {
*/
                /* discard the packet and remove it from queue*/
                out_Queue_Shift(handle, queue_Ind);
                if (packet_Need_Response(
                    RIO_OUT_GET_ENTRY(handle, index)->entry.ftype,
                    RIO_OUT_GET_ENTRY(handle, index)->entry.ttype
                    ) == RIO_OK)
                {
                    /*delete error message about retry count
                    RIO_PL_Print_Message(handle, RIO_PL_ERROR_28, 
                        (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->pl_Retry_Cnt, 
                        (unsigned long)tag); 
                    */
                   
                    init_Buffer_Entry(RIO_OUT_GET_ENTRY(handle, index), 
                        RIO_OUT_GET_ENTRY(handle, index)->res_For_Prio);

                    /* notify LL about packet discarding */
                    handle->ext_Interfaces.rio_PL_Req_Time_Out(
                        handle->ext_Interfaces.pl_Interface_Context,
                        tag);
                }
                else
                {
                    /* init entry*/
                    /*
                    RIO_PL_Print_Message(handle, RIO_PL_ERROR_29, 
                        (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->pl_Retry_Cnt, 
                        (unsigned long)tag);  
                    */
                    init_Buffer_Entry(RIO_OUT_GET_ENTRY(handle, index), 
                        RIO_OUT_GET_ENTRY(handle, index)->res_For_Prio);
                }
                /*try to load new packet*/
                RIO_PL_Out_Load_Entry(handle);
/*            }
            else
            {
                RIO_OUT_GET_QUEUE_ELEM(handle, queue_Ind)->state = RIO_PL_QUEUE_STATE_READY;
                RIO_OUT_GET_ENTRY(handle, index)->state = RIO_PL_OUT_READY;
                RIO_OUT_GET_ENTRY(handle, index)->pl_Retry_Cnt++;
            }
*/
            break;
        }

        /* intervention engine */
        case RIO_PL_OUT_INTERV:
            /*
             * we check if the limit is overstepped and if yes
             * init 
             */
            if (RIO_OUT_GET_INT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_PL_OUT_INTRV_ENGINE_LENGHT)
            {
                tag = RIO_OUT_GET_INT_ENG(handle).source_Index;
                
                /* check if we overstepped the limit */
/*                if (RIO_OUT_GET_INT_ENG_ELEM(handle, index)->pl_Retry_Cnt >= 
                    handle->parameters_Set.physical_Retry_Count)
                {
*/
                    /* index of current transmitted packet*/
                    arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;
                    
                    RIO_OUT_GET_ENTRY(handle, tag)->pl_Retry_Cnt = 
                        RIO_OUT_GET_INT_ENG_ELEM(handle, index)->pl_Retry_Cnt;
                    init_Int_Engine(&RIO_OUT_GET_INT_ENG(handle));
                    
                    /* one packet from engine is transmitting but engine
                    * has to be initialized
                    */
                    if (arbiter_Src == RIO_PL_OUT_INTERV)
                        handle->tx_Data.need_STOMP = RIO_TRUE;
                    RIO_PL_Out_Packet_Time_Out(handle, RIO_PL_OUT_QUEUE | tag, time);
/*                }
                else
                {
                    tag = RIO_OUT_GET_ENTRY(handle, tag)->trx_Tag;
                    RIO_PL_Print_Message(handle, RIO_PL_WARNING_5, (unsigned long)tag, time);
                    RIO_OUT_GET_INT_ENG_ELEM(handle, index)->pl_Retry_Cnt++;
                    RIO_OUT_GET_INT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
                }
*/
            }
            break;

        /* multicast engine */
        case RIO_PL_OUT_MULTI:
            if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_OUT_GET_MULT_ENG(handle).entries_Cnt)
            {
                tag = RIO_OUT_GET_MULT_ENG(handle).source_Index;
                
                /* check if we overstepped the limit */
/*              if (RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->pl_Retry_Cnt >= 
                    handle->parameters_Set.physical_Retry_Count)
                {
*/                    
                /* source of transmitting packet */
                    arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;
                    
                    RIO_OUT_GET_ENTRY(handle, tag)->pl_Retry_Cnt = 
                        RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->pl_Retry_Cnt;
                    init_Mult_Engine(&RIO_OUT_GET_MULT_ENG(handle));
                    
                    /* one packet from engine is transmitting but engine
                    * has to be initialized
                    */
                    if (arbiter_Src == RIO_PL_OUT_MULTI)
                        handle->tx_Data.need_STOMP = RIO_TRUE;
                    RIO_PL_Out_Packet_Time_Out(handle, RIO_PL_OUT_QUEUE | tag, time); 
/*                }
                else
                {
                    tag = RIO_OUT_GET_ENTRY(handle, tag)->trx_Tag;
                    RIO_PL_Print_Message(handle, RIO_PL_WARNING_5, (unsigned long)tag, time);
                    RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->pl_Retry_Cnt++;
                    RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
                }
*/
            }
            break;

        /* split engine */
        case RIO_PL_OUT_SPLIT:
            if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt)
            {
                tag = RIO_OUT_GET_SPLIT_ENG(handle).source_Index;
                
                /* check if we overstepped the limit */
/*                if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->pl_Retry_Cnt >= 
                    handle->parameters_Set.physical_Retry_Count)
                {
*/
                    /* source of transmitting packet */
                    arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;
                    
                    RIO_OUT_GET_ENTRY(handle, tag)->pl_Retry_Cnt = 
                        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->pl_Retry_Cnt;
                    init_Split_Engine(&RIO_OUT_GET_SPLIT_ENG(handle));
                    
                    /* one packet from engine is transmitting but engine
                    * has to be initialized
                    */
                    if (arbiter_Src == RIO_PL_OUT_SPLIT)
                        handle->tx_Data.need_STOMP = RIO_TRUE;
                    RIO_PL_Out_Packet_Time_Out(handle, RIO_PL_OUT_QUEUE | tag, time); 
/*                }
                else
                {
                    tag = RIO_OUT_GET_ENTRY(handle, tag)->trx_Tag;
                    RIO_PL_Print_Message(handle, RIO_PL_WARNING_5, (unsigned long)tag, time);
                    RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->pl_Retry_Cnt++;
                    RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
                }
*/
            }
            break;

        default:
            ;
    }
}

/***************************************************************************
 * Function : RIO_PL_Out_Packet_Discarded
 *
 * Description: notify the buffer about discarding packet at receiver side
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Packet_Discarded(
    RIO_PL_DS_T *handle, 
    unsigned int packet_Handle,
    RIO_BOOL_T is_PNA
    )
{
    /* source and the index */
    unsigned int index, src_Index;
    unsigned int i, i_Max;
 


    if (handle == NULL)
        return;

    src_Index = packet_Handle & RIO_PL_OUT_SRC_MASK;
    index = packet_Handle & RIO_PL_OUT_INDEX_MASK;

    /* check if it's a packet which we are transmitting */
    if (packet_Handle == RIO_OUT_GET_ARB(handle).index)
        RIO_PL_Out_Init_Arbiter(&RIO_OUT_GET_ARB(handle));

    
    switch (src_Index)
    {
        /* outbound queue */
        case RIO_PL_OUT_QUEUE:

            /* check if it's a packet which we are transmitting */
/*            if (packet_Handle == RIO_OUT_GET_ARB(handle).index)
                RIO_PL_Out_Init_Arbiter(&RIO_OUT_GET_ARB(handle));
*/
            i_Max = RIO_PL_Out_Get_Buf_Size(handle);
            
            for (i = 0; i < i_Max; i++)
                if (RIO_OUT_GET_QUEUE_ELEM(handle, i)->index == index)
                {
			if (is_PNA == RIO_TRUE)
			{
                          /*if ((RIO_OUT_GET_ENTRY(handle, index)->packet_Resend_Count == 0) && 
                   	      (handle->is_PLLight_Model == RIO_TRUE))-- Before Bug#43 Fix*/

                           
                             /*GDA: Bug43 Fix */
                             if (((RIO_OUT_GET_ENTRY(handle, index)->packet_Resend_Count == 0) && 
		                  (handle->is_PLLight_Model == RIO_TRUE)) ||
                                 ((handle->pe_Pl_Tx_Illegal_Pnacc_Resend_Cnt==0) &&
                                  (handle->is_PLLight_Model == RIO_FALSE) &&
                                  (handle->inst_Param_Set.gda_Tx_Illegal_Packet==RIO_TRUE))) 
                        	 {
#ifndef _CARBON_SRIO_XTOR
                                   printf("\npacket_Resend_Count=0, Dropping the illegal Packet\n");
#endif
				   RIO_PL_Out_Packet_Ack(handle, packet_Handle, RIO_FALSE);
				   return;
				 }
				else
                                 {
                                    /*GDA: Bug43 Fix (Start)*/

#ifndef _CARBON_SRIO_XTOR
                                    printf("\n<PLL>Decrementing packet_Resend_Count: D`%u\n",
				           RIO_OUT_GET_ENTRY(handle, index)->packet_Resend_Count);
#endif

                                    if ((handle->is_PLLight_Model == RIO_FALSE)&&
				        (handle->inst_Param_Set.gda_Tx_Illegal_Packet==RIO_TRUE))
                                      { 
#ifndef _CARBON_SRIO_XTOR
                                        printf("\n<PE/PL>Decrementing packet_Resend_Count: D`%u\n",
				           handle->pe_Pl_Tx_Illegal_Pnacc_Resend_Cnt);
#endif

                                           handle->pe_Pl_Tx_Illegal_Pnacc_Resend_Cnt--;
                                      }
                                    /*GDA: Bug43 Fix (End)  */

				    RIO_OUT_GET_ENTRY(handle, index)->packet_Resend_Count--;
                                 }
			}
                    RIO_OUT_GET_QUEUE_ELEM(handle, i)->state = RIO_PL_QUEUE_STATE_READY;
                    break;
                }

            /* if i == i_Max packet hasn't been found */
            if (i == i_Max)
                return;

         
            /* change state  */
            RIO_OUT_GET_ENTRY(handle, index)->state = RIO_PL_OUT_READY;
            break;

        case RIO_PL_OUT_INTERV:
            if (RIO_OUT_GET_INT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_PL_OUT_INTRV_ENGINE_LENGHT)
                RIO_OUT_GET_INT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
            break;

        case RIO_PL_OUT_MULTI:
            if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_OUT_GET_MULT_ENG(handle).entries_Cnt)
                RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
            break;

        case RIO_PL_OUT_SPLIT:
            if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_TRUE &&
                index < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt)
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
            break;

        default:
            ;
    }
}

/***************************************************************************
 * Function : RIO_PL_Out_Init
 *
 * Description: initialize outbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Init(
    RIO_PL_DS_T        *handle,
    RIO_PL_PARAM_SET_T *param_Set
    )
{
    unsigned int i, init_Prio = 3, count = 0; /* loop counter */

    if (handle == NULL || param_Set == NULL)
        return;

    /* clear buffer state */
    init_Buffer_Entry(&handle->outbound_Buf.temp, 0);

    /* 
     * initialize buffer with proper reservation for prio from 3 prio to 1
     * this initialization is making to get proper entries reservation under
     * corresponding priority value
     */
    for (i = 0; i < handle->inst_Param_Set.pl_Outbound_Buffer_Size; i++, count++)
    {
        /* check priority 3 reservation */
        if (init_Prio == 3 && count >= RIO_OUT_RES_FOR_3(handle))
        {
            init_Prio = 2;
            count = 0;
        }

        /* check priority 2 reservation */
        if (init_Prio == 2 && count >= RIO_OUT_RES_FOR_2(handle))
        {
            init_Prio = 1;
            count = 0;
        }

        /* check priority 1 reservation */
        if (init_Prio == 1 && count >= RIO_OUT_RES_FOR_1(handle))
            init_Prio = 0;

        init_Buffer_Entry(RIO_OUT_GET_ENTRY(handle, i), init_Prio);

        /* initialize sending queue */
        RIO_OUT_GET_QUEUE_ELEM(handle, i)->value = RIO_PL_QUEUE_ELEM_UNUSED;
        RIO_OUT_GET_QUEUE_ELEM(handle, i)->index = 0;
        RIO_OUT_GET_QUEUE_ELEM(handle, i)->state = RIO_PL_QUEUE_STATE_READY;

    }

    /* initialize arbiter */
    RIO_PL_Out_Init_Arbiter(&RIO_OUT_GET_ARB(handle));

    /* initialize intervention engine*/
    init_Int_Engine(&RIO_OUT_GET_INT_ENG(handle));

    /* intialize muticast engine */
    init_Mult_Engine(&RIO_OUT_GET_MULT_ENG(handle));

    /* initialize split engine */
    init_Split_Engine(&RIO_OUT_GET_SPLIT_ENG(handle));
}

/***************************************************************************
 * Function : RIO_PL_Out_Init_Arbiter
 *
 * Description: initialize arbiter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Init_Arbiter(RIO_PL_OUT_ARB_T *element)
{
    if (element == NULL)
        return;

    /*initialize arbiter fields */
    element->cur_Pos = 0;
    element->index = 0;
    element->packet_Len = 0;
}

/***************************************************************************
 * Function : init_Int_Engine
 *
 * Description: initialize intervention engine
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Int_Engine(RIO_PL_OUT_INTERV_ENG_T *engine)
{
    unsigned int i; /* loop counter */

    if (engine == NULL)
        return;

    /*initialize engine fields */
    engine->valid = RIO_FALSE;
    engine->source_Index = 0;

    /* initialize engine's entries one by one */
    for (i = 0; i < RIO_PL_OUT_INTRV_ENGINE_LENGHT; i++)
        init_Buffer_Entry(engine->entries + i, 0);
}

/***************************************************************************
 * Function : init_Split_Engine
 *
 * Description: initialize intervention engine
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Split_Engine(RIO_PL_OUT_SPLIT_ENG_T *engine)
{
    unsigned int i; /* loop counter */

    if (engine == NULL)
        return;

    /*initialize engine fields */
    engine->valid = RIO_FALSE;
    engine->source_Index = 0;
    engine->entries_Cnt = 0;

    /* initialize engine's entries one by one */
    for (i = 0; i < RIO_PL_OUT_SPLIT_ENGINE_LENGHT; i++)
        init_Buffer_Entry(engine->entries + i, 0);
}

/***************************************************************************
 * Function : init_Mult_Engine
 *
 * Description: initialize multicast engine
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Mult_Engine(RIO_PL_OUT_MULTICAST_ENG_T *engine)
{
    unsigned int i; /* loop counter */

    if (engine == NULL)
        return;

    /*initialize engine fields */
    engine->valid = RIO_FALSE;
    engine->source_Index = 0;
    engine->entries_Cnt = 0;

    /* initialize engine's entries one by one */
    for (i = 0; i < RIO_PL_OUT_MULTICAST_ENGINE_LENGHT; i++)
        init_Buffer_Entry(engine->entries + i, 0);
}

/***************************************************************************
 * Function : init_Buffer_Entry
 *
 * Description: initialize one outbound buffer entry
 *
 * Returns: none
 *
 * Notes: the only place for buffer entry initialization 
 *
 **************************************************************************/
static void init_Buffer_Entry(RIO_PL_OUT_ENTRY_T *element, unsigned int prio)
{
    unsigned int i; /* loop counter*/

    if (element == NULL)
        return;

    /* initialize general parameters */
    element->state = RIO_PL_OUT_INVALID;
    element->ll_Retry_Cnt = 0;
    element->pl_Retry_Cnt = 0;
    element->payload_Size = 0;
    element->payload_Source = NULL;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    element->payload_Source_hw = NULL;   /*bug-124*/
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    element->trx_Tag = 0;
    element->res_For_Prio = prio;
    element->time_After_Acked = 0;
    element->time_After_Got_From_LL = 0;
    element->max_Size = 0;
    element->offset = 0;
    element->multicast_Size = 0;
    element->resp_Cnt = 0;
    element->byte_Num = 0;
    element->byte_Offset = 0;

    /* 
     *initialize payload by 0xdeadbeaf value to see incorrect payload easy only
     */
    for (i = 0; i < RIO_PL_MAX_PAYLOAD_SIZE; i++)
        element->payload[i].ls_Word = element->payload[i].ms_Word = 0xdeadbeef;

   /* GDA: Bug#124 - Support for Data Streaming packet  added below IF Loop*/
     if(element->entry.ftype!= RIO_DS)
     {
    /*initialize payload by 0xdeadbeaf value to see incorrect payload easy only
     */
    for (i = 0; i < RIO_PL_MAX_PAYLOAD_SIZE; i++)
        element->payload[i].ls_Word = element->payload[i].ms_Word = 0xdeadbeef;
     }
     else
     {
    for (i = 0; i < RIO_PL_MAX_DS_PAYLOAD_SIZE; i++)
        element->payload_hw[i] = element->payload_hw[i]=(unsigned short) 0xdeadbeef;
     }
    

    /* initialize entry parameters */
    memset(&element->entry, '\0', sizeof(RIO_PL_OUTBOUND_RQ_T));
    element->entry.hop_Count = 0xFF;
}

/***************************************************************************
 * Function : RIO_PL_Out_Get_Buf_Size
 *
 * Description: return size of outbound buffer
 *
 * Returns: number
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_PL_Out_Get_Buf_Size(RIO_PL_DS_T *handle)
{
    if (handle == NULL)
        return 0;
    else 
        return handle->inst_Param_Set.pl_Outbound_Buffer_Size;
}

/***************************************************************************
 * Function : RIO_PL_Out_Get_Buf_Free
 *
 * Description: return count of free entries in the buffer
 *
 * Returns: number
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_PL_Out_Get_Buf_Free(RIO_PL_DS_T *handle, unsigned int value)
{
    unsigned int i, i_Max, count = 0; /* loop */

    if (handle == NULL)
        return 0;
    /* count the number of available entries in outbound buffer */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);
    for (i = 0; i < i_Max; i++)
        if (((value >= RIO_OUT_GET_ENTRY(handle, i)->res_For_Prio) ||
			(handle->is_PLLight_Model)) &&
            RIO_OUT_GET_ENTRY(handle, i)->state == RIO_PL_OUT_INVALID)
            count++;

    return count;
}

/***************************************************************************
 * Function : out_Queue_Add
 *
 * Description: add element to sending queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Queue_Add(RIO_PL_DS_T *handle, int prio, unsigned int index)
{
    unsigned int i, i_Max; /* loop  counter and limit value */

    if (handle == NULL)
        return;

    i_Max = RIO_PL_Out_Get_Buf_Size(handle);
    for (i = 0; i < i_Max; i++)
    {
        if (((prio > RIO_OUT_GET_QUEUE_ELEM(handle, i)->value) && (handle->is_PLLight_Model == RIO_FALSE)) ||
        ((RIO_OUT_GET_QUEUE_ELEM(handle, i)->value == RIO_PL_QUEUE_ELEM_UNUSED) && (handle->is_PLLight_Model)))
        {
            /* check if we need shift other elements*/
            if (RIO_OUT_GET_QUEUE_ELEM(handle, i)->value != RIO_PL_QUEUE_ELEM_UNUSED)
            {
                /* temporary */
                RIO_PL_QUEUE_ELEM_T temp, temp2;

                temp = *RIO_OUT_GET_QUEUE_ELEM(handle, i);
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->value = prio;
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->index = index;
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->state = RIO_PL_QUEUE_STATE_READY;

                for (++i; i < i_Max; i++)
                {
                    temp2 = *RIO_OUT_GET_QUEUE_ELEM(handle, i);
                    *RIO_OUT_GET_QUEUE_ELEM(handle, i) = temp;
                    temp = temp2;
                }
            }
            else
            {
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->value = prio;
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->index = index;
                RIO_OUT_GET_QUEUE_ELEM(handle, i)->state = RIO_PL_QUEUE_STATE_READY;
            }
            break;
        }
    }
}

/***************************************************************************
 * Function : out_Queue_Shift
 *
 * Description: shift queue to the left starting from the index
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Queue_Shift(RIO_PL_DS_T *handle, unsigned int pos)
{
    unsigned int i, i_Max; /* loop  counter and limit value */

    if (handle == NULL)
        return;

    i_Max = RIO_PL_Out_Get_Buf_Size(handle) - 1;

    /* shift queue */
    for (i = pos; i < i_Max; i++)
        *RIO_OUT_GET_QUEUE_ELEM(handle, i) = *RIO_OUT_GET_QUEUE_ELEM(handle, i + 1);

    /* clear the last element */
    RIO_OUT_GET_QUEUE_ELEM(handle, i)->value = RIO_PL_QUEUE_ELEM_UNUSED;
    RIO_OUT_GET_QUEUE_ELEM(handle, i)->index = 0;
}

/***************************************************************************
 * Function : pl_Out_Buf_Make_Stream
 *
 * Description: convert packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_Stream(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{

/*RIO_DW_T             payload[RIO_PL_MAX_PAYLOAD_SIZE];
    unsigned int         payload_Size; */

    if (entry == NULL || arbiter == NULL)
        return;

    switch(entry->entry.ftype)
    {
        /* maintanence packet request*/
        case RIO_MAINTENANCE:
            pl_Out_Buf_Make_8(ctray, entry, arbiter);
            break;

        case RIO_RESPONCE:
            pl_Out_Buf_Make_13(ctray, entry, arbiter);
            break;

        /* doorbell request */
        case RIO_DOORBELL:
            pl_Out_Buf_Make_10(entry, arbiter);
            break;

        /* message request */
        case RIO_MESSAGE:
            pl_Out_Buf_Make_11(ctray, entry, arbiter);
            break;

        /* nonintervention request */
        case RIO_NONINTERV_REQUEST:
            pl_Out_Buf_Make_2(entry, arbiter);
            break;

        /* write class */
        case RIO_WRITE:
            pl_Out_Buf_Make_5(ctray, entry, arbiter);
            break;

        /* stream write */
        case RIO_STREAM_WRITE:
            pl_Out_Buf_Make_6(ctray, entry, arbiter);
            break;

        case RIO_INTERV_REQUEST:
            pl_Out_Buf_Make_1(entry, arbiter);
            break;

	/* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	    pl_Out_Buf_Make_9(ctray, entry, arbiter);
            break;
	/* GDA: Bug#124 */

        /* GDA: Bug#125 - Support for Flow control packet */
        case RIO_FC:
            pl_Out_Buf_Make_7(ctray,entry, arbiter);
            break;
	/* GDA: Bug#125 */

    }
}

/***************************************************************************
 * Function : pl_Out_Buf_Make_8
 *
 * Description: convert maintanace packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_8(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 

    if (ctray == NULL || entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;
    

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i]  |= (RIO_UCHAR_T)(entry->size & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->src_TID & 0xff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->hop_Count & 0xff);
    if ((entry->ttype == RIO_MAINTENANCE_READ_RESPONCE ||
        entry->ttype == RIO_MAINTENANCE_WRITE_RESPONCE ) && (entry->is_PLLight_Model == RIO_FALSE))
        arbiter->packet_Stream[i] = 0xFF; /*The maximum hop_Count value should be set
                                            for maintenance responce packets*/ 
    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->config_Off & 0x1fe000) >> 13);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->config_Off & 0x001fe0) >> 5);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->config_Off & 0x1f) << 3);
    arbiter->packet_Stream[i++] |= (RIO_UCHAR_T)((entry->wdptr & 0x01) << 2);
    if ((entry->ttype == RIO_MAINTENANCE_READ_RESPONCE ||
        entry->ttype == RIO_MAINTENANCE_WRITE_RESPONCE ) && (entry->is_PLLight_Model == RIO_FALSE))

    {
        /*set reserved fields to 0*/
        arbiter->packet_Stream[i - 1] = 0;
        arbiter->packet_Stream[i - 2] = 0;
        arbiter->packet_Stream[i - 3] = 0;
    }

	if (entry->is_PLLight_Model == RIO_TRUE)
	    arbiter->packet_Stream[i - 1] |= (RIO_UCHAR_T)(entry->xamsbs & 0x03);

    /* data payload */
#ifndef DISABLE_PAYLOAD_CHECKING
    if ((RIO_PL_Is_Payload(entry->ftype, entry->ttype) == RIO_OK) || 
		(entry->is_PLLight_Model == RIO_TRUE))
#endif
    {
        unsigned int j = 0; /* loop counter */

        /* check if the payload wasn't already passed */
        if (entry_Out->payload_Source == NULL)
        {
            if(RIO_PL_Is_Req(entry->ftype, entry->ttype) == RIO_OK)
            {
                ctray->rio_PL_Get_Req_Data(
                    ctray->pl_Interface_Context,
                    entry_Out->trx_Tag,
                    entry_Out->offset,
                    (RIO_UCHAR_T)entry_Out->payload_Size,
                    entry_Out->payload);
            }
            else
            {
                ctray->rio_PL_Get_Resp_Data(
                    ctray->pl_Interface_Context,
                    entry_Out->trx_Tag,
                    0,
                    (RIO_UCHAR_T)entry_Out->payload_Size,
                    entry_Out->payload);
            }
        }

        /* convert payload to the stream */
        for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
            RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);
    }


    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */


    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
}

/***************************************************************************
 * Function : pl_Out_Buf_Make_10
 *
 * Description: convert doorbell packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_10(
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 

    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;

    

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* */
    if (entry->additional_Fields_Valid == RIO_TRUE)
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->rsrv_Ftype10 & 0xFF);
    else
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(0x00);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->src_TID & 0xff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->doorbell_Info & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->doorbell_Info & 0x00ff);


    i++;    
	if (entry->is_PLLight_Model == RIO_TRUE)
	{
        unsigned int j = 0; /* loop counter */

		/* convert payload to the stream */
		for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
			RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);

		/* check if we need to insert intermediate CRC */
		if (i > 80)
		{
			unsigned int k; /* loop counter*/
			RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
				tmp2 = arbiter->packet_Stream[81],
				tmp3, tmp4; /* temporaries */

			for (k = 82; k < i; k += 2)
			{
				tmp3 = arbiter->packet_Stream[k];
				tmp4 = arbiter->packet_Stream[k + 1];

				arbiter->packet_Stream[k] = tmp1;
				arbiter->packet_Stream[k + 1] = tmp2;

				tmp1 = tmp3;
				tmp2 = tmp4;
			}

			arbiter->packet_Stream[i] = tmp1;
			arbiter->packet_Stream[i + 1] = tmp2;

			/* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }
	}

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */


    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
}

/***************************************************************************
 * Function : pl_Out_Buf_Make_2
 *
 * Description: convert nonintervention packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_2(
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 

    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* ttype & size*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->size & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->src_TID & 0xff);

    /* extended address*/
    if (entry->ext_Address_Valid == RIO_TRUE)
    {
        if (entry->ext_Address_16 == RIO_FALSE)
        {
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0xff000000) >> 24);
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x00ff0000) >> 16);
        }

        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x0000ff00) >> 8);
        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)(entry->extended_Address & 0x000000ff);

    }

    /* address */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x1fe00000) >> 21);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x001fe000) >> 13);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x00001fe0) >> 5);

    /* address, wdptr & xambs */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x0000001f) << 3);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)((entry->wdptr & 0x01) << 2);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)(entry->xamsbs & 0x03);

    i++;    

#ifndef DISABLE_PAYLOAD_CHECKING
    if (entry->is_PLLight_Model == RIO_TRUE)
#endif
    {
        if (entry_Out != NULL)
        {
            if (entry_Out->payload_Source != NULL)
		    {
                unsigned int j;      /*payload counter*/

                /* convert payload to the stream */
                for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
                    RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);
            }
        }
    }

	if (entry->is_PLLight_Model == RIO_TRUE)
	{

		/* check if we need to insert intermediate CRC */
		if (i > 80)
		{
			unsigned int k; /* loop counter*/
			RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
				tmp2 = arbiter->packet_Stream[81],
				tmp3, tmp4; /* temporaries */

			for (k = 82; k < i; k += 2)
			{
				tmp3 = arbiter->packet_Stream[k];
				tmp4 = arbiter->packet_Stream[k + 1];

				arbiter->packet_Stream[k] = tmp1;
				arbiter->packet_Stream[k + 1] = tmp2;

				tmp1 = tmp3;
				tmp2 = tmp4;
			}

			arbiter->packet_Stream[i] = tmp1;
			arbiter->packet_Stream[i + 1] = tmp2;

			/* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }
	}

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */


    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
}

/***************************************************************************
 * Function : pl_Out_Buf_Make_1
 *
 * Description: convert intervention packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_1(
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 1 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 

    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* ttype & size*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->size & 0x0f);

    /* source TID*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->src_TID & 0xff);

    /* secondary ID*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->sec_ID & 0xff);

    /* secondary TID*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->sec_TID & 0xff);


    /* extended address*/
    if (entry->ext_Address_Valid == RIO_TRUE)
    {
        if (entry->ext_Address_16 == RIO_FALSE)
        {
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0xff000000) >> 24);
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x00ff0000) >> 16);
        }

        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x0000ff00) >> 8);
        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)(entry->extended_Address & 0x000000ff);

    }

    /* address */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x1fe00000) >> 21);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x001fe000) >> 13);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x00001fe0) >> 5);

    /* address, wdptr & xambs */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x0000001f) << 3);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)((entry->wdptr & 0x01) << 2);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)(entry->xamsbs & 0x03);

    
    i++;    
	if (entry->is_PLLight_Model == RIO_TRUE)
	{
        unsigned int j = 0; /* loop counter */

		/* convert payload to the stream */
		for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
			RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);

		/* check if we need to insert intermediate CRC */
		if (i > 80)
		{
			unsigned int k; /* loop counter*/
			RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
				tmp2 = arbiter->packet_Stream[81],
				tmp3, tmp4; /* temporaries */

			for (k = 82; k < i; k += 2)
			{
				tmp3 = arbiter->packet_Stream[k];
				tmp4 = arbiter->packet_Stream[k + 1];

				arbiter->packet_Stream[k] = tmp1;
				arbiter->packet_Stream[k + 1] = tmp2;

				tmp1 = tmp3;
				tmp2 = tmp4;
			}

			arbiter->packet_Stream[i] = tmp1;
			arbiter->packet_Stream[i + 1] = tmp2;

			/* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
		}
	}
    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */



    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
}

/***************************************************************************
 * Function : RIO_PL_Out_Conv_Dw_Char
 *
 * Description: convert double word to the stream
 *
 * Returns: none
 *
 * Notes: the only place for double-word to byte stream converting
 *
 **************************************************************************/
void RIO_PL_Out_Conv_Dw_Char(RIO_DW_T *dw, RIO_UCHAR_T *stream)
{
    /* 
     * used offsets and masks are unique and defined by endian mode and
     * structure defenition - they can be used only here in the sense of
     * doubleword encoding
     */
    unsigned int i, shift;
    unsigned long mask;

    if (dw == NULL || stream == NULL)
        return;

    /* convert the first word*/
    for (i = 0, mask = 0xff000000, shift = 24; i < 4; i++, shift -= 8, mask >>= 8)
        stream[i] = (RIO_UCHAR_T)((dw->ms_Word & mask) >> shift);

    /* convert the second word */
    for (mask = 0xff000000, shift = 24; i < 8; i++, shift -= 8, mask >>= 8)
        stream[i] = (RIO_UCHAR_T)((dw->ls_Word & mask) >> shift);
}

/***************************************************************************
 * Function : RIO_PL_Out_Compute_CRC
 *
 * Description: compute CRC code
 *
 * Returns: none
 *
 * Notes: the only place where CRC is counting (in according to the spec)
 *
 **************************************************************************/
void RIO_PL_Out_Compute_CRC(unsigned short *crc, RIO_UCHAR_T stream[], unsigned int i_Max)
{
    /* 
     * this routine count CRC for byte stream  
     * Used offsets and masks are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    
    /* previous control */
    RIO_UCHAR_T c0, c1, e0, e1; 
    /* CRC bits */
    RIO_UCHAR_T c00, c01, c02, c03, c04, c05, c06, c07, 
        c08, c09, c10, c11, c12, c13, c14, c15;
    /* data bits */
    RIO_UCHAR_T d00, d01, d02, d03, d04, d05, d06, d07, 
        d08, d09, d10, d11, d12, d13, d14, d15;

    /* loop control */
    unsigned int i;

    if (crc == NULL || stream == NULL)
        return;

    /* select controls */
    c0 = (RIO_UCHAR_T)((*crc & 0xff00) >> 8);
    c1 = (RIO_UCHAR_T)(*crc & 0x00ff); 

    for (i = 0; i < i_Max; i += 2)
    {
        /* c XOR d */
        e0 = (RIO_UCHAR_T)(c0 ^ stream[i]);
      /*  e1 = (RIO_UCHAR_T)(c1 ^ stream[i + 1]);*/
	
	/* GDA: Bug#124 - Support for Data Streaming packet */
	if(i+1 == i_Max)
        e1 = (RIO_UCHAR_T)(c1 ^ 0);
	else
        e1 = (RIO_UCHAR_T)(c1 ^ stream[i + 1]);
	/* GDA: Bug#124 - Support for Data Streaming packet */
	

        /* data bits selection*/
        d00 = (RIO_UCHAR_T)((e0 & 0x80) >> 7);
        d01 = (RIO_UCHAR_T)((e0 & 0x40) >> 6);
        d02 = (RIO_UCHAR_T)((e0 & 0x20) >> 5);
        d03 = (RIO_UCHAR_T)((e0 & 0x10) >> 4);
        d04 = (RIO_UCHAR_T)((e0 & 0x08) >> 3);
        d05 = (RIO_UCHAR_T)((e0 & 0x04) >> 2);
        d06 = (RIO_UCHAR_T)((e0 & 0x02) >> 1);
        d07 = (RIO_UCHAR_T)(e0 & 0x01);
        d08 = (RIO_UCHAR_T)((e1 & 0x80) >> 7);
        d09 = (RIO_UCHAR_T)((e1 & 0x40) >> 6);
        d10 = (RIO_UCHAR_T)((e1 & 0x20) >> 5);
        d11 = (RIO_UCHAR_T)((e1 & 0x10) >> 4);
        d12 = (RIO_UCHAR_T)((e1 & 0x08) >> 3);
        d13 = (RIO_UCHAR_T)((e1 & 0x04) >> 2);
        d14 = (RIO_UCHAR_T)((e1 & 0x02) >> 1);
        d15 = (RIO_UCHAR_T)(e1 & 0x01);

        /*  XOR equation network */
        c00 = (RIO_UCHAR_T)(d04 ^ d05 ^ d08 ^ d12);

        c01 = (RIO_UCHAR_T)(d05 ^ d06 ^ d09 ^ d13);

        c02 = (RIO_UCHAR_T)(d06 ^ d07 ^ d10 ^ d14);

        c03 = (RIO_UCHAR_T)(d00 ^ d07 ^ d08 ^ d11 ^ d15);

        c04 = (RIO_UCHAR_T)(d00 ^ d01 ^ d04 ^ d05 ^ d09);

        c05 = (RIO_UCHAR_T)(d01 ^ d02 ^ d05 ^ d06 ^ d10);

        c06 = (RIO_UCHAR_T)(d00 ^ d02 ^ d03 ^ d06 ^ d07 ^ d11);

        c07 = (RIO_UCHAR_T)(d00 ^ d01 ^ d03 ^ d04 ^ d07 ^ d08 ^ d12);

        c08 = (RIO_UCHAR_T)(d00 ^ d01 ^ d02 ^ d04 ^ d05 ^ d08 ^ d09 ^ d13);

        c09 = (RIO_UCHAR_T)(d01 ^ d02 ^ d03 ^ d05 ^ d06 ^ d09 ^ d10 ^ d14);

        c10 = (RIO_UCHAR_T)(d02 ^ d03 ^ d04 ^ d06 ^ d07 ^ d10 ^ d11 ^ d15);

        c11 = (RIO_UCHAR_T)(d00 ^ d03 ^ d07 ^ d11);

        c12 = (RIO_UCHAR_T)(d00 ^ d01 ^ d04 ^ d08 ^ d12);

        c13 = (RIO_UCHAR_T)(d01 ^ d02 ^ d05 ^ d09 ^ d13);

        c14 = (RIO_UCHAR_T)(d02 ^ d03 ^ d06 ^ d10 ^ d14);

        c15 = (RIO_UCHAR_T)(d03 ^ d04 ^ d07 ^ d11 ^ d15);

        /* compute CRC */
        c0 = (RIO_UCHAR_T)(c07 + (c06 << 1) + (c05 << 2) + (c04 << 3) + (c03 << 4) +
            (c02 << 5) + (c01 << 6) + (c00 << 7));

        c1 = (RIO_UCHAR_T)(c15 + (c14 << 1) + (c13 << 2) + (c12 << 3) + (c11 << 4) +
            (c10 << 5) + (c09 << 6) + (c08 << 7));
    }

    /* store calculated CRC */
    *crc = (unsigned short)(((c0 & 0xff) << 8) + (c1 & 0xff));
}

/***************************************************************************
 * Function : RIO_PL_Is_Req
 *
 * Description: detect if ttype is request ttype
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T RIO_PL_Is_Req(unsigned int ftype, unsigned int ttype)
{
    switch(ftype)
    {
        case RIO_MAINTENANCE:
            if (ttype == RIO_MAINTENANCE_CONF_READ ||
                ttype == RIO_MAINTENANCE_CONF_WRITE ||
                ttype == RIO_MAINTENANCE_PORT_WRITE)
                return RIO_OK;
            break;

        /* one break because of one result */
        case RIO_DOORBELL:
        case RIO_MESSAGE:
        case RIO_NONINTERV_REQUEST:
        case RIO_WRITE:
        case RIO_STREAM_WRITE:
        case RIO_INTERV_REQUEST:
        /* GDA: Bug#124 - Support for Data Streaming packet */
	case RIO_DS:
	/* GDA: Bug#125 - Support for Flow control packet */
	case RIO_FC:
            return RIO_OK;

        default:
            ;
    }

    return RIO_ERROR;
}

/***************************************************************************
 * Function : out_Time_Out_In_Buffer
 *
 * Description: perform logical layer time out in the outbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Time_Out_In_Buffer(RIO_PL_DS_T *handle, unsigned int index)
{
    unsigned int i_Max, tag, j; /* outbound buffer size */

    if (handle == NULL)
        return;

    /* size of the outbound buffer */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);

    /* write diagnostic */
    RIO_PL_Print_Message(handle, RIO_PL_WARNING_6, 
        (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->trx_Tag, 
        (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->time_After_Acked);


    /* increase count of time-out events and check */
/*    if (RIO_OUT_GET_ENTRY(handle, index)->ll_Retry_Cnt >=
        handle->parameters_Set.logical_Retry_Count)
    {
*/
        /* drop the transaction */
        tag = RIO_OUT_GET_ENTRY(handle, index)->trx_Tag;
                
        /* remove from the queue */
        for (j = 0; j < i_Max; j++)
            if (RIO_OUT_GET_QUEUE_ELEM(handle, j)->index == index)
            {
                out_Queue_Shift(handle, j);
                break;
            }
            
/*        RIO_PL_Print_Message(handle, RIO_PL_ERROR_30, (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->ll_Retry_Cnt,
            (unsigned long)tag);
*/
            
        init_Buffer_Entry(RIO_OUT_GET_ENTRY(handle, index), RIO_OUT_GET_ENTRY(handle, index)->res_For_Prio);
            
        handle->ext_Interfaces.rio_PL_Req_Time_Out(
            handle->ext_Interfaces.pl_Interface_Context,
            tag);
            
        RIO_PL_Out_Load_Entry(handle);
/*    }
    else
    {
        unsigned int j;
*/
        /* reshedule the packet for sending */
/*        for (j = 0; j < i_Max; j++)
            if (RIO_OUT_GET_QUEUE_ELEM(handle, j)->index == index)
            {
                RIO_OUT_GET_QUEUE_ELEM(handle, j)->state = RIO_PL_QUEUE_STATE_READY;
                break;
            }
*/
            
        /* reshedule transaction for sending again */
/*        RIO_OUT_GET_ENTRY(handle, index)->ll_Retry_Cnt++;
        RIO_OUT_GET_ENTRY(handle, index)->state = RIO_PL_OUT_READY;
        RIO_OUT_GET_ENTRY(handle, index)->pl_Retry_Cnt = 0;
        RIO_OUT_GET_ENTRY(handle, index)->resp_Cnt = 0;
    }
*/
}
/***************************************************************************
 * Function : out_Time_Out_In_Buffer_Errata_14_0503
 *
 * Description: perform logical layer time out in the outbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Time_Out_In_Buffer_Errata_14_0503(RIO_PL_DS_T *handle, unsigned int index)
{
    unsigned int i_Max, tag, j, arbiter_Src; /* outbound buffer size */

    if (handle == NULL)
        return;

    /* size of the outbound buffer */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);

    /* write diagnostic */
    RIO_PL_Print_Message(handle, RIO_PL_WARNING_6, 
        (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->trx_Tag, 
        (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->time_After_Got_From_LL);

        /* drop the transaction */
        tag = RIO_OUT_GET_ENTRY(handle, index)->trx_Tag;

        if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_TRUE && tag == RIO_OUT_GET_SPLIT_ENG(handle).source_Index)
        {
            /* source of transmitting transaction */
            arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;

            init_Split_Engine(&RIO_OUT_GET_SPLIT_ENG(handle));

            if (arbiter_Src == RIO_PL_OUT_SPLIT)
                handle->tx_Data.need_STOMP = RIO_TRUE;
        }
        /*remove from the multicast engine in case of such*/
        else if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_TRUE && tag == RIO_OUT_GET_MULT_ENG(handle).source_Index)
        {
             /* source of transmitting transaction */
            arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;

            init_Mult_Engine(&RIO_OUT_GET_MULT_ENG(handle));

            if (arbiter_Src == RIO_PL_OUT_MULTI)
                handle->tx_Data.need_STOMP = RIO_TRUE;

        }

                
        /* remove from the queue */
        for (j = 0; j < i_Max; j++)
            if (RIO_OUT_GET_QUEUE_ELEM(handle, j)->index == index)
            {
                out_Queue_Shift(handle, j);
                break;
            }
            
/*        RIO_PL_Print_Message(handle, RIO_PL_ERROR_30, (unsigned long)RIO_OUT_GET_ENTRY(handle, index)->ll_Retry_Cnt,
            (unsigned long)tag);
*/
            
        init_Buffer_Entry(RIO_OUT_GET_ENTRY(handle, index), RIO_OUT_GET_ENTRY(handle, index)->res_For_Prio);
            
        handle->ext_Interfaces.rio_PL_Req_Time_Out(
            handle->ext_Interfaces.pl_Interface_Context,
            tag);
            
        RIO_PL_Out_Load_Entry(handle);

}
/***************************************************************************
 * Function : out_Time_Out_Split
 *
 * Description: count logical layer time out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Time_Out_Split(RIO_PL_DS_T *handle, unsigned int index)
{
    unsigned int tag, arbiter_Src;

    if (handle == NULL || index >= RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt)
        return;

    tag = RIO_OUT_GET_SPLIT_ENG(handle).source_Index;

    /* increase count of time-out events and check */
/*    if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->ll_Retry_Cnt >=
        handle->parameters_Set.logical_Retry_Count)
    {
*/
        /* source of transmitting transaction */
        arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;

/*        RIO_OUT_GET_ENTRY(handle, tag)->ll_Retry_Cnt = RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->ll_Retry_Cnt;*/
        RIO_OUT_GET_ENTRY(handle, tag)->time_After_Acked = RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->time_After_Acked;

        init_Split_Engine(&RIO_OUT_GET_SPLIT_ENG(handle));

        if (arbiter_Src == RIO_PL_OUT_SPLIT)
            handle->tx_Data.need_STOMP = RIO_TRUE;

        out_Time_Out_In_Buffer(handle, tag);
/*    }
    else
    {
*/
        /* write diagnostic */
       /* RIO_PL_Print_Message(handle, RIO_PL_WARNING_6, 
            (unsigned long)RIO_OUT_GET_ENTRY(handle, tag)->trx_Tag, 
            (unsigned long)RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->time_After_Acked);*/

        /* reshedule transaction for sending again */
/*        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->ll_Retry_Cnt++;
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->pl_Retry_Cnt = 0;
    }
*/
}

/***************************************************************************
 * Function : out_Time_Out_Mult
 *
 * Description: count logical layer time out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Time_Out_Mult(RIO_PL_DS_T *handle, unsigned int index)
{
    unsigned int tag, arbiter_Src;

    if (handle == NULL || index >= RIO_OUT_GET_MULT_ENG(handle).entries_Cnt)
        return;

    tag = RIO_OUT_GET_MULT_ENG(handle).source_Index;

    /* increase count of time-out events and check */
 /*   if (RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->ll_Retry_Cnt >=
        handle->parameters_Set.logical_Retry_Count)
    {
*/
        /* source of transmitting transaction */
        arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;

/*        RIO_OUT_GET_ENTRY(handle, tag)->ll_Retry_Cnt = RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->ll_Retry_Cnt;*/
        RIO_OUT_GET_ENTRY(handle, tag)->time_After_Acked = RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->time_After_Acked;

        init_Mult_Engine(&RIO_OUT_GET_MULT_ENG(handle));

        if (arbiter_Src == RIO_PL_OUT_MULTI)
            handle->tx_Data.need_STOMP = RIO_TRUE;

        out_Time_Out_In_Buffer(handle, tag);
/*    }
    else
    {*/
        /* write diagnostic */
      /*  RIO_PL_Print_Message(handle, RIO_PL_WARNING_6, 
            (unsigned long)RIO_OUT_GET_ENTRY(handle, tag)->trx_Tag, 
            (unsigned long)RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->time_After_Acked);*/
    
        /* reshedule transaction for sending again */
/*        RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->ll_Retry_Cnt++;
        RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
        RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->pl_Retry_Cnt = 0;
    }
*/
}

/***************************************************************************
 * Function : RIO_PL_Out_Log_Time_Out
 *
 * Description: count logical layer time out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Log_Time_Out(RIO_PL_DS_T *handle)
{
    unsigned int i, i_Max; /* loop counter and limit value*/

    if (handle == NULL)
        return;

    /* if LL time-out is 0 we don't check it at all*/
    if (!handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out)
        return;

    /* size of the outbound buffer */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);

    for (i = 0; i < i_Max; i++)
    {
        if (RIO_OUT_GET_ENTRY(handle, i)->state == RIO_PL_OUT_ACKED)
        {
            /* increase time after acked and check time-out */
            if (RIO_OUT_GET_ENTRY(handle, i)->time_After_Acked >=
                handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out)
                out_Time_Out_In_Buffer(handle, i);
            else
                RIO_OUT_GET_ENTRY(handle, i)->time_After_Acked++;
        }
    } /* look through all entries in outbound buffer*/
    

    /* look through split engine */
    if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_TRUE &&
        packet_Need_Response(
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, 0)->entry.ftype,
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, 0)->entry.ttype) == RIO_OK)
    {
        for (i = 0; i < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt; i++)
        {
            /* increase time after acked and check time-out */
            if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->state == RIO_PL_OUT_ACKED)
            {
                if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->time_After_Acked >=
                    handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out)
                    out_Time_Out_Split(handle, i);
                else
                    RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->time_After_Acked++;
            }
        }
    }
    /* look through multicast engine it always needs in response */
    if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_TRUE)
    {
        for (i = 0; i < RIO_OUT_GET_MULT_ENG(handle).entries_Cnt; i++)
        {
            /* increase time after acked and check time-out */
            if (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->state == RIO_PL_OUT_ACKED)
            {
                if (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->time_After_Acked >=
                    handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out)
                    out_Time_Out_Mult(handle, i);
                else
                    RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->time_After_Acked++;
            }

        }

    }
}

/***************************************************************************
 * Function : RIO_PL_Out_Log_Time_Out_Errta_14_0503
 *
 * Description: count logical layer time out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Log_Time_Out_Errata_14_0503(RIO_PL_DS_T *handle)
{
    unsigned int i, i_Max; /* loop counter and limit value*/

    if (handle == NULL)
        return;

    /* if LL time-out is 0 we don't check it at all*/
    if (!handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out)
        return;

    /* size of the outbound buffer */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);
    
    for (i = 0; i < i_Max; i++)
    {
        if (RIO_OUT_GET_ENTRY(handle, i)->state != RIO_PL_OUT_INVALID)
        {
            /* increase time after acked and check time-out */
            if (RIO_OUT_GET_ENTRY(handle, i)->time_After_Got_From_LL >=
                handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out)
                out_Time_Out_In_Buffer_Errata_14_0503(handle, i);
            else
                RIO_OUT_GET_ENTRY(handle, i)->time_After_Got_From_LL++;
        }
    } /* look through all entries in outbound buffer*/

   
}

/***************************************************************************
 * Function : pl_Out_Buf_Make_13
 *
 * Description: convert 13 packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_13(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 13 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 

  
    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->status & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->src_TID & 0xff);
    i++;

    /* data payload */
#ifndef DISABLE_PAYLOAD_CHECKING
    if ((entry->ttype == RIO_RESPONCE_WITH_DATA )|| 
		(entry->is_PLLight_Model == RIO_TRUE))
#endif
    {
        unsigned int j = 0; /* loop counter */

        /* check if the payload wasn't already passed */
        if (entry_Out->payload_Source == NULL)
            ctray->rio_PL_Get_Resp_Data(
                ctray->pl_Interface_Context,
                entry_Out->trx_Tag,
                0,
                (RIO_UCHAR_T)entry_Out->payload_Size,
                entry_Out->payload);


        /* convert payload to the stream */
        for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
            RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

    i++;

    /* check if we need to pad packet */
    if (i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;

}

/***************************************************************************
 * Function : pl_Out_Buf_Make_11
 *
 * Description: convert message packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_11(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 11 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int j = 0; /* loop counter */
    unsigned int first_Crc_Pos = 0;

    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* msglen & ssize*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->msglen & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->size & 0x0f);

    /* letter, mbox & msgseg */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->letter & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->mbox & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->msgseg & 0x0f);
    i++;

    /* check if the payload wasn't already passed */
    if (entry_Out->payload_Source == NULL)
        ctray->rio_PL_Get_Req_Data(
        ctray->pl_Interface_Context,
        entry_Out->trx_Tag,
        0,
        (RIO_UCHAR_T)entry_Out->payload_Size,
        entry_Out->payload);
    
    /* convert payload to the stream */
    for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
        RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

    i++;

    /* check if we need to pad packet */
    if (i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;

}

/***************************************************************************
 * Function : pl_Out_Buf_Make_5
 *
 * Description: convert write packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_5(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 5 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int j = 0; /* loop counter */
    unsigned int first_Crc_Pos = 0;

    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* ttype & size*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->size & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->src_TID & 0xff);

    /* extended address*/
    if (entry->ext_Address_Valid == RIO_TRUE)
    {
        if (entry->ext_Address_16 == RIO_FALSE)
        {
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0xff000000) >> 24);
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x00ff0000) >> 16);
        }

        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x0000ff00) >> 8);
        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)(entry->extended_Address & 0x000000ff);

    }           

    /* address */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x1fe00000) >> 21);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x001fe000) >> 13);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x00001fe0) >> 5);

    /* address, wdptr & xambs */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x0000001f) << 3);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)((entry->wdptr & 0x01) << 2);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)(entry->xamsbs & 0x03);
    i++;

    /* check if the payload wasn't already passed */
    if (entry_Out->payload_Source == NULL)
        ctray->rio_PL_Get_Req_Data(
        ctray->pl_Interface_Context,
        entry_Out->trx_Tag,
        entry_Out->offset,
        (RIO_UCHAR_T)entry_Out->payload_Size,
        entry_Out->payload);
    
    /* convert payload to the stream */
    for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
        RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
#ifndef _CARBON_SRIO_XTOR
		printf("\n check ...data corrupetd flag was set \n");
#endif
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

    i++;

    /* check if we need to pad packet */
    if (i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;

}

/***************************************************************************
 * Function : pl_Out_Buf_Make_6
 *
 * Description: convert stream write packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_6(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 5 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int j = 0; /* loop counter */
    unsigned int first_Crc_Pos = 0;

    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

    /* extended address*/
    if (entry->ext_Address_Valid == RIO_TRUE)
    {
        if (entry->ext_Address_16 == RIO_FALSE)
        {
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0xff000000) >> 24);
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x00ff0000) >> 16);
        }

        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x0000ff00) >> 8);
        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)(entry->extended_Address & 0x000000ff);

    }

    /* address */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x1fe00000) >> 21);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x001fe000) >> 13);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x00001fe0) >> 5);

    /* address, wdptr & xambs */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x0000001f) << 3);
 
    /*for packet type 6 wdptr field shall be set at 0*/
    if (entry->additional_Fields_Valid == RIO_TRUE)
        arbiter->packet_Stream[i] |=
                (RIO_UCHAR_T)((entry->rsrv_Ftype6 & 1) << 2);
    else 
        arbiter->packet_Stream[i] &=
                    (RIO_UCHAR_T)(0xFB);

    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)(entry->xamsbs & 0x03); 
    i++;

    /* check if the payload wasn't already passed */
    if (entry_Out->payload_Source == NULL)
        ctray->rio_PL_Get_Req_Data(
        ctray->pl_Interface_Context,
        entry_Out->trx_Tag,
        entry_Out->offset,
        (RIO_UCHAR_T)entry_Out->payload_Size,
        entry_Out->payload);
    
    /* convert payload to the stream */
    for (j = 0; j < entry_Out->payload_Size; j++, i += 8)
        RIO_PL_Out_Conv_Dw_Char(entry_Out->payload + j, arbiter->packet_Stream + i);

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

    i++;

    /* check if we need to pad packet */
    if (i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;

}

/***************************************************************************
 * Function : out_Rec_Buffer_Resp
 *
 * Description: perform response come to outbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Rec_Buffer_Resp(RIO_PL_DS_T *handle, RIO_RESPONSE_T *ext_Resp, unsigned int index)
{
    unsigned int tag;
    unsigned int j;
    unsigned int need_Clear = 1; /* flag to deallocate entry */
    
    /* check parameters */
    if (handle == NULL || index >= handle->inst_Param_Set.pl_Outbound_Buffer_Size || ext_Resp == NULL)
        return;

    /* detect tag*/
    tag = RIO_OUT_GET_ENTRY(handle, index)->trx_Tag;

    /* additional check for intervention support */
    if ((ext_Resp->status == RIO_RESPONCE_STATUS_DONE_INTERV ||
        ext_Resp->status == RIO_RESPONCE_STATUS_DATA_ONLY) &&
        RIO_OUT_GET_ENTRY(handle, index)->resp_Cnt != RIO_PL_CNT_OF_INT_RESP)
    {
        RIO_OUT_GET_ENTRY(handle, index)->resp_Cnt = RIO_PL_CNT_OF_INT_RESP;
        need_Clear = 0;
    }

    /* clear the queue*/
    if (need_Clear)
    {
        for (j = 0; j < handle->inst_Param_Set.pl_Outbound_Buffer_Size; j++)
            if (RIO_OUT_GET_QUEUE_ELEM(handle, j)->index == index)
            {
                out_Queue_Shift(handle, j);
                break;
            }
            
        init_Buffer_Entry(RIO_OUT_GET_ENTRY(handle, index), RIO_OUT_GET_ENTRY(handle, index)->res_For_Prio);
    }
        
    /* notify the environment about response arriving */
    handle->ext_Interfaces.rio_PL_Remote_Response(
        handle->ext_Interfaces.pl_Interface_Context,
        tag,
        ext_Resp);
        
    /* try to load new packet */
    if (need_Clear)
        RIO_PL_Out_Load_Entry(handle);
}

/***************************************************************************
 * Function : out_Rec_Split_Resp
 *
 * Description: perform response come to outbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Rec_Split_Resp(RIO_PL_DS_T *handle, RIO_RESPONSE_T *ext_Resp, unsigned int index)
{
    unsigned int bad_Resp = 0, arbiter_Src, i, i_Max;


    /* check parameters */
    if (handle == NULL || ext_Resp == NULL || RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_FALSE ||
        index >= RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt)
        return;
    /* 
     * response with  with status other than DONE or with different payload size
     */
    if (ext_Resp->dw_Num != RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->payload_Size  &&
        (RIO_PL_Is_Payload(
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ftype,
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ttype) == RIO_ERROR))
        bad_Resp = 1;
    
    if (ext_Resp->dw_Num &&
        (RIO_PL_Is_Payload(
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ftype,
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ttype) == RIO_OK))
        bad_Resp = 1;

    /* check status */
    if (ext_Resp->status != RIO_RESPONCE_STATUS_DONE) 
        bad_Resp = 1;

    /* check compativity between request and response ftypes */
    if (ext_Resp->ftype == RIO_MAINTENANCE &&
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ftype != RIO_MAINTENANCE)
        bad_Resp = 1;

    if (ext_Resp->ftype != RIO_MAINTENANCE &&
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ftype == RIO_MAINTENANCE)
        bad_Resp = 1;

    if (ext_Resp->ftype != RIO_RESPONCE &&
        RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ftype != RIO_MAINTENANCE)
        bad_Resp = 1;

    /* check MAINTENANCE */
    if (ext_Resp->ftype == RIO_MAINTENANCE)
    {
        if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ttype == RIO_MAINTENANCE_CONF_READ &&
            ext_Resp->transaction != RIO_MAINTENANCE_READ_RESPONCE)
            bad_Resp = 1;

        if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ttype == RIO_MAINTENANCE_CONF_WRITE &&
            ext_Resp->transaction != RIO_MAINTENANCE_WRITE_RESPONCE)
            bad_Resp = 1;
    }
    else
    {
        if (RIO_PL_Is_Payload(RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ftype,
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->entry.ttype) == RIO_OK)
        {
            if (ext_Resp->transaction != RIO_RESPONCE_WO_DATA)
                bad_Resp = 1;
        }
        else
        {
            if (ext_Resp->transaction != RIO_RESPONCE_WITH_DATA)
                bad_Resp = 1;
        }
    }

    /* the response is bad */
    if (bad_Resp)
    {
        arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;

        /* detect if we need to STOMP current packet */
        if (arbiter_Src == RIO_PL_OUT_SPLIT)
            handle->tx_Data.need_STOMP = RIO_TRUE;

        /* ask the outbound buffer about finishing */
        arbiter_Src = RIO_OUT_GET_SPLIT_ENG(handle).source_Index;

        /* clear engine */
        init_Split_Engine(&RIO_OUT_GET_SPLIT_ENG(handle));

        out_Rec_Buffer_Resp(handle, ext_Resp, arbiter_Src);
        return;
    }

    /* if DONE we shall check if request hav been responsed*/
    RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_RESPONSED;
    if (ext_Resp->dw_Num)
    {
        /* hold payload */
        for (i = 0; i < ext_Resp->dw_Num; i++)
        {
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->payload[i].ls_Word = ext_Resp->data[i].ls_Word;
            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, index)->payload[i].ms_Word = ext_Resp->data[i].ms_Word;
        }
    }

    /* 
     * after getting DONE response check if it's the last response we need in 
     * arbiter_Cnt is used as flag here
     */
    i_Max = RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt;
    arbiter_Src = 0;
    for (i = 0; i < i_Max; i++)
        if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->state != RIO_PL_OUT_RESPONSED)
            arbiter_Src = 1;

    /* all responses are got */
    if (!arbiter_Src)
    {
        /* pass the cumulative response to the buffer and clear engine 
         * arbiter_Cnt holds the buffer's entry index
         */
        RIO_RESPONSE_T cummul_Resp;
        unsigned int   src_Index = RIO_OUT_GET_SPLIT_ENG(handle).source_Index;
        RIO_DW_T       cummul_Data[RIO_PL_MAX_PAYLOAD_SIZE];

        cummul_Resp.data = NULL;
        cummul_Resp.transp_Type = ext_Resp->transp_Type;

        /* make cummulative response */
        if (RIO_PL_Is_Payload(RIO_OUT_GET_ENTRY(handle, src_Index)->entry.ftype,
            RIO_OUT_GET_ENTRY(handle, src_Index)->entry.ttype) == RIO_ERROR)
        {
            cummul_Resp.dw_Num = (RIO_UCHAR_T)RIO_OUT_GET_ENTRY(handle, src_Index)->payload_Size;
            cummul_Resp.data = cummul_Data;

            /* split or misalign ? */
            if (RIO_OUT_GET_ENTRY(handle, src_Index)->payload_Size > 1)
            {
                /* accumulate data */
                for (i = 0; i < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt; i++)
                {
                    unsigned int j; /* loop counter */
                    
                    for (j = 0; j < RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->payload_Size; j++)
                    {
                        cummul_Data[RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->offset + j] = 
                            RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->payload[j];
                    }
                }
            }
            else
            {
                unsigned long mask, offset;

                /* clean data */
                cummul_Data[0].ls_Word = 0;
                cummul_Data[0].ms_Word = 0;

                /* accumulate data */
                for (i = 0; i < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt; i++)
                {
                    offset = RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->byte_Offset;
                    offset = (offset >= 4) ? offset - 4 : offset;
                    
                    switch (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->byte_Num)
                    {
                        case 1:
                            mask = 0x000000ff;
                            mask <<= (3 - offset) * 8;
                            break;

                        case 2:
                            mask = 0x0000ffff;
                            mask <<= (2 - offset) * 8;
                            break;

                        default:
                            mask = 0xffffffff;
                    }

                    /* less significant word */
                    if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.wdptr)
                        cummul_Data[0].ls_Word += (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->payload[0].ls_Word & mask);
                    else
                        cummul_Data[0].ms_Word += (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->payload[0].ms_Word & mask);
                }
            }
        }
        else
        {
            cummul_Resp.dw_Num = 0;
            cummul_Resp.data = NULL;
        }

        /* forms other fields */
        cummul_Resp.ftype = ext_Resp->ftype;
        cummul_Resp.prio = ext_Resp->prio;
        cummul_Resp.sec_ID = 0;
        cummul_Resp.sec_TID = 0;
        cummul_Resp.target_TID = ext_Resp->target_TID;
        cummul_Resp.tr_Info = ext_Resp->tr_Info;
        cummul_Resp.status = ext_Resp->status;
        cummul_Resp.transaction = ext_Resp->transaction;

        /* pass response to LL and clear split engine */
        arbiter_Src = RIO_OUT_GET_SPLIT_ENG(handle).source_Index;
        init_Split_Engine(&RIO_OUT_GET_SPLIT_ENG(handle));
        out_Rec_Buffer_Resp(handle, &cummul_Resp, arbiter_Src);
    }
}

/***************************************************************************
 * Function : out_Rec_Multi_Resp
 *
 * Description: perform response come to outbound buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Rec_Multi_Resp(RIO_PL_DS_T *handle, RIO_RESPONSE_T *ext_Resp, unsigned int index)
{
    unsigned int arbiter_Src, i, i_Max;

    /* check parameters */
    if (handle == NULL || ext_Resp == NULL || RIO_OUT_GET_MULT_ENG(handle).valid == RIO_FALSE ||
        index >= RIO_OUT_GET_MULT_ENG(handle).entries_Cnt)
        return;

    /* 
     * response with data is an error response and with status other than
     * DONE and RETRY
     */
    if (ext_Resp->dw_Num ||
        (ext_Resp->status != RIO_RESPONCE_STATUS_RETRY && 
        ext_Resp->status != RIO_RESPONCE_STATUS_DONE))
    {
        arbiter_Src = RIO_OUT_GET_ARB(handle).index & RIO_PL_OUT_SRC_MASK;

        /* detect if we need to STOMP current packet */
        if (arbiter_Src == RIO_PL_OUT_MULTI)
            handle->tx_Data.need_STOMP = RIO_TRUE;

        /* ask the outbound buffer about finishing */
        arbiter_Src = RIO_OUT_GET_MULT_ENG(handle).source_Index;

        /* clear engine */
        init_Mult_Engine(&RIO_OUT_GET_MULT_ENG(handle));

        out_Rec_Buffer_Resp(handle, ext_Resp, arbiter_Src);
        return;
    }

    /* check if response is RETRY - retry request then */
    if (ext_Resp->status == RIO_RESPONCE_STATUS_RETRY)
    {
        RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_READY;
        return;
    }

    /* if DONE we shall check if request hav been responsed*/
    RIO_OUT_GET_MULT_ENG_ELEM(handle, index)->state = RIO_PL_OUT_RESPONSED;

    /* 
     * after getting DONE response check if it's the last response we need in 
     * arbiter_Cnt is used as flag here
     */
    i_Max = RIO_OUT_GET_MULT_ENG(handle).entries_Cnt;
    arbiter_Src = 0;
    for (i = 0; i < i_Max; i++)
        if (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->state != RIO_PL_OUT_RESPONSED)
            arbiter_Src = 1;

    /* all responses are got */
    if (!arbiter_Src)
    {
        /* pass the cumulative response to the buffer and clear engine 
         * arbiter_Cnt holds the buffer's entry index
         */
        arbiter_Src = RIO_OUT_GET_MULT_ENG(handle).source_Index;
        init_Mult_Engine(&RIO_OUT_GET_MULT_ENG(handle));
        out_Rec_Buffer_Resp(handle, ext_Resp, arbiter_Src);
    }
}


/***************************************************************************
 * Function : RIO_PL_Out_Rec_Resp
 *
 * Description: notify the buffer about arrived response
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Out_Rec_Resp(
    RIO_PL_DS_T    *handle, 
    RIO_RESPONSE_T *ext_Resp)
{
    unsigned int i, i_Max;

    if (handle == NULL || ext_Resp == NULL)
        return;

    /* check if we asked for this response */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);
    for(i = 0; i < i_Max; i++)
    {
      
      if ((RIO_OUT_GET_ENTRY(handle, i)->state == RIO_PL_OUT_ACKED ) &&    
           RIO_OUT_GET_ENTRY(handle, i)->entry.src_TID == ext_Resp->target_TID &&
            (
                /* prevent error in case when requested message transaction
                and transaction with the target tid equal to combination
                of message letter mbox and msgseg */
                (RIO_OUT_GET_ENTRY(handle, i)->entry.ftype != RIO_MESSAGE && 
                    ext_Resp->ftype == RIO_RESPONCE && 
                    ext_Resp->transaction != RIO_RESPONCE_MESSAGE ) ||
                (RIO_OUT_GET_ENTRY(handle, i)->entry.ftype == RIO_MESSAGE && 
                    ext_Resp->ftype == RIO_RESPONCE && 
                    ext_Resp->transaction == RIO_RESPONCE_MESSAGE ) ||
                (RIO_OUT_GET_ENTRY(handle, i)->entry.ftype != RIO_MESSAGE && 
                    ext_Resp->ftype != RIO_RESPONCE )
            )
        )
        {
            out_Rec_Buffer_Resp(handle, ext_Resp, i);
            return;
        }
    }

    /* look through split engine */
    if (RIO_OUT_GET_SPLIT_ENG(handle).valid == RIO_TRUE)
    {
        for (i = 0; i < RIO_OUT_GET_SPLIT_ENG(handle).entries_Cnt; i++)
            if (RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->state == RIO_PL_OUT_ACKED &&
                RIO_OUT_GET_SPLIT_ENG_ELEM(handle, i)->entry.src_TID == ext_Resp->target_TID)
            {
                out_Rec_Split_Resp(handle, ext_Resp, i);
                return;
            }
    }

    /* look through multicast engine */
    if (RIO_OUT_GET_MULT_ENG(handle).valid == RIO_TRUE)
    {
        for (i = 0; i < RIO_OUT_GET_MULT_ENG(handle).entries_Cnt; i++)
            if (RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->state == RIO_PL_OUT_ACKED &&
                RIO_OUT_GET_MULT_ENG_ELEM(handle, i)->entry.src_TID == ext_Resp->target_TID)
            {
                out_Rec_Multi_Resp(handle, ext_Resp, i);
                return;
            }
    }

    /* the source of this response hasn't been found */
          RIO_PL_Print_Message(handle, RIO_PL_ERROR_32, (unsigned long)ext_Resp->target_TID);
     
}

/***************************************************************************
 * Function : RIO_PL_Out_Assert_TID
 *
 * Description: detect new TID for a packet
 *
 * Returns: TID
 *
 * Notes: none
 *
 **************************************************************************/
unsigned char RIO_PL_Out_Assert_TID(RIO_PL_DS_T *handle)
{
    unsigned char ass_TID = 0;

    if (handle == NULL)
        return ass_TID;
    else
    {
    	/* ass_TID = handle->trans_Id;
        handle->tid = (unsigned char)((handle->tid + 1) % RIO_PL_MAX_TID_PLUS_ONE);
        return ass_TID; */
	    
        /* GDA:Bug#137 - passing transaction-id from the testbench */
        ass_TID = handle->trans_Id;
        handle->trans_Id = (unsigned int)((handle->trans_Id + 1) % RIO_PL_MAX_TID_PLUS_ONE);
	return ass_TID;
        /* GDA:Bug#137 - passing transaction-id from the testbench */
    }
}


/*****************************************************************************/


/* GDA: Bug#124 - Support for Data Streaming packet */

/*****************************************************************************
 *  Function : pl_Out_Buf_Make_9
 *  
 *  Description: convert DATA STREAM write packet structure to the stream
 *  Returns: none
 *  
 *  Notes: none
 *  
 ***************************************************************************/

static void pl_Out_Buf_Make_9(
	  RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
	  RIO_PL_OUT_ENTRY_T           *entry_Out, 
	  RIO_PL_OUT_ARB_T             *arbiter
        )
{
	    /* 
	     *     this routine makes bytes steram to type 9  packet in according to the 
	     *     specification. Used offsets and masks are unique in general case for 
	     *     this type of packet, so all using constant are unique and SHALL be 
	     *     changed in case of specification changing only HERE!!!
	     */
	    unsigned int i = 0; /* byte counter */
	    RIO_PL_OUTBOUND_RQ_T *entry; 
	    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
	    unsigned int j = 0,pad=0; /* loop counter */
	    unsigned int first_Crc_Pos = 0;

	     if (entry_Out == NULL || arbiter == NULL)
	        return;
	         
	     entry = &entry_Out->entry;
	     /* 0 byte already set in caller*/
	     i = 0;
 		entry->stream_ID.flow_ID = entry->src_TID;
	     
	     /* make 1 byte */
	     arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
	     arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
	     arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

	     /* make 2 byte */
	     if (entry->tt != 0)
	     {    
		arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
		arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
	     }

	     /*  */
	     arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);
	     /*  */
	     arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);

	     /* cos class of service field */
	     arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->cos);
	     
	     /*start , End and reserve fields*/
	     arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->s_Ds & 0x01) << 7);
	     arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->e_Ds & 0x01) << 6);
	     arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->rsrv_Ftype9 & 3) << 3);
	     arbiter->packet_Stream[i]  |= (RIO_UCHAR_T)((0) << 2);

	     pad = i;

	     if(entry->e_Ds == 1  && entry->s_Ds == 1 )
	     {
		/* Single Segment */
#ifndef _CARBON_SRIO_XTOR
		printf("\nSingle Segment\n");
#endif
	     	arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->o & 0x01 )<< 1);
	     	arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->p & 0x01));
	     	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->stream_ID.flow_ID);
	     	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->stream_ID.src_ID);
	      }

	     else if(entry->e_Ds == 1)
	     {
		/* End Segment */
#ifndef _CARBON_SRIO_XTOR
		printf("\nEnd Segment\n");
#endif
	     	arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->o & 0x01 )<< 1);
	     	arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->p & 0x01));
	     	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->length) >> 8);
	     	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->length);

		/* printf("\nat tx  odd is %d and pad is %d \n",entry->o,entry->p);*/
	      }

	      else if(entry->s_Ds == 1)
	      {
	     	/* Start Segment */
#ifndef _CARBON_SRIO_XTOR
	    	 printf("\n Start Segment\n");
#endif
	     	/*streamID */
             	arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->rsrv_Ftype9 & 2);
	     	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->stream_ID.flow_ID);
	     	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->stream_ID.src_ID);
	      }

	      else
	      {
#ifndef _CARBON_SRIO_XTOR
	     	printf("\n Continuous Segment \n"); 
#endif
             	arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->rsrv_Ftype9 & 2);
	      }

	      i++;  

	     /* check if the payload wasn't already passed */
	     if (entry_Out->payload_Source_hw == NULL)
	          ctray->rio_PL_Get_Req_Data_Hw(
	             ctray->pl_Interface_Context,
	 	     entry_Out->trx_Tag,
		     entry_Out->offset,
		     (RIO_UCHAR_T)entry_Out->payload_Size,
		     entry_Out->payload_hw);

	     /* convert payload to the stream */
	     for (j = 0; j < entry_Out->payload_Size; j++, i+=2)/* 8 - double word; 2 - half word */
		 RIO_PL_Out_Conv_Hw_Char(entry_Out->payload_hw + j, arbiter->packet_Stream + i);

#ifndef _CARBON_SRIO_XTOR
	     printf("\n\n");
#endif

	     /*padding for odd number of half words */
             if(i < 80)
	     {   
	  	for(;(i+2)%4 !=0;i++)
		{
                   arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
		   entry->p = 1;
		}
	        arbiter->packet_Stream[pad] |= (RIO_UCHAR_T)((entry->p & 0x01));
	     }
	     else
	     {
	     	for(;i%4!=0;i++)
		{
                   arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
		   entry->p=1;
		}
	        arbiter->packet_Stream[pad] |= (RIO_UCHAR_T)((entry->p & 0x01));
	     }

	     /* check if we need to insert intermediate CRC */
	     if (i > 80)
	     {
	         unsigned int k; /* loop counter*/

	         RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
		 tmp2 = arbiter->packet_Stream[81],
		 tmp3, tmp4; /* temporaries */

                 for (k = 82; k < i; k += 2)
		 {
		      tmp3 = arbiter->packet_Stream[k];
	              tmp4 = arbiter->packet_Stream[k + 1];
       		      arbiter->packet_Stream[k] = tmp1;
		      arbiter->packet_Stream[k + 1] = tmp2;
		      tmp1 = tmp3;
       		      tmp2 = tmp4;
             	 }

		 arbiter->packet_Stream[i] = tmp1;
		 arbiter->packet_Stream[i + 1] = tmp2;
	         /* compute intermadiate CRC */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[80] = 0; 
        arbiter->packet_Stream[81] = 0;;
        }
       else
       {
        RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        SUBSTITUTE_INTERMEDIATE_CRC; /*replace value with user crc if necessary*/

        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

        i += 2;
    }

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

	      i++;

	      /* check if we need to pad packet */
    if (i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
    /* update arbiter data */
    arbiter->packet_Len = i;
}

/***************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */

/***************************************************************************
 * Function : RIO_PL_Out_Conv_Hw_Char
 *
 * Description: convert half word to the stream
 *
 * Returns: none
 *
 * Notes: the only place for half-word to byte stream converting
 *
 **************************************************************************/
void RIO_PL_Out_Conv_Hw_Char(RIO_HW_T *hw, RIO_UCHAR_T *stream)
{
    /* 
     * used offsets and masks are unique and defined by endian mode and
     * structure defenition - they can be used only here in the sense of
     * halfword encoding
     */
    unsigned int i, shift;
    unsigned int  mask;

    if (hw == NULL || stream == NULL)
        return;

    /* convert the first word*/
    for (i = 0, mask = 0xff00, shift = 8; i < 2; i++, shift -= 8, mask >>= 8)
        stream[i] = (RIO_UCHAR_T)((*hw & mask) >> shift);

}

/* GDA: Bug#124 - Support for Data Streaming packet */
/***************************************************************************/



/* GDA: Bug#125 - Support for Flow control packet */
/***************************************************************************
 * Function : pl_Out_Buf_Make_7
 *
 * Description: convert stream write packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Out_Buf_Make_7(
    RIO_PL_MODEL_CALLBACK_TRAY_T *ctray, 
    RIO_PL_OUT_ENTRY_T           *entry_Out, 
    RIO_PL_OUT_ARB_T             *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 5 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_PL_OUTBOUND_RQ_T *entry; 
    unsigned short crc = RIO_PL_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0;
    (void) ctray;

    if (entry_Out == NULL || arbiter == NULL)
        return;
    
    entry = &entry_Out->entry;

    /* 0 byte already set in caller*/
    i = 0;
   entry->tt=0;
   
    
    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tranp_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->tranp_Info & 0x000000ff);
    
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tgtdest_ID & 0xff) >> 8);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tgtdest_ID));
    }
    else
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->tgtdest_ID));
    
        /*arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->x_On_Off & 0x8f) << 7);*/
   /* entry->x_On_Off=1;*/
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->x_On_Off & 0x8f) << 7);


 
    /*for packet type 7 wdptr field shall be set at 0*/
    if (entry->additional_Fields_Valid == RIO_TRUE)
        arbiter->packet_Stream[i] |=
                (RIO_UCHAR_T)((entry->rsrv_Ftype7 & 7));
    else 
        arbiter->packet_Stream[i] &=
                    (RIO_UCHAR_T)(0xFB);

        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->flow_ID & 0xfe) << 1);
        arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->soc & 0x01));
    i++;
    i++;

    /* here we need to compute CRC code */
	/*GDA:Bug#139 remove the need to recompute crc */
	if(entry_Out->data_Corrupted==RIO_TRUE)
        {
        arbiter->packet_Stream[i] = 0; 
        arbiter->packet_Stream[++i] = 0;
	entry_Out->data_Corrupted=RIO_FALSE;
        }
       else
       {
    	RIO_PL_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    	SUBSTITUTE_CRC; /*replace value with user crc if necessary*/

    	arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
    	arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);
       }
	/*GDA:Bug#139 remove the need to recompute crc */

    i++;

    /* for(j=0;j<i ; j++)
	    printf("\n stream[%d] is %d \n",j,arbiter->packet_Stream[j]);*/

    /* check if we need to pad packet */
    if (i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
    /* update arbiter data */
    arbiter->packet_Len = i;

}
/***************************************************************************/
