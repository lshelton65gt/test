#******************************************************************************
#*
#* Copyright Motorola, Inc. 2002-2004
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/pl/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds relocatable object file for physical layer model
#

include ../../verilog_env/demo/debug.inc

CC              =   gcc
TARGET          =   pl.o
SERIAL_TARGET	=   pl_serial.o
PARALLEL_TARGET	=   pl_parallel.o

DEBUG		=   -DNDEBUG

DEFINES         =   $(DEBUG) $(_DEBUG)

COMMON_INCLUDES		=   -I../interfaces/common  
SERIAL_INCLUDES		=   -I../interfaces/serial_interfaces  
PARALLEL_INCLUDES	=   -I../interfaces/parallel_interfaces

PL_INCLUDES			=   -I./include
PCS_PMA_INCLUDES		=   -I../pcs_pma/include
PCS_PMA_ADAPTER_INCLUDES	=   -I../pcs_pma_adapter/include
TXRX_INCLUDES			=   -I../txrx/include
PARALLEL_INIT_INCLUDES		=   -I../parallel_init/include

LIBS            =

CFLAGS          =   -c $(prof) $(DEFINES) -elf -ansi -pedantic -Wall -O3
LFLAGS          =   $(prof) -r

COMMON_OBJS		=   rio_pl_in_buf.o rio_pl_out_buf.o rio_pl_errors.o rio_pl_registers.o rio_pl_tx.o rio_pl_rx.o rio_pl_pllight_model_common.o
PCS_PMA_OBJS		=   ../pcs_pma/rio_c2char.o ../pcs_pma/rio_char2c.o ../pcs_pma/rio_pcs_pma.o ../pcs_pma/rio_pcs_pma_dp.o ../pcs_pma/rio_pcs_pma_dp_cl.o ../pcs_pma/rio_pcs_pma_dp_pins_driver.o ../pcs_pma/rio_pcs_pma_dp_sb.o ../pcs_pma/rio_pcs_pma_pm.o ../pcs_pma/rio_pcs_pma_pm_align.o ../pcs_pma/rio_pcs_pma_pm_init.o ../pcs_pma/rio_pcs_pma_pm_sync.o
PCS_PMA_ADAPTER_OBJS	=   ../pcs_pma_adapter/rio_pcs_pma_adapter.o
TXRX_OBJS		=   ../txrx/rio_32_txrx_model.o
PARALLEL_INIT_OBJS	=   ../parallel_init/rio_parallel_init.o
PARALLEL_OBJS	=   rio_pl_pllight_parallel_model.o 
SERIAL_OBJS	    =   rio_pl_pllight_serial_model.o

###############################################################################
# print the usage info
all                 :
	            @echo ""
	            @echo "Insufficient number of arguments."
	            @echo "To build the physical layer relocatable object file, execute:"
	            @echo "    make pl"
	            @echo "To clean the physical layer data, execute:"
	            @echo "    make clean"

# make target including parallel and serial parts
$(TARGET) :	$(COMMON_OBJS) $(PARALLEL_OBJS) $(SERIAL_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS)
		ld  $(LFLAGS) $(COMMON_OBJS) $(PARALLEL_OBJS) $(SERIAL_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS) $(LIBS) -o $(TARGET)

# make target including serial parts only
$(SERIAL_TARGET) :	$(COMMON_OBJS) $(SERIAL_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS)
		ld  $(LFLAGS) $(COMMON_OBJS) $(SERIAL_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS) $(LIBS) -o $(SERIAL_TARGET)

# make target including parallel parts only
$(PARALLEL_TARGET) :	$(COMMON_OBJS) $(PARALLEL_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS)
		ld  $(LFLAGS) $(COMMON_OBJS) $(PARALLEL_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(LIBS) -o $(PARALLEL_TARGET)

#clean data
clean :		
		rm -f $(TARGET)
		rm -f $(SERIAL_TARGET)
		rm -f $(PARALLEL_TARGET)
		rm -f $(COMMON_OBJS)
		rm -f $(TXRX_OBJS)
		rm -f $(PARALLEL_INIT_OBJS)
		rm -f $(PCS_PMA_OBJS)
		rm -f $(PCS_PMA_ADAPTER_OBJS)

#make pl relocatable object including both serial and parallel parts
pl :		$(TARGET)

pl_model:    pl   
CFLAGS += -DRIO_PL_MODEL

#make pl relocatable object including serial parts only
serial_pl :	$(SERIAL_TARGET)

pl_serial_model:	serial_pl   
CFLAGS += -DRIO_PL_MODEL

#make pl relocatable object including parallel parts only
parallel_pl :	$(PARALLEL_TARGET)

pl_parallel_model:	parallel_pl   
CFLAGS += -DRIO_PL_MODEL

%.o :		%.c
		$(CC) $(CFLAGS) $(COMMON_INCLUDES) $(SERIAL_INCLUDES) $(PARALLEL_INCLUDES) $(PL_INCLUDES) $(PCS_PMA_INCLUDES) $(PCS_PMA_ADAPTER_INCLUDES) $(TXRX_INCLUDES) $(PARALLEL_INIT_INCLUDES) -o $*.o $<

###############################################################################
             
