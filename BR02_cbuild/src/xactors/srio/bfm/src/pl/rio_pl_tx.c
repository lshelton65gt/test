/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/rio_pl_tx.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Physical layer transmitter
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>     


#include "rio_pl_tx.h"
#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_in_buf.h"
#include "rio_pl_out_buf.h"


#define RIO_OUT_GET_ENTRY(handle, i) ((handle)->outbound_Buf.buffer + (i))
#define RIO_OUT_GET_QUEUE_ELEM(handle, i) ((handle)->outbound_Buf.send_Queue + (i))

/* GDA Implemented  Bug#116 */    
static void get_Multicast(RIO_PL_DS_T *handle, RIO_GRANULE_T *multicast);
/* GDA Implemented  Bug#116 */ 

/* prototypes for internal static function */
static void get_IDLE(RIO_PL_DS_T *handle, RIO_GRANULE_T *idle);
static void get_LRQ_Reset(RIO_PL_DS_T *handle, RIO_GRANULE_T *lrqr);

/* handlers for proceding corresponding transmitter states*/
static void tx_Normal(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static unsigned int get_Tx_Current_Index(RIO_PL_DS_T *handle);
static void get_EOP(RIO_PL_DS_T *handle, RIO_GRANULE_T *eop);
static void get_Restart_From_Retry(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static void discard_Entry(RIO_PL_DS_T *handle, unsigned int index, RIO_BOOL_T is_PNA);
static void send_Acknowledge(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static void send_Link_Request(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static void send_Link_Response(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static void send_Packet_Part(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static void send_Rest_From_Retr(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static void start_New_Packet(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
static void get_STOMP(RIO_PL_DS_T *handle, RIO_GRANULE_T *idle);
static void send_Throttle(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
#ifndef _CARBON_SRIO_XTOR_FIX
static void send_Training(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
#endif
static void check_States(RIO_PL_DS_T *handle);

static void prepare_Link_Request_For_Tx_Error_Rec(RIO_PL_DS_T *handle);
static void start_Tx_Error_Rec(RIO_PL_DS_T *handle);


#define RIO_PL_LINK_RESP_STATE_CLEAR_MASK 0x80000000
#define RIO_PL_TX_CLEAR_MASK              0x0012ffff
#define RIO_PL_TX_STOPPED_ERROR           0x00030000
#define RIO_PL_TX_STOPPED_RETRY           0x001c0000
#define RIO_PL_STOPPED_UNREC_ERROR        0x00000004
#define RIO_PL_REG_OK                     0x00000002


/***************************************************************************
 * Function : RIO_PL_Tx_Send_Ack
 *
 * Description: store an acknowledgment for heading out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Send_Ack(RIO_PL_DS_T *handle, RIO_GRANULE_T *ack_Granule)
{
        	    
    if (handle == NULL || ack_Granule == NULL)
        return;

       /* check if buffer is full*/
    if (handle->tx_Data.ack_Out_Buffer.is_Full == RIO_TRUE )
        return;

  #ifndef POFF
#ifndef _CARBON_SRIO_XTOR
    printf("\n\t([Inst: D`%u] Storing ACKS)  Ack ID: D`%u\n",handle->instance_Number, ack_Granule->granule_Struct.ack_ID);  /* GDA: Debug */
#endif
  #endif

    /* add new acknowledgment for sending */
    handle->tx_Data.ack_Out_Buffer.ack_Buffer[handle->tx_Data.ack_Out_Buffer.buffer_Top] = *ack_Granule;

    /* top is cyclicly going within buffer*/
    handle->tx_Data.ack_Out_Buffer.buffer_Top = (handle->tx_Data.ack_Out_Buffer.buffer_Top + 1) % handle->ack_Buffer_Size;

    /* check if buffer is full */
    if (handle->tx_Data.ack_Out_Buffer.buffer_Top == handle->tx_Data.ack_Out_Buffer.buffer_Head)
        handle->tx_Data.ack_Out_Buffer.is_Full = RIO_TRUE;
       
}

/***************************************************************************
 * Function : RIO_PL_Tx_Rec_Ack
 *
 * Description: an acknowledgment for headed out packet has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Rec_Ack(RIO_PL_DS_T *handle, RIO_GRANULE_T *ack_Granule)
{
    unsigned int expect_Ack;
    RIO_BOOL_T is_Unsolicited; /*true in case of unsolicited acknowledge*/
  
  
   if (handle == NULL || ack_Granule == NULL)
        return;

  #ifndef POFF
#ifndef _CARBON_SRIO_XTOR
   printf("\t([Inst: D`%u] Receiving ACK) Ack ID: D`%u\n", handle->instance_Number, \
           ack_Granule->granule_Struct.ack_ID); /* GDA: Debug */
#endif
  #endif

   if ((ack_Granule->granule_Type != RIO_GR_PACKET_NOT_ACCEPTED) && 
        (ack_Granule->granule_Type != RIO_GR_PACKET_RETRY || !(handle->is_Parallel_Model))) 
        /* PACKET_RETRY is a special case - it contains buf_Status in serial mode and
           does not in parallel */
        RIO_PL_Set_State(handle, RIO_PARTNER_BUF_STATUS, ack_Granule->granule_Struct.buf_Status, 0);
     	
        expect_Ack = handle->tx_Data.ack_In_Buffer.buffer_Head;

    /*check is this ack unsolicited or no*/
    if ((handle->tx_Data.ack_In_Buffer.buffer_Head 
            == handle->tx_Data.ack_In_Buffer.buffer_Top) &&
            (handle->tx_Data.ack_In_Buffer.is_Full == RIO_FALSE))
        is_Unsolicited = RIO_TRUE;
    else
        is_Unsolicited = RIO_FALSE;

    /*check that this is a retry in output error stoped state*/
    if (ack_Granule->granule_Struct.stype == RIO_GR_PACKET_RETRY_STYPE &&
        handle->tx_Data.state == RIO_PL_STOPPED_DUE_TO_ERROR)
    {
           RIO_PL_Print_Message(handle, RIO_PL_ERROR_65, 
            ack_Granule->granule_Struct.stype);
	   
    }


    /*check for unexpected or unsolicited acknowlege*/
    else if (ack_Granule->granule_Struct.ack_ID != expect_Ack ||
        is_Unsolicited == RIO_TRUE)
    {
   
       if (is_Unsolicited == RIO_TRUE)
        {
            /*unsolicited acknowledge*/
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_66);               
        }
        else
        {
       
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_52);
        }

        start_Tx_Error_Rec(handle);
        return;
    }

        /* check if it's expected acknowledge */
    if (ack_Granule->granule_Type == RIO_GR_PACKET_ACCEPTED &&
        ack_Granule->granule_Struct.ack_ID == expect_Ack &&
        (handle->tx_Data.ack_In_Buffer.ack_Buffer[expect_Ack].state == RIO_PL_OUT_SENT ||
        handle->tx_Data.ack_In_Buffer.ack_Buffer[expect_Ack].state == RIO_PL_OUT_TIMEOUTED)) 
            {

        /* commit packet sending */
        RIO_PL_Out_Packet_Ack(
            handle, 
            handle->tx_Data.ack_In_Buffer.ack_Buffer[expect_Ack].packet_Handle,
			RIO_TRUE
            );
        handle->tx_Data.ack_In_Buffer.buffer_Head = (expect_Ack + 1 ) % handle->ack_Buffer_Size;
        handle->tx_Data.ack_In_Buffer.is_Full = RIO_FALSE;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[expect_Ack].packet_Handle = 0;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[expect_Ack].state = RIO_PL_OUT_INVALID;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[expect_Ack].time_After_Sending = 0;


        /*set expected ackID in the next received ack control*/ 

        RIO_PL_Set_State(
            handle,
            RIO_OUTSTANDING_ACKID, 
            handle->tx_Data.ack_In_Buffer.buffer_Head,
            0
            );
           return;
    }


    if (ack_Granule->granule_Type == RIO_GR_PACKET_ACCEPTED &&
        handle->tx_Data.ack_In_Buffer.ack_Buffer[ack_Granule->granule_Struct.ack_ID].state == RIO_PL_OUT_SENDING)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_53);
        start_Tx_Error_Rec(handle);
        return;
    }

    if (ack_Granule->granule_Type == RIO_GR_PACKET_RETRY)
    {
        unsigned long state;
        if (handle->tx_Data.state == RIO_PL_OK)
        {
            /* this is for Serial PCS/PMA SC/PD correct choosing */
            if (RIO_TRUE == handle->tx_Data.in_Progress)
                RIO_PL_Set_State(
                    handle,
                    RIO_SYMBOL_IS_PD, 
                    RIO_TRUE,
                    0
                );

            discard_Entry(handle, ack_Granule->granule_Struct.ack_ID, RIO_FALSE);
            /*  handle->tx_Data.in_Progress = RIO_FALSE;*/

            /* set appropriate state and load restart from retry to the latch */
            handle->tx_Data.stomp_Generator.sent_STOMP = RIO_FALSE;
            handle->tx_Data.state = RIO_PL_STOPPED_DUE_TO_RETRY;
            handle->tx_Data.restart_From_Retry.valid = RIO_TRUE;

            get_Restart_From_Retry(handle, &handle->tx_Data.restart_From_Retry.control);

            /* update state in the register */
            RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
                handle->inst_Param_Set.entry_Extended_Features_Ptr, &state, RIO_TRUE);
            /* clear transmitter bits*/
            state &= RIO_PL_ERROR_TX_STATE_CLEAR_MASK;
            /* set stopped due to retry bit */
            state |= RIO_PL_ERROR_TX_RETRY;
            RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
                handle->inst_Param_Set.entry_Extended_Features_Ptr, state, RIO_TRUE);

        }

	 return;
    }

    if (ack_Granule->granule_Type == RIO_GR_PACKET_NOT_ACCEPTED)
    {
        /*discard_Entry(handle, ack_Granule->granule_Struct.ack_ID);*/
        /*handle->tx_Data.in_Progress = RIO_FALSE;*/
       
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_54);
        start_Tx_Error_Rec(handle);
        return;
    }
}


/***************************************************************************
 * Function : RIO_PL_Tx_Send_Link_Resp
 *
 * Description: a link response shall be sent
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Send_Link_Resp(RIO_PL_DS_T *handle, RIO_GRANULE_T *link_Response)
{
     if (handle == NULL || link_Response == NULL)
        return;

    /* check if one link response is waiting for heading out */
    if (handle->tx_Data.link_Response.valid == RIO_TRUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_22);
        return;
    }

    /* hold link response in the latch */
    handle->tx_Data.link_Response.valid = RIO_TRUE;
    handle->tx_Data.link_Response.control = *link_Response;
    handle->tx_Data.link_Response.last_Ack = handle->tx_Data.ack_Out_Buffer.buffer_Top;
}

/***************************************************************************
 * Function : RIO_PL_Tx_Send_Link_Req
 *
 * Description:  a link request shall be head out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Send_Link_Req(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    /* check if a link request already exists */
    if (handle->tx_Data.link_Request.valid == RIO_TRUE)   
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_19);
        return;
    }

    /* store new LINK_REQUEST to latch */
    handle->tx_Data.link_Request.control = *granule;
    handle->tx_Data.link_Request.valid = RIO_TRUE;
    handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
    handle->tx_Data.link_Request.time_After_Sending = 0;
    handle->tx_Data.link_Request.retry_Cnt = 0;

}

/***************************************************************************
 * Function : RIO_PL_Tx_Send_Training
 *
 * Description:  a training pattern shall be head out
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Send_Training(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule, unsigned int num_Trainings)
{
    if (handle == NULL || granule == NULL)
        return;

    /* check if a link request already exists */
    if (handle->tx_Data.training_Generator.valid == RIO_TRUE)   
    {
 /*       RIO_PL_Print_Message(handle, RIO_PL_ERROR_19);*/
        return;
    }

    /* store new Training pattern  to latch */
    handle->tx_Data.training_Generator.training_Cnt = num_Trainings;
    handle->tx_Data.training_Generator.valid = RIO_TRUE;

}

/***************************************************************************
 * Function : RIO_PL_Tx_Rec_Link_Resp
 *
 * Description:  a link response has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Rec_Link_Resp(RIO_PL_DS_T *handle, RIO_GRANULE_T *link_Response)
{
    /* status of link response register */
    unsigned long status;
     

    if (handle == NULL || link_Response == NULL)
        return;

    /* check if the instanse is waiting for LINK_RESPONSE*/
    if (handle->tx_Data.link_Request.valid == RIO_FALSE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_20);
        start_Tx_Error_Rec(handle);
        return;
    }

    /* delete LINK_REQUEST and store response to register */
    handle->tx_Data.link_Request.valid = RIO_FALSE;
    handle->tx_Data.link_Request.time_After_Sending = 0;
    handle->tx_Data.link_Request.is_Sent = RIO_FALSE;

    /* set valid, link status, expected ackID and retry cnt values
     * these constants are used only here not anywhere else.
     * change them here in case of LINK ReSPONSE STATUS register changing
     */
    if (handle->is_Parallel_Model == RIO_TRUE)
    {
        status = (unsigned long)RIO_PL_LINK_RESP_STATE_CLEAR_MASK;
        status |= (unsigned long)link_Response->granule_Struct.buf_Status; 
        status |= ((unsigned long)link_Response->granule_Struct.ack_ID) << 4;
        status |= (unsigned long)(0x1) << 31;
    }
    else 
    {
        status = (unsigned long)RIO_PL_LINK_RESP_STATE_CLEAR_MASK;
        status |= (unsigned long)link_Response->granule_Struct.buf_Status; 
        status |= ((unsigned long)link_Response->granule_Struct.ack_ID) << 5;
        status |= (unsigned long)(0x1) << 31;
    }

    handle->tx_Data.link_Request.retry_Cnt = 0;

   /*set lrsp ackID status*/
        RIO_PL_Set_State(
            handle,
            RIO_LRSP_ACKID_STATUS, 
            link_Response->granule_Struct.ack_ID,
            0
            );


    RIO_PL_Write_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr
        + RIO_LINK_VALID_RESP_A, status, RIO_TRUE);

    if (handle->tx_Data.state == RIO_PL_OK && handle->tx_Data.ack_In_Buffer.buffer_Top != 
        link_Response->granule_Struct.ack_ID)
    {        
        if (handle->tx_Data.ack_In_Buffer.ack_Buffer[(handle->tx_Data.ack_In_Buffer.buffer_Top +
            ((handle->ack_Buffer_Size) - 1)) % (handle->ack_Buffer_Size)].state == RIO_PL_OUT_SENDING &&
            (handle->tx_Data.ack_In_Buffer.buffer_Top + ((handle->ack_Buffer_Size) - 1)) % (handle->ack_Buffer_Size) ==link_Response->granule_Struct.ack_ID);
        else
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_51);
            handle->tx_Data.state = RIO_PL_ERROR;
            /*need write to the register*/
            RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
                handle->inst_Param_Set.entry_Extended_Features_Ptr, &status, RIO_TRUE);
            /* clear  bits exclude width*/
            status &= RIO_PL_ERROR_PORT_STATE_CLEAR_MASK;
            /* set OK bit for transmitter and receiver */
            status |= RIO_PL_ERROR_STATE_UNREC_ERROR;
            RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
                handle->inst_Param_Set.entry_Extended_Features_Ptr, status, RIO_TRUE);
            return;
        }
    }

    if (handle->tx_Data.state == RIO_PL_STOPPED_DUE_TO_ERROR || 
        handle->tx_Data.state == RIO_PL_STOPPED_DUE_TO_RETRY)
    {
        unsigned int          buffer_Index;
        
        /* move boundaries*/
	/* store buffer_Top because it gets modified in discard_Entry */
        buffer_Index = handle->tx_Data.ack_In_Buffer.buffer_Head;

        if (handle->tx_Data.ack_In_Buffer.is_Full == RIO_FALSE)
            while (buffer_Index != link_Response->granule_Struct.ack_ID)
            {
                if ((buffer_Index == handle->tx_Data.ack_In_Buffer.buffer_Top) && 
                    (handle->parameters_Set.enable_LRQ_Resending == RIO_TRUE))
                {   /*maintis id 44 modification
                    in case of initialization parameters the model can
                    resend the link request symbol*/
                    prepare_Link_Request_For_Tx_Error_Rec(handle);

                    RIO_PL_Print_Message(handle, RIO_PL_WARNING_13);
                    return;
                }
                else if (buffer_Index == handle->tx_Data.ack_In_Buffer.buffer_Top)
                {    
                    RIO_PL_Print_Message(handle, RIO_PL_ERROR_51);
                    handle->tx_Data.state = RIO_PL_ERROR;
                    /*need write to the register*/
                    RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
                        handle->inst_Param_Set.entry_Extended_Features_Ptr, &status, RIO_TRUE);
                    /* clear  bits */
                    status &= RIO_PL_ERROR_PORT_STATE_CLEAR_MASK;
                    /* set unrecoverable error bit */
                    status |= RIO_PL_ERROR_STATE_UNREC_ERROR;
                    RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
                        handle->inst_Param_Set.entry_Extended_Features_Ptr, status, RIO_TRUE);
                    return;
                }
                buffer_Index = (buffer_Index + 1 ) % handle->ack_Buffer_Size;
            }


        discard_Entry(handle, link_Response->granule_Struct.ack_ID, RIO_TRUE);

        if ((handle->tx_Data.ack_In_Buffer.buffer_Head 
            == handle->tx_Data.ack_In_Buffer.buffer_Top) &&
            (handle->tx_Data.ack_In_Buffer.is_Full == RIO_FALSE))
        {
            handle->tx_Data.ack_In_Buffer.buffer_Head = 
                handle->tx_Data.ack_In_Buffer.buffer_Top = 
                link_Response->granule_Struct.ack_ID;

            /*set next out port transmitted ackID value in packet*/
            RIO_PL_Set_State(
                handle,
                RIO_OUTBOUND_ACKID, 
                handle->tx_Data.ack_In_Buffer.buffer_Top,
                0
                );

            /*set expected ackID in the next received ack control*/
            RIO_PL_Set_State(
                handle,
                RIO_OUTSTANDING_ACKID, 
                handle->tx_Data.ack_In_Buffer.buffer_Head,
                0
                );
        }
        else
        {
            while (handle->tx_Data.ack_In_Buffer.buffer_Head != handle->tx_Data.ack_In_Buffer.buffer_Top)
            {
                RIO_PL_Out_Packet_Ack(
                    handle, 
                    handle->tx_Data.ack_In_Buffer.ack_Buffer[handle->tx_Data.ack_In_Buffer.buffer_Head].packet_Handle,
					RIO_TRUE
                    );
                handle->tx_Data.ack_In_Buffer.is_Full = RIO_FALSE;
                handle->tx_Data.ack_In_Buffer.ack_Buffer[handle->tx_Data.ack_In_Buffer.buffer_Head].packet_Handle = 0;
                handle->tx_Data.ack_In_Buffer.ack_Buffer[handle->tx_Data.ack_In_Buffer.buffer_Head].state = RIO_PL_OUT_INVALID;
                handle->tx_Data.ack_In_Buffer.ack_Buffer[handle->tx_Data.ack_In_Buffer.buffer_Head].time_After_Sending = 0;
                handle->tx_Data.ack_In_Buffer.buffer_Head =
                    (handle->tx_Data.ack_In_Buffer.buffer_Head + 1 ) % handle->ack_Buffer_Size;

                /*set expected ackID in the next received ack control*/
                RIO_PL_Set_State(
                    handle,
                    RIO_OUTSTANDING_ACKID, 
                    handle->tx_Data.ack_In_Buffer.buffer_Head,
                    0
                    );
            }
        /*    handle->tx_Data.ack_In_Buffer.buffer_Top = 
                    link_Response->granule_Struct.ack_ID;*/
        }

        /* change transmitter state in the register */
        /* recover to normal state after error */
        handle->tx_Data.state = RIO_PL_OK;
        
        /*need write to the register*/
        RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
            handle->inst_Param_Set.entry_Extended_Features_Ptr, &status, RIO_TRUE);
        /* clear  bits exclude width*/
        status &= RIO_PL_ERROR_TX_STATE_CLEAR_MASK;
        RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
            handle->inst_Param_Set.entry_Extended_Features_Ptr, status, RIO_TRUE);

/*        status = 0lu;
        RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A, &status, RIO_TRUE);
*/
        /* clear transmitter bits*/
/*        status &= RIO_PL_TX_CLEAR_MASK;
*/
        /* set OK bit */
/*        status |= RIO_PL_TX_REG_OK;
        RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A, status, RIO_TRUE);
*/
    }


}

/***************************************************************************
 * Function : RIO_PL_Tx_Rec_Throttle
 *
 * Description:  a THROTTLE has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Rec_Throttle(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL )
        return;

    /* check if THROTTLE corresponds to any packets transmission */
/*
    if (handle->tx_Data.in_Progress != RIO_TRUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_18);*/
/*        start_Tx_Error_Rec(handle);*/
/*        return;
    }
*/
    
    /* store new THROTTLE */
    if (granule->granule_Struct.buf_Status == 0xf)
    {
        handle->tx_Data.throttle_Generator.valid = RIO_FALSE;
        return;
    }
    else if (granule->granule_Struct.buf_Status == 0xe)
    {
        handle->tx_Data.throttle_Generator.valid = RIO_TRUE;
        handle->tx_Data.throttle_Generator.idle_Cnt = 1;
    }
    else
    {
        handle->tx_Data.throttle_Generator.valid = RIO_TRUE;
        handle->tx_Data.throttle_Generator.idle_Cnt = 
        1 << granule->granule_Struct.buf_Status;
    }
    /* check if THROTTLE overstep the limit */
    if (handle->tx_Data.throttle_Generator.idle_Cnt >
        handle->parameters_Set.throttle_Idle_Lim)
    {
        /* clear THROTTLE */
        handle->tx_Data.throttle_Generator.idle_Cnt = 0;
        handle->tx_Data.throttle_Generator.valid = RIO_FALSE;
        /* abort current packet by STOMP*/
         
        if (handle->tx_Data.in_Progress == RIO_TRUE)
            handle->tx_Data.need_STOMP = RIO_TRUE;
    }
}

/***************************************************************************
 * Function : RIO_PL_Tx_Init
 *
 * Description: initialize the transmitter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Init(RIO_PL_DS_T *handle)
{
    unsigned int i; /* loop counter */ 

    if (handle == NULL )
        return;

/*GDA: Initialization of handle variables Bug#86*/
    handle->ack_Cycle_Delay_Counter = 0;
    handle->ack_Delay_Period = 0;
    handle->set_Random = RIO_FALSE;
    handle->response_Packet_Delay_Flag=RIO_FALSE; 
/*GDA Bug#86*/
 /* GDA: Bug#138 */
#ifndef _CARBON_SRIO_XTOR
    printf("\n initialize at RIO_PL_Tx_Init \n");
#endif
    handle->resp_Packet_Delay_Enable 	= RIO_FALSE; 
    handle->resp_Packet_Delay_Cntr	= 0;
    /* GDA: Bug#138 */


/*GDA: Initialization of handle variables Bug#43*/ 
    if (handle->is_PLLight_Model==RIO_FALSE)
      { 
        handle->pe_Pl_Tx_Illegal_Pnacc_Resend_Cnt=handle->inst_Param_Set.tx_Illegal_Pnacc_Resend_Cnt;
      }
/*GDA: Bug#43 */

    /* initialize */
    handle->tx_Data.in_Progress = RIO_FALSE;

/*    handle->tx_Data.state = RIO_PL_OK;*/
/*    handle->tx_Data.state = RIO_PL_UNINIT;*/

    handle->tx_Data.state = RIO_PL_OK;

/*    if (handle->parameters_Set.requires_Window_Alignment == RIO_TRUE)
    {
        handle->tx_Data.train_state = RIO_PL_TX_SEND_TRN_REQ;
        RIO_PL_Print_Message(handle, RIO_PL_WARNING_8,
            handle->tx_Data.train_state);
    }
    else
        handle->tx_Data.train_state = RIO_PL_TX_SEND_IDLES_REQ;
*/    
    handle->tx_Data.expected_Ack_Id = 0;
    handle->tx_Data.need_STOMP = RIO_FALSE;
    handle->tx_Data.need_STOMP_Due_Gen = RIO_FALSE;

    handle->tx_Data.throttle_For_Send.valid = RIO_FALSE;
    
    /* initialize outbound ack buffer*/
    handle->tx_Data.ack_Out_Buffer.buffer_Head = handle->tx_Data.ack_Out_Buffer.buffer_Top = 0;
    handle->tx_Data.ack_Out_Buffer.is_Full = RIO_FALSE;

    /* initialize inbound ack buffer */
    handle->tx_Data.ack_In_Buffer.buffer_Head = handle->tx_Data.ack_In_Buffer.buffer_Top = 0;

    /*set next out port transmitted ackID value in packet*/
    RIO_PL_Set_State(
        handle,
        RIO_OUTBOUND_ACKID, 
        handle->tx_Data.ack_In_Buffer.buffer_Top,
        0
        );

    /*set expected ackID in the next received ack control*/
    RIO_PL_Set_State(
        handle,
        RIO_OUTSTANDING_ACKID, 
        handle->tx_Data.ack_In_Buffer.buffer_Head,
        0
        );

    handle->tx_Data.ack_In_Buffer.is_Full = RIO_FALSE;
    for (i = 0; i < handle->ack_Buffer_Size; i++)
    {
        handle->tx_Data.ack_In_Buffer.ack_Buffer[i].state = RIO_PL_OUT_INVALID;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[i].time_After_Sending = 0;
    }

    /* initialize LINK REQUEST latch */
    handle->tx_Data.link_Request.valid = RIO_FALSE;
    handle->tx_Data.link_Request.time_After_Sending = 0;
    handle->tx_Data.link_Request.retry_Cnt = 0;
    handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
    handle->tx_Data.link_Request.training_Counter.valid = RIO_TRUE;
    handle->tx_Data.link_Request.training_Counter.training_Cnt = 0;
    handle->tx_Data.link_Request.training_Counter.time_After_Sending = 0;


    /* initialize THROTTLE generator */
    handle->tx_Data.throttle_Generator.valid = RIO_FALSE;
    handle->tx_Data.throttle_Generator.idle_Cnt = 0;

    /* initialize IDLE generator */
    handle->tx_Data.idle_Generator.valid = RIO_FALSE;
    handle->tx_Data.idle_Generator.idle_Cnt = 0;

    /* initialize random IDLE generator */
    handle->tx_Data.rnd_Idle_Generator.gr_Num = 0;
    handle->tx_Data.rnd_Idle_Generator.hb = 0;
    handle->tx_Data.rnd_Idle_Generator.lb = 0;
    handle->tx_Data.rnd_Idle_Generator.idle_Cnt = 0;
    handle->tx_Data.rnd_Idle_Generator.packet_Ongoing = RIO_FALSE;
    handle->tx_Data.rnd_Idle_Generator.valid = RIO_FALSE;
    handle->tx_Data.force_EOP = RIO_FALSE;

   /* initialize packet stomp generator */
    handle->tx_Data.stomp_Generator.factor = 0; /* no stomp by default */
    handle->tx_Data.stomp_Generator.stomp_Cnt = 0;
    handle->tx_Data.stomp_Generator.sent_STOMP = RIO_FALSE;
    handle->tx_Data.stomp_Generator.sent_STOMP_ack_ID = 0;

   /* initialize LRQ-Reset generator */
    handle->tx_Data.lrqr_Generator.factor = 0; /* no LRQ-Reset by default */
    handle->tx_Data.lrqr_Generator.lrqr_Cnt = 0;
    handle->tx_Data.lrqr_Generator.need_RESET = RIO_FALSE;

    /* initialize LINK RESPONSE latch */
    handle->tx_Data.link_Response.valid = RIO_FALSE;
    handle->tx_Data.link_Response.last_Ack = 0;

    /* initialize RESTART FROM RETRY latch */
    handle->tx_Data.restart_From_Retry.valid = RIO_FALSE;


    /* initialize TRAINING latch */
    handle->tx_Data.training_Generator.valid = RIO_FALSE;
    handle->tx_Data.training_Generator.training_Cnt = 0;

    /* initialize IDLEWATCH  */
    handle->tx_Data.idle_Watch = RIO_FALSE;

    handle->tx_Data.pins_Disabled = RIO_FALSE;

    /*GDA: Bug#116 Implementation*/
           handle->multicast_Enable_Flag = RIO_FALSE;
           handle->multicast_Low_Boundary=0;
           handle->multicast_High_Boundary=0;  
           handle->set_Multicast_Rnd_Generator_Flag=0;  
           handle->set_Multicast_Flag = RIO_FALSE; 
    /*GDA: Bug#116 Implementation*/

}

/***************************************************************************
 * Function : RIO_PL_Granule_For_Transmitting
 *
 * Description: main transmitter loop
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Granule_For_Transmitting(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{

    if (handle == NULL || granule == NULL)
        return;

    /*training state machine state check*/
    /*handler_Train_Tx_State_Machine(handle);*/
        
    /*
     * we check all information source
     * if we are sending a packet ask for new portion
     */

    /*set sending idle latch to FALSE*/
    handle->tx_Data.idle_Watch = RIO_FALSE;

    /*check if errors occured*/
    check_States(handle);

    if (handle->tx_Data.state == RIO_PL_ERROR) 
    {
        get_IDLE(handle, granule);
        return;
    }

    /* GDA: Bug#116 Implementation */ 
    if (handle->set_Multicast_Flag == RIO_TRUE) 
      {
         get_Multicast(handle, granule);
         handle->set_Multicast_Flag = RIO_FALSE;
         return;
      }
    /*GDA: Bug#116 Implementation */

    if (handle->tx_Data.idle_Generator.valid == RIO_TRUE)
    {
        /* send IDLE */
        get_IDLE(handle, granule);
        /* decrease IDLE counter */
 /*       if (!--handle->tx_Data.idle_Generator.idle_Cnt)
               handle->tx_Data.idle_Generator.valid = RIO_FALSE;
        changer_Train_Tx_State_Machine(handle);*/
        return;
    }

    if (handle->tx_Data.rnd_Idle_Generator.gr_Num != 0)
        handle->tx_Data.rnd_Idle_Generator.gr_Num--;

    if (handle->tx_Data.rnd_Idle_Generator.valid == RIO_TRUE &&
        handle->tx_Data.rnd_Idle_Generator.packet_Ongoing == RIO_TRUE &&
        handle->tx_Data.rnd_Idle_Generator.gr_Num == 0 )
    {
        /* decrease IDLE counter */
        if (handle->tx_Data.rnd_Idle_Generator.idle_Cnt != 0)  
            handle->tx_Data.rnd_Idle_Generator.idle_Cnt--;
           
        /* send IDLE */
        get_IDLE(handle, granule);
        
        if (handle->tx_Data.rnd_Idle_Generator.idle_Cnt == 0)
        {
            handle->tx_Data.rnd_Idle_Generator.packet_Ongoing = RIO_FALSE;

        }
        return;
    }

    if (handle->tx_Data.lrqr_Generator.need_RESET == RIO_TRUE)
    {
        /* decrease Reset counter */
        if (handle->tx_Data.lrqr_Generator.lrqr_Cnt != 0)
            handle->tx_Data.lrqr_Generator.lrqr_Cnt--;
        
        get_LRQ_Reset(handle, granule);
        send_Link_Request(handle, granule);

        if (handle->tx_Data.lrqr_Generator.lrqr_Cnt == 0)
        {
            handle->tx_Data.lrqr_Generator.need_RESET = RIO_FALSE;
            
        }
        return;
    }

      
   

  

    /* send TRAINING if there is one */
    /* if (handle->tx_Data.training_Generator.valid == RIO_TRUE)
    {
        send_Training(handle, granule);
        changer_Train_Tx_State_Machine(handle);
        return;
    }*/

    /*changed priority of link_response*/
    if (handle->tx_Data.link_Response.valid == RIO_TRUE &&
        handle->tx_Data.link_Response.last_Ack == handle->tx_Data.ack_Out_Buffer.buffer_Head)
    {           
        send_Link_Response(handle, granule);
        return;
    }

    /* send LINK_REQUEST if there is one */
    if (handle->tx_Data.link_Request.valid == RIO_TRUE &&
        handle->tx_Data.link_Request.is_Sent == RIO_FALSE)
    {
          send_Link_Request(handle, granule);
	  /* changer_Train_Tx_State_Machine(handle);*/
        return;
    }

    /* send acknowledge if there is one */
    if ((handle->tx_Data.ack_Out_Buffer.is_Full ||
        handle->tx_Data.ack_Out_Buffer.buffer_Top !=
        handle->tx_Data.ack_Out_Buffer.buffer_Head ) && 
        ((handle->inst_Param_Set.ack_Delay_Enable==RIO_FALSE)||(handle->is_PLLight_Model)))  /*GDA Included */
    {
	send_Acknowledge(handle,granule);

	if ((handle->set_Random==RIO_TRUE) )       /*GDA Included*/
        {
             handle->set_Random=RIO_FALSE;                      /*GDA Included*/
             handle->inst_Param_Set.ack_Delay_Enable=RIO_TRUE;  /*GDA Included*/
             handle->response_Packet_Delay_Flag=RIO_TRUE;       /*GDA Included*/
        }

    #ifndef POFF
#ifndef _CARBON_SRIO_XTOR
        printf("\t([Inst: D`%u] Releasing ACK) Ack ID: D`%u; Granule_Type: D`%u; ack_Delay_Coutner: D`%u\n",\
                 handle->instance_Number,granule->granule_Struct.ack_ID,granule->granule_Type, \
		 handle->ack_Cycle_Delay_Counter); /* GDA: Debug */
#endif
    #endif

        return;
    }

      /* send link response if there is one */
/*    if (handle->tx_Data.link_Response.valid == RIO_TRUE &&
        handle->tx_Data.link_Response.last_Ack == handle->tx_Data.ack_Out_Buffer.buffer_Head)
    {
        send_Link_Response(handle, granule);
        return;
    }
*/

    /* send restart from retry if there is one */
    if (handle->tx_Data.restart_From_Retry.valid == RIO_TRUE)
    {
        send_Rest_From_Retr(handle, granule);
        if (handle->tx_Data.state == RIO_PL_STOPPED_DUE_TO_RETRY)
        {
            unsigned long status = 0lu; /* transmitter state*/
	    /* change transmitter state in the register */
            RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
                handle->inst_Param_Set.entry_Extended_Features_Ptr, &status, RIO_TRUE);
            /* clear transmitter bits*/
            status &= RIO_PL_ERROR_TX_STATE_CLEAR_MASK;
            /* set OK bit */
/*            status |= RIO_PL_TX_REG_OK;*/
            RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
                handle->inst_Param_Set.entry_Extended_Features_Ptr, status, RIO_TRUE);
            handle->tx_Data.state = RIO_PL_OK;
        }
        return;
        
    }

    /* sent THROTTLE if any */ 
    if (handle->tx_Data.throttle_For_Send.valid == RIO_TRUE)
    {
        send_Throttle(handle, granule);
        return;
    }

    /* other actions depend on transmitter state */
    switch (handle->tx_Data.state)
    {
        /* normal state proceding */
        case RIO_PL_OK:       
            tx_Normal(handle, granule);
            break;

        /* retry recovery */
        case RIO_PL_STOPPED_DUE_TO_RETRY:
            get_IDLE(handle, granule);
            break;

        /* error recovery */
        case RIO_PL_STOPPED_DUE_TO_ERROR:
            get_IDLE(handle, granule);
            break;

        case RIO_PL_UNINIT:
            get_IDLE(handle, granule);

        /* transmitter in unrecovereble error state */
        default:
            get_IDLE(handle, granule);

    }
}

/***************************************************************************
 * Function : get_IDLE
 *
 * Description: form proper IDLE for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void get_IDLE(RIO_PL_DS_T *handle, RIO_GRANULE_T *idle)
{
    if (handle == NULL || idle == NULL)
        return;
    /*shows we currently sending idle*/
    handle->tx_Data.idle_Watch = RIO_TRUE;

    idle->granule_Type = RIO_GR_IDLE;
    idle->granule_Struct.s = 1;
    idle->granule_Struct.ack_ID = RIO_GR_IDLE_SUB_TYPE;
    idle->granule_Struct.buf_Status = RIO_PL_Get_Our_Buf_Status(handle); /*RIO_PL_In_Buf_Status(handle);*/
    idle->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
}

/***************************************************************************
 * Function : get_STOMP
 *
 * Description: form proper IDLE for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void get_STOMP(RIO_PL_DS_T *handle, RIO_GRANULE_T *idle)
{
    if (handle == NULL || idle == NULL)
        return;

    idle->granule_Type = RIO_GR_STOMP;
    idle->granule_Struct.s = 1;
    idle->granule_Struct.ack_ID = RIO_GR_STOMP_SUB_TYPE;
    idle->granule_Struct.buf_Status = 0;
    idle->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
}


/***************************************************************************
 * Function : get_EOP
 *
 * Description: form proper EOP for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void get_EOP(RIO_PL_DS_T *handle, RIO_GRANULE_T *eop)
{
    if (handle == NULL || eop == NULL)
        return;
    /* form end of packet control */
    eop->granule_Type = RIO_GR_EOP;
    eop->granule_Struct.s = 1;
    eop->granule_Struct.ack_ID = RIO_GR_EOP_SUB_TYPE;
 /*   eop->granule_Struct.buf_Status = RIO_PL_In_Buf_Status(handle);*/
    
    eop->granule_Struct.buf_Status = RIO_PL_Get_Our_Buf_Status(handle);

    eop->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
    /* finish current packet */
}

/***************************************************************************
 * Function : get_LRQ_Reset
 *
 * Description: form proper LRQ_Reset for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void get_LRQ_Reset(RIO_PL_DS_T *handle, RIO_GRANULE_T *lrqr)
{
    if (handle == NULL || lrqr == NULL)
        return;
   
    /*check that model in the ok state*/
    if (handle->tx_Data.state == RIO_PL_STOPPED_DUE_TO_ERROR)
        return;

    lrqr->granule_Type = RIO_GR_LINK_REQUEST;
    lrqr->granule_Struct.s = 1; 
    lrqr->granule_Struct.ack_ID = RIO_PL_LREQ_RESET; 
    lrqr->granule_Struct.stype = RIO_GR_LINK_REQUEST_STYPE;
    lrqr->granule_Struct.buf_Status = RIO_PL_ALL_BUFS_FREE;

    /* store new LINK_REQUEST to latch */
    handle->tx_Data.link_Request.control = *lrqr;
    handle->tx_Data.link_Request.valid = RIO_TRUE;
    handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
    handle->tx_Data.link_Request.time_After_Sending = 0;
    handle->tx_Data.link_Request.retry_Cnt = 0;

}

/***************************************************************************
 * Function : tx_Normal
 *
 * Description: detect data for sending in normal state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void tx_Normal(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
	/* GDA : Bug#138 Support for dropping the response packet*/
 unsigned int i, i_Max;
 i_Max = RIO_PL_Out_Get_Buf_Size(handle);
     /* packet transmitting*/
    if (handle->tx_Data.in_Progress == RIO_TRUE)
    {
	    for(i=0; i<i_Max;i++)
	    {
		    if (((RIO_OUT_GET_ENTRY(handle, RIO_OUT_GET_QUEUE_ELEM(handle , i)->index)->entry.ftype) == RIO_RESPONCE) && handle->inst_Param_Set.resp_Stomp_Needed == RIO_TRUE)
		    {
			      handle->tx_Data.need_STOMP = handle->inst_Param_Set.resp_Stomp_Needed;
 }
 
 } 
 
		/* GDA : Bug#138 Support for dropping the response packet */

        /* check if we need to STOMP packet */
        if (handle->tx_Data.need_STOMP == RIO_TRUE)
        {
            /* discard current packet*/
            discard_Entry(handle, get_Tx_Current_Index(handle), RIO_FALSE);
            get_STOMP(handle, granule);
            handle->tx_Data.need_STOMP = RIO_FALSE;
            handle->tx_Data.need_STOMP_Due_Gen = RIO_FALSE;
            handle->tx_Data.in_Progress = RIO_FALSE;

            /* init THROTTLE genarator */
    handle->tx_Data.throttle_Generator.valid = RIO_FALSE;

            return;
        } 
        /* check if we need to STOMP packet due to stomp generator*/
        if (handle->tx_Data.need_STOMP_Due_Gen == RIO_TRUE)
        {
            
            unsigned int current_Index, cur_AckId;

            current_Index = get_Tx_Current_Index(handle); 

            /* commit the packet which has been sent */
            handle->tx_Data.ack_In_Buffer.ack_Buffer[current_Index].state = RIO_PL_OUT_SENT ;

			/* update state of acknowledgment pool*/
            if (handle->tx_Data.ack_In_Buffer.buffer_Head == 
                handle->tx_Data.ack_In_Buffer.buffer_Top
            )
               handle->tx_Data.ack_In_Buffer.is_Full = RIO_TRUE;
		    
            /* discard_Entry(handle, get_Tx_Current_Index(handle)); */
            get_STOMP(handle, granule);
            handle->tx_Data.need_STOMP = RIO_FALSE;
            handle->tx_Data.need_STOMP_Due_Gen = RIO_FALSE;
            handle->tx_Data.in_Progress = RIO_FALSE;

            /* init THROTTLE genarator */
            handle->tx_Data.throttle_Generator.valid = RIO_FALSE;

            handle->tx_Data.stomp_Generator.sent_STOMP = RIO_TRUE;
			
            cur_AckId = handle->tx_Data.ack_In_Buffer.buffer_Top == 0 ? handle->ack_Buffer_Size - 1 : handle->tx_Data.ack_In_Buffer.buffer_Top - 1;

            handle->tx_Data.stomp_Generator.sent_STOMP_ack_ID = cur_AckId;
            /*handle->tx_Data.state = RIO_PL_STOPPED_DUE_TO_RETRY;*/
            return;
        }

        /* received THROTTLE performing */
        if (handle->tx_Data.throttle_Generator.valid == RIO_TRUE)
        {
            /* send IDLE */
            get_IDLE(handle, granule);
            /* decrise IDLE counter */
            if (!--handle->tx_Data.throttle_Generator.idle_Cnt)
                handle->tx_Data.throttle_Generator.valid = RIO_FALSE;
           
            return;
        } 
        /* send the next portion of a packet */
        send_Packet_Part(handle, granule);
        return;
    }
    else 
    {
        /* received THROTTLE performing */
        if (handle->tx_Data.throttle_Generator.valid == RIO_TRUE)
        {
            /* send IDLE */
            get_IDLE(handle, granule);       
            /* decrise IDLE counter */
            if (!--handle->tx_Data.throttle_Generator.idle_Cnt)
                handle->tx_Data.throttle_Generator.valid = RIO_FALSE;
            return;
        }
    } 

    /*prevention of sending 8-th unacknowlwdged packet */
    if (handle->tx_Data.ack_In_Buffer.is_Full != RIO_TRUE &&
        ((handle->tx_Data.ack_In_Buffer.buffer_Top + 1) % handle->ack_Buffer_Size) ==
        handle->tx_Data.ack_In_Buffer.buffer_Head)
    {
        get_IDLE(handle, granule);
        return;
    }

    /* trying starting new packet transmission */
    if (handle->tx_Data.ack_In_Buffer.is_Full == RIO_TRUE)
    {
        get_IDLE(handle, granule);
        return;
    }

    start_New_Packet(handle, granule);
    /*init LRQ-Reset generator if necessary*/
    if (handle->tx_Data.lrqr_Generator.factor != 0 && 
        granule->granule_Type == RIO_GR_IDLE)
   {
        unsigned int lrqr_Prob; /* probability of LRQ-Reset event */
        double rand_Res_l;	         
       rand_Res_l = rand() / (double)RAND_MAX;
        /* 0.5 is necessary for correct round-off*/
        lrqr_Prob =  (int)(100 - rand_Res_l * 100 + 0.5);
        if (lrqr_Prob*10 <= handle->tx_Data.lrqr_Generator.factor)
        {
            handle->tx_Data.lrqr_Generator.need_RESET = RIO_TRUE;
            handle->tx_Data.lrqr_Generator.lrqr_Cnt = RIO_PL_LRQ_RESET_CNT;

        }
    }		
} 

/***************************************************************************
 * Function : start_New_Packet
 *
 * Description: start new packet transmission
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void start_New_Packet(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned int cur_Ack_Id, cur_Handle;
    int outstand_Pack_Num = 0;
    int free_Buf_Size = 0;
    int i_Max,i,index=0,j=0; /* GDA: Bug #138 */
    static int a[32];        /* GDA : Bug #138 */
    if (handle == NULL || granule == NULL)
        return;

    if (handle->tx_Data.ack_In_Buffer.is_Full == RIO_TRUE)
    {
        get_IDLE(handle, granule);
        return;
    }


    /*check if transmit flow control mode is set*/
    outstand_Pack_Num = (handle->tx_Data.ack_In_Buffer.buffer_Top + handle->ack_Buffer_Size 
        - handle->tx_Data.ack_In_Buffer.buffer_Head) % handle->ack_Buffer_Size;

    free_Buf_Size = RIO_PL_Get_Partner_Buf_Status(handle);
    

    /* check can we start sending new packet dependine on type of the link 
       used by the model, flow control mode, oustanding packets number etc.
       */
    if ( handle->is_Parallel_Model ) /* 8/16 LP-LVDS link case */
    {
        /* case when we are unable to send new packet is when the link partner 
           is in transmitter flow control mode and have no buffers available */
        if ( free_Buf_Size != 0x0f &&                 /* transmitter flow control mode */
            free_Buf_Size - outstand_Pack_Num <= 0 ) /* receiver still have free buffer space 
                                                         after reception of currently unacknowledged 
                                                         packets which are not indicated in buf_Status */
        {
        
            get_IDLE(handle, granule);
            return;
        };
    }
    else /* 1x/4x LP-Serial link case */
    {
        /* case when we are unable to send new packet is when the link partner 
           is in transmitter flow control mode and have no buffers available */
        if ( free_Buf_Size != 0x1f &&                 /* transmitter flow control mode */
            free_Buf_Size < 0x1e &&                  /* in this case receiver is able to accept more than 30 packets */
            free_Buf_Size - outstand_Pack_Num <= 0 ) /* receiver still have free buffer space 
                                                        after reception of currently unacknowledged 
                                                        packets which are not indicated in buf_Status */
        {
           get_IDLE(handle, granule);
           return;
        };
    }
    
    cur_Ack_Id = handle->tx_Data.ack_In_Buffer.buffer_Top;

   /* GDA: Bug#138 */
    i_Max = RIO_PL_Out_Get_Buf_Size(handle);

     for (i = 0; i < i_Max; i++,j++)
	         {
                         index = ((handle->outbound_Buf.send_Queue) + (i))->index;

			if((RIO_OUT_GET_ENTRY(handle, index))->entry.ftype == RIO_RESPONCE) 
			{
			
				break;
	                  			a[j]=index;
			
			}
			
		 }

    handle->check_Ack_Id=index;



    /* GDA: Bug#138 */
    
    if (RIO_PL_Out_Start_New_Packet(
        handle, 
        granule, 
        &cur_Handle, 
        cur_Ack_Id) == RIO_ERROR
        )
        get_IDLE(handle, granule);
    else
    {
        /* init THROTTLE genarator */
        handle->tx_Data.throttle_Generator.valid = RIO_FALSE;

        handle->tx_Data.ack_In_Buffer.ack_Buffer[cur_Ack_Id].state = RIO_PL_OUT_SENDING;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[cur_Ack_Id].packet_Handle = cur_Handle;
        handle->tx_Data.in_Progress = RIO_TRUE;

        handle->tx_Data.ack_In_Buffer.buffer_Top = (cur_Ack_Id + 1) % handle->ack_Buffer_Size;
        if (handle->tx_Data.ack_In_Buffer.buffer_Top == handle->tx_Data.ack_In_Buffer.buffer_Head)
            handle->tx_Data.ack_In_Buffer.is_Full = RIO_TRUE;
        /*init random idle generator if necessary*/
        if (handle->tx_Data.rnd_Idle_Generator.valid == RIO_TRUE)
        {
            double rand_Res;
            double packet_Len;

            packet_Len = (double)handle->outbound_Arb.packet_Len / RIO_PL_BYTE_CNT_IN_WORD;
 
            /*calculate placement of the idles in the packet
            2 is necessary for value canbe from 1 to packet_lengh - 1
            0.5 is necessary for correct round-off*/
            rand_Res = rand() / (double)RAND_MAX;
            handle->tx_Data.rnd_Idle_Generator.gr_Num = 
                1 + (int)( rand_Res * ( packet_Len - 2) + 0.5);
            /*perform calculation of idles number*/
            rand_Res = rand() / (double)RAND_MAX;
            handle->tx_Data.rnd_Idle_Generator.idle_Cnt = 
                handle->tx_Data.rnd_Idle_Generator.lb + (int)( rand_Res * 
                (handle->tx_Data.rnd_Idle_Generator.hb - handle->tx_Data.rnd_Idle_Generator.lb) + 0.5);
            handle->tx_Data.rnd_Idle_Generator.packet_Ongoing = 
                (handle->tx_Data.rnd_Idle_Generator.idle_Cnt == 0) ? RIO_FALSE : RIO_TRUE;
        }

        
        /* clear stomp generator value sent_STOMP if packet is re-sending */
        if ((handle->tx_Data.stomp_Generator.sent_STOMP == RIO_TRUE) && 
            (handle->tx_Data.stomp_Generator.sent_STOMP_ack_ID == cur_Ack_Id))
        {
            handle->tx_Data.stomp_Generator.sent_STOMP = RIO_FALSE;
        }

        /* init packet stomp generator if necessary */
        if ((handle->tx_Data.stomp_Generator.factor != 0) &&
            (handle->tx_Data.need_STOMP_Due_Gen == RIO_FALSE) &&
            (handle->tx_Data.stomp_Generator.sent_STOMP == RIO_FALSE))
        {
            double rand_Res_s;	 
            unsigned int stomp_Prob; /* probability of packet stomp event */
            rand_Res_s = rand() / (double)RAND_MAX;
            /* 0.5 is necessary for correct round-off*/
            stomp_Prob =  (int)(100 - rand_Res_s * 100 + 0.5);
            if (stomp_Prob <= handle->tx_Data.stomp_Generator.factor)
            {
                handle->tx_Data.stomp_Generator.stomp_Cnt++;
                handle->tx_Data.need_STOMP_Due_Gen = RIO_TRUE;

            }
        }		

        /*set next out port transmitted ackID value in packet*/
        RIO_PL_Set_State(
            handle,
            RIO_OUTBOUND_ACKID, 
            handle->tx_Data.ack_In_Buffer.buffer_Top,
            0
            );

    }
}

/***************************************************************************
 * Function : get_Tx_Current_Index
 *
 * Description: return current transmitting packet index
 *
 * Returns: current transmitting packet index
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Tx_Current_Index(RIO_PL_DS_T *handle)
{
    if (handle == NULL)
        return 0;

    if (handle->tx_Data.ack_In_Buffer.buffer_Top == 0)
        return handle->ack_Buffer_Size - 1;
    else 
        return handle->tx_Data.ack_In_Buffer.buffer_Top - 1;

}

/***************************************************************************
 * Function : send_Packet_Part
 *
 * Description: send the next part of a packet
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void send_Packet_Part(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned int current_Index;
    RIO_BOOL_T is_Last;
    RIO_GRANULE_T next_Granule;

    if (handle == NULL || granule == NULL)
        return;
        
    /* 
     * there are not any THTOTTLE - send next part of a packet 
     * obtain new data and check if packet is fully transmitted
     */

    current_Index = get_Tx_Current_Index(handle);
/*    current_Handle = handle->tx_Data.ack_In_Buffer.ack_Buffer[current_Index].packet_Handle; */

    RIO_PL_Out_Get_Data(handle, &next_Granule, &is_Last);
    if (is_Last == RIO_FALSE)
    {
        *granule = next_Granule;
        return;
    }

    /* commit the packet which has been sent */
    handle->tx_Data.in_Progress = RIO_FALSE;
    handle->tx_Data.ack_In_Buffer.ack_Buffer[current_Index].state = RIO_PL_OUT_SENT ;
/*    handle->tx_Data.ack_In_Buffer.buffer_Top = (current_Index + 1) % handle->ack_Buffer_Size; */

    /* update state of acknowledgment pool*/
    if (handle->tx_Data.ack_In_Buffer.buffer_Head == 
        handle->tx_Data.ack_In_Buffer.buffer_Top
        )
        handle->tx_Data.ack_In_Buffer.is_Full = RIO_TRUE;

    /*check that it is necessary to insert eop at the end of each packet*/
    if (handle->tx_Data.force_EOP == RIO_TRUE)
    {
        get_EOP(handle, granule);
        return;
    }

    /*prevention of sending 8-th unacknowlwdged packet */
    if (handle->tx_Data.ack_In_Buffer.is_Full != RIO_TRUE &&
        ((handle->tx_Data.ack_In_Buffer.buffer_Top + 1) % handle->ack_Buffer_Size) ==
        handle->tx_Data.ack_In_Buffer.buffer_Head)
    {
        get_EOP(handle, granule);
        return;
    }
    /* check if there is any new packet for transmitting */
    if (handle->tx_Data.ack_In_Buffer.is_Full == RIO_TRUE)
    {
        get_EOP(handle, granule);
        return;
    }
    /* try start new packet */
    start_New_Packet(handle, granule);

    /* if there are not any packets for transmission */
    if (handle->tx_Data.in_Progress == RIO_FALSE)
        get_EOP(handle, granule);
}

/***************************************************************************
 * Function : send_Link_Response
 *
 * Description: send LINK RESPONSE control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void send_Link_Response(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    /* send link response and clear response latch */
    *granule = handle->tx_Data.link_Response.control;
    handle->tx_Data.link_Response.valid = RIO_FALSE;
}

/***************************************************************************
 * Function : send_Link_Request
 *
 * Description: send LINK REQUEST control
 *
 * Returns: none
 *
 * Notes: discard currently transmitting packet if necessary
 *
 **************************************************************************/
static void send_Link_Request(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
/*    unsigned long status;*/

    if (handle == NULL || granule == NULL)
        return;

    /* send link request*/
    *granule = handle->tx_Data.link_Request.control;

    /* if it's RESET command clear flags to prevent from response waiting */
    if (handle->tx_Data.link_Request.control.granule_Struct.ack_ID == RIO_PL_LREQ_RESET)
    {
        handle->tx_Data.link_Request.valid = RIO_FALSE;
        handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
        handle->tx_Data.link_Request.retry_Cnt = 0;
        handle->tx_Data.link_Request.time_After_Sending = 0;
    }
    else if (handle->tx_Data.link_Request.control.granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
    {           
    /*    if (handle->tx_Data.state == RIO_PL_UNINIT)
        {
            RIO_GRANULE_T training;
            RIO_PL_Tx_Send_Training(handle, &training);
        }*/
        
        handle->tx_Data.link_Request.training_Counter.valid = RIO_TRUE;
        handle->tx_Data.link_Request.valid = RIO_FALSE;
        handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
        handle->tx_Data.link_Request.retry_Cnt = 0;
        handle->tx_Data.link_Request.time_After_Sending = 0;
        handle->tx_Data.link_Request.training_Counter.time_After_Sending = 0;
    }
    else
    {
        handle->tx_Data.link_Request.is_Sent = RIO_TRUE;
        handle->tx_Data.link_Request.time_After_Sending = 0;
    }
    
    /* terminate current packet if necessary */
    if (handle->tx_Data.in_Progress == RIO_TRUE)
    {
        /* 
         * position of current packet in ack_In_Buffer and the
         * handle of corresponding packet 
         */
        unsigned int current_Ind;

        /* 
         * discard current packet and set next ackID for 
         * sending to corresponding value of discarded packet
         */
        current_Ind = get_Tx_Current_Index(handle);
   /*     discard_Entry(handle, current_Ind);*/
        handle->tx_Data.in_Progress = RIO_FALSE;

        RIO_PL_Set_State(
            handle,
            RIO_SYMBOL_IS_PD, 
            RIO_TRUE,
            0
            );
    }

    /* 
     * set retry cnt values
     */
    if (handle->tx_Data.link_Request.control.granule_Struct.ack_ID == RIO_PL_LREQ_RESET ||
        handle->tx_Data.link_Request.control.granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
    {
        unsigned long status;
        RIO_PL_Read_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_RESP_A,
            &status, RIO_TRUE);
        /*
        status = (unsigned long)0;
        status |= ((unsigned long)handle->tx_Data.link_Request.retry_Cnt) << RIO_PL_BITS_IN_BYTE;
        */
        status |= ((unsigned long)(0x1)) << 31;
        RIO_PL_Write_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_RESP_A,
            status, RIO_TRUE);
    }


}

/***************************************************************************
 * Function : send_Rest_From_Retr
 *
 * Description: send RESTART FROM RETRY control
 *
 * Returns: none
 *
 * Notes: discard currently transmitting packet if necessary
 *
 **************************************************************************/
static void send_Rest_From_Retr(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    /* send link request */
    *granule = handle->tx_Data.restart_From_Retry.control;
    handle->tx_Data.restart_From_Retry.valid = RIO_FALSE;
    
    /* terminate current packet if necessary */
    if (handle->tx_Data.in_Progress == RIO_TRUE)
    {
        /* 
         * position of current packet in ack_In_Buffer and the
         * handle of corresponding packet 
         */
        unsigned int current_Ind;

        /* 
         * discard current packet and set next ackID for 
         * sending to corresponding value of discarded packet
         */
        current_Ind = get_Tx_Current_Index(handle);
        discard_Entry(handle, current_Ind, RIO_FALSE);
        handle->tx_Data.in_Progress = RIO_FALSE;

        RIO_PL_Set_State(
            handle,
            RIO_SYMBOL_IS_PD, 
            RIO_TRUE,
            0
            );

    }
}

/***************************************************************************
 * Function : discard_Entry
 *
 * Description: discard entry in ack_In_Buffer
 *
 * Returns: none
 *
 * Notes: discard all next entry too
 *
 **************************************************************************/
static void discard_Entry(RIO_PL_DS_T *handle, unsigned int index, RIO_BOOL_T is_PNA)
{
    /*  packet handle */
    unsigned int pack_Handle, i, flag = 0;

    /* check if we are in buffer boundaries*/
    if (handle == NULL || index >= handle->ack_Buffer_Size)
        return;

    /* check if index within*/
    if (handle->tx_Data.ack_In_Buffer.is_Full == RIO_TRUE)
        flag = 1;
    else
    {
        for (i = handle->tx_Data.ack_In_Buffer.buffer_Head;
            i != handle->tx_Data.ack_In_Buffer.buffer_Top;
            i = (i + 1) % handle->ack_Buffer_Size)
        {
            if (i == index)
            {
                flag = 1;
                break;
            }
        }
    }

    if (!flag)
        return;

    /* go through acknowledge buffer and discard corresponding packets, if any*/
    i = index;
    do {
        pack_Handle = handle->tx_Data.ack_In_Buffer.ack_Buffer[i].packet_Handle;
        RIO_PL_Out_Packet_Discarded(handle, pack_Handle, is_PNA);
		is_PNA = RIO_FALSE;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[i].state = RIO_PL_OUT_INVALID;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[i].time_After_Sending = 0;
        handle->tx_Data.ack_In_Buffer.ack_Buffer[i].packet_Handle = 0;
        i = (i + 1) % handle->ack_Buffer_Size;
    } while (i != handle->tx_Data.ack_In_Buffer.buffer_Top);

    /* set buffer_Top */
    handle->tx_Data.ack_In_Buffer.buffer_Top = index;
    handle->tx_Data.ack_In_Buffer.is_Full = RIO_FALSE;

    /*set next out port transmitted ackID value in packet*/
    RIO_PL_Set_State(
        handle,
        RIO_OUTBOUND_ACKID, 
        handle->tx_Data.ack_In_Buffer.buffer_Top,
        0
        );
}

/***************************************************************************
 * Function : send_Acknowledge
 *
 * Description: send the next acknowledge
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void send_Acknowledge(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    /* index of current acknowledge in ack_Out_Buffer */
    unsigned int current_Ack;
    
    if (handle == NULL || granule == NULL)
        return;

    /* send the acknowledge*/
    current_Ack = handle->tx_Data.ack_Out_Buffer.buffer_Head;
    
    *granule = handle->tx_Data.ack_Out_Buffer.ack_Buffer[current_Ack];
    handle->tx_Data.ack_Out_Buffer.buffer_Head = (current_Ack + 1) % handle->ack_Buffer_Size;
    handle->tx_Data.ack_Out_Buffer.is_Full = RIO_FALSE;
}

/***************************************************************************
 * Function : get_Restart_From_Retry
 *
 * Description: get proper RESTART FROM RETRY control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void get_Restart_From_Retry(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    granule->granule_Struct.s = 1;
    granule->granule_Struct.ack_ID = RIO_GR_RESTART_FROM_RETRY_SUB_TYPE;
    granule->granule_Struct.buf_Status = 0;
    granule->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
    granule->granule_Type = RIO_GR_RESTART_FROM_RETRY;
}

/***************************************************************************
 * Function : get_Link_Request
 *
 * Description: get proper LINK REQUEST control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Get_Link_Request(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    granule->granule_Struct.s = 1;
    granule->granule_Type = RIO_GR_LINK_REQUEST;
    granule->granule_Struct.s = 1;
    granule->granule_Struct.ack_ID = RIO_LP_LREQ_INPUT_STATUS;

    granule->granule_Struct.buf_Status = RIO_PL_Get_Our_Buf_Status(handle); /*RIO_PL_In_Buf_Status(handle);*/
    
    granule->granule_Struct.stype = RIO_GR_LINK_REQUEST_STYPE;
}

/***************************************************************************
 * Function : RIO_PL_Tx_Link_Req_Time_Out
 *
 * Description: count time out for sent LINK REQUEST
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Link_Req_Time_Out(RIO_PL_DS_T *handle)
{
    unsigned long state;

    if (handle == NULL)
        return;

    /* if time-out is zero don't check it at all*/
    if (!handle->tx_Data.tx_Param_Set.lresp_Time_Out)
        return;

    /* check time-out */
    if (handle->tx_Data.link_Request.valid == RIO_TRUE &&
        handle->tx_Data.link_Request.is_Sent == RIO_TRUE)
    {
     
 	
        /* check if time-out event has just happened*/
        if (handle->tx_Data.link_Request.time_After_Sending >=
            (handle->tx_Data.tx_Param_Set.lresp_Time_Out))    
        {
            if (handle->parameters_Set.enable_LRQ_Resending == RIO_TRUE)
            {
                /*maintis id 44 modification
                in case of initialization parameters the model can
                resend the link request symbol in case of timeout*/
                RIO_PL_Print_Message(handle, RIO_PL_WARNING_14);
                /*reset previous link request*/
                handle->tx_Data.link_Request.valid = RIO_FALSE;
                /*prepare new link request symbol*/
                prepare_Link_Request_For_Tx_Error_Rec(handle);
                return;
            }

            /* print diagnostic */
            RIO_PL_Print_Message(handle, RIO_PL_WARNING_3, 
                (unsigned long)(handle->tx_Data.link_Request.time_After_Sending));
            
            /* check if retry limit is overstepped */
/*            if (handle->tx_Data.link_Request.retry_Cnt >= 
                handle->tx_Data.tx_Param_Set.lresp_Retry_Cnt) *//*retry count shall be removed*/
           /* {*/
                /* print diagnostic */
              /*  RIO_PL_Print_Message(handle, RIO_PL_ERROR_24, 
                    (unsigned long)(handle->tx_Data.link_Request.retry_Cnt));*/
                
                /* delete link request from schedule*/
                handle->tx_Data.link_Request.valid = RIO_FALSE;
                handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
                handle->tx_Data.link_Request.time_After_Sending = 0;
                handle->tx_Data.link_Request.retry_Cnt = 0;

                if (handle->tx_Data.state != RIO_PL_UNINIT)
                {
                    RIO_PL_Print_Message(handle, RIO_PL_ERROR_51);
                    handle->tx_Data.state = RIO_PL_ERROR;
                            
                    /*need write to the register*/
                    RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
                        handle->inst_Param_Set.entry_Extended_Features_Ptr, &state, RIO_TRUE);
                    /* clear  bits exclude width*/
                    state &= RIO_PL_ERROR_PORT_STATE_CLEAR_MASK;
                    /* set OK bit for transmitter and receiver */
                    state |= RIO_PL_ERROR_STATE_UNREC_ERROR;
                    RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A +
                        handle->inst_Param_Set.entry_Extended_Features_Ptr, state, RIO_TRUE);

                }
           /* }
            else
            {*/
                /* reshedule link request for sending */
/*                handle->tx_Data.link_Request.retry_Cnt++;
                handle->tx_Data.link_Request.time_After_Sending = 0;
                handle->tx_Data.link_Request.valid = RIO_TRUE;
                handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
            }
*/
        }
        else
            handle->tx_Data.link_Request.time_After_Sending++;

    } /* end link request time-out proceding */
}
/***************************************************************************
 * Function : RIO_PL_Tx_Link_Req_Training_Time_Out
 *
 * Description: count time out for sent LINK REQUEST/send_Training
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Link_Req_Training_Time_Out(RIO_PL_DS_T *handle)
{
    unsigned long state;

    if (handle == NULL)
        return;

    /* if time-out is zero don't check it at all*/
    if (!handle->tx_Data.tx_Param_Set.lresp_Time_Out)
        return;

    /* check time-out */
    if (handle->tx_Data.link_Request.training_Counter.valid == RIO_TRUE &&
        handle->tx_Data.link_Request.training_Counter.training_Cnt == 0)
    {

        
	
        /* check if time-out event has just happened*/
        if (handle->tx_Data.link_Request.training_Counter.time_After_Sending >=
            handle->tx_Data.tx_Param_Set.lresp_Time_Out)
        {
            /* print diagnostic */
            RIO_PL_Print_Message(handle, RIO_PL_WARNING_3, 
                (unsigned long)(handle->tx_Data.link_Request.training_Counter.time_After_Sending));
            
            if (handle->tx_Data.state != RIO_PL_UNINIT)
            {
               RIO_PL_Print_Message(handle, RIO_PL_ERROR_51);
               handle->tx_Data.state = RIO_PL_ERROR;
               handle->tx_Data.link_Request.training_Counter.valid = RIO_FALSE;
               handle->tx_Data.link_Request.training_Counter.time_After_Sending = 0;
               /*need write to the register*/
               RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
                   handle->inst_Param_Set.entry_Extended_Features_Ptr, &state, RIO_TRUE);
               /* clear  bits exclude width*/
               state &= RIO_PL_ERROR_PORT_STATE_CLEAR_MASK;
               /* set OK bit for transmitter and receiver */
               state |= RIO_PL_ERROR_STATE_UNREC_ERROR;
               RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
                    handle->inst_Param_Set.entry_Extended_Features_Ptr, state, RIO_TRUE);

             }

        }
        else
            handle->tx_Data.link_Request.training_Counter.time_After_Sending++;

    } /* end link request/send training time-out proceding */
}
/***************************************************************************
 * Function : RIO_PL_Tx_Phys_Time_Out
 *
 * Description: count physical layer time out 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Phys_Time_Out(RIO_PL_DS_T *handle)
{
    unsigned int i = 0; /* loop counter */
 
 
    if (handle == NULL)
        return;

     
    /* if time-out value 0 time-out feature is turned off*/
    if (!handle->tx_Data.tx_Param_Set.lresp_Time_Out)
        return;
  

    /* detect if there are any entries for counting */
    if (handle->tx_Data.ack_In_Buffer.buffer_Head == 
        handle->tx_Data.ack_In_Buffer.buffer_Top &&
        handle->tx_Data.ack_In_Buffer.is_Full == RIO_FALSE)
        return;


        /* go through acknowledge buffer and detect time-out event, if any*/
    i = handle->tx_Data.ack_In_Buffer.buffer_Head;

    do {
        if (handle->tx_Data.ack_In_Buffer.ack_Buffer[i].state == RIO_PL_OUT_SENT)
        {

            if (handle->tx_Data.ack_In_Buffer.ack_Buffer[i].time_After_Sending >=
                handle->tx_Data.tx_Param_Set.lresp_Time_Out)
            {
		/*   RIO_PL_Out_Packet_Time_Out(
                    handle, 
                    handle->tx_Data.ack_In_Buffer.ack_Buffer[i].packet_Handle,
                    (unsigned long)handle->tx_Data.ack_In_Buffer.ack_Buffer[i].time_After_Sending
                    );*/
		
                RIO_PL_Print_Message(handle, RIO_PL_ERROR_56, (unsigned long)i,
                    handle->tx_Data.ack_In_Buffer.ack_Buffer[i].time_After_Sending);
                
                handle->tx_Data.ack_In_Buffer.ack_Buffer[i].time_After_Sending = 0;
                handle->tx_Data.ack_In_Buffer.ack_Buffer[i].state = RIO_PL_OUT_TIMEOUTED; 
                /* discard entries starting from the time-outed - Already it was*/
                /*discard_Entry(handle, i);-- Already it was*/
                start_Tx_Error_Rec(handle);

            }
            else
                handle->tx_Data.ack_In_Buffer.ack_Buffer[i].time_After_Sending++;
        }
        /* locate the next entry - - Already it was */
        i = (i + 1) % handle->ack_Buffer_Size;
    } while (i != handle->tx_Data.ack_In_Buffer.buffer_Top);
}

/***************************************************************************
 * Function : RIO_PL_Tx_Send_Throttle
 *
 * Description: store Throttle
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Send_Throttle(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    handle->tx_Data.throttle_For_Send.control = *granule;
    handle->tx_Data.throttle_For_Send.valid = RIO_TRUE;
}

/***************************************************************************
 * Function : send_Throttle
 *
 * Description: send Throttle
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void send_Throttle
    (RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    handle->tx_Data.throttle_For_Send.valid = RIO_FALSE;
    *granule = handle->tx_Data.throttle_For_Send.control;
}

/***************************************************************************
 * Function : send_Training
 *
 * Description: send Training
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#ifndef _CARBON_SRIO_XTOR
static void send_Training
    (RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL || handle->tx_Data.training_Generator.training_Cnt == 0)
        return;


    
    if (handle->tx_Data.in_Progress == RIO_TRUE)
    {
        /* discard current packet*/
        discard_Entry(handle, get_Tx_Current_Index(handle), RIO_FALSE);
        get_STOMP(handle, granule);
        handle->tx_Data.need_STOMP = RIO_FALSE;
        handle->tx_Data.in_Progress = RIO_FALSE;

        /* init THROTTLE genarator */
        handle->tx_Data.throttle_Generator.valid = RIO_FALSE;
        return;
    }

    granule->granule_Type = RIO_GR_TRAINING;

    handle->tx_Data.training_Generator.training_Cnt--;
    if(handle->tx_Data.training_Generator.training_Cnt == 0)
      {
       handle->tx_Data.training_Generator.valid = RIO_FALSE;
      }
#ifdef _DEBUG
    /* prints bytes the 0th to the 3th */
#ifndef _CARBON_SRIO_XTOR
    printf("Transmitter D`%lu: training num D`%d\n",
    handle->instance_Number, handle->tx_Data.training_Generator.training_Cnt + 1);
#endif
#endif
}
#endif

/***************************************************************************
 * Function : prepare_Link_Request_For_Tx_Error_Rec
 *
 * Description: prepares the link request symbol for issuing one on the link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void prepare_Link_Request_For_Tx_Error_Rec(RIO_PL_DS_T *handle)
{
    if (handle->tx_Data.link_Request.valid == RIO_FALSE)
    {
        handle->tx_Data.link_Request.valid = RIO_TRUE;
        handle->tx_Data.link_Request.is_Sent = RIO_FALSE;
        handle->tx_Data.link_Request.time_After_Sending = 0;
        handle->tx_Data.link_Request.retry_Cnt = 0;
        RIO_PL_Tx_Get_Link_Request(handle, &(handle->tx_Data.link_Request.control));

        /*this action is necessary to prevent unexpected RFR error
        (clear transition from output retry into output error state)*/
        if (handle->tx_Data.restart_From_Retry.valid == RIO_TRUE)
            handle->tx_Data.restart_From_Retry.valid = RIO_FALSE;
    }   
}
/***************************************************************************
 * Function : start_Tx_Error_Rec
 *
 * Description: starts output error recovery mechanizm
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void start_Tx_Error_Rec(RIO_PL_DS_T *handle)
{
    unsigned long state;
    
    if (handle == NULL) return;

    if (handle->tx_Data.state != RIO_PL_OK &&
        handle->tx_Data.state != RIO_PL_STOPPED_DUE_TO_RETRY) return;
    
    handle->tx_Data.state = RIO_PL_STOPPED_DUE_TO_ERROR;

    prepare_Link_Request_For_Tx_Error_Rec(handle);

    /* update state in the register */
    RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
        handle->inst_Param_Set.entry_Extended_Features_Ptr, &state, RIO_TRUE);
    /* clear transmitter bits*/
    state &=  RIO_PL_ERROR_TX_STATE_CLEAR_MASK;
    /* set stopped due to error bit */
    state |= RIO_PL_ERROR_TX_ERROR;
    RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
        handle->inst_Param_Set.entry_Extended_Features_Ptr, state, RIO_TRUE);
    return;
}

/***************************************************************************
 * Function : RIO_PL_Tx_Rec_Unexp_Packet_Control
 *
 * Description:  an unexpected packet control has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Rec_Unexp_Packet_Control(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL )
        return;
    RIO_PL_Print_Message(handle, RIO_PL_ERROR_48, granule->granule_Type);
    start_Tx_Error_Rec(handle);
    return;
  
}

/***************************************************************************
 * Function : RIO_PL_Tx_Rec_Training
 *
 * Description:  a training pattern has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Rec_Training_Seq(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned long state;

    if (handle == NULL || granule == NULL)
        return;

    
    switch(granule->granule_Type)
    {
        case RIO_GR_TRAINING:
            /* check if a link request already exists */
            if (handle->tx_Data.link_Request.training_Counter.valid != RIO_TRUE)   
            {
                RIO_PL_Print_Message(handle, RIO_PL_ERROR_50);
                start_Tx_Error_Rec(handle);
                return;
            }
            /* store new Training pattern  to latch */
            if (++handle->tx_Data.link_Request.training_Counter.training_Cnt > RIO_PL_TRAINING_NUM)
                break;
            return;
        case RIO_GR_LINK_REQUEST:
            if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
                return;
            else 
                break;
        case RIO_GR_IDLE:
            if (handle->tx_Data.link_Request.training_Counter.training_Cnt == RIO_PL_TRAINING_NUM)
            {
                
                RIO_PL_Print_Message(handle, RIO_PL_WARNING_9,
                    handle->tx_Data.link_Request.training_Counter.training_Cnt);

                handle->tx_Data.link_Request.training_Counter.valid = RIO_FALSE;
                handle->tx_Data.link_Request.training_Counter.time_After_Sending = 0;
                handle->tx_Data.link_Request.training_Counter.training_Cnt = 0;
                return;
            }
            else
                break;
        default: {} 
    }
    /*unrecoverable error has been detected*/

    if (handle->tx_Data.state != RIO_PL_UNINIT && 
        handle->tx_Data.link_Request.training_Counter.valid == RIO_TRUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_51);
        handle->tx_Data.state = RIO_PL_ERROR;
        handle->tx_Data.link_Request.training_Counter.valid = RIO_FALSE;
        handle->tx_Data.link_Request.training_Counter.time_After_Sending = 0;
        handle->tx_Data.link_Request.training_Counter.training_Cnt = 0;
        /*need write to the register*/
        RIO_PL_Read_Register(handle, RIO_LINK_ERROR_A + 
            handle->inst_Param_Set.entry_Extended_Features_Ptr, &state, RIO_TRUE);
        /* clear  bits exclude width*/
        state &= RIO_PL_ERROR_PORT_STATE_CLEAR_MASK;
        /* set OK bit for transmitter and receiver */
        state |= RIO_PL_ERROR_STATE_UNREC_ERROR;
        RIO_PL_Write_Register(handle, RIO_LINK_ERROR_A + 
            handle->inst_Param_Set.entry_Extended_Features_Ptr, state, RIO_TRUE);
    }
    else if(handle->tx_Data.state == RIO_PL_UNINIT)
    {
        RIO_PL_Print_Message(handle, RIO_PL_WARNING_9,
        handle->tx_Data.link_Request.training_Counter.training_Cnt);
        handle->tx_Data.link_Request.training_Counter.valid = RIO_FALSE;
        handle->tx_Data.link_Request.training_Counter.time_After_Sending = 0;
        handle->tx_Data.link_Request.training_Counter.training_Cnt = 0;
    }
    return;
}

/***************************************************************************
 * Function : RIO_PL_Control_Is_Expected
 *
 * Description: detect, if received conrol was expected by transmitter
 *
 * Returns: RIO_TRUE if control was expected
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T RIO_PL_Tx_Control_Is_Expected(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return RIO_FALSE;

      if ((granule->granule_Type == RIO_GR_TRAINING) && 
        (handle->tx_Data.link_Request.training_Counter.valid == RIO_FALSE))
        return RIO_FALSE;
    if ((granule->granule_Type == RIO_GR_LINK_REQUEST) && 
        (handle->tx_Data.link_Response.valid == RIO_TRUE))
        return RIO_FALSE;
    return RIO_TRUE;
}

/***************************************************************************
 * Function : RIO_PL_Tx_Rec_Link_Rq
 *
 * Description:  a link request/send_training has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_PL_Tx_Rec_Link_Req(RIO_PL_DS_T *handle, RIO_GRANULE_T *link_Request)
{
    /* status of link response register */

    if (handle == NULL || link_Request == NULL)
        return;
   

    /*change buf_status*/
    RIO_PL_Set_State(handle, RIO_PARTNER_BUF_STATUS, link_Request->granule_Struct.buf_Status, 0);

    if (link_Request->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING) 
    {
        /*if (handle->tx_Data.train_state == RIO_PL_TX_OK)
            handle->tx_Data.train_state = RIO_PL_TX_OK_SEND_TRN_PTTN;*/
        /*-----------!!!!The function to change state shall be called!!!-----*/
    }
    return;
}
/***************************************************************************
 * Function : RIO_PL_Get_Our_Buf_Status
 *
 * Description: return status of inbound buffer
 *
 * Returns: number of free entry
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Our_Buf_Status(RIO_PL_DS_T *handle)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Param;

    if (handle == NULL)
        return 0;

 
    /*get current buffer state*/
    RIO_PL_Get_State(handle, RIO_FLOW_CONTROL_MODE, &indicator_Value, &indicator_Param);
    if (indicator_Value  == RIO_TRUE) /*transmit flow control*/
    {
        RIO_PL_Get_State(handle, RIO_OUR_BUF_STATUS, &indicator_Value, &indicator_Param);
        return indicator_Value;
    }
    else if (handle->is_Parallel_Model == RIO_TRUE)
        return 0xf;
    else
        return 0x1f;
}

/***************************************************************************
 * Function : RIO_PL_Get_Partner_Buf_Status
 *
 * Description: return status of inbound buffer
 *
 * Returns: number of free entry
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Partner_Buf_Status(RIO_PL_DS_T *handle)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Param;

    if (handle == NULL)
        return 0;

    /*get current buffer state*/
    RIO_PL_Get_State(handle, RIO_FLOW_CONTROL_MODE, &indicator_Value, &indicator_Param);
    if (indicator_Value  == RIO_TRUE) /*transmit flow control*/
    {
        RIO_PL_Get_State(handle, RIO_PARTNER_BUF_STATUS, &indicator_Value, &indicator_Param);
        return indicator_Value;
    }
    else if (handle->is_Parallel_Model == RIO_TRUE)
        return 0xf;
    else
        return 0x1f;

}

/***************************************************************************
 * Function : check_States
 *
 * Description: 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void check_States(RIO_PL_DS_T *handle)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Parameter;

    if (handle == NULL)
        return;

    RIO_PL_Get_State(handle, RIO_UNREC_ERROR, &indicator_Value, &indicator_Parameter);

    if (indicator_Value == RIO_TRUE && handle->tx_Data.state != RIO_PL_ERROR)
    {
        handle->tx_Data.state = RIO_PL_ERROR;
        return;
    }    

    RIO_PL_Get_State(handle, RIO_OUTPUT_ERROR, &indicator_Value, &indicator_Parameter);

    if (indicator_Value == RIO_TRUE && (handle->tx_Data.state == RIO_PL_OK ||
        handle->tx_Data.state == RIO_PL_STOPPED_DUE_TO_RETRY))
        start_Tx_Error_Rec(handle);

    return;
}

/*****************************************************************************
 * Function : ack_Delay_Time_Out
 *
 * Description: count ack delay time out 
 *
 * Returns: none
 *
 * Notes: GDA Included for ack delay purpose 
 *
 ******************************************************************************/
void ack_Delay_Time_Out(RIO_PL_DS_T *handle)
{

   if (handle == NULL)
          return;

   if (handle->inst_Param_Set.ack_Delay_Min > handle->inst_Param_Set.ack_Delay_Max)
	handle->inst_Param_Set.ack_Delay_Enable = RIO_FALSE;

   if  ((handle->tx_Data.ack_Out_Buffer.is_Full ||
         handle->tx_Data.ack_Out_Buffer.buffer_Top !=
         handle->tx_Data.ack_Out_Buffer.buffer_Head )&& 
        (handle->inst_Param_Set.ack_Delay_Enable==RIO_TRUE))
   {  
     	if (!handle->set_Random)
	{
	    handle->ack_Cycle_Delay_Counter=0;     /* Reset the ack_Cycle_Delay_Counter */
	    handle->ack_Delay_Period = random_Delay_Period(handle->inst_Param_Set.ack_Delay_Min,
							    handle->inst_Param_Set.ack_Delay_Max);
	    handle->set_Random=RIO_TRUE;
	}

	handle->ack_Cycle_Delay_Counter++;

        if (handle->ack_Cycle_Delay_Counter>=handle->ack_Delay_Period)
	    handle->inst_Param_Set.ack_Delay_Enable=RIO_FALSE;

   }
}

/*****************************************************************************
 * Function : random_Delay_Period
 *
 * Description:  generates random delay period within min & max range
 *
 * Returns: none
 *
 * Notes: GDA Included for ack delay purpose 
 *
 ******************************************************************************/
unsigned long random_Delay_Period(RIO_WORD_T min_Delay,RIO_WORD_T max_Delay)
{
   unsigned long delay_Differnce = 0;
   unsigned long delay_Period = 0;

#ifndef _CARBON_SRIO_XTOR
   printf("\n\tMIN value = D`%d; MAX value = D`%d\n",min_Delay,max_Delay);  
#endif
   
   delay_Differnce = (max_Delay - min_Delay ) + 1;
   delay_Period    = ((rand()%delay_Differnce)+ min_Delay) * 2;

#ifndef _CARBON_SRIO_XTOR
   printf("\n\tDelay_Period: D`%u\n",delay_Period);
#endif
   
   return delay_Period;
}

/****************************************************************************
 * Function : get_MULTICAST
 *
 * Description: form proper Multicast event contro symbol for sending
 *
 * Returns: none
 *
 * Notes: GDA Implemented  Bug#116   
 *
 **************************************************************************/
static void get_Multicast(RIO_PL_DS_T *handle, RIO_GRANULE_T *multicast)
{
    if (handle == NULL || multicast == NULL)
        return;
   
    multicast->granule_Type = RIO_GR_TOD_SYNC;
    multicast->granule_Struct.s = 1;
    multicast->granule_Struct.ack_ID = RIO_GR_TOD_SYNC_SUB_TYPE;
    multicast->granule_Struct.buf_Status = RIO_PL_Get_Our_Buf_Status(handle); 
    multicast->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;

#ifndef _CARBON_SRIO_XTOR
    printf("\n\tMulticast Control Symbol Transmitted [Inst Numr: D`%u]\n",handle->instance_Number);
#endif

}
/*****************************************************************************
 * Function : multicast_Delay_Time_Out
 *
 * Description: count multicast delay time out 
 *
 * Returns: none
 *
 * Notes: GDA Included for multicast control symbol delay purpose (Bug#116) 
 *
 ******************************************************************************/
void multicast_Delay_Time_Out(RIO_PL_DS_T *handle)
{

   if (handle == NULL)
       return;

   if (handle->multicast_Enable_Flag == RIO_TRUE)  
   {
      if (!handle->set_Multicast_Rnd_Generator_Flag)
      {
	handle->multicast_Clock_Cycle_Counter=0;  /* Reset multicast_Clock_Cycle_Counter*/
	handle->multicast_Clock_Cycle_Period = random_Delay_Period(
						handle->multicast_Low_Boundary,
						handle->multicast_High_Boundary);

	handle->set_Multicast_Rnd_Generator_Flag = RIO_TRUE;
      }

      handle->multicast_Clock_Cycle_Counter++;

      if (handle->multicast_Clock_Cycle_Counter>=handle->multicast_Clock_Cycle_Period)
      {
#ifndef _CARBON_SRIO_XTOR
	printf("\n\t(Multicast Time Elapsed)\n");
#endif
	handle->set_Multicast_Flag=RIO_TRUE;
	handle->multicast_Enable_Flag = RIO_FALSE;
      }
    }          
}


/*****************************************************************************
* GDA: Bug#138
 *
 * Function : response_Delay_Time_Out
 *
 * Description: count response delay time out 
 *
 * Returns: none
 *
 * Notes: GDA Included for response delay as per bug#138 
 *
 ******************************************************************************/
void response_Delay_Time_Out(RIO_PL_DS_T *handle, RIO_GRANULE_T * granule)
{
  (void)granule;
  static int delay_cnt = 0;
  static unsigned int tmp_ack=40;

  if (handle == NULL)
     return;

  

  if(tmp_ack==handle->check_Ack_Id)
	  return;



  if(handle->resp_Packet_Delay_Enable == RIO_TRUE && handle->resp_Packet_Delay_Cntr <= 0)
  {
	handle->resp_Packet_Delay_Enable = RIO_FALSE;
#ifndef _CARBON_SRIO_XTOR
	printf("\n[Inst %d]Enable & cntr <= 0\n", handle->instance_Number);
#endif
  }
  else if(handle->resp_Packet_Delay_Cntr > 0)/*handle->resp_Packet_Delay_Enable == RIO_TRUE && handle->resp_Packet_Delay_Cntr > 0)*/
  {
    handle->resp_Packet_Delay_Cntr--;
    if(handle->resp_Packet_Delay_Cntr <= 0)
    {
/*	handle->resp_Packet_Delay_Enable = RIO_FALSE;*/
	  handle->resp_Packet_Send=RIO_TRUE;
	  (RIO_OUT_GET_ENTRY(handle, handle->check_Ack_Id))->entry.resp_Delay = RIO_TRUE;
 
	 tmp_ack=handle->check_Ack_Id;

#ifndef _CARBON_SRIO_XTOR
	  printf("\n  tmp ack  %d  current ack is %d \n",tmp_ack,handle->check_Ack_Id);
	printf("\n[Inst %d]resp_Packet_Delay_Enable: %d; delay_cnt: %d\n", handle->instance_Number,\
			handle->resp_Packet_Delay_Enable, ++delay_cnt);
#else
	++delay_cnt;
#endif
	
    }
  
  }

}

