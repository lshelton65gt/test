/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/rio_pl_pllight_model_common.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                       to display revision history information.
*
* Description:  Physical layer source
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

#include "rio_pl_pllight_model_common.h"
#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_rx.h"
#include "rio_pl_tx.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"

#include "rio_common.h"		/* GDA - GDA_PKT_SIZE is defined */

/***************************************************************************
 * Function : RIO_PL_Print_Version
 *
 * Description: prints version of the model
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Print_Version(
    RIO_HANDLE_T handle
)
{
    RIO_PL_DS_T *instance;
    char        buffer[255];

    /* check handle */
    if ( handle == NULL )
        return RIO_ERROR;

    /* convert handle to the data structure pointer */
    instance = (RIO_PL_DS_T*)handle;

    /* check if PL instance was bound already */
    if ( !instance->was_Bound || instance->ext_Interfaces.rio_User_Msg == NULL )
        return RIO_ERROR;
    
    /* print out the version info*/
    sprintf( buffer, "RIO PL Model %s\n", RIO_MODEL_VERSION );

    instance->ext_Interfaces.rio_User_Msg(
        instance->ext_Interfaces.rio_Msg, 
        buffer
    );

    return RIO_OK;
}



/***************************************************************************
 * Function : RIO_PL_Detect_IO_Size_Wdprt
 *
 * Description: detect size and wdptr of IO transactions
 *
 * Returns: 0 - RIO_OK, RIO_RETRY - need misaligned, RIO_ERROR - error
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Detect_IO_Size_Wdprt(
    unsigned int *size, 
    unsigned int *wdptr, 
    unsigned int dw_Size, 
    unsigned int byte_Num,
    unsigned int offset)
{
    if (size == NULL || wdptr == NULL)
        return RIO_ERROR;
    /*
     * this function is used to convert payload from count of double-words to
     * specification defined encoding. So this is the only place there these constants
     * are used in this sense. Corrections in case of specification changing
     * must be here only.
     */

    /* at first check subdouble-word payload */
    if (dw_Size == 1)
    {
        /* check for errors*/
        if (byte_Num == 0 || 
            offset > RIO_PL_MAX_OFFSET_IN_DWORD || 
            byte_Num > RIO_PL_BYTE_CNT_IN_DWORD)
            return RIO_ERROR;

        /* modify byte_Num to prevent cross the boundary */
        if ((offset + byte_Num) > RIO_PL_BYTE_CNT_IN_DWORD)
            byte_Num = (RIO_UCHAR_T)(RIO_PL_BYTE_CNT_IN_DWORD - offset);

        /* detect fields */
        switch (byte_Num)
        {
            /* ALWAYS ALIGNED */
            case 1:
                /* detect wdptr */
                if (offset >= RIO_PL_THE_SECOND_WORD_OFFSET)
                    *wdptr = 1;
                else
                    *wdptr = 0;
                /* detect size */
                *size = offset % 4;
                return RIO_OK;

            /*aligned on even addresses only */
            case 2:
                /* detect misaligned engine */
                if (offset % 2)
                {
                    return RIO_RETRY;
                }
                else
                {
                    if (offset >= RIO_PL_THE_SECOND_WORD_OFFSET)
                        *wdptr = 1;
                    else
                        *wdptr = 0;

                    /* CALCULATE SIZE */
                    *size = 4 + offset % 4;
                    return RIO_OK;
                }

            /* aligned on word addresses only */
            case 4:
                /* detect misaligned assess */
                if (offset % 4)
                    return RIO_RETRY;
                else
                {
                    if (offset)
                        *wdptr = 1;
                    else
                        *wdptr = 0;

                    *size = 8;
                    return RIO_OK;
                }


            case 3:
                                /* detect misaligned assess */
                if (offset % 5)
                    return RIO_RETRY;
                else
                {
                    if (offset)
                        *wdptr = 1;
                    else
                        *wdptr = 0;

                    *size = 5;
                    return RIO_OK;
                }
            case 5:
                                /* detect misaligned assess */
                if (offset % 3)
                    return RIO_RETRY;
                else
                {
                    if (offset)
                        *wdptr = 1;
                    else
                        *wdptr = 0;

                    *size = 7;
                    return RIO_OK;
                }
            case 6:
                                /* detect misaligned assess */
                if (offset % 2)
                    return RIO_RETRY;
                else
                {
                    if (offset)
                        *wdptr = 1;
                    else
                        *wdptr = 0;

                    *size = 9;
                    return RIO_OK;
                }
            case 7:
                                /* detect misaligned assess */
                if (offset % 1)
                    return RIO_RETRY;
                else
                {
                    if (offset)
                        *wdptr = 1;
                    else
                        *wdptr = 0;

                    *size = 10;
                    return RIO_OK;
                }
            /* single double-words */
            case 8:
                *size = 11;
                *wdptr = 0;
                return RIO_OK;
        }
    }

    /* OK */
    if (dw_Size == 2)
    {
        *size = 11;
        *wdptr = 1;
        return RIO_OK;
    }

    if (dw_Size <= 4)
    {
        *size = 12;
        *wdptr = 0;
        return RIO_OK;
    }

    if (dw_Size <= 8)
    {
        *size = 12;
        *wdptr = 1;
        return RIO_OK;
    }

        if (dw_Size == 12)
    {
        *size = 13;
        *wdptr = 0;
        return RIO_OK;
    }

    if (dw_Size <= 16)
    {
        *size = 13;
        *wdptr = 1;
        return RIO_OK;
    }

        if (dw_Size == 20)
    {
        *size = 14;
        *wdptr = 0;
        return RIO_OK;
    }

        if (dw_Size == 24)
    {
        *size = 14;
        *wdptr = 1;
        return RIO_OK;
    }

        if (dw_Size == 28)
    {
        *size = 15;
        *wdptr = 0;
        return RIO_OK;
    }

    if (dw_Size <= GDA_PKT_SIZE) 	/* GDA */
    {
        *size = 15;
        *wdptr = 1;
        return RIO_OK;
    }

    /* it's an error */
   return RIO_ERROR; /* commented */
  }




/***************************************************************************
 * Function : RIO_PL_Ack_Remote_Req
 *
 * Description: notify remote request's been accepted
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Ack_Remote_Req(
    RIO_HANDLE_T handle, 
    RIO_TAG_T    tag
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /* tell the inbound buffer about request acknowledge by the environment */
    RIO_PL_In_Ack_Ext_Req(handle, tag);
 
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Set_Config_Reg
 *
 * Description: set value for configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset, 
    unsigned long     data
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /* try to set register */
    return RIO_PL_Write_Register(instanse, config_Offset, data, RIO_FALSE);
}

/***************************************************************************
 * Function : RIO_PL_Get_Config_Reg
 *
 * Description: get value of configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset, 
    unsigned long     *data
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL || data == NULL)
        return RIO_ERROR;

    /* try to read register */
    return RIO_PL_Read_Register(instanse, config_Offset, data, RIO_FALSE);
}



/***************************************************************************
 * Function : RIO_PL_Get_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Granule(
    RIO_CONTEXT_T context,   /* my handle */
    RIO_HANDLE_T  handle,    /* invokater's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    )
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check pointers and state */
    if (instanse == NULL || handle == NULL || 
        granule == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if (instanse->mid_Block_Instanse != handle)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_6); 
        return RIO_ERROR;
    }


    /* obtain granule for transmitting */
    RIO_PL_Granule_For_Transmitting(instanse, granule);

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Put_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    )
{
    /* instanse handler */
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    if (instanse == NULL || handle == NULL || granule == NULL)
        return RIO_ERROR;

    /* check if call comes from proper link */
    if (instanse->mid_Block_Instanse != handle)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_7);
        return RIO_ERROR;
    }

    /* pass the granule to receiver */
    RIO_PL_Granule_Received(instanse, granule);

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Set_State_Flag
 *
 * Description: set value for configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            indicator_Value,
    unsigned int             indicator_Param
    )
{
    (void)context;
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /* try to set register */
    return RIO_PL_Set_State(instanse, indicator_ID, indicator_Value, indicator_Param);
}

/***************************************************************************
 * Function : RIO_PL_Get_State_Flag
 *
 * Description: get value of configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param
    )
{
    (void)context;
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL || indicator_Value == NULL || indicator_Param == NULL)
        return RIO_ERROR;

    /* try to read register */
    return RIO_PL_Get_State(instanse, indicator_ID, indicator_Value, indicator_Param);
}

/***************************************************************************
 * Function : RIO_PL_Msg
 *
 * Description: calls pl_Print_Message for 32 bit tx/rx
 *              uses message type to map to one of 32txrx special entries in 
 *              message array
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Msg(
    RIO_CONTEXT_T     context,    /* callback context */
    RIO_PL_MSG_TYPE_T msg_Type,
    char              *string
    )
{
    RIO_PL_MESSAGES_T message_Num;
    /* 
     * context here is instanse handle because this function is 
     * invoking as inside pl so from txrx instanse
     */
    RIO_PL_DS_T *handle = (RIO_PL_DS_T*)context;    

    if (handle == NULL || string == NULL)
        return RIO_ERROR;

    switch( msg_Type )
    {
        case RIO_PL_MSG_WARNING_MSG:
            message_Num = RIO_PL_WARNING_1;
            break;
        
        case RIO_PL_MSG_ERROR_MSG:
            message_Num = RIO_PL_ERROR_1;
            break;
            
        case RIO_PL_MSG_NOTE_MSG:
            message_Num = RIO_PL_NOTE_1;
            break;
            
        default:
            return RIO_ERROR;
    }
    
    /* print warning */
    return RIO_PL_Print_Message(
        handle, 
        message_Num,
        string);
}


/***************************************************************************
 * Function : RIO_PL_Set_Retry_Generator
 *
 * Description: Set packet retry generator
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_Retry_Generator(
    RIO_HANDLE_T handle,  
    unsigned int    retry_Prob_Prio0,
    unsigned int    retry_Prob_Prio1,
    unsigned int    retry_Prob_Prio2,
    unsigned int    retry_Prob_Prio3
)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /*check parameters*/
    if (retry_Prob_Prio0 > RIO_ALL_PACKETS || retry_Prob_Prio1 > RIO_ALL_PACKETS || retry_Prob_Prio2 > RIO_ALL_PACKETS || retry_Prob_Prio3 > RIO_ALL_PACKETS)
        return RIO_PARAM_INVALID;

#ifndef _CARBON_SRIO_XTOR
    /* GDA Bug#130*/
    printf("\nBFM RETRY RND() prob_prio0:%u prob_prio1:%u prob_prio2:%u  prob_prio3:%u \n",retry_Prob_Prio0,
                                                         retry_Prob_Prio1,retry_Prob_Prio2,retry_Prob_Prio3); 
#endif
    /*set retry generator*/
    instanse->rx_Data.retry_Generator.prob_Prio0 = retry_Prob_Prio0; /* probability in percents */
    instanse->rx_Data.retry_Generator.prob_Prio1 = retry_Prob_Prio1; /* probability in percents */
    instanse->rx_Data.retry_Generator.prob_Prio2 = retry_Prob_Prio2; /* probability in percents */
    instanse->rx_Data.retry_Generator.prob_Prio3 = retry_Prob_Prio3; /* probability in percents */
    instanse->rx_Data.retry_Generator.retry_Cnt = 0; /* reset retry counter */

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Set_PNACC_Generator
 *
 * Description: Set packet-not-accepted generator
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_PNACC_Generator(
    RIO_HANDLE_T handle,  
    unsigned int pnacc_Factor)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /*check parameters*/
    if ((pnacc_Factor + instanse->rx_Data.pnacc_Generator.factor) > RIO_ALL_PACKETS)
        return RIO_PARAM_INVALID;


#ifndef _CARBON_SRIO_XTOR
    printf("\nBFM PNACC RND Func()  prob:%u \n",pnacc_Factor);  /*GDA Bug#130*/
#endif

    /*set pnacc generator*/
    instanse->rx_Data.pnacc_Generator.factor = pnacc_Factor; /* probability in percents */
    instanse->rx_Data.pnacc_Generator.pnacc_Cnt = 0; /* reset pnacc counter */

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Set_Stomp_Generator
 *
 * Description: Set packet stomp generator
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_Stomp_Generator(
    RIO_HANDLE_T handle,  
    unsigned int stomp_Factor)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /*check parameters*/
    if (stomp_Factor > RIO_ALL_PACKETS)
        return RIO_PARAM_INVALID;

    /*set stomp generator*/
    instanse->tx_Data.stomp_Generator.factor = stomp_Factor; /* probability in percents */
    instanse->tx_Data.stomp_Generator.stomp_Cnt = 0; /* reset stomp counter */

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Set_LRQR_Generator
 *
 * Description: Set LRQ-Reset generator
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_LRQR_Generator(
    RIO_HANDLE_T handle,  
    unsigned int lrqr_Factor)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /*check parameters*/
    if (lrqr_Factor > RIO_ALL_PACKETS)
        return RIO_PARAM_INVALID;
    if (lrqr_Factor > 10)
    {
        lrqr_Factor = 10;
        RIO_PL_Msg(
                instanse,
                RIO_PL_MSG_WARNING_MSG,
                "Warning: It is not allowed to set probability"
                "  for LRQ-Reset Generator greater than 10 percents. \n"
            );
    }
     

    /*set LRQ-Reset generator*/
    instanse->tx_Data.lrqr_Generator.factor = lrqr_Factor; /* probability in percents */
    instanse->tx_Data.lrqr_Generator.need_RESET = RIO_FALSE;
    instanse->tx_Data.lrqr_Generator.lrqr_Cnt = 0;

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Enable_Rnd_Idle_Generator
 *
 * Description: Enable or disable idle random generator
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int pl_Enable_Rnd_Idle_Generator(
    RIO_HANDLE_T handle,  
    unsigned int low_Boundary,
    unsigned int high_Boundary,
    RIO_BOOL_T enable)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    /*check parameters*/
    if (IS_NOT_A_BOOL(enable))
        return RIO_PARAM_INVALID;

    if (low_Boundary > high_Boundary)
        return RIO_PARAM_INVALID;

    /*enable idle generator*/
    instanse->tx_Data.rnd_Idle_Generator.valid = enable;
    instanse->tx_Data.rnd_Idle_Generator.hb = high_Boundary;
    instanse->tx_Data.rnd_Idle_Generator.lb = low_Boundary;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Set_Random_Generator
 *
 * Description: Set random generator with seed value
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_Random_Generator(
    unsigned int seed_Value
    )
{
    /* Setup seed value */
	srand(seed_Value);

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Enable_RND_Pnacc_Generator_Init
 *
 * Description: Initial parameters are set
 *
 * Returns: error code
 *
 * Notes: GDA Bug#130
 *
 **************************************************************************/
int RIO_PL_Enable_RND_Pnacc_Generator_Init(RIO_HANDLE_T handle,RIO_WORD_T pnacc_Low_Boundary,
                                 RIO_WORD_T pnacc_High_Boundary,RIO_BYTE_T pnacc_Probability)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
       return RIO_ERROR;

    /*check parameters*/
    if ((pnacc_Probability) > RIO_ALL_PACKETS)
        return RIO_PARAM_INVALID;
   
    if (pnacc_Low_Boundary > pnacc_High_Boundary)
        return RIO_PARAM_INVALID;

    /*set pnacc generator*/
    instanse->pnacc_Probability   = pnacc_Probability;   /* probability in percents */
    instanse->pnacc_Low_Boundary  = pnacc_Low_Boundary;
    instanse->pnacc_High_Boundary = pnacc_High_Boundary;
   
#ifndef _CARBON_SRIO_XTOR
    /*GDA Bug#130*/
    printf("\nOn the FLY PNACC RND Func() low_bound:%u high_bound:%u prob:%u \n",
              pnacc_Low_Boundary,pnacc_High_Boundary,pnacc_Probability); 
#endif
 
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Enable_RND_Retry_Generator_Init
 *
 * Description: Initial parameters are set
 *
 * Returns: error code
 *
 * Notes: GDA Bug#130
 *
 **************************************************************************/
int RIO_PL_Enable_RND_Retry_Generator_Init(RIO_HANDLE_T handle,
					   RIO_WORD_T retry_Low_Boundary,
                                           RIO_WORD_T retry_High_Boundary,
                                           RIO_BYTE_T retry_Probability_Prio0,
					   RIO_BYTE_T retry_Probability_Prio1,
                                           RIO_BYTE_T retry_Probability_Prio2,
                                           RIO_BYTE_T retry_Probability_Prio3
                                          )
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
       return RIO_ERROR;

    /*check parameters*/
   if (retry_Probability_Prio0 > RIO_ALL_PACKETS || retry_Probability_Prio1 > RIO_ALL_PACKETS || 
       retry_Probability_Prio2 > RIO_ALL_PACKETS || retry_Probability_Prio3 > RIO_ALL_PACKETS)
        return RIO_PARAM_INVALID;

   if (retry_Low_Boundary > retry_High_Boundary)
        return RIO_PARAM_INVALID;

    /*set retry generator*/
    instanse->retry_Low_Boundary  = retry_Low_Boundary;
    instanse->retry_High_Boundary = retry_High_Boundary;

    instanse->retry_Probability_Prio0=retry_Probability_Prio0;
    instanse->retry_Probability_Prio1=retry_Probability_Prio1;
    instanse->retry_Probability_Prio2=retry_Probability_Prio2;
    instanse->retry_Probability_Prio3=retry_Probability_Prio3;

#ifndef _CARBON_SRIO_XTOR
    /*GDA Bug#130*/
    printf("\nOn the FLY RETRY RND() low_bound:%u high_bound:%u prob_prio0:%u prob_prio1:%u prob_prio2:%u  prob_prio3:%u \n",retry_Low_Boundary,retry_High_Boundary,retry_Probability_Prio0,\
    retry_Probability_Prio1,retry_Probability_Prio2,retry_Probability_Prio3); 
#endif
  
    return RIO_OK;

}
/***************************************************************************
 * Function : RIO_PL_Generate_Multicast
 *
 * Description: Generates Multicast Flag
 *
 * Returns: error code
 *
 * Notes: GDA Bug#116
 *
 **************************************************************************/
int RIO_PL_Generate_Multicast(RIO_HANDLE_T handle,
			    RIO_WORD_T   low_Boundary,
                            RIO_WORD_T   high_Boundary)
{
 /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
       return RIO_ERROR;

    if (low_Boundary > high_Boundary)
       return RIO_PARAM_INVALID;
  
#ifndef _CARBON_SRIO_XTOR
    /*GDA:  Bug#116 */
    printf("\nMulticast Control Symbol Invoked\n");
#endif

    instanse->multicast_Enable_Flag   =  RIO_TRUE;
    instanse->multicast_Low_Boundary  =  low_Boundary;
    instanse->multicast_High_Boundary =  high_Boundary;

    return RIO_OK;
}  
/**************************************************************************/

