/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/rio_pl_registers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer registers source
*
* Notes:
*
******************************************************************************/

#include <stdio.h>
#include <limits.h>

#include "rio_types.h"
#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"

#include "rio_pl_rx.h"
#include "rio_pl_tx.h"

#include "rio_pl_in_buf.h"
#include "rio_pl_out_buf.h"

#include "rio_common.h"		/* GDA  - GDA_PKT_SIZE is defined */

/*
 * init values for registers
 */
#define RIO_DEV_IDS_I              0
#define RIO_DEV_INFO_I              (unsigned long)0x00000004 /*0.4 spec complaince*/

#define RIO_ASS_IDS_I              0
#define RIO_ASS_INFO_I              (unsigned long)0x00000100

#define RIO_PE_FEAT_I              0
#define RIO_SWITCH_INFO_I          0

#define RIO_SOURCE_OPS_I           0
#define RIO_DEST_OPS_I             0

/*#define RIO_MAX_SOURCE_SIZE_I    (unsigned long)0xeec00000
#define RIO_MAX_DEST_SIZE_I        (unsigned long)0xeec00000

#define RIO_MAX_SOURCE_MES_SIZE_I  0
#define RIO_MAX_DEST_MES_SIZE_I    0
*/

#define RIO_MAILBOX_STATUS_I       0
#define RIO_WRITE_PORT_STATUS_I    0

/*
#define RIO_CONF_SOURCE_SIZE_I     (unsigned long)0xeec00000
#define RIO_CONF_DEST_SIZE_I       (unsigned long)0xeec00000

#define RIO_CONF_SOURCE_MES_SIZE_I 0
#define RIO_CONF_DEST_MES_SIZE_I   0
*/

#define RIO_PE_LL_CONTROL_I        0

#define RIO_LCSHBAR_I              0
#define RIO_LCSBAR_I               0

/*transport registers*/
#define RIO_BASE_DEV_ID_I 		0
#define RIO_HOST_BASE_DEV_ID_LOCK_I 	(unsigned long)0x0000FFFF
#define RIO_COMPONENT_TAG_I		0

#define RIO_PORT_MAINT_HEADER_0_I          (unsigned long)0x04000001
#define RIO_PORT_MAINT_HEADER_0_I_SERIAL   (unsigned long)0x04000001

#define RIO_PORT_MAINT_HEADER_1_I          (unsigned long)0x00000000

#define RIO_EXTEND_FEAT_HEADER_0_I         (unsigned long)0x00000001
#define RIO_EXTEND_FEAT_HEADER_1_I         (unsigned long)0x00000000

/* LP-EP registers */
#define RIO_LINK_VALID_TO1_I               (unsigned long)0xffffff00
#define RIO_LINK_VALID_TO2_I               (unsigned long)0xffffff00

#define RIO_LINK_GEN_CONTROL_I             0 /*implementation-defined*/

#define RIO_LINK_ERROR_I                   (unsigned long)0x00000001
#define RIO_LINK_CONTROL_I                 0 /*OR with init values*/
#define RIO_LINK_CONTROL_SERIAL_I          0x1


/* for software assistant error recovery*/
#define RIO_LINK_VALID_REQ_I               0 
#define RIO_LINK_VALID_RESP_I              0
#define RIO_LINK_ACKID_STATUS_I            0
#define RIO_LINK_BUF_STATUS_I              0

/*
#define RIO_LINK_VALID_REQ_I               0
#define RIO_LINK_VALID_RESP_I              0

#define RIO_LINK_VALID_TO1_I              
#define RIO_LINK_VALID_TO2_I             

#define RIO_LINK_ERROR_I              (unsigned long)0x00010001

*/
/*
 * masks for register
 */
#define RIO_DEV_IDS_M              ULONG_MAX
#define RIO_DEV_INFO_M             (unsigned long)0x0000ffff

#define RIO_ASS_IDS_M              ULONG_MAX
#define RIO_ASS_INFO_M             ULONG_MAX

/* #define RIO_PE_FEAT_M           (unsigned long)0xf0f8001f */

/* GDA: Bug#125 - Support for Flow control packet */
#define RIO_PE_FEAT_M              (unsigned long)0xf0f8009f

#define RIO_SWITCH_INFO_M          0

/*#define RIO_SOURCE_OPS_M         (unsigned long)0xffc0fdf4
#define RIO_DEST_OPS_M             (unsigned long)0xffc0fdf4*/

/* GDA: Bug#133 - support for ttype C of type5 pkt. Refer part1:I/O Logical Spec, Rev. 1.3 (pg-50) */
/*
#define RIO_SOURCE_OPS_M           (unsigned long)0xffc0fdfc 
#define RIO_DEST_OPS_M             (unsigned long)0xffc0fdfc*/
/* GDA: Bug#133 */ 

/* GDA: Bug#124 - Support for Data Streaming packet */
#define RIO_SOURCE_OPS_M           (unsigned long)0xffc4fdfc 
#define RIO_DEST_OPS_M             (unsigned long)0xffc4fdfc
#define RIO_PE_LL_DS_CONTROL_M     (unsigned long)0x0000007F 
#define RIO_PE_LL_DS_INFO_M        (unsigned long)0xFFFFFFFF 
/* GDA: Bug#124 - Support for Data Streaming packet */



/*
#define RIO_MAX_SOURCE_SIZE_M      (unsigned long)0xffff0000
#define RIO_MAX_DEST_SIZE_M        (unsigned long)0xffff0000

#define RIO_MAX_SOURCE_MES_SIZE_M  ULONG_MAX
#define RIO_MAX_DEST_MES_SIZE_M    ULONG_MAX
*/

#define RIO_MAILBOX_STATUS_M       ULONG_MAX
#define RIO_WRITE_PORT_STATUS_M    ULONG_MAX

/*
#define RIO_CONF_SOURCE_SIZE_M     (unsigned long)0xffff0000
#define RIO_CONF_DEST_SIZE_M       (unsigned long)0xffff0000

#define RIO_CONF_SOURCE_MES_SIZE_M  ULONG_MAX
#define RIO_CONF_DEST_MES_SIZE_M    ULONG_MAX
*/

#define RIO_PE_LL_CONTROL_M         (unsigned long)0x00000007

#define RIO_LCSHBAR_M               ULONG_MAX
#define RIO_LCSHBAR_M_50_ADDR       0x7FFF
#define RIO_LCSHBAR_M_66_ADDR       0x7FFFFFFF

#define RIO_LCSBAR_M                ULONG_MAX
#define RIO_LCSBAR_M_32_ADDR        0x7FFFFFFF


/*transport registers*/
#define RIO_BASE_DEV_ID_M            (unsigned long)0x00FFFFFF
#define RIO_HOST_BASE_DEV_ID_LOCK_M  (unsigned long)0x0000FFFF
#define RIO_COMPONENT_TAG_M          ULONG_MAX

#define RIO_PORT_MAINT_HEADER_0_M    ULONG_MAX
#define RIO_PORT_MAINT_HEADER_1_M    0/*0xffff0000*/

#define RIO_EXTEND_FEAT_HEADER_0_M   ULONG_MAX
#define RIO_EXTEND_FEAT_HEADER_1_M   0/*0xffff0000*/

/* LP-EP registers*/

/*common*/
#define RIO_LINK_VALID_TO1_M        (unsigned long)0xffffff00
#define RIO_LINK_VALID_TO2_M        (unsigned long)0xffffff00

#define RIO_LINK_GEN_CONTROL_M      (unsigned long)0xe0000000

#define RIO_LINK_ERROR_M            (unsigned long)0x001f071f
#define RIO_LINK_ERROR_READ_MASK    (unsigned long)0x000d050b

#define RIO_LINK_CONTROL_M          (unsigned long)0xeec00000
#define RIO_LINK_CONTROL_READ_MASK  (unsigned long)0x88c00000
#define RIO_LINK_CONTROL_SERIAL_M   (unsigned long)0xfff80001
#define RIO_LINK_CONTROL_SERIAL_READ_MASK  (unsigned long)0xf81fffff /*pins disabling and error checking disabling is r/o*/



/* for software assistant error recovery*/

#define RIO_LINK_VALID_REQ_M        (unsigned long)0x00000007
#define RIO_LINK_VALID_RESP_M       (unsigned long)0x800003ff
#define RIO_LINK_ACKID_STATUS_M     (unsigned long)0x1f00ff1f
#define RIO_LINK_BUF_STATUS_M       (unsigned long)0x80009f9f    

/* prototypes*/
static RIO_PL_REG_T *find_Register(RIO_PL_DS_T *handle, RIO_CONF_OFFS_T offset);

static void set_Register(
    RIO_PL_REG_T    *reg,
    unsigned long   reg_Value,
    unsigned long   reg_Mask,
    unsigned long   reg_Offset,
    RIO_REG_TYPE_T  access_Type
    );

static int read_Ext_Register(
    RIO_PL_DS_T *handle, 
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     *data);

static int write_Ext_Register(
    RIO_PL_DS_T       *handle, 
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     data);

static unsigned long convert_To_Big(unsigned long data);

/*static int RIO_PL_Check_Conf_Source_Dest_Size(
                    RIO_PL_DS_T       *handle,
                    RIO_CONF_OFFS_T   offset,   
                    unsigned long     data);

static int RIO_PL_Check_Conf_Source_Dest_Mes_Size(
                    RIO_PL_DS_T       *handle,
                    RIO_CONF_OFFS_T   offset,   
                    unsigned long     data);
*/
/* variable to hold endian mode */
static RIO_BOOL_T big_Endian = RIO_FALSE;


/***************************************************************************
 * Function : PL_Send_Link_Req
 *
 * Description: sends link req symbol
 *
 * Returns: 
 *
 * Notes:  Handle is not checked because this routine is invoked from the 
 *          routine which alerady checks one
 *
 **************************************************************************/
static void pl_Send_Link_Req(RIO_PL_DS_T * handle, unsigned int ack_ID)
{ 
    RIO_GRANULE_T link_Request;

    /*check that link request is not sending now*/
    if (handle->tx_Data.link_Request.valid == RIO_TRUE &&
        handle->tx_Data.link_Request.is_Sent == RIO_FALSE)
        return;

    /*check that model in the ok state*/
    /* here is the part of letter from Sudhi R Proch (IBM) regarding
    this manits ID (sent 11.06.2003):
    What will the model do, if it is in the middle of Output error recovery
    (has already issued a Link req. input status due to some error happaning on
    the link) and user writes to the Link maint CSR register (with command of
    input status, reset or send training)?
    In this case even if the model ignores the Link maint CSR register write,
    it is OK with us.
    */
    if (handle->tx_Data.state == RIO_PL_STOPPED_DUE_TO_ERROR)
        return;


    link_Request.granule_Struct.s = 1; 
    link_Request.granule_Struct.ack_ID = ack_ID & 0x07; 
    link_Request.granule_Struct.stype = RIO_GR_LINK_REQUEST_STYPE;
    link_Request.granule_Type = RIO_GR_LINK_REQUEST;

    /*get buf_status*/
    if (handle->parameters_Set.transmit_Flow_Control_Support == RIO_TRUE)
        link_Request.granule_Struct.buf_Status = RIO_PL_In_Buf_Status(handle);
    else 
        link_Request.granule_Struct.buf_Status = RIO_PL_ALL_BUFS_FREE;

    /*stop the packets sending if this is the input status request*/
    if (ack_ID == RIO_LP_LREQ_INPUT_STATUS)
        handle->tx_Data.state = RIO_PL_STOPPED_DUE_TO_ERROR;


    /*perform symbol sending*/
    RIO_PL_Tx_Send_Link_Req(handle, &link_Request);
}

/***************************************************************************
 * Function : read_Ext_Register
 *
 * Description: stub for corresponding callback invokation
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int read_Ext_Register(
    RIO_PL_DS_T *handle, 
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     *data)
{
    if (handle == NULL || data == NULL || 
        handle->ext_Interfaces.extend_Reg_Read == NULL) 
        return RIO_ERROR;

    /* invoke callback and return result */
    return handle->ext_Interfaces.extend_Reg_Read(
        handle->ext_Interfaces.extend_Reg_Context,
        offset,
        data);
}

/***************************************************************************
 * Function : write_Ext_Register
 *
 * Description: stub for corresponding callback invokation
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int write_Ext_Register(
    RIO_PL_DS_T       *handle, 
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     data)
{
    if (handle == NULL || handle->ext_Interfaces.extend_Reg_Write == NULL) 
        return RIO_ERROR;

    /* invoke callback and return result */
    return handle->ext_Interfaces.extend_Reg_Write(
        handle->ext_Interfaces.extend_Reg_Context,
        offset,
        data);
}


/***************************************************************************
 * Function : RIO_PL_Read_Register
 *
 * Description: read a register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Read_Register(
    RIO_PL_DS_T       *handle,
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     *data,
    RIO_BOOL_T        internal
)
{
    /* pointer to register where we need to read */
    RIO_PL_REG_T *reg;

    if (handle == NULL || data == NULL)
        return RIO_ERROR;

    /*clean data first */
    *data = 0;
    
    /* detect if it's unaligned access trying */
    if (offset & RIO_PL_REG_MISALIGN_MASK)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_9, (unsigned long)offset);
        return RIO_ERROR;
    }

    /* 
     * detect if it's extended features register which shall be 
     * maintaned by the environment - add new space, not hardly defined
     */
/*    if (offset >= 
        ((handle->reg_Set.common_Regs.port_Maint_Header_0.reg_Value >> RIO_PL_REG_HEADER_SHIFT) + RIO_PL_BYTE_CNT_IN_DWORD) &&
        offset < 
        ((handle->reg_Set.common_Regs.port_Maint_Header_0.reg_Value >> RIO_PL_REG_HEADER_SHIFT) + 
        (handle->reg_Set.common_Regs.extend_Feat_Header_1.reg_Value >> RIO_PL_REG_HEADER_SHIFT)))
        return read_Ext_Register(handle, offset, data);
*/
    if (((offset >= RIO_EF_SPACE_BOTTOM) &&
        (offset < (handle->inst_Param_Set.entry_Extended_Features_Ptr - RIO_PL_BYTE_CNT_IN_WORD))) ||
        ((offset >= (handle->inst_Param_Set.entry_Extended_Features_Ptr + handle->reg_Set.lpep_Size)) &&
        (offset < (RIO_ID_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD))))
        return read_Ext_Register(handle, offset, data);

    /* detect other external maintened registers */
    if (offset == RIO_MAILBOX_STATUS_A ||
        offset == RIO_WRITE_PORT_STATUS_A)
        return read_Ext_Register(handle, offset, data);
    
    
    if (offset >= (RIO_ID_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD))
    {
        /*RIO_PL_Print_Message(handle, RIO_PL_ERROR_11, (unsigned long)offset);*/
        return RIO_ERROR;
    }
    /* find register to read*/
    if ((reg = find_Register(handle, offset)) == NULL)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_13, (unsigned long)offset);
        return RIO_OK;
    }

    /*forbidd access to internal regs*/
    if (reg->access_Type == RIO_REG_INTERNAL_ONLY &&
        internal == RIO_FALSE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_13, (unsigned long)offset);
        return RIO_OK;
    }
    /* check if we can read to it*/
    if (reg->access_Type == RIO_REG_WRITE_ONLY &&
        internal == RIO_FALSE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_14, (unsigned long)offset);
        return RIO_ERROR;
    }

    /* masked data reserved bits */
    if (internal == RIO_FALSE)
        *data = convert_To_Big(reg->reg_Value & reg->reg_Mask);
    else
        *data = reg->reg_Value & reg->reg_Mask;

    /* check if reading shall perform some actions*/
    if (internal == RIO_TRUE)
    {
    }
    else
    {
        switch (offset - handle->inst_Param_Set.entry_Extended_Features_Ptr)
        {
            /* 
             *link validation response we shall clean response_valid bit
             */
           case (RIO_LINK_VALID_RESP_A): 
                reg->reg_Value <<= 1;
                reg->reg_Value >>= 1;
                break;

            default:
                ;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Write_Register
 *
 * Description: write a register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Write_Register(
    RIO_PL_DS_T       *handle,
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     data,
    RIO_BOOL_T        internal
)
{
    /* pointer to register where we need to write */
    RIO_PL_REG_T *reg;
    /*int ret_Val; *//*temporary*/
    
    if (handle == NULL)
        return RIO_ERROR;

    /* detect if it's unaligned access trying */
    if (offset & RIO_PL_REG_MISALIGN_MASK)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_10, (unsigned long)offset);
        return RIO_ERROR;
    }

    /* 
     * detect if it's extended features register which shall be 
     * maintaned by th environment 
     */
/*    printf("maint_head >> + : %x\n", (handle->reg_Set.common_Regs.port_Maint_Header_0.reg_Value >>
        RIO_PL_REG_HEADER_SHIFT) + RIO_PL_BYTE_CNT_IN_DWORD);*/

    /*    if (offset >= 
        ((handle->reg_Set.common_Regs.port_Maint_Header_0.reg_Value >> RIO_PL_REG_HEADER_SHIFT) + RIO_PL_BYTE_CNT_IN_DWORD) &&
        offset < 
        ((handle->reg_Set.common_Regs.port_Maint_Header_0.reg_Value >> RIO_PL_REG_HEADER_SHIFT) + 
        (handle->reg_Set.common_Regs.extend_Feat_Header_1.reg_Value >> RIO_PL_REG_HEADER_SHIFT)))
        return write_Ext_Register(handle, offset, data);
    */

    if (((offset >= RIO_EF_SPACE_BOTTOM) && (offset <
        (handle->inst_Param_Set.entry_Extended_Features_Ptr - RIO_PL_BYTE_CNT_IN_WORD))) ||
        ((offset >= (handle->inst_Param_Set.entry_Extended_Features_Ptr + handle->reg_Set.lpep_Size)) &&
        (offset < (RIO_ID_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD))))
        return write_Ext_Register(handle, offset, data);

    /* detect other external maintened registers */
    if (offset == RIO_MAILBOX_STATUS_A ||
        offset == RIO_WRITE_PORT_STATUS_A)
        return write_Ext_Register(handle, offset, data);


    if (offset >= (RIO_ID_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD))
    {
        /*RIO_PL_Print_Message(handle, RIO_PL_ERROR_11, (unsigned long)offset);*/
        return RIO_ERROR;
    }

    /* find register to write*/
    if ((reg = find_Register(handle, offset)) == NULL)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_11, (unsigned long)offset);
        return RIO_OK;
    }

    /*forbidd access to internal regs*/
    if (reg->access_Type == RIO_REG_INTERNAL_ONLY &&
        internal == RIO_FALSE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_11, (unsigned long)offset);
        return RIO_OK;
    }
    /* check if we can write to it*/
    if (reg->access_Type == RIO_REG_READ_ONLY &&
        internal == RIO_FALSE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_12, (unsigned long)offset);
        return RIO_OK;
    }

    /* masked data to prevent changing reserved bits */
    if (internal == RIO_FALSE)
        data = convert_To_Big(data);

/*    if ((offset == RIO_CONF_SOURCE_SIZE_A) || (offset == RIO_CONF_DEST_SIZE_A))
        if ((ret_Val = RIO_PL_Check_Conf_Source_Dest_Size(
            handle,(unsigned long) offset,data)) == RIO_ERROR)
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_47, (unsigned long)offset);
            return RIO_ERROR;
        }
*/
/*    if ((offset == RIO_CONF_SOURCE_MES_SIZE_A) || (offset == RIO_CONF_DEST_MES_SIZE_A))
        if ((ret_Val = RIO_PL_Check_Conf_Source_Dest_Mes_Size(
            handle,(unsigned long) offset,data)) == RIO_ERROR)
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_47, (unsigned long)offset);
            return RIO_ERROR;
        }
*/    

/*    reg->reg_Value = data & reg->reg_Mask;*/
    /*perform writing to special accessed registers*/
    if (internal == RIO_TRUE)
    {
        reg->reg_Value = data & reg->reg_Mask;            
    }
    else
    {
        switch (offset)
        {
            case (RIO_HOST_BASE_DEV_ID_LOCK_A):
                if (reg->access_Type == RIO_REG_READ_RESET)
                {
                    if ((data & reg->reg_Mask) == (reg->reg_Value & reg->reg_Mask))
                    {
                        reg->reg_Value = reg->reg_Mask & 0xffff; 
                        reg->access_Type = RIO_REG_READ_WRITE;
                    }
                }
                else if (reg->access_Type == RIO_REG_READ_WRITE)
                {
                    reg->reg_Value = data & reg->reg_Mask;
                    reg->access_Type = RIO_REG_READ_RESET;
                }
            break;

            case (RIO_PE_LL_CONTROL_A):
                if(((handle->reg_Set.common_Regs.pe_Features.reg_Value & reg->reg_Mask) & 
                    (data & reg->reg_Mask)) != 0)
                {
                    switch(data & reg->reg_Mask)
                    {
                        /*34*/
                        case 1:
                            reg->reg_Value = data & reg->reg_Mask;
                            handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid = RIO_FALSE;
                            handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 = RIO_FALSE;
                            break;

                        /*50*/
                        case 2:
                            reg->reg_Value = data & reg->reg_Mask;
                            handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid = RIO_TRUE;
                            handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 = RIO_TRUE;
                        break;

                        /*66*/
                        case 4:
                            reg->reg_Value = data & reg->reg_Mask;
                            handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid = RIO_TRUE;
                            handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 = RIO_FALSE;
                            break;

                        default:
                            RIO_PL_Print_Message(handle, RIO_PL_ERROR_47, (unsigned long)offset);
                    }
                }
                else 
                    RIO_PL_Print_Message(handle, RIO_PL_ERROR_47, (unsigned long)offset);
            break;

            default:
                switch (offset - handle->inst_Param_Set.entry_Extended_Features_Ptr)
                {
                    case (RIO_LINK_CONTROL_A):
                        if (handle->is_Parallel_Model == RIO_TRUE)
                        {
                            reg->reg_Value =(reg->reg_Value & RIO_LINK_CONTROL_READ_MASK) | 
                            (data & ~(RIO_LINK_CONTROL_READ_MASK));
                        }
                        else
                        {
                            reg->reg_Value =(reg->reg_Value & RIO_LINK_CONTROL_SERIAL_READ_MASK) | 
                            (data & ~(RIO_LINK_CONTROL_SERIAL_READ_MASK));

                        }
                    break;

                    case (RIO_LINK_ERROR_A):
                        reg->reg_Value = reg->reg_Value & ((~(data) & reg->reg_Mask) | RIO_LINK_ERROR_READ_MASK);
                    break;

                    default:
                        reg->reg_Value = data & reg->reg_Mask;
                }        
        }
    }

    /* check if writing shall perform some actions*/
    if (internal == RIO_TRUE)
    {
    }
    else
    {
        /* LINK_REQUEST which shall be headed out */
 /*       RIO_GRANULE_T link_Request;*/

        switch (offset - handle->inst_Param_Set.entry_Extended_Features_Ptr)
        {
            /* 
             *link validation request shall be passed to the transmitter
             */
            case RIO_LINK_VALID_REQ_A:
                if ( (reg->reg_Value & 0x07) == RIO_PL_LREQ_SEND_TRAINING && handle->is_Parallel_Model == RIO_TRUE)
                {
                    pl_Send_Link_Req(handle, reg->reg_Value);
                }
                else if ( (reg->reg_Value & 0x07) == RIO_LP_LREQ_INPUT_STATUS || 
                            (reg->reg_Value & 0x07) == RIO_PL_LREQ_RESET)
                {
                    pl_Send_Link_Req(handle, reg->reg_Value);
                }
                else
                {
                    RIO_PL_Print_Message(handle, RIO_PL_ERROR_21, (unsigned long)(reg->reg_Value & 0x07));
                }
                break;


                
 

                /* check if cmd field is correct */
/*                if (link_Request.granule_Struct.ack_ID == RIO_PL_LREQ_RESET ||
                    link_Request.granule_Struct.ack_ID == RIO_LP_LREQ_INPUT_STATUS ||
                    link_Request.granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING )
                {
                    RIO_PL_Tx_Send_Link_Req(handle, &link_Request);
                }
                else
                {
                    RIO_PL_Print_Message(handle, RIO_PL_ERROR_21, (unsigned long)link_Request.granule_Struct.ack_ID);
                }
                break;
*/
            case (RIO_LINK_VALID_TO1_A):
                handle->tx_Data.tx_Param_Set.lresp_Time_Out = reg->reg_Value >> 8;
                break;

            case (RIO_LINK_VALID_TO2_A):
                handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out = reg->reg_Value >> 8;
                break;
            

            case (RIO_LINK_GEN_CONTROL_A):
                /*check Master Enable bit*/
                handle->outbound_Buf.out_Buf_Param_Set.only_Responses = 
                    ((reg->reg_Value & (unsigned long)0x40000000) == 0 ? RIO_TRUE : RIO_FALSE);
                break;

            case (RIO_LINK_CONTROL_A):
                /* output/input ports enable bits */
                if (handle->is_Parallel_Model == RIO_TRUE)
                {
                    handle->outbound_Buf.out_Buf_Param_Set.only_Maint_Responses = 
                        ((reg->reg_Value & (unsigned long)0x40000000) == 0 ? RIO_TRUE : RIO_FALSE);

                    handle->inbound_Buf.in_Buf_Param_Set.only_Maint_Requests = 
                        ((reg->reg_Value & (unsigned long)0x04000000) == 0 ? RIO_TRUE : RIO_FALSE);

                    if (handle->tx_Data.pins_Disabled !=
                        ((reg->reg_Value & (unsigned long)0x20000000) == 0 ? RIO_FALSE : RIO_TRUE))
                    {
                        handle->tx_Data.pins_Disabled =
                            (reg->reg_Value & (unsigned long)0x20000000) == 0 ? RIO_FALSE : RIO_TRUE;
                        if (handle->internal_Ftray.rio_Link_Tx_Disabling != NULL)
                            handle->internal_Ftray.rio_Link_Tx_Disabling(handle->link_Instanse,
                            handle->tx_Data.pins_Disabled);
                    }

                    if (handle->rx_Data.pins_Disabled !=
                        ((reg->reg_Value & (unsigned long)0x02000000) == 0 ? RIO_FALSE : RIO_TRUE))
                    {
                        handle->rx_Data.pins_Disabled =
                            (reg->reg_Value & (unsigned long)0x02000000) == 0 ? RIO_FALSE : RIO_TRUE;
                        if (handle->internal_Ftray.rio_Link_Rx_Disabling != NULL)
                            handle->internal_Ftray.rio_Link_Rx_Disabling(handle->link_Instanse,
                            handle->rx_Data.pins_Disabled);
                    }
                }
                else
                {
                    handle->outbound_Buf.out_Buf_Param_Set.only_Maint_Responses = 
                        ((reg->reg_Value & (unsigned long)0x00400000) == 0 ? RIO_TRUE : RIO_FALSE);

                    handle->inbound_Buf.in_Buf_Param_Set.only_Maint_Requests = 
                        ((reg->reg_Value & (unsigned long)0x00200000) == 0 ? RIO_TRUE : RIO_FALSE);

                }
                break;

            default:
                ;

        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : find_Register
 *
 * Description: return address of register
 *
 * Returns: pointer or NULL
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_PL_REG_T *find_Register(RIO_PL_DS_T *handle, RIO_CONF_OFFS_T offset)
{
    /* loop counter*/
    unsigned long i;

    if (handle == NULL)
        return NULL;

    /* look through common register at first */
    switch(offset)
    {
        case RIO_DEV_IDS_A:
            return &handle->reg_Set.common_Regs.device_Ids;

        case RIO_DEV_INFO_A:
            return &handle->reg_Set.common_Regs.device_Info;

        case RIO_ASS_IDS_A:
            return &handle->reg_Set.common_Regs.assembly_Ids;

        case RIO_ASS_INFO_A:
            return &handle->reg_Set.common_Regs.assembly_Info;

        case RIO_PE_FEAT_A:
            return &handle->reg_Set.common_Regs.pe_Features;

        case RIO_SWITCH_INFO_A:
            return &handle->reg_Set.common_Regs.switch_Info;

        case RIO_SOURCE_OPS_A:
            return &handle->reg_Set.common_Regs.source_Ops;

        case RIO_DEST_OPS_A:
            return &handle->reg_Set.common_Regs.dest_Ops;

        case RIO_PE_LL_CONTROL_A:
            return &handle->reg_Set.common_Regs.pe_Ll_Control;

	/* GDA: Bug#124 - Support for Data Streaming packet */
        case RIO_PE_LL_DS_CONTROL_A:
            return &handle->reg_Set.common_Regs.pe_Ll_DS_Control;  
	    
        case RIO_PE_LL_DS_INFO_A:
            return &handle->reg_Set.common_Regs.pe_Ll_DS_Info;  
	/* GDA: Bug#124 - Support for Data Streaming packet */
	    

/*      case RIO_MAX_SOURCE_SIZE_A:
            return &handle->reg_Set.common_Regs.max_Source_Size;

        case RIO_MAX_DEST_SIZE_A:
            return &handle->reg_Set.common_Regs.max_Dest_Size;

        case RIO_MAX_SOURCE_MES_SIZE_A:
            return &handle->reg_Set.common_Regs.max_Source_Mes_Size;

        case RIO_MAX_DEST_MES_SIZE_A:
            return &handle->reg_Set.common_Regs.max_Dest_Mes_Size;
*/
        case RIO_MAILBOX_STATUS_A:
            return &handle->reg_Set.common_Regs.mailbox_Status;

        case RIO_WRITE_PORT_STATUS_A:
            return &handle->reg_Set.common_Regs.write_Port_Status;

/*      case RIO_CONF_SOURCE_SIZE_A:
            return &handle->reg_Set.common_Regs.conf_Source_Size;

        case RIO_CONF_DEST_SIZE_A:
            return &handle->reg_Set.common_Regs.conf_Dest_Size;

        case RIO_CONF_SOURCE_MES_SIZE_A:
            return &handle->reg_Set.common_Regs.conf_Source_Mes_Size;
*/
        case RIO_LCSHBAR_A:
            return &handle->reg_Set.common_Regs.lcshbar;

        case RIO_LCSBAR_A:
            return &handle->reg_Set.common_Regs.lcsbar;

/*        case RIO_CONF_DEST_MES_SIZE_A:
            return &handle->reg_Set.common_Regs.conf_Dest_Mes_Size;
        
*/
        case RIO_BASE_DEV_ID_A:
            return &handle->reg_Set.common_Regs.base_Dev_Id;
            
        case RIO_HOST_BASE_DEV_ID_LOCK_A:
            return &handle->reg_Set.common_Regs.host_Base_Dev_Id_Lock;

        case RIO_COMPONENT_TAG_A:
            return &handle->reg_Set.common_Regs.component_Tag;

        default: 
            switch (offset - handle->inst_Param_Set.entry_Extended_Features_Ptr)
            {
                case (RIO_PORT_MAINT_HEADER_0_A):
                    return &handle->reg_Set.common_Regs.port_Maint_Header_0;

                case (RIO_PORT_MAINT_HEADER_1_A):
                    return &handle->reg_Set.common_Regs.port_Maint_Header_1;

                case (RIO_EXTEND_FEAT_HEADER_0_A):
                    return &handle->reg_Set.common_Regs.extend_Feat_Header_0;

                case (RIO_EXTEND_FEAT_HEADER_1_A):
                    return &handle->reg_Set.common_Regs.extend_Feat_Header_1;

                case (RIO_LINK_VALID_TO1_A):
                    return &handle->reg_Set.common_Regs.valid_Time_Out_1;
       
                case (RIO_LINK_VALID_TO2_A):
                    return &handle->reg_Set.common_Regs.valid_Time_Out_2;

                case (RIO_LINK_GEN_CONTROL_A):
                    return &handle->reg_Set.common_Regs.port_General_Control;
            }
    }

    /* now look through a set of lpep registers*/
    for (i = 0; i < handle->reg_Set.lpep_Count; i++)
    {
        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_REQ_A)
        {
            return &handle->reg_Set.lpep_Regs[i].link_Valid_Req;
        }

        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_RESP_A)
        {
            return &handle->reg_Set.lpep_Regs[i].link_Valid_Resp;
        }

        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_ACKID_STATUS_A)
        {
            return &handle->reg_Set.lpep_Regs[i].link_AckID_Status;
        }

/*        if (offset == RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_TO1_A)
        {
            return &handle->reg_Set.lpep_Regs[i].valid_Time_Out_1;
        }

        if (offset == RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_TO2_A)
        {
            return &handle->reg_Set.lpep_Regs[i].valid_Time_Out_2;
        }
*/
  
        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_ERROR_A)
        {
            return &handle->reg_Set.lpep_Regs[i].port_Error_Status;
        }

        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_CONTROL_A)
        {
            return &handle->reg_Set.lpep_Regs[i].port_Control;
        }

        /*this is necessary for buf status defining*/
        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_BUF_STATUS_A)
        {
            return &handle->reg_Set.lpep_Regs[i].port_Buf_Status;
        }
    }

    return NULL;
}

/***************************************************************************
 * Function : RIO_PL_Init_Regs
 *
 * Description: intialize all registers
 *
 * Returns: pointer or NULL
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Init_Regs(RIO_PL_DS_T *handle)

{
    /* the registers intitialization performs in according with the specification for the 
     * corresponding registers, which defines registers fields and their encoding
     * all used constans SHALL by changed only HERE if it's necessary becasue they are closed 
     * defined by the spec.
     */

    /* loop counter*/
    unsigned int i;
    unsigned long temp; /* temporary */
    unsigned long lcsb_0_Mask, lcsb_1_Mask; /*temporary for lscsbar mask storing*/

    /* detect the endian mode */
    int x = 1;
    if(*(char *)&x == 1)
        big_Endian = RIO_FALSE;
    else    
        big_Endian = RIO_TRUE;

    if (handle == NULL)
        return RIO_ERROR;

    set_Register(&handle->reg_Set.common_Regs.device_Ids,
        ((handle->inst_Param_Set.device_Identity << 16) | 
        (handle->inst_Param_Set.device_Vendor_Identity & 0xffff)) & RIO_DEV_IDS_M,
        RIO_DEV_IDS_M,
        RIO_DEV_IDS_A,
        RIO_REG_READ_ONLY
        );
    
    set_Register(&handle->reg_Set.common_Regs.device_Info,
        ((((unsigned long)handle->inst_Param_Set.device_Rev) << 8) |
        (handle->inst_Param_Set.device_Major_Rev << 4) |
        ((handle->inst_Param_Set.device_Minor_Rev & 0x0f) & RIO_DEV_INFO_M)),
        RIO_DEV_INFO_M,
        RIO_DEV_INFO_A,
        RIO_REG_READ_ONLY
        );

    set_Register(&handle->reg_Set.common_Regs.assembly_Ids,
        ((handle->inst_Param_Set.assy_Identity << 16) |
        (handle->inst_Param_Set.assy_Vendor_Identity & 0x0000ffff)) & RIO_ASS_IDS_M,
        RIO_ASS_IDS_M,
        RIO_ASS_IDS_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.assembly_Info,
        ((handle->inst_Param_Set.assy_Rev << 16 ) & 0xFFFF0000) |
        (handle->inst_Param_Set.entry_Extended_Features_Ptr & 0x0000ffff),/*0x00000100,*/
        RIO_ASS_INFO_M,
        RIO_ASS_INFO_A,
        RIO_REG_READ_ONLY
        );

    /* make PE features value */
    temp = 0x08;
    temp |= (handle->inst_Param_Set.is_Bridge == RIO_TRUE ? 1 : 0) << 31;
    temp |= (handle->inst_Param_Set.has_Memory == RIO_TRUE ? 0x01 : 0x00) << 30;
    temp |= (handle->inst_Param_Set.has_Processor == RIO_TRUE ? 0x01 : 0x00) << 29;
    temp |= (handle->parameters_Set.transp_Type == RIO_PL_TRANSP_32) ? 0x10 : 0x00;
    temp |= handle->inst_Param_Set.ext_Address_Support & 0x07;
    temp |= (handle->inst_Param_Set.mbox1 == RIO_TRUE ? 0x01 : 0x00) << 23;
    temp |= (handle->inst_Param_Set.mbox2 == RIO_TRUE ? 0x01 : 0x00) << 22;
    temp |= (handle->inst_Param_Set.mbox3 == RIO_TRUE ? 0x01 : 0x00) << 21;
    temp |= (handle->inst_Param_Set.mbox4 == RIO_TRUE ? 0x01 : 0x00) << 20;
    temp |= (handle->inst_Param_Set.has_Doorbell == RIO_TRUE ? 0x01 : 0x00) << 19;

    /* GDA: Bug#125 - Support for Flow control packet */
    temp |= (handle->inst_Param_Set.has_Fcp == RIO_TRUE ? 0x01 : 0x00) << 7;

    set_Register(&handle->reg_Set.common_Regs.pe_Features,
        temp,
        RIO_PE_FEAT_M,
        RIO_PE_FEAT_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.switch_Info,
        RIO_SWITCH_INFO_I,
        RIO_SWITCH_INFO_M,
        RIO_SWITCH_INFO_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.source_Ops,
        handle->inst_Param_Set.source_Trx & RIO_SOURCE_OPS_M,
        RIO_SOURCE_OPS_M,
        RIO_SOURCE_OPS_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.dest_Ops,
        handle->inst_Param_Set.dest_Trx & RIO_DEST_OPS_M,
        RIO_DEST_OPS_M,
        RIO_DEST_OPS_A,
        RIO_REG_READ_ONLY
        );
    
    handle->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid = handle->parameters_Set.ext_Address;
    handle->outbound_Buf.out_Buf_Param_Set.ext_Address_16 = handle->parameters_Set.ext_Address_16;
    set_Register(&handle->reg_Set.common_Regs.pe_Ll_Control,
        (handle->parameters_Set.ext_Address == RIO_FALSE ? (unsigned long)0x00000001 : 
        (handle->parameters_Set.ext_Address_16 == RIO_FALSE ? (unsigned long)0x00000004 : 
        (unsigned long)0x00000002)),
        RIO_PE_LL_CONTROL_M,
        RIO_PE_LL_CONTROL_A,
        RIO_REG_READ_WRITE
        );
    

    /* GDA: Bug#124 - Support for Data Streaming packet */
    set_Register(&handle->reg_Set.common_Regs.pe_Ll_DS_Control,
        (unsigned long)0x0000007F,
        RIO_PE_LL_DS_CONTROL_M,
        RIO_PE_LL_DS_CONTROL_A,
        RIO_REG_READ_WRITE
        );
    
    set_Register(&handle->reg_Set.common_Regs.pe_Ll_DS_Info,
        (unsigned long)0xFFFFFFFF,
        RIO_PE_LL_DS_INFO_M,
        RIO_PE_LL_DS_INFO_A,
        RIO_REG_READ_ONLY
        );
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    set_Register(&handle->reg_Set.common_Regs.assembly_Ids,
        0x00040000,/*handle->inst_Param_Set.dest_Trx & RIO_DEST_OPS_M,*/	/*serial*/
        RIO_ASS_IDS_M,
        RIO_ASS_IDS_A,
        RIO_REG_READ_WRITE
        );
    /* GDA: Bug#125 */

    /* set maximum size 256 by default*/ 

    /* temp = handle->inst_Param_Set.max_Source_Op_Sizes;*/
    /* temp = 0xffffffff;
   
    temp &= 0xfff0ffff;
    temp |= ((handle->inst_Param_Set.coh_Granule_Size_32 == RIO_TRUE ? 0x0b : 0x0c) << 16); 
    
    set_Register(&handle->reg_Set.common_Regs.max_Source_Size,
        temp,
        RIO_MAX_SOURCE_SIZE_M,
        RIO_MAX_SOURCE_SIZE_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.conf_Source_Size,
        temp,
        RIO_CONF_SOURCE_SIZE_M,
        RIO_CONF_SOURCE_SIZE_A,
        RIO_REG_READ_WRITE
        );
*/
    /*set maximum size 256 by default*/ 
    /*temp = handle->inst_Param_Set.max_Dest_Op_Sizes;*/

/*
    temp = 0xffffffff;
    temp &= 0xfff0ffff;
    temp |= ((handle->inst_Param_Set.coh_Granule_Size_32 == RIO_TRUE ? 0x0b : 0x0c) << 16); 

    set_Register(&handle->reg_Set.common_Regs.max_Dest_Size,
        temp,
        RIO_MAX_DEST_SIZE_M,
        RIO_MAX_DEST_SIZE_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.max_Source_Mes_Size,
        handle->inst_Param_Set.max_Source_MP_Op_Sizes & RIO_MAX_SOURCE_MES_SIZE_M,
        RIO_MAX_SOURCE_MES_SIZE_M,
        RIO_MAX_SOURCE_MES_SIZE_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.max_Dest_Mes_Size,
        handle->inst_Param_Set.max_Dest_MP_Op_Sizes & RIO_MAX_DEST_MES_SIZE_M,
        RIO_MAX_DEST_MES_SIZE_M,
        RIO_MAX_DEST_MES_SIZE_A,
        RIO_REG_READ_ONLY
        );
    */
    set_Register(&handle->reg_Set.common_Regs.mailbox_Status,
        RIO_MAILBOX_STATUS_I,
        RIO_MAILBOX_STATUS_M,
        RIO_MAILBOX_STATUS_A,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.write_Port_Status,
        RIO_WRITE_PORT_STATUS_I,
        RIO_WRITE_PORT_STATUS_M,
        RIO_WRITE_PORT_STATUS_A,
        RIO_REG_READ_ONLY
        );
    /*
    set_Register(&handle->reg_Set.common_Regs.conf_Dest_Size,
        handle->inst_Param_Set.max_Dest_Op_Sizes & RIO_CONF_DEST_SIZE_M,
        RIO_CONF_DEST_SIZE_M,
        RIO_CONF_DEST_SIZE_A,
        RIO_REG_READ_WRITE
        );
    set_Register(&handle->reg_Set.common_Regs.conf_Source_Mes_Size,
        handle->inst_Param_Set.max_Source_MP_Op_Sizes & RIO_CONF_SOURCE_MES_SIZE_M,
        RIO_CONF_SOURCE_MES_SIZE_M,
        RIO_CONF_SOURCE_MES_SIZE_A,
        RIO_REG_READ_WRITE
        );
    set_Register(&handle->reg_Set.common_Regs.conf_Dest_Mes_Size,
        handle->inst_Param_Set.max_Dest_MP_Op_Sizes & RIO_CONF_DEST_MES_SIZE_M,
        RIO_CONF_DEST_MES_SIZE_M,
        RIO_CONF_DEST_MES_SIZE_A,
        RIO_REG_READ_WRITE
        );
    */
    if (handle->parameters_Set.ext_Address == RIO_FALSE)
    {
        lcsb_0_Mask = 0;
        lcsb_1_Mask = RIO_LCSBAR_M_32_ADDR;
    } 
    else if (handle->parameters_Set.ext_Address_16 == RIO_TRUE)
    {
        lcsb_0_Mask = RIO_LCSHBAR_M_50_ADDR;
        lcsb_1_Mask = RIO_LCSBAR_M;
    }
    else /*ext_Address_16 == RIO_FALSE*/
    {
        lcsb_0_Mask = RIO_LCSHBAR_M_66_ADDR;
        lcsb_1_Mask = RIO_LCSBAR_M;
    }

    set_Register(&handle->reg_Set.common_Regs.lcshbar,
        handle->parameters_Set.lcshbar,
        lcsb_0_Mask,
        RIO_LCSHBAR_A,
        RIO_REG_READ_WRITE
        );

    set_Register(&handle->reg_Set.common_Regs.lcsbar,
        handle->parameters_Set.lcsbar,
        lcsb_1_Mask,
        RIO_LCSBAR_A,
        RIO_REG_READ_WRITE
        );

    /*new for transport registers*/
    temp = handle->parameters_Set.dev_ID;
    /*temp = (handle->parameters_Set.transp_Type == RIO_PL_TRANSP_32) ? (temp & 0xFFFF) : (temp & 0xFF) << 16;*/
    temp = (handle->parameters_Set.transp_Type == RIO_PL_TRANSP_32) ?
        ((temp & 0xFFFF) | ((temp & 0xFF) << 16)) : (temp & 0xFF) << 16;


    set_Register(&handle->reg_Set.common_Regs.base_Dev_Id,
        temp,
        RIO_BASE_DEV_ID_M,
        RIO_BASE_DEV_ID_A,
        RIO_REG_READ_WRITE
        );

    set_Register(&handle->reg_Set.common_Regs.host_Base_Dev_Id_Lock,
        RIO_HOST_BASE_DEV_ID_LOCK_I,
        RIO_HOST_BASE_DEV_ID_LOCK_M,
        RIO_HOST_BASE_DEV_ID_LOCK_A,
        RIO_REG_READ_WRITE
        );

    set_Register(&handle->reg_Set.common_Regs.component_Tag,
        RIO_COMPONENT_TAG_I,
        RIO_COMPONENT_TAG_M,
        RIO_COMPONENT_TAG_A,
        RIO_REG_READ_WRITE
        );

   /*extended feature regs*/
    
    set_Register(&handle->reg_Set.common_Regs.port_Maint_Header_0,
        ((handle->inst_Param_Set.next_Extended_Features_Ptr << 16) & 0xffff0000) |
        ((handle->is_Parallel_Model == RIO_TRUE ? RIO_PORT_MAINT_HEADER_0_I :
        RIO_PORT_MAINT_HEADER_0_I_SERIAL) & 0x0000ffff),/*0x00000100,*/
        RIO_PORT_MAINT_HEADER_0_M,
        RIO_PORT_MAINT_HEADER_0_A + handle->inst_Param_Set.entry_Extended_Features_Ptr,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.port_Maint_Header_1,
        RIO_PORT_MAINT_HEADER_1_I,
        RIO_PORT_MAINT_HEADER_1_M,
        RIO_PORT_MAINT_HEADER_1_A + handle->inst_Param_Set.entry_Extended_Features_Ptr,
        RIO_REG_READ_ONLY
        );
/*
    set_Register(&handle->reg_Set.common_Regs.extend_Feat_Header_0,
        RIO_EXTEND_FEAT_HEADER_0_I,
        RIO_EXTEND_FEAT_HEADER_0_M,
        RIO_EXTEND_FEAT_HEADER_0_A + RIO_LPEP_REG_BASE,
        RIO_REG_READ_ONLY
        );
    set_Register(&handle->reg_Set.common_Regs.extend_Feat_Header_1,
        RIO_EXTEND_FEAT_HEADER_1_I,
        RIO_EXTEND_FEAT_HEADER_1_M,
        RIO_EXTEND_FEAT_HEADER_1_A + RIO_LPEP_REG_BASE,
        RIO_REG_READ_ONLY
        );
*/
    /* GDA: Support for Bug#131
    handle->tx_Data.tx_Param_Set.lresp_Time_Out = RIO_LINK_VALID_TO1_I >> 8; */
    handle->tx_Data.tx_Param_Set.lresp_Time_Out = handle->inst_Param_Set.RIO_Link_Valid_To1_I >> 8;
    set_Register(&handle->reg_Set.common_Regs.valid_Time_Out_1,
        handle->inst_Param_Set.RIO_Link_Valid_To1_I,	/* GDA Bug#131 - RIO_LINK_VALID_TO1_I, */
        RIO_LINK_VALID_TO1_M,
        handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_TO1_A,
        RIO_REG_READ_WRITE
        );

    handle->outbound_Buf.out_Buf_Param_Set.logical_Time_Out = RIO_LINK_VALID_TO2_I >> 8;
    set_Register(&handle->reg_Set.common_Regs.valid_Time_Out_2,
        RIO_LINK_VALID_TO2_I,
        RIO_LINK_VALID_TO2_M,
        handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_TO2_A,
        RIO_REG_READ_WRITE
        );

    handle->outbound_Buf.out_Buf_Param_Set.only_Responses = 
            (handle->parameters_Set.is_Master_Enable == RIO_TRUE ? RIO_FALSE : RIO_TRUE);
    set_Register(&handle->reg_Set.common_Regs.port_General_Control,
        (handle->parameters_Set.is_Host == RIO_TRUE ? (unsigned long)0x80000000 : 
        (unsigned long)0x00000000) | (handle->parameters_Set.is_Master_Enable == RIO_TRUE ? (unsigned long)0x40000000 : 
        (unsigned long)0x00000000) | (handle->parameters_Set.is_Discovered == RIO_TRUE ? (unsigned long)0x20000000 : 
        (unsigned long)0x00000000) | RIO_LINK_GEN_CONTROL_I,
        RIO_LINK_GEN_CONTROL_M,
        handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_GEN_CONTROL_A,
        RIO_REG_READ_WRITE
        );
    /* now look through a set of lpep registers*/
    for (i = 0; i < handle->reg_Set.lpep_Count; i++)
    {
  /*      set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Req,
            RIO_LINK_VALID_REQ_I,
            RIO_LINK_VALID_REQ_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_REQ_A,
            RIO_REG_WRITE_ONLY
            );
        set_Register(&handle->reg_Set.lpep_Regs[i].linl_Valid_Resp,
            RIO_LINK_VALID_RESP_I,
            RIO_LINK_VALID_RESP_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            RIO_REG_READ_ONLY
            );
        
        handle->tx_Data.tx_Param_Set.lresp_Time_Out = RIO_LINK_VALID_TO1_I;
        set_Register(&handle->reg_Set.lpep_Regs[i].valid_Time_Out_1,
            RIO_LINK_VALID_TO1_I,
            RIO_LINK_VALID_TO1_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_TO1_A,
            RIO_REG_READ_WRITE
            );

        handle->tx_Data.tx_Param_Set.lresp_Retry_Cnt = RIO_LINK_VALID_TO2_I;
        set_Register(&handle->reg_Set.lpep_Regs[i].valid_Time_Out_2,
            RIO_LINK_VALID_TO2_I,
            RIO_LINK_VALID_TO2_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_TO2_A,
            RIO_REG_READ_WRITE
            );
*/
        
        set_Register(&handle->reg_Set.lpep_Regs[i].port_Error_Status,
            RIO_LINK_ERROR_I,
            RIO_LINK_ERROR_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            RIO_REG_READ_WRITE
            );

        handle->outbound_Buf.out_Buf_Param_Set.only_Maint_Responses = 
            (handle->parameters_Set.output_Is_Enable == RIO_TRUE ? RIO_FALSE : RIO_TRUE);
        handle->inbound_Buf.in_Buf_Param_Set.only_Maint_Requests = 
            (handle->parameters_Set.input_Is_Enable == RIO_TRUE ? RIO_FALSE : RIO_TRUE);
        if (handle->is_Parallel_Model == RIO_FALSE)
        {
            set_Register(&handle->reg_Set.lpep_Regs[i].port_Control,
                (
                    (handle->parameters_Set.lp_Serial_Is_1x == RIO_TRUE ? (unsigned long)0x00000000 : 
                        (handle->parameters_Set.is_Force_1x_Mode == RIO_FALSE ? (unsigned long)0x50000000 :
                            (
                                handle->parameters_Set.is_Force_1x_Mode_Lane_0 == RIO_TRUE ? (unsigned long)0x42000000 :
                                    (unsigned long)0x4B000000
                            )
                        )
                    ) |
                    (handle->parameters_Set.output_Is_Enable == RIO_TRUE ? (unsigned long)0x00400000 : (unsigned long)0x00000000)|
                    (handle->parameters_Set.input_Is_Enable == RIO_TRUE ? (unsigned long)0x00200000 : (unsigned long)0x00000000)|
                    RIO_LINK_CONTROL_SERIAL_I
                ),
                RIO_LINK_CONTROL_SERIAL_M,
                handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
                RIO_REG_READ_WRITE
                );
        }
        else
        {
            set_Register(&handle->reg_Set.lpep_Regs[i].port_Control,
                (handle->parameters_Set.work_Mode_Is_8 == RIO_TRUE ? (unsigned long)0x00000000 : 
                (unsigned long)0x88000000) | (handle->parameters_Set.output_Is_Enable == RIO_TRUE ? (unsigned long)0x40000000 : 
                (unsigned long)0x00000000) | (handle->parameters_Set.input_Is_Enable == RIO_TRUE ? (unsigned long)0x04000000 : 
                (unsigned long)0x00000000) | RIO_LINK_CONTROL_I,
                RIO_LINK_CONTROL_M,
                handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
                RIO_REG_READ_WRITE
                );

        }
        set_Register(&handle->reg_Set.lpep_Regs[i].port_Buf_Status,
            (RIO_LINK_BUF_STATUS_I | ((handle->parameters_Set.transmit_Flow_Control_Support & 0x1) << 31) |
            (handle->is_Parallel_Model == RIO_TRUE ? 
            (handle->inst_Param_Set.pl_Inbound_Buffer_Size >= 0xf ? 0xe : handle->inst_Param_Set.pl_Inbound_Buffer_Size) : 
            (handle->inst_Param_Set.pl_Inbound_Buffer_Size >= 0x1f ? 0x1e : handle->inst_Param_Set.pl_Inbound_Buffer_Size))),
            RIO_LINK_BUF_STATUS_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            RIO_REG_INTERNAL_ONLY
            );

        set_Register(&handle->reg_Set.lpep_Regs[i].link_AckID_Status,
            RIO_LINK_ACKID_STATUS_I,
            RIO_LINK_ACKID_STATUS_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            RIO_REG_INTERNAL_ONLY
            );

        set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Req,
            RIO_LINK_VALID_REQ_I,
            RIO_LINK_VALID_REQ_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_REQ_A,
            RIO_REG_READ_WRITE
            );

        set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Resp,
            RIO_LINK_VALID_RESP_I,
            RIO_LINK_VALID_RESP_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            RIO_REG_READ_ONLY
            );
    }

    /*init the indicators*/
    handle->state_Indicators_Params.is_Packet_Delimiter = RIO_FALSE;
    handle->state_Indicators_Params.is_Align_Lost = RIO_FALSE;
    handle->state_Indicators_Params.is_Sync_Lost = RIO_FALSE;
    handle->state_Indicators_Params.is_Character_Invalid = RIO_FALSE;
    handle->state_Indicators_Params.copy_To_Lane2 = RIO_FALSE;
    handle->state_Indicators_Params.suppress_Hook =  RIO_FALSE;
    handle->state_Indicators_Params.initial_Run_Disp = 2; /* do not change Running Disparity */
    handle->state_Indicators_Params.decode_Any_Run_Disp = 0;
    handle->state_Indicators_Params.lane2_Just_Forced_By_Discovery = RIO_FALSE;
    handle->state_Indicators_Params.rx_Packet_Is_Ongoing = RIO_FALSE;

    return RIO_OK;
}

/***************************************************************************
 * Function : set_Register
 *
 * Description: seta register
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void set_Register(
    RIO_PL_REG_T   *reg,
    unsigned long  reg_Value,
    unsigned long  reg_Mask,
    unsigned long  reg_Offset,
    RIO_REG_TYPE_T access_Type
    )
{
    reg->access_Type = access_Type;
    reg->reg_Mask = reg_Mask;
    reg->reg_Offset = reg_Offset;
    reg->reg_Value = reg_Value;
}

/***************************************************************************
 * Function : convert_To_Big
 *
 * Description: convert little Endian lond data to Big
 *
 * Returns: big endian value
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned long convert_To_Big(unsigned long data)
{

    /* it's not necessary now */
    return data;

    /* 
     * the registers values are stored in machine endian mode, but the 
     * RIO spec demand Big_Endian mode, so if machine use litle convert data.
     * in case of other Endian mode implementation - it's the only place there 
     * mode conversion is performing. change constant HERE only
     */
    if (big_Endian == RIO_FALSE)
    {
        unsigned long result = 0;

        result = (data & (unsigned long)0x00000001) << 31;
        result += (data & (unsigned long)0x00000002) << 29;
        result += (data & (unsigned long)0x00000004) << 27;
        result += (data & (unsigned long)0x00000008) << 25;
        result += (data & (unsigned long)0x00000010) << 23;
        result += (data & (unsigned long)0x00000020) << 21;
        result += (data & (unsigned long)0x00000040) << 19;
        result += (data & (unsigned long)0x00000080) << 17;
        result += (data & (unsigned long)0x00000100) << 15;
        result += (data & (unsigned long)0x00000200) << 13;
        result += (data & (unsigned long)0x00000400) << 11;
        result += (data & (unsigned long)0x00000800) << 9;
        result += (data & (unsigned long)0x00001000) << 7;
        result += (data & (unsigned long)0x00002000) << 5;
        result += (data & (unsigned long)0x00004000) << 3;
        result += (data & (unsigned long)0x00008000) << 1;

        result += (data & (unsigned long)0x00010000) >> 1;
        result += (data & (unsigned long)0x00020000) >> 3;
        result += (data & (unsigned long)0x00040000) >> 5;
        result += (data & (unsigned long)0x00080000) >> 7;
        result += (data & (unsigned long)0x00100000) >> 9;
        result += (data & (unsigned long)0x00200000) >> 11;
        result += (data & (unsigned long)0x00400000) >> 13;
        result += (data & (unsigned long)0x00800000) >> 15;
        result += (data & (unsigned long)0x01000000) >> 17;
        result += (data & (unsigned long)0x02000000) >> 19;
        result += (data & (unsigned long)0x04000000) >> 21;
        result += (data & (unsigned long)0x08000000) >> 23;
        result += (data & (unsigned long)0x10000000) >> 25;
        result += (data & (unsigned long)0x20000000) >> 27;
        result += (data & (unsigned long)0x40000000) >> 29;
        result += (data & (unsigned long)0x80000000) >> 31;

        return result;
    }
    else 
        return data;
}

/***************************************************************************
 * Function : RIO_PL_Get_Max_Req_Size
 *
 * Description: return count of DW which can be issue in request packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_PL_Get_Max_Req_Size(RIO_PL_DS_T *handle)
{
    /*unsigned int max_Size , conf_Size*/; /* corresponding max and cong values*/

    if (handle == NULL)
        return 0;

/*  max_Size = handle->reg_Set.common_Regs.max_Source_Size.reg_Value;
    max_Size >>= 28;

    conf_Size = handle->reg_Set.common_Regs.conf_Source_Size.reg_Value;
    conf_Size >>= 28;

    max_Size = max_Size > conf_Size ? conf_Size : max_Size;
*/
    return 0xf;
}

/***************************************************************************
 * Function : RIO_PL_Conv_Size_To_Dw_Cnt
 *
 * Description: return count of DW which can be issue in request packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_PL_Conv_Size_To_Dw_Cnt(unsigned int size)
{



    /* 
     * here performs conversion from RIO size encoding to the count 
     * of dowble-words. These constants are defined in the specifactions
     * and can be changed here in case of specifications changing 
     */
    switch (size)
    {
        /* subdouble-word sizes - one DW*/
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 6:
        case 8:
        case 9:
            return 0x01;

        case 10:
            return 0x02;

        case 11:
            return 0x04;

        case 12:
            return 0x08;

        case 13:
            return 0x10;

        case 14:
            return 0x18;

        case 15:
            return GDA_PKT_SIZE;	/* GDA - 0x20 */

        /* reserved value */
        default:
            return 0x00;
    }
}
/***************************************************************************
 * Function : RIO_PL_Get_Max_Resp_Size
 *
 * Description: return count of DW which can be issue in response packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Max_Resp_Size(RIO_PL_DS_T *handle)
{

    if (handle == NULL)
        return 0;

    return 0xf;
}

/***************************************************************************
 * Function : RIO_PL_Get_Max_Port_Wr_Size
 *
 * Description: return count of DW which can be issue in response packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Max_Port_Wr_Size(RIO_PL_DS_T *handle)
{

    if (handle == NULL)
        return 0;

    return 0xf;
}

/***************************************************************************
 * Function : RIO_PL_Get_Max_Resp_Dem_Size
 *
 * Description: return count of DW which can be demand in response packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Max_Resp_Dem_Size(RIO_PL_DS_T *handle)
{

    if (handle == NULL)
        return 0;

    return 0xf;
}

/***************************************************************************
 * Function : RIO_PL_Get_Max_Mes_Size
 *
 * Description: return count of DW which can be demand in message packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Max_Mes_Size(RIO_PL_DS_T *handle, unsigned int mbox)
{

    if (handle == NULL || mbox > RIO_PL_MAX_MBOX_VALUE)
        return 0;

    return 0xf;
}

/***************************************************************************
 * Function : RIO_PL_Get_Max_Mes_Cnt
 *
 * Description: return count of segments in message packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Max_Mes_Cnt(RIO_PL_DS_T *handle, unsigned int mbox)
{

    if (handle == NULL || mbox > RIO_PL_MAX_MBOX_VALUE)
        return 0;

    return 0x10;
}

/***************************************************************************
 * Function : RIO_PL_Get_Max_Gr_Size
 *
 * Description: return source granule size
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Max_Gr_Size(RIO_PL_DS_T *handle)
{

    if (handle == NULL)
        return 0;

    return 0xf;
}

/***************************************************************************
 * Function : RIO_PL_Get_Max_Dem_Gr_Size
 *
 * Description: return destination granule size
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_Max_Dem_Gr_Size(RIO_PL_DS_T *handle)
{

    if (handle == NULL)
        return 0;

    return 0xf;
}

/***************************************************************************
 * Function : RIO_PL_Check_Conf_Source_Dest_Size
 *
 * Description: check if value to write to conf source/dest regs is correct
 *
 * Returns: 
 *
 * Notes: none
 *
 **************************************************************************/
/*#pragma check_stack(on)
static int RIO_PL_Check_Conf_Source_Dest_Size(RIO_PL_DS_T       *handle,
                                              RIO_CONF_OFFS_T   offset,   
                                              unsigned long     data)
{
    unsigned int max_Size; 

    if (handle == NULL)
        return RIO_ERROR;

    if (offset == RIO_CONF_SOURCE_SIZE_A)
        max_Size = handle->reg_Set.common_Regs.max_Source_Size.reg_Value;
    else if (offset == RIO_CONF_DEST_SIZE_A)
        max_Size = handle->reg_Set.common_Regs.max_Dest_Size.reg_Value;
  
    data=(data >> 20);
    if (((data & 0xf)  == 0x5) || ((data & 0xf) == 0x7) || ((data &0xf) == 0xf) || 
        ((data & 0xf) > ((max_Size >> 20) & 0xf)))    return RIO_ERROR;
    data >>= 4;
    if (((data & 0xf) == 0x5) || ((data & 0xf) == 0x7) || ((data & 0xf)== 0xf) ||
        ((data & 0xf) > ((max_Size >> 24) & 0xf))) return RIO_ERROR;
    data >>= 4;
    if (((data & 0xf) == 0x5) || ((data & 0xf) == 0x7) || ((data & 0xf)== 0xf) ||
        ((data & 0xf) > ((max_Size >> 28) & 0xf))) return RIO_ERROR;
    
    return RIO_OK;
}
*/
/***************************************************************************
 * Function : RIO_PL_Check_Conf_Source_Dest_Mes_Size
 *
 * Description: check if value to write to conf source/dest Mes size 
 *                regs is correct
 *
 * Returns: 
 *
 * Notes: none
 *
 **************************************************************************/
/*#pragma check_stack(on)
static int RIO_PL_Check_Conf_Source_Dest_Mes_Size(RIO_PL_DS_T       *handle,
                                                  RIO_CONF_OFFS_T   offset,   
                                                  unsigned long     data)
{
  
    unsigned int max_Size; 
    int i; 

    if (handle == NULL)
        return RIO_ERROR;

    if (offset == RIO_CONF_SOURCE_MES_SIZE_A)
        max_Size = handle->reg_Set.common_Regs.max_Source_Mes_Size.reg_Value;
    else if (offset == RIO_CONF_DEST_MES_SIZE_A)
        max_Size = handle->reg_Set.common_Regs.max_Dest_Mes_Size.reg_Value;

    
    for (i = 0; i < 8; i++)
    {
        if ((data & 0xf) > (max_Size & 0xf)) return RIO_ERROR;
        if ( i >3 )
            if (((data & 0xf)  == 0x5) || ((data & 0xf) == 0x7) || ((data &0xf) == 0xf)) 
            return RIO_ERROR;
        data >>= 4;
        max_Size >>= 4;
    }
    return RIO_OK;
}
*/
/***************************************************************************
 * Function : RIO_PL_Get_State
 *
 * Description: read a state
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Get_State(
    RIO_PL_DS_T              *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param
    )
{
    /* pointer to register where we need to read */
    /*RIO_PL_REG_T *reg;*/
    unsigned long data;

    if (handle == NULL || indicator_Value == NULL || indicator_Param == NULL)
        return RIO_ERROR;

    if (indicator_ID >= RIO_STATE_INDICATORS_NUMBER)
        return RIO_ERROR;

    
    /*clean data first */
  /*  *indicator_Value = handle->state_Indicators[indicator_ID].indicator_Value;
    *indicator_Parameter = handle->state_Indicators[indicator_ID].indicator_Parameter;
    */
    
    
    switch (indicator_ID)
    {
        case RIO_OUR_BUF_STATUS:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = data & 0x0000001f;
        
        break;
        case RIO_PARTNER_BUF_STATUS:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (data & 0x0001f00) >> 8;

        break;
        case RIO_LRSP_ACKID_STATUS:
        
        /*let lrsp register be like in serial specification*/

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_RESP_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 5) & 0x1f;
        
        break;
        case RIO_INBOUND_ACKID:
            
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 24) & 0xff; /*0-7 bit*/
    
        break;
        case RIO_OUTBOUND_ACKID:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 0) & 0xff;/*24-31 bit*/
    
        break;
        case RIO_OUTSTANDING_ACKID:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 8) & 0xff;/*16-23 bit*/
        break;

        case RIO_TX_DATA_IN_PROGRESS:

/*
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);
        
        *indicator_Value = (data & 0x0000080) >> 7;
*/
        /* TBD: let Masha check that this is correct!!! */
        *indicator_Value = handle->tx_Data.in_Progress;

        break;
                                                                                                                                    
        case RIO_RX_DATA_IN_PROGRESS:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = ((data & 0x00008000) >> 15);

        break;

        case RIO_FLOW_CONTROL_MODE:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (data & 0x80000000) >> 31;
        break;



/*      case RIO_UNINITIALIZED: 

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)(data & 0x1);
        
        break;
*/
        case RIO_INITIALIZED: /*bit 30 in 0x58 - 1, bit 31 - 0*/
            RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

    
        *indicator_Value = (RIO_BOOL_T)((data & 0x2) >> 1) ;

        break;

        case RIO_UNREC_ERROR:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)((data & 0x4) >> 2) ;

        break;

        case RIO_OUTPUT_ERROR:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)((data >> 16) & 0x1) ;
        *indicator_Param = handle->state_Indicators_Params.output_Error_Type;
        
        break;

        case RIO_INPUT_ERROR:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)((data >> 8) & 0x1);
        *indicator_Param = handle->state_Indicators_Params.input_Error_Type;

        break;

        /****************************************************************************/

        case RIO_PORT_WIDTH:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ((data & 0x1) == 0x1) /*serial link*/
        {
            *indicator_Value =  (data >> 30) & 0x3; /*0,1 bit*/
        }
        else /*parallel link*/
        {
            *indicator_Value =  (data >> 31) & 0x1; /*0 bit- output*/
        }

        break;

        case RIO_INITIALIZED_PORT_WIDTH:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ((data & 0x1) == 0x1) /*serial link*/
        {
            *indicator_Value =  (data >> 27) & 0x7; /*2-4 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        break;

        case RIO_PORT_WIDTH_OVERRIDE:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ((data & 0x1) == 0x1) /*serial link*/
        {
            *indicator_Value =  (data >> 24) & 0x7; /*5-7 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        break;

        /* for specific internal (intermodule) needs: */
        case RIO_LANE_WITH_INVALID_CHARACTER:  /* number of the lane where invalid character occured */
            *indicator_Value = handle->state_Indicators_Params.lane_With_Invalid_Character;
        break;

        case RIO_IS_PORT_TRUE_4X: /* RIO_TRUE if port is working in 4x mode */
            *indicator_Value = handle->state_Indicators_Params.is_Port_Mode_4x;
        break;

        case RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE: /* in case of 1x mode it is a number of the current byte in granule */
            *indicator_Value = handle->state_Indicators_Params.byte_In_Rcv_Granule_Cur_Num;
        break;

        case RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE: /* in case of 1x mode it is a number of the current byte in granule */
            *indicator_Value = handle->state_Indicators_Params.byte_In_Trx_Granule_Cur_Num;
        break;

        case RIO_ARE_ALL_IDLES_IN_GRANULE:
            *indicator_Value = handle->state_Indicators_Params.are_All_Idles_In_Granule;
        break;

        case RIO_SYMBOL_IS_PD: 
            *indicator_Value = handle->state_Indicators_Params.is_Packet_Delimiter;
        break;
    
        case RIO_IS_ALIGNMENT_LOST:
            *indicator_Value = handle->state_Indicators_Params.is_Align_Lost;
        break;

        case RIO_IS_SYNC_LOST:
            *indicator_Value = handle->state_Indicators_Params.is_Sync_Lost;
        break;

        case RIO_IS_CHARACTER_INVALID:
            *indicator_Value = handle->state_Indicators_Params.is_Character_Invalid;
        break;

        case RIO_COPY_TO_LANE2:
            *indicator_Value = handle->state_Indicators_Params.copy_To_Lane2;
        break;

        case RIO_SUPPRESS_HOOK:
            *indicator_Value = handle->state_Indicators_Params.suppress_Hook;
        break;

        case RIO_INITIAL_RUN_DISP:
            *indicator_Value = handle->state_Indicators_Params.initial_Run_Disp;
        break;

        case RIO_DECODE_ANY_RUN_DISP:
            *indicator_Value = handle->state_Indicators_Params.decode_Any_Run_Disp;
        break;

        case RIO_LANE2_JUST_FORCED_BY_DISCOVERY:
            *indicator_Value = handle->state_Indicators_Params.lane2_Just_Forced_By_Discovery;
        break;

        case RIO_RX_IS_PD:
            *indicator_Value = handle->state_Indicators_Params.rx_Packet_Is_Ongoing;
            break;

        /****************************************************************************/

        default:
        break;
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Set_State
 *
 * Description: read a state
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Set_State(
    RIO_PL_DS_T              *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Param
    )
{
    /* pointer to register where we need to read */
    /*RIO_PL_REG_T *reg;*/
    unsigned long     data;
    
    if (handle == NULL)
        return RIO_ERROR;

    if (indicator_ID >= RIO_STATE_INDICATORS_NUMBER)
        return RIO_ERROR;

        
    switch (indicator_ID)
    {
        case RIO_OUR_BUF_STATUS:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        if (handle->is_Parallel_Model == RIO_TRUE && indicator_Value >= 0xf)
            data = (data & 0xffffffe0) | (0xe);
        else if (handle->is_Parallel_Model == RIO_FALSE && indicator_Value >= 0x1f)
            data = (data & 0xffffffe0) | (0x1e);
        else
            data = (data & 0xffffffe0) | (indicator_Value & 0x0000001f);

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;
        case RIO_PARTNER_BUF_STATUS:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        data = (data & 0xffffe0ff) | ((indicator_Value << 8) & 0x00001f00);

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;
        case RIO_LRSP_ACKID_STATUS:
        
        /*let lrsp register be like in serial specification*/

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_RESP_A,
            &data,
            RIO_TRUE);
    
        data &= 0xfffffc1f;
        data |= ((indicator_Value & 0x1f) << 5); /*22-26 bit*/
    
        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_RESP_A,
            data,
            RIO_TRUE);

        break;
        case RIO_INBOUND_ACKID:
            
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        data &= 0x00ffffff;
        data |= ((indicator_Value & 0xff) << 24); /*0-7 bit*/
    
        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            data,
            RIO_TRUE);
        break;
        case RIO_OUTBOUND_ACKID:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        data &= 0xffffff00;
        data |= ((indicator_Value & 0xff) << 0); /*24-31 bit*/
    
        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            data,
            RIO_TRUE);
        break;
        case RIO_OUTSTANDING_ACKID:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        data &= 0xffff00ff;
        data |= ((indicator_Value & 0xff) << 8); /*16-23 bit*/
    
        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ACKID_STATUS_A,
            data,
            RIO_TRUE);
        break;
        case RIO_TX_DATA_IN_PROGRESS:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);
        
        data = (data & 0xffffff7f) | ((indicator_Value  & 0x1) << 7);
        

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);
        
        break;
                                                                                                                                    
        case RIO_RX_DATA_IN_PROGRESS:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        data = (data & 0xffff7fff) | ((indicator_Value  & 0x1) << 15);

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;

        case RIO_FLOW_CONTROL_MODE:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        data = (data & 0x7fffffff) | ((indicator_Value  & 0x1) << 31);

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;

/*      case RIO_UNINITIALIZED: 

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        
        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffffe) | 0x1;
        }
        else 
        {
            data = (data & 0xfffffffe);
        }

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);
        

        break;
*/

        case RIO_INITIALIZED: /*bit 30, 31 in 0x58*/
            RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);
        /*If port is initialized but it sets to the uninit state
            it is necessary reset lower layer of pl*/
        if ( ((data & 2) == 2) && (indicator_Value == RIO_FALSE))
        {
            unsigned long buffer_Head, buffer_Top;
            unsigned int expected_Ackid, receiving_Ackid;

            /*store data about ackid*/
            buffer_Head = handle->tx_Data.ack_In_Buffer.buffer_Head;
			buffer_Top = handle->tx_Data.ack_In_Buffer.buffer_Top;

            expected_Ackid = handle->rx_Data.expected_Ack_Id;
            receiving_Ackid = handle->rx_Data.receiving_Ack_Id;

            RIO_PL_Rx_Init(handle);
            RIO_PL_Tx_Init(handle);
            RIO_PL_Out_Init_Arbiter(&(handle->outbound_Arb));
            RIO_PL_Out_Init_Arbiter(&(handle->inbound_Arb));
            handle->cmd_To_Wrap((RIO_HANDLE_T)handle, REINIT_ADAPTER);
            
            
            /*restore ackid data*/
            handle->rx_Data.expected_Ack_Id = expected_Ackid;
            handle->rx_Data.receiving_Ack_Id = receiving_Ackid;
        
            RIO_PL_Set_State(
                handle,
                RIO_INBOUND_ACKID, 
                handle->rx_Data.expected_Ack_Id,
                0
                );

            
            handle->tx_Data.ack_In_Buffer.buffer_Head = buffer_Head;
			handle->tx_Data.ack_In_Buffer.buffer_Top = buffer_Top;
            
        }

        /*set OK or unitialized bit*/
        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffffc) | 0x2;
        }
        else 
        {
            data = (data & 0xfffffffc) | 0x1;
        }

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        break;

        case RIO_UNREC_ERROR:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffffb) | 0x4;
        }
        else 
        {
            data = (data & 0xfffffffb);
        }

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        break;

        case RIO_OUTPUT_ERROR:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffcffff) | 0x00030000;
        }
        else 
        {
            data = (data & 0xfffeffff);
        }

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        handle->state_Indicators_Params.output_Error_Type = indicator_Param;

        break;

        case RIO_INPUT_ERROR:
        
        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffcff) | 0x00000300;
        }
        else 
        {
            data = (data & 0xfffffeff);
        }

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        handle->state_Indicators_Params.input_Error_Type = indicator_Param;

        break;

        /****************************************************************************/

        case RIO_PORT_WIDTH:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ((data & 0x1) == 0x1) /*serial link*/
        {
            data &= 0x3fffffff;
            data |= ((indicator_Value & 0x3) << 30); /*0,1 bit*/
        }
        else /*parallel link*/
        {
            data &= 0x77ffffff;
            data |= ((indicator_Value & 0x1) << 31); /*0 - bit - output*/
            data |= ((indicator_Value & 0x1) << 27); /*4 - bit - input*/

        }
        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            data,
            RIO_TRUE);

        break;

        case RIO_INITIALIZED_PORT_WIDTH:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ((data & 0x1) == 0x1) /*serial link*/
        {
            data &= 0xc7ffffff;
            data |= ((indicator_Value & 0x7) << 27); /*2-4 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            data,
            RIO_TRUE);
        break;

        case RIO_PORT_WIDTH_OVERRIDE:

        RIO_PL_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ((data & 0x1) == 0x1) /*serial link*/
        {
            data &= 0xf8ffffff;
            data |= ((indicator_Value & 0x7) << 24); /*5-7 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        RIO_PL_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            data,
            RIO_TRUE);
        break;

        /* for specific internal (intermodule) needs: */
        case RIO_LANE_WITH_INVALID_CHARACTER:  /* number of the lane where invalid character occured */
            handle->state_Indicators_Params.lane_With_Invalid_Character = indicator_Value;
        break;

        case RIO_IS_PORT_TRUE_4X: /* RIO_TRUE if port is working in 4x mode */
            handle->state_Indicators_Params.is_Port_Mode_4x = (RIO_BOOL_T)indicator_Value;
        break;

        case RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE: /* in case of 1x mode it is a number of the current byte in granule */
            handle->state_Indicators_Params.byte_In_Rcv_Granule_Cur_Num = indicator_Value;
            break;
        case RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE:
            handle->state_Indicators_Params.byte_In_Trx_Granule_Cur_Num = indicator_Value;
        break;

        case RIO_ARE_ALL_IDLES_IN_GRANULE:
            handle->state_Indicators_Params.are_All_Idles_In_Granule = indicator_Value;
        break;

        case RIO_SYMBOL_IS_PD:
            handle->state_Indicators_Params.is_Packet_Delimiter = indicator_Value;
        break;
    
        case RIO_IS_ALIGNMENT_LOST:
            handle->state_Indicators_Params.is_Align_Lost = indicator_Value;
        break;

        case RIO_IS_SYNC_LOST:
            handle->state_Indicators_Params.is_Sync_Lost = indicator_Value;
        break;

        case RIO_IS_CHARACTER_INVALID:
            handle->state_Indicators_Params.is_Character_Invalid = indicator_Value;
        break;

        case RIO_COPY_TO_LANE2:
            handle->state_Indicators_Params.copy_To_Lane2 = indicator_Value;
        break;

        case RIO_SUPPRESS_HOOK:
            handle->state_Indicators_Params.suppress_Hook = indicator_Value;
        break;

        case RIO_INITIAL_RUN_DISP:
            handle->state_Indicators_Params.initial_Run_Disp = (unsigned char)indicator_Value;
        break;

        case RIO_DECODE_ANY_RUN_DISP:
            handle->state_Indicators_Params.decode_Any_Run_Disp = indicator_Value;
        break;

        case RIO_LANE2_JUST_FORCED_BY_DISCOVERY:
            handle->state_Indicators_Params.lane2_Just_Forced_By_Discovery = indicator_Value;
        break;

        case RIO_RX_IS_PD:
            handle->state_Indicators_Params.rx_Packet_Is_Ongoing = (RIO_BOOL_T)indicator_Value;
        break;


        /****************************************************************************/

        default:
        break;
    }
    
   

    return RIO_OK;
}

/********************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */
/***************************************************************************
 * Function : RIO_PL_Conv_Size_To_Hw_Cnt
 *
 * Description: return count of DW which can be issue in request packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_PL_Conv_Size_To_Hw_Cnt(unsigned int size)
{



    /* 
     * here performs conversion from RIO size encoding to the count 
     * of half-words. These constants are defined in the specifactions
     * and can be changed here in case of specifications changing 
     */
    switch (size)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 6:
        case 8:
        case 9:
            return 0x04;

        case 10:
            return 0x08;

        case 11:
            return 0x10;

        case 12:
            return 0x20;

        case 13:
            return 0x40;

        case 14:
            return 0x60;

        case 15:
            return DS_PKT_SIZE;	/* GDA - 0xFF */

        /* reserved value */
        default:
            return 0x00;
    }
}

/************************************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */

