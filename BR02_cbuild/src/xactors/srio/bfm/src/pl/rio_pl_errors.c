/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\pl\rio_pl_errors.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  physical layer errors handlers source
*
* Notes:
*
******************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "rio_pl_ds.h"
#include "rio_pl_errors.h"

#define RIO_PL_MAX_ERR_STR     255
#define RIO_PL_ERR_MAX_ADD_PAR 3

typedef struct {
    char *string;
    int count_Of_Add_Args;
} PL_ERROR_WARNING_ENTRY_T;

/*
 * array of error strings
 */
#if defined(RIO_PL_MODEL)  

static char* granules_Types [] = {
    "NEW PACKET granule",
    "DATA granule",
    "PACKET_ACCEPTED acknowledge symbol" ,
    "PACKET_RETRY acknowledge symbol" ,
    "PACKET_NOT_ACCEPTED acknowledge symbol" ,
    "IDLE control symbol" ,
    "THROTTLE control symbol", 
    "RESTART_FROM_RETRY control symbol" ,
    "TOD_SYNC control symbol" , 
    "EOP control symbol",
    "STOMP control symbol",
    "LINK_REQUEST control symbol",
    "LINK_RESPONSE control symbol",
    "TRAINING pattern",
    "CORRUPTED symbol",
    "UNRECOGNIZED granule (with reserved fields encoding)",
    "granule with S-PARITY BIT error"
};

static char* transmitter_Training_State [] = {
    "RIO_PL_TX_RESET",
    "RIO_PL_TX_SEND_TRN_REQ",
    "RIO_PL_TX_SEND_TRN_PTTN",
    "RIO_PL_TX_SEND_IDLES",
    "RIO_PL_TX_OK",
    "RIO_PL_TX_OK_SEND_TRN_PTTN",
    "RIO_PL_TX_OK_SEND_IDLES",
    "RIO_PL_TX_SEND_IDLES_REQ"
}; 

/*
 * this type represents receiver training machine state
 */
static char* receiver_Training_State [] = {
    "RIO_PL_RX_RESET",
    "RIO_PL_RX_WAIT_GOOD_PTTN",
    "RIO_PL_RX_WAIT_FOR_IDLE",
    "RIO_PL_RX_OK",
    "RIO_PL_RX_PRE_RESET",
    "RIO_PL_RX_RECEIVE_TRAIN_SEQ_OK"
};

/*corresponds to RIO_BC_PACKET_VIOLATION_T enum*/
static char * packet_Violations_Msg [] = {
    "packet is longer than 276 bytes \n",
    "incorrect CRC in packet \n",
    "reserved ftype value in packet \n",
    "reserved ttype value in packet \n",
    "reserved tt value in packet \n",
    "reserved bits in packet are non-zero \n",
    "incorrect packet header\n",
    "packet payload is not double-word aligned \n"
};

static PL_ERROR_WARNING_ENTRY_T pl_Message_Strings[] = {
    {"instance %lu %s", 1},
    {"instance %lu. Attempt to invoke clock_Edge callback\n\thas failed.\n", 0},
    {"instance %lu. Attempt to initialize in work mode.\n", 0},
    {"instance %lu. Attempt to set pass_By_Prio to wrong value %lu.\n", 1},
    {"instance %lu. Attempt to invoke set_Pins callback has failed.\n", 0},
    {"instance %lu. Instance has been invoked from uninstantiated TxRx (get_Granule).\n", 0},
    {"instance %lu. Instance has been invoked from uninstantiated TxRx (put_Granule).\n", 0},
    {"instance %lu. Attempt to invoke get_Pins callback has failed.\n", 0},
    {"instance %lu. Attempt to read register using misaligned address %lu.\n", 1},
    {"instance %lu. Attempt to write register using misaligned address %lu.\n", 1},

    {"instance %lu. Attempt to write to reserved address %lu.\n", 1},
    {"instance %lu. Attempt to write to read//only register %lu.\n", 1},
    {"instance %lu. Attempt to read by reserved address %lu.\n", 1},
    {"instance %lu. Attempt to read write//only register %lu.\n", 1},
    {"instance %lu. Received data out of any packet receiving.\n", 0},
    {"instance %lu. Output error is detected: received unexpected STOMP control symbol.\n", 0},
    {"instance %lu. Output error is detected: received unexpected EOP control symbol.\n", 0},
    {"instance %lu. Output error is detected: received unexpected THROTTLE control symbol.\n", 0},
    {"instance %lu. Trying to send LINK_REQUEST control symbol when one is already" \
        "waiting for heading out.\n", 0},
    {"instance %lu. Output error is detected: received LINK_RESPONSE control symbol " \
        "without corresponding LINK_REQUEST control symbol.\n", 0},

    {"instance %lu. Writing to Link Validation Request Register wrong cmd field %lu.\n", 1},
    {"instance %lu. Output error is detected: received LINK_REQUEST control symbol while " \
        "previous LINK_RESPONSE is waiting for heading out.\n", 0},
    {"instance %lu. Output error is detected:received unexpected RESTART_FROM_RETRY control symbol.\n", 0},
    {"instance %lu. LINK_REQUEST control symbol has been retransmitted %lu times due to time-out.\n", 1},
    {"instance %lu. Wrong ttype %lu for ftype %lu.\n", 2},
    {"instance %lu. Wrong payload size %lu for ftype %lu and type %lu .\n", 3},
    {"instance %lu. Maintanence read request with wrong dw_Size %lu and sub_Dw_Pos %lu.\n", 2},
    {"instance %lu. After %lu retransmissions " \
        "of request %lu due to PL time-out it has been dropped.\n", 2},
    {"instance %lu. After %lu retransmissions of response %lu " \
        "due to PL time-out it has been dropped.\n", 2},
    {"instance %lu. After %lu retransmissions of request %lu " \
        "due to LL time-out it has been dropped.\n", 2},
    
    {"instance %lu. Maintanence read response with wrong data paylod size %lu.\n", 1},
    {"instance %lu. Received response with target_TID\\target_info %lu without any requests.\n", 1},
    {"instance %lu. Response with wrong data paylod size %lu.\n", 1},
    {"instance %lu. Wrong prio field %lu.\n", 1},
    {"instance %lu. Wrong mbox field %lu.\n", 1},
    {"instance %lu. Wrong msglen field %lu.\n", 1},
    {"instance %lu. Wrong msgseg %lu and msglen %lu combination.\n", 2},
    {"instance %lu. Wrong payload size %lu for the last segment (ssize %lu) .\n", 2},
    {"instance %lu. Wrong payload size %lu for not last segment (ssize %lu) .\n", 2},
    {"instance %lu. Wrong letter field %lu.\n", 1},

    {"instance %lu. Message payload size %lu is bigger than min(maxSMDP, confSMDP) %lu.\n", 2},
    {"instance %lu. Count of segments in the message %lu " \
        "is bigger than min(maxSMP, confSMP) %lu.\n", 2},
    {"instance %lu. Response payload size %lu is " \
        "bigger than min(maxSRespPDP, confSRespPDP) %lu.\n", 2},
    {"instance %lu. GSM request payload size %lu is bigger than %lu.\n", 2},
    {"instance %lu. GSM multicast does not supported for ftype %lu and ttype %lu.\n", 2},
    {"instance %lu. GSM multicast for %lu recipients does not supported.\n", 1},
    {"instance %lu. Attempt to write incorrect value to register %lu.\n", 1},
    /* RIO_PL_ERROR_48 */
    {"instance %lu. Output error is detected: received unexpected  %s.\n", 1},
    {"instance %lu. Training sequence has not been followed by IDLE control symbol\n", 0},
    {"instance %lu. Output error is detected: received unexpected TRAINING pattern\n", 0},
    {"instance %lu. Non-recoverable error is detected. Output stopped \n", 0},
    {"instance %lu. Output error is detected: received acknowledge with unexpected ackID.\n", 0},
    {"instance %lu. Output error is detected: received PACKET_ACCEPTED acknowledge symbol on still sending packet.\n", 0},
    {"instance %lu. Output error is detected: received PACKET_NOT_ACCEPTED acknowledge symbol\n", 0},
    {"instance %lu. Time-Out for LINK_REQUEST control symbol after %lu clock events.\n", 1},
    {"instance %lu. Output error is detected: Time-Out(physical) for request %lu control after %lu clock events.\n", 2},
    {"instance %lu. Output error is detected: Time-Out(physical) for response %lu control after %lu clock events.\n", 2},
    {"instance %lu. Input error is detected: internal error.\n", 0},
    {"instance %lu. Input error is detected: received packet is greater than 276 bytes or is not word aligned.\n", 0},
    {"instance %lu. Input error is detected: bad CRC in received packet.\n", 0},
    {"instance %lu. Input error is detected: unexpected ackID in received packet.\n", 0},
    {"instance %lu. Input error is detected: received corrupted symbol.\n", 0},
    {"instance %lu. Input error is detected: received granule with s-bit parity error.\n", 0},

    {"instance %lu %s", 1},
    {"instance %lu. The instanse go through initializitation sequence " \
        "due to RESET command receiving.\n", 0},
    {"instance %lu. Time-Out for LINK_REQUEST control symbol after %lu clock events.\n", 1},
    /*{"instance %lu. Time-Out(physical) for request %lu control after %lu clock events.\n", 2},*/
    {"instance %lu. Time-Out(physical) for packet with ackID %lu control after %lu clock events.\n", 2},
    {"instance %lu. Time-Out(physical) for response %lu control after %lu clock events.\n", 2},
    {"instance %lu. Time-Out(logical) for request %lu control after %lu clock events.\n", 2},
    {"instance %lu. Input training state machine: transits into %s state.\n", 1},
    {"instance %lu. Output training state machine: transits into %s state.\n", 1},
    {"instance %lu. The number of received trainings is  %u.\n", 1},
    {"instance %lu. Received %s.\n", 1},
    {"instance %lu. Error in received packet is detected: %s.\n", 1},

    /*------------------------------------------*/
        /*added entry*/
    {"(physical layer, instance %lu). Packet-Not_Accepted is received with cause: %u.\n", 1},

    /*------------------------------------------*/

    {"instance %lu. Input error is detected: received invalid character, or valid but illegal.\n", 0},


    {"instance %lu. Received %s.\n", 1},
    /*------------------------------------------*/
    {"(instance %lu). symbol with stype = %i received in output error stopped state\n", 1},

    {"(instance %lu). Output error is detected: received unsolicited acknowledge ", 0},

    {"(instance %lu). Input error is detected: Received more than %i unacknowledged packets ", 1},

    {"(instance %lu).  Non-recoverable error is detected: Received link responce with unexpected ackId."\
    " LRQ will be resent", 1},
    {"(instance %lu). Non-recoverable error is detected: Link Responce timeout. LRQ will be resent", 1},
    {"(instance %lu). Packet-Accepted is received for stomped packet", 1}

};

static char pl_Error_Prefix_Str[]   = "RIO Physical layer model: Error{#%lu}: ";
static char pl_Warning_Prefix_Str[] = "RIO Physical layer model: Warning{#%lu}: ";
static char pl_Note_Prefix_Str[]    = "RIO Physical layer model: Info{#%lu}: ";


#else

static char* granules_Types [] = {
    "NEW PACKET granule",
    "DATA granule",
    "PACKET_ACCEPTED acknowledge symbol" ,
    "PACKET_RETRY acknowledge symbol" ,
    "PACKET_NOT_ACCEPTED acknowledge symbol" ,
    "IDLE control symbol" ,
    "THROTTLE control symbol", 
    "RESTART_FROM_RETRY control symbol" ,
    "TOD_SYNC control symbol" , 
    "EOP control symbol",
    "STOMP control symbol",
    "LINK_REQUEST control symbol",
    "LINK_RESPONSE control symbol",
    "TRAINING pattern",
    "CORRUPTED symbol",
    "UNRECOGNIZED granule (with reserved fields encoding)",
    "granule with S-PARITY BIT error"
};

static char* transmitter_Training_State[] =  {
    "RIO_PL_TX_RESET",
    "RIO_PL_TX_SEND_TRN_REQ",
    "RIO_PL_TX_SEND_TRN_PTTN",
    "RIO_PL_TX_SEND_IDLES",
    "RIO_PL_TX_OK",
    "RIO_PL_TX_OK_SEND_TRN_PTTN"
};

/*
 * this type represents receiver training machine state
 */
static char* receiver_Training_State[] = {
    "RIO_PL_RX_RESET",
    "RIO_PL_RX_WAIT_GOOD_PTTN",
    "RIO_PL_RX_WAIT_FOR_IDLE",
    "RIO_PL_RX_OK",
    "RIO_PL_RX_PRE_RESET",
    "RIO_PL_RX_RECEIVE_TRAIN_SEQ_OK"
};

/*corresponds to RIO_BC_PACKET_VIOLATION_T enum*/
static char * packet_Violations_Msg [] = {
    "packet is longer than 276 bytes \n",
    "incorrect CRC in packet \n",
    "reserved ftype value in packet \n",
    "reserved ttype value in packet \n",
    "reserved tt value in packet \n",
    "reserved bits in packet are non-zero \n",
    "incorrect packet header\n",
    "packet payload is not double-word aligned \n"
};

static PL_ERROR_WARNING_ENTRY_T pl_Message_Strings[] = {   
/* 0 */    {"(physical layer, instance %lu). %s", 1},
/* 1 */    {"(physical layer, instance %lu). Attempt to invoke clock_Edge callback\n\thas failed.\n", 0},
/* 2 */    {"(physical layer, instance %lu). Attempt to initialize in work mode.\n", 0},
/* 3 */    {"(physical layer, instance %lu). Attempt to set pass_By_Prio to wrong value %lu.\n", 1},
/* 4 */    {"(physical layer, instance %lu). Attempt to invoke set_Pins callback thas been failed.\n", 0},
/* 5 */    {"(physical layer, instance %lu). Instance has been invoked from uninstantiated TxRx (get_Granule).\n", 0},
/* 6 */    {"(physical layer, instance %lu). Instance has been invoked from uninstantiated TxRx (put_Granule).\n", 0},
/* 7 */    {"(physical layer, instance %lu). Attempt to invoke get_Pins callback has failed.\n", 0},
/* 8 */    {"(physical layer, instance %lu). Attempt to read register using misaligned address %lu.\n", 1},
/* 9 */    {"(physical layer, instance %lu). Attempt to write register using misaligned address %lu.\n", 1},

/* 10 */    {"(physical layer, instance %lu). Attempt to write to reserved address %lu.\n", 1},
/* 11 */    {"(physical layer, instance %lu). Attempt to write to read//only register %lu.\n", 1},
/* 12 */    {"(physical layer, instance %lu). Attempt to read by reserved address %lu.\n", 1},
/* 13 */    {"(physical layer, instance %lu). Attempt to read write//only register %lu.\n", 1},
/* 14 */    {"(physical layer, instance %lu). Received data out of any packet receiving.\n", 0},
/* 15 */    {"(physical layer, instance %lu). Output error is detected: received unexpected STOMP control symbol.\n", 0},
/* 16 */    {"(physical layer, instance %lu). Output error is detected: received unexpected EOP control symbol.\n", 0},
/* 17 */    {"(physical layer, instance %lu). Output error is detected: received unexpected THROTTLE control symbol.\n", 0},
/* 18 */    {"(physical layer, instance %lu). Trying to send LINK_REQUEST control symbol when one is already" \
        "waiting for heading out.\n", 0},
/* 19 */    {"(physical layer, instance %lu). Output error is detected: received LINK_RESPONSE control symbol " \
        "without corresponding LINK_REQUEST.\n", 0},

/* 20 */    {"(physical layer, instance %lu). Writing to Link Validation Request Register wrong cmd field %lu.\n", 1},
/* 21 */    {"(physical layer, instance %lu). Output error is detected: received LINK_REQUEST control symbol while " \
        "previous LINK_RESPONSE control symbol is waiting for heading out.\n", 0},
/* 22 */    {"(physical layer, instance %lu). Output error is detected: received unexpected RESTART_FROM_RETRY control symbol.\n", 0},
/* 23 */    {"(physical layer, instance %lu). LINK_REQUEST control symbol has been retransmitted %lu times due to time-out.\n", 1},
/* 24 */    {"(physical layer, instance %lu). Wrong ttype %lu for ftype %lu.\n", 2},
/* 25 */    {"(physical layer, instance %lu). Wrong payload size %lu for ftype %lu and ttype %lu .\n", 3},
/* 26 */    {"(physical layer, instance %lu). Maintanence read request with wrong dw_Size %lu and sub_Dw_Pos %lu.\n", 2},
/* 27 */    {"(physical layer, instance %lu). After %lu retransmissions " \
        "of request %lu due to PL time-out it has been dropped.\n", 2},
/* 28 */    {"(physical layer, instance %lu). After %lu retransmissions of response %lu " \
        "due to PL time-out it has been dropped.\n", 2},
/* 29 */    {"(physical layer, instance %lu). After %lu retransmissions of request %lu " \
        "due to LL time-out it has been dropped.\n", 2},

/* 30 */    {"(physical layer, instance %lu). Maintanence read response with wrong data paylod size %lu.\n", 1},
/* 31 */    {"(physical layer, instance %lu). Received response with target_TID\\target_info %lu without any requests.\n", 1},
/* 32 */    {"(physical layer, instance %lu). Response with wrong data paylod size %lu.\n", 1},
/* 33 */    {"(physical layer, instance %lu). Wrong prio field %lu.\n", 1},
/* 34 */    {"(physical layer, instance %lu). Wrong mbox field %lu.\n", 1},
/* 35 */    {"(physical layer, instance %lu). Wrong msglen field %lu.\n", 1},
/* 36 */    {"(physical layer, instance %lu). Wrong msgseg %lu and msglen %lu combination.\n", 2},
/* 37 */    {"(physical layer, instance %lu). Wrong payload size %lu for the last segment (ssize %lu) .\n", 2},
/* 38 */    {"(physical layer, instance %lu). Wrong payload size %lu for not last segment (ssize %lu) .\n", 2},
/* 39 */    {"(physical layer, instance %lu). Wrong letter field %lu.\n", 1},

/* 40 */    {"(physical layer, instance %lu). Message payload size %lu is bigger than min(maxSMDP, confSMDP) %lu.\n", 2},
/* 41 */    {"(physical layer, instance %lu). Count of segments in the message %lu " \
        "is bigger than min(maxSMP, confSMP) %lu.\n", 2},
/* 42 */    {"(physical layer, instance %lu). Response payload size %lu is " \
        "bigger than min(maxSRespPDP, confSRespPDP) %lu.\n", 2},
/* 43 */    {"(physical layer, instance %lu). GSM request payload size %lu is bigger than %lu.\n", 2},
/* 44 */    {"(physical layer, instance %lu). GSM multicast is not supported for ftype %lu and ttype %lu.\n", 2},
/* 45 */    {"(physical layer, instance %lu). GSM multicast for %lu recipients is not supported.\n", 1},
/* 46 */    {"(physical layer, instance %lu). Attempt to write incorrect value to register %lu.\n", 1},
/* 47 */    {"(physical layer, instance %lu). Output error is detected: received unexpected %s.\n", 1},
/* 48 */    {"(physical layer, instance %lu). Training sequence has not been followed by IDLE control symbol\n", 0},
/* 49 */    {"(physical layer, instance %lu). Output error is detected: received unexpected TRAINING pattern\n", 0},
/* 50 */    {"(physical layer, instance %lu). Non-recoverable error is detected. Output stopped \n", 0},
/* 51 */    {"(physical layer, instance %lu). Output error is detected: received acknowledge with unexpected ackID.\n", 0},
/* 52 */    {"(physical layer, instance %lu). Output error is detected: received Packet_Accepted acknowledge" \
        "symbol before packet transmission is finished.\n", 0},
/* 53 */    {"(physical layer, instance %lu). Output error is detected: received Packet_Not_Accepted acknowledge symbol.\n", 0},
/* 54 */    {"(physical layer, instance %lu). Time-Out for LINK_REQUEST control symbol after %lu clock events.\n", 1},
/* 55 */    {"(physical layer, instance %lu). Output error is detected: Time-Out(physical) for request" \
        "%lu control after %lu clock events.\n", 2},
/* 56 */    {"(physical layer, instance %lu). Output error is detected: Time-Out(physical) for response" \
        "%lu control after %lu clock events.\n", 2},
/* 57 */    {"(physical layer, instance %lu). Input error is detected: internal error.\n", 0},
/* 58 */    {"(physical layer, instance %lu). Input error is detected: received packet is greater " \
        "than 276 bytes or is not word aligned.\n", 0},
/* 59 */    {"(physical layer, instance %lu). Input error is detected: bad CRC in received packet.\n", 0},
/* 60 */    {"(physical layer, instance %lu). Input error is detected: unexpected ackID in received packet.\n", 0},
/* 61 */    {"(physical layer, instance %lu). Input error is detected: received corrupted symbol.\n", 0},
/* 62 */    {"(physical layer, instance %lu). Input error is detected: received granule with s-bit parity error.\n", 0},

/* 63 */    {"(physical layer, instance %lu). %s", 1},
/* 64 */    {"(physical layer, instance %lu). The instanse gooes through initializitation sequence " \
        "due to RESET command receiving.\n", 0},
/* 65 */    {"(physical layer, instance %lu). Time-Out for LINK_REQUEST control symbol after %lu clock events.\n", 1},
/* 66 */    {"(physical layer, instance %lu). Time-Out(physical) for request %lu control after %lu clock events.\n", 2},
/* 67 */    {"(physical layer, instance %lu). Time-Out(physical) for response %lu control after %lu clock events.\n", 2},
/* 68 */    {"(physical layer, instance %lu). Time-Out(logical) for request %lu control after %lu clock events.\n", 2},
/* 69 */    {"(physical layer, instance %lu). Input training state machine: transits into %s state.\n", 1},
/* 70 */    {"(physical layer, instance %lu). Output training state machine: transits into %s state.\n", 1},
/* 71 */    {"(physical layer, instance %lu). The number of received trainings is  %u.\n", 1},
/* 72 */    {"(physical layer, instance %lu). Received %s.\n", 1},
/* 73 */    {"(physical layer, instance %lu). Error in received packet is detected: %s.\n", 1},

    /*------------------------------------------*/
    /*added entry*/
/* 74 */    {"(physical layer, instance %lu). Packet-Not_Accepted is received with cause: %u.\n", 1},

    /*------------------------------------------*/

/* 75 */    {"(physical layer, instance %lu). Input error is detected: received invalid character, or valid but illegal.\n", 0},


/* 76 */    {"(physical layer, instance %lu). %s.\n", 1},
    /*------------------------------------------*/
/* 77 */    {"(physical layer, instance %lu). symbol with stype = %i received in output error stopped state\n", 1},

/* 78 */    {"(physical layer, instance %lu). Output error is detected: received unsolicited acknowledge ", 0},

/* 79 */    {"(physical layer, instance %lu). Input error is detected: Received more than %i unacknowledged packets ", 1},

/* 80 */    {"(physical layer, instance %lu). Non-recoverable error is detected: Received link responce with unexpected ackId."\
    " LRQ will be resent", 1},
/* 81 */    {"(physical layer, instance %lu). Non-recoverable error is detected: Link Responce timeout. LRQ will be resent", 1},
/* 82 */    {"(physical layer, instance %lu). Packet-Accepted is received for stomped packet", 1}
};

static char pl_Error_Prefix_Str[]   = "Error{#%lu}: ";
static char pl_Warning_Prefix_Str[] = "Warning{#%lu}: ";
static char pl_Note_Prefix_Str[]    = "Info{#%lu}: ";

#endif

static const int pl_Message_Strings_Cnt = sizeof(pl_Message_Strings) / sizeof(pl_Message_Strings[0]);

#define RIO_PL_MAX_ERR_STR 255
#define RIO_PL_ERR_MAX_ADD_PAR 3


/* the size of this array shall be equal to size of ll_Message_Strings array */    
static RIO_PL_MSG_TYPE_T pl_Message_To_Type_Map[] = {
    RIO_PL_MSG_ERROR_MSG,     /* do not change: to this entry all 32 bit txrx errors are mapped */
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,

    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_WARNING_MSG,

    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
        
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,

    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_WARNING_MSG,
        /* RIO_PL_ERROR_48 */
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,

    RIO_PL_MSG_WARNING_MSG,   /* do not change: to this entry all 32 bit txrx warnings are mapped */
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_NOTE_MSG,
    RIO_PL_MSG_NOTE_MSG,
    RIO_PL_MSG_NOTE_MSG,
    RIO_PL_MSG_NOTE_MSG,
    RIO_PL_MSG_WARNING_MSG,

    /*--------------------------*/

    RIO_PL_MSG_WARNING_MSG, /*added entry*/
    /*-----------------------------*/

    RIO_PL_MSG_WARNING_MSG,



    RIO_PL_MSG_NOTE_MSG,      /* do not change: to this entry all 32 bit txrx errors are mapped */

    RIO_PL_MSG_WARNING_MSG, /*unexpected ackID received in output error stopped state*/
    RIO_PL_MSG_WARNING_MSG,  /*unsolicited acknowledge*/
    RIO_PL_MSG_ERROR_MSG, /*received more than 7(31) unacknowleged packet*/
    RIO_PL_MSG_WARNING_MSG, /*incorrect ackid value in the link responce, mantis 44 implementation*/
    RIO_PL_MSG_WARNING_MSG, /*timeout for link responce, mantis 44 implementation.*/
    RIO_PL_MSG_WARNING_MSG  /*received PA for stomped packet.*/
};

static const int pl_Message_Map_Count = sizeof(pl_Message_To_Type_Map) / sizeof(pl_Message_To_Type_Map[0]);



/***************************************************************************
 * Function : RIO_PL_Print_Message
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Print_Message(
    RIO_PL_DS_T* handle, RIO_PL_MESSAGES_T message_Type, ...)
{
    /* 
     * buffer to store error message and additional pointer becasue
     * for the first warning argument will be of this type
     */
    char message_Buffer[RIO_PL_MAX_ERR_STR];
    char temp_Buffer[RIO_PL_MAX_ERR_STR]; 
    char *warning_1 = NULL;

    /* 
     * additional information which shall present in warning message
     * is passing through additional (optional) parameters:
     * add_Args[0]...[2]. 
     * i - loop counter
     */
    unsigned long add_Args[RIO_PL_ERR_MAX_ADD_PAR] = {0, 0, 0};
    int i;

    /* this is necessary to detect additional parameters */
    va_list pointer_To_Next_Arg;

    /*
     * check all using pointers to be not NULL, check
     * that type of error is within corresponding array
     * and when invoke callback with actual parameters.
     * actual error message obtain using error type as an index
     */
    if (handle == NULL || 
        handle->ext_Interfaces.rio_User_Msg == NULL ||
        (int)message_Type < 0 || 
        (int)message_Type > (pl_Message_Strings_Cnt - 1) ||
        (int)message_Type > (pl_Message_Map_Count - 1) ||
        pl_Message_Map_Count != pl_Message_Strings_Cnt)
        return RIO_ERROR;

    /* 
     * get additional parameters, which is int type for all warnings,
     * exclude 1 that comes from TxRx (char*)
     */
    va_start(pointer_To_Next_Arg, message_Type);

    for (i = 1; i <= pl_Message_Strings[(int)message_Type].count_Of_Add_Args; i++)
    {
        if (message_Type == RIO_PL_ERROR_1   || 
            message_Type == RIO_PL_WARNING_1 ||
            message_Type == RIO_PL_NOTE_1)
        {
            warning_1 = va_arg(pointer_To_Next_Arg, char*);     
        }
        else if(message_Type == RIO_PL_ERROR_48)
        {
            warning_1 = granules_Types[va_arg(pointer_To_Next_Arg, unsigned long)];
        }
        else if(message_Type == RIO_PL_WARNING_7)
        {
            warning_1 = receiver_Training_State[va_arg(pointer_To_Next_Arg, unsigned long)];
        }
        else if(message_Type == RIO_PL_WARNING_8)
        {
            warning_1 = transmitter_Training_State[va_arg(pointer_To_Next_Arg, unsigned long)];
        }
        else if(message_Type == RIO_PL_WARNING_10)
        {
            warning_1 = granules_Types[va_arg(pointer_To_Next_Arg, unsigned long)];
        }
        else if(message_Type == RIO_PL_WARNING_11)
        {
            warning_1 = packet_Violations_Msg[va_arg(pointer_To_Next_Arg, unsigned long)];
        }
        else
        {
            add_Args[i - 1] = va_arg(pointer_To_Next_Arg, unsigned long);     
        }
    }

    va_end(pointer_To_Next_Arg);

    /*
     * obtain actual warning message - the constants have sense of additional arguments 
     * count
     */
    switch (pl_Message_Strings[(int)message_Type].count_Of_Add_Args)
    {
        case 0:
            sprintf(message_Buffer, pl_Message_Strings[(int)message_Type].string,
                handle->instance_Number);
            break;

        case 1:
            if (message_Type == RIO_PL_ERROR_1 || message_Type == RIO_PL_ERROR_48)
            {
                sprintf(message_Buffer, pl_Message_Strings[(int)message_Type].string,
                    handle->instance_Number, warning_1);
            }
            else if (message_Type == RIO_PL_WARNING_1 ||
                        message_Type == RIO_PL_NOTE_1 ||
                        message_Type == RIO_PL_WARNING_7 ||
                        message_Type == RIO_PL_WARNING_8 ||
                        message_Type == RIO_PL_WARNING_10 || 
                        message_Type == RIO_PL_WARNING_11)
            {
                sprintf(message_Buffer, pl_Message_Strings[(int)message_Type].string,
                    handle->instance_Number, warning_1);
            }
            else
            {
                sprintf(message_Buffer, pl_Message_Strings[(int)message_Type].string,
                    handle->instance_Number, add_Args[0]);
            }
            break;

        case 2:
            sprintf(message_Buffer, pl_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1]);
            break;

        case 3:
            sprintf(message_Buffer, pl_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1], add_Args[2]);
            break;

        default:
            return RIO_ERROR;
    }   

    /* detect the type of message */
    switch (pl_Message_To_Type_Map[(int)message_Type])
    {
        case RIO_PL_MSG_WARNING_MSG:
            sprintf(temp_Buffer, pl_Warning_Prefix_Str, (int)message_Type);
            break;
        
        case RIO_PL_MSG_ERROR_MSG:
            sprintf(temp_Buffer, pl_Error_Prefix_Str, (int)message_Type);
            break;
        
        case RIO_PL_MSG_NOTE_MSG:
            sprintf(temp_Buffer, pl_Note_Prefix_Str, (int)message_Type);
            break;
        
        default:
            return RIO_ERROR;
    
    } 
    
    /*strcat(temp_Buffer, message_Buffer);*/
    strcpy(&temp_Buffer[strlen(temp_Buffer)], message_Buffer);
    
    /* print result error message*/
    handle->ext_Interfaces.rio_User_Msg(
        handle->ext_Interfaces.rio_Msg, 
        temp_Buffer);

    return RIO_OK;

}



/**************************************************************************/

