/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/pl/rio_pl_pllight_serial_model.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  RapidIO PL and PLLIGHT serial model common part
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "rio_pl_pllight_serial_model.h"
#include "rio_pl_pllight_model_common.h"

#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_rx.h"
#include "rio_pl_tx.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"

#include "rio_pcs_pma_model.h"
#include "rio_pcs_pma_adapter.h"

#ifdef _LICENSING
#include "lmpolicy.h"
#endif /* _LICENSING */


/* these are constants defining licensing for the RapidIO Serial model */
#ifdef _LICENSING

#define RIO_SERIAL_FEATURE_NAME    "mot_eda_rapidio_serial_bfm"
#define RIO_SERIAL_FEATURE_VER     "1.0"
#define RIO_SERIAL_LIC_SEARCH_PATH "mot_rapidio_serial_bfm.lic;."

#endif /* _LICENSING */

/* count of LP-EP interfaces*/
#define RIO_PL_COUNT_OF_LPEP 1

/* count of LP-EP interfaces*/
#define RIO_PL_SERIAL_ACKID_QUEUE_SIZE  32 

/* variable for number of created instanses counting */
static unsigned long pl_Instanses_Count = 0;


/*
 * prototypes for static functions to be passed via ftray/ctray
 */

static int pl_Model_Start_Reset(
    RIO_HANDLE_T handle
    );

static void pl_Commands_Handler(
    RIO_HANDLE_T                handle,
    RIO_COMMAND_PL_WRAP_T       cmd
    );


static int pl_Model_Initialize(
    RIO_HANDLE_T              handle,
    RIO_PL_SERIAL_PARAM_SET_T *param_Set
    );

static int pl_Clock_Edge(RIO_HANDLE_T handle);

static int pl_Get_Pins(
    RIO_HANDLE_T handle, /* instanse's handle */
    RIO_SIGNAL_T rlane0, /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1, /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2, /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3  /* receive data on lane 3*/
    );

static int pl_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T tlane0,   /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,   /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,   /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3    /* transmit data on lane 3*/
    );

int pl_Granule_To_Send(
    RIO_CONTEXT_T         context, /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule /* pointer to granule data structure */
);

int pl_Granule_Received(
    RIO_CONTEXT_T         context, /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule /* pointer to granule data structure */
);

int pl_Character_Column_To_Send(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
);

int pl_Character_Column_Received(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
);

int pl_Codegroup_Column_To_Send(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
);

int pl_Codegroup_Received(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
);

static int pl_Delete_Instanse(
    RIO_HANDLE_T          handle
    );


/*
 * Prototypes for static functions used inside the file
 */

static int pl_Create_Bind_Link(RIO_PL_DS_T *handle);
static int pl_Initialize_Instanse(RIO_PL_DS_T *instanse);
static int check_Init_Param_Set(RIO_PL_DS_T *instanse, RIO_PL_SERIAL_PARAM_SET_T *param_Set);
static int check_Inst_Param_Set(RIO_MODEL_INST_PARAM_T *param_Set);
static void count_Time_Outs(RIO_PL_DS_T *handle);

#ifdef _CARBON_SRIO_XTOR_FIX
void multicast_Delay_Time_Out(RIO_PL_DS_T *handle);
void response_Delay_Time_Out(RIO_PL_DS_T *handle, RIO_GRANULE_T *granule);
#endif




/* External Physical Layer and Physical Layer Light functions implementation
 */


/***************************************************************************
 * Function : RIO_PL_PLLight_Serial_Model_Create_Instance
 *
 * Description: Create new physical layer instanse
 *
 * Returns: error code
 *
 * Notes: instanse handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_PL_PLLight_Serial_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_MODEL_INST_PARAM_T  *param_Set
    )
{
    /* handle to structure will allocated*/
    RIO_PL_DS_T *pl_Instanse = NULL;

#ifdef _LICENSING

    static RIO_BOOL_T is_Lic_Checkedout = RIO_FALSE;
    LP_HANDLE *lp_Handle;
    
#endif /* _LICENSING */

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for physical layer 
     * data structure
     */
    if (handle == NULL || 
        param_Set == NULL || 
        check_Inst_Param_Set(param_Set) == RIO_ERROR)
        return RIO_ERROR;

#ifdef _LICENSING
    /* try checking the license for the model */
    if( is_Lic_Checkedout == RIO_FALSE )
    {
        if( lp_checkout(LPCODE, LM_RESTRICTIVE | LM_MANUAL_HEARTBEAT, RIO_SERIAL_FEATURE_NAME, 
                        RIO_SERIAL_FEATURE_VER, 1, RIO_SERIAL_LIC_SEARCH_PATH, &lp_Handle) )
        {
            fprintf(stderr, "RapidIO PL Model Serial Create Instance: checkout of feature %s failed: %s\n", 
                    RIO_SERIAL_FEATURE_NAME, lp_errstring(lp_Handle));

            /* set handle to NULL */
            *handle = NULL;

            /* return RIO_ERROR;*/
            return RIO_ERROR;
        }
        else
        {
            is_Lic_Checkedout = RIO_TRUE;
            fprintf(stderr, "RapidIO PL Model Serial Create Instance: successfull checkout of feature %s\n", 
                    RIO_SERIAL_FEATURE_NAME);
        }
    }
#endif /* _LICENSING */

    /* clear handle */
    *handle = NULL;

    /* allocate instanse */
    if ((pl_Instanse = malloc(sizeof(RIO_PL_DS_T))) == NULL)
        return RIO_ERROR;

    /* mark the instanse as the seriall one */
    pl_Instanse->is_Parallel_Model = RIO_FALSE;

    /* mark the instance as not bound one */
    pl_Instanse->was_Bound = RIO_FALSE;

    /* allocate mamory for LP-EP registers */
    pl_Instanse->reg_Set.lpep_Count  = RIO_PL_COUNT_OF_LPEP;
        pl_Instanse->reg_Set.lpep_Size   = RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP; 
    if ((pl_Instanse->reg_Set.lpep_Regs = malloc(sizeof(RIO_PL_LPEP_REGS_T) * pl_Instanse->reg_Set.lpep_Count)) == NULL)
    {
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for outboud buffer */
    if ((pl_Instanse->outbound_Buf.buffer = 
        malloc(sizeof(RIO_PL_OUT_ENTRY_T) * param_Set->pl_Outbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for sending queue */
    if ((pl_Instanse->outbound_Buf.send_Queue = 
        malloc(sizeof(RIO_PL_QUEUE_ELEM_T) * param_Set->pl_Outbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for inbound buffer */
    if ((pl_Instanse->inbound_Buf.buffer = 
        malloc(sizeof(RIO_PL_IN_ENTRY_T) * param_Set->pl_Inbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for inbound queue */
    if ((pl_Instanse->inbound_Buf.rec_Queue = 
        malloc(sizeof(RIO_PL_QUEUE_ELEM_T) * param_Set->pl_Inbound_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /*set size of ackID queues*/
   pl_Instanse->ack_Buffer_Size = RIO_PL_SERIAL_ACKID_QUEUE_SIZE;
    
    /*allocate memory for ackID queues*/
    if ((pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer = 
        malloc(sizeof(RIO_PL_OUT_T) * pl_Instanse->ack_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    if ((pl_Instanse->tx_Data.ack_Out_Buffer.ack_Buffer = 
        malloc(sizeof(RIO_GRANULE_T) * pl_Instanse->ack_Buffer_Size)) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    if ((pl_Instanse->specific_Param_Set = 
        malloc(sizeof(RIO_SERIAL_PARAM_SET_T))) == NULL)
    {
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer);
        free(pl_Instanse->tx_Data.ack_Out_Buffer.ack_Buffer);
        free(pl_Instanse);
        return RIO_ERROR;
    }    

        
    /*
     * clear pointers to external callbacks 
     */
    pl_Instanse->ext_Interfaces.rio_PL_Ack_Request     = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Get_Req_Data    = NULL;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    pl_Instanse->ext_Interfaces.rio_PL_Get_Req_Data_Hw = NULL;  
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    pl_Instanse->ext_Interfaces.rio_PL_FC_To_Send      = NULL;
    /* GDA: Bug#125 */

    pl_Instanse->ext_Interfaces.rio_PL_Ack_Response    = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Get_Resp_Data   = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Req_Time_Out    = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Remote_Request  = NULL;
    pl_Instanse->ext_Interfaces.rio_PL_Remote_Response = NULL;
    pl_Instanse->ext_Interfaces.pl_Interface_Context   = NULL; 

    pl_Instanse->ext_Interfaces.rio_User_Msg = NULL;
    pl_Instanse->ext_Interfaces.rio_Msg      = NULL;

    pl_Instanse->ext_Interfaces.extend_Reg_Read    = NULL;
    pl_Instanse->ext_Interfaces.extend_Reg_Write   = NULL;
    pl_Instanse->ext_Interfaces.extend_Reg_Context = NULL;

    pl_Instanse->ext_Interfaces.rio_Request_Received  = NULL;
    pl_Instanse->ext_Interfaces.rio_Response_Received = NULL;
    pl_Instanse->ext_Interfaces.rio_Packet_To_Send    = NULL;
    pl_Instanse->ext_Interfaces.rio_Hooks_Context     = NULL;
    pl_Instanse->ext_Interfaces.rio_Symbol_To_Send    = NULL;

    /* allocate memory for serial callback tray */
    pl_Instanse->ext_Specific_Interfaces = (RIO_HANDLE_T)malloc(sizeof(RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T));
    memset((void*)(pl_Instanse->ext_Specific_Interfaces), 0 , sizeof(RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T)); 

    /*
     * Allocate memory for PCS/PMA FTray and clear it
     */
    pl_Instanse->link_Instanse  = NULL;
    pl_Instanse->link_Interface = (RIO_HANDLE_T)malloc(sizeof(RIO_PCS_PMA_MODEL_FTRAY_T));
    ((RIO_PCS_PMA_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->rio_Serial_Get_Pins = NULL; 
    ((RIO_PCS_PMA_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->clock_Edge          = NULL; 
    ((RIO_PCS_PMA_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->start_Reset         = NULL; 
    ((RIO_PCS_PMA_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->initialize          = NULL; 
    ((RIO_PCS_PMA_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->delete_Instance     = NULL;
    ((RIO_PCS_PMA_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->machine_Management  = NULL;

    /*
     *clear PL PCS/PMA Adapter pointers
     */
    pl_Instanse->mid_Block_Instanse  = NULL;
    pl_Instanse->mid_Block_Interface = (RIO_HANDLE_T)malloc(sizeof(RIO_PL_PCS_PMA_ADAPTER_FTRAY_T));
    ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Adapter_Start_Reset     = NULL;
    ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Adapter_Initialize      = NULL;
    ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Get_Granule             = NULL;
    ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Put_Granule             = NULL;
    ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(pl_Instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Adapter_Delete_Instance = NULL;

    /* try create and bind a link */
    if (pl_Create_Bind_Link(pl_Instanse) == RIO_ERROR)
    {
        free(pl_Instanse->inbound_Buf.rec_Queue);
        free(pl_Instanse->inbound_Buf.buffer);
        free(pl_Instanse->outbound_Buf.send_Queue);
        free(pl_Instanse->outbound_Buf.buffer);
        free(pl_Instanse->reg_Set.lpep_Regs);
        free(pl_Instanse->tx_Data.ack_In_Buffer.ack_Buffer);
        free(pl_Instanse->tx_Data.ack_Out_Buffer.ack_Buffer);
        free(pl_Instanse->specific_Param_Set);
        free(pl_Instanse);
        return RIO_ERROR;
    }

    /*allocation was successful */
    pl_Instanse->instance_Number = ++pl_Instanses_Count;
    pl_Instanse->in_Reset = RIO_TRUE;
    pl_Instanse->inst_Param_Set = *param_Set;
    pl_Instanse->tid = 0;
    pl_Instanse->restart_Function = (RIO_PL_II_MODEL_INITIALIZE)pl_Model_Initialize;
    pl_Instanse->reset_Function   = pl_Model_Start_Reset;
    pl_Instanse->cmd_To_Wrap = pl_Commands_Handler;

    /* set handle for the environment */
    *handle = (RIO_HANDLE_T)pl_Instanse;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_PLLight_Serial_Model_Get_Function_Tray
 *
 * Description: export physical layer instanse function tray
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_PLLight_Serial_Model_Get_Function_Tray(
    RIO_PL_SERIAL_MODEL_FTRAY_T *ftray             
    )
{
    /*
     * check passed poinetrs and if they are valid compete functional
     * tray with actual entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->rio_PL_Clock_Edge              = pl_Clock_Edge;
    ftray->rio_PL_Model_Start_Reset       = pl_Model_Start_Reset;
    ftray->rio_PL_Serial_Model_Initialize = pl_Model_Initialize;
    ftray->rio_PL_Serial_Get_Pins         = pl_Get_Pins;
    
    ftray->rio_PL_Print_Version           = RIO_PL_Print_Version;

    ftray->rio_PL_Delete_Instance         = pl_Delete_Instanse;
    ftray->rio_PL_Enable_Rnd_Idle_Generator = pl_Enable_Rnd_Idle_Generator;
    ftray->rio_PL_Set_Retry_Generator     = RIO_PL_Set_Retry_Generator;
    ftray->rio_PL_Set_PNACC_Generator     = RIO_PL_Set_PNACC_Generator;
    ftray->rio_PL_Set_Stomp_Generator     = RIO_PL_Set_Stomp_Generator;
    ftray->rio_PL_Set_LRQR_Generator      = RIO_PL_Set_LRQR_Generator;

   /*GDA: Rnd Generaotr(PNACC/RETRY)*/
    ftray->rio_PL_Enable_RND_Pnacc_Generator_Init =  RIO_PL_Enable_RND_Pnacc_Generator_Init;
    ftray->rio_PL_Enable_RND_Retry_Generator_Init =  RIO_PL_Enable_RND_Retry_Generator_Init;
   /*GDA: Rnd Generaotr(PNACC/RETRY)*/

   /*GDA: Multicast Contro Symbol Bug#116*/
    ftray->rio_PL_Generate_Multicast =  RIO_PL_Generate_Multicast;
   /*GDA: Multicast Contro Symbol Bug#116*/

    return RIO_OK;
}



/***************************************************************************
  Static functions to be passed via ftray/ctray implementation
***************************************************************************/

/***************************************************************************
 * Function : pl_Commands_Handler
 *
 * Description: hadler of commands from the pl core
 *
 * Returns: 
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Commands_Handler(
    RIO_HANDLE_T                handle,
    RIO_COMMAND_PL_WRAP_T       cmd)
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL) return;

    switch (cmd)
    {
        /*reset the pcs/pma adapter*/
        case REINIT_ADAPTER:
            ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Adapter_Start_Reset(
                instanse->mid_Block_Instanse);
            ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Adapter_Initialize(
                instanse->mid_Block_Instanse);
        break;
    }
}

/***************************************************************************
 * Function : pl_Model_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Model_Start_Reset(
    RIO_HANDLE_T handle
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

    /* check that the instance is the serial model */
    if ( instanse->is_Parallel_Model )
    {
        if ( instanse->was_Bound != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_PL_Msg(
                instanse,
                RIO_PL_MSG_ERROR_MSG,
                "Error: It is not allowed to call 1x/4x LP-Serial specific API"
                " functions while the model is configured to work with the 8/16 LP-LVDS link. \n"
            );
            return RIO_ERROR;
        }
    }

    /* check pointers */
    if (instanse == NULL ||
        ((RIO_PCS_PMA_MODEL_FTRAY_T*)(instanse->link_Interface))->start_Reset == NULL)
        return RIO_ERROR;

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;

    /* send reset to the link */
    if (((RIO_PCS_PMA_MODEL_FTRAY_T*)(instanse->link_Interface))->start_Reset(
        instanse->link_Instanse) == RIO_ERROR
        )
        return RIO_ERROR;

    if (((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Adapter_Start_Reset(
        instanse->mid_Block_Instanse) == RIO_ERROR)
        return RIO_ERROR;

    /* modify state to be _in_reset_ */
    instanse->in_Reset = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Model_Initialize
 *
 * Description: turn the instanse to work mode and initialize
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Model_Initialize(
    RIO_HANDLE_T              handle,
    RIO_PL_SERIAL_PARAM_SET_T *param_Set
    )
{
    /* instanse handle and link's parameters set */
    RIO_PL_DS_T             *instanse = (RIO_PL_DS_T*)handle;
    RIO_PCS_PMA_PARAM_SET_T link_Param_Set;

    /* check that the instance is the serial model */
    if ( instanse->is_Parallel_Model )
    {
        if ( instanse->was_Bound != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_PL_Msg(
                instanse,
                RIO_PL_MSG_ERROR_MSG,
                "Error: It is not allowed to call 1x/4x LP-Serial specific API"
                " functions while the model is configured to work with the 8/16 LP-LVDS link. \n"
            );
            return RIO_ERROR;
        }
    }

    /* check pointers */
    if (instanse == NULL || param_Set == NULL)
        return RIO_ERROR;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

    /* check reset state */
    if (instanse->in_Reset == RIO_FALSE)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_3);
        return RIO_ERROR;
    }

    /* check parameters set */
    if (check_Init_Param_Set(instanse, param_Set) == RIO_ERROR)
        return RIO_ERROR;

    link_Param_Set.is_Init_Proc_On      = RIO_TRUE;
    link_Param_Set.silence_Period       = param_Set->silence_Period;
    link_Param_Set.discovery_Period     = param_Set->discovery_Period;
    link_Param_Set.is_Comp_Seq_Gen_On   = RIO_TRUE;
    link_Param_Set.comp_Seq_Rate        = param_Set->comp_Seq_Rate;
    link_Param_Set.is_Status_Sym_Gen_On = RIO_TRUE;
    link_Param_Set.status_Sym_Rate      = param_Set->status_Sym_Rate;
    link_Param_Set.comp_Seq_Size        = param_Set->comp_Seq_Size;
    link_Param_Set.comp_Seq_Rate_Check  = param_Set->comp_Seq_Rate_Check;
    link_Param_Set.icounter_Max         = param_Set->icounter_Max;
    link_Param_Set.status_Symbols_To_Send = RIO_INIT_STATUS_SYMBOLS_TO_SEND;
    link_Param_Set.status_Symbols_To_Receive = RIO_INIT_STATUS_SYMBOLS_TO_RECEIVE;

    if (((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Adapter_Initialize(
        instanse->mid_Block_Instanse) == RIO_ERROR)
        return RIO_ERROR;

    /* save parameters and initialize the instanse */
    instanse->parameters_Set.transp_Type                   = param_Set->transp_Type;                   
    instanse->parameters_Set.dev_ID                        = param_Set->dev_ID;                       
    instanse->parameters_Set.lcshbar                       = param_Set->lcshbar;                      
    instanse->parameters_Set.lcsbar                        = param_Set->lcsbar;                       
    instanse->parameters_Set.is_Host                       = param_Set->is_Host;                      
    instanse->parameters_Set.is_Master_Enable              = param_Set->is_Master_Enable;             
    instanse->parameters_Set.is_Discovered                 = param_Set->is_Discovered;                
    instanse->parameters_Set.ext_Address                   = param_Set->ext_Address;                  
    instanse->parameters_Set.ext_Address_16                = param_Set->ext_Address_16;               
    instanse->parameters_Set.ext_Mailbox_Support           = param_Set->ext_Mailbox_Support;
    instanse->parameters_Set.input_Is_Enable               = param_Set->input_Is_Enable;              
    instanse->parameters_Set.output_Is_Enable              = param_Set->output_Is_Enable;             
    instanse->parameters_Set.res_For_3_Out                 = param_Set->res_For_3_Out;                
    instanse->parameters_Set.res_For_2_Out                 = param_Set->res_For_2_Out;                
    instanse->parameters_Set.res_For_1_Out                 = param_Set->res_For_1_Out;                
    instanse->parameters_Set.res_For_3_In                  = param_Set->res_For_3_In;                 
    instanse->parameters_Set.res_For_2_In                  = param_Set->res_For_2_In;                 
    instanse->parameters_Set.res_For_1_In                  = param_Set->res_For_1_In;                 
    instanse->parameters_Set.pass_By_Prio                  = param_Set->pass_By_Prio;                 
    instanse->parameters_Set.transmit_Flow_Control_Support = param_Set->transmit_Flow_Control_Support;
    instanse->parameters_Set.lp_Serial_Is_1x               = param_Set->lp_Serial_Is_1x;         
    instanse->parameters_Set.is_Force_1x_Mode              = param_Set->is_Force_1x_Mode;        
    instanse->parameters_Set.is_Force_1x_Mode_Lane_0       = param_Set->is_Force_1x_Mode_Lane_0; 
    instanse->parameters_Set.discovery_Period              = param_Set->discovery_Period;        
    instanse->parameters_Set.silence_Period                = param_Set->silence_Period;          
    instanse->parameters_Set.comp_Seq_Rate                 = param_Set->comp_Seq_Rate;           
    instanse->parameters_Set.status_Sym_Rate               = param_Set->status_Sym_Rate;
    instanse->parameters_Set.comp_Seq_Size                 = param_Set->comp_Seq_Size;           
    instanse->parameters_Set.comp_Seq_Rate_Check           = param_Set->comp_Seq_Rate_Check;           
    instanse->parameters_Set.enable_Retry_Return_Code      = param_Set->enable_Retry_Return_Code;
    instanse->parameters_Set.enable_LRQ_Resending          = param_Set->enable_LRQ_Resending;
    /*GDA:Bug#139 remove the need to recompute crc */
    instanse->data_Corrupted                 = param_Set->data_Corrupted;
    /*GDA:Bug#139 remove the need to recompute crc */
    

    instanse->parameters_Set.receive_Idle_Lim = (unsigned int) (-1);
    instanse->parameters_Set.throttle_Idle_Lim = (unsigned int) (-1);

    instanse->in_Reset = RIO_FALSE;

    *((RIO_PL_SERIAL_PARAM_SET_T *)(instanse->specific_Param_Set)) = *param_Set;
   

    if (pl_Initialize_Instanse(instanse) == RIO_ERROR) 
        return RIO_ERROR;

    /* try to initialize the link */
    if (((RIO_PCS_PMA_MODEL_FTRAY_T*)(instanse->link_Interface))->initialize(
        instanse->link_Instanse,
        &link_Param_Set) == RIO_ERROR)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Clock_Edge
 *
 * Description: physical layer clock
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Clock_Edge(RIO_HANDLE_T handle)
{

    /* instanse handle and the result of callback invokation */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    int         clock_Edge_Result;
    unsigned long data;
    

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /*
     * check handle and callback then return and check the result
     */
    if (instanse == NULL ||
        ((RIO_PCS_PMA_MODEL_FTRAY_T*)(instanse->link_Interface))->clock_Edge == NULL ||
        instanse->in_Reset == RIO_TRUE)
       {
         return RIO_ERROR;
       }
     
    RIO_PL_Read_Register(instanse,
        instanse->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
        &data,
        RIO_TRUE);

    if ((data & 0x00800000) == 0)
    {
        if ((clock_Edge_Result = ((RIO_PCS_PMA_MODEL_FTRAY_T*)(instanse->link_Interface))->clock_Edge(
            instanse->link_Instanse)) == RIO_ERROR)
        {
            RIO_PL_Print_Message(instanse, RIO_PL_ERROR_2);
        }
    }
	else
        clock_Edge_Result = RIO_OK;

    /* count all pl time outs */
    count_Time_Outs(instanse);
     
    /* try unloading external request */
    RIO_PL_In_Not_About_Request(instanse);

    return clock_Edge_Result;
}

/***************************************************************************
 * Function : pl_Set_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T tlane0,   /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,   /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,   /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3    /* transmit data on lane 3*/
    )
{
    RIO_PL_DS_T *instanse;
    int         set_Pins_Result;

    /* check that not NULL instance handle is suplied */
    if ( context == NULL )
        return RIO_ERROR;

    instanse = (RIO_PL_DS_T*)context;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

    /* check that the instance is the serial model */
    if ( instanse->is_Parallel_Model )
    {
        if ( instanse->was_Bound != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_PL_Msg(
                instanse,
                RIO_PL_MSG_ERROR_MSG,
                "Error: It is not allowed to call 1x/4x LP-Serial specific API"
                " functions while the model is configured to work with the 8/16 LP-LVDS link. \n"
            );
            return RIO_ERROR;
        }
    }

    /* check pointers */
    if (instanse == NULL || 
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_PL_Set_Pins == NULL)
        return RIO_ERROR;

    /* invoke environments callback */
    if ((set_Pins_Result = ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_PL_Set_Pins(
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->lp_Serial_Interface_Context,
        tlane0,
        tlane1,
        tlane2,
        tlane3)) == RIO_ERROR)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_5);
    }

    return set_Pins_Result;
}

/***************************************************************************
 * Function : pl_Get_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Get_Pins(
    RIO_HANDLE_T handle, /* instanse's handle */
    RIO_SIGNAL_T rlane0, /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1, /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2, /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3  /* receive data on lane 3*/
    )
{
    RIO_PL_DS_T *instanse;
    int         get_Pins_Result;
    unsigned long     data;

    /* check that not NULL instance handle is supplied */
    if ( handle == NULL )
        return RIO_ERROR;

    instanse = (RIO_PL_DS_T*)handle;

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    if (instanse == NULL ||
        ((RIO_PCS_PMA_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Serial_Get_Pins == NULL)
        return RIO_ERROR;


    /* Check port disable bit */
    RIO_PL_Read_Register(instanse,
            instanse->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

    if ((data & 0x00800000) == 0)
    {
        /* invoke link's function */
        if ((get_Pins_Result = ((RIO_PCS_PMA_MODEL_FTRAY_T*)(instanse->link_Interface))->rio_Serial_Get_Pins(
            instanse->link_Instanse,
            rlane0,
            rlane1,
            rlane2,
            rlane3)) == RIO_ERROR)
        {
            RIO_PL_Print_Message(instanse, RIO_PL_ERROR_8);
        }
    }
	else
        get_Pins_Result = RIO_OK;


    return get_Pins_Result;
}

/***************************************************************************
 * Function : pl_Granule_To_Send
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int pl_Granule_To_Send(
    RIO_CONTEXT_T         context, /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule /* pointer to granule data structure */
)
{
    unsigned int result = 0;
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL ||
       ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Get_Granule == NULL)
       return RIO_ERROR;

    /* get granule from the PL PCS/PMA adapter */
    result = ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Get_Granule(
        instanse->mid_Block_Instanse,
        granule
        );

    if (result == RIO_NO_DATA)
        return result;

    /* invoke environments callback */
    if (((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Granule_To_Send != NULL)
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Granule_To_Send(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Hooks_Context,
            granule
        );
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Granule_Received
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int pl_Granule_Received(
    RIO_CONTEXT_T         context, /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule /* pointer to granule data structure */
)
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL ||
        ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Put_Granule == NULL)
        return RIO_ERROR;

    /* invoke environments callback */
    if (((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Granule_Received != NULL)
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Granule_Received(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Hooks_Context,
            granule
        );
    
    /* put granule to the PL PCS/PMA adapter */
    ((RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)(instanse->mid_Block_Interface))->rio_Pl_Pcs_Pma_Put_Granule(
        instanse->mid_Block_Instanse,
        granule
    );

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Symbol_To_Send
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
    )
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL)
        return RIO_ERROR;

    /* invoke environments callback */
    if(instanse->ext_Interfaces.rio_Symbol_To_Send != NULL)
        instanse->ext_Interfaces.rio_Symbol_To_Send(
            instanse->ext_Interfaces.rio_Hooks_Context,
            symbol_Buffer,
            buffer_Size
        );
    

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Character_Column_To_Send
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int pl_Character_Column_To_Send(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
)
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

   /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL) 
        return RIO_ERROR;

    /* invoke environments callback */
    if (((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Character_Column_To_Send != NULL)
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Character_Column_To_Send(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Hooks_Context,
            character
        );
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Character_Column_Received
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int pl_Character_Column_Received(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
)
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL) 
        return RIO_ERROR;

    /* invoke environments callback */
    if (((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Character_Column_Received != NULL)
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Character_Column_Received(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Hooks_Context,
            character
        );
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Codegroup_Column_To_Send
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int pl_Codegroup_Column_To_Send(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
)
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL) 
        return RIO_ERROR;

    /* invoke environments callback */
    if (((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Codegroup_Column_To_Send != NULL)
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Codegroup_Column_To_Send(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Hooks_Context,
            codegroup
        );
   
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Codegroup_Received
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int pl_Codegroup_Received(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
)
{
    RIO_PL_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (instanse == NULL) 
        return RIO_ERROR;

    /* invoke environments callback */
    if (((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Codegroup_Received != NULL)
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Codegroup_Received(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Serial_Hooks_Context,
            codegroup
        );
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Delete_Instanse
 *
 * Description: deletes instanse of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Delete_Instanse(
    RIO_HANDLE_T          handle
    )
{ 
    RIO_PL_DS_T *pl_Instanse = (RIO_HANDLE_T)handle;

    /* check that the instanse was created as serial one */
    if ( pl_Instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check pointers */
    if (pl_Instanse == NULL) 
        return RIO_ERROR;

    ((RIO_PCS_PMA_MODEL_FTRAY_T*)(pl_Instanse->link_Interface))->delete_Instance(pl_Instanse->link_Instanse);

    if (pl_Instanse->inbound_Buf.rec_Queue != NULL)
        free(pl_Instanse->inbound_Buf.rec_Queue);

    if (pl_Instanse->inbound_Buf.buffer != NULL)
        free(pl_Instanse->inbound_Buf.buffer);

    if (pl_Instanse->outbound_Buf.send_Queue != NULL)
        free(pl_Instanse->outbound_Buf.send_Queue);

    if (pl_Instanse->outbound_Buf.buffer != NULL)
        free(pl_Instanse->outbound_Buf.buffer);

    if (pl_Instanse->reg_Set.lpep_Regs != NULL)
        free(pl_Instanse->reg_Set.lpep_Regs);

    if (pl_Instanse != NULL)
        free(pl_Instanse);

    return RIO_OK; 

}


/***************************************************************************
  Static functions and macros which are used in this file only
***************************************************************************/


/***************************************************************************
 * Function : pl_Create_Bind_Link
 *
 * Description: instantiate 32-Bit Transmitter/Receiver into physical layer
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Create_Bind_Link(RIO_PL_DS_T *handle)
{
    unsigned int                           flag = 0; /* flag for loop*/
    RIO_PCS_PMA_MODEL_CALLBACK_TRAY_T      link_Ctray;
    RIO_PL_PCS_PMA_ADAPTER_CALLBACK_TRAY_T mid_Block_Ctray;

    /* check that the instanse was created as serial one */
    if ( handle->is_Parallel_Model )
        return RIO_ERROR;

    /* 
     * create an instanse, export its function tray and bind it to
     * physical layer
     * if an error happens - memory will be deallocated and 
     * an error code will be returned
     */
    if (RIO_PCS_PMA_Model_Create_Instance(&handle->link_Instanse) == RIO_ERROR ||
        handle->link_Instanse == NULL)
        return RIO_ERROR;

    if (RIO_PL_Pcs_Pma_Adapter_Create_Instance(&handle->mid_Block_Instanse) == RIO_ERROR ||
        handle->mid_Block_Instanse == NULL)
        return RIO_ERROR;

    do
    {

        /*get function trays from PCS/PMA and PCS/PMA Adapter*/
        if (RIO_PCS_PMA_Model_Get_Function_Tray(
            (RIO_PCS_PMA_MODEL_FTRAY_T*)handle->link_Interface) == RIO_ERROR)
            break;

        if (RIO_PL_Pcs_Pma_Adapter_Get_Function_Tray(
            (RIO_PL_PCS_PMA_ADAPTER_FTRAY_T*)handle->mid_Block_Interface) == RIO_ERROR)
            break;

        /* fill internal function tray */
        handle->internal_Ftray.rio_Link_Tx_Disabling = NULL;
        handle->internal_Ftray.rio_Link_Rx_Disabling = NULL;

        /*fill ctray for PCS/PMA*/
        link_Ctray.rio_Set_State_Flag = RIO_PL_Set_State_Flag;
        link_Ctray.rio_Get_State_Flag = RIO_PL_Get_State_Flag;
        link_Ctray.rio_Regs_Context   = (RIO_CONTEXT_T)handle;

        link_Ctray.rio_Serial_Set_Pins         = pl_Set_Pins;
        link_Ctray.lp_Serial_Interface_Context = (RIO_CONTEXT_T)handle;
        
        link_Ctray.get_Granule           = pl_Granule_To_Send;
        link_Ctray.put_Granule           = pl_Granule_Received;
	link_Ctray.rio_Symbol_To_Send = pl_Symbol_To_Send;   /*bug-132*/

        /*only mecessary for BC!!*/
        link_Ctray.put_Corrupted_Granule = NULL;

        link_Ctray.granule_Level_Context = (RIO_CONTEXT_T)handle;

        link_Ctray.put_Character_Column    = pl_Character_Column_Received;
        link_Ctray.get_Character_Column    = pl_Character_Column_To_Send;
        link_Ctray.character_Level_Context = (RIO_CONTEXT_T)handle;

        link_Ctray.put_Codegroup           = pl_Codegroup_Received;
        link_Ctray.get_Codegroup_Column    = pl_Codegroup_Column_To_Send;
        link_Ctray.codegroup_Level_Context = (RIO_CONTEXT_T)handle;

        link_Ctray.rio_User_Msg     = (RIO_PCS_PMA_MSG)RIO_PL_Msg;
        link_Ctray.user_Msg_Context = (RIO_CONTEXT_T)handle;

        /*fill ctray for PL PCS/PMA adpater*/
        mid_Block_Ctray.rio_Pl_Pcs_Pma_Adapter_Get_Granule     = RIO_PL_Get_Granule;
        mid_Block_Ctray.rio_Pl_Pcs_Pma_Adapter_Put_Granule     = RIO_PL_Put_Granule;
        mid_Block_Ctray.rio_PL_Interface_Context = (RIO_CONTEXT_T)handle;

        mid_Block_Ctray.rio_Set_State_Flag = RIO_PL_Set_State_Flag;
        mid_Block_Ctray.rio_Get_State_Flag = RIO_PL_Get_State_Flag;
        mid_Block_Ctray.rio_Regs_Context   = (RIO_CONTEXT_T)handle;
        mid_Block_Ctray.rio_Symbol_To_Send = pl_Symbol_To_Send;
        mid_Block_Ctray.rio_Hooks_Context = (RIO_CONTEXT_T)handle;

        if (RIO_PCS_PMA_Model_Bind_Instance(
                handle->link_Instanse, 
                &link_Ctray) == RIO_ERROR)
                break;
            
        if (RIO_PL_Pcs_Pma_Adapter_Bind_Instance(
                handle->mid_Block_Instanse, 
                &mid_Block_Ctray) == RIO_ERROR)
                break;

        return RIO_OK;

    }while (flag);

    /* creating and binding wasn't successful */
    free(handle->link_Instanse);
    free(handle->mid_Block_Instanse);

    return RIO_ERROR;
}

/***************************************************************************
 * Function : pl_Initialize_Instanse
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Initialize_Instanse(
    RIO_PL_DS_T *instanse
    )
{
    /*
     * check handle
     */
    if (instanse == NULL)
        return RIO_ERROR;
    
    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    /*
     * initialize components of the instanse
     */
    if (RIO_PL_Init_Regs(instanse) == RIO_ERROR)
        return RIO_ERROR;

    RIO_PL_Rx_Init(instanse);
    RIO_PL_Tx_Init(instanse);
    RIO_PL_Out_Init(instanse, &instanse->parameters_Set);
    RIO_PL_In_Init(instanse, &instanse->parameters_Set);

    instanse->initialized = RIO_FALSE;


    return RIO_OK;
}

/***************************************************************************
 * Function : check_Inst_Param_Set
 *
 * Description: check is instantiation physical layer parameters set is valid
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Inst_Param_Set(RIO_MODEL_INST_PARAM_T *param_Set)
{
    /*unsigned long temp_1;*/ /* temporary */


    if (param_Set == NULL || 
        IS_NOT_A_BOOL(param_Set->mbox1) ||
        IS_NOT_A_BOOL(param_Set->mbox2) ||
        IS_NOT_A_BOOL(param_Set->mbox3) ||
        IS_NOT_A_BOOL(param_Set->mbox4) ||
        IS_NOT_A_BOOL(param_Set->has_Memory) ||
        IS_NOT_A_BOOL(param_Set->has_Processor) ||
        IS_NOT_A_BOOL(param_Set->has_Doorbell) ||
        IS_NOT_A_BOOL(param_Set->is_Bridge))
        return RIO_ERROR;


        /*check extended features ptr for lieing in the extended features space with its block */
        if ((param_Set->entry_Extended_Features_Ptr < RIO_EF_SPACE_BOTTOM) ||
                (param_Set->entry_Extended_Features_Ptr > 
                ((RIO_EF_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD) - 
                (RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP))) ||
                (param_Set->entry_Extended_Features_Ptr % RIO_PL_BYTE_CNT_IN_DWORD))
                return RIO_ERROR;

        /*check next extended features ptr for lieing in the extended features space,
        but not in the first extended features block*/
        if (((param_Set->next_Extended_Features_Ptr != 0) && (param_Set->next_Extended_Features_Ptr < RIO_EF_SPACE_BOTTOM)) ||
                (param_Set->next_Extended_Features_Ptr > 
                ((RIO_EF_SPACE_TOP + RIO_PL_BYTE_CNT_IN_DWORD) - 
                (RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP))) ||
                (param_Set->next_Extended_Features_Ptr % RIO_PL_BYTE_CNT_IN_DWORD) ||
                ((param_Set->next_Extended_Features_Ptr >= param_Set->entry_Extended_Features_Ptr) && 
                (param_Set->next_Extended_Features_Ptr < (param_Set->entry_Extended_Features_Ptr + 
                RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * RIO_PL_COUNT_OF_LPEP))))
                return RIO_ERROR;

    /* check sizes of inbound and outbound buffers*/
    if ((!param_Set->pl_Outbound_Buffer_Size) ||
	(param_Set->pl_Outbound_Buffer_Size > RIO_PL_MAX_BUFFER_SIZE) || 
        (!param_Set->pl_Inbound_Buffer_Size) || 
	(param_Set->pl_Inbound_Buffer_Size > RIO_PL_MAX_BUFFER_SIZE))
        return RIO_ERROR;

    /*if (param_Set->ext_Address_Support > 7)*/
    if (param_Set->ext_Address_Support != 0x01 &&
        param_Set->ext_Address_Support != 0x03 &&
        param_Set->ext_Address_Support != 0x05 &&
        param_Set->ext_Address_Support != 0x07)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Init_Param_Set
 *
 * Description: check is initialization physical layer 
 *              parameters set is valid
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Init_Param_Set(
    RIO_PL_DS_T               *instanse, 
    RIO_PL_SERIAL_PARAM_SET_T *param_Set
    )
{
    if (param_Set == NULL || instanse == NULL)
        return RIO_ERROR;
        
    /* check that the instanse was created as serial one */
    if ( instanse->is_Parallel_Model )
        return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->pass_By_Prio))
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_4, (unsigned long)param_Set->pass_By_Prio);
        return RIO_ERROR;
    }
    if (IS_NOT_A_BOOL(param_Set->ext_Address) ||
        IS_NOT_A_BOOL(param_Set->ext_Address_16)/* ||
        IS_NOT_A_BOOL(param_Set->xamsbs)*/)
        return RIO_ERROR;

/*    if (param_Set->transp_Type != RIO_PL_TRANSP_32 &&
        param_Set->transp_Type != RIO_PL_TRANSP_16)
        return RIO_ERROR;*/

/*    if (param_Set->ext_Address == RIO_FALSE &&
        param_Set->ext_Address_16 == RIO_TRUE)
        return RIO_ERROR;
*/
    if (IS_NOT_A_BOOL(param_Set->input_Is_Enable))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->is_Discovered))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->is_Host))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->is_Master_Enable))
                return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->output_Is_Enable))
                return RIO_ERROR;
    if (IS_NOT_A_BOOL(param_Set->enable_LRQ_Resending))
        return RIO_ERROR;

    /* 
     * check correspondense between supported address  - the constant in case 
     * labels mean specification defined encoding for supported address range.
     * they are used anywhere else. Checking supported addresses with initialization parameters 
     * is checking. Change constant here in case of specification changing
     */
    switch (instanse->inst_Param_Set.ext_Address_Support)
    {
        /* 34 bit address only */
        case 1:
            if (/*param_Set->xamsbs == RIO_FALSE ||*/
                param_Set->ext_Address == RIO_TRUE)
                return RIO_ERROR;
            break;

        /* 50 and 34 */
        case 3:
            if (/*param_Set->xamsbs == RIO_FALSE ||*/
                (param_Set->ext_Address == RIO_TRUE && param_Set->ext_Address_16 == RIO_FALSE))
                return RIO_ERROR;
            break;

        /* 66 and 34 */
        case 5:
            if (/*param_Set->xamsbs == RIO_FALSE ||*/
                (param_Set->ext_Address == RIO_TRUE && param_Set->ext_Address_16 == RIO_TRUE))
                return RIO_ERROR;
            break;

        /* 66, 50 and 34 */
        case 7:
/*            if (param_Set->xamsbs == RIO_FALSE)
                return RIO_ERROR;
*/
            break;

        default:
            return RIO_ERROR;
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : count_Time_Outs
 *
 * Description: count time after sendimg for instanses parts
 *              
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void count_Time_Outs(RIO_PL_DS_T *handle)
{

     if (handle == NULL)
        return;

    /* check that the instanse was created as serial one */
    if ( handle->is_Parallel_Model )
       {
        return;
       }

  
    /*GDA: ack delay time-out proceding */	
      ack_Delay_Time_Out(handle);  /*GDA: Description=> Supporting Random Delay feature */

    /*GDA: multicast delay time-out proceding */	
      multicast_Delay_Time_Out(handle);  /*GDA: Description=> Supporting Random Delay feature */

    /*GDA: Rnd PNACC Generator*/
      rio_Enable_RND_Pnacc_Generator(handle);

    /*GDA: Rnd RETRY Generator*/
      rio_Enable_RND_Retry_Generator(handle);


    /* 
     * link request time-out proceding
     */
    RIO_PL_Tx_Link_Req_Time_Out(handle);


    /*sending packet time-out proceding */
    RIO_PL_Tx_Phys_Time_Out(handle);


    /* logical layer time-out*/
    /*RIO_PL_Out_Log_Time_Out(handle);*/
	if (handle->is_PLLight_Model == RIO_FALSE)
		RIO_PL_Out_Log_Time_Out_Errata_14_0503(handle);
 /* GDA: Bug#138 - Response Delay time out function call */
    response_Delay_Time_Out(handle, NULL);

	
    return;
}

/****************************************************************************/

