/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrx/rio_32_txrx_model.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  32-Bit Transmitter/Receiver source
*
* Notes:
*
******************************************************************************/

#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>      
#include <stdarg.h>


#include "rio_32_txrx_model.h"
#define RIO_32TXRX_BIT_IN_BYTE    8

/*
 * Global variable for counting number of 32-Bit Transmitter/Receiver 
 * instantiations. Using for proper instance number assignement
 */
static unsigned long rio_Ph_Tx_Rx_Cnt = 0;

/*
 * data structure intended for storing 32-Bit Transmitter/Receiver 
 * internal state
 */
typedef struct {
    unsigned int   clock_Event_Count;
    unsigned int   data_Counter;
    RIO_GRANULE_T  current_Granule;
    RIO_BYTE_T     prev_FRAME;
    RIO_BYTE_T     cur_FRAME;
    RIO_BOOL_T     is_Train;
    unsigned int   train_Cnt;
    RIO_BOOL_T       port_Disabled;    
} RIO_PH_DIR_DATA_T;

/*
 * data type, which represent possible errors and corresponding
 * error and warning messages.
 * This one describe warning/error entry, which contains enough 
 * information for forming an error or warning message.
 * string is a format string for sprintf function and count_Of_Add_Args
 * is a number of additional parameters, which fully describe error
 * context. Additional are all other parameters after instance number
 */
typedef struct {
    char *string;
    int count_Of_Add_Args;
} LINK_ERROR_WARNING_ENTRY_T;

typedef enum {
    LINK_ERROR_1 = 0,
    LINK_ERROR_2,
    LINK_ERROR_3,
    LINK_ERROR_4,
    LINK_ERROR_5,
    LINK_ERROR_6,
    LINK_ERROR_7,
    LINK_ERROR_8,
    LINK_ERROR_9,
    LINK_ERROR_10,
    LINK_ERROR_11,
    LINK_ERROR_12,
    LINK_ERROR_13,
    LINK_ERROR_14,
    LINK_ERROR_15,
    LINK_ERROR_16,
    LINK_ERROR_17,
    LINK_ERROR_18,
    LINK_ERROR_19,
    LINK_ERROR_20,
    LINK_ERROR_21,

    LINK_WARNING_1,
    LINK_WARNING_2,
    LINK_WARNING_3,
    LINK_WARNING_4,
    LINK_WARNING_5,
    LINK_WARNING_6,
    LINK_WARNING_7,
    LINK_WARNING_8,
    LINK_WARNING_9,
    LINK_WARNING_10,
    LINK_WARNING_11,
    LINK_WARNING_12

} LINK_MESSAGES_T;

/*
 * these errors and warning entries are used to form competed error/warning
 * message strings, which are passed up to the physical layer or to the 
 * switch model. There additional context is added to them
 */
static LINK_ERROR_WARNING_ENTRY_T link_Message_Strings[] = {
{"(the 32-Bit TxRx, instance %lu).Attempt to initialize in work mode.\n", 0},
{"(the 32-Bit TxRx, instance %lu).Attempt to initialize parameter Lp_Ep_Is_8 using wrong value %lu.\n", 1},
{"(the 32-Bit TxRx, instance %lu).Attempt to initialize parameter single_Data_Rate using wrong value %lu.\n", 1},
{"(the 32-Bit TxRx, instance %lu).Attempt to get new granule failed.\n", 0},
{"(the 32-Bit TxRx, instance %lu).Attempt to put new granule failed.\n", 0},
{"(the 32-Bit TxRx, instance %lu).Attempt to set 8/16 LP-EP new values failed.\n", 0},
{"(the 32-Bit TxRx, instance %lu).Attempt to initialize parameter check_SECDED using wrong value %lu.\n", 1},
{"(the 32-Bit TxRx, instance %lu).Attempt to initialize parameter correct_Using_SECDED using wrong value %lu.\n", 1},
{"(the 32-Bit TxRx, instance %lu).Corrupted control symbol received (%lu, %lu, %lu, %lu).\n", 4},
{"(the 32-Bit TxRx, instance %lu).Received PACKET_RETRY acknowledge symbol with reserved buf_Status has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received PACKET_NOT_ACCEPTED acknowledge with reserved buf_Status has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received LINK_REQUEST control symbol with reserved cmd field has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received LINK_RESPONSE control symbol with reserved link_status" \
    " field has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received corrupted TRAINING pattern (%lu, %lu, %lu, %lu).\n", 4},
{"(the 32-Bit TxRx, instance %lu).Received unknown granule has been dropped (%lu, %lu, %lu, %lu).\n", 4},
{"(the 32-Bit TxRx, instance %lu).Received THROTTLE control symbol with reserved idles has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received packet control with reserved sub_type has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).In received LINK_RESPONSE control symbol"\
    " ack_ID_status(%lu) does not match link_Status(%lu).\n", 2},
{"(the 32-Bit TxRx, instance %lu).Attempt to initialize parameter work_Mode_Is_8 using wrong value %lu.\n", 1},
{"(the 32-Bit TxRx, instance %lu).Attempt to initialize parameter work_Mode_Is_8"
    " for 16-bit while parameter Lp_Ep_Is is for 8-bit %lu.\n", 0},
{"(the 32-Bit TxRx, instance %lu).S-bit parity error was detected in granule (%lu, %lu, %lu, %lu).\n", 4},

{"(the 32-Bit TxRx, instanse %lu).New 32-Bit boundary is detected, received part of granule has been dropped.\n", 0},
{"(the 32-Bit TxRx, instance %lu).Correctable error has been detected in granule (%lu, %lu, %lu, %lu).\n", 4},
{"(the 32-Bit TxRx, instance %lu).Granule has been corrected to (%lu, %lu, %lu, %lu).\n", 4},
{"(the 32-Bit TxRx, instance %lu).Received PACKET_RETRY acknowledge symbol with" \
" reserved buf_Status has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received PACKET_NOT_ACCEPTED acknowledge symbol"\
    " with reserved buf_Status has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received LINK_REQUEST control symbol with reserved cmd  has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received LINK_RESPONSE control symbol with reserved link_status has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received corrupted TRAINING pattern  has been dropped (%lu, %lu, %lu, %lu).\n", 4},
{"(the 32-Bit TxRx, instance %lu).Received unknown granule has been dropped (%lu, %lu, %lu, %lu).\n", 4},
{"(the 32-Bit TxRx, instance %lu).Received THROTTLE control symbol with reserved idles has been dropped (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).Received packet control with reserved sub_type (%lu).\n", 1},
{"(the 32-Bit TxRx, instance %lu).In received LINK_RESPONSE control symbol ack_ID_status(%lu)"\
    " does not match link_Status (%lu).\n", 2}

};

static const int link_Message_Strings_Cnt = sizeof(link_Message_Strings)/sizeof(link_Message_Strings[0]);

static RIO_PL_MSG_TYPE_T link_Message_To_Type_Map[] = {
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_WARNING_MSG,

    RIO_PL_MSG_NOTE_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_WARNING_MSG
};

static const int link_Messsage_Map_Count = sizeof(link_Message_To_Type_Map) / sizeof(link_Message_To_Type_Map[0]);


/*
 * static function prototypes
 */
typedef struct {
    unsigned int                   instance_Number;
    RIO_LINK_PARAM_SET_T           param_Set;
    RIO_BOOL_T                     in_Reset;
    RIO_PH_DIR_DATA_T              tx_Data;
    RIO_PH_DIR_DATA_T              rx_Data;
    RIO_LINK_MODEL_CALLBACK_TRAY_T interfaces;
    unsigned int                   first_Toggle;
    RIO_BOOL_T                     input_Present;
} RIO_PH_LNK_T;


static int link_Start_Reset(RIO_HANDLE_T handle);

static int tx_Clock(RIO_HANDLE_T handle);

static int rx_Get_Pins(RIO_HANDLE_T handle,
    RIO_BYTE_T frame,    
    RIO_BYTE_T rd,       
    RIO_BYTE_T rdl,      
    RIO_BYTE_T rclk      
    );

static int link_Initialize(
    RIO_HANDLE_T           handle,     
    RIO_LINK_PARAM_SET_T   *param_Set  
    );

static int link_Rx_Disabling(
    RIO_HANDLE_T           handle,
    RIO_BOOL_T             rx_Disable
    );

static int link_Tx_Disabling(
    RIO_HANDLE_T           handle,
    RIO_BOOL_T             tx_Disable
    );

static int dir_Initialize(RIO_PH_DIR_DATA_T* handle);

static int tx_Send_Next_Data(RIO_PH_LNK_T* handle);

static int rx_Accept_New_Data(RIO_PH_LNK_T* handle, RIO_BYTE_T rd, RIO_BYTE_T rdl);

static int detect_Granule_Type(RIO_PH_LNK_T *handle, unsigned int frameToggle);

static int get_Granule(RIO_PH_LNK_T* handle);

static int put_Granule(RIO_PH_LNK_T* handle);

static int print_Message(RIO_PH_LNK_T* handle, LINK_MESSAGES_T warning_Type, ...);

static int set_Pins(
    RIO_PH_LNK_T* handle, 
    RIO_BYTE_T tframe, 
    RIO_BYTE_T td, 
    RIO_BYTE_T tdl, 
    RIO_BYTE_T tclk);

static void convert_To_Struct(
    RIO_UCHAR_T    array[], 
    RIO_PL_16_DATA_T *struct_Ptr, 
    RIO_BOOL_T     complain);

/*static RIO_UCHAR_T compute_SECDED(RIO_UCHAR_T covered);

static int check_Correct_SECDED(RIO_PH_LNK_T *handle);

static int detect_Sort_Of_Err(RIO_UCHAR_T xor_Sindroms);

static void correct_Part(
    RIO_PL_16_DATA_T *struct_Ptr, 
    RIO_UCHAR_T    sindrom);
*/
static void convert_From_Struct(
    RIO_UCHAR_T array[],  
    RIO_PL_16_DATA_T *struct_Ptr, 
    RIO_BOOL_T complain);
static void check_Packet_Control(RIO_PH_LNK_T *handle, RIO_GRANULE_T *granule);

static int link_Delete_Instanse(
    RIO_HANDLE_T          handle
    );



/***************************************************************************
 * Function : IS_NOT_A_BOOL
 *
 * Description: detect if a variable is RIO_BOOL_T type
 *
 * Returns: bool
 *
 * Notes: none
 *
 **************************************************************************/
#define IS_NOT_A_BOOL(bool_Value) ((bool_Value) != RIO_TRUE && (bool_Value) != RIO_FALSE)

/*
#pragma check_stack(on)

*/

/***************************************************************************
 * Function : RIO_Link_Model_Create_Instance
 *
 * Description: Create new Tx/Rx instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_Link_Model_Create_Instance(RIO_HANDLE_T *handle)
{
    RIO_PH_LNK_T *txRxHandle = NULL;
    
    *handle = NULL; /* clear context */

    if (handle == NULL) /* check pointer is NULL */
        return RIO_ERROR;
    
    /*
     * allocate memory for data staructure and check if 
     * allocation was successful
     */
    if ((txRxHandle = malloc(sizeof(RIO_PH_LNK_T))) == NULL)
        return RIO_ERROR;
    /*
     * clear all pointers
     */
    txRxHandle->interfaces.error_Warning_Context = NULL;
    txRxHandle->interfaces.lpep_Interface_Context = NULL;
    txRxHandle->interfaces.palpdl_Interface_Context = NULL;
/*    txRxHandle->interfaces.rio_Error_Msg = NULL; */
    txRxHandle->interfaces.rio_User_Msg = NULL;
    txRxHandle->interfaces.rio_Link_Get_Granule = NULL;
    txRxHandle->interfaces.rio_Link_Put_Granule = NULL;
    txRxHandle->interfaces.rio_Ph_Set_Pins = NULL;
/*    txRxHandle->interfaces.rio_Warning_Msg = NULL; */
    txRxHandle->interfaces.rio_Hooks_Context = NULL;
    txRxHandle->interfaces.rio_Granule_Received = NULL;
    txRxHandle->interfaces.rio_Symbol_To_Send = NULL;
    txRxHandle->interfaces.rio_Link_Input_Present = NULL;

    /*
     * increase count of TxRx instances and set
     * up the number of instance, turn link into reset
     */
    *handle = txRxHandle;
    txRxHandle->instance_Number = ++rio_Ph_Tx_Rx_Cnt;
    txRxHandle->in_Reset = RIO_TRUE; 

    return RIO_OK;

}

/***************************************************************************
 * Function : RIO_Link_Model_Get_Function_Tray
 *
 * Description: Export function tray, set pointers to corresponding 
 *              functions entry point
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Link_Model_Get_Function_Tray(
    RIO_LINK_MODEL_FTRAY_T *ftray             
    )
{
    /*
     * check pointers to not NULL values and complete function tray
     * with valid entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->rio_Link_Clock_Edge = tx_Clock;
    ftray->rio_Link_Get_Pins = rx_Get_Pins;
    ftray->rio_Link_Initialize = link_Initialize;
    ftray->rio_Link_Start_Reset = link_Start_Reset;
    ftray->rio_Link_Tx_Disabling = link_Tx_Disabling;
    ftray->rio_Link_Rx_Disabling = link_Rx_Disabling;
    ftray->rio_Link_Delete_Instanse = link_Delete_Instanse;


    
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_Link_Model_Bind_Instance
 *
 * Description: bind the instance with the environment, load entry point from
 *              callback tray to internal structure
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Link_Model_Bind_Instance(
    RIO_HANDLE_T                     handle,
    RIO_LINK_MODEL_CALLBACK_TRAY_T   *ctray   
    )
{
    /* 
     * convert void pointer to pointer to instance structure and 
     * check all callbacks pointers to be not NULL and 
     * store external callbacks with environment's context
     * from callback tray in instance's data structure
     */
    RIO_PH_LNK_T* tx_Rx_Handle = (RIO_PH_LNK_T*)handle;

    if (ctray == NULL || 
        handle == NULL ||
        ctray->rio_User_Msg == NULL ||
        ctray->rio_Link_Get_Granule == NULL ||
        ctray->rio_Link_Put_Granule == NULL ||
        ctray->rio_Ph_Set_Pins == NULL 
        )
        return RIO_ERROR;
    
    tx_Rx_Handle->interfaces = *ctray;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : get_Granule
 *
 * Description: stab to invoke rio_Link_Get_Granule callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int get_Granule(RIO_PH_LNK_T* handle)
{
    /* the result of attemption to get new data granule for heading out*/
    int get_Granule_Result;

    /*
     * check all pointers to be not NULL and invoke callback with
     * actual parameters
     */
    if (handle == NULL || 
        handle->interfaces.rio_Link_Get_Granule == NULL)
        return RIO_ERROR;

    get_Granule_Result = (handle->interfaces.rio_Link_Get_Granule(
        handle->interfaces.palpdl_Interface_Context, 
        (RIO_HANDLE_T)handle, &handle->tx_Data.current_Granule));

    /*
     * after obtaining new granule in structure for heading out
     * we shall make corresponding data stream
     */
    if (get_Granule_Result == RIO_ERROR)
        print_Message(handle, LINK_ERROR_4);
    /*add type training simbol*/
    else if (handle->tx_Data.current_Granule.granule_Type != RIO_GR_DATA &&
            handle->tx_Data.current_Granule.granule_Type != RIO_GR_NEW_PACKET &&
            handle->tx_Data.current_Granule.granule_Type != RIO_GR_TRAINING)
        {
            /* form covered bits and compute SECDED */
            /*RIO_UCHAR_T covered;

            covered = (RIO_UCHAR_T)(handle->tx_Data.current_Granule.granule_Struct.ack_ID & 0x07);
            covered <<= 3;
            covered |= (RIO_UCHAR_T)(handle->tx_Data.current_Granule.granule_Struct.stype & 0x07);*/


            if (handle->param_Set.is_TxRx_Model == RIO_FALSE)
                handle->tx_Data.current_Granule.granule_Struct.secded = 0;/*compute_SECDED(covered);*/


            /* make correct granule */
            convert_From_Struct(
                handle->tx_Data.current_Granule.granule_Date, 
                &handle->tx_Data.current_Granule.granule_Struct, RIO_FALSE);

            convert_From_Struct(
                handle->tx_Data.current_Granule.granule_Date + 2, 
                &handle->tx_Data.current_Granule.granule_Struct, RIO_TRUE);
        }
    else if (handle->tx_Data.current_Granule.granule_Type == RIO_GR_TRAINING)
        {

            handle->tx_Data.is_Train = RIO_TRUE;
            handle->tx_Data.train_Cnt = 1;    
            /*(RIO_BYTE_T)0xff;  set 0th to ff */
            handle->tx_Data.current_Granule.granule_Date[0] = 
                (RIO_BYTE_T)((handle->param_Set.training_Pattern[0] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 3)) & 0xff);
            /*(RIO_BYTE_T)0xff;  set 1th to ff */
            handle->tx_Data.current_Granule.granule_Date[1] = 
                (RIO_BYTE_T)((handle->param_Set.training_Pattern[0] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 2)) & 0xff);
            /*(RIO_BYTE_T)0xff;  set 2th to ff */
            handle->tx_Data.current_Granule.granule_Date[2] =
                (RIO_BYTE_T)((handle->param_Set.training_Pattern[0] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 1)) & 0xff);
            /*(RIO_BYTE_T)0xff;  set 3th to ff */
            handle->tx_Data.current_Granule.granule_Date[3] = 
                (RIO_BYTE_T)((handle->param_Set.training_Pattern[0] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 0)) & 0xff);
        }
  
    if (handle->tx_Data.current_Granule.granule_Type != RIO_GR_DATA &&
        handle->tx_Data.current_Granule.granule_Type != RIO_GR_NEW_PACKET)
    {
        if (handle->interfaces.rio_Symbol_To_Send != NULL)
        handle->interfaces.rio_Symbol_To_Send(
            handle->interfaces.rio_Hooks_Context,
            handle->tx_Data.current_Granule.granule_Date,
            RIO_PL_CNT_BYTE_IN_GRAN
            );
    }

    return get_Granule_Result;
}

/***************************************************************************
 * Function : put_Granule
 *
 * Description: stab to invoke rio_Link_Put_Granule callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int put_Granule(RIO_PH_LNK_T* handle)
{
    /* the result of putting received granule */
    int put_Granule_Result;

    /*
     * check all pointers to be not NULL and when invoke callback
     * with actual parameters
     */
    if (handle == NULL ||
        handle->interfaces.rio_Link_Put_Granule == NULL)
        return RIO_ERROR;

    put_Granule_Result = (handle->interfaces.rio_Link_Put_Granule(handle->interfaces.palpdl_Interface_Context,
        (RIO_HANDLE_T)handle, &handle->rx_Data.current_Granule));

    if (put_Granule_Result == RIO_ERROR)
        print_Message(handle, LINK_ERROR_5);

    return put_Granule_Result;
}

/***************************************************************************
 * Function : print_Message
 *
 * Description: detect error message by error code and send it to callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int print_Message(RIO_PH_LNK_T* handle, LINK_MESSAGES_T message_Type, ...)
{
    /* buffer to store error message maximum size 255 bytes */
    char message_Buffer[255];

    /* 
     * additional information which shall present in error message
     * is passing through additional (optional) parameters:
     * add_Args[0]...[4]. 
     * i - loop counter
     */
    unsigned long add_Args[4] = {0, 0, 0, 0};
    int i;

    /* this is necessary to detect additional parameters */
    va_list pointer_To_Next_Arg;

    /*
     * check all using pointers to be not NULL, check
     * that type of error is within corresponding array
     * and when invoke callback with actual parameters.
     * actual error message obtain using error type as an index
     */
    if (handle == NULL || 
        handle->interfaces.rio_User_Msg == NULL ||
        (int)message_Type < 0 || 
        (int)message_Type > (link_Message_Strings_Cnt - 1) ||
        (int)message_Type > (link_Messsage_Map_Count - 1))
        return RIO_ERROR;

    /* get additional parameters*/
    va_start(pointer_To_Next_Arg, message_Type);

    for (i = 1; i <= link_Message_Strings[(int)message_Type].count_Of_Add_Args; i++)
    {
        add_Args[i - 1] = va_arg(pointer_To_Next_Arg, unsigned long);     
    }

    va_end(pointer_To_Next_Arg);

    /*
     * obtain actual error message - switch labels are count of arguments 
     * for example, 4 means 4 additional arguments 
     */
    switch (link_Message_Strings[(int)message_Type].count_Of_Add_Args)
    {
        case 0:
            sprintf(message_Buffer, link_Message_Strings[(int)message_Type].string,
                handle->instance_Number);
            break;

        case 1:
            sprintf(message_Buffer, link_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0]);
            break;

        case 2:
            sprintf(message_Buffer, link_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1]);
            break;

        case 3:
            sprintf(message_Buffer, link_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1], add_Args[2]);
            break;

        case 4:
            sprintf(message_Buffer, link_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1], add_Args[2], add_Args[3]);
            break;

        default:
            return RIO_ERROR;
        
    }

    /* print result error message*/
    return (handle->interfaces.rio_User_Msg(handle->interfaces.error_Warning_Context, 
        link_Message_To_Type_Map[(int)message_Type],
        message_Buffer));
    
}


/***************************************************************************
 * Function : set_Pins
 *
 * Description: stab to safly invoke rio_Ph_Set_Pins callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int set_Pins(
    RIO_PH_LNK_T* handle, 
    RIO_BYTE_T tframe, 
    RIO_BYTE_T td, 
    RIO_BYTE_T tdl, 
    RIO_BYTE_T tclk)
{
    /* the result of setting LP/EP pins to new values*/
    int set_Pins_Result;

    /*
     * check that using pointers is not NULL and invoke 
     * the callback
     */
    if (handle == NULL ||
        handle->interfaces.rio_Ph_Set_Pins == NULL)
        return RIO_ERROR;

    set_Pins_Result = (handle->interfaces.rio_Ph_Set_Pins(
        handle->interfaces.lpep_Interface_Context,
        tframe,
        td,
        tdl,
        tclk));

    if (set_Pins_Result == RIO_ERROR)
        print_Message(handle, LINK_ERROR_6);

    return set_Pins_Result;
}

/***************************************************************************
 * Function : tx_Clock
 *
 * Description: transmit clock
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int tx_Clock(RIO_HANDLE_T handle)
{
    RIO_PH_LNK_T* txRxHandle = (RIO_PH_LNK_T*)handle;

    if (txRxHandle == NULL)
        return RIO_ERROR;

    /* during reset mode all clock events are ignored */
    if (txRxHandle->in_Reset == RIO_TRUE)
        return RIO_OK;

    /*tx pins disabling*/

    if (txRxHandle->tx_Data.port_Disabled == RIO_TRUE)
        return RIO_OK;

    /* 
     * detect if frame has to be toggled  and store 
     * new value for previous frame signal when set current 
     * value of frame signal (actual for current granule)
     */
    if (txRxHandle->tx_Data.clock_Event_Count == 0)
    {
        /*check if txrx32 is connected to TxRx Model. If not, set frame automatically*/
/*        if (txRxHandle->param_Set.is_TxRx_Model == RIO_FALSE)
        {
            if (txRxHandle->tx_Data.current_Granule.granule_Type == RIO_GR_DATA ||
                (txRxHandle->tx_Data.current_Granule.granule_Type == RIO_GR_TRAINING &&
                (txRxHandle->tx_Data.train_Cnt !=1 && txRxHandle->tx_Data.train_Cnt !=3)))
                txRxHandle->tx_Data.cur_FRAME = 
                    (RIO_BYTE_T)(txRxHandle->tx_Data.prev_FRAME ? '\01' : '\0');
            else
                txRxHandle->tx_Data.cur_FRAME = 
                    (RIO_BYTE_T)(txRxHandle->tx_Data.prev_FRAME ? '\0' : '\01');
        }
        else
        {
            txRxHandle->tx_Data.cur_FRAME =
                (RIO_BYTE_T)(txRxHandle->tx_Data.current_Granule.granule_Frame == RIO_TRUE ? '\01' : '\0');
        }
*/ 
        if (txRxHandle->param_Set.is_TxRx_Model == RIO_FALSE)
        {
            if (txRxHandle->tx_Data.current_Granule.granule_Type == RIO_GR_DATA ||
                (txRxHandle->tx_Data.current_Granule.granule_Type == RIO_GR_TRAINING &&
                (txRxHandle->tx_Data.train_Cnt != 1 && txRxHandle->tx_Data.train_Cnt != 3)))
                txRxHandle->tx_Data.cur_FRAME = 
                    (RIO_BYTE_T)(txRxHandle->tx_Data.prev_FRAME ? '\01' : '\0');
            else
                txRxHandle->tx_Data.cur_FRAME = 
                    (RIO_BYTE_T)(txRxHandle->tx_Data.prev_FRAME ? '\0' : '\01');
        }
        else
        {
            if (txRxHandle->tx_Data.current_Granule.granule_Frame == RIO_FALSE ||
                (txRxHandle->tx_Data.current_Granule.granule_Type == RIO_GR_TRAINING &&
                (txRxHandle->tx_Data.train_Cnt != 1 && txRxHandle->tx_Data.train_Cnt != 3)))
                    txRxHandle->tx_Data.cur_FRAME = 
                    (RIO_BYTE_T)(txRxHandle->tx_Data.prev_FRAME ? '\01' : '\0');
            else
                txRxHandle->tx_Data.cur_FRAME = 
                    (RIO_BYTE_T)(txRxHandle->tx_Data.prev_FRAME ? '\0' : '\01');
        }

    }

    
    /*
     *  frame has to be toggled if it's the first event of the granule
     *   after reset or reinitialization, so set it to 3 (unused value )
     * and first granule should be link request/send training
    */

    if (txRxHandle->tx_Data.clock_Event_Count == 1 &&
        txRxHandle->tx_Data.prev_FRAME == 3)
    {
        if (get_Granule(txRxHandle) == RIO_ERROR)
            return RIO_ERROR;
        /* set new pins' values  */
        tx_Send_Next_Data(txRxHandle);

        txRxHandle->tx_Data.prev_FRAME = txRxHandle->tx_Data.cur_FRAME;
        txRxHandle->tx_Data.clock_Event_Count = 0;
        txRxHandle->tx_Data.data_Counter = 0;
      /*  txRxHandle->tx_Data.is_Train = RIO_FALSE;*/
        /*txRxHandle->tx_Data.is_Train = RIO_TRUE;*/
        return RIO_OK;
    }
    else
    {
        /* set new pins' values  */
        tx_Send_Next_Data(txRxHandle);
    }

    /* 
     *  check that heading out granule's just been sent
     *  in such case set transmitter parameters to starting values 
     *  and request new granule (it happens on 4, 3, and 7 cycle 
     */
    if (txRxHandle->tx_Data.data_Counter == 4 &&
        (txRxHandle->param_Set.single_Data_Rate == RIO_FALSE ||
        txRxHandle->tx_Data.clock_Event_Count == 7 ||
        txRxHandle->tx_Data.clock_Event_Count == 3))
    {
        unsigned int need_Get = 1; /* flag if we need obtain new granule */

        txRxHandle->tx_Data.prev_FRAME = txRxHandle->tx_Data.cur_FRAME;
        txRxHandle->tx_Data.clock_Event_Count = 0;  
        txRxHandle->tx_Data.data_Counter = 0;

        /* check if we send TRAINING control */
        if (txRxHandle->tx_Data.is_Train == RIO_TRUE)
        {
            if (txRxHandle->param_Set.work_Mode_Is_8 == RIO_TRUE)
                txRxHandle->tx_Data.train_Cnt += 2;
            else
                txRxHandle->tx_Data.train_Cnt++;

            txRxHandle->tx_Data.current_Granule.granule_Date[0] = 
            /*(RIO_BYTE_T)0xff;  set 0th to ff */
                (RIO_BYTE_T)((txRxHandle->param_Set.training_Pattern[txRxHandle->tx_Data.train_Cnt - 1 ] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 3)) & 0xff);
            /*(RIO_BYTE_T)0xff;  set 1th to ff */
            txRxHandle->tx_Data.current_Granule.granule_Date[1] = 
                (RIO_BYTE_T)((txRxHandle->param_Set.training_Pattern[txRxHandle->tx_Data.train_Cnt - 1] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 2)) & 0xff);
            /*(RIO_BYTE_T)0xff;  set 2th to ff */
            txRxHandle->tx_Data.current_Granule.granule_Date[2] =
                (RIO_BYTE_T)((txRxHandle->param_Set.training_Pattern[txRxHandle->tx_Data.train_Cnt - 1] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 1)) & 0xff);
            /*(RIO_BYTE_T)0xff;  set 3th to ff */
            txRxHandle->tx_Data.current_Granule.granule_Date[3] = 
                (RIO_BYTE_T)((txRxHandle->param_Set.training_Pattern[txRxHandle->tx_Data.train_Cnt - 1] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 0)) & 0xff);

            if (txRxHandle->interfaces.rio_Symbol_To_Send != NULL &&
                /*following condition is necessary for preventing 
                double invocation of sym to send callback*/
                txRxHandle->tx_Data.train_Cnt <= 4 )
                    txRxHandle->interfaces.rio_Symbol_To_Send(
                        txRxHandle->interfaces.rio_Hooks_Context,
                        txRxHandle->tx_Data.current_Granule.granule_Date,
                        RIO_PL_CNT_BYTE_IN_GRAN
                        );
  
            if (txRxHandle->tx_Data.train_Cnt == 1 || txRxHandle->tx_Data.train_Cnt == 2 || txRxHandle->tx_Data.train_Cnt == 3 || 
                txRxHandle->tx_Data.train_Cnt == 4)
                    need_Get = 0;

             /* the third part of training */
                         
            if (need_Get)
            {
                txRxHandle->tx_Data.is_Train = RIO_FALSE;
                txRxHandle->tx_Data.train_Cnt = 0;
            }
        }

        if (need_Get)
        {
            if (get_Granule(txRxHandle) == RIO_ERROR)
                return RIO_ERROR;
             
#ifdef _DEBUG
                /* prints bytes the 0th to the 3th */
                if (txRxHandle->instance_Number == 1)
                printf("Transmitter%lu: new granule (%X, %X, %X, %X)  gran type %d\n",
                txRxHandle->instance_Number,
                txRxHandle->tx_Data.current_Granule.granule_Date[0],
                txRxHandle->tx_Data.current_Granule.granule_Date[1],
                txRxHandle->tx_Data.current_Granule.granule_Date[2],
                txRxHandle->tx_Data.current_Granule.granule_Date[3],
                txRxHandle->tx_Data.current_Granule.granule_Type);
#endif
        }
        return RIO_OK;
    }
     
    txRxHandle->tx_Data.clock_Event_Count++;

    return RIO_OK;
}

/***************************************************************************
 * Function : rx_Get_Pins
 *
 * Description: receiver clock and pins values accepting
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int rx_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_BYTE_T frame,    /* receive frame */
    RIO_BYTE_T rd,       /* receive data */
    RIO_BYTE_T rdl,      /* receive data low part */
    RIO_BYTE_T rclk      /* receive clock */
    )
{
    (void)rclk;
    RIO_PH_LNK_T* txRxHandle = (RIO_PH_LNK_T*)handle;

    if (txRxHandle == NULL)
        return RIO_ERROR;

    if (txRxHandle->in_Reset == RIO_TRUE)
        return RIO_OK;
    if (txRxHandle->input_Present == RIO_FALSE)
    {
        txRxHandle->input_Present = RIO_TRUE;
        if (txRxHandle->interfaces.rio_Link_Input_Present != NULL)
            txRxHandle->interfaces.rio_Link_Input_Present(
            txRxHandle->interfaces.rio_Hooks_Context, 
            (RIO_HANDLE_T)txRxHandle, txRxHandle->input_Present);

    }

    if (txRxHandle->rx_Data.port_Disabled == RIO_TRUE)
        return RIO_OK;


#ifdef _DEBUG
    if (txRxHandle->instance_Number == 10)
    printf("Receiver%d:    RFRAME:%d, RCLK:%d, RD:%d, RDL:%d, 8 bit:%d, single:%d\n",
        txRxHandle->instance_Number,
        frame,
        rclk,
        rd,
        rdl,
        txRxHandle->param_Set.lp_Ep_Is_8,
        txRxHandle->param_Set.single_Data_Rate);
#endif

    frame = (RIO_BYTE_T)(frame ? '\01' : '\0');

    /*
     * frame toggling detection in this case (frame's toggled)
     * clear accepting data, clock event number  and set new 
     * current frame 
     */
    if (frame != txRxHandle->rx_Data.cur_FRAME) /* toggle */
    {
        txRxHandle->rx_Data.prev_FRAME = txRxHandle->rx_Data.cur_FRAME;
        txRxHandle->rx_Data.cur_FRAME = frame;

        /*
         * if frame's been toggled at non 32-bit boundary set new boundary
         * and clear received part of granule
         */
        if (txRxHandle->rx_Data.clock_Event_Count != 0)
        {
            txRxHandle->rx_Data.clock_Event_Count = 0;
            txRxHandle->rx_Data.data_Counter = 0;
            
            if (txRxHandle->first_Toggle != 0)
            {
                print_Message(txRxHandle, LINK_WARNING_1);
                txRxHandle->rx_Data.current_Granule.granule_Type = RIO_GR_HAS_BEEN_CORRUPTED;
                txRxHandle->rx_Data.current_Granule.granule_Frame = RIO_TRUE;
                put_Granule(txRxHandle);
            }
            txRxHandle->first_Toggle = 1; 
        }
    }
        
    /*
     * accept new data on negedge always and on posedge if double data rate
     * parameters is set to yes, increase clock counter
     */
    if ((txRxHandle->rx_Data.clock_Event_Count & 1) ||
        txRxHandle->param_Set.single_Data_Rate == RIO_FALSE)
        rx_Accept_New_Data(txRxHandle, rd, rdl);

    txRxHandle->rx_Data.clock_Event_Count++;

    /*
     * if all granule's been received detect its type,
     * store frame in previous frame, clear data number and clock
     * events count and pass data to PAL/PDL
     */
    if (txRxHandle->rx_Data.data_Counter == 4)
    {
        /* hold if frame has been toggled */
        unsigned int frame_Toggle = txRxHandle->rx_Data.prev_FRAME != 
            txRxHandle->rx_Data.cur_FRAME;

        txRxHandle->rx_Data.data_Counter = 0;
        txRxHandle->rx_Data.clock_Event_Count = 0;
        txRxHandle->rx_Data.prev_FRAME = txRxHandle->rx_Data.cur_FRAME;

#ifdef _DEBUG
        /* print bytes from the 0th to the 3th */
        if (txRxHandle->instance_Number == 10)
        printf("Receiver%d: granule received (%d, %d, %d, %d)\n",
            txRxHandle->instance_Number,
            txRxHandle->rx_Data.current_Granule.granule_Date[0],
            txRxHandle->rx_Data.current_Granule.granule_Date[1],
            txRxHandle->rx_Data.current_Granule.granule_Date[2],
            txRxHandle->rx_Data.current_Granule.granule_Date[3]
            );
#endif
        
        /*pass frame toggling*/
        txRxHandle->rx_Data.current_Granule.granule_Frame = (RIO_BOOL_T)frame_Toggle;
        /* detect granule type */
        if (!frame_Toggle && txRxHandle->rx_Data.is_Train == RIO_FALSE)
        {
            txRxHandle->rx_Data.current_Granule.granule_Type = RIO_GR_DATA;
            put_Granule(txRxHandle);
        }
        else 
        {
            detect_Granule_Type(txRxHandle, frame_Toggle);
            if (txRxHandle->rx_Data.is_Train == RIO_FALSE)
            {
                if (txRxHandle->interfaces.rio_Granule_Received != NULL)
                {
                    RIO_GRANULE_STRUCT_T hook_Granule;
                    hook_Granule.granule_Type = txRxHandle->rx_Data.current_Granule.granule_Type;
                    hook_Granule.ack_ID = txRxHandle->rx_Data.current_Granule.granule_Struct.ack_ID;
                    hook_Granule.buf_Status = txRxHandle->rx_Data.current_Granule.granule_Struct.buf_Status;
                    hook_Granule.stype =  txRxHandle->rx_Data.current_Granule.granule_Struct.stype;

                    txRxHandle->interfaces.rio_Granule_Received(
                        txRxHandle->interfaces.rio_Hooks_Context,
                        &hook_Granule);
                }

 /*               if (txRxHandle->rx_Data.current_Granule.granule_Type != RIO_GR_UNRECOGNIZED)*/
                    put_Granule(txRxHandle);

            }
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : link_Initialize
 *
 * Description: initialize link and turn it to work mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int link_Initialize(
    RIO_HANDLE_T           handle,     /* handle of instance*/
    RIO_LINK_PARAM_SET_T   *param_Set  /* initialization parameters */
    )
{
    unsigned int i;
    RIO_PH_LNK_T *txRxHandle = (RIO_PH_LNK_T*)handle;    

    /*
     * check handle isn't NULL, check parameter set is valid,
     * check that the instance is in reset mode,
     * set link parameters to necessary values and turn the
     * instance to work mode.
     */
    if (handle == NULL || param_Set == NULL)
        return RIO_ERROR;

    if (txRxHandle->in_Reset == RIO_FALSE)
    {
        print_Message(txRxHandle, LINK_ERROR_1);
        return RIO_ERROR;
    }

    if (IS_NOT_A_BOOL(param_Set->lp_Ep_Is_8))
    {
        print_Message(txRxHandle, LINK_ERROR_2, (unsigned long)param_Set->lp_Ep_Is_8);
        return RIO_ERROR;

    }

    if (IS_NOT_A_BOOL(param_Set->work_Mode_Is_8))
    {
        print_Message(txRxHandle, LINK_ERROR_19, (unsigned long)param_Set->work_Mode_Is_8);
        return RIO_ERROR;

    }

    if (IS_NOT_A_BOOL(param_Set->single_Data_Rate))
    {
        print_Message(txRxHandle, LINK_ERROR_3, (unsigned long)param_Set->single_Data_Rate);
        return RIO_ERROR;
    }

/*    if (IS_NOT_A_BOOL(param_Set->check_SECDED))
    {
        print_Message(txRxHandle, LINK_ERROR_7, (unsigned long)param_Set->check_SECDED);
        return RIO_ERROR;
    }
    
    if (IS_NOT_A_BOOL(param_Set->correct_Using_SECDED))
    {
        print_Message(txRxHandle, LINK_ERROR_8, (unsigned long)param_Set->correct_Using_SECDED);
        return RIO_ERROR;
    }
*/

    if (param_Set->lp_Ep_Is_8 == RIO_TRUE && param_Set->work_Mode_Is_8 == RIO_FALSE)
    {
        print_Message(txRxHandle, LINK_ERROR_20);
        return RIO_ERROR;
    }

    if (IS_NOT_A_BOOL(param_Set->is_TxRx_Model))
    {
        /*add new msg type!*/
        /* print_Message(txRxHandle, LINK_ERROR_3, (unsigned long)param_Set->single_Data_Rate);*/
        return RIO_ERROR;
    }

    txRxHandle->param_Set = *param_Set;

    for (i = 0; i < RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE; i++)
    {
        txRxHandle->param_Set.training_Pattern[i] = param_Set->training_Pattern[i];
    }

    if (param_Set->work_Mode_Is_8 == RIO_TRUE)
    {
        txRxHandle->param_Set.training_Pattern[2] = 
            txRxHandle->param_Set.training_Pattern[1];
    }


    txRxHandle->in_Reset = RIO_FALSE;
    txRxHandle->first_Toggle = 0;
    txRxHandle->input_Present = RIO_FALSE;
    dir_Initialize(&txRxHandle->rx_Data);
    dir_Initialize(&txRxHandle->tx_Data);
    
    return RIO_OK;
}

/***************************************************************************
 * Function : link_Start_Reset
 *
 * Description: turn link to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int link_Start_Reset(RIO_HANDLE_T handle)
{
    RIO_PH_LNK_T *txRxHandle = (RIO_PH_LNK_T*)handle;    
    
    /*
     * check handle isn't NULL, check if the instance is in 
     * reset mode and turn the instance to reset mode
     */

    if (handle == NULL)
        return RIO_ERROR;

    if(txRxHandle->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    txRxHandle->in_Reset = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : dir_Initialize
 *
 * Description: set transmitter or receiver instance data to 
 *              initial station
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int dir_Initialize(RIO_PH_DIR_DATA_T* handle)
{
    /*
     * check pointer isn't NULL, set count of clock events to 0,
     * count of received/trensmitted data to 0, current frame to 0,
     * previous frame to not initialized state and initialize
     * current data granule to LINK REQUEST/SEND TRAINING control
     */
    if (handle == NULL)
        return RIO_ERROR;
    
    handle->clock_Event_Count = 1;
    handle->data_Counter = 0;
    handle->cur_FRAME = 0;
    handle->prev_FRAME = 3; /* set 3 to be toggled */
    handle->is_Train = RIO_FALSE;
    handle->train_Cnt = 0;
    handle->port_Disabled = RIO_FALSE;

    handle->current_Granule.granule_Frame = RIO_TRUE;
    return RIO_OK;
}

/***************************************************************************
 * Function : tx_Send_Next_Data
 *
 * Description: drive transmitter pins based on bus width and clock
 *              rate
 * Returns: error code
 *
 * Notes: increase data counter
 *
 **************************************************************************/
static int tx_Send_Next_Data(RIO_PH_LNK_T* handle)
{
    RIO_BYTE_T tclk;

    if (handle == NULL)
        return RIO_ERROR;

    /* detect ptoper clk pin value it can be 0,2,4, and 6*/
    if (handle->tx_Data.clock_Event_Count == 0 ||
        handle->tx_Data.clock_Event_Count == 2 ||
        handle->tx_Data.clock_Event_Count == 4 ||
        handle->tx_Data.clock_Event_Count == 6)
        tclk = (RIO_BYTE_T)'\01';
    else
        tclk = (RIO_BYTE_T)'\0';

    /* 
     * drive pins based on bus width parameter
     * and select new data in double rate or 
     * in single rate and negedge
     */
if (handle->param_Set.lp_Ep_Is_8 == RIO_FALSE)
    {
        if (handle->param_Set.work_Mode_Is_8 == RIO_FALSE)
        {
#ifdef _DEBUG
            if (handle->instance_Number == 1)
            printf("Transmitter%d: TFRAME:%d, TCLK:%d, TD:%X, TDL:%X, 8 bit:%X, 8 bit work:%X, single:%X\n",
                handle->instance_Number,
                handle->tx_Data.cur_FRAME,
                tclk,
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter],
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter + 1],
                handle->param_Set.lp_Ep_Is_8,
                handle->param_Set.work_Mode_Is_8,
                handle->param_Set.single_Data_Rate);
#endif

            set_Pins(handle, 
                handle->tx_Data.cur_FRAME, 
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter], 
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter + 1], 
                tclk);
        
            if (handle->param_Set.single_Data_Rate == RIO_FALSE ||
                (handle->param_Set.single_Data_Rate == RIO_TRUE && 
                tclk == 0x00))
                handle->tx_Data.data_Counter += 2; /* add 2 */
        }
        else
        {
#ifdef _DEBUG
            if (handle->instance_Number == 1)
            printf("Transmitter%d: TFRAME:%d, TCLK:%d, TD:%X, TDL:%X, 8 bit:%X, 8 bit work:%X, single:%d\n",
                handle->instance_Number,
                handle->tx_Data.cur_FRAME,
                tclk,
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter],
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter],
                handle->param_Set.lp_Ep_Is_8,
                handle->param_Set.work_Mode_Is_8,
                handle->param_Set.single_Data_Rate);
#endif

            set_Pins(handle, 
                handle->tx_Data.cur_FRAME, 
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter], 
                handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter], 
                tclk);
        
            if (handle->param_Set.single_Data_Rate == RIO_FALSE ||
                (handle->param_Set.single_Data_Rate == RIO_TRUE && 
                tclk == 0x00))
                handle->tx_Data.data_Counter += 1; /* add 2 */
        }
    }
    else
    {

#ifdef _DEBUG
        if (handle->instance_Number == 1)
        printf("Transmitter%d: TFRAME:%d, TCLK:%d, TD:%X, TDL:%X, 8 bit:%X, 8 bit work:%X, single:%X\n",
            handle->instance_Number,
            handle->tx_Data.cur_FRAME,
            tclk,
            handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter],
            '\0',
            handle->param_Set.lp_Ep_Is_8,
            handle->param_Set.work_Mode_Is_8,
            handle->param_Set.single_Data_Rate);
#endif
        
        set_Pins(handle, 
            handle->tx_Data.cur_FRAME, 
            handle->tx_Data.current_Granule.granule_Date[handle->tx_Data.data_Counter], 
            (RIO_UCHAR_T)0, 
            tclk);

        if ( handle->param_Set.single_Data_Rate == RIO_FALSE || 
            (handle->param_Set.single_Data_Rate == RIO_TRUE && 
            tclk == '\0') )
            handle->tx_Data.data_Counter++;
    }
        
    return RIO_OK;
}

/***************************************************************************
 * Function : rx_Accept_New_Data
 *
 * Description: accept new data received from Rx pins
 *              
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int rx_Accept_New_Data(RIO_PH_LNK_T* handle, RIO_BYTE_T rd, RIO_BYTE_T rdl)
{
    if (handle == NULL)
        return RIO_ERROR;

    handle->rx_Data.current_Granule.granule_Date[handle->rx_Data.data_Counter++] = rd;
    
    if (handle->param_Set.work_Mode_Is_8 == RIO_FALSE) 
        handle->rx_Data.current_Granule.granule_Date[handle->rx_Data.data_Counter++] = rdl;
       
    return RIO_OK;
}

/***************************************************************************
 * Function : convert_From_Struct
 *
 * Description: convert control char structure to two char array 
 *              
 * Returns: void
 *
 * Notes: none
 *
 **************************************************************************/
static void convert_From_Struct(
    RIO_UCHAR_T array[], 
    RIO_PL_16_DATA_T *struct_Ptr, 
    RIO_BOOL_T complain)
{
    if (struct_Ptr == NULL)
        return;

    /* form the first byte */
    array[0] = (RIO_UCHAR_T)((struct_Ptr->s & 0x01) << 7);
    array[0] |= (RIO_UCHAR_T)((struct_Ptr->ack_ID & 0x07) << 4);
    array[0] |= (RIO_UCHAR_T)((struct_Ptr->secded & 0x1e) >> 1);
 
    /* form the second byte */
    array[1] = (RIO_UCHAR_T)((struct_Ptr->secded & 0x01) << 7);
    array[1] |= (RIO_UCHAR_T)((struct_Ptr->buf_Status & 0x0f) << 3);
    array[1] |= (RIO_UCHAR_T)(struct_Ptr->stype & 0x07);

    /* complain if necessary */
    if (complain == RIO_TRUE)
    {
        array[0] = (RIO_UCHAR_T)~array[0];
        array[1] = (RIO_UCHAR_T)~array[1];
    }
}

/***************************************************************************
 * Function : convert_To_Struct
 *
 * Description: convert two char array to control structure
 *              
 * Returns: void
 *
 * Notes: none
 *
 **************************************************************************/
static void convert_To_Struct(
    RIO_UCHAR_T array[], 
    RIO_PL_16_DATA_T *struct_Ptr, 
    RIO_BOOL_T complain)
{
    RIO_UCHAR_T elem;

    if (array == NULL)
        return;
    
    /* convert the first byte */
    elem = (RIO_UCHAR_T)(complain == RIO_TRUE ? ~array[0] : array[0]);
    struct_Ptr->s = (elem &  0x80) >> 7;
    struct_Ptr->ack_ID = (elem & 0x70) >> 4;
    struct_Ptr->secded = (elem & 0x0f) << 1;

    /* convert the second byte */
    elem = (RIO_UCHAR_T)(complain == RIO_TRUE ? ~array[1] : array[1]);
    struct_Ptr->secded += (elem & 0x80) >> 7 ;
    struct_Ptr->buf_Status = (elem & 0x78) >> 3;
    struct_Ptr->stype = elem & 0x07;
}
/***************************************************************************
 * Function : detect_Granule_Type
 *
 * Description: detect type of received granule
 *              
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int detect_Granule_Type(RIO_PH_LNK_T *handle, unsigned int frameToggle)
{
    RIO_GRANULE_T *received_Gr = NULL;
    
    if (handle == NULL)
        return RIO_ERROR;

    received_Gr = &handle->rx_Data.current_Granule;

    /*
     * if it's a start of the new packet we needn't do any actions at all
     * check s bit - it's the first one
     */
    if (handle->rx_Data.is_Train == RIO_TRUE)
    {
      /*  handle->rx_Data.train_Cnt++;*/

        /* the second part */
        if (handle->rx_Data.train_Cnt == 2)
        {
/*                if (received_Gr->granule_Date[0] == 0xff &&
                    received_Gr->granule_Date[1] == 0xff &&
                    received_Gr->granule_Date[2] == 0xff &&
                    received_Gr->granule_Date[3] == 0xff &&
                    !(frameToggle)
                    )
*/
                if ((received_Gr->granule_Date[0] == 
                        ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                            (RIO_32TXRX_BIT_IN_BYTE * 3)) & 0xff)) &&
                    (received_Gr->granule_Date[1] == 
                        ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                            (RIO_32TXRX_BIT_IN_BYTE * 2)) & 0xff)) &&
                    (received_Gr->granule_Date[2] == 
                        ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                            (RIO_32TXRX_BIT_IN_BYTE * 1)) & 0xff)) &&
                    (received_Gr->granule_Date[3] == 
                        ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                            (RIO_32TXRX_BIT_IN_BYTE * 0)) & 0xff)) &&
                    !(frameToggle))
                {
                    received_Gr->granule_Type = RIO_GR_TRAINING;
                
                    handle->rx_Data.train_Cnt++;
                    return RIO_OK;
                }
                else
                {
                    handle->rx_Data.is_Train = RIO_FALSE;
                    handle->rx_Data.train_Cnt = 0;
                    received_Gr->granule_Type = RIO_GR_CORRUPTED_TRAINING;
                    return RIO_OK;
                }
        }
            

        /* the third part */
        if (handle->rx_Data.train_Cnt == 3)
        {
            /* shall all be 0 */
/*            if (received_Gr->granule_Date[0] == 0x00 &&
                received_Gr->granule_Date[1] == 0x00 &&
                received_Gr->granule_Date[2] == 0x00 &&
                received_Gr->granule_Date[3] == 0x00 &&
                frameToggle
                )
*/

            if ((received_Gr->granule_Date[0] == ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 3)) & 0xff)) &&
                (received_Gr->granule_Date[1] == ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 2)) & 0xff)) &&
                (received_Gr->granule_Date[2] == ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 1)) & 0xff)) &&
                (received_Gr->granule_Date[3] == ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                    (RIO_32TXRX_BIT_IN_BYTE * 0)) & 0xff)) &&
                (frameToggle)
                )
            {
                received_Gr->granule_Type = RIO_GR_TRAINING;
                if (handle->param_Set.work_Mode_Is_8 == RIO_TRUE)
                {
                    handle->rx_Data.is_Train = RIO_FALSE;
                    handle->rx_Data.train_Cnt = 0;
                }
                handle->rx_Data.train_Cnt++;
                return RIO_OK;
            }
            else
            {
                handle->rx_Data.is_Train = RIO_FALSE;
                handle->rx_Data.train_Cnt = 0;
                received_Gr->granule_Type = RIO_GR_CORRUPTED_TRAINING;
                return RIO_OK;
            }
        }

        /* the forth part */
        if (handle->rx_Data.train_Cnt == 4)
        {
/*            if (received_Gr->granule_Date[0] == 0x00 &&
                received_Gr->granule_Date[1] == 0x00 &&
                received_Gr->granule_Date[2] == 0x00 &&
                received_Gr->granule_Date[3] == 0x00 &&
                !(frameToggle)
                )
*/
            if ((received_Gr->granule_Date[0] == 
                    ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                        (RIO_32TXRX_BIT_IN_BYTE * 3)) & 0xff)) &&
                (received_Gr->granule_Date[1] == 
                    ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                        (RIO_32TXRX_BIT_IN_BYTE * 2)) & 0xff)) &&
                (received_Gr->granule_Date[2] == 
                    ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                        (RIO_32TXRX_BIT_IN_BYTE * 1)) & 0xff)) &&
                (received_Gr->granule_Date[3] == 
                    ((handle->param_Set.training_Pattern[handle->rx_Data.train_Cnt - 1] >> 
                        (RIO_32TXRX_BIT_IN_BYTE * 0)) & 0xff)) &&
                !(frameToggle)
                )

            {
                received_Gr->granule_Type = RIO_GR_TRAINING;
                handle->rx_Data.is_Train = RIO_FALSE;
                handle->rx_Data.train_Cnt = 0;
                return RIO_OK;
            }
            else
            {
                handle->rx_Data.is_Train = RIO_FALSE;
                handle->rx_Data.train_Cnt = 0;
                received_Gr->granule_Type = RIO_GR_CORRUPTED_TRAINING;
                return RIO_OK;
            }
        }
    }

    /* clear TRAINING count and flag */
    handle->rx_Data.is_Train = RIO_FALSE;
    handle->rx_Data.train_Cnt = 0;

    /* detect the most significant bit */
    if ((received_Gr->granule_Date[0] & 0x80) == 0)
    {
        if ((received_Gr->granule_Date[0] & 0x04) != 0x04)
        {
            print_Message(handle, LINK_ERROR_21, 
            (unsigned long) received_Gr->granule_Date[0],
            (unsigned long) received_Gr->granule_Date[1],
            (unsigned long) received_Gr->granule_Date[2],
            (unsigned long) received_Gr->granule_Date[3]
            );
            received_Gr->granule_Type = RIO_GR_S_BIT_CORRUPTED;
            return RIO_OK;
        }
        received_Gr->granule_Type = RIO_GR_NEW_PACKET;
        received_Gr->granule_Struct.s = 0;

        /* detect ackID bits - shift them 4 bits to the right */
        received_Gr->granule_Struct.ack_ID = 
            (received_Gr->granule_Date[0] & (RIO_UCHAR_T)0x70) >> 4;

        /* in start of packet prio field will be stored in buf_Status 
         * we shall shift if 6 bits to the right 
         */
        received_Gr->granule_Struct.buf_Status =
            (received_Gr->granule_Date[1] & (RIO_UCHAR_T)0xc0) >> 6;
        return RIO_OK;
    }

    /* check if it's TRAINING - all bytes are ff*/
/*    if (received_Gr->granule_Date[0] == 0xff &&
        received_Gr->granule_Date[1] == 0xff &&
        received_Gr->granule_Date[2] == 0xff &&
        received_Gr->granule_Date[3] == 0xff &&
        (frameToggle)
        )
*/
    if ((received_Gr->granule_Date[0] == 
            ((handle->param_Set.training_Pattern[0] >> 
                (RIO_32TXRX_BIT_IN_BYTE * 3)) & 0xff)) &&
        (received_Gr->granule_Date[1] == 
            ((handle->param_Set.training_Pattern[0] >> 
                (RIO_32TXRX_BIT_IN_BYTE * 2)) & 0xff)) &&
        (received_Gr->granule_Date[2] == 
            ((handle->param_Set.training_Pattern[0] >> 
                (RIO_32TXRX_BIT_IN_BYTE * 1)) & 0xff)) &&
        (received_Gr->granule_Date[3] == 
            ((handle->param_Set.training_Pattern[0] >> 
                (RIO_32TXRX_BIT_IN_BYTE * 0)) & 0xff)) &&
        (frameToggle)
        )

    {
        handle->rx_Data.is_Train = RIO_TRUE;
        if (handle->param_Set.work_Mode_Is_8 == RIO_TRUE)
            handle->rx_Data.train_Cnt = 3;
        else
            handle->rx_Data.train_Cnt = 2;
        
        received_Gr->granule_Type = RIO_GR_TRAINING;
        return RIO_OK;
    }

    /*
     * we shall check SECDED code and possiblly correct single error
     * if corresponding settings are set
     */
/*    if (handle->param_Set.check_SECDED == RIO_TRUE)
    {
        if (check_Correct_SECDED(handle) == RIO_ERROR)
        {*/
            /* print four corrupted bytes */
/*            print_Message(handle, LINK_ERROR_9, 
                (unsigned long) received_Gr->granule_Date[0],
                (unsigned long) received_Gr->granule_Date[1],
                (unsigned long) received_Gr->granule_Date[2],
                (unsigned long) received_Gr->granule_Date[3]
                );
            received_Gr->granule_Type = RIO_GR_HAS_BEEN_CORRUPTED;
            return RIO_OK;
        }
    }
    else
    {*/
        /*
         * convert array of chars to structures representated control symbol
         */
/*        convert_To_Struct(
            handle->rx_Data.current_Granule.granule_Date, 
            &handle->rx_Data.current_Granule.granule_Struct, RIO_FALSE);
    }*/

    /*new control struct - s-parity bit checking*/

    if ((received_Gr->granule_Date[0] & 0x04) != 0x0)
    {
        print_Message(handle, LINK_ERROR_21, 
        (unsigned long) received_Gr->granule_Date[0],
        (unsigned long) received_Gr->granule_Date[1],
        (unsigned long) received_Gr->granule_Date[2],
        (unsigned long) received_Gr->granule_Date[3]
        );
        received_Gr->granule_Type = RIO_GR_S_BIT_CORRUPTED;
        return RIO_OK;
    }

    if ((((RIO_UCHAR_T)received_Gr->granule_Date[0]) != (RIO_UCHAR_T)~(received_Gr->granule_Date[2])) ||
        (((RIO_UCHAR_T)received_Gr->granule_Date[1]) != (RIO_UCHAR_T)~(received_Gr->granule_Date[3])))
    {
         /* print four corrupted bytes */
        print_Message(handle, LINK_ERROR_9, 
            (unsigned long) received_Gr->granule_Date[0],
            (unsigned long) received_Gr->granule_Date[1],
            (unsigned long) received_Gr->granule_Date[2],
            (unsigned long) received_Gr->granule_Date[3]
        );
        received_Gr->granule_Type = RIO_GR_HAS_BEEN_CORRUPTED;
        return RIO_OK;
    }

    convert_To_Struct(handle->rx_Data.current_Granule.granule_Date, 
        &handle->rx_Data.current_Granule.granule_Struct, RIO_FALSE);

    /*check symbol for reserved field value */
    if (((handle->rx_Data.current_Granule.granule_Struct.secded & 0x10) != 0) ||
        ((handle->rx_Data.current_Granule.granule_Struct.secded & 0x07) != 0))
    {
        received_Gr->granule_Type = RIO_GR_UNRECOGNIZED;
        return RIO_OK;
    }

    /*
     * now detect granule type based on valid granule structure
     */
    switch (received_Gr->granule_Struct.stype)
    {
        case RIO_GR_PACKET_ACCEPTED_STYPE: 
            
            received_Gr->granule_Type = RIO_GR_PACKET_ACCEPTED;
            break;

        case RIO_GR_PACKET_RETRY_STYPE: 
            
            /* check for reserved value */
            if (received_Gr->granule_Struct.buf_Status == 0x00)
                received_Gr->granule_Type = RIO_GR_PACKET_RETRY;
            else
            {
                print_Message(handle, LINK_WARNING_4, 
                    (unsigned long)received_Gr->granule_Struct.buf_Status);
                received_Gr->granule_Type = RIO_GR_UNRECOGNIZED;
            }
            break;

        case RIO_GR_PACKET_NOT_ACCEPTED_STYPE: 
            /* check if cause field is valid */
            if (received_Gr->granule_Struct.buf_Status == RIO_PL_PNACC_INTERNAL_ERROR ||
                received_Gr->granule_Struct.buf_Status == RIO_PL_PNACC_UNEXPECTED_ACKID ||
                received_Gr->granule_Struct.buf_Status == RIO_PL_PNACC_BAD_CONTROL ||
                received_Gr->granule_Struct.buf_Status == RIO_PL_PNACC_INPUT_STOPPED ||
                received_Gr->granule_Struct.buf_Status == RIO_PL_PNACC_BAD_CRC ||
                received_Gr->granule_Struct.buf_Status == RIO_PL_PNACC_GENERAL_ERROR ||
                received_Gr->granule_Struct.buf_Status == RIO_PL_PNACC_BAD_S_BIT_PARITY
                ) 
                received_Gr->granule_Type = RIO_GR_PACKET_NOT_ACCEPTED;
            else
            {
                print_Message(handle, LINK_WARNING_5,
                    (unsigned long)received_Gr->granule_Struct.buf_Status
                    );
                received_Gr->granule_Type = RIO_GR_UNRECOGNIZED;
            }
            break;

        case RIO_GR_PACKET_CONTROL_STYPE: 
            
            check_Packet_Control(handle, received_Gr);
            
            break;

        case RIO_GR_LINK_REQUEST_STYPE: 

            if (received_Gr->granule_Struct.ack_ID == RIO_PL_LREQ_RESET ||
                received_Gr->granule_Struct.ack_ID == RIO_LP_LREQ_INPUT_STATUS ||
                received_Gr->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING
                )
                received_Gr->granule_Type = RIO_GR_LINK_REQUEST;
            else
            {
                print_Message(handle, LINK_WARNING_6, 
                    (unsigned long)received_Gr->granule_Struct.ack_ID 
                    );
                received_Gr->granule_Type = RIO_GR_LINK_REQUEST;
            }

            break;

        case RIO_GR_LINK_RESPONSE_STYPE: 
            /* check if link status is possible it can have 1 or 6 in buf_status */
            if (received_Gr->granule_Struct.buf_Status != 0x01 &&
                received_Gr->granule_Struct.buf_Status != 0x06
                )
            {
                if (received_Gr->granule_Struct.buf_Status >= RIO_PL_LRESP_OK &&
                    (received_Gr->granule_Struct.ack_ID  != (received_Gr->granule_Struct.buf_Status - RIO_PL_LRESP_OK)))
                {
                    print_Message(handle, LINK_WARNING_12, 
                        (unsigned long)received_Gr->granule_Struct.ack_ID,
                        (unsigned int)received_Gr->granule_Struct.buf_Status
                        );

                    received_Gr->granule_Type = RIO_GR_UNRECOGNIZED;
                }
                else
                    received_Gr->granule_Type = RIO_GR_LINK_RESPONSE;
            }
            else
            {
                print_Message(handle, LINK_WARNING_7, 
                    (unsigned long)received_Gr->granule_Struct.buf_Status 
                    );
                received_Gr->granule_Type = RIO_GR_UNRECOGNIZED;
            }
            break;


        default:
            /* unexpected type print four bytes */
            print_Message(handle, LINK_WARNING_9, 
                    (unsigned long)received_Gr->granule_Date[0],
                    (unsigned long)received_Gr->granule_Date[1],
                    (unsigned long)received_Gr->granule_Date[2],
                    (unsigned long)received_Gr->granule_Date[3]
                    );
            handle->rx_Data.current_Granule.granule_Type = RIO_GR_UNRECOGNIZED;
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Packet_Control
 *
 * Description: detect type of received packet control
 *              
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static void check_Packet_Control(RIO_PH_LNK_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    /* detect type of packet control based on sub_type field */
    switch (granule->granule_Struct.ack_ID)
    {
        case RIO_GR_IDLE_SUB_TYPE: 
            granule->granule_Type = RIO_GR_IDLE;
            break;

        case RIO_GR_STOMP_SUB_TYPE: 
            granule->granule_Type = RIO_GR_STOMP;
            break;

        case RIO_GR_EOP_SUB_TYPE: 
            granule->granule_Type = RIO_GR_EOP;
            break;

        case RIO_GR_RESTART_FROM_RETRY_SUB_TYPE: 
            granule->granule_Type = RIO_GR_RESTART_FROM_RETRY;
            break;

        case RIO_GR_THROTTLE_SUB_TYPE: 
            /* check reserved values of idle field cannot be more than d or
             * less than b
             */
            if (granule->granule_Struct.buf_Status >= 0x0b &&
                granule->granule_Struct.buf_Status <= 0x0d
                )
            {
                print_Message(handle, LINK_WARNING_10, 
                    (unsigned long)granule->granule_Struct.buf_Status
                    );
                granule->granule_Type = RIO_GR_UNRECOGNIZED;
            }
            else
                granule->granule_Type = RIO_GR_THROTTLE;
            break;

        case RIO_GR_TOD_SYNC_SUB_TYPE: 
            granule->granule_Type = RIO_GR_TOD_SYNC;
            break;

        default:
            print_Message(handle, LINK_WARNING_11, 
                    (unsigned long)granule->granule_Struct.ack_ID 
                    );
            granule->granule_Type = RIO_GR_UNRECOGNIZED;

    }
}

/***************************************************************************
 * Function : link_Tx_Disabling
 *
 * Description: turn off tx pins
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int link_Tx_Disabling(RIO_HANDLE_T handle, RIO_BOOL_T tx_Disable)
{
    RIO_PH_LNK_T *txRxHandle = (RIO_PH_LNK_T*)handle;    
    
    /*
     * check handle isn't NULL, check if the instance is in 
     * reset mode and turn the instance to reset mode
     */

    if (handle == NULL)
        return RIO_ERROR;

    txRxHandle->tx_Data.port_Disabled = tx_Disable;

    return RIO_OK;
}

/***************************************************************************
 * Function : link_Rx_Disabling
 *
 * Description: turn off tx pins
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int link_Rx_Disabling(RIO_HANDLE_T handle, RIO_BOOL_T rx_Disable)
{
    RIO_PH_LNK_T *txRxHandle = (RIO_PH_LNK_T*)handle;    
    
    /*
     * check handle isn't NULL, check if the instance is in 
     * reset mode and turn the instance to reset mode
     */

    if (handle == NULL)
        return RIO_ERROR;

    txRxHandle->rx_Data.port_Disabled = rx_Disable;

    return RIO_OK;
}
/***************************************************************************
 * Function : link_Delete_Instance
 *
 * Description: deletes instance of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int link_Delete_Instanse(
    RIO_HANDLE_T          handle
    )
{
    RIO_PH_LNK_T *txRxHandle = (RIO_PH_LNK_T*)handle;    
    
    /*
     * check handle isn't NULL
     */

    if (txRxHandle == NULL)
        return RIO_ERROR;

    free(txRxHandle);

    return RIO_OK;
}
/*****************************************************************************/
