#ifndef RIO_32_TXRX_MODEL_H
#define RIO_32_TXRX_MODEL_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrx/include/rio_32_txrx_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  This file contains 32-Bit Transmitter/Receiver data types,
*                which are external visible 
*  Notes:      
* 
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_PARALLEL_TYPES_H
#include "rio_parallel_types.h"
#endif

#ifndef RIO_32_TXRX_INTERFACE_H
#include "rio_32_txrx_interface.h"
#endif

#ifndef RIO_LP_EP_INTERFACE_H
#include "rio_lp_ep_interface.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif

#ifndef RIO_PARALLEL_COMMON_H
#include "rio_parallel_common.h"
#endif

/* 
 * function tray to be passed to the environment by the 32 TxRx model 
 */
typedef struct {
    RIO_PH_GET_PINS            rio_Link_Get_Pins; 
    RIO_LINK_CLOCK_EDGE        rio_Link_Clock_Edge;
    RIO_LINK_START_RESET    rio_Link_Start_Reset;
    RIO_LINK_INITIALIZE        rio_Link_Initialize;
    RIO_LINK_TX_DISABLING    rio_Link_Tx_Disabling;
    RIO_LINK_RX_DISABLING    rio_Link_Rx_Disabling;
    RIO_DELETE_INSTANCE     rio_Link_Delete_Instanse;
} RIO_LINK_MODEL_FTRAY_T;

/* 
 * callback tray to be used by the link (supplied by the model environment). Also
 * includes context of 8LP/EP interface and context of PAL/PDL interface. These
 * contexts using to run corresponding callbacks in proper environment 
 */
typedef struct {
    RIO_PH_SET_PINS         rio_Ph_Set_Pins;
    RIO_CONTEXT_T           lpep_Interface_Context;    
    RIO_LINK_GET_GRANULE    rio_Link_Get_Granule;
    RIO_LINK_PUT_GRANULE    rio_Link_Put_Granule;
    RIO_CONTEXT_T           palpdl_Interface_Context; 
/*    RIO_ERROR_MSG           rio_Error_Msg;
    RIO_WARNING_MSG         rio_Warning_Msg; */
    RIO_LINK_MSG            rio_User_Msg;
    RIO_CONTEXT_T           error_Warning_Context;
    RIO_LINK_INPUT_PRESENT  rio_Link_Input_Present;
    RIO_GRANULE_RECEIVED    rio_Granule_Received;
    RIO_SYMBOL_TO_SEND      rio_Symbol_To_Send;
    RIO_CONTEXT_T           rio_Hooks_Context;
} RIO_LINK_MODEL_CALLBACK_TRAY_T;

/* 
 * the function creates instance of PL model and returns handle to its 
 * data structure. There are not any create parameters for 32 bit link
 */
int RIO_Link_Model_Create_Instance(
    RIO_HANDLE_T *handle
    );

/* 
 * the function returns link model interface functions to be used for model access.
 * function tray is allocated by invokating side and pointer to tray is passed.
 * link only stores actual entry points there.
 */
int RIO_Link_Model_Get_Function_Tray(
    RIO_LINK_MODEL_FTRAY_T *ftray             
    );

/* 
 * the function binds link model instance to the environment. Callback tray is allocated
 * outside, stored with actual entry points and pointer to it is passed here. Link
 * copies these pointers to its internal data.
 */
int RIO_Link_Model_Bind_Instance(
    RIO_HANDLE_T                     handle,
    RIO_LINK_MODEL_CALLBACK_TRAY_T   *ctray   
    );

#endif /* RIO_32_TXRX_MODEL_H*/

