#ifndef RIO_32_TXRX_INTERFACE_H
#define RIO_32_TXRX_INTERFACE_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrx/include/rio_32_txrx_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    32txrx model interface
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_PARALLEL_TYPES_H
#include "rio_parallel_types.h"
#endif

#ifndef RIO_GRAN_TYPE_H
#include "rio_gran_type.h"
#endif


/* we need separate prototype for message callback for the link model
   because it uses PL or Switch message callbacks to print itself, 
   thus it should pass msg_Type to them
*/

typedef int (*RIO_LINK_MSG) (
    RIO_CONTEXT_T     context,    /* callback context */
    RIO_PL_MSG_TYPE_T msg_Type,
    char              *string
    );

/* 
 * this function is invoked to ask 32 bit Transmitter about clock
 * edge (posedge and negedge). It syncronizes the transmittion using this event.
 */ 
typedef int (*RIO_LINK_CLOCK_EDGE)(
    RIO_HANDLE_T handle      /* instance's handle */
    ); 

/*
 * 32 bit Transmitter invokes this callback if it needs in new data
 * granule for transmitting. It invokes the callback at the same clock event
 * when the last portion of data granule is set (held) on the pins
 */
typedef int (*RIO_LINK_GET_GRANULE)(
    RIO_CONTEXT_T context,   /* callback context */
    RIO_HANDLE_T  handle,    /* instance's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    ); 

/*
 * 32 bit Receiver invokes this callback to pass received data granule to
 * high level logic. It can be PAL/PDL logic in the physical layer or 
 * switch
 */
typedef int (*RIO_LINK_PUT_GRANULE)(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    );

/*
 * 32 bit Receiver invokes this callback to pass received data granule to
 * high level logic. It can be PAL/PDL logic in the physical layer or 
 * switch
 */
typedef int (*RIO_LINK_INPUT_PRESENT)(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_BOOL_T    input_Present/* indicate if txrx input receives clock */
    );

/* this function is used to turn 32 bit Transmitter/Receiver into reset
 * mode. In reset mode instance does not send or accept anything, does not
 * react on clock events. All Tx/Rx internal parameters turn to initial values.
 */
typedef int (*RIO_LINK_START_RESET)(
    RIO_HANDLE_T handle       /* instance's handle */
    );

/*
 * the structure represend set of 32 Bit Transmitter/Receiver parameters:
 * data rate and bus width. All these parameters can be set during initialization,
 * re-initialization. Intitial values (after reset) are single-data rate and 8 bit
 * data bus
 */
typedef struct {
    RIO_BOOL_T    single_Data_Rate;  /* if TRUE single-, if FALSE double-data rate */
    RIO_BOOL_T    lp_Ep_Is_8;        /* if TRUE 8 bit pins, if FALSE 16 bit pins */
    RIO_BOOL_T    work_Mode_Is_8;    /* if TRUE 8 bit data, if FALSE 16 bit data */
    RIO_BOOL_T    check_SECDED;      
    RIO_BOOL_T    correct_Using_SECDED;
    RIO_BOOL_T    is_TxRx_Model;  
    unsigned long  training_Pattern[RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE]; 
        /*in case of invalid users value shall have traditional value*/
} RIO_LINK_PARAM_SET_T;

/* 
 * the function is used to initialize and re-initialize the link (32 bit
 * Transmitter/Receiver. The function invokation turns the instance from 
 * reset to work mode.
 */
typedef int (*RIO_LINK_INITIALIZE)(
    RIO_HANDLE_T           handle,     /* handle of instance*/
    RIO_LINK_PARAM_SET_T   *param_Set  /* initialization parameters */
    );
/* 
 * the function is used to enabling/disabling the link Transmitter.
 * The function invokation set tx mode - 
 * if transmitter drives the pins or is switched off.
 */
typedef int (*RIO_LINK_TX_DISABLING)(
    RIO_HANDLE_T           handle,     /* handle of instance*/
    RIO_BOOL_T               tx_Disable /* parameters */
    );
/* 
 * the function is used to enabling/disabling the link Receiver.
 * The function invokation set rx mode - 
 * if receiver drives the pins or is switched off.
 */
typedef int (*RIO_LINK_RX_DISABLING)(
    RIO_HANDLE_T           handle,     /* handle of instance*/
    RIO_BOOL_T               rx_Disable /* parameters */
    );

#endif /* RIO_32_TXRX_INTERFACE_H */

