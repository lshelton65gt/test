/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\riom\pcs_pma_adapter\rio_pcs_pma_adapter.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Physical layer transmitter
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdio.h>

#include "rio_pcs_pma_adapter.h"
#include "rio_pcs_pma_dp_ds.h"

/*
 * Global variable for counting number of 32-Bit Transmitter/Receiver 
 * instantiations. Using for proper instance number assignement
 */
static unsigned long rio_Pl_Pcs_Pma_Adapter_Inst_Count = 0;

typedef enum {
    RIO_PL_SERIAL_STYPE_0 = 0,
    RIO_PL_SERIAL_STYPE_1,
    RIO_PL_SERIAL_DATA,
    RIO_PL_SERIAL_CATS_NUM
} RIO_PL_PCS_PMA_ADAPTER_GRANULE_CATS_T;


typedef struct {
    RIO_GRANULE_T granule[RIO_PL_SERIAL_CATS_NUM];
    RIO_BOOL_T granule_Flag[RIO_PL_SERIAL_CATS_NUM];
    RIO_BOOL_T packet_In_Progress;
} RIO_PL_PCS_PMA_ADAPTER_TX_DS_T;

typedef struct {
    RIO_BOOL_T    start_Packet_Flag;
} RIO_PL_PCS_PMA_ADAPTER_RX_DS_T;

typedef struct {
    unsigned long                           instance_Number;
    RIO_PL_PCS_PMA_ADAPTER_TX_DS_T          tx_Data;
    RIO_PL_PCS_PMA_ADAPTER_RX_DS_T          rx_Data;
    
    RIO_PL_PCS_PMA_ADAPTER_CALLBACK_TRAY_T   interfaces;

    RIO_PL_PCS_PMA_ADAPTER_INITIALIZE        restart_Function;
    RIO_PL_PCS_PMA_ADAPTER_START_RESET       reset_Function;

    RIO_BOOL_T                              in_Reset;
} RIO_PL_PCS_PMA_ADAPTER_DS_T;


static int pl_Pcs_Pma_Adapter_Get_Granule(
    RIO_CONTEXT_T context,   /* my handle */
    RIO_PCS_PMA_GRANULE_T *granule   /* pointer to store granule for heading out */
    );

static int pl_Pcs_Pma_Adapter_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_PCS_PMA_GRANULE_T *granule    /* pinter to granule data structure */
    );

static int pl_Pcs_Pma_Adapter_Initialize(
    RIO_HANDLE_T         handle
    );

static int pl_Pcs_Pma_Adapter_Start_Reset(
    RIO_HANDLE_T handle
    );

static int pl_Pcs_Pma_Adapter_Delete_Instance(
    RIO_HANDLE_T          handle
    );

static int pl_Pcs_Pma_Adapter_Granule_For_Transmitting(
    RIO_PL_PCS_PMA_ADAPTER_DS_T *handle,
    RIO_PCS_PMA_GRANULE_T *granule
    );

static void pl_Pcs_Pma_Adapter_Granule_Received(
    RIO_PL_PCS_PMA_ADAPTER_DS_T *handle,
    RIO_PCS_PMA_GRANULE_T *granule
    );

static unsigned int check_For_Idle_Nop_Symbol(
    RIO_PCS_PMA_GRANULE_T *granule, 
    RIO_BOOL_T            packet_Flag
    );

static void form_Serial_Granule(
    RIO_PL_PCS_PMA_ADAPTER_DS_T *handle,
    unsigned int gran_Category,
    RIO_PCS_PMA_GRANULE_T *granule
    );

static unsigned int get_Granule_Category(RIO_GRANULE_T *pl_Granule);

static void form_Serial_Symbol_Part(
    RIO_PL_PCS_PMA_ADAPTER_DS_T *handle,
    RIO_BOOL_T is_Stype_0,
    RIO_BOOL_T is_In_Buffer,
    RIO_PCS_PMA_GRANULE_T *granule
    );

static void form_Data_Granule(
    RIO_PL_PCS_PMA_ADAPTER_DS_T *handle,
    RIO_PCS_PMA_GRANULE_T *granule
    );

static unsigned int get_Pl_Granule_Type(RIO_PCS_PMA_GRANULE_T *granule, unsigned int category);



/***************************************************************************
 * Function : RIO_PL_Model_Create_Instance
 *
 * Description: Create new physical layer instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PL_Pcs_Pma_Adapter_Create_Instance(
    RIO_HANDLE_T            *handle
    )
{
    /* handle to structure will allocated*/
    RIO_PL_PCS_PMA_ADAPTER_DS_T *pl_Pcs_Pma_Adapter_Instanse = NULL;

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for physical layer 
     * data structure
     */
    if (handle == NULL )
        return RIO_ERROR;

    /* clear handle */
    *handle = NULL;

    /* allocate instanse */
    if ((pl_Pcs_Pma_Adapter_Instanse = malloc(sizeof(RIO_PL_PCS_PMA_ADAPTER_DS_T))) == NULL)
        return RIO_ERROR;

    /*
     * clear all pointers
     */
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_Get_State_Flag = NULL;
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_Pl_Pcs_Pma_Adapter_Get_Granule = NULL;
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_Pl_Pcs_Pma_Adapter_Put_Granule = NULL;
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_PL_Interface_Context = NULL;
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_Regs_Context = NULL;
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_Set_State_Flag = NULL;
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_Symbol_To_Send = NULL;
    pl_Pcs_Pma_Adapter_Instanse->interfaces.rio_Hooks_Context = NULL; 
        
    /*
     * increase count of TxRx instances and set
     * up the number of instance, turn link into reset
     */
    *handle = pl_Pcs_Pma_Adapter_Instanse;
    pl_Pcs_Pma_Adapter_Instanse->instance_Number = ++rio_Pl_Pcs_Pma_Adapter_Inst_Count;
    pl_Pcs_Pma_Adapter_Instanse->in_Reset = RIO_TRUE; 

    return RIO_OK;

}

/***************************************************************************
 * Function : RIO_PL_Pcs_Pma_Adapter_Get_Function_Tray
 *
 * Description: Export function tray, set pointers to corresponding 
 *              functions entry point
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PL_Pcs_Pma_Adapter_Get_Function_Tray(
    RIO_PL_PCS_PMA_ADAPTER_FTRAY_T *ftray             
    )
{
    /*
     * check pointers to not NULL values and complete function tray
     * with valid entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

 /*   ftray->rio_PL_Pcs_Pma_Adapter_Delete_Instance = ;*/
    ftray->rio_Pl_Pcs_Pma_Adapter_Initialize = pl_Pcs_Pma_Adapter_Initialize;
    ftray->rio_Pl_Pcs_Pma_Adapter_Start_Reset = pl_Pcs_Pma_Adapter_Start_Reset;

    ftray->rio_Pl_Pcs_Pma_Get_Granule = pl_Pcs_Pma_Adapter_Get_Granule;
    ftray->rio_Pl_Pcs_Pma_Put_Granule = pl_Pcs_Pma_Adapter_Put_Granule;

    ftray->rio_Pl_Pcs_Pma_Adapter_Delete_Instance = pl_Pcs_Pma_Adapter_Delete_Instance;

    
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Pcs_Pma_Adapter_Bind_Instance
 *
 * Description: bind the instance with the environment, load entry point from
 *              callback tray to internal structure
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
int RIO_PL_Pcs_Pma_Adapter_Bind_Instance(
    RIO_HANDLE_T                        handle,
    RIO_PL_PCS_PMA_ADAPTER_CALLBACK_TRAY_T   *ctray   
    )
{
    /* 
     * convert void pointer to pointer to instance structure and 
     * check all callbacks pointers to be not NULL and 
     * store external callbacks with environment's context
     * from callback tray in instance's data structure
     */
    RIO_PL_PCS_PMA_ADAPTER_DS_T* instanse = (RIO_PL_PCS_PMA_ADAPTER_DS_T*)handle;

    if (ctray == NULL || 
        handle == NULL ||
/*        ctray->rio_Error_Msg == NULL || */
        ctray->rio_Pl_Pcs_Pma_Adapter_Get_Granule == NULL ||
        ctray->rio_Pl_Pcs_Pma_Adapter_Put_Granule == NULL ||
        ctray->rio_Set_State_Flag == NULL   ||
        ctray->rio_Get_State_Flag == NULL
/*        ctray->rio_Warning_Msg == NULL || */
        )
        return RIO_ERROR;
    
    instanse->interfaces = *ctray;
    
    return RIO_OK;
}
/***************************************************************************
 * Function : rio_PL_Pcs_Pma_Adapter_Delete_Instance
 *
 * Description: deletes instance of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Pcs_Pma_Adapter_Delete_Instance(
    RIO_HANDLE_T          handle
    )
{ 
    RIO_PL_PCS_PMA_ADAPTER_DS_T *pl_Pcs_Pma_Adapter_Instanse = (RIO_HANDLE_T)handle;

    /* check pointers */
    if (pl_Pcs_Pma_Adapter_Instanse == NULL) 
        return RIO_ERROR;

    if (pl_Pcs_Pma_Adapter_Instanse != NULL)
        free(pl_Pcs_Pma_Adapter_Instanse);
   
    return RIO_OK; 

}
/***************************************************************************
 * Function : pl_Get_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pl_Pcs_Pma_Adapter_Get_Granule(
    RIO_CONTEXT_T context,   /* my handle */
    RIO_PCS_PMA_GRANULE_T *granule   /* pointer to store granule for heading out */
    )
{
    RIO_PL_PCS_PMA_ADAPTER_DS_T *instanse = (RIO_PL_PCS_PMA_ADAPTER_DS_T*)context;
    int res;

    /* check pointers and state */
    if (instanse == NULL || granule == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*it's need to be thinked over!!!!! - how to check link handle*/
    /* check if invokation comes form our link */
 /*   if (instanse->link_Instanse != handle)
    {*/
    /*    RIO_PL_Pcs_Pma_Adapter_Print_Message(instanse, RIO_PL_ERROR_6); */
/*        return RIO_ERROR;
    }*/


    res = pl_Pcs_Pma_Adapter_Granule_For_Transmitting(instanse, granule);

    /* instanse->interfaces.rio_Symbol_To_Send(instanse->interfaces.rio_Symbol_To_Send,hkjhk);*/
    if (granule->is_Data != RIO_TRUE && res == RIO_OK)
    {
        RIO_UCHAR_T tmp_Buf[4];

        /*put structure data to the flat buffer to 
        pass this buffer to hook interface*/
        tmp_Buf[0] = 0;
        tmp_Buf[1] = ((granule->granule_Struct.stype0 & 7) << 5) |
                    (granule->granule_Struct.parameter0 & 0x1F);
        tmp_Buf[2] = ((granule->granule_Struct.parameter1 & 0x1F) << 3) |
                    (granule->granule_Struct.stype1 & 7);
        tmp_Buf[3] = (granule->granule_Struct.cmd & 7) << 5;

        if (instanse->interfaces.rio_Symbol_To_Send != NULL)
        instanse->interfaces.rio_Symbol_To_Send(
            instanse->interfaces.rio_Hooks_Context,
            tmp_Buf,
            RIO_PL_CNT_BYTE_IN_GRAN
            );

	/*Bug#102*/
	if((tmp_Buf[3] & 0x1F) != 0)
	granule->granule_Struct.is_Crc_Valid = RIO_TRUE;
	/*Bug#102*/

        /*put data after user change back to structure*/
        granule->granule_Struct.stype0 = tmp_Buf[1] >> 5;
        granule->granule_Struct.parameter0 = tmp_Buf[1] & 0x1F;

        granule->granule_Struct.parameter1 = tmp_Buf[2] >> 3;
        granule->granule_Struct.stype1 = tmp_Buf[2] & 7;

        granule->granule_Struct.cmd = tmp_Buf[3] >> 5;
    }

    return res;
}
/***************************************************************************
 * Function : pl_Put_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pl_Pcs_Pma_Adapter_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_PCS_PMA_GRANULE_T *granule    /* pinter to granule data structure */
    )
{
    /* instanse handler */
    RIO_PL_PCS_PMA_ADAPTER_DS_T *instanse = (RIO_PL_PCS_PMA_ADAPTER_DS_T*)context;

    if (instanse == NULL || granule == NULL)
        return RIO_ERROR;

    /* check if call comes from proper link */
/*    if (instanse->link_Instanse != handle)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_7);
        return RIO_ERROR;
    }*/

    /* pass the granule to receiver */
    pl_Pcs_Pma_Adapter_Granule_Received(instanse, granule);

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Pcs_Pma_Adapter_Init
 *
 * Description: initialize the transmitter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pl_Pcs_Pma_Adapter_Initialize(
    RIO_HANDLE_T         handle
    )
{
    unsigned int i;
    /* instanse handle and link's parameters set */
    RIO_PL_PCS_PMA_ADAPTER_DS_T *instanse = (RIO_PL_PCS_PMA_ADAPTER_DS_T*)handle;
   
    /* check pointers */
    if (instanse == NULL)
        return RIO_ERROR;

    /* check reset state */
    if (instanse->in_Reset == RIO_FALSE)
    {
       /* RIO_PL_Print_Message(instanse, RIO_PL_ERROR_3);*/
        return RIO_ERROR;
    }
    
    /*set off the reset flag*/
    instanse->in_Reset = RIO_FALSE;
    instanse->rx_Data.start_Packet_Flag = RIO_FALSE;
    instanse->interfaces.rio_Set_State_Flag(
        instanse->interfaces.rio_Regs_Context, instanse,
        RIO_RX_IS_PD, instanse->rx_Data.start_Packet_Flag, 0);

    for (i = 0; i < RIO_PL_SERIAL_CATS_NUM; i++)
        instanse->tx_Data.granule_Flag[i] = RIO_FALSE;
    instanse->tx_Data.packet_In_Progress  = RIO_FALSE;
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Pcs_Pma_Adapter_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
static int pl_Pcs_Pma_Adapter_Start_Reset(
    RIO_HANDLE_T handle
    )
{
    RIO_PL_PCS_PMA_ADAPTER_DS_T *instanse = (RIO_PL_PCS_PMA_ADAPTER_DS_T*)handle;

    /* check pointers */
    if (instanse == NULL)
        return RIO_ERROR;

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;
    
    /* modify state to be _in_reset_ */
    instanse->in_Reset = RIO_TRUE;
    instanse->tx_Data.packet_In_Progress = RIO_FALSE;

    return RIO_OK;
}
/***************************************************************************
 * Function : pl_Pcs_Pma_Adapter_Granule_For_Transmitting
 *
 * Description: main transmitter loop
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Pcs_Pma_Adapter_Granule_For_Transmitting(RIO_PL_PCS_PMA_ADAPTER_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule)
{
    unsigned int gran_Category;
    unsigned int i = 0;
    
    RIO_GRANULE_T pl_Granule;

    if (handle == NULL || granule == NULL)
        return RIO_ERROR;

    while (RIO_TRUE)
    {
        /*check if there are any data*/
        if (handle->tx_Data.granule_Flag[RIO_PL_SERIAL_DATA] == RIO_TRUE)
        {
            form_Serial_Granule(handle, RIO_PL_SERIAL_DATA, granule);
            return RIO_OK;
        }
        /*get next granule from pl*/
        handle->interfaces.rio_Pl_Pcs_Pma_Adapter_Get_Granule(handle->interfaces.rio_PL_Interface_Context, handle, &pl_Granule);
        
        /*detect the category of got from pl granule*/
        if ((gran_Category = get_Granule_Category(&pl_Granule)) >= RIO_PL_SERIAL_CATS_NUM)
        {
            /*handle error - report about it*/
            /* ......*/
            return RIO_ERROR;
        }
        /*check if there is the granule of this category in buffer already*/
        if (handle->tx_Data.granule_Flag[gran_Category] == RIO_TRUE)
        {            
            form_Serial_Granule(handle, gran_Category, granule); /*form Serial Granule for transmitting*/
            handle->tx_Data.granule_Flag[gran_Category] = RIO_TRUE;
            handle->tx_Data.granule[gran_Category] = pl_Granule;
            return check_For_Idle_Nop_Symbol(granule, handle->tx_Data.packet_In_Progress );
        }
        
        handle->tx_Data.granule_Flag[gran_Category] = RIO_TRUE;
        handle->tx_Data.granule[gran_Category] = pl_Granule;
         
        /*check the buffer for already contained data which is necessary to send*/
        for (i = 1; i < RIO_PL_SERIAL_CATS_NUM; i++)
            if (handle->tx_Data.granule_Flag[(gran_Category + i) % RIO_PL_SERIAL_CATS_NUM] == RIO_TRUE)
            {
                form_Serial_Granule(handle, (gran_Category + i) % RIO_PL_SERIAL_CATS_NUM, granule);
                return check_For_Idle_Nop_Symbol(granule, handle->tx_Data.packet_In_Progress );
            }
    }    
}

/***************************************************************************
 * Function : check_For_Idle_Nop_Symbol
 *
 * Description: checks for Status_NOP symbol to remove it from stream
 *
 * Returns: the category
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_For_Idle_Nop_Symbol(
    RIO_PCS_PMA_GRANULE_T *granule, 
    RIO_BOOL_T            packet_Flag
)
{
    if (granule == NULL)
        return RIO_ERROR; /*report errors by this way*/

    if (granule->is_Data == RIO_FALSE &&
        granule->granule_Struct.stype0 == RIO_PL_SERIAL_GR_IDLE_STYPE0 &&
        granule->granule_Struct.stype1 == RIO_PL_SERIAL_GR_NOP_STYPE1 &&
        packet_Flag == RIO_FALSE)
        return RIO_NO_DATA;

    return RIO_OK;    
}
/***************************************************************************
 * Function : get_Granule_Category
 *
 * Description: return the granule category - stype0, stype1, or data
 *
 * Returns: the category
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Granule_Category(RIO_GRANULE_T *pl_Granule)
{
    if (pl_Granule == NULL)
        return RIO_PL_SERIAL_CATS_NUM; /*report errors by this way*/

    switch(pl_Granule->granule_Type)
    { 
        case RIO_GR_DATA:
        case RIO_GR_NEW_PACKET:
            return RIO_PL_SERIAL_DATA;
            
        /*stype 0 symbols - need to be complemented with stype 1*/
        case RIO_GR_PACKET_ACCEPTED:
        case RIO_GR_PACKET_RETRY:
        case RIO_GR_PACKET_NOT_ACCEPTED:
        case RIO_GR_IDLE:
        case RIO_GR_LINK_RESPONSE:
            return RIO_PL_SERIAL_STYPE_0;

        /*stype 1 symbols - need to be comlemented with stype 0*/
        case RIO_GR_RESTART_FROM_RETRY:
        case RIO_GR_TOD_SYNC:
        case RIO_GR_EOP:
        case RIO_GR_STOMP:
        case RIO_GR_LINK_REQUEST:
            return RIO_PL_SERIAL_STYPE_1;
        default:
            return RIO_PL_SERIAL_CATS_NUM; /*report errors by this way*/
        }

    return RIO_PL_SERIAL_CATS_NUM; /*report errors by this way*/
}

/***************************************************************************
 * Function : get_Symbol_Serial_Stype
 *
 * Description: return the granule category - stype0, stype1, or data
 *
 * Returns: the category
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Symbol_Serial_Stype(RIO_GRANULE_T *pl_Granule)
{
    if (pl_Granule == NULL)
        return RIO_PL_SERIAL_CATS_NUM; /*report errors by this way*/

    switch(pl_Granule->granule_Type)
    { 
        case RIO_GR_DATA:
            return 0;
            
        /*stype 0 symbols - need to be complemented with stype 1*/
        case RIO_GR_PACKET_ACCEPTED:
            return RIO_PL_SERIAL_GR_PACKET_ACCEPTED_STYPE0;
        case RIO_GR_PACKET_RETRY:
            return RIO_PL_SERIAL_GR_PACKET_RETRY_STYPE0;
        case RIO_GR_PACKET_NOT_ACCEPTED:
            return RIO_PL_SERIAL_GR_PACKET_NOT_ACCEPTED_STYPE0;
        case RIO_GR_IDLE:
            return RIO_PL_SERIAL_GR_IDLE_STYPE0;
        case RIO_GR_LINK_RESPONSE:
            return RIO_PL_SERIAL_GR_LINK_RESPONSE_STYPE0;

        /*stype 1 symbols - need to be comlemented with stype 0*/
        case RIO_GR_NEW_PACKET:
            return RIO_PL_SERIAL_GR_START_OF_PACKET_STYPE1;
        case RIO_GR_RESTART_FROM_RETRY:
            return RIO_PL_SERIAL_GR_RFR_STYPE1;
        case RIO_GR_TOD_SYNC:
            return RIO_PL_SERIAL_GR_MULTICAST_EVENT_STYPE1;
        case RIO_GR_EOP:
            return RIO_PL_SERIAL_GR_EOP_STYPE1;
        case RIO_GR_STOMP:
            return RIO_PL_SERIAL_GR_STOMP_STYPE1;
        case RIO_GR_LINK_REQUEST:
            return RIO_PL_SERIAL_GR_LINK_REQUEST_STYPE1;
        
        default:
            return 0; /*report errors by this way*/
        }

    return 0; /*report errors by this way*/
}
/***************************************************************************
 * Function : form_Serial_Granule
 *
 * Description: return the granule category - stype0, stype1, or data
 *
 * Returns: the category
 *
 * Notes: none
 *
 **************************************************************************/
static void form_Serial_Granule(
    RIO_PL_PCS_PMA_ADAPTER_DS_T *handle,
    unsigned int gran_Category, RIO_PCS_PMA_GRANULE_T *granule
    )
{
    if (handle == NULL || granule == NULL)
        return;
    switch (gran_Category)
    {
        case RIO_PL_SERIAL_STYPE_0:
            form_Serial_Symbol_Part(handle, RIO_TRUE, RIO_TRUE, granule);
            if (handle->tx_Data.granule_Flag[RIO_PL_SERIAL_STYPE_1] == RIO_TRUE)
                form_Serial_Symbol_Part(handle, RIO_FALSE, RIO_TRUE, granule);
            else
                form_Serial_Symbol_Part(handle, RIO_FALSE, RIO_FALSE, granule);
            break;
        case RIO_PL_SERIAL_STYPE_1:
            form_Serial_Symbol_Part(handle, RIO_FALSE, RIO_TRUE, granule);
            if (handle->tx_Data.granule_Flag[RIO_PL_SERIAL_STYPE_0] == RIO_TRUE)
                form_Serial_Symbol_Part(handle, RIO_TRUE, RIO_TRUE, granule);
            else
                form_Serial_Symbol_Part(handle, RIO_TRUE, RIO_FALSE, granule);
            break;
        case RIO_PL_SERIAL_DATA:
            form_Data_Granule(handle, granule);
            break;
    }

    /* 
     * set packet_In_Progress flag to allow the proper generation of the
     * IDLE sequence
     */

    /* if it is SOP granule set packet_In_Progress flag */
    if ( granule->is_Data == RIO_FALSE && 
        granule->granule_Struct.stype1 == RIO_PL_SERIAL_GR_START_OF_PACKET_STYPE1)
        handle->tx_Data.packet_In_Progress = RIO_TRUE;

    /* if it is packet terminating granule clear packet_In_Progress flag */
    if ( granule->is_Data == RIO_FALSE &&
           ( granule->granule_Struct.stype1 == RIO_PL_SERIAL_GR_STOMP_STYPE1 ||
               granule->granule_Struct.stype1 == RIO_PL_SERIAL_GR_EOP_STYPE1 ||
               granule->granule_Struct.stype1 == RIO_PL_SERIAL_GR_RFR_STYPE1 ||
               ( granule->granule_Struct.stype1 == RIO_PL_SERIAL_GR_LINK_REQUEST_STYPE1 &&
                   granule->granule_Struct.cmd == RIO_PL_SERIAL_LREQ_INPUT_STATUS ) )
       )
       handle->tx_Data.packet_In_Progress = RIO_FALSE;
    
}
/***************************************************************************
 * Function : form_Serial_Symbol_Part
 *
 * Description: return the granule category - stype0, stype1, or data
 *
 * Returns: the category
 *
 * Notes: none
 *
 **************************************************************************/
static void form_Serial_Symbol_Part(
    RIO_PL_PCS_PMA_ADAPTER_DS_T *handle,
    RIO_BOOL_T is_Stype_0,
    RIO_BOOL_T is_In_Buffer,
    RIO_PCS_PMA_GRANULE_T *granule
)
{
    unsigned long flag_Value;
    unsigned int flag_Parameter;
    if (handle == NULL || granule == NULL)
        return;

    if (is_Stype_0 == RIO_TRUE)
    {
        if (is_In_Buffer == RIO_FALSE)
        {
            granule->granule_Struct.stype0 = RIO_PL_SERIAL_GR_IDLE_STYPE0;
            /*get ackID status*/
            handle->interfaces.rio_Get_State_Flag(
                handle->interfaces.rio_Regs_Context, handle, RIO_INBOUND_ACKID, &flag_Value, &flag_Parameter);
            granule->granule_Struct.parameter0 = (RIO_UCHAR_T)flag_Value;

            /*get  buf_status*/
            handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle, 
                RIO_FLOW_CONTROL_MODE, &flag_Value, &flag_Parameter);

            if (flag_Value  == RIO_TRUE) /*transmit flow control*/
            {
                handle->interfaces.rio_Get_State_Flag(
                    handle->interfaces.rio_Regs_Context, handle, RIO_OUR_BUF_STATUS, 
                    &flag_Value, &flag_Parameter);
                granule->granule_Struct.parameter1 = (RIO_UCHAR_T)flag_Value;
            }
            else 
                granule->granule_Struct.parameter1 = 0x1f;

        }
        else 
        {
            granule->granule_Struct.stype0 = get_Symbol_Serial_Stype(&(handle->tx_Data.granule[RIO_PL_SERIAL_STYPE_0]));
            if (granule->granule_Struct.stype0 == RIO_PL_SERIAL_GR_IDLE_STYPE0)
            {
                /*get ackID status*/
                handle->interfaces.rio_Get_State_Flag(
                    handle->interfaces.rio_Regs_Context, handle, RIO_INBOUND_ACKID, 
                    &flag_Value, &flag_Parameter);
                granule->granule_Struct.parameter0 = (RIO_UCHAR_T)flag_Value;

                /*get  buf_status*/
                /* handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context,
                    RIO_OUR_BUF_STATUS, &flag_Value, &flag_Parameter);
                granule->granule_Struct.parameter1 = (RIO_UCHAR_T)flag_Value;*/

                handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_FLOW_CONTROL_MODE, &flag_Value, &flag_Parameter);

                if (flag_Value  == RIO_TRUE) /*transmit flow control*/
                {
                    handle->interfaces.rio_Get_State_Flag(
                    handle->interfaces.rio_Regs_Context, handle, RIO_OUR_BUF_STATUS, 
                        &flag_Value, &flag_Parameter);         
                    granule->granule_Struct.parameter1 = (RIO_UCHAR_T)flag_Value;
                }
                else 
                    granule->granule_Struct.parameter1 = 0x1f;

            }
            else
            {
                /*get ackID status*/
                granule->granule_Struct.parameter0 = handle->tx_Data.granule[RIO_PL_SERIAL_STYPE_0].granule_Struct.ack_ID;
                /*get  buf_status*/
                granule->granule_Struct.parameter1 = handle->tx_Data.granule[RIO_PL_SERIAL_STYPE_0].granule_Struct.buf_Status;
            }
            /*set off corresponded flag*/
            handle->tx_Data.granule_Flag[RIO_PL_SERIAL_STYPE_0] = RIO_FALSE;
        }
    }
    else
    {
        if (is_In_Buffer == RIO_FALSE)
        {
            granule->granule_Struct.stype1 = RIO_PL_SERIAL_GR_NOP_STYPE1;
            /*set_Cmd*/
            granule->granule_Struct.cmd = RIO_PL_SERIAL_GR_COMMON_STYPE1_CMD;
            
        }
        else 
        {
            granule->granule_Struct.stype1 = get_Symbol_Serial_Stype(&(handle->tx_Data.granule[RIO_PL_SERIAL_STYPE_1]));
            if (granule->granule_Struct.stype1 == RIO_PL_SERIAL_GR_LINK_REQUEST_STYPE1)
                granule->granule_Struct.cmd = handle->tx_Data.granule[RIO_PL_SERIAL_STYPE_1].granule_Struct.ack_ID;
            else
                granule->granule_Struct.cmd = RIO_PL_SERIAL_GR_COMMON_STYPE1_CMD;
            /*set off corresponded flag*/
            handle->tx_Data.granule_Flag[RIO_PL_SERIAL_STYPE_1] = RIO_FALSE;
        }
    }
    /*set the CRC flag into FALSE value to tell the PCS/PMA calculate the CRC*/
    granule->granule_Struct.is_Crc_Valid = RIO_FALSE;
    granule->is_Data = RIO_FALSE;
    return;
    
}

/***************************************************************************
 * Function : form_Data_Granule
 *
 * Description: forms data granule for transmitting
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void form_Data_Granule(RIO_PL_PCS_PMA_ADAPTER_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule)
{
    
    if (handle == NULL || granule == NULL)
        return;
    if (handle->tx_Data.granule[RIO_PL_SERIAL_DATA].granule_Type == RIO_GR_NEW_PACKET)
    {
            form_Serial_Symbol_Part(handle, RIO_TRUE, RIO_FALSE, granule);
            granule->granule_Struct.stype1 = RIO_PL_SERIAL_GR_START_OF_PACKET_STYPE1;
            granule->granule_Struct.cmd = RIO_PL_SERIAL_GR_COMMON_STYPE1_CMD;
            granule->is_Data = RIO_FALSE;
            /*set the granule type into data*/
            handle->tx_Data.granule[RIO_PL_SERIAL_DATA].granule_Type = RIO_GR_DATA;
    }
    else if (handle->tx_Data.granule[RIO_PL_SERIAL_DATA].granule_Type == RIO_GR_DATA)
    {
        unsigned int i;
        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            granule->granule_Date[i] = handle->tx_Data.granule[RIO_PL_SERIAL_DATA].granule_Date[i];
        granule->is_Data = RIO_TRUE;
        /*set of flag in buffer*/
        handle->tx_Data.granule_Flag[RIO_PL_SERIAL_DATA] = RIO_FALSE;
    }
    return;
}

/***************************************************************************
 * Function : pl_Pcs_Pma_Adapter_Granule_Received
 *
 * Description: receiver main loop
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Pcs_Pma_Adapter_Granule_Received(RIO_PL_PCS_PMA_ADAPTER_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule)
{
    RIO_GRANULE_T pl_Granule;
    unsigned int i;
    if (handle == NULL || granule == NULL)
        return;

    if (granule->is_Data == RIO_FALSE)
    {
        if (granule->granule_Struct.is_Crc_Valid == RIO_FALSE)
        {
            pl_Granule.granule_Type = RIO_GR_HAS_BEEN_CORRUPTED;
            handle->interfaces.rio_Pl_Pcs_Pma_Adapter_Put_Granule(
                handle->interfaces.rio_PL_Interface_Context,
                handle,
                &pl_Granule);
            /*in case when the corrupted granule is packet delimeter
            it is necessary to reset the is_pd flag to prevent 
            idle inside the packet error printout.
            This is works if only one error in the symbol is occured
            for example crc error.  

            In case when double error is occured it is impossible
            to recover correctly and this situation is not described
            in the specification.*/
            if (granule->is_Data == RIO_FALSE)
                if (granule->granule_Struct.stype1 <= 4 /*SOP, STOMP, EOP, RFR, LRQ*/)
                {
                    handle->rx_Data.start_Packet_Flag = RIO_FALSE;
                    handle->interfaces.rio_Set_State_Flag(
                        handle->interfaces.rio_Regs_Context, handle,
                        RIO_RX_IS_PD, handle->rx_Data.start_Packet_Flag, 0);
                }



            return;
        }
        pl_Granule.granule_Type = get_Pl_Granule_Type(granule, RIO_PL_SERIAL_STYPE_0);
        switch (pl_Granule.granule_Type)
        {
            case RIO_GR_PACKET_ACCEPTED:
            case RIO_GR_PACKET_RETRY:
            case RIO_GR_PACKET_NOT_ACCEPTED:
                pl_Granule.granule_Struct.ack_ID = granule->granule_Struct.parameter0;
                pl_Granule.granule_Struct.buf_Status = granule->granule_Struct.parameter1;
            break;
            case RIO_GR_LINK_RESPONSE:
                pl_Granule.granule_Struct.ack_ID = granule->granule_Struct.parameter0;
            case RIO_GR_IDLE:
                pl_Granule.granule_Struct.buf_Status = granule->granule_Struct.parameter1;
            break;
            default:
            break;
        }
        if (pl_Granule.granule_Type != RIO_GR_UNRECOGNIZED)
            handle->interfaces.rio_Pl_Pcs_Pma_Adapter_Put_Granule(
                handle->interfaces.rio_PL_Interface_Context,
                handle,
                &pl_Granule);

        pl_Granule.granule_Type = get_Pl_Granule_Type(granule, RIO_PL_SERIAL_STYPE_1);
        switch (pl_Granule.granule_Type)
        {
            case RIO_GR_NEW_PACKET:
                if (handle->rx_Data.start_Packet_Flag == RIO_FALSE)
                {
                    handle->rx_Data.start_Packet_Flag = RIO_TRUE;
                    handle->interfaces.rio_Set_State_Flag(
                        handle->interfaces.rio_Regs_Context, handle,
                        RIO_RX_IS_PD, handle->rx_Data.start_Packet_Flag, 0);
                }
                else
                {
                    /*protocol violation - unexpected SOP*/
                    handle->interfaces.rio_Set_State_Flag(
                        handle->interfaces.rio_Regs_Context,
                        handle,
                        RIO_OUTPUT_ERROR,
                        RIO_TRUE,
                        0);
#ifdef _DEBUG
                    /* 
                     * this is just to debug this situation as PCS/PMA Adapter
                     * doesn't have any other means of reporting error except printf
                     * (it doesn't have user_Msg callback)
                     */
                    printf("PCS/PMA Adapter: Error: two consequtive SOP symbols without EOP symbol\n");
#endif
                }
            break;
            case RIO_GR_LINK_REQUEST:
                pl_Granule.granule_Struct.ack_ID = granule->granule_Struct.cmd;
            case RIO_GR_RESTART_FROM_RETRY:
                pl_Granule.granule_Struct.buf_Status = granule->granule_Struct.parameter1;
                handle->rx_Data.start_Packet_Flag = RIO_FALSE;
                break;
            case RIO_GR_TOD_SYNC:
                pl_Granule.granule_Struct.buf_Status = granule->granule_Struct.parameter1;
                break;
            case RIO_GR_EOP:
            case RIO_GR_STOMP:
                pl_Granule.granule_Struct.buf_Status = granule->granule_Struct.parameter1;
                handle->rx_Data.start_Packet_Flag = RIO_FALSE;
            break;
            default:
            break;
        }
        /*invoke pl callback*/
        if ((pl_Granule.granule_Type != RIO_GR_NEW_PACKET)  &&
            (pl_Granule.granule_Type != RIO_GR_UNRECOGNIZED) &&
            (pl_Granule.granule_Type != RIO_GR_IDLE))
            handle->interfaces.rio_Pl_Pcs_Pma_Adapter_Put_Granule(
                handle->interfaces.rio_PL_Interface_Context,
                handle,
                &pl_Granule);

        /*reset the is_pd flag if necessary
        this is necessary do after passing granule because
        in case when packet contains only SOP and STOMP
        PL need to access this flag for determine that 
        stomp inside the packet*/
        switch (pl_Granule.granule_Type)
        {
            case RIO_GR_LINK_REQUEST:
            case RIO_GR_RESTART_FROM_RETRY:
            case RIO_GR_EOP:
            case RIO_GR_STOMP:
                handle->interfaces.rio_Set_State_Flag(
                    handle->interfaces.rio_Regs_Context, handle,
                    RIO_RX_IS_PD, handle->rx_Data.start_Packet_Flag, 0);
            break;
            default:
                ;
        }

            
    }
    else
    {
        if (handle->rx_Data.start_Packet_Flag == RIO_TRUE)
        {
            pl_Granule.granule_Type = RIO_GR_NEW_PACKET;
            /*get ackID*/
            pl_Granule.granule_Struct.ack_ID = granule->granule_Date[0] >> 3;

            pl_Granule.granule_Struct.s = 0;

            /* in start of packet prio field will be stored in buf_Status 
             * we shall shift if 6 bits to the right 
             */
            pl_Granule.granule_Struct.buf_Status =
                (granule->granule_Date[1] & (RIO_UCHAR_T)0xc0) >> 6;

            handle->rx_Data.start_Packet_Flag = RIO_FALSE;
            /*handle->interfaces.rio_Set_State_Flag(
                handle->interfaces.rio_Regs_Context, handle,
                RIO_RX_IS_PD, handle->rx_Data.start_Packet_Flag, 0);*/
        }
        else
            pl_Granule.granule_Type = RIO_GR_DATA;

        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            pl_Granule.granule_Date[i] = granule->granule_Date[i];

        /*invoke pl callback*/
        handle->interfaces.rio_Pl_Pcs_Pma_Adapter_Put_Granule(
            handle->interfaces.rio_PL_Interface_Context,
            handle,
            &pl_Granule);
    }
    return;
}
/***************************************************************************
 * Function : get_Symbol_Serial_Stype
 *
 * Description: return the granule category - stype0, stype1, or data
 *
 * Returns: the category
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int get_Pl_Granule_Type(RIO_PCS_PMA_GRANULE_T *granule, unsigned int category)
{
    if (granule == NULL)
        return RIO_PL_SERIAL_CATS_NUM; /*report errors by this way*/

    switch (category)
    {
        case RIO_PL_SERIAL_STYPE_0:
            switch(granule->granule_Struct.stype0)
            { 
                /*stype 0 symbols - need to be complemented with stype 1*/
                case RIO_PL_SERIAL_GR_PACKET_ACCEPTED_STYPE0:
                    return RIO_GR_PACKET_ACCEPTED;
                case RIO_PL_SERIAL_GR_PACKET_RETRY_STYPE0:
                    return RIO_GR_PACKET_RETRY;
                case RIO_PL_SERIAL_GR_PACKET_NOT_ACCEPTED_STYPE0:
                    return RIO_GR_PACKET_NOT_ACCEPTED;
                case RIO_PL_SERIAL_GR_IDLE_STYPE0:
                    return RIO_GR_IDLE;
                case RIO_PL_SERIAL_GR_LINK_RESPONSE_STYPE0:
                    return RIO_GR_LINK_RESPONSE;
                default:
                    return RIO_GR_UNRECOGNIZED;

            }
            break;
        case RIO_PL_SERIAL_STYPE_1:
            switch (granule->granule_Struct.stype1)
            {
                case RIO_PL_SERIAL_GR_START_OF_PACKET_STYPE1:
                    return RIO_GR_NEW_PACKET;
                case RIO_PL_SERIAL_GR_RFR_STYPE1:
                    return RIO_GR_RESTART_FROM_RETRY;
                case RIO_PL_SERIAL_GR_MULTICAST_EVENT_STYPE1:
                    return RIO_GR_TOD_SYNC;
                case RIO_PL_SERIAL_GR_EOP_STYPE1:
                    return RIO_GR_EOP;
                case RIO_PL_SERIAL_GR_STOMP_STYPE1:
                    return RIO_GR_STOMP;
                case RIO_PL_SERIAL_GR_LINK_REQUEST_STYPE1:
                    return RIO_GR_LINK_REQUEST;
                case RIO_PL_SERIAL_GR_NOP_STYPE1:
                    return RIO_GR_IDLE;
                default:
                    return RIO_GR_UNRECOGNIZED;

            }
            break;
        case RIO_PL_SERIAL_DATA:
            return RIO_GR_DATA;
    }
    return 0; /*report errors by this way*/
}
/*****************************************************************************/

