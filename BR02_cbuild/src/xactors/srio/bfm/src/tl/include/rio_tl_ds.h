#ifndef RIO_TL_DS_H
#define RIO_TL_DS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/tl/include/rio_tl_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains tl data structure description 
*
* Notes:        
*
******************************************************************************/


#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif /* RIO_TYPES_H */

#ifndef RIO_LL_INTERFACE_H
#include "rio_ll_interface.h"
#endif

#ifndef RIO_PL_INTERFACE_H
#include "rio_pl_interface.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif 


/* the data type represents transport layer parameters set */
typedef struct {
/*    RIO_TR_INFO_T        dev_ID; */
    RIO_ROUTING_METHOD_T routing_Meth;
    RIO_PL_TRANSP_TYPE_T transp_Type;
} RIO_TL_PARAM_SET_T;


/* callback tray to be used by the transport layer model */
typedef struct {

    /* callbacks provided by the physical layer */
    RIO_SET_CONFIG_REG            rio_Set_Config_Reg;
    RIO_GET_CONFIG_REG            rio_Get_Config_Reg;
    RIO_CONTEXT_T                 rio_Config_Context;

    RIO_PL_GSM_REQUEST            rio_PL_GSM_Request;
    RIO_PL_IO_REQUEST             rio_PL_IO_Request;
    RIO_PL_MESSAGE_REQUEST        rio_PL_Message_Request;
    RIO_PL_DOORBELL_REQUEST       rio_PL_Doorbell_Request;
    RIO_PL_CONFIGURATION_REQUEST  rio_PL_Configuration_Request;
    RIO_PL_SEND_RESPONSE          rio_PL_Send_Response;
    RIO_CONTEXT_T                 rio_PL_Outbound_Context;

    RIO_PL_ACK_REMOTE_REQ         rio_PL_Ack_Remote_Req;
    RIO_CONTEXT_T                 rio_PL_Inbound_Context;

    /* callbacks provided by the logical layer */
    RIO_PL_ACK_REQUEST            rio_LL_Ack_Request;
    RIO_PL_GET_REQ_DATA           rio_LL_Get_Req_Data;
    RIO_PL_GET_RESP_DATA          rio_LL_Get_Resp_Data;
    RIO_PL_ACK_RESPONSE           rio_LL_Ack_Response;
    RIO_PL_REQ_TIME_OUT           rio_LL_Req_Time_Out;
    RIO_PL_REMOTE_REQUEST         rio_LL_Remote_Request;
    RIO_PL_REMOTE_RESPONSE        rio_LL_Remote_Response;
    RIO_CONTEXT_T                 rio_LL_Interface_Context; 
    
    RIO_REMOTE_CONFIG_READ        rio_LL_Remote_Conf_Read;
    RIO_REMOTE_CONFIG_WRITE       rio_LL_Remote_Conf_Write;
    RIO_CONTEXT_T                 extended_Features_Context;
    
    RIO_USER_MSG                  rio_User_Msg;
    RIO_CONTEXT_T                 rio_Msg;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_DS_REQUEST             rio_PL_DS_Request; 
    RIO_PL_GET_REQ_DATA_HW        rio_LL_Get_Req_Data_Hw;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_PL_FC_REQUEST             rio_PL_FC_Request;
    RIO_PL_CONGESTION             rio_PL_Congestion;
    RIO_PL_FC_TO_SEND             rio_LL_FC_To_Send;
    /* GDA: Bug#125 */
 
} RIO_TL_CALLBACK_TRAY_T;


/* logical layer data structure */

typedef struct {

    /* tray of callbacks used to access environment or transport layer */
    RIO_TL_CALLBACK_TRAY_T  callback_Tray;

    /* instance's parameters */
    RIO_TL_PARAM_SET_T      init_Params;  /* parameters set during initialization */

    /* reset flag */
    RIO_BOOL_T              in_Reset;

} RIO_TL_DS_T;

#endif /* RIO_LL_DS_H */
