#ifndef RIO_TL_MODULE_H
#define RIO_TL_MODULE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/tl/include/rio_tl_module.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains transport layer module interfaces to the logical 
*               and physical layers
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif /* RIO_TYPES_H */

/* functions to be called by environment to initialize instance */
int RIO_TL_Initialize(
    RIO_HANDLE_T              handle,
    RIO_TL_PARAM_SET_T        *param_Set
);

int RIO_TL_Start_Reset(
    RIO_HANDLE_T handle
);

int RIO_TL_Delete_Instance(
    RIO_HANDLE_T handle
);

/* functions allowing environment to request various transactions */
int RIO_TL_IO_Request(
    RIO_HANDLE_T   handle, 
    RIO_IO_REQ_T   *rq, 
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
);

int RIO_TL_GSM_Request(
    RIO_HANDLE_T       handle,
    RIO_PL_GSM_TTYPE_T ttype,          /* requested transaction type  */
    RIO_GSM_REQ_T      *rq,
    RIO_UCHAR_T        sec_ID,            /* ignored for packet types other than 1 */
    RIO_UCHAR_T        sec_TID,
    RIO_TAG_T          trx_Tag,           /* user-assigned tag of the request */
    RIO_TR_INFO_T      transport_Info[],
    RIO_UCHAR_T        multicast_Size     /* count of recepients */
);

int RIO_TL_Config_Request(
    RIO_HANDLE_T     handle, 
    RIO_CONFIG_REQ_T *rq, 
    RIO_TAG_T        trx_Tag
);

int RIO_TL_Message_Request(
    RIO_HANDLE_T      handle, 
    RIO_MESSAGE_REQ_T *rq, 
    RIO_TAG_T         trx_Tag
);

int RIO_TL_Doorbell_Request(
    RIO_HANDLE_T       handle, 
    RIO_DOORBELL_REQ_T *rq, 
    RIO_TAG_T          trx_Tag
);

int RIO_TL_Remote_Request(
    RIO_HANDLE_T         handle, 
    RIO_TAG_T            tag, 
    RIO_REMOTE_REQUEST_T *req, 
    RIO_TR_INFO_T        transport_Info
);

int RIO_TL_Remote_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
);

int RIO_TL_Ack_Remote_Request(
    RIO_HANDLE_T handle, 
    RIO_TAG_T    tag
);

int RIO_TL_Set_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     data
);

int RIO_TL_Get_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     *data
);

int RIO_TL_Send_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
);

/* functions to be called by physical layer */
int RIO_TL_Ack_Request(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);

int RIO_TL_Ack_Responce(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);

int RIO_TL_Request_Timeout(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);


/* functions to be called by the physical layer to get data*/
int RIO_TL_Get_Request_Data(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
);

int RIO_TL_Get_Response_Data(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
);

/* functions to be called by the physical layer to get access to the extended features space */
int RIO_TL_Ext_Features_Read( 
    RIO_CONTEXT_T   handle, 
    RIO_CONF_OFFS_T config_Offset, 
    unsigned long   *data 
);

int RIO_TL_Ext_Features_Write( 
    RIO_CONTEXT_T   handle, 
    RIO_CONF_OFFS_T config_Offset, 
    unsigned long   data 
);

/* GDA: Bug#124 - Support for Data Streaming packet */
int RIO_TL_DS_Request(
    RIO_HANDLE_T   handle,
    RIO_DS_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
);

int RIO_TL_Get_Request_Data_Hw(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_HW_T      *data_hw
);
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
int RIO_TL_FC_Request(
    RIO_HANDLE_T   handle, 
    RIO_FC_REQ_T   *rq, 
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
);

int RIO_TL_Congestion(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      trx_Tag
);

int RIO_TL_FC_To_Send(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
);
/* GDA: Bug#125 */ 

#endif /* RIO_LL_MODULE_H */
