/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\tl\rio_tl.c $ 
* $Author: markk $ 
* $Revision: 1.5 $ 
* $VOB: \riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains transport layer module implementation
* Notes:        
*
******************************************************************************/


#include "rio_tl_ds.h"
#include "rio_tl_module.h"

#include <stdlib.h>


/* masks for the transport info processing in case of the TABLE BASED routing */
#define RIO_TL_TR_INFO_16_SRC_MASK  0x000000FF
#define RIO_TL_TR_INFO_16_DEST_MASK 0x0000FF00

#define RIO_TL_TR_INFO_32_SRC_MASK  0x0000FFFF
#define RIO_TL_TR_INFO_32_DEST_MASK 0xFFFF0000

/* offsets of the transport info fields in case of the TABLE BASED routing */
#define RIO_TL_TR_INFO_16_DEST_OFFSET 8
#define RIO_TL_TR_INFO_32_DEST_OFFSET 16

/* word address of the Base Device ID CSR */
#define RIO_TL_BASE_ADDRESS_CSR 0x60

static RIO_TR_INFO_T get_Device_ID(RIO_TL_DS_T *handle, RIO_PL_TRANSP_TYPE_T tt);


/* functions allowing logical layer to request various transactions */

/***************************************************************************
 * Function :    RIO_TL_GSM_Request
 *
 * Description:  requests GSM transaction to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_GSM_Request(
    RIO_HANDLE_T       handle,
    RIO_PL_GSM_TTYPE_T ttype,          /* requested transaction type  */
    RIO_GSM_REQ_T      *rq,
    RIO_UCHAR_T        sec_ID,            /* ignored for packet types other than 1 */
    RIO_UCHAR_T        sec_TID,
    RIO_TAG_T          trx_Tag,           /* user-assigned tag of the request */
    RIO_TR_INFO_T      transport_Info[],
    RIO_UCHAR_T        multicast_Size     /* count of recepients */

)
{
    RIO_TL_DS_T   *tl_Data;
    RIO_TR_INFO_T tr_Info_Copy[RIO_MAX_COH_DOMAIN_SIZE];
    unsigned int  i;
    
    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
 
    dev_ID = get_Device_ID(tl_Data, rq->transp_Type);
    
    /* form proper transport info in case of the TABLE BASED routing algorythm */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
        for ( i = 0; i < multicast_Size; i++ )
        {
            if ( rq->transp_Type == RIO_PL_TRANSP_16 )
            {
                tr_Info_Copy[i] = transport_Info[i];
                tr_Info_Copy[i] <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
                tr_Info_Copy[i] &=  RIO_TL_TR_INFO_16_DEST_MASK;
                tr_Info_Copy[i] |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
            }
            else if ( rq->transp_Type == RIO_PL_TRANSP_32 )
            {
                tr_Info_Copy[i] = transport_Info[i];
                tr_Info_Copy[i] <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
                tr_Info_Copy[i] &=  RIO_TL_TR_INFO_32_DEST_MASK;
                tr_Info_Copy[i] |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
            }
        }
        
        
    return tl_Data->callback_Tray.rio_PL_GSM_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            ttype,
            rq,
            sec_ID,
            sec_TID,
            trx_Tag,
            tr_Info_Copy,
            multicast_Size
            );
}

/***************************************************************************
 * Function :    RIO_TL_IO_Request
 *
 * Description:  requests IO transaction to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_IO_Request(
    RIO_HANDLE_T   handle, 
    RIO_IO_REQ_T   *rq, 
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
)
{
    RIO_TL_DS_T *tl_Data;
    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

    dev_ID = get_Device_ID(tl_Data, rq->transp_Type);
        
    /* form proper transport info in case of the TABLE BASED routing algorythm */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( rq->transp_Type == RIO_PL_TRANSP_16 )
        {
            transport_Info <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
            transport_Info &=  RIO_TL_TR_INFO_16_DEST_MASK;
            transport_Info |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
        }
        else if ( rq->transp_Type == RIO_PL_TRANSP_32 )
        {
            transport_Info <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
            transport_Info &=  RIO_TL_TR_INFO_32_DEST_MASK;
            transport_Info |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
        }
    }
        
    return tl_Data->callback_Tray.rio_PL_IO_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            rq,
            trx_Tag,
            transport_Info
            );
}


/***************************************************************************
 * Function :    RIO_TL_Config_Request
 *
 * Description:  requests configuration transaction to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Config_Request(
    RIO_HANDLE_T     handle, 
    RIO_CONFIG_REQ_T *rq, 
    RIO_TAG_T        trx_Tag
)
{
    RIO_TL_DS_T *tl_Data;
    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    dev_ID = get_Device_ID(tl_Data, rq->transp_Type);
        
    /* form proper transport info in case of the TABLE BASED routing algorythm */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( rq->transp_Type == RIO_PL_TRANSP_16 )
        {
            rq->transport_Info <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
            rq->transport_Info &=  RIO_TL_TR_INFO_16_DEST_MASK;
            rq->transport_Info |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
        }
        else if ( rq->transp_Type == RIO_PL_TRANSP_32 )
        {
            rq->transport_Info <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
            rq->transport_Info &=  RIO_TL_TR_INFO_32_DEST_MASK;
            rq->transport_Info |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
        }
    }
        
    return tl_Data->callback_Tray.rio_PL_Configuration_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            rq,
            trx_Tag
            );
}

/***************************************************************************
 * Function :    RIO_TL_Message_Request
 *
 * Description:  requests message transaction to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Message_Request(
    RIO_HANDLE_T      handle, 
    RIO_MESSAGE_REQ_T *rq, 
    RIO_TAG_T         trx_Tag
)
{
    RIO_TL_DS_T *tl_Data;

    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

    dev_ID = get_Device_ID(tl_Data, rq->transp_Type);
        
    /* form proper transport info in case of the TABLE BASED routing algorythm */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( rq->transp_Type == RIO_PL_TRANSP_16 )
        {
            rq->transport_Info <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
            rq->transport_Info &=  RIO_TL_TR_INFO_16_DEST_MASK;
            rq->transport_Info |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
        }
        else if ( rq->transp_Type == RIO_PL_TRANSP_32 )
        {
            rq->transport_Info <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
            rq->transport_Info &=  RIO_TL_TR_INFO_32_DEST_MASK;
            rq->transport_Info |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
        }
    }
        
    return tl_Data->callback_Tray.rio_PL_Message_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            rq,
            trx_Tag
            );
}

/***************************************************************************
 * Function :    RIO_TL_Doorbell_Request
 *
 * Description:  requests doorbell transaction to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Doorbell_Request(
    RIO_HANDLE_T       handle, 
    RIO_DOORBELL_REQ_T *rq, 
    RIO_TAG_T          trx_Tag
)
{
    RIO_TL_DS_T *tl_Data;

    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

    dev_ID = get_Device_ID(tl_Data, rq->transp_Type);
        
    /* form proper transport info in case of the TABLE BASED routing algorythm */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( rq->transp_Type == RIO_PL_TRANSP_16 )
        {
            rq->transport_Info <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
            rq->transport_Info &=  RIO_TL_TR_INFO_16_DEST_MASK;
            rq->transport_Info |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
        }
        else if ( rq->transp_Type == RIO_PL_TRANSP_32 )
        {
            rq->transport_Info <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
            rq->transport_Info &=  RIO_TL_TR_INFO_32_DEST_MASK;
            rq->transport_Info |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
        }
    }
        
    return tl_Data->callback_Tray.rio_PL_Doorbell_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            rq,
            trx_Tag
            );
}

/***************************************************************************
 * Function :    RIO_TL_Send_Response
 *
 * Description:  requests PL to send response
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Send_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
)
{
    RIO_TL_DS_T   *tl_Data;
    RIO_TR_INFO_T src_ID;
    RIO_TR_INFO_T dest_ID;
    RIO_TR_INFO_T        dev_ID;
    

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

    dev_ID = get_Device_ID(tl_Data, resp->transp_Type);

    /* form proper transport info for the response */
    switch ( tl_Data->init_Params.routing_Meth )
    {
        case RIO_TABLE_BASED_ROUTING:
        {
            /* to compose proper transport info field for the response packet     */
            /* it is needed to change places of the destination ID and the source */
            /* ID in the transport info received with the request                 */
            if ( resp->transp_Type == RIO_PL_TRANSP_16 )
            {
                /* compose transport info field */
                src_ID = resp->tr_Info & RIO_TL_TR_INFO_16_SRC_MASK;
                dest_ID = resp->tr_Info & RIO_TL_TR_INFO_16_DEST_MASK;
                dest_ID >>= RIO_TL_TR_INFO_16_DEST_OFFSET;

                resp->tr_Info = 0;
                resp->tr_Info = src_ID << RIO_TL_TR_INFO_16_DEST_OFFSET;
                resp->tr_Info |= ( RIO_TL_TR_INFO_16_SRC_MASK & dest_ID );
                
                /* compose secondary ID field */
                resp->sec_ID <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
                resp->sec_ID &=  RIO_TL_TR_INFO_16_DEST_MASK;
                resp->sec_ID |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
            }
            else if ( resp->transp_Type == RIO_PL_TRANSP_32 )
            {
                src_ID = resp->tr_Info & RIO_TL_TR_INFO_32_SRC_MASK;
                dest_ID = resp->tr_Info & RIO_TL_TR_INFO_32_DEST_MASK;
                dest_ID >>= RIO_TL_TR_INFO_32_DEST_OFFSET;

                resp->tr_Info = 0;
                resp->tr_Info = src_ID << RIO_TL_TR_INFO_32_DEST_OFFSET;
                resp->tr_Info |= ( RIO_TL_TR_INFO_32_SRC_MASK & dest_ID );
                
                /* compose secondary ID field */                
                resp->sec_ID <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
                resp->sec_ID &=  RIO_TL_TR_INFO_32_DEST_MASK;
                resp->sec_ID |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
            }
                        
            break;
        }

        case RIO_SHIFT_BASED_ROUTING:
            /* do nothing */

        case RIO_INDEX_BASED_ROUTING:
            /* do nothing */
            ;
    }
        
    return tl_Data->callback_Tray.rio_PL_Send_Response(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            tag, 
            resp
            );
}

/* functions are called by physical layer to notify logical layer */

/***************************************************************************
 * Function :    RIO_TL_Remote_Request
 *
 * Description:  notifies LL about new RIO request
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Remote_Request(
    RIO_CONTEXT_T        handle, 
    RIO_TAG_T            tag, 
    RIO_REMOTE_REQUEST_T *req, 
    RIO_TR_INFO_T        transport_Info
)
{
    RIO_TL_DS_T   *tl_Data;
    RIO_TR_INFO_T dest_ID = 0; /* destination device ID from the received packet */
    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

    dev_ID = get_Device_ID(tl_Data, req->transp_Type);
        
    /* check that the received packet has proper transport type */
    if ( (( req->transp_Type != RIO_PL_TRANSP_16 ) &&
        ( req->transp_Type != RIO_PL_TRANSP_32 )) ||
    /*in case of 32 bit tr info it is necessary to check that model
    supports lage transport info*/
        (req->transp_Type == RIO_PL_TRANSP_32 &&
        tl_Data->init_Params.transp_Type != RIO_PL_TRANSP_32 )
        )
    {
        /* issue error message */

        /*error message*/
        if ( tl_Data->callback_Tray.rio_User_Msg != NULL)
            tl_Data->callback_Tray.rio_User_Msg(
                tl_Data->callback_Tray.rio_Msg,
                "Warning: Transport Layer received the packet with illegal transport type field value. \n"
            );

        /* drop the packet */
        tl_Data->callback_Tray.rio_PL_Ack_Remote_Req(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Inbound_Context,
            tag
        );

        return RIO_OK;
    }
        
    /* in case of the TABLE BASED routing extract source device ID from the */
    /* transport info                                                       */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( req->transp_Type == RIO_PL_TRANSP_16 )
            req->header->src_ID = transport_Info & RIO_TL_TR_INFO_16_SRC_MASK;
        else if ( req->transp_Type == RIO_PL_TRANSP_32 )
            req->header->src_ID = transport_Info & RIO_TL_TR_INFO_32_SRC_MASK;
    }

    /* if the destination device ID from the paket isn't equal to our */
    /* device ID the warning message shall be issued                  */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        /* extract destination device ID from the packet */
        if ( req->transp_Type == RIO_PL_TRANSP_16 )
        {
            dest_ID = transport_Info & RIO_TL_TR_INFO_16_DEST_MASK;
            dest_ID = ( dest_ID >> RIO_TL_TR_INFO_16_DEST_OFFSET ) & RIO_TL_TR_INFO_16_SRC_MASK;
        }
        else if ( req->transp_Type == RIO_PL_TRANSP_32 )
        {
            dest_ID = transport_Info & RIO_TL_TR_INFO_32_DEST_MASK;
            dest_ID = ( dest_ID >> RIO_TL_TR_INFO_32_DEST_OFFSET ) & RIO_TL_TR_INFO_32_SRC_MASK;
        }

        /* issue warning message in case the destination device ID isn't equal */
        /* to our device ID                                                    */
        if ( dest_ID != dev_ID && tl_Data->callback_Tray.rio_User_Msg != NULL )
            tl_Data->callback_Tray.rio_User_Msg(
                tl_Data->callback_Tray.rio_Msg,
                "Warning: Transport Layer received the packet with the destination ID not equal to the PE device ID.\n"
            );
    }

        
    return tl_Data->callback_Tray.rio_LL_Remote_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag,            
            req,
            transport_Info
            );
}


/***************************************************************************
 * Function :    RIO_TL_Remote_Response
 *
 * Description:  notifies LL about response arrival from RIO 
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Remote_Response(
    RIO_CONTEXT_T  handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
)
{
    RIO_TL_DS_T   *tl_Data;
    RIO_TR_INFO_T dest_ID = 0;
    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

    dev_ID = get_Device_ID(tl_Data, resp->transp_Type);
        
    /* in case of the TABLE BASED routing extract source device ID from the */
    /* transport info                                                       */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( resp->transp_Type == RIO_PL_TRANSP_16 )
            resp->src_ID = resp->tr_Info & RIO_TL_TR_INFO_16_SRC_MASK;
        else if ( resp->transp_Type == RIO_PL_TRANSP_32 )
            resp->src_ID = resp->tr_Info & RIO_TL_TR_INFO_32_SRC_MASK;
    }

    /* check that the received packet has proper transport type */
    if ( (( resp->transp_Type != RIO_PL_TRANSP_16 ) &&
        ( resp->transp_Type != RIO_PL_TRANSP_32 )) ||
    /*in case of 32 bit tr info it is necessary to check that model
    supports lage transport info*/
        (resp->transp_Type == RIO_PL_TRANSP_32 &&
        tl_Data->init_Params.transp_Type != RIO_PL_TRANSP_32 )
        )
    {
        /* issue error message */

        /*error message*/
        if ( tl_Data->callback_Tray.rio_User_Msg != NULL)
            tl_Data->callback_Tray.rio_User_Msg(
                tl_Data->callback_Tray.rio_Msg,
                "Warning: Transport Layer received the packet with illegal transport type field value. \n"
            );

        /* drop the packet */
        return RIO_OK;
    }



    /* if the destination device ID from the paket isn't equal to our */
    /* device ID the warning message shall be issued                  */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        /* extract destination device ID from the packet */
        if ( resp->transp_Type == RIO_PL_TRANSP_16 )
        {
            dest_ID = resp->tr_Info & RIO_TL_TR_INFO_16_DEST_MASK;
            dest_ID = ( dest_ID >> RIO_TL_TR_INFO_16_DEST_OFFSET ) & RIO_TL_TR_INFO_16_SRC_MASK;
        }
        else if ( resp->transp_Type == RIO_PL_TRANSP_32 )
        {
            dest_ID = resp->tr_Info & RIO_TL_TR_INFO_32_DEST_MASK;
            dest_ID = ( dest_ID >> RIO_TL_TR_INFO_32_DEST_OFFSET ) & RIO_TL_TR_INFO_32_SRC_MASK;
        }

        /* issue warning message in case the destination device ID isn't equal */
        /* to our device ID                                                    */
        if ( dest_ID != dev_ID && tl_Data->callback_Tray.rio_User_Msg != NULL)
            tl_Data->callback_Tray.rio_User_Msg(
                tl_Data->callback_Tray.rio_Msg,
                "Warning: Transport Layer received the response with the destination ID not equal to the PE device ID.\n"
            );
    }

    return tl_Data->callback_Tray.rio_LL_Remote_Response(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag,            
            resp
            );
}

/***************************************************************************
 * Function :    RIO_TL_Ack_Remote_Request
 *
 * Description:  acknoledge remote request to the PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Ack_Remote_Request(
    RIO_HANDLE_T handle, 
    RIO_TAG_T    tag
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

        
    return tl_Data->callback_Tray.rio_PL_Ack_Remote_Req(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Inbound_Context,
            tag
            );
}

/***************************************************************************
 * Function :    RIO_TL_Set_Config_Reg
 *
 * Description:  route configuration write to the PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Set_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     data
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_Set_Config_Reg(
            tl_Data->callback_Tray.rio_Config_Context,
            config_Offset, 
            data
        );
}

/***************************************************************************
 * Function :    RIO_TL_Get_Config_Reg
 *
 * Description:  route configuration read to the PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Get_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     *data
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_Get_Config_Reg(
            tl_Data->callback_Tray.rio_Config_Context,
            config_Offset, 
            data
        );
}

/***************************************************************************
 * Function :    RIO_TL_Ack_Request
 *
 * Description:  acknowledges LL request to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Ack_Request(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Ack_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag         
            );
}

/***************************************************************************
 * Function :    RIO_TL_Ack_Responce
 *
 * Description:  acknowledges LL response to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Ack_Responce(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Ack_Response(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag         
            );
}


/***************************************************************************
 * Function :    RIO_TL_Request_Timeout
 *
 * Description:  notifies LL about logical timeout
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Request_Timeout(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Req_Time_Out(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag         
            );
}


/* functions to be called by the physical layer to get data*/

/***************************************************************************
 * Function :    RIO_TL_Get_Request_Data
 *
 * Description:  PL retrieves data associated with the request
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Get_Request_Data(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data

)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Get_Req_Data(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag,            
            offset,
            cnt,
            data
            );
}

/***************************************************************************
 * Function :    RIO_TL_Get_Response_Data
 *
 * Description:  PL retrieves data associated with the response
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Get_Response_Data(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data

)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Get_Resp_Data(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag,            
            offset,
            cnt,
            data
            );
}

/* functions to be called by the physical layer to get access to the extended features space */

/***************************************************************************
 * Function :    RIO_TL_Ext_Features_Read
 *
 * Description:  access to extended features space
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Ext_Features_Read( 
    RIO_CONTEXT_T   handle, 
    RIO_CONF_OFFS_T config_Offset, 
    unsigned long   *data 
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Remote_Conf_Read(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.extended_Features_Context,
            config_Offset,          
            data
            );
}


/***************************************************************************
 * Function :    RIO_TL_Ext_Features_Write
 *
 * Description:  access to extended features space
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Ext_Features_Write( 
    RIO_CONTEXT_T   handle, 
    RIO_CONF_OFFS_T config_Offset, 
    unsigned long   data 
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Remote_Conf_Write(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.extended_Features_Context,
            config_Offset,          
            data
            );
}

/***************************************************************************
 * Function :    get_Device_ID
 *
 * Description:  calls PL to obtain the value of the Base Device ID CSR
 *
 * Returns:      Device ID
 *
 * Notes:        
 *
 **************************************************************************/

static RIO_TR_INFO_T get_Device_ID(RIO_TL_DS_T *handle, RIO_PL_TRANSP_TYPE_T tt)
{
    unsigned long reg_Value;
    unsigned long field_Value;
    RIO_CONF_OFFS_T   config_Offset;
    

    config_Offset = RIO_TL_BASE_ADDRESS_CSR;
    handle->callback_Tray.rio_Get_Config_Reg( 
        handle->callback_Tray.rio_Config_Context, 
        config_Offset, 
        &reg_Value);
                                              
    if( tt == RIO_PL_TRANSP_16 )
        field_Value = (reg_Value >> 16) & RIO_TL_TR_INFO_16_SRC_MASK;
        
    else if ( tt == RIO_PL_TRANSP_32 )
        field_Value = reg_Value & RIO_TL_TR_INFO_32_SRC_MASK;
        
    else
        /* impossible case */
        field_Value = 0;
    
    return field_Value;
}

/* GDA: Bug#124 - Support for Data Streaming packet */
/***************************************************************************
 * Function :    RIO_TL_DS_Request
 *
 * Description:  requests DataStreaming transaction to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_DS_Request(
    RIO_HANDLE_T   handle, 
    RIO_DS_REQ_T   *rq, 
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
)
{
    RIO_TL_DS_T *tl_Data;
    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;

    dev_ID = get_Device_ID(tl_Data, rq->transp_Type);
        
    /* form proper transport info in case of the TABLE BASED routing algorythm */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( rq->transp_Type == RIO_PL_TRANSP_16 )
        {
            transport_Info <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
            transport_Info &=  RIO_TL_TR_INFO_16_DEST_MASK;
            transport_Info |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
			
        }
        else if ( rq->transp_Type == RIO_PL_TRANSP_32 )
        {
            transport_Info <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
            transport_Info &=  RIO_TL_TR_INFO_32_DEST_MASK;
            transport_Info |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
        }
    }
        
    return tl_Data->callback_Tray.rio_PL_DS_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            rq,
            trx_Tag,
            transport_Info
            );
}


/***************************************************************************/

/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#124 - Support for Data Streaming packet */


/* functions to be called by the physical layer to get data*/

/***************************************************************************
 * Function :    RIO_TL_Get_Request_Data_Hw
 *
 * Description:  PL retrieves data associated with the request
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Get_Request_Data_Hw(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_HW_T      *data_hw

)
{
    RIO_TL_DS_T *tl_Data;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_Get_Req_Data_Hw(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag,            
            offset,
            cnt,
            data_hw
            );

}

/***************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */






/* GDA: Bug#125 - Support for Flow control packet */

/***************************************************************************
 * Function :    RIO_TL_FC_Request
 *
 * Description:  requests FC transaction to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_FC_Request(
    RIO_HANDLE_T   handle, 
    RIO_FC_REQ_T   *rq, 
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
)
{
    RIO_TL_DS_T *tl_Data;
    RIO_TR_INFO_T        dev_ID;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
    dev_ID = get_Device_ID(tl_Data, rq->transp_Type);
        
    /* form proper transport info in case of the TABLE BASED routing algorythm */
    if ( tl_Data->init_Params.routing_Meth == RIO_TABLE_BASED_ROUTING )
    {
        if ( rq->transp_Type == RIO_PL_TRANSP_16 )
        {
            transport_Info <<= RIO_TL_TR_INFO_16_DEST_OFFSET;
            transport_Info &=  RIO_TL_TR_INFO_16_DEST_MASK;
            transport_Info |=  ( RIO_TL_TR_INFO_16_SRC_MASK & dev_ID );
        }
        else if ( rq->transp_Type == RIO_PL_TRANSP_32 )
        {
            transport_Info <<= RIO_TL_TR_INFO_32_DEST_OFFSET;
            transport_Info &=  RIO_TL_TR_INFO_32_DEST_MASK;
            transport_Info |=  ( RIO_TL_TR_INFO_32_SRC_MASK & dev_ID );
        }
    }
        
    return tl_Data->callback_Tray.rio_PL_FC_Request(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            rq,
            trx_Tag,
            transport_Info
            );
}


/* GDA: Bug#125 - Support for Flow control packet */


/***************************************************************************
 * Function :    RIO_TL_Congestion
 *
 * Description:  acknowledges LL request to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Congestion( RIO_HANDLE_T   handle, 
	               RIO_TAG_T      trx_Tag)

{
	
    RIO_TL_DS_T *tl_Data = NULL;

    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;

    return tl_Data->callback_Tray.rio_PL_Congestion(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_PL_Outbound_Context,
            trx_Tag
            );
}
/* GDA: Bug#125 - Support for Flow control packet */


/***************************************************************************
 * Function :    RIO_TL_FC_To_Send
 *
 * Description:  acknowledges LL request to PL
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_FC_To_Send(
    RIO_CONTEXT_T handle, 
    RIO_TAG_T     tag
)
{
    RIO_TL_DS_T *tl_Data;

#ifndef _CARBON_SRIO_XTOR
    printf("\n calling rio_tl_fc_to_send \n");
#endif
    if( handle != NULL )
        tl_Data = (RIO_TL_DS_T*)handle;
    else
        return RIO_PARAM_INVALID;
        
    return tl_Data->callback_Tray.rio_LL_FC_To_Send(
            (RIO_HANDLE_T*)tl_Data->callback_Tray.rio_LL_Interface_Context,
            tag         
            );
}
/* EOF */


