/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/tl/rio_tl_entry.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains layer entry functions, such as instantiation
*               initialization and callback tray exchange routines
*
* Notes:        
*
******************************************************************************/

#include "rio_tl_entry.h"

#include <stdlib.h>

/***************************************************************************
 * Function :    RIO_TL_Create_Instance
 *
 * Description:  this function creates instance of the TL.
 *
 * Returns:      RIO_OK, RIO_ERROR or RIO_PARAM_INVALID
 *
 * Notes:        this is the only function in the layer which allocates
 *               memory for the pointer passed as function parameter
 *
 **************************************************************************/
int RIO_TL_Create_Instance(
    RIO_HANDLE_T        *handle
)
{
    /* pointer to the data of the newly created instance */
    RIO_TL_DS_T        *tl_Data;

    /* check parameters */
    if ( handle == NULL)
        return RIO_PARAM_INVALID;

    /* allocate memory for the instance's data and initialize it */
    tl_Data = (RIO_TL_DS_T *)malloc(sizeof( RIO_TL_DS_T ));
    /* can't allocate memory */
    if ( tl_Data == NULL )
        return RIO_ERROR;

    /* reset LL instance */
    RIO_TL_Start_Reset( tl_Data );

    /* initialize some callbacks pointers */
    tl_Data->callback_Tray.rio_User_Msg = NULL;
    tl_Data->callback_Tray.rio_User_Msg   = NULL;
    tl_Data->callback_Tray.rio_Msg         = NULL;

    /* set routing method value */
    tl_Data->init_Params.routing_Meth      = RIO_TABLE_BASED_ROUTING;
    
    /* return value */
    *handle = (RIO_HANDLE_T)tl_Data;
    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_TL_Get_Function_Tray
 *
 * Description:  this function places pointers to the functions into 
 *               supplied ftray structure.
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Get_Function_Tray(
    RIO_TL_FTRAY_T *ftray
)
{
    /* check parameter */
    if ( ftray == NULL )
        return RIO_PARAM_INVALID;

    /* functions to access transport layer by the logical layer */
    ftray->rio_GSM_Request       = RIO_TL_GSM_Request;
    ftray->rio_IO_Request        = RIO_TL_IO_Request;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    ftray->rio_DS_Request        = RIO_TL_DS_Request;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    ftray->rio_Message_Request   = RIO_TL_Message_Request;
    ftray->rio_Doorbell_Request  = RIO_TL_Doorbell_Request;
    ftray->rio_Conf_Request      = RIO_TL_Config_Request;
    ftray->rio_Send_Response     = RIO_TL_Send_Response;
                                  
    ftray->rio_Ack_Remote_Req    = RIO_TL_Ack_Remote_Request;
    ftray->rio_Set_Config_Reg    = RIO_TL_Set_Config_Reg;
    ftray->rio_Get_Config_Reg    = RIO_TL_Get_Config_Reg;
    
    ftray->rio_Start_Reset       = RIO_TL_Start_Reset; 
    ftray->rio_Init              = RIO_TL_Initialize;

    /* functions to access transport layer from the physical layer */
    ftray->rio_Remote_Request    = RIO_TL_Remote_Request;
    ftray->rio_Remote_Responce   = RIO_TL_Remote_Response;

    ftray->rio_Remote_Conf_Read  = RIO_TL_Ext_Features_Read;
    ftray->rio_Remote_Conf_Write = RIO_TL_Ext_Features_Write;
                               
    ftray->rio_Ack_Request       = RIO_TL_Ack_Request;
    ftray->rio_Get_Req_Data      = RIO_TL_Get_Request_Data;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    ftray->rio_Get_Req_Data_Hw   = RIO_TL_Get_Request_Data_Hw; 
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    ftray->rio_FC_Request        = RIO_TL_FC_Request;
    ftray->rio_Congestion        = RIO_TL_Congestion;
    ftray->rio_FC_To_Send        = RIO_TL_FC_To_Send;
    /* GDA: Bug#125 */
    
    ftray->rio_Get_Resp_Data     = RIO_TL_Get_Response_Data;
    ftray->rio_Ack_Response      = RIO_TL_Ack_Responce;
    ftray->rio_Req_Time_Out      = RIO_TL_Request_Timeout;
    
    ftray->rio_TL_Delete_Instance = RIO_TL_Delete_Instance;

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_TL_Bind_Instance
 *
 * Description:  this function places supplied callback tray into interbal 
 *               TL data structure.
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Bind_Instance(
    RIO_HANDLE_T           handle, 
    RIO_TL_CALLBACK_TRAY_T *ctray
)
{
    RIO_TL_DS_T *tl_Data;

    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        tl_Data = (RIO_TL_DS_T*)handle;

    if ( ctray == NULL )
        return RIO_PARAM_INVALID;


    /* these parameters are stored at first to provide ability for the */
    /* layer to report about errors during instantiation               */
    tl_Data->callback_Tray.rio_User_Msg     = ctray->rio_User_Msg;
    tl_Data->callback_Tray.rio_Msg          = ctray->rio_Msg;

    /* callbacks provided by the physical layer */
    tl_Data->callback_Tray.rio_Set_Config_Reg           = ctray->rio_Set_Config_Reg; 
    tl_Data->callback_Tray.rio_Get_Config_Reg           = ctray->rio_Get_Config_Reg; 
    tl_Data->callback_Tray.rio_Config_Context           = ctray->rio_Config_Context; 

    tl_Data->callback_Tray.rio_PL_GSM_Request           = ctray->rio_PL_GSM_Request;
    tl_Data->callback_Tray.rio_PL_IO_Request            = ctray->rio_PL_IO_Request;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    tl_Data->callback_Tray.rio_PL_DS_Request            = ctray->rio_PL_DS_Request;         
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    /* GDA: Bug#125 - Support for Flow control packet */
    tl_Data->callback_Tray.rio_PL_FC_Request            = ctray->rio_PL_FC_Request;
    tl_Data->callback_Tray.rio_PL_Congestion            = ctray->rio_PL_Congestion;
    /* GDA: Bug#125  */

    tl_Data->callback_Tray.rio_PL_Message_Request       = ctray->rio_PL_Message_Request;
    tl_Data->callback_Tray.rio_PL_Doorbell_Request      = ctray->rio_PL_Doorbell_Request;
    tl_Data->callback_Tray.rio_PL_Configuration_Request = ctray->rio_PL_Configuration_Request;
    tl_Data->callback_Tray.rio_PL_Send_Response         = ctray->rio_PL_Send_Response;
    tl_Data->callback_Tray.rio_PL_Outbound_Context      = ctray->rio_PL_Outbound_Context;

    tl_Data->callback_Tray.rio_PL_Ack_Remote_Req        = ctray->rio_PL_Ack_Remote_Req;
    tl_Data->callback_Tray.rio_PL_Inbound_Context       = ctray->rio_PL_Inbound_Context;

    /* callbacks provided by the logical layer */         
    tl_Data->callback_Tray.rio_LL_Ack_Request           = ctray->rio_LL_Ack_Request;
    tl_Data->callback_Tray.rio_LL_Get_Req_Data          = ctray->rio_LL_Get_Req_Data;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    tl_Data->callback_Tray.rio_LL_Get_Req_Data_Hw       = ctray->rio_LL_Get_Req_Data_Hw;  
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    /* GDA: Bug#125 - Support for Flow control packet */
    tl_Data->callback_Tray.rio_LL_FC_To_Send           = ctray->rio_LL_FC_To_Send;
    /* GDA: Bug#125 */

    tl_Data->callback_Tray.rio_LL_Get_Resp_Data         = ctray->rio_LL_Get_Resp_Data;
    tl_Data->callback_Tray.rio_LL_Ack_Response          = ctray->rio_LL_Ack_Response;
    tl_Data->callback_Tray.rio_LL_Req_Time_Out          = ctray->rio_LL_Req_Time_Out;
    tl_Data->callback_Tray.rio_LL_Remote_Request        = ctray->rio_LL_Remote_Request;
    tl_Data->callback_Tray.rio_LL_Remote_Response       = ctray->rio_LL_Remote_Response;
    tl_Data->callback_Tray.rio_LL_Interface_Context     = ctray->rio_LL_Interface_Context;
    tl_Data->callback_Tray.rio_LL_Remote_Conf_Read      = ctray->rio_LL_Remote_Conf_Read;
    tl_Data->callback_Tray.rio_LL_Remote_Conf_Write     = ctray->rio_LL_Remote_Conf_Write;
    tl_Data->callback_Tray.extended_Features_Context    = ctray->extended_Features_Context;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_TL_Initialize
 *
 * Description:  function initializes LL instance accordingly to supplied 
 *               parameters set
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Initialize(
    RIO_HANDLE_T              handle,
    RIO_TL_PARAM_SET_T        *param_Set
)
{
    RIO_TL_DS_T    *tl_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        tl_Data = (RIO_TL_DS_T*)handle;

    if ( param_Set == NULL )
        return RIO_PARAM_INVALID;

    /* check that LL was reseted before initialization */
    if ( !tl_Data->in_Reset )
    {
        /* issue error message */
        if ( tl_Data->callback_Tray.rio_User_Msg != NULL)
            tl_Data->callback_Tray.rio_User_Msg(
                tl_Data->callback_Tray.rio_Msg,
                "Error: Transport Layer initialize function was invoked outside reset state.\n"
            );

        return RIO_ERROR;
    }

    /* check supplied routing method */
    if ( param_Set->routing_Meth != RIO_INDEX_BASED_ROUTING && 
        param_Set->routing_Meth != RIO_SHIFT_BASED_ROUTING &&
        param_Set->routing_Meth != RIO_TABLE_BASED_ROUTING )
    {
        /* issue error message */
        if ( tl_Data->callback_Tray.rio_User_Msg != NULL)
            tl_Data->callback_Tray.rio_User_Msg(
                tl_Data->callback_Tray.rio_Msg,
                "Error: Transport Layer got wrong routing method.\n"
            );

        return RIO_PARAM_INVALID;
    }
    
    /* copy parameters into instance data structure */
    tl_Data->init_Params.routing_Meth  = param_Set->routing_Meth;

    /* reset reset flag */
    tl_Data->in_Reset = RIO_FALSE;

    if (param_Set->transp_Type != RIO_PL_TRANSP_16 &&
        param_Set->transp_Type != RIO_PL_TRANSP_32)
            return RIO_PARAM_INVALID;
    tl_Data->init_Params.transp_Type = param_Set->transp_Type;

    return RIO_OK;

}

/***************************************************************************
 * Function :    RIO_TL_Start_Reset
 *
 * Description:  this function simply sets reset flag, so nothing can occurs
 *               afer this before initialization
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_TL_Start_Reset(
    RIO_HANDLE_T handle
)
{
    RIO_TL_DS_T *tl_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        tl_Data = (RIO_TL_DS_T*)handle;

    tl_Data->in_Reset = RIO_TRUE;

    return RIO_OK;
}    


/***************************************************************************
 * Function :    RIO_TL_Delete_Instance
 *
 * Description:  
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/

int RIO_TL_Delete_Instance(
    RIO_HANDLE_T handle
)
{
    RIO_TL_DS_T *tl_Data = (RIO_TL_DS_T *)handle;
    
    if( tl_Data == NULL )
        return RIO_ERROR;
        
        
        
    free( tl_Data );
    
    return RIO_OK;
}

/* EOF */

