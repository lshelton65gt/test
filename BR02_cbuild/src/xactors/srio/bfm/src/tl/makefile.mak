#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/tl/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds relocatable object file for transport layer model
#
include ../../verilog_env/demo/debug.inc


CC              =   gcc
TARGET          =   tl.o
PROFTARGET	=   tl_prof.o

DEBUG		=   -DNDEBUG

DEFINES         =   $(_DEBUG)

LIBS            =

INCLUDES        =   -I./include -I../interfaces/common

CFLAGS          =   -c $(DEFINES) -g -elf -ansi -pedantic -Wall
LFLAGS          =   -r

OBJS		=	rio_tl.o rio_tl_entry.o\

###############################################################################
# make default target
all :   tl

# make target
$(TARGET) :	$(OBJS)
		ld  $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

#clean data
clean :		
		rm -f *.o
		rm -f $(TARGET)

#make ll relocatable object
tl :		$(TARGET)

#make tl relocatable object with the profiler support
prof :
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_tl.o rio_tl.c
		$(CC) $(CFLAGS) -pg $(INCLUDES) -o rio_tl_entry.o rio_tl_entry.c
		ld  $(LFLAGS) $(OBJS) $(LIBS) -o $(PROFTARGET)

%.o :		%.c
		$(CC) $(CFLAGS) $(INCLUDES) -o $*.o $<

###############################################################################
             
