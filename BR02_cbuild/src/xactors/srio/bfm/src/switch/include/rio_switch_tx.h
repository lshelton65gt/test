#ifndef RIO_SWITCH_TX_H
#define RIO_SWITCH_TX_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/include/rio_switch_tx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  switch model transmitter declaration file
*
* Notes:
*
******************************************************************************/

/* a link needs in a granule for transmitting */
void RIO_Switch_Tx_Granule_For_Transmitting(
    RIO_SWITCH_DS_T *handle,
    RIO_GRANULE_T   *granule,
    unsigned int    link_Number);

/* initialize the transmitter*/
void RIO_Switch_Tx_Init(RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* load acknowledge for sending */
void RIO_Switch_Tx_Send_Ack(
    RIO_SWITCH_DS_T *handle, unsigned int link_Number, RIO_GRANULE_T *retry);

/* send STOMP to the receiver */
void RIO_Switch_Tx_STOMP_Transmission(
    RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* send link response */
void RIO_Switch_Tx_Send_Link_Resp(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Response, unsigned int link_Number);

/* receved LINK RESPONSE */
void RIO_Switch_Tx_Rec_Link_Resp(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Response, unsigned int link_Number);

/* receved LINK REQUEST */
void RIO_Switch_Tx_Rec_Link_Req(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Request, unsigned int link_Number);

/* get LINK_REQUEST control*/
void RIO_Switch_Tx_Get_Link_Request(RIO_GRANULE_T *granule);

/* send an acknowledge */
void RIO_Switch_Tx_Rec_Ack(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *ack_Granule, unsigned int link_Number);

/* send LINK_REQUEST */
void RIO_Switch_Tx_Send_Link_Req(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Request, unsigned int link_Number);
/* a Training pattern shall be sent*/
void RIO_Switch_Tx_Send_Training(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Request, unsigned int num_Trainings, unsigned int link_Number);

/* time-out proceding */
void RIO_Switch_Tx_Link_Req_Time_Out(RIO_SWITCH_DS_T *handle);

/* time-out proceding */
void RIO_Switch_Tx_Link_Req_Training_Time_Out(RIO_SWITCH_DS_T *handle);

/* tries routing a THROTTLE */
void RIO_Switch_Tx_Rec_Throttle(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/*receiving training sequence */
void RIO_Switch_Tx_Rec_Training_Seq(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Response, unsigned int link_Number);

/*check if outstanding ack exist, and if no - rec new ack in WAIT state*/
int RIO_Switch_Tx_Start_Packet_Ack(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *retry, unsigned int link_Number);

RIO_BOOL_T RIO_Switch_Tx_Control_Is_Expected(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

void RIO_Switch_Tx_Rec_Unexp_Packet_Control(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

int RIO_Switch_Get_Our_Buf_Status(RIO_SWITCH_DS_T *handle, unsigned int link_Number);
int RIO_Switch_Get_Partner_Buf_Status(RIO_SWITCH_DS_T *handle, unsigned int link_Number);

void RIO_Switch_Tx_Phys_Time_Out(RIO_SWITCH_DS_T *handle);

/****************************************************************************/
#endif /* RIO_SWITCH_TX_H */

