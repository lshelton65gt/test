#ifndef RIO_SWITCH_REGISTERS_H
#define RIO_SWITCH_REGISTERS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/include/rio_switch_registers.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  switch registers function prototypes
*               and data 
* Notes:
*
******************************************************************************/

/*register map boundaries*/
#define RIO_EF_SPACE_BOTTOM   (unsigned long) 0x0100
#define RIO_EF_SPACE_TOP      (unsigned long) 0xFFF8
                              
#define RIO_ID_SPACE_BOTTOM   (unsigned long) 0x010000
#define RIO_ID_SPACE_TOP      (unsigned long) 0xFFFFF8

/*
 * rio registers addresses
 */
#define RIO_DEV_IDS_A         0x00
#define RIO_DEV_INFO_A        0x04
                              
#define RIO_ASS_IDS_A         0x08
#define RIO_ASS_INFO_A        0x0c
                              
#define RIO_PE_FEAT_A         0x10
#define RIO_SWITCH_INFO_A     0x14


/*transport registers*/
#define RIO_COMPONENT_TAG_A        0x6c

#define RIO_PORT_MAINT_HEADER_0_A  0x0000
#define RIO_PORT_MAINT_HEADER_1_A  0x0004

/* LP-EP registers */
#define RIO_LPEP_REG_GEN_SIZE      0x40
#define RIO_LPEP_REG_INDEX         0x20

/*common*/
#define RIO_LINK_VALID_TO1_A       0x0020

#define RIO_LINK_GEN_CONTROL_A     0x003c

/*for each port*/
#define RIO_LINK_ERROR_A           0x0058
#define RIO_LINK_CONTROL_A         0x005c

#define RIO_LINK_VALID_REQ_A        0x0040
#define RIO_LINK_VALID_RESP_A        0x0044
#define RIO_LINK_ACKID_STATUS_A        0x0048
#define RIO_LINK_BUF_STATUS_A        0x004c /*for internal purpose - buf_status indicator: input - 12-15 bits, output - 28-31 bits*/


int RIO_Switch_Write_Register(
    RIO_SWITCH_DS_T   *handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     data,
    RIO_BOOL_T        interanal
    );

int RIO_Switch_Read_Register(
    RIO_SWITCH_DS_T   *handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     *data,
    RIO_BOOL_T        internal
    );

int RIO_Switch_Set_State(
    RIO_SWITCH_DS_T              *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Param,
    unsigned int             link_Number
    );

int RIO_Switch_Get_State(
    RIO_SWITCH_DS_T              *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param,
    unsigned int             link_Number
    );

int RIO_Switch_Init_Regs(RIO_SWITCH_DS_T *handle);

int RIO_Switch_Link_Init_Regs(RIO_SWITCH_DS_T *handle, unsigned int link_Number);



unsigned int RIO_Switch_Conv_Size_To_Dw_Cnt(unsigned int size,  unsigned int wdptr);

/*****************************************************************************/
#endif /* RIO_SWITCH_REGISTERS_H*/
