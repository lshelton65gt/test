#ifndef RIO_SWITCH_ERRORS_H
#define RIO_SWITCH_ERRORS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/include/rio_switch_errors.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  switch model errors/warnings handlers prototypes
*               and data 
* Notes:
*
******************************************************************************/

#ifndef RIO_SWITCH_DS_H
#include "rio_switch_ds.h"
#endif

/*
 * this type syblically represent error codes which are used as
 * indexes in error messages array
 */
typedef enum {
    RIO_SWITCH_ERROR_1 = 0,
    RIO_SWITCH_WARNING_1, 
    RIO_SWITCH_NOTE_1
} RIO_SWITCH_MESSAGES_T;


/*
 * this function write an warning message with corresponding explaning
 * based on instanse's state and a set of additional parameters
 */
int RIO_Switch_Print_Message(RIO_SWITCH_DS_T* handle, RIO_SWITCH_MESSAGES_T message_Type, ...);


/****************************************************************************/
#endif /* RIO_SWITCH_ERRORS_H */

