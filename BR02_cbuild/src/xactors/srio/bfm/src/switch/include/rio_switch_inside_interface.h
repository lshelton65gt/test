#ifndef RIO_SWITCH_INSIDE_INTERFACE_H
#define RIO_SWITCH_INSIDE_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/include/rio_switch_inside_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  File contains internal function interface of the RapidIO Switch
*               model
*
* Notes:        
*
******************************************************************************/




#include "rio_types.h"




/* 
 * the function is used for enabling/disabling the link Transmitter.
 * The function invokation set tx mode - 
 * if transmitter drives the pins or is switched off.
 */
typedef int (*RIO_SWITCH_II_LINK_TX_DISABLING)(
    RIO_HANDLE_T handle,     /* handle of instance*/
    RIO_BOOL_T   tx_Disable /* parameters */
    );

/* 
 * the function is used for enabling/disabling the link Receiver.
 * The function invokation set rx mode - 
 * if receiver drives the pins or is switched off.
 */
typedef int (*RIO_SWITCH_II_LINK_RX_DISABLING)(
    RIO_HANDLE_T handle,     /* handle of instance*/
    RIO_BOOL_T   rx_Disable /* parameters */
    );

/*
 * This function is used for the RapidIO Switch model initialization
 * in case of user initialization as well as in case of four LR/RESET
 * control symbols are received in row. Pointer to the function is
 * stored in Switch internal FTray and separate function implementatiosn
 * exist for parallel and serial Switch models
 */
typedef int (*RIO_SWITCH_INTERNAL_INIT)(RIO_HANDLE_T handle);

typedef int (*RIO_SWITCH_LINK_INTERNAL_INIT)(RIO_HANDLE_T handle, unsigned int link_Number);

typedef int (*RIO_SWITCH_LINK_START_RESET)(RIO_HANDLE_T handle, unsigned int link_Number);

#endif /* RIO_SWITCH_INSIDE_INTERFACE_H */

