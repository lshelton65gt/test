#ifndef RIO_SWITCH_DS_H
#define RIO_SWITCH_DS_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\switch\include\rio_switch_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                       to display revision history information.
*
* Description:  switch model data structure declaration file
*
* Notes:
*
******************************************************************************/


#include "rio_gran_type.h"
#include "rio_switch_inside_interface.h"
#include "rio_switch_interface.h"
/*#include "rio_32_txrx_model.h"*/
#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif

#include "rio_parallel_types.h"


#include "rio_switch_inside_interface.h"




/*
 * this register describe type of allowed access to register
 */
typedef enum {
    RIO_REG_READ_ONLY = 0,
    RIO_REG_WRITE_ONLY,
    RIO_REG_READ_WRITE,
    RIO_REG_INTERNAL_ONLY
} RIO_REG_TYPE_T;


/*
 * this structure represents a register as an entity
 */
typedef enum {
    RIO_SWITCH_ORD_REG_KEY = 0,
    RIO_SWITCH_LINK_VALID_REQ_KEY, 
    RIO_SWITCH_LINK_VALID_RESP_KEY,
    RIO_SWITCH_TIME_OUT_REG_1_KEY,
    RIO_SWITCH_TIME_OUT_REG_2_KEY,
    RIO_SWITCH_ERROR_STATUS_REG_KEY,
    RIO_SWITCH_CONTROL_REG_KEY,
    RIO_SWITCH_BUF_STATUS_REG_KEY,
    RIO_SWITCH_ACKID_STATUS_REG_KEY
} RIO_REG_KEY_T;


typedef struct{
    unsigned long     reg_Value;
    unsigned long     reg_Mask;
    unsigned long     reg_Offset;
    RIO_REG_TYPE_T    access_Type;
    RIO_REG_KEY_T     key;
    unsigned int      link_Number;
} RIO_SWITCH_REG_T;


/*
 * this is a set of common register 
 */
typedef struct {

    RIO_SWITCH_REG_T device_Ids;
    RIO_SWITCH_REG_T device_Info;

    RIO_SWITCH_REG_T assembly_Ids;
    RIO_SWITCH_REG_T assembly_Info;

    RIO_SWITCH_REG_T pe_Features;
    RIO_SWITCH_REG_T switch_Info;

    RIO_SWITCH_REG_T component_Tag;

    RIO_SWITCH_REG_T port_Maint_Header_0;
    RIO_SWITCH_REG_T port_Maint_Header_1;

    RIO_SWITCH_REG_T valid_Time_Out_1;
    RIO_SWITCH_REG_T port_General_Control;

} RIO_SWITCH_COMMON_REGS_T;


/*
 * this is a set of LP-EP specific registers
 */
typedef struct{

    unsigned long     reg_Value;
    unsigned long     reg_Mask;
    unsigned long     reg_Offset;
    RIO_REG_TYPE_T    access_Type;

} RIO_SWITCH_LPEP_REG_T;


typedef struct {

    RIO_SWITCH_REG_T port_Error_Status;
    RIO_SWITCH_REG_T port_Control;
    RIO_SWITCH_REG_T port_Buf_Status;

    RIO_SWITCH_REG_T link_Valid_Req;
    RIO_SWITCH_REG_T link_Valid_Resp;
    RIO_SWITCH_REG_T link_AckID_Status; /*new*/ 

} RIO_SWITCH_LPEP_REGS_T;


/*
 * this type defines all PE registers
 */
typedef struct {
    unsigned int             lpep_Size;
    RIO_SWITCH_COMMON_REGS_T common_Regs;
    RIO_SWITCH_LPEP_REGS_T   *lpep_Regs;
} RIO_SWITCH_REGS_T;

typedef struct {
    unsigned int         output_Error_Type;
    unsigned int         input_Error_Type;
    unsigned int         lane_With_Invalid_Character;  /* number of the lane where invalid character occured */
    RIO_BOOL_T           is_Port_Mode_4x; /* RIO_TRUE if port is working in 4x mode */
    unsigned int         byte_In_Rcv_Granule_Cur_Num;
    unsigned int         byte_In_Trx_Granule_Cur_Num;
    unsigned int         are_All_Idles_In_Granule;
    RIO_BOOL_T           is_Packet_Delimiter;
    RIO_BOOL_T           is_Align_Lost;
    RIO_BOOL_T           is_Sync_Lost;
    RIO_BOOL_T           is_Character_Invalid;
    RIO_BOOL_T           copy_To_Lane2;
    RIO_BOOL_T           suppress_Hook;
    RIO_UCHAR_T          initial_Run_Disp;
    unsigned int         decode_Any_Run_Disp;
    RIO_BOOL_T           lane2_Just_Forced_By_Discovery;
    RIO_BOOL_T           rx_Packet_Is_Ongoing;
} RIO_SWITCH_STATE_INDICATORS_PARAMS_T;


/* maximum count of LP/EP interfaces in a switch*/
#define RIO_SWITCH_MAX_LINK_CNT 255


/* the type represent link's state */
typedef enum {

    RIO_SWITCH_LINK_FREE = 0,
    RIO_SWITCH_LINK_WORK,
    RIO_SWITCH_LINK_BLOCKED

} RIO_SWITCH_LINK_STATE_T;


typedef enum {

    RIO_SWITCH_LINK_OK = 0,
    RIO_SWITCH_LINK_STOPPED_DUE_TO_ERROR = 1,
    RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY = 2,
    RIO_SWITCH_LINK_ERROR,
    RIO_SWITCH_LINK_UNINIT,
    RIO_SWITCH_LINK_INITIALIZING

} RIO_SWITCH_LINK_GLOBAL_STATE_T;

typedef enum {
        RIO_SWITCH_RESET_RESET = 0,
        RIO_SWITCH_RESET_OK,
        RIO_SWITCH_RESET_PRE_RESET,
        RIO_SWITCH_RESET_POST_RESET
} RIO_SWITCH_RESET_MACHINE_STATE_T;


/* a receiver has a buffer which is big enough to hold the biggest packet */
#define RIO_SWITCH_BUFFER_SIZE 276

typedef struct {

    RIO_UCHAR_T    buffer[RIO_SWITCH_BUFFER_SIZE];
    unsigned int   buffer_Head;
    unsigned int   buffer_Top;
    unsigned int   buffer_Top_For_Load;
    unsigned short counting_CRC;
    unsigned int   crc_Pos;
    RIO_BOOL_T     need_Recalc_CRC;

} RIO_SWITCH_PACKET_BUFFER_T;


/*
 * a control for representation RESET latch
 */
typedef struct {
    unsigned int   reset_Num; 
    RIO_BOOL_T     valid;
} RIO_SWITCH_RESET_REQ_LATCH_T;


/* this type represents a link's data */
typedef struct {

    RIO_BOOL_T                          idle_watch;
    RIO_SWITCH_LINK_STATE_T             state; 
    RIO_SWITCH_LINK_GLOBAL_STATE_T      global_State;
    RIO_SWITCH_RESET_MACHINE_STATE_T    reset_Machine_State;
    int                                 connected_To;
    unsigned int                        expected_Ack_ID;
    RIO_SWITCH_PACKET_BUFFER_T          buffer;
    RIO_BOOL_T                          conf_Req;
    unsigned int                        waiting_Time;
    RIO_BOOL_T                          initialized;
    RIO_SWITCH_RESET_REQ_LATCH_T        reset_Req_Counter;
    RIO_BOOL_T                          pins_Disabled;

    /***new added for error recovery**/
    RIO_BOOL_T                          in_Progress; /*init mb RIO_FALSE*/
} RIO_SWITCH_LINK_RX_DS_T;


/* pool of acknowledges for sending */
#define RIO_SWITCH_TX_ACK_POOL_MAX_SIZE	32


typedef enum {
    RIO_SWITCH_ACK_POOL_ELEM_FREE = 0,
    RIO_SWITCH_ACK_POOL_ELEM_WAIT,
    RIO_SWITCH_ACK_POOL_ELEM_FULL
} RIO_SWITCH_ACK_POOL_ELEM_STATE_T;


typedef struct {
    RIO_GRANULE_T                    ack_Granule;
    RIO_SWITCH_ACK_POOL_ELEM_STATE_T state;
} RIO_SWITCH_TX_ACK_POOL_ELEM_T;


typedef struct {
    RIO_SWITCH_TX_ACK_POOL_ELEM_T pool[RIO_SWITCH_TX_ACK_POOL_MAX_SIZE];
    unsigned int                  pool_Head;
    unsigned int                  pool_Top;
    RIO_BOOL_T                    is_Full;
} RIO_SWITCH_TX_ACK_POOL_T;


/*
 * a control for representation link request latch
 */
typedef struct {
    RIO_GRANULE_T               control; 
    RIO_BOOL_T                  valid;
    RIO_BOOL_T                  is_Sent;
    unsigned long               time_After_Sending;
    unsigned int                retry_Cnt;
} RIO_SWITCH_LINK_REQUEST_LATCH_T;

/*
 * a control for representation link request latch
 */
typedef struct {
    RIO_BOOL_T                  valid;
    unsigned long               time_After_Sending;
} RIO_SWITCH_ACK_TIME_OUT_LATCH_T;


/*
 * a control for representation link response latch
 */
typedef struct {
    RIO_GRANULE_T  control; 
    RIO_BOOL_T     valid;
    unsigned int   last_Ack; /* last ack shell ce sent before */
} RIO_SWITCH_LINK_RESPONSE_LATCH_T;


/* latch to store restart from retry */
typedef struct {
    RIO_GRANULE_T control;
    RIO_BOOL_T    valid;
} RIO_SWITCH_REST_F_RETRY_LATCH_T;


/*
 * a control for representation THROTTLE  latch
 */
typedef struct {
    RIO_GRANULE_T  control; 
    RIO_BOOL_T     valid;
} RIO_SWITCH_THROT_FOR_SEND_LATCH_T;


/*
 * a control for representation THROTTLE generator
 */
typedef struct {
    unsigned int   idle_Cnt; 
    RIO_BOOL_T     valid;
} RIO_SWITCH_THROTTLE_LATCH_T;


/*
 * a control for representation idle generator
 */
typedef struct {
    unsigned int   idle_Cnt; 
    RIO_BOOL_T     valid;
} RIO_SWITCH_IDLE_LATCH_T;


/* transmitter data */
typedef struct {

    RIO_BOOL_T                          idle_watch;
    RIO_SWITCH_LINK_STATE_T             state; 
    RIO_SWITCH_LINK_GLOBAL_STATE_T      global_State;
    int                                 connected_To;
    unsigned int                        expected_Ack_ID;
    RIO_SWITCH_ACK_TIME_OUT_LATCH_T     ack_Time_Out;
    RIO_SWITCH_TX_ACK_POOL_T            ack_Pool;
    RIO_BOOL_T                          need_STOMP;
    RIO_SWITCH_LINK_REQUEST_LATCH_T     link_Request;
    RIO_SWITCH_LINK_RESPONSE_LATCH_T    link_Response;
    RIO_SWITCH_REST_F_RETRY_LATCH_T     restart_From_Retry;
    unsigned int                        last_Worked_Link;
    RIO_BOOL_T                          conf_Req;
    unsigned long                       lresp_Time_Out;
    RIO_SWITCH_THROT_FOR_SEND_LATCH_T   throttle_For_Send;
    RIO_SWITCH_THROTTLE_LATCH_T         throttle_Gen;
    RIO_SWITCH_IDLE_LATCH_T             idle_Generator;
    RIO_BOOL_T                          pins_Disabled;

    RIO_BOOL_T                          in_Progress; /*init mb RIO_FALSE*/
} RIO_SWITCH_LINK_TX_DS_T;


typedef struct {

    RIO_SWITCH_LINK_RX_DS_T rx_Data;
    RIO_SWITCH_LINK_TX_DS_T tx_Data;

    RIO_BOOL_T  in_Reset; /*indicates if link is in reset state*/
} RIO_SWITCH_LINK_DS_T;


/* 
 * this structure represents a set of common switch callbacks 
 * for parallel and serial models 
 */
typedef struct {

    RIO_USER_MSG    rio_User_Msg;
    RIO_CONTEXT_T   rio_Msg_Context;

} RIO_SWITCH_INTERNAL_CALLBACK_TRAY_T;


/* 
 * function tray to be used internally 
 */
typedef struct {

    RIO_SWITCH_II_LINK_TX_DISABLING rio_Link_Tx_Disabling;
    RIO_SWITCH_II_LINK_RX_DISABLING rio_Link_Rx_Disabling;

    RIO_SWITCH_INTERNAL_INIT        rio_Switch_Internal_Init;
    RIO_SWITCH_START_RESET          rio_Switch_Start_Reset;

    RIO_SWITCH_LINK_INTERNAL_INIT   rio_Switch_Link_Internal_Init;
    RIO_SWITCH_LINK_START_RESET     rio_Switch_Link_Start_Reset; 

} RIO_SWITCH_INTERNAL_FTRAY_T;


typedef struct {

    RIO_ROUTING_METHOD_T    rout_Alg; 

    RIO_UCHAR_T   port_Enc_Size;

    RIO_ROUTE_TABLE_ENTRY_T *routing_Table;
    unsigned int            rout_Table_Size; /* size of routing table */

    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */

} RIO_SWITCH_COMMON_PARAM_SET_T;


typedef struct {

    /* parallel parameters */
    RIO_BOOL_T      lp_Ep_Is_8;
    RIO_BOOL_T      work_Mode_Is_8;

    RIO_BOOL_T      requires_Window_Alignment;
    unsigned int    num_Trainings;

    /* serial parameters */
    RIO_BOOL_T      lp_Serial_Is_1x; 
    RIO_BOOL_T      is_Force_1x_Mode; 
    RIO_BOOL_T      is_Force_1x_Mode_Lane_0; 
    RIO_BOOL_T      transmit_Flow_Control_Support;
    RIO_BOOL_T      only_Maint_Requests;
    RIO_BOOL_T      only_Maint_Responses;

    unsigned int    discovery_Period;   
    unsigned int    silence_Period;   
    unsigned int    comp_Seq_Rate;   /*in spec - 5000*/
    unsigned int    status_Sym_Rate; /*in spec - 1024 code groups*/
    unsigned int    comp_Seq_Rate_Check; /*in spec - 5000*/
    unsigned int    icounter_Max;  /*max value of Icounter for switching sync FSM 
                                    to the NO_SYNC state in spec - 2*/

    /* the size of compensation sequence 
    if comp_Seq_Size set to 0 => "KRRR" sequence will be generated 
    if comp_Seq_Size set to 1 => "KRR" or "KRRRR" sequence will be generated 
    with random size. if comp_Seq_Size set to 2 => "KR" or "KRRRRR" sequence 
    will be generated with random size. 
    Set up the value to 0, 1 or 2. */
    unsigned int    comp_Seq_Size;


} RIO_SWITCH_LINK_COMMON_PARAM_SET_T;


/*
 * this type represent physical layer data structure, which contains its
 * internal state, register pool, buffers, callback tray, etc
 */
typedef struct {
    unsigned long               instance_Number;

    RIO_BOOL_T                  is_Parallel_Model;
    unsigned int                ack_Buffer_Size;

    RIO_BOOL_T                  in_Reset; /* flag that switch is in reset mode */

    RIO_HANDLE_T                link_Handles[RIO_SWITCH_MAX_LINK_CNT]; /* handles of Link models */
    RIO_SWITCH_LINK_DS_T        link_Data[RIO_SWITCH_MAX_LINK_CNT];
    RIO_HANDLE_T                link_Ftray;                           /* link model function tray */

    RIO_HANDLE_T                mid_Block_Handles[RIO_SWITCH_MAX_LINK_CNT]; /* middle block instance handles */
    RIO_HANDLE_T                mid_Block_Ftray;                            /* middle block function tray    */

    RIO_SWITCH_INTERNAL_CALLBACK_TRAY_T switch_Callbacks;
    RIO_HANDLE_T                        link_Specific_Callbacks;

    RIO_SWITCH_INTERNAL_FTRAY_T         internal_Ftray;

    RIO_SWITCH_REGS_T                       reg_Set;
    RIO_SWITCH_STATE_INDICATORS_PARAMS_T    state_Indicators_Params[RIO_SWITCH_MAX_LINK_CNT];

    RIO_SWITCH_PACKET_BUFFER_T          resp_Buffer[RIO_SWITCH_MAX_LINK_CNT];

    RIO_SWITCH_INST_PARAM_T             inst_Param_Set;
    RIO_SWITCH_COMMON_PARAM_SET_T       init_Param_Set;
    RIO_SWITCH_LINK_COMMON_PARAM_SET_T  link_Common_Params[RIO_SWITCH_MAX_LINK_CNT];
} RIO_SWITCH_DS_T;

/***************************************************************************
 * Function : RIO_SWITCH_GET_LNK_DATA
 *
 * Description: return pointer to n-link data 
 *
 * Returns: address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_SWITCH_GET_LNK_DATA(handle, i) ((handle)->link_Data + (i))

/***************************************************************************
 * Function : RIO_SWITCH_GET_LNK_RX_DATA
 *
 * Description: return n-link receiver data 
 *
 * Returns: rx_Data member
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_SWITCH_GET_LNK_RX_DATA(handle, i) (((handle)->link_Data + (i))->rx_Data)

/***************************************************************************
 * Function : RIO_SWITCH_GET_LNK_TX_DATA
 *
 * Description: return n-link data transmitter
 *
 * Returns: tx_Data member
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_SWITCH_GET_LNK_TX_DATA(handle, i) (((handle)->link_Data + (i))->tx_Data)

/***************************************************************************
 * Function : RIO_SWITCH_GET_PORTS_CNT
 *
 * Description: return count of instantiated links 
 *
 * Returns: value
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_SWITCH_GET_PORTS_CNT(handle) ((handle)->inst_Param_Set.link_Number)

/***************************************************************************
 * Function : RIO_SWITCH_GET_ENC_SIZE
 *
 * Description: return port encoding size 
 *
 * Returns: value
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_SWITCH_GET_ENC_SIZE(handle) ((handle)->init_Param_Set.port_Enc_Size)


#define RIO_SWITCH_INIT_CRC             0xffff
#define RIO_SWITCH_GRAN_WITH_CRC        20
#define RIO_SWITCH_CNT_BYTE_IN_GRAN     4
#define RIO_SWITCH_INT_CRC_POS          80
#define RIO_SWITCH_PACK_NEED_INT_CRC    84
#define RIO_SWITCH_NOT_CONNECTED        -1
#define RIO_SWITCH_TR_TYPE_POS_LOW      4
#define RIO_SWITCH_TR_TYPE_POS_HIGH     6
#define RIO_SWITCH_CNT_BYTES_DWORD      8
#define RIO_SWITCH_MAX_CONF_PAY_SIZE    8
#define RIO_SWITCH_MAX_RDSIZE           12
#define RIO_SWITCH_MIN_RDSIZE           8
#define RIO_SWITCH_TRAIN_NUM            256/*number of training simbols in training sequence*/
#define RIO_SWITCH_BYTE_CNT_IN_WORD     4
#define RIO_SWITCH_BITS_IN_BYTE         8
/*registers constants*/
/*error and status register*/
#define RIO_SWITCH_ERROR_STATE_CLEAR_MASK       0x00120204 /* mask for clearing state in register */
#define RIO_SWITCH_ERROR_PORT_STATE_CLEAR_MASK  0xfffffffc /* mask for clearing common state in register(without present bit)*/
#define RIO_SWITCH_ERROR_STATE_UNINIT           0x00000001 /* port is uninitialized */
#define RIO_SWITCH_ERROR_STATE_OK               0x00000002 /* port is initialized */
#define RIO_SWITCH_ERROR_STATE_UNREC_ERROR      0x00000004 /* port is is in unrecoverable error*/
#define RIO_SWITCH_ERROR_STATE_PRESENT          0x00000008 /* port is present */

#define RIO_SWITCH_ERROR_TX_STATE_CLEAR_MASK    0xfff2ffff /* mask for clearing tx state in register */
#define RIO_SWITCH_ERROR_RX_STATE_CLEAR_MASK    0xfffffaff /* mask for clearing tx state in register */

#define RIO_SWITCH_ERROR_TX_ERROR               0x00030000 /*recoverable error was detected*/
#define RIO_SWITCH_ERROR_TX_RETRY               0x001c0000 /*retry state was detected*/
                                                
#define RIO_SWITCH_ERROR_RX_ERROR               0x00000300 /*recoverable error was detected*/
#define RIO_SWITCH_ERROR_RX_RETRY               0x00000400 /*retry state was detected*/






/*these constants are necessary to check packet header size - in bits count*/
#define RIO_SWITCH_FIRST_16_BIT_SIZE       16
#define RIO_SWITCH_TR_INFO_SIZE_16         16
#define RIO_SWITCH_TR_INFO_SIZE_32         32
#define RIO_SWITCH_EXT_ADDR_SIZE_16        16
#define RIO_SWITCH_EXT_ADDR_SIZE_32        32
#define RIO_SWITCH_CRC_FIELD_SIZE          16 /*size of one CRC field*/


/*all packet header sizes not includes first 16 packet bits,
 *size of transport info, extended address fields
 */
#define RIO_SWITCH_PACK_8_HEADER_SIZE      48 /*include hop count*/
#define RIO_SWITCH_PACK_13_HEADER_SIZE     16
#define RIO_SWITCH_PACK_2_HEADER_SIZE      48
#define RIO_SWITCH_PACK_5_HEADER_SIZE      48
#define RIO_SWITCH_PACK_6_HEADER_SIZE      32
#define RIO_SWITCH_PACK_10_HEADER_SIZE     32
#define RIO_SWITCH_PACK_11_HEADER_SIZE     16
#define RIO_SWITCH_PACK_1_HEADER_SIZE      64

/*bit size of reserved packet fields*/
#define RIO_SWITCH_PACK_6_RSRV_FIELD_SIZE          1       
#define RIO_SWITCH_PACK_8_REQ_RSRV_FIELD_SIZE      2    
#define RIO_SWITCH_PACK_8_RESP_RSRV_FIELD_SIZE     24
#define RIO_SWITCH_PACK_10_RSRV_FIELD_SIZE         8


/****************************************************************************/
#endif /* RIO_SWITCH_DS_H */
