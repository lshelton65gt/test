#ifndef RIO_SWITCH_MODEL_COMMON_H
#define RIO_SWITCH_MODEL_COMMON_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/include/rio_switch_model_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  RapidIO Switch model common routines prototypes
*               are declared in this file
*
* Notes:
*
******************************************************************************/




#include "rio_types.h"
#include "rio_gran_type.h"
#include "rio_switch_errors.h"



/* print the version of the model */
int RIO_Switch_Print_Version(
    RIO_HANDLE_T             handle
    );

/* set state indicator flag */
int RIO_Switch_Set_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            indicator_Value,
    unsigned int             indicator_Param
    );

/* get state indicator flag */
int RIO_Switch_Get_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param
    );

/* obtaining granule for transmitting */
int RIO_Switch_Get_Granule(
    RIO_CONTEXT_T context,   /* callback context */
    RIO_HANDLE_T  handle,    /* instance's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    );

/* granule arrived */
int RIO_Switch_Put_Granule(
    RIO_CONTEXT_T context,   /* callback context */
    RIO_HANDLE_T  handle,    /* instance's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    );

/* wrap link error message */
int RIO_Switch_Error_Msg(
    RIO_CONTEXT_T  context,
    char *         error_Msg      /* null-terminated error string */
    );

/* wrap link warning message */
int RIO_Switch_Warning_Msg(
    RIO_CONTEXT_T  context,
    char *         warning_Msg      /* null-terminated warning string */
    );

int RIO_Switch_Msg(
    RIO_CONTEXT_T     context,    /* callback context */
    RIO_PL_MSG_TYPE_T msg_Type,
    char              *string
    );

/*gags for hooks*/
int RIO_Switch_Granule_Received(
    RIO_CONTEXT_T         context,
    RIO_GRANULE_STRUCT_T  *symbol_Struct      /* struct, containing symbol's fields */
    );

int RIO_Switch_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
    );

/*for input present bit in register*/
int RIO_Switch_Link_Input_Present(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_BOOL_T    input_Present /* pinter to granule data structure */
    );


/****************************************************************************/
#endif /* RIO_SWITCH_MODEL_COMMON_H */



