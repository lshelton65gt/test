/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/rio_switch_tx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Switch model transmitter source
*
* Notes: none
*
******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "rio_switch_ds.h"
#include "rio_switch_tx.h"
#include "rio_switch_rx.h"
#include "rio_switch_errors.h"
#include "rio_switch_registers.h"

/* gets EOP packet control */
static void rio_Switch_Tx_Get_EOP(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *eop, unsigned int link_Number);

/*gets STOMP packet control */
static void rio_Switch_Tx_Get_STOMP(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *eop);

/* gets IDLE packet control */
static void rio_Switch_Tx_Get_IDLE
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *idle, unsigned int link_Number);

/* send LINK RESPONSE control */
static void send_Link_Response
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* send RESTART FROM RETRY */
static void send_Rest_From_Retr
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* get RESTART FROM RETRY control*/
static void get_Restart_From_Retry(RIO_GRANULE_T *granule);

/* send the next acknowledge */
static void send_Acknowledge
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* packet transmission in OK state */
static void tx_Normal(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* sent next portion of a packet */
static void send_Packet_Part
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* try starting transmission of new packet */
static void start_New_Packet
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* try finding new packet for transmission */
static int find_New_Packet
(RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* change transmitter state in corresponding register */
static void rio_Switch_Tx_Cng_St_To_Normal
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

static void rio_Switch_Tx_Cng_St_To_Retry
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

static void rio_Switch_Tx_Cng_St_To_Error
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/*change transmitter state to unrecoverable*/
static void rio_Switch_Tx_Cng_St_To_Unrec_Error
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* mask for transmitter state clean */
#define RIO_SWITCH_TX_BITS_CLEAN    0x8000ffff
#define RIO_SWITCH_TX_STATE_OK            0x00020000
#define RIO_SWITCH_TX_ERROR            0x00200000
#define RIO_SWITCH_TX_RETRY            0x00100000
#define RIO_SWITCH_TX_UNREC_ERROR   0x00080000

static void rio_Switch_Start_Tx_Error_Rec(RIO_SWITCH_DS_T *handle, unsigned int link_Number);
static void rio_Switch_Start_Tx_Retry_Rec(RIO_SWITCH_DS_T *handle, unsigned int link_Number);
static void rio_Switch_Check_States(RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/***************************************************************************
 * Function : RIO_Switch_Tx_Granule_For_Transmitting
 *
 * Description: get a granule for transmitting through the link
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Granule_For_Transmitting(
    RIO_SWITCH_DS_T *handle,
    RIO_GRANULE_T   *granule,
    unsigned int    link_Number)
{
    if (handle == NULL || granule == NULL)
        return;

    if (link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /*check states in regs - new added*/
    rio_Switch_Check_States(handle, link_Number);

    /*training state machine state check*/

/*    rio_Switch_Handler_Train_Tx_State_Machine(handle, link_Number);*/

    /*
     * we check all information source
     * if we are sending a packet ask for new portion
     */

    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).idle_watch = RIO_FALSE;

    
/*  fatal error processing
    if (handle->tx_Data.state == RIO_PL_ERROR) 
    {
        get_IDLE(handle, granule);
        return;
    }
*/    
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.idle_Generator.valid == RIO_TRUE)
    {
        /* send IDLE */
        rio_Switch_Tx_Get_IDLE(handle, granule, link_Number);
        /* decrise IDLE counter */
        if (!--RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.idle_Generator.idle_Cnt)
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.idle_Generator.valid = RIO_FALSE;
/*            rio_Switch_Changer_Train_Tx_State_Machine(handle, link_Number);*/
        return;
    }

    /* send TRAINING if there is one */
 /*   if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.training_Generator.valid == RIO_TRUE)
    {
        send_Training(handle, granule,link_Number);
        rio_Switch_Changer_Train_Tx_State_Machine(handle, link_Number);
        return;
    }*/
    /* send link response if there is one */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.valid == RIO_TRUE &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.last_Ack == 
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head)
    {
        send_Link_Response(handle, granule, link_Number);
        return;
    }

    /* send LINK_REQUEST if there is one */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.valid == RIO_TRUE &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.is_Sent == RIO_FALSE)
    {
        RIO_Switch_Tx_Send_Link_Req(handle, granule, link_Number);
    /*    rio_Switch_Changer_Train_Tx_State_Machine(handle, link_Number);*/
        return;
    }

    /* send acknowledge if there is one */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.is_Full ||
        (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top !=
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool [
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head ].state == 
        RIO_SWITCH_ACK_POOL_ELEM_FULL))
    {
        send_Acknowledge(handle, granule, link_Number);
        return;
    }

    /* send link response if there is one */
/*    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.valid == RIO_TRUE &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.last_Ack == 
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head)
    {
        send_Link_Response(handle, granule, link_Number);
        return;
    }
*/
    /* send restart from retry if there is one */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.restart_From_Retry.valid == RIO_TRUE)
    {
        send_Rest_From_Retr(handle, granule, link_Number);
        return;
    }

    /* sent THROTTLE if any */
    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_For_Send.valid == RIO_TRUE)
    {
        *granule = RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_For_Send.control;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_For_Send.valid = RIO_FALSE;
        return;
    }

    /* depends of transmitter state */
    switch (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State)
    {
        case RIO_SWITCH_LINK_OK:
            tx_Normal(handle, granule, link_Number);
            break;

        default:
            rio_Switch_Tx_Get_IDLE(handle, granule, link_Number);
    }
}

/***************************************************************************
 * Function : send_Link_Response
 *
 * Description: send LINK RESPONSE control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void send_Link_Response
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* get the link response */
    *granule = RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.control;

    /* correct the expected ackID field */
    granule->granule_Struct.ack_ID = RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID;

    /* correct ackID also in the link_Status field if necessary */
    if ( (granule->granule_Struct.buf_Status & 0x8) == 0x8 )
        granule->granule_Struct.buf_Status = ((unsigned int)RIO_PL_LRESP_OK) | 
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID; 

    /* clear the link response latch */
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.valid = RIO_FALSE;
}


/***************************************************************************
 * Function : RIO_Switch_Tx_Init
 *
 * Description: initialize the transmitter
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Init(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned int i; /* loop counter */

    if (handle == NULL)
        return;

    /* initialize */
    i = link_Number;
  /*  for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {*/
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).state = RIO_SWITCH_LINK_FREE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).global_State = RIO_SWITCH_LINK_UNINIT;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).connected_To = RIO_SWITCH_NOT_CONNECTED;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).expected_Ack_ID = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Time_Out.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Time_Out.time_After_Sending = 0;

        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Pool.pool_Head = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Pool.pool_Top = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Pool.is_Full = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).need_STOMP = RIO_FALSE;

        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.is_Sent = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.time_After_Sending = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.retry_Cnt = 0;
/*        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.training_Counter.valid = RIO_TRUE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.training_Counter.training_Cnt = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.training_Counter.time_After_Sending = 0;*/

        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Response.valid  = RIO_FALSE;

        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).restart_From_Retry.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).last_Worked_Link = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).conf_Req = RIO_FALSE;

        /* initialize THROTTLE for sending latch */
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).throttle_For_Send.valid = RIO_FALSE;

        /* initialize THROTTLE generator */
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).throttle_Gen.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).throttle_Gen.idle_Cnt = 0;

        /* initialize IDLE generator */
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).idle_Generator.valid = RIO_TRUE;/*RIO_FALSE;*/
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).idle_Generator.idle_Cnt = 3;

        /* initialize TRAINING generator */
/*        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).training_Generator.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).training_Generator.training_Cnt = 0;
*/
        /* initialize IDLEWATCH  */
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).idle_watch = RIO_FALSE;

        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).in_Progress = RIO_FALSE;


    /*    if (handle->requires_Window_Alignment_Params[i] == RIO_TRUE)
            RIO_SWITCH_GET_LNK_TX_DATA(handle, i).train_state = RIO_SWITCH_TX_SEND_TRN_REQ;
        else
            RIO_SWITCH_GET_LNK_TX_DATA(handle, i).train_state = RIO_SWITCH_TX_SEND_IDLES_REQ;*/

      /*  RIO_SWITCH_GET_LNK_TX_DATA(handle, i). = RIO_SWITCH_TX_OK;*/
    
/*        RIO_PL_Print_Warning(handle, RIO_PL_WARNING_8,
            RIO_SWITCH_GET_LNK_TX_DATA(handle, i).train_state); */
    /*}*/
}

/***************************************************************************
 * Function : rio_Switch_Tx_Get_EOP
 *
 * Description: form proper EOP for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Tx_Get_EOP(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *eop, unsigned int link_Number)
{
    if (handle == NULL || eop == NULL)
        return;
    /* form end of packet control */
    eop->granule_Type = RIO_GR_EOP;
    eop->granule_Struct.s = 1;
    eop->granule_Struct.ack_ID = RIO_GR_EOP_SUB_TYPE;
    eop->granule_Struct.buf_Status = RIO_Switch_Get_Our_Buf_Status(handle, link_Number);
    eop->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
    /* finish current packet */
}

/***************************************************************************
 * Function : rio_Switch_Tx_Get_STOMP
 *
 * Description: form proper STOMP for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Tx_Get_STOMP(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *idle)
{
    if (handle == NULL || idle == NULL)
        return;

    idle->granule_Type = RIO_GR_STOMP;
    idle->granule_Struct.s = 1;
    idle->granule_Struct.ack_ID = RIO_GR_STOMP_SUB_TYPE;
    idle->granule_Struct.buf_Status = 0;
    idle->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
}

/***************************************************************************
 * Function : rio_Switch_Tx_Get_IDLE
 *
 * Description: form proper IDLE for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Tx_Get_IDLE(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *idle,
    unsigned int link_Number)
{
    if (handle == NULL || idle == NULL)
        return;

    idle->granule_Type = RIO_GR_IDLE;
    idle->granule_Struct.s = 1;
    idle->granule_Struct.ack_ID = RIO_GR_IDLE_SUB_TYPE;
    idle->granule_Struct.buf_Status = RIO_Switch_Get_Our_Buf_Status(handle, link_Number);
    idle->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;

    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).idle_watch = RIO_TRUE;
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Send_Ack
 *
 * Description: load new acknowledge for sending 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Send_Ack(
    RIO_SWITCH_DS_T *handle, unsigned int link_Number, RIO_GRANULE_T *retry)
{
    unsigned int index;

    if (handle == NULL || retry == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* check if pool is full*/
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.is_Full == RIO_TRUE)
        return;

    index = RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head;
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].state ==
            RIO_SWITCH_ACK_POOL_ELEM_WAIT &&
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].ack_Granule.granule_Struct.ack_ID ==
            retry->granule_Struct.ack_ID)
    {
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->
            tx_Data.ack_Pool.pool[index].state = RIO_SWITCH_ACK_POOL_ELEM_FULL;
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->
            tx_Data.ack_Pool.pool[index].ack_Granule = *retry;

        if (retry->granule_Type  ==  RIO_GR_PACKET_NOT_ACCEPTED)
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top = 
            (index + 1) % handle->ack_Buffer_Size;

        if (retry->granule_Type  ==  RIO_GR_PACKET_RETRY &&
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top == 
            (index + 2) % handle->ack_Buffer_Size &&
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[(index + 1) % handle->ack_Buffer_Size].state ==
            RIO_SWITCH_ACK_POOL_ELEM_FULL  &&
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[(index + 1) 
            % handle->ack_Buffer_Size].ack_Granule.granule_Type != RIO_GR_PACKET_NOT_ACCEPTED)
        {
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top = 
            (index + 1) % handle->ack_Buffer_Size;
        }
        return;    
    }

    if ((RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].state == 
            RIO_SWITCH_ACK_POOL_ELEM_WAIT) ||
            (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].state ==
            RIO_SWITCH_ACK_POOL_ELEM_FULL &&
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].ack_Granule.granule_Type ==
            RIO_GR_PACKET_ACCEPTED))
            index = (index + 1) % handle->ack_Buffer_Size;

    if (index == RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top &&
        retry->granule_Type != RIO_GR_PACKET_ACCEPTED)
    {
        /* ack acknowledge */
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top].state = RIO_SWITCH_ACK_POOL_ELEM_FULL;
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top].ack_Granule = *retry;
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top = 
            (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top + 1) % handle->ack_Buffer_Size;

        if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top ==
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head)
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.is_Full = RIO_TRUE;

        return;
    }

    if ((index + 1) % handle->ack_Buffer_Size == RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].state == RIO_SWITCH_ACK_POOL_ELEM_FULL &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].ack_Granule.granule_Type !=
        RIO_GR_PACKET_NOT_ACCEPTED &&
        retry->granule_Type == RIO_GR_PACKET_NOT_ACCEPTED)
    {
        /* ack acknowledge */
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].state = RIO_SWITCH_ACK_POOL_ELEM_FULL;
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[index].ack_Granule = *retry;
        return;

    }
}

/***************************************************************************
 * Function : RIO_Switch_Tx_STOMP_Transmission
 *
 * Description: set up the STOMP flag  
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_STOMP_Transmission(
    RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    if (handle == NULL || link_Number == RIO_SWITCH_NOT_CONNECTED || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return ;

    /* set up the flag*/
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.need_STOMP = RIO_TRUE;
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Send_Link_Resp
 *
 * Description: a link response shall be sent
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Send_Link_Resp(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Response, unsigned int link_Number)
{
    if (handle == NULL || link_Response == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* check if one link response is waiting for heading out */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.valid == RIO_TRUE)
    {
        return;
    }

    /* hold link response in the latch */
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.valid = RIO_TRUE;
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.control = *link_Response;
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.last_Ack = 
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top;
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Rec_Link_Resp
 *
 * Description:  a link response has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Rec_Link_Resp
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *link_Response, unsigned int link_Number)
{
    /* status of link response register */
    unsigned long status;
    char error_Buffer[255]; 

    if (handle == NULL || link_Response == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* detect offest */
/*    status_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr + 
        link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A;*/

    /* check if the instanse is waiting for LINK_RESPONSE*/
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.valid == RIO_FALSE)
    {
        sprintf(error_Buffer, "Output error detected on link N %lu : unexpected Link Responce.\n",
            (unsigned long)link_Number);
        RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
        /*start error recovery*/
        rio_Switch_Start_Tx_Error_Rec(handle, link_Number);
        return;
    }

    /* delete LINK_REQUEST and store response to register */
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.valid = RIO_FALSE;
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.time_After_Sending = 0;
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.is_Sent = RIO_FALSE;

    /* set valid, link status, expected ackID and retry cnt values*/
    status = (unsigned long)0x80000000; /* 0x80000000 - initial value with valid bit */
    status |= (unsigned long)link_Response->granule_Struct.buf_Status; 
    status |= ((unsigned long)link_Response->granule_Struct.ack_ID) << 4; /* ackID status shall be shifted to the left on 4 bits */
    /* whereas retry cnt is shifted on 8 bits */
    status |= ((unsigned long)RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.retry_Cnt) << 8; 
/*    RIO_Switch_Write_Register(handle, status_Offset, status, RIO_TRUE);*/

    /*set lrsp ackID status*/
    RIO_Switch_Set_State(
            handle,
            RIO_LRSP_ACKID_STATUS, 
            link_Response->granule_Struct.ack_ID,
            0,
            link_Number);

    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.retry_Cnt = 0;

    
    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).global_State == RIO_SWITCH_LINK_OK)
    {
        if ((link_Response->granule_Struct.ack_ID != RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID &&
                RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_FREE) ||
                (link_Response->granule_Struct.ack_ID != RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID &&
                link_Response->granule_Struct.ack_ID != (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID + 1) %
                handle->ack_Buffer_Size &&
                RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state != RIO_SWITCH_LINK_FREE))
        {
                /*unrecoverable error detected**/
            rio_Switch_Tx_Cng_St_To_Unrec_Error(handle, link_Number);
            return;
        }
    }
    else 
    {
        if (link_Response->granule_Struct.ack_ID != RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID &&
                link_Response->granule_Struct.ack_ID != (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID + 1) %
                handle->ack_Buffer_Size)
        {
           /*unrecoverable error detected**/
            rio_Switch_Tx_Cng_St_To_Unrec_Error(handle, link_Number);
            return;
        }

    }
    
    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_BLOCKED)
    {
        RIO_GRANULE_T ack_Granule;

        if (link_Response->granule_Struct.ack_ID == RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID)
        {
            ack_Granule.granule_Type = RIO_GR_PACKET_RETRY;
            ack_Granule.granule_Struct.stype = RIO_GR_PACKET_RETRY_STYPE;
            ack_Granule.granule_Struct.buf_Status = 0;
        }
        else if (link_Response->granule_Struct.ack_ID == (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID + 1) %
            handle->ack_Buffer_Size)
        {
            ack_Granule.granule_Type = RIO_GR_PACKET_ACCEPTED;
            ack_Granule.granule_Struct.stype = RIO_GR_PACKET_ACCEPTED_STYPE;
            ack_Granule.granule_Struct.buf_Status = 1;
        }

        ack_Granule.granule_Struct.ack_ID = link_Response->granule_Struct.ack_ID;
        ack_Granule.granule_Struct.s = 1;
     
         
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;

        if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_TRUE &&
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To == RIO_SWITCH_NOT_CONNECTED)
        {
            if (ack_Granule.granule_Type == RIO_GR_PACKET_ACCEPTED)
            {
                handle->resp_Buffer[link_Number].buffer_Head = 0;
                handle->resp_Buffer[link_Number].buffer_Top = 0;
                RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req = RIO_FALSE;
            }
            else
            {
                handle->resp_Buffer[link_Number].buffer_Head = 0;
            }
        }
        else
            RIO_Switch_Rx_Rec_Ack(handle, &ack_Granule, 
                RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To);
    }

    /* move boundaries*/
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID = 
        link_Response->granule_Struct.ack_ID;
        
    /*set next out port transmitted ackID value in packet*/
    RIO_Switch_Set_State(
        handle,
        RIO_OUTBOUND_ACKID, 
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID,
        0,
        link_Number);

    /*set expected ackID in the next received ack control*/
    RIO_Switch_Set_State(
        handle,
        RIO_OUTSTANDING_ACKID, 
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID,
        0,
        link_Number);

    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).global_State != RIO_SWITCH_LINK_OK)
    {
       
        /* change register */
        rio_Switch_Tx_Cng_St_To_Normal(handle, link_Number);
    }
    return;
}

/***************************************************************************
 * Function : RIO_Pl_Tx_Rec_Ack
 *
 * Description: an acknowledgment for headed out packet has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Rec_Ack
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *ack_Granule, unsigned int link_Number)
{
    char error_Buffer[255];

    if (handle == NULL || ack_Granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;
    /* check if ackID is an unexpected value */
    if (ack_Granule->granule_Struct.ack_ID != 
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).expected_Ack_ID ||
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_FREE ||
        (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To == RIO_SWITCH_NOT_CONNECTED &&
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_FALSE))
    {
        sprintf(error_Buffer, 
            "Output error detected on link N %lu : received unsolicited acknowledge or acknowledge with unexpected ackID.\n",
            (unsigned long)link_Number);
        RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
    
            /* change register */
     /*   rio_Switch_Tx_Cng_St_To_Error(handle, link_Number);

        if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.valid == RIO_FALSE)
        {
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.valid = RIO_TRUE;
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.is_Sent = RIO_FALSE;
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.time_After_Sending = 0;
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.retry_Cnt = 0;
            RIO_Switch_Tx_Get_Link_Request(&RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.control);
        }  */
        
        /*new**/
        rio_Switch_Start_Tx_Error_Rec(handle, link_Number);
        return;
    } 



    /* after acknowledge link is ready to accept new packet */
    switch (ack_Granule->granule_Type)
    {
        /* if packet has been accepted increase ackID for sending */
        case RIO_GR_PACKET_ACCEPTED:
            if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_WORK)
            {
                sprintf(error_Buffer, 
                    "Output error detected on link N %lu : received Packet_Accepted acknowledge on still sending packet.\n",
                    (unsigned long)link_Number);
                RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
                rio_Switch_Start_Tx_Error_Rec(handle, link_Number);
                return;
            }
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID = 
                (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID + 1) %
                handle->ack_Buffer_Size;
            
            RIO_Switch_Set_State(
            handle,
            RIO_OUTSTANDING_ACKID, 
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.expected_Ack_ID,
            0,
            link_Number);
            break;

        case RIO_GR_PACKET_RETRY:
            rio_Switch_Start_Tx_Retry_Rec(handle, link_Number);
            break;

        case RIO_GR_PACKET_NOT_ACCEPTED:
            sprintf(error_Buffer, 
                "Output error detected on link N %lu : received unsolicited acknowledge or acknowledge with unexpected ackID.\n",
                (unsigned long)link_Number);
            RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
            /*error recovery is need to be started*/
            rio_Switch_Start_Tx_Error_Rec(handle, link_Number);
            break;
           
        default: {}
    }

    /* change state and pass the acknowledge */
    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;
    
    /*set off ack Time Out flag*/
    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).ack_Time_Out.valid = RIO_FALSE;
    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).ack_Time_Out.time_After_Sending = 0;

    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_TRUE &&
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To == RIO_SWITCH_NOT_CONNECTED)
    {
        if (ack_Granule->granule_Type == RIO_GR_PACKET_ACCEPTED)
        {
            handle->resp_Buffer[link_Number].buffer_Head = 0;
            handle->resp_Buffer[link_Number].buffer_Top = 0;
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req = RIO_FALSE;
        }
        else
        {
            handle->resp_Buffer[link_Number].buffer_Head = 0;
        }
    }
    else
        RIO_Switch_Rx_Rec_Ack(handle, ack_Granule, 
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To);

    return;
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Send_Link_Req
 *
 * Description: send LINK REQUEST control
 *
 * Returns: none
 *
 * Notes: discard currently transmitting packet if necessary
 *
 **************************************************************************/
void RIO_Switch_Tx_Send_Link_Req
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{

    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* detect offset */
/*    status_Offset = RIO_LPEP_REG_BASE + link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A;*/

    /* send link request */
    *granule = RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.control;
    granule->granule_Struct.buf_Status = RIO_Switch_Get_Our_Buf_Status(handle, link_Number);

    /* RESET command does not get a response */
    if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_RESET)
    {
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.is_Sent = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.time_After_Sending = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.retry_Cnt = 0;
    }
    else if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
    {           
/*        if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).global_State == RIO_SWITCH_LINK_UNINIT)
        {
            RIO_GRANULE_T training;
            RIO_Switch_Tx_Send_Training(handle, &training,link_Number);
        }
*/
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.is_Sent = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.retry_Cnt = 0;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.time_After_Sending = 0;

/*         RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.training_Counter.valid = RIO_TRUE;
         RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.training_Counter.time_After_Sending = 0;
*/         
    }
    else
    {
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.is_Sent = RIO_TRUE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.time_After_Sending = 0;
    }
    
    /* terminate current packet if necessary */
    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_WORK)
    {
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_BLOCKED;
        /* terminate current packet if necessary */
        
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).in_Progress = RIO_FALSE;

        RIO_Switch_Set_State(
            handle,
            RIO_SYMBOL_IS_PD, 
            RIO_TRUE,
            0,
            link_Number);
     }

    /* set valid, link status, expected ackID and retry cnt values*/
    /*status = (unsigned long)0x00000000;*/
    /* retry count shall be shifted 8 bits left */
    /*status |= ((unsigned long)RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).link_Request.retry_Cnt) << 8;
    RIO_Switch_Write_Register(handle, status_Offset, status, RIO_TRUE);                */
}

/***************************************************************************
 * Function : send_Rest_From_Retr
 *
 * Description: send RESTART FROM RETRY control
 *
 * Returns: none
 *
 * Notes: discard currently transmitting packet if necessary
 *
 **************************************************************************/
static void send_Rest_From_Retr
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* send link request */
    *granule = RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).restart_From_Retry.control;
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.restart_From_Retry.valid = RIO_FALSE;
    
    /* terminate current packet if necessary */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.state == RIO_SWITCH_LINK_WORK)
    {
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.state = RIO_SWITCH_LINK_BLOCKED;
        
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).in_Progress = RIO_FALSE;

        RIO_Switch_Set_State(
            handle,
            RIO_SYMBOL_IS_PD, 
            RIO_TRUE,
            0,
            link_Number);
    }

    /* clean transmitter state */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State == 
        RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY)
    {
        /* change register */
        rio_Switch_Tx_Cng_St_To_Normal(handle, link_Number);
    }
}

/***************************************************************************
 * Function : send_Acknowledge
 *
 * Description: send the next acknowledge
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void send_Acknowledge
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    /* index of current acknowledge in ack_Out_Buffer */
    unsigned int current_Ack;

    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* send the acknowledge*/
    current_Ack = RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head;
    *granule = RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[current_Ack].ack_Granule;
    
    if (granule->granule_Type == RIO_GR_PACKET_ACCEPTED)
        granule->granule_Struct.buf_Status = RIO_Switch_Get_Our_Buf_Status(handle, link_Number);

    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head = (current_Ack + 1) % handle->ack_Buffer_Size;
    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.is_Full = RIO_FALSE;
    
}

/***************************************************************************
 * Function : get_Restart_From_Retry
 *
 * Description: get proper RESTART FROM RETRY control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void get_Restart_From_Retry(RIO_GRANULE_T *granule)
{
    if (granule == NULL)
        return;

    granule->granule_Struct.s = 1;
    granule->granule_Struct.ack_ID = RIO_GR_RESTART_FROM_RETRY_SUB_TYPE;
    granule->granule_Struct.buf_Status = 0;
    granule->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
    granule->granule_Type = RIO_GR_RESTART_FROM_RETRY;
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Get_Link_Request
 *
 * Description: get proper LINK REQUEST control
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Get_Link_Request(RIO_GRANULE_T *granule)
{
    if (granule == NULL)
        return;

    granule->granule_Struct.s = 1;
    granule->granule_Type = RIO_GR_LINK_REQUEST;
    granule->granule_Struct.s = 1;
    granule->granule_Struct.ack_ID = RIO_LP_LREQ_INPUT_STATUS;
    granule->granule_Struct.buf_Status = 1;
    granule->granule_Struct.stype = RIO_GR_LINK_REQUEST_STYPE;
}

/***************************************************************************
 * Function : tx_Normal
 *
 * Description: detect data for sending in normal state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void tx_Normal(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* packet transmitting*/
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.state == RIO_SWITCH_LINK_WORK)
    {
        /* check if we need to STOMP packet */
        if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.need_STOMP == RIO_TRUE)
        {
            unsigned int connected_Rx;

            /* STOMP packet and change state */
            rio_Switch_Tx_Get_STOMP(handle, granule);
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).need_STOMP = RIO_FALSE;
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_BLOCKED;

            /* change source state */
            connected_Rx = RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To;

            RIO_SWITCH_GET_LNK_RX_DATA(handle, connected_Rx).buffer.buffer_Head = 
                RIO_SWITCH_GET_LNK_RX_DATA(handle, connected_Rx).buffer.buffer_Top_For_Load = 
                RIO_SWITCH_GET_LNK_RX_DATA(handle, connected_Rx).buffer.buffer_Top;

            /* init THROTTLE generator */
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.valid = RIO_FALSE;

            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).in_Progress = RIO_FALSE;
            
            return;
        }

        /* received THROTTLE performing */
        if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.valid == RIO_TRUE)
        {
            /* send IDLE */
            rio_Switch_Tx_Get_IDLE(handle, granule, link_Number);
            /* decrise IDLE counter */
            if (!--RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.idle_Cnt)
                RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.valid = RIO_FALSE;
            return;
        }

        /* send the next portion of a packet */
        send_Packet_Part(handle, granule, link_Number);
        return;
    }

    /* trying starting new packet transmission */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.state == RIO_SWITCH_LINK_FREE)
    {
        start_New_Packet(handle, granule, link_Number);
        return;
    }

    rio_Switch_Tx_Get_IDLE(handle, granule, link_Number);
}

/***************************************************************************
 * Function : start_New_Packet
 *
 * Description: start new packet transmission
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void start_New_Packet
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* check if we can start finding new packet */
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.state != RIO_SWITCH_LINK_FREE)
    {
        rio_Switch_Tx_Get_IDLE(handle, granule, link_Number);
        return;
    }
    
    if (find_New_Packet(handle, link_Number) == RIO_ERROR)
        rio_Switch_Tx_Get_IDLE(handle, granule, link_Number);
    else
    {
        /* init THROTTLE generator */
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.valid = RIO_FALSE;
        
        /* send first packet granule */
        send_Packet_Part(handle, granule, link_Number);

        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).in_Progress = RIO_TRUE;
    }
}

/***************************************************************************
 * Function : send_Packet_Part
 *
 * Description: send the next part of a packet
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void send_Packet_Part
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    unsigned int               receiver_Index;
    RIO_SWITCH_PACKET_BUFFER_T *source; /* packet source */

    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* detect the proper receiver */
    receiver_Index = RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.connected_To;

    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_TRUE &&
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To == RIO_SWITCH_NOT_CONNECTED)
        source = &handle->resp_Buffer[link_Number];
    else
        source = &RIO_SWITCH_GET_LNK_RX_DATA(handle, receiver_Index).buffer;

    /* check if the last portion of data has already been sent */
    if (source->buffer_Top == source->buffer_Head)
    {
        if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To != RIO_SWITCH_NOT_CONNECTED &&
            RIO_SWITCH_GET_LNK_DATA(handle, receiver_Index)->rx_Data.state == RIO_SWITCH_LINK_WORK)
        {
            rio_Switch_Tx_Get_IDLE(handle, granule, link_Number);
            return;
        }
        else
        {
            rio_Switch_Tx_Get_EOP(handle, granule, link_Number);
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.state = RIO_SWITCH_LINK_BLOCKED;
            
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Time_Out.valid = RIO_TRUE;
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Time_Out.time_After_Sending = 0;


            /*set off the flag*/
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).in_Progress = RIO_FALSE;
            return;
        }
    }

    /* send the next data portion */
    if (source->buffer_Head)
    {
        granule->granule_Type = RIO_GR_DATA;
        granule->granule_Date[0] = 
            source->buffer[source->buffer_Head]; /* byte 0 - ordinary data */
    }
    else
    {
        granule->granule_Type = RIO_GR_NEW_PACKET;
       
        /*set on the flag*/
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).in_Progress = RIO_TRUE;


        /* 
         * complete the first byte with ackID  -depending on mode - serial or parallel - 
         * in according with the specification. 
         * ackID mask and offest are used only here has't to be defined as macroses
         */
        if (handle->is_Parallel_Model == RIO_TRUE)
        {
            granule->granule_Date[0]  = (RIO_UCHAR_T)((RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).expected_Ack_ID & 0x07) << 4);
            /*add s-parity bit*/
            granule->granule_Date[0] |= (RIO_UCHAR_T)(0x1 << 2);
        }
        else
        {
            /*in case of serial model the ackID is 5-bit size*/
          
            granule->granule_Date[0] =
                (RIO_UCHAR_T)((RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).expected_Ack_ID & 0x1f) << 3);
        }


    }

    /* load the data */
    source->buffer_Head++;    

    granule->granule_Date[1] = source->buffer[source->buffer_Head++]; /* byte 1*/

    granule->granule_Date[2] = source->buffer[source->buffer_Head++]; /* byte 2*/

    granule->granule_Date[3] = source->buffer[source->buffer_Head++]; /* byte 3*/
}

/***************************************************************************
 * Function : find_New_Packet
 *
 * Description: try to find new packet for transmission
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static int find_New_Packet
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned int i, flag = 0; /* loop counter */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return RIO_ERROR;

    /* start finding from the MAINTENANCE response */
    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_TRUE)
    {
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_WORK;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To = RIO_SWITCH_NOT_CONNECTED;
        return RIO_OK;
    }

    if (handle->link_Common_Params[link_Number].only_Maint_Responses == RIO_TRUE)
        return RIO_ERROR;

    /* start finding from the next link */
    i = RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.last_Worked_Link;
    i = (i + 1) % RIO_SWITCH_GET_PORTS_CNT(handle);

    /* go through all links */
    for (; !flag; i = (i + 1) % RIO_SWITCH_GET_PORTS_CNT(handle))
    {
        if (RIO_SWITCH_GET_LNK_RX_DATA(handle, i).state == RIO_SWITCH_LINK_WORK &&
            RIO_SWITCH_GET_LNK_RX_DATA(handle, i).connected_To == (int)link_Number &&
            RIO_SWITCH_GET_LNK_RX_DATA(handle, i).conf_Req == RIO_FALSE)
        {
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_WORK;
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To = i;
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).last_Worked_Link = i;

            return RIO_OK;
        }
        if (i == RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.last_Worked_Link)
            flag = 1;
    }

    return RIO_ERROR;
}

/***************************************************************************
 * Function : rio_Switch_Tx_Cng_St_To_Retry
 *
 * Description: update link state to stopped due to retry
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Tx_Cng_St_To_Retry
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long state_Offset; /* corresponding CSR address */
    unsigned long state; /* state itself */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* set appropriate state and load restart from retry to the latch */
    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).global_State = 
        RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY;

    state_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr +
        link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A;

        /* change receiver state in the register */
    RIO_Switch_Read_Register(handle, state_Offset, &state, RIO_TRUE);
    /* clear receiver bits*/
    state &= RIO_SWITCH_ERROR_TX_STATE_CLEAR_MASK;
    /* set OK bit */
    state |= RIO_SWITCH_ERROR_TX_RETRY;
    RIO_Switch_Write_Register(handle, state_Offset, state, RIO_TRUE);
}

/***************************************************************************
 * Function : rio_Switch_Tx_Cng_St_To_Error
 *
 * Description: update link state to stopped due to error
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Tx_Cng_St_To_Error
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long state_Offset; /* corresponding CSR address */
    unsigned long state; /* state itself */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).global_State = 
        RIO_SWITCH_LINK_STOPPED_DUE_TO_ERROR;

    state_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr +
        link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A;

        /* change receiver state in the register */
    RIO_Switch_Read_Register(handle, state_Offset, &state, RIO_TRUE);
    /* clear bits*/
    state &= RIO_SWITCH_ERROR_TX_STATE_CLEAR_MASK;
    /* set OK bit */
    state |= RIO_SWITCH_ERROR_TX_ERROR;
    RIO_Switch_Write_Register(handle, state_Offset, state, RIO_TRUE);
}

/***************************************************************************
 * Function : rio_Switch_Tx_Cng_St_To_Normal
 *
 * Description: update link state to OK
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Tx_Cng_St_To_Normal
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long state_Offset; /* corresponding CSR address */
    unsigned long state; /* state itself */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    state_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr + link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A;

        /* change receiver state in the register */
    RIO_Switch_Read_Register(handle, state_Offset, &state, RIO_TRUE);
    /* clear bits*/
    state &= RIO_SWITCH_ERROR_TX_STATE_CLEAR_MASK;
    /* set OK bit */
    /*state |= RIO_SWITCH_TX_STATE_OK;*/
    RIO_Switch_Write_Register(handle, state_Offset, state, RIO_TRUE);

    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).global_State = RIO_SWITCH_LINK_OK;
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Link_Req_Time_Out
 *
 * Description: count time out for sent LINK REQUEST
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Link_Req_Time_Out(RIO_SWITCH_DS_T *handle)
{
    unsigned int i; /* loop counter */

    if (handle == NULL)
        return;

    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        /* if time-out is zero don't check it at all*/
        if (!RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out)
            return;
        
        /* check time-out */
        if (RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.valid == RIO_TRUE &&
            RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.is_Sent == RIO_TRUE)
        {
            /* check if time-out event has just happened*/
            if (RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.time_After_Sending >=
                RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out)
            {
                /* check if retry limit is overstepped */
/*                if (RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.retry_Cnt >= 
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Retry_Cnt)
                {*/
                    char error_Buffer[255];
                    sprintf(error_Buffer, "Unrecoverable error detected on link N %lu : link-request time-out.\n", (unsigned long)i );
                    RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
                    /* delete link request from schedule*/
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.valid = RIO_FALSE;
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.is_Sent = RIO_FALSE;
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.time_After_Sending = 0;
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.retry_Cnt = 0;

                    /*start output error recovery*/
                    /*rio_Switch_Start_Tx_Error_Rec(handle, i);*/

                    /*unrecoverable error detected*/
                    rio_Switch_Tx_Cng_St_To_Unrec_Error(handle, i);
                    
/*                }
                else
                {*/
                    /* reshedule link request for sending */
/*                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.retry_Cnt++;
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.time_After_Sending = 0;
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.valid = RIO_TRUE;
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.is_Sent = RIO_FALSE;
                }*/
            }
            else
                RIO_SWITCH_GET_LNK_TX_DATA(handle, i).link_Request.time_After_Sending++;
            
        } /* end link request time-out proceding */
    }
}

/***************************************************************************
 * Function : RIO_PL_Tx_Phys_Time_Out
 *
 * Description: count physical layer time out 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Phys_Time_Out(RIO_SWITCH_DS_T *handle)
{
    unsigned int i; /* loop counter */

    if (handle == NULL)
        return;

    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        /* if time-out is zero don't check it at all*/
        if (!RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out)
            return;
        
        /* check time-out */
        if (RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Time_Out.valid == RIO_TRUE)
        {
            /* check if time-out event has just happened*/
            if (RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Time_Out.time_After_Sending >=
                RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out)
            {
                char error_Buffer[255];
                
                RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Time_Out.valid = RIO_FALSE;
                RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Time_Out.time_After_Sending = 0;

                sprintf(error_Buffer, 
                    "Output error detected on link N %lu : phys. Time-Out.\n",
                    (unsigned long)i);

                RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
   
                rio_Switch_Start_Tx_Error_Rec(handle, i);
                return;
            }
            else
                RIO_SWITCH_GET_LNK_TX_DATA(handle, i).ack_Time_Out.time_After_Sending++;
            
        } /* end phys time-out proceding */
    }
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Rec_Throttle
 *
 * Description: tries routing a THROTTLE to corresp inbound link
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void  RIO_Switch_Tx_Rec_Throttle
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    char error_Buffer[255];
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* THROTTLE is valid in context of packet transmission only */
    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state != RIO_SWITCH_LINK_WORK)
    {
        sprintf(error_Buffer, "Output error detected on link N %lu : unexpected Throttle.\n",
            (unsigned long)link_Number);
        RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
        return;
    }

    /* detect if packet is MAINTENANCE response and tx shall perform it by itself */
    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_TRUE)
    {
        /* load THROTTLE generator */
        if (granule->granule_Struct.buf_Status == 0xf)
        {
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.valid = RIO_FALSE;
            return;
        }
        else if (granule->granule_Struct.buf_Status == 0xe)
        {
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.valid = RIO_TRUE;
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.idle_Cnt = 1;
        }
        else
        {
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.valid = RIO_TRUE;
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).throttle_Gen.idle_Cnt = 
                1 << granule->granule_Struct.buf_Status;
        }
    }
    else
    {
        /* route THROTTLE to inbound transmitter */
        int rec_Lnk_Number = RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To;

        /* check number */
        if (rec_Lnk_Number < 0 || (unsigned)rec_Lnk_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
            return;

        /* store THROTTLE */
        RIO_SWITCH_GET_LNK_TX_DATA(handle, rec_Lnk_Number).throttle_For_Send.valid = RIO_TRUE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, rec_Lnk_Number).throttle_For_Send.control = *granule;
    }
}

/***************************************************************************
 * Function : rio_Switch_Tx_Cng_St_To_Error
 *
 * Description: update link state to stopped due to error
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Tx_Cng_St_To_Unrec_Error
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long state_Offset; /* corresponding CSR address */
    unsigned long state; /* state itself */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).global_State = 
        RIO_SWITCH_LINK_ERROR;

    state_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr +
        link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A;

        /* change receiver state in the register */
    RIO_Switch_Read_Register(handle, state_Offset, &state, RIO_TRUE);
    /* clear bits*/
    state &= RIO_SWITCH_ERROR_PORT_STATE_CLEAR_MASK;
    /* set OK bit */
    state |= RIO_SWITCH_ERROR_STATE_UNREC_ERROR;
    RIO_Switch_Write_Register(handle, state_Offset, state, RIO_TRUE);
}

/***************************************************************************
 * Function : RIO_Switch_Tx_Start_Packet_Ack
 *
 * Description: load new acknowledge for sending 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Tx_Start_Packet_Ack(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *retry, unsigned int link_Number)
{

    if (handle == NULL || retry == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return RIO_ERROR;

    /* check if pool is full*/
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.is_Full == RIO_TRUE)
        return RIO_ERROR;


    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top ==
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Head)
    {
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top].state = RIO_SWITCH_ACK_POOL_ELEM_WAIT;
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool[
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top].ack_Granule = *retry;

        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top = 
            (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.ack_Pool.pool_Top + 1) % handle->ack_Buffer_Size;
        return RIO_OK;
    }
    else return RIO_ERROR;

}

/***************************************************************************
 * Function : RIO_Switch_Tx_Control_Is_Expected
 *
 * Description: detect, if received conrol was expected by transmitter
 *
 * Returns: RIO_TRUE if control was expected
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T RIO_Switch_Tx_Control_Is_Expected(
        RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return RIO_FALSE;

    if ((granule->granule_Type == RIO_GR_LINK_REQUEST) && 
        (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Response.valid == RIO_TRUE))
        return RIO_FALSE;
    return RIO_TRUE;
}
/***************************************************************************
 * Function : RIO_PL_Tx_Rec_Unexp_Packet_Control
 *
 * Description:  an unexpected packet control has been received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Tx_Rec_Unexp_Packet_Control(RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL )
        return;
    /*RIO_PL_Print_Message(handle, RIO_PL_ERROR_48, granule->granule_Type);*/
    rio_Switch_Start_Tx_Error_Rec(handle, link_Number);
    return;
  
}
/***************************************************************************
 * Function : start_Tx_Error_Rec
 *
 * Description: starts output error recovery mechanizm
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Start_Tx_Error_Rec(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    
    if (handle == NULL) return;

    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State != RIO_SWITCH_LINK_OK &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State != RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY)
        return;
       
    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.valid == RIO_FALSE)
    {
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.valid = RIO_TRUE;
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.is_Sent = RIO_FALSE;
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.time_After_Sending = 0;
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.retry_Cnt = 0;

       /*set off ack Time Out flag*/
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).ack_Time_Out.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).ack_Time_Out.time_After_Sending = 0;


        RIO_Switch_Tx_Get_Link_Request(&RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.link_Request.control);

        /*this action is necessary to prevent unexpected RFR error
        (clear transition from output retry into output error state)*/
        if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.restart_From_Retry.valid == RIO_TRUE)
            RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.restart_From_Retry.valid = RIO_FALSE;
    }  
    
    /* update state in the register */
    rio_Switch_Tx_Cng_St_To_Error(handle, link_Number);

    return;
}


/***************************************************************************
 * Function : start_Tx_Error_Rec
 *
 * Description: starts output error recovery mechanizm
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Start_Tx_Retry_Rec(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    
    if (handle == NULL) return;

    if (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State != RIO_SWITCH_LINK_OK)
        return;
       
    /* change register */
    rio_Switch_Tx_Cng_St_To_Retry(handle, link_Number);

    RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.restart_From_Retry.valid = RIO_TRUE;
    get_Restart_From_Retry(&RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.restart_From_Retry.control);

    return;
}
/***************************************************************************
 * Function : RIO_PL_Get_Our_Buf_Status
 *
 * Description: return status of inbound buffer
 *
 * Returns: number of free entry
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Get_Our_Buf_Status(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Param;

    if (handle == NULL)
        return 0;

    /*get current buffer state*/
    RIO_Switch_Get_State(handle, RIO_FLOW_CONTROL_MODE, &indicator_Value, &indicator_Param, link_Number);
    if (indicator_Value  == RIO_TRUE) /*transmit flow control*/
    {
        RIO_Switch_Get_State(handle, RIO_OUR_BUF_STATUS, &indicator_Value, &indicator_Param, link_Number);
        return indicator_Value;
    }
    else if (handle->is_Parallel_Model == RIO_TRUE)
        return 0xf;
    else
        return 0x1f;
}

/***************************************************************************
 * Function : RIO_PL_Get_Partner_Buf_Status
 *
 * Description: return status of inbound buffer
 *
 * Returns: number of free entry
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Get_Partner_Buf_Status(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Param;

    if (handle == NULL)
        return 0;

    /*get current buffer state*/
    RIO_Switch_Get_State(handle, RIO_FLOW_CONTROL_MODE, &indicator_Value, &indicator_Param, link_Number);
    if (indicator_Value  == RIO_TRUE) /*transmit flow control*/
    {
        RIO_Switch_Get_State(handle, RIO_PARTNER_BUF_STATUS, &indicator_Value, &indicator_Param, link_Number);
        return indicator_Value;
    }
    else if (handle->is_Parallel_Model == RIO_TRUE)
        return 0xf;
    else
        return 0x1f;

}
/***************************************************************************
 * Function : check_States
 *
 * Description: 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Check_States(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Parameter;

    if (handle == NULL)
        return;

    RIO_Switch_Get_State(handle, RIO_UNREC_ERROR, &indicator_Value, &indicator_Parameter, link_Number);

    if (indicator_Value == RIO_TRUE &&
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State != RIO_SWITCH_LINK_ERROR)
    {
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State = RIO_SWITCH_LINK_ERROR;
        return;
    }    

    
    RIO_Switch_Get_State(handle, RIO_OUTPUT_ERROR, &indicator_Value, &indicator_Parameter, link_Number);

    if (indicator_Value == RIO_TRUE && (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State == RIO_SWITCH_LINK_OK ||
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->tx_Data.global_State == RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY))
        rio_Switch_Start_Tx_Error_Rec(handle, link_Number);

    return;
}
/*************************************************************************/

