/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/rio_switch_errors.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  switch model errors handlers source
*
* Notes:
*
******************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "rio_switch_ds.h"
#include "rio_switch_errors.h"


typedef struct {
    char *string;
    int count_Of_Add_Args;
} SWITCH_ERROR_WARNING_ENTRY_T;

/*
 * array of error strings
 */
static SWITCH_ERROR_WARNING_ENTRY_T switch_Message_Strings[] = {
    {"switch model, instance %lu %s", 1},
    {"switch model, instance %lu %s", 1},
    {"switch model, instance %lu %s", 1}

};

static const int switch_Messages_Cnt = sizeof(switch_Message_Strings) / sizeof(switch_Message_Strings[0]);

static char switch_Error_Prefix_Str[]   = "Error: ";
static char switch_Warning_Prefix_Str[] = "Warning: ";
static char switch_Note_Prefix_Str[]    = "Note: ";


/* this array should not be changed because RIO_SWITCH_ERROR_MSG_1, RIO_SWITCH_WARNING_MSG_1
   and RIO_SWITCH_NOTE_MSG_1 are mapped to depending on type of message passed from 
   32 bit txrx */    
static RIO_PL_MSG_TYPE_T switch_Message_To_Type_Map[] = {
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_NOTE_MSG
};

static const int switch_Message_Map_Count = sizeof(switch_Message_To_Type_Map) / sizeof(switch_Message_To_Type_Map[0]);


#define RIO_SWITCH_MAX_ERR_STR 512
#define RIO_SWITCH_MAX_ADD_PAR_CNT 3

static char error_Buffer[RIO_SWITCH_MAX_ERR_STR];
static char temp_Buffer[RIO_SWITCH_MAX_ERR_STR];

/***************************************************************************
 * Function : RIO_Switch_Print_Message
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Print_Message(
    RIO_SWITCH_DS_T *handle, RIO_SWITCH_MESSAGES_T error_Type, ...)
{
    /* 
     * buffer to store error message and pointer to char which is
     * necessary for argument of error type 1 because it's of this
     * type
     */
    char *error_1 = NULL;

    /* 
     * additional information which shall present in error message
     * is passing through additional (optional) parameters:
     * add_Args[0]...[2]. 
     * i - loop counter
     */
    unsigned long add_Args[RIO_SWITCH_MAX_ADD_PAR_CNT] = {0, 0, 0};
    int i;

    /* this is necessary to detect additional parameters */
    va_list pointer_To_Next_Arg;

    /*
     * check all using pointers to be not NULL, check
     * that type of error is within corresponding array
     * and when invoke callback with actual parameters.
     * actual error message obtain using error type as an index
     */
    if (handle == NULL || 
        handle->switch_Callbacks.rio_User_Msg == NULL ||
        (int)error_Type < 0 || 
        (int)error_Type > (switch_Messages_Cnt - 1) ||
        (int)error_Type > (switch_Message_Map_Count - 1))
        return RIO_ERROR;

    /* 
     * get additional parameters, which is int type for all errors,
     * exclude 1 that comes from TxRx (char*)
     */
    va_start(pointer_To_Next_Arg, error_Type);

    for (i = 1; i <= switch_Message_Strings[(int)error_Type].count_Of_Add_Args; i++)
    {
        if (error_Type == RIO_SWITCH_ERROR_1 ||
            error_Type == RIO_SWITCH_WARNING_1 ||
            error_Type == RIO_SWITCH_NOTE_1)
        {
            error_1 = va_arg(pointer_To_Next_Arg, char*);     
        }
        else
        {
            add_Args[i - 1] = va_arg(pointer_To_Next_Arg, unsigned long);     
        }
    }


    va_end(pointer_To_Next_Arg);

    /* obtain actual error message - here dectecting of
     * additional parameters count is doing - case labales mean count itself
     */
    switch (switch_Message_Strings[(int)error_Type].count_Of_Add_Args)
    {
        case 0:
            sprintf(error_Buffer, switch_Message_Strings[(int)error_Type].string,
                handle->instance_Number);
            break;

        case 1:
            if (error_Type == RIO_SWITCH_ERROR_1 ||
                error_Type == RIO_SWITCH_WARNING_1 ||
                error_Type == RIO_SWITCH_NOTE_1)
            {
                sprintf(error_Buffer, switch_Message_Strings[(int)error_Type].string,
                    handle->instance_Number, error_1);
            }
            else
            {
                sprintf(error_Buffer, switch_Message_Strings[(int)error_Type].string,
                    handle->instance_Number, add_Args[0]);
            }
            break;

        case 2:
            sprintf(error_Buffer, switch_Message_Strings[(int)error_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1]);
            break;

        case 3:
            sprintf(error_Buffer, switch_Message_Strings[(int)error_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1], add_Args[2]);
            break;

        default:
            return RIO_ERROR;
        
    }

    switch (switch_Message_To_Type_Map[(int)error_Type])
    {
        case RIO_PL_MSG_WARNING_MSG:
            sprintf(temp_Buffer, switch_Warning_Prefix_Str);
            break;
        
        case RIO_PL_MSG_ERROR_MSG:
            sprintf(temp_Buffer, switch_Error_Prefix_Str);
            break;
        
        case RIO_PL_MSG_NOTE_MSG:
            sprintf(temp_Buffer, switch_Note_Prefix_Str);
            break;
        
        default:
            return RIO_ERROR;
    
    } 
    
    strcat(temp_Buffer, error_Buffer);

    /* print result error message*/
    handle->switch_Callbacks.rio_User_Msg(
        handle->switch_Callbacks.rio_Msg_Context, 
        temp_Buffer);

    return RIO_OK;
  
}


/**************************************************************************/

