/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/rio_switch_rx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Switch model receiver source
*
* Notes: none
*
******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "rio_switch_ds.h"
#include "rio_switch_rx.h"
#include "rio_switch_errors.h"
#include "rio_switch_registers.h"
#include "rio_switch_tx.h"

/* routes data */
static void rio_Switch_Route_Data(
    RIO_SWITCH_DS_T *handle,
    RIO_GRANULE_T   *granule,
    unsigned int    link_Number);

/* try to start new packet routing*/
static void rio_Switch_Start_New_Packet(
    RIO_SWITCH_DS_T *handle,
    RIO_GRANULE_T   *granule,
    unsigned int    link_Number);

/* finish packet routing */
static void rio_Switch_Finish_Packet(
    RIO_SWITCH_DS_T *handle,
    unsigned int    link_Number);

/* aborts packet routing */
static void rio_Switch_Abort_Packet(
    RIO_SWITCH_DS_T *handle,
    unsigned int    link_Number);

/* form packet retry acknowledge */
static void rio_Switch_Get_Retry_Granule(
    RIO_GRANULE_T *retry, unsigned int ack_ID);

/* detect transport info from the stream */
static void rio_Switch_Get_Transp_Info(
    RIO_UCHAR_T *buffer, unsigned long *transp_Info, unsigned int *tt);

/* detect TT field */
static unsigned int rio_Switch_Get_Tt(RIO_UCHAR_T *arbiter);

/* detect link where the packet shall be routed to */
static int rio_Switch_Route_To(
    RIO_SWITCH_DS_T *handle, unsigned long transp_Info, unsigned int tt, unsigned int link_Number);

/* OK receiver state handle */
static void rio_Switch_Handler_Normal_State(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* OK receiver state handle */
static void rio_Switch_Handler_Init_State(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* start receiver retry recovery */
static void rio_Switch_Start_Rx_Retry_Rec(RIO_SWITCH_DS_T *handle, unsigned int ack_ID, unsigned int link_Number);

static void rio_Switch_Start_Rx_Error_Rec(
    RIO_SWITCH_DS_T *handle, unsigned int link_Number, unsigned int status);

/* handle RETRY state in receiver */
static void rio_Switch_Handler_Retry_State
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* handle ERROR state in receiver */
static void rio_Switch_Handler_Error_State
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);

/* detect current link status */
static int rio_Switch_Current_Rx_Status(RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* change state to OK*/
static void rio_Switch_Start_Rx_Normal(RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* detect TTYPE field */
static unsigned int rio_Switch_Get_Ttype(RIO_UCHAR_T *arbiter);

/* detect FTYPE field */
static unsigned int rio_Switch_Get_Ftype(RIO_UCHAR_T *arbiter);

/* detect if packet has request format */
static RIO_BOOL_T rio_Switch_Is_Req(unsigned int ftype, unsigned int ttype);

/* update transport info field in the packet stream */
static void rio_Switch_Update_Tr_Info
    (RIO_UCHAR_T *buffer, unsigned long transp_Info, unsigned int tt);

/* detects last CRC start position */
static unsigned int rio_Switch_Get_CRC_Start
    (RIO_SWITCH_PACKET_BUFFER_T *arbiter, RIO_BOOL_T ext_Address, RIO_BOOL_T ext_Address_16);

/* change receiver state (in register) */
static void rio_Switch_Rx_Cng_St_To_Retry
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

static void rio_Switch_Rx_Cng_St_To_Error
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

static void rio_Switch_Rx_Cng_St_To_Normal
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);


static void rio_Switch_Handler_Pre_Reset_State(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number);


/* mask for receiver state clean */
/*#define RIO_SWITCH_RX_BITS_CLEAN    0xffff8000
#define RIO_SWITCH_RX_STATE_OK            0x00000002
#define RIO_SWITCH_RX_ERROR            0x00000020
#define RIO_SWITCH_RX_RETRY            0x00000010
#define RIO_SWITCH_RX_INITIALIZING    0x00000040    
*/

/* detect hop_count field */
static unsigned int rio_Switch_Get_Hop_Count
    (RIO_UCHAR_T *arbiter, unsigned int tt);

/* try update hop_Count field */
static void rio_Switch_Upd_Hop_Count
    (RIO_UCHAR_T *arbiter, unsigned int tt, unsigned int hop_Count);

/* check received packet */
static int rio_Switch_Check_Packet
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* send PACKET ACCEPTED to the request sender */
static void rio_Switch_Acknowledge_Req
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* perform MAINTANANCE request to the switch */
static void rio_Switch_Commit_Perform_Req
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number);

/* detect prio field */
static unsigned int rio_Switch_Get_Prio(RIO_UCHAR_T *arbiter);

/* convert stream to dwords */
static void rio_Switch_Stream_To_Dw(RIO_DW_T *dws, RIO_UCHAR_T *stream, unsigned int size);

/* performs external request */
static void rio_Switch_Ext_Req
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number, RIO_REMOTE_REQUEST_T *req, unsigned long tr_Info);

/* send response */
static void rio_Switch_Send_Response
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number, RIO_REMOTE_REQUEST_T *req, unsigned int status, unsigned long tr_Info);

/* convert dwords to the stream */
static void rio_Switch_Conv_Dw_Char(RIO_DW_T *dw, RIO_UCHAR_T *stream);

static void rio_Switch_Packet_Is_Not_Sent(RIO_SWITCH_DS_T *handle, unsigned int link_Number);



static int rio_Switch_In_Check_Current(RIO_SWITCH_DS_T *handle,
                            RIO_BOOL_T* error_Flag,
                            RIO_PACKET_VIOLATION_T *violation_Type,
                            unsigned int link_Number);

static unsigned int rio_Switch_Check_Ftype(RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Check_Tt(RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Check_Ttype(RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Check_CRC(RIO_SWITCH_DS_T *handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Get_Header_Size(RIO_SWITCH_DS_T * handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Check_Header_Size(RIO_SWITCH_DS_T * handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Check_Payload_Align(RIO_SWITCH_DS_T * handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Check_Reserved_Fields( RIO_SWITCH_DS_T *handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter);

static unsigned int rio_Switch_Check_Reserved_Field(
    RIO_SWITCH_PACKET_BUFFER_T *arbiter,
    unsigned int field_Pos_Start,
    unsigned int field_Size
    );

static void rio_Switch_Check_States(RIO_SWITCH_DS_T *handle, unsigned int link_Number);


/* max payload size */
#define RIO_SWITCH_MAX_PAYLOAD_DWORDS  32

/***************************************************************************
 * Function : RIO_Switch_Rx_Granule_Received
 *
 * Description: accepts a granule received from the link
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Rx_Granule_Received(
    RIO_SWITCH_DS_T *handle,
    RIO_GRANULE_T   *granule,
    unsigned int    link_Number)
{

    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /*check states in regs - new added*/
    rio_Switch_Check_States(handle, link_Number);

    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).idle_watch = RIO_FALSE;


    switch (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State)
    {
        case RIO_SWITCH_RESET_RESET:
            return;
        case RIO_SWITCH_RESET_PRE_RESET:
            rio_Switch_Handler_Pre_Reset_State(handle, granule, link_Number);

            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State == RIO_SWITCH_RESET_POST_RESET)
            {
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State = RIO_SWITCH_RESET_OK;
                return; 
            }
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State != RIO_SWITCH_RESET_OK)
                return; 
            
            break;
        case RIO_SWITCH_RESET_OK:
	case RIO_SWITCH_RESET_POST_RESET:
	    break;
    }
    /* actions detect from the type of granule */
    switch (granule->granule_Type)
    {
        /*
         * if an acknowledgment is received we pass it to the transmitter
         * directly, so break here is only one
         */
        case RIO_GR_PACKET_ACCEPTED:
        case RIO_GR_PACKET_RETRY:
        case RIO_GR_PACKET_NOT_ACCEPTED:
            RIO_Switch_Tx_Rec_Ack(handle, granule, link_Number);
            break;

       /* pass THROTTLE to the transmitter */
        case RIO_GR_THROTTLE:
            RIO_Switch_Tx_Rec_Throttle(handle, granule, link_Number); 
            break;

        /* pass link response to the transmitter */
        case RIO_GR_LINK_RESPONSE:
            
            RIO_Switch_Tx_Rec_Link_Resp(handle, granule, link_Number);
            break;

        /* switch does not react on symbols of these types */
        case RIO_GR_TOD_SYNC:
            break; 
     
        default:
            {
                /* if it's RESET command go through reset */
                if (granule->granule_Type == RIO_GR_LINK_REQUEST)
                {
                    RIO_BOOL_T is_Expected_Result;

                    if ((is_Expected_Result = RIO_Switch_Tx_Control_Is_Expected(handle, granule, link_Number)) == RIO_FALSE)
                    {
                        char error_Buffer[255];
                        sprintf(error_Buffer, "Output error detected on link N %lu : unexpected Link Request.\n", 
                            (unsigned long)link_Number);
                        RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
                        break;
                    }
                    
                    if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_RESET)
                    {
 /*                       handle->reset_Function((RIO_HANDLE_T)handle);
                        RIO_Switch_Internal_Init(handle);
                        break;
*/
                        if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State == RIO_SWITCH_RESET_OK)
                        {
                            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State = RIO_SWITCH_RESET_PRE_RESET;
                            /*RIO_PL_Print_Warning(handle, RIO_PL_WARNING_7,
                                handle->rx_Data.train_state);*/
                        }
                    }
                    else if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING )
                    {
                    
                        /*RIO_GRANULE_T training;
                        RIO_Switch_Tx_Send_Training(handle, &training, RIO_SWITCH_TRAIN_NUM, link_Number);   */
                    /*    RIO_Switch_Tx_Rec_Link_Req(handle, granule,link_Number);*/
                    }
                    else if (granule->granule_Struct.ack_ID == RIO_LP_LREQ_INPUT_STATUS)
                    {
                        /* send link response in any way */
                        RIO_GRANULE_T link_Response;

                        link_Response.granule_Type = RIO_GR_LINK_RESPONSE;
                        link_Response.granule_Struct.s = 1;
                        link_Response.granule_Struct.ack_ID = RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID;
                        link_Response.granule_Struct.stype = RIO_GR_LINK_RESPONSE_STYPE;
                        link_Response.granule_Struct.buf_Status = rio_Switch_Current_Rx_Status(handle, link_Number);
                        RIO_Switch_Tx_Send_Link_Resp(handle, &link_Response, link_Number);
                    }
                }
                
                if (granule->granule_Type == RIO_GR_TRAINING) 
                {
                }

                if (granule->granule_Type == RIO_GR_IDLE || granule->granule_Type == RIO_GR_EOP)
                {
                    RIO_Switch_Set_State(handle,
                        RIO_PARTNER_BUF_STATUS, granule->granule_Struct.buf_Status, 0, link_Number);
                }

                /* other control and data performing depends of receiver state */
                switch (handle->link_Data[link_Number].rx_Data.global_State)
                {
                    /* here normal data proceding */
                    case RIO_SWITCH_LINK_OK:
                        rio_Switch_Handler_Normal_State(handle, granule, link_Number);
                        break;

                    /* here statamachine for error state */
                    case RIO_SWITCH_LINK_STOPPED_DUE_TO_ERROR:
                        rio_Switch_Handler_Error_State(handle, granule, link_Number);
                        break;
                    
                    /* here state machine for retry state */
                    case RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY:
                        rio_Switch_Handler_Retry_State(handle, granule, link_Number);
                        break;
                    case RIO_SWITCH_LINK_UNINIT:
/*                    case RIO_SWITCH_LINK_INITIALIZING:
                    rio_Switch_Handler_Init_State(handle, granule,link_Number);
*/
                    break;
                    
                    default: {}
                }
            }
    }
}

/***************************************************************************
 * Function : RIO_Switch_Rx_Init
 *
 * Description: initialize the receiver
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Rx_Init(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned int i; 
    unsigned long state; /* loop counter and state*/

    if (handle == NULL)
        return;

    i = link_Number;
    /* initialize */
/*   for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
*/
        /* initialize state */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).state = RIO_SWITCH_LINK_FREE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).global_State = RIO_SWITCH_LINK_UNINIT;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).connected_To = RIO_SWITCH_NOT_CONNECTED;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).expected_Ack_ID = 0;
/*?????    RIO_SWITCH_GET_LNK_RX_DATA(handle, i).receiving_Ack_ID = 7;*/
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).conf_Req = RIO_FALSE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).initialized = RIO_FALSE;


        /*set corresponding states*/
        /*set state next expected by the attached device in the packet*/
        RIO_Switch_Set_State(
            handle,
            RIO_INBOUND_ACKID, 
            RIO_SWITCH_GET_LNK_RX_DATA(handle, i).expected_Ack_ID,
            0,
            i);

        /* initialize RESET latch */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).reset_Req_Counter.valid = RIO_FALSE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).reset_Req_Counter.reset_Num = 0;
        
        /*state of receiver state machine*/
    /*    if (handle->requires_Window_Alignment_Params[i] == RIO_TRUE)
             RIO_SWITCH_GET_LNK_RX_DATA(handle, i).train_state = RIO_SWITCH_RX_WAIT_GOOD_PTTN;
        else
             RIO_SWITCH_GET_LNK_RX_DATA(handle, i).train_state = RIO_SWITCH_RX_WAIT_FOR_IDLE;
    */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).reset_Machine_State = RIO_SWITCH_RESET_OK;

        RIO_SWITCH_GET_LNK_DATA(handle, i)->tx_Data.global_State = RIO_SWITCH_LINK_OK;  

        RIO_SWITCH_GET_LNK_DATA(handle, i)->rx_Data.global_State = RIO_SWITCH_LINK_OK;
    
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).initialized = RIO_TRUE;
        RIO_Switch_Read_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr
            + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A, &state, RIO_TRUE);

        state &= RIO_SWITCH_ERROR_PORT_STATE_CLEAR_MASK;
        state |= RIO_SWITCH_ERROR_STATE_OK;
        RIO_Switch_Write_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr
                    + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A, state, RIO_TRUE);

/*        RIO_PL_Print_Warning(handle, RIO_PL_WARNING_7,
             RIO_SWITCH_GET_LNK_RX_DATA(handle, i).train_state); */

        /* initialize buffer */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).buffer.buffer_Top = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).buffer.crc_Pos = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).buffer.need_Recalc_CRC = RIO_FALSE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).buffer.buffer_Top_For_Load = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).buffer.buffer_Head = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, i).buffer.counting_CRC = RIO_SWITCH_INIT_CRC;

        /* change receiver state in the register */
    /*    RIO_Switch_Read_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr + 
            i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A, &rec_State, RIO_TRUE);*/
        /* clear receiver bits*/
        /*rec_State &= RIO_SWITCH_RX_BITS_CLEAN;*/
        /* set initializing bit */
        /*rec_State |= RIO_SWITCH_RX_INITIALIZING;
        RIO_Switch_Write_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr + 
            i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A, rec_State, RIO_TRUE);*/

/*    }*/
}

/***************************************************************************
 * Function : rio_Switch_Route_Data
 *
 * Description: routes data
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Route_Data(
    RIO_SWITCH_DS_T *handle,
    RIO_GRANULE_T   *granule,
    unsigned int    link_Number)
{
    unsigned int granule_Num, tt;
    /* ftype of packet */
    unsigned int ftype;


    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle) || granule == NULL)
        return;

    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state != RIO_SWITCH_LINK_WORK)
    {
        rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_GENERAL_ERROR);
        return;
    }

    /* check if we can accept this data */
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load >= RIO_SWITCH_BUFFER_SIZE)
    {
        
        rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_GENERAL_ERROR);
        return;
    }

    /* load data */
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load++] = 
        granule->granule_Date[0]; /* 0 byte */

    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load++] = 
        granule->granule_Date[1]; /* 1 byte */

    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load++] = 
        granule->granule_Date[2]; /* 2 byte */

    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load++] = 
        granule->granule_Date[3]; /* 3 byte */

    /* detect TT */
    tt = rio_Switch_Get_Tt(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer);
    ftype = rio_Switch_Get_Ftype(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer);

    /* detect number of received granule */
    granule_Num = RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load / RIO_SWITCH_CNT_BYTE_IN_GRAN;

    /* add latency for proper intermediate CRC detection detection*/
    if (granule_Num != RIO_SWITCH_GRAN_WITH_CRC && granule_Num != 2) 
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top += RIO_SWITCH_CNT_BYTE_IN_GRAN;

    /* in the first granule clear the first byte */
    if (granule_Num == 1)
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[0] = 0;

    /* detect hop count field in 2 granule for tr_info 16 buts and 
     * in 3 granule if transport info is 32 bits
     */
    if ( (granule_Num == 3 && tt == RIO_PL_TRANSP_32) ||
        (granule_Num == 2 && tt == RIO_PL_TRANSP_16) )
    {
        unsigned long transp_Info = 0;
        unsigned int need_Be_Routed = 0;
        unsigned int ttype = 
            rio_Switch_Get_Ttype(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer); 
        
        /* check if we need recalculate CRC */
        if (handle->init_Param_Set.rout_Alg == RIO_INDEX_BASED_ROUTING ||
            handle->init_Param_Set.rout_Alg == RIO_SHIFT_BASED_ROUTING)
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_TRUE;

        if (ftype == RIO_MAINTENANCE && rio_Switch_Is_Req(ftype, ttype) == RIO_OK)
        {
            unsigned int hop_Count = 
                rio_Switch_Get_Hop_Count(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer, tt);
            
            /* detect if it's our packet */
            if (!hop_Count)
            {
                /* check if transmitter already has a response for transmittion */
                if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_TRUE)
                    rio_Switch_Abort_Packet(handle, link_Number);
                else
                    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).conf_Req = RIO_TRUE;
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_FALSE;
            }
            else
            {
                rio_Switch_Upd_Hop_Count(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer, tt, --hop_Count);
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_TRUE;
                need_Be_Routed = 1;
            }
        }
        else if (ftype == RIO_MAINTENANCE && (ttype == RIO_MAINTENANCE_READ_RESPONCE ||
            ttype == RIO_MAINTENANCE_WRITE_RESPONCE) )
        {
            unsigned int hop_Count = 
                rio_Switch_Get_Hop_Count(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer, tt);
            rio_Switch_Upd_Hop_Count(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer, tt, --hop_Count);
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_TRUE;
            need_Be_Routed = 1;
        }
        else
            need_Be_Routed = 1;

        /* route packet */
        if (need_Be_Routed)
        {
            /* detect transport info */
            rio_Switch_Get_Transp_Info(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer, &transp_Info, &tt);
            
            /* try to detect proper link for transmission */
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To = 
                rio_Switch_Route_To(handle, transp_Info, tt, link_Number);

            /* if RIO_SWITCH_NOT_CONNECTED target link hasn't been recognized */
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To == RIO_SWITCH_NOT_CONNECTED)
            {
                rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_GENERAL_ERROR);
                return;
            }
        }
    }

    /* check if we need insert intermadiate CRC */
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load == RIO_SWITCH_PACK_NEED_INT_CRC &&
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC == RIO_TRUE)
    {
        /* compute intermediate CRC which finishes in position 80*/
        RIO_Switch_Compute_CRC(&RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC, 
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer, RIO_SWITCH_INT_CRC_POS);

        /* new CRC is started from 82 position */
   /*     RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos = RIO_SWITCH_INT_CRC_POS + 2;*/
        /* new CRC is started from 80 position */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos = RIO_SWITCH_INT_CRC_POS;

        /* update int CRC in position 80 mask 0xff00 - most significant byte shift 8 to the right */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[RIO_SWITCH_INT_CRC_POS] = 
            (RIO_UCHAR_T)((RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC & 0xff00) >> 8);

        /* update int CRC in position 81 mask 0x00ff - less significant byte*/
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[RIO_SWITCH_INT_CRC_POS + 1] = 
            (RIO_UCHAR_T)(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC & 0x00ff);
    }

}

/***************************************************************************
 * Function : rio_Switch_Start_New_Packet
 *
 * Description: try to start new packet routing
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Start_New_Packet(
    RIO_SWITCH_DS_T *handle,
    RIO_GRANULE_T   *granule,
    unsigned int    link_Number)
{
    unsigned int res;
    
    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle) || granule == NULL)
        return;

    /* check if the link is busy - finish current packet */
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_WORK)
    {
        rio_Switch_Finish_Packet(handle, link_Number);

        /* if packet has been retried exit */
        if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State !=
            RIO_SWITCH_LINK_OK)
            return;
    }

   
    /* check if link is blocked - send PACKET_RETRY*/
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state != RIO_SWITCH_LINK_FREE)
    {
        rio_Switch_Start_Rx_Retry_Rec(handle, granule->granule_Struct.ack_ID, link_Number);
        return;
    }

     /*new added - check for correct ackID in the packet*/
    if (granule->granule_Struct.ack_ID != RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID)
    {
        rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_UNEXPECTED_ACKID);
        return;
    }


    if ((res = RIO_Switch_Tx_Start_Packet_Ack(handle, granule, link_Number)) == RIO_ERROR)
    {
        rio_Switch_Start_Rx_Retry_Rec(handle, granule->granule_Struct.ack_ID, link_Number);
        return;
    }

    
    /* start working with new packet */
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_WORK;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To = RIO_SWITCH_NOT_CONNECTED; /* not connected */
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID = granule->granule_Struct.ack_ID;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_FALSE;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC = RIO_SWITCH_INIT_CRC;


    /* load data */
    rio_Switch_Route_Data(handle, granule, link_Number);
}

/***************************************************************************
 * Function : rio_Switch_Finish_Packet
 *
 * Description: finish packet routing
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Finish_Packet(
    RIO_SWITCH_DS_T *handle,
    unsigned int    link_Number)
{
    unsigned int status;

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    status = rio_Switch_Check_Packet(handle, link_Number); /* status of packet checking */


    /* check current */
    if (status != RIO_PL_PNACC_OK)
    {
        /* send NOT ACCEPTED acknowledge and reset link */
        rio_Switch_Start_Rx_Error_Rec(handle, link_Number, status);
        return;
    }

    /* check if packet is a configuration request to the switch */
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).conf_Req == RIO_TRUE)
    {
            
        /* send acknowledge */
        rio_Switch_Acknowledge_Req(handle, link_Number);

        /* commit current */
        rio_Switch_Commit_Perform_Req(handle, link_Number);

        /* change state */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To = RIO_SWITCH_NOT_CONNECTED; /* not connected */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).conf_Req = RIO_FALSE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID = 
            (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID + 1) % 
            handle->ack_Buffer_Size;
        
        RIO_Switch_Set_State(
            handle,
            RIO_INBOUND_ACKID, 
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID,
            0,
            link_Number);
        return;
    }

    /* check if packet is currently sending */
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head) 
/*    if (1) */
    {
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_BLOCKED;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top = 
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load;

        /* find final CRC */

        if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC == RIO_TRUE)
        {
            unsigned int final_CRC_Pos;
            
            final_CRC_Pos = rio_Switch_Get_CRC_Start(&(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer),
                handle->init_Param_Set.ext_Address, handle->init_Param_Set.ext_Address_16);
            
            /* insert final CRC */
            if (final_CRC_Pos <= RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos)
                return;
            
            RIO_Switch_Compute_CRC(&RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC,
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer +
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos,
                final_CRC_Pos - RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos);
            
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[final_CRC_Pos] = 
                (RIO_UCHAR_T)((RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC & 0xff00) >> 8);
            
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[final_CRC_Pos + 1] = 
                (RIO_UCHAR_T)(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC & 0x00ff);

            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).waiting_Time = 0;
        }
        return;
    }
    else
    {
        rio_Switch_Packet_Is_Not_Sent(handle, link_Number);
    }
}

/***************************************************************************
 * Function : rio_Switch_Packet_Is_Not_Sent
 *
 * Description: finish packet routing
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Packet_Is_Not_Sent(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    /* else clear packet and send PACKET_RETRY */
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_FALSE;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load = 0;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC = RIO_SWITCH_INIT_CRC;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To = RIO_SWITCH_NOT_CONNECTED;
    rio_Switch_Start_Rx_Retry_Rec(handle, RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID, link_Number);
}

/***************************************************************************
 * Function : RIO_Switch_Rx_Count_Waiting
 *
 * Description: finish packet routing
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Rx_Count_Waiting(RIO_SWITCH_DS_T *handle)
{
    unsigned int i;

    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        if (RIO_SWITCH_GET_LNK_RX_DATA(handle, i).state == RIO_SWITCH_LINK_BLOCKED &&
            !RIO_SWITCH_GET_LNK_RX_DATA(handle, i).buffer.buffer_Head)
        {
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, i).waiting_Time >= 16)
                rio_Switch_Packet_Is_Not_Sent(handle, i);
            else
                RIO_SWITCH_GET_LNK_RX_DATA(handle, i).waiting_Time++;
        }
    }
}

/***************************************************************************
 * Function : rio_Switch_Abort_Packet
 *
 * Description: abort packet routing by receiving LINK_REQUEST, STOMP, etc.
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Abort_Packet(
    RIO_SWITCH_DS_T *handle,
    unsigned int    link_Number)
{
    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* detect if packet is transmitting */
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head)
    {
        /* send STOMP to the receiver */
        RIO_Switch_Tx_STOMP_Transmission(handle, 
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To);
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_BLOCKED;
    }
    else
    {
        /* send retry acknowledge to the sender */
/*        rio_Switch_Start_Rx_Retry_Rec(handle, RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID, link_Number);*/
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_FALSE;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load = 0;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC = RIO_SWITCH_INIT_CRC;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To = RIO_SWITCH_NOT_CONNECTED;

        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress = RIO_FALSE;
        RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
    }
}

/***************************************************************************
 * Function : rio_Switch_Get_Retry_Granule
 *
 * Description: abort packet routing
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Get_Retry_Granule(
    RIO_GRANULE_T *retry, unsigned int ack_ID)
{
    if (retry == NULL || ack_ID > RIO_SWITCH_TX_ACK_POOL_MAX_SIZE)
        return;

    /* form acknowledge */
    retry->granule_Struct.ack_ID = ack_ID;
    retry->granule_Struct.s = 1;
    retry->granule_Struct.buf_Status = 0;
    retry->granule_Struct.secded = 0;
    retry->granule_Struct.stype = RIO_GR_PACKET_RETRY_STYPE;
    retry->granule_Type = RIO_GR_PACKET_RETRY;
}

/***************************************************************************
 * Function : rio_Switch_Get_Transp_Info
 *
 * Description: detect transport info from the stream
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Get_Transp_Info(
    RIO_UCHAR_T *arbiter, unsigned long *transp_Info, unsigned int *tt)
{

    if (arbiter == NULL || transp_Info == NULL)
        return;

    if ((*tt = rio_Switch_Get_Tt(arbiter)) == RIO_PL_TRANSP_16)
        *transp_Info = (((unsigned long)arbiter[2]) << 8) |
            ((unsigned long)arbiter[3]);
    else
        *transp_Info = (((unsigned long)arbiter[2]) << 24) |
            (((unsigned long)arbiter[3]) << 16) |
            (((unsigned long)arbiter[4]) << 8) |
            ((unsigned long)arbiter[5]);
}

/***************************************************************************
 * Function : rio_Switch_Get_Tt
 *
 * Description: detect TT field
 *
 * Returns: TT
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Get_Tt(RIO_UCHAR_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return (arbiter[1] & 0x30) >> 4;
}

/***************************************************************************
 * Function : rio_Switch_Route_To
 *
 * Description: detect target link 
 *
 * Returns: link number or RIO_SWITCH_NOT_CONNECTED if it hasn't been detected 
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Route_To(
    RIO_SWITCH_DS_T *handle, unsigned long transp_Info, unsigned int tt, unsigned int link_Number)
{
    if (handle == NULL)
        return RIO_SWITCH_NOT_CONNECTED;

    /* other work depends of routing algoriphm */
    if (handle->init_Param_Set.rout_Alg == RIO_TABLE_BASED_ROUTING)
    {
        unsigned long target_ID;
        unsigned int i;

        if (tt == RIO_PL_TRANSP_16)
        {
            target_ID = transp_Info & 0x0000ff00;
            target_ID >>= 8;

        }
        else
        {
            target_ID = transp_Info & 0xffff0000;
            target_ID >>= 16;
        }

        /* look through routing table */
        for (i = 0; i < handle->init_Param_Set.rout_Table_Size; i++)
        {
            if (target_ID == handle->init_Param_Set.routing_Table[i].target_ID)
            {
                return handle->init_Param_Set.routing_Table[i].target_Port_Number < RIO_SWITCH_GET_PORTS_CNT(handle) ? 
                    handle->init_Param_Set.routing_Table[i].target_Port_Number : RIO_SWITCH_NOT_CONNECTED;
            }
        }
        /* target link hasn't been found */
        return RIO_SWITCH_NOT_CONNECTED;
    }
    else if (handle->init_Param_Set.rout_Alg == RIO_SHIFT_BASED_ROUTING)
    {
        /* buffer where the packet is accumulating */
        RIO_UCHAR_T *buffer = RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer;

        /* ftype, ttype fileds of packet and port number for routing to */
        unsigned int ftype = rio_Switch_Get_Ftype(buffer),
            ttype = rio_Switch_Get_Ttype(buffer),
            port_Num;

        /* detect if packet is a request */
        if (rio_Switch_Is_Req(ftype, ttype) == RIO_OK)
        {
            /* port number is n-msb bits */
            port_Num = (tt == RIO_PL_TRANSP_16) ? transp_Info >> (16 - RIO_SWITCH_GET_ENC_SIZE(handle)) : 
                transp_Info >> (32 -  RIO_SWITCH_GET_ENC_SIZE(handle));

            /* update transport info field */
            transp_Info <<= RIO_SWITCH_GET_ENC_SIZE(handle);
            transp_Info |= (link_Number & ((1 << RIO_SWITCH_GET_ENC_SIZE(handle)) - 1));
        }
        else
        {
            /* detect */
            port_Num = transp_Info & ((1 << RIO_SWITCH_GET_ENC_SIZE(handle)) - 1);

            /* update transport info */
            transp_Info >>= RIO_SWITCH_GET_ENC_SIZE(handle);

            transp_Info |= ((link_Number & ((1 << RIO_SWITCH_GET_ENC_SIZE(handle)) - 1)) << 
                ((tt == RIO_PL_TRANSP_16) ? 16 - RIO_SWITCH_GET_ENC_SIZE(handle) : 
                32 -  RIO_SWITCH_GET_ENC_SIZE(handle)));
        }
        /* check if port num is proper */
        if (port_Num >= RIO_SWITCH_GET_PORTS_CNT(handle))
            return RIO_SWITCH_NOT_CONNECTED;

        /* update transport info */
        rio_Switch_Update_Tr_Info(buffer, transp_Info, tt);

        return port_Num;
    }
    else if (handle->init_Param_Set.rout_Alg == RIO_INDEX_BASED_ROUTING)
    {
        /* buffer where the packet is accumulating */
        RIO_UCHAR_T *buffer = RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer;

        /* ftype, ttype fileds of packet and port number for routing to */
        unsigned int ftype = rio_Switch_Get_Ftype(buffer),
            ttype = rio_Switch_Get_Ttype(buffer),
            port_Num, 
            index = transp_Info & 0x1f;
        unsigned long path = (tt == RIO_PL_TRANSP_16 ? (transp_Info & 0xffe0) :
            (transp_Info & 0xffffffe0)) >> 5;

        unsigned long mask = ((1 << RIO_SWITCH_GET_ENC_SIZE(handle)) - 1);

        /* if response decrease index before locating  */
        if (rio_Switch_Is_Req(ftype, ttype) == RIO_ERROR)
            index -= RIO_SWITCH_GET_ENC_SIZE(handle);

        /* locate number of target port */
        port_Num = (path >> 
            (tt == RIO_PL_TRANSP_16 ? 
            (11 - RIO_SWITCH_GET_ENC_SIZE(handle) - index) :
            (27 - RIO_SWITCH_GET_ENC_SIZE(handle) - index)));
        
        port_Num &= mask;

        /* set incoming port number to the path */
        mask <<= (tt == RIO_PL_TRANSP_16 ? 
            (11 - index - RIO_SWITCH_GET_ENC_SIZE(handle)) :
            (27 - index - RIO_SWITCH_GET_ENC_SIZE(handle)));

        /* inverce mask to clean port number bits */
        mask = ~mask;

        /* clear port number */
        path &= mask;

        /* prepare incoming link number */
        mask = ((1 << RIO_SWITCH_GET_ENC_SIZE(handle)) - 1);
        mask &= link_Number;
        mask <<= (tt == RIO_PL_TRANSP_16 ? 
            (11 - index - RIO_SWITCH_GET_ENC_SIZE(handle)) :
            (27 - index - RIO_SWITCH_GET_ENC_SIZE(handle)));

        path |= mask;

        /* if request increase index after locating */
        if (rio_Switch_Is_Req(ftype, ttype) == RIO_OK)
            index += RIO_SWITCH_GET_ENC_SIZE(handle);

        /* check if port num is proper */
        if (port_Num >= RIO_SWITCH_GET_PORTS_CNT(handle))
            return RIO_SWITCH_NOT_CONNECTED;

        /* form new transport info field */
        if (tt == RIO_PL_TRANSP_16)
            transp_Info = ((path & 0x7ff) << 5) + (index & 0x1f);
        else
            transp_Info = ((path & 0x7ffffff) << 5) + (index & 0x1f);

        /* update transport info */
        rio_Switch_Update_Tr_Info(buffer, transp_Info, tt);

        return port_Num;
    }

    return RIO_SWITCH_NOT_CONNECTED;
}

/***************************************************************************
 * Function : handler_Normal_State
 *
 * Description: state machine for OK state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Handler_Normal_State(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* detect which type of granule has been received */
    switch (granule->granule_Type)
    {
        case RIO_GR_IDLE:
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).idle_watch = RIO_TRUE;
            break;
    
        case RIO_GR_NEW_PACKET:
            rio_Switch_Start_New_Packet(handle, granule, link_Number);
           
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress = RIO_TRUE;
            RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_TRUE, 0, link_Number);
  
            break;
            
        case RIO_GR_EOP:
            if (handle->link_Data[link_Number].rx_Data.in_Progress == RIO_TRUE)
            {
                handle->link_Data[link_Number].rx_Data.in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            else
            {
                char error_Buffer[255];
                sprintf(error_Buffer, "Output error detected on link N %lu : unexpected EOP.\n",
                    (unsigned long)link_Number);
                RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);

                RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);
            }

            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_WORK)
            {
                rio_Switch_Finish_Packet(handle, link_Number);
            }
            break;

         /* next part of data has been received*/
        case RIO_GR_DATA:
            rio_Switch_Route_Data(handle, granule, link_Number);
            break;

        case RIO_GR_LINK_REQUEST:
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress == RIO_TRUE)
            {
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state != RIO_SWITCH_LINK_WORK)
                break;
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head)
            {
                /* send STOMP to the receiver */
                RIO_Switch_Tx_STOMP_Transmission(handle, 
                    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To);
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_BLOCKED;
            }
            else
            {
                /* send retry acknowledge to the sender */
                /*rio_Switch_Start_Rx_Retry_Rec(handle,
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID, link_Number);*/
                
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top = 0;
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.need_Recalc_CRC = RIO_FALSE;
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.crc_Pos = 0;
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load = 0;
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC = RIO_SWITCH_INIT_CRC;
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To = RIO_SWITCH_NOT_CONNECTED;
            }
            break;
        case RIO_GR_STOMP:
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress == RIO_TRUE)
            {
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            else
            {
                char error_Buffer[255];
                sprintf(error_Buffer, "Output error detected on link N %lu : unexpected STOMP.\n",
                    (unsigned long)link_Number);
                RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
                RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);
            }

            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_WORK)
            {
                rio_Switch_Abort_Packet(handle, link_Number);
            }
            break;
        case RIO_GR_RESTART_FROM_RETRY:
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress == RIO_TRUE)
            {
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);

            }
            /*to set error flag*/
            {
                char error_Buffer[255];
                sprintf(error_Buffer, "Output error detected on link N %lu : unexpected RESTART_FROM_RETRY.\n",
                    (unsigned long)link_Number);
                RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
                rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_GENERAL_ERROR);
                /*RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);*/
            }
            /*Check that this condition will never fit*/
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_WORK)
            {
                rio_Switch_Abort_Packet(handle, link_Number);    
            }
            
            break;

        /* if granule has been corrupted go through error recovery */
        case RIO_GR_HAS_BEEN_CORRUPTED:
            rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_BAD_CONTROL);
            break;
        case RIO_GR_S_BIT_CORRUPTED:
            rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_BAD_S_BIT_PARITY);
            break;
        default: {}
    }
}

/***************************************************************************
 * Function : handler_Init_State
 *
 * Description: state machine for Initialization and Uninitialized state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Handler_Init_State(
            RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{

    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* detect which type of granule has been received */
    switch (granule->granule_Type)
    {
                /*
         * 1) IDLE is out of a packet receiving drop it else 
         * if limit is overstepped cancel packet. 
         * 2) LINK_REQUEST, STOMP and RESTART_FROM_RETRY cancel packet receiving
         * 3) unexpected control start error recovery
         *         ONLY ONE BREAK
         */
        case RIO_GR_TRAINING:
            
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State == RIO_SWITCH_LINK_UNINIT)
            {

                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State = RIO_SWITCH_LINK_INITIALIZING;

                /* change receiver state in the register */
                /*RIO_Switch_Read_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr
                    + link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A, &rec_State, RIO_TRUE);*/
                /* clear receiver bits*/
                /*rec_State &= RIO_SWITCH_RX_BITS_CLEAN;*/
                /* set initializing bit */
                /*rec_State |= RIO_SWITCH_RX_INITIALIZING;
                RIO_Switch_Write_Register(handle, handle->inst_Param_Set.entry_Extended_Features_Ptr +
                    link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A, rec_State, RIO_TRUE);*/
            }
            break;

        case RIO_GR_IDLE:
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).idle_watch = RIO_TRUE;
            break;

        default: {}
        /* if granule has been corrupted go through error recovery */
     
    }

}

/***************************************************************************
 * Function : start_Rx_Retry_Rec
 *
 * Description: turn receiver to stopped due to retry state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Start_Rx_Retry_Rec(RIO_SWITCH_DS_T *handle, unsigned int ack_ID, unsigned int link_Number)
{
    /* acknowledge for sending*/
    RIO_GRANULE_T retry;
   
    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle)) 
        return;

    /* turn state to stopped due to retry*/
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State = RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY;
    
    /* form packet retry acknowledge and send it */
    rio_Switch_Get_Retry_Granule(&retry, ack_ID);
    RIO_Switch_Tx_Send_Ack(handle, link_Number, &retry);

    /* change register */
    rio_Switch_Rx_Cng_St_To_Retry(handle, link_Number);

}

/***************************************************************************
 * Function : start_Rx_Error_Rec
 *
 * Description: turn receiver to stopped due to error state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Start_Rx_Error_Rec(
    RIO_SWITCH_DS_T *handle, unsigned int link_Number, unsigned int status)
{
    char error_Buffer[255];
    RIO_GRANULE_T not_Accepted;


    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;
      
    switch (status)
    {
    case RIO_PL_PNACC_INTERNAL_ERROR:
        sprintf(error_Buffer, "Input error detected on link N %lu : internal error.\n",
            (unsigned long)link_Number);
        break;
    case RIO_PL_PNACC_UNEXPECTED_ACKID:
        sprintf(error_Buffer, "Input error detected on link N %lu : unexpected ackID in packet.\n",
            (unsigned long)link_Number);
        break;
    case RIO_PL_PNACC_BAD_CONTROL:
        sprintf(error_Buffer, "Input error detected on link N %lu : bad control received.\n",
            (unsigned long)link_Number);
        break;
    case RIO_PL_PNACC_BAD_CRC:
        sprintf(error_Buffer, "Input error detected on link N %lu : bad CRC in packet.\n",
            (unsigned long)link_Number);
        break;
    case RIO_PL_PNACC_GENERAL_ERROR:
        sprintf(error_Buffer, "Input error detected on link N %lu : general error.\n",
            (unsigned long)link_Number);
        break;
    case RIO_PL_PNACC_BAD_S_BIT_PARITY:
        if (handle->is_Parallel_Model == RIO_TRUE)
        {
            sprintf(error_Buffer, "Input error detected on link N %lu : s-bit parity error.\n",
                (unsigned long)link_Number);
        }
        else
        {
            sprintf(error_Buffer, "Input error detected on link N %lu : received invalid character, or valid but illegal.\n",
                (unsigned long)link_Number);
        }
        break;
    case RIO_PL_PNACC_INPUT_STOPPED:
        sprintf(error_Buffer, "Input error detected on link N %lu : packet is received in the input stopped state.\n",
            (unsigned long)link_Number);
        break;
    }

    RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Buffer);
    
    /* packet is sending */
    if ( (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_WORK &&
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head) ||
        (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state == RIO_SWITCH_LINK_BLOCKED &&
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head != 
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load)
        )
    {
        /*aborts packet in case it is already transmitting to another link*/
        rio_Switch_Abort_Packet(handle, link_Number);
       /* return;*/
    }
    /*else
    {*/
    /*send PNA and change state*/

        /* packet is only receiving or link is free or packet is sent */
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State = RIO_SWITCH_LINK_STOPPED_DUE_TO_ERROR;
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;

        /*
         * send not_accepted with ackID value based on the type of error 
         * i.e. expected ackID value for packet errors and unexpected ackID
         * (expected ackID + 1) for corrupted packet errors
         */
        not_Accepted.granule_Struct.s = 1;
        not_Accepted.granule_Struct.stype = RIO_GR_PACKET_NOT_ACCEPTED_STYPE;
        not_Accepted.granule_Struct.buf_Status = status;

        if (handle->is_Parallel_Model == RIO_TRUE)
            not_Accepted.granule_Struct.buf_Status = status;
        else
        {
            if (status == RIO_PL_PNACC_INTERNAL_ERROR)
                status = RIO_PL_PNACC_GENERAL_ERROR;

            not_Accepted.granule_Struct.buf_Status = (status == 0xf ? (0x1f) : (status & (0x7)));
        }

        not_Accepted.granule_Type = RIO_GR_PACKET_NOT_ACCEPTED;
        /* define an appropriate ackID value and insert into the packet */
        switch (status)
        {
        case RIO_PL_PNACC_INTERNAL_ERROR:
        case RIO_PL_PNACC_UNEXPECTED_ACKID:
        case RIO_PL_PNACC_BAD_CRC:
        case RIO_PL_PNACC_GENERAL_ERROR:
            not_Accepted.granule_Struct.ack_ID = 
                (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID /*+ 1*/) % handle->ack_Buffer_Size;
            break;
        case RIO_PL_PNACC_BAD_CONTROL:
        case RIO_PL_PNACC_BAD_S_BIT_PARITY:
            not_Accepted.granule_Struct.ack_ID = 
                (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID + 1) % handle->ack_Buffer_Size;
            break;
        }

        RIO_Switch_Tx_Send_Ack(handle, link_Number, &not_Accepted);

        /* change register */
        rio_Switch_Rx_Cng_St_To_Error(handle, link_Number);
        return;
    /*}*/
}

/***************************************************************************
 * Function : handler_Error_State
 *
 * Description: state machine for stopped due to error state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Handler_Error_State
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* link request is restart from error symbol */
/*    if (granule->granule_Type == RIO_GR_LINK_REQUEST)
        rio_Switch_Start_Rx_Normal(handle, link_Number);*/


    /*****from pl****************************************/

    /* link request is restart from error symbol */
    if (granule->granule_Type == RIO_GR_LINK_REQUEST && 
        granule->granule_Struct.ack_ID == RIO_LP_LREQ_INPUT_STATUS)
        rio_Switch_Start_Rx_Normal(handle, link_Number);

    switch (granule->granule_Type)
    {
        case RIO_GR_NEW_PACKET:
            handle->link_Data[link_Number].rx_Data.in_Progress = RIO_TRUE;
            RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_TRUE, 0, link_Number);
            break;
            
        case RIO_GR_EOP:
            if (handle->link_Data[link_Number].rx_Data.in_Progress == RIO_TRUE)
            {
                handle->link_Data[link_Number].rx_Data.in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            else
                RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);
            break;

 
     /*   case RIO_GR_TRAINING:
            if ((handle->link_Data[link_Number].rx_Data.in_Progress == RIO_TRUE) && 
                (RIO_PL_Tx_Control_Is_Expected(handle, granule) == RIO_TRUE));
            break;*/
                
     
        
        case RIO_GR_STOMP:
            if (handle->link_Data[link_Number].rx_Data.in_Progress == RIO_TRUE)
            {
                handle->link_Data[link_Number].rx_Data.in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            else
                RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);
            break;

        case RIO_GR_RESTART_FROM_RETRY:
            if (handle->link_Data[link_Number].rx_Data.in_Progress == RIO_TRUE)
            {
                handle->link_Data[link_Number].rx_Data.in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);
            break;

        default: {}
        
    }
    /****************************************************/
}

/***************************************************************************
 * Function : handler_Retry_State
 *
 * Description: state machine for stopped due to retry state 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Handler_Retry_State
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* 
     * link request and restart from retry turn the receiver to
     * normal state from stopped due to retry one
     */
   /* if (granule->granule_Type == RIO_GR_LINK_REQUEST ||
        granule->granule_Type == RIO_GR_RESTART_FROM_RETRY)
        rio_Switch_Start_Rx_Normal(handle, link_Number);*/


    /*****************from pl****************************/
    /* 
     * link request and restart from retry turn the receiver to
     * normal state from stopped due to retry one
     */
    if ((granule->granule_Type == RIO_GR_LINK_REQUEST &&
        granule->granule_Struct.ack_ID == RIO_LP_LREQ_INPUT_STATUS ) ||
        granule->granule_Type == RIO_GR_RESTART_FROM_RETRY)
    {
        handle->link_Data[link_Number].rx_Data.in_Progress = RIO_FALSE;

        RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
        rio_Switch_Start_Rx_Normal(handle, link_Number);
    }

    if (granule->granule_Type == RIO_GR_HAS_BEEN_CORRUPTED)
        rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_BAD_CONTROL);
    else if (granule->granule_Type == RIO_GR_S_BIT_CORRUPTED)
        rio_Switch_Start_Rx_Error_Rec(handle, link_Number, RIO_PL_PNACC_BAD_S_BIT_PARITY);

    switch (granule->granule_Type)
    {
        case RIO_GR_NEW_PACKET:
            handle->link_Data[link_Number].rx_Data.in_Progress = RIO_TRUE;
            RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_TRUE, 0, link_Number);
            break;
            
        case RIO_GR_EOP:
            if (handle->link_Data[link_Number].rx_Data.in_Progress == RIO_TRUE)
            {
                handle->link_Data[link_Number].rx_Data.in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            else
                RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);
            break;
 
    /*    case RIO_GR_TRAINING:
            if ((handle->rx_Data.in_Progress == RIO_TRUE) && 
                ((is_Expected_Result= RIO_PL_Tx_Control_Is_Expected(handle,granule)) == RIO_TRUE))
                start_Rx_Error_Rec(handle, RIO_PL_PNACC_GENERAL_ERROR);
            break;*/
        
        case RIO_GR_STOMP:
            if (handle->link_Data[link_Number].rx_Data.in_Progress == RIO_TRUE)
            {
                handle->link_Data[link_Number].rx_Data.in_Progress = RIO_FALSE;
                RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
            }
            else
                RIO_Switch_Tx_Rec_Unexp_Packet_Control(handle, granule, link_Number);
            break;

        default: {}
    }

    /****************************************************/

}

/***************************************************************************
 * Function : current_Rx_Status
 *
 * Description: return receiver state for link response
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Current_Rx_Status(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return RIO_PL_LRESP_ERROR; /* error code */

    if (handle->is_Parallel_Model == RIO_TRUE)
    {
        switch (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State)
        {
            case RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY:
                return RIO_PL_LRESP_RETRY_STOPPED; /* retry-stopped*/

            case RIO_SWITCH_LINK_STOPPED_DUE_TO_ERROR:
                return RIO_PL_LRESP_ERROR_STOPPED; /* error-stopped */

            default:
                return ((unsigned int)RIO_PL_LRESP_OK) | RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID;
        }
    }
    else
    {
        switch (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State)
        {
            case RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY:
                return RIO_PL_LRESP_RETRY_STOPPED; /* retry-stopped*/

            case RIO_SWITCH_LINK_STOPPED_DUE_TO_ERROR:
                return RIO_PL_LRESP_ERROR_STOPPED; /* error-stopped */

            default:
                return ((unsigned int)RIO_PL_LRESP_SERIAL_OK);
        }

    }
}

/***************************************************************************
 * Function : RIO_Switch_Rx_Rec_Ack
 *
 * Description: accept acnowledge 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Rx_Rec_Ack
    (RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *ack_Granule, unsigned int link_Number)
{
    if (handle == NULL || ack_Granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* check if its an acknowledge we are waiting for */
    ack_Granule->granule_Struct.ack_ID = RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID;

    switch (ack_Granule->granule_Type)
    {
        case RIO_GR_PACKET_ACCEPTED:
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID = 
                (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID + 1) %
                handle->ack_Buffer_Size;
            break;

        case RIO_GR_PACKET_RETRY:
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State == RIO_SWITCH_LINK_OK)
            {
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State = RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY;
                /* change register */
                rio_Switch_Rx_Cng_St_To_Retry(handle, link_Number);
            }
            break;

        case RIO_GR_PACKET_NOT_ACCEPTED:
            if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State == RIO_SWITCH_LINK_OK ||
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State == RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY)
            {
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State = RIO_SWITCH_LINK_STOPPED_DUE_TO_ERROR;
                /* change register */
                rio_Switch_Rx_Cng_St_To_Error(handle, link_Number);
            }
            break;

        default: {}
    }

    /* change state and pass acknowledge for sending */
    RIO_Switch_Tx_Send_Ack(handle, link_Number, ack_Granule);
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state = RIO_SWITCH_LINK_FREE;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To = -1;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).conf_Req = RIO_FALSE;
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.counting_CRC = RIO_SWITCH_INIT_CRC;
}

/***************************************************************************
 * Function : start_Rx_Normal
 *
 * Description: turn receiver to normal mode
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Start_Rx_Normal(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle)) 
        return;

    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).global_State = RIO_SWITCH_LINK_OK;
    
    /* discard receiving packet */
    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).in_Progress = RIO_FALSE;

    RIO_Switch_Set_State(handle, RIO_RX_DATA_IN_PROGRESS, RIO_FALSE, 0, link_Number);
    
    /* change register */
    rio_Switch_Rx_Cng_St_To_Normal(handle, link_Number);
}

/***************************************************************************
 * Function : rio_Switch_Get_Ftype
 *
 * Description: detect FTYPE
 *
 * Returns: FTYPE
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Get_Ftype(RIO_UCHAR_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    /* returns the less significant octet from the first byte */
    return (arbiter[1] & 0x0f);

}

/***************************************************************************
 * Function : rio_Switch_Get_Ttype
 *
 * Description: detect TTYPE field
 *
 * Returns: ttype
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Get_Ttype(RIO_UCHAR_T *arbiter)
{
    unsigned int tt;

    if (arbiter == NULL)
        return 0;

    tt = rio_Switch_Get_Tt(arbiter);
    if (tt == RIO_PL_TRANSP_16)
        return (arbiter[4] & 0xf0) >> 4; /* the most signif octet from 4th byte */
    else
        return (arbiter[6] & 0xf0) >> 4; /* the most signif. octet from the 6th byte */
}

/***************************************************************************
 * Function : rio_Switch_Is_Req
 *
 * Description: detect if ttype is request ttype
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T rio_Switch_Is_Req(unsigned int ftype, unsigned int ttype)
{
    switch(ftype)
    {
        case RIO_MAINTENANCE:
            if (ttype == RIO_MAINTENANCE_CONF_READ ||
                ttype == RIO_MAINTENANCE_CONF_WRITE ||
                ttype == RIO_MAINTENANCE_PORT_WRITE)
                return RIO_OK;
            break;

        /* one break because of one result */
        case RIO_DOORBELL:
        case RIO_MESSAGE:
        case RIO_NONINTERV_REQUEST:
        case RIO_WRITE:
        case RIO_STREAM_WRITE:
        case RIO_INTERV_REQUEST:
            return RIO_OK;

        default:
            ;
    }

    return RIO_ERROR;
}

/***************************************************************************
 * Function : rio_Switch_Update_Tr_Info
 *
 * Description: update transport info in packet stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Update_Tr_Info
    (RIO_UCHAR_T *buffer, unsigned long transp_Info, unsigned int tt)
{
    unsigned int i = 2; /* the second byte is start for transport field */

    if (buffer == NULL)
        return;

    if (tt != RIO_PL_TRANSP_16)
    {
        buffer [i++] = (RIO_UCHAR_T)(transp_Info >> 24); /* shift 24 bits to the right */
        buffer [i++] = (RIO_UCHAR_T)(transp_Info >> 16); /* shift 16 bits to the right */
    }

    buffer [i++] = (RIO_UCHAR_T)(transp_Info >> 8); /* shift 8 bits to the right */
    buffer [i++] = (RIO_UCHAR_T)transp_Info;
}

/***************************************************************************
 * Function : RIO_Switch_Compute_CRC
 *
 * Description: compute CRC code
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_Switch_Compute_CRC(unsigned short *crc, RIO_UCHAR_T stream[], unsigned int i_Max)
{
    /* 
     * CRC is counted in according to the specification - see
     * corresponding chapter for fields and offsets encoding 
     */

    /* previous control */
    RIO_UCHAR_T c0, c1, e0, e1; 
    /* CRC bits */
    RIO_UCHAR_T c00, c01, c02, c03, c04, c05, c06, c07, 
        c08, c09, c10, c11, c12, c13, c14, c15;
    /* data bits */
    RIO_UCHAR_T d00, d01, d02, d03, d04, d05, d06, d07, 
        d08, d09, d10, d11, d12, d13, d14, d15;

    /* loop control */
    unsigned int i;

    if (crc == NULL || stream == NULL)
        return;

    /* select controls */
    c0 = (RIO_UCHAR_T)((*crc & 0xff00) >> 8);
    c1 = (RIO_UCHAR_T)(*crc & 0x00ff); 

    for (i = 0; i < i_Max; i += 2)
    {
        /* c XOR d */
        e0 = (RIO_UCHAR_T)(c0 ^ stream[i]);
        e1 = (RIO_UCHAR_T)(c1 ^ stream[i + 1]);

        /* data bits selection*/
        d00 = (RIO_UCHAR_T)((e0 & 0x80) >> 7);
        d01 = (RIO_UCHAR_T)((e0 & 0x40) >> 6);
        d02 = (RIO_UCHAR_T)((e0 & 0x20) >> 5);
        d03 = (RIO_UCHAR_T)((e0 & 0x10) >> 4);
        d04 = (RIO_UCHAR_T)((e0 & 0x08) >> 3);
        d05 = (RIO_UCHAR_T)((e0 & 0x04) >> 2);
        d06 = (RIO_UCHAR_T)((e0 & 0x02) >> 1);
        d07 = (RIO_UCHAR_T)(e0 & 0x01);
        d08 = (RIO_UCHAR_T)((e1 & 0x80) >> 7);
        d09 = (RIO_UCHAR_T)((e1 & 0x40) >> 6);
        d10 = (RIO_UCHAR_T)((e1 & 0x20) >> 5);
        d11 = (RIO_UCHAR_T)((e1 & 0x10) >> 4);
        d12 = (RIO_UCHAR_T)((e1 & 0x08) >> 3);
        d13 = (RIO_UCHAR_T)((e1 & 0x04) >> 2);
        d14 = (RIO_UCHAR_T)((e1 & 0x02) >> 1);
        d15 = (RIO_UCHAR_T)(e1 & 0x01);

        /*  XOR equation network */
        c00 = (RIO_UCHAR_T)(d04 ^ d05 ^ d08 ^ d12);

        c01 = (RIO_UCHAR_T)(d05 ^ d06 ^ d09 ^ d13);

        c02 = (RIO_UCHAR_T)(d06 ^ d07 ^ d10 ^ d14);

        c03 = (RIO_UCHAR_T)(d00 ^ d07 ^ d08 ^ d11 ^ d15);

        c04 = (RIO_UCHAR_T)(d00 ^ d01 ^ d04 ^ d05 ^ d09);

        c05 = (RIO_UCHAR_T)(d01 ^ d02 ^ d05 ^ d06 ^ d10);

        c06 = (RIO_UCHAR_T)(d00 ^ d02 ^ d03 ^ d06 ^ d07 ^ d11);

        c07 = (RIO_UCHAR_T)(d00 ^ d01 ^ d03 ^ d04 ^ d07 ^ d08 ^ d12);

        c08 = (RIO_UCHAR_T)(d00 ^ d01 ^ d02 ^ d04 ^ d05 ^ d08 ^ d09 ^ d13);

        c09 = (RIO_UCHAR_T)(d01 ^ d02 ^ d03 ^ d05 ^ d06 ^ d09 ^ d10 ^ d14);

        c10 = (RIO_UCHAR_T)(d02 ^ d03 ^ d04 ^ d06 ^ d07 ^ d10 ^ d11 ^ d15);

        c11 = (RIO_UCHAR_T)(d00 ^ d03 ^ d07 ^ d11);

        c12 = (RIO_UCHAR_T)(d00 ^ d01 ^ d04 ^ d08 ^ d12);

        c13 = (RIO_UCHAR_T)(d01 ^ d02 ^ d05 ^ d09 ^ d13);

        c14 = (RIO_UCHAR_T)(d02 ^ d03 ^ d06 ^ d10 ^ d14);

        c15 = (RIO_UCHAR_T)(d03 ^ d04 ^ d07 ^ d11 ^ d15);

        /* compute CRC */
        c0 = (RIO_UCHAR_T)(c07 + (c06 << 1) + (c05 << 2) + (c04 << 3) + (c03 << 4) +
            (c02 << 5) + (c01 << 6) + (c00 << 7));

        c1 = (RIO_UCHAR_T)(c15 + (c14 << 1) + (c13 << 2) + (c12 << 3) + (c11 << 4) +
            (c10 << 5) + (c09 << 6) + (c08 << 7));
    }

    /* store calculated CRC */
    *crc = (unsigned short)(((c0 & 0xff) << 8) + (c1 & 0xff));
}

/***************************************************************************
 * Function : rio_Switch_Get_CRC_Start
 *
 * Description: returns the start position of the end CRC in the stream 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Get_CRC_Start
    (RIO_SWITCH_PACKET_BUFFER_T *arbiter, RIO_BOOL_T ext_Address, RIO_BOOL_T ext_Address_16)
{
    /*
     * all constants are packet specific - they does not correlate between and
     * are defined by the spec for corresponding packet format 
     */
    unsigned int ftype, tt;

    if (arbiter == NULL)
        return 1;

    /* get inbound arbiter */
  /*  arbiter = &handle->buffer;*/

    ftype = rio_Switch_Get_Ftype(arbiter->buffer);
    tt = rio_Switch_Get_Tt(arbiter->buffer);

    switch (ftype)
    {
        /* maintanence */
        case RIO_MAINTENANCE:
            if (tt == RIO_PL_TRANSP_16)
                return arbiter->buffer_Top_For_Load - 2;
            else
                return arbiter->buffer_Top_For_Load - 4;
        
        /* doorbell request */
        case RIO_DOORBELL:
            if (tt == RIO_PL_TRANSP_16)
                return arbiter->buffer_Top_For_Load - 4;
            else
                return arbiter->buffer_Top_For_Load - 2;
        
        /* response packet */
        case RIO_RESPONCE:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
                    return arbiter->buffer_Top_For_Load - 4;
                else
                    return arbiter->buffer_Top_For_Load - 2;
            }
            else
            {
                if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
                    return arbiter->buffer_Top_For_Load - 2;
                else
                    return arbiter->buffer_Top_For_Load - 4;
            }

        /* message request */
        case RIO_MESSAGE:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
                    return arbiter->buffer_Top_For_Load - 4;
                else
                    return arbiter->buffer_Top_For_Load - 2;
            }
            else
            {
                if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
                    return arbiter->buffer_Top_For_Load - 2;
                else 
                    return arbiter->buffer_Top_For_Load - 4;
            }

        /* nonintervention request */
        case RIO_NONINTERV_REQUEST:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (ext_Address == RIO_TRUE)
                {
                    if (ext_Address_16 == RIO_TRUE)
                        return arbiter->buffer_Top_For_Load - 4;
                    else
                        return arbiter->buffer_Top_For_Load - 2;
                }
                else
                    return arbiter->buffer_Top_For_Load - 2;
            }
            else
            {
                if (ext_Address == RIO_TRUE)
                {
                    if (ext_Address_16 == RIO_TRUE)
                        return arbiter->buffer_Top_For_Load - 2;
                    else
                        return arbiter->buffer_Top_For_Load - 4;
                }
                else
                    return arbiter->buffer_Top_For_Load - 4;
            }

        /* write class */
        case RIO_WRITE:
            if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 2;
                        else
                            return arbiter->buffer_Top_For_Load - 4;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 4;
                }
                else
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 4;
                        else
                            return arbiter->buffer_Top_For_Load - 2;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 2;
                }
            }
            else
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 4;
                        else
                            return arbiter->buffer_Top_For_Load - 2;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 2;
                }
                else
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 2;
                        else
                            return arbiter->buffer_Top_For_Load - 4;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 4;
                }
            }

        /* stream write */
        case RIO_STREAM_WRITE:
            if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 4;
                        else
                            return arbiter->buffer_Top_For_Load - 2;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 2;
                }
                else
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 2;
                        else
                            return arbiter->buffer_Top_For_Load - 4;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 4;
                }
            }
            else
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 2;
                        else
                            return arbiter->buffer_Top_For_Load - 4;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 4;
                }
                else
                {
                    if (ext_Address == RIO_TRUE)
                    {
                        if (ext_Address_16 == RIO_TRUE)
                            return arbiter->buffer_Top_For_Load - 4;
                        else
                            return arbiter->buffer_Top_For_Load - 2;
                    }
                    else
                        return arbiter->buffer_Top_For_Load - 2;
                }
            }

            /* intervention request */
        case RIO_INTERV_REQUEST:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (ext_Address == RIO_TRUE)
                {
                    if (ext_Address_16 == RIO_TRUE)
                        return arbiter->buffer_Top_For_Load - 2;
                    else
                        return arbiter->buffer_Top_For_Load - 4;
                }
                else
                    return arbiter->buffer_Top_For_Load - 4;
            }
            else
            {
                if (ext_Address == RIO_TRUE)
                {
                    if (ext_Address_16 == RIO_TRUE)
                        return arbiter->buffer_Top_For_Load - 4;
                    else
                        return arbiter->buffer_Top_For_Load - 2;
                }
                else
                    return arbiter->buffer_Top_For_Load - 2;
            }
            


        /* other not-implemented types */
        default :
            return 1;
    }
}

/***************************************************************************
 * Function : rio_Switch_Rx_Cng_St_To_Retry
 *
 * Description: update link state to stopped due to retry
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Rx_Cng_St_To_Retry
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long state_Offset; /* corresponding CSR address */
    unsigned long state; /* state itself */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    state_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr +
        link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A;

        /* change receiver state in the register */
    RIO_Switch_Read_Register(handle, state_Offset, &state, RIO_TRUE);
    /* clear receiver bits*/
    state &= RIO_SWITCH_ERROR_RX_STATE_CLEAR_MASK;
    /* set OK bit */
    state |= RIO_SWITCH_ERROR_RX_RETRY;
    RIO_Switch_Write_Register(handle, state_Offset, state, RIO_TRUE);
}

/***************************************************************************
 * Function : rio_Switch_Rx_Cng_St_To_Error
 *
 * Description: update link state to stopped due to error
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Rx_Cng_St_To_Error
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long state_Offset; /* corresponding CSR address */
    unsigned long state; /* state itself */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    state_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr +
        link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A;

        /* change receiver state in the register */
    RIO_Switch_Read_Register(handle, state_Offset, &state, RIO_TRUE);
    /* clear receiver bits*/
    state &= RIO_SWITCH_ERROR_RX_STATE_CLEAR_MASK;
    /* set OK bit */
    state |= RIO_SWITCH_ERROR_RX_ERROR;
    RIO_Switch_Write_Register(handle, state_Offset, state, RIO_TRUE);
}

/***************************************************************************
 * Function : rio_Switch_Rx_Cng_St_To_Normal
 *
 * Description: update link state to OK
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Rx_Cng_St_To_Normal
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long state_Offset; /* corresponding CSR address */
    unsigned long state; /* state itself */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    state_Offset = handle->inst_Param_Set.entry_Extended_Features_Ptr + 
        link_Number * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A;

        /* change receiver state in the register */
    RIO_Switch_Read_Register(handle, state_Offset, &state, RIO_TRUE);
    /* clear receiver bits*/
    state &= RIO_SWITCH_ERROR_RX_STATE_CLEAR_MASK;
    /* set OK bit */
    /*state |= RIO_SWITCH_RX_STATE_OK;*/
    RIO_Switch_Write_Register(handle, state_Offset, state, RIO_TRUE);
}

/***************************************************************************
 * Function : rio_Switch_Get_Hop_Count
 *
 * Description: detect hop_count field
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Get_Hop_Count
    (RIO_UCHAR_T *arbiter, unsigned int tt)
{
    if (arbiter == NULL)
        return 0;

    if (tt == RIO_PL_TRANSP_16)
        return arbiter[6]; /* returns byte number 6 */
    else
        return arbiter[8]; /* returns byte number 8 */
}

/***************************************************************************
 * Function : rio_Switch_Upd_Hop_Count
 *
 * Description: stote hop_Count field to byte stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Upd_Hop_Count
    (RIO_UCHAR_T *arbiter, unsigned int tt, unsigned int hop_Count)
{
    if (arbiter == NULL)
        return;

    if (tt == RIO_PL_TRANSP_16)
        arbiter[6] = (RIO_UCHAR_T)hop_Count; /* update byte number 6 */
    else
        arbiter[8] = (RIO_UCHAR_T)hop_Count; /* update byte number 8 */
}


/***************************************************************************
 * Function : rio_Switch_Check_Packet
 *
 * Description: check receiving packet for error
 *
 * Returns: 8 - ok, else error code specefied in packet not accepted 
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Check_Packet
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
   /* unsigned short crc = RIO_SWITCH_INIT_CRC, src_Crc;
    unsigned int first_Crc_Pos, start_For_Cnt = 0;*/

    RIO_BOOL_T error_Flag;
    RIO_PACKET_VIOLATION_T violation_Type;
    unsigned int ftype;

    if (handle == NULL)
        return RIO_PL_PNACC_INTERNAL_ERROR; /*internal error*/

    /* check if packet isn't word aligned to 4 byte boundary*/
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load % RIO_SWITCH_BYTE_CNT_IN_WORD)
    {       
/*???????later        RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
        return RIO_PL_PNACC_GENERAL_ERROR;
    }

    ftype = rio_Switch_Get_Ftype(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer);
    if ( (handle->link_Common_Params[link_Number].only_Maint_Requests == RIO_TRUE) &&
        (ftype != RIO_MAINTENANCE)  && (ftype != RIO_RESPONCE))
    {
        /*???RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
        return RIO_PL_PNACC_INPUT_STOPPED;
    }


    if (rio_Switch_In_Check_Current(handle, &error_Flag, &violation_Type, link_Number) != RIO_ERROR)
        if (error_Flag == RIO_TRUE)
        {
            /*RIO_PL_Print_Message(handle, RIO_PL_WARNING_11, violation_Type);*/
            switch (violation_Type)
            {
                case CRC_ERROR:
                    /*RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
                    return RIO_PL_PNACC_BAD_CRC;
                
                case FTYPE_RESERVED:
                    /*RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
                    return RIO_PL_PNACC_GENERAL_ERROR;
                
                case TTYPE_RESERVED:
                    /*RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
                    return RIO_PL_PNACC_GENERAL_ERROR;
                
                case TT_RESERVED:
                    /*RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
                    return RIO_PL_PNACC_GENERAL_ERROR;
                
                case NON_ZERO_RESERVED_PACKET_BITS_VALUE:
                    return RIO_PL_PNACC_OK;
                
                case INCORRECT_PACKET_HEADER:
                    /*RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
                    return RIO_PL_PNACC_GENERAL_ERROR;

                case PAYLOAD_NOT_DW_ALIGNED:
                    /*RIO_PL_Out_Init_Arbiter(&RIO_IN_GET_ARB(handle));*/
                    return RIO_PL_PNACC_GENERAL_ERROR;

                default:
                    break;
            }
        }
        
    return RIO_PL_PNACC_OK;
}

/***************************************************************************
 * Function : rio_Switch_Acknowledge_Req
 *
 * Description: send PACKET ACCEPTED acknowledge to the sender
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Acknowledge_Req
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    RIO_GRANULE_T accepted; /* acknowledge */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* form granule structure */
    accepted.granule_Type = RIO_GR_PACKET_ACCEPTED;
    accepted.granule_Struct.ack_ID = RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).expected_Ack_ID;
    accepted.granule_Struct.s = 1;
    accepted.granule_Struct.stype = RIO_GR_PACKET_ACCEPTED_STYPE;
    accepted.granule_Struct.buf_Status = 1;

    /* pass acknowledge to the transmitter */
    RIO_Switch_Tx_Send_Ack(handle, link_Number, &accepted);
}

/***************************************************************************
 * Function : rio_Switch_Commit_Perform_Req
 *
 * Description: perform MAINTANANCE request to itself and buid the response
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Commit_Perform_Req
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    /* stripped request */
    RIO_REQ_HEADER_T     request_Header;
    RIO_REMOTE_REQUEST_T request;
    RIO_DW_T             data[RIO_SWITCH_MAX_PAYLOAD_DWORDS];
    unsigned long        tr_Info;
    unsigned int         i; /* data locater */

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    /* prepare request structures */
    request.data = data;
    request.header = &request_Header;

    /* strip the request */
    request_Header.format_Type = (RIO_UCHAR_T)rio_Switch_Get_Ftype(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer);
    request_Header.ttype = (RIO_UCHAR_T)rio_Switch_Get_Ttype(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer);

    /* check that it's a request which switch can perform */
    if (rio_Switch_Is_Req(request_Header.format_Type, request_Header.ttype) == RIO_ERROR)
        return;

    /* check that it's a MAINTENANCE */
    if (request_Header.format_Type != RIO_MAINTENANCE)
        return;

    /* PORT WRITE does not matter anything for the switch */
    if (request_Header.ttype == RIO_MAINTENANCE_PORT_WRITE)
        return;

    /* strip other fields */
    request_Header.prio = (RIO_UCHAR_T)rio_Switch_Get_Prio(
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer);

    request.transp_Type = rio_Switch_Get_Tt(
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer);

    rio_Switch_Get_Transp_Info(
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer,
        &tr_Info, (unsigned int *)&request.transp_Type);

    /* locate first unparsed byte */
    if (request.transp_Type == RIO_PL_TRANSP_16)
        i = RIO_SWITCH_TR_TYPE_POS_LOW;
    else
        i = RIO_SWITCH_TR_TYPE_POS_HIGH;

    /* complete fields */
    request_Header.rdsize = 
        (RIO_UCHAR_T)(RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[i] & 0x0f);

    request_Header.target_TID = 
        RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[++i];

    /* pass hop_count */
    ++i;

    /* detect offset from 3 bytes as defeined in the spec */
    request_Header.offset = 
        ((unsigned long)RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[++i]) << 13;

    request_Header.offset |= 
        ((unsigned long)RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[++i]) << 5;

    request_Header.offset |= 
        ((unsigned long)RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[++i] & 0xf8) >> 3;

    /* detect wdptr as defined in the spec */
    request_Header.wdptr = 
        (RIO_UCHAR_T)((RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer[i++] & 0x04) >> 2);

    /* shift offset again to the left on 3 bits */
    request_Header.offset <<= 3;

    /* count payload size - skip 2 bytes which is used by CRC */
    request.dw_Num = 
        (RIO_UCHAR_T)((RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Top_For_Load - i - 2) 
        / RIO_SWITCH_CNT_BYTES_DWORD);
    
    /* check data payload */
    if (request.dw_Num)
    {
        /* convert the stream to payload */
        rio_Switch_Stream_To_Dw(
            data,
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer + i,
            request.dw_Num);
    }

    rio_Switch_Ext_Req(handle, link_Number, &request, tr_Info);
    return;
}

/***************************************************************************
 * Function : rio_Switch_Ext_Req
 *
 * Description: performs external request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Ext_Req
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number, RIO_REMOTE_REQUEST_T *req, unsigned long tr_Info)
{
    unsigned int status = RIO_RESPONCE_STATUS_DONE;
    unsigned int size;
    unsigned long address;

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle) || req == NULL)
        return;

    /* request checking */
    size = RIO_Switch_Conv_Size_To_Dw_Cnt(req->header->rdsize, req->header->wdptr);

    if (!size || size > RIO_SWITCH_MAX_CONF_PAY_SIZE)
        status = RIO_RESPONCE_STATUS_ERROR;

    if (req->header->ttype == RIO_MAINTENANCE_CONF_READ && req->dw_Num)
        status = RIO_RESPONCE_STATUS_ERROR;

    if (req->header->ttype == RIO_MAINTENANCE_CONF_WRITE && 
        (req->dw_Num > size || !req->dw_Num))
        status = RIO_RESPONCE_STATUS_ERROR;

    if (req->header->rdsize < RIO_SWITCH_MIN_RDSIZE /*&& req->header->rdsize > RIO_SWITCH_MAX_RDSIZE*/) 
        status = RIO_RESPONCE_STATUS_ERROR;

    /* write request can have lesser payload in packet */
    if (req->header->ttype == RIO_MAINTENANCE_CONF_READ)
        req->dw_Num = (RIO_UCHAR_T)size;

    /* if no error has been detected, send DONE response */
    if (status != RIO_RESPONCE_STATUS_ERROR)
    {
        /* start address in registers space */
        address = req->header->offset;

        /* check if it's word access */
        if (req->header->rdsize == RIO_SWITCH_MIN_RDSIZE)
        {
            /* if wdptr add 4 to get address of less ginificant halfword */
            address += req->header->wdptr ? 4 : 0 ;
            
            if (req->header->ttype == RIO_MAINTENANCE_CONF_READ)
            {
                /* store data */
                if (req->header->wdptr)
                {
                    RIO_Switch_Read_Register(
                        handle, 
                        address, 
                        &req->data[0].ls_Word, 
                        RIO_FALSE);

                    /* check switch info CAR access */
                    if (address == RIO_SWITCH_INFO_A)
                    {
                        req->data[0].ls_Word &= 0xfffffff0;
                        req->data[0].ls_Word |= (link_Number & 0x0f);
                    }
                    
                    req->data[0].ms_Word = 0;
                }
                else
                {
                    RIO_Switch_Read_Register(
                        handle, 
                        address, 
                        &req->data[0].ms_Word, 
                        RIO_FALSE);

                    /* check switch info CAR access */
                    if (address == RIO_SWITCH_INFO_A)
                    {
                        req->data[0].ms_Word &= 0xfffffff0;
                        req->data[0].ms_Word |= (link_Number & 0x0f);
                    }

                    req->data[0].ls_Word = 0;
                }
            }
            else
            {
                /* get data */
                if (req->header->wdptr)
                    RIO_Switch_Write_Register(
                        handle, 
                        address, 
                        req->data[0].ls_Word, 
                        RIO_FALSE);
                else
                    RIO_Switch_Write_Register(
                        handle, 
                        address, 
                        req->data[0].ms_Word, 
                        RIO_FALSE);
            }
        }
        else
        {
            /* multidword access */
            unsigned int i; /* loop counter */

            /* go through all dwords */
            for (i = 0; i < req->dw_Num; i++)
            {
                if (req->header->ttype == RIO_MAINTENANCE_CONF_READ)
                {
                    RIO_Switch_Read_Register(
                        handle, 
                        address, 
                        &req->data[i].ms_Word, 
                        RIO_FALSE);

                    /* check switch info CAR access */
                    if (address == RIO_SWITCH_INFO_A)
                    {
                        req->data[i].ms_Word &= 0xffffff00;
                        req->data[i].ms_Word |= (link_Number & 0xff);
                    }

                    /* less signif word has address + 4 */
                    RIO_Switch_Read_Register(
                        handle, 
                        address + 4, 
                        &req->data[i].ls_Word, 
                        RIO_FALSE);

                    /* check switch info CAR access */
                    if ((address + 4)== RIO_SWITCH_INFO_A)
                    {
                        req->data[i].ls_Word &= 0xffffff00;
                        req->data[i].ls_Word |= (link_Number & 0xff);
                    }

                }
                else
                {
                    RIO_Switch_Write_Register(
                        handle, 
                        address, 
                        req->data[i].ms_Word, 
                        RIO_FALSE);

                    /* less sign word has address bigger by 4 */
                    RIO_Switch_Write_Register(
                        handle, 
                        address + 4, 
                        req->data[i].ls_Word, 
                        RIO_FALSE);
                }

                /* increase address by 8 to get new double-word */
                address += 8;
            }
        }
    }
    /* send response */
    rio_Switch_Send_Response(handle, link_Number, req, status, tr_Info);

    /* notify transmitter */
    RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req = RIO_TRUE;

}

/***************************************************************************
 * Function : rio_Switch_Send_Response
 *
 * Description: send response
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Send_Response
    (RIO_SWITCH_DS_T *handle, unsigned int link_Number, RIO_REMOTE_REQUEST_T *req, unsigned int status, unsigned long tr_Info)
{
    RIO_UCHAR_T *arbiter; /* the response stream */
    unsigned int i; /* byte locater */
    unsigned short crc = RIO_SWITCH_INIT_CRC;

    if (handle == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle) || req == NULL)
        return;

    arbiter = handle->resp_Buffer[link_Number].buffer;

    /* make 0 byte */
    arbiter[i = 0] = (RIO_UCHAR_T)0;

    /* make 1 byte */
    if (req->header->prio < 3)
        req->header->prio++;

    /* address field putting in according with spec */
    arbiter[++i] = (RIO_UCHAR_T)((req->header->prio & 0x03) << 6);
    arbiter[i] |= (RIO_UCHAR_T)((req->transp_Type & 0x03) << 4);
    arbiter[i] |= (RIO_UCHAR_T)(req->header->format_Type & 0x0f);

    /* make transport info with according to spec it can 
     * occupy 2 or 4 bytes in according to transp_Type field
     */
    if (handle->init_Param_Set.rout_Alg == RIO_TABLE_BASED_ROUTING)
    {
        /* wrapp transport info before sending for table routing algorithm */
        if (req->transp_Type == RIO_PL_TRANSP_16)
            tr_Info = ((tr_Info & 0xff00) >> 8) + ((tr_Info & 0x00ff) << 8);
        else
            tr_Info = ((tr_Info & 0xffff0000) >> 16) + ((tr_Info & 0x0000ffff) << 16);
    }

    /* the most signif two bytes of transport info */
    if (req->transp_Type != RIO_PL_TRANSP_16)
    {
        arbiter[++i] = (RIO_UCHAR_T)((tr_Info & 0xff000000) >> 24);
        arbiter[++i] = (RIO_UCHAR_T)((tr_Info & 0x00ff0000) >> 16);
    }

    /*  the third byte of transport info */
    arbiter[++i] = (RIO_UCHAR_T)((tr_Info & 0x0000ff00) >> 8);

    /*  the less ginificant byte */
    arbiter[++i] = (RIO_UCHAR_T)(tr_Info & 0x000000ff);

    /* */
    req->header->ttype = (RIO_UCHAR_T)((req->header->ttype == RIO_MAINTENANCE_CONF_READ) ?
        RIO_MAINTENANCE_READ_RESPONCE : RIO_MAINTENANCE_WRITE_RESPONCE);

    /* store ttype in according to the spec */
    arbiter[++i] = (RIO_UCHAR_T)((req->header->ttype & 0x0f) << 4);
    arbiter[i] |= (RIO_UCHAR_T)(status & 0x0f);

    /* stote TID in according to the spec */
    arbiter[++i] = (RIO_UCHAR_T)(req->header->target_TID & 0xff);

    /* */
    arbiter[++i] = (RIO_UCHAR_T)(0xff);
    /* */
    arbiter[++i] = (RIO_UCHAR_T)(0);

    /* */
    arbiter[++i] = (RIO_UCHAR_T)(0);

    /* */
    arbiter[++i] = (RIO_UCHAR_T)(0);
    i++;

    /* data payload */
    if (req->header->ttype == RIO_MAINTENANCE_READ_RESPONCE &&
        status == RIO_RESPONCE_STATUS_DONE)
    {
        unsigned int j = 0; /* loop counter */

        /* convert payload to the stream */
        for (j = 0; j < req->dw_Num; j++, i += RIO_SWITCH_CNT_BYTES_DWORD)
            rio_Switch_Conv_Dw_Char(req->data + j, arbiter + i);
    }

    /* here we need to compute CRC code */
    RIO_Switch_Compute_CRC(&crc, arbiter, i);
    arbiter[i] = (RIO_UCHAR_T)((crc & 0xff00) >> RIO_SWITCH_CNT_BYTES_DWORD);

    /* */
    arbiter[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet - if packet is not 4 bytes aligned */
    if (++i % 4)
    {
        arbiter[i] = (RIO_UCHAR_T)0x00;
        arbiter[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    handle->resp_Buffer[link_Number].buffer_Top = i;
    handle->resp_Buffer[link_Number].buffer_Head = 0;
}

/***************************************************************************
 * Function : rio_Switch_Conv_Dw_Char
 *
 * Description: convert double word to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Conv_Dw_Char(RIO_DW_T *dw, RIO_UCHAR_T *stream)
{
    /*
     * this conversions is performing in according to the used Endian mode
     * double-word structure fieds definition 
     */
    unsigned int i, shift;
    unsigned long mask;

    if (dw == NULL || stream == NULL)
        return;

    /* convert the first word */
    for (i = 0, mask = 0xff000000, shift = 24; i < 4; i++, shift -= 8, mask >>= 8)
        stream[i] = (RIO_UCHAR_T)((dw->ms_Word & mask) >> shift);

    /* convert the second word */
    for (mask = 0xff000000, shift = 24; i < 8; i++, shift -= 8, mask >>= 8)
        stream[i] = (RIO_UCHAR_T)((dw->ls_Word & mask) >> shift);
}

/***************************************************************************
 * Function : rio_Switch_Get_Prio
 *
 * Description: detect prio field
 *
 * Returns: prio
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Get_Prio(RIO_UCHAR_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return (arbiter[1] & 0xc0) >> 6; /* get two most significant bits from the first byte */
}

/***************************************************************************
 * Function : rio_Switch_Stream_To_Dw
 *
 * Description: convert bytes to dws
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Stream_To_Dw(RIO_DW_T *dws, RIO_UCHAR_T *stream, unsigned int size)
{
    unsigned int i; /* loop counter */

    if (stream == NULL)
        return;

    for (i = 0; i < size; i++)
    {
        /* convert one dword to eight bytes from the most significant - is shifted to the
         * right on 24 bits to the less significant is shifted to 0 bits */
        dws[i].ms_Word = ((unsigned long)stream[i * 8] << 24);
        dws[i].ms_Word |= ((unsigned long)stream[i * 8 + 1] << 16);
        dws[i].ms_Word |= ((unsigned long)stream[i * 8 + 2] << 8);
        dws[i].ms_Word |= (unsigned long)stream[i * 8 + 3];

        /* second word the legend is the same */
        dws[i].ls_Word = ((unsigned long)stream[i * 8 + 4] << 24);
        dws[i].ls_Word |= ((unsigned long)stream[i * 8 + 5] << 16);
        dws[i].ls_Word |= ((unsigned long)stream[i * 8 + 6] << 8);
        dws[i].ls_Word |= (unsigned long)stream[i * 8 + 7];
    }
}

/***************************************************************************
 * Function : handler_Train_Pre_Reset_State
 *
 * Description: input training state machine handler of situation, when 4 Link_request/reset
 *                is receiving in row. 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Handler_Pre_Reset_State(
    RIO_SWITCH_DS_T *handle, RIO_GRANULE_T *granule, unsigned int link_Number)
{
    if (handle == NULL || granule == NULL || link_Number >= RIO_SWITCH_GET_PORTS_CNT(handle))
        return;

    switch (granule->granule_Type)
    {
        case RIO_GR_IDLE:
            return;

        case RIO_GR_LINK_REQUEST:
            if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_RESET)
            {
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Req_Counter.reset_Num++;
                if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Req_Counter.reset_Num == 3)
                {
/*                    RIO_PL_Print_Warning(handle, RIO_PL_WARNING_2); */
                    handle->internal_Ftray.rio_Switch_Link_Start_Reset((RIO_HANDLE_T)handle, link_Number);
                    /*RIO_Switch_Internal_Init(handle);*/
                    handle->internal_Ftray.rio_Switch_Link_Internal_Init((RIO_HANDLE_T)handle, link_Number);
                }
                return;
            }
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State = RIO_SWITCH_RESET_OK;
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Req_Counter.reset_Num = 0;
            /*RIO_PL_Print_Warning(handle, RIO_PL_WARNING_7,
                    handle->rx_Data.train_state);*/
            return;
        default:
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State = RIO_SWITCH_RESET_OK;
            RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Req_Counter.reset_Num = 0;
            /*O_PL_Print_Warning(handle, RIO_PL_WARNING_7,
                    handle->rx_Data.train_state);*/
            return;
    }

}
/***************************************************************************
 * Function : in_Check_Current
 *
 * Description: check receiving packet stream if it could be parsered
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_In_Check_Current(RIO_SWITCH_DS_T *handle,
                            RIO_BOOL_T* error_Flag,
                            RIO_PACKET_VIOLATION_T *violation_Type,
                            unsigned int link_Number)
{
    int check_Result;

    RIO_SWITCH_PACKET_BUFFER_T* arbiter;

    if (handle == NULL || violation_Type == NULL || error_Flag == NULL)
        return RIO_ERROR; /*internal error*/

    arbiter = &RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer;

    /*check if packet is greater than 276 bytes*/
 /*   if (arbiter->cur_Pos > RIO_BC_PL_MAX_PROTOCOL_PACKET_LEN)
    {
        *error_Flag = RIO_BC_TRUE;
        *violation_Type = RIO_BC_PACKET_IS_LONGER_276;
        return RIO_BC_OK;
    }
*/
    if ((check_Result = rio_Switch_Check_Ftype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = FTYPE_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = rio_Switch_Check_Tt(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TT_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = rio_Switch_Check_CRC(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = CRC_ERROR;
            return RIO_OK;
    }

    if ((check_Result = rio_Switch_Check_Header_Size(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = INCORRECT_PACKET_HEADER;
            return RIO_OK;
    }

    if ((check_Result = rio_Switch_Check_Payload_Align(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = PAYLOAD_NOT_DW_ALIGNED;
            return RIO_OK;
    }
   
    if ((check_Result = rio_Switch_Check_Ttype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TTYPE_RESERVED;
            return RIO_OK;
    }
    
    /*check reserved field value*/
    if ((check_Result = rio_Switch_Check_Reserved_Fields(handle, arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = NON_ZERO_RESERVED_PACKET_BITS_VALUE;
            return RIO_OK;            
 
    }
    

    *error_Flag = RIO_FALSE;
    return RIO_OK;
}

/***************************************************************************
 * Function : check_Ftype
 *
 * Description: check if ftype is correct to reserved value
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_Ftype(RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    unsigned int ftype;

    if (arbiter == NULL)
        return RIO_ERROR;

    ftype = rio_Switch_Get_Ftype(arbiter->buffer); 

    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
        case RIO_NONINTERV_REQUEST:
        case RIO_WRITE:
        case RIO_STREAM_WRITE:
        case RIO_MAINTENANCE:
        case RIO_DOORBELL:
        case RIO_MESSAGE:
        case RIO_RESPONCE:
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
}
/***************************************************************************
 * Function : check_Tt
 *
 * Description: check if tt is correct to reserved values
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_Tt(RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    unsigned int tt;

    if (arbiter == NULL)
        return RIO_ERROR;

    tt = rio_Switch_Get_Tt(arbiter->buffer); 

    switch (tt)
    {
        case RIO_PL_TRANSP_16:
        case RIO_PL_TRANSP_32:
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
}
/***************************************************************************
 * Function : check_Ttype
 *
 * Description: check if ttype is correct to reserved value
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_Ttype(RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    unsigned int ftype, ttype;

    if (arbiter == NULL)
        return RIO_ERROR;

    ftype = rio_Switch_Get_Ftype(arbiter->buffer);
    ttype = rio_Switch_Get_Ttype(arbiter->buffer);

    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            if (ttype != RIO_INTERV_READ_OWNER &&
                ttype != RIO_INTERV_READ_TO_OWN_OWNER &&
                ttype != RIO_INTERV_IO_READ_OWNER)
                return RIO_ERROR;
            break;
        case RIO_NONINTERV_REQUEST:
            break;/*all transactions are allowed*/
        case RIO_WRITE:
            if (ttype != RIO_WRITE_CASTOUT &&
                ttype != RIO_WRITE_FLUSH_WITH_DATA &&
                ttype != RIO_WRITE_NWRITE &&
                ttype != RIO_WRITE_NWRITE_R &&
                ttype != RIO_WRITE_ATOMIC_TSWAP &&
		/* GDA: Added for Bug#133. Included support for TType C for Type 5 packet */
                ttype != RIO_WRITE_ATOMIC_SWAP)
                return RIO_ERROR;
            break;
        case RIO_STREAM_WRITE:
            break; /*there is no transaction field*/
        case RIO_MAINTENANCE:
            if (ttype != RIO_MAINTENANCE_CONF_READ &&
                ttype != RIO_MAINTENANCE_CONF_WRITE &&
                ttype != RIO_MAINTENANCE_READ_RESPONCE &&
                ttype != RIO_MAINTENANCE_WRITE_RESPONCE &&
                ttype != RIO_MAINTENANCE_PORT_WRITE)
                return RIO_ERROR;
            break;
        case RIO_DOORBELL:
        case RIO_MESSAGE:
            break;/*there is no transaction field*/
        case RIO_RESPONCE:
            if (ttype != RIO_RESPONCE_WO_DATA &&
                ttype != RIO_RESPONCE_MESSAGE &&
                ttype != RIO_RESPONCE_WITH_DATA)
                return RIO_ERROR;
            break;
        default:
            return RIO_ERROR;
    }
    return RIO_OK;
}
/***************************************************************************
 * Function : check_CRC
 *
 * Description: check if CRC is correct
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_CRC(RIO_SWITCH_DS_T *handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    RIO_UCHAR_T first_Byte;
    unsigned short crc = RIO_SWITCH_INIT_CRC, src_Crc;
    unsigned int first_Crc_Pos, start_For_Cnt = 0;
    unsigned int ftype, ttype, tt;

    if (arbiter == NULL || handle == NULL)
        return RIO_ERROR;

    /* CRC checking*/

    first_Byte = arbiter->buffer[0];
    
    arbiter->buffer[0] = 
        arbiter->buffer[0] & 0x03;

    /* get ftype, ttype etc. to find out if this packet is a maintenance request
       or response */
    ftype = rio_Switch_Get_Ftype( arbiter->buffer );
    ttype = rio_Switch_Get_Ttype( arbiter->buffer ); 
    tt = rio_Switch_Get_Tt( arbiter->buffer );

    /* 
     * check if this packet is MAINTENANCE request or MAINTENANCE reponse
     * if yes hop_Count needs to be temporarly returned to original value (incremented)
     * while checking CRC 
     */
    if (ftype == RIO_MAINTENANCE && rio_Switch_Is_Req(ftype, ttype) == RIO_OK && 
        arbiter->need_Recalc_CRC)
    {
        unsigned int hop_Count = 
            rio_Switch_Get_Hop_Count( arbiter->buffer, tt );
        rio_Switch_Upd_Hop_Count( arbiter->buffer, tt, ++hop_Count );
    }
    else if (ftype == RIO_MAINTENANCE && (ttype == RIO_MAINTENANCE_READ_RESPONCE ||
        ttype == RIO_MAINTENANCE_WRITE_RESPONCE) && arbiter->need_Recalc_CRC)
    {
        unsigned int hop_Count = 
            rio_Switch_Get_Hop_Count( arbiter->buffer, tt );
        rio_Switch_Upd_Hop_Count( arbiter->buffer, tt, ++hop_Count );
    }
    
    /* check if CRC was countered twice */
    if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
    {
        RIO_Switch_Compute_CRC(&crc, 
            arbiter->buffer, RIO_SWITCH_INT_CRC_POS);

        src_Crc = arbiter->buffer[RIO_SWITCH_INT_CRC_POS];
        src_Crc <<= 8; /* shift 8 bits to the left */
        src_Crc |= (unsigned short)(arbiter->buffer[RIO_SWITCH_INT_CRC_POS + 1]);

        /* next CRC will be counted from here */
 /*     start_For_Cnt = RIO_SWITCH_INT_CRC_POS + 2;*/ /* the next date is 2 bytes to the right */
        start_For_Cnt = RIO_SWITCH_INT_CRC_POS; /* the next data is 0 bytes to the right */

        if (src_Crc != crc)
        {
            return RIO_ERROR;
        }
    }

    /* here we check the next CRC */
    first_Crc_Pos = rio_Switch_Get_CRC_Start(arbiter, 
        handle->init_Param_Set.ext_Address, handle->init_Param_Set.ext_Address_16);

    /* check if CRC is 2 bytes aligned */
    if (first_Crc_Pos % 2)
    {
        return RIO_ERROR;
    }

    if (first_Crc_Pos <= start_For_Cnt)
    {
        return RIO_ERROR;
    }

    /* count CRC */
    RIO_Switch_Compute_CRC(&crc, 
        arbiter->buffer + start_For_Cnt, 
        first_Crc_Pos - start_For_Cnt);
    src_Crc = arbiter->buffer[first_Crc_Pos];
    src_Crc <<= 8; /* shift 8 bits to the left */
    src_Crc |= arbiter->buffer[first_Crc_Pos + 1];

    /* 
     * check if this packet is MAINTENANCE request or MAINTENANCE reponse
     * if yes hop_Count needs to be decremented back
     */
    if (ftype == RIO_MAINTENANCE && rio_Switch_Is_Req(ftype, ttype) == RIO_OK && 
        arbiter->need_Recalc_CRC)
    {
        unsigned int hop_Count = 
            rio_Switch_Get_Hop_Count( arbiter->buffer, tt );
        rio_Switch_Upd_Hop_Count( arbiter->buffer, tt, --hop_Count );
    }
    else if (ftype == RIO_MAINTENANCE && (ttype == RIO_MAINTENANCE_READ_RESPONCE ||
        ttype == RIO_MAINTENANCE_WRITE_RESPONCE) && arbiter->need_Recalc_CRC)
    {
        unsigned int hop_Count = 
            rio_Switch_Get_Hop_Count( arbiter->buffer, tt );
        rio_Switch_Upd_Hop_Count( arbiter->buffer, tt, --hop_Count );
    }
    
    /* check */
    if (src_Crc != crc)
    {
        return RIO_ERROR;
    }

    arbiter->buffer[0] = first_Byte;

    return RIO_OK;

}
/***************************************************************************
 * Function : get_Header_Size
 *
 * Description: calculate required packet header size depending on ftype,tt 
 *              and internal parameters value
 *
 * Returns: header size in bits   
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Get_Header_Size(RIO_SWITCH_DS_T * handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    /*temporary*/
    unsigned int tt, ftype;
    /*length of headers and required length of fields - in bits*/
    unsigned int required_Length, ext_Addr_Size, tr_Info_Size;
    RIO_BOOL_T ext_Address_Valid, ext_Address_16;

    if (arbiter == NULL || handle == NULL)
        return 0;

    /*get extended address params*/
    

    ext_Address_Valid = handle->init_Param_Set.ext_Address;
    ext_Address_16 = handle->init_Param_Set.ext_Address_16;

    ftype = rio_Switch_Get_Ftype(arbiter->buffer);
    if ((tt = rio_Switch_Get_Tt(arbiter->buffer)) == RIO_PL_TRANSP_16)
        tr_Info_Size = RIO_SWITCH_TR_INFO_SIZE_16; /*16 bit transport info is supposed*/
    else
        tr_Info_Size = RIO_SWITCH_TR_INFO_SIZE_32; /*32 bit transport info is supposed*/

    if (ext_Address_Valid == RIO_TRUE)
    {
        if(ext_Address_16 == RIO_TRUE)
            ext_Addr_Size = RIO_SWITCH_EXT_ADDR_SIZE_16; /*16 bit extended address is supposed*/
        else
            ext_Addr_Size = RIO_SWITCH_EXT_ADDR_SIZE_32; /*32 bit extended address is supposed*/
    }
    else
        ext_Addr_Size = 0; /*no extended address is supposed*/
    
    /*necessary part of all packets*/
    required_Length = RIO_SWITCH_FIRST_16_BIT_SIZE + tr_Info_Size;

    /*add size of packet depended headers*/
    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            required_Length += ext_Addr_Size + RIO_SWITCH_PACK_1_HEADER_SIZE;
            break;

        case RIO_NONINTERV_REQUEST:
            required_Length += ext_Addr_Size + RIO_SWITCH_PACK_2_HEADER_SIZE;
            break;
        case RIO_WRITE:
            required_Length += ext_Addr_Size + RIO_SWITCH_PACK_5_HEADER_SIZE;
            break;
        case RIO_STREAM_WRITE:
            required_Length += ext_Addr_Size + RIO_SWITCH_PACK_6_HEADER_SIZE;
            break;
        case RIO_MAINTENANCE:
            required_Length += RIO_SWITCH_PACK_8_HEADER_SIZE;
            break;
        case RIO_DOORBELL:
            required_Length += RIO_SWITCH_PACK_10_HEADER_SIZE;
            break;

        case RIO_MESSAGE:
            required_Length += RIO_SWITCH_PACK_11_HEADER_SIZE;
            break;

        case RIO_RESPONCE:
            required_Length += RIO_SWITCH_PACK_13_HEADER_SIZE;
            break;
        default:
            break; /*impossible*/
    }
    
    return required_Length;

}
/***************************************************************************
 * Function : check_Header_Size
 *
 * Description: check if header length is correct
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_Header_Size(RIO_SWITCH_DS_T * handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    /*length of headers*/
    unsigned int required_Length;
    if (arbiter == NULL || handle == NULL)
        return RIO_ERROR;

    /*get required header size in bits */
    required_Length = rio_Switch_Get_Header_Size(handle, arbiter) + RIO_SWITCH_CRC_FIELD_SIZE;
    /*convert from bit count to byte count*/
    required_Length = (unsigned int)(required_Length / RIO_SWITCH_BITS_IN_BYTE) + 
        ((required_Length % RIO_SWITCH_BITS_IN_BYTE) == 0 ? 0 : 1); 

    /* arbiter data is always aligned to 32 bit boundary,
        so required_Length align is unnecessary*/
    if (arbiter->buffer_Top_For_Load < required_Length)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Payload_Align
 *
 * Description: check if payload is aligned to dw boundary
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_Payload_Align(RIO_SWITCH_DS_T * handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    /*length of headers*/
    unsigned int required_Length, first_Crc_Pos;
    if (arbiter == NULL || handle == NULL)
        return RIO_ERROR;

    /*get first CRC Pos - packet end pointer without CRC and padding*/
    first_Crc_Pos = rio_Switch_Get_CRC_Start(arbiter,
        handle->init_Param_Set.ext_Address, handle->init_Param_Set.ext_Address_16);
    /*get required header size in bits */
    required_Length = rio_Switch_Get_Header_Size(handle, arbiter);
    /*if CRC is single, decrease header lenght to it -neccesary to calculate payload size*/
    if (arbiter->buffer_Top_For_Load > RIO_SWITCH_PACK_NEED_INT_CRC)
        required_Length += RIO_SWITCH_CRC_FIELD_SIZE;  
    

    /*convert from bit count to byte count*/
    required_Length = (unsigned int)(required_Length / RIO_SWITCH_BITS_IN_BYTE) + 
        ((required_Length % RIO_SWITCH_BITS_IN_BYTE) == 0 ? 0 : 1); 

    /* calculate payload size and compare it to DW boundary*/
    if (((first_Crc_Pos - required_Length) % RIO_SWITCH_CNT_BYTES_DWORD) != 0)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Reserved_Fields
 *
 * Description: check if reserved fields are equal to zero
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_Reserved_Fields( RIO_SWITCH_DS_T *handle, RIO_SWITCH_PACKET_BUFFER_T *arbiter)
{
    /*all used numbers are individualy and can be changed only in ths part of spec*/
    /*length of headers*/
    unsigned int ftype, head_Size, field_Pos_Start;
    int field_Size;

    if (arbiter == NULL ||
        handle == NULL)
        return RIO_ERROR;

    /*check physical fields, common to all, in first byte */
    /*first reserved field*/
    /*depending on parallel or serial mode*/
    if (handle->is_Parallel_Model == RIO_TRUE)
    {
        if ((arbiter->buffer[0] & 0x08) != 0)
            return RIO_ERROR;
        /*second reserved bit*/
        if ((arbiter->buffer[0] & 0x03) != 0)
            return RIO_ERROR;
    }
    else
    {
        if ((arbiter->buffer[0] & 0x07) != 0)
            return RIO_ERROR;
    }

    /*check reserved fields depending on ftype*/
        /*get required header size in bits */
    head_Size = rio_Switch_Get_Header_Size(handle, arbiter);
    ftype = rio_Switch_Get_Ftype(arbiter->buffer);

    /*here the the place of reserved field will be detected 
    by calculating it from the end of appropriate packet header*/
    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_NONINTERV_REQUEST:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_WRITE:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_STREAM_WRITE:
            /*pos in bits count*/
            field_Pos_Start = head_Size - 3;
            field_Size = RIO_SWITCH_PACK_6_RSRV_FIELD_SIZE;
            return rio_Switch_Check_Reserved_Field(arbiter, field_Pos_Start, field_Size);
        
        case RIO_MAINTENANCE:
            if (rio_Switch_Is_Req(ftype, rio_Switch_Get_Ttype(arbiter->buffer)) == RIO_TRUE)
            {
                field_Pos_Start = head_Size - 2;
                field_Size = RIO_SWITCH_PACK_8_REQ_RSRV_FIELD_SIZE;
            }
            else
            {
                field_Pos_Start = head_Size - 24;
                field_Size = RIO_SWITCH_PACK_8_RESP_RSRV_FIELD_SIZE;
            }
            return rio_Switch_Check_Reserved_Field(arbiter, field_Pos_Start, field_Size);
        
        case RIO_DOORBELL:
            field_Pos_Start = head_Size - 32;
            field_Size = RIO_SWITCH_PACK_10_RSRV_FIELD_SIZE;
            return rio_Switch_Check_Reserved_Field(arbiter, field_Pos_Start, field_Size);

        case RIO_MESSAGE:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_RESPONCE:
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : check_Reserved_Field
 *
 * Description: check if reserved field is equal to zero
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int rio_Switch_Check_Reserved_Field(
    RIO_SWITCH_PACKET_BUFFER_T *arbiter, unsigned int field_Pos_Start, unsigned int field_Size)
{
    unsigned int byte_Num, mask;
    
    byte_Num = field_Pos_Start / RIO_SWITCH_BITS_IN_BYTE;
    /*start pos in byte*/
    field_Pos_Start = field_Pos_Start % RIO_SWITCH_BITS_IN_BYTE;
    while (field_Size > 0)
    {
        if (field_Size > (RIO_SWITCH_BITS_IN_BYTE - field_Pos_Start))
        {
            mask = ((char) (0x1) << (RIO_SWITCH_BITS_IN_BYTE - field_Pos_Start)) - 1;
            field_Size -= (RIO_SWITCH_BITS_IN_BYTE - field_Pos_Start);
            field_Pos_Start = 0;
            if ((arbiter->buffer[byte_Num] & mask) != 0)
                return RIO_ERROR;
            byte_Num++;
        }
        else
        {
            mask = ( ( (char) (0x1) << (RIO_SWITCH_BITS_IN_BYTE - field_Pos_Start)) - 1) -
                (( (char) (0x1) << (RIO_SWITCH_BITS_IN_BYTE - (field_Pos_Start + field_Size))) - 1);
            field_Size = 0;
            if ((arbiter->buffer[byte_Num] & mask) != 0)
                return RIO_ERROR;
        }
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : check_States
 *
 * Description: 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Check_States(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Parameter;

    if (handle == NULL)
        return;

    RIO_Switch_Get_State(handle, RIO_INPUT_ERROR, &indicator_Value, &indicator_Parameter, link_Number);

    if (indicator_Value == RIO_TRUE && 
        (RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->rx_Data.global_State == RIO_SWITCH_LINK_OK ||
        RIO_SWITCH_GET_LNK_DATA(handle, link_Number)->rx_Data.global_State == RIO_SWITCH_LINK_STOPPED_DUE_TO_RETRY))
    {
        /*if (indicator_Parameter == RIO_PL_PNACC_GENERAL_ERROR)
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_59);*/
        rio_Switch_Start_Rx_Error_Rec(handle, link_Number, indicator_Parameter);
    }
 
    return;
}

/*****************************************************************************/




