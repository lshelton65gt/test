/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/rio_switch_parallel_model.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  Parallel RapidIO Switch model main source code file.
*
* Notes: instantiates number of 32-Bit TxRx modules
*
******************************************************************************/



#include <malloc.h>
#include <stdlib.h>

#include "rio_switch_parallel_model.h"
#include "rio_switch_model_common.h"

#include "rio_switch_ds.h"
#include "rio_switch_errors.h"
#include "rio_switch_tx.h"
#include "rio_switch_rx.h"
#include "rio_switch_registers.h"

#include "rio_32_txrx_model.h"
#include "rio_parallel_init.h"




/* Number of allowed ackIDs */
#define RIO_SWITCH_PARALLEL_ACKID_QUEUE_SIZE 8

/* variable holds count of created switch instanses*/
static int rio_Switch_Inst_Count = 0;

/* switch transmitter clock function */
static int rio_Switch_Clock_Edge(RIO_HANDLE_T instanse);

/* turns the switch to reset mode */
static int rio_Switch_Start_Reset(RIO_HANDLE_T instanse);

/* function which initializes the switch */
static int rio_Switch_Initialize(
    RIO_HANDLE_T                    instanse,
    RIO_SWITCH_PARALLEL_PARAM_SET_T *param_Set
    );

/* deletes an instance of the switch model */
static int rio_Switch_Delete_Instance(RIO_HANDLE_T handle);

/* checks switch instantiation parameters */
static int switch_Check_Inst_Param_Set(RIO_SWITCH_INST_PARAM_T *param_Set);

/* checks switch initialization parameters */
static int switch_Check_Init_Param_Set(RIO_SWITCH_DS_T *handle, RIO_SWITCH_PARALLEL_PARAM_SET_T *param_Set);

/* 
 * this function provides actual implementation of the 
 * intialization process du eto necessaity to call it
 * when four LR/RESET are received in row
 */
static int switch_Internal_Init(RIO_HANDLE_T handle);

static int switch_Link_Internal_Init(RIO_HANDLE_T instanse, unsigned int link_Number);

static int rio_Switch_Link_Start_Reset(RIO_HANDLE_T instanse, unsigned int link_Number);




/***************************************************************************
 * 
 * RapidIO Parallel Switch Model global functions 
 *
 **************************************************************************/

/***************************************************************************
 * Function : RIO_Switch_Parallel_Create_Instance
 *
 * Description: Creates an instance of the 8/16 LP-LVDS capable RapidIO 
 *              Switch model
 *
 * Returns: error code 
 *
 * Notes: handlers are returning through parameters by the reference
 *
 **************************************************************************/
int RIO_Switch_Parallel_Create_Instance(
    RIO_HANDLE_T            *instanse, 
    RIO_HANDLE_T            **link_Handles, 
    RIO_SWITCH_INST_PARAM_T *param_Set
)
{
    RIO_SWITCH_DS_T  *handle; /* holds data structure */
    unsigned int i;           /* loop counter */

    if (instanse == NULL || link_Handles == NULL)
        return RIO_ERROR;

    /* check instantiation parameters */
    if (switch_Check_Inst_Param_Set(param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* clean pointers*/
    *instanse = NULL;
    *link_Handles = NULL;

    /* create switch data */
    if ((handle = malloc(sizeof(RIO_SWITCH_DS_T))) == 0)
        return RIO_ERROR;

    /* set the model as parallel one */
    handle->is_Parallel_Model = RIO_TRUE;

    /*set size of ackID queues*/
    handle->ack_Buffer_Size = RIO_SWITCH_PARALLEL_ACKID_QUEUE_SIZE;
    
    /* calculate registers block size */
    handle->reg_Set.lpep_Size = RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX * param_Set->link_Number; 

    /* create registers */
    if ((handle->reg_Set.lpep_Regs = malloc(sizeof(RIO_SWITCH_LPEP_REGS_T) * param_Set->link_Number)) == NULL)
    {
        free(handle);
        return RIO_ERROR;
    }

    /* create array of pointers to LPEP handles*/
    if ((*link_Handles = malloc(sizeof(RIO_HANDLE_T) * param_Set->link_Number)) == NULL)
    {
        *link_Handles = NULL;
        free(handle->reg_Set.lpep_Regs);
        free(handle);
        return RIO_ERROR;
    }

    /* create an instance of the link specific callback tray in the switch DS */
    if ( (handle->link_Specific_Callbacks = malloc(sizeof(RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T))) == NULL)
    {
        free(handle->reg_Set.lpep_Regs);
        free(handle);
        free(*link_Handles);
        *link_Handles = NULL;

        return RIO_ERROR;
    }

    /* create an instance of the link specific function tray in the switch DS */
    if ( (handle->link_Ftray = malloc(sizeof(RIO_LINK_MODEL_FTRAY_T))) == NULL)
    {
        free(handle->link_Specific_Callbacks);
        free(handle->reg_Set.lpep_Regs);
        free(handle);
        free(*link_Handles);
        *link_Handles = NULL;

        return RIO_ERROR;
    }

    /* create an instance of the parallel init block function tray in the switch DS */
    if ( (handle->mid_Block_Ftray = malloc(sizeof(RIO_PL_PAR_INIT_BLOCK_FTRAY_T))) == NULL)
    {
        free(handle->link_Ftray);
        free(handle->link_Specific_Callbacks);
        free(handle->reg_Set.lpep_Regs);
        free(handle);
        free(*link_Handles);
        *link_Handles = NULL;

        return RIO_ERROR;
    }

    /* allocate memory for switch link context */
    if ( (((RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T*)(handle->link_Specific_Callbacks))->lpep_Interface_Context 
        = malloc(sizeof(RIO_CONTEXT_T) * RIO_SWITCH_MAX_LINK_CNT)) == NULL )
    {
        free(handle->link_Specific_Callbacks);
        free(handle->mid_Block_Ftray);
        free(handle->link_Ftray);
        free(handle->reg_Set.lpep_Regs);
        free(handle);
        free(*link_Handles);
        *link_Handles = NULL;

        return RIO_ERROR;
    }

    /* clean callback pointers */
    handle->switch_Callbacks.rio_User_Msg = NULL;
    ((RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T*)(handle->link_Specific_Callbacks))->rio_Ph_Set_Pins = NULL;

    /* clean link pointers */
    ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Get_Pins = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Clock_Edge = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Start_Reset = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Initialize = NULL;

    ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Tx_Disabling = NULL;
    ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Rx_Disabling = NULL;

    /*
     *clear parallel init blocks FTray
     */
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Start_Reset = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Initialize  = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Get_Granule = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Put_Granule = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Delete_Instance = NULL;
    ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Clock_Edge = NULL;

    /* clean internal function tray pointers */
    handle->internal_Ftray.rio_Link_Rx_Disabling = NULL;
    handle->internal_Ftray.rio_Link_Tx_Disabling = NULL;
    handle->internal_Ftray.rio_Switch_Internal_Init = NULL;
    handle->internal_Ftray.rio_Switch_Start_Reset   = NULL;
    handle->internal_Ftray.rio_Switch_Link_Internal_Init = NULL;
    handle->internal_Ftray.rio_Switch_Link_Start_Reset   = NULL;

    /* clean routing table pointer */
    handle->init_Param_Set.routing_Table   = NULL;
    handle->init_Param_Set.rout_Table_Size = 0;

    /* set link parameters to the default state */
    for ( i = 0; i < param_Set->link_Number; i++ )
    {
        handle->link_Common_Params[i].lp_Ep_Is_8     = RIO_FALSE;
        handle->link_Common_Params[i].work_Mode_Is_8 = RIO_FALSE;
    }

    /* clean pointers to 8/16 LP-EP links */
    for (i = 0; i < RIO_SWITCH_MAX_LINK_CNT; i++)
    {
        if (i < param_Set->link_Number)
            (*link_Handles)[i] = NULL;

        ((RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T*)(handle->link_Specific_Callbacks))->lpep_Interface_Context[i] = NULL;
    }

    /* 
     * try to create necessary number of the 32bit TxRx's and Parallel Init Blocks
     */
    for (i = 0; i < param_Set->link_Number; i++)
    {
        RIO_Link_Model_Create_Instance(*link_Handles + i);
        RIO_PL_Par_Init_Block_Create_Instance(handle->mid_Block_Handles + i);

        if ( ((*link_Handles)[i] == NULL) || (handle->mid_Block_Handles[i] == NULL) )
            break;
    }

    /* if all instanses were created successfully i == link_Number */
    if ( (i == param_Set->link_Number) && 
        (RIO_Link_Model_Get_Function_Tray((RIO_LINK_MODEL_FTRAY_T*)handle->link_Ftray) == RIO_OK) &&
        (RIO_PL_Par_Init_Block_Get_Function_Tray((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)handle->mid_Block_Ftray) == RIO_OK) )
    {
        /* increase count of switch instanses */
        handle->instance_Number = ++rio_Switch_Inst_Count;

        /* save LP/EP handlers and link count */
        handle->inst_Param_Set.link_Number       = param_Set->link_Number;

        handle->inst_Param_Set.device_Identity        = param_Set->device_Identity;
        handle->inst_Param_Set.device_Vendor_Identity = param_Set->device_Vendor_Identity;
        handle->inst_Param_Set.device_Rev             = param_Set->device_Rev;
        handle->inst_Param_Set.device_Minor_Rev       = param_Set->device_Minor_Rev;
        handle->inst_Param_Set.device_Major_Rev       = param_Set->device_Major_Rev;
        handle->inst_Param_Set.assy_Identity          = param_Set->assy_Identity;
        handle->inst_Param_Set.assy_Vendor_Identity   = param_Set->assy_Vendor_Identity;
        handle->inst_Param_Set.assy_Rev               = param_Set->assy_Rev;

        handle->inst_Param_Set.entry_Extended_Features_Ptr = param_Set->entry_Extended_Features_Ptr;


        for (i = 0; i < param_Set->link_Number; i++)
            handle->link_Handles[i] = (*link_Handles)[i];

        /* fill in the internal function tray */
        handle->internal_Ftray.rio_Link_Tx_Disabling    = ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Tx_Disabling;
        handle->internal_Ftray.rio_Link_Rx_Disabling    = ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Rx_Disabling;
        handle->internal_Ftray.rio_Switch_Internal_Init = switch_Internal_Init;
        handle->internal_Ftray.rio_Switch_Start_Reset   = rio_Switch_Start_Reset;

        handle->internal_Ftray.rio_Switch_Link_Internal_Init = switch_Link_Internal_Init;
        handle->internal_Ftray.rio_Switch_Link_Start_Reset   = rio_Switch_Link_Start_Reset;


        /* after creation the switch is in reset */
        handle->in_Reset = RIO_TRUE;

        /* other handles are NULL*/
        for (; i < RIO_SWITCH_MAX_LINK_CNT; i++)
            handle->link_Handles[i] = NULL;

        /* store handle */
        *instanse = handle;

        return RIO_OK;

    }

    /* deallocate memory */
    free(handle->reg_Set.lpep_Regs);
    free(handle);
    for(i = 0; i < param_Set->link_Number; i++)
    {
        if (link_Handles[i])
            free((*link_Handles)[i]);
    }

    free(*link_Handles);
    *link_Handles = NULL;

    return RIO_ERROR;
}


/***************************************************************************
 * Function : RIO_Switch_Parallel_Get_Function_Tray
 *
 * Description: Returns function tray of the parallel RapidIO Switch model
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Parallel_Get_Function_Tray(
    RIO_SWITCH_PARALLEL_FTRAY_T *ftray
)
{
    RIO_LINK_MODEL_FTRAY_T  link_Ftray; /* link Ftray*/

    if (ftray == NULL)
        return RIO_ERROR;

    /* try to get lpep link function tray */
    if (RIO_Link_Model_Get_Function_Tray(&link_Ftray) == RIO_ERROR)
        return RIO_ERROR;

    /* store pointers */
    ftray->rio_Ph_Get_Pins        = link_Ftray.rio_Link_Get_Pins;
    ftray->rio_Switch_Clock_Edge  = rio_Switch_Clock_Edge;
    ftray->rio_Switch_Initialize  = rio_Switch_Initialize;
    ftray->rio_Switch_Start_Reset = rio_Switch_Start_Reset;

    ftray->rio_Switch_Print_Version = RIO_Switch_Print_Version;

    ftray->rio_Switch_Delete_Instance = rio_Switch_Delete_Instance;
    
    return RIO_OK;
}


/***************************************************************************
 * Function : RIO_Switch_Parallel_Bind_Instance
 *
 * Description: Binds Parallel RapidIO Switch Model with the environment
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Parallel_Bind_Instance(
    RIO_HANDLE_T                        instanse, 
    RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T *ctray
)
{
    RIO_SWITCH_DS_T                *handle = instanse; /* points to switch data */
    RIO_LINK_MODEL_CALLBACK_TRAY_T        link_Ctray;
    RIO_PL_PAR_INIT_BLOCK_CALLBACK_TRAY_T par_Init_Ctray;
    unsigned int i;

    if (handle == NULL || ctray == NULL)
        return RIO_ERROR;

    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /* check callbacks pointers */
    if ( ctray->rio_Ph_Set_Pins == NULL || ctray->rio_User_Msg == NULL)
        return RIO_ERROR;

    /*
     * prepare TxRx model callback tray 
     */
    link_Ctray.rio_Ph_Set_Pins          = ctray->rio_Ph_Set_Pins;
    link_Ctray.rio_Link_Get_Granule     = ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)
        (handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Get_Granule;
    link_Ctray.rio_Link_Put_Granule     = ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)
        (handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Put_Granule;

    link_Ctray.rio_User_Msg             = RIO_Switch_Msg;
    link_Ctray.error_Warning_Context    = instanse;

    link_Ctray.rio_Link_Input_Present   = RIO_Switch_Link_Input_Present;
    link_Ctray.rio_Granule_Received     = RIO_Switch_Granule_Received;
    link_Ctray.rio_Symbol_To_Send       = RIO_Switch_Symbol_To_Send;
    link_Ctray.rio_Hooks_Context        = instanse;

    /*
     * prepare Parallel Init block callback tray 
     */
    par_Init_Ctray.rio_Link_Get_Granule = RIO_Switch_Get_Granule;
    par_Init_Ctray.rio_Link_Put_Granule = RIO_Switch_Put_Granule;
    par_Init_Ctray.rio_PL_Interface_Context = instanse; 
    
    par_Init_Ctray.rio_Set_State_Flag   = RIO_Switch_Set_State_Flag;
    par_Init_Ctray.rio_Get_State_Flag   = RIO_Switch_Get_State_Flag;
    par_Init_Ctray.rio_Regs_Context     = instanse; 
    
    par_Init_Ctray.rio_User_Msg         = RIO_Switch_Msg;
    par_Init_Ctray.rio_Msg              = instanse;

    /*
     * try to bind all links and Parallel Init blocks
     */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        /* prepare link callback tray changeble part */
        link_Ctray.lpep_Interface_Context   = ctray->lpep_Interface_Context[i];
        link_Ctray.palpdl_Interface_Context = (RIO_HANDLE_T)(handle->mid_Block_Handles[i]);

        /* bind link model */
        if ( RIO_Link_Model_Bind_Instance(handle->link_Handles[i], &link_Ctray) == RIO_ERROR )
            return RIO_ERROR;

        /* bind Parallel Init block */
        if ( RIO_PL_Par_Init_Block_Bind_Instance( handle->mid_Block_Handles[i], &par_Init_Ctray ) == RIO_ERROR )
            return RIO_ERROR;
    }

    /* 
     * store callbacks in the switch DS
     */
    *((RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T*)(handle->link_Specific_Callbacks)) = *ctray;
    handle->switch_Callbacks.rio_User_Msg    = ctray->rio_User_Msg;
    handle->switch_Callbacks.rio_Msg_Context = ctray->rio_Msg_Context;

    return RIO_OK;

}




/***************************************************************************
 * 
 * RapidIO Parallel Switch Model static functions to be passed via FTray
 *
 **************************************************************************/

/***************************************************************************
 * Function : rio_Switch_Clock_Edge
 *
 * Description: switch transmitter clock
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Clock_Edge(RIO_HANDLE_T instanse)
{
    RIO_SWITCH_DS_T *handle = instanse; /* points to switch data */
    unsigned int i;                     /* loop counter */
    int  tx_Clock_Result;               /* callback invokation result */

    if (handle == NULL)
        return RIO_ERROR;

    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /* in reset mode switch doesnot work */
    if (handle->in_Reset == RIO_TRUE)
        return RIO_OK;

    /* invoke transmitter clock for all links */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        if ( ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Clock_Edge == NULL)
            return RIO_ERROR;

        tx_Clock_Result = ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Clock_Edge(
            handle->link_Handles[i]);

        if (tx_Clock_Result == RIO_ERROR)
            return RIO_ERROR;
    }

    /* LINK_REQUESTs time-out counting */
    RIO_Switch_Tx_Link_Req_Time_Out(handle);
    RIO_Switch_Tx_Phys_Time_Out(handle);

    return RIO_OK;
}


/***************************************************************************
 * Function : rio_Switch_Start_Reset
 *
 * Description: start reset mode 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Start_Reset(RIO_HANDLE_T instanse)
{
    RIO_SWITCH_DS_T *handle = instanse; /* points to the switch data */
    unsigned int    i;                  /* loop counter */

    if (handle == NULL || handle->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /* start reset on all links */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        if ( ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Start_Reset(
            handle->link_Handles[i]) == RIO_ERROR )
            return RIO_ERROR;

        if ( ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Start_Reset(
            handle->mid_Block_Handles[i]) == RIO_ERROR )
            return RIO_ERROR;
    }

    /* turn flag */
    handle->in_Reset = RIO_TRUE;

    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        handle->link_Data[i].in_Reset = RIO_TRUE;
    }
    
    return RIO_OK;
}


/***************************************************************************
 * Function : rio_Switch_Initialize
 *
 * Description: initialize the switch 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Initialize(
    RIO_HANDLE_T                    instanse,
    RIO_SWITCH_PARALLEL_PARAM_SET_T *param_Set
)
{
    RIO_SWITCH_DS_T *handle = instanse; /* points to the switch data */
    unsigned int    i; /* loop counter */

    if (handle == NULL || param_Set == NULL || param_Set->lp_Ep_8_Used == NULL  ||
        param_Set->work_Mode_Is_8_Array == NULL ||
        param_Set->requires_Window_Alignment_Array == NULL ||
        param_Set->num_Trainings_Array == NULL)
            return RIO_ERROR;

    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /* initializing is only legal in reset mode */
    if (handle->in_Reset == RIO_FALSE)
    {
        return RIO_ERROR;
    }

    /* check parameters set */
    if (switch_Check_Init_Param_Set(handle, param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* try to load routing table */
    if (handle->init_Param_Set.routing_Table == NULL)
    {
        handle->init_Param_Set.routing_Table = malloc(sizeof(RIO_ROUTE_TABLE_ENTRY_T) * param_Set->rout_Table_Size);
        if (!handle->init_Param_Set.routing_Table)
        {
            handle->init_Param_Set.rout_Table_Size = 0;
            return RIO_ERROR;
        }
        handle->init_Param_Set.rout_Table_Size = param_Set->rout_Table_Size;
    }
    else
    {
        if (handle->init_Param_Set.rout_Table_Size < param_Set->rout_Table_Size)
        {
            handle->init_Param_Set.routing_Table = realloc(handle->init_Param_Set.routing_Table, 
                sizeof(sizeof(RIO_ROUTE_TABLE_ENTRY_T) * param_Set->rout_Table_Size));

            if (!handle->init_Param_Set.routing_Table)
            {
                handle->init_Param_Set.rout_Table_Size = 0;
                return RIO_ERROR;
            }
            handle->init_Param_Set.rout_Table_Size = param_Set->rout_Table_Size;
        }
    }


    /* store common parameters */
    {
        handle->init_Param_Set.rout_Table_Size = param_Set->rout_Table_Size;
        handle->init_Param_Set.ext_Address     = param_Set->ext_Address;
        handle->init_Param_Set.ext_Address_16  = param_Set->ext_Address_16;
        handle->init_Param_Set.rout_Alg        = RIO_TABLE_BASED_ROUTING;
    }

    /* store routing table */
    for (i = 0; i < param_Set->rout_Table_Size; i++)
        handle->init_Param_Set.routing_Table[i] = param_Set->routing_Table[i];

    /* store links' parameters */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        handle->link_Common_Params[i].lp_Ep_Is_8     = param_Set->lp_Ep_8_Used[i];
        handle->link_Common_Params[i].work_Mode_Is_8 = param_Set->work_Mode_Is_8_Array[i];
        handle->link_Common_Params[i].requires_Window_Alignment = param_Set->requires_Window_Alignment_Array[i];
        handle->link_Common_Params[i].num_Trainings  = param_Set->num_Trainings_Array[i];
        handle->link_Common_Params[i].transmit_Flow_Control_Support = RIO_FALSE;
    }  

    return switch_Internal_Init( instanse );
}


/***************************************************************************
 * Function : rio_Switch_Delete_Instance
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Delete_Instance(RIO_HANDLE_T instanse)
{

    RIO_SWITCH_DS_T *handle = instanse; /* points to the switch data */

    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /* delete 32 txrx model for the links */
    
    /* delete switch itself */

    return RIO_OK;

}




/***************************************************************************
 * 
 * RapidIO Parallel Switch Model service static functions
 *
 **************************************************************************/

/***************************************************************************
 * Function : switch_Check_Inst_Param_Set
 *
 * Description: checks switch instantiation parameters 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int switch_Check_Inst_Param_Set(RIO_SWITCH_INST_PARAM_T *param_Set)
{
    if (param_Set == NULL)
        return RIO_ERROR;

    /* check ports count */
    if (param_Set->link_Number == 0 || param_Set->link_Number > RIO_SWITCH_MAX_LINK_CNT)
        return RIO_ERROR;

    /*check extended features ptr for lieing in the extended features space with its block */
    if ((param_Set->entry_Extended_Features_Ptr < RIO_EF_SPACE_BOTTOM) ||
        (param_Set->entry_Extended_Features_Ptr > 
        ((RIO_EF_SPACE_TOP + RIO_SWITCH_CNT_BYTES_DWORD) - 
        (RIO_LPEP_REG_GEN_SIZE + RIO_LPEP_REG_INDEX* param_Set->link_Number))) ||
        (param_Set->entry_Extended_Features_Ptr % RIO_SWITCH_CNT_BYTES_DWORD))
            return RIO_ERROR;

    return RIO_OK;
}


/***************************************************************************
 * Function : switch_Check_Init_Param_Set
 *
 * Description: checks switch initialization parameters 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int switch_Check_Init_Param_Set(
    RIO_SWITCH_DS_T *handle, 
    RIO_SWITCH_PARALLEL_PARAM_SET_T *param_Set
)
{
    unsigned int i; /* loop counter */

    if (param_Set == NULL || param_Set->lp_Ep_8_Used == NULL ||
        param_Set->requires_Window_Alignment_Array ==NULL ||
        param_Set->work_Mode_Is_8_Array == NULL ||
        param_Set->num_Trainings_Array == NULL
        /*|| param_Set->single_Data_Rate == NULL*/)
            return RIO_ERROR;

    /* check routing table */
    if (!param_Set->rout_Table_Size || param_Set->routing_Table == NULL)
        return RIO_ERROR;

    /* check physical layer parameters */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        if (param_Set->lp_Ep_8_Used[i] != RIO_TRUE &&
            param_Set->lp_Ep_8_Used[i] != RIO_FALSE)
                return RIO_ERROR;

        if (param_Set->requires_Window_Alignment_Array[i] != RIO_TRUE &&
            param_Set->requires_Window_Alignment_Array[i] != RIO_FALSE)
                return RIO_ERROR;

        if (param_Set->work_Mode_Is_8_Array[i] != RIO_TRUE &&
            param_Set->work_Mode_Is_8_Array[i] != RIO_FALSE)
                return RIO_ERROR;

        if (param_Set->lp_Ep_8_Used[i] == RIO_TRUE &&
            param_Set->work_Mode_Is_8_Array[i] == RIO_FALSE)
                return RIO_ERROR;

        if (param_Set->num_Trainings_Array[i] < 1)
            return RIO_ERROR;
        
    }

    /* check address lengh parameters */
    if (param_Set->ext_Address != RIO_TRUE &&
        param_Set->ext_Address != RIO_FALSE)
        return RIO_ERROR;

    if (param_Set->ext_Address_16 != RIO_TRUE &&
        param_Set->ext_Address_16 != RIO_FALSE)
        return RIO_ERROR;

    return RIO_OK;
}


/***************************************************************************
 * Function :   switch_Internal_Init
 *
 * Description: initializes the switch model instance
 *
 * Returns:     error code
 *
 * Notes: none
 *
 **************************************************************************/
static int switch_Internal_Init(RIO_HANDLE_T instanse)
{
    RIO_SWITCH_DS_T                *handle = (RIO_SWITCH_DS_T*)instanse;
    RIO_LINK_PARAM_SET_T           link_Param_Set; /* parameters to initialize link*/
    RIO_PAR_INIT_BLOCK_PARAM_SET_T init_Param_Set; /* parameters for Parallel Init block */
    unsigned int i;

    if ( handle == NULL )
        return RIO_ERROR;
    
    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /* prepare link paramters set */
    link_Param_Set.check_SECDED         = RIO_FALSE;
    link_Param_Set.correct_Using_SECDED = RIO_FALSE;

    /* prepare Parallel Init block parameters set */
    init_Param_Set.is_Init_Proc_On               = RIO_TRUE;
    init_Param_Set.transmit_Flow_Control_Support = RIO_FALSE;

    /* try initializing all links */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        link_Param_Set.lp_Ep_Is_8       = handle->link_Common_Params[i].lp_Ep_Is_8;
        link_Param_Set.single_Data_Rate = RIO_FALSE;
        link_Param_Set.work_Mode_Is_8   = handle->link_Common_Params[i].work_Mode_Is_8;
        
        if (link_Param_Set.work_Mode_Is_8 == RIO_TRUE)
        {
            link_Param_Set.training_Pattern[0]    = 0xffffffff;     
            link_Param_Set.training_Pattern[1]    = 0x00;
        }
        else
        {

            link_Param_Set.training_Pattern[0]    = 0xffffffff;     
            link_Param_Set.training_Pattern[1]    = 0xffffffff;     
            link_Param_Set.training_Pattern[2]    = 0x0;     
            link_Param_Set.training_Pattern[3]    = 0x0;     
        }

        /*necessary to detect if to toggle frame automatically or not*/
        link_Param_Set.is_TxRx_Model = RIO_FALSE;

        /* initialize link model */
        if (((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Initialize(
            handle->link_Handles[i], &link_Param_Set) == RIO_ERROR)
            return RIO_ERROR;

        /* initialize Parallel Init block */
        init_Param_Set.num_Trainings             = handle->link_Common_Params[i].num_Trainings;
        init_Param_Set.requires_Window_Alignment = handle->link_Common_Params[i].requires_Window_Alignment;

        if (((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Initialize(
            handle->mid_Block_Handles[i], &init_Param_Set) == RIO_ERROR)
            return RIO_ERROR;

    }

    /* turn to work mode */
    handle->in_Reset = RIO_FALSE;

    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        handle->link_Data[i].in_Reset = RIO_FALSE;
        /* initialize transmitter */

        RIO_Switch_Tx_Init(handle, i);

        /* initialize receiver */
        RIO_Switch_Rx_Init(handle, i);
    }

    /* initialize registers */
    RIO_Switch_Init_Regs(handle);
    

    /* initialize buffers*/
    for(i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
        handle->resp_Buffer[i].buffer_Head = 0;
        handle->resp_Buffer[i].buffer_Top = 0;
        handle->resp_Buffer[i].buffer_Top_For_Load = 0;
        handle->resp_Buffer[i].counting_CRC = 0xffff;
        handle->resp_Buffer[i].crc_Pos = 0;
        handle->resp_Buffer[i].need_Recalc_CRC = RIO_FALSE;
    }

    return RIO_OK;
}
/***************************************************************************
 * Function :   switch_Internal_Init
 *
 * Description: initializes the switch model instance
 *
 * Returns:     error code
 *
 * Notes: none
 *
 **************************************************************************/
static int switch_Link_Internal_Init(RIO_HANDLE_T instanse, unsigned int link_Number)
{
    RIO_SWITCH_DS_T                *handle = (RIO_SWITCH_DS_T*)instanse;
    RIO_LINK_PARAM_SET_T           link_Param_Set; /* parameters to initialize link*/
    RIO_PAR_INIT_BLOCK_PARAM_SET_T init_Param_Set; /* parameters for Parallel Init block */
    
    if ( handle == NULL )
        return RIO_ERROR;
    
    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /* prepare link paramters set */
    link_Param_Set.check_SECDED         = RIO_FALSE;
    link_Param_Set.correct_Using_SECDED = RIO_FALSE;

    /* prepare Parallel Init block parameters set */
    init_Param_Set.is_Init_Proc_On               = RIO_TRUE;
    init_Param_Set.transmit_Flow_Control_Support = RIO_FALSE;

    /* try initializing particular link */
    link_Param_Set.lp_Ep_Is_8       = handle->link_Common_Params[link_Number].lp_Ep_Is_8;
    link_Param_Set.single_Data_Rate = RIO_FALSE;
    link_Param_Set.work_Mode_Is_8   = handle->link_Common_Params[link_Number].work_Mode_Is_8;
        
    if (link_Param_Set.work_Mode_Is_8 == RIO_TRUE)
    {
        link_Param_Set.training_Pattern[0]    = 0xffffffff;     
        link_Param_Set.training_Pattern[1]    = 0x00;
    }
    else
    {
        link_Param_Set.training_Pattern[0]    = 0xffffffff;     
        link_Param_Set.training_Pattern[1]    = 0xffffffff;     
        link_Param_Set.training_Pattern[2]    = 0x0;     
        link_Param_Set.training_Pattern[3]    = 0x0;        
    }

        /*necessary to detect if to toggle frame automatically or not*/
    link_Param_Set.is_TxRx_Model = RIO_FALSE;

    /* initialize link model */
    if (((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Initialize(
            handle->link_Handles[link_Number], &link_Param_Set) == RIO_ERROR)
            return RIO_ERROR;

    /* initialize Parallel Init block */
    init_Param_Set.num_Trainings             = handle->link_Common_Params[link_Number].num_Trainings;
    init_Param_Set.requires_Window_Alignment = handle->link_Common_Params[link_Number].requires_Window_Alignment;

    if (((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Initialize(
        handle->mid_Block_Handles[link_Number], &init_Param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* turn to work mode */
    handle->link_Data[link_Number].in_Reset = RIO_FALSE;

    /* initialize transmitter */
    RIO_Switch_Tx_Init(handle, link_Number);

    /* initialize receiver */
    RIO_Switch_Rx_Init(handle, link_Number);

    /* initialize registers */
    RIO_Switch_Link_Init_Regs(handle, link_Number);


    /* initialize buffers*/
   
    handle->resp_Buffer[link_Number].buffer_Head = 0;
    handle->resp_Buffer[link_Number].buffer_Top = 0;
    handle->resp_Buffer[link_Number].buffer_Top_For_Load = 0;
    handle->resp_Buffer[link_Number].counting_CRC = 0xffff;
    handle->resp_Buffer[link_Number].crc_Pos = 0;
    handle->resp_Buffer[link_Number].need_Recalc_CRC = RIO_FALSE;

    RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).reset_Machine_State = RIO_SWITCH_RESET_POST_RESET;

    return RIO_OK;
}
/***************************************************************************
 * Function : rio_Switch_Start_Reset
 *
 * Description: start reset mode 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_Switch_Link_Start_Reset(RIO_HANDLE_T instanse, unsigned int link_Number)
{
    RIO_SWITCH_DS_T *handle = instanse; /* points to the switch data */

    if (handle == NULL || handle->link_Data[link_Number].in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check that the instance of the model was created as parallel one */
    if (!handle->is_Parallel_Model)
        return RIO_ERROR;

    /*stomp transmittion on connected link, if necessary*/
    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).state != RIO_SWITCH_LINK_FREE)
    {
        if (RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).buffer.buffer_Head)
        {
            /* send STOMP to the receiver */
            RIO_Switch_Tx_STOMP_Transmission(handle, 
                RIO_SWITCH_GET_LNK_RX_DATA(handle, link_Number).connected_To);
        }
    }

    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).state != RIO_SWITCH_LINK_FREE)
    {
        if (RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).conf_Req == RIO_TRUE &&
            RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To == RIO_SWITCH_NOT_CONNECTED)
        {
            ;
        }
        else
        {
            RIO_GRANULE_T ack_Granule;

            ack_Granule.granule_Type = RIO_GR_PACKET_RETRY;
            ack_Granule.granule_Struct.stype = RIO_GR_PACKET_RETRY_STYPE;
            ack_Granule.granule_Struct.buf_Status = 0;
            ack_Granule.granule_Struct.ack_ID = RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).expected_Ack_ID;
            ack_Granule.granule_Struct.s = 1;
            RIO_Switch_Rx_Rec_Ack(handle, &ack_Granule, 
                RIO_SWITCH_GET_LNK_TX_DATA(handle, link_Number).connected_To);
        }
    }

    /* start reset on link */
    if ( ((RIO_LINK_MODEL_FTRAY_T*)(handle->link_Ftray))->rio_Link_Start_Reset(
            handle->link_Handles[link_Number]) == RIO_ERROR )
            return RIO_ERROR;

    if ( ((RIO_PL_PAR_INIT_BLOCK_FTRAY_T*)(handle->mid_Block_Ftray))->rio_PL_Par_Init_Block_Start_Reset(
            handle->mid_Block_Handles[link_Number]) == RIO_ERROR )
            return RIO_ERROR;
    
    
   

    /* turn flag */
    handle->link_Data[link_Number].in_Reset = RIO_TRUE;

    return RIO_OK;
}

/*******************************************************************************************/

