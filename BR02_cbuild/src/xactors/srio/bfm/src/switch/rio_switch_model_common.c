/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/rio_switch_model_common.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  Parallel RapidIO Switch model main source code file.
*
* Notes: instantiates 32-Bit TxRx 
*
******************************************************************************/



#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

#include "rio_switch_ds.h"
#include "rio_switch_errors.h"
#include "rio_switch_registers.h"
#include "rio_switch_rx.h"
#include "rio_switch_tx.h"
#include "rio_switch_model_common.h"




/***************************************************************************
 * Function : RIO_Switch_Print_Version
 *
 * Description: prints version of the model
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Print_Version(
    RIO_HANDLE_T             handle
    )
{
    RIO_SWITCH_DS_T *instanse = (RIO_SWITCH_DS_T*)handle;
    char            buffer[255];

    if ( instanse == NULL || instanse->switch_Callbacks.rio_User_Msg == NULL )
        return RIO_ERROR;

    sprintf( buffer, "RapidIO Switch Model %s\n", RIO_MODEL_VERSION );

    instanse->switch_Callbacks.rio_User_Msg( 
        instanse->switch_Callbacks.rio_Msg_Context,
        buffer
    );

    return RIO_OK;
}


/***************************************************************************
 * Function : RIO_Switch_Set_State_Flag
 *
 * Description: set value for configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Set_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            indicator_Value,
    unsigned int             indicator_Param
    )
{
    RIO_SWITCH_DS_T *instanse = (RIO_SWITCH_DS_T*)handle;
    unsigned int    i;

    if (instanse == NULL || context == NULL )
        return RIO_ERROR;

    /* 
     * find out the link number for the link level block or mid level 
     * block which requested the operation
     */
    /* at first try to find in the link level blocks handles */
    for ( i = 0; i < instanse->inst_Param_Set.link_Number; i++ )
        if ( instanse->link_Handles[i] == context)
            break;

    /* at second try to find in the mid level blocks handles */
    if ( i == instanse->inst_Param_Set.link_Number )
    {
        for ( i = 0; i < instanse->inst_Param_Set.link_Number; i++ )
            if ( instanse->mid_Block_Handles[i] == context)
                break;
        
        /* in case link number for the block was not found return 
           an error code */
        if ( i == instanse->inst_Param_Set.link_Number )
            return RIO_ERROR;
    }

    /* try to set register */
    return RIO_Switch_Set_State( instanse, indicator_ID, indicator_Value, indicator_Param, i );
}


/***************************************************************************
 * Function : RIO_Switch_Get_State_Flag
 *
 * Description: get value of configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Get_State_Flag(
    RIO_HANDLE_T             handle,
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param
    )
{
    RIO_SWITCH_DS_T *instanse = (RIO_SWITCH_DS_T*)handle;
    unsigned int    i;

    if (instanse == NULL || context == NULL || 
        indicator_Value == NULL || indicator_Param == NULL)
        return RIO_ERROR;

    /* 
     * find out the link number for the link level block or mid level 
     * block which requested the operation
     */
    /* at first try to find in the link level blocks handles */
    for ( i = 0; i < instanse->inst_Param_Set.link_Number; i++ )
        if ( instanse->link_Handles[i] == context)
            break;

    /* at second try to find in the mid level blocks handles */
    if ( i == instanse->inst_Param_Set.link_Number )
    {
        for ( i = 0; i < instanse->inst_Param_Set.link_Number; i++ )
            if ( instanse->mid_Block_Handles[i] == context)
                break;
        
        /* in case link number for the block was not found return 
           an error code */
        if ( i == instanse->inst_Param_Set.link_Number )
            return RIO_ERROR;
    }

    /* try to read register */
    return RIO_Switch_Get_State( instanse, indicator_ID, indicator_Value, indicator_Param, i );
}


/***************************************************************************
 * Function : RIO_Switch_Get_Granule
 *
 * Description: wraps link callback 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Get_Granule(
    RIO_CONTEXT_T context,     /* callback context */
    RIO_HANDLE_T  link_Handle, /* instance's handle */
    RIO_GRANULE_T *granule     /* pointer to store granule for heading out */
    )
{
    RIO_SWITCH_DS_T *handle = context; /* points to switch data */
    unsigned int    i;

    if (context == NULL || granule == NULL || 
        link_Handle == NULL || handle->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check if its our link */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
        if (link_Handle == handle->mid_Block_Handles[i])
        {
            /* obtain granule from the transmitter */
            RIO_Switch_Tx_Granule_For_Transmitting(handle, granule, i);
            return RIO_OK;
        }

    /* not found */
    return RIO_ERROR;
}


/***************************************************************************
 * Function : RIO_Switch_Put_Granule
 *
 * Description: wraps link callback 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  link_Handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    )
{
    RIO_SWITCH_DS_T *handle = context; /* points to switch data */
    unsigned int i;

    if (context == NULL || granule == NULL || 
        link_Handle == NULL || handle->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check if its our link */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
        if (link_Handle == handle->mid_Block_Handles[i])
        {
            /* send granule to receiver */
            RIO_Switch_Rx_Granule_Received(handle, granule, i);
            return RIO_OK;
        }

    /* not find */
    return RIO_ERROR;
}


/***************************************************************************
 * Function : RIO_Switch_Error_Msg
 *
 * Description: wraps link error 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Error_Msg(
    RIO_CONTEXT_T  context,
    char *         error_Msg      /* null-terminated error string */
    )
{
    RIO_SWITCH_DS_T *handle = context; /* points to switch data */

    if (handle == NULL || error_Msg == NULL)
        return RIO_ERROR;

    /* write messsage */
    RIO_Switch_Print_Message(handle, RIO_SWITCH_ERROR_1, error_Msg);

    return RIO_OK;
}


/***************************************************************************
 * Function : RIO_Switch_Warning_Msg
 *
 * Description: wraps link warning 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Warning_Msg(
    RIO_CONTEXT_T  context,
    char *         warning_Msg      /* null-terminated warning string */
    )
{
    RIO_SWITCH_DS_T *handle = context; /* points to switch data */

    if (handle == NULL || warning_Msg == NULL)
        return RIO_ERROR;

    /* write messsage */
    RIO_Switch_Print_Message(handle, RIO_SWITCH_WARNING_1, warning_Msg);

    return RIO_OK;
}


/***************************************************************************
 * Function : RIO_Switch_Msg
 *
 * Description: calls pl_Print_Message for 32 bit tx/rx
 *              uses message type to map to one of 32txrx special entries in 
 *              message array
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Msg(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_PL_MSG_TYPE_T msg_Type,
    char          *string
    )
{
    RIO_SWITCH_MESSAGES_T message_Num;
    /* 
     * context here is instanse handle because this function is 
     * invoking as inside pl so from txrx instanse
     */
    RIO_SWITCH_DS_T *handle = (RIO_SWITCH_DS_T*)context;    

    if (handle == NULL || string == NULL)
        return RIO_ERROR;

    switch( msg_Type )
    {
        case RIO_PL_MSG_WARNING_MSG:
            message_Num = RIO_SWITCH_WARNING_1;
            break;
        
        case RIO_PL_MSG_ERROR_MSG:
            message_Num = RIO_SWITCH_ERROR_1;
            break;
            
        case RIO_PL_MSG_NOTE_MSG:
            message_Num = RIO_SWITCH_NOTE_1;
            break;
            
        default:
            return RIO_ERROR;
    }
    
    /* print warning */
    return RIO_Switch_Print_Message(
        handle, 
        message_Num,
        string);

}


/***************************************************************************
 * Function : RIO_Switch_Granule_Received
 *
 * Description: a gag wrapping link callback 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Granule_Received(
    RIO_CONTEXT_T         context,
    RIO_GRANULE_STRUCT_T  *symbol_Struct /* struct, containing symbol's fields */
    )
{
    RIO_SWITCH_DS_T *handle = context; /* points to switch data */
    if (context == NULL || symbol_Struct == NULL || 
        /*link_Handle == NULL || */handle->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    return RIO_ERROR;
}


/***************************************************************************
 * Function : RIO_Switch_Symbol_To_Send
 *
 * Description: a gag wrapping link callback 
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer, /* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size    /* Size always = 4 */     
    )
{
    RIO_SWITCH_DS_T *handle = context; /* points to switch data */
    if (context == NULL || symbol_Buffer == NULL || 
        /*link_Handle == NULL || */handle->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    return RIO_ERROR;
}


/***************************************************************************
 * Function : RIO_Switch_Link_Input_Present
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Link_Input_Present(
    RIO_CONTEXT_T context,      /* callback context */
    RIO_HANDLE_T  handle,       /* instance's handle */
    RIO_BOOL_T    input_Present /* pinter to granule data structure */
    )
{
    unsigned long rec_State;
    unsigned int  i;

    /* instanse handler */
    RIO_SWITCH_DS_T *instanse = (RIO_HANDLE_T)context;

    if (instanse == NULL || handle == NULL)
        return RIO_ERROR;

    /* check if its our link */
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(instanse); i++)
        if (handle == instanse->link_Handles[i])
        {
            RIO_Switch_Read_Register(
                instanse,
                instanse->inst_Param_Set.entry_Extended_Features_Ptr + 
                RIO_LINK_ERROR_A + i * RIO_LPEP_REG_INDEX,
                &rec_State, RIO_TRUE);
            if (instanse->link_Common_Params[i].requires_Window_Alignment == RIO_TRUE)
            {
                /* clear receiver bits*/
                rec_State &= ~RIO_SWITCH_ERROR_STATE_PRESENT;
                /* set stopped due to error bit */
                rec_State |= (input_Present == RIO_TRUE ? RIO_SWITCH_ERROR_STATE_PRESENT : 0x0);
            }
            else 
            {
                rec_State &= ~RIO_SWITCH_ERROR_STATE_PRESENT;
                rec_State &= ~RIO_SWITCH_ERROR_STATE_UNINIT;
                rec_State |= (input_Present == RIO_TRUE ? RIO_SWITCH_ERROR_STATE_PRESENT : 0x0);
                rec_State |= (input_Present == RIO_TRUE ? RIO_SWITCH_ERROR_STATE_OK : 0x0);
            }
            RIO_Switch_Write_Register(
                instanse, 
                RIO_LINK_ERROR_A + i * RIO_LPEP_REG_INDEX +
                instanse->inst_Param_Set.entry_Extended_Features_Ptr,
                rec_State, RIO_TRUE);
            return RIO_OK;
        }
    /* change receiver state in the register */

    return RIO_ERROR;
}


/*******************************************************************************************/

