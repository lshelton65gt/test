#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/switch/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds relocatable object file for switch model
#

include ../../verilog_env/demo/debug.inc

CC              =   gcc
TARGET          =   switch.o
SERIAL_TARGET	=   switch_serial.o
PARALLEL_TARGET	=   switch_parallel.o

FLEXLM_PATH      =   ../../../flexlm

DEBUG		=   -DNDEBUG

FLEXLM_DEFINES  =

DEFINES         =   $(DEBUG) $(FLEXLM_DEFINES) $(_DEBUG)

LIBS            =
LIBS_FLEXLM     =   $(FLEXLM_PATH)/sun4_u5/lm_new.o -L $(FLEXLM_PATH)/sun4_u5 -l lmgr -l crvs -l sb -lsocket

COMMON_INCLUDES		=   -I../interfaces/common -I../pl/include
SERIAL_INCLUDES		=   -I../interfaces/serial_interfaces -I$(FLEXLM_PATH)/machind
PARALLEL_INCLUDES	=   -I../interfaces/parallel_interfaces

SWITCH_INCLUDES		=   -I./include
PCS_PMA_INCLUDES		=   -I../pcs_pma/include
PCS_PMA_ADAPTER_INCLUDES	=   -I../pcs_pma_adapter/include
TXRX_INCLUDES			=   -I../txrx/include
PARALLEL_INIT_INCLUDES		=   -I../parallel_init/include

CFLAGS          =   -c $(prof) $(DEFINES) -elf -ansi -pedantic -Wall -O3
LFLAGS          =   $(prof) -r

COMMON_OBJS		=   rio_switch_model_common.o rio_switch_registers.o rio_switch_errors.o\
			rio_switch_tx.o rio_switch_rx.o
PARALLEL_OBJS		=   rio_switch_parallel_model.o
SERIAL_OBJS		=   rio_switch_serial_model.o
PCS_PMA_OBJS		=   ../pcs_pma/rio_c2char.o ../pcs_pma/rio_char2c.o ../pcs_pma/rio_pcs_pma.o\
			../pcs_pma/rio_pcs_pma_dp.o ../pcs_pma/rio_pcs_pma_dp_cl.o\
			../pcs_pma/rio_pcs_pma_dp_pins_driver.o ../pcs_pma/rio_pcs_pma_dp_sb.o\
			../pcs_pma/rio_pcs_pma_pm.o ../pcs_pma/rio_pcs_pma_pm_align.o\
			../pcs_pma/rio_pcs_pma_pm_init.o ../pcs_pma/rio_pcs_pma_pm_sync.o
PCS_PMA_ADAPTER_OBJS	=   ../pcs_pma_adapter/rio_pcs_pma_adapter.o
TXRX_OBJS		=   ../txrx/rio_32_txrx_model.o
PARALLEL_INIT_OBJS	=   ../parallel_init/rio_parallel_init.o

###############################################################################
# print the usage info
all                 :
	            @echo ""
	            @echo "Insufficient number of arguments."
	            @echo "To build the switch relocatable object file, execute:"
	            @echo "    make switch"
	            @echo "To build the parallel switch relocatable object file, execute:"
	            @echo "    make parallel_switch"
	            @echo "To build the serial switch relocatable object file, execute:"
	            @echo "    make serial_switch"
	            @echo "To clean the switch data, execute:"
	            @echo "    make clean"

# make target including parallel and serial parts
$(TARGET) :	$(PARALLEL_OBJS) $(SERIAL_OBJS) $(COMMON_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS)
	@echo "Building TARGET"
		ld  $(LFLAGS) $(PARALLEL_OBJS) $(SERIAL_OBJS) $(COMMON_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS) $(LIBS) -o $(TARGET)

all_flexlm :	$(PARALLEL_OBJS) $(SERIAL_OBJS) $(COMMON_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS)
		ld  $(LFLAGS) $(PARALLEL_OBJS) $(SERIAL_OBJS) $(COMMON_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS) $(LIBS_FLEXLM) $(LIBS) -o $(TARGET)


# make target including serial parts only
$(SERIAL_TARGET) :	$(COMMON_OBJS) $(SERIAL_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS)
		ld  $(LFLAGS) $(COMMON_OBJS) $(SERIAL_OBJS) $(PCS_PMA_OBJS) $(PCS_PMA_ADAPTER_OBJS) $(LIBS) $(LIBS_FLEXLM) -o $(SERIAL_TARGET)

# make target including parallel parts only
$(PARALLEL_TARGET) :	$(COMMON_OBJS) $(PARALLEL_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS)
		ld  $(LFLAGS) $(COMMON_OBJS) $(PARALLEL_OBJS) $(TXRX_OBJS) $(PARALLEL_INIT_OBJS) $(LIBS) -o $(PARALLEL_TARGET)

#clean data
clean :		
		rm -f $(TARGET)
		rm -f $(SERIAL_TARGET)
		rm -f $(PARALLEL_TARGET)
		rm -f $(COMMON_OBJS)
		rm -f $(TXRX_OBJS)
		rm -f $(PARALLEL_INIT_OBJS)
		rm -f $(PCS_PMA_OBJS)
		rm -f $(PCS_PMA_ADAPTER_OBJS)
		rm -f *.o

#make switch relocatable object including parallel and serial parts
switch :	$(TARGET)
	@echo "$(TARGET) is up-to-date"

#make switch relocatable object including serial parts only
serial_switch :	$(SERIAL_TARGET)

#make switch relocatable object including parallel parts only
parallel_switch :	$(PARALLEL_TARGET)

%.o :		%.c
	@echo "Compiling c file"
		$(CC) $(CFLAGS) $(COMMON_INCLUDES) $(SERIAL_INCLUDES) $(PARALLEL_INCLUDES) $(SWITCH_INCLUDES)\
		$(PCS_PMA_INCLUDES) $(PCS_PMA_ADAPTER_INCLUDES) $(TXRX_INCLUDES) $(PARALLEL_INIT_INCLUDES) -o $*.o $<

###############################################################################
             

