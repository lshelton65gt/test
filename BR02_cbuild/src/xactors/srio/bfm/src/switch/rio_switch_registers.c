/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/switch/rio_switch_registers.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                       to display revision history information.
*
* Description:  switch registers source
*
* Notes:
*
******************************************************************************/

#include <stdio.h>
#include <limits.h>

#include "rio_types.h"
#include "rio_switch_ds.h"
#include "rio_switch_errors.h"
#include "rio_switch_registers.h"
#include "rio_switch_tx.h"

/*
 * init values for registers
 */
#define RIO_DEV_IDS_I     0
#define RIO_DEV_INFO_I    (unsigned long)0x00000004 /*0.4 spec complaince*/
                          
#define RIO_ASS_IDS_I     0
#define RIO_ASS_INFO_I    (unsigned long)0x00000100/*(unsigned long)0x01000000 */
                          
#define RIO_PE_FEAT_I     0
#define RIO_SWITCH_INFO_I 0

/*transport register*/
#define RIO_COMPONENT_TAG_I 0

#define RIO_PORT_MAINT_HEADER_0_I              (unsigned long)0x04000003
#define RIO_PORT_MAINT_HEADER_0_I_SERIAL       (unsigned long)0x04000003

#define RIO_PORT_MAINT_HEADER_1_I (unsigned long)0x00000000

/* LP-EP registers */
#define RIO_LINK_VALID_TO1_I    (unsigned long)0xffffff00
                                
#define RIO_LINK_GEN_CONTROL_I  0 /*reserved*/

#define RIO_LINK_ERROR_I               (unsigned long)0x00000001
/*set input/output port enable bits*/  
#define RIO_LINK_CONTROL_I                        0x44000000/*OR with init values*/
#define RIO_LINK_CONTROL_SERIAL_I                0x00600001

#define RIO_LINK_BUF_STATUS_I               0
#define RIO_LINK_ACKID_STATUS_I               0
#define RIO_LINK_VALID_REQ_I                0 
#define RIO_LINK_VALID_RESP_I                0



/*
 * masks for register
 */
#define RIO_DEV_IDS_M      ULONG_MAX
#define RIO_DEV_INFO_M     (unsigned long)0x0000ffff
                           
#define RIO_ASS_IDS_M      ULONG_MAX
#define RIO_ASS_INFO_M     ULONG_MAX
                           
#define RIO_PE_FEAT_M      (unsigned long)0xf0f8001f
#define RIO_SWITCH_INFO_M  (unsigned long)0x0000ffff



/*#define RIO_PORT_MAINT_HEADER_0_M              ULONG_MAX
#define RIO_PORT_MAINT_HEADER_1_M              0xffff0000
*/

#define RIO_COMPONENT_TAG_M        ULONG_MAX
                                   
#define RIO_PORT_MAINT_HEADER_0_M  ULONG_MAX
#define RIO_PORT_MAINT_HEADER_1_M  0/*0xffff0000*/


/* LP-EP registers */

/*common*/
#define RIO_LINK_VALID_TO1_M       (unsigned long)0xffffff00
                                   
#define RIO_LINK_GEN_CONTROL_M     (unsigned long)0x20000000
                                   
#define RIO_LINK_ERROR_M           (unsigned long)0x001f070f
#define RIO_LINK_ERROR_READ_MASK   (unsigned long)0x000d050b
                                   



#define RIO_LINK_CONTROL_M                  (unsigned long)0xeec00000
#define RIO_LINK_CONTROL_READ_MASK          (unsigned long)0x88c00000
#define RIO_LINK_CONTROL_SERIAL_M           (unsigned long)0xfff80001
#define RIO_LINK_CONTROL_SERIAL_READ_MASK   (unsigned long)0xf89fffff /*pins disabling and error checking disabling is r/o*/

#define RIO_LINK_BUF_STATUS_M               (unsigned long)0x80009f9f    
#define RIO_LINK_ACKID_STATUS_M             (unsigned long)0x1f00ff1f
#define RIO_LINK_VALID_REQ_M                (unsigned long)0x00000007
#define RIO_LINK_VALID_RESP_M               (unsigned long)0x800003ff


/* prototypes*/
static RIO_SWITCH_REG_T *rio_Switch_Find_Register(RIO_SWITCH_DS_T *handle, RIO_CONF_OFFS_T offset);

static void rio_Switch_Set_Register(
    RIO_SWITCH_REG_T   *reg,
    unsigned long  reg_Value,
    unsigned long  reg_Mask,
    unsigned long  reg_Offset,
    RIO_REG_TYPE_T access_Type,
    RIO_REG_KEY_T     key,
    unsigned int      link_Number
    );

/***************************************************************************
 * Function : RIO_Switch_Read_Register
 *
 * Description: read a register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Read_Register(
    RIO_SWITCH_DS_T   *handle,
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     *data,
    RIO_BOOL_T        internal
)
{
    /* pointer to register where we need to read */
    RIO_SWITCH_REG_T *reg;

    if (handle == NULL || data == NULL)
        return RIO_ERROR;

    /*clean data first */
    *data = 0;
    
    /* detect if it's unaligned access trying */
    if (offset & 0x03)
    {
/*        RIO_PL_Print_Error(handle, RIO_PL_ERROR_9, (unsigned long)offset); */
        return RIO_ERROR;
    }

   
    /* find register to read*/
    if ((reg = rio_Switch_Find_Register(handle, offset)) == NULL)
    {
/*        RIO_PL_Print_Error(handle, RIO_PL_ERROR_13, (unsigned long)offset); */
        return RIO_ERROR;
    }


    /*forbidd access to internal regs*/
    if (reg->access_Type == RIO_REG_INTERNAL_ONLY &&
        internal == RIO_FALSE)
    {
     /*   RIO_PL_Print_Message(handle, RIO_PL_ERROR_11, (unsigned long)offset);*/
        return RIO_ERROR;
    }

    /* check if we can read to it*/
    if (reg->access_Type == RIO_REG_WRITE_ONLY &&
        internal == RIO_FALSE)
    {
/*        RIO_PL_Print_Error(handle, RIO_PL_ERROR_14, (unsigned long)offset); */
        return RIO_ERROR;
    }

    /* masked data reserved bits */
    *data = reg->reg_Value & reg->reg_Mask;

    /* check if reading shall perform some actions*/
    if (internal == RIO_TRUE)
    {
    }
    else
    {
        switch (reg->key)
        {
            /* 
             *link validation response we shall clean response_valid bit
             */
           /* case RIO_SWITCH_LINK_VALID_RESP_KEY: 
                reg->reg_Value &= 0x7fffffff;
                break;
            */
            default:
                ;
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_Switch_Write_Register
 *
 * Description: write a register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Write_Register(
    RIO_SWITCH_DS_T       *handle,
    RIO_CONF_OFFS_T   offset,   /* word offset */
    unsigned long     data,
    RIO_BOOL_T        internal
)
{
    /* pointer to register where we need to write */
    RIO_SWITCH_REG_T *reg;
    unsigned int i = 0;
    
    if (handle == NULL)
        return RIO_ERROR;

    /* detect if it's unaligned access trying */
    if (offset & 0x03)
    {
/*        RIO_PL_Print_Error(handle, RIO_PL_ERROR_10, (unsigned long)offset); */
        return RIO_ERROR;
    }

    /* find register to write*/
    if ((reg = rio_Switch_Find_Register(handle, offset)) == NULL)
    {
/*        RIO_PL_Print_Error(handle, RIO_PL_ERROR_11, (unsigned long)offset); */
        return RIO_ERROR;
    }

    /*forbidd access to internal regs*/
    if (reg->access_Type == RIO_REG_INTERNAL_ONLY &&
        internal == RIO_FALSE)
    {
     /*   RIO_PL_Print_Message(handle, RIO_PL_ERROR_11, (unsigned long)offset);*/
        return RIO_ERROR;
    }
    /* check if we can write to it*/
    if (reg->access_Type == RIO_REG_READ_ONLY &&
        internal == RIO_FALSE)
    {
/*        RIO_PL_Print_Error(handle, RIO_PL_ERROR_12, (unsigned long)offset); */
        return RIO_ERROR;
    }

    /* masked data to prevent changing reserved bits */
    /*reg->reg_Value = data & reg->reg_Mask;*/

    if (internal == RIO_TRUE)
    {
        reg->reg_Value = data & reg->reg_Mask;                  
    }
    else
    {
        switch (reg->key)
        {
        case RIO_SWITCH_ERROR_STATUS_REG_KEY:
            reg->reg_Value = reg->reg_Value & ( (~(data) & reg->reg_Mask) | RIO_LINK_ERROR_READ_MASK);
        break;
        case RIO_SWITCH_CONTROL_REG_KEY:
            if (handle->is_Parallel_Model == RIO_TRUE)
            {
                reg->reg_Value =(reg->reg_Value & RIO_LINK_CONTROL_READ_MASK) | 
                    (data & ~(RIO_LINK_CONTROL_READ_MASK));
            }
            else
            {
                reg->reg_Value =(reg->reg_Value & RIO_LINK_CONTROL_SERIAL_READ_MASK) | 
                    (data & ~(RIO_LINK_CONTROL_SERIAL_READ_MASK));
            }
        break;

        default:
            reg->reg_Value = data & reg->reg_Mask;
        }
    }


    /* check if writing shall perform some actions*/
    if (internal == RIO_TRUE)
    {
    }
    else
    {
        /* LINK_REQUEST which shall be headed out */
        /*...*/

        switch (reg->key)
        {
            /* 
             *link validation request shall be passed to the transmitter
             */

            case RIO_SWITCH_TIME_OUT_REG_1_KEY:
                for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
                {
                    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out = reg->reg_Value >> 8;
                }
                break;

/*          case RIO_SWITCH_TIME_OUT_REG_2_KEY:
                RIO_SWITCH_GET_LNK_TX_DATA(handle, reg->link_Number).lresp_Retry_Cnt = reg->reg_Value;
                break;
*/        
            case RIO_SWITCH_CONTROL_REG_KEY:
                /* output/input ports enable bits */
                if (handle->is_Parallel_Model == RIO_TRUE)
                {

                    handle->link_Common_Params[reg->link_Number].only_Maint_Responses = 
                        ((reg->reg_Value & (unsigned long)0x40000000) == 0 ? RIO_TRUE : RIO_FALSE);

                    handle->link_Common_Params[reg->link_Number].only_Maint_Requests = 
                        ((reg->reg_Value & (unsigned long)0x04000000) == 0 ? RIO_TRUE : RIO_FALSE);


                    if (RIO_SWITCH_GET_LNK_TX_DATA(handle, reg->link_Number).pins_Disabled !=
                        ((reg->reg_Value & (unsigned long)0x20000000) == 0 ? RIO_FALSE : RIO_TRUE))
                    {
                        RIO_SWITCH_GET_LNK_TX_DATA(handle, reg->link_Number).pins_Disabled =
                            (reg->reg_Value & (unsigned long)0x20000000) == 0 ? RIO_FALSE : RIO_TRUE;
                        if (handle->internal_Ftray.rio_Link_Tx_Disabling != NULL)
                            handle->internal_Ftray.rio_Link_Tx_Disabling(
                            handle->link_Handles[reg->link_Number],
                            RIO_SWITCH_GET_LNK_TX_DATA(handle, reg->link_Number).pins_Disabled);
                    }

                    if (RIO_SWITCH_GET_LNK_RX_DATA(handle, reg->link_Number).pins_Disabled !=
                        ((reg->reg_Value & (unsigned long)0x02000000) == 0 ? RIO_FALSE : RIO_TRUE))
                    {
                        RIO_SWITCH_GET_LNK_RX_DATA(handle, reg->link_Number).pins_Disabled =
                            (reg->reg_Value & (unsigned long)0x02000000) == 0 ? RIO_FALSE : RIO_TRUE;
                        if (handle->internal_Ftray.rio_Link_Rx_Disabling != NULL)
                            handle->internal_Ftray.rio_Link_Rx_Disabling(
                            handle->link_Handles[reg->link_Number],
                            RIO_SWITCH_GET_LNK_RX_DATA(handle, reg->link_Number).pins_Disabled);
                    }
                }
                else
                {
                  
                    handle->link_Common_Params[reg->link_Number].only_Maint_Responses = 
                        ((reg->reg_Value & (unsigned long)0x00400000) == 0 ? RIO_TRUE : RIO_FALSE);

                    handle->link_Common_Params[reg->link_Number].only_Maint_Requests = 
                        ((reg->reg_Value & (unsigned long)0x00200000) == 0 ? RIO_TRUE : RIO_FALSE);


                }
            break;

                   
            default:
                ;

        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : find_Register
 *
 * Description: return address of register
 *
 * Returns: pointer or NULL
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_SWITCH_REG_T *rio_Switch_Find_Register(RIO_SWITCH_DS_T *handle, RIO_CONF_OFFS_T offset)
{
    /* loop counter*/
    unsigned long i;

    if (handle == NULL)
        return NULL;

    /* look through common register at first */
    switch(offset)
    {
        case RIO_DEV_IDS_A:
            return &handle->reg_Set.common_Regs.device_Ids;

        case RIO_DEV_INFO_A:
            return &handle->reg_Set.common_Regs.device_Info;

        case RIO_ASS_IDS_A:
            return &handle->reg_Set.common_Regs.assembly_Ids;

        case RIO_ASS_INFO_A:
            return &handle->reg_Set.common_Regs.assembly_Info;

        case RIO_PE_FEAT_A:
            return &handle->reg_Set.common_Regs.pe_Features;

        case RIO_SWITCH_INFO_A:
            return &handle->reg_Set.common_Regs.switch_Info;

        case RIO_COMPONENT_TAG_A:
            return &handle->reg_Set.common_Regs.component_Tag;

        default: 
            switch (offset - handle->inst_Param_Set.entry_Extended_Features_Ptr)
            {

                case RIO_PORT_MAINT_HEADER_0_A:
                    return &handle->reg_Set.common_Regs.port_Maint_Header_0;

                case RIO_PORT_MAINT_HEADER_1_A:
                    return &handle->reg_Set.common_Regs.port_Maint_Header_1;

                case  RIO_LINK_VALID_TO1_A:                  
                    return &handle->reg_Set.common_Regs.valid_Time_Out_1;

                case (RIO_LINK_GEN_CONTROL_A):
                    return &handle->reg_Set.common_Regs.port_General_Control;
            }

    }

    /* now look through a set of lpep registers*/
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
/*        if (offset == RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_REQ_A)
        {
            return &handle->reg_Set.lpep_Regs[i].link_Valid_Req;
        }

        if (offset == RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_RESP_A)
        {
            return &handle->reg_Set.lpep_Regs[i].linl_Valid_Resp;
        }

        if (offset == RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_TO1_A)
        {
            return &handle->reg_Set.lpep_Regs[i].valid_Time_Out_1;
        }

        if (offset == RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_TO2_A)
        {
            return &handle->reg_Set.lpep_Regs[i].valid_Time_Out_2;
        }
*/
        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + 
            i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A)
        {
            return &handle->reg_Set.lpep_Regs[i].port_Error_Status;
        }

        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr+ 
            i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A)
        {
            return &handle->reg_Set.lpep_Regs[i].port_Control;
        }

        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_REQ_A)
        {
            return &handle->reg_Set.lpep_Regs[i].link_Valid_Req;
        }

        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_VALID_RESP_A)
        {
            return &handle->reg_Set.lpep_Regs[i].link_Valid_Resp;
        }

        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_ACKID_STATUS_A)
        {
            return &handle->reg_Set.lpep_Regs[i].link_AckID_Status;
        }

        /*this is necessary for buf status defining*/
        if (offset == handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX +
            RIO_LINK_BUF_STATUS_A)
        {
            return &handle->reg_Set.lpep_Regs[i].port_Buf_Status;
        }


 
    }

    return NULL;
}

/***************************************************************************
 * Function : RIO_Switch_Init_Regs
 *
 * Description: intialize all registers
 *
 * Returns: pointer or NULL
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Init_Regs(RIO_SWITCH_DS_T *handle)
{
    /* 
     * here perofrming registers initialization in according with specification
     * some regisers consist from some fields which have to be initializes. 
     * Used constants: mask and offset are unique and can be changed here 
     */
    
    /* loop counter*/
    unsigned int i;
    unsigned long temp; /* temporary */

    if (handle == NULL)
        return RIO_ERROR;

    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.device_Ids,
        ((handle->inst_Param_Set.device_Identity << 16) | 
        (handle->inst_Param_Set.device_Vendor_Identity & 0xffff)) & RIO_DEV_IDS_M, 
        RIO_DEV_IDS_M,
        RIO_DEV_IDS_A,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);

    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.device_Info,
        ((((unsigned long)handle->inst_Param_Set.device_Rev) << 8) |
        (handle->inst_Param_Set.device_Major_Rev << 4) |
        ((handle->inst_Param_Set.device_Minor_Rev & 0x0f) & RIO_DEV_INFO_M) ),
        RIO_DEV_INFO_M,
        RIO_DEV_INFO_A,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);
    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.assembly_Ids,
        ((handle->inst_Param_Set.assy_Identity << 16) |
        (handle->inst_Param_Set.assy_Vendor_Identity & 0x0000ffff)) & RIO_ASS_IDS_M,
        RIO_ASS_IDS_M,
        RIO_ASS_IDS_A,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);
    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.assembly_Info,
        ((handle->inst_Param_Set.assy_Rev << 16 ) & 0xFFFF0000) |
        (handle->inst_Param_Set.entry_Extended_Features_Ptr & 0x0000ffff),/*0x00000100,*/
        RIO_ASS_INFO_M,
        RIO_ASS_INFO_A,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);

    /* make PE features value */
    temp = 0x10000018;

    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.pe_Features,
        temp,
        RIO_PE_FEAT_M,
        RIO_PE_FEAT_A,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);

    if (handle->init_Param_Set.rout_Alg == RIO_TABLE_BASED_ROUTING)
        temp = 0x80000000;
    else if (handle->init_Param_Set.rout_Alg == RIO_SHIFT_BASED_ROUTING)
        temp = 0x40000000;
    else
        temp = 0x20000000;
    
    /* count of LPEP ports */
    temp |= ((handle->inst_Param_Set.link_Number & 0xff) << 8);

    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.switch_Info,
        temp,
        RIO_SWITCH_INFO_M,
        RIO_SWITCH_INFO_A,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);
    
    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.component_Tag,
        RIO_COMPONENT_TAG_I,
        RIO_COMPONENT_TAG_M,
        RIO_COMPONENT_TAG_A,
        RIO_REG_READ_WRITE,
        RIO_SWITCH_ORD_REG_KEY,
        0);


    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.port_Maint_Header_0,
        ((handle->is_Parallel_Model == RIO_TRUE ? RIO_PORT_MAINT_HEADER_0_I :
        RIO_PORT_MAINT_HEADER_0_I_SERIAL) & 0x0000ffff),
        RIO_PORT_MAINT_HEADER_0_M,
        RIO_PORT_MAINT_HEADER_0_A + handle->inst_Param_Set.entry_Extended_Features_Ptr,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);

    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.port_Maint_Header_1,
        RIO_PORT_MAINT_HEADER_1_I,
        RIO_PORT_MAINT_HEADER_1_M,
        RIO_PORT_MAINT_HEADER_1_A + handle->inst_Param_Set.entry_Extended_Features_Ptr,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);

    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.valid_Time_Out_1,
	/* GDA: Support for Bug#131 */
        handle->inst_Param_Set.RIO_Link_Valid_To1_I, /* RIO_LINK_VALID_TO1_I, */
        RIO_LINK_VALID_TO1_M,
        handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_TO1_A,
        RIO_REG_READ_WRITE,
        RIO_SWITCH_TIME_OUT_REG_1_KEY,
        0);


    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.port_General_Control,
        RIO_LINK_GEN_CONTROL_I,
        RIO_LINK_GEN_CONTROL_M,
        handle->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_GEN_CONTROL_A,
        RIO_REG_READ_WRITE,
        RIO_SWITCH_ORD_REG_KEY,
        0);

    /* now look through a set of lpep registers*/
    for (i = 0; i < RIO_SWITCH_GET_PORTS_CNT(handle); i++)
    {
	/* GDA: Support for Bug#131 */
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out = handle->inst_Param_Set.RIO_Link_Valid_To1_I >> 8;	/* RIO_LINK_VALID_TO1_I >> 8; */

      /*       rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Req,
            RIO_LINK_VALID_REQ_I,
            RIO_LINK_VALID_REQ_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_REQ_A,
            RIO_REG_INTERNAL_ONLY,
            RIO_SWITCH_LINK_VALID_REQ_KEY,
            i);
        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].linl_Valid_Resp,
            RIO_LINK_VALID_RESP_I,
            RIO_LINK_VALID_RESP_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            RIO_REG_INTERNAL_ONLY,
            RIO_SWITCH_LINK_VALID_RESP_KEY,
            i);
       
       rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].valid_Time_Out_1,
            RIO_LINK_VALID_TO1_I,
            RIO_LINK_VALID_TO1_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_TO1_A,
            RIO_REG_READ_WRITE,
            RIO_SWITCH_TIME_OUT_REG_1_KEY,
            i);
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out = RIO_LINK_VALID_TO1_I;

        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].valid_Time_Out_2,
            RIO_LINK_VALID_TO2_I,
            RIO_LINK_VALID_TO2_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_TO2_A,
            RIO_REG_READ_WRITE,
            RIO_SWITCH_TIME_OUT_REG_2_KEY,
            i);
         
        RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Retry_Cnt = RIO_LINK_VALID_TO2_I;

        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Error_Status,
            (handle->lp_Length_Params[i] == RIO_TRUE ? (unsigned long)0x00000000 : 
            (unsigned long)0x80000000) | RIO_LINK_ERROR_I,
            RIO_LINK_ERROR_M,
            RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            RIO_REG_READ_ONLY,
            RIO_SWITCH_ERROR_STATUS_REG_KEY,
            i);
*/
        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Error_Status,
            RIO_LINK_ERROR_I,
            RIO_LINK_ERROR_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            RIO_REG_READ_WRITE,
            RIO_SWITCH_ERROR_STATUS_REG_KEY,
            i);



        handle->link_Common_Params[i].only_Maint_Requests = RIO_FALSE;
        handle->link_Common_Params[i].only_Maint_Responses = RIO_FALSE;
        if (handle->is_Parallel_Model == RIO_FALSE)
        {
            rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Control,
                (
                    (handle->link_Common_Params[i].lp_Serial_Is_1x == RIO_TRUE ? (unsigned long)0x00000000 : 
                        (handle->link_Common_Params[i].is_Force_1x_Mode == RIO_FALSE ? (unsigned long)0x50000000 :
                            (
                                handle->link_Common_Params[i].is_Force_1x_Mode_Lane_0 == RIO_TRUE ? (unsigned long)0x42000000 :
                                    (unsigned long)0x4B000000
                            )
                        )
                    ) |
                    RIO_LINK_CONTROL_SERIAL_I
                ),
                RIO_LINK_CONTROL_SERIAL_M,
                handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
                RIO_REG_READ_WRITE,
                RIO_SWITCH_CONTROL_REG_KEY,
                i);
        }
        else
        {

            rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Control,
                (handle->link_Common_Params[i].lp_Ep_Is_8  == RIO_TRUE ? (unsigned long)0x00000000 : 
                (unsigned long)0x88000000) | RIO_LINK_CONTROL_I,
                RIO_LINK_CONTROL_M,
                handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
                RIO_REG_READ_WRITE,
                RIO_SWITCH_CONTROL_REG_KEY,
                i);
        }

        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Buf_Status,
            (RIO_LINK_BUF_STATUS_I | ((handle->link_Common_Params[i].transmit_Flow_Control_Support & 0x1) << 31) |
            1),
            RIO_LINK_BUF_STATUS_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            RIO_REG_INTERNAL_ONLY,
            RIO_SWITCH_BUF_STATUS_REG_KEY,
            i);


        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].link_AckID_Status,
            RIO_LINK_ACKID_STATUS_I,
            RIO_LINK_ACKID_STATUS_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            RIO_REG_INTERNAL_ONLY, 
            RIO_SWITCH_ACKID_STATUS_REG_KEY,
            i);

        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Req,
            RIO_LINK_VALID_REQ_I,
            RIO_LINK_VALID_REQ_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_REQ_A,
            RIO_REG_INTERNAL_ONLY, 
            RIO_SWITCH_LINK_VALID_REQ_KEY,
            i);

        rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Resp,
            RIO_LINK_VALID_RESP_I,
            RIO_LINK_VALID_RESP_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            RIO_REG_INTERNAL_ONLY,
            RIO_SWITCH_LINK_VALID_RESP_KEY,
            i);
        /*temp = RIO_LPEP_REG_BASE + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A + 4;
        temp -= RIO_PORT_MAINT_HEADER_0_A;
        temp &= 0x0000ffff;
        temp <<= 16;*/
    }
/*
    rio_Switch_Set_Register(&handle->reg_Set.common_Regs.port_Maint_Header_1,
        temp,
        RIO_PORT_MAINT_HEADER_1_M,
        RIO_PORT_MAINT_HEADER_1_A,
        RIO_REG_READ_ONLY,
        RIO_SWITCH_ORD_REG_KEY,
        0);
*/
    return RIO_OK;
}

/***************************************************************************
 * Function : set_Register
 *
 * Description: seta register
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void rio_Switch_Set_Register(
    RIO_SWITCH_REG_T   *reg,
    unsigned long  reg_Value,
    unsigned long  reg_Mask,
    unsigned long  reg_Offset,
    RIO_REG_TYPE_T access_Type,
    RIO_REG_KEY_T     key,
    unsigned int      link_Number
    )
{
    reg->access_Type = access_Type;
    reg->reg_Mask = reg_Mask;
    reg->reg_Offset = reg_Offset;
    reg->reg_Value = reg_Value;
    reg->key = key;
    reg->link_Number = link_Number;
}

/***************************************************************************
 * Function : RIO_Switch_Conv_Size_To_Dw_Cnt
 *
 * Description: return count of DW which can be issue in request packet
 *
 * Returns: count
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_Switch_Conv_Size_To_Dw_Cnt(unsigned int size, unsigned int wdptr )
{
    /*
     * RIO size encoding to count of double-words are performing here
     * in according to the specification
     */
    switch (size)
    {
        /* subdouble-word sizes - one DW*/
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 6:
        case 8:
        case 9:
        case 10:
            return 0x01;

        case 11:
            if (wdptr == 0)
                return 0x01;
            else if (wdptr ==1)
                return 0x02;

        case 12:
            if (wdptr == 0)
                return 0x04;
            else if (wdptr ==1)
                return 0x08;
        case 13:
            if (wdptr == 0)
                return 0x0C;
            else if (wdptr ==1)
                return 0x10;

        case 14:
            if (wdptr == 0)
                return 0x14;
            else if (wdptr ==1)
                return 0x18;
                
        case 15:
            if (wdptr == 0)
                return 0x1C;
            else if (wdptr ==1)
                return 0x20;

            /* reserved value */
        default:
            return 0x00;
    }
}

/***************************************************************************
 * Function : RIO_Switch_Get_State
 *
 * Description: read a state
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Get_State(
    RIO_SWITCH_DS_T          *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Param,
    unsigned int             link_Number
    )
{
    /* pointer to register where we need to read */
    unsigned long data;
    unsigned int i;

    if (handle == NULL || indicator_Value == NULL || indicator_Param == NULL)
        return RIO_ERROR;

    if (indicator_ID >= RIO_STATE_INDICATORS_NUMBER)
        return RIO_ERROR;

    if (link_Number  >= RIO_SWITCH_MAX_LINK_CNT)
        return RIO_ERROR;

    i = link_Number;

    
    /*clean data first */
  /*  *indicator_Value = handle->state_Indicators[indicator_ID].indicator_Value;
    *indicator_Parameter = handle->state_Indicators[indicator_ID].indicator_Parameter;
    */
    
    
    switch (indicator_ID)
    {
        case RIO_OUR_BUF_STATUS:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = data & 0x0000001f;
        
        break;
        case RIO_PARTNER_BUF_STATUS:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (data & 0x0001f00) >> 8;

        break;
        case RIO_LRSP_ACKID_STATUS:
        
        /*let lrsp register be like in serial specification*/

     /*   RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 5) & 0x1f;*/
        
        break;
        case RIO_INBOUND_ACKID:
            
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 24) & 0xff; /*0-7 bit*/
    
        break;
        case RIO_OUTBOUND_ACKID:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 0) & 0xff;/*24-31 bit*/
    
        break;
        case RIO_OUTSTANDING_ACKID:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        *indicator_Value = (data >> 8) & 0xff;/*16-23 bit*/
        break;

        case RIO_TX_DATA_IN_PROGRESS:

/*
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);
        
        *indicator_Value = (data & 0x0000080) >> 7;
*/
        /* TBD: let Masha check that this is correct!!! */
        *indicator_Value = handle->link_Data[link_Number].tx_Data.in_Progress;

        break;
                                                                                                                                    
        case RIO_RX_DATA_IN_PROGRESS:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (data & 0x00008000) >> 15;

        break;

        case RIO_FLOW_CONTROL_MODE:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (data & 0x80000000) >> 31;
        break;



/*      case RIO_UNINITIALIZED: 

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)(data & 0x1);
        
        break;
*/
        case RIO_INITIALIZED: /*bit 30 in 0x58 - 1, bit 31 - 0*/
            RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

    
        *indicator_Value = (RIO_BOOL_T)((data & 0x2) >> 1) ;

        break;

        case RIO_UNREC_ERROR:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)((data & 0x4) >> 2) ;

        break;

        case RIO_OUTPUT_ERROR:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)((data >> 16) & 0x1) ;
        *indicator_Param = handle->state_Indicators_Params[i].output_Error_Type;
        
        break;

        case RIO_INPUT_ERROR:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        *indicator_Value = (RIO_BOOL_T)((data >> 8) & 0x1);
        *indicator_Param = handle->state_Indicators_Params[i].input_Error_Type;

        break;

        /****************************************************************************/

        case RIO_PORT_WIDTH:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ( (data & 0x1) == 0x1) /*serial link*/
        {
            *indicator_Value =  (data >> 30) & 0x3; /*0,1 bit*/
        }
        else /*parallel link*/
        {
            *indicator_Value =  (data >> 31) & 0x1; /*0 bit- output*/
        }

        break;

        case RIO_INITIALIZED_PORT_WIDTH:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ( (data & 0x1) == 0x1) /*serial link*/
        {
            *indicator_Value =  (data >> 27) & 0x7; /*2-4 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        break;

        case RIO_PORT_WIDTH_OVERRIDE:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ( (data & 0x1) == 0x1) /*serial link*/
        {
            *indicator_Value =  (data >> 24) & 0x7; /*5-7 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        break;

        /* for specific internal (intermodule) needs: */
        case RIO_LANE_WITH_INVALID_CHARACTER:  /* number of the lane where invalid character occured */
            *indicator_Value = handle->state_Indicators_Params[i].lane_With_Invalid_Character;
        break;

        case RIO_IS_PORT_TRUE_4X: /* RIO_TRUE if port is working in 4x mode */
            *indicator_Value = handle->state_Indicators_Params[i].is_Port_Mode_4x;
        break;

        case RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE: /* in case of 1x mode it is a number of the current byte in granule */
            *indicator_Value = handle->state_Indicators_Params[i].byte_In_Rcv_Granule_Cur_Num;
        break;

        case RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE: /* in case of 1x mode it is a number of the current byte in granule */
            *indicator_Value = handle->state_Indicators_Params[i].byte_In_Trx_Granule_Cur_Num;
        break;

        case RIO_ARE_ALL_IDLES_IN_GRANULE:
            *indicator_Value = handle->state_Indicators_Params[i].are_All_Idles_In_Granule;
        break;

        case RIO_SYMBOL_IS_PD: 
            *indicator_Value = handle->state_Indicators_Params[i].is_Packet_Delimiter;
        break;
    
        case RIO_IS_ALIGNMENT_LOST:
            *indicator_Value = handle->state_Indicators_Params[i].is_Align_Lost;
        break;

        case RIO_IS_SYNC_LOST:
            *indicator_Value = handle->state_Indicators_Params[i].is_Sync_Lost;
        break;

        case RIO_IS_CHARACTER_INVALID:
            *indicator_Value = handle->state_Indicators_Params[i].is_Character_Invalid;
        break;

        case RIO_COPY_TO_LANE2:
            *indicator_Value = handle->state_Indicators_Params[i].copy_To_Lane2;
        break;

        case RIO_SUPPRESS_HOOK:
            *indicator_Value = handle->state_Indicators_Params[i].suppress_Hook;
        break;

        case RIO_INITIAL_RUN_DISP:
            *indicator_Value = handle->state_Indicators_Params[i].initial_Run_Disp;
        break;

        case RIO_DECODE_ANY_RUN_DISP:
            *indicator_Value = handle->state_Indicators_Params[i].decode_Any_Run_Disp;
        break;

        case RIO_LANE2_JUST_FORCED_BY_DISCOVERY:
            *indicator_Value = handle->state_Indicators_Params[i].lane2_Just_Forced_By_Discovery;
        break;

        case RIO_RX_IS_PD:
            *indicator_Value = handle->state_Indicators_Params[i].rx_Packet_Is_Ongoing;
            break;


        /****************************************************************************/

        default:
        break;
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_Switch_Set_State
 *
 * Description: read a state
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Set_State(
    RIO_SWITCH_DS_T          *handle,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Param,
    unsigned int             link_Number
    )
{
    /* pointer to register where we need to read */
    unsigned long     data;
    unsigned int      i;

    if (handle == NULL)
        return RIO_ERROR;

    if (indicator_ID >= RIO_STATE_INDICATORS_NUMBER)
        return RIO_ERROR;

   if (link_Number  >= RIO_SWITCH_MAX_LINK_CNT)
        return RIO_ERROR;

    i = link_Number;


        
    switch (indicator_ID)
    {
        case RIO_OUR_BUF_STATUS:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        if (handle->is_Parallel_Model == RIO_TRUE && indicator_Value >= 0xf)
            data = (data & 0xffffffe0) | (0xe);
        else if (handle->is_Parallel_Model == RIO_FALSE && indicator_Value >= 0x1f)
            data = (data & 0xffffffe0) | (0x1e);
        else
            data = (data & 0xffffffe0) | (indicator_Value & 0x0000001f);

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;
        case RIO_PARTNER_BUF_STATUS:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        data = (data & 0xffffe0ff) | ((indicator_Value << 8) & 0x00001f00);

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;
        case RIO_LRSP_ACKID_STATUS:
        
        /*let lrsp register be like in serial specification*/

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            &data,
            RIO_TRUE);
    
        data &= 0xfffffc1f;
        data |= ((indicator_Value & 0x1f) << 5); /*22-26 bit*/
    
        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            data,
            RIO_TRUE);

        break;
        case RIO_INBOUND_ACKID:
            
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        data &= 0x00ffffff;
        data |= ((indicator_Value & 0xff) << 24); /*0-7 bit*/
    
        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            data,
            RIO_TRUE);
        break;
        case RIO_OUTBOUND_ACKID:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        data &= 0xffffff00;
        data |= ((indicator_Value & 0xff) << 0); /*24-31 bit*/
    
        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            data,
            RIO_TRUE);
        break;
        case RIO_OUTSTANDING_ACKID:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            &data,
            RIO_TRUE);
    
        data &= 0xffff00ff;
        data |= ((indicator_Value & 0xff) << 8); /*16-23 bit*/
    
        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            data,
            RIO_TRUE);
        break;
        case RIO_TX_DATA_IN_PROGRESS:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);
        
        data = (data & 0xffffff7f) | ((indicator_Value  & 0x1) << 7);
        

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);
        
        break;
                                                                                                                                    
        case RIO_RX_DATA_IN_PROGRESS:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        data = (data & 0xffff7fff) | ((indicator_Value  & 0x1) << 15);

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;

        case RIO_FLOW_CONTROL_MODE:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            &data,
            RIO_TRUE);

        data = (data & 0x7fffffff) | ((indicator_Value  & 0x1) << 31);

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            data,
            RIO_TRUE);

        break;

/*      case RIO_UNINITIALIZED: 

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        
        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffffe) | 0x1;
        }
        else 
        {
            data = (data & 0xfffffffe);
        }

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);
        

        break;
*/

        case RIO_INITIALIZED: /*bit 30, 31 in 0x58*/
            RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        /*set OK or unitialized bit*/
        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffffc) | 0x2;
        }
        else 
        {
            data = (data & 0xfffffffc) | 0x1;
        }

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        break;

        case RIO_UNREC_ERROR:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffffb) | 0x4;
        }
        else 
        {
            data = (data & 0xfffffffb);
        }

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        break;

        case RIO_OUTPUT_ERROR:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffcffff) | 0x00030000;
        }
        else 
        {
            data = (data & 0xfffeffff);
        }

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        handle->state_Indicators_Params[i].output_Error_Type = indicator_Param;

        break;

        case RIO_INPUT_ERROR:
        
        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            &data,
            RIO_TRUE);

        if (indicator_Value == RIO_TRUE)
        {
            data = (data & 0xfffffcff) | 0x00000300;
        }
        else 
        {
            data = (data & 0xfffffeff);
        }

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            data,
            RIO_TRUE);

        handle->state_Indicators_Params[i].input_Error_Type = indicator_Param;

        break;

        /****************************************************************************/

        case RIO_PORT_WIDTH:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ( (data & 0x1) == 0x1) /*serial link*/
        {
            data &= 0x3fffffff;
            data |= ((indicator_Value & 0x3) << 30); /*0,1 bit*/
        }
        else /*parallel link*/
        {
            data &= 0x77ffffff;
            data |= ((indicator_Value & 0x1) << 31); /*0 - bit - output*/
            data |= ((indicator_Value & 0x1) << 27); /*4 - bit - input*/

        }
        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            data,
            RIO_TRUE);

        break;

        case RIO_INITIALIZED_PORT_WIDTH:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ( (data & 0x1) == 0x1) /*serial link*/
        {
            data &= 0xc7ffffff;
            data |= ((indicator_Value & 0x7) << 27); /*2-4 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            data,
            RIO_TRUE);
        break;

        case RIO_PORT_WIDTH_OVERRIDE:

        RIO_Switch_Read_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            &data,
            RIO_TRUE);

        if ( (data & 0x1) == 0x1) /*serial link*/
        {
            data &= 0xf8ffffff;
            data |= ((indicator_Value & 0x7) << 24); /*5-7 bit*/
        }
        else /*parallel link*/
        {
            /*none*/
        }

        RIO_Switch_Write_Register(handle,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
            data,
            RIO_TRUE);
        break;

        /* for specific internal (intermodule) needs: */
        case RIO_LANE_WITH_INVALID_CHARACTER:  /* number of the lane where invalid character occured */
            handle->state_Indicators_Params[i].lane_With_Invalid_Character = indicator_Value;
        break;

        case RIO_IS_PORT_TRUE_4X: /* RIO_TRUE if port is working in 4x mode */
            handle->state_Indicators_Params[i].is_Port_Mode_4x = (RIO_BOOL_T)indicator_Value;
        break;

        case RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE: /* in case of 1x mode it is a number of the current byte in granule */
            handle->state_Indicators_Params[i].byte_In_Rcv_Granule_Cur_Num = indicator_Value;
            break;
        case RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE:
            handle->state_Indicators_Params[i].byte_In_Trx_Granule_Cur_Num = indicator_Value;
        break;

        case RIO_ARE_ALL_IDLES_IN_GRANULE:
            handle->state_Indicators_Params[i].are_All_Idles_In_Granule = indicator_Value;
        break;

        case RIO_SYMBOL_IS_PD:
            handle->state_Indicators_Params[i].is_Packet_Delimiter = indicator_Value;
        break;
    
        case RIO_IS_ALIGNMENT_LOST:
            handle->state_Indicators_Params[i].is_Align_Lost = indicator_Value;
        break;

        case RIO_IS_SYNC_LOST:
            handle->state_Indicators_Params[i].is_Sync_Lost = indicator_Value;
        break;

        case RIO_IS_CHARACTER_INVALID:
            handle->state_Indicators_Params[i].is_Character_Invalid = indicator_Value;
        break;

        case RIO_COPY_TO_LANE2:
            handle->state_Indicators_Params[i].copy_To_Lane2 = indicator_Value;
        break;

        case RIO_SUPPRESS_HOOK:
            handle->state_Indicators_Params[i].suppress_Hook = indicator_Value;
        break;

        case RIO_INITIAL_RUN_DISP:
            handle->state_Indicators_Params[i].initial_Run_Disp = (unsigned char)indicator_Value;
        break;

        case RIO_DECODE_ANY_RUN_DISP:
            handle->state_Indicators_Params[i].decode_Any_Run_Disp = indicator_Value;
        break;

        case RIO_LANE2_JUST_FORCED_BY_DISCOVERY:
            handle->state_Indicators_Params[i].lane2_Just_Forced_By_Discovery = indicator_Value;
        break;

        case RIO_RX_IS_PD:
            handle->state_Indicators_Params[i].rx_Packet_Is_Ongoing = (RIO_BOOL_T)indicator_Value;
            break;

        /****************************************************************************/

        default:
        break;
    }
    
   

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_Switch_Init_Regs
 *
 * Description: intialize all registers
 *
 * Returns: pointer or NULL
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Switch_Link_Init_Regs(RIO_SWITCH_DS_T *handle, unsigned int link_Number)
{
    /* 
     * here perofrming registers initialization in according with specification
     * some regisers consist from some fields which have to be initializes. 
     * Used constants: mask and offset are unique and can be changed here 
     */
    
    /* loop counter*/
    unsigned int i;

    if (handle == NULL)
        return RIO_ERROR;

    i = link_Number;

    /* now look through a set of lpep registers*/
    /* GDA: Support for Bug#131 */ 
    RIO_SWITCH_GET_LNK_TX_DATA(handle, i).lresp_Time_Out = handle->inst_Param_Set.RIO_Link_Valid_To1_I >> 8;								 /* RIO_LINK_VALID_TO1_I >> 8; */

    
    rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Error_Status,
            RIO_LINK_ERROR_I,
            RIO_LINK_ERROR_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ERROR_A,
            RIO_REG_READ_WRITE,
            RIO_SWITCH_ERROR_STATUS_REG_KEY,
            i);


    if (handle->is_Parallel_Model == RIO_FALSE)
    {
            rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Control,
                (
                    (handle->link_Common_Params[i].lp_Serial_Is_1x == RIO_TRUE ? (unsigned long)0x00000000 : 
                        (handle->link_Common_Params[i].is_Force_1x_Mode == RIO_FALSE ? (unsigned long)0x50000000 :
                            (
                                handle->link_Common_Params[i].is_Force_1x_Mode_Lane_0 == RIO_TRUE ? (unsigned long)0x42000000 :
                                    (unsigned long)0x4B000000
                            )
                        )
                    ) |
                    (unsigned long)0x00400000|
                    (unsigned long)0x00200000|
                    RIO_LINK_CONTROL_SERIAL_I
                ),
                RIO_LINK_CONTROL_SERIAL_M,
                handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
                RIO_REG_READ_WRITE,
                RIO_SWITCH_CONTROL_REG_KEY,
                i);
    }
    else
    {

            rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Control,
                (handle->link_Common_Params[i].lp_Ep_Is_8  == RIO_TRUE ? (unsigned long)0x00000000 : 
                (unsigned long)0x88000000) | RIO_LINK_CONTROL_I,
                RIO_LINK_CONTROL_M,
                handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_CONTROL_A,
                RIO_REG_READ_WRITE,
                RIO_SWITCH_CONTROL_REG_KEY,
                i);
    }

    rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].port_Buf_Status,
            (RIO_LINK_BUF_STATUS_I | ((handle->link_Common_Params[i].transmit_Flow_Control_Support & 0x1) << 31) |
            1),
            RIO_LINK_BUF_STATUS_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_BUF_STATUS_A,
            RIO_REG_INTERNAL_ONLY,
            RIO_SWITCH_BUF_STATUS_REG_KEY,
            i);


    rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].link_AckID_Status,
            RIO_LINK_ACKID_STATUS_I,
            RIO_LINK_ACKID_STATUS_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_ACKID_STATUS_A,
            RIO_REG_INTERNAL_ONLY, 
            RIO_SWITCH_ACKID_STATUS_REG_KEY,
            i);

    rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Req,
            RIO_LINK_VALID_REQ_I,
            RIO_LINK_VALID_REQ_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_REQ_A,
            RIO_REG_INTERNAL_ONLY, 
            RIO_SWITCH_LINK_VALID_REQ_KEY,
            i);

    rio_Switch_Set_Register(&handle->reg_Set.lpep_Regs[i].link_Valid_Resp,
            RIO_LINK_VALID_RESP_I,
            RIO_LINK_VALID_RESP_M,
            handle->inst_Param_Set.entry_Extended_Features_Ptr + i * RIO_LPEP_REG_INDEX + RIO_LINK_VALID_RESP_A,
            RIO_REG_INTERNAL_ONLY,
            RIO_SWITCH_LINK_VALID_RESP_KEY,
            i);

        /*init the indicators*/
    handle->state_Indicators_Params[link_Number].is_Packet_Delimiter = RIO_FALSE;
    handle->state_Indicators_Params[link_Number].is_Align_Lost = RIO_FALSE;
    handle->state_Indicators_Params[link_Number].is_Sync_Lost = RIO_FALSE;
    handle->state_Indicators_Params[link_Number].is_Character_Invalid = RIO_FALSE;
    handle->state_Indicators_Params[link_Number].copy_To_Lane2 = RIO_FALSE;
    handle->state_Indicators_Params[link_Number].suppress_Hook =  RIO_FALSE;
    handle->state_Indicators_Params[link_Number].initial_Run_Disp = 2; /* do not change Running Disparity */
    handle->state_Indicators_Params[link_Number].decode_Any_Run_Disp = 0;
    handle->state_Indicators_Params[link_Number].lane2_Just_Forced_By_Discovery = RIO_FALSE;


    return RIO_OK;
}

/********************************************************************/

