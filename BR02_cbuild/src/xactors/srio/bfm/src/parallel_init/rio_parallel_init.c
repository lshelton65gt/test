/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/parallel_init/rio_parallel_init.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  Physical layer transmitter
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include <stdarg.h>

#include "rio_parallel_init.h"
/*
 * Global variable for counting number of 32-Bit Transmitter/Receiver 
 * instantiations. Using for proper instance number assignement
 */
static unsigned long rio_Pl_Par_Init_Block_Inst_Count = 0;

/*messages processing*/
typedef struct {
    char *string;
    int count_Of_Add_Args;
} INIT_BLOCK_ERROR_WARNING_ENTRY_T;

typedef enum {
    INIT_BLOCK_ERROR_1 = 0,
    INIT_BLOCK_ERROR_2, 
    INIT_BLOCK_ERROR_3 
} INIT_BLOCK_MESSAGES_T;

/*
 * these errors and warning entries are used to form competed error/warning
 * message strings, which are passed up to the physical layer or to the 
 * switch model. There additional context is added to them
 */
static INIT_BLOCK_ERROR_WARNING_ENTRY_T init_Block_Message_Strings[] = {
{"(the init block, instanse %lu).Unexpected training received.\n", 0},

{"(the init block, instanse %lu).Unrecoverable error detected.\n", 0},

{"(the init block, instanse %lu).Received training embedded or canceling the packet.\n", 0}

};

static const int init_Block_Message_Strings_Cnt = sizeof(init_Block_Message_Strings) / sizeof(init_Block_Message_Strings[0]);

static RIO_PL_MSG_TYPE_T init_Block_Message_To_Type_Map[] = {
    RIO_PL_MSG_WARNING_MSG,
    RIO_PL_MSG_ERROR_MSG,
    RIO_PL_MSG_ERROR_MSG
};

static const int init_Block_Messsage_Map_Count = sizeof(init_Block_Message_To_Type_Map) / sizeof(init_Block_Message_To_Type_Map[0]);

/*messages processing end*/


/*
 * this type represents transmitter training machine state
 */
typedef enum {
    RIO_PL_TX_RESET = 0,
    RIO_PL_TX_SEND_TRN_REQ,
    RIO_PL_TX_SEND_TRN_PTTN,
    RIO_PL_TX_SEND_IDLES_REQ,
    RIO_PL_TX_OK,
    RIO_PL_TX_OK_SEND_TRN_REQ,
    RIO_PL_TX_OK_PRE_SEND_TRN_PTTN
} RIO_PL_TX_TRAIN_MACHINE_STATE_T;

/*
 * this type represents receiver training machine state
 */
typedef enum {
    RIO_PL_RX_RESET = 0,
    RIO_PL_RX_WAIT_GOOD_PTTN,
    RIO_PL_RX_WAIT_FOR_IDLE,
    RIO_PL_RX_OK,
    RIO_PL_RX_PRE_WAIT_FOR_IDLE,
    RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK,
    RIO_PL_RX_RECEIVE_TRAIN_SEQ_OK
} RIO_PL_RX_TRAIN_MACHINE_STATE_T;

typedef struct {
    unsigned int train_State;
    unsigned int train_Counter;
    unsigned int time_Out_Counter;
    RIO_BOOL_T got_2I_Non_T_Lrqst; /*the parameter indicates if the attached device
                                    have started to operate (more than 1 idles, non-training, non-LRQST received)*/
} RIO_PL_TX_RX_INIT_DS_T;

typedef struct {
    unsigned long                           instance_Number;
    RIO_BOOL_T                                is_On;
    RIO_PL_TX_RX_INIT_DS_T                  tx_Data;
    RIO_PL_TX_RX_INIT_DS_T                  rx_Data;
    
    RIO_PL_PAR_INIT_BLOCK_CALLBACK_TRAY_T   interfaces;

    RIO_PL_PAR_INIT_BLOCK_INITIALIZE        restart_Function;
    RIO_PL_PAR_INIT_BLOCK_START_RESET       reset_Function;

    RIO_PAR_INIT_BLOCK_PARAM_SET_T            parameters_Set;
    
    RIO_BOOL_T                              in_Reset;
} RIO_PL_PAR_INIT_BLOCK_DS_T;


static int pl_Par_Init_Block_Get_Granule(
    RIO_CONTEXT_T context,   /* my handle */
    RIO_HANDLE_T  handle,    /* invokater's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    );

static int pl_Par_Init_Block_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    );

static int pl_Par_Init_Block_Initialize(
    RIO_HANDLE_T         handle,
    RIO_PAR_INIT_BLOCK_PARAM_SET_T   *param_Set
    );

static int pl_Par_Init_Block_Start_Reset(
    RIO_HANDLE_T handle
    );

static int pl_Par_Init_Block_Delete_Instance(
    RIO_HANDLE_T          handle
    );

static void pl_Par_Init_Block_Granule_For_Transmitting(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );

static void pl_Par_Init_Block_Granule_Received(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );

static int pl_Par_Init_Block_Clock_Edge(
    RIO_HANDLE_T handle
    );

static void form_Idle(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *idle);
static void form_LRQST(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule);
static void form_Training(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule);
void count_LRQST_Time_Out(RIO_PL_PAR_INIT_BLOCK_DS_T *handle);

static void handler_Train_Wait_Good_Pttn_State(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );

static void handler_Train_Wait_For_Idle_State(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );

static void handler_Train_Receive_Train_Seq_OK_State(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );

static void handler_Train_Rx_OK_State(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );

static void handler_Train_Pre_Receive_Train_Seq_OK_State(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );

static void handler_Train_Pre_Receive_Wait_For_Idle_State(
    RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
    RIO_GRANULE_T *granule
    );


static void set_Tx_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, unsigned int state);
static void set_Rx_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, unsigned int state);

static int pl_Par_Init_Block_Machine_Management(
    RIO_HANDLE_T    handle,
    RIO_BOOL_T      machine_Is_On
    );

static int pl_Par_Init_Block_Machine_Change_State(
    RIO_HANDLE_T                    handle,
    RIO_PAR_INIT_BLOCK_COMMANDS_T   command /*the new state of machine*/
    );

static int print_Message(RIO_PL_PAR_INIT_BLOCK_DS_T* handle, INIT_BLOCK_MESSAGES_T message_Type, ...);
/***************************************************************************
 * Function : RIO_PL_Model_Create_Instance
 *
 * Description: Create new physical layer instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_PL_Par_Init_Block_Create_Instance(
    RIO_HANDLE_T            *handle
    )
{
    /* handle to structure will allocated*/
    RIO_PL_PAR_INIT_BLOCK_DS_T *pl_Par_Init_Block_Instanse = NULL;

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for physical layer 
     * data structure
     */
    if (handle == NULL )
        return RIO_ERROR;

    /* clear handle */
    *handle = NULL;

    /* allocate instanse */
    if ((pl_Par_Init_Block_Instanse = malloc(sizeof(RIO_PL_PAR_INIT_BLOCK_DS_T))) == NULL)
        return RIO_ERROR;

    /*
     * clear all pointers
     */
    pl_Par_Init_Block_Instanse->interfaces.rio_Get_State_Flag = NULL;
    pl_Par_Init_Block_Instanse->interfaces.rio_Link_Get_Granule = NULL;
    pl_Par_Init_Block_Instanse->interfaces.rio_Link_Put_Granule = NULL;
    pl_Par_Init_Block_Instanse->interfaces.rio_Msg = NULL;
    pl_Par_Init_Block_Instanse->interfaces.rio_PL_Interface_Context = NULL;
    pl_Par_Init_Block_Instanse->interfaces.rio_Regs_Context = NULL;
    pl_Par_Init_Block_Instanse->interfaces.rio_Set_State_Flag = NULL;
    pl_Par_Init_Block_Instanse->interfaces.rio_User_Msg = NULL;

    /*
     * increase count of TxRx instances and set
     * up the number of instance, turn link into reset
     */
    *handle = pl_Par_Init_Block_Instanse;
    pl_Par_Init_Block_Instanse->instance_Number = ++rio_Pl_Par_Init_Block_Inst_Count;
    pl_Par_Init_Block_Instanse->in_Reset = RIO_TRUE; 

    return RIO_OK;

}

/***************************************************************************
 * Function : RIO_PL_Par_Init_Block_Get_Function_Tray
 *
 * Description: Export function tray, set pointers to corresponding 
 *              functions entry point
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Par_Init_Block_Get_Function_Tray(
    RIO_PL_PAR_INIT_BLOCK_FTRAY_T *ftray             
    )
{
    /*
     * check pointers to not NULL values and complete function tray
     * with valid entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

 /*   ftray->rio_PL_Par_Init_Block_Delete_Instance = ;*/
    ftray->rio_PL_Par_Init_Block_Initialize = pl_Par_Init_Block_Initialize;
    ftray->rio_PL_Par_Init_Block_Start_Reset = pl_Par_Init_Block_Start_Reset;

    ftray->rio_PL_Par_Init_Block_Get_Granule = pl_Par_Init_Block_Get_Granule;
    ftray->rio_PL_Par_Init_Block_Put_Granule = pl_Par_Init_Block_Put_Granule;

    ftray->rio_PL_Par_Init_Block_Delete_Instance = pl_Par_Init_Block_Delete_Instance;
    ftray->rio_PL_Par_Init_Block_Clock_Edge = pl_Par_Init_Block_Clock_Edge;
    ftray->rio_PL_Par_Init_Block_Machine_Management = pl_Par_Init_Block_Machine_Management;
    ftray->rio_PL_Par_Init_Block_Machine_Change_State = pl_Par_Init_Block_Machine_Change_State;

    
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Par_Init_Block_Bind_Instance
 *
 * Description: bind the instance with the environment, load entry point from
 *              callback tray to internal structure
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Par_Init_Block_Bind_Instance(
    RIO_HANDLE_T                        handle,
    RIO_PL_PAR_INIT_BLOCK_CALLBACK_TRAY_T   *ctray   
    )
{
    /* 
     * convert void pointer to pointer to instance structure and 
     * check all callbacks pointers to be not NULL and 
     * store external callbacks with environment's context
     * from callback tray in instance's data structure
     */
    RIO_PL_PAR_INIT_BLOCK_DS_T* instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T*)handle;

    if (ctray == NULL || 
        handle == NULL ||
/*        ctray->rio_Error_Msg == NULL || */
        ctray->rio_Link_Get_Granule == NULL ||
        ctray->rio_Link_Put_Granule == NULL ||
        ctray->rio_Set_State_Flag == NULL   ||
        ctray->rio_Get_State_Flag == NULL
/*        ctray->rio_Warning_Msg == NULL || */
        )
        return RIO_ERROR;
    
    instanse->interfaces = *ctray;
    
    return RIO_OK;
}
/***************************************************************************
 * Function : rio_PL_Par_Init_Block_Delete_Instance
 *
 * Description: deletes instance of PL
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Delete_Instance(
    RIO_HANDLE_T          handle
    )
{ 
    RIO_PL_PAR_INIT_BLOCK_DS_T *pl_Par_Init_Block_Instanse;

    /* check pointers */
    if (handle == NULL) 
        return RIO_ERROR;

    pl_Par_Init_Block_Instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T*)handle;

    if (pl_Par_Init_Block_Instanse != NULL)
        free(pl_Par_Init_Block_Instanse);
   
    return RIO_OK; 

}
/***************************************************************************
 * Function : pl_Get_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Get_Granule(
    RIO_CONTEXT_T context,   /* my handle */
    RIO_HANDLE_T  handle,    /* invokater's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    )
{
    RIO_PL_PAR_INIT_BLOCK_DS_T *instanse; 

    /* check pointers and state */
    if (context == NULL || handle == NULL || 
        granule == NULL)
        return RIO_ERROR;

    instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T*)context;
    /*it's need to be thinked over!!!!! - how to check link handle*/
    /* check if invokation comes form our link */
 /*   if (instanse->link_Instanse != handle)
    {*/
    /*    RIO_PL_Par_Init_Block_Print_Message(instanse, RIO_PL_ERROR_6); */
/*        return RIO_ERROR;
    }*/

    if (instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if (instanse->is_On == RIO_TRUE)
        pl_Par_Init_Block_Granule_For_Transmitting(instanse, granule);
    else
        instanse->interfaces.rio_Link_Get_Granule(
        instanse->interfaces.rio_PL_Interface_Context,
        instanse,
        granule);
    return RIO_OK;
}
/***************************************************************************
 * Function : pl_Put_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    )
{
    RIO_PL_PAR_INIT_BLOCK_DS_T *instanse;

    if (context == NULL || handle == NULL || granule == NULL)
        return RIO_ERROR;

    /* instanse handler */
    instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T*)context;

    

    /* check if call comes from proper link */
/*    if (instanse->link_Instanse != handle)
    {
        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_7);
        return RIO_ERROR;
    }*/

    if (instanse->is_On == RIO_TRUE)
    /* pass the granule to receiver */
        pl_Par_Init_Block_Granule_Received(instanse, granule);
    else
        instanse->interfaces.rio_Link_Put_Granule(
                instanse->interfaces.rio_PL_Interface_Context,
                instanse,
                granule);

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Par_Init_Block_Init
 *
 * Description: initialize the transmitter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Initialize(
    RIO_HANDLE_T                     handle,
    RIO_PAR_INIT_BLOCK_PARAM_SET_T   *param_Set
    )
{
    RIO_PL_PAR_INIT_BLOCK_DS_T *instanse;
    
    /* check pointers */
    if (handle == NULL || param_Set == NULL)
        return RIO_ERROR;

    instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T *)(handle);
   
    /* check reset state */
    if (instanse->in_Reset == RIO_FALSE)
    {
       /* RIO_PL_Print_Message(instanse, RIO_PL_ERROR_3);*/
        return RIO_ERROR;
    }
    
    /*pass the necessary params*/
    instanse->parameters_Set.num_Trainings = param_Set->num_Trainings;
    instanse->parameters_Set.requires_Window_Alignment = param_Set->requires_Window_Alignment;
    instanse->parameters_Set.transmit_Flow_Control_Support = param_Set->transmit_Flow_Control_Support;
    instanse->parameters_Set.is_Init_Proc_On = param_Set->is_Init_Proc_On;
    /*firstly the training machine shall be switched on*/
    instanse->is_On = param_Set->is_Init_Proc_On;

    
    instanse->rx_Data.train_Counter = 0;
    instanse->tx_Data.train_Counter = 0;

    instanse->tx_Data.got_2I_Non_T_Lrqst = RIO_FALSE;
    instanse->rx_Data.got_2I_Non_T_Lrqst = RIO_FALSE;

    /* initialize */
    if (instanse->parameters_Set.requires_Window_Alignment == RIO_TRUE)
    {
        set_Tx_State(handle, RIO_PL_TX_SEND_TRN_REQ);
        set_Rx_State(handle, RIO_PL_RX_WAIT_GOOD_PTTN);
    }
    else
    {
        set_Tx_State(handle, RIO_PL_TX_SEND_IDLES_REQ);
        /*immediately shift output state machine to the OK state
        for model can immediately sends packets
        First set to SEND_IDLES_REQ state because state machine checker
        allow transmit to the OK state only from send idles state*/
        set_Tx_State(handle, RIO_PL_TX_OK);

        set_Rx_State(handle, RIO_PL_RX_WAIT_FOR_IDLE);
        /*immediately shift training machine to OK state for model
        can accept packets immediately.
        The first initialization of the state machine is necessary 
        because state machine checker allow shift to OK state only from
        the WAIT_FOR_IDLE state*/
        set_Rx_State(handle, RIO_PL_RX_OK);
    }

    instanse->interfaces.rio_Set_State_Flag(instanse->interfaces.rio_Regs_Context, instanse,
                    RIO_INITIALIZED, RIO_FALSE, 0);

    /*set off the reset flag*/
    instanse->in_Reset = RIO_FALSE;
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Par_Init_Block_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Start_Reset(
    RIO_HANDLE_T handle
    )
{
    RIO_PL_PAR_INIT_BLOCK_DS_T *instanse;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T *)(handle);

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;
    
    /* modify state to be _in_reset_ */
    instanse->in_Reset = RIO_TRUE;

    return RIO_OK;
}
/***************************************************************************
 * Function : pl_Par_Init_Block_Clock_Edge
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Clock_Edge(
    RIO_HANDLE_T handle
    )
{
    RIO_PL_PAR_INIT_BLOCK_DS_T *instanse;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T *)(handle);

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;

    count_LRQST_Time_Out(handle);
    
    
    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Par_Init_Block_Machine_Management
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Machine_Management(
    RIO_HANDLE_T handle,
    RIO_BOOL_T    machine_Is_On
    )
{
    
    RIO_PL_PAR_INIT_BLOCK_DS_T *instanse;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T *)(handle);

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;

    instanse->is_On = machine_Is_On;

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Par_Init_Block_Machine_Change_State
 *
 * Description: change the state of init block in case it is switched on
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int pl_Par_Init_Block_Machine_Change_State(
    RIO_HANDLE_T                    handle,
    RIO_PAR_INIT_BLOCK_COMMANDS_T   command
    )
{
    
    RIO_PL_PAR_INIT_BLOCK_DS_T *instanse;
    
    /* check pointers */
    if (handle == NULL)
        return RIO_ERROR;

    instanse = (RIO_PL_PAR_INIT_BLOCK_DS_T *)(handle);

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;

    if (instanse->is_On == RIO_TRUE)
    {
        switch (command)
        {
            case RIO_PAR_INIT_BLOCK_LOST_SYNC:
            if (instanse->parameters_Set.requires_Window_Alignment == RIO_TRUE)
            {
                
                switch ( instanse->rx_Data.train_State)
                {
                    case RIO_PL_RX_WAIT_FOR_IDLE:
                    case RIO_PL_RX_OK:
                    case RIO_PL_RX_PRE_WAIT_FOR_IDLE:
                    case RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK:
                    case RIO_PL_RX_RECEIVE_TRAIN_SEQ_OK:
                        set_Rx_State(handle, RIO_PL_RX_WAIT_GOOD_PTTN);
                        break;
                    default:
                        break;
                }

                switch (instanse->tx_Data.train_State)
                {
                    case RIO_PL_TX_OK:
                        set_Tx_State(handle, RIO_PL_TX_SEND_TRN_REQ);
                        break;
                    default:
                        break;
                    
                }
            }
            break;

            case RIO_PAR_INIT_BLOCK_LOST_RECAL:
            if (instanse->parameters_Set.requires_Window_Alignment == RIO_TRUE)
            {
                switch ( instanse->tx_Data.train_State)
                {
                    case RIO_PL_TX_OK:
                        set_Tx_State(handle, RIO_PL_TX_OK_SEND_TRN_REQ); 
                        break;
                    default:
                        break;
                }
            }
            break;
        }
    }

    return RIO_OK;
}
/***************************************************************************
 * Function : pl_Par_Init_Block_Granule_For_Transmitting
 *
 * Description: main transmitter loop
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Par_Init_Block_Granule_For_Transmitting(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned long tx_Data_In_Progress = RIO_FALSE;
    unsigned int tmp; /*stub of indicator_Param for invocation of get_state routine*/

    if (handle == NULL || granule == NULL)
        return;
    
    switch (handle->tx_Data.train_State)
    {
        /* normal state proceding */
        case RIO_PL_TX_SEND_TRN_REQ:
            form_LRQST(handle, granule);
            set_Tx_State(handle, RIO_PL_TX_SEND_TRN_PTTN);
            break;
        case RIO_PL_TX_SEND_TRN_PTTN:
            form_Training(handle, granule);
            handle->tx_Data.train_Counter++;
            if (handle->tx_Data.train_Counter >= handle->parameters_Set.num_Trainings)
            {
                if (handle->rx_Data.train_State == RIO_PL_RX_WAIT_GOOD_PTTN)
                    set_Tx_State(handle, RIO_PL_TX_SEND_TRN_REQ);
                else 
                    set_Tx_State(handle, RIO_PL_TX_SEND_IDLES_REQ);
            }
            break;
        case RIO_PL_TX_SEND_IDLES_REQ:
            form_Idle(handle, granule);
           /* set_Tx_State(handle, RIO_PL_TX_SEND_IDLES);*/
            if (handle->rx_Data.train_State == RIO_PL_RX_OK)
            {
                set_Tx_State(handle, RIO_PL_TX_OK);
                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_INITIALIZED, 1, 0);
            }
            else if (handle->rx_Data.train_State != RIO_PL_RX_WAIT_GOOD_PTTN)
                set_Tx_State(handle, RIO_PL_TX_SEND_TRN_PTTN);
            else
                set_Tx_State(handle, RIO_PL_TX_SEND_TRN_REQ);
            break;

        case RIO_PL_TX_OK:
            /*call the upper layer*/
            handle->interfaces.rio_Link_Get_Granule(handle->interfaces.rio_PL_Interface_Context, handle, granule);
            if ((granule->granule_Type == RIO_GR_LINK_REQUEST || 
                (granule->granule_Type == RIO_GR_UNRECOGNIZED && granule->granule_Struct.stype == RIO_GR_LINK_REQUEST_STYPE)) && 
                granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
                set_Rx_State(handle, RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK); 
            break;
        
        case RIO_PL_TX_OK_SEND_TRN_REQ:
            form_LRQST(handle, granule);
            set_Rx_State(handle, RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK); 
            set_Tx_State(handle, RIO_PL_TX_OK);
            break;
 
        case RIO_PL_TX_OK_PRE_SEND_TRN_PTTN:
            handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                RIO_TX_DATA_IN_PROGRESS, &tx_Data_In_Progress, &tmp);

            if (tx_Data_In_Progress == RIO_FALSE)
            {
                set_Tx_State(handle, RIO_PL_TX_SEND_TRN_PTTN);
                form_Training(handle, granule);
                handle->tx_Data.train_Counter++;
                if (handle->tx_Data.train_Counter >= handle->parameters_Set.num_Trainings)
                {
                    if (handle->rx_Data.train_State == RIO_PL_RX_WAIT_GOOD_PTTN)
                        set_Tx_State(handle, RIO_PL_TX_SEND_TRN_REQ);
                    else 
                        set_Tx_State(handle, RIO_PL_TX_SEND_IDLES_REQ);
                }
                break;
            }
            else
            {
                handle->interfaces.rio_Link_Get_Granule(handle->interfaces.rio_PL_Interface_Context, handle, granule);
                if (granule->granule_Type == RIO_GR_LINK_REQUEST && 
                    granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
                    set_Rx_State(handle, RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK); 
                break;
            }
        
    } 


}

/***************************************************************************
 * Function : form_Idle
 *
 * Description: form proper IDLE for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void form_Idle(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *idle)
{
    unsigned long indicator_Value; 
    unsigned int  indicator_Param = 0;
    if (handle == NULL || idle == NULL)
        return;
    idle->granule_Type = RIO_GR_IDLE;
    idle->granule_Struct.s = 1;
    idle->granule_Struct.ack_ID = RIO_GR_IDLE_SUB_TYPE;
    idle->granule_Struct.secded = 0;
    /*get buf_Status*/
    
    handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
        RIO_FLOW_CONTROL_MODE, &indicator_Value, &indicator_Param);

    if (indicator_Value  == RIO_TRUE) /*transmit flow control*/
    {
        unsigned long buf_Status = 0;
        handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
            RIO_OUR_BUF_STATUS, &buf_Status, &indicator_Param);
        idle->granule_Struct.buf_Status = buf_Status;
    }
    else 
        idle->granule_Struct.buf_Status = 0xf;

    idle->granule_Struct.stype = RIO_GR_PACKET_CONTROL_STYPE;
    return;
}

/***************************************************************************
 * Function : form_LRQST
 *
 * Description: form proper Link Request/send training for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void form_LRQST(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned long indicator_Value;
    unsigned int  indicator_Param = 0;
    
    if (handle == NULL || granule == NULL)
        return;
    granule->granule_Type = RIO_GR_LINK_REQUEST;
    granule->granule_Struct.s = 1;
    granule->granule_Struct.ack_ID = RIO_PL_LREQ_SEND_TRAINING;
    granule->granule_Struct.stype = RIO_GR_LINK_REQUEST_STYPE;
    granule->granule_Struct.secded = 0;
    /*get buf_Status*/
    /*
    handle->interfaces.rio_Get_State_Flag(
        handle->interfaces.rio_Regs_Context,
        RIO_OUR_BUF_STATUS,
        &(granule->granule_Struct.buf_Status), &indicator_Param);*/

    handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
        RIO_FLOW_CONTROL_MODE, &indicator_Value, &indicator_Param);

    if (indicator_Value  == RIO_TRUE) /*transmit flow control*/
    {
        unsigned long buf_Status = 0;
        handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
            RIO_OUR_BUF_STATUS, &buf_Status, &indicator_Param);
        granule->granule_Struct.buf_Status = buf_Status;
    }
    else 
        granule->granule_Struct.buf_Status = 0xf;
    
    return;
    
}

/***************************************************************************
 * Function : form_Training
 *
 * Description: form proper training for sending
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void form_Training(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;
    granule->granule_Type = RIO_GR_TRAINING;

    return;
}
/***************************************************************************
 * Function : pl_Par_Init_Block_Granule_Received
 *
 * Description: receiver main loop
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void pl_Par_Init_Block_Granule_Received(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
    if (handle == NULL || granule == NULL)
        return;

    switch (handle->rx_Data.train_State)
    {
        /* here training state machine*/
        case RIO_PL_RX_RESET:
            return;
        case RIO_PL_RX_WAIT_GOOD_PTTN:
            handler_Train_Wait_Good_Pttn_State(handle, granule);
            return;
        case RIO_PL_RX_WAIT_FOR_IDLE:
            handler_Train_Wait_For_Idle_State(handle, granule);
            return;
        case RIO_PL_RX_OK:
            handler_Train_Rx_OK_State(handle, granule);
            break;
        case RIO_PL_RX_PRE_WAIT_FOR_IDLE:
            handler_Train_Pre_Receive_Wait_For_Idle_State(handle, granule);
            break; 
        case RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK:
            handler_Train_Pre_Receive_Train_Seq_OK_State(handle, granule);
            break; 
        case RIO_PL_RX_RECEIVE_TRAIN_SEQ_OK:
            handler_Train_Receive_Train_Seq_OK_State(handle, granule);
            break; 
    };
    return;
}
/***************************************************************************
 * Function : count_LRQST_Time_Out
 *
 * Description: count time out for sent LINK REQUEST/send_Training
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void count_LRQST_Time_Out(RIO_PL_PAR_INIT_BLOCK_DS_T *handle)
{
    if (handle == NULL)
        return;

    if (handle->rx_Data.train_State == RIO_PL_RX_WAIT_GOOD_PTTN ||
        handle->rx_Data.train_State == RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK)
        handle->rx_Data.time_Out_Counter++;
    return;
}
/***************************************************************************
 * Function : handler_Train_Wait_Good_Pttn_State
 *
 * Description: input training state machine handler of Wait_Good_Pttn state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Train_Wait_Good_Pttn_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
   /* detect which type of granule has been received */
    switch (granule->granule_Type)
    {
        case RIO_GR_TRAINING:
            handle->rx_Data.train_Counter++;
            set_Rx_State(handle, RIO_PL_RX_WAIT_FOR_IDLE);
            /*RIO_PL_Print_Message(handle, RIO_PL_WARNING_7,
                    handle->rx_Data.train_state);*/
            break;

        default:
            break;
    }

}
/***************************************************************************
 * Function : handler_Train_Wait_For_Idle_State
 *
 * Description: input training state machine handler of Wait_For_Idle state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Train_Wait_For_Idle_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
    switch (granule->granule_Type)
    {
        case RIO_GR_TRAINING:
            handle->rx_Data.train_Counter++;
            if (handle->rx_Data.train_Counter > handle->parameters_Set.num_Trainings)
            {
                /*write warning*/
            }

            break;

        case RIO_GR_IDLE:
    /*        if (handle->tx_Data.train_State == RIO_PL_TX_SEND_IDLES)
            {
                set_Tx_State(handle, RIO_PL_TX_OK);
                set_Rx_State(handle, RIO_PL_RX_OK);
                
                 if (granule->granule_Struct.buf_Status != 0xffff)
                {
                    handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context,
                        RIO_FLOW_CONTROL_MODE, RIO_TRUE, 0); ?????*/
        /*        }
                else
                    handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context,
                        RIO_FLOW_CONTROL_MODE, RIO_FALSE, 0);

                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context,
                        RIO_PARTNER_BUF_STATUS, granule->granule_Struct.buf_Status, 0);
                
                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context,
                    RIO_INITIALIZED, 1, 0); ?????*/

            /*    RIO_PL_Print_Message(handle, RIO_PL_WARNING_7,
                    handle->rx_Data.train_state);
                RIO_PL_Print_Message(handle, RIO_PL_WARNING_8,
                    handle->tx_Data.train_state);
            */

        /*    }*/
    
            set_Rx_State(handle, RIO_PL_RX_OK);

            if (handle->parameters_Set.transmit_Flow_Control_Support == RIO_TRUE)
            {
                if (granule->granule_Struct.buf_Status != 0xf)
                {
                    handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                        RIO_FLOW_CONTROL_MODE, RIO_TRUE, 0);/*?????*/
                }
                else
                    handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                        RIO_FLOW_CONTROL_MODE, RIO_FALSE, 0);
            }

            handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_PARTNER_BUF_STATUS, granule->granule_Struct.buf_Status, 0);

            
            break;

        case RIO_GR_LINK_REQUEST:
            if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
                break;
        default:
        /*    RIO_PL_Print_Message(handle, RIO_PL_ERROR_48,granule->granule_Type);*/

            if (handle->parameters_Set.requires_Window_Alignment == RIO_TRUE)
            {
                set_Rx_State(handle, RIO_PL_RX_WAIT_GOOD_PTTN);
              /*  set_Tx_State(handle, RIO_PL_TX_SEND_TRN_REQ);*/
                                
                /*RIO_PL_Print_Message(handle, RIO_PL_WARNING_7,
                    handle->rx_Data.train_state);
                RIO_PL_Print_Message(handle, RIO_PL_WARNING_8,
                    handle->tx_Data.train_state);*/
            }
            break;
    }

}
/***************************************************************************
 * Function : handler_Train_Receive_Train_Seq_OK_State
 *
 * Description: input training state machine handler of state when training sequence 
 *                is receiving in OK                
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Train_Receive_Train_Seq_OK_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
    switch (granule->granule_Type)
    {
        case RIO_GR_TRAINING:
            handle->rx_Data.train_Counter++;

            if (handle->rx_Data.train_Counter > handle->parameters_Set.num_Trainings)
            {
                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_UNREC_ERROR, 1, 0);

                print_Message(handle, INIT_BLOCK_ERROR_2);

                set_Rx_State(handle, RIO_PL_RX_OK);
            }

            break;

        case RIO_GR_IDLE:
            if (handle->rx_Data.train_Counter < handle->parameters_Set.num_Trainings)
            {
                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_UNREC_ERROR, 1, 0);
                print_Message(handle, INIT_BLOCK_ERROR_2);

            };
            set_Rx_State(handle, RIO_PL_RX_OK);

            /*    RIO_PL_Print_Message(handle, RIO_PL_WARNING_7,
                    handle->rx_Data.train_state);
                RIO_PL_Print_Message(handle, RIO_PL_WARNING_8,
                    handle->tx_Data.train_state);
            */

            break;
        default:
            /*RIO_PL_Print_Message(handle, RIO_PL_ERROR_48,granule->granule_Type);*/
            handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_UNREC_ERROR, 1, 0);
            print_Message(handle, INIT_BLOCK_ERROR_2);

            set_Rx_State(handle, RIO_PL_RX_OK);
            break;
    }
    return;
}

/***************************************************************************
 * Function : handler_Train_Rx_OK_State
 *
 * Description: input training state machine handler of state when training sequence 
 *                is receiving in OK                
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Train_Rx_OK_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, RIO_GRANULE_T *granule)
{
    switch (granule->granule_Type)
    {
        case RIO_GR_TRAINING:

            if (handle->rx_Data.got_2I_Non_T_Lrqst == RIO_TRUE)
            {
                handle->rx_Data.train_Counter++;

                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_TX_DATA_IN_PROGRESS, RIO_FALSE, 0);
            
                if (handle->tx_Data.train_State == RIO_PL_TX_OK)
                    set_Tx_State(handle, RIO_PL_TX_OK_PRE_SEND_TRN_PTTN);

                set_Rx_State(handle, RIO_PL_RX_WAIT_FOR_IDLE);
            }

            handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                        RIO_OUTPUT_ERROR, 1, 0);

            print_Message(handle, INIT_BLOCK_ERROR_1);
            
            break;
        case RIO_GR_LINK_REQUEST:
            if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
            {
                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                RIO_TX_DATA_IN_PROGRESS, RIO_FALSE, 0);
            
                if (handle->tx_Data.train_State == RIO_PL_TX_OK)
                    set_Tx_State(handle, RIO_PL_TX_OK_PRE_SEND_TRN_PTTN);
                
                set_Rx_State(handle, RIO_PL_RX_PRE_WAIT_FOR_IDLE);
                /*RIO_PL_Print_Message(handle, RIO_PL_WARNING_8,
                    handle->tx_Data.train_state);*/
            }
        /*    break;*/
        default:
            handle->rx_Data.got_2I_Non_T_Lrqst = RIO_TRUE;
            /*RIO_PL_Print_Message(handle, RIO_PL_ERROR_48,granule->granule_Type);*/
            handle->interfaces.rio_Link_Put_Granule(
                handle->interfaces.rio_PL_Interface_Context,
                handle,
                granule);
            break;
    }
    return;
}

/***************************************************************************
 * Function : handler_Train_Pre_Receive_Train_Seq_OK_State
 *
 * Description: input training state machine handler of state when training sequence 
 *                is receiving in OK                
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Train_Pre_Receive_Train_Seq_OK_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
                                                            RIO_GRANULE_T *granule)
{
    unsigned long rx_Data_In_Progress = RIO_FALSE;
    unsigned int parameter = 0;

    switch (granule->granule_Type)
    {
        case RIO_GR_TRAINING:
            handle->rx_Data.train_Counter++;
            set_Rx_State(handle, RIO_PL_RX_RECEIVE_TRAIN_SEQ_OK);
            
            handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                RIO_RX_DATA_IN_PROGRESS, &rx_Data_In_Progress, &parameter);
            
            if (rx_Data_In_Progress == RIO_TRUE)
            {
                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_INPUT_ERROR, RIO_TRUE, RIO_PL_PNACC_GENERAL_ERROR);
                print_Message(handle, INIT_BLOCK_ERROR_3);
            }
            break;

        case RIO_GR_LINK_REQUEST:
            if (granule->granule_Struct.ack_ID == RIO_PL_LREQ_SEND_TRAINING)
            {
              /*  handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context,
                RIO_TX_DATA_IN_PROGRESS, RIO_FALSE, 0);*/
                /*in order to cancel or end packet (if currently is transmitted)*/
                
                set_Tx_State(handle, RIO_PL_TX_OK_PRE_SEND_TRN_PTTN);
                /*RIO_PL_Print_Message(handle, RIO_PL_WARNING_8,
                    handle->tx_Data.train_state);*/
            }
        /*    break;*/
        default:
            /*RIO_PL_Print_Message(handle, RIO_PL_ERROR_48,granule->granule_Type);*/
            handle->interfaces.rio_Link_Put_Granule(
                handle->interfaces.rio_PL_Interface_Context,
                handle,
                granule);
            break;
    }
    return;
}
/***************************************************************************
 * Function : handler_Train_Pre_Receive_Wait_For_Idle_State
 *
 * Description: input training state machine handler of state when training sequence 
 *                is receiving in OK                
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Train_Pre_Receive_Wait_For_Idle_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle,
                                                            RIO_GRANULE_T *granule)
{
    unsigned long rx_Data_In_Progress = RIO_FALSE;
    unsigned int parameter = 0;

    switch (granule->granule_Type)
    {
        case RIO_GR_TRAINING:
            handle->rx_Data.train_Counter++;
            set_Rx_State(handle, RIO_PL_RX_WAIT_FOR_IDLE);
            
            handle->interfaces.rio_Get_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                RIO_RX_DATA_IN_PROGRESS, &rx_Data_In_Progress, &parameter);
            
            if (rx_Data_In_Progress == RIO_TRUE)
            {
                handle->interfaces.rio_Set_State_Flag(handle->interfaces.rio_Regs_Context, handle,
                    RIO_INPUT_ERROR, RIO_TRUE, RIO_PL_PNACC_GENERAL_ERROR);
                print_Message(handle, INIT_BLOCK_ERROR_3);
            }
            break;
        
        default:

            set_Rx_State(handle, RIO_PL_RX_OK);
            /*RIO_PL_Print_Message(handle, RIO_PL_ERROR_48,granule->granule_Type);*/
            handle->interfaces.rio_Link_Put_Granule(
                handle->interfaces.rio_PL_Interface_Context,
                handle,
                granule);
            break;
    }
    return;
}

/***************************************************************************
 * Function : set_Tx_State
 *
 * Description: input training state machine handler of state when training sequence 
 *                is receiving in OK                
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void set_Tx_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, unsigned int state)
{
    if (handle == NULL)
        return;
    switch (state)
    {
        case RIO_PL_TX_RESET:
           /*? handle->restart_Function(handle);*/
            break;
        case RIO_PL_TX_SEND_TRN_REQ:
            if (handle->parameters_Set.requires_Window_Alignment == RIO_TRUE)
                handle->tx_Data.train_State = state;
            break;
        case RIO_PL_TX_SEND_TRN_PTTN:
            if (handle->tx_Data.train_State != RIO_PL_TX_SEND_TRN_PTTN)
            {
                handle->tx_Data.train_State = state;
                handle->tx_Data.train_Counter = 0;
            }
            break;
        case RIO_PL_TX_SEND_IDLES_REQ:
            handle->tx_Data.train_State = state; 
            break;
     /*   case RIO_PL_TX_SEND_IDLES:
            handle->tx_Data.train_State = state;
            break;*/
        case RIO_PL_TX_OK:
            if (handle->tx_Data.train_State == RIO_PL_TX_SEND_IDLES_REQ ||
                handle->tx_Data.train_State == RIO_PL_TX_OK_SEND_TRN_REQ)
                handle->tx_Data.train_State = state; 
            break;
        case RIO_PL_TX_OK_PRE_SEND_TRN_PTTN:
            if (handle->tx_Data.train_State == RIO_PL_TX_OK)
            {
                handle->tx_Data.train_State = state;
                handle->tx_Data.train_Counter = 0;
            }
            break;
        case RIO_PL_TX_OK_SEND_TRN_REQ:
            if (handle->tx_Data.train_State == RIO_PL_TX_OK)
                handle->tx_Data.train_State = state;
          /*  handle->tx_Data.train_Counter = 0;*/
          /*  break;*/
      /*  case RIO_PL_TX_OK_SEND_IDLES:
            handle->tx_Data.train_State = state;
            break;*/
    }
    return;
}

/***************************************************************************
 * Function : set_Rx_State
 *
 * Description: input training state machine handler of state when training sequence 
 *                is receiving in OK                
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void set_Rx_State(RIO_PL_PAR_INIT_BLOCK_DS_T *handle, unsigned int state)
{
    if (handle == NULL)
        return;
    switch (state)
    {
        case RIO_PL_RX_RESET:
            handle->rx_Data.train_State = state;
            break;
        case RIO_PL_RX_WAIT_GOOD_PTTN:
            if (handle->parameters_Set.requires_Window_Alignment == RIO_TRUE)
            {
                handle->rx_Data.train_Counter = 0;
                handle->rx_Data.train_State = state;
                handle->rx_Data.time_Out_Counter = 0;
            }
            break;
        case RIO_PL_RX_WAIT_FOR_IDLE:
            handle->rx_Data.train_State = state;
            break;
        case RIO_PL_RX_OK:
            if (handle->rx_Data.train_State == RIO_PL_RX_WAIT_FOR_IDLE)
            {
                /*this is done to prevent the situation when the shifts between the receiver and 
                transmiter lanes leads to infinite initialization process
                the flag is indicates, than nothing except idle is received on the link;
                and shall be set on when the next data portion different to trainings is received*/
                handle->rx_Data.got_2I_Non_T_Lrqst = RIO_FALSE; 
            };

            handle->rx_Data.train_State = state;
            handle->rx_Data.train_Counter = 0;
           
            break;
        case RIO_PL_RX_PRE_WAIT_FOR_IDLE:
            handle->rx_Data.train_Counter = 0;
            handle->rx_Data.train_State = state;
            break;
        case RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK:
            if (handle->rx_Data.train_State == RIO_PL_RX_OK)
            {
                handle->rx_Data.train_Counter = 0;
                handle->rx_Data.train_State = state;
            }
            break;
        case RIO_PL_RX_RECEIVE_TRAIN_SEQ_OK:
            if (handle->rx_Data.train_State == RIO_PL_RX_PRE_RECEIVE_TRAIN_SEQ_OK)
            {
                handle->rx_Data.train_State = state;
            }
            break;
    }
    return;
}
/***************************************************************************
 * Function : print_Message
 *
 * Description: detect error message by error code and send it to callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int print_Message(RIO_PL_PAR_INIT_BLOCK_DS_T* handle, INIT_BLOCK_MESSAGES_T message_Type, ...)
{
    /* buffer to store error message maximum size 255 bytes */
    char message_Buffer[255];

    /* 
     * additional information which shall present in error message
     * is passing through additional (optional) parameters:
     * add_Args[0]...[4]. 
     * i - loop counter
     */
    unsigned long add_Args[4] = {0, 0, 0, 0};
    int i;

    /* this is necessary to detect additional parameters */
    va_list pointer_To_Next_Arg;

    /*
     * check all using pointers to be not NULL, check
     * that type of error is within corresponding array
     * and when invoke callback with actual parameters.
     * actual error message obtain using error type as an index
     */
    if (handle == NULL || 
        handle->interfaces.rio_User_Msg == NULL ||
        (int)message_Type < 0 || 
        (int)message_Type > (init_Block_Message_Strings_Cnt - 1) ||
        (int)message_Type > (init_Block_Messsage_Map_Count - 1))
        return RIO_ERROR;

    /* get additional parameters*/
    va_start(pointer_To_Next_Arg, message_Type);

    for (i = 1; i <= init_Block_Message_Strings[(int)message_Type].count_Of_Add_Args; i++)
    {
        add_Args[i - 1] = va_arg(pointer_To_Next_Arg, unsigned long);     
    }

    va_end(pointer_To_Next_Arg);

    /*
     * obtain actual error message - switch labels are count of arguments 
     * for example, 4 means 4 additional arguments 
     */
    switch (init_Block_Message_Strings[(int)message_Type].count_Of_Add_Args)
    {
        case 0:
            sprintf(message_Buffer, init_Block_Message_Strings[(int)message_Type].string,
                handle->instance_Number);
            break;

        case 1:
            sprintf(message_Buffer, init_Block_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0]);
            break;

        case 2:
            sprintf(message_Buffer, init_Block_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1]);
            break;

        case 3:
            sprintf(message_Buffer, init_Block_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1], add_Args[2]);
            break;

        case 4:
            sprintf(message_Buffer, init_Block_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1], add_Args[2], add_Args[3]);
            break;

        default:
            return RIO_ERROR;
        
    }

    /* print result error message*/
    if (handle->interfaces.rio_User_Msg != NULL && handle->interfaces.rio_Msg != NULL)
        return (handle->interfaces.rio_User_Msg(handle->interfaces.rio_Msg, 
            init_Block_Message_To_Type_Map[(int)message_Type],
            message_Buffer));
    else 
        return RIO_ERROR;
    
}

/*****************************************************************************/

