#ifndef RIO_PL_PAR_INIT_BLOCK_INTERFACE_H
#define RIO_PL_PAR_INIT_BLOCK_INTERFACE_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/parallel_init/include/rio_parallel_init_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    32txrx model interface
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_GRAN_TYPE_H
#include "rio_gran_type.h"
#endif

typedef struct{
    unsigned int num_Trainings;
    RIO_BOOL_T requires_Window_Alignment;
    RIO_BOOL_T is_Init_Proc_On;
    RIO_BOOL_T transmit_Flow_Control_Support; 
} RIO_PAR_INIT_BLOCK_PARAM_SET_T;

/* this function is used to turn 32 bit Transmitter/Receiver into reset
 * mode. In reset mode instance does not send or accept anything, does not
 * react on clock events. All Tx/Rx internal parameters turn to initial values.
 */
typedef int (*RIO_PL_PAR_INIT_BLOCK_START_RESET)(
    RIO_HANDLE_T handle       /* instance's handle */
    );

/* 
 * the function is used to initialize and re-initialize the link (32 bit
 * Transmitter/Receiver. The function invokation turns the instance from 
 * reset to work mode.
 */
typedef int (*RIO_PL_PAR_INIT_BLOCK_INITIALIZE)(
    RIO_HANDLE_T           handle,     /* handle of instance*/
    RIO_PAR_INIT_BLOCK_PARAM_SET_T   *param_Set  /* initialization parameters */
    );

/*
 * to switch on/off init machine. 
 */
typedef int (*RIO_TRAIN_INIT_MACHINE_MANAGEMENT)(
    RIO_HANDLE_T                handle,
    RIO_BOOL_T                    machine_Is_Switched_On /*RIO_TRUE if switched on*/
    );

/*
 * to switch on/off init machine. 
 */
typedef int (*RIO_TRAIN_INIT_MACHINE_CHANGE_STATE)(
    RIO_HANDLE_T                  handle,
    RIO_PAR_INIT_BLOCK_COMMANDS_T command /*the new state of machine*/
    );

#endif /* RIO_PL_PAR_INIT_BLOCK_INTERFACE_H */

