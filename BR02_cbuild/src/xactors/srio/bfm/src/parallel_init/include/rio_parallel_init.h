#ifndef RIO_PL_PAR_INIT_BLOCK_H
#define RIO_PL_PAR_INIT_BLOCK_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/parallel_init/include/rio_parallel_init.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the physical layer
*
* Notes:        
*
******************************************************************************/
/*
#ifndef RIO_PL_INTERFACE_H
#include "rio_pl_interface.h"
#endif
*/

#ifndef RIO_32_TXRX_INTERFACE_H
#include "rio_32_txrx_interface.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif

#ifndef RIO_PL_PAR_INIT_BLOCK_INTERFACE_H
#include "rio_parallel_init_interface.h"
#endif



/* 
 * function tray to be passed to the environment by the physical layer model
 */
typedef struct {
    
    RIO_PL_PAR_INIT_BLOCK_START_RESET   rio_PL_Par_Init_Block_Start_Reset;
    RIO_PL_PAR_INIT_BLOCK_INITIALIZE    rio_PL_Par_Init_Block_Initialize;

    RIO_LINK_GET_GRANULE                rio_PL_Par_Init_Block_Get_Granule;
    RIO_LINK_PUT_GRANULE                rio_PL_Par_Init_Block_Put_Granule;
        
    RIO_DELETE_INSTANCE                 rio_PL_Par_Init_Block_Delete_Instance;
    RIO_LINK_CLOCK_EDGE                 rio_PL_Par_Init_Block_Clock_Edge;

    RIO_TRAIN_INIT_MACHINE_MANAGEMENT   rio_PL_Par_Init_Block_Machine_Management;
    RIO_TRAIN_INIT_MACHINE_CHANGE_STATE rio_PL_Par_Init_Block_Machine_Change_State;

} RIO_PL_PAR_INIT_BLOCK_FTRAY_T;


/* 
 * callback tray to be used by the physical layer model (supplied by the model 
 * environment). It also includes two contexts: context of 8LP/EP callbacks 
 * interface and context of physical layer interface callbacks. These contexts are
 * used for running corresponding callbacks in proper environment.
 */
typedef struct {
    RIO_LINK_GET_GRANULE        rio_Link_Get_Granule;
    RIO_LINK_PUT_GRANULE        rio_Link_Put_Granule;
    RIO_CONTEXT_T               rio_PL_Interface_Context; 

    RIO_SET_STATE               rio_Set_State_Flag;
    RIO_GET_STATE               rio_Get_State_Flag;
    RIO_CONTEXT_T               rio_Regs_Context; 
    
    
    RIO_LINK_MSG                rio_User_Msg;
    RIO_CONTEXT_T               rio_Msg;
    
} RIO_PL_PAR_INIT_BLOCK_CALLBACK_TRAY_T;




/* 
 * the function creates instance of PL model and returns handle to its data structure.
 * there are some create parameters, which can be defined only at the moment of 
 * instance creation. They are: size (in count of whole packets) of inbound buffer and
 * size (the same) of outbound buffer.
 * After the instance's been created it is in reset mode. It's necessary to
 * invoke Initialize function to turn it into work mode.
 */
int RIO_PL_Par_Init_Block_Create_Instance(
    RIO_HANDLE_T                     *handle
    );

/* 
 * the function returns PL model interface functions to be used for model access. 
 * the function tray shall be allocated by invokation side and pointer to it is 
 * passed to the function. PL only load this tray by actual entry points pointers.
 */
int RIO_PL_Par_Init_Block_Get_Function_Tray(
    RIO_PL_PAR_INIT_BLOCK_FTRAY_T *ftray             
    );


/* 
 * the function binds PL model instance to the environment. The callback tray is
 * allocated outside, loaded by proper callback entry point pointers and passed 
 * through the pointer to it here. 
 * The tray is stored internal in instance's internal data
 */
int RIO_PL_Par_Init_Block_Bind_Instance(
    RIO_HANDLE_T                            handle,
    RIO_PL_PAR_INIT_BLOCK_CALLBACK_TRAY_T   *ctray   
    );

#endif /* RIO_PL_PAR_INIT_BLOCK_H */

