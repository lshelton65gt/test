#include <iostream>
#include <string>
#include <cassert>
using namespace std;

extern "C" {
#include "rio_serial_model.h"
}

// #define DEBUG

struct SrioPins {
    RIO_SIGNAL_T *tlane0;
    RIO_SIGNAL_T *tlane1;
    RIO_SIGNAL_T *tlane2;
    RIO_SIGNAL_T *tlane3;
    RIO_SIGNAL_T *rlane0;
    RIO_SIGNAL_T *rlane1;
    RIO_SIGNAL_T *rlane2;
    RIO_SIGNAL_T *rlane3;
};

class SrioXtor {
    private:
        unsigned int _id;
        RIO_HANDLE_T _bfm;
        SrioPins _pins;
        RIO_PL_SERIAL_MODEL_FTRAY_T _ftray;
        RIO_TAG_T _trx_Tag;
        unsigned int _currentTime;
    public:
        SrioXtor(const unsigned int id, SrioPins pins);
        unsigned int getId() const { return _id; }
        SrioPins getPins() const { return _pins; }
        unsigned int getCurrentTime() const { return _currentTime; }
        void init();
        void run(unsigned int t);
        void sendIoRequest(RIO_IO_REQ_T *rq);

        static int ackRequest(RIO_CONTEXT_T context, RIO_TAG_T tag);
        static int getReqData(RIO_CONTEXT_T context, RIO_TAG_T tag,
                RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data);
        static int getReqDataHw(RIO_CONTEXT_T context, RIO_TAG_T tag,
                RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_HW_T *data);
        static int ackResponse(RIO_CONTEXT_T context, RIO_TAG_T tag);
        static int getRespData(RIO_CONTEXT_T context, RIO_TAG_T tag,
                RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data);
        static int reqTimeOut(RIO_CONTEXT_T context, RIO_TAG_T tag);
        static int remoteRequest(RIO_CONTEXT_T context, RIO_TAG_T tag,
                RIO_REMOTE_REQUEST_T *req, RIO_TR_INFO_T transport_Info);
        static int remoteResponse(RIO_CONTEXT_T context, RIO_TAG_T tag,
                RIO_RESPONSE_T *resp);
        static int setPins(RIO_CONTEXT_T context,
                RIO_SIGNAL_T tlane0, RIO_SIGNAL_T tlane1,
                RIO_SIGNAL_T tlane2, RIO_SIGNAL_T tlane3);
        static int msg(RIO_CONTEXT_T  context, char *user_Msg = NULL);
        static int extRegRead(RIO_CONTEXT_T context,
                RIO_CONF_OFFS_T config_Offset, unsigned long *data);
        static int extRegWrite(RIO_CONTEXT_T context,
                RIO_CONF_OFFS_T config_Offset, unsigned long data);
        static int requestReceived(RIO_CONTEXT_T context,
                RIO_REMOTE_REQUEST_T *packet_Struct);
        static int responseReceived(RIO_CONTEXT_T context,
                RIO_RESPONSE_T *packet_Struct);
};

SrioXtor::SrioXtor(const unsigned int id, SrioPins pins):
    _id(id), _pins(pins), _trx_Tag(30), _currentTime(0)
{
    // Create BFM instance
    RIO_MODEL_INST_PARAM_T param;
    param.ext_Address_Support = 0x7;
    param.is_Bridge = RIO_FALSE;
    param.has_Memory = RIO_TRUE;
    param.has_Processor = RIO_TRUE;
    param.has_Fcp = RIO_FALSE;
    param.coh_Granule_Size_32 = RIO_TRUE;
    param.coh_Domain_Size = 0;
    param.device_Identity = _id;
    param.device_Vendor_Identity = 0;
    param.device_Rev = 0;
    param.device_Minor_Rev = 0;
    param.device_Major_Rev = 1;
    param.assy_Identity = 0;
    param.assy_Vendor_Identity = 0;
    param.assy_Rev = 0;
    param.entry_Extended_Features_Ptr = 0x0100;
    param.next_Extended_Features_Ptr = 0;
    // supports all R/W, no DS, i.e bit(16, 17, 18, 19) = (1, 1, 0, 1)
    // - note big-endian bit ordering
    param.source_Trx = param.dest_Trx = 0x0000D000;
    param.mbox1 = param.mbox2 = param.mbox3 = param.mbox4 = RIO_FALSE;
    param.has_Doorbell = RIO_FALSE;
    param.pl_Inbound_Buffer_Size = param.pl_Outbound_Buffer_Size = 8;
    param.ll_Inbound_Buffer_Size = param.ll_Outbound_Buffer_Size = 8;
    param.ack_Delay_Enable = RIO_FALSE;
    param.ack_Delay_Min = param.ack_Delay_Max = 0;
    param.gda_Tx_Illegal_Packet = param.gda_Rx_Illegal_Packet = RIO_FALSE;
    param.tx_Illegal_Pnacc_Resend_Cnt = 0;
    param.RIO_Link_Valid_To1_I = 0;
    param.resp_Packet_Delay_Enable = RIO_FALSE;
    param.resp_Packet_Delay_Cntr = 0;
    param.resp_Stomp_Needed = RIO_FALSE;
    assert(RIO_PL_Serial_Model_Create_Instance(&_bfm, &param) == RIO_OK);

    // Get function tray
    assert(RIO_PL_Serial_Model_Get_Function_Tray(&_ftray) == RIO_OK);

    // Bind BFM with callbacks
    RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T ctray;
    ctray.rio_PL_Ack_Request = &ackRequest;
    ctray.rio_PL_Get_Req_Data = &getReqData;
    ctray.rio_PL_Get_Req_Data_Hw = &getReqDataHw;
    ctray.rio_PL_Ack_Response = &ackResponse;
    ctray.rio_PL_Get_Resp_Data = &getRespData;
    ctray.rio_PL_Req_Time_Out = &reqTimeOut;
    ctray.rio_PL_Remote_Request = &remoteRequest;
    ctray.rio_PL_Remote_Response = &remoteResponse;
    ctray.pl_Interface_Context = this;
    ctray.rio_PL_Set_Pins = &setPins;
    ctray.lp_Serial_Interface_Context = this;
    ctray.rio_User_Msg = &msg;
    ctray.rio_Msg = this;
    ctray.extend_Reg_Read = &extRegRead;
    ctray.extend_Reg_Write = &extRegWrite;
    ctray.extend_Reg_Context = this;
    ctray.rio_Serial_Granule_To_Send = NULL;
    ctray.rio_Serial_Granule_Received = NULL;
    ctray.rio_Serial_Character_Column_To_Send = NULL;
    ctray.rio_Serial_Character_Column_Received = NULL;
    ctray.rio_Serial_Codegroup_Column_To_Send = NULL;
    ctray.rio_Serial_Codegroup_Received = NULL;
    ctray.rio_Serial_Hooks_Context = this;
    ctray.rio_Request_Received = &requestReceived;
    ctray.rio_Response_Received = &responseReceived;
    ctray.rio_Symbol_To_Send = NULL;
    ctray.rio_Packet_To_Send = NULL;
    ctray.rio_Hooks_Context = this;
    ctray.rio_PL_FC_To_Send = NULL;
    assert(RIO_PL_Serial_Model_Bind_Instance(_bfm, &ctray) == RIO_OK);

    // Initialize
    init();
}

void SrioXtor::init()
{
    // Reset
    // assert(_ftray.rio_PL_Model_Start_Reset(_bfm) == RIO_OK);

    // Initialize
    RIO_PL_SERIAL_PARAM_SET_T param;
    param.transp_Type = RIO_PL_TRANSP_16; // src and dest id
    param.dev_ID = _id;
    param.lcshbar = 0x0;
    param.lcsbar = 0x0;
    param.is_Host = RIO_FALSE;
    param.is_Master_Enable = RIO_TRUE;
    param.is_Discovered = RIO_FALSE;
    param.ext_Address = RIO_FALSE;
    param.ext_Address_16 = RIO_FALSE;
    param.ext_Mailbox_Support = RIO_FALSE;
    param.lp_Serial_Is_1x = RIO_FALSE; // 1x line
    param.is_Force_1x_Mode = RIO_FALSE;
    param.is_Force_1x_Mode_Lane_0 = RIO_FALSE;
    param.input_Is_Enable = RIO_TRUE;
    param.output_Is_Enable = RIO_TRUE;
    param.discovery_Period = 1000;
    param.silence_Period = 0;
    param.comp_Seq_Rate = 5000;
    param.status_Sym_Rate = 132;
    param.comp_Seq_Rate_Check = 0;
    param.comp_Seq_Size = 0;
    param.res_For_3_Out = 0;
    param.res_For_2_Out = 0;
    param.res_For_1_Out = 0;
    param.res_For_3_In = 0;
    param.res_For_2_In = 0;
    param.res_For_1_In = 0;
    param.pass_By_Prio = RIO_TRUE;
    param.transmit_Flow_Control_Support = RIO_FALSE;
    param.enable_LRQ_Resending = RIO_FALSE;
    param.enable_Retry_Return_Code = RIO_FALSE; //RIO_TRUE;
    param.icounter_Max = 2;
    param.data_Corrupted = RIO_FALSE;
    assert(_ftray.rio_PL_Serial_Model_Initialize(_bfm, &param) == RIO_OK);

    // unsigned long data;
    // assert(_ftray.rio_PL_Get_Config_Reg(_bfm, 0x5c, &data) == RIO_OK);
    // cout << data << endl; assert(0);
}
 
void SrioXtor::run(unsigned int t)
{
    _currentTime = t;
    assert(_ftray.rio_PL_Serial_Get_Pins(_bfm, *(_pins.rlane0), *(_pins.rlane1),
                *(_pins.rlane2), *(_pins.rlane3)) == RIO_OK);
    assert(_ftray.rio_PL_Clock_Edge(_bfm) == RIO_OK);
}

void SrioXtor::sendIoRequest(RIO_IO_REQ_T *rq)
{
    assert(_ftray.rio_PL_IO_Request(_bfm, rq, _trx_Tag++,
                rq->destId <<8 | _id) == RIO_OK);
}

int SrioXtor::ackRequest(RIO_CONTEXT_T context, RIO_TAG_T tag)
{
    msg(context);
    cout << "Request transaction " << int(tag) << " acknowledged." << endl;
    return RIO_OK;
}

int SrioXtor::getReqData(RIO_CONTEXT_T context, RIO_TAG_T tag,
        RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data)
{
    msg(context);
    cout << "Request data not provided for transaction " << int(tag) << endl;
    assert(0);
    return RIO_OK;
}

int SrioXtor::getReqDataHw(RIO_CONTEXT_T context, RIO_TAG_T tag,
        RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_HW_T *data)
{
    msg(context);
    cout << "Request data not provided for transaction " << int(tag) << endl;
    assert(0);
    return RIO_OK;
}

int SrioXtor::ackResponse(RIO_CONTEXT_T context, RIO_TAG_T tag)
{
    msg(context);
    cout << "Response for transaction " << int(tag) << " acknowledged." << endl;
    return RIO_OK;
}

int SrioXtor::getRespData(RIO_CONTEXT_T context, RIO_TAG_T tag,
        RIO_UCHAR_T offset, RIO_UCHAR_T cnt, RIO_DW_T *data)
{
    msg(context);
    cout << "Response data not provided for transaction " << int(tag) << endl;
    assert(0);
    return RIO_OK;
}

int SrioXtor::reqTimeOut(RIO_CONTEXT_T context, RIO_TAG_T tag)
{
    msg(context);
    cout << "Request timed out for transaction " << int(tag) << endl;
    return RIO_OK;
}

int SrioXtor::remoteRequest(RIO_CONTEXT_T context, RIO_TAG_T tag,
        RIO_REMOTE_REQUEST_T *req, RIO_TR_INFO_T transport_Info)
{
    msg(context);
    cout << "Remote request transaction " << int(tag) << " received." << endl;
    cout << "packet type = " << int(req->packet_Type) << endl;
    cout << "ttype = " << int(req->header->ttype) << endl;
    cout << "wrsize = " << int(req->header->rdsize) << endl;
    cout << "wdptr = " << int(req->header->wdptr) << endl;
    cout << "address = " << int(req->header->address) << endl;
    cout << "src_ID = " << int(req->header->src_ID) << endl;
    cout << "trans ID = " << int(req->header->target_TID) << endl;
    cout << "src_ID = " << int(req->header->src_ID) << endl;
    cout << "tag = " << int(tag) << endl;
    SrioXtor *xtor = static_cast<SrioXtor*>(context);

    RIO_RESPONSE_T resp; // following API copies the content of resp
    resp.prio = 0;
    resp.ftype = 13;
    resp.transaction = 8; // response with data payload
    resp.status = 0; // DONE
    resp.src_ID = xtor->getId();
    resp.tr_Info = req->header->src_ID << 8 | resp.src_ID;
    resp.target_TID = req->header->target_TID;
    resp.sec_ID = 0;
    resp.sec_TID = 0;
    resp.dw_Num = 8; // actually you should find this value from 
                     // wdptr and wrsize of request received
    RIO_DW_T data[8];
    for (int i = 0; i < 8; i++)
    {
        data[i].ms_Word = i;
        data[i].ls_Word = i;
    }
    resp.data = data;
    resp.transp_Type = RIO_PL_TRANSP_16;
    assert(xtor->_ftray.rio_PL_Send_Response(xtor->_bfm, tag,
                &resp) == RIO_OK);

    assert(xtor->_ftray.rio_PL_Ack_Remote_Req(xtor->_bfm, tag) == RIO_OK);
    return RIO_OK;
}

int SrioXtor::remoteResponse(RIO_CONTEXT_T context, RIO_TAG_T tag,
        RIO_RESPONSE_T *resp)
{
    msg(context);
    cout << "Remote response for transaction " << int(tag) <<
        " received." << endl;
    cout << "src_ID = " << int(resp->src_ID) << endl;
    cout << "dw_Num = " << int(resp->dw_Num) << endl;
    for (int i = 0; i < resp->dw_Num; i++)
        cout << "data[" << i << "] = " <<
            resp->data[i].ms_Word << resp->data[i].ls_Word << endl;
    return RIO_OK;
}

int SrioXtor::setPins(RIO_CONTEXT_T context,
        RIO_SIGNAL_T tlane0, RIO_SIGNAL_T tlane1,
        RIO_SIGNAL_T tlane2, RIO_SIGNAL_T tlane3)
{
    SrioXtor *xtor = static_cast<SrioXtor*>(context);
#ifdef DEBUG
    msg(context);
    cout << "tlane0 = " << tlane0 << ", " <<
        "tlane1 = " << tlane1 << ", " <<
        "tlane2 = " << tlane2 << ", " <<
        "tlane3 = " << tlane3 << "." << endl;
#endif
    SrioPins pins = xtor->getPins();
    *(pins.tlane0) = tlane0;
    *(pins.tlane1) = tlane1;
    *(pins.tlane2) = tlane2;
    *(pins.tlane3) = tlane3;
    return RIO_OK;
}

int SrioXtor::msg(RIO_CONTEXT_T  context, char *user_Msg)
{
    SrioXtor *xtor = static_cast<SrioXtor*>(context);
    cout << "SRIO[" << xtor->getId() << "]: " <<
        xtor->getCurrentTime() << ": ";
    if (user_Msg != NULL)
        cout << user_Msg << endl;
    return RIO_OK;
}

int SrioXtor::extRegRead(RIO_CONTEXT_T context,
        RIO_CONF_OFFS_T config_Offset, unsigned long *data)
{
    msg(context, "Extended config read");
    return RIO_OK;
}

int SrioXtor::extRegWrite(RIO_CONTEXT_T context,
        RIO_CONF_OFFS_T config_Offset, unsigned long data)
{
    msg(context, "Extended config write");
    return RIO_OK;
}

int SrioXtor::requestReceived(RIO_CONTEXT_T context,
        RIO_REMOTE_REQUEST_T *packet_Struct)
{
    msg(context, "Request received");
    return RIO_OK;
}

int SrioXtor::responseReceived(RIO_CONTEXT_T context,
        RIO_RESPONSE_T *packet_Struct)
{
    msg(context, "Response received");
    return RIO_OK;
}

int main()
{
    RIO_SIGNAL_T flane[4] = { RIO_SIGNAL_0, RIO_SIGNAL_0,
                                RIO_SIGNAL_0, RIO_SIGNAL_0 };
    RIO_SIGNAL_T blane[4] = { RIO_SIGNAL_0, RIO_SIGNAL_0,
                                RIO_SIGNAL_0, RIO_SIGNAL_0 };
    SrioPins pe1Pins, pe2Pins;
    pe1Pins.tlane0 = pe2Pins.rlane0 = &flane[0];
    pe1Pins.tlane1 = pe2Pins.rlane1 = &flane[1];
    pe1Pins.tlane2 = pe2Pins.rlane2 = &flane[2];
    pe1Pins.tlane3 = pe2Pins.rlane3 = &flane[3];
    pe1Pins.rlane0 = pe2Pins.tlane0 = &blane[0];
    pe1Pins.rlane1 = pe2Pins.tlane1 = &blane[1];
    pe1Pins.rlane2 = pe2Pins.tlane2 = &blane[2];
    pe1Pins.rlane3 = pe2Pins.tlane3 = &blane[3];

    SrioXtor pe1(1, pe1Pins);
    SrioXtor pe2(2, pe2Pins);

    unsigned int t = 0, i;
    for (i = 0; i < 30000; i++, t++)
    {
        pe1.run(t);
        pe2.run(t);
    }

    // Start an IO request
    RIO_IO_REQ_T rq;
    rq.ttype = RIO_IO_NREAD;
    rq.prio = 0;
    rq.address = 64;
    rq.extended_Address = 0;
    rq.xamsbs = 0;
    rq.dw_Size = 8;
    rq.offset = 0;
    rq.byte_Num = 0;
    rq.data = NULL;
    rq.transp_Type = RIO_PL_TRANSP_16;
    rq.destId = 1;
    rq.trans_Id = 103;
    cout << "PE1: " << t << ": Starting a NREAD IO request" << endl;
    pe1.sendIoRequest(&rq);
    for (i = 0; i < 50000; i++, t++)
    {
        pe1.run(t);
        pe2.run(t);
    }

    return 0;
}

