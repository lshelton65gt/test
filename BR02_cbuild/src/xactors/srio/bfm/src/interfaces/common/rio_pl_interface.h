#ifndef RIO_PL_INTERFACE_H
#define RIO_PL_INTERFACE_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/common/rio_pl_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the logical layer
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_PL_PLLIGHT_INTERFACE_H
#include "rio_pl_pllight_interface.h"
#endif

/*
 * the function notify the instance about GSM request that shall head out.
 * if more priorital request is appear this function shall be invoke again.
 * tag value is assigned in environment and all requests have the same 
 * tag values area. Also it is used to bind request with its responce.
 */
typedef int (*RIO_PL_GSM_REQUEST)(
    RIO_HANDLE_T       handle,
    RIO_PL_GSM_TTYPE_T ttype,          /* requested transaction type  */
    RIO_GSM_REQ_T      *rq,
    RIO_UCHAR_T        sec_ID,            /* ignored for packet types other than 1 */
    RIO_UCHAR_T        sec_TID,
    RIO_TAG_T          trx_Tag,           /* user-assigned tag of the request */
    RIO_TR_INFO_T      transport_Info[],
    RIO_UCHAR_T        multicast_Size     /* count of recepients */
    );

/*
 * the function notify the instance about IO request that shall head out.
 * if more priorital request is appear this function shall be invoke again
 * tag value is assigned in environment and all requests have the same 
 * tag values area. Also it is used to bind request with its responce.
 */
typedef int (*RIO_PL_IO_REQUEST)( 
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
    RIO_TR_INFO_T  transport_Info
    );

/*
 * the function notify the instance about message passing request that shall
 * head out. if more priorital request is appear this function shall be invoke again
 * tag value is assigned in environment and all requests have the same 
 * tag values area. Also it is used to bind request with its responce.
 */
typedef int (*RIO_PL_MESSAGE_REQUEST)(
    RIO_HANDLE_T       handle,
    RIO_MESSAGE_REQ_T  *rq,
    RIO_TAG_T          trx_Tag             /* user-assigned tag of the request */
    );

/*
 * the function notify the instance about doorbell request that shall head out.
 * if more priorital request is appear this function shall be invoke again
 * tag value is assigned in environment and all requests have the same 
 * tag values area. Also it is used to bind request with its responce.
 */
typedef int (*RIO_PL_DOORBELL_REQUEST)(
    RIO_HANDLE_T        handle,
    RIO_DOORBELL_REQ_T  *rq,
    RIO_TAG_T           trx_Tag             /* user-assigned tag of the request */
    );


/* GDA: Bug#124 - Support for Data Streaming packet */
/*
 * the function notify the instance about DataStreaming request that shall head out.
 * if more priorital request is appear this function shall be invoke again
 * tag value is assigned in environment and all requests have the same 
 * tag values area. Also it is used to bind request with its responce.
 */
typedef int (*RIO_PL_DS_REQUEST)(
       	RIO_HANDLE_T   handle,
        RIO_DS_REQ_T   *rq,
        RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
        RIO_TR_INFO_T  transport_Info
    );
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
typedef int (*RIO_PL_FC_REQUEST)( 
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
    RIO_TR_INFO_T  transport_Info
    );
typedef int (*RIO_PL_CONGESTION)( 
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    );

typedef int (*RIO_PL_FC_TO_SEND)(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
    );
/* GDA: Bug#125 - Support for Flow control packet */


/*
 * the function notify the instance about CONFIGURATION request that shall head out.
 * if more priorital request is appear this function shall be invoke again
 * tag value is assigned in environment and all requests have the same 
 * tag values area. Also it is used to bind request with its responce.
 */
typedef int (*RIO_PL_CONFIGURATION_REQUEST)(
    RIO_HANDLE_T      handle,
    RIO_CONFIG_REQ_T  *rq,
    RIO_TAG_T         trx_Tag          /* user-assigned tag of the request */
    );


/* 
 * this callback is used to notify the environment what the request has been
 * accepted by PL and load into outbound buffer.
 */
typedef int (*RIO_PL_ACK_REQUEST)(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
    );

/*
 * the callback is invoked to get data payload (all or part) for heading out 
 * request. It is used if request contains data but corresponding field in
 * request data structure was clean (NULL pointer). This function will invoke
 * in this case each time when the logical time-out event has happened and 
 * the request has been retransmitted. Also if request is divided by misaligned 
 * engin for each paket will be separate call.
 */
typedef int (*RIO_PL_GET_REQ_DATA)(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
    );


/* GDA: Bug#124 - Support for Data Streaming packet */

/*
 * the callback is invoked to get data payload (all or part) for heading out 
 * request. It is used if request contains data but corresponding field in
 * request data structure was clean (NULL pointer). This function will invoke
 * in this case each time when the logical time-out event has happened and 
 * the request has been retransmitted. Also if request is divided by misaligned 
 * engin for each paket will be separate call.
 */
typedef int (*RIO_PL_GET_REQ_DATA_HW)(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_HW_T      *data_hw
		);

/* GDA: Bug#124 - Support for Data Streaming packet */

/*
 * the function is used to notify about responce that shall head out. It is invoked 
 * each time when more priority responce is available.
 * In general case, responce tag is from other space than request.
 */
typedef int (*RIO_PL_SEND_RESPONSE)(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
    );

/*
 * the callback is invoked when heading out responce has been accepted by PL and 
 * loaded into outbound buffer
 */
typedef int (*RIO_PL_ACK_RESPONSE)(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
    );

/*
 * the callback is invoked to get data payload (all or part) for heading out 
 * response. It is used if response contains data but corresponding field in
 * response data structure was clean (NULL pointer). 
 */
typedef int (*RIO_PL_GET_RESP_DATA)(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
    );

/* 
 * this callback notifies the environment about overstep the limit value of
 * retries after time-out event (logical time-out). It means that corresponding
 * request has been dropped from outbound buffer
 */
typedef int (*RIO_PL_REQ_TIME_OUT)(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
    );

/*
 * notify the environment about received external request. 
 * tag is assigned by PL 
 */
typedef int (*RIO_PL_REMOTE_REQUEST)(
    RIO_CONTEXT_T        context, 
    RIO_TAG_T            tag, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_TR_INFO_T        transport_Info
    );

/*
 * notify the environment about responce, which is corresponding to an issued 
 * request.
 * tag value is the same as corresponding request tag.
 * unrequested responses are dropped.
 * No any acknowledgment are necessary. After notification buffer is deallocated.
 */
typedef int (*RIO_PL_REMOTE_RESPONSE)(
    RIO_CONTEXT_T  context, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
    );

/*
 * the function notify PL what received request has been accepted by the 
 * environment. After it buffer can be deallocated
 */
typedef int (*RIO_PL_ACK_REMOTE_REQ)(
    RIO_HANDLE_T handle, 
    RIO_TAG_T    tag
    );


/*  the function sends packet that is specified in txrx model way
    please note that in case of this usage you can corrupt normal 
    way of packet sending
*/
typedef int (*RIO_PL_PACKET_STRUCT_REQUEST)(
    RIO_HANDLE_T       handle, 
    RIO_MODEL_PACKET_STRUCT_RQ_T *rq, 
    RIO_TAG_T          trx_Tag);







#endif /* RIO_PL_INTERFACE_H */

