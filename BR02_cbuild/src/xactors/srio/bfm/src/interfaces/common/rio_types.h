#ifndef RIO_TYPES_H
#define RIO_TYPES_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/common/rio_types.h $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    RapidIO Models types description
*
* Notes:        
*
******************************************************************************/
typedef void*         RIO_HANDLE_T; /* instance handle */
typedef void*         RIO_CONTEXT_T; /* callback context */

typedef unsigned long RIO_WORD_T;
typedef unsigned long RIO_ADDRESS_T;
typedef unsigned char RIO_UCHAR_T;
typedef unsigned char RIO_BYTE_T;
typedef unsigned int  RIO_TR_INFO_T;     /* transport info for appropriate request routing via the fabric */
typedef unsigned int  RIO_DB_INFO_T;     /* doorbell info for doorbell requests */
typedef unsigned long RIO_CONF_OFFS_T;   /* configuration register space offset */
typedef unsigned long RIO_SH_MASK_T;     /* memory directory sharing mask */
typedef unsigned int  RIO_TAG_T;
/* GDA: Bug#124 - Support for Data Streaming packet */
typedef unsigned short RIO_HW_T; 
typedef unsigned short RIO_HW_SIZE; 
typedef unsigned short RIO_DS_LENGTH_T;  /*LENGTH for data streaming packet*/
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
#define RIO_MAX_DELAY_ORPHAND_XON     100        /*maximal Delay  for orphand xon */
/* GDA: Bug#125 - Support for Flow control packet */


/* RIO model calls return values */
#define RIO_OK             0  /* function result is ok */
#define RIO_ERROR          1
#define RIO_RETRY          2  /* the request should be retried (due to buffer shortage or collision condition) */
#define RIO_PARAM_INVALID  3  /* some of the passed parameters have invalid values - see error message */
#define RIO_NO_DATA           4  /* no awaited data is passed through the callback */

/* GDA: Bug#125 - Support for Flow control packet */
#define RIO_CONGES    5  /* CONGESTION is there at reciever side */
/* GDA: Bug#125  */


#define RIO_MAX_COH_DOMAIN_SIZE         16   /* maximal number of the participants in the cache   */
                                             /* coherency domain                                  */

/* GDA: Bug#124 - Support for Data Streaming packet */
typedef struct {
    RIO_UCHAR_T     flow_ID;  /* flow ID for the request packet */
    RIO_TR_INFO_T   src_ID;   /* device ID of the requestor     */
} RIO_DS_INFO_T;
/* GDA: Bug#124 - Support for Data Streaming packet */

typedef struct {
    RIO_WORD_T ms_Word;   /* most-significat word;  bits 0-31 in big-endian format  */
    RIO_WORD_T ls_Word;   /* less-significant word; bits 32-63 in big-endian format */
} RIO_DW_T;

typedef enum {
    RIO_FALSE = 0,
    RIO_TRUE = 1
} RIO_BOOL_T;

/* width of transport info  */
typedef enum {
    RIO_PL_TRANSP_16 = 0,
    RIO_PL_TRANSP_32 = 1
} RIO_PL_TRANSP_TYPE_T;

typedef enum {
    RIO_GSM_READ_SHARED = 0, 
    RIO_GSM_INSTR_READ  = 1, 
    RIO_GSM_READ_TO_OWN = 2, 
    RIO_GSM_CASTOUT     = 3, 
    RIO_GSM_DC_I        = 4, 
    RIO_GSM_TLB_IE      = 5, 
    RIO_GSM_TLB_SYNC    = 6, 
    RIO_GSM_IC_I        = 7, 
    RIO_GSM_IO_READ     = 8,
    RIO_GSM_FLUSH       = 9
} RIO_GSM_TTYPE_T;

/* the constants in enum are used for entering collision tables, because of that they are assigned explicitly */
typedef enum {
    RIO_PL_GSM_READ_HOME         = 0, /* the first 10 constants in this enumeration are equal to    */ 
    RIO_PL_GSM_IREAD_HOME        = 1, /* values in the RIO_GSM_TTYPE_T enum. It is done to make GSM */
    RIO_PL_GSM_READ_TO_OWN_HOME  = 2, /* transaction servicing more easy                            */
    RIO_PL_GSM_CASTOUT           = 3,
    RIO_PL_GSM_DKILL_HOME        = 4,
    RIO_PL_GSM_TLBIE             = 5,
    RIO_PL_GSM_TLBSYNC           = 6,
    RIO_PL_GSM_IKILL_HOME        = 7,
    RIO_PL_GSM_IO_READ_HOME      = 8,
    RIO_PL_GSM_FLUSH_WO_DATA     = 9,
    RIO_PL_GSM_FLUSH_WITH_DATA   = 10,
    RIO_PL_GSM_READ_OWNER        = 11,
    RIO_PL_GSM_READ_TO_OWN_OWNER = 12,
    RIO_PL_GSM_IO_READ_OWNER     = 13,
    RIO_PL_GSM_DKILL_SHARER      = 14,
    RIO_PL_GSM_IKILL_SHARER      = 15
} RIO_PL_GSM_TTYPE_T;


typedef enum {
    RIO_IO_NREAD = 0, 
    RIO_IO_NWRITE, 
    RIO_IO_SWRITE, 
    RIO_IO_NWRITE_R,
    RIO_IO_ATOMIC_INC,
    RIO_IO_ATOMIC_DEC,
    RIO_IO_ATOMIC_TSWAP,
    RIO_IO_ATOMIC_SET,
    RIO_IO_ATOMIC_CLEAR,
    RIO_IO_ATOMIC_SWAP		/* GDA: Bug#133 - Support for TType C of packet type 5 */
} RIO_IO_TTYPE_T;


typedef enum {
    RIO_CONF_READ = 0,
    RIO_CONF_WRITE,
    RIO_CONF_PORT_WRITE = 4 
} RIO_CONF_TTYPE_T;

typedef enum {
    RIO_LR_RETRY = 0,
    RIO_LR_EXCLUSIVE,
    RIO_LR_SHARED,
    RIO_LR_OK
} RIO_LOCAL_RTYPE_T;

typedef enum {
    RIO_REQ_DONE = 0,
    RIO_REQ_ERROR,     /* the target device has signalled an unrecoverable error */
    RIO_REQ_CANCEL     /* transaction was cancelled due to collision resolution  */
} RIO_REQ_RESULT_T;

typedef enum {
    RIO_LOCAL_READ = 0,
    RIO_LOCAL_READ_TO_OWN,
    RIO_LOCAL_READ_LATEST,
    RIO_LOCAL_DKILL,
    RIO_LOCAL_IKILL,
    RIO_LOCAL_TLBIE,
    RIO_LOCAL_TLBSYNC,
    RIO_LOCAL_NONCOHERENT
} RIO_LOCAL_TTYPE_T;

typedef enum {
    RIO_SNOOP_HIT = 0,
    RIO_SNOOP_MISS
} RIO_SNOOP_RESULT_T; 

/* values for transaction types defined 
 *accordingly to the "RIO Interconnect" specification 
 */
typedef enum {
    RIO_INTERV_REQUEST    = 1,
    RIO_NONINTERV_REQUEST = 2,
    RIO_WRITE             = 5,
    RIO_STREAM_WRITE      = 6,

    /* GDA: Bug#125 - Support for Type7 flow control packet
       CCP - Congestion Control Packet */ 
    RIO_FC		  = 7,
    /* GDA: Bug#125 - Support for Type7 flow control packet*/

    RIO_MAINTENANCE       = 8,

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_DS         	  = 9,  
    /* GDA: Bug#124 - Support for Data Streaming packet */


    RIO_DOORBELL          = 10,
    RIO_MESSAGE           = 11,
    RIO_RESPONCE          = 13
}RIO_FTYPE_T;             

/* values for ftypes of the TTYPE=RIO_INTERV_REQUEST
 * transactions defined accordingly to the "RIO Interconnect" specification 
 */
typedef enum {
    RIO_INTERV_READ_OWNER        = 0,
    RIO_INTERV_READ_TO_OWN_OWNER = 1,
    RIO_INTERV_IO_READ_OWNER     = 2
} RIO_INTERV_TTYPE_T;

/* values for ttypes of the FTYPE=RIO_NONINTERV_REQUEST 
 * transactions defined accordingly to the "RIO Interconnect" specification 
 */
typedef enum {
    RIO_NONINTERV_READ_HOME        = 0,
    RIO_NONINTERV_READ_TO_OWN_HOME = 1,
    RIO_NONINTERV_IO_READ_HOME     = 2,
    RIO_NONINTERV_DKILL_HOME       = 3,
    RIO_NONINTERV_NREAD            = 4,
    RIO_NONINTERV_IKILL_HOME       = 5,
    RIO_NONINTERV_TLBIE            = 6,
    RIO_NONINTERV_TLBSYNC          = 7,
    RIO_NONINTERV_IREAD_HOME       = 8,
    RIO_NONINTERV_FLUSH_WO_DATA    = 9,
    RIO_NONINTERV_IKILL_SHARER     = 10,
    RIO_NONINTERV_DKILL_SHARER     = 11,
    RIO_NONINTERV_ATOMIC_INC       = 12,
    RIO_NONINTERV_ATOMIC_DEC       = 13,
    RIO_NONINTERV_ATOMIC_SET       = 14,
    RIO_NONINTERV_ATOMIC_CLEAR     = 15
} RIO_NONINTERV_TTYPE_T;

/* values for ftypes of the TTYPE=RIO_WRITE 
 * transactions defined accordingly to the "RIO Interconnect" specification 
 */
typedef enum {
    RIO_WRITE_CASTOUT         = 0,
    RIO_WRITE_FLUSH_WITH_DATA = 1,
    RIO_WRITE_NWRITE          = 4,
    RIO_WRITE_NWRITE_R        = 5,
    RIO_WRITE_ATOMIC_SWAP     = 12,	/* GDA: Bug#133 - Support for TType C of packet type 5 */
    RIO_WRITE_ATOMIC_TSWAP    = 14
} RIO_WRITE_TTYPE_T;

/* values for ftypes of the TTYPE=RIO_MAINTENENCE 
 * transactions defined accordingly to the "RIO Interconnect" specification 
 */

typedef enum {
    RIO_MAINTENANCE_CONF_READ      = 0,
    RIO_MAINTENANCE_CONF_WRITE     = 1,
    RIO_MAINTENANCE_READ_RESPONCE  = 2,
    RIO_MAINTENANCE_WRITE_RESPONCE = 3,
    RIO_MAINTENANCE_PORT_WRITE     = 4
} RIO_MAINTENANCE_TTYPE_T;

/* values for transaction field of the responce  
 * transactions defined accordingly to the "RIO Interconnect" specification 
 */
typedef enum {
    RIO_RESPONCE_WO_DATA   = 0,
    RIO_RESPONCE_MESSAGE   = 1,
    RIO_RESPONCE_WITH_DATA = 8,
    RIO_RESPONCE_INTERVENTION,   /* will force PL to forward response to intervention engine */
                                 /* and produce INTERV with data and DATA ONLY with data     */
                                 /* is to be used for the READ_OWNER and READ_TO_OWN_OWNER   */
    RIO_RESPONCE_INTERVENTION_WO_DATA   /* will force PL to forward response to intervention engine */
                                        /* and produce INTERV without data and DATA ONLY with data  */
                                        /* is to be used for the IO_READ_OWNER and READ_TO_OWN_OWNER*/
} RIO_RESPONCE_TRANSACTION_T;

/* definitions for the status field in the responce packets */
typedef enum {
    RIO_RESPONCE_STATUS_DONE           = 0,
    RIO_RESPONCE_STATUS_DATA_ONLY      = 1,
    RIO_RESPONCE_STATUS_NOT_OWNER      = 2,
    RIO_RESPONCE_STATUS_RETRY          = 3,
    RIO_RESPONCE_STATUS_INTERV         = 4,
    RIO_RESPONCE_STATUS_DONE_INTERV    = 5,
    RIO_RESPONCE_STATUS_ERROR          = 7,
    /* user-defined value in RIO spec;  in the model means that request could not
       be serviced because its target doesn't have local memory */    
    RIO_RESPONCE_STATUS_NO_MEMORY      = 12, 
    /* user-defined value in RIO spec;  in the model means that request could not
       be serviced because its target doesn't support corresponding trasaction
       through configuration in the Destination transactions CSR */    
    RIO_RESPONCE_STATUS_CANNOT_SERVICE = 14
} RIO_RESPONCE_STATUS_T; 

/* this enum defines values to be used by the IO and GSM routing functions */
typedef enum {
    RIO_ROUTE_TO_LOCAL = 0,
    RIO_ROUTE_TO_REMOTE    
} RIO_ROUTE_T;

/* this type represent routing algorithm */
typedef enum {
    RIO_TABLE_BASED_ROUTING = 0,
    RIO_INDEX_BASED_ROUTING,
    RIO_SHIFT_BASED_ROUTING
} RIO_ROUTING_METHOD_T;

typedef enum {
    RIO_MEM_READ = 0,
    RIO_MEM_WRAP_READ,  /* read shall be wrapped when reaches address aligned to size of read request */
    RIO_MEM_WRITE,
    RIO_MEM_ATOMIC_INC,
    RIO_MEM_ATOMIC_DEC,
    RIO_MEM_ATOMIC_SET,
    RIO_MEM_ATOMIC_CLEAR,
    RIO_MEM_ATOMIC_TSWAP,
    RIO_MEM_ATOMIC_SWAP		/* GDA: Bug#133 - Support for TType C of packet type 5 */

} RIO_MEMORY_REQ_TYPE_T;


typedef struct {
    RIO_UCHAR_T     prio;              /* priority of the transaction */
    RIO_ADDRESS_T   address;           /* double-word aligned address */
    RIO_ADDRESS_T   extended_Address;  /* extended address            */
    RIO_UCHAR_T     xamsbs;            /* xamsbs                      */
    RIO_UCHAR_T     dw_Size;           /* size of transaction in double words, meaningful in case of write */
    RIO_UCHAR_T     offset;            /* offest of the data; meaningful only if dw_Size == 1 */
    RIO_UCHAR_T     byte_Num;          /* number of bytes starting from the offset, should not cross dw boundary */
    RIO_DW_T        *data;             /* 1, 2, 4, 8, 16 or 32 double-words and not exceeding 
                                        coherence granule size or cache line boundary; 0 if read */
/*    RIO_BOOL_T      ext_Address_Valid;*/ /* extended address valid flag */
/*    RIO_BOOL_T      ext_Address_16;   */ /* is extended address 16 flag */
    RIO_PL_TRANSP_TYPE_T transp_Type; /* type of the transport info in the remote packet */
} RIO_GSM_REQ_T;



typedef struct {
    RIO_IO_TTYPE_T ttype;              /* type of the transaction     */
    RIO_UCHAR_T    prio;               /* priority of the transaction */
    RIO_ADDRESS_T  address;            /* double-word aligned address */
    RIO_ADDRESS_T  extended_Address;   /* extended address field      */
    RIO_UCHAR_T    xamsbs;             /* xamsbs field for the transaction */
    RIO_UCHAR_T    dw_Size;            /* size of transaction in double words */
    RIO_UCHAR_T    offset;             /* offest of the data; meaningful only if dw_Size == 1 */
    RIO_UCHAR_T    byte_Num;           /* number of bytes starting from the offset, should not cross dw boundary */
    RIO_DW_T       *data;              /* pointer to the data area for the transaction */
/*    RIO_BOOL_T      ext_Address_Valid;*/ /* extended address valid flag */
/*    RIO_BOOL_T      ext_Address_16;   */ /* is extended address 16 flag */
    RIO_PL_TRANSP_TYPE_T transp_Type; /* type of the transport info in the remote packet */
    RIO_TAG_T      destId;            /* Bug#104 passing destination-id from the testbench */
    unsigned  char trans_Id;          /* Bug#137 passing transaction-id from the testbench */
} RIO_IO_REQ_T;

typedef struct {
    RIO_TR_INFO_T  transport_Info;     /* transport info for the transaction packet routing */
    RIO_UCHAR_T    prio;               /* priority of the transaction               */
    RIO_UCHAR_T    msglen;             /* number of the segments comprising message */
    RIO_UCHAR_T    ssize;              /* size of the one segment                   */
    RIO_UCHAR_T    letter;             /* number of the letter for the message request */
    RIO_UCHAR_T    mbox;               /* number of the message box for the message request */
    RIO_UCHAR_T    msgseg;             /* number of the current message segment */
    RIO_UCHAR_T    dw_Size;            /* size of transaction in double words   */
    RIO_DW_T       *data;              /* pointer to the data area for the transaction */

    RIO_PL_TRANSP_TYPE_T transp_Type; /* type of the transport info in the remote packet */
} RIO_MESSAGE_REQ_T;    

typedef struct {
    RIO_UCHAR_T    prio;               /* priority of the transaction */
    RIO_TR_INFO_T  transport_Info;     /* transport info for the transaction packet routing */
    RIO_DB_INFO_T  doorbell_Info;      /* doorbell_Info field for the transaction */

    RIO_PL_TRANSP_TYPE_T transp_Type; /* type of the transport info in the remote packet */
} RIO_DOORBELL_REQ_T;

typedef struct {
    RIO_UCHAR_T       prio;            /* priority of the transaction */
    RIO_TR_INFO_T     transport_Info;  /* transport info for the transaction packet routing */
    RIO_UCHAR_T       hop_Count;       /* meaningful only if table-based routing is enabled */
    RIO_CONF_TTYPE_T  rw;              /* type of the transaction */
    RIO_CONF_OFFS_T   config_Offset;   /* don't care in case of the PORT WRITE request */
    RIO_UCHAR_T       dw_Size;         /* can not exceed 8 doublewords */
    RIO_UCHAR_T       sub_Dw_Pos;      /* in case of sub-doubleword access, the data position inside dw:
                                          0 - first half-dw, 1 - second, else - whole dw is being accessed */
    RIO_DW_T          *data;           /* pointer to the data are afor the transaction */

    RIO_PL_TRANSP_TYPE_T transp_Type; /* type of the transport info in the remote packet */
} RIO_CONFIG_REQ_T;

/* GDA: Bug#124 - Support for Data Streaming packet */
typedef struct{
   RIO_UCHAR_T       cos;              /*class of service*/
   RIO_BOOL_T  	     s_Ds;             /*indicates start of segment*/
   RIO_BOOL_T  	     e_Ds;   	       /*end of segment*/
   RIO_UCHAR_T 	     offset; 	       /*offset of the data; meaningful only if hw_size==1*/
   RIO_UCHAR_T       byte_Num;         /* number of bytes starting from the offset, should not cross hw boundary */
   RIO_HW_T    	     *data_hw;         /*pointer to the data area for the transaction */
   RIO_HW_SIZE       hw_Size;  	       /*size of transaction in half words*/
   RIO_BOOL_T        o;		       /* If set payload has an odd no.of halfwords */
   RIO_BOOL_T        p; 	       /* If set pad byte was used to pad to a half word boundary*/
   RIO_DS_LENGTH_T   length;           /* in bytes of the segmented PDU */
   RIO_DS_INFO_T     stream_ID;        /* traffic stream identifier*/
   RIO_UCHAR_T 	     mtu ;	       /* Maximun transmission unit for segments */
   RIO_UCHAR_T       prio;             /* priority of the transaction */
   RIO_PL_TRANSP_TYPE_T transp_Type;   /* type of the transport info in the remote packet */
} RIO_DS_REQ_T;
/* GDA: Bug#124 - Support for Data Streaming packet */

/************************************************************ 
 GDA: Bug#125 
 Support for Type 7 (flow control packet) 
*************************************************************/
typedef struct {
    RIO_UCHAR_T		x_On_Off;	/* Stop/Start issuing req for specified & lower priority 
					   transaction request flowP) 				*/
    RIO_UCHAR_T		flow_ID;	/* Highest priority affected transaction request flow 	*/
    RIO_ADDRESS_T	dest_ID;	/* Indicates which end pt the CCP is destined for (srcID
					   of the packet that caused the generation of the CCP) */
    RIO_ADDRESS_T	tgtdest_ID;	/* Combined with the flowID field, indicates which 
					   transaction request flows need to be acted upon 
					   (destinationID field of the packet which caused the 
					   generation of the CCP)				*/
    RIO_UCHAR_T		soc;		/* Source Of Congestion from Switch / End point	*/
    RIO_UCHAR_T         prio;              /* priority of the transaction */
    RIO_UCHAR_T         offset;             /* offest of the data; meaningful only if dw_Size == 1 */
    RIO_PL_TRANSP_TYPE_T transp_Type;  /* type of the transport info in the remote packet */
    
} RIO_FC_REQ_T;
/* GDA: Bug#125 - Support for Flow control packet */


typedef enum {
    RIO_SNOOP_READ_OWNER = 0,
    RIO_SNOOP_READ_TO_OWN_OWNER,
    RIO_SNOOP_IO_READ_OWNER,
    RIO_SNOOP_READ_HOME,
    RIO_SNOOP_READ_TO_OWN_HOME,
    RIO_SNOOP_IO_READ_HOME,
    RIO_SNOOP_DKILL_HOME,
    RIO_SNOOP_IREAD_HOME,
    RIO_SNOOP_DKILL_SHARER,
    RIO_SNOOP_IKILL,
    RIO_SNOOP_TLBIE,
    RIO_SNOOP_TLBSYNC,
    RIO_SNOOP_CASTOUT,
    RIO_SNOOP_FLUSH,

    RIO_SNOOP_NREAD, 
    RIO_SNOOP_NWRITE, 
    RIO_SNOOP_SWRITE, 
    RIO_SNOOP_NWRITE_R,
    RIO_SNOOP_ATOMIC
} RIO_SNOOP_TTYPE_T;

typedef struct {
    RIO_ADDRESS_T      address;          /* address for the snoop oeration           */
    RIO_ADDRESS_T      extended_Address; /* extended address for the snoop operation */
    RIO_UCHAR_T        xamsbs;           /* xamsbs address field                     */
    RIO_LOCAL_TTYPE_T  lttype;           /* local type from protocol description     */
    RIO_SNOOP_TTYPE_T  ttype;            /* rio incoming packet ttype field          */
} RIO_SNOOP_REQ_T;

typedef struct {
    RIO_UCHAR_T    msglen;  /* number of the segments comprising message */
    RIO_UCHAR_T    ssize;   /* standard size of the segment (decoded by model to doublewords)*/
    RIO_UCHAR_T    letter;  /* number of the letter for the message      */
    RIO_UCHAR_T    mbox;    /* number of the message box                 */
    RIO_UCHAR_T    msgseg;  /* number of the message segment             */
    RIO_UCHAR_T    dw_Size; /* size of the data payload in the current segment */
    RIO_DW_T       *data;   /* pointer to the data area for the transaction */
} RIO_REMOTE_MP_REQ_T;



/* request packet send to physical layer. */


/* request to send response packet to physical layer */
typedef struct {
    RIO_UCHAR_T   prio;        /* priority of the response packet    */
    RIO_UCHAR_T   ftype;       /* format type of the responce packet */
    RIO_UCHAR_T   transaction; /* transaction field of the response packet */
    RIO_UCHAR_T   status;      /* status field of the resposne packet */
    RIO_TR_INFO_T tr_Info;     /* transport info for the response packet routing*/
    RIO_TR_INFO_T src_ID;      /* device ID of the response originator */
    RIO_UCHAR_T   target_TID;  /* this field is letter_mbox_msgseg for message responses */
    RIO_TR_INFO_T sec_ID;      /* used for intervention responces by the Intervention Engine in the PE */
    RIO_UCHAR_T   sec_TID;     /* used for intervention responces by the Intervention Engine in the PE */
    RIO_UCHAR_T   dw_Num;      /* number of data doublewords which are supplied with this response */
    RIO_DW_T      *data;       /* if not NULL, user supplies data in the request; otherwise data will be retrieved 
                               by calling RIO_PH_GET_RESP_DATA */
    RIO_PL_TRANSP_TYPE_T transp_Type; /* type of the transport info in the remote packet */
} RIO_RESPONSE_T;




/* rio physical layer notifies that a new request packet has arrived via 8LP-EP link */

/* those fields not applicable to some packet types will be zeroed */
typedef struct {
    RIO_UCHAR_T      target_TID;  /* transaction ID for the request packet         */
    RIO_TR_INFO_T    src_ID;      /* device ID of the requestor                    */
    RIO_UCHAR_T      prio;        /* priority of the request                       */
    RIO_UCHAR_T      wdptr;       /* data size encoded accordingly to the RIO spec */
    RIO_UCHAR_T      rdsize;
    RIO_UCHAR_T      format_Type; /* format of the request                */
    RIO_UCHAR_T      ttype;       /* transaction type of the request      */
    RIO_ADDRESS_T    address;     /* address of the request               */
    RIO_ADDRESS_T    extended_Address; /* extended address of the request */
    RIO_UCHAR_T      xamsbs;      /* xamsbs field of the request          */
    RIO_TR_INFO_T    sec_ID;      /* secondary device ID of the request   */
    RIO_UCHAR_T      sec_TID;     /* secondary transaction ID of the request */
    RIO_UCHAR_T      mbox;        /* message box for the message request  */
    RIO_UCHAR_T      letter;      /* letter number of the request         */
    RIO_UCHAR_T      msgseg;      /* number of the message segment        */
    RIO_UCHAR_T      ssize;       /* size of the message segment          */
    RIO_UCHAR_T      msglen;      /* number of the segments in the message */
    RIO_DB_INFO_T    doorbell_Info; /* doorbell info of the doorbell request */
    RIO_CONF_OFFS_T  offset;      /* configuration offset                 */
    
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_DS_INFO_T    stream_ID;   /* Stream id it is the combination of device id and transaction id */  /*bug-124*/
    RIO_BOOL_T       o;		  /* If set payload has an odd no.of halfwords */
    RIO_BOOL_T       p; 	  /* If set pad byte was used to pad to a half word boundary*/
    RIO_DS_LENGTH_T  length;      /* in bytes of the segmented PDU */
    RIO_BOOL_T       s_Ds;        /*indicates start of segment*/
    RIO_BOOL_T       e_Ds;   	  /*end of segment*/
    RIO_UCHAR_T      cos;         /*class of service*/
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_UCHAR_T	     x_On_Off;	 /* Stop/Start issuing req for specified & lower priority 
				   transaction request flowP) 				*/
    RIO_UCHAR_T	     flow_ID;	 /* Highest priority affected transaction request flow 	*/
    RIO_ADDRESS_T    dest_ID;	 /* Indicates which end pt the CCP is destined for (srcID
					   of the packet that caused the generation of the CCP) */
    RIO_ADDRESS_T    tgtdest_ID; /* Combined with the flowID field, indicates which 
				   transaction request flows need to be acted upon 
				   (destinationID field of the packet which caused the 
				   generation of the CCP)				*/
    RIO_UCHAR_T	     soc;        /* Source Of Congestion from Switch / End point	*/
    RIO_BOOL_T       is_congestion; 
    /* GDA: Bug#125 - Support for Flow control packet */

    
} RIO_REQ_HEADER_T;


typedef struct {
    RIO_UCHAR_T          packet_Type; /* this field selects packet type - from 1 to 12 and allows to understand which
                                         header data structure fields are valid for this packet */
    RIO_REQ_HEADER_T     *header; 
    RIO_UCHAR_T          dw_Num;      /* number of received double words for the transaction   */
    RIO_DW_T             *data;       /* data is supplied only if required by the request type */
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_HW_T             *data_hw;    /* data is supplied only if required by the request type */
    RIO_UCHAR_T          hw_Num;      /* number of received half words for the transaction   */
    RIO_HW_T             *ds_data;    /* data is supplied only if required by the request type */
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_TRANSP_TYPE_T transp_Type; /* type of the transport info in the remote packet */
} RIO_REMOTE_REQUEST_T;




typedef struct {
    RIO_TR_INFO_T target_ID;
    RIO_UCHAR_T   target_Port_Number;
} RIO_ROUTE_TABLE_ENTRY_T;




typedef struct {
    RIO_UCHAR_T   ext_Address_Support; /* support of the extended addresseses encoded accordingly */
                                       /* to specification (bits 29-31 in PE Features)            */
    RIO_BOOL_T    is_Bridge;           /* bit 0 in PE Features CAR                                */
    RIO_BOOL_T    has_Memory;          /* bit 1 in PE Features CAR                                */
    RIO_BOOL_T    has_Processor;       /* bit 2 in PE Features CAR                                */
    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_BOOL_T    has_Fcp;             /* bit 24 in PE Features CAR 				  */
    RIO_BOOL_T    coh_Granule_Size_32; /* size of coherence granule - 32 or 64 bytes */
    RIO_UCHAR_T   coh_Domain_Size;     /* size of coherence domain - 2, 4, 16 participants allowed */

    /* parameters necessary to set CARs */
    RIO_WORD_T    device_Identity;
    RIO_WORD_T    device_Vendor_Identity;
    RIO_UCHAR_T   device_Rev;
    RIO_UCHAR_T   device_Minor_Rev;
    RIO_UCHAR_T   device_Major_Rev;
    RIO_WORD_T    assy_Identity;   
    RIO_WORD_T    assy_Vendor_Identity;
    RIO_WORD_T    assy_Rev;
   
    RIO_WORD_T    entry_Extended_Features_Ptr; /*pointer to the first entry of ext.feat. list (16-31 bits in Assembly Info CAR)*/
    RIO_WORD_T    next_Extended_Features_Ptr;    /*ponter to the next ext. feat. block (0-15 bits in Port Maint.Block Header 0)*/

    RIO_WORD_T    source_Trx; 
    RIO_WORD_T    dest_Trx;        

    RIO_BOOL_T    mbox1;
    RIO_BOOL_T    mbox2;
    RIO_BOOL_T    mbox3;
    RIO_BOOL_T    mbox4;
     /*GDA Bug #123 : Support for extended mailbox(xmbox) feature */
    RIO_BOOL_T    mbox5;
    RIO_BOOL_T    mbox6;
    RIO_BOOL_T    mbox7;
    RIO_BOOL_T    mbox8;
    RIO_BOOL_T    mbox9;
    RIO_BOOL_T    mbox10;
    RIO_BOOL_T    mbox11;
    RIO_BOOL_T    mbox12;
    RIO_BOOL_T    mbox13;
    RIO_BOOL_T    mbox14;
    RIO_BOOL_T    mbox15;
    RIO_BOOL_T    mbox16;
    RIO_BOOL_T    mbox17;
    RIO_BOOL_T    mbox18;
    RIO_BOOL_T    mbox19;
    RIO_BOOL_T    mbox20;
    RIO_BOOL_T    mbox21;
    RIO_BOOL_T    mbox22;
    RIO_BOOL_T    mbox23;
    RIO_BOOL_T    mbox24;
    RIO_BOOL_T    mbox25;
    RIO_BOOL_T    mbox26;
    RIO_BOOL_T    mbox27;
    RIO_BOOL_T    mbox28;
    RIO_BOOL_T    mbox29;
    RIO_BOOL_T    mbox30;
    RIO_BOOL_T    mbox31;
    RIO_BOOL_T    mbox32;
    RIO_BOOL_T    mbox33;
    RIO_BOOL_T    mbox34;
    RIO_BOOL_T    mbox35;
    RIO_BOOL_T    mbox36;
    RIO_BOOL_T    mbox37;
    RIO_BOOL_T    mbox38;
    RIO_BOOL_T    mbox39;
    RIO_BOOL_T    mbox40;
    RIO_BOOL_T    mbox41;
    RIO_BOOL_T    mbox42;
    RIO_BOOL_T    mbox43;
    RIO_BOOL_T    mbox44;
    RIO_BOOL_T    mbox45;
    RIO_BOOL_T    mbox46;
    RIO_BOOL_T    mbox47;
    RIO_BOOL_T    mbox48;
    RIO_BOOL_T    mbox49;
    RIO_BOOL_T    mbox50;
    RIO_BOOL_T    mbox51;
    RIO_BOOL_T    mbox52;
    RIO_BOOL_T    mbox53;
    RIO_BOOL_T    mbox54;
    RIO_BOOL_T    mbox55;
    RIO_BOOL_T    mbox56;
    RIO_BOOL_T    mbox57;
    RIO_BOOL_T    mbox58;
    RIO_BOOL_T    mbox59;
    RIO_BOOL_T    mbox60;
    RIO_BOOL_T    mbox61;
    RIO_BOOL_T    mbox62;
    RIO_BOOL_T    mbox63;
    RIO_BOOL_T    mbox64;

    /* GDA : Bug #123 Support for Extended Mailbox Feature */
 
    RIO_BOOL_T    has_Doorbell;

    RIO_UCHAR_T   pl_Inbound_Buffer_Size;
    RIO_UCHAR_T   pl_Outbound_Buffer_Size;

    RIO_UCHAR_T   ll_Inbound_Buffer_Size;
    RIO_UCHAR_T   ll_Outbound_Buffer_Size;

    RIO_BOOL_T    ack_Delay_Enable;/* GDA: Description- '1' to enable Random Delay */
    RIO_WORD_T    ack_Delay_Min;   /* GDA: Description- Minimum Random Delay */
    RIO_WORD_T    ack_Delay_Max;   /* GDA: Description- Maximum Random Delay */

    /*Bug: 43 Fix*/
    RIO_BOOL_T    gda_Tx_Illegal_Packet;
    RIO_BOOL_T    gda_Rx_Illegal_Packet;
    RIO_UCHAR_T   tx_Illegal_Pnacc_Resend_Cnt; /* Pnacc resend counter for Tx_Illegal_Packet */   
    /*Bug: 43 Fix*/

    /* GDA: Support for Bug#131 */
    RIO_WORD_T	 RIO_Link_Valid_To1_I;
    /* GDA Bug#131 */
      /* GDA: Bug#138 */
    RIO_BOOL_T	  resp_Packet_Delay_Enable;/* GDA: To delay the res pkt.  RIO_FALSE by default */
    RIO_WORD_T    resp_Packet_Delay_Cntr;/* GDA: Delay Counter.  '0' by default */
    RIO_BOOL_T    resp_Stomp_Needed; /* GDA: To Discard a Packet */
     /* GDA : Bug#138 */ 
} RIO_MODEL_INST_PARAM_T;


typedef struct {
    unsigned int link_Number; /* Number of links switch model has */
    
    /* parameters necessary to set CARs */
    RIO_WORD_T    device_Identity;
    RIO_WORD_T    device_Vendor_Identity;
    RIO_UCHAR_T   device_Rev;
    RIO_UCHAR_T   device_Minor_Rev;
    RIO_UCHAR_T   device_Major_Rev;
    RIO_WORD_T    assy_Identity;   
    RIO_WORD_T    assy_Vendor_Identity;
    RIO_WORD_T    assy_Rev;

    RIO_WORD_T    entry_Extended_Features_Ptr;

    /* GDA: Support for Bug#131 */
    RIO_WORD_T	  RIO_Link_Valid_To1_I;
    /* GDA Bug#131 */

} RIO_SWITCH_INST_PARAM_T;




/*
 * this type represents general packet struct for txrxm  packet send request
 */
typedef struct {
    RIO_UCHAR_T     ack_ID;
    RIO_UCHAR_T     prio;
    RIO_UCHAR_T     rsrv_Phy_1;
    RIO_UCHAR_T     rsrv_Phy_2;
    RIO_UCHAR_T     tt;
    RIO_UCHAR_T     ftype;
    RIO_TR_INFO_T   transport_Info;
    RIO_UCHAR_T     ttype;
    RIO_UCHAR_T     rdwr_Size; /*read or write size*/
    RIO_UCHAR_T     srtr_TID; /*source or target TID*/
    RIO_UCHAR_T     hop_Count;
    unsigned long   config_Offset;
    RIO_UCHAR_T     wdptr; 
    RIO_UCHAR_T     status;
    unsigned int    doorbell_Info;
    RIO_UCHAR_T     msglen;
    RIO_UCHAR_T     letter;
    RIO_UCHAR_T     mbox;
    RIO_UCHAR_T     msgseg;
    RIO_UCHAR_T     ssize;
    RIO_ADDRESS_T   address;          /* double-word aligned address */
    RIO_ADDRESS_T   extended_Address;
    RIO_UCHAR_T     xamsbs;
    RIO_BOOL_T      ext_Address_Valid; 
    RIO_BOOL_T      ext_Address_16;
    RIO_UCHAR_T     rsrv_Ftype6;
    unsigned long   rsrv_Ftype8;
    RIO_UCHAR_T     rsrv_Ftype10;

    unsigned int    crc;
    RIO_BOOL_T      is_Crc_Valid;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_UCHAR_T     rsrv_Ftype9;  
    RIO_DS_INFO_T   stream_ID;  /* Stream id it is the combination of device id and transaction id */ 
    RIO_BOOL_T      o;		/* If set payload has an odd no.of halfwords */
    RIO_BOOL_T      p; 		/* If set pad byte was used to pad to a half word boundary*/
    RIO_DS_LENGTH_T length;     /* in bytes of the segmented PDU */
    RIO_BOOL_T      s_Ds;       /*indicates start of segment*/
    RIO_BOOL_T      e_Ds;   	/*end of segment*/
    RIO_UCHAR_T     cos;        /*class of service*/
    /* GDA: Bug#124 - Support for Data Streaming packet */

   /* GDA: Bug#125 - Support for Flow control packet */
    RIO_UCHAR_T      rsrv_Ftype7;
    RIO_UCHAR_T	     x_On_Off;	/* Stop/Start issuing req for specified & lower priority 
					   transaction request flowP) 				*/
    RIO_UCHAR_T	     flow_ID;	/* Highest priority affected transaction request flow 	*/
    RIO_ADDRESS_T    dest_ID;	/* Indicates which end pt the CCP is destined for (srcID
					   of the packet that caused the generation of the CCP) */
    RIO_ADDRESS_T    tgtdest_ID;/* Combined with the flowID field, indicates which 
				   transaction request flows need to be acted upon 
				   (destinationID field of the packet which caused the 
				   generation of the CCP)				*/
    RIO_UCHAR_T	     soc;	/* Source Of Congestion from Switch / End point	*/
    /* GDA: Bug#125 - Support for Flow control packet */

    

} RIO_TXRXM_PACKET_REQ_STRUCT_T;



/*packet struct interface for the PE/PL models*/
typedef struct
{
    RIO_TXRXM_PACKET_REQ_STRUCT_T packet;  /*the ackID value from this structure is not used in the PE/PL models*/
    unsigned long   intermediate_Crc;
    RIO_BOOL_T      is_Int_Crc_Valid;     /*if RIO_TRUE model used intermediate crc value
                                            from the intermediate_Crc field instead of calculated one*/
    unsigned long   crc;
    RIO_BOOL_T      is_Crc_Valid;     /*if RIO_TRUE model used crc value from the crc field 
                                        instead of calculated one*/
    RIO_UCHAR_T          dw_Size;      /* size of the data arry   */
    RIO_DW_T             *data;       /* data is supplied only if required by the request type */
} RIO_MODEL_PACKET_STRUCT_RQ_T;



/* types of granules supported*/

typedef enum {
    RIO_GR_NEW_PACKET          = 0x00,
    RIO_GR_DATA                = 0x01,
    RIO_GR_PACKET_ACCEPTED     = 0x02,
    RIO_GR_PACKET_RETRY        = 0x03,
    RIO_GR_PACKET_NOT_ACCEPTED = 0x04,
    RIO_GR_IDLE                = 0x05,
    RIO_GR_THROTTLE            = 0x06, 
    RIO_GR_RESTART_FROM_RETRY  = 0x07,
    RIO_GR_TOD_SYNC            = 0x08, 
    RIO_GR_EOP                 = 0x09,
    RIO_GR_STOMP               = 0x0A,
    RIO_GR_LINK_REQUEST        = 0x0B,
    RIO_GR_LINK_RESPONSE       = 0x0C,
    RIO_GR_TRAINING            = 0x0D,
    RIO_GR_HAS_BEEN_CORRUPTED  = 0x0E,
    RIO_GR_UNRECOGNIZED        = 0x0F,
    RIO_GR_S_BIT_CORRUPTED     = 0x10,
    RIO_GR_CORRUPTED_TRAINING  = 0x11
} RIO_GRANULE_TYPE_T;

/*this enum defines kind of places in packet stream for txrxm*/
typedef enum {
    FREE = 0,
    EMBEDDED,
    CANCELING
}RIO_PLACE_IN_STREAM_T;


/*this enum defines packet's violation types detected in txrxm*/
typedef enum {
    PACKET_IS_LONGER_276 = 0,
    CRC_ERROR,
    FTYPE_RESERVED,
    TTYPE_RESERVED,
    TT_RESERVED,
    NON_ZERO_RESERVED_PACKET_BITS_VALUE,
    INCORRECT_PACKET_HEADER,
    PAYLOAD_NOT_DW_ALIGNED
}RIO_PACKET_VIOLATION_T;


typedef enum {
    RIO_OUR_BUF_STATUS = 0,
    RIO_PARTNER_BUF_STATUS, /*GDA1 val1*/
    RIO_FLOW_CONTROL_MODE,
    RIO_INBOUND_ACKID_STATUS, /*next ackID to be sent in the ack control*/
    RIO_INBOUND_ACKID, /* next expected by the attached device in the packet */ 
    RIO_OUTBOUND_ACKID, /*GDA1 val5*//*out port next transmitted ackID value in packet*/
    RIO_OUTSTANDING_ACKID, /* expected ackID in the next received ack control*/
    RIO_LRSP_ACKID_STATUS,
    RIO_TX_DATA_IN_PROGRESS,
    RIO_RX_DATA_IN_PROGRESS,
    RIO_INITIALIZED,   /*GDA1 val10*/
    RIO_UNREC_ERROR,
    RIO_OUTPUT_ERROR,
    RIO_INPUT_ERROR,
    RIO_PORT_WIDTH,
    RIO_INITIALIZED_PORT_WIDTH,  /*GDA1 Val15 */
    RIO_PORT_WIDTH_OVERRIDE,

    /* for specific internal (intermodule) needs: */
    RIO_LANE_WITH_INVALID_CHARACTER,  /* number of the lane where invalid character occured */
    RIO_IS_PORT_TRUE_4X, /* RIO_TRUE if port is working in 4x mode */
    RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE, /* in case of 1x mode it is a number of the current byte in received granule */
    RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE, /*GDA1 20*//* in case of 1x mode it is a number of the current byte in transmitted granule */
    RIO_ARE_ALL_IDLES_IN_GRANULE, /* indication for the INIT FSM that such granule shall not be reported to PL/TxRxM */
    RIO_SYMBOL_IS_PD,  /* this flag shows when shoose /PD/ in case of LReq/RFR */
    RIO_IS_ALIGNMENT_LOST,  /* this flag is used to signalize from PM to DP that alignment is lost (queues are to be reset) */
    RIO_IS_SYNC_LOST,  /* this flag is used to signalize from PM to DP that synchronization is lost (streaming is to be reset) */
    RIO_IS_CHARACTER_INVALID,  /*GDA1 25 *//* this flag is used to signalize from DP to PM that codegroup could not be decoded */
    RIO_COPY_TO_LANE2,  /* shows that in 1x mode lane2 is used (needed for wrapper to adjust data in hooks) */
    RIO_SUPPRESS_HOOK,  /* notifies that the corresponding hook (codegroup or character received) shall be suppressed */
    RIO_INITIAL_RUN_DISP,  /* notifies DP CL that first encountered comma on a lane has a positive disparity */
    RIO_DECODE_ANY_RUN_DISP,  /* notifies DP CL that there shall be an attempt to decode codegroup trying both run.disparities */
    RIO_IS_SC_PD_INCORRECT,  /*GDA1 30 *//* asserted when SC/PD is incorrectly chosen */
    RIO_LANE2_JUST_FORCED_BY_DISCOVERY,  /* asserted when 4x mode -> to 1x_lane2 mode by discovery timer while in SYNC FSM */
    RIO_RX_IS_PD,            /*true if packet sending flag in PCS_PMA adapter is set*/
    RIO_SYMBOL_RX_FIRST_BYTE_VAL, /*value of the first character in the received symbol*/
    RIO_PACKET_FINISHING_CNT, /*number of codegroups that will be sent before the last granule of packet sending*/
    
    RIO_STATE_INDICATORS_NUMBER /* !!! must be the last !!! */
} RIO_STATE_INDICATORS_T;    



/* these enums are used in PL LIGHT model*/
typedef enum {
    RIO_CALCULATE_BOTH_CRC = 0,
    RIO_CALCULATE_LAST_CRC,
    RIO_CALCULATE_INTERMEDIATE_CRC,
    RIO_DONT_CALCULATE_CRC
} RIO_CRC_CALCULATE_SETTING_T;

typedef enum {
    RIO_SET_BOTH_CRC_CORRECT = 0,
    RIO_SET_LAST_CRC_CORRECT_ONLY,
    RIO_SET_INTERMEDIATE_CRC_CORRECT_ONLY,
    RIO_SET_BOTH_CRC_INCORRECT
} RIO_CRC_HANDLING_SETTING_T;

typedef enum {
    RIO_EXT_ADDR_NOT_VALID = 0,
    RIO_EXT_ADDR_VALID_32,
    RIO_EXT_ADDR_VALID_16
} RIO_EXT_ADDR_SIZE_T;

/* this struct is used in the PL LIGHT model */
typedef struct
{
    RIO_BOOL_T      s; 		/* for parallel mode only */
    RIO_BOOL_T      s_inv; 	/* for parallel mode only */
    RIO_UCHAR_T     ack_ID;
    RIO_UCHAR_T     rsrv_Phy_1;
    RIO_UCHAR_T     rsrv_Phy_2;

    RIO_UCHAR_T     prio;
    RIO_UCHAR_T     tt;
    RIO_UCHAR_T     ftype;
    RIO_TR_INFO_T   transport_Info;
    RIO_PL_TRANSP_TYPE_T     transport_Info_Size;

    /* ttype parameter is used for all transactions.
    For doorbell transactions ttype parameter is used to fill doorbell_reserved field (upper bits).
    For message transactions ttype parameter is used to fill msglen field. */
    RIO_UCHAR_T     ttype;

    /* rdwr_Size parameter is used for all transactions except stream_write. 
    For doorbell transactions rdwr_Size parameter is used to fill doorbell_reserved field (lower bits).
    For response transactions rdwr_Size parameter is used to fill status field.*/
    RIO_UCHAR_T     rdwr_Size; 

    /* srtr_TID (source or target TID) parameter is used for all transactions except stream_write. 
    For message transactions srtr_TID parameter is used to fill letter-mbox-msgseg fields. */
    RIO_UCHAR_T     srtr_TID; 

    /* address parameter is used for all transactions except response, message.
    For doorbell transactions address parameter is used to fill doorbell_info field.
    For maintenance transactions address parameter is used to fill config_offset field. */
    unsigned int address;

    /* wdptr parameter is used for all transactions where address field is filled.*/
    RIO_UCHAR_T     wdptr;

    /* xamsbs parameter is used for all transactions where address field is filled. */
    RIO_UCHAR_T     xamsbs;

    /* extended_Address parameter is used only for intervention requests, 
    non_intervention requests, writes, stream write requests depending on
    ext_addr_Size field */
    RIO_ADDRESS_T   extended_Address;
    RIO_EXT_ADDR_SIZE_T ext_addr_Size;

    /* hop_Count parameter is used only for maintenance transactions */
    RIO_UCHAR_T     hop_Count;

    /* gsm_Specific parameter is used only for intervention requests,
    these parameters set up fields sec_domain, secID and secTID 
    the size of all fields is 16 bits */
    unsigned int     gsm_Specific;

    RIO_CRC_HANDLING_SETTING_T crc_Valid_Setting;

    /* payload is used only for responses, messages, writes, stream writes */
    RIO_UCHAR_T          dw_Size;     
    RIO_DW_T             *data;     
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_HW_T         *data_hw; 
    RIO_HW_SIZE      hw_Size;   
    RIO_DS_INFO_T    stream_ID;  /* Stream id it is the combination of device id and transactionid */  
    RIO_BOOL_T       o;		 /* If set payload has an odd no.of halfwords */
    RIO_BOOL_T       p; 	 /* If set pad byte was used to pad to a half word boundary*/
    RIO_DS_LENGTH_T  length;     /* in bytes of the segmented PDU */
    RIO_BOOL_T       s_Ds;       /*indicates start of segment*/
    RIO_BOOL_T       e_Ds;   	 /*end of segment*/
    /* GDA: Bug#124 - Support for Data Streaming packet */

   /* GDA: Bug#125 - Support for Flow control packet */
    
    RIO_UCHAR_T		x_On_Off;	/* Stop/Start issuing req for specified & lower priority 
					   transaction request flowP) 				*/
    RIO_UCHAR_T		flow_ID;	/* Highest priority affected transaction request flow 	*/
    RIO_ADDRESS_T	dest_ID;	/* Indicates which end pt the CCP is destined for (srcID
					   of the packet that caused the generation of the CCP) */
    RIO_ADDRESS_T	tgtdest_ID;	/* Combined with the flowID field, indicates which 
					   transaction request flows need to be acted upon 
					   (destinationID field of the packet which caused the 
					   generation of the CCP)				*/
    RIO_UCHAR_T		soc;		/* Source Of Congestion from Switch / End point	*/
    /* GDA: Bug#125 - Support for Flow control packet */


    

} RIO_PLLIGHT_PACKET_T;

typedef struct {
    RIO_UCHAR_T   pl_Inbound_Buffer_Size;
    RIO_UCHAR_T   pl_Outbound_Buffer_Size;

    RIO_UCHAR_T   ll_Inbound_Buffer_Size;
    RIO_UCHAR_T   ll_Outbound_Buffer_Size;

    /* Bug: 43 Fix */
    RIO_BOOL_T    gda_Tx_Illegal_Packet;
    RIO_BOOL_T    gda_Rx_Illegal_Packet;

    /* GDA: Support for Bug#131 */
    RIO_WORD_T	  RIO_Link_Valid_To1_I;
    /* GDA Bug#131 */

} RIO_PLLIGHT_MODEL_INST_PARAM_T;

#endif /* RIO_TYPES_H */
