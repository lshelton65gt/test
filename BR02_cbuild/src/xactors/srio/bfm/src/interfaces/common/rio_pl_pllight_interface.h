#ifndef RIO_PL_PLLIGHT_INTERFACE_H
#define RIO_PL_PLLIGHT_INTERFACE_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/common/rio_pl_pllight_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains common interfaces for pl and pllight models
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

/*
 * this function notify the instance about external clock event
 */
typedef int (*RIO_PL_CLOCK_EDGE)(
    RIO_HANDLE_T handle /* instance handle */
    );

/*
 * the function turn PL model instance to reset mode and all internal 
 * parameters (exclude cteation ) is set to initial values
 */
typedef int (*RIO_PL_MODEL_START_RESET)(RIO_HANDLE_T handle);


/*
 * the function sets the packet retry generator
 */
typedef int (*RIO_PL_SET_RETRY_GENERATOR)(
    RIO_HANDLE_T    handle,  
    unsigned int    retry_Prob_Prio0,
    unsigned int    retry_Prob_Prio1,
    unsigned int    retry_Prob_Prio2,
    unsigned int    retry_Prob_Prio3
    );
/*
 * the function sets the packet-not-accepted generator
 */
typedef int (*RIO_PL_SET_PNACC_GENERATOR)(
    RIO_HANDLE_T    handle,  
    unsigned int    pnacc_Factor
    );
/*
 * the function sets the packet stomp generator
 */
typedef int (*RIO_PL_SET_STOMP_GENERATOR)(
    RIO_HANDLE_T    handle,  
    unsigned int    stomp_Factor
    );

/*
 * the function sets LRQ-RESET generator
 */
typedef int (*RIO_PL_SET_LRQR_GENERATOR)(
    RIO_HANDLE_T    handle,  
    unsigned int    lrqr_Factor
    );

/*
 * the function sets RND idle generator
 */
typedef int (*RIO_PL_ENABLE_RND_IDLE_GENERATOR)(
    RIO_HANDLE_T handle,  
    unsigned int low_Boundary,
    unsigned int high_Boundary,
    RIO_BOOL_T enable
    );

/*
 *GDA:the function sets RND Pnacc generator
 * 
 */

typedef int (*RIO_PL_ENABLE_RND_PNACC_GENERATOR_INIT)(
              RIO_HANDLE_T    handle,
              RIO_WORD_T pnacc_Low_Boundary,
              RIO_WORD_T pnacc_High_Boundary,
              RIO_BYTE_T pnacc_Probability  
            );


/*
 * GDA:the function sets RND Retry generator
 * 
 */
typedef int (*RIO_PL_ENABLE_RND_RETRY_GENERATOR_INIT)(
	      RIO_HANDLE_T handle,
    	      RIO_WORD_T retry_Low_Boundary,
              RIO_WORD_T retry_High_Boundary,
              RIO_BYTE_T retry_Probability_Prio0,
              RIO_BYTE_T retry_Probability_Prio1,
              RIO_BYTE_T retry_Probability_Prio2,
              RIO_BYTE_T retry_Probability_Prio3
            );

/*
 * GDA:the function to invoke multicast control symbol Bug#116
 * 
 */
typedef int (*RIO_PL_GENERATE_MULTICAST)(
	      RIO_HANDLE_T handle,
    	      RIO_WORD_T low_Boundary,
              RIO_WORD_T high_Boundary);



#endif /* RIO_PL_PLLIGHT_INTERFACE_H */

