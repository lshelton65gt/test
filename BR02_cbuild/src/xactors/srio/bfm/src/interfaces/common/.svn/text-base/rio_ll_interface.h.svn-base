#ifndef RIO_LL_INTERFACE_H
#define RIO_LL_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/src/interfaces/common/rio_ll_interface.h,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains user-visible interface of the logical layer
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif


typedef int (*RIO_GSM_REQUEST)(
    RIO_HANDLE_T    handle,
    RIO_GSM_TTYPE_T ttype,             /* requested transaction type  */
    RIO_GSM_REQ_T   *rq,
    RIO_TAG_T       trx_Tag            /* user-assigned tag of the request */
    );

/* the callback is used for GSM request to tell local device how to mark the cacheline */
typedef int (*RIO_LOCAL_RESPONSE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_LOCAL_RTYPE_T response
    );

typedef int (*RIO_GSM_REQUEST_DONE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,           /* in case of error, is set equal to status field of the 
                                             response packet and notifies the user about error code */
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    );

                    
typedef int (*RIO_IO_REQUEST)( 
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    );

/* is not called for write operations requiring no response */
typedef int (*RIO_IO_REQUEST_DONE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,           /* in case of error, is set equal to status field
                                             of the response packet and notifies the user about error code */
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    );


typedef int (*RIO_MESSAGE_REQUEST)(
    RIO_HANDLE_T       handle,
    RIO_MESSAGE_REQ_T  *rq,
    RIO_TAG_T          trx_Tag             /* user-assigned tag of the request */
    );

typedef int (*RIO_MP_REQUEST_DONE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code            /* in case of error, is set equal to status field
                                             of the response packet and notifies the user about error code */
    );

typedef int (*RIO_DOORBELL_REQUEST)(
    RIO_HANDLE_T        handle,
    RIO_DOORBELL_REQ_T  *rq,
    RIO_TAG_T           trx_Tag             /* user-assigned tag of the request */
    );

typedef int (*RIO_DOORBELL_REQUEST_DONE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code           /* in case of error, is set equal to status field
                                             of the response packet and notifies the user about error code */
    );

typedef int (*RIO_CONFIGURATION_REQUEST)(
    RIO_HANDLE_T      handle,
    RIO_CONFIG_REQ_T  *rq,
    RIO_TAG_T         trx_Tag          /* user-assigned tag of the request */
    );

typedef int (*RIO_CONFIG_REQUEST_DONE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,           /* in case of error, is set equal to status field
                                             of the response packet and notifies the user about error code */
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    );

/* GDA: Bug#124 - Support for Data Streaming packet */
typedef int (*RIO_DS_REQUEST)(
    RIO_HANDLE_T   handle,
    RIO_DS_REQ_T   *rq,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    );

typedef int (*RIO_DS_REQUEST_DONE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag,
    RIO_REQ_RESULT_T  result,
    RIO_UCHAR_T       err_Code,   /* in case of error, is set equal to status field
                                    of the response packet and notifies the user about error code */
    RIO_HW_SIZE       hw_Size,
    RIO_HW_T          *data_hw
    );
/* GDA: Bug#124 - Support for Data Streaming packet */


/****************************************************************************** 
 * 
 *
 *****************************************************************************/

typedef int (*RIO_SNOOP_REQUEST)(
    RIO_CONTEXT_T      context,
    RIO_SNOOP_REQ_T    rq
    );

typedef int (*RIO_SNOOP_RESPONSE)(
    RIO_HANDLE_T        handle,
    RIO_SNOOP_RESULT_T  snoop_Result,
    RIO_DW_T            *data                  /* valid if snoop hit */
    );


typedef int (*RIO_MP_REMOTE_REQUEST)(
    RIO_CONTEXT_T       context,
    RIO_REMOTE_MP_REQ_T message
    );


typedef int (*RIO_DOORBELL_REMOTE_REQUEST)(
    RIO_CONTEXT_T  context,
    RIO_DB_INFO_T  doorbell_Info
    );

typedef int (*RIO_PORT_WRITE_REMOTE_REQUEST)(
    RIO_CONTEXT_T context,
    RIO_UCHAR_T   dw_Size,    /* can not exceed 8 doublewords */
    RIO_UCHAR_T   sub_DW_Pos, /* in case of sub-doubleword access, the data position inside dw:
                                 0 - first half-dw, 1 - second, else - whole dw is being accessed */
    RIO_DW_T      *data       /* pointer to the data area */
);



/****************************************************************************** 
 *  Memory and memory directory access interface
 * 
 *
 *
 *****************************************************************************/


typedef int (*RIO_MEMORY_REQUEST)(
    RIO_CONTEXT_T          context,
    RIO_ADDRESS_T          address,              /* 32 bit double-word aligned address */
    RIO_ADDRESS_T          extended_Address,
    RIO_UCHAR_T            xamsbs,
    RIO_UCHAR_T            dw_Size,              /* size of access in double-words */
    RIO_MEMORY_REQ_TYPE_T  req_Type,             /* memory request type */
    RIO_BOOL_T             is_GSM,               /* this parameter shall be RIO_TRUE for the memory */
                                                 /* requests for the GSM transactions servicing     */
    RIO_UCHAR_T            be                    /* byte enables (all 1s in case of multi-doubleword access */
    );

    
/* 
   function is called to set data obtained for memory read; data should be returned by 
   the memory controller in correct order, wrapping around cacheline for WRAP_READ request
*/

typedef int (*RIO_SET_MEMORY_DATA)( 
    RIO_HANDLE_T  handle,
    RIO_DW_T      *dw              /* double word */
    );
    
/* function is called to get data for memory write */

typedef int (*RIO_GET_MEMORY_DATA)( 
    RIO_HANDLE_T  handle,
    RIO_DW_T      *dw             /* double word */
    );


/* memory directory read callback */

typedef int (*RIO_READ_DIRECTORY)(
    RIO_CONTEXT_T  context, 
    RIO_ADDRESS_T  address,         /* address of coherence granule (cacheline-size aligned) */
    RIO_ADDRESS_T  extended_Address,
    RIO_UCHAR_T    xamsbs,
    RIO_SH_MASK_T  *sharing_Mask    /* cache line sharing mask */
    );


/* memory directory write callback */

typedef int (*RIO_WRITE_DIRECTORY)(
    RIO_CONTEXT_T  context, 
    RIO_ADDRESS_T  address,         /* address of coherence granule (cacheline-size aligned) */
    RIO_ADDRESS_T  extended_Address,
    RIO_UCHAR_T    xamsbs,
    RIO_SH_MASK_T  sharing_Mask     /* updated cache line sharing mask to be stored in the directory*/
    );


/****************************************************************************** 
 *  User-defined functions (UDFs) 
 * 
 *
 *
 *****************************************************************************/

/* should return 0 if local access, else means remote */
typedef int (*RIO_TRANSLATE_LOCAL_IO_REQ)(
    RIO_CONTEXT_T    context,
    RIO_ADDRESS_T    *address,           /* callback gets original address and returns translated address */
    RIO_ADDRESS_T    *extended_Address,
    RIO_UCHAR_T      *xamsbs,
    RIO_TR_INFO_T    *transport_Info     /* transport_info field of the packet (for instance, device id)  */
    );

typedef int (*RIO_TRANSLATE_REMOTE_IO_REQ)(
    RIO_CONTEXT_T    context,
    RIO_ADDRESS_T    *address,           /* callback gets original address and returns translated address */
    RIO_ADDRESS_T    *extended_Address,
    RIO_UCHAR_T      *xamsbs
    );
/* GDA: Bug#124 - Support for Data Streaming packet */
typedef int (*RIO_TRANSLATE_LOCAL_DS_REQ)(
    RIO_CONTEXT_T    context,
    RIO_TR_INFO_T    *transport_Info     /* transport_info field of the packet (for instance, device id)  */
    );

typedef int (*RIO_TRANSLATE_REMOTE_DS_REQ)(
    RIO_CONTEXT_T    context
    );
/* GDA: Bug#124 - Support for Data Streaming packet */

/* should return 0 if local access, else means remote */
typedef int (*RIO_ROUTE_GSM_REQ)(
    RIO_CONTEXT_T   context,
    RIO_ADDRESS_T   address,             /* address of the local GSM request */
    RIO_ADDRESS_T   extended_Address,
    RIO_UCHAR_T     xamsbs,
    RIO_TR_INFO_T   *transport_Info      /* transport_info field of the packet (for instance, device id)  */
    );


typedef int (*RIO_MODEL_START_RESET)(
    RIO_HANDLE_T handle
    );


/* GDA: Bug#125 - Support for Flow control packet */
typedef int (*RIO_FC_REQUEST)( 
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T  *rq,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    );

typedef int (*RIO_FC_REQUEST_DONE)(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code           /* in case of error, is set equal to status field
                                             of the response packet and notifies the user about error code */
    );

typedef int (*RIO_CONGESTION)( 
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
    );


typedef int (*RIO_TRANSLATE_LOCAL_FC_REQ)(
    RIO_CONTEXT_T    context,
    RIO_TR_INFO_T    *transport_Info     /* transport_info field of the packet (for instance, device id)  */
    );

typedef int (*RIO_TRANSLATE_REMOTE_FC_REQ)(
    RIO_CONTEXT_T    context
    );

/* GDA: Bug#125  */


#endif /* RIO_LL_INTERFACE_H */

