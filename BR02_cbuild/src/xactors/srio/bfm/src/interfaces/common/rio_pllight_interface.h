#ifndef RIO_PLLIGHT_INTERFACE_H
#define RIO_PLLIGHT_INTERFACE_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/common/rio_pllight_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the logical layer
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_PL_PLLIGHT_INTERFACE_H
#include "rio_pl_pllight_interface.h"
#endif

/*
 * This routine requests that the RapidIO PL LIGHT model sends a packet
 */
typedef int (*RIO_PLLIGHT_SEND_PACKET)(
    RIO_HANDLE_T handle,
    RIO_PLLIGHT_PACKET_T *packet, 
    unsigned int packet_Num,
    unsigned int packet_Resend_Count);

/*
 * This routine requests that the RapidIO PL LIGHT model sends a packet
 */
typedef int (*RIO_PLLIGHT_SEND_PACKET_STREAM)(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T* packet_Buffer, 
    unsigned int packet_Size,
    RIO_CRC_CALCULATE_SETTING_T crc_setting,
    unsigned int packet_Num,
    unsigned int packet_Resend_Count,
    RIO_BOOL_T is_AckID_Valid,
    RIO_BOOL_T is_Padded);

/*
 * This routine sets RapidIO PL LIGHT model port-link timeout
 * The value is stored into the Port Link Time-out Control CSR
 * and used in link request time-out proceding and
 * sending packet time-out proceding 
 */
typedef int (*RIO_PLLIGHT_SET_PORT_LINK_TIME_OUT)(
    RIO_HANDLE_T handle,
    unsigned int timeout);

/*
 * This callback is called by the RapidIO PL LIGHT model after 
 * the requested packet has been acknoweldged 
 * (RapidIO PL LIGHT model received positive physical acknowledge)
 */
typedef int (*RIO_PLLIGHT_PACKET_ACKNOWLEDGED)(
    RIO_CONTEXT_T context,
    unsigned int packet_Num,
    RIO_BOOL_T is_Positive);

/*
 * This callback is invoked by the RapidIO PL LIGHT model
 * when a remote transaction packet has arrived.
 */
typedef int (*RIO_PLLIGHT_PACKET_RECEIVED)(
    RIO_CONTEXT_T context,
    RIO_UCHAR_T* packet_Buffer,
    unsigned int packet_Size,
    RIO_PLLIGHT_PACKET_T *packet);

#endif /* RIO_PLLIGHT_INTERFACE_H */

