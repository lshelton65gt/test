#ifndef RIO_SWITCH_INTERFACE_H
#define RIO_SWITCH_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/common/rio_switch_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  file contains common switch interface functions
*
* Notes:        
*
******************************************************************************/

#include "rio_types.h"

/*
this function is used by the environment to notify switch about
clock event
*/
typedef int (*RIO_SWITCH_CLOCK_EDGE)(
    RIO_HANDLE_T handle  /* instance's handle */
    );

/*this function is used to turn switch into reset state */
typedef int (*RIO_SWITCH_START_RESET)(
    RIO_HANDLE_T handle
    );


#endif /* RIO_SWITCH_INTERFACE_H */

