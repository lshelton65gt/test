#ifndef RIO_TXRXM_INTERFACE_H
#define RIO_TXRXM_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/common/rio_txrxm_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes external TxRxM interface.
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

/*
 * this function notify the instance about external clock event
 */
typedef int (*RIO_TXRXM_CLOCK_EDGE)(
    RIO_HANDLE_T handle /* instance handle */
    );

/*
 * the function notify the instance about request that shall head out.
 * this request is represented in the struct form.
 */
typedef int (*RIO_TXRXM_PACKET_STRUCT_REQUEST)( 
    RIO_HANDLE_T                    handle,
    RIO_TXRXM_PACKET_REQ_STRUCT_T   *rq,
    RIO_UCHAR_T                     dw_Size,           
    RIO_DW_T                        *data             
    );



/*
 * the function notify the instance about common(that means not dependent of type
 * packet that shall head out).
 * Calling this function user can make all necessary error injections in this particular
 * packet by filling the packet buffer as he suppose. 
 */
typedef int (*RIO_TXRXM_PACKET_STREAM_REQUEST)(
    RIO_HANDLE_T            handle,
    RIO_UCHAR_T*            packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int            packet_Size        /*packet array size in byte count*/  
    );

/*
 * callback is called in case of request receiving
 */

typedef int (*RIO_TXRXM_REQUEST_RECEIVED)(
    RIO_CONTEXT_T         context,
    RIO_REMOTE_REQUEST_T  *packet_Struct,      /* struct, containing packets's fields */
    RIO_UCHAR_T           ack_ID,
    RIO_TR_INFO_T         transport_Info,
    RIO_UCHAR_T           hop_Count
    );

/*
 * callback is called in case of request receiving
 */

typedef int (*RIO_TXRXM_RESPONSE_RECEIVED)(
    RIO_CONTEXT_T         context,
    RIO_RESPONSE_T        *packet_Struct,      /* struct, containing response's fields */
    RIO_UCHAR_T           ack_ID,
    RIO_UCHAR_T           hop_Count
    );    
    
/*
 * notify the environment about received errorneous external packet. 
 * tag is assigned by TXRXM 
 */
typedef int (*RIO_TXRXM_VIOLENT_PACKET_RECEIVED)(
    RIO_CONTEXT_T                context, 
    RIO_UCHAR_T*                packet_Buffer,      /* array, containing packet's bit stream (max - 300 bytes) */
    unsigned int                packet_Size,
    RIO_PACKET_VIOLATION_T      violation_Type,
    unsigned short              expected_Crc
    );


/*
 * notify the environment about received canceled external packet. 
 * tag is assigned by TXRXM 
 */
typedef int (*RIO_TXRXM_CANCELED_PACKET_RECEIVED)(
    RIO_CONTEXT_T                context, 
    RIO_UCHAR_T*                packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int                packet_Size  /*packet size in byte count*/
    );

/*
 * Hook callback requested by Devendra Kumar (Xilinx). 
 * This callback represents character stream of the packet 
 * before the txrx model pass this stream to the lower layers
 */
typedef int (*RIO_TXRXM_PACKET_TO_SEND_STREAM)(
    RIO_CONTEXT_T               context, 
    RIO_UCHAR_T                 packet_Buffer[],      /* array, containing packet's bit stream */
    unsigned int                packet_Size  /*packet size in byte count*/
    );

/*hook callback that is invoked in case when granule is sent to 
 *the pcs/pma from the txrx model
 *This callbackIs requested by Jim Tollar (Motorola)*/
typedef void (*RIO_TXRXM_GRANULE_TO_SEND)(
    RIO_CONTEXT_T               context,
    void*                       granule /*RIO_PCS_PMA_GRANULE_T* in serial mode
                                          RIO_TXRX_GRANULE_T* in parallel mode*/);

/*feedback from the (TxRx) environment that indicates 
 *that the last granuale of a packet will be transmitted 
 *in the next clock edge. 
 *This callbackIs requested by Jim Tollar (Motorola)*/
typedef void (*RIO_TXRXM_LAST_PACKET_GRANULE)(
    RIO_CONTEXT_T context);


/*hook context for the symbol sending*/
typedef int (*RIO_TXRXM_SYMBOL_BUFFER_TO_SEND)(
    RIO_CONTEXT_T               context, 
    RIO_UCHAR_T                 symbol_Buffer[], /* array, containing character stream */
    unsigned int                buffer_Size  /*buffer size in byte count*/
);

/*
 * the function turn TXRXM model instance to reset mode and all internal 
 * parameters (exclude cteation ) is set to initial values
 */
typedef int (*RIO_TXRXM_MODEL_START_RESET)(RIO_HANDLE_T handle);


#endif /* RIO_TXRXM_INTERFACE_H */









