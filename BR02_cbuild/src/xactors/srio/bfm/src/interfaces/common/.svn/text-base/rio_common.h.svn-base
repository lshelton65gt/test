#ifndef RIO_COMMON_H
#define RIO_COMMON_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: Y:\riosuite\src\riom\interfaces\common\rio_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains functions which are common to logical and 
*               physical layers
*
* Notes:        
*
******************************************************************************/

/*GDA - Packet size in DWORD */
#define GDA_PKT_SIZE 	64    /* 32 or 64 DWord Size */	
/*GDA - End */

/* GDA: Bug#124 - Support for Data Streaming packet */
#define DS_PKT_SIZE    256    /* 128 HALF WORD SIZE*/
#define DS_SIZE        32768    /*32k HALF WORD SIZE - Max Size*/
/* GDA: Bug#124 - Support for Data Streaming packet */


#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif


/* version of the RapidIO Models product */
#define RIO_MODEL_VERSION "version 2.1 build 08 RP"




/*
 * functions prints version info 
 */
typedef int (*RIO_PRINT_VERSION)(
    RIO_HANDLE_T handle
    );

/*
 * request to set configuration register 
 */
typedef int (*RIO_SET_CONFIG_REG)(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     data
    );

/*
 * request to read configuration register
 */
typedef int (*RIO_GET_CONFIG_REG)(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   /* word offset */
    unsigned long     *data
    );

/*
 * request to read extended features configuration register
 */
typedef int (*RIO_REMOTE_CONFIG_READ)(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,      /* double-word aligned offset inside configuration space */
    unsigned long    *data
    );

/*
 * request to write extened features configuration register 
 */
typedef int (*RIO_REMOTE_CONFIG_WRITE)(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,       /* double-word aligned offset inside configuration space */
    unsigned long    data
    );


/*
 * callback is called in case of error
 */

typedef int (*RIO_USER_MSG)(
    RIO_CONTEXT_T  context,
    char *         user_Msg      /* null-terminated error string */
    );


/*
 * callback is called in case of request receiving
 */

typedef int (*RIO_REQUEST_RECEIVED)(
    RIO_CONTEXT_T         context,
    RIO_REMOTE_REQUEST_T  *packet_Struct      /* struct, containing packets's fields */
    );

/*
 * callback is called in case of request receiving
 */

typedef int (*RIO_RESPONSE_RECEIVED)(
    RIO_CONTEXT_T         context,
    RIO_RESPONSE_T        *packet_Struct      /* struct, containing response's fields */
    );

/*
 * callback is called in case of symbol transmitting
 */

typedef int (*RIO_SYMBOL_TO_SEND)(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
    );

/*
 * callback is called in case of symbol transmitting
 */

typedef int (*RIO_PACKET_TO_SEND)(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int          buffer_Size,        /*Size always = 276*/  
    RIO_UCHAR_T*          packet_Length_In_Granules, /* maximum = 276/4 = 69 granules!!*/
    RIO_TAG_T             tag /*tag, assigned with the transaction, related to this packet */
    );


/* 
   Deletes the model instance. The handle is not valid after the call is complete
   because all the memory used for the instance data is deallocated 
 */
typedef int (*RIO_DELETE_INSTANCE)(
    RIO_HANDLE_T         handle
    );

/*
 * request to set configuration register 
 */
typedef int (*RIO_SET_STATE)(
    RIO_HANDLE_T             handle,         /* callback context */
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Parameter
    );

/*
 * request to read configuration register
 */
typedef int (*RIO_GET_STATE)(
    RIO_HANDLE_T             handle,    /* callback context */
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Parameter
    );

/*
 * the function sets seed value of the random generator
 */
int RIO_PL_Set_Random_Generator(
    unsigned int seed_Value
    );


#endif  /* RIO_COMMON_H */


