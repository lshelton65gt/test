#ifndef RIO_TXRXM_SERIAL_MODEL_H
#define RIO_TXRXM_SERIAL_MODEL_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_txrxm_serial_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    file contains user-visible interface of the TxRxM
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_TXRXM_INTERFACE_H
#include "rio_txrxm_interface.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif

#ifndef RIO_LP_SERIAL_INTERFACE_H
#include "rio_lp_serial_interface.h"
#endif

#ifndef RIO_TXRXM_SERIAL_INTERFACE_H
#include "rio_txrxm_serial_interface.h"
#endif

#ifndef RIO_SERIAL_COMMON_H
#include "rio_serial_common.h"
#endif



/* 
 * function tray to be passed to the environment by the txrxm layer model
 */
typedef struct {
    RIO_TXRXM_CLOCK_EDGE                         rio_TxRxM_Clock_Edge;
                                                
    RIO_TXRXM_PACKET_STRUCT_REQUEST              rio_TxRxM_Packet_Struct_Request;
    RIO_TXRXM_PACKET_STREAM_REQUEST              rio_TxRxM_Packet_Stream_Request;
                                                
    RIO_TXRXM_SERIAL_SYMBOL_REQUEST              rio_TxRxM_Serial_Symbol_Request;
    RIO_TXRXM_SERIAL_CHARACTER_REQUEST           rio_TxRxM_Serial_Character_Request;
    RIO_TXRXM_SERIAL_CHARACTER_COLUMN_REQUEST    rio_TxRxM_Serial_Character_Column_Request;
    RIO_TXRXM_SERIAL_CODE_GROUP_REQUEST          rio_TxRxM_Serial_Code_Group_Request;
    RIO_TXRXM_SERIAL_CODE_GROUP_COLUMN_REQUEST   rio_TxRxM_Serial_Code_Group_Column_Request;
    RIO_TXRXM_SERIAL_SINGLE_CHARACTER_REQUEST    rio_TxRxM_Serial_Single_Character_Request;
    RIO_TXRXM_SERIAL_SINGLE_BIT_REQUEST          rio_TxRxM_Serial_Single_Bit_Request;
    RIO_TXRXM_SERIAL_COMP_SEQ_REQUEST            rio_TxRxM_Serial_Comp_Seq_Request;
    RIO_TXRXM_SERIAL_PCS_PMA_MACHINE_MANAGEMENT  rio_TxRxM_Serial_Pcs_Pma_Machine_Management;
    RIO_TXRXM_SERIAL_POP_BIT                     rio_TxRxM_Serial_Pop_Bit;
    RIO_TXRXM_SERIAL_POP_CHARACTER               rio_TxRxM_Serial_Pop_Character;
                                                
                                                
    RIO_TXRXM_MODEL_START_RESET                  rio_TxRxM_Model_Start_Reset;
    RIO_TXRXM_SERIAL_MODEL_INITIALIZE            rio_TxRxM_Model_Initialize;
    RIO_SERIAL_GET_PINS                          rio_TxRxM_Get_Pins;
                                                
    RIO_PRINT_VERSION                            rio_TxRxM_Print_Version;

    RIO_DELETE_INSTANCE                          rio_TxRxM_Delete_Instance;

} RIO_TXRXM_SERIAL_MODEL_FTRAY_T;


/* 
 * callback tray to be used by the TxRx model (supplied by the model 
 * environment). It also includes three contexts: context of 8LP/EP callbacks 
 * interface, context of TxRx interface callbacks, context fo ruser message callback.
 * These contexts are used for running corresponding callbacks in proper environment.
 */
typedef struct {
    RIO_TXRXM_REQUEST_RECEIVED                    rio_TxRxM_Request_Received;
    RIO_TXRXM_RESPONSE_RECEIVED                    rio_TxRxM_Response_Received;
    RIO_TXRXM_VIOLENT_PACKET_RECEIVED            rio_TxRxM_Violent_Packet_Received;
    RIO_TXRXM_CANCELED_PACKET_RECEIVED            rio_TxRxM_Canceled_Packet_Received;

    RIO_TXRXM_SERIAL_SYMBOL_RECEIVED            rio_TxRxM_Serial_Symbol_Received;
    RIO_TXRXM_SERIAL_VIOLENT_SYMBOL_RECEIVED    rio_TxRxM_Serial_Violent_Symbol_Received;
    RIO_TXRXM_SERIAL_CODE_GROUP_RECEIVED        rio_TxRxM_Serial_Code_Group_Received;
    RIO_TXRXM_SERIAL_VIOLENT_CHARACTER_RECEIVED rio_TxRxM_Serial_Violent_Character_Received;
    RIO_TXRXM_SERIAL_CHARACTER_RECEIVED            rio_TxRxM_Serial_Character_Received;
 
    RIO_CONTEXT_T                                txrxm_Interface_Context; 

    RIO_SERIAL_SET_PINS                            rio_TxRxM_Set_Pins;
    RIO_CONTEXT_T                                lp_Serial_Interface_Context; 
    
    RIO_USER_MSG                                rio_User_Msg;
    RIO_CONTEXT_T                                rio_Msg;
    /*hook callbacks*/
    RIO_TXRXM_PACKET_TO_SEND_STREAM             rio_TxRxM_Packet_To_Send;
    RIO_TXRXM_GRANULE_TO_SEND                   rio_TxRxM_Granule_To_Send;
    RIO_TXRXM_LAST_PACKET_GRANULE               rio_TxRxM_Last_Packet_Granule;
    RIO_CONTEXT_T                               hook_Context;

} RIO_TXRXM_SERIAL_MODEL_CALLBACK_TRAY_T;




/* 
 * the function creates instance of TxRx model and returns handle to its data structure.
 * there are some create parameters, which can be defined only at the moment of 
 * instance creation. They are: size of outbound buffer of packets,symbols, granules .
 * After the instance's been created it is in reset mode. It's necessary to
 * invoke Initialize function to turn it into work mode.
 */
int RIO_TxRxM_Serial_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_TXRXM_SERIAL_INST_PARAM_T  *param
    );

/* 
 * the function returns TxRx model interface functions to be used for model access. 
 * the function tray shall be allocated by invokation side and pointer to it is 
 * passed to the function. TxRx only load this tray by actual entry points pointers.
 */
int RIO_TxRxM_Serial_Model_Get_Function_Tray(
    RIO_TXRXM_SERIAL_MODEL_FTRAY_T *ftray             
    );


/* 
 * the function binds TxRx model instance to the environment. The callback tray is
 * allocated outside, loaded by proper callback entry point pointers and passed 
 * through the pointer to it here. 
 * The tray is stored internal in instance's internal data
 */
int RIO_TxRxM_Serial_Model_Bind_Instance(
    RIO_HANDLE_T                    handle,
    RIO_TXRXM_SERIAL_MODEL_CALLBACK_TRAY_T   *ctray   
    );

/*set up seed value for txrx model*/
void RIO_TxRx_Set_Random_Generator(
    unsigned int seed_Value
    );

#endif /* RIO_TXRXM_SERIAL_MODEL_H */





