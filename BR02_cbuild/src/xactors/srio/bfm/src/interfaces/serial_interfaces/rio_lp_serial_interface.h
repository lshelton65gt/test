#ifndef RIO_LP_SERIAL_INTERFACE_H
#define RIO_LP_SERIAL_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_lp_serial_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains rio model 8/16LP-EP link interface 
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_SERIAL_TYPES_H
#include "rio_serial_types.h"
#endif

/*
this function is used by the environment to notify the model about
LP/EP receiver pins values
*/
typedef int (*RIO_SERIAL_GET_PINS)(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
    );

/*
this callback is used by the model to notify the environment about 
LP/EP transmitter pins values
*/
typedef int (*RIO_SERIAL_SET_PINS)(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3       /* transmit data on lane 3*/
    );

#endif /* RIO_LP_SERIAL_INTERFACE_H */

