#ifndef RIO_PL_PLLIGHT_SERIAL_MODEL_H
#define RIO_PL_PLLIGHT_SERIAL_MODEL_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_pl_pllight_serial_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                       to display revision history information.
*
* Description:  file contains user-visible interface of the physical layer
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_PL_INTERFACE_H
#include "rio_pl_interface.h"
#endif

#ifndef RIO_PL_SERIAL_INTERFACE_H
#include "rio_pl_serial_interface.h"
#endif

#ifndef RIO_LP_SERIAL_INTERFACE_H
#include "rio_lp_serial_interface.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif

#ifndef RIO_SERIAL_COMMON_H
#include "rio_serial_common.h"
#endif


/* 
 * function tray to be passed to the environment by the physical layer model
 */
typedef struct {
    RIO_PL_CLOCK_EDGE              rio_PL_Clock_Edge;
    RIO_PL_GSM_REQUEST             rio_PL_GSM_Request;
    RIO_PL_IO_REQUEST              rio_PL_IO_Request;
    RIO_PL_MESSAGE_REQUEST         rio_PL_Message_Request;
    RIO_PL_DOORBELL_REQUEST        rio_PL_Doorbell_Request;
    RIO_PL_CONFIGURATION_REQUEST   rio_PL_Configuration_Request;
    RIO_PL_PACKET_STRUCT_REQUEST   rio_PL_Packet_Struct_Request;
    RIO_PL_SEND_RESPONSE           rio_PL_Send_Response;
    RIO_PL_ACK_REMOTE_REQ          rio_PL_Ack_Remote_Req;
    RIO_SET_CONFIG_REG             rio_PL_Set_Config_Reg;
    RIO_GET_CONFIG_REG             rio_PL_Get_Config_Reg;
    RIO_PL_MODEL_START_RESET       rio_PL_Model_Start_Reset;
    RIO_PL_SERIAL_MODEL_INITIALIZE rio_PL_Serial_Model_Initialize;
    RIO_SERIAL_GET_PINS            rio_PL_Serial_Get_Pins;
    RIO_PRINT_VERSION              rio_PL_Print_Version;
    RIO_DELETE_INSTANCE            rio_PL_Delete_Instance;
    RIO_PL_ENABLE_RND_IDLE_GENERATOR rio_PL_Enable_Rnd_Idle_Generator;
    RIO_PL_SET_RETRY_GENERATOR     rio_PL_Set_Retry_Generator;
    RIO_PL_SET_PNACC_GENERATOR     rio_PL_Set_PNACC_Generator;
    RIO_PL_SET_STOMP_GENERATOR     rio_PL_Set_Stomp_Generator;
    RIO_PL_SET_LRQR_GENERATOR      rio_PL_Set_LRQR_Generator;

    /*GDA:Rnd Generator (PNACC/RETRY)*/
    RIO_PL_ENABLE_RND_PNACC_GENERATOR_INIT     rio_PL_Enable_RND_Pnacc_Generator_Init;
    RIO_PL_ENABLE_RND_RETRY_GENERATOR_INIT     rio_PL_Enable_RND_Retry_Generator_Init;
    /*GDA:Rnd Generator (PNACC/RETRY)*/
 
    /*GDA: Bug#116 Multicast Control Symbol */
    RIO_PL_GENERATE_MULTICAST 			rio_PL_Generate_Multicast;
    /*GDA: Bug#116 Multicast Control Symbol */

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_DS_REQUEST              rio_PL_DS_Request;
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_PL_FC_REQUEST              rio_PL_FC_Request;
    RIO_PL_CONGESTION              rio_PL_Congestion;
    /* GDA: Bug#125 - Support for Flow control packet */
    
    

} RIO_PL_SERIAL_MODEL_FTRAY_T;

/* 
 * callback tray to be used by the physical layer model (supplied by the model 
 * environment). It also includes two contexts: context of 8LP/EP callbacks 
 * interface and context of physical layer interface callbacks. These contexts are
 * used for running corresponding callbacks in proper environment.
 */
typedef struct {
    RIO_PL_ACK_REQUEST      rio_PL_Ack_Request;
    RIO_PL_GET_REQ_DATA     rio_PL_Get_Req_Data;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_GET_REQ_DATA_HW  rio_PL_Get_Req_Data_Hw; 
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    RIO_PL_ACK_RESPONSE     rio_PL_Ack_Response;
    RIO_PL_GET_RESP_DATA    rio_PL_Get_Resp_Data;
    RIO_PL_REQ_TIME_OUT     rio_PL_Req_Time_Out;
    RIO_PL_REMOTE_REQUEST   rio_PL_Remote_Request;
    RIO_PL_REMOTE_RESPONSE  rio_PL_Remote_Response;
    RIO_CONTEXT_T           pl_Interface_Context; 
    RIO_SERIAL_SET_PINS     rio_PL_Set_Pins;
    RIO_CONTEXT_T           lp_Serial_Interface_Context; 
    
    RIO_USER_MSG            rio_User_Msg;
    RIO_CONTEXT_T           rio_Msg;

    RIO_REMOTE_CONFIG_READ  extend_Reg_Read;
    RIO_REMOTE_CONFIG_WRITE extend_Reg_Write;
    RIO_CONTEXT_T           extend_Reg_Context;

    RIO_PCS_PMA_TRANSFER_GRANULE    rio_Serial_Granule_To_Send;
    RIO_PCS_PMA_TRANSFER_GRANULE    rio_Serial_Granule_Received;
   
    RIO_PCS_PMA_TRANSFER_CHARACTER  rio_Serial_Character_Column_To_Send;
    RIO_PCS_PMA_TRANSFER_CHARACTER  rio_Serial_Character_Column_Received;
    
    RIO_PCS_PMA_TRANSFER_CODEGROUP  rio_Serial_Codegroup_Column_To_Send;
    RIO_PCS_PMA_TRANSFER_CODEGROUP  rio_Serial_Codegroup_Received;
    RIO_CONTEXT_T                   rio_Serial_Hooks_Context;
   
    RIO_REQUEST_RECEIVED            rio_Request_Received;
    RIO_RESPONSE_RECEIVED           rio_Response_Received;
    RIO_SYMBOL_TO_SEND              rio_Symbol_To_Send;
    RIO_PACKET_TO_SEND              rio_Packet_To_Send;
    RIO_CONTEXT_T                   rio_Hooks_Context;

   /* GDA: Bug#125 - Support for Flow control packet */
    RIO_PL_FC_TO_SEND              rio_PL_FC_To_Send;
    /* GDA: Bug#125 - Support for Flow control packet */
    


} RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T;

#endif /* RIO_PL_PLLIGHT_SERIAL_MODEL_H */

