#ifndef RIO_SERIAL_COMMON_H
#define RIO_SERIAL_COMMON_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_serial_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes internal PCS/PMA interface.
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_SERIAL_TYPES_H
#include "rio_serial_types.h"
#endif


/*
 * this is a function to transmit (set/get) granule 
 * between PCS/PMA protocol management & data processor submodules
 */
typedef int (*RIO_PCS_PMA_TRANSFER_GRANULE)(
    RIO_CONTEXT_T              handle_context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T      *granule          /* pointer to granule data structure */
);

/*
 * this is a function to transmit (set/get) character
 * between PCS/PMA protocol management & data processor submodules
 */
typedef int (*RIO_PCS_PMA_TRANSFER_CHARACTER)(
    RIO_CONTEXT_T              handle_context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T      *character        /* pointer to character data structure */
);

/*
 * this is a function to transmit (set/get) code group column
 * between PCS/PMA protocol management & data processor submodules
 */
typedef int (*RIO_PCS_PMA_TRANSFER_CODEGROUP)(
    RIO_CONTEXT_T                    handle_context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T        *codegroup        /* pointer to codegroup data structure */
);

#endif /* RIO_SERIAL_COMMON_H */

