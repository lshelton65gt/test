#ifndef RIO_TXRXM_SERIAL_INTERFACE_H
#define RIO_TXRXM_SERIAL_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_txrxm_serial_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes external TxRxM interface.
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_SERIAL_TYPES_H
#include "rio_serial_types.h"
#endif


/*
 * the function notify the instance about control symbol that shall head out
 */
typedef int (*RIO_TXRXM_SERIAL_SYMBOL_REQUEST)(
    RIO_HANDLE_T                handle, 
    RIO_SYMBOL_SERIAL_STRUCT_T  *sym,/*control symbol as a fields struct*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );


/*
 * the function notify the instance about character, that shall head out. For 1x link - one character, for 
 * 4x link - the column consisted of the 4 copies of the requested character 
 */
typedef int (*RIO_TXRXM_SERIAL_CHARACTER_REQUEST)(
    RIO_HANDLE_T                                            handle, 
    RIO_UCHAR_T                                                character,/* character data*/
    RIO_CHARACTER_TYPE_T                                    char_Type, 
    RIO_PLACE_IN_STREAM_T                                    place, /*describe placement in packet*/
    unsigned int                                            granule_Num/* number of granule in packet after which it placed*/
);


/*
 * the function notify the instance about char column that shall head out. For 1x link - 4 characters in a row, for 
 * 4x link - the requested column 
 */
typedef int (*RIO_TXRXM_SERIAL_CHARACTER_COLUMN_REQUEST)(
    RIO_HANDLE_T                handle, 
    RIO_UCHAR_T                    column[RIO_PL_SERIAL_NUMBER_OF_LANES],/* column data (the array size shall be 4)*/
    RIO_CHARACTER_TYPE_T        char_Type[RIO_PL_SERIAL_NUMBER_OF_LANES], /*the array size is 4 bytes*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );


/*
 * the function notify the instance about code group that shall head out. For 1x link - one character, for 
 * 4x link - the column consisted of the 4 copies of the requested code group 
 */
typedef int (*RIO_TXRXM_SERIAL_CODE_GROUP_REQUEST)(
    RIO_HANDLE_T                handle, 
    unsigned int                code_Group,/* code group data- size is 10*/
    RIO_PLACE_IN_STREAM_T         place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );


/*
 * the function notify the instance about code column that shall head out. For 1x link - 4 characters in a row, for 
 * 4x link - the requested column 
 */
typedef int (*RIO_TXRXM_SERIAL_CODE_GROUP_COLUMN_REQUEST)(
    RIO_HANDLE_T                handle, 
    unsigned int                code_Column[RIO_PL_SERIAL_NUMBER_OF_LANES],/* column data (the array size shall be 10x4)*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );



/*
 * the function notify the instance about single character, that shall be inserted, replaced or deleted
 * in the said place of outbound data stream.
 */
typedef int (*RIO_TXRXM_SERIAL_SINGLE_CHARACTER_REQUEST)(
    RIO_HANDLE_T                handle, 
    RIO_UCHAR_T                 character,/* granule data*/
    RIO_CHARACTER_TYPE_T        char_Type, 
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num,/* number of granule in packet after which it placed*/
    RIO_PLACE_IN_GRANULE_T        place_In_Granule, /*describe type of placement in granule, selected by place 
                                                        and granule_Num parameters*/
    unsigned int                character_Num/* number of character in granule selected by 
                                                place and granule_Num parameters, which is 
                                                the object of the request*/
);

/*
 * the function notify the instance about single bit, that shall be inserted, replaced or deleted
 * in the said place of outbound data stream.
 */
typedef int (*RIO_TXRXM_SERIAL_SINGLE_BIT_REQUEST)(
    RIO_HANDLE_T                handle, 
    RIO_SIGNAL_T                bit_Value,/* granule data*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num,/* number of granule in packet after which it placed*/
    RIO_PLACE_IN_GRANULE_T        place_In_Granule, /*describe type of placement in granule, selected by 
                                                    place and granule_Num parameters*/
    unsigned int                character_Num,/* number of character in granule selected by place 
                                                and granule_Num parameters,
                                                which is the object of the request*/
    unsigned int                bit_Num /* number of bit in the said by character_Num parameter 
                                        character in granule selected by place and 
                                        granule_Num parameters, which is the object of the request*/
);


/*
 * the function to send the sync sequence
 */
typedef int (*RIO_TXRXM_SERIAL_COMP_SEQ_REQUEST)(
    RIO_HANDLE_T        handle,
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
    );
  
/*
 * notify the environment about received serial control symbol. 
 */
typedef int (*RIO_TXRXM_SERIAL_SYMBOL_RECEIVED)(
    RIO_CONTEXT_T                context, 
    RIO_SYMBOL_SERIAL_STRUCT_T    *sym,/*control symbol as a struct with type and fields*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );


/*
 * notify the environment about received control symbol with errors. 
 */
typedef int (*RIO_TXRXM_SERIAL_VIOLENT_SYMBOL_RECEIVED)(
    RIO_CONTEXT_T                    context, 
    unsigned int                    symbol_Buffer,/* containing symbol's bit stream (1 granules) */
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num,/*    number of granule in packet after which it placed*/
    RIO_SYMBOL_SERIAL_VIOLATION_T    violation_Type /*describe why symbol can't be considered as good*/
    );


/*
 * notify the environment about received code group. 
 */
typedef int (*RIO_TXRXM_SERIAL_CODE_GROUP_RECEIVED)(
    RIO_CONTEXT_T                context,
    RIO_PCS_PMA_CODE_GROUP_T    code_Group
);

/*
 * notify the environment about received violent code group. 
 */
typedef int (*RIO_TXRXM_SERIAL_VIOLENT_CHARACTER_RECEIVED)(
    RIO_CONTEXT_T                context,
    RIO_PCS_PMA_CODE_GROUP_T    code_Group,
    RIO_PCS_PMA_CHARACTER_T     character,
    RIO_UCHAR_T                    violation_Lane_Mask /*the mask of lanes where the invalid character(s) was (were) detected*/
);

/*
 * notify the environment about received character in case of 1x link, and character column in case of 4x link. 
 */
typedef int (*RIO_TXRXM_SERIAL_CHARACTER_RECEIVED)(
    RIO_CONTEXT_T                context, 
    RIO_PCS_PMA_CHARACTER_T     character
);

/*
 * the function to manage the PCS_PMA machines 
 */
typedef int (*RIO_TXRXM_SERIAL_PCS_PMA_MACHINE_MANAGEMENT)(
    RIO_HANDLE_T        handle,
    RIO_BOOL_T          set_On_Machine, /*RIO_TRUE if seq must be cont. sent*/
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num,/*    number of granule in packet after which it placed*/
    RIO_PCS_PMA_MACHINE_T            machine_Type
);


/*
 * the function allowes to move the bit queue up for one bit on the said lane 
 */
typedef int (*RIO_TXRXM_SERIAL_POP_BIT)(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T  lane_Num, /*0-3*/
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
);

/*
 * the function allowes to move the character queue up for one character on the said lane 
 */
typedef int (*RIO_TXRXM_SERIAL_POP_CHARACTER)(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T  lane_Num, /*0-3*/
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
);

/* 
 * the function is used to initialize and re-initialize the RIO model. It set 
 * TXRXM_SERIAL model instance parameters to requested values and turn the TXRXM_SERIAL model
 * instance to work mode
 */
typedef int (*RIO_TXRXM_SERIAL_MODEL_INITIALIZE)(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_SERIAL_PARAM_SET_T   *param_Set
    );

#endif /* RIO_TXRXM_SERIAL_INTERFACE_H */






