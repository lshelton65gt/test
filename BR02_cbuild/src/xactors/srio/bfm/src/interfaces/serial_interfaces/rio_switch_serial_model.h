#ifndef RIO_SWITCH_SERIAL_MODEL_H
#define RIO_SWITCH_SERIAL_MODEL_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_switch_serial_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  file contains user-visible interface of the serial
*               RapidIO Switch Model
*
* Notes:        
*
******************************************************************************/

#include "rio_common.h"
#include "rio_lp_serial_interface.h"
#include "rio_switch_interface.h"
#include "rio_switch_serial_interface.h"


/* function tray to be passed to the environment by the switch */
typedef struct {

    RIO_SWITCH_START_RESET       rio_Switch_Start_Reset;
    RIO_SWITCH_SERIAL_INITIALIZE rio_Switch_Initialize;
    RIO_SWITCH_CLOCK_EDGE        rio_Switch_Clock_Edge;
    RIO_SERIAL_GET_PINS          rio_Switch_Serial_Get_Pins;
    
    RIO_PRINT_VERSION            rio_Switch_Print_Version;

    RIO_DELETE_INSTANCE          rio_Switch_Delete_Instance;

} RIO_SWITCH_SERIAL_FTRAY_T;


/* callback tray to be used by the switch (supplied by the model environment) */
typedef struct {

    RIO_SERIAL_SET_PINS rio_Switch_Serial_Set_Pins;
    RIO_CONTEXT_T       *lp_Serial_Interface_Context; /* contexts where callback to 1x/4x LP-Serial will run */

    RIO_USER_MSG    rio_User_Msg;
    RIO_CONTEXT_T   rio_Msg_Context;

} RIO_SWITCH_SERIAL_CALLBACK_TRAY_T;


/* the function creates instance of switch and returns handle to its data structure */
int RIO_Switch_Serial_Create_Instance(
    RIO_HANDLE_T            *handle,
    RIO_HANDLE_T            **lp_Serial_Handles, /* array of LP-Serial handles */
    RIO_SWITCH_INST_PARAM_T *param_Set
    );


/* the function returns switch interface functions to be used for model access */
int RIO_Switch_Serial_Get_Function_Tray(
    RIO_SWITCH_SERIAL_FTRAY_T *ftray  /* pointer to the RIO Switch model function tray */
    );


/* the function binds switch instance to the environment */
int RIO_Switch_Serial_Bind_Instance(
    RIO_HANDLE_T                       handle,
    RIO_SWITCH_SERIAL_CALLBACK_TRAY_T  *ctray   /* environment callbacks to be registered inside RIO model instance */
    );

#endif /* RIO_SWITCH_SERIAL_MODEL_H */

