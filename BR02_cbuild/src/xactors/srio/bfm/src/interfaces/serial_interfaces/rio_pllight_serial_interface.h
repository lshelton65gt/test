#ifndef RIO_PLLIGHT_SERIAL_INTERFACE_H
#define RIO_PLLIGHT_SERIAL_INTERFACE_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_pllight_serial_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the logical layer
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_SERIAL_TYPES_H
#include "rio_serial_types.h"
#endif


/* 
 * the function is used to initialize and re-initialize the RIO model. It set 
 * PL model instance parameters to requested values and turn the PL model
 * instance to work mode
 */
typedef int (*RIO_PLLIGHT_SERIAL_MODEL_INITIALIZE)(
    RIO_HANDLE_T         handle,
    RIO_PLLIGHT_SERIAL_PARAM_SET_T   *param_Set
    );

#endif /* RIO_PLLIGHT_SERIAL_INTERFACE_H */

