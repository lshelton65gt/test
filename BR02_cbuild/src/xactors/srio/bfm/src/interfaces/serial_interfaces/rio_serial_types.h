#ifndef RIO_SERIAL_TYPES_H
#define RIO_SERIAL_TYPES_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/serial_interfaces/rio_serial_types.h $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    RapidIO Models types description
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

/*
 * The structure represented data granule consisting from early defined 
 * control data types and type of data granule
 */
#define RIO_PL_SERIAL_CNT_BYTE_IN_GRAN  4 /* count of bytes in granule */
#define RIO_PL_SERIAL_NUMBER_OF_LANES   4
                                        
#define RIO_PL_ALL_LANES_MASK           0xF /* al lanes are chosen */
                                        
#define RIO_PL_BUF_STATUS_MAX           31 /* max buffer size */

/*
#define RIO_PL_COMPENSATION_SEQUENCE_PERIOD     5000
#define RIO_PL_STATUS_SYMBOLS_PERIOD            1024
*/

#define RIO_PL_MAX_PACKET_SIZE                  276

#define RIO_INIT_STATUS_SYMBOLS_TO_RECEIVE         7
#define RIO_INIT_STATUS_SYMBOLS_TO_SEND           15


#define RIO_LANE_MASK_LANE0_LANE2       0x5
#define RIO_LANE_MASK_LANE2             0x4


/*defines the struct of passing and receiving control symbols*/
typedef struct {
    RIO_UCHAR_T first_Char; /*used only for Serial_Symbol_Received callback. 
                            in the request this field will be ignored*/
    RIO_UCHAR_T stype0;
    RIO_UCHAR_T parameter0;
    RIO_UCHAR_T parameter1;
    RIO_UCHAR_T stype1;
    RIO_UCHAR_T cmd;
    RIO_UCHAR_T crc;
    RIO_BOOL_T  is_Crc_Valid;/*show for requests if the CRC could be taken from struct or
                               shall be calculated*/
}RIO_SYMBOL_SERIAL_STRUCT_T;

#define RIO_SYMBOL_STYPE0_SIZE       3
#define RIO_SYMBOL_PARAMETER0_SIZE   5
#define RIO_SYMBOL_PARAMETER1_SIZE   5
#define RIO_SYMBOL_STYPE1_SIZE       3
#define RIO_SYMBOL_CMD_SIZE          3
#define RIO_SYMBOL_CRC_SIZE          5

#define RIO_SYMBOL_STYPE0_MASK       0x07
#define RIO_SYMBOL_PARAMETER0_MASK   0x1F
#define RIO_SYMBOL_PARAMETER1_MASK   0x1F
#define RIO_SYMBOL_STYPE1_MASK       0x07
#define RIO_SYMBOL_CMD_MASK          0x07
#define RIO_SYMBOL_CRC_MASK          0x1F


typedef struct {
    RIO_UCHAR_T                  granule_Date[RIO_PL_SERIAL_CNT_BYTE_IN_GRAN];
    RIO_SYMBOL_SERIAL_STRUCT_T   granule_Struct;
    RIO_BOOL_T                   is_Data; /*RIO_TRUE if data and RIO_FALSE if symbol*/
} RIO_PCS_PMA_GRANULE_T;


typedef enum {
    RIO_SC = 0,
    RIO_PD,
    RIO_K,
    RIO_A,
    RIO_R,
    RIO_D,
    RIO_I /*invalid*/
} RIO_CHARACTER_TYPE_T;


typedef enum {
    PCS_PMA__RUN_DISP_NEG = 0,
    PCS_PMA__RUN_DISP_POS,
    PCS_PMA__RUN_DISP_NOT_CHANGE
} PCS_PMA__RUN_DISP_T;


typedef struct {
    RIO_UCHAR_T             character_Data[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_CHARACTER_TYPE_T    character_Type[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_UCHAR_T             lane_Mask;
} RIO_PCS_PMA_CHARACTER_T;


typedef struct {
    unsigned int         code_Group_Data[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_BOOL_T           is_Running_Disparity_Positive[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_UCHAR_T          lane_Mask;
} RIO_PCS_PMA_CODE_GROUP_T;


/* 
 * lane_ID - is got as log2(lane_Mask)
 * only arguments 1, 2, 4, 8 are valid
 */
extern RIO_UCHAR_T RIO_Log2_Values[];
#define RIO_LOG2_VALUES_TABLE_SIZE  (sizeof(rio_Log2_Values) / sizeof(rio_Log2_Values[0]))


/*
 * the data type represents serial model parameters set.
 */
typedef struct {
    RIO_PL_TRANSP_TYPE_T transp_Type;
    RIO_TR_INFO_T dev_ID;
    RIO_TR_INFO_T *remote_Dev_ID;      /* pointer to the array of deviceIDs for coherence domain participants */
    RIO_WORD_T    lcshbar;
    RIO_WORD_T    lcsbar;

    /*These fields define the initial value  of General Control CSR (offset 0x038 lower word(EF)*/
    RIO_BOOL_T      is_Host;
    RIO_BOOL_T      is_Master_Enable;
    RIO_BOOL_T      is_Discovered;
    
    RIO_WORD_T    config_Space_Size;   /* this parameter defins size of the memory window */
                                       /* mapped to the configuration space               */
    /*
     * according to TWG work only table-based routing is officially supported by 
     * the specification, so this parameter is commented out
     */                                                                         
    /* RIO_ROUTING_METHOD_T    routing_Meth; */

    /*
     * This fields define structure of the address information 
     * for the incoming packets received by this PE and for packets
     * being issued by the PE 
     * The parameters are necessary to set PE LL CSR
     */
    RIO_BOOL_T    ext_Address;         /* extended address field is present in the packets */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */

    RIO_BOOL_T    ext_Mailbox_Support; /* PE supports extended mailbox (msglen=0, mailbox_number=msgseg||mbox) */

    /*TRUE in case os 1x serial link*/
    RIO_BOOL_T        lp_Serial_Is_1x; 

    /*TRUE in case of 4x link used in 1x mode (valid if lp_Serial_Is_1x == RIO_FALSE)*/
    RIO_BOOL_T        is_Force_1x_Mode; 

    /*TRUE in case of 4x link used in 1x mode on lane 0
    (valid if lp_Serial_Is_1x == RIO_FALSE, is_Force_1x_Mode == RIO_TRUE)*/
    RIO_BOOL_T        is_Force_1x_Mode_Lane_0; 

   /*define if output and input are enable after reset(enable to send and receive all kinds
     of packets (if False - only MAINTENANCE requests and responds are processed))*/
    RIO_BOOL_T     input_Is_Enable;
    RIO_BOOL_T     output_Is_Enable;

    
    unsigned int    discovery_Period;   
    unsigned int    silence_Period;   
    unsigned int    comp_Seq_Rate; /*in spec - 5000*/
    unsigned int    status_Sym_Rate; /*in spec - 1024 code groups*/
    
    /* compensation sequence rate ("KRRR"), checked by compensation sequence monitor.
    If "KRRR" sequence rate is larger, than specified error message is outputted.
    Set value comp_Seq_Rate_Check to 0 to disable compensation sequence monitor. */
    unsigned int    comp_Seq_Rate_Check;

    /* the size of compensation sequence 
    if comp_Seq_Size set to 0 => "KRRR" sequence will be generated 
    if comp_Seq_Size set to 1 => "KRR" or "KRRRR" sequence will be generated 
    with random size. if comp_Seq_Size set to 2 => "KR" or "KRRRRR" sequence 
    will be generated with random size. 
    Set up the value to 0, 1 or 2. */
    unsigned int    comp_Seq_Size; 
                                   

    /*
     * these parameters define reservation police for outbound buffer
     */
    RIO_UCHAR_T res_For_3_Out;
    RIO_UCHAR_T res_For_2_Out;
    RIO_UCHAR_T res_For_1_Out;

    /*
     * these parameters define reservation police for inbound buffer
     */
    RIO_UCHAR_T res_For_3_In;
    RIO_UCHAR_T res_For_2_In;
    RIO_UCHAR_T res_For_1_In;

    /*
     * order in that inbound packets are passed to the environment
     * pass_By_Prio     if TRUE the most priority packet goes first
     *                      else the oldest
     */
    RIO_BOOL_T pass_By_Prio;
    RIO_BOOL_T transmit_Flow_Control_Support; /*RIO_TRUE - flow control is detected automaticly,
                                                RIO_FALSE - only rcv flow control is supported*/
    RIO_BOOL_T enable_LRQ_Resending;          /*if true the models will resend the link request
                                                symbol if link responce is not accepted or
                                                accepted lresponce with incorrect ackid
                                                if false then model will fail to the unrec. error*/

    unsigned int    icounter_Max;  /*max value of Icounter for switching sync FSM 
                                    to the NO_SYNC state in spec - 2*/
    RIO_BOOL_T data_Corrupted; /*GDA:Bug#139 remove the need to recompute crc */

} RIO_SERIAL_PARAM_SET_T;


/*
 * the data type represents parameters set for the serial switch model
 */
typedef struct {

    /*TRUE in case of 1x serial link*/
    RIO_BOOL_T *lp_Serial_Is_1x_Array; 

    /*TRUE in case of 4x link used in 1x mode (valid if lp_Serial_Is_1x == RIO_FALSE)*/
    RIO_BOOL_T *is_Force_1x_Mode_Array; 

    /*TRUE in case of 4x link used in 1x mode on lane 0
    (valid if lp_Serial_Is_1x == RIO_FALSE, is_Force_1x_Mode == RIO_TRUE)*/
    RIO_BOOL_T *is_Force_1x_Mode_Lane_0_Array; 
    
    unsigned int    discovery_Period;   
    unsigned int    silence_Period;   
    unsigned int    comp_Seq_Rate;   /*in spec - 5000*/
    unsigned int    status_Sym_Rate; /*in spec - 1024 code groups*/

    /* compensation sequence rate ("KRRR"), checked by compensation sequence monitor.
    If "KRRR" sequence rate is larger, than specified error message is outputted.
    Set value comp_Seq_Rate_Check to 0 to disable compensation sequence monitor. */
    unsigned int    comp_Seq_Rate_Check;

    /* the size of compensation sequence 
    if comp_Seq_Size set to 0 => "KRRR" sequence will be generated 
    if comp_Seq_Size set to 1 => "KRR" or "KRRRR" sequence will be generated 
    with random size. if comp_Seq_Size set to 2 => "KR" or "KRRRRR" sequence 
    will be generated with random size. 
    Set up the value to 0, 1 or 2. */
    unsigned int    comp_Seq_Size;

    RIO_ROUTE_TABLE_ENTRY_T *routing_Table;
    unsigned int            rout_Table_Size; /* size of routing table */

    /*
     * according to TWG work only table-based routing is officially supported by 
     * the specification, so this parameter is commented out
     */                                                                         
    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */
    unsigned int  icounter_Max;  /*max value of Icounter for switching sync FSM 
                                    to the NO_SYNC state in spec - 2*/

} RIO_SWITCH_SERIAL_PARAM_SET_T;


/*
 * the data type represents PL model parameters set.
 */
typedef struct {

    RIO_PL_TRANSP_TYPE_T transp_Type;

    RIO_TR_INFO_T dev_ID;

    /* values of corresponding registers */
    RIO_WORD_T    lcshbar;
    RIO_WORD_T    lcsbar;
    
    /*These fields define the initial value  of General Control CSR (offset 0x038 lower word(EF)*/

    RIO_BOOL_T      is_Host;
    RIO_BOOL_T      is_Master_Enable;
    RIO_BOOL_T      is_Discovered;

    /*
     * These fields define structure of the address information 
     * for the incoming packets recived by this PE
     */
    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */
    
    RIO_BOOL_T    ext_Mailbox_Support; /* PE supports extended mailbox (msglen=0, mailbox_number=msgseg||mbox) */

    /*TRUE in case os 1x serial link*/
    RIO_BOOL_T        lp_Serial_Is_1x; 

    /*TRUE in case of 4x link used in 1x mode (valid if lp_Serial_Is_1x == RIO_FALSE)*/
    RIO_BOOL_T        is_Force_1x_Mode; 

    /*TRUE in case of 4x link used in 1x mode on lane 0
    (valid if lp_Serial_Is_1x == RIO_FALSE, is_Force_1x_Mode == RIO_TRUE)*/
    RIO_BOOL_T        is_Force_1x_Mode_Lane_0; 
    
    /*define if output and input are enable after reset(enable to send and receive all kinds
    of packets (if False - only MAINTENANCE requests and responds are processed))*/
    RIO_BOOL_T     input_Is_Enable;
    RIO_BOOL_T     output_Is_Enable;

    unsigned int    discovery_Period;   
    unsigned int    silence_Period;   

    unsigned int    comp_Seq_Rate; /*in spec - 5000*/
    unsigned int    status_Sym_Rate; /*in spec - 1024 code groups*/

    /* compensation sequence rate ("KRRR"), checked by compensation sequence monitor.
    If "KRRR" sequence rate is larger, than specified error message is outputted.
    Set value comp_Seq_Rate_Check to 0 to disable compensation sequence monitor. */
    unsigned int    comp_Seq_Rate_Check;

    /* the size of compensation sequence 
    if comp_Seq_Size set to 0 => "KRRR" sequence will be generated 
    if comp_Seq_Size set to 1 => "KRR" or "KRRRR" sequence will be generated 
    with random size. if comp_Seq_Size set to 2 => "KR" or "KRRRRR" sequence 
    will be generated with random size. 
    Set up the value to 0, 1 or 2. */
    unsigned int    comp_Seq_Size; 

    /*
     * these parameters define reservation police for outbound buffer
     */
    RIO_UCHAR_T res_For_3_Out;
    RIO_UCHAR_T res_For_2_Out;
    RIO_UCHAR_T res_For_1_Out;

    /*
     * these parameters define reservation police for inbound buffer
     */
    RIO_UCHAR_T res_For_3_In;
    RIO_UCHAR_T res_For_2_In;
    RIO_UCHAR_T res_For_1_In;

    /*
     * order in that inbound packets are passed to the environment
     * pass_By_Prio     if TRUE the most priority packet goes first
     *                      else the oldest
     */
    RIO_BOOL_T pass_By_Prio;

    /*new parameters*/

    RIO_BOOL_T transmit_Flow_Control_Support; /*RIO_TRUE - flow control is detected automaticly,
                                                RIO_FALSE - only rcv flow control is supported*/

    RIO_BOOL_T enable_Retry_Return_Code;      /*if true pl will return RIO_RETRY code*/
    RIO_BOOL_T enable_LRQ_Resending;          /*if true the models will resend the link request
                                                symbol if link responce is not accepted or
                                                accepted lresponce with incorrect ackid
                                                if false then model will fail to the unrec. error*/

    unsigned int    icounter_Max;  /*max value of Icounter for switching sync FSM 
                                    to the NO_SYNC state in spec - 2*/
    RIO_BOOL_T data_Corrupted; /*GDA:Bug#139 remove the need to recompute crc */

} RIO_PL_SERIAL_PARAM_SET_T;

/*
 * the data type represents PLLIGHT model parameters set.
 */
typedef struct {

    RIO_PL_TRANSP_TYPE_T transp_Type;

    RIO_TR_INFO_T dev_ID;

    /*
     * These fields define structure of the address information 
     * for the incoming packets recived by this PE
     */
    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */
    
    /*TRUE in case os 1x serial link*/
    RIO_BOOL_T        lp_Serial_Is_1x; 

    /*TRUE in case of 4x link used in 1x mode (valid if lp_Serial_Is_1x == RIO_FALSE)*/
    RIO_BOOL_T        is_Force_1x_Mode; 

    /*TRUE in case of 4x link used in 1x mode on lane 0
    (valid if lp_Serial_Is_1x == RIO_FALSE, is_Force_1x_Mode == RIO_TRUE)*/
    RIO_BOOL_T        is_Force_1x_Mode_Lane_0; 
    
    unsigned int    discovery_Period;   
    unsigned int    silence_Period;   

    unsigned int    comp_Seq_Rate; /*in spec - 5000*/
    unsigned int    status_Sym_Rate; /*in spec - 1024 code groups*/

    /* compensation sequence rate ("KRRR"), checked by compensation sequence monitor.
    If "KRRR" sequence rate is larger, than specified error message is outputted.
    Set value comp_Seq_Rate_Check to 0 to disable compensation sequence monitor. */
    unsigned int    comp_Seq_Rate_Check;

    /* the size of compensation sequence 
    if comp_Seq_Size set to 0 => "KRRR" sequence will be generated 
    if comp_Seq_Size set to 1 => "KRR" or "KRRRR" sequence will be generated 
    with random size. if comp_Seq_Size set to 2 => "KR" or "KRRRRR" sequence 
    will be generated with random size. 
    Set up the value to 0, 1 or 2. */
    unsigned int    comp_Seq_Size; 

    /*new parameters*/

    RIO_BOOL_T transmit_Flow_Control_Support; /*RIO_TRUE - flow control is detected automaticly,
                                                RIO_FALSE - only rcv flow control is supported*/

    RIO_BOOL_T enable_LRQ_Resending;          /*if true the models will resend the link request
                                                symbol if link responce is not accepted or
                                                accepted lresponce with incorrect ackid
                                                if false then model will fail to the unrec. error*/

    unsigned int    icounter_Max;  /*max value of Icounter for switching sync FSM 
                                    to the NO_SYNC state in spec - 2*/

} RIO_PLLIGHT_SERIAL_PARAM_SET_T;

/*the type represent the serial TXRX model inst param set*/
typedef struct {
    /*outbound buffers sizes*/
    unsigned int packets_Buffer_Size;
    unsigned int symbols_Buffer_Size;
    unsigned int character_Buffer_Size;
    unsigned int character_Column_Buffer_Size;
    unsigned int code_Group_Buffer_Size;
    unsigned int code_Group_Column_Buffer_Size;
    unsigned int single_Bit_Buffer_Size;
    unsigned int single_Character_Buffer_Size;
    unsigned int comp_Seq_Request_Buffer_Size;
    unsigned int management_Request_Buffer_Size; /*to switch on/of machine and etc*/
    unsigned int pop_Char_Buffer_Size;
    unsigned int pop_Bit_Buffer_Size;
} RIO_TXRXM_SERIAL_INST_PARAM_T;

/*the type represent the serial TXRX model init param set*/
typedef struct {
    /*TRUE in case os 1x serial link*/
    RIO_BOOL_T        lp_Serial_Is_1x; 

    /*TRUE in case of 4x link used in 1x mode (valid if lp_Serial_Is_1x == RIO_FALSE)*/
    RIO_BOOL_T        is_Force_1x_Mode; 

    /*TRUE in case of 4x link used in 1x mode on lane 0
    (valid if lp_Serial_Is_1x == RIO_FALSE, is_Force_1x_Mode == RIO_TRUE)*/
    RIO_BOOL_T        is_Force_1x_Mode_Lane_0; 

    /*defines if extended address exists in inbound packets*/
    RIO_BOOL_T      is_Ext_Address_Valid; 
    RIO_BOOL_T      is_Ext_Address_16;

    /*RIO_TRUE if init seq shall be performed by txrx model itself.
    In this case the sync and align management functions don't work (?)*/
    RIO_BOOL_T      is_Init_Proc_On; 
    unsigned int    silence_Period;   /*clocks*/

    unsigned int    discovery_Period; /*clocks ????*/

    RIO_BOOL_T      is_Comp_Seq_Gen_On;
    unsigned int    comp_Seq_Rate; /*in spec - 5000 clocks*/

    RIO_BOOL_T      is_Status_Sym_Gen_On;
    unsigned int    status_Sym_Rate; /*in code groups (spec - 1024 code groups)*/

    /* the size of compensation sequence 
    if comp_Seq_Size set to 0 => "KRRR" sequence will be generated 
    if comp_Seq_Size set to 1 => "KRR" or "KRRRR" sequence will be generated 
    with random size. if comp_Seq_Size set to 2 => "KR" or "KRRRRR" sequence 
    will be generated with random size. 
    Set up the value to 0, 1 or 2. */
    unsigned int    comp_Seq_Size;

    /* compensation sequence rate ("KRRR"), checked by compensation sequence monitor.
    If "KRRR" sequence rate is larger, than specified error message is outputted.
    Set value comp_Seq_Rate_Check to 0 to disable compensation sequence monitor. */
    unsigned int    comp_Seq_Rate_Check;

    unsigned int    icounter_Max;  /*max value of Icounter 
                                    for switching to the NO_SYNC in spec - 2*/

}RIO_TXRXM_SERIAL_PARAM_SET_T;


typedef enum {
    RIO_SIGNAL_0 = 0,
    RIO_SIGNAL_1 = 1
}RIO_SIGNAL_T;

#define RIO_SIGNAL_NO_SIGNAL 0

typedef enum {
    RIO_PCS_PMA_MACHINE_INIT = 0,
    RIO_PCS_PMA_MACHINE_COMP_SEQ_GEN,
    RIO_PCS_PMA_MACHINE_STATUS_SYM_GEN
}RIO_PCS_PMA_MACHINE_T;


typedef enum {
    RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_CRC = 0,
    RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_SC_PD
}RIO_SYMBOL_SERIAL_VIOLATION_T;

typedef enum {
    RIO_PLACE_IN_GRANULE_EMBEDDED = 0,
    RIO_PLACE_IN_GRANULE_REPLACING,
    RIO_PLACE_IN_GRANULE_DELETING
}RIO_PLACE_IN_GRANULE_T;

/* values of the Port Control CSR fields */
#define RIO_PORT_WIDTH__SINGLE_LANE                 0
#define RIO_PORT_WIDTH__FOUR_LANE                   1
#define RIO_INITIALIZED_PORT_WIDTH__SINGLE_LANE_0   0
#define RIO_INITIALIZED_PORT_WIDTH__SINGLE_LANE_2   1
#define RIO_INITIALIZED_PORT_WIDTH__FOUR_LANE       2
#define RIO_PORT_WIDTH_OVERRIDE__NO_OVERRIDE        0
#define RIO_PORT_WIDTH_OVERRIDE__SINGLE_LANE_0      2
#define RIO_PORT_WIDTH_OVERRIDE__SINGLE_LANE_2      3


#endif /* RIO_SERIAL_TYPES_H */


