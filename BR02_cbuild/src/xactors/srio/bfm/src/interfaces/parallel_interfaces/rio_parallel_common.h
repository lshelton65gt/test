#ifndef RIO_PARALLEL_COMMON_H
#define RIO_PARALLEL_COMMON_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_parallel_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains functions which are common to logical and 
*               physical layers
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

#ifndef RIO_COMMON_H
#include "rio_common.h"
#endif

#ifndef RIO_PARALLEL_TYPES_H
#include "rio_parallel_types.h"
#endif

/*
 * callback is called in case of symbol receiving
 */

typedef int (*RIO_GRANULE_RECEIVED)(
    RIO_CONTEXT_T           context,
    RIO_GRANULE_STRUCT_T*   granule_Struct      /* struct, containing symbol's fields */
    );

/*
 * to made the init machine to loose sync. 
 */
typedef int (*RIO_PL_PARALLEL_INIT_MACHINE_LOST_SYNC_COMMAND)(
    RIO_HANDLE_T                  handle
    );


typedef int (*RIO_PL_FORCE_EOP_INSERTION)(
    RIO_HANDLE_T handle,  
    RIO_BOOL_T enable_Force);




#endif  /* RIO_PARALLEL_COMMON_H */


