#ifndef RIO_SWITCH_PARALLEL_INTERFACE_H
#define RIO_SWITCH_PARALLEL_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_switch_parallel_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  file contains RapidIO Switch Model parallel specific 
*               interface functions
*
* Notes:        
*
******************************************************************************/

#include "rio_types.h"
#include "rio_parallel_types.h"

/* the function is used to initialize and re-initialize the switch */
typedef int (*RIO_SWITCH_PARALLEL_INITIALIZE)(
    RIO_HANDLE_T                    handle,
    RIO_SWITCH_PARALLEL_PARAM_SET_T *param_Set
    );


#endif /* RIO_SWITCH_PARALLEL_INTERFACE_H */

