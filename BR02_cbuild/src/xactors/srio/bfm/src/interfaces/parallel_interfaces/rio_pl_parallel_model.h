#ifndef RIO_PL_PARALLEL_MODEL_H
#define RIO_PL_PARALLEL_MODEL_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_pl_parallel_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the physical layer
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_PL_PLLIGHT_PARALLEL_MODEL_H
#include "rio_pl_pllight_parallel_model.h"
#endif






/* 
 * the function creates instance of PL model and returns handle to its data structure.
 * there are some create parameters, which can be defined only at the moment of 
 * instance creation. They are: size (in count of whole packets) of inbound buffer and
 * size (the same) of outbound buffer.
 * After the instance's been created it is in reset mode. It's necessary to
 * invoke Initialize function to turn it into work mode.
 */
int RIO_PL_Parallel_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_MODEL_INST_PARAM_T  *param
    );

/* 
 * the function returns PL model interface functions to be used for model access. 
 * the function tray shall be allocated by invokation side and pointer to it is 
 * passed to the function. PL only load this tray by actual entry points pointers.
 */
int RIO_PL_Parallel_Model_Get_Function_Tray(
    RIO_PL_PARALLEL_MODEL_FTRAY_T *ftray             
    );


/* 
 * the function binds PL model instance to the environment. The callback tray is
 * allocated outside, loaded by proper callback entry point pointers and passed 
 * through the pointer to it here. 
 * The tray is stored internal in instance's internal data
 */
int RIO_PL_Parallel_Model_Bind_Instance(
    RIO_HANDLE_T                    handle,
    RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T   *ctray   
    );

#endif /* RIO_PL_PARALLEL_MODEL_H */

