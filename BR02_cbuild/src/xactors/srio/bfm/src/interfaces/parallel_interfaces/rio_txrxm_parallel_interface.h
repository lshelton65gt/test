#ifndef RIO_TXRXM_PARALLEL_INTERFACE_H
#define RIO_TXRXM_PARALLEL_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_txrxm_parallel_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    Describes external TxRxM interface.
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif


#ifndef RIO_PARALLEL_TYPES_H
#include "rio_parallel_types.h"
#endif

/*
 * the function notify the instance about control symbol that shall head out
 */
typedef int (*RIO_TXRXM_SYMBOL_REQUEST)(
    RIO_HANDLE_T                handle, 
    RIO_SYMBOL_STRUCT_T            *sym,/*control symbol as a fields struct*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

/*
 * the function notify the instance about training control symbol that shall head out
 */
typedef int (*RIO_TXRXM_TRAINING_REQUEST)(
    RIO_HANDLE_T                handle, 
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

/*
 * the function notify the instance about data granule that shall head out
 */
typedef int (*RIO_TXRXM_GRANULE_REQUEST)(
    RIO_HANDLE_T                handle, 
    unsigned long               granule,/* granule data*/
    RIO_BOOL_T                  has_Frame_Toggled, /*if frame has toggled at the granule begin*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );
/*
 * notify the environment about received control symbol with errors. 
 */
typedef int (*RIO_TXRXM_SYMBOL_RECEIVED)(
    RIO_CONTEXT_T                context, 
    RIO_SYMBOL_STRUCT_T         *sym,/*control symbol as a struct with type and fields*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

/*
 * notify the environment about received control symbol with errors. 
 */
typedef int (*RIO_TXRXM_TRAINING_RECEIVED)(
    RIO_CONTEXT_T                context, 
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

typedef int (*RIO_TXRXM_VIOLENT_SYMBOL_RECEIVED)(
    RIO_CONTEXT_T                context, 
    unsigned int                symbol_Buffer,/* containing symbol's bit stream (1 granules) */
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num,/*    number of granule in packet after which it placed*/
    RIO_SYMBOL_VIOLATION_T        violation_Type /*describe why symbol can't be considered as good*/
    );


/*
 * notify the environment about received granule. 
 */
typedef int (*RIO_TXRXM_GRANULE_RECEIVED)(
    RIO_CONTEXT_T                context,
    RIO_GRANULE_TYPE_T          granule_Type,
    unsigned int                granule,/* granule data */
    RIO_BOOL_T                  has_Frame_Toggled /*if frame has toggled at the granule begin*/
    );

/* 
 * the function is used to initialize and re-initialize the RIO model. It set 
 * TXRXM model instance parameters to requested values and turn the TXRXM model
 * instance to work mode
 */
typedef int (*RIO_TXRXM_PARALLEL_MODEL_INITIALIZE)(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_PARALLEL_PARAM_SET_T   *param_Set
    );

/*
    Request for performing some events such as
    on\off Training module
*/
typedef int (*RIO_TXRXM_PARALLEL_MODEL_EVENT_REQ)(
    RIO_HANDLE_T                handle, 
    RIO_TXRX_EVENT_T            event,/* event*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
);

typedef int (*RIO_TXRXM_PARALLEL_INIT_MACHINE_LOST_SYNC_COMMAND)(
    RIO_HANDLE_T                handle
);

#endif /* RIO_TXRXM_PARALLEL_INTERFACE_H */









