#ifndef RIO_PLLIGHT_PARALLEL_MODEL_H
#define RIO_PLLIGHT_PARALLEL_MODEL_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_pllight_parallel_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the physical layer light
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_PL_PLLIGHT_PARALLEL_MODEL_H
#include "rio_pl_pllight_parallel_model.h"
#endif

#ifndef RIO_PLLIGHT_INTERFACE_H
#include "rio_pllight_interface.h"
#endif

#ifndef RIO_PLLIGHT_PARALLEL_INTERFACE_H
#include "rio_pllight_parallel_interface.h"
#endif


/* 
 * function tray to be passed to the environment by the physical layer light model
 */
typedef struct {
    RIO_PL_CLOCK_EDGE            rio_PL_Clock_Edge;
    RIO_PLLIGHT_SEND_PACKET           rio_PLLight_Send_Packet;
    RIO_PLLIGHT_SEND_PACKET_STREAM           rio_PLLight_Send_Packet_Stream;
    RIO_PLLIGHT_SET_PORT_LINK_TIME_OUT       rio_PLLight_Set_Port_Link_Time_Out;
    RIO_PL_MODEL_START_RESET     rio_PL_Model_Start_Reset;
    RIO_PLLIGHT_PARALLEL_MODEL_INITIALIZE      rio_PLLight_Model_Initialize;
    RIO_PH_GET_PINS              rio_PL_Get_Pins;
    RIO_PRINT_VERSION            rio_PL_Print_Version;
    RIO_PL_PARALLEL_INIT_MACHINE_LOST_SYNC_COMMAND rio_PL_Lost_Sync_Command;
    RIO_DELETE_INSTANCE           rio_PL_Delete_Instance;
    RIO_PL_ENABLE_RND_IDLE_GENERATOR rio_PL_Enable_Rnd_Idle_Generator;
    RIO_PL_FORCE_EOP_INSERTION    rio_PL_Force_EOP_Insertion;
    RIO_PL_SET_RETRY_GENERATOR   rio_PL_Set_Retry_Generator;
    RIO_PL_SET_PNACC_GENERATOR   rio_PL_Set_PNACC_Generator;
    RIO_PL_SET_STOMP_GENERATOR   rio_PL_Set_Stomp_Generator;
    RIO_PL_SET_LRQR_GENERATOR    rio_PL_Set_LRQR_Generator;

    /*GDA:Rnd Generator(PNACC/RETRY)*/
    RIO_PL_ENABLE_RND_PNACC_GENERATOR_INIT     rio_PL_Enable_RND_Pnacc_Generator_Init;
    RIO_PL_ENABLE_RND_RETRY_GENERATOR_INIT     rio_PL_Enable_RND_Retry_Generator_Init;
   /*GDA:Rnd Generator(PNACC/RETRY)*/

   /*GDA: Bug#116 Multicast Control Symbol */
    RIO_PL_GENERATE_MULTICAST 		       rio_PL_Generate_Multicast;
   /*GDA: Bug#116 Multicast Control Symbol */

} RIO_PLLIGHT_PARALLEL_MODEL_FTRAY_T;


/* 
 * callback tray to be used by the physical layer light model (supplied by the model 
 * environment). It also includes two contexts: context of 8LP/EP callbacks 
 * interface and context of physical layer light interface callbacks. These contexts are
 * used for running corresponding callbacks in proper environment.
 */
typedef struct {
    RIO_PLLIGHT_PACKET_RECEIVED rio_PLLight_Packet_Received;
    RIO_PLLIGHT_PACKET_ACKNOWLEDGED rio_PLLight_Packet_Acknowledged;
    RIO_CONTEXT_T           pl_Interface_Context; 
    RIO_PH_SET_PINS         rio_PL_Set_Pins;
    RIO_CONTEXT_T           lpep_Interface_Context; 
    
    RIO_USER_MSG            rio_User_Msg;
    RIO_CONTEXT_T           rio_Msg;

    RIO_GRANULE_RECEIVED    rio_Granule_Received;
    RIO_SYMBOL_TO_SEND      rio_Symbol_To_Send;
    RIO_PACKET_TO_SEND      rio_Packet_To_Send;
    RIO_CONTEXT_T           rio_Hooks_Context;
} RIO_PLLIGHT_PARALLEL_MODEL_CALLBACK_TRAY_T;



/* 
 * the function creates instance of PL light model and returns handle to its data structure.
 * there are some create parameters, which can be defined only at the moment of 
 * instance creation. They are: size (in count of whole packets) of inbound buffer and
 * size (the same) of outbound buffer.
 * After the instance's been created it is in reset mode. It's necessary to
 * invoke Initialize function to turn it into work mode.
 */
int RIO_PLLight_Parallel_Model_Create_Instance(
    RIO_HANDLE_T *handle,
    RIO_PLLIGHT_MODEL_INST_PARAM_T *param
	);

/* 
 * the function returns PL light model interface functions to be used for model access. 
 * the function tray shall be allocated by invokation side and pointer to it is 
 * passed to the function. PL only load this tray by actual entry points pointers.
 */
int RIO_PLLight_Parallel_Model_Get_Function_Tray(
    RIO_PLLIGHT_PARALLEL_MODEL_FTRAY_T *ftray
	);

/* 
 * the function binds PL light model instance to the environment. The callback tray is
 * allocated outside, loaded by proper callback entry point pointers and passed 
 * through the pointer to it here. 
 * The tray is stored internal in instance's internal data
 */
int RIO_PLLight_Parallel_Model_Bind_Instance(
    RIO_HANDLE_T handle,
    RIO_PLLIGHT_PARALLEL_MODEL_CALLBACK_TRAY_T *ctray
	);

#endif /* RIO_PLLIGHT_PARALLEL_MODEL_H */

