#ifndef RIO_SWITCH_PARALLEL_MODEL_H
#define RIO_SWITCH_PARALLEL_MODEL_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_switch_parallel_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  file contains user-visible interface of the parallel
*               RapidIO Switch Model
*
* Notes:        
*
******************************************************************************/

#include "rio_common.h"
#include "rio_lp_ep_interface.h"
#include "rio_switch_interface.h"
#include "rio_switch_parallel_interface.h"


/* function tray to be passed to the environment by the switch */
typedef struct {

    RIO_SWITCH_START_RESET         rio_Switch_Start_Reset;
    RIO_SWITCH_PARALLEL_INITIALIZE rio_Switch_Initialize;
    RIO_SWITCH_CLOCK_EDGE          rio_Switch_Clock_Edge;
    RIO_PH_GET_PINS                rio_Ph_Get_Pins;
    
    RIO_PRINT_VERSION              rio_Switch_Print_Version;

    RIO_DELETE_INSTANCE            rio_Switch_Delete_Instance;

} RIO_SWITCH_PARALLEL_FTRAY_T;


/* callback tray to be used by the switch (supplied by the model environment) */
typedef struct {

    RIO_PH_SET_PINS rio_Ph_Set_Pins;
    RIO_CONTEXT_T   *lpep_Interface_Context;    /* contexts where callback to 8LP-EP will run */

    RIO_USER_MSG    rio_User_Msg;
    RIO_CONTEXT_T   rio_Msg_Context;

} RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T;


/* the function creates instance of switch and returns handle to its data structure */
int RIO_Switch_Parallel_Create_Instance(
    RIO_HANDLE_T            *handle,
    RIO_HANDLE_T            **lp_Ep_Handles, /* array of LP/EP handles */
    RIO_SWITCH_INST_PARAM_T *param_Set
    );


/* the function returns switch interface functions to be used for model access */
int RIO_Switch_Parallel_Get_Function_Tray(
    RIO_SWITCH_PARALLEL_FTRAY_T *ftray  /* pointer to the RIO model function tray */
    );


/* the function binds switch instance to the environment */
int RIO_Switch_Parallel_Bind_Instance(
    RIO_HANDLE_T                         handle,
    RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T  *ctray   /* environment callbacks to be registered inside RIO model instance */
    );

#endif /* RIO_SWITCH_PARALLEL_MODEL_H */

