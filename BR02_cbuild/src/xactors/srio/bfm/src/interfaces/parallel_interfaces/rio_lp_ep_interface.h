#ifndef RIO_LP_EP_INTERFACE_H
#define RIO_LP_EP_INTERFACE_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_lp_ep_interface.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains rio model 8/16LP-EP link interface 
*
* Notes:        
*
******************************************************************************/

#ifndef RIO_TYPES_H
#include "rio_types.h"
#endif

/*
this function is used by the environment to notify the model about
LP/EP receiver pins values
*/
typedef int (*RIO_PH_GET_PINS)(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_BYTE_T frame,    /* receive frame */
    RIO_BYTE_T rd,       /* receive data */
    RIO_BYTE_T rdl,      /* receive data low part */
    RIO_BYTE_T rclk      /* receive clock */
    );

/*
this callback is used by the model to notify the environment about 
LP/EP transmitter pins values
*/
typedef int (*RIO_PH_SET_PINS)(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_BYTE_T tframe,     /* transmit frame */
    RIO_BYTE_T td,         /* transmit data */
    RIO_BYTE_T tdl,        /* transmit data low part */
    RIO_BYTE_T tclk        /* transmit clock */
    );

#endif /* RIO_LP_EP_INTERFACE_H */

