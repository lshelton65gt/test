#ifndef RIO_PARALLEL_MODEL_H
#define RIO_PARALLEL_MODEL_H

/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_parallel_model.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:  file contains user-visible interface of the logical layer
*
* Notes:        
*
******************************************************************************/


#include "rio_ll_interface.h"
#include "rio_pl_interface.h"
#include "rio_common.h"

#include "rio_parallel_interface.h"
#include "rio_pl_parallel_interface.h"
#include "rio_lp_ep_interface.h"
#include "rio_parallel_common.h"
#include "rio_pl_parallel_model.h"

/* function tray to be passed to the environment by the RIO model */
typedef struct {
    /* call to these functions will initialize instance as either in PE or PL configuration */
    RIO_PARALLEL_MODEL_INITIALIZE         rio_Init;   /* call this function to initialize instance as PE */
    RIO_PL_PARALLEL_MODEL_INITIALIZE      rio_PL_Model_Initialize; /* call this function to initialize instance as PL */

    /* LL API (logical layer interface) - accessible only for PE instance configuration */
    RIO_GSM_REQUEST              rio_GSM_Request;
    RIO_IO_REQUEST               rio_IO_Request;
    RIO_MESSAGE_REQUEST          rio_Message_Request;
    RIO_DOORBELL_REQUEST         rio_Doorbell_Request;
    RIO_CONFIGURATION_REQUEST    rio_Conf_Request;
    RIO_PL_PACKET_STRUCT_REQUEST rio_Packet_Struct_Request;
    RIO_SNOOP_RESPONSE           rio_Snoop_Response;
    RIO_SET_MEMORY_DATA          rio_Set_Mem_Data;
    RIO_GET_MEMORY_DATA          rio_Get_Mem_Data;
    
    RIO_MODEL_START_RESET        rio_Start_Reset;

    /* API accessible in both LL and PL instance configurations */
    RIO_SET_CONFIG_REG           rio_Set_Conf_Reg;
    RIO_GET_CONFIG_REG           rio_Get_Conf_Reg;

    RIO_PL_CLOCK_EDGE            rio_PL_Clock_Edge;
    RIO_PH_GET_PINS              rio_PL_Get_Pins;
    RIO_DELETE_INSTANCE          rio_Delete_Instance;

    RIO_PRINT_VERSION            rio_Print_Version;

    /* PL API (physical layer interface) - accessible only in PL instance configuration */
    RIO_PL_GSM_REQUEST           rio_PL_GSM_Request;
    RIO_PL_IO_REQUEST            rio_PL_IO_Request;
    RIO_PL_MESSAGE_REQUEST       rio_PL_Message_Request;
    RIO_PL_DOORBELL_REQUEST      rio_PL_Doorbell_Request;
    RIO_PL_CONFIGURATION_REQUEST rio_PL_Configuration_Request;
    RIO_PL_PACKET_STRUCT_REQUEST rio_PL_Packet_Struct_Request;
    RIO_PL_SEND_RESPONSE         rio_PL_Send_Response;
    RIO_PL_ACK_REMOTE_REQ        rio_PL_Ack_Remote_Req;

    RIO_PL_MODEL_START_RESET     rio_PL_Model_Start_Reset;

    RIO_PL_PARALLEL_INIT_MACHINE_LOST_SYNC_COMMAND rio_PL_Lost_Sync_Command;

    RIO_PL_ENABLE_RND_IDLE_GENERATOR rio_PL_Enable_Rnd_Idle_Generator;
    RIO_PL_FORCE_EOP_INSERTION rio_PL_Force_EOP_Insertion;
    
    RIO_PL_SET_RETRY_GENERATOR   rio_PL_Set_Retry_Generator;
    RIO_PL_SET_PNACC_GENERATOR   rio_PL_Set_PNACC_Generator;
    RIO_PL_SET_STOMP_GENERATOR   rio_PL_Set_Stomp_Generator;
    RIO_PL_SET_LRQR_GENERATOR    rio_PL_Set_LRQR_Generator;

    /*GDA: Rnd Generator (PNACC/RETRY)*/
    RIO_PL_ENABLE_RND_PNACC_GENERATOR_INIT     rio_PL_Enable_RND_Pnacc_Generator_Init;
    RIO_PL_ENABLE_RND_RETRY_GENERATOR_INIT     rio_PL_Enable_RND_Retry_Generator_Init;
    /*GDA: Rnd Generator (PNACC/RETRY)*/

    /*GDA: Bug#116 Multicast Control Symbol */
    RIO_PL_GENERATE_MULTICAST 			rio_PL_Generate_Multicast;
    /*GDA: Bug#116 Multicast Control Symbol */
    
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_DS_REQUEST               rio_DS_Request;  
    RIO_PL_DS_REQUEST            rio_PL_DS_Request; 
   /* GDA: Bug#124 - Support for Data Streaming packet */

   /* GDA: Bug#125 - Support for Flow control packet */
     RIO_FC_REQUEST               rio_FC_Request;
     RIO_PL_FC_REQUEST            rio_PL_FC_Request;
     RIO_CONGESTION               rio_Congestion;
     RIO_PL_CONGESTION            rio_PL_Congestion;
    /* GDA: Bug#125 */

     
} RIO_PARALLEL_MODEL_FTRAY_T;



/* callback tray to be used by the RIO model (supplied by the model environment) */
typedef struct {
    RIO_LOCAL_RESPONSE            rio_Local_Response;
    RIO_GSM_REQUEST_DONE          rio_GSM_Request_Done;
    RIO_IO_REQUEST_DONE           rio_IO_Request_Done;
    RIO_MP_REQUEST_DONE           rio_MP_Request_Done;
    RIO_DOORBELL_REQUEST_DONE     rio_Doorbell_Request_Done;
    RIO_CONFIG_REQUEST_DONE       rio_Config_Request_Done;
    RIO_CONTEXT_T                 local_Device_Context;

    RIO_SNOOP_REQUEST             rio_Snoop_Request;
    RIO_CONTEXT_T                 snoop_Context;

    RIO_MP_REMOTE_REQUEST         rio_MP_Remote_Request;
    RIO_CONTEXT_T                 mailbox_Context;
    RIO_DOORBELL_REMOTE_REQUEST   rio_Doorbell_Remote_Request;
    RIO_CONTEXT_T                 doorbell_Context;
    RIO_PORT_WRITE_REMOTE_REQUEST rio_Port_Write_Remote_Request;
    RIO_CONTEXT_T                 port_Context;

    RIO_MEMORY_REQUEST            rio_Memory_Request;
    RIO_CONTEXT_T                 memory_Context;

    RIO_READ_DIRECTORY            rio_Read_Dir;
    RIO_WRITE_DIRECTORY           rio_Write_Dir;
    RIO_CONTEXT_T                 directory_Context;

    RIO_TRANSLATE_LOCAL_IO_REQ    rio_Tr_Local_IO;
    RIO_TRANSLATE_REMOTE_IO_REQ   rio_Tr_Remote_IO;
    RIO_ROUTE_GSM_REQ             rio_Route_GSM;
    RIO_CONTEXT_T                 address_Translation_Context;

    RIO_REMOTE_CONFIG_READ        rio_Remote_Conf_Read;
    RIO_REMOTE_CONFIG_WRITE       rio_Remote_Conf_Write;
    RIO_CONTEXT_T                 extended_Features_Context;

    RIO_PH_SET_PINS               rio_PL_Set_Pins;
    RIO_CONTEXT_T                 lpep_Interface_Context;    /* context where callback to 8LP-EP will run */

    RIO_USER_MSG                  rio_User_Msg;
    RIO_CONTEXT_T                 rio_Msg;

    RIO_GRANULE_RECEIVED          rio_Granule_Received;
    RIO_REQUEST_RECEIVED          rio_Request_Received;
    RIO_RESPONSE_RECEIVED         rio_Response_Received;
    RIO_SYMBOL_TO_SEND            rio_Symbol_To_Send;
    RIO_PACKET_TO_SEND            rio_Packet_To_Send;
    RIO_CONTEXT_T                 rio_Hooks_Context;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_DS_REQUEST_DONE           rio_DS_Request_Done;  
    RIO_TRANSLATE_LOCAL_DS_REQ    rio_Tr_Local_DS;     
    RIO_TRANSLATE_REMOTE_DS_REQ   rio_Tr_Remote_DS;   
    /* GDA: Bug#124 - Support for Data Streaming packet */

    /* GDA: Bug#125 - Support for Flow control packet */
     RIO_FC_REQUEST_DONE           rio_FC_Request_Done;
     RIO_TRANSLATE_LOCAL_FC_REQ    rio_Tr_Local_FC;
     RIO_TRANSLATE_REMOTE_FC_REQ   rio_Tr_Remote_FC;
    /* GDA: Bug#125 - Support for Flow control packet */


} RIO_PARALLEL_MODEL_CALLBACK_TRAY_T;


/* the function creates instance of RIO model and returns handle to its data structure */
int RIO_Parallel_Model_Create_Instance(
    RIO_HANDLE_T            *handle,
    RIO_MODEL_INST_PARAM_T  *param  
    );


/* the function returns RIO model interface functions to be used for model access */
int RIO_Parallel_Model_Get_Function_Tray(
    RIO_PARALLEL_MODEL_FTRAY_T *ftray             /* pointer to the RIO model function tray */
    );

/* 
 * some of the callbacks supplied in the trays of the following functions will be binded
 * to the same events inside the model.
 * This is necessary because these functions can be used independently. If you use
 * both calls however you don't need to supply the same callbacks twice. If you do, 
 * (i.e. you don't assign NULL value in the futher calls), the 
 * callbacks supplied in the latest call will replace previously supplied ones.
 */

/* the function binds RIO model instance to the environment */
int RIO_Parallel_Model_Bind_Instance(
    RIO_HANDLE_T                handle,
    RIO_PARALLEL_MODEL_CALLBACK_TRAY_T   *ctray   /* environment callbacks to be registered inside RIO model instance */
    );

/* the function binds RIO model PL layer to the environment */
int RIO_Parallel_Model_Bind_PL_Instance(
    RIO_HANDLE_T                   handle,
    RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T  *ctray
    );
    
#endif /* RIO_PARALLEL_MODEL_H */

