#ifndef RIO_PARALLEL_TYPES_H
#define RIO_PARALLEL_TYPES_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/interfaces/parallel_interfaces/rio_parallel_types.h $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    RapidIO Models types description
*
* Notes:        
*
******************************************************************************/
/* types used by physical layer interface */
#define RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE    4
/*size of granule*/
#define RIO_GRAN_SIZE                       4

typedef enum {
    RIO_SINGLE_RATE = 0,
    RIO_DOUBLE_RATE,
    RIO_LPEP_8,
    RIO_LPEP_16
} RIO_PH_PARAMETERS_T;


typedef struct {
    RIO_GRANULE_TYPE_T granule_Type;
    unsigned int       ack_ID;
    unsigned int       buf_Status;
    unsigned int       stype;
} RIO_GRANULE_STRUCT_T;



/*
 * the data type represents model parameters set.
 */
typedef struct {
    RIO_PL_TRANSP_TYPE_T transp_Type;
    RIO_TR_INFO_T dev_ID;
    RIO_TR_INFO_T *remote_Dev_ID;      /* pointer to the array of deviceIDs for coherence domain participants */
    RIO_WORD_T    lcshbar;
    RIO_WORD_T    lcsbar;

    /*These fields define the initial value  of General Control CSR (offset 0x038 lower word(EF)*/
    RIO_BOOL_T      is_Host;
    RIO_BOOL_T      is_Master_Enable;
    RIO_BOOL_T      is_Discovered;
    
    RIO_WORD_T    config_Space_Size;   /* this parameter defins size of the memory window */
                                       /* mapped to the configuration space               */
    /*
     * This fields define structure of the address information 
     * for the incoming packets received by this PE and for packets
     * being issued by the PE 
     * The parameters are necessary to set PE LL CSR
     */
    RIO_BOOL_T    ext_Address;         /* extended address field is present in the packets */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */

    RIO_BOOL_T    ext_Mailbox_Support; /* PE supports extended mailbox (msglen=0, mailbox_number=msgseg||mbox) */

    /*
     * lp_Ep_Is_8       if set to TRUE make 8LP/EP interface,
     */
    RIO_BOOL_T   lp_Ep_Is_8;       
    
    /*defines the current working mode : RIO_TRUE if operating mode is 8 - bit*/
    RIO_BOOL_T     work_Mode_Is_8;

   /*define if output and input are enable after reset(enable to send and receive all kinds
     of packets (if False - only MAINTENANCE requests and responds are processed))*/
    RIO_BOOL_T     input_Is_Enable;
    RIO_BOOL_T     output_Is_Enable;

    /*
     * this parameter define limit number of sent 
     * Training patterns (actually symbols pair)
     * Should be 1 or more.
     */
    unsigned int num_Trainings;

    /*defines if the model needs training sequence*/
    RIO_BOOL_T   requires_Window_Alignment;

    /*defines the training pattern type to be used by the model*/
    unsigned int custom_Training_Pattern[RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE];
 
    /*
     * this parameter define limit number of continiously received 
     * IDLE symbols embedded in a packet if this value is oversteped
     * packet acknowledge by RETRY
     */
    unsigned int receive_Idle_Lim;

    /*
     * this parameter define limit number of IDLE, which is set in received
     * THROTTLE - if this value is overstepped packet transmittion terminated
     * STOMP sending
     */
    unsigned int throttle_Idle_Lim;

    /*
     * these parameters define reservation police for outbound buffer
     */
    RIO_UCHAR_T res_For_3_Out;
    RIO_UCHAR_T res_For_2_Out;
    RIO_UCHAR_T res_For_1_Out;

    /*
     * these parameters define reservation police for inbound buffer
     */
    RIO_UCHAR_T res_For_3_In;
    RIO_UCHAR_T res_For_2_In;
    RIO_UCHAR_T res_For_1_In;

    /*
     * order in that inbound packets are passed to the environment
     * pass_By_Prio     if TRUE the most priority packet goes first
     *                      else the oldest
     */
    RIO_BOOL_T pass_By_Prio;
    RIO_BOOL_T transmit_Flow_Control_Support; /*RIO_TRUE - flow control is detected automaticly,
                                                RIO_FALSE - only rcv flow control is supported*/
    RIO_BOOL_T enable_LRQ_Resending;          /*if true the models will resend the link request
                                                symbol if link responce is not accepted or
                                                accepted lresponce with incorrect ackid
                                                if false then model will fail to the unrec. error*/
    RIO_BOOL_T data_Corrupted;  /*GDA:Bug#139 remove the need to recompute crc */

} RIO_PARALLEL_PARAM_SET_T;

/*
 * the data type represents parameters set for the parallel switch model
 */
typedef struct {

    /* RIO_BOOL_T           *single_Data_Rate; */

    RIO_BOOL_T  *lp_Ep_8_Used;
    RIO_BOOL_T  *work_Mode_Is_8_Array;


    RIO_ROUTE_TABLE_ENTRY_T *routing_Table;
    unsigned int            rout_Table_Size; /* size of routing table */

    /*
     * this parameter define limit number of sent 
     * Training patterns (actually symbols pair)
     * Should be 1 or more.
     */
    unsigned int  *num_Trainings_Array;

    /*defines if the model needs training sequence*/
    RIO_BOOL_T    *requires_Window_Alignment_Array;

    /*
     * according to TWG work only table-based routing is officially supported by 
     * the specification, so this parameter is commented out
     */                                                                         

    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */

} RIO_SWITCH_PARALLEL_PARAM_SET_T;


/*
 * the data type represents PL model parameters set.
 */
typedef struct {

    RIO_PL_TRANSP_TYPE_T transp_Type;

    RIO_TR_INFO_T dev_ID;

    /* values of corresponding registers */
    RIO_WORD_T    lcshbar;
    RIO_WORD_T    lcsbar;
    
    /*These fields define the initial value  of General Control CSR (offset 0x038 lower word(EF)*/

    RIO_BOOL_T    is_Host;
    RIO_BOOL_T    is_Master_Enable;
    RIO_BOOL_T    is_Discovered;

    /*
     * These fields define structure of the address information 
     * for the incoming packets recived by this PE
     */
    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */
    
    RIO_BOOL_T    ext_Mailbox_Support; /* PE supports extended mailbox (msglen=0, mailbox_number=msgseg||mbox) */

    /*
     * lp_Ep_Is_8       if set to TRUE make 8LP/EP interface,
     *                      else 16LP/EP.
     */
    RIO_BOOL_T   lp_Ep_Is_8; 
    /*defines the current working mode : RIO_TRUE if operating mode is 8 - bit*/
    RIO_BOOL_T   work_Mode_Is_8;
    
    /*define if output and input are enable after reset(enable to send and receive all kinds
    of packets (if False - only MAINTENANCE requests and responds are processed))*/
    RIO_BOOL_T   input_Is_Enable;
    RIO_BOOL_T   output_Is_Enable;

    /*
     * this parameter define limit number of sent 
     * Training patterns (actually symbols pair)
     * Should be 1 or more.
     */
    unsigned int num_Trainings;

    /*defines if the model needs training sequence*/
    RIO_BOOL_T   requires_Window_Alignment;

    /*defines the training pattern type to be used by the model*/
    unsigned int custom_Training_Pattern[RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE];
    
    /*
     * this parameter define limit number of continiously received 
     * IDLE symbols embedded in a packet if this value is oversteped
     * packet acknowledge by RETRY
     */
    unsigned int receive_Idle_Lim;

    /*
     * this parameter define limit number of IDLE, which is set in received
     * THROTTLE - if this value is overstepped packet transmittion terminated
     * STOMP sending
     */
    unsigned int throttle_Idle_Lim;

    /*
     * these parameters define reservation police for outbound buffer
     */
    RIO_UCHAR_T res_For_3_Out;
    RIO_UCHAR_T res_For_2_Out;
    RIO_UCHAR_T res_For_1_Out;

    /*
     * these parameters define reservation police for inbound buffer
     */
    RIO_UCHAR_T res_For_3_In;
    RIO_UCHAR_T res_For_2_In;
    RIO_UCHAR_T res_For_1_In;

    /*
     * order in that inbound packets are passed to the environment
     * pass_By_Prio     if TRUE the most priority packet goes first
     *                      else the oldest
     */
    RIO_BOOL_T pass_By_Prio;

    /*new parameters*/

    RIO_BOOL_T transmit_Flow_Control_Support; /*RIO_TRUE - flow control is detected automaticly,
                                                RIO_FALSE - only rcv flow control is supported*/

    RIO_BOOL_T enable_Retry_Return_Code;      /*if true pl will return RIO_RETRY code*/
    RIO_BOOL_T enable_LRQ_Resending;          /*if true the models will resend the link request
                                                symbol if link responce is not accepted or
                                                accepted lresponce with incorrect ackid
                                                if false then model will fail to the unrec. error*/
    RIO_BOOL_T data_Corrupted;  /*GDA:Bug#139 remove the need to recompute crc */
} RIO_PL_PARALLEL_PARAM_SET_T;



/*
 * the data type represents PLLIGHT model parameters set.
 */
typedef struct {

    RIO_PL_TRANSP_TYPE_T transp_Type;

    RIO_TR_INFO_T dev_ID;

    /*
     * These fields define structure of the address information 
     * for the incoming packets recived by this PE
     */
    RIO_BOOL_T    ext_Address;         /* extended address field */
    RIO_BOOL_T    ext_Address_16;      /* size of extended address - 16 or 32 bits */
    
    /*
     * lp_Ep_Is_8       if set to TRUE make 8LP/EP interface,
     *                      else 16LP/EP.
     */
    RIO_BOOL_T   lp_Ep_Is_8; 
    /*defines the current working mode : RIO_TRUE if operating mode is 8 - bit*/
    RIO_BOOL_T   work_Mode_Is_8;
    
    /*
     * this parameter define limit number of sent 
     * Training patterns (actually symbols pair)
     * Should be 1 or more.
     */
    unsigned int num_Trainings;

    /*defines if the model needs training sequence*/
    RIO_BOOL_T   requires_Window_Alignment;

    /*defines the training pattern type to be used by the model*/
    unsigned int custom_Training_Pattern[RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE];
    
    /*
     * this parameter define limit number of continiously received 
     * IDLE symbols embedded in a packet if this value is oversteped
     * packet acknowledge by RETRY
     */
    unsigned int receive_Idle_Lim;

    /*
     * this parameter define limit number of IDLE, which is set in received
     * THROTTLE - if this value is overstepped packet transmittion terminated
     * STOMP sending
     */
    unsigned int throttle_Idle_Lim;


    /*new parameters*/

    RIO_BOOL_T transmit_Flow_Control_Support; /*RIO_TRUE - flow control is detected automaticly,
                                                RIO_FALSE - only rcv flow control is supported*/

    RIO_BOOL_T enable_LRQ_Resending;          /*if true the models will resend the link request
                                                symbol if link responce is not accepted or
                                                accepted lresponce with incorrect ackid
                                                if false then model will fail to the unrec. error*/
} RIO_PLLIGHT_PARALLEL_PARAM_SET_T;





/*this enum defines symbol's violation types detected in txrxm*/
typedef enum {
    WRONG_S_PARITY = 0,
    WRONG_INVERSION,
    RESERVED_FIELDS_ENCODINGS,
    NON_ZERO_RESERVED_SYMBOL_BITS_VALUE,
    CORRUPTED_TRAINING,
    NO_FRAME_TOGGLE
} RIO_SYMBOL_VIOLATION_T;


/*defines the struct of passing and receiving control symbols*/
typedef struct {
    RIO_UCHAR_T s;
    RIO_UCHAR_T ackID;
    RIO_UCHAR_T rsrv_1;
    RIO_UCHAR_T s_Parity;
    RIO_UCHAR_T rsrv_2;
    RIO_UCHAR_T buf_Status;
    RIO_UCHAR_T stype;
}RIO_SYMBOL_STRUCT_T;

typedef struct {
    RIO_UCHAR_T             granule_Data[RIO_GRAN_SIZE];
    RIO_SYMBOL_STRUCT_T     granule_Struct;
    RIO_GRANULE_TYPE_T      granule_Type;
    RIO_BOOL_T              granule_Frame;
} RIO_TXRX_GRANULE_T;


typedef struct {

    unsigned long default_Granule; /*if there is no requests to send*/
    RIO_BOOL_T    default_Frame_Toggle; /*frame toggle for default granule*/
    /*defines the current port width : RIO_TRUE if port width is 8 - bit*/
    RIO_BOOL_T   lp_Ep_Is_8; 
    /*defines the current working mode : RIO_TRUE if operating mode is 8 - bit*/
    RIO_BOOL_T     work_Mode_Is_8;
    /*defines if extended address exists in inbound packets*/
    RIO_BOOL_T      in_Ext_Address_Valid; 
    RIO_BOOL_T      in_Ext_Address_16;
    /*Initialization block parameters*/
    unsigned int num_Trainings;
    RIO_BOOL_T requires_Window_Alignment;
    
    RIO_BOOL_T      is_Init_Proc_On; 

}RIO_TXRXM_PARALLEL_PARAM_SET_T;


typedef struct {
    /*outbound buffers sizes*/
    unsigned int packets_Buffer_Size;
    unsigned int symbols_Buffer_Size;
    unsigned int granules_Buffer_Size;
    unsigned int event_Buffer_Size; /*to switch on/off machine and etc*/
} RIO_TXRXM_PARALLEL_INST_PARAM_T;


/*types for perform txrx events execution*/
typedef enum
{
    RIO_TXRXM_TURN_TRAINING_MODULE_ON = 0,
    RIO_TXRXM_TURN_TRAINING_MODULE_OFF
}RIO_TXRX_EVENT_T;

#endif /* RIO_PARALLEL_TYPES_H */


