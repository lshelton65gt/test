#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio_pl/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds PL model shared library
#

include ../../../verilog_env/demo/debug.inc

CC              =   gcc

TARGET          =   librio_pl.so
SERIAL_TARGET	=   librio_pl_serial.so
PARALLEL_TARGET	=   librio_pl_parallel.so

FLEXLM_PATH      =   ../../../../flexlm

DEBUG		=   -DNDEBUG

FLEXLM_DEFINES  = 

DEFINES         = $(FLEXLM_DEFINES) $(_DEBUG)

LIBS            =   -lc
LIBS_FLEXLM     =   $(FLEXLM_PATH)/sun4_u5/lm_new.o -L $(FLEXLM_PATH)/sun4_u5 -l lmgr -l crvs -l sb -lsocket


COMMON_INCLUDES		=   -I../../interfaces/common  
SERIAL_INCLUDES		=   -I../../interfaces/serial_interfaces -I$(FLEXLM_PATH)/machind 
PARALLEL_INCLUDES	=   -I../../interfaces/parallel_interfaces

PL_MODEL_INCLUDES		=   -I./include
PL_INCLUDES			=   -I../../pl/include
PCS_PMA_INCLUDES		=   -I../../pcs_pma/include
PCS_PMA_ADAPTER_INCLUDES	=   -I../../pcs_pma_adapter/include
TXRX_INCLUDES			=   -I../../txrx/include
PARALLEL_INIT_INCLUDES		=   -I../../parallel_init/include

CFLAGS          =   -c $(prof) $(DEFINES) -elf -ansi -pedantic -Wall -O3
LFLAGS          =   $(prof) -G -s -B symbolic
LFLAGS_AIX	=   $(prof) -G -bE:rio_pl.exp
LFLAGS_LINUX    =   $(prof) -G -s -Bsymbolic

COMMON_OBJS	=   rio_pl_model_common.o
SERIAL_OBJS	=   rio_pl_serial_model.o
PARALLEL_OBJS	=   rio_pl_parallel_model.o

COMMON_PL	=   ../../pl/pl.o
SERIAL_PL	=   ../../pl/pl_serial.o
PARALLEL_PL	=   ../../pl/pl_parallel.o



###############################################################################
# make default target
all :   $(TARGET)

serial :	$(SERIAL_TARGET) 	

parallel :	$(PARALLEL_TARGET) 	

# make target including both serial and parallel models
$(TARGET) :	$(COMMON_PL) $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS)
		ld  $(LFLAGS) $(COMMON_PL) $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)

$(COMMON_PL):
		cd ../../pl; make -f makefile.mak  pl.o

# make target including serial models only
$(SERIAL_TARGET) :	$(SERIAL_PL) $(COMMON_OBJS) $(SERIAL_OBJS)
		ld  $(LFLAGS) $(SERIAL_PL) $(COMMON_OBJS) $(SERIAL_OBJS) $(LIBS) -o $(TARGET)

$(SERIAL_PL):
		cd ../../pl; make -f makefile.mak  pl_serial.o

# make target including parallel models only
$(PARALLEL_TARGET) :	$(PARALLEL_PL) $(COMMON_OBJS) $(PARALLEL_OBJS)
		ld  $(LFLAGS) $(PARALLEL_PL) $(COMMON_OBJS) $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)

$(PARALLEL_PL):
		cd ../../pl; make -f makefile.mak  pl_parallel.o

#clean data
clean :		
		rm -f *.o
		rm -f $(TARGET)
		rm -f $(SERIAL_TARGET)
		rm -f $(PARALLEL_TARGET)
		cd ../../pl; make -f makefile.mak clean
		
all_flexlm: $(COMMON_PL) $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS)
		ld  $(LFLAGS) $(COMMON_PL) $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS) $(LIBS_FLEXLM) $(LIBS) -o $(TARGET)
		#$(FLEXLM_PATH)/sun4_u5/lmstrip -r $(TARGET)

aix_all: $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS)
		cd ../../pl; make -f makefile.mak  pl.o
		ld  $(LFLAGS_AIX) $(COMMON_PL) $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)

aix_par: $(COMMON_OBJS) $(PARALLEL_OBJS)
		cd ../../pl; make -f makefile.mak  pl_parallel.o
		ld  $(LFLAGS_AIX) $(PARALLEL_PL) $(COMMON_OBJS) $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)


linux_all: $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS)
		cd ../../pl; make -f makefile.mak  pl.o
		ld  $(LFLAGS_LINUX) $(COMMON_PL) $(COMMON_OBJS) $(SERIAL_OBJS) $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)

linux_par: $(COMMON_OBJS) $(PARALLEL_OBJS)
		cd ../../pl; make -f makefile.mak  pl_parallel.o
		ld  $(LFLAGS_LINUX) $(PARALLEL_PL) $(COMMON_OBJS) $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)

%.o :		%.c
		$(CC) $(CFLAGS) $(COMMON_INCLUDES) $(SERIAL_INCLUDES) $(PARALLEL_INCLUDES) $(PL_MODEL_INCLUDES) $(PL_INCLUDES) $(PCS_PMA_INCLUDES) $(PCS_PMA_ADAPTER_INCLUDES) $(TXRX_INCLUDES) $(PARALLEL_INIT_INCLUDES) -o $*.o $<

###############################################################################
