#******************************************************************************
#*
#* Copyright Motorola, Inc. 2002-2004
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio_pl/makefile_nt.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# Microsoft Developer Studio Generated NMAKE File, Based on rio_pl.dsp
!IF "$(CFG)" == ""
CFG=rio_pl - Win32 Debug
!MESSAGE No configuration specified. Defaulting to rio_pl - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "rio_pl - Win32 Release" && "$(CFG)" != "rio_pl - Win32 Debug" && "$(CFG)" != "rio_ppl - Win32 Release" && "$(CFG)" != "rio_ppl - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "rio_pl.mak" CFG="rio_pl - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "rio_pl - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "rio_pl - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "rio_pl - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\rio_pl.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\rio_c2char.obj"
	-@erase "$(INTDIR)\rio_char2c.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_adapter.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_cl.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_sb.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_align.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_sync.obj"
	-@erase "$(INTDIR)\rio_pl_errors.obj"
	-@erase "$(INTDIR)\rio_pl_in_buf.obj"
	-@erase "$(INTDIR)\rio_pl_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_out_buf.obj"
	-@erase "$(INTDIR)\rio_pl_parallel_model.obj"
	-@erase "$(INTDIR)\rio_pl_registers.obj"
	-@erase "$(INTDIR)\rio_pl_rx.obj"
	-@erase "$(INTDIR)\rio_pl_serial_model.obj"
	-@erase "$(INTDIR)\rio_pl_tx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\rio_pl.dll"
	-@erase "$(OUTDIR)\rio_pl.exp"
	-@erase "$(OUTDIR)\rio_pl.lib"
	-@erase "$(INTDIR)\rio_pl_pllight_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_pllight_parallel_model.obj"
	-@erase "$(INTDIR)\rio_pl_pllight_serial_model.obj"


"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O2 /I "$(RAPIDIO_PATH)\interfaces\common" /I "$(RAPIDIO_PATH)\interfaces\parallel_interfaces" /I "$(RAPIDIO_PATH)\interfaces\serial_interfaces" /I "$(RAPIDIO_PATH)\parallel_init\include" /I "$(RAPIDIO_PATH)\pcs_pma\include" /I "$(RAPIDIO_PATH)\pcs_pma_adapter\include" /I "$(RAPIDIO_PATH)\pl\include" /I "$(RAPIDIO_PATH)\tl\include" /I "$(RAPIDIO_PATH)\txrx\include" /I "$(RAPIDIO_PATH)\models\rio_pl\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_PL_EXPORTS" /Fp"$(INTDIR)\rio_pl.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_pl.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\rio_pl.pdb" /machine:I386 /def:".\rio_pl.def" /out:"$(OUTDIR)\rio_pl.dll" /implib:"$(OUTDIR)\rio_pl.lib" 
DEF_FILE= \
	".\rio_pl.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_pl_model_common.obj" \
	"$(INTDIR)\rio_pl_parallel_model.obj" \
	"$(INTDIR)\rio_pl_serial_model.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_c2char.obj" \
	"$(INTDIR)\rio_char2c.obj" \
	"$(INTDIR)\rio_pcs_pma.obj" \
	"$(INTDIR)\rio_pcs_pma_dp.obj" \
	"$(INTDIR)\rio_pcs_pma_dp_cl.obj" \
	"$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj" \
	"$(INTDIR)\rio_pcs_pma_dp_sb.obj" \
	"$(INTDIR)\rio_pcs_pma_pm.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_align.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_init.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_sync.obj" \
	"$(INTDIR)\rio_pcs_pma_adapter.obj" \
	"$(INTDIR)\rio_pl_errors.obj" \
	"$(INTDIR)\rio_pl_in_buf.obj" \
	"$(INTDIR)\rio_pl_out_buf.obj" \
	"$(INTDIR)\rio_pl_registers.obj" \
	"$(INTDIR)\rio_pl_rx.obj" \
	"$(INTDIR)\rio_pl_tx.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" \
	"$(INTDIR)\rio_pl_pllight_model_common.obj" \
	"$(INTDIR)\rio_pl_pllight_parallel_model.obj" \
	"$(INTDIR)\rio_pl_pllight_serial_model.obj"


"$(OUTDIR)\rio_pl.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "rio_pl - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\rio_pl.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\rio_c2char.obj"
	-@erase "$(INTDIR)\rio_char2c.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_adapter.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_cl.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_sb.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_align.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_sync.obj"
	-@erase "$(INTDIR)\rio_pl_errors.obj"
	-@erase "$(INTDIR)\rio_pl_in_buf.obj"
	-@erase "$(INTDIR)\rio_pl_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_out_buf.obj"
	-@erase "$(INTDIR)\rio_pl_parallel_model.obj"
	-@erase "$(INTDIR)\rio_pl_registers.obj"
	-@erase "$(INTDIR)\rio_pl_rx.obj"
	-@erase "$(INTDIR)\rio_pl_serial_model.obj"
	-@erase "$(INTDIR)\rio_pl_tx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\rio_pl.dll"
	-@erase "$(OUTDIR)\rio_pl.exp"
	-@erase "$(OUTDIR)\rio_pl.ilk"
	-@erase "$(OUTDIR)\rio_pl.lib"
	-@erase "$(OUTDIR)\rio_pl.pdb"
	-@erase "$(INTDIR)\rio_pl_pllight_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_pllight_parallel_model.obj"
	-@erase "$(INTDIR)\rio_pl_pllight_serial_model.obj"


"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I "$(RAPIDIO_PATH)\interfaces\common" /I "$(RAPIDIO_PATH)\interfaces\parallel_interfaces" /I "$(RAPIDIO_PATH)\interfaces\serial_interfaces" /I "$(RAPIDIO_PATH)\parallel_init\include" /I "$(RAPIDIO_PATH)\pcs_pma\include" /I "$(RAPIDIO_PATH)\pcs_pma_adapter\include" /I "$(RAPIDIO_PATH)\pl\include" /I "$(RAPIDIO_PATH)\tl\include" /I "$(RAPIDIO_PATH)\txrx\include" /I "$(RAPIDIO_PATH)\models\rio_pl\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_PL_EXPORTS" /Fp"$(INTDIR)\rio_pl.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_pl.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\rio_pl.pdb" /debug /machine:I386 /def:".\rio_pl.def" /out:"$(OUTDIR)\rio_pl.dll" /implib:"$(OUTDIR)\rio_pl.lib" /pdbtype:sept 
DEF_FILE= \
	".\rio_pl.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_pl_model_common.obj" \
	"$(INTDIR)\rio_pl_parallel_model.obj" \
	"$(INTDIR)\rio_pl_serial_model.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_c2char.obj" \
	"$(INTDIR)\rio_char2c.obj" \
	"$(INTDIR)\rio_pcs_pma.obj" \
	"$(INTDIR)\rio_pcs_pma_dp.obj" \
	"$(INTDIR)\rio_pcs_pma_dp_cl.obj" \
	"$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj" \
	"$(INTDIR)\rio_pcs_pma_dp_sb.obj" \
	"$(INTDIR)\rio_pcs_pma_pm.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_align.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_init.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_sync.obj" \
	"$(INTDIR)\rio_pcs_pma_adapter.obj" \
	"$(INTDIR)\rio_pl_errors.obj" \
	"$(INTDIR)\rio_pl_in_buf.obj" \
	"$(INTDIR)\rio_pl_out_buf.obj" \
	"$(INTDIR)\rio_pl_registers.obj" \
	"$(INTDIR)\rio_pl_rx.obj" \
	"$(INTDIR)\rio_pl_tx.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" \
	"$(INTDIR)\rio_pl_pllight_model_common.obj" \
	"$(INTDIR)\rio_pl_pllight_parallel_model.obj" \
	"$(INTDIR)\rio_pl_pllight_serial_model.obj"

"$(OUTDIR)\rio_pl.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "rio_ppl - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\rio_pl.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\rio_c2char.obj"
	-@erase "$(INTDIR)\rio_char2c.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_adapter.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_cl.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_sb.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_align.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_sync.obj"
	-@erase "$(INTDIR)\rio_pl_errors.obj"
	-@erase "$(INTDIR)\rio_pl_in_buf.obj"
	-@erase "$(INTDIR)\rio_pl_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_out_buf.obj"
	-@erase "$(INTDIR)\rio_pl_parallel_model.obj"
	-@erase "$(INTDIR)\rio_pl_registers.obj"
	-@erase "$(INTDIR)\rio_pl_rx.obj"
	-@erase "$(INTDIR)\rio_pl_serial_model.obj"
	-@erase "$(INTDIR)\rio_pl_tx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\rio_pl.dll"
	-@erase "$(OUTDIR)\rio_pl.exp"
	-@erase "$(OUTDIR)\rio_pl.lib"
	-@erase "$(INTDIR)\rio_pl_pllight_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_pllight_parallel_model.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O2 /I "$(RAPIDIO_PATH)\interfaces\common" /I "$(RAPIDIO_PATH)\interfaces\parallel_interfaces" /I "$(RAPIDIO_PATH)\interfaces\serial_interfaces" /I "$(RAPIDIO_PATH)\parallel_init\include" /I "$(RAPIDIO_PATH)\pcs_pma\include" /I "$(RAPIDIO_PATH)\pcs_pma_adapter\include" /I "$(RAPIDIO_PATH)\pl\include" /I "$(RAPIDIO_PATH)\tl\include" /I "$(RAPIDIO_PATH)\txrx\include" /I "$(RAPIDIO_PATH)\models\rio_pl\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_PL_EXPORTS" /Fp"$(INTDIR)\rio_pl.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_pl.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\rio_pl.pdb" /machine:I386 /def:".\rio_ppl.def" /out:"$(OUTDIR)\rio_pl.dll" /implib:"$(OUTDIR)\rio_pl.lib" 
DEF_FILE= \
	".\rio_ppl.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_pl_model_common.obj" \
	"$(INTDIR)\rio_pl_parallel_model.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_pl_errors.obj" \
	"$(INTDIR)\rio_pl_in_buf.obj" \
	"$(INTDIR)\rio_pl_out_buf.obj" \
	"$(INTDIR)\rio_pl_registers.obj" \
	"$(INTDIR)\rio_pl_rx.obj" \
	"$(INTDIR)\rio_pl_tx.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" \
	"$(INTDIR)\rio_pl_pllight_model_common.obj" \
	"$(INTDIR)\rio_pl_pllight_parallel_model.obj"

"$(OUTDIR)\rio_pl.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "rio_ppl - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\rio_pl.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\rio_c2char.obj"
	-@erase "$(INTDIR)\rio_char2c.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_adapter.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_cl.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_dp_sb.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_align.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_sync.obj"
	-@erase "$(INTDIR)\rio_pl_errors.obj"
	-@erase "$(INTDIR)\rio_pl_in_buf.obj"
	-@erase "$(INTDIR)\rio_pl_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_out_buf.obj"
	-@erase "$(INTDIR)\rio_pl_parallel_model.obj"
	-@erase "$(INTDIR)\rio_pl_registers.obj"
	-@erase "$(INTDIR)\rio_pl_rx.obj"
	-@erase "$(INTDIR)\rio_pl_serial_model.obj"
	-@erase "$(INTDIR)\rio_pl_tx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\rio_pl.dll"
	-@erase "$(OUTDIR)\rio_pl.exp"
	-@erase "$(OUTDIR)\rio_pl.ilk"
	-@erase "$(OUTDIR)\rio_pl.lib"
	-@erase "$(OUTDIR)\rio_pl.pdb"
	-@erase "$(INTDIR)\rio_pl_pllight_model_common.obj"
	-@erase "$(INTDIR)\rio_pl_pllight_parallel_model.obj"
	-@erase "$(INTDIR)\rio_pl_pllight_serial_model.obj"


"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /I "$(RAPIDIO_PATH)\interfaces\common" /I "$(RAPIDIO_PATH)\interfaces\parallel_interfaces" /I "$(RAPIDIO_PATH)\interfaces\serial_interfaces" /I "$(RAPIDIO_PATH)\parallel_init\include" /I "$(RAPIDIO_PATH)\pcs_pma\include" /I "$(RAPIDIO_PATH)\pcs_pma_adapter\include" /I "$(RAPIDIO_PATH)\pl\include" /I "$(RAPIDIO_PATH)\tl\include" /I "$(RAPIDIO_PATH)\txrx\include" /I "$(RAPIDIO_PATH)\models\rio_pl\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_PL_EXPORTS" /Fp"$(INTDIR)\rio_pl.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_pl.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\rio_pl.pdb" /debug /machine:I386 /def:".\rio_ppl.def" /out:"$(OUTDIR)\rio_pl.dll" /implib:"$(OUTDIR)\rio_pl.lib" /pdbtype:sept 
DEF_FILE= \
	".\rio_ppl.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_pl_model_common.obj" \
	"$(INTDIR)\rio_pl_parallel_model.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_pl_errors.obj" \
	"$(INTDIR)\rio_pl_in_buf.obj" \
	"$(INTDIR)\rio_pl_out_buf.obj" \
	"$(INTDIR)\rio_pl_registers.obj" \
	"$(INTDIR)\rio_pl_rx.obj" \
	"$(INTDIR)\rio_pl_tx.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" \
	"$(INTDIR)\rio_pl_pllight_model_common.obj" \
	"$(INTDIR)\rio_pl_pllight_parallel_model.obj"

"$(OUTDIR)\rio_pl.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("rio_pl.dep")
!INCLUDE "rio_pl.dep"
!ELSE 
!MESSAGE Warning: cannot find "rio_pl.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "rio_pl - Win32 Release" || "$(CFG)" == "rio_pl - Win32 Debug" || "$(CFG)" == "rio_ppl - Win32 Release" || "$(CFG)" == "rio_ppl - Win32 Debug"
SOURCE=$(RAPIDIO_PATH)\txrx\rio_32_txrx_model.c

"$(INTDIR)\rio_32_txrx_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_c2char.c

"$(INTDIR)\rio_c2char.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_char2c.c

"$(INTDIR)\rio_char2c.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\parallel_init\rio_parallel_init.c

"$(INTDIR)\rio_parallel_init.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma.c

"$(INTDIR)\rio_pcs_pma.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma_adapter\rio_pcs_pma_adapter.c

"$(INTDIR)\rio_pcs_pma_adapter.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_dp.c

"$(INTDIR)\rio_pcs_pma_dp.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_dp_cl.c

"$(INTDIR)\rio_pcs_pma_dp_cl.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_dp_pins_driver.c

"$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_dp_sb.c

"$(INTDIR)\rio_pcs_pma_dp_sb.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_pm.c

"$(INTDIR)\rio_pcs_pma_pm.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_pm_align.c

"$(INTDIR)\rio_pcs_pma_pm_align.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_pm_init.c

"$(INTDIR)\rio_pcs_pma_pm_init.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pcs_pma\rio_pcs_pma_pm_sync.c

"$(INTDIR)\rio_pcs_pma_pm_sync.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_errors.c

"$(INTDIR)\rio_pl_errors.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_in_buf.c

"$(INTDIR)\rio_pl_in_buf.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\models\rio_pl\rio_pl_model_common.c

"$(INTDIR)\rio_pl_model_common.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_out_buf.c

"$(INTDIR)\rio_pl_out_buf.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\models\rio_pl\rio_pl_parallel_model.c

"$(INTDIR)\rio_pl_parallel_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_registers.c

"$(INTDIR)\rio_pl_registers.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_rx.c

"$(INTDIR)\rio_pl_rx.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\models\rio_pl\rio_pl_serial_model.c

"$(INTDIR)\rio_pl_serial_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_tx.c

"$(INTDIR)\rio_pl_tx.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_pllight_model_common.c

"$(INTDIR)\rio_pl_pllight_model_common.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_pllight_parallel_model.c

"$(INTDIR)\rio_pl_pllight_parallel_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=$(RAPIDIO_PATH)\pl\rio_pl_pllight_serial_model.c

"$(INTDIR)\rio_pl_pllight_serial_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)




!ENDIF 

