#ifndef RIO_PL_MODEL_COMMON_H
#define RIO_PL_MODEL_COMMON_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio_pl/include/rio_pl_model_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains declaration of the common RapidIO PL model
*               function interface
*
* Notes:        
*
******************************************************************************/

#include "rio_types.h"
#include "rio_gran_type.h"
#include "rio_pl_pllight_model_common.h"

int RIO_PL_Ack_Remote_Req(
    RIO_HANDLE_T handle, 
    RIO_TAG_T    tag
    );

int RIO_PL_Send_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
    );

int RIO_PL_Configuration_Request(
    RIO_HANDLE_T      handle,
    RIO_CONFIG_REQ_T  *rq,
    RIO_TAG_T         trx_Tag
    );

int RIO_PL_Doorbell_Request(
    RIO_HANDLE_T        handle,
    RIO_DOORBELL_REQ_T  *rq,
    RIO_TAG_T           trx_Tag 
    );

int RIO_PL_Message_Request(
    RIO_HANDLE_T       handle,
    RIO_MESSAGE_REQ_T  *rq,
    RIO_TAG_T          trx_Tag             
    );

int RIO_PL_IO_Request(
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
    );

int RIO_PL_GSM_Request(  
    RIO_HANDLE_T       handle,
    RIO_PL_GSM_TTYPE_T ttype,          
    RIO_GSM_REQ_T      *rq,
    RIO_UCHAR_T        sec_ID,         
    RIO_UCHAR_T        sec_TID,
    RIO_TAG_T          trx_Tag,        
    RIO_TR_INFO_T      transport_Info[],
    RIO_UCHAR_T        multicast_Size
    );

int RIO_PL_Packet_Struct_Request(
    RIO_HANDLE_T       handle, 
    RIO_MODEL_PACKET_STRUCT_RQ_T *rq, 
    RIO_TAG_T          trx_Tag
    );

/* GDA: Bug#124 - Support for Data Streaming packet */
int RIO_PL_DS_Request(
    RIO_HANDLE_T   handle,
    RIO_DS_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
    );
/* GDA: Bug#124 - Support for Data Streaming packet */

/* GDA: Bug#125 - Support for Flow control packet */
int RIO_PL_FC_Request(
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
    );

int RIO_PL_Congestion(
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag
    );
/* GDA: Bug#125 - Support for Flow control packet */

#endif /* RIO_PL_MODEL_COMMON_H */


