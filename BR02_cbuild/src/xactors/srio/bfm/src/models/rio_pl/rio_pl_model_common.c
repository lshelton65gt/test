/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio_pl/rio_pl_model_common.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                       to display revision history information.
*
* Description:  Physical layer source
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

#include "rio_pl_model_common.h"
#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_rx.h"
#include "rio_pl_tx.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"


/***************************************************************************************
  Static functions prototypes 
***************************************************************************************/ 

static void detect_IO_Type(unsigned int *ftype, unsigned int *ttype, RIO_IO_TTYPE_T type);
static void detect_GSM_Type(unsigned int *ftype, unsigned int *ttype, RIO_PL_GSM_TTYPE_T type);


/***************************************************************************
 * Function : RIO_PL_GSM_Request
 *
 * Description: notify about new GSM request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_GSM_Request(  
    RIO_HANDLE_T       handle,
    RIO_PL_GSM_TTYPE_T ttype,          
    RIO_GSM_REQ_T      *rq,
    RIO_UCHAR_T        sec_ID,         
    RIO_UCHAR_T        sec_TID,
    RIO_TAG_T          trx_Tag,        
    RIO_TR_INFO_T      transport_Info[],
    RIO_UCHAR_T        multicast_Size
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle; /* instanse handle */
    unsigned int pl_Ttype = 0, pl_Ftype = 0;      /* request ftype and ttype */
    unsigned int size, wdptr, max_Size;           /* parameters for size detection*/
    int result;                                   /*result of routine execution*/

    /* check pointers and state */
    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* detect proper packet and transaction type */
    detect_GSM_Type(&pl_Ftype, &pl_Ttype, ttype);

    /* check priority value */
    if (rq->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)rq->prio);
        return RIO_ERROR;
    }

    /* check size */
    if (rq->dw_Size > RIO_PL_MAX_PAYLOAD_SIZE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, 
            (unsigned long)pl_Ftype, (unsigned long)pl_Ttype);
        return RIO_ERROR;
    }

    /* address only transactions size shall be zero */
    if (!rq->dw_Size)
    {
        if (ttype != RIO_PL_GSM_DKILL_HOME &&
            ttype != RIO_PL_GSM_DKILL_SHARER &&
            ttype != RIO_PL_GSM_IKILL_HOME &&
            ttype != RIO_PL_GSM_IKILL_SHARER &&
            ttype != RIO_PL_GSM_TLBIE &&
            ttype != RIO_PL_GSM_TLBSYNC &&
            ttype != RIO_PL_GSM_FLUSH_WO_DATA)
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, 
                (unsigned long)pl_Ftype, (unsigned long)pl_Ttype);
            return RIO_ERROR;
        }
    }

    /* transaction only request some fields shall be zero*/
    if (ttype == RIO_PL_GSM_TLBSYNC)
    {
        rq->address = 0;
        rq->extended_Address = 0;
        rq->xamsbs = 0;
    }

    /* check subdouble-word payload - its allowed only for FLUSHes */
    if (rq->dw_Size == 1 && rq->byte_Num != RIO_PL_BYTE_CNT_IN_DWORD)
    {
        if (ttype != RIO_PL_GSM_FLUSH_WO_DATA &&
            ttype != RIO_PL_GSM_FLUSH_WITH_DATA &&
            ttype != RIO_PL_GSM_IO_READ_HOME &&
            ttype != RIO_PL_GSM_IO_READ_OWNER)
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, 
                (unsigned long)pl_Ftype, (unsigned long)pl_Ttype);
            return RIO_ERROR;
        }
    }

    /* detect max granule size for GSM operation */

    if (RIO_PL_Is_Payload(pl_Ftype, pl_Ttype) == RIO_OK)
        max_Size = RIO_PL_Get_Max_Gr_Size(handle);
    else
        max_Size = RIO_PL_Get_Max_Dem_Gr_Size(handle);

    max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(max_Size);

    /* GSM does not support any split */
    if (rq->dw_Size > max_Size)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, (unsigned long)pl_Ftype, (unsigned long)pl_Ttype);
        return RIO_ERROR;
    }

    /* detect size and wdprt */
    if (ttype == RIO_PL_GSM_DKILL_HOME ||
        ttype == RIO_PL_GSM_DKILL_SHARER ||
        ttype == RIO_PL_GSM_IKILL_HOME ||
        ttype == RIO_PL_GSM_IKILL_SHARER ||
        ttype == RIO_PL_GSM_TLBIE ||
        ttype == RIO_PL_GSM_TLBSYNC ||
        ttype == RIO_PL_GSM_FLUSH_WO_DATA)
    {
        /* address and transaction only - clear size */
        size = 0;
        wdptr = 0;
    }
    else
    {
        /* detect */
        if (RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, rq->dw_Size, rq->byte_Num, rq->offset) != RIO_OK)
            return RIO_ERROR; 
    }

    /* detect if multicast engine is necessary */
    if (multicast_Size > 1 && (pl_Ftype != RIO_NONINTERV_REQUEST ||
        (pl_Ftype == RIO_NONINTERV_REQUEST &&
        pl_Ttype != RIO_NONINTERV_DKILL_SHARER &&
        pl_Ttype != RIO_NONINTERV_IKILL_SHARER &&
        pl_Ttype != RIO_NONINTERV_TLBSYNC &&
        pl_Ttype != RIO_NONINTERV_TLBIE)))
    {
        /* print diagnostic */
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_45, (unsigned long)pl_Ftype, (unsigned long)pl_Ttype);
        return RIO_ERROR;
    }

    /* detect count of multicast recipients */
    if (multicast_Size > RIO_PL_OUT_MULTICAST_ENGINE_LENGHT || !multicast_Size)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_46, (unsigned long)multicast_Size);
        return RIO_ERROR;
    }

    /* load request to temporary buffer and try to load it to the outbound */
    if (multicast_Size > 1 || (pl_Ftype == RIO_NONINTERV_REQUEST &&
        (pl_Ttype == RIO_NONINTERV_DKILL_SHARER ||
        pl_Ttype == RIO_NONINTERV_IKILL_SHARER ||
        pl_Ttype == RIO_NONINTERV_TLBSYNC ||
        pl_Ttype == RIO_NONINTERV_TLBIE)))
    {
        unsigned int i; /* loop counter */

        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_WAIT_MULTICAST;
        RIO_PL_GET_TEMP(instanse).multicast_Size = multicast_Size;



        /* save recipients transport informations */
        for (i = 0; i < multicast_Size; i++)
            RIO_PL_GET_TEMP(instanse).tr_Infos[i] = transport_Info[i];
        
    }
    else
    {
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;
        RIO_PL_GET_TEMP(instanse).entry.tranp_Info = transport_Info[0];
        RIO_PL_GET_TEMP(instanse).multicast_Size = 1;
    }
    
         
    /* fill the temporary */
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;

    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;

    /* save payload if any and pointer isn't NULL */
    if (RIO_PL_Is_Payload(pl_Ftype, pl_Ttype) == RIO_OK)
    {
        RIO_PL_GET_TEMP(instanse).payload_Source = rq->data;
        RIO_PL_GET_TEMP(instanse).payload_Size = rq->dw_Size;

        /* try to load the payload at once */
        if (rq->data)
        {
            unsigned int i; /* loop counter */
            
            for (i = 0; i < rq->dw_Size; i++)
                RIO_PL_GET_TEMP(instanse).payload[i] = rq->data[i];
        }
    }
    else
    {
        RIO_PL_GET_TEMP(instanse).payload_Source = NULL;
        RIO_PL_GET_TEMP(instanse).payload_Size = 0;
    }

    /* form packet fields */
    RIO_PL_GET_TEMP(instanse).entry.prio = rq->prio;
    RIO_PL_GET_TEMP(instanse).entry.ftype = pl_Ftype;
    RIO_PL_GET_TEMP(instanse).entry.ttype = pl_Ttype;
    RIO_PL_GET_TEMP(instanse).entry.tt = rq->transp_Type;

    /* this constant is used to set address on double-word boundary */
    RIO_PL_GET_TEMP(instanse).entry.address = (rq->address >> 3);
    RIO_PL_GET_TEMP(instanse).entry.extended_Address = rq->extended_Address;
    RIO_PL_GET_TEMP(instanse).entry.xamsbs = rq->xamsbs/*(instanse->parameters_Set.xamsbs == RIO_TRUE) ? rq->xamsbs : 0*/;
    RIO_PL_GET_TEMP(instanse).entry.ext_Address_Valid = instanse->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid;
    RIO_PL_GET_TEMP(instanse).entry.ext_Address_16 = instanse->outbound_Buf.out_Buf_Param_Set.ext_Address_16;
    RIO_PL_GET_TEMP(instanse).entry.wdptr = wdptr;
    RIO_PL_GET_TEMP(instanse).entry.size = size;
    RIO_PL_GET_TEMP(instanse).entry.sec_ID = sec_ID;
    RIO_PL_GET_TEMP(instanse).entry.sec_TID = sec_TID;

    /* try to load packet at once */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_IO_Request
 *
 * Description: notify about new IO request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_IO_Request(
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
    )
{

    /* 
     * instanse handler, packet ftype and ttype, parameters for size dection
     * split implementation 
     */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    unsigned int ttype = 0, ftype = 0;
    unsigned int size, wdptr;
    unsigned int need_Split, max_Size;
    int result;  /*result of routine execution*/

    /* check pointers */
    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;


    /* detect request ftype and ttype fields */
    detect_IO_Type(&ftype, &ttype, rq->ttype);

    /* check priority value */
    if (rq->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)rq->prio);
        return RIO_ERROR;
    }

    /* GDA:Bug#43 fix */
    if (instanse->inst_Param_Set.gda_Tx_Illegal_Packet && rq->dw_Size > 32)
	{
#ifndef _CARBON_SRIO_XTOR
	printf("\n<!Warning at Instance %u> Request for more than 32 DWords\n",\
		instanse->instance_Number);
#endif
	}

    if (rq->dw_Size > 32 && !(instanse->inst_Param_Set.gda_Tx_Illegal_Packet))
    {
#ifndef _CARBON_SRIO_XTOR
	printf("\n<!Error at Instance %u> Request for more than 32 DWORDs, Which is not allowed\n",\
		instanse->instance_Number);
#else
	char msg[250];
	sprintf(msg,"%s%lu%s","\n<!Error at Instance",instanse->instance_Number,
			"> Request for more than 32 DWORDs, Which is not allowed\n");

	instanse->ext_Interfaces.rio_User_Msg(instanse->ext_Interfaces.rio_Msg, msg);
#endif
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, ftype, (unsigned long)ttype);
        return RIO_ERROR;
    }

    /* check size */
    if (rq->dw_Size > RIO_PL_MAX_PAYLOAD_SIZE || !rq->dw_Size)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, ftype, (unsigned long)ttype);
        return RIO_ERROR;
    }

    /* atomic transaction are allowed to be in dword boundary*/
    /*if ((ftype == RIO_WRITE) && (ttype == RIO_WRITE_ATOMIC_TSWAP) && (rq->dw_Size > 1)) - orig */
    if ((ftype == RIO_WRITE) && (ttype == RIO_WRITE_ATOMIC_TSWAP ||
	/* GDA: Added for Bug#133. Included feature for TType C for Type 5 packet */
	ttype == RIO_WRITE_ATOMIC_SWAP) &&
	/* GDA: End */
	(rq->dw_Size > 1))
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, ftype, (unsigned long)ttype);
        return RIO_ERROR;
    }

    if ((ftype == RIO_NONINTERV_REQUEST) && 
        (ttype == RIO_NONINTERV_ATOMIC_INC ||
        ttype == RIO_NONINTERV_ATOMIC_DEC ||
        ttype == RIO_NONINTERV_ATOMIC_SET ||
        ttype == RIO_NONINTERV_ATOMIC_CLEAR) && (rq->dw_Size > 1))
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, ftype, (unsigned long)ttype);
        return RIO_ERROR;
    }

    /* streeming write cannot be within dword boundary, either*/
    if (ftype == RIO_STREAM_WRITE && rq->dw_Size == 1 && rq->byte_Num != RIO_PL_BYTE_CNT_IN_DWORD)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, ftype, (unsigned long)ttype);
        return RIO_ERROR;
    }

    /* detect allowed multidouble word payload - these constant 
     * mean count of double-words - their themself 
     */
    if (rq->ttype == RIO_IO_NREAD &&
        rq->dw_Size != 1 &&
        rq->dw_Size != 2 &&
        rq->dw_Size != 4 &&
        rq->dw_Size != 8 &&
        rq->dw_Size != 16 &&
        rq->dw_Size != 32 &&
        rq->dw_Size != 12 &&
        rq->dw_Size != 20 &&
        rq->dw_Size != 24 &&
        rq->dw_Size != 28 &&
        rq->dw_Size != 32)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, ftype, (unsigned long)ttype);
        return RIO_ERROR;
    }


    /* detect size and wdprt */
    need_Split = 0;


        if((rq->ttype == RIO_IO_NWRITE ||
            rq->ttype == RIO_IO_SWRITE ||
            rq->ttype == RIO_IO_NWRITE_R) &&
        (rq->dw_Size == 12 ||
            rq->dw_Size == 20 ||
            rq->dw_Size == 24 ||
            rq->dw_Size == 28))
    {
        /* these sizes are reserved for write transactions and are not reserved for read, so
           we cant call RIO_PL_Detect_IO_Size_Wdprt directly because it doesn't discern between
           read and writes, so we try to fool it; sizes between 8dw and 16, as well as 
           between 16 and 32 dws are handled in it */
        switch( rq->dw_Size )
        {
            case 12:
                RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, 16, rq->byte_Num, rq->offset);
                break;
            case 20:
            case 24:
            case 28:
                RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, 32, rq->byte_Num, rq->offset);
                break;
            default:
                break;        
        }
    }
    else
    {
        /* convert dwsize and other to packet size parameters and check if any engine is necessary*/
        switch (RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, rq->dw_Size, rq->byte_Num, rq->offset))
        {
            case RIO_OK: /* OK */
                break;

            case RIO_RETRY: /* need misaligned engine */
                need_Split = 1;
                break;

            default:
                return RIO_ERROR;  /* GDA: 133bug error includes the null byte_Num - subdouble dw 
					   Doesnt gives any usermsg for all pl transactions */
        }
    }

    /* additional checking for split engine*/
    if (RIO_PL_Is_Payload(ftype, ttype) == RIO_OK)
        max_Size = RIO_PL_Get_Max_Req_Size(handle);
    else
        max_Size = RIO_PL_Get_Max_Resp_Dem_Size(handle);

    /* convert size to count of dwords */
    max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(max_Size);

    /* detect if split engine is necessary */
    if (rq->dw_Size > max_Size)
        need_Split = 1;

    /* load request to temporary buffer and try to load it to the outbound */    
    if (need_Split)
    {
        /* if size 1 - misaligned engine is necessary */
        if (rq->dw_Size == 1)

        {
            /* if we request cross dword boundary, cut it */
            if ((rq->offset + rq->byte_Num) > RIO_PL_BYTE_CNT_IN_DWORD)
                RIO_PL_GET_TEMP(instanse).byte_Num = (RIO_UCHAR_T)(RIO_PL_BYTE_CNT_IN_DWORD - rq->offset); 
            else
                RIO_PL_GET_TEMP(instanse).byte_Num = rq->byte_Num; 

            RIO_PL_GET_TEMP(instanse).byte_Offset = rq->offset;
        }
        
        /* store maximum size for splitting */
        RIO_PL_GET_TEMP(instanse).max_Size = max_Size;
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_WAIT_SPLIT;
    }
    else
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;

    /* load request to the temporary */
    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;
    RIO_PL_GET_TEMP(instanse).payload_Size = rq->dw_Size;

    /* try to load payload */
    if (RIO_PL_Is_Payload(ftype, ttype) == RIO_OK)
    {
        RIO_PL_GET_TEMP(instanse).payload_Source = rq->data;

        /* try to load the payload at once */
        if (rq->data)
        {
            unsigned int i; /* loop counter */
            
            for (i = 0; i < rq->dw_Size; i++)
                RIO_PL_GET_TEMP(instanse).payload[i] = rq->data[i];
        }
    }
   
    /*Bug#137 */
    instanse->trans_Id = rq->trans_Id;
    /*Bug#137 */
        
    /* form packet fields */
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;

    RIO_PL_GET_TEMP(instanse).entry.prio = rq->prio;
    RIO_PL_GET_TEMP(instanse).entry.ftype = ftype;
    RIO_PL_GET_TEMP(instanse).entry.ttype = ttype;
    RIO_PL_GET_TEMP(instanse).entry.tranp_Info = transport_Info;
    RIO_PL_GET_TEMP(instanse).entry.tt = rq->transp_Type;
    RIO_PL_GET_TEMP(instanse).entry.address = (rq->address >> 3);
    RIO_PL_GET_TEMP(instanse).entry.extended_Address = rq->extended_Address;
    RIO_PL_GET_TEMP(instanse).entry.xamsbs = rq->xamsbs;/*(instanse->parameters_Set.xamsbs == RIO_TRUE) ? rq->xamsbs : 0;*/
    RIO_PL_GET_TEMP(instanse).entry.ext_Address_Valid = instanse->outbound_Buf.out_Buf_Param_Set.ext_Address_Valid;
    RIO_PL_GET_TEMP(instanse).entry.ext_Address_16 = instanse->outbound_Buf.out_Buf_Param_Set.ext_Address_16;
    RIO_PL_GET_TEMP(instanse).entry.wdptr = wdptr;
    RIO_PL_GET_TEMP(instanse).entry.size = size;

    /* try load request to the buffer at once */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Message_Request
 *
 * Description: notify about new message request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Message_Request(
    RIO_HANDLE_T       handle,
    RIO_MESSAGE_REQ_T  *rq,
    RIO_TAG_T          trx_Tag             
    )
{
    /* parameters to hold instanse handler and detect size of packet and split */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    unsigned int size/*, wdptr*/;
    unsigned int max_Size, max_Cnt;
    int result;  /*result of routine execution*/


    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;
    /* 
     * load request to temporary buffer and try to load it to the outbound
     * check parameters 
     * check priority value 
     */
    if (rq->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)rq->prio);
        return RIO_ERROR;
    }

    /* check payload size */
    if (rq->ssize == 0 ||
        (rq->ssize != 1 &&
        rq->ssize != 2 &&
        rq->ssize != 4 &&
        rq->ssize != 8 &&
        rq->ssize != 16 &&
        rq->ssize != 32))
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, (unsigned long)RIO_MESSAGE, 0lu);
        return RIO_ERROR;
    }

    /* check mbox */
    if (rq->mbox > RIO_PL_MAX_MBOX_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_35, (unsigned long)rq->mbox);
        return RIO_ERROR;
    }

    /* check msglen */
    if (rq->msglen > RIO_PL_MAX_MSGLEN_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_36, (unsigned long)rq->msglen);
        return RIO_ERROR;
    }

    /* check msgseg */
    if ( ((rq->msgseg > rq->msglen) && (rq->msglen > 0)) 
        || ((rq->msgseg > rq->msglen) && (rq->msglen == 0) && (instanse->parameters_Set.ext_Mailbox_Support != RIO_TRUE)) )
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_37, (unsigned long)rq->msgseg, (unsigned long)rq->msglen);
        return RIO_ERROR;
    }

    /* check dw_Num */
    if ( (rq->msgseg == rq->msglen) || ((rq->msglen == 0) && (instanse->parameters_Set.ext_Mailbox_Support == RIO_TRUE)) )
    {
        if (rq->dw_Size > rq->ssize)
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_38, (unsigned long)rq->dw_Size, (unsigned long)rq->ssize);
            return RIO_ERROR;
        }
    }
    else
    {
        if (rq->dw_Size != rq->ssize)
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_39, (unsigned long)rq->dw_Size, (unsigned long)rq->ssize);
            return RIO_ERROR;
        }
    }

    if (rq->letter > RIO_PL_MAX_LETTER_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_40, (unsigned long)rq->letter);   
        return RIO_ERROR;
    }

    /* additional size checking */
    /*  RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, rq->ssize, RIO_PL_BYTE_CNT_IN_DWORD, 0);*/
    if( rq->ssize == 1 )                /* 1001 */
        size = 9;
    else if( rq->ssize == 2 )   /* 1010 */
        size = 10;
    else if( rq->ssize == 4 )   /* 1011 */
        size = 11;
    else if( rq->ssize == 8 )   /* 1100 */
        size = 12;
    else if( rq->ssize == 16 )  /* 1101 */
        size = 13;
    else if( rq->ssize == 32 )  /* 1110 */
        size = 14;
    else
        return RIO_ERROR;
        
        max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(RIO_PL_Get_Max_Mes_Size(instanse, rq->mbox));


    /* check if payload is less than we can issue */
    if (rq->dw_Size > max_Size)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_41, (unsigned long)rq->dw_Size, (unsigned long)max_Size);
        return RIO_ERROR;
    }

    /* check segments count*/
    max_Cnt = RIO_PL_Get_Max_Mes_Cnt(handle, rq->mbox);
    if (rq->msglen > max_Cnt)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_42, (unsigned long)rq->msglen, (unsigned long)max_Cnt);
        return RIO_ERROR;
    }

    /* load request to temporary buffer and try to load it to the outbound */
    RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;
    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;
    RIO_PL_GET_TEMP(instanse).payload_Size = rq->dw_Size;
    RIO_PL_GET_TEMP(instanse).payload_Source = rq->data;

    /* try to load the payload at once */
    if (rq->data)
    {
        unsigned int i; /* loop counter */
        
        for (i = 0; i < rq->dw_Size; i++)
            RIO_PL_GET_TEMP(instanse).payload[i] = rq->data[i];
    }
    
    /* form packet fields */
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;

    RIO_PL_GET_TEMP(instanse).entry.prio = rq->prio;
    RIO_PL_GET_TEMP(instanse).entry.ftype = RIO_MESSAGE;
    RIO_PL_GET_TEMP(instanse).entry.tranp_Info = rq->transport_Info;
    RIO_PL_GET_TEMP(instanse).entry.tt = rq->transp_Type;
    RIO_PL_GET_TEMP(instanse).entry.msglen = rq->msglen;
    RIO_PL_GET_TEMP(instanse).entry.size = size;
    RIO_PL_GET_TEMP(instanse).entry.letter = rq->letter;
    RIO_PL_GET_TEMP(instanse).entry.mbox = rq->mbox;
    RIO_PL_GET_TEMP(instanse).entry.msgseg = rq->msgseg;

    /* this target_info coding performs here to build information which
     * comes with response as whole byte - constants are used here only
     */
    RIO_PL_GET_TEMP(instanse).entry.src_TID = ((rq->letter & 0x03) << 6) +
            ((rq->mbox & 0x03) << 4) + (rq->msgseg & 0x0f);
    /* load request */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Doorbell_Request
 *
 * Description: notify about new doorbell request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Doorbell_Request(
    RIO_HANDLE_T        handle,
    RIO_DOORBELL_REQ_T  *rq,
    RIO_TAG_T           trx_Tag 
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    int result;  /*result of routine execution*/

    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check priority value */
    if (rq->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)rq->prio);
        return RIO_ERROR;
    }

    /* load request to temporary buffer and try to load it to the outbound */
    RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;
    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;
    
    /* form packet fields */
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;

    RIO_PL_GET_TEMP(instanse).entry.prio = rq->prio;
    RIO_PL_GET_TEMP(instanse).entry.ftype = RIO_DOORBELL;
    RIO_PL_GET_TEMP(instanse).entry.tranp_Info = rq->transport_Info;
    RIO_PL_GET_TEMP(instanse).entry.tt = rq->transp_Type;
    RIO_PL_GET_TEMP(instanse).entry.doorbell_Info = rq->doorbell_Info;

    /* load request */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Configuration_Request
 *
 * Description: notify about new configuration request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Configuration_Request(
    RIO_HANDLE_T      handle,
    RIO_CONFIG_REQ_T  *rq,
    RIO_TAG_T         trx_Tag
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    unsigned int size, wdptr, byte_Num, offset;
    unsigned int max_Size;
    unsigned int need_Split = 0;
    int result;  /*result of routine execution*/


    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check ttype transaction parameters */
    if (rq->rw != RIO_MAINTENANCE_CONF_READ &&
        rq->rw != RIO_MAINTENANCE_CONF_WRITE &&
        rq->rw != RIO_MAINTENANCE_PORT_WRITE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_25, (unsigned long)rq->rw, (unsigned long)RIO_MAINTENANCE);
        return RIO_ERROR;
    }

    /* check priority value */
    if (rq->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)rq->prio);
        return RIO_ERROR;
    }

    /* check dw_Size */
    if (rq->dw_Size > RIO_PL_BYTE_CNT_IN_DWORD || !rq->dw_Size)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, (unsigned long)RIO_MAINTENANCE, 
            (unsigned long)rq->rw);
        return RIO_ERROR;
    }

    /*additional dw_Size checking - means size of payload in count of double-words */
    if (rq->rw == RIO_MAINTENANCE_CONF_READ && 
        (rq->dw_Size != 1 &&
        rq->dw_Size != 2 &&
        rq->dw_Size != 4 &&
        rq->dw_Size != 8))
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->dw_Size, (unsigned long)RIO_MAINTENANCE, 
            (unsigned long)rq->rw);
        return RIO_ERROR;
    }
    /* check if we need split request*/
    if (rq->dw_Size == 1)
    {
        if (rq->sub_Dw_Pos == 0)
        {
            offset = 0;
            byte_Num = RIO_PL_BYTE_CNT_IN_WORD;
        }
        else if (rq->sub_Dw_Pos == 1)
        {
            offset = RIO_PL_THE_SECOND_WORD_OFFSET;
            byte_Num = RIO_PL_BYTE_CNT_IN_WORD;
        }
        else
        {
            byte_Num = RIO_PL_BYTE_CNT_IN_DWORD;
            offset = 0;
        }
    }
    else
    {
        byte_Num = RIO_PL_BYTE_CNT_IN_DWORD;
        offset = 0;
    }

    /* convert size */
    RIO_PL_Detect_IO_Size_Wdprt(&size, &wdptr, rq->dw_Size, byte_Num, offset);

    /* check configured parameters */
    if (rq->rw == RIO_MAINTENANCE_CONF_WRITE)
    {
        max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(
            RIO_PL_Get_Max_Req_Size(instanse));

        if (rq->dw_Size > max_Size)
            need_Split = 1;
    }
    else if (rq->rw == RIO_MAINTENANCE_PORT_WRITE)
    {
        max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(
            RIO_PL_Get_Max_Port_Wr_Size(instanse));

        if (rq->dw_Size > max_Size)
            need_Split = 1;
    }
    else
    {
        max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(
            RIO_PL_Get_Max_Resp_Dem_Size(instanse));

        if (rq->dw_Size > max_Size)
            need_Split = 1;

    }
    /* load request to temporary buffer and try to load it to the outbound */
    if (need_Split)
    {
        RIO_PL_GET_TEMP(instanse).max_Size = max_Size;
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_WAIT_SPLIT;
    }
    else
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;

    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;
    RIO_PL_GET_TEMP(instanse).payload_Size = rq->dw_Size; /* write uses maximum values */

    /* initialize payload referenced fields */
    if (RIO_PL_Is_Payload(RIO_MAINTENANCE, rq->rw) == RIO_OK)
    {
        RIO_PL_GET_TEMP(instanse).payload_Source = rq->data;

        /* try to load the payload at once */
        if (rq->data)
        {
            unsigned int i; /* loop counter */
            
            for (i = 0; i < rq->dw_Size; i++)
                RIO_PL_GET_TEMP(instanse).payload[i] = rq->data[i];
        }
    }

    /* complete packet fields info */
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;

    RIO_PL_GET_TEMP(instanse).entry.prio = rq->prio;

    if (rq->rw != RIO_MAINTENANCE_PORT_WRITE)
        /* set offset to double-word boundary (3) digit */
        RIO_PL_GET_TEMP(instanse).entry.config_Off = rq->config_Offset >> 3;
    else 
        RIO_PL_GET_TEMP(instanse).entry.config_Off = 0;

    RIO_PL_GET_TEMP(instanse).entry.ttype = rq->rw;
    RIO_PL_GET_TEMP(instanse).entry.ftype = RIO_MAINTENANCE;
    RIO_PL_GET_TEMP(instanse).entry.tranp_Info = rq->transport_Info;
    RIO_PL_GET_TEMP(instanse).entry.tt = rq->transp_Type;
    RIO_PL_GET_TEMP(instanse).entry.hop_Count = rq->hop_Count;
    RIO_PL_GET_TEMP(instanse).entry.wdptr = wdptr;
    RIO_PL_GET_TEMP(instanse).entry.size = size;

    /* try to load the request immediately */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Send_Response
 *
 * Description: notify about new response
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Send_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
    )
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    unsigned int max_Size;
    int result;  /*result of routine execution*/

    if (instanse == NULL || resp == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check if it's a response */
    if (RIO_PL_Is_Req(resp->ftype, resp->transaction) == RIO_OK)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_25, (unsigned long)resp->ftype, (unsigned long)resp->transaction);
        return RIO_ERROR; 
    }

    /* check priority value */
    if (resp->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)resp->prio);
        return RIO_ERROR;
    }

    /* check payload size - count of double-words are checking */
    if (resp->ftype == RIO_MAINTENANCE &&
        resp->transaction == RIO_MAINTENANCE_READ_RESPONCE && 
        (resp->dw_Num != 1 &&
        resp->dw_Num != 2 &&
        resp->dw_Num != 4 &&
        resp->dw_Num != 8))
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)resp->dw_Num, 
            (unsigned long)resp->ftype, (unsigned long)resp->transaction);
        return RIO_ERROR;
    }

    /* the response format can has bigger payload */
    if (resp->ftype == RIO_RESPONCE &&
        (resp->transaction == RIO_RESPONCE_WITH_DATA ||
        resp->transaction == RIO_RESPONCE_INTERVENTION ||
        resp->transaction == RIO_RESPONCE_INTERVENTION_WO_DATA) &&
        (resp->dw_Num != 1 &&
        resp->dw_Num != 2 &&
        resp->dw_Num != 4 &&
        resp->dw_Num != 8 &&
        resp->dw_Num != 12 &&
        resp->dw_Num != 16 &&
        resp->dw_Num != 20 &&
        resp->dw_Num != 24 &&
        resp->dw_Num != 28 &&
        resp->dw_Num != 32))
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)resp->dw_Num, 
            (unsigned long)resp->ftype, (unsigned long)resp->transaction);
        return RIO_ERROR;
    }
    /* check maximum and configured sizes */
    if (resp->transaction ==  RIO_RESPONCE_INTERVENTION ||
        resp->transaction == RIO_RESPONCE_INTERVENTION_WO_DATA ||
        RIO_PL_Is_Payload(resp->ftype, resp->transaction) == RIO_OK)
    {
        max_Size = RIO_PL_Get_Max_Resp_Size(handle);
        max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(max_Size);

        if (resp->dw_Num > max_Size)
        {
            RIO_PL_Print_Message(handle, RIO_PL_ERROR_43, (unsigned long)resp->dw_Num, 
                (unsigned long)max_Size);
            return RIO_ERROR;
        }
    }

    /* load temp */
    if (resp->transaction == RIO_RESPONCE_INTERVENTION ||
        resp->transaction == RIO_RESPONCE_INTERVENTION_WO_DATA)
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_WAIT_INTERVEN;
    else
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;

    RIO_PL_GET_TEMP(instanse).trx_Tag = tag;

    /* store all fields */
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;

    RIO_PL_GET_TEMP(instanse).entry.ftype = resp->ftype;
    RIO_PL_GET_TEMP(instanse).entry.ttype = resp->transaction;
    RIO_PL_GET_TEMP(instanse).entry.src_TID = resp->target_TID;
    RIO_PL_GET_TEMP(instanse).entry.tranp_Info = resp->tr_Info;
    RIO_PL_GET_TEMP(instanse).entry.tt = resp->transp_Type;
    RIO_PL_GET_TEMP(instanse).entry.prio = resp->prio;
    RIO_PL_GET_TEMP(instanse).entry.sec_ID = resp->sec_ID;
    RIO_PL_GET_TEMP(instanse).entry.sec_TID = resp->sec_TID;

    /* in maintenam=nce status is returning in size field */
    if (resp->ftype == RIO_MAINTENANCE)
    {
        RIO_PL_GET_TEMP(instanse).entry.size = resp->status;
        /*set the reserved field to 0 and hop count to FF*/
        RIO_PL_GET_TEMP(instanse).entry.hop_Count = 0xFF;
        RIO_PL_GET_TEMP(instanse).entry.config_Off = 0;
    }
    else
        RIO_PL_GET_TEMP(instanse).entry.status = resp->status;

    /* load payload, if any */
    if (resp->transaction == RIO_RESPONCE_INTERVENTION ||
        resp->transaction == RIO_RESPONCE_INTERVENTION_WO_DATA ||
        RIO_PL_Is_Payload(resp->ftype, resp->transaction) == RIO_OK)
    {
        RIO_PL_GET_TEMP(instanse).payload_Source = resp->data;
        RIO_PL_GET_TEMP(instanse).payload_Size = resp->dw_Num;
        
        /* try to load payload at once */
        if (resp->data)
        {
            unsigned int i; /* loop counter */
            
            for (i = 0; i < resp->dw_Num; i++)
                RIO_PL_GET_TEMP(instanse).payload[i] = resp->data[i];
        }
    }
    
    /* try to load response at once */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Packet_Struct_Request
 *
 * Description: request for a packet by field types
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Packet_Struct_Request(
    RIO_HANDLE_T       handle, 
    RIO_MODEL_PACKET_STRUCT_RQ_T *rq, 
    RIO_TAG_T          trx_Tag
)
{
    /* 
     * instanse handler, packet ftype and ttype, parameters for size dection
     * split implementation 
     */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    int result;  /*result of routine execution*/


    /* check pointers */
    if (instanse == NULL || rq == NULL)
        return RIO_ERROR;

    if (instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if (IS_NOT_A_BOOL(rq->is_Int_Crc_Valid))
        return RIO_PARAM_INVALID;

    if (IS_NOT_A_BOOL(rq->is_Crc_Valid))
        return RIO_PARAM_INVALID;

    if (rq->dw_Size > RIO_PL_MAX_PAYLOAD_SIZE)
        return RIO_PARAM_INVALID;
    
    /*store request fields*/
    instanse->outbound_Buf.temp.entry.prio = rq->packet.prio;
    instanse->outbound_Buf.temp.entry.tt = rq->packet.tt;
    instanse->outbound_Buf.temp.entry.ftype = rq->packet.ftype;
    instanse->outbound_Buf.temp.entry.tranp_Info = rq->packet.transport_Info;
    instanse->outbound_Buf.temp.entry.ttype = rq->packet.ttype;
    
    if (rq->packet.ftype == RIO_MESSAGE)
        instanse->outbound_Buf.temp.entry.size = rq->packet.ssize;
    else
        instanse->outbound_Buf.temp.entry.size = rq->packet.rdwr_Size;
    
    instanse->outbound_Buf.temp.entry.src_TID = rq->packet.srtr_TID;
    instanse->outbound_Buf.temp.entry.hop_Count = rq->packet.hop_Count;

    /*in case of configuration response the config offset is replaced
    with the reserved field values*/
    if ((rq->packet.ftype == RIO_MAINTENANCE) && 
        ((rq->packet.ttype == 2) || (rq->packet.ttype == 3))
    )
        instanse->outbound_Buf.temp.entry.config_Off = rq->packet.rsrv_Ftype8;
    else 
        instanse->outbound_Buf.temp.entry.config_Off = rq->packet.config_Offset;

    instanse->outbound_Buf.temp.entry.wdptr = rq->packet.wdptr;
    instanse->outbound_Buf.temp.entry.status = rq->packet.status;
    instanse->outbound_Buf.temp.entry.doorbell_Info = rq->packet.doorbell_Info;
    instanse->outbound_Buf.temp.entry.msglen = rq->packet.msglen;
    instanse->outbound_Buf.temp.entry.letter = rq->packet.letter;
    instanse->outbound_Buf.temp.entry.mbox = rq->packet.mbox;
    instanse->outbound_Buf.temp.entry.msgseg = rq->packet.msgseg;
    instanse->outbound_Buf.temp.entry.address = rq->packet.address;
    instanse->outbound_Buf.temp.entry.extended_Address = rq->packet.extended_Address;
    instanse->outbound_Buf.temp.entry.xamsbs = rq->packet.xamsbs;
    instanse->outbound_Buf.temp.entry.ext_Address_Valid = rq->packet.ext_Address_Valid;
    instanse->outbound_Buf.temp.entry.ext_Address_16 = rq->packet.ext_Address_16;

    instanse->outbound_Buf.temp.entry.sec_ID = 0; /*TBD*/
    instanse->outbound_Buf.temp.entry.sec_TID = 0; /*TBD*/

    instanse->outbound_Buf.temp.entry.additional_Fields_Valid = RIO_TRUE; 
    
    /*additional fields that are used for the packet struct request*/
    instanse->outbound_Buf.temp.entry.rsrv_Phy_1 = rq->packet.rsrv_Phy_1;
    instanse->outbound_Buf.temp.entry.rsrv_Phy_2 = rq->packet.rsrv_Phy_2;
    instanse->outbound_Buf.temp.entry.rsrv_Ftype6 = rq->packet.rsrv_Ftype6;
    instanse->outbound_Buf.temp.entry.rsrv_Ftype8 = rq->packet.rsrv_Ftype8;
    instanse->outbound_Buf.temp.entry.rsrv_Ftype10 = rq->packet.rsrv_Ftype10;

    instanse->outbound_Buf.temp.entry.intermediate_Crc = rq->intermediate_Crc;
    instanse->outbound_Buf.temp.entry.is_Int_Crc_Valid = rq->is_Int_Crc_Valid;
    instanse->outbound_Buf.temp.entry.crc = rq->crc;
    instanse->outbound_Buf.temp.entry.is_Crc_Valid = rq->is_Crc_Valid;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    instanse->outbound_Buf.temp.entry.s_Ds = rq->packet.s_Ds;
    instanse->outbound_Buf.temp.entry.e_Ds = rq->packet.e_Ds;
    instanse->outbound_Buf.temp.entry.cos = rq->packet.cos;
    instanse->outbound_Buf.temp.entry.length = rq->packet.length;
    instanse->outbound_Buf.temp.entry.stream_ID = rq->packet.stream_ID;
    instanse->outbound_Buf.temp.entry.o = rq->packet.o;
    instanse->outbound_Buf.temp.entry.p = rq->packet.p;
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    instanse->outbound_Buf.temp.entry.soc = rq->packet.soc;
    instanse->outbound_Buf.temp.entry.tgtdest_ID = rq->packet.tgtdest_ID;
    instanse->outbound_Buf.temp.entry.dest_ID = rq->packet.dest_ID;
    instanse->outbound_Buf.temp.entry.flow_ID = rq->packet.flow_ID;
    instanse->outbound_Buf.temp.entry.x_On_Off = rq->packet.x_On_Off;
    /* GDA: Bug#125 */

    /*user tag and payload*/
    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;
    RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;

    RIO_PL_GET_TEMP(instanse).payload_Size = rq->dw_Size; /* write uses maximum values */
    /*store payload if necessary*/
    if (rq->data)
    {
        unsigned int i; /* loop counter */
        for (i = 0; i < rq->dw_Size; i++)
            RIO_PL_GET_TEMP(instanse).payload[i] = rq->data[i];
    } 

    /* try to load response at once */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/***************************************************************************************
  Static functions implementation
***************************************************************************************/
 

/***************************************************************************
 * Function : detect_IO_Type
 *
 * Description: detect IO transaction ftype and ttype fields
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void detect_IO_Type(unsigned int *ftype, unsigned int *ttype, RIO_IO_TTYPE_T type)
{
    switch (type)
    {
        case RIO_IO_NREAD:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_NREAD;
            break;

        case RIO_IO_NWRITE:
            *ftype = RIO_WRITE;
            *ttype = RIO_WRITE_NWRITE;
            break;

        case RIO_IO_SWRITE:
            *ftype = RIO_STREAM_WRITE;
            *ttype = 0;
            break;

        case RIO_IO_NWRITE_R:
            *ftype = RIO_WRITE;
            *ttype = RIO_WRITE_NWRITE_R;
            break;

        case RIO_IO_ATOMIC_INC:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_ATOMIC_INC;
            break;

        case RIO_IO_ATOMIC_DEC:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_ATOMIC_DEC;
            break;

	/* GDA: Added for Bug#133. Included feature for TType C for Type 5 packet */
        case RIO_IO_ATOMIC_SWAP:
            *ftype = RIO_WRITE;
            *ttype = RIO_WRITE_ATOMIC_SWAP;
            break;
	/* GDA: End */

        case RIO_IO_ATOMIC_TSWAP:
            *ftype = RIO_WRITE;
            *ttype = RIO_WRITE_ATOMIC_TSWAP;
            break;

        case RIO_IO_ATOMIC_SET:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_ATOMIC_SET;
            break;

        case RIO_IO_ATOMIC_CLEAR:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_ATOMIC_CLEAR;
            break;
    }
}

/***************************************************************************
 * Function : detect_GSM_Type
 *
 * Description: detect IO transaction ftype and ttype fields
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void detect_GSM_Type(unsigned int *ftype, unsigned int *ttype, RIO_PL_GSM_TTYPE_T type)
{
    switch (type)
    {
        case RIO_PL_GSM_READ_HOME:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_READ_HOME;
            break;

        case RIO_PL_GSM_IREAD_HOME:
            *ftype = RIO_NONINTERV_REQUEST;  
            *ttype = RIO_NONINTERV_IREAD_HOME;
            break;

        case RIO_PL_GSM_READ_TO_OWN_HOME:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_READ_TO_OWN_HOME;
            break;

        case RIO_PL_GSM_CASTOUT:
            *ftype = RIO_WRITE;
            *ttype = RIO_WRITE_CASTOUT;
            break;

        case RIO_PL_GSM_DKILL_HOME:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_DKILL_HOME;
            break;

        case RIO_PL_GSM_TLBIE:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_TLBIE;
            break;

        case RIO_PL_GSM_TLBSYNC:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_TLBSYNC;
            break;

        case RIO_PL_GSM_IKILL_HOME:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_IKILL_HOME;
            break;

        case RIO_PL_GSM_IKILL_SHARER:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_IKILL_SHARER;
            break;

        case RIO_PL_GSM_FLUSH_WITH_DATA:
            *ftype = RIO_WRITE;
            *ttype = RIO_WRITE_FLUSH_WITH_DATA;
            break;

        case RIO_PL_GSM_FLUSH_WO_DATA:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_FLUSH_WO_DATA;
            break;

        case RIO_PL_GSM_IO_READ_HOME:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_IO_READ_HOME;
            break;

        case RIO_PL_GSM_READ_OWNER:
            *ftype = RIO_INTERV_REQUEST;
            *ttype = RIO_INTERV_READ_OWNER;
            break;

        case RIO_PL_GSM_READ_TO_OWN_OWNER:
            *ftype = RIO_INTERV_REQUEST;
            *ttype = RIO_INTERV_READ_TO_OWN_OWNER;
            break;

        case RIO_PL_GSM_IO_READ_OWNER:
            *ftype = RIO_INTERV_REQUEST;
            *ttype = RIO_INTERV_IO_READ_OWNER;
            break;

        case RIO_PL_GSM_DKILL_SHARER:
            *ftype = RIO_NONINTERV_REQUEST;
            *ttype = RIO_NONINTERV_DKILL_SHARER;
            break;
    }
}

/****************************************************************************/

/* GDA: Bug#124 - Support for Data Streaming packet */

/***************************************************************************
 * Function : RIO_PL_DS_Request
 *
 * Description: notify about new DataStreaming request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_DS_Request(
    RIO_HANDLE_T   handle,
    RIO_DS_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
    )
{

    /* 
     * instanse handler, packet ftype and ttype, parameters for size dection
     * split implementation 
     */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    unsigned int  ftype = RIO_DS;
    unsigned short need_Split, max_Size;
    int result;  /*result of routine execution*/

    /* check pointers */
    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;



    /* check priority value */
    if (rq->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)rq->prio);
        return RIO_ERROR;
    }

    /* GDA:Bug#43 fix */
#ifndef _CARBON_SRIO_XTOR_FIX
    // This can never happen since rq->length is an unsigned short wich cannot be > 0xFFFF
    if (instanse->inst_Param_Set.gda_Tx_Illegal_Packet && rq->length > 0xFFFF)
	{
	printf("\n<!Warning at Instance %u> Request for more than 32k HalfWords\n",\
		instanse->instance_Number);
	}
    // This can never happen since rq->length is an unsigned short wich cannot be > 0xFFFF
    if (rq->length > 0xFFFF && !(instanse->inst_Param_Set.gda_Tx_Illegal_Packet))
    {
	printf("\n<!Error at Instance %u> Request for more than 32K HALFWORDs, Which is not allowed\n",\
		instanse->instance_Number);
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned long)rq->length, ftype, 0);
        return RIO_ERROR;
    }
#endif
	
    /* check size */
    if (rq->hw_Size > RIO_PL_MAX_DS_PAYLOAD_SIZE )
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_26, (unsigned short)rq->hw_Size, ftype,0);
        return RIO_ERROR;
    }


ftype = RIO_DS;

    /* additional checking for split engine*/
    if (RIO_PL_Is_Payload(ftype,0)  == RIO_OK)
        max_Size = RIO_PL_Get_Max_Req_Size(handle);
    else
        max_Size = RIO_PL_Get_Max_Resp_Dem_Size(handle);

    /* convert size to count of HALF words */
    max_Size = RIO_PL_Conv_Size_To_Hw_Cnt(max_Size);

    /* detect if split engine is necessary */
    if (rq->hw_Size > max_Size)
        need_Split = 1;
     else
	need_Split = 0;

    /* load request to temporary buffer and try to load it to the outbound */    
    if (need_Split)
    {
        /* if size 1 - misaligned engine is necessary */
        if (rq->hw_Size == 1)

        {
            /* if we request cross dword boundary, cut it */
            if ((rq->offset + rq->byte_Num) > RIO_PL_BYTE_CNT_IN_HALF_WORD)
                RIO_PL_GET_TEMP(instanse).byte_Num = (RIO_UCHAR_T)(RIO_PL_BYTE_CNT_IN_HALF_WORD - rq->offset); 
            else
                RIO_PL_GET_TEMP(instanse).byte_Num = rq->byte_Num; 

            RIO_PL_GET_TEMP(instanse).byte_Offset = rq->offset;
        }
        
        /* store maximum size for splitting */
        RIO_PL_GET_TEMP(instanse).max_Size = max_Size;
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_WAIT_SPLIT;
    }
    else
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;

    /* load request to the temporary */
    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;
    RIO_PL_GET_TEMP(instanse).payload_Size = rq->hw_Size;

    /* try to load payload */
    if (RIO_PL_Is_Payload(ftype,0 ) == RIO_OK)
    {
        RIO_PL_GET_TEMP(instanse).payload_Source_hw = rq->data_hw;

        /* try to load the payload at once */
        if (rq->data_hw)
        {
            unsigned int i; /* loop counter */
            
            for (i = 0; i < rq->hw_Size; i++)
                RIO_PL_GET_TEMP(instanse).payload_hw[i] = rq->data_hw[i];
        }
    }
   
        
    /* form packet fields */
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;

    RIO_PL_GET_TEMP(instanse).entry.prio = rq->prio;
    RIO_PL_GET_TEMP(instanse).entry.ftype = ftype;
    RIO_PL_GET_TEMP(instanse).entry.tranp_Info = transport_Info;
    RIO_PL_GET_TEMP(instanse).entry.tt = rq->transp_Type;
    RIO_PL_GET_TEMP(instanse).entry.s_Ds =rq->s_Ds ;
    RIO_PL_GET_TEMP(instanse).entry.e_Ds = rq->e_Ds;
    RIO_PL_GET_TEMP(instanse).entry.stream_ID = rq->stream_ID;
    RIO_PL_GET_TEMP(instanse).entry.length = rq->length;
    RIO_PL_GET_TEMP(instanse).entry.o = rq->o;
    RIO_PL_GET_TEMP(instanse).entry.p = rq->p;
    RIO_PL_GET_TEMP(instanse).entry.cos =rq->cos;

    /* try load request to the buffer at once */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/* GDA: Bug#124 - Support for Data Streaming packet */
/***************************************************************************/


/* GDA: Bug#125 - Support for Flow control packet */
/***************************************************************************
 * Function : RIO_PL_FC_Request
 *
 * Description: notify about new IO request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_FC_Request(
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,
    RIO_TR_INFO_T  transport_Info
    )
{
    /* 
     * instanse handler, packet ftype and ttype, parameters for size dection
     * split implementation 
     */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    unsigned int size = 0;
    int result; /*result of routine execution*/

    /* check pointers */
    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* printf("\n at RIO_PL_FC_Request function \n"); */
   
    /* check priority value */
    if (rq->prio > RIO_PL_MAX_PRIO_VALUE)
    {
        RIO_PL_Print_Message(handle, RIO_PL_ERROR_34, (unsigned long)rq->prio);
        return RIO_ERROR;
    }

    /* GDA:Bug#43 fix */
    /* if (instanse->inst_Param_Set.gda_Tx_Illegal_Packet && rq->dw_Size > 32)
        }
    }*/

    /* additional checking for split engine*/
    /* if (RIO_PL_Is_Payload(ftype, ttype) == RIO_OK)
        max_Size = RIO_PL_Get_Max_Req_Size(handle);
    else
        max_Size = RIO_PL_Get_Max_Resp_Dem_Size(handle);
    */
    /* convert size to count of dwords */
    /*  max_Size = RIO_PL_Conv_Size_To_Dw_Cnt(max_Size);*/
    /* detect if split engine is necessary */
    /*  if (rq->dw_Size > max_Size)
        need_Split = 1;
    */
    /* load request to temporary buffer and try to load it to the outbound */    
    /*  if (need_Split)
    {
        if (rq->dw_Size == 1)

        {
          if ((rq->offset + rq->byte_Num) > RIO_PL_BYTE_CNT_IN_DWORD)
            RIO_PL_GET_TEMP(instanse).byte_Num = (RIO_UCHAR_T)(RIO_PL_BYTE_CNT_IN_DWORD - rq->offset); 
          else
            RIO_PL_GET_TEMP(instanse).byte_Num = rq->byte_Num; 

          RIO_PL_GET_TEMP(instanse).byte_Offset = rq->offset;
        }
        
        RIO_PL_GET_TEMP(instanse).max_Size = max_Size;
        RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_WAIT_SPLIT;
    }
    else */

    RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;
    RIO_PL_GET_TEMP(instanse).trx_Tag = trx_Tag;

    /*
    RIO_PL_GET_TEMP(instanse).payload_Size = rq->dw_Size;
    if (RIO_PL_Is_Payload(ftype, ttype) == RIO_OK)
    {
        RIO_PL_GET_TEMP(instanse).payload_Source = rq->data;
        if (rq->data)
        {
            for (i = 0; i < rq->dw_Size; i++)
                RIO_PL_GET_TEMP(instanse).payload[i] = rq->data[i];
        }
    }*/
        
    RIO_PL_GET_TEMP(instanse).entry.additional_Fields_Valid = RIO_FALSE;
    RIO_PL_GET_TEMP(instanse).entry.prio = rq->prio;
    RIO_PL_GET_TEMP(instanse).entry.ftype = RIO_FC;
    RIO_PL_GET_TEMP(instanse).entry.ttype = 0;
    RIO_PL_GET_TEMP(instanse).entry.tranp_Info = transport_Info;
    RIO_PL_GET_TEMP(instanse).entry.tt = rq->transp_Type;
    RIO_PL_GET_TEMP(instanse).entry.size = size;
    RIO_PL_GET_TEMP(instanse).entry.soc = rq->soc;
    RIO_PL_GET_TEMP(instanse).entry.tgtdest_ID = rq->tgtdest_ID;
    RIO_PL_GET_TEMP(instanse).entry.dest_ID = rq->dest_ID;
    RIO_PL_GET_TEMP(instanse).entry.flow_ID = rq->flow_ID;
    RIO_PL_GET_TEMP(instanse).entry.x_On_Off = rq->x_On_Off;

    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}

/*************************************************************/
/*int RIO_PL_Congestion(
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag
    )
{
   
  RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
  
  printf("\n at  RIO_PL_Congestion \n");
    if (instanse == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;
          
    else
        return RIO_OK;
}
*************************************************************/
