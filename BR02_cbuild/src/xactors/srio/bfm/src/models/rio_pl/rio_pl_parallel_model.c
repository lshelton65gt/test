/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio_pl/rio_pl_parallel_model.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  RapidIO PL parallel model 
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "rio_pl_parallel_model.h"
#include "rio_pl_model_common.h"
#include "rio_pl_pllight_par_model.h"

#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_rx.h"
#include "rio_pl_tx.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"

#include "rio_32_txrx_model.h"
#include "rio_parallel_init.h"


/* 
 * External Physical Layer functions implementation
 */

/***************************************************************************
 * Function : RIO_Parallel_PL_Model_Create_Instance
 *
 * Description: Create new physical layer instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_PL_Parallel_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_MODEL_INST_PARAM_T  *param_Set
    )
{
    RIO_PL_DS_T *pl_Instanse;

    if (RIO_PL_PLLight_Parallel_Model_Create_Instance(handle, param_Set) != RIO_OK)
        return RIO_ERROR;

    pl_Instanse = (RIO_PL_DS_T*) (*handle);
    pl_Instanse->is_PLLight_Model = 0;
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PL_Parallel_Model_Get_Function_Tray
 *
 * Description: export physical layer instance function tray
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Parallel_Model_Get_Function_Tray(
    RIO_PL_PARALLEL_MODEL_FTRAY_T *ftray
    )
{
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->rio_PL_GSM_Request           = RIO_PL_GSM_Request;
    ftray->rio_PL_IO_Request            = RIO_PL_IO_Request;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ftray->rio_PL_DS_Request            = RIO_PL_DS_Request;  
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    ftray->rio_PL_FC_Request            = RIO_PL_FC_Request;
    ftray->rio_PL_Congestion            = RIO_PL_Congestion;
    /* GDA: Bug#125 */

    ftray->rio_PL_Message_Request       = RIO_PL_Message_Request;
    ftray->rio_PL_Doorbell_Request      = RIO_PL_Doorbell_Request;
    ftray->rio_PL_Configuration_Request = RIO_PL_Configuration_Request;
    ftray->rio_PL_Send_Response         = RIO_PL_Send_Response;
    ftray->rio_PL_Ack_Remote_Req        = RIO_PL_Ack_Remote_Req;
    ftray->rio_PL_Set_Config_Reg        = RIO_PL_Set_Config_Reg;
    ftray->rio_PL_Get_Config_Reg        = RIO_PL_Get_Config_Reg;
    ftray->rio_PL_Packet_Struct_Request = RIO_PL_Packet_Struct_Request;

    return RIO_PL_PLLight_Parallel_Model_Get_Function_Tray(ftray);
}

/***************************************************************************
 * Function : RIO_PL_Parallel_Model_Bind_Instance
 *
 * Description: bind physical layer instance 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PL_Parallel_Model_Bind_Instance(
    RIO_HANDLE_T                          handle,
    RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T *ctray   
    )
{
    /* instanse handle */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    /*
     * check pointers and callbacks if OK load callbacks
     */
    if (instanse == NULL || ctray == NULL ||
        ctray->rio_PL_Set_Pins == NULL ||
        ctray->rio_PL_Ack_Request == NULL ||
        ctray->rio_PL_Ack_Response == NULL ||
        ctray->rio_PL_Get_Req_Data_Hw == NULL ||/* GDA: Bug#124 - Support for Data Streaming packet */
        ctray->rio_PL_Get_Req_Data == NULL ||
        ctray->rio_PL_Get_Resp_Data == NULL ||
        ctray->rio_PL_Remote_Request == NULL ||
        ctray->rio_PL_Remote_Response == NULL ||
        ctray->rio_PL_Req_Time_Out == NULL ||
        ctray->rio_User_Msg == NULL ||
        ctray->extend_Reg_Read == NULL ||
        ctray->extend_Reg_Write == NULL 
        )
        return RIO_ERROR;

    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;


    /* store callbacks tray into the layer's structure */
    instanse->ext_Interfaces.pl_Interface_Context   = ctray->pl_Interface_Context; 
                                                                            
    instanse->ext_Interfaces.rio_User_Msg           = ctray->rio_User_Msg;         
    instanse->ext_Interfaces.rio_Msg                = ctray->rio_Msg;              
                                                                            
	instanse->ext_Interfaces.rio_PL_Ack_Request     = ctray->rio_PL_Ack_Request;    
    instanse->ext_Interfaces.rio_PL_Get_Req_Data    = ctray->rio_PL_Get_Req_Data;  

    /* GDA: Bug#124 - Support for Data Streaming packet */
    instanse->ext_Interfaces.rio_PL_Get_Req_Data_Hw    = ctray->rio_PL_Get_Req_Data_Hw;  
    /* GDA: Bug#124 */

    instanse->ext_Interfaces.rio_PL_Ack_Response    = ctray->rio_PL_Ack_Response;  
    instanse->ext_Interfaces.rio_PL_Get_Resp_Data   = ctray->rio_PL_Get_Resp_Data; 
    instanse->ext_Interfaces.rio_PL_Req_Time_Out    = ctray->rio_PL_Req_Time_Out;  
    instanse->ext_Interfaces.rio_PL_Remote_Request  = ctray->rio_PL_Remote_Request;
    instanse->ext_Interfaces.rio_PL_Remote_Response = ctray->rio_PL_Remote_Response;
    instanse->ext_Interfaces.extend_Reg_Read        = ctray->extend_Reg_Read;      
    instanse->ext_Interfaces.extend_Reg_Write       = ctray->extend_Reg_Write;     
    instanse->ext_Interfaces.extend_Reg_Context     = ctray->extend_Reg_Context;   
    instanse->ext_Interfaces.rio_Request_Received   = ctray->rio_Request_Received; 
    instanse->ext_Interfaces.rio_Response_Received  = ctray->rio_Response_Received;
    instanse->ext_Interfaces.rio_Packet_To_Send     = ctray->rio_Packet_To_Send;   
    instanse->ext_Interfaces.rio_Hooks_Context      = ctray->rio_Hooks_Context;      

    /* GDA: Bug#125 - Support for Flow control packet */
    instanse->ext_Interfaces.rio_PL_FC_To_Send     = ctray->rio_PL_FC_To_Send;
    /* GDA: Bug#125 */

    /* copy parallel specific callbacks */
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_PL_Set_Pins        
        = ctray->rio_PL_Set_Pins;       
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->lpep_Interface_Context 
        = ctray->lpep_Interface_Context;

    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Granule_Received   
        = ctray->rio_Granule_Received; 
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Symbol_To_Send     
        = ctray->rio_Symbol_To_Send;   
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Hooks_Context      
        = ctray->rio_Hooks_Context;      

    /* mark the instance as the bound one */
    instanse->was_Bound = RIO_TRUE;

    return RIO_OK;
}

/****************************************************************************/
