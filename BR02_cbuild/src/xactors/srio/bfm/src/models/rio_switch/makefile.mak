#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio_switch/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds switch model shared library
#

include ../../../verilog_env/demo/debug.inc

CC              =   gcc
TARGET          =   librio_switch.so

DEBUG		    =   -DNDEBUG

DEFINES         =   $(_DEBUG) 

LIBS            =   -lc
LIBS_AIX	=   -lc
LIBS_LIN	=   -lc

INCLUDES        =   -I./include -I../../interfaces/common -I../../interfaces/serial_interfaces -I../../interfaces/parallel_interfaces\
			-I../../switch/include -I../../tl/include -I../../pl/include -I../rio_pl/include -I../../txrx/include\
			-I../../parallel_init/include -I../../pcs_pma/include -I../../pcs_pma_adapter/include

CFLAGS          =   -c $(prof) $(DEFINES) -elf -ansi -pedantic -Wall
LFLAGS          =   $(prof) -G -s -B symbolic
LFLAGS_AIX	=   $(prof) -G -s -bE:rio_switch.exp
LFLAGS_LINUX    =   $(prof) -G -s -Bsymbolic
FLEXLM_LFLAGS   =   -lsocket

OBJS            =   ../../switch/switch.o

OBJS_P          =   ../../switch/switch_parallel.o

OBJS_S          =   ../../switch/switch_serial.o

###############################################################################
# make default target
all :   $(TARGET)

# make target
$(TARGET) :	
		cd ../../switch; make -f makefile.mak switch
		ld  $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

all_flexlm :	
		cd ../../switch; make -f makefile.mak all_flexlm
		ld  $(LFLAGS) $(FLEXLM_LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)
		#$(FLEXLM_PATH)/sun4_u5/lmstrip -r $(TARGET)		

sun_par :
		cd ../../switch; make -f makefile.mak parallel_switch
		ld  $(LFLAGS) $(OBJS_P) $(LIBS) -o $(TARGET)

sun_serial :
		cd ../../switch; make -f makefile.mak serial_switch
		ld  $(LFLAGS) $(FLEXLM_LFLAGS) $(OBJS_S) $(LIBS) -o $(TARGET)

aix_all:	
		cd ../../switch; make -f makefile.mak switch
		ld  $(LFLAGS_AIX) $(OBJS) $(LIBS) -o $(TARGET)

aix_par:	
		cd ../../switch; make -f makefile.mak parallel_switch
		ld  $(LFLAGS_AIX) $(OBJS_P) $(LIBS) -o $(TARGET)

linux_all:	
		cd ../../switch; make -f makefile.mak switch.o
		ld  $(LFLAGS_LINUX) $(OBJS) $(LIBS) -o $(TARGET)

linux_par:	
		cd ../../switch; make -f makefile.mak parallel_switch
		ld  $(LFLAGS_LINUX) $(OBJS_P) $(LIBS) -o $(TARGET)

#clean data
clean :		
		rm -f *.o
		rm -f $(TARGET)
		cd ../../switch; make -f makefile.mak clean


###############################################################################

