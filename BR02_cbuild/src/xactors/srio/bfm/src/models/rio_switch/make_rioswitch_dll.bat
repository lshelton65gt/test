rem ########################################################################################################
rem 
rem       COPYRIGHT 2002 MOTOROLA, ALL RIGHTS RESERVED
rem 
rem       The code is the property of Motorola St.Petersburg Software Development
rem       and is Motorola Confidential Proprietary Information.
rem 
rem       The copyright notice above does not evidence any
rem       actual or intended publication of such source code.
rem 
rem Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio_switch/make_rioswitch_dll.bat,v $
rem Author:       $Author: knutson $
rem Locker:       $Locker:  $
rem State:        $State: Exp $
rem Revision:     $Revision: 1.1 $
rem 
rem History:      Use the CVS command log to display revision history
rem               information.
rem 
rem Description:  Batch file for building of RapidIO Switch model under Windows NT.
rem 
rem Notes:
rem
rem ########################################################################################################


setlocal

rem
rem
rem
rem compiler specific

set COMPILER_PATH=c:\apps\vc6.0\bin
set STANDARD_LIB=C:\Apps\VC6.0\Lib
set STANDARD_INC=c:\apps\vc6.0\include
set path=c:\apps\vc6\msdev98\bin;%COMPILER_PATH%;%path%

rem path to sources
set SRC_PATH=%RAPIDIO_PATH_1_4%\src

%COMPILER_PATH%\nmake -f makefile_nt.mak LIBPATH=%STANDARD_LIB% STANDARD_INC=%STANDARD_INC% RAPIDIO_PATH=%SRC_PATH% CFG="rio_switch - Win32 Release"  

