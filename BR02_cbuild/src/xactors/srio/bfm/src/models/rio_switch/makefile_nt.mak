#******************************************************************************
#*
#* Copyright Motorola, Inc. 2002-2004
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio_switch/makefile_nt.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************

!IF "$(CFG)" == ""
CFG=rio_switch - Win32 Debug
!MESSAGE No configuration specified. Defaulting to rio_switch - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "rio_switch - Win32 Release" && "$(CFG)" != "rio_switch - Win32 Debug" && "$(CFG)" != "rio_switch_p - Win32 Release" && "$(CFG)" != "rio_switch_p - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "rio_switch.mak" CFG="rio_switch - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "rio_switch - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "rio_switch - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "rio_switch_p - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "rio_switch_p - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

INCLUDE=-I"$(STANDARD_INC)" -I"$(RAPIDIO_PATH)\interfaces\common" -I"$(RAPIDIO_PATH)\interfaces\parallel_interfaces" -I"$(RAPIDIO_PATH)\interfaces\serial_interfaces" -I"$(RAPIDIO_PATH)\ll\include" -I"$(RAPIDIO_PATH)\pl\include" -I"$(RAPIDIO_PATH)\tl\include" -I"$(RAPIDIO_PATH)\switch\include" -I"$(RAPIDIO_PATH)\txrx\include" -I"$(RAPIDIO_PATH)\parallel_init\include" -I"$(RAPIDIO_PATH)\pcs_pma\include" -I"$(RAPIDIO_PATH)\pcs_pma_adapter\include"

!IF  "$(CFG)" == "rio_switch - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\rio_switch.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_switch_errors.obj"
	-@erase "$(INTDIR)\rio_switch_model_common.obj"
	-@erase "$(INTDIR)\rio_switch_parallel_model.obj"
	-@erase "$(INTDIR)\rio_switch_registers.obj"
	-@erase "$(INTDIR)\rio_switch_rx.obj"
	-@erase "$(INTDIR)\rio_switch_serial_model.obj"
	-@erase "$(INTDIR)\rio_switch_tx.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_adapter.obj"
        -@erase "$(INTDIR)\rio_c2char.obj"
        -@erase "$(INTDIR)\rio_char2c.obj"
        -@erase "$(INTDIR)\rio_pcs_pma.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp_cl.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp_sb.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_pm.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_pm_align.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_pm_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_sync.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\rio_switch.dll"
	-@erase "$(OUTDIR)\rio_switch.exp"
	-@erase "$(OUTDIR)\rio_switch.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=$(INCLUDE) /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_SWITCH_EXPORTS" /Fp"$(INTDIR)\rio_switch.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_switch.bsc" 
BSC32_SBRS= \

LINK32=link.exe
LINK32_FLAGS=-libpath:"$(LIBPATH)" kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\rio_switch.pdb" /machine:I386 /def:".\rio_switch.def" /out:"$(OUTDIR)\rio_switch.dll" /implib:"$(OUTDIR)\rio_switch.lib" 
DEF_FILE= \
	".\rio_switch.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_switch_errors.obj" \
	"$(INTDIR)\rio_switch_model_common.obj" \
	"$(INTDIR)\rio_switch_parallel_model.obj" \
	"$(INTDIR)\rio_switch_registers.obj" \
	"$(INTDIR)\rio_switch_rx.obj" \
	"$(INTDIR)\rio_switch_serial_model.obj" \
	"$(INTDIR)\rio_switch_tx.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" \
	"$(INTDIR)\rio_pcs_pma_adapter.obj" \
        "$(INTDIR)\rio_c2char.obj" \
        "$(INTDIR)\rio_char2c.obj" \
        "$(INTDIR)\rio_pcs_pma.obj" \
        "$(INTDIR)\rio_pcs_pma_dp.obj" \
        "$(INTDIR)\rio_pcs_pma_dp_cl.obj" \
        "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj" \
        "$(INTDIR)\rio_pcs_pma_dp_sb.obj" \
        "$(INTDIR)\rio_pcs_pma_pm.obj" \
        "$(INTDIR)\rio_pcs_pma_pm_align.obj" \
        "$(INTDIR)\rio_pcs_pma_pm_init.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_sync.obj"

"$(OUTDIR)\rio_switch.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE="$(InputPath)"
PostBuild_Desc=copy switch libraries
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\rio_switch.dll"
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "rio_switch - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\rio_switch.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_switch_errors.obj"
	-@erase "$(INTDIR)\rio_switch_model_common.obj"
	-@erase "$(INTDIR)\rio_switch_parallel_model.obj"
	-@erase "$(INTDIR)\rio_switch_registers.obj"
	-@erase "$(INTDIR)\rio_switch_rx.obj"
	-@erase "$(INTDIR)\rio_switch_serial_model.obj"
	-@erase "$(INTDIR)\rio_switch_tx.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_adapter.obj"
        -@erase "$(INTDIR)\rio_c2char.obj"
        -@erase "$(INTDIR)\rio_char2c.obj"
        -@erase "$(INTDIR)\rio_pcs_pma.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp_cl.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_dp_sb.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_pm.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_pm_align.obj"
        -@erase "$(INTDIR)\rio_pcs_pma_pm_init.obj"
	-@erase "$(INTDIR)\rio_pcs_pma_pm_sync.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\rio_switch.dll"
	-@erase "$(OUTDIR)\rio_switch.exp"
	-@erase "$(OUTDIR)\rio_switch.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=$(INCLUDE) /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_SWITCH_EXPORTS" /Fp"$(INTDIR)\rio_switch.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ  /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_switch.bsc" 
BSC32_SBRS= \

LINK32=link.exe
LINK32_FLAGS=-libpath:"$(LIBPATH)" kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\rio_switch.pdb" /debug /machine:I386 /def:".\rio_switch.def" /out:"$(OUTDIR)\rio_switch.dll" /implib:"$(OUTDIR)\rio_switch.lib" /pdbtype:sept 
DEF_FILE= \
	".\rio_switch.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_switch_errors.obj" \
	"$(INTDIR)\rio_switch_model_common.obj" \
	"$(INTDIR)\rio_switch_parallel_model.obj" \
	"$(INTDIR)\rio_switch_registers.obj" \
	"$(INTDIR)\rio_switch_rx.obj" \
	"$(INTDIR)\rio_switch_serial_model.obj" \
	"$(INTDIR)\rio_switch_tx.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" \
	"$(INTDIR)\rio_pcs_pma_adapter.obj" \
        "$(INTDIR)\rio_c2char.obj" \
        "$(INTDIR)\rio_char2c.obj" \
        "$(INTDIR)\rio_pcs_pma.obj" \
        "$(INTDIR)\rio_pcs_pma_dp.obj" \
        "$(INTDIR)\rio_pcs_pma_dp_cl.obj" \
        "$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj" \
        "$(INTDIR)\rio_pcs_pma_dp_sb.obj" \
        "$(INTDIR)\rio_pcs_pma_pm.obj" \
        "$(INTDIR)\rio_pcs_pma_pm_align.obj" \
        "$(INTDIR)\rio_pcs_pma_pm_init.obj" \
	"$(INTDIR)\rio_pcs_pma_pm_sync.obj"

"$(OUTDIR)\rio_switch.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE="$(InputPath)"
PostBuild_Desc=copy switch libraries
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\rio_switch.dll"
   copy release\rio_switch.dll ..\..\..\models\rio_switch
	copy release\rio_switch.lib ..\..\..\models\rio_switch
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "rio_switch_p - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\rio_switch.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_switch_errors.obj"
	-@erase "$(INTDIR)\rio_switch_model_common.obj"
	-@erase "$(INTDIR)\rio_switch_parallel_model.obj"
	-@erase "$(INTDIR)\rio_switch_registers.obj"
	-@erase "$(INTDIR)\rio_switch_rx.obj"
	-@erase "$(INTDIR)\rio_switch_tx.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\rio_switch.dll"
	-@erase "$(OUTDIR)\rio_switch.exp"
	-@erase "$(OUTDIR)\rio_switch.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=$(INCLUDE) /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_SWITCH_EXPORTS" /Fp"$(INTDIR)\rio_switch.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_switch.bsc" 
BSC32_SBRS= \

LINK32=link.exe
LINK32_FLAGS=-libpath:"$(LIBPATH)" kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\rio_switch.pdb" /machine:I386 /def:".\rio_switch_p.def" /out:"$(OUTDIR)\rio_switch.dll" /implib:"$(OUTDIR)\rio_switch.lib" 
DEF_FILE= \
	".\rio_switch_p.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_switch_errors.obj" \
	"$(INTDIR)\rio_switch_model_common.obj" \
	"$(INTDIR)\rio_switch_parallel_model.obj" \
	"$(INTDIR)\rio_switch_registers.obj" \
	"$(INTDIR)\rio_switch_rx.obj" \
	"$(INTDIR)\rio_switch_tx.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" 

"$(OUTDIR)\rio_switch.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE="$(InputPath)"
PostBuild_Desc=copy switch libraries
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\rio_switch.dll"
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "rio_switch_p - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\rio_switch.dll"


CLEAN :
	-@erase "$(INTDIR)\rio_switch_errors.obj"
	-@erase "$(INTDIR)\rio_switch_model_common.obj"
	-@erase "$(INTDIR)\rio_switch_parallel_model.obj"
	-@erase "$(INTDIR)\rio_switch_registers.obj"
	-@erase "$(INTDIR)\rio_switch_rx.obj"
	-@erase "$(INTDIR)\rio_switch_tx.obj"
	-@erase "$(INTDIR)\rio_parallel_init.obj"
	-@erase "$(INTDIR)\rio_32_txrx_model.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\rio_switch.dll"
	-@erase "$(OUTDIR)\rio_switch.exp"
	-@erase "$(OUTDIR)\rio_switch.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=$(INCLUDE) /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RIO_SWITCH_EXPORTS" /Fp"$(INTDIR)\rio_switch.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ  /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\rio_switch.bsc" 
BSC32_SBRS= \

LINK32=link.exe
LINK32_FLAGS=-libpath:"$(LIBPATH)" kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\rio_switch.pdb" /debug /machine:I386 /def:".\rio_switch_p.def" /out:"$(OUTDIR)\rio_switch.dll" /implib:"$(OUTDIR)\rio_switch.lib" /pdbtype:sept 
DEF_FILE= \
	".\rio_switch_p.def"
LINK32_OBJS= \
	"$(INTDIR)\rio_switch_errors.obj" \
	"$(INTDIR)\rio_switch_model_common.obj" \
	"$(INTDIR)\rio_switch_parallel_model.obj" \
	"$(INTDIR)\rio_switch_registers.obj" \
	"$(INTDIR)\rio_switch_rx.obj" \
	"$(INTDIR)\rio_switch_tx.obj" \
	"$(INTDIR)\rio_parallel_init.obj" \
	"$(INTDIR)\rio_32_txrx_model.obj" 

"$(OUTDIR)\rio_switch.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE="$(InputPath)"
PostBuild_Desc=copy switch libraries
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\rio_switch.dll"
   copy release\rio_switch.dll ..\..\..\models\rio_switch
	copy release\rio_switch.lib ..\..\..\models\rio_switch
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("rio_switch.dep")
!INCLUDE "rio_switch.dep"
!ELSE 
!MESSAGE Warning: cannot find "rio_switch.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "rio_switch - Win32 Release" || "$(CFG)" == "rio_switch - Win32 Debug" || "$(CFG)" == "rio_switch_p - Win32 Release" || "$(CFG)" == "rio_switch_p - Win32 Debug"

SOURCE=..\..\switch\rio_switch_errors.c

"$(INTDIR)\rio_switch_errors.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\switch\rio_switch_model_common.c

"$(INTDIR)\rio_switch_model_common.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\switch\rio_switch_parallel_model.c

"$(INTDIR)\rio_switch_parallel_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\switch\rio_switch_registers.c

"$(INTDIR)\rio_switch_registers.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\switch\rio_switch_rx.c

"$(INTDIR)\rio_switch_rx.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\switch\rio_switch_serial_model.c

"$(INTDIR)\rio_switch_serial_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\switch\rio_switch_tx.c

"$(INTDIR)\rio_switch_tx.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\parallel_init\rio_parallel_init.c

"$(INTDIR)\rio_parallel_init.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\txrx\rio_32_txrx_model.c

"$(INTDIR)\rio_32_txrx_model.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma_adapter\rio_pcs_pma_adapter.c

"$(INTDIR)\rio_pcs_pma_adapter.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_c2char.c

"$(INTDIR)\rio_c2char.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_char2c.c

"$(INTDIR)\rio_char2c.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma.c

"$(INTDIR)\rio_pcs_pma.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_dp.c

"$(INTDIR)\rio_pcs_pma_dp.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_dp_cl.c

"$(INTDIR)\rio_pcs_pma_dp_cl.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_dp_pins_driver.c

"$(INTDIR)\rio_pcs_pma_dp_pins_driver.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_dp_sb.c

"$(INTDIR)\rio_pcs_pma_dp_sb.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_pm.c

"$(INTDIR)\rio_pcs_pma_pm.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_pm_align.c

"$(INTDIR)\rio_pcs_pma_pm_align.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_pm_init.c

"$(INTDIR)\rio_pcs_pma_pm_init.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\pcs_pma\rio_pcs_pma_pm_sync.c

"$(INTDIR)\rio_pcs_pma_pm_sync.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 





