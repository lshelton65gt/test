/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio/rio_model_common.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  
*
* Notes:                
*
******************************************************************************/

#include "rio_model_common.h"
#include "rio_tl_entry.h"
#include "rio_ll_entry.h"

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>


/***************************************************************************
 *
 *
 *     Common function interface implementation
 *
 *
 */


/***************************************************************************
 * Function : RIO_Model_Print_Version
 *
 * Description: prints version of the model
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_Model_Print_Version(
    RIO_HANDLE_T             handle
)
{
    RIO_MODEL_DS_T *model_Data;
    char           buffer[255];

    /* check parameters */
    if( handle == NULL )
        return RIO_ERROR;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    if ( model_Data->message_Callback == NULL )
        return RIO_OK;

    /* compose version info */
    sprintf( buffer, "RIO PE Model %s\n", RIO_MODEL_VERSION );
    
    /* invoke callback */
    model_Data->message_Callback(
        model_Data->msg_Context,
        buffer
    );
    
    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_Model_GSM_Request
 *
 * Description:  simple wrapper around LL's GSM request function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_GSM_Request(
    RIO_HANDLE_T    handle, 
    RIO_GSM_TTYPE_T ttype,
    RIO_GSM_REQ_T   *rq, 
    RIO_TAG_T       trx_Tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was bound already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_GSM_Request( 
        model_Data->ll_Handle, 
        ttype,
        rq,
        trx_Tag
    );    
}


/***************************************************************************
 * Function :    RIO_Model_IO_Request
 *
 * Description:  simple wrapper around LL's IO request function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_IO_Request( 
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag         
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_IO_Request( 
        model_Data->ll_Handle, 
        rq,
        trx_Tag
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Message_Request
 *
 * Description:  simple wrapper around LL's Message request function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Message_Request(
    RIO_HANDLE_T      handle, 
    RIO_MESSAGE_REQ_T *rq, 
    RIO_TAG_T         trx_Tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_Message_Request( 
        model_Data->ll_Handle, 
        rq,
        trx_Tag
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Doorbell_Request
 *
 * Description:  simple wrapper around LL's Doorbell request function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Doorbell_Request(
    RIO_HANDLE_T       handle, 
    RIO_DOORBELL_REQ_T *rq, 
    RIO_TAG_T          trx_Tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_Doorbell_Request( 
        model_Data->ll_Handle, 
        rq,
        trx_Tag
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Config_Request
 *
 * Description:  simple wrapper around LL's Configuration request function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Config_Request(
    RIO_HANDLE_T     handle, 
    RIO_CONFIG_REQ_T *rq, 
    RIO_TAG_T        trx_Tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_Conf_Request( 
        model_Data->ll_Handle, 
        rq,
        trx_Tag
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Snoop_Response
 *
 * Description:  simple wrapper around LL's snoop response function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Snoop_Response(
    RIO_CONTEXT_T      handle, 
    RIO_SNOOP_RESULT_T snoop_Result, 
    RIO_DW_T           *data
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_Snoop_Response( 
        model_Data->ll_Handle, 
        snoop_Result,
        data
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Set_Memory_Data
 *
 * Description:  simple wrapper around LL's set memory data function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Set_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *dw
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_Set_Mem_Data( 
        model_Data->ll_Handle, 
        dw
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Get_Memory_Data
 *
 * Description:  simple wrapper around LL's get memory data function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Get_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *dw
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->ll_FTray.rio_Get_Mem_Data( 
        model_Data->ll_Handle, 
        dw
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Set_Config_Reg
 *
 * Description:  simple wrapper around PL's set configuration register 
 *               function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Set_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,  
    unsigned long     data
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( (model_Data->was_Binded != RIO_TRUE && model_Data->was_PL_Binded != RIO_TRUE) ||
        model_Data->instance_Mode == RIO_MODEL_NOT_INITIALIZED 
        )
        return RIO_ERROR;
    
    /* invoke function */
    return model_Data->pl_Common_FTray.rio_PL_Set_Config_Reg( 
        model_Data->pl_Handle, 
        config_Offset,
        data
    );    
}

/***************************************************************************
 * Function :    RIO_Model_Get_Config_Reg
 *
 * Description:  simple wrapper around PL's get configuration register 
 *               function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Get_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   
    unsigned long     *data
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( (model_Data->was_Binded != RIO_TRUE && model_Data->was_PL_Binded != RIO_TRUE) ||
        model_Data->instance_Mode == RIO_MODEL_NOT_INITIALIZED 
        )
        return RIO_ERROR;
    
    /* invoke function */
    return model_Data->pl_Common_FTray.rio_PL_Get_Config_Reg( 
        model_Data->pl_Handle, 
        config_Offset,
        data
    );    
}


/***************************************************************************
 * Function :    RIO_Model_Start_Reset
 *
 * Description:  this function start initialization for all model layers
 *               afer this before initialization
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Start_Reset(
    RIO_HANDLE_T handle
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }


    model_Data->instance_Mode = RIO_MODEL_NOT_INITIALIZED;

    /* reset layers using their functions */
    if ( model_Data->ll_FTray.rio_Start_Reset( model_Data->ll_Handle ) != RIO_OK )
        return RIO_ERROR;

    if ( model_Data->tl_FTray.rio_Start_Reset( model_Data->tl_Handle ) != RIO_OK )
        return RIO_ERROR;

    if ( model_Data->pl_Common_FTray.rio_PL_Model_Start_Reset( model_Data->pl_Handle ) != RIO_OK )
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_Model_Delete_Instance
 *
 * Description:  deletes the instance of the model 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Delete_Instance(
    RIO_HANDLE_T      handle
)
{
    RIO_MODEL_DS_T  *model_Data;

    model_Data = (RIO_MODEL_DS_T*)handle;

    if( model_Data == NULL )
        return RIO_ERROR;

    model_Data->pl_Common_FTray.rio_PL_Delete_Instance( model_Data->pl_Handle );
    model_Data->tl_FTray.rio_TL_Delete_Instance( model_Data->tl_Handle );
    model_Data->ll_FTray.rio_LL_Delete_Instance( model_Data->ll_Handle );
    
    free( model_Data );
    
    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_Model_Message_Callback
 *
 * Description:  simple stub to invoke environment error message callback
 *
 * Returns:      RIO_OK or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Message_Callback(
    RIO_CONTEXT_T  context,
    char *         error_Msg 
)
{
    RIO_MODEL_DS_T *model_Data;
    char           buffer[255];

    /* check parameters */
    if( context == NULL )
        return RIO_ERROR;
    else
        model_Data = (RIO_MODEL_DS_T*)context;

    if ( model_Data->message_Callback == NULL )
        return RIO_OK;

    /* compose error message */
    sprintf( buffer, "RIO Model : %s", error_Msg );    
    
    /* invoke callback */
    model_Data->message_Callback(
        model_Data->msg_Context,
        buffer
    );
    
    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_Model_Packet_Struct_Request
 *
 * Description:  Wrapper around LL packet struct request
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Packet_Struct_Request(
    RIO_HANDLE_T       handle, 
    RIO_MODEL_PACKET_STRUCT_RQ_T *rq, 
    RIO_TAG_T          trx_Tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }
    
    /* invoke function */
    return model_Data->pl_Common_FTray.rio_PL_Packet_Struct_Request ( 
        model_Data->pl_Handle, 
        rq,
        trx_Tag
    );
}

/***************************************************************************
 * PL API
****************************************************************************/

/***************************************************************************
 * Function :    RIO_Model_Clock_Edge
 *
 * Description:  simple wrapper around PL's clock edge function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Model_Clock_Edge(
    RIO_HANDLE_T handle /* instance handle */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( (model_Data->was_Binded != RIO_TRUE && model_Data->was_PL_Binded != RIO_TRUE) ||
        model_Data->instance_Mode == RIO_MODEL_NOT_INITIALIZED 
        )
        return RIO_ERROR;
    
    /* invoke function */
    return model_Data->pl_Common_FTray.rio_PL_Clock_Edge( 
        model_Data->pl_Handle
    );    
}

/***************************************************************************
 * Function :    RIO_Model_PL_Model_Start_Reset
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Model_Start_Reset(RIO_HANDLE_T handle)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    model_Data->instance_Mode = RIO_MODEL_NOT_INITIALIZED;

    /* reset layers using their functions */
    if ( model_Data->ll_FTray.rio_Start_Reset( model_Data->ll_Handle ) != RIO_OK )
        return RIO_ERROR;

    if ( model_Data->tl_FTray.rio_Start_Reset( model_Data->tl_Handle ) != RIO_OK )
        return RIO_ERROR;

    if ( model_Data->pl_Common_FTray.rio_PL_Model_Start_Reset( model_Data->pl_Handle ) != RIO_OK )
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_Model_PL_GSM_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_GSM_Request(
    RIO_HANDLE_T       handle,
    RIO_PL_GSM_TTYPE_T ttype,          /* requested transaction type  */
    RIO_GSM_REQ_T      *rq,
    RIO_UCHAR_T        sec_ID,            /* ignored for packet types other than 1 */
    RIO_UCHAR_T        sec_TID,
    RIO_TAG_T          trx_Tag,           /* user-assigned tag of the request */
    RIO_TR_INFO_T      transport_Info[],
    RIO_UCHAR_T        multicast_Size     /* count of recepients */
    )
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_GSM_Request(
        model_Data->pl_Handle,
        ttype,          /* requested transaction type  */
        rq,
        sec_ID,            /* ignored for packet types other than 1 */
        sec_TID,
        trx_Tag,           /* user-assigned tag of the request */
        transport_Info,
        multicast_Size     /* count of recepients */
    );
    
}

/***************************************************************************
 * Function :    RIO_Model_PL_IO_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_IO_Request(
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
    RIO_TR_INFO_T  transport_Info
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_IO_Request(
        model_Data->pl_Handle,    
        rq,
        trx_Tag,             /* user-assigned tag of the request */
        transport_Info
        );
}

 
/***************************************************************************
 * Function :    RIO_Model_PL_Message_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Message_Request(
    RIO_HANDLE_T       handle,
    RIO_MESSAGE_REQ_T  *rq,
    RIO_TAG_T          trx_Tag             /* user-assigned tag of the request */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_Message_Request(
        model_Data->pl_Handle,
        rq,
        trx_Tag             /* user-assigned tag of the request */
        );         
}


/***************************************************************************
 * Function :    RIO_Model_PL_Doorbell_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Doorbell_Request(
    RIO_HANDLE_T        handle,
    RIO_DOORBELL_REQ_T  *rq,
    RIO_TAG_T           trx_Tag             /* user-assigned tag of the request */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_Doorbell_Request(
        model_Data->pl_Handle,
        rq,
        trx_Tag             /* user-assigned tag of the request */
        );
}


/***************************************************************************
 * Function :    RIO_Model_PL_Configuration_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Configuration_Request(
    RIO_HANDLE_T      handle,
    RIO_CONFIG_REQ_T  *rq,
    RIO_TAG_T         trx_Tag          /* user-assigned tag of the request */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }
    
    return model_Data->pl_Common_FTray.rio_PL_Configuration_Request(
        model_Data->pl_Handle,
        rq,
        trx_Tag          /* user-assigned tag of the request */
        );    
}

/***************************************************************************
 * Function :    RIO_Model_PL_Packet_Struct_Request
 *
 * Description:  simple wrapper around PL's RIO_PL_Packet_Struct_Request function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Packet_Struct_Request(
    RIO_HANDLE_T      handle,
    RIO_MODEL_PACKET_STRUCT_RQ_T  *rq,
    RIO_TAG_T         trx_Tag          /* user-assigned tag of the request */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }
    
    return model_Data->pl_Common_FTray.rio_PL_Packet_Struct_Request(
        model_Data->pl_Handle,
        rq,
        trx_Tag          /* user-assigned tag of the request */
        );    
}

/***************************************************************************
 * Function :    RIO_Model_PL_Send_Response
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Send_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_Send_Response(
        model_Data->pl_Handle,
        tag, 
        resp
        );
}


/***************************************************************************
 * Function :    RIO_Model_PL_Ack_Remote_Req
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Ack_Remote_Req(
    RIO_HANDLE_T handle, 
    RIO_TAG_T    tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_Ack_Remote_Req(
        model_Data->pl_Handle,
        tag
        );        
}
                 
/***************************************************************************
 * PL callbacks realized in PE model and routed to either TL or environment
 ****************************************************************************/


/***************************************************************************
 * Function :    RIO_Model_PL_Ack_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Ack_Request(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Ack_Request(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Ack_Request(
            model_Data->tl_Handle,
            tag
        );
    }
    else
        return RIO_ERROR;
}


/***************************************************************************
 * Function :    RIO_Model_PL_Get_Req_Data
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Get_Req_Data(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Get_Req_Data(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag,
            offset,
            cnt,
            data
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Get_Req_Data(
            model_Data->tl_Handle,
            tag,
            offset,
            cnt,
            data
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_PL_Ack_Response
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Ack_Response(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Ack_Response(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Ack_Response(
            model_Data->tl_Handle,
            tag
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_PL_Get_Resp_Data
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Get_Resp_Data(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Get_Resp_Data(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag, 
            offset, 
            cnt, 
            data
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Get_Resp_Data(
            model_Data->tl_Handle,
            tag, 
            offset, 
            cnt, 
            data
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_PL_Req_Time_Out
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Req_Time_Out(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Req_Time_Out(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Req_Time_Out(
            model_Data->tl_Handle,
            tag
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_PL_Remote_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Remote_Request(
    RIO_CONTEXT_T        context, 
    RIO_TAG_T            tag, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_TR_INFO_T        transport_Info
)
{                                       
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Remote_Request(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag,
            req,
            transport_Info
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Remote_Request(
            model_Data->tl_Handle,
            tag,
            req,
            transport_Info
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_PL_Remote_Response
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Remote_Response(
    RIO_CONTEXT_T  context, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Remote_Response(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag,
            resp
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Remote_Responce(
            model_Data->tl_Handle,
            tag,
            resp
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_PL_Extend_Reg_Read
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Extend_Reg_Read(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,      /* double-word aligned offset inside configuration space */
    unsigned long    *data
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.extend_Reg_Read(
            model_Data->user_PL_Common_Ctray.extend_Reg_Context,
            config_Offset,
            data
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Remote_Conf_Read(
            model_Data->tl_Handle,
            config_Offset,
            data
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_PL_Extend_Reg_Write
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Extend_Reg_Write(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,       /* double-word aligned offset inside configuration space */
    unsigned long    data
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.extend_Reg_Write(
            model_Data->user_PL_Common_Ctray.extend_Reg_Context,
            config_Offset,
            data
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Remote_Conf_Write(
            model_Data->tl_Handle,
            config_Offset,
            data
        );
        
    }
    else
        return RIO_ERROR;

}


/***************************************************************************
 * Function :    RIO_Model_Symbol_To_Send
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->user_PL_Common_Ctray.rio_Symbol_To_Send != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_Symbol_To_Send(
            model_Data->user_PL_Common_Ctray.rio_Hooks_Context,
            symbol_Buffer,
            buffer_Size
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_Model_Request_Received
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Request_Received(
    RIO_CONTEXT_T         context,
    RIO_REMOTE_REQUEST_T  *packet_Struct      /* struct, containing packets's fields */
)
{ 

    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->user_PL_Common_Ctray.rio_Request_Received != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_Request_Received(
            model_Data->user_PL_Common_Ctray.rio_Hooks_Context,
            packet_Struct
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_Model_Response_Received
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Response_Received(
    RIO_CONTEXT_T         context,
    RIO_RESPONSE_T        *packet_Struct      /* struct, containing response's fields */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->user_PL_Common_Ctray.rio_Response_Received != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_Response_Received(
            model_Data->user_PL_Common_Ctray.rio_Hooks_Context,
            packet_Struct
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_Model_Packet_To_Send
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Packet_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int          buffer_Size,        /*Size always = 276*/  
    RIO_UCHAR_T*          packet_Length_In_Granules, /* maximum = 276/4 = 69 granules!!*/
    RIO_TAG_T             tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->user_PL_Common_Ctray.rio_Packet_To_Send != NULL )
    { 
       /* invoke callback from user-supplied ctray */
       return model_Data->user_PL_Common_Ctray.rio_Packet_To_Send(
            model_Data->user_PL_Common_Ctray.rio_Hooks_Context,
            packet_Buffer,
            buffer_Size,
            packet_Length_In_Granules,
            tag
        );
        
    }

    return RIO_OK;
}

/***************************************************************************
 * Function :    RIO_Model_Set_Retry_Generator
 *
 * Description:  sets packet retry generator
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Set_Retry_Generator(
    RIO_CONTEXT_T    context,
    unsigned int    retry_Prob_Prio0,
    unsigned int    retry_Prob_Prio1,
    unsigned int    retry_Prob_Prio2,
    unsigned int    retry_Prob_Prio3
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;


    return model_Data->pl_Common_FTray.rio_PL_Set_Retry_Generator(
        model_Data->pl_Handle, retry_Prob_Prio0, retry_Prob_Prio1,
        retry_Prob_Prio2, retry_Prob_Prio3);        
}

/***************************************************************************
 * Function :    RIO_Model_Set_PNACC_Generator
 *
 * Description:  sets packet-not-accepted generator
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Set_PNACC_Generator(
    RIO_CONTEXT_T    context,
    unsigned int    pnacc_Factor
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;


    return model_Data->pl_Common_FTray.rio_PL_Set_PNACC_Generator(
        model_Data->pl_Handle, pnacc_Factor);        
}

/***************************************************************************
 * Function :    RIO_Model_Set_Stomp_Generator
 *
 * Description:  sets packet stomp generator
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Set_Stomp_Generator(
    RIO_CONTEXT_T    context,
    unsigned int    stomp_Factor
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;


    return model_Data->pl_Common_FTray.rio_PL_Set_Stomp_Generator(
        model_Data->pl_Handle, stomp_Factor);        
}


/***************************************************************************
 * Function :    RIO_Model_Set_LRQR_Generator
 *
 * Description:  sets LRQ-Reset generator
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_Set_LRQR_Generator(
    RIO_CONTEXT_T    context,
    unsigned int     lrqr_Factor
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;


    return model_Data->pl_Common_FTray.rio_PL_Set_LRQR_Generator(
        model_Data->pl_Handle, lrqr_Factor);        
}

/***************************************************************************
 * Function :    enable_Rnd_Idle_Generator
 *
 * Description:  Enable idles generator.
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
int enable_Rnd_Idle_Generator(
    RIO_CONTEXT_T           context,
    unsigned int low_Boundary,
    unsigned int high_Boundary,
    RIO_BOOL_T enable
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;

    return model_Data->pl_Common_FTray.rio_PL_Enable_Rnd_Idle_Generator(
        model_Data->pl_Handle, low_Boundary, high_Boundary, enable);
}

/***************************************************************************
 * Function :    Enable_Rnd_Pnacc_Generator_Init
 *
 * Description:  Enable PNACC generator.
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:        GDA 
 *
 **************************************************************************/

int RIO_Model_Enable_RND_Pnacc_Generator_Init(
    RIO_CONTEXT_T    context,
    RIO_WORD_T pnacc_Low_Boundary,
    RIO_WORD_T pnacc_High_Boundary,
    RIO_BYTE_T pnacc_Probability
    )

{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;


    return model_Data->pl_Common_FTray.rio_PL_Enable_RND_Pnacc_Generator_Init(
        model_Data->pl_Handle,pnacc_Low_Boundary,pnacc_High_Boundary,pnacc_Probability);        
}

/***************************************************************************
 * Function :    Enable_Rnd_Retry_Generator_Init
 *
 * Description:  Enable Retry generator.
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:        GDA 
 *
 **************************************************************************/

int RIO_Model_Enable_RND_Retry_Generator_Init(
    RIO_CONTEXT_T    context,
    RIO_WORD_T retry_Low_Boundary,
    RIO_WORD_T retry_High_Boundary,
    RIO_BYTE_T retry_Probability_Prio0,
    RIO_BYTE_T retry_Probability_Prio1,
    RIO_BYTE_T retry_Probability_Prio2,
    RIO_BYTE_T retry_Probability_Prio3
    )
{

    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;


    return model_Data->pl_Common_FTray.rio_PL_Enable_RND_Retry_Generator_Init(model_Data->pl_Handle,
                     retry_Low_Boundary,retry_High_Boundary,retry_Probability_Prio0,retry_Probability_Prio1,
                     retry_Probability_Prio2,retry_Probability_Prio3); 


}
/***************************************************************************
 * Function :    Enable_Multicast
 *
 * Description:  Enable Multicast control symbol
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:        GDA 
 *
 **************************************************************************/
int RIO_Model_Generate_Multicast(
	RIO_CONTEXT_T    context,
        RIO_WORD_T       low_Boundary,
        RIO_WORD_T       high_Boundary
	)
{
     RIO_MODEL_DS_T *model_Data;


    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;


 return model_Data->pl_Common_FTray.rio_PL_Generate_Multicast(model_Data->pl_Handle,low_Boundary,high_Boundary); 
}
/*****************************************************************************/


/* GDA: Bug#124 - Support for Data Streaming packet */
/***************************************************************************
 *  * Function :    RIO_Model_DS_Request
 *   
 *    Description:  simple wrapper around LL's DataStreaming  request function 
 *    
 *    Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *    
 *    Notes:
 *   
 *************************************************************************/
int RIO_Model_DS_Request(
	 RIO_HANDLE_T   handle,
	 RIO_DS_REQ_T   *rq,
	 RIO_TAG_T      trx_Tag
		)
{
	    RIO_MODEL_DS_T *model_Data;

	        /* check parameters */
	    if( handle == NULL )
	        return RIO_PARAM_INVALID;
	    else
	            model_Data = (RIO_MODEL_DS_T*)handle;
	        /* check if the model was binded already */
         if ( model_Data->was_Binded != RIO_TRUE ||
				        model_Data->instance_Mode != RIO_MODEL_PE_MODE)
	 {

	    RIO_Model_Message_Callback(
                	                model_Data,
	           "Error: It is not allowed to access PE API when instance is in PL mode\n"
		            );
	            return RIO_ERROR;
	 }

	     /* invoke function */
	     return model_Data->ll_FTray.rio_DS_Request(
		                  model_Data->ll_Handle,
	             			             rq,
		  	         	             trx_Tag
						         );
}


/* GDA: Bug#124 - Support for Data Streaming packet */

/***************************************************************************
 *  * Function :    RIO_Model_PL_DS_Request
 *  
 *   Description:  simple wrapper around PL's get pins function 
 *  
 *  Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *  
 *   Notes:
 *
 * **************************************************************************/

int RIO_Model_PL_DS_Request(
	 RIO_HANDLE_T   handle,
	 RIO_DS_REQ_T   *rq,
	 RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
	 RIO_TR_INFO_T  transport_Info
		)


{
	 RIO_MODEL_DS_T *model_Data;
	        /* check parameters */
       if( handle == NULL )
	        return RIO_PARAM_INVALID;
       else
                 model_Data = (RIO_MODEL_DS_T*)handle;

		        /* check if the model was binded already */
	        if ( model_Data->was_PL_Binded != RIO_TRUE ||
					        model_Data->instance_Mode != RIO_MODEL_PL_MODE
						        )
		{
				
                     RIO_Model_Message_Callback(
	                                     model_Data,
	             "Error: It is not allowed to access PL API when instance is in PE mode\n"
					          );
		            return RIO_ERROR;
	             }

		    return model_Data->pl_Common_FTray.rio_PL_DS_Request(
	           			  model_Data->pl_Handle,
	             			                     rq,
			                                 trx_Tag, /* user-assigned tag of the request */
					           transport_Info
					            );
}

/* GDA: Bug#124 - Support for Data Streaming packet */

/***************************************************************************
 * Function :    RIO_Model_PL_Get_Req_Data__Hw
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Get_Req_Data_Hw(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_HW_T      *data_hw
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_Get_Req_Data_Hw(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag,
            offset,
            cnt,
            data_hw
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_Get_Req_Data_Hw(
            model_Data->tl_Handle,
            tag,
            offset,
            cnt,
            data_hw
        );
    }
    else
        return RIO_ERROR;

}


/***************************************************************************/
/* GDA: Bug#124 - Support for Data Streaming packet */




/***************************************************************************
 * GDA: Bug#125 - Support for Flow control packet 
 *
 * Function	: RIO_Model_FC_Request
 *
 * Description	: simple wrapper around LL's Flow control packet 
 *		  request function 
 *
 * Returns	: RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes	:
 *
 **************************************************************************/
int RIO_Model_FC_Request( 
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag         
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }

    /* invoke function */
    return model_Data->ll_FTray.rio_FC_Request( 
        model_Data->ll_Handle, 
        rq,
        trx_Tag
    );    
}
/* GDA: Bug#125 - Support for Flow control packet */


/***************************************************************************
 * Function :    RIO_Model_PL_FC_Request
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_FC_Request(
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
    RIO_TR_INFO_T  transport_Info
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_FC_Request(
        model_Data->pl_Handle,    
        rq,
        trx_Tag,             /* user-assigned tag of the request */
        transport_Info
        );
}

 
/***************************************************************************
 * GDA: Bug#125 - Support for Flow control packet 

 * Function	: RIO_Model_Congestion
 *
 * Description	: simple wrapper around LL's Flow control packet 
		  request function 
 *
 * Returns	: RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes	:
 *
 **************************************************************************/
int RIO_Model_Congestion( 
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag         
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PE_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PE API when instance is in PL mode\n"
        );
        return RIO_ERROR;
    }

    /* invoke function */
    return model_Data->ll_FTray.rio_Congestion( 
        model_Data->ll_Handle, 
        trx_Tag
    );    
}
/* GDA: Bug#125 - Support for Flow control packet */


/***************************************************************************
 * Function :    RIO_Model_PL_Congestion
 *  *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_Congestion(
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE ||
        model_Data->instance_Mode != RIO_MODEL_PL_MODE 
        )
    {
        RIO_Model_Message_Callback(
            model_Data,
            "Error: It is not allowed to access PL API when instance is in PE mode\n"
        );
        return RIO_ERROR;
    }

    return model_Data->pl_Common_FTray.rio_PL_Congestion(
        model_Data->pl_Handle,    
        trx_Tag             /* user-assigned tag of the request */
        );
}

 
/* GDA: Bug#125 - Support for Flow control packet */

/***************************************************************************
 * Function :    RIO_Model_PL_FC_To_Send
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Model_PL_FC_To_Send(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    if( model_Data->instance_Mode == RIO_MODEL_PL_MODE )
    {
        /* invoke callback from user-supplied ctray */
        
        return model_Data->user_PL_Common_Ctray.rio_PL_FC_To_Send(
            model_Data->user_PL_Common_Ctray.pl_Interface_Context,
            tag
        );
    }
    else if( model_Data->instance_Mode == RIO_MODEL_PE_MODE )
    {
        /* invoke TL layer */
        
        return model_Data->tl_FTray.rio_FC_To_Send(
            model_Data->tl_Handle,
            tag
        );
    }
    else
        return RIO_ERROR;
}
