#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds executable for the unit testing environment
#

include ../../../verilog_env/demo/debug.inc

CC              =   gcc
TARGET          =   librio.so

FLEXLM_PATH      =   ../../../../flexlm

DEBUG		=   -DNDEBUG

FLEXLM_DEFINES  =

DEFINES         = $(FLEXLM_DEFINES) $(_DEBUG)

LIBS            =   -lc
LIBS_AIX	=   -lc
LIBS_LIN	=   -lc
LIBS_FLEXLM     =   $(FLEXLM_PATH)/sun4_u5/lm_new.o -L $(FLEXLM_PATH)/sun4_u5 -l lmgr  -l crvs -l sb -lsocket

INCLUDES        =   -I./include -I../../interfaces/common -I../../interfaces/serial_interfaces -I../../interfaces/parallel_interfaces\
			-I../../ll/include -I../../tl/include -I../../pl/include -I../rio_pl/include -I../../txrx/include\
			-I../../parallel_init/include -I../../pcs_pma/include -I../../pcs_pma_adapter/include \
			-I$(FLEXLM_PATH)/machind

CFLAGS          =   -c $(prof) $(DEFINES) -elf -ansi -pedantic -Wall
LFLAGS          =   $(prof) -G -B symbolic -s
LFLAGS_AIX	=   $(prof) -G -s -bE:rio.exp
LFLAGS_LINUX    =   $(prof) -G -Bsymbolic -s

OBJS            =   rio_model_common.o rio_serial_model.o rio_parallel_model.o\
			 ../rio_pl/rio_pl_model_common.o ../rio_pl/rio_pl_parallel_model.o ../rio_pl/rio_pl_serial_model.o

OBJS_P          =   rio_model_common.o rio_parallel_model.o\
			 ../rio_pl/rio_pl_model_common.o ../rio_pl/rio_pl_parallel_model.o

OBJS_S          =   rio_model_common.o rio_serial_model.o\
			 ../rio_pl/rio_pl_model_common.o ../rio_pl/rio_pl_serial_model.o


LLOBJ			=	../../ll/ll.o

PLOBJ			=	../../pl/pl.o

TLOBJ			=	../../tl/tl.o

PLOBJ_P			=	../../pl/pl_parallel.o

PLOBJ_S			=	../../pl/pl_serial.o

###############################################################################
# make default target
all :   $(TARGET)

# make target
$(TARGET) :	$(OBJS)
		cd ../../ll; make -f makefile.mak ll.o
		cd ../../tl; make -f makefile.mak tl.o
		cd ../../pl; make -f makefile.mak pl.o
		ld  $(LFLAGS) $(OBJS) $(LLOBJ) $(TLOBJ) $(PLOBJ) $(LIBS) -o $(TARGET)

# make target
all_flexlm :	$(OBJS)
		cd ../../ll; make -f makefile.mak ll.o
		cd ../../tl; make -f makefile.mak tl.o
		cd ../../pl; make -f makefile.mak pl.o
		ld  $(LFLAGS) $(OBJS) $(LLOBJ) $(TLOBJ) $(PLOBJ) $(LIBS) $(LIBS_FLEXLM) -o $(TARGET)
		#$(FLEXLM_PATH)/sun4_u5/lmstrip -r $(TARGET)


sun_par : $(OBJS_P)
		cd ../../ll; make -f makefile.mak all
		cd ../../tl; make -f makefile.mak all
		cd ../../pl; make -f makefile.mak parallel_pl
		ld  $(LFLAGS) $(OBJS_P) $(LLOBJ) $(TLOBJ) $(PLOBJ_P) $(LIBS) -o $(TARGET)

sun_serial : $(OBJS_S)
		cd ../../ll; make -f makefile.mak all
		cd ../../tl; make -f makefile.mak all
		cd ../../pl; make -f makefile.mak serial_pl
		ld  $(LFLAGS) $(OBJS_S) $(LLOBJ) $(TLOBJ) $(PLOBJ_S) $(LIBS) $(LIBS_FLEXLM) -o $(TARGET)


aix_all:	$(OBJS)
		cd ../../ll; make -f makefile.mak ll.o
		cd ../../tl; make -f makefile.mak tl.o
		cd ../../pl; make -f makefile.mak pl.o
		ld  $(LFLAGS_AIX) $(OBJS) $(LLOBJ) $(TLOBJ) $(PLOBJ) $(LIBS_AIX) -o $(TARGET)

aix_par : $(OBJS_P)
		cd ../../ll; make -f makefile.mak ll.o
		cd ../../tl; make -f makefile.mak tl.o
		cd ../../pl; make -f makefile.mak pl_parallel.o
		ld  $(LFLAGS_AIX) $(OBJS_P) $(LLOBJ) $(TLOBJ) $(PLOBJ_P) $(LIBS_AIX) -o $(TARGET)


linux_all:	$(OBJS)
		cd ../../ll; make -f makefile.mak ll.o
		cd ../../tl; make -f makefile.mak tl.o
		cd ../../pl; make -f makefile.mak pl.o
		ld  $(LFLAGS_LINUX) $(OBJS) $(LLOBJ) $(TLOBJ) $(PLOBJ) $(LIBS_LIN) -o $(TARGET)

linux_par : $(OBJS_P)
		cd ../../ll; make -f makefile.mak ll.o
		cd ../../tl; make -f makefile.mak tl.o
		cd ../../pl; make -f makefile.mak pl_parallel.o
		ld  $(LFLAGS_LINUX) $(OBJS_P) $(LLOBJ) $(TLOBJ) $(PLOBJ_P) $(LIBS_LIN) -o $(TARGET)

%.o :		%.c
		$(CC) $(CFLAGS) $(INCLUDES) -o $*.o $<

#clean data
clean :		
		rm -f $(OBJS)
		rm -f $(TARGET)
		cd ../../ll; make -f makefile.mak clean
		cd ../../tl; make -f makefile.mak clean
		cd ../../pl; make -f makefile.mak clean

###############################################################################
             
