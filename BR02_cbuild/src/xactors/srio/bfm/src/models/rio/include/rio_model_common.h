#ifndef RIO_MODEL_COMMON_H
#define RIO_MODEL_COMMON_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio/include/rio_model_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains declaration of the common RapidIO PE model
*               function interface
*
* Notes:        
*
******************************************************************************/

#include "rio_types.h"
#include "rio_common.h"

#include "rio_pl_interface.h"
#include "rio_tl_entry.h"
#include "rio_ll_entry.h"


/*********************************************************************
 * These data structures describe whole RIO model.                     
 * They ar not used elsethere in the source code so they are
 * implemented here                                                   
 *********************************************************************/

typedef enum {
    RIO_MODEL_NOT_INITIALIZED = 0,
    RIO_MODEL_PE_MODE,
    RIO_MODEL_PL_MODE
}RIO_MODEL_MODE_T;



typedef struct {

    RIO_PL_CLOCK_EDGE            rio_PL_Clock_Edge;
    RIO_PL_GSM_REQUEST           rio_PL_GSM_Request;
    RIO_PL_IO_REQUEST            rio_PL_IO_Request;
    RIO_PL_MESSAGE_REQUEST       rio_PL_Message_Request;
    RIO_PL_DOORBELL_REQUEST      rio_PL_Doorbell_Request;
    RIO_PL_CONFIGURATION_REQUEST rio_PL_Configuration_Request;
    RIO_PL_SEND_RESPONSE         rio_PL_Send_Response;
    RIO_PL_ACK_REMOTE_REQ        rio_PL_Ack_Remote_Req;
    
    RIO_SET_CONFIG_REG           rio_PL_Set_Config_Reg;
    RIO_GET_CONFIG_REG           rio_PL_Get_Config_Reg;

    RIO_PL_PACKET_STRUCT_REQUEST rio_PL_Packet_Struct_Request;

    RIO_PL_MODEL_START_RESET     rio_PL_Model_Start_Reset;
    RIO_DELETE_INSTANCE          rio_PL_Delete_Instance;

    RIO_PL_ENABLE_RND_IDLE_GENERATOR rio_PL_Enable_Rnd_Idle_Generator;
    RIO_PL_SET_RETRY_GENERATOR   rio_PL_Set_Retry_Generator;
    RIO_PL_SET_PNACC_GENERATOR   rio_PL_Set_PNACC_Generator;
    RIO_PL_SET_STOMP_GENERATOR   rio_PL_Set_Stomp_Generator;
    RIO_PL_SET_LRQR_GENERATOR    rio_PL_Set_LRQR_Generator;

    /*GDA: Rnd Generator (PNACC/RETRY)*/
    RIO_PL_ENABLE_RND_PNACC_GENERATOR_INIT  rio_PL_Enable_RND_Pnacc_Generator_Init;
    RIO_PL_ENABLE_RND_RETRY_GENERATOR_INIT  rio_PL_Enable_RND_Retry_Generator_Init;
    /*GDA: Rnd Generator (PNACC/RETRY)*/


    /*GDA: Mulitcast Control Generator Bug#116 */
    RIO_PL_GENERATE_MULTICAST   rio_PL_Generate_Multicast;
    /*GDA: Mulitcast Control Generator Bug#116 */

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_DS_REQUEST            rio_PL_DS_Request;   
    /* GDA: Bug#124 - Support for Data Streaming packet */

   /* GDA: Bug#125 - Support for Flow control packet */
    RIO_PL_FC_REQUEST            rio_PL_FC_Request;
    RIO_PL_CONGESTION            rio_PL_Congestion;
    /* GDA: Bug#125 - Support for Flow control packet */


} RIO_PL_COMMON_FTRAY;



typedef struct {

    RIO_PL_ACK_REQUEST      rio_PL_Ack_Request;
    RIO_PL_GET_REQ_DATA     rio_PL_Get_Req_Data;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    RIO_PL_GET_REQ_DATA_HW  rio_PL_Get_Req_Data_Hw;
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    RIO_PL_ACK_RESPONSE     rio_PL_Ack_Response;
    RIO_PL_GET_RESP_DATA    rio_PL_Get_Resp_Data;
    RIO_PL_REQ_TIME_OUT     rio_PL_Req_Time_Out;
    RIO_PL_REMOTE_REQUEST   rio_PL_Remote_Request;
    RIO_PL_REMOTE_RESPONSE  rio_PL_Remote_Response;
    RIO_CONTEXT_T           pl_Interface_Context; 

    RIO_REMOTE_CONFIG_READ  extend_Reg_Read;
    RIO_REMOTE_CONFIG_WRITE extend_Reg_Write;
    RIO_CONTEXT_T           extend_Reg_Context;

    RIO_REQUEST_RECEIVED    rio_Request_Received;
    RIO_RESPONSE_RECEIVED   rio_Response_Received;
    RIO_SYMBOL_TO_SEND      rio_Symbol_To_Send;
    RIO_PACKET_TO_SEND      rio_Packet_To_Send;
    RIO_CONTEXT_T           rio_Hooks_Context;

    /* GDA: Bug#125 - Support for Flow control packet */
    RIO_PL_FC_TO_SEND      rio_PL_FC_To_Send;
    /* GDA: Bug#125 - Support for Flow control packet */

    
} RIO_PL_COMMON_CTRAY;



typedef struct {

    RIO_BOOL_T is_Parallel_Model; /* RIO_TRUE in case model was created as parallel */

    RIO_HANDLE_T  ll_Handle; /* pointer to the LL data structure */
    RIO_HANDLE_T  tl_Handle; /* pointer to the TL data structure */
    RIO_HANDLE_T  pl_Handle; /* pointer to the PL data structure */       

    RIO_LL_FTRAY_T      ll_FTray; /* Logical Layer function tray   */
    RIO_TL_FTRAY_T      tl_FTray; /* Transport Layer function tray */
    RIO_HANDLE_T        pl_FTray; /* Pointer to either serial or parallel Physical Layer FTray is stored here*/
    RIO_PL_COMMON_FTRAY pl_Common_FTray; /* Functions which are not specific to seraial or parallel Physical
                                            Layer implementation are copied to this FTray                    */

    RIO_USER_MSG    message_Callback;   /* environment's error message callback  */
    RIO_HANDLE_T    msg_Context;              /* context for the messages              */

    RIO_WORD_T      coh_Domain_Size; /* this field is necessary for the proper copying  */
                                     /* of the remote devices IDs during initialization */

    RIO_BOOL_T      support_GSM; /* this flag is set during instantiation if the model */
                                 /* supports GSM transactions                          */

    RIO_BOOL_T      was_Binded; /* this flag shows that PE model was already binded or not */
    
    /* data necessary for allowing use of LL and PL API in one instance */
    RIO_BOOL_T      was_PL_Binded; /* shows that PL model was user-binded or not */

    RIO_MODEL_MODE_T instance_Mode; /* switch telling that instance is working in PL or PE mode */

    RIO_HANDLE_T        user_PL_Ctray; /* pointer to callback tray supplied by user in case PL API is used. 
                                          This pointer stores either serial or parallel CTray              */ 
    RIO_PL_COMMON_CTRAY user_PL_Common_Ctray; /* This is a set of functions which are common for both parallel 
                                                 and serial physical layer implementations                     */
} RIO_MODEL_DS_T;


/* these are masks for the destination OPs CAR and source OPs CAR */
#define RIO_SOURCE_GSM_OPS_MASK 0xFFC00000
#define RIO_DEST_GSM_OPS_MASK   0xFFC00000


/* this is a maximum size of the window mapped form the IO space to the config space */
#define RIO_MAX_CONFIG_SPACE_SIZE 0x1000000


int RIO_Model_Print_Version(
    RIO_HANDLE_T    handle
    );

/* 
 * These are function prototypes for external LL model interface.
 * The functions from the PL and LL cannot be used here because of 
 * differences in parameters.
 */

int RIO_Model_GSM_Request(
    RIO_HANDLE_T    handle, 
    RIO_GSM_TTYPE_T ttype,
    RIO_GSM_REQ_T   *rq, 
    RIO_TAG_T       trx_Tag
);

int RIO_Model_IO_Request( 
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag         
);

int RIO_Model_Message_Request(
    RIO_HANDLE_T      handle, 
    RIO_MESSAGE_REQ_T *rq, 
    RIO_TAG_T         trx_Tag
);

int RIO_Model_Doorbell_Request(
    RIO_HANDLE_T       handle, 
    RIO_DOORBELL_REQ_T *rq, 
    RIO_TAG_T          trx_Tag
);

int RIO_Model_Config_Request(
    RIO_HANDLE_T     handle, 
    RIO_CONFIG_REQ_T *rq, 
    RIO_TAG_T        trx_Tag
);

int RIO_Model_Snoop_Response(
    RIO_CONTEXT_T      handle, 
    RIO_SNOOP_RESULT_T snoop_Result, 
    RIO_DW_T           *data
);

int RIO_Model_Set_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *dw
);

int RIO_Model_Get_Memory_Data(
    RIO_HANDLE_T handle, 
    RIO_DW_T     *dw
);

int RIO_Model_Set_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,  
    unsigned long     data
);

int RIO_Model_Get_Config_Reg(
    RIO_HANDLE_T      handle,
    RIO_CONF_OFFS_T   config_Offset,   
    unsigned long     *data
);

int RIO_Model_Start_Reset(
    RIO_HANDLE_T handle
);

int RIO_Model_Delete_Instance(
    RIO_HANDLE_T handle
);

/* GDA: Bug#124 - Support for Data Streaming packet */

int RIO_Model_DS_Request(
    RIO_HANDLE_T   handle,
    RIO_DS_REQ_T   *rq,
    RIO_TAG_T      trx_Tag
		);
/* GDA: Bug#124 - Support for Data Streaming packet */


/* this function is to be supplied as an error callback to the layers  */
/* It is only intended to add error message prefix "RIO Model" to the  */
/* layers error messages                                               */

int RIO_Model_Message_Callback(
    RIO_CONTEXT_T  context,
    char *         error_Msg 
);


int RIO_Model_Packet_Struct_Request(
    RIO_HANDLE_T       handle, 
    RIO_MODEL_PACKET_STRUCT_RQ_T *rq, 
    RIO_TAG_T          trx_Tag
);


/*
 * PL API
 */

int RIO_Model_Clock_Edge(
    RIO_HANDLE_T handle /* instance handle */
);

int RIO_Model_PL_GSM_Request(
    RIO_HANDLE_T       handle,
    RIO_PL_GSM_TTYPE_T ttype,          /* requested transaction type  */
    RIO_GSM_REQ_T      *rq,
    RIO_UCHAR_T        sec_ID,            /* ignored for packet types other than 1 */
    RIO_UCHAR_T        sec_TID,
    RIO_TAG_T          trx_Tag,           /* user-assigned tag of the request */
    RIO_TR_INFO_T      transport_Info[],
    RIO_UCHAR_T        multicast_Size     /* count of recepients */
);

int RIO_Model_PL_IO_Request(
    RIO_HANDLE_T   handle,
    RIO_IO_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
    RIO_TR_INFO_T  transport_Info
);

int RIO_Model_PL_Message_Request(
    RIO_HANDLE_T       handle,
    RIO_MESSAGE_REQ_T  *rq,
    RIO_TAG_T          trx_Tag             /* user-assigned tag of the request */
);

int RIO_Model_PL_Doorbell_Request(
    RIO_HANDLE_T        handle,
    RIO_DOORBELL_REQ_T  *rq,
    RIO_TAG_T           trx_Tag             /* user-assigned tag of the request */
);

int RIO_Model_PL_Configuration_Request(
    RIO_HANDLE_T      handle,
    RIO_CONFIG_REQ_T  *rq,
    RIO_TAG_T         trx_Tag          /* user-assigned tag of the request */
);

int RIO_Model_PL_Packet_Struct_Request(
    RIO_HANDLE_T      handle,
    RIO_MODEL_PACKET_STRUCT_RQ_T  *rq,
    RIO_TAG_T         trx_Tag          /* user-assigned tag of the request */
);

int RIO_Model_PL_Send_Response(
    RIO_HANDLE_T   handle, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
);

int RIO_Model_PL_Ack_Remote_Req(
    RIO_HANDLE_T handle, 
    RIO_TAG_T    tag
);

/* GDA: Bug#124 - Support for Data Streaming packet */
int RIO_Model_PL_DS_Request(
    RIO_HANDLE_T   handle,
    RIO_DS_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
    RIO_TR_INFO_T  transport_Info
);
/* GDA: Bug#124 - Support for Data Streaming packet */


/* 
 * PL API callbacks realized in PE model and routed to either TL or environment
 */

int RIO_Model_PL_Model_Start_Reset(RIO_HANDLE_T handle);

int RIO_Model_PL_Ack_Request(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
);

int RIO_Model_PL_Get_Req_Data(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
);

/* GDA: Bug#124 - Support for Data Streaming packet */
int RIO_Model_PL_Get_Req_Data_Hw(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_HW_T      *data_hw
);
/* GDA: Bug#124 - Support for Data Streaming packet */



/* GDA: Bug#125 - Support for Flow control packet */

int RIO_Model_FC_Request( 
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag         
);
int RIO_Model_Congestion( 
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag         
);

int RIO_Model_PL_FC_Request(
    RIO_HANDLE_T   handle,
    RIO_FC_REQ_T   *rq,
    RIO_TAG_T      trx_Tag,             /* user-assigned tag of the request */
    RIO_TR_INFO_T  transport_Info
);

int RIO_Model_PL_Congestion(
    RIO_HANDLE_T   handle,
    RIO_TAG_T      trx_Tag             /* user-assigned tag of the request */
);

int RIO_Model_PL_FC_To_Send(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
);
/* GDA: Bug#125 - Support for Flow control packet */

int RIO_Model_PL_Ack_Response(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
);

int RIO_Model_PL_Get_Resp_Data(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag, 
    RIO_UCHAR_T   offset, 
    RIO_UCHAR_T   cnt, 
    RIO_DW_T      *data
);

int RIO_Model_PL_Req_Time_Out(
    RIO_CONTEXT_T context, 
    RIO_TAG_T     tag
);

int RIO_Model_PL_Remote_Request(
    RIO_CONTEXT_T        context, 
    RIO_TAG_T            tag, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_TR_INFO_T        transport_Info
);

int RIO_Model_PL_Remote_Response(
    RIO_CONTEXT_T  context, 
    RIO_TAG_T      tag, 
    RIO_RESPONSE_T *resp
);


int RIO_Model_PL_Extend_Reg_Read(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,      /* double-word aligned offset inside configuration space */
    unsigned long    *data
);

int RIO_Model_PL_Extend_Reg_Write(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,       /* double-word aligned offset inside configuration space */
    unsigned long    data
);

int RIO_Model_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
);

int RIO_Model_Request_Received(
    RIO_CONTEXT_T         context,
    RIO_REMOTE_REQUEST_T  *packet_Struct      /* struct, containing packets's fields */
);

int RIO_Model_Response_Received(
    RIO_CONTEXT_T         context,
    RIO_RESPONSE_T        *packet_Struct      /* struct, containing response's fields */
);

int RIO_Model_Packet_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int          buffer_Size,        /*Size always = 276*/  
    RIO_UCHAR_T*          packet_Length_In_Granules, /* maximum = 276/4 = 69 granules!!*/
    RIO_TAG_T             tag
);

int RIO_Model_Set_Retry_Generator(
    RIO_CONTEXT_T    context,
    unsigned int    retry_Prob_Prio0,
    unsigned int    retry_Prob_Prio1,
    unsigned int    retry_Prob_Prio2,
    unsigned int    retry_Prob_Prio3
);

int RIO_Model_Set_PNACC_Generator(
    RIO_CONTEXT_T    context,
    unsigned int    pnacc_Factor
);

int RIO_Model_Set_Stomp_Generator(
    RIO_CONTEXT_T    context,
    unsigned int    stomp_Factor
);

int RIO_Model_Set_LRQR_Generator(
    RIO_CONTEXT_T    context,
    unsigned int    lrqr_Factor
);

int enable_Rnd_Idle_Generator(
    RIO_CONTEXT_T    context,
    unsigned int low_Boundary,
    unsigned int high_Boundary,
    RIO_BOOL_T enable
);


/*GDA: Rnd Generator (PNACC/RETRY)*/

int RIO_Model_Enable_RND_Pnacc_Generator_Init(

    RIO_CONTEXT_T    context,
    RIO_WORD_T pnacc_Low_Boundary,
    RIO_WORD_T pnacc_High_Boundary,
    RIO_BYTE_T pnacc_Probability 
);



int RIO_Model_Enable_RND_Retry_Generator_Init(
    RIO_CONTEXT_T    context,
    RIO_WORD_T retry_Low_Boundary,
    RIO_WORD_T retry_High_Boundary,
    RIO_BYTE_T retry_Probability_Prio0,
    RIO_BYTE_T retry_Probability_Prio1,
    RIO_BYTE_T retry_Probability_Prio2,
    RIO_BYTE_T retry_Probability_Prio3
);

/*GDA: Rnd Generator (PNACC/RETRY)*/

/*GDA: Multicast Control Symbol Bug#116*/
int RIO_Model_Generate_Multicast(
    RIO_CONTEXT_T    context, 
    RIO_WORD_T       low_Boundary,
    RIO_WORD_T       high_Boundary
    );
/*GDA: Multicast Control Symbol Bug#116*/

#endif /* RIO_MODEL_COMMON_H */


