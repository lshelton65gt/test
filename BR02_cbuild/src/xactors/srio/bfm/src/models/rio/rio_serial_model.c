/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio/rio_serial_model.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  
*
* Notes:                
*
******************************************************************************/

#include "rio_serial_model.h"
#include "rio_model_common.h"
#include "rio_pl_serial_model.h"
#include "rio_tl_entry.h"
#include "rio_ll_entry.h"


#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#ifdef _LICENSING
#include "lmpolicy.h"
#endif /* _LICENSING */


/* these are constants defining licensing for the RapidIO Serial model */
#ifdef _LICENSING

#define RIO_SERIAL_FEATURE_NAME    "mot_eda_rapidio_serial_bfm"
#define RIO_SERIAL_FEATURE_VER     "1.0"
#define RIO_SERIAL_LIC_SEARCH_PATH "mot_rapidio_serial_bfm.lic;."

#endif /* _LICENSING */

/*
 * These are functions prototypes for RIO parallel model initialization 
 * and reset
 * These functions declared as static because it is no need to use them
 * outside this file. Pointers to these functions are supportde via
 * model function tray.
 */

static int rio_Serial_Model_Initialize(
    RIO_HANDLE_T           handle,
    RIO_SERIAL_PARAM_SET_T *param_Set
);

/*
 *  LP-EP link interface 
 */
 
static int rio_Serial_Model_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0, /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1, /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2, /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3  /* receive data on lane 3*/
);


/*
 * PL API
 */

static int rio_Serial_Model_PL_Model_Initialize(
    RIO_HANDLE_T              handle,
    RIO_PL_SERIAL_PARAM_SET_T *param_Set
);


/* 
 * PL API callbacks realized in PE model and routed to either TL or environment
 */

static int rio_Serial_Model_PL_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T tlane0,   /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,   /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,   /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3    /* transmit data on lane 3*/
);
    

static int rio_Serial_Model_Granule_To_Send(
    RIO_CONTEXT_T         context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule   /* pointer to granule data structure */
);

static int rio_Serial_Model_Granule_Received(
    RIO_CONTEXT_T         context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule   /* pointer to granule data structure */
);

static int rio_Serial_Model_Character_Column_To_Send(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
);

static int rio_Serial_Model_Character_Column_Received(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
);

static int rio_Serial_Model_Codegroup_Column_To_Send(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
);

static int rio_Serial_Model_Codegroup_Received(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
);


/***************************************************************************
 * Function :    RIO_Serial_Model_Create_Instance
 *
 * Description:  this function creates instance of the model.
 *
 * Returns:      RIO_OK, RIO_ERROR or RIO_PARAM_INVALID
 *
 * Notes:        this is the only function in the model which allocates
 *               memory for the pointer passed as function parameter
 *
 **************************************************************************/
int RIO_Serial_Model_Create_Instance(
    RIO_HANDLE_T           *handle, 
    RIO_MODEL_INST_PARAM_T *param
)
{
    /* pointer to the data of the newly created instance */
    RIO_MODEL_DS_T  *model_Data;

    RIO_LL_INST_PARAM_T ll_Param;

    RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T pl_CTray;
 
#ifdef _LICENSING

    static RIO_BOOL_T is_Lic_Checkedout = RIO_FALSE;
    LP_HANDLE *lp_Handle;
    
#endif /* _LICENSING */

    /* check parameters */
    if ( handle == NULL)
        return RIO_PARAM_INVALID;

    if ( param == NULL )
        return RIO_PARAM_INVALID;

#ifdef _LICENSING
    /* try checking the license for the model */
    if( is_Lic_Checkedout == RIO_FALSE )
    {
        if( lp_checkout(LPCODE, LM_RESTRICTIVE | LM_MANUAL_HEARTBEAT, RIO_SERIAL_FEATURE_NAME, 
                        RIO_SERIAL_FEATURE_VER, 1, RIO_SERIAL_LIC_SEARCH_PATH, &lp_Handle) )
        {
            fprintf(stderr, "RapidIO Model Serial Create Instance: checkout of feature %s failed: %s\n", RIO_SERIAL_FEATURE_NAME, 
                    lp_errstring(lp_Handle));

            /* set handle to NULL */
            *handle = NULL;

            /* return RIO_ERROR;*/
            return RIO_ERROR;
        }
        else
        {
            is_Lic_Checkedout = RIO_TRUE;
            fprintf(stderr, "RapidIO Model Serial Create Instance: successfull checkout of feature %s\n", RIO_SERIAL_FEATURE_NAME);
        }
    }
#endif /* _LICENSING */

    /* allocate memory for the instance's data and initialize it */
    model_Data = (RIO_MODEL_DS_T *)malloc(sizeof( RIO_MODEL_DS_T ));
    /* can't allocate memory */
    if ( model_Data == NULL )
        return RIO_ERROR;

    /* initialize model handle fields */
    model_Data->is_Parallel_Model        = RIO_FALSE;
    model_Data->message_Callback         = NULL;
    model_Data->msg_Context              = NULL;
    model_Data->was_Binded               = RIO_FALSE;
    model_Data->coh_Domain_Size          = param->coh_Domain_Size;
    model_Data->support_GSM              = ( ( ( param->source_Trx & RIO_SOURCE_GSM_OPS_MASK ) != 0 ) || 
                                            ( ( param->source_Trx & RIO_DEST_GSM_OPS_MASK ) != 0 ) );

    model_Data->was_PL_Binded            = RIO_FALSE;
    model_Data->instance_Mode            = RIO_MODEL_NOT_INITIALIZED;

    /* callback tray supplied by user in case PL API is used */ 
    model_Data->user_PL_Ctray = (RIO_HANDLE_T)malloc(sizeof(RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T));
    memset((void*)(model_Data->user_PL_Ctray), 0, sizeof(RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T)); 

    /* allocate memory for the PL Function Tray structure */
    model_Data->pl_FTray = (RIO_HANDLE_T)malloc(sizeof(RIO_PL_SERIAL_MODEL_FTRAY_T));
    memset((void*)(model_Data->pl_FTray), 0, sizeof(RIO_PL_SERIAL_MODEL_FTRAY_T));
    
    /* clear PL common CTray fields */
    model_Data->user_PL_Common_Ctray.rio_PL_Ack_Request     = NULL;
    model_Data->user_PL_Common_Ctray.rio_PL_Get_Req_Data    = NULL;
    
    /* GDA: Bug#124 - Support for Data Streaming packet */
    model_Data->user_PL_Common_Ctray.rio_PL_Get_Req_Data_Hw = NULL; 
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    model_Data->user_PL_Common_Ctray.rio_PL_Ack_Response    = NULL;
    model_Data->user_PL_Common_Ctray.rio_PL_Get_Resp_Data   = NULL;
    model_Data->user_PL_Common_Ctray.rio_PL_Req_Time_Out    = NULL;
    model_Data->user_PL_Common_Ctray.rio_PL_Remote_Request  = NULL;
    model_Data->user_PL_Common_Ctray.rio_PL_Remote_Response = NULL;
    model_Data->user_PL_Common_Ctray.pl_Interface_Context   = NULL; 

    model_Data->user_PL_Common_Ctray.extend_Reg_Read        = NULL;
    model_Data->user_PL_Common_Ctray.extend_Reg_Write       = NULL;
    model_Data->user_PL_Common_Ctray.extend_Reg_Context     = NULL;

    model_Data->user_PL_Common_Ctray.rio_Request_Received   = NULL;
    model_Data->user_PL_Common_Ctray.rio_Response_Received  = NULL;
    model_Data->user_PL_Common_Ctray.rio_Symbol_To_Send     = NULL;
    model_Data->user_PL_Common_Ctray.rio_Packet_To_Send     = NULL;
    model_Data->user_PL_Common_Ctray.rio_Hooks_Context      = NULL;

    /* GDA: Bug#125 - Support for Flow control packet */
    model_Data->user_PL_Common_Ctray.rio_PL_FC_To_Send      = NULL;

    /* check coherency domain size */
    if ( param->coh_Domain_Size > RIO_MAX_COH_DOMAIN_SIZE ||
        ( model_Data->support_GSM && param->coh_Domain_Size < 2 ) ) /* two is a minimum allowed coherency domain size */
        return RIO_PARAM_INVALID;
    
    /* instantiate PL                                                */
    /* it is not necessary to compose instantiation param set for it */
    if ( RIO_PL_Serial_Model_Create_Instance( &(model_Data->pl_Handle), param ) != RIO_OK )
        return RIO_ERROR;

    /* create instance of the Transport Layer */
    if ( RIO_TL_Create_Instance( &(model_Data->tl_Handle) ) != RIO_OK)
        return RIO_ERROR;

    /* compose instantiation parameters for the LL and instantiate it */
    ll_Param.coh_Granule_Size_32    = param->coh_Granule_Size_32;   
    ll_Param.coh_Domain_Size        = param->coh_Domain_Size;
   
    ll_Param.source_Trx = param->source_Trx;
    ll_Param.dest_Trx   = param->dest_Trx;

    ll_Param.has_Memory             = param->has_Memory;                 
    ll_Param.has_Processor          = param->has_Processor;                 
    ll_Param.has_Doorbell           = param->has_Doorbell;                 
    ll_Param.mbox1                  = param->mbox1;                 
    ll_Param.mbox2                  = param->mbox2;                 
    ll_Param.mbox3                  = param->mbox3;                 
    ll_Param.mbox4                  = param->mbox4;                 
     /* GDA : Bug #123 Support for extended mailbox (xmbox) feature */
    ll_Param.mbox5                  = param->mbox5;
    ll_Param.mbox6                  = param->mbox6;
    ll_Param.mbox7                  = param->mbox7;
    ll_Param.mbox8                  = param->mbox8;
    ll_Param.mbox9                  = param->mbox9;
    ll_Param.mbox10                 = param->mbox10;
    ll_Param.mbox11                 = param->mbox11;
    ll_Param.mbox12                 = param->mbox12;
    ll_Param.mbox13                 = param->mbox13;
    ll_Param.mbox14                 = param->mbox14;
    ll_Param.mbox15                 = param->mbox15;
    ll_Param.mbox16                 = param->mbox16;
    ll_Param.mbox17                 = param->mbox17;
    ll_Param.mbox18                 = param->mbox18;
    ll_Param.mbox19                 = param->mbox19;
    ll_Param.mbox20                 = param->mbox20;
    ll_Param.mbox21                 = param->mbox21;
    ll_Param.mbox22                 = param->mbox22;
    ll_Param.mbox23                 = param->mbox23;
    ll_Param.mbox24                 = param->mbox24;
    ll_Param.mbox25                 = param->mbox25;
    ll_Param.mbox26                 = param->mbox26;
    ll_Param.mbox27                 = param->mbox27;
    ll_Param.mbox28                 = param->mbox28;
    ll_Param.mbox29                 = param->mbox29;
    ll_Param.mbox30                 = param->mbox30;
    ll_Param.mbox31                 = param->mbox31;
    ll_Param.mbox32                 = param->mbox32;
    ll_Param.mbox33                 = param->mbox33;
    ll_Param.mbox34                 = param->mbox34;
    ll_Param.mbox35                 = param->mbox35;
    ll_Param.mbox36                 = param->mbox36;
    ll_Param.mbox37                 = param->mbox37;
    ll_Param.mbox38                 = param->mbox38;
    ll_Param.mbox39                 = param->mbox39;
    ll_Param.mbox40                 = param->mbox40;
    ll_Param.mbox41                 = param->mbox41;
    ll_Param.mbox42                 = param->mbox42;
    ll_Param.mbox43                 = param->mbox43;
    ll_Param.mbox44                 = param->mbox44;
    ll_Param.mbox45                 = param->mbox45;
    ll_Param.mbox46                 = param->mbox46;
    ll_Param.mbox47                 = param->mbox47;
    ll_Param.mbox48                 = param->mbox48;
    ll_Param.mbox49                 = param->mbox49;
    ll_Param.mbox50                 = param->mbox50;
    ll_Param.mbox51                 = param->mbox51;
    ll_Param.mbox52                 = param->mbox52;
    ll_Param.mbox53                 = param->mbox53;
    ll_Param.mbox54                 = param->mbox54;
    ll_Param.mbox55                 = param->mbox55;
    ll_Param.mbox56                 = param->mbox56;
    ll_Param.mbox57                 = param->mbox57;
    ll_Param.mbox58                 = param->mbox58;
    ll_Param.mbox59                 = param->mbox59;
    ll_Param.mbox60                 = param->mbox60;
    ll_Param.mbox61                 = param->mbox61;
    ll_Param.mbox62                 = param->mbox62;
    ll_Param.mbox63                 = param->mbox63;
    ll_Param.mbox64                 = param->mbox64;

   /* GDA : Bug # 123 Support for extended mailbox (xmbox) feature */
 
    ll_Param.inbound_Buffer_Size    = param->ll_Inbound_Buffer_Size;   
    ll_Param.outbound_Buffer_Size   = param->ll_Outbound_Buffer_Size;  

   
    /*GDA: Bug#43 Fix*/
    ll_Param.gda_Tx_Illegal_Packet  = param->gda_Tx_Illegal_Packet;
    ll_Param.gda_Rx_Illegal_Packet  = param->gda_Rx_Illegal_Packet;
    /*GDA: Bug#43 Fix*/


    if ( RIO_LL_Create_Instance( &(model_Data->ll_Handle), &ll_Param) != RIO_OK)
        return RIO_ERROR;

    /* get function trays for the layers */
    if ( RIO_LL_Get_Function_Tray( &(model_Data->ll_FTray) ) != RIO_OK)
        return RIO_ERROR;

    if ( RIO_TL_Get_Function_Tray( &(model_Data->tl_FTray) ) != RIO_OK)
        return RIO_ERROR;

    if ( RIO_PL_Serial_Model_Get_Function_Tray( (RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray) ) != RIO_OK)
        return RIO_ERROR;

    /* copy common Physical Layer functions to common PL FTray */
    model_Data->pl_Common_FTray.rio_PL_Clock_Edge
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Clock_Edge;
    model_Data->pl_Common_FTray.rio_PL_GSM_Request           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_GSM_Request;
    model_Data->pl_Common_FTray.rio_PL_IO_Request           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_IO_Request;           

    /* GDA: Bug#124 - Support for Data Streaming packet */
    model_Data->pl_Common_FTray.rio_PL_DS_Request
          = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_DS_Request;
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    model_Data->pl_Common_FTray.rio_PL_FC_Request           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_FC_Request;           
    model_Data->pl_Common_FTray.rio_PL_Congestion           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Congestion;           
    /* GDA: Bug#125 */
 
    model_Data->pl_Common_FTray.rio_PL_Message_Request           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Message_Request;
    model_Data->pl_Common_FTray.rio_PL_Doorbell_Request           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Doorbell_Request;      
    model_Data->pl_Common_FTray.rio_PL_Configuration_Request           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Configuration_Request;
    model_Data->pl_Common_FTray.rio_PL_Packet_Struct_Request = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Packet_Struct_Request;

    model_Data->pl_Common_FTray.rio_PL_Send_Response           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Send_Response;
    model_Data->pl_Common_FTray.rio_PL_Ack_Remote_Req           
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Ack_Remote_Req;        
    model_Data->pl_Common_FTray.rio_PL_Model_Start_Reset
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Model_Start_Reset;
    model_Data->pl_Common_FTray.rio_PL_Delete_Instance
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Delete_Instance;

    model_Data->pl_Common_FTray.rio_PL_Set_Config_Reg
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Set_Config_Reg;
    model_Data->pl_Common_FTray.rio_PL_Get_Config_Reg
        = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Get_Config_Reg;

    model_Data->pl_Common_FTray.rio_PL_Enable_Rnd_Idle_Generator = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Enable_Rnd_Idle_Generator;
    model_Data->pl_Common_FTray.rio_PL_Set_Retry_Generator = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Set_Retry_Generator;
    model_Data->pl_Common_FTray.rio_PL_Set_PNACC_Generator = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Set_PNACC_Generator;
    model_Data->pl_Common_FTray.rio_PL_Set_Stomp_Generator = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Set_Stomp_Generator;
    model_Data->pl_Common_FTray.rio_PL_Set_LRQR_Generator = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Set_LRQR_Generator;


    /*GDA: Rnd Generator (PNACC/RETRY)*/
    model_Data->pl_Common_FTray.rio_PL_Enable_RND_Pnacc_Generator_Init = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Enable_RND_Pnacc_Generator_Init;

    model_Data->pl_Common_FTray.rio_PL_Enable_RND_Retry_Generator_Init = 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Enable_RND_Retry_Generator_Init;

    /*GDA: Rnd Generator (PNACC/RETRY)*/

     /*GDA: Bug#116 Implementation*/
     model_Data->pl_Common_FTray.rio_PL_Generate_Multicast= 
        ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Generate_Multicast;
     /*GDA: Bug#116 Implementation*/

    
    /* bind PL instance with wrappers supplied by the PE model */
    pl_CTray.rio_PL_Ack_Request     = RIO_Model_PL_Ack_Request;    
    pl_CTray.rio_PL_Get_Req_Data    = RIO_Model_PL_Get_Req_Data;   
    
    /* GDA: Bug#124 - Support for Data Streaming packet */
    pl_CTray.rio_PL_Get_Req_Data_Hw = RIO_Model_PL_Get_Req_Data_Hw;  
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    pl_CTray.rio_PL_Ack_Response    = RIO_Model_PL_Ack_Response;   
    pl_CTray.rio_PL_Get_Resp_Data   = RIO_Model_PL_Get_Resp_Data;  
    pl_CTray.rio_PL_Req_Time_Out    = RIO_Model_PL_Req_Time_Out;   
    pl_CTray.rio_PL_Remote_Request  = RIO_Model_PL_Remote_Request; 
    pl_CTray.rio_PL_Remote_Response = RIO_Model_PL_Remote_Response;
    pl_CTray.pl_Interface_Context   = (RIO_CONTEXT_T)model_Data;

    pl_CTray.rio_PL_Set_Pins             = rio_Serial_Model_PL_Set_Pins;
    pl_CTray.lp_Serial_Interface_Context = (RIO_CONTEXT_T)model_Data;

    pl_CTray.rio_User_Msg           = RIO_Model_Message_Callback;
    pl_CTray.rio_Msg                = (RIO_CONTEXT_T)model_Data;  

    pl_CTray.extend_Reg_Read        = RIO_Model_PL_Extend_Reg_Read;
    pl_CTray.extend_Reg_Write       = RIO_Model_PL_Extend_Reg_Write;
    pl_CTray.extend_Reg_Context     = (RIO_CONTEXT_T)model_Data;
    
    
    pl_CTray.rio_Serial_Granule_To_Send           = rio_Serial_Model_Granule_To_Send;
    pl_CTray.rio_Serial_Granule_Received          = rio_Serial_Model_Granule_Received;          
    pl_CTray.rio_Serial_Character_Column_To_Send  = rio_Serial_Model_Character_Column_To_Send;
    pl_CTray.rio_Serial_Character_Column_Received = rio_Serial_Model_Character_Column_Received; 
    pl_CTray.rio_Serial_Codegroup_Column_To_Send  = rio_Serial_Model_Codegroup_Column_To_Send;
    pl_CTray.rio_Serial_Codegroup_Received        = rio_Serial_Model_Codegroup_Received;
    pl_CTray.rio_Serial_Hooks_Context             = (RIO_CONTEXT_T)model_Data;

    pl_CTray.rio_Request_Received   = RIO_Model_Request_Received;
    pl_CTray.rio_Response_Received  = RIO_Model_Response_Received;
    pl_CTray.rio_Symbol_To_Send     = RIO_Model_Symbol_To_Send;
    pl_CTray.rio_Packet_To_Send     = RIO_Model_Packet_To_Send;
    pl_CTray.rio_Hooks_Context      = (RIO_CONTEXT_T)model_Data;

    /* GDA: Bug#125 - Support for Flow control packet */
    pl_CTray.rio_PL_FC_To_Send     = RIO_Model_PL_FC_To_Send;    

    if ( RIO_PL_Serial_Model_Bind_Instance( model_Data->pl_Handle, &pl_CTray ) != RIO_OK )
        return RIO_ERROR;
    
    /* return value */
    *handle = (RIO_HANDLE_T)model_Data;

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_Serial_Model_Get_Function_Tray
 *
 * Description:  this function places pointers to the functions into 
 *               supplied ftray structure.
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Serial_Model_Get_Function_Tray(
    RIO_SERIAL_MODEL_FTRAY_T *ftray
)
{
    /* check parameter */   
    if ( ftray == NULL )
        return RIO_PARAM_INVALID;

    /* compose function tray from the LL and PL trays */
    ftray->rio_GSM_Request      = RIO_Model_GSM_Request;
    ftray->rio_IO_Request       = RIO_Model_IO_Request;
    ftray->rio_Message_Request  = RIO_Model_Message_Request;
    ftray->rio_Doorbell_Request = RIO_Model_Doorbell_Request;
    ftray->rio_Conf_Request     = RIO_Model_Config_Request;
    ftray->rio_Snoop_Response   = RIO_Model_Snoop_Response;
    ftray->rio_Set_Mem_Data     = RIO_Model_Set_Memory_Data;
    ftray->rio_Get_Mem_Data     = RIO_Model_Get_Memory_Data;
    ftray->rio_Set_Conf_Reg     = RIO_Model_Set_Config_Reg;
    ftray->rio_Get_Conf_Reg     = RIO_Model_Get_Config_Reg;
    ftray->rio_Packet_Struct_Request = RIO_Model_Packet_Struct_Request;

    ftray->rio_Start_Reset      = RIO_Model_Start_Reset;
    ftray->rio_Serial_Init      = rio_Serial_Model_Initialize;

    ftray->rio_PL_Clock_Edge    = RIO_Model_Clock_Edge;
    ftray->rio_PL_Get_Pins      = rio_Serial_Model_Get_Pins;
    
    ftray->rio_Print_Version    = RIO_Model_Print_Version;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ftray->rio_DS_Request       = RIO_Model_DS_Request;   
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    
    /* PL API */

    ftray->rio_PL_Serial_Model_Initialize = rio_Serial_Model_PL_Model_Initialize;
    ftray->rio_PL_Model_Start_Reset       = RIO_Model_PL_Model_Start_Reset;

    ftray->rio_PL_GSM_Request            = RIO_Model_PL_GSM_Request;
    ftray->rio_PL_IO_Request             = RIO_Model_PL_IO_Request;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ftray->rio_PL_DS_Request             = RIO_Model_PL_DS_Request;   
    /* GDA: Bug#124 - Support for Data Streaming packet */

    ftray->rio_PL_Message_Request        = RIO_Model_PL_Message_Request;
    ftray->rio_PL_Doorbell_Request       = RIO_Model_PL_Doorbell_Request;
    ftray->rio_PL_Configuration_Request  = RIO_Model_PL_Configuration_Request; 
    ftray->rio_PL_Packet_Struct_Request  = RIO_Model_PL_Packet_Struct_Request;
    ftray->rio_PL_Send_Response          = RIO_Model_PL_Send_Response;
    ftray->rio_PL_Ack_Remote_Req         = RIO_Model_PL_Ack_Remote_Req;
    
    ftray->rio_PL_Enable_Rnd_Idle_Generator = enable_Rnd_Idle_Generator;
    ftray->rio_Delete_Instance           = RIO_Model_Delete_Instance;
    ftray->rio_PL_Set_Retry_Generator    = RIO_Model_Set_Retry_Generator;
    ftray->rio_PL_Set_PNACC_Generator    = RIO_Model_Set_PNACC_Generator;
    ftray->rio_PL_Set_Stomp_Generator    = RIO_Model_Set_Stomp_Generator;
    ftray->rio_PL_Set_LRQR_Generator     = RIO_Model_Set_LRQR_Generator;

    /*GDA: Rnd Generator (PNACC/RETRY)*/
    ftray->rio_PL_Enable_RND_Pnacc_Generator_Init = RIO_Model_Enable_RND_Pnacc_Generator_Init;
    ftray->rio_PL_Enable_RND_Retry_Generator_Init = RIO_Model_Enable_RND_Retry_Generator_Init;
    /*GDA: Rnd Generator (PNACC/RETRY)*/

    /*GDA: Bug#116 Implementation*/
     ftray->rio_PL_Generate_Multicast  	 = RIO_Model_Generate_Multicast;
    /*GDA: Bug#116 */

    /* GDA: Bug#125 - Support for Flow control packet */
    ftray->rio_FC_Request  		 = RIO_Model_FC_Request;
    ftray->rio_PL_FC_Request             = RIO_Model_PL_FC_Request;
    ftray->rio_Congestion  		 = RIO_Model_Congestion;
    ftray->rio_PL_Congestion             = RIO_Model_PL_Congestion;
    /* GDA: Bug#125 */
 
    return RIO_OK;
}



/***************************************************************************
 * Function :    RIO_Serial_Model_Bind_Instance
 *
 * Description:  this function places supplied callback tray into internal 
 *               model data structure i.e. LL callback tray and PL callback 
 *               tray.
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
int RIO_Serial_Model_Bind_Instance(
    RIO_HANDLE_T                     handle, 
    RIO_SERIAL_MODEL_CALLBACK_TRAY_T *ctray
)
{
    RIO_MODEL_DS_T *model_Data;

    RIO_LL_CALLBACK_TRAY_T                ll_CTray;
    RIO_TL_CALLBACK_TRAY_T                tl_CTray;

    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    if ( ctray == NULL )
        return RIO_PARAM_INVALID;

    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    /* set callback fields in the model data structure */
    if( ctray->rio_User_Msg != NULL )
    {        
        model_Data->message_Callback         = ctray->rio_User_Msg;
        model_Data->msg_Context              = ctray->rio_Msg;
    }

    /* compose LL callback tray and supply it to the layer */
    ll_CTray.rio_Local_Response          = ctray->rio_Local_Response;         
    ll_CTray.rio_GSM_Request_Done        = ctray->rio_GSM_Request_Done;       
    ll_CTray.rio_IO_Request_Done         = ctray->rio_IO_Request_Done;        

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ll_CTray.rio_DS_Request_Done         = ctray->rio_DS_Request_Done;  
    /* GDA: Bug#124 */

    ll_CTray.rio_MP_Request_Done         = ctray->rio_MP_Request_Done;        
    ll_CTray.rio_Doorbell_Request_Done   = ctray->rio_Doorbell_Request_Done;  
    ll_CTray.rio_Config_Request_Done     = ctray->rio_Config_Request_Done;    
    ll_CTray.local_Device_Context        = ctray->local_Device_Context;       

    ll_CTray.rio_Snoop_Request           = ctray->rio_Snoop_Request;          
    ll_CTray.snoop_Context               = ctray->snoop_Context;              

    ll_CTray.rio_MP_Remote_Request         = ctray->rio_MP_Remote_Request;      
    ll_CTray.mailbox_Context               = ctray->mailbox_Context;            
    ll_CTray.rio_Doorbell_Remote_Request   = ctray->rio_Doorbell_Remote_Request;
    ll_CTray.doorbell_Context              = ctray->doorbell_Context;           
    ll_CTray.rio_Port_Write_Remote_Request = ctray->rio_Port_Write_Remote_Request;
    ll_CTray.port_Context                  = ctray->port_Context;           

    ll_CTray.rio_Memory_Request          = ctray->rio_Memory_Request;         
    ll_CTray.memory_Context              = ctray->memory_Context;             

    ll_CTray.rio_Read_Dir                = ctray->rio_Read_Dir;               
    ll_CTray.rio_Write_Dir               = ctray->rio_Write_Dir;              
    ll_CTray.directory_Context           = ctray->directory_Context;          

    ll_CTray.rio_Tr_Local_IO             = ctray->rio_Tr_Local_IO;            
    ll_CTray.rio_Tr_Remote_IO            = ctray->rio_Tr_Remote_IO;           

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ll_CTray.rio_Tr_Local_DS             = ctray->rio_Tr_Local_DS;
    ll_CTray.rio_Tr_Remote_DS            = ctray->rio_Tr_Remote_DS;
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    ll_CTray.rio_Tr_Remote_FC            = ctray->rio_Tr_Remote_FC;           
    ll_CTray.rio_Tr_Local_FC             = ctray->rio_Tr_Local_FC;            
    ll_CTray.rio_TL_FC_Request           = model_Data->tl_FTray.rio_FC_Request;          
    ll_CTray.rio_FC_Request_Done         = ctray->rio_FC_Request_Done;        
    ll_CTray.rio_TL_Congestion           = model_Data->tl_FTray.rio_Congestion;          
    /* GDA: Bug#125 */
 
    ll_CTray.rio_Route_GSM               = ctray->rio_Route_GSM;              
    ll_CTray.address_Translation_Context = ctray->address_Translation_Context;

    ll_CTray.rio_Remote_Conf_Read        = ctray->rio_Remote_Conf_Read;       
    ll_CTray.rio_Remote_Conf_Write       = ctray->rio_Remote_Conf_Write;      
    ll_CTray.extended_Features_Context   = ctray->extended_Features_Context;  

    ll_CTray.rio_Set_Config_Reg          = model_Data->tl_FTray.rio_Set_Config_Reg;
    ll_CTray.rio_Get_Config_Reg          = model_Data->tl_FTray.rio_Get_Config_Reg;
    ll_CTray.rio_Config_Context          = model_Data->tl_Handle;

    ll_CTray.rio_TL_GSM_Request          = model_Data->tl_FTray.rio_GSM_Request;         
    ll_CTray.rio_TL_IO_Request           = model_Data->tl_FTray.rio_IO_Request;          

    /* GDA: Bug#124 - Support for Data Streaming packet */
    ll_CTray.rio_TL_DS_Request           = model_Data->tl_FTray.rio_DS_Request;  
    /* GDA: Bug#124 - Support for Data Streaming packet */
    
    ll_CTray.rio_TL_Message_Request       = model_Data->tl_FTray.rio_Message_Request;     
    ll_CTray.rio_TL_Doorbell_Request      = model_Data->tl_FTray.rio_Doorbell_Request;    
    ll_CTray.rio_TL_Configuration_Request = model_Data->tl_FTray.rio_Conf_Request;
    ll_CTray.rio_TL_Send_Response         = model_Data->tl_FTray.rio_Send_Response;       
    ll_CTray.rio_TL_Outbound_Context      = model_Data->tl_Handle;

    ll_CTray.rio_TL_Ack_Remote_Req       = model_Data->tl_FTray.rio_Ack_Remote_Req;
    ll_CTray.rio_TL_Inbound_Context      = model_Data->tl_Handle;

    ll_CTray.rio_User_Msg                = RIO_Model_Message_Callback;
    ll_CTray.rio_Msg                     = model_Data;  
    
    if ( RIO_LL_Bind_Instance( model_Data->ll_Handle, &ll_CTray ) != RIO_OK)
        return RIO_ERROR;

    /* compose TL callback tray and supply it to the layer */
    tl_CTray.rio_Set_Config_Reg = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Set_Config_Reg; 
    tl_CTray.rio_Get_Config_Reg = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Get_Config_Reg; 
    tl_CTray.rio_Config_Context = model_Data->pl_Handle;            
                                            
    tl_CTray.rio_PL_GSM_Request = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_GSM_Request;
    tl_CTray.rio_PL_IO_Request  = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_IO_Request;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    tl_CTray.rio_PL_DS_Request  = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_DS_Request;
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    tl_CTray.rio_PL_FC_Request  = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_FC_Request;
    tl_CTray.rio_PL_Congestion  = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Congestion;
    /* GDA: Bug#125 */

    tl_CTray.rio_PL_Message_Request       = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Message_Request;
    tl_CTray.rio_PL_Doorbell_Request      = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Doorbell_Request;
    tl_CTray.rio_PL_Configuration_Request = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Configuration_Request;
    tl_CTray.rio_PL_Send_Response         = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Send_Response;
    tl_CTray.rio_PL_Outbound_Context      = model_Data->pl_Handle;       
                                            
    tl_CTray.rio_PL_Ack_Remote_Req  = ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Ack_Remote_Req;         
    tl_CTray.rio_PL_Inbound_Context = model_Data->pl_Handle; 

    tl_CTray.rio_LL_Ack_Request       = model_Data->ll_FTray.rio_Ack_Request;
    tl_CTray.rio_LL_Get_Req_Data      = model_Data->ll_FTray.rio_Get_Req_Data;

    /* GDA: Bug#124 - Support for Data Streaming packet */
    tl_CTray.rio_LL_Get_Req_Data_Hw   = model_Data->ll_FTray.rio_Get_Req_Data_Hw;
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    tl_CTray.rio_LL_FC_To_Send       = model_Data->ll_FTray.rio_FC_To_Send;
    /* GDA: Bug#125 */

    tl_CTray.rio_LL_Get_Resp_Data     = model_Data->ll_FTray.rio_Get_Resp_Data;
    tl_CTray.rio_LL_Ack_Response      = model_Data->ll_FTray.rio_Ack_Response;
    tl_CTray.rio_LL_Req_Time_Out      = model_Data->ll_FTray.rio_Req_Time_Out;
    tl_CTray.rio_LL_Remote_Request    = model_Data->ll_FTray.rio_Remote_Request;  
    tl_CTray.rio_LL_Remote_Response   = model_Data->ll_FTray.rio_Remote_Responce;
    tl_CTray.rio_LL_Interface_Context = model_Data->ll_Handle; 

    tl_CTray.rio_LL_Remote_Conf_Read   = model_Data->ll_FTray.rio_Remote_Conf_Read;
    tl_CTray.rio_LL_Remote_Conf_Write  = model_Data->ll_FTray.rio_Remote_Conf_Write;
    tl_CTray.extended_Features_Context = model_Data->ll_Handle;

    tl_CTray.rio_User_Msg    = RIO_Model_Message_Callback;
    tl_CTray.rio_Msg         = model_Data;  

    if ( RIO_TL_Bind_Instance( model_Data->tl_Handle, &tl_CTray ) != RIO_OK)
        return RIO_ERROR;


    if( ctray->rio_PL_Set_Pins != NULL )
    {
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_PL_Set_Pins        
            = ctray->rio_PL_Set_Pins;
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->lp_Serial_Interface_Context 
            = ctray->lp_Serial_Interface_Context;
    }
    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_PL_Set_Pins == NULL )
        return RIO_ERROR;

    if( ctray->rio_Serial_Granule_To_Send != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_To_Send 
            = ctray->rio_Serial_Granule_To_Send;
    if( ctray->rio_Serial_Granule_Received != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_Received 
            = ctray->rio_Serial_Granule_Received;
    if( ctray->rio_Serial_Character_Column_To_Send != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_To_Send 
            = ctray->rio_Serial_Character_Column_To_Send;
    if( ctray->rio_Serial_Character_Column_Received != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_Received 
            = ctray->rio_Serial_Character_Column_Received;
    if( ctray->rio_Serial_Codegroup_Column_To_Send != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Column_To_Send 
            = ctray->rio_Serial_Codegroup_Column_To_Send;
    if( ctray->rio_Serial_Codegroup_Received != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Received 
            = ctray->rio_Serial_Codegroup_Received;

    if( ctray->rio_Serial_Granule_To_Send != NULL ||
        ctray->rio_Serial_Granule_Received != NULL ||
        ctray->rio_Serial_Character_Column_To_Send != NULL ||
        ctray->rio_Serial_Character_Column_Received != NULL ||
        ctray->rio_Serial_Codegroup_Column_To_Send != NULL ||
        ctray->rio_Serial_Codegroup_Received != NULL
        )     
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context 
            = ctray->rio_Serial_Hooks_Context;

    if( ctray->rio_Request_Received != NULL )
        model_Data->user_PL_Common_Ctray.rio_Request_Received   = ctray->rio_Request_Received;
    if( ctray->rio_Response_Received != NULL )
        model_Data->user_PL_Common_Ctray.rio_Response_Received  = ctray->rio_Response_Received;
    if( ctray->rio_Symbol_To_Send != NULL )
        model_Data->user_PL_Common_Ctray.rio_Symbol_To_Send     = ctray->rio_Symbol_To_Send;
    if( ctray->rio_Packet_To_Send != NULL )
        model_Data->user_PL_Common_Ctray.rio_Packet_To_Send     = ctray->rio_Packet_To_Send;

    if( ctray->rio_Packet_To_Send != NULL ||
        ctray->rio_Symbol_To_Send != NULL ||
        ctray->rio_Response_Received != NULL ||
        ctray->rio_Request_Received != NULL
        )     
        model_Data->user_PL_Common_Ctray.rio_Hooks_Context = ctray->rio_Hooks_Context;


    /* set was binded flag */
    model_Data->was_Binded = RIO_TRUE;

    return RIO_OK;
}


/***************************************************************************
 * Function :    RIO_Serial_Model_Bind_PL_Instance
 *
 * Description:  
 *
 * Returns:      
 *
 * Notes:
 *
 **************************************************************************/

int RIO_Serial_Model_Bind_PL_Instance(
    RIO_HANDLE_T                         handle,
    RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T  *ctray
)
{
    RIO_MODEL_DS_T *model_Data;

    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    if ( ctray == NULL )
        return RIO_PARAM_INVALID;

    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    if( ctray->rio_User_Msg != NULL )
    {        
        model_Data->message_Callback = ctray->rio_User_Msg;
        model_Data->msg_Context      = ctray->rio_Msg;
    }


    if( ctray->rio_PL_Ack_Request == NULL ||
        ctray->rio_PL_Ack_Response == NULL ||
        ctray->rio_PL_Get_Req_Data == NULL ||

        ctray->rio_PL_Get_Req_Data_Hw == NULL ||/* GDA:Bug#124 - Support for Data Streaming packet */
	ctray->rio_PL_FC_To_Send == NULL ||	/* GDA: Bug#125 - Support for Flow control packet */

        ctray->rio_PL_Get_Resp_Data == NULL ||
        ctray->rio_PL_Remote_Request == NULL ||
        ctray->rio_PL_Remote_Response == NULL ||
        ctray->rio_PL_Req_Time_Out == NULL ||
        ctray->extend_Reg_Read == NULL ||
        ctray->extend_Reg_Write == NULL 
        )
        return RIO_PARAM_INVALID;

    /* mandatory callbacks */
    model_Data->user_PL_Common_Ctray.rio_PL_Ack_Request     = ctray->rio_PL_Ack_Request;    
    model_Data->user_PL_Common_Ctray.rio_PL_Get_Req_Data    = ctray->rio_PL_Get_Req_Data;   

    /* GDA: Bug#124 - Support for Data Streaming packet */
    model_Data->user_PL_Common_Ctray.rio_PL_Get_Req_Data_Hw    = ctray->rio_PL_Get_Req_Data_Hw;   
    /* GDA: Bug#124 */

    /* GDA: Bug#125 - Support for Flow control packet */
    model_Data->user_PL_Common_Ctray.rio_PL_FC_To_Send     = ctray->rio_PL_FC_To_Send;    
    /* GDA: Bug#125 */

    model_Data->user_PL_Common_Ctray.rio_PL_Ack_Response    = ctray->rio_PL_Ack_Response;   
    model_Data->user_PL_Common_Ctray.rio_PL_Get_Resp_Data   = ctray->rio_PL_Get_Resp_Data;  
    model_Data->user_PL_Common_Ctray.rio_PL_Req_Time_Out    = ctray->rio_PL_Req_Time_Out;   
    model_Data->user_PL_Common_Ctray.rio_PL_Remote_Request  = ctray->rio_PL_Remote_Request; 
    model_Data->user_PL_Common_Ctray.rio_PL_Remote_Response = ctray->rio_PL_Remote_Response;
    model_Data->user_PL_Common_Ctray.pl_Interface_Context   = ctray->pl_Interface_Context;

    model_Data->user_PL_Common_Ctray.extend_Reg_Read        = ctray->extend_Reg_Read;
    model_Data->user_PL_Common_Ctray.extend_Reg_Write       = ctray->extend_Reg_Write;
    model_Data->user_PL_Common_Ctray.extend_Reg_Context     = ctray->extend_Reg_Context;


    if( ctray->rio_PL_Set_Pins != NULL )
    {
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_PL_Set_Pins        
            = ctray->rio_PL_Set_Pins;
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->lp_Serial_Interface_Context 
            = ctray->lp_Serial_Interface_Context;
    }
    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_PL_Set_Pins == NULL )
        return RIO_ERROR;

    if( ctray->rio_Serial_Granule_To_Send != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_To_Send 
            = ctray->rio_Serial_Granule_To_Send;
    if( ctray->rio_Serial_Granule_Received != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_Received 
            = ctray->rio_Serial_Granule_Received;
    if( ctray->rio_Serial_Character_Column_To_Send != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_To_Send 
            = ctray->rio_Serial_Character_Column_To_Send;
    if( ctray->rio_Serial_Character_Column_Received != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_Received 
            = ctray->rio_Serial_Character_Column_Received;
    if( ctray->rio_Serial_Codegroup_Column_To_Send != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Column_To_Send 
            = ctray->rio_Serial_Codegroup_Column_To_Send;
    if( ctray->rio_Serial_Codegroup_Received != NULL )
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Received 
            = ctray->rio_Serial_Codegroup_Received;

    if( ctray->rio_Serial_Granule_To_Send != NULL ||
        ctray->rio_Serial_Granule_Received != NULL ||
        ctray->rio_Serial_Character_Column_To_Send != NULL ||
        ctray->rio_Serial_Character_Column_Received != NULL ||
        ctray->rio_Serial_Codegroup_Column_To_Send != NULL ||
        ctray->rio_Serial_Codegroup_Received != NULL
        )     
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context 
            = ctray->rio_Serial_Hooks_Context;

    if( ctray->rio_Request_Received != NULL )
        model_Data->user_PL_Common_Ctray.rio_Request_Received   = ctray->rio_Request_Received;
    if( ctray->rio_Response_Received != NULL )
        model_Data->user_PL_Common_Ctray.rio_Response_Received  = ctray->rio_Response_Received;
    if( ctray->rio_Symbol_To_Send != NULL )
        model_Data->user_PL_Common_Ctray.rio_Symbol_To_Send     = ctray->rio_Symbol_To_Send;
    if( ctray->rio_Packet_To_Send != NULL )
        model_Data->user_PL_Common_Ctray.rio_Packet_To_Send     = ctray->rio_Packet_To_Send;

    if( ctray->rio_Packet_To_Send != NULL ||
        ctray->rio_Symbol_To_Send != NULL ||
        ctray->rio_Response_Received != NULL ||
        ctray->rio_Request_Received != NULL 
        )     
        model_Data->user_PL_Common_Ctray.rio_Hooks_Context = ctray->rio_Hooks_Context;

    /* set was binded flag */
    model_Data->was_PL_Binded = RIO_TRUE;

    return RIO_OK;
}    




/***************************************************************************
 *
 *
 *     Static functions to be passed through the Function tray
 *
 *
 */


/***************************************************************************
 * Function :    rio_Serial_Model_Initialize
 *
 * Description:  function initializes model instance accordingly to supplied 
 *               parameters set
 *
 * Returns:      RIO_OK or RIO_PARAM_INVALID
 *
 * Notes:
 *
 **************************************************************************/
static int rio_Serial_Model_Initialize(
    RIO_HANDLE_T           handle,
    RIO_SERIAL_PARAM_SET_T *param_Set
)
{
    RIO_MODEL_DS_T     *model_Data;

    RIO_PL_SERIAL_PARAM_SET_T pl_Param_Set;
    RIO_TL_PARAM_SET_T        tl_Param_Set;
    RIO_LL_PARAM_SET_T        ll_Param_Set;

    unsigned int       i;


    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    if ( param_Set == NULL )
        return RIO_PARAM_INVALID;

    /* check if the model was binded already */
    if ( model_Data->was_Binded != RIO_TRUE )
        return RIO_ERROR;

    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
    {
        if ( model_Data->was_Binded != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_Model_Message_Callback(
                model_Data,
                "Error: It is not allowed to call 1x/4x LP-Serial specific API"
                " functions while the model is configured to work with the 8/16 LP-LVDS link. \n"
            );
            return RIO_ERROR;
        }
    }

    if( model_Data->instance_Mode != RIO_MODEL_NOT_INITIALIZED )
        return RIO_ERROR;
        
    if( param_Set->transp_Type == RIO_PL_TRANSP_32 && 
        model_Data->support_GSM && param_Set->dev_ID > 255 )
    {
        /* GSM won't work correctly */
        RIO_Model_Message_Callback(
            model_Data,
            "Error: You can not set device ID larger than 255 for devices supporting"
            " GSM transactions and using large transport field format. \n"
        );
        return RIO_PARAM_INVALID;
    }
    
    if( param_Set->config_Space_Size > RIO_MAX_CONFIG_SPACE_SIZE )
        return RIO_PARAM_INVALID;

    /* compose PL parameter set and initialize it */
    pl_Param_Set.is_Host              = param_Set->is_Host;
    pl_Param_Set.is_Master_Enable     = param_Set->is_Master_Enable;
    pl_Param_Set.is_Discovered        = param_Set->is_Discovered;
    pl_Param_Set.input_Is_Enable      = param_Set->input_Is_Enable;
    pl_Param_Set.output_Is_Enable     = param_Set->output_Is_Enable;
    
    pl_Param_Set.lp_Serial_Is_1x         = param_Set->lp_Serial_Is_1x;         
    pl_Param_Set.is_Force_1x_Mode        = param_Set->is_Force_1x_Mode;       
    pl_Param_Set.is_Force_1x_Mode_Lane_0 = param_Set->is_Force_1x_Mode_Lane_0; 
    pl_Param_Set.discovery_Period        = param_Set->discovery_Period;         
    pl_Param_Set.silence_Period          = param_Set->silence_Period;         
    pl_Param_Set.comp_Seq_Rate           = param_Set->comp_Seq_Rate;          
    pl_Param_Set.status_Sym_Rate         = param_Set->status_Sym_Rate;         
    pl_Param_Set.comp_Seq_Size           = param_Set->comp_Seq_Size;          
    pl_Param_Set.comp_Seq_Rate_Check     = param_Set->comp_Seq_Rate_Check;

    pl_Param_Set.lcsbar               = param_Set->lcsbar;              
    pl_Param_Set.lcshbar              = param_Set->lcshbar;              
    pl_Param_Set.ext_Address          = param_Set->ext_Address;       
    pl_Param_Set.ext_Address_16       = param_Set->ext_Address_16;    
    pl_Param_Set.ext_Mailbox_Support  = param_Set->ext_Mailbox_Support;
    pl_Param_Set.transp_Type          = param_Set->transp_Type;         
    pl_Param_Set.dev_ID               = param_Set->dev_ID;              
    pl_Param_Set.res_For_3_Out        = param_Set->res_For_3_Out;       
    pl_Param_Set.res_For_2_Out        = param_Set->res_For_2_Out;       
    pl_Param_Set.res_For_1_Out        = param_Set->res_For_1_Out;       
    pl_Param_Set.res_For_3_In         = param_Set->res_For_3_In;        
    pl_Param_Set.res_For_2_In         = param_Set->res_For_2_In;        
    pl_Param_Set.res_For_1_In         = param_Set->res_For_1_In;        
    pl_Param_Set.pass_By_Prio         = param_Set->pass_By_Prio;        
    pl_Param_Set.transmit_Flow_Control_Support = param_Set->transmit_Flow_Control_Support;
    pl_Param_Set.enable_Retry_Return_Code = RIO_FALSE; /*PE model doesnt need for return codes*/
    pl_Param_Set.enable_LRQ_Resending = param_Set->enable_LRQ_Resending;
    pl_Param_Set.icounter_Max = param_Set->icounter_Max;
     pl_Param_Set.data_Corrupted = param_Set->data_Corrupted; /*GDA:Bug#139 remove the need to recompute crc */


    if ( ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Serial_Model_Initialize( 
        model_Data->pl_Handle, 
        &pl_Param_Set ) != RIO_OK )
            return RIO_ERROR;

    /* compose TL parameter set and initialize it */
    
    /* routing method is now fixed to be table-based only and is not supplied in init. params */
    tl_Param_Set.routing_Meth = RIO_TABLE_BASED_ROUTING;
    tl_Param_Set.transp_Type  = param_Set->transp_Type;

    if ( model_Data->tl_FTray.rio_Init( model_Data->tl_Handle, &tl_Param_Set ) != RIO_OK )
        return RIO_ERROR;

    ll_Param_Set.config_Space_Size = param_Set->config_Space_Size;              
    ll_Param_Set.transp_Type       = param_Set->transp_Type;
    
    /* check remote devices id's array pointer */
    if ( model_Data->coh_Domain_Size >= 2 && param_Set->remote_Dev_ID == NULL )
        return RIO_PARAM_INVALID;

    /* copy remote devices id's array */
    if ( model_Data->coh_Domain_Size >= 2 ) /* two is a minimum allowed coherency domain size */
        for ( i = 0; i < model_Data->coh_Domain_Size - 1; i++ )
        {
            ll_Param_Set.remote_Dev_ID[i] = param_Set->remote_Dev_ID[i];  
            
            if ( param_Set->transp_Type == RIO_PL_TRANSP_32 &&
                model_Data->support_GSM && param_Set->remote_Dev_ID[i] > 255 )
                RIO_Model_Message_Callback(
                    model_Data,
                    "Warning: GSM protocols may function incorrectly when coherence"
                    " domain participants have device ID larger than 255 for"
                    " systems using large transport field.\n"
                );
        }                
                 

    ll_Param_Set.ext_Mailbox_Support  = param_Set->ext_Mailbox_Support;
    ll_Param_Set.res_For_3_Out = param_Set->res_For_3_Out;
    ll_Param_Set.res_For_2_Out = param_Set->res_For_2_Out;
    ll_Param_Set.res_For_1_Out = param_Set->res_For_1_Out;
    ll_Param_Set.res_For_3_In  = param_Set->res_For_3_In; 
    ll_Param_Set.res_For_2_In  = param_Set->res_For_2_In; 
    ll_Param_Set.res_For_1_In  = param_Set->res_For_1_In; 
    ll_Param_Set.pass_By_Prio  = param_Set->pass_By_Prio; 

    if ( model_Data->ll_FTray.rio_Init( model_Data->ll_Handle, &ll_Param_Set ) != RIO_OK )
        return RIO_ERROR;

    model_Data->instance_Mode = RIO_MODEL_PE_MODE;

    return RIO_OK;

}



/***************************************************************************
 * LP-EP link API
****************************************************************************/


/***************************************************************************
 * Function :    rio_Serial_Model_Get_Pins
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
static int rio_Serial_Model_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0, /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1, /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2, /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3  /* receive data on lane 3*/
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    /* check if the model was binded already */
    if ( (model_Data->was_Binded != RIO_TRUE && model_Data->was_PL_Binded != RIO_TRUE) ||
        model_Data->instance_Mode == RIO_MODEL_NOT_INITIALIZED )
        return RIO_ERROR;

    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model)
    {
        if ( model_Data->was_Binded != RIO_TRUE  && model_Data->was_PL_Binded != RIO_TRUE ) 
            return RIO_ERROR;
        else
        {
            RIO_Model_Message_Callback(
                model_Data,
                "Error: It is not allowed to call 1x/4x LP-Serial specific API"
                " functions while the model is configured to work with the 8/16 LP-LVDS link. \n"
            );
            return RIO_ERROR;
        }
    }

    /* invoke function */
    return ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Serial_Get_Pins( 
        model_Data->pl_Handle, 
        rlane0,
        rlane1,
        rlane2,
        rlane3 
    );    
}


/***************************************************************************
 * PL API
****************************************************************************/

/***************************************************************************
 * Function :    rio_Serial_Model_PL_Model_Initialize
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_PL_Model_Initialize(
    RIO_HANDLE_T              handle,
    RIO_PL_SERIAL_PARAM_SET_T *param_Set
)
{
    RIO_MODEL_DS_T     *model_Data;

    /* check parameters */
    if( handle == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)handle;

    if ( param_Set == NULL )
        return RIO_PARAM_INVALID;

    /* check if the model was binded already */
    if ( model_Data->was_PL_Binded != RIO_TRUE )
        return RIO_ERROR;

    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
    {
        if ( model_Data->was_PL_Binded != RIO_TRUE )
            return RIO_ERROR;
        else
        {
            RIO_Model_Message_Callback(
                model_Data,
                "Error: It is not allowed to call 1x/4x LP-Serial specific API"
                " functions while the model is configured to work with the 8/16 LP-LVDS link. \n"
            );
            return RIO_ERROR;
        }
    }

    if( model_Data->instance_Mode != RIO_MODEL_NOT_INITIALIZED )
        return RIO_ERROR;

    if ( ((RIO_PL_SERIAL_MODEL_FTRAY_T*)(model_Data->pl_FTray))->rio_PL_Serial_Model_Initialize( 
        model_Data->pl_Handle, 
        param_Set ) != RIO_OK )
            return RIO_ERROR;

    model_Data->instance_Mode  = RIO_MODEL_PL_MODE;

    return RIO_OK;
}


/***************************************************************************
 * PL callbacks realized in PE model and routed to either TL or environment
 ****************************************************************************/


/***************************************************************************
 * Function :    rio_Serial_Model_PL_Set_Pins
 *
 * Description:  simple wrapper around PL's get pins function 
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_PL_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T tlane0,   /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,   /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,   /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3    /* transmit data on lane 3*/
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    return ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_PL_Set_Pins(
        ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->lp_Serial_Interface_Context,
        tlane0,
        tlane1,
        tlane2,
        tlane3 
    );
}


/***************************************************************************
 * Function :    rio_Serial_Model_Granule_To_Send
 *
 * Description:  simple wrapper around PL's callback
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_Granule_To_Send(
    RIO_CONTEXT_T         context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule   /* pointer to granule data structure */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_To_Send != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_To_Send(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context,
            granule
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    rio_Serial_Model_Granule_Received
 *
 * Description:  simple wrapper around PL's callback
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_Granule_Received(
    RIO_CONTEXT_T         context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule   /* pointer to granule data structure */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_Received != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Granule_Received(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context,
            granule
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    rio_Serial_Model_Character_Column_To_Send
 *
 * Description:  simple wrapper around PL's callback
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_Character_Column_To_Send(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_To_Send != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_To_Send(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context,
            character
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    rio_Serial_Model_Character_Column_Received
 *
 * Description:  simple wrapper around PL's callback
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_Character_Column_Received(
    RIO_CONTEXT_T           context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character /* pointer to character data structure */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_Received != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Character_Column_Received(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context,
            character
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    rio_Serial_Model_Codegroup_Column_To_Send
 *
 * Description:  simple wrapper around PL's callback
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_Codegroup_Column_To_Send(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Column_To_Send != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Column_To_Send(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context,
            codegroup
        );
    }

    return RIO_OK;
}


/***************************************************************************
 * Function :    rio_Serial_Model_Codegroup_Received
 *
 * Description:  simple wrapper around PL's callback
 *
 * Returns:      RIO_OK, RIO_PARAM_INVALID or RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/

static int rio_Serial_Model_Codegroup_Received(
    RIO_CONTEXT_T            context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup /* pointer to codegroup data structure */
)
{
    RIO_MODEL_DS_T *model_Data;

    /* check parameters */
    if( context == NULL )
        return RIO_PARAM_INVALID;
    else
        model_Data = (RIO_MODEL_DS_T*)context;
        
    /* check that the instance is the serial model */
    if ( model_Data->is_Parallel_Model )
        return RIO_ERROR;

    if( ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Received != NULL )
    {
        /* invoke callback from user-supplied ctray */
        
        return ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Codegroup_Received(
            ((RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T*)(model_Data->user_PL_Ctray))->rio_Serial_Hooks_Context,
            codegroup
        );
    }

    return RIO_OK;
}


/*****************************************************************************/

