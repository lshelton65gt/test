/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio_pllight/rio_pllight_parallel_model.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  RapidIO PL parallel model 
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "rio_pllight_parallel_model.h"
#include "rio_pllight_model_common.h"
#include "rio_pl_pllight_par_model.h"

#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_rx.h"
#include "rio_pl_tx.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"

#include "rio_32_txrx_model.h"
#include "rio_parallel_init.h"

/*
 * prototypes for static functions to be passed via ftray/ctray
 */

static int (*pl_Parallel_Model_Initialize_Routine)(
    RIO_HANDLE_T         handle,
    RIO_PL_PARALLEL_PARAM_SET_T   *param_Set
    );

static int plLight_Model_Initialize(
    RIO_HANDLE_T                handle,
    RIO_PLLIGHT_PARALLEL_PARAM_SET_T                 *param_Set
    );

/* 
 * External Physical Layer Light functions implementation
 */

/***************************************************************************
 * Function : RIO_PLLight_Parallel_Model_Create_Instance
 *
 * Description: Create new physical layer light instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_PLLight_Parallel_Model_Create_Instance(
    RIO_HANDLE_T *handle,
    RIO_PLLIGHT_MODEL_INST_PARAM_T *param_Set
    )
{
    RIO_PL_DS_T *pl_Instanse;
    RIO_MODEL_INST_PARAM_T  pl_Param_Set;

    pl_Param_Set.ext_Address_Support = 7;
    pl_Param_Set.is_Bridge = RIO_TRUE;
    pl_Param_Set.has_Memory = RIO_TRUE;
    pl_Param_Set.has_Processor = RIO_FALSE;
    pl_Param_Set.coh_Granule_Size_32 = RIO_TRUE; 
    pl_Param_Set.coh_Domain_Size = 2;

    pl_Param_Set.device_Identity = 0;
    pl_Param_Set.device_Vendor_Identity = 0;
    pl_Param_Set.device_Rev = 0;
    pl_Param_Set.device_Minor_Rev = 0;
    pl_Param_Set.device_Major_Rev = 0;
    pl_Param_Set.assy_Identity = 0;   
    pl_Param_Set.assy_Vendor_Identity = 0;
    pl_Param_Set.assy_Rev = 0;
   
    pl_Param_Set.entry_Extended_Features_Ptr = 0x100; 
    pl_Param_Set.next_Extended_Features_Ptr = 0x1000; 

    pl_Param_Set.source_Trx = 0xFFFFFFF4;
    pl_Param_Set.dest_Trx = 0xFFFFFFF4;

    pl_Param_Set.mbox1 = RIO_TRUE;
    pl_Param_Set.mbox2 = RIO_TRUE;
    pl_Param_Set.mbox3 = RIO_TRUE;
    pl_Param_Set.mbox4 = RIO_TRUE;
    pl_Param_Set.has_Doorbell = RIO_TRUE;

    pl_Param_Set.pl_Inbound_Buffer_Size  = param_Set->pl_Inbound_Buffer_Size;
    pl_Param_Set.pl_Outbound_Buffer_Size = param_Set->pl_Outbound_Buffer_Size;
    pl_Param_Set.ll_Inbound_Buffer_Size  = param_Set->ll_Inbound_Buffer_Size;
    pl_Param_Set.ll_Outbound_Buffer_Size = param_Set->ll_Outbound_Buffer_Size;

    /* GDA: Bug#43 */
    pl_Param_Set.gda_Tx_Illegal_Packet   = param_Set->gda_Tx_Illegal_Packet;
    pl_Param_Set.gda_Rx_Illegal_Packet   = param_Set->gda_Rx_Illegal_Packet;
    /* GDA: Bug#43 */

    /* GDA: Support for Bug#131 */
    pl_Param_Set.RIO_Link_Valid_To1_I 	 = param_Set->RIO_Link_Valid_To1_I;

    if (RIO_PL_PLLight_Parallel_Model_Create_Instance(handle, &pl_Param_Set) != RIO_OK)
        return RIO_ERROR;

    pl_Instanse = (RIO_PL_DS_T*) (*handle);
    pl_Instanse->is_PLLight_Model = 1;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PLLight_Parallel_Model_Get_Function_Tray
 *
 * Description: export physical layer light instance function tray
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_PLLight_Parallel_Model_Get_Function_Tray(
    RIO_PLLIGHT_PARALLEL_MODEL_FTRAY_T *ftray
    )
{
    RIO_PL_PARALLEL_MODEL_FTRAY_T pl_ftray;
	
    if ((RIO_PL_PLLight_Parallel_Model_Get_Function_Tray(&pl_ftray)) != RIO_OK)
        return RIO_ERROR;

    /* Copy data to ftray parameter */
    ftray->rio_PL_Clock_Edge            = pl_ftray.rio_PL_Clock_Edge;
    ftray->rio_PL_Model_Start_Reset     = pl_ftray.rio_PL_Model_Start_Reset;
    ftray->rio_PLLight_Model_Initialize      = plLight_Model_Initialize; 
	pl_Parallel_Model_Initialize_Routine = pl_ftray.rio_PL_Model_Initialize;
    ftray->rio_PL_Get_Pins              = pl_ftray.rio_PL_Get_Pins;

    ftray->rio_PL_Print_Version         = pl_ftray.rio_PL_Print_Version;

    ftray->rio_PL_Lost_Sync_Command     = pl_ftray.rio_PL_Lost_Sync_Command;

    ftray->rio_PL_Delete_Instance       = pl_ftray.rio_PL_Delete_Instance;
    ftray->rio_PL_Enable_Rnd_Idle_Generator = pl_ftray.rio_PL_Enable_Rnd_Idle_Generator;
    ftray->rio_PL_Force_EOP_Insertion = pl_ftray.rio_PL_Force_EOP_Insertion;

    ftray->rio_PL_Set_Retry_Generator = pl_ftray.rio_PL_Set_Retry_Generator;
    ftray->rio_PL_Set_PNACC_Generator = pl_ftray.rio_PL_Set_PNACC_Generator;
    ftray->rio_PL_Set_Stomp_Generator = pl_ftray.rio_PL_Set_Stomp_Generator;
    ftray->rio_PL_Set_LRQR_Generator  = pl_ftray.rio_PL_Set_LRQR_Generator;

    /* PL Light specific */
    ftray->rio_PLLight_Send_Packet = RIO_PLLight_Send_Packet;
    ftray->rio_PLLight_Send_Packet_Stream = RIO_PLLight_Send_Packet_Stream;
	ftray->rio_PLLight_Set_Port_Link_Time_Out = RIO_PLLight_Set_Port_Link_Time_Out;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_PLLight_Parallel_Model_Bind_Instance
 *
 * Description: bind physical layer instance 
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_PLLight_Parallel_Model_Bind_Instance(
    RIO_HANDLE_T handle,
    RIO_PLLIGHT_PARALLEL_MODEL_CALLBACK_TRAY_T *ctray
    )
{
    /* instanse handle */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    /*
     * check pointers and callbacks if OK load callbacks
     */
    if (instanse == NULL || ctray == NULL)
        return RIO_ERROR;

	if (ctray->rio_PL_Set_Pins == NULL ||
        ctray->rio_PLLight_Packet_Received == NULL ||
        ctray->rio_PLLight_Packet_Acknowledged == NULL ||
        ctray->rio_User_Msg == NULL 
        )
        return RIO_ERROR;


    /* check that the instance was created as parallel one */
    if ( !instanse->is_Parallel_Model )
        return RIO_ERROR;

    /* check that the instance was created as pllight */
    if ( !instanse->is_PLLight_Model )
        return RIO_ERROR;

    /* store callbacks tray into the layer's structure */
    instanse->ext_Interfaces.pl_Interface_Context   = ctray->pl_Interface_Context; 
                                                                            
    instanse->ext_Interfaces.rio_User_Msg           = ctray->rio_User_Msg;         
    instanse->ext_Interfaces.rio_Msg                = ctray->rio_Msg;              
                                                                            
	instanse->ext_Interfaces.rio_PL_Ack_Request     = NULL;    
    instanse->ext_Interfaces.rio_PL_Get_Req_Data    = NULL;  
    instanse->ext_Interfaces.rio_PL_Ack_Response    = NULL;  
    instanse->ext_Interfaces.rio_PL_Get_Resp_Data   = NULL; 
    instanse->ext_Interfaces.rio_PL_Req_Time_Out    = NULL;  
    instanse->ext_Interfaces.rio_PL_Remote_Request  = NULL;
    instanse->ext_Interfaces.rio_PL_Remote_Response = NULL;
    instanse->ext_Interfaces.extend_Reg_Read        = NULL;      
    instanse->ext_Interfaces.extend_Reg_Write       = NULL;     
    instanse->ext_Interfaces.extend_Reg_Context     = NULL;   
    instanse->ext_Interfaces.rio_Request_Received   = NULL; 
    instanse->ext_Interfaces.rio_Response_Received  = NULL;
    instanse->ext_Interfaces.rio_Packet_To_Send     = ctray->rio_Packet_To_Send;   
    instanse->ext_Interfaces.rio_Hooks_Context      = ctray->rio_Hooks_Context;      

	/* copy light specific callbacks */
	instanse->ext_Interfaces.rio_PLLight_Packet_Received     = ctray->rio_PLLight_Packet_Received;
    instanse->ext_Interfaces.rio_PLLight_Packet_Acknowledged    = ctray->rio_PLLight_Packet_Acknowledged;

    /* copy parallel specific callbacks */
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_PL_Set_Pins        
        = ctray->rio_PL_Set_Pins;       
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->lpep_Interface_Context 
        = ctray->lpep_Interface_Context;

    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Granule_Received   
        = ctray->rio_Granule_Received; 
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Symbol_To_Send     
        = ctray->rio_Symbol_To_Send;   
    ((RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T*)(instanse->ext_Specific_Interfaces))->rio_Hooks_Context      
        = ctray->rio_Hooks_Context;      


    /* mark the instance as the bound one */
    instanse->was_Bound = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : plLight_Model_Initialize
 *
 * Description: turn the instanse to work mode and initialize
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int plLight_Model_Initialize(
    RIO_HANDLE_T         handle,
    RIO_PLLIGHT_PARALLEL_PARAM_SET_T   *param_Set
    )
{
    RIO_PL_PARALLEL_PARAM_SET_T pl_Param_Set;
	unsigned int i;

    /* check pointers */
    if (handle == NULL || param_Set == NULL)
        return RIO_ERROR;

	pl_Param_Set.transp_Type = param_Set->transp_Type;

    pl_Param_Set.dev_ID = param_Set->dev_ID;

    /* values of corresponding registers */
    pl_Param_Set.lcshbar = 0x40000;
    pl_Param_Set.lcsbar = 0x40000;
    
    /*These fields define the initial value  of General Control CSR (offset 0x038 lower word(EF)*/
    pl_Param_Set.is_Host = RIO_TRUE;
    pl_Param_Set.is_Master_Enable = RIO_TRUE;
    pl_Param_Set.is_Discovered = RIO_TRUE;

    pl_Param_Set.ext_Address = param_Set->ext_Address;         /* extended address field */
    pl_Param_Set.ext_Address_16 = param_Set->ext_Address_16;      /* size of extended address - 16 or 32 bits */
    pl_Param_Set.ext_Mailbox_Support = RIO_TRUE; /* PE supports extended mailbox (msglen=0, mailbox_number=msgseg||mbox) */

    pl_Param_Set.lp_Ep_Is_8 = param_Set->lp_Ep_Is_8;
    pl_Param_Set.work_Mode_Is_8 = param_Set->work_Mode_Is_8;
    
    pl_Param_Set.input_Is_Enable = RIO_TRUE;
    pl_Param_Set.output_Is_Enable = RIO_TRUE;

    pl_Param_Set.num_Trainings = param_Set->num_Trainings;

    pl_Param_Set.requires_Window_Alignment = param_Set->requires_Window_Alignment;

    for (i = 0; i < RIO_PARALLEL_TRN_PTTN_ARRAY_SIZE; i++)
	{
	    pl_Param_Set.custom_Training_Pattern[i] = param_Set->custom_Training_Pattern[i];
	}
    
    pl_Param_Set.receive_Idle_Lim = param_Set->receive_Idle_Lim;

    pl_Param_Set.throttle_Idle_Lim = param_Set->throttle_Idle_Lim;

    pl_Param_Set.res_For_3_Out = 0;
    pl_Param_Set.res_For_2_Out = 0;
    pl_Param_Set.res_For_1_Out = 0;
    pl_Param_Set.res_For_3_In = 0;
    pl_Param_Set.res_For_2_In = 0;
    pl_Param_Set.res_For_1_In = 0;

    pl_Param_Set.pass_By_Prio = RIO_FALSE;

    pl_Param_Set.transmit_Flow_Control_Support = param_Set->transmit_Flow_Control_Support;

    pl_Param_Set.enable_Retry_Return_Code = RIO_TRUE;
    pl_Param_Set.enable_LRQ_Resending = param_Set->enable_LRQ_Resending;

		
    if (pl_Parallel_Model_Initialize_Routine(handle, &pl_Param_Set) != RIO_OK)
        return RIO_ERROR;

    return RIO_OK;
}


/****************************************************************************/
