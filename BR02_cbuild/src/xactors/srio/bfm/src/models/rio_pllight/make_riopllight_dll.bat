rem ########################################################################################################
rem 
rem       COPYRIGHT 2002 MOTOROLA, ALL RIGHTS RESERVED
rem 
rem       The code is the property of Motorola St.Petersburg Software Development
rem       and is Motorola Confidential Proprietary Information.
rem 
rem       The copyright notice above does not evidence any
rem       actual or intended publication of such source code.
rem 
rem Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio_pllight/make_riopllight_dll.bat,v $
rem Author:       $Author: knutson $
rem Locker:       $Locker:  $
rem State:        $State: Exp $
rem Revision:     $Revision: 1.1 $
rem 
rem History:      Use the CVS command log to display revision history
rem               information.
rem 
rem Description:  Batch file for building of RapidIO PL model under Windows NT.
rem 
rem Notes:
rem
rem ########################################################################################################


setlocal

rem
rem
rem
rem compiler specific

set COMPILER_PATH=%MSVCDir%\bin
set STANDARD_LIB=%MSVCDir%\Lib
set STANDARD_INC=%MSVCDir%\include
set path=%MSVCDir%\bin;%COMPILER_PATH%;%path%

rem path to sources
set SRC_PATH=\riosuite\src\riom

%COMPILER_PATH%\nmake -f makefile_nt.mak LIBPATH=%STANDARD_LIB% STANDARD_INC=%STANDARD_INC% RAPIDIO_PATH=%SRC_PATH% CFG="rio_pllight - Win32 Release"  

pause