#ifndef RIO_PLLIGHT_MODEL_COMMON_H
#define RIO_PLLIGHT_MODEL_COMMON_H

/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio_pllight/include/rio_pllight_model_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  file contains declaration of the common RapidIO PLLight model
*               function interface
*
* Notes:        
*
******************************************************************************/

#include "rio_types.h"
#include "rio_gran_type.h"
#include "rio_pl_pllight_model_common.h"

int RIO_PLLight_Send_Packet(
    RIO_HANDLE_T handle,
    RIO_PLLIGHT_PACKET_T *packet, 
    unsigned int packet_Num,
    unsigned int packet_Resend_Count);

int RIO_PLLight_Send_Packet_Stream(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T* packet_Buffer, 
    unsigned int packet_Size,
    RIO_CRC_CALCULATE_SETTING_T crc_setting,
    unsigned int packet_Num,
    unsigned int packet_Resend_Count,
    RIO_BOOL_T is_AckID_Valid,
    RIO_BOOL_T is_Padded);

int RIO_PLLight_Set_Port_Link_Time_Out(
    RIO_HANDLE_T handle,
    unsigned int timeout);

#endif /* RIO_PLLIGHT_MODEL_COMMON_H */


