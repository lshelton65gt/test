/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola SPSD 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/models/rio_pllight/rio_pllight_model_common.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*               to display revision history information.
*
* Description:  Physical layer source
*
* Notes: physical layer instantiates 32-Bit Transmitter/Receiver
*
******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

#include "rio_pllight_model_common.h"
#include "rio_pl_ds.h"
#include "rio_pl_errors.h"
#include "rio_pl_registers.h"
#include "rio_pl_rx.h"
#include "rio_pl_tx.h"
#include "rio_pl_out_buf.h"
#include "rio_pl_in_buf.h"


/***************************************************************************
 * Function : RIO_PLLight_Send_Packet
 *
 * Description: send new packet to the link
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PLLight_Send_Packet(
    RIO_HANDLE_T handle,
    RIO_PLLIGHT_PACKET_T *packet, 
    unsigned int packet_Num,
    unsigned int packet_Resend_Count)
{
    /* 
     * instanse handler, packet ftype and ttype, parameters for size dection
     * split implementation 
     */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    int result;  /*result of routine execution*/

    /* check pointers */
    if (instanse == NULL || packet == NULL)
        return RIO_ERROR;

    if (instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if ( instanse->is_Parallel_Model )
	{
        if (IS_NOT_A_BOOL(packet->s))
            return RIO_PARAM_INVALID;

        instanse->outbound_Buf.temp.entry.s = packet->s;

        if (IS_NOT_A_BOOL(packet->s_inv))
            return RIO_PARAM_INVALID;

        instanse->outbound_Buf.temp.entry.s_inv = packet->s_inv;
	}

    /* GDA: Bug#43 */
    if (instanse->inst_Param_Set.gda_Tx_Illegal_Packet && packet->dw_Size > 32)
	{
#ifndef _CARBON_SRIO_XTOR
	printf("\n<!Warning at Instanse %u> Trying to send more than 32 DWORDs\n",instanse->instance_Number);
#endif
	}
    /*if (packet->dw_Size >= RIO_PL_MAX_PAYLOAD_SIZE) */
    if ((packet->dw_Size > 32) && !(instanse->inst_Param_Set.gda_Tx_Illegal_Packet)) 
    {
#ifndef _CARBON_SRIO_XTOR
	printf("\n<!Error at Instanse %u> Trying to send more than 32 DWORDs\n",instanse->instance_Number);
#else
	char msg[250];
	sprintf(msg,"%s%u%s","\n<!Error at Instance",instanse->instance_Number,
			"> Trying to send more than 32 DWORDs\n");
	instanse->ext_Interfaces.rio_User_Msg(instanse->ext_Interfaces.rio_Msg, msg);
#endif
	return RIO_PARAM_INVALID;
    }
    /* GDA: Bug#43 */


    /*store request fields*/
    instanse->outbound_Buf.temp.entry.prio = packet->prio;
    instanse->outbound_Buf.temp.entry.tt = packet->tt;
    instanse->outbound_Buf.temp.entry.ftype = packet->ftype;
    instanse->outbound_Buf.temp.entry.tranp_Info = packet->transport_Info;
    instanse->outbound_Buf.temp.entry.ttype = packet->ttype;
    
    instanse->outbound_Buf.temp.entry.size = packet->rdwr_Size;
    
    instanse->outbound_Buf.temp.entry.src_TID = packet->srtr_TID;
    instanse->outbound_Buf.temp.entry.hop_Count = packet->hop_Count;

    instanse->outbound_Buf.temp.entry.config_Off = packet->address;

    instanse->outbound_Buf.temp.entry.wdptr = packet->wdptr;
    instanse->outbound_Buf.temp.entry.status = packet->rdwr_Size;
    instanse->outbound_Buf.temp.entry.doorbell_Info = packet->address;
    instanse->outbound_Buf.temp.entry.msglen = packet->ttype;
    instanse->outbound_Buf.temp.entry.letter = (packet->srtr_TID >> 6) & 3;
    instanse->outbound_Buf.temp.entry.mbox = (packet->srtr_TID >> 4) & 3;
    instanse->outbound_Buf.temp.entry.msgseg = (packet->srtr_TID) & 0xF;
    instanse->outbound_Buf.temp.entry.address = packet->address;
    instanse->outbound_Buf.temp.entry.extended_Address = packet->extended_Address;
    instanse->outbound_Buf.temp.entry.xamsbs = packet->xamsbs;
    instanse->outbound_Buf.temp.entry.ext_Address_Valid = (!(packet->ext_addr_Size == RIO_EXT_ADDR_NOT_VALID));
    instanse->outbound_Buf.temp.entry.ext_Address_16 = (packet->ext_addr_Size == RIO_EXT_ADDR_VALID_16);

    instanse->outbound_Buf.temp.entry.sec_ID = packet->gsm_Specific >> 8; 
    instanse->outbound_Buf.temp.entry.sec_TID = packet->gsm_Specific & 0xFF; 

    instanse->outbound_Buf.temp.entry.additional_Fields_Valid = RIO_TRUE; 

    /*additional fields that are used for the packet struct request*/
    instanse->outbound_Buf.temp.entry.rsrv_Phy_1 = packet->rsrv_Phy_1;
    instanse->outbound_Buf.temp.entry.rsrv_Phy_2 = packet->rsrv_Phy_2;
    instanse->outbound_Buf.temp.entry.rsrv_Ftype6 = packet->wdptr;
    instanse->outbound_Buf.temp.entry.rsrv_Ftype8 = packet->xamsbs;
    instanse->outbound_Buf.temp.entry.rsrv_Ftype10 = (packet->ttype << 4) | (packet->rdwr_Size << 4);
    instanse->outbound_Buf.temp.entry.is_PLLight_Model = RIO_TRUE;

    instanse->outbound_Buf.temp.entry.intermediate_Crc = 0xFFFF; /* value to be xored */
    instanse->outbound_Buf.temp.entry.is_Int_Crc_Valid = /* if it should be xored */
        (packet->crc_Valid_Setting == RIO_SET_BOTH_CRC_INCORRECT) || 
        (packet->crc_Valid_Setting == RIO_SET_LAST_CRC_CORRECT_ONLY);
    instanse->outbound_Buf.temp.entry.crc = 0xFFFF; /* value to be xored */
    instanse->outbound_Buf.temp.entry.is_Crc_Valid = /* if it should be xored */
        (packet->crc_Valid_Setting == RIO_SET_BOTH_CRC_INCORRECT) || 
        (packet->crc_Valid_Setting == RIO_SET_INTERMEDIATE_CRC_CORRECT_ONLY);

    /*user tag and payload*/
    RIO_PL_GET_TEMP(instanse).trx_Tag = packet_Num;
    RIO_PL_GET_TEMP(instanse).packet_Resend_Count = (int) packet_Resend_Count;

    RIO_PL_GET_TEMP(instanse).is_Stream = RIO_FALSE;
    RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;

    RIO_PL_GET_TEMP(instanse).payload_Size = packet->dw_Size;

    RIO_PL_GET_TEMP(instanse).payload_Source = (RIO_DW_T *) packet;

    /*store payload if necessary*/
    if (packet->data)
    {
        unsigned int i; /* loop counter */
        
        for (i = 0; i < packet->dw_Size; i++)
            RIO_PL_GET_TEMP(instanse).payload[i] = packet->data[i];
    }
	else
	{
        if (packet->dw_Size != 0)
	        return RIO_ERROR;

	}

    /* try to load */
    result = RIO_PL_Out_Load_Entry(instanse);
    
    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;

}

/***************************************************************************
 * Function : RIO_PLLight_Send_Packet_Stream
 *
 * Description: send new packet to the link
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PLLight_Send_Packet_Stream(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T* packet_Buffer, 
    unsigned int packet_Size,
    RIO_CRC_CALCULATE_SETTING_T crc_setting,
    unsigned int packet_Num,
	unsigned int packet_Resend_Count,
	RIO_BOOL_T is_AckID_Valid,
	RIO_BOOL_T is_Padded)
{
    /* 
     * instanse handler, packet ftype and ttype, parameters for size dection
     * split implementation 
     */
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;
    int result;  /*result of routine execution*/
    unsigned int i; /* loop counter */

    /* check pointers */
    if (instanse == NULL || packet_Buffer == NULL)
        return RIO_ERROR;

    if (instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if (packet_Size > RIO_PL_OUT_MAX_PACK_STREAM_LEN)
        return RIO_PARAM_INVALID;

    instanse->outbound_Buf.temp.entry.additional_Fields_Valid = RIO_TRUE; 

    instanse->outbound_Buf.temp.entry.is_Int_Crc_Valid = 
        (crc_setting == RIO_CALCULATE_LAST_CRC) || 
        (crc_setting == RIO_DONT_CALCULATE_CRC);
    instanse->outbound_Buf.temp.entry.is_Crc_Valid =
        (crc_setting == RIO_CALCULATE_INTERMEDIATE_CRC) || 
        (crc_setting == RIO_DONT_CALCULATE_CRC);

    /*user tag and payload*/
    RIO_PL_GET_TEMP(instanse).trx_Tag = packet_Num;
    RIO_PL_GET_TEMP(instanse).packet_Resend_Count = (int) packet_Resend_Count;
    RIO_PL_GET_TEMP(instanse).is_AckID_Valid = is_AckID_Valid;
    RIO_PL_GET_TEMP(instanse).is_Padded = is_Padded;

    RIO_PL_GET_TEMP(instanse).is_Stream = RIO_TRUE;
    RIO_PL_GET_TEMP(instanse).state = RIO_PL_OUT_READY;

    /* update ftype & ttype for special cases */
	RIO_PL_GET_TEMP(instanse).entry.ftype = packet_Buffer[1] & 0xF;
	RIO_PL_GET_TEMP(instanse).entry.tt = (packet_Buffer[1] >> 4) & 3;

	if (((packet_Buffer[1] >> 4) & 3) == RIO_PL_TRANSP_16)
       RIO_PL_GET_TEMP(instanse).entry.ttype = (packet_Buffer[4] >> 4) & 0xF;
    else
       RIO_PL_GET_TEMP(instanse).entry.ttype = (packet_Buffer[6] >> 4) & 0xF;


	RIO_PL_GET_TEMP(instanse).packet_Len = packet_Size;
    for (i = 0; i < packet_Size; i++)
        RIO_PL_GET_TEMP(instanse).packet_Stream[i] = packet_Buffer[i];


    /* try to load */
    result = RIO_PL_Out_Load_Entry(instanse);

    if (instanse->parameters_Set.enable_Retry_Return_Code == RIO_TRUE)
        return result;
    else
        return RIO_OK;
}



/***************************************************************************
 * Function : RIO_PLLight_Set_Port_Link_Time_Out
 *
 * Description: send new packet to the link
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_PLLight_Set_Port_Link_Time_Out(
    RIO_HANDLE_T handle,
    unsigned int timeout)
{
    RIO_PL_DS_T *instanse = (RIO_PL_DS_T*)handle;

    if (instanse == NULL)
        return RIO_ERROR;

    /* check that the instance was bound already */
    if ( !instanse->was_Bound )
        return RIO_ERROR;

	instanse->tx_Data.tx_Param_Set.lresp_Time_Out = timeout;
	RIO_PL_Write_Register(handle, 
		instanse->inst_Param_Set.entry_Extended_Features_Ptr + RIO_LINK_VALID_TO1_A,
        timeout, RIO_TRUE);

	return RIO_OK;
}
/****************************************************************************/
