#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/models/rio_txrxm/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds PL model shared library
#

include ../../../verilog_env/demo/debug.inc

CC              =   gcc
TARGET          =   librio_txrx.so

DEBUG		    =   -DNDEBUG

DEFINES         =   $(_DEBUG)

LIBS            =   -lc

CFLAGS          =   -c $(prof) $(DEFINES) -elf -ansi -pedantic -Wall -O3
LFLAGS          =   $(prof) -G -s -B symbolic
LFLAGS_AIX	=   $(prof) -G -s -bE:rio_txrx.exp
LFLAGS_LINUX    =   $(prof) -G -s -Bsymbolic

OBJS            =   ../../txrxm/txrxm.o
POBJS           =   ../../txrxm/ptxrxm.o
SOBJS           =   ../../txrxm/stxrxm.o


###############################################################################
# make default target
all :   $(TARGET)

# make target
$(TARGET) :
		cd ../../txrxm; make -f makefile.mak  txrx_model        
		ld  $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)
		
all_flexlm :
		cd ../../txrxm; make -f makefile.mak  all_flexlm         
		ld  $(LFLAGS) -lsocket $(OBJS) $(LIBS) -o $(TARGET)		
		#$(FLEXLM_PATH)/sun4_u5/lmstrip -r $(TARGET)		

sun_par :
		cd ../../txrxm; make -f makefile.mak  ptxrxm.o        
		ld  $(LFLAGS) $(POBJS) $(LIBS) -o $(TARGET)
        
sun_serial :
		cd ../../txrxm; make -f makefile.mak  stxrxm.o        
		ld  $(LFLAGS) -lsocket $(SOBJS) $(LIBS) -o $(TARGET)

#clean data
clean :		
		rm -f *.o
		rm -f $(TARGET)
		cd ../../txrxm; make -f makefile.mak clean
		
aix_all :	
		cd ../../txrxm; make -f makefile.mak  txrxm.o
		ld  $(LFLAGS_AIX) $(OBJS) $(LIBS) -o $(TARGET)

aix_par :
		cd ../../txrxm; make -f makefile.mak  ptxrxm.o        
		ld  $(LFLAGS_AIX) $(POBJS) $(LIBS) -o $(TARGET)


linux_all :	
		cd ../../txrxm; make -f makefile.mak  txrxm.o
		ld  $(LFLAGS_LINUX) $(OBJS) $(LIBS) -o $(TARGET)

linux_par :
		cd ../../txrxm; make -f makefile.mak  ptxrxm.o        
		ld  $(LFLAGS_LINUX) $(POBJS) $(LIBS) -o $(TARGET)


###############################################################################
             
