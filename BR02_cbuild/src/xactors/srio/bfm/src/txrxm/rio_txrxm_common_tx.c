/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/rio_txrxm_common_tx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model transmitter source
*
* Notes:        
*
******************************************************************************/
#include <stdlib.h>

#include "rio_txrxm_common_ds.h"


/* make maintanance stream */
static void txrxm_Out_Make_Packet_8(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/* make response stream */
static void txrxm_Out_Make_Packet_13(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );
/* make doorbell stream */
static void txrxm_Out_Make_Packet_10(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/* convert message to stream */
static void txrxm_Out_Make_Packet_11(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/* convert nonintervention packet to the stream */
static void txrxm_Out_Make_Packet_2(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/* convert write packet to the stream */
static void txrxm_Out_Make_Packet_5(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/* convert stream write packet to the stream */
static void txrxm_Out_Make_Packet_6(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/***************************************************************************
 * Function : txrxm_Out_Load_Packet_From_Struct
 *
 * Description: convert packet structure to the stream and load it to arbiter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void TxRxM_Out_Load_Packet_From_Struct(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    if (packet_Struct == NULL || arbiter == NULL)
        return;

    /*crc counting must include reserved bits from first byte - hadcoded number is unique*/
    arbiter->packet_Stream[0] = (RIO_UCHAR_T)(packet_Struct->entry.rsrv_Phy_2 & 0x03);

    switch(packet_Struct->entry.ftype)
    {
        /* maintanence packet request*/
        case RIO_MAINTENANCE:
            txrxm_Out_Make_Packet_8(packet_Struct, arbiter);
            break;

        /*response*/
        case RIO_RESPONCE:
            txrxm_Out_Make_Packet_13(packet_Struct, arbiter);
            break;

        /* doorbell request */
        case RIO_DOORBELL:
            txrxm_Out_Make_Packet_10(packet_Struct, arbiter);
            break;

        /* message request */
        case RIO_MESSAGE:
            txrxm_Out_Make_Packet_11(packet_Struct, arbiter);
            break;

        /* nonintervention request */
        case RIO_NONINTERV_REQUEST:
            txrxm_Out_Make_Packet_2(packet_Struct, arbiter);
            break;

        /* write class */
        case RIO_WRITE:
            txrxm_Out_Make_Packet_5(packet_Struct, arbiter);
            break;

        /* stream write */
        case RIO_STREAM_WRITE:
            txrxm_Out_Make_Packet_6(packet_Struct, arbiter);
            break;

    }

    /*set first byte, common to all packets - hardcoded numbers are unique and shall be changed only here*/
    arbiter->packet_Stream[0] |= (RIO_UCHAR_T)((packet_Struct->entry.ack_ID & 0x07) << 4);
    arbiter->packet_Stream[0] |= (RIO_UCHAR_T)((packet_Struct->entry.rsrv_Phy_1 & 0x01) << 3);
    arbiter->packet_Stream[0] |= (RIO_UCHAR_T)((1 & 0x01) << 2);
    
}



/***************************************************************************
 * Function : txrxm_Out_Make_Packet_8
 *
 * Description: convert maintanace packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void txrxm_Out_Make_Packet_8(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    unsigned int i = 0; /* byte counter */
    RIO_TXRXM_PACKET_REQ_STRUCT_T *entry; 
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 


    if (packet_Struct == NULL || arbiter == NULL)
        return;
    
    entry = &packet_Struct->entry;

    /* Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */

    /* make 0 byte */
    /*arbiter->packet_Stream[i = 0] = (RIO_UCHAR_T)0;*/
    
    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->transport_Info & 0x000000ff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->rdwr_Size & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->srtr_TID & 0xff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->hop_Count & 0xff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->config_Offset & 0x1fe000) >> 13);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->config_Offset & 0x001fe0) >> 5);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->config_Offset & 0x1f) << 3);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->wdptr & 0x01) << 2);
    arbiter->packet_Stream[i++] |= (RIO_UCHAR_T)(entry->rsrv_Ftype8 & 0x03);


    /* data payload */
    if (packet_Struct->payload_Size > 0)
    {
        unsigned int j = 0; /* loop counter */
        /* convert payload to the stream */
        for (j = 0; j < packet_Struct->payload_Size; j++, i += 8)
            RIO_TxRxM_Out_Conv_Dw_Char(packet_Struct->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
        RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
        i += 2;
/*        first_Crc_Pos = 82;*/
        first_Crc_Pos = 80;
    }

    RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    if (packet_Struct->entry.is_Crc_Valid == RIO_TRUE)
        crc = crc ^ packet_Struct->entry.crc;
    
    /* here we need to compute CRC code - if length is less than 80*/
    /*RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream, i);*/

    arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
    return;
}
/***************************************************************************
 * Function : txrxm_Out_Make_Packet_10
 *
 * Description: convert doorbell packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void txrxm_Out_Make_Packet_10(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    unsigned int i = 0; /* byte counter */
    RIO_TXRXM_PACKET_REQ_STRUCT_T *entry; 
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 


    if (packet_Struct == NULL || arbiter == NULL)
        return;
    
    entry = &packet_Struct->entry;

    /* Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */

    /* make 0 byte */
    /*arbiter->packet_Stream[i = 0] = (RIO_UCHAR_T)0;*/
    
    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->transport_Info & 0x000000ff);

    /*doorbell reserved field */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->rsrv_Ftype10 & 0xff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->srtr_TID & 0xff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->doorbell_Info & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->doorbell_Info & 0x00ff);


    /*check if any payload is in request (incorrect to protocol but shall be sent) */
    
     /* data payload */
    i++;
    if (packet_Struct->payload_Size > 0)
    {
        unsigned int j = 0; /* loop counter */
        /* convert payload to the stream */
        for (j = 0; j < packet_Struct->payload_Size; j++, i += 8)
            RIO_TxRxM_Out_Conv_Dw_Char(packet_Struct->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
        RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
        i += 2;
/*        first_Crc_Pos = 82;*/
        first_Crc_Pos = 80;
    }

    /* here we need to compute CRC code */
    RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    if (packet_Struct->entry.is_Crc_Valid == RIO_TRUE)
        crc = crc ^ packet_Struct->entry.crc;
    
    /* here we need to compute CRC code - if length is less than 80*/
    /*RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream, i);*/

    arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
    return;
}

/***************************************************************************
 * Function : txrxm_Out_Make_Packet_2
 *
 * Description: convert nonintervention packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void txrxm_Out_Make_Packet_2(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    unsigned int i = 0; /* byte counter */
    RIO_TXRXM_PACKET_REQ_STRUCT_T *entry; 
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 


    if (packet_Struct == NULL || arbiter == NULL)
        return;
    
    entry = &packet_Struct->entry;

    /* Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */

    /* make 0 byte */
    /*arbiter->packet_Stream[i = 0] = (RIO_UCHAR_T)0;*/
    

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->transport_Info & 0x000000ff);

    /* ttype & size*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->rdwr_Size & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->srtr_TID & 0xff);

    /* extended address*/
    if (entry->ext_Address_Valid == RIO_TRUE)
    {
        if (entry->ext_Address_16 == RIO_FALSE)
        {
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0xff000000) >> 24);
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x00ff0000) >> 16);
        }

        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x0000ff00) >> 8);
        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)(entry->extended_Address & 0x000000ff);

    }

    /* address */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x1fe00000) >> 21);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x001fe000) >> 13);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x00001fe0) >> 5);

    /* address, wdptr & xambs */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x0000001f) << 3);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)((entry->wdptr & 0x01) << 2);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)(entry->xamsbs & 0x03);
    
    
    
    /*check if any payload is in request (incorrect to protocol but shall be sent) */
    i++;        
     /* data payload */
    if (packet_Struct->payload_Size > 0)
    {
        unsigned int j = 0; /* loop counter */
        /* convert payload to the stream */
        for (j = 0; j < packet_Struct->payload_Size; j++, i += 8)
            RIO_TxRxM_Out_Conv_Dw_Char(packet_Struct->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
        RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
        i += 2;
/*        first_Crc_Pos = 82;*/
        first_Crc_Pos = 80;
    }

    /* here we need to compute CRC code */
    RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    if (packet_Struct->entry.is_Crc_Valid == RIO_TRUE)
        crc = crc ^ packet_Struct->entry.crc;
    

    arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
    return;
}
/***************************************************************************
 * Function : txrxm_Out_Make_Packet_13
 *
 * Description: convert 13 packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void txrxm_Out_Make_Packet_13(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 13 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_TXRXM_PACKET_REQ_STRUCT_T *entry; 
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 


    if (packet_Struct == NULL || arbiter == NULL)
        return;
    
    entry = &packet_Struct->entry;

    /* make 0 byte */
    /*arbiter->packet_Stream[i = 0] = (RIO_UCHAR_T)0;*/
    
    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->transport_Info & 0x000000ff);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->status & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->srtr_TID & 0xff);
    i++;

    /* data payload */
    if (packet_Struct->payload_Size > 0)
    {
        unsigned int j = 0; /* loop counter */
        /* convert payload to the stream */
        for (j = 0; j < packet_Struct->payload_Size; j++, i += 8)
            RIO_TxRxM_Out_Conv_Dw_Char(packet_Struct->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
        RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
        i += 2;

        first_Crc_Pos = 80;
    }

    /* here we need to compute CRC code */
    RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    if (packet_Struct->entry.is_Crc_Valid == RIO_TRUE)
        crc = crc ^ packet_Struct->entry.crc;
    
    arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
    return;
}

/***************************************************************************
 * Function : txrxm_Out_Make_Packet_11
 *
 * Description: convert message packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void txrxm_Out_Make_Packet_11(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 11 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_TXRXM_PACKET_REQ_STRUCT_T *entry; 
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 


    if (packet_Struct == NULL || arbiter == NULL)
        return;
    
    entry = &packet_Struct->entry;

    /* make 0 byte */
    /*arbiter->packet_Stream[i = 0] = (RIO_UCHAR_T)0;*/
    
    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->transport_Info & 0x000000ff);

    /* msglen & ssize*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->msglen & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ssize & 0x0f);

    /* letter, mbox & msgseg */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->letter & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->mbox & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->msgseg & 0x0f);
    i++;

    /* data payload */
    if (packet_Struct->payload_Size > 0)
    {
        unsigned int j = 0; /* loop counter */
        /* convert payload to the stream */
        for (j = 0; j < packet_Struct->payload_Size; j++, i += 8)
            RIO_TxRxM_Out_Conv_Dw_Char(packet_Struct->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
        RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
        i += 2;
/*        first_Crc_Pos = 82;*/
        first_Crc_Pos = 80;
    }

    /* here we need to compute CRC code */
    RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    if (packet_Struct->entry.is_Crc_Valid == RIO_TRUE)
        crc = crc ^ packet_Struct->entry.crc;
    
    /* here we need to compute CRC code - if length is less than 80*/
    /*RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream, i);*/

    arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
    return;

}

/***************************************************************************
 * Function : txrxm_Out_Make_Packet_5
 *
 * Description: convert write packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void txrxm_Out_Make_Packet_5(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 5 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_TXRXM_PACKET_REQ_STRUCT_T *entry; 
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 


    if (packet_Struct == NULL || arbiter == NULL)
        return;
    
    entry = &packet_Struct->entry;

    /* make 0 byte */
    /*arbiter->packet_Stream[i = 0] = (RIO_UCHAR_T)0;*/
    

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->transport_Info & 0x000000ff);

    /* ttype & size*/
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->ttype & 0x0f) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->rdwr_Size & 0x0f);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->srtr_TID & 0xff);

    /* extended address*/
    if (entry->ext_Address_Valid == RIO_TRUE)
    {
        if (entry->ext_Address_16 == RIO_FALSE)
        {
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0xff000000) >> 24);
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x00ff0000) >> 16);
        }

        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x0000ff00) >> 8);
        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)(entry->extended_Address & 0x000000ff);

    }

    /* address */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x1fe00000) >> 21);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x001fe000) >> 13);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x00001fe0) >> 5);

    /* address, wdptr & xambs */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x0000001f) << 3);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)((entry->wdptr & 0x01) << 2);
    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)(entry->xamsbs & 0x03);
    i++;

    /* data payload */
    if (packet_Struct->payload_Size > 0)
    {
        unsigned int j = 0; /* loop counter */
        /* convert payload to the stream */
        for (j = 0; j < packet_Struct->payload_Size; j++, i += 8)
            RIO_TxRxM_Out_Conv_Dw_Char(packet_Struct->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
        RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
        i += 2;
/*        first_Crc_Pos = 82;*/
        first_Crc_Pos = 80;
    }

    /* here we need to compute CRC code */
    RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    if (packet_Struct->entry.is_Crc_Valid == RIO_TRUE)
        crc = crc ^ packet_Struct->entry.crc;
    
    arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
    return;

}

/***************************************************************************
 * Function : txrxm_Out_Make_Packet_6
 *
 * Description: convert stream write packet structure to the stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void txrxm_Out_Make_Packet_6(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    /* 
     * this routine makes bytes steram to type 5 packet in according to the 
     * specification. Used offsets and masks are unique in general case for 
     * this type of packet, so all using constant are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    unsigned int i = 0; /* byte counter */
    RIO_TXRXM_PACKET_REQ_STRUCT_T *entry; 
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE; /* CRC code of a packet */
    unsigned int first_Crc_Pos = 0; 


    if (packet_Struct == NULL || arbiter == NULL)
        return;
    
    entry = &packet_Struct->entry;

    /* make 0 byte */
    /*arbiter->packet_Stream[i = 0] = (RIO_UCHAR_T)0;*/
    

    /* make 1 byte */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->prio & 0x03) << 6);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)((entry->tt & 0x03) << 4);
    arbiter->packet_Stream[i] |= (RIO_UCHAR_T)(entry->ftype & 0x0f);

    /* make 2 byte */
    if (entry->tt != 0)
    {    
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0xff000000) >> 24);
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x00ff0000) >> 16);
    }

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)((entry->transport_Info & 0x0000ff00) >> 8);

    /*  */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(entry->transport_Info & 0x000000ff);

    /* extended address*/
    if (entry->ext_Address_Valid == RIO_TRUE)
    {
        if (entry->ext_Address_16 == RIO_FALSE)
        {
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0xff000000) >> 24);
            arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x00ff0000) >> 16);
        }

        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->extended_Address & 0x0000ff00) >> 8);
        arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)(entry->extended_Address & 0x000000ff);

    }

    /* address */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x1fe00000) >> 21);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x001fe000) >> 13);
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x00001fe0) >> 5);

    /* address, wdptr & xambs */
    arbiter->packet_Stream[++i] = 
                (RIO_UCHAR_T)((entry->address & 0x0000001f) << 3);
 
    arbiter->packet_Stream[i] |=
                (RIO_UCHAR_T)((entry->rsrv_Ftype6 & 0x1) << 2);

    arbiter->packet_Stream[i] |= 
                (RIO_UCHAR_T)(entry->xamsbs & 0x03); 
    i++;

    /* data payload */
    if (packet_Struct->payload_Size > 0)
    {
        unsigned int j = 0; /* loop counter */
        /* convert payload to the stream */
        for (j = 0; j < packet_Struct->payload_Size; j++, i += 8)
            RIO_TxRxM_Out_Conv_Dw_Char(packet_Struct->payload + j, arbiter->packet_Stream + i);
    }

    /* check if we need to insert intermediate CRC */
    if (i > 80)
    {
        unsigned int k; /* loop counter*/
        RIO_UCHAR_T  tmp1 = arbiter->packet_Stream[80], 
            tmp2 = arbiter->packet_Stream[81],
            tmp3, tmp4; /* temporaries */

        for (k = 82; k < i; k += 2)
        {
            tmp3 = arbiter->packet_Stream[k];
            tmp4 = arbiter->packet_Stream[k + 1];

            arbiter->packet_Stream[k] = tmp1;
            arbiter->packet_Stream[k + 1] = tmp2;

            tmp1 = tmp3;
            tmp2 = tmp4;
        }

        arbiter->packet_Stream[i] = tmp1;
        arbiter->packet_Stream[i + 1] = tmp2;

        /* compute intermadiate CRC */
        RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, 80);
        arbiter->packet_Stream[80] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);
        arbiter->packet_Stream[81] = (RIO_UCHAR_T)(crc & 0x00ff);
        i += 2;
/*        first_Crc_Pos = 82;*/
        first_Crc_Pos = 80;
    }

    /* here we need to compute CRC code */
    RIO_TxRxM_Out_Compute_CRC(&crc, arbiter->packet_Stream + first_Crc_Pos, i - first_Crc_Pos);
    if (packet_Struct->entry.is_Crc_Valid == RIO_TRUE)
        crc = crc ^ packet_Struct->entry.crc;
    
    arbiter->packet_Stream[i] = (RIO_UCHAR_T)((crc & 0xff00) >> 8);

    /* */
    arbiter->packet_Stream[++i] = (RIO_UCHAR_T)(crc & 0x00ff);

    /* check if we need to pad packet */
    if (++i % 4)
    {
        arbiter->packet_Stream[i] = (RIO_UCHAR_T)0x00;
        arbiter->packet_Stream[++i] = (RIO_UCHAR_T)0x00;
        i++;
    }
        
    /* update arbiter data */
    arbiter->packet_Len = i;
    return;
}

/***************************************************************************
 * Function : RIO_TxRxM_Out_Conv_Dw_Char
 *
 * Description: convert double word to the stream
 *
 * Returns: none
 *
 * Notes: the only place for double-word to byte stream converting
 *
 **************************************************************************/
void RIO_TxRxM_Out_Conv_Dw_Char(RIO_DW_T *dw, RIO_UCHAR_T *stream)
{
    /* 
     * used offsets and masks are unique and defined by endian mode and
     * structure defenition - they can be used only here in the sense of
     * doubleword encoding
     */
    unsigned int i, shift;
    unsigned long mask;

    if (dw == NULL || stream == NULL)
        return;

    /* convert the first word*/
    for (i = 0, mask = 0xff000000, shift = 24; i < 4; i++, shift -= 8, mask >>= 8)
        stream[i] = (RIO_UCHAR_T)((dw->ms_Word & mask) >> shift);

    /* convert the second word */
    for (mask = 0xff000000, shift = 24; i < 8; i++, shift -= 8, mask >>= 8)
        stream[i] = (RIO_UCHAR_T)((dw->ls_Word & mask) >> shift);
}

/***************************************************************************
 * Function : RIO_TxRxM_Out_Compute_CRC
 *
 * Description: compute CRC code
 *
 * Returns: none
 *
 * Notes: the only place where CRC is counting (in according to the spec)
 *
 **************************************************************************/
void RIO_TxRxM_Out_Compute_CRC(unsigned short *crc, RIO_UCHAR_T stream[], unsigned int i_Max)
{
    /* 
     * this routine count CRC for byte stream  
     * Used offsets and masks are unique and SHALL be 
     * changed in case of specification changing only HERE!!!
     */
    
    /* previous control */
    RIO_UCHAR_T c0, c1, e0, e1; 
    /* CRC bits */
    RIO_UCHAR_T c00, c01, c02, c03, c04, c05, c06, c07, 
        c08, c09, c10, c11, c12, c13, c14, c15;
    /* data bits */
    RIO_UCHAR_T d00, d01, d02, d03, d04, d05, d06, d07, 
        d08, d09, d10, d11, d12, d13, d14, d15;

    /* loop control */
    unsigned int i;

    if (crc == NULL || stream == NULL)
        return;

    /* select controls */
    c0 = (RIO_UCHAR_T)((*crc & 0xff00) >> 8);
    c1 = (RIO_UCHAR_T)(*crc & 0x00ff); 

    for (i = 0; i < i_Max; i += 2)
    {
        /* c XOR d */
        e0 = (RIO_UCHAR_T)(c0 ^ stream[i]);
        e1 = (RIO_UCHAR_T)(c1 ^ stream[i + 1]);

        /* data bits selection*/
        d00 = (RIO_UCHAR_T)((e0 & 0x80) >> 7);
        d01 = (RIO_UCHAR_T)((e0 & 0x40) >> 6);
        d02 = (RIO_UCHAR_T)((e0 & 0x20) >> 5);
        d03 = (RIO_UCHAR_T)((e0 & 0x10) >> 4);
        d04 = (RIO_UCHAR_T)((e0 & 0x08) >> 3);
        d05 = (RIO_UCHAR_T)((e0 & 0x04) >> 2);
        d06 = (RIO_UCHAR_T)((e0 & 0x02) >> 1);
        d07 = (RIO_UCHAR_T)(e0 & 0x01);
        d08 = (RIO_UCHAR_T)((e1 & 0x80) >> 7);
        d09 = (RIO_UCHAR_T)((e1 & 0x40) >> 6);
        d10 = (RIO_UCHAR_T)((e1 & 0x20) >> 5);
        d11 = (RIO_UCHAR_T)((e1 & 0x10) >> 4);
        d12 = (RIO_UCHAR_T)((e1 & 0x08) >> 3);
        d13 = (RIO_UCHAR_T)((e1 & 0x04) >> 2);
        d14 = (RIO_UCHAR_T)((e1 & 0x02) >> 1);
        d15 = (RIO_UCHAR_T)(e1 & 0x01);

        /*  XOR equation network */
        c00 = (RIO_UCHAR_T)(d04 ^ d05 ^ d08 ^ d12);

        c01 = (RIO_UCHAR_T)(d05 ^ d06 ^ d09 ^ d13);

        c02 = (RIO_UCHAR_T)(d06 ^ d07 ^ d10 ^ d14);

        c03 = (RIO_UCHAR_T)(d00 ^ d07 ^ d08 ^ d11 ^ d15);

        c04 = (RIO_UCHAR_T)(d00 ^ d01 ^ d04 ^ d05 ^ d09);

        c05 = (RIO_UCHAR_T)(d01 ^ d02 ^ d05 ^ d06 ^ d10);

        c06 = (RIO_UCHAR_T)(d00 ^ d02 ^ d03 ^ d06 ^ d07 ^ d11);

        c07 = (RIO_UCHAR_T)(d00 ^ d01 ^ d03 ^ d04 ^ d07 ^ d08 ^ d12);

        c08 = (RIO_UCHAR_T)(d00 ^ d01 ^ d02 ^ d04 ^ d05 ^ d08 ^ d09 ^ d13);

        c09 = (RIO_UCHAR_T)(d01 ^ d02 ^ d03 ^ d05 ^ d06 ^ d09 ^ d10 ^ d14);

        c10 = (RIO_UCHAR_T)(d02 ^ d03 ^ d04 ^ d06 ^ d07 ^ d10 ^ d11 ^ d15);

        c11 = (RIO_UCHAR_T)(d00 ^ d03 ^ d07 ^ d11);

        c12 = (RIO_UCHAR_T)(d00 ^ d01 ^ d04 ^ d08 ^ d12);

        c13 = (RIO_UCHAR_T)(d01 ^ d02 ^ d05 ^ d09 ^ d13);

        c14 = (RIO_UCHAR_T)(d02 ^ d03 ^ d06 ^ d10 ^ d14);

        c15 = (RIO_UCHAR_T)(d03 ^ d04 ^ d07 ^ d11 ^ d15);

        /* compute CRC */
        c0 = (RIO_UCHAR_T)(c07 + (c06 << 1) + (c05 << 2) + (c04 << 3) + (c03 << 4) +
            (c02 << 5) + (c01 << 6) + (c00 << 7));

        c1 = (RIO_UCHAR_T)(c15 + (c14 << 1) + (c13 << 2) + (c12 << 3) + (c11 << 4) +
            (c10 << 5) + (c09 << 6) + (c08 << 7));
    }

    /* store calculated CRC */
    *crc = (unsigned short)(((c0 & 0xff) << 8) + (c1 & 0xff));
}

/***************************************************************************
 * Function : txrxm_Out_Load_Packet_From_Stream
 *
 * Description: convert packet structure to the stream and load it to arbiter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void TxRxM_Out_Load_Packet_From_Stream(
    RIO_TXRXM_OUT_PACKET_STREAM_T *packet_Stream, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    )
{
    unsigned int i;/*counter*/
                          
    /*check params*/
    if (packet_Stream == NULL || arbiter == NULL)
        return;

    /*copy stream*/
    for (i = 0; i < packet_Stream->packet_Len; i++)
        arbiter->packet_Stream[i] = packet_Stream->packet_Stream[i];
    
    /*set length*/
    arbiter->packet_Len = packet_Stream->packet_Len;
    return;
}
