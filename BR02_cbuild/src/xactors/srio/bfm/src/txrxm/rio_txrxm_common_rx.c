/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\riom\txrxm\rio_txrxm_common_rx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model receiver source
*
* Notes:        
*
******************************************************************************/
#include <stdlib.h>
#include "rio_txrxm_common_rx.h"

/***************************************************************************
 * Function : check_Reserved_Field
 *
 * Description: check if reserved field is equal to zero
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int check_Reserved_Field(RIO_TXRXM_PACKET_ARB_T *arbiter, unsigned int field_Pos_Start, unsigned int field_Size)
{
    unsigned int byte_Num, mask;
    
    /*hardcoded numbers are unique*/
    byte_Num = field_Pos_Start / RIO_TXRXM_BITS_IN_BYTE;
    /*start pos in byte*/
    field_Pos_Start = field_Pos_Start % RIO_TXRXM_BITS_IN_BYTE;
    while (field_Size > 0)
    {
        if (field_Size > (RIO_TXRXM_BITS_IN_BYTE - field_Pos_Start))
        {
            mask = ((char) (0x1) << (RIO_TXRXM_BITS_IN_BYTE - field_Pos_Start)) - 1;
            field_Size -= (RIO_TXRXM_BITS_IN_BYTE - field_Pos_Start);
            field_Pos_Start = 0;
            if ((arbiter->packet_Stream[byte_Num] & mask) != 0)
                return RIO_ERROR;
            byte_Num++;
        }
        else
        {
            mask = ( ( (char) (0x1) << (RIO_TXRXM_BITS_IN_BYTE - field_Pos_Start)) - 1) -
                (( (char) (0x1) << (RIO_TXRXM_BITS_IN_BYTE - (field_Pos_Start + field_Size))) - 1);
            field_Size = 0;
            if ((arbiter->packet_Stream[byte_Num] & mask) != 0)
                return RIO_ERROR;
        }
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : in_Stream_To_Dw
 *
 * Description: convert bytes to dws
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_In_Stream_To_Dw(RIO_DW_T *dws, RIO_UCHAR_T *stream, unsigned int size)
{
    unsigned int i; /* loop counter */

    if (stream == NULL)
        return;
    /* 
     * this code converts bytes stream to double-words stream
     * hardcore constants are use here aren't used in the same sence everywhere else
     */
    for (i = 0; i < size; i++)
    {
        dws[i].ms_Word = ((unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD] << (RIO_TXRXM_BITS_IN_BYTE * 3));
        dws[i].ms_Word |= ((unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD + 1] << (RIO_TXRXM_BITS_IN_BYTE * 2));
        dws[i].ms_Word |= ((unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD + 2] << RIO_TXRXM_BITS_IN_BYTE);
        dws[i].ms_Word |= (unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD + 3];

        dws[i].ls_Word = ((unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD + 4] << (RIO_TXRXM_BITS_IN_BYTE * 3));
        dws[i].ls_Word |= ((unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD + 5] << (RIO_TXRXM_BITS_IN_BYTE * 2));
        dws[i].ls_Word |= ((unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD + 6] << RIO_TXRXM_BITS_IN_BYTE);
        dws[i].ls_Word |= (unsigned long)stream[i * RIO_TXRXM_BYTE_CNT_IN_DWORD + 7];
    }
}

/***************************************************************************
 * Function : get_Ack_ID
 *
 * Description: get ack_ID
 *
 * Returns: packet ack_ID
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Get_Ack_ID_Parallel(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    /*hardcoded number is unique and shall be changed only here*/
    return ((arbiter->packet_Stream[RIO_TXRXM_ACKID_POS] & RIO_TXRXM_ACKID_MASK) >> 4);

}
/***************************************************************************
 * Function : get_Ftype
 *
 * Description: detect FTYPE
 *
 * Returns: FTYPE
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Get_Ftype(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return arbiter->packet_Stream[RIO_TXRXM_FTYPE_POS] & RIO_TXRXM_FTYPE_MASK;

}
/***************************************************************************
 * Function : get_Tt
 *
 * Description: detect TT field
 *
 * Returns: TT
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Get_Tt(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return (arbiter->packet_Stream[RIO_TXRXM_TT_POS] & RIO_TXRXM_TT_MASK) >> RIO_TXRXM_TT_SHIFT;
}

/***************************************************************************
 * Function : get_Ttype
 *
 * Description: detect TTYPE field
 *
 * Returns: ttype
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Get_Ttype(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    unsigned int tt;

    if (arbiter == NULL)
        return 0;

    tt = get_Tt(arbiter);
    if (tt == RIO_PL_TRANSP_16)
        return (arbiter->packet_Stream[RIO_TXRXM_TTYPE_POS_LOW] & RIO_TXRXM_TTYPE_MASK) >> RIO_TXRXM_TTYPE_SHIFT;
    else
        return (arbiter->packet_Stream[RIO_TXRXM_TTYPE_POS_HIGH] & RIO_TXRXM_TTYPE_MASK) >> RIO_TXRXM_TTYPE_SHIFT;
}

/***************************************************************************
 * Function : get_Prio
 *
 * Description: detect prio field
 *
 * Returns: prio
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Get_Prio(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    if (arbiter == NULL)
        return 0;

    return (arbiter->packet_Stream[RIO_TXRXM_PRIO_POS] & RIO_TXRXM_PRIO_MASK) >> RIO_TXRXM_PRIO_SHIFT;
}

/***************************************************************************
 * Function : get_Transp_Info
 *
 * Description: detect transport info field
 *
 * Returns: transport info
 *
 * Notes: none
 *
 **************************************************************************/
unsigned long RIO_TxRxM_Get_Transp_Info(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    unsigned int tt; /* size of transport information */

    if (arbiter == NULL)
        return 0;

    /*
     * this code obtaint transport info from the packet. It does it in 
     * according with the specification. And all hardcore numbers are unique.
     * Change code here if specification changes.
     */
    if ((tt = get_Tt(arbiter)) == RIO_PL_TRANSP_16)
        return (((unsigned long)arbiter->packet_Stream[RIO_TXRXM_TRANSP_TYPE_POS]) << RIO_TXRXM_BITS_IN_BYTE) |
            ((unsigned long)arbiter->packet_Stream[RIO_TXRXM_TRANSP_TYPE_POS + 1]);
    else
        return (((unsigned long)arbiter->packet_Stream[RIO_TXRXM_TRANSP_TYPE_POS]) << (RIO_TXRXM_BITS_IN_BYTE * 3)) |
            (((unsigned long)arbiter->packet_Stream[RIO_TXRXM_TRANSP_TYPE_POS + 1]) << (RIO_TXRXM_BITS_IN_BYTE * 2)) |
            (((unsigned long)arbiter->packet_Stream[RIO_TXRXM_TRANSP_TYPE_POS + 2]) << RIO_TXRXM_BITS_IN_BYTE) |
            ((unsigned long)arbiter->packet_Stream[RIO_TXRXM_TRANSP_TYPE_POS + 3]);
}

/***************************************************************************
 * Function : check_Ftype
 *
 * Description: check if ftype is correct to reserved value
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Check_Ftype(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    unsigned int ftype;

    if (arbiter == NULL)
        return RIO_ERROR;

    ftype = get_Ftype(arbiter); 

    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
        case RIO_NONINTERV_REQUEST:
        case RIO_WRITE:
        case RIO_STREAM_WRITE:
        case RIO_MAINTENANCE:
        case RIO_DOORBELL:
        case RIO_MESSAGE:
        case RIO_RESPONCE:
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
}

/***************************************************************************
 * Function : check_Tt
 *
 * Description: check if tt is correct to reserved values
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Check_Tt(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    unsigned int tt;

    if (arbiter == NULL)
        return RIO_ERROR;

    tt = get_Tt(arbiter); 

    switch (tt)
    {
        case RIO_PL_TRANSP_16:
        case RIO_PL_TRANSP_32:
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
}

/***************************************************************************
 * Function : check_Ttype
 *
 * Description: check if ttype is correct to reserved value
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Check_Ttype(RIO_TXRXM_PACKET_ARB_T *arbiter)
{
    unsigned int ftype, ttype;

    if (arbiter == NULL)
        return RIO_ERROR;

    ftype = get_Ftype(arbiter);
    ttype = get_Ttype(arbiter);

    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            if (ttype != RIO_INTERV_READ_OWNER &&
                ttype != RIO_INTERV_READ_TO_OWN_OWNER &&
                ttype != RIO_INTERV_IO_READ_OWNER)
                return RIO_ERROR;
            break;
        case RIO_NONINTERV_REQUEST:
            break;/*all transactions are allowed*/
        case RIO_WRITE:
            if (ttype != RIO_WRITE_CASTOUT &&
                ttype != RIO_WRITE_FLUSH_WITH_DATA &&
                ttype != RIO_WRITE_NWRITE &&
                ttype != RIO_WRITE_NWRITE_R &&
                ttype != RIO_WRITE_ATOMIC_TSWAP &&
		/* GDA: Added for Bug#133. Included support for TType C for Type 5 packet */
                ttype != RIO_WRITE_ATOMIC_SWAP)
                return RIO_ERROR;
            break;
        case RIO_STREAM_WRITE:
            break; /*there is no transaction field*/
        case RIO_MAINTENANCE:
            if (ttype != RIO_MAINTENANCE_CONF_READ &&
                ttype != RIO_MAINTENANCE_CONF_WRITE &&
                ttype != RIO_MAINTENANCE_READ_RESPONCE &&
                ttype != RIO_MAINTENANCE_WRITE_RESPONCE &&
                ttype != RIO_MAINTENANCE_PORT_WRITE)
                return RIO_ERROR;
            break;
        case RIO_DOORBELL:
        case RIO_MESSAGE:
            break;/*there is no transaction field*/
        case RIO_RESPONCE:
            if (ttype != RIO_RESPONCE_WO_DATA &&
                ttype != RIO_RESPONCE_MESSAGE &&
                ttype != RIO_RESPONCE_WITH_DATA)
                return RIO_ERROR;
            break;
        default:
            return RIO_ERROR;
    }
    return RIO_OK;
}


/***************************************************************************
 * Function : check_CRC
 *
 * Description: check if CRC is correct
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Check_CRC(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16,
    unsigned short *expected_Crc)
{
    RIO_UCHAR_T first_Byte;
    unsigned short crc = RIO_TXRXM_INIT_CRC_VALUE, src_Crc;
    unsigned int first_Crc_Pos, start_For_Cnt = 0;

    if (arbiter == NULL)
        return RIO_ERROR;

    /* CRC checking - hardcoded number is unique*/
    first_Byte = arbiter->packet_Stream[0];
    arbiter->packet_Stream[0] = first_Byte & 0x03;

    /* check if CRC was countered twice */
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        RIO_TxRxM_Out_Compute_CRC(&crc, 
            arbiter->packet_Stream, RIO_TXRXM_INT_CRC_START);
        src_Crc = arbiter->packet_Stream[RIO_TXRXM_INT_CRC_START];
        src_Crc <<= RIO_TXRXM_BITS_IN_BYTE;
        src_Crc |= (unsigned short)(arbiter->packet_Stream[RIO_TXRXM_INT_CRC_START + 1]);

        start_For_Cnt = RIO_TXRXM_INT_CRC_START;

        if (src_Crc != crc)
        {
            *expected_Crc = crc;
            /*set first byte value back*/
            arbiter->packet_Stream[0] = first_Byte;
            return RIO_ERROR;
        }
    }
    /* here we check the next CRC */
    first_Crc_Pos = RIO_TxRxM_Get_CRC_Start(arbiter, is_Ext_Address_Valid, is_Ext_Address_16);

    /* check if CRC is 16-bit aligned */
    if (first_Crc_Pos % RIO_TXRXM_BYTES_IN_HALF_WORD)
        return RIO_ERROR;

    if (first_Crc_Pos <= start_For_Cnt)
    {
        /*set first byte value back*/
        arbiter->packet_Stream[0] = first_Byte;
        return RIO_ERROR;
    }

    /* count CRC */
    RIO_TxRxM_Out_Compute_CRC(&crc, 
        arbiter->packet_Stream + start_For_Cnt, 
        first_Crc_Pos - start_For_Cnt);
    src_Crc = arbiter->packet_Stream[first_Crc_Pos];
    src_Crc <<= RIO_TXRXM_BITS_IN_BYTE;
    src_Crc |= arbiter->packet_Stream[first_Crc_Pos + 1];

    /* check and return an error if incorrect*/
    if (src_Crc != crc)
    {
        /*set first byte value back*/
        arbiter->packet_Stream[0] = first_Byte;
        *expected_Crc = crc;
        return RIO_ERROR;
    }

    /*set first byte value back*/
    arbiter->packet_Stream[0] = first_Byte;
    return RIO_OK;

}

/***************************************************************************
 * Function : get_CRC_Start
 *
 * Description: returns the start position of the end CRC in the stream 
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Get_CRC_Start(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    unsigned int ftype, tt;

    ftype = get_Ftype(arbiter);
   /* ttype = get_Ttype(arbiter);*/
    tt = get_Tt(arbiter);

    /*
     * this logic depends on: packet size, existion extended address field
     * and its length, packet type. All used hardcore numbers are unique for
     * each case (each packet types and other conditions). In case of changing 
     * packet format make all changing here.
     */
    switch (ftype)
    {
        /* maintanence */
        case RIO_MAINTENANCE:
            if (tt == RIO_PL_TRANSP_16)
                return arbiter->cur_Pos - 2;
            else
                return arbiter->cur_Pos - 4;
        
        /* doorbell request */
        case RIO_DOORBELL:
            if (tt == RIO_PL_TRANSP_16)
                return arbiter->cur_Pos - 4;
            else
                return arbiter->cur_Pos - 2;
        
        /* response packet */
        case RIO_RESPONCE:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 4;
                else
                    return arbiter->cur_Pos - 2;
            }
            else
            {
                if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 2;
                else
                    return arbiter->cur_Pos - 4;
            }

        /* message request */
        case RIO_MESSAGE:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 4;
                else
                    return arbiter->cur_Pos - 2;
            }
            else
            {
                if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
                    return arbiter->cur_Pos - 2;
                else 
                    return arbiter->cur_Pos - 4;
            }

        /* nonintervention request */
        case RIO_NONINTERV_REQUEST:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (is_Ext_Address_Valid == RIO_TRUE)
                {
                    if (is_Ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 4;
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                    return arbiter->cur_Pos - 2;
            }
            else
            {
                if (is_Ext_Address_Valid == RIO_TRUE)
                {
                    if (is_Ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 2;
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                    return arbiter->cur_Pos - 4;
            }

        /* write class */
        case RIO_WRITE:
            if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
            }
            else
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
            }

        /* stream write */
        case RIO_STREAM_WRITE:
            if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
            }
            else
            {
                if (tt == RIO_PL_TRANSP_16)
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 2;
                        else
                            return arbiter->cur_Pos - 4;
                    }
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                {
                    if (is_Ext_Address_Valid == RIO_TRUE)
                    {
                        if (is_Ext_Address_16 == RIO_TRUE)
                            return arbiter->cur_Pos - 4;
                        else
                            return arbiter->cur_Pos - 2;
                    }
                    else
                        return arbiter->cur_Pos - 2;
                }
            }

            /* intervention request */
        case RIO_INTERV_REQUEST:
            if (tt == RIO_PL_TRANSP_16)
            {
                if (is_Ext_Address_Valid == RIO_TRUE)
                {
                    if (is_Ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 2;
                    else
                        return arbiter->cur_Pos - 4;
                }
                else
                    return arbiter->cur_Pos - 4;
            }
            else
            {
                if (is_Ext_Address_Valid == RIO_TRUE)
                {
                    if (is_Ext_Address_16 == RIO_TRUE)
                        return arbiter->cur_Pos - 4;
                    else
                        return arbiter->cur_Pos - 2;
                }
                else
                    return arbiter->cur_Pos - 2;
            }

        /* other not-implemented types */
        default :
            return 1;
    }
}

/***************************************************************************
 * Function : check_Header_Size
 *
 * Description: check if header length is correct
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Check_Header_Size(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    /*length of headers*/
    unsigned int required_Length;
    if (arbiter == NULL)
        return RIO_ERROR;

    /*get required header size in bits */
    required_Length = RIO_TxRxM_Get_Header_Size(arbiter, 
        is_Ext_Address_Valid, is_Ext_Address_16) + RIO_TXRXM_CRC_FIELD_SIZE;
    /*convert from bit count to byte count*/
    required_Length = (unsigned int)(required_Length / RIO_TXRXM_BITS_IN_BYTE) + 
        ((required_Length % RIO_TXRXM_BITS_IN_BYTE) == 0 ? 0 : 1); 

    /* arbiter data is always aligned to 32 bit boundary,
        so required_Length align is unnecessary*/
    if (arbiter->cur_Pos < required_Length)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : get_Header_Size
 *
 * Description: calculate required packet header size depending on ftype,tt 
 *              and internal parameters value
 *
 * Returns: header size in bits   
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Get_Header_Size(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    /*temporary*/
    unsigned int tt, ftype;
    /*length of headers and required length of fields - in bits*/
    unsigned int required_Length, ext_Addr_Size, tr_Info_Size;
    if (arbiter == NULL)
        return 0;

    ftype = get_Ftype(arbiter);
    if ((tt = get_Tt(arbiter)) == RIO_PL_TRANSP_16)
        tr_Info_Size = RIO_TXRXM_TR_INFO_SIZE_16; /*16 bit transport info is supposed*/
    else
        tr_Info_Size = RIO_TXRXM_TR_INFO_SIZE_32; /*32 bit transport info is supposed*/

    if (is_Ext_Address_Valid == RIO_TRUE)
    {
        if(is_Ext_Address_16 == RIO_TRUE)
            ext_Addr_Size = RIO_TXRXM_EXT_ADDR_SIZE_16; /*16 bit extended address is supposed*/
        else
            ext_Addr_Size = RIO_TXRXM_EXT_ADDR_SIZE_32; /*32 bit extended address is supposed*/
    }
    else
        ext_Addr_Size = 0; /*no extended address is supposed*/
    
    /*necessary part of all packets*/
    required_Length = RIO_TXRXM_FIRST_16_BIT_SIZE + tr_Info_Size;

    /*add size of packet depended headers*/
    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            required_Length += ext_Addr_Size + RIO_TXRXM_PACK_1_HEADER_SIZE;
            break;

        case RIO_NONINTERV_REQUEST:
            required_Length += ext_Addr_Size + RIO_TXRXM_PACK_2_HEADER_SIZE;
            break;
        case RIO_WRITE:
            required_Length += ext_Addr_Size + RIO_TXRXM_PACK_5_HEADER_SIZE;
            break;
        case RIO_STREAM_WRITE:
            required_Length += ext_Addr_Size + RIO_TXRXM_PACK_6_HEADER_SIZE;
            break;
        case RIO_MAINTENANCE:
            required_Length += RIO_TXRXM_PACK_8_HEADER_SIZE;
            break;
        case RIO_DOORBELL:
            required_Length += RIO_TXRXM_PACK_10_HEADER_SIZE;
            break;

        case RIO_MESSAGE:
            required_Length += RIO_TXRXM_PACK_11_HEADER_SIZE;
            break;

        case RIO_RESPONCE:
            required_Length += RIO_TXRXM_PACK_13_HEADER_SIZE;
            break;
        default:
            break; /*impossible*/
    }
    
    return required_Length;

}

/***************************************************************************
 * Function : check_Payload_Align
 *
 * Description: check if payload is aligned to dw boundary
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Check_Payload_Align(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    /*length of headers*/
    unsigned int required_Length, first_Crc_Pos;
    if (arbiter == NULL)
        return RIO_ERROR;

    /*get first CRC Pos - packet end pointer without CRC and padding*/
    first_Crc_Pos = RIO_TxRxM_Get_CRC_Start(arbiter, is_Ext_Address_Valid, is_Ext_Address_16);
    /*get required header size in bits */
    required_Length = RIO_TxRxM_Get_Header_Size(arbiter, is_Ext_Address_Valid, is_Ext_Address_16);
    /*if CRC is single, decrease header lenght to it -neccesary to calculate payload size*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
        required_Length += RIO_TXRXM_CRC_FIELD_SIZE;  
    

    /*convert from bit count to byte count*/
    required_Length = (unsigned int)(required_Length / RIO_TXRXM_BITS_IN_BYTE) + 
        ((required_Length % RIO_TXRXM_BITS_IN_BYTE) == 0 ? 0 : 1); 

    /* calculate payload size and compare it to DW boundary*/
    if (((first_Crc_Pos - required_Length) % RIO_TXRXM_BYTE_CNT_IN_DWORD) != 0)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : check_Reserved_Fields
 *
 * Description: check if reserved fields are equal to zero
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
unsigned int RIO_TxRxM_Check_Reserved_Fields(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16,
    RIO_BOOL_T is_Parallel)
{
    /*all used numbers are individualy and can be changed only in ths part of spec*/
    /*length of headers*/
    unsigned int ftype, head_Size, field_Pos_Start;
    int field_Size;

    if (arbiter == NULL)
        return RIO_ERROR;

    /*hardcoded numbers are unique and corresponds to RIO spec. Shall be changed only here*/
    /*check physical fields, common to all, in first byte */
    /*first reserved field*/
    if (is_Parallel == RIO_TRUE)
    {
        if ((arbiter->packet_Stream[0] & 0x08) != 0)
            return RIO_ERROR;
        /*second reserved bit*/
        if ((arbiter->packet_Stream[0] & 0x03) != 0)
            return RIO_ERROR;
    }
    else  /*if serial*/
    {
        if ((arbiter->packet_Stream[0] & 0x07) != 0)
            return RIO_ERROR;
    }
    /*check reserved fields depending on ftype*/
        /*get required header size in bits */
    head_Size = RIO_TxRxM_Get_Header_Size(arbiter,
        is_Ext_Address_Valid,
        is_Ext_Address_16);
    ftype = get_Ftype(arbiter);

    /*here the the place of reserved field will be detected 
    by calculating it from the end of appropriate packet header*/
    switch (ftype)
    {
        case RIO_INTERV_REQUEST:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_NONINTERV_REQUEST:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_WRITE:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_STREAM_WRITE:
            /*pos in bits count*/
            field_Pos_Start = head_Size - 3;
            field_Size = RIO_TXRXM_PACK_6_RSRV_FIELD_SIZE;
            return check_Reserved_Field(arbiter, field_Pos_Start, field_Size);
        
        case RIO_MAINTENANCE:
            if (RIO_TxRxM_Is_Req(ftype, get_Ttype(arbiter)) == RIO_TRUE)
            {
                field_Pos_Start = head_Size - 2;
                field_Size = RIO_TXRXM_PACK_8_REQ_RSRV_FIELD_SIZE;
            }
            else
            {
                field_Pos_Start = head_Size - 24;
                field_Size = RIO_TXRXM_PACK_8_RESP_RSRV_FIELD_SIZE;
            }
            return check_Reserved_Field(arbiter, field_Pos_Start, field_Size);
        
        case RIO_DOORBELL:
            field_Pos_Start = head_Size - 32;
            field_Size = RIO_TXRXM_PACK_10_RSRV_FIELD_SIZE;
            return check_Reserved_Field(arbiter, field_Pos_Start, field_Size);

        case RIO_MESSAGE:
            return RIO_OK; /*no reserved bits*/
        
        case RIO_RESPONCE:
            return RIO_OK;
        default:
            return RIO_ERROR;
    }
    return RIO_OK;
}


/***************************************************************************
 * Function : is_Req
 *
 * Description: detect if ttype is request ttype
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
RIO_BOOL_T RIO_TxRxM_Is_Req(unsigned int ftype, unsigned int ttype)
{
    switch(ftype)
    {
        case RIO_MAINTENANCE:
            if (ttype == RIO_MAINTENANCE_CONF_READ ||
                ttype == RIO_MAINTENANCE_CONF_WRITE ||
                ttype == RIO_MAINTENANCE_PORT_WRITE)
                return RIO_TRUE;
            break;

        /* one break because of one result */
        case RIO_DOORBELL:
        case RIO_MESSAGE:
        case RIO_NONINTERV_REQUEST:
        case RIO_WRITE:
        case RIO_STREAM_WRITE:
        case RIO_INTERV_REQUEST:
            return RIO_TRUE;

        default:
            ;
    }

    return RIO_FALSE;
}


/***************************************************************************
 * Function : prod_Req_8
 *
 * Description: parse maintenance request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Prod_Req_8(
    RIO_TXRXM_PACKET_ARB_T  *arbiter,
    RIO_REMOTE_REQUEST_T *req,
    RIO_UCHAR_T          *hop_Count)
{
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (req == NULL ||
        hop_Count == NULL)
        return;

    /* locate first unparsed byte */
    if (req->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH;

    /* complete fields */
    req->header->ssize = req->header->rdsize = 
        (RIO_UCHAR_T)(arbiter->packet_Stream[i] & RIO_TXRXM_SIZE_MASK);
    req->header->target_TID = (RIO_UCHAR_T)(arbiter->packet_Stream[++i]);
    *hop_Count = (RIO_UCHAR_T)(arbiter->packet_Stream[++i]);
    req->header->offset = ((unsigned long)arbiter->packet_Stream[++i]) << 13;
    req->header->offset |= ((unsigned long)arbiter->packet_Stream[++i]) << 5;
    req->header->offset |= ((unsigned long)arbiter->packet_Stream[++i] & 0xf8) >> 3;
    req->header->wdptr = (RIO_UCHAR_T)(((unsigned long)arbiter->packet_Stream[i++] & 0x04) >> 2);

    /* shift offset again */
    req->header->offset <<= 3;
    
    /* payload is always */
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            req->dw_Num = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        req->dw_Num = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(req->data, arbiter->packet_Stream + i, req->dw_Num);
}

/***************************************************************************
 * Function : prod_Req_10
 *
 * Description: parse doorbell request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Prod_Req_10(RIO_TXRXM_PACKET_ARB_T  *arbiter, RIO_REMOTE_REQUEST_T *req)
{
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (req == NULL)
        return;

    /* locate first unparsed byte */
    if (req->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW + 1;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH + 1;

    
    /* complete fields */
    req->header->ttype = 0;
    req->header->target_TID = (RIO_UCHAR_T)arbiter->packet_Stream[i];
    req->header->doorbell_Info = ((unsigned int)arbiter->packet_Stream[++i]) << RIO_TXRXM_BITS_IN_BYTE;
    req->header->doorbell_Info |= (unsigned int)arbiter->packet_Stream[++i];
    i++;

    /* payload is always checking and passing*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            req->dw_Num = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        req->dw_Num = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(req->data, arbiter->packet_Stream + i, req->dw_Num);
}

/***************************************************************************
 * Function : prod_Req_11
 *
 * Description: parse message request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Prod_Req_11(RIO_TXRXM_PACKET_ARB_T  *arbiter, RIO_REMOTE_REQUEST_T *req)
{
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (req == NULL)
        return;

    /* locate first unparsed byte */
    if (req->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH;

    /* complete fields */
    req->header->ttype = 0;/*no ttype field in packet format*/
    req->header->ssize = req->header->rdsize = 
        (RIO_UCHAR_T)(arbiter->packet_Stream[i] & RIO_TXRXM_SIZE_MASK);
    req->header->msglen = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & 0xf0) >> 4);

    /* letter, mbox & msgseg */
    req->header->letter = (RIO_UCHAR_T)((arbiter->packet_Stream[++i] & 0xc0) >> 6);
    req->header->mbox = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & 0x30) >> 4);
    req->header->msgseg = (RIO_UCHAR_T)(arbiter->packet_Stream[i] & 0x0f);

    req->header->target_TID = (RIO_UCHAR_T)(arbiter->packet_Stream[i++]);


   
    /* payload is always checking and passing*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            req->dw_Num = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        req->dw_Num = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(req->data, arbiter->packet_Stream + i, req->dw_Num);
}

/***************************************************************************
 * Function : prod_Req_2
 *
 * Description: parse noninterventing request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Prod_Req_2(
    RIO_TXRXM_PACKET_ARB_T  *arbiter, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (arbiter == NULL ||
        req == NULL)
        return;

    /* locate first unparsed byte */
    if (req->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH;

    /* complete fields */
    req->header->ttype = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & RIO_TXRXM_TTYPE_MASK) >> RIO_TXRXM_TTYPE_SHIFT);
    req->header->ssize = req->header->rdsize = 
        (RIO_UCHAR_T)(arbiter->packet_Stream[i] & RIO_TXRXM_SIZE_MASK);
    /* target_TID */
    req->header->target_TID = (RIO_UCHAR_T) arbiter->packet_Stream[++i];

    /* extended address */
    req->header->extended_Address = 0;

    if (is_Ext_Address_Valid == RIO_TRUE)
    {
        if (is_Ext_Address_16 == RIO_FALSE)
        {
            req->header->extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 3);
            req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 2);
        }
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_TXRXM_BITS_IN_BYTE;
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    req->header->address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    req->header->address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    req->header->address <<= 3;

    req->header->wdptr = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & 0x04) >> 2);
    req->header->xamsbs = (RIO_UCHAR_T)(arbiter->packet_Stream[i] & 0x03);
   
    /* payload is never */
    /* req->header->dw_Num = 0; */

    i++;
    /* payload is always checking and passing*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            req->dw_Num = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        req->dw_Num = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(req->data, arbiter->packet_Stream + i, req->dw_Num);


}

/***************************************************************************
 * Function : prod_Req_5
 *
 * Description: parse write request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Prod_Req_5(
    RIO_TXRXM_PACKET_ARB_T  *arbiter, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (arbiter == NULL ||
        req == NULL)
        return;

    /* locate first unparsed byte */
    if (req->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH;


    /* complete fields */
    req->header->ttype = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & RIO_TXRXM_TTYPE_MASK) >> RIO_TXRXM_TTYPE_SHIFT);
    req->header->ssize = req->header->rdsize = 
        (RIO_UCHAR_T)(arbiter->packet_Stream[i] & RIO_TXRXM_SIZE_MASK);
    /* target_TID */
    req->header->target_TID = (RIO_UCHAR_T)arbiter->packet_Stream[++i];

    /* extended address */
    req->header->extended_Address = 0;

    if (is_Ext_Address_Valid == RIO_TRUE)
    {
        if (is_Ext_Address_16 == RIO_FALSE)
        {
            req->header->extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 3);
            req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 2);
        }
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_TXRXM_BITS_IN_BYTE;
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    req->header->address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    req->header->address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    req->header->address <<= 3;

    req->header->wdptr = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & 0x04) >> 2);
    req->header->xamsbs = (RIO_UCHAR_T)(arbiter->packet_Stream[i] & 0x03);
    i++;
   
    /* payload is always checking and passing*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            req->dw_Num = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        req->dw_Num = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(req->data, arbiter->packet_Stream + i, req->dw_Num);
}

/***************************************************************************
 * Function : prod_Req_6
 *
 * Description: parse streaming write request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Prod_Req_6(
    RIO_TXRXM_PACKET_ARB_T  *arbiter,
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (arbiter == NULL ||
        req == NULL)
        return;

    /* locate first unparsed byte */
    if (req->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW - 1;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH - 1;

    /* complete fields */
    req->header->ttype = 0;

    /* extended address */
    req->header->extended_Address = 0;

    if (is_Ext_Address_Valid == RIO_TRUE)
    {
        if (is_Ext_Address_16 == RIO_FALSE)
        {
            req->header->extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 3);
            req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 2);
        }
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_TXRXM_BITS_IN_BYTE;
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    req->header->address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    req->header->address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    req->header->address <<= 3;

    req->header->wdptr = (RIO_UCHAR_T)(arbiter->packet_Stream[i] & 0x04) >> 2;
    req->header->xamsbs = (RIO_UCHAR_T)arbiter->packet_Stream[i] & 0x03;
    i++;
   
    /* payload is always checking and passing*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            req->dw_Num = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        req->dw_Num = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(req->data, arbiter->packet_Stream + i, req->dw_Num);
}

/***************************************************************************
 * Function : prod_Req_1
 *
 * Description: parse interventing request
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void prod_Req_1(
    RIO_TXRXM_PACKET_ARB_T  *arbiter,
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16)
{
    /* index in the steram */
    unsigned int i;

    /* 
     * this function strips inbound packet to structure fields in according to
     * the specification. All necessary logic with corresponding hardcore constats
     * are incapsulated here.
     * This constants are actual only for certain packet type and can be changed without 
     * other packets type impacting 
     */
    if (arbiter == NULL ||
        req == NULL)
        return;

    /* locate first unparsed byte */
    if (req->transp_Type == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH;

    /* complete fields */
    req->header->ttype = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & RIO_TXRXM_TTYPE_MASK) >> RIO_TXRXM_TTYPE_SHIFT);
    req->header->ssize = req->header->rdsize = 
        (RIO_UCHAR_T)(arbiter->packet_Stream[i] & RIO_TXRXM_SIZE_MASK);
    /* target_TID */
    req->header->target_TID = (RIO_UCHAR_T)(arbiter->packet_Stream[++i]);

    /* secondary ID*/
    req->header->sec_ID = (RIO_UCHAR_T)(arbiter->packet_Stream[++i]);

    /* secondary TID*/
    req->header->sec_TID = (RIO_UCHAR_T)(arbiter->packet_Stream[++i]);

    /* extended address */
    req->header->extended_Address = 0;

    if (is_Ext_Address_Valid == RIO_TRUE)
    {
        if (is_Ext_Address_16 == RIO_FALSE)
        {
            req->header->extended_Address = (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 3);
            req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << (RIO_TXRXM_BITS_IN_BYTE * 2);
        }
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i] << RIO_TXRXM_BITS_IN_BYTE;
        req->header->extended_Address |= (unsigned long)arbiter->packet_Stream[++i];
    }

    /* address */
    req->header->address = (unsigned long)arbiter->packet_Stream[++i] << 21;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 13;
    req->header->address |= (unsigned long)arbiter->packet_Stream[++i] << 5;

    /* address, wdptr, xambs */
    req->header->address |= (arbiter->packet_Stream[++i] & 0xf8) >> 3 ;

    /* address has been shifted previously*/
    req->header->address <<= 3;

    req->header->wdptr = (RIO_UCHAR_T)((arbiter->packet_Stream[i] & 0x04) >> 2);
    req->header->xamsbs = (RIO_UCHAR_T)(arbiter->packet_Stream[i] & 0x03);
   
    /* payload is never */
    /* req->header->dw_Num = 0; */

    i++;
    /* payload is always checking and passing*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            req->dw_Num = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        req->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        req->dw_Num = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(req->data, arbiter->packet_Stream + i, req->dw_Num);
}

/***************************************************************************
 * Function : in_Form_Resp
 *
 * Description: form response structure
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_In_Form_Resp(
    RIO_TXRXM_PACKET_ARB_T  *arbiter, 
    RIO_RESPONSE_T    *ext_Resp,
    RIO_UCHAR_T       *ack_ID,
    RIO_UCHAR_T       *hop_Count,
    RIO_BOOL_T        is_Parallel
    )
{
    unsigned int ftype, ttype; 
    unsigned int i;

    if (arbiter == NULL ||
        ext_Resp == NULL ||
        ack_ID == NULL ||
        hop_Count == NULL)
        return;

    /* locate entry */
     /* form entry fields */
    if (is_Parallel == RIO_TRUE)
        *ack_ID = get_Ack_ID(arbiter);
    else *ack_ID = arbiter->packet_Stream[0] >> RIO_STXRXM_ACK_ID_SHIFT;
    
    if (get_Tt(arbiter) == RIO_PL_TRANSP_16)
        i = RIO_TXRXM_TTYPE_POS_LOW;
    else
        i = RIO_TXRXM_TTYPE_POS_HIGH;

    /* form fields */
    ext_Resp->tr_Info = get_Transp_Info(arbiter);
    ext_Resp->ftype = (RIO_UCHAR_T)(ftype = get_Ftype(arbiter));
    ext_Resp->transaction = (RIO_UCHAR_T)(ttype = get_Ttype(arbiter));
    ext_Resp->prio = (RIO_UCHAR_T)get_Prio(arbiter);
    ext_Resp->status = (RIO_UCHAR_T)(arbiter->packet_Stream[i] & RIO_TXRXM_STATUS_MASK);
    ext_Resp->target_TID = arbiter->packet_Stream[++i];
    ext_Resp->sec_ID = 0;
    ext_Resp->sec_TID = 0;
    ext_Resp->transp_Type = get_Tt(arbiter);

    /* 
     * different responses proceding depends on response packet format. All hardcore numbers defines
     * offsets some fields in certain type of packet and are unique. If some packets 
     * formats are changed - change code here.
     */
    switch(ftype)
    {
        case RIO_MAINTENANCE:
            *hop_Count = arbiter->packet_Stream[++i];
            i += 4;
            break;
        case RIO_RESPONCE:
            i++;
            break;
        default:
            return;
    }

    /* payload is always checking and passing*/
    if (arbiter->cur_Pos > RIO_TXRXM_PACKET_LEN_NEED_INT_CRC)
    {
        unsigned int k; /* loop counter */

        if( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD + RIO_TXRXM_BYTES_IN_HALF_WORD) )
        {
            ext_Resp->dw_Num = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD - RIO_TXRXM_BYTES_IN_HALF_WORD)
                / RIO_TXRXM_BYTE_CNT_IN_DWORD);
            for (k = RIO_TXRXM_INT_CRC_START; k < (arbiter->cur_Pos - RIO_TXRXM_BYTES_IN_HALF_WORD); k++)
                arbiter->packet_Stream[k] = arbiter->packet_Stream[k + RIO_TXRXM_BYTES_IN_HALF_WORD];
        }
        else
            ext_Resp->dw_Num  = 0;
    }
    else if ( arbiter->cur_Pos >= (i + RIO_TXRXM_BYTES_IN_HALF_WORD) )
    {
        ext_Resp->dw_Num  = (RIO_UCHAR_T)((arbiter->cur_Pos - i - RIO_TXRXM_BYTES_IN_HALF_WORD) / RIO_TXRXM_BYTE_CNT_IN_DWORD);
    }
    else
        ext_Resp->dw_Num  = 0;
    
    /* convert the stream to payload */
    RIO_TxRxM_In_Stream_To_Dw(ext_Resp->data, arbiter->packet_Stream + i, ext_Resp->dw_Num );

    return;
}
