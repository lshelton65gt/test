/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/txrxm_parallel_model.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model source
*
* Notes:        
*
******************************************************************************/
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

#include "rio_txrxm_parallel_model.h"
#include "rio_txrxm_ds.h"
#include "rio_txrxm_tx.h"
#include "rio_txrxm_rx.h"
#include "rio_txrxm_errors.h"


#define TXRX_BUF_STATUS_VAL    15
/* variable for number of created instanses counting */
static unsigned long txrxm_Instances_Count = 0;


/*structure for verifying the handle correct*/
typedef struct RIO_TXRX_HANDLE_STORAGE{
    struct RIO_TXRX_HANDLE_STORAGE * next;
    RIO_HANDLE_T handle;
}RIO_TXRX_HANDLE_STORAGE_T;
/*routines for checking models handle*/
static RIO_TXRX_HANDLE_STORAGE_T par_Handles = {NULL, NULL};

static RIO_BOOL_T is_Handle_Is_Correct(RIO_HANDLE_T handle);
static int add_Handle(RIO_HANDLE_T handle);
static void remove_Handle_From_Storage(RIO_HANDLE_T handle);
/*static char incorrect_Handle_Message[] = "PTxRx Error: Incorrect handle\n";*/
/*--------- end of handle checking routines --------*/



static int txrxm_Print_Version(
    RIO_HANDLE_T handle
    );

static int txrxm_Model_Initialize(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_PARALLEL_PARAM_SET_T   *param_Set
    );

static int txrxm_Model_Start_Reset(
    RIO_HANDLE_T handle
    );

static int txrxm_Clock_Edge(RIO_HANDLE_T handle);

static int txrxm_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_BYTE_T frame,    /* receive frame */
    RIO_BYTE_T rd,       /* receive data */
    RIO_BYTE_T rdl,      /* receive data low part */
    RIO_BYTE_T rclk      /* receive clock */
    );
static int txrxm_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_BYTE_T tframe,     /* transmit frame */
    RIO_BYTE_T td,         /* transmit data */
    RIO_BYTE_T tdl,        /* transmit data low part */
    RIO_BYTE_T tclk        /* transmit clock */
    );
static int txrxm_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    );

static int txrxm_Get_Granule(
    RIO_CONTEXT_T context,   /* my handle */
    RIO_HANDLE_T  handle,    /* invokater's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    );

static int txrxm_Msg(
    RIO_CONTEXT_T     context,    
    RIO_PL_MSG_TYPE_T msg_Type,
    char              *string
    );

static int txrxm_Lost_Sync(RIO_HANDLE_T handle);

/*txrxm special callbacks*/

static int txrxm_Packet_Struct_Request(
    RIO_HANDLE_T                    handle,
    RIO_TXRXM_PACKET_REQ_STRUCT_T   *rq,
    RIO_UCHAR_T                     dw_Size,           
    RIO_DW_T                        *data             
    );

static int txrxm_Packet_Stream_Request(
    RIO_HANDLE_T            handle,
    RIO_UCHAR_T*            packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int            packet_Size        /*packet array size in byte count*/  
    );

static int txrxm_Symbol_Request(
    RIO_HANDLE_T                handle, 
    RIO_SYMBOL_STRUCT_T            *sym,/*control symbol as a fields struct*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Training_Request(
    RIO_HANDLE_T                handle, 
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Granule_Request(
    RIO_HANDLE_T                handle, 
    unsigned long               granule,/* granule data*/
    RIO_BOOL_T                  has_Frame_Toggled, /*if frame has toggled at the granule begin*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Event_Request(
    RIO_HANDLE_T                handle, 
    RIO_TXRX_EVENT_T            event,/* event*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Initialize_Instance(RIO_TXRXM_DS_T *instanse, RIO_TXRXM_PARALLEL_PARAM_SET_T *param_Set);
static int check_Init_Param_Set(RIO_TXRXM_DS_T *instanse, RIO_TXRXM_PARALLEL_PARAM_SET_T *param_Set);
static int check_Inst_Param_Set(RIO_TXRXM_PARALLEL_INST_PARAM_T *param_Set);
static int txrxm_Create_Bind_Link(RIO_TXRXM_DS_T *handle);

static int rio_TxRxM_Delete_Instance( RIO_HANDLE_T handle );

/*Initialization block special callbacks*/
static int rio_TxRxM_Set_State(
    RIO_HANDLE_T             handle,    /* callback context */
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
	unsigned int			 indicator_Parameter
    );

static int rio_TxRxM_Get_State(
    RIO_HANDLE_T             handle,    /* callback context */
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
	unsigned int			 *indicator_Parameter
    );




/***************************************************************************
 * Function : IS_NOT_A_BOOL
 *
 * Description: detect if a variable is RIO_BOOL_T type
 *
 * Returns: bool
 *
 * Notes: none
 *
 **************************************************************************/
#define IS_NOT_A_BOOL(bool_Value) ((bool_Value) != RIO_TRUE && (bool_Value) != RIO_FALSE)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_PACKET_TEMP
 *
 * Description: return address of an outbound packet buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_PACKET_TEMP(handle) ((handle)->outbound_Data.out_Packet_Buffer.packet_Temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_GRANULE_TEMP
 *
 * Description: return address of an outbound granule buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_GRANULE_TEMP(handle) ((handle)->outbound_Data.out_Granule_Buffer.granule_Temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_EVENT_TEMP
 *
 * Description: return address of an outbound granule buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_EVENT_TEMP(handle) ((handle)->outbound_Data.out_Event_Buffer.event_Temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_SYMBOL_TEMP
 *
 * Description: return address of an outbound symbol buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_SYMBOL_TEMP(handle) ((handle)->outbound_Data.out_Symbol_Buffer.symbol_Temp)
/***************************************************************************
 * Function : RIO_TxRxM_Model_Create_Instance
 *
 * Description: Create new txrxm model instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_TxRxM_Parallel_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_TXRXM_PARALLEL_INST_PARAM_T  *param_Set
    )
{
    /* handle to structure will allocated*/
    RIO_TXRXM_DS_T *txrxm_Instanse = NULL;

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for txrxm model 
     * data structure
     */
    if (handle == NULL || 
        param_Set == NULL || 
        check_Inst_Param_Set(param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* clear handle */
    *handle = NULL;

    /* allocate instanse */
    if ((txrxm_Instanse = malloc(sizeof(RIO_TXRXM_DS_T))) == NULL)
        return RIO_ERROR;

    /* allocate memory for outbound packets buffers */
    if ((txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer = 
        malloc(sizeof(RIO_TXRXM_PACKET_OUT_ENTRY_T) * param_Set->packets_Buffer_Size)) == NULL)
    {
        free(txrxm_Instanse);
        return RIO_ERROR;
    }

    /* allocate memory for outbound packets buffers */
    if ((txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer = 
        malloc(sizeof(RIO_TXRXM_SYMBOL_OUT_ENTRY_T) * param_Set->symbols_Buffer_Size)) == NULL)
    {
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);
        free(txrxm_Instanse);

        return RIO_ERROR;
    }

    if ((txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer = 
        malloc(sizeof(RIO_TXRXM_GRANULE_OUT_ENTRY_T) * param_Set->granules_Buffer_Size)) == NULL)
    {
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer); 
        free(txrxm_Instanse);

        return RIO_ERROR;
    }


    if ((txrxm_Instanse->outbound_Data.out_Queue.queue_Buffer = 
        malloc(sizeof(RIO_TXRXM_QUEUE_OUT_ENTRY_T) * (param_Set->granules_Buffer_Size + 
        param_Set->packets_Buffer_Size + param_Set->symbols_Buffer_Size
        + param_Set->event_Buffer_Size))) == NULL)
    {
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer); 
        free(txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer);
        free(txrxm_Instanse);

        return RIO_ERROR;
    }

    if ((txrxm_Instanse->outbound_Data.out_Event_Buffer.event_Buffer = 
        malloc(sizeof(RIO_TXRXM_EVENT_OUT_ENTRY_T) * param_Set->event_Buffer_Size)) == NULL)
    {
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer); 
        free(txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Queue.queue_Buffer);
        free(txrxm_Instanse);

        return RIO_ERROR;
    }


    /*high priority queue*/
    if ((txrxm_Instanse->outbound_Data.out_High_Prio_Queue.queue_Buffer = 
        malloc(sizeof(RIO_TXRXM_QUEUE_OUT_ENTRY_T) * param_Set->symbols_Buffer_Size)) == NULL)
    {
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer); 
        free(txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Queue.queue_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Event_Buffer.event_Buffer);
        free(txrxm_Instanse);

        return RIO_ERROR;
    }

    /*high prio buffer*/
    if ((txrxm_Instanse->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer = 
        malloc(sizeof(RIO_TXRXM_SYMBOL_OUT_ENTRY_T) * param_Set->symbols_Buffer_Size)) == NULL)
    {
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer); 
        free(txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Queue.queue_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Event_Buffer.event_Buffer);
        free(txrxm_Instanse->outbound_Data.out_High_Prio_Queue.queue_Buffer);
        free(txrxm_Instanse);

        return RIO_ERROR;
    }

    /*
     * clear pointers to external callbacks 
     */
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Request_Received = NULL;
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Canceled_Packet_Received = NULL;
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Violent_Packet_Received = NULL;
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Response_Received = NULL;
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Symbol_Received = NULL;
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Training_Received = NULL;
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Violent_Symbol_Received = NULL;
    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Granule_Received = NULL;
    txrxm_Instanse->ext_Interfaces.txrxm_Interface_Context = NULL;

    txrxm_Instanse->ext_Interfaces.rio_TxRxM_Set_Pins = NULL;
    txrxm_Instanse->ext_Interfaces.lpep_Interface_Context = NULL;

    txrxm_Instanse->ext_Interfaces.rio_User_Msg = NULL;
    txrxm_Instanse->ext_Interfaces.rio_Msg = NULL;
    

    /*
     * clear 32-Bit TxRx pointers
     */
    txrxm_Instanse->link_Instanse = NULL;
    txrxm_Instanse->link_Interface.rio_Link_Get_Pins = NULL;   
    txrxm_Instanse->link_Interface.rio_Link_Clock_Edge = NULL;
    txrxm_Instanse->link_Interface.rio_Link_Start_Reset = NULL;
    txrxm_Instanse->link_Interface.rio_Link_Initialize = NULL;
    
    txrxm_Instanse->link_Interface.rio_Link_Tx_Disabling = NULL;
    txrxm_Instanse->link_Interface.rio_Link_Rx_Disabling = NULL;

    /* try create and bind a link */
    if (txrxm_Create_Bind_Link(txrxm_Instanse) == RIO_ERROR)
    {
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer);
        free(txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer); 
        free(txrxm_Instanse);
        return RIO_ERROR;
    }

    /*allocation was successful */
    txrxm_Instanse->instance_Number = ++txrxm_Instances_Count;
    txrxm_Instanse->in_Reset = RIO_TRUE;
    txrxm_Instanse->inst_Param_Set = *param_Set;
    txrxm_Instanse->restart_Function = txrxm_Model_Initialize;
    txrxm_Instanse->reset_Function = txrxm_Model_Start_Reset;

    txrxm_Instanse->is_Bound = RIO_FALSE;

    /* set handle for the environment */
    *handle = (RIO_HANDLE_T)txrxm_Instanse;
    /*pass handle to the handle storage*/
    if (add_Handle(*handle) != RIO_OK) return RIO_ERROR;

    return RIO_OK;
}
/***************************************************************************
 * Function : txrxm_Create_Bind_Link
 *
 * Description: instantiate 32-Bit Transmitter/Receiver into txrx model
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Create_Bind_Link(RIO_TXRXM_DS_T *handle)
{
    unsigned int flag = 0; /* flag for loop*/
    /* 
     * definition of 32-Bit TxRx with structure initialization, all 
     * txrx callbacks are physical layer functions and contextxs are 
     * NULLs
     */
    RIO_LINK_MODEL_CALLBACK_TRAY_T ctray = {
        txrxm_Set_Pins, 
        NULL,
        /*granules will be taken from the initialization block
        this pointers will be reinitialized behind after access
        to the Initialization block Ftray*/
        NULL /*txrxm_Get_Granule*/,
        NULL /*txrxm_Put_Granule*/,
        NULL,
        /*end of callback for granules access*/
        txrxm_Msg,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
        };

    RIO_PL_PAR_INIT_BLOCK_CALLBACK_TRAY_T init_Ctray = 
    {
/*link parameters*/
        txrxm_Get_Granule,
        txrxm_Put_Granule,
        NULL, 
/*current state of initialization block parameters*/
        rio_TxRxM_Set_State,
        rio_TxRxM_Get_State,
        NULL, 
/*message parameters*/
        txrxm_Msg,
        NULL
    };
        

    /*
     * this pointers cannot be assigned in initialization
     */
    ctray.error_Warning_Context = (RIO_CONTEXT_T)handle;
    ctray.lpep_Interface_Context = (RIO_CONTEXT_T)handle;
    ctray.palpdl_Interface_Context = (RIO_CONTEXT_T)handle;
    ctray.rio_Hooks_Context = (RIO_CONTEXT_T)handle;
        /*initialization bkick contexts initialization*/
    init_Ctray.rio_PL_Interface_Context = (RIO_CONTEXT_T)handle; 
    init_Ctray.rio_Msg = (RIO_CONTEXT_T)handle;
    init_Ctray.rio_Regs_Context = (RIO_CONTEXT_T)handle;

    /*
        Create an instace of the Initialization Block
    */

    if (RIO_PL_Par_Init_Block_Create_Instance(&handle->init_Instanse) != RIO_OK)
        return RIO_ERROR;
    if ( handle->init_Instanse == NULL) return RIO_ERROR;
    /*Get ftray of the initialization block*/
    if (RIO_PL_Par_Init_Block_Get_Function_Tray(
        &handle->init_Interface) != RIO_OK)
        return RIO_ERROR;

    /*try to bind instance*/
    if (RIO_PL_Par_Init_Block_Bind_Instance(
        handle->init_Instanse, &init_Ctray) != RIO_OK)
    {
        handle->init_Interface.rio_PL_Par_Init_Block_Delete_Instance(handle->init_Instanse);
        handle->init_Instanse = NULL;
        return RIO_ERROR;
    }
    /*change callback tray for the txrx module*/
    ctray.rio_Link_Get_Granule = handle->init_Interface.rio_PL_Par_Init_Block_Get_Granule;
    ctray.rio_Link_Put_Granule = handle->init_Interface.rio_PL_Par_Init_Block_Put_Granule;
    ctray.palpdl_Interface_Context = (RIO_CONTEXT_T)handle->init_Instanse;
    /* -=end of initialization block creation=- */

    /* 
     * create an instanse, export its function tray and bind it to
     * physical layer
     * if an error happens - memory will be deallocated and 
     * an error code will be returned
     */
    if (RIO_Link_Model_Create_Instance(&handle->link_Instanse) == RIO_ERROR ||
        handle->link_Instanse == NULL)
        return RIO_ERROR;

    /* try create and bind link */
    do
    {
        if (RIO_Link_Model_Get_Function_Tray(
            &handle->link_Interface) == RIO_ERROR)
            break;

        if (RIO_Link_Model_Bind_Instance(
            handle->link_Instanse, 
            &ctray) == RIO_ERROR)
            break;
        
        /* a new instanse has just been binded within pl */
        return RIO_OK;
        
    } while (flag);

    /* creating and binding wasn't successful */
    free(handle->link_Instanse);

    return RIO_ERROR;
}

/***************************************************************************
 * Function : RIO_TXRXM_Model_Get_Function_Tray
 *
 * Description: export physical layer instance function tray
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Parallel_Model_Get_Function_Tray(
    RIO_TXRXM_PARALLEL_MODEL_FTRAY_T *ftray             
    )
{
    /*
     * check passed poinetrs and if they are valid compete functional
     * tray with actual entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->rio_TxRxM_Clock_Edge            = txrxm_Clock_Edge;
    ftray->rio_TxRxM_Packet_Struct_Request = txrxm_Packet_Struct_Request;
    ftray->rio_TxRxM_Packet_Stream_Request = txrxm_Packet_Stream_Request;
    ftray->rio_TxRxM_Symbol_Request        = txrxm_Symbol_Request;
    ftray->rio_TxRxM_Training_Request      = txrxm_Training_Request;
    ftray->rio_TxRxM_Granule_Request       = txrxm_Granule_Request;
    ftray->rio_TxRxM_Model_Start_Reset     = txrxm_Model_Start_Reset;
    ftray->rio_TxRxM_Model_Initialize      = txrxm_Model_Initialize;
    ftray->rio_TxRxM_Get_Pins              = txrxm_Get_Pins;
    ftray->rio_TxRxM_Event_Req             = txrxm_Event_Request;

    ftray->rio_TxRxM_Print_Version         = txrxm_Print_Version;
    
    ftray->rio_TxRxM_Delete_Instance       = rio_TxRxM_Delete_Instance;

    ftray->rio_TxRxM_Lost_Sync             = txrxm_Lost_Sync;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_TxRxM_Parallel_Model_Bind_Instance
 *
 * Description: bind txrx model instance 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Parallel_Model_Bind_Instance(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_PARALLEL_MODEL_CALLBACK_TRAY_T   *ctray   
    )
{

    /* instanse handle */
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;

    /*
     * check pointers and callbacks if OK load callbacks
     */
    if (instanse == NULL || ctray == NULL ||
        ctray->rio_TxRxM_Set_Pins == NULL)
        return RIO_ERROR;

    if (is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        /*printf(incorrect_Handle_Message);*/
        return RIO_ERROR;
    }

    /* store callbacks tray into the layer's structure */
    instanse->ext_Interfaces = *ctray;

    instanse->is_Bound = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Print_Version
 *
 * Description: prints model version
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Print_Version(RIO_HANDLE_T handle)
{

    /* instanse handle and the result of callback invokation */
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
    char           buffer[255];

    /*
     * check handle and callback then return and check the result
     */
    if (instanse == NULL ||
        instanse->ext_Interfaces.rio_User_Msg == NULL ||
        instanse->is_Bound == RIO_FALSE
    )
        return RIO_ERROR;

    /* print the version info iinto the buffer string */
    sprintf( buffer, "RIO TxRxM Model %s\n", RIO_MODEL_VERSION );

    instanse->ext_Interfaces.rio_User_Msg(
        instanse->ext_Interfaces.rio_Msg,
        buffer
    );

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Clock_Edge
 *
 * Description: physical layer clock
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Clock_Edge(RIO_HANDLE_T handle)
{

    /* instanse handle and the result of callback invokation */
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
    int clock_Edge_Result;

    /*
     * check handle and callback then return and check the result
     */
    if (instanse == NULL ||
        instanse->link_Interface.rio_Link_Clock_Edge == NULL ||
        instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if ((clock_Edge_Result = instanse->link_Interface.rio_Link_Clock_Edge(
        instanse->link_Instanse)) == RIO_ERROR)
    {
/*        RIO_TxRxM_Print_Message(instanse,RIO_TXRXM_ERROR_2);*/
    }
    
    if ( (instanse->init_Interface.rio_PL_Par_Init_Block_Clock_Edge
        (instanse->init_Instanse)) != RIO_OK)
        clock_Edge_Result = RIO_ERROR;

    
    return clock_Edge_Result;
}

/***************************************************************************
 * Function : txrxm_Packet_Struct_Request
 *
 * Description: notify about new struct request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Packet_Struct_Request(
    RIO_HANDLE_T                    handle,
    RIO_TXRXM_PACKET_REQ_STRUCT_T   *rq,
    RIO_UCHAR_T                     dw_Size,           
    RIO_DW_T                        *data             
    )
{

    /*instanse handler, temp for function result storing*/
    unsigned int load_Result;
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
        
    /* check pointers */
    if (instanse == NULL || rq == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    switch (rq->ftype)
    {
    case RIO_NONINTERV_REQUEST:
    case RIO_WRITE:
    case RIO_STREAM_WRITE:
    case RIO_MAINTENANCE:
    case RIO_DOORBELL:
    case RIO_MESSAGE:
    case RIO_RESPONCE:
        break;
    default:
        return RIO_ERROR;
    }


    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).as_Struct = RIO_TRUE;
     /*copy packet struct*/
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.ack_ID = rq->ack_ID;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.prio = rq->prio;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.rsrv_Phy_1 = rq->rsrv_Phy_1;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.rsrv_Phy_2 = rq->rsrv_Phy_2;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.tt = rq->tt;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.ftype = rq->ftype;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.transport_Info = rq->transport_Info;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.ttype = rq->ttype;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.rdwr_Size = rq->rdwr_Size;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.srtr_TID = rq->srtr_TID;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.hop_Count = rq->hop_Count;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.config_Offset = rq->config_Offset;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.wdptr = rq->wdptr; 
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.status = rq->status;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.doorbell_Info = rq->doorbell_Info;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.msglen = rq->msglen;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.letter = rq->letter;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.mbox = rq->mbox;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.msgseg = rq->msgseg;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.ssize = rq->ssize;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.address = rq->address;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.extended_Address = rq->extended_Address;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.xamsbs = rq->xamsbs;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.ext_Address_Valid = rq->ext_Address_Valid; 
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.ext_Address_16 = rq->ext_Address_16;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.rsrv_Ftype6 = rq->rsrv_Ftype6;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.rsrv_Ftype8 = rq->rsrv_Ftype8;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.rsrv_Ftype10 = rq->rsrv_Ftype10;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.crc = rq->crc;
    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.entry.is_Crc_Valid = rq->is_Crc_Valid;

    /*set payload size*/
    if (dw_Size > RIO_TXRXM_MAX_PAYLOAD_SIZE) 
        RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.payload_Size =
                RIO_TXRXM_MAX_PAYLOAD_SIZE;
    else
        RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.payload_Size = dw_Size;

    /*copy payload*/
    if (data)
    {
        unsigned int i; /* loop counter */
        for (i = 0; i < RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.payload_Size; i++)
            RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.payload[i] = data[i];
    }
    else 
        RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Struct.payload_Size = 0;


    if ((load_Result = RIO_TxRxM_Load_Packet_Entry(instanse)) == RIO_ERROR)
        return RIO_ERROR;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Packet_Stream_Request
 *
 * Description: notify about new stream request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Packet_Stream_Request(
    RIO_HANDLE_T            handle,
    RIO_UCHAR_T*            packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int            packet_Size        /*packet array size in byte count*/  
    )
{
    /*instanse handler, temp for function result storing*/
    unsigned int load_Result;
    unsigned int i; /* loop counter */
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
        
    /* check pointers */
    if (instanse == NULL || packet_Buffer == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).as_Struct = RIO_FALSE;

    /*set packet size*/
    if (packet_Size > RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN) 
        RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Stream.packet_Len =
                RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN;
    else
        RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Stream.packet_Len = packet_Size;

    /*copy stream*/
    for (i = 0; i < RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Stream.packet_Len; i++)
            RIO_TXRXM_OUT_GET_PACKET_TEMP(instanse).packet_Stream.packet_Stream[i] = packet_Buffer[i];


    if ((load_Result = RIO_TxRxM_Load_Packet_Entry(instanse)) == RIO_ERROR)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Symbol_Request
 *
 * Description: notify about new symbol request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Symbol_Request(
    RIO_HANDLE_T                handle, 
    RIO_SYMBOL_STRUCT_T            *sym,/*control symbol as a fields struct*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    /*instanse handler, temp for function result storing*/
    unsigned int load_Result;
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
        
    /* check pointers */
    if (instanse == NULL || sym == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*set symbol fields*/
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).place = place;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).granule_Num = granule_Num;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).symbol.ackID = sym->ackID;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).symbol.buf_Status = sym->buf_Status;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).symbol.stype = sym->stype;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).symbol.s = sym->s;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).symbol.s_Parity = sym->s_Parity;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).symbol.rsrv_1 = sym->rsrv_1;
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).symbol.rsrv_2 = sym->rsrv_2;

    /*set type different from data,training, start_packet*/
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).granule_Type = RIO_GR_UNRECOGNIZED;
    
    if ((load_Result = RIO_TxRxM_Load_Symbol_Entry(instanse)) == RIO_ERROR)
        return RIO_ERROR;
    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Granule_Request
 *
 * Description: notify about new granule request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Granule_Request(
    RIO_HANDLE_T                handle, 
    unsigned long               granule,/* granule data*/
    RIO_BOOL_T                  has_Frame_Toggled, /*if frame has toggled at the granule begin*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    /*instanse handler, temp for function result storing*/
    unsigned int load_Result;
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
       
    /* check pointers */
    if (instanse == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;
 
    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instanse->outbound_Data.out_Packet_Buffer.packet_Buffer_Head == 
        instanse->outbound_Data.out_Packet_Buffer.packet_Buffer_Top && 
        instanse->outbound_Data.out_Packet_Buffer.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_OUT_GET_GRANULE_TEMP(instanse).place = FREE;
    }
    else
        RIO_TXRXM_OUT_GET_GRANULE_TEMP(instanse).place = place;

    /*set other symbol fields*/
    RIO_TXRXM_OUT_GET_GRANULE_TEMP(instanse).granule_Num = granule_Num;
    RIO_TXRXM_OUT_GET_GRANULE_TEMP(instanse).has_Frame_Toggled = has_Frame_Toggled;
    RIO_TXRXM_OUT_GET_GRANULE_TEMP(instanse).granule = granule;
    /*set type different from data,training, start_packet*/
    RIO_TXRXM_OUT_GET_GRANULE_TEMP(instanse).granule_Type = RIO_GR_DATA;

    if ((load_Result = RIO_TxRxM_Load_Granule_Entry(instanse)) == RIO_ERROR)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Training_Request
 *
 * Description: notify about new symbol request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Training_Request(
    RIO_HANDLE_T                handle, 
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    /*instanse handler, temp for function result storing*/
    unsigned int load_Result;
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
    
    unsigned int buf_Top;
        
    /* check pointers */
    if (instanse == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    buf_Top = instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer_Top;

        
    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instanse->outbound_Data.out_Packet_Buffer.packet_Buffer_Head == 
        instanse->outbound_Data.out_Packet_Buffer.packet_Buffer_Top && 
        instanse->outbound_Data.out_Packet_Buffer.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).place = FREE;
    }
    else
        RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).place = place;

    /*set other symbol fields*/
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).granule_Num = granule_Num;
    /*set type training*/
    RIO_TXRXM_OUT_GET_SYMBOL_TEMP(instanse).granule_Type = RIO_GR_TRAINING;
    
    
    if ((load_Result = RIO_TxRxM_Load_Symbol_Entry(instanse)) == RIO_ERROR)
        return RIO_ERROR;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : check_Inst_Param_Set
 *
 * Description: check is txrx model instantiation parameters set is valid
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Inst_Param_Set(RIO_TXRXM_PARALLEL_INST_PARAM_T *param_Set)
{
    /*check params handle*/
    if (param_Set == NULL)
        return RIO_ERROR;

    /*check oubound buffers size*/
    if (param_Set->packets_Buffer_Size < 1 ||
        param_Set->symbols_Buffer_Size < 1 ||
        param_Set->granules_Buffer_Size < 1 ||
        param_Set->event_Buffer_Size < 1)
        return RIO_ERROR;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Set_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Set_Pins(
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_BYTE_T tframe,     /* transmit frame */
    RIO_BYTE_T td,         /* transmit data */
    RIO_BYTE_T tdl,        /* transmit data low part */
    RIO_BYTE_T tclk        /* transmit clock */
    )
{
    RIO_TXRXM_DS_T *instanse = (RIO_HANDLE_T)context;
    int set_Pins_Result;

    /* check pointers */
    if (instanse == NULL || 
        instanse->ext_Interfaces.rio_TxRxM_Set_Pins == NULL)
        return RIO_ERROR;

    /* invoke environments callback */
    if ((set_Pins_Result = instanse->ext_Interfaces.rio_TxRxM_Set_Pins(
        instanse->ext_Interfaces.lpep_Interface_Context,
        tframe,
        td,
        tdl,
        tclk)) == RIO_ERROR)
    {
/*        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_5);*/
    }

    return set_Pins_Result;
}

/***************************************************************************
 * Function : pl_Get_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_BYTE_T frame,    /* receive frame */
    RIO_BYTE_T rd,       /* receive data */
    RIO_BYTE_T rdl,      /* receive data low part */
    RIO_BYTE_T rclk      /* receive clock */
    )
{
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
    int get_Pins_Result;

    /*check that pointer is correct*/
    if (is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        /*printf(incorrect_Handle_Message);*/
        return RIO_ERROR;
    }

    if (instanse == NULL ||
        instanse->link_Interface.rio_Link_Get_Pins == NULL)
        return RIO_ERROR;

    /* invoke link's function */
    if ((get_Pins_Result = instanse->link_Interface.rio_Link_Get_Pins(
        instanse->link_Instanse,
        frame,
        rd,
        rdl,
        rclk)) == RIO_ERROR)
    {
 /*       RIO_PL_Print_Message(instanse, RIO_PL_ERROR_8);*/
    }
     
    return get_Pins_Result;
}

/***************************************************************************
 * Function : txrxm_Get_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Get_Granule(
    RIO_CONTEXT_T context,   /* my handle */
    RIO_HANDLE_T  handle,    /* invokater's handle */
    RIO_GRANULE_T *granule   /* pointer to store granule for heading out */
    )
{
    int i;
    RIO_TXRXM_DS_T *instanse = (RIO_HANDLE_T)context;

    /* check pointers and state */
    if (instanse == NULL || handle == NULL || 
        granule == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /* check if invokation comes form our link */
    /*if (instanse->link_Instanse != handle)*/
    if (instanse->init_Instanse != handle)
    {
/*        RIO_PL_Print_Message(instanse, RIO_PL_ERROR_6); */
        return RIO_ERROR;
    }

    /* obtain granule for transmitting */

    RIO_TxRxM_Granule_For_Transmitting(instanse, granule);

    if(instanse->ext_Interfaces.rio_TxRxM_Granule_To_Send != NULL)
    {
        RIO_TXRX_GRANULE_T granule_Buf;
        granule_Buf.granule_Type = granule->granule_Type;
        granule_Buf.granule_Frame = granule->granule_Frame;

        for(i =0; i < RIO_GRAN_SIZE; i++)
            granule_Buf.granule_Data[i] = granule->granule_Date[i];

        granule_Buf.granule_Struct.s = granule->granule_Struct.s;
        granule_Buf.granule_Struct.ackID = granule->granule_Struct.ack_ID;
        granule_Buf.granule_Struct.rsrv_1 = 
            (granule->granule_Struct.secded >> 4) & 0x01;
        granule_Buf.granule_Struct.s_Parity =
            (granule->granule_Struct.secded >> 3) & 0x01;
        granule_Buf.granule_Struct.rsrv_2 =
            (granule->granule_Struct.secded & 0x07);
        granule_Buf.granule_Struct.buf_Status = granule->granule_Struct.buf_Status;
        granule_Buf.granule_Struct.stype = granule->granule_Struct.stype;

        instanse->ext_Interfaces.rio_TxRxM_Granule_To_Send(
            instanse->ext_Interfaces.hook_Context,
            (void*)(&granule_Buf));
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Model_Initialize
 *
 * Description: turn the instanse to work mode and initialize
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Model_Initialize(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_PARALLEL_PARAM_SET_T   *param_Set
    )
{
    /* instanse handle and link's parameters set */
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
    RIO_LINK_PARAM_SET_T link_Param_Set;
    RIO_PAR_INIT_BLOCK_PARAM_SET_T  init_Block_Param_Set;


    /* check pointers */
    if (instanse == NULL || param_Set == NULL)
        return RIO_ERROR;

    if (instanse->is_Bound != RIO_TRUE) return RIO_ERROR;

    /* check reset state */
    if (instanse->in_Reset == RIO_FALSE)
    {
        /*RIO_PL_Print_Message(instanse, RIO_PL_ERROR_3);*/
        return RIO_ERROR;
    }


    /* check parameters set */
    if (check_Init_Param_Set(instanse, param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* prepare link's parameters set */
    link_Param_Set.lp_Ep_Is_8 = param_Set->lp_Ep_Is_8;
    link_Param_Set.work_Mode_Is_8 = param_Set->work_Mode_Is_8;

    link_Param_Set.single_Data_Rate = RIO_FALSE;
    link_Param_Set.check_SECDED = RIO_FALSE;
    link_Param_Set.correct_Using_SECDED = RIO_FALSE;

    /*necessary to detect if to toggle frame automatically or not*/
    link_Param_Set.is_TxRx_Model = RIO_TRUE;

    /*pass value of the training pattern
    the default training pattern in the spec is FF00*/
    link_Param_Set.training_Pattern[0] = 0xFFFFFFFF;
    if (param_Set->lp_Ep_Is_8 == RIO_TRUE || param_Set->work_Mode_Is_8 == RIO_TRUE)
        link_Param_Set.training_Pattern[1] = 0;
    else 
        link_Param_Set.training_Pattern[1] = 0xFFFFFFFF;
    link_Param_Set.training_Pattern[2] = 0;
    link_Param_Set.training_Pattern[3] = 0;


    /* try to initialize the link */
    if (instanse->link_Interface.rio_Link_Initialize(
        instanse->link_Instanse,
        &link_Param_Set) == RIO_ERROR)
        return RIO_ERROR;

    /* save parameters and initialize the instanse */
    instanse->parameters_Set = *param_Set;

    if (txrxm_Initialize_Instance(instanse, param_Set) == RIO_ERROR) return RIO_ERROR;

    /*Initialize Init Block instance*/
    init_Block_Param_Set.num_Trainings = param_Set->num_Trainings;
    init_Block_Param_Set.requires_Window_Alignment = param_Set->requires_Window_Alignment;

    if ( instanse->init_Interface.rio_PL_Par_Init_Block_Initialize(
        instanse->init_Instanse, &init_Block_Param_Set) != RIO_OK)
        return RIO_ERROR;

    /*perform machine managment*/
    instanse->init_Interface.rio_PL_Par_Init_Block_Machine_Management(
        instanse->init_Instanse, param_Set->is_Init_Proc_On);

    /*unset the initialization flag*/
    instanse->in_Reset = RIO_FALSE;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Initialize_Instanse
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Initialize_Instance(RIO_TXRXM_DS_T *instanse, RIO_TXRXM_PARALLEL_PARAM_SET_T *param_Set)
{
    /*
     * check handle
     */
    if (instanse == NULL)
        return RIO_ERROR;
    
    /*
     * initialize components of the instanse
     */
    
    RIO_TxRxM_Rx_Init(instanse);
    RIO_TxRxM_Tx_Init(instanse);
    
    return RIO_OK;
}

/***************************************************************************
 * Function : check_Init_Param_Set
 *
 * Description: check is initialization txrxm 
 *              parameters set is valid
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Init_Param_Set(
    RIO_TXRXM_DS_T *instanse, 
    RIO_TXRXM_PARALLEL_PARAM_SET_T *param_Set
    )
{
    if (param_Set == NULL || instanse == NULL)
        return RIO_ERROR;
        
    if (IS_NOT_A_BOOL(param_Set->in_Ext_Address_Valid) ||
        IS_NOT_A_BOOL(param_Set->in_Ext_Address_16))
        return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->lp_Ep_Is_8))
        return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->work_Mode_Is_8))
        return RIO_ERROR;

    if (IS_NOT_A_BOOL(param_Set->default_Frame_Toggle))
        return RIO_ERROR;

    if (param_Set->lp_Ep_Is_8 == RIO_TRUE && param_Set->work_Mode_Is_8 == RIO_FALSE)
        return RIO_ERROR;

    /*check parameters for the Initialization block*/
    if (IS_NOT_A_BOOL(param_Set->requires_Window_Alignment))
        return RIO_ERROR;


    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Model_Start_Reset
 *
 * Description: turn the instanse to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Model_Start_Reset(
    RIO_HANDLE_T handle
    )
{
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;

    /* check pointers */
    if (instanse == NULL ||
        instanse->link_Interface.rio_Link_Start_Reset == NULL ||
        instanse->init_Interface.rio_PL_Par_Init_Block_Start_Reset == NULL)
        return RIO_ERROR;

    /* check work state */
    if (instanse->in_Reset == RIO_TRUE)
        return RIO_OK;

    /* send reset to the link */
    if (instanse->link_Interface.rio_Link_Start_Reset(
        instanse->link_Instanse) == RIO_ERROR)
        return RIO_ERROR;
    /*send reset to the initialization module*/
    if (instanse->init_Interface.rio_PL_Par_Init_Block_Start_Reset(
        instanse->init_Instanse) != RIO_OK )
        return RIO_ERROR;

    /* modify state to be _in_reset_ */
    instanse->in_Reset = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Put_Granule
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Put_Granule(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_HANDLE_T  handle,     /* instance's handle */
    RIO_GRANULE_T *granule    /* pinter to granule data structure */
    )
{
    /* instanse handler */
    RIO_TXRXM_DS_T *instanse = (RIO_HANDLE_T)context;

    if (instanse == NULL || handle == NULL || granule == NULL)
        return RIO_ERROR;

    /* check if call comes from proper link */
    /*if (instanse->link_Instanse != handle)*/
    if (instanse->init_Instanse != handle)
    {
        /*RIO_PL_Print_Message(instanse, RIO_PL_ERROR_7);*/
        return RIO_ERROR;
    }

    /* pass the granule to receiver */
    RIO_TxRxM_Granule_Received(instanse, granule);

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Msg
 *
 * Description: calls txrxm_Print_Message for 32 bit tx/rx
 *              uses message type to map to one of 32txrx special entries in 
 *              message array
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Msg(
    RIO_CONTEXT_T context,    /* callback context */
    RIO_PL_MSG_TYPE_T msg_Type,
    char          *string
    )
{
    RIO_TXRXM_MESSAGES_T message_Num;
    /* 
     * context here is instanse handle because this function is 
     * invoking as inside txrxm so from txrx instanse
     */
    RIO_TXRXM_DS_T *handle = (RIO_TXRXM_DS_T*)context;    

    if (handle == NULL || string == NULL)
        return RIO_ERROR;

    switch( msg_Type )
    {
        case RIO_PL_MSG_WARNING_MSG:
            message_Num = RIO_TXRXM_WARNING_1;
            break;
        
        case RIO_PL_MSG_ERROR_MSG:
            message_Num = RIO_TXRXM_ERROR_1;
            break;
            
        case RIO_PL_MSG_NOTE_MSG:
            message_Num = RIO_TXRXM_NOTE_1;
            break;
            
        default:
            return RIO_ERROR;
    }
    
    /* print warning */
    return RIO_TxRxM_Print_Message(
        handle, 
        message_Num,
        string);
}


/***************************************************************************
 * Function : rio_TxRxM_Delete_Instance
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/

static int rio_TxRxM_Delete_Instance( RIO_HANDLE_T handle )
{

    RIO_TXRXM_DS_T *txrxm_Instanse = (RIO_TXRXM_DS_T*)handle;
    
    if (txrxm_Instanse == NULL)
        return RIO_ERROR;

    /*Check handler*/
    if (is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        /*printf(incorrect_Handle_Message);*/
        return RIO_ERROR;
    }
    remove_Handle_From_Storage(handle);


    if (txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer != NULL)
        free(txrxm_Instanse->outbound_Data.out_Packet_Buffer.packet_Buffer);

    if (txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer != NULL)
        free(txrxm_Instanse->outbound_Data.out_Symbol_Buffer.symbol_Buffer);

    if (txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer != NULL)
        free(txrxm_Instanse->outbound_Data.out_Granule_Buffer.granule_Buffer); 

    if (txrxm_Instanse->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer != NULL)
        free(txrxm_Instanse->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer);

    if (txrxm_Instanse->outbound_Data.out_High_Prio_Queue.queue_Buffer != NULL)
        free(txrxm_Instanse->outbound_Data.out_High_Prio_Queue.queue_Buffer);
        
    if (txrxm_Instanse != NULL)
        free(txrxm_Instanse);
    
    return RIO_OK;
}

/*routines for providing Initialization block state changes*/
/***************************************************************************
 * Function : txrxm_Event_Request
 *
 * Description: notify about new event request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Event_Request(
    RIO_HANDLE_T                handle, 
    RIO_TXRX_EVENT_T            event,/* event*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    /*instanse handler, temp for function result storing*/
    unsigned int load_Result;
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;
       
    /* check pointers */
    if (instanse == NULL || instanse->in_Reset == RIO_TRUE)
        return RIO_ERROR;
 
    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instanse->outbound_Data.out_Packet_Buffer.packet_Buffer_Head == 
        instanse->outbound_Data.out_Packet_Buffer.packet_Buffer_Top && 
        instanse->outbound_Data.out_Packet_Buffer.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_OUT_GET_EVENT_TEMP(instanse).place = FREE;
    }
    else
        RIO_TXRXM_OUT_GET_EVENT_TEMP(instanse).place = place;

    /*set other symbol fields*/
    RIO_TXRXM_OUT_GET_EVENT_TEMP(instanse).granule_Num = granule_Num;
    RIO_TXRXM_OUT_GET_EVENT_TEMP(instanse).event = event;

    if ((load_Result = RIO_TxRxM_Load_Event_Entry(instanse)) == RIO_ERROR)
        return RIO_ERROR;

    return RIO_OK;
}


/*routines for supporting current state of the Initialization block*/

/***************************************************************************
 * Function : rio_TxRxM_Set_State
 *
 * Description: Implemetation of state changing callback for the Initialization block
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_TxRxM_Set_State(
    RIO_HANDLE_T             handle,    /* callback context */
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
	unsigned int			 indicator_Parameter
    )
{
    /*this is a stub*/

    if ( (indicator_ID == RIO_INPUT_ERROR) && (indicator_Value == RIO_TRUE) )
    {
        return txrxm_Msg(handle, RIO_PL_MSG_NOTE_MSG, "Input error is detected");
    }
    
    if ( (indicator_ID == RIO_OUTPUT_ERROR) && (indicator_Value == RIO_TRUE) )
    {
        return txrxm_Msg(handle, RIO_PL_MSG_NOTE_MSG, "Output error is detected");
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : rio_TxRxM_Set_State
 *
 * Description: request to read configuration register of Initialization block
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int rio_TxRxM_Get_State(
    RIO_HANDLE_T             handle,    /* callback context */
    RIO_CONTEXT_T            context,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
	unsigned int			 *indicator_Parameter
    )
{
    RIO_TXRXM_DS_T *txrxm_Instanse = (RIO_TXRXM_DS_T*)handle;
    /*check the handle*/
    if (txrxm_Instanse == NULL)
        return RIO_ERROR;

    /*in case of buf status reqest this callback returns the constant value
    in case of RIO_TX_DATA_IN_PROGRESS RIO_RX_DATA_IN_PROGRESS this callback
    returns current state of sender/receiver 
    other requests are ignored in the txrx model*/
    if (indicator_ID == RIO_PARTNER_BUF_STATUS)
    {
        *indicator_Value = TXRX_BUF_STATUS_VAL;
        return RIO_OK;
    }

    if (indicator_ID == RIO_OUR_BUF_STATUS)
    {
        *indicator_Value = TXRX_BUF_STATUS_VAL;
        return RIO_OK;
    }

    /*inbound port in the progress of data receiving if inbound buffer
     is in the packet receiving state*/
    if (indicator_ID == RIO_RX_DATA_IN_PROGRESS)
    {
        *indicator_Value = txrxm_Instanse->inbound_Data.packet_Is_Receiving;
        return RIO_OK;
    }

    /*tx data in progress only if outbound queue is not empty*/
    if (indicator_ID == RIO_TX_DATA_IN_PROGRESS)
    {
        *indicator_Value = txrxm_Instanse->outbound_Data.packet_Is_Sending;
        return RIO_OK;
    }
    
    /*for other requests no action required in the txrx model*/
    return RIO_OK;
}


/***************************************************************************
 * Function : is_Handle_Is_Correct
 *
 * Description: Check that passed handle is the handle to the serial TxRx model
 *
 * Returns: RIO_TRUE if handle is correct RIO_FALSE otherwise
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T is_Handle_Is_Correct(RIO_HANDLE_T handle)
{
    RIO_TXRX_HANDLE_STORAGE_T * it; /*iterator*/

    it = par_Handles.next;

    while (it != NULL)
    {
        if (it->handle == handle) return RIO_TRUE;
        it = it->next;
    }
    return RIO_FALSE;
}

/***************************************************************************
 * Function : add_Handle
 *
 * Description: Adds handle to the handle storage
 *
 * Returns: Error code
 *
 * Notes: none
 *
 **************************************************************************/
static int add_Handle(RIO_HANDLE_T handle)
{
    RIO_TXRX_HANDLE_STORAGE_T * item; /*pointer to the new item*/
    item = (RIO_TXRX_HANDLE_STORAGE_T*)malloc(sizeof(RIO_TXRX_HANDLE_STORAGE_T));
    if (item == NULL) return RIO_ERROR;
    item->handle = handle;
    item->next = par_Handles.next;
    par_Handles.next = item;
    return RIO_OK;
}

/***************************************************************************
 * Function : remove_Handle_From_Storage
 *
 * Description: Removes specified handle from the storage
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void remove_Handle_From_Storage(RIO_HANDLE_T handle)
{
    RIO_TXRX_HANDLE_STORAGE_T * it; /*iterator*/
    RIO_TXRX_HANDLE_STORAGE_T * id; /*iterator to items for deleting*/

    if (par_Handles.next == NULL) return;
    /*check first element*/
    if (par_Handles.next->handle == handle)
    {
        id = par_Handles.next;
        par_Handles.next = id->next;
        free(id);
        return;
    }
    /*if this is not first element then search in the others element*/
    it = par_Handles.next;
    while (it->next != NULL)
    {
        if (it->next->handle == handle)
        {
            id = it->next;
            it->next = id->next;
            free(id);
            return;
        }
        it = it->next;
    }
    
}

/***************************************************************************
 * Function : txrxm_Lost_Sync
 *
 * Description: change training machine state
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Lost_Sync(RIO_HANDLE_T handle)
{
    /* instanse handle and the result of callback invokation */
    RIO_TXRXM_DS_T *instanse = (RIO_TXRXM_DS_T*)handle;

    if (is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        /*printf(incorrect_Handle_Message);*/
        return RIO_ERROR;
    }
    return (instanse->init_Interface.rio_PL_Par_Init_Block_Machine_Change_State(
        instanse->init_Instanse, RIO_PAR_INIT_BLOCK_LOST_SYNC));
}
