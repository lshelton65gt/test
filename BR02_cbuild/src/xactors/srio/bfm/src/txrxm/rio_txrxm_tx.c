/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/rio_txrxm_tx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model transmitter source
*
* Notes:        
*
******************************************************************************/
#include <stdio.h>
#include <string.h>

#include "rio_txrxm_tx.h"
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_PACKET_TEMP
 *
 * Description: return address of an outbound packet buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_PACKET_TEMP(handle) ((handle)->outbound_Data.out_Packet_Buffer.packet_Temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_GRANULE_TEMP
 *
 * Description: return address of an outbound granule buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_GRANULE_TEMP(handle) ((handle)->outbound_Data.out_Granule_Buffer.granule_Temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_EVENT_TEMP
 *
 * Description: return address of an outbound event buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_EVENT_TEMP(handle) ((handle)->outbound_Data.out_Event_Buffer.event_Temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_SYMBOL_TEMP
 *
 * Description: return address of an outbound symbol buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_SYMBOL_TEMP(handle) ((handle)->outbound_Data.out_Symbol_Buffer.symbol_Temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_PACKET_ENTRY
 *
 * Description: return address of an outbound packet buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_PACKET_ENTRY(handle, i) ((handle)->outbound_Data.out_Packet_Buffer.packet_Buffer + (i))
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_GRANULE_ENTRY
 *
 * Description: return address of an outbound granule buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, i) ((handle)->outbound_Data.out_Granule_Buffer.granule_Buffer + (i))
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_EVENT_ENTRY
 *
 * Description: return address of an outbound event buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_EVENT_ENTRY(handle, i) ((handle)->outbound_Data.out_Event_Buffer.event_Buffer + (i))
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_SYMBOL_ENTRY
 *
 * Description: return address of an outbound symbol buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_SYMBOL_ENTRY(handle, i) ((handle)->outbound_Data.out_Symbol_Buffer.symbol_Buffer + (i))

/***************************************************************************
 * Function : RIO_OUT_GET_QUEUE_ENTRY
 *
 * Description: return address of an outbound symbol buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_OUT_GET_QUEUE_ENTRY(handle, i) ((handle)->outbound_Data.out_Queue.queue_Buffer + (i))
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_ARB
 *
 * Description: return outbound arbiter
 *
 * Returns: outbound arbiter
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_OUT_GET_ARB(handle) ((handle)->outbound_Data.outbound_Packet_Arb)
/***************************************************************************
 * Function : Invoke_End_Packet_Callback
 *
 * Description: return outbound arbiter
 *
 * Returns: outbound arbiter
 *
 * Notes: none
 *
 **************************************************************************/
#define Invoke_End_Packet_Callback(handle) \
{\
    if ((handle)->ext_Interfaces.rio_TxRxM_Last_Packet_Granule != NULL)\
        ((handle)->ext_Interfaces.rio_TxRxM_Last_Packet_Granule)(\
            (handle)->ext_Interfaces.hook_Context);\
}

/*add element to queue*/
static void out_Queue_Add(
    RIO_TXRXM_DS_T *handle,
    unsigned int buffer_Index,
    RIO_TXRXM_ELEMENT_T element_Type
    );

/*add element to high prio queue*/
static void out_HP_Queue_Add(
    RIO_TXRXM_DS_T *handle, 
    unsigned int buffer_Index, 
    RIO_TXRXM_ELEMENT_T element_Type);

/*delete element from queue*/
static void out_Queue_Del(RIO_TXRXM_DS_T *handle);

/*check if it's time to get new element to send*/
static RIO_BOOL_T get_New_Element(
    RIO_TXRXM_DS_T *handle,
    RIO_TXRXM_ELEMENT_T *element_Type
    );

/*start new packet to send*/
static void start_New_Packet(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule
    );

/*load packet sending buffer with new packet stream*/
static void load_Packet_Arbiter(
    RIO_TXRXM_DS_T *handle,
    unsigned int index
    );

/*convert packet struct to stream*/
void TxRxM_Out_Load_Packet_From_Struct(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/*load packet from stream*/
void TxRxM_Out_Load_Packet_From_Stream(
    RIO_TXRXM_OUT_PACKET_STREAM_T *packet_Stream, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );


/*get new symbol for transmitting*/
static void get_Symbol(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule,
    RIO_BOOL_T is_HP
    );

/*get new granule for transmitting*/
static void get_Granule(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule
    );

/*performs event processing*/
static void get_Event(RIO_TXRXM_DS_T * handle);

/*send packet part*/
static void send_Packet_Part(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule
    );

/*get default granule for transmitting*/
static void get_Default_Granule(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule
    );

/*inintialize unit data structure*/
static void init_Granule_Buffer(RIO_TXRXM_OUT_GRANULE_BUFFER_T *buffer);
static void init_Packet_Buffer(RIO_TXRXM_OUT_PACKET_BUFFER_T *buffer);
static void init_Symbol_Buffer(RIO_TXRXM_OUT_SYMBOL_BUFFER_T *buffer);
static void init_Event_Buffer(RIO_TXRXM_OUT_EVENT_BUFFER_T *buffer);
static void init_Queue(RIO_TXRXM_OUT_QUEUE_T *queue);


/***************************************************************************
 * Function : RIO_TxRxM_Load_Packet_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Load_Packet_Entry(RIO_TXRXM_DS_T *handle)
{
    unsigned int buf_Top;

    if (handle == NULL)
        return RIO_ERROR;

    if (handle->outbound_Data.out_Packet_Buffer.buffer_Full == RIO_TRUE)
        return RIO_ERROR;

    buf_Top = handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Top;

    /* load new entry to the buffer */
    *RIO_TXRXM_OUT_GET_PACKET_ENTRY(handle, buf_Top) = RIO_TXRXM_OUT_GET_PACKET_TEMP(handle);
            
    /* update sending queue */
    out_Queue_Add(handle, buf_Top, RIO_TXRXM_ELEMENT_PACKET);

    handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Top = 
        (buf_Top + 1) % handle->inst_Param_Set.packets_Buffer_Size;

    if (handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Top == 
        handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Head)
        handle->outbound_Data.out_Packet_Buffer.buffer_Full = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : load_High_Prio_Symbol_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded
 *
 * Notes: none
 *
 **************************************************************************/
static int load_High_Prio_Symbol_Entry(RIO_TXRXM_DS_T *handle)
{
    int buf_Top;

    if (handle->outbound_Data.out_High_Prio_Symbol_Buffer.buffer_Full == RIO_TRUE)
        return RIO_ERROR;

    buf_Top = handle->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer_Top;

    /* load new entry to the buffer */
    handle->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer[buf_Top] =
        handle->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Temp;

    /* update sending queue */
    out_HP_Queue_Add(handle, buf_Top, RIO_TXRXM_ELEMENT_HP_SYMBOL);

    handle->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer_Top = 
        (buf_Top + 1) % handle->inst_Param_Set.symbols_Buffer_Size;

    if (handle->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer_Top == 
        handle->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Buffer_Head)
        handle->outbound_Data.out_High_Prio_Symbol_Buffer.buffer_Full = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_TxRxM_Load_Symbol_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Load_Symbol_Entry(RIO_TXRXM_DS_T *handle)
{
    unsigned int buf_Top;

    if (handle == NULL)
        return RIO_ERROR;

    /*check that this is high priority symbol*/
    if (RIO_TXRXM_OUT_GET_SYMBOL_TEMP(handle).granule_Num == (unsigned int)-1)
    {
        handle->outbound_Data.out_High_Prio_Symbol_Buffer.symbol_Temp = RIO_TXRXM_OUT_GET_SYMBOL_TEMP(handle);
        return load_High_Prio_Symbol_Entry(handle);
    }

    if (handle->outbound_Data.out_Symbol_Buffer.buffer_Full == RIO_TRUE)
        return RIO_ERROR;

    buf_Top = handle->outbound_Data.out_Symbol_Buffer.symbol_Buffer_Top;

    /* load new entry to the buffer */
    *RIO_TXRXM_OUT_GET_SYMBOL_ENTRY(handle, buf_Top) = RIO_TXRXM_OUT_GET_SYMBOL_TEMP(handle);

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((RIO_TXRXM_OUT_GET_SYMBOL_TEMP(handle).place == EMBEDDED ||
        RIO_TXRXM_OUT_GET_SYMBOL_TEMP(handle).place == CANCELING)  &&
        handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Head == 
        handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Top && 
        handle->outbound_Data.out_Packet_Buffer.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_OUT_GET_SYMBOL_ENTRY(handle, buf_Top)->place = FREE;
    }
            
    /* update sending queue */
    out_Queue_Add(handle, buf_Top, RIO_TXRXM_ELEMENT_SYMBOL);

    handle->outbound_Data.out_Symbol_Buffer.symbol_Buffer_Top = 
        (buf_Top + 1) % handle->inst_Param_Set.symbols_Buffer_Size;

    if (handle->outbound_Data.out_Symbol_Buffer.symbol_Buffer_Top == 
        handle->outbound_Data.out_Symbol_Buffer.symbol_Buffer_Head)
        handle->outbound_Data.out_Symbol_Buffer.buffer_Full = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_TxRxM_Load_Granule_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Load_Granule_Entry(RIO_TXRXM_DS_T *handle)
{
    unsigned int buf_Top;

    if (handle == NULL)
        return RIO_ERROR;

    if (handle->outbound_Data.out_Granule_Buffer.buffer_Full == RIO_TRUE)
        return RIO_ERROR;

    buf_Top = handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Top;

    /* load new entry to the buffer */
    *RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Top) = RIO_TXRXM_OUT_GET_GRANULE_TEMP(handle);

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((RIO_TXRXM_OUT_GET_GRANULE_TEMP(handle).place == EMBEDDED ||
        RIO_TXRXM_OUT_GET_GRANULE_TEMP(handle).place == CANCELING)  &&
        handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Head == 
        handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Top && 
        handle->outbound_Data.out_Packet_Buffer.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Top)->place = FREE;
    }
            
    /* update sending queue */
    out_Queue_Add(handle, buf_Top, RIO_TXRXM_ELEMENT_GRANULE);

    handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Top = 
        (buf_Top + 1) % handle->inst_Param_Set.granules_Buffer_Size;

    if (handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Top == 
        handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Head)
        handle->outbound_Data.out_Granule_Buffer.buffer_Full = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_TxRxM_Load_Event_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Load_Event_Entry(RIO_TXRXM_DS_T *handle)
{
    unsigned int buf_Top;

    if (handle == NULL)
        return RIO_ERROR;

    if (handle->outbound_Data.out_Event_Buffer.buffer_Full == RIO_TRUE)
        return RIO_ERROR;

    buf_Top = handle->outbound_Data.out_Event_Buffer.event_Buffer_Top;

    /* load new entry to the buffer */
    *RIO_TXRXM_OUT_GET_EVENT_ENTRY(handle, buf_Top) = RIO_TXRXM_OUT_GET_EVENT_TEMP(handle);

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((RIO_TXRXM_OUT_GET_EVENT_TEMP(handle).place == EMBEDDED ||
        RIO_TXRXM_OUT_GET_EVENT_TEMP(handle).place == CANCELING)  &&
        handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Head == 
        handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Top && 
        handle->outbound_Data.out_Packet_Buffer.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_OUT_GET_EVENT_ENTRY(handle, buf_Top)->place = FREE;
    }
            
    /* update sending queue */
    out_Queue_Add(handle, buf_Top, RIO_TXRXM_ELEMENT_EVENT);

    handle->outbound_Data.out_Event_Buffer.event_Buffer_Top = 
        (buf_Top + 1) % handle->inst_Param_Set.event_Buffer_Size;

    if (handle->outbound_Data.out_Event_Buffer.event_Buffer_Top == 
        handle->outbound_Data.out_Event_Buffer.event_Buffer_Head)
        handle->outbound_Data.out_Event_Buffer.buffer_Full = RIO_TRUE;

    return RIO_OK;
}


/***************************************************************************
 * Function : out_Queue_Add
 *
 * Description: add element to sending queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Queue_Add(RIO_TXRXM_DS_T *handle, unsigned int buffer_Index, RIO_TXRXM_ELEMENT_T element_Type)
{
    if (handle == NULL)
        return;

    RIO_OUT_GET_QUEUE_ENTRY(handle, handle->outbound_Data.out_Queue.queue_Top)->element_Place_In_Buffer
        = buffer_Index;
    RIO_OUT_GET_QUEUE_ENTRY(handle, handle->outbound_Data.out_Queue.queue_Top)->element_Type
        = element_Type;

    handle->outbound_Data.out_Queue.queue_Top = 
        (handle->outbound_Data.out_Queue.queue_Top + 1) %
        (handle->inst_Param_Set.packets_Buffer_Size + 
        handle->inst_Param_Set.symbols_Buffer_Size +
        handle->inst_Param_Set.granules_Buffer_Size);

    if (handle->outbound_Data.out_Queue.queue_Empty == RIO_TRUE)
        handle->outbound_Data.out_Queue.queue_Empty = RIO_FALSE;
    
    return;
}

/***************************************************************************
 * Function : out_HP_Queue_Add
 *
 * Description: add element to sending high priority queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_HP_Queue_Add(RIO_TXRXM_DS_T *handle, unsigned int buffer_Index, RIO_TXRXM_ELEMENT_T element_Type)
{
    int top; /*top of the queue*/

    if (handle == NULL)
        return;

    top = handle->outbound_Data.out_High_Prio_Queue.queue_Top;
    handle->outbound_Data.out_High_Prio_Queue.queue_Buffer[top].element_Place_In_Buffer
        = buffer_Index;
    handle->outbound_Data.out_High_Prio_Queue.queue_Buffer[top].element_Type
        = element_Type;

    handle->outbound_Data.out_High_Prio_Queue.queue_Top = 
        (top + 1) % handle->inst_Param_Set.symbols_Buffer_Size;

    if (handle->outbound_Data.out_High_Prio_Queue.queue_Empty == RIO_TRUE)
        handle->outbound_Data.out_High_Prio_Queue.queue_Empty = RIO_FALSE;
    
    return;
}

/***************************************************************************
 * Function : out_Queue_Del
 *
 * Description: delete sent element from sending queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Queue_Del(RIO_TXRXM_DS_T *handle)
{
    if (handle == NULL)
        return;

    handle->outbound_Data.out_Queue.queue_Head = 
        (handle->outbound_Data.out_Queue.queue_Head + 1) %
        (handle->inst_Param_Set.packets_Buffer_Size + 
        handle->inst_Param_Set.symbols_Buffer_Size +
        handle->inst_Param_Set.granules_Buffer_Size);

    if (handle->outbound_Data.out_Queue.queue_Head == 
        handle->outbound_Data.out_Queue.queue_Top)
        handle->outbound_Data.out_Queue.queue_Empty = RIO_TRUE;
    
    return;
}
/***************************************************************************
 * Function : RIO_TxRxM_Granule_For_Transmitting
 *
 * Description: main transmitter loop
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Granule_For_Transmitting(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule)
{

    RIO_BOOL_T get_Element;/*help to store return value*/
    RIO_TXRXM_ELEMENT_T element_Type;

    if (handle == NULL || granule == NULL)
        return;
    /*
     * we check all information source: all queues
     * if we are sending a packet ask for new portion
     */

    /*check if new element from queue is needed and get it*/
    if ((get_Element = get_New_Element(handle, &element_Type)) == RIO_TRUE)
        switch (element_Type)
        {
            case RIO_TXRXM_ELEMENT_PACKET:
                start_New_Packet(handle, granule);
            return;
            case RIO_TXRXM_ELEMENT_SYMBOL:
                get_Symbol(handle, granule, RIO_FALSE);
            return;
            case RIO_TXRXM_ELEMENT_GRANULE:
                get_Granule(handle, granule);
            return;
            case RIO_TXRXM_ELEMENT_EVENT:
                /*event doesn't produce a data so necessary
                remove event from queue, perform necessary action
                and reinvoke this routine for granule suppliment*/
                get_Event(handle);
                RIO_TxRxM_Granule_For_Transmitting(handle, granule);
                return;
            case RIO_TXRXM_ELEMENT_HP_SYMBOL:
                get_Symbol(handle, granule, RIO_TRUE);
                return;
            default:
            break;
        }

    if (handle->outbound_Data.packet_Is_Sending == RIO_TRUE)
    {
        send_Packet_Part(handle, granule);
        return;
    }

    get_Default_Granule(handle, granule);
    return;
}

/***************************************************************************
 * Function : get_New_Element
 *
 * Description: check if it's time to get new element to send
 *
 * Returns: RIO_TRUE if new element is needed
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T get_New_Element(RIO_TXRXM_DS_T *handle, RIO_TXRXM_ELEMENT_T *element_Type)
{
    /*help temporaries*/
    unsigned long queue_Head;
    unsigned int buf_Head;

    if (handle == NULL)
        return RIO_FALSE;

    /*first of all check the high priority queue*/
    if (handle->outbound_Data.out_High_Prio_Queue.queue_Empty == RIO_FALSE)
    {
        RIO_TXRXM_OUT_SYMBOL_BUFFER_T * pbuf; /*pointer to the high prio buf*/

        queue_Head = handle->outbound_Data.out_High_Prio_Queue.queue_Head;
        pbuf = &(handle->outbound_Data.out_High_Prio_Symbol_Buffer);

        *element_Type = 
            handle->outbound_Data.out_High_Prio_Queue.queue_Buffer[queue_Head].element_Type;

        buf_Head = pbuf->symbol_Buffer_Head;

        /*if symbol is not free and packet is sending then necessary to wait*/
        if ( !(pbuf->symbol_Buffer[buf_Head].place == FREE &&
            handle->outbound_Data.packet_Is_Sending == RIO_TRUE))
        {
        /*it is necessary to notify about finishing packet to
        the lower layer for it can correctly invoke packet_finishing callback
        (mantis 71 implementation)*/
            if ( pbuf->symbol_Buffer[buf_Head].place == CANCELING &&
                handle->outbound_Data.packet_Is_Sending == RIO_TRUE)
                Invoke_End_Packet_Callback(handle)

            return RIO_TRUE;
        }
    }


    /*if queue empty, need to get default granule*/
    if (handle->outbound_Data.out_Queue.queue_Empty == RIO_TRUE)
        return RIO_FALSE;

    queue_Head = handle->outbound_Data.out_Queue.queue_Head;
    /*if packet is not currently sending, first queue element is taken*/
    if (handle->outbound_Data.packet_Is_Sending == RIO_FALSE)
    {
        *element_Type = RIO_OUT_GET_QUEUE_ENTRY(handle, queue_Head)->element_Type;
        return RIO_TRUE;
    }
    /*if packet is sending, but Symbol is first in queue(can be embedded or cancel packet)*/
    else if (RIO_OUT_GET_QUEUE_ENTRY(handle, queue_Head)->element_Type == RIO_TXRXM_ELEMENT_SYMBOL)
    {
        buf_Head = handle->outbound_Data.out_Symbol_Buffer.symbol_Buffer_Head;

        /*cur_Pos - in byte count, granule_Num - in granule count*/
        if ((RIO_TXRXM_OUT_GET_SYMBOL_ENTRY(handle, buf_Head)->place == EMBEDDED || 
            RIO_TXRXM_OUT_GET_SYMBOL_ENTRY(handle, buf_Head)->place == CANCELING) && 
            (((RIO_TXRXM_OUT_GET_SYMBOL_ENTRY(handle, buf_Head)->granule_Num + 1) * RIO_TXRXM_BYTE_CNT_IN_WORD) == 
            RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos))
        {
            *element_Type = RIO_TXRXM_ELEMENT_SYMBOL;
            /*The request for the next granule is called before the 
            last character issuing. So callback that is called from
            this place will show event that occure on the next clock*/
            if (RIO_TXRXM_OUT_GET_SYMBOL_ENTRY(handle, buf_Head)->place == CANCELING)
                Invoke_End_Packet_Callback(handle)

            return RIO_TRUE;
        }
        return RIO_FALSE;
        
    }
    /*if packet is sending, but Granule is first in queue(can be embedded or cancel packet)*/
    else if (RIO_OUT_GET_QUEUE_ENTRY(handle, queue_Head)->element_Type == RIO_TXRXM_ELEMENT_GRANULE)
    {
        buf_Head = handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Head;
        /*cur_Pos - in byte count, granule_Num - in granule count*/
        if ((RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Head)->place == EMBEDDED || 
            RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Head)->place == CANCELING) && 
            (((RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Head)->granule_Num + 1) * RIO_TXRXM_BYTE_CNT_IN_WORD) == 
            RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos))
        {
            *element_Type = RIO_TXRXM_ELEMENT_GRANULE;
            return RIO_TRUE;
        }
        return RIO_FALSE;
    }
    else if (RIO_OUT_GET_QUEUE_ENTRY(handle, queue_Head)->element_Type == RIO_TXRXM_ELEMENT_EVENT)
    {
        buf_Head = handle->outbound_Data.out_Event_Buffer.event_Buffer_Head;
        /*cur_Pos - in byte count, granule_Num - in granule count*/
        if ((RIO_TXRXM_OUT_GET_EVENT_ENTRY(handle, buf_Head)->place == EMBEDDED || 
            RIO_TXRXM_OUT_GET_EVENT_ENTRY(handle, buf_Head)->place == CANCELING) && 
            (((RIO_TXRXM_OUT_GET_EVENT_ENTRY(handle, buf_Head)->granule_Num + 1) * RIO_TXRXM_BYTE_CNT_IN_WORD) == 
            RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos))
        {
            *element_Type = RIO_TXRXM_ELEMENT_GRANULE;
            return RIO_TRUE;
        }
        return RIO_FALSE;
    }

    return RIO_FALSE;
}
/***************************************************************************
 * Function : start_New_Packet
 *
 * Description: start new packet transmission
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void start_New_Packet(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule)
{
    /*check if parameters wrong*/
    if (handle == NULL || granule == NULL)
        return;

    /*load packet arbiter with new packet*/
    load_Packet_Arbiter(handle, handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Head);

    /* modify arbiter*/
    RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos = 0;

    /*update packet buffer params*/
    handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Head = 
        (handle->outbound_Data.out_Packet_Buffer.packet_Buffer_Head + 1)%
        handle->inst_Param_Set.packets_Buffer_Size;
    handle->outbound_Data.out_Packet_Buffer.buffer_Full = RIO_FALSE;

    /*delete sending packet from queue*/
    out_Queue_Del(handle);

    handle->outbound_Data.packet_Is_Sending = RIO_TRUE;
    
    /* prepare the first granule  -consist from 4 bytes with corresponding offsets from 0 to 3*/
    granule->granule_Type = RIO_GR_NEW_PACKET;/*granule type*/
    granule->granule_Frame = RIO_TRUE; /*toggle frame at first packet granule*/
    {
        unsigned int i; /* loop counter */

        for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
            granule->granule_Date[i] = RIO_TXRXM_OUT_GET_ARB(handle).packet_Stream[i];
    }

    /* update the arbiter state*/
    RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos = RIO_TXRXM_BYTE_CNT_IN_WORD;
    return;
}
/***************************************************************************
 * Function : load_Packet_Arbiter
 *
 * Description: load packet arbiter with packet stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_Packet_Arbiter(RIO_TXRXM_DS_T *handle, unsigned int index)
{
    /*check if parameters wrong*/
    if (handle == NULL )
        return;

    /*if packet was requested as struct*/
    if (RIO_TXRXM_OUT_GET_PACKET_ENTRY(handle, index)->as_Struct == RIO_TRUE)
    {
        /*convert packet struct to stream*/
        TxRxM_Out_Load_Packet_From_Struct(&(RIO_TXRXM_OUT_GET_PACKET_ENTRY(handle, index)->packet_Struct),
            &RIO_TXRXM_OUT_GET_ARB(handle));
    }
    else
    {
        /*load packet stream*/
        TxRxM_Out_Load_Packet_From_Stream(&(RIO_TXRXM_OUT_GET_PACKET_ENTRY(handle, index)->packet_Stream),
            &RIO_TXRXM_OUT_GET_ARB(handle));
    }

    if (handle->ext_Interfaces.rio_TxRxM_Packet_To_Send != NULL)
    {
        handle->ext_Interfaces.rio_TxRxM_Packet_To_Send(handle->ext_Interfaces.hook_Context,
            RIO_TXRXM_OUT_GET_ARB(handle).packet_Stream,
            RIO_TXRXM_OUT_GET_ARB(handle).packet_Len);
    }

    return;
}

/***************************************************************************
 * Function : send_Packet_Part
 *
 * Description: obtain new data for transmitting packet
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void send_Packet_Part(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned int i; /* loop counter */
    /*check if parameters wrong*/
    if (handle == NULL || granule == NULL)
        return;

    if (handle->outbound_Data.packet_Is_Sending == RIO_TRUE)
    {
        /* load new data to the granule and update position */
        granule->granule_Type = RIO_GR_DATA;/*granule type*/
        granule->granule_Frame = RIO_FALSE;/*frame does not toggle while packet transmittion*/

        for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
            if ((RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos + i) >= RIO_TXRXM_OUT_GET_ARB(handle).packet_Len)
                granule->granule_Date[i] = 0;
            else
                granule->granule_Date[i] = 
                    RIO_TXRXM_OUT_GET_ARB(handle).packet_Stream[RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos + i];

        RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos += RIO_TXRXM_BYTE_CNT_IN_WORD;
    
    
        /* check if the last portion of packet is sending */
    
        if (RIO_TXRXM_OUT_GET_ARB(handle).cur_Pos >= RIO_TXRXM_OUT_GET_ARB(handle).packet_Len)
        {
            /*reset packet sending flag*/
            handle->outbound_Data.packet_Is_Sending = RIO_FALSE;
            /* clear arbiter state */
            RIO_TxRxM_Out_Init_Arbiter(&RIO_TXRXM_OUT_GET_ARB(handle));

            /*The request for the next granule is called before the 
            last character issuing. So callback that is called from
            this place will show event that occure on the next clock*/
            Invoke_End_Packet_Callback(handle)
        }
    }
    return;
}

/***************************************************************************
 * Function : RIO_TxRxM_Out_Init_Arbiter
 *
 * Description: initialize arbiter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Out_Init_Arbiter(RIO_TXRXM_PACKET_ARB_T *element)
{
    if (element == NULL)
        return;

    /*initialize arbiter fields */
    element->cur_Pos = 0;
    element->packet_Len = 0;
    return;
}

/***************************************************************************
 * Function : get_Symbol
 *
 * Description: get new symbol for sending
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void get_Symbol(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule, RIO_BOOL_T is_HP)
{
    unsigned int buf_Index;
    RIO_TXRXM_OUT_SYMBOL_BUFFER_T * pbuf; /*pointer to the symbol buffer*/
    /*check if parameters wrong*/
    if (handle == NULL || granule == NULL)
        return;

    pbuf = is_HP == RIO_TRUE ? &(handle->outbound_Data.out_High_Prio_Symbol_Buffer) :
        &(handle->outbound_Data.out_Symbol_Buffer);

    buf_Index = pbuf->symbol_Buffer_Head;

    /*if symbol place is canceling, stop packet transmittion*/
    if (pbuf->symbol_Buffer[buf_Index].place == CANCELING)
    {
        handle->outbound_Data.packet_Is_Sending = RIO_FALSE;
        RIO_TxRxM_Out_Init_Arbiter(&RIO_TXRXM_OUT_GET_ARB(handle));
    }

    /*form granule*/
    if (pbuf->symbol_Buffer[buf_Index].granule_Type != RIO_GR_TRAINING)
    {
        /*form symbol fields*/
        granule->granule_Struct.ack_ID = pbuf->symbol_Buffer[buf_Index].symbol.ackID;
        granule->granule_Struct.buf_Status = pbuf->symbol_Buffer[buf_Index].symbol.buf_Status;
        granule->granule_Struct.stype = pbuf->symbol_Buffer[buf_Index].symbol.stype;
        granule->granule_Struct.s = pbuf->symbol_Buffer[buf_Index].symbol.s;
    
        /*form field which was secded before - used offsets and masks are unique and SHALL be 
        * changed in case of specification changing only HERE!!!
        */

        granule->granule_Struct.secded = 0;
        granule->granule_Struct.secded |= 
            ((pbuf->symbol_Buffer[buf_Index].symbol.rsrv_1 & 0x01) << 4);
        granule->granule_Struct.secded |= 
            ((pbuf->symbol_Buffer[buf_Index].symbol.s_Parity & 0x01) << 3);
        granule->granule_Struct.secded |= 
            (pbuf->symbol_Buffer[buf_Index].symbol.rsrv_2 & 0x07);
    }
   
    /*set type granule type: training or unrecognized*/
    granule->granule_Type = pbuf->symbol_Buffer[buf_Index].granule_Type;

    granule->granule_Frame = RIO_TRUE;/*frame toggles at start symbol transmittion*/

     /*update symbol buffer params*/
    pbuf->symbol_Buffer_Head = 
        (pbuf->symbol_Buffer_Head + 1)%
        handle->inst_Param_Set.symbols_Buffer_Size;
    pbuf->buffer_Full = RIO_FALSE;

    /*delete sending symbol from queue*/
    if (is_HP == RIO_TRUE)
    {
        handle->outbound_Data.out_High_Prio_Queue.queue_Head = 
            (handle->outbound_Data.out_High_Prio_Queue.queue_Head + 1) %
            handle->inst_Param_Set.symbols_Buffer_Size;
        if (handle->outbound_Data.out_High_Prio_Queue.queue_Head == 
            handle->outbound_Data.out_High_Prio_Queue.queue_Top)
            handle->outbound_Data.out_High_Prio_Queue.queue_Empty = RIO_TRUE;
    }
    else
        out_Queue_Del(handle);
    return;
}

/***************************************************************************
 * Function : get_Granule
 *
 * Description: get new granule for transmitting
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void get_Granule(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned int buf_Index;
    /*check if parameters wrong*/
    if (handle == NULL || granule == NULL)
        return;

    buf_Index = handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Head;

    /*set frame*/
    granule->granule_Frame = RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Index)->has_Frame_Toggled;
    /*convert granule data to array and set - hardcoded numbers are used only here to convert granule into array*/
    granule->granule_Date[0] = 
        (RIO_UCHAR_T)((RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Index)->granule & 0xff000000) >> RIO_TXRXM_BITS_IN_BYTE * 3);
    granule->granule_Date[1] = 
        (RIO_UCHAR_T)((RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Index)->granule & 0x00ff0000) >> RIO_TXRXM_BITS_IN_BYTE * 2);
    granule->granule_Date[2] = 
        (RIO_UCHAR_T)((RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Index)->granule & 0x0000ff00) >> RIO_TXRXM_BITS_IN_BYTE);
    granule->granule_Date[3] = 
        (RIO_UCHAR_T)(RIO_TXRXM_OUT_GET_GRANULE_ENTRY(handle, buf_Index)->granule & 0x000000ff);

    /*set granule type*/
    granule->granule_Type = RIO_GR_DATA;

    /*update symbol buffer params*/
    handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Head = 
        (handle->outbound_Data.out_Granule_Buffer.granule_Buffer_Head + 1)%
        handle->inst_Param_Set.granules_Buffer_Size;
    handle->outbound_Data.out_Granule_Buffer.buffer_Full = RIO_FALSE;

    /*delete sending symbol from queue*/
    out_Queue_Del(handle);
    return;
}

/***************************************************************************
 * Function : get_Event
 *
 * Description: performs action which is necessary for this event
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void get_Event(RIO_TXRXM_DS_T * handle)
{
    unsigned int buf_Index;
    RIO_BOOL_T is_Switch_On;
    /*check if parameters wrong*/
    if (handle == NULL)
        return;

    buf_Index = handle->outbound_Data.out_Event_Buffer.event_Buffer_Head;

    /*performs necessary event*/

    
    if (handle->outbound_Data.out_Event_Buffer.event_Buffer[buf_Index].event == 
        RIO_TXRXM_TURN_TRAINING_MODULE_ON)
        is_Switch_On = RIO_TRUE;
    else
        is_Switch_On = RIO_FALSE;

    handle->init_Interface.rio_PL_Par_Init_Block_Machine_Management(
        handle->init_Instanse, is_Switch_On);


    /*update Event buffer params*/
    handle->outbound_Data.out_Event_Buffer.event_Buffer_Head = 
        (handle->outbound_Data.out_Event_Buffer.event_Buffer_Head + 1)%
        handle->inst_Param_Set.event_Buffer_Size;
    handle->outbound_Data.out_Event_Buffer.buffer_Full = RIO_FALSE;

    /*delete sending symbol from queue*/
    out_Queue_Del(handle);
    return;
}


/***************************************************************************
 * Function : get_Default_Granule
 *
 * Description: get default granule for transmitting
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void get_Default_Granule(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule)
{
    /*check if parameters wrong*/
    if (handle == NULL || granule == NULL)
        return;

    /*set frame*/
    granule->granule_Frame = handle->parameters_Set.default_Frame_Toggle;
    /*convert granule data to array and set - hardcoded numbers are used only here*/
    granule->granule_Date[0] = 
        (RIO_UCHAR_T)((handle->parameters_Set.default_Granule & 0xff000000) >> RIO_TXRXM_BITS_IN_BYTE * 3);
    granule->granule_Date[1] = 
        (RIO_UCHAR_T)((handle->parameters_Set.default_Granule & 0x00ff0000) >> RIO_TXRXM_BITS_IN_BYTE * 2);
    granule->granule_Date[2] = 
        (RIO_UCHAR_T)((handle->parameters_Set.default_Granule & 0x0000ff00) >> RIO_TXRXM_BITS_IN_BYTE);
    granule->granule_Date[3] = 
        (RIO_UCHAR_T)(handle->parameters_Set.default_Granule & 0x000000ff);

    /*set granule type*/
    granule->granule_Type = RIO_GR_DATA;
    return;
}
/***************************************************************************
 * Function : RIO_TxRxM_Tx_Init
 *
 * Description: init transmitter
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#pragma check_stack(on)
void RIO_TxRxM_Tx_Init(RIO_TXRXM_DS_T *handle)
{
   /*check pointer*/
    if (handle == NULL)
        return;
    /*init packet arbiter*/
    RIO_TxRxM_Out_Init_Arbiter(&RIO_TXRXM_OUT_GET_ARB(handle));     
    /*init data flag*/
    handle->outbound_Data.packet_Is_Sending = RIO_FALSE;

    init_Packet_Buffer(&handle->outbound_Data.out_Packet_Buffer);
    init_Symbol_Buffer(&handle->outbound_Data.out_Symbol_Buffer);
    init_Symbol_Buffer(&handle->outbound_Data.out_High_Prio_Symbol_Buffer);
    init_Granule_Buffer(&handle->outbound_Data.out_Granule_Buffer);
    init_Event_Buffer(&handle->outbound_Data.out_Event_Buffer);
    init_Queue(&handle->outbound_Data.out_Queue);
    init_Queue(&handle->outbound_Data.out_High_Prio_Queue);
    return;
}

/***************************************************************************
 * Function : init_Packet_Buffer
 *
 * Description: init packet buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Packet_Buffer(RIO_TXRXM_OUT_PACKET_BUFFER_T *buffer)
{
    if (buffer == NULL)
        return;

    /*init buffer params*/
    buffer->buffer_Full = RIO_FALSE;
    buffer->packet_Buffer_Head = buffer->packet_Buffer_Top = 0;

    return;
}

/***************************************************************************
 * Function : init_Symbol_Buffer
 *
 * Description: init symbol buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Symbol_Buffer(RIO_TXRXM_OUT_SYMBOL_BUFFER_T *buffer)
{
    if (buffer == NULL)
        return;

    /*init buffer params*/
    buffer->buffer_Full = RIO_FALSE;
    buffer->symbol_Buffer_Head = buffer->symbol_Buffer_Top = 0;

    return;
}

/***************************************************************************
 * Function : init_Granule_Buffer
 *
 * Description: init granule buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Granule_Buffer(RIO_TXRXM_OUT_GRANULE_BUFFER_T *buffer)
{
    if (buffer == NULL)
        return;

    /*init buffer params*/
    buffer->buffer_Full = RIO_FALSE;
    buffer->granule_Buffer_Head = buffer->granule_Buffer_Top = 0;

    return;
}

/***************************************************************************
 * Function : init_Event_Buffer
 *
 * Description: init event buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Event_Buffer(RIO_TXRXM_OUT_EVENT_BUFFER_T *buffer)
{
    if (buffer == NULL)
        return;

    /*init buffer params*/
    buffer->buffer_Full = RIO_FALSE;
    buffer->event_Buffer_Head = buffer->event_Buffer_Top = 0;
}

/***************************************************************************
 * Function : init_Queue
 *
 * Description: init sending queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Queue(RIO_TXRXM_OUT_QUEUE_T *queue)
{
    if (queue == NULL)
        return;

    /*init queue params*/
    queue->queue_Empty = RIO_TRUE;
    queue->queue_Head = queue->queue_Top = 0;

    return;
}



