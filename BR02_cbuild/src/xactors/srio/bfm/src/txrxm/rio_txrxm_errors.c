/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/rio_txrxm_errors.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to distxrxmay revision history information.
*
* Description:    txrx model errors handlers source
*               
* Notes:        
*
******************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "rio_txrxm_ds.h"
#include "rio_txrxm_errors.h"

#define RIO_TXRXM_MAX_ERR_STR     255
#define RIO_TXRXM_ERR_MAX_ADD_PAR 3

typedef struct {
    char *string;
    int count_Of_Add_Args;
} TXRXM_ERROR_WARNING_ENTRY_T;

/*
 * array of error strings
 */
#if defined(RIO_TXRXM_MODEL)  

static TXRXM_ERROR_WARNING_ENTRY_T txrxm_Message_Strings[] = {
    {"instance %lu %s", 1},
    {"instance %lu %s", 1},
    {"instance %lu %s"\n, 1}
};

static char txrxm_Error_Prefix_Str[]   = "RIO Physical layer model: Error: ";
static char txrxm_Warning_Prefix_Str[] = "RIO Physical layer model: Warning: ";
static char txrxm_Note_Prefix_Str[]    = "RIO Physical layer model: Info: ";


#else  /*RIO_TXRXM_MODEL not defined*/

static TXRXM_ERROR_WARNING_ENTRY_T txrxm_Message_Strings[] = {
    {"(physical layer, instance %lu). %s", 1},
    {"(physical layer, instance %lu). %s", 1},
    {"(physical layer, instance %lu). %s.\n", 1}
};

static char txrxm_Error_Prefix_Str[]   = "Error: ";
static char txrxm_Warning_Prefix_Str[] = "Warning: ";
static char txrxm_Note_Prefix_Str[]    = "Info: ";

#endif  /*RIO_TXRXM_MODEL*/

static const int txrxm_Message_Strings_Cnt = sizeof(txrxm_Message_Strings) / sizeof(txrxm_Message_Strings[0]);

#define RIO_TXRXM_MAX_ERR_STR 255
#define RIO_TXRXM_ERR_MAX_ADD_PAR 3


/* the size of this array shall be equal to size of ll_Message_Strings array */    
static RIO_PL_MSG_TYPE_T txrxm_Message_To_Type_Map[] = {
    RIO_PL_MSG_ERROR_MSG,     /* do not change: to this entry all 32 bit txrx errors are mapped */
    
    RIO_PL_MSG_WARNING_MSG,   /* do not change: to this entry all 32 bit txrx warnings are mapped */
    
    RIO_PL_MSG_NOTE_MSG      /* do not change: to this entry all 32 bit txrx errors are mapped */
};

static const int txrxm_Message_Map_Count = sizeof(txrxm_Message_To_Type_Map) / sizeof(txrxm_Message_To_Type_Map[0]);

/***************************************************************************
 * Function : RIO_TXRXM_Print_Message
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Print_Message(
    RIO_TXRXM_DS_T* handle, RIO_TXRXM_MESSAGES_T message_Type, ...)
{
    /* 
     * buffer to store error message and additional pointer becasue
     * for the first warning argument will be of this type
     */
    char message_Buffer[RIO_TXRXM_MAX_ERR_STR];
    char temp_Buffer[RIO_TXRXM_MAX_ERR_STR]; 
    char *warning_1 = NULL;

    /* 
     * additional information which shall present in warning message
     * is passing through additional (optional) parameters:
     * add_Args[0]...[2]. 
     * i - loop counter
     */
    unsigned long add_Args[RIO_TXRXM_ERR_MAX_ADD_PAR] = {0, 0, 0};
    int i;

    /* this is necessary to detect additional parameters */
    va_list pointer_To_Next_Arg;

    /*
     * check all using pointers to be not NULL, check
     * that type of error is within corresponding array
     * and when invoke callback with actual parameters.
     * actual error message obtain using error type as an index
     */
    if (handle == NULL || 
        handle->ext_Interfaces.rio_User_Msg == NULL ||
        (int)message_Type < 0 || 
        (int)message_Type > (txrxm_Message_Strings_Cnt - 1) ||
        (int)message_Type > (txrxm_Message_Map_Count - 1) ||
        txrxm_Message_Map_Count != txrxm_Message_Strings_Cnt)
        return RIO_ERROR;

    /* 
     * get additional parameters, which is int type for all warnings,
     * exclude 1 that comes from TxRx (char*)
     */
    va_start(pointer_To_Next_Arg, message_Type);

    for (i = 1; i <= txrxm_Message_Strings[(int)message_Type].count_Of_Add_Args; i++)
    {
        if (message_Type == RIO_TXRXM_ERROR_1   || 
            message_Type == RIO_TXRXM_WARNING_1 ||
            message_Type == RIO_TXRXM_NOTE_1)
        {
            warning_1 = va_arg(pointer_To_Next_Arg, char*);     
        }
        else
        {
            add_Args[i - 1] = va_arg(pointer_To_Next_Arg, unsigned long);     
        }
    }

    va_end(pointer_To_Next_Arg);

    /*
     * obtain actual warning message - the constants have sense of additional arguments 
     * count
     */
    switch (txrxm_Message_Strings[(int)message_Type].count_Of_Add_Args)
    {
        case 0:
            sprintf(message_Buffer, txrxm_Message_Strings[(int)message_Type].string,
                handle->instance_Number);
            break;

        case 1:
            if (message_Type == RIO_TXRXM_ERROR_1  ||
                message_Type == RIO_TXRXM_WARNING_1 ||
                message_Type == RIO_TXRXM_NOTE_1)
            {
                sprintf(message_Buffer, txrxm_Message_Strings[(int)message_Type].string,
                    handle->instance_Number, warning_1);
            }
            break;

        case 2:
            sprintf(message_Buffer, txrxm_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1]);
            break;

        case 3:
            sprintf(message_Buffer, txrxm_Message_Strings[(int)message_Type].string,
                handle->instance_Number, add_Args[0], add_Args[1], add_Args[2]);
            break;

        default:
            return RIO_ERROR;
    }   

    /* detect the type of message */
    switch (txrxm_Message_To_Type_Map[(int)message_Type])
    {
        case RIO_PL_MSG_WARNING_MSG:
            sprintf(temp_Buffer, txrxm_Warning_Prefix_Str);
            break;
        
        case RIO_PL_MSG_ERROR_MSG:
            sprintf(temp_Buffer, txrxm_Error_Prefix_Str);
            break;
        
        case RIO_PL_MSG_NOTE_MSG:
            sprintf(temp_Buffer, txrxm_Note_Prefix_Str);
            break;
        
        default:
            return RIO_ERROR;
    
    } 
    
    /*strcat(temp_Buffer, message_Buffer);*/
    strcpy(&temp_Buffer[strlen(temp_Buffer)], message_Buffer);
    
    /* print result error message*/
    handle->ext_Interfaces.rio_User_Msg(
        handle->ext_Interfaces.rio_Msg, 
        temp_Buffer);

    return RIO_OK;

}
/**************************************************************************/
