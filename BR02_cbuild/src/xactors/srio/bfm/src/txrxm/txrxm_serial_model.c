/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/txrxm_serial_model.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx serial model source
*
* Notes:        
*
******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include "rio_txrxm_serial_model.h"
#include "rio_txrxm_serial_ds.h"
#include "rio_txrxm_serial_tx.h"
#include "rio_txrxm_serial_rx.h"

#ifdef _LICENSING
#include "lmpolicy.h"
#endif /* _LICENSING */




/* these are constants defining licensing for the RapidIO Serial model */
#ifdef _LICENSING

#define RIO_SERIAL_FEATURE_NAME    "mot_eda_rapidio_serial_bfm"
#define RIO_SERIAL_FEATURE_VER     "1.0"
#define RIO_SERIAL_LIC_SEARCH_PATH "mot_rapidio_serial_bfm.lic;."

#endif /* _LICENSING */

/*structure for verifying the handle correct*/
typedef struct RIO_TXRX_HANDLE_STORAGE{
    struct RIO_TXRX_HANDLE_STORAGE * next;
    RIO_HANDLE_T handle;
}RIO_TXRX_HANDLE_STORAGE_T;
/*routines for checking models handle*/
static RIO_TXRX_HANDLE_STORAGE_T serial_Handles = {NULL, NULL};

static RIO_BOOL_T is_Handle_Is_Correct(RIO_HANDLE_T handle);
static int add_Handle(RIO_HANDLE_T handle);
static void remove_Handle_Storage(void);
static void remove_Handle_From_Storage(RIO_HANDLE_T handle);
/*--------- end of handle checking routines --------*/


/* variable for number of created instances counting */
static unsigned long txrxm_Instances_Count = 0;

/*checks that passed parameters are ok*/
static int check_Inst_Param_Set(RIO_TXRXM_SERIAL_INST_PARAM_T *param_Set);
/*Create and bind instances of the data and protocol manager*/
static int txrxm_Create_Bind_Link(RIO_TXRXM_SERIAL_DS_T *handle);
/*reset instance of the txrx model*/
static int txrxm_Model_Start_Reset(RIO_HANDLE_T handle);

/**************************************************************************************/
/*declaration of routines which are participate in the 
  ftray implementation*/

static int txrxm_Print_Version(
    RIO_HANDLE_T handle
    );

static int txrxm_Clock_Edge(
    RIO_HANDLE_T handle /* instance handle */
    );

static int txrxm_Packet_Struct_Request( 
    RIO_HANDLE_T                    handle,
    RIO_TXRXM_PACKET_REQ_STRUCT_T   *rq,
    RIO_UCHAR_T                     dw_Size,           
    RIO_DW_T                        *data             
    );

static int txrxm_Packet_Stream_Request(
    RIO_HANDLE_T            handle,
    RIO_UCHAR_T*            packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int            packet_Size        /*packet array size in byte count*/  
    );

static int txrxm_Symbol_Request(
    RIO_HANDLE_T                handle, 
    RIO_SYMBOL_SERIAL_STRUCT_T  *sym,/*control symbol as a fields struct*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Character_Request(
    RIO_HANDLE_T            handle, 
    RIO_UCHAR_T             character,/* character data*/
    RIO_CHARACTER_TYPE_T    char_Type, 
    RIO_PLACE_IN_STREAM_T   place, /*describe placement in packet*/
    unsigned int            granule_Num/* number of granule in packet after which it placed*/
    );
    
static int txrxm_Character_Column_Request(
    RIO_HANDLE_T                handle, 
    RIO_UCHAR_T                 column[RIO_PL_SERIAL_NUMBER_OF_LANES],/* column data (the array size shall be 4)*/
    RIO_CHARACTER_TYPE_T        char_Type[RIO_PL_SERIAL_NUMBER_OF_LANES], /*the array size is 4 bytes*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Codegroup_Request(
    RIO_HANDLE_T                handle, 
    unsigned int                code_Group,/* code group data- size is 10*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Codegroup_Column_Request(
    RIO_HANDLE_T                handle, 
    unsigned int                code_Column[RIO_PL_SERIAL_NUMBER_OF_LANES],/* column data (the array size shall be 10x4)*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    );

static int txrxm_Single_Character_Request(
    RIO_HANDLE_T                handle, 
    RIO_UCHAR_T                 character,/* granule data*/
    RIO_CHARACTER_TYPE_T        char_Type, 
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num,/* number of granule in packet after which it placed*/
    RIO_PLACE_IN_GRANULE_T      place_In_Granule, /*describe type of placement in granule, selected by place and 
                                                    granule_Num parameters*/
    unsigned int                character_Num/* number of character in granule selected by place and 
                                            granule_Num parameters, which is the object of the request*/
    );

static int txrxm_Single_Bit_Request(
    RIO_HANDLE_T                handle, 
    RIO_SIGNAL_T                bit_Value,/* granule data*/
    RIO_PLACE_IN_STREAM_T       place, /*describe placement in packet*/
    unsigned int                granule_Num,/* number of granule in packet after which it placed*/
    RIO_PLACE_IN_GRANULE_T      place_In_Granule, /*describe type of placement in granule, selected by 
                                                    place and granule_Num parameters*/
    unsigned int                character_Num,/* number of character in granule selected by place and granule_Num parameters, 
                                                    which is the object of the request*/
    unsigned int                bit_Num /* number of bit in the said by character_Num parameter character in granule
                                    selected by place and granule_Num parameters, which is the object of the request*/
    );

static int txrxm_Comp_Seq_Request(
    RIO_HANDLE_T        handle,
    RIO_PLACE_IN_STREAM_T           place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
    );

static int txrxm_Pcs_Pma_Machine_Mgmnt(
    RIO_HANDLE_T        handle,
    RIO_BOOL_T          set_On_Machine, /*RIO_TRUE if seq must be cont. sent*/
    RIO_PLACE_IN_STREAM_T           place, /*describe placement in packet*/
    unsigned int                    granule_Num,/*    number of granule in packet after which it placed*/
    RIO_PCS_PMA_MACHINE_T           machine_Type
    );

static int txrxm_Pop_Bit(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T  lane_Num, /*0-3*/
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                     granule_Num/*    number of granule in packet after which it placed*/
);

static int txrxm_Pop_Character(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T  lane_Num, /*0-3*/
    RIO_PLACE_IN_STREAM_T           place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
);


static int txrxm_Model_Start_Reset(RIO_HANDLE_T handle);

static int txrxm_Model_Initialize(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_SERIAL_PARAM_SET_T   *param_Set
    );

static int txrxm_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
    );

static int txrxm_Set_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T tlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T tlane3       /* receive data on lane 3*/
    );
    
static int rio_TxRxM_Delete_Instance(RIO_HANDLE_T handle);
static void init_Registers (RIO_TXRXM_SERIAL_DS_T * handle);
/*end of ftray routines declaration*/
/**************************************************************************************/

static int check_Init_Param_Set(
    RIO_TXRXM_SERIAL_DS_T *instance, 
    RIO_TXRXM_SERIAL_PARAM_SET_T *param_Set
    );

static int message (
    RIO_CONTEXT_T                context,    /* callback context */
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
    );

static int set_State(
    RIO_HANDLE_T             handle_Param,    /* callback context */
    RIO_CONTEXT_T            caller_Inst,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Parameter
    );

static int get_State(
    RIO_HANDLE_T             handle_Param,    /* callback context */
    RIO_CONTEXT_T            caller_Inst,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Parameter
    );

/***************************************************************************
 * Function : IS_NOT_A_BOOL
 *
 * Description: detect if a variable is RIO_BOOL_T type
 *
 * Returns: bool
 *
 * Notes: none
 *
 **************************************************************************/
#define IS_NOT_A_BOOL(bool_Value) ((bool_Value) != RIO_TRUE && (bool_Value) != RIO_FALSE)

/***************************************************************************
 * Function : Char_Type_Is_Incorrect
 *
 * Description: Detetct that character type is incorrect
 *
 * Returns: bool
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define Char_Type_Is_Incorrect(c) \
    ( \
        ((c) != RIO_SC) && \
        ((c) != RIO_PD) && \
        ((c) != RIO_K) && \
        ((c) != RIO_A) && \
        ((c) != RIO_R) && \
        ((c) != RIO_D) \
    )

/***************************************************************************
 * Function : Place_Is_Incorrect
 *
 * Description: Detetct that place parameter is incorrect
 *
 * Returns: bool
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define Place_Is_Incorrect(place) \
    ( \
        ((place) != FREE) && \
        ((place) != EMBEDDED) && \
        ((place) != CANCELING) \
    )
/***************************************************************************
 * Function : Place_In_Granule_Is_Incorrect
 *
 * Description: Detetct that place in granule parameter is incorrect
 *
 * Returns: bool
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define Place_In_Granule_Is_Incorrect(place) \
    ( \
        ((place) != RIO_PLACE_IN_GRANULE_EMBEDDED) && \
        ((place) != RIO_PLACE_IN_GRANULE_REPLACING) && \
        ((place) != RIO_PLACE_IN_GRANULE_DELETING) \
    )
/***************************************************************************
 * Function : Correct_Free
 *
 * Description: Frees memory and checks pointer for NULL before freing
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define Correct_Free(item) if ((item) != NULL) free(item)

/***************************************************************************
 * Function : Pop_Routine_Body
 *
 * Description: Implementation of routine for all pop procedure
 *
 * Returns: res if error none if ok
 *
 * Notes: none
 *
 **************************************************************************/
#define Pop_Routine_Body(buf)\
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle; \
{\
    /* check pointers */\
    if (instance == NULL || instance->in_Reset == RIO_TRUE) \
        return RIO_ERROR; \
    /*check parameters*/ \
    if (lane_Num >= RIO_PL_SERIAL_NUMBER_OF_LANES) return RIO_PARAM_INVALID; \
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID; \
\
    /*checking the possibility of emb/canc packet*/ \
    /*only for FS complaince - not to embed or cancel sending packets*/ \
    if ((place == EMBEDDED || place == CANCELING)  && \
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == \
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && \
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE) \
    { \
        RIO_TXRXM_SERIAL_OUT_GET_POP_TEMP(instance, buf).place = FREE; \
    } \
    else \
        RIO_TXRXM_SERIAL_OUT_GET_POP_TEMP(instance, buf).place = place; \
\
    RIO_TXRXM_SERIAL_OUT_GET_POP_TEMP(instance, buf).granule_Num = granule_Num; \
    RIO_TXRXM_SERIAL_OUT_GET_POP_TEMP(instance, buf).lane_Num = lane_Num; \
}

 /***************************************************************************
 * Function : accept_Corrupted_Granule_Stub
 *
 * Description: stub for the PCS/PMA callback
 *
 * Returns: RIO_OK
 *
 * Notes: 
 *
 **************************************************************************/
static int accept_Corrupted_Granule_Stub(
    RIO_CONTEXT_T                   handle_context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T           *granule,          /* pointer to granule data structure */
    RIO_PCS_PMA_CHARACTER_T         *character,        /* pointer to character data structure */
    RIO_PCS_PMA_GRANULE_VIOLATION_T violation
)
{
    return RIO_OK;
}



 
 /***************************************************************************
 * Function : RIO_TxRxM_Model_Create_Instance
 *
 * Description: Create new txrxm model instance
 *
 * Returns: error code
 *
 * Notes: instance handle is stored in handle parameter
 *
 **************************************************************************/
int RIO_TxRxM_Serial_Model_Create_Instance(
    RIO_HANDLE_T            *handle, 
    RIO_TXRXM_SERIAL_INST_PARAM_T  *param_Set
    )
{
    int i;
    /* handle to structure will allocated*/
    RIO_TXRXM_SERIAL_DS_T *txrxm_Instance = NULL;

#ifdef _LICENSING

    static RIO_BOOL_T is_Lic_Checkedout = RIO_FALSE;
    LP_HANDLE *lp_Handle;
    
#endif /* _LICENSING */

    /*
     * check that pointers are valid and clear contents handle pointer
     * refers to, after that try allocate memory for txrxm model 
     * data structure
     */
    if (handle == NULL) return RIO_ERROR;

    /*reset the handle value*/
    *handle = NULL;

    if (param_Set == NULL || 
        check_Inst_Param_Set(param_Set) == RIO_ERROR)
        return RIO_ERROR;

#ifdef _LICENSING
    /* try checking the license for the model */
    if( is_Lic_Checkedout == RIO_FALSE )
    {
        if( lp_checkout(LPCODE, LM_RESTRICTIVE | LM_MANUAL_HEARTBEAT, RIO_SERIAL_FEATURE_NAME, 
                        RIO_SERIAL_FEATURE_VER, 1, RIO_SERIAL_LIC_SEARCH_PATH, &lp_Handle) )
        {
            fprintf(stderr, "RapidIO TxRx Model Serial Create Instance: checkout of feature %s failed: %s\n", 
                    RIO_SERIAL_FEATURE_NAME, lp_errstring(lp_Handle));

            /* set handle to NULL */
            *handle = NULL;

            /* return RIO_ERROR;*/
            return RIO_ERROR;
        }
        else
        {
            is_Lic_Checkedout = RIO_TRUE;
            fprintf(stderr, "RapidIO TxRx Model Serial Create Instance: successfull checkout of feature %s\n", 
                    RIO_SERIAL_FEATURE_NAME);
        }
    }
#endif /* _LICENSING */

    /* allocate instance */
    if ((txrxm_Instance = malloc(sizeof(RIO_TXRXM_SERIAL_DS_T))) == NULL)
        return RIO_ERROR;

    /* allocate memory for outbound buffers */
    if ((txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer = 
        malloc(sizeof(RIO_TXRXM_PACKET_OUT_ENTRY_T) * param_Set->packets_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* symbol */
    if ((txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_SYMBOL_OUT_ENTRY_T) * 
            param_Set->symbols_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }
    if ((txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_SYMBOL_OUT_ENTRY_T) * 
            param_Set->symbols_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    

    /* characters */
    if ((txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_OUT_ENTRY_T) * 
            param_Set->character_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* character's column*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_COLUMN_OUT_ENTRY_T) * 
            param_Set->character_Column_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* single character*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_SINGLE_CHAR_ENTRY_T) * 
            param_Set->single_Character_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* codegroup*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_CG_ENTRY_T) * 
            param_Set->code_Group_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* codegroup column*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer = 
        malloc(sizeof(RIO_TXRX_SERIAL_CG_COLUMN_ENTRY_T) * 
            param_Set->code_Group_Column_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* single bit*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer = 
        malloc(sizeof(RIO_TXRX_SERIAL_SINGLE_BIT_ENTRY_T) * 
            param_Set->single_Bit_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }


    /* compensation sequence request*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer = 
        malloc(sizeof(RIO_TXRX_SERIAL_COMP_SEQ_RQ_ENTRY_T) * 
            param_Set->comp_Seq_Request_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* machine managment*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer = 
        malloc(sizeof(RIO_TXRX_SERIAL_MACHINE_MANAGMENT_ENTRY_T) * 
            param_Set->management_Request_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /* pop buffer*/
    if ((txrxm_Instance->outbound_Data.out_Bufs.pop_Byte_Buf.buffer = 
        malloc(sizeof(RIO_TXRX_SERIAL_MACHINE_MANAGMENT_ENTRY_T) * 
            param_Set->pop_Char_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    if ((txrxm_Instance->outbound_Data.out_Bufs.pop_Bit_Buf.buffer = 
        malloc(sizeof(RIO_TXRX_SERIAL_MACHINE_MANAGMENT_ENTRY_T) * 
            param_Set->pop_Bit_Buffer_Size)) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Byte_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /*allocate memory for the high prio queue*/
    txrxm_Instance->outbound_Data.queue_High_Prio_Symbols.queue_Max_Size = 
        param_Set->symbols_Buffer_Size;
    if ((txrxm_Instance->outbound_Data.queue_High_Prio_Symbols.queue_Buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T) * 
        txrxm_Instance->outbound_Data.queue_High_Prio_Symbols.queue_Max_Size )) == NULL)
    {
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Byte_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /*allocate memory for the queue1*/
    txrxm_Instance->outbound_Data.queue1.out_Queue.queue_Max_Size = 
        param_Set->packets_Buffer_Size+
        param_Set->symbols_Buffer_Size+
        param_Set->character_Buffer_Size+
        param_Set->character_Column_Buffer_Size+
        param_Set->single_Character_Buffer_Size+
        param_Set->code_Group_Buffer_Size+
        param_Set->code_Group_Column_Buffer_Size+
        param_Set->single_Bit_Buffer_Size+
        param_Set->comp_Seq_Request_Buffer_Size+
        param_Set->management_Request_Buffer_Size +
        param_Set->pop_Bit_Buffer_Size +
        param_Set->pop_Char_Buffer_Size;

    if ((txrxm_Instance->outbound_Data.queue1.out_Queue.queue_Buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T) * 
        txrxm_Instance->outbound_Data.queue1.out_Queue.queue_Max_Size )) == NULL)
    {
        free(txrxm_Instance->outbound_Data.queue_High_Prio_Symbols.queue_Buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Byte_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /*allocate memory for the queue2*/
    txrxm_Instance->outbound_Data.queue2.out_Queue.queue_Max_Size =
        param_Set->character_Buffer_Size +
        param_Set->character_Column_Buffer_Size +
        param_Set->single_Character_Buffer_Size +
        param_Set->code_Group_Buffer_Size +
        param_Set->code_Group_Column_Buffer_Size +
        param_Set->single_Bit_Buffer_Size +
        param_Set->comp_Seq_Request_Buffer_Size +
        param_Set->pop_Bit_Buffer_Size + 
        param_Set->pop_Char_Buffer_Size;

    if ((txrxm_Instance->outbound_Data.queue2.out_Queue.queue_Buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T) * 
        txrxm_Instance->outbound_Data.queue2.out_Queue.queue_Max_Size )) == NULL)
    {
        free(txrxm_Instance->outbound_Data.queue_High_Prio_Symbols.queue_Buffer);
        free(txrxm_Instance->outbound_Data.queue1.out_Queue.queue_Buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Byte_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /*allocate memory for the queue3*/
    txrxm_Instance->outbound_Data.queue3.out_Queue.queue_Max_Size =
        param_Set->code_Group_Buffer_Size+
        param_Set->code_Group_Column_Buffer_Size+
        param_Set->single_Bit_Buffer_Size;

    if ((txrxm_Instance->outbound_Data.queue3.out_Queue.queue_Buffer = 
        malloc(sizeof(RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T) * 
        txrxm_Instance->outbound_Data.queue3.out_Queue.queue_Max_Size )) == NULL)
    {
        free(txrxm_Instance->outbound_Data.queue_High_Prio_Symbols.queue_Buffer);
        free(txrxm_Instance->outbound_Data.queue2.out_Queue.queue_Buffer);
        free(txrxm_Instance->outbound_Data.queue1.out_Queue.queue_Buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.pop_Byte_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        txrxm_Instance->outbound_Data.queue2.arbiter.list[i] = NULL;
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        txrxm_Instance->outbound_Data.queue3.arbiter.list[i] = NULL;




    /*
     * clear pointers to external callbacks 
     */
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Request_Received = NULL;
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Response_Received = NULL;
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Violent_Packet_Received = NULL;
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Canceled_Packet_Received = NULL;

    txrxm_Instance->ext_Interfaces.rio_TxRxM_Serial_Symbol_Received = NULL;
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Serial_Violent_Symbol_Received = NULL;
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Serial_Code_Group_Received = NULL;
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Serial_Violent_Character_Received = NULL;
    txrxm_Instance->ext_Interfaces.rio_TxRxM_Serial_Character_Received = NULL;
 
    txrxm_Instance->ext_Interfaces.txrxm_Interface_Context = NULL; 

    txrxm_Instance->ext_Interfaces.rio_TxRxM_Set_Pins = NULL;
    txrxm_Instance->ext_Interfaces.lp_Serial_Interface_Context = NULL; 
    
    txrxm_Instance->ext_Interfaces.rio_User_Msg = NULL;
    txrxm_Instance->ext_Interfaces.rio_Msg = NULL;
    

    /*
     * clear physical layer pointers
     */
    /*data manager interfaces*/
    txrxm_Instance->dp_Interfaces.start_Reset = NULL;
    txrxm_Instance->dp_Interfaces.initialize = NULL;
        
    txrxm_Instance->dp_Interfaces.clock_Edge = NULL;
    txrxm_Instance->dp_Interfaces.rio_Link_Delete_Instance = NULL;

    txrxm_Instance->dp_Interfaces.rio_Serial_Get_Pins = NULL; 
 
    txrxm_Instance->dp_Interfaces.put_Character_Column = NULL;
    txrxm_Instance->dp_Interfaces.get_Character_Column = NULL;

    txrxm_Instance->dp_Interfaces.put_Codegroup = NULL;
    txrxm_Instance->dp_Interfaces.get_Codegroup_Column = NULL;

    txrxm_Instance->dp_Interfaces.pop_Char_Buffer = NULL;
    txrxm_Instance->dp_Interfaces.pop_Codegroup_Buffer = NULL;


    /*protocol manager interfaces*/
    txrxm_Instance->pm_Interfaces.start_Reset = NULL;
    txrxm_Instance->pm_Interfaces.initialize = NULL;
    txrxm_Instance->pm_Interfaces.machine_Management = NULL;
    txrxm_Instance->pm_Interfaces.delete_Instance = NULL;

    txrxm_Instance->pm_Interfaces.put_Granule = NULL;
    txrxm_Instance->pm_Interfaces.get_Granule = NULL;

    txrxm_Instance->pm_Interfaces.put_Character_Column = NULL;
    txrxm_Instance->pm_Interfaces.get_Character_Column = NULL;

    txrxm_Instance->pm_Interfaces.put_Codegroup = NULL;
    txrxm_Instance->pm_Interfaces.get_Codegroup_Column = NULL;



    /* try create and bind a link */
    if (txrxm_Create_Bind_Link(txrxm_Instance) == RIO_ERROR)
    {
        free(txrxm_Instance->outbound_Data.queue1.out_Queue.queue_Buffer);
        free(txrxm_Instance->outbound_Data.queue_High_Prio_Symbols.queue_Buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.cg_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.char_Buf.buffer);
        
        free(txrxm_Instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
        free(txrxm_Instance->outbound_Data.out_Bufs.packet_Buf.buffer);
        free(txrxm_Instance);
        return RIO_ERROR;
    }

    /*allocation was successful */
    txrxm_Instance->instance_Number = ++txrxm_Instances_Count;
    txrxm_Instance->in_Reset = RIO_TRUE;
    txrxm_Instance->inst_Param_Set = *param_Set;
    txrxm_Instance->restart_Function = txrxm_Model_Initialize;
    txrxm_Instance->reset_Function = txrxm_Model_Start_Reset;
    txrxm_Instance->is_Bound = RIO_FALSE;

    /* set handle for the environment */
    *handle = (RIO_HANDLE_T)txrxm_Instance;

    /*store handle to the handle storage*/
    if (add_Handle(*handle) != RIO_OK) return RIO_ERROR;


    return RIO_OK;
}
/***************************************************************************
 * Function : txrxm_Create_Bind_Link
 *
 * Description: instantiate 32-Bit Transmitter/Receiver into txrx model
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Create_Bind_Link(RIO_TXRXM_SERIAL_DS_T *handle)
{
    /*create instance of the protocol manager 
    then create instace of data manager after all instancess are binded*/
    RIO_PCS_PMA_PM_CALLBACK_TRAY_T pm_Callbacks;
    RIO_PCS_PMA_DP_CALLBACK_TRAY_T dp_Callbacks;

    int res;    /*varialble for the result storing*/

    /*initialize routines for access to the registers*/
    handle->get_State = get_State;
    handle->set_State = set_State;
    /*-------*/
        
    res = RIO_PCS_PMA_PM_Create_Instance( &(handle->pm),  NULL /*pointer to MUM instance*/);
    CHECK_RESULT;

    res = RIO_PCS_PMA_DP_Create_Instance( &(handle->dp), NULL /*pointer to MUM instance*/ );
    CHECK_RESULT;

    /*accepting of function trays*/

    res = RIO_PCS_PMA_PM_Get_Function_Tray( &(handle->pm_Interfaces) );
    CHECK_RESULT;

    res = RIO_PCS_PMA_DP_Get_Function_Tray( &(handle->dp_Interfaces) );
    CHECK_RESULT;

    /*bind instances of the Data and Protocol managers*/        
    pm_Callbacks.rio_User_Msg = message;
    pm_Callbacks.user_Msg_Context = handle;

    pm_Callbacks.rio_Set_State_Flag = set_State;
    pm_Callbacks.rio_Get_State_Flag = get_State;
    pm_Callbacks.rio_Regs_Context = handle;

    pm_Callbacks.get_Granule = RIO_STxRxM_Get_Granule_From_Queue1;
    pm_Callbacks.put_Granule = RIO_STxRxM_Granule_Acceptor;
    pm_Callbacks.granule_Level_Context = handle;

    pm_Callbacks.put_Character_Column = RIO_STxRxM_Pass_Character_Column;
    pm_Callbacks.get_Character_Column = TxRxM_Get_Char_Column;
    pm_Callbacks.pop_Char_Buffer = RIO_TxRxM_Pop_Char_Buffer_Connector;
    pm_Callbacks.character_Level_Context = handle;

    pm_Callbacks.put_Codegroup = RIO_STxRxM_Pass_CG;
    pm_Callbacks.get_Codegroup_Column = TxRxM_Get_CG_Column;
    pm_Callbacks.pop_Codegroup_Buffer = RIO_TxRxM_Pop_CG_Buffer_Connector;
    pm_Callbacks.codegroup_Level_Context = handle;

    res = RIO_PCS_PMA_PM_Bind_Instance(handle->pm, &pm_Callbacks);
    CHECK_RESULT;

    /*data processor binding*/
    dp_Callbacks.rio_Serial_Set_Pins = txrxm_Set_Pins;
    dp_Callbacks.lp_Serial_Interface_Context = handle;    

    dp_Callbacks.rio_User_Msg = message;
    dp_Callbacks.user_Msg_Context = handle;

    dp_Callbacks.get_Granule = RIO_STxRxM_Get_Granule_Connector;
    dp_Callbacks.put_Granule = RIO_STxRxM_Put_Granule;
    dp_Callbacks.granule_Level_Context = handle;

    dp_Callbacks.put_Character_Column = RIO_STxRxM_Put_Character_Column;
    dp_Callbacks.get_Character_Column = TxRxM_Transfer_Character_Column;
    dp_Callbacks.character_Level_Context = handle;

    dp_Callbacks.put_Codegroup = RIO_STxRxM_Put_CG;
    dp_Callbacks.get_Codegroup_Column = TxRxM_Transfer_Codegroup_Column;
    dp_Callbacks.codegroup_Level_Context = handle;

    dp_Callbacks.rio_Set_State_Flag = set_State;
    dp_Callbacks.rio_Get_State_Flag = get_State;
    dp_Callbacks.rio_Regs_Context = handle;

    dp_Callbacks.put_Corrupted_Granule = accept_Corrupted_Granule_Stub;
    

    res = RIO_PCS_PMA_DP_Bind_Instance(handle->dp, &dp_Callbacks);
    CHECK_RESULT;

    return RIO_OK;
}




/***************************************************************************
 * Function : RIO_TxRxM_Serial_Model_Get_Function_Tray
 *
 * Description: export physical layer instance function tray
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Serial_Model_Get_Function_Tray(
    RIO_TXRXM_SERIAL_MODEL_FTRAY_T *ftray             
    )
{
    /*
     * check passed poinetrs and if they are valid compete functional
     * tray with actual entry points
     */
    if (ftray == NULL)
        return RIO_ERROR;

    ftray->rio_TxRxM_Clock_Edge = txrxm_Clock_Edge;
    ftray->rio_TxRxM_Packet_Struct_Request = txrxm_Packet_Struct_Request;
    ftray->rio_TxRxM_Packet_Stream_Request = txrxm_Packet_Stream_Request;

    ftray->rio_TxRxM_Serial_Symbol_Request = txrxm_Symbol_Request;
    ftray->rio_TxRxM_Serial_Character_Request = txrxm_Character_Request;
    ftray->rio_TxRxM_Serial_Character_Column_Request = txrxm_Character_Column_Request;
    ftray->rio_TxRxM_Serial_Code_Group_Request = txrxm_Codegroup_Request;
    ftray->rio_TxRxM_Serial_Code_Group_Column_Request = txrxm_Codegroup_Column_Request;
    ftray->rio_TxRxM_Serial_Single_Character_Request = txrxm_Single_Character_Request;
    ftray->rio_TxRxM_Serial_Single_Bit_Request = txrxm_Single_Bit_Request;
    ftray->rio_TxRxM_Serial_Comp_Seq_Request = txrxm_Comp_Seq_Request;

    ftray->rio_TxRxM_Serial_Pcs_Pma_Machine_Management = txrxm_Pcs_Pma_Machine_Mgmnt;

    ftray->rio_TxRxM_Serial_Pop_Bit = txrxm_Pop_Bit;
    ftray->rio_TxRxM_Serial_Pop_Character = txrxm_Pop_Character;


    ftray->rio_TxRxM_Model_Start_Reset = txrxm_Model_Start_Reset;
    ftray->rio_TxRxM_Model_Initialize = txrxm_Model_Initialize;
    ftray->rio_TxRxM_Get_Pins = txrxm_Get_Pins;
    
    ftray->rio_TxRxM_Print_Version   = txrxm_Print_Version;
    
    ftray->rio_TxRxM_Delete_Instance = rio_TxRxM_Delete_Instance;
    
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_TxRxM_Serial_Model_Bind_Instance
 *
 * Description: bind model instance 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Serial_Model_Bind_Instance(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_SERIAL_MODEL_CALLBACK_TRAY_T   *ctray   
    )
{

    /* instance handle */
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;

    /*
     * check pointers and callbacks if OK load callbacks
     */
    if (instance == NULL || ctray == NULL ||
        ctray->rio_TxRxM_Set_Pins == NULL)
        return RIO_ERROR;
    if (is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        return RIO_ERROR;
    }

    /* store callbacks tray into the layer's structure */
    instance->ext_Interfaces = *ctray;
    instance->is_Bound = RIO_TRUE;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Print_Version
 *
 * Description: prints model version
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Print_Version(RIO_HANDLE_T handle)
{

    /* instanse handle and the result of callback invokation */
    RIO_TXRXM_SERIAL_DS_T *instanse = (RIO_TXRXM_SERIAL_DS_T*)handle;
    char                  buffer[255];

    /*
     * check handle and callback then return and check the result
     */
    if (instanse == NULL ||
        instanse->ext_Interfaces.rio_User_Msg == NULL ||
        instanse->is_Bound == RIO_FALSE
    )
        return RIO_ERROR;

    /* print the version info into the buffer string */
    sprintf( buffer, "RIO TxRxM Model %s\n", RIO_MODEL_VERSION );

    instanse->ext_Interfaces.rio_User_Msg(
        instanse->ext_Interfaces.rio_Msg,
        buffer
    );

    return RIO_OK;
}

/***************************************************************************
 * Function : pl_Clock_Edge
 *
 * Description: physical layer clock
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Clock_Edge(RIO_HANDLE_T handle)
{

    /* instance handle and the result of callback invokation */
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;

    /*
     * check handle and callback then return and check the result
     */
    if (instance == NULL ||
        instance->dp_Interfaces.clock_Edge == NULL ||
        instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    return instance->dp_Interfaces.clock_Edge(instance->dp);
}

/***************************************************************************
 * Function : txrxm_Packet_Struct_Request
 *
 * Description: notify about new struct request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Packet_Struct_Request(
    RIO_HANDLE_T                    handle,
    RIO_TXRXM_PACKET_REQ_STRUCT_T   *rq,
    RIO_UCHAR_T                     dw_Size,           
    RIO_DW_T                        *data             
    )
{

    /*instance handler, temp for function result storing*/
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
        
    /* check pointers */
    if (instance == NULL || rq == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    switch (rq->ftype)
    {
    case RIO_NONINTERV_REQUEST:
    case RIO_WRITE:
    case RIO_STREAM_WRITE:
    case RIO_MAINTENANCE:
    case RIO_DOORBELL:
    case RIO_MESSAGE:
    case RIO_RESPONCE:
        break;
    default:
        return RIO_ERROR;
    }


    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).as_Struct = RIO_TRUE;
     /*copy packet struct*/
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.ack_ID = rq->ack_ID;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.prio = rq->prio;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.rsrv_Phy_1 = rq->rsrv_Phy_1;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.rsrv_Phy_2 = rq->rsrv_Phy_2;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.tt = rq->tt;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.ftype = rq->ftype;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.transport_Info = rq->transport_Info;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.ttype = rq->ttype;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.rdwr_Size = rq->rdwr_Size;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.srtr_TID = rq->srtr_TID;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.hop_Count = rq->hop_Count;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.config_Offset = rq->config_Offset;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.wdptr = rq->wdptr; 
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.status = rq->status;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.doorbell_Info = rq->doorbell_Info;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.msglen = rq->msglen;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.letter = rq->letter;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.mbox = rq->mbox;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.msgseg = rq->msgseg;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.ssize = rq->ssize;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.address = rq->address;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.extended_Address = rq->extended_Address;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.xamsbs = rq->xamsbs;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.ext_Address_Valid = rq->ext_Address_Valid; 
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.ext_Address_16 = rq->ext_Address_16;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.rsrv_Ftype6 = rq->rsrv_Ftype6;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.rsrv_Ftype8 = rq->rsrv_Ftype8;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.rsrv_Ftype10 = rq->rsrv_Ftype10;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.crc = rq->crc;
    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.entry.is_Crc_Valid = rq->is_Crc_Valid;


    /*set payload size*/
    if (dw_Size > RIO_TXRXM_MAX_PAYLOAD_SIZE) 
        RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.payload_Size =
                RIO_TXRXM_MAX_PAYLOAD_SIZE;
    else
        RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.payload_Size = dw_Size;

    /*copy payload*/
    if (data)
    {
        unsigned int i; /* loop counter */
        for (i = 0; i < RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.payload_Size; i++)
            RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.payload[i] = data[i];
    }
    else 
        RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Struct.payload_Size = 0;


    return RIO_STxRxM_Load_Packet_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Packet_Stream_Request
 *
 * Description: notify about new stream request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Packet_Stream_Request(
    RIO_HANDLE_T            handle,
    RIO_UCHAR_T*            packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int            packet_Size        /*packet array size in byte count*/  
    )
{
    /*instance handler, temp for function result storing*/
    unsigned int i; /* loop counter */
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
        
    /* check pointers */
    if (instance == NULL || packet_Buffer == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).as_Struct = RIO_FALSE;

    /*set packet size*/
    if (packet_Size > RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN) 
        RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Stream.packet_Len =
                RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN;
    else
        RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Stream.packet_Len = packet_Size;

    /*copy stream*/
    for (i = 0; i < RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Stream.packet_Len; i++)
            RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(instance).packet_Stream.packet_Stream[i] = packet_Buffer[i];


    return RIO_STxRxM_Load_Packet_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Symbol_Request
 *
 * Description: notify about new symbol request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Symbol_Request(
    RIO_HANDLE_T                handle, 
    RIO_SYMBOL_SERIAL_STRUCT_T  *sym,/*control symbol as a fields struct*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    /*instance handler, temp for function result storing*/
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
        
    /* check pointers */
    if (instance == NULL || sym == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*set symbol fields*/
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).granule_Num = granule_Num;
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.stype0 = sym->stype0;
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.parameter0 = sym->parameter0;
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.parameter1 = sym->parameter1;
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.stype1 = sym->stype1;
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.cmd = sym->cmd;
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.crc = sym->crc;
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.is_Crc_Valid = sym->is_Crc_Valid;
    
    RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).symbol.first_Char = 0;
        

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE &&
        granule_Num != (unsigned int)-1)
    {
        RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).place = place;

    if (RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).granule_Num == (unsigned int)-1)
        instance->outbound_Data.out_Bufs.hp_Symbol_Buf.temp = 
            RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance);


    
    return RIO_STxRxM_Load_Symbol_Entry(instance);
}



/***************************************************************************
 * Function : txrxm_Character_Request
 *
 * Description: notify about new character request request
 *
 * Returns: error code
 *
 * Notes: If instance in 4x mode then character will be multiplied for issuing
 *        to the all lanes.
 *
 **************************************************************************/
static int txrxm_Character_Request(
    RIO_HANDLE_T            handle, 
    RIO_UCHAR_T             character,/* character data*/
    RIO_CHARACTER_TYPE_T    char_Type, 
    RIO_PLACE_IN_STREAM_T   place, /*describe placement in packet*/
    unsigned int            granule_Num/* number of granule in packet after which it placed*/
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
    int i; 
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*check that parameters are correct*/
    if (Char_Type_Is_Incorrect(char_Type)) return RIO_PARAM_INVALID;
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).place = place;

    /*set other symbol fields*/


    RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).granule_Num = granule_Num;

    /*fills data of the character*/
    RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.character_Data[0] = character;
    RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.character_Type[0] = char_Type;
    /*multiplie fields*/
    for (i = 1; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
    {
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.character_Data[i] = 
            RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.character_Data[0];
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.character_Type[i] = 
            RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.character_Type[0];
    }
    /*set mask with accordance to the lane mode*/
    if (instance->initialization_Param_Set.is_Force_1x_Mode == RIO_FALSE)
            RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.lane_Mask = 
                RIO_TXRXM_SERIAL_ALL_LANES_USED_MASK;
    else  RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.lane_Mask = 
                RIO_TXRXM_SERIAL_1_LANE_USED_MASK;
                
    return RIO_STxRxM_Load_Char_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Character_Column_Request
 *
 * Description: notify about new character column request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Character_Column_Request(
    RIO_HANDLE_T                handle, 
    RIO_UCHAR_T                    column[RIO_PL_SERIAL_NUMBER_OF_LANES],/* column data (the array size shall be 4)*/
    RIO_CHARACTER_TYPE_T        char_Type[RIO_PL_SERIAL_NUMBER_OF_LANES], /*the array size is 4 bytes*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
    int i; 
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*check that parameters are correct*/
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        if (Char_Type_Is_Incorrect(char_Type[i])) return RIO_PARAM_INVALID;
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;


    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_COLUMN_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_COLUMN_TEMP(instance).place = place;

    /*set other symbol fields*/


    RIO_TXRXM_SERIAL_OUT_GET_CHAR_COLUMN_TEMP(instance).granule_Num = granule_Num;

    /*fills data of the column*/
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
    {
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_COLUMN_TEMP(instance).character.character_Data[i] = 
            column[i];
        RIO_TXRXM_SERIAL_OUT_GET_CHAR_COLUMN_TEMP(instance).character.character_Type[i] = 
            char_Type[i];
    }
    RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(instance).character.lane_Mask = 
        RIO_TXRXM_SERIAL_ALL_LANES_USED_MASK;
                
    return RIO_STxRxM_Load_Char_Column_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Single_Character_Request
 *
 * Description: notify about new single character request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Single_Character_Request(
    RIO_HANDLE_T                handle, 
    RIO_UCHAR_T                 character,/* granule data*/
    RIO_CHARACTER_TYPE_T        char_Type, 
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num,/* number of granule in packet after which it placed*/
    RIO_PLACE_IN_GRANULE_T        place_In_Granule, /*describe type of placement in granule, selected by 
                                                    place and granule_Num parameters*/
    unsigned int                character_Num/* number of character in granule selected by place and granule_Num parameters, 
                                                which is the object of the request*/
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*check that parameters are correct*/
    if (Char_Type_Is_Incorrect(char_Type)) return RIO_PARAM_INVALID;
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;
    if (Place_In_Granule_Is_Incorrect(place_In_Granule)) return RIO_PARAM_INVALID;
    if (character_Num >= RIO_PL_SERIAL_CNT_BYTE_IN_GRAN) return RIO_PARAM_INVALID;

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(instance).place = place;

    /*set other symbol fields*/
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(instance).character = character;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(instance).char_Type = char_Type;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(instance).granule_Num = granule_Num;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(instance).place_In_Granule = place_In_Granule;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(instance).char_Num = character_Num;

    return RIO_STxRxM_Load_Single_Char_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Codegroup_Request
 *
 * Description: notify about new codegroup request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Codegroup_Request(
    RIO_HANDLE_T                handle, 
    unsigned int                code_Group,/* code group data- size is 10*/
    RIO_PLACE_IN_STREAM_T         place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_CG_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_CG_TEMP(instance).place = place;

    /*set other cg fields*/


    RIO_TXRXM_SERIAL_OUT_GET_CG_TEMP(instance).granule_Num = granule_Num;

    /*fills data of the codegroup*/
    RIO_TXRXM_SERIAL_OUT_GET_CG_TEMP(instance).cg = code_Group;

    return RIO_STxRxM_Load_Cg_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Codegroup_Column_Request
 *
 * Description: notify about new coderoup column request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Codegroup_Column_Request(
    RIO_HANDLE_T                handle, 
    unsigned int                code_Column[RIO_PL_SERIAL_NUMBER_OF_LANES],/* column data (the array size shall be 10x4)*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num/* number of granule in packet after which it placed*/
    )
{
    int i;
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;
    /*check for parameters*/
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_CG_COLUMN_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_CG_COLUMN_TEMP(instance).place = place;

    /*pass other fields*/
    RIO_TXRXM_SERIAL_OUT_GET_CG_COLUMN_TEMP(instance).granule_Num = granule_Num;

    for(i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        RIO_TXRXM_SERIAL_OUT_GET_CG_COLUMN_TEMP(instance).code_Column[i] = code_Column[i];

    return RIO_STxRxM_Load_Cg_Column_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Single_Bit_Request
 *
 * Description: notify about new single bit request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Single_Bit_Request(
    RIO_HANDLE_T                handle, 
    RIO_SIGNAL_T                bit_Value,/* granule data*/
    RIO_PLACE_IN_STREAM_T        place, /*describe placement in packet*/
    unsigned int                granule_Num,/* number of granule in packet after which it placed*/
    RIO_PLACE_IN_GRANULE_T        place_In_Granule, /*describe type of placement in granule, selected 
                                                    by place and granule_Num parameters*/
    unsigned int                character_Num,/* number of character in granule selected by place and granule_Num parameters, 
                                                    which is the object of the request*/
    unsigned int                bit_Num /* number of bit in the said by character_Num parameter character in granule
                                    selected by place and granule_Num parameters, which is the object of the request*/
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*check that parameters is correct*/
    if (character_Num >= RIO_PL_SERIAL_CNT_BYTE_IN_GRAN) return RIO_PARAM_INVALID;
    if (bit_Num >= RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP) return RIO_PARAM_INVALID;
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;
    if (Place_In_Granule_Is_Incorrect(place_In_Granule)) return RIO_PARAM_INVALID;
    if ((bit_Value != RIO_SIGNAL_0) && (bit_Value != RIO_SIGNAL_1)) return RIO_PARAM_INVALID;


    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(instance).place = place;
    /*initialization of bit parameters*/
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(instance).bit_Value = bit_Value;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(instance).granule_Num = granule_Num;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(instance).place_In_Granule = place_In_Granule;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(instance).character_Num = character_Num;
    RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(instance).bit_Num = bit_Num;

    return RIO_STxRxM_Load_Single_Bit_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Comp_Seq_Request
 *
 * Description: notify about compensation sequence request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Comp_Seq_Request(
    RIO_HANDLE_T        handle,
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;

    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_COMP_SEQ_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_COMP_SEQ_TEMP(instance).place = place;

    RIO_TXRXM_SERIAL_OUT_GET_COMP_SEQ_TEMP(instance).granule_Num = granule_Num;

    return RIO_STxRxM_Load_Comp_Seq_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Pcs_Pma_Machine_Mgmnt
 *
 * Description: notify about new machine managment request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Pcs_Pma_Machine_Mgmnt(
    RIO_HANDLE_T        handle,
    RIO_BOOL_T          set_On_Machine, /*RIO_TRUE if seq must be cont. sent*/
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num,/*    number of granule in packet after which it placed*/
    RIO_PCS_PMA_MACHINE_T            machine_Type
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
       
    /* check pointers */
    if (instance == NULL || instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;

    /*check parameters*/
    if (Place_Is_Incorrect(place)) return RIO_PARAM_INVALID;
    if (IS_NOT_A_BOOL(set_On_Machine)) return RIO_PARAM_INVALID;
    if ((machine_Type != RIO_PCS_PMA_MACHINE_INIT) &&
        ( machine_Type !=  RIO_PCS_PMA_MACHINE_COMP_SEQ_GEN) &&
        ( machine_Type !=  RIO_PCS_PMA_MACHINE_STATUS_SYM_GEN) )
            return RIO_PARAM_INVALID;


    /*checking the possibility of emb/canc packet*/
    /*only for FS complaince - not to embed or cancel sending packets*/
    if ((place == EMBEDDED || place == CANCELING)  &&
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Head == 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Top && 
        instance->outbound_Data.out_Bufs.packet_Buf.buffer_Full == RIO_FALSE)
    {
        RIO_TXRXM_SERIAL_OUT_GET_MACHINE_MGMNT_TEMP(instance).place = FREE;
    }
    else
        RIO_TXRXM_SERIAL_OUT_GET_MACHINE_MGMNT_TEMP(instance).place = place;

    RIO_TXRXM_SERIAL_OUT_GET_MACHINE_MGMNT_TEMP(instance).granule_Num = granule_Num;
    RIO_TXRXM_SERIAL_OUT_GET_MACHINE_MGMNT_TEMP(instance).machine_Type = machine_Type;
    RIO_TXRXM_SERIAL_OUT_GET_MACHINE_MGMNT_TEMP(instance).set_On_Machine = set_On_Machine;

    return RIO_STxRxM_Load_Machine_Managment_Entry(instance);
}


/***************************************************************************
 * Function : txrxm_Pop_Bit
 *
 * Description: notify about new bit removing request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Pop_Bit(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T  lane_Num, /*0-3*/
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
)
{
    Pop_Routine_Body(pop_Bit_Buf);
    return RIO_STxRxM_Load_Pop_Bit_Entry(instance);
}

/***************************************************************************
 * Function : txrxm_Pop_Character
 *
 * Description: notify about new character removing request
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Pop_Character(
    RIO_HANDLE_T handle,
    RIO_UCHAR_T  lane_Num, /*0-3*/
    RIO_PLACE_IN_STREAM_T            place, /*describe placement in packet*/
    unsigned int                    granule_Num/*    number of granule in packet after which it placed*/
)
{
    Pop_Routine_Body(pop_Byte_Buf);
    return RIO_STxRxM_Load_Pop_Char_Entry(instance);
}


/***************************************************************************
 * Function : check_Inst_Param_Set
 *
 * Description: check is txrx model instantiation parameters set is valid
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Inst_Param_Set(RIO_TXRXM_SERIAL_INST_PARAM_T *param_Set)
{
    /*check params handle*/
    if (param_Set == NULL)
        return RIO_ERROR;

    /*check oubound buffers size*/
    if (param_Set->packets_Buffer_Size < 1 ||
        param_Set->symbols_Buffer_Size < 1 ||
        param_Set->character_Buffer_Size < 1 ||
        param_Set->character_Column_Buffer_Size < 1 ||
        param_Set->code_Group_Buffer_Size < 1 ||
        param_Set->code_Group_Column_Buffer_Size < 1 ||
        param_Set->single_Bit_Buffer_Size < 1 ||
        param_Set->single_Character_Buffer_Size < 1 ||
        param_Set->comp_Seq_Request_Buffer_Size < 1 ||
        param_Set->management_Request_Buffer_Size < 1
        )
        return RIO_ERROR;

    return RIO_OK;
}



/***************************************************************************
 * Function : txrxm_Set_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Set_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T tlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T tlane3       /* receive data on lane 3*/
    )
{
    RIO_TXRXM_SERIAL_DS_T * instance = (RIO_TXRXM_SERIAL_DS_T *)handle;

    if (handle == NULL) return RIO_ERROR;

    if (instance->ext_Interfaces.rio_TxRxM_Set_Pins == NULL) return RIO_OK;

    return (instance->ext_Interfaces.rio_TxRxM_Set_Pins)(
        instance->ext_Interfaces.lp_Serial_Interface_Context,
        tlane0, tlane1, tlane2, tlane3);
}

/***************************************************************************
 * Function : pl_Get_Pins
 *
 * Description: stub to invoke corresponding callback
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Get_Pins(
    RIO_HANDLE_T handle, /* instance's handle */
    RIO_SIGNAL_T rlane0,       /* receive data on lane 0*/
    RIO_SIGNAL_T rlane1,       /* receive data on lane 1*/
    RIO_SIGNAL_T rlane2,       /* receive data on lane 2*/
    RIO_SIGNAL_T rlane3       /* receive data on lane 3*/
    )
{
    RIO_TXRXM_SERIAL_DS_T * instance = (RIO_TXRXM_SERIAL_DS_T *)handle;

    /*check that pointer is correct*/
    if (is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        return RIO_ERROR;
    }

    if (instance == NULL ||
        instance->dp_Interfaces.rio_Serial_Get_Pins == NULL ||
        instance->in_Reset == RIO_TRUE)
        return RIO_ERROR;


    return instance->dp_Interfaces.rio_Serial_Get_Pins(instance->dp, 
        rlane0, rlane1, rlane2, rlane3);
}


/***************************************************************************
 * Function : txrxm_Model_Initialize
 *
 * Description: turn the instance to work mode and initialize
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Model_Initialize(
    RIO_HANDLE_T         handle,
    RIO_TXRXM_SERIAL_PARAM_SET_T   *param_Set
    )
{
    /* instance handle and link's parameters set */
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
    RIO_PCS_PMA_PARAM_SET_T dp_Param_Set;
    RIO_PCS_PMA_PM_PARAM_SET_T pm_Param_Set;
    int res;    /*result of routines execution*/

    /* check pointers */
    if (instance == NULL || param_Set == NULL)
        return RIO_ERROR;
    /*check that pointer is correct*/
    if (is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        return RIO_ERROR;
    }

    /*check that instance is bound*/
    if (instance->is_Bound != RIO_TRUE)
        return RIO_ERROR;

    /* check reset state */
    if (instance->in_Reset == RIO_FALSE)
    {
        return RIO_ERROR;
    }

    /* check parameters set */
    res = check_Init_Param_Set(instance, param_Set);
    CHECK_RESULT;

    /*reset initial values*/
    instance->outbound_Data.idle_Counter = RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1;

    /*initialize state machines structure*/
    instance->mach_State.align_Enabled = RIO_TRUE;
    instance->mach_State.init_Enabled = RIO_TRUE;
    instance->mach_State.sync_Enabled = RIO_TRUE;
    /* save parameters and initialize the instance */
    instance->initialization_Param_Set = *param_Set;


    /*initialize registers*/
    init_Registers(instance);

    /* prepare data processor's parameters set */
    dp_Param_Set.is_Init_Proc_On            = param_Set->is_Init_Proc_On;
    dp_Param_Set.silence_Period                = param_Set->silence_Period;
    dp_Param_Set.is_Comp_Seq_Gen_On            = param_Set->is_Comp_Seq_Gen_On;
    dp_Param_Set.comp_Seq_Rate                = param_Set->comp_Seq_Rate;
    dp_Param_Set.is_Status_Sym_Gen_On        = param_Set->is_Status_Sym_Gen_On;
    dp_Param_Set.status_Sym_Rate            = param_Set->status_Sym_Rate;
    dp_Param_Set.comp_Seq_Size                = param_Set->comp_Seq_Size;
    dp_Param_Set.comp_Seq_Rate_Check          = param_Set->comp_Seq_Rate_Check;
    dp_Param_Set.icounter_Max               = param_Set->icounter_Max;
    dp_Param_Set.status_Symbols_To_Send = RIO_INIT_STATUS_SYMBOLS_TO_SEND;
    dp_Param_Set.status_Symbols_To_Receive = RIO_INIT_STATUS_SYMBOLS_TO_RECEIVE;

    /*prepare protocol manager's parameter set*/
    pm_Param_Set.is_Init_Proc_On            = param_Set->is_Init_Proc_On;
    pm_Param_Set.silence_Period                = param_Set->silence_Period;
    pm_Param_Set.is_Comp_Seq_Gen_On            = param_Set->is_Comp_Seq_Gen_On;
    pm_Param_Set.comp_Seq_Rate                = param_Set->comp_Seq_Rate;
    pm_Param_Set.is_Status_Sym_Gen_On        = param_Set->is_Status_Sym_Gen_On;
    pm_Param_Set.status_Sym_Rate            = param_Set->status_Sym_Rate;
    pm_Param_Set.comp_Seq_Size                = param_Set->comp_Seq_Size;
    pm_Param_Set.comp_Seq_Rate_Check          = param_Set->comp_Seq_Rate_Check;
    pm_Param_Set.discovery_Period            = param_Set->discovery_Period;
    pm_Param_Set.icounter_Max                = param_Set->icounter_Max;
    pm_Param_Set.status_Symbols_To_Send = RIO_INIT_STATUS_SYMBOLS_TO_SEND;
    pm_Param_Set.status_Symbols_To_Receive = RIO_INIT_STATUS_SYMBOLS_TO_RECEIVE;
    /*intitialization of the data processor*/
    res = (instance->dp_Interfaces.initialize)(instance->dp, &dp_Param_Set);
    CHECK_RESULT;
    /*initialization of the protocol manager*/
    res = (instance->pm_Interfaces.initialize)(instance->pm, &pm_Param_Set);
    CHECK_RESULT;

    /*Initialize rx and tx part*/
    res = RIO_STxRxM_Tx_Data_Init(instance);
    CHECK_RESULT;
    RIO_STxRxM_Rx_Data_Init(instance);
    /*set reset flag to the proper value*/
    instance->in_Reset = RIO_FALSE;

   

    return RIO_OK;
}


/***************************************************************************
 * Function : check_Init_Param_Set
 *
 * Description: check is initialization txrxm 
 *              parameters set is valid
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int check_Init_Param_Set(
    RIO_TXRXM_SERIAL_DS_T *instance, 
    RIO_TXRXM_SERIAL_PARAM_SET_T *param_Set
    )
{
    if (param_Set == NULL || instance == NULL)
        return RIO_ERROR;
        
    if (IS_NOT_A_BOOL(param_Set->lp_Serial_Is_1x) ||
        IS_NOT_A_BOOL(param_Set->is_Force_1x_Mode) ||
        IS_NOT_A_BOOL(param_Set->is_Force_1x_Mode_Lane_0) ||
        IS_NOT_A_BOOL(param_Set->is_Ext_Address_Valid) ||
        IS_NOT_A_BOOL(param_Set->is_Ext_Address_16) ||
        IS_NOT_A_BOOL(param_Set->is_Init_Proc_On) ||
        IS_NOT_A_BOOL(param_Set->is_Comp_Seq_Gen_On) ||
        IS_NOT_A_BOOL(param_Set->is_Status_Sym_Gen_On) )
        return RIO_PARAM_INVALID;

    if (param_Set->status_Sym_Rate <= 1 ||
        param_Set->comp_Seq_Rate <= 1)
        return RIO_PARAM_INVALID;

    return RIO_OK;
}

/***************************************************************************
 * Function : txrxm_Model_Start_Reset
 *
 * Description: turn the instance to reset mode
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int txrxm_Model_Start_Reset(
    RIO_HANDLE_T handle
    )
{
    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
    int res; /*variable for storing the result of routines invocation*/

    /* check pointers */
    if (instance == NULL ||
        instance->dp_Interfaces.start_Reset == NULL ||
        instance->pm_Interfaces.start_Reset == NULL)
        return RIO_ERROR;

    /* check work state */
    if (instance->in_Reset == RIO_TRUE)
        return RIO_OK;
    /*reset the tx and rx parts*/
    RIO_STxRxM_Tx_Data_Reset(instance);


    /* send reset to the link */
    res = (instance->dp_Interfaces.start_Reset)(instance->dp);
    CHECK_RESULT;
    res = (instance->pm_Interfaces.start_Reset)(instance->pm);
    CHECK_RESULT;

    /* modify state to be _in_reset_ */
    instance->in_Reset = RIO_TRUE;


    return RIO_OK;
}


/***************************************************************************
 * Function : rio_TxRxM_Delete_Instance
 *
 * Description: 
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/

static int rio_TxRxM_Delete_Instance( RIO_HANDLE_T handle )
{

    RIO_TXRXM_SERIAL_DS_T *instance = (RIO_TXRXM_SERIAL_DS_T*)handle;
    int res; /*variable for storing the result of routines invocation*/

    /* check pointers */
    if (instance == NULL ||
        instance->dp_Interfaces.start_Reset == NULL ||
        instance->pm_Interfaces.start_Reset == NULL)
        return RIO_ERROR;
    if(is_Handle_Is_Correct(handle) != RIO_TRUE) 
    {
        return RIO_ERROR;
    }
    remove_Handle_From_Storage(handle);
    


    res = (instance->dp_Interfaces.rio_Link_Delete_Instance)(instance->dp);
    CHECK_RESULT;
    res = (instance->pm_Interfaces.delete_Instance)(instance->pm);
    CHECK_RESULT;
    
    /*freing memory of all local buffers*/
    RIO_STxRxM_Tx_Data_Delete(instance);

    Correct_Free(instance->outbound_Data.out_Bufs.pop_Bit_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.pop_Byte_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.machine_Managment_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.single_Bit_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.cg_Column_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.cg_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.single_Char_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.char_Column_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.char_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.symbol_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.hp_Symbol_Buf.buffer);
    Correct_Free(instance->outbound_Data.out_Bufs.packet_Buf.buffer);
    
    /*free queue buffer*/
    Correct_Free(instance->outbound_Data.queue1.out_Queue.queue_Buffer);
    Correct_Free(instance->outbound_Data.queue2.out_Queue.queue_Buffer);
    Correct_Free(instance->outbound_Data.queue3.out_Queue.queue_Buffer);
    Correct_Free(instance->outbound_Data.queue_High_Prio_Symbols.queue_Buffer);
    
    Correct_Free(instance);



    return RIO_OK;
}

/***************************************************************************
 * Function : message 
 *
 * Description: message handler
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int message (
    RIO_CONTEXT_T                context,    /* callback context */
    RIO_PL_SERIAL_MSG_TYPE_T    msg_Type,
    char                        *string
    )
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T *)context;
    char msg_Buf[MAX_MESSAGE_BUF_SIZE];

    if (handle == NULL) return RIO_ERROR;

    if (handle->ext_Interfaces.rio_User_Msg == NULL) return RIO_OK;

    sprintf(msg_Buf, "STxRx%ld:%s:%s", handle->instance_Number, 
        msg_Type == RIO_PL_SERIAL_MSG_WARNING_MSG ? "Warning" :
        msg_Type == RIO_PL_SERIAL_MSG_ERROR_MSG ? "Error" : "Note",
        string);

    return (handle->ext_Interfaces.rio_User_Msg)(
        handle->ext_Interfaces.rio_Msg, msg_Buf);
}


/***************************************************************************
 * Function : set_State
 *
 * Description: request to write configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int set_State(
    RIO_HANDLE_T             handle_Param,    /* callback context */
    RIO_CONTEXT_T            caller_Inst,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            indicator_Value,
    unsigned int             indicator_Parameter
    )
{
    RIO_TXRXM_SERIAL_DS_T *  handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Param;
    if (handle == NULL) return RIO_ERROR;
    switch (indicator_ID)
    {
        case RIO_OUR_BUF_STATUS:
            handle->regs.our_Buf_Status = (RIO_UCHAR_T) indicator_Value;
            return RIO_OK;
        case RIO_PARTNER_BUF_STATUS:
            handle->regs.parthner_Buf_Status = (RIO_UCHAR_T) indicator_Value;
            return RIO_OK;
        case RIO_FLOW_CONTROL_MODE:
            return RIO_OK;
        case RIO_INBOUND_ACKID_STATUS:
            handle->regs.in_Ack_Id = (RIO_UCHAR_T) indicator_Value;
            return RIO_OK;
        case RIO_INBOUND_ACKID:
        case RIO_OUTBOUND_ACKID:
        case RIO_OUTSTANDING_ACKID:
        case RIO_TX_DATA_IN_PROGRESS:
        case RIO_RX_DATA_IN_PROGRESS:
        case RIO_INITIALIZED:
        case RIO_UNREC_ERROR:
            return RIO_OK;
        case RIO_OUTPUT_ERROR:
            return RIO_OK;
        case RIO_INPUT_ERROR:
            handle->regs.is_Inp_Err = (RIO_BOOL_T) indicator_Value;
            handle->regs.not_Accepted_Cause = 
                (RIO_PL_PACKET_NOT_ACCEPTED_CAUSE_T) indicator_Parameter;
/*            if ((RIO_PL_PACKET_NOT_ACCEPTED_CAUSE_T)indicator_Parameter == RIO_PL_PNACC_INVALID_CHARACTER &&
                RIO_TRUE == (RIO_BOOL_T) indicator_Value)
                handle->inbound_Data.skip_Granule = RIO_TRUE;*/
            return RIO_OK;
        case RIO_PORT_WIDTH:
            handle->regs.port_With = (RIO_UCHAR_T)indicator_Value;
            return RIO_OK;
        case RIO_INITIALIZED_PORT_WIDTH:
            handle->regs.initalized_Port_With = (RIO_UCHAR_T)indicator_Value;
            return RIO_OK;
        case RIO_PORT_WIDTH_OVERRIDE:
            handle->regs.port_With_Override = (RIO_UCHAR_T)indicator_Value;
            return RIO_OK;
        case RIO_LANE_WITH_INVALID_CHARACTER:
            handle->regs.lane_No = (RIO_UCHAR_T)indicator_Value;
            return RIO_OK;
        case RIO_IS_PORT_TRUE_4X:
            handle->regs.is_Port_4x = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE:
            handle->regs.curr_Byte_Rx = (RIO_UCHAR_T)indicator_Value;
            return RIO_OK;
        case RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE:
            handle->regs.curr_Byte_Tx = (RIO_UCHAR_T)indicator_Value;
            return RIO_OK;
        case RIO_ARE_ALL_IDLES_IN_GRANULE:
            handle->regs.are_All_Idles = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_SYMBOL_IS_PD:
            handle->regs.is_PD = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_IS_ALIGNMENT_LOST:
            handle->regs.is_Alignment_Lost = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_IS_SYNC_LOST:
            handle->regs.is_Sync_Lost = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_IS_CHARACTER_INVALID:
            handle->regs.is_Character_Invalid = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_COPY_TO_LANE2:
            handle->regs.is_Copy_To_Lane_2 = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_SUPPRESS_HOOK:
            handle->regs.supress_Hook = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_INITIAL_RUN_DISP:
            handle->regs.initial_Run_Disp = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_DECODE_ANY_RUN_DISP:
            handle->regs.decode_Any_Run_Disp = (unsigned int)indicator_Value;
            return RIO_OK;
        case RIO_IS_SC_PD_INCORRECT:
            handle->regs.incorrect_SC_PD = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_LANE2_JUST_FORCED_BY_DISCOVERY:
            handle->regs.lane_2_Just_Forced = (RIO_BOOL_T)indicator_Value;
            return RIO_OK;
        case RIO_SYMBOL_RX_FIRST_BYTE_VAL:
            handle->regs.first_Char_Val_Rx = (RIO_UCHAR_T)(indicator_Value & 0xFF);
            return RIO_OK;
        case RIO_PACKET_FINISHING_CNT:
            handle->regs.packet_Finishing_Cnt = (unsigned int)(indicator_Value);
            if (handle->regs.packet_Finishing_Cnt == 0)
                if (handle->ext_Interfaces.rio_TxRxM_Last_Packet_Granule != NULL)
                    handle->ext_Interfaces.rio_TxRxM_Last_Packet_Granule(
                        handle->ext_Interfaces.hook_Context);
            return RIO_OK;
        default: {}
    }
    
    return RIO_ERROR;
}

/***************************************************************************
 * Function : get_State
 *
 * Description: request to read configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int get_State(
    RIO_HANDLE_T             handle_Param, /* callback context */
    RIO_CONTEXT_T            caller_Inst,
    RIO_STATE_INDICATORS_T   indicator_ID,   /* word offset */
    unsigned long            *indicator_Value,
    unsigned int             *indicator_Parameter
    )
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T *)handle_Param;
    if (handle == NULL) return RIO_ERROR;
    switch (indicator_ID)
    {
        case RIO_OUR_BUF_STATUS:
            *indicator_Value = handle->regs.our_Buf_Status;
            return RIO_OK;
        case RIO_PARTNER_BUF_STATUS:
            *indicator_Value = handle->regs.parthner_Buf_Status;
            return RIO_OK;
        case RIO_FLOW_CONTROL_MODE:
            *indicator_Value = (RIO_BOOL_T)RIO_FALSE;
            return RIO_OK;
        case RIO_INBOUND_ACKID_STATUS:
            *indicator_Value = handle->regs.in_Ack_Id;
            return RIO_OK;
        case RIO_INBOUND_ACKID:
            *indicator_Value = handle->inbound_Data.in_Ack_Id;
            return RIO_OK;
        case RIO_OUTBOUND_ACKID:
            *indicator_Value = handle->outbound_Data.queue1.out_Ack_Id;
            return RIO_OK;
        case RIO_OUTSTANDING_ACKID:
            return RIO_OK;
        case RIO_TX_DATA_IN_PROGRESS:
            if (handle->outbound_Data.queue1.packet_Is_Sending == RIO_TRUE)
                *indicator_Value = RIO_TRUE;
            else 
                *indicator_Value = handle->regs.is_PD;
            return RIO_OK;
        case RIO_RX_DATA_IN_PROGRESS:
            *indicator_Value = handle->inbound_Data.packet_Is_Receiving;
            return RIO_OK;
        case RIO_INITIALIZED:
            *indicator_Value = handle->in_Reset == RIO_TRUE ? RIO_FALSE : RIO_TRUE;
            return RIO_OK;
        case RIO_UNREC_ERROR:
            return RIO_OK;
        case RIO_OUTPUT_ERROR:
            return RIO_OK;
        case RIO_INPUT_ERROR:
            *indicator_Value = handle->regs.is_Inp_Err;
            *indicator_Parameter = handle->regs.not_Accepted_Cause;
            return RIO_OK;
        case RIO_PORT_WIDTH:
            *indicator_Value = handle->regs.port_With;
            return RIO_OK;
        case RIO_INITIALIZED_PORT_WIDTH:
            *indicator_Value = handle->regs.initalized_Port_With;
            return RIO_OK;
        case RIO_PORT_WIDTH_OVERRIDE:
            *indicator_Value = handle->regs.port_With_Override;
            return RIO_OK;
        case RIO_LANE_WITH_INVALID_CHARACTER:
            *indicator_Value = handle->regs.lane_No;
            return RIO_OK;
        case RIO_IS_PORT_TRUE_4X:
            *indicator_Value = handle->regs.is_Port_4x;
            return RIO_OK;
        case RIO_CUR_BYTE_NUMBER_IN_RCV_GRANULE:
            *indicator_Value = handle->regs.curr_Byte_Rx;
            return RIO_OK;
        case RIO_CUR_BYTE_NUMBER_IN_TRX_GRANULE:
            *indicator_Value = handle->regs.curr_Byte_Tx;
            return RIO_OK;
        case RIO_ARE_ALL_IDLES_IN_GRANULE:
            *indicator_Value = handle->regs.are_All_Idles;
            return RIO_OK;
        case RIO_SYMBOL_IS_PD:
            if (handle->outbound_Data.queue1.packet_Is_Sending == RIO_TRUE)
                *indicator_Value = RIO_TRUE;
            else 
            {
                *indicator_Value = handle->regs.is_PD;
                handle->regs.is_PD = RIO_FALSE;
            }
            /**indicator_Value = handle->outbound_Data.queue1.packet_Is_Sending;*/
            return RIO_OK;
        case RIO_IS_ALIGNMENT_LOST:
            *indicator_Value = handle->regs.is_Alignment_Lost;
            return RIO_OK;
        case RIO_IS_SYNC_LOST:
            *indicator_Value = handle->regs.is_Sync_Lost;
            return RIO_OK;
        case RIO_IS_CHARACTER_INVALID:
            *indicator_Value = handle->regs.is_Character_Invalid;
            return RIO_OK;
        case RIO_COPY_TO_LANE2:
            *indicator_Value = handle->regs.is_Copy_To_Lane_2;
            return RIO_OK;
        case RIO_SUPPRESS_HOOK:
            *indicator_Value = handle->regs.supress_Hook;
            return RIO_OK;
        case RIO_INITIAL_RUN_DISP:
            *indicator_Value = handle->regs.initial_Run_Disp;
            return RIO_OK;
        case RIO_DECODE_ANY_RUN_DISP:
            *indicator_Value = handle->regs.decode_Any_Run_Disp;
            return RIO_OK;
        case RIO_IS_SC_PD_INCORRECT:
            *indicator_Value = handle->regs.incorrect_SC_PD;
            return RIO_OK;
        case RIO_LANE2_JUST_FORCED_BY_DISCOVERY:
            *indicator_Value = handle->regs.lane_2_Just_Forced;
            return RIO_OK;
        case RIO_RX_IS_PD:
            *indicator_Value = handle->inbound_Data.packet_Is_Receiving;
            return RIO_OK;
        case RIO_SYMBOL_RX_FIRST_BYTE_VAL:
            *indicator_Value = handle->regs.first_Char_Val_Rx;
            return RIO_OK;
        case RIO_PACKET_FINISHING_CNT:
            *indicator_Value = handle->regs.packet_Finishing_Cnt;
            return RIO_OK;
        default: {}
    } /*end of switch*/
    return RIO_ERROR;
}


/***************************************************************************
 * Function : get_State
 *
 * Description: request to read configuration register
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void init_Registers (RIO_TXRXM_SERIAL_DS_T * handle)
{
    RIO_TXRXM_SERIAL_REGISTERS_T * regs; /*temporary cvariable*/
    if (handle == NULL) return;

    regs = &(handle->regs);

    regs->in_Ack_Id = 0;
    regs->our_Buf_Status = RIO_STXRXRM_INITIAL_BUF_STATUS;
    regs->parthner_Buf_Status = RIO_STXRXRM_INITIAL_BUF_STATUS;
    regs->is_Inp_Err = RIO_FALSE;
    /*regs->not_Accepted_Cause; - unnecessary to initialized*/
    /*values of the control csr*/
    regs->port_With = handle->initialization_Param_Set.lp_Serial_Is_1x == RIO_TRUE ?
        RIO_STXRXM_PORT_WITH_SLINGLE_LANE : RIO_STXRXM_PORT_WITH_FOUR_LANE;
    regs->initalized_Port_With = handle->initialization_Param_Set.is_Force_1x_Mode == RIO_FALSE ?
        RIO_STXRXM_INITIALIZED_PORT_WITH_4 : 
        handle->initialization_Param_Set.is_Force_1x_Mode_Lane_0 == RIO_TRUE ? 
            RIO_STXRXM_INITIALIZED_PORT_WITH_0 : RIO_STXRXM_INITIALIZED_PORT_WITH_2;
    /*initialize override port with*/
    if(handle->initialization_Param_Set.lp_Serial_Is_1x == RIO_TRUE ||
        handle->initialization_Param_Set.is_Force_1x_Mode == RIO_FALSE)
        regs->port_With_Override = 0;
    else regs->port_With_Override = handle->initialization_Param_Set.is_Force_1x_Mode_Lane_0 == RIO_TRUE ? 
        RIO_STXRXM_LANE_0_FORSED : RIO_STXRXM_LANE_2_FORSED;

    regs->lane_No = 0;  /* number of the lane where invalid character occured */
    if ( handle->initialization_Param_Set.is_Force_1x_Mode == RIO_TRUE ||
            handle->initialization_Param_Set.lp_Serial_Is_1x == RIO_TRUE)
        regs->is_Port_4x = RIO_FALSE;
    else regs->is_Port_4x = RIO_TRUE;
    regs->curr_Byte_Tx = 0; /* in case of 1x mode it is a number of the current byte in received granule */
    regs->curr_Byte_Rx = 0; /* in case of 1x mode it is a number of the current byte in transmitted granule */
    regs->is_PD = RIO_FALSE;
    regs->is_Copy_To_Lane_2 = RIO_FALSE; /*true in case of forse 2 lane in 4x mode*/
    /*initialize registers which are is necessary only for pcs/pma*/
    regs->is_Alignment_Lost = RIO_FALSE;
    regs->is_Sync_Lost = RIO_FALSE;
    regs->is_Character_Invalid = RIO_FALSE;
    regs->supress_Hook = RIO_FALSE;
    regs->initial_Run_Disp = PCS_PMA__RUN_DISP_NOT_CHANGE;
    regs->decode_Any_Run_Disp = 0;
    regs->incorrect_SC_PD = RIO_FALSE;
    regs->lane_2_Just_Forced = RIO_FALSE;
    regs->packet_Finishing_Cnt = 0;
}


/***************************************************************************
 * Function : is_Handle_Is_Correct
 *
 * Description: Check that passed handle is the handle to the serial TxRx model
 *
 * Returns: RIO_TRUE if handle is correct RIO_FALSE otherwise
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T is_Handle_Is_Correct(RIO_HANDLE_T handle)
{
    RIO_TXRX_HANDLE_STORAGE_T * it; /*iterator*/

    it = serial_Handles.next;

    while (it != NULL)
    {
        if (it->handle == handle) return RIO_TRUE;
        it = it->next;
    }
    /*printf("STxRx Error: Incorrect handle\n");*/
    return RIO_FALSE;
}

/***************************************************************************
 * Function : add_Handle
 *
 * Description: Adds handle to the handle storage
 *
 * Returns: Error code
 *
 * Notes: none
 *
 **************************************************************************/
static int add_Handle(RIO_HANDLE_T handle)
{
    RIO_TXRX_HANDLE_STORAGE_T * item; /*pointer to the new item*/
    item = (RIO_TXRX_HANDLE_STORAGE_T*)malloc(sizeof(RIO_TXRX_HANDLE_STORAGE_T));
    if (item == NULL) return RIO_ERROR;
    item->handle = handle;
    item->next = serial_Handles.next;
    serial_Handles.next = item;
    return RIO_OK;
}

/***************************************************************************
 * Function : remove_Handle_Storage
 *
 * Description: Kills handler storage
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void remove_Handle_Storage(void)
{
    RIO_TXRX_HANDLE_STORAGE_T * pDell; /*pointer to deleted item*/

    while (serial_Handles.next != NULL)
    {
        pDell = serial_Handles.next;
        serial_Handles.next = pDell->next;
        free (pDell);
    }
}

/***************************************************************************
 * Function : remove_Handle_From_Storage
 *
 * Description: Removes specified handle from the storage
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void remove_Handle_From_Storage(RIO_HANDLE_T handle)
{
    RIO_TXRX_HANDLE_STORAGE_T * it; /*iterator*/
    RIO_TXRX_HANDLE_STORAGE_T * id; /*iterator to items for deleting*/

    if (serial_Handles.next == NULL) return;
    /*check first element*/
    if (serial_Handles.next->handle == handle)
    {
        id = serial_Handles.next;
        serial_Handles.next = id->next;
        free(id);
        return;
    }
    /*if this is not first element then search in the others element*/
    it = serial_Handles.next;
    while (it->next != NULL)
    {
        if (it->next->handle == handle)
        {
            id = it->next;
            it->next = id->next;
            free(id);
            return;
        }
        it = it->next;
    }
    
}

/***************************************************************************
 * Function : RIO_TxRx_Set_Random_Generator
 *
 * Description: Initializes random generator
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRx_Set_Random_Generator(
    unsigned int seed_Value
    )
{
    /* Setup seed value */
  	srand(seed_Value);
}
