/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/rio_txrxm_serial_rx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model receiver source
*
* Notes:        
*
******************************************************************************/
#include <stdlib.h>
#ifdef _DEBUG
#include <stdio.h>
#endif

#include "rio_txrxm_serial_ds.h"
#include "rio_txrxm_serial_rx.h"
#include "rio_txrxm_common_rx.h"

/*granule types*/
typedef enum {
    RIO_STXRXM_SOP, /*start of packet*/
    RIO_STXRXM_EOP, /*end of packet*/
    RIO_STXRXM_DATA, /*data granule*/
    RIO_STXRXM_RESTART_FROM_RETRY,
    RIO_STXRXM_STOMP,
    RIO_STXRXM_PACKET_ACCEPTED,
    RIO_STXRXM_PACKET_RETRY,
    RIO_STXRXM_PACKET_NOT_ACCEPTED,
    RIO_STXRXM_LINK_RESPONSE,
    RIO_STXRXM_NOP,
    RIO_STXRXM_RESERVED,
    RIO_STXRXM_STATUS,
    RIO_STXRXM_MULTICAST,
    RIO_STXRXM_RESET,
    RIO_STXRXM_CORRUPT /*corrupted granule*/
} RIO_STXRXM_GR_TYPE_T;
static void init_Arbiter(RIO_TXRXM_SERIAL_DS_T *instance);
static int put_CG(
    RIO_TXRXM_SERIAL_DS_T * handle,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup        /* pointer to codegroup data structure */
);
static int put_Char_Column(
    RIO_TXRXM_SERIAL_DS_T * handle,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
);


static RIO_STXRXM_GR_TYPE_T granule_Type_0(RIO_PCS_PMA_GRANULE_T *granule);
static RIO_STXRXM_GR_TYPE_T granule_Type_1(RIO_PCS_PMA_GRANULE_T *granule);


static void handler_Data(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    RIO_PCS_PMA_GRANULE_T  *granule);

static void handler_Violent_Symbol(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_PCS_PMA_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num,
    RIO_SYMBOL_SERIAL_VIOLATION_T violation);

static void handler_Symbol(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_PCS_PMA_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num);

static void cancel_Packet(RIO_TXRXM_SERIAL_DS_T *handle);

static void handler_Start_New_Packet(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    RIO_PCS_PMA_GRANULE_T  *granule);

static void finish_Packet(RIO_TXRXM_SERIAL_DS_T *handle);

static int in_Check_Current(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_BOOL_T *error_Flag, 
    RIO_PACKET_VIOLATION_T *violation_Type,
    unsigned short *expected_crc);

/*static unsigned int check_Reserved_Fields( 
    RIO_TXRXM_SERIAL_DS_T *handle, 
    RIO_TXRXM_PACKET_ARB_T *arbiter);*/

static void accept_Violated_Packet(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    RIO_PACKET_VIOLATION_T violation_Type,
    unsigned short expected_Crc);

static void parse_Packet(RIO_TXRXM_SERIAL_DS_T *handle);

static void in_Load_From_Stream(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_REMOTE_REQUEST_T *req_Struct,
    RIO_UCHAR_T          *ack_ID,
    RIO_TR_INFO_T        *transport_Info,
    RIO_UCHAR_T          *hop_Count);

/*calllbacks for the DP*/
/***************************************************************************
 * Function : RIO_STxRxM_Put_CG
 *
 * Description: Pass codegroup column from DP to the upper levels
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
int RIO_STxRxM_Put_CG(
    RIO_CONTEXT_T					handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup        /* pointer to codegroup data structure */
    )
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    if (handle_Context == NULL || codegroup == NULL) return RIO_ERROR;

    if (handle->mach_State.sync_Enabled == RIO_TRUE)
        return (handle->pm_Interfaces.put_Codegroup)(handle->pm, codegroup);
    else
        return put_CG(handle, codegroup);
}

/***************************************************************************
 * Function : RIO_STxRxM_Put_Character_Column
 *
 * Description: Pass character column from DP to the upper levels
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
int RIO_STxRxM_Put_Character_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
    )
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    unsigned long char_Val; /*SC/PD value*/

    if (handle_Context == NULL || character == NULL) return RIO_ERROR;


    switch (character->character_Type[0])
    {
        case RIO_SC: char_Val = 0x1C; break;
        case RIO_PD: char_Val = 0x7C; break;
        case RIO_K: char_Val = 0xBC; break;
        case RIO_A: char_Val = 0xFB; break;
        case RIO_R: char_Val = 0xFD; break;
        case RIO_I: char_Val = character->character_Data[0]; break;
        default: char_Val = 0;
    }

    if(char_Val)
        handle->set_State(handle, NULL, RIO_SYMBOL_RX_FIRST_BYTE_VAL, char_Val, 0);

    if (handle->mach_State.align_Enabled == RIO_TRUE)
        return (handle->pm_Interfaces.put_Character_Column)(handle->pm, character);
    else
        return put_Char_Column(handle, character);
}

/***************************************************************************
 * Function : RIO_STxRxM_Put_Granule
 *
 * Description: Pass granule from DP to the upper levels
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
int RIO_STxRxM_Put_Granule(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T	  *granule          /* pointer to granule data structure */
    )
{
    int res;
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    if (handle_Context == NULL || granule == NULL) return RIO_ERROR;

    res = (handle->pm_Interfaces.put_Granule)(handle->pm, granule);
    /*check that idles inside the packet are inserted*/

    if (handle->regs.is_Inp_Err == RIO_TRUE && 
        handle->regs.not_Accepted_Cause == RIO_PL_PNACC_INVALID_CHARACTER &&
        handle->regs.incorrect_SC_PD != RIO_TRUE &&
        handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
    {
        RIO_PCS_PMA_GRANULE_T no_Data; /*stub*/

        no_Data.is_Data = RIO_FALSE;
        no_Data.granule_Struct.cmd = RIO_STXRX_GRANULE_ACCEPTOR_DO_NOTHING;

        RIO_STxRxM_Granule_Acceptor( handle_Context, &no_Data);
    }

    return res;
}

/*implementation of the PM callbacks*/
/***************************************************************************
 * Function : RIO_STxRxM_Pass_CG
 *
 * Description: pass CG column from the DP to PM
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
int RIO_STxRxM_Pass_CG(
    RIO_CONTEXT_T					handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup        /* pointer to codegroup data structure */
    )
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    if (handle_Context == NULL || codegroup == NULL) return RIO_ERROR;

    if (handle->mach_State.sync_Enabled == RIO_TRUE)
        return put_CG(handle, codegroup);
    else
        return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Pass_Character_Column
 *
 * Description: Pass character column from PM to the DP levels
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
int RIO_STxRxM_Pass_Character_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
    )
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    if (handle_Context == NULL || character == NULL) return RIO_ERROR;


    if (handle->mach_State.align_Enabled == RIO_TRUE)
        return put_Char_Column(handle, character);
    else
        return RIO_OK;
}



/*routines for passing control from the pm to dp*/
/***************************************************************************
 * Function : put_CG_Column
 *
 * Description: Puts CG column to the DP
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
static int put_CG(
    RIO_TXRXM_SERIAL_DS_T * handle,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T * codegroup        /* pointer to codegroup data structure */
)
{
    unsigned char lNum; /*lane number*/
    RIO_UCHAR_T lane_Mask;  /*temporary lane mask storage*/ 
    RIO_PCS_PMA_CODE_GROUP_T cg_To_User; /*codegroup for user*/
    /*invoke user callback*/
    if (handle->ext_Interfaces.rio_TxRxM_Serial_Code_Group_Received != NULL &&
        handle->regs.supress_Hook == RIO_FALSE)
    {
        cg_To_User = *codegroup;
        if (handle->regs.is_Copy_To_Lane_2 && cg_To_User.lane_Mask == 1)
        {
            cg_To_User.lane_Mask = RIO_TXRXM_SERIAL_2_LANE_MASK;
            cg_To_User.is_Running_Disparity_Positive[RIO_TXRXM_LANE_2] = 
                cg_To_User.is_Running_Disparity_Positive[RIO_TXRXM_LANE_0];
            cg_To_User.code_Group_Data[RIO_TXRXM_LANE_2] = 
                cg_To_User.code_Group_Data[RIO_TXRXM_LANE_0];
        }
        (handle->ext_Interfaces.rio_TxRxM_Serial_Code_Group_Received)(
            handle->ext_Interfaces.txrxm_Interface_Context, cg_To_User);
    }
    /*store codegroup data to the internal buffer*/
    /*determine lane num*/
    lNum = 0;
    lane_Mask = codegroup->lane_Mask;
    while ( lNum < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN)
    {
        if ((lane_Mask & 1) == 1) break;
        lNum++;
        lane_Mask = lane_Mask >> 1;
    }
    /*store codegroup value*/
    handle->inbound_Data.codegroups.code_Group_Data[lNum] = 
        codegroup->code_Group_Data[lNum];
    handle->inbound_Data.codegroups.is_Running_Disparity_Positive[lNum] = 
        codegroup->is_Running_Disparity_Positive[lNum];

    return (handle->dp_Interfaces.put_Codegroup)(handle->dp, codegroup);
}

/***************************************************************************
 * Function : put_Char_Column
 *
 * Description: Puts Character column to the DP
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
static int put_Char_Column(
    RIO_TXRXM_SERIAL_DS_T * handle,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
)
{
    int i;
    RIO_PCS_PMA_CHARACTER_T usr_Char; /*character for external interface*/
    RIO_UCHAR_T mask; /*mask for detecton of invalid codegroups*/
    if (character == NULL) return RIO_ERROR;


    /*search for invalid characters*/
    mask = 0;
	for (i = 0 ; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++ )
    {
		if (((character->lane_Mask >> i) & 1) == 1)
			if (character->character_Type[i] == RIO_I)
				mask |= (1 << i );
    }
    /*check that invalid characters are absent*/
    if (mask == 0 && 
        handle->ext_Interfaces.rio_TxRxM_Serial_Character_Received != NULL &&
        handle->regs.supress_Hook == RIO_FALSE)
    {
        usr_Char = *character;
        if (handle->regs.is_Copy_To_Lane_2 == RIO_TRUE && usr_Char.lane_Mask == 1)
        {
            usr_Char.lane_Mask = RIO_TXRXM_SERIAL_2_LANE_MASK;
            usr_Char.character_Data[RIO_TXRXM_LANE_2] = 
                usr_Char.character_Data[RIO_TXRXM_LANE_0];
            usr_Char.character_Type[RIO_TXRXM_LANE_2] = 
                usr_Char.character_Type[RIO_TXRXM_LANE_0];
        }

        (handle->ext_Interfaces.rio_TxRxM_Serial_Character_Received)(
            handle->ext_Interfaces.txrxm_Interface_Context, usr_Char);
    }
    else if (handle->ext_Interfaces.rio_TxRxM_Serial_Violent_Character_Received != NULL &&
        handle->regs.supress_Hook == RIO_FALSE)
    {
        /*prepare all necessary data for user callback invocation*/
        usr_Char = *character;
        
        if (handle->regs.is_Copy_To_Lane_2 == RIO_TRUE && mask == 1)
        {
            handle->inbound_Data.codegroups.lane_Mask = RIO_TXRXM_SERIAL_2_LANE_MASK;
            handle->inbound_Data.codegroups.code_Group_Data[RIO_TXRXM_LANE_2] = 
                handle->inbound_Data.codegroups.code_Group_Data[RIO_TXRXM_LANE_0];
            handle->inbound_Data.codegroups.is_Running_Disparity_Positive[RIO_TXRXM_LANE_2] = 
                handle->inbound_Data.codegroups.is_Running_Disparity_Positive[RIO_TXRXM_LANE_0];

            usr_Char.lane_Mask = RIO_TXRXM_SERIAL_2_LANE_MASK;
            usr_Char.character_Data[RIO_TXRXM_LANE_2] = 
                usr_Char.character_Data[RIO_TXRXM_LANE_0];
            usr_Char.character_Type[RIO_TXRXM_LANE_2] = 
                usr_Char.character_Type[RIO_TXRXM_LANE_0];
        }
        else handle->inbound_Data.codegroups.lane_Mask = mask;

        (handle->ext_Interfaces.rio_TxRxM_Serial_Violent_Character_Received)(
            handle->ext_Interfaces.txrxm_Interface_Context, 
            handle->inbound_Data.codegroups, usr_Char, handle->inbound_Data.codegroups.lane_Mask);
    }

    return (handle->dp_Interfaces.put_Character_Column)(handle->dp, character);
}


/*routines for implementation of packet accepting*/
/***************************************************************************
 * Function : RIO_STxRxM_Rx_Data_Reset
 *
 * Description: Initializes the inbound data structures
 *
 * Returns: none
 *
 * Notes: noone
 *
 **************************************************************************/
void RIO_STxRxM_Rx_Data_Init(RIO_TXRXM_SERIAL_DS_T *instance)
{
    init_Arbiter(instance);
    instance->inbound_Data.pop_Bit_Requests = 0;
    instance->inbound_Data.skip_Granule = RIO_FALSE;
    instance->inbound_Data.in_Ack_Id = 0;
}

/***************************************************************************
 * Function : init_Arbiter
 *
 * Description: resets the arbiter
 *
 * Returns: none
 *
 * Notes: noone
 *
 **************************************************************************/
static void init_Arbiter(RIO_TXRXM_SERIAL_DS_T *instance)
{
    instance->inbound_Data.packet_Is_Receiving = RIO_FALSE;
    instance->inbound_Data.inbound_Arb.cur_Pos = 0;
    instance->inbound_Data.inbound_Arb.packet_Len = 0;
}

/***************************************************************************
 * Function : RIO_STxRxM_Granule_Acceptor
 *
 * Description: Performs packet construction
 *
 * Returns: Error code
 *
 * Notes: noone
 *
 **************************************************************************/
int RIO_STxRxM_Granule_Acceptor(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T	  *granule          /* pointer to granule data structure */
)
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    unsigned int gran_Num = 0;/*receiving packet current granule num*/
    RIO_BOOL_T is_First_Emb;     /*true when after SOP embedded control symbol*/
    if (handle_Context == NULL || granule == NULL) return RIO_ERROR;

    /*get current granule num in packet*/
    if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
    {
        if (handle->inbound_Data.inbound_Arb.cur_Pos == 0)
            gran_Num = 0;
        else
            gran_Num = (unsigned int)((handle->inbound_Data.inbound_Arb.cur_Pos - 1) / RIO_TXRXM_BYTE_CNT_IN_WORD);
    }
    is_First_Emb = RIO_FALSE;
    /*If this is the first symbol then necessary 
    assume that this is FREE symbol*/
    if (handle->inbound_Data.inbound_Arb.cur_Pos == 0)
        is_First_Emb = RIO_TRUE;

    

    if (handle->regs.is_Inp_Err == RIO_TRUE && 
        handle->regs.not_Accepted_Cause == RIO_PL_PNACC_INVALID_CHARACTER &&
        handle->regs.incorrect_SC_PD != RIO_TRUE &&
        handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
    {
        /*terminate packet in case of invalid characters inside*/
        handle->regs.is_Inp_Err = RIO_FALSE;
        cancel_Packet(handle);
        return RIO_OK;
    }

    /*check that parameter is not stub from the RIO_STxRxM_Put_Granule
    routine*/
    if (    granule->is_Data == RIO_FALSE && 
        granule->granule_Struct.cmd == RIO_STXRX_GRANULE_ACCEPTOR_DO_NOTHING)
    {
        return RIO_OK;
    }


    /*check that this granule doesn't contain invalid codegroup*/
    if (handle->inbound_Data.skip_Granule == RIO_TRUE)
    {
/*
    The following lines reports about incorrect granule in case
    when invalid codegroup is received.

    At now these lines are commented because double error reporting is not necessary

        if (is_First_Emb == RIO_TRUE || handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            handler_Violent_Symbol(handle, granule, FREE, 0, 
                RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_CRC);
        else 
            handler_Violent_Symbol(handle, granule, FREE, 0, 
                RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_CRC);
*/
        handle->regs.is_Inp_Err = RIO_FALSE;
        handle->inbound_Data.skip_Granule = RIO_FALSE;
        return RIO_OK;
    }
    handle->regs.is_Inp_Err = RIO_FALSE;

    if (handle->regs.incorrect_SC_PD == RIO_TRUE)
    {
        handle->regs.incorrect_SC_PD = RIO_FALSE;

        if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
        {
            handler_Violent_Symbol(handle, granule, EMBEDDED, gran_Num, 
                RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_SC_PD);
        }
        else
        {
            handler_Violent_Symbol(handle, granule, FREE, 0, 
                RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_SC_PD);
        }
        return RIO_OK;
    }


    /*perform actions according granule type*/
    switch (granule_Type_0(granule))
    {

        case RIO_STXRXM_DATA:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                handler_Data(handle, granule);
                return RIO_OK;
            }
            else
            {
                handler_Violent_Symbol(handle, granule, FREE, 0, 
                    RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_SC_PD);
                return RIO_OK;
            }
        case RIO_STXRXM_PACKET_ACCEPTED:
        case RIO_STXRXM_PACKET_RETRY:
        case RIO_STXRXM_PACKET_NOT_ACCEPTED:
        case RIO_STXRXM_LINK_RESPONSE:
        case RIO_STXRXM_STATUS:
            break;
        case RIO_STXRXM_CORRUPT:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                if (is_First_Emb == RIO_FALSE)
                    handler_Violent_Symbol(handle, granule, EMBEDDED, gran_Num, 
                        RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_CRC);
                else
                    handler_Violent_Symbol(handle, granule, FREE, 0, 
                        RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_CRC);
                return RIO_OK;
            }
            handler_Violent_Symbol(handle, granule, FREE, 0, 
                RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_CRC);
            return RIO_OK;
        default : {}
    }        
    /*perform 2 part of symbol passing*/
    switch (granule_Type_1(granule))
    {
        case RIO_STXRXM_SOP:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                finish_Packet(handle);
            }
            handler_Start_New_Packet(handle, granule);
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_STXRXM_EOP:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                finish_Packet(handle);
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_STXRXM_RESTART_FROM_RETRY:
        case RIO_STXRXM_STOMP:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                cancel_Packet(handle);
                handler_Symbol(handle, granule, CANCELING, gran_Num);
                break;
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_STXRXM_NOP:
        case RIO_STXRXM_RESERVED:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                if (is_First_Emb == RIO_FALSE)
                    handler_Symbol(handle, granule, EMBEDDED, gran_Num);
                else 
                    handler_Symbol(handle, granule, FREE, 0);
                break;
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_STXRXM_STATUS:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                cancel_Packet(handle);
                handler_Symbol(handle, granule, CANCELING, gran_Num);
                break;
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_STXRXM_MULTICAST:
        case RIO_STXRXM_RESET:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                if (is_First_Emb == RIO_FALSE)
                    handler_Symbol(handle, granule, EMBEDDED, gran_Num);
                else 
                    handler_Symbol(handle, granule, FREE, 0);
                break;
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        default : {}
    }

    return RIO_OK;
}


/***************************************************************************
 * Function : granule_Type
 *
 * Description: Detect type of granule
 *
 * Returns: Granule type
 *
 * Notes: noone
 *
 **************************************************************************/
static RIO_STXRXM_GR_TYPE_T granule_Type_0(RIO_PCS_PMA_GRANULE_T *granule)
{
    if (granule->is_Data == RIO_TRUE) return RIO_STXRXM_DATA;
    if (granule->granule_Struct.is_Crc_Valid == RIO_FALSE) return RIO_STXRXM_CORRUPT;

    /*the constants placed before are used only in current place
    so no definitions for ones are necessary*/
    if (granule->granule_Struct.stype0 == 0) return RIO_STXRXM_PACKET_ACCEPTED;
    if (granule->granule_Struct.stype0 == 1) return RIO_STXRXM_PACKET_RETRY;
    if (granule->granule_Struct.stype0 == 2) return RIO_STXRXM_PACKET_NOT_ACCEPTED;
    if (granule->granule_Struct.stype0 == 4) return RIO_STXRXM_STATUS;
    if (granule->granule_Struct.stype0 == 6) return RIO_STXRXM_LINK_RESPONSE;
    return RIO_STXRXM_RESERVED;
}

/***************************************************************************
 * Function : granule_Type
 *
 * Description: Detect type of granule
 *
 * Returns: Granule type
 *
 * Notes: noone
 *
 **************************************************************************/
static RIO_STXRXM_GR_TYPE_T granule_Type_1(RIO_PCS_PMA_GRANULE_T *granule)
{
    if (granule->is_Data == RIO_TRUE) return RIO_STXRXM_DATA;
    if (granule->granule_Struct.is_Crc_Valid == RIO_FALSE) return RIO_STXRXM_CORRUPT;

    /*the constants placed before are used only in current place
    so no definitions for ones are necessary*/
    if (granule->granule_Struct.stype1 == 0) return RIO_STXRXM_SOP;
    if (granule->granule_Struct.stype1 == 1) return RIO_STXRXM_STOMP;
    if (granule->granule_Struct.stype1 == 2) return RIO_STXRXM_EOP;
    if (granule->granule_Struct.stype1 == 3) return RIO_STXRXM_RESTART_FROM_RETRY;
    if (granule->granule_Struct.stype1 == 5) return RIO_STXRXM_MULTICAST;
    if (granule->granule_Struct.stype1 == 7) return RIO_STXRXM_NOP;
    if (granule->granule_Struct.stype1 == 4)
    {
        if (granule->granule_Struct.cmd == 3) return RIO_STXRXM_RESET;
        if (granule->granule_Struct.cmd == 4) return RIO_STXRXM_STATUS;
    }
    return RIO_STXRXM_RESERVED;
}


/***************************************************************************
 * Function : handler_Data
 *
 * Description: packet data handler
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Data(RIO_TXRXM_SERIAL_DS_T *handle, RIO_PCS_PMA_GRANULE_T  *granule)
{
    /*help variables*/
    unsigned int i;
    RIO_TXRXM_PACKET_ARB_T* arbiter;

    arbiter = &(handle->inbound_Data.inbound_Arb);
    
    for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
    {
        arbiter->packet_Stream[arbiter->cur_Pos] = granule->granule_Date[i];
        arbiter->cur_Pos++;
        if (arbiter->cur_Pos == RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN)
        {
            finish_Packet(handle);
            break;
        }
    }
}

/***************************************************************************
 * Function : handler_Violent_Symbol
 *
 * Description: actions when violent control symbol received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Violent_Symbol(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_PCS_PMA_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num,
    RIO_SYMBOL_SERIAL_VIOLATION_T violation)
{
    unsigned int symbol, i; /*form symbol struct*/
    unsigned int stub;      /*temporary variable*/
    unsigned long char_Val; /*SC/PD value*/

    /* 
     * 1) if the receiving process is in progress, check if current packet shall be canceled or finished at first,
     * 2) form the appropriate symbol structure and pass it to the environment  
     */

    if (handle->ext_Interfaces.rio_TxRxM_Serial_Violent_Symbol_Received == NULL ||
        handle->regs.supress_Hook == RIO_TRUE) return;

    /*convert granule array to value*/
    symbol = 0;
    if (granule->is_Data == RIO_TRUE)
    {
        for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
        {
            symbol = symbol << RIO_TXRXM_BITS_IN_BYTE;
            symbol |= granule->granule_Date[i];
        }
    }
    else
    {   /*the constants which are used before is used only here so 
        it is not necessary to prepare defines for ones*/

        /*SC/PD*/
        handle->get_State(handle, NULL/*caller inst is not used*/, 
            RIO_SYMBOL_RX_FIRST_BYTE_VAL, &char_Val, &stub);
        symbol = (char_Val & 0xFF);
        symbol = symbol << RIO_TXRXM_BITS_IN_BYTE;
        /*symbol fields*/
        symbol |= (granule->granule_Struct.stype0 << 5) | (granule->granule_Struct.parameter0 & 0x1F);
        symbol = symbol << RIO_TXRXM_BITS_IN_BYTE;

        symbol |= (granule->granule_Struct.parameter1 << 3) | (granule->granule_Struct.stype1 & 7);
        symbol = symbol << RIO_TXRXM_BITS_IN_BYTE;

        symbol |= (granule->granule_Struct.cmd << 5) | (granule->granule_Struct.crc & 0x1F);
    }

    handle->ext_Interfaces.rio_TxRxM_Serial_Violent_Symbol_Received(
        handle->ext_Interfaces.txrxm_Interface_Context,
        symbol, place, gran_Num, violation);
}

/***************************************************************************
 * Function : handle_Symbol
 *
 * Description: actions when control symbol received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Symbol(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_PCS_PMA_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num)
{
    RIO_SYMBOL_SERIAL_STRUCT_T symbol; /*data for passing to the user callback*/

    if (handle->ext_Interfaces.rio_TxRxM_Serial_Symbol_Received != NULL &&
        handle->regs.supress_Hook == RIO_FALSE)
    {    
        unsigned int stub;      /*temporary variable*/
        unsigned long char_Val; /*SC/PD value*/

        handle->get_State(handle, NULL/*caller inst is not used*/, 
            RIO_SYMBOL_RX_FIRST_BYTE_VAL, &char_Val, &stub);

        symbol = granule->granule_Struct;
        symbol.first_Char = (RIO_UCHAR_T)char_Val;


        (handle->ext_Interfaces.rio_TxRxM_Serial_Symbol_Received)(
            handle->ext_Interfaces.txrxm_Interface_Context,
            &symbol, place, gran_Num);
    }
}


/***************************************************************************
 * Function : cancel_Packet
 *
 * Description: form packet array to pass canceled packet
 *              and invoke external callback
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void cancel_Packet(RIO_TXRXM_SERIAL_DS_T *handle)
{
    /*structures to hold passing packets*/
    RIO_UCHAR_T packet_Buffer[RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN];
    unsigned int packet_Size, i;

    /*try to pass packet*/
    if (handle->ext_Interfaces.rio_TxRxM_Canceled_Packet_Received != NULL)
    {
        /*fill passing data*/
        packet_Size = handle->inbound_Data.inbound_Arb.cur_Pos;
        for (i = 0; i < packet_Size; i++)
            packet_Buffer[i] = handle->inbound_Data.inbound_Arb.packet_Stream [i];

        /*invoke callback*/
        handle->ext_Interfaces.rio_TxRxM_Canceled_Packet_Received(
            handle->ext_Interfaces.txrxm_Interface_Context,
            packet_Buffer, packet_Size);

    }

    /*init arbiter */
    init_Arbiter(handle);
}

/***************************************************************************
 * Function : handler_Start_New_Packet
 *
 * Description: packet data handler
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Start_New_Packet(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    RIO_PCS_PMA_GRANULE_T  *granule)
{
    /*help variables*/
    RIO_TXRXM_PACKET_ARB_T* arbiter;

    /*if anouther packet was currently receiving, itv shall be finished*/
    if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
        finish_Packet(handle);

    /*get arbiter*/
    arbiter = &(handle->inbound_Data.inbound_Arb);

    /*init arbiter*/
    init_Arbiter(handle);

    /*Start of packet symbol is not placed here*/

    handle->inbound_Data.packet_Is_Receiving = RIO_TRUE;
}


/***************************************************************************
 * Function : finish_Packet
 *
 * Description: finishes packet receiving
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void finish_Packet(RIO_TXRXM_SERIAL_DS_T *handle)
{
    RIO_BOOL_T error_Flag;
    RIO_PACKET_VIOLATION_T violation_Type;
    unsigned short expected_Crc = RIO_TXRXM_INIT_CRC_VALUE;


    if (in_Check_Current(handle, &error_Flag, &violation_Type, &expected_Crc) == RIO_OK)
    {
        if (error_Flag == RIO_FALSE)
            parse_Packet(handle);
        else
            accept_Violated_Packet(handle, violation_Type, expected_Crc);

    }   
    /*init arbiter*/
    init_Arbiter(handle);
}

/***************************************************************************
 * Function : in_Check_Current
 *
 * Description: check receiving packet stream if it could be parsered
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int in_Check_Current(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_BOOL_T *error_Flag, 
    RIO_PACKET_VIOLATION_T *violation_Type,
    unsigned short *expected_Crc)
{
    int check_Result;

    RIO_TXRXM_PACKET_ARB_T* arbiter;

    if (handle == NULL || violation_Type == NULL)
        return RIO_ERROR; /*internal error*/

    arbiter = &(handle->inbound_Data.inbound_Arb);

    /*check if packet is greater than 276 bytes*/
    if (arbiter->cur_Pos > RIO_TXRXM_MAX_PROTOCOL_PACKET_LEN)
    {
        *error_Flag = RIO_TRUE;
        *violation_Type = PACKET_IS_LONGER_276;
        return RIO_OK;
    }
    /*check if packet is less than 6 bytes*/
    if (arbiter->cur_Pos < RIO_TXRXM_MIN_PROTOCOL_PACKET_LEN)
    {
        *error_Flag = RIO_TRUE;
        *violation_Type = INCORRECT_PACKET_HEADER;
        return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Ftype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = FTYPE_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Tt(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TT_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_CRC(arbiter,
        handle->initialization_Param_Set.is_Ext_Address_Valid,
        handle->initialization_Param_Set.is_Ext_Address_16, 
        expected_Crc)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = CRC_ERROR;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Header_Size(arbiter,
        handle->initialization_Param_Set.is_Ext_Address_Valid,
        handle->initialization_Param_Set.is_Ext_Address_16)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = INCORRECT_PACKET_HEADER;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Payload_Align(arbiter,
        handle->initialization_Param_Set.is_Ext_Address_Valid,
        handle->initialization_Param_Set.is_Ext_Address_16)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = PAYLOAD_NOT_DW_ALIGNED;
            return RIO_OK;
    }
    
    

    
    /*check physical reserved field value*/
    if ((check_Result = RIO_TxRxM_Check_Reserved_Fields(arbiter,
        handle->initialization_Param_Set.is_Ext_Address_Valid,
        handle->initialization_Param_Set.is_Ext_Address_16,
        RIO_FALSE)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = NON_ZERO_RESERVED_PACKET_BITS_VALUE;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Ttype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TTYPE_RESERVED;
            return RIO_OK;
    }

    *error_Flag = RIO_FALSE;
    return RIO_OK;
}


/***************************************************************************
 * Function : accept_Violated_Packet
 *
 * Description: form packet array and invoke external callback
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void accept_Violated_Packet(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    RIO_PACKET_VIOLATION_T violation_Type, 
    unsigned short expected_Crc)
{
    /*structures to hold passing packets*/
    RIO_UCHAR_T packet_Buffer[RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN];
    unsigned int packet_Size, i;

    if (handle == NULL)
        return;

    /*try to pass packet*/
    if (handle->ext_Interfaces.rio_TxRxM_Violent_Packet_Received != NULL)
    {
        /*fill passing data*/
        packet_Size = handle->inbound_Data.inbound_Arb.cur_Pos;
        for (i = 0; i < packet_Size; i++)
            packet_Buffer[i] = handle->inbound_Data.inbound_Arb.packet_Stream [i];

        /*invoke callback*/
        handle->ext_Interfaces.rio_TxRxM_Violent_Packet_Received(
            handle->ext_Interfaces.txrxm_Interface_Context,
            packet_Buffer, packet_Size, violation_Type, expected_Crc);

    }

    /*init arbiter */
    init_Arbiter(handle);
}

/***************************************************************************
 * Function : parse_Packet
 *
 * Description: try to parse received packet and invoke external callbacks
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void parse_Packet(RIO_TXRXM_SERIAL_DS_T *handle)
{
    unsigned int ftype, ttype, prio;
    RIO_UCHAR_T          ack_ID;

    if (handle == NULL)
        return;

    ftype = get_Ftype(&(handle->inbound_Data.inbound_Arb));
    ttype = get_Ttype(&(handle->inbound_Data.inbound_Arb));
    prio = get_Prio(&(handle->inbound_Data.inbound_Arb));

    ack_ID = 0;


    if (RIO_TxRxM_Is_Req(ftype, ttype) == RIO_TRUE)
    {
        if (handle->ext_Interfaces.rio_TxRxM_Request_Received != NULL)
        {
            /*create new packet structure*/
            RIO_REMOTE_REQUEST_T req_Struct;
            RIO_REQ_HEADER_T     header;
            RIO_DW_T             payload[RIO_TXRXM_MAX_PAYLOAD_SIZE];
            RIO_TR_INFO_T        transport_Info;
            RIO_UCHAR_T          hop_Count;

            /*set initial values*/
            hop_Count = 0;
            transport_Info = 0;

            req_Struct.header = &header;
            req_Struct.data = payload;
            /*load request from stream*/
            in_Load_From_Stream(handle, &req_Struct, &ack_ID, &transport_Info, &hop_Count);
            /*invoke external callback*/
            handle->ext_Interfaces.rio_TxRxM_Request_Received(handle->ext_Interfaces.txrxm_Interface_Context,
                &req_Struct, ack_ID, transport_Info, hop_Count);
        }
    }
    else
    {
        if (handle->ext_Interfaces.rio_TxRxM_Response_Received != NULL)
        {
            /* here structures to pass to the environment */
            RIO_RESPONSE_T        ext_Resp;
            RIO_DW_T              payload[RIO_TXRXM_MAX_PAYLOAD_SIZE];
            RIO_UCHAR_T           hop_Count;

            /*set initial values*/
            hop_Count = 0;

            ext_Resp.data = payload;
            /*form response structure*/
            RIO_TxRxM_In_Form_Resp(&(handle->inbound_Data.inbound_Arb),
                &ext_Resp, &ack_ID, &hop_Count, RIO_FALSE);
            /*invoke external callback*/
            handle->ext_Interfaces.rio_TxRxM_Response_Received(
                handle->ext_Interfaces.txrxm_Interface_Context,
                &ext_Resp, ack_ID, hop_Count);
        }
    }
    /*init arbiter*/
    init_Arbiter(handle);
    /*store the ackid*/
    handle->inbound_Data.in_Ack_Id = (ack_ID + 1) & RIO_STXRXM_ACK_ID_MASK;
}


/***************************************************************************
 * Function : in_Load_From_Stream
 *
 * Description: load inbound buffer entry from the packet stream
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void in_Load_From_Stream(
    RIO_TXRXM_SERIAL_DS_T *handle,
    RIO_REMOTE_REQUEST_T *req_Struct,
    RIO_UCHAR_T          *ack_ID,
    RIO_TR_INFO_T        *transport_Info,
    RIO_UCHAR_T          *hop_Count)
{
    if (handle == NULL ||
        req_Struct == NULL ||
        ack_ID == NULL ||
        transport_Info == NULL ||
        hop_Count == NULL || 
        req_Struct->header == NULL)
        return;

    /* locate entry */
     /* form entry fields */
    /*the ackId contains in the first 5 bits*/
    *ack_ID = handle->inbound_Data.inbound_Arb.packet_Stream[0] >> RIO_STXRXM_ACK_ID_SHIFT; 
    req_Struct->packet_Type = req_Struct->header->format_Type = 
        RIO_TxRxM_Get_Ftype(&(handle->inbound_Data.inbound_Arb));
    req_Struct->header->prio = RIO_TxRxM_Get_Prio(&(handle->inbound_Data.inbound_Arb));
    req_Struct->header->ttype = RIO_TxRxM_Get_Ttype(&(handle->inbound_Data.inbound_Arb));
    req_Struct->transp_Type = RIO_TxRxM_Get_Tt(&(handle->inbound_Data.inbound_Arb));
    *transport_Info = RIO_TxRxM_Get_Transp_Info(&(handle->inbound_Data.inbound_Arb));


    /* parse the stream to the structure */
    switch(req_Struct->packet_Type)
    {
        case RIO_MAINTENANCE:
            RIO_TxRxM_Prod_Req_8(&(handle->inbound_Data.inbound_Arb), 
                req_Struct, hop_Count);
            break;

        case RIO_DOORBELL:
            RIO_TxRxM_Prod_Req_10(&(handle->inbound_Data.inbound_Arb), req_Struct);
            break;

        case RIO_MESSAGE:
            RIO_TxRxM_Prod_Req_11(&(handle->inbound_Data.inbound_Arb), req_Struct);
            break;

        case RIO_NONINTERV_REQUEST:
            RIO_TxRxM_Prod_Req_2(&(handle->inbound_Data.inbound_Arb),
                req_Struct,
                handle->initialization_Param_Set.is_Ext_Address_Valid,
                handle->initialization_Param_Set.is_Ext_Address_16);
            break;

        case RIO_WRITE:
            RIO_TxRxM_Prod_Req_5(&(handle->inbound_Data.inbound_Arb),
                req_Struct,
                handle->initialization_Param_Set.is_Ext_Address_Valid,
                handle->initialization_Param_Set.is_Ext_Address_16);
            break;

        case RIO_STREAM_WRITE:
            RIO_TxRxM_Prod_Req_6(&(handle->inbound_Data.inbound_Arb),
                req_Struct,
                handle->initialization_Param_Set.is_Ext_Address_Valid,
                handle->initialization_Param_Set.is_Ext_Address_16);
            break;

        case RIO_INTERV_REQUEST:
            prod_Req_1(&(handle->inbound_Data.inbound_Arb),
                req_Struct,
                handle->initialization_Param_Set.is_Ext_Address_Valid,
                handle->initialization_Param_Set.is_Ext_Address_16);
            break;

    }

    return;    
}


