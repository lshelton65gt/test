/******************************************************************************
*
*       Copyright Motorola, Inc. 2002-2004
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/rio_txrxm_serial_tx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $Author: knutson $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model transmitter source
*
* Notes:        
*
******************************************************************************/
#include <stdlib.h>
#ifdef _DEBUG
#include <stdio.h>
#endif

/*#define Q3_LIST_CHECK*/

#include "rio_txrxm_serial_ds.h"
#include "rio_txrxm_serial_tx.h"

/*internal error handler*/
static void tx_STxRx_Internal_Error(void);
/*Adds element to a queue*/
static void out_Queue1_Add(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    unsigned int buffer_Index, 
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type);
static void out_Add_Symbol_To_Queue(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    unsigned int buffer_Index, 
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type);
/*perform new packets starting*/
static int start_New_Packet(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    RIO_PCS_PMA_GRANULE_T *granule);
/*returns symbol for transmitting*/
static int get_Symbol(RIO_TXRXM_SERIAL_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule, RIO_BOOL_T is_HP);
/*sends packet part*/
static int send_Packet_Part(RIO_TXRXM_SERIAL_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule);
/*loads packet arbiter*/
static void load_Packet_Arbiter(RIO_TXRXM_SERIAL_DS_T * handle);
/*remove element from queue*/
static void out_Queue1_Del(RIO_TXRXM_SERIAL_DS_T * handle);
/*convert packet struct to stream*/
void TxRxM_Out_Load_Packet_From_Struct(
    RIO_TXRXM_OUT_PACKET_STRUCT_T *packet_Struct, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

/*load packet from stream*/
void TxRxM_Out_Load_Packet_From_Stream(
    RIO_TXRXM_OUT_PACKET_STREAM_T *packet_Stream, 
    RIO_TXRXM_PACKET_ARB_T        *arbiter
    );

static void queue_Add(
    RIO_TXRXM_SERIAL_OUT_QUEUE_T * queue,
    unsigned int element_Place_In_Buffer,
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type
    );
static void queue_Dell(RIO_TXRXM_SERIAL_OUT_QUEUE_T * queue);

static void machine_Managment(RIO_TXRXM_SERIAL_DS_T * handle);
static void pop_Item(
    RIO_TXRXM_SERIAL_DS_T * handle, 
    RIO_TXRXM_SERIAL_ELEMENT_T element);

static void move_Elements_From_Queue1_To_Queue2(RIO_TXRXM_SERIAL_DS_T * handle);
/*queue2 routines*/
static int get_Char_Column(
    RIO_TXRXM_SERIAL_DS_T * handle,         
    RIO_PCS_PMA_CHARACTER_T	  *character);        /* pointer to character data structure */

static void load_Char_Column(RIO_TXRXM_SERIAL_DS_T * handle);
static void load_Queue2_Items_1x(RIO_TXRXM_SERIAL_DS_T * handle);
static void load_Queue2_Items_4x(
	RIO_TXRXM_SERIAL_DS_T * handle, 
	RIO_BOOL_T is_First);

static RIO_BOOL_T get_New_Queue2_Item (
    RIO_TXRXM_SERIAL_DS_T * handle, 
    RIO_TXRXM_SERIAL_ELEMENT_T * element_Type);

static void move_Elements_From_Queue2_To_Queue3(RIO_TXRXM_SERIAL_DS_T * handle);
/*queue3 routines*/
static int get_CG_Column(
    RIO_TXRXM_SERIAL_DS_T * handle,         
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup        /* pointer to codegroup data structure */
);

static void load_CG_Column(RIO_TXRXM_SERIAL_DS_T * handle);
static void load_Queue3_Items_1x(
	RIO_TXRXM_SERIAL_DS_T * handle, 
	RIO_BOOL_T is_First);
static void load_Queue3_Items_4x(
	RIO_TXRXM_SERIAL_DS_T * handle,
	RIO_BOOL_T is_First);

static RIO_BOOL_T get_New_Queue3_Item (
    RIO_TXRXM_SERIAL_DS_T * handle, 
    RIO_TXRXM_SERIAL_ELEMENT_T * element_Type);

static unsigned int get_CG_From_List(RIO_TXRXM_SERIAL_ARB_T * arb, int lane);
/***************************************************************************
 * Function : BUFS
 *
 * Description: returns buffers structure
 *
 * Returns: none
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define BUFS(handle) ((handle)->outbound_Data.out_Bufs)
/***************************************************************************
 * Function : ARB_QUEUE1
 *
 * Description: Repersents queue arbiter
 *
 * Returns: queue arbiter
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define ARB_QUEUE1(handle) ((handle)->outbound_Data.queue1.outbound_Packet_Arb)
/***************************************************************************
 * Function : ARB_QUEUE2
 *
 * Description: Repersents queue2 arbiter
 *
 * Returns: queue2 arbiter
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define ARB_QUEUE2(handle) ((handle)->outbound_Data.queue2.arbiter)
/***************************************************************************
 * Function : ARB_QUEUE3
 *
 * Description: Repersents queue3 arbiter
 *
 * Returns: queue3 arbiter
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define ARB_QUEUE3(handle) ((handle)->outbound_Data.queue3.arbiter)
/***************************************************************************
 * Function : IS_FORCE_1X
 *
 * Description: Returns parameter of lane mode
 *
 * Returns: parameter pf lane mode
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define IS_FORCE_1X(handle) ((handle)->regs.is_Port_4x == RIO_FALSE)
/***************************************************************************
 * Function : Load_Buffer_Entry
 *
 * Description: Loads new entry to a specified buffer
 *
 * Returns: Result of loading
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define Load_Buffer_Entry(handle, buf_Name, element_Name, max_Buf_Size, update_Sending_Queue_Routine_Name) \
{ \
    unsigned int buf_Top; \
\
    if ((handle)->outbound_Data.out_Bufs.buf_Name.buffer_Full == RIO_TRUE) \
        return RIO_RETRY; \
\
    buf_Top = (handle)->outbound_Data.out_Bufs.buf_Name.buffer_Top; \
\
    /* load new entry to the buffer */ \
    (handle)->outbound_Data.out_Bufs.buf_Name.buffer[buf_Top] = \
        (handle)->outbound_Data.out_Bufs.buf_Name.temp; \
\
    /* update sending queue */ \
    update_Sending_Queue_Routine_Name((handle), buf_Top, element_Name); \
\
    (handle)->outbound_Data.out_Bufs.buf_Name.buffer_Top = \
        (buf_Top + 1) % (max_Buf_Size); \
\
    if ((handle)->outbound_Data.out_Bufs.buf_Name.buffer_Top == \
        (handle)->outbound_Data.out_Bufs.buf_Name.buffer_Head) \
        (handle)->outbound_Data.out_Bufs.buf_Name.buffer_Full = RIO_TRUE; \
\
}

/***************************************************************************
 * Function : Print_Buf_Size
 *
 * Description: prints buffer size
 *
 * Returns: npne
 *
 * Notes: This is a macro
 *
 **************************************************************************/
#define Print_Buf_Size(handle, buf_Var, buf_Size)\
{\
    printf("is_Full %i; top %i; head %i; size %i; " #buf_Var ":   %i,\n", \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Full, \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Top, \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Head, \
        handle->inst_Param_Set.buf_Size, \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Top - \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Head >= 0 ? \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Top - \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Head : \
        handle->inst_Param_Set.buf_Size - \
        abs(handle->outbound_Data.out_Bufs.buf_Var.buffer_Top - \
        handle->outbound_Data.out_Bufs.buf_Var.buffer_Head) \
        );\
}


/***************************************************************************
 * Function : tx_STxRx_Internal_Error
 *
 * Description: Internal errors handler 
 *
 * Returns: none
 *
 * Notes: Is used only for debug purpose
 *
 **************************************************************************/
static void tx_STxRx_Internal_Error(void)
{
#ifdef _DEBUG
    printf ("TxRx Model internal error \n");
#endif
}

/*routines for work with the list*/
/***************************************************************************
 * Function : list_Del
 *
 * Description: Delete the list
 *
 * Returns: none
 *
 * Notes: noone
 *
 **************************************************************************/
static void list_Del(RIO_TXRXM_LIST_ITEM_T  * list)
{
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/
    it = list;
    while(it != NULL)
    {
        list = list->next;
        if (it->item != NULL) free(it->item);
        free (it);

        it = list;
    }
}

/***************************************************************************
 * Function : list_Init
 *
 * Description: Initializes the list
 *
 * Returns: none
 *
 * Notes: noone
 *
 **************************************************************************/
static void  list_Init(RIO_TXRXM_LIST_ITEM_T  ** plist)
{
    RIO_TXRXM_LIST_ITEM_T * list;
    list = (RIO_TXRXM_LIST_ITEM_T*)malloc(sizeof(RIO_TXRXM_LIST_ITEM_T));
    list->next = NULL;
    list->item = NULL;
    *plist = list;
}
/***************************************************************************
 * Function : list_Del_Item
 *
 * Description: Deletes list item which are next for the passed as prev item
 *
 * Returns: none
 *
 * Notes: noone
 *
 **************************************************************************/
static void list_Del_Item(RIO_TXRXM_LIST_ITEM_T * prev)
{
    RIO_TXRXM_LIST_ITEM_T * for_Del; /*pointer to the item which is necessary to delete*/
    if (prev == NULL) return;
    if (prev->next == NULL) return;
    for_Del = prev->next;
    prev->next = for_Del->next;
    if (for_Del->item != NULL)free(for_Del->item);
    free(for_Del);
}

/***************************************************************************
 * Function : list_Get_Item
 *
 * Description: Remove first item of the list and pass one to the requestor
 *
 * Returns: pointer to the first item
 *
 * Notes: none
 *
 **************************************************************************/
static void * list_Get_Item(RIO_TXRXM_LIST_ITEM_T * list)
{
    void * ret; /*return value*/
    ret = list->next->item;
    list->next->item = NULL;
    list_Del_Item(list);
    return ret;
}

/***************************************************************************
 * Function : list_Add
 *
 * Description: Adds item to the list
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void list_Add(RIO_TXRXM_LIST_ITEM_T ** last, void * new_Item)
{
    RIO_TXRXM_LIST_ITEM_T * new_List_Item; /*new item*/
    new_List_Item = (RIO_TXRXM_LIST_ITEM_T *)malloc(sizeof (RIO_TXRXM_LIST_ITEM_T));
    new_List_Item->item = new_Item;
    new_List_Item->next = NULL;
    (*last)->next = new_List_Item;
    *last = new_List_Item;
}


/***************************************************************************
 * Function : list_Add_2
 *
 * Description: Adds item to the list
 *
 * Returns: none
 *
 * Notes: Searchs from the start of list and 
 *
 **************************************************************************/
static void list_Add_2(
    RIO_TXRXM_LIST_ITEM_T * list, 
    RIO_TXRXM_LIST_ITEM_T ** last, 
    void * new_Item)
{
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/
    /*searchs for the end of list*/
    it = list;
    while (it->next != NULL)
        it = it->next;
    (*last) = it;
    list_Add(last, new_Item);
}

/***************************************************************************
 * Function : list_Tail
 *
 * Description: Returns pointer to the last element of the list
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_TXRXM_LIST_ITEM_T * list_Tail(RIO_TXRXM_LIST_ITEM_T * list)
{
    RIO_TXRXM_LIST_ITEM_T * it;  /*iterator*/

    it = list;

    while (it->next != NULL) it = it->next;
    return it;
}

/***************************************************************************
 * Function : list_Check
 *
 * Description: Checks that head and tail of list are correct
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T list_Check(RIO_TXRXM_LIST_ITEM_T * list, RIO_TXRXM_LIST_ITEM_T * last)
{
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/

    /*if (list == last && list->next == NULL) return RIO_TRUE;*/

    it = list;
    while (it->next != NULL)
        it = it->next;
    if (it != last) return RIO_TRUE;
    else return RIO_FALSE;
}


/***************************************************************************
 * Function : list_Len
 *
 * Description: Calculates lenght of the list
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static unsigned int list_Len(RIO_TXRXM_LIST_ITEM_T * list)
{
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/
    int len;

    /*if (list == last && list->next == NULL) return RIO_TRUE;*/

    it = list;
    len = 0;
    while (it->next != NULL)
    {
        it = it->next;
        len++;
    }
    return len;
}

/***************************************************************************
 * Function : list_Insert
 *
 * Description: inserts item to the list
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void list_Insert(RIO_TXRXM_LIST_ITEM_T * elem, void * new_Item)
{
    RIO_TXRXM_LIST_ITEM_T * new_List_Item;

    new_List_Item = (RIO_TXRXM_LIST_ITEM_T*)malloc(sizeof(RIO_TXRXM_LIST_ITEM_T));
    new_List_Item->item = new_Item;
    new_List_Item->next = elem->next;
    elem->next = new_List_Item;
}

/***************************************************************************
 * Function : list_Backup
 *
 * Description: Backups first part of the list
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void list_Backup(
    RIO_TXRXM_LIST_ITEM_T ** list, 
    RIO_TXRXM_LIST_ITEM_T ** backup, 
    unsigned int * list_Len)
{
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/

    *backup = *list;
    
    it = *list;
    *list_Len = 0;

    while (it->next != NULL)
        it = it->next;

    *list = it;
}

/***************************************************************************
 * Function : list_Restore
 *
 * Description: restores first part of the list
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void list_Restore(
    RIO_TXRXM_LIST_ITEM_T ** list, 
    RIO_TXRXM_LIST_ITEM_T * backup, 
    unsigned int * list_Len)
{
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/

    if (backup == NULL) return;

    *list = backup;

    it = (*list)->next;

    *list_Len = 0;
    while (it != NULL)
    {
        (*list_Len)++;
        it = it->next;
    }
}

/***************************************************************************
 * Function : convert_CG_To_Bits
 *
 * Description: converts integer to a bit array
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void convert_CG_To_Bits(
    unsigned int data, 
    RIO_SIGNAL_T cg[RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP])
{
    int i;
    if (cg == NULL) return;
    for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
        cg[i] = ( (data >> (RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP - i - 1)) & 1 ) == 1 ?
            RIO_SIGNAL_1 : RIO_SIGNAL_0;

}

/***************************************************************************
 * Function : convert_Bits_To_CG
 *
 * Description: converts bit array to a integer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void convert_Bits_To_CG(
    RIO_SIGNAL_T cg[RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP],
    unsigned int * data )
{
    int i;
    if (cg == NULL) return;
    *data = 0;
    for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
        *data |= (cg[i] << (RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP - i - 1));
}


/***************************************************************************
 * Function : init_Queue
 *
 * Description: intializes queue
 *
 * Returns: result of initialization
 *
 * Notes: none
 *
 **************************************************************************/
static int init_Queue(RIO_TXRXM_SERIAL_OUT_QUEUE_T * queue)
{
    queue->queue_Head = 0;
    queue->queue_Top = 0;
    queue->queue_Empty = RIO_TRUE;
    if (queue->queue_Buffer == NULL) return RIO_ERROR;
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Tx_Data_Init
 *
 * Description: Initialize data structures which are used in the tx part of 
 *              the model
 *
 * Returns: result of initialization
 *
 * Notes: This routine is clled from the other TxRx routine so assumed that
 *        instance pointer check is unnecessary
 *
 **************************************************************************/
int RIO_STxRxM_Tx_Data_Init(RIO_TXRXM_SERIAL_DS_T *instance)
{   
    int res; /*result of execution*/
    int i;


    Initialize_Buffer(instance->outbound_Data.out_Bufs.packet_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.symbol_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.hp_Symbol_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.char_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.char_Column_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.single_Char_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.cg_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.cg_Column_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.single_Bit_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.comp_Seq_Req_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.machine_Managment_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.pop_Byte_Buf)
    Initialize_Buffer(instance->outbound_Data.out_Bufs.pop_Bit_Buf)

    /*init high prio queue*/
    res = init_Queue(&(instance->outbound_Data.queue_High_Prio_Symbols));
    CHECK_RESULT;
    /*init queue1*/
    res = init_Queue(&(instance->outbound_Data.queue1.out_Queue));
    CHECK_RESULT;
    instance->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
    instance->outbound_Data.queue1.symbol_Is_Sent = RIO_FALSE;
    instance->outbound_Data.queue1.out_Ack_Id = 0;
    ARB_QUEUE1(instance).packet_Len = 0;
    ARB_QUEUE1(instance).cur_Pos = 0;
    /*init queue2*/
    res = init_Queue(&(instance->outbound_Data.queue2.out_Queue));
    CHECK_RESULT;

    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
    {
        list_Init(&(instance->outbound_Data.queue2.arbiter.list[i]));
        ARB_QUEUE2(instance).len[i] = 0;
        ARB_QUEUE2(instance).last[i] = ARB_QUEUE2(instance).list[i];
    }
    ARB_QUEUE2(instance).curr = 0;

    /*init queue3*/
    res = init_Queue(&(instance->outbound_Data.queue3.out_Queue));
    CHECK_RESULT;

    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
    {
        list_Init(&(instance->outbound_Data.queue3.arbiter.list[i]));
        ARB_QUEUE3(instance).len[i] = 0;
        ARB_QUEUE3(instance).last[i] = ARB_QUEUE3(instance).list[i];
    }
    ARB_QUEUE3(instance).curr = RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1;

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Tx_Data_Delete
 *
 * Description: Frees all allocated data
 *
 * Returns: result of initialization
 *
 * Notes: This routine is clled from the other TxRx routine so assumed that
 *        instance pointer check is unnecessary
 *
 **************************************************************************/
int RIO_STxRxM_Tx_Data_Delete(RIO_TXRXM_SERIAL_DS_T *instance)
{
    int i;
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        list_Del(instance->outbound_Data.queue2.arbiter.list[i]);
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        list_Del(instance->outbound_Data.queue3.arbiter.list[i]);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Tx_Data_Delete
 *
 * Description: Resets all allocated data
 *
 * Returns: result of initialization
 *
 * Notes: This routine is clled from the other TxRx routine so assumed that
 *        instance pointer check is unnecessary
 *
 **************************************************************************/
int RIO_STxRxM_Tx_Data_Reset(RIO_TXRXM_SERIAL_DS_T *instance)
{
    int i;
    /*arbiters reset*/
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
    {
        list_Del(instance->outbound_Data.queue2.arbiter.list[i]);
        ARB_QUEUE2(instance).last[i] = NULL;
        ARB_QUEUE2(instance).len[i] = 0;
        ARB_QUEUE2(instance).list[i] = NULL;
    }
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
    {
        list_Del(instance->outbound_Data.queue3.arbiter.list[i]);
        ARB_QUEUE3(instance).last[i] = NULL;
        ARB_QUEUE3(instance).len[i] = 0;
        ARB_QUEUE3(instance).list[i] = NULL;
    }
    return RIO_OK;
}



/***************************************************************************
 * Function : queue_Add
 *
 * Description: Adds new element to the specefied queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void queue_Add(
    RIO_TXRXM_SERIAL_OUT_QUEUE_T * queue,
    unsigned int element_Place_In_Buffer,
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type
    )
{
    queue->queue_Buffer[queue->queue_Top].element_Place_In_Buffer = 
        element_Place_In_Buffer;
        
    queue->queue_Buffer[queue->queue_Top].element_Type = element_Type;

    if (++(queue->queue_Top) >= queue->queue_Max_Size)
            queue->queue_Top -= queue->queue_Max_Size;

    if (queue->queue_Empty == RIO_TRUE)
        queue->queue_Empty = RIO_FALSE;
    
}

/***************************************************************************
 * Function : queue_Dell
 *
 * Description: deletes element from the specefied queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void queue_Dell(RIO_TXRXM_SERIAL_OUT_QUEUE_T * queue)
{
    if (++(queue->queue_Head) >= queue->queue_Max_Size)
        queue->queue_Head -= queue->queue_Max_Size;


    if (queue->queue_Head == queue->queue_Top) queue->queue_Empty = RIO_TRUE;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Packet_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Packet_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, packet_Buf, RIO_STXRXM_ELEMENT_PACKET, 
        instance->inst_Param_Set.packets_Buffer_Size, out_Queue1_Add)

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Symbol_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Symbol_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    if (RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(instance).granule_Num == (unsigned int)-1)
        Load_Buffer_Entry(instance, hp_Symbol_Buf, RIO_STXRXM_ELEMENT_HP_SYMBOL, 
            instance->inst_Param_Set.symbols_Buffer_Size, out_Add_Symbol_To_Queue)
    else
        Load_Buffer_Entry(instance, symbol_Buf, RIO_STXRXM_ELEMENT_SYMBOL, 
            instance->inst_Param_Set.symbols_Buffer_Size, out_Add_Symbol_To_Queue)


    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Char_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Char_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, char_Buf, RIO_STXRXM_ELEMENT_CHAR, 
        instance->inst_Param_Set.character_Buffer_Size, out_Queue1_Add)
    
    /*if queue is empty then move this request to the necessary queue*/
    move_Elements_From_Queue1_To_Queue2(instance);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Char_Column_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Char_Column_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, char_Column_Buf, RIO_STXRXM_ELEMENT_CHAR_COLUMN, 
        instance->inst_Param_Set.character_Column_Buffer_Size, out_Queue1_Add)

    /*if queue is empty then move this request to the necessary queue*/
    move_Elements_From_Queue1_To_Queue2(instance);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Single_Char_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Single_Char_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, single_Char_Buf, RIO_STXRXM_ELEMENT_SINGLE_CHAR, 
        instance->inst_Param_Set.single_Character_Buffer_Size, out_Queue1_Add)

    /*if queue is empty then move this request to the necessary queue*/
    move_Elements_From_Queue1_To_Queue2(instance);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Cg_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Cg_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, cg_Buf, RIO_STXRXM_ELEMENT_CG, 
        instance->inst_Param_Set.code_Group_Buffer_Size, out_Queue1_Add)

    /*if queue is empty then move this request to the necessary queue*/
    move_Elements_From_Queue1_To_Queue2(instance);
    move_Elements_From_Queue2_To_Queue3(instance);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Cg_Column_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Cg_Column_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, cg_Column_Buf, RIO_STXRXM_ELEMENT_CG_COLUMN, 
        instance->inst_Param_Set.code_Group_Column_Buffer_Size, out_Queue1_Add)

    /*if queue is empty then move this request to the necessary queue*/
    move_Elements_From_Queue1_To_Queue2(instance);
    move_Elements_From_Queue2_To_Queue3(instance);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Single_Bit_Entry 
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Single_Bit_Entry (RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, single_Bit_Buf, RIO_STXRXM_ELEMENT_SINGLE_BIT, 
        instance->inst_Param_Set.single_Bit_Buffer_Size, out_Queue1_Add)

    /*if queue is empty then move this request to the necessary queue*/
    move_Elements_From_Queue1_To_Queue2(instance);
    move_Elements_From_Queue2_To_Queue3(instance);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Comp_Seq_Entry 
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Comp_Seq_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, comp_Seq_Req_Buf, RIO_STXRXM_ELEMENT_COMP_SEQ, 
        instance->inst_Param_Set.comp_Seq_Request_Buffer_Size, out_Queue1_Add)

    /*if queue is empty then move this request to the necessary queue*/
    move_Elements_From_Queue1_To_Queue2(instance);
    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Machine_Managment_Entry 
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Machine_Managment_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, machine_Managment_Buf, RIO_STXRXM_ELEMENT_MACHINE_MGMNT, 
        instance->inst_Param_Set.management_Request_Buffer_Size, out_Queue1_Add)

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_STxRxM_Load_Pop_Bit_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Pop_Bit_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, pop_Bit_Buf, RIO_STXRXM_ELEMENT_POP_BIT_ITEM, 
        instance->inst_Param_Set.pop_Bit_Buffer_Size, out_Queue1_Add)

    return RIO_OK;
}
/***************************************************************************
 * Function : RIO_STxRxM_Load_Pop_Char_Entry
 *
 * Description: try load current request to outbound buffer
 *
 * Returns: OK - loaded, ERROR - not loaded, RETRY - buffer full
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Load_Pop_Char_Entry(RIO_TXRXM_SERIAL_DS_T *instance)
{
    Load_Buffer_Entry(instance, pop_Byte_Buf, RIO_STXRXM_ELEMENT_POP_CHAR_ITEM, 
        instance->inst_Param_Set.pop_Char_Buffer_Size, out_Queue1_Add)

    return RIO_OK;
}



/***************************************************************************
 * Function : out_Queue1_Add
 *
 * Description: add element to sending queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Queue1_Add(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    unsigned int buffer_Index, 
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type)
{
    queue_Add(&(handle->outbound_Data.queue1.out_Queue),
        buffer_Index, element_Type);
}

/***************************************************************************
 * Function : out_High_Prio_Queue_Add
 *
 * Description: add element to sending high priority queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Add_Symbol_To_Queue(
    RIO_TXRXM_SERIAL_DS_T *handle, 
    unsigned int buffer_Index, 
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type)
{
    if (element_Type == RIO_STXRXM_ELEMENT_HP_SYMBOL)
        queue_Add(&(handle->outbound_Data.queue_High_Prio_Symbols),
            buffer_Index, element_Type);
    else
        queue_Add(&(handle->outbound_Data.queue1.out_Queue),
            buffer_Index, element_Type);
}
/***************************************************************************
 * Function : move_Elements_From_Queue1_To_Queue2
 *
 * Description: Move elements which are need to be passed to the lower
 *              layer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void move_Elements_From_Queue1_To_Queue2(RIO_TXRXM_SERIAL_DS_T * handle)
{
    unsigned int queue_Head;
    RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T q_Entry;
    while (handle->outbound_Data.queue1.out_Queue.queue_Empty != RIO_TRUE)
    {
        queue_Head = handle->outbound_Data.queue1.out_Queue.queue_Head;
        if (handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head].element_Type != 
                RIO_STXRXM_ELEMENT_PACKET &&
            handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head].element_Type !=
                RIO_STXRXM_ELEMENT_SYMBOL &&
            handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head].element_Type !=
                RIO_STXRXM_ELEMENT_MACHINE_MGMNT &&
            handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head].element_Type !=
                RIO_STXRXM_ELEMENT_POP_CHAR_ITEM &&
            handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head].element_Type !=
                RIO_STXRXM_ELEMENT_POP_BIT_ITEM)
        {
            q_Entry = handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head];
            queue_Add(&(handle->outbound_Data.queue2.out_Queue), 
                q_Entry.element_Place_In_Buffer,
                q_Entry.element_Type);
            queue_Dell(&(handle->outbound_Data.queue1.out_Queue));
        }
        else break;
    }
}

/***************************************************************************
 * Function : move_Elements_From_Queue2_To_Queue3
 *
 * Description: Move elements which are need to be passed to the lower
 *              layer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void move_Elements_From_Queue2_To_Queue3(RIO_TXRXM_SERIAL_DS_T * handle)
{
    unsigned int queue_Head;
    RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T q_Entry;
    while (handle->outbound_Data.queue2.out_Queue.queue_Empty != RIO_TRUE)
    {
        queue_Head = handle->outbound_Data.queue2.out_Queue.queue_Head;
        if (handle->outbound_Data.queue2.out_Queue.queue_Buffer[queue_Head].element_Type != 
                RIO_STXRXM_ELEMENT_CHAR &&
            handle->outbound_Data.queue2.out_Queue.queue_Buffer[queue_Head].element_Type !=
                RIO_STXRXM_ELEMENT_COMP_SEQ &&
            handle->outbound_Data.queue2.out_Queue.queue_Buffer[queue_Head].element_Type !=
                RIO_STXRXM_ELEMENT_CHAR_COLUMN &&
            handle->outbound_Data.queue2.out_Queue.queue_Buffer[queue_Head].element_Type !=
                RIO_STXRXM_ELEMENT_SINGLE_CHAR )
        {
            q_Entry = handle->outbound_Data.queue2.out_Queue.queue_Buffer[queue_Head];
            queue_Add(&(handle->outbound_Data.queue3.out_Queue), 
                q_Entry.element_Place_In_Buffer,
                q_Entry.element_Type);
            queue_Dell(&(handle->outbound_Data.queue2.out_Queue));
        }
        else break;
    }
}

/***************************************************************************
 * Function : get_New_Element
 *
 * Description: check if it's time to get new element to send
 *
 * Returns: RIO_TRUE if new element is needed
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T get_New_Element(
    RIO_TXRXM_SERIAL_DS_T * handle, 
    RIO_TXRXM_SERIAL_ELEMENT_T * element_Type)
{
    /*help temporaries*/
    unsigned long queue_Head;
    unsigned int buf_Head;
    RIO_BOOL_T eop_Available = RIO_FALSE;

    move_Elements_From_Queue1_To_Queue2(handle); /*rmove all previous elements*/

    /*first check for high priority queue*/
    if (handle->outbound_Data.queue_High_Prio_Symbols.queue_Empty == RIO_FALSE)
    {
        queue_Head = handle->outbound_Data.queue_High_Prio_Symbols.queue_Head;

        *element_Type = 
            handle->outbound_Data.queue_High_Prio_Symbols.queue_Buffer[queue_Head].element_Type;

        buf_Head = BUFS(handle).hp_Symbol_Buf.buffer_Head;

        /*if packet is finished and symbol is free then it is necessary send
        eop first*/
        if (BUFS(handle).hp_Symbol_Buf.buffer[buf_Head].place == FREE && 
            handle->outbound_Data.queue1.packet_Is_Sending == RIO_FALSE)
        {
            /*check that next symbol is available - to insert after eop*/
            if (handle->outbound_Data.queue1.out_Queue.queue_Empty == RIO_FALSE)
            {
                RIO_TXRXM_SERIAL_ELEMENT_T et; /*element type*/
                unsigned long qh;
                qh = handle->outbound_Data.queue1.out_Queue.queue_Head;
                et = handle->outbound_Data.queue1.out_Queue.queue_Buffer[qh].element_Type;
                if (et == RIO_STXRXM_ELEMENT_SYMBOL) eop_Available = RIO_TRUE;
            }
        }
        /*if symbol is not free and packet is sending then necessary to wait*/
        if ( !(BUFS(handle).hp_Symbol_Buf.buffer[buf_Head].place == FREE &&
            handle->outbound_Data.queue1.packet_Is_Sending == RIO_TRUE)
            && eop_Available == RIO_FALSE)
        {
        /*it is necessary to notify about finishing packet to
        the lower layer for it can correctly invoke packet_finishing callback
        (mantis 71 implementation)*/
            if ( BUFS(handle).hp_Symbol_Buf.buffer[buf_Head].place == CANCELING &&
                handle->outbound_Data.queue1.packet_Is_Sending == RIO_TRUE &&
                ARB_QUEUE1(handle).last_Gr_Sent == RIO_FALSE)
            {
                handle->regs.packet_Finishing_Cnt = 1;
                ARB_QUEUE1(handle).last_Gr_Sent = RIO_TRUE;
            }

            return RIO_TRUE;
        }

    }
    /*if queue empty, need to get default granule*/
    if (handle->outbound_Data.queue1.out_Queue.queue_Empty == RIO_TRUE)
        return RIO_FALSE;

    queue_Head = handle->outbound_Data.queue1.out_Queue.queue_Head;

    *element_Type = 
        handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head].element_Type;

    /*if packet is not currently sending, first queue element is taken*/
    if (handle->outbound_Data.queue1.packet_Is_Sending == RIO_FALSE)
    {
        return RIO_TRUE;
    }

    if (*element_Type == RIO_STXRXM_ELEMENT_POP_CHAR_ITEM)
    {
        buf_Head = BUFS(handle).pop_Byte_Buf.buffer_Head;
        if ( (BUFS(handle).pop_Byte_Buf.buffer[buf_Head].place == EMBEDDED ||
            BUFS(handle).pop_Byte_Buf.buffer[buf_Head].place == CANCELING) && 
            ( (BUFS(handle).pop_Byte_Buf.buffer[buf_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD == ARB_QUEUE1(handle).cur_Pos))
            return RIO_TRUE;
        return RIO_FALSE;
        
    }
    else if (*element_Type == RIO_STXRXM_ELEMENT_POP_BIT_ITEM)
    {
        buf_Head = BUFS(handle).pop_Bit_Buf.buffer_Head;
        if ( (BUFS(handle).pop_Bit_Buf.buffer[buf_Head].place == EMBEDDED ||
            BUFS(handle).pop_Bit_Buf.buffer[buf_Head].place == CANCELING) && 
            ( (BUFS(handle).pop_Bit_Buf.buffer[buf_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD == ARB_QUEUE1(handle).cur_Pos))
            return RIO_TRUE;
        return RIO_FALSE;
    }
    else if (*element_Type == RIO_STXRXM_ELEMENT_MACHINE_MGMNT)
    {
        buf_Head = BUFS(handle).machine_Managment_Buf.buffer_Head;
        if ( (BUFS(handle).machine_Managment_Buf.buffer[buf_Head].place == EMBEDDED ||
            BUFS(handle).machine_Managment_Buf.buffer[buf_Head].place == CANCELING) && 
            ( (BUFS(handle).machine_Managment_Buf.buffer[buf_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD == ARB_QUEUE1(handle).cur_Pos))
            return RIO_TRUE;
        return RIO_FALSE;
    }
    /*if packet is sending, but Symbol is first in queue(can be embedded or cancel packet)*/
    else if (*element_Type == RIO_STXRXM_ELEMENT_SYMBOL)
    {
        buf_Head = BUFS(handle).symbol_Buf.buffer_Head;
        /*it is necessary to notify about finishing packet to
        the lower layer for it can correctly invoke packet_finishing callback
        (mantis 71 implementation)*/
        if ( BUFS(handle).symbol_Buf.buffer[buf_Head].place == CANCELING && 
            ( (BUFS(handle).symbol_Buf.buffer[buf_Head].granule_Num) *
                RIO_TXRXM_BYTE_CNT_IN_WORD == ARB_QUEUE1(handle).cur_Pos) &&
                ARB_QUEUE1(handle).last_Gr_Sent == RIO_FALSE)
        {
            handle->regs.packet_Finishing_Cnt = RIO_TXRXM_BYTE_CNT_IN_WORD;
            ARB_QUEUE1(handle).last_Gr_Sent = RIO_TRUE;
        }


        /*cur_Pos - in byte count, granule_Num - in granule count*/
        if ( (BUFS(handle).symbol_Buf.buffer[buf_Head].place == EMBEDDED ||
            BUFS(handle).symbol_Buf.buffer[buf_Head].place == CANCELING) && 
            ( (BUFS(handle).symbol_Buf.buffer[buf_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD == ARB_QUEUE1(handle).cur_Pos))
            return RIO_TRUE;
        return RIO_FALSE;
    }
    return RIO_FALSE;
}

/***************************************************************************
 * Function : RIO_STxRxM_Get_Branule_Connector
 *
 * Description: Pass control and granule value from DP to PM
 *
 * Returns: Error code
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_STxRxM_Get_Granule_Connector(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T	  *granule          /* pointer to granule data structure */
)
{
    int result;
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    if (handle == NULL) return RIO_ERROR;


    result = (handle->pm_Interfaces.get_Granule)(handle->pm, granule);


    /*In case of 1x mode and idle link the granule to send callback 
    should be invoked each 4 character so the following if implements
    counter update*/
    if (result == RIO_NO_DATA && IS_FORCE_1X(handle))
    {                            
        handle->outbound_Data.idle_Counter = (handle->outbound_Data.idle_Counter + 1) % RIO_PL_SERIAL_CNT_BYTE_IN_GRAN;
        if(handle->outbound_Data.idle_Counter) return result;
    }
    else 
        /* in case of non-idle character the counter should be reset
        for possibility to invoke granule clock callback for the
        first idle sequence character */
        handle->outbound_Data.idle_Counter = RIO_PL_SERIAL_CNT_BYTE_IN_GRAN - 1;

    /*invoke granule to send callback if necessary*/
    if (handle->ext_Interfaces.rio_TxRxM_Granule_To_Send != NULL)
    {
        RIO_PCS_PMA_GRANULE_T granule_Buf = *granule;
        if (result == RIO_NO_DATA)
            granule_Buf.is_Data = 3;

        handle->ext_Interfaces.rio_TxRxM_Granule_To_Send(
            handle->ext_Interfaces.hook_Context,
            (void*)(&granule_Buf));
    }

    return result;
}


/***************************************************************************
 * Function : RIO_STxRxM_Get_Granule_From_Queue1
 *
 * Description: Takes granule from the queue1 and return it to the requestor
 *
 * Returns: RIO_OK if all right RIO_NO_DATA if queue is empty
 *
 * Notes: This routine is used as callback for pm
 *
 **************************************************************************/
int RIO_STxRxM_Get_Granule_From_Queue1(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T	  *granule          /* pointer to granule data structure */
)
{
	int res; /*value of return code*/
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type;
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;

    if (handle_Context == NULL || granule == NULL) return RIO_ERROR;

    /*reset flag of symbol sending*/
    handle->outbound_Data.queue1.symbol_Is_Sent = RIO_FALSE;
    /*
     * we check all information source: all queues
     * if we are sending a packet ask for new portion
     */

    /*check if new element from queue is needed and get it*/
    if ( get_New_Element(handle, &element_Type) == RIO_TRUE)
        switch (element_Type)
        {
            case RIO_STXRXM_ELEMENT_PACKET:
				res = start_New_Packet(handle, granule);
				move_Elements_From_Queue1_To_Queue2(handle);
                return res;
            case RIO_STXRXM_ELEMENT_SYMBOL:
				res = get_Symbol(handle, granule, RIO_FALSE);
				move_Elements_From_Queue1_To_Queue2(handle);
                /*set flag for notification that symbol is passed to the 
                protocol manager*/
                handle->outbound_Data.queue1.symbol_Is_Sent = RIO_TRUE;
                return res;
            case RIO_STXRXM_ELEMENT_MACHINE_MGMNT:
                machine_Managment(handle);
                return RIO_STxRxM_Get_Granule_From_Queue1(handle, granule);
            case RIO_STXRXM_ELEMENT_POP_CHAR_ITEM:
                pop_Item(handle, RIO_STXRXM_ELEMENT_POP_CHAR_ITEM);
                return RIO_STxRxM_Get_Granule_From_Queue1(handle, granule);
            case RIO_STXRXM_ELEMENT_POP_BIT_ITEM:
                pop_Item(handle, RIO_STXRXM_ELEMENT_POP_BIT_ITEM);
                return RIO_STxRxM_Get_Granule_From_Queue1(handle, granule);
            case RIO_STXRXM_ELEMENT_HP_SYMBOL:
				res = get_Symbol(handle, granule, RIO_TRUE);
                /*set flag for notification that symbol is passed to the 
                protocol manager*/
                handle->outbound_Data.queue1.symbol_Is_Sent = RIO_TRUE;
                return res;
            default:
                tx_STxRx_Internal_Error();
            break;
        }

    if (handle->outbound_Data.queue1.packet_Is_Sending == RIO_TRUE)
    {
        send_Packet_Part(handle, granule);
        return RIO_OK;
    }

    /*In no data to send then callback returns no_data exit code*/
    handle->regs.is_PD = RIO_FALSE;
    return RIO_NO_DATA;
}


/***************************************************************************
 * Function : start_New_Packet
 *
 * Description: start new packet transmission
 *
 * Returns: Result of execution
 *
 * Notes: none
 *
 **************************************************************************/
static int start_New_Packet(RIO_TXRXM_SERIAL_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule)
{
    int i;

    /*load packet arbiter with new packet*/
    load_Packet_Arbiter(handle);

    /*update packet buffer params*/
    BUFS(handle).packet_Buf.buffer_Head = 
        (BUFS(handle).packet_Buf.buffer_Head + 1) %
        handle->inst_Param_Set.packets_Buffer_Size;
    BUFS(handle).packet_Buf.buffer_Full = RIO_FALSE;

    /*delete sending packet from queue*/
    out_Queue1_Del(handle);

    handle->outbound_Data.queue1.packet_Is_Sending = RIO_TRUE;
    /*set flag for SC/PD in the symbols*/
    /*handle->regs.is_PD = RIO_FALSE;*/
    
    /* prepare the first granule  -consist from 4 bytes with corresponding offsets from 0 to 3*/

    granule->is_Data = RIO_TRUE; /*the granule contains data*/
    for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        granule->granule_Date[i] = ARB_QUEUE1(handle).packet_Stream[i];

    /*initialize expected ackid*/
    handle->outbound_Data.queue1.out_Ack_Id = 1 + (ARB_QUEUE1(handle).packet_Stream[0] >> 3);
    handle->outbound_Data.queue1.out_Ack_Id &= RIO_STXRXM_ACK_ID_MASK; /*remove all unnecessary bits*/
    /* update the arbiter state*/
    ARB_QUEUE1(handle).cur_Pos = RIO_TXRXM_BYTE_CNT_IN_WORD;

    /*in case of 8-byte packet it is necessary to 
    initialize variables for set last packet flag 
    callback invocation from here because in the send packet part
    routine will be too late. This is Mantis 71 implementation*/
    if (ARB_QUEUE1(handle).packet_Len == RIO_TXRXM_BYTE_CNT_IN_WORD * 2 &&
        handle->regs.packet_Finishing_Cnt == 0 &&
        ARB_QUEUE1(handle).last_Gr_Sent == RIO_FALSE)
    {
        handle->regs.packet_Finishing_Cnt = RIO_TXRXM_BYTE_CNT_IN_WORD;
        ARB_QUEUE1(handle).last_Gr_Sent = RIO_TRUE;
    }

    /*in case of packet that is cancelled after first granule it is
    necessary to set last packt flag immediately*/
    if (handle->outbound_Data.queue1.out_Queue.queue_Empty != RIO_TRUE)
    {
        int queue_Head, buf_Head;
        queue_Head = handle->outbound_Data.queue1.out_Queue.queue_Head;
        if (handle->outbound_Data.queue1.out_Queue.queue_Buffer[queue_Head].element_Type == RIO_STXRXM_ELEMENT_SYMBOL)
        {

            buf_Head = BUFS(handle).symbol_Buf.buffer_Head;
            if ( BUFS(handle).symbol_Buf.buffer[buf_Head].place == CANCELING && 
                BUFS(handle).symbol_Buf.buffer[buf_Head].granule_Num == 0 &&
                ARB_QUEUE1(handle).last_Gr_Sent == RIO_FALSE)
            {
                    /*after writing of zero to the register via special
                    task the callback will be invoked immediately*/
                    handle->set_State(handle, handle /*caller inst*/,
                        RIO_PACKET_FINISHING_CNT,
                        0, 0 /*indicator's value and parameter*/);
                    ARB_QUEUE1(handle).last_Gr_Sent = RIO_TRUE;
            }
        }
    }
    


    return RIO_OK;
}

/***************************************************************************
 * Function : load_Packet_Arbiter
 *
 * Description: loads packet arbiter with packet stream
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_Packet_Arbiter(RIO_TXRXM_SERIAL_DS_T * handle)
{
    /*if packet was requested as struct*/
    if (BUFS(handle).packet_Buf.buffer[BUFS(handle).packet_Buf.buffer_Head].as_Struct == RIO_TRUE)
    {
        /*set the rsrvphy2 the same as rsrv1*/
        BUFS(handle).packet_Buf.buffer[BUFS(handle).packet_Buf.buffer_Head].packet_Struct.entry.rsrv_Phy_2 = 
            BUFS(handle).packet_Buf.buffer[BUFS(handle).packet_Buf.buffer_Head].packet_Struct.entry.rsrv_Phy_1;
            
        /*convert packet struct to stream*/
        TxRxM_Out_Load_Packet_From_Struct(
            &(BUFS(handle).packet_Buf.buffer[BUFS(handle).packet_Buf.buffer_Head].packet_Struct),
            &ARB_QUEUE1(handle));

    /*TxRxM_Out_Load_Packet_From_Struct routine configure first 6 bits as 
    described in the Parallel specification. The first 6 bits are mot protected
    by crc so it is possible to change ones here*/
    /*set first byte, common to all packets - hardcoded numbers are unique and shall be changed only here*/
        ARB_QUEUE1(handle).packet_Stream[0] = (RIO_UCHAR_T)(
            (BUFS(handle).packet_Buf.buffer[BUFS(handle).packet_Buf.buffer_Head].packet_Struct.entry.ack_ID\
                & RIO_STXRXM_ACK_ID_MASK) << 3);
        ARB_QUEUE1(handle).packet_Stream[0] |= (RIO_UCHAR_T)
            (BUFS(handle).packet_Buf.buffer[BUFS(handle).packet_Buf.buffer_Head].packet_Struct.entry.rsrv_Phy_1 & 0x7);
    }
    else
    {
        /*load packet stream*/
        TxRxM_Out_Load_Packet_From_Stream(
            &(BUFS(handle).packet_Buf.buffer[BUFS(handle).packet_Buf.buffer_Head].packet_Stream),
            &ARB_QUEUE1(handle));
    }

    /* modify arbiter*/
    ARB_QUEUE1(handle).cur_Pos = 0;
    ARB_QUEUE1(handle).last_Gr_Sent = RIO_FALSE;
    /*invoke packet to send callback*/
    if (handle->ext_Interfaces.rio_TxRxM_Packet_To_Send != NULL)
    {
        handle->ext_Interfaces.rio_TxRxM_Packet_To_Send(handle->ext_Interfaces.hook_Context,
            ARB_QUEUE1(handle).packet_Stream,
            ARB_QUEUE1(handle).packet_Len);
    }
}

/***************************************************************************
 * Function : out_Queue1_Del
 *
 * Description: delete sent element from sending queue
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void out_Queue1_Del(RIO_TXRXM_SERIAL_DS_T * handle)
{
    queue_Dell(&(handle->outbound_Data.queue1.out_Queue));
}


/***************************************************************************
 * Function : get_Symbol
 *
 * Description: get new symbol for sending
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int get_Symbol(RIO_TXRXM_SERIAL_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule, RIO_BOOL_T is_HP)
{
    unsigned int buf_Index;
    RIO_PLACE_IN_STREAM_T place;
    RIO_TXRXM_SERIAL_OUT_SYMBOL_BUFFER_T * cur_Buffer;

    cur_Buffer = is_HP == RIO_TRUE ? &(BUFS(handle).hp_Symbol_Buf) : &(BUFS(handle).symbol_Buf);

    buf_Index = cur_Buffer->buffer_Head;

    place = cur_Buffer->buffer[buf_Index].place;

    /*form symbol fields*/
    granule->is_Data = RIO_FALSE;
    granule->granule_Struct.cmd = cur_Buffer->buffer[buf_Index].symbol.cmd;
    granule->granule_Struct.crc = cur_Buffer->buffer[buf_Index].symbol.crc;
    granule->granule_Struct.is_Crc_Valid = cur_Buffer->buffer[buf_Index].symbol.is_Crc_Valid;
    granule->granule_Struct.parameter0 = cur_Buffer->buffer[buf_Index].symbol.parameter0;
    granule->granule_Struct.parameter1 = cur_Buffer->buffer[buf_Index].symbol.parameter1;
    granule->granule_Struct.stype0 = cur_Buffer->buffer[buf_Index].symbol.stype0;
    granule->granule_Struct.stype1 = cur_Buffer->buffer[buf_Index].symbol.stype1;
    
     /*update symbol buffer params*/
    if (++(cur_Buffer->buffer_Head) >= handle->inst_Param_Set.symbols_Buffer_Size)
        cur_Buffer->buffer_Head -= handle->inst_Param_Set.symbols_Buffer_Size;

    /*if (cur_Buffer->buffer_Head == cur_Buffer->buffer_Top)*/
        cur_Buffer->buffer_Full = RIO_FALSE;
    /*delete sending symbol from queue*/
    if (is_HP == RIO_FALSE)
        out_Queue1_Del(handle);
    else 
        queue_Dell(&(handle->outbound_Data.queue_High_Prio_Symbols));
    /*check that symbol is SOP/EOP
    in case of sop it is necessary to set is_PD to true
    in case of EOP it is necessary to set is_PD to false*/
    if (granule->is_Data == RIO_FALSE && 
        granule->granule_Struct.stype1 == STXRX_STYPE1_SOP)
        handle->regs.is_PD = RIO_TRUE;
    if (granule->is_Data == RIO_FALSE && 
        granule->granule_Struct.stype1 == STXRX_STYPE1_EOP)
        handle->regs.is_PD = RIO_FALSE;
    /*check other sc/pd*/
    if (place == CANCELING)
        handle->regs.is_PD = RIO_FALSE;
    if (place == CANCELING && 
        handle->outbound_Data.queue1.packet_Is_Sending == RIO_TRUE)
    {
        if (granule->is_Data == RIO_FALSE &&
            (granule->granule_Struct.stype1 == STXRX_STYPE1_SOP ||
            granule->granule_Struct.stype1 == STXRX_STYPE1_STOMP ||
            granule->granule_Struct.stype1 == STXRX_STYPE1_EOP ||
            granule->granule_Struct.stype1 == STXRX_STYPE1_RFR ||
            granule->granule_Struct.stype1 == STXRX_STYPE1_LRQ))
                handle->regs.is_PD = RIO_TRUE;
    }
    /*if symbol place is canceling, stop packet transmittion*/
    if (place == CANCELING)
    {
        handle->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
        /*reset arbiter*/
        ARB_QUEUE1(handle).packet_Len = 0;
        ARB_QUEUE1(handle).cur_Pos = 0;
    }
    
    return RIO_OK;
}

/***************************************************************************
 * Function : send_Packet_Part
 *
 * Description: obtain new data for transmitting packet
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int send_Packet_Part(RIO_TXRXM_SERIAL_DS_T *handle, RIO_PCS_PMA_GRANULE_T *granule)
{
    unsigned int i; /* loop counter */

    if (handle->outbound_Data.queue1.packet_Is_Sending == RIO_TRUE)
    {
        /* load new data to the granule and update position */
        granule->is_Data = RIO_TRUE;
        for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
            if ( ARB_QUEUE1(handle).cur_Pos + i >= ARB_QUEUE1(handle).packet_Len)
                granule->granule_Date[i] = 0;
            else
                granule->granule_Date[i] = 
                    ARB_QUEUE1(handle).packet_Stream[ARB_QUEUE1(handle).cur_Pos + i];

        ARB_QUEUE1(handle).cur_Pos += RIO_TXRXM_BYTE_CNT_IN_WORD;
        
        /* check if the last portion of packet is sending */
    
        if (ARB_QUEUE1(handle).cur_Pos >= ARB_QUEUE1(handle).packet_Len)
        {
            /*reset packet sending flag*/
            handle->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
            /*handle->regs.is_PD = RIO_FALSE;*/
            /* clear arbiter state */
            
            ARB_QUEUE1(handle).packet_Len = 0;
            ARB_QUEUE1(handle).cur_Pos = 0;
        }
    
        /*it is necessary to notify about finishing packet to
        the lower layer for it can correctly invoke packet_finishing callback
        (mantis 71 implementation)*/
        if (ARB_QUEUE1(handle).packet_Len > 0)
            if (ARB_QUEUE1(handle).cur_Pos >= ARB_QUEUE1(handle).packet_Len - RIO_TXRXM_BYTE_CNT_IN_WORD &&
                handle->regs.packet_Finishing_Cnt == 0 &&
                ARB_QUEUE1(handle).last_Gr_Sent == RIO_FALSE)
            {
                handle->regs.packet_Finishing_Cnt = RIO_TXRXM_BYTE_CNT_IN_WORD;
                ARB_QUEUE1(handle).last_Gr_Sent = RIO_TRUE;
            }
    
    
    }
    return RIO_OK;
}

/***************************************************************************
 * Function : machine_Managment
 *
 * Description: Enable/disable selected machine
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void machine_Managment(RIO_TXRXM_SERIAL_DS_T * handle)
{
    RIO_TXRX_SERIAL_MACHINE_MANAGMENT_ENTRY_T entry;
    entry = BUFS(handle).machine_Managment_Buf.buffer[BUFS(handle).machine_Managment_Buf.buffer_Head];
    Pop_Buffer_Item(BUFS(handle).machine_Managment_Buf, handle->inst_Param_Set.management_Request_Buffer_Size);

    /*Cancel packet if necessary*/
    if (entry.place == CANCELING)
    {
        handle->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
        handle->regs.is_PD = RIO_FALSE;
        /*reset arbiter*/
        ARB_QUEUE1(handle).packet_Len = 0;
        ARB_QUEUE1(handle).cur_Pos = 0;
    }
    /*Enable/Disable machines*/
    if (entry.machine_Type == RIO_PCS_PMA_MACHINE_INIT)
    {
        handle->pm_Interfaces.machine_Management(handle->pm, entry.set_On_Machine, 
            RIO_PCS_PMA_MACHINE_INIT);
    }
    if (entry.machine_Type == RIO_PCS_PMA_MACHINE_COMP_SEQ_GEN)
    {
        handle->pm_Interfaces.machine_Management(handle->pm, entry.set_On_Machine, 
            RIO_PCS_PMA_MACHINE_COMP_SEQ_GEN);
    }
    if (entry.machine_Type == RIO_PCS_PMA_MACHINE_STATUS_SYM_GEN)
    {
        handle->pm_Interfaces.machine_Management(handle->pm, entry.set_On_Machine, 
            RIO_PCS_PMA_MACHINE_STATUS_SYM_GEN);
    }
    /*delete sending symbol from queue*/
    out_Queue1_Del(handle);
}

/***************************************************************************
 * Function : pop_Item
 *
 * Description: Pop item from the specified lane
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static void pop_Item(
    RIO_TXRXM_SERIAL_DS_T * handle, 
    RIO_TXRXM_SERIAL_ELEMENT_T element)
{
    unsigned char lane;     /*number of selected lane*/

    if (element == RIO_STXRXM_ELEMENT_POP_CHAR_ITEM)
    {
        /*Cancel packet if necessary*/
        if (BUFS(handle).pop_Byte_Buf.buffer[BUFS(handle).pop_Byte_Buf.buffer_Head].place == CANCELING)
        {
            handle->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
            handle->regs.is_PD = RIO_FALSE;
            /*reset arbiter*/
            ARB_QUEUE1(handle).packet_Len = 0;
            ARB_QUEUE1(handle).cur_Pos = 0;
        }
        lane = BUFS(handle).pop_Byte_Buf.buffer[BUFS(handle).pop_Byte_Buf.buffer_Head].place;
        Pop_Buffer_Item(BUFS(handle).pop_Byte_Buf, handle->inst_Param_Set.pop_Char_Buffer_Size);
        (handle->dp_Interfaces.pop_Char_Buffer)(
            handle->dp, lane);
    }
    else
    {
        /*Cancel packet if necessary*/
        if (BUFS(handle).pop_Bit_Buf.buffer[BUFS(handle).pop_Bit_Buf.buffer_Head].place == CANCELING)
        {
            handle->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
            handle->regs.is_PD = RIO_FALSE;
            /*reset arbiter*/
            ARB_QUEUE1(handle).packet_Len = 0;
            ARB_QUEUE1(handle).cur_Pos = 0;
        }
        lane = BUFS(handle).pop_Bit_Buf.buffer[BUFS(handle).pop_Bit_Buf.buffer_Head].place;
        Pop_Buffer_Item(BUFS(handle).pop_Bit_Buf, handle->inst_Param_Set.pop_Bit_Buffer_Size);
        handle->inbound_Data.pop_Bit_Requests++;
        
    }
    /*delete sending symbol from queue*/
    out_Queue1_Del(handle);
}


/************************************************************************/
/*routines for the QUEUE2 implementation*/
/************************************************************************/


/***************************************************************************
 * Function : TxRxM_Transfer_Character_Column
 *
 * Description: Pass character column from pm to dp and insert requested 
 *              changes to the data flow
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int TxRxM_Transfer_Character_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
)
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    int i;
    if (handle_Context == NULL || character == NULL) return RIO_ERROR;


    /*check that it is nothing to send*/
    if (IS_FORCE_1X(handle))
    {
		load_Queue2_Items_1x(handle);
/*        if (ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0] < 1)
        {
            load_Char_Column(handle);
            load_Queue2_Items_1x(handle);
        }*/
    }
    else
    {       
        load_Queue2_Items_4x(handle, RIO_TRUE);
			/*check all lanes len*/
        if (ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0] < 1 ||
            ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_1] < 1 ||
            ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_2] < 1 ||
            ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_3] < 1)
        {
            /*remove reminder of the previous operation*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
                list_Backup(&(ARB_QUEUE2(handle).list[i]), 
                    &(ARB_QUEUE2(handle).backup[i]), 
                    &(ARB_QUEUE2(handle).len[i]));

            load_Char_Column(handle);
            load_Queue2_Items_4x(handle, RIO_FALSE);

            /*restore reminder at begin of list*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
                list_Restore(&(ARB_QUEUE2(handle).list[i]), 
                    ARB_QUEUE2(handle).backup[i], 
                    &(ARB_QUEUE2(handle).len[i]));
        }
    }
    /*pass data to the requestor*/
    if (IS_FORCE_1X(handle))
    {
        RIO_TXRXM_SERIAL_CHAR_T * ch; /*character value*/
        character->lane_Mask = 1;
        ch = (RIO_TXRXM_SERIAL_CHAR_T *)list_Get_Item(ARB_QUEUE2(handle).list[RIO_TXRXM_LANE_0]);
        ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]--;
        if (ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0] == 0)
            ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0] = ARB_QUEUE2(handle).list[RIO_TXRXM_LANE_0];


        character->character_Type[RIO_TXRXM_LANE_0] = ch->type;
        character->character_Data[RIO_TXRXM_LANE_0] = ch->data;
        free(ch);
    }
    else 
    {
        int i;
        RIO_TXRXM_SERIAL_CHAR_T * ch; /*character value*/
        character->lane_Mask = RIO_TXRXM_SERIAL_ALL_LANES_USED_MASK;
        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            ch = (RIO_TXRXM_SERIAL_CHAR_T *)list_Get_Item(ARB_QUEUE2(handle).list[i]);
            ARB_QUEUE2(handle).len[i]--;
            if (ARB_QUEUE2(handle).len[i] == 0)
                ARB_QUEUE2(handle).last[i] = ARB_QUEUE2(handle).list[i];

            character->character_Type[i] = ch->type;
            character->character_Data[i] = ch->data;
            free(ch);
        }
    }

    /*remove elements which are necessary place to the lower layer*/
    move_Elements_From_Queue2_To_Queue3(handle);

    return RIO_OK;
}

/***************************************************************************
 * Function : TxRxM_Get_Char_Column
 *
 * Description: Implementation of callback for the pm
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int TxRxM_Get_Char_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
)
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;

    if (handle_Context == NULL || character == NULL) return RIO_ERROR;
    
    if (handle->mach_State.align_Enabled != RIO_TRUE) return RIO_OK;

    return (handle->dp_Interfaces.get_Character_Column)(handle->dp, character);
}

/***************************************************************************
 * Function : get_Char_Column
 *
 * Description: Wrapper for PM's ftray routines. Need for possibility to 
 *              on/of State Machines invocation
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int get_Char_Column(
    RIO_TXRXM_SERIAL_DS_T * handle,         
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
)
{
    if (handle->mach_State.align_Enabled == RIO_FALSE)
        return (handle->dp_Interfaces.get_Character_Column)(handle->dp, character);
    return (handle->pm_Interfaces.get_Character_Column)(handle->pm, character);
}

/***************************************************************************
 * Function : load_Char_Column
 *
 * Description: Loads character column from the protocol manager or 
 *              data processor and pass ones to the intermediate buffer
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_Char_Column(RIO_TXRXM_SERIAL_DS_T * handle)
{
    RIO_PCS_PMA_CHARACTER_T character;  /*Parameter for get_Char_Columm invocation*/
    RIO_TXRXM_SERIAL_CHAR_T * ch;       /*item for the int. list*/
    int i;

    if (IS_FORCE_1X(handle))
    {
        get_Char_Column(handle, &character);
        ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
        ch->data = character.character_Data[RIO_TXRXM_LANE_0];
        ch->type = character.character_Type[RIO_TXRXM_LANE_0];
        list_Add(&(ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]), ch);
        ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]++;
        /*modify number of current send byte*/
        ARB_QUEUE2(handle).curr = handle->regs.curr_Byte_Tx;
        ARB_QUEUE3(handle).curr = handle->regs.curr_Byte_Tx;
    }
    else
    {
        get_Char_Column(handle, &character);
        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
            ch->data = character.character_Data[i];
            ch->type = character.character_Type[i];
            list_Add(&(ARB_QUEUE2(handle).last[i]), ch);
            ARB_QUEUE2(handle).len[i]++;
        }
    }
}

/***************************************************************************
 * Function : load_Queue2_Items_1x
 *
 * Description: loads all items from the queue, which are applicable for
 *              current granule
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_Queue2_Items_1x(RIO_TXRXM_SERIAL_DS_T * handle)
{
    RIO_TXRXM_SERIAL_CHAR_T * ch; /*temporary pointer to the list item*/
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type;
    unsigned int i;

    while (get_New_Queue2_Item(handle, &element_Type) == RIO_TRUE)
    {
        if (element_Type == RIO_STXRXM_ELEMENT_CHAR)
        {
            ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
            ch->data = 
                BUFS(handle).char_Buf.buffer[BUFS(handle).char_Buf.buffer_Head].\
                character.character_Data[RIO_TXRXM_LANE_0];
            ch->type = 
                BUFS(handle).char_Buf.buffer[BUFS(handle).char_Buf.buffer_Head].\
                character.character_Type[RIO_TXRXM_LANE_0];
            /*insert data to the list*/
            list_Add(&(ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]), ch);
            ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]++;
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.char_Buf, 
                handle->inst_Param_Set.character_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_CHAR_COLUMN)
        {
            
            /*insert data to the link*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
                ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
                ch->data = 
                    BUFS(handle).char_Column_Buf.buffer[BUFS(handle).char_Column_Buf.buffer_Head].\
                    character.character_Data[i];
                ch->type = 
                    BUFS(handle).char_Column_Buf.buffer[BUFS(handle).char_Column_Buf.buffer_Head].\
                    character.character_Type[i];
                
                list_Add(&(ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]), ch);
                ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]++;
            }
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.char_Column_Buf,
                handle->inst_Param_Set.character_Column_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_SINGLE_CHAR)
        {
            /*in case of 1x mode it is necessary to insert character to the end of 
            current list because the list contains only one character*/
            RIO_PLACE_IN_GRANULE_T place_In_Granule;

            ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
            ch->data = 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].\
                character;
            ch->type = 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].\
                char_Type;
            place_In_Granule = 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].\
                place_In_Granule;
            if (place_In_Granule == RIO_PLACE_IN_GRANULE_EMBEDDED )
            {
                /*search item after which necessary insert*/
                list_Add(&(ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]), ch);
                ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]++;
            }
            if (place_In_Granule == RIO_PLACE_IN_GRANULE_REPLACING &&
                ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0] > 0)
            {
                /*search item after which situated replaced item*/
                *((RIO_TXRXM_SERIAL_CHAR_T*)(ARB_QUEUE2(handle).list[RIO_TXRXM_LANE_0]->next->item)) =
                    *ch;
                free(ch);
            }
            if (place_In_Granule == RIO_PLACE_IN_GRANULE_DELETING &&
                ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0] > 0)
            {
                /*deletes the first character*/
                list_Del_Item(ARB_QUEUE2(handle).list[RIO_TXRXM_LANE_0]);
                ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]--;
                /*search end of the list*/
                ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0] = 
                    ARB_QUEUE2(handle).list[RIO_TXRXM_LANE_0];
                while (ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]->next != NULL)
                    ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0] = 
                        ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]->next;
                free(ch);
            }
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.single_Char_Buf,
                handle->inst_Param_Set.single_Character_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_COMP_SEQ)
        {
            /*In case of compensation sequence request necessary
            add 4 characters to the output*/
            ch = (RIO_TXRXM_SERIAL_CHAR_T*)malloc(sizeof (RIO_TXRXM_SERIAL_CHAR_T));
            ch->data = RIO_TXRXM_K_VAL;
            ch->type = RIO_K;
            list_Add(&(ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]), ch);
            ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]++;
            for (i = 1; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
                ch = (RIO_TXRXM_SERIAL_CHAR_T*)malloc(sizeof (RIO_TXRXM_SERIAL_CHAR_T));
                ch->data = RIO_TXRXM_R_VAL;
                ch->type = RIO_R;
                list_Add(&(ARB_QUEUE2(handle).last[RIO_TXRXM_LANE_0]), ch);
                ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0]++;
            }

            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.comp_Seq_Req_Buf,
                handle->inst_Param_Set.comp_Seq_Request_Buffer_Size);
        }
    } /*end of while*/
    /*If buffer is empty after deleting then necessary invoke this routine
    once more for next character accepting*/
    if (ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0] < 1)
    {
        load_Char_Column(handle);
        load_Queue2_Items_1x(handle);
    }

}

/***************************************************************************
 * Function : load_Queue2_Items_4x
 *
 * Description: loads all items from the queue, which are applicable for
 *              current granule
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_Queue2_Items_4x(RIO_TXRXM_SERIAL_DS_T * handle, RIO_BOOL_T is_First)
{
    RIO_TXRXM_SERIAL_CHAR_T * ch; /*temporary pointer to the list item*/
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type;
    unsigned int i, k;              /*loop counters*/

    while (get_New_Queue2_Item(handle, &element_Type) == RIO_TRUE)
    {
        if (element_Type == RIO_STXRXM_ELEMENT_CHAR)
        {
            RIO_TXRXM_SERIAL_CHAR_T * ch2; /*temporary pointer to the list item*/
            ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
            ch->data = 
                BUFS(handle).char_Buf.buffer[BUFS(handle).char_Buf.buffer_Head].\
                character.character_Data[RIO_TXRXM_LANE_0];
            ch->type = 
                BUFS(handle).char_Buf.buffer[BUFS(handle).char_Buf.buffer_Head].\
                character.character_Type[RIO_TXRXM_LANE_0];
            /*insert data to the each lane list*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
                ch2 = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
                *ch2 = *ch;
                list_Add(&(ARB_QUEUE2(handle).last[i]), ch2);
                ARB_QUEUE2(handle).len[i]++;
            }
            free(ch);
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.char_Buf, 
                handle->inst_Param_Set.character_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_CHAR_COLUMN)
        {
            
            /*insert data to the link*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
                ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
                ch->data = 
                    BUFS(handle).char_Column_Buf.buffer[BUFS(handle).char_Column_Buf.buffer_Head].\
                    character.character_Data[i];
                ch->type = 
                    BUFS(handle).char_Column_Buf.buffer[BUFS(handle).char_Column_Buf.buffer_Head].\
                    character.character_Type[i];
                
                list_Add(&(ARB_QUEUE2(handle).last[i]), ch);
                ARB_QUEUE2(handle).len[i]++;
            }
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.char_Column_Buf,
                handle->inst_Param_Set.character_Column_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_SINGLE_CHAR)
        {
            /*in case of 1x mode it is necessary to insert character to the end of 
            current list because the list contains only one character*/
            RIO_PLACE_IN_GRANULE_T place_In_Granule;
            unsigned int char_Num;  /*number of char in the granule*/

            /*acces character characteristics*/
            ch = (RIO_TXRXM_SERIAL_CHAR_T *)malloc(sizeof(RIO_TXRXM_SERIAL_CHAR_T));
            ch->data = 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].\
                character;
            ch->type = 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].\
                char_Type;
            place_In_Granule = 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].\
                place_In_Granule;
            char_Num = 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].\
                char_Num;
            /*perform requested action*/
            if (place_In_Granule == RIO_PLACE_IN_GRANULE_EMBEDDED )
            {
                /*search item after which necessary insert*/
                list_Add(&(ARB_QUEUE2(handle).last[char_Num]), ch);
                ARB_QUEUE2(handle).len[char_Num]++;
            }
            if (place_In_Granule == RIO_PLACE_IN_GRANULE_REPLACING &&
                ARB_QUEUE2(handle).len[char_Num] > 0)
            {
                /*search item after which situated replaced item*/
                *((RIO_TXRXM_SERIAL_CHAR_T*)(ARB_QUEUE2(handle).list[char_Num]->next->item)) =
                    *ch;
                free(ch);
            }
            if (place_In_Granule == RIO_PLACE_IN_GRANULE_DELETING &&
                ARB_QUEUE2(handle).len[char_Num] > 0)
            {
                /*deletes the first character*/
                list_Del_Item(ARB_QUEUE2(handle).list[char_Num]);
                ARB_QUEUE2(handle).len[char_Num]--;
                /*search end of the list*/
                ARB_QUEUE2(handle).last[char_Num] = ARB_QUEUE2(handle).list[char_Num];
                while (ARB_QUEUE2(handle).last[char_Num]->next != NULL)
                    ARB_QUEUE2(handle).last[char_Num] = 
                        ARB_QUEUE2(handle).last[char_Num]->next;
                free(ch);

            }
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.single_Char_Buf,
                handle->inst_Param_Set.single_Character_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_COMP_SEQ)
        {
            /*In case of compensation sequence request necessary
            add 4 characters to the output*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
                ch = (RIO_TXRXM_SERIAL_CHAR_T*)malloc(sizeof (RIO_TXRXM_SERIAL_CHAR_T));
                ch->data = RIO_TXRXM_K_VAL;
                ch->type = RIO_K;
                list_Add(&(ARB_QUEUE2(handle).last[i]), ch);
                ARB_QUEUE2(handle).len[i]++;
                /*add 3 R symbol*/
                for (k = 1 ; k < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; k++)
                {
                    ch = (RIO_TXRXM_SERIAL_CHAR_T*)malloc(sizeof (RIO_TXRXM_SERIAL_CHAR_T));
                    ch->data = RIO_TXRXM_R_VAL;
                    ch->type = RIO_R;
                    list_Add(&(ARB_QUEUE2(handle).last[i]), ch);
                    ARB_QUEUE2(handle).len[i]++;
                }
            }

            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.comp_Seq_Req_Buf,
                handle->inst_Param_Set.comp_Seq_Request_Buffer_Size);
        }
    } /*end of while*/

	/*If this is the firs call then no necessity to check that in the 
	buffer data is present. This check will be performed at the
	upper level and if necessary buffer will be updated
	*/
	if (is_First == RIO_TRUE) return;

    /*If buffer is empty after deleting then necessary invoke this routine
    once more for next character accepting*/
    
    if (ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_0] < 1 ||
        ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_1] < 1 ||
        ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_2] < 1 ||
        ARB_QUEUE2(handle).len[RIO_TXRXM_LANE_3] < 1)
    {
        /*necessary to remove all data from list for correct
        work with the next granule
        after finishing this procedure invocation 
        all heads of lists will be restored in
        the TxRxM_Transfer_Character_Column routine*/
        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            ARB_QUEUE2(handle).list[i] = ARB_QUEUE2(handle).last[i];
            ARB_QUEUE2(handle).len[i] = 0;
        }

        load_Char_Column(handle);
        load_Queue2_Items_4x(handle, RIO_FALSE);
    }


}




/***************************************************************************
 * Function : get_New_Queue2_Item 
 *
 * Description: loads all items from the queue, which are applicable for
 *              current column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T get_New_Queue2_Item (
    RIO_TXRXM_SERIAL_DS_T * handle, 
    RIO_TXRXM_SERIAL_ELEMENT_T * element_Type)
{
    unsigned long queue_Head;/*help temporaries*/
    RIO_PLACE_IN_STREAM_T place;
    
    /*remove elements which are necessary place to the lower layer*/
    move_Elements_From_Queue2_To_Queue3(handle);

    if (handle->outbound_Data.queue2.out_Queue.queue_Empty == RIO_TRUE)
        return RIO_FALSE;
    queue_Head = handle->outbound_Data.queue2.out_Queue.queue_Head;

    *element_Type = handle->outbound_Data.queue2.out_Queue.queue_Buffer[queue_Head].element_Type;

    if (handle->outbound_Data.queue1.packet_Is_Sending == RIO_FALSE)
    {
		/*if packet is not sended now but in the queue some requests
		are present it is necessary to issue ones with alligment by granule
		or at specified place*/
    	if (((*element_Type == RIO_STXRXM_ELEMENT_CHAR) ||
            (*element_Type == RIO_STXRXM_ELEMENT_CHAR_COLUMN) ||
            (*element_Type == RIO_STXRXM_ELEMENT_COMP_SEQ)) &&
            (handle->outbound_Data.queue1.symbol_Is_Sent == RIO_TRUE))
        {
		    if (IS_FORCE_1X(handle) && ARB_QUEUE2(handle).curr != RIO_TXRXM_BYTE_CNT_IN_WORD - 1)
		        return RIO_FALSE;
        }
        if( (*element_Type == RIO_STXRXM_ELEMENT_SINGLE_CHAR) &&
            (handle->outbound_Data.queue1.symbol_Is_Sent == RIO_TRUE))
        {
            if (IS_FORCE_1X(handle) && ARB_QUEUE2(handle).curr != 
                BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].char_Num)
                return RIO_FALSE;
        }
        
        /*in 1x case it is necessary to wait till the last packet
        granule will be issued on link */
        if (((*element_Type == RIO_STXRXM_ELEMENT_CHAR) ||
            (*element_Type == RIO_STXRXM_ELEMENT_CHAR_COLUMN) ||
            (*element_Type == RIO_STXRXM_ELEMENT_COMP_SEQ)) &&
            IS_FORCE_1X(handle))
        {
            switch (*element_Type)
            {
                case RIO_STXRXM_ELEMENT_CHAR:
                    place = BUFS(handle).char_Buf.buffer[BUFS(handle).char_Buf.buffer_Head].place;
                    break;
                case RIO_STXRXM_ELEMENT_CHAR_COLUMN:
                    place = BUFS(handle).char_Column_Buf.buffer[BUFS(handle).char_Column_Buf.buffer_Head].place;
                    break;
                case RIO_STXRXM_ELEMENT_SINGLE_CHAR:
                    place = BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].place;
                default: 
                    place = FREE;
                    break; /*upper if prevents all other cases - this line for no warnings*/
            }
            /*wait for last character of the current granule in case of free*/
            if (place == FREE && handle->regs.curr_Byte_Tx != 3)
                return RIO_FALSE; 
        }

        queue_Dell(&(handle->outbound_Data.queue2.out_Queue));
		move_Elements_From_Queue2_To_Queue3(handle);
        return RIO_TRUE;
    }

    if (*element_Type == RIO_STXRXM_ELEMENT_CHAR)
    {
        place = BUFS(handle).char_Buf.buffer[BUFS(handle).char_Buf.buffer_Head].place;
        /*if this is free buffer then do nothing*/
        if ( place == FREE)
            return RIO_FALSE;
        if( (BUFS(handle).char_Buf.buffer[BUFS(handle).char_Buf.buffer_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD != ARB_QUEUE1(handle).cur_Pos )
            return RIO_FALSE;
        if (IS_FORCE_1X(handle) && ARB_QUEUE2(handle).curr != RIO_TXRXM_BYTE_CNT_IN_WORD - 1)
            return RIO_FALSE;
    }
    else if (*element_Type == RIO_STXRXM_ELEMENT_CHAR_COLUMN)
    {
        place = BUFS(handle).char_Column_Buf.buffer[BUFS(handle).char_Column_Buf.buffer_Head].place;
        /*if this is free buffer then do nothing*/
        if ( place == FREE)
            return RIO_FALSE;
        if( (BUFS(handle).char_Column_Buf.buffer[BUFS(handle).char_Column_Buf.buffer_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD != ARB_QUEUE1(handle).cur_Pos)
            return RIO_FALSE;
        if (IS_FORCE_1X(handle) && ARB_QUEUE2(handle).curr != RIO_TXRXM_BYTE_CNT_IN_WORD - 1)
            return RIO_FALSE;
    } 
    else if (*element_Type == RIO_STXRXM_ELEMENT_SINGLE_CHAR)
    {
        place = BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].place;
        /*if this is free buffer then do nothing*/
        if ( place == FREE) 
            return RIO_FALSE;
        if( (BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD != ARB_QUEUE1(handle).cur_Pos)
            return RIO_FALSE;
        if (IS_FORCE_1X(handle) && ARB_QUEUE2(handle).curr != 
            BUFS(handle).single_Char_Buf.buffer[BUFS(handle).single_Char_Buf.buffer_Head].char_Num)
            return RIO_FALSE;
    }
    else if (*element_Type == RIO_STXRXM_ELEMENT_COMP_SEQ)
    {
        place = BUFS(handle).comp_Seq_Req_Buf.buffer[BUFS(handle).comp_Seq_Req_Buf.buffer_Head].place;
        /*if this is free buffer then do nothing*/
        if ( place == FREE) 
            return RIO_FALSE;
        if( (BUFS(handle).comp_Seq_Req_Buf.buffer[BUFS(handle).comp_Seq_Req_Buf.buffer_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD != ARB_QUEUE1(handle).cur_Pos)
            return RIO_FALSE;
        if (IS_FORCE_1X(handle) && ARB_QUEUE2(handle).curr != 
                RIO_TXRXM_BYTE_CNT_IN_WORD - 1)
            return RIO_FALSE;
    }
    else 
    {
        place = FREE;
        tx_STxRx_Internal_Error();
    }
    /*cancel packet if necessary*/
    if (place == CANCELING)
    {
        handle->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
        /*reset arbiter*/
        ARB_QUEUE1(handle).packet_Len = 0;
        ARB_QUEUE1(handle).cur_Pos = 0;
    }

    queue_Dell(&(handle->outbound_Data.queue2.out_Queue));
	move_Elements_From_Queue2_To_Queue3(handle);
    return RIO_TRUE;
}


/************************************************************************/
/*routines for the QUEUE3 implementation*/
/************************************************************************/
/***************************************************************************
 * Function : TxRxM_Transfer_Codegroup_Column
 *
 * Description: Pass character column from pm to dp and insert requested 
 *              changes to the data flow
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int TxRxM_Transfer_Codegroup_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup  /* pointer to codegroup data structure */
)
{
    int i;
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;
    if (handle_Context == NULL || codegroup == NULL) return RIO_ERROR;

/*debug purpuse*/
#ifdef DEBUG_QUEUES_LOAD
/*
    printf("TxRx model buffers status --------------------------\n");
    Print_Buf_Size(handle, packet_Buf, packets_Buffer_Size)
    Print_Buf_Size(handle, symbol_Buf, symbols_Buffer_Size)
    Print_Buf_Size(handle, hp_Symbol_Buf, symbols_Buffer_Size)
    Print_Buf_Size(handle, char_Buf, character_Buffer_Size)
    Print_Buf_Size(handle, char_Column_Buf, character_Column_Buffer_Size)
    Print_Buf_Size(handle, single_Char_Buf, single_Character_Buffer_Size)
    Print_Buf_Size(handle, cg_Buf, code_Group_Buffer_Size)
    Print_Buf_Size(handle, cg_Column_Buf, code_Group_Column_Buffer_Size)
    Print_Buf_Size(handle, single_Bit_Buf, single_Bit_Buffer_Size)
    Print_Buf_Size(handle, comp_Seq_Req_Buf, comp_Seq_Request_Buffer_Size)
    Print_Buf_Size(handle, machine_Managment_Buf, management_Request_Buffer_Size)
*/
#endif /* DEBUG_QUEUES_LOAD */
/*end of debug purpuse*/


    /*check that it is nothing to send*/
    if (IS_FORCE_1X(handle))
    {
        load_Queue3_Items_1x(handle, RIO_TRUE);
        if (ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0] < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP)
        {
            /*remove reminder of the previous operation*/
            list_Backup(&(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]), 
                &(ARB_QUEUE3(handle).backup[RIO_TXRXM_LANE_0]), 
                &(ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]));

#ifdef Q3_LIST_CHECK
            if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
            {
                tx_STxRx_Internal_Error();
            }
#endif

            /*increment codegroup counter for case when 
            queue2 dont access to PM and get characters from the 
            queue2 internal list*/
            ARB_QUEUE3(handle).curr = (ARB_QUEUE3(handle).curr + 1) % RIO_PL_SERIAL_CNT_BYTE_IN_GRAN;
            load_CG_Column(handle);
            load_Queue3_Items_1x(handle, RIO_FALSE);

            /*restore first part of list*/
            list_Restore(&(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]), 
                ARB_QUEUE3(handle).backup[RIO_TXRXM_LANE_0], 
                &(ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]));

#ifdef Q3_LIST_CHECK
            if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
            {
                tx_STxRx_Internal_Error();
            }
#endif
        }
    }
    else
    {   
		load_Queue3_Items_4x(handle, RIO_TRUE);
		/*check all lanes len*/
        if (ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0] < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP ||
            ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_1] < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP ||
            ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_2] < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP ||
            ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_3] < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP)
        {
            /*remove reminder of the previous operation*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
#ifdef Q3_LIST_CHECK
                if (list_Len(ARB_QUEUE3(handle).list[i]) != 
                    ARB_QUEUE3(handle).len[i])
                        tx_STxRx_Internal_Error();
                if (list_Check(ARB_QUEUE3(handle).list[i], 
                    ARB_QUEUE3(handle).last[i]))
                        tx_STxRx_Internal_Error();
#endif


                list_Backup(&(ARB_QUEUE3(handle).list[i]), 
                    &(ARB_QUEUE3(handle).backup[i]), 
                    &(ARB_QUEUE3(handle).len[i]));



#ifdef Q3_LIST_CHECK
                if (list_Len(ARB_QUEUE3(handle).list[i]) != 
                    ARB_QUEUE3(handle).len[i])
                        tx_STxRx_Internal_Error();
                if (list_Check(ARB_QUEUE3(handle).list[i], 
                    ARB_QUEUE3(handle).last[i]))
                        tx_STxRx_Internal_Error();
#endif
            }
            load_CG_Column(handle);
            load_Queue3_Items_4x(handle, RIO_FALSE);

            /*restore reminder at begin of list*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
                list_Restore(&(ARB_QUEUE3(handle).list[i]), 
                    ARB_QUEUE3(handle).backup[i], 
                    &(ARB_QUEUE3(handle).len[i]));


#ifdef Q3_LIST_CHECK
                if (list_Len(ARB_QUEUE3(handle).list[i]) != 
                    ARB_QUEUE3(handle).len[i])
                {
                    tx_STxRx_Internal_Error();
                }
                if (list_Check(ARB_QUEUE3(handle).list[i], 
                    ARB_QUEUE3(handle).last[i]))
                {
                    tx_STxRx_Internal_Error();
                }
#endif


            }


        }
    }
    /*pass data to the requestor*/
    if (IS_FORCE_1X(handle))
    {
        codegroup->lane_Mask = RIO_TXRXM_SERIAL_1_LANE_USED_MASK;
        codegroup->code_Group_Data[RIO_TXRXM_LANE_0] = 
            get_CG_From_List(&(ARB_QUEUE3(handle)), RIO_TXRXM_LANE_0);
        codegroup->code_Group_Data[RIO_TXRXM_LANE_2] = 
            codegroup->code_Group_Data[RIO_TXRXM_LANE_0];
    }
    else 
    {
        int i;
        codegroup->lane_Mask = RIO_TXRXM_SERIAL_ALL_LANES_USED_MASK;

        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            codegroup->code_Group_Data[i] = 
                get_CG_From_List(&(ARB_QUEUE3(handle)), i);
        }
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : TxRxM_Get_CG_Column
 *
 * Description: Implementation of callback for the pm
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
int TxRxM_Get_CG_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup        /* pointer to codegroup data structure */
)
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T*)handle_Context;

    if (handle_Context == NULL || codegroup == NULL) return RIO_ERROR;
    
    if (handle->mach_State.sync_Enabled != RIO_TRUE) return RIO_OK;

    return (handle->dp_Interfaces.get_Codegroup_Column)(handle->dp, codegroup);
}

/***************************************************************************
 * Function : get_CG_Column
 *
 * Description: Wrapper for PM's ftray routines. Need for possibility to 
 *              on/of State Machines invocation
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
static int get_CG_Column(
    RIO_TXRXM_SERIAL_DS_T * handle,         
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup        /* pointer to codegroup data structure */
)
{
    if (handle->mach_State.sync_Enabled == RIO_FALSE) 
        return (handle->dp_Interfaces.get_Codegroup_Column)(handle->dp, codegroup);
    return (handle->pm_Interfaces.get_Codegroup_Column)(handle->pm, codegroup);
}


/***************************************************************************
 * Function : load_CG_Column
 *
 * Description: loads codegroup column from the protocol manager or data pro-
 *              cessor
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_CG_Column(RIO_TXRXM_SERIAL_DS_T * handle)
{
    RIO_PCS_PMA_CODE_GROUP_T codegroup;  /*Parameter for get_CG_Columm invocation*/
    RIO_SIGNAL_T cg[RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP]; /*codegroup data*/
    RIO_TXRXM_SERIAL_CG_BIT_T *item;     /*item for list filling*/
    int lane_N, i; /*lane counter and bit in granule counter*/

    if (IS_FORCE_1X(handle))
    {
        get_CG_Column(handle, &codegroup);
        convert_CG_To_Bits(codegroup.code_Group_Data[RIO_TXRXM_LANE_0], cg);
        for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
        {
            item = (RIO_TXRXM_SERIAL_CG_BIT_T*)malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
            item->bit = cg[i];
            /*list_Add_2(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0],*/
                list_Add(&(ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0]), item);
            ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]++;

#ifdef Q3_LIST_CHECK
            if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
            {
                tx_STxRx_Internal_Error();
            }
#endif            

        }
        /*modify number of current send byte*/
    }
    else
    {
        get_CG_Column(handle, &codegroup);
        for (lane_N = 0; lane_N < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; lane_N++)
        {

            convert_CG_To_Bits(codegroup.code_Group_Data[lane_N], cg);
            for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
            {
                item = (RIO_TXRXM_SERIAL_CG_BIT_T*)malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                item->bit = cg[i];
                /*list_Add_2(ARB_QUEUE3(handle).list[lane_N],*/
                    list_Add(&(ARB_QUEUE3(handle).last[lane_N]), item);
                ARB_QUEUE3(handle).len[lane_N]++;
#ifdef Q3_LIST_CHECK
                if (list_Len(ARB_QUEUE3(handle).list[lane_N]) != 
                    ARB_QUEUE3(handle).len[lane_N])
                {
                    tx_STxRx_Internal_Error();
                }
#endif

            }
        }
    }
}

/***************************************************************************
 * Function : load_Queue3_Items_1x
 *
 * Description: loads all items from queue 3 which are aplicable for 
 *              current granule
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_Queue3_Items_1x(RIO_TXRXM_SERIAL_DS_T * handle, RIO_BOOL_T is_First)
{

    RIO_SIGNAL_T cg[RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP]; /*storage for whole CG*/
    RIO_TXRXM_SERIAL_CG_BIT_T *item;   /*storage of the single bit of codegroup*/
    unsigned int cg_Val; /*codegroup value*/
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type;
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/
    unsigned int i, k;

    /*before changing of codegroup it is necessary to 
    count all bits*/
    it = ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]->next;
    for (i = 0; i < ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]; i++)
    {
        ((RIO_TXRXM_SERIAL_CG_BIT_T*)(it->item))->num = i;
        it = it->next;
    }

    while (get_New_Queue3_Item(handle, &element_Type) == RIO_TRUE)
    {
        if (element_Type == RIO_STXRXM_ELEMENT_CG)
        {
            cg_Val = BUFS(handle).cg_Buf.buffer[BUFS(handle).cg_Buf.buffer_Head].cg;
            convert_CG_To_Bits(cg_Val, cg);
            /*insert data to the list*/
            for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
            {
                item = (RIO_TXRXM_SERIAL_CG_BIT_T*)malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                item->bit = cg[i];
                item->num = -1;
                /*list_Add_2(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0],*/
                    list_Add(&(ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0]), item);
                ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]++;
#ifdef Q3_LIST_CHECK
                if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                    ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
                {
                    tx_STxRx_Internal_Error();
                }
#endif
            }
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.cg_Buf, 
                handle->inst_Param_Set.code_Group_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_CG_COLUMN)
        {
            
            /*insert data to the link*/
            for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
            {
                cg_Val = 
                    BUFS(handle).cg_Column_Buf.buffer[BUFS(handle).cg_Column_Buf.buffer_Head].\
                    code_Column[i];
                convert_CG_To_Bits(cg_Val, cg);
             
                for (k = 0; k < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; k++)
                {
                    item = (RIO_TXRXM_SERIAL_CG_BIT_T*)malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                    item->bit = cg[k];
                    item->num = -1;
                    /*list_Add_2(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0],*/
                        list_Add(&(ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0]), item);
                    ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]++;
#ifdef Q3_LIST_CHECK
                    if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                        ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
                    {
                        tx_STxRx_Internal_Error();
                    }
#endif
                }                
            }
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.cg_Column_Buf,
                handle->inst_Param_Set.code_Group_Column_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_SINGLE_BIT)
        {
            RIO_BOOL_T is_Found; /*search flag*/
            RIO_PLACE_IN_GRANULE_T place_In_Gr = 
                BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head]\
                .place_In_Granule;
            /*search bit which is necessary to change*/
            it = ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0];
            is_Found = RIO_FALSE;
            while (it->next != NULL)
            {
                if ( ((RIO_TXRXM_SERIAL_CG_BIT_T*)(it->next->item))->num == 
                    BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].\
                    bit_Num) 
                {
                    is_Found = RIO_TRUE;
                    break;
                }
                it = it->next;
            }
            if (is_Found == RIO_TRUE)
            {
                item = (RIO_TXRXM_SERIAL_CG_BIT_T*) malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                item->num = -1;
                item->bit = 
                    BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].\
                    bit_Value;
                if (place_In_Gr == RIO_PLACE_IN_GRANULE_EMBEDDED)
                {
                    it = it->next; /*move iterator to the item after which 
                                    necessary replace*/
                    list_Insert(it, item);
                    ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0] = list_Tail(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]);
                    ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]++;
#ifdef Q3_LIST_CHECK
                    if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                        ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
                    {
                        tx_STxRx_Internal_Error();
                    }
#endif
                }
                if (place_In_Gr == RIO_PLACE_IN_GRANULE_REPLACING)
                {
                        it = it->next; /*move iterator to the item after which 
                        necessary replace*/
                        /*replace value of item*/
                        ((RIO_TXRXM_SERIAL_CG_BIT_T*)(it->item))->bit = item->bit;
                        free(item);
                }
                if (place_In_Gr == RIO_PLACE_IN_GRANULE_DELETING)
                {
                    free(item);
                    list_Del_Item(it);
                    ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0] = list_Tail(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]);
                    ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]--;

#ifdef Q3_LIST_CHECK
                if (list_Check(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0], ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0]))
                {
                    tx_STxRx_Internal_Error();
                }
                if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                    ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
                {
                    tx_STxRx_Internal_Error();
                }
#endif
                    ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0] = 
                        ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0];
                    while (ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0]->next != NULL)
                        ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0] = 
                            ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0]->next;
                }

            }
            else if (place_In_Gr == RIO_PLACE_IN_GRANULE_EMBEDDED && 
                ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0] == 0)
            {
                /*insert requested bit to the begin of the sequence*/
                item = (RIO_TXRXM_SERIAL_CG_BIT_T*) malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                item->num = -1;
                item->bit = 
                    BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].\
                    bit_Value;
                /*list_Add_2(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0],*/
                    list_Add(&(ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0]), item);
                ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0]++;
            
#ifdef Q3_LIST_CHECK
                if (list_Len(ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0]) != 
                    ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0])
                {
                    tx_STxRx_Internal_Error();
                }
#endif
            }
            
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.single_Bit_Buf,
                handle->inst_Param_Set.single_Bit_Buffer_Size);
        }
    } /*end of while*/

	if (is_First == RIO_TRUE) return;

    /*If buffer is empty after deleting then necessary invoke this routine
    once more for next character accepting*/
    
    if (ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0] < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP)
    {
        /*necessary to remove all data from list for correct
        work with the next granule
        after finishing this procedure invocation 
        all heads of lists will be restored in
        the TxRxM_Transfer_Codegroup_Column routine*/
        ARB_QUEUE3(handle).list[RIO_TXRXM_LANE_0] = ARB_QUEUE3(handle).last[RIO_TXRXM_LANE_0];
        ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0] = 0;
        
        load_CG_Column(handle);
        load_Queue3_Items_1x(handle, RIO_FALSE);
    }
}

/***************************************************************************
 * Function : load_Queue3_Items_4x
 *
 * Description: loads all items from queue 3 which are aplicable for 
 *              current granule
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void load_Queue3_Items_4x(RIO_TXRXM_SERIAL_DS_T * handle, RIO_BOOL_T is_First)
{
    unsigned int cg_Val; /*value of the codegroup*/
    RIO_SIGNAL_T cg[RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP];
    RIO_TXRXM_SERIAL_CG_BIT_T *item;   /*storage of the single bit of codegroup*/
    RIO_TXRXM_SERIAL_ELEMENT_T element_Type;
    RIO_TXRXM_LIST_ITEM_T * it; /*iterator*/
    unsigned int i, lane_N;

    /*before changing of codegroup it is necessary to 
    count all bits*/
    for (lane_N = 0; lane_N < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; lane_N++)
    {
        it = ARB_QUEUE3(handle).list[lane_N]->next;
        for (i = 0; i < ARB_QUEUE3(handle).len[lane_N]; i++)
        {
            ((RIO_TXRXM_SERIAL_CG_BIT_T*)(it->item))->num = i;
            it = it->next;
        }
    }

    while (get_New_Queue3_Item(handle, &element_Type) == RIO_TRUE)
    {
        if (element_Type == RIO_STXRXM_ELEMENT_CG)
        {
            cg_Val = BUFS(handle).cg_Buf.buffer[BUFS(handle).cg_Buf.buffer_Head].cg;
            convert_CG_To_Bits(cg_Val, cg);
            /*insert data to the list*/
            for (lane_N = 0; lane_N < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; lane_N++)
            {
                for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
                {
                    item = (RIO_TXRXM_SERIAL_CG_BIT_T*)malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                    item->bit = cg[i];
                    item->num = -1;
                    /*list_Add_2(ARB_QUEUE3(handle).list[lane_N],*/
                        list_Add(&(ARB_QUEUE3(handle).last[lane_N]), item);
                    ARB_QUEUE3(handle).len[lane_N]++;
#ifdef Q3_LIST_CHECK
                    if (list_Len(ARB_QUEUE3(handle).list[lane_N]) != 
                        ARB_QUEUE3(handle).len[lane_N])
                    {
                        tx_STxRx_Internal_Error();
                    }
#endif
                }
            }

            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.cg_Buf, 
                handle->inst_Param_Set.code_Group_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_CG_COLUMN)
        {
            /*insert data to the link*/
            for (lane_N = 0; lane_N < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; lane_N++)
            {
                cg_Val = 
                    BUFS(handle).cg_Column_Buf.buffer[BUFS(handle).cg_Column_Buf.buffer_Head].\
                    code_Column[lane_N];
                convert_CG_To_Bits(cg_Val, cg);
             
                for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
                {
                    item = (RIO_TXRXM_SERIAL_CG_BIT_T*)malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                    item->bit = cg[i];
                    item->num = -1;
                    /*list_Add_2(ARB_QUEUE3(handle).list[lane_N],*/
                        list_Add(&(ARB_QUEUE3(handle).last[lane_N]), item);
                    ARB_QUEUE3(handle).len[lane_N]++;

#ifdef Q3_LIST_CHECK
                    if (list_Len(ARB_QUEUE3(handle).list[lane_N]) != 
                        ARB_QUEUE3(handle).len[lane_N])
                    {
                        tx_STxRx_Internal_Error();
                    }
#endif
                }                
            }
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.cg_Column_Buf,
                handle->inst_Param_Set.code_Group_Column_Buffer_Size);
        }
        if (element_Type == RIO_STXRXM_ELEMENT_SINGLE_BIT)
        {
            RIO_BOOL_T is_Found; /*search flag*/
            RIO_PLACE_IN_GRANULE_T place_In_Gr = 
                BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head]\
                .place_In_Granule;
            unsigned int character_Num = 
                BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head]\
                .character_Num;
            /*search bit which is necessary to change*/
            it = ARB_QUEUE3(handle).list[character_Num];
            is_Found = RIO_FALSE;
            while (it->next != NULL)
            {
                if ( ((RIO_TXRXM_SERIAL_CG_BIT_T*)(it->next->item))->num == 
                    BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].\
                    bit_Num) 
                {
                    is_Found = RIO_TRUE;
                    break;
                }
                it = it->next;
            }
            if (is_Found == RIO_TRUE)
            {
                item = (RIO_TXRXM_SERIAL_CG_BIT_T*) malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                item->num = -1;
                item->bit = 
                    BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].\
                    bit_Value;
                if (place_In_Gr == RIO_PLACE_IN_GRANULE_EMBEDDED)
                {
                    it = it->next; /*move iterator to the item after which 
                                    necessary replace*/
                    list_Insert(it, item);
                    ARB_QUEUE3(handle).last[character_Num] = list_Tail(ARB_QUEUE3(handle).list[character_Num]);
                    ARB_QUEUE3(handle).len[character_Num]++;
#ifdef Q3_LIST_CHECK
                    if (list_Len(ARB_QUEUE3(handle).list[character_Num]) != 
                        ARB_QUEUE3(handle).len[character_Num])
                    {
                        tx_STxRx_Internal_Error();
                    }
#endif
                }
                if (place_In_Gr == RIO_PLACE_IN_GRANULE_REPLACING)
                {
                        it = it->next; /*move iterator to the item after which 
                        necessary replace*/
                        /*replace value of item*/
                        ((RIO_TXRXM_SERIAL_CG_BIT_T*)(it->item))->bit = item->bit;
                        free(item);
                }
                if (place_In_Gr == RIO_PLACE_IN_GRANULE_DELETING)
                {
                    free(item);
                    list_Del_Item(it);
                    ARB_QUEUE3(handle).last[character_Num] = list_Tail(ARB_QUEUE3(handle).list[character_Num]);
                    ARB_QUEUE3(handle).len[character_Num]--;

#ifdef Q3_LIST_CHECK
                    if (list_Check(ARB_QUEUE3(handle).list[character_Num], ARB_QUEUE3(handle).last[character_Num]))
                    {
                        tx_STxRx_Internal_Error();
                    }

                    if (list_Len(ARB_QUEUE3(handle).list[character_Num]) != 
                        ARB_QUEUE3(handle).len[character_Num])
                    {
                        tx_STxRx_Internal_Error();
                    }
#endif

                    ARB_QUEUE3(handle).last[character_Num] = 
                        ARB_QUEUE3(handle).list[character_Num];
                    while (ARB_QUEUE3(handle).last[character_Num]->next != NULL)
                        ARB_QUEUE3(handle).last[character_Num] = 
                            ARB_QUEUE3(handle).last[character_Num]->next;

                }

            }
            else if (place_In_Gr == RIO_PLACE_IN_GRANULE_EMBEDDED && 
                ARB_QUEUE3(handle).len[character_Num] == 0)
            {
                /*insert requested bit to the begin of the sequence*/
                item = (RIO_TXRXM_SERIAL_CG_BIT_T*) malloc(sizeof(RIO_TXRXM_SERIAL_CG_BIT_T));
                item->num = -1;
                item->bit = 
                    BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].\
                    bit_Value;
                /*list_Add_2(ARB_QUEUE3(handle).last[character_Num],*/
                    list_Add(&(ARB_QUEUE3(handle).last[character_Num]), item);
                ARB_QUEUE3(handle).len[character_Num]++;
#ifdef Q3_LIST_CHECK
                if (list_Len(ARB_QUEUE3(handle).list[character_Num]) != 
                    ARB_QUEUE3(handle).len[character_Num])
                {
                    tx_STxRx_Internal_Error();
                }
#endif
            }

            
            Pop_Buffer_Item(handle->outbound_Data.out_Bufs.single_Bit_Buf,
                handle->inst_Param_Set.single_Bit_Buffer_Size);

        }
    } /*end of while*/

	if (is_First == RIO_TRUE) return;

    /*If buffer is empty after deleting then necessary invoke this routine
    once more for next character accepting*/
    
    if (ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_0] < 1 ||
        ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_1] < 1 ||
        ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_2] < 1 ||
        ARB_QUEUE3(handle).len[RIO_TXRXM_LANE_3] < 1)
    {
        /*necessary to remove all data from list for correct
        work with the next granule
        after finishing this procedure invocation 
        all heads of lists will be restored in
        the TxRxM_Transfer_Codegroup_Column routine*/
        for (i = 0; i < RIO_PL_SERIAL_CNT_BYTE_IN_GRAN; i++)
        {
            ARB_QUEUE3(handle).list[i] = ARB_QUEUE3(handle).last[i];
            ARB_QUEUE3(handle).len[i] = 0;
        }
        load_CG_Column(handle);
        load_Queue3_Items_4x(handle, RIO_FALSE);
    }

}
/***************************************************************************
 * Function : RIO_BOOL_T get_New_Queue3_Item 
 *
 * Description: loads all items from the queue, which are applicable for
 *              current column
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static RIO_BOOL_T get_New_Queue3_Item (
    RIO_TXRXM_SERIAL_DS_T * handle, 
    RIO_TXRXM_SERIAL_ELEMENT_T * element_Type)
{
    unsigned long queue_Head;/*help temporaries*/
    RIO_PLACE_IN_STREAM_T place;
    
    if (handle->outbound_Data.queue3.out_Queue.queue_Empty == RIO_TRUE)
        return RIO_FALSE;
    queue_Head = handle->outbound_Data.queue3.out_Queue.queue_Head;
    *element_Type = handle->outbound_Data.queue3.out_Queue.queue_Buffer[queue_Head].element_Type;

    if (handle->outbound_Data.queue1.packet_Is_Sending == RIO_FALSE)
    {
        /*in case of 1x handle all requestst should be alligned*/
        if (IS_FORCE_1X(handle) && 
            (handle->outbound_Data.queue1.symbol_Is_Sent == RIO_TRUE))
        {
            if ( 
                ((*element_Type == RIO_STXRXM_ELEMENT_CG) ||
                (*element_Type == RIO_STXRXM_ELEMENT_CG_COLUMN)) &&
                (ARB_QUEUE3(handle).curr != RIO_TXRXM_BYTE_CNT_IN_WORD - 1)
                )
                return RIO_FALSE;
            if ( (*element_Type == RIO_STXRXM_ELEMENT_SINGLE_BIT) &&
                (ARB_QUEUE3(handle).curr != 
                BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].character_Num)
                )
                return RIO_FALSE;
        }
        /*check character num for single bit requests*/
        else if (*element_Type == RIO_STXRXM_ELEMENT_SINGLE_BIT && IS_FORCE_1X(handle) &&
            ARB_QUEUE3(handle).curr != 
                BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].character_Num)
        {
                return RIO_FALSE;
        }


        queue_Dell(&(handle->outbound_Data.queue3.out_Queue));
        return RIO_TRUE;
    }

    if (*element_Type == RIO_STXRXM_ELEMENT_CG)
    {
        place = BUFS(handle).cg_Buf.buffer[BUFS(handle).cg_Buf.buffer_Head].place;
        /*if this is free buffer then do nothing*/
        if ( place == FREE)
            return RIO_FALSE;
        if( (BUFS(handle).cg_Buf.buffer[BUFS(handle).cg_Buf.buffer_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD != ARB_QUEUE1(handle).cur_Pos )
            return RIO_FALSE;
        if (IS_FORCE_1X(handle) && ARB_QUEUE3(handle).curr != RIO_TXRXM_BYTE_CNT_IN_WORD - 1)
            return RIO_FALSE;
    }
    else if (*element_Type == RIO_STXRXM_ELEMENT_CG_COLUMN)
    {
        place = BUFS(handle).cg_Column_Buf.buffer[BUFS(handle).cg_Column_Buf.buffer_Head].place;
        /*if this is free buffer then do nothing*/
        if ( place == FREE)
            return RIO_FALSE;
        if( (BUFS(handle).cg_Column_Buf.buffer[BUFS(handle).cg_Column_Buf.buffer_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD != ARB_QUEUE1(handle).cur_Pos)
            return RIO_FALSE;
        if (IS_FORCE_1X(handle) && ARB_QUEUE3(handle).curr != RIO_TXRXM_BYTE_CNT_IN_WORD - 1)
            return RIO_FALSE;
    } 
    else if (*element_Type == RIO_STXRXM_ELEMENT_SINGLE_BIT)
    {
        place = BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].place;
        /*if this is free buffer then do nothing*/
        if ( place == FREE)
            return RIO_FALSE;
        if( (BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].granule_Num + 1) *
                RIO_TXRXM_BYTE_CNT_IN_WORD != ARB_QUEUE1(handle).cur_Pos)
            return RIO_FALSE;
        if (IS_FORCE_1X(handle) && 
            ARB_QUEUE3(handle).curr != 
            BUFS(handle).single_Bit_Buf.buffer[BUFS(handle).single_Bit_Buf.buffer_Head].character_Num)
            return RIO_FALSE;
    }
    else
    {
        place = FREE;
        tx_STxRx_Internal_Error();
    }
    /*cancel packet if necessary*/
    if (place == CANCELING)
    {
        handle->outbound_Data.queue1.packet_Is_Sending = RIO_FALSE;
        /*reset arbit*/
        ARB_QUEUE1(handle).packet_Len = 0;
        ARB_QUEUE1(handle).cur_Pos = 0;
    }
    queue_Dell(&(handle->outbound_Data.queue3.out_Queue));
    return RIO_TRUE;
}

/***************************************************************************
 * Function : get_CG_From_List
 *
 * Description: returns codegroup value from the list
 *
 * Returns: none
 *
 * Notes: Assumed that list contains necessary number of granules
 *          so no checking is implemented
 *
 **************************************************************************/
static unsigned int get_CG_From_List(RIO_TXRXM_SERIAL_ARB_T * arb, int lane)
{
    unsigned int cg;    /*codegroup value*/
    unsigned int bit;   /*bit value*/
    RIO_TXRXM_SERIAL_CG_BIT_T * item; /*value of the bit*/
    int i;

#ifdef Q3_LIST_CHECK
    if (list_Check(arb->list[lane], arb->last[lane]))
    {
        tx_STxRx_Internal_Error();
    }
    if (list_Len(arb->list[lane]) != 
        arb->len[lane])
    {
        tx_STxRx_Internal_Error();
    }
#endif

    cg = 0;
    for (i = 0; i < RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP; i++)
    {
        item = (RIO_TXRXM_SERIAL_CG_BIT_T*)list_Get_Item(arb->list[lane]);
        bit = item->bit == RIO_SIGNAL_1 ? 1 : 0;
        free(item);
        cg = (cg << 1) | (bit & 1);
        arb->len[lane]--;
    }

    if (arb->len[lane] == 0) arb->last[lane] = arb->list[lane];

#ifdef Q3_LIST_CHECK
    if (list_Check(arb->list[lane], arb->last[lane]))
    {
        tx_STxRx_Internal_Error();
    }
#endif

    return cg;
}

/*connectors for the pop calls from the protocol manager to the 
data processor*/
/***************************************************************************
 * Function : RIO_TxRxM_Pop_Char_Buffer_Stub
 *
 * Description: Connector for the pop char call
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Pop_Char_Buffer_Connector(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
	RIO_UCHAR_T				  lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
)
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T *)handle_Context;

    if (handle == NULL) return RIO_ERROR;
    return (handle->dp_Interfaces.pop_Char_Buffer)(
        handle->dp, lane_Mask);
}

/***************************************************************************
 * Function : RIO_TxRxM_Pop_Char_Buffer_Stub
 *
 * Description: Connector for the pop codegroup call
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
int RIO_TxRxM_Pop_CG_Buffer_Connector(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
	RIO_UCHAR_T				  lane_ID			/* 0 - for lane 0, 1 - for lane 1, etc. */
)
{
    RIO_TXRXM_SERIAL_DS_T * handle = (RIO_TXRXM_SERIAL_DS_T *)handle_Context;

    if (handle == NULL) return RIO_ERROR;

    if (handle->inbound_Data.pop_Bit_Requests == 0) 
        return (handle->dp_Interfaces.pop_Codegroup_Buffer)(
            handle->dp, lane_ID);
    else 
    {
        handle->inbound_Data.pop_Bit_Requests--;
        return RIO_OK;
    }

}
