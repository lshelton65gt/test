#ifndef RIO_TXRXM_SERIAL_RX_H
#define RIO_TXRXM_SERIAL_RX_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_serial_rx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model transmitter header
*
* Notes:        
*
******************************************************************************/
#include "rio_txrxm_serial_ds.h"

/*routines for DP ctray*/
int RIO_STxRxM_Put_CG(
    RIO_CONTEXT_T					handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup        /* pointer to codegroup data structure */
    );

int RIO_STxRxM_Put_Character_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
    );

int RIO_STxRxM_Put_Granule(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T	  *granule          /* pointer to granule data structure */
    );


/*routines for PM ctray*/
int RIO_STxRxM_Pass_CG(
    RIO_CONTEXT_T					handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T		*codegroup        /* pointer to codegroup data structure */
    );

int RIO_STxRxM_Pass_Character_Column(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T	  *character        /* pointer to character data structure */
    );

int RIO_STxRxM_Granule_Acceptor(
    RIO_CONTEXT_T			  handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T	  *granule          /* pointer to granule data structure */
);

void RIO_STxRxM_Rx_Data_Init(RIO_TXRXM_SERIAL_DS_T *instance);

/****************************************************************************/
#endif /* RIO_TXRXM_SERIAL_RX_H */
