#ifndef RIO_TXRXM_COMMON_DS_H
#define RIO_TXRXM_COMMON_DS_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\riom\txrxm\include\rio_txrxm_common_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model data structure description
*
* Notes:        
*
******************************************************************************/
#include "rio_types.h"

#define RIO_TXRXM_MAX_PAYLOAD_SIZE          32 
/* the maximum length of packet stream */
#define RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN   300

/* this structure represent unified type for outbound buffer entry*/
typedef struct {
    RIO_TXRXM_PACKET_REQ_STRUCT_T   entry;
    RIO_DW_T                        payload[RIO_TXRXM_MAX_PAYLOAD_SIZE];
    unsigned int                    payload_Size;
} RIO_TXRXM_OUT_PACKET_STRUCT_T;

/* this type represents tha arbiter with current data stream */
typedef struct {
    RIO_UCHAR_T packet_Stream[RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN];
    unsigned int packet_Len;
} RIO_TXRXM_OUT_PACKET_STREAM_T;

typedef struct {
    RIO_BOOL_T                    as_Struct;/*shows how packet is represented: as struct or as stream*/  
    RIO_TXRXM_OUT_PACKET_STRUCT_T packet_Struct;
    RIO_TXRXM_OUT_PACKET_STREAM_T packet_Stream;
}RIO_TXRXM_PACKET_OUT_ENTRY_T;

/* this type represents the arbiter with current data stream */
typedef struct {
    RIO_UCHAR_T packet_Stream[RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN];
    unsigned int packet_Len; /*packet length in bytes count*/
    unsigned int cur_Pos; /*current byte number*/
    RIO_BOOL_T last_Gr_Sent; /*last packet granule event 
                            for this packet already published
                            this is serial only parameter*/
} RIO_TXRXM_PACKET_ARB_T;

/* the entry of inbound buffer */

typedef struct {
    RIO_REQ_HEADER_T     header;
    RIO_REMOTE_REQUEST_T request;
    RIO_DW_T             payload[RIO_TXRXM_MAX_PAYLOAD_SIZE];
    RIO_UCHAR_T          ack_ID;
} RIO_TXRXM_IN_REQ_TO_LL_T;

/*compute CRC*/
void RIO_TxRxM_Out_Compute_CRC(unsigned short *crc, RIO_UCHAR_T stream[], unsigned int i_Max);
/*convert double-words to char arrays*/
void RIO_TxRxM_Out_Conv_Dw_Char(RIO_DW_T *dw, RIO_UCHAR_T *stream);

#define RIO_TXRXM_INIT_CRC_VALUE            0xffff /*init crc value (help)*/

#define RIO_TXRXM_BYTE_CNT_IN_DWORD         8   /* count of bytes in double word */
#define RIO_TXRXM_INIT_CRC_VALUE            0xffff /*init crc value (help)*/
#define RIO_TXRXM_BYTE_CNT_IN_WORD          4   /* byte count in word */
#define RIO_TXRXM_MAX_PROTOCOL_PACKET_LEN   276	/* 276 maximum packet length according the protocol*/
#define RIO_TXRXM_MIN_PROTOCOL_PACKET_LEN   6   /*lenght of the minimal packet size*/
#define RIO_TXRXM_PACKET_LEN_NEED_INT_CRC   84
#define RIO_TXRXM_BITS_IN_BYTE              8
#define RIO_TXRXM_INT_CRC_START             80

#define RIO_TXRXM_ACKID_POS               0
#define RIO_TXRXM_ACKID_MASK              0x70
#define RIO_TXRXM_FTYPE_POS               1
#define RIO_TXRXM_TTYPE_POS_LOW           4
#define RIO_TXRXM_TTYPE_POS_HIGH          6
#define RIO_TXRXM_TTYPE_MASK              0xf0
#define RIO_TXRXM_TTYPE_SHIFT             4
#define RIO_TXRXM_FTYPE_MASK              0x0f
#define RIO_TXRXM_TT_POS                  1
#define RIO_TXRXM_TT_MASK                 0x30
#define RIO_TXRXM_TT_SHIFT                4
#define RIO_TXRXM_PRIO_POS                1
#define RIO_TXRXM_PRIO_MASK               0xc0
#define RIO_TXRXM_PRIO_SHIFT              6
#define RIO_TXRXM_TRANSP_TYPE_POS         2
#define RIO_TXRXM_SIZE_MASK               0x0f
#define RIO_TXRXM_CNT_OF_INT_RESP         2
#define RIO_TXRXM_MAX_TID_TXRXMUS_ONE     256 
#define RIO_TXRXM_REG_MISALIGN_MASK       0x03
#define RIO_TXRXM_REG_HEADER_SHIFT        16
#define RIO_TXRXM_STATUS_MASK             0x0f
#define RIO_TXRXM_BYTES_IN_HALF_WORD      2

/*these constants are necessary to check packet header size - in bits count*/
#define RIO_TXRXM_FIRST_16_BIT_SIZE       16
#define RIO_TXRXM_TR_INFO_SIZE_16         16
#define RIO_TXRXM_TR_INFO_SIZE_32         32
#define RIO_TXRXM_EXT_ADDR_SIZE_16        16
#define RIO_TXRXM_EXT_ADDR_SIZE_32        32
#define RIO_TXRXM_CRC_FIELD_SIZE          16 /*size of one CRC field*/


/*all packet header sizes not includes first 16 packet bits,
 *size of transport info, extended address fields
 */
#define RIO_TXRXM_PACK_8_HEADER_SIZE      48 /*include hop count*/
#define RIO_TXRXM_PACK_13_HEADER_SIZE     16
#define RIO_TXRXM_PACK_2_HEADER_SIZE      48
#define RIO_TXRXM_PACK_5_HEADER_SIZE      48
#define RIO_TXRXM_PACK_6_HEADER_SIZE      32
#define RIO_TXRXM_PACK_10_HEADER_SIZE     32
#define RIO_TXRXM_PACK_11_HEADER_SIZE     16
#define RIO_TXRXM_PACK_1_HEADER_SIZE      64

/*bit size of reserved packet fields*/
#define RIO_TXRXM_PACK_6_RSRV_FIELD_SIZE          1       
#define RIO_TXRXM_PACK_8_REQ_RSRV_FIELD_SIZE      2    
#define RIO_TXRXM_PACK_8_RESP_RSRV_FIELD_SIZE     24
#define RIO_TXRXM_PACK_10_RSRV_FIELD_SIZE         8

#define RIO_STXRXM_ACK_ID_SHIFT                   3

/****************************************************************************/
#endif /* RIO_TXRXM_COMMON_DS_H */





