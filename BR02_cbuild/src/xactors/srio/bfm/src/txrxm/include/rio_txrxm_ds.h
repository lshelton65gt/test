#ifndef RIO_TXRXM_DS_H
#define RIO_TXRXM_DS_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model data structure description
*
* Notes:        
*
******************************************************************************/

/*includes which are necessary only for the parallel mode*/
#include "rio_32_txrx_model.h"
#include "rio_txrxm_parallel_model.h"
#include "rio_txrxm_common_ds.h"
#include "rio_parallel_init.h"


/*defines */
typedef enum {
    RIO_TXRXM_ELEMENT_PACKET = 0,
    RIO_TXRXM_ELEMENT_SYMBOL,
    RIO_TXRXM_ELEMENT_GRANULE,
    RIO_TXRXM_ELEMENT_EVENT,
    RIO_TXRXM_ELEMENT_HP_SYMBOL /*symbol with the higest priority*/
} RIO_TXRXM_ELEMENT_T;


typedef struct {
    RIO_SYMBOL_STRUCT_T        symbol;
    RIO_PLACE_IN_STREAM_T    place; /*describe placement in packet*/
    unsigned int            granule_Num;
    RIO_GRANULE_TYPE_T      granule_Type;
} RIO_TXRXM_SYMBOL_OUT_ENTRY_T;

typedef struct {
    unsigned long            granule;
    RIO_BOOL_T              has_Frame_Toggled;
    RIO_PLACE_IN_STREAM_T    place; /*describe placement in packet*/
    unsigned int            granule_Num;
    RIO_GRANULE_TYPE_T      granule_Type;
} RIO_TXRXM_GRANULE_OUT_ENTRY_T;

typedef struct {
    RIO_TXRX_EVENT_T        event;
    RIO_PLACE_IN_STREAM_T   place; /*describe placement in packet*/
    unsigned int            granule_Num;
} RIO_TXRXM_EVENT_OUT_ENTRY_T;


/* the structure of outbound buffer */
typedef struct {
    RIO_TXRXM_PACKET_OUT_ENTRY_T       *packet_Buffer;     /* buffer itself */
    unsigned int                       packet_Buffer_Head;/* first full entry*/
    unsigned int                       packet_Buffer_Top; /* next(first) empty entry*/
    RIO_BOOL_T                         buffer_Full;
    RIO_TXRXM_PACKET_OUT_ENTRY_T       packet_Temp; /*temporary entry for incoming requests*/
} RIO_TXRXM_OUT_PACKET_BUFFER_T;


typedef struct {
    RIO_TXRXM_SYMBOL_OUT_ENTRY_T    *symbol_Buffer;
    unsigned int                    symbol_Buffer_Head;/* first full entry*/
    unsigned int                    symbol_Buffer_Top; /* next(first) empty entry*/
    RIO_BOOL_T                      buffer_Full;
    RIO_TXRXM_SYMBOL_OUT_ENTRY_T    symbol_Temp; /*temporary entry for incoming requests*/
} RIO_TXRXM_OUT_SYMBOL_BUFFER_T;

typedef struct {
    RIO_TXRXM_GRANULE_OUT_ENTRY_T    *granule_Buffer;
    unsigned int                    granule_Buffer_Head;/* first full entry*/
    unsigned int                    granule_Buffer_Top; /* next(first) empty entry*/
    RIO_BOOL_T                      buffer_Full;
    RIO_TXRXM_GRANULE_OUT_ENTRY_T   granule_Temp; /*temporary entry for incoming requests*/
} RIO_TXRXM_OUT_GRANULE_BUFFER_T;

typedef struct {
    RIO_TXRXM_EVENT_OUT_ENTRY_T     *event_Buffer;
    unsigned int                    event_Buffer_Head;/* first full entry*/
    unsigned int                    event_Buffer_Top; /* next(first) empty entry*/
    RIO_BOOL_T                      buffer_Full;
    RIO_TXRXM_EVENT_OUT_ENTRY_T     event_Temp; /*temporary entry for incoming requests*/
} RIO_TXRXM_OUT_EVENT_BUFFER_T;


typedef struct {
    unsigned int                    element_Place_In_Buffer;
    RIO_TXRXM_ELEMENT_T             element_Type;
} RIO_TXRXM_QUEUE_OUT_ENTRY_T;

typedef struct {
    RIO_TXRXM_QUEUE_OUT_ENTRY_T       *queue_Buffer;
    unsigned long                    queue_Head;/* first full entry*/
    unsigned long                    queue_Top; /* next(first) empty entry*/
    RIO_BOOL_T                      queue_Empty;
} RIO_TXRXM_OUT_QUEUE_T;



typedef struct {
    RIO_BOOL_T                      packet_Is_Sending;
    RIO_TXRXM_OUT_PACKET_BUFFER_T   out_Packet_Buffer;
    RIO_TXRXM_OUT_SYMBOL_BUFFER_T   out_Symbol_Buffer;
    RIO_TXRXM_OUT_SYMBOL_BUFFER_T   out_High_Prio_Symbol_Buffer;
    RIO_TXRXM_OUT_GRANULE_BUFFER_T  out_Granule_Buffer;
    RIO_TXRXM_OUT_EVENT_BUFFER_T    out_Event_Buffer;
    RIO_TXRXM_OUT_QUEUE_T           out_Queue;
    RIO_TXRXM_OUT_QUEUE_T           out_High_Prio_Queue;
    RIO_TXRXM_PACKET_ARB_T          outbound_Packet_Arb;
} RIO_TXRXM_OUT_DS_T;

typedef struct {
    RIO_BOOL_T                      packet_Is_Receiving;
    RIO_TXRXM_PACKET_ARB_T          inbound_Arb;
} RIO_TXRXM_IN_DS_T;

/* inbound buffer structure */

typedef struct {
    unsigned long                   instance_Number;
    RIO_BOOL_T                      in_Reset;
    RIO_BOOL_T                      is_Bound;

    
    RIO_LINK_MODEL_FTRAY_T          link_Interface;
    RIO_HANDLE_T                    link_Instanse;
    
    RIO_TXRXM_MODEL_START_RESET     reset_Function;

    RIO_TXRXM_OUT_DS_T              outbound_Data;
    RIO_TXRXM_IN_DS_T               inbound_Data;
    RIO_TXRXM_PARALLEL_MODEL_CALLBACK_TRAY_T ext_Interfaces;
    RIO_TXRXM_PARALLEL_PARAM_SET_T           parameters_Set;
    RIO_TXRXM_PARALLEL_INST_PARAM_T          inst_Param_Set;
    RIO_TXRXM_PARALLEL_MODEL_INITIALIZE      restart_Function;
    /*changes for the Training block instance*/
    RIO_HANDLE_T                    init_Instanse;
    RIO_PL_PAR_INIT_BLOCK_FTRAY_T   init_Interface;
} RIO_TXRXM_DS_T;



/****************************************************************************/
#endif /* RIO_TXRXM_DS_H */





