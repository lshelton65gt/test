#ifndef RIO_TXRXM_TX_H
#define RIO_TXRXM_TX_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_tx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model transmitter header
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_TXRXM_DS_H
#include "rio_txrxm_ds.h"
#endif      /*RIO_TXRXM_DS_H*/


/*load entry in packet buffer from temp*/
int RIO_TxRxM_Load_Packet_Entry(RIO_TXRXM_DS_T *handle);

/*load entry in symbol buffer from temp*/
int RIO_TxRxM_Load_Symbol_Entry(RIO_TXRXM_DS_T *handle);

/*load entry in granule buffer from temp*/
int RIO_TxRxM_Load_Granule_Entry(RIO_TXRXM_DS_T *handle);

/*load entry in event buffer from temp*/
int RIO_TxRxM_Load_Event_Entry(RIO_TXRXM_DS_T *handle);

/*get new granule for transmitting*/
void RIO_TxRxM_Granule_For_Transmitting(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule
    );

/*init packet arbiter*/
void RIO_TxRxM_Out_Init_Arbiter(RIO_TXRXM_PACKET_ARB_T *element);

/*initialize transmitter*/
void RIO_TxRxM_Tx_Init(RIO_TXRXM_DS_T *handle);


/****************************************************************************/
#endif /* RIO_TXRXM_TX_H */







