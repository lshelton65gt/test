#ifndef RIO_TXRXM_SERIAL_TX_H
#define RIO_TXRXM_SERIAL_TX_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_serial_tx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model transmitter header
*
* Notes:        
*
******************************************************************************/
#include "rio_txrxm_serial_ds.h"

int RIO_STxRxM_Tx_Data_Init(RIO_TXRXM_SERIAL_DS_T *instance);

int RIO_STxRxM_Load_Packet_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Symbol_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Char_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Char_Column_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Single_Char_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Cg_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Cg_Column_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Single_Bit_Entry (RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Comp_Seq_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Machine_Managment_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Pop_Bit_Entry(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Load_Pop_Char_Entry(RIO_TXRXM_SERIAL_DS_T *instance);

/*callback description*/
int RIO_STxRxM_Get_Granule_From_Queue1(
    RIO_CONTEXT_T             handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T     *granule          /* pointer to granule data structure */
);

int RIO_STxRxM_Get_Granule_Connector(
    RIO_CONTEXT_T         handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_GRANULE_T *granule          /* pointer to granule data structure */
);

int TxRxM_Transfer_Character_Column(
    RIO_CONTEXT_T           handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character        /* pointer to character data structure */
);


int TxRxM_Get_Char_Column(
    RIO_CONTEXT_T           handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CHARACTER_T *character        /* pointer to character data structure */
);


int TxRxM_Get_CG_Column(
    RIO_CONTEXT_T            handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T *codegroup        /* pointer to codegroup data structure */
);

int TxRxM_Transfer_Codegroup_Column(
    RIO_CONTEXT_T             handle_Context,   /* callback handle/context */
    RIO_PCS_PMA_CODE_GROUP_T  *codegroup  /* pointer to codegroup data structure */
);


int RIO_STxRxM_Tx_Data_Reset(RIO_TXRXM_SERIAL_DS_T *instance);
int RIO_STxRxM_Tx_Data_Delete(RIO_TXRXM_SERIAL_DS_T *instance);

int RIO_TxRxM_Pop_Char_Buffer_Connector(
    RIO_CONTEXT_T    handle_Context,   /* callback handle/context */
    RIO_UCHAR_T      lane_Mask         /* bit 0 - for lane 0, bit 1 - for lane 1, etc. */
);

int RIO_TxRxM_Pop_CG_Buffer_Connector(
    RIO_CONTEXT_T    handle_Context,   /* callback handle/context */
    RIO_UCHAR_T      lane_ID /* 0 - for lane 0, 1 - for lane 1, etc. */
);



/****************************************************************************/
#endif /* RIO_TXRXM_SERIAL_TX_H */
