#ifndef RIO_TXRXM_RX_H
#define RIO_TXRXM_RX_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_rx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model receiver header
*
* Notes:        
*
******************************************************************************/
#ifndef RIO_TXRXM_DS_H
#include "rio_txrxm_ds.h"
#endif    /*RIO_TXRXM_DS_H*/

/*main loop for granule processing*/
void RIO_TxRxM_Granule_Received(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule);

/*initialize receiver*/
void RIO_TxRxM_Rx_Init(RIO_TXRXM_DS_T *handle);
/****************************************************************************/
#endif /* RIO_TXRXM_RX_H */






