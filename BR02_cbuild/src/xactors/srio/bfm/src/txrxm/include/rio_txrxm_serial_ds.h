#ifndef RIO_TXRXM_SERIAL_DS_H
#define RIO_TXRXM_SERIAL_DS_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_serial_ds.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model data structure description
*
* Notes:        
*
******************************************************************************/

/*includes which are necessary only for the parallel mode*/
#include "rio_txrxm_serial_model.h"
#include "rio_txrxm_common_ds.h"
#include "rio_pcs_pma_dp.h"
#include "rio_pcs_pma_pm.h"
#include "rio_gran_type.h"


/*defines */
typedef enum {
    RIO_STXRXM_ELEMENT_PACKET = 0,
    RIO_STXRXM_ELEMENT_SYMBOL,
    RIO_STXRXM_ELEMENT_CHAR,
    RIO_STXRXM_ELEMENT_CHAR_COLUMN,
    RIO_STXRXM_ELEMENT_SINGLE_CHAR,
    RIO_STXRXM_ELEMENT_CG,
    RIO_STXRXM_ELEMENT_CG_COLUMN,
    RIO_STXRXM_ELEMENT_SINGLE_BIT,
    RIO_STXRXM_ELEMENT_COMP_SEQ,
    RIO_STXRXM_ELEMENT_MACHINE_MGMNT,
    RIO_STXRXM_ELEMENT_POP_CHAR_ITEM,
    RIO_STXRXM_ELEMENT_POP_BIT_ITEM,
    RIO_STXRXM_ELEMENT_HP_SYMBOL
} RIO_TXRXM_SERIAL_ELEMENT_T;


typedef struct {
    RIO_UCHAR_T data;   /*value*/
    RIO_CHARACTER_TYPE_T type; /*symbol type*/
} RIO_TXRXM_SERIAL_CHAR_T;

typedef struct {
    RIO_SIGNAL_T bit;   /*codegroup bit*/
    RIO_UCHAR_T num;    /*number in codegroup*/
} RIO_TXRXM_SERIAL_CG_BIT_T;

typedef struct RIO_TXRXM_LIST_ITEM_TYPE{
    void * item;        /*pointer to the value*/
    struct RIO_TXRXM_LIST_ITEM_TYPE * next; /*pointer to the next value*/
}RIO_TXRXM_LIST_ITEM_T;

/*buffers types*/
typedef struct {
    RIO_SYMBOL_SERIAL_STRUCT_T  symbol;
    RIO_PLACE_IN_STREAM_T       place; /*describe placement in packet*/
    unsigned int                granule_Num; /*describe granule number in packet*/
} RIO_TXRXM_SERIAL_SYMBOL_OUT_ENTRY_T;

typedef struct {
    RIO_PCS_PMA_GRANULE_T   granule;
    RIO_PLACE_IN_STREAM_T   place; /*describe placement in packet*/
    unsigned int            granule_Num; /*describe granule number in the packet*/
} RIO_TXRXM_SERAIL_GRANULE_OUT_ENTRY_T;

/*character which will be inserted after specified granule*/
typedef struct {
    RIO_PCS_PMA_CHARACTER_T character;
    RIO_PLACE_IN_STREAM_T       place; /*describe placement in packet*/
    unsigned int granule_Num;   /* number of granule in packet after which it placed*/
} RIO_TXRXM_SERIAL_CHAR_OUT_ENTRY_T;

/*characters column entry*/
typedef struct {
    RIO_PCS_PMA_CHARACTER_T character;  /*describe whole codegroup*/
    RIO_PLACE_IN_STREAM_T       place; /*describe placement in packet*/
    unsigned int granule_Num;   /* number of granule in packet after which it placed*/
} RIO_TXRXM_SERIAL_CHAR_COLUMN_OUT_ENTRY_T;

/*single bit entry*/
typedef struct {
    RIO_UCHAR_T character;
    RIO_CHARACTER_TYPE_T char_Type;
    RIO_PLACE_IN_STREAM_T place; /*describe placement in the packet*/
    unsigned int granule_Num;
    RIO_PLACE_IN_GRANULE_T place_In_Granule; /*type of placement in the granule*/
    unsigned int char_Num;      /*Number of placement in the granule which sall be changed*/
} RIO_TXRXM_SERIAL_SINGLE_CHAR_ENTRY_T;

/*codegroup entry*/
typedef struct {
    unsigned int cg; /*10 bit of codegroup*/
    RIO_PLACE_IN_STREAM_T place; /*place in stram*/
    unsigned int granule_Num;   /*number of granule after which it placed*/
} RIO_TXRXM_SERIAL_CG_ENTRY_T;

/*codegroup column entry*/
typedef struct {
    unsigned int                code_Column[RIO_PL_SERIAL_NUMBER_OF_LANES];/* column data (the array size shall be 10x4)*/
    RIO_PLACE_IN_STREAM_T		place; /*describe placement in packet*/
	unsigned int				granule_Num; /* number of granule in packet after which it placed*/
} RIO_TXRX_SERIAL_CG_COLUMN_ENTRY_T;

/*single bit request entry*/
typedef struct {
    RIO_SIGNAL_T				bit_Value;/* granule data*/
    RIO_PLACE_IN_STREAM_T		place; /*describe placement in packet*/
	unsigned int				granule_Num;/* number of granule in packet after which it placed*/
	RIO_PLACE_IN_GRANULE_T		place_In_Granule; /*describe type of placement in granule, selected by place and granule_Num parameters*/
	unsigned int				character_Num;/* number of character in granule selected by place and granule_Num 
	                                            parameters, which is the object of the request*/
	unsigned int				bit_Num; /* number of bit in the said by character_Num parameter character in granule
									selected by place and granule_Num parameters, which is the object of the request*/
}RIO_TXRX_SERIAL_SINGLE_BIT_ENTRY_T;

/*compensation sequence request entry*/
typedef struct {
    RIO_PLACE_IN_STREAM_T		place; /*describe placement in packet*/
	unsigned int				granule_Num;/* number of granule in packet after which it placed*/
}RIO_TXRX_SERIAL_COMP_SEQ_RQ_ENTRY_T;

/*pcs/pma machine managment request buffer entry*/
typedef struct {
    RIO_BOOL_T              set_On_Machine; /*RIO_TRUE if seq must be cont. sent*/
    RIO_PLACE_IN_STREAM_T	place; /*describe placement in packet*/
	unsigned int	    	granule_Num;/*	number of granule in packet after which it placed*/
	RIO_PCS_PMA_MACHINE_T	machine_Type;
}RIO_TXRX_SERIAL_MACHINE_MANAGMENT_ENTRY_T;

/*bit/char pop buffer entry*/
typedef struct {
    RIO_UCHAR_T             lane_Num;
    RIO_PLACE_IN_STREAM_T	place; /*describe placement in packet*/
	unsigned int	    	granule_Num;/*	number of granule in packet after which it placed*/
}RIO_TXRX_SERIAL_POP_ENTRY_T;


/***************************************************************************
 * Function : Create_Buffer_Struct
 *
 * Description: Creates buffer item
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
#define Create_Buffer_Struct(item_Type)\
typedef struct {\
    item_Type                          *buffer;     /* buffer itself */\
    unsigned int                       buffer_Head;/* first full entry*/\
    unsigned int                       buffer_Top; /* next(first) empty entry*/\
    RIO_BOOL_T                         buffer_Full;\
    item_Type                          temp; /*temporary entry for incoming requests*/\
}

/***************************************************************************
 * Function : Initialize_Buffer
 *
 * Description: Make specified buffer empty
 *
 * Returns: Error if buffer is absent
 *
 * Notes: none
 *
 **************************************************************************/
#define Initialize_Buffer(buf_Name) \
{ \
    if ((buf_Name).buffer == NULL) return RIO_ERROR; \
    (buf_Name).buffer_Head = 0; \
    (buf_Name).buffer_Top = 0; \
    (buf_Name).buffer_Full = RIO_FALSE; \
}

/***************************************************************************
 * Function : Pop_Buffer_Item
 *
 * Description: Remove item from the buffer
 *
 * Returns: none
 *
 * Notes: this is a macro
 *
 **************************************************************************/
#define Pop_Buffer_Item(buf, buf_Size)\
{\
    if ( (buf).buffer_Head != (buf).buffer_Top  || (buf).buffer_Full != RIO_FALSE)\
    {\
        (buf).buffer_Head++;\
        if ((buf).buffer_Head >= (buf_Size)) (buf).buffer_Head -= (buf_Size);\
        (buf).buffer_Full = RIO_FALSE;\
    }\
}


Create_Buffer_Struct(RIO_TXRXM_PACKET_OUT_ENTRY_T)RIO_TXRXM_SERIAL_OUT_PACKET_BUFFER_T;
Create_Buffer_Struct(RIO_TXRXM_SERIAL_SYMBOL_OUT_ENTRY_T)RIO_TXRXM_SERIAL_OUT_SYMBOL_BUFFER_T;
Create_Buffer_Struct(RIO_TXRXM_SERAIL_GRANULE_OUT_ENTRY_T)RIO_TXRXM_SERIAL_OUT_GRANULE_BUFFER_T;
Create_Buffer_Struct(RIO_TXRXM_SERIAL_CHAR_OUT_ENTRY_T)RIO_TXRXM_SERIAL_OUT_CHAR_BUFFER_T;
Create_Buffer_Struct(RIO_TXRXM_SERIAL_CHAR_COLUMN_OUT_ENTRY_T)RIO_TXRXM_SERIAL_OUT_CHAR_COLUMN_BUFFER_T;
Create_Buffer_Struct(RIO_TXRXM_SERIAL_SINGLE_CHAR_ENTRY_T)RIO_TXRXM_SERIAL_OUT_SINGLE_CHAR_BUFFER_T;
Create_Buffer_Struct(RIO_TXRXM_SERIAL_CG_ENTRY_T)RIO_TXRXM_SERIAL_OUT_CG_BUFFER_T;
Create_Buffer_Struct(RIO_TXRX_SERIAL_CG_COLUMN_ENTRY_T)RIO_TXRXM_SERIAL_OUT_CG_COLUMN_BUFFER_T;
Create_Buffer_Struct(RIO_TXRX_SERIAL_SINGLE_BIT_ENTRY_T)RIO_TXRXM_SERIAL_OUT_SINGLE_BIT_BUFFER_T;
Create_Buffer_Struct(RIO_TXRX_SERIAL_COMP_SEQ_RQ_ENTRY_T)RIO_TXRXM_SERIAL_OUT_COMP_SEQ_RQ_BUFFER_T;
Create_Buffer_Struct(RIO_TXRX_SERIAL_MACHINE_MANAGMENT_ENTRY_T)RIO_TXRXM_SERIAL_OUT_MACHINE_MANAGMENT_BUFFER_T;
Create_Buffer_Struct(RIO_TXRX_SERIAL_POP_ENTRY_T)RIO_TXRXM_SERIAL_OUT_POP_BUFFER_T;



/*queues description*/
typedef struct {
    unsigned int                    element_Place_In_Buffer;
    RIO_TXRXM_SERIAL_ELEMENT_T      element_Type;
} RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T;

typedef struct {
    RIO_TXRXM_SERIAL_QUEUE_OUT_ENTRY_T      *queue_Buffer;
    unsigned long                    queue_Head;/* first full entry*/
    unsigned long                    queue_Top; /* next(first) empty entry*/
    RIO_BOOL_T                       queue_Empty;
    unsigned int                     queue_Max_Size;
} RIO_TXRXM_SERIAL_OUT_QUEUE_T;

/*for description of queues see HLD, figure 14*/
typedef struct {
    RIO_BOOL_T                      packet_Is_Sending;
    RIO_BOOL_T                      symbol_Is_Sent;
/*common queue*/    
    RIO_TXRXM_SERIAL_OUT_QUEUE_T    out_Queue;
    RIO_TXRXM_PACKET_ARB_T          outbound_Packet_Arb;
    unsigned int                    out_Ack_Id; /*ackid of the next outbound packet*/
} RIO_TXRXM_SERIAL_OUT_QUEUE1_T;

/*QUEUE2*/
typedef struct {
    RIO_TXRXM_LIST_ITEM_T * list[RIO_PL_SERIAL_CNT_BYTE_IN_GRAN];
    RIO_TXRXM_LIST_ITEM_T * backup[RIO_PL_SERIAL_CNT_BYTE_IN_GRAN]; /*backup pointers for list headers*/
    unsigned int len[RIO_PL_SERIAL_CNT_BYTE_IN_GRAN];    /*lenght of the list*/
    RIO_TXRXM_LIST_ITEM_T * last[RIO_PL_SERIAL_CNT_BYTE_IN_GRAN]; /*pointer to the last element of list*/
    RIO_UCHAR_T curr;       /*number of items in the granule/codegroup which has been sent
                            this is used for 1x only*/
}RIO_TXRXM_SERIAL_ARB_T;

typedef struct {
    RIO_TXRXM_SERIAL_OUT_QUEUE_T    out_Queue;
    RIO_TXRXM_SERIAL_ARB_T  arbiter;
}RIO_TXRXM_SERIAL_OUT_MODULE_T;

/*end of queue description*/

typedef struct {
    RIO_TXRXM_SERIAL_OUT_PACKET_BUFFER_T        packet_Buf;
    RIO_TXRXM_SERIAL_OUT_SYMBOL_BUFFER_T        symbol_Buf;
    RIO_TXRXM_SERIAL_OUT_SYMBOL_BUFFER_T        hp_Symbol_Buf; /*high prio symbols*/
    RIO_TXRXM_SERIAL_OUT_CHAR_BUFFER_T          char_Buf;
    RIO_TXRXM_SERIAL_OUT_CHAR_COLUMN_BUFFER_T   char_Column_Buf;
    RIO_TXRXM_SERIAL_OUT_SINGLE_CHAR_BUFFER_T   single_Char_Buf;
    RIO_TXRXM_SERIAL_OUT_CG_BUFFER_T            cg_Buf;
    RIO_TXRXM_SERIAL_OUT_CG_COLUMN_BUFFER_T     cg_Column_Buf;
    RIO_TXRXM_SERIAL_OUT_SINGLE_BIT_BUFFER_T    single_Bit_Buf;
    RIO_TXRXM_SERIAL_OUT_COMP_SEQ_RQ_BUFFER_T   comp_Seq_Req_Buf;
    RIO_TXRXM_SERIAL_OUT_MACHINE_MANAGMENT_BUFFER_T machine_Managment_Buf;
    RIO_TXRXM_SERIAL_OUT_POP_BUFFER_T           pop_Byte_Buf;
    RIO_TXRXM_SERIAL_OUT_POP_BUFFER_T           pop_Bit_Buf;
} RIO_TXRXM_SERIAL_BUFFERS_T;



/* inbound buffer structure */
typedef struct {
    RIO_BOOL_T                      packet_Is_Receiving;
    RIO_TXRXM_PACKET_ARB_T          inbound_Arb;
    unsigned int                    pop_Bit_Requests;
    /*storage for granules column for reporting about invalid symbol*/
    RIO_PCS_PMA_CODE_GROUP_T        codegroups;
    RIO_BOOL_T                      skip_Granule; /*true if necessary to skip grnaule*/
    /*storage for the next expected ackid of the packet*/
    unsigned int                    in_Ack_Id;

} RIO_TXRXM_SERIAL_IN_DS_T;

/*outbound data structure*/
typedef struct {
    RIO_TXRXM_SERIAL_OUT_QUEUE_T    queue_High_Prio_Symbols;
    RIO_TXRXM_SERIAL_OUT_QUEUE1_T   queue1;
    RIO_TXRXM_SERIAL_OUT_MODULE_T   queue2;
    RIO_TXRXM_SERIAL_OUT_MODULE_T   queue3;

    RIO_TXRXM_SERIAL_BUFFERS_T      out_Bufs;

    int                             idle_Counter;

}RIO_TXRXM_SERIAL_OUT_DS_T;


typedef struct {
    RIO_BOOL_T sync_Enabled;
    RIO_BOOL_T align_Enabled;
    RIO_BOOL_T init_Enabled;
} RIO_TXRXM_SERIAL_MACHINES_STATE_T;


/*data storage for the internal registers*/
typedef struct {
    RIO_UCHAR_T in_Ack_Id;
    RIO_UCHAR_T our_Buf_Status;
    RIO_UCHAR_T parthner_Buf_Status;
    RIO_BOOL_T is_Inp_Err;
    RIO_PL_PACKET_NOT_ACCEPTED_CAUSE_T not_Accepted_Cause;
    /*values of the control csr*/
    RIO_UCHAR_T port_With;
    RIO_UCHAR_T initalized_Port_With;
    RIO_UCHAR_T port_With_Override;

    RIO_UCHAR_T lane_No;  /* number of the lane where invalid character occured */
    RIO_BOOL_T is_Port_4x; /* RIO_TRUE if port is working in 4x mode - Initialized by DP*/
    RIO_UCHAR_T curr_Byte_Tx; /* in case of 1x mode it is a number of the current byte in received granule */
    RIO_UCHAR_T curr_Byte_Rx; /* in case of 1x mode it is a number of the current byte in transmitted granule */
    RIO_BOOL_T are_All_Idles;  /*Indicates that all characters in codegroup column is idles*/
    RIO_BOOL_T is_PD;       /*Indicates type of the symbol*/
    RIO_BOOL_T is_Alignment_Lost;  /* this flag is used to signalize from PM to DP that alignment 
                                    is lost (queues are to be reset) */
    RIO_BOOL_T is_Sync_Lost;  /* this flag is used to signalize from PM to DP that synchronization 
                                    is lost (streaming is to be reset) */
    RIO_BOOL_T is_Character_Invalid; /*this flag is used to signalize from DP to PM that codegroup 
                                    could not be decoded*/
    RIO_BOOL_T is_Copy_To_Lane_2; /*true in case when 2 lane mode is forsed*/
    RIO_BOOL_T supress_Hook;   /* notifies that the corresponding hook 
                                    (codegroup or character received) shall be suppressed*/
    RIO_UCHAR_T initial_Run_Disp;  /* notifies DP CL that first encountered comma on a lane has 
                                    a positive disparity */
    unsigned int decode_Any_Run_Disp;  /* notifies DP CL that there shall be an attempt to 
                                    decode codegroup trying both run.disparities */
    RIO_BOOL_T  incorrect_SC_PD;    
    RIO_BOOL_T  lane_2_Just_Forced;  /* asserted when 4x mode -> to 1x_lane2 mode 
                                        by discovery timer while in SYNC FSM */
    RIO_UCHAR_T  first_Char_Val_Rx;   /*value of the first character in the symbol*/
    unsigned int packet_Finishing_Cnt; /*size of characters that will be sent before the 
                                         last granule of packet will be requested for sending*/
} RIO_TXRXM_SERIAL_REGISTERS_T;

typedef struct {
    unsigned long                   instance_Number;
    RIO_BOOL_T                      in_Reset;
        /*parameters which are passed to the create instance routine*/
    RIO_TXRXM_SERIAL_INST_PARAM_T   inst_Param_Set;
        /*true if instance is bound */
    RIO_BOOL_T                      is_Bound;
        /*parameters which are passed to the init routine*/
    RIO_TXRXM_SERIAL_PARAM_SET_T    initialization_Param_Set; 
    /*input/output data for packet and other items sending*/
    RIO_TXRXM_SERIAL_OUT_DS_T       outbound_Data;
    RIO_TXRXM_SERIAL_IN_DS_T        inbound_Data;
    /*pcs/pma layer data*/
    RIO_TXRXM_SERIAL_MACHINES_STATE_T mach_State; /*state of the model's machines*/
    RIO_HANDLE_T                    pm; /*protocol manager*/
    RIO_HANDLE_T                    dp; /*data processor*/
    RIO_PCS_PMA_DP_FTRAY_T          dp_Interfaces;
    RIO_PCS_PMA_PM_FTRAY_T          pm_Interfaces;
    /*external interfaces*/
    RIO_TXRXM_SERIAL_MODEL_CALLBACK_TRAY_T  ext_Interfaces;
    RIO_TXRXM_SERIAL_MODEL_INITIALIZE       restart_Function;
    RIO_TXRXM_MODEL_START_RESET             reset_Function;
    /*model's registers*/
    RIO_TXRXM_SERIAL_REGISTERS_T regs;
    /*routines for access to the model's registers*/
    RIO_GET_STATE                   get_State;
    RIO_SET_STATE                   set_State;

} RIO_TXRXM_SERIAL_DS_T;


/*Defines for work with the strucuture*/

/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP
 *
 * Description: return address of an outbound packet buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_PACKET_TEMP(handle) ((handle)->outbound_Data.out_Bufs.packet_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_OUT_GET_SYMBOL_TEMP
 *
 * Description: return address of an outbound symbol buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_SYMBOL_TEMP(handle) ((handle)->outbound_Data.out_Bufs.symbol_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP
 *
 * Description: return address of an outbound character buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP(handle) ((handle)->outbound_Data.out_Bufs.char_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_CHAR_TEMP
 *
 * Description: return address of an outbound characters column buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_CHAR_COLUMN_TEMP(handle) ((handle)->outbound_Data.out_Bufs.char_Column_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP
 *
 * Description: return address of an outbound single character buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_SINGLE_CHAR_TEMP(handle) ((handle)->outbound_Data.out_Bufs.single_Char_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_CG_TEMP
 *
 * Description: return address of an outbound codegroup buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_CG_TEMP(handle) ((handle)->outbound_Data.out_Bufs.cg_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_CG_COLUMN_TEMP
 *
 * Description: return address of an outbound codegroups column buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_CG_COLUMN_TEMP(handle) ((handle)->outbound_Data.out_Bufs.cg_Column_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP
 *
 * Description: return address of an outbound single bit buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_SINGLE_BIT_TEMP(handle) ((handle)->outbound_Data.out_Bufs.single_Bit_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_COMP_SEQ_TEMP
 *
 * Description: return address of an outbound compensation sequence buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_COMP_SEQ_TEMP(handle) ((handle)->outbound_Data.out_Bufs.comp_Seq_Req_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_MACHINE_MGMNT_TEMP
 *
 * Description: return address of an outbound machine managment buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_MACHINE_MGMNT_TEMP(handle) ((handle)->outbound_Data.out_Bufs.machine_Managment_Buf.temp)
/***************************************************************************
 * Function : RIO_TXRXM_SERIAL_OUT_GET_POP_TEMP
 *
 * Description: return address of an outbound machine managment buffer entry
 *
 * Returns: outbound entry address
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_SERIAL_OUT_GET_POP_TEMP(handle, buf) ((handle)->outbound_Data.out_Bufs.buf.temp)
#define RIO_TXRXM_SERIAL_POP_BUFFER_SIZE 10




#define RIO_TXRXM_SERIAL_ALL_LANES_USED_MASK 0xF    /*mask for RIO_PCS_PMA_CHARACTER_T type*/    
#define RIO_TXRXM_SERIAL_1_LANE_USED_MASK    0x5    /*mask for RIO_PCS_PMA_CHARACTER_T type*/    

#define RIO_TXRXM_SERIAL_2_LANE_MASK         0x4    /*mask for lane 2 only*/



#define RIO_TXRXM_SERIAL_BITS_IN_CODEGROUP   10

#define RIO_TXRXM_LANE_0  0
#define RIO_TXRXM_LANE_1  1
#define RIO_TXRXM_LANE_2  2
#define RIO_TXRXM_LANE_3  3

#define RIO_TXRXM_K_VAL 0xBC
#define RIO_TXRXM_R_VAL 0xFD

#define RIO_STXRX_GRANULE_ACCEPTOR_DO_NOTHING 254 /*value for stub*/ 

#define MAX_MESSAGE_BUF_SIZE    300

/*values of stype1 for sop and eop*/
#define STXRX_STYPE1_SOP        0
#define STXRX_STYPE1_STOMP      1
#define STXRX_STYPE1_EOP        2
#define STXRX_STYPE1_RFR        3
#define STXRX_STYPE1_LRQ        4


/*values for registers intialization*/
#define RIO_STXRXRM_INITIAL_BUF_STATUS      15
#define RIO_STXRXM_PORT_WITH_SLINGLE_LANE   0
#define RIO_STXRXM_PORT_WITH_FOUR_LANE      1

#define RIO_STXRXM_INITIALIZED_PORT_WITH_4  2
#define RIO_STXRXM_INITIALIZED_PORT_WITH_0  0
#define RIO_STXRXM_INITIALIZED_PORT_WITH_2  1

#define RIO_STXRXM_LANE_0_FORSED            2
#define RIO_STXRXM_LANE_2_FORSED            3

#define RIO_STXRXM_ACK_ID_MASK              0x1F

/***************************************************************************
 * Function : CHECK_RESULT
 *
 * Description: Checks that res variable is OK
 *
 * Returns: res if error none if ok
 *
 * Notes: none
 *
 **************************************************************************/
#define CHECK_RESULT if (res != RIO_OK) return res

/****************************************************************************/
#endif /* RIO_TXRXM_DS_H */





