#ifndef RIO_TXRXM_COMMON_RX_H
#define RIO_TXRXM_COMMON_RX_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_common_rx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model transmitter header
*
* Notes:        
*
******************************************************************************/
#include "rio_txrxm_common_ds.h"

unsigned int RIO_TxRxM_Check_Ftype(RIO_TXRXM_PACKET_ARB_T *arbiter);
unsigned int RIO_TxRxM_Check_Tt(RIO_TXRXM_PACKET_ARB_T *arbiter);
unsigned int RIO_TxRxM_Check_Ttype(RIO_TXRXM_PACKET_ARB_T *arbiter);
unsigned int RIO_TxRxM_Check_CRC(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16,
    unsigned short *expected_Crc);

unsigned int RIO_TxRxM_Get_CRC_Start(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

unsigned int RIO_TxRxM_Check_Header_Size(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

unsigned int RIO_TxRxM_Get_Header_Size(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

unsigned int RIO_TxRxM_Check_Payload_Align(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

unsigned int RIO_TxRxM_Check_Reserved_Fields(
    RIO_TXRXM_PACKET_ARB_T *arbiter, 
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16,
    RIO_BOOL_T is_Parallel);

RIO_BOOL_T RIO_TxRxM_Is_Req(unsigned int ftype, unsigned int ttype);

void RIO_TxRxM_In_Stream_To_Dw(RIO_DW_T *dws, RIO_UCHAR_T *stream, unsigned int size);

void RIO_TxRxM_Prod_Req_8(
    RIO_TXRXM_PACKET_ARB_T  *arbiter,
    RIO_REMOTE_REQUEST_T *req,
    RIO_UCHAR_T          *hop_Count);

void RIO_TxRxM_Prod_Req_10(RIO_TXRXM_PACKET_ARB_T  *arbiter, RIO_REMOTE_REQUEST_T *req);

void RIO_TxRxM_Prod_Req_11(RIO_TXRXM_PACKET_ARB_T  *arbiter, RIO_REMOTE_REQUEST_T *req);

void RIO_TxRxM_Prod_Req_2(
    RIO_TXRXM_PACKET_ARB_T  *arbiter, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

void RIO_TxRxM_Prod_Req_5(
    RIO_TXRXM_PACKET_ARB_T  *arbiter, 
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

void RIO_TxRxM_Prod_Req_6(
    RIO_TXRXM_PACKET_ARB_T  *arbiter,
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

void prod_Req_1(
    RIO_TXRXM_PACKET_ARB_T  *arbiter,
    RIO_REMOTE_REQUEST_T *req,
    RIO_BOOL_T is_Ext_Address_Valid,
    RIO_BOOL_T is_Ext_Address_16);

void RIO_TxRxM_In_Form_Resp(
    RIO_TXRXM_PACKET_ARB_T  *arbiter, 
    RIO_RESPONSE_T    *ext_Resp,
    RIO_UCHAR_T       *ack_ID,
    RIO_UCHAR_T       *hop_Count,
    RIO_BOOL_T        is_Parallel
    );


unsigned int RIO_TxRxM_Get_Ttype(RIO_TXRXM_PACKET_ARB_T *arbiter);
#define get_Ttype RIO_TxRxM_Get_Ttype
unsigned int RIO_TxRxM_Get_Tt(RIO_TXRXM_PACKET_ARB_T *arbiter);
#define get_Tt RIO_TxRxM_Get_Tt
unsigned int RIO_TxRxM_Get_Ftype(RIO_TXRXM_PACKET_ARB_T *arbiter);
#define get_Ftype RIO_TxRxM_Get_Ftype
unsigned int RIO_TxRxM_Get_Prio(RIO_TXRXM_PACKET_ARB_T *arbiter);
#define get_Prio RIO_TxRxM_Get_Prio
unsigned long RIO_TxRxM_Get_Transp_Info(RIO_TXRXM_PACKET_ARB_T *arbiter);
#define get_Transp_Info RIO_TxRxM_Get_Transp_Info
unsigned int RIO_TxRxM_Get_Ack_ID_Parallel(RIO_TXRXM_PACKET_ARB_T *arbiter);
#define get_Ack_ID RIO_TxRxM_Get_Ack_ID_Parallel
/****************************************************************************/
#endif /* RIO_TXRXM_COMMON_RX_H */
