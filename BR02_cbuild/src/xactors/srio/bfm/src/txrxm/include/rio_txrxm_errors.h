#ifndef RIO_TXRXM_ERRORS_H
#define RIO_TXRXM_ERRORS_H
/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola SPSD
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/include/rio_txrxm_errors.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    txrx model errors/warnings handlers prototypes
*               and data 
* Notes:        
*
******************************************************************************/
#ifndef RIO_TXRXM_DS_H
#include "rio_txrxm_ds.h"
#endif     /*RIO_TXRXM_DS_H*/

/*
 * this type syblically represent error codes which are used as
 * indexes in error messages array
 */
typedef enum {
    RIO_TXRXM_ERROR_1 = 0,    /* 32 bit txrx errors */
    
    RIO_TXRXM_WARNING_1,  /* 32 bit tx/rx warnings */
        
    RIO_TXRXM_NOTE_1     /* 32 bit txrx notes */

} RIO_TXRXM_MESSAGES_T;

/*
 * this function write an warning message with corresponding explaning
 * based on instanse's state and a set of additional parameters
 */
int RIO_TxRxM_Print_Message(RIO_TXRXM_DS_T* handle, RIO_TXRXM_MESSAGES_T warning_Type, ...);


/****************************************************************************/
#endif /* RIO_TXRXM_ERRORS_H */



