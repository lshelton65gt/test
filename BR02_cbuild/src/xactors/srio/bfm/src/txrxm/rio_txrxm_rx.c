/******************************************************************************
*
*       COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/riom/txrxm/rio_txrxm_rx.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the ClearCase command "History"
*                to display revision history information.
*
* Description:    TxRx model receiver source
*
* Notes:        
*
******************************************************************************/
#include <stdio.h>
#include <string.h>

#include "rio_txrxm_rx.h"
#include "rio_txrxm_tx.h"
#include "rio_txrxm_common_rx.h"

/*constants for source ID extraction*/
#define RIO_TXRX_8_BIT_DEV_ID_MASK    0xFF
#define RIO_TXRX_16_BIT_DEV_ID_MASK   0xFFFF


/***************************************************************************
 * Function : RIO_TXRXM_IN_GET_ARB
 *
 * Description: return outbound arbiter
 *
 * Returns: outbound arbiter
 *
 * Notes: none
 *
 **************************************************************************/
#define RIO_TXRXM_IN_GET_ARB(handle) ((handle)->inbound_Data.inbound_Arb)

/*stub to invoke callback on received training*/
static void handler_Granule(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule);

/*stub to invoke callback on received training*/
static void handler_Training(
    RIO_TXRXM_DS_T *handle,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num
    );

/*packet data handler*/
static void handler_Data(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T  *granule
    );

/*finishes packet receiving*/
static void finish_Packet(RIO_TXRXM_DS_T *handle);

/*stub to invoke request received or response received external callbacks*/
static void parse_Packet(RIO_TXRXM_DS_T *handle);

/*check packet for violations*/
static int in_Check_Current(
    RIO_TXRXM_DS_T *handle,
    RIO_BOOL_T *error_Flag, 
    RIO_PACKET_VIOLATION_T *violation_Type,
    unsigned short *expected_Crc
    );


/*convert stream to request structure*/
static void in_Load_From_Stream(
    RIO_TXRXM_DS_T *handle,
    RIO_REMOTE_REQUEST_T *req_Struct,
    RIO_UCHAR_T *ack_ID,
    RIO_TR_INFO_T *transport_Info,
    RIO_UCHAR_T *hop_Count
    );

/*received symbol processing*/
static void handler_Symbol(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num
    );

/*first packet granule processing*/
static void handler_Start_New_Packet(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T  *granule);

/*violent symbol processing*/
static void handler_Violent_Symbol(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num);

/*cancel receiving packet and invoke external callback*/
static void cancel_Packet(RIO_TXRXM_DS_T *handle);

/*pass violated packet to the environment*/
static void accept_Violated_Packet(RIO_TXRXM_DS_T *handle, RIO_PACKET_VIOLATION_T violation_Type,
    unsigned short expected_Crc);

/***************************************************************************
 * Function : RIO_TxRxM_Granule_Received
 *
 * Description: receiver main loop
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Granule_Received(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule)
{
    unsigned int gran_Num = 0;/*receiving packet current granule num*/
    /*check pointers*/
    if (handle == NULL || granule == NULL)
        return;
    /*form granule and pass to the environment*/
    handler_Granule(handle, granule);
    /*get current granule num in packet*/
    if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
        gran_Num = (unsigned int)((RIO_TXRXM_IN_GET_ARB(handle).cur_Pos - 1)  / RIO_TXRXM_BYTE_CNT_IN_WORD);
    /*perform actions according granule type*/
    switch (granule->granule_Type)
    {
        case RIO_GR_NEW_PACKET:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                finish_Packet(handle);
            }
            handler_Start_New_Packet(handle, granule);
            break;
        case RIO_GR_DATA:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                handler_Data(handle, granule);
            }
            else
                handler_Violent_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_GR_RESTART_FROM_RETRY:
        case RIO_GR_STOMP:
        case RIO_GR_LINK_REQUEST:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                cancel_Packet(handle);
                handler_Symbol(handle, granule, CANCELING, gran_Num);
                break;
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_GR_EOP:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                finish_Packet(handle);
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_GR_PACKET_ACCEPTED:
        case RIO_GR_PACKET_RETRY:
        case RIO_GR_PACKET_NOT_ACCEPTED:
        case RIO_GR_IDLE:
        case RIO_GR_THROTTLE:
        case RIO_GR_TOD_SYNC:
        case RIO_GR_LINK_RESPONSE:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                handler_Symbol(handle, granule, EMBEDDED, gran_Num);
                break;
            }
            handler_Symbol(handle, granule, FREE, 0);
            break;
        case RIO_GR_TRAINING:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                handler_Training(handle, EMBEDDED, gran_Num);
                break;
            }
            handler_Training(handle, FREE, 0);
            break;
        case RIO_GR_HAS_BEEN_CORRUPTED:
        case RIO_GR_UNRECOGNIZED:
        case RIO_GR_S_BIT_CORRUPTED:
        case RIO_GR_CORRUPTED_TRAINING:
            if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
            {
                handler_Violent_Symbol(handle, granule, EMBEDDED, gran_Num);
                break;
            }
            handler_Violent_Symbol(handle, granule, FREE, 0);
            break;
        default:
            break;
    }
    return;
}
/***************************************************************************
 * Function : handler_Granule
 *
 * Description: stub to invoke callback on received granule
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Granule(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T *granule)
{

    unsigned int granule_Temp, i;

    /*check pointers*/
    if (handle == NULL || granule == NULL)  
        return;
                
    /*check external callback*/
    if (handle->ext_Interfaces.rio_TxRxM_Granule_Received == NULL)
        return;

    /*form granule*/
    granule_Temp = 0;
    for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
    {
        granule_Temp = granule_Temp << RIO_TXRXM_BITS_IN_BYTE ;
        granule_Temp |= granule->granule_Date[i];
    };

    /*invoke callback to pass granule*/
    handle->ext_Interfaces.rio_TxRxM_Granule_Received(handle->ext_Interfaces.txrxm_Interface_Context,
        granule->granule_Type, granule_Temp, granule->granule_Frame);
    return;    
}

/***************************************************************************
 * Function : handler_Training
 *
 * Description: stub to invoke callback on received training
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Training(
    RIO_TXRXM_DS_T *handle,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num)
{
    /*check pointers*/
    if (handle == NULL)  
        return;
                
    /*check external callback*/
    if (handle->ext_Interfaces.rio_TxRxM_Training_Received == NULL)
        return;

    /*invoke callback to pass granule*/
    handle->ext_Interfaces.rio_TxRxM_Training_Received(
        handle->ext_Interfaces.txrxm_Interface_Context,
        place,
        granule_Num
        );

    return;    
}

/***************************************************************************
 * Function : handler_Data
 *
 * Description: packet data handler
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Data(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T  *granule)
{
    /*help variables*/
    unsigned int i;
    RIO_TXRXM_PACKET_ARB_T* arbiter;
    /*check pointers*/
    if (handle == NULL || granule == NULL)  
        return;

    arbiter = &RIO_TXRXM_IN_GET_ARB(handle);
    
    for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
    {
        arbiter->packet_Stream[arbiter->cur_Pos] = granule->granule_Date[i];
        arbiter->cur_Pos++;
        if (arbiter->cur_Pos == RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN)
        {
            finish_Packet(handle);
            break;
        }
    }
    
    return;    
}

/***************************************************************************
 * Function : finish_Packet
 *
 * Description: finishes packet receiving
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void finish_Packet(RIO_TXRXM_DS_T *handle)
{
    RIO_BOOL_T error_Flag;
    RIO_PACKET_VIOLATION_T violation_Type;
    unsigned short expected_Crc = RIO_TXRXM_INIT_CRC_VALUE;
    int ret_Val;

    /*check pointers*/
    if (handle == NULL)  
        return;

    if ((ret_Val = in_Check_Current(handle, &error_Flag, &violation_Type, &expected_Crc)) == RIO_OK)
    {
        if (error_Flag == RIO_FALSE)
            parse_Packet(handle);
        else
            accept_Violated_Packet(handle, violation_Type, expected_Crc);

    }   
    handle->inbound_Data.packet_Is_Receiving = RIO_FALSE;
    return;    
}

/***************************************************************************
 * Function : parse_Packet
 *
 * Description: try to parse received packet and invoke external callbacks
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void parse_Packet(RIO_TXRXM_DS_T *handle)
{
    unsigned int ftype, ttype, prio;

    if (handle == NULL)
        return;

    ftype = get_Ftype(&RIO_TXRXM_IN_GET_ARB(handle));
    ttype = get_Ttype(&RIO_TXRXM_IN_GET_ARB(handle));
    prio = get_Prio(&RIO_TXRXM_IN_GET_ARB(handle));

    if (RIO_TxRxM_Is_Req(ftype, ttype) == RIO_TRUE)
    {
        if (handle->ext_Interfaces.rio_TxRxM_Request_Received != NULL)
        {
            /*create new packet structure*/
            RIO_REMOTE_REQUEST_T req_Struct;
            RIO_REQ_HEADER_T     header;
            RIO_DW_T             payload[RIO_TXRXM_MAX_PAYLOAD_SIZE];
            RIO_UCHAR_T          ack_ID;
            RIO_TR_INFO_T        transport_Info;
            RIO_UCHAR_T          hop_Count;

            req_Struct.header = &header;
            req_Struct.data = payload;
            /*load request from stream*/
            in_Load_From_Stream(handle, &req_Struct, &ack_ID, &transport_Info, &hop_Count);
            /*invoke external callback*/
            handle->ext_Interfaces.rio_TxRxM_Request_Received(handle->ext_Interfaces.txrxm_Interface_Context,
                &req_Struct, ack_ID, transport_Info, hop_Count);
        }
    }
    else
    {
        if (handle->ext_Interfaces.rio_TxRxM_Response_Received != NULL)
        {
            /* here structures to pass to the environment */
            RIO_RESPONSE_T        ext_Resp;
            RIO_DW_T              payload[RIO_TXRXM_MAX_PAYLOAD_SIZE];
            RIO_UCHAR_T           ack_ID;
            RIO_UCHAR_T           hop_Count;

            ext_Resp.data = payload;
            /*form response structure*/
            RIO_TxRxM_In_Form_Resp(&RIO_TXRXM_IN_GET_ARB(handle), 
                &ext_Resp, &ack_ID, &hop_Count, RIO_TRUE);
            /*invoke external callback*/
            handle->ext_Interfaces.rio_TxRxM_Response_Received(
                handle->ext_Interfaces.txrxm_Interface_Context,
                &ext_Resp, ack_ID, hop_Count);
        }
    }
    RIO_TxRxM_Out_Init_Arbiter(&RIO_TXRXM_IN_GET_ARB(handle));
}

/***************************************************************************
 * Function : in_Check_Current
 *
 * Description: check receiving packet stream if it could be parsered
 *
 * Returns: error code 
 *
 * Notes: none
 *
 **************************************************************************/
static int in_Check_Current(
    RIO_TXRXM_DS_T *handle,
    RIO_BOOL_T *error_Flag, 
    RIO_PACKET_VIOLATION_T *violation_Type,
    unsigned short *expected_Crc)
{
    int check_Result;

    RIO_TXRXM_PACKET_ARB_T* arbiter;

    if (handle == NULL || violation_Type == NULL)
        return RIO_ERROR; /*internal error*/

    arbiter = &RIO_TXRXM_IN_GET_ARB(handle);

    /*check if packet is greater than 276 bytes*/
    if (arbiter->cur_Pos > RIO_TXRXM_MAX_PROTOCOL_PACKET_LEN)
    {
        *error_Flag = RIO_TRUE;
        *violation_Type = PACKET_IS_LONGER_276;
        return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Ftype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = FTYPE_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Tt(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TT_RESERVED;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_CRC(arbiter,
        handle->parameters_Set.in_Ext_Address_Valid,
        handle->parameters_Set.in_Ext_Address_16, 
        expected_Crc)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = CRC_ERROR;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Header_Size(arbiter,
        handle->parameters_Set.in_Ext_Address_Valid,
        handle->parameters_Set.in_Ext_Address_16)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = INCORRECT_PACKET_HEADER;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Payload_Align(arbiter,
        handle->parameters_Set.in_Ext_Address_Valid,
        handle->parameters_Set.in_Ext_Address_16)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = PAYLOAD_NOT_DW_ALIGNED;
            return RIO_OK;
    }
    
    

    
    /*check physical reserved field value*/
    if ((check_Result = RIO_TxRxM_Check_Reserved_Fields(arbiter,
        handle->parameters_Set.in_Ext_Address_Valid,
        handle->parameters_Set.in_Ext_Address_16,
        RIO_TRUE)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = NON_ZERO_RESERVED_PACKET_BITS_VALUE;
            return RIO_OK;
    }

    if ((check_Result = RIO_TxRxM_Check_Ttype(arbiter)) == RIO_ERROR)
    {
            *error_Flag = RIO_TRUE;
            *violation_Type = TTYPE_RESERVED;
            return RIO_OK;
    }

    *error_Flag = RIO_FALSE;
    return RIO_OK;
}


/***************************************************************************
 * Function : in_Load_From_Stream
 *
 * Description: load inbound buffer entry from the packet stream
 *
 * Returns: none 
 *
 * Notes: none
 *
 **************************************************************************/
static void in_Load_From_Stream(
    RIO_TXRXM_DS_T *handle,
    RIO_REMOTE_REQUEST_T *req_Struct,
    RIO_UCHAR_T          *ack_ID,
    RIO_TR_INFO_T        *transport_Info,
    RIO_UCHAR_T          *hop_Count)
{
    if (handle == NULL ||
        req_Struct == NULL ||
        ack_ID == NULL ||
        transport_Info == NULL ||
        hop_Count == NULL || 
        req_Struct->header == NULL)
        return;

    /* locate entry */
     /* form entry fields */
    *ack_ID = get_Ack_ID(&RIO_TXRXM_IN_GET_ARB(handle));
    req_Struct->packet_Type = req_Struct->header->format_Type = 
        get_Ftype(&RIO_TXRXM_IN_GET_ARB(handle));
    req_Struct->header->prio = get_Prio(&RIO_TXRXM_IN_GET_ARB(handle));
    req_Struct->header->ttype = get_Ttype(&RIO_TXRXM_IN_GET_ARB(handle));
    req_Struct->transp_Type = get_Tt(&RIO_TXRXM_IN_GET_ARB(handle));
    *transport_Info = get_Transp_Info(&RIO_TXRXM_IN_GET_ARB(handle));
    req_Struct->header->src_ID = req_Struct->transp_Type == RIO_PL_TRANSP_16 ?
        (*transport_Info) & RIO_TXRX_8_BIT_DEV_ID_MASK : (*transport_Info) & RIO_TXRX_16_BIT_DEV_ID_MASK;


    /* parse the stream to the structure */
    switch(req_Struct->packet_Type)
    {
        case RIO_MAINTENANCE:
            RIO_TxRxM_Prod_Req_8(&RIO_TXRXM_IN_GET_ARB(handle), 
                req_Struct, hop_Count);
            break;

        case RIO_DOORBELL:
            RIO_TxRxM_Prod_Req_10(&RIO_TXRXM_IN_GET_ARB(handle), req_Struct);
            break;

        case RIO_MESSAGE:
            RIO_TxRxM_Prod_Req_11(&RIO_TXRXM_IN_GET_ARB(handle), req_Struct);
            break;

        case RIO_NONINTERV_REQUEST:
            RIO_TxRxM_Prod_Req_2(&RIO_TXRXM_IN_GET_ARB(handle),
                req_Struct,
                handle->parameters_Set.in_Ext_Address_Valid,
                handle->parameters_Set.in_Ext_Address_16);
            break;

        case RIO_WRITE:
            RIO_TxRxM_Prod_Req_5(&RIO_TXRXM_IN_GET_ARB(handle),
                req_Struct,
                handle->parameters_Set.in_Ext_Address_Valid,
                handle->parameters_Set.in_Ext_Address_16);
            break;

        case RIO_STREAM_WRITE:
            RIO_TxRxM_Prod_Req_6(&RIO_TXRXM_IN_GET_ARB(handle),
                req_Struct,
                handle->parameters_Set.in_Ext_Address_Valid,
                handle->parameters_Set.in_Ext_Address_16);
            break;

        case RIO_INTERV_REQUEST:
            prod_Req_1(&RIO_TXRXM_IN_GET_ARB(handle),
                req_Struct,
                handle->parameters_Set.in_Ext_Address_Valid,
                handle->parameters_Set.in_Ext_Address_16);
            break;

    }

    return;    
}


/***************************************************************************
 * Function : accept_Violated_Packet
 *
 * Description: form packet array and invoke external callback
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void accept_Violated_Packet(RIO_TXRXM_DS_T *handle,  
                                   RIO_PACKET_VIOLATION_T violation_Type,
                                   unsigned short expected_Crc)
{
    /*structures to hold passing packets*/
    RIO_UCHAR_T packet_Buffer[RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN];
    unsigned int packet_Size, i;

    if (handle == NULL)
        return;

    /*try to pass packet*/
    if (handle->ext_Interfaces.rio_TxRxM_Violent_Packet_Received != NULL)
    {
        /*fill passing data*/
        packet_Size = RIO_TXRXM_IN_GET_ARB(handle).cur_Pos;
        for (i = 0; i < packet_Size; i++)
            packet_Buffer[i] = RIO_TXRXM_IN_GET_ARB(handle).packet_Stream [i];

        /*invoke callback*/
        handle->ext_Interfaces.rio_TxRxM_Violent_Packet_Received(
            handle->ext_Interfaces.txrxm_Interface_Context,
            packet_Buffer, packet_Size, violation_Type, expected_Crc);

    }

    /*init arbiter */
    RIO_TxRxM_Out_Init_Arbiter(&RIO_TXRXM_IN_GET_ARB(handle));
    return;
}

/***************************************************************************
 * Function : cancel_Packet
 *
 * Description: form packet array to pass canceled packet
 *              and invoke external callback
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void cancel_Packet(RIO_TXRXM_DS_T *handle)
{
    /*structures to hold passing packets*/
    RIO_UCHAR_T packet_Buffer[RIO_TXRXM_OUT_MAX_PACK_STREAM_LEN];
    unsigned int packet_Size, i;

    if (handle == NULL)
        return;

    /*try to pass packet*/
    if (handle->ext_Interfaces.rio_TxRxM_Canceled_Packet_Received != NULL)
    {
        /*fill passing data*/
        packet_Size = RIO_TXRXM_IN_GET_ARB(handle).cur_Pos;
        for (i = 0; i < packet_Size; i++)
            packet_Buffer[i] = RIO_TXRXM_IN_GET_ARB(handle).packet_Stream [i];

        /*invoke callback*/
        handle->ext_Interfaces.rio_TxRxM_Canceled_Packet_Received(
            handle->ext_Interfaces.txrxm_Interface_Context,
            packet_Buffer, packet_Size);

    }

    handle->inbound_Data.packet_Is_Receiving = RIO_FALSE;
    /*init arbiter */
    RIO_TxRxM_Out_Init_Arbiter(&RIO_TXRXM_IN_GET_ARB(handle));
    return;
}
/***************************************************************************
 * Function : handler_Violent_Symbol
 *
 * Description: actions when violent control symbol received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Violent_Symbol(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num)
{

    if (handle == NULL || granule == NULL)  
        return;
                
    /* 
     * 1) if the receiving process is in progress, check if current packet shall be canceled or finished at first,
     * 2) form the appropriate symbol structure and pass it to the environment  
     */

    if (handle->ext_Interfaces.rio_TxRxM_Violent_Symbol_Received != NULL)
    {    
        /*form symbol struct*/
        unsigned int symbol, i;
        RIO_SYMBOL_VIOLATION_T violation;

        /*get violation type*/
        switch (granule->granule_Type)
        {
            case RIO_GR_HAS_BEEN_CORRUPTED:
                violation = WRONG_INVERSION;
                break;
            case RIO_GR_S_BIT_CORRUPTED:
                violation = WRONG_S_PARITY;
                break;
            case RIO_GR_CORRUPTED_TRAINING:
                violation = CORRUPTED_TRAINING;
                break;
            case RIO_GR_UNRECOGNIZED:
                if (granule->granule_Struct.secded != 0)
                    violation = NON_ZERO_RESERVED_SYMBOL_BITS_VALUE;
                else
                    violation = RESERVED_FIELDS_ENCODINGS;
                break;
            case RIO_GR_DATA:
                violation = NO_FRAME_TOGGLE;
                break;
            default:
                violation = NO_FRAME_TOGGLE;
                break;
        }
        /*convert granule array to value*/
        symbol = 0;
        for (i = 0; i < RIO_PL_CNT_BYTE_IN_GRAN; i++)
        {
            symbol = symbol << RIO_TXRXM_BITS_IN_BYTE;
            symbol |= granule->granule_Date[i];
        }

        handle->ext_Interfaces.rio_TxRxM_Violent_Symbol_Received(
            handle->ext_Interfaces.txrxm_Interface_Context,
            symbol, place, gran_Num, violation);
    };

    return;    
}
/***************************************************************************
 * Function : handle_Symbol
 *
 * Description: actions when control symbol received
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Symbol(
    RIO_TXRXM_DS_T *handle,
    RIO_GRANULE_T *granule,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int gran_Num)
{

    if (handle == NULL || granule == NULL)  
        return;
                
    /* 
     * 1) if the receiving process is in progress, check if current packet shall be canceled or finished at first,
     * 2) form the appropriate symbol structure and pass it to the environment  
     */

    if (handle->ext_Interfaces.rio_TxRxM_Symbol_Received != NULL)
    {    
        /*form symbol struct*/
        /*hardcoded numbers are unique and corresponds to symbol structure in RIO spec.
        Shall be changed only here*/
        
        RIO_SYMBOL_STRUCT_T symbol;
        symbol.s = (RIO_UCHAR_T) granule->granule_Struct.s;
        symbol.ackID = (RIO_UCHAR_T) granule->granule_Struct.ack_ID;
        symbol.rsrv_1 = (RIO_UCHAR_T)((granule->granule_Struct.secded & 0x10) >> 4);
        symbol.s_Parity = (RIO_UCHAR_T)((granule->granule_Struct.secded & 0x08) >> 3);
        symbol.rsrv_2 = (RIO_UCHAR_T)(granule->granule_Struct.secded & 0x07);
        symbol.buf_Status = (RIO_UCHAR_T) granule->granule_Struct.buf_Status;
        symbol.stype = (RIO_UCHAR_T) granule->granule_Struct.stype;

        handle->ext_Interfaces.rio_TxRxM_Symbol_Received(
            handle->ext_Interfaces.txrxm_Interface_Context,
            &symbol, place, gran_Num);
        
    }
    return;
}
/***************************************************************************
 * Function : handler_Start_New_Packet
 *
 * Description: packet data handler
 *
 * Returns: none
 *
 * Notes: none
 *
 **************************************************************************/
static void handler_Start_New_Packet(RIO_TXRXM_DS_T *handle, RIO_GRANULE_T  *granule)
{
    /*help variables*/
    unsigned int i;
    RIO_TXRXM_PACKET_ARB_T* arbiter;
    /*check pointers*/
    if (handle == NULL || granule == NULL)  
        return;

    /*if anouther packet was currently receiving, itv shall be finished*/
    if (handle->inbound_Data.packet_Is_Receiving == RIO_TRUE)
        finish_Packet(handle);

    /*get arbiter*/
    arbiter = &RIO_TXRXM_IN_GET_ARB(handle);

    /*init arbiter*/
    RIO_TxRxM_Out_Init_Arbiter(arbiter);

    /*put first data granule*/
    for (i = 0; i < RIO_TXRXM_BYTE_CNT_IN_WORD; i++)
    {
        arbiter->packet_Stream[arbiter->cur_Pos] = granule->granule_Date[i];
        arbiter->cur_Pos++;
    }
    handle->inbound_Data.packet_Is_Receiving = RIO_TRUE;
    return;    
}

/***************************************************************************
 * Function : RIO_TxRxM_Rx_Init
 *
 * Description: init receiver
 *
 * Returns: error code
 *
 * Notes: none
 *
 **************************************************************************/
void RIO_TxRxM_Rx_Init(RIO_TXRXM_DS_T *handle)
{
   /*check pointer*/
    if (handle == NULL)
        return;
    /*init packet arbiter*/
    RIO_TxRxM_Out_Init_Arbiter(&RIO_TXRXM_IN_GET_ARB(handle));     
    /*init data flag*/
    handle->inbound_Data.packet_Is_Receiving = RIO_FALSE;
    return;
}
