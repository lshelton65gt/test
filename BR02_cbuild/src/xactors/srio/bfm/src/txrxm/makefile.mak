#******************************************************************************
#*
#* COPYRIGHT 2002-2004 MOTOROLA, ALL RIGHTS RESERVED
#*
#* This code is the property of Motorola St. Petersburg Software Development
#* and is Motorola Confidential Proprietary Information.
#*
#* The copyright notice above does not evidence any actual or intended
#* publication of such source code.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/src/txrxm/makefile.mak,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile builds relocatable object file for physical layer model
#

include ../../verilog_env/demo/debug.inc

CC              =   gcc
TARGET          =   txrxm.o
PAR_TARGET      =   ptxrxm.o
SERIAL_TARGET   =   stxrxm.o


FLEXLM_PATH      =   ../../../flexlm

DEBUG		=   -DNDEBUG

FLEXLM_DEFINES  =

DEFINES         =   $(DEBUG) $(FLEXLM_DEFINES) $(_DEBUG)

LIBS            =

LIBS_FLEXLM     =   $(FLEXLM_PATH)/sun4_u5/lm_new.o -L $(FLEXLM_PATH)/sun4_u5 -l lmgr -l crvs -l sb -lsocket

INCLUDES        =   -I$(FLEXLM_PATH)/machind


CFLAGS          =   -c $(prof) $(DEFINES) -elf -ansi -pedantic -Wall -O3
LFLAGS          =   $(prof) -r

OBJS            =   ../txrx/rio_32_txrx_model.o \
        rio_txrxm_common_rx.o rio_txrxm_common_tx.o rio_txrxm_errors.o rio_txrxm_rx.o rio_txrxm_serial_rx.o \
        rio_txrxm_serial_tx.o rio_txrxm_tx.o txrxm_parallel_model.o txrxm_serial_model.o\
../pcs_pma/rio_c2char.o \
../pcs_pma/rio_char2c.o \
../pcs_pma/rio_pcs_pma.o \
../pcs_pma/rio_pcs_pma_dp.o \
../pcs_pma/rio_pcs_pma_dp_cl.o \
../pcs_pma/rio_pcs_pma_dp_pins_driver.o \
../pcs_pma/rio_pcs_pma_dp_sb.o \
../pcs_pma/rio_pcs_pma_pm.o \
../pcs_pma/rio_pcs_pma_pm_align.o \
../pcs_pma/rio_pcs_pma_pm_init.o \
../pcs_pma/rio_pcs_pma_pm_sync.o \
../parallel_init/rio_parallel_init.o 

PARALLEL_OBJS  =   ../txrx/rio_32_txrx_model.o \
        rio_txrxm_common_rx.o rio_txrxm_common_tx.o rio_txrxm_errors.o rio_txrxm_rx.o\
        rio_txrxm_tx.o txrxm_parallel_model.o\
        ../parallel_init/rio_parallel_init.o 
        
SERIAL_OBJS    =  rio_txrxm_common_rx.o rio_txrxm_common_tx.o rio_txrxm_serial_rx.o \
        rio_txrxm_serial_tx.o txrxm_serial_model.o\
../pcs_pma/rio_c2char.o \
../pcs_pma/rio_char2c.o \
../pcs_pma/rio_pcs_pma.o \
../pcs_pma/rio_pcs_pma_dp.o \
../pcs_pma/rio_pcs_pma_dp_cl.o \
../pcs_pma/rio_pcs_pma_dp_pins_driver.o \
../pcs_pma/rio_pcs_pma_dp_sb.o \
../pcs_pma/rio_pcs_pma_pm.o \
../pcs_pma/rio_pcs_pma_pm_align.o \
../pcs_pma/rio_pcs_pma_pm_init.o \
../pcs_pma/rio_pcs_pma_pm_sync.o \
../pcs_pma_adapter/rio_pcs_pma_adapter.o 


###############################################################################
# print the usage info
all                 :
	            @echo ""
	            @echo "Insufficient number of arguments."
	            @echo "To build the txrx relocatable object file, execute:"
	            @echo "    make txrx_model"
	            @echo "To clean the physical layer data, execute:"
	            @echo "    make clean"

# make target
$(TARGET) :	$(OBJS)
		ld  $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

all_flexlm :	$(OBJS)
		ld  $(LFLAGS) $(OBJS) $(LIBS_FLEXLM) $(LIBS) -o $(TARGET)
		


$(PAR_TARGET) : $(PARALLEL_OBJS)
		ld  $(LFLAGS) $(PARALLEL_OBJS) $(LIBS) -o $(PAR_TARGET)

$(SERIAL_TARGET) : $(SERIAL_OBJS)
		ld  $(LFLAGS) $(SERIAL_OBJS) $(LIBS) $(LIBS_FLEXLM) -o $(SERIAL_TARGET)



#clean data
clean :		
		rm -f ./*.o
		rm -f $(TARGET)
		rm -f $(PAR_TARGET)
		rm -f ../txrx/*.o
		rm -f ../pcs_pma/*.o
		rm -f ../parallel_init/*.o

#make pl relocatable object
txrx_model :		$(TARGET)
#CFLAGS += -DRIO_PL_MODEL

%.o :		%.c
		$(CC) $(CFLAGS) $(INCLUDES) -I./include -I../parallel_init/include -I../pcs_pma/include -I../pcs_pma_adapter/include -I../txrx/include -I../pl/include -I../interfaces/common -I../interfaces/parallel_interfaces -I../interfaces/serial_interfaces -o $*.o $<

###############################################################################
             
