************************************************************
RapidIO Models 2.3.0.2 shapshot
readme.txt
COPYRIGHT 2000-2003 MOTOROLA ALL RIGHTS RESERVED
************************************************************

This package contains RapidIO Models source code from 05 March 2003

This source snapshot is covered by the terms and conditions of the RapidIO
BFM Master License Agreement between Motorola and the Trade Association


This document contains additional information about the product.
The notes presented are the latest available.

The following topics are covered:

1. Source code structure
2. Necessary tools versions
3. Build information
3.1 Testbench Build information
4. Support Information
5. What's New


1. Source code structure

verilog_env/
    demo/ - Demo Application for the RapidIO Models. This folder contains
            two subfolders:
        env/ - Demo environment and scenarios (written in Verilog)
        pli/ - PLI wrapper to connect RapidIO Models with the Demo environment

    pliwrap/ - Wrappers of the models (including Verilog stubs) which are
               necessary to use models with Verilog. Contains subfolders:
           pe/ - PE RIO model wrapper and makefile
           pl/ - PE RIO model wrapper and makefile
           txrxm/ - PE RIO model wrapper and makefile

    tb/ - Testbench which connects instances of models to provide quick
          start for custom verificaiton environment (makes use of wrappers
          situated in pliwrap/ directory). Contains subfolders:
      module_pe/ - module representing PE RIO Model
      module_pl/ - module representing PL RIO Model
      module_txrx/ - module representing TxRx RIO Model
      top_pe-pe/ - top level module with PE to PE modules connection over RapidIO
      top_pe-pl/ - top level module with PE to PL modules connection over RapidIO
      top_sp/    - top level module with PE to PE and PE to TxRx modules 
                    in parallel and serial modes connection over RapidIO. 

src/ - RapidIO Models source codes
    interfaces/ - RapidIO Models header files
    ll/         - Logical Layer header and source code files
    models/     - PL, PL, TXRX and Switch models top level source code files and makefiles
    pl/         - Physical Layer header and source code files
    switch/     - Switch model header and source code files
    tl/         - Transport Layer header and source code files
    txrx/       - 32 bit transmitter/receiver header and source code files
    txrxm/      - TXRX model header and source code files

manual/ - RapidIO Models User's Manual in Frame Maker 5.1 format

2. Necessary tools versions

The following tools are necessary in order to build RapidIO BFM models for
different platforms:

Solaris 2.5.1:
    gcc version 2.7.2.3
    ld: Software Generation Utilities - Solaris/ELF (3.0)
    make - standard utility from SUN Solaris distribution
    vcs 5.1
    ncvlog: v03.10.(s020)

Solaris 2.8:
    gcc version 2.95.2
    ld: Software Generation Utilities - Solaris/ELF (3.0)
    make - standard utility from SUN Solaris distribution
    vcs 5.1
    ncvlog: v03.10.(s020)



3. Build information

In order to build the models on different platforms it is necessary to perform
the following actions:

Solaris:

parallel models preparation:
    RapidIO PE model:
        - Go to directory src/models/rio
        - Run the command make -f makefile.mak clean sun_par

    RapidIO PL model:
        - Go to directory src/models/rio_pl
        - Run the command -f makefile.mak clean parallel

    RapidIO Switch model:
        - Go to directory src/models/rio_switch
        - Run the command -f makefile.mak clean sun_par

    RapidIO TXRX model:
        - Go to directory src/models/rio_txrxm
        - Run the command -f makefile.mak clean sun_par

parallel and serial models preparation:
    RapidIO PE model:
        - Go to directory src/models/rio
        - Run the command make -f makefile.mak clean all

    RapidIO PL model:
        - Go to directory src/models/rio_pl
        - Run the command -f makefile.mak clean all

    RapidIO Switch model:
        - Go to directory src/models/rio_switch
        - Run the command -f makefile.mak clean all

    RapidIO TXRX model:
        - Go to directory src/models/rio_txrxm
        - Run the command -f makefile.mak clean all
        
parallel and serial models with the flexlm usage preparation
    RapidIO PE model:
        - Go to directory src/models/rio
        - Run the command make -f makefile.mak clean all_flexlm

    RapidIO PL model:
        - Go to directory src/models/rio_pl
        - Run the command -f makefile.mak clean all_flexlm

    RapidIO Switch model:
        - Go to directory src/models/rio_switch
        - Run the command -f makefile.mak clean all_flexlm

    RapidIO TXRX model:
        - Go to directory src/models/rio_txrxm
        - Run the command -f makefile.mak clean all_flexlm

3.1 Testbenches and PLI wrappers build information

    Before you billd wrappers and testbenches you need 
correctly initialize the following environments` variables:
RIO_PATH - you need to set this variable to your src directory:
> cd install_dir/src; setenv RIO_PATH `pwd`
VERILOG_DIR - you need to set this variable to your 
verilog_env directory:
> cd install_dir/verilog_env; setenv VERILOG_DIR `pwd`


4. Support Information

For RapidIO Models related issues please email to:
rio-model-support@asc.corp.mot.com

            
Developers:
            Pavel Yegorov,
            Nickolay Dalmatov,
            Igor Mayanov,
            Dmitry Tylik
            Maria Salieva
            Konstantin Zibin
            Oleg Kruzhkov
			Ruslan Podkhlebny

Test Engineer:
            Viacheslav Ilyin
 
5. What's New

5.1 Version 2.3.0.1 (20 February 2004):

5.1.1 Bug fixes from RapidIO Models 2.2.4:
      - Error in pcs/pma adapter module for Multicast event 
        inserted after Start Of Packet - fixed;
  
5.1.2 Functionality modifications from RapidIO Models 2.2.4:

   The following mantis ID's are fixed : 
   #73, #74, #75, #76

5.2 Version 2.3.0.2 (05 March 2004):

5.2.1 Bug fixes from RapidIO Models 2.2.4:
      - Incorrect handling of Multicast control symbol
       in PCS/PMA Adapter module - fixed.
	  - Error in the TxRx model (sending status symbols through txrx model 
	  in case compensation symbol sending machine is disabled) - fixed.
  
5.1.2 Functionality modifications from RapidIO Models 2.2.4:

   The following mantis ID's are fixed : 
    - #73 (updated for PCS/PMA messages);
   
   The following enhancements are implemented:
    - Packet retry scenario generator;
	- Programmable delay for PE BFM Logical Layer response.

5.3 Limitations

None.
 
