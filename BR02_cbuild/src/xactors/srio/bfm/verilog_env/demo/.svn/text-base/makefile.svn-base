#******************************************************************************
#*
#* COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
#* All Rights Reserved
#*
#* This code is the property of Motorola
#* and is Motorola Confidential Proprietary Information.
#*
#* $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/makefile,v $
#* $Author: knutson $
#* $Locker:  $
#* $State: Exp $
#* $Revision: 1.1 $
#*
#******************************************************************************
# this makefile provides Demo Application compilation, execution and clearance
# under VCS   
#

LD_LIBRARY_PATH   +=  :${RIO_PATH}/models/rio:${RIO_PATH}/models/rio_switch:${RIO_PATH}/demo/pli:${RIO_PATH}/verilog_env/demo/pli:${RIO_PATH}/src/models/rio:${RIO_PATH}/src/models/rio_switch

SHARED_OBJECTS     =
SHARED_PATHES      =     
DEMO_FILES         =
DEMO_INC           =
RIO_OPTIONS        =
PLI_LIST           =

PLI_DIR	= ${RIO_PATH}/demo/pli
ENV_DIR	= ./env


# Demo environment model top module file for simple demo
DEMO_TOP_SIMPLE     +=  ${ENV_DIR}/top.v

# Demo environment model top module file for simple demo
DEMO_TOP_SWITCH     +=  ${ENV_DIR}/top_2.v \
            	      ${ENV_DIR}/switch.v


# Demo environment model common files
DEMO_FILES  +=  ${ENV_DIR}/memory_control.v \
		      ${ENV_DIR}/mailbox_control.v \
		      ${ENV_DIR}/cache_control.v \
		      ${ENV_DIR}/processing_element.v  \
		      ${ENV_DIR}/dma.v 

# Compiler generic options
RIO_OPTIONS       +=  +vcs+lic+wait -M -Mupdate -S -notice -I -line
RIO_AIX_OPTIONS     = -Mupdate +acc+4

# PLI list ("tab") file
PLI_LIST          +=  -P ${PLI_DIR}/pli_wrapper.tab

# Model include path
DEMO_INC	  +=  +incdir+${ENV_DIR}
                    
# 
SHARED_OBJECTS    +=  -lrio \
                      -lrio_switch \
	              -lwraprio		

SHARED_AIX_OBJECTS = ${RIO_PATH}/demo/pli/libwraprio.so


# Path for RIO model shared object file
SHARED_PATHES     +=  -L${PLI_DIR} \
		      -L${RIO_PATH}/models/rio 	\
		      -L${RIO_PATH}/models/rio_switch \
		      -L${RIO_PATH}/src/models/rio 	\
		      -L${RIO_PATH}/src/models/rio_switch              


# Targets:

# print the usage info
all                 :
	            @echo ""
	            @echo "Insufficient number of arguments."
	            @echo "To build the simple demo model, execute:"
	            @echo "    make simple"
	            @echo "To build the switch-based demo model, execute:"
	            @echo "    make switch"
	            @echo "To clean the demo data, execute:"
	            @echo "    make clean"
	            @echo "To run the demo hardware without xvcs GUI, execute:"
	            @echo "    make run"
	            @echo "To run the simple demo hardware with xvcs GUI, execute:"
	            @echo "    make simple_gui"
	            @echo "To run the switch-based demo hardware with xvcs GUI, execute:"
	            @echo "    make switch_gui"


	            @echo "To build the simple demo model for AIX, execute:"
	            @echo "    make simple_aix"
	            @echo "To build the switch-based demo model for AIX, execute:"
	            @echo "    make switch_aix"
	            @echo "To run the simple demo hardware with vcs GUI for AIX, execute:"
	            @echo "    make simple_aix_gui"
	            @echo "To run the switch-based demo hardware with vcs GUI for AIX, execute:"
	            @echo "    make switch_aix_gui"

	            @echo "To build the simple demo model for linux, execute:"
	            @echo "    make simple_lin"
	            @echo "To build the switch-based demo model for Linux, execute:"
	            @echo "    make switch_lin"
	            @echo "To run the demo hardware without GUI, execute:"
	            @echo "    make run"
	            @echo "To run the simple demo hardware with GUI for linux, execute:"
	            @echo "    make simple_lin_gui"
	            @echo "To run the switch-based demo hardware with GUI for linux, execute:"
	            @echo "    make switch_lin_gui"

	            @echo ""

# clean
clean               :
	            @echo ""
	            @echo "Cleaning demo data"
	            @echo ""
	            @rm -rf ./simv ./simv.daidir ./csrc

# build simple demo hardware
simple		    :   makefile $${DEMO_TOP_SIMPLE} $${DEMO_FILES} $${OBJECT_FILES}
	            @echo ""
	            @echo "Building simple demo model"
	            @echo "demo environment is located at ${ENV_DIR}"
	            @echo ""
	            @vcs ${RIO_OPTIONS} ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS}"

# build simple demo hardware
switch		    :   makefile $${DEMO_TOP_SWITCH} $${DEMO_FILES} $${OBJECT_FILES}
	            @echo ""
	            @echo "Building switch demo model"
	            @echo "demo environment is located at ${ENV_DIR}"
	            @echo ""
	            @vcs ${RIO_OPTIONS} ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS}"
                    
# execute without GUI
run                 :
	            @echo ""
	            @echo "Running ./simv without GUI."
	            @echo ""
	            @./simv

# execute with GUI
simple_gui          :
	            @echo ""
	            @echo "Running xvcs GUI with ./simv (simple demo)"
	            @echo ""
	            @xvcs +sim+./simv +cli ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS}"
	            
# execute with GUI
switch_gui          :
	            @echo ""
	            @echo "Running xvcs GUI with ./simv (switch demo)"
	            @echo ""
	            @xvcs +sim+./simv +cli ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS}"

simple_aix:                
	            @vcs ${RIO_AIX_OPTIONS} ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_AIX_OBJECTS}"

switch_aix:
	            @vcs ${RIO_AIX_OPTIONS} ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_AIX_OBJECTS}"

simple_aix_gui:                
	            @vcs -RI ${RIO_AIX_OPTIONS} ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_AIX_OBJECTS}"

switch_aix_gui:
	            @vcs -RI ${RIO_AIX_OPTIONS} ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_AIX_OBJECTS}"
                
#linux targets

# build simple demo hardware
simple_lin	    :   makefile ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${OBJECT_FILES}
	            @echo ""
	            @echo "Building simple demo model"
	            @echo "demo environment is located at ${ENV_DIR}"
	            @echo ""
	            @vcs ${RIO_OPTIONS} ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS} ${SHARED_PATHES}"

# build simple demo hardware
switch_lin	    :   makefile ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${OBJECT_FILES}
	            @echo ""
	            @echo "Building switch demo model"
	            @echo "demo environment is located at ${ENV_DIR}"
	            @echo ""
	            @vcs ${RIO_OPTIONS} ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS} ${SHARED_PATHES}"

# build simple demo hardware with gui
simple_lin_gui	    :   makefile ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${OBJECT_FILES}
	            @echo ""
	            @echo "Building simple demo model"
	            @echo "demo environment is located at ${ENV_DIR}"
	            @echo ""
	            @vcs ${RIO_OPTIONS} -RI ${DEMO_TOP_SIMPLE} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS} ${SHARED_PATHES}"

# build simple demo hardware with gui
switch_lin_gui	    :   makefile ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${OBJECT_FILES}
	            @echo ""
	            @echo "Building switch demo model"
	            @echo "demo environment is located at ${ENV_DIR}"
	            @echo ""
	            @vcs ${RIO_OPTIONS} -RI ${DEMO_TOP_SWITCH} ${DEMO_FILES} ${PLI_LIST} ${DEMO_INC} -LDFLAGS "${SHARED_OBJECTS} ${SHARED_PATHES}"




