#ifndef PLI_WRAPPER_H
#define PLI_WRAPPER_H
/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/pli/pli_wrapper.h,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* Functions:    
*
* History:      Use the CVS command log to display revision history
*               information.
*
* Description:  Header file for the PLI Wrapper  
*
* Notes:        
*
******************************************************************************/
static char pli_wrapper_h[] = "pli_wrapper.h";

typedef RIO_PARALLEL_MODEL_FTRAY_T RIO_MODEL_FTRAY_T;
typedef RIO_PARALLEL_MODEL_CALLBACK_TRAY_T RIO_MODEL_CALLBACK_TRAY_T;
typedef RIO_PARALLEL_PARAM_SET_T RIO_PARAM_SET_T;

typedef RIO_SWITCH_PARALLEL_FTRAY_T RIO_SWITCH_FTRAY_T;
typedef RIO_SWITCH_PARALLEL_CALLBACK_TRAY_T RIO_SWITCH_CALLBACK_TRAY_T;
typedef RIO_SWITCH_PARALLEL_PARAM_SET_T RIO_SWITCH_PARAM_SET_T;



#define RIO_PLI_SHORT_REGISTER_SIZE                 8
#define RIO_PLI_LONG_REGISTER_SIZE                  32
#define RIO_MAX_DATA_SIZE                           64

#define RIO_MAX_DW_SIZE                             32

#define RIO_MAX_NUM_OF_PE                           256
#define RIO_MAX_NUM_OF_SWITCHES                     16

/* Data size convertion constants */

#define RIO_BITS_IN_BYTE                            8
#define RIO_BITS_IN_WORD                            32
#define RIO_BITS_IN_DW                              64
#define RIO_BYTES_IN_WORD                           4
#define RIO_BYTES_IN_DW                             8
#define RIO_WORDS_IN_DW                             2

/* Mailbox status bits positions */

#define RIO_MB_AVAILABLE_BIT_POS                    0
#define RIO_MB_FULL_BIT_POS                         1
#define RIO_MB_EMPTY_BIT_POS                        2
#define RIO_MB_BUSY_BIT_POS                         3
#define RIO_MB_FAILED_BIT_POS                       4
#define RIO_MB_ERROR_BIT_POS                        5

#define RIO_MB_BITS_SHIFT                           8

/* Extended features registers offsets */

#define RIO_MB_CSR_OFFSET                           0x40
#define RIO_DB_CSR_OFFSET                           0x44
#define RIO_MB_MSR_OFFSET                           0x410
#define RIO_MB_SAR_OFFSET                           0x414 
#define RIO_MB_DAR_OFFSET                           0x418 
#define RIO_MB_BCR_OFFSET                           0x41C 
#define RIO_MB_SRC_TR_INFO_OFFSET                   0x420 

/* Switch context = inst_ID (of switch) * this value: */
#define RIO_SWITCH_CONTEXT_MULTIPLIER               100

#define RIO_SWITCH_NUM_OF_LINKS                     16

#define RIO_LARGE_GRAN_DW_SIZE                      8
#define RIO_SMALL_GRAN_DW_SIZE                      4

typedef enum {
    CONFIG_WRITE = 0,
    CONFIG_READ
} RIO_CONFIG_OPER_T;

typedef enum {
    RIO_GENERAL_DATA = 0,
    RIO_GSM_DATA,
    RIO_IO_DATA,
    RIO_CONFIG_DATA,
    RIO_MP_DATA,
    RIO_MEM_DATA,
    RIO_SNOOP_DATA
} RIO_PLI_DATA_ID_T;

#define RIO_MAX_PLI_DATA_ID   RIO_SNOOP_DATA

typedef struct {
    int childDemoScenarioID;
    int childMailboxControlID;
    int childCacheControlID;
    int childMemoryControlID;
} RIO_CHILD_IDENTIFIERS_T;

typedef struct {
    handle pli_GSM_Trx_Tag_Reg;
    handle pli_GSM_Result_Reg;
    handle pli_GSM_Err_Code_Reg;
    handle pli_GSM_Req_Done_Flag_Reg;

    handle pli_Loc_Trx_Tag_Reg;
    handle pli_Loc_Response_Reg;
    handle pli_Loc_Response_Flag_Reg;

    handle pli_IO_Trx_Tag_Reg; 
    handle pli_IO_Result_Reg;
    handle pli_IO_Err_Code_Reg;
    handle pli_IO_Req_Done_Flag_Reg;

    handle pli_MP_Trx_Tag_Reg; 
    handle pli_MP_Result_Reg;
    handle pli_MP_Err_Code_Reg;
    handle pli_MP_Req_Done_Flag_Reg;

    handle pli_DB_Trx_Tag_Reg; 
    handle pli_DB_Result_Reg;
    handle pli_DB_Err_Code_Reg;
    handle pli_DB_Req_Done_Flag_Reg;

    handle pli_Config_Trx_Tag_Reg; 
    handle pli_Config_Result_Reg;
    handle pli_Config_Err_Code_Reg;
    handle pli_Config_Req_Done_Flag_Reg;

    handle pli_Snoop_Address_Reg;
    handle pli_Snoop_Ext_Address_Reg;
    handle pli_Snoop_XAMSBS_Reg;
    handle pli_Snoop_LTType_Reg;
    handle pli_Snoop_TType_Reg;
    handle pli_Snoop_Req_Flag_Reg;

    handle pli_MP_MSGLEN_Reg;
    handle pli_MP_SSIZE_Reg;
    handle pli_MP_LETTER_Reg;
    handle pli_MP_MBOX_Reg;
    handle pli_MP_MSGSEG_Reg;
    handle pli_MP_DW_Size_Reg;
    handle pli_MP_Flag_Reg;

    handle pli_DB_Info_Reg;
    handle pli_DB_Flag_Reg;

    handle pli_Mem_Address_Reg;
    handle pli_Mem_Ext_Address_Reg;
    handle pli_Mem_XAMSBS_Reg;
    handle pli_Mem_DW_Size_Reg;
    handle pli_Mem_Type_Reg;
    handle pli_Mem_Is_GSM_Reg;
    handle pli_Mem_BE_Reg;
    handle pli_Mem_Flag_Reg;

    handle pli_Pins_TFrame_Reg;
    handle pli_Pins_TD_Reg;
    handle pli_Pins_TDL_Reg;
    handle pli_Pins_TCLK_Reg;
    handle pli_Pins_Flag_Reg;

    handle mailbox_CSR;
    handle doorbell_CSR;

    handle mem_IO_Space_XAMSBS;
    handle mem_IO_Space_Ext_Address;
    handle mem_IO_Space_Start_Address;
    handle mem_IO_Space_DW_Size;
    handle mem_GSM_Space_XAMSBS;
    handle mem_GSM_Space_Ext_Address;
    handle mem_GSM_Space_Start_Address;
    handle mem_GSM_Space_DW_Size;

    handle dma_MSR;
    handle dma_SAR;
    handle dma_DAR;
    handle dma_BCR;
    handle dma_Src_Inst_ID;
} RIO_PLI_REG_HANDLES_T;

typedef struct {
    handle pli_Pins_TFrame_Reg;
    handle pli_Pins_TCLK_Reg;
    handle pli_Pins_Flag_Reg;
    handle pli_Pins_TD_Regs[16];
    handle pli_Pins_TDL_Regs[16];
} RIO_SWITCH_PLI_REG_HANDLES_T;

typedef struct {
    RIO_HANDLE_T inst_Handle;                          /* RIO handle */
    char inst_Name[50];                                /* name of the instantiated module */
    handle pli_Inst_Handle;                            /* PLI handle */
    RIO_PLI_REG_HANDLES_T pli_Reg_Handles;             /* PLI handles of registers for PLI data exchange */
    RIO_DW_T* pli_Data_Areas[RIO_MAX_PLI_DATA_ID + 1]; /* Array of pointers to PLI data areas */
    unsigned long* memory_Directory;                   /* Pointer to the PE memory directory data */
    unsigned long memory_Directory_Size;               /* Number of lines in the PE memory directory (number of GSM granules) */
    int coh_Granule_DW_Size;                           /* Coherency granule DW size: can be 4 or 8 DW-s */
    int coh_Domain_Size;                               /* Size of coherence domain - 2, 4 or 16 participants allowed */
    RIO_BOOL_T xamsbs_Enabled;                         /* XAMSBS enabled (1) / disabled (0) */
    RIO_BOOL_T ext_Address_Enabled;                    /* Extended Address enabled (1) / disabled (0) */
    RIO_BOOL_T ext_Address_16;                         /* Actual only if Ext Address enabled. 1 : 16 bits, 0 : 32 bits */
    RIO_ROUTING_METHOD_T routing_Method;               /* Table-, shift-, or index-based routing */ 
    RIO_TR_INFO_T* transport_Info_Table;               /* List of transport info values (table for shift- or index-based routing)*/
    RIO_PL_TRANSP_TYPE_T tr_Type;                      /*current transport type*/
} RIO_PE_INSTANCES_TABLE_T;

typedef struct {
    RIO_HANDLE_T switch_Handle;                        /* RIO handle of switch instance */
    RIO_HANDLE_T* link_Handles;                        /* RIO handles of LP-EP links */
    int link_Number;                                   /* Number of used LP-EP links (0..15) */
    RIO_SWITCH_PLI_REG_HANDLES_T pli_Reg_Handles;      /* PLI handles of registers for PLI data exchange */
} RIO_SWITCH_INSTANCES_TABLE_T;

/*********************** Exterrnal functions *****************************/

/************************************************************************** 
****************  PLI SW parts of "pair" functions  ***********************
**************************************************************************/ 

void SW_Init();
void SW_Close();
void SW_Lg_GSM_Request();
void SW_Lg_IO_Request();
void SW_Lg_Configuration_Request();
void SW_Lg_Message_Request();
void SW_Lg_Doorbell_Request();
void SW_Lg_Snoop_Response();
void SW_Lg_Set_Config_Reg();
void SW_Lg_Get_Config_Reg();
void SW_Lg_Get_Memory_Data();
void SW_Lg_Set_Memory_Data();
void SW_Get_Pins();
void SW_Switch_Get_Pins();
void SW_Clock_Edge();
void SW_Switch_Clock_Edge();

/************************************************************************** 
******************  PLI Data Areas access functions: **********************
**************************************************************************/ 

void SW_Set_PLI_DW();
void SW_Get_PLI_DW();
void SW_Get_PLI_DW_Value();
void SW_Get_PLI_MS_Word();
void SW_Get_PLI_LS_Word();
void SW_Get_Address_Settings();
void SW_Get_Tr_Info();
void  SW_Set_Switch_Mode_Param();

/************************************************************************** 
* *****************  PLI callbacks  *************************************** 
**************************************************************************/ 

int RIO_Lg_GSM_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    );

int RIO_Lg_Local_Response(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_LOCAL_RTYPE_T response 
    );

int RIO_Lg_IO_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,           
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    );

int RIO_Lg_MP_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code
    );

int RIO_Lg_Doorbell_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code           
    );

int RIO_Lg_Config_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,           
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    );

int RIO_Lg_Snoop_Request(
    RIO_CONTEXT_T      context,
    RIO_SNOOP_REQ_T    req
    );

int RIO_Lg_MP_Remote_Request(
    RIO_CONTEXT_T       context,
    RIO_REMOTE_MP_REQ_T message
    );

int RIO_Lg_Doorbell_Remote_Request(
    RIO_CONTEXT_T  context,
    RIO_DB_INFO_T  doorbell_Info
    );

int RIO_Lg_Memory_Request(
    RIO_CONTEXT_T          context,
    RIO_ADDRESS_T          address,                 
    RIO_ADDRESS_T          extended_Address,
    RIO_UCHAR_T            xamsbs,
    RIO_UCHAR_T            dw_Size,              
    RIO_MEMORY_REQ_TYPE_T  req_Type,             
    RIO_BOOL_T             is_GSM,               
    RIO_UCHAR_T            be                    
    );

int RIO_Lg_Read_Directory(
    RIO_CONTEXT_T  context, 
    RIO_ADDRESS_T  address,         
    RIO_ADDRESS_T  ext_Address,
    RIO_UCHAR_T    xamsbs,
    RIO_SH_MASK_T  *sharing_Mask    
    );

int RIO_Lg_Write_Directory(
    RIO_CONTEXT_T  context, 
    RIO_ADDRESS_T  address,         
    RIO_ADDRESS_T  ext_Address,
    RIO_UCHAR_T    xamsbs,
    RIO_SH_MASK_T  sharing_Mask    
    );

int RIO_Set_Pins(
    RIO_CONTEXT_T context, 
    RIO_BYTE_T    tframe,     
    RIO_BYTE_T    td,         
    RIO_BYTE_T    tdl,        
    RIO_BYTE_T    tclk
    );       

int RIO_Switch_Set_Pins(
    RIO_CONTEXT_T context,
    RIO_BYTE_T tframe,
    RIO_BYTE_T td,
    RIO_BYTE_T tdl,
    RIO_BYTE_T tclk
    );

/************************************************************************** 
* User-defined callbacks: address translation and remote config. requests *
**************************************************************************/ 

int RIO_Lg_Translate_Remote_IO_Req(
    RIO_CONTEXT_T    context, 
    RIO_ADDRESS_T    *address,           /* callback gets original address and returns translated address */
    RIO_ADDRESS_T    *ext_Address,
    RIO_UCHAR_T      *xamsbs
    );

int RIO_Lg_Translate_Local_IO_Req(
    RIO_CONTEXT_T    context, 
    RIO_ADDRESS_T    *address,           /* callback gets original address and returns translated address */
    RIO_ADDRESS_T    *ext_Address,
    RIO_UCHAR_T      *xamsbs,
    RIO_TR_INFO_T    *transport_Info     /* transport_info field of the packet (for instance, device id)  */
    );

int RIO_Lg_Route_GSM_Req(
    RIO_CONTEXT_T   context,
    RIO_ADDRESS_T   address,             /* address of the local GSM request */
    RIO_ADDRESS_T   ext_Address,
    RIO_UCHAR_T     xamsbs,
    RIO_TR_INFO_T   *transport_Info      /* transport_info field of the packet (for instance, device id)  */
    );

int RIO_Lg_Remote_Config_Read(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,   /* double-word aligned offset inside configuration space */
    unsigned long    *data
    );

int RIO_Lg_Remote_Config_Write(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset, /* double-word aligned offset inside configuration space */
    unsigned long    data
    );

/************************************************************************** 
******************  Error and Warning message callbacks   *****************
**************************************************************************/ 

int RIO_User_Msg(RIO_CONTEXT_T context, char *user_Msg);
int RIO_Switch_User_Msg(RIO_CONTEXT_T  context, char * user_Msg);
/************************************************************************** 
*********  Special functions: Instance registration, reset   **************
**************************************************************************/ 

void SW_Register_Instance();
void SW_Switch_Register_Instance();
void SW_Initialize_Instance();
void SW_Switch_Initialize_Instance();
void SW_Start_Reset_Instance();
void SW_Switch_Start_Reset_Instance();

/*callbacks for hooking*/
int SW_Rio_Granule_Received(
    RIO_CONTEXT_T           context,
    RIO_GRANULE_STRUCT_T*   granule_Struct      /* struct, containing symbol's fields */);

int SW_Rio_Request_Received(
    RIO_CONTEXT_T         context,
    RIO_REMOTE_REQUEST_T  *packet_Struct      /* struct, containing packets's fields */);

int SW_Rio_Response_Received(
    RIO_CONTEXT_T         context,
    RIO_RESPONSE_T        *packet_Struct      /* struct, containing response's fields */);

int SW_Rio_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     );

int SW_Rio_Packet_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int          buffer_Size,        /*Size always = 276*/  
    RIO_UCHAR_T*          packet_Length_In_Granules /* maximum = 276/4 = 69 granules!!*/);

/*****************************************************************************/
#endif /* PLI_WRAPPER_H */
