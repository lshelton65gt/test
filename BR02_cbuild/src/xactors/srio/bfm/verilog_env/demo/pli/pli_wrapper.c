#define PLI_WRAPPER_C
/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\riom\pli\pli_wrapper.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  PLI wrapper for RIO PE demo environment model
*
* Notes:        This file contains the source code of the Demo Application
*               module called PLI Wrapper. This module uses a special library
*               called PLI (it is supplied with VCS compiler) and provides
*               data exchange between the RIO model, written in C language,
*               and the Demo Environment, written in Verilog. Those functions
*               of the PLI Wrapper which are intended to be called from
*               the Verilog source code can#t receive its arguments directly,
*               so it#s needed to call a PLI library function with
*               the specified argument number (1, 2, 3, etc...) to obtain
*               each of the PLI Wrapper function argument values and
*               (if needed) change some of them and return the result value.
*               Most of these PLI Wrapper functions have a large number of
*               the arguments, and using of any named constants instead of
*               the hard-coded argument numbers couldn#t make the code better
*               and easier to understand. To make the code easier to correct
*               in case of the interface changes, correction index variable
*               called k is used in the functions with a very large number
*               of the arguments, so in case of addition or removal of any
*               arguments in the interfaces of these functions it#s only
*               needed to correct this index instead of corrections of
*               argument numbers.
*
******************************************************************************/
static char pli_wrapper_c[] = "$RCSfile: pli_wrapper.c,v $ $Revision: 1.2 $"; /* RCS identifiers */

#include "acc_user.h"
#ifdef _MODELSIM_
#include "veriuser.h"
#else
#include "vcsuser.h"
#endif

#include "rio_types.h" 
#include "rio_parallel_types.h"
#include "rio_common.h"
#include "rio_parallel_common.h"
#include "rio_parallel_model.h"
#include "rio_ll_interface.h"
#include "rio_switch_interface.h"
#include "rio_switch_parallel_interface.h"
#include "rio_switch_parallel_model.h"
#include "pli_wrapper.h"
#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <memory.h>

/* Table of the instantiated PE-s with needed data for each of them */
static RIO_PE_INSTANCES_TABLE_T * peInstancesTable[RIO_MAX_NUM_OF_PE + 1];
/* Table of the instantiated switches with needed data for each of them */
static RIO_SWITCH_INSTANCES_TABLE_T * switchInstancesTable[RIO_MAX_NUM_OF_SWITCHES + 1];

static unsigned int total_Number_Of_PE;
static unsigned int total_Number_Of_Switches;

static RIO_MODEL_FTRAY_T *funcTray;                     /* function tray (foe PE-s) */
static RIO_SWITCH_FTRAY_T *switchFuncTray;              /* function tray for switches */

static int dev_IDs[RIO_MAX_NUM_OF_PE + 1];              /* PE device identifiers */  


/*Arrays for switch initializing*/
static RIO_BOOL_T Work_Modes[RIO_SWITCH_NUM_OF_LINKS];	/* Port widths for switch initializing*/
static RIO_BOOL_T Req_Window_Align[RIO_SWITCH_NUM_OF_LINKS];	
static unsigned int Num_Trainings[RIO_SWITCH_NUM_OF_LINKS];	/*number of trainings pattern for each link*/

/*********************** Internal functions ******************************/

static void setRegValue(handle hRegSet, int index, unsigned long value);
static unsigned long getRegValue(handle hRegSet, int index);
static void setIntegerRegValue(handle hReg, int value);
static int getIntegerRegValue(handle hReg);

static RIO_REQ_RESULT_T accessConfigReg(
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    RIO_CONFIG_OPER_T operation,
    unsigned long *data
    );

/*************************************************************************/

/************************************************************************** 
****************  PLI SW parts of "pair" functions  ***********************
**************************************************************************/ 

/***************************************************************************
 * Function : SW_Init
 *
 * Description: Software part of PLI function pair to initialize
 *                PLI engine, shall be called when demo starts
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Init()
{
    int i;

    acc_initialize();

    total_Number_Of_PE = acc_fetch_tfarg_int(1);
    if (total_Number_Of_PE > RIO_MAX_NUM_OF_PE)
        RIO_User_Msg(0, "Error: Init: Too many PE");
        
/*  if ((peInstancesTable = (RIO_PE_INSTANCES_TABLE_T**) malloc((total_Number_Of_PE + 1) *
    sizeof(RIO_PE_INSTANCES_TABLE_T*))) == 0);        
        RIO_Error_Msg(0, "Init: Unable to allocate memory for PE table");       */

    total_Number_Of_Switches = acc_fetch_tfarg_int(2);
    if (total_Number_Of_Switches > RIO_MAX_NUM_OF_SWITCHES)
        RIO_User_Msg(0, "Error: Init: Too many Switches");
        
/*  if ((switchInstancesTable = (RIO_SWITCH_INSTANCES_TABLE_T**) malloc((total_Number_Of_Switches + 1) *
    sizeof(RIO_SWITCH_INSTANCES_TABLE_T*))) == 0);        
      RIO_Error_Msg(0, "Init: Unable to allocate memory for Switches table");    */
    
    for (i = 0; i <= RIO_MAX_NUM_OF_PE; i++)
        peInstancesTable[i] = NULL;                  

    for (i = 0; i <= RIO_MAX_NUM_OF_SWITCHES; i++)
        switchInstancesTable[i] = NULL;                  

    /* obtain function tray */
    funcTray = malloc(sizeof(RIO_MODEL_FTRAY_T));
    if (RIO_Parallel_Model_Get_Function_Tray(funcTray) != RIO_OK)
        RIO_User_Msg(0, "Error: Init: RIO_Model_Get_Function_Tray: not OK");

       /* obtain function tray for switches*/
    switchFuncTray = malloc(sizeof(RIO_SWITCH_FTRAY_T));
    if (RIO_Switch_Parallel_Get_Function_Tray(switchFuncTray) != RIO_OK)
        RIO_User_Msg(0, "Error: Init: RIO_Switch_Get_Function_Tray: not OK");

}

/***************************************************************************
 * Function : SW_Close
 *
 * Description: Software part of PLI function pair to free internal
 *                PLI structures, shall be called when demo finishes
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Close()
{

    acc_close();

    if (funcTray) free(funcTray);

    if (switchFuncTray) free(switchFuncTray);
}

/***************************************************************************
 * Function : SW_Lg_GSM_Request
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_GSM_REQUEST
 *              from Demo Scenario or Cache control to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_GSM_Request()
{
    int data_Flag, i;
    int instID = acc_fetch_tfarg_int(1);
    RIO_TAG_T trx_Tag;
    int result;
    RIO_GSM_TTYPE_T ttype = acc_fetch_tfarg_int(2);
    RIO_GSM_REQ_T req;

    req.prio = (RIO_UCHAR_T)  acc_fetch_tfarg_int(3);
    req.address = (RIO_ADDRESS_T) acc_fetch_tfarg_int(4);
    req.extended_Address = (RIO_ADDRESS_T) acc_fetch_tfarg_int(5);
    req.xamsbs = (RIO_UCHAR_T) acc_fetch_tfarg_int(6);
    req.dw_Size = (RIO_UCHAR_T) acc_fetch_tfarg_int(7);
    req.offset = (RIO_UCHAR_T) acc_fetch_tfarg_int(8);
    req.byte_Num = (RIO_UCHAR_T) acc_fetch_tfarg_int(9);
/*    req.ext_Address_Valid = (RIO_BOOL_T) acc_fetch_tfarg_int(10); 
    req.ext_Address_16 = (RIO_BOOL_T) acc_fetch_tfarg_int(11);*/
    req.transp_Type = peInstancesTable[instID]->tr_Type;
    
    trx_Tag = acc_fetch_tfarg_int(10);

    data_Flag = (int) acc_fetch_tfarg(11);
    req.data = (RIO_DW_T*) malloc(req.dw_Size * RIO_BYTES_IN_DW);         

    if(data_Flag)
    {
        for(i = 0; i < req.dw_Size; i++)
        {
            (req.data + i)->ms_Word = (RIO_WORD_T) acc_fetch_tfarg(12 + i * 2);        
            (req.data + i)->ls_Word = (RIO_WORD_T) acc_fetch_tfarg(13 + i * 2);        
        }
    }
    else
    {
        /*    req.data = peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA];  */
        for(i = 0; i < req.dw_Size; i++)
        {
            (req.data + i)->ms_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ms_Word;
            (req.data + i)->ls_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ls_Word;
        }
    }

    result = (*(funcTray->rio_GSM_Request)) (peInstancesTable[instID]->inst_Handle, ttype, &req, trx_Tag);

    /*printf("WRAPPER: PE %2d: GSM Req sent: Result: %2d \n", instID, result); */
    
    if (result != RIO_OK && result != RIO_RETRY)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_GSM_Request: not OK");

    if (req.data) free(req.data);

    tf_putp(0, result);
}

/***************************************************************************
 * Function : SW_Lg_IO_Request
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_IO_REQUEST
 *              from Demo Scenario to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_IO_Request()
{
    int data_Flag;
    int i;
    int instID = acc_fetch_tfarg_int(1);
    RIO_TAG_T trx_Tag;
    int result;
    RIO_IO_REQ_T req;

    req.ttype = (RIO_IO_TTYPE_T) acc_fetch_tfarg_int(2);
    req.prio = (RIO_UCHAR_T) acc_fetch_tfarg_int(3);
    req.address = (RIO_ADDRESS_T) acc_fetch_tfarg_int(4);
    req.extended_Address = (RIO_ADDRESS_T) acc_fetch_tfarg_int(5);
    req.xamsbs = (RIO_UCHAR_T) acc_fetch_tfarg_int(6);
    req.dw_Size = (RIO_UCHAR_T) acc_fetch_tfarg_int(7);
    req.offset = (RIO_UCHAR_T) acc_fetch_tfarg_int(8);
    req.byte_Num = (RIO_UCHAR_T) acc_fetch_tfarg_int(9);
/*    req.ext_Address_Valid = (RIO_BOOL_T) acc_fetch_tfarg_int(10); 
    req.ext_Address_16 = (RIO_BOOL_T) acc_fetch_tfarg_int(11);*/
    req.transp_Type = peInstancesTable[instID]->tr_Type;

    trx_Tag = acc_fetch_tfarg_int(10);  

    data_Flag = (int) acc_fetch_tfarg(11);
    req.data = (RIO_DW_T*) malloc(req.dw_Size * RIO_BYTES_IN_DW);         

    if (data_Flag)
    {
        for(i = 0; i < req.dw_Size; i++)
        {
            (req.data + i)->ms_Word = (RIO_WORD_T) acc_fetch_tfarg(12 + i * 2);        
            (req.data + i)->ls_Word = (RIO_WORD_T) acc_fetch_tfarg(13 + i * 2);        
        }
    } else
    {
        /*    req.data = peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA];  */
        for(i = 0; i < req.dw_Size; i++)
        {
            (req.data + i)->ms_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ms_Word;
            (req.data + i)->ls_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ls_Word;
        }
    }

    result = (*(funcTray->rio_IO_Request)) (peInstancesTable[instID]->inst_Handle, &req, trx_Tag);

/*    printf("WRAPPER: PE %2d: IO Req sent: Type: %2d Result: %2d \n", instID, req.ttype, result);  */
    
    if (result != RIO_OK && result != RIO_RETRY)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_IO_Request: not OK");

    if (req.data) free(req.data);

    tf_putp(0, result);
}

/***************************************************************************
 * Function : SW_Lg_Configuration_Request
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_CONFIGURATION_REQUEST
 *              from Demo Scenario to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_Configuration_Request()
{
    int data_Flag;
    int i;
    int result;
    int instID = acc_fetch_tfarg_int(1);
    RIO_TAG_T trx_Tag;
    RIO_CONFIG_REQ_T req;

    req.prio = (RIO_UCHAR_T) acc_fetch_tfarg_int(2);
    req.transport_Info = (RIO_TR_INFO_T) acc_fetch_tfarg_int(3);
    req.hop_Count = (RIO_UCHAR_T) acc_fetch_tfarg_int(4);       
    req.rw = (RIO_CONF_TTYPE_T) acc_fetch_tfarg_int(5);
    req.config_Offset = (RIO_CONF_OFFS_T) acc_fetch_tfarg_int(6);
    req.sub_Dw_Pos = (RIO_UCHAR_T) acc_fetch_tfarg_int(7);      
    req.dw_Size = (RIO_UCHAR_T) acc_fetch_tfarg_int(8);
    trx_Tag = acc_fetch_tfarg_int(9);

    req.transp_Type = peInstancesTable[instID]->tr_Type;

    if (req.rw == RIO_CONF_READ)
        req.data = NULL;    
    else
    {
        data_Flag = (int) acc_fetch_tfarg(10);
        req.data = (RIO_DW_T*) malloc(req.dw_Size * RIO_BYTES_IN_DW);         
        if(data_Flag)
        {
            for(i = 0; i < req.dw_Size; i++)
            {
                (req.data + i)->ms_Word = (RIO_WORD_T) acc_fetch_tfarg(11 + i * 2);        
                (req.data + i)->ls_Word = (RIO_WORD_T) acc_fetch_tfarg(12 + i * 2);        
            }
        } else
        {
            /*    req.data = peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA];  */
            for(i = 0; i < req.dw_Size; i++)
            {
                (req.data + i)->ms_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ms_Word;
                (req.data + i)->ls_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ls_Word;
            }
        }
    }

    result = (*(funcTray->rio_Conf_Request)) (peInstancesTable[instID]->inst_Handle, &req, trx_Tag);

    if (result != RIO_OK && result != RIO_RETRY)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Config_Request: not OK");

    if (req.data) free(req.data);

    tf_putp(0, result);
}

/***************************************************************************
 * Function : SW_Lg_Message_Request
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_MESSAGE_REQUEST
 *              from Demo Scenario to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_Message_Request()
{
    int data_Flag;
    int i;
    int result;
    int instID = acc_fetch_tfarg_int(1);
    RIO_TAG_T trx_Tag;
    RIO_MESSAGE_REQ_T req;

    req.prio = (RIO_UCHAR_T) acc_fetch_tfarg_int(2);
    req.transport_Info = (RIO_TR_INFO_T) acc_fetch_tfarg_int(3);
    req.msglen = (RIO_UCHAR_T) acc_fetch_tfarg_int(4);
    req.ssize = (RIO_UCHAR_T) acc_fetch_tfarg_int(5);
    req.letter = (RIO_UCHAR_T) acc_fetch_tfarg_int(6);
    req.mbox = (RIO_UCHAR_T) acc_fetch_tfarg_int(7);
    req.msgseg = (RIO_UCHAR_T) acc_fetch_tfarg_int(8);
    req.dw_Size = (RIO_UCHAR_T) acc_fetch_tfarg_int(9);
    trx_Tag = (RIO_TAG_T) acc_fetch_tfarg_int(10);     
    req.transp_Type = peInstancesTable[instID]->tr_Type;
           
    data_Flag = (int) acc_fetch_tfarg(11);

    req.data = (RIO_DW_T*) malloc(req.dw_Size * RIO_BYTES_IN_DW);         

    if (data_Flag)
    {
        for(i = 0; i < req.dw_Size; i++)
        {
            (req.data + i)->ms_Word = (RIO_WORD_T) acc_fetch_tfarg(12 + i * 2);        
            (req.data + i)->ls_Word = (RIO_WORD_T) acc_fetch_tfarg(13 + i * 2);        
        }
    } else
    {
        /*    req.data = peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA];  */
        for(i = 0; i < req.dw_Size; i++)
        {
            (req.data + i)->ms_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ms_Word;
            (req.data + i)->ls_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA] + i)->ls_Word;
        }
    }

    result = (*(funcTray->rio_Message_Request)) (peInstancesTable[instID]->inst_Handle, &req, trx_Tag);

    if (result != RIO_OK && result != RIO_RETRY)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Message_Request: not OK");

    if (req.data) free(req.data);

    tf_putp(0, result);
}

/***************************************************************************
 * Function : SW_Lg_Doorbell_Request
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_DOORBELL_REQUEST
 *              from Demo Scenario or Mailbox Control module to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_Doorbell_Request()
{
    int result;
    int instID = acc_fetch_tfarg_int(1);
    RIO_TAG_T trx_Tag;
    RIO_DOORBELL_REQ_T req;
    req.prio = (RIO_UCHAR_T) acc_fetch_tfarg_int(2);
    req.transport_Info = (RIO_TR_INFO_T) acc_fetch_tfarg_int(3);
    req.doorbell_Info = (RIO_DB_INFO_T) acc_fetch_tfarg_int(4);
    req.transp_Type = peInstancesTable[instID]->tr_Type;

    trx_Tag = acc_fetch_tfarg_int(5);

    result = (*(funcTray->rio_Doorbell_Request))(peInstancesTable[instID]->inst_Handle, &req, trx_Tag);

    if (result != RIO_OK && result != RIO_RETRY)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Doorbell_Request: not OK");

    tf_putp(0, result);
}

/***************************************************************************
 * Function : SW_Lg_Snoop_Response
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_SNOOP_RESPONSE
 *              from Snooper (Cache Control module) to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_Snoop_Response()
{
    int instID = acc_fetch_tfarg_int(1);
    RIO_SNOOP_RESULT_T result = acc_fetch_tfarg_int(2); 
    RIO_BOOL_T push_Flag = acc_fetch_tfarg_int(3);
    RIO_DW_T *data;         
    int i;

    if (result == RIO_SNOOP_HIT)
    {
        if (push_Flag)
            io_printf("WRAPPER: PE %d: Snoop Response, result: HIT, data push: YES\n", instID);
        else
            io_printf("WRAPPER: PE %d: Snoop Response, result: HIT, data push: NO\n", instID);
    }
    else
        io_printf("WRAPPER: PE %d: Snoop Response, result: MISS\n", instID);
    
    if (push_Flag)
    {
        data = (RIO_DW_T*) malloc(peInstancesTable[instID]->coh_Granule_DW_Size * RIO_BYTES_IN_DW);
    
        for(i = 0; i < peInstancesTable[instID]->coh_Granule_DW_Size; i++)
        {
            (data + i)->ms_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_SNOOP_DATA] + i)->ms_Word;
            (data + i)->ls_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_SNOOP_DATA] + i)->ls_Word;
            /*io_printf("WRAPPER: Snoop Response: data[%d]: MSW = %x LSW = %x\n", i, (data + i)->ms_Word, (data + i)->ls_Word); */
        }
    }
    else
        data = NULL;
    
    if ((*(funcTray->rio_Snoop_Response)) (peInstancesTable[instID]->inst_Handle, result, data) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Snoop_Response: not OK");

    if (data) free(data);
}

/***************************************************************************
 * Function : SW_Lg_Set_Config_Reg
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_SET_CONFIG_REG
 *              from Demo Scenario to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_Set_Config_Reg()
{
    int instID = acc_fetch_tfarg_int(1);

    RIO_CONF_OFFS_T offset = acc_fetch_tfarg_int(2);
    unsigned long data = acc_fetch_tfarg_int(3);
    
    if ((*(funcTray->rio_Set_Conf_Reg)) (peInstancesTable[instID]->inst_Handle, offset, data) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Set_Config_Reg: not OK");
}

/***************************************************************************
 * Function : SW_Lg_Get_Config_Reg
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_GET_CONFIG_REG
 *              from Demo Scenario to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Lg_Get_Config_Reg()
{
    int instID = acc_fetch_tfarg_int(1);

    RIO_CONF_OFFS_T offset = acc_fetch_tfarg_int(2);
    unsigned long data;

    if ((*(funcTray->rio_Get_Conf_Reg)) (peInstancesTable[instID]->inst_Handle, offset, &data) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Get_Config_Reg: not OK");

    peInstancesTable[instID]->pli_Data_Areas[RIO_GENERAL_DATA]->ms_Word = data;
}

/***************************************************************************
 * Function : SW_Lg_Get_Memory_Data
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_GET_MEMORY_DATA
 *              from Memory Control module to RIO model  
 *
 * Returns: void
 *
 * Notes: Data transfer FROM RIO TO environment memory  
 *
 **************************************************************************/
void SW_Lg_Get_Memory_Data()
{
    int instID = acc_fetch_tfarg_int(1);
    int i;

    RIO_UCHAR_T dw_Size = acc_fetch_tfarg_int(2);

    RIO_DW_T *data = (RIO_DW_T*) malloc(dw_Size * RIO_BYTES_IN_DW);         

    /*io_printf("WRAPPER: PE %d: Get Memory Data called\n", instID); */

    if ((*(funcTray->rio_Get_Mem_Data)) (peInstancesTable[instID]->inst_Handle, data) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Get_Memory_Data: not OK");

    /*    peInstancesTable[instID]->pli_Data_Areas[RIO_MEM_DATA] = data;  */
    for(i = 0; i < dw_Size; i++)
    {
        (peInstancesTable[instID]->pli_Data_Areas[RIO_MEM_DATA] + i)->ms_Word = (data + i)->ms_Word;
        (peInstancesTable[instID]->pli_Data_Areas[RIO_MEM_DATA] + i)->ls_Word = (data + i)->ls_Word;
    }
}

/***************************************************************************
 * Function : SW_Lg_Set_Memory_Data
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_SET_MEMORY_DATA
 *              from Memory Control module to RIO model  
 *
 * Returns: void
 *
 * Notes: Data transfer FROM environment memory TO RIO  
 *
 **************************************************************************/
void SW_Lg_Set_Memory_Data()
{
    int instID = acc_fetch_tfarg_int(1);
    int i;

    RIO_UCHAR_T dw_Size = acc_fetch_tfarg_int(2);

    RIO_DW_T *data = (RIO_DW_T*) malloc(dw_Size * RIO_BYTES_IN_DW);         

    /*io_printf("WRAPPER: PE %d: Set Memory Data called\n", instID); */

    /*    data = peInstancesTable[instID]->pli_Data_Areas[RIO_MEM_DATA];  */
    for(i = 0; i < dw_Size; i++)
    {
        (data + i)->ms_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_MEM_DATA] + i)->ms_Word;
        (data + i)->ls_Word = (peInstancesTable[instID]->pli_Data_Areas[RIO_MEM_DATA] + i)->ls_Word;
    }

    if ((*(funcTray->rio_Set_Mem_Data)) (peInstancesTable[instID]->inst_Handle, data) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: RIO_Set_Memory_Data: not OK");
}

/***************************************************************************
 * Function : SW_Get_Pins
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_PH_GET_PINS
 *              from Physical Layer environment to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Get_Pins()
{
    int instID = acc_fetch_tfarg_int(1);

    (*(funcTray->rio_PL_Get_Pins)) (peInstancesTable[instID]->inst_Handle,
        (RIO_BYTE_T) acc_fetch_tfarg_int(2),
        (RIO_BYTE_T) acc_fetch_tfarg_int(3),
        (RIO_BYTE_T) acc_fetch_tfarg_int(4),
        (RIO_BYTE_T) acc_fetch_tfarg_int(5)
        );
}

/***************************************************************************
 * Function : SW_Switch_Get_Pins
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_PH_GET_PINS
 *              from switch to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Switch_Get_Pins()
{
    int instID;
    int link_Index; 
    
    instID = acc_fetch_tfarg_int(1);
    link_Index = acc_fetch_tfarg(2);

    (*(switchFuncTray->rio_Ph_Get_Pins)) (switchInstancesTable[instID]->link_Handles[link_Index],
        (RIO_BYTE_T) acc_fetch_tfarg_int(3),
        (RIO_BYTE_T) acc_fetch_tfarg_int(4),
        (RIO_BYTE_T) acc_fetch_tfarg_int(5),
        (RIO_BYTE_T) acc_fetch_tfarg_int(6)
        );
}

/***************************************************************************
 * Function : SW_Clock_Edge
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_Pl_Clock_Edge
 *              from Physical Layer environment to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Clock_Edge()
{
    int instID = acc_fetch_tfarg_int(1);

    (*(funcTray->rio_PL_Clock_Edge)) (peInstancesTable[instID]->inst_Handle);
}

/***************************************************************************
 * Function : SW_Switch_Clock_Edge
 *
 * Description: Software part of PLI function pair to pass RIO Request
 *                RIO_Pl_Clock_Edge
 *              from switch to RIO model  
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Switch_Clock_Edge()
{
    int instID;

    instID = acc_fetch_tfarg(1);
    (*(switchFuncTray->rio_Switch_Clock_Edge)) (switchInstancesTable[instID]->switch_Handle);
}

/************************************************************************** 
******************  PLI Data Areas access functions: **********************
**************************************************************************/ 

/***************************************************************************
 * Function : SW_Set_PLI_DW
 *
 * Description: Sets DW["index"] in the PLI data area "Data_ID"
 *                to the specified value
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Set_PLI_DW()
{
    int inst_ID = acc_fetch_tfarg_int(1);
    RIO_PLI_DATA_ID_T data_ID = acc_fetch_tfarg_int(2);
    unsigned int dw_Index = acc_fetch_tfarg_int(3);
    unsigned long ms_Word = acc_fetch_tfarg_int(4);
    unsigned long ls_Word = acc_fetch_tfarg_int(5);

    (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ms_Word = ms_Word;    
    (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ls_Word = ls_Word;    
}

/***************************************************************************
 * Function : SW_Get_PLI_DW
 *
 * Description: Gets DW["index"] from the PLI data area "Data_ID"
 *                and returns as two words in the specified registers
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Get_PLI_DW()
{
    int inst_ID = acc_fetch_tfarg_int(1);
    RIO_PLI_DATA_ID_T data_ID = acc_fetch_tfarg_int(2);
    unsigned int dw_Index = acc_fetch_tfarg_int(3);

    tf_putp(4, (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ms_Word);    
    tf_putp(5, (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ls_Word);    
}

/***************************************************************************
 * Function : SW_Get_PLI_DW_Value
 *
 * Description: Returns DW["index"] from the PLI data area "Data_ID"
 *                as 64-bit value
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Get_PLI_DW_Value()
{
    int inst_ID = acc_fetch_tfarg_int(1);
    RIO_PLI_DATA_ID_T data_ID = acc_fetch_tfarg_int(2);
    unsigned int dw_Index = acc_fetch_tfarg_int(3);

    tf_putlongp(0, (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ls_Word,
                   (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ms_Word);    
}

/***************************************************************************
 * Function : SW_Get_PLI_MS_Word
 *
 * Description: Returns MS of DW["index"] from the PLI data area "Data_ID"
 *                as 32-bit value
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Get_PLI_MS_Word()
{
    int inst_ID = acc_fetch_tfarg_int(1);
    RIO_PLI_DATA_ID_T data_ID = acc_fetch_tfarg_int(2);
    unsigned int dw_Index = acc_fetch_tfarg_int(3);

    tf_putp(0, (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ms_Word);    
}

/***************************************************************************
 * Function : SW_Get_PLI_LS_Word
 *
 * Description: Returns LS of DW["index"] from the PLI data area "Data_ID"
 *                as 32-bit value
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SW_Get_PLI_LS_Word()
{
    int inst_ID = acc_fetch_tfarg_int(1);
    RIO_PLI_DATA_ID_T data_ID = acc_fetch_tfarg_int(2);
    unsigned int dw_Index = acc_fetch_tfarg_int(3);

    tf_putp(0, (peInstancesTable[inst_ID]->pli_Data_Areas[data_ID] + dw_Index)->ls_Word);    
}

/***************************************************************************
 * Function : SW_Get_Address_Settings
 *
 * Description: Gives the following address settings for the specified PE: 
 *                ext_Address_Enabled 
 *                  ext_Address_16 (actual if ext_Address_Enabled is true)    
 *                   xamsbs_Enabled     
 *
 * Returns: void
 *
 * Notes: If the instance isn't registered yet, gives all zeros  
 *
 **************************************************************************/
void SW_Get_Address_Settings()
{
    int inst_ID = acc_fetch_tfarg_int(1);

/*    io_printf("WRAPPER: SW_Get_Address_Settings: PE %2d\n", inst_ID);   */
    if (peInstancesTable[inst_ID] == NULL)  
    {
        tf_putp(2, 0);    
        tf_putp(3, 0);    
        tf_putp(4, 0);    
    }
    else
    {
        tf_putp(2, peInstancesTable[inst_ID]->ext_Address_Enabled);    
        tf_putp(3, peInstancesTable[inst_ID]->ext_Address_16);    
        tf_putp(4, peInstancesTable[inst_ID]->xamsbs_Enabled);    
    }        
}

/***************************************************************************
 * Function : SW_Get_Tr_Info
 *
 * Description: Returns the Transport Info for the specified source and 
 *              destination PE-s 
 *
 * Returns: Requested Transport Info value (through PLI interface)
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Get_Tr_Info()
{
    int src_ID = acc_fetch_tfarg_int(1);
    int dest_ID = acc_fetch_tfarg_int(2);

    if (src_ID > total_Number_Of_PE)
        RIO_User_Msg((RIO_CONTEXT_T) src_ID, "Error: Get_Tr_Info_Value: Source PE ID is too large");

    if (dest_ID > total_Number_Of_PE)
        RIO_User_Msg((RIO_CONTEXT_T) dest_ID, "Error: Get_Tr_Info_Value: Destination PE ID is too large");
    
    if ((peInstancesTable[src_ID]->routing_Method == RIO_INDEX_BASED_ROUTING) ||
        (peInstancesTable[src_ID]->routing_Method == RIO_SHIFT_BASED_ROUTING))
    {    
        /*io_printf("Wrapper: Get_Tr_Info: src: %d dst: %d tr_info: %d\n",
            src_ID, dest_ID, peInstancesTable[src_ID]->transport_Info_Table[dest_ID]); */

        tf_putp(0, peInstancesTable[src_ID]->transport_Info_Table[dest_ID]);        
    }    
    else if (peInstancesTable[src_ID]->routing_Method == RIO_TABLE_BASED_ROUTING)    
    {
        /*io_printf("Wrapper: Get_Tr_Info: src: %d dst: %d tr_info: %d\n", src_ID, dest_ID, dev_IDs[dest_ID]); */

        tf_putp(0, dev_IDs[dest_ID]);
    }    
    else    
        RIO_User_Msg((RIO_CONTEXT_T) src_ID, "Error: This PE uses unknown routing method");
}

/************************************************************************** 
* *****************  PLI callbacks  *************************************** 
**************************************************************************/ 

/***************************************************************************
 * Function : RIO_Lg_GSM_Request_Done
 *
 * Description: Implementation of RIO callback function
 *                RIO_GSM_REQUEST_DONE
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Demo Scenario or Cache Control  
 *
 **************************************************************************/
int RIO_Lg_GSM_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    )
{
    unsigned int i;

    /*io_printf("WRAPPER: PE %2d: GSM Req DONE: Result: %2d \n", (int)context, result); */
    
    /*    peInstancesTable[instID]->pli_Data_Areas[RIO_GSM_DATA] = data;  */
    for(i = 0; i < dw_Size; i++)
    {
        (peInstancesTable[(int)context]->pli_Data_Areas[RIO_GSM_DATA] + i)->ms_Word = (data + i)->ms_Word;
        (peInstancesTable[(int)context]->pli_Data_Areas[RIO_GSM_DATA] + i)->ls_Word = (data + i)->ls_Word;
    }

    /*if (getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_GSM_Req_Done_Flag_Reg))
        RIO_Warning_Msg((int)context, "GSM request done, but previous is not catched yet"); */
        
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_GSM_Trx_Tag_Reg, trx_Tag);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_GSM_Result_Reg, result);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_GSM_Err_Code_Reg, err_Code);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_GSM_Req_Done_Flag_Reg, 1);

    /* these values shall be catched by waiting block in Verilog and passed to   */
    /* Cache Control _or_ Demo Scenario                                          */

    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_Local_Response
 *
 * Description: Implementation of RIO callback function
 *                RIO_LOCAL_RESPONSE
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Demo Scenario or Cache Control  
 *
 **************************************************************************/
int RIO_Lg_Local_Response(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_LOCAL_RTYPE_T response 
    )
{
/*    io_printf("WRAPPER: PE %d: Local Response received, response code: h%x\n", (int)context, response); */

/*    if (getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Loc_Response_Flag_Reg))
        RIO_Warning_Msg((int)context, "Local response arrived, but previous is not catched yet");   */

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Loc_Trx_Tag_Reg, trx_Tag);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Loc_Response_Reg, response);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Loc_Response_Flag_Reg, 1);

    /* these values shall be catched by waiting block in Verilog and passed to   */
    /* Cache Control _or_ Demo Scenario                                          */

    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_IO_Request_Done
 *
 * Description: Implementation of RIO callback function
 *                RIO_IO_REQUEST_DONE
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Demo Scenario or DMA control  
 *
 **************************************************************************/
int RIO_Lg_IO_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,           
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    )
{
    unsigned int i;

    /*io_printf("WRAPPER: PE %2d: IO Req DONE: Result: %2d \n", (int)context, result); */

    /*    peInstancesTable[instID]->pli_Data_Areas[RIO_IO_DATA] = data;  */
    for(i = 0; i < dw_Size; i++)
    {
        (peInstancesTable[(int)context]->pli_Data_Areas[RIO_IO_DATA] + i)->ms_Word = (data + i)->ms_Word;
        (peInstancesTable[(int)context]->pli_Data_Areas[RIO_IO_DATA] + i)->ls_Word = (data + i)->ls_Word;
    }
    
/*    if (getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_IO_Req_Done_Flag_Reg))
        RIO_Warning_Msg((int)context, "IO request done, but previous is not catched yet");  */

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_IO_Trx_Tag_Reg, trx_Tag);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_IO_Result_Reg, result);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_IO_Err_Code_Reg, err_Code);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_IO_Req_Done_Flag_Reg, 1);

    /* these values shall be catched by waiting block in Verilog and passed to   */
    /* DMA Control _or_ Demo Scenario                                            */

    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_MP_Request_Done
 *
 * Description: Implementation of RIO callback function
 *                RIO_MP_REQUEST_DONE
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Demo Scenario  
 *
 **************************************************************************/
int RIO_Lg_MP_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code
    )
{
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_Trx_Tag_Reg, trx_Tag);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_Result_Reg, result);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_Err_Code_Reg, err_Code);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_Req_Done_Flag_Reg, 1);

    /*io_printf("WRAPPER: PE %2d: MSG Req DONE: Result: %2d \n", (int)context, result); */
    
    /* these values shall be catched by waiting block in Verilog and passed to   */
    /* Demo Scenario                                                             */

    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_Doorbell_Request_Done
 *
 * Description: Implementation of RIO callback function
 *                RIO_DOORBELL_REQUEST_DONE
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Demo Scenario or DMA control  
 *
 **************************************************************************/
int RIO_Lg_Doorbell_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code           
    )
{
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_DB_Trx_Tag_Reg, trx_Tag);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_DB_Result_Reg, result);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_DB_Err_Code_Reg, err_Code);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_DB_Req_Done_Flag_Reg, 1);

    /*io_printf("WRAPPER: PE %2d: DB Req DONE: Result: %2d \n", (int)context, result); */
    
    /* these values shall be catched by waiting block in Verilog and passed to   */
    /* Demo Scenario _or_ DMA Control                                            */

    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_Config_Request_Done
 *
 * Description: Implementation of RIO callback function
 *                RIO_CONFIG_REQUEST_DONE
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Demo Scenario  
 *
 **************************************************************************/
int RIO_Lg_Config_Request_Done(
    RIO_CONTEXT_T     context,
    RIO_TAG_T         trx_Tag, 
    RIO_REQ_RESULT_T  result, 
    RIO_UCHAR_T       err_Code,           
    RIO_WORD_T        dw_Size,
    RIO_DW_T          *data
    )
{
    unsigned int i;

    if (data)
    {
        /*    peInstancesTable[(int)context]->pli_Data_Areas[RIO_CONFIG_DATA] = data;  */
        for(i = 0; i < dw_Size; i++)
        {
            (peInstancesTable[(int)context]->pli_Data_Areas[RIO_CONFIG_DATA] + i)->ms_Word = (data + i)->ms_Word;
            (peInstancesTable[(int)context]->pli_Data_Areas[RIO_CONFIG_DATA] + i)->ls_Word = (data + i)->ls_Word;
        }
    }

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Config_Trx_Tag_Reg, trx_Tag);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Config_Result_Reg, result);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Config_Err_Code_Reg, err_Code);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Config_Req_Done_Flag_Reg, 1);

    /*io_printf("WRAPPER: PE %2d: Cfg Req DONE: Result: %2d \n", (int)context, result); */
    
    /* these values shall be catched by waiting block in Verilog and passed to   */
    /* Demo Scenario                                                             */

    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_Snoop_Request
 *
 * Description: Implementation of RIO callback function
 *                RIO_SNOOP_REQUEST
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Cache Control (snooper) 
 *
 **************************************************************************/
int RIO_Lg_Snoop_Request(
    RIO_CONTEXT_T      context,
    RIO_SNOOP_REQ_T    req
    )
{
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Snoop_Address_Reg, req.address);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Snoop_Ext_Address_Reg, req.extended_Address);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Snoop_XAMSBS_Reg, req.xamsbs);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Snoop_LTType_Reg, req.lttype);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Snoop_TType_Reg, req.ttype);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Snoop_Req_Flag_Reg, 1);

    /* these values shall be catched by Cache Control block in Verilog */

    /*io_printf("WRAPPER: PE %2d: SNOOP REQUEST: req: %3d, address: h %x \n", (int)context, req.lttype, req.address); */
    
    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_MP_Remote_Request
 *
 * Description: Implementation of RIO callback function
 *              RIO_MP_REMOTE_REQUEST
 *
 * Returns: RIO_OK, RIO_RETRY or RIO_ERROR
 *
 * Notes: Receiver: Mailbox Control  
 *
 **************************************************************************/
int RIO_Lg_MP_Remote_Request(
    RIO_CONTEXT_T       context,
    RIO_REMOTE_MP_REQ_T message
    )
{
    int i;

    /* Check current status of the specified mailbox */ 

    unsigned long cur_CSR = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mailbox_CSR);
    RIO_BOOL_T mailbox_Busy = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_Flag_Reg);

    /*io_printf("WRAPPER: PE %d: Remote MP Request received\n", (int)context); */

    if (mailbox_Busy)
    {
        io_printf("WRAPPER: PE %d: Remote MP Request: RETRIED\n", (int)context);
        return RIO_RETRY;
    }
    
    if (cur_CSR >> (RIO_PLI_LONG_REGISTER_SIZE - (RIO_MB_BITS_SHIFT * message.mbox + RIO_MB_FAILED_BIT_POS)) & 1)
        return RIO_ERROR;

    if (cur_CSR >> (RIO_PLI_LONG_REGISTER_SIZE - (RIO_MB_BITS_SHIFT * message.mbox + RIO_MB_FULL_BIT_POS)) & 1)
        return RIO_RETRY;

    /*    io_printf("WRAPPER: PE %d: Remote MP Request passed to HW \n", (int)context);   */

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_MSGLEN_Reg, message.msglen);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_SSIZE_Reg, message.ssize);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_LETTER_Reg, message.letter);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_MBOX_Reg, message.mbox);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_MSGSEG_Reg, message.msgseg);

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_DW_Size_Reg, message.dw_Size);

    /*    peInstancesTable[instID]->pli_Data_Areas[RIO_MP_DATA] = data;  */
    for(i = 0; i < message.dw_Size; i++)
    {
        (peInstancesTable[(int)context]->pli_Data_Areas[RIO_MP_DATA] + i)->ms_Word = (message.data + i)->ms_Word;
        (peInstancesTable[(int)context]->pli_Data_Areas[RIO_MP_DATA] + i)->ls_Word = (message.data + i)->ls_Word;
    }

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_MP_Flag_Reg, 1);

    /* these values shall be catched by Mailbox Control block in Verilog */

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_Lg_Doorbell_Remote_Request
 *
 * Description: Implementation of RIO callback function
 *                RIO_DOORBELL_REMOTE_REQUEST
 *
 * Returns: RIO_OK, RIO_RETRY or RIO_ERROR
 *
 * Notes: Receiver: Mailbox Control  
 *
 **************************************************************************/
int RIO_Lg_Doorbell_Remote_Request(
    RIO_CONTEXT_T  context,
    RIO_DB_INFO_T  doorbell_Info
    )
{
    /* Check current status of the doorbell hardware */ 

    unsigned long cur_CSR = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.doorbell_CSR);


    if (cur_CSR >> (RIO_PLI_LONG_REGISTER_SIZE - RIO_MB_FAILED_BIT_POS) & 1)
        return RIO_ERROR;

    if (cur_CSR >> (RIO_PLI_LONG_REGISTER_SIZE - RIO_MB_FULL_BIT_POS) & 1)
        return RIO_RETRY;

    if (cur_CSR >> (RIO_PLI_LONG_REGISTER_SIZE - RIO_MB_BUSY_BIT_POS) & 1)
        return RIO_RETRY;


    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_DB_Info_Reg, doorbell_Info);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_DB_Flag_Reg, 1);


    /* these values shall be catched by Mailbox Control block in Verilog */

    return RIO_OK;
}

/***************************************************************************
 * Function : RIO_Lg_Memory_Request
 *
 * Description: Implementation of RIO callback function
 *                RIO_MEMORY_REQUEST
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Memory Control  
 *
 **************************************************************************/
int RIO_Lg_Memory_Request(
    RIO_CONTEXT_T           context,
    RIO_ADDRESS_T          address,                 
    RIO_ADDRESS_T          extended_Address,
    RIO_UCHAR_T            xamsbs,
    RIO_UCHAR_T            dw_Size,              
    RIO_MEMORY_REQ_TYPE_T  req_Type,             
    RIO_BOOL_T             is_GSM,               
    RIO_UCHAR_T            be                    
    )
{
/*    RIO_BOOL_T memory_Busy = getIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Flag_Reg);  */

/*    io_printf("WRAPPER: PE %d: (Remote) Memory Request received, address: h%x\n", (int)context, (int)address); */

/*    if (memory_Busy)
    {
        io_printf("WRAPPER: PE %d: (Remote) Memory Request: RETRIED\n", (int)context);
        return RIO_RETRY;
    }   */

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_Address_Reg, address);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_Ext_Address_Reg, extended_Address);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_XAMSBS_Reg, xamsbs);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_DW_Size_Reg, dw_Size);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_Type_Reg, req_Type);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_Is_GSM_Reg, is_GSM);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_BE_Reg, be);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Mem_Flag_Reg, 1);

    /* these values shall be catched by Memory Control block in Verilog */

    return 0;
}

/***************************************************************************
 * Function : RIO_Lg_Read_Directory
 *
 * Description: Implementation of RIO callback function
 *                RIO_READ_DIRECTORY
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Memory Control    
 *
 **************************************************************************/
int RIO_Lg_Read_Directory(
    RIO_CONTEXT_T  context, 
    RIO_ADDRESS_T  address,         
    RIO_ADDRESS_T  ext_Address,
    RIO_UCHAR_T    xamsbs,
    RIO_SH_MASK_T  *sharing_Mask    
    )
{
    unsigned long index;

    unsigned int gsm_XAMSBS = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_XAMSBS);
    unsigned long gsm_Ext = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_Ext_Address);
    unsigned long gsm_Base = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_Start_Address);
    unsigned long gsm_Size = RIO_BYTES_IN_DW *
        getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_DW_Size);

/*    io_printf("WRAPPER: PE %2d: Read_Directory request\n", (int)context); */
    
    if (((peInstancesTable[(int)context]->xamsbs_Enabled == RIO_FALSE) || (xamsbs == gsm_XAMSBS)) &&
        ((peInstancesTable[(int)context]->ext_Address_Enabled == RIO_FALSE) || (ext_Address == gsm_Ext)) &&
        ((address - gsm_Base) >= 0) &&
        ((address - gsm_Base) < gsm_Size))
    {
        index = (address - gsm_Base) / (RIO_BYTES_IN_DW * peInstancesTable[(int)context]->coh_Granule_DW_Size);
        if (index < peInstancesTable[(int)context]->memory_Directory_Size)
        {
            *sharing_Mask = peInstancesTable[(int)context]->memory_Directory[index];
/*            io_printf("WRAPPER: PE %2d: Read_Directory: h%x\n", (int)context, *sharing_Mask);  */
            return RIO_OK;
        }
        else
        {
            RIO_User_Msg(context, "Error: Read_Directory: out of memory directory");
/*          io_printf("index = %d, address = h %x, base = h %x, dir_size = %d\n",    
                index, address, gsm_Base, peInstancesTable[(int)context]->memory_Directory_Size);   */
            return RIO_ERROR;
        }
    }
    else
    {
        RIO_User_Msg(context, "Error: Read_Directory: address is out of GSM space");
        return RIO_ERROR;
    }
}

/***************************************************************************
 * Function : RIO_Lg_Write_Directory
 *
 * Description: Implementation of RIO callback function
 *                RIO_WRITE_DIRECTORY
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Memory Control    
 *
 **************************************************************************/
int RIO_Lg_Write_Directory(
    RIO_CONTEXT_T  context, 
    RIO_ADDRESS_T  address,         
    RIO_ADDRESS_T  ext_Address,
    RIO_UCHAR_T    xamsbs,
    RIO_SH_MASK_T  sharing_Mask    
    )
{
    unsigned long index;

    unsigned int gsm_XAMSBS = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_XAMSBS);
    unsigned long gsm_Ext = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_Ext_Address);
    unsigned long gsm_Base = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_Start_Address);
    unsigned long gsm_Size = RIO_BYTES_IN_DW * 
        getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_DW_Size);

/*    io_printf("WRAPPER: PE %2d: Write_Directory request, mask: h%x\n", (int)context, sharing_Mask); */
    
    if (((peInstancesTable[(int)context]->xamsbs_Enabled == RIO_FALSE) || (xamsbs == gsm_XAMSBS)) &&
        ((peInstancesTable[(int)context]->ext_Address_Enabled == RIO_FALSE) || (ext_Address == gsm_Ext)) &&
        ((address - gsm_Base) >= 0) &&
        ((address - gsm_Base) < gsm_Size))
    {
        index = (address - gsm_Base) / (RIO_BYTES_IN_DW * peInstancesTable[(int)context]->coh_Granule_DW_Size);
        if (index < peInstancesTable[(int)context]->memory_Directory_Size)
        {
            peInstancesTable[(int)context]->memory_Directory[index] = sharing_Mask;
            return RIO_OK;
        }
        else
        {
            RIO_User_Msg(context, "Error: Write_Directory: out of memory directory");
            return RIO_ERROR;
        }
    }
    else
    {
        RIO_User_Msg(context, "Error: Write_Directory: address is out of GSM space");
        return RIO_ERROR;
    }
}

/***************************************************************************
 * Function : RIO_Set_Pins
 *
 * Description: Implementation of RIO callback function
 *                RIO_PL_SET_PINS
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Processing Element (physical layer interface)    
 *
 **************************************************************************/
int RIO_Set_Pins(
    RIO_CONTEXT_T context, 
    RIO_BYTE_T tframe,     
    RIO_BYTE_T td,         
    RIO_BYTE_T tdl,        
    RIO_BYTE_T tclk )       
{

    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Pins_TFrame_Reg, tframe);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Pins_TD_Reg, td);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Pins_TDL_Reg, tdl);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Pins_TCLK_Reg, tclk);
    setIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.pli_Pins_Flag_Reg, 1);

    /* these values shall be catched by Processing Element in Verilog */

    return 0;
}

/***************************************************************************
 * Function : RIO_Switch_Set_Pins
 *
 * Description: Implementation of RIO callback function
 *                RIO_PH_SET_PINS for switch
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: Switch (physical layer interface)    
 *
 **************************************************************************/
int RIO_Switch_Set_Pins(
    RIO_CONTEXT_T context,
    RIO_BYTE_T tframe,
    RIO_BYTE_T td,
    RIO_BYTE_T tdl,
    RIO_BYTE_T tclk
    )
{
    int switch_Context;
    int link_Index;
    
    unsigned int cur_TFrame;
    unsigned int cur_TCLK;
    unsigned int cur_Flag;

    switch_Context = (int)context / RIO_SWITCH_CONTEXT_MULTIPLIER;
    link_Index = (int)context - switch_Context * RIO_SWITCH_CONTEXT_MULTIPLIER;

    setIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_TD_Regs[link_Index], td);
    setIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_TDL_Regs[link_Index], tdl);
    
    cur_TFrame = getIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_TFrame_Reg);
    if (tframe == 1)
        cur_TFrame |= 1 << (RIO_SWITCH_NUM_OF_LINKS - 1 - link_Index);
    else
        cur_TFrame &= ~(1 << (RIO_SWITCH_NUM_OF_LINKS - 1 - link_Index));
    setIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_TFrame_Reg, cur_TFrame);

    cur_TCLK = getIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_TCLK_Reg);
    if (tclk == 1)
        cur_TCLK |= 1 << (RIO_SWITCH_NUM_OF_LINKS - 1 - link_Index);
    else
        cur_TCLK &= ~(1 << (RIO_SWITCH_NUM_OF_LINKS - 1 - link_Index));
    setIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_TCLK_Reg, cur_TCLK);

    cur_Flag = getIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_Flag_Reg);
/*    if ((cur_Flag & (1 << (RIO_SWITCH_NUM_OF_LINKS - 1 - link_Index))) != 0)
        RIO_Warning_Msg((int)context, "New SWITCH SET PINS request arrived, but previous is not catched yet"); */
    cur_Flag |= 1 << (RIO_SWITCH_NUM_OF_LINKS - 1 - link_Index);
    setIntegerRegValue(switchInstancesTable[switch_Context]->pli_Reg_Handles.pli_Pins_Flag_Reg, cur_Flag);
    
    return 0;
}

/************************************************************************** 
* User-defined callbacks: address translation and remote config. requests *
**************************************************************************/ 

/***************************************************************************
 * Function : RIO_Lg_Translate_Remote_IO_Req
 *
 * Description: Implementation of RIO callback function
 *                RIO_TRANSLATE_REMOTE_IO_REQ
 *
 * Returns: 0 - translation done, ok , 1 - error, no translation performed        
 *
 * Notes: Receiver: no 
 *
 **************************************************************************/
int RIO_Lg_Translate_Remote_IO_Req(
    RIO_CONTEXT_T    context, 
    RIO_ADDRESS_T    *address,           /* callback gets original address and returns translated address */
    RIO_ADDRESS_T    *ext_Address,
    RIO_UCHAR_T      *xamsbs
    )
{
    unsigned int io_XAMSBS = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_XAMSBS);
    unsigned long io_Ext = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_Ext_Address);
    unsigned long io_Base = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_Start_Address);
    unsigned long io_Size = RIO_BYTES_IN_DW *
        getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_DW_Size);

/*    io_printf("WRAPPER: PE %d: Request to translate remote IO address h%x:\n", (int)context, *address); */
/*    io_printf("WRAPPER: PE %d: Base = h%x, Size = %lu bytes\n", (int)context, io_Base, io_Size);  */

    if (((peInstancesTable[(int)context]->xamsbs_Enabled == RIO_FALSE) || (*xamsbs == io_XAMSBS)) &&
        ((peInstancesTable[(int)context]->ext_Address_Enabled == RIO_FALSE) || (*ext_Address == io_Ext)) &&
        ((*address - io_Base) >= 0) &&
        ((*address - io_Base) < io_Size))
    {
        *address -= io_Base; 
        *ext_Address = 0;
        *xamsbs = 0;
/*        io_printf("WRAPPER: PE %d: Rem IO Request translated OK : h%x\n", (int)context, *address); */
        return 0;  /* this is local IO address */
    }
    else
    {
/*        io_printf("WRAPPER: PE %d: Rem ADDR = h%x, IO_BASE = h%x, IO_SIZE = %d\n", parentID, *address, io_Base, io_Size);  */
/*        io_printf("WRAPPER: PE %d: Rem IO Request translation: MISSED\n", (int)context);  */
        return 1;  /* error, this is not local IO address */
    }
}

/***************************************************************************
 * Function : RIO_Lg_Translate_Local_IO_Req
 *
 * Description: Implementation of RIO callback function
 *                RIO_TRANSLATE_LOCAL_IO_REQ
 *
 * Returns: RIO_ROUTE_TO_LOCAL if local access, RIO_ROUTE_TO_REMOTE if remote      
 *
 * Notes: Receiver: no    
 *
 **************************************************************************/
int RIO_Lg_Translate_Local_IO_Req(
    RIO_CONTEXT_T    context, 
    RIO_ADDRESS_T    *address,           /* callback gets original address and returns translated address */
    RIO_ADDRESS_T    *ext_Address,
    RIO_UCHAR_T      *xamsbs,
    RIO_TR_INFO_T    *transport_Info     /* transport_info field of the packet (for instance, device id)  */
    )
{
    unsigned int io_XAMSBS = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_XAMSBS);
    unsigned long io_Ext = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_Ext_Address);
    unsigned long io_Base = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_Start_Address);
    unsigned long io_Size = RIO_BYTES_IN_DW *
        getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_IO_Space_DW_Size);

    int i;
    RIO_ADDRESS_T    tmp_Address = *address;           
    RIO_ADDRESS_T    tmp_Extended_Address = *ext_Address;
    RIO_UCHAR_T      tmp_XAMSBS = *xamsbs; 

/*    io_printf("WRAPPER: PE %d: Request to translate local IO address h%x:\n", (int)context, *address); */
/*    io_printf("WRAPPER: PE %d: Local Base = h%x, Size = %lu bytes\n", parentID, io_Base, io_Size);  */

    if (((peInstancesTable[(int)context]->xamsbs_Enabled == RIO_FALSE) || (*xamsbs == io_XAMSBS)) &&
        ((peInstancesTable[(int)context]->ext_Address_Enabled == RIO_FALSE) || (*ext_Address == io_Ext)) &&
        ((*address - io_Base) >= 0) &&
        ((*address - io_Base) < io_Size))
    {
        *address -= io_Base; 
        *ext_Address = 0;
        *xamsbs = 0;

/*        io_printf("WRAPPER: PE %d: Loc IO Request translated as local: h%x\n", (int)context, *address); */
        return RIO_ROUTE_TO_LOCAL;  /* this is local IO address */
    }
    else
    {
        *transport_Info = 0;
        for (i = 1; i <= total_Number_Of_PE; i++)
        {
            if ((peInstancesTable[i] != NULL) && (i != (int)context)) 
                if (RIO_Lg_Translate_Remote_IO_Req((RIO_CONTEXT_T) i, &tmp_Address, &tmp_Extended_Address, &tmp_XAMSBS) == 0)
                {   /* address is local for this PE: so this PE is the destination for the request  */
                    if ((peInstancesTable[(int)context]->routing_Method == RIO_INDEX_BASED_ROUTING) ||
                        (peInstancesTable[(int)context]->routing_Method == RIO_SHIFT_BASED_ROUTING))
                        *transport_Info = peInstancesTable[(int)context]->transport_Info_Table[i];
                    else    
                        *transport_Info = dev_IDs[i];
/*                  io_printf("WRAPPER: PE %d: Loc IO Request translated as remote, tr_info: %d\n", (int)context, *transport_Info); */
                    return RIO_ROUTE_TO_REMOTE;
                }
        }
        
        io_printf("WRAPPER: PE %d: Loc IO Request translation FAILED, suppose request is remote\n", (int)context);
        return RIO_ROUTE_TO_REMOTE;  /* this is not local IO address */
    }
}

/***************************************************************************
 * Function : RIO_Lg_Route_GSM_Req
 *
 * Description: Implementation of RIO callback function
 *                RIO_ROUTE_GSM_REQ
 *
 * Returns: RIO_ROUTE_TO_LOCAL if local access, RIO_ROUTE_TO_REMOTE if remote      
 *
 * Notes: Receiver: no    
 *
 **************************************************************************/
int RIO_Lg_Route_GSM_Req(
    RIO_CONTEXT_T   context,
    RIO_ADDRESS_T   address,             /* address of the local GSM request */
    RIO_ADDRESS_T   ext_Address,
    RIO_UCHAR_T     xamsbs,
    RIO_TR_INFO_T   *transport_Info      /* transport_info field of the packet (for instance, device id)  */
    )
{
    int i;

    unsigned int gsm_XAMSBS = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_XAMSBS);
    unsigned long gsm_Ext = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_Ext_Address);
    unsigned long gsm_Base = getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_Start_Address);
    unsigned long gsm_Size = RIO_BYTES_IN_DW *
        getIntegerRegValue(peInstancesTable[(int)context]->pli_Reg_Handles.mem_GSM_Space_DW_Size);

/*    io_printf("WRAPPER: PE %d: Request to route GSM address h%x\n", (int)context, address); */

    if (((peInstancesTable[(int)context]->xamsbs_Enabled == 0) || (xamsbs == gsm_XAMSBS)) &&
        ((peInstancesTable[(int)context]->ext_Address_Enabled == 0) || (ext_Address == gsm_Ext)) &&
        (gsm_Base != 0) && ((address - gsm_Base) >= 0) && ((address - gsm_Base) < gsm_Size))
    {
/*        io_printf("WRAPPER: PE %d: GSM Request translated as local\n", (int)context); */
        return RIO_ROUTE_TO_LOCAL;  /* this is local GSM address */
    }
    else
    {
        *transport_Info = 0;
        for (i = 1; i <= total_Number_Of_PE; i++)
        {
            if ((peInstancesTable[i] != NULL) && (i != (int)context)) 
            {
                gsm_XAMSBS = getIntegerRegValue(peInstancesTable[i]->pli_Reg_Handles.mem_GSM_Space_XAMSBS);
                gsm_Ext = getIntegerRegValue(peInstancesTable[i]->pli_Reg_Handles.mem_GSM_Space_Ext_Address);
                gsm_Base = getIntegerRegValue(peInstancesTable[i]->pli_Reg_Handles.mem_GSM_Space_Start_Address);
                gsm_Size = RIO_BYTES_IN_DW * getIntegerRegValue(peInstancesTable[i]->pli_Reg_Handles.mem_GSM_Space_DW_Size);

                if (((peInstancesTable[i]->xamsbs_Enabled == 0) || (xamsbs == gsm_XAMSBS)) &&
                    ((peInstancesTable[i]->ext_Address_Enabled == 0) || (ext_Address == gsm_Ext)) &&
                     ((address - gsm_Base) >= 0) &&
                    ((address - gsm_Base) < gsm_Size))
                {   /* address is local for this PE: so this PE is the destination for the request */
                    if ((peInstancesTable[(int)context]->routing_Method == RIO_INDEX_BASED_ROUTING) ||
                        (peInstancesTable[(int)context]->routing_Method == RIO_SHIFT_BASED_ROUTING))
                        *transport_Info = peInstancesTable[(int)context]->transport_Info_Table[i];
                    else    
                        *transport_Info = dev_IDs[i];
/*                      io_printf("WRAPPER: PE %d: GSM Request translated as remote, tr_info: %d\n", (int)context, *transport_Info); */
                    return RIO_ROUTE_TO_REMOTE;
                }
            }
        }
        io_printf("WRAPPER: PE %d: GSM Request translation FAILED, suppose request is remote\n", (int)context);
        return RIO_ROUTE_TO_REMOTE;  /* this is not local GSM address */
    }
}

/***************************************************************************
 * Function : RIO_Lg_Remote_Config_Read
 *
 * Description: Implementation of RIO callback function
 *                RIO_REMOTE_CONFIG_READ
 *
 * Returns: RIO_ERROR or RIO_OK
 *
 * Notes: Receiver: no
 *
 **************************************************************************/
int RIO_Lg_Remote_Config_Read(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset,   /* double-word aligned offset inside configuration space */
    unsigned long    *data
    )
{
    if (accessConfigReg(context, config_Offset, CONFIG_READ, data) == RIO_REQ_DONE)
        return RIO_OK;
    else
        return RIO_ERROR;
}

/***************************************************************************
 * Function : RIO_Lg_Remote_Config_Write
 *
 * Description: Implementation of RIO callback function
 *                RIO_REMOTE_CONFIG_WRITE
 *
 * Returns: RIO_ERROR or RIO_OK
 *
 * Notes: Receiver: no
 *
 **************************************************************************/
int RIO_Lg_Remote_Config_Write(
    RIO_CONTEXT_T    context,
    RIO_CONF_OFFS_T  config_Offset, /* double-word aligned offset inside configuration space */
    unsigned long    data
    )
{
    if (accessConfigReg(context, config_Offset, CONFIG_WRITE, &data) == RIO_REQ_DONE)
        return RIO_OK;
    else
        return RIO_ERROR;

}

/************************************************************************** 
**************************  message callbacks   ***************************
**************************************************************************/ 

/***************************************************************************
 * Function : RIO_User_Msg
 *
 * Description: Implementation of RIO callback function
 *                RIO_USER_MSG
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: no
 *
 **************************************************************************/
int RIO_User_Msg(RIO_CONTEXT_T context, char *user_Msg)
{
    io_printf("PE %d: %s\n", (int)context, user_Msg);

    return 0;
}

/***************************************************************************
 * Function : RIO_Switch_User_Msg
 *
 * Description: Implementation of RIO callback function
 *                RIO_USER_MSG for switch
 *
 * Returns: result code (0 if OK)
 *
 * Notes: Receiver: no
 *
 **************************************************************************/
int RIO_Switch_User_Msg(RIO_CONTEXT_T  context, char * user_Msg)
{
    io_printf("SWITCH %d: %s\n", (int)context, user_Msg);

    return 0;
}

/************************************************************************** 
*****  Special functions: Instance registration, kill, reset   ************
**************************************************************************/ 

/***************************************************************************
 * Function : SW_Register_Instance
 *
 * Description: Software part of PLI function pair to provide registration
 *                of the PE instance in the RIO model and PLI wrapper
 *
 * Returns: void
 *
 * Notes: Instance ID shall be passed from Verilog as
 *        the first parameter of this function   
 *
 **************************************************************************/
void SW_Register_Instance()
{
    RIO_MODEL_INST_PARAM_T parameters;
    int i;
    int k;        /* Corrector of the current argument number */
    int instID;                        
    RIO_MODEL_CALLBACK_TRAY_T * callbackTray;
    unsigned long tmp_DW;
    handle tmpPLIHandle;

    instID = acc_fetch_tfarg_int(1);
    if (instID > total_Number_Of_PE)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: PE instance ID is too large");

/*    io_printf("PLI WRAPPER: PE %d registration is started\n", instID);   */

    if (peInstancesTable[instID] != NULL)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Warning: PE is already registered and will be re-registered!");

    peInstancesTable[instID] = (RIO_PE_INSTANCES_TABLE_T *) malloc(sizeof(RIO_PE_INSTANCES_TABLE_T));

    peInstancesTable[instID]->transport_Info_Table = (RIO_TR_INFO_T *) malloc((total_Number_Of_PE + 1) * sizeof(RIO_TR_INFO_T));
    
    peInstancesTable[instID]->ext_Address_Enabled = 0;                        
    peInstancesTable[instID]->ext_Address_16 = 0;                            
    peInstancesTable[instID]->xamsbs_Enabled = 1;                            

    /* Reservate memory for PLI data areas */
    for (i = 0; i <= RIO_MAX_PLI_DATA_ID; i++)
        peInstancesTable[instID]->pli_Data_Areas[i] = (RIO_DW_T *) malloc(RIO_MAX_DW_SIZE * RIO_BYTES_IN_DW);

    /* obtain PLI handle and name for the PE instance */ 

    tmpPLIHandle = acc_handle_tfarg(2); /* Instance PLI handle */
    peInstancesTable[instID]->pli_Inst_Handle = tmpPLIHandle;
    sprintf(peInstancesTable[instID]->inst_Name, "%s", acc_fetch_name(tmpPLIHandle));

    /* obtain instance parameters (instance constants) */    

    k = 0;

    parameters.ext_Address_Support = acc_fetch_tfarg_int(k + 3); 
    parameters.is_Bridge = acc_fetch_tfarg_int(k + 4);           
    parameters.has_Memory = acc_fetch_tfarg_int(k + 5);          
    parameters.has_Processor = acc_fetch_tfarg_int(k + 6);       
    parameters.coh_Granule_Size_32 = acc_fetch_tfarg_int(k + 7); 

/*    parameters.component_ID = acc_fetch_tfarg_int(k + 8);
    parameters.component_Info = acc_fetch_tfarg_int(k + 9);
    parameters.assembly_ID = acc_fetch_tfarg_int(k + 10);
    parameters.assembly_Info = acc_fetch_tfarg_int(k + 11);*/
	k = k - 4;

    parameters.source_Trx = acc_fetch_tfarg_int(k + 12);
    parameters.dest_Trx = acc_fetch_tfarg_int(k + 13);
/*    parameters.max_Source_Op_Sizes = acc_fetch_tfarg_int(k + 14);
    parameters.max_Dest_Op_Sizes = acc_fetch_tfarg_int(k + 15);
    parameters.max_Source_MP_Op_Sizes = acc_fetch_tfarg_int(k + 16);
    parameters.max_Dest_MP_Op_Sizes = acc_fetch_tfarg_int(k + 17);*/
	k = k - 4;
    parameters.mbox1 = acc_fetch_tfarg_int(k + 18);
    parameters.mbox2 = acc_fetch_tfarg_int(k + 19);
    parameters.mbox3 = acc_fetch_tfarg_int(k + 20);
    parameters.mbox4 = acc_fetch_tfarg_int(k + 21);
    parameters.has_Doorbell = acc_fetch_tfarg_int(k + 22);
    parameters.pl_Inbound_Buffer_Size = acc_fetch_tfarg_int(k + 23);
    parameters.pl_Outbound_Buffer_Size = acc_fetch_tfarg_int(k + 24);
    parameters.ll_Inbound_Buffer_Size = acc_fetch_tfarg_int(k + 25);
    parameters.ll_Outbound_Buffer_Size = acc_fetch_tfarg_int(k + 26);
    parameters.coh_Domain_Size = acc_fetch_tfarg_int(k + 27);

/* New parameters*/
    parameters.device_Identity = (RIO_WORD_T)acc_fetch_tfarg_int(k + 28);
    parameters.device_Vendor_Identity = (RIO_WORD_T)acc_fetch_tfarg_int(k + 29);
    parameters.device_Rev = (RIO_WORD_T)acc_fetch_tfarg_int(k + 30);
    parameters.device_Minor_Rev = (RIO_UCHAR_T)acc_fetch_tfarg_int(k + 31);
    parameters.device_Major_Rev = (RIO_UCHAR_T)acc_fetch_tfarg_int(k + 32);
    parameters.assy_Identity = (RIO_WORD_T)acc_fetch_tfarg_int(k + 33);   
    parameters.assy_Vendor_Identity = (RIO_WORD_T)acc_fetch_tfarg_int(k + 34);
    parameters.assy_Rev = (RIO_WORD_T)acc_fetch_tfarg_int(k + 35);
	parameters.entry_Extended_Features_Ptr = (RIO_WORD_T)acc_fetch_tfarg_int(k + 36); /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	parameters.next_Extended_Features_Ptr = (RIO_WORD_T)acc_fetch_tfarg_int(k + 37);	/*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
    k = k + 38;

/*
    peInstancesTable[instID]->pli_Reg_Handles.pli_MS_Word_Data_Regs = acc_handle_tfarg(k);
    peInstancesTable[instID]->pli_Reg_Handles.pli_LS_Word_Data_Regs = acc_handle_tfarg(k + 1);
*/  k = k - 2;

    peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Trx_Tag_Reg = acc_handle_tfarg(k + 2);
    peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Result_Reg = acc_handle_tfarg(k + 3);
    peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Err_Code_Reg = acc_handle_tfarg(k + 4);
/*
    peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_MS_Word_Data_Regs = acc_handle_tfarg(k + 5);
    peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_LS_Word_Data_Regs = acc_handle_tfarg(k + 6);
*/    k = k - 2;  

    peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Req_Done_Flag_Reg = acc_handle_tfarg(k + 7);

    peInstancesTable[instID]->pli_Reg_Handles.pli_Loc_Trx_Tag_Reg = acc_handle_tfarg(k + 8);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Loc_Response_Reg = acc_handle_tfarg(k + 9);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Loc_Response_Flag_Reg = acc_handle_tfarg(k + 10);

    peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Trx_Tag_Reg = acc_handle_tfarg(k + 11); 
    peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Result_Reg = acc_handle_tfarg(k + 12);
    peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Err_Code_Reg = acc_handle_tfarg(k + 13);

/*
    peInstancesTable[instID]->pli_Reg_Handles.pli_IO_MS_Word_Data_Regs = acc_handle_tfarg(k + 14);
    peInstancesTable[instID]->pli_Reg_Handles.pli_IO_LS_Word_Data_Regs = acc_handle_tfarg(k + 15);
*/  k = k - 2;

    peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Req_Done_Flag_Reg = acc_handle_tfarg(k + 16);

    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Trx_Tag_Reg = acc_handle_tfarg(k + 17); 
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Result_Reg = acc_handle_tfarg(k + 18);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Err_Code_Reg = acc_handle_tfarg(k + 19);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Req_Done_Flag_Reg = acc_handle_tfarg(k + 20);

    peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Trx_Tag_Reg = acc_handle_tfarg(k + 21); 
    peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Result_Reg = acc_handle_tfarg(k + 22);
    peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Err_Code_Reg = acc_handle_tfarg(k + 23);
    peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Req_Done_Flag_Reg = acc_handle_tfarg(k + 24);

    peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Trx_Tag_Reg = acc_handle_tfarg(k + 25); 
    peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Result_Reg = acc_handle_tfarg(k + 26);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Err_Code_Reg = acc_handle_tfarg(k + 27);
/*
    peInstancesTable[instID]->pli_Reg_Handles.pli_Config_MS_Word_Data_Regs = acc_handle_tfarg(k + 28);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Config_LS_Word_Data_Regs = acc_handle_tfarg(k + 29);
*/  k = k - 2;

    peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Req_Done_Flag_Reg = acc_handle_tfarg(k + 30);

    peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_Address_Reg = acc_handle_tfarg(k + 31);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_Ext_Address_Reg = acc_handle_tfarg(k + 32);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_XAMSBS_Reg = acc_handle_tfarg(k + 33);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_LTType_Reg = acc_handle_tfarg(k + 34);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_TType_Reg = acc_handle_tfarg(k + 35);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_Req_Flag_Reg = acc_handle_tfarg(k + 36);

    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_MSGLEN_Reg = acc_handle_tfarg(k + 37);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_SSIZE_Reg = acc_handle_tfarg(k + 38);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_LETTER_Reg = acc_handle_tfarg(k + 39);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_MBOX_Reg = acc_handle_tfarg(k + 40);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_MSGSEG_Reg = acc_handle_tfarg(k + 41);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_DW_Size_Reg = acc_handle_tfarg(k + 42);
/*
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_MS_Word_Data_Regs = acc_handle_tfarg(k + 43);
    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_LS_Word_Data_Regs = acc_handle_tfarg(k + 44);
*/  k = k - 2;

    peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Flag_Reg = acc_handle_tfarg(k + 45);

    peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Info_Reg = acc_handle_tfarg(k + 46);
    peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Flag_Reg = acc_handle_tfarg(k + 47);

    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Address_Reg = acc_handle_tfarg(k + 48);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Ext_Address_Reg = acc_handle_tfarg(k + 49);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_XAMSBS_Reg = acc_handle_tfarg(k + 50);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_DW_Size_Reg = acc_handle_tfarg(k + 51);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Type_Reg = acc_handle_tfarg(k + 52);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Is_GSM_Reg = acc_handle_tfarg(k + 53);

    k = k + 1;
/*
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_MS_Word_Data_Regs = acc_handle_tfarg(k + 53);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_LS_Word_Data_Regs = acc_handle_tfarg(k + 54);
*/  k = k - 2;

    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_BE_Reg = acc_handle_tfarg(k + 55);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Flag_Reg = acc_handle_tfarg(k + 56);

    peInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TFrame_Reg = acc_handle_tfarg(k + 57);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TD_Reg = acc_handle_tfarg(k + 58);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TDL_Reg = acc_handle_tfarg(k + 59);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TCLK_Reg = acc_handle_tfarg(k + 60);
    peInstancesTable[instID]->pli_Reg_Handles.pli_Pins_Flag_Reg = acc_handle_tfarg(k + 61);

    peInstancesTable[instID]->pli_Reg_Handles.mailbox_CSR = acc_handle_tfarg(k + 62);
    peInstancesTable[instID]->pli_Reg_Handles.doorbell_CSR = acc_handle_tfarg(k + 63);

    peInstancesTable[instID]->pli_Reg_Handles.mem_IO_Space_XAMSBS = acc_handle_tfarg(k + 64);
    peInstancesTable[instID]->pli_Reg_Handles.mem_IO_Space_Ext_Address = acc_handle_tfarg(k + 65);
    peInstancesTable[instID]->pli_Reg_Handles.mem_IO_Space_Start_Address = acc_handle_tfarg(k + 66);
    peInstancesTable[instID]->pli_Reg_Handles.mem_IO_Space_DW_Size = acc_handle_tfarg(k + 67);
    peInstancesTable[instID]->pli_Reg_Handles.mem_GSM_Space_XAMSBS = acc_handle_tfarg(k + 68);
    peInstancesTable[instID]->pli_Reg_Handles.mem_GSM_Space_Ext_Address = acc_handle_tfarg(k + 69);
    peInstancesTable[instID]->pli_Reg_Handles.mem_GSM_Space_Start_Address = acc_handle_tfarg(k + 70);
    peInstancesTable[instID]->pli_Reg_Handles.mem_GSM_Space_DW_Size = acc_handle_tfarg(k + 71);

    peInstancesTable[instID]->pli_Reg_Handles.dma_MSR = acc_handle_tfarg(k + 72);
    peInstancesTable[instID]->pli_Reg_Handles.dma_SAR = acc_handle_tfarg(k + 73);
    peInstancesTable[instID]->pli_Reg_Handles.dma_DAR = acc_handle_tfarg(k + 74);
    peInstancesTable[instID]->pli_Reg_Handles.dma_BCR = acc_handle_tfarg(k + 75);
    peInstancesTable[instID]->pli_Reg_Handles.dma_Src_Inst_ID = acc_handle_tfarg(k + 76);

    /* Reservate memory for the PE memory directory */

    peInstancesTable[instID]->coh_Domain_Size = parameters.coh_Domain_Size;
    peInstancesTable[instID]->coh_Granule_DW_Size =
        (parameters.coh_Granule_Size_32) ? RIO_SMALL_GRAN_DW_SIZE : RIO_LARGE_GRAN_DW_SIZE;
    tmp_DW = getIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.mem_GSM_Space_DW_Size);
    peInstancesTable[instID]->memory_Directory_Size = tmp_DW / peInstancesTable[instID]->coh_Granule_DW_Size;

    peInstancesTable[instID]->memory_Directory = (unsigned long *) malloc(peInstancesTable[instID]->memory_Directory_Size *
        RIO_BYTES_IN_DW);

    /* register this PE instance in RIO model and obtain handle */
    if (RIO_Parallel_Model_Create_Instance(&(peInstancesTable[instID]->inst_Handle), &parameters) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: Register_Instance: RIO_Model_Create_Instance: not OK");

    /* create callback tray */
    callbackTray = (RIO_MODEL_CALLBACK_TRAY_T*) malloc(sizeof(RIO_MODEL_CALLBACK_TRAY_T));

    /* a) now specify the contexts for callback tray */

    callbackTray->snoop_Context = (RIO_CONTEXT_T) instID;
    callbackTray->mailbox_Context = (RIO_CONTEXT_T) instID;
    callbackTray->doorbell_Context = (RIO_CONTEXT_T) instID;
    callbackTray->memory_Context = (RIO_CONTEXT_T) instID;
    callbackTray->directory_Context = (RIO_CONTEXT_T) instID;
    callbackTray->local_Device_Context = (RIO_CONTEXT_T) instID;
    callbackTray->address_Translation_Context = (RIO_CONTEXT_T) instID;
    callbackTray->extended_Features_Context = (RIO_CONTEXT_T) instID;
    callbackTray->lpep_Interface_Context = (RIO_CONTEXT_T) instID;
    callbackTray->rio_Msg = (RIO_CONTEXT_T) instID;
    
    /* b) now specify the callback function pointers for callback tray */

    callbackTray->rio_Local_Response = &RIO_Lg_Local_Response;
    callbackTray->rio_GSM_Request_Done = &RIO_Lg_GSM_Request_Done;
    callbackTray->rio_IO_Request_Done = &RIO_Lg_IO_Request_Done;
    callbackTray->rio_MP_Request_Done = &RIO_Lg_MP_Request_Done;
    callbackTray->rio_Doorbell_Request_Done = &RIO_Lg_Doorbell_Request_Done;
    callbackTray->rio_Config_Request_Done = &RIO_Lg_Config_Request_Done;
    callbackTray->rio_Snoop_Request = &RIO_Lg_Snoop_Request;
    callbackTray->rio_MP_Remote_Request = &RIO_Lg_MP_Remote_Request;
    callbackTray->rio_Doorbell_Remote_Request = &RIO_Lg_Doorbell_Remote_Request;
    callbackTray->rio_Memory_Request = &RIO_Lg_Memory_Request;
    callbackTray->rio_Read_Dir = &RIO_Lg_Read_Directory;
    callbackTray->rio_Write_Dir = &RIO_Lg_Write_Directory;
    callbackTray->rio_PL_Set_Pins = &RIO_Set_Pins;
    callbackTray->rio_Tr_Local_IO = &RIO_Lg_Translate_Local_IO_Req;
    callbackTray->rio_Tr_Remote_IO = &RIO_Lg_Translate_Remote_IO_Req;
    callbackTray->rio_Route_GSM = &RIO_Lg_Route_GSM_Req;
    callbackTray->rio_Remote_Conf_Read = &RIO_Lg_Remote_Config_Read;
    callbackTray->rio_Remote_Conf_Write = &RIO_Lg_Remote_Config_Write;

/*Error, Warning and Informatiom messafges handler*/
    callbackTray->rio_User_Msg = &RIO_User_Msg;
    callbackTray->rio_Msg = (RIO_CONTEXT_T) instID;

/*New callbacks are added below*/
    callbackTray->rio_Granule_Received = SW_Rio_Granule_Received;
    callbackTray->rio_Request_Received = SW_Rio_Request_Received;
    callbackTray->rio_Response_Received = SW_Rio_Response_Received;
    callbackTray->rio_Symbol_To_Send = SW_Rio_Symbol_To_Send;
    callbackTray->rio_Packet_To_Send = SW_Rio_Packet_To_Send;
    callbackTray->rio_Hooks_Context = (RIO_CONTEXT_T) instID;


    /* finally, provide RIO model with a pointer to the callback tray */

    if (RIO_Parallel_Model_Bind_Instance(peInstancesTable[instID]->inst_Handle, callbackTray) != RIO_OK) 
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: Register_Instance: RIO_Model_Bind_Instance: not OK");

/*    io_printf("PLI WRAPPER: PE %d registration finished\n", instID);   */
}

/***************************************************************************
 * Function : SW_Switch_Register_Instance
 *
 * Description: Software part of PLI function pair to provide registration
 *                of the switch instance in the RIO model and PLI wrapper
 *
 * Returns: void
 *
 * Notes: Instance ID shall be passed from Verilog as
 *        the first parameter of this function   
 *
 **************************************************************************/
void SW_Switch_Register_Instance()
{
    int instID;
    unsigned long i;
    int k;
    RIO_SWITCH_CALLBACK_TRAY_T * switchCTray;
    RIO_SWITCH_INST_PARAM_T parameters;

    instID = acc_fetch_tfarg(1);
    if (instID > total_Number_Of_Switches)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: Switch instance ID is too large");
    
    switchInstancesTable[instID] = (RIO_SWITCH_INSTANCES_TABLE_T *) malloc(sizeof(RIO_SWITCH_INSTANCES_TABLE_T));

    switchCTray = (RIO_SWITCH_CALLBACK_TRAY_T*) malloc(sizeof(RIO_SWITCH_CALLBACK_TRAY_T));

    k = 0;
    
    switchInstancesTable[instID]->link_Number = (unsigned long) acc_fetch_tfarg(k + 2);
    parameters.link_Number = switchInstancesTable[instID]->link_Number;

/*New parameters*/
    parameters.device_Identity = (RIO_WORD_T)acc_fetch_tfarg( k + 3 );
    parameters.device_Vendor_Identity = (RIO_WORD_T)acc_fetch_tfarg( k + 4);
    parameters.device_Rev = (RIO_UCHAR_T)acc_fetch_tfarg( k + 5);
    parameters.device_Minor_Rev = (RIO_UCHAR_T)acc_fetch_tfarg( k + 6 );
    parameters.device_Major_Rev = (RIO_UCHAR_T)acc_fetch_tfarg( k + 7 );
    parameters.assy_Identity = (RIO_WORD_T)acc_fetch_tfarg( k + 8);   
    parameters.assy_Vendor_Identity = (RIO_WORD_T)acc_fetch_tfarg( k + 9);
    parameters.assy_Rev = (RIO_WORD_T)acc_fetch_tfarg( k + 10);

    parameters.entry_Extended_Features_Ptr = (RIO_WORD_T)acc_fetch_tfarg( k + 11);
	k = k + 4;
/*End of new parameters*/

    /* create switch instance and obtain its handle and its link handles */
    if (RIO_Switch_Parallel_Create_Instance(
        &(switchInstancesTable[instID]->switch_Handle),
        &(switchInstancesTable[instID]->link_Handles),
        &parameters
        ) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: Switch_Register_Instance: RIO_Switch_Create_Instance: not OK");

    switchInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TFrame_Reg = acc_handle_tfarg(k + 8);
    switchInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TCLK_Reg = acc_handle_tfarg(k + 9);
    switchInstancesTable[instID]->pli_Reg_Handles.pli_Pins_Flag_Reg = acc_handle_tfarg(k + 10);

    k = k + 11;
    
    for (i = 0; i <= RIO_SWITCH_NUM_OF_LINKS - 1; i++)
        switchInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TD_Regs[i] = acc_handle_tfarg(k + i);

    k += RIO_SWITCH_NUM_OF_LINKS;

    for (i = 0; i <= RIO_SWITCH_NUM_OF_LINKS - 1; i++)
        switchInstancesTable[instID]->pli_Reg_Handles.pli_Pins_TDL_Regs[i] = acc_handle_tfarg(k + i);
    
    /* Create callback tray */
    switchCTray->rio_Ph_Set_Pins = &RIO_Switch_Set_Pins;
    switchCTray->rio_User_Msg = &RIO_Switch_User_Msg;
    switchCTray->rio_Msg_Context = (RIO_CONTEXT_T)instID;

    switchCTray->lpep_Interface_Context = (RIO_CONTEXT_T *) malloc(switchInstancesTable[instID]->link_Number *
        sizeof(RIO_CONTEXT_T));   

    for(i = 0; i < switchInstancesTable[instID]->link_Number; i++)
        switchCTray->lpep_Interface_Context[i] = (RIO_CONTEXT_T)(RIO_SWITCH_CONTEXT_MULTIPLIER * instID + i); 

    if (RIO_Switch_Parallel_Bind_Instance(switchInstancesTable[instID]->switch_Handle, switchCTray) != RIO_OK) 
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: Switch_Register_Instance: RIO_Switch_Bind_Instance: not OK");
}

/***************************************************************************
 * Function : SW_Initialize_Instance
 *
 * Description: Software part of PLI function pair to provide initialization
 *                of the PE instance in the RIO model with specified parameters
 *
 * Returns: void
 *
 * Notes: Instance (PE) ID shall be passed from Verilog as the first
 *        parameter of this function   
 *
 **************************************************************************/
void SW_Initialize_Instance()
{
    int instID = acc_fetch_tfarg_int(1);      
    unsigned int i;
    int k;
    RIO_TR_INFO_T * domain;
    RIO_PARAM_SET_T parameters;

    k = 0;

    parameters.transp_Type = (RIO_PL_TRANSP_TYPE_T) acc_fetch_tfarg_int(k + 2);

    peInstancesTable[instID]->tr_Type = parameters.transp_Type;

    parameters.dev_ID = (RIO_TR_INFO_T) acc_fetch_tfarg_int(k + 3);
    dev_IDs[instID] = parameters.dev_ID;

    parameters.lcshbar = (RIO_WORD_T) acc_fetch_tfarg_int(k + 4);
    parameters.lcsbar = (RIO_WORD_T) acc_fetch_tfarg_int(k + 5);
/*  !!!!!
    parameters.routing_Meth = (RIO_ROUTING_METHOD_T) acc_fetch_tfarg_int(k + 6);
*/
    peInstancesTable[instID]->routing_Method = RIO_TABLE_BASED_ROUTING;
  
    k = k - 1;

    parameters.ext_Address = acc_fetch_tfarg_int(k + 7);
    parameters.ext_Address_16 = acc_fetch_tfarg_int(k + 8);
    /*parameters.xamsbs = acc_fetch_tfarg_int(k + 9);              */
	k--;

    peInstancesTable[instID]->ext_Address_Enabled = parameters.ext_Address;                        
    peInstancesTable[instID]->ext_Address_16 = parameters.ext_Address_16;                            
    peInstancesTable[instID]->xamsbs_Enabled = 1;                            

    /* parameters.single_Data_Rate = (RIO_BOOL_T) acc_fetch_tfarg_int(k + 10); */

    k = k - 1;
    parameters.lp_Ep_Is_8 = (RIO_BOOL_T) acc_fetch_tfarg_int(k + 11);

/*    parameters.check_SECDED = (RIO_BOOL_T) acc_fetch_tfarg_int(k + 12);      
    parameters.correct_Using_SECDED = (RIO_BOOL_T) acc_fetch_tfarg_int(k + 13);*/
	k -= 2;
    
    parameters.receive_Idle_Lim = (unsigned int) acc_fetch_tfarg_int(k + 14);
    parameters.throttle_Idle_Lim = (unsigned int) acc_fetch_tfarg_int(k + 15);

/*    parameters.physical_Time_Out = (unsigned int) acc_fetch_tfarg_int(k + 16);
    parameters.physical_Retry_Count = (unsigned int) acc_fetch_tfarg_int(k + 17);
    parameters.logical_Time_Out = (unsigned int) acc_fetch_tfarg_int(k + 18);
    parameters.logical_Retry_Count = (unsigned int) acc_fetch_tfarg_int(k + 19);*/
    k -= 4;

    parameters.res_For_3_Out = (RIO_UCHAR_T) acc_fetch_tfarg_int(k + 20);
    parameters.res_For_2_Out = (RIO_UCHAR_T) acc_fetch_tfarg_int(k + 21);
    parameters.res_For_1_Out = (RIO_UCHAR_T) acc_fetch_tfarg_int(k + 22);
    parameters.res_For_3_In = (RIO_UCHAR_T) acc_fetch_tfarg_int(k + 23);
    parameters.res_For_2_In = (RIO_UCHAR_T) acc_fetch_tfarg_int(k + 24);
    parameters.res_For_1_In = (RIO_UCHAR_T) acc_fetch_tfarg_int(k + 25);

    parameters.pass_By_Prio = (RIO_BOOL_T) acc_fetch_tfarg_int(k + 26);

    parameters.config_Space_Size = (RIO_WORD_T) acc_fetch_tfarg_int(k + 27); 

	/*defines the current working mode : RIO_TRUE if operating mode is 8 - bit*/
	parameters.work_Mode_Is_8 = (RIO_BOOL_T) acc_fetch_tfarg_int(k + 28);

	/*defines if the model needs training sequence*/
    parameters.requires_Window_Alignment = (RIO_BOOL_T) acc_fetch_tfarg_int(k + 29);

    /*
     * this parameter define limit number of sent 
     * Training patterns (actually symbols pair)
	 * Should be 1 or more.
     * This parameter is used only if requires otherwise it's skipped
     */
 
    parameters.num_Trainings = (unsigned int)acc_fetch_tfarg_int(k + 30);
/*New 5 parameters*/
  	parameters.is_Host = (RIO_BOOL_T)acc_fetch_tfarg_int(k + 31);
	parameters.is_Master_Enable = (RIO_BOOL_T)acc_fetch_tfarg_int(k + 32);
	parameters.is_Discovered = (RIO_BOOL_T)acc_fetch_tfarg_int(k + 33);
    parameters.input_Is_Enable = (RIO_BOOL_T)acc_fetch_tfarg_int(k + 34);
    parameters.output_Is_Enable = (RIO_BOOL_T)acc_fetch_tfarg_int(k + 35);

/*trainings parameters*/
    if (parameters.lp_Ep_Is_8 == RIO_TRUE || parameters.work_Mode_Is_8 == RIO_TRUE)
    {
        parameters.custom_Training_Pattern[0] = 0xFFFFFFFF;
        parameters.custom_Training_Pattern[1] = 0;
    }
    else
    {
        parameters.custom_Training_Pattern[0] = 0xFFFFFFFF;
        parameters.custom_Training_Pattern[1] = 0xFFFFFFFF;
        parameters.custom_Training_Pattern[2] = 0;
        parameters.custom_Training_Pattern[3] = 0;
    }
    parameters.transmit_Flow_Control_Support = RIO_FALSE;
    parameters.ext_Mailbox_Support = RIO_TRUE;
    parameters.enable_LRQ_Resending = RIO_FALSE;



    k += 36;
    
    if (peInstancesTable[instID]->coh_Domain_Size > 1)
    {
        domain = (RIO_TR_INFO_T*) malloc((peInstancesTable[instID]->coh_Domain_Size - 1) * sizeof(RIO_TR_INFO_T));
        for (i = 0; i < peInstancesTable[instID]->coh_Domain_Size - 1; i++)
        {
            *(domain + i) = (RIO_TR_INFO_T) acc_fetch_tfarg_int(k + i);
        }
        parameters.remote_Dev_ID = domain;      
        k += peInstancesTable[instID]->coh_Domain_Size - 1;
    } else
        parameters.remote_Dev_ID = NULL;    
        
    if ((peInstancesTable[instID]->routing_Method == RIO_INDEX_BASED_ROUTING) ||
        (peInstancesTable[instID]->routing_Method == RIO_SHIFT_BASED_ROUTING))
    {
        for (i = 1; i <= total_Number_Of_PE; i++)
            peInstancesTable[instID]->transport_Info_Table[i] = (RIO_TR_INFO_T) acc_fetch_tfarg_int(k - 1 + i);
        k += total_Number_Of_PE;
    }

    if (funcTray->rio_Init(peInstancesTable[instID]->inst_Handle, &parameters) != RIO_OK)    
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: Initialize_Instance: not OK"); 

    /* Initialize memory directory */
    
    for(i = 0; i < peInstancesTable[instID]->memory_Directory_Size; i++)
        peInstancesTable[instID]->memory_Directory[i] = 0;
}

/***************************************************************************
 * Function : SW_Switch_Initialize_Instance
 *
 * Description: Software part of PLI function pair to provide initialization
 *                of the switch instance in the RIO model with spec. parameters
 *
 * Returns: void
 *
 * Notes: Instance (switch) ID shall be passed from Verilog as the first
 *        parameter of this function   
 *
 **************************************************************************/
void SW_Switch_Initialize_Instance()
{
    int k;
    RIO_SWITCH_PARAM_SET_T p;
    RIO_ROUTE_TABLE_ENTRY_T *t;
    /* RIO_BOOL_T   *rate_List; */
    RIO_BOOL_T   *lp_EP_List; 
    /* RIO_BOOL_T   single_Data_Rate; */
    RIO_BOOL_T   lp_Ep_8_Used;
    int instID;
    int i;

    k = 0;

    instID = acc_fetch_tfarg(k + 1);
    /* single_Data_Rate = acc_fetch_tfarg(2);  */
    lp_Ep_8_Used = acc_fetch_tfarg(k + 2);           
/*    p.rout_Alg = acc_fetch_tfarg(3);!!!!!*/


	k = k - 1;

    p.ext_Address = acc_fetch_tfarg(k + 4);
    p.ext_Address_16 = acc_fetch_tfarg(k + 5);
    
    p.rout_Table_Size = acc_fetch_tfarg(k + 6);

    if (p.rout_Table_Size > 0)
    {
        t = (RIO_ROUTE_TABLE_ENTRY_T *) malloc(p.rout_Table_Size * sizeof(RIO_ROUTE_TABLE_ENTRY_T));    
        for (i = 0; i < p.rout_Table_Size; i++)
        {
            (t + i)->target_Port_Number = acc_fetch_tfarg(k + 7 + i * 2);
            (t + i)->target_ID = acc_fetch_tfarg(k + 8 + i * 2);
        }
    }
    else
        t = NULL;
    p.routing_Table = t;

    /* rate_List = (RIO_BOOL_T *) malloc(switchInstancesTable[instID]->link_Number * sizeof(RIO_BOOL_T));  */
    lp_EP_List = (RIO_BOOL_T *) malloc(switchInstancesTable[instID]->link_Number * sizeof(RIO_BOOL_T));
    for (i = 0; i < switchInstancesTable[instID]->link_Number; i++)
    {
        /* rate_List[i] = single_Data_Rate; */
        lp_EP_List[i] = lp_Ep_8_Used;
    }
    /* p.single_Data_Rate = rate_List; */
    p.lp_Ep_8_Used = lp_EP_List;  

	p.work_Mode_Is_8_Array = Work_Modes;
	p.requires_Window_Alignment_Array = Req_Window_Align;
	p.num_Trainings_Array = Num_Trainings;

    (*(switchFuncTray->rio_Switch_Initialize)) (switchInstancesTable[instID]->switch_Handle, &p);
	free (lp_EP_List);
	if (t != NULL) free (t);
}

/***************************************************************************
 * Function : SW_Start_Reset_Instance
 *
 * Description: Software part of PLI function pair to provide reset
 *                of the PE instance in the RIO model
 *
 * Returns: void
 *
 * Notes: Instance (PE) ID shall be passed from Verilog as the first
 *        parameter of this function   
 *
 **************************************************************************/
void SW_Start_Reset_Instance()
{
    int instID = acc_fetch_tfarg_int(1);      
    if ((*(funcTray->rio_Start_Reset)) (peInstancesTable[instID]->inst_Handle) != RIO_OK)
    {
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: SW_Start_Reset_Instance: not OK");
        return;
    }
    
    /* PLI registers reset */
    
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Trx_Tag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Result_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Err_Code_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_GSM_Req_Done_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Loc_Trx_Tag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Loc_Response_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Loc_Response_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Trx_Tag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Result_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Err_Code_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_IO_Req_Done_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Trx_Tag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Result_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Err_Code_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Req_Done_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Trx_Tag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Result_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Err_Code_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Req_Done_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Trx_Tag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Result_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Err_Code_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Config_Req_Done_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_Address_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_Ext_Address_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_XAMSBS_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_LTType_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_TType_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Snoop_Req_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_MSGLEN_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_SSIZE_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_LETTER_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_MBOX_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_MSGSEG_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_DW_Size_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_MP_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Info_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_DB_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Address_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Ext_Address_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_XAMSBS_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_DW_Size_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Type_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Is_GSM_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_BE_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.pli_Mem_Flag_Reg, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.dma_MSR, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.dma_SAR, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.dma_DAR, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.dma_BCR, 0);
    setIntegerRegValue(peInstancesTable[instID]->pli_Reg_Handles.dma_Src_Inst_ID, 0);
}

/***************************************************************************
 * Function : SW_Switch_Start_Reset_Instance
 *
 * Description: Software part of PLI function pair to provide reset
 *                of the switch instance in the RIO model
 *
 * Returns: void
 *
 * Notes: Instance (switch) ID shall be passed from Verilog as the first
 *        parameter of this function   
 *
 **************************************************************************/
void SW_Switch_Start_Reset_Instance()
{
    int instID; 
    
    instID = acc_fetch_tfarg(1);

    if ((*(switchFuncTray->rio_Switch_Start_Reset)) (switchInstancesTable[instID]->switch_Handle) != RIO_OK)
        RIO_User_Msg((RIO_CONTEXT_T) instID, "Error: SW_Switch_Start_Reset_Instance: not OK");
}

/************************************************************************** 
*************************  Internal functions: ****************************
**************************************************************************/ 

/***************************************************************************
 * Function : setRegValue
 *
 * Description: Sets register regName to specified value 
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
static void setRegValue(handle hRegSet, int index, unsigned long value)
{
    static s_acc_value val = {accVectorVal};    
    static s_setval_delay delay = {{0, 0, 0, 0.0}, accNoDelay}; 

    s_acc_vecval s_Vector[RIO_MAX_DW_SIZE];

    val.value.vector = (p_acc_vecval)(&s_Vector[0]);

    acc_fetch_value(hRegSet, "%%", &val);

    val.value.vector[index].aval = value;    
    val.value.vector[index].bval = 0;    

    if (acc_set_value(hRegSet, &val, &delay))
    {
        /* finish if error */
    }
}

/***************************************************************************
 * Function : getRegValue
 *
 * Description: Gets register regName value 
 *
 * Returns: requested value
 *
 * Notes:   
 *
 **************************************************************************/
static unsigned long getRegValue(handle hRegSet, int index)
{
    static s_acc_value val = {accVectorVal};    

    s_acc_vecval s_Vector[RIO_MAX_DW_SIZE];

    val.value.vector = (p_acc_vecval)(&s_Vector[0]);

    acc_fetch_value(hRegSet, "%%", &val);

    return val.value.vector[index].aval;    
}

/***************************************************************************
 * Function : setIntegerRegValue
 *
 * Description: Sets register regName to specified integer value 
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
static void setIntegerRegValue(handle hReg, int value)
{
    static s_acc_value val = {accIntVal};    /* value  */
    static s_setval_delay delay = {{0, 0, 0, 0.0}, accNoDelay}; /* delay - NO */

    /* set register value */
    val.value.integer = value;
    
if( hReg == (handle)29901960 )
{
   io_printf("PLI::::SetIntegerValue::::: value = %d\n", value);    
   io_printf("PLI::::SetIntegerValue::::: val.value.integer = %d\n", val.value.integer);
   }    
   if (acc_set_value(hReg, &val, &delay))
    {
        /* finish if error */
    }
}

/***************************************************************************
 * Function : getIntegerRegValue
 *
 * Description: Gets integer value of register regName 
 *
 * Returns: Integer value of the requested register
 *
 * Notes:   
 *
 **************************************************************************/
static int getIntegerRegValue(handle hReg)
{
    static s_acc_value val = {accIntVal};       /* value  */

    /* set register value */
    acc_fetch_value(hReg, "%%", &val);

    return val.value.integer;
}

/***************************************************************************
 * Function : accessConfigReg
 *
 * Description: Provides access for the env. configuration registers 
 *
 * Returns: RIO_REQ_DONE  - successful 
 *          RIO_REQ_ERROR - error (probably no register with such offset) 
 *
 * Notes:   
 *
 **************************************************************************/
static RIO_REQ_RESULT_T accessConfigReg(RIO_CONTEXT_T context, RIO_CONF_OFFS_T config_Offset,  
    RIO_CONFIG_OPER_T operation, unsigned long *data)
{
    handle pli_Reg_Handle;
    unsigned long reg_Read_Only_Mask = 0;  /* each bit: 0 - R/W, 1 - read only */
    unsigned long cur_Value;
    unsigned long new_Value;

    /*io_printf("PLI WRAPPER: PE %d: Config Register Access: offset = h%x\n", (int)context, config_Offset); */  

    switch (config_Offset)
    {
        case RIO_MB_CSR_OFFSET: /* Mailbox CSR  */   
            pli_Reg_Handle = peInstancesTable[(int)context]->pli_Reg_Handles.mailbox_CSR;
            break;

        case RIO_DB_CSR_OFFSET: /* Doorbell CSR  */
            pli_Reg_Handle = peInstancesTable[(int)context]->pli_Reg_Handles.doorbell_CSR;
            break;

        case RIO_MB_MSR_OFFSET: /* Mode and Status Register */
            pli_Reg_Handle = peInstancesTable[(int)context]->pli_Reg_Handles.dma_MSR;
            reg_Read_Only_Mask = 0x0F;
            /*io_printf("CFG SPACE: MSR access\n"); */
            break;

        case RIO_MB_SAR_OFFSET: /* Source Address Register  */
            pli_Reg_Handle = peInstancesTable[(int)context]->pli_Reg_Handles.dma_SAR;
            /*io_printf("CFG SPACE: SAR access\n"); */
            break;

        case RIO_MB_DAR_OFFSET: /* Destination Address Register  */
            /*io_printf("CFG SPACE: DAR access\n"); */
            pli_Reg_Handle = peInstancesTable[(int)context]->pli_Reg_Handles.dma_DAR;
            break;

        case RIO_MB_BCR_OFFSET: /* Byte Count Register */
            /*io_printf("CFG SPACE: BCR access\n"); */
            pli_Reg_Handle = peInstancesTable[(int)context]->pli_Reg_Handles.dma_BCR;
            break;

        case RIO_MB_SRC_TR_INFO_OFFSET: /* Source Transport Info  */
            /*io_printf("CFG SPACE: SRC_TR_INFO access\n"); */
            pli_Reg_Handle = peInstancesTable[(int)context]->pli_Reg_Handles.dma_Src_Inst_ID;
            break;

        /* TBD */

        default:
            /* error: such offset is not recognized by the environment */
            return RIO_REQ_ERROR;
    }

    cur_Value = getIntegerRegValue(pli_Reg_Handle);

    if (operation == CONFIG_WRITE)
    {
        new_Value = *data;
        /*io_printf("PLI WRAPPER: Config Register Access: trying to WRITE h%x\n", new_Value); */
        new_Value = reg_Read_Only_Mask ^ (cur_Value & reg_Read_Only_Mask) ^ (new_Value | reg_Read_Only_Mask);
        setIntegerRegValue(pli_Reg_Handle, new_Value);
        /*io_printf("PLI WRAPPER: Config Register Access: WRITE: OK: written value = h%x\n", new_Value); */
    }
    else if (operation == CONFIG_READ)
        *data = cur_Value;            

    /*io_printf("PLI WRAPPER: Config Register Access Done: OK\n"); */
    
    return RIO_REQ_DONE;
}


/***************************************************************************
 * Function : SW_Set_Switch_Mode_Param
 *
 * Description: Gets parameters for switch initialize procedure and puts it 
 *				to the correspond internal arrays. This arrays uses in the 
 *				rio_Switch_Initialize routine.
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void  SW_Set_Switch_Mode_Param()
{
	int n = acc_fetch_tfarg_int(1);

	if (n > RIO_SWITCH_NUM_OF_LINKS - 1)
	{
		RIO_User_Msg(0, "Error: Set_Switch_Mode_Param: Error link's number");
		return;
	}
/*Reads parameters of link with number i*/
	Work_Modes[n] = (RIO_BOOL_T)acc_fetch_tfarg_int(2);	
 	Req_Window_Align[n] = (RIO_BOOL_T)acc_fetch_tfarg_int(3);
	Num_Trainings[n] = (unsigned int)acc_fetch_tfarg_int(4);
	printf ("n:%i Wm:%i WndAl:%i NumTr:%i\n",n,(int)Work_Modes[n],(int)Req_Window_Align[n],Num_Trainings[n]);
}



/*New callbacks for hooking*/

/***************************************************************************
 * Function : SW_Rio_Granule_Received
 *
 * Description: callback is called in case of symbol receiving
 *
 * Returns: RIO_OK
 *
 * Notes:   
 *
 **************************************************************************/
int SW_Rio_Granule_Received(
    RIO_CONTEXT_T           context,
    RIO_GRANULE_STRUCT_T*   granule_Struct      /* struct, containing symbol's fields */
    )
{
	return RIO_OK;
}



/***************************************************************************
 * Function : SW_Rio_Request_Received
 *
 * Description: callback is called in case of request receiving
 *
 * Returns: RIO_OK
 *
 * Notes:   
 *
 **************************************************************************/
int SW_Rio_Request_Received(
    RIO_CONTEXT_T         context,
    RIO_REMOTE_REQUEST_T  *packet_Struct      /* struct, containing packets's fields */
    )
{
	return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Response_Received
 *
 * Description: callback is called in case of request receiving
 *
 * Returns: RIO_OK
 *
 * Notes:   
 *
 **************************************************************************/
int SW_Rio_Response_Received(
    RIO_CONTEXT_T         context,
    RIO_RESPONSE_T        *packet_Struct      /* struct, containing response's fields */
    )
{
	return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Symbol_To_Send
 *
 * Description: callback is called in case of symbol transmitting
 *
 * Returns: RIO_OK
 *
 * Notes:   
 *
 **************************************************************************/
int SW_Rio_Symbol_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          symbol_Buffer,/* array, containing symbol's bit stream (4 bytes) */
    RIO_UCHAR_T           buffer_Size /* Size always = 4 */     
    )
{
	return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Packet_To_Send
 *
 * Description: callback is called in case of symbol transmitting
 *
 * Returns: RIO_OK
 *
 * Notes:   
 *
 **************************************************************************/
int SW_Rio_Packet_To_Send(
    RIO_CONTEXT_T         context,
    RIO_UCHAR_T*          packet_Buffer,      /* array, containing packet's bit stream (max - 276 bytes) */
    unsigned int          buffer_Size,        /*Size always = 276*/  
    RIO_UCHAR_T*          packet_Length_In_Granules /* maximum = 276/4 = 69 granules!!*/
    )
{
	return RIO_OK;
}


/*****************************************************************************/


