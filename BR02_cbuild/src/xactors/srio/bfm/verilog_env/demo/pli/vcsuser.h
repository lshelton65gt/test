/*
 * vcsuser.h - Chronologic Simulation pli-tf routine include file
 $Header: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/pli/vcsuser.h,v 1.2 2007/09/12 14:54:12 knutson Exp $
*/

#include <varargs.h>

#define true    1
#define false   0
#define TRUE    1
#define FALSE   0

typedef struct t_tfexprinfo {
    unsigned short expr_type;
    unsigned short padding;
    struct t_vecval *expr_value_p;
    double real_value;
    char *expr_string;
    int expr_ngroups;
    int expr_vec_size;
    int expr_sign;
    int expr_lhs_select; /* not used */
    int expr_rhs_select; /* not used */
} s_tfexprinfo, *p_tfexprinfo;

#define tf_nullparam 0
#define TF_NULLPARAM 0
#define tf_string 1
#define TF_STRING 1
#define tf_specialparam 2
#define TF_SPECIALPARAM 2
#define tf_readonly 10
#define TF_READONLY 10
#define tf_readwrite 11
#define TF_READWRITE 11
#define tf_rwbitselect 12
#define TF_RWBITSELECT 12
#define tf_rwpartselect 13
#define TF_RWPARTSELECT 13
#define tf_rwmemselect 14
#define TF_RWMEMSELECT 14
#define tf_readonlyreal 15
#define TF_READONLYREAL 15
#define tf_readwritereal 16
#define TF_READWRITEREAL 16


typedef struct t_tfnodeinfo
{
    short node_type;
    short padding;
    union {    
        struct t_vecval *vecval_p;
        struct t_strengthval *strengthval_p;
        char *memoryval_p;
        double *real_val_p;
    } node_value;
    char *node_symbol;
    int node_ngroups;
    int node_vec_size;
    int node_sign;
    int node_ms_index; /* not used */
    int node_ls_index; /* not used */
    int node_mem_size; /* not used */
    int node_lhs_element; /* not used */
    int node_rhs_element; /* not used */
    int *node_handle; /* not used */
} s_tfnodeinfo, *p_tfnodeinfo;

#define tf_null_node 100
#define TF_NULL_NODE 100
#define tf_reg_node 101
#define TF_REG_NODE 101
#define tf_integer_node 102
#define TF_INTEGER_NODE 102
#define tf_time_node 103
#define TF_TIME_NODE 103
#define tf_netvector_node 104
#define TF_NETVECTOR_NODE 104
#define tf_netscalar_node 105
#define TF_NETSCALAR_NODE 105
#define tf_memory_node 106
#define TF_MEMORY_NODE 106
#define tf_real_node 107
#define TF_REAL_NODE 107


typedef struct t_vecval {
    int avalbits; /* '0':0  '1':1  'z':0  'x':1 */
    int bvalbits; /*     0      0      1      1 */
} s_vecval, *p_vecval;

typedef struct t_strengthval
{
    int strength0;
    int strength1;
} s_strengthval, *p_strengthval;


/*** misctf callback reasons ***/
#define reason_checktf      1
#define REASON_CHECKTF      1
#define reason_sizetf       2
#define REASON_SIZETF       2
#define reason_calltf       3
#define REASON_CALLTF       3
#define reason_save         4
#define REASON_SAVE         4
#define reason_restart      5
#define REASON_RESTART      5
#define reason_disable      6
#define REASON_DISABLE      6
#define reason_paramvc      7
#define REASON_PARAMVC      7
#define reason_synch        8
#define REASON_SYNCH        8
#define reason_finish       9
#define REASON_FINISH       9
#define reason_reactivate  10
#define REASON_REACTIVATE  10
#define reason_rosynch     11
#define REASON_ROSYNCH     11
#define reason_paramdrc    15	/* Not implemented */
#define REASON_PARAMDRC    15
#define reason_endofcompile 16
#define REASON_ENDOFCOMPILE 16
#define reason_scope       17	/* Not implemented */
#define REASON_SCOPE       17
#define reason_interactive 18
#define REASON_INTERACTIVE 18
#define reason_reset       19
#define REASON_RESET       19
#define reason_endofreset  20
#define REASON_ENDOFRESET  20
#define reason_force       21	/* Not implemented */
#define REASON_FORCE       21
#define reason_release     22	/* Not implemented */
#define REASON_RELEASE     22
#define reason_startofsave 27
#define REASON_STARTOFSAVE 27
#define reason_startofrestart 28 
#define REASON_STARTOFRESTART 28 

/*** levels for tf_message() ***/
#define ERR_MESSAGE  1
#define ERR_WARNING  2
#define ERR_ERROR    3
#define ERR_INTERNAL 4
#define ERR_SYSTEM   5


/*** this structure is provided to allow compilation of pli applications ***/
/*** containing s_tfcell arrays; these arrays are not used by VCS ***/
typedef struct t_tfcell {
    short type;
    short data;
    int (*checktf)();
    int (*sizetf)();
    int (*calltf)();
    int (*misctf)();
    char *tfname;
    char *fill1;
    char *fill2;
    int fill3;
    struct t_tfcell *fill4;
    struct t_tfcell *fill5;
    char *fill6;
    int fill7;
} s_tfcell;

#define usertask         1
#define userfunction     2
#define userrealfunction 3


#ifdef VCSXLIF
#include "vcsuserxlif.h"
#endif

/*** utility routine declarations ***/

#if defined(__WATCOMC__)
extern int		io_printf(va_alist);
extern int		io_mcdprintf(va_alist);
#else
extern int		io_printf();
extern int		io_mcdprintf();
#endif
extern char		*mc_scan_plusargs();
extern int		tf_asynchoff();
extern int		tf_iasynchoff();
extern int		tf_asynchon();
extern int		tf_iasynchon();
extern int		tf_clearalldelays();
extern int		tf_iclearalldelays();
extern int		tf_copypvc_flag();
extern int		tf_icopypvc_flag();
extern int		tf_dostop();
extern int		tf_dofinish();
#if defined(__WATCOMC__)
extern int		tf_error(va_alist);
#else
extern int		tf_error();
#endif
extern p_tfexprinfo	tf_exprinfo();
extern p_tfexprinfo	tf_iexprinfo();
extern char		*tf_getcstringp();
extern char		*tf_igetcstringp();
extern char		*tf_getinstance();
extern int		tf_getlongtime(), tf_igetlongtime();
extern int		tf_getlongp();
extern int		tf_igetlongp();
extern int		tf_getnextlongtime();
extern int		tf_getp();
extern int		tf_igetp();
extern int		tf_getpchange();
extern int		tf_igetpchange();
extern char		*tf_getroutine();
extern char		*tf_igetroutine();
extern char		*tf_gettflist();
extern char		*tf_igettflist();
extern int		tf_gettime(), tf_igettime();
extern int		tf_gettimeprecision();
extern int		tf_igettimeprecision();
extern int		tf_gettimeunit();
extern int		tf_igettimeunit();
extern char		*tf_getworkarea();
extern char		*tf_igetworkarea();
extern char		*tf_longtime_tostr();
#if defined(__WATCOMC__)
extern int		tf_message(va_alist);
#else
extern int		tf_message();
#endif
extern char		*tf_mipname();
extern char		*tf_imipname();
extern int		tf_movepvc_flag();
extern int		tf_imovepvc_flag();
extern p_tfnodeinfo	tf_nodeinfo();
extern p_tfnodeinfo	tf_inodeinfo();
extern int		tf_propagatep();
extern int		tf_ipropagatep();
extern int		tf_nump();
extern int		tf_inump();
extern int		tf_putlongp();
extern int		tf_iputlongp();
extern int		tf_putp();
extern int		tf_iputp();
extern int		tf_rosynchronize();
extern int		tf_irosynchronize();
extern void		tf_scale_longdelay();
extern int		tf_setdelay();
extern int		tf_isetdelay();
extern void		tf_setworkarea();
extern void		tf_isetworkarea();
extern void		tf_setroutine();
extern void		tf_isetroutine();
extern void		tf_settflist();
extern void		tf_isettflist();
extern int		tf_sizep();
extern int		tf_isizep();
extern char		*tf_spname();
extern char		*tf_ispname();
extern int		tf_strdelputp();
extern int		tf_istrdelputp();
extern char		*tf_strgetp();
extern char		*tf_istrgetp();
extern char		*tf_strgettime();
extern int		tf_setlongdelay();
extern int		tf_isetlongdelay();
extern int		tf_strlongdelputp();
extern int		tf_istrlongdelputp();
extern int		tf_synchronize();
extern int		tf_isynchronize();
extern int		tf_testpvc_flag();
extern int		tf_itestpvc_flag();
#if defined(__WATCOMC__)
extern inttf_text(va_alist);
#else
extern int		tf_text();
#endif
extern int		tf_typep();
extern int		tf_itypep();
extern void		tf_unscale_longdelay();
#if defined(__WATCOMC__)
extern int		tf_warning(va_alist), tf_fatal(va_alist);
#else
extern int		tf_warning(), tf_fatal();
#endif
extern int		tf_setrealdelay(), tf_isetrealdelay() ;
extern void		tf_unscale_realdelay(), tf_scale_realdelay() ;
extern void		tf_long_to_real(), tf_real_to_long() ;
extern double		tf_getrealp(), tf_igetrealp() ;
extern int		tf_iputrealp(), tf_putrealp() ;
extern int		tf_strrealdelputp(), tf_istrrealdelputp() ;
extern double		tf_igetrealtime(),tf_getrealtime() ;
extern int		tf_add_long();
extern int		tf_subtract_long();
extern int		tf_compare_long();
extern int		tf_multiply_long();
extern int		tf_divide_long();
extern int		tf_evaluatep(), tf_ievaluatep();

