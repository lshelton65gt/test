/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/cache_control.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  RIO cache control model
*
* Notes:        
*
******************************************************************************/

`timescale 1ns/1ns

module `RIO_CACHE_CONTROL_MODULE;

// This parameter specifies instance number for this instance, which will be used 
// to recognize the instance in PLI interface and RIO model functions 
    parameter inst_ID = 0;

// If this parameter = 1, then coherence granule size in this cache
// (as well as in whole PE) is 64 bytes. If 0, then it's 32 bytes
    parameter coh_Granule_Is_64 = 1;

// PLI registers

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_GSM_Trx_Tag_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_GSM_Result_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_GSM_Err_Code_Reg;
    reg pli_GSM_Req_Done_Flag_Reg;

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_Loc_Trx_Tag_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Loc_Response_Reg;
    reg pli_Loc_Response_Flag_Reg;

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1]  pli_Snoop_Address_Reg;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1]  pli_Snoop_Ext_Address_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Snoop_XAMSBS_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Snoop_LTType_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Snoop_TType_Reg;
    reg pli_Snoop_Req_Flag_Reg;

// CACHE implementation

// cache line index: set_number * `RIO_CACHE_NUM_OF_LINES_IN_SET + line_number 
    reg [0 : `RIO_CACHE_LINE_LENGTH - 1] cache_Lines[0 : `RIO_CACHE_NUM_OF_LINES_IN_SET * `RIO_CACHE_NUM_OF_SETS - 1]; 
    reg [0 : `RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 - 1] cache_Tags[0 : `RIO_CACHE_NUM_OF_LINES_IN_SET *
        `RIO_CACHE_NUM_OF_SETS - 1]; 
    reg cache_M_Bits[0 : `RIO_CACHE_NUM_OF_LINES_IN_SET * `RIO_CACHE_NUM_OF_SETS - 1]; 
    reg cache_S_Bits[0 : `RIO_CACHE_NUM_OF_LINES_IN_SET * `RIO_CACHE_NUM_OF_SETS - 1]; 
    reg cache_GSM_Bits[0 : `RIO_CACHE_NUM_OF_LINES_IN_SET * `RIO_CACHE_NUM_OF_SETS - 1]; 

// Cache Requests Buffer Implementation

    reg [0 : `RIO_TRX_TAG_SIZE - 1] cache_Req_Buffer_Tags[0 : `RIO_CACHE_REQ_BUFFER_SIZE - 1];
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] cache_Req_Buffer_TType[0 : `RIO_CACHE_REQ_BUFFER_SIZE - 1];
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] cache_Req_Buffer_Address[0 : `RIO_CACHE_REQ_BUFFER_SIZE - 1];
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] cache_Req_Buffer_Ext_Address[0 : `RIO_CACHE_REQ_BUFFER_SIZE - 1];
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] cache_Req_Buffer_XAMSBS[0 : `RIO_CACHE_REQ_BUFFER_SIZE - 1];
    reg [0 : `RIO_CACHE_REQ_BUFFER_SIZE - 1] cache_Req_Buffer_Valid;

// Current cache-initiated transaction tag value
    reg [0 : `RIO_BITS_IN_WORD] cur_CC_GSM_Trx_Tag;

// "Round Robin" variable for replacement algorithm
    integer replacement_Set_Number;

// For temporary storage of trx_Tag values
    integer tmp_Trx_Tag_1;
    integer tmp_Trx_Tag_2;
    integer tmp_Trx_Tag_3;
    integer tmp_Trx_Tag_4;

// Cache configuration bits
    reg xamsbs_Enabled;
    reg ext_Address_Enabled; 
    reg ext_Address_16;         // Actual only if ext_Address_Enabled is `RIO_TRUE
                                // 1 : ext_Address size is 16; 0 : ext_Address size is 32 

// Loop counters
//    integer index, i, k;

// Snooper variables
    reg snoop_Hit;
    reg snoop_Push;
    reg [0 : `RIO_CACHE_LINE_LENGTH - 1] snoop_Line;
    integer snoop_Index; 
    integer snoop_I;

// Events waiting block variables
    integer req_I;
    reg req_I_Fixed;
    integer req_Index;

// Register to store cache line index in case when received GSM data was copied to the cache
// Value is actual when pli_GSM_Req_Done_Flag_Reg is set 
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] new_GSM_Cache_Line_Index_Reg;
    
/////////////////////////////////////////////////////////////////

// Current trx tag initial setting
initial
begin
    cur_CC_GSM_Trx_Tag = `RIO_CC_GSM_TRX_BASE_TAG + inst_ID * `RIO_TRX_TAG_PE_OFFSET;   
end

// Cache initialization
initial
begin
    Reset_Cache;
end

//
// THE SNOOPER
//
always @(posedge pli_Snoop_Req_Flag_Reg)
begin
//    wait (pli_Snoop_Req_Flag_Reg === 1'b1);    // Snoop request arrived from RIO model
//    pli_Snoop_Req_Flag_Reg = 1'b0;
    snoop_Hit = `RIO_FALSE;
    snoop_Index = 0; 
    snoop_Push = `RIO_FALSE;

    //$display("CACHE: PE %d: (time %7t) : Snoop_Request SERVICING, lttype: %3d", inst_ID, $time, pli_Snoop_LTType_Reg); 
    
    case (pli_Snoop_LTType_Reg)

        `RIO_LOCAL_READ:        
            begin
                $display("CACHE CONTROL (time %t) : PE %2d: Snoop Request: Read (shared): address h%h",
                    $time, inst_ID, pli_Snoop_Address_Reg);

                Cache_Search(pli_Snoop_Address_Reg, pli_Snoop_Ext_Address_Reg,
                    pli_Snoop_XAMSBS_Reg, `RIO_TRUE /*is_GSM*/, snoop_Hit, snoop_Index);
                if (snoop_Hit)
                begin
                    if (!cache_GSM_Bits[snoop_Index])
                        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : READ (GSM) Snoop_Request with non-GSM address",
                            inst_ID, $time); 
                    if (cache_M_Bits[snoop_Index] && !cache_S_Bits[snoop_Index]) // line is "Modified"
                    begin
                        Get_Cache_Line(snoop_Index, `RIO_SNOOP_DATA);
                        snoop_Push = `RIO_TRUE;
                        // mark line as "Shared"
                        cache_M_Bits[snoop_Index] = 1'b0;                        
                        cache_S_Bits[snoop_Index] = 1'b1;                        
                    end
                end    
                pli_Snoop_Req_Flag_Reg = 1'b0;
                $HW_Lg_Snoop_Response(inst_ID, (snoop_Hit) ? `RIO_SNOOP_HIT : `RIO_SNOOP_MISS, snoop_Push);
            end
        
        `RIO_LOCAL_READ_TO_OWN: 
            begin
                $display("CACHE CONTROL (time %t) : PE %2d: Snoop Request: Read to own: address h%h",
                    $time, inst_ID, pli_Snoop_Address_Reg);

                Cache_Search(pli_Snoop_Address_Reg, pli_Snoop_Ext_Address_Reg,
                    pli_Snoop_XAMSBS_Reg, `RIO_TRUE /*is_GSM*/, snoop_Hit, snoop_Index);
                if (snoop_Hit)
                begin
                    if (!cache_GSM_Bits[snoop_Index])
                        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : READ_TO_OWN (GSM) Snoop_Request with non-GSM address",
                            inst_ID, $time); 
                    Get_Cache_Line(snoop_Index, `RIO_SNOOP_DATA);
                    snoop_Push = `RIO_TRUE;
                    // mark line as "Invalid"
                    cache_M_Bits[snoop_Index] = 1'b0;                        
                    cache_S_Bits[snoop_Index] = 1'b0;                        
                end
                pli_Snoop_Req_Flag_Reg = 1'b0;
                $HW_Lg_Snoop_Response(inst_ID, (snoop_Hit) ? `RIO_SNOOP_HIT : `RIO_SNOOP_MISS, snoop_Push);
            end
        
        `RIO_LOCAL_READ_LATEST: 
            begin
                $display("CACHE CONTROL (time %t) : PE %2d: Snoop Request: Read latest: address h%h",
                    $time, inst_ID, pli_Snoop_Address_Reg);

                Cache_Search(pli_Snoop_Address_Reg, pli_Snoop_Ext_Address_Reg,
                    pli_Snoop_XAMSBS_Reg, `RIO_TRUE /*is_GSM*/, snoop_Hit, snoop_Index);
                if (snoop_Hit)
                begin
                    if (!cache_GSM_Bits[snoop_Index])
                        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : READ_LATEST (GSM) Snoop_Request with non-GSM address",
                            inst_ID, $time); 
                    if (cache_M_Bits[snoop_Index] && !cache_S_Bits[snoop_Index]) // line is "Modified"
                    begin
                        Get_Cache_Line(snoop_Index, `RIO_SNOOP_DATA);
                        snoop_Push = `RIO_TRUE;
                    end
                end
                pli_Snoop_Req_Flag_Reg = 1'b0;
                $HW_Lg_Snoop_Response(inst_ID, (snoop_Hit) ? `RIO_SNOOP_HIT : `RIO_SNOOP_MISS, snoop_Push);
            end
    
        `RIO_LOCAL_DKILL:       
            begin
                $display("CACHE CONTROL (time %t) : PE %2d: Snoop Request: Data kill: address h%h",
                    $time, inst_ID, pli_Snoop_Address_Reg);

                Cache_Search(pli_Snoop_Address_Reg, pli_Snoop_Ext_Address_Reg,
                    pli_Snoop_XAMSBS_Reg, `RIO_TRUE /*is_GSM*/, snoop_Hit, snoop_Index);
                if (snoop_Hit)
                begin
                    if (!cache_GSM_Bits[snoop_Index])
                        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : DKILL (GSM) Snoop_Request with non-GSM address",
                            inst_ID, $time); 
                    // mark line as "Invalid"
                    cache_M_Bits[snoop_Index] = 1'b0;                        
                    cache_S_Bits[snoop_Index] = 1'b0;                        
                end
                pli_Snoop_Req_Flag_Reg = 1'b0;
                $HW_Lg_Snoop_Response(inst_ID, (snoop_Hit) ? `RIO_SNOOP_HIT : `RIO_SNOOP_MISS, snoop_Push);
            end

        `RIO_LOCAL_NONCOHERENT:
            begin
                $display("CACHE CONTROL(time %t) : PE %2d: Snoop Request: Non-coherent: address h%h",
                    $time, inst_ID, pli_Snoop_Address_Reg);

                Cache_Search(pli_Snoop_Address_Reg, pli_Snoop_Ext_Address_Reg,
                    pli_Snoop_XAMSBS_Reg, `RIO_FALSE /*is_GSM*/, snoop_Hit, snoop_Index);
                if (snoop_Hit)
                begin
                    if (cache_GSM_Bits[snoop_Index])
                        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : NON_COHERENT Snoop_Request with GSM address",
                            inst_ID, $time); 
                    //if (cache_M_Bits[snoop_Index] && !cache_S_Bits[snoop_Index]) // line is "Modified"
                    //begin
                    Get_Cache_Line(snoop_Index, `RIO_SNOOP_DATA);
                    snoop_Push = `RIO_TRUE;
                    //end
                    // mark line as "Invalid"
                    cache_M_Bits[snoop_Index] = 1'b0;                        
                    cache_S_Bits[snoop_Index] = 1'b0;                        
                end
                pli_Snoop_Req_Flag_Reg = 1'b0;
                $HW_Lg_Snoop_Response(inst_ID, (snoop_Hit) ? `RIO_SNOOP_HIT : `RIO_SNOOP_MISS, snoop_Push);
            end
    
        default:
            $display("ERROR: PE %d: CACHE CONTROL (time %7t) : Snoop_Request: Unsupported lttype: %d", inst_ID, $time, 
                pli_Snoop_LTType_Reg);    
    endcase
    //$display("CACHE: PE %d: (time %7t) : Snoop_Request SERVICING FINISHED", inst_ID, $time); 
end

//
// GSM EVENTS WAITING BLOCK
//
always
begin : GSM_EVENTS_WAITER

    wait ((pli_GSM_Req_Done_Flag_Reg === 1'b1) && ((pli_GSM_Trx_Tag_Reg & `RIO_TRX_BASE_TAG_MASK) === `RIO_CC_GSM_TRX_BASE_TAG));

    //$display("CC (time %7t) : Events Waiter: GSM Req Done, searching in buffer...", $time);
    
    // Search in the cache requests buffer
    req_I = 0;
    req_I_Fixed = `RIO_FALSE;
    while ((req_I < `RIO_CACHE_REQ_BUFFER_SIZE) && !req_I_Fixed)
    begin
        if ((cache_Req_Buffer_Valid[req_I] === 1'b1) &&
            ((cache_Req_Buffer_Tags[req_I] & `RIO_TRX_TAG_TAIL_MASK) == (pli_GSM_Trx_Tag_Reg & `RIO_TRX_TAG_TAIL_MASK)))   
        begin
            req_I_Fixed = `RIO_TRUE;
            cache_Req_Buffer_Valid[req_I] = 1'b0;     // mark it as invalid, so free
        end
        else    
            req_I = req_I + 1;
    end

    if (!req_I_Fixed)
    begin
        $display("ERROR: PE %d: CACHE CONTROL (time %7t) : Cache request trx tag not found in cache_Req_Buffer", inst_ID, $time);
        pli_GSM_Req_Done_Flag_Reg = 1'b0;
        disable GSM_EVENTS_WAITER;
    end

    //$display("CC (time %7t) : Events Waiter: GSM Req Done: found in buffer, OK", $time);

    req_Index = 0;

    // Copy data to cache if needed
    if ((cache_Req_Buffer_TType[req_I] == `RIO_GSM_READ_SHARED) ||
        (cache_Req_Buffer_TType[req_I] == `RIO_GSM_READ_TO_OWN))        
    begin
        $display("CACHE CONTROL (time %t) : PE %2d: GSM Request done, copying returned data to cache: address h%h",
            $time, inst_ID, cache_Req_Buffer_Address[req_I]);
    
        //$display("CC (time %7t) : Events Waiter: Have to copy result in cache...", $time);
        Prepare_Cache_Line(
            cache_Req_Buffer_Address[req_I],
            cache_Req_Buffer_Ext_Address[req_I],
            cache_Req_Buffer_XAMSBS[req_I],
            `RIO_TRUE /*is_GSM*/, 
            req_Index
            );
        cache_GSM_Bits[req_Index] = 1'b1;
        if (cache_Req_Buffer_TType[req_I] == `RIO_GSM_READ_SHARED)
        begin
            cache_M_Bits[req_Index] = 1'b0;
            cache_S_Bits[req_Index] = 1'b1;
        end
        else if (cache_Req_Buffer_TType[req_I] == `RIO_GSM_READ_TO_OWN)
        begin
            cache_M_Bits[req_Index] = 1'b1;
            cache_S_Bits[req_Index] = 1'b0;
        end
        Set_Cache_Line(req_Index, `RIO_GSM_DATA); 
    end
    else
        $display("CACHE CONTROL (time %t) : PE %2d: GSM Request done: address h%h",
            $time, inst_ID, cache_Req_Buffer_Address[req_I]);

    pli_GSM_Req_Done_Flag_Reg = 1'b0;

    // Simulate GSM_Request_Done 

    new_GSM_Cache_Line_Index_Reg = req_Index;
    
    //$display("CC (time %7t) : Events Waiter: GSM req done for demo generation...", $time);
    
    pli_GSM_Trx_Tag_Reg = cache_Req_Buffer_Tags[req_I];
    pli_GSM_Req_Done_Flag_Reg = 1'b1;

end  // GSM_EVENTS_WAITER

/***************************************************************************
 * Function : Add_To_Cache_Req_Buffer
 *
 * Description: Attempts to insert the specified request data to the 
 *              cache_Req_Buffer (Trx_Tags and TType)
 *
 * Returns:    `RIO_OK if all ok or `RIO_ERROR if no empty space
 *              in the buffer  
 *
 * Notes: 
 *
 **************************************************************************/
function Add_To_Cache_Req_Buffer;
    input trx_Tag;
    input ttype;
    input [0 : `RIO_ADDRESS_WIDTH - 1] address;
    input [0 : `RIO_ADDRESS_WIDTH - 1] ext_Address;
    input [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] xamsbs;

    integer trx_Tag;
    integer ttype;

    integer i;  // Loop index

begin : ADD_TO_CACHE_REQ_BUFFER
    for (i = 0; i < `RIO_CACHE_REQ_BUFFER_SIZE; i = i + 1)
        if (cache_Req_Buffer_Valid[i] === 1'b0)   // invalid, so free 
        begin
            cache_Req_Buffer_Valid[i] = 1'b1;     // mark it as valid, so busy
            cache_Req_Buffer_Tags[i] = trx_Tag;
            cache_Req_Buffer_TType[i] = ttype;
            cache_Req_Buffer_Address[i] = address;
            cache_Req_Buffer_Ext_Address[i] = ext_Address;
            cache_Req_Buffer_XAMSBS[i] = xamsbs;
            Add_To_Cache_Req_Buffer = `RIO_OK;
            disable ADD_TO_CACHE_REQ_BUFFER;
        end    
    $display("WARNING: PE %d: CACHE CONTROL (time %7t) : cache_Req_Buffer is full, request will be retried", inst_ID, $time);    
    Add_To_Cache_Req_Buffer = `RIO_ERROR;
end
endfunction    

/***************************************************************************
 * Task : GSM_Request
 *
 * Description: GSM request cache handler   
 *
 * Notes: 
 *
 **************************************************************************/
task GSM_Request;
    input ttype;
    input prio;
    input [0 : `RIO_ADDRESS_WIDTH - 1] address;
    input [0 : `RIO_ADDRESS_WIDTH - 1] ext_Address;
    input [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] xamsbs;
    input dw_Size;
    input offset;
    input byte_Num; 
    input ext_Address_Valid; 
    input ext_Address_16;
    input trx_Tag;
    output result;       // `RIO_OK, `RIO_ERROR or `RIO_RETRY
    output data_Ready;   // `RIO_FALSE - no data, GSM request was passed to RIO
                         // `RIO_TRUE - actual data was found in the cache
                         //             (in the line specified by line_Index)
    output line_Index;   // actual only when data_Ready is `RIO_TRUE

    integer ttype;
    integer prio;
    integer dw_Size;
    integer offset;
    integer byte_Num; 
    integer trx_Tag;
    integer line_Index; 
    integer result;  

    reg hit;        // "is cache hit occured"
    integer index;  // index of the found cache line, actual if hit occured

begin
    data_Ready = `RIO_FALSE;
    line_Index = 0;
    result = `RIO_OK;

    case (ttype)

        `RIO_GSM_READ_SHARED,
        `RIO_GSM_IO_READ:    
            begin
                Cache_Search(address, ext_Address, xamsbs, `RIO_TRUE /*is_GSM*/, hit, index);
                if (hit)
                begin
                    if (!cache_GSM_Bits[index])
                        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : GSM READ: Cache hit with non-GSM status",
                            inst_ID, $time); 
                    data_Ready = `RIO_TRUE;
                    line_Index = index;
                end     
                else
                    if (Add_To_Cache_Req_Buffer(trx_Tag, ttype, address, ext_Address, xamsbs) == `RIO_OK)
                    begin
                        //$display("Add to cache_Req_Buffer result: OK");    
                        result = $HW_Lg_GSM_Request(
                            inst_ID, ttype, prio, address, ext_Address,   
                            xamsbs, dw_Size, offset, byte_Num, 
                            `RIO_CC_GSM_TRX_BASE_TAG + (trx_Tag & `RIO_TRX_TAG_TAIL_MASK),
                            `RIO_FALSE /*data_flag*/
                            );
                    end        
                    else
                    begin
                        //$display("cache_Req_Buffer result: RETRY");    
                        result = `RIO_RETRY;
                    end    
            end

        `RIO_GSM_READ_TO_OWN:
            begin
                Cache_Search(address, ext_Address, xamsbs, `RIO_TRUE /*is_GSM*/, hit, index);
                if (hit && ({cache_M_Bits[index], cache_S_Bits[index]} === 2'b10)) // "Modified"
                begin
                    if (!cache_GSM_Bits[index])
                        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : GSM READ_TO_OWN: Cache hit with non-GSM status",
                            inst_ID, $time); 
                    data_Ready = `RIO_TRUE;
                    line_Index = index;
                end
                else
                    if (Add_To_Cache_Req_Buffer(trx_Tag, ttype, address, ext_Address, xamsbs) == `RIO_OK)
                    begin
                        result = $HW_Lg_GSM_Request(
                            inst_ID, ttype, prio, address, ext_Address,   
                            xamsbs, dw_Size, offset, byte_Num,
                            `RIO_CC_GSM_TRX_BASE_TAG + (trx_Tag & `RIO_TRX_TAG_TAIL_MASK),
                            `RIO_FALSE /*data_flag*/
                            );
                        //$display("PE %d: CACHE CONTROL (time %7t) : GSM READ_TO_OWN req result: %d",
                        //    inst_ID, $time, result); 
                    end        
                    else
                        result = `RIO_RETRY;
            end

        `RIO_GSM_CASTOUT,
        `RIO_GSM_DC_I,
        `RIO_GSM_FLUSH:      
            begin
                result = $HW_Lg_GSM_Request(
                    inst_ID, ttype, prio, address, ext_Address,   
                    xamsbs, dw_Size, offset, byte_Num,
                    trx_Tag, `RIO_FALSE /*data_flag*/
                    );
            end
    
        default:
            $display("ERROR: PE %d: CACHE CONTROL (time %7t) : GSM_Request: Unsupported ttype: %d", inst_ID, $time,
                pli_Snoop_LTType_Reg);    
    endcase

end
endtask

/***************************************************************************
 * Function : Compose_Address
 *
 * Description: Calculates full memory address based on xamsbs, ext_Address,
 *              address values and cache configuration bit settings 
 *
 * Returns:    Calculated address   
 *
 * Notes: 
 *
 **************************************************************************/
function [0 : `RIO_CACHE_MAX_ADDRESS_LENGTH - 1] Compose_Address;
    input [0 : `RIO_ADDRESS_WIDTH - 1] address;
    input [0 : `RIO_ADDRESS_WIDTH - 1] ext_Address;
    input [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] xamsbs;

    reg [0 : (`RIO_ADDRESS_WIDTH / 2) - 1] ext_Address_Value_16;    // 16-bit extended address
    reg [0 : `RIO_XAMSBS_WIDTH - 1] xamsbs_Value_2;                 // 2-bit XAMSBS   

begin
    ext_Address_Value_16 = ext_Address[(`RIO_ADDRESS_WIDTH / 2) : `RIO_ADDRESS_WIDTH - 1];
    xamsbs_Value_2 = xamsbs[`RIO_PLI_SHORT_REGISTER_SIZE - `RIO_XAMSBS_WIDTH : `RIO_PLI_SHORT_REGISTER_SIZE - 1];
    
    if (xamsbs_Enabled)
        if (ext_Address_Enabled)                   
            if (ext_Address_16)                    // xamsbs, 16-bit ext_Address and address   
                Compose_Address = {xamsbs_Value_2, ext_Address_Value_16, address};                
            else                                   // xamsbs, 32-bit ext_Address and address
                Compose_Address = {xamsbs_Value_2, ext_Address, address};                
        else                                       // xamsbs and address only
            Compose_Address = {xamsbs_Value_2, address};                
    else                                           
        if (ext_Address_Enabled)                   
            if (ext_Address_16)                    // 16-bit ext_Address and address   
                Compose_Address = {ext_Address_Value_16, address};                
            else                                   // 32-bit ext_Address and address
                Compose_Address = {ext_Address, address};                
        else                                       // address only
            Compose_Address = address;                
end
endfunction

/***************************************************************************
 * Task : Decompose_Address
 *
 * Description: Calculates xamsbs, ext_Address and address values based on
 *              the full address value and cache configuration bit settings 
 *
 * Notes: 
 *
 **************************************************************************/
task Decompose_Address;
    input [0 : `RIO_CACHE_MAX_ADDRESS_LENGTH - 1] full_Address;
    output [0 : `RIO_ADDRESS_WIDTH - 1] address;
    output [0 : `RIO_ADDRESS_WIDTH - 1] ext_Address;
    output [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] xamsbs;

begin
    address = full_Address[`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH : `RIO_CACHE_MAX_ADDRESS_LENGTH - 1];

    if (ext_Address_Enabled)
    begin
        if (ext_Address_16)
        begin
            ext_Address = full_Address[(`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH) - (`RIO_ADDRESS_WIDTH / 2) :
                (`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH) - 1]; 
            if (xamsbs_Enabled)
                xamsbs = full_Address[(`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH - (`RIO_ADDRESS_WIDTH / 2)) -
                    `RIO_XAMSBS_WIDTH : (`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH - (`RIO_ADDRESS_WIDTH / 2)) - 1];
            else
                xamsbs = 0;
        end
        else // 32-bit extended address
        begin
            ext_Address = full_Address[(`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH) - `RIO_ADDRESS_WIDTH :
                (`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH) - 1]; 
            if (xamsbs_Enabled)
                xamsbs = full_Address[(`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH - `RIO_ADDRESS_WIDTH) -
                    `RIO_XAMSBS_WIDTH : (`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH - `RIO_ADDRESS_WIDTH) - 1];
                else
                    xamsbs = 0;
        end
    end
    else
    begin
        ext_Address = 0; 
        if (xamsbs_Enabled)
            xamsbs = full_Address[(`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH) - `RIO_XAMSBS_WIDTH :
                (`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_ADDRESS_WIDTH) - 1];
        else
            xamsbs = 0;
    end
end
endtask

/***************************************************************************
 * Task : Cache_Search
 *
 * Description: Cache search task. 
 *
 * Notes: If cache hit occured, number of the cache line found is passed
 *        through the output argument found_Line_Index
 *
 **************************************************************************/
task Cache_Search;
    input [0 : `RIO_ADDRESS_WIDTH - 1] address;
    input [0 : `RIO_ADDRESS_WIDTH - 1] ext_Address;
    input [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] xamsbs;
    input is_GSM;
    output is_Hit;
    output found_Line_Index;

    integer found_Line_Index;

    reg [0 : `RIO_CACHE_MAX_ADDRESS_LENGTH - 1] full_Address;
    reg [0 : `RIO_CACHE_NUM_OF_BITS_IN_LINE_NUM - 1] line_Number; // Current number of cache line
    integer set_Number;                                           // Current number of cache set
    integer cur_Index;   // Cache line index for current set_Number and line_Number values

begin : CACHE_SEARCH
    // Full address calculation
    full_Address = Compose_Address(address, ext_Address, xamsbs);                

    //$display("CC (time %7t) : Cache_Search: Full address: h%h", $time, full_Address);
    
    line_Number = full_Address[(`RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64) : 
        (`RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 + `RIO_CACHE_NUM_OF_BITS_IN_LINE_NUM - 1)];

    //$display("CC (time %7t) : Cache_Search: Line number: %d", $time, line_Number);
        
    found_Line_Index = 0;
    is_Hit = `RIO_FALSE;            
    set_Number = 0;
    while ((set_Number < `RIO_CACHE_NUM_OF_SETS) && (is_Hit === `RIO_FALSE))
    begin   
        cur_Index = set_Number * `RIO_CACHE_NUM_OF_LINES_IN_SET + line_Number;
//        $display("CC (time %7t) : Cache_Search: Cur Index: %d", $time, cur_Index);
        if ((full_Address[0 : `RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 - 1] === cache_Tags[cur_Index]) &&
            ({cache_M_Bits[cur_Index], cache_S_Bits[cur_Index]} !== 2'b00) &&
            (cache_GSM_Bits[cur_Index] === is_GSM))  
        begin
            $display("CACHE CONTROL (time %t) : PE %2d: Cache_Search: HIT!: line: %d", $time, inst_ID, cur_Index);
            found_Line_Index = cur_Index;
            is_Hit = `RIO_TRUE;            
        end
        set_Number = set_Number + 1;
    end
end
endtask    

/***************************************************************************
 * Task : Prepare_Cache_Line
 *
 * Description: Find an invalid cache line for the specified memory address,
 *              sets the new cache tag value and returns line index.
 *              If all appropriate lines are valid, replaces (invalidates)
 *              one of them accordingly to the cache line replacement
 *              algorithm, sets new cache tag value and returns line index.   
 *
 * Notes: Checks for cache hit and, if hit occured, prepares the found line
 *
 **************************************************************************/
task Prepare_Cache_Line;
    input [0 : `RIO_ADDRESS_WIDTH - 1] address;
    input [0 : `RIO_ADDRESS_WIDTH - 1] ext_Address;
    input [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] xamsbs;
    input is_GSM; 
    output line_Index;

    integer line_Index;
    
    reg [0 : `RIO_CACHE_MAX_ADDRESS_LENGTH - 1] full_Address;
    reg [0 : `RIO_CACHE_NUM_OF_BITS_IN_LINE_NUM - 1] line_Number;   // Current number of cache line
    integer set_Number;                                             // Current number of cache set
    integer cur_Index;    // Cache line index for current set_Number and line_Number values        
    reg check_Hit;        // "is cache hit occured"
    integer tmp_Counter;  // Cache sets counter

begin : PREPARE_CACHE_LINE
    // Full address calculation
    full_Address = Compose_Address(address, ext_Address, xamsbs);                

    //$display("CC (time %7t) : Prepare_Cache_Line: Full address: h%h", $time, full_Address);
    
    line_Number = full_Address[`RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 : 
        `RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 + `RIO_CACHE_NUM_OF_BITS_IN_LINE_NUM - 1];

    //$display("CC (time %7t) : Prepare_Cache_Line: Line number: %d", $time, line_Number);
        
    Cache_Search(address, ext_Address, xamsbs, is_GSM, check_Hit, cur_Index);
    //$display("CC (time %7t) : Prepare_Cache_Line: Search: hit: %d index: %d", $time, check_Hit, cur_Index);
    if (check_Hit)
    begin
        //$display("CC (time %7t) : Prepare_Cache_Line: HIT!: line %d", $time, cur_Index);
        Invalidate_Cache_Line(cur_Index);
        line_Index = cur_Index;
        cache_Tags[cur_Index] = full_Address[0 : `RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 - 1];
        disable PREPARE_CACHE_LINE;
    end

    for (set_Number = 0; set_Number < `RIO_CACHE_NUM_OF_SETS; set_Number = set_Number + 1)
    begin   
        cur_Index = set_Number * `RIO_CACHE_NUM_OF_LINES_IN_SET + line_Number;
        if ({cache_M_Bits[cur_Index], cache_S_Bits[cur_Index]} === 2'b00) // cache line status is "Invalid"
        begin
            //$display("CC (time %7t) : Prepare_Cache_Line: FREE line found: %d", $time, cur_Index);
            line_Index = cur_Index;
            cache_Tags[cur_Index] = full_Address[0 : `RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 - 1];
            disable PREPARE_CACHE_LINE;
        end
    end

    // If we are here, then appropriate lines in all sets are valid, so we shall replace one of them: 

    //$display("CC (time %7t) : Prepare_Cache_Line: NO appropriate free lines", $time);
    
    tmp_Counter = 0;
    replacement_Set_Number = (replacement_Set_Number < (`RIO_CACHE_NUM_OF_SETS - 1)) ? replacement_Set_Number + 1 : 0;
    cur_Index = replacement_Set_Number * `RIO_CACHE_NUM_OF_LINES_IN_SET + line_Number;
    while (({cache_GSM_Bits[line_Index], cache_M_Bits[line_Index], cache_S_Bits[line_Index]} === 3'b010) &&
        (tmp_Counter <= `RIO_CACHE_NUM_OF_SETS))
    begin
        tmp_Counter = tmp_Counter + 1;
        replacement_Set_Number = (replacement_Set_Number < (`RIO_CACHE_NUM_OF_SETS - 1)) ? replacement_Set_Number + 1 : 0;
        cur_Index = replacement_Set_Number * `RIO_CACHE_NUM_OF_LINES_IN_SET + line_Number;
    end   
    if (tmp_Counter > `RIO_CACHE_NUM_OF_SETS) 
        $display("WARNING: PE %d: CACHE CONTROL (time %7t) : Prepare_Cache_Line: all appr. cache lines are non-GSM and Modified",
            inst_ID, $time); 
    Invalidate_Cache_Line(cur_Index);
    line_Index = cur_Index;
    cache_Tags[cur_Index] = full_Address[0 : `RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64 - 1];
end
endtask

/***************************************************************************
 * Task: Invalidate_Cache_Line
 *
 * Description: Invalidates specified cache line if it is valid 
 *
 * Notes: 
 *
 **************************************************************************/
task Invalidate_Cache_Line;
    input line_Index;

    integer line_Index;

    reg [0 : `RIO_CACHE_NUM_OF_BITS_IN_LINE_NUM - 1] line_Number;  // Number of the cache line
    integer set_Number;                                            // Number of the cache set
    // Calculated address fields for the specified cache line:
    reg [0 : `RIO_CACHE_MAX_ADDRESS_LENGTH - 1] full_Address;
    reg [0 : `RIO_ADDRESS_WIDTH - 1] address;
    reg [0 : `RIO_ADDRESS_WIDTH - 1] ext_Address;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] xamsbs;

begin
    if ((line_Index < 0) || (line_Index >= `RIO_CACHE_NUM_OF_LINES_IN_SET * `RIO_CACHE_NUM_OF_SETS)) 
        $display("ERROR: PE %d: CACHE CONTROL (time %7t) : Invalidate_Cache_Line: line index is out of range: %d", inst_ID, $time, 
            line_Index);    
    else if ({cache_M_Bits[line_Index], cache_S_Bits[line_Index]} !== 2'b00) // line is valid
    begin
        // calculate full address for the cache line

        set_Number = line_Index / `RIO_CACHE_NUM_OF_LINES_IN_SET;
        line_Number = line_Index - set_Number * `RIO_CACHE_NUM_OF_LINES_IN_SET;
        full_Address = {cache_Tags[line_Index], line_Number};           
        if (coh_Granule_Is_64)
            full_Address = full_Address << (`RIO_CACHE_MIN_NUM_OF_BITS_IN_BYTE_NUM + 1);
        else
            full_Address = full_Address << `RIO_CACHE_MIN_NUM_OF_BITS_IN_BYTE_NUM;

        // calculate xamsbs, ext_address and address values for the cache line based on the
        // calculated full address and cache configuration bit settings

        Decompose_Address(full_Address, address, ext_Address, xamsbs);

        tmp_Trx_Tag_1 = cur_CC_GSM_Trx_Tag;

        if ({cache_M_Bits[line_Index], cache_S_Bits[line_Index]} === 2'b01) // Line status is "Shared"
        begin
            cache_M_Bits[line_Index] = 1'b0;
            cache_S_Bits[line_Index] = 1'b0;
        end    
        else if ({cache_M_Bits[line_Index], cache_S_Bits[line_Index]} === 2'b10) // Line status is "Modified"
            if (cache_GSM_Bits[line_Index])
            begin
                Get_Cache_Line(line_Index, `RIO_GENERAL_DATA);
                while (  
                    $HW_Lg_GSM_Request(
                        inst_ID /*handle*/,
                        `RIO_GSM_CASTOUT /*ttype*/,
                        1 /*prio*/,
                        address /*address*/,
                        ext_Address /*extended_Address*/,
                        xamsbs /*xamsbs*/,
                        (coh_Granule_Is_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE /*dw_Size*/,
                        0 /* offset */,
                        0 /* byte_Num */,
                        tmp_Trx_Tag_1 /*trx_Tag*/,
                        `RIO_FALSE /*data_flag*/
                        ) == `RIO_RETRY )
                    #(`RIO_WAIT_BEFORE_RETRY);
                cur_CC_GSM_Trx_Tag = cur_CC_GSM_Trx_Tag + 1;
                wait ((pli_GSM_Req_Done_Flag_Reg === 1'b1) && (pli_GSM_Trx_Tag_Reg === tmp_Trx_Tag_1));
                cache_M_Bits[line_Index] = 1'b0;
                cache_S_Bits[line_Index] = 1'b0;
            end
            else
            begin
                $display("WARNING: PE %d: CACHE CONTROL (time %7t) : Invalidated line %d was non-GSM and Modified, loss of data",
                    inst_ID, $time, line_Index); 
                cache_M_Bits[line_Index] = 1'b0;
                cache_S_Bits[line_Index] = 1'b0;
            end
        else // Line status has a reserved value
            $display("ERROR: PE %d: CACHE CONTROL (time %7t) : Invalidate_Cache_Line: illegal line status", inst_ID, $time);    
    end
end
endtask

/***************************************************************************
 * Task: Get_Cache_Line
 *
 * Description: Reads data from the specified cache line to specified PLI
 *              data area
 *
 * Notes: 
 *
 **************************************************************************/
task Get_Cache_Line;
    input line_Index;
    input data_Area;

    integer line_Index;
    integer data_Area;

    reg [0 : `RIO_CACHE_LINE_LENGTH - 1] cache_Line;  // Cache line data obtained from the cache

begin
    cache_Line = cache_Lines[line_Index];
    $HW_Set_PLI_DW(
        inst_ID, data_Area, 0,
        cache_Line[`RIO_BITS_IN_WORD * 0 /**/ : `RIO_BITS_IN_WORD * (0 /**/ + 1) - 1],
        cache_Line[`RIO_BITS_IN_WORD * 1 /**/ : `RIO_BITS_IN_WORD * (1 /**/ + 1) - 1]
        );
    $HW_Set_PLI_DW(
        inst_ID, data_Area, 1,
        cache_Line[`RIO_BITS_IN_WORD * 2 /**/ : `RIO_BITS_IN_WORD * (2 /**/ + 1) - 1], 
        cache_Line[`RIO_BITS_IN_WORD * 3 /**/ : `RIO_BITS_IN_WORD * (3 /**/ + 1) - 1]
        );
    $HW_Set_PLI_DW(
        inst_ID, data_Area, 2,
        cache_Line[`RIO_BITS_IN_WORD * 4 /**/ : `RIO_BITS_IN_WORD * (4 /**/ + 1) - 1], 
        cache_Line[`RIO_BITS_IN_WORD * 5 /**/ : `RIO_BITS_IN_WORD * (5 /**/ + 1) - 1] 
        );
    $HW_Set_PLI_DW(
        inst_ID, data_Area, 3,
        cache_Line[`RIO_BITS_IN_WORD * 6 /**/ : `RIO_BITS_IN_WORD * (6 /**/ + 1) - 1], 
        cache_Line[`RIO_BITS_IN_WORD * 7 /**/ : `RIO_BITS_IN_WORD * (7 /**/ + 1) - 1] 
        );

    if (coh_Granule_Is_64)
    begin
        $HW_Set_PLI_DW(
            inst_ID, data_Area, 4,
            cache_Line[`RIO_BITS_IN_WORD * 8 /**/ : `RIO_BITS_IN_WORD * (8 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 9 /**/ : `RIO_BITS_IN_WORD * (9 /**/ + 1) - 1] 
            );
        $HW_Set_PLI_DW(
            inst_ID, data_Area, 5,
            cache_Line[`RIO_BITS_IN_WORD * 10 /**/ : `RIO_BITS_IN_WORD * (10 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 11 /**/ : `RIO_BITS_IN_WORD * (11 /**/ + 1) - 1] 
            );
        $HW_Set_PLI_DW(
            inst_ID, data_Area, 6,
            cache_Line[`RIO_BITS_IN_WORD * 12 /**/ : `RIO_BITS_IN_WORD * (12 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 13 /**/ : `RIO_BITS_IN_WORD * (13 /**/ + 1) - 1] 
            );
        $HW_Set_PLI_DW(
            inst_ID, data_Area, 7,
            cache_Line[`RIO_BITS_IN_WORD * 14 /**/ : `RIO_BITS_IN_WORD * (14 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 15 /**/ : `RIO_BITS_IN_WORD * (15 /**/ + 1) - 1] 
            );
    end
end
endtask

/***************************************************************************
 * Task: Set_Cache_Line
 *
 * Description: Writes data from the specified PLI data area to
 *              the specified cache line
 *
 * Notes: 
 *
 **************************************************************************/
task Set_Cache_Line;
    input line_Index;
    input data_Area;

    integer line_Index;
    integer data_Area;

    reg [0 : `RIO_CACHE_LINE_LENGTH - 1] cache_Line;  // Cache line data to be stored in the cache

begin
    $HW_Get_PLI_DW(
        inst_ID, data_Area, 0,
        cache_Line[`RIO_BITS_IN_WORD * 0 /**/ : `RIO_BITS_IN_WORD * (0 /**/ + 1) - 1],
        cache_Line[`RIO_BITS_IN_WORD * 1 /**/ : `RIO_BITS_IN_WORD * (1 /**/ + 1) - 1]
        );
    $HW_Get_PLI_DW(
        inst_ID, data_Area, 1,
        cache_Line[`RIO_BITS_IN_WORD * 2 /**/ : `RIO_BITS_IN_WORD * (2 /**/ + 1) - 1], 
        cache_Line[`RIO_BITS_IN_WORD * 3 /**/ : `RIO_BITS_IN_WORD * (3 /**/ + 1) - 1]
        );
    $HW_Get_PLI_DW(
        inst_ID, data_Area, 2,
        cache_Line[`RIO_BITS_IN_WORD * 4 /**/ : `RIO_BITS_IN_WORD * (4 /**/ + 1) - 1], 
        cache_Line[`RIO_BITS_IN_WORD * 5 /**/ : `RIO_BITS_IN_WORD * (5 /**/ + 1) - 1] 
        );
    $HW_Get_PLI_DW(
        inst_ID, data_Area, 3,
        cache_Line[`RIO_BITS_IN_WORD * 6 /**/ : `RIO_BITS_IN_WORD * (6 /**/ + 1) - 1], 
        cache_Line[`RIO_BITS_IN_WORD * 7 /**/ : `RIO_BITS_IN_WORD * (7 /**/ + 1) - 1] 
        );

    if (coh_Granule_Is_64)
    begin
        $HW_Get_PLI_DW(
            inst_ID, data_Area, 4,
            cache_Line[`RIO_BITS_IN_WORD * 8 /**/ : `RIO_BITS_IN_WORD * (8 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 9 /**/ : `RIO_BITS_IN_WORD * (9 /**/ + 1) - 1] 
            );
        $HW_Get_PLI_DW(
            inst_ID, data_Area, 5,
            cache_Line[`RIO_BITS_IN_WORD * 10 /**/ : `RIO_BITS_IN_WORD * (10 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 11 /**/ : `RIO_BITS_IN_WORD * (11 /**/ + 1) - 1] 
            );
        $HW_Get_PLI_DW(
            inst_ID, data_Area, 6,
            cache_Line[`RIO_BITS_IN_WORD * 12 /**/ : `RIO_BITS_IN_WORD * (12 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 13 /**/ : `RIO_BITS_IN_WORD * (13 /**/ + 1) - 1] 
            );
        $HW_Get_PLI_DW(
            inst_ID, data_Area, 7,
            cache_Line[`RIO_BITS_IN_WORD * 14 /**/ : `RIO_BITS_IN_WORD * (14 /**/ + 1) - 1], 
            cache_Line[`RIO_BITS_IN_WORD * 15 /**/ : `RIO_BITS_IN_WORD * (15 /**/ + 1) - 1] 
            );
    end
    else
        cache_Line[`RIO_BITS_IN_WORD * 8 /**/ : `RIO_BITS_IN_WORD * (15 /**/ + 1) - 1] = 0; 
    
    cache_Lines[line_Index] = cache_Line;
end
endtask

/***************************************************************************
 * Task: Invalidate_Cache
 *
 * Description: Invalidates all valid cache lines 
 *
 * Notes: 
 *
 **************************************************************************/
task Invalidate_Cache;
    integer i;

begin
    for (i = 0; i < `RIO_CACHE_NUM_OF_LINES_IN_SET * `RIO_CACHE_NUM_OF_SETS; i = i + 1)
        Invalidate_Cache_Line(i);
end
endtask    

/***************************************************************************
 * Task: Reset_Cache
 *
 * Description: Clears all cache lines and sets cache parameters to 
 *              its initial (default) values
 *
 * Notes: Normally shall be called after each model reset/initialization
 *
 **************************************************************************/
task Reset_Cache;
    integer i;
    
begin
//    $display("CACHE: Start reset");    
    for (i = 0; i < `RIO_CACHE_NUM_OF_LINES_IN_SET * `RIO_CACHE_NUM_OF_SETS; i = i + 1)
    begin
//        cache_Lines[i] = {`RIO_CACHE_LINE_LENGTH'b0}; 
          cache_Lines[i] = 'b0;        
//        cache_Tags[i] = {{`RIO_CACHE_MAX_TAG_LENGTH - coh_Granule_Is_64} 'b0}; 
        cache_Tags[i] = 'b0; 
        cache_M_Bits[i] = 1'b0; 
        cache_S_Bits[i] = 1'b0; 
        cache_GSM_Bits[i] = 1'b0; 
    end

    $HW_Get_Address_Settings(inst_ID, ext_Address_Enabled, ext_Address_16, xamsbs_Enabled); 

    cache_Req_Buffer_Valid = 0;
    for (i = 0; i < `RIO_CACHE_REQ_BUFFER_SIZE; i = i + 1)
    begin
        cache_Req_Buffer_Tags[i] = 0;
        cache_Req_Buffer_TType[i] = 0;
        cache_Req_Buffer_Address[i] = 0;
        cache_Req_Buffer_Ext_Address[i] = 0;
        cache_Req_Buffer_XAMSBS[i] = 0;
    end
//    $display("CACHE: Finish reset");    
end
endtask    

endmodule

/*****************************************************************************/
