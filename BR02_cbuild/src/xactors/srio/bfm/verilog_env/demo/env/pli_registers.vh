/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/pli_registers.vh,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.1 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  Registers set for data exchange between PLI wrapper (written in C)
*               and PE environment (written in Verilog).
*
* Notes:        This file shall be included to PE and DMA PE models 
*
******************************************************************************/

// "mirrors" of registers driven by PLI

    wire [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_GSM_Trx_Tag_Mirror;
    wire [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_GSM_Result_Mirror;
    wire [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_GSM_Err_Code_Mirror;
    wire pli_GSM_Req_Done_Flag_Mirror;

    wire [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_Loc_Trx_Tag_Mirror;
    wire [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Loc_Response_Mirror;
    wire pli_Loc_Response_Flag_Mirror;

// registers driven by PLI

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_IO_Trx_Tag_Reg; 
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_IO_Result_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_IO_Err_Code_Reg;
    reg pli_IO_Req_Done_Flag_Reg;

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_MP_Trx_Tag_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_Result_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_Err_Code_Reg;
    reg pli_MP_Req_Done_Flag_Reg;

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_DB_Trx_Tag_Reg; 
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_DB_Result_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_DB_Err_Code_Reg;
    reg pli_DB_Req_Done_Flag_Reg;

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_Config_Trx_Tag_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Config_Result_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Config_Err_Code_Reg;
    reg pli_Config_Req_Done_Flag_Reg;

    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TFrame_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TCLK_Reg;
    reg pli_Pins_Flag_Reg;

/*****************************************************************************/
