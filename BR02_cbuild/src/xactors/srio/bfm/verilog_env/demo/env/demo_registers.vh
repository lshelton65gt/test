/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/demo_registers.vh,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.1 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  Registers set for data exchange between the top module
*               of demo application and demo scenarios of PE-s
*
* Notes:        This file shall be included to all PE and DMA PE models 
*
******************************************************************************/

// registers driven by PLI

// Flags used for demo scenarios activation/deactivation

    reg demo_Process_Scenario_1;
    reg demo_Process_Scenario_2;
    reg demo_Process_Scenario_3;
    reg demo_Process_Scenario_4;
    reg demo_Process_Scenario_5;

    reg demo_Process_Scenario_1S;
    reg demo_Process_Scenario_2S;
    reg demo_Process_Scenario_3S;
    reg demo_Process_Scenario_4S;
    reg demo_Process_Scenario_5S;
    
// Current transaction tags
    
    reg [0 : `RIO_BITS_IN_WORD] cur_Config_Trx_Tag;
    reg [0 : `RIO_BITS_IN_WORD] cur_IO_Trx_Tag;
    reg [0 : `RIO_BITS_IN_WORD] cur_GSM_Trx_Tag;

    reg [0 : `RIO_BITS_IN_WORD] cur_DB_Trx_Tag;
    reg [0 : `RIO_BITS_IN_WORD] cur_MP_Trx_Tag;

// Flags and tags initialization
initial
begin
    demo_Process_Scenario_1 = 1'b0;
    demo_Process_Scenario_2 = 1'b0;
    demo_Process_Scenario_3 = 1'b0;
    demo_Process_Scenario_4 = 1'b0;
    demo_Process_Scenario_5 = 1'b0;

    demo_Process_Scenario_1S = 1'b0;
    demo_Process_Scenario_2S = 1'b0;
    demo_Process_Scenario_3S = 1'b0;
    demo_Process_Scenario_4S = 1'b0;
    demo_Process_Scenario_5S = 1'b0;
    
    cur_Config_Trx_Tag = `RIO_CONFIG_TRX_BASE_TAG + inst_ID * `RIO_TRX_TAG_PE_OFFSET;
    cur_IO_Trx_Tag = `RIO_IO_TRX_BASE_TAG + inst_ID * `RIO_TRX_TAG_PE_OFFSET;    
    cur_GSM_Trx_Tag = `RIO_GSM_TRX_BASE_TAG + inst_ID * `RIO_TRX_TAG_PE_OFFSET;    

    cur_DB_Trx_Tag = `RIO_DB_TRX_BASE_TAG + inst_ID * `RIO_TRX_TAG_PE_OFFSET;    
    cur_MP_Trx_Tag = `RIO_MP_TRX_BASE_TAG + inst_ID * `RIO_TRX_TAG_PE_OFFSET;   
end   

/*****************************************************************************/
