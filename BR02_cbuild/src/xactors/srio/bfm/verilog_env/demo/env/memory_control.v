/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/memory_control.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  RIO memory control model
*
* Notes:        
*
******************************************************************************/

`timescale 1ns/1ns

module `RIO_MEMORY_CONTROL_MODULE;

// This parameter specifies instance number for this instance, which will be used 
// to recognize the instance in PLI interface and RIO model functions 
    parameter inst_ID = 0;

//    wire [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] inst_ID_Mirror;
//    assign inst_ID_Mirror = inst_ID; 

// PLI registers

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_Mem_Address_Reg;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_Mem_Ext_Address_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Mem_XAMSBS_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Mem_DW_Size_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Mem_Type_Reg;
    reg pli_Mem_Is_GSM_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Mem_BE_Reg;
    reg pli_Mem_Flag_Reg;

// Reservations for DW_Size and BE values of the current proceeded request

    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Cur_Mem_DW_Size_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Cur_Mem_BE_Reg;
    
// Local memory location in global memory space

//     IO memory space range:

    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] mem_IO_Space_XAMSBS;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] mem_IO_Space_Ext_Address;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] mem_IO_Space_Start_Address;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] mem_IO_Space_DW_Size;         

//     GSM memory space range:

    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] mem_GSM_Space_XAMSBS;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] mem_GSM_Space_Ext_Address;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] mem_GSM_Space_Start_Address;
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] mem_GSM_Space_DW_Size;         

// Local IO memory space

    reg [0 : `RIO_LOCAL_MEMORY_CELL_WIDTH - 1] mem_Local_IO_Space[0 : `RIO_LOCAL_MEMORY_SPACE_DW_SIZE * `RIO_WORDS_IN_DW - 1]; 

// Local GSM memory space

    reg [0 : `RIO_LOCAL_MEMORY_CELL_WIDTH - 1] mem_Local_GSM_Space[0 : `RIO_LOCAL_MEMORY_SPACE_DW_SIZE * `RIO_WORDS_IN_DW - 1]; 

// Loop counters

    integer i;
    integer j;
    integer k;
    integer index;
// Granule margin index
    integer next_Granule_Start_Index;
// Specifies is the request GSM or IO type
    integer req_Kind;

// Temporary storage
    reg [0 : `RIO_LOCAL_MEMORY_CELL_WIDTH * 2 - 1] tmp_Reg;
    reg [0 : `RIO_LOCAL_MEMORY_CELL_WIDTH * 2 - 1] tmp_Reg_2;
    reg    tmp_Modified;
    reg [0 : `RIO_BITS_IN_BYTE - 1] tmp_Byte; 

/////////////////////////////////////////////////////////////////

// local memory space range settings:
initial
begin

//     IO memory space range:

    if (inst_ID == 1)         
    begin
        mem_IO_Space_XAMSBS = `RIO_MEM_IO_XAMSBS_1;
        mem_IO_Space_Ext_Address = `RIO_MEM_IO_EXT_ADDRR_1;
        mem_IO_Space_Start_Address = `RIO_MEM_IO_START_ADDRR_1;
        mem_IO_Space_DW_Size = `RIO_MEM_IO_DW_SIZE_1;         
    end
    else if (inst_ID == 2) 
    begin
        mem_IO_Space_XAMSBS = `RIO_MEM_IO_XAMSBS_2;
        mem_IO_Space_Ext_Address = `RIO_MEM_IO_EXT_ADDRR_2;
        mem_IO_Space_Start_Address = `RIO_MEM_IO_START_ADDRR_2;
        mem_IO_Space_DW_Size = `RIO_MEM_IO_DW_SIZE_2;         
    end
    else if (inst_ID == 3) 
    begin
        mem_IO_Space_XAMSBS = `RIO_MEM_IO_XAMSBS_3;
        mem_IO_Space_Ext_Address = `RIO_MEM_IO_EXT_ADDRR_3;
        mem_IO_Space_Start_Address = `RIO_MEM_IO_START_ADDRR_3;
        mem_IO_Space_DW_Size = `RIO_MEM_IO_DW_SIZE_3;         
    end
    else if (inst_ID == 4) 
    begin
        mem_IO_Space_XAMSBS = `RIO_MEM_IO_XAMSBS_4;
        mem_IO_Space_Ext_Address = `RIO_MEM_IO_EXT_ADDRR_4;
        mem_IO_Space_Start_Address = `RIO_MEM_IO_START_ADDRR_4;
        mem_IO_Space_DW_Size = `RIO_MEM_IO_DW_SIZE_4;         
    end
    else if (inst_ID == 5) 
    begin
        mem_IO_Space_XAMSBS = `RIO_MEM_IO_XAMSBS_5;
        mem_IO_Space_Ext_Address = `RIO_MEM_IO_EXT_ADDRR_5;
        mem_IO_Space_Start_Address = `RIO_MEM_IO_START_ADDRR_5;
        mem_IO_Space_DW_Size = `RIO_MEM_IO_DW_SIZE_5;         
    end
    else // Otherwise - NO IO MEMORY
    begin
        mem_IO_Space_XAMSBS = 0;
        mem_IO_Space_Ext_Address = 0;
        mem_IO_Space_Start_Address = 0;
        mem_IO_Space_DW_Size = 0;         
    end

    if (mem_IO_Space_DW_Size > `RIO_LOCAL_MEMORY_SPACE_DW_SIZE)
    begin
        $display("MEM (PE %2d): WARNING: Too large IO space size specified (%d DW-s), will be AUTOMATICALLY RESET to %d DW-s",
            inst_ID, mem_IO_Space_DW_Size, `RIO_LOCAL_MEMORY_SPACE_DW_SIZE);
        mem_IO_Space_DW_Size = `RIO_LOCAL_MEMORY_SPACE_DW_SIZE;
    end

//     GSM memory space range:

    if (inst_ID == 1)         
    begin
        mem_GSM_Space_XAMSBS = `RIO_MEM_GSM_XAMSBS_1;
        mem_GSM_Space_Ext_Address = `RIO_MEM_GSM_EXT_ADDRR_1;
        mem_GSM_Space_Start_Address = `RIO_MEM_GSM_START_ADDRR_1;
        mem_GSM_Space_DW_Size = `RIO_MEM_GSM_DW_SIZE_1;         
    end
    else if (inst_ID == 2) 
    begin
        mem_GSM_Space_XAMSBS = `RIO_MEM_GSM_XAMSBS_2;
        mem_GSM_Space_Ext_Address = `RIO_MEM_GSM_EXT_ADDRR_2;
        mem_GSM_Space_Start_Address = `RIO_MEM_GSM_START_ADDRR_2;
        mem_GSM_Space_DW_Size = `RIO_MEM_GSM_DW_SIZE_2;         
    end
    else if (inst_ID == 3) 
    begin
        mem_GSM_Space_XAMSBS = `RIO_MEM_GSM_XAMSBS_3;
        mem_GSM_Space_Ext_Address = `RIO_MEM_GSM_EXT_ADDRR_3;
        mem_GSM_Space_Start_Address = `RIO_MEM_GSM_START_ADDRR_3;
        mem_GSM_Space_DW_Size = `RIO_MEM_GSM_DW_SIZE_3;         
    end
    else if (inst_ID == 4) 
    begin
        mem_GSM_Space_XAMSBS = `RIO_MEM_GSM_XAMSBS_4;
        mem_GSM_Space_Ext_Address = `RIO_MEM_GSM_EXT_ADDRR_4;
        mem_GSM_Space_Start_Address = `RIO_MEM_GSM_START_ADDRR_4;
        mem_GSM_Space_DW_Size = `RIO_MEM_GSM_DW_SIZE_4;         
    end
    else if (inst_ID == 5) 
    begin
        mem_GSM_Space_XAMSBS = `RIO_MEM_GSM_XAMSBS_5;
        mem_GSM_Space_Ext_Address = `RIO_MEM_GSM_EXT_ADDRR_5;
        mem_GSM_Space_Start_Address = `RIO_MEM_GSM_START_ADDRR_5;
        mem_GSM_Space_DW_Size = `RIO_MEM_GSM_DW_SIZE_5;         
    end
    else // Otherwise - NO GSM MEMORY
    begin
        mem_GSM_Space_XAMSBS = 0;
        mem_GSM_Space_Ext_Address = 0;
        mem_GSM_Space_Start_Address = 0;
        mem_GSM_Space_DW_Size = 0;         
    end

    if (mem_GSM_Space_DW_Size > `RIO_LOCAL_MEMORY_SPACE_DW_SIZE)
    begin
        $display("MEM (PE %2d): WARNING: Too large GSM space size specified (%d DW-s), will be AUTOMATICALLY RESET to %d DW-s",
            inst_ID, mem_GSM_Space_DW_Size, `RIO_LOCAL_MEMORY_SPACE_DW_SIZE);
        mem_GSM_Space_DW_Size = `RIO_LOCAL_MEMORY_SPACE_DW_SIZE;
    end

//    $display("MEM in PE %d: IO_DW_SIZE = %d", inst_ID, mem_IO_Space_DW_Size);

end

// Note: there are no any delays in the current memory access model

always /*@(posedge pli_Mem_Flag_Reg)*/
begin
    wait (pli_Mem_Flag_Reg === 1'b1);    // Memory request arrived from RIO model
    pli_Mem_Flag_Reg = 1'b0;

//    $display("MEM: Mem Request service started: address = h%h DW_size = %d", pli_Mem_Address_Reg, pli_Mem_DW_Size_Reg);

    req_Kind = CheckMemoryRequest(pli_Mem_Address_Reg, pli_Mem_DW_Size_Reg, pli_Mem_Is_GSM_Reg);

    // for non-multi-doubleword requests:
    if (req_Kind == `RIO_IO_OK)
    begin
        //$display("MEM: Mem Request service: it's IO request");
        index = pli_Mem_Address_Reg / `RIO_BYTES_IN_WORD;   // first word index
        tmp_Reg = {mem_Local_IO_Space[index], mem_Local_IO_Space[index + 1]};
    end
    else if (req_Kind == `RIO_GSM_OK)
    begin
        //$display("MEM: Mem Request service: it's GSM request");
        index = (pli_Mem_Address_Reg - mem_GSM_Space_Start_Address) / `RIO_BYTES_IN_WORD;   // first word index
        tmp_Reg = {mem_Local_GSM_Space[index], mem_Local_GSM_Space[index + 1]};
    end
    else
        $display("MEM: ERROR: Mem Request service: address is out of range: h%h", pli_Mem_Address_Reg);

    tmp_Modified = 1'b0;
    
    pli_Cur_Mem_DW_Size_Reg = pli_Mem_DW_Size_Reg;
    pli_Cur_Mem_BE_Reg = pli_Mem_BE_Reg;
                
    case (pli_Mem_Type_Reg)

        `RIO_MEM_READ:
            begin
                if (req_Kind == `RIO_IO_OK)
                    $display("MEMORY (time %t) : PE %2d: Read Request: IO address h%h, dw size %d",
                        $time, inst_ID, pli_Mem_Address_Reg, pli_Cur_Mem_DW_Size_Reg);
                else
                    $display("MEMORY (time %t) : PE %2d: Read Request: GSM address h%h, dw size %d",
                        $time, inst_ID, pli_Mem_Address_Reg, pli_Cur_Mem_DW_Size_Reg);
                        
                if ((pli_Cur_Mem_DW_Size_Reg === 1) && (pli_Cur_Mem_BE_Reg !== 8'b11111111)) 
                begin
                    for (i = 0; i < `RIO_BYTES_IN_DW; i = i + 1)
                        if (pli_Cur_Mem_BE_Reg[i] === 1'b0)   // do not read this byte, so return 0
                            for (j = 0; j < `RIO_BITS_IN_BYTE; j = j + 1)
                                tmp_Reg[`RIO_BITS_IN_BYTE * i + j] = 1'b0;                                
                    $HW_Set_PLI_DW(inst_ID, `RIO_MEM_DATA, i,
                        tmp_Reg[0 : `RIO_LOCAL_MEMORY_CELL_WIDTH - 1],
                        tmp_Reg[`RIO_LOCAL_MEMORY_CELL_WIDTH : 2 * `RIO_LOCAL_MEMORY_CELL_WIDTH - 1]);
                end
                else
                begin
                    //$display("MEM: Mem Request service: BIG READ subbranch");
                    for (i = 0; i < pli_Cur_Mem_DW_Size_Reg; i = i + 1)
                        if (req_Kind == `RIO_IO_OK)
                        begin
                            $HW_Set_PLI_DW(inst_ID, `RIO_MEM_DATA, i,
                                mem_Local_IO_Space[index + 2 * i],
                                mem_Local_IO_Space[index + 2 * i + 1]);
                        end
                        else // GSM_OK
                        begin
                            $HW_Set_PLI_DW(inst_ID, `RIO_MEM_DATA, i,
                                mem_Local_GSM_Space[index + 2 * i],
                                mem_Local_GSM_Space[index + 2 * i + 1]);
                        end
                end

                //$display("MEM: Mem Request service: Set Data: MSW = h%h LSW = h%h",
                //$HW_Get_PLI_MS_Word(inst_ID, `RIO_MEM_DATA, 0), $HW_Get_PLI_LS_Word(inst_ID, `RIO_MEM_DATA, 0));

                $HW_Lg_Set_Memory_Data(inst_ID, pli_Cur_Mem_DW_Size_Reg);
            end
                                            
        `RIO_MEM_WRAP_READ:                               
            begin
                $display("MEMORY (time %t) : PE %2d: Wrap Read Request: GSM address h%h, dw size %d",
                    $time, inst_ID, pli_Mem_Address_Reg, pli_Cur_Mem_DW_Size_Reg);

                next_Granule_Start_Index = (index / (pli_Cur_Mem_DW_Size_Reg * 2) + 1) * (pli_Cur_Mem_DW_Size_Reg * 2);

                // For GSM transactions only, pli_Mem_DW_Size_Reg == coh. granule size
                for (i = 0; i < pli_Cur_Mem_DW_Size_Reg; i = i + 1)
                begin
                    if (req_Kind == `RIO_GSM_OK)
                    begin
                        $HW_Set_PLI_DW(inst_ID, `RIO_MEM_DATA, i,
                            mem_Local_GSM_Space[index + 2 * i],
                            mem_Local_GSM_Space[index + 2 * i + 1]);
                    end
                    
                    if (index + 2 * i + 1 == next_Granule_Start_Index - 1)
                        index = index - pli_Cur_Mem_DW_Size_Reg * 2;
                end
                $HW_Lg_Set_Memory_Data(inst_ID, pli_Cur_Mem_DW_Size_Reg);
            end                

        `RIO_MEM_WRITE:
            begin
                if (req_Kind == `RIO_IO_OK)
                   $display("MEMORY (time %t) : PE %2d: Write Request: IO address h%h, dw size %d",
                        $time, inst_ID, pli_Mem_Address_Reg, pli_Cur_Mem_DW_Size_Reg);
                else
                   $display("MEMORY (time %t) : PE %2d: Write Request: GSM address h%h, dw size %d",
                        $time, inst_ID, pli_Mem_Address_Reg, pli_Cur_Mem_DW_Size_Reg);

                $HW_Lg_Get_Memory_Data(inst_ID, pli_Cur_Mem_DW_Size_Reg);

                //$display("MEM: Mem Request service: Got Data to write: MSW = h%h LSW = h%h",
                //$HW_Get_PLI_MS_Word(inst_ID, `RIO_MEM_DATA, 0), $HW_Get_PLI_LS_Word(inst_ID, `RIO_MEM_DATA, 0));

                if ((pli_Cur_Mem_DW_Size_Reg === 1) && (pli_Cur_Mem_BE_Reg !== 8'b11111111)) 
                begin
                    //$display("MEM: Mem Request service: SUB WRITE subbranch, BE: %b", pli_Mem_BE_Reg);
                    tmp_Reg_2 = $HW_Get_PLI_DW_Value(inst_ID, `RIO_MEM_DATA, 0);
                    for (i = 0; i < `RIO_BYTES_IN_DW; i = i + 1)
                        if (pli_Cur_Mem_BE_Reg[i])   // replace this byte with the new value
                        begin
                            for (j = 0; j < `RIO_BITS_IN_BYTE; j = j + 1)
                                tmp_Reg[`RIO_BITS_IN_BYTE * i + j] = tmp_Reg_2[`RIO_BITS_IN_BYTE * i + j];
                            tmp_Modified = 1'b1;
                        end
                end
                else
                begin
                    //$display("MEM: Mem Request service: BIG WRITE subbranch");
                    for (i = 0; i < pli_Cur_Mem_DW_Size_Reg; i = i + 1)
                        if (req_Kind == `RIO_IO_OK)
                        begin
                            mem_Local_IO_Space[index + 2 * i] = $HW_Get_PLI_MS_Word(inst_ID, `RIO_MEM_DATA, i);
                            mem_Local_IO_Space[index + 2 * i + 1] = $HW_Get_PLI_LS_Word(inst_ID, `RIO_MEM_DATA, i);
                        end
                        else // GSM_OK
                        begin
                            mem_Local_GSM_Space[index + 2 * i] = $HW_Get_PLI_MS_Word(inst_ID, `RIO_MEM_DATA, i);
                            mem_Local_GSM_Space[index + 2 * i + 1] = $HW_Get_PLI_LS_Word(inst_ID, `RIO_MEM_DATA, i);
                        end
                end
            end

        `RIO_MEM_ATOMIC_INC:                              
            begin
                i = `RIO_BITS_IN_BYTE - 1;
                while (i >= 0)
                begin
                    if (pli_Cur_Mem_BE_Reg[i])
                        begin
                            tmp_Reg_2 = tmp_Reg >> (`RIO_LOCAL_MEMORY_CELL_WIDTH * 2 - 1) - (`RIO_BITS_IN_BYTE * (i + 1) - 1);
                            tmp_Reg_2 = tmp_Reg_2 + 1;
                            tmp_Reg_2 = tmp_Reg << (`RIO_LOCAL_MEMORY_CELL_WIDTH * 2 - 1) - (`RIO_BITS_IN_BYTE * (i + 1) - 1);
                            for (j = 0; j < `RIO_BITS_IN_BYTE * (i + 1); j = j + 1)
                                tmp_Reg[j] = tmp_Reg_2[j];                                
                            i = 0;
                            tmp_Modified = 1'b1;
                        end    
                    i = i - 1;
                end 
            end

        `RIO_MEM_ATOMIC_DEC:                              
            begin
                i = `RIO_BITS_IN_BYTE - 1;
                while (i >= 0)
                begin
                    if (pli_Cur_Mem_BE_Reg[i])
                        begin
                            tmp_Reg_2 = tmp_Reg >> (`RIO_LOCAL_MEMORY_CELL_WIDTH * 2 - 1) - (`RIO_BITS_IN_BYTE * (i + 1) - 1);
                            tmp_Reg_2 = tmp_Reg_2 - 1;
                            tmp_Reg_2 = tmp_Reg << (`RIO_LOCAL_MEMORY_CELL_WIDTH * 2 - 1) - (`RIO_BITS_IN_BYTE * (i + 1) - 1);
                            for (j = 0; j < `RIO_BITS_IN_BYTE * (i + 1); j = j + 1)
                                tmp_Reg[j] = tmp_Reg_2[j];                                
                            i = 0;
                            tmp_Modified = 1'b1;
                        end    
                    i = i - 1;
                end 
            end

        `RIO_MEM_ATOMIC_SET:                              
            begin
                for (i = 0; i < `RIO_BYTES_IN_DW; i = i + 1)
                    if (pli_Cur_Mem_BE_Reg[i])   // set this byte to all "1"
                    begin
                        for (j = 0; j < `RIO_BITS_IN_BYTE; j = j + 1)
                            tmp_Reg[`RIO_BITS_IN_BYTE * i + j] = 1'b1;                                
                        tmp_Modified = 1'b1;
                    end
            end

        `RIO_MEM_ATOMIC_CLEAR:                            
            begin
                for (i = 0; i < `RIO_BYTES_IN_DW; i = i + 1)
                    if (pli_Cur_Mem_BE_Reg[i])   // set this byte to all "0"
                    begin
                        for (j = 0; j < `RIO_BITS_IN_BYTE; j = j + 1)
                            tmp_Reg[`RIO_BITS_IN_BYTE * i + j] = 1'b0;                                
                        tmp_Modified = 1'b1;
                    end    
            end
    
        `RIO_MEM_ATOMIC_TSWAP:
            begin
                $HW_Lg_Get_Memory_Data(inst_ID, pli_Cur_Mem_DW_Size_Reg);
                tmp_Reg_2 = $HW_Get_PLI_DW_Value(inst_ID, `RIO_MEM_DATA, 0); 
                i = 0;
                while (i <= `RIO_BITS_IN_BYTE - 1)
                begin
                    j = 0;
                    while (j <= `RIO_BITS_IN_BYTE - 1)
                    begin
                        if ((pli_Cur_Mem_BE_Reg[i]) && tmp_Reg[`RIO_BITS_IN_BYTE * i + j] !== 1'b0)
                        begin
                            tmp_Modified = 1'b1;
                            i = `RIO_BITS_IN_BYTE - 1;
                            j = `RIO_BITS_IN_BYTE - 1;   
                        end
                        j = j + 1;
                    end
                    i = i + 1; 
                end
                
                if (tmp_Modified) // if it was set in prev. "while" loop because of non-zero value found
                begin
                    for (i = 0; i < `RIO_BYTES_IN_DW; i = i + 1)
                        if (pli_Cur_Mem_BE_Reg[i])   // replace this byte with the new value
                            for (j = 0; j < `RIO_BITS_IN_BYTE; j = j + 1)
                                tmp_Reg[`RIO_BITS_IN_BYTE * i + j] = tmp_Reg_2[`RIO_BITS_IN_BYTE * i + j];  
                end
     
                // return the "old" value
                if (req_Kind == `RIO_IO_OK)
                begin
                    $HW_Set_PLI_DW(inst_ID, `RIO_MEM_DATA, i, mem_Local_IO_Space[index + i], mem_Local_IO_Space[index + i + 1]);
                end
                else
                begin
                    $HW_Set_PLI_DW(inst_ID, `RIO_MEM_DATA, i, mem_Local_GSM_Space[index + i], mem_Local_GSM_Space[index + i + 1]);
                end
                $HW_Lg_Set_Memory_Data(inst_ID, pli_Cur_Mem_DW_Size_Reg);
            end

        default:                             
            begin
            // error
                $display("ERROR: PE %d: MEMORY CONTROL (time %7t) : Unknown request type: %d", inst_ID, $time, pli_Mem_Type_Reg);
            end
    endcase

    if (tmp_Modified)
    begin
        if (req_Kind == `RIO_IO_OK)
        begin
            mem_Local_IO_Space[index] = tmp_Reg[0 : `RIO_LOCAL_MEMORY_CELL_WIDTH - 1];
            mem_Local_IO_Space[index + 1] = tmp_Reg[`RIO_LOCAL_MEMORY_CELL_WIDTH : 2 * `RIO_LOCAL_MEMORY_CELL_WIDTH - 1];
        end
           else
        begin
            mem_Local_GSM_Space[index] = tmp_Reg[0 : `RIO_LOCAL_MEMORY_CELL_WIDTH - 1];
            mem_Local_GSM_Space[index + 1] = tmp_Reg[`RIO_LOCAL_MEMORY_CELL_WIDTH : 2 * `RIO_LOCAL_MEMORY_CELL_WIDTH - 1];
        end
    end

//    $display("MEM: Mem Request service: FIN: In GSM Memory: MSW = h%h LSW = h%h",
//             mem_Local_GSM_Space[index], mem_Local_GSM_Space[index + 1]);
//    $display("MEM: Mem Request service: FIN: In IO Memory: MSW = h%h LSW = h%h",
//        mem_Local_IO_Space[index], mem_Local_IO_Space[index + 1]);
//    $display("MEM: Mem Request service: FINISHED");

//    pli_Mem_Flag_Reg    = 1'b0;
end

/***************************************************************************
 * Function : CheckMemoryRequest
 *
 * Description: Finds out in which memory space (IO or GSM) this request is
 *              addressed and checks that all requested data is located in
 *              the memory space (to avoid out of range)
 *
 * Returns:    `RIO_IO_OK, `RIO_GSM_OK or `RIO_MEM_ERROR
 *
 * Notes:
 *
 **************************************************************************/
function integer CheckMemoryRequest;
    input [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] address;
    input [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] dw_Size;
    input is_GSM_Access;
begin
    if ((is_GSM_Access) &&
    (address >= mem_GSM_Space_Start_Address) &&
    ((address + (dw_Size * `RIO_BYTES_IN_DW)) <= (mem_GSM_Space_Start_Address + mem_GSM_Space_DW_Size * `RIO_BYTES_IN_DW)))
        CheckMemoryRequest = `RIO_GSM_OK;
    else if ((!is_GSM_Access) && 
    (address >= 0) &&
    ((address + (dw_Size * `RIO_BYTES_IN_DW)) <= mem_IO_Space_DW_Size * `RIO_BYTES_IN_DW))
        CheckMemoryRequest = `RIO_IO_OK;
    else
        CheckMemoryRequest = `RIO_MEM_ERROR;
end
endfunction    

endmodule

/*****************************************************************************/
