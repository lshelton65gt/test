/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/env_defines.vh,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.1 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  Defines macronames used in demo environment models and demo
*               scenario and allowed to be changed only in case of serious
*               model or demo environment corrections
*
* Notes:        
*
******************************************************************************/

// PLI registers size

`define RIO_PLI_SHORT_REGISTER_SIZE         8
`define RIO_PLI_LONG_REGISTER_SIZE          32
`define RIO_MAX_DATA_SIZE                   64
`define RIO_MAX_DW_SIZE                     32

`define RIO_ADDRESS_WIDTH                   32
`define RIO_XAMSBS_WIDTH                    2

// Data size convertion constants

`define RIO_BITS_IN_BYTE                    8
`define RIO_BITS_IN_WORD                    32
`define RIO_BITS_IN_DW                      64
`define RIO_BYTES_IN_WORD                   4
`define RIO_BYTES_IN_DW                     8
`define RIO_WORDS_IN_DW                     2

// ----------------------------------------------------------------------------------------
// NOTE: the following defines must be in accordance with the appropriate enumeration types
//       declared in rio_types.h file of RIO model   
// ----------------------------------------------------------------------------------------

// Return values for some functions

`define RIO_OK                              0  
`define RIO_ERROR                           1
`define RIO_RETRY                           2  
`define RIO_PARAM_INVALID                   3  

// Bool values

`define RIO_FALSE                           1'b0
`define RIO_TRUE                            1'b1

// GSM transaction types

`define RIO_GSM_READ_SHARED                 0 
`define RIO_GSM_INSTR_READ                  1
`define RIO_GSM_READ_TO_OWN                 2
`define RIO_GSM_CASTOUT                     3
`define RIO_GSM_DC_I                        4
`define RIO_GSM_TLB_IE                      5
`define RIO_GSM_TLB_SYNC                    6
`define RIO_GSM_IC_I                        7
`define RIO_GSM_FLUSH                       8
`define RIO_GSM_IO_READ                     9

// GSM physical layer transaction types 

`define RIO_PL_GSM_READ_OWNER               0 
`define RIO_PL_GSM_READ_TO_OWN_OWNER        1
`define RIO_PL_GSM_IO_READ_OWNER            2
`define RIO_PL_GSM_READ_HOME                3
`define RIO_PL_GSM_READ_TO_OWN_HOME         4
`define RIO_PL_GSM_IO_READ_HOME             5
`define RIO_PL_GSM_DKILL_HOME               6
`define RIO_PL_GSM_IREAD_HOME               7
`define RIO_PL_GSM_DKILL_SHARER             8
`define RIO_PL_GSM_IKILL                    9
`define RIO_PL_GSM_TLBIE                    10
`define RIO_PL_GSM_TLBSYNC                  11
`define RIO_PL_GSM_CASTOUT                  12
`define RIO_PL_GSM_FLUSH                    13

// IO transaction types

`define RIO_IO_NREAD                        0 
`define RIO_IO_NWRITE                       1
`define RIO_IO_SWRITE                       2
`define RIO_IO_NWRITE_R                     3
`define RIO_IO_ATOMIC_INC                   4
`define RIO_IO_ATOMIC_DEC                   5
`define RIO_IO_ATOMIC_SWAP                  6
`define RIO_IO_ATOMIC_TSWAP                 7
`define RIO_IO_ATOMIC_SET                   8
`define RIO_IO_ATOMIC_CLEAR                 9

// Configuration transaction types

`define RIO_CONF_READ                       0
`define RIO_CONF_WRITE                      1

// Local response types

`define RIO_LR_RETRY                        0
`define RIO_LR_EXCLUSIVE                    1
`define RIO_LR_SHARED                       2
`define RIO_LR_OK                           3

// Result types

`define RIO_REQ_DONE                        0
`define RIO_REQ_ERROR                       1

// Snoop request types

`define RIO_LOCAL_READ                      0
`define RIO_LOCAL_READ_TO_OWN               1
`define RIO_LOCAL_READ_LATEST               2
`define RIO_LOCAL_DKILL                     3
`define RIO_LOCAL_IKILL                     4
`define RIO_LOCAL_TLBIE                     5
`define RIO_LOCAL_TLBSYNC                   6
`define RIO_LOCAL_NONCOHERENT               7

// Snoop result types

`define RIO_SNOOP_HIT                       0
`define RIO_SNOOP_MISS                      1

// Memory request type

`define RIO_MEM_READ                        0
`define RIO_MEM_WRAP_READ                   1
`define RIO_MEM_WRITE                       2
`define RIO_MEM_ATOMIC_INC                  3
`define RIO_MEM_ATOMIC_DEC                  4
`define RIO_MEM_ATOMIC_SET                  5
`define RIO_MEM_ATOMIC_CLEAR                6
`define RIO_MEM_ATOMIC_SWAP                 7
`define RIO_MEM_ATOMIC_TSWAP                8

// DATA_IDs for PLI data areas access

`define RIO_GENERAL_DATA                    0
`define RIO_GSM_DATA                        1
`define RIO_IO_DATA                         2
`define RIO_CONFIG_DATA                     3
`define RIO_MP_DATA                         4
`define RIO_MEM_DATA                        5
`define RIO_SNOOP_DATA                      6

// --------------------------- DMA ------------------------------

// Bit positions in DMA Mode and Status Register (msr):
// Error Interrupt (doorbell) Enabled
`define RIO_DMA_EIE_BIT_NUMBER              0    
// End-Of-Transfer Interrupt (doorbell) Enabled
`define RIO_DMA_EOTIE_BIT_NUMBER            1    
// Channel Start 
`define RIO_DMA_CS_BIT_NUMBER               2    
// 1 - Use IO transaction instead of GSM ones 
`define RIO_DMA_NON_GSM_TRANSFER            3    
// 1 - Error occured during the transfer
`define RIO_DMA_ERR_BIT_NUMBER              4    
// 1 - DMA is busy
`define RIO_DMA_BUSY_BIT_NUMBER             5    
// 1 - DMA transfer is finished
`define RIO_DMA_FIN_BIT_NUMBER              6    
// Reserved
`define RIO_DMA_RSRV_BIT_NUMBER             7    

/*Addresses which used in the scenarios for access to memory during I/O transactions*/
// Mode and Status Register
`define RIO_DMA_MSR_ADDR                    'h410 
`define RIO_DMA_MSR_SUB_DW                  0  
// Source Address Register
`define RIO_DMA_SAR_ADDR                    'h410 
`define RIO_DMA_SAR_SUB_DW                  1  
// Destination Address Register
`define RIO_DMA_DAR_ADDR                    'h418 
`define RIO_DMA_DAR_SUB_DW                  0 
// Byte Count Register
`define RIO_DMA_BCR_ADDR                    'h418 
`define RIO_DMA_BCR_SUB_DW                  1   
// Source Instance ID
`define RIO_DMA_SRC_INST_ID_ADDR            'h420 
`define RIO_DMA_SRC_INST_ID_SUB_DW          0 

// --------------------  mailbox control  -----------------------

// Mailbox data sizes

`define RIO_DB_INFO_SIZE                    16
`define RIO_MB_LINE_LENGTH                  `RIO_BITS_IN_DW
`define RIO_MB_LINES_IN_LETTER              512
`define RIO_MB_LETTERS_IN_MB                4
`define RIO_MB_TOTAL_MB                     4
`define RIO_MB_MAX_LETTERS_IN_MB            4
`define RIO_MB_MAX_TOTAL_MB                 4
`define RIO_MB_TOTAL_LINES                  `RIO_MB_LINES_IN_LETTER * `RIO_MB_LETTERS_IN_MB * `RIO_MB_TOTAL_MB
`define RIO_MB_MAX_PACKETS_IN_LETTER        16

// mailbox & Dorbell CSR bit positions

`define RIO_MB_AVAILABLE_BIT_POS            0
`define RIO_MB_FULL_BIT_POS                 1
`define RIO_MB_EMPTY_BIT_POS                2
`define RIO_MB_BUSY_BIT_POS                 3
`define RIO_MB_FAILED_BIT_POS               4
`define RIO_MB_ERROR_BIT_POS                5

`define RIO_MB_BITS_SHIFT                   8

// Bit position in CSR for needed mailbox is: `RIO_MB_BITS_SHIFT * (mailbox number) + `RIO_MB_(needed)_BIT_POS
// For doorbell CSR: just `RIO_MB_(needed)_BIT_POS

// --------------------  memory control  -----------------------

`define RIO_LOCAL_MEMORY_CELL_WIDTH         32

`define RIO_LARGE_GRAN_DW_SIZE              8
`define RIO_SMALL_GRAN_DW_SIZE              4

`define RIO_IO_OK                           0
`define RIO_GSM_OK                          1
`define RIO_MEM_ERROR                       2

// -------------------- for demo scenario -----------------------

`define RIO_PE_SUPPORTS_66_50_34_ADDR       3'b111    
`define RIO_PE_SUPPORTS_66_34_ADDR          3'b110    
`define RIO_PE_SUPPORTS_50_34_ADDR          3'b101    
`define RIO_PE_SUPPORTS_34_ADDR             3'b100    
`define RIO_PE_SUPPORTS_64_48_32_ADDR       3'b011    
`define RIO_PE_SUPPORTS_64_32_ADDR          3'b010    
`define RIO_PE_SUPPORTS_48_32_ADDR          3'b001    
`define RIO_PE_SUPPORTS_32_ADDR             3'b000    

// 8/16 LP/EP mode
`define RIO_MODE_8                          1
`define RIO_MODE_16                         0

// single/double rate
`define RIO_SINGLE_RATE                     1
`define RIO_DOUBLE_RATE                     0

`define RIO_PL_TRANSP_16                    0
`define RIO_PL_TRANSP_32                    1

// discipline for inbound packets
`define RIO_PASS_OLDEST                     0
`define RIO_PASS_BY_PRIO                    1

// function return codes
`define RIO_FUNCTION_OK                     0
`define RIO_FUNCTION_RETRY                  2
`define RIO_FUNCTION_ERROR                  1

// maximum length of RIO packet
`define RIO_PACKET_MAX_LENGTH               256

// maximum size of RIO packet data payload
`define RIO_MAX_DATA_SIZE                   256

// routing method
`define RIO_TABLE_BASED_ROUTING             0
`define RIO_INDEX_BASED_ROUTING             1
`define RIO_SHIFT_BASED_ROUTING             2

// transaction tags size
`define RIO_TRX_TAG_SIZE                    32

`define RIO_NUM_OF_SWITCH_LINKS             16
`define RIO_LP_EP_WIRE_WIDTH                8

`define RIO_NUM_OF_SWITCH_DEMO_ARGS         3

// ------------ cache control settings --------------------

`define RIO_CACHE_NUM_OF_LINES_IN_SET           64
`define RIO_CACHE_NUM_OF_BITS_IN_LINE_NUM       6
// For 32-byte coherence granules, for 64-byte granules one more bit required
`define RIO_CACHE_MIN_NUM_OF_BITS_IN_BYTE_NUM   5
`define RIO_CACHE_NUM_OF_SETS                   8
`define RIO_CACHE_LINE_LENGTH                   64 * `RIO_BITS_IN_BYTE
// xamsbs (2 bits) + ext. address (max(16, 32) = 32 bits) + address (32 bits) = 66 bits
`define RIO_CACHE_MAX_ADDRESS_LENGTH            66
// For 32-byte coherence granules, for 64-byte granules one more bit required
`define RIO_CACHE_MIN_ADDR_TAIL_LENGTH          (`RIO_CACHE_MIN_NUM_OF_BITS_IN_BYTE_NUM + `RIO_CACHE_NUM_OF_BITS_IN_LINE_NUM)
// For 32-byte coherence granules, for 64-byte granules LAST bit is not used 
`define RIO_CACHE_MAX_TAG_LENGTH                (`RIO_CACHE_MAX_ADDRESS_LENGTH - `RIO_CACHE_MIN_ADDR_TAIL_LENGTH) 
`define RIO_CACHE_REQ_BUFFER_SIZE               16



