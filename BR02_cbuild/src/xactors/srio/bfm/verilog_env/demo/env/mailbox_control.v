/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/mailbox_control.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  RIO Mailbox Control model
*
* Notes:        Four mailboxes are instantiated in this module       
*
******************************************************************************/

`timescale 1ns/1ns

module `RIO_MAILBOX_CONTROL_MODULE;

// This parameter specifies instance number for this instance, which will be used 
// to recognize the instance in PLI interface and RIO model functions 
    parameter inst_ID = 0;
    
//    wire [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] inst_ID_Mirror;
//    assign inst_ID_Mirror = inst_ID; 

// This parameter defines whether doorbell hardware is available for usage.
// Bit = 1: doorbell hardware is available; 0: doorbell hardware is off 
//    parameter doorbell_Available = 1'b0;

// This parameter defines which mailbox hardwares are available for usage.
// For bits 0, 1, 2 and 3: Bit = 1: corresponding mailbox is available; 0: mailbox is off 
// For bit 4: bit = 1: doorbell hardware is available; 0: doorbell hardware is off 
    parameter [0 : 4] hw_Available = 5'b0000_0;
    
// PLI registers:

    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_MSGLEN_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_SSIZE_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_LETTER_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_MBOX_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_MSGSEG_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_MP_DW_Size_Reg;
    reg pli_MP_Flag_Reg;

    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_DB_Info_Reg;
    reg pli_DB_Flag_Reg;
    
// common mailbox memory space:
// mailbox and letters margins are fixed: use Get_MB_Base_DW_Index to calculate its values   
//
 
// Memory for storage incoming messages
    reg [0 : `RIO_MB_LINE_LENGTH - 1] mailbox_Memory[0 : `RIO_MB_TOTAL_LINES - 1];

// Memory for storage incoming doorbell data
    reg [0 : `RIO_DB_INFO_SIZE - 1] doorbell_Info; 
    
// Mailbox control status register
    wire [0 : `RIO_BITS_IN_DW - 1] mailbox_CSR;

// Doorbell control status register
    wire [0 : `RIO_BITS_IN_DW - 1] doorbell_CSR;
    
    reg [0 : `RIO_MB_MAX_PACKETS_IN_LETTER - 1] mailbox_Received_Packets [0 : `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB - 1];
    // needed letter status is: status = mailbox_Received_Packets[mailbox_number * `RIO_MB_LETTERS_IN_MB + letter_number]
    // so packet status is: status[packet_number]

    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB - 1] mailbox_Received_Letters;
    // needed letter status is = mailbox_Received_Letters[mailbox_number * `RIO_MB_LETTERS_IN_MB + letter_number]
    // if it is 1 => new message is stored in this letter, all its packets are present
    // if it is 0 => this letter is empty or only part of the message is now stored in it
    
    reg [0 : 3] mailbox_MSGLEN [0 : `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB - 1];
    // packets_in_letter_except_last = mailbox_MSGLEN[mailbox_number * `RIO_MB_LETTERS_IN_MB + letter_number]

    reg [0 : 7] mailbox_SSIZE [0 : `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB - 1];
    // packet_size (except last one) = mailbox_SSIZE[mailbox_number * `RIO_MB_LETTERS_IN_MB + letter_number]
    // (in dowblewords)
    
    reg [0 : 7] mailbox_LSIZE [0 : `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB - 1];
    // last_packet_size = mailbox_LSIZE[mailbox_number * `RIO_MB_LETTERS_IN_MB + letter_number]
    // (in dowblewords)
    
    // Letters status registers

    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB - 1] mailbox_Empty_Letters [0 : `RIO_MB_MAX_TOTAL_MB - 1];
    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB - 1] mailbox_Busy_Letters [0 : `RIO_MB_MAX_TOTAL_MB - 1];
    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB - 1] mailbox_Full_Letters [0 : `RIO_MB_MAX_TOTAL_MB - 1];
    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB - 1] mailbox_Failed_Letters [0 : `RIO_MB_MAX_TOTAL_MB - 1];

    // Doorbell status registers
    
    reg doorbell_Empty;
    reg doorbell_Busy;
    reg doorbell_Full;
    reg doorbell_Failed;

                             
// Working variables and registers

    integer i;  // loop counter

// Connections

    // Mailbox is empty if all its letter slots are empty
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 0 + `RIO_MB_EMPTY_BIT_POS] = &(mailbox_Empty_Letters[0]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 1 + `RIO_MB_EMPTY_BIT_POS] = &(mailbox_Empty_Letters[1]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 2 + `RIO_MB_EMPTY_BIT_POS] = &(mailbox_Empty_Letters[2]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 3 + `RIO_MB_EMPTY_BIT_POS] = &(mailbox_Empty_Letters[3]);

    // Mailbox is busy if at least one of its letter slots is busy
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 0 + `RIO_MB_BUSY_BIT_POS] = |(mailbox_Busy_Letters[0]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 1 + `RIO_MB_BUSY_BIT_POS] = |(mailbox_Busy_Letters[1]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 2 + `RIO_MB_BUSY_BIT_POS] = |(mailbox_Busy_Letters[2]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 3 + `RIO_MB_BUSY_BIT_POS] = |(mailbox_Busy_Letters[3]);

    // Mailbox is full if all its letter slots are full
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 0 + `RIO_MB_FULL_BIT_POS] = &(mailbox_Full_Letters[0]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 1 + `RIO_MB_FULL_BIT_POS] = &(mailbox_Full_Letters[1]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 2 + `RIO_MB_FULL_BIT_POS] = &(mailbox_Full_Letters[2]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 3 + `RIO_MB_FULL_BIT_POS] = &(mailbox_Full_Letters[3]);

    // Mailbox is failed if at least one of its letter slots failed
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 0 + `RIO_MB_FAILED_BIT_POS] = |(mailbox_Failed_Letters[0]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 1 + `RIO_MB_FAILED_BIT_POS] = |(mailbox_Failed_Letters[1]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 2 + `RIO_MB_FAILED_BIT_POS] = |(mailbox_Failed_Letters[2]);
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 3 + `RIO_MB_FAILED_BIT_POS] = |(mailbox_Failed_Letters[3]);

    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 0 + `RIO_MB_AVAILABLE_BIT_POS] = hw_Available[0];
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 1 + `RIO_MB_AVAILABLE_BIT_POS] = hw_Available[1];
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 2 + `RIO_MB_AVAILABLE_BIT_POS] = hw_Available[2];
    assign mailbox_CSR[`RIO_MB_BITS_SHIFT * 3 + `RIO_MB_AVAILABLE_BIT_POS] = hw_Available[3];
    
    assign doorbell_CSR[`RIO_MB_EMPTY_BIT_POS] = doorbell_Empty;
    assign doorbell_CSR[`RIO_MB_BUSY_BIT_POS] = doorbell_Busy;
    assign doorbell_CSR[`RIO_MB_FULL_BIT_POS] = doorbell_Full;
    assign doorbell_CSR[`RIO_MB_FAILED_BIT_POS] = doorbell_Failed;
    assign doorbell_CSR[`RIO_MB_AVAILABLE_BIT_POS] = hw_Available[4];
    
    
// Processes    

// Registers initialization
    
initial
begin
    pli_MP_MSGLEN_Reg = 0;
    pli_MP_SSIZE_Reg = 0;
    pli_MP_LETTER_Reg = 0;
    pli_MP_MBOX_Reg = 0;
    pli_MP_MSGSEG_Reg = 0;
    pli_MP_DW_Size_Reg = 0;
    pli_MP_Flag_Reg = 0;
    pli_DB_Info_Reg = 0;
    pli_DB_Flag_Reg = 0;
    
    for (i = 0; i < `RIO_MB_TOTAL_LINES; i = i + 1)
        mailbox_Memory[i] = 0;

    doorbell_Info = 0; 
    
    mailbox_Received_Letters = 0;

    for (i = 0; i < `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB; i = i + 1)
    begin
        mailbox_Received_Packets[i] = 0;
        mailbox_MSGLEN[i] = 0;
        mailbox_SSIZE[i] = 0;
        mailbox_LSIZE[i] = 0;
    end    
    
    for (i = 0; i < `RIO_MB_MAX_TOTAL_MB; i = i + 1)
    begin
        mailbox_Empty_Letters[i] = {`RIO_MB_MAX_LETTERS_IN_MB {1'b1}};
        mailbox_Busy_Letters[i] = 0;
        mailbox_Full_Letters[i] = 0;
        mailbox_Failed_Letters[i] = 0;
    end

    doorbell_Empty = 1;
    doorbell_Busy = 0;
    doorbell_Full = 0;
    doorbell_Failed = 0;
end    
    
    reg tmp_Result;  // result of packet receiving and check

// Message packet arrival handler
always   
begin                                
    wait(pli_MP_Flag_Reg === 1'b1);
    $display("DEMO: MAILBOX CONTROL in PE %d (time %7t) : message packet received:", inst_ID, $time);
    tmp_Result = Receive_And_Check_MB_Packet(pli_MP_MBOX_Reg);
//    $display("MAILBOX CONTROL: Received letters: %b", mailbox_Received_Letters);
//    if (tmp_Result == 0)
//        $display("MAILBOX CONTROL: message packet received OK");
//    else
//        $display("MAILBOX CONTROL: message packet FAILED");
    pli_MP_Flag_Reg = 1'b0;
end

// Doorbell arrival handler
always @(posedge pli_DB_Flag_Reg)    
begin
//    $display("MAILBOX CONTROL: doorbell received, DB info: h%h", pli_DB_Info_Reg);
    // obtain doorbell info
    doorbell_Info = pli_DB_Info_Reg;    
    // set "busy" and "full" status bits
    doorbell_Busy = 1'b1;
    doorbell_Full = 1'b1;
    // clear "empty" status bit
    doorbell_Empty = 1'b0;
end

/***************************************************************************
 * Function : Get_Msg_Letter_DW_Offset
 *
 * Description: Calculates position of the first doubleword of specified
 *              letter in specified mailbox
 *
 * Returns: calculated position (integer)
 *
 * Notes: 
 *
 **************************************************************************/
function integer Get_Msg_Letter_DW_Offset;
    input [0 : 1] mb_Num;
    input [0 : 1] letter_Num;    
begin
    Get_Msg_Letter_DW_Offset = (mb_Num * `RIO_MB_LINES_IN_LETTER * `RIO_MB_LETTERS_IN_MB) + (letter_Num * `RIO_MB_LINES_IN_LETTER);
end
endfunction

/***************************************************************************
 * Function : Receive_And_Check_MB_Packet
 *
 * Description: This function shall be called when any message packet
 *              was received from RIO model (qualified Remote_MP_Request)
 *              Note that appropriate PLI data shall be already read and
 *              stored in internal mailbox structures  
 *
 * Returns: 0 : Ok, packet was received and status bit was set successfully.
 *          1 : Error, this status bit was already set! Error reported,
 *              no data received.
 *
 * Notes: 
 *
 **************************************************************************/
function Receive_And_Check_MB_Packet;
    input [0 : 1] mb_Num;    // 0 .. 3

    reg [0 : 1] letter_Num;  // 0 .. 3
    reg [0 : 3] packet_Num;  // 0 .. 15  
    reg [0 : `RIO_MB_MAX_PACKETS_IN_LETTER - 1] status;    
    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB - 1] tmp;
    reg [0 : 3] msglen;
    integer ssize;
    integer i;
    integer position;
    reg flag;
begin
    letter_Num = pli_MP_LETTER_Reg;
    packet_Num = pli_MP_MSGSEG_Reg;
//    $display("MAILBOX CONTROL: received packet number %d for mailbox %d, letter slot %d", packet_Num, mb_Num, letter_Num);

    status = mailbox_Received_Packets[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num];

    $display("DEMO: MAILBOX CONTROL in PE %d (time %7t) : packet: msgseg: %d ssize: %d msglen: %d", inst_ID, $time, packet_Num,
        pli_MP_SSIZE_Reg, pli_MP_MSGLEN_Reg);
    
    if (status[packet_Num] === 1'b0)
    begin
        if (status === 0) // no packets received, so it's the first packet!
        begin
//            $display("MAILBOX CONTROL: this packet is the first in the message!");
            // receive size and msglen and store in internal data structures
            msglen = pli_MP_MSGLEN_Reg;
            mailbox_MSGLEN[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = msglen;
            ssize = pli_MP_SSIZE_Reg;  // NON-encoded value
//            $display("MAILBOX CONTROL: packet ssize: %d msglen: %d", ssize, msglen);
            mailbox_SSIZE[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = ssize;
        end
        else    
        begin
//            $display("MAILBOX CONTROL: current packets status: %b", status);
            msglen = mailbox_MSGLEN[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num];
            ssize = mailbox_SSIZE[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num];
//            $display("MAILBOX CONTROL: current packet ssize: %d msglen: %d", ssize, msglen);
        end
        
        if (packet_Num == msglen)  // it is the last (by number) packet            
        begin
//            $display("MAILBOX CONTROL: LAST packet (by number)!, status: %b", status);
            ssize = pli_MP_DW_Size_Reg;
            mailbox_LSIZE[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = ssize;
//            $display("MAILBOX CONTROL: LAST packet ssize: %d", ssize);
        end            
            
        // receive packet data and store in the mailbox memory space
        
        position = Get_Msg_Letter_DW_Offset(mb_Num, letter_Num);        
        position = position + (packet_Num * ssize);
        for (i = 0; i < pli_MP_DW_Size_Reg; i = i + 1)
        begin
            mailbox_Memory[position + i] = $HW_Get_PLI_DW_Value(inst_ID, `RIO_MP_DATA, i);
//            $display("MAILBOX CONTROL DATA: packet %d, DW %d : %h", packet_Num, i, mailbox_Memory[position + i]);
        end
         
        // set packet status bit
        status[packet_Num] = 1'b1;

//        $display("MAILBOX CONTROL: new status: %b", status);

        mailbox_Received_Packets[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = status;
        // check: may be now all packets of the letter are received?
//        if (status[0 : (msglen + 1) - 1] === ~((msglen + 1)'b0))
        flag = 1'b0;    
        i = 0;
        while ((flag === 1'b0) && (i <= msglen))
        begin
            if (status[i] !== 1'b1)
                flag = 1'b1;
            i = i + 1;                      
        end 

        if (flag === 1'b0)
        begin
//            $display("MAILBOX CONTROL: Now letter slot is FULL!: %d", status);
            // really full => set "full" status bit and store size of the last packet
            tmp = mailbox_Full_Letters[mb_Num];
            tmp[letter_Num] = 1'b1;        
            mailbox_Full_Letters[mb_Num] = tmp;
            mailbox_Received_Letters[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = 1'b1;
        end
        // make sure that "busy" status bit is set
        tmp = mailbox_Busy_Letters[mb_Num];
        tmp[letter_Num] = 1'b1;        
        mailbox_Busy_Letters[mb_Num] = tmp;
        // make sure that "empty" status bit is cleared
        tmp = mailbox_Empty_Letters[mb_Num];
        tmp[letter_Num] = 1'b0;        
        mailbox_Empty_Letters[mb_Num] = tmp;
        // return Ok        
        Receive_And_Check_MB_Packet = 0;
    end 
    else   // this packet is already received!
    begin
        $display("DEMO ERROR: MAILBOX CONTROL in PE %d (time %7t) : this packet has already received!", inst_ID, $time);
        // set "failed" status bit 
        tmp = mailbox_Failed_Letters[mb_Num];
        tmp[letter_Num] = 1'b1;        
        mailbox_Failed_Letters[mb_Num] = tmp;
        // return error
        Receive_And_Check_MB_Packet = 1;
    end
end
endfunction

/***************************************************************************
 * Function : Get_Msg_DW_Size
 *
 * Description:
 *
 * Returns: Size of message in doublewords 
 *
 * Notes: 
 *
 **************************************************************************/
function integer Get_Msg_DW_Size;
    input [0 : 1] mb_Num;      // 0 .. 3
    input [0 : 1] letter_Num;  // 0 .. 3
begin
    Get_Msg_DW_Size = 
        mailbox_MSGLEN[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] * // it's except last packet!
        mailbox_SSIZE[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] +  // packet size except last (DWs)
        mailbox_LSIZE[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num];   // last packet size (DWs)
end
endfunction

/***************************************************************************
 * Function : Get_Mailbox_Mem_MS_Word
 *
 * Description:
 *
 * Returns: MS Word from the specified line of Mailbox Control memory space
 *
 * Notes: 
 *
 **************************************************************************/
function [0 : 31] Get_Mailbox_Mem_MS_Word;
    input dw_Offset;      
    integer dw_Offset;
    reg [0 : `RIO_MB_LINE_LENGTH - 1] mem_Line;
begin
    if ((dw_Offset >= 0) && dw_Offset <= (`RIO_MB_TOTAL_LINES - 1))
    begin
        mem_Line = mailbox_Memory[dw_Offset];
        Get_Mailbox_Mem_MS_Word = mem_Line[0 : `RIO_BITS_IN_WORD - 1];
    end
    else
    begin
        $display("DEMO ERROR: MAILBOX CONTROL in PE %d (time %7t) : incorrect MB memory DW offset specified: %d", inst_ID, $time, 
            dw_Offset);
        Get_Mailbox_Mem_MS_Word = 0;
    end        
end
endfunction

/***************************************************************************
 * Function : Get_Mailbox_Mem_LS_Word
 *
 * Description:
 *
 * Returns: LS Word from the specified line of Mailbox Control memory space
 *
 * Notes: 
 *
 **************************************************************************/
function [0 : 31] Get_Mailbox_Mem_LS_Word;
    input dw_Offset;      
    integer dw_Offset;
    reg [0 : `RIO_MB_LINE_LENGTH - 1] mem_Line;
begin
    if ((dw_Offset >= 0) && dw_Offset <= (`RIO_MB_TOTAL_LINES - 1))
    begin
        mem_Line = mailbox_Memory[dw_Offset];
        Get_Mailbox_Mem_LS_Word = mem_Line[`RIO_BITS_IN_WORD : `RIO_BITS_IN_DW - 1];
    end
    else
    begin
        $display("DEMO ERROR: MAILBOX CONTROL in PE %d (time %7t) : incorrect MB memory DW offset specified: %d", inst_ID, $time, 
            dw_Offset);
        Get_Mailbox_Mem_LS_Word = 0;
    end    
end
endfunction

/***************************************************************************
 * Task : Free_Msg_Letter
 *
 * Description: Called from demo scenario to free specified mailbox letter
 *
 * Notes:
 *
 **************************************************************************/
task Free_Msg_Letter;
    input [0 : 1] mb_Num;      // 0 .. 3
    input [0 : 1] letter_Num;  // 0 .. 3

    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB - 1] tmp;
begin
    mailbox_MSGLEN[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = 0;
    mailbox_SSIZE[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = 0;
    mailbox_LSIZE[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = 0;

    mailbox_Received_Packets[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = 0;
    mailbox_Received_Letters[mb_Num * `RIO_MB_LETTERS_IN_MB + letter_Num] = 0;
    
    // clear "full" status bit
    tmp = mailbox_Full_Letters[mb_Num];
    tmp[letter_Num] = 1'b0;        
    mailbox_Full_Letters[mb_Num] = tmp;
    // clear "busy" status bit
    tmp = mailbox_Busy_Letters[mb_Num];
    tmp[letter_Num] = 1'b0;        
    mailbox_Busy_Letters[mb_Num] = tmp;
    // set "empty" status bit
    tmp = mailbox_Empty_Letters[mb_Num];
    tmp[letter_Num] = 1'b1;        
    mailbox_Empty_Letters[mb_Num] = tmp;
    // clear "failed" status bit
    tmp = mailbox_Failed_Letters[mb_Num];
    tmp[letter_Num] = 1'b0;        
    mailbox_Failed_Letters[mb_Num] = tmp;
end
endtask

/***************************************************************************
 * Task : Free_Doorbell
 *
 * Description: Called from demo scenario to free doorbell hardware
 *
 * Notes:
 *
 **************************************************************************/
task Free_Doorbell;
begin
    doorbell_Empty = 1;
    doorbell_Busy = 0;
    doorbell_Full = 0;
    doorbell_Failed = 0;
end
endtask

endmodule

/*****************************************************************************/
