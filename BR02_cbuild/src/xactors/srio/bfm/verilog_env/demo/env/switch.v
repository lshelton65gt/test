/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/switch.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*                information.
*               
* Description:  Switch module
*            
* Notes:        
*
******************************************************************************/

`timescale 1ns/1ns

module `RIO_SWITCH_MODULE (
    clock, tclk, tframe, rclk, rframe, 
    td0, tdl0, rd0, rdl0, td1, tdl1, rd1, rdl1,
    td2, tdl2, rd2, rdl2, td3, tdl3, rd3, rdl3,
    td4, tdl4, rd4, rdl4, td5, tdl5, rd5, rdl5,
    td6, tdl6, rd6, rdl6, td7, tdl7, rd7, rdl7,
    td8, tdl8, rd8, rdl8, td9, tdl9, rd9, rdl9,
    td10, tdl10, rd10, rdl10, td11, tdl11, rd11, rdl11,
    td12, tdl12, rd12, rdl12, td13, tdl13, rd13, rdl13,
    td14, tdl14, rd14, rdl14, td15, tdl15, rd15, rdl15
    );
    
    input clock; // self clock for tclk forcing

    input [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] rclk;    
    input [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] rframe;

    output [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] tclk;
    output [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] tframe;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd0;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl0;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td0;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl0;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd1;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl1;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td1;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl1;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd2;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl2;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td2;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl2;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd3;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl3;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td3;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl3;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd4;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl4;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td4;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl4;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd5;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl5;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td5;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl5;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd6;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl6;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td6;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl6;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd7;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl7;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td7;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl7;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd8;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl8;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td8;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl8;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd9;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl9;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td9;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl9;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd10;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl10;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td10;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl10;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd11;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl11;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td11;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl11;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd12;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl12;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td12;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl12;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd13;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl13;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td13;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl13;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd14;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl14;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td14;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl14;

    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd15;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl15;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td15;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl15;

// (1)
// This parameter specifies instance number for the Switch instance, which will be used 
// to recognize Switch instance in PLI interface and RIO model functions 
    parameter inst_ID = 0;

// (2)
// This parameter shall be set as (maximum(numbers of all used LP-EP switch links) + 1)
    parameter links_Number = `RIO_NUM_OF_SWITCH_LINKS;
    
// PLI registers

//    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_Link_Index_Reg;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_0;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_1;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_2;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_3;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_4;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_5;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_6;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_7;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_8;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_9;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_10;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_11;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_12;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_13;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_14;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TD_Reg_15;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_0;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_1;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_2;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_3;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_4;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_5;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_6;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_7;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_8;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_9;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_10;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_11;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_12;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_13;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_14;
    reg [0 : `RIO_PLI_SHORT_REGISTER_SIZE - 1] pli_Pins_TDL_Reg_15;
    reg [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] pli_Pins_TFrame_Reg;
    reg [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] pli_Pins_TCLK_Reg;
    reg [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] pli_Pins_Flag_Reg;

// Input/Output control registers

    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td_Reg[0 : `RIO_NUM_OF_SWITCH_LINKS - 1];
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl_Reg[0 : `RIO_NUM_OF_SWITCH_LINKS - 1];
    reg [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] tclk_Reg;
    reg [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] tframe_Reg;

    reg [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] old_RCLK;    
    integer i_R;
    integer i_T;

    assign tclk = tclk_Reg;
    assign tframe = tframe_Reg;

    assign td0 = td_Reg[0];
    assign tdl0 = tdl_Reg[0];
    assign td1 = td_Reg[1];
    assign tdl1 = tdl_Reg[1];
    assign td2 = td_Reg[2];
    assign tdl2 = tdl_Reg[2];
    assign td3 = td_Reg[3];
    assign tdl3 = tdl_Reg[3];
    assign td4 = td_Reg[4];
    assign tdl4 = tdl_Reg[4];
    assign td5 = td_Reg[5];
    assign tdl5 = tdl_Reg[5];
    assign td6 = td_Reg[6];
    assign tdl6 = tdl_Reg[6];
    assign td7 = td_Reg[7];
    assign tdl7 = tdl_Reg[7];
    assign td8 = td_Reg[8];
    assign tdl8 = tdl_Reg[8];
    assign td9 = td_Reg[9];
    assign tdl9 = tdl_Reg[9];
    assign td10 = td_Reg[10];
    assign tdl10 = tdl_Reg[10];
    assign td11 = td_Reg[11];
    assign tdl11 = tdl_Reg[11];
    assign td12 = td_Reg[12];
    assign tdl12 = tdl_Reg[12];
    assign td13 = td_Reg[13];
    assign tdl13 = tdl_Reg[13];
    assign td14 = td_Reg[14];
    assign tdl14 = tdl_Reg[14];
    assign td15 = td_Reg[15];
    assign tdl15 = tdl_Reg[15];

/* ********************************************************************* */

// initialization
initial
begin
    Reset_Switch;
end

// Input LP-EP clock handler
always @(rclk)
begin
//    $display("SWITCH %d: RCLK CHANGED", inst_ID);
    for (i_R = 0; i_R < links_Number; i_R = i_R + 1) 
        if (rclk[i_R] !== old_RCLK[i_R])
        begin
            case (i_R)

                0: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd0, rdl0, rclk[i_R]);

                1: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd1, rdl1, rclk[i_R]);

                2: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd2, rdl2, rclk[i_R]);

                3: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd3, rdl3, rclk[i_R]);

                4: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd4, rdl4, rclk[i_R]);

                5: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd5, rdl5, rclk[i_R]);

                6: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd6, rdl6, rclk[i_R]);

                7: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd7, rdl7, rclk[i_R]);

                8: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd8, rdl8, rclk[i_R]);

                9: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd9, rdl9, rclk[i_R]);

                10: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd10, rdl10, rclk[i_R]);

                11: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd11, rdl11, rclk[i_R]);

                12: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd12, rdl12, rclk[i_R]);

                13: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd13, rdl13, rclk[i_R]);

                14: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd14, rdl14, rclk[i_R]);

                15: $HW_Switch_Get_Pins(inst_ID, i_R, rframe[i_R], rd15, rdl15, rclk[i_R]);
                    
            endcase
        end    
    old_RCLK = rclk;
end 

// Output LP-EP pins driver
always @(pli_Pins_Flag_Reg)
begin
    for (i_T = 0; i_T < links_Number; i_T = i_T + 1) 
        if (pli_Pins_Flag_Reg[i_T])
        begin
            //$display("SWITCH %d: TCLK[ %3d ] CHANGED: frame = %b", inst_ID, pli_Pins_Link_Index_Reg, pli_Pins_TFrame_Reg);
            case (i_T)
                0: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_0;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_0;
                    end    

                1: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_1;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_1;
                    end    

                2: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_2;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_2;
                    end    

                3: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_3;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_3;
                    end    

                4: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_4;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_4;
                    end    

                5: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_5;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_5;
                    end    

                6: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_6;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_6;
                    end    

                7: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_7;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_7;
                    end    

                8: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_8;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_8;
                    end    

                9: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_9;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_9;
                    end    

                10: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_10;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_10;
                    end    

                11: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_11;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_11;
                    end    

                12: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_12;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_12;
                    end    

                13: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_13;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_13;
                    end    

                14: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_14;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_14;
                    end    

                15: 
                    begin
                        td_Reg[i_T] = pli_Pins_TD_Reg_15;
                        tdl_Reg[i_T] = pli_Pins_TDL_Reg_15;
                    end    
            endcase
            tframe_Reg[i_T] = pli_Pins_TFrame_Reg[i_T];
            tclk_Reg[i_T] = pli_Pins_TCLK_Reg[i_T];
            pli_Pins_Flag_Reg[i_T] = 1'b0;
        end    
end

// Input external clock handler
always @(clock)
begin
    $HW_Switch_Clock_Edge(inst_ID);
end

/***************************************************************************
 * Task : Reset_Switch
 *
 * Description: Switch reset task
 *
 * Notes:
 *
 **************************************************************************/
task Reset_Switch;
begin
    old_RCLK = rclk;
end
endtask

endmodule

