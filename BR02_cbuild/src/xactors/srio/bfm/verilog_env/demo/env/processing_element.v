/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/processing_element.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  RIO processing element environment
*
* Notes:        
*
******************************************************************************/

`timescale 1ns/1ns

module `RIO_PE_MODULE (clock, tclk, tframe, td, tdl, rclk, rframe, rd, rdl);
    
    input clock; // self clock for tclk forcing

    input rclk;    
    input rframe;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    output tclk;
    output tframe;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;

    reg tclk_Reg;
    reg tframe_Reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td_Reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl_Reg;

    assign tclk = tclk_Reg;
    assign tframe = tframe_Reg;
    assign td = td_Reg;
    assign tdl = tdl_Reg;

// (1)
// This parameter specifies instance number for the PE instance, which will be used 
// to recognize PE instance in PLI interface and RIO model functions 
    parameter inst_ID = 0;

// (2)
// This parameter is the mask which selects which submodules shall be included in this PE
// instance. 1 means "include", 0 means "do not include". Default value: include all.
// Positions: 1: cache control
//            2: mailbox control
//            3: memory control
    parameter [1 : 3] include_Instances_Mask = 3'b111; 

// (3)
// This parameter selects which scenario shall be used with the PE instance 
// It can be used by the Demo Scenario, which is the part of PE model
// Zero value means that Demo Scenario is absent in this PE instance (for example,
// in case of DMA PE or memory-only PE)  
//    parameter scenario_Number = 0;  

/* common registers for data exchange through PLI */
    `include "pli_registers.vh"

/* common registers for data/events exchange between the top module and demo scenarios of PE-s */
/* also some needed variables definition and initialization                                    */
    `include "demo_registers.vh"

/* demo scenario data */

    // Data loop indexes

    integer letter_Index;
    integer dw_Index;
    integer gran_Index;
    integer mem_Index;
    integer alt_Index;

    // Data for message receiving     

    integer mb_Num;
    integer letter_Num;
    integer msg_DW_Size;
    integer msg_DW_Offset;
    integer msg_Granules_In_Msg;
    integer msg_DW_In_Granule;
    reg [0 : `RIO_BITS_IN_WORD - 1] msg_Src_Address_Offset;
    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB - 1] last_Mailbox_Received_Letters;
    reg [0 : `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB - 1] just_Arrived_Letters;

    // Data for simple demo scenario results check     

    reg [0 : `RIO_BITS_IN_WORD - 1] temp_MS_Word;
    reg [0 : `RIO_BITS_IN_WORD - 1] temp_LS_Word;
    reg [0 : `RIO_BITS_IN_WORD - 1] temp_etl_MS_Word;
    reg [0 : `RIO_BITS_IN_WORD - 1] temp_etl_LS_Word;

    reg [0 : `RIO_BITS_IN_WORD - 1] temp_MS_Word_1;
    reg [0 : `RIO_BITS_IN_WORD - 1] temp_LS_Word_1;
    reg [0 : `RIO_BITS_IN_WORD - 1] temp_etl_MS_Word_1;
    reg [0 : `RIO_BITS_IN_WORD - 1] temp_etl_LS_Word_1;

    reg [0 : `RIO_DB_INFO_SIZE - 1] temp_DB_Info; 

    integer temp_Res;

    // Counter of errors occured in demo scenario
    integer error_Counter;

    // Temporary storage of transaction tags of request were sent
    integer tmp_Trx_Tag_1;
    integer tmp_Trx_Tag_2;
    integer tmp_Trx_Tag_3;
    integer tmp_Trx_Tag_4;
    integer tmp_Trx_Tag_5;

    // Transport info to sent config/message requests
    integer cur_Tr_Info;

    // Transactions results and indexes
    
    integer gsm_Result;
    reg gsm_Hit;
    integer gsm_Cache_Index;

    integer io_Cache_Index;
    
    // Loop indexes
    integer index_1;
    integer index_2;
    integer index_3;
    integer index_4;
    integer index_5;
    
/////////////////////////////////////////////////////////////////

/* Submodules instantiation */

    `RIO_CACHE_CONTROL_MODULE #(include_Instances_Mask[1] ? (inst_ID /* * 10 + 1 */) : 0) `RIO_CACHE_CONTROL(); 
    `RIO_MAILBOX_CONTROL_MODULE #(include_Instances_Mask[2] ? (inst_ID /* * 10 + 2 */) : 0) `RIO_MAILBOX_CONTROL();
    `RIO_MEMORY_CONTROL_MODULE #(include_Instances_Mask[3] ? (inst_ID /* * 10 + 3 */) : 0) `RIO_MEMORY_CONTROL(); 

    assign pli_GSM_Trx_Tag_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg;
    assign pli_GSM_Result_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Result_Reg;
    assign pli_GSM_Err_Code_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg;
    assign pli_GSM_Req_Done_Flag_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg;

    assign pli_Loc_Trx_Tag_Mirror = `RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg;
    assign pli_Loc_Response_Mirror = `RIO_CACHE_CONTROL.pli_Loc_Response_Reg;
    assign pli_Loc_Response_Flag_Mirror = `RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg;

/* ********************************************************************* */

// External clock handler 
always @(clock)
begin
    $HW_Clock_Edge(inst_ID);
end 

// Input LP-EP clock handler 
always @(rclk)
begin
    $HW_Get_Pins(inst_ID, rframe, rd, rdl, rclk);
end 

// Output LP-EP pins driver
always @(posedge pli_Pins_Flag_Reg)
begin
    tframe_Reg = pli_Pins_TFrame_Reg;
    td_Reg = pli_Pins_TD_Reg;
    tdl_Reg = pli_Pins_TDL_Reg;
    tclk_Reg = pli_Pins_TCLK_Reg;
    pli_Pins_Flag_Reg = 1'b0;
end

//---------------------------------------------------------------------------------------
//                                      DEMO SCENARIOS
//---------------------------------------------------------------------------------------

// initialization
initial
begin
//    $display("PE1");
    error_Counter = 0;

    last_Mailbox_Received_Letters = {(`RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB) {1'b0}};
    just_Arrived_Letters = {(`RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB) {1'b0}};
end

// ---------------------------  SIMPLE DEMO SCENARIOS  ----------------------------


// Simple demo scenario 1 
always @(posedge demo_Process_Scenario_1)
begin
    $display("DEMO (time %7t) : Scenario 1 of PE %d started", $time, inst_ID);


    $display("DEMO (time %7t) : PE %d: Config request: Write DMA Src_Tr_Info:", $time, inst_ID);
    if (`RIO_DMA_SRC_INST_ID_SUB_DW === 1'b0)
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, inst_ID, 0);
    else
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, 0, inst_ID);
    cur_Tr_Info = $HW_Get_Tr_Info(inst_ID, 2);
    while ($HW_Lg_Configuration_Request(inst_ID, 1 /*prio*/,
        cur_Tr_Info /*transport_Info*/,
        'hFF /*hop_Count*/, `RIO_CONF_WRITE,
        `RIO_DMA_SRC_INST_ID_ADDR /*config_Offset*/, `RIO_DMA_SRC_INST_ID_SUB_DW /*sub_Dw_Pos*/,
        1 /*dw_Size*/, cur_Config_Trx_Tag /*trx_Tag*/, `RIO_FALSE /*data_flag*/        
        ) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    cur_Config_Trx_Tag = cur_Config_Trx_Tag + 1; 
//    $display("DEMO: Config request: Write DMA Src_Tr_Info: waiting for response...");
    wait (pli_Config_Req_Done_Flag_Reg === 1'b1 && pli_Config_Trx_Tag_Reg === cur_Config_Trx_Tag - 1); 
//    $display("DEMO: Config request: Write DMA Src_Tr_Info: response received, result: %d", pli_Config_Result_Reg);

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA SAR:", $time, inst_ID);
    if (`RIO_DMA_SAR_SUB_DW === 1'b0)
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, `RIO_DEMO_DMA_SRC_ADDRESS, 0);
    else
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, 0, `RIO_DEMO_DMA_SRC_ADDRESS);
//    cur_Tr_Info = $HW_Get_Tr_Info(inst_ID, 2);
    while ($HW_Lg_Configuration_Request(inst_ID, 1 /*prio*/,
        cur_Tr_Info /*transport_Info*/,
        'hFF /*hop_Count*/, `RIO_CONF_WRITE,
        `RIO_DMA_SAR_ADDR /*config_Offset*/, `RIO_DMA_SAR_SUB_DW /*sub_Dw_Pos*/,
        1 /*dw_Size*/, cur_Config_Trx_Tag /*trx_Tag*/, `RIO_FALSE /*data_flag*/        
        ) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    cur_Config_Trx_Tag = cur_Config_Trx_Tag + 1; 
//    $display("DEMO: Config request: Write DMA SAR: waiting for response...");
    wait (pli_Config_Req_Done_Flag_Reg === 1'b1 && pli_Config_Trx_Tag_Reg === cur_Config_Trx_Tag - 1); 
//    $display("DEMO: Config request: Write DMA SAR: response received, result: %d", pli_Config_Result_Reg);

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA DAR:", $time, inst_ID);
    if (`RIO_DMA_DAR_SUB_DW === 1'b0)
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, `RIO_DEMO_DMA_DEST_ADDRESS, 0);
    else
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, 0, `RIO_DEMO_DMA_DEST_ADDRESS);
//    cur_Tr_Info = $HW_Get_Tr_Info(inst_ID, 2);
    while ($HW_Lg_Configuration_Request(inst_ID, 1 /*prio*/,
        cur_Tr_Info /*transport_Info*/,
        'hFF /*hop_Count*/, `RIO_CONF_WRITE,
        `RIO_DMA_DAR_ADDR /*config_Offset*/, `RIO_DMA_DAR_SUB_DW /*sub_Dw_Pos*/,
        1 /*dw_Size*/, cur_Config_Trx_Tag /*trx_Tag*/, `RIO_FALSE /*data_flag*/        
        ) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    cur_Config_Trx_Tag = cur_Config_Trx_Tag + 1; 
//    $display("DEMO: Config request: Write DMA DAR: waiting for response...");
    wait (pli_Config_Req_Done_Flag_Reg === 1'b1 && pli_Config_Trx_Tag_Reg === cur_Config_Trx_Tag - 1); 
//    $display("DEMO: Config request: Write DMA DAR: response received, result: %d", pli_Config_Result_Reg);

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA BCR:", $time, inst_ID);
    if (`RIO_DMA_BCR_SUB_DW === 1'b0)
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, `RIO_DEMO_DMA_DW_SIZE * `RIO_BYTES_IN_DW, 0);
    else
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, 0, `RIO_DEMO_DMA_DW_SIZE * `RIO_BYTES_IN_DW);
//    cur_Tr_Info = $HW_Get_Tr_Info(inst_ID, 2);
    while ($HW_Lg_Configuration_Request(inst_ID, 1 /*prio*/,
        cur_Tr_Info /*transport_Info*/,
        'hFF /*hop_Count*/, `RIO_CONF_WRITE,
        `RIO_DMA_BCR_ADDR /*config_Offset*/, `RIO_DMA_BCR_SUB_DW /*sub_Dw_Pos*/,
        1 /*dw_Size*/, cur_Config_Trx_Tag /*trx_Tag*/, `RIO_FALSE /*data_flag*/        
        ) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    cur_Config_Trx_Tag = cur_Config_Trx_Tag + 1; 
//    $display("DEMO: Config request: Write DMA BCR: waiting for response...");
    wait (pli_Config_Req_Done_Flag_Reg === 1'b1 && pli_Config_Trx_Tag_Reg === cur_Config_Trx_Tag - 1); 
//    $display("DEMO: Config request: Write DMA BCR: response received, result: %d", pli_Config_Result_Reg);

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA MSR:", $time, inst_ID);
    if (`RIO_DMA_MSR_SUB_DW === 1'b0)
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, 8'b11110000, 0);
    else
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, 0, 8'b11110000);
//    cur_Tr_Info = $HW_Get_Tr_Info(inst_ID, 2);
    while ($HW_Lg_Configuration_Request(inst_ID, 1 /*prio*/,
        cur_Tr_Info /*transport_Info*/,
        'hFF /*hop_Count*/, `RIO_CONF_WRITE,
        `RIO_DMA_MSR_ADDR /*config_Offset*/, `RIO_DMA_MSR_SUB_DW /*sub_Dw_Pos*/,
        1 /*dw_Size*/, cur_Config_Trx_Tag /*trx_Tag*/, `RIO_FALSE /*data_flag*/        
        ) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    cur_Config_Trx_Tag = cur_Config_Trx_Tag + 1; 
//    $display("DEMO: Config request: Write DMA MSR: waiting for response...");
    wait (pli_Config_Req_Done_Flag_Reg === 1'b1 && pli_Config_Trx_Tag_Reg === cur_Config_Trx_Tag - 1); 
//    $display("DEMO: Config request: Write DMA MSR: response received, result: %d", pli_Config_Result_Reg);

    $display("DEMO (time %7t) : PE %d: Now DMA shall start the transfer", $time, inst_ID);
    $display("DEMO (time %7t) : PE %d: Waiting for Doorbell arrival from DMA...", $time, inst_ID);

//--------------------------------------------------------------------------------------

    // start "ALT" process - IO write and read requests 
    demo_Process_Scenario_3 = 1'b1;

//--------------------------------------------------------------------------------------

    wait (`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg === 1'b1); 

    if (`RIO_MAILBOX_CONTROL.doorbell_Info == `RIO_DMA_FIN_DB_INFO)
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: Finished OK", $time, inst_ID);
    else if (`RIO_MAILBOX_CONTROL.doorbell_Info == `RIO_DMA_ERR_DB_INFO)
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: ERROR", $time, inst_ID);
    else
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: Unknown...: %b", $time, inst_ID, 
           `RIO_MAILBOX_CONTROL.doorbell_Info);

    `RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg = 1'b0;
           
    $display("DEMO (time %7t) : PE %d: Now lets compare init memory values with values written by DMA transfer:", $time, inst_ID);

    for (mem_Index = 0; mem_Index < `RIO_DEMO_DMA_DW_SIZE; mem_Index = mem_Index + 1)
    begin    
//        $display("DEMO (time %7t) : PE %d: comparation %d: READ SRC", $time, inst_ID, mem_Index);
        tmp_Trx_Tag_1 = cur_IO_Trx_Tag; 
        while ($HW_Lg_IO_Request(inst_ID /*handle*/, `RIO_IO_NREAD /*ttype*/, 1 /*prio*/,
            `RIO_DEMO_DMA_SRC_ADDRESS + mem_Index * `RIO_BYTES_IN_DW /*address*/,
            0 /*extended_Address*/, 0 /*xamsbs*/,
            1 /*dw_Size*/, 0 /*offset*/, 8 /*byte_Num*/,
            tmp_Trx_Tag_1 /*trx_Tag*/, `RIO_FALSE /*data_flag*/
            ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
        cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;
        wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_1));
        temp_etl_MS_Word = $HW_Get_PLI_MS_Word(inst_ID, `RIO_IO_DATA, 0);
        temp_etl_LS_Word = $HW_Get_PLI_LS_Word(inst_ID, `RIO_IO_DATA, 0);
        pli_IO_Req_Done_Flag_Reg = 1'b0;
        
//        $display("DEMO (time %7t) : PE %d: comparation %d: READ DEST", $time, inst_ID, mem_Index);
        tmp_Trx_Tag_1 = cur_IO_Trx_Tag; 
        while ($HW_Lg_IO_Request(inst_ID /*handle*/, `RIO_IO_NREAD /*ttype*/, 1 /*prio*/,
            `RIO_DEMO_DMA_DEST_ADDRESS + mem_Index * `RIO_BYTES_IN_DW /*address*/,
            0 /*extended_Address*/, 0 /*xamsbs*/,
            1 /*dw_Size*/, 0 /*offset*/, 8 /*byte_Num*/,
            tmp_Trx_Tag_1 /*trx_Tag*/, `RIO_FALSE /*data_flag*/
            ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
        cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;
        wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_1));
        temp_MS_Word = $HW_Get_PLI_MS_Word(inst_ID, `RIO_IO_DATA, 0);
        temp_LS_Word = $HW_Get_PLI_LS_Word(inst_ID, `RIO_IO_DATA, 0);
        pli_IO_Req_Done_Flag_Reg = 1'b0;

        $display("DEMO PE %d: DMA DW number %d: SRC : MSW = %h, LSW = %h", inst_ID, mem_Index, temp_etl_MS_Word, temp_etl_LS_Word);
        $display("DEMO PE %d: DMA DW number %d: DEST: MSW = %h, LSW = %h", inst_ID, mem_Index, temp_MS_Word, temp_LS_Word);
        if ((temp_etl_MS_Word !== temp_MS_Word) || (temp_etl_LS_Word !== temp_LS_Word))
        begin
            error_Counter = error_Counter + 1;
            $display("DEMO ERROR (time %7t) : PE %d: DMA DW number %d: values are not equal", $time, inst_ID, mem_Index);
        end
    end

    wait (demo_Process_Scenario_3 === 1'b0);

    $display("DEMO (time %7t) : Scenario 1 of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_1 = 1'b0;
end

// Simple demo scenario 2
always @(posedge demo_Process_Scenario_2)  // Messages receiving, see MESSAGES WAITER below
begin
    $display("DEMO (time %7t) : Scenario 2 of PE %d started", $time, inst_ID);


    wait (demo_Process_Scenario_2 === 1'b0);   // cleared by MESSAGES WAITER process

    $display("DEMO (time %7t) : Scenario 2 of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_2 = 1'b0;
end

// Simple demo scenario 3
always @(posedge demo_Process_Scenario_3)      // ALT process: IO write requests and check result
begin
    $display("DEMO: Scenario 3 of PE %d started", inst_ID);

    $display("DEMO (time %7t) : PE %d: ALT IO write requests to alternative memory area", $time, inst_ID);

    for (alt_Index = 0; alt_Index <= `RIO_DEMO_DMA_ALT_DW_SIZE; alt_Index = alt_Index + 1)
    begin    
        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, 0, (6'b111111 << 25) + (alt_Index << 15) + 6'b111111,
                                                      (6'b101010 << 25) + (alt_Index << 15) + 6'b101010);
        tmp_Trx_Tag_3 = cur_IO_Trx_Tag; 
        while (  
            $HW_Lg_IO_Request(inst_ID /*handle*/, `RIO_IO_NWRITE /*ttype*/, 1 /*prio*/,
            `RIO_DEMO_DMA_ALT_ADDRESS + alt_Index * `RIO_BYTES_IN_DW /*address*/,
            0 /*extended_Address*/, 0 /*xamsbs*/,
            1 /*dw_Size*/, 0 /*offset*/, 8 /*byte_Num*/,
            tmp_Trx_Tag_3 /*trx_Tag*/, `RIO_FALSE /*data_flag*/
            ) == `RIO_RETRY )
            #(`RIO_WAIT_BEFORE_RETRY);

        cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;
//        wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_3));
//        pli_IO_Req_Done_Flag_Reg = 1'b0;
    end

    $display("DEMO (time %7t) : PE %d: ALT IO: Now lets compare written values with the desired ones:", $time, inst_ID);

    for (alt_Index = 0; alt_Index < `RIO_DEMO_DMA_ALT_DW_SIZE; alt_Index = alt_Index + 1)
    begin    
        temp_etl_MS_Word_1 = (6'b111111 << 25) + (alt_Index << 15) + 6'b111111;
        temp_etl_LS_Word_1 = (6'b101010 << 25) + (alt_Index << 15) + 6'b101010;
        tmp_Trx_Tag_3 = cur_IO_Trx_Tag; 
        while ($HW_Lg_IO_Request(inst_ID /*handle*/, `RIO_IO_NREAD /*ttype*/, 1 /*prio*/,
            `RIO_DEMO_DMA_ALT_ADDRESS + alt_Index * `RIO_BYTES_IN_DW /*address*/,
            0 /*extended_Address*/, 0 /*xamsbs*/,
            1 /*dw_Size*/, 0 /*offset*/, 8 /*byte_Num*/,
            tmp_Trx_Tag_3 /*trx_Tag*/, `RIO_FALSE /*data_flag*/
            ) == `RIO_RETRY )
            #(`RIO_WAIT_BEFORE_RETRY);

        cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;
        wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_3));
        temp_MS_Word_1 = $HW_Get_PLI_MS_Word(inst_ID, `RIO_IO_DATA, 0);
        temp_LS_Word_1 = $HW_Get_PLI_LS_Word(inst_ID, `RIO_IO_DATA, 0);
        pli_IO_Req_Done_Flag_Reg = 1'b0;
        $display("DEMO PE %d: ALT DW number %d: MUST BE: MSW = %h, LSW = %h",
            inst_ID, alt_Index, temp_etl_MS_Word_1, temp_etl_LS_Word_1);
        $display("DEMO PE %d: ALT DW number %d: WRITTEN: MSW = %h, LSW = %h",
            inst_ID, alt_Index, temp_MS_Word_1, temp_LS_Word_1);
        if ((temp_etl_MS_Word_1 !== temp_MS_Word_1) || (temp_etl_LS_Word_1 !== temp_LS_Word_1))
        begin
            error_Counter = error_Counter + 1;
            $display("DEMO ERROR (time %7t) : PE %d: ALT DW number %d: values are not equal", $time, inst_ID, alt_Index);
        end
    end

//    $display("DEMO: Scenario 3 of PE %d finished", inst_ID);
    demo_Process_Scenario_3 = 1'b0;
end

// Simple demo scenario 4
always @(posedge demo_Process_Scenario_4) 
begin
    $display("DEMO (time %7t) : Scenario 4 of PE %d started", $time, inst_ID);

    
    $display("DEMO (time %7t) : Scenario 4 of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_4 = 1'b0;
end

// Simple demo scenario 5
always @(posedge demo_Process_Scenario_5)
begin
    $display("DEMO (time %7t) : Scenario 5 of PE %d started", $time, inst_ID);


    $display("DEMO (time %7t) : Scenario 5 of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_5 = 1'b0;
end

// ---------------------------  SWITCH-BASED DEMO SCENARIOS  ----------------------------

    // Switch-based scenario data

    integer arg_Index_1S;
    reg [0 : 1] arg_Index_As_2_Bits_1S;
    reg [0 : 1] arg_Index_As_2_Bits_5S;
    integer arg_Index_2S;
    integer arg_Index_5S;
    integer gran_Index_1S;
    integer gran_Index_2S;
    integer gran_Index_5S;
    integer dw_Index_1S;
    integer dw_Index_2S;
    integer dw_Index_5S;

    integer num_Of_DW_In_Granule;

    reg [0 : `RIO_ADDRESS_WIDTH - 1] cur_GSM_Address;    
    reg [0 : `RIO_ADDRESS_WIDTH - 1] cur_IO_Address;    
    
    reg [0 : `RIO_BITS_IN_DW - 1]  granule_Arg_1[0 : `RIO_LARGE_GRAN_DW_SIZE - 1];
    reg [0 : `RIO_BITS_IN_DW - 1]  granule_Arg_2[0 : `RIO_LARGE_GRAN_DW_SIZE - 1];
    reg [0 : `RIO_BITS_IN_DW - 1]  granule_Arg_3[0 : `RIO_LARGE_GRAN_DW_SIZE - 1];
    reg [0 : `RIO_BITS_IN_DW - 1]  granule_Res[0 : `RIO_LARGE_GRAN_DW_SIZE - 1];

    //reg [0 : `RIO_BITS_IN_DW - 1]  granule_DW_1;
    //reg [0 : `RIO_BITS_IN_DW - 1]  granule_DW_2;
    //reg [0 : `RIO_BITS_IN_DW - 1]  granule_DW_3;
    reg [0 : `RIO_BITS_IN_DW - 1]  granule_DW;
    
    reg [0 : `RIO_BITS_IN_WORD - 1]  temp_MSW;
    reg [0 : `RIO_BITS_IN_WORD - 1]  temp_LSW;
    

    integer tmp_Trx_Tag_1S;
    integer tmp_Trx_Tag_2S;
    integer tmp_Trx_Tag_3S;
    integer tmp_Trx_Tag_4S;
    integer tmp_Trx_Tag_5S;

    integer gsm_Result_2;
    reg gsm_Hit_2;
    integer gsm_Cache_Index_2;

// Switch-based demo scenario 1
always @(posedge demo_Process_Scenario_1S)
begin
    $display("DEMO (time %7t) : Scenario 1S of PE %d started", $time, inst_ID);

    cur_IO_Address = `RIO_DEMO_DRIVE_IO_ADDRESS;
    num_Of_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;    

    for (arg_Index_1S = 1; arg_Index_1S <= (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1); arg_Index_1S = arg_Index_1S + 1)
    begin
        arg_Index_As_2_Bits_1S = (arg_Index_1S == (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1)) ? 0 : arg_Index_1S;
        
        for (gran_Index_1S = 0; gran_Index_1S < `RIO_DEMO_NUM_OF_GRANULES_IN_ARG; gran_Index_1S = gran_Index_1S + 1)
        begin
            for (dw_Index_1S = 0; dw_Index_1S < num_Of_DW_In_Granule; dw_Index_1S = dw_Index_1S + 1)
                $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, dw_Index_1S,
                    {(`RIO_BITS_IN_WORD / 2) {arg_Index_As_2_Bits_1S}},
                    {(`RIO_BITS_IN_WORD / 2) {arg_Index_As_2_Bits_1S}}
                    );

            tmp_Trx_Tag_1S = cur_IO_Trx_Tag; 
            while ($HW_Lg_IO_Request(inst_ID, `RIO_IO_NWRITE, 1 /* priority */,
                cur_IO_Address /* address */, 0 /* ext. address */, 0 /* xamsbs */,
                num_Of_DW_In_Granule /* dw_Size */,
                0 /* offset */, `RIO_BYTES_IN_DW /* byte_Num */, 
                tmp_Trx_Tag_1S /* trx_Tag */, `RIO_FALSE /* data_flag */
                ) == `RIO_RETRY)
                #(`RIO_WAIT_BEFORE_RETRY);     
            cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

            wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_1S));
            
            cur_IO_Address = cur_IO_Address + num_Of_DW_In_Granule * `RIO_BYTES_IN_DW;    
        end    
    end        
    
    $display("DEMO (time %7t) : Scenario 1S of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_1S = 1'b0;
end

// Switch-based demo scenario 2
always @(posedge demo_Process_Scenario_2S)
begin
    $display("DEMO (time %7t) : Scenario 2S of PE %d started", $time, inst_ID);

    num_Of_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;    
    
    for (gran_Index_2S = 0; gran_Index_2S < `RIO_DEMO_NUM_OF_GRANULES_IN_ARG; gran_Index_2S = gran_Index_2S + 1)
    begin
        for (arg_Index_2S = 1; arg_Index_2S <= (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1); arg_Index_2S = arg_Index_2S + 1)
        begin
            case (arg_Index_2S)
                1: cur_GSM_Address = `RIO_DEMO_ARG1_ADDRESS;
        
                2: cur_GSM_Address = `RIO_DEMO_ARG2_ADDRESS;

                3: cur_GSM_Address = `RIO_DEMO_ARG3_ADDRESS;

                4: cur_GSM_Address = `RIO_DEMO_RES_ADDRESS;
            endcase
            cur_GSM_Address = cur_GSM_Address + gran_Index_2S * num_Of_DW_In_Granule * `RIO_BYTES_IN_DW;
            
            tmp_Trx_Tag_2S = cur_GSM_Trx_Tag; 
            gsm_Result_2 = `RIO_RETRY;
            while (gsm_Result_2 == `RIO_RETRY)
            begin : RIO_GSM_CALC_READ_CYCLE_2S
                `RIO_CACHE_CONTROL.GSM_Request(
                    (arg_Index_2S == (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1)) ? `RIO_GSM_READ_TO_OWN : `RIO_GSM_READ_SHARED /*ttype*/,
                    (inst_ID == 1) ? 2 : 0 /*prio*/,
                    cur_GSM_Address /*address*/, 0 /*ext_Address*/, 0 /*xamsbs*/,
                    num_Of_DW_In_Granule /*dw_Size*/,
                    0 /*offset*/, 0 /*byte_Num*/,
                    `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
                    `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
                    tmp_Trx_Tag_2S /*trx_Tag*/,
                    gsm_Result_2 /*result*/, gsm_Hit_2 /*data_Read*/, gsm_Cache_Index_2 /*line_Index*/
                    );
                if (gsm_Result_2 == `RIO_ERROR) 
                    $display("DEMO ERROR (time %7t) : Scenario 2S of PE %d: GSM read error", $time, inst_ID);
                else if (gsm_Result_2 == `RIO_RETRY)        
                    #(`RIO_WAIT_BEFORE_RETRY);
                else
                begin
                    if (!gsm_Hit_2)
                    begin
                        $display("DEMO (time %7t) : Scenario 2S of PE %d: Cache miss, so GSM request was sent", $time, inst_ID);
                        wait ((pli_GSM_Req_Done_Flag_Mirror === 1'b1) && (pli_GSM_Trx_Tag_Mirror === tmp_Trx_Tag_2S));
                        $display("DEMO (time %7t) : Scenario 2S of PE %d: GSM request done", $time, inst_ID);
                        gsm_Cache_Index_2 = `RIO_CACHE_CONTROL.new_GSM_Cache_Line_Index_Reg;            
                        `RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg = 1'b0;
                    end    
                    if ({`RIO_CACHE_CONTROL.cache_M_Bits[gsm_Cache_Index_2],
                        `RIO_CACHE_CONTROL.cache_S_Bits[gsm_Cache_Index_2]} === 'b00) // line is already invalid
                    begin    
                        gsm_Result_2 = `RIO_RETRY;  // repeat request                                      
                        cur_GSM_Trx_Tag = cur_GSM_Trx_Tag + 1;
                        tmp_Trx_Tag_2S = cur_GSM_Trx_Tag; 
                        $display("DEMO (time %7t) : Scenario 2S of PE %d: Have to REPEAT GSM request", $time, inst_ID);
                    end    
                end    
            end  // End of cycle of GSM request repeats       

            cur_GSM_Trx_Tag = cur_GSM_Trx_Tag + 1;
            
            if (arg_Index_2S == (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1))
            begin
                // read cache line
                `RIO_CACHE_CONTROL.Get_Cache_Line(gsm_Cache_Index_2, `RIO_GENERAL_DATA);
                /* calculate result and copy it to the cache line */
                //$display("DEMO (time %7t) : Scenario 2S of PE %d: CALCULATION", $time, inst_ID);
                for (dw_Index_2S = 0; dw_Index_2S < num_Of_DW_In_Granule; dw_Index_2S = dw_Index_2S + 1)
                begin
                    $HW_Get_PLI_DW(inst_ID, `RIO_GENERAL_DATA, dw_Index_2S,
                        granule_DW[0 : (`RIO_BITS_IN_DW / 2) - 1],
                        granule_DW[`RIO_BITS_IN_DW / 2 : `RIO_BITS_IN_DW - 1]
                        );
                    //$display("DEMO (time %7t) : CALCULATION: OLD DW %d in granule %d: %b ",
                    //    $time, dw_Index_2S, gran_Index_2S, granule_DW);
                    if ((inst_ID - 1 == dw_Index_2S) || (`RIO_SMALL_GRAN_DW_SIZE + inst_ID - 1 == dw_Index_2S))
                    begin
                        case (inst_ID)
                            1: granule_DW = granule_Arg_1[dw_Index_2S];
                
                            3: granule_DW = granule_Arg_2[dw_Index_2S];

                            4: granule_DW = granule_Arg_3[dw_Index_2S];
                        
                            default: 
                                begin
                                    granule_DW = 0;
                                    $display("DEMO (time %7t) : Scenario 2S of PE %d: unexpected instance ID", $time, inst_ID);
                                end
                        endcase

                        $display("DEMO (time %7t) : PE %d: CALCULATION: Fill result DW %d in granule %d: %b",
                            $time, inst_ID, dw_Index_2S, gran_Index_2S, granule_DW);
                        
                        $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, dw_Index_2S,
                            granule_DW[0 : (`RIO_BITS_IN_DW / 2) - 1],
                            granule_DW[`RIO_BITS_IN_DW / 2 : `RIO_BITS_IN_DW - 1]
                            );
                    end    
                end
                // write cache line (some DW-s were changed, some are the same)
                `RIO_CACHE_CONTROL.Set_Cache_Line(gsm_Cache_Index_2, `RIO_GENERAL_DATA);
            end    
            else
            begin
                /* copy argument granule from the cache line to the temporary data structure*/
                `RIO_CACHE_CONTROL.Get_Cache_Line(gsm_Cache_Index_2, `RIO_GENERAL_DATA);
                for (dw_Index_2S = 0; dw_Index_2S < num_Of_DW_In_Granule; dw_Index_2S = dw_Index_2S + 1)
                begin
                    granule_DW = $HW_Get_PLI_DW_Value(inst_ID, `RIO_GENERAL_DATA, dw_Index_2S);
                    case (arg_Index_2S)
                        1: granule_Arg_1[dw_Index_2S] = granule_DW;
            
                        2: granule_Arg_2[dw_Index_2S] = granule_DW;

                        3: granule_Arg_3[dw_Index_2S] = granule_DW;
                    endcase
                end
            end    
        end
    end
    
    $display("DEMO (time %7t) : Scenario 2S of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_2S = 1'b0;
end

// Switch-based demo scenario 3
always @(posedge demo_Process_Scenario_3S)
begin
    $display("DEMO (time %7t) : Scenario 3S of PE %d started", $time, inst_ID);


    $display("DEMO (time %7t) : Scenario 3S of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_3S = 1'b0;
end

// Switch-based demo scenario 4
always @(posedge demo_Process_Scenario_4S)
begin
    $display("DEMO (time %7t) : Scenario 4S of PE %d started", $time, inst_ID);


    $display("DEMO (time %7t) : Scenario 4S of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_4S = 1'b0;
end

// Switch-based demo scenario 5
always @(posedge demo_Process_Scenario_5S)
begin
    $display("DEMO (time %7t) : Scenario 5S of PE %d started", $time, inst_ID);

    cur_IO_Address = `RIO_DEMO_DRIVE_IO_ADDRESS;
    num_Of_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;    

    for (arg_Index_5S = 1; arg_Index_5S <= (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1); arg_Index_5S = arg_Index_5S + 1)
    begin
        for (gran_Index_5S = 0; gran_Index_5S < `RIO_DEMO_NUM_OF_GRANULES_IN_ARG; gran_Index_5S = gran_Index_5S + 1)
        begin
            tmp_Trx_Tag_5S = cur_IO_Trx_Tag; 
            while ($HW_Lg_IO_Request(inst_ID, `RIO_IO_NREAD, 1 /* priority */,
                cur_IO_Address /* address */, 0 /* ext. address */, 0 /* xamsbs */,
                num_Of_DW_In_Granule /* dw_Size */,
                0 /* offset */, `RIO_BYTES_IN_DW /* byte_Num */, 
                tmp_Trx_Tag_5S /* trx_Tag */, `RIO_FALSE /* data_flag */
                ) == `RIO_RETRY)
                #(`RIO_WAIT_BEFORE_RETRY);     
            cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

            wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_5S));

            for (dw_Index_5S = 0; dw_Index_5S < num_Of_DW_In_Granule; dw_Index_5S = dw_Index_5S + 1)
            begin
                if (arg_Index_5S == (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1))
                begin
                    arg_Index_As_2_Bits_5S = 0;
                    case (dw_Index_5S)
                        0: arg_Index_As_2_Bits_5S = 1;
            
                        2: arg_Index_As_2_Bits_5S = 2;

                        3: arg_Index_As_2_Bits_5S = 3;

                        4: arg_Index_As_2_Bits_5S = 1;
            
                        6: arg_Index_As_2_Bits_5S = 2;

                        7: arg_Index_As_2_Bits_5S = 3;
                    endcase
                end
                else    
                    arg_Index_As_2_Bits_5S = arg_Index_5S;
                    
                $HW_Get_PLI_DW(inst_ID, `RIO_IO_DATA, dw_Index_5S, temp_MSW, temp_LSW);

                if (arg_Index_5S == (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1))
                    $display("DEMO: PE %d: DRIVE SPACE: RESULT: GRANULE %d: DW %d: MSW: %b LSW: %b",
                        inst_ID, gran_Index_5S, dw_Index_5S, temp_MSW, temp_LSW);
                else        
                    $display("DEMO: PE %d: DRIVE SPACE: ARG %2d: GRANULE %d: DW %d: MSW: %b LSW: %b",
                        inst_ID, arg_Index_5S, gran_Index_5S, dw_Index_5S, temp_MSW, temp_LSW);

                if ({(`RIO_BITS_IN_DW / 2) {arg_Index_As_2_Bits_5S}} !== {temp_MSW, temp_LSW})
                begin
                    if (arg_Index_5S == (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1))
                        $display("DEMO: PE %d: ERROR: DRIVE SPACE: RESULT: GRANULE %d: DW %d: VALUE SHALL BE: MSW: %b LSW: %b",
                            inst_ID, gran_Index_5S, dw_Index_5S,
                            {(`RIO_BITS_IN_WORD / 2) {arg_Index_As_2_Bits_5S}},
                            {(`RIO_BITS_IN_WORD / 2) {arg_Index_As_2_Bits_5S}});
                    else
                        $display("DEMO: PE %d: ERROR: DRIVE SPACE: ARG %2d: GRANULE %d: DW %d: VALUE SHALL BE: MSW: %b LSW: %b",
                            inst_ID, arg_Index_5S, gran_Index_5S, dw_Index_5S,
                            {(`RIO_BITS_IN_WORD / 2) {arg_Index_As_2_Bits_5S}},
                            {(`RIO_BITS_IN_WORD / 2) {arg_Index_As_2_Bits_5S}});
                    error_Counter = error_Counter + 1;
                end
                else    
                    if (arg_Index_5S == (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1))
                        $display("DEMO: PE %d: DRIVE SPACE: RESULT: GRANULE %d: DW %d : OK, VALUE IS CORRECT!",
                            inst_ID, gran_Index_5S, dw_Index_5S);
                    else        
                        $display("DEMO: PE %d: DRIVE SPACE: ARG %2d: GRANULE %d: DW %d : OK, VALUE IS CORRECT!",
                            inst_ID, arg_Index_5S, gran_Index_5S, dw_Index_5S);
            end        
            
            cur_IO_Address = cur_IO_Address + num_Of_DW_In_Granule * `RIO_BYTES_IN_DW;    
        end    
    end        

    $display("DEMO (time %7t) : Scenario 5S of PE %d finished", $time, inst_ID);
    demo_Process_Scenario_5S = 1'b0;
end

//----------  MESSAGES WAITER  ---------------

always
begin
    wait (demo_Process_Scenario_2 === 1'b1);
    wait (last_Mailbox_Received_Letters !== `RIO_MAILBOX_CONTROL.mailbox_Received_Letters);
    $display("DEMO (time %7t) : PE %d: Message obtained, contains data to be used for ", $time, inst_ID);
    $display("      initialization of memory area wich will be read during DMA transfer");

//    mem_Index = 0;

    // just_Arrived_Letters: if bit == "1" then appropriate letter slot contains NEW received message
    just_Arrived_Letters = (`RIO_MAILBOX_CONTROL.mailbox_Received_Letters ^
        last_Mailbox_Received_Letters) & `RIO_MAILBOX_CONTROL.mailbox_Received_Letters;
    // because all letter slots with "NEW" messages will be cleared till the next cycle:  
    last_Mailbox_Received_Letters = ~(just_Arrived_Letters) & `RIO_MAILBOX_CONTROL.mailbox_Received_Letters;

    for (letter_Index = 0; letter_Index < `RIO_MB_MAX_LETTERS_IN_MB * `RIO_MB_MAX_TOTAL_MB; letter_Index = letter_Index + 1)
    begin
        if (just_Arrived_Letters[letter_Index] === 1'b1)
        begin  // read the message and free the letter slot
            $display(" Reading letter %d", letter_Index);
            mb_Num = letter_Index / `RIO_MB_LETTERS_IN_MB;
            letter_Num = letter_Index - (mb_Num * `RIO_MB_LETTERS_IN_MB);   
            msg_DW_Size = `RIO_MAILBOX_CONTROL.Get_Msg_DW_Size(mb_Num, letter_Num);
            $display("DEMO: MSG DW SIZE: %d", msg_DW_Size);
            msg_DW_Offset = `RIO_MAILBOX_CONTROL.Get_Msg_Letter_DW_Offset(mb_Num, letter_Num);

            msg_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;
            msg_Granules_In_Msg = msg_DW_Size / msg_DW_In_Granule;
            msg_Src_Address_Offset = `RIO_DEMO_DMA_SRC_ADDRESS - `RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address;

            if ((msg_Granules_In_Msg * msg_DW_In_Granule == msg_DW_Size) &&
                (msg_Src_Address_Offset[`RIO_BITS_IN_WORD - (5 + `RIO_COH_GRANULE_SIZE_IS_64) : `RIO_BITS_IN_WORD - 1] ===
                {(5 + `RIO_COH_GRANULE_SIZE_IS_64) {1'b0}})
                )  // if msg_DW_Size = integer number of coherency granules and 
            begin  // address is granule-aligned
                $display("DEMO: COPYING MSG DATA TO CACHE (%d granules)", msg_Granules_In_Msg);
                for (gran_Index = 0; gran_Index < msg_Granules_In_Msg; gran_Index = gran_Index + 1)
                begin
                    // prepare free cache line
                    `RIO_CACHE_CONTROL.Prepare_Cache_Line(
                        `RIO_DEMO_DMA_SRC_ADDRESS +
                        gran_Index * msg_DW_In_Granule * `RIO_BYTES_IN_DW -
                        `RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address /*address*/,
                        0 /*extended_Address*/, 0 /*xamsbs*/,
                        `RIO_FALSE /*is_GSM*/,
                        io_Cache_Index /*line_Index*/
                        );

                    // prepare data copy for storing in cache
                    for (dw_Index = 0; dw_Index < msg_DW_In_Granule; dw_Index = dw_Index + 1)
                    begin
                        temp_MS_Word = `RIO_MAILBOX_CONTROL.Get_Mailbox_Mem_MS_Word(
                            msg_DW_Offset +
                            gran_Index * msg_DW_In_Granule +
                            dw_Index);
                        temp_LS_Word = `RIO_MAILBOX_CONTROL.Get_Mailbox_Mem_LS_Word(
                            msg_DW_Offset +
                            gran_Index * msg_DW_In_Granule +
                            dw_Index);
                        $HW_Set_PLI_DW(
                           inst_ID,
                           `RIO_GENERAL_DATA,
                           dw_Index,
                           temp_MS_Word, temp_LS_Word
                           );
                    end

                    // fill the prepared cache line
                    `RIO_CACHE_CONTROL.Set_Cache_Line(io_Cache_Index, `RIO_GENERAL_DATA);
                
                    `RIO_CACHE_CONTROL.cache_GSM_Bits[io_Cache_Index] = 1'b0; 
                    `RIO_CACHE_CONTROL.cache_M_Bits[io_Cache_Index] = 1'b0;
                    `RIO_CACHE_CONTROL.cache_S_Bits[io_Cache_Index] = 1'b1;
                end        
            end    
            else
            begin
                for (dw_Index = 0; dw_Index < msg_DW_Size; dw_Index = dw_Index + 1)
                begin
                    temp_MS_Word = `RIO_MAILBOX_CONTROL.Get_Mailbox_Mem_MS_Word(msg_DW_Offset + dw_Index);
                    temp_LS_Word = `RIO_MAILBOX_CONTROL.Get_Mailbox_Mem_LS_Word(msg_DW_Offset + dw_Index);
                    
                    $display("DEMO (time %7t) : PE %d: MSG DW OBTAINED: MB %d, LTR: %d, DW: %d, MSW: h%h LSW: h%h",
                        $time, inst_ID, mb_Num, letter_Num, dw_Index, temp_MS_Word, temp_LS_Word);

                    tmp_Trx_Tag_2 = cur_IO_Trx_Tag; 
                    temp_Res = $HW_Lg_IO_Request(inst_ID /*handle*/, `RIO_IO_NWRITE /*ttype*/, 2 /*prio*/,
                        `RIO_DEMO_DMA_SRC_ADDRESS + dw_Index * `RIO_BYTES_IN_DW /*address*/,
                        0 /*extended_Address*/, 0 /*xamsbs*/,
                        1 /*dw_Size*/, 0 /*offset*/, 8 /*byte_Num*/,
                        tmp_Trx_Tag_2 /*trx_Tag*/, `RIO_TRUE /*data_flag*/,
                        temp_MS_Word, temp_LS_Word
                        );
                    cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;
                    wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_2));
                    pli_IO_Req_Done_Flag_Reg = 1'b0;
                    //mem_Index = mem_Index + 1;
                end
            end
            `RIO_MAILBOX_CONTROL.Free_Msg_Letter(mb_Num, letter_Num);
        end
    end
    demo_Process_Scenario_2 = 1'b0;
end

//----------  DOORBELLS WAITER  ---------------

always
begin
    wait (demo_Process_Scenario_1);
    wait (`RIO_MAILBOX_CONTROL.doorbell_Full === 1'b1);
    temp_DB_Info = `RIO_MAILBOX_CONTROL.doorbell_Info;
//    $display("DEMO: PE %d: DOORBELL OBTAINED: DB INFO: h%h", inst_ID, temp_DB_Info);

    // PLACE FOR MEMORY REQUEST TO THE SPECIAL AREA IN THE HOME MEMORY

    `RIO_MAILBOX_CONTROL.Free_Doorbell;
end


endmodule

/*****************************************************************************/
