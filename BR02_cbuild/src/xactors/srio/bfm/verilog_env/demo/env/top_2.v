/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/top_2.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*                information.
*               
* Description:  Top module - variant 2 - for RIO Models demo application
*            
* Notes:        
*
******************************************************************************/

`timescale 1ns/1ns
`include "env_defines.vh"
`include "demo_defines.vh"

module `RIO_TOP_2_MODULE;

// registers and wires

    // clock generator

    reg    sys_CLK; 

    // connection wires 

    wire tclk1;
    wire tclk2;
    wire tclk3;
    wire tclk4;
    wire tclk5;
    wire tclk6;

    wire tframe1;
    wire tframe2;
    wire tframe3;
    wire tframe4;
    wire tframe5;
    wire tframe6;
    
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td1;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td2;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td3;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td4;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td5;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td6;

    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl1;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl2;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl3;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl4;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl5;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl6;

    wire rclk1;    
    wire rclk2;    
    wire rclk3;    
    wire rclk4;    
    wire rclk5;    
    wire rclk6;    

    wire rframe1;
    wire rframe2;
    wire rframe3;
    wire rframe4;
    wire rframe5;
    wire rframe6;

    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd1;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd2;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd3;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd4;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd5;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd6;

    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl1;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl2;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl3;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl4;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl5;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl6;
    
    wire [0 : `RIO_NUM_OF_SWITCH_LINKS - 1] nothing;
    
// Processing elements    

    `RIO_PE_MODULE #(1, 3'b111) `RIO_PE_1    
        (
        .clock(sys_CLK),
        .tclk(tclk1), 
        .tframe(tframe1),
        .td(td1), 
        .tdl(tdl1), 
        .rclk(rclk1), 
        .rframe(rframe1), 
        .rd(rd1), 
        .rdl(rdl1)
        );

    defparam `RIO_PE_1.`RIO_CACHE_CONTROL.coh_Granule_Is_64 = `RIO_COH_GRANULE_SIZE_IS_64;
    defparam `RIO_PE_1.`RIO_MAILBOX_CONTROL.hw_Available = 5'b1111_1;  // Mailboxes 0, 1, 2, 3 and doorbell are on

    `RIO_DMA_PE_MODULE #(2, 3'b111) `RIO_DMA_PE    
        (
        .clock(sys_CLK),
        .tclk(tclk2), 
        .tframe(tframe2),
        .td(td2), 
        .tdl(tdl2), 
        .rclk(rclk2), 
        .rframe(rframe2), 
        .rd(rd2), 
        .rdl(rdl2)
        );

    defparam `RIO_DMA_PE.`RIO_CACHE_CONTROL.coh_Granule_Is_64 = `RIO_COH_GRANULE_SIZE_IS_64;
    defparam `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.hw_Available = 5'b1111_1;  // Mailboxes 0, 1, 2, 3 and doorbell are on

    `RIO_PE_MODULE #(3, 3'b111) `RIO_PE_3    
        (
        .clock(sys_CLK),
        .tclk(tclk3), 
        .tframe(tframe3),
        .td(td3), 
        .tdl(tdl3), 
        .rclk(rclk3), 
        .rframe(rframe3), 
        .rd(rd3), 
        .rdl(rdl3)
        );

    defparam `RIO_PE_3.`RIO_CACHE_CONTROL.coh_Granule_Is_64 = `RIO_COH_GRANULE_SIZE_IS_64;
    defparam `RIO_PE_3.`RIO_MAILBOX_CONTROL.hw_Available = 5'b1111_1;  // Mailboxes 0, 1, 2, 3 and doorbell are on
    
    `RIO_PE_MODULE #(4, 3'b111) `RIO_PE_4    
        (
        .clock(sys_CLK),
        .tclk(tclk4), 
        .tframe(tframe4),
        .td(td4), 
        .tdl(tdl4), 
        .rclk(rclk4), 
        .rframe(rframe4), 
        .rd(rd4), 
        .rdl(rdl4)
        );

    defparam `RIO_PE_4.`RIO_CACHE_CONTROL.coh_Granule_Is_64 = `RIO_COH_GRANULE_SIZE_IS_64;
    defparam `RIO_PE_4.`RIO_MAILBOX_CONTROL.hw_Available = 5'b1111_1;  // Mailboxes 0, 1, 2, 3 and doorbell are on

    `RIO_PE_MODULE #(5, 3'b111) `RIO_PE_5    
        (
        .clock(sys_CLK),
        .tclk(tclk5), 
        .tframe(tframe5),
        .td(td5), 
        .tdl(tdl5), 
        .rclk(rclk5), 
        .rframe(rframe5), 
        .rd(rd5), 
        .rdl(rdl5)
        );

    defparam `RIO_PE_5.`RIO_CACHE_CONTROL.coh_Granule_Is_64 = `RIO_COH_GRANULE_SIZE_IS_64;
    defparam `RIO_PE_5.`RIO_MAILBOX_CONTROL.hw_Available = 5'b1111_1;  // Mailboxes 0, 1, 2, 3 and doorbell are on
    
// Switches

    `RIO_SWITCH_MODULE #(1, 7) `RIO_SWITCH_1    
        (
        .clock(sys_CLK),
        .tclk({nothing[0], rclk1, nothing[2], rclk3, nothing[4], rclk5, tclk6, nothing[7 : 15]}), 
        .tframe({nothing[0], rframe1, nothing[2], rframe3, nothing[4], rframe5, tframe6, nothing[7 : 15]}), 
        .rclk({1'b0, tclk1, 1'b0, tclk3, 1'b0, tclk5, rclk6, 9'b0}), 
        .rframe({1'b0, tframe1, 1'b0, tframe3, 1'b0, tframe5, rframe6, 9'b0}), 
        .td1(rd1), 
        .tdl1(rdl1), 
        .rd1(td1), 
        .rdl1(tdl1),
        .td3(rd3), 
        .tdl3(rdl3), 
        .rd3(td3), 
        .rdl3(tdl3),
        .td5(rd5), 
        .tdl5(rdl5), 
        .rd5(td5), 
        .rdl5(tdl5),
        .td6(td6), 
        .tdl6(tdl6), 
        .rd6(rd6), 
        .rdl6(rdl6)
        );

    `RIO_SWITCH_MODULE #(2, 7) `RIO_SWITCH_2    
        (
        .clock(sys_CLK),
        .tclk({nothing[0], nothing[1], rclk2, nothing[3], rclk4, nothing[5], rclk6, nothing[7 : 15]}), 
        .tframe({nothing[0], nothing[1], rframe2, nothing[3], rframe4, nothing[5], rframe6, nothing[7 : 15]}), 
        .rclk({1'b0, 1'b0, tclk2, 1'b0, tclk4, 1'b0, tclk6, 9'b0}), 
        .rframe({1'b0, 1'b0, tframe2, 1'b0, tframe4, 1'b0, tframe6, 9'b0}), 
        .td2(rd2), 
        .tdl2(rdl2), 
        .rd2(td2), 
        .rdl2(tdl2),
        .td4(rd4), 
        .tdl4(rdl4), 
        .rd4(td4), 
        .rdl4(tdl4),
        .td6(rd6), 
        .tdl6(rdl6), 
        .rd6(td6), 
        .rdl6(tdl6)
        );
        
// --------------------------------------------    
    
// Use this register instead of non-implemented registers to check access to them 
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_EMPTY_Reg;

// Loop counter
    integer i;
  
/* ********************************************************************* */

// clock and reset control
initial 
begin
    sys_CLK = 0;
    #(`RIO_EMERGENCY_TIME_SWITCH) $finish;   // emergency finish to avoid infinite simulation if any errors
end

// clock generator
always 
begin 
    #(`RIO_HALF_CLOCK_PERIOD) sys_CLK = ~sys_CLK;
end

// -------------------------------------------------------------

// initialization
initial pli_EMPTY_Reg = 0;

// "EMPTY" register access monitor
always
begin
    wait (pli_EMPTY_Reg !== 0);
    pli_EMPTY_Reg = 0;
    $display("DEMO WARNING (time %7t) : Attempt to write into non-implemented register detected.", $time);
end

// -------------------------------------------------------------


// registers initialization
initial
begin 
//    $display("1");
// Initialize PLI wrapper

    $HW_Init(5 /* Total number of PE */ , 2 /* Total number of switches */);

//    $display("2");

    // PEs REGISTRATION        

    $HW_Register_Instance(
    1,
    `RIO_PE_1,
    /* parameters: */
    `RIO_ADDRESS_SUPPORT_CODE, /*ext_Address_Support code*/
    `RIO_FALSE, /*is_Bridge*/
    `RIO_TRUE, /*has_Memory*/
    `RIO_TRUE, /*has_Processor*/
    ~`RIO_COH_GRANULE_SIZE_IS_64, /*coh_Granule_Size_32*/
    `RIO_TRX_ALL, /*source_Trx*/
    `RIO_TRX_ALL, /*dest_Trx*/ 
    `RIO_TRUE, /*mbox1*/
    `RIO_TRUE, /*mbox2*/
    `RIO_TRUE, /*mbox3*/
    `RIO_TRUE, /*mbox4*/
    `RIO_TRUE, /*has_Doorbell*/
    `RIO_IN_BUF_SIZE, /*pl_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*pl_Outbound_Buffer_Size*/
    `RIO_IN_BUF_SIZE, /*ll_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*ll_Outbound_Buffer_Size*/
    4 /*coh_Domain_Size*/,

/*---------------------------------------------------------*/
/*New 10 parameters*/
    0, //device_Identity
    0, //device_Vendor_Identity
    0, //device_Rev
    0, //device_Minor_Rev
    0, //device_Major_Rev
    0, //assy_Identity
    0, //assy_Vendor_Identity
    0, //assy_Rev
	'h100, //entry_Extended_Features_Ptr /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	'h1000, //next_Extended_Features_Ptr /*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
/*End of new 10 parameters*/
/*---------------------------------------------------------*/

    /* PLI registers: */
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Result_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Loc_Response_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg,
    `RIO_PE_1.pli_IO_Trx_Tag_Reg, 
    `RIO_PE_1.pli_IO_Result_Reg,
    `RIO_PE_1.pli_IO_Err_Code_Reg,
    `RIO_PE_1.pli_IO_Req_Done_Flag_Reg,
    `RIO_PE_1.pli_MP_Trx_Tag_Reg, 
    `RIO_PE_1.pli_MP_Result_Reg,
    `RIO_PE_1.pli_MP_Err_Code_Reg,
    `RIO_PE_1.pli_MP_Req_Done_Flag_Reg,
    `RIO_PE_1.pli_DB_Trx_Tag_Reg, 
    `RIO_PE_1.pli_DB_Result_Reg,
    `RIO_PE_1.pli_DB_Err_Code_Reg,
    `RIO_PE_1.pli_DB_Req_Done_Flag_Reg,
    `RIO_PE_1.pli_Config_Trx_Tag_Reg, 
    `RIO_PE_1.pli_Config_Result_Reg,
    `RIO_PE_1.pli_Config_Err_Code_Reg,
    `RIO_PE_1.pli_Config_Req_Done_Flag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_Address_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_Ext_Address_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_XAMSBS_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_LTType_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_TType_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_Req_Flag_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_MSGLEN_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_SSIZE_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_LETTER_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_MBOX_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_MSGSEG_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_DW_Size_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_Flag_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_DB_Info_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Address_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Ext_Address_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_XAMSBS_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_DW_Size_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Type_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Is_GSM_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_BE_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Flag_Reg,
    `RIO_PE_1.pli_Pins_TFrame_Reg,
    `RIO_PE_1.pli_Pins_TD_Reg,
    `RIO_PE_1.pli_Pins_TDL_Reg,
    `RIO_PE_1.pli_Pins_TCLK_Reg,
    `RIO_PE_1.pli_Pins_Flag_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.mailbox_CSR,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.doorbell_CSR,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_XAMSBS,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_Ext_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_DW_Size,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_XAMSBS,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_Ext_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_Start_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_DW_Size,
    pli_EMPTY_Reg /* dma_MSR */,
    pli_EMPTY_Reg /* dma_SAR */,
    pli_EMPTY_Reg /* dma_DAR */,
    pli_EMPTY_Reg /* dma_BCR */,
    pli_EMPTY_Reg /* dma_src_Inst_ID */
    );

    //$display("3");

    $HW_Register_Instance(
    2,
    `RIO_DMA_PE,
    /* parameters: */
    `RIO_ADDRESS_SUPPORT_CODE, /*ext_Address_Support code*/
    `RIO_FALSE, /*is_Bridge*/
    `RIO_TRUE, /*has_Memory*/
    `RIO_TRUE, /*has_Processor*/
    ~`RIO_COH_GRANULE_SIZE_IS_64, /*coh_Granule_Size_32*/
    `RIO_TRX_ALL, /*source_Trx*/
    `RIO_TRX_ALL, /*dest_Trx*/ 
    `RIO_TRUE, /*mbox1*/
    `RIO_TRUE, /*mbox2*/
    `RIO_TRUE, /*mbox3*/
    `RIO_TRUE, /*mbox4*/
    `RIO_TRUE, /*has_Doorbell*/
    `RIO_IN_BUF_SIZE, /*pl_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*pl_Outbound_Buffer_Size*/
    `RIO_IN_BUF_SIZE, /*ll_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*ll_Outbound_Buffer_Size*/
    4 /*coh_Domain_Size*/,

/*---------------------------------------------------------*/
/*New 10 parameters*/
    0, //device_Identity
    0, //device_Vendor_Identity
    0, //device_Rev
    0, //device_Minor_Rev
    0, //device_Major_Rev
    0, //assy_Identity
    0, //assy_Vendor_Identity
    0, //assy_Rev
	'h100, //entry_Extended_Features_Ptr /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	'h1000, //next_Extended_Features_Ptr /*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
/*End of new 10 parameters*/
/*---------------------------------------------------------*/


    /* PLI registers: */
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Result_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Loc_Response_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg,
    `RIO_DMA_PE.pli_IO_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_IO_Result_Reg,
    `RIO_DMA_PE.pli_IO_Err_Code_Reg,
    `RIO_DMA_PE.pli_IO_Req_Done_Flag_Reg,
    `RIO_DMA_PE.pli_MP_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_MP_Result_Reg,
    `RIO_DMA_PE.pli_MP_Err_Code_Reg,
    `RIO_DMA_PE.pli_MP_Req_Done_Flag_Reg,
    `RIO_DMA_PE.pli_DB_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_DB_Result_Reg,
    `RIO_DMA_PE.pli_DB_Err_Code_Reg,
    `RIO_DMA_PE.pli_DB_Req_Done_Flag_Reg,
    `RIO_DMA_PE.pli_Config_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_Config_Result_Reg,
    `RIO_DMA_PE.pli_Config_Err_Code_Reg,
    `RIO_DMA_PE.pli_Config_Req_Done_Flag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_Address_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_Ext_Address_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_XAMSBS_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_LTType_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_TType_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_Req_Flag_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_MSGLEN_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_SSIZE_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_LETTER_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_MBOX_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_MSGSEG_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_DW_Size_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_Flag_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_DB_Info_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Address_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Ext_Address_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_XAMSBS_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_DW_Size_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Type_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Is_GSM_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_BE_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Flag_Reg,
    `RIO_DMA_PE.pli_Pins_TFrame_Reg,
    `RIO_DMA_PE.pli_Pins_TD_Reg,
    `RIO_DMA_PE.pli_Pins_TDL_Reg,
    `RIO_DMA_PE.pli_Pins_TCLK_Reg,
    `RIO_DMA_PE.pli_Pins_Flag_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.mailbox_CSR,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.doorbell_CSR,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_XAMSBS,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_Ext_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_DW_Size,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_XAMSBS,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_Ext_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_Start_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_DW_Size,
    `RIO_DMA_PE.msr,
    `RIO_DMA_PE.sar,
    `RIO_DMA_PE.dar,
    `RIO_DMA_PE.bcr,
    `RIO_DMA_PE.src_Inst_ID
    );

//    $display("4");
    
    $HW_Register_Instance(
    3,
    `RIO_PE_3,
    /* parameters: */
    `RIO_ADDRESS_SUPPORT_CODE, /*ext_Address_Support code*/
    `RIO_FALSE, /*is_Bridge*/
    `RIO_TRUE, /*has_Memory*/
    `RIO_TRUE, /*has_Processor*/
    ~`RIO_COH_GRANULE_SIZE_IS_64, /*coh_Granule_Size_32*/
    `RIO_TRX_ALL, /*source_Trx*/
    `RIO_TRX_ALL, /*dest_Trx*/ 
    `RIO_TRUE, /*mbox1*/
    `RIO_TRUE, /*mbox2*/
    `RIO_TRUE, /*mbox3*/
    `RIO_TRUE, /*mbox4*/
    `RIO_TRUE, /*has_Doorbell*/
    `RIO_IN_BUF_SIZE, /*pl_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*pl_Outbound_Buffer_Size*/
    `RIO_IN_BUF_SIZE, /*ll_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*ll_Outbound_Buffer_Size*/
    4 /*coh_Domain_Size*/,

/*---------------------------------------------------------*/
/*New 10 parameters*/
    0, //device_Identity
    0, //device_Vendor_Identity
    0, //device_Rev
    0, //device_Minor_Rev
    0, //device_Major_Rev
    0, //assy_Identity
    0, //assy_Vendor_Identity
    0, //assy_Rev
	'h100, //entry_Extended_Features_Ptr /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	'h1000, //next_Extended_Features_Ptr /*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
/*End of new 10 parameters*/
/*---------------------------------------------------------*/


    /* PLI registers: */
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_GSM_Result_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Loc_Response_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg,
    `RIO_PE_3.pli_IO_Trx_Tag_Reg, 
    `RIO_PE_3.pli_IO_Result_Reg,
    `RIO_PE_3.pli_IO_Err_Code_Reg,
    `RIO_PE_3.pli_IO_Req_Done_Flag_Reg,
    `RIO_PE_3.pli_MP_Trx_Tag_Reg, 
    `RIO_PE_3.pli_MP_Result_Reg,
    `RIO_PE_3.pli_MP_Err_Code_Reg,
    `RIO_PE_3.pli_MP_Req_Done_Flag_Reg,
    `RIO_PE_3.pli_DB_Trx_Tag_Reg, 
    `RIO_PE_3.pli_DB_Result_Reg,
    `RIO_PE_3.pli_DB_Err_Code_Reg,
    `RIO_PE_3.pli_DB_Req_Done_Flag_Reg,
    `RIO_PE_3.pli_Config_Trx_Tag_Reg, 
    `RIO_PE_3.pli_Config_Result_Reg,
    `RIO_PE_3.pli_Config_Err_Code_Reg,
    `RIO_PE_3.pli_Config_Req_Done_Flag_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Snoop_Address_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Snoop_Ext_Address_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Snoop_XAMSBS_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Snoop_LTType_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Snoop_TType_Reg,
    `RIO_PE_3.`RIO_CACHE_CONTROL.pli_Snoop_Req_Flag_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_MP_MSGLEN_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_MP_SSIZE_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_MP_LETTER_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_MP_MBOX_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_MP_MSGSEG_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_MP_DW_Size_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_MP_Flag_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_DB_Info_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_Address_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_Ext_Address_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_XAMSBS_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_DW_Size_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_Type_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_Is_GSM_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_BE_Reg,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.pli_Mem_Flag_Reg,
    `RIO_PE_3.pli_Pins_TFrame_Reg,
    `RIO_PE_3.pli_Pins_TD_Reg,
    `RIO_PE_3.pli_Pins_TDL_Reg,
    `RIO_PE_3.pli_Pins_TCLK_Reg,
    `RIO_PE_3.pli_Pins_Flag_Reg,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.mailbox_CSR,
    `RIO_PE_3.`RIO_MAILBOX_CONTROL.doorbell_CSR,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_IO_Space_XAMSBS,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_IO_Space_Ext_Address,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_IO_Space_DW_Size,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_GSM_Space_XAMSBS,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_GSM_Space_Ext_Address,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_GSM_Space_Start_Address,
    `RIO_PE_3.`RIO_MEMORY_CONTROL.mem_GSM_Space_DW_Size,
    pli_EMPTY_Reg /* dma_MSR */,
    pli_EMPTY_Reg /* dma_SAR */,
    pli_EMPTY_Reg /* dma_DAR */,
    pli_EMPTY_Reg /* dma_BCR */,
    pli_EMPTY_Reg /* dma_src_Inst_ID */
    );

//    $display("5");

    $HW_Register_Instance(
    4,
    `RIO_PE_4,
    /* parameters: */
    `RIO_ADDRESS_SUPPORT_CODE, /*ext_Address_Support code*/
    `RIO_FALSE, /*is_Bridge*/
    `RIO_TRUE, /*has_Memory*/
    `RIO_TRUE, /*has_Processor*/
    ~`RIO_COH_GRANULE_SIZE_IS_64, /*coh_Granule_Size_32*/
    `RIO_TRX_ALL, /*source_Trx*/
    `RIO_TRX_ALL, /*dest_Trx*/ 
    `RIO_TRUE, /*mbox1*/
    `RIO_TRUE, /*mbox2*/
    `RIO_TRUE, /*mbox3*/
    `RIO_TRUE, /*mbox4*/
    `RIO_TRUE, /*has_Doorbell*/
    `RIO_IN_BUF_SIZE, /*pl_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*pl_Outbound_Buffer_Size*/
    `RIO_IN_BUF_SIZE, /*ll_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*ll_Outbound_Buffer_Size*/
    4 /*coh_Domain_Size*/,

/*---------------------------------------------------------*/
/*New 10 parameters*/
    0, //device_Identity
    0, //device_Vendor_Identity
    0, //device_Rev
    0, //device_Minor_Rev
    0, //device_Major_Rev
    0, //assy_Identity
    0, //assy_Vendor_Identity
    0, //assy_Rev
	'h100, //entry_Extended_Features_Ptr /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	'h1000, //next_Extended_Features_Ptr /*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
/*End of new 10 parameters*/
/*---------------------------------------------------------*/


    /* PLI registers: */
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_GSM_Result_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Loc_Response_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg,
    `RIO_PE_4.pli_IO_Trx_Tag_Reg, 
    `RIO_PE_4.pli_IO_Result_Reg,
    `RIO_PE_4.pli_IO_Err_Code_Reg,
    `RIO_PE_4.pli_IO_Req_Done_Flag_Reg,
    `RIO_PE_4.pli_MP_Trx_Tag_Reg, 
    `RIO_PE_4.pli_MP_Result_Reg,
    `RIO_PE_4.pli_MP_Err_Code_Reg,
    `RIO_PE_4.pli_MP_Req_Done_Flag_Reg,
    `RIO_PE_4.pli_DB_Trx_Tag_Reg, 
    `RIO_PE_4.pli_DB_Result_Reg,
    `RIO_PE_4.pli_DB_Err_Code_Reg,
    `RIO_PE_4.pli_DB_Req_Done_Flag_Reg,
    `RIO_PE_4.pli_Config_Trx_Tag_Reg, 
    `RIO_PE_4.pli_Config_Result_Reg,
    `RIO_PE_4.pli_Config_Err_Code_Reg,
    `RIO_PE_4.pli_Config_Req_Done_Flag_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Snoop_Address_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Snoop_Ext_Address_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Snoop_XAMSBS_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Snoop_LTType_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Snoop_TType_Reg,
    `RIO_PE_4.`RIO_CACHE_CONTROL.pli_Snoop_Req_Flag_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_MP_MSGLEN_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_MP_SSIZE_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_MP_LETTER_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_MP_MBOX_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_MP_MSGSEG_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_MP_DW_Size_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_MP_Flag_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_DB_Info_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_Address_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_Ext_Address_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_XAMSBS_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_DW_Size_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_Type_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_Is_GSM_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_BE_Reg,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.pli_Mem_Flag_Reg,
    `RIO_PE_4.pli_Pins_TFrame_Reg,
    `RIO_PE_4.pli_Pins_TD_Reg,
    `RIO_PE_4.pli_Pins_TDL_Reg,
    `RIO_PE_4.pli_Pins_TCLK_Reg,
    `RIO_PE_4.pli_Pins_Flag_Reg,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.mailbox_CSR,
    `RIO_PE_4.`RIO_MAILBOX_CONTROL.doorbell_CSR,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_IO_Space_XAMSBS,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_IO_Space_Ext_Address,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_IO_Space_DW_Size,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_GSM_Space_XAMSBS,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_GSM_Space_Ext_Address,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_GSM_Space_Start_Address,
    `RIO_PE_4.`RIO_MEMORY_CONTROL.mem_GSM_Space_DW_Size,
    pli_EMPTY_Reg /* dma_MSR */,
    pli_EMPTY_Reg /* dma_SAR */,
    pli_EMPTY_Reg /* dma_DAR */,
    pli_EMPTY_Reg /* dma_BCR */,
    pli_EMPTY_Reg /* dma_src_Inst_ID */
    );

//    $display("6");

    $HW_Register_Instance(
    5,
    `RIO_PE_5,
    /* parameters: */
    `RIO_ADDRESS_SUPPORT_CODE, /*ext_Address_Support code*/
    `RIO_FALSE, /*is_Bridge*/
    `RIO_TRUE, /*has_Memory*/
    `RIO_FALSE, /*has_Processor*/
    ~`RIO_COH_GRANULE_SIZE_IS_64, /*coh_Granule_Size_32*/
    `RIO_TRX_NOGSM, //32'h003FFFF0, /*source_Trx*/
    `RIO_TRX_ALL, /*dest_Trx*/ 
    `RIO_TRUE, /*mbox1*/
    `RIO_TRUE, /*mbox2*/
    `RIO_TRUE, /*mbox3*/
    `RIO_TRUE, /*mbox4*/
    `RIO_TRUE, /*has_Doorbell*/
    `RIO_IN_BUF_SIZE, /*pl_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*pl_Outbound_Buffer_Size*/
    `RIO_IN_BUF_SIZE, /*ll_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*ll_Outbound_Buffer_Size*/
    0 /*coh_Domain_Size*/,

/*---------------------------------------------------------*/
/*New 10 parameters*/
    0, //device_Identity
    0, //device_Vendor_Identity
    0, //device_Rev
    0, //device_Minor_Rev
    0, //device_Major_Rev
    0, //assy_Identity
    0, //assy_Vendor_Identity
    0, //assy_Rev
	'h100, //entry_Extended_Features_Ptr /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	'h1000, //next_Extended_Features_Ptr /*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
/*End of new 10 parameters*/
/*---------------------------------------------------------*/


    /* PLI registers: */
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_GSM_Result_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Loc_Response_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg,
    `RIO_PE_5.pli_IO_Trx_Tag_Reg, 
    `RIO_PE_5.pli_IO_Result_Reg,
    `RIO_PE_5.pli_IO_Err_Code_Reg,
    `RIO_PE_5.pli_IO_Req_Done_Flag_Reg,
    `RIO_PE_5.pli_MP_Trx_Tag_Reg, 
    `RIO_PE_5.pli_MP_Result_Reg,
    `RIO_PE_5.pli_MP_Err_Code_Reg,
    `RIO_PE_5.pli_MP_Req_Done_Flag_Reg,
    `RIO_PE_5.pli_DB_Trx_Tag_Reg, 
    `RIO_PE_5.pli_DB_Result_Reg,
    `RIO_PE_5.pli_DB_Err_Code_Reg,
    `RIO_PE_5.pli_DB_Req_Done_Flag_Reg,
    `RIO_PE_5.pli_Config_Trx_Tag_Reg, 
    `RIO_PE_5.pli_Config_Result_Reg,
    `RIO_PE_5.pli_Config_Err_Code_Reg,
    `RIO_PE_5.pli_Config_Req_Done_Flag_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Snoop_Address_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Snoop_Ext_Address_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Snoop_XAMSBS_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Snoop_LTType_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Snoop_TType_Reg,
    `RIO_PE_5.`RIO_CACHE_CONTROL.pli_Snoop_Req_Flag_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_MP_MSGLEN_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_MP_SSIZE_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_MP_LETTER_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_MP_MBOX_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_MP_MSGSEG_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_MP_DW_Size_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_MP_Flag_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_DB_Info_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_Address_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_Ext_Address_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_XAMSBS_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_DW_Size_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_Type_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_Is_GSM_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_BE_Reg,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.pli_Mem_Flag_Reg,
    `RIO_PE_5.pli_Pins_TFrame_Reg,
    `RIO_PE_5.pli_Pins_TD_Reg,
    `RIO_PE_5.pli_Pins_TDL_Reg,
    `RIO_PE_5.pli_Pins_TCLK_Reg,
    `RIO_PE_5.pli_Pins_Flag_Reg,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.mailbox_CSR,
    `RIO_PE_5.`RIO_MAILBOX_CONTROL.doorbell_CSR,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_IO_Space_XAMSBS,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_IO_Space_Ext_Address,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_IO_Space_DW_Size,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_GSM_Space_XAMSBS,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_GSM_Space_Ext_Address,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_GSM_Space_Start_Address,
    `RIO_PE_5.`RIO_MEMORY_CONTROL.mem_GSM_Space_DW_Size,
    pli_EMPTY_Reg /* dma_MSR */,
    pli_EMPTY_Reg /* dma_SAR */,
    pli_EMPTY_Reg /* dma_DAR */,
    pli_EMPTY_Reg /* dma_BCR */,
    pli_EMPTY_Reg /* dma_src_Inst_ID */
    );
    
    // SWITCHES REGISTRATION        

    $HW_Switch_Register_Instance(
    1 /* = 1-st instance parameter - inst_ID */,
    7 /* = 2-nd instance parameter - max. number of used LP-EP link + 1 */,

/*New parameters*/
    1, /*device_Identity*/
    0, /*device_Vendor_Identity*/
    0, /*device_Rev*/
    0, /*device_Minor_Rev */
    0, /*device_Major_Rev */
    0, /*assy_Identity */
    0, /*assy_Vendor_Identity*/
    0, /*assy_Rev*/

    'h100,/*entry_Extended_Features_Ptr */

/*End of new parameters*/

    /* PLI registers: */
    `RIO_SWITCH_1.pli_Pins_TFrame_Reg,
    `RIO_SWITCH_1.pli_Pins_TCLK_Reg,
    `RIO_SWITCH_1.pli_Pins_Flag_Reg,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_0,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_1,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_2,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_3,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_4,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_5,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_6,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_7,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_8,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_9,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_10,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_11,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_12,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_13,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_14,
    `RIO_SWITCH_1.pli_Pins_TD_Reg_15,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_0,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_1,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_2,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_3,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_4,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_5,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_6,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_7,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_8,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_9,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_10,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_11,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_12,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_13,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_14,
    `RIO_SWITCH_1.pli_Pins_TDL_Reg_15
    );

//    $display("7");
    
    $HW_Switch_Register_Instance(
    2 /* = 1-st instance parameter - inst_ID */,
    7 /* = 2-nd instance parameter - max. number of used LP-EP link + 1 */,

/*New parameters*/
    2, /*device_Identity*/
    0, /*device_Vendor_Identity*/
    0, /*device_Rev*/
    0, /*device_Minor_Rev */
    0, /*device_Major_Rev */
    0, /*assy_Identity */
    0, /*assy_Vendor_Identity*/
    0, /*assy_Rev*/

    'h100,/*entry_Extended_Features_Ptr */

/*End of new parameters*/


    /* PLI registers: */
    `RIO_SWITCH_2.pli_Pins_TFrame_Reg,
    `RIO_SWITCH_2.pli_Pins_TCLK_Reg,
    `RIO_SWITCH_2.pli_Pins_Flag_Reg,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_0,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_1,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_2,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_3,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_4,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_5,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_6,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_7,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_8,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_9,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_10,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_11,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_12,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_13,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_14,
    `RIO_SWITCH_2.pli_Pins_TD_Reg_15,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_0,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_1,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_2,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_3,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_4,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_5,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_6,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_7,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_8,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_9,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_10,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_11,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_12,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_13,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_14,
    `RIO_SWITCH_2.pli_Pins_TDL_Reg_15
    );

//    $display("8");
    
//------------------------------------------------------
    
    $display(" ");
    $display("***************** Demo Application started *********************");
    $display("   Start simulation time: %7t", $time);
    $display(" ");

    $display("----------------------------------------------------------------");
    $display("                       INITIALIZATION                           ");
    $display("----------------------------------------------------------------");
    
    // PEs INITIALIZATION        
    
//    $display("Reset 1");
//    $HW_Start_Reset_Instance(1);
//    $display("Init 1");
    $HW_Initialize_Instance(1,
        (`RIO_TR_INFO_SIZE == `RIO_TR_INFO_16) ? `RIO_PL_TRANSP_16 : `RIO_PL_TRANSP_32 /*Tr. Info size*/,
        `RIO_DEV_ID_PE1 /*dev_ID*/,
        0 /*lcshbar*/,
        0 /*lcsbar*/,
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        `RIO_MODE_8,
        `RIO_MAX_TIME_OUT /*receive_Idle_Lim*/,
        `RIO_MAX_TIME_OUT /*throttle_Idle_Lim*/,
        `RIO_RES_FOR_OUT /*res_For_3_Out*/,
        `RIO_RES_FOR_OUT /*res_For_2_Out*/,
        `RIO_RES_FOR_OUT /*res_For_1_Out*/, 
        `RIO_RES_FOR_IN /*res_For_3_In*/,
        `RIO_RES_FOR_IN /*res_For_2_In*/,
        `RIO_RES_FOR_IN /*res_For_1_In*/,
        `RIO_PASS_BY_PRIO,
        0 /*config_Space_Size (mapped on IO memory space)*/,
		`RIO_OPERATING_MODE,	/*defines the current working mode : 8 or 16 bit*/
		`RIO_REQ_WINDOW_ALIGN,/*defines if the model needs training sequence*/
		`RIO_NUM_TRANINGS,	/*number of sent training patterns not used if RIO_REQ_WINDOW_ALIGN = RIO_FALSE*/

/*--------------------------------------------------------------------------*/
/*New 5 parameters*/
  	0, //is_Host
	`RIO_TRUE, //is_Master_Enable 
	0, //is_Discovered
    `RIO_TRUE, //input_Is_Enable
    `RIO_TRUE, //output_Is_Enable
/*End of New 5 parameters*/
/*--------------------------------------------------------------------------*/

        /* add ID-s if coh_Domain_Size > 1 */
        `RIO_DEV_ID_PE2,        
        `RIO_DEV_ID_PE3,        
        `RIO_DEV_ID_PE4,        
        /* add tr_info table values for shift- or index-based routing */
        /* 1 -> 1 */
        {`RIO_TR_INFO_SIZE {1'b0}},                                     
        /* 1 -> 2 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b010,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},   
        /* 1 -> 3 */
        {`RIO_PORT_ENC_SIZE'b011, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},  
        /* 1 -> 4 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b100,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},  
        /* 1 -> 5 */
        {`RIO_PORT_ENC_SIZE'b101, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}}  
        );

//    $display("9");

//    $display("Reset 2");
//    $HW_Start_Reset_Instance(2);
//    $display("Init 2");
    $HW_Initialize_Instance(2,
        (`RIO_TR_INFO_SIZE == `RIO_TR_INFO_16) ? `RIO_PL_TRANSP_16 : `RIO_PL_TRANSP_32 /*Tr. Info size*/,
        `RIO_DEV_ID_PE2 /*dev_ID*/,
        0 /*lcshbar*/,
        0 /*lcsbar*/,
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        `RIO_MODE_8,
        `RIO_MAX_TIME_OUT /*receive_Idle_Lim*/,
        `RIO_MAX_TIME_OUT /*throttle_Idle_Lim*/,
        `RIO_RES_FOR_OUT /*res_For_3_Out*/,
        `RIO_RES_FOR_OUT /*res_For_2_Out*/,
        `RIO_RES_FOR_OUT /*res_For_1_Out*/, 
        `RIO_RES_FOR_IN /*res_For_3_In*/,
        `RIO_RES_FOR_IN /*res_For_2_In*/,
        `RIO_RES_FOR_IN /*res_For_1_In*/,
        `RIO_PASS_BY_PRIO,
        0 /*config_Space_Size (mapped on IO memory space)*/,
		`RIO_OPERATING_MODE,	/*defines the current working mode : 8 or 16 bit*/
		`RIO_REQ_WINDOW_ALIGN,/*defines if the model needs training sequence*/
		`RIO_NUM_TRANINGS,	/*number of sent training patterns not used if RIO_REQ_WINDOW_ALIGN = RIO_FALSE*/

/*--------------------------------------------------------------------------*/
/*New 5 parameters*/
  	0, //is_Host
	`RIO_TRUE, //is_Master_Enable 
	0, //is_Discovered
    `RIO_TRUE, //input_Is_Enable
    `RIO_TRUE, //output_Is_Enable
/*End of New 5 parameters*/
/*--------------------------------------------------------------------------*/

        /* add ID-s if coh_Domain_Size > 1 */
        `RIO_DEV_ID_PE1,        
        `RIO_DEV_ID_PE3,        
        `RIO_DEV_ID_PE4,        
        /* add tr_info table values for shift- or index-based routing */
        /* 2 -> 1 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b001,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},   
        /* 2 -> 2 */
        {`RIO_TR_INFO_SIZE {1'b0}},                               
        /* 2 -> 3 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b011,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},   
        /* 2 -> 4 */
        {`RIO_PORT_ENC_SIZE'b100, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},               
        /* 2 -> 5 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b101,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}}    
        );

//    $display("10");
        
//    $display("Reset 3");
//    $HW_Start_Reset_Instance(3);
//    $display("Init 3");
    $HW_Initialize_Instance(3,
        (`RIO_TR_INFO_SIZE == `RIO_TR_INFO_16) ? `RIO_PL_TRANSP_16 : `RIO_PL_TRANSP_32 /*Tr. Info size*/,
        `RIO_DEV_ID_PE3 /*dev_ID*/,
        0 /*lcshbar*/,
        0 /*lcsbar*/,
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        `RIO_MODE_8,
        `RIO_MAX_TIME_OUT /*receive_Idle_Lim*/,
        `RIO_MAX_TIME_OUT /*throttle_Idle_Lim*/,
        `RIO_RES_FOR_OUT /*res_For_3_Out*/,
        `RIO_RES_FOR_OUT /*res_For_2_Out*/,
        `RIO_RES_FOR_OUT /*res_For_1_Out*/, 
        `RIO_RES_FOR_IN /*res_For_3_In*/,
        `RIO_RES_FOR_IN /*res_For_2_In*/,
        `RIO_RES_FOR_IN /*res_For_1_In*/,
        `RIO_PASS_BY_PRIO,
        0 /*config_Space_Size (mapped on IO memory space)*/,
		`RIO_OPERATING_MODE,	/*defines the current working mode : 8 or 16 bit*/
		`RIO_REQ_WINDOW_ALIGN,/*defines if the model needs training sequence*/
		`RIO_NUM_TRANINGS,	/*number of sent training patterns not used if RIO_REQ_WINDOW_ALIGN = RIO_FALSE*/

/*--------------------------------------------------------------------------*/
/*New 5 parameters*/
  	0, //is_Host
	`RIO_TRUE, //is_Master_Enable 
	0, //is_Discovered
    `RIO_TRUE, //input_Is_Enable
    `RIO_TRUE, //output_Is_Enable
/*End of New 5 parameters*/
/*--------------------------------------------------------------------------*/

        /* add ID-s if coh_Domain_Size > 1 */
        `RIO_DEV_ID_PE1,        
        `RIO_DEV_ID_PE2,        
        `RIO_DEV_ID_PE4,        
        /* add tr_info table values for shift- or index-based routing */
        /* 3 -> 1 */
        {`RIO_PORT_ENC_SIZE'b001, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},               
        /* 3 -> 2 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b010,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},
        /* 3 -> 3 */
        {`RIO_TR_INFO_SIZE {1'b0}},                         
        /* 3 -> 4 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b100,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},
        /* 3 -> 5 */
        {`RIO_PORT_ENC_SIZE'b101, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}}                
        );

//    $display("11");
        
//    $display("Reset 4");
//    $HW_Start_Reset_Instance(4);
//    $display("Init 4");
    $HW_Initialize_Instance(4,
        (`RIO_TR_INFO_SIZE == `RIO_TR_INFO_16) ? `RIO_PL_TRANSP_16 : `RIO_PL_TRANSP_32 /*Tr. Info size*/,
        `RIO_DEV_ID_PE4 /*dev_ID*/,
        0 /*lcshbar*/,
        0 /*lcsbar*/,
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        `RIO_MODE_8,
        `RIO_MAX_TIME_OUT /*receive_Idle_Lim*/,
        `RIO_MAX_TIME_OUT /*throttle_Idle_Lim*/,
        `RIO_RES_FOR_OUT /*res_For_3_Out*/,
        `RIO_RES_FOR_OUT /*res_For_2_Out*/,
        `RIO_RES_FOR_OUT /*res_For_1_Out*/, 
        `RIO_RES_FOR_IN /*res_For_3_In*/,
        `RIO_RES_FOR_IN /*res_For_2_In*/,
        `RIO_RES_FOR_IN /*res_For_1_In*/,
        `RIO_PASS_BY_PRIO,
        0 /*config_Space_Size (mapped on IO memory space)*/,
		`RIO_OPERATING_MODE,	/*defines the current working mode : 8 or 16 bit*/
		`RIO_REQ_WINDOW_ALIGN,/*defines if the model needs training sequence*/
		`RIO_NUM_TRANINGS,	/*number of sent training patterns not used if RIO_REQ_WINDOW_ALIGN = RIO_FALSE*/

/*--------------------------------------------------------------------------*/
/*New 5 parameters*/
  	0, //is_Host
	`RIO_TRUE, //is_Master_Enable 
	0, //is_Discovered
    `RIO_TRUE, //input_Is_Enable
    `RIO_TRUE, //output_Is_Enable
/*End of New 5 parameters*/
/*--------------------------------------------------------------------------*/

        /* add ID-s if coh_Domain_Size > 1 */
        `RIO_DEV_ID_PE1,        
        `RIO_DEV_ID_PE2,        
        `RIO_DEV_ID_PE3,        
        /* add tr_info table values for shift- or index-based routing */
        /* 4 -> 1 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b001,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},  
        /* 4 -> 2 */
        {`RIO_PORT_ENC_SIZE'b010, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},            
        /* 4 -> 3 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b011,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},  
        /* 4 -> 4 */
        {`RIO_TR_INFO_SIZE {1'b0}},                         
        /* 4 -> 5 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b101,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}}   
        );

//    $display("12");

//    $display("Reset 5");
//    $HW_Start_Reset_Instance(5);
//    $display("Init 5");
    $HW_Initialize_Instance(5,
        (`RIO_TR_INFO_SIZE == `RIO_TR_INFO_16) ? `RIO_PL_TRANSP_16 : `RIO_PL_TRANSP_32 /*Tr. Info size*/,
        `RIO_DEV_ID_PE5 /*dev_ID*/,
        0 /*lcshbar*/,
        0 /*lcsbar*/,
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        `RIO_MODE_8,
        `RIO_MAX_TIME_OUT /*receive_Idle_Lim*/,
        `RIO_MAX_TIME_OUT /*throttle_Idle_Lim*/,
        `RIO_RES_FOR_OUT /*res_For_3_Out*/,
        `RIO_RES_FOR_OUT /*res_For_2_Out*/,
        `RIO_RES_FOR_OUT /*res_For_1_Out*/, 
        `RIO_RES_FOR_IN /*res_For_3_In*/,
        `RIO_RES_FOR_IN /*res_For_2_In*/,
        `RIO_RES_FOR_IN /*res_For_1_In*/,
        `RIO_PASS_BY_PRIO,
        0 /*config_Space_Size (mapped on IO memory space)*/,
		`RIO_OPERATING_MODE,	/*defines the current working mode : 8 or 16 bit*/
		`RIO_REQ_WINDOW_ALIGN,/*defines if the model needs training sequence*/
		`RIO_NUM_TRANINGS,	/*number of sent training patterns not used if RIO_REQ_WINDOW_ALIGN = RIO_FALSE*/

/*--------------------------------------------------------------------------*/
/*New 5 parameters*/
  	`RIO_TRUE, //is_Host
	`RIO_TRUE, //is_Master_Enable 
	0, //is_Discovered
    `RIO_TRUE, //input_Is_Enable
    `RIO_TRUE, //output_Is_Enable
/*End of New 5 parameters*/
/*--------------------------------------------------------------------------*/

        /* add ID-s if coh_Domain_Size > 1 */
        /* add tr_info table values for shift- or index-based routing */
        /* 5 -> 1 */
        {`RIO_PORT_ENC_SIZE'b001, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},         
        /* 5 -> 2 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b010,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}}, 
        /* 5 -> 3 */
        {`RIO_PORT_ENC_SIZE'b011, {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}},         
        /* 5 -> 4 */
        {`RIO_PORT_ENC_SIZE'b110, `RIO_PORT_ENC_SIZE'b100,
        {(`RIO_TR_INFO_SIZE - `RIO_PORT_ENC_SIZE - `RIO_PORT_ENC_SIZE) {1'b0}}}, 
        /* 5 -> 5 */
        {`RIO_TR_INFO_SIZE {1'b0}}              
        );
        
    // SWITCHES INITIALIZATION        
        
//    $HW_Switch_Start_Reset_Instance(1);

//    $display("13");
//    $display("SWITCHES INITIALIZATION");


//	Filling internal array for switch initialization
	for (i = 0; i <=  `RIO_NUM_OF_SWITCH_LINKS - 1; i = i + 1)
	begin
		$HW_Set_Switch_Mode_Param(i, `RIO_OPERATING_MODE, `RIO_REQ_WINDOW_ALIGN, `RIO_NUM_TRANINGS);
	end

    $HW_Switch_Initialize_Instance(1,
        //`RIO_CLOCK_RATE,
        `RIO_MODE_8,
//        `RIO_ROUTING_METHOD, /*not support already*/
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        5 /* routing table size */,
        /* routing table: */
        1, `RIO_DEV_ID_PE1,
        3, `RIO_DEV_ID_PE3,
        5, `RIO_DEV_ID_PE5,
        6, `RIO_DEV_ID_PE2,
        6, `RIO_DEV_ID_PE4
        );

//    $display("14");
        
//    $HW_Switch_Start_Reset_Instance(2);

//    $display("15");

    $HW_Switch_Initialize_Instance(2,
        //`RIO_CLOCK_RATE,
        `RIO_MODE_8,
//        `RIO_ROUTING_METHOD, /*not support already*/
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        5 /* routing table size */,
        /* routing table: */
        2, `RIO_DEV_ID_PE2,
        4, `RIO_DEV_ID_PE4,
        6, `RIO_DEV_ID_PE1,
        6, `RIO_DEV_ID_PE3,
        6, `RIO_DEV_ID_PE5
        );

//    $display("INIT DONE - DEMO START!");

    $display("----------------------------------------------------------------");
    $display("         INITIALIZATION FINISHED, DEMO SCENARIO STARTED         ");
    $display("----------------------------------------------------------------");
    $display("");


    if (`RIO_USE_SIMPLE_SCENARIO_IN_SWITCH)
    begin
        $display(" ");
        $display("************ First part of Simple Demo Scenario started ***********");
        $display("   Simulation time: %7t", $time);
        $display(" ");

        //`RIO_PE_1.demo_Process_Scenario_1 = 1'b1;     // Config requests + DMA + Doorbell from DMA
        `RIO_PE_1.demo_Process_Scenario_2 = 1'b1;     // Message receiver and memory initialization
        //`RIO_PE_1.demo_Process_Scenario_3 = 1'b1;     // ALT process (just IO write and read)

        `RIO_DMA_PE.demo_Process_Scenario_1 = 1'b1;   // Message sender
        //`RIO_DMA_PE.demo_Process_Scenario_2 = 1'b1;   // IO requestor     /////

        wait ((`RIO_PE_1.demo_Process_Scenario_1 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_1 === 1'b0) &&
            (`RIO_PE_1.demo_Process_Scenario_2 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_2 === 1'b0) &&    
            (`RIO_PE_1.demo_Process_Scenario_3 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_3 === 1'b0) &&    
            (`RIO_PE_1.demo_Process_Scenario_4 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_4 === 1'b0) &&    
            (`RIO_PE_1.demo_Process_Scenario_5 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_5 === 1'b0));    

        $display(" ");
        $display("*********** Second part of Simple Demo Scenario started ***********");
        $display("   Simulation time: %7t", $time);
        $display(" ");

        `RIO_PE_1.demo_Process_Scenario_1 = 1'b1;     // Config requests + DMA + Doorbell from DMA
        //`RIO_PE_1.demo_Process_Scenario_2 = 1'b1;     // Message receiver
        //`RIO_PE_1.demo_Process_Scenario_3 = 1'b1;     // ALT process, called AUTOMATICALLY from scenario 1

        //`RIO_DMA_PE.demo_Process_Scenario_1 = 1'b1;   // Message sender
        //`RIO_DMA_PE.demo_Process_Scenario_2 = 1'b1;   // IO requestor

        wait ((`RIO_PE_1.demo_Process_Scenario_1 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_1 === 1'b0) &&
            (`RIO_PE_1.demo_Process_Scenario_2 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_2 === 1'b0) &&    
            (`RIO_PE_1.demo_Process_Scenario_3 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_3 === 1'b0) &&    
            (`RIO_PE_1.demo_Process_Scenario_4 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_4 === 1'b0) &&    
            (`RIO_PE_1.demo_Process_Scenario_5 === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_5 === 1'b0));    
    end        
    else
    begin
        /* Here it's needed to initialize data in the "DRIVE" memory */
    
        $display("-------------------------- STEP 1 ------------------------------");
        $display("");
        $display("DEMO: At first, we shall initialize data stored in the drive    ");
        $display("      memory space                                              ");
        $display("");
        $display("----------------------------------------------------------------");
        
        MakePause;
        
        `RIO_PE_5.demo_Process_Scenario_1S = 1'b1;   // Drive initializes its memory space

        wait (`RIO_PE_5.demo_Process_Scenario_1S === 1'b0) 
        
        /* Here it's needed to load arguments from the "DRIVE" IO memory to "HOST" IO memory */
        /* We could program DMA to do it */ 
        
        $display("-------------------------- STEP 2 ------------------------------");
        $display("");
        $display("DEMO: Now arguments are stored in the drive memory space,       ");
        $display("       programming DMA to load it to the host memory space      ");
        $display("");
        $display("----------------------------------------------------------------");

        MakePause;

        `RIO_DMA_PE.demo_Process_Scenario_1S = 1'b1;   // Host programmes its own DMA to
                                                       // make required IO transfer

        wait (`RIO_DMA_PE.demo_Process_Scenario_1S === 1'b0) 

        /* Here it's needed to copy arguments and result data from host IO memory to its GSM locations */

        $display("-------------------------- STEP 3 ------------------------------");
        $display("");
        $display("DEMO: Now arguments are stored in the host memory space,        ");
        $display("      copying to the arguments GSM memory space for encounting  ");
        $display("");
        $display("----------------------------------------------------------------");
        
        MakePause;

        `RIO_DMA_PE.demo_Process_Scenario_2S = 1'b1;   // Host reads own local memory and
                                                       // fills "ARGUMENTS" and "RESULT" memory spaces

        wait (`RIO_DMA_PE.demo_Process_Scenario_2S === 1'b0) 

        $display("-------------------------- STEP 4 ------------------------------");
        $display("");
        $display("DEMO: Arguments are ready, starting calculation processes (PE 1, 3 and 4)...");
        $display("");
        $display("----------------------------------------------------------------------------");
        
        MakePause;

        `RIO_PE_1.demo_Process_Scenario_2S = 1'b1;     // Calculation process in PE1
        `RIO_PE_3.demo_Process_Scenario_2S = 1'b1;     // Calculation process in PE3
        `RIO_PE_4.demo_Process_Scenario_2S = 1'b1;     // Calculation process in PE4

        wait ((`RIO_PE_1.demo_Process_Scenario_2S === 1'b0) &&     
            (`RIO_PE_3.demo_Process_Scenario_2S === 1'b0) &&
            (`RIO_PE_4.demo_Process_Scenario_2S === 1'b0));     

        /* Now host shall read "RESULT" memory space  */
        /* and copy it to the own local memory        */    
            
        $display("-------------------------- STEP 5 ------------------------------");
        $display("");
        $display("DEMO: Calculation finished, copying result to the host memory...");
        $display("");
        $display("----------------------------------------------------------------");
            
        MakePause;

        `RIO_DMA_PE.demo_Process_Scenario_3S = 1'b1;   

        wait (`RIO_DMA_PE.demo_Process_Scenario_3S === 1'b0);

        /* Here it's needed to store result from the "HOST" IO memory on the "DRIVE" IO memory */
        /* We could program DMA to do it */ 

        $display("-------------------------- STEP 6 ------------------------------");
        $display("");
        $display("DEMO: Now result is stored in the host memory, programming DMA  ");
        $display("      to store it in the drive (PE5) memory space               ");
        $display("");
        $display("----------------------------------------------------------------");
        
        MakePause;
        
        #10;

        `RIO_DMA_PE.demo_Process_Scenario_4S = 1'b1;   // Host programmes its own DMA to
                                                       // make required IO transfer

        wait (`RIO_DMA_PE.demo_Process_Scenario_4S === 1'b0) 
        
        /* Here it's needed to read and display data in the "DRIVE" memory */

        $display("-------------------------- STEP 7 ------------------------------");
        $display("");
        $display("DEMO: At last, we shall display the resulted data stored        ");
        $display("       in the drive memory space                                ");
        $display("");
        $display("----------------------------------------------------------------");
        
        MakePause;

        `RIO_PE_5.demo_Process_Scenario_5S = 1'b1;   // Drive reads "RESULT" memory space
                                                     // and displays it
    
        wait ((`RIO_PE_1.demo_Process_Scenario_1S === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_1S === 1'b0) &&
            (`RIO_PE_3.demo_Process_Scenario_1S === 1'b0) &&     
            (`RIO_PE_4.demo_Process_Scenario_1S === 1'b0) &&     
            (`RIO_PE_5.demo_Process_Scenario_1S === 1'b0) &&     
            (`RIO_PE_1.demo_Process_Scenario_2S === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_2S === 1'b0) &&    
            (`RIO_PE_3.demo_Process_Scenario_2S === 1'b0) &&     
            (`RIO_PE_4.demo_Process_Scenario_2S === 1'b0) &&     
            (`RIO_PE_5.demo_Process_Scenario_2S === 1'b0) &&     
            (`RIO_PE_1.demo_Process_Scenario_3S === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_3S === 1'b0) &&    
            (`RIO_PE_3.demo_Process_Scenario_3S === 1'b0) &&     
            (`RIO_PE_4.demo_Process_Scenario_3S === 1'b0) &&     
            (`RIO_PE_5.demo_Process_Scenario_3S === 1'b0) &&     
            (`RIO_PE_1.demo_Process_Scenario_4S === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_4S === 1'b0) &&    
            (`RIO_PE_3.demo_Process_Scenario_4S === 1'b0) &&     
            (`RIO_PE_4.demo_Process_Scenario_4S === 1'b0) &&     
            (`RIO_PE_5.demo_Process_Scenario_4S === 1'b0) &&     
            (`RIO_PE_1.demo_Process_Scenario_5S === 1'b0) &&     
            (`RIO_DMA_PE.demo_Process_Scenario_5S === 1'b0) &&    
            (`RIO_PE_3.demo_Process_Scenario_5S === 1'b0) &&     
            (`RIO_PE_4.demo_Process_Scenario_5S === 1'b0) &&     
            (`RIO_PE_5.demo_Process_Scenario_5S === 1'b0));     
    end
 
    $display(" ");
    $display("******************* Demo Application Results ******************");
 

    if (`RIO_USE_SIMPLE_SCENARIO_IN_SWITCH)
    begin
        if ((`RIO_PE_1.error_Counter + `RIO_DMA_PE.error_Counter) === 0)
            $display("                  NO ERRORS: FINISHED DEMO OK");
        else
        begin
            $display("       DEMO: %d ERRORS occured in PE 1 demo scenario", `RIO_PE_1.error_Counter);
            $display("       DEMO: %d ERRORS occured in PE 2 (DMA PE) demo scenario", `RIO_DMA_PE.error_Counter);
            $display("                ERRORS HAVE OCCURED: DEMO FAILED");
        end
    end
    else
    begin
        if ((`RIO_PE_1.error_Counter +
            `RIO_DMA_PE.error_Counter +
            `RIO_PE_3.error_Counter +           
            `RIO_PE_4.error_Counter +           
            `RIO_PE_5.error_Counter) === 0)
            $display("                  NO ERRORS: FINISHED DEMO OK");
        else
        begin
            $display("       DEMO: %d ERRORS occured in the demo scenario",
                `RIO_PE_1.error_Counter +
                `RIO_DMA_PE.error_Counter +
                `RIO_PE_3.error_Counter +           
                `RIO_PE_4.error_Counter +           
                `RIO_PE_5.error_Counter);
            $display("                ERRORS HAVE OCCURED: DEMO FAILED");
        end
    end

    $display("   Finish simulation time: %7t", $time);
    $display("****************** Demo Application Finished ******************");
    $display(" ");

    $HW_Close;

    $finish;
end

/***************************************************************************
 * Task : MakePause
 *
 * Description: If (`RIO_DEMO_MAKE_PAUSES) makes pause ($stop) and asks
 *              user for continue
 *
 * Notes: 
 *
 **************************************************************************/
task MakePause;
    integer i;
begin
    if (`RIO_DEMO_MAKE_PAUSES)
    begin
        $display("");
`ifdef _MODELSIM_
        $display("Please enter 'run -all' to start this step of the demo scenario or 'quit' to quit from the Demo Application");
`else                
        $display("Please enter '.' to start this step of the demo scenario or 'q' to quit from the Demo Application right now");
`endif
        $stop;
    end
end
endtask

// -------------------------------------------------------------

endmodule

