/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/dma.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  RIO DMA processing element
*
* Notes:        
*
******************************************************************************/

`timescale 1ns/1ns

`define RIO_DMA_QUEUE_WIDTH        `RIO_BYTES_IN_WORD * `RIO_BITS_IN_BYTE
`define RIO_DMA_QUEUE_LENGTH       5
`define RIO_DMA_ADDRESS_WIDTH      32 
`define RIO_DMA_MAX_BYTE_COUNT     65536    

module `RIO_DMA_PE_MODULE (clock, tclk, tframe, td, tdl, rclk, rframe, rd, rdl);
    
    input clock; // self clock for tclk forcing

    input rclk;    
    input rframe;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    output tclk;
    output tframe;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;

    reg tclk_Reg;
    reg tframe_Reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td_Reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl_Reg;

    assign tclk = tclk_Reg;
    assign tframe = tframe_Reg;
    assign td = td_Reg;
    assign tdl = tdl_Reg;

// (1)
// This parameter specifies instance number for the PE instance, which will be used 
// to recognize PE instance in PLI interface and RIO model functions 
    parameter inst_ID = 0;

// (2)
// This parameter is the mask which selects which submodules shall be included in this PE
// instance. 1 means "include", 0 means "do not include". Default value: include nothing.
// Positions: 1: cache control
//            2: mailbox control
//            3: memory control
    parameter [1 : 3] include_Instances_Mask = 3'b000; 

// (3)
// This parameter selects which scenario shall be used with the PE instance 
// It can be used by the Demo Scenario, which is the part of PE model
// Zero value means that Demo Scenario is absent in this PE instance (for example,
// in case of DMA PE or memory-only PE)  
// Because it's DMA PE, this parameter is not used and should be always = 0
//    parameter scenario_Number = 0;  

/* common registers for data exchange through PLI */
    `include "pli_registers.vh"

/* common registers for data/events exchange between the top module and demo scenarios of PE-s */
/* also some needed variables definition and initialization                                    */
    `include "demo_registers.vh"

//  DMA registers

    reg [0 : `RIO_DMA_QUEUE_WIDTH - 1] queue_MS[1 : `RIO_DMA_QUEUE_LENGTH];   // DMA queue: MS bits
    reg [0 : `RIO_DMA_QUEUE_WIDTH - 1] queue_LS[1 : `RIO_DMA_QUEUE_LENGTH];   // DMA queue: LS bits
    reg [0 : `RIO_BITS_IN_BYTE - 1] msr;                                      // Mode and Status Register
    reg [0 : `RIO_DMA_ADDRESS_WIDTH - 1] sar;                                 // Source Address Register
    reg [0 : `RIO_DMA_ADDRESS_WIDTH - 1] dar;                                 // Destination Address Register
    reg [0 : `RIO_DMA_ADDRESS_WIDTH - 1] bcr;                                 // Byte Count Register
    reg [0 : `RIO_BITS_IN_BYTE - 1] src_Inst_ID;                              // Source PE instance ID 

// DMA algorithm working variables

    integer queueWritePos;    // Number of the last free line (cell) in the DMA queue
                              // = `RIO_DMA_QUEUE_LENGTH - queue is empty (e.g. before DMA transfer)
                              // = 0 - queue is full, no empty places
//    integer queueByteCount[1 : `RIO_DMA_QUEUE_LENGTH];

    reg [0 : 2] queue_Offset;
    reg [0 : 2] queue_Remainder;    
                         
    integer temp_Res;                         

// Variables for temporary trx tag storage     
    
    integer tmp_Trx_Tag_1;
    integer tmp_Trx_Tag_2;
    integer tmp_Trx_Tag_3;
    integer tmp_Trx_Tag_4;
                              
/////////////////////////////////////////////////////////////////

/* Submodules instantiation */

    `RIO_CACHE_CONTROL_MODULE #(include_Instances_Mask[1] ? (inst_ID /* * 10 + 1 */) : 0) `RIO_CACHE_CONTROL(); 
    `RIO_MAILBOX_CONTROL_MODULE #(include_Instances_Mask[2] ? (inst_ID /* * 10 + 2 */) : 0) `RIO_MAILBOX_CONTROL();
    `RIO_MEMORY_CONTROL_MODULE #(include_Instances_Mask[3] ? (inst_ID /* * 10 + 3 */) : 0) `RIO_MEMORY_CONTROL(); 

    // PLI registers repeaters

    assign pli_GSM_Trx_Tag_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg;
    assign pli_GSM_Result_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Result_Reg;
    assign pli_GSM_Err_Code_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg;
    assign pli_GSM_Req_Done_Flag_Mirror = `RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg;

    assign pli_Loc_Trx_Tag_Mirror = `RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg;
    assign pli_Loc_Response_Mirror = `RIO_CACHE_CONTROL.pli_Loc_Response_Reg;
    assign pli_Loc_Response_Flag_Mirror = `RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg;

/* ********************************************************************* */

// physical (LP-EP) interface control 

// external clock receiver
always @(clock)
begin
    $HW_Clock_Edge(inst_ID);
end 

// incoming LP-EP clock receiver
always @(rclk)
begin
    $HW_Get_Pins(inst_ID, rframe, rd, rdl, rclk);
end 

// output LP-EP pins driver
always @(posedge pli_Pins_Flag_Reg)
begin
    tframe_Reg = pli_Pins_TFrame_Reg;
    td_Reg = pli_Pins_TD_Reg;
    tdl_Reg = pli_Pins_TDL_Reg;
    tclk_Reg = pli_Pins_TCLK_Reg;
    pli_Pins_Flag_Reg = 1'b0;
end

// DMA algorithm

// Working variables and registers

    event startTransfer;

    reg [0 : 2] src_Offset;
    reg [0 : 2] dest_Offset;

    integer tempReadByteCount;

    reg [0 : 2] cur_Offset;
    reg [0 : 2] cur_Remainder;
    integer cur_Index;
    reg [0 : 3] cur_Byte_Nums[0 : 3];
    integer error_Counter;

// DMA initialization
initial
begin
//    $display("DMA1");
    error_Counter = 0;
    msr[`RIO_DMA_BUSY_BIT_NUMBER] = 1'b0;
    ResetDMA;
end    

// "Channel start" bit assertion handler
always @(posedge msr[`RIO_DMA_CS_BIT_NUMBER])   
begin
    if (msr[`RIO_DMA_BUSY_BIT_NUMBER] === 1'b0) // do nothing if DMA is busy
    begin
        msr[`RIO_DMA_BUSY_BIT_NUMBER] = 1'b1;    
        src_Offset = sar[`RIO_DMA_ADDRESS_WIDTH - 3 : `RIO_DMA_ADDRESS_WIDTH - 1]; 
        if (src_Offset !== 3'b000)
        begin
            // "sar" must be DW-aligned
            sar[`RIO_DMA_ADDRESS_WIDTH - 1] = 3'b000;
        end
        dest_Offset = dar[`RIO_DMA_ADDRESS_WIDTH - 3 : `RIO_DMA_ADDRESS_WIDTH - 1]; 
        if (dest_Offset !== 3'b000)
        begin
            // Temporary: misaligned accesses are supported only partly in this version,
            // so only DW-aligned and equally-misaligned DMA requests are currently allowed.
            dest_Offset = src_Offset;
            // "dar" must be DW-aligned
            dar[`RIO_DMA_ADDRESS_WIDTH - 1] = 3'b000;
        end
        
        tempReadByteCount = 0;
        queue_Remainder = 0;
        cur_Offset = 0;
        cur_Remainder = 0;
        cur_Index = 0;
        cur_Byte_Nums[0] = `RIO_BYTES_IN_DW;
        cur_Byte_Nums[1] = 0;
        cur_Byte_Nums[2] = 0;
        cur_Byte_Nums[3] = 0;

        $display("DEMO DMA PE %d (time %7t) : DMA transfer started!", inst_ID, $time);

        -> startTransfer;
    end
end

// DMA read branch: reading from source memory area to DMA queue
always @(startTransfer)               
begin : RIO_DMA_READ_PROCESS
    while (bcr !== 0 && msr[`RIO_DMA_BUSY_BIT_NUMBER] !== 1'b0)
    begin
//$display("DMA:1 DMA READ cycle start");
    
        wait ((msr[`RIO_DMA_CS_BIT_NUMBER] === 1'b1) && (queueWritePos > 0));
//$display("DMA:2 DMA READ cycle start");
        if (bcr < (`RIO_DMA_QUEUE_WIDTH * `RIO_WORDS_IN_DW / `RIO_BITS_IN_BYTE))
        begin
            tempReadByteCount = bcr;            
            queue_Remainder = (`RIO_DMA_QUEUE_WIDTH * `RIO_WORDS_IN_DW / `RIO_BITS_IN_BYTE) - bcr; 
        end
        else
            tempReadByteCount = `RIO_DMA_QUEUE_WIDTH * `RIO_WORDS_IN_DW / `RIO_BITS_IN_BYTE;

        queue_Offset = src_Offset;
        
        tmp_Trx_Tag_1 = cur_IO_Trx_Tag;
        while ($HW_Lg_IO_Request(inst_ID,
            `RIO_IO_NREAD,
            1, /* priority */
            sar, /* address */
            0, /* ext. address */
            0, /* xamsbs */
            1, /* dw_Size */
            0, /* offset */
            `RIO_BYTES_IN_DW, /* byte_Num */
            tmp_Trx_Tag_1, /* trx_Tag */
            `RIO_FALSE /* data_flag */
            ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);     

//$display("DMA: cur_IO_Trx_Tag = %b", cur_IO_Trx_Tag);

        cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

        wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_1));

//$display("DMA: DMA READ: cycle: Request done");

        if (pli_IO_Result_Reg === `RIO_REQ_ERROR)
        begin
            $display("DEMO ERROR: DMA PE %d: DMA READ cycle (time %7t) error", inst_ID, $time);
            msr[`RIO_DMA_ERR_BIT_NUMBER] = 1'b1;
        end
        else
        begin    
            // if all OK:
            //queueByteCount[queueWritePos] = tempReadByteCount;
            queue_MS[queueWritePos] = $HW_Get_PLI_MS_Word(inst_ID, `RIO_IO_DATA, 0);
            queue_LS[queueWritePos] = $HW_Get_PLI_LS_Word(inst_ID, `RIO_IO_DATA, 0);

            $display("DMA: DMA READ: current: SAR = h%h, queuePos = %d, MSW = h%h, LSW = h%h",
                     sar, queueWritePos, queue_MS[queueWritePos], queue_LS[queueWritePos]);

            bcr = bcr - tempReadByteCount;
            sar = sar + `RIO_BYTES_IN_DW; 
            queueWritePos = queueWritePos - 1;

        end
        pli_IO_Req_Done_Flag_Reg = 1'b0;
        src_Offset = 0;
//$display("end DMA read");

    end
end // RIO_DMA_READ_PROCESS

// DMA write branch: writing from DMA queue to destination memory area
always @(startTransfer)               
begin : RIO_DMA_WRITE_PROCESS
    while (msr[`RIO_DMA_BUSY_BIT_NUMBER] !== 1'b0)
    begin
//$display("DMA:1 DMA WRITE cycle start");

        wait (msr[`RIO_DMA_CS_BIT_NUMBER] === 1'b1 && queueWritePos < `RIO_DMA_QUEUE_LENGTH);

//$display("DMA:2 DMA WRITE cycle start");
        if (dest_Offset !== 0)
        begin
//            $display("DMA: DMA WRITE: dst_offset != 0... NO NEED!");
            cur_Offset = dest_Offset;
            cur_Index = 1;
            dest_Offset = 0;
            
            case (`RIO_BYTES_IN_DW - cur_Offset)  // total byte num
                1 : begin
                        cur_Byte_Nums[1] = 1;  
                        cur_Byte_Nums[2] = 0;  
                        cur_Byte_Nums[3] = 0;  
                    end

                2 : begin
                        cur_Byte_Nums[1] = 2;  
                        cur_Byte_Nums[2] = 0;  
                        cur_Byte_Nums[3] = 0;  
                    end

                3 : begin
                        cur_Byte_Nums[1] = 1;  
                        cur_Byte_Nums[2] = 2;  
                        cur_Byte_Nums[3] = 0;  
                    end

                4 : begin
                        cur_Byte_Nums[1] = 4;  
                        cur_Byte_Nums[2] = 0;  
                        cur_Byte_Nums[3] = 0;  
                    end

                5 : begin
                        cur_Byte_Nums[1] = 1;  
                        cur_Byte_Nums[2] = 4;  
                        cur_Byte_Nums[3] = 0;  
                    end

                6 : begin
                        cur_Byte_Nums[1] = 2;  
                        cur_Byte_Nums[2] = 4;  
                        cur_Byte_Nums[3] = 0;  
                    end

                7 : begin
                        cur_Byte_Nums[1] = 1;  
                        cur_Byte_Nums[2] = 2;  
                        cur_Byte_Nums[3] = 4;  
                    end
            endcase 
        end

        if ((queueWritePos === (`RIO_DMA_QUEUE_LENGTH - 1)) && (queue_Remainder !== 0)) 
        begin
//            $display("DMA: DMA WRITE: queue_Remainder != 0... NO NEED!");

            cur_Remainder = queue_Remainder;
            cur_Offset = 0;
            cur_Index = 1;
            queue_Remainder = 0;
            
            case (`RIO_BYTES_IN_DW - cur_Remainder)  // total byte num
                1 : begin
                        cur_Byte_Nums[1] = 1;  
                        cur_Byte_Nums[2] = 0;  
                        cur_Byte_Nums[3] = 0;  
                    end

                2 : begin
                        cur_Byte_Nums[1] = 2;  
                        cur_Byte_Nums[2] = 0;  
                        cur_Byte_Nums[3] = 0;  
                    end

                3 : begin
                        cur_Byte_Nums[1] = 2;  
                        cur_Byte_Nums[2] = 1;  
                        cur_Byte_Nums[3] = 0;  
                    end

                4 : begin
                        cur_Byte_Nums[1] = 4;  
                        cur_Byte_Nums[2] = 0;  
                        cur_Byte_Nums[3] = 0;  
                    end

                5 : begin
                        cur_Byte_Nums[1] = 4;  
                        cur_Byte_Nums[2] = 1;  
                        cur_Byte_Nums[3] = 0;  
                    end

                6 : begin
                        cur_Byte_Nums[1] = 4;  
                        cur_Byte_Nums[2] = 2;  
                        cur_Byte_Nums[3] = 0;  
                    end

                7 : begin
                        cur_Byte_Nums[1] = 4;  
                        cur_Byte_Nums[2] = 2;  
                        cur_Byte_Nums[3] = 1;  
                    end
            endcase 
        end

        if ((cur_Index > 0) && (cur_Byte_Nums[cur_Index] === 0))
        begin
//            $display("DMA: DMA WRITE: cur_Index != 0... NO NEED!");
            cur_Index = 0;
            cur_Offset = 0;
        end
        else
        begin
//            $display("DMA: DMA WRITE: current: DAR = h%h, BCR = %d, MSW = h%h, LSW = h%h",
//                 dar, bcr, queue_MS[`RIO_DMA_QUEUE_LENGTH], queue_LS[`RIO_DMA_QUEUE_LENGTH]);

            tmp_Trx_Tag_2 = cur_IO_Trx_Tag;
            while ($HW_Lg_IO_Request(inst_ID,
                `RIO_IO_NWRITE_R,
                1, /* priority */
                dar, /* address */
                0, /* ext. address */
                0, /* xamsbs */
                1, /* dw_Size */
                cur_Offset, /* offset */
                cur_Byte_Nums[cur_Index], /* byte num */
                tmp_Trx_Tag_2, /* trx_Tag */
                `RIO_TRUE, /* data_flag */
                queue_MS[`RIO_DMA_QUEUE_LENGTH], /* data MS */
                queue_LS[`RIO_DMA_QUEUE_LENGTH], /* data LS */
                ) == `RIO_RETRY)
                #(`RIO_WAIT_BEFORE_RETRY);     

//            $display("DMA: cur_IO_Trx_Tag = %b", cur_IO_Trx_Tag);

            cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

            if (cur_Index === 3)
            begin
//                $display("DMA: DMA WRITE: cur_Index == 3... NO NEED!");
                cur_Index = 0;
                cur_Offset = 0;
            end
            else if (cur_Index > 0)
            begin
//                $display("DMA: DMA WRITE: cur_Index != 0... NO NEED!");
                cur_Offset = cur_Offset + cur_Byte_Nums[cur_Index];
                cur_Index = cur_Index + 1;
            end    

            wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_2));
        
//        $display("DMA: DMA WRITE: cycle: Request done");
            
            if (pli_IO_Result_Reg === `RIO_REQ_ERROR)
            begin
                $display("DEMO ERROR: DMA PE %d (time %7t) : DMA WRITE cycle error", inst_ID, $time);
                msr[`RIO_DMA_ERR_BIT_NUMBER] = 1'b1;
            end
            pli_IO_Req_Done_Flag_Reg = 1'b0;
        end
//        $display("DMA: WRITE: Now Queue will be shifted: current queueWritePos: %d", queueWritePos);
        ShiftQueue;
//        $display("DMA: WRITE: Queue shifted: new queueWritePos: %d", queueWritePos);
        dar = dar + `RIO_BYTES_IN_DW;    /* byte num */
        if (bcr === 0 && queueWritePos === `RIO_DMA_QUEUE_LENGTH)  // no data remained to transfer
        begin
            $display("DEMO DMA PE %d (time %7t) : DMA TRANSFER DONE!", inst_ID, $time);
            msr[`RIO_DMA_FIN_BIT_NUMBER] = 1'b1;               
            disable RIO_DMA_WRITE_PROCESS;
        end
        dest_Offset = 0;
    end
end // RIO_DMA_WRITE_PROCESS

    integer db_Tr_Info;

// Transfer error handler
always @(posedge msr[`RIO_DMA_ERR_BIT_NUMBER])
begin
    msr[`RIO_DMA_BUSY_BIT_NUMBER] = 1'b0;
    if (msr[`RIO_DMA_EIE_BIT_NUMBER] === 1'b1)
    begin
        // !!! send doorbell message about DMA error
        db_Tr_Info = $HW_Get_Tr_Info(inst_ID, src_Inst_ID);
        while ($HW_Lg_Doorbell_Request(inst_ID,
            1, /* priority */
            db_Tr_Info, /* transp. info */
            `RIO_DMA_ERR_DB_INFO, /* db_info */
            cur_DB_Trx_Tag
            ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
        cur_DB_Trx_Tag = cur_DB_Trx_Tag + 1;

        ClearQueue;
    end
end

// Transfer finish handler
always @(posedge msr[`RIO_DMA_FIN_BIT_NUMBER])
begin
    msr[`RIO_DMA_BUSY_BIT_NUMBER] = 1'b0;
    if (msr[`RIO_DMA_EOTIE_BIT_NUMBER] === 1'b1)
    begin
        // !!! send doorbell message about DMA finish
        db_Tr_Info = $HW_Get_Tr_Info(inst_ID, src_Inst_ID);
        while ($HW_Lg_Doorbell_Request(inst_ID,
            1, /* priority */
            db_Tr_Info, /* transp. info */
            `RIO_DMA_FIN_DB_INFO, /* db_info */
            cur_DB_Trx_Tag
            ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
        cur_DB_Trx_Tag = cur_DB_Trx_Tag + 1;
    end
end

//---------------------------------------------------------------------------------------
//                                      DEMO SCENARIOS
//---------------------------------------------------------------------------------------

    integer num_Of_Bytes_In_Packet;

    integer index_Packet;
    integer index_DW;

    integer cur_Value;

    integer cur_Tr_Info;

// ---------------------------  SIMPLE DEMO SCENARIOS  ----------------------------

// Simple scenario 1
always @(posedge demo_Process_Scenario_1)   // Passing message to PE1
begin
    $display("DEMO (time %7t) : Scenario 1 of PE %d (DMA PE) started", $time, inst_ID);

    num_Of_Bytes_In_Packet = `RIO_NUM_OF_DW_IN_PACKET * `RIO_BYTES_IN_DW;

    for (index_Packet = 0; index_Packet <= `RIO_NUM_OF_PACKETS_IN_MSG - 1; index_Packet = index_Packet + 1)
    begin
        for (index_DW = 0; index_DW <= `RIO_NUM_OF_DW_IN_PACKET - 1; index_DW = index_DW + 1)
        begin
            cur_Value = index_Packet * `RIO_NUM_OF_DW_IN_PACKET + index_DW;
            $HW_Set_PLI_DW(inst_ID, `RIO_GENERAL_DATA, index_DW, (3'b010 << 28) + (cur_Value << 15) + 3'b010,
                (3'b101 << 28) + (cur_Value << 15) + 3'b101);
        end

        cur_Tr_Info = $HW_Get_Tr_Info(inst_ID, 1);
        while ($HW_Lg_Message_Request(
            inst_ID, /*handle*/
            1, /*prio*/
            cur_Tr_Info, /*transport_Info*/
            `RIO_NUM_OF_PACKETS_IN_MSG - 1, /*msglen = num of packets except the last one*/
            `RIO_NUM_OF_DW_IN_PACKET, /*ssize*/
            `RIO_LETTER_NUMBER, /*letter*/
            `RIO_MB_NUMBER, /*mbox*/
            `RIO_NUM_OF_PACKETS_IN_MSG - 1 - index_Packet, /*msgseg*/
            `RIO_NUM_OF_DW_IN_PACKET, /*dw_Size*/
            cur_MP_Trx_Tag, /*trx_Tag*/
            `RIO_FALSE /*data_flag*/
            ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
        cur_MP_Trx_Tag = cur_MP_Trx_Tag + 1; 
    end        

    $display("DEMO (time %7t) : Scenario 1 of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_1 = 1'b0;
end

// Simple scenario 2
always @(posedge demo_Process_Scenario_2)  // Just for debugging, single IO write and read requests
begin
    $display("DEMO (time %7t) : Scenario 2 of PE %2d (DMA PE) started", $time, inst_ID);

    while ($HW_Lg_IO_Request(
        inst_ID, /*handle*/
        `RIO_IO_NWRITE, /*ttype*/
        1, /*prio*/
        'h110, /*address*/
        0, /*extended_Address*/
        0, /*xamsbs*/
        1, /*dw_Size*/
        0, /*offset*/
        8, /*byte_Num*/
        cur_IO_Trx_Tag, /*trx_Tag*/
        `RIO_TRUE, /*data_flag*/
        32'h01234567, /*MS_Word*/
        32'h89ABCDEF  /*LS_Word*/
        ) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

    $display("DEMO (time %7t) : PE %2d (DMA PE): normal write IO request was sent", $time, inst_ID);

    while ($HW_Lg_IO_Request(
        inst_ID, /*handle*/
        `RIO_IO_NREAD, /*ttype*/
        1, /*prio*/
        'h110, /*address*/
        0, /*extended_Address*/
        0, /*xamsbs*/
        1, /*dw_Size*/
        0, /*offset*/
        8, /*byte_Num*/,
        cur_IO_Trx_Tag, /*trx_Tag*/
        `RIO_FALSE /*data_flag*/
        ) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

    wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === cur_IO_Trx_Tag - 1));

    $display("DEMO: PE %2d (DMA PE): normal read: data received: MSW = %h, LSW = %h", inst_ID,
        $HW_Get_PLI_MS_Word(inst_ID, `RIO_IO_DATA, 0), $HW_Get_PLI_LS_Word(inst_ID, `RIO_IO_DATA, 0));

    pli_IO_Req_Done_Flag_Reg = 1'b0;

    $display("DEMO (time %7t) : Scenario 2 of PE %2d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_2 = 1'b0;

    #1000;
end

// Simple scenario 3
always @(posedge demo_Process_Scenario_3)
begin
    $display("DEMO (time %7t) : Scenario 3 of PE %d (DMA PE) started", $time, inst_ID);


    $display("DEMO (time %7t) : Scenario 3 of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_3 = 1'b0;
end

// Simple scenario 4
always @(posedge demo_Process_Scenario_4)
begin
    $display("DEMO (time %7t) : Scenario 4 of PE %d (DMA PE) started", $time, inst_ID);


    $display("DEMO (time %7t) : Scenario 4 of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_4 = 1'b0;
end

// Simple scenario 5
always @(posedge demo_Process_Scenario_5)
begin
    $display("DEMO (time %7t) : Scenario 5 of PE %d (DMA PE) started", $time, inst_ID);


    $display("DEMO (time %7t) : Scenario 5 of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_5 = 1'b0;
end

// ---------------------------  SWITCH-BASED DEMO SCENARIOS  ----------------------------

    integer arg_Index_1S;
    integer arg_Index_2S;
    integer arg_Index_3S;
    integer gran_Index_1S;
    integer gran_Index_2S;
    integer gran_Index_3S;
    integer dw_Index_1S;
    integer dw_Index_2S;
    integer dw_Index_3S;
    integer num_Of_DW_In_Granule;
    reg [0 : `RIO_DMA_ADDRESS_WIDTH - 1] cur_IO_Address;    
    reg [0 : `RIO_DMA_ADDRESS_WIDTH - 1] cur_GSM_Address;    

    integer tmp_Trx_Tag_1S;
    integer tmp_Trx_Tag_2S;
    integer tmp_Trx_Tag_2GS;
    integer tmp_Trx_Tag_3S;
    integer tmp_Trx_Tag_3GS;
    integer tmp_Trx_Tag_4S;

    integer gsm_Result_2;
    integer gsm_Result_3;
    reg gsm_Hit_2;
    reg gsm_Hit_3;
    integer gsm_Cache_Index_2;
    integer gsm_Cache_Index_3;
    
// Switch-based scenario 1
always @(posedge demo_Process_Scenario_1S)
begin
    $display("DEMO (time %7t) : Scenario 1S of PE %d (DMA PE) started", $time, inst_ID);

    $display("DEMO (time %7t) : PE %d: DMA Reset", $time, inst_ID);
    ResetDMA;
    
    $display("DEMO (time %7t) : PE %d: Config request: Write DMA Src_Tr_Info:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_SRC_INST_ID_ADDR + `RIO_DMA_SRC_INST_ID_SUB_DW * `RIO_BYTES_IN_WORD, /*config_Offset*/
        inst_ID /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA SAR:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_SAR_ADDR + `RIO_DMA_SAR_SUB_DW * `RIO_BYTES_IN_WORD, /*config_Offset*/
        `RIO_DEMO_DRIVE_IO_ADDRESS /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA DAR:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_DAR_ADDR + `RIO_DMA_DAR_SUB_DW * `RIO_BYTES_IN_WORD, /*config_Offset*/
        `RIO_DEMO_HOST_IO_ADDRESS /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA BCR:", $time, inst_ID);
    num_Of_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;    
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_BCR_ADDR + `RIO_DMA_BCR_SUB_DW * `RIO_BYTES_IN_WORD, /*config_Offset*/
        (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1) * `RIO_DEMO_NUM_OF_GRANULES_IN_ARG *
        num_Of_DW_In_Granule * `RIO_BYTES_IN_DW /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA MSR:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_MSR_ADDR + `RIO_DMA_MSR_SUB_DW * `RIO_BYTES_IN_WORD, /*config_Offset*/
        8'b11110000 /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Now DMA shall start the transfer", $time, inst_ID);
    $display("DEMO (time %7t) : PE %d: Waiting for Doorbell arrival from DMA...", $time, inst_ID);

    wait (`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg === 1'b1);
    #0; 

    if (`RIO_MAILBOX_CONTROL.doorbell_Info == `RIO_DMA_FIN_DB_INFO)
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: Finished OK", $time, inst_ID);
    else if (`RIO_MAILBOX_CONTROL.doorbell_Info == `RIO_DMA_ERR_DB_INFO)
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: ERROR", $time, inst_ID);
    else
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: Unknown...: %b", $time, inst_ID, 
           `RIO_MAILBOX_CONTROL.doorbell_Info);

    `RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg = 1'b0;
    
    $display("DEMO (time %7t) : Scenario 1S of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_1S = 1'b0;
end

// Switch-based scenario 2
always @(posedge demo_Process_Scenario_2S)
begin
    // Read data from host IO memory and write it to "ARGi" and "RES" GSM memory spaces
    $display("DEMO (time %7t) : Scenario 2S of PE %d (DMA PE) started", $time, inst_ID);

    cur_IO_Address = `RIO_DEMO_HOST_IO_ADDRESS;
    num_Of_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;    

    for (arg_Index_2S = 1; arg_Index_2S <= (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1); arg_Index_2S = arg_Index_2S + 1)
    begin
        case (arg_Index_2S)
            1: cur_GSM_Address = `RIO_DEMO_ARG1_ADDRESS;

            2: cur_GSM_Address = `RIO_DEMO_ARG2_ADDRESS;

            3: cur_GSM_Address = `RIO_DEMO_ARG3_ADDRESS;

            4: cur_GSM_Address = `RIO_DEMO_RES_ADDRESS;
        endcase
    
        for (gran_Index_2S = 0; gran_Index_2S < `RIO_DEMO_NUM_OF_GRANULES_IN_ARG; gran_Index_2S = gran_Index_2S + 1)
        begin
            tmp_Trx_Tag_2GS = cur_GSM_Trx_Tag; 
            gsm_Result_2 = `RIO_RETRY;
            while (gsm_Result_2 == `RIO_RETRY)
            begin
                `RIO_CACHE_CONTROL.GSM_Request(
                    `RIO_GSM_READ_TO_OWN, /*ttype*/
                    1, /*prio*/
                    cur_GSM_Address, /*address*/
                    0, /*ext_Address*/
                    0, /*xamsbs*/
                    num_Of_DW_In_Granule, /*dw_Size*/
                    0, /*offset*/
                    0, /*byte_Num*/
                    `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
                    `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
                    tmp_Trx_Tag_2GS, /*trx_Tag*/
                    gsm_Result_2, /*result*/
                    gsm_Hit_2, /*data_Read*/
                    gsm_Cache_Index_2 /*line_Index*/
                    );
                if (gsm_Result_2 == `RIO_ERROR) 
                    $display("DEMO ERROR (time %7t) : Scenario 2S of PE %d: GSM read error", $time, inst_ID);
                if (gsm_Result_2 == `RIO_RETRY)
                    #(`RIO_WAIT_BEFORE_RETRY);    
            end        
            cur_GSM_Trx_Tag = cur_GSM_Trx_Tag + 1;

            //$display("DEMO (time %7t) : Scenario 2S of PE %d: GSM Request was received, NEXT!", $time, inst_ID);
        
            if (!gsm_Hit_2)
            begin
                wait ((pli_GSM_Req_Done_Flag_Mirror === 1'b1) && (pli_GSM_Trx_Tag_Mirror === tmp_Trx_Tag_2GS));
                $display("DEMO (time %7t) : Scenario 2S of PE %d: GSM request done", $time, inst_ID);
                gsm_Cache_Index_2 = `RIO_CACHE_CONTROL.new_GSM_Cache_Line_Index_Reg;            
                `RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg = 1'b0;
            end    
        
            // now we have a cache line with status "Modified GSM", its index = gsm_Cache_Index_2
            // so, read data from IO host memory and copy it to this cache line
    
            tmp_Trx_Tag_2S = cur_IO_Trx_Tag;
            while ($HW_Lg_IO_Request(
                inst_ID,
                `RIO_IO_NREAD,
                1, /* priority */
                cur_IO_Address, /* address */
                0, /* ext. address */
                0, /* xamsbs */
                num_Of_DW_In_Granule, /* dw_Size */
                0, /* offset */
                `RIO_BYTES_IN_DW, /* byte_Num */ 
                tmp_Trx_Tag_2S, /* trx_Tag */
                `RIO_FALSE /* data_flag */
                ) == `RIO_RETRY)
                #(`RIO_WAIT_BEFORE_RETRY);     
            cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

            wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_2S));
            
            // DATA COPYING to the cache line
            `RIO_CACHE_CONTROL.Set_Cache_Line(gsm_Cache_Index_2, `RIO_IO_DATA);
    
            cur_IO_Address = cur_IO_Address + num_Of_DW_In_Granule * `RIO_BYTES_IN_DW;    
            cur_GSM_Address = cur_GSM_Address + num_Of_DW_In_Granule * `RIO_BYTES_IN_DW;    
        end    
    end        
    
    $display("DEMO (time %7t) : Scenario 2S of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_2S = 1'b0;
end

// Switch-based scenario 3
always @(posedge demo_Process_Scenario_3S)
begin
    $display("DEMO (time %7t) : Scenario 3S of PE %d (DMA PE) started", $time, inst_ID);

    cur_IO_Address = `RIO_DEMO_HOST_IO_ADDRESS;
    num_Of_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;    

    for (arg_Index_3S = 1; arg_Index_3S <= (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1); arg_Index_3S = arg_Index_3S + 1)
    begin
        case (arg_Index_3S)
            1: cur_GSM_Address = `RIO_DEMO_ARG1_ADDRESS;

            2: cur_GSM_Address = `RIO_DEMO_ARG2_ADDRESS;

            3: cur_GSM_Address = `RIO_DEMO_ARG3_ADDRESS;

            4: cur_GSM_Address = `RIO_DEMO_RES_ADDRESS;
        endcase
    
        for (gran_Index_3S = 0; gran_Index_3S < `RIO_DEMO_NUM_OF_GRANULES_IN_ARG; gran_Index_3S = gran_Index_3S + 1)
        begin
            tmp_Trx_Tag_3GS = cur_GSM_Trx_Tag; 
            gsm_Result_3 = `RIO_RETRY;
            while (gsm_Result_3 == `RIO_RETRY)
            begin
                `RIO_CACHE_CONTROL.GSM_Request(
                    `RIO_GSM_READ_SHARED, /*ttype*/
                    1, /*prio*/
                    cur_GSM_Address, /*address*/
                    0, /*ext_Address*/
                    0, /*xamsbs*/
                    num_Of_DW_In_Granule, /*dw_Size*/
                    0, /*offset*/
                    0, /*byte_Num*/
                    `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
                    `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
                    tmp_Trx_Tag_3GS, /*trx_Tag*/
                    gsm_Result_3, /*result*/
                    gsm_Hit_3, /*data_Read*/
                    gsm_Cache_Index_3 /*line_Index*/
                    );
                if (gsm_Result_3 == `RIO_ERROR) 
                    $display("DEMO ERROR (time %7t) : Scenario 3S of PE %d: GSM read error", $time, inst_ID);
                if (gsm_Result_3 == `RIO_RETRY)
                    #(`RIO_WAIT_BEFORE_RETRY);    
            end        
            cur_GSM_Trx_Tag = cur_GSM_Trx_Tag + 1;

            //$display("DEMO (time %7t) : Scenario 3S of PE %d: GSM Request was received, NEXT!", $time, inst_ID);
        
            if (!gsm_Hit_3)
            begin
                wait ((pli_GSM_Req_Done_Flag_Mirror === 1'b1) && (pli_GSM_Trx_Tag_Mirror === tmp_Trx_Tag_3GS));
                $display("DEMO (time %7t) : Scenario 3S of PE %d: GSM request done", $time, inst_ID);
                gsm_Cache_Index_3 = `RIO_CACHE_CONTROL.new_GSM_Cache_Line_Index_Reg;            
                `RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg = 1'b0;
            end    
        
            // now we have a cache line with status "Shared GSM", its index = gsm_Cache_Index_3
    
            // DATA reading from the cache line
            `RIO_CACHE_CONTROL.Get_Cache_Line(gsm_Cache_Index_3, `RIO_GENERAL_DATA);
    
            tmp_Trx_Tag_3S = cur_IO_Trx_Tag;
            while ($HW_Lg_IO_Request(
                inst_ID,
                `RIO_IO_NWRITE,
                1, /* priority */
                cur_IO_Address, /* address */
                0, /* ext. address */
                0, /* xamsbs */
                num_Of_DW_In_Granule, /* dw_Size */
                0, /* offset */
                `RIO_BYTES_IN_DW, /* byte_Num */
                tmp_Trx_Tag_3S, /* trx_Tag */
                `RIO_FALSE /* data_flag */
                ) == `RIO_RETRY)
                #(`RIO_WAIT_BEFORE_RETRY);     
            cur_IO_Trx_Tag = cur_IO_Trx_Tag + 1;

            wait ((pli_IO_Req_Done_Flag_Reg === 1'b1) && (pli_IO_Trx_Tag_Reg === tmp_Trx_Tag_3S));
            
            cur_IO_Address = cur_IO_Address + num_Of_DW_In_Granule * `RIO_BYTES_IN_DW;    
            cur_GSM_Address = cur_GSM_Address + num_Of_DW_In_Granule * `RIO_BYTES_IN_DW;    
        end    
    end        

    $display("DEMO (time %7t) : Scenario 3S of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_3S = 1'b0;
end

// Switch-based scenario 4
always @(posedge demo_Process_Scenario_4S)
begin
    $display("DEMO (time %7t) : Scenario 4S of PE %d (DMA PE) started", $time, inst_ID);

    $display("DEMO (time %7t) : PE %d: DMA Reset", $time, inst_ID);
    ResetDMA;
    #1;
    $display("DEMO (time %7t) : PE %d: Config request: Write DMA Src_Tr_Info:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_SRC_INST_ID_ADDR + `RIO_DMA_SRC_INST_ID_SUB_DW * `RIO_BYTES_IN_WORD /*config_Offset*/,
        inst_ID /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA SAR:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_SAR_ADDR + `RIO_DMA_SAR_SUB_DW * `RIO_BYTES_IN_WORD /*config_Offset*/,
        `RIO_DEMO_HOST_IO_ADDRESS /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA DAR:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_DAR_ADDR + `RIO_DMA_DAR_SUB_DW * `RIO_BYTES_IN_WORD /*config_Offset*/,
        `RIO_DEMO_DRIVE_IO_ADDRESS /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA BCR:", $time, inst_ID);
    num_Of_DW_In_Granule = (`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE;    
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_BCR_ADDR + `RIO_DMA_BCR_SUB_DW * `RIO_BYTES_IN_WORD /*config_Offset*/,
        (`RIO_NUM_OF_SWITCH_DEMO_ARGS + 1) * `RIO_DEMO_NUM_OF_GRANULES_IN_ARG *
        num_Of_DW_In_Granule * `RIO_BYTES_IN_DW /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Config request: Write DMA MSR:", $time, inst_ID);
    $HW_Lg_Set_Config_Reg(
        inst_ID,
        `RIO_DMA_MSR_ADDR + `RIO_DMA_MSR_SUB_DW * `RIO_BYTES_IN_WORD /*config_Offset*/,
        8'b11110000 /* data word */ 
        );

    $display("DEMO (time %7t) : PE %d: Now DMA shall start the transfer", $time, inst_ID);
    $display("DEMO (time %7t) : PE %d: Waiting for Doorbell arrival from DMA...", $time, inst_ID);

    wait (`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg === 1'b1); 

    if (`RIO_MAILBOX_CONTROL.doorbell_Info == `RIO_DMA_FIN_DB_INFO)
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: Finished OK", $time, inst_ID);
    else if (`RIO_MAILBOX_CONTROL.doorbell_Info == `RIO_DMA_ERR_DB_INFO)
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: ERROR", $time, inst_ID);
    else
        $display("DEMO (time %7t) : PE %d: Doorbell from DMA just arrived! Result of DMA: Unknown...: %b", $time, inst_ID, 
           `RIO_MAILBOX_CONTROL.doorbell_Info);

    `RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg = 1'b0;
        
    $display("DEMO (time %7t) : Scenario 4S of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_4S = 1'b0;
end

// Switch-based scenario 5
always @(posedge demo_Process_Scenario_5S)
begin
    $display("DEMO (time %7t) : Scenario 5S of PE %d (DMA PE) started", $time, inst_ID);


    $display("DEMO (time %7t) : Scenario 5S of PE %d (DMA PE) finished", $time, inst_ID);
    demo_Process_Scenario_5S = 1'b0;
end

// -------------------------------------------------------------------------

/***************************************************************************
 * Task : ShiftQueue
 *
 * Description: Provides shift of the DMA queue and so releases one place
 *              in the end of this this queue
 *
 * Notes:
 *
 **************************************************************************/
task ShiftQueue;
    integer k;
begin
    if (queueWritePos != `RIO_DMA_QUEUE_LENGTH)  // nothing to do if the queue is empty
    begin
        for (k = `RIO_DMA_QUEUE_LENGTH; k > queueWritePos + 1; k = k - 1)
        begin
            queue_MS[k] = queue_MS[k - 1];
            queue_LS[k] = queue_LS[k - 1];
        end
        queue_MS[queueWritePos + 1] = {(`RIO_DMA_QUEUE_WIDTH - 1){1'b0}};
        queue_LS[queueWritePos + 1] = {(`RIO_DMA_QUEUE_WIDTH - 1){1'b0}};
        queueWritePos = queueWritePos + 1;        
    end
end
endtask    

/***************************************************************************
 * Task : ClearQueue
 *
 * Description: Removes all data from the DMA queue and makes it empty
 *
 * Notes:
 *
 **************************************************************************/
task ClearQueue;
    integer k;
begin
    if (queueWritePos != `RIO_DMA_QUEUE_LENGTH)  // nothing to do if the queue is empty
    begin
        for (k = `RIO_DMA_QUEUE_LENGTH; k >= queueWritePos + 1; k = k - 1)
        begin
            queue_MS[k] = 0;
            queue_LS[k] = 0;
        end
        queueWritePos = `RIO_DMA_QUEUE_LENGTH;   // Now the queue is empty      
    end
end
endtask    

/***************************************************************************
 * Task : ResetDMA
 *
 * Description: Sets DMA data to initial values
 *
 * Notes: Does nothing if DMA is busy (transfer in progress)
 *
 **************************************************************************/
task ResetDMA;
    integer i;
begin
    if (msr[`RIO_DMA_BUSY_BIT_NUMBER] === 1'b0)
    begin
        for (i = 1; i <= `RIO_DMA_QUEUE_LENGTH; i = i + 1)
        begin
            queue_MS[i] = {(`RIO_DMA_QUEUE_WIDTH - 1){1'b0}};
            queue_LS[i] = {(`RIO_DMA_QUEUE_WIDTH - 1){1'b0}};
        end
        msr = 8'b0;  
        sar = {(`RIO_DMA_ADDRESS_WIDTH - 1){1'b0}};
        dar = {(`RIO_DMA_ADDRESS_WIDTH - 1){1'b0}};
        bcr = {(`RIO_DMA_ADDRESS_WIDTH - 1){1'b0}};

        queueWritePos = `RIO_DMA_QUEUE_LENGTH;    // queue is empty
$display("dma reset at time = %t", $time);        
    end
end
endtask

endmodule

/*****************************************************************************/
