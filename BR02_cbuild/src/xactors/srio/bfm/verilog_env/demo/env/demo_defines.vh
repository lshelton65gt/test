/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/demo_defines.vh,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.1 $ 
*
* History:      Use the CVS command log to display revision history
*               information.
*               
* Description:  Defines macronames used in demo environment models and
*               demo scenario and allowed to be changed by user
*
* Notes:       
*
******************************************************************************/

//------------------------------------------------------------------------
//               Names of Demo modules and its instances
//------------------------------------------------------------------------

// modules

`define RIO_CACHE_CONTROL_MODULE            Cache_Control
`define RIO_MAILBOX_CONTROL_MODULE          Mailbox_Control
`define RIO_MEMORY_CONTROL_MODULE           Memory_Control
`define RIO_PE_MODULE                       Processing_Element   
`define RIO_DMA_PE_MODULE                   DMA   
`define RIO_SWITCH_MODULE                   Switch   
`define RIO_TOP_MODULE                      Top
`define RIO_TOP_2_MODULE                    Top_2

// instances

`define RIO_CACHE_CONTROL                   cache_Control
`define RIO_MAILBOX_CONTROL                 mailbox_Control
`define RIO_MEMORY_CONTROL                  memory_Control
`define RIO_PE_1                            pe_1   
`define RIO_PE_2                            pe_2   
`define RIO_PE_3                            pe_3   
`define RIO_PE_4                            pe_4   
`define RIO_PE_5                            pe_5   
`define RIO_DMA_PE                          pe_DMA   
`define RIO_SWITCH_1                        switch_1   
`define RIO_SWITCH_2                        switch_2   

// Maximum number of instances

`define RIO_MAX_NUM_OF_PE                   256
`define RIO_MAX_NUM_OF_SWITCHES             16 

//------------------------------------------------------------------------
//            Demo instantiation and initialization settings              
//------------------------------------------------------------------------

// parameters for RIO PE module creation
`define RIO_MAX_TIME_OUT                    32'hFFFFFFFF
`define RIO_MAX_RETRY                       32'hFFFFFFFF
`define RIO_MAX_OP_SIZE                     32'hFFCEFFFF
`define RIO_MAX_MP_OP_SIZE                  32'hEEEEFFFF
`define RIO_TRX_ALL                         32'hFFFFFFF0
`define RIO_TRX_NOGSM                       32'h003FFFF0

`define RIO_TRX_NO_GSM                      32'h0000FFF0
`define RIO_RES_FOR_OUT                     1
`define RIO_RES_FOR_IN                      1
`define RIO_IN_BUF_SIZE                     8
`define RIO_OUT_BUF_SIZE                    8

// port encoding size in index- and shift-based routing
`define RIO_PORT_ENC_SIZE                   3

// device (PE) identifiers
`define RIO_DEV_ID_PE1                      1
`define RIO_DEV_ID_PE2                      2
`define RIO_DEV_ID_PE3                      3
`define RIO_DEV_ID_PE4                      4
`define RIO_DEV_ID_PE5                      5

`define RIO_TR_INFO_16                      16
`define RIO_TR_INFO_32                      32
// Transport info size, can be only 16 or 32 bits
`define RIO_TR_INFO_SIZE                    `RIO_TR_INFO_16

// Coherency granule size. If true - 64 bytes (8 DW-s), if false - 32 bytes (4 DW-s)
`define RIO_COH_GRANULE_SIZE_IS_64          `RIO_FALSE

//Training parameters
`define	RIO_REQ_WINDOW_ALIGN				`RIO_FALSE
`define RIO_NUM_TRANINGS					1

//Operating mode parameter
`define RIO_OPERATING_MODE					`RIO_TRUE

//------------------------------------------------------------------------
//                  Address configuration settings              
//------------------------------------------------------------------------

// Allowed full address sizes:
// Possible values:
//   `RIO_PE_SUPPORTS_66_50_34_ADDR      
//   `RIO_PE_SUPPORTS_66_34_ADDR         
//   `RIO_PE_SUPPORTS_50_34_ADDR         
//   `RIO_PE_SUPPORTS_34_ADDR            

`define RIO_ADDRESS_SUPPORT_CODE            `RIO_PE_SUPPORTS_66_50_34_ADDR
// NOTE: The following three parameters shall be in accordance with the `RIO_ADDRESS_SUPPORT_CODE value:
// Extended address enabled/disabled: true - enabled, false - disabled 
`define RIO_EXT_ADDRESS_SUPPORT             `RIO_TRUE
// Extended address size (if enabled): true - 16 bits, false - 32 bits
`define RIO_EXT_ADDRESS_SIZE_SELECTOR       `RIO_FALSE

//------------------------------------------------------------------------
//                               Timing
//------------------------------------------------------------------------

// Clock
`define RIO_HALF_CLOCK_PERIOD               10

// RIO model clock rate, can be single or double. Double rate is the default now.

// single data rate is not supported by the specification; Model is defaulted to
// double data rate
//`define RIO_CLOCK_RATE                      `RIO_DOUBLE_RATE

// Wait time before the request must be retried
`define RIO_WAIT_BEFORE_RETRY               10

// Execution time limits
`define RIO_EMERGENCY_TIME_SIMPLE           500000
`define RIO_EMERGENCY_TIME_SWITCH           1000000

//------------------------------------------------------------------------
//                           Memory map
//------------------------------------------------------------------------

`define RIO_LOCAL_MEMORY_SPACE_DW_SIZE      2000   /*Size of array for memory realization*/

/*Constants for mapping memory*/
`define RIO_MEM_IO_XAMSBS_1                 0
`define RIO_MEM_IO_EXT_ADDRR_1              0
`define RIO_MEM_IO_START_ADDRR_1            'h1000
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_IO_DW_SIZE_1                1500

`define RIO_MEM_GSM_XAMSBS_1                0
`define RIO_MEM_GSM_EXT_ADDRR_1             0
`define RIO_MEM_GSM_START_ADDRR_1           'h100000
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_GSM_DW_SIZE_1               1000

`define RIO_MEM_IO_XAMSBS_2                 0
`define RIO_MEM_IO_EXT_ADDRR_2              0
`define RIO_MEM_IO_START_ADDRR_2            'h4000
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_IO_DW_SIZE_2                1000

`define RIO_MEM_GSM_XAMSBS_2                0
`define RIO_MEM_GSM_EXT_ADDRR_2             0
`define RIO_MEM_GSM_START_ADDRR_2           'h200000
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_GSM_DW_SIZE_2               1000

`define RIO_MEM_IO_XAMSBS_3                 0
`define RIO_MEM_IO_EXT_ADDRR_3              0
`define RIO_MEM_IO_START_ADDRR_3            'h0
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_IO_DW_SIZE_3                0

`define RIO_MEM_GSM_XAMSBS_3                0
`define RIO_MEM_GSM_EXT_ADDRR_3             0
`define RIO_MEM_GSM_START_ADDRR_3           'h0
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_GSM_DW_SIZE_3               0

`define RIO_MEM_IO_XAMSBS_4                 0
`define RIO_MEM_IO_EXT_ADDRR_4              0
`define RIO_MEM_IO_START_ADDRR_4            'h0
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_IO_DW_SIZE_4                0

`define RIO_MEM_GSM_XAMSBS_4                0
`define RIO_MEM_GSM_EXT_ADDRR_4             0
`define RIO_MEM_GSM_START_ADDRR_4           'h0
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_GSM_DW_SIZE_4               0

`define RIO_MEM_IO_XAMSBS_5                 0
`define RIO_MEM_IO_EXT_ADDRR_5              0
`define RIO_MEM_IO_START_ADDRR_5            'h25000
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_IO_DW_SIZE_5                2000

`define RIO_MEM_GSM_XAMSBS_5                0
`define RIO_MEM_GSM_EXT_ADDRR_5             0
`define RIO_MEM_GSM_START_ADDRR_5           'h0
// Shall not exceed `RIO_LOCAL_MEMORY_SPACE_DW_SIZE value:
`define RIO_MEM_GSM_DW_SIZE_5               0

//------------------------------------------------------------------------
//                     Demo scenarios settings              
//------------------------------------------------------------------------

// If true, switch-based demo scenario will make a pause on the each step
`define RIO_DEMO_MAKE_PAUSES                `RIO_TRUE

// If true, simple scenario (instead of the switch-based one)
// will be executed on the switch-based demo application 
`define RIO_USE_SIMPLE_SCENARIO_IN_SWITCH   `RIO_FALSE

// Doorbell's info for DMA' doorbells about DMA transfer finish or error 

`define RIO_DMA_ERR_DB_INFO                 `RIO_DB_INFO_SIZE'b111000 
`define RIO_DMA_FIN_DB_INFO                 `RIO_DB_INFO_SIZE'b000111 

// Simple scenario: DMA address settings

`define RIO_DEMO_DMA_SRC_ADDRESS            `RIO_MEM_IO_START_ADDRR_1 + 'h1000
`define RIO_DEMO_DMA_DEST_ADDRESS           `RIO_MEM_IO_START_ADDRR_1 + 'h2000
`define RIO_DEMO_DMA_DW_SIZE                10

`define RIO_DEMO_DMA_ALT_ADDRESS            `RIO_MEM_IO_START_ADDRR_2 + 'h10
`define RIO_DEMO_DMA_ALT_DW_SIZE            10

// Simple scenario: Initialization message settings

/* 1 ... 16 */
`define RIO_NUM_OF_PACKETS_IN_MSG           15
/* 1, 2, 4, 8, 16 or 32 */
`define RIO_NUM_OF_DW_IN_PACKET             ((`RIO_COH_GRANULE_SIZE_IS_64) ? `RIO_LARGE_GRAN_DW_SIZE : `RIO_SMALL_GRAN_DW_SIZE)
/* 0, 1, 2 or 3 */
`define RIO_MB_NUMBER                       1         
/* 0, 1, 2 or 3 */
`define RIO_LETTER_NUMBER                   2         

// Switch-based demo settings

`define RIO_DEMO_NUM_OF_GRANULES_IN_ARG     3

`define RIO_DEMO_ARG1_ADDRESS               `RIO_MEM_GSM_START_ADDRR_1 + 'h100               
`define RIO_DEMO_ARG2_ADDRESS               `RIO_MEM_GSM_START_ADDRR_1 + 'h600               
`define RIO_DEMO_ARG3_ADDRESS               `RIO_MEM_GSM_START_ADDRR_2 + 'h100               
`define RIO_DEMO_RES_ADDRESS                `RIO_MEM_GSM_START_ADDRR_2 + 'h600               

// Temporary buffer in IO memory space of the host PE
`define RIO_DEMO_HOST_IO_ADDRESS            `RIO_MEM_IO_START_ADDRR_2 + 'h100               
// "Storage" unit memory location
`define RIO_DEMO_DRIVE_IO_ADDRESS           `RIO_MEM_IO_START_ADDRR_5 + 'h100

// Transaction tags base values (main rule - avoid any overlaps in the tags ranges) 

`define RIO_CONFIG_TRX_BASE_TAG             'b001_00_000_00000000_00000000_00000000
`define RIO_IO_TRX_BASE_TAG                 'b010_00_000_00000000_00000000_00000000
`define RIO_GSM_TRX_BASE_TAG                'b100_00_000_00000000_00000000_00000000
`define RIO_CC_GSM_TRX_BASE_TAG             'b100_00_001_00000000_00000000_00000000

`define RIO_DB_TRX_BASE_TAG                 'b001_01_000_00000000_00000000_00000000
`define RIO_MP_TRX_BASE_TAG                 'b010_01_000_00000000_00000000_00000000
`define RIO_DMA_DB_TRX_TAG_1                'b001_01_001_00000000_00000000_00000001
`define RIO_DMA_DB_TRX_TAG_2                'b001_01_010_00000000_00000000_00000010

`define RIO_TRX_TAG_PE_OFFSET               'b000_00_000_00000001_00000000_00000000

`define RIO_TRX_BASE_TAG_MASK               'b111_11_111_00000000_00000000_00000000
`define RIO_TRX_TAG_TAIL_MASK               'b000_00_000_11111111_11111111_11111111
