/******************************************************************************
*
*       COPYRIGHT 1999-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* Filename:     $Source: /cvs/repository/src/xactors/srio/bfm/verilog_env/demo/env/top.v,v $
* Author:       $Author: knutson $ 
* Locker:       $Locker:  $
* State:        $State: Exp $
* Revision:     $Revision: 1.2 $ 
*
* History:      Use the CVS command log to display revision history
*                information.
*               
* Description:  Top module for RIO Models simple demo application
*            
* Notes:        
*
******************************************************************************/

`timescale 1ns/1ns
`include "env_defines.vh"
`include "demo_defines.vh"

module `RIO_TOP_MODULE;

// registers and wires

    // clock
    reg sys_CLK;

    // LP-EP link
    wire tclk;
    wire tframe;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;
    wire rclk;    
    wire rframe;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    `RIO_PE_MODULE #(1, 3'b111) `RIO_PE_1    
        (
        .clock(sys_CLK),
        .tclk(tclk), 
        .tframe(tframe),
        .td(td), 
        .tdl(tdl), 
        .rclk(rclk), 
        .rframe(rframe), 
        .rd(rd), 
        .rdl(rdl)
        );

    defparam `RIO_PE_1.`RIO_CACHE_CONTROL.coh_Granule_Is_64 = `RIO_COH_GRANULE_SIZE_IS_64;
    defparam `RIO_PE_1.`RIO_MAILBOX_CONTROL.hw_Available = 5'b1111_1; // Mailboxes 0, 1, 2, 3 and doorbell are on

    `RIO_DMA_PE_MODULE #(2, 3'b111) `RIO_DMA_PE    
        (
        .clock(sys_CLK),
        .tclk(rclk), 
        .tframe(rframe),
        .td(rd), 
        .tdl(rdl), 
        .rclk(tclk), 
        .rframe(tframe), 
        .rd(td), 
        .rdl(tdl)
        );

    defparam `RIO_DMA_PE.`RIO_CACHE_CONTROL.coh_Granule_Is_64 = `RIO_COH_GRANULE_SIZE_IS_64;
    defparam `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.hw_Available = 5'b1111_1; // Mailboxes 0, 1, 2, 3 and doorbell are on

// Use this register instead of non-implemented registers to check access to them 
    reg [0 : `RIO_PLI_LONG_REGISTER_SIZE - 1] pli_EMPTY_Reg;

// Loop counter
    integer i;
  
/* ********************************************************************* */

// clock and reset control
initial 
begin
//    $display("clk");
    sys_CLK = 0;
    #(`RIO_EMERGENCY_TIME_SIMPLE) $finish;   // emergency finish to avoid infinite simulation if any errors
end

// clock generator
always 
begin 
//    $display("TIC-TAC!");
    #(`RIO_HALF_CLOCK_PERIOD) sys_CLK = ~sys_CLK;
end

// -------------------------------------------------------------

// initialization
initial pli_EMPTY_Reg = 0;

// "EMPTY" register access monitor
always
begin
    wait (pli_EMPTY_Reg !== 0);
    pli_EMPTY_Reg = 0;
    $display("DEMO WARNING (time %7t) : Attempt to write into non-implemented register detected.", $time);
end

// -------------------------------------------------------------

// initialization and simple scenario running
initial
begin 
//    $display("1");

// Initialize PLI wrapper
    $HW_Init(2 /* Total number of PE */ , 0 /* Total number of switches */);

// Register PE instances

//    $display("2");

    $HW_Register_Instance(1, `RIO_PE_1,
    /* parameters: */
    `RIO_ADDRESS_SUPPORT_CODE, /*ext_Address_Support code*/
    `RIO_FALSE, /*is_Bridge*/
    `RIO_TRUE, /*has_Memory*/
    `RIO_TRUE, /*has_Processor*/
    ~`RIO_COH_GRANULE_SIZE_IS_64, /*coh_Granule_Size_32*/
    `RIO_TRX_ALL, /*source_Trx*/
    `RIO_TRX_ALL, /*dest_Trx*/ 
    `RIO_TRUE, /*mbox1*/
    `RIO_TRUE, /*mbox2*/
    `RIO_TRUE, /*mbox3*/
    `RIO_TRUE, /*mbox4*/
    `RIO_TRUE, /*has_Doorbell*/
    `RIO_IN_BUF_SIZE, /*pl_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*pl_Outbound_Buffer_Size*/
    `RIO_IN_BUF_SIZE, /*ll_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*ll_Outbound_Buffer_Size*/
    2, /*coh_Domain_Size*/

/*---------------------------------------------------------*/
/*New 10 parameters*/
    0, //device_Identity
    0, //device_Vendor_Identity
    0, //device_Rev
    0, //device_Minor_Rev
    0, //device_Major_Rev
    0, //assy_Identity
    0, //assy_Vendor_Identity
    0, //assy_Rev
	'h100, //entry_Extended_Features_Ptr /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	'h1000, //next_Extended_Features_Ptr /*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
/*End of new 10 parameters*/
/*---------------------------------------------------------*/


    /* PLI registers: */
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Result_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Loc_Response_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg,
    `RIO_PE_1.pli_IO_Trx_Tag_Reg, 
    `RIO_PE_1.pli_IO_Result_Reg,
    `RIO_PE_1.pli_IO_Err_Code_Reg,
    `RIO_PE_1.pli_IO_Req_Done_Flag_Reg,
    `RIO_PE_1.pli_MP_Trx_Tag_Reg, 
    `RIO_PE_1.pli_MP_Result_Reg,
    `RIO_PE_1.pli_MP_Err_Code_Reg,
    `RIO_PE_1.pli_MP_Req_Done_Flag_Reg,
    `RIO_PE_1.pli_DB_Trx_Tag_Reg, 
    `RIO_PE_1.pli_DB_Result_Reg,
    `RIO_PE_1.pli_DB_Err_Code_Reg,
    `RIO_PE_1.pli_DB_Req_Done_Flag_Reg,
    `RIO_PE_1.pli_Config_Trx_Tag_Reg, 
    `RIO_PE_1.pli_Config_Result_Reg,
    `RIO_PE_1.pli_Config_Err_Code_Reg,
    `RIO_PE_1.pli_Config_Req_Done_Flag_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_Address_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_Ext_Address_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_XAMSBS_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_LTType_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_TType_Reg,
    `RIO_PE_1.`RIO_CACHE_CONTROL.pli_Snoop_Req_Flag_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_MSGLEN_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_SSIZE_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_LETTER_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_MBOX_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_MSGSEG_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_DW_Size_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_MP_Flag_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_DB_Info_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Address_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Ext_Address_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_XAMSBS_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_DW_Size_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Type_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Is_GSM_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_BE_Reg,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.pli_Mem_Flag_Reg,
    `RIO_PE_1.pli_Pins_TFrame_Reg,
    `RIO_PE_1.pli_Pins_TD_Reg,
    `RIO_PE_1.pli_Pins_TDL_Reg,
    `RIO_PE_1.pli_Pins_TCLK_Reg,
    `RIO_PE_1.pli_Pins_Flag_Reg,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.mailbox_CSR,
    `RIO_PE_1.`RIO_MAILBOX_CONTROL.doorbell_CSR,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_XAMSBS,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_Ext_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_IO_Space_DW_Size,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_XAMSBS,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_Ext_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_Start_Address,
    `RIO_PE_1.`RIO_MEMORY_CONTROL.mem_GSM_Space_DW_Size,
    pli_EMPTY_Reg, /* dma_MSR */
    pli_EMPTY_Reg, /* dma_SAR */
    pli_EMPTY_Reg, /* dma_DAR */
    pli_EMPTY_Reg, /* dma_BCR */
    pli_EMPTY_Reg  /* dma_src_Inst_ID */
    );

//    $display("3");

    $HW_Register_Instance(2, `RIO_DMA_PE,
    /* parameters: */
    `RIO_ADDRESS_SUPPORT_CODE, /*ext_Address_Support code*/
    `RIO_FALSE, /*is_Bridge*/
    `RIO_TRUE, /*has_Memory*/
    `RIO_TRUE, /*has_Processor*/      
    ~`RIO_COH_GRANULE_SIZE_IS_64, /*coh_Granule_Size_32*/
    `RIO_TRX_ALL, /*source_Trx*/
    `RIO_TRX_ALL, /*dest_Trx*/ 
    `RIO_TRUE, /*mbox1*/
    `RIO_TRUE, /*mbox2*/
    `RIO_TRUE, /*mbox3*/
    `RIO_TRUE, /*mbox4*/
    `RIO_TRUE, /*has_Doorbell*/
    `RIO_IN_BUF_SIZE, /*pl_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*pl_Outbound_Buffer_Size*/
    `RIO_IN_BUF_SIZE, /*ll_Inbound_Buffer_Size*/
    `RIO_OUT_BUF_SIZE, /*ll_Outbound_Buffer_Size*/
    2, /*coh_Domain_Size*/ 
/*---------------------------------------------------------*/
/*New 10 parameters*/
    0, //device_Identity
    0, //device_Vendor_Identity
    0, //device_Rev
    0, //device_Minor_Rev
    0, //device_Major_Rev
    0, //assy_Identity
    0, //assy_Vendor_Identity
    0, //assy_Rev
	'h100, //entry_Extended_Features_Ptr /*pointer to the first entry of extended features list (16-31 bits in Assembly Info CAR)*/
	'h1000, //next_Extended_Features_Ptr /*ponter to the next extended features (user-defined) block (0-15 bits in Port Maint. Block Header 0)*/
/*End of new 10 parameters*/
/*---------------------------------------------------------*/
    /* PLI registers: */
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Trx_Tag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Result_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Err_Code_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_GSM_Req_Done_Flag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Loc_Trx_Tag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Loc_Response_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Loc_Response_Flag_Reg,
    `RIO_DMA_PE.pli_IO_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_IO_Result_Reg,
    `RIO_DMA_PE.pli_IO_Err_Code_Reg,
    `RIO_DMA_PE.pli_IO_Req_Done_Flag_Reg,
    `RIO_DMA_PE.pli_MP_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_MP_Result_Reg,
    `RIO_DMA_PE.pli_MP_Err_Code_Reg,
    `RIO_DMA_PE.pli_MP_Req_Done_Flag_Reg,
    `RIO_DMA_PE.pli_DB_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_DB_Result_Reg,
    `RIO_DMA_PE.pli_DB_Err_Code_Reg,
    `RIO_DMA_PE.pli_DB_Req_Done_Flag_Reg,
    `RIO_DMA_PE.pli_Config_Trx_Tag_Reg, 
    `RIO_DMA_PE.pli_Config_Result_Reg,
    `RIO_DMA_PE.pli_Config_Err_Code_Reg,
    `RIO_DMA_PE.pli_Config_Req_Done_Flag_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_Address_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_Ext_Address_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_XAMSBS_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_LTType_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_TType_Reg,
    `RIO_DMA_PE.`RIO_CACHE_CONTROL.pli_Snoop_Req_Flag_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_MSGLEN_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_SSIZE_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_LETTER_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_MBOX_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_MSGSEG_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_DW_Size_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_MP_Flag_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_DB_Info_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.pli_DB_Flag_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Address_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Ext_Address_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_XAMSBS_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_DW_Size_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Type_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Is_GSM_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_BE_Reg,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.pli_Mem_Flag_Reg,
    `RIO_DMA_PE.pli_Pins_TFrame_Reg,
    `RIO_DMA_PE.pli_Pins_TD_Reg,
    `RIO_DMA_PE.pli_Pins_TDL_Reg,
    `RIO_DMA_PE.pli_Pins_TCLK_Reg,
    `RIO_DMA_PE.pli_Pins_Flag_Reg,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.mailbox_CSR,
    `RIO_DMA_PE.`RIO_MAILBOX_CONTROL.doorbell_CSR,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_XAMSBS,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_Ext_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_Start_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_IO_Space_DW_Size,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_XAMSBS,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_Ext_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_Start_Address,
    `RIO_DMA_PE.`RIO_MEMORY_CONTROL.mem_GSM_Space_DW_Size,
    `RIO_DMA_PE.msr,
    `RIO_DMA_PE.sar,
    `RIO_DMA_PE.dar,
    `RIO_DMA_PE.bcr,
    `RIO_DMA_PE.src_Inst_ID,
    );

    $display(" ");
    $display("***************** Demo Application started *********************");
    $display("   Start simulation time: %7t", $time);
    $display(" ");

    $display("----------------------------------------------------------------");
    $display("                       INITIALIZATION                           ");
    $display("----------------------------------------------------------------");
    
//    $display("Reset 1");
//    $HW_Start_Reset_Instance(1);
//    $display("Init 1");

    $HW_Initialize_Instance(1,
        (`RIO_TR_INFO_SIZE == `RIO_TR_INFO_16) ? `RIO_PL_TRANSP_16 : `RIO_PL_TRANSP_32, /*Tr. Info size*/
        `RIO_DEV_ID_PE1, /*dev_ID*/
        0, /*lcshbar*/
        0, /*lcsbar*/
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        `RIO_MODE_8,
        `RIO_MAX_TIME_OUT, /*receive_Idle_Lim*/
        `RIO_MAX_TIME_OUT, /*throttle_Idle_Lim*/
        `RIO_RES_FOR_OUT, /*res_For_3_Out*/
        `RIO_RES_FOR_OUT, /*res_For_2_Out*/
        `RIO_RES_FOR_OUT, /*res_For_1_Out*/ 
        `RIO_RES_FOR_IN, /*res_For_3_In*/
        `RIO_RES_FOR_IN, /*res_For_2_In*/
        `RIO_RES_FOR_IN, /*res_For_1_In*/
        `RIO_PASS_BY_PRIO,
        0, /*config_Space_Size (mapped on IO memory space)*/
		`RIO_OPERATING_MODE,	/*defines the current working mode : 8 or 16 bit*/
		`RIO_REQ_WINDOW_ALIGN,/*defines if the model needs training sequence*/
		/*next parameter is not used if RIO_REQ_WINDOW_ALIGN = RIO_FALSE*/
		`RIO_NUM_TRANINGS,	/*number of sent training patterns*/

/*--------------------------------------------------------------------------*/
/*New 5 parameters*/
  	0, //is_Host
	`RIO_TRUE, //is_Master_Enable 
	0, //is_Discovered
    `RIO_TRUE, //input_Is_Enable
    `RIO_TRUE, //output_Is_Enable
/*End of New 5 parameters*/
/*--------------------------------------------------------------------------*/


        /* add ID-s if coh_Domain_Size > 1 */
        `RIO_DEV_ID_PE2,        
        /* add tr_info table values if shift- or index-based routing selected */
        0,
        0
        );

//    $display("Reset 2");
//    $HW_Start_Reset_Instance(2);
//    $display("Init 2");

    $HW_Initialize_Instance(2, (`RIO_TR_INFO_SIZE == `RIO_TR_INFO_16) ? `RIO_PL_TRANSP_16 : `RIO_PL_TRANSP_32 /*Tr. Info size*/,
        `RIO_DEV_ID_PE2, /*dev_ID*/
        0, /*lcshbar*/
        0, /*lcsbar*/
        `RIO_EXT_ADDRESS_SUPPORT /*ext_address*/,
        `RIO_EXT_ADDRESS_SIZE_SELECTOR /*ext_address_16*/,
        `RIO_MODE_8,
        `RIO_MAX_TIME_OUT, /*receive_Idle_Lim*/
        `RIO_MAX_TIME_OUT, /*throttle_Idle_Lim*/
        `RIO_RES_FOR_OUT, /*res_For_3_Out*/
        `RIO_RES_FOR_OUT, /*res_For_2_Out*/
        `RIO_RES_FOR_OUT, /*res_For_1_Out*/ 
        `RIO_RES_FOR_IN, /*res_For_3_In*/
        `RIO_RES_FOR_IN, /*res_For_2_In*/
        `RIO_RES_FOR_IN, /*res_For_1_In*/
        `RIO_PASS_BY_PRIO,
        0, /*config_Space_Size (mapped on IO memory space)*/
		`RIO_OPERATING_MODE,	/*defines the current working mode : 8 or 16 bit*/
		`RIO_REQ_WINDOW_ALIGN,/*defines if the model needs training sequence*/
		/*next parameter is not used if RIO_REQ_WINDOW_ALIGN = RIO_FALSE*/
		`RIO_NUM_TRANINGS,	/*number of sent training patterns*/

/*--------------------------------------------------------------------------*/
/*New 5 parameters*/
  	0, //is_Host
	`RIO_TRUE, //is_Master_Enable 
	0, //is_Discovered
    `RIO_TRUE, //input_Is_Enable
    `RIO_TRUE, //output_Is_Enable
/*End of New 5 parameters*/
/*--------------------------------------------------------------------------*/


        /* add ID-s if coh_Domain_Size > 1 */
        `RIO_DEV_ID_PE1,        
        /* add tr_info table values if shift- or index-based routing selected */
        0,
        0
        );

    $display("----------------------------------------------------------------");
    $display("         INITIALIZATION FINISHED, DEMO SCENARIO STARTED         ");
    $display("----------------------------------------------------------------");
    $display("");

    // run demo scenario
    $display(" ");
    $display("************ First part of Simple Demo Scenario started ***********");
    $display("   Simulation time: %7t", $time);
    $display(" ");

    `RIO_PE_1.demo_Process_Scenario_2 = 1'b1;     // Message receiver and memory initialization

//////////    `RIO_PE_1.demo_Process_Scenario_4 = 1'b1;     // GSM simple transactions

    `RIO_DMA_PE.demo_Process_Scenario_1 = 1'b1;   // Message sender

//    `RIO_DMA_PE.demo_Process_Scenario_2 = 1'b1;   // IO requestor     ///

    wait ((`RIO_PE_1.demo_Process_Scenario_1 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_1 === 1'b0) &&
        (`RIO_PE_1.demo_Process_Scenario_2 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_2 === 1'b0) &&    
        (`RIO_PE_1.demo_Process_Scenario_3 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_3 === 1'b0) &&    
        (`RIO_PE_1.demo_Process_Scenario_4 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_4 === 1'b0));    
 
    $display(" ");
    $display("************ Second part of Simple Demo Scenario started ***********");
    $display("   Simulation time: %7t", $time);
    $display(" ");

    `RIO_PE_1.demo_Process_Scenario_1 = 1'b1;     // Config requests + DMA + Doorbell from DMA

    wait ((`RIO_PE_1.demo_Process_Scenario_1 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_1 === 1'b0) &&
        (`RIO_PE_1.demo_Process_Scenario_2 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_2 === 1'b0) &&    
        (`RIO_PE_1.demo_Process_Scenario_3 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_3 === 1'b0) &&   
        (`RIO_PE_1.demo_Process_Scenario_4 === 1'b0) &&     
        (`RIO_DMA_PE.demo_Process_Scenario_4 === 1'b0));    

    $display(" ");
    $display("******************* Demo Application Results ******************");
 
    if ((`RIO_PE_1.error_Counter + `RIO_DMA_PE.error_Counter) === 0)
        $display("                  NO ERRORS: FINISHED DEMO OK");
    else
    begin
        $display("       DEMO: %d ERRORS occured in PE 1 demo scenario", `RIO_PE_1.error_Counter);
        $display("       DEMO: %d ERRORS occured in PE 2 (DMA PE) demo scenario", `RIO_DMA_PE.error_Counter);
        $display("                ERRORS HAVE OCCURED: DEMO FAILED");
    end


    $HW_Close;
    $display("   Finish simulation time: %7t", $time);
    $display("****************** Demo Application Finished ******************");
    $display(" ");

    

    $finish;
end

// -------------------------------------------------------------

endmodule

