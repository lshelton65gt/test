/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: W:\riosuite\src\verilog_env\ts_tb\module_pe\scenario_pe.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Scenarios for RIO Pe module
*
* Notes:  this file shall be included to the module_pe.v file and 
*         it is not separate module file
*
******************************************************************************/

/*registers for starting one of scenarios*/
reg start_scenario;
reg start_small_scenario;

integer trx_tag; //value of trx tag
integer address; //address for transactions
integer size;    //size of payload

/***************************************************************************
 * Description: Initialization of all flags
 *
 * Notes:
 *
 **************************************************************************/
initial
begin
    start_scenario = 1'b0;
    start_small_scenario = 1'b0;
    trx_tag = 0;
end 


/***************************************************************************
 * Description: Block which is realized small scenario 
 *
 * Notes: Here implemented omly IO transactions
 *
 **************************************************************************/
always @(posedge start_small_scenario)   
begin
    /*
            IO transactions
    */

    //Set data for transactions sending
    for (i = 0; i < 32; i = i + 1 )
        $HW_Put_Data_Pe (inst_Id, `RIO_IO_REQUEST_RQ_DATA, i, 32'h0E0E0E0E /*ms_Word*/, 32'h0F0F0F0F /*ls_Word*/);

    address = `RIO_PE_IO_START_ADDRESS * (2 - inst_Id);

    //Send 15 NWRITE packets
    for (i = 1; i < 16; i = i + 1)
    begin
        while ( $HW_Rio_IO_Request( inst_Id,
            `RIO_IO_NWRITE, //rq_ttype
            1, //rq_prio
            address, //rq_address
            `RIO_PE_IO_EXT_ADDRESS, //rq_extended_address
            `RIO_PE_IO_XAMSBS, //rq_xamsbs
            i, //rq_dw_size
            0, //rq_offset
            8, //rq_byte_num
            trx_tag,
            `RIO_PE_TRANSP_TYPE)  == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
    /*wait while transaction processed, if you need to serialize requests*/
            wait (rio_io_request_done_flag_reg === 1'b1 && rio_io_request_done_trx_tag_reg === trx_tag); 
            rio_io_request_done_flag_reg = 1'b0;
            trx_tag = trx_tag + 1; 
    //Insert delaying
            #(`RIO_WAIT_BEFORE_RETRY);
end

//Scenario finished
    start_small_scenario = 1'b0;
end 


/***************************************************************************
 * Description: Implementation of big scenario
 *
 * Notes: Here realized example of calling all wrappers routines
 *
 **************************************************************************/
always @(posedge start_scenario)   
begin

    //Io transactions
    for (i = 0; i < 32; i = i + 1 )
        $HW_Put_Data_Pe (inst_Id, `RIO_IO_REQUEST_RQ_DATA, i, 32'h0E0E0E0E /*ms_Word*/, 32'h0F0F0F0F /*ls_Word*/);

    //Set address for hit to memory of other PE
    address = `RIO_PE_IO_START_ADDRESS * (2 - inst_Id);

    //Send 15 NWRITE packets
    for (i = 1; i < 16; i = i + 1)
    begin
        while ( $HW_Rio_IO_Request( inst_Id,
            `RIO_IO_NWRITE, //rq_ttype
            1, //rq_prio
            address, //rq_address
            `RIO_PE_IO_EXT_ADDRESS, //rq_extended_address
            `RIO_PE_IO_XAMSBS, //rq_xamsbs
            i, //rq_dw_size
            0, //rq_offset
            8, //rq_byte_num
            trx_tag,
            `RIO_PE_TRANSP_TYPE)  == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
    /*wait while transaction processed, if you need to serialize requests*/
            wait (rio_io_request_done_flag_reg === 1'b1 && rio_io_request_done_trx_tag_reg === trx_tag); 
            rio_io_request_done_flag_reg = 1'b0;
            trx_tag = trx_tag + 1; 
    //Insert delaying
            #(`RIO_WAIT_BEFORE_RETRY);
    end

    //Send 3 NREAD packets
        size = 1;
    for (i = 1; i < 4; i = i + 1)
    begin
        while ( $HW_Rio_IO_Request( inst_Id,
            `RIO_IO_NREAD, //rq_ttype
            1, //rq_prio
            address, //rq_address
            `RIO_PE_IO_EXT_ADDRESS, //rq_extended_address
            `RIO_PE_IO_XAMSBS, //rq_xamsbs
            size, //rq_dw_size
            0, //rq_offset
            8, //rq_byte_num
            trx_tag,
            `RIO_PE_TRANSP_TYPE)  == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
    /*wait while transaction processed, if you need to serialize requests*/
            wait (rio_io_request_done_flag_reg === 1'b1 && rio_io_request_done_trx_tag_reg === trx_tag); 
            rio_io_request_done_flag_reg = 1'b0;
            trx_tag = trx_tag + 1; 
    //Insert delaying
            #(`RIO_WAIT_BEFORE_RETRY);
        size = size * 2; 
        address = address + 8;
    end                  

    //Send NWRITE_R transaction
        while ( $HW_Rio_IO_Request( inst_Id,
            `RIO_IO_NWRITE_R, //rq_ttype
            1, //rq_prio
            address, //rq_address
            `RIO_PE_IO_EXT_ADDRESS, //rq_extended_address
            `RIO_PE_IO_XAMSBS, //rq_xamsbs
            4, //rq_dw_size
            0, //rq_offset
            8, //rq_byte_num
            trx_tag,
            `RIO_PE_TRANSP_TYPE)  == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
    /*wait while transaction processed, if you need to serialize requests*/
            wait (rio_io_request_done_flag_reg === 1'b1 && rio_io_request_done_trx_tag_reg === trx_tag); 
            rio_io_request_done_flag_reg = 1'b0;
            trx_tag = trx_tag + 1; 

//Message transactions
    for (i = 0; i < 32; i = i + 1 )
        $HW_Put_Data_Pe (inst_Id, `RIO_MESSAGE_REQUEST_RQ_DATA, i, 32'h0E0E0E0E /*ms_Word*/, 32'h0F0F0F0F /*ls_Word*/);

    for (i = 0; i < 8 - 1; i = i + 1)
    begin
        while($HW_Rio_Message_Request( inst_Id,
            (1 - inst_Id),  //rq_transport_info
            1,              //rq_prio,
            8 - 1,          //rq_msglen,
            16,             //rq_ssize,
            1,              //rq_letter,
            2,              //rq_mbox,
            8 - 1 - i,      //rq_msgseg,
            16,             //rq_dw_size,
            trx_tag,
            `RIO_PE_TRANSP_TYPE) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
        trx_tag = trx_tag + 1;
    end
    wait (rio_mp_request_done_flag_reg === 1'b1 && rio_mp_request_done_trx_tag_reg === trx_tag - 1); 
    rio_mp_request_done_flag_reg = 1'b0;

//Doorbell request
    while( $HW_Rio_Doorbell_Request( inst_Id,
        2,              //rq_prio,
        (1 - inst_Id),  //rq_transport_info,
        7,              //rq_doorbell_info,
        trx_tag,
        `RIO_PE_TRANSP_TYPE) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    wait (rio_doorbell_request_done_flag_reg === 1'b1 && rio_doorbell_request_done_trx_tag_reg == trx_tag);
    rio_doorbell_request_done_flag_reg = 1'b0;
    trx_tag = trx_tag + 1;


//congiguration requests
    for (i = 0; i < 32; i = i + 1 )
        $HW_Put_Data_Pe (inst_Id, `RIO_CONF_REQUEST_RQ_DATA, i, 32'hF1F2F3F4 /*ms_Word*/, 32'hF5F6F7F8 /*ls_Word*/);

    while ($HW_Rio_Conf_Request( inst_Id,
        0,              //rq_prio,
        (1 - inst_Id),  //rq_transport_info,
        1,              //rq_hop_count,
        `RIO_CONF_WRITE, //rq_rw,
        'h500,            //rq_config_offset,
        1,              //rq_dw_size,
        3,              //rq_sub_dw_pos,
        trx_tag,
        `RIO_PE_TRANSP_TYPE) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    wait (rio_config_request_done_flag_reg === 1'b1 && rio_config_request_done_trx_tag_reg == trx_tag);
    rio_config_request_done_flag_reg = 1'b0;
    trx_tag = trx_tag + 1;

    while ($HW_Rio_Conf_Request( inst_Id,
        0,              //rq_prio,
        (1 - inst_Id),  //rq_transport_info,
        1,              //rq_hop_count,
        `RIO_CONF_READ, //rq_rw,
        'h500,            //rq_config_offset,
        1,              //rq_dw_size,
        3,              //rq_sub_dw_pos,
        trx_tag,
        `RIO_PE_TRANSP_TYPE) == `RIO_RETRY)
        #(`RIO_WAIT_BEFORE_RETRY);
    wait (rio_config_request_done_flag_reg === 1'b1 && rio_config_request_done_trx_tag_reg == trx_tag);
    rio_config_request_done_flag_reg = 1'b0;
    trx_tag = trx_tag + 1;

    //Scenario finished
        start_scenario = 1'b0;
end
