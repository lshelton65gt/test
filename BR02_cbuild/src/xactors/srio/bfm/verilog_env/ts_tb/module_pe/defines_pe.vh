/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_pe\defines_pe.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Defines for creating PE instance
*
* Notes:        
*
******************************************************************************/

//------------------------------------------------------------------------
//                  Address configuration settings              
//------------------------------------------------------------------------

// Allowed full address sizes:
// Possible values:
//   `RIO_PE_SUPPORTS_66_50_34_ADDR      
//   `RIO_PE_SUPPORTS_66_34_ADDR         
//   `RIO_PE_SUPPORTS_50_34_ADDR         
//   `RIO_PE_SUPPORTS_34_ADDR            
`define RIO_PE_ADDRESS_SUPPORT_CODE            `RIO_PE_SUPPORTS_66_50_34_ADDR
// Extended address enabled/disabled: true - enabled, false - disabled 
`define RIO_PE_EXT_ADDRESS_SUPPORT             `RIO_FALSE
// Extended address size (if enabled): true - 16 bits, false - 32 bits
`define RIO_PE_EXT_ADDRESS_SIZE_SELECTOR       `RIO_FALSE                         

`define RIO_PE_CONFIG_SPACE_SIZE                0

//------------------------------------------------------------------------
//                  Extended features configuration settings              
//------------------------------------------------------------------------
`define RIO_PE_ENTRY_EXTENDED_FEATURES_PTR      'h100
`define RIO_PE_NEXT_EXTENDED_FEATURES_PTR       'h1000


//------------------------------------------------------------------------
//                  TRX constants
//------------------------------------------------------------------------
`define RIO_PE_SOURCE_TRX                       32'hFFFFFFF0
`define RIO_PE_DEST_TRX                         32'hFFFFFFF4

//------------------------------------------------------------------------
//                  BUFFER sizes
//------------------------------------------------------------------------
`define RIO_PE_INBOUND_BUF_SIZE                 12
`define RIO_PE_OUTBOUND_BUF_SIZE                25

//------------------------------------------------------------------------
//                  CSR values
//------------------------------------------------------------------------
`define PE_DOORBEL_CSR                          32'hA00000A0 /*doorbell available and empty; write poert available and empty*/

//------------------------------------------------------------------------
//                  GSM space boiundaries
//------------------------------------------------------------------------
`define RIO_PE_GSM_EXT_ADDRESS                  0
`define RIO_PE_GSM_START_ADDRESS                'h200000
`define RIO_PE_GSM_XAMSBS                       0
`define RIO_PE_GSM_SPACE_DW_SIZE                1000

//------------------------------------------------------------------------
//                  IO space boiundaries
//------------------------------------------------------------------------
`define RIO_PE_IO_EXT_ADDRESS                  0
`define RIO_PE_IO_START_ADDRESS                32'h1000 /*'0 - for testing suite; h1000 for RapidIO models*/
`define RIO_PE_IO_XAMSBS                       0
`define RIO_PE_IO_SPACE_DW_SIZE                500


//------------------------------------------------------------------------
//                  TRansport type
//------------------------------------------------------------------------
//transport type 16 RIO_PE_TRANSP_TYPE=0; transport type 32 RIO_PE_TRANSP_TYPE = 1
`define RIO_PE_TRANSP_TYPE                      0

//------------------------------------------------------------------------
//                  Window alignment parameters
//------------------------------------------------------------------------
`define RIO_REQ_WND_ALIGN                       `RIO_TRUE
`define NUM_TRAININGS                           256

//------------------------------------------------------------------------
//                  Additional parameters
//------------------------------------------------------------------------
`define RIO_PE_MODE_IS_8                        1  /*can be 1 or 0*/
`define RIO_PE_PARAM_SET_LP_EP_IS_8             1  /*can be 1 or 0*/
`define RIO_PE_IDLE_LIM                         100
`define RIO_PE_RES_FOR_OUT                      3
`define RIO_PE_RES_FOR_IN                       3
`define RIO_PE_ALL_MBOX_AVAILABLE               'hA0A0A0A0
`define RIO_WAIT_BEFORE_RETRY                   10
`define RIO_PE_ALL_LANES_CHANGED                4'b1111

//serial link parameters
`define RIO_PE_PARAM_SET_SERIAL_IS_1X           `RIO_FALSE
`define RIO_PE_IS_FORSE_1X_MODE                 `RIO_FALSE
`define RIO_PE_IS_FORSE_1X_MODE_LANE0           `RIO_FALSE
`define RIO_PE_SILENCE_PERIOD                   100
`define RIO_PE_COMP_SEQ_RATE                    5000
`define RIO_PE_STATUS_SYM_RATE                  1024
`define RIO_PE_TRANSMIT_FC_SUPPORT              `RIO_TRUE
`define RIO_PE_DISCOVERY_PERIOD                 500
