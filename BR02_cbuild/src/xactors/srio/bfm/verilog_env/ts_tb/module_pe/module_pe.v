/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_pe\module_pe.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  module for RIO PE Models 
*
* Notes:
*
******************************************************************************/

`include "wrapper_pe.vh"
`include "defines_pe.vh"


module PE_Module (clock, tclk, tframe, td, tdl, rclk, rframe, rd, rdl,
/*serial interface*/
rlane0, rlane1, rlane2, rlane3, tlane0, tlane1, tlane2, tlane3,
/* new 10bit interface*/
clock_10, rlane0_10, rlane1_10, rlane2_10, rlane3_10, tlane0_10, tlane1_10, tlane2_10, tlane3_10);


/*PapidIO bus input/output pins*/
    input clock; 
	input clock_10;

    input rclk;    
    input rframe;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    input rlane0;
    input rlane1;
    input rlane2;
    input rlane3;

   /* 10bit interface */
    input [ 0 : 9 ] rlane0_10;
    input [ 0 : 9 ] rlane1_10;
    input [ 0 : 9 ] rlane2_10;
    input [ 0 : 9 ] rlane3_10;

    output tclk;
    output tframe;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;

    output tlane0;
    output tlane1;
    output tlane2;
    output tlane3;

   /* 10bit interface */
    output [ 0 : 9 ] tlane0_10;
    output [ 0 : 9 ] tlane1_10;
    output [ 0 : 9 ] tlane2_10;
    output [ 0 : 9 ] tlane3_10;

    reg tclk_reg;
    reg tframe_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl_reg;

    reg tlane0_reg;
    reg tlane1_reg;
    reg tlane2_reg;
    reg tlane3_reg;

    /* 10bit interface */
    reg [ 0 : 9 ] tlane0_10_reg;
    reg [ 0 : 9 ] tlane1_10_reg;
    reg [ 0 : 9 ] tlane2_10_reg;
    reg [ 0 : 9 ] tlane3_10_reg;

    assign tclk = tclk_reg;
    assign tframe = tframe_reg;
    assign td = td_reg;
    assign tdl = tdl_reg;

    assign tlane0 = tlane0_reg;
    assign tlane1 = tlane1_reg;
    assign tlane2 = tlane2_reg;
    assign tlane3 = tlane3_reg;

   /* 10bit interface */
    assign tlane0_10 = tlane0_10_reg;
    assign tlane1_10 = tlane1_10_reg;
    assign tlane2_10 = tlane2_10_reg;
    assign tlane3_10 = tlane3_10_reg;

/*parameter of the mode in which current instance will be work*/
    parameter mode = `RIO_PE_INST_PAR_MODE;

/*temporary buffer for serial input pins flag*/
    reg [0 : `RIO_LANE_NUM - 1] lane_changed;

/*Definition of all pli registers*/
`include "../../pliwrap/pe/pli_registers.vh"   


    integer inst_Id;    //Instance ID of current module
    integer ms_word;    //Value of most significant word
    integer ls_word;    //Value of least significant word
    integer i;          //loop counter
    integer data;       //temporary storage for something numbers
    integer result;     //contains result of operations
//reset register
reg reset_pe;

/*includes file with the scenarios for demo purposes*/
`include "scenario_pe.v"

//MAILBOX instantination
Mailbox_Module mailbox();
Memory_Module  memory();

/***************************************************************************
 * Description: Performs internal blocks reset
 *
 * Notes:
 *
 **************************************************************************/
always 
begin
    wait (reset_pe === 1'b1);
    
    mailbox.reset_mbox = 1'b1;
//    wait (mailbox.reset_mbox === 1'b0);
         
    memory.reset_mem = 1'b1;
//    wait(memory.reset_mem === 1'b0);
    
    $HW_Rio_Start_Reset(inst_Id);
    reset_pe = 1'b0;
end

//------------------------------------------------------
//routines for GSM transactions
/* Begin of example how to call routine Rio_GSM_Request
result = $HW_Rio_GSM_Request( inst_Id,
    ttype,
    rq_prio,
    rq_address,
    rq_extended_address,
    rq_xamsbs,
    rq_dw_size,
    rq_offset,
    rq_byte_num,
    trx_tag);
End of example how to call routine Rio_GSM_Request*/

/*realization of Rio_GSM_Request_Done task*/
//always
//begin
//wait (rio_gsm_request_done_flag_reg === 1'b1);
//    rio_gsm_request_done_flag_reg = 1'b0;

    /*@@@@ Insert something code here @@@@ = rio_gsm_request_done_trx_tag_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_gsm_request_done_result_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_gsm_request_done_err_code_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_gsm_request_done_dw_size_reg;*/ 

     /*Insert code for realization Rio_GSM_Request_Done task here*/
//end


//------------------------------------------------------
//Routines for IO transactions

/* Begin of example how to call routine Rio_IO_Request
result = $HW_Rio_IO_Request( inst_Id,
    rq_ttype,
    rq_prio,
    rq_address,
    rq_extended_address,
    rq_xamsbs,
    rq_dw_size,
    rq_offset,
    rq_byte_num,
    trx_tag);
End of example how to call routine Rio_IO_Request*/

/*realization of Rio_IO_Request_Done task*/
//always
//begin
//wait (rio_io_request_done_flag_reg === 1'b1);
//    rio_io_request_done_flag_reg = 1'b0;

    /*@@@@ Insert something code here @@@@ = rio_io_request_done_trx_tag_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_io_request_done_result_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_io_request_done_err_code_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_io_request_done_dw_size_reg;*/ 

     /*Insert code for realization Rio_IO_Request_Done task here*/
//end



//------------------------------------------------------
//Message passing

/* Begin of example how to call routine Rio_Message_Request
result = $HW_Rio_Message_Request( inst_Id,
    rq_transport_info,
    rq_prio,
    rq_msglen,
    rq_ssize,
    rq_letter,
    rq_mbox,
    rq_msgseg,
    rq_dw_size,
    trx_tag);
End of example how to call routine Rio_Message_Request*/


/*realization of Rio_MP_Request_Done task*/
//always
//begin
//wait (rio_mp_request_done_flag_reg === 1'b1);
//    rio_mp_request_done_flag_reg = 1'b0;
 
    /*@@@@ Insert something code here @@@@ = rio_mp_request_done_trx_tag_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_mp_request_done_result_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_mp_request_done_err_code_reg;*/ 

     /*Insert code for realization Rio_MP_Request_Done task here*/
//end


//------------------------------------------------------
//Dorrbell passing

/* Begin of example how to call routine Rio_Doorbell_Request
result = $HW_Rio_Doorbell_Request( inst_Id,
    rq_prio,
    rq_transport_info,
    rq_doorbell_info,
    trx_tag);
End of example how to call routine Rio_Doorbell_Request*/

/*realization of Rio_Doorbell_Request_Done task*/
//always
//begin
//wait (rio_doorbell_request_done_flag_reg === 1'b1);
//    rio_doorbell_request_done_flag_reg = 1'b0;

    /*@@@@ Insert something code here @@@@ = rio_doorbell_request_done_trx_tag_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_doorbell_request_done_result_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_doorbell_request_done_err_code_reg;*/ 

     /*Insert code for realization Rio_Doorbell_Request_Done task here*/
//end


//------------------------------------------------------
//Configuration request

/* Begin of example how to call routine Rio_Conf_Request
result = $HW_Rio_Conf_Request( inst_Id,
    rq_prio,
    rq_transport_info,
    rq_hop_count,
    rq_rw,
    rq_config_offset,
    rq_dw_size,
    rq_sub_dw_pos,
    trx_tag);
End of example how to call routine Rio_Conf_Request*/

/*realization of Rio_Config_Request_Done task*/
//always
//begin
//wait (rio_config_request_done_flag_reg === 1'b1);
//    rio_config_request_done_flag_reg = 1'b0;

    /*@@@@ Insert something code here @@@@ = rio_config_request_done_trx_tag_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_config_request_done_result_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_config_request_done_err_code_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_config_request_done_dw_size_reg;*/ 

     /*Insert code for realization Rio_Config_Request_Done task here*/
//end




//------------------------------------------------------
//additional routines

/* Begin of example how to call routine Rio_Snoop_Response
result = $HW_Rio_Snoop_Response( inst_Id,
    snoop_result);
End of example how to call routine Rio_Snoop_Response*/


/* Begin of example how to call routine Rio_Set_Mem_Data
result = $HW_Rio_Set_Mem_Data( inst_Id);
End of example how to call routine Rio_Set_Mem_Data*/


/* Begin of example how to call routine Rio_Get_Mem_Data
result = $HW_Rio_Get_Mem_Data( inst_Id);
End of example how to call routine Rio_Get_Mem_Data*/


/* Begin of example how to call routine Rio_Set_Conf_Reg
result = $HW_Rio_Set_Conf_Reg( inst_Id,
    config_offset,
    data);
End of example how to call routine Rio_Set_Conf_Reg*/


/* Begin of example how to call routine Rio_Get_Conf_Reg
result = $HW_Rio_Get_Conf_Reg( inst_Id,
    config_offset);
End of example how to call routine Rio_Get_Conf_Reg*/


/* Begin of example how to call routine Rio_Start_Reset
$HW_Rio_Start_Reset( inst_Id);
End of example how to call routine Rio_Start_Reset*/

/* Begin of example how to pass arrays to wrapper
$HW_Put_Data_Pe (type, index, ms_word, ls_word);

type can take following values:

    RIO_GSM_REQUEST_RQ_DATA
    RIO_IO_REQUEST_RQ_DATA
    RIO_MESSAGE_REQUEST_RQ_DATA
    RIO_CONF_REQUEST_RQ_DATA
    RIO_SNOOP_RESPONSE_DATA
    RIO_SET_MEM_DATA_DW

$HW_Put_Data_Pe (RIO_INIT_PARAM_SET_REMOTE_DEV_ID, index, value);
    
End of example */


/* Begin of example how to gets arrays from wrapper
ms_word = $HW_Get_Data_Pe (inst_Id, type, index, MSW);
ls_word = $HW_Get_Data_Pe (inst_Id, type, index, LSW);
config_reg = $HW_Get_Data_Pe (inst_Id, RIO_GET_CONF_REG_DATA);

type can take following values:
    RIO_GSM_REQUEST_DONE_DATA
    RIO_IO_REQUEST_DONE_DATA
    RIO_CONFIG_REQUEST_DONE_DATA
    RIO_MP_REMOTE_REQUEST_MESSAGE_DATA
    RIO_PORT_WRITE_REMOTE_REQUEST_DATA
    RIO_GET_MEM_DATA_DW
End of example */


/***************************************************************************
 * Description: External clock handler
 *
 * Notes:
 *
 **************************************************************************/
always @(clock)
begin
    if (~(clock === 1'bx) && inst_Id != `RIO_PE_ERROR_INST_ID)
        $HW_Rio_Clock_Edge( inst_Id );
end 

/***************************************************************************
 * Description: External clock handler for 10bit interface
 *
 * Notes:
 *
 **************************************************************************/
always @(clock_10)
begin
    if (~(clock_10 === 1'bx) && inst_Id != `RIO_PE_ERROR_INST_ID)
        $HW_Rio_Clock_Edge_New( inst_Id );
end 


/***************************************************************************
 * Description: Input LP-EP clock handler
 *
 * Notes:
 *
 **************************************************************************/
always @(rclk)
if (mode == `RIO_PE_INST_PAR_MODE)
begin
    if (~(rclk === 1'bx) && inst_Id != `RIO_PE_ERROR_INST_ID)
        $HW_Rio_Get_Pins( inst_Id, rframe, rd, rdl, rclk);
end 

/***************************************************************************
 * Description: Input pcs-pma pin handler
 *
 * Notes: 0-3
 *
 **************************************************************************/
always 
begin
/*    wait (lane_changed === `RIO_PE_ALL_LANES_CHANGED);
    lane_changed = 1'b0;*/
    @(clock);
    #1;  /*wait for all pins setting*/
    if (mode == `RIO_PE_INST_SERIAL_MODE)
        $HW_Rio_Serial_Get_Pins(inst_Id, rlane0, rlane1, rlane2, rlane3);
end

/***************************************************************************
 * Description: Input pcs-pma pin handler for 10-bit interface
 *
 * Notes: 0-3
 *
 **************************************************************************/
always 
begin
/*    wait (lane_changed === `RIO_PE_ALL_LANES_CHANGED);
    lane_changed = 1'b0;*/
    @(clock_10);
    #1;  /*wait for all pins setting*/
    if (mode == `RIO_PE_INST_SERIAL_MODE)
        $HW_Rio_Serial_Get_Pins_New(inst_Id, rlane0_10, rlane1_10, rlane2_10, rlane3_10);
end


/*
    =========== Callbacks implementation ===========
*/

/***************************************************************************
 * Description: realization of Rio_Snoop_Request task
 *
 * Notes:
 *
 **************************************************************************/
always
begin
    wait (rio_snoop_request_flag_reg === 1'b1);
        rio_snoop_request_flag_reg = 1'b0;

    /*@@@@ Insert something code here @@@@ = rio_snoop_request_address_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_snoop_request_extended_address_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_snoop_request_xamsbs_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_snoop_request_lttype_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_snoop_request_ttype_reg;*/ 
    $display("PE%d:    Snoop request to address %d", inst_Id, rio_snoop_request_address_reg);

    #(`RIO_PE_SNOOP_REQUEST_DELAY); //Alvays necessary create delay before snoop responce
    result = $HW_Rio_Snoop_Response( inst_Id, `RIO_SNOOP_MISS);
    if (result != `RIO_OK) $display("Snoop response not OK");
end


/***************************************************************************
 * Description: realization of Rio_Doorbell_Remote_Request task
 *
 * Notes: doorbell request
 *
 **************************************************************************/
always
begin               
wait (rio_doorbell_remote_request_flag_reg === 1'b1);
    rio_doorbell_remote_request_flag_reg = 1'b0;

    /*@@@@ Insert something code here @@@@ = rio_doorbell_remote_request_doorbell_info_reg;*/ 

     /*Insert code for realization Rio_Doorbell_Remote_Request task here*/
    $display ("PE%d:    Doorbel request is received; dorrbel info = %d", inst_Id, 
        rio_doorbell_remote_request_doorbell_info_reg);
end


/*realization of Rio_Port_Write_Remote_Request task*/
/***************************************************************************
 * Description: realization of Rio_Port_Write_Remote_Request task
 *
 * Notes: 
 *
 **************************************************************************/
always
begin
    wait (rio_port_write_remote_request_flag_reg === 1'b1);
        rio_port_write_remote_request_flag_reg = 1'b0;

        /*@@@@ Insert something code here @@@@ = rio_port_write_remote_request_dw_size_reg;*/ 
        /*@@@@ Insert something code here @@@@ = rio_port_write_remote_request_sub_dw_pos_reg;*/ 

         /*Insert code for realization Rio_Port_Write_Remote_Request task here*/

    $display ("PE%d:    Port write request is received;", inst_Id);
    for (i = 0; i < rio_port_write_remote_request_dw_size_reg; i = i + 1)
    begin
        ms_word = $HW_Get_Data_Pe (inst_Id, `RIO_PORT_WRITE_REMOTE_REQUEST_DATA, i, `MSW);
        ls_word = $HW_Get_Data_Pe (inst_Id, `RIO_PORT_WRITE_REMOTE_REQUEST_DATA, i, `LSW);
    end
end


/***************************************************************************
 * Description: parallel link output interface
 *
 * Notes: 
 *
 **************************************************************************/
always
begin
    wait (rio_pl_set_pins_flag_reg === 1'b1);
        rio_pl_set_pins_flag_reg = 1'b0;

        tframe_reg = rio_pl_set_pins_tframe_reg;
        td_reg = rio_pl_set_pins_td_reg;
        tdl_reg = rio_pl_set_pins_tdl_reg;
        tclk_reg = rio_pl_set_pins_tclk_reg;
end

/***************************************************************************
 * Description: serial link output interface
 *
 * Notes: 
 *
 **************************************************************************/
always
begin
    wait (rio_pl_set_serial_pins_flag_reg === 1'b1);
        rio_pl_set_serial_pins_flag_reg = 1'b0;

        tlane0_reg = rio_pl_set_pins_tlane0_reg;
        tlane1_reg = rio_pl_set_pins_tlane1_reg;
        tlane2_reg = rio_pl_set_pins_tlane2_reg;
        tlane3_reg = rio_pl_set_pins_tlane3_reg;

end

/***************************************************************************
 * Description: serial link output interface (10 bits)
 *
 * Notes: 
 *
 **************************************************************************/
always
begin
    wait (rio_pl_set_serial_pins_10bit_flag_reg === 1'b1);
        rio_pl_set_serial_pins_10bit_flag_reg = 1'b0;

        tlane0_10_reg = rio_pl_set_pins_tlane0_10_reg;
        tlane1_10_reg = rio_pl_set_pins_tlane1_10_reg;
        tlane2_10_reg = rio_pl_set_pins_tlane2_10_reg;
        tlane3_10_reg = rio_pl_set_pins_tlane3_10_reg;

end



/***************************************************************************
 * Description: Creates instance of models
 *
 * Notes: 
 *
 **************************************************************************/
initial
if (mode == `RIO_PE_INST_PAR_MODE)
begin
    #1


    inst_Id = $HW_RIO_Model_Create_Instance (
    `RIO_PE_ADDRESS_SUPPORT_CODE,       //param_ext_address_support
    `RIO_FALSE,                         //param_is_bridge
    `RIO_TRUE,                          //param_has_memory
    `RIO_TRUE,                          //param_has_processor
    `RIO_TRUE,                          //param_coh_granule_size_32
    2,                                  //param_coh_domain_size
    0,                                  //param_device_identity,
    1,                                  //param_device_vendor_identity,
    0,                                  //param_device_rev,
    0,                                  //param_device_minor_rev,
    0,                                  //param_device_major_rev,
    0,                                  //param_assy_identity,
    0,                                  //param_assy_vendor_identity,
    0,                                  //param_assy_rev,
    `RIO_PE_ENTRY_EXTENDED_FEATURES_PTR, //param_entry_extended_features_ptr,
    `RIO_PE_NEXT_EXTENDED_FEATURES_PTR, //param_next_extended_features_ptr,
    `RIO_PE_SOURCE_TRX,                 //param_source_trx,
    `RIO_PE_DEST_TRX,                   //param_dest_trx,
    `RIO_TRUE,                          //param_mbox1,
    `RIO_TRUE,                          //param_mbox2,
    `RIO_TRUE,                          //param_mbox3,
    `RIO_TRUE,                          //param_mbox4,
    `RIO_TRUE,                          //param_has_doorbell,
    `RIO_PE_INBOUND_BUF_SIZE,           //param_pl_inbound_buffer_size,
    `RIO_PE_OUTBOUND_BUF_SIZE,          //param_pl_outbound_buffer_size,
    `RIO_PE_INBOUND_BUF_SIZE,           //param_ll_inbound_buffer_size,
    `RIO_PE_OUTBOUND_BUF_SIZE,          //param_ll_outbound_buffer_size,
    /*registers handlers passing*/
    rio_gsm_request_done_trx_tag_reg,
    rio_gsm_request_done_result_reg,
    rio_gsm_request_done_err_code_reg,
    rio_gsm_request_done_dw_size_reg,
    rio_gsm_request_done_flag_reg,
    
    rio_io_request_done_trx_tag_reg,
    rio_io_request_done_result_reg,
    rio_io_request_done_err_code_reg,
    rio_io_request_done_dw_size_reg,
    rio_io_request_done_flag_reg,
    
    rio_mp_request_done_trx_tag_reg,
    rio_mp_request_done_result_reg,
    rio_mp_request_done_err_code_reg,
    rio_mp_request_done_flag_reg,

    rio_doorbell_request_done_trx_tag_reg,
    rio_doorbell_request_done_result_reg,
    rio_doorbell_request_done_err_code_reg,
    rio_doorbell_request_done_flag_reg,

    rio_config_request_done_trx_tag_reg,
    rio_config_request_done_result_reg,
    rio_config_request_done_err_code_reg,
    rio_config_request_done_dw_size_reg,
    rio_config_request_done_flag_reg,

    rio_snoop_request_address_reg,
    rio_snoop_request_extended_address_reg,
    rio_snoop_request_xamsbs_reg,
    rio_snoop_request_lttype_reg,
    rio_snoop_request_ttype_reg,
    rio_snoop_request_flag_reg,

    mailbox.rio_mailbox_msglen_reg,
    mailbox.rio_mailbox_ssize_reg,
    mailbox.rio_mailbox_letter_reg,
    mailbox.rio_mailbox_mbox_reg,
    mailbox.rio_mailbox_msgseg_reg,
    mailbox.rio_mailbox_dw_size_reg,
    mailbox.rio_mailbox_flag_reg,

    rio_doorbell_remote_request_doorbell_info_reg,
    rio_doorbell_remote_request_flag_reg,

    rio_port_write_remote_request_dw_size_reg,
    rio_port_write_remote_request_sub_dw_pos_reg,
    rio_port_write_remote_request_flag_reg,

    memory.rio_mem_address_reg,
    memory.rio_mem_extended_address_reg,
    memory.rio_mem_xamsbs_reg,
    memory.rio_mem_dw_size_reg,
    memory.rio_mem_req_type_reg,
    memory.rio_mem_is_gsm_reg,
    memory.rio_mem_be_reg,
    memory.rio_mem_flag_reg,

    rio_pl_set_pins_tframe_reg,
    rio_pl_set_pins_td_reg,
    rio_pl_set_pins_tdl_reg,
    rio_pl_set_pins_tclk_reg,
    rio_pl_set_pins_flag_reg,
    /*Values of dorrbel csr*/    
    rio_doorbell_CSR,
    /*Values of port write csr - the register is the same as for doorbell csr*/    
    rio_doorbell_CSR,
    /*registers for Values of gsm boundaries*/
    mem_gsm_space_ext_address,
    mem_gsm_space_start_address,
    mem_gsm_space_xamsbs,
    mem_gsm_space_dw_size,
    /*register for values of io space boundaries*/
    mem_io_space_xamsbs,
    mem_io_space_ext_address,
    mem_io_space_start_address,
    mem_io_space_dw_size,
    /*value for mailbox csr*/
    mailbox.mailbox_CSR);

    if (inst_Id != `RIO_PE_ERROR_INST_ID)
        $display ("PE Module:PE instance is created; id = %d", inst_Id);
    else
    begin
        $display("PE Module: Error during instance creating");
        $finish;
    end

    mailbox.inst_Id = inst_Id;
    memory.inst_Id = inst_Id;
//Coherence table initialization
    $HW_Put_Data_Pe (0, `RIO_INIT_PARAM_SET_REMOTE_DEV_ID, 0, 1 - inst_Id);

/*Initialization*/
    result = $HW_Rio_Init( inst_Id,
    `RIO_PE_TRANSP_TYPE,                //param_set_transp_type,
    inst_Id,                            //param_set_dev_id,
    0,                                  //param_set_lcshbar,
    0,                                  //param_set_lcsbar,
    `RIO_FALSE,                         //param_set_is_host,
    `RIO_TRUE,                          //param_set_is_master_enable,
    `RIO_FALSE,                         //param_set_is_discovered,
    `RIO_PE_CONFIG_SPACE_SIZE,          //param_set_config_space_size,
    `RIO_PE_EXT_ADDRESS_SUPPORT,        //param_set_ext_address,
    `RIO_PE_EXT_ADDRESS_SIZE_SELECTOR,  //param_set_ext_address_16,
    `RIO_PE_PARAM_SET_LP_EP_IS_8,       //param_set_lp_ep_is_8
    `RIO_PE_MODE_IS_8,                  //param_set_work_mode_is_8,
    `RIO_TRUE,                          //param_set_input_is_enable,
    `RIO_TRUE,                          //param_set_output_is_enable,
    `NUM_TRAININGS,                     //param_set_num_trainings,
    `RIO_REQ_WND_ALIGN,                 //param_set_requires_window_alignment,
    `RIO_PE_IDLE_LIM,                   //param_set_receive_idle_lim,
    `RIO_PE_IDLE_LIM,                   //param_set_throttle_idle_lim,
    `RIO_PE_RES_FOR_OUT,                //param_set_res_for_3_out,
    `RIO_PE_RES_FOR_OUT,                // param_set_res_for_2_out,
    `RIO_PE_RES_FOR_OUT,                // param_set_res_for_1_out,
    `RIO_PE_RES_FOR_IN,                 //param_set_res_for_3_in,
    `RIO_PE_RES_FOR_IN,                 //param_set_res_for_2_in,
    `RIO_PE_RES_FOR_IN,                 //param_set_res_for_1_in,
    `RIO_TRUE,                          //param_set_pass_by_prio
    `RIO_TRUE,                           //ext_Mailbox_Support
    `RIO_FALSE                          //Enable link request resending
    );

    if (result == `RIO_OK)
        $display ("PE Module:PE instance is initialized; id = %d", inst_Id);

    if (result != `RIO_OK)
    begin
        $display ("PE Module: Initialization failed; result = %d", result);
        $finish;
    end

    //Change memory map for current device
    mem_io_space_start_address = `RIO_PE_IO_START_ADDRESS * (inst_Id + 1);

    /*add register handlers for configuratuion request*/
    $HW_Add_Register_Handlers( inst_Id,
        dest_id_reg, enable_internal_io_routing_reg, 
        mailbox.mailbox_CSR, rio_doorbell_CSR, 
        reg_500, reg_504);
end




/***************************************************************************
 * Description: Creates Serial instance of models
 *
 * Notes: 
 *
 **************************************************************************/
initial
if (mode == `RIO_PE_INST_SERIAL_MODE)
begin
    #1


    inst_Id = $HW_RIO_Serial_Model_Create_Instance (
    `RIO_PE_ADDRESS_SUPPORT_CODE,       //param_ext_address_support
    `RIO_FALSE,                         //param_is_bridge
    `RIO_TRUE,                          //param_has_memory
    `RIO_TRUE,                          //param_has_processor
    `RIO_TRUE,                          //param_coh_granule_size_32
    2,                                  //param_coh_domain_size
    0,                                  //param_device_identity,
    1,                                  //param_device_vendor_identity,
    0,                                  //param_device_rev,
    0,                                  //param_device_minor_rev,
    0,                                  //param_device_major_rev,
    0,                                  //param_assy_identity,
    0,                                  //param_assy_vendor_identity,
    0,                                  //param_assy_rev,
    `RIO_PE_ENTRY_EXTENDED_FEATURES_PTR, //param_entry_extended_features_ptr,
    `RIO_PE_NEXT_EXTENDED_FEATURES_PTR, //param_next_extended_features_ptr,
    `RIO_PE_SOURCE_TRX,                 //param_source_trx,
    `RIO_PE_DEST_TRX,                   //param_dest_trx,
    `RIO_TRUE,                          //param_mbox1,
    `RIO_TRUE,                          //param_mbox2,
    `RIO_TRUE,                          //param_mbox3,
    `RIO_TRUE,                          //param_mbox4,
    `RIO_TRUE,                          //param_has_doorbell,
    `RIO_PE_INBOUND_BUF_SIZE,           //param_pl_inbound_buffer_size,
    `RIO_PE_OUTBOUND_BUF_SIZE,          //param_pl_outbound_buffer_size,
    `RIO_PE_INBOUND_BUF_SIZE,           //param_ll_inbound_buffer_size,
    `RIO_PE_OUTBOUND_BUF_SIZE,          //param_ll_outbound_buffer_size,
    /*registers handlers passing*/
    rio_gsm_request_done_trx_tag_reg,
    rio_gsm_request_done_result_reg,
    rio_gsm_request_done_err_code_reg,
    rio_gsm_request_done_dw_size_reg,
    rio_gsm_request_done_flag_reg,
    
    rio_io_request_done_trx_tag_reg,
    rio_io_request_done_result_reg,
    rio_io_request_done_err_code_reg,
    rio_io_request_done_dw_size_reg,
    rio_io_request_done_flag_reg,
    
    rio_mp_request_done_trx_tag_reg,
    rio_mp_request_done_result_reg,
    rio_mp_request_done_err_code_reg,
    rio_mp_request_done_flag_reg,

    rio_doorbell_request_done_trx_tag_reg,
    rio_doorbell_request_done_result_reg,
    rio_doorbell_request_done_err_code_reg,
    rio_doorbell_request_done_flag_reg,

    rio_config_request_done_trx_tag_reg,
    rio_config_request_done_result_reg,
    rio_config_request_done_err_code_reg,
    rio_config_request_done_dw_size_reg,
    rio_config_request_done_flag_reg,

    rio_snoop_request_address_reg,
    rio_snoop_request_extended_address_reg,
    rio_snoop_request_xamsbs_reg,
    rio_snoop_request_lttype_reg,
    rio_snoop_request_ttype_reg,
    rio_snoop_request_flag_reg,

    mailbox.rio_mailbox_msglen_reg,
    mailbox.rio_mailbox_ssize_reg,
    mailbox.rio_mailbox_letter_reg,
    mailbox.rio_mailbox_mbox_reg,
    mailbox.rio_mailbox_msgseg_reg,
    mailbox.rio_mailbox_dw_size_reg,
    mailbox.rio_mailbox_flag_reg,

    rio_doorbell_remote_request_doorbell_info_reg,
    rio_doorbell_remote_request_flag_reg,

    rio_port_write_remote_request_dw_size_reg,
    rio_port_write_remote_request_sub_dw_pos_reg,
    rio_port_write_remote_request_flag_reg,

    memory.rio_mem_address_reg,
    memory.rio_mem_extended_address_reg,
    memory.rio_mem_xamsbs_reg,
    memory.rio_mem_dw_size_reg,
    memory.rio_mem_req_type_reg,
    memory.rio_mem_is_gsm_reg,
    memory.rio_mem_be_reg,
    memory.rio_mem_flag_reg,

    rio_pl_set_pins_tlane0_reg, 
    rio_pl_set_pins_tlane1_reg, 
    rio_pl_set_pins_tlane2_reg, 
    rio_pl_set_pins_tlane3_reg, 
    rio_pl_set_serial_pins_flag_reg,
    /*Values of dorrbel csr*/    
    rio_doorbell_CSR,
    /*Values of port write csr - the register is the same as for doorbell csr*/    
    rio_doorbell_CSR,
    /*registers for Values of gsm boundaries*/
    mem_gsm_space_ext_address,
    mem_gsm_space_start_address,
    mem_gsm_space_xamsbs,
    mem_gsm_space_dw_size,
    /*register for values of io space boundaries*/
    mem_io_space_xamsbs,
    mem_io_space_ext_address,
    mem_io_space_start_address,
    mem_io_space_dw_size,
    /*value for mailbox csr*/
    mailbox.mailbox_CSR,

    /* registers for 10bit interface */
    rio_pl_set_pins_tlane0_10_reg, 
    rio_pl_set_pins_tlane1_10_reg, 
    rio_pl_set_pins_tlane2_10_reg, 
    rio_pl_set_pins_tlane3_10_reg, 
    rio_pl_set_serial_pins_10bit_flag_reg);

    if (inst_Id != `RIO_PE_ERROR_INST_ID)
        $display ("PE Module:PE instance is created; id = %d", inst_Id);
    else
    begin
        $display("PE Module: Error during instance creating");
        $finish;
    end

    mailbox.inst_Id = inst_Id;
    memory.inst_Id = inst_Id;
//Coherence table initialization
    $HW_Put_Data_Pe (0, `RIO_INIT_PARAM_SET_REMOTE_DEV_ID, 0, 1 - inst_Id);

/*Initialization*/
    result = $HW_Rio_Serial_Init( inst_Id,
    `RIO_PE_TRANSP_TYPE,                //param_set_transp_type,
    inst_Id,                            //param_set_dev_id,
    0,                                  //param_set_lcshbar,
    0,                                  //param_set_lcsbar,
    `RIO_FALSE,                         //param_set_is_host,
    `RIO_TRUE,                          //param_set_is_master_enable,
    `RIO_FALSE,                         //param_set_is_discovered,
    `RIO_PE_CONFIG_SPACE_SIZE,          //param_set_config_space_size,
    `RIO_PE_EXT_ADDRESS_SUPPORT,        //param_set_ext_address,
    `RIO_PE_EXT_ADDRESS_SIZE_SELECTOR,  //param_set_ext_address_16,
    `RIO_PE_PARAM_SET_SERIAL_IS_1X,     //param_set_lp_Serial_Is_1x
    `RIO_PE_IS_FORSE_1X_MODE,           //param_set_is_Force_1x_Mode
    `RIO_PE_IS_FORSE_1X_MODE_LANE0,     //param_set_is_Force_1x_Mode_Lane0
    `RIO_TRUE,                          //param_set_input_is_enable,
    `RIO_TRUE,                          //param_set_output_is_enable,
    `RIO_PE_SILENCE_PERIOD,             //param_set_silence_Period,
    `RIO_PE_COMP_SEQ_RATE,              //param_set_comp_Seq_Rate,
    `RIO_PE_STATUS_SYM_RATE,            //param_set_status_Sym_Rate
    `RIO_PE_DISCOVERY_PERIOD,           //discovery period
    `RIO_PE_RES_FOR_OUT,                //param_set_res_for_3_out,
    `RIO_PE_RES_FOR_OUT,                // param_set_res_for_2_out,
    `RIO_PE_RES_FOR_OUT,                // param_set_res_for_1_out,
    `RIO_PE_RES_FOR_IN,                 //param_set_res_for_3_in,
    `RIO_PE_RES_FOR_IN,                 //param_set_res_for_2_in,
    `RIO_PE_RES_FOR_IN,                 //param_set_res_for_1_in,
    `RIO_TRUE,                          //param_set_pass_by_prio
    `RIO_PE_TRANSMIT_FC_SUPPORT,        //transmit_Flow_Control_Support
    `RIO_TRUE,                           //ext_Mailbox_Support
    `RIO_FALSE                          //Enable link request resending
    );

    if (result == `RIO_OK)
        $display ("PE Module:PE instance is initialized; id = %d", inst_Id);

    if (result != `RIO_OK)
    begin
        $display ("PE Module: Initialization failed; result = %d", result);
        $finish;
    end

    //Change memory map for current device
    mem_io_space_start_address = `RIO_PE_IO_START_ADDRESS * (inst_Id + 1);

    /*add register handlers for configuratuion request*/
    $HW_Add_Register_Handlers( inst_Id,
        dest_id_reg, enable_internal_io_routing_reg, 
        mailbox.mailbox_CSR, rio_doorbell_CSR, 
        reg_500, reg_504);
end





/***************************************************************************
 * Description: Reset flag registers
 *
 * Notes: 
 *
 **************************************************************************/
initial
begin
    rio_gsm_request_done_flag_reg = 1'b0;
    rio_io_request_done_flag_reg = 1'b0;
    rio_mp_request_done_flag_reg = 1'b0;
    rio_doorbell_request_done_flag_reg = 1'b0;
    rio_config_request_done_flag_reg = 1'b0;
    rio_snoop_request_flag_reg = 1'b0;
    rio_mp_remote_request_flag_reg = 1'b0;
    rio_doorbell_remote_request_flag_reg = 1'b0;
    rio_port_write_remote_request_flag_reg = 1'b0;
    rio_pl_set_pins_flag_reg = 1'b0;
    rio_pl_set_serial_pins_flag_reg = 1'b0;
	rio_pl_set_serial_pins_10bit_flag_reg = 1'b0;

/*Additional registers initializing*/

    rio_doorbell_CSR = `PE_DOORBEL_CSR;

    mem_io_space_xamsbs = `RIO_PE_IO_XAMSBS;
    mem_io_space_ext_address = `RIO_PE_IO_EXT_ADDRESS;
    mem_io_space_start_address = `RIO_PE_IO_START_ADDRESS;
    mem_io_space_dw_size = `RIO_PE_IO_SPACE_DW_SIZE;

    mem_gsm_space_xamsbs = `RIO_PE_GSM_XAMSBS;
    mem_gsm_space_ext_address = `RIO_PE_GSM_EXT_ADDRESS;
    mem_gsm_space_start_address = `RIO_PE_GSM_START_ADDRESS;
    mem_gsm_space_dw_size = `RIO_PE_GSM_SPACE_DW_SIZE;         

    reset_pe = 1'b0;

    lane_changed = 1'b0;
/*initialization of registers for the IO packet routing*/
    enable_internal_io_routing_reg = `RIO_TRUE; 
    dest_id_reg = 0;
    inst_Id = `RIO_PE_ERROR_INST_ID;
end


endmodule
