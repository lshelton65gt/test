/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_pe\memory.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Implementation of memory
*
* Notes:        This memory is prepared for demonstration 
*               and testing for ATOMIC transactions
*
******************************************************************************/ 
`include "wrapper_pe.vh"
`include "memory_def.vh"

module Memory_Module;

//instance id of the processing element
integer inst_Id;

//memory array
reg [0 : `RIO_BITS_IN_DW - 1] pe_memory_array [0 : `CURR_MEMORY_SIZE_DW * `MEMORY_SIZE_DW];
//memory array for current operation
reg [0 : `RIO_BITS_IN_DW - 1] pe_curr_used_memory [0 : `CURR_MEMORY_SIZE_DW];

//temporary variables
reg [0 : `RIO_BITS_IN_WORD - 1] ms_word;
reg [0 : `RIO_BITS_IN_WORD - 1] ls_word;
reg [0 : `RIO_BITS_IN_DW - 1]   dword;
integer i;  //loop counter
integer k;  //loop counter
integer result; //result storage
//values for boundaries of subregister's value
integer first_byte_pos;
integer least_byte_pos; 
reg [0 : `RIO_BITS_IN_DW - 1] mask;   //used as mask for atomic INC and DEC operations
reg [0 : `RIO_BITS_IN_DW - 1] new_val;   //new value for test-and-swap transactions

//registers for acces to PLI
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mem_address_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mem_extended_address_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mem_xamsbs_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mem_dw_size_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mem_req_type_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mem_is_gsm_reg; 
reg [0 : `RIO_BITS_IN_BYTE - 1] rio_mem_be_reg; 
reg rio_mem_flag_reg; 

//reset register
reg reset_mem;

// Logical Layer delay
integer delay;

/***************************************************************************
 * Description: set initial values
 *
 * Notes: 
 *
 **************************************************************************/
initial
begin
    reset_mem = 1'b0;
	delay = 1;
end


/***************************************************************************
 * Description: reset memory
 *
 * Notes: 
 *
 **************************************************************************/
always
begin
    wait (reset_mem === 1'b1);
    disable MEMORY_REQUEST_PROCESSING;
    reset_mem = 1'b0;
end


/***************************************************************************
 * Function : Load_Curr_Mem
 *
 * Description: Load part of memory to the work array
 *
 * Returns:
 *
 * Notes: This function uses only address parameter
 *
 **************************************************************************/
function Load_Curr_Mem;
input address;
input ext_address;
input [0 : `XAMSBS_SIZE - 1]xamsbs;
integer address;
integer ext_address;
//local variables
integer i;
    begin
        for (i = 0; i < `CURR_MEMORY_SIZE_DW; i = i + 1)
            pe_curr_used_memory[i] = pe_memory_array[(address / `RIO_BYTES_IN_DW) + i];
    end
endfunction

/***************************************************************************
 * Function : Save_Curr_Mem
 *
 * Description: Save work array to a memory
 *
 * Returns:
 *
 * Notes: This function uses only address parameter
 *
 **************************************************************************/
function Save_Curr_Mem;
input address;
input ext_address;
input [0 : `XAMSBS_SIZE - 1]xamsbs;
integer address;
integer ext_address;
//local variables
integer i;
    begin
        for (i = 0; i < `CURR_MEMORY_SIZE_DW; i = i + 1)
            pe_memory_array[(address  / `RIO_BYTES_IN_DW) + i] = pe_curr_used_memory[i]; 
    end
endfunction

/***************************************************************************
 * Function : Write_Data
 *
 * Description: Put on the link data wich no contains in the pe_curr_used_memory array
 *
 * Returns:
 *
 * Notes:
 *
 **************************************************************************/
function Write_Data;
input dw_size;
integer dw_size;
integer i;
    begin
        for (i = 0; i < dw_size; i = i + 1)
        begin
            dword = pe_curr_used_memory[i];
            ms_word = dword[0 : `RIO_BITS_IN_WORD - 1];
            ls_word = dword[`RIO_BITS_IN_WORD : `RIO_BITS_IN_DW - 1];
            $HW_Put_Data_Pe (inst_Id, `RIO_SET_MEM_DATA_DW, i, ms_word, ls_word);
        end
        Write_Data = $HW_Rio_Set_Mem_Data(inst_Id);
    end
endfunction
/***************************************************************************
 * Description: memory access implementation
 *
 * Notes: 
 *
 **************************************************************************/
always
begin : MEMORY_REQUEST_PROCESSING
    wait (rio_mem_flag_reg === 1'b1);
        rio_mem_flag_reg = 1'b0;

        #(delay);  /* LOGICAL LAYER DELAY */

        //load memory to work array
        result = Load_Curr_Mem(rio_mem_address_reg, rio_mem_extended_address_reg, rio_mem_xamsbs_reg);
        /*@@@@ Insert something code here @@@@ = rio_mem_address_reg;*/ 
        /*@@@@ Insert something code here @@@@ = rio_mem_extended_address_reg;*/ 
        /*@@@@ Insert something code here @@@@ = rio_mem_xamsbs_reg;*/ 
        /*@@@@ Insert something code here @@@@ = rio_mem_dw_size_reg;*/ 
        /*@@@@ Insert something code here @@@@ = rio_mem_req_type_reg;*/ 
        /*@@@@ Insert something code here @@@@ = rio_mem_is_gsm_reg;*/ 
        /*@@@@ Insert something code here @@@@ = rio_mem_be_reg;*/ 

        $display ("PE%d:    Memory request to address %d", inst_Id, rio_mem_address_reg);
//OPERATIONS for read memory - assumed that all operations is read from the same address
        if (rio_mem_req_type_reg === `RIO_MEM_READ || 
            rio_mem_req_type_reg === `RIO_MEM_WRAP_READ)
        begin
            if (Write_Data(rio_mem_dw_size_reg) != `RIO_OK) $display("Set_Mem_Data not OK"); 
        end
//ATOMIC SET operaion
        else if (rio_mem_req_type_reg === `RIO_MEM_ATOMIC_SET)
        begin
            //return previous value
            if (Write_Data(rio_mem_dw_size_reg) != `RIO_OK) $display("Set_Mem_Data not OK"); 
         /* rio_mem_be_reg - in case of the subdoubleword access requires individual
        bytes of the doubleword to be read or written this variable should has bits
        corresponding to that bytes set. Correspondence is treated in the following
        way: most significant bit of this variable corresponds to the most significant
        byte of the doubleword and so on. Value of this variable is undefined in case
        of the multiple doubleword memory access.*/
            for (i = 0; i < `RIO_BITS_IN_BYTE; i = i + 1) if (rio_mem_be_reg[i] === 1'b1)
                pe_curr_used_memory[0] = pe_curr_used_memory[0] | (`ALL_1 << (`RIO_BITS_IN_BYTE * (`RIO_BYTES_IN_DW - 1 - i)) );
        end
        else if (rio_mem_req_type_reg === `RIO_MEM_ATOMIC_CLEAR)
        begin
            //return previous value
            if (Write_Data(rio_mem_dw_size_reg) != `RIO_OK) $display("Set_Mem_Data not OK"); 
         /* rio_mem_be_reg - in case of the subdoubleword access requires individual
        bytes of the doubleword to be read or written this variable should has bits
        corresponding to that bytes set. Correspondence is treated in the following
        way: most significant bit of this variable corresponds to the most significant
        byte of the doubleword and so on. Value of this variable is undefined in case
        of the multiple doubleword memory access.*/
            for (i = 0; i < `RIO_BITS_IN_BYTE; i = i + 1) if (rio_mem_be_reg[i] === 1'b1)
            begin
                dword = `LSW_BYTE_IS_0;
                for (k = 0; k < `RIO_BYTES_IN_DW - i - 1; k = k + 1)
                     dword = (dword << `RIO_BITS_IN_BYTE) | `ALL_1;
                pe_curr_used_memory[0] = pe_curr_used_memory[0] & dword;
            end
                
        end
        else if (rio_mem_req_type_reg === `RIO_MEM_ATOMIC_INC || 
                 rio_mem_req_type_reg === `RIO_MEM_ATOMIC_DEC)
        begin
            //return previous value
            if (Write_Data(rio_mem_dw_size_reg) != `RIO_OK) $display("Set_Mem_Data not OK"); 
            //increment the specified data and pass it to the 
            /*at begin find the boundaries of incremented area then get this value and increment after 
            this incremented value writes back*/
            if (rio_mem_be_reg === `RIO_BITS_IN_BYTE'h0)
                $display ("Mem:Error be value - transaction will be ignore");
            else
            begin
                i = 0;
                while (i < `RIO_BITS_IN_BYTE && rio_mem_be_reg[i] !== 1'b1) i = i + 1;
                first_byte_pos = i;
                while (i < `RIO_BITS_IN_BYTE && rio_mem_be_reg[i] !== 1'b0) i = i + 1;
                least_byte_pos = i;
                //boundaries detected now will be increment dword
                dword = pe_curr_used_memory[0];
                //shift right before increment
                dword = dword >> ( (`RIO_BYTES_IN_DW - least_byte_pos) * `RIO_BITS_IN_BYTE);
                //change the value
                if (rio_mem_req_type_reg === `RIO_MEM_ATOMIC_INC) dword = dword + 1;
                if (rio_mem_req_type_reg === `RIO_MEM_ATOMIC_DEC) dword = dword - 1;
                 //remove left bits which are not hit to boundaries of subword
                //for this purpose prepare mask
                mask = `RIO_BITS_IN_DW'h0;
                for (i = 0; i < least_byte_pos - first_byte_pos; i = i + 1)
                    mask = (mask << `RIO_BITS_IN_BYTE) | `ALL_1;
                //apply mask to value
                dword = dword & mask;
                //shift result to previous place 
                dword = dword << ( (`RIO_BYTES_IN_DW - least_byte_pos) * `RIO_BITS_IN_BYTE);
                /*set values of dword var. which are not included to the boundaries to values 
                same as values of memory i.e:
                dword[0:first_byte_pos] = pe_curr_used_memory[0:first_byte_pos];
                dword[least_byte_pos : 63] = pe_curr_used_memory[least_byte_pos : 63];*/

                //prepare mask for least significant bits
                mask = `RIO_BITS_IN_DW'h0;
                for (i = 0; i < `RIO_BYTES_IN_DW - least_byte_pos; i = i + 1)
                    mask = (mask << `RIO_BITS_IN_BYTE) | `ALL_1;
                //apply mask
                dword = dword | (pe_curr_used_memory[0] & mask);

                //prepare mask for the most significant bits
                mask = `RIO_BITS_IN_DW'h0; 
                mask = ~mask;
                mask = mask << (`RIO_BITS_IN_BYTE * (`RIO_BITS_IN_BYTE - first_byte_pos));
                dword = dword | (pe_curr_used_memory[0] & mask);

                //put result back to memory
                pe_curr_used_memory[0] = dword;

            end
        end

        else if (rio_mem_req_type_reg === `RIO_MEM_ATOMIC_TSWAP)
        begin : MEM_ATOMIC_TSW_SECTION
  
            if ($HW_Rio_Get_Mem_Data(inst_Id) != `RIO_OK)
            begin
                $display("Memory: Error: Get_Mem_Data not OK");
                $finish;
            end

            //return previous value
            if (Write_Data(rio_mem_dw_size_reg) != `RIO_OK) 
            begin
                $display("Memory: Error: Set_Mem_Data not OK");
                $finish;
            end


            //get new value 
            new_val = {$HW_Get_Data_Pe (inst_Id, `RIO_GET_MEM_DATA_DW, 0, `MSW),
                       $HW_Get_Data_Pe (inst_Id, `RIO_GET_MEM_DATA_DW, 0, `LSW)};
            /*at begin find the boundaries of incremented area then get this value and change, after 
            this incremented value writes back*/
            if (rio_mem_be_reg === `RIO_BITS_IN_BYTE'h0)
                $display ("Mem:Error `be` value - transaction will be ignore");
            else
            begin
                i = 0;
                while (i < `RIO_BITS_IN_BYTE && rio_mem_be_reg[i] !== 1'b1) i = i + 1;
                first_byte_pos = i;
                while (i < `RIO_BITS_IN_BYTE && rio_mem_be_reg[i] !== 1'b0) i = i + 1;
                least_byte_pos = i;
                //boundaries detected now will be increment dword
                dword = pe_curr_used_memory[0];
                //shift right before increment
                dword = dword >> ( (`RIO_BYTES_IN_DW - least_byte_pos) * `RIO_BITS_IN_BYTE);
                new_val = new_val >> ( (`RIO_BYTES_IN_DW - least_byte_pos) * `RIO_BITS_IN_BYTE);
                 //remove left bits which are not hit to boundaries of subword
                //for this purpose prepare mask
                mask = `RIO_BITS_IN_DW'h0; 
                for (i = 0; i < least_byte_pos - first_byte_pos; i = i + 1)
                    mask = (mask << `RIO_BITS_IN_BYTE) | `ALL_1;
                //apply mask to value
                dword = dword & mask;
                new_val = new_val & mask;
                //if zero - then change the value
                //else - do nothing 
                if (dword === `RIO_BITS_IN_DW'h0) dword = new_val;
                else disable MEM_ATOMIC_TSW_SECTION;
                //shift result to previous place 
                dword = dword << ( (`RIO_BYTES_IN_DW - least_byte_pos) * `RIO_BITS_IN_BYTE);
                /*set values of dword var. which are not included to the boundaries to values 
                same as values of memory i.e:
                dword[0:first_byte_pos] = pe_curr_used_memory[0:first_byte_pos];
                dword[least_byte_pos : 63] = pe_curr_used_memory[least_byte_pos : 63];*/

                //prepare mask for least significant bits
                mask = `RIO_BITS_IN_DW'h0;
                for (i = 0; i < `RIO_BYTES_IN_DW - least_byte_pos; i = i + 1)
                    mask = (mask << `RIO_BITS_IN_BYTE) | `ALL_1;
                //apply mask
                dword = dword | (pe_curr_used_memory[0] & mask);

                //prepare mask for the most significant bits
                mask = `RIO_BITS_IN_DW'h0; 
                mask = ~mask;
                mask = mask << (`RIO_BITS_IN_BYTE * (`RIO_BITS_IN_BYTE - first_byte_pos));
                dword = dword | (pe_curr_used_memory[0] & mask);

                //put result back to memory
                pe_curr_used_memory[0] = dword;

            end
        end
        else //writes to memory
        begin

            if ($HW_Rio_Get_Mem_Data( inst_Id) == `RIO_OK)
            begin
                //if payload have not sub double word size then dw_size > 1 or dw_size = 1 and all sub-bytes
                //need to be written - in this case least 8 bits of rio_mem_be_reg is sets to 1
                if ( (rio_mem_dw_size_reg > 1) || 
                     (rio_mem_dw_size_reg === 1 && rio_mem_be_reg === `PLI_LONG_REGISTER_SIZE'b1111_1111))
                for (i = 0; i < rio_mem_dw_size_reg; i = i + 1)
                    begin
                        ms_word = $HW_Get_Data_Pe (inst_Id, `RIO_GET_MEM_DATA_DW, i, `MSW);
                        ls_word = $HW_Get_Data_Pe (inst_Id, `RIO_GET_MEM_DATA_DW, i, `LSW);
                        pe_curr_used_memory[i] = {ms_word, ls_word};
                    end
                else //if necessary write sub-doubleword value
                begin 
                    dword = pe_curr_used_memory[0];
                    //get bytes need to be written
                    ms_word = $HW_Get_Data_Pe (inst_Id, `RIO_GET_MEM_DATA_DW, 0, `MSW);
                    ls_word = $HW_Get_Data_Pe (inst_Id, `RIO_GET_MEM_DATA_DW, 0, `LSW);
                    
                    if (rio_mem_be_reg[0] === 1'b1) 
                        dword[0 : `RIO_BITS_IN_BYTE - 1] = ms_word[0 : `RIO_BITS_IN_BYTE - 1];
                    if (rio_mem_be_reg[1] === 1'b1) 
                        dword[`RIO_BITS_IN_BYTE : `RIO_BITS_IN_BYTE * 2 - 1] = 
                            ms_word[`RIO_BITS_IN_BYTE : `RIO_BITS_IN_BYTE * 2 - 1];
                    if (rio_mem_be_reg[2] === 1'b1) 
                        dword[`RIO_BITS_IN_BYTE * 2 : `RIO_BITS_IN_BYTE * 3 - 1] = 
                            ms_word[`RIO_BITS_IN_BYTE * 2 : `RIO_BITS_IN_BYTE * 3 - 1];
                    if (rio_mem_be_reg[3] === 1'b1) 
                        dword[`RIO_BITS_IN_BYTE * 3 : `RIO_BITS_IN_BYTE * 4 - 1] = 
                            ms_word[`RIO_BITS_IN_BYTE * 3 : `RIO_BITS_IN_BYTE * 4 - 1];

                    if (rio_mem_be_reg[4] === 1'b1) 
                        dword[`RIO_BITS_IN_BYTE * 4 : `RIO_BITS_IN_BYTE * 5 - 1] = 
                            ls_word[0 : `RIO_BITS_IN_BYTE - 1];
                    if (rio_mem_be_reg[5] === 1'b1) 
                        dword[`RIO_BITS_IN_BYTE * 5 : `RIO_BITS_IN_BYTE * 6 - 1] = 
                            ls_word[`RIO_BITS_IN_BYTE : `RIO_BITS_IN_BYTE * 2 - 1];
                    if (rio_mem_be_reg[6] === 1'b1) 
                        dword[`RIO_BITS_IN_BYTE * 6 : `RIO_BITS_IN_BYTE * 7 - 1] = 
                            ls_word[`RIO_BITS_IN_BYTE * 2 : `RIO_BITS_IN_BYTE * 3 - 1];
                    if (rio_mem_be_reg[7] === 1'b1) 
                        dword[`RIO_BITS_IN_BYTE * 7 : `RIO_BITS_IN_BYTE * 8 - 1] = 
                            ls_word[`RIO_BITS_IN_BYTE * 3 : `RIO_BITS_IN_BYTE * 4 - 1];

                    pe_curr_used_memory[0] = dword;
                end 
            end
            else
            begin
                $display("PE memory - error during payload data getting");
            end
        end
        //save memory to work array
        result = Save_Curr_Mem(rio_mem_address_reg, rio_mem_extended_address_reg, rio_mem_xamsbs_reg);

end


endmodule
