/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: M:\zkg_rio\riosuite\src\verilog_env\ts_tb\module_pe\mailbox.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Implementation of mailboxes
*
* Notes:        This mailbox not allowed recept multiply letters to the same mailbox at the same time
*
******************************************************************************/ 
module Mailbox_Module;

`include "mailbox_def.vh"
`include "wrapper_pe.vh"


reg [0 : `RIO_BITS_IN_WORD - 1] mailbox_CSR;  //Mailbox control status register


//registers for access to PLI
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mailbox_msglen_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mailbox_ssize_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mailbox_letter_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mailbox_mbox_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mailbox_msgseg_reg; 
reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mailbox_dw_size_reg; 
reg rio_mailbox_flag_reg; 


/*arrays for mailboxes*/
reg [0 : `RIO_BITS_IN_DW - 1] mailbox0_memory [0 : `MAX_SEGMENT_SIZE * `MAX_MSGSEG_NUM - 1];
reg [0 : `RIO_BITS_IN_DW - 1] mailbox1_memory [0 : `MAX_SEGMENT_SIZE * `MAX_MSGSEG_NUM - 1];
reg [0 : `RIO_BITS_IN_DW - 1] mailbox2_memory [0 : `MAX_SEGMENT_SIZE * `MAX_MSGSEG_NUM - 1];
reg [0 : `RIO_BITS_IN_DW - 1] mailbox3_memory [0 : `MAX_SEGMENT_SIZE * `MAX_MSGSEG_NUM - 1];

//Value of PE inst id
integer inst_Id;
//reset register 
reg reset_mbox;
//for result
integer result;




/***************************************************************************
 * Description: Set initial values for all registers
 *
 * Notes:
 *
 **************************************************************************/
initial
begin
    mailbox_CSR = `ALL_MBOX_EMPTY;
    reset_mbox = 1'b0;
end




/***************************************************************************
 * Description: This always waiting for new message and put it to the memory
 *
 * Notes:
 *
 **************************************************************************/
always
begin : MESSAGE_ACCEPTOR
    wait (rio_mailbox_flag_reg === 1'b1);
        rio_mailbox_flag_reg = 1'b0;
/*  For first segment necessary set CSR to buisy value 
    this CSR is used by wrapper for send RETRY responce for other message*/
    if (rio_mailbox_msgseg_reg === 0)
        mailbox_CSR[`BUSY_BIT_OFFS_L +  rio_mailbox_mbox_reg  * `BITS_FOR_ONE_MBOX - 1] = 1'b1;  

    result = Put_Data_To_Mbox(rio_mailbox_mbox_reg, rio_mailbox_msgseg_reg, rio_mailbox_msglen_reg, rio_mailbox_dw_size_reg);
/* if this is last message then necessary reset buisy flag*/
    if (rio_mailbox_msgseg_reg === rio_mailbox_msglen_reg)
    begin
    //Dealy for emulate working time
        #(`MBOX_WORK_DELAY)
        mailbox_CSR[`BUSY_BIT_OFFS_L +  rio_mailbox_mbox_reg  * `BITS_FOR_ONE_MBOX - 1] = 1'b0;  
    end
    /*@@@@ Insert something code here @@@@ = rio_mailbox_msglen_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_mailbox_ssize_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_mailbox_letter_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_mailbox_mbox_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_mailbox_msgseg_reg;*/ 
    /*@@@@ Insert something code here @@@@ = rio_mailbox_dw_size_reg;*/ 

end


/***************************************************************************
 * Function : Put_Data_To_Mbox
 *
 * Description: Puts data from internal PLI buffer to the proper mailbox
 *
 * Returns:
 *
 * Notes:
 *
 **************************************************************************/
function Put_Data_To_Mbox;
input [0 : `MBOX_FIELD_SIZE - 1] mbox;
input [0 : `MSGSEG_FIELD_SIZE - 1] msgseg;
input [0 : `MSGLEN_FIELD_SIZE - 1] msglen;
input dw_size;

integer dw_size;
//local variavbles
integer i;
reg [0 : `RIO_BITS_IN_WORD - 1] ms_word;
reg [0 : `RIO_BITS_IN_WORD - 1] ls_word;

    begin
        for (i = 0; i < dw_size; i = i + 1)
        begin
            ms_word = $HW_Get_Data_Pe (inst_Id, `RIO_MP_REMOTE_REQUEST_MESSAGE_DATA, i, `MSW);
            ls_word = $HW_Get_Data_Pe (inst_Id, `RIO_MP_REMOTE_REQUEST_MESSAGE_DATA, i, `LSW);
            case (mbox)
                `MBOX0:
                begin
                    mailbox0_memory[msglen * msgseg + i] = {ms_word, ls_word};
                end
                `MBOX1:
                begin
                    mailbox1_memory[msglen * msgseg + i] = {ms_word, ls_word};
                end
                `MBOX2:
                begin
                    mailbox2_memory[msglen * msgseg + i] = {ms_word, ls_word};
                end
                `MBOX3:
                begin
                    mailbox3_memory[msglen * msgseg + i] = {ms_word, ls_word};
                end
            endcase
        end
        Put_Data_To_Mbox = 1'b0;
    end

endfunction


/***************************************************************************
 * Description: Performs mailbox reset
 *
 * Notes:
 *
 **************************************************************************/
always 
begin
    wait (reset_mbox === 1'b1);
    reset_mbox = 1'b0;
    disable MESSAGE_ACCEPTOR;
    mailbox_CSR = `ALL_MBOX_EMPTY;
end

endmodule
