/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: M:\zkg_rio\riosuite\src\verilog_env\ts_tb\module_pe\memory_def.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Defines for the memory module
*
* Notes:        This memory is prepared for demonstration 
*               and testing for ATOMIC transactions
*
******************************************************************************/ 


`define MEMORY_SIZE_DW              100         /*size of memory in doublewords*/
`define CURR_MEMORY_SIZE_DW         32          /*size of current used memory array*/
`define ALL_1                       8'hFF       /*byte with all bits = 1*/
`define LSW_BYTE_IS_0               64'hFFFF_FFFF_FFFF_FF00
`define XAMSBS_SIZE                 2           /*size of xamsbs field*/