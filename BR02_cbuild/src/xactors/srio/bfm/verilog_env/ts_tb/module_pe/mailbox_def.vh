/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: M:\zkg_rio\riosuite\src\verilog_env\ts_tb\module_pe\mailbox_def.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Defines for mailbox module
*
* Notes:
*
******************************************************************************/ 

//dealys
`define MBOX_WORK_DELAY                 100     /*working delay*/
//sizes of message
`define MAX_MSGSEG_NUM                  16      /*maximum segments in the message*/
`define MAX_SEGMENT_SIZE                32      /*maximum DW's in the segment*/

//Values for acces to CSR
`define BUSY_BIT_OFFS                  5       /*number of busy bit ofset*/
`define BUSY_BIT_OFFS_L                4       /*number of busy bit ofset from left*/
`define BITS_FOR_ONE_MBOX               8       /*number of bits for one mailbox info in the CSR*/


//initial values
`define ALL_MBOX_EMPTY                  32'hA0A0A0A0

//field size defenition
`define MBOX_FIELD_SIZE                 2
`define MSGSEG_FIELD_SIZE               4
`define MSGLEN_FIELD_SIZE               4

//mailbox names
`define MBOX0                           0
`define MBOX1                           1
`define MBOX2                           2
`define MBOX3                           3