/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\top_pe-pe\top.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  module for RIO PE Models 
*
* Notes:
*
******************************************************************************/
`include "wrapper_pe.vh"

`define WIRE_WIDTH          8
`define HALF_CLOCK_PERIOD   10
`define WAIT_FOR_INITIALIZE 2

module Top;

    // clock
    reg sys_CLK;

    // LP-EP link
    wire tclk;
    wire tframe;
    wire [0 : `WIRE_WIDTH - 1] td;
    wire [0 : `WIRE_WIDTH - 1] tdl;
    wire rclk;    
    wire rframe;
    wire [0 : `WIRE_WIDTH - 1] rd;
    wire [0 : `WIRE_WIDTH - 1] rdl;

    //Serial link

    wire rlane0;
    wire rlane1;
    wire rlane2;
    wire rlane3;

    wire tlane0;
    wire tlane1;
    wire tlane2;
    wire tlane3;

`ifdef PARALLEL_SCENARIO
    PE_Module #(`RIO_PE_INST_PAR_MODE) PE0 
`else
    PE_Module #(`RIO_PE_INST_SERIAL_MODE) PE0 
`endif
        (
        .clock(sys_CLK),
        .tclk(tclk), 
        .tframe(tframe),
        .td(td), 
        .tdl(tdl), 
        .rclk(rclk), 
        .rframe(rframe), 
        .rd(rd), 
        .rdl(rdl),
//serial link connection
        .rlane0(rlane0),
        .rlane1(rlane1),
        .rlane2(rlane2),
        .rlane3(rlane3),

        .tlane0(tlane0),
        .tlane1(tlane1),
        .tlane2(tlane2),
        .tlane3(tlane3) 
        );

`ifdef PARALLEL_SCENARIO
    PE_Module #(`RIO_PE_INST_PAR_MODE) PE1
`else
    PE_Module #(`RIO_PE_INST_SERIAL_MODE) PE1
`endif
        (
        .clock(sys_CLK),
        .tclk(rclk), 
        .tframe(rframe),
        .td(rd), 
        .tdl(rdl), 
        .rclk(tclk), 
        .rframe(tframe), 
        .rd(td), 
        .rdl(tdl),
//serial link connection
        .rlane0(tlane0),
        .rlane1(tlane1),
        .rlane2(tlane2),
        .rlane3(tlane3),

        .tlane0(rlane0),
        .tlane1(rlane1),
        .tlane2(rlane2),
        .tlane3(rlane3) 
        );


/***************************************************************************
 * Description: clock generator
 *
 * Notes:
 *
 **************************************************************************/
initial 
begin
    #(`WAIT_FOR_INITIALIZE);  //Necessary delay for initializing all devices
    sys_CLK = 0;            //reset clock generator
    while (1)               //Sart clocking
    begin
        #(`HALF_CLOCK_PERIOD);
        sys_CLK = ~sys_CLK; 
    end 
end




/***************************************************************************
 * Description: Min cycle
 *
 * Notes: starts scenarios from the created fabrics
 *
 **************************************************************************/
initial
begin
    $display ("The scenario is started");
    $HW_Init(2 /*total number of PE in demo*/);
    #(`WAIT_FOR_INITIALIZE);  //Waiting for initialize

    PE0.start_small_scenario = 1'b1;
    PE1.start_scenario = 1'b1;
    wait(PE1.start_scenario === 1'b0); 

    #(`HALF_CLOCK_PERIOD); //Waiting for finish
    $display ("The scenario is finished");
    $HW_Close;
    $finish;

end


endmodule
