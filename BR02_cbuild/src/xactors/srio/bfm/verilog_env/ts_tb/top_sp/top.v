/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/verilog_env/ts_tb/top_sp/top.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Testbench which contain connection between 2 serial pe models 
*               2 parallel pe models and serial txrx and serial pe models
*
* Notes: This is only demonstration of usage models in the serial and parallel mode
*        in the same testbench
*
******************************************************************************/
`include "wrapper_pe.vh"
`include "wrapper_txrx.vh"

`define HALF_CLOCK_PERIOD   10
`define WAIT_FOR_INITIALIZE 2

module Top;

    // clock
    reg sys_CLK;

    // LP-EP link
    wire tclk;
    wire tframe;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;
    wire rclk;    
    wire rframe;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    wire [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    //Serial link

    wire rlane0;
    wire rlane1;
    wire rlane2;
    wire rlane3;

    wire tlane0;
    wire tlane1;
    wire tlane2;
    wire tlane3;
//wires for the serial txrx-pe connection
    wire txrx_rlane0;
    wire txrx_rlane1;
    wire txrx_rlane2;
    wire txrx_rlane3;

    wire txrx_tlane0;
    wire txrx_tlane1;
    wire txrx_tlane2;
    wire txrx_tlane3;


//instantination of parallel models
    PE_Module #(`RIO_PE_INST_PAR_MODE) PE0_Par 
        (
        .clock(sys_CLK),
        .tclk(tclk), 
        .tframe(tframe),
        .td(td), 
        .tdl(tdl), 
        .rclk(rclk), 
        .rframe(rframe), 
        .rd(rd), 
        .rdl(rdl)
        );

    PE_Module #(`RIO_PE_INST_PAR_MODE) PE1_Par
        (
        .clock(sys_CLK),
        .tclk(rclk), 
        .tframe(rframe),
        .td(rd), 
        .tdl(rdl), 
        .rclk(tclk), 
        .rframe(tframe), 
        .rd(td), 
        .rdl(tdl)
        );

//instantination of serial models
    PE_Module #(`RIO_PE_INST_SERIAL_MODE) PE0_Serial
        (
        .clock(sys_CLK),
        .rlane0(rlane0),
        .rlane1(rlane1),
        .rlane2(rlane2),
        .rlane3(rlane3),

        .tlane0(tlane0),
        .tlane1(tlane1),
        .tlane2(tlane2),
        .tlane3(tlane3) 
        );

    PE_Module #(`RIO_PE_INST_SERIAL_MODE) PE1_Serial
        (
        .clock(sys_CLK),
        .rlane0(tlane0),
        .rlane1(tlane1),
        .rlane2(tlane2),
        .rlane3(tlane3),

        .tlane0(rlane0),
        .tlane1(rlane1),
        .tlane2(rlane2),
        .tlane3(rlane3) 
        );

//connection txrx and PE model
    PE_Module #(`RIO_PE_INST_SERIAL_MODE) PE2_Serial
        (
        .clock(sys_CLK),
        .rlane0(txrx_rlane0),
        .rlane1(txrx_rlane1),
        .rlane2(txrx_rlane2),
        .rlane3(txrx_rlane3),

        .tlane0(txrx_tlane0),
        .tlane1(txrx_tlane1),
        .tlane2(txrx_tlane2),
        .tlane3(txrx_tlane3) 
        );

    TXRX_Module #(`RIO_TXRX_INST_SERIAL_MODE) TxRx_Serial
        (
        .clock(sys_CLK),
        .rlane0(txrx_tlane0),
        .rlane1(txrx_tlane1),
        .rlane2(txrx_tlane2),
        .rlane3(txrx_tlane3),

        .tlane0(txrx_rlane0),
        .tlane1(txrx_rlane1),
        .tlane2(txrx_rlane2),
        .tlane3(txrx_rlane3) 
        );


/***************************************************************************
 * Description: clock generator
 *
 * Notes:
 *
 **************************************************************************/
initial 
begin
    #(`WAIT_FOR_INITIALIZE);  //Necessary delay for initializing all devices
    sys_CLK = 0;            //reset clock generator
    while (1)               //Sart clocking
    begin
        #(`HALF_CLOCK_PERIOD);
        sys_CLK = ~sys_CLK; 
    end 
end




/***************************************************************************
 * Description: Main work cycle
 *
 * Notes: starts scenarios from the created PE models
 *
 **************************************************************************/
initial
begin
    $display ("The scenario is started");
    $HW_Init(5 /*total number of PE in demo*/);
    $HW_TxRx_Init(1 /*total number of TxRx in demo*/);
    #(`WAIT_FOR_INITIALIZE);  //Waiting for initialize

//    PE0_Par.start_scenario = 1'b1;
//    PE0_Serial.start_scenario = 1'b1;
    TxRx_Serial.simple_scenario = 1'b1;


/*    wait(PE0_Par.start_scenario === 1'b0); 
    wait(PE0_Serial.start_scenario === 1'b0); 
    wait(TxRx_Serial.simple_scenario === 1'b0);*/

#150000;

    #(`HALF_CLOCK_PERIOD); //Waiting for finish
    $display ("The scenario is finished");
    $HW_Close;
    $HW_TxRx_Close;
    $finish;

end


endmodule
