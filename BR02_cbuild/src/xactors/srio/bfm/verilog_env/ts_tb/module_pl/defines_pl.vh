/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_pl\defines_pl.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Defines for creating PE instance
*
* Notes:        
*
******************************************************************************/

//------------------------------------------------------------------------
//                  Address configuration settings              
//------------------------------------------------------------------------

/*Device ID for current demonstration*/
`define PL_ID     1 
// Allowed full address sizes:
// Possible values:
//   `RIO_PL_SUPPORTS_66_50_34_ADDR      
//   `RIO_PL_SUPPORTS_66_34_ADDR         
//   `RIO_PL_SUPPORTS_50_34_ADDR         
//   `RIO_PL_SUPPORTS_34_ADDR            
`define RIO_PL_ADDRESS_SUPPORT_CODE            `RIO_PL_SUPPORTS_66_50_34_ADDR
// Extended address enabled/disabled: true - enabled, false - disabled 
`define RIO_PL_EXT_ADDRESS_SUPPORT             `RIO_FALSE
// Extended address size (if enabled): true - 16 bits, false - 32 bits
`define RIO_PL_EXT_ADDRESS_SIZE_SELECTOR       `RIO_FALSE                         

//------------------------------------------------------------------------
//                  Extended features configuration settings              
//------------------------------------------------------------------------
`define RIO_PL_ENTRY_EXTENDED_FEATURES_PTR      'h100
`define RIO_PL_NEXT_EXTENDED_FEATURES_PTR       'h1000


//------------------------------------------------------------------------
//                  TRX constants
//------------------------------------------------------------------------
`define RIO_PL_SOURCE_TRX                       32'hFFFFFFF0
`define RIO_PL_DEST_TRX                         32'hFFFFFFF0

//------------------------------------------------------------------------
//                  BUFFER sizes
//------------------------------------------------------------------------
`define RIO_PL_INBOUND_BUF_SIZE                 12
`define RIO_PL_OUTBOUND_BUF_SIZE                12

//------------------------------------------------------------------------
//                  IO space boiundaries
//------------------------------------------------------------------------
`define RIO_TRANSP_16                           0
`define RIO_TRANSP_32                           1
`define RIO_PL_TRANSP_T                         `RIO_TRANSP_16

//------------------------------------------------------------------------
//                  Window alignment parameters
//------------------------------------------------------------------------
`define RIO_REQ_WND_ALIGN                       `RIO_TRUE
`define NUM_TRAININGS                           256

//------------------------------------------------------------------------
//                  Additional parameters
//------------------------------------------------------------------------
`define RIO_PL_MODE_IS_8                        1  /*can be 1 or 0*/
`define RIO_PL_PARAM_SET_LP_EP_IS_8             1  /*can be 1 or 0*/
`define RIO_PL_IDLE_LIM                         10000
`define RIO_PL_RES_FOR_OUT                      3
`define RIO_PL_RES_FOR_IN                       3
`define RIO_PL_ALL_MBOX_AVAILABLE               'hA0A0A0A0
`define RIO_WAIT_BEFORE_RETRY                   10
`define RIO_PL_ALL_LANES_CHANGED                4'b1111

//serial link parameters
`define RIO_PL_PARAM_SET_SERIAL_IS_1X           `RIO_FALSE
`define RIO_PL_IS_FORSE_1X_MODE                 `RIO_FALSE
`define RIO_PL_IS_FORSE_1X_MODE_LANE0           `RIO_FALSE
`define RIO_PL_SILENCE_PERIOD                   100
`define RIO_PL_COMP_SEQ_RATE                    5000
`define RIO_PL_STATUS_SYM_RATE                  1024
`define RIO_PL_TRANSMIT_FC_SUPPORT              `RIO_TRUE
`define RIO_PL_DISCOVERY_PERIOD                 500