/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_pl\scenario_pl.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Scenario for displaying how to use pl wrapper
*
* Notes:   This is not module and this file shall inclue to module_PL file
*
******************************************************************************/

reg start_scenario;
reg start_ts_scenario;
integer trx_Tag; //value of trx tag
integer address; //address for transactions
integer size;    //size of payload

`define IO_START_ADDRESS 'h1000
`define WAIT_ALL_TRANSACTION_FINISH 4100

/***************************************************************************
 * Description: Flags Reseting 
 *
 * Notes: need for disabling starting scenario at begin of work
 *
 **************************************************************************/
initial
begin
    start_scenario = 1'b0;
    start_ts_scenario = 1'b0;
    trx_Tag = 0;
end 

/***************************************************************************
 * Description: Implementation of small scenario for PL model
 *
 * Notes: Necessary set flag for begining
 *
 **************************************************************************/
always @(posedge start_scenario)   
begin
    //Prepare data for IO transactions
    for (i = 0; i < 32; i = i + 1 )
        $HW_Put_Data_Pl (inst_Id, `RIO_PL_IO_REQUEST_RQ_DATA, i, 32'h1E1E1E1E /*ms_Word*/, 32'h1F1F1F1F /*ls_Word*/);

    address = `IO_START_ADDRESS;
    //address for IO transactions is defined from the other place
    for (i = 1; i < 8; i = i + 1)
    begin
         while ( $HW_Rio_PL_IO_Request( inst_Id,
                `RIO_IO_NWRITE,   //rq_ttype,
                1,                  //rq_prio,
                address,            //rq_address,
                `RIO_PE_IO_EXT_ADDRESS, //rq_extended_address,
                `RIO_PE_IO_XAMSBS,  //rq_xamsbs,
                i,                  //rq_dw_size,
                0,                  //rq_offset,
                8,                  //rq_byte_num,
                trx_Tag,
                0,                  //transport_info - PE id
                `RIO_PL_TRANSP_T
               ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
    //Wait while while previously requested transaction with tag tag has been accepted for servicing.
    wait (rio_pl_ack_request_flag_reg === 1'b1 && rio_pl_ack_request_tag_reg == trx_Tag);
        rio_pl_ack_request_flag_reg = 1'b0;
        
    trx_Tag = trx_Tag + 1;

    end

    //Wait while other transanctions finish
    #(`WAIT_ALL_TRANSACTION_FINISH)

    //Scenario finished
        start_scenario = 1'b0;
end

/***************************************************************************
 * Description: Implementation of small scenario for PL model
 *
 * Notes: Used in the TS testbench and satrt address is equal ro zero
 *
 **************************************************************************/
always @(posedge start_ts_scenario)   
begin
    //Prepare data for IO transactions
    for (i = 0; i < 32; i = i + 1 )
        $HW_Put_Data_Pl (inst_Id, `RIO_PL_IO_REQUEST_RQ_DATA, i, 32'h1E1E1E1E /*ms_Word*/, 32'h1C1C1C1C /*ls_Word*/);

    for (i = 1; i < 8; i = i + 1)
    begin
         while ( $HW_Rio_PL_IO_Request( inst_Id,
                `RIO_IO_NWRITE_R,     //rq_ttype,
                1,                  //rq_prio,
                address,            //rq_address,
                0, //rq_extended_address,
                0,  //rq_xamsbs,
                i,                  //rq_dw_size,
                0,                  //rq_offset,
                8,                  //rq_byte_num,
                trx_Tag,
                0,                  //transport_info - PE id
                `RIO_PL_TRANSP_T
               ) == `RIO_RETRY)
            #(`RIO_WAIT_BEFORE_RETRY);
    //Wait while while previously requested transaction with tag tag has been accepted for servicing.
    wait (rio_pl_ack_request_flag_reg === 1'b1 && rio_pl_ack_request_tag_reg == trx_Tag);
        rio_pl_ack_request_flag_reg = 1'b0;
        
    trx_Tag = trx_Tag + 1;

    end

    //Wait while other transanctions finish
    #(`WAIT_ALL_TRANSACTION_FINISH)

    //Scenario finished
        start_ts_scenario = 1'b0;
end

