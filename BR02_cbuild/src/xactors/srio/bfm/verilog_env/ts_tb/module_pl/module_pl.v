/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_pl\module_pl.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Module for RIO Models
*
* Notes:
*
******************************************************************************/


`include "wrapper_pl.vh"
`include "defines_pl.vh"


module PL_Module (clock, tclk, tframe, td, tdl, rclk, rframe, rd, rdl,
/*serial interface*/
rlane0, rlane1, rlane2, rlane3, tlane0, tlane1, tlane2, tlane3,
/* new 10bit interface*/
clock_10, rlane0_10, rlane1_10, rlane2_10, rlane3_10, tlane0_10, tlane1_10, tlane2_10, tlane3_10);


    input clock; 
	input clock_10;

    input rclk;
    input rframe;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    input rlane0;
    input rlane1;
    input rlane2;
    input rlane3;

   /* 10bit interface */
    input [ 0 : 9 ] rlane0_10;
    input [ 0 : 9 ] rlane1_10;
    input [ 0 : 9 ] rlane2_10;
    input [ 0 : 9 ] rlane3_10;

    output tclk;
    output tframe;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;

    output tlane0;
    output tlane1;
    output tlane2;
    output tlane3;

   /* 10bit interface */
    output tlane0_10;
    output tlane1_10;
    output tlane2_10;
    output tlane3_10;

    reg tclk_reg;
    reg tframe_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl_reg;

    reg tlane0_reg;
    reg tlane1_reg;
    reg tlane2_reg;
    reg tlane3_reg;

    /* 10bit interface */
    reg [ 0 : 9 ] tlane0_10_reg;
    reg [ 0 : 9 ] tlane1_10_reg;
    reg [ 0 : 9 ] tlane2_10_reg;
    reg [ 0 : 9 ] tlane3_10_reg;

    assign tclk = tclk_reg;
    assign tframe = tframe_reg;
    assign td = td_reg;
    assign tdl = tdl_reg;

    assign tlane0 = tlane0_reg;
    assign tlane1 = tlane1_reg;
    assign tlane2 = tlane2_reg;
    assign tlane3 = tlane3_reg;

   /* 10bit interface */
    assign tlane0_10 = tlane0_10_reg;
    assign tlane1_10 = tlane1_10_reg;
    assign tlane2_10 = tlane2_10_reg;
    assign tlane3_10 = tlane3_10_reg;

//working mode
    parameter mode = `RIO_PL_INST_PAR_MODE;

/*temporary buffer for serial input pins flag*/
    reg [0 : `RIO_LANE_NUM - 1] lane_changed;

`include "pli_pl_registers.vh" 

    integer inst_Id;    //Instance ID of current module
    integer ms_word;    //Value of most significant word
    integer ls_word;    //Value of least significant word
    integer i;          //loop counter
    integer data;       //temporary storage for something numbers
    integer result;     //contains result of operations

/*Include scenarios*/
`include "scenario_pl.v"

/* Begin of example how to call routine Rio_PL_GSM_Request
result = $HW_Rio_PL_GSM_Request( inst_Id,
    ttype,
    rq_prio,
    rq_address,
    rq_extended_address,
    rq_xamsbs,
    rq_dw_size,
    rq_offset,
    rq_byte_num,
    sec_id,
    sec_tid,
    trx_tag,
    multicast_size);
End of example how to call routine Rio_PL_GSM_Request*/


/* Begin of example how to call routine Rio_PL_IO_Request
result = $HW_Rio_PL_IO_Request( inst_Id,
    rq_ttype,
    rq_prio,
    rq_address,
    rq_extended_address,
    rq_xamsbs,
    rq_dw_size,
    rq_offset,
    rq_byte_num,
    trx_tag,
    transport_info);
End of example how to call routine Rio_PL_IO_Request*/


/* Begin of example how to call routine Rio_PL_Message_Request
result = $HW_Rio_PL_Message_Request( inst_Id,
    rq_transport_info,
    rq_prio,
    rq_msglen,
    rq_ssize,
    rq_letter,
    rq_mbox,
    rq_msgseg,
    rq_dw_size,
    trx_tag);
End of example how to call routine Rio_PL_Message_Request*/


/* Begin of example how to call routine Rio_PL_Doorbell_Request
result = $HW_Rio_PL_Doorbell_Request( inst_Id,
    rq_prio,
    rq_transport_info,
    rq_doorbell_info,
    trx_tag);
End of example how to call routine Rio_PL_Doorbell_Request*/


/* Begin of example how to call routine Rio_PL_Configuration_Request
result = $HW_Rio_PL_Configuration_Request( inst_Id,
    rq_prio,
    rq_transport_info,
    rq_hop_count,
    rq_rw,
    rq_config_offset,
    rq_dw_size,
    rq_sub_dw_pos,
    trx_tag);
End of example how to call routine Rio_PL_Configuration_Request*/


/* Begin of example how to call routine Rio_PL_Packet_Struct_Request
$HW_Rio_PL_Packet_Struct_Request( inst_Id,
    rq_packet.ack_id,
    rq_packet.prio,
    rq_packet.rsrv_phy_1,
    rq_packet.rsrv_phy_2,
    rq_packet.tt,
    rq_packet.ftype,
    rq_packet.transport_info,
    rq_packet.ttype,
    rq_packet.rdwr_size,
    rq_packet.srtr_tid,
    rq_packet.hop_count,
    rq_packet.config_offset,
    rq_packet.wdptr,
    rq_packet.status,
    rq_packet.doorbell_info,
    rq_packet.msglen,
    rq_packet.letter,
    rq_packet.mbox,
    rq_packet.msgseg,
    rq_packet.ssize,
    rq_packet.address,
    rq_packet.extended_address,
    rq_packet.xamsbs,
    rq_packet.ext_address_valid,
    rq_packet.ext_address_16,
    rq_packet.rsrv_ftype6,
    rq_packet.rsrv_ftype8,
    rq_packet.rsrv_ftype10,
    rq_intermediate_crc,
    rq_is_int_crc_valid,
    rq_crc,
    rq_is_crc_valid,
    rq_dw_size,
    trx_tag);
End of example how to call routine Rio_PL_Packet_Struct_Request*/

/* Begin of example how to call routine Rio_PL_Send_Response
result = $HW_Rio_PL_Send_Response( inst_Id,
    tag,
    resp_prio,
    resp_ftype,
    resp_transaction,
    resp_status,
    resp_tr_info,
    resp_src_id,
    resp_target_tid,
    resp_sec_id,
    resp_sec_tid,
    resp_dw_num);
End of example how to call routine Rio_PL_Send_Response*/


/* Begin of example how to call routine Rio_PL_Ack_Remote_Req
result = $HW_Rio_PL_Ack_Remote_Req( inst_Id, tag);
End of example how to call routine Rio_PL_Ack_Remote_Req*/


/* Begin of example how to call routine Rio_PL_Set_Config_Reg
result = $HW_Rio_PL_Set_Config_Reg( inst_Id,
    config_offset,
    data);
End of example how to call routine Rio_PL_Set_Config_Reg*/


/* Begin of example how to call routine Rio_PL_Get_Config_Reg
result = $HW_Rio_PL_Get_Config_Reg( inst_Id, config_offset);
value = $HW_Get_Data_Pl(inst_Id, `RIO_PL_GET_CONFIG_REG_DATA);
End of example how to call routine Rio_PL_Get_Config_Reg*/


/* Begin of example how to call routine Rio_PL_Model_Start_Reset
$HW_Rio_PL_Model_Start_Reset( inst_Id);
End of example how to call routine Rio_PL_Model_Start_Reset*/


/* Begin of example how to access to internal task's buffer

    value = $HW_Get_Data_Pl(inst_Id, `RIO_PL_GET_CONFIG_REG_DATA);

    value_msw = $HW_Get_Data_Pl(inst_Id, Buffer_Name, index, MSW);
    value_lsw = $HW_Get_Data_Pl(inst_Id, Buffer_Name, index, LSW);

    Buffer_Name can be:
         `RIO_PL_REMOTE_REQUEST_REQ_DATA;
         `RIO_PL_REMOTE_RESPONSE_RESP_DATA;
End of example how to access to internal task's buffer*/


/* Begin of example how to access to internal routines buffer

    $HW_Put_Data_Pl(inst_Id, `RIO_PL_GSM_REQUEST_TRANSPORT_INFO, index, tr_info);


    $HW_Put_Data_Pl(inst_Id, type, index, ms_Word, ls_Word);
      where type can be :
        `RIO_PL_GSM_REQUEST_RQ_DATA
        `RIO_PL_IO_REQUEST_RQ_DATA
        `RIO_PL_MESSAGE_REQUEST_RQ_DATA
        `RIO_PL_CONFIGURATION_REQUEST_RQ_DATA
        `RIO_PL_PACKET_STRUCT_REQUEST_RQ_DATA
        `RIO_PL_SEND_RESPONSE_RESP_DATA
End of example how to access to internal callback buffer*/



/***************************************************************************
 * Description: Clock edge driver
 *
 * Notes:
 *
 **************************************************************************/
always @(clock)
begin
    if ( ~(clock === 1'bx) && inst_Id != `RIO_PL_ERROR_INST_ID)
        $HW_Rio_PL_Clock_Edge(inst_Id);
end 

/***************************************************************************
 * Description: External clock handler for 10bit interface
 *
 * Notes:
 *
 **************************************************************************/
always @(clock_10)
begin
    if (~(clock_10 === 1'bx) && inst_Id != `RIO_PE_ERROR_INST_ID)
        $HW_Rio_Clock_Edge_New( inst_Id );
end 

/***************************************************************************
 * Description: Input LP-EP clock handler
 *
 * Notes:
 *
 **************************************************************************/
always @(rclk)
if (mode == `RIO_PL_INST_PAR_MODE)
begin
    if (~(rclk === 1'bx) && inst_Id != `RIO_PL_ERROR_INST_ID)
        $HW_Rio_PL_Get_Pins( inst_Id, rframe, rd, rdl, rclk);
end


/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 0
 *
 **************************************************************************/
always @(rlane0)
if ((mode == `RIO_PL_INST_SERIAL_MODE) && (rlane0 !== 1'bx)) lane_changed[0] = 1'b1;

/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 1
 *
 **************************************************************************/
always @(rlane1)
if ((mode == `RIO_PL_INST_SERIAL_MODE) && (rlane1 !== 1'bx)) lane_changed[1] = 1'b1;

/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 2
 *
 **************************************************************************/
always @(rlane2)
if ((mode == `RIO_PL_INST_SERIAL_MODE) && (rlane2 !== 1'bx)) lane_changed[2] = 1'b1;

/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 3
 *
 **************************************************************************/
always @(rlane3)
if ((mode == `RIO_PL_INST_SERIAL_MODE) && (rlane3 !== 1'bx)) lane_changed[3] = 1'b1;


/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 0-3
 *
 **************************************************************************/
always 
begin
/*    wait (lane_changed === `RIO_PE_ALL_LANES_CHANGED);
    lane_changed = 1'b0;*/
    @(clock);
    #1;  /*wait for all pins setting*/
    if (mode == `RIO_PL_INST_SERIAL_MODE)
        $HW_Rio_PL_Serial_Get_Pins(inst_Id, rlane0, rlane1, rlane2, rlane3);
end

/***************************************************************************
 * Description: Input pcs-pma pin handler for 10-bit interface
 *
 * Notes: 0-3
 *
 **************************************************************************/
always 
begin
/*    wait (lane_changed === `RIO_PE_ALL_LANES_CHANGED);
    lane_changed = 1'b0;*/
    @(clock_10);
    #1;  /*wait for all pins setting*/
    if (mode == `RIO_PE_INST_SERIAL_MODE)
        $HW_Rio_Serial_Get_Pins_New(inst_Id, rlane0_10, rlane1_10, rlane2_10, rlane3_10);
end


/*realization of Rio_PL_Ack_Request task*/
/*This task is invoked by the RapidIO PL model instance to notify its environment
that previously requested transaction with tag tag has been accepted for
servicing.*/
//always
//begin
//wait (rio_pl_ack_request_flag_reg === 1'b1);
//    rio_pl_ack_request_flag_reg = 1'b0;

    /* something_variable =rio_pl_ack_request_tag_reg;*/

    /*Insert code for realization Rio_PL_Ack_Request task here*/
//    $display("PL%d:    The Rio_PL_Ack_Request task is invoked", inst_Id);
//end


/*realization of Rio_PL_Ack_Response task*/
//always
//begin
//wait (rio_pl_ack_response_flag_reg === 1'b1);
//    rio_pl_ack_response_flag_reg = 1'b0;
    
    /* something_variable = rio_pl_ack_response_tag_reg;*/

     /*Insert code for realization Rio_PL_Ack_Response task here*/
//     $display("PL%d:    The Rio_PL_Ack_Response task is invoked", inst_Id);
//end


/***************************************************************************
 * Description: realization of Rio_PL_Req_Time_Out task
 *
 * Notes:
 *
 **************************************************************************/
always
begin
wait (rio_pl_req_time_out_flag_reg === 1'b1);
    rio_pl_req_time_out_flag_reg = 1'b0;

    /* something_variable = rio_pl_req_time_out_tag_reg;*/
     /*Insert code for realization Rio_PL_Req_Time_Out task here*/
     $display("PL%d:    Timeout occured; tag=%d", inst_Id, rio_pl_req_time_out_tag_reg);
end


/***************************************************************************
 * Description: realization of Rio_PL_Remote_Request task
 *
 * Notes:
 *
 **************************************************************************/
always
begin
wait (rio_pl_remote_request_flag_reg === 1'b1);
    rio_pl_remote_request_flag_reg = 1'b0;

    /* something_variable = rio_pl_remote_request_tag_reg*/
    /* something_variable = rio_pl_remote_request_packet_type_reg;*/ 
    /* something_variable = rio_pl_remote_request_req_target_tid*/
    /* something_variable = rio_pl_remote_request_req_src_id*/
    /* something_variable = rio_pl_remote_request_req__prio*/
    /* something_variable = rio_pl_remote_request_req_wdptr*/
    /* something_variable = rio_pl_remote_request_req_rdsize*/
    /* something_variable = rio_pl_remote_request_req_ftype*/
    /* something_variable = rio_pl_remote_request_req_ttype*/
    /* something_variable = rio_pl_remote_request_req_address*/
    /* something_variable = rio_pl_remote_request_req_extended_address*/
    /* something_variable = rio_pl_remote_request_req_xamsbs*/
    /* something_variable = rio_pl_remote_request_req_sec_id*/
    /* something_variable = rio_pl_remote_request_req_sec_tid*/
    /* something_variable = rio_pl_remote_request_req_mbox*/
    /* something_variable = rio_pl_remote_request_req_letter*/
    /* something_variable = rio_pl_remote_request_req_msgseg*/
    /* something_variable = rio_pl_remote_request_req_ssize*/
    /* something_variable = rio_pl_remote_request_req_msglen*/
    /* something_variable = rio_pl_remote_request_req_doorbell_info*/
    /* something_variable = rio_pl_remote_request_req_offset*/

    /* something_variable = rio_pl_remote_request_dw_num_reg;*/ 
    /* something_variable = rio_pl_remote_request_transp_type_reg;*/ 
    /* something_variable = rio_pl_remote_request_transport_info_reg;*/ 

     /*Insert code for realization Rio_PL_Remote_Request task here*/
     $display("PL%d:    The Rio_PL_Remote_Request task is invoked", inst_Id);
    #(`RIO_WAIT_BEFORE_RETRY);    
    result = $HW_Rio_PL_Ack_Remote_Req(inst_Id, rio_pl_remote_request_tag_reg);

    if (result != `RIO_OK)
         $display("PL%d:    Error, Acknowledge on request failed", inst_Id);

    /*  here shall present responce
    result = $HW_Rio_PL_Send_Response( inst_Id,
        tag,
        resp_prio,
        resp_ftype,
        resp_transaction,
        resp_status,
        resp_tr_info,
        resp_src_id,
        resp_target_tid,
        resp_sec_id,
        resp_sec_tid,
        resp_dw_num); */
end


/***************************************************************************
 * Description: realization of Rio_PL_Remote_Response task
 *
 * Notes:
 *
 **************************************************************************/
always
begin
wait (rio_pl_remote_response_flag_reg === 1'b1);
    rio_pl_remote_response_flag_reg = 1'b0;

    /* something_variable = rio_pl_remote_response_tag_reg;*/ 
    /* something_variable = rio_pl_remote_response_prio_reg;*/ 
    /* something_variable = rio_pl_remote_response_ftype_reg;*/ 
    /* something_variable = rio_pl_remote_response_transaction_reg;*/ 
    /* something_variable = rio_pl_remote_response_status_reg;*/ 
    /* something_variable = rio_pl_remote_response_tr_info_reg;*/ 
    /* something_variable = rio_pl_remote_response_src_id_reg;*/ 
    /* something_variable = rio_pl_remote_response_target_tid_reg;*/ 
    /* something_variable = rio_pl_remote_response_sec_id_reg;*/ 
    /* something_variable = rio_pl_remote_response_sec_tid_reg;*/ 
    /* something_variable = rio_pl_remote_response_dw_num_reg;*/ 

     /*Insert code for realization Rio_PL_Remote_Response task here*/
     $display("PL%d:    The Rio_PL_Remote_Response task is invoked", inst_Id);
end


/***************************************************************************
 * Description: parallel link output interface
 *
 * Notes:
 *
 **************************************************************************/
always
begin
wait (rio_pl_set_pins_flag_reg === 1'b1);
    rio_pl_set_pins_flag_reg = 1'b0;

    tframe_reg =  rio_pl_set_pins_tframe_reg;
    td_reg = rio_pl_set_pins_td_reg;
    tdl_reg = rio_pl_set_pins_tdl_reg;
    tclk_reg = rio_pl_set_pins_tclk_reg;
end

/***************************************************************************
 * Description: serial link output interface
 *
 * Notes: 
 *
 **************************************************************************/
always
begin
    wait (rio_pl_set_serial_pins_flag_reg === 1'b1);
        rio_pl_set_serial_pins_flag_reg = 1'b0;

        tlane0_reg = rio_pl_set_pins_tlane0_reg;
        tlane1_reg = rio_pl_set_pins_tlane1_reg;
        tlane2_reg = rio_pl_set_pins_tlane2_reg;
        tlane3_reg = rio_pl_set_pins_tlane3_reg;

end


/***************************************************************************
 * Description: serial link output interface (10 bits)
 *
 * Notes: 
 *
 **************************************************************************/
always
begin
    wait (rio_pl_set_serial_pins_10bit_flag_reg === 1'b1);
        rio_pl_set_serial_pins_10bit_flag_reg = 1'b0;

        tlane0_10_reg = rio_pl_set_pins_tlane0_10_reg;
        tlane1_10_reg = rio_pl_set_pins_tlane1_10_reg;
        tlane2_10_reg = rio_pl_set_pins_tlane2_10_reg;
        tlane3_10_reg = rio_pl_set_pins_tlane3_10_reg;

end


/***************************************************************************
 * Description: realization of Extend_Reg_Write task
 *
 * Notes:
 *
 **************************************************************************/
always
begin
wait (extend_reg_write_flag_reg === 1'b1);
    extend_reg_write_flag_reg = 1'b0;
                 
    /* something_variable = extend_reg_write_config_offset_reg;*/ 
    /* something_variable = extend_reg_write_data_reg;*/ 

     /*Insert code for realization Extend_Reg_Write task here*/
     $display("PL%d:    The Extend_Reg_Write task is invoked; offset = %d", inst_Id, extend_reg_write_config_offset_reg);
end


/***************************************************************************
 * Description: Creates instance of modles
 *
 * Notes: 
 *
 **************************************************************************/
initial
if (mode == `RIO_PL_INST_PAR_MODE)
begin
    #1; /*wait for intialize registers*/

    inst_Id = $HW_RIO_PL_Model_Create_Instance (
    `RIO_PL_ADDRESS_SUPPORT_CODE,   //param_ext_address_support,
    `RIO_FALSE,                     // param_is_bridge,
    `RIO_TRUE,                      //param_has_memory,
    `RIO_TRUE,                      //param_has_processor,
    `RIO_TRUE,                      //param_coh_granule_size_32,
    2,                              //param_coh_domain_size,
    1,                              //param_device_identity,
    1,                              //param_device_vendor_identity,
    1,                              //param_device_rev,
    1,                              //param_device_minor_rev,
    1,                              //param_device_major_rev,
    1,                              //param_assy_identity,
    1,                              //param_assy_vendor_identity,
    1,                              //param_assy_rev,
    `RIO_PL_ENTRY_EXTENDED_FEATURES_PTR,    //param_entry_extended_features_ptr,
    `RIO_PL_NEXT_EXTENDED_FEATURES_PTR,     //param_next_extended_features_ptr,
    `RIO_PL_SOURCE_TRX,             //param_source_trx,
    `RIO_PL_DEST_TRX,              //param_dest_trx,
    `RIO_TRUE,                     //param_mbox1,
    `RIO_TRUE,                     //param_mbox2,
    `RIO_TRUE,                     //param_mbox3,
    `RIO_TRUE,                     //param_mbox4,
    `RIO_TRUE,                     //param_has_doorbell,
    `RIO_PL_INBOUND_BUF_SIZE,      //param_pl_inbound_buffer_size,
    `RIO_PL_OUTBOUND_BUF_SIZE,     //param_pl_outbound_buffer_size,
    `RIO_PL_INBOUND_BUF_SIZE,      //param_ll_inbound_buffer_size,
    `RIO_PL_OUTBOUND_BUF_SIZE,     //param_ll_outbound_buffer_size,
    /*registers handlers passing*/
    rio_pl_ack_request_tag_reg,
    rio_pl_ack_request_flag_reg,
    rio_pl_ack_response_tag_reg,
    rio_pl_ack_response_flag_reg,
    rio_pl_req_time_out_tag_reg,
    rio_pl_req_time_out_flag_reg,
    rio_pl_remote_response_tag_reg,
    rio_pl_remote_response_prio_reg,
    rio_pl_remote_response_ftype_reg,
    rio_pl_remote_response_transaction_reg,
    rio_pl_remote_response_status_reg,
    rio_pl_remote_response_tr_info_reg,
    rio_pl_remote_response_src_id_reg,
    rio_pl_remote_response_target_tid_reg,
    rio_pl_remote_response_sec_id_reg,
    rio_pl_remote_response_sec_tid_reg,
    rio_pl_remote_response_dw_num_reg,
    rio_pl_remote_response_flag_reg,
    rio_pl_set_pins_tframe_reg,
    rio_pl_set_pins_td_reg,
    rio_pl_set_pins_tdl_reg,
    rio_pl_set_pins_tclk_reg,
    rio_pl_set_pins_flag_reg,
    extend_reg_write_config_offset_reg,
    extend_reg_write_data_reg,
    extend_reg_write_flag_reg,

    rio_pl_remote_request_tag_reg,
    rio_pl_remote_request_packet_type_reg,
    rio_pl_remote_request_dw_num_reg,
    rio_pl_remote_request_transp_type_reg,
    rio_pl_remote_request_transport_info_reg,
    rio_pl_remote_request_flag_reg,

    rio_pl_remote_request_req_target_tid,
    rio_pl_remote_request_req_src_id,
    rio_pl_remote_request_req_prio,
    rio_pl_remote_request_req_wdptr,
    rio_pl_remote_request_req_rdsize,
    rio_pl_remote_request_req_ftype,
    rio_pl_remote_request_req_ttype,
    rio_pl_remote_request_req_address,
    rio_pl_remote_request_req_extended_address,
    rio_pl_remote_request_req_xamsbs,
    rio_pl_remote_request_req_sec_id,
    rio_pl_remote_request_req_sec_tid,
    rio_pl_remote_request_req_mbox,
    rio_pl_remote_request_req_letter,
    rio_pl_remote_request_req_msgseg,
    rio_pl_remote_request_req_ssize,
    rio_pl_remote_request_req_msglen,
    rio_pl_remote_request_req_doorbell_info,
    rio_pl_remote_request_req_offset);

    if (inst_Id != `RIO_PL_ERROR_INST_ID)
       $display ("PL Module:PL instance is created; id = %d", `PL_ID);
    else 
    begin
        $display ("PL Module: Error during Instance creating");
        $finish;
    end

    result = $HW_Rio_PL_Model_Initialize( inst_Id,
    `RIO_PL_TRANSP_T,                     //param_set_transp_type,
    `PL_ID,                              //param_set_dev_id,
    0,                                  //param_set_lcshbar,
    0,                                  //param_set_lcsbar,
    `RIO_FALSE,                         //param_set_is_host,
    `RIO_TRUE,                          //param_set_is_master_enable,
    `RIO_FALSE,                         //param_set_is_discovered,
    `RIO_PL_EXT_ADDRESS_SUPPORT,        //param_set_ext_address,
    `RIO_PL_EXT_ADDRESS_SIZE_SELECTOR,  //param_set_ext_address_16,
    `RIO_PL_PARAM_SET_LP_EP_IS_8,       //param_set_lp_ep_is_8
    `RIO_PL_MODE_IS_8,                  //param_set_work_mode_is_8,
    `RIO_TRUE,                          //param_set_input_is_enable,
    `RIO_TRUE,                          //param_set_output_is_enable,
    `NUM_TRAININGS,                     //param_set_num_trainings,
    `RIO_REQ_WND_ALIGN,                 //param_set_requires_window_alignment,
    `RIO_PL_IDLE_LIM,                   //param_set_receive_idle_lim,
    `RIO_PL_IDLE_LIM,                   //param_set_throttle_idle_lim,
    `RIO_PL_RES_FOR_OUT,                //param_set_res_for_3_out,
    `RIO_PL_RES_FOR_OUT,                // param_set_res_for_2_out,
    `RIO_PL_RES_FOR_OUT,                // param_set_res_for_1_out,
    `RIO_PL_RES_FOR_IN,                 //param_set_res_for_3_in,
    `RIO_PL_RES_FOR_IN,                 //param_set_res_for_2_in,
    `RIO_PL_RES_FOR_IN,                 //param_set_res_for_1_in,
    `RIO_TRUE,                          //param_set_pass_by_prio
    `RIO_FALSE,                          //enable retry return code
    `RIO_FALSE                          //Enable link request resending
    );

    if (result != `RIO_OK)
    begin
        $display ("PE Module: Initialization failed");
        $finish;
    end



end


/***************************************************************************
 * Description: Creates instance of serial model
 *
 * Notes: 
 *
 **************************************************************************/
initial
if (mode == `RIO_PL_INST_SERIAL_MODE)
begin
    #1; /*wait for intialize registers*/

    inst_Id = $HW_RIO_PL_Serial_Model_Create_Instance (
    `RIO_PL_ADDRESS_SUPPORT_CODE,   //param_ext_address_support,
    `RIO_FALSE,                     // param_is_bridge,
    `RIO_TRUE,                      //param_has_memory,
    `RIO_TRUE,                      //param_has_processor,
    `RIO_TRUE,                      //param_coh_granule_size_32,
    2,                              //param_coh_domain_size,
    1,                              //param_device_identity,
    1,                              //param_device_vendor_identity,
    1,                              //param_device_rev,
    1,                              //param_device_minor_rev,
    1,                              //param_device_major_rev,
    1,                              //param_assy_identity,
    1,                              //param_assy_vendor_identity,
    1,                              //param_assy_rev,
    `RIO_PL_ENTRY_EXTENDED_FEATURES_PTR,    //param_entry_extended_features_ptr,
    `RIO_PL_NEXT_EXTENDED_FEATURES_PTR,     //param_next_extended_features_ptr,
    `RIO_PL_SOURCE_TRX,             //param_source_trx,
    `RIO_PL_DEST_TRX,              //param_dest_trx,
    `RIO_TRUE,                     //param_mbox1,
    `RIO_TRUE,                     //param_mbox2,
    `RIO_TRUE,                     //param_mbox3,
    `RIO_TRUE,                     //param_mbox4,
    `RIO_TRUE,                     //param_has_doorbell,
    `RIO_PL_INBOUND_BUF_SIZE,      //param_pl_inbound_buffer_size,
    `RIO_PL_OUTBOUND_BUF_SIZE,     //param_pl_outbound_buffer_size,
    `RIO_PL_INBOUND_BUF_SIZE,      //param_ll_inbound_buffer_size,
    `RIO_PL_OUTBOUND_BUF_SIZE,     //param_ll_outbound_buffer_size,
    /*registers handlers passing*/
    rio_pl_ack_request_tag_reg,
    rio_pl_ack_request_flag_reg,
    rio_pl_ack_response_tag_reg,
    rio_pl_ack_response_flag_reg,
    rio_pl_req_time_out_tag_reg,
    rio_pl_req_time_out_flag_reg,
    rio_pl_remote_response_tag_reg,
    rio_pl_remote_response_prio_reg,
    rio_pl_remote_response_ftype_reg,
    rio_pl_remote_response_transaction_reg,
    rio_pl_remote_response_status_reg,
    rio_pl_remote_response_tr_info_reg,
    rio_pl_remote_response_src_id_reg,
    rio_pl_remote_response_target_tid_reg,
    rio_pl_remote_response_sec_id_reg,
    rio_pl_remote_response_sec_tid_reg,
    rio_pl_remote_response_dw_num_reg,
    rio_pl_remote_response_flag_reg,
    rio_pl_set_pins_tlane0_reg,
    rio_pl_set_pins_tlane1_reg,
    rio_pl_set_pins_tlane2_reg,
    rio_pl_set_pins_tlane3_reg,
    rio_pl_set_serial_pins_flag_reg,
    extend_reg_write_config_offset_reg,
    extend_reg_write_data_reg,
    extend_reg_write_flag_reg,

    rio_pl_remote_request_tag_reg,
    rio_pl_remote_request_packet_type_reg,
    rio_pl_remote_request_dw_num_reg,
    rio_pl_remote_request_transp_type_reg,
    rio_pl_remote_request_transport_info_reg,
    rio_pl_remote_request_flag_reg,

    rio_pl_remote_request_req_target_tid,
    rio_pl_remote_request_req_src_id,
    rio_pl_remote_request_req_prio,
    rio_pl_remote_request_req_wdptr,
    rio_pl_remote_request_req_rdsize,
    rio_pl_remote_request_req_ftype,
    rio_pl_remote_request_req_ttype,
    rio_pl_remote_request_req_address,
    rio_pl_remote_request_req_extended_address,
    rio_pl_remote_request_req_xamsbs,
    rio_pl_remote_request_req_sec_id,
    rio_pl_remote_request_req_sec_tid,
    rio_pl_remote_request_req_mbox,
    rio_pl_remote_request_req_letter,
    rio_pl_remote_request_req_msgseg,
    rio_pl_remote_request_req_ssize,
    rio_pl_remote_request_req_msglen,
    rio_pl_remote_request_req_doorbell_info,
    rio_pl_remote_request_req_offset,
   /* registers for 10bit interface */
    rio_pl_set_pins_tlane0_10_reg, 
    rio_pl_set_pins_tlane1_10_reg, 
    rio_pl_set_pins_tlane2_10_reg, 
    rio_pl_set_pins_tlane3_10_reg, 
    rio_pl_set_serial_pins_10bit_flag_reg);


    if (inst_Id != `RIO_PL_ERROR_INST_ID)
       $display ("PL Module:PL instance is created; id = %d", `PL_ID);
    else 
    begin
        $display ("PL Module: Error during Instance creating");
        $finish;
    end

    result = $HW_Rio_PL_Serial_Model_Initialize( inst_Id,
    `RIO_PL_TRANSP_T,                     //param_set_transp_type,
    `PL_ID,                              //param_set_dev_id,
    0,                                  //param_set_lcshbar,
    0,                                  //param_set_lcsbar,
    `RIO_FALSE,                         //param_set_is_host,
    `RIO_TRUE,                          //param_set_is_master_enable,
    `RIO_FALSE,                         //param_set_is_discovered,
    `RIO_PL_EXT_ADDRESS_SUPPORT,        //param_set_ext_address,
    `RIO_PL_EXT_ADDRESS_SIZE_SELECTOR,  //param_set_ext_address_16,
    `RIO_PL_PARAM_SET_SERIAL_IS_1X,     //param_set_is_1x
    `RIO_PL_IS_FORSE_1X_MODE,           //param_set_is_Force_1x_Mode
    `RIO_PL_IS_FORSE_1X_MODE_LANE0,     //param_set_is_Force_1x_Mode_Lane0
    `RIO_TRUE,                          //param_set_input_is_enable,
    `RIO_TRUE,                          //param_set_output_is_enable,
    `RIO_PL_SILENCE_PERIOD,             //param_set_silence_Period,
    `RIO_PL_COMP_SEQ_RATE,              //param_set_comp_Seq_Rate,
    `RIO_PL_STATUS_SYM_RATE,            //param_set_status_Sym_Rate
    `RIO_PL_DISCOVERY_PERIOD,           //discovery period
    `RIO_PL_RES_FOR_OUT,                //param_set_res_for_3_out,
    `RIO_PL_RES_FOR_OUT,                // param_set_res_for_2_out,
    `RIO_PL_RES_FOR_OUT,                // param_set_res_for_1_out,
    `RIO_PL_RES_FOR_IN,                 //param_set_res_for_3_in,
    `RIO_PL_RES_FOR_IN,                 //param_set_res_for_2_in,
    `RIO_PL_RES_FOR_IN,                 //param_set_res_for_1_in,
    `RIO_TRUE,                           //param_set_pass_by_prio
    `RIO_PL_TRANSMIT_FC_SUPPORT,         //transmit_Flow_Control_Support
    `RIO_FALSE,                          //enable retry return code
    `RIO_FALSE                          //Enable link request resending
    );

    if (result != `RIO_OK)
    begin
        $display ("PE Module: Initialization failed");
        $finish;
    end



end


/***************************************************************************
 * Description: Reset flag registers
 *
 * Notes: 
 *
 **************************************************************************/
initial
begin
    rio_pl_ack_request_flag_reg = 1'b0;
    rio_pl_get_req_data_flag_reg = 1'b0;
    rio_pl_ack_response_flag_reg = 1'b0;
    rio_pl_get_resp_data_flag_reg = 1'b0;
    rio_pl_req_time_out_flag_reg = 1'b0;
    rio_pl_remote_request_flag_reg = 1'b0;
    rio_pl_remote_response_flag_reg = 1'b0;
    rio_pl_set_pins_flag_reg = 1'b0;
    extend_reg_write_flag_reg = 1'b0;
    inst_Id = `RIO_PL_ERROR_INST_ID;
    lane_changed = 1'b0;
end


endmodule
