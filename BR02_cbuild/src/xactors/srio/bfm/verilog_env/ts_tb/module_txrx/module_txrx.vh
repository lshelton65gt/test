/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* $Element: /project/riosuite/src/verilog_env/ts_tb/module_txrx/module_txrx.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: /project/riosuite $ 
* $OS: solaris $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  defines for TxRx module
*
* Notes:
*
******************************************************************************/
//initialization parameters

`define TXRX_DEFAULT_GRANULE        32'h80647F9B
`define TXRX_DEFAULT_FRAME_TOGGLE   `RIO_TRUE
`define TXRX_LP_EP_IS_8             `RIO_TRUE
`define TXRX_WORK_MODE_IS_8         `RIO_TRUE
`define TXRX_EXT_ADDRESS_VALID      `RIO_FALSE
`define TXRX_EXT_ADDRESS_16         `RIO_FALSE
`define TXRX_NUM_TRAININGS          256
`define TXRX_RQ_WND_ALLIGN          `RIO_TRUE



`define TXRX_PACKETS_BUFFER_SIZE    10
`define TXRX_SYMBOLS_BUFFER_SIZE    10
`define TXRX_GRANULES_BUFFER_SIZE   10


//defines for scenario processing
`define NOT_APPLICABLE              0




//serial constants
//instantination parameters
`define RIO_STXRX_PACKET_BUF_SIZE       8
`define RIO_STXRX_SYMBOL_BUF_SIZE       16
`define RIO_STXRX_CHAR_BUF_SIZE         8
`define RIO_STXRX_CHAR_COLUMN_BUF_SIZE  8
`define RIO_STXRX_CG_BUF_SIZE           8
`define RIO_STXRX_CG_COLUMN_BUF_SIZE    8
`define RIO_STXRX_SINGLE_BIT_BUF_SIZE   8
`define RIO_STXRX_SINGLE_CHAR_BUF_SIZE  8
`define RIO_STXRX_COMP_SEQ_RQ_BUF_SIZE  8
`define RIO_STXRX_MANAG_RQ_BUF_SIZE     8
`define RIO_STXRX_POP_BUF_SIZE          20

`define RIO_STXRX_ERR_VAL               -1

//initialization parameters
`define RIO_TXRX_IS_1X                  `RIO_FALSE
`define RIO_TXRX_FORSE_1X_MODE          `RIO_FALSE
`define RIO_TXRX_IS_FORSE_1X_LANE0      `RIO_FALSE
`define RIO_TXRX_IS_EXT_ADDR_VALID      `RIO_TRUE
`define RIO_TXRX_EXT_ADDR_16            `RIO_FALSE
`define RIO_TXRX_IS_INIT_PROC_ON        `RIO_TRUE
`define RIO_TXRX_SILENCE_PERIOD         100
`define RIO_TXRX_IS_COMP_SEQ_GEN_ON     `RIO_TRUE
`define RIO_TXRX_COMP_SEQ_RATE          5000
`define RIO_TXRX_STATUS_SYM_GEN_ON      `RIO_TRUE
`define RIO_TXRX_STATUS_SYM_RATE        1024
`define RIO_TXRX_DISCOVERY_PERIOD       500
