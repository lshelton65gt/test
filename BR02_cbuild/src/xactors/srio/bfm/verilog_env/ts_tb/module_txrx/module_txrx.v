/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_txrx\module_txrx.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  example of module for RIO Txrx Model
*
* Notes:
*
******************************************************************************/


`include "wrapper_txrx.vh"
`include "module_txrx.vh"


module TXRX_Module (clock, tclk, tframe, td, tdl, rclk, rframe, rd, rdl,
rlane0, rlane1, rlane2, rlane3, tlane0, tlane1, tlane2, tlane3, 
/* new 10bit interface*/
clock_10, rlane0_10, rlane1_10, rlane2_10, rlane3_10, tlane0_10, tlane1_10, tlane2_10, tlane3_10);


    input clock; 
	input clock_10;

    input rclk;
    input rframe;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    input rlane0;
    input rlane1;
    input rlane2;
    input rlane3;

   /* 10bit interface */
    input [ 0 : 9 ] rlane0_10;
    input [ 0 : 9 ] rlane1_10;
    input [ 0 : 9 ] rlane2_10;
    input [ 0 : 9 ] rlane3_10;

    parameter mode = `RIO_TXRX_INST_PAR_MODE;

    output tclk;
    output tframe;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;

    output tlane0;
    output tlane1;
    output tlane2;
    output tlane3;

   /* 10bit interface */
    output [ 0 : 9 ] tlane0_10;
    output [ 0 : 9 ] tlane1_10;
    output [ 0 : 9 ] tlane2_10;
    output [ 0 : 9 ] tlane3_10;

    reg tclk_reg;
    reg tframe_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl_reg;

    assign tclk = tclk_reg;
    assign tframe = tframe_reg;
    assign td = td_reg;
    assign tdl = tdl_reg;

    reg tlane0_reg;
    reg tlane1_reg;
    reg tlane2_reg;
    reg tlane3_reg;

      /* 10bit interface */
    reg [ 0 : 9 ] tlane0_10_reg;
    reg [ 0 : 9 ] tlane1_10_reg;
    reg [ 0 : 9 ] tlane2_10_reg;
    reg [ 0 : 9 ] tlane3_10_reg;

    assign tlane0 = tlane0_reg;
    assign tlane1 = tlane1_reg;
    assign tlane2 = tlane2_reg;
    assign tlane3 = tlane3_reg;

   /* 10bit interface */
    assign tlane0_10 = tlane0_10_reg;
    assign tlane1_10 = tlane1_10_reg;
    assign tlane2_10 = tlane2_10_reg;
    assign tlane3_10 = tlane3_10_reg;

`include "../../pliwrap/txrxm/pli_registers.vh" 

    integer inst_Id;
    integer result;
    integer i; /*common counter variable for various things*/

/*temporary buffer for serial input pins flag*/
    reg [0 : `RIO_PL_SERIAL_NUMBER_OF_LANES - 1] lane_changed;

`include "scenario_txrx.v"



//Pin drivers
/* External clock handler */
always @(clock)
begin
    if (~(clock === 1'bx) && inst_Id != `RIO_TXRX_ERROR_INST_ID)
        $HW_Rio_TxRxM_Clock_Edge(inst_Id);
end 

/***************************************************************************
 * Description: External clock handler for 10bit interface
 *
 * Notes:
 *
 **************************************************************************/
always @(clock_10)
begin
    if (~(clock_10 === 1'bx) && inst_Id != `RIO_TXRX_ERROR_INST_ID)
        $HW_Rio_TxRxM_Clock_Edge_New(inst_Id);
end 

/* Input LP-EP clock handler */
always @(rclk)
begin
    if (~(rclk === 1'bx) && inst_Id != `RIO_TXRX_ERROR_INST_ID)
        $HW_Rio_TxRxM_Get_Pins(inst_Id, rframe, rd, rdl, rclk);
end 

/*realization of Rio_TxRxM_Set_Pins task*/
always
begin
wait (rio_txrxm_set_pins_flag_reg === 1'b1);
    rio_txrxm_set_pins_flag_reg = 1'b0;

    tframe_reg = rio_txrxm_set_pins_tframe_reg;
    td_reg = rio_txrxm_set_pins_td_reg;
    tdl_reg = rio_txrxm_set_pins_tdl_reg;
    tclk_reg = rio_txrxm_set_pins_tclk_reg;
end


/*implementation of Rio_TxRxM_Set_Pins task*/
always
begin
wait (rio_txrxm_serial_set_pins_flag_reg === 1'b1);
    rio_txrxm_serial_set_pins_flag_reg = 1'b0;

    tlane0_reg = rio_txrxm_set_pins_tlane0_reg;
    tlane1_reg = rio_txrxm_set_pins_tlane1_reg; 
    tlane2_reg = rio_txrxm_set_pins_tlane2_reg; 
    tlane3_reg = rio_txrxm_set_pins_tlane3_reg; 
end


/*implementation of Rio_TxRxM_Serial_Set_Pins_New task*/
always
begin
wait (rio_txrxm_serial_set_pins_10bit_flag_reg === 1'b1);
    rio_txrxm_serial_set_pins_10bit_flag_reg = 1'b0;

    tlane0_10_reg = rio_txrxm_set_pins_tlane0_10_reg;
    tlane1_10_reg = rio_txrxm_set_pins_tlane1_10_reg; 
    tlane2_10_reg = rio_txrxm_set_pins_tlane2_10_reg; 
    tlane3_10_reg = rio_txrxm_set_pins_tlane3_10_reg; 
end


/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 0
 *
 **************************************************************************/
//always @(rlane0)
//if ((mode == `RIO_TXRX_INST_SERIAL_MODE) && (rlane0 !== 1'bx)) lane_changed[0] = 1'b1;

/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 1
 *
 **************************************************************************/
//always @(rlane1)
//if ((mode == `RIO_TXRX_INST_SERIAL_MODE) && (rlane1 !== 1'bx)) lane_changed[1] = 1'b1;

/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 2
 *
 **************************************************************************/
//always @(rlane2)
//if ((mode == `RIO_TXRX_INST_SERIAL_MODE) && (rlane2 !== 1'bx)) lane_changed[2] = 1'b1;

/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 3
 *
 **************************************************************************/
//always @(rlane3)
//if ((mode == `RIO_TXRX_INST_SERIAL_MODE) && (rlane3 !== 1'bx)) lane_changed[3] = 1'b1;


/***************************************************************************
 * Description: Input pcs-pma pin hander
 *
 * Notes: 0-3
 *
 **************************************************************************/
always 
begin
/*    wait (lane_changed === `RIO_TXRX_ALL_LANES_CHANGED);
    lane_changed = 1'b0;*/
    @(clock);
    #1;  /*wait for all pins setting*/
    if (mode == `RIO_TXRX_INST_SERIAL_MODE)
        $HW_Serial_Rio_TxRxM_Get_Pins( inst_Id,
            rlane0,
            rlane1,
            rlane2,
            rlane3);
end

/***************************************************************************
 * Description: Input pcs-pma pin hander  for 10-bit interface
 *
 * Notes: 0-3
 *
 **************************************************************************/
always 
begin
    @(clock_10);
    #1;  /*wait for all pins setting*/
    if (mode == `RIO_TXRX_INST_SERIAL_MODE)
        $HW_Serial_Rio_TxRxM_Get_Pins_New( inst_Id,
            rlane0_10,
            rlane1_10,
            rlane2_10,
            rlane3_10);
end

/* Begin of example how to call routine Rio_TxRxM_Packet_Struct_Request
$HW_Rio_TxRxM_Packet_Struct_Request( inst_Id,
    rq_ack_id,
    rq_prio,
    rq_rsrv_phy_1,
    rq_rsrv_phy_2,
    rq_tt,
    rq_ftype,
    rq_transport_info,
    rq_ttype,
    rq_rdwr_size,
    rq_srtr_tid,
    rq_hop_count,
    rq_config_offset,
    rq_wdptr,
    rq_status,
    rq_doorbell_info,
    rq_msglen,
    rq_letter,
    rq_mbox,
    rq_msgseg,
    rq_ssize,
    rq_address,
    rq_extended_address,
    rq_xamsbs,
    rq_ext_address_valid,
    rq_ext_address_16,
    rq_rsrv_ftype6,
    rq_rsrv_ftype8,
    rq_rsrv_ftype10,
    dw_size);
End of example how to call routine Rio_TxRxM_Packet_Struct_Request*/


/* Begin of example how to call routine Rio_TxRxM_Packet_Stream_Request
$HW_Rio_TxRxM_Packet_Stream_Request( inst_Id,
    packet_size);
End of example how to call routine Rio_TxRxM_Packet_Stream_Request*/


/* Begin of example how to call routine Rio_TxRxM_Symbol_Request
$HW_Rio_TxRxM_Symbol_Request( inst_Id,
    sym_s,
    sym_ackid,
    sym_rsrv_1,
    sym_s_parity,
    sym_rsrv_2,
    sym_buf_status,
    sym_stype,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Symbol_Request*/


/* Begin of example how to call routine Rio_TxRxM_Training_Request
$HW_Rio_TxRxM_Training_Request( inst_Id,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Training_Request*/


/* Begin of example how to call routine Rio_TxRxM_Granule_Request
$HW_Rio_TxRxM_Granule_Request( inst_Id,
    granule,
    has_frame_toggled,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Granule_Request*/


/* Begin of example how to call routine Rio_TxRxM_Model_Start_Reset
$HW_Rio_TxRxM_Model_Start_Reset( inst_Id);
End of example how to call routine Rio_TxRxM_Model_Start_Reset*/


/* Begin of example how to call routine Rio_TxRxM_Model_Initialize
$HW_Rio_TxRxM_Model_Initialize(inst_Id,
    param_set_default_granule,
    param_set_default_frame_toggle,
    param_set_lp_ep_is_8,
    param_set_work_mode_is_8,
    param_set_in_ext_address_valid,
    param_set_in_ext_address_16);
End of example how to call routine Rio_TxRxM_Model_Initialize*/

/*Begin of example how to access callback internal buffers
    ms_Word = $HW_Get_Data_TxRx(inst_Id, type, index, `MSW);
    ls_Word = $HW_Get_Data_TxRx(inst_Id, type, index, `LSW);
    where type can take next values:
        `RIO_TXRXM_REQUEST_RECEIVED_PACKET_DATA
        `RIO_TXRXM_RESPONSE_RECEIVED_PACKET_DATA
    unsigned_char_data = $HW_Get_Data_TxRx(inst_Id, type, index,);
    where type can take next values:
        `RIO_TXRXM_VIOLENT_PACKET_RECEIVED_BUFFER
        `RIO_TXRXM_CANCELED_PACKET_RECEIVED_BUFFER
        `RIO_TXRXM_PACKET_TO_SEND_BUFFER
End of example how to access callback internal buffers*/

/*Begin of example how to access model API routines internal buffers
    $HW_Put_Data_TxRx(`RIO_TXRXM_PACKET_STRUCT_REQUEST_DATA, index, ms_Word, ls_Word);
    $HW_Put_Data_TxRx(`RIO_TXRXM_PACKET_STREAM_REQUEST_PACKET_BUFFER, index, unsigned_char_data);    
End of example how to access model API routines internal buffers*/


//serail routines

/* Begin of example how to call routine Rio_TxRxM_Serial_Symbol_Request
$HW_Rio_TxRxM_Serial_Symbol_Request( inst_Id,
    sym_stype0,
    sym_parameter0,
    sym_parameter1,
    sym_stype1,
    sym_cmd,
    sym_crc,
    sym_is_crc_valid,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Serial_Symbol_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Character_Request
$HW_Rio_TxRxM_Serial_Character_Request( inst_Id,
    character,
    char_type,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Serial_Character_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Character_Column_Request
$HW_Rio_TxRxM_Serial_Character_Column_Request( inst_Id,
    place,
    granule_num
    column[0], column[1], column[2], column[3],
    char_Type[0], char_Type[1], char_Type[2], char_Type[3]);
End of example how to call routine Rio_TxRxM_Serial_Character_Column_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Code_Group_Request
$HW_Rio_TxRxM_Serial_Code_Group_Request( inst_Id,
    code_group,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Serial_Code_Group_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Code_Group_Column_Request
$HW_Rio_TxRxM_Serial_Code_Group_Column_Request( inst_Id,
    place,
    granule_num, column[0], column[1], column[2], column[3]);
End of example how to call routine Rio_TxRxM_Serial_Code_Group_Column_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Single_Character_Request
$HW_Rio_TxRxM_Serial_Single_Character_Request( inst_Id,
    character,
    char_type,
    place,
    granule_num,
    place_in_granule,
    character_num);
End of example how to call routine Rio_TxRxM_Serial_Single_Character_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Single_Bit_Request
$HW_Rio_TxRxM_Serial_Single_Bit_Request( inst_Id,
    bit_value,
    place,
    granule_num,
    place_in_granule,
    character_num,
    bit_num);
End of example how to call routine Rio_TxRxM_Serial_Single_Bit_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Comp_Seq_Request
$HW_Rio_TxRxM_Serial_Comp_Seq_Request( inst_Id,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Serial_Comp_Seq_Request*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Pcs_Pma_Machine_Management
$HW_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management( inst_Id,
    set_on_machine,
    place,
    granule_num,
    machine_type);
End of example how to call routine Rio_TxRxM_Serial_Pcs_Pma_Machine_Management*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Pop_Bit
$HW_Rio_TxRxM_Serial_Pop_Bit( inst_Id,
    lane_num,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Serial_Pop_Bit*/


/* Begin of example how to call routine Rio_TxRxM_Serial_Pop_Character
$HW_Rio_TxRxM_Serial_Pop_Character( inst_Id,
    lane_num,
    place,
    granule_num);
End of example how to call routine Rio_TxRxM_Serial_Pop_Character*/


/* Begin of example how to call routine Rio_TxRxM_Model_Start_Reset
$HW_Rio_TxRxM_Model_Start_Reset( inst_Id);
End of example how to call routine Rio_TxRxM_Model_Start_Reset*/


/* Begin of example how to call routine Rio_TxRxM_Model_Initialize
$HW_Serial_Rio_TxRxM_Model_Initialize( inst_Id,
    param_set_lp_serial_is_1x,
    param_set_is_force_1x_mode,
    param_set_is_force_1x_mode_lane_0,
    param_set_is_ext_address_valid,
    param_set_is_ext_address_16,
    param_set_is_init_proc_on,
    param_set_silence_period,
    param_set_is_comp_seq_gen_on,
    param_set_comp_seq_rate,
    param_set_is_status_sym_gen_on,
    param_set_status_sym_rate);
End of example how to call routine Rio_TxRxM_Model_Initialize*/




/* Begin of example how to call routine Rio_TxRxM_Delete_Instance
$HW_Rio_TxRxM_Delete_Instance( inst_Id);
End of example how to call routine Rio_TxRxM_Delete_Instance*/



/*realization of Rio_TxRxM_Request_Received task*/
always
begin
wait (rio_txrxm_request_received_flag_reg === 1'b1);
    rio_txrxm_request_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_request_received_packet_type_reg;*/ 
    /* something_variable = rio_txrxm_request_received_dw_num_reg;*/ 
    /* something_variable = rio_txrxm_request_received_transp_type_reg;*/ 
    /* something_variable = rio_txrxm_request_received_ack_id_reg;*/ 
    /* something_variable = rio_txrxm_request_received_transport_info_reg;*/ 
    /* something_variable = rio_txrxm_request_received_hop_count_reg;*/ 

    /* something_variable = rio_txrxm_request_received_target_tid;*/ 
    /* something_variable = rio_txrxm_request_received_src_id;*/ 
    /* something_variable = rio_txrxm_request_received_prio;*/ 
    /* something_variable = rio_txrxm_request_received_wdptr;*/ 
    /* something_variable = rio_txrxm_request_received_rdsize;*/ 
    /* something_variable = rio_txrxm_request_received_format_type;*/ 
    /* something_variable = rio_txrxm_request_received_ttype;*/ 
    /* something_variable = rio_txrxm_request_received_address;*/ 
    /* something_variable = rio_txrxm_request_received_ext_address;*/ 
    /* something_variable = rio_txrxm_request_received_xamsbs;*/ 
    /* something_variable = rio_txrxm_request_received_sec_id;*/ 
    /* something_variable = rio_txrxm_request_received_sec_tid;*/ 
    /* something_variable = rio_txrxm_request_received_mbox;*/ 
    /* something_variable = rio_txrxm_request_received_letter;*/ 
    /* something_variable = rio_txrxm_request_received_msgseg;*/ 
    /* something_variable = rio_txrxm_request_received_ssize;*/ 
    /* something_variable = rio_txrxm_request_received_msglen;*/ 
    /* something_variable = rio_txrxm_request_received_doorbell_info;*/ 
    /* something_variable = rio_txrxm_request_received_offset;*/ 




     /*Insert code for realization Rio_TxRxM_Request_Received task here*/
     $display("The Rio_TxRxM_Request_Received task is invoked");
end


/*realization of Rio_TxRxM_Response_Received task*/
always
begin
wait (rio_txrxm_response_received_flag_reg === 1'b1);
    rio_txrxm_response_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_response_received_prio_reg;*/ 
    /* something_variable = rio_txrxm_response_received_ftype_reg;*/ 
    /* something_variable = rio_txrxm_response_received_transaction_reg;*/ 
    /* something_variable = rio_txrxm_response_received_status_reg;*/ 
    /* something_variable = rio_txrxm_response_received_tr_info_reg;*/ 
    /* something_variable = rio_txrxm_response_received_src_id_reg;*/ 
    /* something_variable = rio_txrxm_response_received_target_tid_reg;*/ 
    /* something_variable = rio_txrxm_response_received_sec_id_reg;*/ 
    /* something_variable = rio_txrxm_response_received_sec_tid_reg;*/ 
    /* something_variable = rio_txrxm_response_received_dw_num_reg;*/ 
    /* something_variable = rio_txrxm_response_received_ack_id_reg;*/ 
    /* something_variable = rio_txrxm_response_received_hop_count_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Response_Received task here*/
     $display("The Rio_TxRxM_Response_Received task is invoked");
end


/*realization of Rio_TxRxM_Violent_Packet_Received task*/
always
begin
wait (rio_txrxm_violent_packet_received_flag_reg === 1'b1);
    rio_txrxm_violent_packet_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_violent_packet_received_packet_size_reg;*/ 
    /* something_variable = rio_txrxm_violent_packet_received_violation_type_reg;*/ 
	/* someth_variabe = rio_txrxm_violent_packet_received_expected_crc_reg*/

     /*Insert code for realization Rio_TxRxM_Violent_Packet_Received task here*/
     $display("The Rio_TxRxM_Violent_Packet_Received task is invoked");
end


/*realization of Rio_TxRxM_Canceled_Packet_Received task*/
always
begin
wait (rio_txrxm_canceled_packet_received_flag_reg === 1'b1);
    rio_txrxm_canceled_packet_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_canceled_packet_received_packet_size_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Canceled_Packet_Received task here*/
     $display("The Rio_TxRxM_Canceled_Packet_Received task is invoked");
end


/*realization of Rio_TxRxM_Symbol_Received task*/
always
begin
wait (rio_txrxm_symbol_received_flag_reg === 1'b1);
    rio_txrxm_symbol_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_symbol_received_s_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_ackid_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_rsrv_1_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_s_parity_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_rsrv_2_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_buf_status_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_stype_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_place_reg;*/ 
    /* something_variable = rio_txrxm_symbol_received_granule_num_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Symbol_Received task here*/
     $display("The Rio_TxRxM_Symbol_Received task is invoked");
end


/*realization of Rio_TxRxM_Training_Received task*/
always
begin
wait (rio_txrxm_training_received_flag_reg === 1'b1);
    rio_txrxm_training_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_training_received_place_reg;*/ 
    /* something_variable = rio_txrxm_training_received_granule_num_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Training_Received task here*/
//     $display("The Rio_TxRxM_Training_Received task is invoked");
end


/*realization of Rio_TxRxM_Violent_Symbol_Received task*/
always
begin
wait (rio_txrxm_violent_symbol_received_flag_reg === 1'b1);
    rio_txrxm_violent_symbol_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_violent_symbol_received_symbol_buffer_reg;*/ 
    /* something_variable = rio_txrxm_violent_symbol_received_place_reg;*/ 
    /* something_variable = rio_txrxm_violent_symbol_received_granule_num_reg;*/ 
    /* something_variable = rio_txrxm_violent_symbol_received_violation_type_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Violent_Symbol_Received task here*/
//     $display("The Rio_TxRxM_Violent_Symbol_Received task is invoked");
end


/*realization of Rio_TxRxM_Granule_Received task*/
always
begin
wait (rio_txrxm_granule_received_flag_reg === 1'b1);
    rio_txrxm_granule_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_granule_received_granule_type_reg;*/ 
    /* something_variable = rio_txrxm_granule_received_granule_reg;*/ 
    /* something_variable = rio_txrxm_granule_received_has_frame_toggled_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Granule_Received task here*/
//     $display("The Rio_TxRxM_Granule_Received task is invoked");
end

//Implementation of serial-specific tasks
/*realization of Rio_TxRxM_Serial_Symbol_Received task*/
always
begin
wait (rio_txrxm_serial_symbol_received_flag_reg === 1'b1);
    rio_txrxm_serial_symbol_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_serial_symbol_received_stype0_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_parameter0_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_parameter1_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_stype1_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_cmd_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_crc_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_is_crc_valid_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_place_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_granule_num_reg;*/ 
    /* something_variable = rio_txrxm_serial_symbol_received_first_char_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Serial_Symbol_Received task here*/
    $display("----------------------------------------------------");
    $display("The Rio_TxRxM_Serial_Symbol_Received task is invoked");
    $display("stype0:%d", rio_txrxm_serial_symbol_received_stype0_reg);
    $display("par0:%d", rio_txrxm_serial_symbol_received_parameter0_reg);
    $display("par1:%d", rio_txrxm_serial_symbol_received_parameter1_reg);
    $display("stype1:%d", rio_txrxm_serial_symbol_received_stype1_reg);
    $display("cmd:%d", rio_txrxm_serial_symbol_received_cmd_reg);
    $display("Is CRC valid:%d", rio_txrxm_serial_symbol_received_is_crc_valid_reg);
    $display(" ");

end


/*realization of Rio_TxRxM_Serial_Violent_Symbol_Received task*/
always
begin
wait (rio_txrxm_serial_violent_symbol_received_flag_reg === 1'b1);
    rio_txrxm_serial_violent_symbol_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_serial_violent_symbol_received_symbol_buffer_reg;*/ 
    /* something_variable = rio_txrxm_serial_violent_symbol_received_place_reg;*/ 
    /* something_variable = rio_txrxm_serial_violent_symbol_received_granule_num_reg;*/ 
    /* something_variable = rio_txrxm_serial_violent_symbol_received_violation_type_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Serial_Violent_Symbol_Received task here*/
     $display("The Rio_TxRxM_Serial_Violent_Symbol_Received task is invoked");
end


/*realization of Rio_TxRxM_Serial_Code_Group_Received task*/
always
begin
wait (rio_txrxm_serial_code_group_received_flag_reg === 1'b1);
    rio_txrxm_serial_code_group_received_flag_reg = 1'b0;

    /* something_variable = rio_txrxm_serial_code_group_received_lane_mask_reg;*/ 


    /* something_variable = rio_txrxm_serial_code_group_received_cg_data_0_reg;*/
    /* something_variable = rio_txrxm_serial_code_group_received_cg_data_1_reg;  */
    /* something_variable = rio_txrxm_serial_code_group_received_cg_data_2_reg;*/
    /* something_variable = rio_txrxm_serial_code_group_received_cg_data_3_reg;*/
    /* something_variable = rio_txrxm_serial_code_group_received_is_rd_positive_reg;*/


     /*Insert code for realization Rio_TxRxM_Serial_Code_Group_Received task here*/
//     $display("The Rio_TxRxM_Serial_Code_Group_Received task is invoked");
end


/*realization of Rio_TxRxM_Serial_Violent_Character_Received task*/
always
begin
wait (rio_txrxm_serial_violent_character_received_flag_reg === 1'b1);
    rio_txrxm_serial_violent_character_received_flag_reg = 1'b0;

    // something_variable =  rio_txrxm_serial_violent_character_received_cg_data_0_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_cg_data_1_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_cg_data_2_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_cg_data_3_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_is_rd_positive_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_data_0_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_data_1_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_data_2_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_data_3_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_type_0_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_type_1_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_type_2_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_char_type_3_reg;
    // something_variable =  rio_txrxm_serial_violent_character_received_cg_lane_mask_reg; 
    // something_variable =  rio_txrxm_serial_violent_character_received_lane_mask_reg; 
    // something_variable =  rio_txrxm_serial_violent_character_received_violation_lane_mask_reg; 


    /* something_variable = rio_txrxm_serial_violent_character_received_lane_mask_reg;*/ 
    /* something_variable = rio_txrxm_serial_violent_character_received_lane_mask_reg;*/ 
    /* something_variable = rio_txrxm_serial_violent_character_received_violation_lane_mask_reg;*/ 

     /*Insert code for realization Rio_TxRxM_Serial_Violent_Character_Received task here*/
     $display("The Rio_TxRxM_Serial_Violent_Character_Received task is invoked");
end


/*realization of Rio_TxRxM_Serial_Character_Received task*/
always
begin
wait (rio_txrxm_serial_character_received_flag_reg === 1'b1);
    rio_txrxm_serial_character_received_flag_reg = 1'b0;

    // something_variable =  rio_txrxm_serial_character_received_char_data_0_reg;
    // something_variable =  rio_txrxm_serial_character_received_char_data_1_reg;
    // something_variable =  rio_txrxm_serial_character_received_char_data_2_reg;
    // something_variable =  rio_txrxm_serial_character_received_char_data_3_reg;
    // something_variable =  rio_txrxm_serial_character_received_char_type_0_reg;
    // something_variable =  rio_txrxm_serial_character_received_char_type_1_reg;
    // something_variable =  rio_txrxm_serial_character_received_char_type_2_reg;
    // something_variable =  rio_txrxm_serial_character_received_char_type_3_reg;
    // something_variable =  rio_txrxm_serial_character_received_lane_mask_reg; 

     /*Insert code for realization Rio_TxRxM_Serial_Character_Received task here*/
//     $display("The Rio_TxRxM_Serial_Character_Received task is invoked");
end

always
begin
wait (rio_txrxm_packet_to_send_flag_reg === 1'b1);
     rio_txrxm_packet_to_send_flag_reg = 1'b0;
     
     $display("The Packet_to_send:");

     for (i = 0; i < rio_txrxm_packet_to_send_packet_size; i = i + 1)
         $display("-->",$HW_Get_Data_TxRx(inst_Id, `RIO_TXRXM_PACKET_TO_SEND_BUFFER, i));

     $display("-----------------------------\n");
     
end


/***************************************************************************
 * Description: Creates instance of modles
 *
 * Notes: 
 *
 **************************************************************************/
initial if (mode === `RIO_TXRX_INST_PAR_MODE)
begin
    #1;
    inst_Id = $HW_RIO_TxRxM_Model_Create_Instance(
    `TXRX_PACKETS_BUFFER_SIZE,
    `TXRX_SYMBOLS_BUFFER_SIZE,
    `TXRX_GRANULES_BUFFER_SIZE,
    10,
    /*registers handlers passing*/
    rio_txrxm_request_received_packet_type_reg,

    rio_txrxm_request_received_target_tid,
    rio_txrxm_request_received_src_id,
    rio_txrxm_request_received_prio,
    rio_txrxm_request_received_wdptr,
    rio_txrxm_request_received_rdsize,
    rio_txrxm_request_received_format_type,
    rio_txrxm_request_received_ttype,
    rio_txrxm_request_received_address,
    rio_txrxm_request_received_ext_address,
    rio_txrxm_request_received_xamsbs,
    rio_txrxm_request_received_sec_id,
    rio_txrxm_request_received_sec_tid,
    rio_txrxm_request_received_mbox,
    rio_txrxm_request_received_letter,
    rio_txrxm_request_received_msgseg,
    rio_txrxm_request_received_ssize,
    rio_txrxm_request_received_msglen,
    rio_txrxm_request_received_doorbell_info,
    rio_txrxm_request_received_offset,


    rio_txrxm_request_received_dw_num_reg,
    rio_txrxm_request_received_transp_type_reg,
    rio_txrxm_request_received_ack_id_reg,
    rio_txrxm_request_received_transport_info_reg,
    rio_txrxm_request_received_hop_count_reg,
    rio_txrxm_request_received_flag_reg,
    rio_txrxm_response_received_prio_reg,
    rio_txrxm_response_received_ftype_reg,
    rio_txrxm_response_received_transaction_reg,
    rio_txrxm_response_received_status_reg,
    rio_txrxm_response_received_tr_info_reg,
    rio_txrxm_response_received_src_id_reg,
    rio_txrxm_response_received_target_tid_reg,
    rio_txrxm_response_received_sec_id_reg,
    rio_txrxm_response_received_sec_tid_reg,
    rio_txrxm_response_received_dw_num_reg,
    rio_txrxm_response_received_ack_id_reg,
    rio_txrxm_response_received_hop_count_reg,
    rio_txrxm_response_received_flag_reg,
    rio_txrxm_violent_packet_received_packet_size_reg,
    rio_txrxm_violent_packet_received_violation_type_reg,
    rio_txrxm_violent_packet_received_flag_reg,
    rio_txrxm_canceled_packet_received_packet_size_reg,
    rio_txrxm_canceled_packet_received_flag_reg,
    rio_txrxm_symbol_received_s_reg,
    rio_txrxm_symbol_received_ackid_reg,
    rio_txrxm_symbol_received_rsrv_1_reg,
    rio_txrxm_symbol_received_s_parity_reg,
    rio_txrxm_symbol_received_rsrv_2_reg,
    rio_txrxm_symbol_received_buf_status_reg,
    rio_txrxm_symbol_received_stype_reg,
    rio_txrxm_symbol_received_place_reg,
    rio_txrxm_symbol_received_granule_num_reg,
    rio_txrxm_symbol_received_flag_reg,
    rio_txrxm_training_received_place_reg,
    rio_txrxm_training_received_granule_num_reg,
    rio_txrxm_training_received_flag_reg,
    rio_txrxm_violent_symbol_received_symbol_buffer_reg,
    rio_txrxm_violent_symbol_received_place_reg,
    rio_txrxm_violent_symbol_received_granule_num_reg,
    rio_txrxm_violent_symbol_received_violation_type_reg,
    rio_txrxm_violent_symbol_received_flag_reg,
    rio_txrxm_granule_received_granule_type_reg,
    rio_txrxm_granule_received_granule_reg,
    rio_txrxm_granule_received_has_frame_toggled_reg,
    rio_txrxm_granule_received_flag_reg,
    rio_txrxm_set_pins_tframe_reg,
    rio_txrxm_set_pins_td_reg,
    rio_txrxm_set_pins_tdl_reg,
    rio_txrxm_set_pins_tclk_reg,
    rio_txrxm_set_pins_flag_reg,
    rio_txrxm_packet_to_send_packet_size,
    rio_txrxm_packet_to_send_flag_reg);

    if (inst_Id != -1)
        $display("TxRx instance is created; inst_Id = %d", inst_Id);
    else
    begin 
        $display("TxRx model instance creating failed");
        $finish;
    end
    
    result = $HW_Rio_TxRxM_Model_Initialize(inst_Id,
    `TXRX_DEFAULT_GRANULE, //param_set_default_granule,
    `TXRX_DEFAULT_FRAME_TOGGLE, //param_set_default_frame_toggle,
    `TXRX_LP_EP_IS_8, //param_set_lp_ep_is_8,
    `TXRX_WORK_MODE_IS_8,  //param_set_work_mode_is_8,
    `TXRX_EXT_ADDRESS_VALID, //param_set_in_ext_address_valid,
    `TXRX_EXT_ADDRESS_16, //param_set_in_ext_address_16
    `TXRX_NUM_TRAININGS,
    `TXRX_RQ_WND_ALLIGN,
    `RIO_TRUE   //RIO_TRUE -> training machine will work, otherwise will be disabled
    );
    if (result != `RIO_OK)
    begin
        $display ("TxRx %d:Initialization failed", inst_Id);
        $finish;
    end            

end


/***************************************************************************
 * Description: Creates instance of serial model
 *
 * Notes: 
 *
 **************************************************************************/
initial if (mode === `RIO_TXRX_INST_SERIAL_MODE)
begin
    #1;
    inst_Id = $HW_RIO_TxRxM_Serial_Model_Create_Instance (
// ,
    `RIO_STXRX_PACKET_BUF_SIZE,      //param_packets_buffer_size,
    `RIO_STXRX_SYMBOL_BUF_SIZE,      //param_symbols_buffer_size,
    `RIO_STXRX_CHAR_BUF_SIZE,        //param_character_buffer_size,
    `RIO_STXRX_CHAR_COLUMN_BUF_SIZE, //param_character_column_buffer_size,
    `RIO_STXRX_CG_BUF_SIZE,          //param_code_group_buffer_size,
    `RIO_STXRX_CG_COLUMN_BUF_SIZE,   //param_code_group_column_buffer_size,
    `RIO_STXRX_SINGLE_BIT_BUF_SIZE,  //param_single_bit_buffer_size,
    `RIO_STXRX_SINGLE_CHAR_BUF_SIZE, //param_single_character_buffer_size,
    `RIO_STXRX_COMP_SEQ_RQ_BUF_SIZE, //param_comp_seq_request_buffer_size,
    `RIO_STXRX_MANAG_RQ_BUF_SIZE,    //param_management_request_buffer_size,
    `RIO_STXRX_POP_BUF_SIZE,         //pop char buf size
    `RIO_STXRX_POP_BUF_SIZE,         //pop bit buf size
    /*registers handlers passing*/
    rio_txrxm_request_received_packet_type_reg,
    rio_txrxm_request_received_target_tid,
    rio_txrxm_request_received_src_id,
    rio_txrxm_request_received_prio,
    rio_txrxm_request_received_wdptr,
    rio_txrxm_request_received_rdsize,
    rio_txrxm_request_received_format_type,
    rio_txrxm_request_received_ttype,
    rio_txrxm_request_received_address,
    rio_txrxm_request_received_ext_address,
    rio_txrxm_request_received_xamsbs,
    rio_txrxm_request_received_sec_id,
    rio_txrxm_request_received_sec_tid,
    rio_txrxm_request_received_mbox,
    rio_txrxm_request_received_letter,
    rio_txrxm_request_received_msgseg,
    rio_txrxm_request_received_ssize,
    rio_txrxm_request_received_msglen,
    rio_txrxm_request_received_doorbell_info,
    rio_txrxm_request_received_offset,

    rio_txrxm_request_received_dw_num_reg,
    rio_txrxm_request_received_transp_type_reg,
    rio_txrxm_request_received_ack_id_reg,
    rio_txrxm_request_received_transport_info_reg,
    rio_txrxm_request_received_hop_count_reg,
    rio_txrxm_request_received_flag_reg,
    rio_txrxm_response_received_prio_reg,
    rio_txrxm_response_received_ftype_reg,
    rio_txrxm_response_received_transaction_reg,
    rio_txrxm_response_received_status_reg,
    rio_txrxm_response_received_tr_info_reg,
    rio_txrxm_response_received_src_id_reg,
    rio_txrxm_response_received_target_tid_reg,
    rio_txrxm_response_received_sec_id_reg,
    rio_txrxm_response_received_sec_tid_reg,
    rio_txrxm_response_received_dw_num_reg,
    rio_txrxm_response_received_ack_id_reg,
    rio_txrxm_response_received_hop_count_reg,
    rio_txrxm_response_received_flag_reg,
    rio_txrxm_violent_packet_received_packet_size_reg,
    rio_txrxm_violent_packet_received_violation_type_reg,
	rio_txrxm_violent_packet_received_expected_crc_reg,
    rio_txrxm_violent_packet_received_flag_reg,
    rio_txrxm_canceled_packet_received_packet_size_reg,
    rio_txrxm_canceled_packet_received_flag_reg,
    rio_txrxm_serial_symbol_received_stype0_reg,
    rio_txrxm_serial_symbol_received_parameter0_reg,
    rio_txrxm_serial_symbol_received_parameter1_reg,
    rio_txrxm_serial_symbol_received_stype1_reg,
    rio_txrxm_serial_symbol_received_cmd_reg,
    rio_txrxm_serial_symbol_received_crc_reg,
    rio_txrxm_serial_symbol_received_is_crc_valid_reg,
    rio_txrxm_serial_symbol_received_place_reg,
    rio_txrxm_serial_symbol_received_granule_num_reg,
    rio_txrxm_serial_symbol_received_first_char_reg,
    rio_txrxm_serial_symbol_received_flag_reg,
    rio_txrxm_serial_violent_symbol_received_symbol_buffer_reg,
    rio_txrxm_serial_violent_symbol_received_place_reg,
    rio_txrxm_serial_violent_symbol_received_granule_num_reg,
    rio_txrxm_serial_violent_symbol_received_violation_type_reg,
    rio_txrxm_serial_violent_symbol_received_flag_reg,

    rio_txrxm_serial_code_group_received_cg_data_0_reg,
    rio_txrxm_serial_code_group_received_cg_data_1_reg,
    rio_txrxm_serial_code_group_received_cg_data_2_reg,
    rio_txrxm_serial_code_group_received_cg_data_3_reg,
    rio_txrxm_serial_code_group_received_is_rd_positive_reg,
    rio_txrxm_serial_code_group_received_lane_mask_reg,
    rio_txrxm_serial_code_group_received_flag_reg,
    
    rio_txrxm_serial_violent_character_received_cg_data_0_reg,
    rio_txrxm_serial_violent_character_received_cg_data_1_reg,    
    rio_txrxm_serial_violent_character_received_cg_data_2_reg,    
    rio_txrxm_serial_violent_character_received_cg_data_3_reg,    
    rio_txrxm_serial_violent_character_received_is_rd_positive_reg,
    rio_txrxm_serial_violent_character_received_char_data_0_reg,
    rio_txrxm_serial_violent_character_received_char_data_1_reg,    
    rio_txrxm_serial_violent_character_received_char_data_2_reg,    
    rio_txrxm_serial_violent_character_received_char_data_3_reg,    
    rio_txrxm_serial_violent_character_received_char_type_0_reg,
    rio_txrxm_serial_violent_character_received_char_type_1_reg,
    rio_txrxm_serial_violent_character_received_char_type_2_reg,
    rio_txrxm_serial_violent_character_received_char_type_3_reg,
    rio_txrxm_serial_violent_character_received_cg_lane_mask_reg,
    rio_txrxm_serial_violent_character_received_lane_mask_reg,
    rio_txrxm_serial_violent_character_received_violation_lane_mask_reg,
    rio_txrxm_serial_violent_character_received_flag_reg,
    
    rio_txrxm_serial_character_received_char_data_0_reg,
    rio_txrxm_serial_character_received_char_data_1_reg,
    rio_txrxm_serial_character_received_char_data_2_reg,
    rio_txrxm_serial_character_received_char_data_3_reg,
    rio_txrxm_serial_character_received_char_type_0_reg,
    rio_txrxm_serial_character_received_char_type_1_reg,
    rio_txrxm_serial_character_received_char_type_2_reg,
    rio_txrxm_serial_character_received_char_type_3_reg,
    rio_txrxm_serial_character_received_lane_mask_reg,
    rio_txrxm_serial_character_received_flag_reg,
    
    rio_txrxm_set_pins_tlane0_reg,
    rio_txrxm_set_pins_tlane1_reg,
    rio_txrxm_set_pins_tlane2_reg,
    rio_txrxm_set_pins_tlane3_reg,
    rio_txrxm_serial_set_pins_flag_reg,
    rio_txrxm_packet_to_send_packet_size,
    rio_txrxm_packet_to_send_flag_reg,
    rio_txrxm_set_pins_tlane0_10_reg,
    rio_txrxm_set_pins_tlane1_10_reg,
    rio_txrxm_set_pins_tlane2_10_reg,
    rio_txrxm_set_pins_tlane3_10_reg,
    rio_txrxm_serial_set_pins_10bit_flag_reg);

    if (inst_Id === `RIO_STXRX_ERR_VAL)
    begin
        $display("Error during Serial TxRx instance creating");
        $finish;
    end


    if ($HW_Serial_Rio_TxRxM_Model_Initialize( inst_Id,
    `RIO_TXRX_IS_1X,              //param_set_lp_serial_is_1x,
    `RIO_TXRX_FORSE_1X_MODE,      //param_set_is_force_1x_mode,
    `RIO_TXRX_IS_FORSE_1X_LANE0,  //param_set_is_force_1x_mode_lane_0,
    `RIO_TXRX_IS_EXT_ADDR_VALID,  //param_set_is_ext_address_valid,
    `RIO_TXRX_EXT_ADDR_16,        //param_set_is_ext_address_16,
    `RIO_TXRX_IS_INIT_PROC_ON,    //param_set_is_init_proc_on,
    `RIO_TXRX_SILENCE_PERIOD,     //param_set_silence_period,
    `RIO_TXRX_IS_COMP_SEQ_GEN_ON, //param_set_is_comp_seq_gen_on,
    `RIO_TXRX_COMP_SEQ_RATE,      //param_set_comp_seq_rate,
    `RIO_TXRX_STATUS_SYM_GEN_ON,  //param_set_is_status_sym_gen_on,
    `RIO_TXRX_STATUS_SYM_RATE,    //param_set_status_sym_rate
    `RIO_TXRX_DISCOVERY_PERIOD  //discovery period
    ) !== `RIO_OK)
    begin
        $display("Unable to initialize serial txrx instance %d", inst_Id);
        $finish;
    end

    $display("Serial TxRx instance is created; instId = %d", inst_Id);

end


/***************************************************************************
 * Description: Reset flag registers
 *
 * Notes: 
 *
 **************************************************************************/
initial
begin
    rio_txrxm_request_received_flag_reg = 1'b0;
    rio_txrxm_response_received_flag_reg = 1'b0;
    rio_txrxm_violent_packet_received_flag_reg = 1'b0;
    rio_txrxm_canceled_packet_received_flag_reg = 1'b0;
    rio_txrxm_symbol_received_flag_reg = 1'b0;
    rio_txrxm_training_received_flag_reg = 1'b0;
    rio_txrxm_violent_symbol_received_flag_reg = 1'b0;
    rio_txrxm_granule_received_flag_reg = 1'b0;
    rio_txrxm_set_pins_flag_reg = 1'b0;
    inst_Id = `RIO_TXRX_ERROR_INST_ID;

    rio_txrxm_serial_symbol_received_flag_reg = 1'b0;
    rio_txrxm_serial_violent_symbol_received_flag_reg = 1'b0;
    rio_txrxm_serial_code_group_received_flag_reg = 1'b0;
    rio_txrxm_serial_violent_character_received_flag_reg = 1'b0;
    rio_txrxm_serial_character_received_flag_reg = 1'b0;
    rio_txrxm_serial_set_pins_flag_reg = 1'b0;

end


endmodule
