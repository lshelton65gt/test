/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\ts_tb\module_txrx\scenario_txrx.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  scenarios for the txrx module
*
* Notes:
*
******************************************************************************/
reg start_scenario;
reg start_training;
reg simple_scenario;
reg simple_rw_scenario;


`define INIT_DELAY 256*8*10
`define RQ_TID  100

initial
begin
    start_scenario = 1'b0;
    start_training = 1'b0;
    simple_scenario = 1'b0;
    simple_rw_scenario = 1'b0;
end 


always
begin
    wait(start_scenario === 1'b1);
    Scenario1;
    start_scenario = 1'b0;
end

always
begin
    wait(start_training === 1'b1);
    Training_Scenario;
    start_training = 1'b0;
end

always 
begin
    wait(simple_scenario === 1'b1);

    //wait for initialization
#900000;
    //send a packet
    //request for start of packet
    while ($HW_Rio_TxRxM_Serial_Symbol_Request(inst_Id,
    4, //stype0 = status
    0, //parameter0 = packet ack_Id
    12, //parameter1 = buf_Status
    0, //stype1 = SOP
    0, //cmd = 0
    `NOT_APPLICABLE, //crc value 
    `RIO_FALSE, //is_Crc_Valid crc will be calculated by txrx

    `RIO_TXRX_SYMBOL_FREE, /*place in stream*/
    `NOT_APPLICABLE //granule_Num
    ) == `RIO_RETRY)  #(`RIO_TXRX_WAIT_BEFORE_RETRY); 

    //packet data request
    while ($HW_Rio_TxRxM_Packet_Struct_Request(inst_Id,
        0,  // rq_ack_id
        1, //rq_prio,
        `NOT_APPLICABLE, //rq_rsrv_phy_1,
        `NOT_APPLICABLE, //rq_rsrv_phy_2,
        00,              //rq_tt,
        6, // rq_ftype = swrite
        16'h0100, //rq_transport_info,
        `NOT_APPLICABLE, //rq_ttype
        `NOT_APPLICABLE, //rq_rdwr_size,
        `NOT_APPLICABLE, //rq_srtr_tid,
        `NOT_APPLICABLE, //rq_hop_count,
        `NOT_APPLICABLE, //rq_config_offset,
        `NOT_APPLICABLE, //rq_wdptr,
        `NOT_APPLICABLE, //rq_status,
        `NOT_APPLICABLE, //rq_doorbell_info,
        `NOT_APPLICABLE, //rq_msglen,
        `NOT_APPLICABLE, //rq_letter,
        `NOT_APPLICABLE, //rq_mbox,
        `NOT_APPLICABLE, //rq_msgseg,
        `NOT_APPLICABLE, //rq_ssize,
        1000, //rq_address,
        20, //rq_extended_address,
        0, //rq_xamsbs,
        `RIO_FALSE, //rq_ext_address_valid,
        `RIO_FALSE, //rq_ext_address_16,
        `NOT_APPLICABLE, //rq_rsrv_ftype6,
        `NOT_APPLICABLE, //rq_rsrv_ftype8,
        `NOT_APPLICABLE, //RIOrq_rsrv_ftype10,
        0,              //user defiened crc
        `RIO_FALSE,      //don't use user-defined crc
        16 //dw_size
        ) == `RIO_RETRY)  #(`RIO_TXRX_WAIT_BEFORE_RETRY); 

    //request for end of packet
    while ($HW_Rio_TxRxM_Serial_Symbol_Request(inst_Id,
    4, //stype0 = status
    0, //parameter0 = packet ack_Id
    12, //parameter1 = buf_Status
    2, //stype1 = EOP
    0, //cmd = 0
    `NOT_APPLICABLE, //crc value 
    `RIO_FALSE, //is_Crc_Valid crc will be calculated by txrx

    `RIO_TXRX_SYMBOL_FREE, /*place in stream*/
    `NOT_APPLICABLE //granule_Num
    ) == `RIO_RETRY)  #(`RIO_TXRX_WAIT_BEFORE_RETRY); 

    simple_scenario = 1'b0;
end


/***************************************************************************
 * Task : Training_Scenario
 *
 * Description: Performs training scenario
 *
 * Notes: 
 *
 **************************************************************************/
task Training_Scenario;
begin
    while($HW_Rio_TxRxM_Symbol_Request(inst_Id,
        1, //sym_s,
        000,    //sym_ackid,
        0,   //sym_rsrv_1,
        0,   //sym_s_parity,
        0, //sym_rsrv_2,
        15, //sym_buf_status,
        `RIO_TXRX_LINK_MAINTENANCE_STYPE, // sym_stype,
        `RIO_TXRX_SYMBOL_FREE, //place,
        0, //granule_num
        ) == `RIO_RETRY) #(`RIO_TXRX_WAIT_BEFORE_RETRY);

    //send training
    repeat (`NUM_TRAININGS)
    begin
        while ($HW_Rio_TxRxM_Training_Request(inst_Id,
            `RIO_TXRX_SYMBOL_FREE, //place,
            0) == `RIO_RETRY) #(`RIO_TXRX_WAIT_BEFORE_RETRY);
    end
end
endtask


/***************************************************************************
 * Task : Scenario1
 *
 * Description: Simple scenario
 *
 * Notes: 
 *
 **************************************************************************/
task Scenario1;
integer i;
begin
    for (i = 0; i<32; i = i + 1)                                    /*any payload will be passed*/
        $HW_Put_Data_TxRx(`RIO_TXRXM_PACKET_STRUCT_REQUEST_DATA, i, 32'hABABABAB, 32'h10101010);


    while($HW_Rio_TxRxM_Packet_Struct_Request(inst_Id,
        7,  // rq_ack_id
        1, //rq_prio,
        `NOT_APPLICABLE, //rq_rsrv_phy_1,
        `NOT_APPLICABLE, //rq_rsrv_phy_2,
        00,              //rq_tt,
        `RIO_TXRX_WRITE, // rq_ftype
        16'h0000, //rq_transport_info,
        4,        //rq_ttype == NREAD,
        0,        //rq_rdwr_size,
        0,       //rq_srtr_tid,
        255,    //rq_hop_count,
        `NOT_APPLICABLE, //rq_config_offset,
        0,      //rq_wdptr,
        `NOT_APPLICABLE, //rq_status,
        `NOT_APPLICABLE, //rq_doorbell_info,
        `NOT_APPLICABLE, //rq_msglen,
        `NOT_APPLICABLE, //rq_letter,
        `NOT_APPLICABLE, //rq_mbox,
        `NOT_APPLICABLE, //rq_msgseg,
        `NOT_APPLICABLE, //rq_ssize,
        0, //rq_address,
        0, //rq_extended_address,
        0, //rq_xamsbs,
        `RIO_FALSE, //rq_ext_address_valid,
        `RIO_FALSE, //rq_ext_address_16,
        `NOT_APPLICABLE, //rq_rsrv_ftype6,
        `NOT_APPLICABLE, //rq_rsrv_ftype8,
        `NOT_APPLICABLE, //RIOrq_rsrv_ftype10,
        0,              //user defiened crc
        `RIO_FALSE,      //don't use user-defined crc
        16 //dw_size
        ) == `RIO_RETRY)
        #(`RIO_TXRX_WAIT_BEFORE_RETRY);
//send eop symbol

    while($HW_Rio_TxRxM_Symbol_Request(inst_Id,
        1,  //sym_s,     
        2,  //sym_ackid,
        0,  //sym_rsrv_1,
        0,  //sym_s_parity,
        0,  //sym_rsrv_2,
        6,  //sym_buf_status,
        4,  //sym_stype,
        `RIO_TXRX_SYMBOL_FREE, //place,
        `NOT_APPLICABLE,  //granule_num
        ) == `RIO_RETRY)
        #(`RIO_TXRX_WAIT_BEFORE_RETRY);

#(`RIO_TXRX_WAIT_BEFORE_RETRY * 8 * (8 + 16 /*dw_size*/) );

end
endtask

always 
begin
    wait (simple_rw_scenario === 1'b1)
/*since the 2.0 ver of models the txrx include 
initialization state machine so here intialization 
process can is scipped. 
this will be work only in case when last parameter of initialize routine is 
RIO_TRUE. If this parameter is RIO_FALSE the Training_Scenario task usage is
necessary
*/

#(`INIT_DELAY); //scip initialization process.

`ifdef PARALLEL_SCENARIO
    while($HW_Rio_TxRxM_Symbol_Request(inst_Id,
        1, //sym_s,
        000,    //sym_ackid,
        0,   //sym_rsrv_1,
        0,   //sym_s_parity,
        0, //sym_rsrv_2,
        8, //sym_buf_status,
        `RIO_TXRX_LINK_RESPONCE_STYPE, // sym_stype,
        `RIO_TXRX_SYMBOL_FREE, //place,
        0, //granule_num
        ) == `RIO_RETRY) #(`RIO_TXRX_WAIT_BEFORE_RETRY);
#(`INIT_DELAY);
`endif

//now initialization finished NREAD will  be performed


    while($HW_Rio_TxRxM_Packet_Struct_Request(inst_Id,
        0,  // rq_ack_id
        1, //rq_prio,
        `NOT_APPLICABLE, //rq_rsrv_phy_1,
        `NOT_APPLICABLE, //rq_rsrv_phy_2,
        00,              //rq_tt,
        `RIO_TXRX_NONINTERV_REQUEST, // rq_ftype
        16'h0001, //rq_transport_info,
        4,        //rq_ttype == NREAD,
        32'b1011,        //rq_rdwr_size = 8 byte,
        `RQ_TID,     //rq_srtr_tid,
        `NOT_APPLICABLE,    //rq_hop_count,
        `NOT_APPLICABLE, //rq_config_offset,
        0,      //rq_wdptr,
        `NOT_APPLICABLE, //rq_status,
        `NOT_APPLICABLE, //rq_doorbell_info,
        `NOT_APPLICABLE, //rq_msglen,
        `NOT_APPLICABLE, //rq_letter,
        `NOT_APPLICABLE, //rq_mbox,
        `NOT_APPLICABLE, //rq_msgseg,
        `NOT_APPLICABLE, //rq_ssize,
        `RIO_PE_IO_START_ADDRESS >> 3, //rq_address,
        `RIO_PE_IO_EXT_ADDRESS, //rq_extended_address,
        `RIO_PE_IO_XAMSBS, //rq_xamsbs,
        `RIO_FALSE, //rq_ext_address_valid,
        `RIO_FALSE, //rq_ext_address_16,
        `NOT_APPLICABLE, //rq_rsrv_ftype6,
        `NOT_APPLICABLE, //rq_rsrv_ftype8,
        `NOT_APPLICABLE, //RIOrq_rsrv_ftype10,
        0,              //user defiened crc
        `RIO_FALSE,      //don't use user-defined crc
        0 //dw_size
        ) == `RIO_RETRY)
        #(`RIO_TXRX_WAIT_BEFORE_RETRY);
//send eop symbol
    while($HW_Rio_TxRxM_Symbol_Request(inst_Id,
        1,  //sym_s,     
        2,  //sym_ackid,
        0,  //sym_rsrv_1,
        0,  //sym_s_parity,
        0,  //sym_rsrv_2,
        6,  //sym_buf_status,
        4,  //sym_stype,
        `RIO_TXRX_SYMBOL_FREE, //place,
        `NOT_APPLICABLE,  //granule_num
        ) == `RIO_RETRY)
        #(`RIO_TXRX_WAIT_BEFORE_RETRY);

//wait for responce from the PE
    @(rio_txrxm_response_received_flag_reg);

//check that this is response for our req
    if (rio_txrxm_response_received_target_tid_reg == `RQ_TID)
    begin
        $display("responce for nread received");
        $display("data value:", 
            $HW_Get_Data_TxRx(inst_Id, `RIO_TXRXM_RESPONSE_RECEIVED_PACKET_DATA, 0, `MSW),
            $HW_Get_Data_TxRx(inst_Id, `RIO_TXRXM_RESPONSE_RECEIVED_PACKET_DATA, 0, `LSW));
    end
//send packet accpeted
    while($HW_Rio_TxRxM_Symbol_Request(inst_Id,
        1,  //sym_s,     
        rio_txrxm_response_received_ack_id_reg,  //sym_ackid,
        0,  //sym_rsrv_1,
        0,  //sym_s_parity,
        0,  //sym_rsrv_2,
        8,  //sym_buf_status,
        `RIO_TXRX_PACKET_ACCEPTED_STYPE,  //sym_stype,
        `RIO_TXRX_SYMBOL_FREE, //place,
        `NOT_APPLICABLE,  //granule_num
        ) == `RIO_RETRY)
        #(`RIO_TXRX_WAIT_BEFORE_RETRY);

    simple_rw_scenario = 1'b0;
end


