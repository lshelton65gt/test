/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: M:\zkg_rio\riosuite\src\verilog_env\pliwrap\txrxm\wrapvars.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  Wrappers variables description
*
* Notes:        This file is generated automaticaly.
*               SHALL INCLUDED ONLY TO THE wrapper_txrx.c FILE
*
******************************************************************************/

#ifndef WRAPWARS_TXRX_H
#define WRAPWARS_TXRX_H

/*Function trays*/
RIO_TXRXM_PARALLEL_MODEL_FTRAY_T Rio_Txrxm_Model_Ftray_T_Tray;
/*Callback tray*/
RIO_TXRXM_PARALLEL_MODEL_CALLBACK_TRAY_T Rio_Txrxm_Model_Callback_Tray_T_Tray;

#endif /*WRAPWARS_TXRX_H*/

