/******************************************************************************
*
*       COPYRIGHT 2002-2003 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\txrxm\s_txrx_wrapper.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*		to display revision history information.
*               
* Description: PLI wrapper implementation file
*
* Notes: template is generated
*
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "s_txrx_wrapper.h" 
#include "s_prototypes.h" 


/*Global variables*/
extern int rio_TxRx_Wrap_Initialized;

/*Contexts for instances*/
extern RIO_CONTEXT_TXRX_T *Contexts_TxRx;

extern int TxRx_Max_Inst_Num;  /*Maximum instance number*/
extern int TxRx_Inst_Num;  /*Current allocated instances number*/
/*Internal input buffer for SW_Rio_TxRxM_Packet_Struct_Request routine's variable*/ 
extern RIO_DW_T Rio_TxRxM_Packet_Struct_Request_Data[PLI_MAX_ARRAY_SIZE];
/*Internal input buffer for SW_Rio_TxRxM_Packet_Stream_Request routine's variable*/ 
extern RIO_UCHAR_T Rio_TxRxM_Packet_Stream_Request_Packet_Buffer[PLI_MAX_STREAM_SIZE];

/*Function trays*/
extern RIO_TXRXM_SERIAL_MODEL_FTRAY_T Rio_Txrxm_Serial_Model_Ftray_T_Tray;
/*Callback tray*/
extern RIO_TXRXM_SERIAL_MODEL_CALLBACK_TRAY_T Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray;

/* Flags to handle 10bit interface */
extern RIO_BOOL_T ten_Bit_Interface_TxRxM;
extern RIO_BOOL_T set_Serial_Pins_10bit_Flag_Reg_TxRxM;

void SW_TxRx_Wrapper_Message (int id, char *type, char * msg);

/***************************************************************************
 * Function : SW_Serial_S_TxRx_Register_Access
 *
 * Description: Provides access to registers for read/write
 *
 * Returns: RIO_OK if all right
 *
 * Notes:  
 *
 **************************************************************************/
int SW_Serial_S_TxRx_Register_Access(int instId, int config_Offset, unsigned long *data,  REG_ACCESS_T acc_Type)
{
    	if (acc_Type == REG_READ)
    	{
    		/*Realize code for reading from register*/
    	}
    	else 
    	{
    	    /*Realize code for writing to register*/
    	}

        return RIO_OK;
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Symbol_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Symbol_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Symbol_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Symbol_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_SYMBOL_SERIAL_STRUCT_T sym;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Symbol_Request - wrapper isn't initialized");
    sym.stype0 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    sym.parameter0 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    sym.parameter1 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    sym.stype1 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 5 ); 
    sym.cmd = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    sym.crc = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    sym.is_Crc_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 8 ); 
/*End of the fields value getting for the sym structure*/
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 9 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 10 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Symbol_Request))( Contexts_TxRx[instId].handle,
        &sym , place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Character_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Character_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Character_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Character_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_UCHAR_T character;
    RIO_CHARACTER_TYPE_T char_Type;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Character_Request - wrapper isn't initialized");
    character = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    char_Type = ( RIO_CHARACTER_TYPE_T ) acc_fetch_tfarg_int( k + 3 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 4 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 5 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Character_Request))( Contexts_TxRx[instId].handle,
        character , char_Type , place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Character_Column_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Character_Column_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Character_Column_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Character_Column_Request ()
{
    int k, i;
    int instId;
/*Input parameters*/
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
    RIO_UCHAR_T column[RIO_PL_SERIAL_NUMBER_OF_LANES];
    RIO_CHARACTER_TYPE_T char_Type[RIO_PL_SERIAL_NUMBER_OF_LANES]; /*the array size is 4 bytes*/
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( ++k );
 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Character_Column_Request - wrapper isn't initialized");
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int(++k); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int(++k); 

/*access column data*/
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        column[i] = (RIO_UCHAR_T)acc_fetch_tfarg_int( ++k );
/*access character types*/
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        char_Type[i] = (RIO_CHARACTER_TYPE_T)acc_fetch_tfarg_int( ++k );

    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Character_Column_Request))( Contexts_TxRx[instId].handle,
        column , char_Type , place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Code_Group_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Code_Group_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Code_Group_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Code_Group_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    unsigned int code_Group;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Code_Group_Request - wrapper isn't initialized");
    code_Group = ( unsigned int ) acc_fetch_tfarg_int( k + 2 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 3 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 4 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Code_Group_Request))( Contexts_TxRx[instId].handle,
        code_Group , place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Code_Group_Column_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Code_Group_Column_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Code_Group_Column_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Code_Group_Column_Request ()
{
    int k, i;
    int instId;
/*Input parameters*/
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
    unsigned int column[RIO_PL_SERIAL_NUMBER_OF_LANES];
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Code_Group_Column_Request - wrapper isn't initialized");
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 2 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 3 ); 
    k+=4; /*shift k to the current position in the input parameters list*/
/*get codegroup column*/
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        column[i] = (unsigned int) acc_fetch_tfarg_int( k++ );
  
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Code_Group_Column_Request))( Contexts_TxRx[instId].handle,
        column , place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Single_Character_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Single_Character_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Single_Character_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Single_Character_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_UCHAR_T character;
    RIO_CHARACTER_TYPE_T char_Type;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
    RIO_PLACE_IN_GRANULE_T place_In_Granule;
    unsigned int character_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Single_Character_Request - wrapper isn't initialized");
    character = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    char_Type = ( RIO_CHARACTER_TYPE_T ) acc_fetch_tfarg_int( k + 3 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 4 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 5 ); 
    place_In_Granule = ( RIO_PLACE_IN_GRANULE_T ) acc_fetch_tfarg_int( k + 6 ); 
    character_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 7 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Single_Character_Request))( Contexts_TxRx[instId].handle,
        character , char_Type , place , granule_Num , place_In_Granule , character_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Single_Bit_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Single_Bit_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Single_Bit_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Single_Bit_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_SIGNAL_T bit_Value;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
    RIO_PLACE_IN_GRANULE_T place_In_Granule;
    unsigned int character_Num;
    unsigned int bit_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Single_Bit_Request - wrapper isn't initialized");
    bit_Value = ( RIO_SIGNAL_T ) acc_fetch_tfarg_int( k + 2 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 3 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 4 ); 
    place_In_Granule = ( RIO_PLACE_IN_GRANULE_T ) acc_fetch_tfarg_int( k + 5 ); 
    character_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 6 ); 
    bit_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 7 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Single_Bit_Request))( Contexts_TxRx[instId].handle,
        bit_Value , place , granule_Num , place_In_Granule , character_Num , bit_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Comp_Seq_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Comp_Seq_Request routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Comp_Seq_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Comp_Seq_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Comp_Seq_Request - wrapper isn't initialized");
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 2 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 3 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Comp_Seq_Request))( Contexts_TxRx[instId].handle,
        place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Pcs_Pma_Machine_Management routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Pcs_Pma_Machine_Management routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_BOOL_T set_On_Machine;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
    RIO_PCS_PMA_MACHINE_T machine_Type;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Pcs_Pma_Machine_Management - wrapper isn't initialized");
    set_On_Machine = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 2 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 3 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 4 ); 
    machine_Type = ( RIO_PCS_PMA_MACHINE_T ) acc_fetch_tfarg_int( k + 5 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Pcs_Pma_Machine_Management))( Contexts_TxRx[instId].handle,
        set_On_Machine , place , granule_Num , machine_Type));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Pop_Bit
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Pop_Bit routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Pop_Bit routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Pop_Bit ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_UCHAR_T lane_Num;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Pop_Bit - wrapper isn't initialized");
    lane_Num = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 3 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 4 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Pop_Bit))( Contexts_TxRx[instId].handle,
        lane_Num , place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Pop_Character
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Serial_Pop_Character routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Serial_Pop_Character routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Pop_Character ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_UCHAR_T lane_Num;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Serial_Pop_Character - wrapper isn't initialized");
    lane_Num = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 3 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 4 ); 
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Serial_Pop_Character))( Contexts_TxRx[instId].handle,
        lane_Num , place , granule_Num));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Model_Initialize
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Model_Initialize routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Model_Initialize routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Model_Initialize ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_TXRXM_SERIAL_PARAM_SET_T param_Set;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Model_Initialize - wrapper isn't initialized");
    param_Set.lp_Serial_Is_1x = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 2 ); 
    param_Set.is_Force_1x_Mode = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 3 ); 
    param_Set.is_Force_1x_Mode_Lane_0 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 4 ); 
    param_Set.is_Ext_Address_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 5 ); 
    param_Set.is_Ext_Address_16 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 6 ); 
    param_Set.is_Init_Proc_On = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 7 ); 
    param_Set.silence_Period = ( int ) acc_fetch_tfarg_int( k + 8 ); 
    param_Set.is_Comp_Seq_Gen_On = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 9 ); 
    param_Set.comp_Seq_Rate = ( int ) acc_fetch_tfarg_int( k + 10 ); 
    param_Set.is_Status_Sym_Gen_On = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 11 ); 
    param_Set.status_Sym_Rate = ( int ) acc_fetch_tfarg_int( k + 12 ); 
    param_Set.discovery_Period = ( int ) acc_fetch_tfarg_int( k + 13 ); 
    param_Set.icounter_Max     = 2;
/*End of the fields value getting for the param_Set structure*/
    tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Model_Initialize))( Contexts_TxRx[instId].handle,
        &param_Set));
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Get_Pins
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Get_Pins routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Get_Pins routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Get_Pins ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_SIGNAL_T rlane0;
    RIO_SIGNAL_T rlane1;
    RIO_SIGNAL_T rlane2;
    RIO_SIGNAL_T rlane3;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_S(instId, "Error in the Rio_TxRxM_Get_Pins - wrapper isn't initialized");
    rlane0 = ( RIO_SIGNAL_T ) acc_fetch_tfarg_int( k + 2 ); 
    rlane1 = ( RIO_SIGNAL_T ) acc_fetch_tfarg_int( k + 3 ); 
    rlane2 = ( RIO_SIGNAL_T ) acc_fetch_tfarg_int( k + 4 ); 
    rlane3 = ( RIO_SIGNAL_T ) acc_fetch_tfarg_int( k + 5 ); 
    (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Get_Pins))( Contexts_TxRx[instId].handle,
        rlane0 , rlane1 , rlane2 , rlane3);
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Get_Pins_New
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Get_Pins_New routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Get_Pins_New routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Get_Pins_New ()
{
    int i_Count;
    int instId;
/*Input parameters*/
    unsigned short rlane0_10;       /* receive 10-bit data on lane 0*/
    unsigned short rlane1_10;       /* receive 10-bit data on lane 1*/
    unsigned short rlane2_10;       /* receive 10-bit data on lane 2*/
    unsigned short rlane3_10;       /* receive 10-bit data on lane 3*/
/* End of parameters defintion*/

    instId = acc_fetch_tfarg_int( 1 );

    rlane0_10 = acc_fetch_tfarg_int( 2 );
    rlane1_10 = acc_fetch_tfarg_int( 3 );
    rlane2_10 = acc_fetch_tfarg_int( 4 );
    rlane3_10 = acc_fetch_tfarg_int( 5 );

 /*check that instance id is correct*/
    Wrap_Check_Id_S(instId, "Error in the Rio_TxRxM_Get_Pins - wrapper isn't initialized");
    
    for (i_Count =0; i_Count <= 10; i_Count++)
    {
        (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Get_Pins))( Contexts_TxRx[instId].handle,
                (rlane0_10 >> (9 - i_Count) ) & 1, 
                (rlane1_10 >> (9 - i_Count) ) & 1, 
                (rlane2_10 >> (9 - i_Count) ) & 1, 
                (rlane3_10 >> (9 - i_Count) ) & 1 );
    }
}


/*****************************************************************************/
/*     Callbacks implementation                                              */
/*****************************************************************************/



/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Symbol_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Serial_Symbol_Received
 *
 * Returns: result of Rio_TxRxM_Serial_Symbol_Received routine work
 *
 * Notes: 
 *
 **************************************************************************/
int SW_Serial_Rio_TxRxM_Serial_Symbol_Received (
    RIO_CONTEXT_T context,
    RIO_SYMBOL_SERIAL_STRUCT_T * sym,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Stype0_Reg, (int)sym->stype0);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Parameter0_Reg, (int)sym->parameter0);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Parameter1_Reg, (int)sym->parameter1);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Stype1_Reg, (int)sym->stype1);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Cmd_Reg, (int)sym->cmd);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Crc_Reg, (int)sym->crc);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Is_Crc_Valid_Reg, (int)sym->is_Crc_Valid);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Place_Reg, (int)place);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Granule_Num_Reg, (int)granule_Num);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_First_Char_Reg, (int)sym->first_Char);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Flag_Reg, 1);
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Violent_Symbol_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Serial_Violent_Symbol_Received
 *
 * Returns: result of Rio_TxRxM_Serial_Violent_Symbol_Received routine work
 *
 * Notes: 
 *
 **************************************************************************/
int SW_Serial_Rio_TxRxM_Serial_Violent_Symbol_Received (
    RIO_CONTEXT_T context,
    unsigned int symbol_Buffer,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num,
    RIO_SYMBOL_SERIAL_VIOLATION_T violation_Type )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Symbol_Buffer_Reg, (int)symbol_Buffer);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Place_Reg, (int)place);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Granule_Num_Reg, (int)granule_Num);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Violation_Type_Reg, (int)violation_Type);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Flag_Reg, 1);
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Code_Group_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Serial_Code_Group_Received
 *
 * Returns: result of Rio_TxRxM_Serial_Code_Group_Received routine work
 *
 * Notes: 
 *
 **************************************************************************/
int SW_Serial_Rio_TxRxM_Serial_Code_Group_Received (
    RIO_CONTEXT_T context,
    RIO_PCS_PMA_CODE_GROUP_T code_Group )
{
    int instId, i;
    unsigned int is_RD_Positive;

    instId = (int)context;
/*Pass all lanes codegroup value*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_0_Reg, 
        code_Group.code_Group_Data[0]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_1_Reg, 
        code_Group.code_Group_Data[1]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_2_Reg, 
        code_Group.code_Group_Data[2]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_3_Reg, 
        code_Group.code_Group_Data[3]);
/*combinate codegroup running disparity flags to the single flag register*/
    is_RD_Positive = 0;
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        is_RD_Positive = (is_RD_Positive << i) | (code_Group.is_Running_Disparity_Positive[i] & 1);

    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_Is_RD_Positive_Reg, is_RD_Positive);

    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_Lane_Mask_Reg, (int)code_Group.lane_Mask);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_Flag_Reg, 1);
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Violent_Character_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Serial_Violent_Character_Received
 *
 * Returns: result of Rio_TxRxM_Serial_Violent_Character_Received routine work
 *
 * Notes: 
 *
 **************************************************************************/
int SW_Serial_Rio_TxRxM_Serial_Violent_Character_Received (
    RIO_CONTEXT_T context,
    RIO_PCS_PMA_CODE_GROUP_T code_Group,
    RIO_PCS_PMA_CHARACTER_T character,
    RIO_UCHAR_T violation_Lane_Mask )
{
    int instId, i;
    unsigned int is_RD_Positive;
    instId = (int)context;
    /*pass codegroup data array*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_0_Reg,
        (int)code_Group.code_Group_Data[0]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_1_Reg, 
        (int)code_Group.code_Group_Data[1]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_2_Reg, 
        (int)code_Group.code_Group_Data[2]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_3_Reg, 
        (int)code_Group.code_Group_Data[3]);
    /*is running disparity flags will be grouped to the single flags register*/
    is_RD_Positive = 0;
    for (i = 0; i < RIO_PL_SERIAL_NUMBER_OF_LANES; i++)
        is_RD_Positive = (is_RD_Positive << i) | (code_Group.is_Running_Disparity_Positive[i] & 1);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_is_RD_Positive_Reg,
        (int)is_RD_Positive);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Lane_Mask_Reg, 
        (int)code_Group.lane_Mask);
    /*passing character data array*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_0_Reg, 
        (int)character.character_Data[0]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_1_Reg, 
        (int)character.character_Data[1]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_2_Reg, 
        (int)character.character_Data[2]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_3_Reg,  
        (int)character.character_Data[3]);
    /*passing character type*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_0_Reg,  
        (int)character.character_Type[0]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_1_Reg,  
        (int)character.character_Type[1]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_2_Reg,  
        (int)character.character_Type[2]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_3_Reg,  
        (int)character.character_Type[3]);
    /*passing of other parameters*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Lane_Mask_Reg,  
        (int)character.lane_Mask);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Violation_Lane_Mask_Reg, 
        (int)violation_Lane_Mask);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Flag_Reg, 1);
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Character_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Serial_Character_Received
 *
 * Returns: result of Rio_TxRxM_Serial_Character_Received routine work
 *
 * Notes: 
 *
 **************************************************************************/
int SW_Serial_Rio_TxRxM_Serial_Character_Received (
    RIO_CONTEXT_T context,
    RIO_PCS_PMA_CHARACTER_T character )
{
    int instId;
    instId = (int)context;
    /*pass array of the characters data*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_0_Reg,  
        (int)character.character_Data[0]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_1_Reg,  
        (int)character.character_Data[1]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_2_Reg,  
        (int)character.character_Data[2]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_3_Reg,  
        (int)character.character_Data[3]);
    /*passing character type*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_0_Reg,  
        (int)character.character_Type[0]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_1_Reg,  
        (int)character.character_Type[1]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_2_Reg,  
        (int)character.character_Type[2]);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_3_Reg,  
        (int)character.character_Type[3]);
    /*LANE mask processing*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Lane_Mask_Reg,  
        (int)character.lane_Mask);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Flag_Reg, 1);
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Set_Pins
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Set_Pins
 *
 * Returns: result of Rio_TxRxM_Set_Pins routine work
 *
 * Notes: 
 *
 **************************************************************************/
int SW_Serial_Rio_TxRxM_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_SIGNAL_T tlane0,
    RIO_SIGNAL_T tlane1,
    RIO_SIGNAL_T tlane2,
    RIO_SIGNAL_T tlane3 )
{
    int instId;
    instId = (int)context;
    if (ten_Bit_Interface_TxRxM == RIO_FALSE)
    {
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane0_Reg, (int)tlane0);
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane1_Reg, (int)tlane1);
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane2_Reg, (int)tlane2);
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane3_Reg, (int)tlane3);
        /*Last flag value updating*/
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Set_Pins_Flag_Reg, 1);
    }
        else
    {
        /* new regs for 10-bit interface */
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane0_10_Reg, (int)tlane0);
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane1_10_Reg, (int)tlane1);
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane2_10_Reg, (int)tlane2);
        SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane3_10_Reg, (int)tlane3);
        if (set_Serial_Pins_10bit_Flag_Reg_TxRxM == RIO_TRUE)
            SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Set_Pins_10bit_Flag_Reg, 1);
    }

    return RIO_OK;
}

/*****************************************************************************/
/*     End of callbacks implementation                                       */
/*****************************************************************************/




/***************************************************************************
 * Function : SW_Serial_RIO_TxRxM_Serial_Model_Create_Instance
 *
 * Description: Software part of PLI function pair to provide
 *              Instance creation and binding
 *
 * Returns: Identificator of instance if All right or -1 if error
 *
 * Notes: The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Serial_RIO_TxRxM_Serial_Model_Create_Instance ()
{
    int k;
    int instId;
    int res;
/*Input parameters*/
    RIO_TXRXM_SERIAL_INST_PARAM_T param;
/* End of parameters defintion*/
    /*check that wrapper is initialized*/
    if (rio_TxRx_Wrap_Initialized != RIO_TRUE)
    {
        SW_TxRx_Wrapper_Message(WRAP_ERROR_RESULT, "Error", "Create_Instnce - S_TxRx Wrapper isn't initialized");
        tf_putp(0, WRAP_ERROR_RESULT);
        return;
    }

    k = 0;
/*Instance ID calculating*/
    instId = TxRx_Inst_Num++;
/*  If no more memory for instances available then raise an error */
    if (TxRx_Inst_Num > TxRx_Max_Inst_Num)
    {
        SW_TxRx_Wrapper_Message(WRAP_ERROR_RESULT, "Error","Too many instances created");
        tf_putp( 0, WRAP_ERROR_RESULT );
        return;
    }

/*Parameters accessing*/
    param.packets_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 1 ); 
    param.symbols_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 2 ); 
    param.character_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 3 ); 
    param.character_Column_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 4 ); 
    param.code_Group_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 5 ); 
    param.code_Group_Column_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 6 ); 
    param.single_Bit_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 7 ); 
    param.single_Character_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 8 ); 
    param.comp_Seq_Request_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 9 ); 
    param.management_Request_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 10 ); 
    param.pop_Char_Buffer_Size = (int)  acc_fetch_tfarg_int( k + 11 );
    param.pop_Bit_Buffer_Size = (int)  acc_fetch_tfarg_int( k + 12 );
    k += 2;
/*End of the fields value getting for the param structure*/
/*Get registers handles*/
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Packet_Type_Reg = acc_handle_tfarg( k + 11);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Target_TID = acc_handle_tfarg( k + 12);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Src_ID = acc_handle_tfarg( k + 13);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Prio = acc_handle_tfarg( k + 14);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Wdptr = acc_handle_tfarg( k + 15);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Rdsize = acc_handle_tfarg( k + 16);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Format_Type = acc_handle_tfarg( k + 17);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ttype = acc_handle_tfarg( k + 18);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Address = acc_handle_tfarg( k + 19);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ext_Address = acc_handle_tfarg( k + 20);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Xasmsbs = acc_handle_tfarg( k + 21);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Sec_ID = acc_handle_tfarg( k + 22);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Sec_TID = acc_handle_tfarg( k + 23);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Mbox = acc_handle_tfarg( k + 24);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Letter = acc_handle_tfarg( k + 25);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Msgseg = acc_handle_tfarg( k + 26);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ssize = acc_handle_tfarg( k + 27);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Msglen = acc_handle_tfarg( k + 28);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Doorbell_Info = acc_handle_tfarg( k + 29);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Offset = acc_handle_tfarg( k + 30);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Dw_Num_Reg = acc_handle_tfarg( k + 31);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Transp_Type_Reg = acc_handle_tfarg( k + 32);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ack_ID_Reg = acc_handle_tfarg( k + 33);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Transport_Info_Reg = acc_handle_tfarg( k + 34);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Hop_Count_Reg = acc_handle_tfarg( k + 35);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Flag_Reg = acc_handle_tfarg( k + 36);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Prio_Reg = acc_handle_tfarg( k + 37);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Ftype_Reg = acc_handle_tfarg( k + 38);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Transaction_Reg = acc_handle_tfarg( k + 39);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Status_Reg = acc_handle_tfarg( k + 40);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Tr_Info_Reg = acc_handle_tfarg( k + 41);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Src_ID_Reg = acc_handle_tfarg( k + 42);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Target_TID_Reg = acc_handle_tfarg( k + 43);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Sec_ID_Reg = acc_handle_tfarg( k + 44);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Sec_TID_Reg = acc_handle_tfarg( k + 45);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Dw_Num_Reg = acc_handle_tfarg( k + 46);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Ack_ID_Reg = acc_handle_tfarg( k + 47);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Hop_Count_Reg = acc_handle_tfarg( k + 48);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Flag_Reg = acc_handle_tfarg( k + 49);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Packet_Size_Reg = acc_handle_tfarg( k + 50);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Violation_Type_Reg = acc_handle_tfarg( k + 51);

    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Expected_Crc_Reg = acc_handle_tfarg( k + 52);
    k++;

    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Flag_Reg = acc_handle_tfarg( k + 52);
    Contexts_TxRx[instId].regs.rio_TxRxM_Canceled_Packet_Received_Packet_Size_Reg = acc_handle_tfarg( k + 53);
    Contexts_TxRx[instId].regs.rio_TxRxM_Canceled_Packet_Received_Flag_Reg = acc_handle_tfarg( k + 54);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Stype0_Reg = acc_handle_tfarg( k + 55);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Parameter0_Reg = acc_handle_tfarg( k + 56);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Parameter1_Reg = acc_handle_tfarg( k + 57);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Stype1_Reg = acc_handle_tfarg( k + 58);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Cmd_Reg = acc_handle_tfarg( k + 59);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Crc_Reg = acc_handle_tfarg( k + 60);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Is_Crc_Valid_Reg = acc_handle_tfarg( k + 61);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Place_Reg = acc_handle_tfarg( k + 62);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Granule_Num_Reg = acc_handle_tfarg( k + 63);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_First_Char_Reg = acc_handle_tfarg( k + 64);
    k++;
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Symbol_Received_Flag_Reg = acc_handle_tfarg( k + 64);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Symbol_Buffer_Reg = acc_handle_tfarg( k + 65);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Place_Reg = acc_handle_tfarg( k + 66);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Granule_Num_Reg = acc_handle_tfarg( k + 67);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Violation_Type_Reg = acc_handle_tfarg( k + 68);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Symbol_Received_Flag_Reg = acc_handle_tfarg( k + 69);

    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_0_Reg = acc_handle_tfarg( k + 70);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_1_Reg = acc_handle_tfarg( k + 71);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_2_Reg = acc_handle_tfarg( k + 72);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_CG_Data_3_Reg = acc_handle_tfarg( k + 73);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_Is_RD_Positive_Reg = acc_handle_tfarg( k + 74);
    k+=5;
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_Lane_Mask_Reg = acc_handle_tfarg( k + 70);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Code_Group_Received_Flag_Reg = acc_handle_tfarg( k + 71);

    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_0_Reg = acc_handle_tfarg( k + 72);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_1_Reg = acc_handle_tfarg( k + 73);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_2_Reg = acc_handle_tfarg( k + 74);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Data_3_Reg = acc_handle_tfarg( k + 75);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_is_RD_Positive_Reg = acc_handle_tfarg( k + 76);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_0_Reg = acc_handle_tfarg( k + 77);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_1_Reg = acc_handle_tfarg( k + 78);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_2_Reg = acc_handle_tfarg( k + 79);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Data_3_Reg = acc_handle_tfarg( k + 80);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_0_Reg = acc_handle_tfarg( k + 81);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_1_Reg = acc_handle_tfarg( k + 82);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_2_Reg = acc_handle_tfarg( k + 83);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Char_Type_3_Reg = acc_handle_tfarg( k + 84);
    k+=13;
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_CG_Lane_Mask_Reg = acc_handle_tfarg( k + 72);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Lane_Mask_Reg = acc_handle_tfarg( k + 73);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Violation_Lane_Mask_Reg = acc_handle_tfarg( k + 74);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Violent_Character_Received_Flag_Reg = acc_handle_tfarg( k + 75);


    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_0_Reg = acc_handle_tfarg( k + 76);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_1_Reg = acc_handle_tfarg( k + 77);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_2_Reg = acc_handle_tfarg( k + 78);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Data_3_Reg = acc_handle_tfarg( k + 79);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_0_Reg = acc_handle_tfarg( k + 80);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_1_Reg = acc_handle_tfarg( k + 81);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_2_Reg = acc_handle_tfarg( k + 82);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Char_Type_3_Reg = acc_handle_tfarg( k + 83);
    k+=8;
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Lane_Mask_Reg = acc_handle_tfarg( k + 76);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Character_Received_Flag_Reg = acc_handle_tfarg( k + 77);

    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane0_Reg = acc_handle_tfarg( k + 78);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane1_Reg = acc_handle_tfarg( k + 79);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane2_Reg = acc_handle_tfarg( k + 80);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane3_Reg = acc_handle_tfarg( k + 81);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Set_Pins_Flag_Reg = acc_handle_tfarg( k + 82);
    
    Contexts_TxRx[instId].regs.rio_TxRxM_Packet_To_Send_Packet_Size = acc_handle_tfarg( k + 83);
    Contexts_TxRx[instId].regs.rio_TxRxM_Packet_To_Send_Flag_Reg = acc_handle_tfarg( k + 84);
    
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane0_10_Reg = acc_handle_tfarg( k + 85);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane1_10_Reg = acc_handle_tfarg( k + 86);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane2_10_Reg = acc_handle_tfarg( k + 87);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tlane3_10_Reg = acc_handle_tfarg( k + 88);
    Contexts_TxRx[instId].regs.rio_TxRxM_Serial_Set_Pins_10bit_Flag_Reg = acc_handle_tfarg( k + 89);
    
    res = RIO_TxRxM_Serial_Model_Create_Instance(&(Contexts_TxRx[instId].handle),
        &param);
    if ( res != RIO_OK )
    {
        io_printf ("Error during instance creating \n");
        TxRx_Inst_Num--;
        tf_putp(0, WRAP_ERROR_RESULT );
        return;
    }



/*Instance binding*/

    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.txrxm_Interface_Context = (RIO_CONTEXT_T)instId;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.lp_Serial_Interface_Context = (RIO_CONTEXT_T)instId;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_Msg = (RIO_CONTEXT_T)instId;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.hook_Context = (RIO_CONTEXT_T)instId;
    
    res = RIO_TxRxM_Serial_Model_Bind_Instance(
        Contexts_TxRx[instId].handle,
        &Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray);
    if ( res != RIO_OK )
    {
        io_printf ("Error during instance binding \n");
        TxRx_Inst_Num--;
        tf_putp(0, WRAP_ERROR_RESULT );
        return;
    }
/*End of instance binding*/
    Contexts_TxRx[instId].is_Serial = RIO_TRUE;

    tf_putp(0, instId);
}

/***************************************************************************
 * Function : Initialize_CB_Tray_S_TxRx
 *
 * Description: Function for callback trays initialization
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void Initialize_CB_Tray_S_TxRx(void)
{
/*Initialize callbacks for tray 0*/
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Request_Received = SW_Rio_TxRxM_Request_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Response_Received = SW_Rio_TxRxM_Response_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Violent_Packet_Received =  
        SW_Rio_TxRxM_Violent_Packet_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Canceled_Packet_Received =  
        SW_Rio_TxRxM_Canceled_Packet_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Serial_Symbol_Received =  
        SW_Serial_Rio_TxRxM_Serial_Symbol_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Serial_Violent_Symbol_Received =  
        SW_Serial_Rio_TxRxM_Serial_Violent_Symbol_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Serial_Code_Group_Received =  
        SW_Serial_Rio_TxRxM_Serial_Code_Group_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Serial_Violent_Character_Received =  
        SW_Serial_Rio_TxRxM_Serial_Violent_Character_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Serial_Character_Received =  
        SW_Serial_Rio_TxRxM_Serial_Character_Received;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Set_Pins = SW_Serial_Rio_TxRxM_Set_Pins;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_User_Msg = SW_TxRx_Rio_User_Msg;
    
/*hook callbacks*/
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Packet_To_Send = SW_Rio_TxRxM_Packet_To_Send;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Granule_To_Send = NULL;
    Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray.rio_TxRxM_Last_Packet_Granule = NULL;

}
