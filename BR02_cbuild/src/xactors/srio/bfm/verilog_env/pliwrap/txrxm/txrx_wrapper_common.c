/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\txrxm\txrx_wrapper_common.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:     
*
******************************************************************************/
#include <stdlib.h>
#include <string.h>

#ifndef TXRX_WRAP_SERIAL_ONLY
#include "wrapper_txrx.h"
#include "prototypes.h" 
#include "wrapvars.h"
#endif /*TXRX_WRAP_SERIAL_ONLY*/

#ifndef TXRX_WRAP_PAR_ONLY
#include "s_txrx_wrapper.h"
#include "s_wrapvars.h"
#include "s_prototypes.h" 
#endif /*TXRX_WRAP_PAR_ONLY*/

/*Global variables*/
int rio_TxRx_Wrap_Initialized = RIO_FALSE;

/*Contexts for instances*/
RIO_CONTEXT_TXRX_T *Contexts_TxRx;

int TxRx_Max_Inst_Num;  /*Maximum instance number*/
int TxRx_Inst_Num;  /*Current allocated instances number*/
/*Internal input buffer for SW_Rio_TxRxM_Packet_Struct_Request routine's variable*/ 
RIO_DW_T Rio_TxRxM_Packet_Struct_Request_Data[PLI_MAX_ARRAY_SIZE];
/*Internal input buffer for SW_Rio_TxRxM_Packet_Stream_Request routine's variable*/ 
RIO_UCHAR_T Rio_TxRxM_Packet_Stream_Request_Packet_Buffer[PLI_MAX_STREAM_SIZE];


/* Names of array's indexes for model API routines*/
typedef enum {
    RIO_TXRXM_PACKET_STRUCT_REQUEST_DATA = 0,
    RIO_TXRXM_PACKET_STREAM_REQUEST_PACKET_BUFFER 
} PUT_IND_T;

/* Names of array's indexes for Callback*/
typedef enum {
    RIO_TXRXM_REQUEST_RECEIVED_PACKET_STRUCT_DATA = 0,
    RIO_TXRXM_RESPONSE_RECEIVED_PACKET_STRUCT_DATA,
    RIO_TXRXM_VIOLENT_PACKET_RECEIVED_PACKET_BUFFER,
    RIO_TXRXM_CANCELED_PACKET_RECEIVED_PACKET_BUFFER,
    RIO_TXRXM_PACKET_TO_SEND_BUFFER
} GET_IND_T;

/*end of variables*/


/***************************************************************************
 * Function : SW_TxRx_Wrapper_Message
 *
 * Description: Simple Error handle
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_TxRx_Wrapper_Message (int id, char *type, char *msg)
{
    io_printf ("%s TxRx wrapper, instance=%i: %s\n", type, id, msg);
}


/***************************************************************************
 * Function : SetIntegerRegValue
 *
 * Description: Sets register regName to specified integer value
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SetIntegerRegValue(handle hReg, int value)
{
    static s_acc_value val = {accIntVal};    /* value  */
    static s_setval_delay delay = {{0, 0, 0, 0.0}, accNoDelay}; /* delay - NO */

    /*check handle*/
    if (hReg == (handle)0)
    {
        io_printf("TxRx wrapper: Warning register's handle is NULL\n");
        return;
    }
    /* set register value */
    val.value.integer = value;

    if (acc_set_value(hReg, &val, &delay))
    {
        io_printf ("TxRx Wrapper: Error during access to the verilog register");
        tf_dofinish(); /* finish if error */
    }
}


/***************************************************************************
 * Function : GetIntegerRegValue
 *
 * Description: Gets integer value of register regName
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
int GetIntegerRegValue(handle hReg)
{
    static s_acc_value val = {accIntVal};       /* value  */

    /* set register value */
    acc_fetch_value(hReg, "%%", &val);

    return val.value.integer;
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Clock_Edge
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Clock_Edge routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Clock_Edge routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Clock_Edge ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id_S(instId, "Error in the Rio_TxRxM_Clock_Edge - wrapper isn't initialized");

    /*Check is instance with the current Inst_ID created of no*/
    if ((instId < 0) || (instId >= TxRx_Inst_Num))
        SW_TxRx_Wrapper_Message (instId, "Warning", "Clock_Edge is called for non-existing instance");
    else
    {
        if (Contexts_TxRx[instId].is_Serial == RIO_FALSE)
        {
#ifndef TXRX_WRAP_SERIAL_ONLY
            (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Clock_Edge))(Contexts_TxRx[instId].handle);
#endif /*TXRX_WRAP_SERIAL_ONLY*/
        }
        else
        {
#ifndef TXRX_WRAP_PAR_ONLY
            (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Clock_Edge))(Contexts_TxRx[instId].handle);
#endif /*TXRX_WRAP_PAR_ONLY*/
        }
    }
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Clock_Edge_New
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Clock_Edge routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Clock_Edge_New routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Clock_Edge_New ()
{
    int i_Count;
 
#ifndef TXRX_WRAP_PAR_ONLY
    set_Serial_Pins_10bit_Flag_Reg_TxRxM = RIO_FALSE;
    ten_Bit_Interface_TxRxM = RIO_TRUE;
        for (i_Count =1; i_Count <= 10; i_Count++)
        {
            SW_Rio_PL_Clock_Edge ();

        }
        set_Serial_Pins_10bit_Flag_Reg_TxRxM = RIO_TRUE; 
#endif /*TXRX_WRAP_PAR_ONLY*/

}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Packet_Struct_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Packet_Struct_Request routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Packet_Struct_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Packet_Struct_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_TXRXM_PACKET_REQ_STRUCT_T rq;
    RIO_UCHAR_T dw_Size;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Packet_Struct_Request - wrapper isn't initialized");

    rq.ack_ID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.rsrv_Phy_1 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.rsrv_Phy_2 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.tt = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.ftype = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.ttype = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 ); 
    rq.rdwr_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 10 ); 
    rq.srtr_TID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 11 ); 
    rq.hop_Count = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 12 ); 
    rq.config_Offset = ( long ) acc_fetch_tfarg_int( k + 13 ); 
    rq.wdptr = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 14 ); 
    rq.status = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 15 ); 
    rq.doorbell_Info = ( int ) acc_fetch_tfarg_int( k + 16 ); 
    rq.msglen = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 17 ); 
    rq.letter = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 18 ); 
    rq.mbox = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 19 ); 
    rq.msgseg = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 20 ); 
    rq.ssize = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 21 ); 
    rq.address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 22 ); 
    rq.extended_Address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 23 ); 
    rq.xamsbs = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 24 ); 
    rq.ext_Address_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 25 ); 
    rq.ext_Address_16 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 26 ); 
    rq.rsrv_Ftype6 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 27 ); 
    rq.rsrv_Ftype8 = ( long ) acc_fetch_tfarg_int( k + 28 ); 
    rq.rsrv_Ftype10 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 29 ); 
    rq.crc = ( unsigned int ) acc_fetch_tfarg_int( k + 30 ); 
    rq.is_Crc_Valid = (RIO_BOOL_T)acc_fetch_tfarg_int( k + 31 ); 
/*End of the fields value getting for the rq structure*/
    dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 32 ); 

    if (Contexts_TxRx[instId].is_Serial == RIO_FALSE)
    {
#ifndef TXRX_WRAP_SERIAL_ONLY
        tf_putp(0, (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Packet_Struct_Request))( Contexts_TxRx[instId].handle,
            &rq , dw_Size , Rio_TxRxM_Packet_Struct_Request_Data ));
#endif /*TXRX_WRAP_SERIAL_ONLY*/
    }
    else
    {
#ifndef TXRX_WRAP_PAR_ONLY
        tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Packet_Struct_Request))( Contexts_TxRx[instId].handle,
            &rq , dw_Size , Rio_TxRxM_Packet_Struct_Request_Data));
#endif /*TXRX_WRAP_PAR_ONLY*/
    }

}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Packet_Stream_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Packet_Stream_Request routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Packet_Stream_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Packet_Stream_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    unsigned int packet_Size;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id_Ret(instId, "Error in the Rio_TxRxM_Packet_Stream_Request - wrapper isn't initialized");

    packet_Size = ( unsigned int ) acc_fetch_tfarg_int( k + 2 ); 
    if (Contexts_TxRx[instId].is_Serial == RIO_FALSE)
    {
#ifndef TXRX_WRAP_SERIAL_ONLY
        tf_putp(0, (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Packet_Stream_Request))( Contexts_TxRx[instId].handle,
            Rio_TxRxM_Packet_Stream_Request_Packet_Buffer , packet_Size ));
#endif  /*TXRX_WRAP_SERIAL_ONLY*/
    }
    else 
    {
#ifndef TXRX_WRAP_PAR_ONLY
        tf_putp(0, (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Packet_Stream_Request))( Contexts_TxRx[instId].handle,
            Rio_TxRxM_Packet_Stream_Request_Packet_Buffer , packet_Size));
#endif  /*TXRX_WRAP_PAR_ONLY*/
    }

}

/***************************************************************************
 * Function : SW_Rio_TxRxM_Model_Start_Reset
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Model_Start_Reset routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Model_Start_Reset routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Model_Start_Reset ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id_S(instId, "Error in the Rio_TxRxM_Model_Start_Reset - wrapper isn't initialized");

    if (Contexts_TxRx[instId].is_Serial == RIO_FALSE)
    {
#ifndef TXRX_WRAP_SERIAL_ONLY
        (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Model_Start_Reset))( 
            Contexts_TxRx[instId].handle);
#endif /*TXRX_WRAP_SERIAL_ONLY*/
    }
    else
    {
#ifndef TXRX_WRAP_PAR_ONLY
        (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Model_Start_Reset))( 
            Contexts_TxRx[instId].handle);
#endif /*TXRX_WRAP_PAR_ONLY*/
    }
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Delete_Instance
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Delete_Instance routine in the RIO_TXRXM_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Delete_Instance routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Rio_TxRxM_Delete_Instance ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_S(instId, "Error in the Rio_TxRxM_Delete_Instance - wrapper isn't initialized");
    if (Contexts_TxRx[instId].is_Serial == RIO_FALSE)
    {
#ifndef TXRX_WRAP_SERIAL_ONLY
        (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Delete_Instance))(Contexts_TxRx[instId].handle);
#endif /*TXRX_WRAP_SERIAL_ONLY*/
    }
    else
    {
#ifndef TXRX_WRAP_PAR_ONLY
        (*(Rio_Txrxm_Serial_Model_Ftray_T_Tray.rio_TxRxM_Delete_Instance))(Contexts_TxRx[instId].handle);
#endif /*TXRX_WRAP_PAR_ONLY*/
    }
}


/*common callback implementation*/

/***************************************************************************
 * Function : SW_Rio_TxRxM_Request_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Request_Received
 *
 * Returns: result of Rio_TxRxM_Request_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Request_Received (
    RIO_CONTEXT_T context,
    RIO_REMOTE_REQUEST_T *packet_Struct,
    RIO_UCHAR_T ack_ID,
    RIO_TR_INFO_T transport_Info,
    RIO_UCHAR_T hop_Count )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Packet_Type_Reg, (int)packet_Struct->packet_Type);
/*header structure necessary*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Target_TID, (int)packet_Struct->header->target_TID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Src_ID, (int)packet_Struct->header->src_ID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Prio, (int)packet_Struct->header->prio);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Wdptr, (int)packet_Struct->header->wdptr);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Rdsize, (int)packet_Struct->header->rdsize);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Format_Type, 
        (int)packet_Struct->header->format_Type);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ttype, (int)packet_Struct->header->ttype);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Address, (int)packet_Struct->header->address);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ext_Address, 
        (int)packet_Struct->header->extended_Address);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Xasmsbs, (int)packet_Struct->header->xamsbs);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Sec_ID, (int)packet_Struct->header->sec_ID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Sec_TID, (int)packet_Struct->header->sec_TID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Mbox, (int)packet_Struct->header->mbox);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Letter, (int)packet_Struct->header->letter);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Msgseg, (int)packet_Struct->header->msgseg);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ssize, (int)packet_Struct->header->ssize);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Msglen, (int)packet_Struct->header->msglen);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Doorbell_Info, 
        (int)packet_Struct->header->doorbell_Info);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Offset, (int)packet_Struct->header->offset);
/*end of header structure*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Dw_Num_Reg, (int)packet_Struct->dw_Num);
    memcpy (Contexts_TxRx[instId].bufs.rio_TxRxM_Request_Received_Packet_Struct_Data, 
        packet_Struct->data, packet_Struct->dw_Num * sizeof(RIO_DW_T) );
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Transp_Type_Reg, (int)packet_Struct->transp_Type);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ack_ID_Reg, (int)ack_ID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Transport_Info_Reg, (int)transport_Info);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Hop_Count_Reg, (int)hop_Count);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Flag_Reg, 1);
    return 0;
}



/***************************************************************************
 * Function : SW_Rio_TxRxM_Response_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Response_Received
 *
 * Returns: result of Rio_TxRxM_Response_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Response_Received (
    RIO_CONTEXT_T context,
    RIO_RESPONSE_T *packet_Struct,
    RIO_UCHAR_T ack_ID,
    RIO_UCHAR_T hop_Count )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Prio_Reg, (int)packet_Struct->prio);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Ftype_Reg, (int)packet_Struct->ftype);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Transaction_Reg, (int)packet_Struct->transaction);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Status_Reg, (int)packet_Struct->status);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Tr_Info_Reg, (int)packet_Struct->tr_Info);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Src_ID_Reg, (int)packet_Struct->src_ID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Target_TID_Reg, (int)packet_Struct->target_TID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Sec_ID_Reg, (int)packet_Struct->sec_ID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Sec_TID_Reg, (int)packet_Struct->sec_TID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Dw_Num_Reg, (int)packet_Struct->dw_Num);
    memcpy (Contexts_TxRx[instId].bufs.rio_TxRxM_Response_Received_Packet_Struct_Data, 
        packet_Struct->data, packet_Struct->dw_Num * sizeof(RIO_DW_T) );
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Ack_ID_Reg, (int)ack_ID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Hop_Count_Reg, (int)hop_Count);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Violent_Packet_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Violent_Packet_Received
 *
 * Returns: result of Rio_TxRxM_Violent_Packet_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Violent_Packet_Received (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T * packet_Buffer,
    unsigned int packet_Size,
    RIO_PACKET_VIOLATION_T violation_Type, 
    unsigned short              expected_Crc)
{
    int instId;
    instId = (int)context;
    memcpy (Contexts_TxRx[instId].bufs.rio_TxRxM_Violent_Packet_Received_Packet_Buffer, 
        packet_Buffer, packet_Size * sizeof(RIO_UCHAR_T) );
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Packet_Size_Reg, (int)packet_Size);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Violation_Type_Reg, (int)violation_Type);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Expected_Crc_Reg, (int)expected_Crc);
    
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Flag_Reg, 1);
    
    return 0;
}

/***************************************************************************
 * Function : SW_Rio_TxRxM_Canceled_Packet_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Canceled_Packet_Received
 *
 * Returns: result of Rio_TxRxM_Canceled_Packet_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Canceled_Packet_Received (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T * packet_Buffer,
    unsigned int packet_Size )
{
    int instId;
    instId = (int)context;
    memcpy (Contexts_TxRx[instId].bufs.rio_TxRxM_Canceled_Packet_Received_Packet_Buffer, 
        packet_Buffer, packet_Size * sizeof(RIO_UCHAR_T) );
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Canceled_Packet_Received_Packet_Size_Reg, (int)packet_Size);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Canceled_Packet_Received_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Request_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Request_Received
 *
 * Returns: result of Rio_TxRxM_Request_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Packet_To_Send (
    RIO_CONTEXT_T               context, 
    RIO_UCHAR_T                 packet_Buffer[],
    unsigned int                packet_Size  /*packet size in byte count*/
    )
{
    int instId;
    instId = (int)context;
    
/*copy packet to the internal buffer*/
    memcpy (Contexts_TxRx[instId].bufs.rio_TxRxM_Packet_To_Send_Data, 
        packet_Buffer, packet_Size * sizeof(RIO_UCHAR_T) );

/*header structure necessary*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Packet_To_Send_Packet_Size, (int)packet_Size);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Packet_To_Send_Flag_Reg, 1); 
}

/*routines for access internal buffers*/
/***************************************************************************
 * Function : SW_Serial_Put_Data_S_TxRx
 *
 * Description: Routine for access to internal buffers
 *
 * Returns:  
 *
 * Notes: The routines is generated by WrapGen
 *
 **************************************************************************/
void SW_Put_Data_TxRx()
{
    PUT_IND_T type = (PUT_IND_T) acc_fetch_tfarg_int(1);    /*Type of the array*/
    int index = acc_fetch_tfarg_int(2);    /*Index in the array*/

    /* check that index is correct*/
    if ( (index < 0) || ((index > PLI_MAX_ARRAY_SIZE ) && (type == RIO_TXRXM_PACKET_STRUCT_REQUEST_DATA)) ||
        ((index > PLI_MAX_STREAM_SIZE ) && (type == RIO_TXRXM_PACKET_STREAM_REQUEST_PACKET_BUFFER)) )
    {
        SW_TxRx_Wrapper_Message(WRAP_ERROR_RESULT, "Error", "Put data: The index value is out of range");
        tf_dofinish();
        /*tf_putp(0, WRAP_ERROR_RESULT);*/
    }

    switch (type)
    {
        case RIO_TXRXM_PACKET_STRUCT_REQUEST_DATA:
            Rio_TxRxM_Packet_Struct_Request_Data[index].ms_Word = acc_fetch_tfarg_int(3);
            Rio_TxRxM_Packet_Struct_Request_Data[index].ls_Word = acc_fetch_tfarg_int(4);
            break;
        case RIO_TXRXM_PACKET_STREAM_REQUEST_PACKET_BUFFER:
            Rio_TxRxM_Packet_Stream_Request_Packet_Buffer[index] = (RIO_UCHAR_T)acc_fetch_tfarg_int(3);
            break;
        default:
            SW_TxRx_Wrapper_Message(WRAP_ERROR_RESULT, "Error", "The internal buffer with specified number is not exist");
    }
}




/***************************************************************************
 * Function : SW_Get_Data_TxRx
 *
 * Description: Routine for access to internal buffers
 *
 * Returns:
 *
 * Notes:
 *
 **************************************************************************/
void SW_Get_Data_TxRx()
{
    int instId = acc_fetch_tfarg_int(1);
    GET_IND_T type = (GET_IND_T) acc_fetch_tfarg_int(2);    /*Type of the array*/
    int index = acc_fetch_tfarg_int(3);    /*Index in the array*/
    WORD_POS_T pos = LSW;                  /*position of word in the doubleword*/


/*The follow strings checks that passed instance ID is correct and wrapper is initialized*/
    if ((instId <0) || (instId >= TxRx_Inst_Num))
    {
        SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", "Incorrect instId for Get_Data routine");
        tf_putp (0, WRAP_ERROR_RESULT);
        return;
    }

    if  ( ( ((index < 0) || (index >= PLI_MAX_ARRAY_SIZE)) && 
        (type != RIO_TXRXM_VIOLENT_PACKET_RECEIVED_PACKET_BUFFER) && 
        (type != RIO_TXRXM_CANCELED_PACKET_RECEIVED_PACKET_BUFFER)  ) ||
        ((index < 0) || (index >= PLI_MAX_STREAM_SIZE)) )
    {
        SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", "Get_Data - index incorrect");
        return;
    }

    if ((type == RIO_TXRXM_REQUEST_RECEIVED_PACKET_STRUCT_DATA) ||
        (type == RIO_TXRXM_RESPONSE_RECEIVED_PACKET_STRUCT_DATA))
            pos = (WORD_POS_T)acc_fetch_tfarg_int(4);

    switch (type)
    {
        case RIO_TXRXM_REQUEST_RECEIVED_PACKET_STRUCT_DATA:
            if (pos == LSW)
                tf_putp(0, Contexts_TxRx[instId].bufs.rio_TxRxM_Request_Received_Packet_Struct_Data[index].ls_Word);
            else
                tf_putp(0, Contexts_TxRx[instId].bufs.rio_TxRxM_Request_Received_Packet_Struct_Data[index].ms_Word);
            break;

        case RIO_TXRXM_RESPONSE_RECEIVED_PACKET_STRUCT_DATA:
            if (pos == LSW)
                tf_putp(0, Contexts_TxRx[instId].bufs.rio_TxRxM_Response_Received_Packet_Struct_Data[index].ls_Word);
            else
                tf_putp(0, Contexts_TxRx[instId].bufs.rio_TxRxM_Response_Received_Packet_Struct_Data[index].ms_Word);
            break;
        case RIO_TXRXM_VIOLENT_PACKET_RECEIVED_PACKET_BUFFER:
            tf_putp(0, Contexts_TxRx[instId].bufs.rio_TxRxM_Violent_Packet_Received_Packet_Buffer[index]);
            break;
        case RIO_TXRXM_CANCELED_PACKET_RECEIVED_PACKET_BUFFER:
            tf_putp(0, Contexts_TxRx[instId].bufs.rio_TxRxM_Canceled_Packet_Received_Packet_Buffer[index]);
            break;
        case RIO_TXRXM_PACKET_TO_SEND_BUFFER:
            tf_putp(0, Contexts_TxRx[instId].bufs.rio_TxRxM_Packet_To_Send_Data[index]);
            break;
        default:
            SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
                "The internal callback's buffer with specified number is not exist");
    }
}


/*the routines for the creation and deleting instance of wrapper*/


/***************************************************************************
 * Function : SW_TxRx_Init
 *
 * Description: Software part of PLI function pair to initialize
 *                PLI engine, shall be called when demo starts
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_TxRx_Init ()
{
    int num;

    /*Check that this is first call of initialization routine*/
    if (rio_TxRx_Wrap_Initialized == RIO_TRUE)
    {
        SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Warning", "TxRx Wrapper alredy initialized");    
        return;
    }
    else
    /*set flag for notifying that wrapper is initialized*/
        rio_TxRx_Wrap_Initialized = RIO_TRUE;

    acc_initialize();
#ifndef TXRX_WRAP_SERIAL_ONLY
    if(RIO_TxRxM_Parallel_Model_Get_Function_Tray( &Rio_Txrxm_Model_Ftray_T_Tray ) != RIO_OK )
    {
        SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", "RIO_TxRxM_Parallel_Model_Get_Function_Tray - not OK");
        tf_dofinish();
    }
#endif /*TXRX_WRAP_SERIAL_ONLY*/

#ifndef TXRX_WRAP_PAR_ONLY
    if(RIO_TxRxM_Serial_Model_Get_Function_Tray ( &Rio_Txrxm_Serial_Model_Ftray_T_Tray ) != RIO_OK )
    {
        SW_TxRx_Wrapper_Message(WRAP_ERROR_RESULT, "Error", "RIO_TxRxM_Serial_Model_Get_Function_Tray - not OK");
        tf_dofinish();
    }
#endif /*TXRX_WRAP_PAR_ONLY*/



/*reset initial values*/
    TxRx_Max_Inst_Num = 0;
    TxRx_Inst_Num = 0;


/*Allocation memory for contexts*/
    num = acc_fetch_tfarg_int(1);
    if (num < 0) SW_TxRx_Wrapper_Message (RIO_TXRX_ABSENT_INST_ID, "Error", "SW_TxRx_Init, Negative number of instances");
    TxRx_Max_Inst_Num = num;
    Contexts_TxRx = (RIO_CONTEXT_TXRX_T *) malloc( num * sizeof( RIO_CONTEXT_TXRX_T ) );
    if ( Contexts_TxRx == 0) SW_TxRx_Wrapper_Message (RIO_TXRX_ABSENT_INST_ID,
        "Error", "SW_TxRx_Init, Error during memory allocation");

/*Fills Callback tray*/
#ifndef TXRX_WRAP_SERIAL_ONLY
    Initialize_CB_Tray_TxRx();
#endif /*TXRX_WRAP_SERIAL_ONLY*/

#ifndef TXRX_WRAP_PAR_ONLY
    Initialize_CB_Tray_S_TxRx();
#endif /*TXRX_WRAP_PAR_ONLY*/
}


/***************************************************************************
 * Function : SW_TxRx_Close
 *
 * Description: Software part of PLI function pair to free internal
 *                PLI structures, shall be called when demo finishes
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_TxRx_Close ()
{

    if (rio_TxRx_Wrap_Initialized == RIO_FALSE)
    {
        SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Warning", "TxRx Wrapper alredy closed");    
        return;
    }
    acc_close();
    free ( Contexts_TxRx );
    /*Reset flag*/
    rio_TxRx_Wrap_Initialized = RIO_FALSE;
}
