#define MODELSIM_PLI_C
/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\txrxm\modelsim_pli.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  File to link pli wrapper to Mentor Grahics ModelSIM
*
* Notes:        This file is generated automaticaly by WrapGen
*
******************************************************************************/

#include "veriuser.h"
extern void SW_TxRx_Init();
extern void SW_TxRx_Close();
extern void SW_Rio_TxRxM_Clock_Edge();
extern void SW_Rio_TxRxM_Packet_Struct_Request();
extern void SW_Rio_TxRxM_Packet_Stream_Request();
extern void SW_Rio_TxRxM_Symbol_Request();
extern void SW_Rio_TxRxM_Training_Request();
extern void SW_Rio_TxRxM_Granule_Request();
extern void SW_Rio_TxRxM_Model_Start_Reset();
extern void SW_Rio_TxRxM_Model_Initialize();
extern void SW_Rio_TxRxM_Get_Pins();
extern void SW_RIO_TxRxM_Model_Create_Instance();
extern void SW_Put_Data_TxRx();
extern void SW_Get_Data_TxRx();
/*serial routines*/
extern void SW_Serial_Rio_TxRxM_Serial_Symbol_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Character_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Character_Column_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Code_Group_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Code_Group_Column_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Single_Character_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Single_Bit_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Comp_Seq_Request();
extern void SW_Serial_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management();
extern void SW_Serial_Rio_TxRxM_Serial_Pop_Bit();
extern void SW_Serial_Rio_TxRxM_Serial_Pop_Character();
extern void SW_Serial_Rio_TxRxM_Model_Initialize();
extern void SW_Serial_Rio_TxRxM_Get_Pins();
extern void SW_Serial_RIO_TxRxM_Serial_Model_Create_Instance();
/*end of serial routines*/
extern void SW_Rio_TxRxM_Clock_Edge_New();
extern void SW_Serial_Rio_TxRxM_Get_Pins_New();


/***************************************************************************
 * Function : RIO_Sizetf_32
 *
 * Description: Routine for size detection
 *
 * Returns: 32
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_32()
{
    return 32; /*size of 32 bit variable*/
}




s_tfcell veriusertfs[] = {
    {
        usertask,           /* tells whether Verilog task or function */
        0,                  /* data argument of callback function */
        0,                  /* checktf */
        0,  /* size of returned data if function */
        SW_TxRx_Init,            /* pointer to C function associated with Verilog task or function */
        0,                  /* misctf */
        "$HW_TxRx_Init"          /* verilog name */
    },
{usertask, 0, 0, 0, SW_TxRx_Close, 0, "$HW_TxRx_Close"},
{usertask, 0, 0, 0, SW_Rio_TxRxM_Clock_Edge, 0, "$HW_Rio_TxRxM_Clock_Edge"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_TxRxM_Packet_Struct_Request, 0, "$HW_Rio_TxRxM_Packet_Struct_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_TxRxM_Packet_Stream_Request, 0, "$HW_Rio_TxRxM_Packet_Stream_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_TxRxM_Symbol_Request, 0, "$HW_Rio_TxRxM_Symbol_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_TxRxM_Training_Request, 0, "$HW_Rio_TxRxM_Training_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_TxRxM_Granule_Request, 0, "$HW_Rio_TxRxM_Granule_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_TxRxM_Model_Start_Reset, 0, "$HW_Rio_TxRxM_Model_Start_Reset"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_TxRxM_Model_Initialize, 0, "$HW_Rio_TxRxM_Model_Initialize"},
{usertask, 0, 0, 0, SW_Rio_TxRxM_Get_Pins, 0, "$HW_Rio_TxRxM_Get_Pins"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_RIO_TxRxM_Model_Create_Instance, 0, "$HW_RIO_TxRxM_Model_Create_Instance"},
{usertask, 0, 0, 0, SW_Put_Data_TxRx, 0, "$HW_Put_Data_TxRx"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Get_Data_TxRx, 0, "$HW_Get_Data_TxRx"},

/*serial routines*/
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Symbol_Request, 0, "$HW_Rio_TxRxM_Serial_Symbol_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Character_Request, 0, "$HW_Rio_TxRxM_Serial_Character_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Character_Column_Request, 0, 
    "$HW_Rio_TxRxM_Serial_Character_Column_Request"
},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Code_Group_Request, 0, "$HW_Rio_TxRxM_Serial_Code_Group_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Code_Group_Column_Request, 0, 
    "$HW_Rio_TxRxM_Serial_Code_Group_Column_Request"
},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Single_Character_Request, 0, 
    "$HW_Rio_TxRxM_Serial_Single_Character_Request"
},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Single_Bit_Request, 0, 
    "$HW_Rio_TxRxM_Serial_Single_Bit_Request"
},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Comp_Seq_Request, 0, "$HW_Rio_TxRxM_Serial_Comp_Seq_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management, 0, 
    "$HW_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management"
},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Pop_Bit, 0, "$HW_Rio_TxRxM_Serial_Pop_Bit"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Serial_Pop_Character, 0, "$HW_Rio_TxRxM_Serial_Pop_Character"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_Rio_TxRxM_Model_Initialize, 0, "$HW_Serial_Rio_TxRxM_Model_Initialize"},
{usertask, 0, 0, 0, SW_Serial_Rio_TxRxM_Get_Pins, 0, "$HW_Serial_Rio_TxRxM_Get_Pins"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Serial_RIO_TxRxM_Serial_Model_Create_Instance, 0, 
    "$HW_RIO_TxRxM_Serial_Model_Create_Instance"
},
{usertask, 0, 0, 0, SW_Rio_TxRxM_Clock_Edge_New, 0, "$HW_Rio_TxRxM_Clock_Edge_New"},
{usertask, 0, 0, 0, SW_Serial_Rio_TxRxM_Get_Pins_New, 0, "$HW_Serial_Rio_TxRxM_Get_Pins_New"},
    {0} /* last entry must be 0 */
};
