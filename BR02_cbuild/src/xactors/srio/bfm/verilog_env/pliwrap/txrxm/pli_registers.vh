/******************************************************************************
*
*       COPYRIGHT 2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\txrxm\pli_registers.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*		to display revision history information.
*               
* Description: PLI wrapper registers
*
* Notes: 
*
******************************************************************************/


/* registers driven by PLI*/
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_packet_type_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_target_tid;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_src_id;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_prio;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_wdptr;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_rdsize;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_format_type;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_ttype;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_ext_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_xamsbs;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_sec_id;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_sec_tid;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_mbox;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_letter;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_msgseg;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_ssize;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_msglen;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_doorbell_info;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_offset;

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_dw_num_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_transp_type_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_ack_id_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_transport_info_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_request_received_hop_count_reg; 
    reg rio_txrxm_request_received_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_prio_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_ftype_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_transaction_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_status_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_tr_info_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_src_id_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_target_tid_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_sec_id_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_sec_tid_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_dw_num_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_ack_id_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_response_received_hop_count_reg; 
    reg rio_txrxm_response_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_violent_packet_received_packet_size_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_violent_packet_received_violation_type_reg; 
	reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_violent_packet_received_expected_crc_reg;
    reg rio_txrxm_violent_packet_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_canceled_packet_received_packet_size_reg; 
    reg rio_txrxm_canceled_packet_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_s_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_ackid_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_rsrv_1_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_s_parity_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_rsrv_2_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_buf_status_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_stype_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_place_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_symbol_received_granule_num_reg; 
    reg rio_txrxm_symbol_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_training_received_place_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_training_received_granule_num_reg; 
    reg rio_txrxm_training_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_violent_symbol_received_symbol_buffer_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_violent_symbol_received_place_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_violent_symbol_received_granule_num_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_violent_symbol_received_violation_type_reg; 
    reg rio_txrxm_violent_symbol_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_granule_received_granule_type_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_granule_received_granule_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_granule_received_has_frame_toggled_reg; 
    reg rio_txrxm_granule_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_set_pins_tframe_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_set_pins_td_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_set_pins_tdl_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_set_pins_tclk_reg; 
    reg rio_txrxm_set_pins_flag_reg; 


//Serial registers

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_stype0_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_parameter0_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_parameter1_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_stype1_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_cmd_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_crc_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_is_crc_valid_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_place_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_granule_num_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_symbol_received_first_char_reg; 

    reg rio_txrxm_serial_symbol_received_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_violent_symbol_received_symbol_buffer_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_violent_symbol_received_place_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_violent_symbol_received_granule_num_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_violent_symbol_received_violation_type_reg; 
    reg rio_txrxm_serial_violent_symbol_received_flag_reg; 

    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_code_group_received_cg_data_0_reg;
    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_code_group_received_cg_data_1_reg;
    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_code_group_received_cg_data_2_reg;
    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_code_group_received_cg_data_3_reg;
    reg [ 0 : `RIO_PL_SERIAL_NUMBER_OF_LANES - 1 ]rio_txrxm_serial_code_group_received_is_rd_positive_reg;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_code_group_received_lane_mask_reg; 
    reg rio_txrxm_serial_code_group_received_flag_reg; 

    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_violent_character_received_cg_data_0_reg;
    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_violent_character_received_cg_data_1_reg;
    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_violent_character_received_cg_data_2_reg;
    reg [ 0 : `RIO_BITS_IN_CODEGROUP - 1 ]rio_txrxm_serial_violent_character_received_cg_data_3_reg;
    reg [ 0 : `RIO_PL_SERIAL_NUMBER_OF_LANES - 1 ]rio_txrxm_serial_violent_character_received_is_rd_positive_reg;
    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_violent_character_received_char_data_0_reg;
    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_violent_character_received_char_data_1_reg;
    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_violent_character_received_char_data_2_reg;
    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_violent_character_received_char_data_3_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_violent_character_received_char_type_0_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_violent_character_received_char_type_1_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_violent_character_received_char_type_2_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_violent_character_received_char_type_3_reg;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_violent_character_received_cg_lane_mask_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_violent_character_received_lane_mask_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_violent_character_received_violation_lane_mask_reg; 
    reg rio_txrxm_serial_violent_character_received_flag_reg; 

    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_character_received_char_data_0_reg;
    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_character_received_char_data_1_reg;
    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_character_received_char_data_2_reg;
    reg [ 0 : `RIO_BITS_IN_BYTE - 1 ]rio_txrxm_serial_character_received_char_data_3_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_character_received_char_type_0_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_character_received_char_type_1_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_character_received_char_type_2_reg;
    reg [ 0 : `PLI_SMALLL_REGISTER_SIZE - 1 ]rio_txrxm_serial_character_received_char_type_3_reg;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_serial_character_received_lane_mask_reg; 
    reg rio_txrxm_serial_character_received_flag_reg; 

    reg rio_txrxm_set_pins_tlane0_reg; 
    reg rio_txrxm_set_pins_tlane1_reg; 
    reg rio_txrxm_set_pins_tlane2_reg; 
    reg rio_txrxm_set_pins_tlane3_reg; 
    reg rio_txrxm_serial_set_pins_flag_reg; 
//hook registers
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_txrxm_packet_to_send_packet_size;
    reg rio_txrxm_packet_to_send_flag_reg;

 //Serial pin 10-bit interface
    reg [0 : 9] rio_txrxm_set_pins_tlane0_10_reg; 
    reg [0 : 9] rio_txrxm_set_pins_tlane1_10_reg; 
    reg [0 : 9] rio_txrxm_set_pins_tlane2_10_reg; 
    reg [0 : 9] rio_txrxm_set_pins_tlane3_10_reg; 
    reg [0 : 9] rio_txrxm_set_serial_pins_10bit_flag_reg; 
//end of serial pin 10-bit interface
