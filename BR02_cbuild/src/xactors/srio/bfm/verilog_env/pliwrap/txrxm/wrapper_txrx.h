/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/verilog_env/pliwrap/txrxm/wrapper_txrx.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Description: This file contain prototypes c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:       Prototype of this file is generated automaticaly by WrapGen
*
******************************************************************************/


#ifndef _TXRXWRAPPER_H
#define _TXRXWRAPPER_H

#include "rio_txrxm_parallel_model.h"
#include "txrx_wrapper_common.h"



/*macro which checks that inst id is ok*/
#define Wrap_Check_Id(id)\
{\
    if ( ((id) < 0 ) || ((id) >= TxRx_Inst_Num) )\
    {\
        SW_TxRx_Wrapper_Message((id), "Error", "incorrect instance id value");\
        return;\
    }\
    if (rio_TxRx_Wrap_Initialized == RIO_FALSE)\
    {\
        SW_TxRx_Wrapper_Message((id), "Error", "TxRx Wrapper isn't initialized");\
        return;\
    }\
}

#endif /*_TXRXWRAPPER_H*/




