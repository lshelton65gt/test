#ifndef TXRX_WRAPPER_COMMON_H
#define TXRX_WRAPPER_COMMON_H
/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\txrxm\txrx_wrapper_common.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Description: Constants and types for serial and parallel txrx wrapper
*
* Notes:       
*
******************************************************************************/

#ifndef _NC_VLOG_ 
    #include "acc_user.h"
    #ifdef _MODELSIM_
    #include "veriuser.h"
    #else /*_MODELSIM_ is undefined*/
    #include "vcsuser.h"
    #endif /*_MODELSIM_*/
#else /*_NC_VLOG_ is defined*/
    #include "acc_user.h"
    #include "veriuser.h"
    #include "vxl_veriuser.h"
#endif /*_NC_VLOG_*/

#include "rio_types.h"

#define RIO_PLI_SHORT_REGISTER_SIZE                 8
#define RIO_PLI_LONG_REGISTER_SIZE                  32
#define RIO_MAX_DATA_SIZE                           64

#define RIO_MAX_DW_SIZE                             32

#define RIO_MAX_NUM_OF_PE                           256
#define RIO_MAX_NUM_OF_SWITCHES                     16

/* Data size convertion constants */

#define RIO_BITS_IN_BYTE                            8
#define RIO_BITS_IN_WORD                            32
#define RIO_BITS_IN_DW                              64
#define RIO_BYTES_IN_WORD                           4
#define RIO_BYTES_IN_DW                             8
#define RIO_WORDS_IN_DW                             2

#define PLI_MAX_ARRAY_SIZE                          350
#define PLI_MAX_STREAM_SIZE                         (PLI_MAX_ARRAY_SIZE * RIO_BYTES_IN_DW)

/*value error result in the PLI WRAPPER*/
#define WRAP_ERROR_RESULT     -1 

/*Constant for error instance id*/
#define RIO_TXRX_ABSENT_INST_ID                     -1

 /*Values for write register routine*/
typedef enum
{
    REG_READ = 0,
    REG_WRITE
} REG_ACCESS_T;

/*Values for access to internal buffers*/
typedef enum 
{
    LSW = 0,    /*Least significent word*/
    MSW         /*Most significent word*/
} WORD_POS_T;

/* Types for instance identification*/

typedef struct
{
    handle rio_TxRxM_Request_Received_Packet_Type_Reg;

    handle rio_TxRxM_Request_Received_Target_TID;
    handle rio_TxRxM_Request_Received_Src_ID;
    handle rio_TxRxM_Request_Received_Prio;
    handle rio_TxRxM_Request_Received_Wdptr;
    handle rio_TxRxM_Request_Received_Rdsize;
    handle rio_TxRxM_Request_Received_Format_Type;
    handle rio_TxRxM_Request_Received_Ttype;
    handle rio_TxRxM_Request_Received_Address;
    handle rio_TxRxM_Request_Received_Ext_Address;
    handle rio_TxRxM_Request_Received_Xasmsbs;
    handle rio_TxRxM_Request_Received_Sec_ID;
    handle rio_TxRxM_Request_Received_Sec_TID;
    handle rio_TxRxM_Request_Received_Mbox;
    handle rio_TxRxM_Request_Received_Letter;
    handle rio_TxRxM_Request_Received_Msgseg;
    handle rio_TxRxM_Request_Received_Ssize;
    handle rio_TxRxM_Request_Received_Msglen;
    handle rio_TxRxM_Request_Received_Doorbell_Info;
    handle rio_TxRxM_Request_Received_Offset;

    handle rio_TxRxM_Request_Received_Dw_Num_Reg;
    handle rio_TxRxM_Request_Received_Transp_Type_Reg;
    handle rio_TxRxM_Request_Received_Ack_ID_Reg;
    handle rio_TxRxM_Request_Received_Transport_Info_Reg;
    handle rio_TxRxM_Request_Received_Hop_Count_Reg;
    handle rio_TxRxM_Request_Received_Flag_Reg;



    handle rio_TxRxM_Response_Received_Prio_Reg;
    handle rio_TxRxM_Response_Received_Ftype_Reg;
    handle rio_TxRxM_Response_Received_Transaction_Reg;
    handle rio_TxRxM_Response_Received_Status_Reg;
    handle rio_TxRxM_Response_Received_Tr_Info_Reg;
    handle rio_TxRxM_Response_Received_Src_ID_Reg;
    handle rio_TxRxM_Response_Received_Target_TID_Reg;
    handle rio_TxRxM_Response_Received_Sec_ID_Reg;
    handle rio_TxRxM_Response_Received_Sec_TID_Reg;
    handle rio_TxRxM_Response_Received_Dw_Num_Reg;
    handle rio_TxRxM_Response_Received_Ack_ID_Reg;
    handle rio_TxRxM_Response_Received_Hop_Count_Reg;
    handle rio_TxRxM_Response_Received_Flag_Reg;

    handle rio_TxRxM_Violent_Packet_Received_Packet_Size_Reg;
    handle rio_TxRxM_Violent_Packet_Received_Violation_Type_Reg;
    handle rio_TxRxM_Violent_Packet_Received_Expected_Crc_Reg;
    handle rio_TxRxM_Violent_Packet_Received_Flag_Reg;

    handle rio_TxRxM_Canceled_Packet_Received_Packet_Size_Reg;
    handle rio_TxRxM_Canceled_Packet_Received_Flag_Reg;

    handle rio_TxRxM_Symbol_Received_S_Reg;
    handle rio_TxRxM_Symbol_Received_AckID_Reg;
    handle rio_TxRxM_Symbol_Received_Rsrv_1_Reg;
    handle rio_TxRxM_Symbol_Received_S_Parity_Reg;
    handle rio_TxRxM_Symbol_Received_Rsrv_2_Reg;
    handle rio_TxRxM_Symbol_Received_Buf_Status_Reg;
    handle rio_TxRxM_Symbol_Received_Stype_Reg;
    handle rio_TxRxM_Symbol_Received_Place_Reg;
    handle rio_TxRxM_Symbol_Received_Granule_Num_Reg;
    handle rio_TxRxM_Symbol_Received_Flag_Reg;

    handle rio_TxRxM_Training_Received_Place_Reg;
    handle rio_TxRxM_Training_Received_Granule_Num_Reg;
    handle rio_TxRxM_Training_Received_Flag_Reg;

    handle rio_TxRxM_Violent_Symbol_Received_Symbol_Buffer_Reg;
    handle rio_TxRxM_Violent_Symbol_Received_Place_Reg;
    handle rio_TxRxM_Violent_Symbol_Received_Granule_Num_Reg;
    handle rio_TxRxM_Violent_Symbol_Received_Violation_Type_Reg;
    handle rio_TxRxM_Violent_Symbol_Received_Flag_Reg;

    handle rio_TxRxM_Granule_Received_Granule_Type_Reg;
    handle rio_TxRxM_Granule_Received_Granule_Reg;
    handle rio_TxRxM_Granule_Received_Has_Frame_Toggled_Reg;
    handle rio_TxRxM_Granule_Received_Flag_Reg;

    handle rio_TxRxM_Set_Pins_Tframe_Reg;
    handle rio_TxRxM_Set_Pins_Td_Reg;
    handle rio_TxRxM_Set_Pins_Tdl_Reg;
    handle rio_TxRxM_Set_Pins_Tclk_Reg;
    handle rio_TxRxM_Set_Pins_Flag_Reg;

/*serial registers*/
    handle rio_TxRxM_Serial_Symbol_Received_Stype0_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Parameter0_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Parameter1_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Stype1_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Cmd_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Crc_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Is_Crc_Valid_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Place_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Granule_Num_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_First_Char_Reg;
    handle rio_TxRxM_Serial_Symbol_Received_Flag_Reg;
    handle rio_TxRxM_Serial_Violent_Symbol_Received_Symbol_Buffer_Reg;
    handle rio_TxRxM_Serial_Violent_Symbol_Received_Place_Reg;
    handle rio_TxRxM_Serial_Violent_Symbol_Received_Granule_Num_Reg;
    handle rio_TxRxM_Serial_Violent_Symbol_Received_Violation_Type_Reg;
    handle rio_TxRxM_Serial_Violent_Symbol_Received_Flag_Reg;
/*serial CG received callback registers*/
    handle rio_TxRxM_Serial_Code_Group_Received_CG_Data_0_Reg;
    handle rio_TxRxM_Serial_Code_Group_Received_CG_Data_1_Reg;
    handle rio_TxRxM_Serial_Code_Group_Received_CG_Data_2_Reg;
    handle rio_TxRxM_Serial_Code_Group_Received_CG_Data_3_Reg;
    handle rio_TxRxM_Serial_Code_Group_Received_Is_RD_Positive_Reg;

    handle rio_TxRxM_Serial_Code_Group_Received_Lane_Mask_Reg;
    handle rio_TxRxM_Serial_Code_Group_Received_Flag_Reg;
/*violent character received callback registers*/    
    handle rio_TxRxM_Serial_Violent_Character_Received_CG_Data_0_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_CG_Data_1_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_CG_Data_2_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_CG_Data_3_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_is_RD_Positive_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Data_0_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Data_1_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Data_2_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Data_3_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Type_0_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Type_1_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Type_2_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Char_Type_3_Reg;

    handle rio_TxRxM_Serial_Violent_Character_Received_CG_Lane_Mask_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Lane_Mask_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Violation_Lane_Mask_Reg;
    handle rio_TxRxM_Serial_Violent_Character_Received_Flag_Reg;
/*serial character received callback registers*/
    handle rio_TxRxM_Serial_Character_Received_Char_Data_0_Reg;
    handle rio_TxRxM_Serial_Character_Received_Char_Data_1_Reg;
    handle rio_TxRxM_Serial_Character_Received_Char_Data_2_Reg;
    handle rio_TxRxM_Serial_Character_Received_Char_Data_3_Reg;
    handle rio_TxRxM_Serial_Character_Received_Char_Type_0_Reg;
    handle rio_TxRxM_Serial_Character_Received_Char_Type_1_Reg;
    handle rio_TxRxM_Serial_Character_Received_Char_Type_2_Reg;
    handle rio_TxRxM_Serial_Character_Received_Char_Type_3_Reg;

    handle rio_TxRxM_Serial_Character_Received_Lane_Mask_Reg;
    handle rio_TxRxM_Serial_Character_Received_Flag_Reg;
/*set pins registers*/
    handle rio_TxRxM_Set_Pins_Tlane0_Reg;
    handle rio_TxRxM_Set_Pins_Tlane1_Reg;
    handle rio_TxRxM_Set_Pins_Tlane2_Reg;
    handle rio_TxRxM_Set_Pins_Tlane3_Reg;
    handle rio_TxRxM_Serial_Set_Pins_Flag_Reg;

    /*set pins registers for 10bit interface*/
    handle rio_TxRxM_Set_Pins_Tlane0_10_Reg;
    handle rio_TxRxM_Set_Pins_Tlane1_10_Reg;
    handle rio_TxRxM_Set_Pins_Tlane2_10_Reg;
    handle rio_TxRxM_Set_Pins_Tlane3_10_Reg;
    handle rio_TxRxM_Serial_Set_Pins_10bit_Flag_Reg;

/*hook registers*/
    handle rio_TxRxM_Packet_To_Send_Packet_Size;
    handle rio_TxRxM_Packet_To_Send_Flag_Reg;

} REGISTERS_TXRX_T;

typedef struct
{
    RIO_DW_T rio_TxRxM_Request_Received_Packet_Struct_Data[PLI_MAX_ARRAY_SIZE];
    RIO_DW_T rio_TxRxM_Response_Received_Packet_Struct_Data[PLI_MAX_ARRAY_SIZE];
    RIO_UCHAR_T rio_TxRxM_Violent_Packet_Received_Packet_Buffer[PLI_MAX_STREAM_SIZE];
    RIO_UCHAR_T rio_TxRxM_Canceled_Packet_Received_Packet_Buffer[PLI_MAX_STREAM_SIZE];
    RIO_UCHAR_T rio_TxRxM_Packet_To_Send_Data[PLI_MAX_STREAM_SIZE];
} BUFFERS_TXRX_T;

typedef struct
{
    RIO_HANDLE_T handle;                 /*Instance id*/
    REGISTERS_TXRX_T regs;               /*handlers of registers for callbacks*/
    BUFFERS_TXRX_T bufs;                 /*Internal buffers for array arguments*/
    RIO_BOOL_T is_Serial;                /*defines types of instances*/
} RIO_CONTEXT_TXRX_T;

/*macro for routines which don`t return value*/
#define Wrap_Check_Id_S(id, msg)\
{\
    if ( ((id) < 0 ) || ((id) >= TxRx_Inst_Num) )\
    {\
        SW_TxRx_Wrapper_Message((id), "Error", "incorrect instance id value");\
        return;\
    }\
    if (rio_TxRx_Wrap_Initialized == RIO_FALSE)\
    {\
        SW_TxRx_Wrapper_Message((id), "Error", msg);\
        return;\
    }\
}

/*macro for routines which return value*/
#define Wrap_Check_Id_Ret(id, msg)\
{\
    if ( ((id) < 0 ) || ((id) >= TxRx_Inst_Num) )\
    {\
        SW_TxRx_Wrapper_Message((id), "Error", "incorrect instance id value");\
        tf_putp(0, WRAP_ERROR_RESULT);\
        return;\
    }\
    if (rio_TxRx_Wrap_Initialized == RIO_FALSE)\
    {\
        SW_TxRx_Wrapper_Message((id), "Error", msg);\
        tf_putp(0, WRAP_ERROR_RESULT);\
        return;\
    }\
}

#endif /*TXRX_WRAPPER_COMMON_H*/
