/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: M:\zkg_riobuild_nt\riosuite\src\verilog_env\pliwrap\txrxm\s_txrx_wrapper.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*		to display revision history information.
*               
* Description: PLI wrapper implementation file
*
* Notes: template is generated
*
******************************************************************************/




#ifndef _S_TXRXWRAPPER_H
#define _S_TXRXWRAPPER_H

#include "rio_txrxm_serial_model.h"
#include "txrx_wrapper_common.h"




/*value for instance id which is absent*/
#define S_TXRX_ABSENT_INST_ID        WRAP_ERROR_RESULT 

#define RIO_SERIAL_MAX_DATA_SIZE                           64
#define RIO_SERIAL_MAX_DW_SIZE                             32

/* Data size convertion constants */



#endif /*_S_TXRXWRAPPER_H*/



