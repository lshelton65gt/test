/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\txrxm\s_wrapvars.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*		to display revision history information.
*               
* Description: PLI wrapper implementation file
*
* Notes: template is generated
*
******************************************************************************/


#ifndef S_TXRX_WRAPWARS_H
#define S_TXRX_WRAPWARS_H

/*Function trays*/
RIO_TXRXM_SERIAL_MODEL_FTRAY_T Rio_Txrxm_Serial_Model_Ftray_T_Tray;



/*Callback tray*/
RIO_TXRXM_SERIAL_MODEL_CALLBACK_TRAY_T Rio_Txrxm_Serial_Model_Callback_Tray_T_Tray;

/* Flags to handle 10-bit interface */
RIO_BOOL_T ten_Bit_Interface_TxRxM = RIO_FALSE;
RIO_BOOL_T set_Serial_Pins_10bit_Flag_Reg_TxRxM = RIO_FALSE;

#endif /*S_TXRX_WRAPWARS_H*/

