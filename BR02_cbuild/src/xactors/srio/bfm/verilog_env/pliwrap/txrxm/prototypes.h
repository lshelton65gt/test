/******************************************************************************
*
*       COPYRIGHT 2001-2003 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/verilog_env/pliwrap/txrxm/prototypes.h $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Description: Prototypes of all wrapper functions
*
* Notes:       Prototype of this file is generated automaticaly by WrapGen
*
******************************************************************************/
#ifndef PROTOTYPES_TXRX_H
#define PROTOTYPES_TXRX_H

void SW_TxRx_Wrapper_Message (int id, char *type, char *msg); /*simple error handler */

void SetIntegerRegValue(handle hReg, int value);
int GetIntegerRegValue(handle hReg);
void SW_TxRx_Init ();    /*Initialize wrapper*/
void SW_TxRx_Close ();    /*Closes pli engine*/
void SW_Rio_TxRxM_Clock_Edge ();
void SW_Rio_TxRxM_Packet_Struct_Request ();
void SW_Rio_TxRxM_Packet_Stream_Request ();
void SW_Rio_TxRxM_Symbol_Request ();
void SW_Rio_TxRxM_Training_Request ();
void SW_Rio_TxRxM_Granule_Request ();
void SW_Rio_TxRxM_Model_Start_Reset ();
void SW_Rio_TxRxM_Model_Initialize ();
void SW_Rio_TxRxM_Get_Pins ();
/* Callbacks */
int SW_Rio_TxRxM_Request_Received (
    RIO_CONTEXT_T context,
    RIO_REMOTE_REQUEST_T *packet_Struct,
    RIO_UCHAR_T ack_ID,
    RIO_TR_INFO_T transport_Info,
    RIO_UCHAR_T hop_Count );

int SW_Rio_TxRxM_Response_Received (
    RIO_CONTEXT_T context,
    RIO_RESPONSE_T *packet_Struct,
    RIO_UCHAR_T ack_ID,
    RIO_UCHAR_T hop_Count );

int SW_Rio_TxRxM_Violent_Packet_Received (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *packet_Buffer,
    unsigned int packet_Size,
    RIO_PACKET_VIOLATION_T violation_Type,
    unsigned short expected_Crc);

int SW_Rio_TxRxM_Canceled_Packet_Received (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *packet_Buffer,
    unsigned int packet_Size );

int SW_Rio_TxRxM_Training_Received (
    RIO_CONTEXT_T context,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num );

int SW_Rio_TxRxM_Granule_Received (
    RIO_CONTEXT_T context,
    RIO_GRANULE_TYPE_T granule_Type,
    unsigned int granule,
    RIO_BOOL_T has_Frame_Toggled );

int SW_Rio_TxRxM_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_BYTE_T tframe,
    RIO_BYTE_T td,
    RIO_BYTE_T tdl,
    RIO_BYTE_T tclk );

int SW_TxRx_Rio_User_Msg (
    RIO_CONTEXT_T context,
    char *user_Msg );

void SW_RIO_TxRxM_Model_Create_Instance ();
/*Function for callback trays initialization*/
void Initialize_CB_Tray_TxRx(void);


/*hook*/
int SW_Rio_TxRxM_Packet_To_Send (
    RIO_CONTEXT_T               context, 
    RIO_UCHAR_T                 packet_Buffer[],
    unsigned int                packet_Size  /*packet size in byte count*/
    );


#endif /*PROTOTYPES_TXRX_H*/

