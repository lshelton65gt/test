/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       This code is the property of Motorola
*       and is Motorola Confidential Proprietary Information.
*
* $Element: /project/riosuite/src/verilog_env/pliwrap/txrxm/wrapper_txrx.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: /project/riosuite $ 
* $OS: solaris $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Registers set for data exchange between the top module
*               of demo application and demo scenarios of PE-s
*
* Notes:        This file shall be included to all PE and DMA PE models 
*
******************************************************************************/
`define PLI_LONG_REGISTER_SIZE 32
`define PLI_SHORT_REGISTER_SIZE 8
// Data size convertion constants

`define RIO_BITS_IN_BYTE                    8
`define RIO_BITS_IN_WORD                    32
`define RIO_BITS_IN_DW                      64
`define RIO_BYTES_IN_WORD                   4
`define RIO_BYTES_IN_DW                     8
`define RIO_WORDS_IN_DW                     2
`define RIO_LP_EP_WIRE_WIDTH                8
`define RIO_BITS_IN_CODEGROUP                      10



`define PLI_SMALLL_REGISTER_SIZE 4
`define RIO_MAX_COH_DOMAIN_SIZE    16

//Constants description
`define RIO_PL_SERIAL_CNT_BYTE_IN_GRAN    4
`define RIO_PL_SERIAL_NUMBER_OF_LANES    4


// Return values for some functions

`define RIO_OK                                          0
`define RIO_ERROR                                       1
`define RIO_RETRY                                       2
`define RIO_PARAM_INVALID                               3

// Boolean constants

`define RIO_FALSE                                       0
`define RIO_TRUE                                        1

//Wire witdth constant - don't change it
`define RIO_LP_EP_WIRE_WIDTH                            8


//constants for access to internal buffer

//for put data
`define RIO_TXRXM_PACKET_STRUCT_REQUEST_DATA            0
`define RIO_TXRXM_PACKET_STREAM_REQUEST_PACKET_BUFFER   1

//for get data
`define RIO_TXRXM_REQUEST_RECEIVED_PACKET_DATA          0
`define RIO_TXRXM_RESPONSE_RECEIVED_PACKET_DATA         1
`define RIO_TXRXM_VIOLENT_PACKET_RECEIVED_BUFFER        2
`define RIO_TXRXM_CANCELED_PACKET_RECEIVED_BUFFER       3 
`define RIO_TXRXM_PACKET_TO_SEND_BUFFER                 4

//for select most or least significent word
`define LSW                                             0
`define MSW                                             1

`define RIO_TXRX_WAIT_BEFORE_RETRY                      10

//types for the ftype fields
`define RIO_TXRX_INTERV_REQUEST         1
`define RIO_TXRX_NONINTERV_REQUEST      2
`define RIO_TXRX_WRITE                  5
`define RIO_TXRX_STREAM_WRITE           6
`define RIO_TXRX_MAINTENANCE            8
`define RIO_TXRX_DOORBELL               10
`define RIO_TXRX_MESSAGE                11
`define RIO_TXRX_RESPONCE               13


//Types for the packet place parameter
`define RIO_TXRX_SYMBOL_FREE            0
`define RIO_TXRX_SYMBOL_EMBEDDED        1
`define RIO_TXRX_SYMBOL_CANCELING       2


//Constants for RIO_PLACE_IN_STREAM_T;
`define FREE  0
`define EMBEDDED  1
`define CANCELING  2

// types for the sym_stype

`define RIO_TXRX_PACKET_ACCEPTED_STYPE          0
`define RIO_TXRX_PACKET_RETRY_STYPE             1
`define RIO_TXRX_PACKET_NOT_ACCEPTED_STYPE      2
`define RIO_TXRX_PACKET_CONTROL_STYPE           4
`define RIO_TXRX_LINK_MAINTENANCE_STYPE         5
`define RIO_TXRX_LINK_RESPONCE_STYPE            6

//constant for error instance ID
`define RIO_TXRX_ERROR_INST_ID                  -1



//Constants for RIO_CHARACTER_TYPE_T;
`define RIO_PD  0
`define RIO_SC  1
`define RIO_K  2
`define RIO_R  3
`define RIO_A  4
`define RIO_D  5
`define RIO_I  6

//Instance type constants
`define RIO_TXRX_INST_PAR_MODE      0
`define RIO_TXRX_INST_SERIAL_MODE   1
//Constants for RIO_SIGNAL_T;
`define RIO_SIGNAL_0  0
`define RIO_SIGNAL_1  1

//Constants for RIO_PCS_PMA_MACHINE_T;
`define RIO_PCS_PMA_MACHINE_SYNC  0
`define RIO_PCS_PMA_MACHINE_ALIGN  1
`define RIO_PCS_PMA_MACHINE_INIT  2
`define RIO_PCS_PMA_MACHINE_COMP_SEQ_GEN  3


//Constants for RIO_SYMBOL_SERIAL_VIOLATION_T;
`define RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_CRC  0
`define RIO_SYMBOL_SERIAL_VIOLATION_INCORRECT_SC_PD  1

//Constants for RIO_PLACE_IN_GRANULE_T;
`define RIO_PLACE_IN_GRANULE_EMBEDDED  0
`define RIO_PLACE_IN_GRANULE_REPLACING  1
`define RIO_PLACE_IN_GRANULE_DELETING  2

//Constants for RIO_PACKET_VIOLATION_T;
`define PACKET_IS_LONGER_276  0
`define CRC_ERROR  1
`define FTYPE_RESERVED  2
`define TTYPE_RESERVED  3
`define TT_RESERVED  4
`define NON_ZERO_RESERVED_PACKET_BITS_VALUE  5
`define INCORRECT_PACKET_HEADER  6
`define PAYLOAD_NOT_DW_ALIGNED  7


//Constants for RIO_STATE_INDICATORS_T;
`define RIO_OUR_BUF_STATUS  0
`define RIO_PARTNER_BUF_STATUS  1
`define RIO_FLOW_CONTROL_MODE  2
`define RIO_LRSP_ACKID_STATUS  3
`define RIO_INBOUND_ACKID  4
`define RIO_OUTBOUND_ACKID  5
`define RIO_OUTSTANDING_ACKID  6
`define RIO_TX_DATA_IN_PROGRESS  7
`define RIO_RX_DATA_IN_PROGRESS  8
`define RIO_INITIALIZED  9
`define RIO_UNREC_ERROR  10
`define RIO_OUTPUT_ERROR  11
`define RIO_INPUT_ERROR  12
`define RIO_STATE_INDICATORS_NUMBER  13

//4 ones
`define RIO_TXRX_ALL_LANES_CHANGED  4'b1111

