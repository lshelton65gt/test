/******************************************************************************
*
*       COPYRIGHT 2002-2003 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\txrxm\s_prototypes.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*		to display revision history information.
*               
* Description: PLI wrapper implementation file
*
* Notes: template is generated
*
******************************************************************************/
#ifndef S_TXRX_PROTOTYPES_H
#define S_TXRX_PROTOTYPES_H

void SW_Serial_S_TxRx_Wrapper_Message (int id, char *type, char * msg); /*simple error handler */
int SW_Serial_S_TxRx_Register_Access(
    int instId, 
    int config_Offset, 
    unsigned long *data,  
    REG_ACCESS_T acc_Type); /*routine for access to registers*/

void SetIntegerRegValue(handle hReg, int value);
int GetIntegerRegValue(handle hReg);
void SW_Rio_TxRxM_Clock_Edge ();
void SW_Rio_TxRxM_Packet_Struct_Request ();
void SW_Rio_TxRxM_Packet_Stream_Request ();
void SW_Serial_Rio_TxRxM_Serial_Symbol_Request ();
void SW_Serial_Rio_TxRxM_Serial_Character_Request ();
void SW_Serial_Rio_TxRxM_Serial_Character_Column_Request ();
void SW_Serial_Rio_TxRxM_Serial_Code_Group_Request ();
void SW_Serial_Rio_TxRxM_Serial_Code_Group_Column_Request ();
void SW_Serial_Rio_TxRxM_Serial_Single_Character_Request ();
void SW_Serial_Rio_TxRxM_Serial_Single_Bit_Request ();
void SW_Serial_Rio_TxRxM_Serial_Comp_Seq_Request ();
void SW_Serial_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management ();
void SW_Serial_Rio_TxRxM_Serial_Pop_Bit ();
void SW_Serial_Rio_TxRxM_Serial_Pop_Character ();
void SW_Serial_Rio_TxRxM_Model_Start_Reset ();
void SW_Serial_Rio_TxRxM_Model_Initialize ();
void SW_Serial_Rio_TxRxM_Get_Pins ();
void SW_Serial_Rio_TxRxM_Delete_Instance ();
void SW_Rio_TxRxM_Clock_Edge_New ();
void SW_Serial_Rio_TxRxM_Get_Pins_New ();
/* Callbacks */
int SW_Rio_TxRxM_Request_Received (
    RIO_CONTEXT_T context,
    RIO_REMOTE_REQUEST_T * packet_Struct,
    RIO_UCHAR_T ack_ID,
    RIO_TR_INFO_T transport_Info,
    RIO_UCHAR_T hop_Count );

int SW_Rio_TxRxM_Response_Received (
    RIO_CONTEXT_T context,
    RIO_RESPONSE_T * packet_Struct,
    RIO_UCHAR_T ack_ID,
    RIO_UCHAR_T hop_Count );

int SW_Rio_TxRxM_Violent_Packet_Received (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T * packet_Buffer,
    unsigned int packet_Size,
    RIO_PACKET_VIOLATION_T violation_Type, 
    unsigned short expected_Crc);

int SW_Rio_TxRxM_Canceled_Packet_Received (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T * packet_Buffer,
    unsigned int packet_Size );

int SW_Serial_Rio_TxRxM_Serial_Symbol_Received (
    RIO_CONTEXT_T context,
    RIO_SYMBOL_SERIAL_STRUCT_T * sym,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num );

int SW_Serial_Rio_TxRxM_Serial_Violent_Symbol_Received (
    RIO_CONTEXT_T context,
    unsigned int symbol_Buffer,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num,
    RIO_SYMBOL_SERIAL_VIOLATION_T violation_Type );

int SW_Serial_Rio_TxRxM_Serial_Code_Group_Received (
    RIO_CONTEXT_T context,
    RIO_PCS_PMA_CODE_GROUP_T code_Group );

int SW_Serial_Rio_TxRxM_Serial_Violent_Character_Received (
    RIO_CONTEXT_T context,
    RIO_PCS_PMA_CODE_GROUP_T code_Group,
    RIO_PCS_PMA_CHARACTER_T character,
    RIO_UCHAR_T violation_Lane_Mask );

int SW_Serial_Rio_TxRxM_Serial_Character_Received (
    RIO_CONTEXT_T context,
    RIO_PCS_PMA_CHARACTER_T character );

int SW_Serial_Rio_TxRxM_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_SIGNAL_T tlane0,
    RIO_SIGNAL_T tlane1,
    RIO_SIGNAL_T tlane2,
    RIO_SIGNAL_T tlane3 );

int SW_TxRx_Rio_User_Msg (
    RIO_CONTEXT_T context,
    char *user_Msg );

void SW_Serial_RIO_TxRxM_Serial_Model_Create_Instance ();
/*Function for callback trays initialization*/
void Initialize_CB_Tray_S_TxRx(void);


/*hook*/
int SW_Rio_TxRxM_Packet_To_Send (
    RIO_CONTEXT_T               context, 
    RIO_UCHAR_T                 packet_Buffer[],
    unsigned int                packet_Size  /*packet size in byte count*/
    );

#endif /*S_TXRX_PROTOTYPES_H*/
