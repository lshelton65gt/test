/******************************************************************************
*
*       COPYRIGHT 2001-2003 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/verilog_env/pliwrap/txrxm/wrapper_txrx.c $ 
* $Author: knutson $ 
* $Revision: 1.3 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:          Prototype of this file is generated automaticaly by WrapGen
*               constants in the acc_fetsh_.. routines specifies number of value in the 
*               parameter lists
*
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "wrapper_txrx.h" 
#include "prototypes.h" 


/*Global variables*/
extern int rio_TxRx_Wrap_Initialized;

/*Contexts for instances*/
extern RIO_CONTEXT_TXRX_T *Contexts_TxRx;

extern int TxRx_Max_Inst_Num;  /*Maximum instance number*/
extern int TxRx_Inst_Num;  /*Current allocated instances number*/
/*Internal input buffer for SW_Rio_TxRxM_Packet_Struct_Request routine's variable*/ 
extern RIO_DW_T Rio_TxRxM_Packet_Struct_Request_Data[PLI_MAX_ARRAY_SIZE];
/*Internal input buffer for SW_Rio_TxRxM_Packet_Stream_Request routine's variable*/ 
extern RIO_UCHAR_T Rio_TxRxM_Packet_Stream_Request_Packet_Buffer[PLI_MAX_STREAM_SIZE];

/*Function trays*/
extern RIO_TXRXM_PARALLEL_MODEL_FTRAY_T Rio_Txrxm_Model_Ftray_T_Tray;
/*Callback tray*/
extern RIO_TXRXM_PARALLEL_MODEL_CALLBACK_TRAY_T Rio_Txrxm_Model_Callback_Tray_T_Tray;


/***************************************************************************
 * Function : SW_Rio_TxRxM_Symbol_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Symbol_Request routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Symbol_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Symbol_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_SYMBOL_STRUCT_T sym;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    sym.s = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    sym.ackID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    sym.rsrv_1 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    sym.s_Parity = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 5 ); 
    sym.rsrv_2 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    sym.buf_Status = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    sym.stype = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
/*End of the fields value getting for the sym structure*/
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 9 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 10 ); 
    tf_putp(0, (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Symbol_Request))( Contexts_TxRx[instId].handle,
        &sym , place , granule_Num ));
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Training_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Training_Request routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Training_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Training_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 2 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 3 ); 
    tf_putp(0, (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Training_Request))( Contexts_TxRx[instId].handle,
        place , granule_Num ));
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Granule_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Granule_Request routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Granule_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Granule_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    unsigned long granule;
    RIO_BOOL_T has_Frame_Toggled;
    RIO_PLACE_IN_STREAM_T place;
    unsigned int granule_Num;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    granule = ( unsigned long ) acc_fetch_tfarg_int( k + 2 ); 
    has_Frame_Toggled = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 3 ); 
    place = ( RIO_PLACE_IN_STREAM_T ) acc_fetch_tfarg_int( k + 4 ); 
    granule_Num = ( unsigned int ) acc_fetch_tfarg_int( k + 5 ); 
    tf_putp(0, (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Granule_Request))( Contexts_TxRx[instId].handle,
        granule , has_Frame_Toggled , place , granule_Num ));
}




/***************************************************************************
 * Function : SW_Rio_TxRxM_Model_Initialize
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Model_Initialize routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Model_Initialize routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Model_Initialize ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_TXRXM_PARALLEL_PARAM_SET_T param_Set;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    param_Set.default_Granule = ( long ) acc_fetch_tfarg_int( k + 2 ); 
    param_Set.default_Frame_Toggle = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 3 ); 
    param_Set.lp_Ep_Is_8 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 4 ); 
    param_Set.work_Mode_Is_8 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 5 ); 
    param_Set.in_Ext_Address_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 6 ); 
    param_Set.in_Ext_Address_16 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 7 );
    param_Set.num_Trainings = (unsigned int) acc_fetch_tfarg_int( k + 8 );
    param_Set.requires_Window_Alignment = (RIO_BOOL_T) acc_fetch_tfarg_int( k + 9 );
    param_Set.is_Init_Proc_On = (RIO_BOOL_T) acc_fetch_tfarg_int( k + 10 );
/*End of the fields value getting for the param_Set structure*/
    tf_putp(0, (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Model_Initialize))( Contexts_TxRx[instId].handle,
        &param_Set ));
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Get_Pins
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_TxRxM_Get_Pins routine in the RIO_TXRXM_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_TxRxM_Get_Pins routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_TxRxM_Get_Pins ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_BYTE_T frame;
    RIO_BYTE_T rd;
    RIO_BYTE_T rdl;
    RIO_BYTE_T rclk;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    frame = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 2 ); 
    rd = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 3 ); 
    rdl = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 4 ); 
    rclk = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 5 ); 

    /*Check is instance with the current Inst_ID created of no*/
    if ((instId < 0) || (instId >= TxRx_Inst_Num))
        SW_TxRx_Wrapper_Message (instId, "Warning", "Get_Pins is called for non-existing instance");
    else
    (*(Rio_Txrxm_Model_Ftray_T_Tray.rio_TxRxM_Get_Pins))( Contexts_TxRx[instId].handle,
        frame , rd , rdl , rclk );
}




/*****************************************************************************/
/*     Callbacks implementation                                              */
/*****************************************************************************/




/***************************************************************************
 * Function : SW_Rio_TxRxM_Symbol_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Symbol_Received
 *
 * Returns: result of Rio_TxRxM_Symbol_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Symbol_Received (
    RIO_CONTEXT_T context,
    RIO_SYMBOL_STRUCT_T *sym,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_S_Reg, (int)sym->s);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_AckID_Reg, (int)sym->ackID);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Rsrv_1_Reg, (int)sym->rsrv_1);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_S_Parity_Reg, (int)sym->s_Parity);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Rsrv_2_Reg, (int)sym->rsrv_2);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Buf_Status_Reg, (int)sym->buf_Status);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Stype_Reg, (int)sym->stype);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Place_Reg, (int)place);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Granule_Num_Reg, (int)granule_Num);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Training_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Training_Received
 *
 * Returns: result of Rio_TxRxM_Training_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Training_Received (
    RIO_CONTEXT_T context,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Training_Received_Place_Reg, (int)place);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Training_Received_Granule_Num_Reg, (int)granule_Num);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Training_Received_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Violent_Symbol_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Violent_Symbol_Received
 *
 * Returns: result of Rio_TxRxM_Violent_Symbol_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Violent_Symbol_Received (
    RIO_CONTEXT_T context,
    unsigned int symbol_Buffer,
    RIO_PLACE_IN_STREAM_T place,
    unsigned int granule_Num,
    RIO_SYMBOL_VIOLATION_T violation_Type )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Symbol_Buffer_Reg, (int)symbol_Buffer);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Place_Reg, (int)place);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Granule_Num_Reg, (int)granule_Num);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Violation_Type_Reg, (int)violation_Type);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Granule_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Granule_Received
 *
 * Returns: result of Rio_TxRxM_Granule_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Granule_Received (
    RIO_CONTEXT_T context,
    RIO_GRANULE_TYPE_T granule_Type,
    unsigned int granule,
    RIO_BOOL_T has_Frame_Toggled )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Granule_Type_Reg, (int)granule_Type);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Granule_Reg, (int)granule);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Has_Frame_Toggled_Reg, (int)has_Frame_Toggled);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_TxRxM_Set_Pins
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_TxRxM_Set_Pins
 *
 * Returns: result of Rio_TxRxM_Set_Pins routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_TxRxM_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_BYTE_T tframe,
    RIO_BYTE_T td,
    RIO_BYTE_T tdl,
    RIO_BYTE_T tclk )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tframe_Reg, (int)tframe);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Td_Reg, (int)td);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tdl_Reg, (int)tdl);
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tclk_Reg, (int)tclk);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Flag_Reg, 1);
    return 0;
}

/*****************************************************************************/
/*     End of callbacks implementation                                       */
/*****************************************************************************/




/***************************************************************************
 * Function : SW_RIO_TxRxM_Model_Create_Instance
 *
 * Description: Software part of PLI function pair to provide
 *              Instance creation and binding
 *
 * Returns: Identificator of instance if All right or -1 if error
 *
 * Notes: The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_RIO_TxRxM_Model_Create_Instance ()
{
    int k;
    int instId;
    int res;

    RIO_TXRXM_PARALLEL_INST_PARAM_T param;
    RIO_HANDLE_T   handle;
/* End of parameters defintion*/

    /*check that wrapper is initialized*/
    if (rio_TxRx_Wrap_Initialized == RIO_FALSE)
    {
        SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", "Create_Instnce - TxRx Wrapper isn't initialized");    
        tf_putp(0, (int)RIO_TXRX_ABSENT_INST_ID);
        return;
    }

    k = 0;
/*Instance ID calculating*/
    instId = TxRx_Inst_Num++;
/*  If no more memory for instances available then raise an error */
    if (TxRx_Inst_Num > TxRx_Max_Inst_Num)
    {
        SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", "Too many instances created");
        tf_putp(0, (int)RIO_TXRX_ABSENT_INST_ID);
        return;
    }

/*Parameters accessing*/
    param.packets_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 1 ); 
    param.symbols_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 2 ); 
    param.granules_Buffer_Size = ( int ) acc_fetch_tfarg_int( k + 3 ); 
    k++;
    param.event_Buffer_Size =  ( int ) acc_fetch_tfarg_int( k + 3 );
/*End of the fields value getting for the param structure*/
/*Get registers handles*/
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Packet_Type_Reg = acc_handle_tfarg( k + 4);
/*registers handlers for access to register structure*/
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Target_TID = acc_handle_tfarg( k + 5);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Src_ID = acc_handle_tfarg( k + 6);  
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Prio = acc_handle_tfarg( k + 7);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Wdptr = acc_handle_tfarg( k + 8);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Rdsize = acc_handle_tfarg( k + 9);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Format_Type = acc_handle_tfarg( k + 10);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ttype = acc_handle_tfarg( k + 11);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Address = acc_handle_tfarg( k + 12);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ext_Address = acc_handle_tfarg( k + 13);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Xasmsbs = acc_handle_tfarg( k + 14);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Sec_ID = acc_handle_tfarg( k + 15);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Sec_TID = acc_handle_tfarg( k + 16);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Mbox = acc_handle_tfarg( k + 17);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Letter = acc_handle_tfarg( k + 18);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Msgseg = acc_handle_tfarg( k + 19);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ssize = acc_handle_tfarg( k + 20);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Msglen = acc_handle_tfarg( k + 21);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Doorbell_Info = acc_handle_tfarg( k + 22);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Offset = acc_handle_tfarg( k + 23);
    k = k + 19;
/*end of registers handlers for access to register structure*/

    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Dw_Num_Reg = acc_handle_tfarg( k + 5);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Transp_Type_Reg = acc_handle_tfarg( k + 6);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Ack_ID_Reg = acc_handle_tfarg( k + 7);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Transport_Info_Reg = acc_handle_tfarg( k + 8);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Hop_Count_Reg = acc_handle_tfarg( k + 9);
    Contexts_TxRx[instId].regs.rio_TxRxM_Request_Received_Flag_Reg = acc_handle_tfarg( k + 10);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Prio_Reg = acc_handle_tfarg( k + 11);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Ftype_Reg = acc_handle_tfarg( k + 12);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Transaction_Reg = acc_handle_tfarg( k + 13);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Status_Reg = acc_handle_tfarg( k + 14);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Tr_Info_Reg = acc_handle_tfarg( k + 15);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Src_ID_Reg = acc_handle_tfarg( k + 16);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Target_TID_Reg = acc_handle_tfarg( k + 17);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Sec_ID_Reg = acc_handle_tfarg( k + 18);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Sec_TID_Reg = acc_handle_tfarg( k + 19);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Dw_Num_Reg = acc_handle_tfarg( k + 20);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Ack_ID_Reg = acc_handle_tfarg( k + 21);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Hop_Count_Reg = acc_handle_tfarg( k + 22);
    Contexts_TxRx[instId].regs.rio_TxRxM_Response_Received_Flag_Reg = acc_handle_tfarg( k + 23);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Packet_Size_Reg = acc_handle_tfarg( k + 24);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Violation_Type_Reg = acc_handle_tfarg( k + 25);
    
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Expected_Crc_Reg = acc_handle_tfarg( k + 52);
    k++;

    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Packet_Received_Flag_Reg = acc_handle_tfarg( k + 26);
    Contexts_TxRx[instId].regs.rio_TxRxM_Canceled_Packet_Received_Packet_Size_Reg = acc_handle_tfarg( k + 27);
    Contexts_TxRx[instId].regs.rio_TxRxM_Canceled_Packet_Received_Flag_Reg = acc_handle_tfarg( k + 28);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_S_Reg = acc_handle_tfarg( k + 29);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_AckID_Reg = acc_handle_tfarg( k + 30);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Rsrv_1_Reg = acc_handle_tfarg( k + 31);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_S_Parity_Reg = acc_handle_tfarg( k + 32);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Rsrv_2_Reg = acc_handle_tfarg( k + 33);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Buf_Status_Reg = acc_handle_tfarg( k + 34);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Stype_Reg = acc_handle_tfarg( k + 35);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Place_Reg = acc_handle_tfarg( k + 36);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Granule_Num_Reg = acc_handle_tfarg( k + 37);
    Contexts_TxRx[instId].regs.rio_TxRxM_Symbol_Received_Flag_Reg = acc_handle_tfarg( k + 38);
    Contexts_TxRx[instId].regs.rio_TxRxM_Training_Received_Place_Reg = acc_handle_tfarg( k + 39);
    Contexts_TxRx[instId].regs.rio_TxRxM_Training_Received_Granule_Num_Reg = acc_handle_tfarg( k + 40);
    Contexts_TxRx[instId].regs.rio_TxRxM_Training_Received_Flag_Reg = acc_handle_tfarg( k + 41);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Symbol_Buffer_Reg = acc_handle_tfarg( k + 42);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Place_Reg = acc_handle_tfarg( k + 43);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Granule_Num_Reg = acc_handle_tfarg( k + 44);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Violation_Type_Reg = acc_handle_tfarg( k + 45);
    Contexts_TxRx[instId].regs.rio_TxRxM_Violent_Symbol_Received_Flag_Reg = acc_handle_tfarg( k + 46);
    Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Granule_Type_Reg = acc_handle_tfarg( k + 47);
    Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Granule_Reg = acc_handle_tfarg( k + 48);
    Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Has_Frame_Toggled_Reg = acc_handle_tfarg( k + 49);
    Contexts_TxRx[instId].regs.rio_TxRxM_Granule_Received_Flag_Reg = acc_handle_tfarg( k + 50);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tframe_Reg = acc_handle_tfarg( k + 51);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Td_Reg = acc_handle_tfarg( k + 52);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tdl_Reg = acc_handle_tfarg( k + 53);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Tclk_Reg = acc_handle_tfarg( k + 54);
    Contexts_TxRx[instId].regs.rio_TxRxM_Set_Pins_Flag_Reg = acc_handle_tfarg( k + 55);
    Contexts_TxRx[instId].regs.rio_TxRxM_Packet_To_Send_Packet_Size = acc_handle_tfarg( k + 56);
    Contexts_TxRx[instId].regs.rio_TxRxM_Packet_To_Send_Flag_Reg = acc_handle_tfarg( k + 57);
    
    
    res = RIO_TxRxM_Parallel_Model_Create_Instance( &handle,
        &param );
    if ( res != RIO_OK )
    {
        io_printf ("Error during instance creating \n");
        tf_putp(0, (int)RIO_TXRX_ABSENT_INST_ID);
        TxRx_Inst_Num--;
        return;
    }

    Contexts_TxRx[instId].handle = handle;

/*Instance binding*/

    Rio_Txrxm_Model_Callback_Tray_T_Tray.txrxm_Interface_Context = (RIO_CONTEXT_T)instId;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.lpep_Interface_Context = (RIO_CONTEXT_T)instId;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_Msg = (RIO_CONTEXT_T)instId;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.hook_Context = (RIO_CONTEXT_T)instId;
    
    res = RIO_TxRxM_Parallel_Model_Bind_Instance(
            Contexts_TxRx[instId].handle,
            &Rio_Txrxm_Model_Callback_Tray_T_Tray);
    if ( res != RIO_OK )
    {
        io_printf ("Error during instance binding \n");
        tf_putp(0, (int)RIO_TXRX_ABSENT_INST_ID);
        TxRx_Inst_Num--;
        return;
    }
/*End of instance binding*/
    
    Contexts_TxRx[instId].is_Serial = RIO_FALSE;
    tf_putp(0, instId);
}





/***************************************************************************
 * Function : Initialize_CB_Tray_TxRx
 *
 * Description: Function for callback trays initialization
 *
 * Returns:  
 *
 * Notes: This function is generated automaticaly by WrapGen
 *
 **************************************************************************/
void Initialize_CB_Tray_TxRx(void)
{
/*Initialize callbacks for tray 0*/
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Request_Received = SW_Rio_TxRxM_Request_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Response_Received = SW_Rio_TxRxM_Response_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Violent_Packet_Received = SW_Rio_TxRxM_Violent_Packet_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Canceled_Packet_Received = SW_Rio_TxRxM_Canceled_Packet_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Symbol_Received = SW_Rio_TxRxM_Symbol_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Training_Received = SW_Rio_TxRxM_Training_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Violent_Symbol_Received = SW_Rio_TxRxM_Violent_Symbol_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Granule_Received = SW_Rio_TxRxM_Granule_Received;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Set_Pins = SW_Rio_TxRxM_Set_Pins;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_User_Msg = SW_TxRx_Rio_User_Msg;
/*hook callbacks*/
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Packet_To_Send = SW_Rio_TxRxM_Packet_To_Send;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Granule_To_Send = NULL;
    Rio_Txrxm_Model_Callback_Tray_T_Tray.rio_TxRxM_Last_Packet_Granule = NULL;
}

/*implementation of stubs for the serial routines*/
#ifdef TXRX_WRAP_PAR_ONLY

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Symbol_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Symbol_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Character_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Character_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Character_Column_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Character_Column_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Code_Group_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Code_Group_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Code_Group_Column_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Code_Group_Column_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Single_Character_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Single_Character_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Single_Bit_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Single_Bit_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Comp_Seq_Request
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Comp_Seq_Request()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Pcs_Pma_Machine_Management()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Pop_Bit
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Pop_Bit()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Serial_Pop_Character
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Serial_Pop_Character()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Model_Initialize
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Model_Initialize()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Serial_Rio_TxRxM_Get_Pins
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_Rio_TxRxM_Get_Pins()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
}

/***************************************************************************
 * Function : SW_Serial_RIO_TxRxM_Serial_Model_Create_Instance
 *
 * Description: Stub for serial routine
 *
 * Returns:  
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Serial_RIO_TxRxM_Serial_Model_Create_Instance()
{
    SW_TxRx_Wrapper_Message(RIO_TXRX_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}


#endif /*TXRX_WRAP_PAR_ONLY*/
