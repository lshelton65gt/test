/******************************************************************************
*
*       COPYRIGHT 2001-2003 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development 
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: /project/riosuite/src/verilog_env/pliwrap/txrxm/txrx_user_func.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: /project/riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: Routines which depend of user
*
* Notes:       
*
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "txrx_wrapper_common.h"
#include "prototypes.h" 


/***************************************************************************
 * Function : SW_Rio_User_Msg
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_User_Msg
 *
 * Returns: result of Rio_User_Msg routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_TxRx_Rio_User_Msg (
    RIO_CONTEXT_T context,
    char *user_Msg )
{
    int instId;
    instId = (int)context;
    
    io_printf ("TxRx%i:time = %i: %s\n", (int)context, tf_gettime(), user_Msg);

    return RIO_OK;
}


