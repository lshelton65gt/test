#define MODELSIM_PLI_C
/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\veriuser.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  File to link pli wrapper to Mentor Grahics ModelSIM
*
* Notes:        This file is generated automaticaly by WrapGen
*
******************************************************************************/

#include "veriuser.h"

#ifdef _NC_VLOG_
#include "vxl_veriuser.h"
#endif


extern void SW_PL_Init();
extern void SW_PL_Close();
extern void SW_Rio_PL_Clock_Edge();
extern void SW_Rio_PL_GSM_Request();
extern void SW_Rio_PL_IO_Request();
extern void SW_Rio_PL_Message_Request();
extern void SW_Rio_PL_Doorbell_Request();
extern void SW_Rio_PL_Configuration_Request();
extern void SW_Rio_PL_Send_Response();
extern void SW_Rio_PL_Ack_Remote_Req();
extern void SW_Rio_PL_Set_Config_Reg();
extern void SW_Rio_PL_Get_Config_Reg();
extern void SW_Rio_PL_Model_Start_Reset();
extern void SW_Rio_PL_Model_Initialize();
extern void SW_Rio_PL_Get_Pins();
extern void SW_RIO_PL_Model_Create_Instance();
extern void SW_Put_Data_Pl();
extern void SW_Get_Data_Pl();

extern void SW_RIO_PL_Serial_Model_Create_Instance ();
extern void SW_Rio_PL_Serial_Model_Initialize ();
extern void SW_Rio_PL_Serial_Get_Pins ();

extern void SW_PL_Lost_Sync_Command();
extern void SW_PL_Enable_Rnd_Idle_Generator();
extern void SW_PL_Force_EOP_Insertion();

extern void SW_Rio_PL_Set_Retry_Generator();

/***************************************************************************
 * Function : RIO_Sizetf_8
 *
 * Description: Routine for size detection
 *
 * Returns: 8
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_8()
{
    return 8;
}



/***************************************************************************
 * Function : RIO_Sizetf_16
 *
 * Description: Routine for size detection
 *
 * Returns: 16
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_16()
{
    return 16;
}



/***************************************************************************
 * Function : RIO_Sizetf_32
 *
 * Description: Routine for size detection
 *
 * Returns: 32
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_32()
{
    return 32;
}



/***************************************************************************
 * Function : RIO_Sizetf_64
 *
 * Description: Routine for size detection
 *
 * Returns: 64
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_64()
{
    return 64;
}

s_tfcell veriusertfs_pl[] = {
    {
        usertask,           /* tells whether Verilog task or function */
        0,                  /* data argument of callback function */
        0,                  /* checktf */
        0,  /* size of returned data if function */
        SW_PL_Init,            /* pointer to C function associated with Verilog task or function */
        0,                  /* misctf */
        "$HW_PL_Init"          /* verilog name */
    },
{usertask, 0, 0, 0, SW_PL_Close, 0, "$HW_PL_Close"},
{usertask, 0, 0, 0, SW_Rio_PL_Clock_Edge, 0, "$HW_Rio_PL_Clock_Edge"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_GSM_Request, 0, "$HW_Rio_PL_GSM_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_IO_Request, 0, "$HW_Rio_PL_IO_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Message_Request, 0, "$HW_Rio_PL_Message_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Doorbell_Request, 0, "$HW_Rio_PL_Doorbell_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Configuration_Request, 0, "$HW_Rio_PL_Configuration_Request"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Send_Response, 0, "$HW_Rio_PL_Send_Response"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Ack_Remote_Req, 0, "$HW_Rio_PL_Ack_Remote_Req"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Set_Config_Reg, 0, "$HW_Rio_PL_Set_Config_Reg"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Get_Config_Reg, 0, "$HW_Rio_PL_Get_Config_Reg"},
{usertask, 0, 0, 0, SW_Rio_PL_Model_Start_Reset, 0, "$HW_Rio_PL_Model_Start_Reset"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Model_Initialize, 0, "$HW_Rio_PL_Model_Initialize"},
{usertask, 0, 0, 0, SW_Rio_PL_Get_Pins, 0, "$HW_Rio_PL_Get_Pins"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_RIO_PL_Model_Create_Instance, 0, "$HW_RIO_PL_Model_Create_Instance"},
{usertask, 0, 0, 0, SW_Put_Data_Pl, 0, "$HW_Put_Data_Pl"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Get_Data_Pl, 0, "$HW_Get_Data_Pl"},
/*serial specific routines*/
{userfunction, 0, 0, RIO_Sizetf_32, SW_RIO_PL_Serial_Model_Create_Instance, 0, "$HW_RIO_PL_Serial_Model_Create_Instance"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Serial_Model_Initialize, 0, "$HW_Rio_PL_Serial_Model_Initialize"},
{usertask, 0, 0, 0, SW_Rio_PL_Serial_Get_Pins, 0, "$HW_Rio_PL_Serial_Get_Pins"},

{usertask, 0, 0, 0, SW_PL_Lost_Sync_Command, 0, "$HW_Rio_PL_Lost_Sync_Command"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_PL_Enable_Rnd_Idle_Generator, 0, "$HW_Rio_PL_Enable_Rnd_Idle_Generator"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_PL_Force_EOP_Insertion, 0, "$HW_Rio_PL_Force_EOP_Insertion"},
{userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_PL_Set_Retry_Generator, 0, "$HW_Rio_PL_Set_Retry_Generator"},
{0} /* last entry must be 0 */
};



p_tfcell Bootstrap_PL ()
{
    return (veriusertfs_pl);
}
