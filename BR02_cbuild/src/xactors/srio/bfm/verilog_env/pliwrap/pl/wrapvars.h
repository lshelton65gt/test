/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\wrapvars.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  Wrappers variables description
*
* Notes:        This file is generated automaticaly.
*               SHALL INCLUDED ONLY TO THE wrapper_pl.c FILE
*
******************************************************************************/

#ifndef PL_WRAPVARS_H
#define PL_WRAPVARS_H

#ifndef PL_WRAP_SERIAL_ONLY
/*Function trays*/
RIO_PL_PARALLEL_MODEL_FTRAY_T Rio_Pl_Model_Ftray_T_Tray;
/*Callback tray*/
RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T Rio_Pl_Model_Callback_Tray_T_Tray;
#endif

#ifndef PL_WRAP_PAR_ONLY
/*Function trays*/
RIO_PL_SERIAL_MODEL_FTRAY_T Rio_Pl_Serial_Model_Ftray_T_Tray;
/*Callback tray*/
RIO_PL_SERIAL_MODEL_CALLBACK_TRAY_T Rio_Pl_Serial_Model_Callback_Tray_T_Tray;
/* Flags to handle 10-bit interface */
RIO_BOOL_T ten_Bit_Interface_PL = RIO_FALSE;
RIO_BOOL_T set_Serial_Pins_10bit_Flag_Reg_PL = RIO_FALSE;
#endif


/*Contexts for instances*/
RIO_CONTEXT_PL_T *Contexts_Pl;

/*flag which signalized that wrapper is initialized*/
int rio_Pl_Wrap_Initialized = RIO_FALSE;


int Pl_Max_Inst_Num;  /*Maximum instance number*/
int Pl_Inst_Num;  /*Current allocated instances number*/


/* Names of array's indexes for model API routines*/
typedef enum {
    RIO_PL_GSM_REQUEST_RQ_DATA = 0,
    RIO_PL_GSM_REQUEST_TRANSPORT_INFO,
    RIO_PL_IO_REQUEST_RQ_DATA,
    RIO_PL_MESSAGE_REQUEST_RQ_DATA,
    RIO_PL_CONFIGURATION_REQUEST_RQ_DATA,
    RIO_PL_PACKET_STRUCT_REQUEST_RQ_DATA,
    RIO_PL_SEND_RESPONSE_RESP_DATA
} PUT_IND_T;

/* Names of array's indexes for Callback*/
typedef enum {
    RIO_PL_REMOTE_REQUEST_REQ_DATA = 0,
    RIO_PL_REMOTE_RESPONSE_RESP_DATA,
    RIO_PL_GET_CONFIG_REG_DATA
} GET_IND_T;

#endif /*PL_WRAPVARS_H*/
