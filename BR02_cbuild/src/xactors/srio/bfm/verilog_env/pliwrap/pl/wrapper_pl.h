/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\wrapper_pl.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Description: This file contain prototypes c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:       Prototype of this file is generated automaticaly by WrapGen
*
******************************************************************************/


#ifndef _PLWRAPPER_H
#define _PLWRAPPER_H

#ifndef PL_WRAP_SERIAL_ONLY
#include "rio_pl_parallel_model.h"
#endif


#ifndef PL_WRAP_PAR_ONLY
#include "rio_pl_serial_model.h"
#endif



#define RIO_PLI_SHORT_REGISTER_SIZE                 8
#define RIO_PLI_LONG_REGISTER_SIZE                  32
#define RIO_MAX_DATA_SIZE                           64

#define RIO_MAX_DW_SIZE                             32

#define RIO_MAX_NUM_OF_PE                           256
#define RIO_MAX_NUM_OF_SWITCHES                     16

/* Data size convertion constants */

#define RIO_BITS_IN_BYTE                            8
#define RIO_BITS_IN_WORD                            32
#define RIO_BITS_IN_DW                              64
#define RIO_BYTES_IN_WORD                           4
#define RIO_BYTES_IN_DW                             8
#define RIO_WORDS_IN_DW                             2

#define PLI_MAX_ARRAY_SIZE                          255

/*Value for error instance ID value*/
#define RIO_PL_ABSENT_INST_ID                       -1
#define WRAP_ERROR_RESULT                           -1

 /*Values for write register routine*/
typedef enum
{
    REG_READ = 0,
    REG_WRITE
} REG_ACCESS_T;

/*Values for access to internal buffers*/
typedef enum 
{
    LSW = 0,    /*Least significent word*/
    MSW         /*Most significent word*/
} WORD_POS_T;


/*working mode of the instance*/
typedef enum
{
    PL_INST_PAR_MODE = 0,
    PL_INST_SERIAL_MODE
} PL_WRAP_INST_MODE_T;


/* Types for instance identification*/
typedef struct
{
    /*This structure is user-defined and can contain any user's variables*/
    PL_WRAP_INST_MODE_T inst_Mode;
} RIO_PRIVATE_PL_T;

typedef struct
{
    handle rio_PL_Ack_Request_Tag_Reg;
    handle rio_PL_Ack_Request_Flag_Reg;

    handle rio_PL_Ack_Response_Tag_Reg;
    handle rio_PL_Ack_Response_Flag_Reg;

    handle rio_PL_Req_Time_Out_Tag_Reg;
    handle rio_PL_Req_Time_Out_Flag_Reg;

    handle rio_PL_Remote_Request_Req_Target_TID;
    handle rio_PL_Remote_Request_Req_Src_ID;
    handle rio_PL_Remote_Request_Req_Prio;
    handle rio_PL_Remote_Request_Req_Wdptr;
    handle rio_PL_Remote_Request_Req_Rdsize;
    handle rio_PL_Remote_Request_Req_Ftype;
    handle rio_PL_Remote_Request_Req_Ttype;
    handle rio_PL_Remote_Request_Req_Address;
    handle rio_PL_Remote_Request_Req_Extended_Address;
    handle rio_PL_Remote_Request_Req_Xamsbs;
    handle rio_PL_Remote_Request_Req_Sec_ID;
    handle rio_PL_Remote_Request_Req_Sec_TID;
    handle rio_PL_Remote_Request_Req_Mbox;
    handle rio_PL_Remote_Request_Req_Letter;
    handle rio_PL_Remote_Request_Req_Msgseg;
    handle rio_PL_Remote_Request_Req_Ssize;
    handle rio_PL_Remote_Request_Req_Msglen;
    handle rio_PL_Remote_Request_Req_Doorbell_Info;
    handle rio_PL_Remote_Request_Req_Offset;

    handle rio_PL_Remote_Request_Tag_Reg;
    handle rio_PL_Remote_Request_Packet_Type_Reg;
    handle rio_PL_Remote_Request_Dw_Num_Reg;
    handle rio_PL_Remote_Request_Transp_Type_Reg;
    handle rio_PL_Remote_Request_Transport_Info_Reg;
    handle rio_PL_Remote_Request_Flag_Reg;

    handle rio_PL_Remote_Response_Tag_Reg;
    handle rio_PL_Remote_Response_Prio_Reg;
    handle rio_PL_Remote_Response_Ftype_Reg;
    handle rio_PL_Remote_Response_Transaction_Reg;
    handle rio_PL_Remote_Response_Status_Reg;
    handle rio_PL_Remote_Response_Tr_Info_Reg;
    handle rio_PL_Remote_Response_Src_ID_Reg;
    handle rio_PL_Remote_Response_Target_TID_Reg;
    handle rio_PL_Remote_Response_Sec_ID_Reg;
    handle rio_PL_Remote_Response_Sec_TID_Reg;
    handle rio_PL_Remote_Response_Dw_Num_Reg;
    handle rio_PL_Remote_Response_Flag_Reg;

    handle rio_PL_Set_Pins_Tframe_Reg;
    handle rio_PL_Set_Pins_Td_Reg;
    handle rio_PL_Set_Pins_Tdl_Reg;
    handle rio_PL_Set_Pins_Tclk_Reg;
    handle rio_PL_Set_Pins_Flag_Reg;

    handle rio_PL_Set_Pins_Tlane0_Reg;
    handle rio_PL_Set_Pins_Tlane1_Reg;
    handle rio_PL_Set_Pins_Tlane2_Reg;
    handle rio_PL_Set_Pins_Tlane3_Reg;
    handle rio_PL_Set_Serial_Pins_Flag_Reg;

    /*serial link interface - 10-bits*/
    handle rio_PL_Set_Pins_Tlane0_10_Reg;
    handle rio_PL_Set_Pins_Tlane1_10_Reg;
    handle rio_PL_Set_Pins_Tlane2_10_Reg;
    handle rio_PL_Set_Pins_Tlane3_10_Reg;
    handle rio_PL_Set_Serial_Pins_10bit_Flag_Reg;

    handle extend_Reg_Write_Config_Offset_Reg;
    handle extend_Reg_Write_Data_Reg;
    handle extend_Reg_Write_Flag_Reg;
} REGISTERS_PL_T;

typedef struct
{
    /*buffers for the callbacks*/
    RIO_DW_T rio_PL_Remote_Request_Req_Data[PLI_MAX_ARRAY_SIZE];
    RIO_DW_T rio_PL_Remote_Response_Resp_Data[PLI_MAX_ARRAY_SIZE];
    unsigned long rio_PL_Get_Config_Reg_Data;
    /*buffers for the request routines*/
    /*Internal input buffer for SW_Rio_PL_GSM_Request routine's variable*/ 
    RIO_DW_T rio_PL_GSM_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_PL_GSM_Request routine's variable*/ 
    RIO_TR_INFO_T rio_PL_GSM_Request_Transport_Info[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_PL_IO_Request routine's variable*/ 
    RIO_DW_T rio_PL_IO_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_PL_Message_Request routine's variable*/ 
    RIO_DW_T rio_PL_Message_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_PL_Configuration_Request routine's variable*/ 
    RIO_DW_T rio_PL_Configuration_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_PL_Send_Response routine's variable*/ 
    RIO_DW_T rio_PL_Send_Response_Resp_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_PL_Send_Response routine's variable*/ 
    RIO_DW_T Rio_PL_Send_Response_Resp_Data[PLI_MAX_ARRAY_SIZE];
    /*packet struct request data internal buffer*/
    RIO_DW_T Rio_PL_Packet_Struct_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
} BUFFERS_PL_T;

typedef struct
{
    RIO_HANDLE_T handle;           /*Instance id*/
    REGISTERS_PL_T regs;               /*handlers of registers for callbacks*/
    BUFFERS_PL_T bufs;                 /*Internal buffers for array arguments*/
    RIO_PRIVATE_PL_T priv_Data;    /*Private features*/
} RIO_CONTEXT_PL_T;

/*macro which checks that inst id is ok*/
#define Wrap_Check_Id(id)\
{\
    if ( ((id) < 0 ) || ((id) >= Pl_Inst_Num) )\
    {\
        SW_Pl_Wrapper_Message((id), "Error", "incorrect instance id value");\
        return;\
    }\
    if (rio_Pl_Wrap_Initialized == RIO_FALSE)\
    {\
        SW_Pl_Wrapper_Message((id), "Error", "PL Wrapper isn't initialized");\
        return;\
    }\
}

/*macro for routines which return value*/
#define Wrap_Check_Id_Ret(id, msg)\
{\
    if ( ((id) < 0 ) || ((id) >= Pl_Inst_Num) )\
    {\
        SW_Pl_Wrapper_Message((id), "Error", "incorrect instance id value");\
        tf_putp(0, WRAP_ERROR_RESULT);\
        return;\
    }\
    if (rio_Pl_Wrap_Initialized == RIO_FALSE)\
    {\
        SW_Pl_Wrapper_Message((id), "Error", msg);\
        tf_putp(0, WRAP_ERROR_RESULT);\
        return;\
    }\
}



#define PL_IN_PAR_MODE (Contexts_Pl[instId].priv_Data.inst_Mode == PL_INST_PAR_MODE)
#define PL_IN_SERIAL_MODE (Contexts_Pl[instId].priv_Data.inst_Mode == PL_INST_SERIAL_MODE)

#endif /*_PLWRAPPER_H*/





