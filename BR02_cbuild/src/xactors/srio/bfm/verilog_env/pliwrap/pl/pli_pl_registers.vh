/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\pli_pl_registers.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description: Registers set for data exchange between PLI wrapper (written in C)
*				and PE environment (written in Verilog).
*
* Notes:       This file shall be included to Verilog modules
*
******************************************************************************/


/* registers driven by PLI*/
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_ack_request_tag_reg; 
    reg rio_pl_ack_request_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_get_req_data_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_get_req_data_offset_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_get_req_data_cnt_reg; 
    reg rio_pl_get_req_data_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_ack_response_tag_reg; 
    reg rio_pl_ack_response_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_get_resp_data_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_get_resp_data_offset_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_get_resp_data_cnt_reg; 
    reg rio_pl_get_resp_data_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_req_time_out_tag_reg; 
    reg rio_pl_req_time_out_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_prio_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_ftype_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_transaction_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_status_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_tr_info_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_src_id_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_target_tid_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_sec_id_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_sec_tid_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_response_dw_num_reg; 
    reg rio_pl_remote_response_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_tframe_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_td_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_tdl_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_tclk_reg; 
    reg rio_pl_set_pins_flag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] extend_reg_write_config_offset_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] extend_reg_write_data_reg; 
    reg extend_reg_write_flag_reg; 



    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_target_tid;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_src_id;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_prio;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_wdptr;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_rdsize;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_ftype;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_ttype;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_extended_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_xamsbs;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_sec_id;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_sec_tid;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_mbox;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_letter;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_msgseg;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_ssize;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_msglen;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_doorbell_info;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_req_offset;
    
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_dw_num_reg;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_packet_type_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_transp_type_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_remote_request_transport_info_reg; 
    reg rio_pl_remote_request_flag_reg; 

//Serial pin registers
    reg rio_pl_set_pins_tlane0_reg;
    reg rio_pl_set_pins_tlane1_reg;
    reg rio_pl_set_pins_tlane2_reg;
    reg rio_pl_set_pins_tlane3_reg;
    reg rio_pl_set_serial_pins_flag_reg;

//Serial pin 10-bit interface
    reg [0 : 9] rio_pl_set_pins_tlane0_10_reg; 
    reg [0 : 9] rio_pl_set_pins_tlane1_10_reg; 
    reg [0 : 9] rio_pl_set_pins_tlane2_10_reg; 
    reg [0 : 9] rio_pl_set_pins_tlane3_10_reg; 
    reg [0 : 9] rio_pl_set_serial_pins_10bit_flag_reg; 
//end of serial pin 10-bit interface

