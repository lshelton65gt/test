/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\wrapper_pl.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:          Prototype of this file is generated automaticaly by WrapGen
*               constants in the acc_fetch_.. routines are used for access to parameter
*               of passed parameter list
*
******************************************************************************/



#include "acc_user.h"
#ifdef _MODELSIM_
#include "veriuser.h"
#else
#include "vcsuser.h"
#endif


#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "wrapper_pl.h" 
#include "prototypes.h" 
#include "wrapvars.h"



/***************************************************************************
 * Function : SW_Pl_Wrapper_Message
 *
 * Description: Simple Error handle
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_Pl_Wrapper_Message (int id, char *type, char * msg)
{
    io_printf ("%s wrapper: SW_Pl_Wrapper_Message, instance=%i: %s\n", type, id, msg);
}


/***************************************************************************
 * Function : SetIntegerRegValue
 *
 * Description: Sets register regName to specified integer value
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SetIntegerRegValue(handle hReg, int value)
{
    static s_acc_value val = {accIntVal};    /* value  */
    static s_setval_delay delay = {{0, 0, 0, 0.0}, accNoDelay}; /* delay - NO */

    /*check handle*/
    if (hReg == (handle)0)
    {
        io_printf("PL wrapper: Warning register's handle is NULL\n");
        return;
    }
    /* set register value */
    val.value.integer = value;

    if (acc_set_value(hReg, &val, &delay))
    {
        io_printf ("PL Wrapper: Error during access to the verilog register");
        tf_dofinish(); /* finish if error */
    }
}


/***************************************************************************
 * Function : GetIntegerRegValue
 *
 * Description: Gets integer value of register regName
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
int GetIntegerRegValue(handle hReg)
{
    static s_acc_value val = {accIntVal};       /* value  */

    /* set register value */
    acc_fetch_value(hReg, "%%", &val);

    return val.value.integer;
}




/***************************************************************************
 * Function : SW_Init
 *
 * Description: Software part of PLI function pair to initialize
 *                PLI engine, shall be called when demo starts
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_PL_Init ()
{
    int num;


    /*Check that this is first call of initialization routine*/
    if (rio_Pl_Wrap_Initialized == RIO_TRUE)
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Warning", "PL Wrapper alredy initialized");    
        return;
    }
    else
    /*set flag for notifying that wrapper is initialized*/
        rio_Pl_Wrap_Initialized = RIO_TRUE;

    acc_initialize();
#ifndef PL_WRAP_SERIAL_ONLY
    if(RIO_PL_Parallel_Model_Get_Function_Tray( &Rio_Pl_Model_Ftray_T_Tray ) != RIO_OK)
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", "RIO_PL_Model_Get_Function_Tray - not OK");
    }
    Initialize_CB_Tray_Pl();
#endif

#ifndef PL_WRAP_PAR_ONLY
    if(RIO_PL_Serial_Model_Get_Function_Tray ( &Rio_Pl_Serial_Model_Ftray_T_Tray ) != RIO_OK)
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", "RIO_PL_Serial_Model_Get_Function_Tray - not OK");
    }
    Initialize_Serial_CB_Tray_Pl();
#endif

/*reset initial values*/
    Pl_Max_Inst_Num = 0;
    Pl_Inst_Num = 0;


/*Allocation memory for contexts*/
    num = acc_fetch_tfarg_int(1);
    if (num < 0) 
    {
        SW_Pl_Wrapper_Message (RIO_PL_ABSENT_INST_ID, "Error", "SW_Init, Negative number of instances");
        abort();
    }
    Pl_Max_Inst_Num = num;
    Contexts_Pl = (RIO_CONTEXT_PL_T *) malloc( num * sizeof( RIO_CONTEXT_PL_T ) );
    if ( Contexts_Pl == 0) 
    {
        SW_Pl_Wrapper_Message (RIO_PL_ABSENT_INST_ID, "Error", "SW_Init, Error during memory allocation");
        abort();
    }

}


/***************************************************************************
 * Function : SW_Close
 *
 * Description: Software part of PLI function pair to free internal
 *                PLI structures, shall be called when demo finishes
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_PL_Close ()
{
    if (rio_Pl_Wrap_Initialized == RIO_FALSE)
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Warning", "PL Wrapper alredy closed");    
        return;
    }
    acc_close();
    free ( Contexts_Pl );
    /*reset the flag*/
    rio_Pl_Wrap_Initialized = RIO_FALSE;
}


/***************************************************************************
 * Function : SW_Rio_PL_Clock_Edge
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Clock_Edge routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Clock_Edge routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Clock_Edge ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    /*Check is instance with the current Inst_ID created of no*/
    if ((instId < 0) || (instId >= Pl_Inst_Num))
        SW_Pl_Wrapper_Message (instId, "Warning", "Clock_Edge is called for non-existing instance");
    else
    {
#ifndef PL_WRAP_SERIAL_ONLY
        if (PL_IN_PAR_MODE)
        {
            (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Clock_Edge))( Contexts_Pl[instId].handle);
            return;
        }
#endif
#ifndef PL_WRAP_PAR_ONLY
        if (PL_IN_SERIAL_MODE)
        {
            (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Clock_Edge))( Contexts_Pl[instId].handle);
            return;
        }
#endif
    }
}

/***************************************************************************
 * Function : SW_Rio_PL_Clock_Edge_New
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Clock_Edge routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Clock_Edge_New routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Clock_Edge_New ()
{
    int i_Count;
    int instId;  

#ifndef PL_WRAP_PAR_ONLY
    set_Serial_Pins_10bit_Flag_Reg_PL = RIO_FALSE;
    if (PL_IN_SERIAL_MODE)
        {
            ten_Bit_Interface_PL = RIO_TRUE;
            for (i_Count =1; i_Count <= 10; i_Count++)
            {
                SW_Rio_PL_Clock_Edge ();

            }
            set_Serial_Pins_10bit_Flag_Reg_PL = RIO_TRUE; 
        }
#endif


}


/***************************************************************************
 * Function : SW_Rio_PL_GSM_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_GSM_Request routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_GSM_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_GSM_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_PL_GSM_TTYPE_T ttype;
    RIO_GSM_REQ_T rq;
    RIO_UCHAR_T sec_ID;
    RIO_UCHAR_T sec_TID;
    RIO_TAG_T trx_Tag;
    RIO_UCHAR_T multicast_Size;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    ttype = ( RIO_PL_GSM_TTYPE_T ) acc_fetch_tfarg_int( k + 2 ); 
/*Gets fields of the rq structure*/
    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.extended_Address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.xamsbs = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.offset = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.byte_Num = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 );  
    rq.data = Contexts_Pl[instId].bufs.rio_PL_GSM_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    sec_ID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 10 ); 
    sec_TID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 11 ); 
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 12 ); 
    multicast_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 13 ); 
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int( k + 14 );

#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_GSM_Request))( Contexts_Pl[instId].handle,
            ttype , &rq , sec_ID , sec_TID , trx_Tag , 
            Contexts_Pl[instId].bufs.rio_PL_GSM_Request_Transport_Info , multicast_Size ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_GSM_Request))( Contexts_Pl[instId].handle,
            ttype , &rq , sec_ID , sec_TID , trx_Tag , 
            Contexts_Pl[instId].bufs.rio_PL_GSM_Request_Transport_Info , multicast_Size ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_PL_IO_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_IO_Request routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_IO_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_IO_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_IO_REQ_T rq;
    RIO_TAG_T trx_Tag;
    RIO_TR_INFO_T transport_Info;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.ttype = ( RIO_IO_TTYPE_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.extended_Address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.xamsbs = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.offset = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.byte_Num = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 );  
    rq.data = Contexts_Pl[instId].bufs.rio_PL_IO_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 10 ); 
    transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 11 ); 
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int( k + 12 );

#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_IO_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag , transport_Info ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_IO_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag , transport_Info ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);

}


/***************************************************************************
 * Function : SW_Rio_PL_Message_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Message_Request routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Message_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Message_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_MESSAGE_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.msglen = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.ssize = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.letter = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.mbox = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.msgseg = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 );  
    rq.data = Contexts_Pl[instId].bufs.rio_PL_Message_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 10 );
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int( k + 11 ); 


#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Message_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Message_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);

}


/***************************************************************************
 * Function : SW_Rio_PL_Doorbell_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Doorbell_Request routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Doorbell_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Doorbell_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_DOORBELL_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.doorbell_Info = ( RIO_DB_INFO_T ) acc_fetch_tfarg_int( k + 4 ); 
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 5 );
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int( k + 6 ); 


#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Doorbell_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Doorbell_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_PL_Configuration_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Configuration_Request routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Configuration_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Configuration_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_CONFIG_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.hop_Count = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.rw = ( RIO_CONF_TTYPE_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.config_Offset = ( RIO_CONF_OFFS_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.sub_Dw_Pos = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 );  
    rq.data = Contexts_Pl[instId].bufs.rio_PL_Configuration_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 9 );
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int( k + 10 ); 


#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Configuration_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Configuration_Request))( Contexts_Pl[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_PL_Packet_Struct_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Packet_Struct_Request routine in the RIO_PL_SERIAL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Packet_Struct_Request routine work
 *
 * Notes: 
 *
 **************************************************************************/
void SW_Rio_PL_Packet_Struct_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_MODEL_PACKET_STRUCT_RQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

 /*check that instance id is correct*/
    Wrap_Check_Id_Ret(instId,"Error in the Rio_PL_Packet_Struct_Request - wrapper isn't initialized");

    rq.packet.ack_ID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.packet.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.packet.rsrv_Phy_1 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.packet.rsrv_Phy_2 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.packet.tt = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.packet.ftype = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.packet.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.packet.ttype = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 ); 
    rq.packet.rdwr_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 10 ); 
    rq.packet.srtr_TID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 11 ); 
    rq.packet.hop_Count = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 12 ); 
    rq.packet.config_Offset = ( long ) acc_fetch_tfarg_int( k + 13 ); 
    rq.packet.wdptr = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 14 ); 
    rq.packet.status = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 15 ); 
    rq.packet.doorbell_Info = ( int ) acc_fetch_tfarg_int( k + 16 ); 
    rq.packet.msglen = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 17 ); 
    rq.packet.letter = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 18 ); 
    rq.packet.mbox = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 19 ); 
    rq.packet.msgseg = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 20 ); 
    rq.packet.ssize = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 21 ); 
    rq.packet.address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 22 ); 
    rq.packet.extended_Address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 23 ); 
    rq.packet.xamsbs = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 24 ); 
    rq.packet.ext_Address_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 25 ); 
    rq.packet.ext_Address_16 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 26 ); 
    rq.packet.rsrv_Ftype6 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 27 ); 
    rq.packet.rsrv_Ftype8 = ( long ) acc_fetch_tfarg_int( k + 28 ); 
    rq.packet.rsrv_Ftype10 = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 29 ); 
/*    rq.packet.crc = ( int ) acc_fetch_tfarg_int( k + 30 ); 
    rq.packet.is_Crc_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 31 ); */
    k -= 2;
    rq.intermediate_Crc = ( long ) acc_fetch_tfarg_int( k + 32 ); 
    rq.is_Int_Crc_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 33 ); 
    rq.crc = ( long ) acc_fetch_tfarg_int( k + 34 ); 
    rq.is_Crc_Valid = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 35 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 36 );  
    rq.data = Contexts_Pl[instId].bufs.Rio_PL_Packet_Struct_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 37 ); 

#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0,(*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Packet_Struct_Request))( Contexts_Pl[instId].handle,
         &rq , trx_Tag));
    }
#endif

#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0,(*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Packet_Struct_Request))( Contexts_Pl[instId].handle,
         &rq , trx_Tag));
    }
#endif

}

/***************************************************************************
 * Function : SW_Rio_PL_Send_Response
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Send_Response routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Send_Response routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Send_Response ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_TAG_T tag;
    RIO_RESPONSE_T resp;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 2 ); 
/*Gets fields of the resp structure*/
    resp.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    resp.ftype = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    resp.transaction = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 5 ); 
    resp.status = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    resp.tr_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 7 ); 
    resp.src_ID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
    resp.target_TID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 ); 
    resp.sec_ID = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 10 ); 
    resp.sec_TID = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 11 ); 
    resp.dw_Num = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 12 );  
    resp.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int( k + 13 );
    resp.data = Contexts_Pl[instId].bufs.rio_PL_Send_Response_Resp_Data; /*reading from internal buffer*/
/*End of the fields value getting for the resp structure*/


#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Send_Response))( Contexts_Pl[instId].handle,
            tag , &resp ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Send_Response))( Contexts_Pl[instId].handle,
            tag , &resp ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_PL_Ack_Remote_Req
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Ack_Remote_Req routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Ack_Remote_Req routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Ack_Remote_Req ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_TAG_T tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 2 ); 


#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Ack_Remote_Req))( Contexts_Pl[instId].handle,    tag ));
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Ack_Remote_Req))( Contexts_Pl[instId].handle,    tag ));
#endif
}


/***************************************************************************
 * Function : SW_Rio_PL_Set_Config_Reg
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Set_Config_Reg routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Set_Config_Reg routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Set_Config_Reg ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_CONF_OFFS_T config_Offset;
    unsigned long data;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    config_Offset = ( RIO_CONF_OFFS_T ) acc_fetch_tfarg_int( k + 2 ); 
    data = ( unsigned long ) acc_fetch_tfarg_int( k + 3 ); 


#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Set_Config_Reg))( Contexts_Pl[instId].handle,
            config_Offset , data ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Set_Config_Reg))( Contexts_Pl[instId].handle,
            config_Offset , data ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_PL_Get_Config_Reg
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Get_Config_Reg routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Get_Config_Reg routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Get_Config_Reg ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_CONF_OFFS_T config_Offset;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    config_Offset = ( RIO_CONF_OFFS_T ) acc_fetch_tfarg_int( k + 2 ); 


#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Get_Config_Reg))( Contexts_Pl[instId].handle,
            config_Offset , &(Contexts_Pl[instId].bufs.rio_PL_Get_Config_Reg_Data)  ));
        return;
    }
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Get_Config_Reg))( Contexts_Pl[instId].handle,
            config_Offset , &(Contexts_Pl[instId].bufs.rio_PL_Get_Config_Reg_Data)  ));
        return;
    }
#endif
    tf_putp(0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_PL_Model_Start_Reset
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Model_Start_Reset routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Model_Start_Reset routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Model_Start_Reset ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);



#ifndef PL_WRAP_SERIAL_ONLY
    if (PL_IN_PAR_MODE)
        (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Model_Start_Reset))( Contexts_Pl[instId].handle);
#endif
#ifndef PL_WRAP_PAR_ONLY
    if (PL_IN_SERIAL_MODE)
        (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Model_Start_Reset))( Contexts_Pl[instId].handle);
#endif
}



/***************************************************************************
 * Function : SW_Rio_PL_Set_Retry_Generator
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Set_Retry_Generator routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Set_Retry_Generator routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Set_Retry_Generator()
{
    int k;
    int instId;
    unsigned int retry_Factor;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    retry_Factor = (unsigned int)acc_fetch_tfarg_int( k + 2 );

#ifndef PL_WRAP_SERIAL_ONLY
        if (PL_IN_PAR_MODE)
            (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Set_Retry_Generator))( Contexts_Pl[instId].handle, retry_Factor);
#endif 
#ifndef PL_WRAP_PAR_ONLY
        if (PL_IN_SERIAL_MODE)
            (*(Rio_Pl_Serial_Model_Ftray_T_Tray.rio_PL_Set_Retry_Generator))( Contexts_Pl[instId].handle, retry_Factor);
#endif


}


/*****************************************************************************/
/*     Callbacks implementation                                              */
/*****************************************************************************/


/***************************************************************************
 * Function : SW_Rio_PL_Ack_Request
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Ack_Request
 *
 * Returns: result of Rio_PL_Ack_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Ack_Request (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Ack_Request_Tag_Reg, (int)tag);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Ack_Request_Flag_Reg, 1);
    return 0;
}

/***************************************************************************
 * Function : SW_Rio_PL_Ack_Response
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Ack_Response
 *
 * Returns: result of Rio_PL_Ack_Response routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Ack_Response (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Ack_Response_Tag_Reg, (int)tag);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Ack_Response_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_PL_Req_Time_Out
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Req_Time_Out
 *
 * Returns: result of Rio_PL_Req_Time_Out routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Req_Time_Out (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Req_Time_Out_Tag_Reg, (int)tag);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Req_Time_Out_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_PL_Remote_Request
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Remote_Request
 *
 * Returns: result of Rio_PL_Remote_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_REMOTE_REQUEST_T *req,
    RIO_TR_INFO_T transport_Info )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Tag_Reg, (int)tag);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Packet_Type_Reg, (int)req->packet_Type);
/*header - pointer to a structure so necessary pass all it fields*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Target_TID, (int)req->header->target_TID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Src_ID, (int)req->header->src_ID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Prio, (int)req->header->prio);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Wdptr, (int)req->header->wdptr);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Rdsize, (int)req->header->rdsize);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Ftype, (int) req->header->format_Type);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Ttype, (int)req->header->ttype);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Address, (int)req->header->address);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Extended_Address, (int)req->header->extended_Address);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Xamsbs, (int)req->header->xamsbs);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Sec_ID, (int)req->header->sec_ID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Sec_TID, (int)req->header->sec_TID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Mbox, (int)req->header->mbox);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Letter, (int)req->header->letter);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Msgseg, (int)req->header->msgseg);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Ssize, (int)req->header->ssize);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Msglen, (int)req->header->msglen);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Doorbell_Info, (int)req->header->doorbell_Info);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Offset, (int)req->header->offset);
/*end of header structure parsing*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Dw_Num_Reg, (int)req->dw_Num);
    memcpy (Contexts_Pl[instId].bufs.rio_PL_Remote_Request_Req_Data, req->data, req->dw_Num * sizeof(RIO_DW_T) );
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Transp_Type_Reg, (int)req->transp_Type);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Transport_Info_Reg, (int)transport_Info);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Request_Flag_Reg, 1);
    return 0;
}


/***************************************************************************
 * Function : SW_Rio_PL_Remote_Response
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Remote_Response
 *
 * Returns: result of Rio_PL_Remote_Response routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Remote_Response (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_RESPONSE_T *resp )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Tag_Reg, (int)tag);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Prio_Reg, (int)resp->prio);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Ftype_Reg, (int)resp->ftype);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Transaction_Reg, (int)resp->transaction);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Status_Reg, (int)resp->status);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Tr_Info_Reg, (int)resp->tr_Info);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Src_ID_Reg, (int)resp->src_ID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Target_TID_Reg, (int)resp->target_TID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Sec_ID_Reg, (int)resp->sec_ID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Sec_TID_Reg, (int)resp->sec_TID);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Dw_Num_Reg, (int)resp->dw_Num);
    memcpy (Contexts_Pl[instId].bufs.rio_PL_Remote_Response_Resp_Data, resp->data, resp->dw_Num * sizeof(RIO_DW_T) );
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Remote_Response_Flag_Reg, 1);
    return 0;
}

/***************************************************************************
 * Function : SW_Extend_Reg_Write
 *
 * Description:  Implementation of RIO callback function 
 *              Extend_Reg_Write
 *
 * Returns: result of Extend_Reg_Write routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Extend_Reg_Write (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long data )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_Pl[instId].regs.extend_Reg_Write_Config_Offset_Reg, (int)config_Offset);
    SetIntegerRegValue(Contexts_Pl[instId].regs.extend_Reg_Write_Data_Reg, (int)data);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.extend_Reg_Write_Flag_Reg, 1);
    return 0;
}


/*****************************************************************************/
/*     End of callbacks implementation                                       */
/*****************************************************************************/






/***************************************************************************
 * Function : SW_Put_Data
 *
 * Description: Routine for access to internal buffers
 *
 * Returns:  
 *
 * Notes: The routines is generated by WrapGen
 *
 **************************************************************************/
void SW_Put_Data_Pl()
{
    int instId = acc_fetch_tfarg_int(1);
    PUT_IND_T type = (PUT_IND_T) acc_fetch_tfarg_int(2);    /*Type of the array*/
    int index = acc_fetch_tfarg_int(3);    /*Index in the array*/

    if ( (index >= PLI_MAX_ARRAY_SIZE) || (index < 0) )
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", "Put_Data - index incorrect");
        return;
    }

/*The following macro checks that passed instance ID 
    is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    switch (type)
    {
        case RIO_PL_GSM_REQUEST_RQ_DATA:
            Contexts_Pl[instId].bufs.rio_PL_GSM_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pl[instId].bufs.rio_PL_GSM_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_PL_GSM_REQUEST_TRANSPORT_INFO:
            Contexts_Pl[instId].bufs.rio_PL_GSM_Request_Transport_Info[index] = acc_fetch_tfarg_int(4);
            break;
        case RIO_PL_IO_REQUEST_RQ_DATA:
            Contexts_Pl[instId].bufs.rio_PL_IO_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pl[instId].bufs.rio_PL_IO_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_PL_MESSAGE_REQUEST_RQ_DATA:
            Contexts_Pl[instId].bufs.rio_PL_Message_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pl[instId].bufs.rio_PL_Message_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_PL_CONFIGURATION_REQUEST_RQ_DATA:
            Contexts_Pl[instId].bufs.rio_PL_Configuration_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pl[instId].bufs.rio_PL_Configuration_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_PL_PACKET_STRUCT_REQUEST_RQ_DATA:
            Contexts_Pl[instId].bufs.Rio_PL_Packet_Struct_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pl[instId].bufs.Rio_PL_Packet_Struct_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_PL_SEND_RESPONSE_RESP_DATA:
            Contexts_Pl[instId].bufs.rio_PL_Send_Response_Resp_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pl[instId].bufs.rio_PL_Send_Response_Resp_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        default:
            SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", "The internal buffer with specified number is not exist");
    }
}


/***************************************************************************
 * Function : SW_Get_Data_Pl
 *
 * Description: Routine for access to internal buffers
 *
 * Returns:  
 *
 * Notes: The routines is generated by WrapGen
 *
 **************************************************************************/
void SW_Get_Data_Pl()
{
    int instId = acc_fetch_tfarg_int(1);
    GET_IND_T type = (GET_IND_T) acc_fetch_tfarg_int(2);    /*Type of the array*/
    int index;                                                /*Index in the array*/
    WORD_POS_T pos;                                            /*Least or most significant word*/

    if (type == RIO_PL_GET_CONFIG_REG_DATA)
    {
        tf_putp(0, Contexts_Pl[instId].bufs.rio_PL_Get_Config_Reg_Data);
        return;
    }

/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    index = acc_fetch_tfarg_int(3);
    pos = (WORD_POS_T)acc_fetch_tfarg_int(4);

    if ( (index >= PLI_MAX_ARRAY_SIZE) || (index < 0) )
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", "Get_Data - index incorrect");
        return;
    }

    switch (type)
    {
        case RIO_PL_REMOTE_REQUEST_REQ_DATA:
            if (pos == MSW) tf_putp(0, Contexts_Pl[instId].bufs.rio_PL_Remote_Request_Req_Data[index].ms_Word);
            else tf_putp(0, Contexts_Pl[instId].bufs.rio_PL_Remote_Request_Req_Data[index].ls_Word);
            break;
        case RIO_PL_REMOTE_RESPONSE_RESP_DATA:
            if (pos == MSW) tf_putp(0, Contexts_Pl[instId].bufs.rio_PL_Remote_Response_Resp_Data[index].ms_Word);
            else tf_putp(0, Contexts_Pl[instId].bufs.rio_PL_Remote_Response_Resp_Data[index].ls_Word);
            break;
        default:
            SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", "The internal callback's buffer with specified number is absent");
    }
}
