/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: M:\zkg_rio\riosuite\src\verilog_env\pliwrap\pl\module_pl.v $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Module for RIO Models
*
* Notes:
*
******************************************************************************/


`include "wrapper_pl.vh"


module PL_MODULE (clock, tclk, tframe, td, tdl, rclk, rframe, rd, rdl); 


    input clock; 

    input rclk;
    input rframe;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rd;
    input [0 : `RIO_LP_EP_WIRE_WIDTH - 1] rdl;

    output tclk;
    output tframe;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td;
    output [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl;

    reg tclk_reg;
    reg tframe_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] td_reg;
    reg [0 : `RIO_LP_EP_WIRE_WIDTH - 1] tdl_reg;

    assign tclk = tclk_reg;
    assign tframe = tframe_reg;
    assign td = td_reg;
    assign tdl = tdl_reg;

//working mode
    parameter mode = `PL_INST_PAR_MODE;

`include "pli_pl_registers.vh" 

    integer inst_Id;

/* Begin of example how to call routine Rio_PL_GSM_Request
result = $HW_Rio_PL_GSM_Request( inst_Id,
    ttype,
    rq_prio,
    rq_address,
    rq_extended_address,
    rq_xamsbs,
    rq_dw_size,
    rq_offset,
    rq_byte_num,
    sec_id,
    sec_tid,
    trx_tag,
    multicast_size);
End of example how to call routine Rio_PL_GSM_Request*/


/* Begin of example how to call routine Rio_PL_IO_Request
result = $HW_Rio_PL_IO_Request( inst_Id,
    rq_ttype,
    rq_prio,
    rq_address,
    rq_extended_address,
    rq_xamsbs,
    rq_dw_size,
    rq_offset,
    rq_byte_num,
    trx_tag,
    transport_info);
End of example how to call routine Rio_PL_IO_Request*/


/* Begin of example how to call routine Rio_PL_Message_Request
result = $HW_Rio_PL_Message_Request( inst_Id,
    rq_transport_info,
    rq_prio,
    rq_msglen,
    rq_ssize,
    rq_letter,
    rq_mbox,
    rq_msgseg,
    rq_dw_size,
    trx_tag);
End of example how to call routine Rio_PL_Message_Request*/


/* Begin of example how to call routine Rio_PL_Doorbell_Request
result = $HW_Rio_PL_Doorbell_Request( inst_Id,
    rq_prio,
    rq_transport_info,
    rq_doorbell_info,
    trx_tag);
End of example how to call routine Rio_PL_Doorbell_Request*/


/* Begin of example how to call routine Rio_PL_Configuration_Request
result = $HW_Rio_PL_Configuration_Request( inst_Id,
    rq_prio,
    rq_transport_info,
    rq_hop_count,
    rq_rw,
    rq_config_offset,
    rq_dw_size,
    rq_sub_dw_pos,
    trx_tag);
End of example how to call routine Rio_PL_Configuration_Request*/


/* Begin of example how to call routine Rio_PL_Send_Response
result = $HW_Rio_PL_Send_Response( inst_Id,
    tag,
    resp_prio,
    resp_ftype,
    resp_transaction,
    resp_status,
    resp_tr_info,
    resp_src_id,
    resp_target_tid,
    resp_sec_id,
    resp_sec_tid,
    resp_dw_num);
End of example how to call routine Rio_PL_Send_Response*/


/* Begin of example how to call routine Rio_PL_Ack_Remote_Req
result = $HW_Rio_PL_Ack_Remote_Req( inst_Id, tag);
End of example how to call routine Rio_PL_Ack_Remote_Req*/


/* Begin of example how to call routine Rio_PL_Set_Config_Reg
result = $HW_Rio_PL_Set_Config_Reg( inst_Id,
    config_offset,
    data);
End of example how to call routine Rio_PL_Set_Config_Reg*/


/* Begin of example how to call routine Rio_PL_Get_Config_Reg
result = $HW_Rio_PL_Get_Config_Reg( inst_Id, config_offset);
value = $HW_Get_Data_Pl(inst_Id, `RIO_PL_GET_CONFIG_REG_DATA);
End of example how to call routine Rio_PL_Get_Config_Reg*/


/* Begin of example how to call routine Rio_PL_Model_Start_Reset
$HW_Rio_PL_Model_Start_Reset( inst_Id);
End of example how to call routine Rio_PL_Model_Start_Reset*/


/* Begin of example how to call routine Rio_PL_Model_Initialize
result = $HW_Rio_PL_Model_Initialize( inst_Id,
    param_set_transp_type,
    param_set_dev_id,
    param_set_lcshbar,
    param_set_lcsbar,
    param_set_is_host,
    param_set_is_master_enable,
    param_set_is_discovered,
    param_set_ext_address,
    param_set_ext_address_16,
    param_set_lp_ep_is_8,
    param_set_work_mode_is_8,
    param_set_input_is_enable,
    param_set_output_is_enable,
    param_set_num_trainings,
    param_set_requires_window_alignment,
    param_set_receive_idle_lim,
    param_set_throttle_idle_lim,
    param_set_res_for_3_out,
    param_set_res_for_2_out,
    param_set_res_for_1_out,
    param_set_res_for_3_in,
    param_set_res_for_2_in,
    param_set_res_for_1_in,
    param_set_pass_by_prio);
End of example how to call routine Rio_PL_Model_Initialize*/

/* Begin of example how to access to internal task's buffer

    value = $HW_Get_Data_Pl(inst_Id, `RIO_PL_GET_CONFIG_REG_DATA);

    value_msw = $HW_Get_Data_Pl(inst_Id, Buffer_Name, index, MSW);
    value_lsw = $HW_Get_Data_Pl(inst_Id, Buffer_Name, index, LSW);

    Buffer_Name can be:
         `RIO_PL_REMOTE_REQUEST_REQ_DATA;
         `RIO_PL_REMOTE_RESPONSE_RESP_DATA;
End of example how to access to internal task's buffer*/


/* Begin of example how to access to internal routines buffer

    $HW_Put_Data_Pl(`RIO_PL_GSM_REQUEST_TRANSPORT_INFO, index, tr_info);


    $HW_Put_Data_Pl(type, index, ms_Word, ls_Word);
      where type can be :
        `RIO_PL_GSM_REQUEST_RQ_DATA
        `RIO_PL_IO_REQUEST_RQ_DATA
        `RIO_PL_MESSAGE_REQUEST_RQ_DATA
        `RIO_PL_CONFIGURATION_REQUEST_RQ_DATA
        `RIO_PL_SEND_RESPONSE_RESP_DATA
End of example how to access to internal callback buffer*/



/*Clock edge driver*/
always @(clock)
begin
     $HW_Rio_PL_Clock_Edge(inst_Id);
end 

/* Input LP-EP clock handler */
always @(rclk)
if (mode == `PL_INST_PAR_MODE)
begin
    $HW_Rio_PL_Get_Pins( inst_Id, rframe, rd, rdl, rclk);
end

/*realization of Rio_PL_Ack_Request task*/
always
begin
wait (rio_pl_ack_request_flag_reg === 1'b1);
    rio_pl_ack_request_flag_reg = 1'b0;

    /* something_variable =rio_pl_ack_request_tag_reg;*/

    /*Insert code for realization Rio_PL_Ack_Request task here*/
    $display("The Rio_PL_Ack_Request task is invoked");
end


/*realization of Rio_PL_Ack_Response task*/
always
begin
wait (rio_pl_ack_response_flag_reg === 1'b1);
    rio_pl_ack_response_flag_reg = 1'b0;
    
    /* something_variable = rio_pl_ack_response_tag_reg;*/

     /*Insert code for realization Rio_PL_Ack_Response task here*/
     $display("The Rio_PL_Ack_Response task is invoked");
end


/*realization of Rio_PL_Req_Time_Out task*/
always
begin
wait (rio_pl_req_time_out_flag_reg === 1'b1);
    rio_pl_req_time_out_flag_reg = 1'b0;

    /* something_variable = rio_pl_req_time_out_tag_reg;*/
     /*Insert code for realization Rio_PL_Req_Time_Out task here*/
     $display("The Rio_PL_Req_Time_Out task is invoked");
end


/*realization of Rio_PL_Remote_Request task*/
always
begin
wait (rio_pl_remote_request_flag_reg === 1'b1);
    rio_pl_remote_request_flag_reg = 1'b0;

    /* something_variable = rio_pl_remote_request_tag_reg*/
    /* something_variable = rio_pl_remote_request_packet_type_reg;*/ 
    /* something_variable = rio_pl_remote_request_req_target_tid*/
    /* something_variable = rio_pl_remote_request_req_src_id*/
    /* something_variable = rio_pl_remote_request_req__prio*/
    /* something_variable = rio_pl_remote_request_req_wdptr*/
    /* something_variable = rio_pl_remote_request_req_rdsize*/
    /* something_variable = rio_pl_remote_request_req_ftype*/
    /* something_variable = rio_pl_remote_request_req_ttype*/
    /* something_variable = rio_pl_remote_request_req_address*/
    /* something_variable = rio_pl_remote_request_req_extended_address*/
    /* something_variable = rio_pl_remote_request_req_xamsbs*/
    /* something_variable = rio_pl_remote_request_req_sec_id*/
    /* something_variable = rio_pl_remote_request_req_sec_tid*/
    /* something_variable = rio_pl_remote_request_req_mbox*/
    /* something_variable = rio_pl_remote_request_req_letter*/
    /* something_variable = rio_pl_remote_request_req_msgseg*/
    /* something_variable = rio_pl_remote_request_req_ssize*/
    /* something_variable = rio_pl_remote_request_req_msglen*/
    /* something_variable = rio_pl_remote_request_req_doorbell_info*/
    /* something_variable = rio_pl_remote_request_req_offset*/

    /* something_variable = rio_pl_remote_request_dw_num_reg;*/ 
    /* something_variable = rio_pl_remote_request_transp_type_reg;*/ 
    /* something_variable = rio_pl_remote_request_transport_info_reg;*/ 

     /*Insert code for realization Rio_PL_Remote_Request task here*/
     $display("The Rio_PL_Remote_Request task is invoked");
end


/*realization of Rio_PL_Remote_Response task*/
always
begin
wait (rio_pl_remote_response_flag_reg === 1'b1);
    rio_pl_remote_response_flag_reg = 1'b0;

    /* something_variable = rio_pl_remote_response_tag_reg;*/ 
    /* something_variable = rio_pl_remote_response_prio_reg;*/ 
    /* something_variable = rio_pl_remote_response_ftype_reg;*/ 
    /* something_variable = rio_pl_remote_response_transaction_reg;*/ 
    /* something_variable = rio_pl_remote_response_status_reg;*/ 
    /* something_variable = rio_pl_remote_response_tr_info_reg;*/ 
    /* something_variable = rio_pl_remote_response_src_id_reg;*/ 
    /* something_variable = rio_pl_remote_response_target_tid_reg;*/ 
    /* something_variable = rio_pl_remote_response_sec_id_reg;*/ 
    /* something_variable = rio_pl_remote_response_sec_tid_reg;*/ 
    /* something_variable = rio_pl_remote_response_dw_num_reg;*/ 

     /*Insert code for realization Rio_PL_Remote_Response task here*/
     $display("The Rio_PL_Remote_Response task is invoked");
end


/*realization of Rio_PL_Set_Pins task*/
always
begin
wait (rio_pl_set_pins_flag_reg === 1'b1);
    rio_pl_set_pins_flag_reg = 1'b0;

    tframe_reg =  rio_pl_set_pins_tframe_reg;
    td_reg = rio_pl_set_pins_td_reg;
    tdl_reg = rio_pl_set_pins_tdl_reg;
    tclk_reg = rio_pl_set_pins_tclk_reg;
end


/*realization of Extend_Reg_Write task*/
always
begin
wait (extend_reg_write_flag_reg === 1'b1);
    extend_reg_write_flag_reg = 1'b0;
                 
    /* something_variable = extend_reg_write_config_offset_reg;*/ 
    /* something_variable = extend_reg_write_data_reg;*/ 

     /*Insert code for realization Extend_Reg_Write task here*/
     $display("The Extend_Reg_Write task is invoked");
end


/***************************************************************************
 * Description: Creates instance of modles
 *
 * Notes: 
 *
 **************************************************************************/
initial
if (mode == `PL_INST_PAR_MODE)
begin
    #1;
    inst_Id = $HW_RIO_PL_Model_Create_Instance (
// ,
    param_ext_address_support,
    param_is_bridge,
    param_has_memory,
    param_has_processor,
    param_coh_granule_size_32,
    param_coh_domain_size,
    param_device_identity,
    param_device_vendor_identity,
    param_device_rev,
    param_device_minor_rev,
    param_device_major_rev,
    param_assy_identity,
    param_assy_vendor_identity,
    param_assy_rev,
    param_entry_extended_features_ptr,
    param_next_extended_features_ptr,
    param_source_trx,
    param_dest_trx,
    param_mbox1,
    param_mbox2,
    param_mbox3,
    param_mbox4,
    param_has_doorbell,
    param_pl_inbound_buffer_size,
    param_pl_outbound_buffer_size,
    param_ll_inbound_buffer_size,
    param_ll_outbound_buffer_size,
    /*registers handlers passing*/
    rio_pl_ack_request_tag_reg,
    rio_pl_ack_request_flag_reg,
    rio_pl_ack_response_tag_reg,
    rio_pl_ack_response_flag_reg,
    rio_pl_req_time_out_tag_reg,
    rio_pl_req_time_out_flag_reg,
    rio_pl_remote_response_tag_reg,
    rio_pl_remote_response_prio_reg,
    rio_pl_remote_response_ftype_reg,
    rio_pl_remote_response_transaction_reg,
    rio_pl_remote_response_status_reg,
    rio_pl_remote_response_tr_info_reg,
    rio_pl_remote_response_src_id_reg,
    rio_pl_remote_response_target_tid_reg,
    rio_pl_remote_response_sec_id_reg,
    rio_pl_remote_response_sec_tid_reg,
    rio_pl_remote_response_dw_num_reg,
    rio_pl_remote_response_flag_reg,
    rio_pl_set_pins_tframe_reg,
    rio_pl_set_pins_td_reg,
    rio_pl_set_pins_tdl_reg,
    rio_pl_set_pins_tclk_reg,
    rio_pl_set_pins_flag_reg,
    extend_reg_write_config_offset_reg,
    extend_reg_write_data_reg,
    extend_reg_write_flag_reg,

    rio_pl_remote_request_tag_reg,
    rio_pl_remote_request_packet_type_reg,
    rio_pl_remote_request_dw_num_reg,
    rio_pl_remote_request_transp_type_reg,
    rio_pl_remote_request_transport_info_reg,
    rio_pl_remote_request_flag_reg,

    rio_pl_remote_request_req_target_tid,
    rio_pl_remote_request_req_src_id,
    rio_pl_remote_request_req_prio,
    rio_pl_remote_request_req_wdptr,
    rio_pl_remote_request_req_rdsize,
    rio_pl_remote_request_req_ftype,
    rio_pl_remote_request_req_ttype,
    rio_pl_remote_request_req_address,
    rio_pl_remote_request_req_extended_address,
    rio_pl_remote_request_req_xamsbs,
    rio_pl_remote_request_req_sec_id,
    rio_pl_remote_request_req_sec_tid,
    rio_pl_remote_request_req_mbox,
    rio_pl_remote_request_req_letter,
    rio_pl_remote_request_req_msgseg,
    rio_pl_remote_request_req_ssize,
    rio_pl_remote_request_req_msglen,
    rio_pl_remote_request_req_doorbell_info,
    rio_pl_remote_request_req_offset);
end


/***************************************************************************
 * Description: Reset flag registers
 *
 * Notes: 
 *
 **************************************************************************/
initial
begin
    rio_pl_ack_request_flag_reg = 1'b0;
    rio_pl_get_req_data_flag_reg = 1'b0;
    rio_pl_ack_response_flag_reg = 1'b0;
    rio_pl_get_resp_data_flag_reg = 1'b0;
    rio_pl_req_time_out_flag_reg = 1'b0;
    rio_pl_remote_request_flag_reg = 1'b0;
    rio_pl_remote_response_flag_reg = 1'b0;
    rio_pl_set_pins_flag_reg = 1'b0;
    extend_reg_write_flag_reg = 1'b0;
end


endmodule
