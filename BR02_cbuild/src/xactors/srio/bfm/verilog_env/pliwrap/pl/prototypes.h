/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\prototypes.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Description: Prototypes of all wrapper functions
*
* Notes:       Prototype of this file is generated automaticaly by WrapGen
*
******************************************************************************/
#ifndef PROTOTYPES_PL_H
#define PROTOTYPES_PL_H

void SW_Pl_Wrapper_Message (int id, char *type, char *msg); /*simple error handler */

/*routine for access to registers*/
int SW_Rio_Register_Access(int instId, int config_Offset, unsigned long *data,  REG_ACCESS_T acc_Type); 

void SetIntegerRegValue(handle hReg, int value);
int GetIntegerRegValue(handle hReg);
void SW_Pl_Init ();    /*Initialize wrapper*/
void SW_Pl_Close ();    /*Closes pli engine*/
void SW_Rio_PL_Clock_Edge ();
void SW_Rio_PL_GSM_Request ();
void SW_Rio_PL_IO_Request ();
void SW_Rio_PL_Message_Request ();
void SW_Rio_PL_Doorbell_Request ();
void SW_Rio_PL_Configuration_Request ();
void SW_Rio_PL_Send_Response ();
void SW_Rio_PL_Ack_Remote_Req ();
void SW_Rio_PL_Set_Config_Reg ();
void SW_Rio_PL_Get_Config_Reg ();
void SW_Rio_PL_Model_Start_Reset ();
void SW_Rio_PL_Model_Initialize ();
void SW_Rio_PL_Get_Pins ();
void SW_Rio_PL_Clock_Edge_New ();
void SW_Rio_PL_Get_Pins_New ();
/* Callbacks */
int SW_Rio_PL_Ack_Request (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag );

int SW_Rio_PL_Get_Req_Data (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_UCHAR_T offset,
    RIO_UCHAR_T cnt,
    RIO_DW_T *data );

int SW_Rio_PL_Ack_Response (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag );

int SW_Rio_PL_Get_Resp_Data (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_UCHAR_T offset,
    RIO_UCHAR_T cnt,
    RIO_DW_T *data );

int SW_Rio_PL_Req_Time_Out (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag );

int SW_Rio_PL_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_REMOTE_REQUEST_T *req,
    RIO_TR_INFO_T transport_Info );

int SW_Rio_PL_Remote_Response (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_RESPONSE_T *resp );

int SW_Rio_PL_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_BYTE_T tframe,
    RIO_BYTE_T td,
    RIO_BYTE_T tdl,
    RIO_BYTE_T tclk );

int SW_Pl_Rio_User_Msg (
    RIO_CONTEXT_T context,
    char *user_Msg );

int SW_Extend_Reg_Read (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long *data );

int SW_Extend_Reg_Write (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long data );

#ifndef PL_WRAP_SERIAL_ONLY
int SW_Rio_Granule_Received (
    RIO_CONTEXT_T context,
    RIO_GRANULE_STRUCT_T *granule_Struct );
#endif

int SW_Rio_Request_Received (
    RIO_CONTEXT_T context,
    RIO_REMOTE_REQUEST_T *packet_Struct );

int SW_Rio_Response_Received (
    RIO_CONTEXT_T context,
    RIO_RESPONSE_T *packet_Struct );

int SW_Rio_Symbol_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *symbol_Buffer,
    RIO_UCHAR_T buffer_Size );

int SW_Rio_Packet_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *packet_Buffer,
    unsigned int buffer_Size,
    RIO_UCHAR_T *packet_Length_In_Granules );

void SW_RIO_PL_Model_Create_Instance ();
/*Function for callback trays initialization*/
void Initialize_CB_Tray_Pl(void);
void Initialize_Serial_CB_Tray_Pl(void);

#ifndef PL_WRAP_PAR_ONLY
int SW_Rio_PL_Serial_Set_Pins (
    RIO_CONTEXT_T context, /* enviroment's context */
    RIO_SIGNAL_T tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3       /* transmit data on lane 3*/);
#endif 



#endif /*PROTOTYPES_PL_H*/


