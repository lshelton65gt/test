/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\wrapper_pl.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description:  Registers set for data exchange between the top module
*               of demo application and demo scenarios of PE-s
*
* Notes:        This file shall be included to all PE and DMA PE models 
*
******************************************************************************/
`define PLI_LONG_REGISTER_SIZE 32
`define PLI_SHORT_REGISTER_SIZE 8
// Data size convertion constants

`define RIO_BITS_IN_BYTE                    8
`define RIO_BITS_IN_WORD                    32
`define RIO_BITS_IN_DW                      64
`define RIO_BYTES_IN_WORD                   4
`define RIO_BYTES_IN_DW                     8
`define RIO_WORDS_IN_DW                     2
`define RIO_LP_EP_WIRE_WIDTH                8

// ----------------------------------------------------------------------------------------
// NOTE: the following defines must be in accordance with the appropriate enumeration types
//       declared in rio_types.h file of RIO model   
// ----------------------------------------------------------------------------------------

// Return values for some functions

`define RIO_OK                              0
`define RIO_ERROR                           1
`define RIO_RETRY                           2
`define RIO_PARAM_INVALID                   3

// Boolean constants

`define RIO_FALSE                          0
`define RIO_TRUE                           1

//Wire witdth constant - don't change it
`define RIO_LP_EP_WIRE_WIDTH               8

//Constants for access to routines internal buffers
`define RIO_PL_GSM_REQUEST_RQ_DATA              0
`define RIO_PL_GSM_REQUEST_TRANSPORT_INFO       1
`define RIO_PL_IO_REQUEST_RQ_DATA               2
`define RIO_PL_MESSAGE_REQUEST_RQ_DATA          3
`define RIO_PL_CONFIGURATION_REQUEST_RQ_DATA    4
`define RIO_PL_PACKET_STRUCT_REQUEST_RQ_DATA    5
`define RIO_PL_SEND_RESPONSE_RESP_DATA          6

//Constants for access to callback internal buffers
`define RIO_PL_REMOTE_REQUEST_REQ_DATA     0
`define RIO_PL_REMOTE_RESPONSE_RESP_DATA   1
`define RIO_PL_GET_CONFIG_REG_DATA         2

`define MSW                                1
`define LSW                                0


// GSM transaction types

`define RIO_GSM_READ_SHARED                 0
`define RIO_GSM_INSTR_READ                  1
`define RIO_GSM_READ_TO_OWN                 2
`define RIO_GSM_CASTOUT                     3
`define RIO_GSM_DC_I                        4
`define RIO_GSM_TLB_IE                      5
`define RIO_GSM_TLB_SYNC                    6
`define RIO_GSM_IC_I                        7
`define RIO_GSM_FLUSH                       8
`define RIO_GSM_IO_READ                     9

// GSM physical layer transaction types 

`define RIO_PL_GSM_READ_OWNER               0
`define RIO_PL_GSM_READ_TO_OWN_OWNER        1
`define RIO_PL_GSM_IO_READ_OWNER            2
`define RIO_PL_GSM_READ_HOME                3
`define RIO_PL_GSM_READ_TO_OWN_HOME         4
`define RIO_PL_GSM_IO_READ_HOME             5
`define RIO_PL_GSM_DKILL_HOME               6
`define RIO_PL_GSM_IREAD_HOME               7
`define RIO_PL_GSM_DKILL_SHARER             8
`define RIO_PL_GSM_IKILL                    9
`define RIO_PL_GSM_TLBIE                    10
`define RIO_PL_GSM_TLBSYNC                  11
`define RIO_PL_GSM_CASTOUT                  12
`define RIO_PL_GSM_FLUSH                    13

// IO transaction types

`define RIO_IO_NREAD                        0
`define RIO_IO_NWRITE                       1
`define RIO_IO_SWRITE                       2
`define RIO_IO_NWRITE_R                     3
`define RIO_IO_ATOMIC_INC                   4
`define RIO_IO_ATOMIC_DEC                   5
`define RIO_IO_ATOMIC_SWAP                  6
`define RIO_IO_ATOMIC_TSWAP                 7
`define RIO_IO_ATOMIC_SET                   8
`define RIO_IO_ATOMIC_CLEAR                 9

// Configuration transaction types

`define RIO_CONF_READ                       0
`define RIO_CONF_WRITE                      1

// Local response types

`define RIO_LR_RETRY                        0
`define RIO_LR_EXCLUSIVE                    1
`define RIO_LR_SHARED                       2
`define RIO_LR_OK                           3

// Result types

`define RIO_REQ_DONE                        0
`define RIO_REQ_ERROR                       1

// Snoop request types

`define RIO_LOCAL_READ                      0
`define RIO_LOCAL_READ_TO_OWN               1
`define RIO_LOCAL_READ_LATEST               2
`define RIO_LOCAL_DKILL                     3
`define RIO_LOCAL_IKILL                     4
`define RIO_LOCAL_TLBIE                     5
`define RIO_LOCAL_TLBSYNC                   6
`define RIO_LOCAL_NONCOHERENT               7

// Snoop result types

`define RIO_SNOOP_HIT                       0
`define RIO_SNOOP_MISS                      1

// Memory request type

`define RIO_MEM_READ                        0
`define RIO_MEM_WRAP_READ                   1
`define RIO_MEM_WRITE                       2
`define RIO_MEM_ATOMIC_INC                  3
`define RIO_MEM_ATOMIC_DEC                  4
`define RIO_MEM_ATOMIC_SET                  5
`define RIO_MEM_ATOMIC_CLEAR                6
`define RIO_MEM_ATOMIC_SWAP                 7
`define RIO_MEM_ATOMIC_TSWAP                8

`define RIO_PL_SUPPORTS_66_50_34_ADDR       3'b111
`define RIO_PL_SUPPORTS_66_34_ADDR          3'b101
`define RIO_PL_SUPPORTS_50_34_ADDR          3'b011
`define RIO_PL_SUPPORTS_34_ADDR             3'b001


//constant for error instance id value
`define RIO_PL_ERROR_INST_ID                -1

//constants for working mode
`define RIO_PL_INST_PAR_MODE                    0
`define RIO_PL_INST_SERIAL_MODE                 1

`define RIO_LANE_NUM                        4