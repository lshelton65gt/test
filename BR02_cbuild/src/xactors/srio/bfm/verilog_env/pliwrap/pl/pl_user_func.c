/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: M:\zkg_rio\riosuite\src\verilog_env\pliwrap\pl\pl_user_func.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:      This file contain user-defined routines
*
******************************************************************************/

#ifndef _NC_VLOG_
    #include "acc_user.h"
    #ifdef _MODELSIM_
    #include "veriuser.h"
    #else /*_MODELSIM_ undefined*/
    #include "vcsuser.h"
    #endif /*_MODELSIM_*/
#else /*_NC_VLOG_ is defined*/
    #include "acc_user.h"
    #include "veriuser.h"
    #include "vxl_veriuser.h"
#endif /*_NC_VLOG_*/

#include "wrapper_pl.h" 
#include "prototypes.h" 

/***************************************************************************
 * Function : SW_Pl_Rio_User_Msg
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_User_Msg
 *
 * Returns: result of Rio_User_Msg routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Pl_Rio_User_Msg (
    RIO_CONTEXT_T context,
    char *user_Msg )
{
    io_printf ("Pl%i: %s", (int)context, user_Msg);

    return RIO_OK;
}

/***************************************************************************
 * Function : SW_Extend_Reg_Read
 *
 * Description:  Implementation of RIO callback function 
 *              Extend_Reg_Read
 *
 * Returns: result of Extend_Reg_Read routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Extend_Reg_Read (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long *data )
{
    int instId;
    instId = (int)context;
    /*Insert you code here*/
    return RIO_OK;
}

/***************************************************************************
 * Function : SW_Rio_Request_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Request_Received
 *
 * Returns: result of Rio_Request_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Request_Received (
    RIO_CONTEXT_T context,
    RIO_REMOTE_REQUEST_T *packet_Struct )
{
    int instId;
    instId = (int)context;
    /*Insert you code here*/
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Response_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Response_Received
 *
 * Returns: result of Rio_Response_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Response_Received (
    RIO_CONTEXT_T context,
    RIO_RESPONSE_T *packet_Struct )
{
    int instId;
    instId = (int)context;
    /*Insert you code here*/
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Symbol_To_Send
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Symbol_To_Send
 *
 * Returns: result of Rio_Symbol_To_Send routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Symbol_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *symbol_Buffer,
    RIO_UCHAR_T buffer_Size )
{
    int instId;
    instId = (int)context;
    /*Insert you code here*/
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Packet_To_Send
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Packet_To_Send
 *
 * Returns: result of Rio_Packet_To_Send routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Packet_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *packet_Buffer,
    unsigned int buffer_Size,
    RIO_UCHAR_T *packet_Length_In_Granules )
{
    int instId;
    instId = (int)context;
    /*Insert you code here*/
    return RIO_OK;
}

/***************************************************************************
 * Function : SW_Rio_PL_Get_Resp_Data
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Get_Resp_Data
 *
 * Returns: result of Rio_PL_Get_Resp_Data routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Get_Resp_Data (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_UCHAR_T offset,
    RIO_UCHAR_T cnt,
    RIO_DW_T *data )
{
    int instId = (int)context;
    SW_Pl_Wrapper_Message (instId, "Info", "SW_Rio_PL_Get_Resp_Data is invoked");
    return 0;
}

/***************************************************************************
 * Function : SW_Rio_PL_Get_Req_Data
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Get_Req_Data
 *
 * Returns: result of Rio_PL_Get_Req_Data routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Get_Req_Data (
    RIO_CONTEXT_T context,
    RIO_TAG_T tag,
    RIO_UCHAR_T offset,
    RIO_UCHAR_T cnt,
    RIO_DW_T *data )
{
    SW_Pl_Wrapper_Message ((int)context, "Info", "SW_Rio_PL_Get_Req_Data is invoked");
    return 0;
}

/***************************************************************************
 * Function : SW_Rio_Register_Access
 *
 * Description: Provides access to registers for read/write
 *
 * Returns: RIO_OK if all right; RIO_ERROR othervise
 *
 * Notes:  
 *
 **************************************************************************/
int SW_Rio_Register_Access(int instId, int config_Offset, unsigned long *data,  REG_ACCESS_T acc_Type)
{
        if (acc_Type == REG_READ)
        {
            /*Realize code for reading from register*/
        }
        else 
        {
            /*Realize code for writing to register*/
        }

        return RIO_OK;
}


