/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pl\pl_par_specific.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  Routines which are specific for the parallel model
*
* Notes:       
*
******************************************************************************/

#include "acc_user.h"
#ifdef _MODELSIM_
#include "veriuser.h"
#else
#include "vcsuser.h"
#endif


#include "wrapper_pl.h" 
#include "prototypes.h" 




/*Function trays*/
extern RIO_PL_PARALLEL_MODEL_FTRAY_T Rio_Pl_Model_Ftray_T_Tray;



/*Contexts for instances*/
extern RIO_CONTEXT_PL_T *Contexts_Pl;

/*flag which signalized that wrapper is initialized*/
extern int rio_Pl_Wrap_Initialized;


extern int Pl_Max_Inst_Num;  /*Maximum instance number*/
extern int Pl_Inst_Num;  /*Current allocated instances number*/

/*Callback tray*/
extern RIO_PL_PARALLEL_MODEL_CALLBACK_TRAY_T Rio_Pl_Model_Callback_Tray_T_Tray;

/*functions implemetation*/


/***************************************************************************
 * Function : SW_RIO_PL_Model_Create_Instance
 *
 * Description: Software part of PLI function pair to provide
 *              Instance creation and binding
 *
 * Returns: Identificator of instance if All right or -1 if error
 *
 * Notes: The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_RIO_PL_Model_Create_Instance ()
{
    int k;
    int instId;
    int res;
/*Input parameters*/
    RIO_MODEL_INST_PARAM_T param;
/*Output parameters*/
    RIO_HANDLE_T handle;
/* End of parameters defintion*/

    /*check that wrapper is initialized*/
    if (rio_Pl_Wrap_Initialized == RIO_FALSE)
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", "Create_Instnce - PL Wrapper isn't initialized");    
        tf_putp(0, (int)RIO_PL_ABSENT_INST_ID);
        return;
    }

    k = 0;
/*Instance ID calculating*/
    instId = Pl_Inst_Num++;
/*  If no more memory for instances available then raise an error */
    if (Pl_Inst_Num > Pl_Max_Inst_Num)
    {
        SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error","Too many instances created");
        tf_putp(0, (int)RIO_PL_ABSENT_INST_ID);
        return;
    }

/*Parameters accessing*/
    param.ext_Address_Support = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 1 ); 
    param.is_Bridge = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 2 ); 
    param.has_Memory = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 3 ); 
    param.has_Processor = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 4 ); 
    param.coh_Granule_Size_32 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 5 ); 
    param.coh_Domain_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    param.device_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 7 ); 
    param.device_Vendor_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 8 ); 
    param.device_Rev = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 ); 
    param.device_Minor_Rev = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 10 ); 
    param.device_Major_Rev = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 11 ); 
    param.assy_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 12 ); 
    param.assy_Vendor_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 13 ); 
    param.assy_Rev = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 14 ); 
    param.entry_Extended_Features_Ptr = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 15 ); 
    param.next_Extended_Features_Ptr = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 16 ); 
    param.source_Trx = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 17 ); 
    param.dest_Trx = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 18 ); 
    param.mbox1 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 19 ); 
    param.mbox2 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 20 ); 
    param.mbox3 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 21 ); 
    param.mbox4 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 22 ); 
    param.has_Doorbell = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 23 ); 
    param.pl_Inbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 24 ); 
    param.pl_Outbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 25 ); 
    param.ll_Inbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 26 ); 
    param.ll_Outbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 27 ); 
/*End of the fields value getting for the param structure*/
/*Get registers handles*/
    Contexts_Pl[instId].regs.rio_PL_Ack_Request_Tag_Reg = acc_handle_tfarg( k + 28);
    Contexts_Pl[instId].regs.rio_PL_Ack_Request_Flag_Reg = acc_handle_tfarg( k + 29);
    Contexts_Pl[instId].regs.rio_PL_Ack_Response_Tag_Reg = acc_handle_tfarg( k + 30);
    Contexts_Pl[instId].regs.rio_PL_Ack_Response_Flag_Reg = acc_handle_tfarg( k + 31);

    Contexts_Pl[instId].regs.rio_PL_Req_Time_Out_Tag_Reg = acc_handle_tfarg( k + 32);
    Contexts_Pl[instId].regs.rio_PL_Req_Time_Out_Flag_Reg = acc_handle_tfarg( k + 33);
    
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Tag_Reg = acc_handle_tfarg( k + 34);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Prio_Reg = acc_handle_tfarg( k + 35);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Ftype_Reg = acc_handle_tfarg( k + 36);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Transaction_Reg = acc_handle_tfarg( k + 37);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Status_Reg = acc_handle_tfarg( k + 38);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Tr_Info_Reg = acc_handle_tfarg( k + 39);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Src_ID_Reg = acc_handle_tfarg( k + 40);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Target_TID_Reg = acc_handle_tfarg( k + 41);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Sec_ID_Reg = acc_handle_tfarg( k + 42);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Sec_TID_Reg = acc_handle_tfarg( k + 43);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Dw_Num_Reg = acc_handle_tfarg( k + 44);
    Contexts_Pl[instId].regs.rio_PL_Remote_Response_Flag_Reg = acc_handle_tfarg( k + 45);
    
    Contexts_Pl[instId].regs.rio_PL_Set_Pins_Tframe_Reg = acc_handle_tfarg( k + 46);
    Contexts_Pl[instId].regs.rio_PL_Set_Pins_Td_Reg = acc_handle_tfarg( k + 47);
    Contexts_Pl[instId].regs.rio_PL_Set_Pins_Tdl_Reg = acc_handle_tfarg( k + 48);
    Contexts_Pl[instId].regs.rio_PL_Set_Pins_Tclk_Reg = acc_handle_tfarg( k + 49);
    Contexts_Pl[instId].regs.rio_PL_Set_Pins_Flag_Reg = acc_handle_tfarg( k + 50);
    
    Contexts_Pl[instId].regs.extend_Reg_Write_Config_Offset_Reg = acc_handle_tfarg( k + 51);
    Contexts_Pl[instId].regs.extend_Reg_Write_Data_Reg = acc_handle_tfarg( k + 52);
    Contexts_Pl[instId].regs.extend_Reg_Write_Flag_Reg = acc_handle_tfarg( k + 53);


    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Tag_Reg = acc_handle_tfarg( k + 54);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Packet_Type_Reg = acc_handle_tfarg( k + 55);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Dw_Num_Reg = acc_handle_tfarg( k + 56);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Transp_Type_Reg = acc_handle_tfarg( k + 57);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Transport_Info_Reg = acc_handle_tfarg( k + 58);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Flag_Reg = acc_handle_tfarg( k + 59);
/*Registers for nested structure*/
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Target_TID = acc_handle_tfarg( k + 60);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Src_ID = acc_handle_tfarg( k + 61);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Prio = acc_handle_tfarg( k + 62);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Wdptr = acc_handle_tfarg( k + 63);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Rdsize = acc_handle_tfarg( k + 64);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Ftype = acc_handle_tfarg( k + 65);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Ttype = acc_handle_tfarg( k + 66);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Address = acc_handle_tfarg( k + 67);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Extended_Address = acc_handle_tfarg( k + 68);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Xamsbs = acc_handle_tfarg( k + 69);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Sec_ID = acc_handle_tfarg( k + 70);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Sec_TID = acc_handle_tfarg( k + 71);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Mbox = acc_handle_tfarg( k + 72);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Letter = acc_handle_tfarg( k + 73);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Msgseg = acc_handle_tfarg( k + 74);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Ssize = acc_handle_tfarg( k + 75);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Msglen = acc_handle_tfarg( k + 76);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Doorbell_Info = acc_handle_tfarg( k + 77);
    Contexts_Pl[instId].regs.rio_PL_Remote_Request_Req_Offset = acc_handle_tfarg( k + 78);
/*End of nested structure*/

    res = RIO_PL_Parallel_Model_Create_Instance(
        &handle , &param );
    if ( res != RIO_OK )
    {
        io_printf ("PL Wrapper : Error during instance creating \n");
        tf_putp(0, (int)RIO_PL_ABSENT_INST_ID);
        Pl_Inst_Num--;
        return;
    }
    Contexts_Pl[instId].handle = handle;



/*Instance binding*/

    Rio_Pl_Model_Callback_Tray_T_Tray.pl_Interface_Context = (RIO_CONTEXT_T)instId;
    Rio_Pl_Model_Callback_Tray_T_Tray.lpep_Interface_Context = (RIO_CONTEXT_T)instId;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_Msg = (RIO_CONTEXT_T)instId;
    Rio_Pl_Model_Callback_Tray_T_Tray.extend_Reg_Context = (RIO_CONTEXT_T)instId;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_Hooks_Context = (RIO_CONTEXT_T)instId;
    res = RIO_PL_Parallel_Model_Bind_Instance(
            Contexts_Pl[instId].handle,
            &Rio_Pl_Model_Callback_Tray_T_Tray);
    if ( res != RIO_OK )
    {
        io_printf ("PL Wrapper : Error during instance binding \n");
        tf_putp(0, (int)RIO_PL_ABSENT_INST_ID);
        Pl_Inst_Num--;
        return;
    }
/*End of instance binding*/

/*set current mode of the instance*/
    Contexts_Pl[instId].priv_Data.inst_Mode = PL_INST_PAR_MODE;

    tf_putp(0, instId);
}


/***************************************************************************
 * Function : SW_Rio_PL_Model_Initialize
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Model_Initialize routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Model_Initialize routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Model_Initialize ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_PL_PARALLEL_PARAM_SET_T param_Set;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);
    if (PL_IN_PAR_MODE) /*do nothing*/;
    else
    {
        SW_Pl_Wrapper_Message(instId, "Error", "Attempt to invoke parallel mode routine for non-parallel instance");
        tf_putp(0, RIO_ERROR);
        return;
    } 

    param_Set.transp_Type = ( RIO_PL_TRANSP_TYPE_T ) acc_fetch_tfarg_int( k + 2 ); 
    param_Set.dev_ID = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 3 ); 
    param_Set.lcshbar = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 4 ); 
    param_Set.lcsbar = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 5 ); 
    param_Set.is_Host = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 6 ); 
    param_Set.is_Master_Enable = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 7 ); 
    param_Set.is_Discovered = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 8 ); 
    param_Set.ext_Address = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 9 ); 
    param_Set.ext_Address_16 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 10 ); 
    param_Set.lp_Ep_Is_8 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 11 ); 
    param_Set.work_Mode_Is_8 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 12 ); 
    param_Set.input_Is_Enable = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 13 ); 
    param_Set.output_Is_Enable = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 14 ); 
    param_Set.num_Trainings = ( int ) acc_fetch_tfarg_int( k + 15 ); 
    param_Set.requires_Window_Alignment = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 16 ); 
    param_Set.receive_Idle_Lim = ( int ) acc_fetch_tfarg_int( k + 17 ); 
    param_Set.throttle_Idle_Lim = ( int ) acc_fetch_tfarg_int( k + 18 ); 
    param_Set.res_For_3_Out = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 19 ); 
    param_Set.res_For_2_Out = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 20 ); 
    param_Set.res_For_1_Out = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 21 ); 
    param_Set.res_For_3_In = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 22 ); 
    param_Set.res_For_2_In = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 23 ); 
    param_Set.res_For_1_In = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 24 ); 
    param_Set.pass_By_Prio = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 25 ); 
    param_Set.enable_Retry_Return_Code = ( RIO_BOOL_T )acc_fetch_tfarg_int( k + 26 ); 
    param_Set.enable_LRQ_Resending  = ( RIO_BOOL_T )acc_fetch_tfarg_int( k + 27 ); 
    if (param_Set.lp_Ep_Is_8 == RIO_TRUE || param_Set.work_Mode_Is_8 == RIO_TRUE)
    {
        param_Set.custom_Training_Pattern[0] = 0xFFFFFFFF;
        param_Set.custom_Training_Pattern[1] = 0;
    }
    else
    {
        param_Set.custom_Training_Pattern[0] = 0xFFFFFFFF;
        param_Set.custom_Training_Pattern[1] = 0xFFFFFFFF;
        param_Set.custom_Training_Pattern[2] = 0;
        param_Set.custom_Training_Pattern[3] = 0;
    }
    param_Set.transmit_Flow_Control_Support = RIO_FALSE;

/*End of the fields value getting for the param_Set structure*/


    tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Model_Initialize))( Contexts_Pl[instId].handle,
        &param_Set ));
}


/***************************************************************************
 * Function : SW_Rio_PL_Get_Pins
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Get_Pins routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Get_Pins routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_PL_Get_Pins ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_BYTE_T frame;
    RIO_BYTE_T rd;
    RIO_BYTE_T rdl;
    RIO_BYTE_T rclk;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);
    if (PL_IN_PAR_MODE) /*do nothing*/;
    else
    {
        SW_Pl_Wrapper_Message(instId, "Error", "Attempt to invoke parallel mode routine for non-parallel instance");
        return;
    } 

    frame = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 2 ); 
    rd = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 3 ); 
    rdl = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 4 ); 
    rclk = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 5 ); 

    if ((instId < 0) || (instId >= Pl_Inst_Num))
        SW_Pl_Wrapper_Message (instId, "Warning", "Get_Pins is called for non-existing instance");
    else
        (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Get_Pins))( Contexts_Pl[instId].handle, frame , rd , rdl , rclk );
}

/***************************************************************************
 * Function : Initialize_CB_Tray_Pl
 *
 * Description: Function for callback trays initialization
 *
 * Returns:  
 *
 * Notes: This function is generated automaticaly by WrapGen
 *
 **************************************************************************/
void Initialize_CB_Tray_Pl(void)
{
/*Initialize callbacks for tray 0*/
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Ack_Request = SW_Rio_PL_Ack_Request;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Get_Req_Data = SW_Rio_PL_Get_Req_Data;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Ack_Response = SW_Rio_PL_Ack_Response;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Get_Resp_Data = SW_Rio_PL_Get_Resp_Data;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Req_Time_Out = SW_Rio_PL_Req_Time_Out;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Remote_Request = SW_Rio_PL_Remote_Request;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Remote_Response = SW_Rio_PL_Remote_Response;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_PL_Set_Pins = SW_Rio_PL_Set_Pins;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_User_Msg = SW_Pl_Rio_User_Msg;
    Rio_Pl_Model_Callback_Tray_T_Tray.extend_Reg_Read = SW_Extend_Reg_Read;
    Rio_Pl_Model_Callback_Tray_T_Tray.extend_Reg_Write = SW_Extend_Reg_Write;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_Granule_Received = SW_Rio_Granule_Received;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_Request_Received = SW_Rio_Request_Received;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_Response_Received = SW_Rio_Response_Received;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_Symbol_To_Send = SW_Rio_Symbol_To_Send;
    Rio_Pl_Model_Callback_Tray_T_Tray.rio_Packet_To_Send = SW_Rio_Packet_To_Send;
}


/***************************************************************************
 * Function : SW_PL_Lost_Sync_Command
 *
 * Description: Software part of PLI function pair to provide
 *              Access to RIO_PL_PARALLEL_INIT_MACHINE_LOST_SYNC_COMMAND 
 *              routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Model_Start_Reset routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_PL_Lost_Sync_Command ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);



    if (PL_IN_PAR_MODE)
        (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Lost_Sync_Command))(Contexts_Pl[instId].handle);
}

/***************************************************************************
 * Function : SW_PL_Enable_Rnd_Idle_Generator
 *
 * Description: Software part of PLI function pair to provide
 *              Access to RIO_PL_ENABLE_RND_IDLE_GENERATOR 
 *              routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Model_Start_Reset routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_PL_Enable_Rnd_Idle_Generator ()
{
    int k;
    int instId;
    unsigned int low_Boundary;
    unsigned int high_Boundary;
    RIO_BOOL_T enable;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    low_Boundary = (unsigned int)acc_fetch_tfarg_int( k + 2 );
    high_Boundary = (unsigned int)acc_fetch_tfarg_int( k + 3 );
    enable = (RIO_BOOL_T)acc_fetch_tfarg_int( k + 4 );

    if (PL_IN_PAR_MODE)
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Enable_Rnd_Idle_Generator))(
            Contexts_Pl[instId].handle, low_Boundary, high_Boundary, enable));
    else tf_putp(0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_PL_Force_EOP_Insertion
 *
 * Description: Software part of PLI function pair to provide
 *              Access to RIO_PL_FORCE_EOP_INSERTION
 *              routine in the RIO_PL_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Model_Start_Reset routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_PL_Force_EOP_Insertion()
{
    int k;
    int instId;
    RIO_BOOL_T enable;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    enable = (RIO_BOOL_T)acc_fetch_tfarg_int( k + 2 );

    if (PL_IN_PAR_MODE)
        tf_putp(0, (*(Rio_Pl_Model_Ftray_T_Tray.rio_PL_Force_EOP_Insertion))(
            Contexts_Pl[instId].handle, enable));
    else tf_putp(0, RIO_ERROR);
}


/*****************************************************************************/
/*     Callbacks implementation                                              */
/*****************************************************************************/

/***************************************************************************
 * Function : SW_Rio_PL_Set_Pins
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Set_Pins
 *
 * Returns: result of Rio_PL_Set_Pins routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_PL_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_BYTE_T tframe,
    RIO_BYTE_T td,
    RIO_BYTE_T tdl,
    RIO_BYTE_T tclk )
{
    int instId;
    instId = (int)context;
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Set_Pins_Tframe_Reg, (int)tframe);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Set_Pins_Td_Reg, (int)td);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Set_Pins_Tdl_Reg, (int)tdl);
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Set_Pins_Tclk_Reg, (int)tclk);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pl[instId].regs.rio_PL_Set_Pins_Flag_Reg, 1);
    return 0;
}

/***************************************************************************
 * Function : SW_Rio_Granule_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Granule_Received
 *
 * Returns: result of Rio_Granule_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Granule_Received (
    RIO_CONTEXT_T context,
    RIO_GRANULE_STRUCT_T *granule_Struct )
{
    int instId;
    instId = (int)context;
    /*Insert you code here*/
    return RIO_OK;
}


/*****************************************************************************/
/*     implementation of stub for the serial routines                        */
/*****************************************************************************/

#ifdef PL_WRAP_PAR_ONLY

/***************************************************************************
 * Function : SW_Rio_PL_Serial_Model_Initialize
 *
 * Description: stub for serial part of wrapper
 *
 * Returns: -1
 *
 * Notes:
 **************************************************************************/
void SW_RIO_PL_Serial_Model_Create_Instance ()
{
    SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_PL_ABSENT_INST_ID);
}

/***************************************************************************
 * Function : SW_Rio_PL_serial_Model_Initialize
 *
 * Description: stub for serial part of wrapper
 *
 * Returns: RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
void SW_Rio_PL_Serial_Model_Initialize ()
{
    SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Rio_PL_Get_Pins
 *
 * Description: stub for serial part of wrapper
 *
 * Returns: 
 *
 * Notes:
 *
 **************************************************************************/
void SW_Rio_PL_Serial_Get_Pins ()
{
    SW_Pl_Wrapper_Message(RIO_PL_ABSENT_INST_ID, "Error", 
        "Serial functionality is not implemented in the current delivery");        
}

#endif /*PL_WRAP_PAR_ONLY*/
