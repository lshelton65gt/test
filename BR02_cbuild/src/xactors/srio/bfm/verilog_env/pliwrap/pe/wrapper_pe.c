/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\wrapper_pe.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:          Prototype of this file is generated automaticaly by WrapGen
*                 constants used in the acc_fetch_tfarg_int and acc_handle_tfarg 
*                 routines are used to specify position in the arguments list
*
******************************************************************************/


#ifndef _NC_VLOG_
    #include "acc_user.h"
    #ifdef _MODELSIM_
    #include "veriuser.h"
    #else /*_MODELSIM_ not defined*/
    #include "vcsuser.h"
    #endif /*_MODELSIM_*/
#else /*_NC_VLOG_ defined*/
    #include "acc_user.h"
    #include "veriuser.h"
    #include "vxl_veriuser.h"
#endif /*_NC_VLOG_*/

#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include "wrapper_pe.h" 
#include "prototypes.h" 
#include "wrapvars.h"


/*Internal input buffer for SW_Rio_Init routine's variable*/ 
RIO_TR_INFO_T Rio_Init_Param_Set_Remote_Dev_ID[PLI_MAX_ARRAY_SIZE];


/***************************************************************************
 * Function : SW_Pe_Wrapper_Message
 *
 * Description: Simple Error handle
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_Pe_Wrapper_Message (int id, char *type, char * msg)
{
    io_printf ("%s wrapper: SW_Pe_Wrapper_Message, instance=%i: %s\n", type, id, msg);
}




/***************************************************************************
 * Function : SW_Init
 *
 * Description: Software part of PLI function pair to initialize
 *                PLI engine, shall be called when demo starts
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_Init ()
{
    int num;

    /*Check that this is first call of initialization routine*/
    if (Rio_Pe_Wrap_Initialized == RIO_TRUE)
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Warning", "PE Wrapper alredy initialized");    
        return;
    }
    else
    /*set flag for notifying that wrapper is initialized*/
        Rio_Pe_Wrap_Initialized = RIO_TRUE;

    acc_initialize();

#ifndef PE_WRAP_SERIAL_ONLY
    if(RIO_Parallel_Model_Get_Function_Tray ( &Rio_Model_Ftray_T_Tray ) != RIO_OK)
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", "RIO_Model_Get_Function_Tray - not OK");
        tf_dofinish();
    }
    /*Fills Callback tray*/
    Initialize_CBTray();
#endif

#ifndef PE_WRAP_PAR_ONLY
    if(RIO_Serial_Model_Get_Function_Tray ( &Rio_Serial_Model_Ftray_T_Tray ) != RIO_OK)
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", "RIO_Serial_Model_Get_Function_Tray - not OK");
        tf_dofinish();
    }
    /*Fills Callback tray*/
    Initialize_Serial_CBTray();
#endif

/*reset initial values*/
    Pe_Max_Inst_Num = 0;
    Pe_Inst_Num = 0;


/*Allocation memory for contexts*/
    num = acc_fetch_tfarg_int(1);
    if (num < 0) 
    {
        SW_Pe_Wrapper_Message (RIO_PE_ABSENT_INST_ID, "Error", "SW_Init, Negative number of instances");
        tf_dofinish();
    }
    Pe_Max_Inst_Num = num;
    Contexts_Pe = (RIO_CONTEXT_PE_T *)malloc( num * sizeof( RIO_CONTEXT_PE_T ) );
    if ( Contexts_Pe == NULL)
    {
        SW_Pe_Wrapper_Message (RIO_PE_ABSENT_INST_ID, "Error", "SW_Init, Error during memory allocation");
        tf_dofinish();
    }

}


/***************************************************************************
 * Function : SW_Close
 *
 * Description: Software part of PLI function pair to free internal
 *                PLI structures, shall be called when demo finishes
 *
 * Returns:  
 *
 * Notes:  
 *
 **************************************************************************/
void SW_Close ()
{
    if (Rio_Pe_Wrap_Initialized == RIO_FALSE)
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Warning", "PE Wrapper alredy closed");    
        return;
    }
    acc_close();
    free ( Contexts_Pe );
    /*reset the flag*/
    Rio_Pe_Wrap_Initialized = RIO_FALSE;
}


/***************************************************************************
 * Function : SW_Rio_GSM_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_GSM_Request routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_GSM_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_GSM_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_GSM_TTYPE_T ttype;
    RIO_GSM_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    ttype = ( RIO_GSM_TTYPE_T ) acc_fetch_tfarg_int( k + 2 ); 
/*Gets fields of the rq structure*/
    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.extended_Address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.xamsbs = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.offset = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.byte_Num = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 );  
    rq.data = Contexts_Pe[instId].bufs.Rio_GSM_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 10 ); 
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int(k + 11);

/*return result*/
#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp (0, (*Rio_Model_Ftray_T_Tray.rio_GSM_Request) (Contexts_Pe[instId].handle ,  ttype , &rq , trx_Tag ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp (0, (*Rio_Serial_Model_Ftray_T_Tray.rio_GSM_Request) (Contexts_Pe[instId].handle ,  ttype , &rq , trx_Tag ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);

}


/***************************************************************************
 * Function : SW_Rio_IO_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_IO_Request routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_IO_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_IO_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_IO_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.ttype = ( RIO_IO_TTYPE_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.extended_Address = ( RIO_ADDRESS_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.xamsbs = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.offset = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.byte_Num = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 );  
    rq.data = Contexts_Pe[instId].bufs.Rio_IO_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 10 ); 
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int(k + 11);

#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_IO_Request))( Contexts_Pe[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_IO_Request))( Contexts_Pe[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Message_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Message_Request routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Message_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Message_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_MESSAGE_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.msglen = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.ssize = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.letter = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.mbox = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.msgseg = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 );  
    rq.data = Contexts_Pe[instId].bufs.Rio_Message_Request_Rq_Data; /*reading from internal buffer*/
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 10 ); 
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int(k + 11);

#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Message_Request))( Contexts_Pe[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Message_Request))( Contexts_Pe[instId].handle,
            &rq , trx_Tag ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Doorbell_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Doorbell_Request routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Doorbell_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Doorbell_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_DOORBELL_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.doorbell_Info = ( RIO_DB_INFO_T ) acc_fetch_tfarg_int( k + 4 ); 
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int(k + 6);

#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Doorbell_Request))( Contexts_Pe[instId].handle,
            &rq, trx_Tag ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Doorbell_Request))( Contexts_Pe[instId].handle,
            &rq, trx_Tag ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Conf_Request
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Conf_Request routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Conf_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Conf_Request ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_CONFIG_REQ_T rq;
    RIO_TAG_T trx_Tag;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    rq.prio = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 2 ); 
    rq.transport_Info = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 3 ); 
    rq.hop_Count = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 4 ); 
    rq.rw = ( RIO_CONF_TTYPE_T ) acc_fetch_tfarg_int( k + 5 ); 
    rq.config_Offset = ( RIO_CONF_OFFS_T ) acc_fetch_tfarg_int( k + 6 ); 
    rq.dw_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 7 ); 
    rq.sub_Dw_Pos = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 8 );  
    if (rq.rw == RIO_CONF_READ) 
    {
        rq.data = NULL;
    }
    else 
    {
        rq.data = Contexts_Pe[instId].bufs.Rio_Conf_Request_Rq_Data; /*reading from internal buffer*/
    }
/*End of the fields value getting for the rq structure*/
    trx_Tag = ( RIO_TAG_T ) acc_fetch_tfarg_int( k + 9 ); 
    rq.transp_Type = (RIO_PL_TRANSP_TYPE_T)acc_fetch_tfarg_int(k + 10);


#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Conf_Request))( Contexts_Pe[instId].handle,
            &rq, trx_Tag ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Conf_Request))( Contexts_Pe[instId].handle,
            &rq, trx_Tag ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Snoop_Response
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Snoop_Response routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Snoop_Response routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Snoop_Response ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_SNOOP_RESULT_T snoop_Result;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    snoop_Result = ( RIO_SNOOP_RESULT_T ) acc_fetch_tfarg_int( k + 2 ); 


#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Snoop_Response))( Contexts_Pe[instId].handle,
            snoop_Result , Contexts_Pe[instId].bufs.Rio_Snoop_Response_Data ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Snoop_Response))( Contexts_Pe[instId].handle,
            snoop_Result , Contexts_Pe[instId].bufs.Rio_Snoop_Response_Data ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);

}


/***************************************************************************
 * Function : SW_Rio_Set_Mem_Data
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Set_Mem_Data routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Set_Mem_Data routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Set_Mem_Data ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);



#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Set_Mem_Data))( Contexts_Pe[instId].handle, Contexts_Pe[instId].bufs.Rio_Set_Mem_Data_Dw ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Set_Mem_Data))( Contexts_Pe[instId].handle, Contexts_Pe[instId].bufs.Rio_Set_Mem_Data_Dw ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Get_Mem_Data
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Get_Mem_Data routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Get_Mem_Data routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Get_Mem_Data ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    
#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Get_Mem_Data))( Contexts_Pe[instId].handle,
            Contexts_Pe[instId].bufs.rio_Get_Mem_Data_Dw ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Get_Mem_Data))( Contexts_Pe[instId].handle,
            Contexts_Pe[instId].bufs.rio_Get_Mem_Data_Dw ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Set_Conf_Reg
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Set_Conf_Reg routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Set_Conf_Reg routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Set_Conf_Reg ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_CONF_OFFS_T config_Offset;
    unsigned long data;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    config_Offset = ( RIO_CONF_OFFS_T ) acc_fetch_tfarg_int( k + 2 ); 
    data = ( unsigned long ) acc_fetch_tfarg_int( k + 3 ); 

#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Set_Conf_Reg))( Contexts_Pe[instId].handle,
            config_Offset , data ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Set_Conf_Reg))( Contexts_Pe[instId].handle,
            config_Offset , data ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Get_Conf_Reg
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Get_Conf_Reg routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Get_Conf_Reg routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Get_Conf_Reg ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_CONF_OFFS_T config_Offset;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    config_Offset = ( RIO_CONF_OFFS_T ) acc_fetch_tfarg_int( k + 2 ); 


#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
    {
        tf_putp(0, (*(Rio_Model_Ftray_T_Tray.rio_Get_Conf_Reg))( Contexts_Pe[instId].handle,
            config_Offset , &(Contexts_Pe[instId].bufs.rio_Get_Conf_Reg_Data) ));
        return;
    }
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
    {
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Get_Conf_Reg))( Contexts_Pe[instId].handle,
            config_Offset , &(Contexts_Pe[instId].bufs.rio_Get_Conf_Reg_Data) ));
        return;
    }
#endif
    tf_putp (0, RIO_ERROR);
}


/***************************************************************************
 * Function : SW_Rio_Start_Reset
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Start_Reset routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Start_Reset routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Start_Reset ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

#ifndef PE_WRAP_SERIAL_ONLY
    if (PE_IN_PAR_MODE)
        (*(Rio_Model_Ftray_T_Tray.rio_Start_Reset))( Contexts_Pe[instId].handle);
#endif 
#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
        (*(Rio_Serial_Model_Ftray_T_Tray.rio_Start_Reset))( Contexts_Pe[instId].handle);
#endif
}




/***************************************************************************
 * Function : SW_Rio_Clock_Edge
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Clock_Edge routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Clock_Edge routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Clock_Edge ()
{
    int k;
    int instId;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    /*Check is instance with the current Inst_ID created of no*/
    if ((instId < 0) || (instId >= Pe_Inst_Num))
        SW_Pe_Wrapper_Message (instId, "Warning", "Clock_Edge is called for non-existing instance");
    else
    {
#ifndef PE_WRAP_SERIAL_ONLY
        if (PE_IN_PAR_MODE)
            (*(Rio_Model_Ftray_T_Tray.rio_PL_Clock_Edge))( Contexts_Pe[instId].handle);
#endif 
#ifndef PE_WRAP_PAR_ONLY
        if (PE_IN_SERIAL_MODE)
            (*(Rio_Serial_Model_Ftray_T_Tray.rio_PL_Clock_Edge))( Contexts_Pe[instId].handle);
#endif

    }
}

/***************************************************************************
 * Function : SW_Rio_Clock_Edge_New
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Clock_Edge routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Clock_Edge routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Clock_Edge_New ()
{
    int i_Count;
    int instId; 

#ifndef PE_WRAP_PAR_ONLY
    set_Serial_Pins_10bit_Flag_Reg = RIO_FALSE;
        if (PE_IN_SERIAL_MODE)
        {
            ten_Bit_Interface = RIO_TRUE;
            for (i_Count =1; i_Count <= 10; i_Count++)
            {
                SW_Rio_Clock_Edge ();

            }
            set_Serial_Pins_10bit_Flag_Reg = RIO_TRUE; 
        }
#endif

    
}

/***************************************************************************
 * Function : SW_Rio_Set_Retry_Generator
 *
 * Description: Software part of PLI function pair to provide
 *              Set Packet Retry Generator
 *
 * Returns: result of Rio_PL_Set_Retry_Generator routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Set_Retry_Generator()
{
    int k;
    int instId;
    unsigned int retry_Factor;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );

/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    /*Check is instance with the current Inst_ID created of no*/
    if ((instId < 0) || (instId >= Pe_Inst_Num))
        SW_Pe_Wrapper_Message (instId, "Warning", "Set_Retry_Generator is called for non-existing instance");
    else
    {

        retry_Factor = (unsigned int)acc_fetch_tfarg_int( k + 2 );

#ifndef PE_WRAP_SERIAL_ONLY
        if (PE_IN_PAR_MODE)
            (*(Rio_Model_Ftray_T_Tray.rio_PL_Set_Retry_Generator))( Contexts_Pe[instId].handle, retry_Factor);
#endif 
#ifndef PE_WRAP_PAR_ONLY
        if (PE_IN_SERIAL_MODE)
            (*(Rio_Serial_Model_Ftray_T_Tray.rio_PL_Set_Retry_Generator))( Contexts_Pe[instId].handle, retry_Factor);
#endif

    }

}



/*****************************************************************************/
/*     Callbacks implementation                                              */
/*****************************************************************************/




/***************************************************************************
 * Function : SW_Rio_Local_Response
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Local_Response
 *
 * Returns: result of Rio_Local_Response routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Local_Response (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_LOCAL_RTYPE_T response )
{
    int instId;
    instId = (int)context;

/*    SW_Pe_Wrapper_Message (instId, "Info", "Local responce is called");*/

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_GSM_Request_Done
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_GSM_Request_Done
 *
 * Returns: result of Rio_GSM_Request_Done routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_GSM_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code,
    RIO_WORD_T dw_Size,
    RIO_DW_T *data )
{
    int instId  = (int)context;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_GSM_Request_Done_Trx_Tag_Reg, (int)trx_Tag);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_GSM_Request_Done_Result_Reg, (int)result);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_GSM_Request_Done_Err_Code_Reg, (int)err_Code);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_GSM_Request_Done_Dw_Size_Reg, (int)dw_Size);
    memcpy (Contexts_Pe[instId].bufs.rio_GSM_Request_Done_Data, data, dw_Size * sizeof(RIO_DW_T) );
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_GSM_Request_Done_Flag_Reg, 1);

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_IO_Request_Done
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_IO_Request_Done
 *
 * Returns: result of Rio_IO_Request_Done routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_IO_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code,
    RIO_WORD_T dw_Size,
    RIO_DW_T *data )
{
    int instId = (int)context;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_IO_Request_Done_Trx_Tag_Reg, (int)trx_Tag);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_IO_Request_Done_Result_Reg, (int)result);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_IO_Request_Done_Err_Code_Reg, (int)err_Code);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_IO_Request_Done_Dw_Size_Reg, (int)dw_Size);
    memcpy (Contexts_Pe[instId].bufs.rio_IO_Request_Done_Data, data, dw_Size * sizeof(RIO_DW_T) );
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_IO_Request_Done_Flag_Reg, 1);

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_MP_Request_Done
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_MP_Request_Done
 *
 * Returns: result of Rio_MP_Request_Done routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_MP_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code )
{
    int instId = (int)context;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Request_Done_Trx_Tag_Reg, (int)trx_Tag);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Request_Done_Result_Reg, (int)result);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Request_Done_Err_Code_Reg, (int)err_Code);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Request_Done_Flag_Reg, 1);

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_Doorbell_Request_Done
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Doorbell_Request_Done
 *
 * Returns: result of Rio_Doorbell_Request_Done routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Doorbell_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code )
{
    int instId = (int)context;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Trx_Tag_Reg, (int)trx_Tag);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Result_Reg, (int)result);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Err_Code_Reg, (int)err_Code);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Flag_Reg, 1);

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_Config_Request_Done
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Config_Request_Done
 *
 * Returns: result of Rio_Config_Request_Done routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Config_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code,
    RIO_WORD_T dw_Size,
    RIO_DW_T *data )
{
    int instId = (int)context;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Config_Request_Done_Trx_Tag_Reg, (int)trx_Tag);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Config_Request_Done_Result_Reg, (int)result);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Config_Request_Done_Err_Code_Reg, (int)err_Code);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Config_Request_Done_Dw_Size_Reg, (int)dw_Size);
    memcpy (Contexts_Pe[instId].bufs.rio_Config_Request_Done_Data, data, dw_Size * sizeof(RIO_DW_T) );
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Config_Request_Done_Flag_Reg, 1);

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_Snoop_Request
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Snoop_Request
 *
 * Returns: result of Rio_Snoop_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Snoop_Request (
    RIO_CONTEXT_T context,
    RIO_SNOOP_REQ_T rq )
{
    int instId = (int)context;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Snoop_Request_Address_Reg, (int)rq.address);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Snoop_Request_Extended_Address_Reg, (int)rq.extended_Address);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Snoop_Request_Xamsbs_Reg, (int)rq.xamsbs);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Snoop_Request_Lttype_Reg, (int)rq.lttype);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Snoop_Request_Ttype_Reg, (int)rq.ttype);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Snoop_Request_Flag_Reg, 1);

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_MP_Remote_Request
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_MP_Remote_Request
 *
 * Returns: result of Rio_MP_Remote_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_MP_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_REMOTE_MP_REQ_T message )
{
    int instId = (int)context;
    unsigned long cur_CSR;
/*Check is mailbox available now*/

    cur_CSR = GetIntegerRegValue(Contexts_Pe[instId].regs.mailbox_CSR);
    /*Extract bits for current MBOX
      8 - number of information bits for each mailbox
      (3 - message.mbox) - position of this bits in the CSR register
      FF - mask for exstracting bits which applicable for current mailbox only */
    cur_CSR = (cur_CSR >> (8 * (3 - message.mbox))) & 0xFF; 

    if ((cur_CSR & MBOX_ERROR) != 0) return RIO_ERROR;
    if ((cur_CSR & MBOX_FAILED) != 0) return RIO_ERROR;
    /*If mailbox is busy then retry only new messages*/
    if ( ((cur_CSR & MBOX_BUSY) != 0) && (message.msgseg == 0) ) return RIO_RETRY;
    if ((cur_CSR & MBOX_FULL) != 0) return RIO_RETRY;
    if ((cur_CSR & MBOX_AVAILABLE) == 0) return RIO_ERROR;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Remote_Request_Msglen_Reg, (int)message.msglen);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Remote_Request_Ssize_Reg, (int)message.ssize);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Remote_Request_Letter_Reg, (int)message.letter);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Remote_Request_Mbox_Reg, (int)message.mbox);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Remote_Request_Msgseg_Reg, (int)message.msgseg);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Remote_Request_Dw_Size_Reg, (int)message.dw_Size);
    memcpy (Contexts_Pe[instId].bufs.rio_MP_Remote_Request_Message_Data, message.data, message.dw_Size * sizeof(RIO_DW_T) );
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_MP_Remote_Request_Flag_Reg, 1);

    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Doorbell_Remote_Request
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Doorbell_Remote_Request
 *
 * Returns: result of Rio_Doorbell_Remote_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Doorbell_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_DB_INFO_T doorbell_Info )
{
    int instId;
    unsigned long cur_CSR;

    instId = (int)context;
    
    cur_CSR = GetIntegerRegValue(Contexts_Pe[instId].regs.rio_Doorbell_CSR);

    if ( ((cur_CSR >> DORBELL_FULL_POS) & 1) == 1 ) return RIO_RETRY;
    if ( ((cur_CSR >> DORBELL_BUSY_POS) & 1) == 1 ) return RIO_RETRY;
    if ( ((cur_CSR >> DORBELL_FAILED_POS) & 1) == 1 ) return RIO_ERROR;
    if ( ((cur_CSR >> DORBELL_ERROR_POS) & 1) == 1 ) return RIO_ERROR;

    
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Doorbell_Remote_Request_Doorbell_Info_Reg, (int)doorbell_Info);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Doorbell_Remote_Request_Flag_Reg, 1);

    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Port_Write_Remote_Request
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Port_Write_Remote_Request
 *
 * Returns: result of Rio_Port_Write_Remote_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Port_Write_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T dw_Size,
    RIO_UCHAR_T sub_DW_Pos,
    RIO_DW_T *data )
{
    int instId;
    unsigned long cur_CSR;

    instId = (int)context;

    cur_CSR = GetIntegerRegValue(Contexts_Pe[instId].regs.rio_Write_Port_CSR);

    if ( ((cur_CSR >> WPORT_FULL_POS) & 1) == 1 ) return RIO_RETRY;
    if ( ((cur_CSR >> WPORT_BUSY_POS) & 1) == 1 ) return RIO_RETRY;
    if ( ((cur_CSR >> WPORT_FAILED_POS) & 1) == 1 ) return RIO_ERROR;
    if ( ((cur_CSR >> WPORT_ERROR_POS) & 1) == 1 ) return RIO_ERROR;


    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Port_Write_Remote_Request_Dw_Size_Reg, (int)dw_Size);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Port_Write_Remote_Request_Sub_DW_Pos_Reg, (int)sub_DW_Pos);
    memcpy (Contexts_Pe[instId].bufs.rio_Port_Write_Remote_Request_Data, data, dw_Size * sizeof(RIO_DW_T) );
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Port_Write_Remote_Request_Flag_Reg, 1);

    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Memory_Request
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Memory_Request
 *
 * Returns: result of Rio_Memory_Request routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Memory_Request (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_UCHAR_T dw_Size,
    RIO_MEMORY_REQ_TYPE_T req_Type,
    RIO_BOOL_T is_GSM,
    RIO_UCHAR_T be )
{
    int instId = (int)context;

    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Address_Reg, (int)address);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Extended_Address_Reg, (int)extended_Address);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Xamsbs_Reg, (int)xamsbs);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Dw_Size_Reg, (int)dw_Size);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Req_Type_Reg, (int)req_Type);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Is_GSM_Reg, (int)is_GSM);
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Be_Reg, (int)be);
    /*Last flag value updating*/
    SetIntegerRegValue(Contexts_Pe[instId].regs.rio_Memory_Request_Flag_Reg, 1);

    return 0;
}


/***************************************************************************
 * Function : SW_Rio_Read_Dir
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Read_Dir
 *
 * Returns: result of Rio_Read_Dir routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Read_Dir (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_SH_MASK_T *sharing_Mask )
{
    int instId = (int)context;
    unsigned long index;

/*Access parameters of gsm*/
    unsigned int gsm_XAMSBS = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_XAMSBS);
    unsigned long gsm_Ext = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_Ext_Address);
    unsigned long gsm_Base = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_Start_Address);
    unsigned long gsm_Size = RIO_BYTES_IN_DW *
        GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_DW_Size);


    if ( ( xamsbs == gsm_XAMSBS) && ((address - gsm_Base) >= 0) && ((address - gsm_Base) < gsm_Size) &&
        ((Contexts_Pe[instId].priv_Data.ext_Address_Enabled == RIO_FALSE) || (extended_Address == gsm_Ext)))
    {
        index = (address - gsm_Base) / (RIO_BYTES_IN_DW * Contexts_Pe[instId].priv_Data.coh_Granule_DW_Size);
        if (index < Contexts_Pe[instId].priv_Data.memory_Directory_Size)
        {
            *sharing_Mask = Contexts_Pe[instId].priv_Data.memory_Directory[index];
            return RIO_OK;
        }
        else
        {
            SW_Pe_Wrapper_Message(instId, "Error", "Read_Directory - out of memory directory");
            return RIO_ERROR;
        }
    }
    else
    {
        SW_Pe_Wrapper_Message(instId, "Error", "Read_Directory - address is out of GSM space");
        return RIO_ERROR;
    }
}


/***************************************************************************
 * Function : SW_Rio_Write_Dir
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Write_Dir
 *
 * Returns: result of Rio_Write_Dir routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Write_Dir (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_SH_MASK_T sharing_Mask )
{
    int instId = (int)context;
    unsigned long index;

    unsigned int gsm_XAMSBS = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_XAMSBS);
    unsigned long gsm_Ext = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_Ext_Address);
    unsigned long gsm_Base = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_Start_Address);
    unsigned long gsm_Size = RIO_BYTES_IN_DW *
        GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_DW_Size);


    if ( ( xamsbs == gsm_XAMSBS) && ((address - gsm_Base) >= 0) && ((address - gsm_Base) < gsm_Size) &&
        ((Contexts_Pe[instId].priv_Data.ext_Address_Enabled == RIO_FALSE) || (extended_Address == gsm_Ext)))
    {
        index = (address - gsm_Base) / (RIO_BYTES_IN_DW * Contexts_Pe[instId].priv_Data.coh_Granule_DW_Size);
        if (index < Contexts_Pe[instId].priv_Data.memory_Directory_Size)
        {
            Contexts_Pe[instId].priv_Data.memory_Directory[index] = sharing_Mask;
            return RIO_OK;
        }
        else
        {
            SW_Pe_Wrapper_Message(instId, "Error", "Write_Directory - out of memory directory");
            return RIO_ERROR;
        }
    }
    else
    {
        SW_Pe_Wrapper_Message(instId, "Error", "Write_Directory - address is out of GSM space");
        return RIO_ERROR;
    }
}




/***************************************************************************
 * Function : SW_Rio_Tr_Remote_IO
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Tr_Remote_IO
 *
 * Returns: result of Rio_Tr_Remote_IO routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Tr_Remote_IO (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T *address,
    RIO_ADDRESS_T *extended_Address,
    RIO_UCHAR_T *xamsbs )
{
    int instId = (int)context;

    unsigned int io_XAMSBS = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_XAMSBS);
    unsigned long io_Ext = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_Ext_Address);
    unsigned long io_Base = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_Start_Address);
    unsigned long io_Size = RIO_BYTES_IN_DW *
        GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_DW_Size);


    if ((*xamsbs == io_XAMSBS) && ((*address - io_Base) >= 0) && ((*address - io_Base) < io_Size) &&
        ((Contexts_Pe[instId].priv_Data.ext_Address_Enabled == RIO_FALSE) || (*extended_Address == io_Ext)) )
    {
/*
        *address = You can assign this parameter to any value; 
        *extended_Address = You can assign this parameter to any value;
        *xamsbs = You can assign this parameter to any value;
*/
/*        SW_Pe_Wrapper_Message(instId, "Info", "Rem IO Request translated OK");*/
        return RIO_OK;  /* this is local IO address */
    }
    else
    {
        SW_Pe_Wrapper_Message(instId, "Info", "Mem IO Request translation - MISSED");
        return RIO_ERROR;  /* error, this is not local IO address */
    }
}


/***************************************************************************
 * Function : SW_Rio_Remote_Conf_Read
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Remote_Conf_Read
 *
 * Returns: result of Rio_Remote_Conf_Read routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Remote_Conf_Read (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long *data )
{
    int instId = (int)context;
    return SW_Rio_Register_Access(instId, config_Offset, data,  REG_READ);
}


/***************************************************************************
 * Function : SW_Rio_Remote_Conf_Write
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Remote_Conf_Write
 *
 * Returns: result of Rio_Remote_Conf_Write routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Remote_Conf_Write (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long data )
{
    int instId = (int)context;

    return SW_Rio_Register_Access(instId, config_Offset, &data,  REG_WRITE);
}


/*****************************************************************************/
/*     End of callbacks implementation                                       */
/*****************************************************************************/

/***************************************************************************
 * Function : SW_Put_Data_Pe
 *
 * Description: Routine for access to internal buffers
 *
 * Returns:  
 *
 * Notes: The routines is generated by WrapGen
 *
 **************************************************************************/
void SW_Put_Data_Pe()
{
    int instId = (PUT_IND_T) acc_fetch_tfarg_int(1);
    PUT_IND_T type = (PUT_IND_T) acc_fetch_tfarg_int(2);    /*Type of the array*/
    int index;                                                 /*Index in the array*/


    index = acc_fetch_tfarg_int(3);

/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);


    if ((index >= PLI_MAX_ARRAY_SIZE) || (index < 0))
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", "Put_Data - index incorrect");
        return;
    }

    switch (type)
    {
        case RIO_GSM_REQUEST_RQ_DATA:
            
            Contexts_Pe[instId].bufs.Rio_GSM_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pe[instId].bufs.Rio_GSM_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_IO_REQUEST_RQ_DATA:
            Contexts_Pe[instId].bufs.Rio_IO_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pe[instId].bufs.Rio_IO_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_MESSAGE_REQUEST_RQ_DATA:
            Contexts_Pe[instId].bufs.Rio_Message_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pe[instId].bufs.Rio_Message_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_CONF_REQUEST_RQ_DATA:
            Contexts_Pe[instId].bufs.Rio_Conf_Request_Rq_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pe[instId].bufs.Rio_Conf_Request_Rq_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_SNOOP_RESPONSE_DATA:
            Contexts_Pe[instId].bufs.Rio_Snoop_Response_Data[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pe[instId].bufs.Rio_Snoop_Response_Data[index].ls_Word = acc_fetch_tfarg_int(5);
            break;
        case RIO_SET_MEM_DATA_DW:
            Contexts_Pe[instId].bufs.Rio_Set_Mem_Data_Dw[index].ms_Word = acc_fetch_tfarg_int(4);
            Contexts_Pe[instId].bufs.Rio_Set_Mem_Data_Dw[index].ls_Word = acc_fetch_tfarg_int(5);
            break;

        case RIO_INIT_PARAM_SET_REMOTE_DEV_ID:
            Rio_Init_Param_Set_Remote_Dev_ID[index] = acc_fetch_tfarg_int(4);
            break;
        default:
            SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", "The internal buffer with specified number is not exist");
    }
}


/***************************************************************************
 * Function : SW_Get_Data_Pe
 *
 * Description: Routine for access to internal buffers
 *
 * Returns:  
 *
 * Notes: The routines is generated by WrapGen
 *
 **************************************************************************/
void SW_Get_Data_Pe()
{
    int instId = acc_fetch_tfarg_int(1);
    GET_IND_T type = (GET_IND_T) acc_fetch_tfarg_int(2);    /*Type of the array*/
    int index;                                                    /*Index in the array*/
    WORD_POS_T pos;                                               /*Least or most significant word*/


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    if (type == RIO_GET_CONF_REG_DATA)
    {
        tf_putp(0, Contexts_Pe[instId].bufs.rio_Get_Conf_Reg_Data);
        return;
    }


    index = acc_fetch_tfarg_int(3);
    pos = (WORD_POS_T)acc_fetch_tfarg_int(4);

    if ( (index >= PLI_MAX_ARRAY_SIZE) || (index < 0))
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", "Get_Data - index incorrect");
        return;
    }



    switch (type)
    {
        case RIO_GSM_REQUEST_DONE_DATA:
            if (pos == MSW) tf_putp(0, Contexts_Pe[instId].bufs.rio_GSM_Request_Done_Data[index].ms_Word);
            else tf_putp(0, Contexts_Pe[instId].bufs.rio_GSM_Request_Done_Data[index].ls_Word);
            break;
        case RIO_IO_REQUEST_DONE_DATA:
            if (pos == MSW) tf_putp(0, Contexts_Pe[instId].bufs.rio_IO_Request_Done_Data[index].ms_Word);
            else tf_putp(0, Contexts_Pe[instId].bufs.rio_IO_Request_Done_Data[index].ls_Word);
            break;
        case RIO_CONFIG_REQUEST_DONE_DATA:
            if (pos == MSW) tf_putp(0, Contexts_Pe[instId].bufs.rio_Config_Request_Done_Data[index].ms_Word);
            else tf_putp(0, Contexts_Pe[instId].bufs.rio_Config_Request_Done_Data[index].ls_Word);
            break;
        case RIO_MP_REMOTE_REQUEST_MESSAGE_DATA:
            if (pos == MSW) tf_putp(0, Contexts_Pe[instId].bufs.rio_MP_Remote_Request_Message_Data[index].ms_Word);
            else tf_putp(0, Contexts_Pe[instId].bufs.rio_MP_Remote_Request_Message_Data[index].ls_Word);
            break;
        case RIO_PORT_WRITE_REMOTE_REQUEST_DATA:
            if (pos == MSW) tf_putp(0, Contexts_Pe[instId].bufs.rio_Port_Write_Remote_Request_Data[index].ms_Word);
            else tf_putp(0, Contexts_Pe[instId].bufs.rio_Port_Write_Remote_Request_Data[index].ls_Word);
            break;
        case RIO_GET_MEM_DATA_DW:
            if (pos == MSW) tf_putp(0, Contexts_Pe[instId].bufs.rio_Get_Mem_Data_Dw[index].ms_Word);
            else tf_putp(0, Contexts_Pe[instId].bufs.rio_Get_Mem_Data_Dw[index].ls_Word);
            break;
        default:
            SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", 
                "The internal callback's buffer with specified number is not exist");
    }
}


/***************************************************************************
 * Function : SetIntegerRegValue
 *
 * Description: Sets register regName to specified integer value 
 *
 * Returns: void
 *
 * Notes:   
 *
 **************************************************************************/
void SetIntegerRegValue(handle hReg, int value)
{
    static s_acc_value val = {accIntVal};    /* value  */
    static s_setval_delay delay = {{0, 0, 0, 0.0}, accNoDelay}; /* delay - NO */

    /*check handle*/
    if (hReg == (handle)0)
    {
        io_printf("PE wrapper: Warning register's handle is NULL\n");
        return;
    }
    
    /* set register value */
    val.value.integer = value;
    
    if (acc_set_value(hReg, &val, &delay))
    {
        io_printf ("PE Wrapper: Error during access to the verilog register");
        tf_dofinish(); /* finish if error */
    }
}

/***************************************************************************
 * Function : GetIntegerRegValue
 *
 * Description: Gets integer value of register regName 
 *
 * Returns: Integer value of the requested register
 *
 * Notes:   
 *
 **************************************************************************/
int GetIntegerRegValue(handle hReg)
{
    static s_acc_value val = {accIntVal};       /* value  */

    /* set register value */
    acc_fetch_value(hReg, "%%", &val);

    return val.value.integer;
}



