/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\prototypes.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Description: Prototypes of all wrapper functions
*
* Notes:       Prototype of this file is generated automaticaly by WrapGen
*
******************************************************************************/

#ifndef PROTOTYPES_PE_H
#define PROTOTYPES_PE_H

void SW_Pe_Wrapper_Message (int id, char *type, char *msg); /*simple error handler */
void SW_Init ();    /*Initialize wrapper*/
void SW_Close ();    /*Closes pli engine*/
void SW_Rio_GSM_Request ();
void SW_Rio_IO_Request ();
void SW_Rio_Message_Request ();
void SW_Rio_Doorbell_Request ();
void SW_Rio_Conf_Request ();
void SW_Rio_Snoop_Response ();
void SW_Rio_Set_Mem_Data ();
void SW_Rio_Get_Mem_Data ();
void SW_Rio_Set_Conf_Reg ();
void SW_Rio_Get_Conf_Reg ();
void SW_Rio_Start_Reset ();
void SW_Rio_Init ();
void SW_Rio_Clock_Edge ();
void SW_Rio_Get_Pins ();
void SW_Put_Data();
void SW_Get_Data_Pe();

void SW_Rio_Clock_Edge_New ();

void SetIntegerRegValue(handle hReg, int value);
int GetIntegerRegValue(handle hReg);
/*access to register routine*/
int SW_Rio_Register_Access(
    int instId, 
    int config_Offset, 
    unsigned long *data,  
    REG_ACCESS_T acc_Type);

/* Callbacks */
int SW_Rio_Local_Response (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_LOCAL_RTYPE_T response );

int SW_Rio_GSM_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code,
    RIO_WORD_T dw_Size,
    RIO_DW_T *data );

int SW_Rio_IO_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code,
    RIO_WORD_T dw_Size,
    RIO_DW_T *data );

int SW_Rio_MP_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code );

int SW_Rio_Doorbell_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code );

int SW_Rio_Config_Request_Done (
    RIO_CONTEXT_T context,
    RIO_TAG_T trx_Tag,
    RIO_REQ_RESULT_T result,
    RIO_UCHAR_T err_Code,
    RIO_WORD_T dw_Size,
    RIO_DW_T *data );

int SW_Rio_Snoop_Request (
    RIO_CONTEXT_T context,
    RIO_SNOOP_REQ_T rq );

int SW_Rio_MP_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_REMOTE_MP_REQ_T message );

int SW_Rio_Doorbell_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_DB_INFO_T doorbell_Info );

int SW_Rio_Port_Write_Remote_Request (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T dw_Size,
    RIO_UCHAR_T sub_DW_Pos,
    RIO_DW_T *data );

int SW_Rio_Memory_Request (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_UCHAR_T dw_Size,
    RIO_MEMORY_REQ_TYPE_T req_Type,
    RIO_BOOL_T is_GSM,
    RIO_UCHAR_T be );

int SW_Rio_Read_Dir (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_SH_MASK_T *sharing_Mask );

int SW_Rio_Write_Dir (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_SH_MASK_T sharing_Mask );

int SW_Rio_Tr_Local_IO (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T *address,
    RIO_ADDRESS_T *extended_Address,
    RIO_UCHAR_T *xamsbs,
    RIO_TR_INFO_T *transport_Info );

int SW_Rio_Tr_Remote_IO (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T *address,
    RIO_ADDRESS_T *extended_Address,
    RIO_UCHAR_T *xamsbs );

int SW_Rio_Route_GSM (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_TR_INFO_T *transport_Info );

int SW_Rio_Remote_Conf_Read (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long *data );

int SW_Rio_Remote_Conf_Write (
    RIO_CONTEXT_T context,
    RIO_CONF_OFFS_T config_Offset,
    unsigned long data );

int SW_Rio_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_BYTE_T tframe,
    RIO_BYTE_T td,
    RIO_BYTE_T tdl,
    RIO_BYTE_T tclk );

int SW_Rio_User_Msg (
    RIO_CONTEXT_T context,
    char *user_Msg );

#ifndef PE_WRAP_SERIAL_ONLY
int SW_Rio_Granule_Received (
    RIO_CONTEXT_T context,
    RIO_GRANULE_STRUCT_T *granule_Struct );
#endif


int SW_Rio_Request_Received (
    RIO_CONTEXT_T context,
    RIO_REMOTE_REQUEST_T *packet_Struct );

int SW_Rio_Response_Received (
    RIO_CONTEXT_T context,
    RIO_RESPONSE_T *packet_Struct );

int SW_Rio_Symbol_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *symbol_Buffer,
    RIO_UCHAR_T buffer_Size );

int SW_Rio_Packet_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *packet_Buffer,
    unsigned int buffer_Size,
    RIO_UCHAR_T *packet_Length_In_Granules );

void SW_RIO_Model_Create_Instance ();
/*Function for callback trays initialization*/
void Initialize_CBTray(void);
void Initialize_Serial_CBTray(void);

/*serial prototypes*/
void SW_RIO_Serial_Model_Create_Instance ();
void SW_Rio_Serial_Init ();
void SW_Rio_Serial_Get_Pins ();
void SW_Rio_Serial_Get_Pins_New ();

#ifndef PE_WRAP_PAR_ONLY
int SW_Rio_Serial_Set_Pins (
    RIO_CONTEXT_T context,
        RIO_SIGNAL_T tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3       /* transmit data on lane 3*/ );

#endif


#endif /*PROTOTYPES_PE_H*/
