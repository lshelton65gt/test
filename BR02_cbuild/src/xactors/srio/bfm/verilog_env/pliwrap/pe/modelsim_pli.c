#define MODELSIM_PLI_C
/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\modelsim_pli.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  File to link pli wrapper to Mentor Grahics ModelSIM
*
* Notes:        This file is generated automaticaly by WrapGen
*
******************************************************************************/

#include "veriuser.h"

#ifdef _NC_VLOG_
#include "vxl_veriuser.h"
#endif


extern void SW_Init();
extern void SW_Close();
extern void SW_Rio_GSM_Request();
extern void SW_Rio_IO_Request();
extern void SW_Rio_Message_Request();
extern void SW_Rio_Doorbell_Request();
extern void SW_Rio_Conf_Request();
extern void SW_Rio_Snoop_Response();
extern void SW_Rio_Set_Mem_Data();
extern void SW_Rio_Get_Mem_Data();
extern void SW_Rio_Set_Conf_Reg();
extern void SW_Rio_Get_Conf_Reg();
extern void SW_Rio_Start_Reset();
extern void SW_Rio_Init();
extern void SW_Rio_Clock_Edge();
extern void SW_Rio_Clock_Edge_New();

extern void SW_Rio_Get_Pins();
extern void SW_RIO_Model_Create_Instance();
extern void SW_Put_Data_Pe();
extern void SW_Get_Data_Pe();
extern void SW_Add_Register_Handlers();

extern void SW_RIO_Serial_Model_Create_Instance ();
extern void SW_Rio_Serial_Init ();
extern void SW_Rio_Serial_Get_Pins ();
extern void SW_Rio_Serial_Get_Pins_New ();

extern void SW_Rio_Enable_RND_Idle_Generator();
extern void SW_Rio_Force_EOP_Insertion();

extern void SW_Rio_Set_Retry_Generator();



/***************************************************************************
 * Function : RIO_Sizetf_8
 *
 * Description: Routine for size detection
 *
 * Returns: 8 - size for 8-bit variables
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_8()
{
    return 8; /*size for 8-bit variables*/
}



/***************************************************************************
 * Function : RIO_Sizetf_16
 *
 * Description: Routine for size detection
 *
 * Returns: 16 - size for 16-bit variables
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_16()
{
    return 16; /*size for 16-bit variables*/
}



/***************************************************************************
 * Function : RIO_Sizetf_32
 *
 * Description: Routine for size detection
 *
 * Returns: 32 - size for 32-bit variables
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_32()
{
    return 32; /*size for 32-bit variables*/
}



/***************************************************************************
 * Function : RIO_Sizetf_64
 *
 * Description: Routine for size detection
 *
 * Returns: 64 - size for 64-bit variables
 *
 * Notes:  
 *
 **************************************************************************/
int RIO_Sizetf_64()
{
    return 64; /*size for 64-bit variables*/
}

s_tfcell veriusertfs[] = {
    {
        usertask,           /* tells whether Verilog task or function */
        0,                  /* data argument of callback function */
        0,                  /* checktf */
        0,  /* size of returned data if function */
        SW_Init,            /* pointer to C function associated with Verilog task or function */
        0,                  /* misctf */
        "$HW_Init"          /* verilog name */
    },
    {usertask, 0, 0, 0, SW_Close, 0, "$HW_Close"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_GSM_Request, 0, "$HW_Rio_GSM_Request"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_IO_Request, 0, "$HW_Rio_IO_Request"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Message_Request, 0, "$HW_Rio_Message_Request"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Doorbell_Request, 0, "$HW_Rio_Doorbell_Request"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Conf_Request, 0, "$HW_Rio_Conf_Request"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Snoop_Response, 0, "$HW_Rio_Snoop_Response"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Set_Mem_Data, 0, "$HW_Rio_Set_Mem_Data"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Get_Mem_Data, 0, "$HW_Rio_Get_Mem_Data"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Set_Conf_Reg, 0, "$HW_Rio_Set_Conf_Reg"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Get_Conf_Reg, 0, "$HW_Rio_Get_Conf_Reg"},
    {usertask, 0, 0, 0, SW_Rio_Start_Reset, 0, "$HW_Rio_Start_Reset"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Init, 0, "$HW_Rio_Init"},
    {usertask, 0, 0, 0, SW_Rio_Clock_Edge, 0, "$HW_Rio_Clock_Edge"},
    {usertask, 0, 0, 0, SW_Rio_Get_Pins, 0, "$HW_Rio_Get_Pins"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_RIO_Model_Create_Instance, 0, "$HW_RIO_Model_Create_Instance"},
    {usertask, 0, 0, 0, SW_Put_Data_Pe, 0, "$HW_Put_Data_Pe"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Get_Data_Pe, 0, "$HW_Get_Data_Pe"},
    {usertask, 0, 0, 0, SW_Add_Register_Handlers, 0, "$HW_Add_Register_Handlers"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Set_Retry_Generator, 0, "$HW_Rio_Set_Retry_Generator"},
/*serial specific routines*/
    {userfunction, 0, 0, RIO_Sizetf_32, SW_RIO_Serial_Model_Create_Instance, 0, "$HW_RIO_Serial_Model_Create_Instance"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Serial_Init, 0, "$HW_Rio_Serial_Init"},
    {usertask, 0, 0, 0, SW_Rio_Serial_Get_Pins, 0, "$HW_Rio_Serial_Get_Pins"},

    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Enable_RND_Idle_Generator, 0, "$HW_Rio_Enable_RND_Idle_Generator"},
    {userfunction, 0, 0, RIO_Sizetf_32, SW_Rio_Force_EOP_Insertion, 0, "$HW_Rio_Force_EOP_Insertion"},

    {usertask, 0, 0, 0, SW_Rio_Clock_Edge_New, 0, "$HW_Rio_Clock_Edge_New"},
    {usertask, 0, 0, 0, SW_Rio_Serial_Get_Pins_New, 0, "$HW_Rio_Serial_Get_Pins_New"},

    { 0 } /* last entry must be 0 */
};

p_tfcell Bootstrap ()
{
    return (veriusertfs);
}

