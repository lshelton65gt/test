#******************************************************************************
#*
#* COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
#*
#* The code is the property of Motorola St.Petersburg Software Development
#* and is Motorola Confidential Proprietary Information
#*
#* The copyright notice above does not evidence any
#* actual or intended publication of such source code.
#*
#* $Element: /export/tristar9/u5/sqe/mnt/sqevob/template/source/file.shell$ 
#* $Author: knutson $ 
#* $Revision: 1.1 $ 
#* $VOB: /export/tristar9/u5/sqe/mnt/sqevob $ 
#*
#******************************************************************************
# this makefile builds relocatable object file for RIO model, wrappered by PLI
#

CC              =   gcc
TARGET          =   libwraprio_pe.so

TARGET_NC       =   libpli.so

INCLUDE_NC      =   -I$(RIO_PATH)/interfaces/common -I$(RIO_PATH)/interfaces/parallel_interfaces -I$(RIO_PATH)/interfaces/serial_interfaces\
                    -I$(RIO_PATH)/src/interfaces/common -I$(RIO_PATH)/src/interfaces/parallel_interfaces -I$(RIO_PATH)/src/interfaces/serial_interfaces\
                    -I$(CDS_INST_DIR)/tools/include -I$(RIO_TS_PATH)/interfaces

DEBUG	    	=   -DNDEBUG

DEFINES         =   $(DEBUG) 

LIBS            =  

CFLAGS          =   -c 
INCLUDE         = -I./ -I$(RIO_PATH)/interfaces/common -I$(RIO_PATH)/interfaces/parallel_interfaces -I$(RIO_PATH)/interfaces/serial_interfaces\
                    -I$(RIO_PATH)/src/interfaces/common -I$(RIO_PATH)/src/interfaces/parallel_interfaces -I$(RIO_PATH)/src/interfaces/serial_interfaces\
                    -I$(RIO_TS_PATH)/interfaces -I$(VCS_HOME)/include

LFLAGS          =   $(prof) -G

OBJS            =   wrapper_pe.o pe_user_func.o pe_par_specific.o pe_serial_specific.o

PARALLEL_OBJS   =   wrapper_pe.o pe_user_func.o pe_par_specific.o

SERIAL_OBJS   =   wrapper_pe.o pe_user_func.o pe_serial_specific.o

PLI_PATH        =   $(RIO_TS_PATH)/pliwrap

LD_LIBRARY_PATH =   :$(RIO_PATH)/models/rio:$(PLI_PATH)/pe:$(RIO_TS_PATH)/bin:$(RIO_TS_PATH)/pliwrap/pe

AIX_FLAGS       =   -G -bE:wrapper.exp -bI:wrapper.imp -brtl -lc -L$(RIO_PATH)/models/rio -lrio
###############################################################################
# print the usage info
all	:
		@echo ""
		@echo "Insufficient number of arguments."
		@echo "To build the PLI-wrapped RIO model object file for the VCS simulator, execute:"
		@echo "    make wrap_vcs - for the parallel and serial models"
		@echo "    make wrap_par_vcs - for the parallel model only"
		@echo "    make wrap_serial_vcs - for the parallel model only"
		@echo "To build the PLI-wrapped RIO model object file for the NC Verilog simulator, execute:"
		@echo "    make wrap_nc - for the parallel and serial models"
		@echo "    make wrap_par_nc - for the parallel model only"
		@echo "    make wrap_serial_nc - for the parallel model only"
		@echo "To build the PLI-wrapped RIO model object file for VCS simulator for AIX, execute:"
		@echo "    make wrap_aix_vcs - for the parallel and serial models"
		@echo "    make wrap_aix_par_vcs - for the parallel model only"
		@echo "To clean the wrapped model data, execute:"
		@echo "    make clean"
                
# make target
$(TARGET) :	$(OBJS)
		ld  $(LFLAGS) -L$(RIO_PATH)/models/rio/ -L$(RIO_TS_PATH)/bin $(OBJS) $(LIBS) -o $(TARGET)

#clean data
clean :		
		rm -f *.o
		rm -f *.so

#make pl relocatable object
wrap_vcs :		$(TARGET)

wrap_par_vcs :	$(PARALLEL_OBJS)
		ld  $(LFLAGS) -L$(RIO_PATH)/models/rio/ -L$(RIO_TS_PATH)/bin $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)

wrap_serial_vcs :	$(SERIAL_OBJS)
		ld  $(LFLAGS) -L$(RIO_PATH)/models/rio/ -L$(RIO_TS_PATH)/bin $(SERIAL_OBJS) $(LIBS) -o $(TARGET)

%.o :		%.c
		$(CC) $(CFLAGS) $(INCLUDE) -o $*.o $<

#make wrapper for NC Verilog simulator
wrap_nc : 
		$(CC) modelsim_pli.c -c $(INCLUDE_NC) -D_NC_VLOG_ $(DEFINES) -o veriuser.o
		$(CC) pe_par_specific.c -c $(INCLUDE_NC) -D_NC_VLOG_ $(DEFINES) -o pe_par_specific.o
		$(CC) pe_serial_specific.c -c $(INCLUDE_NC) -D_NC_VLOG_ $(DEFINES) -o pe_serial_specific.o
		$(CC) pe_user_func.c -c $(INCLUDE_NC) -D_NC_VLOG_ $(DEFINES) -o pe_user_func.o
		$(CC) wrapper_pe.c -c $(INCLUDE_NC) -D_NC_VLOG_ $(DEFINES) -o wrapper_pe.o
		ld -G -L$(RIO_PATH)/models/rio veriuser.o $(OBJS) -lrio -o $(TARGET_NC)
wrap_par_nc : 
		$(CC) modelsim_pli.c -c $(INCLUDE_NC) -DPE_WRAP_PAR_ONLY -D_NC_VLOG_ $(DEFINES) -o veriuser.o
		$(CC) pe_par_specific.c -c $(INCLUDE_NC) -DPE_WRAP_PAR_ONLY -D_NC_VLOG_ $(DEFINES) -o pe_par_specific.o
		$(CC) pe_user_func.c -c $(INCLUDE_NC) -DPE_WRAP_PAR_ONLY -D_NC_VLOG_ $(DEFINES) -o pe_user_func.o
		$(CC) wrapper_pe.c -c $(INCLUDE_NC) -DPE_WRAP_PAR_ONLY -D_NC_VLOG_ $(DEFINES) -o wrapper_pe.o
		ld -G -L$(RIO_PATH)/models/rio veriuser.o $(PARALLEL_OBJS) -lrio -o $(TARGET_NC)

wrap_serial_nc : 
		$(CC) modelsim_pli.c -c $(INCLUDE_NC) -DPE_WRAP_SERIAL_ONLY -D_NC_VLOG_ $(DEFINES) -o veriuser.o
		$(CC) pe_serial_specific.c -c $(INCLUDE_NC) -DPE_WRAP_SERIAL_ONLY -D_NC_VLOG_ $(DEFINES) -o pe_serial_specific.o
		$(CC) pe_user_func.c -c $(INCLUDE_NC) -DPE_WRAP_SERIAL_ONLY -D_NC_VLOG_ $(DEFINES) -o pe_user_func.o
		$(CC) wrapper_pe.c -c $(INCLUDE_NC) -DPE_WRAP_SERIAL_ONLY -D_NC_VLOG_ $(DEFINES) -o wrapper_pe.o
		ld -G -L$(RIO_PATH)/models/rio veriuser.o $(SERIAL_OBJS) -lrio -o $(TARGET_NC)

#target for AIX platform
wrap_aix_vcs :	$(OBJS)
		ld  $(LFLAGS) $(AIX_FLAGS) -L$(RIO_PATH)/models/rio/ -L$(RIO_TS_PATH)/bin $(OBJS) $(LIBS) -o $(TARGET)

wrap_aix_par_vcs :	$(PARALLEL_OBJS)
		ld  $(LFLAGS) $(AIX_FLAGS) -L$(RIO_PATH)/models/rio/ -L$(RIO_TS_PATH)/bin $(PARALLEL_OBJS) $(LIBS) -o $(TARGET)

###############################################################################
             
