/******************************************************************************
*
*       COPYRIGHT 2001-2003 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\pe_user_func.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:     This routines is user-defined and can't be implemented in the verilog
*
* Notes:        
*
******************************************************************************/

#ifndef _NC_VLOG_
    #include "acc_user.h"
    #ifdef _MODELSIM_
    #include "veriuser.h"
    #else /*_MODELSIM_ not defined*/
    #include "vcsuser.h"
    #endif /*_MODELSIM_*/
#else /*_NC_VLOG_ defined*/
    #include "acc_user.h"
    #include "veriuser.h"
    #include "vxl_veriuser.h"
#endif /*_NC_VLOG_*/


#include <stdio.h>
#include <memory.h>
#include "wrapper_pe.h" 
#include "prototypes.h" 


typedef struct
{
    handle demonstration_Reg_500;
    handle demonstration_Reg_504;
    handle dest_Id_Reg;
    handle enable_Internal_IO_Routing_Reg;
    handle mailbox_CSR_Reg;
    handle doorbell_CSR_Reg;
} RIO_CONF_REGS_PE_T;

#define  REG_500_ADDR       0x500
#define  REG_504_ADDR       0x504
#define  MAILBOX_CSR_CONF_OFFSET 0x40
#define  DOORBELL_CSR_CONF_OFFSET 0x44

extern RIO_CONTEXT_PE_T * Contexts_Pe;
extern int Pe_Inst_Num;
extern int Rio_Pe_Wrap_Initialized;

/*array of registers for configuration requests handling*/
RIO_CONF_REGS_PE_T Conf_Reg[PLI_MAX_ARRAY_SIZE];

#define BUF_SIZE 255

/***************************************************************************
 * Function : SW_Rio_User_Msg
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_User_Msg
 *
 * Returns: result of Rio_User_Msg routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_User_Msg (
    RIO_CONTEXT_T context,
    char *user_Msg )
{
    io_printf ("Pe%i:time = %i: %s\n", (int)context, tf_gettime(), user_Msg);
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Tr_Local_IO
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Tr_Local_IO
 *
 * Returns: result of Rio_Tr_Local_IO routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Tr_Local_IO (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T *address,
    RIO_ADDRESS_T *extended_Address,
    RIO_UCHAR_T *xamsbs,
    RIO_TR_INFO_T *transport_Info )
{
    int instId = (int)context;
    int i;

    unsigned int io_XAMSBS = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_XAMSBS);
    unsigned long io_Ext = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_Ext_Address);
    unsigned long io_Base = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_Start_Address);
    unsigned long io_Size = RIO_BYTES_IN_DW *
        GetIntegerRegValue(Contexts_Pe[instId].regs.mem_IO_Space_DW_Size);

/*Insert you code after this comment*/
    if ( RIO_FALSE == (RIO_BOOL_T)GetIntegerRegValue( Conf_Reg[instId].enable_Internal_IO_Routing_Reg))
    {
        /*if internal routing is disabled then transport info taked from dest_Id_Reg register*/
        *transport_Info = GetIntegerRegValue(Conf_Reg[instId].dest_Id_Reg);
        return RIO_ROUTE_TO_REMOTE;  /* assumed that in this case packet always routed to remote device */
    }

    /*followed code implements internal routing*/
    if ((*xamsbs == io_XAMSBS) && ((*address - io_Base) >= 0) && ((*address - io_Base) < io_Size) &&
        ((Contexts_Pe[instId].priv_Data.ext_Address_Enabled == RIO_FALSE) || (*extended_Address == io_Ext)) )
    {
/*
        *address = You can assign this parameter to any value; 
        *extended_Address = You can assign this parameter to any value;
        *xamsbs = You can assign this parameter to any value;
*/
        return RIO_ROUTE_TO_LOCAL;  /* this is local IO address */
    }
    else
    {
        *transport_Info = 1;
/*        *transport_Info = You can assign this parameter to any value;*/
/*The next lines search apropriate PE for working with passed address*/
        for (i = 0; i < Pe_Inst_Num; i++)
        {
            if (i != instId) 
            {
                io_XAMSBS = GetIntegerRegValue(Contexts_Pe[i].regs.mem_IO_Space_XAMSBS);
                io_Ext = GetIntegerRegValue(Contexts_Pe[i].regs.mem_IO_Space_Ext_Address);
                io_Base = GetIntegerRegValue(Contexts_Pe[i].regs.mem_IO_Space_Start_Address);
                io_Size = RIO_BYTES_IN_DW *
                    GetIntegerRegValue(Contexts_Pe[i].regs.mem_IO_Space_DW_Size);

                if ((*xamsbs == io_XAMSBS) && ((*address - io_Base) >= 0) && ((*address - io_Base) < io_Size) &&
                    ((Contexts_Pe[instId].priv_Data.ext_Address_Enabled == RIO_FALSE) || (*extended_Address == io_Ext)) )
                {   
                /*The pe for this address is found - packet will be routed to it*/
                    *transport_Info = i;
                    return RIO_ROUTE_TO_REMOTE;
                }
            }
        }
        SW_Pe_Wrapper_Message(instId, "Info", "Local IO Request translation FAILED, suppose request is remote");

        return RIO_ROUTE_TO_REMOTE;  /* this is not local IO address */
    }
}


/***************************************************************************
 * Function : SW_Rio_Route_GSM
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Route_GSM
 *
 * Returns: result of Rio_Route_GSM routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Route_GSM (
    RIO_CONTEXT_T context,
    RIO_ADDRESS_T address,
    RIO_ADDRESS_T extended_Address,
    RIO_UCHAR_T xamsbs,
    RIO_TR_INFO_T *transport_Info )
{
    int instId = (int)context;
    int i;

    unsigned int gsm_XAMSBS = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_XAMSBS);
    unsigned long gsm_Ext = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_Ext_Address);
    unsigned long gsm_Base = GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_Start_Address);
    unsigned long gsm_Size = RIO_BYTES_IN_DW *
        GetIntegerRegValue(Contexts_Pe[instId].regs.mem_GSM_Space_DW_Size);

    if ( (xamsbs == gsm_XAMSBS) && (gsm_Base != 0) && ((address - gsm_Base) >= 0) && ((address - gsm_Base) < gsm_Size) &&
        ((Contexts_Pe[instId].priv_Data.ext_Address_Enabled == 0) || (extended_Address == gsm_Ext)) )
    {
        return RIO_ROUTE_TO_LOCAL;  /* this is local GSM address */
    }
    else
    {
        *transport_Info = 0;
        for (i = 0; i < Pe_Inst_Num; i++)
        {
            if (i != instId) 
            {
                gsm_XAMSBS = GetIntegerRegValue(Contexts_Pe[i].regs.mem_GSM_Space_XAMSBS);
                gsm_Ext = GetIntegerRegValue(Contexts_Pe[i].regs.mem_GSM_Space_Ext_Address);
                gsm_Base = GetIntegerRegValue(Contexts_Pe[i].regs.mem_GSM_Space_Start_Address);
                gsm_Size = RIO_BYTES_IN_DW * GetIntegerRegValue(Contexts_Pe[i].regs.mem_GSM_Space_DW_Size);

                if ( (xamsbs == gsm_XAMSBS) && (gsm_Base != 0) && 
                    ((address - gsm_Base) >= 0) && ((address - gsm_Base) < gsm_Size) &&
                    ((Contexts_Pe[i].priv_Data.ext_Address_Enabled == 0) || (extended_Address == gsm_Ext)) )
                {   /* address is local for this PE: so this PE is the destination for the request */
                    *transport_Info = i;
                    return RIO_ROUTE_TO_REMOTE;
                }
            }
        }
        SW_Pe_Wrapper_Message(instId, "Info", "GSM Request translation FAILED, suppose request is remote");
        return RIO_ROUTE_TO_REMOTE;  /* this is not local GSM address */
    }
}


/***************************************************************************
 * Function : SW_Rio_Request_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Request_Received
 *
 * Returns: result of Rio_Request_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Request_Received (
    RIO_CONTEXT_T context,
    RIO_REMOTE_REQUEST_T *packet_Struct )
{
    /*int instId = (int)context;*/
    /*Insert you code here*/
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Response_Received
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Response_Received
 *
 * Returns: result of Rio_Response_Received routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Response_Received (
    RIO_CONTEXT_T context,
    RIO_RESPONSE_T *packet_Struct )
{
    /*int instId = (int)context;*/
    /*Insert you code here*/
    return RIO_OK;

}


/***************************************************************************
 * Function : SW_Rio_Symbol_To_Send
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Symbol_To_Send
 *
 * Returns: result of Rio_Symbol_To_Send routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Symbol_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T *symbol_Buffer,
    RIO_UCHAR_T buffer_Size )
{
    /*int instId = (int)context;*/
    /*Insert you code here*/
    return RIO_OK;
}


/***************************************************************************
 * Function : SW_Rio_Packet_To_Send
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_Packet_To_Send
 *
 * Returns: result of Rio_Packet_To_Send routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Packet_To_Send (
    RIO_CONTEXT_T context,
    RIO_UCHAR_T * packet_Buffer,
    unsigned int buffer_Size,
    RIO_UCHAR_T *packet_Length_In_Granules )
{
    /*int instId = (int)context;*/
    /*Insert you code here*/
    return RIO_OK;
}

/***************************************************************************
 * Function : SW_Rio_Register_Access
 *
 * Description: Provides access to registers for read/write
 *
 * Returns: RIO_OK if all right 
 *            RIO_ERROR othervise
 *
 * Notes:    
 *
 **************************************************************************/
int SW_Rio_Register_Access(int instId, int config_Offset, unsigned long *data,  REG_ACCESS_T acc_Type)
{
    char buf[BUF_SIZE];

    if (acc_Type == REG_READ)
    {
        if (config_Offset == REG_500_ADDR)
        {
            *data = (unsigned long) GetIntegerRegValue(Conf_Reg[instId].demonstration_Reg_500);
            return RIO_OK;
        }
        if (config_Offset == REG_504_ADDR)
        {
            *data = (unsigned long) GetIntegerRegValue(Conf_Reg[instId].demonstration_Reg_504);
            return RIO_OK;
        }
        if (config_Offset == MAILBOX_CSR_CONF_OFFSET)
        {
            *data = (unsigned long) GetIntegerRegValue(Conf_Reg[instId].mailbox_CSR_Reg);
            return RIO_OK;
        }
        if (config_Offset == DOORBELL_CSR_CONF_OFFSET)
        {
            *data = (unsigned long) GetIntegerRegValue(Conf_Reg[instId].doorbell_CSR_Reg);
            return RIO_OK;
        }

        sprintf(buf, "Access to register for reading; offset = %X; - need change user-defined routine", config_Offset);
        SW_Pe_Wrapper_Message(instId, "Info", buf);        
        /*Realize code for reading from register*/
        *data = 0;
    }
    else 
    {
        if (config_Offset == REG_500_ADDR)
        {
            SetIntegerRegValue(Conf_Reg[instId].demonstration_Reg_500, *data);
            return RIO_OK;
        }
        if (config_Offset == REG_504_ADDR)
        {
            SetIntegerRegValue(Conf_Reg[instId].demonstration_Reg_504, *data);
            return RIO_OK;
        }

        sprintf(buf, "Access to register for writing; offset = %X - need change user-defined routine", config_Offset);
        SW_Pe_Wrapper_Message(instId, "Info", buf);        
        /*Realize code for writing to register*/
    }

    return RIO_OK;
}

/***************************************************************************
 * Function : SW_Add_Register_Handlers
 *
 * Description: Provides access to registers handlers 
 *              for configuration requests commands
 *
 * Returns: 
 *            
 *
 * Notes: It is necessary to call this routine before sinulation   
 *          The constants in the acc_handle_tfarg routines specifies
 *          number of argument in the arguments lists
 *
 **************************************************************************/
void SW_Add_Register_Handlers()
{
    int k = 0;
    int instId = acc_fetch_tfarg_int(1);    /*instance id*/


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);

    
    Conf_Reg[instId].dest_Id_Reg = acc_handle_tfarg(k + 2);
    Conf_Reg[instId].enable_Internal_IO_Routing_Reg = acc_handle_tfarg(k + 3);
    Conf_Reg[instId].mailbox_CSR_Reg = acc_handle_tfarg(k + 4);
    Conf_Reg[instId].doorbell_CSR_Reg = acc_handle_tfarg(k + 5);
    Conf_Reg[instId].demonstration_Reg_500 = acc_handle_tfarg(k + 6);
    Conf_Reg[instId].demonstration_Reg_504 = acc_handle_tfarg(k + 7);
    
}


