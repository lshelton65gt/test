/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\pli_registers.vh $ 
* $Author: knutson $ 
* $Revision: 1.1 $ 
* $VOB: \riosuite $ 
*
* History:      Use the ClearCase command "History"
*               to display revision history information
*
* Description: Registers set for data exchange between PLI wrapper (written in C)
*				and PE environment (written in Verilog).
*
* Notes:       This file shall be included to Verilog modules
*
******************************************************************************/


/* registers driven by PLI*/
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_gsm_request_done_trx_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_gsm_request_done_result_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_gsm_request_done_err_code_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_gsm_request_done_dw_size_reg; 
    reg rio_gsm_request_done_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_io_request_done_trx_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_io_request_done_result_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_io_request_done_err_code_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_io_request_done_dw_size_reg; 
    reg rio_io_request_done_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_request_done_trx_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_request_done_result_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_request_done_err_code_reg; 
    reg rio_mp_request_done_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_doorbell_request_done_trx_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_doorbell_request_done_result_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_doorbell_request_done_err_code_reg; 
    reg rio_doorbell_request_done_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_config_request_done_trx_tag_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_config_request_done_result_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_config_request_done_err_code_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_config_request_done_dw_size_reg; 
    reg rio_config_request_done_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_snoop_request_address_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_snoop_request_extended_address_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_snoop_request_xamsbs_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_snoop_request_lttype_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_snoop_request_ttype_reg; 
    reg rio_snoop_request_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_remote_request_msglen_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_remote_request_ssize_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_remote_request_letter_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_remote_request_mbox_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_remote_request_msgseg_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_mp_remote_request_dw_size_reg; 
    reg rio_mp_remote_request_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_doorbell_remote_request_doorbell_info_reg; 
    reg rio_doorbell_remote_request_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_port_write_remote_request_dw_size_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_port_write_remote_request_sub_dw_pos_reg; 
    reg rio_port_write_remote_request_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_memory_request_address_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_memory_request_extended_address_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_memory_request_xamsbs_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_memory_request_dw_size_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_memory_request_req_type_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_memory_request_is_gsm_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_memory_request_be_reg; 
    reg rio_memory_request_flag_reg; 

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_tframe_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_td_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_tdl_reg; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_pl_set_pins_tclk_reg; 
    reg rio_pl_set_pins_flag_reg; 
//Serial pin interface
    reg rio_pl_set_pins_tlane0_reg; 
    reg rio_pl_set_pins_tlane1_reg; 
    reg rio_pl_set_pins_tlane2_reg; 
    reg rio_pl_set_pins_tlane3_reg; 
    reg rio_pl_set_serial_pins_flag_reg; 
//end of serial pin interface

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] rio_doorbell_CSR;

//Serial pin 10-bits interface
    reg [0 : 9] rio_pl_set_pins_tlane0_10_reg; 
    reg [0 : 9] rio_pl_set_pins_tlane1_10_reg; 
    reg [0 : 9] rio_pl_set_pins_tlane2_10_reg; 
    reg [0 : 9] rio_pl_set_pins_tlane3_10_reg; 
    reg [0 : 9] rio_pl_set_serial_pins_10bits_flag_reg; 
//end of serial pin 10-bits interface


    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_gsm_space_ext_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_gsm_space_start_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_gsm_space_xamsbs;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_gsm_space_dw_size;

    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_io_space_xamsbs;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_io_space_ext_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_io_space_start_address;
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] mem_io_space_dw_size;


/*registers for configuration req*/
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] reg_500; 
    reg [0 : `PLI_LONG_REGISTER_SIZE - 1] reg_504; 

/*registers for the disabling\enabling internal IO packet routing procedure*/
/*if follow register sets to RIO_TRUE Then routing procedure which is implemented in the wrapper
  if sets to RIO_FALSE then dest_id field of packet will be taked from the dest_id_reg register*/
    reg enable_internal_io_routing_reg; 
    reg [0 : 31]dest_id_reg;    /*value of the dest_id field in case when internal routing routine is disabled*/