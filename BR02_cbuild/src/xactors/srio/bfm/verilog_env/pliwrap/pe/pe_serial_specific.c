/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\pe_serial_specific.c $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* Functions:
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contains routines which are necessary only for the serial mode
*
* Notes:
******************************************************************************/

#include <stdlib.h>

#ifndef _NC_VLOG_
    #include "acc_user.h"
    #ifdef _MODELSIM_
    #include "veriuser.h"
    #else /*_MODELSIM_ not defined*/
    #include "vcsuser.h"
    #endif /*_MODELSIM_*/
#else /*_NC_VLOG_ defined*/
    #include "acc_user.h"
    #include "veriuser.h"
    #include "vxl_veriuser.h"
#endif /*_NC_VLOG_*/

#include "wrapper_pe.h"
#include "prototypes.h"

extern RIO_SERIAL_MODEL_FTRAY_T Rio_Serial_Model_Ftray_T_Tray;
/*flag which signalized that wrapper is initialized*/
extern int Rio_Pe_Wrap_Initialized;

/*Contexts for instances*/
extern RIO_CONTEXT_PE_T *Contexts_Pe;

extern int Pe_Max_Inst_Num;  /*Maximum instance number*/
extern int Pe_Inst_Num;  /*Current allocated instances number*/

/*Callback tray*/
extern RIO_SERIAL_MODEL_CALLBACK_TRAY_T Rio_Serial_Model_Callback_Tray_T_Tray;

/* Flags to handle 10bit interface */
extern RIO_BOOL_T ten_Bit_Interface;
extern RIO_BOOL_T set_Serial_Pins_10bit_Flag_Reg;

/*Internal input buffer for SW_Rio_Init routine's variable*/ 
extern RIO_TR_INFO_T Rio_Init_Param_Set_Remote_Dev_ID[PLI_MAX_ARRAY_SIZE];


/*implementation of routines*/


/***************************************************************************
 * Function : SW_RIO_Model_Create_Instance
 *
 * Description: Software part of PLI function pair to provide
 *              Instance creation and binding
 *
 * Returns: Identificator of instance if All right or -1 if error
 *
 * Notes: The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_RIO_Serial_Model_Create_Instance ()
{
    int k;
    int instId;
    int res;
/*Input parameters*/
    RIO_MODEL_INST_PARAM_T param;
/* End of parameters defintion*/

    /*check that wrapper is initialized*/
    if (Rio_Pe_Wrap_Initialized == RIO_FALSE)
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", "Create_Instnce - PE Wrapper isn't initialized");    
        tf_putp(0, (int)RIO_PE_ABSENT_INST_ID);
        return;
    }

    k = 0;
/*Instance ID calculating*/
    instId = Pe_Inst_Num++;
/*  If no more memory for instances available then raise an error */
    if (Pe_Inst_Num > Pe_Max_Inst_Num)
    {
        SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", "Too many instances created");
        tf_putp(0, (int)RIO_PE_ABSENT_INST_ID);
        return;
    }
/*Parameters accessing*/
    param.ext_Address_Support = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 1 ); 
    param.is_Bridge = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 2 ); 
    param.has_Memory = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 3 ); 
    param.has_Processor = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 4 ); 
    param.coh_Granule_Size_32 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 5 ); 
    param.coh_Domain_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 6 ); 
    param.device_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 7 ); 
    param.device_Vendor_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 8 ); 
    param.device_Rev = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 9 ); 
    param.device_Minor_Rev = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 10 ); 
    param.device_Major_Rev = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 11 ); 
    param.assy_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 12 ); 
    param.assy_Vendor_Identity = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 13 ); 
    param.assy_Rev = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 14 ); 
    param.entry_Extended_Features_Ptr = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 15 ); 
    param.next_Extended_Features_Ptr = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 16 ); 
    param.source_Trx = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 17 ); 
    param.dest_Trx = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 18 ); 
    param.mbox1 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 19 ); 
    param.mbox2 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 20 ); 
    param.mbox3 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 21 ); 
    param.mbox4 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 22 ); 
    param.has_Doorbell = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 23 ); 
    param.pl_Inbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 24 ); 
    param.pl_Outbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 25 ); 
    param.ll_Inbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 26 ); 
    param.ll_Outbound_Buffer_Size = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 27 ); 
/*End of the fields value getting for the param structure*/
/*Get registers handles*/
    Contexts_Pe[instId].regs.rio_GSM_Request_Done_Trx_Tag_Reg = acc_handle_tfarg( k + 28);
    Contexts_Pe[instId].regs.rio_GSM_Request_Done_Result_Reg = acc_handle_tfarg( k + 29);
    Contexts_Pe[instId].regs.rio_GSM_Request_Done_Err_Code_Reg = acc_handle_tfarg( k + 30);
    Contexts_Pe[instId].regs.rio_GSM_Request_Done_Dw_Size_Reg = acc_handle_tfarg( k + 31);
    Contexts_Pe[instId].regs.rio_GSM_Request_Done_Flag_Reg = acc_handle_tfarg( k + 32);

    Contexts_Pe[instId].regs.rio_IO_Request_Done_Trx_Tag_Reg = acc_handle_tfarg( k + 33);
    Contexts_Pe[instId].regs.rio_IO_Request_Done_Result_Reg = acc_handle_tfarg( k + 34);
    Contexts_Pe[instId].regs.rio_IO_Request_Done_Err_Code_Reg = acc_handle_tfarg( k + 35);
    Contexts_Pe[instId].regs.rio_IO_Request_Done_Dw_Size_Reg = acc_handle_tfarg( k + 36);
    Contexts_Pe[instId].regs.rio_IO_Request_Done_Flag_Reg = acc_handle_tfarg( k + 37);

    Contexts_Pe[instId].regs.rio_MP_Request_Done_Trx_Tag_Reg = acc_handle_tfarg( k + 38);
    Contexts_Pe[instId].regs.rio_MP_Request_Done_Result_Reg = acc_handle_tfarg( k + 39);
    Contexts_Pe[instId].regs.rio_MP_Request_Done_Err_Code_Reg = acc_handle_tfarg( k + 40);
    Contexts_Pe[instId].regs.rio_MP_Request_Done_Flag_Reg = acc_handle_tfarg( k + 41);

    Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Trx_Tag_Reg = acc_handle_tfarg( k + 42);
    Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Result_Reg = acc_handle_tfarg( k + 43);
    Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Err_Code_Reg = acc_handle_tfarg( k + 44);
    Contexts_Pe[instId].regs.rio_Doorbell_Request_Done_Flag_Reg = acc_handle_tfarg( k + 45);

    Contexts_Pe[instId].regs.rio_Config_Request_Done_Trx_Tag_Reg = acc_handle_tfarg( k + 46);
    Contexts_Pe[instId].regs.rio_Config_Request_Done_Result_Reg = acc_handle_tfarg( k + 47);
    Contexts_Pe[instId].regs.rio_Config_Request_Done_Err_Code_Reg = acc_handle_tfarg( k + 48);
    Contexts_Pe[instId].regs.rio_Config_Request_Done_Dw_Size_Reg = acc_handle_tfarg( k + 49);
    Contexts_Pe[instId].regs.rio_Config_Request_Done_Flag_Reg = acc_handle_tfarg( k + 50);

    Contexts_Pe[instId].regs.rio_Snoop_Request_Address_Reg = acc_handle_tfarg( k + 51);
    Contexts_Pe[instId].regs.rio_Snoop_Request_Extended_Address_Reg = acc_handle_tfarg( k + 52);
    Contexts_Pe[instId].regs.rio_Snoop_Request_Xamsbs_Reg = acc_handle_tfarg( k + 53);
    Contexts_Pe[instId].regs.rio_Snoop_Request_Lttype_Reg = acc_handle_tfarg( k + 54);
    Contexts_Pe[instId].regs.rio_Snoop_Request_Ttype_Reg = acc_handle_tfarg( k + 55);
    Contexts_Pe[instId].regs.rio_Snoop_Request_Flag_Reg = acc_handle_tfarg( k + 56);

    Contexts_Pe[instId].regs.rio_MP_Remote_Request_Msglen_Reg = acc_handle_tfarg( k + 57);
    Contexts_Pe[instId].regs.rio_MP_Remote_Request_Ssize_Reg = acc_handle_tfarg( k + 58);
    Contexts_Pe[instId].regs.rio_MP_Remote_Request_Letter_Reg = acc_handle_tfarg( k + 59);
    Contexts_Pe[instId].regs.rio_MP_Remote_Request_Mbox_Reg = acc_handle_tfarg( k + 60);
    Contexts_Pe[instId].regs.rio_MP_Remote_Request_Msgseg_Reg = acc_handle_tfarg( k + 61);
    Contexts_Pe[instId].regs.rio_MP_Remote_Request_Dw_Size_Reg = acc_handle_tfarg( k + 62);
    Contexts_Pe[instId].regs.rio_MP_Remote_Request_Flag_Reg = acc_handle_tfarg( k + 63);

    Contexts_Pe[instId].regs.rio_Doorbell_Remote_Request_Doorbell_Info_Reg = acc_handle_tfarg( k + 64);
    Contexts_Pe[instId].regs.rio_Doorbell_Remote_Request_Flag_Reg = acc_handle_tfarg( k + 65);

    Contexts_Pe[instId].regs.rio_Port_Write_Remote_Request_Dw_Size_Reg = acc_handle_tfarg( k + 66);
    Contexts_Pe[instId].regs.rio_Port_Write_Remote_Request_Sub_DW_Pos_Reg = acc_handle_tfarg( k + 67);
    Contexts_Pe[instId].regs.rio_Port_Write_Remote_Request_Flag_Reg = acc_handle_tfarg( k + 68);

    Contexts_Pe[instId].regs.rio_Memory_Request_Address_Reg = acc_handle_tfarg( k + 69);
    Contexts_Pe[instId].regs.rio_Memory_Request_Extended_Address_Reg = acc_handle_tfarg( k + 70);
    Contexts_Pe[instId].regs.rio_Memory_Request_Xamsbs_Reg = acc_handle_tfarg( k + 71);
    Contexts_Pe[instId].regs.rio_Memory_Request_Dw_Size_Reg = acc_handle_tfarg( k + 72);
    Contexts_Pe[instId].regs.rio_Memory_Request_Req_Type_Reg = acc_handle_tfarg( k + 73);
    Contexts_Pe[instId].regs.rio_Memory_Request_Is_GSM_Reg = acc_handle_tfarg( k + 74);
    Contexts_Pe[instId].regs.rio_Memory_Request_Be_Reg = acc_handle_tfarg( k + 75);
    Contexts_Pe[instId].regs.rio_Memory_Request_Flag_Reg = acc_handle_tfarg( k + 76);
/*set pins inerface is changed*/
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane0_Reg = acc_handle_tfarg( k + 77);
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane1_Reg = acc_handle_tfarg( k + 78);
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane2_Reg = acc_handle_tfarg( k + 79);
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane3_Reg = acc_handle_tfarg( k + 80);
    Contexts_Pe[instId].regs.rio_PL_Set_Serial_Pins_Flag_Reg = acc_handle_tfarg( k + 81);

/*Via the next registers the c-routines will read data from verilog*/
    Contexts_Pe[instId].regs.rio_Doorbell_CSR = acc_handle_tfarg( k + 82);
    Contexts_Pe[instId].regs.rio_Write_Port_CSR = acc_handle_tfarg( k + 83);

    Contexts_Pe[instId].regs.mem_GSM_Space_Ext_Address = acc_handle_tfarg( k + 84);
    Contexts_Pe[instId].regs.mem_GSM_Space_Start_Address = acc_handle_tfarg( k + 85);
    Contexts_Pe[instId].regs.mem_GSM_Space_XAMSBS = acc_handle_tfarg( k + 86);
    Contexts_Pe[instId].regs.mem_GSM_Space_DW_Size = acc_handle_tfarg( k + 87);

    Contexts_Pe[instId].regs.mem_IO_Space_XAMSBS = acc_handle_tfarg( k + 88);
    Contexts_Pe[instId].regs.mem_IO_Space_Ext_Address = acc_handle_tfarg( k + 89);
    Contexts_Pe[instId].regs.mem_IO_Space_Start_Address = acc_handle_tfarg( k + 90);
    Contexts_Pe[instId].regs.mem_IO_Space_DW_Size = acc_handle_tfarg( k + 91);

/*mailbox register*/
    Contexts_Pe[instId].regs.mailbox_CSR = acc_handle_tfarg( k + 92);    

/* set pins 10-bit interface */
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane0_10_Reg = acc_handle_tfarg( k + 93);
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane1_10_Reg = acc_handle_tfarg( k + 94);
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane2_10_Reg = acc_handle_tfarg( k + 95);
    Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane3_10_Reg = acc_handle_tfarg( k + 96);
    Contexts_Pe[instId].regs.rio_PL_Set_Serial_Pins_10bit_Flag_Reg = acc_handle_tfarg( k + 97);


    res = RIO_Serial_Model_Create_Instance(&(Contexts_Pe[instId].handle), &param );
    if ( res != RIO_OK )
    {
        io_printf ("Error during instance creating \n");
        tf_putp(0, (int)RIO_PE_ABSENT_INST_ID);
        Pe_Inst_Num--;
        return;
    }

/*Instance binding*/
    /*Fills Callback tray*/
    Initialize_Serial_CBTray();

    Rio_Serial_Model_Callback_Tray_T_Tray.local_Device_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.snoop_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.mailbox_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.doorbell_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.port_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.memory_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.directory_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.address_Translation_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.extended_Features_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.lp_Serial_Interface_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Msg = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Serial_Hooks_Context = (RIO_CONTEXT_T)instId;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Hooks_Context = (RIO_CONTEXT_T)instId;
    res = RIO_Serial_Model_Bind_Instance(Contexts_Pe[instId].handle, &Rio_Serial_Model_Callback_Tray_T_Tray);
    if ( res != RIO_OK )
    {
        io_printf ("Error during instance binding \n");
        tf_putp(0, (int)RIO_PE_ABSENT_INST_ID);
        Pe_Inst_Num--;
        return;
    }
/*End of instance binding*/

/*set current mode of the instance*/
    Contexts_Pe[instId].priv_Data.inst_Mode = PE_INST_SERIAL_MODE;
    
/* Now returns value of current instance ID*/
    tf_putp(0, instId);
}


/***************************************************************************
 * Function : SW_Rio_Init
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_Init routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_Init routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Serial_Init ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_SERIAL_PARAM_SET_T param_Set;
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);
    if (PE_IN_SERIAL_MODE) /*do nothing*/;
    else
    {
        SW_Pe_Wrapper_Message(instId, "Error", "Attempt to invoke serial mode routine for non-serial instance");
        tf_putp(0, RIO_ERROR);
        return;
    } 

    param_Set.transp_Type = ( RIO_PL_TRANSP_TYPE_T ) acc_fetch_tfarg_int( k + 2 ); 
    param_Set.dev_ID = ( RIO_TR_INFO_T ) acc_fetch_tfarg_int( k + 3 );  
    param_Set.remote_Dev_ID = Rio_Init_Param_Set_Remote_Dev_ID; /*reading from internal buffer*/
    param_Set.lcshbar = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 4 ); 
    param_Set.lcsbar = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 5 ); 
    param_Set.is_Host = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 6 ); 
    param_Set.is_Master_Enable = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 7 ); 
    param_Set.is_Discovered = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 8 ); 
    param_Set.config_Space_Size = ( RIO_WORD_T ) acc_fetch_tfarg_int( k + 9 ); 
    param_Set.ext_Address = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 10 ); 
    param_Set.ext_Address_16 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 11 ); 
    param_Set.lp_Serial_Is_1x = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 12 ); 
    param_Set.is_Force_1x_Mode = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 13 ); 
    param_Set.is_Force_1x_Mode_Lane_0 = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 14 );
    k++; 
    param_Set.input_Is_Enable = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 14 ); 
    param_Set.output_Is_Enable = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 15 ); 
    param_Set.silence_Period = ( unsigned int ) acc_fetch_tfarg_int( k + 16 ); 
    param_Set.comp_Seq_Rate = ( unsigned int ) acc_fetch_tfarg_int( k + 17 ); 
    param_Set.status_Sym_Rate = ( unsigned int ) acc_fetch_tfarg_int( k + 18 ); 
    param_Set.discovery_Period = ( unsigned int ) acc_fetch_tfarg_int( k + 19 );
    param_Set.res_For_3_Out = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 20 ); 
    param_Set.res_For_2_Out = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 21 ); 
    param_Set.res_For_1_Out = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 22 ); 
    param_Set.res_For_3_In = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 23 ); 
    param_Set.res_For_2_In = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 24 ); 
    param_Set.res_For_1_In = ( RIO_UCHAR_T ) acc_fetch_tfarg_int( k + 25 ); 

    param_Set.pass_By_Prio = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 26 ); 
    param_Set.transmit_Flow_Control_Support = ( RIO_BOOL_T ) acc_fetch_tfarg_int( k + 27 ); 
    param_Set.ext_Mailbox_Support = (RIO_BOOL_T) acc_fetch_tfarg_int( k + 28 );
    param_Set.enable_LRQ_Resending = (RIO_BOOL_T) acc_fetch_tfarg_int( k + 29 );
    param_Set.icounter_Max = 2;
/*End of the fields value getting for the param_Set structure*/

#ifndef PE_WRAP_PAR_ONLY
    if (PE_IN_SERIAL_MODE)
        tf_putp(0, (*(Rio_Serial_Model_Ftray_T_Tray.rio_Serial_Init))( Contexts_Pe[instId].handle, &param_Set ));
#endif

    
}


/***************************************************************************
 * Function : SW_Rio_Serial_Get_Pins
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Get_Pins routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Get_Pins routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Serial_Get_Pins ()
{
    int k;
    int instId;
/*Input parameters*/
    RIO_SIGNAL_T tlane0;       /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1;       /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2;       /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3;      /* transmit data on lane 3*/
/* End of parameters defintion*/

    k = 0;
    instId = acc_fetch_tfarg_int( k + 1 );


/*The follow macro checks that passed instance ID is correct and wrapper is initialized*/
    Wrap_Check_Id(instId);
    if (PE_IN_SERIAL_MODE) /*do nothing*/;
    else
    {
        SW_Pe_Wrapper_Message(instId, "Error", "Attempt to invoke serial mode routine for non-serial instance");
        return;
    } 

    tlane0 = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 2 ); 
    tlane1 = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 3 ); 
    tlane2 = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 4 ); 
    tlane3 = ( RIO_BYTE_T ) acc_fetch_tfarg_int( k + 5 ); 

    if ((instId < 0) || (instId >= Pe_Inst_Num))
        SW_Pe_Wrapper_Message (instId, "Warning", "Get_Pins is called for non-existing instance");
    else
    {
#ifndef PE_WRAP_PAR_ONLY
        if (PE_IN_SERIAL_MODE)
            (*(Rio_Serial_Model_Ftray_T_Tray.rio_PL_Get_Pins))( Contexts_Pe[instId].handle,
                tlane0 , tlane1 , tlane2 , tlane3 );
#endif
    }
}


/***************************************************************************
 * Function : SW_Rio_Serial_Get_Pins_New
 *
 * Description: Software part of PLI function pair to provide
 *              Access to Rio_PL_Get_Pins routine in the RIO_MODEL_FTRAY_T tray
 *
 * Returns: result of Rio_PL_Get_Pins_New routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Serial_Get_Pins_New ()
{
    int i_Count;
    int instId;
    unsigned short tlane0_10;       /* transmit 10-bit data on lane 0*/
    unsigned short tlane1_10;       /* transmit 10-bit data on lane 1*/
    unsigned short tlane2_10;       /* transmit 10-bit data on lane 2*/
    unsigned short tlane3_10;       /* transmit 10-bit data on lane 3*/
/* End of parameters defintion*/

    instId = acc_fetch_tfarg_int( 1 );

    if (PE_IN_SERIAL_MODE) /*do nothing*/;
    else
    {
        SW_Pe_Wrapper_Message(instId, "Error", "Attempt to invoke serial mode routine for non-serial instance");
        return;
    } 

    tlane0_10 = acc_fetch_tfarg_int( 2 );
    tlane1_10 = acc_fetch_tfarg_int( 3 );
    tlane2_10 = acc_fetch_tfarg_int( 4 );
    tlane3_10 = acc_fetch_tfarg_int( 5 );

    
#ifndef PE_WRAP_PAR_ONLY
    for (i_Count =0; i_Count <= 10; i_Count++)
    {
        if (PE_IN_SERIAL_MODE)
            (*(Rio_Serial_Model_Ftray_T_Tray.rio_PL_Get_Pins))( Contexts_Pe[instId].handle,
                (tlane0_10 >> (9 - i_Count) ) & 1, 
                (tlane1_10 >> (9 - i_Count) ) & 1, 
                (tlane2_10 >> (9 - i_Count) ) & 1, 
                (tlane3_10 >> (9 - i_Count) ) & 1 );
    }
#endif

}

/***************************************************************************
 * Function : Initialize_CBTray
 *
 * Description: Function for callback trays initialization
 *
 * Returns:  
 *
 * Notes: This function is generated automaticaly by WrapGen
 *
 **************************************************************************/
void Initialize_Serial_CBTray(void)
{
/*Initialize callbacks for tray*/
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Local_Response = SW_Rio_Local_Response;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_GSM_Request_Done = SW_Rio_GSM_Request_Done;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_IO_Request_Done = SW_Rio_IO_Request_Done;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_MP_Request_Done = SW_Rio_MP_Request_Done;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Doorbell_Request_Done = SW_Rio_Doorbell_Request_Done;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Config_Request_Done = SW_Rio_Config_Request_Done;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Snoop_Request = SW_Rio_Snoop_Request;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_MP_Remote_Request = SW_Rio_MP_Remote_Request;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Doorbell_Remote_Request = SW_Rio_Doorbell_Remote_Request;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Port_Write_Remote_Request = SW_Rio_Port_Write_Remote_Request;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Memory_Request = SW_Rio_Memory_Request;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Read_Dir = SW_Rio_Read_Dir;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Write_Dir = SW_Rio_Write_Dir;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Tr_Local_IO = SW_Rio_Tr_Local_IO;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Tr_Remote_IO = SW_Rio_Tr_Remote_IO;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Route_GSM = SW_Rio_Route_GSM;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Remote_Conf_Read = SW_Rio_Remote_Conf_Read;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Remote_Conf_Write = SW_Rio_Remote_Conf_Write;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_PL_Set_Pins = SW_Rio_Serial_Set_Pins;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_User_Msg = SW_Rio_User_Msg;
    /*hooks*/
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Request_Received = SW_Rio_Request_Received;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Response_Received = SW_Rio_Response_Received;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Symbol_To_Send = SW_Rio_Symbol_To_Send;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Packet_To_Send = SW_Rio_Packet_To_Send;
    /*serial specific Hooks are not implemented in the PLI wrapper*/

    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Serial_Granule_To_Send = NULL;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Serial_Granule_Received = NULL;
   
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Serial_Character_Column_To_Send = NULL;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Serial_Character_Column_Received = NULL;
   
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Serial_Codegroup_Column_To_Send = NULL;
    Rio_Serial_Model_Callback_Tray_T_Tray.rio_Serial_Codegroup_Received = NULL;
}


/*****************************************************************************/
/*     Callbacks implementation                                              */
/*****************************************************************************/


/***************************************************************************
 * Function : SW_Rio_Serial_Set_Pins
 *
 * Description:  Implementation of RIO callback function 
 *              Rio_PL_Set_Pins
 *
 * Returns: result of Rio_PL_Set_Pins routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
int SW_Rio_Serial_Set_Pins (
    RIO_CONTEXT_T context,
    RIO_SIGNAL_T tlane0,       /* transmit data on lane 0*/
    RIO_SIGNAL_T tlane1,       /* transmit data on lane 1*/
    RIO_SIGNAL_T tlane2,       /* transmit data on lane 2*/
    RIO_SIGNAL_T tlane3       /* transmit data on lane 3*/ )
{
    int instId;
    instId = (int)context;

    if (ten_Bit_Interface == RIO_FALSE)
    {
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane0_Reg, (int)tlane0);
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane1_Reg, (int)tlane1);
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane2_Reg, (int)tlane2);
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane3_Reg, (int)tlane3);
        /*Last flag value updating*/
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Serial_Pins_Flag_Reg, 1);
    }
    else
    {
        /* new regs for 10-bit interface */
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane0_10_Reg, (int)tlane0);
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane1_10_Reg, (int)tlane1);
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane2_10_Reg, (int)tlane2);
        SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Pins_Tlane3_10_Reg, (int)tlane3);
        if (set_Serial_Pins_10bit_Flag_Reg == RIO_TRUE)
            SetIntegerRegValue(Contexts_Pe[instId].regs.rio_PL_Set_Serial_Pins_10bit_Flag_Reg, 1);
    }
    return RIO_OK;
}


/*****************************************************************************/
/*     implementation of stubs for the serial routines                        */
/*****************************************************************************/

#ifdef PE_WRAP_SERIAL_ONLY
/***************************************************************************
 * Function : SW_RIO_Serial_Model_Create_Instance
 *
 * Description: stub for serial part of wrapper
 *
 * Returns:  -1 
 *
 * Notes: 
 *
 **************************************************************************/
void SW_RIO_Model_Create_Instance ()
{
    SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", 
        "Parallel functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_PE_ABSENT_INST_ID);
}    

/***************************************************************************
 * Function : SW_Rio_Serial_Init
 *
 * Description: stub for serial part of wrapper
 *
 * Returns: RIO_ERROR
 *
 * Notes:
 *
 **************************************************************************/
void SW_Rio_Init ()
{
    SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", 
        "Parallel functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Rio_Get_Pins
 *
 * Description: stub for serial part of wrapper
 *
 * Returns: none
 *
 * Notes:    
 *
 **************************************************************************/
void SW_Rio_Get_Pins ()
{
    SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", 
        "Parallel functionality is not implemented in the current delivery");        
}

/***************************************************************************
 * Function : SW_Rio_Enable_RND_Idle_Generator
 *
 * Description: stub
 *
 * Returns: result of Rio_PL_Clock_Edge routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Enable_RND_Idle_Generator()
{
    SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", 
        "Parallel functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

/***************************************************************************
 * Function : SW_Rio_Force_EOP_Insertion
 *
 * Description: Software part of PLI function pair to provide
 *              Force eop at the end of each packet
 *
 * Returns: result of Rio_PL_Clock_Edge routine work
 *
 * Notes:    The template of this function is generated automaticaly
 *
 **************************************************************************/
void SW_Rio_Force_EOP_Insertion()
{
    SW_Pe_Wrapper_Message(RIO_PE_ABSENT_INST_ID, "Error", 
        "Parallel functionality is not implemented in the current delivery");        
    tf_putp(0, RIO_ERROR);
}

#endif /*PE_WRAP_SERIAL_ONLY*/

