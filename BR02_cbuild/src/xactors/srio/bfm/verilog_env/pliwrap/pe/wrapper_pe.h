/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\wrapper_pe.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description: This file contain description c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Description: This file contain prototypes c-realizations of verilog tasks,
*                Which used by the verilog environment
*
* Notes:       Prototype of this file is generated automaticaly by WrapGen
*
******************************************************************************/


#ifndef PE_WRAPPER_H
#define PE_WRAPPER_H

#include "rio_common.h"

#ifndef PE_WRAP_SERIAL_ONLY
#include "rio_parallel_model.h"
#endif

#ifndef PE_WRAP_PAR_ONLY
#include "rio_serial_model.h"
#endif



#define RIO_PLI_SHORT_REGISTER_SIZE                 8
#define RIO_PLI_LONG_REGISTER_SIZE                  32
#define RIO_MAX_DATA_SIZE                           64

#define RIO_MAX_DW_SIZE                             32

#define RIO_MAX_NUM_OF_PE                           256
#define RIO_MAX_NUM_OF_SWITCHES                     16

/* Data size convertion constants */

#define RIO_BITS_IN_BYTE                            8
#define RIO_BITS_IN_WORD                            32
#define RIO_BITS_IN_DW                              64
#define RIO_BYTES_IN_WORD                           4
#define RIO_BYTES_IN_DW                             8
#define RIO_WORDS_IN_DW                             2


/*Arrays sizes*/
#define PLI_MAX_ARRAY_SIZE                          255
#define DIRECTORY_ARRAY_SIZE                        16

/*Positions of Dorbell CSR bits*/
#define DORBELL_FULL_POS                            30
#define DORBELL_BUSY_POS                            28
#define DORBELL_FAILED_POS                          27
#define DORBELL_ERROR_POS                           26

/*Positions of Write port CSR bits*/
#define WPORT_FULL_POS                              6
#define WPORT_BUSY_POS                              4
#define WPORT_FAILED_POS                            3
#define WPORT_ERROR_POS                             2

/*MAilbox constants*/
#define MBOX_AVAILABLE                              0x80
#define MBOX_FULL                                   0x40
#define MBOX_EMPTY                                  0x20
#define MBOX_BUSY                                   0x10
#define MBOX_FAILED                                 0x08
#define MBOX_ERROR                                  0x04

/*constant for error Inst_ID value*/
#define RIO_PE_ABSENT_INST_ID                       -1

/*Values for write register routine*/
typedef enum 
{
    REG_READ = 0,
    REG_WRITE
} REG_ACCESS_T;

/*Values for access to internal buffers*/
typedef enum 
{
    LSW = 0,    /*Least significent word*/
    MSW         /*Most significent word*/
} WORD_POS_T;


typedef enum
{
    PE_INST_PAR_MODE = 0,
    PE_INST_SERIAL_MODE
} PE_WRAP_INST_MODE_T;


/* Types for instance identification*/
typedef struct
{
/*mode of wrapper*/
    PE_WRAP_INST_MODE_T inst_Mode;
/*For GSM transactions*/
    RIO_UCHAR_T ext_Address_Enabled;
    RIO_UCHAR_T coh_Granule_DW_Size;
    RIO_UCHAR_T memory_Directory[DIRECTORY_ARRAY_SIZE];
    int memory_Directory_Size;
    
} RIO_PRIVATE_PE_T;

typedef struct
{
    handle rio_GSM_Request_Done_Trx_Tag_Reg;
    handle rio_GSM_Request_Done_Result_Reg;
    handle rio_GSM_Request_Done_Err_Code_Reg;
    handle rio_GSM_Request_Done_Dw_Size_Reg;
    handle rio_GSM_Request_Done_Flag_Reg;

    handle rio_IO_Request_Done_Trx_Tag_Reg;
    handle rio_IO_Request_Done_Result_Reg;
    handle rio_IO_Request_Done_Err_Code_Reg;
    handle rio_IO_Request_Done_Dw_Size_Reg;
    handle rio_IO_Request_Done_Flag_Reg;

    handle rio_MP_Request_Done_Trx_Tag_Reg;
    handle rio_MP_Request_Done_Result_Reg;
    handle rio_MP_Request_Done_Err_Code_Reg;
    handle rio_MP_Request_Done_Flag_Reg;

    handle rio_Doorbell_Request_Done_Trx_Tag_Reg;
    handle rio_Doorbell_Request_Done_Result_Reg;
    handle rio_Doorbell_Request_Done_Err_Code_Reg;
    handle rio_Doorbell_Request_Done_Flag_Reg;

    handle rio_Config_Request_Done_Trx_Tag_Reg;
    handle rio_Config_Request_Done_Result_Reg;
    handle rio_Config_Request_Done_Err_Code_Reg;
    handle rio_Config_Request_Done_Dw_Size_Reg;
    handle rio_Config_Request_Done_Flag_Reg;

    handle rio_Snoop_Request_Address_Reg;
    handle rio_Snoop_Request_Extended_Address_Reg;
    handle rio_Snoop_Request_Xamsbs_Reg;
    handle rio_Snoop_Request_Lttype_Reg;
    handle rio_Snoop_Request_Ttype_Reg;
    handle rio_Snoop_Request_Flag_Reg;

    handle rio_MP_Remote_Request_Msglen_Reg;
    handle rio_MP_Remote_Request_Ssize_Reg;
    handle rio_MP_Remote_Request_Letter_Reg;
    handle rio_MP_Remote_Request_Mbox_Reg;
    handle rio_MP_Remote_Request_Msgseg_Reg;
    handle rio_MP_Remote_Request_Dw_Size_Reg;
    handle rio_MP_Remote_Request_Flag_Reg;

    handle rio_Doorbell_Remote_Request_Doorbell_Info_Reg;
    handle rio_Doorbell_Remote_Request_Flag_Reg;

    handle rio_Port_Write_Remote_Request_Dw_Size_Reg;
    handle rio_Port_Write_Remote_Request_Sub_DW_Pos_Reg;
    handle rio_Port_Write_Remote_Request_Flag_Reg;

    handle rio_Memory_Request_Address_Reg;
    handle rio_Memory_Request_Extended_Address_Reg;
    handle rio_Memory_Request_Xamsbs_Reg;
    handle rio_Memory_Request_Dw_Size_Reg;
    handle rio_Memory_Request_Req_Type_Reg;
    handle rio_Memory_Request_Is_GSM_Reg;
    handle rio_Memory_Request_Be_Reg;
    handle rio_Memory_Request_Flag_Reg;

    handle rio_PL_Set_Pins_Tframe_Reg;
    handle rio_PL_Set_Pins_Td_Reg;
    handle rio_PL_Set_Pins_Tdl_Reg;
    handle rio_PL_Set_Pins_Tclk_Reg;
    handle rio_PL_Set_Pins_Flag_Reg;

/*serial link interface*/
    handle rio_PL_Set_Pins_Tlane0_Reg;
    handle rio_PL_Set_Pins_Tlane1_Reg;
    handle rio_PL_Set_Pins_Tlane2_Reg;
    handle rio_PL_Set_Pins_Tlane3_Reg;
    handle rio_PL_Set_Serial_Pins_Flag_Reg;

/*serial link interface - 10-bits*/
    handle rio_PL_Set_Pins_Tlane0_10_Reg;
    handle rio_PL_Set_Pins_Tlane1_10_Reg;
    handle rio_PL_Set_Pins_Tlane2_10_Reg;
    handle rio_PL_Set_Pins_Tlane3_10_Reg;
    handle rio_PL_Set_Serial_Pins_10bit_Flag_Reg;
//*Via the next registers the c-routines will read data from verilog*/
    handle rio_Doorbell_CSR;
    handle rio_Write_Port_CSR;

    handle mem_GSM_Space_Ext_Address;
    handle mem_GSM_Space_Start_Address;
    handle mem_GSM_Space_XAMSBS;
    handle mem_GSM_Space_DW_Size;

    handle mem_IO_Space_XAMSBS;
    handle mem_IO_Space_Ext_Address;
    handle mem_IO_Space_Start_Address;
    handle mem_IO_Space_DW_Size;
    
    /*Mailbox register*/
    handle mailbox_CSR;


} REGISTERS_PE_T;

typedef struct
{
    /*buffers for the ctray routines*/
    RIO_DW_T rio_GSM_Request_Done_Data[PLI_MAX_ARRAY_SIZE];
    RIO_DW_T rio_IO_Request_Done_Data[PLI_MAX_ARRAY_SIZE];
    RIO_DW_T rio_Config_Request_Done_Data[PLI_MAX_ARRAY_SIZE];
    RIO_DW_T rio_MP_Remote_Request_Message_Data[PLI_MAX_ARRAY_SIZE];
    RIO_DW_T rio_Port_Write_Remote_Request_Data[PLI_MAX_ARRAY_SIZE];
    RIO_DW_T rio_Get_Mem_Data_Dw[PLI_MAX_ARRAY_SIZE];
    unsigned long rio_Get_Conf_Reg_Data;
    /*buffers for the ftray routines*/
    
    /*Internal input buffer for SW_Rio_GSM_Request routine's variable*/ 
    RIO_DW_T Rio_GSM_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_IO_Request routine's variable*/ 
    RIO_DW_T Rio_IO_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_Message_Request routine's variable*/ 
    RIO_DW_T Rio_Message_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_Conf_Request routine's variable*/ 
    RIO_DW_T Rio_Conf_Request_Rq_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_Snoop_Response routine's variable*/ 
    RIO_DW_T Rio_Snoop_Response_Data[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_Set_Mem_Data routine's variable*/ 
    RIO_DW_T Rio_Set_Mem_Data_Dw[PLI_MAX_ARRAY_SIZE];
    /*Internal input buffer for SW_Rio_Init routine's variable*/ 
    RIO_TR_INFO_T Rio_Init_Param_Set_Remote_Dev_ID[PLI_MAX_ARRAY_SIZE];
} BUFFERS_PE_T;

typedef struct
{
    RIO_HANDLE_T handle;           /*Instance id*/
    REGISTERS_PE_T regs;               /*handlers of registers for callbacks*/
    BUFFERS_PE_T bufs;                 /*Internal buffers for array arguments*/
    RIO_PRIVATE_PE_T priv_Data;        /*Private data*/
} RIO_CONTEXT_PE_T;

/* Names of array's indexes for model API routines*/
typedef enum {
    RIO_GSM_REQUEST_RQ_DATA = 0,
    RIO_IO_REQUEST_RQ_DATA,
    RIO_MESSAGE_REQUEST_RQ_DATA,
    RIO_CONF_REQUEST_RQ_DATA,
    RIO_SNOOP_RESPONSE_DATA,
    RIO_SET_MEM_DATA_DW,
    RIO_INIT_PARAM_SET_REMOTE_DEV_ID 
} PUT_IND_T;

/* Names of array's indexes for Callback*/
typedef enum {
    RIO_GSM_REQUEST_DONE_DATA = 0,
    RIO_IO_REQUEST_DONE_DATA,
    RIO_CONFIG_REQUEST_DONE_DATA,
    RIO_MP_REMOTE_REQUEST_MESSAGE_DATA,
    RIO_PORT_WRITE_REMOTE_REQUEST_DATA,
    RIO_GET_MEM_DATA_DW,
    RIO_GET_CONF_REG_DATA
} GET_IND_T;

/*macro which checks that inst id is ok*/
#define Wrap_Check_Id(id)\
{\
    if ( ((id) < 0 ) || ((id) >= Pe_Inst_Num) )\
    {\
        SW_Pe_Wrapper_Message((id), "Error", "incorrect instance id value");\
        return;\
    }\
    if (Rio_Pe_Wrap_Initialized == RIO_FALSE)\
    {\
        SW_Pe_Wrapper_Message((id), "Error", "PE Wrapper isn't initialized");\
        return;\
    }\
}


#define PE_IN_PAR_MODE (Contexts_Pe[instId].priv_Data.inst_Mode == PE_INST_PAR_MODE)

#define PE_IN_SERIAL_MODE (Contexts_Pe[instId].priv_Data.inst_Mode == PE_INST_SERIAL_MODE)

#endif /*PE_WRAPPER_H*/
