/******************************************************************************
*
*       COPYRIGHT 2001-2002 MOTOROLA, ALL RIGHTS RESERVED
*
*       The code is the property of Motorola St.Petersburg Software Development
*       and is Motorola Confidential Proprietary Information.
*
*       The copyright notice above does not evidence any
*       actual or intended publication of such source code.
*
* $Element: R:\riosuite\src\verilog_env\pliwrap\pe\wrapvars.h $ 
* $Author: knutson $ 
* $Revision: 1.2 $ 
* $VOB: \riosuite $ 
*
* History:      Use the the ClearCase command "History"
*               to display revision history information.
*
* Description:  Wrappers variables description
*
* Notes:        This file is generated automaticaly.
*               SHALL INCLUDED ONLY TO THE wrapper_Pe.c FILE
*
******************************************************************************/

#ifndef WRAPVARS_PE_H
#define WRAPVARS_PE_H

#ifndef PE_WRAP_SERIAL_ONLY
/*Function tray*/
RIO_PARALLEL_MODEL_FTRAY_T Rio_Model_Ftray_T_Tray;
/*Callback tray*/
RIO_PARALLEL_MODEL_CALLBACK_TRAY_T Rio_Model_Callback_Tray_T_Tray;
#endif

#ifndef PE_WRAP_PAR_ONLY
/*Function tray*/
RIO_SERIAL_MODEL_FTRAY_T Rio_Serial_Model_Ftray_T_Tray;
/*Callback tray*/
RIO_SERIAL_MODEL_CALLBACK_TRAY_T Rio_Serial_Model_Callback_Tray_T_Tray;
/* Flags to handle 10-bit interface */
RIO_BOOL_T ten_Bit_Interface = RIO_FALSE;
RIO_BOOL_T set_Serial_Pins_10bit_Flag_Reg = RIO_FALSE;
#endif


/*flag which signalized that wrapper is initialized*/
int Rio_Pe_Wrap_Initialized = RIO_FALSE;

/*Contexts for instances*/
RIO_CONTEXT_PE_T *Contexts_Pe;

int Pe_Max_Inst_Num;  /*Maximum instance number*/
int Pe_Inst_Num;  /*Current allocated instances number*/



#endif /*WRAPVARS_PE_H*/
