// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef SRIOX_TRANS_H
#define SRIOX_TRANS_H

#include "srioXDefines.h"
#include "xactors/srio/carbonXSrio.h"
#include "xactors/srio/carbonXSrioTrans.h"
extern "C" {
#include "rio_serial_model.h"
}

/*!
 * \file srioXTrans.h
 * \brief Header file for Common Transaction Class.
 *
 * This file provides the class definition for Common Transaction objects.
 */

/*!
 * \brief Class to represent Common Transaction.
 *
 */
class CarbonXSrioTransClass
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    CarbonXSrioTransClass() {}
    virtual ~CarbonXSrioTransClass() {}

    virtual CarbonXSrioTransType getType() const = 0;
    virtual CarbonXSrioErrorType getError() const
    { return CarbonXSrioError_NoError; }
    
    // Conversion functions
    virtual CarbonXSrioIoRequest *getIoRequest() const
    { return NULL; }
    virtual CarbonXSrioMessage *getMessage() const 
    { return NULL; }
    virtual CarbonXSrioDoorbell *getDoorbell() const 
    { return NULL; }
    virtual CarbonXSrioResponse *getResponse() const
    { return NULL; }

    // Start transation
    virtual SrioxBool start(CarbonXSrioClass *xtor, UInt tag) const = 0;

    // Check if needs response
    virtual SrioxBool needResponse() const = 0;

    // Utility functions
    static SrioxBool checkError(const CarbonXSrioTrans *trans);
    static RIO_DW_T *copyBfmDw(const UInt size,
            const RIO_DW_T *data);
    static RIO_DW_T *copyToBfmDw(const UInt size,
            const CarbonXSrioDword *data);
    static CarbonXSrioDword *copyFromBfmDw(const UInt size,
            const RIO_DW_T *data);
};

#endif
