//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/////////////////////////////////////////////////////////////////////////////
//
// File Name: sparseMem.c
//
// This file implements functions to create and manage sparse memory
// In order to optimize performance the following approach is taken.
// * Memory is allocated one page at a size.
// * Each page stores  WORDS32_PER_PAGE 32 bit data words
// * Addresses are hashed into HASH_BINS hash bins
// * Memory pages that fall into the same has bin are organized into a Red Black tree
//
//
// Note:
// The implementation of the Red Black tree is derived from algorithms
// published by Prof. Mark Allen Weiss
// Reference: www.cs.fiu.edu/~weiss/
//            www.cs.fiu.edu/~weiss/dsaa_c2e/files.html
// 
// Author  : Reinhard Zippelius
//
// Last Change : 
// History :
//
/////////////////////////////////////////////////////////////////////////////


// http://www.cs.fiu.edu/~weiss/dsaa_c2e/files.html

#include "tbp.h"
#include "carbonXSparseMem.h"
#include <stdlib.h>

// These defines may be modified for performance tuning.
#define WORDS32_PER_PAGE 64
#define PAGE_ADDR_BITS (WORDS32_PER_PAGE/8)
#define ADDR_MASK ((1<<(WORDS32_PER_PAGE/8)) -1)
#define HASH_BINS 100000

#define NegInfinity (-10000)

#define Error( Str )        FatalError( Str )
#define FatalError( Str )   fprintf( stderr, "%s\n", Str ), exit( 1 )

typedef  u_int32 redBlackKey_t;
//typedef  int redBlackKey_t;

typedef struct info_t {
  redBlackKey_t  addr;  // used as key
  u_int32         data[WORDS32_PER_PAGE];
  //  u_int32     dataValid[WORDS32_PER_PAGE];
} info_t, *infoRef_t;

typedef struct redBlackNode_t *nodeRef_t;

typedef enum colorType_t { red, black } colorType_t;

struct redBlackNode_t {
  info_t         info;
  redBlackTree_t left;
  redBlackTree_t right;
  colorType_t    color;
};


// Forward declarations
static void DoPrint( redBlackTree_t T );
redBlackTree_t        Initialize( void );
redBlackTree_t        Insert( redBlackKey_t key, redBlackTree_t T );
static redBlackTree_t MakeEmptyRec( redBlackTree_t T );
#if 0
static redBlackTree_t MakeEmpty( redBlackTree_t T );
#endif
inline infoRef_t      Retrieve( nodeRef_t p );

static nodeRef_t NullNode = NULL;  // Needs initialization 
static nodeRef_t X, P, GP, GGP, foundNode;

sparseMem_t carbonXSparseMemCreateNew(){
  return (sparseMem_t)calloc( HASH_BINS, sizeof(redBlackTree_t) );
}

void carbonXSparseMemDestroy( sparseMem_t mem ){
  u_int32          i;
  redBlackTree_t  tree;

  for( i=0; i<HASH_BINS; i++ ) {
    if ( (tree = mem[i]) != NULL ) {
      // DoPrint( tree );
      tree = MakeEmptyRec( tree );
    }
  }

  return;
}

u_int32 carbonXSparseMemReadWord32( sparseMem_t mem, u_int32 addr ) {
  redBlackTree_t t;
  infoRef_t infoRef;
  u_int32    hashBin;
  u_int32    pageAddr;
  u_int32    seed;
  u_int32    inPageAddr;
  u_int32    data;

  pageAddr = (addr & ~(ADDR_MASK)) >> PAGE_ADDR_BITS;
  //MSG_Printf("pageAddr=0x%0x\n",pageAddr);

  // We use the built in random number generator for hashing
  seed = pageAddr;
  inPageAddr = (addr & ADDR_MASK) >> 2;
  srand(seed);
  hashBin = (u_int16)rand();
  //MSG_Printf("rd hashBin=%x\n",hashBin);

  t = mem[hashBin];
  if ( t== NULL ) {
    t = Initialize();
    t->info.addr = (redBlackKey_t)pageAddr;
  }

  t = Insert( (redBlackKey_t)pageAddr, t );
  if ( t != NullNode ) {
    mem[hashBin] = t;
  }

  infoRef =  Retrieve( foundNode );
  data = infoRef->data[ inPageAddr ];
  //DoPrint( mem[hashBin] );

  return data;
}

void carbonXSparseMemWriteWord32( sparseMem_t mem, u_int32 addr, u_int32 data ) {
  redBlackTree_t t;
  infoRef_t      infoRef;
  u_int32        hashBin;
  u_int32        pageAddr;
  u_int32        seed;
  u_int32        inPageAddr;

  pageAddr = (addr & ~(ADDR_MASK)) >> PAGE_ADDR_BITS;

  // We use the built in random number generator for hashing
  seed = pageAddr;
  inPageAddr = (addr & ADDR_MASK) >> 2;
  srand(seed);
  hashBin = (u_int16)rand();

  t = mem[hashBin];
  if ( t== NULL ) {
    mem[hashBin] = t = Initialize();
    t->info.addr = (redBlackKey_t)pageAddr;
  }

  t = Insert( (redBlackKey_t)pageAddr, t );
  if ( t != NullNode ) {
    mem[hashBin] = t;
  }

  infoRef = Retrieve( foundNode );

  infoRef->data[ inPageAddr ] = data;
  // printf( "writing pageAddr=%x. inPageAddr=%x, nodeRef=%x, infoRef=%x\n", pageAddr, inPageAddr, nodeRef, infoRef);
  //DoPrint( mem[hashBin] );

  return;
}

// Initialization procedure 
redBlackTree_t Initialize( void ) {
  redBlackTree_t T;

  if( NullNode == NULL ) {
      NullNode = (nodeRef_t)calloc( 1, sizeof( struct redBlackNode_t ) );
      if( NullNode == NULL ) {
      	FatalError( "Out of space!!!" );
      }
      NullNode->left = NullNode->right = NullNode;
      NullNode->color = black;
      NullNode->info.addr = 12345;
    }

  // Create the header node 
  T = (nodeRef_t)calloc( 1, sizeof( struct redBlackNode_t ) );
  if( T == NULL ) {
    FatalError( "Out of space!!!" );
  }
  T->info.addr = NegInfinity;
  T->left = T->right = NullNode;
  T->color = black;

  return T;
}

void Output( infoRef_t infoRef ) {
  int i;

  printf( "Page Addr=%x", infoRef->addr );

  for( i=0; i<WORDS32_PER_PAGE; i++ ){
    if ( i%8 == 0 ) { printf( "\n" ); }

    printf( " data[%2x]=%8x", i, infoRef->data[i] );
  }

  printf( "\n" );
}

// Print the tree, watch out for NullNode,  and skip header 

static void DoPrint( redBlackTree_t T ) {
  if( T != NullNode ) {
    DoPrint( T->left );
    Output( &T->info );
    DoPrint( T->right );
  }
}

void PrintTree( redBlackTree_t T ) {
  DoPrint( T->right );
}

static redBlackTree_t MakeEmptyRec( redBlackTree_t T ) {
  if( T != NullNode ) {
    MakeEmptyRec( T->left );
    MakeEmptyRec( T->right );
    free( T );
  }
  return NullNode;
}

#if 0
redBlackTree_t MakeEmpty( redBlackTree_t T ) {
  T->right = MakeEmptyRec( T->right );
  return T;
}
#endif


nodeRef_t Find( redBlackKey_t X, redBlackTree_t T ) {
  if( T == NullNode )
    return NullNode;

  if( X < T->info.addr )
    return Find( X, T->left );
  else
    if( X > T->info.addr )
      return Find( X, T->right );
    else
      return T;
}


nodeRef_t FindMin( redBlackTree_t T ) {
  T = T->right;
  while( T->left != NullNode )
    T = T->left;

  return T;
}


nodeRef_t FindMax( redBlackTree_t T ) {
  while( T->right != NullNode )
    T = T->right;
  
  return T;
}

// This function can be called only if k2 has a left child 
// Perform a rotate between a node (k2) and its left child 
// Update heights, then return new root 

static nodeRef_t SingleRotateWithLeft( nodeRef_t k2 ) {
  nodeRef_t k1;

  k1 = k2->left;
  k2->left = k1->right;
  k1->right = k2;

  return k1;  // New root 
}

// This function can be called only if k1 has a right child 
// Perform a rotate between a node (k1) and its right child 
// Update heights, then return new root 

static nodeRef_t SingleRotateWithRight( nodeRef_t k1 ) {
  nodeRef_t k2;

  k2 = k1->right;
  k1->right = k2->left;
  k2->left = k1;

  return k2;  // New root 
}

// Perform a rotation at node X 
// (whose parent is passed as a parameter) 
// The child is deduced by examining key 

static nodeRef_t Rotate( redBlackKey_t key, nodeRef_t Parent ) {

  if( key < Parent->info.addr )
    return Parent->left = key < Parent->left->info.addr ?
      SingleRotateWithLeft( Parent->left ) :
    SingleRotateWithRight( Parent->left );
  else
    return Parent->right = key < Parent->right->info.addr ?
      SingleRotateWithLeft( Parent->right ) :
    SingleRotateWithRight( Parent->right );
}


static void HandleReorient( redBlackKey_t key, redBlackTree_t T ) {
  X->color = red;        // Do the color flip 
  X->left->color = black;
  X->right->color = black;

  if( P->color == red ) {
    // Have to rotate
    GP->color = red;
    if( (key < GP->info.addr) != (key < P->info.addr) )
      P = Rotate( key, GP );  // Start double rotate 
    X = Rotate( key, GGP );
    X->color = black;
  }
  T->right->color = black;  // Make root black 
}


redBlackTree_t Insert( redBlackKey_t key, redBlackTree_t T ) {
  X = P = GP = T;
  NullNode->info.addr = key;

  while( X->info.addr != key ) {
    // Descend down the tree 
    GGP = GP; GP = P; P = X;
    if( key < X->info.addr )
      X = X->left;
    else
      X = X->right;

    if( X->left->color == red && X->right->color == red )
      HandleReorient( key, T );
  }

  if( X != NullNode ) {
    foundNode = X;
    return NullNode;  // Duplicate
  }

  foundNode = X = (nodeRef_t)calloc( 1, sizeof( struct redBlackNode_t ) );

  if( X == NULL )
    FatalError( "Out of space!!!" );

  X->info.addr = key;
  X->left = X->right = NullNode;

  if( key < P->info.addr )  // Attach to its parent 
    P->left = X;
  else
    P->right = X;
  HandleReorient( key, T ); // color it red; maybe rotate 

  return T;
}


#if pfGCC_4_2_4
__attribute__ ((gnu_inline))
#endif
inline infoRef_t Retrieve( nodeRef_t p ) {
  return &p->info;
}
