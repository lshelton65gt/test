//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef SPARSE_MEM_H
#define SPARSE_MEM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

struct redBlackNode_t;
typedef struct redBlackNode_t *redBlackTree_t;
typedef redBlackTree_t *sparseMem_t;

sparseMem_t carbonXSparseMemCreateNew();
u_int32     carbonXSparseMemReadWord32( sparseMem_t mem, u_int32 addr );
void        carbonXSparseMemWriteWord32( sparseMem_t mem, u_int32 addr, u_int32 data );
void        carbonXSparseMemDestroy( sparseMem_t mem );

#ifdef __cplusplus
}
#endif

#endif  // SPARSE_MEM_H
