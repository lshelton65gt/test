//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "util/CarbonPlatform.h"
#include <tbp.h>
#include "carbonXTransaction.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"
#include "carbonXEnet.h"

#define  carbonXEnetMAX_PKT_SIZE 65536

#if pfLP64
#  define Formatw64(w,x) "%" #w "l" #x
#else
#  if pfWINDOWS
#    define Formatw64(w,x) "%" #w "I64" #x
#  else
#    define Formatw64(w,x) "%" #w "ll" #x
#  endif
#endif

static int g_enet_support_debug = 0;

static u_int16 enet_htonl_16(u_int16 s)
{
    return (s >> 8) | (s << 8);
}
static u_int32 enet_htonl_32(u_int32 l)
{
    return ((u_int32) enet_htonl_16((u_int16)l) << 16) | enet_htonl_16((u_int16)(l >> 16));
}
static u_int64 enet_htonl_64 (u_int64 ll) __attribute__ ((unused));
static u_int64 enet_htonl_64(u_int64 ll)
{
    return ((u_int64) enet_htonl_32((u_int32)ll) << 32) | enet_htonl_32((u_int32)(ll >> 32));
}



/*!\brief Packet packing function to handle "endianness". 
 * \par Description:
 * function used to pack the bytes of a packet in an architecture
 * independant manner (big/little endian host) - packet is always 
 * built in a byte-wise big-endian manner.
 */
/*

 Cases:

   (a)

   P: xxx
   F:    yy zzzzzzzz

   Shift field left by (8 - Pbits - Fbits); or in MSB masked by
   0xff >> Pbits; copy remaining bytes; or in P byte at end of
   field masked by 0xff >> (Pbits + Fbits).

   (b)

   P: xxx
   F:    yyyyy zzzzzzzz

   Or in MSB masked by 0xff >> Pbits; copy remaining bytes.

   P: xxx
   F:    yyyyyy zzzzzzzz

   Shift MSB right by (Pbits + Fbits - 8); or in shifted MSB
   masked by 0xff >> Pbits; shift field left by (8 - (Pbits + Fbits - 8)) =
   (16 - Pbits - Fbits); copy remaining bytes; or in P byte at end of
   field masked by 0xff >> (Pbits + Fbits - 8).

         F
   P  01234567
     0*aaaaaaa
     1caaaaaab
     2caaaaabc
     3caaaabcc
     4caaabccc
     5caabcccc
     6cabccccc
     7cbcccccc

*/
static u_int32 AddToPacket(u_int8 **pkt, u_int32 *bit_index, u_int64 field, u_int32 field_bit_count)
{
  u_int8  *fieldP;
  u_int8  *packetP = *pkt;
  u_int32  pBits;  /* Bits in last byte of packet (0 if full byte).  */
  u_int32  fBits;  /* Bits in MSB of field (8 if full byte).  */
  u_int32  fBytes; /* Bytes in field.  */
  u_int32  maskRight;
  u_int32  addedLen; /* Bytes added to packet.  This is the return result.  */
  u_int8   pEndByte;

#ifdef ADD_TO_PACKET_DEBUG
  MSG_Milestone("PDBi_AddToPacket(): pkt == 0x%08lx; bit_index == %ld; field == "
	    "0x%016llx; field_bit_count == %ld.\n",
	    *pkt, *bit_index, field, field_bit_count);
#endif /* ADD_TO_PACKET_DEBUG */

  /* Swizzle field bytes to make them big endian.  */
  field = enet_htonl_64(field);

  pBits = *bit_index & 0x7;
  fBits = field_bit_count & 0x7;

  /* fBytes is always greater than 0, except if field_bit_count is 0,
     which should not happen (invalid input).  */
  fBytes = (field_bit_count + 7) >> 3;

  /* Point to MSB of field.  Note, since field is represented in a big
     endian 64 bit integer, the bytes will be right adjusted.  */
  fieldP = (u_int8 *) &field + (8 - fBytes);

#ifdef ADD_TO_PACKET_DEBUG
  MSG_Milestone("PDBi_AddToPacket(): pBits == %ld; fBits == %ld; fBytes == %ld; "
	    "&field == 0x%08lx; fieldP == 0x%08lx.\n",
	    pBits, fBits, fBytes, &field, fieldP);
#endif /* ADD_TO_PACKET_DEBUG */

  if (pBits == 0 && fBits == 0)
  {
    /* Both packet and field are byte aligned and field is a multiple
       of bytes.  We can use memcpy() directly and we do not need to
       do any stitching of bytes at the ends of the field.  */
    memcpy(packetP, fieldP, fBytes);
    *pkt = packetP + fBytes;
    addedLen = fBytes;
  }
  else if (pBits + fBits == 8)
  {
    /* Case (b) above.  The field when added to the packet ends at a
       byte boundary leaving the packet byte aligned.  We do not need
       to shift the field before copying it.  */
    maskRight = 0xff >> pBits;
    *packetP = (*packetP & ~maskRight) | (*fieldP & maskRight);

    if (fBytes > 1)
      memcpy(packetP + 1, fieldP + 1, fBytes - 1);

    *pkt = packetP + fBytes;
    addedLen = fBytes - 1;
  }
  else if (pBits + fBits < 8 && fBits != 0)
  {
    /* Case (a) above.  We must shift the field before adding the MSB
       and copying the remainder.  */
    field <<= 8 - pBits - fBits;
    maskRight = 0xff >> pBits;

    /* Grab last byte of packet that field will touch.  */
    pEndByte = packetP[fBytes - 1];
    
    *packetP = (*packetP & ~maskRight) | (*fieldP & maskRight);

    if (fBytes > 1)
      memcpy(packetP + 1, fieldP + 1, fBytes - 1);

    /* Restore the original bits in the part of the last byte not
       covered by the field.  This works for fBytes == 1 too.  */
    packetP[fBytes - 1] |= pEndByte & 0xff >> (pBits + fBits);

    *pkt = packetP + fBytes - 1;
    addedLen = pBits == 0 ? fBytes : fBytes - 1;
  }
  else /* pBits + fBits > 8 || fBits == 0 */
  {
    /* Case (c) above.  We must first add in the MSB, and then shift
       the field before copying the remainder.  This is necessary
       because the field may be using all 8 bytes of the u_int64 and
       left shifting could drop bits.  */
    
    if (fBits == 0)
      fBits = 8;

    maskRight = 0xff >> pBits;
    *packetP = (*packetP & ~maskRight) |
	       (*fieldP >> (pBits + fBits - 8) & maskRight);

    field <<= 16 - pBits - fBits; /* == 8 - (pBits + fBits - 8) */

    pEndByte = packetP[fBytes];

    memcpy(packetP + 1, fieldP, fBytes);

    /* Restore the original bits in the part of the last byte not
       covered by the field.  */
    packetP[fBytes] |= pEndByte & 0xff >> (pBits + fBits - 8);

    *pkt = packetP + fBytes;
    addedLen = fBytes;
  }

  *bit_index += field_bit_count;

  return addedLen;
}

/*!
 * \brief function to print the raw bytes of a frame buffer
 * 
 * \par Detailed Description:
 * This function prints the raw bytes of a frame buffer.
 *
 * \param buffer: A pointer to the packet buffer
 * \param length: the length of the packet buffer
 *
 * \return None.
 */
static void DumpRawFrame(u_int8 *buffer,u_int32 length)
{
  u_int32 dword_length,i,j;
  char byte_string[8],word_string[128];

  /*first dump the index string*/
  MSG_Printf("index:   0  1  2  3  4  5  6  7\n");

  MSG_Printf("-------------------------------\n");

  /*printout formats the bytes into 8 byte rows*/
  dword_length = ((length%8)==0) ? (length/8) : ((length/8) +1);

  for(i=0;i<dword_length;i++){
    /*
     * Print the bytes in rows of 8 bytes - checking for unaligned bytes
     */
    word_string[0] = '\0';
 
   for(j=0;j<8;j++){      
      if(((i*8)+j)<length) {
	sprintf(byte_string,"%2.2x ",(buffer[(i*8)+j] & 0xff));
	strcat(word_string,byte_string);
      }
   }
   /*print this whole line*/
   MSG_Printf("0x%4.4x: %s\n",i,word_string);
  
  }
}

/*! \par Description:
 * Function to build the VLAN header
 * This function is handed a pointer to a
 * VLAN header data structure. The function will
 * convert the structure into a contiguous
 * byte stream.

 \param  vlan Pointer to a vlan header struct
 \param  pkt  Pointer to a packet buffer
 \return      Length of vlan header built.
 
*/
static u_int32 BuildVlan(VLAN_HDR_T * vlan, u_int8 ** pkt) {

  u_int32 length = 0;
  u_int32 bit_offset=0;

  if ((vlan->enable)==0) {
    return(length);
  }
  else {
    length+= AddToPacket(pkt, &bit_offset, vlan->tag, 16);
    length+= AddToPacket(pkt, &bit_offset, vlan->id, 16);
    return(length);
  }
}

void carbonXEnetSetDebug(int on_off){
  g_enet_support_debug = on_off;
}

u_int32 carbonXEnetBuildPacket(ENET_PKT_T * enet, u_int8 *pktP) {
  
  
  u_int32 length = 0;
  u_int32 bit_offset=0;
  u_int8  *pkt = pktP;
  u_int32 i;

  // Check if pointer is NULL
  if(pktP == NULL){ 
    MSG_Error("carbonXEnetBuildPacket: Function called with NULL packet pointer.\n");
    return 0;
  }

  else {
    
    /*
     * source and dest address
     */
    length += AddToPacket(&pkt, &bit_offset, enet->da, 48);
    length += AddToPacket(&pkt, &bit_offset, enet->sa, 48);

    /* add VLAN hdr, if it' senabled */
    length += BuildVlan(&enet->vlan, &pkt);
    length += AddToPacket(&pkt, &bit_offset, enet->typelength, 16);

    // Add the payload
    for(i = 0; i < enet->pl_size; i++) {
      length += AddToPacket(&pkt, &bit_offset, enet->payload[i], 8);
    }

    //DumpRawFrame(pktP, length);
    return length;
  }
  
}

void carbonXEnetBuildTransaction(struct carbonXTransactionStruct *trans, ENET_PKT_T * enet) {
  
  u_int32 length;

  // Calculate Length of resulting packet
  length = 
    (6) +                          // Destination Address
    (6) +                          // Source Address;
    (enet->vlan.enable ? 4 : 0) +  // Vlan Header
    (2) +                          // Typelength Header
    (enet->pl_size);               // Payload Size
  
  trans->mTransId ++;
  trans->mOpcode  = TBP_WRITE;
  trans->mAddress = enet->ipg;
  trans->mSize    = length;
  trans->mStatus  = 0;
  trans->mWidth   = TBP_WIDTH_8;
  trans->mHasData = 1;

  // Reallocated transaction buffer to fit the packet
  carbonXReallocateTransactionBuffer(trans);

  // Create the packet
  length = carbonXEnetBuildPacket(enet, trans->mData);
}

void carbonXEnetWrite(ENET_PKT_T * enet) {
    
  u_int8 *pktP = NULL;
  u_int32 size;

  // Allocate Memory for the Packet
  if(pktP == NULL) {
    pktP = (u_int8 *)carbonmem_malloc(carbonXEnetMAX_PKT_SIZE * sizeof(u_int8));
    
    // Make Sure malloc succeded
    if(pktP == NULL) MSG_Panic("Memory Allocation failed when checking Ethernet Packet.\n");
  }

  // Build Packet
  size = carbonXEnetBuildPacket(enet, pktP);
  
  // Send Packet
  carbonXEnetSendPacket(pktP, size, enet->ipg);
 
  // Free Memory
  carbonmem_free(pktP);
    
}

u_int32 carbonXEnetReceivePacket(u_int8 *pkt) {

  u_int32 length = carbonXReadArray8(0, pkt, 0);
  
  if(g_enet_support_debug) {
    MSG_Printf("Received Raw Packet.\n");
    DumpRawFrame(pkt, length);
  }
  return length;
}

void carbonXEnetSendPacket(u_int8 *pkt, u_int32 size, u_int32 ipg){

  if(g_enet_support_debug) {
    MSG_Printf("Sending Raw Packet.\n");
    DumpRawFrame(pkt, size);
  }
  carbonXWriteArray8(ipg, pkt, size);

}

void carbonXEnetPrintDescriptor(ENET_PKT_T pkt){
  MSG_Printf("Destination Address : 0x" Formatw64(012,x) "\n", pkt.da);
  MSG_Printf("Source Address      : 0x" Formatw64(012,x) "\n", pkt.sa);
  MSG_Printf("Type/Length         : %d\n", pkt.typelength);
  MSG_Printf("Inter Packet Gap    : %d\n", pkt.ipg);
  MSG_Printf("Payload Size        : %d\n", pkt.pl_size);
  if (pkt.vlan.enable) {
    MSG_Printf("VLAN Tag            : 0x%x\n", pkt.vlan.tag);
    MSG_Printf("VLAN ID             : 0x%x\n", pkt.vlan.id);
  }

  MSG_Printf("Payload:\n");
  DumpRawFrame(pkt.payload, pkt.pl_size);
}

void carbonXEnetCheckPacket(ENET_PKT_T desc_pkt, u_int8 *act_pkt, u_int32 act_size) {
  
  u_int8 *exp_pkt = NULL;
  u_int32 exp_size;
  u_int32 i;
  BOOL found_error    = FALSE;
  u_int32 first_error = 0;
  
  // Allocate Memory for the Expected packet
  if(exp_pkt == NULL) {
    exp_pkt = (u_int8 *)carbonmem_malloc(carbonXEnetMAX_PKT_SIZE * sizeof(u_int8));
    
    // Make Sure malloc succeded
    if(exp_pkt == NULL) MSG_Panic("Memory Allocation failed when checking Ethernet Packet.\n");
  }

  // Build the expected packet from the Discriptor
  exp_size = carbonXEnetBuildPacket(&desc_pkt, exp_pkt);
  
  MSG_Printf("Check Packet: Expecting packet size: %d", exp_size);

  if(exp_size != act_size) {
    MSG_Printf("\n");
    MSG_Error("Expected and Actual Packet Size differ. Expected = %d, Actual %d.\n", exp_size, act_size) ;
    found_error = 1;
    first_error = 0;
  }

  else {
    for(i = 0; i < exp_size; i++) {
      if(exp_pkt[i] != act_pkt[i]) {
	found_error = TRUE;
	first_error = i;
	break;
      }
    }
  }

  if(found_error) {
    MSG_Printf("\n");
    MSG_Error("Mismatch found in packet. First error on byte: %d\n", first_error);

    MSG_Printf("Expected Packet.");
    for(i = 0; i < exp_size; i++) {
      if(i%8 == 0) MSG_Printf("\n");
      MSG_Printf("%3x", exp_pkt[i]);
    }
    MSG_Printf("\n");

    MSG_Printf("Actual Packet.");
    for(i = 0; i < act_size; i++) {
      if(i%8 == 0) MSG_Printf("\n");
      MSG_Printf("%3x", act_pkt[i]);
    }
    MSG_Printf("\n");
  }
  else MSG_Printf(" : Passed!\n");

  // Free Allocated Memory
  if(exp_pkt != NULL) carbonmem_free(exp_pkt);
  
}
