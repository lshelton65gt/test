/* PDB support functions for the Ethernet transactors.
 *
 *
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 ******************************************************************
 * File Name    : enetcarbonX.h
 * Author       : Goran Knutson  <knutson@zaiqtech.com>                                     
 * Creation Date: 06/01/2005
 *              : 
 * Comments     : Ethernet Transactor defines
 *
 *
 *  9/12/05   Jim Stein   Modified to match latest version
 *****************************************************************/

#ifndef CARBONXENETSVM__H
#define CARBONXENETSVM__H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * ENET transactor configuration defines
 * ENET_FRTR_CONFIG register bit values
 */
#define ENET_GMII        0x00
#define ENET_MII         0x01   
#define ENET_HALF_DUPLEX 0x04
#define ENET_FCS_ENABLE  0x08
#define ENET_PHY_LINK    0x10
#define ENET_RGMII_MODE  0x20
#define ENET_DEFAULT_IPG 0x40

#define ENET_RX_ERR_STATUS_MSB_BIT 0x10  // The number of defined error bits in the 
                                         // rx_err_status register in the RX transactor.
#define ENET_RX_DEFINED_ERR_NUM 0x7      // Totall errors defined in the rx_err_status.
                                         // Modify this constant with new error addition.
/*
 * matches Synthesizable Xactor
 * Register address Map
 */

#define ENET_FRTR_CONFIG   0x0
#define ENET_ERR_CONTROL   0x4
#define ENET_PREAMBLE_H    0x8
#define ENET_PREAMBLE_L    0xC


// defines for rx_xactor 
// These defines should match the corresponding
// defines on the verilog side i.e. for rx_err_status
// register in the enet_mii_rx_xactor.v also should
// should match the array of strings ErrType_str in the
// enet_pdb_support.c which control the output msg from
// the tests. 
#define ENET_EXPECT_NO_ERROR              0x0
#define ENET_EXPECT_TX_ERR_RECV           0x1
#define ENET_EXPECT_SHORT_FRAME_ERR       0x2
#define ENET_EXPECT_BAD_PREAM_ERR         0x4
#define ENET_EXPECT_FCS_ERR               0x8
#define ENET_EXPECT_SHORT_PREAM_ERR       0x10
#define ENET_EXPECT_SHORT_IPG_ERR         0x20
#define ENET_EXPECT_NO_SPD_ERR            0x40



// defines for tx_xactor
// The following defines should match the corresponding
// defines on the verilog side i.e. err_control register 
// in the enet_mii_tx_xactor.v.
#define ENET_FORCE_NO_ERROR            0x0
#define ENET_FORCE_RCV_ERR             0x1
#define ENET_FORCE_SHORT_EVENT_ERR     0x2
#define ENET_FORCE_ALIGN_ERR           0x4
#define ENET_FORCE_BAD_FCS             0x8
#define ENET_FORCE_LONG_FRAME_ERR      0x10
#define ENET_FORCE_SHORT_IPG           0x20

/*
 * Controls/Error defines
 */


// Expected error defines for rx_xactor
#define ENET_FORCE_RX_CRS_ERR          0x8000
#define ENET_FORCE_RX_COL              0x80000000

/*
 * XGMII transactor configuration defines
 * XGMII_FRTR_CONFIG register bit values
 */



/*
 * matches Synthesizable XGMII
 * Register address Map
 */

#define XGMII_FRTR_CONFIG   0x0
#define XGMII_PREAMBLE_L    0x4
#define XGMII_PREAMBLE_H    0x8
#define XAUI_TX_CONFIG      0xC
#define XAUI_RX_CONFIG      0x4
#define XAUI_LINK_STATUS    0x104

//  Bit definitions in TX & RX control register
#define XGMII_RAW_MODE      0x1
#define XGMII_FCS_ENABLE    0x2
#define XGMII_TX_ERR        0x4

#define XAUI_TX_LOOPBACK_ENABLE  0x1
#define XAUI_TX_REVERSE_10BIT    0x2
#define XAUI_RX_LOOPBACK_ENABLE  0x1
#define XAUI_RX_REVERSE_10BIT    0x2

// TX control register only  bits to force an error
#define XGMII_FORCE_NO_ERR  0x00
#define XGMII_FORCE_RCV_ERR 0x04
#define XGMII_FORCE_FCS_ERR 0x08

// These two bits are not direct config bits, but are used to control
//  injection of these errors
#define XGMII_FORCE_IPG_ERR 0x10
#define XGMII_FORCE_PRE_ERR 0x20


// defines for rx_xactor error status 
// These defines must match the corresponding
// defines on the verilog side for rx_err_status
// register in the enet_xgmii_rx_xactor.v also should
// should match the array of strings ErrType_str in the
// enet_pdb_support.c which control the output msg from
// the tests. 
#define XGMII_NO_ERROR     0x00
#define XGMII_RCV_ERR      0x04
#define XGMII_FCS_ERR      0x08
#define XGMII_IPG_ERR      0x10
#define XGMII_PRE_ERR      0x20
#define XAUI_CODE_ERR      0x40

  /*! \struct VLAN_HDR_T
      \brief  Virtual Local Area Network (VLAN) (used within Ethernet Packet)
  */
typedef struct {
  u_int16 tag;
  u_int16 id;
  u_int32 enable :1;
} VLAN_HDR_T;

typedef struct {

  u_int64    da : 48;     //!< Destination MAC Address
  u_int64    sa : 48;     //!< Source MAC Address
  VLAN_HDR_T vlan;        //!< VLAN tagging
  u_int16    typelength;  //!< Indicates either size of data or type of protocol

  u_int8*    payload;
  u_int32    pl_size;     //!< Indicates length of payload in bytes

  u_int32    ipg;         //!< Inter Packet Gap Size

} ENET_PKT_T;

  u_int32 carbonXEnetBuildPacket(ENET_PKT_T * enet, u_int8 *pktP);
  void    carbonXEnetBuildTransaction(struct carbonXTransactionStruct *trans, ENET_PKT_T * enet);
  u_int32 carbonXEnetReceivePacket(u_int8 *pkt);
  void    carbonXEnetSendPacket(u_int8 *pkt, u_int32 size, u_int32 ipg);
  void    carbonXEnetWrite(ENET_PKT_T * enet);
  void    carbonXEnetPrintDescriptor(ENET_PKT_T enet);
  void    carbonXEnetCheckPacket(ENET_PKT_T desc_pkt, u_int8 *act_pkt, u_int32 act_size);
#ifdef __cplusplus
}
#endif

#endif
