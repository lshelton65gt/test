//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`timescale 1ns / 100 ps

module  carbonx_enetmii_rx( sys_clk,   
                            sys_reset_l,
                            TXD,      
                            TX_ER, 
                            TX_EN, 
                            CRS, 
                            COL, 
                            TX_CLK 
			    );

   // MII signals
   input        sys_clk, sys_reset_l;	//system clock and reset
   input [3:0]  TXD;	// Transmit data from the MAC
   input        TX_ER;	// Transmit Error from the MAC
   input        TX_EN;	// Transmit Enable from the MAC
   input        TX_CLK;	// Transmit clock from the MAC
   output       CRS;	// Carrier Sense to the XIF_core
   output       COL;	// Collision detection output to the XIF core

// CRS, COL are valid only in case of half_duplex operation. The function
// of these pins is undefined for full_duplex operation.
// ----------------------------------------------------------------------

`protect
// carbon license crbn_vsp_exec_xtor_e10_100

//**************************************************************************
//                      Parameter Defines
//**************************************************************************

   parameter
	BFMid     =  1,
	ClockId   =  1,		// Which clock I am connected to.
	DataWidth = 32,
	DataAdWid = 11,		// Max is 15
	GCSwid  = 32*12,	// Get CS Data width Max is 512 bytes
	PCSwid  = 32*12;  	// Put CS Data width Max is 512 bytes

//*****************************************************************************
//  File Name   - carbonx_enetmii_rx.v.v 
//  Author      - Rajat Mohan
//  Date        - March 10th 2003
//******************************************************************************
//* $RCSfile$
//* $Author$
//* $Date$
//* 
//*  Description:  Synthesizable Ethernet Receiver BFM and attaches to the MII Tx
//		   interface of the DUT.This test runs in Full mode and 10Mb/s. 
//                 This transactor module is a C-side initiated transactor and consist  
//		   of XIF_core and BFM. The rx transactor receives configuration data
//		   from XIF_core first to set up the duplex, gigamode, clk_10, clk_100
//                 tbi and rmii mode. Then it receives the data from the MAC.
//		   The transactor then process the incoming flits of data and generates
//		   different frame or transmission and collision error signals. 
//		   
//	

// ----------------------------------------------------------------------------
//  Register definition
// ----------------------------------------------------------------------------
//  get_csdata[31:0]   -> rx_config
//  get_csdata[63:32]  -> err_insertion
//  put_csdata[31:0]   <- rx_err_status
//
//  err_insertion[14:0]  -> loss_crs_delay
//  err_insertion[15]    -> force_loss_crs
//  err_insertion[30:16] -> col_byte_num
//  err_insertion[31]    -> force_col
//
//  rx_config[0]     -> sel_clk_10     
//  rx_config[1]     -> sel_clk_100 --> (_config[1:0]==00) -> gigmode    
//  rx_config[2]     -> half_duplex    
//  rx_config[3]     -> fcs_enable     
//  rx_config[4]     -> rmii_mode 	// Reserved for future  
//  rx_config[5]     -> rgmii_mode	// Reserved for future   
//  rx_config[6]     -> tbi_en   	// Reserved for future 
//  rx_config[7]     -> rtbi_en		// Reserved for future    
// 
//
//


// State machine parameters
   parameter  IDLE         = 3'h0; 
   parameter  GET_PREAM_H  = 3'h1;
   parameter  GET_PREAM_L  = 3'h2;
   parameter  GET_DATA     = 3'h3;
   parameter  CRS_EXTEND   = 3'h4;

//***************************************************************************
//                      Include C Interface File
//***************************************************************************  

`include "xif_core.vh"        // module instantiation of XIF_core


 //**************************************************************************
 //                      Internal Reg/Wire Signals 
 //**************************************************************************


  reg [DataAdWid -1:0] gp_daddr, gp_daddr_clkd;
  reg [2:0]   curr_state;
  reg [2:0]   nxt_state;	
  reg         CRS;	   

  reg [31:0]  txd_int;	      // 32 bit register to store TXD data and send it to the BFM
  reg         flag_crs_err_extend, flag_crs_extend; 

  reg [15:0]  tx_ipg;	      // Counter to count and detect the total number of IPG
			      // between the frames. 	     
  reg [31:0]  got_fcs; 	      // Store the  value of FCS 
 
  reg         BFM_xrun_int;   // Handshake signal
  reg         BFM_xrun_i;     // Handshake signal


  reg  [2:0]  nibble_counter; // count nibbles 
  reg  [2:0]  pream_counter;  // Count 8 nibbles in the preamble feild + SFD

  reg  [7:0]  txd_clkd;	      // One clock cycle delay is in Preamble checking
			      // is used to match the state_machine timing. 
  reg         tx_en_clkd;     // Used in Start_of_packet detection

  reg  [12:0] calc_pkt_length; // Counter for received packet length
  reg         COL_int;         // Collision indication signal 
  reg  [12:0] col_delay_count; // Counter for number of clock delay before asserts the collision 
			       // Same size as packet length. 
  reg  [14:0] loss_crs_count;
  reg         eop_clkd;        // One cycle delay of eop used for FCS calculation

  reg         toggle_nibble_data;


// Error registers
  reg   enet_short_ipg;		// Short IPG  between frames 
  reg   enet_short_frame;	// Short frame error
  reg   enet_bad_pream;		// Asserts when the Preamble field is not equal to 0x55555555555555 
  reg   enet_no_spd;		// Start_of Packet Delimiter error when is not equal to 0xD5
  reg   enet_short_pream ;	// Short Preamble error will be generated when Preamle is less than 7 Octets
				// less than 7 nibble of data is received in this feild 
  reg   enet_tx_err_recv;	// Transmit error received from the MAC  


  wire [15:0] rx_err_status;    // report err status to the C-side      
  wire  enet_fcs_err; 		// CRC error indication signal

  wire        eop,sop;		// Start_of packet and End_of Packet falg 
  wire        force_loss_crs;   // Used for CRS error insertion
  wire        force_col;	// Used for Collision error insertion
  wire [14:0] loss_crs_delay;   // Used for error insertion
  wire [14:0] col_byte_num;     // Insert collision error after col_byte_num of clks

  wire        eightN_cnt_done;  // Set every time the nibble counter is counted to 8.
  wire        gigmode;		// Set to 1 for Giga mode
  wire [31:0] rx_config, err_insertion;	// Configuration and error insertion 

  wire        sel_clk_10, sel_clk_100, half_duplex, fcs_enable;
  wire        rmii_mode, rgmii_mode, tbi_en, rtbi_en;	// Communication modes
   wire       reset_i;
  wire        reset_l;

//***************************************************************************
//                      Continuous Assignments
//*************************************************************************** 

   assign     reset_i   = ~sys_reset_l | XIF_reset; // Internal active low reset 
   assign     reset_l   = ~reset_i;

  assign COL       = COL_int;

  assign eightN_cnt_done = (nibble_counter == 3'h7)? 1:0;

  assign gigmode = !(sel_clk_100 | sel_clk_10);

  assign rx_config     = XIF_get_csdata[31:0];
  assign err_insertion = XIF_get_csdata[63:32];

  assign loss_crs_delay = err_insertion[14:0];
  assign force_loss_crs = err_insertion[15];
  assign col_byte_num   = err_insertion[30:16];
  assign force_col      = err_insertion[31];
      
  assign sel_clk_10  = rx_config[0];
  assign sel_clk_100 = rx_config[1];
  assign half_duplex = rx_config[2];    
  assign fcs_enable  = rx_config[3];
  assign rmii_mode   = rx_config[4];
  assign rgmii_mode  = rx_config[5];
  assign tbi_en      = rx_config[6];
  assign rtbi_en     = rx_config[7] ; 

   // XIF interface related
  assign BFM_put_operation= BFM_NXTREQ;
  assign BFM_put_status   = rx_err_status;
  assign BFM_put_size     = (calc_pkt_length >8) ? (fcs_enable?(calc_pkt_length/2-4):calc_pkt_length/2):0;
  assign BFM_put_address  = 0;
  assign BFM_put_csdata   = rx_err_status;
  assign BFM_put_data     = txd_int;
  assign BFM_put_cphwtosw = 0; 
  assign BFM_put_cpswtohw = 0; 
  assign BFM_interrupt    = 0;

  assign BFM_reset    = ~sys_reset_l;
  assign BFM_clock    = sys_clk; 
  assign BFM_gp_daddr = gp_daddr;
  assign BFM_put_dwe  = (((TX_EN) && (curr_state== GET_DATA) && eightN_cnt_done) | eop);

  // end_of_pkt : single cycle pulse
  assign eop = (!TX_EN && curr_state== GET_DATA) ? 1:0;

  // start_of_pkt : single cycle pulse
  assign sop = (TX_EN & !tx_en_clkd) ? 1:0; 

  assign BFM_xrun = BFM_xrun_i;

  assign enet_fcs_err = ((TX_ER && TX_EN) ||(eop_clkd && (got_fcs != 32'he320bbde) && (fcs_enable == 1)) ?1:0);

  assign rx_err_status = {9'b0, enet_no_spd, enet_short_ipg, enet_short_pream, 
			enet_fcs_err, enet_bad_pream, enet_short_frame, enet_tx_err_recv}; 

 //***************************************************************************
 //                      Always Loops
 //***************************************************************************

 //----------------------------------------------------------------------
 //-- internal 8_Nibble counter --
 // 
 //----------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l) 
     begin
        if(!reset_l) 
          nibble_counter  <= 3'h0;
        else 
	   begin
              if((curr_state== GET_PREAM_H) && (nxt_state== GET_DATA)) 
           	 nibble_counter  <= 3'h0 ;
              else if((curr_state== GET_PREAM_H) || (curr_state== GET_PREAM_L) || 
                     (curr_state== GET_DATA)) 
		  begin
            	     if(nibble_counter != 3'h7) 
               	       	 nibble_counter <= nibble_counter + 1;
                     else 
               		nibble_counter  <= 3'h0 ;
         	   end 
 	     else
            	nibble_counter  <= 3'h0 ;
          end   // ((curr_state==...)
    end      // always

 //----------------------------------------------------------------------
 // -- gp_daddr and call to calculate the FCS --
 //  gp_addr is incremented every time 8 nibble of data is received to make
 //  a 32 bit word. As received data is 4 bites then it takes 8 cycles to receive
 //  a whole 32 bit word. Therefor the gp_daddr will be incremented every
 //  8 clock cycle when state machine is in GET_DATA state.
 // 
 //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) 
	begin 
          gp_daddr <= 11'h0;
          got_fcs  <= 32'hffff_ffff;
          gp_daddr_clkd <= 11'h0;
     	end // (!reset_l)
    else 
	begin 
          gp_daddr_clkd <= gp_daddr;
          if((curr_state ==  GET_DATA) || eop_clkd)  
	     begin
                if (nibble_counter[0] !=0) 
                    got_fcs <= calculate_fcs(got_fcs, txd_clkd);
                if (eightN_cnt_done) 
                    gp_daddr <= gp_daddr + 1;
              end 
          else 
	      begin 
           	if(curr_state == IDLE) got_fcs <= 32'hffff_ffff;
           	  gp_daddr <= 11'h0;
              end 
         end // (else)
  end // always@(...

 //----------------------------------------------------------------------
 //-- get TXD --
 //-------------
 // * Start to get TXD data when Preamble feild and SFD is received.
 //   The state machine should be either in GET_DATA state or GET_PREAM_L
 //   state and all 8 bytes of Preamble + SFD is captured. A whole 32 bit
 //   word will be captured and send to the txd_int register in 8 cycles.
 // 
 //----------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l) 
	begin
      	    if(!reset_l) 
                txd_int  <= 1'b0;
      	    else 
         	if((curr_state== GET_DATA) || ((curr_state== GET_PREAM_L) && eightN_cnt_done))
	   	   begin 
             		case(nibble_counter) 
               		  	3'h6 : txd_int[31:28] <= TXD; 
               			3'h5 : txd_int[27:24] <= TXD; 
               			3'h4 : txd_int[23:20] <= TXD;
               			3'h3 : txd_int[19:16] <= TXD; 
               			3'h2 : txd_int[15:12] <= TXD; 
               			3'h1 : txd_int[11:8]  <= TXD; 
               			3'h0 : txd_int[7:4]   <= TXD;
               			3'h7 : txd_int[3:0]   <= TXD; 
             		endcase
          	   end
   end // always@(....


 //----------------------------------------------------------------------
 // Main state machine to receive TX_signals from G/MAC
 // State machine parameters
 // IDLE         = 3'h0; 
 // GET_PREAM_H  = 3'h1; // Gets the high 8 four_nibble of preamble data
 // GET_PREAM_L  = 3'h2; // Gets the low 3 four_nibbel of preamble + SFD
 // GET_DATA     = 3'h3; // Gets the payload
 // CRS_EXTEND   = 3'h4; // 
 //
 // * extend xrun to gen err_status
 //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) 
	begin 
          curr_state <= IDLE; 
	  BFM_xrun_i   <= 1'b0;
          flag_crs_err_extend = 0;
  	  flag_crs_extend = 0;
       	end 
      else 
	begin 
          curr_state <= nxt_state; 
          if((curr_state == GET_DATA) && (nxt_state == IDLE)) 
             BFM_xrun_i <= 1'b0;
          else
             BFM_xrun_i <= BFM_xrun_int;
     	end
    end // always@(.... 


  always@(curr_state or TX_EN or TXD or COL or TX_ER or XIF_get_operation or 
          eightN_cnt_done) 
  begin
    nxt_state = IDLE;
    case(curr_state)
      IDLE: begin
             BFM_xrun_int = 1'b0;
	      flag_crs_err_extend = 0;
  	      flag_crs_extend = 0;
             case (XIF_get_operation) // decode the transaction opcode
               BFM_READ: begin
                      BFM_xrun_int = 1'b1;
                      if(TX_EN && !TX_ER) 
                         nxt_state = GET_PREAM_H;
                      else 
                         nxt_state = IDLE;
                end

	       BFM_NOP: begin
                       nxt_state = IDLE;
                       BFM_xrun_int = 1'b0; end

               default : begin 
	              //$display("%d operation not supported \n", XIF_get_operation);
                      nxt_state = IDLE;
                      BFM_xrun_int = 1'b0; end
             endcase
       end // idle_state

       GET_PREAM_H: begin
             BFM_xrun_int = 1'b1;
             if(eightN_cnt_done && !TX_ER) 
                nxt_state = GET_PREAM_L;
             else if(TX_ER) 
		begin 
                  nxt_state = IDLE;      // should never get it 
                  BFM_xrun_int = 1'b0;
                end 
	     else 
                nxt_state = GET_PREAM_H;  
       	end // GET_PREAM_H

       GET_PREAM_L: begin
             BFM_xrun_int = 1'b1;
             if((eightN_cnt_done) && !TX_ER) 
                nxt_state = GET_DATA;
             else if(TX_ER) 
		begin 
                  nxt_state = IDLE;      // should never get it 
                  BFM_xrun_int = 1'b0;
                end 
	     else 
                nxt_state =  GET_PREAM_L;  
       end // GET_PREAM_L

`ifdef OLD
       GET_DATA: begin // debug work on this more
             BFM_xrun_int = 1'b1;
             if(TX_ER && (TXD == (8'h0f||8'h1f)))    // crs extend
                nxt_state = CRS_EXTEND;  
             else if(!TX_EN) 
		begin
                  nxt_state = IDLE;                 // normal pkt termination
                  BFM_xrun_int = 1'b0;
                end 
	     else
                nxt_state = GET_DATA;               // receiving pkt
       end // GET_DATA

       CRS_EXTEND: begin
             BFM_xrun_int = 1'b1;
              if(!TX_EN && (TXD == 8'h0f) && TX_ER) 
		begin 
                  nxt_state = CRS_EXTEND;             
                  flag_crs_extend = 1;     	    // debug
                end 
	     else if(!TX_EN && (TXD == 8'h1f) && TX_ER) 
		begin 
                  nxt_state = CRS_EXTEND;             
                  flag_crs_err_extend = 1; 	    // debug
                end 
	     else if(!TX_EN && (TXD != (8'h1f||8'h0f)) && TX_ER) 
		begin
                  nxt_state = IDLE;                 // debug should never come here    
                  BFM_xrun_int = 1'b0;
                end 
	     else if(!TX_ER) 
		begin
                  nxt_state = IDLE;             
                  BFM_xrun_int = 1'b0;
             	end
        end // CRS_EXTEND
`endif

       GET_DATA: begin // debug work on this more
             BFM_xrun_int = 1'b1;
//             if(TX_ER && (TXD == (4'h0||4'h1)))    // crs extend
             if(TX_ER && TX_EN)    // crs extend
                nxt_state = CRS_EXTEND;
                
             else if(!TX_EN) 
		begin
                  nxt_state = IDLE;                 // normal pkt termination
                  BFM_xrun_int = 1'b0;   
                end 
	     else
                nxt_state = GET_DATA;               // receiving pkt
       end // GET_DATA

       CRS_EXTEND: begin
             BFM_xrun_int = 1'b1;
              if(TX_EN) 
		begin 
                  nxt_state = CRS_EXTEND;             
                  flag_crs_extend = 1;     	    // debug
                end 
	     else 
		begin
                  nxt_state = IDLE;             
                  BFM_xrun_int = 1'b0;
             	end
        end // CRS_EXTEND



       default : BFM_xrun_int = 1'b0;
    endcase
  end //always@(....


 //----------------------------------------------------------------------
 // --- Count TX_IPG and  calc_pkt_length
 // *IPG must be 64 before the very first pkt gap. For a real DUT_MAC
 //  this time will be automatically happen since the test must
 //  wait for the DUT to be configured via the slower MDIO interface.
 //  In case the ipg counter overflows, it might indicate false ipg_err
 //  ipg is not part of pkt_length. 
 //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l) 
   begin
     if (!reset_l) 
	begin 
          tx_ipg <= 16'h0;          // preload ipg
	  calc_pkt_length <= 13'h0;
     	end  // (!reset_l)
     else 
	begin 
           if (~TX_EN) 
             tx_ipg <= tx_ipg + (gigmode ? 8 : 4); //Count ipg
           else 
             tx_ipg <= 0;

           if(curr_state== GET_DATA)
             calc_pkt_length <= calc_pkt_length + 1;
           else
             calc_pkt_length <= 13'h0;
             
         end // (else)
  end //always@(....



 //----------------------------------------------------------------------
 // -- Err_status --
 // ----------------
 //                       bit val
 // ENET_GOOD_PKT           0
 // ENET_SHORT_IPG         [0]  1
 // ENET_SHORT_FRAME	   [1]  2
 // ENET_BAD_PREAM         [2]  4
 // ENET_SHORT_PREAM       [3]  8
 // ENET_FCS_ERR           [4]  16
 // ENET_TX_ERR_RECV       [5]  32
 // ENET_NO_SPD            [6]  64
 // 
 // rx_err_status[15:0] == 0 --> good_pkt_rcvd
 //
 //
 // * Check pkt_lenght against received pkt_length.  Note that the calc_pkt_length
 //   shows the number of nibbles, so the real packet length is calc_pkt_length /2.
 // * Short ipg will be reported if the sop is asserted when the frame gap is less 
 //   than 96-Bit time.
 // * Short frame error is generated in case calc_pkt_length < 64 Bytes when sop is asserted.
 //   In this case the enet_short_frame signal will be asserted.
 // * In pream_check txd_clkd is used to match the state_machine timing.
 //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l) 
    begin
     if (!reset_l) 
	begin 
          pream_counter      	<= 3'h7;
          txd_clkd           	<= 8'h0;
          tx_en_clkd         	<= 1'h0;
          enet_short_ipg       	<= 1'h0;
          enet_short_frame   	<= 1'h0;
          enet_bad_pream      	<= 1'h0;
          enet_no_spd        	<= 1'h0;
          enet_short_pream    	<= 1'h0;
          enet_tx_err_recv   	<= 1'h0;       
          eop_clkd           	<= 1'h0;
          toggle_nibble_data 	<= 1'h0;

     	end 
     else 
	begin
          tx_en_clkd <= TX_EN;
          eop_clkd <= eop;

          if(XIF_xav) 
	     begin 
               enet_short_ipg  	<= 1'h0;
               enet_short_frame	<= 1'h0;
               enet_bad_pream  	<= 1'h0;
               enet_no_spd     	<= 1'h0;
               enet_short_pream	<= 1'h0;
               enet_tx_err_recv	<= 1'h0;       
             end 
	   else 
	     begin

       		//----------------------------- 
                if(sop && tx_ipg<96)
              	enet_short_ipg <= 1'b1; 

	        //----------------------------
                // Less than 127 because calc_pkt_length is counting one cycle behind eop
        	   if((eop && (calc_pkt_length<127))  || (curr_state ==  GET_DATA && TX_EN && TX_ER )) 
             	 enet_short_frame <= 1'b1; 

	        //----------------------------- 
        	   if(sop) 
             	     toggle_nibble_data <= 1'h0;
          	   else
              	     toggle_nibble_data <= ~toggle_nibble_data;

       		//------------------------------
		// make a 8 bit word data of the nibble. 
           	    if(sop || toggle_nibble_data) 
            	       txd_clkd[3:0] <= TXD;
          	    else 
          	       txd_clkd[7:4] <= TXD;
           
		//-------------------------------
		// Generates bad preamble error by checking the received preamble
		// pattern with 0x55 55 55 55 55 55 55. 
 	          if(((curr_state == GET_PREAM_H && (txd_clkd != 8'h55)) || 
                      (curr_state == GET_PREAM_L && (txd_clkd != 8'h55) && (nibble_counter!=7)))
                       && (nibble_counter[0]!=0))
                       enet_bad_pream <= 1'b1;

		//---------------------------------
		// Generate the Start-of_Packet Delimiter error when the 0xd5 pattern
		// does not follow the preamble immediately when the nibble_count shows that
		// 7 bytes of preamble is received. 
 	          if(curr_state== GET_PREAM_L && (txd_clkd != 8'hd5) && (nibble_counter==3'h7))
     	             enet_no_spd <= 1'b1;

	 	//--------------------------------
  	        // if(nibble_counter[0]!=0 && (TX_EN && txd_clkd==8'h55) && (curr_state!= GET_DATA))
		   if(nibble_counter[0]!=0 && (TX_EN && txd_clkd==8'h55) && ((curr_state==GET_PREAM_H)  || (curr_state==GET_PREAM_L)))
	              pream_counter <= pream_counter - 1;
	          else if(curr_state== IDLE)
 	             pream_counter <= 3'h7;

 	          if(eop && (pream_counter!=0))
	              enet_short_pream <= 1'b1;
  		//------------------------------------
	     	 if((curr_state== GET_DATA) && TX_ER)
	              enet_tx_err_recv <= 1'b1;       

	      end //(!xav)
     end // reset_l
  end // alwasy@(.....



 // ----------------------------------------------------------------------
 // -- COL gen --
 // -------------
 // Generates the collision output signal while in GET_DATA state
 // after col_byte_num of clocks
 // ----------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l) 
	begin
      	  if(!reset_l) 
		begin
         	  COL_int <= 1'b0;
	 	  col_delay_count <= 13'h0;
      		end
 	  else 
		begin
                   if(XIF_xav)
                       	col_delay_count <= col_byte_num;

         	   else if(((curr_state== GET_DATA) && (col_delay_count==0)) && force_col) 
            	       	COL_int <= 1'b1;
                   else if((curr_state== GET_DATA) && force_col) 
	 		begin 
            	  	    col_delay_count <= col_delay_count - 1;  
            		    COL_int <= 1'b0;
             		end 
         	   else 
			begin
                   	    col_delay_count <= 0;
            	   	    COL_int <= 1'b0;
                	end 
           	end
   end // always
   
  

 // ----------------------------------------------------------------------
 // -- CRS gen --
 // -------------
 //  CRS signal should be asserted and shall remain asserted while state
 //  machine in the  nonIdle mode.
 //  for error condition, once CRS is pulled low, it shall remain deasserted 
 //  till start of the next packet.
 // ----------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l) 
	begin
           if(!reset_l) 
	     	begin
              	  CRS  <= 1'b0;
           	  loss_crs_count <= 15'b0;
             	end
	    else 
		begin
         	  if(curr_state == GET_PREAM_H) 
            	      CRS  <= 1'b1; 
	          else if((curr_state == IDLE) ||
                     (half_duplex && force_loss_crs && (loss_crs_count==0)))
                       CRS  <= 1'b0;

         	   if(XIF_xav)
            		loss_crs_count <= loss_crs_delay ;
         	   else if(loss_crs_count!=0) 
            		loss_crs_count <= loss_crs_count - 1 ;
                end
   end // always
   
 //----------------------------------------------------------------------
 // -- Packet FCS Calculation
 //----------------------------------------------------------------------
   function [31:0] calculate_fcs;
      input [31:0] fcs;
      input [7:0]  byte;
      
      integer 	   index;
      reg 	   feedback;
      
      begin
	 calculate_fcs = fcs;
	 
	 for (index=0; index<=7; index=index+1)
	    begin
	       feedback = calculate_fcs[24] ^ byte[index];
	       
	       calculate_fcs[24] = calculate_fcs[25];
	       calculate_fcs[25] = calculate_fcs[26];
	       calculate_fcs[26] = calculate_fcs[27];
	       calculate_fcs[27] = calculate_fcs[28];
	       calculate_fcs[28] = calculate_fcs[29];
	       calculate_fcs[29] = calculate_fcs[30]  ^ feedback;
	       calculate_fcs[30] = calculate_fcs[31];
	       calculate_fcs[31] = calculate_fcs[16];
	       
	       calculate_fcs[16] = calculate_fcs[17]  ^ feedback;
	       calculate_fcs[17] = calculate_fcs[18]  ^ feedback;
	       calculate_fcs[18] = calculate_fcs[19];
	       calculate_fcs[19] = calculate_fcs[20];
	       calculate_fcs[20] = calculate_fcs[21];
	       calculate_fcs[21] = calculate_fcs[22];
	       calculate_fcs[22] = calculate_fcs[23];
	       calculate_fcs[23] = calculate_fcs[8]   ^ feedback;
	       
	       calculate_fcs[8]  = calculate_fcs[9];
	       calculate_fcs[9]  = calculate_fcs[10];
	       calculate_fcs[10] = calculate_fcs[11];
	       calculate_fcs[11] = calculate_fcs[12]  ^ feedback;
	       calculate_fcs[12] = calculate_fcs[13]  ^ feedback;
	       calculate_fcs[13] = calculate_fcs[14]  ^ feedback;
	       calculate_fcs[14] = calculate_fcs[15];
	       calculate_fcs[15] = calculate_fcs[0]   ^ feedback;
	       
	       calculate_fcs[0]  = calculate_fcs[1]   ^ feedback;
	       calculate_fcs[1]  = calculate_fcs[2];
	       calculate_fcs[2]  = calculate_fcs[3]   ^ feedback;
	       calculate_fcs[3]  = calculate_fcs[4]   ^ feedback;
	       calculate_fcs[4]  = calculate_fcs[5];
	       calculate_fcs[5]  = calculate_fcs[6]   ^ feedback;
	       calculate_fcs[6]  = calculate_fcs[7]   ^ feedback;
	       calculate_fcs[7]  =                      feedback;
	    end
      end
   endfunction

`endprotect
endmodule

// -- eof --
