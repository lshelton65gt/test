//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Description:  Synthesizable Ethernet Transmitter BFM and attached to the 
//                MII Rx interface of the DUT. This tx transactor module 
//                is a C-side initiated transactor and consists of XIF_core and 
//		  BFM. The XIF_core communicates to the C-side. It receives
//		  configuration data first to configure the BFM for error injection,
//		  transmission mode and set up tx_config. Then it receives the
//		  preamble and SF data. After receiving the payload this module
//		  calculates the FCS and then send the cycle-by-cycle pin data
//		  to the BFM. 
// ----------------------------------------------------------------------------


`timescale 1ns / 100 ps

module  carbonx_enetmii_tx( sys_clk,   
                        sys_reset_l,
			RXD,       
			RX_DV,     
			RX_ER,     
			RX_CLK    
			);

   
   // Primary IO
   input        sys_clk;		// System clock
   input	sys_reset_l;	        // System reset
   output       RX_CLK;   		// Receive clock - 2.5/25 MHz
   output       RX_DV;    		// Receive data valid
   output       RX_ER;    		// Receive error
   output [3:0] RXD;      		// Receive data
   

`protect
// carbon license crbn_vsp_exec_xtor_e10_100

 //**************************************************************************
 //                      Parameter Defines
 //**************************************************************************
  parameter 	BFMid     = 1;
  parameter 	ClockId   = 1;       		// Which cclock I am connected to	     
  parameter	DataWidth = 32;
  parameter	DataAdWid = 11;	     		// Max is 15
  parameter	GCSwid    = 32*4;   		// Get CS Data width Max is 512 bytes
  parameter	PCSwid    = 32*2;   		// Put CS Data width Max is 512 bytes

  parameter LONG_FRAME_DATA = 8'h55;

//                                  
//  File Name - carbonx_enetmii_tx.v 
//  Author    - Rajat Mohan
//  Date      - June 12th 2003
// 
//******************************************************************************
//* $RCSfile$
//* $Author$
//* $Date$
//* 
//* Description:  Synthesizable Ethernet Transmitter BFM and attached to the 
//                MII Rx interface of the DUT. This tx transactor module 
//                is a C-side initiated transactor and consists of XIF_core and 
//		  BFM. The XIF_core communicates to the C-side. It receives
//		  configuration data first to configure the BFM for error injection,
//		  transmission mode and set up tx_config. Then it receives the
//		  preamble and SF data. After receiving the payload this module
//		  calculates the FCS and then send the cycle-by-cycle pin data
//		  to the BFM. 
//*
//*
//******************************************************************************

// State machine parameters
  parameter 	IDLE         	 = 3'h0;
  parameter 	SEND_PREAM_H 	 = 3'h1;
  parameter 	SEND_PREAM_L 	 = 3'h2;
  parameter	INJECT_BAD_ALIGN = 3'h3;
  parameter 	SEND_DATA    	 = 3'h4;
  parameter 	SEND_FCS     	 = 3'h5;
  parameter 	SEND_IPG     	 = 3'h6;

  parameter 	PREAM_H      = 32'h5555_5555; 	// The higher 4 octet of Preamble 
  parameter 	PREAM_L_SFD  = 32'h5555_555d;   // The remainder of preamble + SFD

 //***************************************************************************
 //                      Include C Interface File
 //***************************************************************************  

`include "xif_core.vh"   	// module instantiation of XIF_core

//  Register definition
// ----------------------------------------------------------------------
//  get_csdata[31:0]    -> tx_config
//  get_csdata[63:32]   -> err_control
//  get_csdata[95:64]   -> preamble_sfd_l
//  get_csdata[127:96]  -> preamble_sfd_h
//
//  err_control[0]     -> Inject_Rcv_Err    	// Inject error and generates the RX_ER
//  err_control[1]     -> Inject_Short_Event    // Deassert the BFM_xrun_int signal
//  err_control[2]     -> Inject_Align_Err 	// in 10/100 mode only  
//  err_control[3]     -> Inject_Bad_FCS        // Inject error in FCS
//  err_control[4]     -> Inject_Frame_long 	// Reserved for future
//  err_control[5]     -> Reserved
//  err_control[11:6]  -> err_drv_delay   	// 64 byte counter
//  err_control[17:15] -> force_cg_error  	// Reserved for TBI
//  err_control[21:18] -> bad_cg_count    	// Reserved for TBI
//  err_control[31:22] -> reserved
//
//  tx_config[0]     -> sel_clk_10     
//  tx_config[1]     -> sel_clk_100 --> (_config[1:0]==00) -> gigmode    
//  tx_config[2]     -> half_duplex    
//  tx_config[3]     -> fcs_enable     
//  tx_config[4]     -> rmii_mode  		// Reserved for future
//  tx_config[5]     -> rgmii_mode	        // Reserved for future	  
//  tx_config[6]     -> tbi_en  		// Reserved for future
//  tx_config[7]     -> rtbi_en  		// Reserved for future
//  tx_config[31:8] -> reserved			// Reserved for future
// 

 //**************************************************************************
 //                      Internal Reg/Wire Signals 
 //**************************************************************************

//Wires
  wire [31:0] 	err_control;
  wire [31:0] 	tx_config;
  wire [31:0] 	preamble_sfd_l;
  wire [31:0]  	preamble_sfd_h; 
  wire 	      	Inject_Rcv_Err;
  wire 		Inject_Short_Event;
  wire		Inject_Align_Err;
  wire		Inject_Bad_FCS;    
  wire		Inject_Frame_long; 
  wire 		reset_l;
  wire		reset_i;
  wire       	eightN_cnt_done;  
  // count to delay the error insertion from SOP   
  // -> increase the size for delay > 64 Bytes
  wire [5:0]  	err_drv_delay; 
  wire       	mii_en;       
  wire       	sel_clk_10;
  wire		sel_clk_100;
  wire       	half_duplex;
  wire		fcs_enable; 
  wire [1:0] 	valid_bytes;
  wire       	last_addr;
  wire       	last_odd_bytes;
  wire 		no_fcs_3;
  wire 		no_fcs_2;	



  // -- register definition
   reg [2:0]  curr_state;   
   reg [2:0]  nxt_state;
  
   reg [2:0]  nibble_counter;   	// Nibble counter for MII interface to keep
					// track of getting 8 bytes of Preamble + SFD data.
					// The 2 LSB of this counter is used for calculation of CRC 
					// and sending 8 nibbles of FCS data to the BFM.

 
   reg [3:0]  	rxd_int;		
   reg [3:0]   	rxd_clkd;
   reg        	rx_dv_int;
   reg 		rx_dv_clkd;
   reg        	rx_er_int;
   reg		rx_er_clkd;
   reg [31:0] 	got_fcs;  	
   reg        	BFM_xrun_int;     	// Handshake
   reg        	send_short_frame;
   reg        	error_done;
   reg        	tx_crs;           	// debug use-model
   reg [5:0]  	err_delay_count;
   reg        	sending_frame;
   reg        	sending_frame_clkd; 
   reg        	sop_eop;
   reg        	BFM_xrun_i;
   reg        	stay_in_fcs_state;
   reg [7:0]  	ipg_count;
   reg [DataAdWid -1:0] gp_daddr;
 

  //***************************************************************************
  //                      Continuous Assignments
  //*************************************************************************** 

   // assign  Data output signals
   assign RXD   = rxd_clkd;
   assign RX_DV = rx_dv_clkd;
   assign RX_ER = rx_er_clkd;

   // ------------------------------
   // XIF_core interface 
   // Input data to the XIF_core
   // ------------------------------
   assign BFM_clock = sys_clk; 
   assign BFM_put_operation = BFM_NXTREQ;
   assign BFM_interrupt      = 0;
   assign BFM_put_status     = 0;
   assign BFM_put_size[23:0] = 0; 	
   assign BFM_put_address    = 0;
   assign BFM_put_data       = 0;
   assign BFM_put_cphwtosw   = 0; 
   assign BFM_put_cpswtohw   = 0; 
   assign BFM_reset          = ~sys_reset_l;
   assign BFM_put_dwe        = 0;


   //----------------------------------------------------------------------
   // -- CS_Write --\
   // Get the configuration and frame data from C-side via the XIF core..
   // 
   //----------------------------------------------------------------------
   assign tx_config   = XIF_get_csdata[31:0];
   assign err_control = XIF_get_csdata[63:32];

   assign preamble_sfd_l = ((XIF_get_csdata[95:64] !=0)  ? 
                            XIF_get_csdata[95:64] : PREAM_L_SFD);

   assign preamble_sfd_h = ((XIF_get_csdata[127:96] !=0) ? 
                            XIF_get_csdata[127:96] : PREAM_H); 

   //----------------------------------------------------------------------
   // -- CS_Read --
   // Allow C-side to read configuration data from the transactor.
   // 
   //----------------------------------------------------------------------

   assign BFM_put_csdata[63:32]  = err_control;
   assign BFM_put_csdata[31:0]   = tx_config;

   assign Inject_Rcv_Err     = err_control[0];
   assign Inject_Short_Event = err_control[1];
   assign Inject_Align_Err   = err_control[2];
   assign Inject_Bad_FCS     = err_control[3];
   assign Inject_Frame_long  = err_control[4];
   assign err_drv_delay      = err_control[11:6];

   assign reset_i     = ~sys_reset_l | XIF_reset;
   assign reset_l       = ~reset_i;

   // We need these signals to configure the transactor interface
 
   assign RX_CLK       = sys_clk;
   assign sel_clk_10   = tx_config[0];
   assign sel_clk_100  = tx_config[1];
   assign half_duplex  = tx_config[2];
   assign fcs_enable   = tx_config[3];
  
   // Control signals to calculate  32 bits FCS 
   assign no_fcs_2 = ((gp_daddr==(XIF_get_size>>2)) && (nibble_counter==valid_bytes) && 
                      (valid_bytes == 2)) ? 1:0; 
         
   assign no_fcs_3 = ((gp_daddr==(XIF_get_size>>2)) && (nibble_counter==3'h3) && 
                      (valid_bytes == 3)) ? 1:0; 

  // same as gigmode selection
   assign mii_en = (sel_clk_10 | sel_clk_100) ? 1: 0; 

   assign BFM_gp_daddr = gp_daddr;  					// only for get data

   assign eightN_cnt_done = (nibble_counter == 3'h7)? 1:0; 
   assign valid_bytes = (XIF_get_size[1:0]); 				//get remainder for non_word_data 
   assign last_addr = (gp_daddr == ((XIF_get_size>>2)-1)) ? 1 :0; 
   assign last_odd_bytes = (last_addr & (valid_bytes!=0)) ? 1:0; 
   assign BFM_xrun = BFM_xrun_i; // Handshake with xif_core.vh	




 //***************************************************************************
 //                      Always Loops
 //***************************************************************************
  //----------------------------------------------------------------------
  // -- gen tx_crs --
  // ---------------
  // tx_crs is driven by tx-xactor. The rx_xactor takes this as input and then
  // depending on full_duplex mode generates the CRS for the G/MAC
  // tx_crs must be active during error indication 
  //----------------------------------------------------------------------

  // debug -- might be the same as BFM_xrun_int
   always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) begin 
        tx_crs <= 1'b0;
     end else begin
        if((curr_state == SEND_DATA) || (curr_state == SEND_FCS) || 
           (curr_state == SEND_PREAM_H) || (curr_state == SEND_PREAM_L)) 
           tx_crs <= 1'b1;
        else
           tx_crs <= 1'b0;
     end
   end //always@(....

  //---------------------------------------------------------------------
  // -- drive RXD --
  // ---------------
  //   Generate signals for sending long frame. It breaks the packet down 
  //   to the 4 bits words and then send the cycle_by_cycle pin data to the
  //   BFM.
  //   SEND_FCS state drives rxd_clkd (not rxd_int) to take care of 1 cycle 
  //   fcs generation delay.The calculated FCS is XORed to inject an FCS 
  //   error whenever the c test set this bit in the start of the packet
  //   transmission. 
  //----------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) begin 
        rxd_int  <= 4'b0;
        rxd_clkd <= 4'b0;
     end else begin 
        if(!sop_eop && curr_state==SEND_FCS) begin
	   case(nibble_counter)  
	      3'h1 : rxd_clkd <= ~got_fcs[31:28]^Inject_Bad_FCS;
	      3'h0 : rxd_clkd <= ~got_fcs[27:24]^Inject_Bad_FCS;
	      3'h3 : rxd_clkd <= ~got_fcs[23:20]^Inject_Bad_FCS; 
	      3'h2 : rxd_clkd <= ~got_fcs[19:16]^Inject_Bad_FCS; 
	      3'h5 : rxd_clkd <= ~got_fcs[15:12]^Inject_Bad_FCS;
	      3'h4 : rxd_clkd <= ~got_fcs[11:8] ^Inject_Bad_FCS; 
	      3'h7 : rxd_clkd <= ~got_fcs[7:4]  ^Inject_Bad_FCS; 
	      3'h6 : rxd_clkd <= ~got_fcs[3:0]  ^Inject_Bad_FCS;
           endcase 
        end else // state != send_fcs
           rxd_clkd <= rxd_int;

        if(curr_state == IDLE) begin
           rxd_int   <= 8'b0;
        end else if(curr_state == SEND_PREAM_H) begin 
	   case(nibble_counter) 
	      3'h0 : rxd_int <= preamble_sfd_h[31:28]; //MSB first
	      3'h1 : rxd_int <= preamble_sfd_h[27:24]; 
	      3'h2 : rxd_int <= preamble_sfd_h[23:20]; 
	      3'h3 : rxd_int <= preamble_sfd_h[19:16];
	      3'h4 : rxd_int <= preamble_sfd_h[15:12];
	      3'h5 : rxd_int <= preamble_sfd_h[11:8]; 
	      3'h6 : rxd_int <= preamble_sfd_h[7:4]; 
	      3'h7 : rxd_int <= preamble_sfd_h[3:0];
	   endcase
        end else if(curr_state == SEND_PREAM_L) begin 
	   case(nibble_counter) 
	      3'h0 : rxd_int <= preamble_sfd_l[31:28]; //MSB first
	      3'h1 : rxd_int <= preamble_sfd_l[27:24]; 
	      3'h2 : rxd_int <= preamble_sfd_l[23:20]; 
	      3'h3 : rxd_int <= preamble_sfd_l[19:16];
	      3'h4 : rxd_int <= preamble_sfd_l[15:12];
	      3'h5 : rxd_int <= preamble_sfd_l[11:8]; 
	      3'h6 : rxd_int <= preamble_sfd_l[7:4]; 
	      3'h7 : rxd_int <= preamble_sfd_l[3:0];
           endcase
        end else if(curr_state == SEND_DATA) begin
	   case(nibble_counter) // on mii lower nibble is sent first 
              3'h7 : rxd_int <= XIF_get_data[31:28];
              3'h6 : rxd_int <= XIF_get_data[27:24]; 
              3'h5 : rxd_int <= XIF_get_data[23:20]; 
              3'h4 : rxd_int <= XIF_get_data[19:16];
              3'h3 : rxd_int <= XIF_get_data[15:12]; 
              3'h2 : rxd_int <= XIF_get_data[11:8]; 
              3'h1 : rxd_int <= XIF_get_data[7:4]; 
              3'h0 : rxd_int <= XIF_get_data[3:0];
           endcase 
	end else if (curr_state == INJECT_BAD_ALIGN) begin
	 rxd_int <= 4'hf;
        end 
     end
   end // always @(...


  //----------------------------------------------------------------------
  // 			 Nibble counter
  //--------------------------------------------------------------------
  // This counter is used to control the capture of 8 chunk of 4 bites data
  // in the preamble+ SFD, FCS, and  Data. 


   always@(posedge sys_clk or negedge reset_l) begin
      if(!reset_l) 
	begin
           nibble_counter <= 3'h0;
           sop_eop <= 0;
        end 
      else 
	begin
         sop_eop <= (sending_frame_clkd ^ sending_frame);

         if(sop_eop && (curr_state==SEND_FCS)) 
	   begin
             nibble_counter  <= 3'h0 ;
           end 
	 else if((curr_state==SEND_PREAM_H)||(curr_state==SEND_PREAM_L)||
            (curr_state==SEND_FCS) || (curr_state==SEND_DATA)) 
	   begin
             if(nibble_counter != 3'h7) 
               nibble_counter <= nibble_counter + 1;
             else 
               nibble_counter  <= 3'h0;
	   end 
	 else 
            nibble_counter  <= 3'h0 ;
      end   // (reset_l==1)
   end      // always

  //----------------------------------------------------------------------
  // -- Byte alignment --
  // *Get the valid bytes from last cycle of 32 bit data
  // *This block also controls the SEND_DATA state 
  // *gp_daddr is address for 32 bit wide data buffer in xif_core
  // *c-side indicates No. of data bytes via XIF_get_size 
  // The sending_frame_clkd with one clock cycle delay is used 
  // for selection of end of packet and start of the packet.
  //----------------------------------------------------------------------
 
   always@(posedge sys_clk or negedge reset_l)
   begin
      if (!reset_l) begin 
         sending_frame      <= 1'b0;
         sending_frame_clkd <= 1'b0; 
      end else begin
         sending_frame_clkd <= sending_frame;

         if((curr_state == SEND_PREAM_L) && (nxt_state == SEND_DATA))
            sending_frame <= 1'b1;
         else if ((curr_state == SEND_PREAM_L) && (nxt_state == INJECT_BAD_ALIGN))
            sending_frame <= 1'b1;
         else if((last_addr) && (!last_odd_bytes) && (nibble_counter==3'h6))
            sending_frame <= 1'b0;
         else if((gp_daddr==(XIF_get_size>>2)) && (nibble_counter==valid_bytes) &&
                 (valid_bytes == 2)) 
            sending_frame <= 1'b0;
         else if((gp_daddr==(XIF_get_size>>2)) && (nibble_counter==3'h4) &&
                 (valid_bytes == 3)) 
            sending_frame <= 1'b0;
         else if((gp_daddr==(XIF_get_size>>2)) && (nibble_counter==3'h0) &&
                 (valid_bytes==1)) 
            sending_frame <= 1'b0; 
      end // !reset
   end // always@(.... 

  

  //--------------------------------------------------------------------
  // 			Call fcs calculate
  //--------------------------------------------------------------------
  // *perform fcs calculation for 4 clocks (4-bytes) only
  // 
 
   always@(posedge sys_clk or negedge reset_l) begin
      if(!reset_l) begin 
	 got_fcs  <= 32'hffff_ffff;
      end else begin 

         if((curr_state==SEND_DATA) && (nibble_counter[2]==0) && 
            (nxt_state !=SEND_FCS ) && !no_fcs_3 && !no_fcs_2)begin
	    case(nibble_counter[1:0]) 
	       2'b11 : got_fcs <= #1 calculate_fcs(got_fcs, XIF_get_data[31:24]);
	       2'b10 : got_fcs <= #1 calculate_fcs(got_fcs, XIF_get_data[23:16]); 
	       2'b01 : got_fcs <= #1 calculate_fcs(got_fcs, XIF_get_data[15:8]); 
	       2'b00 : got_fcs <= #1 calculate_fcs(got_fcs, XIF_get_data[7:0]); 
            endcase 
         end else if(curr_state == IDLE)
            got_fcs  <= 32'hffff_ffff;

      end
   end // always


  //--------------------------------------------------------------------
  // 			 gp_daddr gen
  //--------------------------------------------------------------------
  // Increments only once per 4 clks
  // daddr must be valid 1 clk before the data is expected.
  // For MII(nibble) - incr once per 8 clks 
 
   always@(posedge sys_clk or negedge reset_l) begin
      if(!reset_l) 
         gp_daddr <= 11'h0;
      else if(curr_state == SEND_DATA) begin 
         if (nibble_counter == 3'h6)
            gp_daddr <= gp_daddr + 1;
      end else
         gp_daddr <= 0;
   end // always
  
  //-------------------------------------------------------------------
  // 				 RX_ER gen 
  //-------------------------------------------------------------------
  //* rx_er is 1 clk pulse and it is generated when inject_Rcv_Err is set
  // The pulse is sent after the specified err_drv_delay.
  // The count starts during the first preamble byte. Max delay value
  // is 64. To increase it, simply increase the err_drv_delay reg size.
  // 
  //* RX_ER to be asserted only during send_data state


   always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) 
	begin 
          rx_er_int  <= 1'b0; 
          rx_er_clkd <= 1'b0; 
	  error_done <= 1'b0;
          err_delay_count <= 6'b0;
     	end 
      else 
	begin 
           rx_er_clkd <= rx_er_int;
           if(XIF_xav)			//Transaction available signal
              error_done <= 1'b0;  
            else if(sending_frame && Inject_Rcv_Err && !error_done) 
	      begin
                 if(err_delay_count < err_drv_delay) 
                     err_delay_count <= err_delay_count + 1;
                 else 
		     begin
                   	rx_er_int  <= 1'b1; 
                   	error_done <= 1'b1;  
                   	err_delay_count <= 6'b0;  
                     end
               end 
	     else 
		begin
                   rx_er_int  <= 1'b0; 
                   err_delay_count <= 6'b0;  
                end 
          end
   end  // always @(.....



  //--------------------------------------------------------------------
  // 				 RX_DV gen
  //--------------------------------------------------------------------
  // * extended 1 cycle for long frame error generation
  //--------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) begin 
        rx_dv_int  <= 1'b0;
        rx_dv_clkd <= 1'b0;
        send_short_frame <= 1'b0;
     end else begin
        rx_dv_clkd <= rx_dv_int ;

        if((curr_state == SEND_FCS) && (nibble_counter==3'h7) && !stay_in_fcs_state) 
           rx_dv_int <= 1'b0;
        else if((curr_state == SEND_DATA) || (curr_state == SEND_FCS) || 
                (curr_state == SEND_PREAM_H)||(curr_state == SEND_PREAM_L) || (curr_state == INJECT_BAD_ALIGN)) begin 
           rx_dv_int <= 1'b1;
           if((curr_state == SEND_PREAM_H) && (Inject_Short_Event))
              send_short_frame <= 1'b1;
        end else begin 
           rx_dv_int <= 1'b0;
           send_short_frame <= 1'b0;
        end
     end
   end // always @(...


  //-------------------------------------------------------------------
  // Don't look at 4B_count_done in the very first cycle of send_FCS
  //-------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) 
        stay_in_fcs_state <= 1'b0;
     else if((curr_state == SEND_DATA) && (nxt_state == SEND_FCS) &&
              (valid_bytes == 3)) 
        stay_in_fcs_state <= 1'b1;
     else 
        stay_in_fcs_state <= 1'b0;
   end // always

  //-------------------------------------------------------------------
  // -- IPG -- Transaction Control task 
  //   This process forst get the number of interpacket gap assigned from 
  //   C-side and assign it to the ipg_counter.  
  //   -2 is done to take care of processing delay
  //-------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) 
        ipg_count <= 8'b0;
     else if((!ipg_count) && (XIF_get_address != 0) && (curr_state == IDLE))
        ipg_count <= XIF_get_address[7:0] - 2;     
     else if((curr_state == SEND_IPG) && (ipg_count != 0))
        ipg_count <= ipg_count - 1;
     else 
        ipg_count <= 8'b0;
   end // always



  //---------------------------------------------------------------------------
  //                            State Machine 
  //---------------------------------------------------------------------------

  //-------------------------------------------------------------------
  // The state machine to toggle the state of the 
  // Ethernet TX-Xactor. The following states is defined to control 
  // the packet transmission. 
  // IDLE         	= 3'h0;
  // SEND_PREAM_H 	= 3'h1;
  // SEND_PREAM_L 	= 3'h2;
  // INJECT_BAD_ALIGN 	= 3'h3
  // SEND_DATA    	= 3'h4;
  // SEND_FCS     	= 3'h5;
  // SEND_IPG     	= 3'h6;
  // 
  // 1. The state m/c must keep Xrun asserted in all states except idle.
  // 2. XIF_get_operation is decoded in idle state only
  //
  // notes
  // * In send_FCS state, valid_bytes=!3 is added to fix the bug that prevent 
  // transmission of FCS when state machine goes to IDLE state.
  // * We insert a couple of cycles even when IPG != 0
  //-------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l)
   begin
     if (!reset_l) begin 
        curr_state <= IDLE; 
	BFM_xrun_i   <= 1'b0;
     end else begin 
        curr_state <= nxt_state; 
        BFM_xrun_i   <= BFM_xrun_int;
     end
   end

   always@(curr_state or eightN_cnt_done or mii_en or sending_frame or ipg_count
           or XIF_get_operation or fcs_enable or send_short_frame 
	   or XIF_get_address or Inject_Align_Err or stay_in_fcs_state)
   begin
     nxt_state = IDLE;
     BFM_xrun_int = 1'b0;
      //drive_defaults;           // task to drive all the default outputs
      case(curr_state) 
         IDLE : begin
	     BFM_xrun_int = 1'b0;

             case (XIF_get_operation) // decode the transaction opcode     
               BFM_WRITE: begin
	                if(mii_en) begin 
                           nxt_state = SEND_IPG; 
	                   BFM_xrun_int = 1'b1;
                        end else begin 
                           nxt_state = IDLE;
                           BFM_xrun_int = 1'b0;
                        end
                     end

	       BFM_NOP: begin
                        nxt_state = IDLE;
                        BFM_xrun_int = 1'b0;
                     end

               default : begin 
                      nxt_state = IDLE;
                      BFM_xrun_int = 1'b0;
	           end
             endcase
          end // idle_state

         SEND_IPG : begin	 
	     BFM_xrun_int  = 1'b1;

	     if((XIF_get_address != 0) && (ipg_count != 0))
                nxt_state = SEND_IPG;
	     else
                nxt_state = SEND_PREAM_H; 
         end

         SEND_PREAM_H : begin	 
	     BFM_xrun_int  = 1'b1;

	     if(eightN_cnt_done)
                nxt_state = SEND_PREAM_L; 
	     else
                nxt_state = SEND_PREAM_H;
         end

         SEND_PREAM_L : begin	 
	     BFM_xrun_int  = 1'b1;

	     if(eightN_cnt_done && send_short_frame) begin 
                nxt_state = IDLE;                //send only preamble for short_event
                BFM_xrun_int  = 1'b0;
             end else if(eightN_cnt_done)
                if (Inject_Align_Err) 		// send one extra nibble to gen. Alignment error
		   nxt_state = INJECT_BAD_ALIGN;
		else
                   nxt_state = SEND_DATA; 
             else begin
                nxt_state = SEND_PREAM_L;
             end
         end
	
	INJECT_BAD_ALIGN : begin
	  BFM_xrun_int  = 1'b1;
	  nxt_state = SEND_DATA;
	end


	 SEND_DATA : begin
	     BFM_xrun_int  = 1'b1;

	     if(sending_frame)                    // still sending data
                nxt_state = SEND_DATA;
             else if(fcs_enable)
                nxt_state = SEND_FCS;            // go send fcs
             else begin
                nxt_state = IDLE;                // don't send fcs 
                BFM_xrun_int  = 1'b0;
             end
          end
         
                       // added logic to prevent state change for (valid_bytes==3)	  
         SEND_FCS : begin	 
	     BFM_xrun_int  = 1'b1;

	     if(eightN_cnt_done && !stay_in_fcs_state)begin
                nxt_state = IDLE; 
                BFM_xrun_int  = 1'b0;
             end else begin 
                nxt_state = SEND_FCS;
             end
          end

      endcase // case(curr_state)
   end // always
   


  //----------------------------------------------------------------------
  // 		     	 Generate CRC 
  //  The calculated CRC is inverted before sending out to the FCS-field
  //----------------------------------------------------------------------

   // synthesis tool must expand and optimise this crc-calculator
   function [31:0] calculate_fcs;
      input [31:0] fcs;
      input [7:0]  byte;
      integer 	   index;
      reg 	   feedback;

      begin
	 calculate_fcs = fcs;

	 for (index=0; index<=7; index=index+1)
	    begin
	       feedback = calculate_fcs[24] ^ byte[index];

	       calculate_fcs[24] = calculate_fcs[25];
	       calculate_fcs[25] = calculate_fcs[26];
	       calculate_fcs[26] = calculate_fcs[27];
	       calculate_fcs[27] = calculate_fcs[28];
	       calculate_fcs[28] = calculate_fcs[29];
	       calculate_fcs[29] = calculate_fcs[30]  ^ feedback;
	       calculate_fcs[30] = calculate_fcs[31];
	       calculate_fcs[31] = calculate_fcs[16];

	       calculate_fcs[16] = calculate_fcs[17]  ^ feedback;
	       calculate_fcs[17] = calculate_fcs[18]  ^ feedback;
	       calculate_fcs[18] = calculate_fcs[19];
	       calculate_fcs[19] = calculate_fcs[20];
	       calculate_fcs[20] = calculate_fcs[21];
	       calculate_fcs[21] = calculate_fcs[22];
	       calculate_fcs[22] = calculate_fcs[23];
	       calculate_fcs[23] = calculate_fcs[8]   ^ feedback;

	       calculate_fcs[8]  = calculate_fcs[9];
	       calculate_fcs[9]  = calculate_fcs[10];
	       calculate_fcs[10] = calculate_fcs[11];
	       calculate_fcs[11] = calculate_fcs[12]  ^ feedback;
	       calculate_fcs[12] = calculate_fcs[13]  ^ feedback;
	       calculate_fcs[13] = calculate_fcs[14]  ^ feedback;
	       calculate_fcs[14] = calculate_fcs[15];
	       calculate_fcs[15] = calculate_fcs[0]   ^ feedback;

	       calculate_fcs[0]  = calculate_fcs[1]   ^ feedback;
	       calculate_fcs[1]  = calculate_fcs[2];
	       calculate_fcs[2]  = calculate_fcs[3]   ^ feedback;
	       calculate_fcs[3]  = calculate_fcs[4]   ^ feedback;
	       calculate_fcs[4]  = calculate_fcs[5];
	       calculate_fcs[5]  = calculate_fcs[6]   ^ feedback;
	       calculate_fcs[6]  = calculate_fcs[7]   ^ feedback;
	       calculate_fcs[7]  =                      feedback;
	    end
      end
   endfunction

`endprotect
endmodule
// -- eof --






































