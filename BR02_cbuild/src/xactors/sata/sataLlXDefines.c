/*
 * FILENAME:  sataLlXDefines.c
 * PURPOSE:   Provides implementation of sataLlXDefines.h
 */

#define DEF_BOOL_CONST /* This file actually defines SatallxTrue/False */
#include "sataLlXDefines.h"

/* This routine is used to check if memory is available - used for emulation
 * of no memory situation */
#ifndef NOCARBON
/* satallxMemoryIndex: Memory allocated till now thru NEW macro
 * CarbonXSataLLMaxMemory: Max memory can be allocated - This can be
 *                         modified to emulate no memory situation */
UInt satallxMemoryIndex = 0, CarbonXSataLLMaxMemory = 0;
UInt MEM(size_t size)
{
    if (CarbonXSataLLMaxMemory <= 0) /* Infinite memory - allocation can
                                        fail depending on system */
        return 1;
    /* say no memory when CarbonXSataLLMaxMemory is reached */
    if (satallxMemoryIndex + size > CarbonXSataLLMaxMemory)
        return 0; 
    satallxMemoryIndex += (UInt) size; /* Increment memory count */
    return 1;
}
#endif
