#ifndef NOCARBON

#include "sataLlXTrans.h"
#include "sataLlXtor.h"

/*
 * FILENAME:  sataLlXTrans.c
 *
 * PURPOSE :  Implementation of the functions defined in file sataLlXTrans.h
 */

/*
 * This routine creates a new link layer transaction structure, initializes its 
 * parameters and returns the reference to the calling function.
 */
CarbonXSataLLTrans* carbonXSataLLTransCreate(void)
{
    CarbonXSataLLTrans *trans = NEW(CarbonXSataLLTrans);
    if (trans == NULL)
    {
        satallxError(NULL, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transaction.");
        return NULL;
    }
    /* initialize */
    trans->transType = (CarbonXSataLLTransType) 0;
    trans->frameSize = 0;
    trans->frameData = NULL;
    trans->syncAbortPos = 0;
    trans->dmatPos = 0;
    trans->holdListSize = 0;
    trans->holdPosList = NULL;
    trans->isGoodFis = SatallxTrue;
    trans->powerMode = (CarbonXSataLLPowerDownMode) 0;
    trans->status = CarbonXSataLLTransStatus_Ok;
    return trans;
}

/*
 * This routine destroys the link layer transaction structure
 */
CarbonUInt32 carbonXSataLLTransDestroy(CarbonXSataLLTrans *trans)
{
    if (trans->transType == CarbonXSataLLTrans_Frame)
    {
        /* data list is freed if exists */
        if (trans->frameData != NULL)
            DELETE(trans->frameData);
        /* hold position list is freed if exists */
        if (trans->holdPosList != NULL)
            DELETE(trans->holdPosList);
    }
    DELETE(trans);
    return 1;
}

/*
 * Routine to set the transaction type of the link layer transaction structure 
 */
void carbonXSataLLTransSetType(CarbonXSataLLTrans *trans,
                CarbonXSataLLTransType transType)
{
    trans->transType = transType;
}

/*
 * Routine to return the transaction type of a link layer transaction
 */
CarbonXSataLLTransType carbonXSataLLTransGetType(
        const CarbonXSataLLTrans *trans)
{
    return trans->transType;
}

/*
 * Routine to set the user data list for a link layer tx frame transaction. It
 * copies the data list to a new list whose memory ownership will be on
 * the frame transaction.
 */
CarbonUInt32 carbonXSataLLTransSetFrameData(CarbonXSataLLTrans *trans,
        CarbonUInt32 frameSize, CarbonUInt32 *data)
{
    UInt i;
    if (frameSize > (2048 + 1)) /* check for frameSize not to exceed the 
                                   maximum allowed frame size (2048) plus 
                                   one for FIS type and other fields in the 
                                   first DWORD. */
    {
        return 0;
    }
    /* copies data and take memory ownership */
    trans->frameData = NEWARRAY(CarbonUInt32, frameSize);
    if (trans->frameData == NULL)
    {
        satallxError(NULL, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transmit frame transaction data.");
        return 0;
    }
    trans->frameSize = frameSize;
    for (i = 0; i < frameSize; i++)
        trans->frameData[i] = data[i];
    return 1;
}

/*
 * Routine to return the frame size of the link layer rx frame transaction
 */
CarbonUInt32 carbonXSataLLTransGetFrameSize(const CarbonXSataLLTrans *trans)
{
    return trans->frameSize;
}

/*
 * This routine returns the pointer of the data array of a link layer rx
 * frame transaction
 */
CarbonUInt32* carbonXSataLLTransGetFrameData(const CarbonXSataLLTrans *trans)
{
    return trans->frameData;
}

/*
 * This routine sets Sync Abort position of a link layer tx/rx frame transaction
 */
void carbonXSataLLTransSetSyncAbortPos(CarbonXSataLLTrans *trans,
        CarbonUInt32 pos)
{
    trans->syncAbortPos = pos;
}

/*
 * Routine to set the DMA Abort position of a link layer rx frame transaction
 */
void carbonXSataLLTransSetDmatPos(CarbonXSataLLTrans *trans, CarbonUInt32 pos)
{
    trans->dmatPos = pos;
}

/*
 * Routine to set the hold list for a link layer tx/rx frame transaction. It
 * copies the hold list to a new list whose memory ownership will be on
 * the frame transaction.
 */
CarbonUInt32 carbonXSataLLTransSetHoldPosList(CarbonXSataLLTrans *trans,
        CarbonUInt32 posListSize, CarbonXSataLLFrameHoldPos *holdPosList)
{
    UInt i;
    /* copies and takes the memory ownership */
    trans->holdPosList = NEWARRAY(CarbonXSataLLFrameHoldPos, posListSize);
    if (trans->holdPosList == NULL)
    {
        satallxError(NULL, NULL, CarbonXSataLLError_NoMemory,
            "Can't allocate memory for transmit frame transaction hold list.");
        return 0;
    }
    trans->holdListSize = posListSize;
    for (i = 0; i < posListSize; i++)
        trans->holdPosList[i] = holdPosList[i];
    return 1;
}

/*
 * Routine to set the value of the received FIS status for a link layer
 * rx transaction
 */
void carbonXSataLLTransSetFisStatus(CarbonXSataLLTrans *trans,
        CarbonUInt32 isGoodFis)
{
    trans->isGoodFis = isGoodFis;
}

/*
 * Routine to set the power down mode of a link layer initiated
 * power down transaction
 */
void carbonXSataLLTransSetPowerDownMode(CarbonXSataLLTrans *trans,
        CarbonXSataLLPowerDownMode powerMode)
{
    trans->powerMode = powerMode;
}

/*
 * Routine to get the power down mode of a link layer received
 * power down transaction
 */
CarbonXSataLLPowerDownMode carbonXSataLLTransGetPowerDownMode(
        CarbonXSataLLTrans *trans)
{
    return trans->powerMode;
}

/*
 * Routine to return the transaction status of a link layer transaction
 */
CarbonXSataLLTransStatus carbonXSataLLTransGetTransStatus(
        CarbonXSataLLTrans *trans)
{
    return trans->status;
}

/* 
 * This routine is called by FSM unit to check if it need to send HOLDp
 * for a particular TX or RX DWORD index. If true it also retrieves the number
 * of HOLDp to be sent through the argument "UInt *holdCount", which is always
 * connected to holdCount field of FSM unit structure.
 */
SatallxBool satallxCheckForHold(SatallxTrans *trans,
        UInt frameDataIndex, UInt *holdCount)
{
    UInt i;
    /* check if this routine has been called during frame transaction only */
    if (trans->transType != CarbonXSataLLTrans_Frame)
        return SatallxFalse;
    /* check if there is no hold position array or zero numbers of hold to be
     * sent */
    if (trans->holdListSize == 0 || trans->holdPosList == NULL)
        return SatallxFalse;
    /* iterate over hold position array to find if HOLDp is to be sent */
    for (i = 0; i < trans->holdListSize; i++)
    {
        SatallxFrameHold holdPos = trans->holdPosList[i];
        if (holdPos.pos == frameDataIndex) /* if HOLDp position matches */
        {
            /* for frame reception, only update the hold count if
             * the current position requires more HOLDp to be sent
             * than the earlier hold count */
            if (*holdCount < holdPos.count)
                *holdCount = holdPos.count; /* returns the hold count */
            return SatallxTrue;
        }
    }
    return SatallxFalse;
}

#endif
