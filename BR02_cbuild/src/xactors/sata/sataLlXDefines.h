#ifndef SATALLX_DEFINES_H
#define SATALLX_DEFINES_H

#include <stdio.h>
#include <string.h>
#include <assert.h>

/*!
 * \file sataLlXDefines.h
 * \brief Header file for type definitions.
 *
 * This file abstracts the type definitions for SATA transactor.
*/

/* Unified type definition for integer and unsigned integer */
/*! \brief Type for integer */
typedef int           Int;
/*! \brief Type of unsigned integer */
typedef unsigned int  UInt;

/* Macro for memory management */
#ifndef NOCARBON
#include "carbon/c_memmanager.h"
/*! \brief Checks if is memory available
 *
 * This routine is provided for emulation of no memory case.
 * \param size Memory need to be allocated
 * \retval 1 If memory can be allocated.
 * \retval 0 Otherwise
 */ 
UInt MEM(size_t size);
/*! \brief Macro to allocate memory */
#define NEW(type) (type*)((MEM(1))?carbonmem_malloc(sizeof(type)):NULL)
/*! \brief Macro to allocate memory for array */
#define NEWARRAY(type,s) (type*)((MEM(s))?carbonmem_malloc(sizeof(type)*(s)):NULL)
/*! \brief Macro to free memory */
#define DELETE(x) carbonmem_free(x)
#else
/* For testing without Carbon environment */
#include <malloc.h>
#define NEW(type) (type*) malloc(sizeof(type))
#define NEWARRAY(type,s) (type*) malloc(sizeof(type)*(s))
#define DELETE(x) free(x)
#endif

/* Macro for debug purpose */
/*! \brief Macro to print a variable value */
#define PRINT(x) printf(#x" = %d\n",x)
/*! \brief Macro to print a variable value in binary format */
#define PRINTBIN(x,y) printf(#x" = ");satallxPrintDword(x,(y)-1);printf("\n");
/*! \brief Macro to print a value in D/Kxx.y format */
#define PRINTCHAR(x) printf(#x" = %d.%d\n",x&0x1F,x>>5)
/*! \brief Macro to print error */
#if 0
#define ERROR(x) printf(x"\n")
#else
#define ERROR(x)
#endif

/* Wrapped data types for project */
#ifndef NOCARBON
#include "carbon/carbon_shelltypes.h"
/*! \brief String type */
typedef char*         SatallxString;
/*! \brief Constant string type */
typedef const char*   SatallxConstString;
/*! \brief Bool type */
typedef unsigned int  SatallxBool;
/*! \brief Bit type */
typedef CarbonUInt8   SatallxBit;
/*! \brief Byte (8 bit) type */
typedef CarbonUInt8   SatallxByte;
/*! \brief Word (16 bit) type */
typedef CarbonUInt32  SatallxWord;
/*! \brief Dword (32 bit) type */
typedef CarbonUInt32  SatallxDword;
/*! \brief 3 bit data type */
typedef CarbonUInt8   SatallxData3b;
/*! \brief 4 bit data type */
typedef CarbonUInt8   SatallxData4b;
/*! \brief 5 bit data type */
typedef CarbonUInt8   SatallxData5b;
/*! \brief 6 bit data type */
typedef CarbonUInt8   SatallxData6b;
/*! \brief 10 bit data type */
typedef CarbonUInt32  SatallxData10b;
#else
typedef char*         SatallxString;
typedef const char*   SatallxConstString;
typedef unsigned char SatallxBool;
typedef unsigned char SatallxBit;
typedef unsigned char SatallxByte;
typedef unsigned int  SatallxWord;
typedef unsigned int  SatallxDword;
typedef unsigned char SatallxData3b;
typedef unsigned char SatallxData4b;
typedef unsigned char SatallxData5b;
typedef unsigned char SatallxData6b;
typedef unsigned int  SatallxData10b;
typedef unsigned int  CarbonUInt32;
typedef unsigned long long CarbonUInt64;
#endif

/* Definition of true and false
 * Note, macro 'DEF_BOOL_CONST' is defined only in sataLlXDefines.c so that
 * sataLlXDefines.c can have the definition while other files will use the
 * extern forward declaration. */
#ifdef DEF_BOOL_CONST
/*! \brief Definition of true */
const SatallxBool SatallxFalse = 0;
/*! \brief Definition of false */
const SatallxBool SatallxTrue  = 1;
#endif
/* Declaration of true and false */
/*! \brief Definition of true */
extern const SatallxBool SatallxFalse;
/*! \brief Definition of false */
extern const SatallxBool SatallxTrue;

/* Declaration for SatallXtor */
struct CarbonXSataLLStruct;
/*! \brief Type for Carbon Sata Link Layer Transactor */
typedef struct CarbonXSataLLStruct SatallXtor;

#endif
