/*
 * FILENAME:  sataLlXFsmUnit.c
 *
 * PURPOSE:   This file implements the routines declared in sataLlXFsmUnit.h
 */
  
#ifndef NOCARBON

/*
   Finite State Machine Updation Routine
 */

#include "sataLlXIntf.h"
#include "sataLlXFsmUnit.h"
#include "sataLlXtor.h"

#define DEBUG 0
#define CLEAN_TRANS_QUEUE_ON_SYNC_ABORT 0

/* forward declarations */
static SatallxBool satallxSendTxPrim(SatallxFsmUnit *fsm, SatallxPrimType prim);
static void satallxSendTxBreakContSeq(SatallxFsmUnit *fsm);
static SatallxBool satallxSendTxData(SatallxFsmUnit *fsm, SatallxDword data);
SatallxBool satallxCheckRxIfData(SatallxFsmUnit *fsm);
static SatallxBool satallxCheckRxPrim(SatallxFsmUnit *fsm,
        SatallxPrimType expPrim);
static SatallxBool satallxCheckRxError(SatallxFsmUnit *fsm);
static SatallxDword satallxGetRxData(SatallxFsmUnit *fsm);
static SatallxBool satallxReceiveIfRxData(SatallxFsmUnit *fsm);

static SatallxBool satallxEndTxTrans(SatallxFsmUnit *fsm,
        CarbonXSataLLTransStatus status);
static SatallxBool satallxStartRxTrans(SatallxFsmUnit *fsm,
        CarbonXSataLLTransType transType);
static SatallxBool satallxEndRxTrans(SatallxFsmUnit *fsm,
        CarbonXSataLLTransStatus status);

/* Creates the FSM Unit
 * Called: when transactor is created */
SatallxFsmUnit *satallxCreateFsmUnit(SatallXtor *xtor,
        SatallxFsmTxIntf *txIntf, SatallxFsmRxIntf *rxIntf)
{
    /* creating an FSM Unit structure for the transactor */
    SatallxFsmUnit *fsm = NEW(SatallxFsmUnit);
    if (fsm == NULL)
        return NULL;
    /* setting the reference of the Xtor in the FSM unit */
    fsm->xtor = xtor;
    if (txIntf == NULL || rxIntf == NULL)
        return NULL;
    /* setting the reference of the Tx and Rx interfaces in the FSM unit */
    fsm->txIntf = txIntf;
    fsm->rxIntf = rxIntf;
    /* creating the FSM - PHY interface to be owned by the FSM unit */
    fsm->phyIntf = satallxCreateFsmPhyIntf(xtor->phyIntf);
    if (fsm->phyIntf == NULL)
        return NULL;
    /* initializing the FSM unit structure */
    satallxResetFsmUnit(fsm);
    return fsm;
}

/*
 * This routine destroys the FSM unit structure referred by 
 * " SatallxFsmUnit *fsm".
 */
void satallxDestroyFsmUnit(SatallxFsmUnit *fsm)
{
    satallxDestroyFsmPhyIntf(fsm->phyIntf);/* the phy interface created under 
                                              ownership of the FSM unit is 
                                              destroyed */
    DELETE(fsm);
}

/* This routine is called to reset the FSM unit.
 * Called: at LRESET */
void satallxResetFsmUnit(SatallxFsmUnit *fsm)
{
    fsm->currentState = LS4_RESET; /* FSM state is initialized at LRESET */
    fsm->frameDataIndex = 0;
    fsm->crcDword = 0;
    fsm->rxDataQueue = NULL;
    fsm->rxTrans = NULL;
    fsm->dontDeleteRxTrans = SatallxFalse;
    fsm->holdCount = 0;
    fsm->nPmAck = 16; /* Can be configured to take any value between 4..16 */
    fsm->pmAckRxLatency = 0;
    fsm->pmAckCount = 0;
}

/* Updates the FSM
 * Called: at each posedge of clock */
SatallxBool satallxUpdateFsmUnit(SatallxFsmUnit *fsm, CarbonUInt64 currTime)
{
    SatallXtor *xtor = fsm->xtor;
    SatallxFsmStateType currentState = fsm->currentState;
    SatallxFsmStateType nextState = currentState; /* setting next state to
                                                     current state by default */
    SatallxTrans *txTrans = (SatallxTrans*)
        satallxQueueTop(fsm->xtor->transQueue); /* transaction waiting
                                                   in queue */

#if DEBUG /* for debugging */
    if (fsm->rxIntf->isPrimitive)
        printf("%s: State %s: Received primitive: %s\n", xtor->name,
                satallxGetStateName(currentState),
                satallxGetPrimName(fsm->rxIntf->rxPrimitive));
    else
        printf("%s: State %s: Received data: %d\n", xtor->name,
                satallxGetStateName(currentState), fsm->rxIntf->rxData);
#endif

    /* state transition logic:
     * inputs: current state and received data / primitive
     * output: next state and data / primitive to be transmitted
     */
    switch (currentState)
    {
        /* State transition template::
         * case <current state>: {
         *     1. output logic
         *        send data or primitive to tx unit
         *     2. update FSM internal variables
         *     3. state transition (prioritized)
         *        check the transaction ready in queue and
         *        data or primitive from rx unit to
         *        determine the next state.
         *     4. Depending on state transition, execute callbacks
         * } */
        /**** Idle State Machine ****/
        case L1_IDLE: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_SYNC);
            fsm->holdCount = 0;

            /* state transition */
            if (txTrans != NULL)
            {
                switch (txTrans->transType)
                {
                    case CarbonXSataLLTrans_Reset: {
                        nextState = LS4_RESET;
                        break;
                    }
                    case CarbonXSataLLTrans_Frame: {
                        if (xtor->isHost)
                            nextState = LT1_H_SendChkRdy;/* host FSM state */
                        else
                            nextState = LT2_D_SendChkRdy;/* device FSM state */
                        break;
                    }
                    case CarbonXSataLLTrans_PowerDown: {
                        if (txTrans->powerMode ==
                                CarbonXSataLLPowerMode_Partial)
                            nextState = LPM1_TPMPartial;
                        else if (txTrans->powerMode ==
                                CarbonXSataLLPowerMode_Slumber)
                            nextState = LPM2_TPMSlumber;
                        else
                        {
                            /* error: no other power modes are valid
                             * in the SATA standard.
                             * nextState is thus not updated. */
                        }
                        break;
                    }
                    default: {
                        /* error: no other transaction type are available.
                         * nextState is thus not updated. */
                        satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_Invalid);
                        break;
                    }
                }
            }
            else
            {
                if (satallxCheckRxPrim(fsm, SatallxPrim_X_RDY))
                {
                    if (!satallxStartRxTrans(fsm, CarbonXSataLLTrans_Frame))
                        return SatallxFalse;
                    nextState = LR2_RcvWaitFifo;
                }
                else if (satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_P) ||
                        satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_S))
                {
                    if (!satallxStartRxTrans(fsm, CarbonXSataLLTrans_PowerDown))
                        return SatallxFalse;
                    /* Note this creates Rx transaction but does not
                     * callback rxTransStartCB */
                    fsm->rxTrans->powerMode =
                        (satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_P)) ?
                            CarbonXSataLLPowerMode_Partial:
                            CarbonXSataLLPowerMode_Slumber;
                    if (xtor->config == NULL || xtor->config->isPowerModeOn)
                    {
                        nextState = LPM3_PMOff;
                        fsm->pmAckCount = 0;
                    }
                    else
                    {
                        nextState = LPM4_PMDeny;
                    }
                }
            }
            /* otherwise remain in same state */
            break;
        }
        case L2_SyncEscape: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_SYNC);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_X_RDY) ||
                    satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                nextState = L1_IDLE;
            }
            /* otherwise remain in same state */
            break;
        }
        case LS4_RESET: {
            /* output logic */
            satallxDepositSignal(fsm->phyIntf->COMRESET, 1);
            satallxDepositSignal(fsm->phyIntf->COMWAKE, 0);
            satallxDepositSignal(fsm->phyIntf->PhyRdy, 0);
            satallxDepositSignal(fsm->phyIntf->DeviceDetect, 0);
            satallxDepositSignal(fsm->phyIntf->PhyInternalError, 0);

            satallxResetFsmUnit(fsm);
            satallxResetTxUnit(xtor->txUnit);
            satallxResetRxUnit(xtor->rxUnit);

            /* state transition */
            nextState = LS2_NoComm;
            break;
        }
        case LS2_NoComm: {
            /* output logic */
            satallxDepositSignal(fsm->phyIntf->COMRESET, 0);
            satallxDepositSignal(fsm->phyIntf->PhyRdy, 1);
            satallxDepositSignal(fsm->phyIntf->DeviceDetect, 1);
            satallxSendTxPrim(fsm, SatallxPrim_ALIGN);

            /* state transition */
            nextState = LS3_SendAlign;
            break;
        }
        case LS3_SendAlign: {
            /* output logic */
            satallxDepositSignal(fsm->phyIntf->PhyRdy, 1);
            satallxSendTxPrim(fsm, SatallxPrim_ALIGN);

            /* state transition */
            if (txTrans != NULL &&
                    txTrans->transType == CarbonXSataLLTrans_Reset)
            {
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_Ok);
            }
            nextState = L1_IDLE;
            break;
        }
        case LS1_NoCommErr: { /* PhyRdy is driven by transactor, so this
                                 state is never reachable when in active
                                 state PhyRdy is deasserted by outer world */
            ERROR("UNREACHABLE FSM STATE: LS1_NoCommErr");
            nextState = L1_IDLE;
            /* error */
            satallxError(xtor, NULL, CarbonXSataLLError_FsmInvalidState,
                    "Invalid FSM State LS1_NoCommErr.");
            return SatallxFalse;
            break;
        }
        /**** Transmit State Machine ****/
        case LT1_H_SendChkRdy: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_X_RDY);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_R_RDY))
            {
                nextState = LT3_SendSOF;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_X_RDY))
            {
                if (!satallxStartRxTrans(fsm, CarbonXSataLLTrans_Frame))
                    return SatallxFalse;
                nextState = LR2_RcvWaitFifo;
            }
            /* otherwise remain in same state */
            break;
        }
        case LT2_D_SendChkRdy: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_X_RDY);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_R_RDY))
            {
                nextState = LT3_SendSOF;
            }
            /* difference with LT1_H_SendChkRdy is that it doesn't
             * accept X_RDYp */
            /* otherwise remain in same state */
            break;
        }
        case LT3_SendSOF: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_SOF);

            /* init CRC */
            fsm->crcDword = satallxInitCrc();
            fsm->frameDataIndex = 0;

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else
            {
                nextState = LT4_SendData;
            }
            break;
        }
        case LT4_SendData: {
            SatallxDword data;

            /* output logic: send the data provided earlier */
            if (txTrans == NULL || fsm->frameDataIndex >= txTrans->frameSize)
                satallxError(xtor, txTrans, CarbonXSataLLError_InternalError,
                        "**** INTERNAL ERROR ****");
            data = txTrans->frameData[fsm->frameDataIndex];
            satallxSendTxData(fsm, data);

            /* update CRC */
            satallxUpdateCrc(&(fsm->crcDword), data);
            fsm->frameDataIndex++;

            /* state transition */
            if (txTrans->syncAbortPos == fsm->frameDataIndex)
            { 
                /* TL requests Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L2_SyncEscape;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (txTrans->frameSize == fsm->frameDataIndex) /* Tx done */
            {
                nextState = LT7_SendCRC;
            }
            /* now onwards data transmission is not complete */
            else if (satallxCheckForHold(txTrans,
                        fsm->frameDataIndex, &(fsm->holdCount)))
            {
                nextState = LT6_SendHold;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_DMAT))
            {
                nextState = LT7_SendCRC;
                txTrans->status = CarbonXSataLLTransStatus_DmatAbort;
            }
            else /* Tx not complete, else path will ensure that:
                    txTrans->frameSize < fsm->frameDataIndex */
            {
                if (satallxCheckRxPrim(fsm, SatallxPrim_HOLD))
                {
                    nextState = LT5_RcvrHold;
                }
                else
                {
                    nextState = LT4_SendData;
                }
            }
            break;
        }
        case LT5_RcvrHold: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_HOLDA);

            /* state transition */
            if (txTrans->syncAbortPos == fsm->frameDataIndex)
            { 
                /* TL requests Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L2_SyncEscape;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (fsm->frameDataIndex < txTrans->frameSize)
                /* more data to transmit - ensured */
            {
                if (satallxCheckRxPrim(fsm, SatallxPrim_DMAT))
                {
                    nextState = LT7_SendCRC;

                    /* Going to finish data sending for DMATp received.
                     * This needs CRC to send and hence this HOLDAp should
                     * not be suppressed using CONTp */
                    satallxSendTxBreakContSeq(fsm);

                    txTrans->status = CarbonXSataLLTransStatus_DmatAbort;
                }
                else if (satallxCheckRxPrim(fsm, SatallxPrim_HOLD) ||
                         satallxCheckRxError(fsm))
                {
                    nextState = LT5_RcvrHold;
                }
                else
                {
                    nextState = LT4_SendData;

                    /* for last HOLDAp stop CONTp sequence */
                    satallxSendTxBreakContSeq(fsm);
                }
            }
            else
            {
                /*** error ***/
                satallxError(xtor, NULL,
                        CarbonXSataLLError_FsmInvalidTransition,
                        "Invalid FSM transition from state LT5_RcvrHold.");
                return SatallxFalse;
            }
            break;
        }
        case LT6_SendHold: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_HOLD);
            if (fsm->holdCount == 0)
                satallxError(xtor, txTrans, CarbonXSataLLError_InternalError,
                        "**** INTERNAL ERROR ****");
            fsm->holdCount--;

            /* for last HOLDp stop CONTp sequence */
            if (fsm->holdCount == 0)
                satallxSendTxBreakContSeq(fsm);

            /* state transition */
            if (txTrans->syncAbortPos == fsm->frameDataIndex)
            { 
                /* TL requests Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L2_SyncEscape;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_DMAT))
            {
                nextState = LT7_SendCRC;

                /* Going to finish data sending for DMATp received.
                 * This needs CRC to send and hence this HOLDp should
                 * not be suppressed using CONTp */
                satallxSendTxBreakContSeq(fsm);

                txTrans->status = CarbonXSataLLTransStatus_DmatAbort;
            }
            else if (fsm->frameDataIndex < txTrans->frameSize)
                /* more data to transmit - ensured */
            {
                if (fsm->holdCount > 0)
                {
                    nextState = LT6_SendHold;
                }
                /* now onwards data is ready to transmit */
                else if (satallxCheckRxPrim(fsm, SatallxPrim_HOLD))
                    /* data is ready to transmit and received HOLDp */
                {
                    nextState = LT5_RcvrHold;
                }
                else
                {
                    nextState = LT4_SendData;
                }
            }
            else
            {
                /*** error ***/
                satallxError(xtor, NULL,
                        CarbonXSataLLError_FsmInvalidTransition,
                        "Invalid FSM transition from state LT6_SendHold.");
                return SatallxFalse;
            }
            break;
        }
        case LT7_SendCRC: {
            /* output logic: send the updated CRC */
            satallxSendTxData(fsm, fsm->crcDword);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else
            {
                nextState = LT8_SendEOF;
            }
            break;
        }
        case LT8_SendEOF: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_EOF);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else
            {
                nextState = LT9_Wait;
            }
            break;
        }
        case LT9_Wait: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_WTRM);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_R_OK))
            {
                /* report TL */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_Ok);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_R_ERR))
            {
                /* report TL */
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_FrameErr);
                nextState = L1_IDLE;
            }
            /* otherwise remain in same state */
            break;
        }
        /**** Receive State Machine ****/
        case LR1_RcvChkRdy: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_R_RDY);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_X_RDY))
            {
                nextState = LR1_RcvChkRdy;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SOF))
            {
                /* init CRC */
                fsm->crcDword = satallxInitCrc();
                fsm->frameDataIndex = 0;

                nextState = LR3_RcvData;
            }
            else
            {
                nextState = L1_IDLE;
            }
            break;
        }
        case LR2_RcvWaitFifo: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_SYNC);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_X_RDY))
            {
                if (fsm->holdCount > 0)
                /* logic to wait at starting when fifo space not available */
                {
                    fsm->holdCount--;

                    if (fsm->holdCount > 0)
                        nextState = LR2_RcvWaitFifo;
                    else
                        nextState = LR1_RcvChkRdy;
                }
                else if (satallxCheckForHold(fsm->rxTrans, 0,
                            &(fsm->holdCount)))
                {
                    nextState = LR2_RcvWaitFifo;
                }
                else
                {
                    nextState = LR1_RcvChkRdy;
                }
            }
            else
            {
                if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
                    satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                else
                    satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_Invalid);

                nextState = L1_IDLE;
            }
            break;
        }
        case LR3_RcvData: {
            /* check if rx data and then push to queue */
            satallxReceiveIfRxData(fsm);

            /* output logic
             * do not move this block before data reception block, we
             * need to use updated frameDataIndex value */
            if (fsm->frameDataIndex == fsm->rxTrans->dmatPos)
            {
                satallxSendTxPrim(fsm, SatallxPrim_DMAT);
                fsm->rxTrans->status = CarbonXSataLLTransStatus_DmatAbort;
            }
            else
                satallxSendTxPrim(fsm, SatallxPrim_R_IP);

            /* state transition */
            if (fsm->frameDataIndex == fsm->rxTrans->syncAbortPos)
            {
                /* TL requests Sync Abort */
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_WTRM))
            {
                nextState = LR9_BadEnd;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_EOF))
            {
                nextState = LR6_RcvEOF;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_HOLD))
            {
                nextState = LR5_SendHold;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_HOLDA))
            {
                nextState = LR3_RcvData;
            }
            else if (satallxCheckForHold(fsm->rxTrans,
                        fsm->frameDataIndex, &(fsm->holdCount)))
            {
                nextState = LR4_Hold;
            }
            /* otherwise remain in same state */
            break;
        }
        case LR4_Hold: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_HOLD);
            if (fsm->holdCount == 0)
                satallxError(xtor, fsm->rxTrans,
                        CarbonXSataLLError_InternalError,
                        "**** INTERNAL ERROR ****");
            fsm->holdCount--;

            /* check if rx data and then push to queue */
            if (satallxReceiveIfRxData(fsm))
            {
                /* if data is received check if user wants to send more hold */
                satallxCheckForHold(fsm->rxTrans, fsm->frameDataIndex,
                        &(fsm->holdCount));
            }

            /* state transition */
            if (fsm->frameDataIndex == fsm->rxTrans->syncAbortPos)
            {
                /* TL requests Sync Abort */
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_EOF))
            {
                nextState = LR6_RcvEOF;
            }
            else if (fsm->holdCount > 0)
                /* Fifo space is not available and hold */
            {
                nextState = LR4_Hold;
            }
            /* Now onwards fifo space is available and hold is not required */
            else if (satallxCheckRxPrim(fsm, SatallxPrim_HOLD))
            {
                nextState = LR5_SendHold;
            }
            else
            {
                nextState = LR3_RcvData;
            }
            break;
        }
        case LR5_SendHold: {
            /* check if the received 32 bit is rx data and push into queue */
            satallxReceiveIfRxData(fsm);

            /* output logic */
            if (fsm->frameDataIndex == fsm->rxTrans->dmatPos)
            {
                satallxSendTxPrim(fsm, SatallxPrim_DMAT);
                fsm->rxTrans->status = CarbonXSataLLTransStatus_DmatAbort;
            }
            else
                satallxSendTxPrim(fsm, SatallxPrim_HOLDA);

            /* state transition */
            if (fsm->frameDataIndex == fsm->rxTrans->syncAbortPos)
            {
                /* TL requests Sync Abort */
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_EOF))
            {
                nextState = LR6_RcvEOF;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_HOLD))
            {
                nextState = LR5_SendHold;
            }
            else /* for AnyDword including other primitives and data */
            {
                nextState = LR3_RcvData;

                /* Note that no hold check will happen here as there is no
                 * transition from LR5_SendHold to LR4_Hold */
            }
            break;
        }
        case LR6_RcvEOF: {
            UInt i = 0;
            SatallxDword *data;

            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_R_IP);

            /* check CRC and create rx frame transaction object */
            if (fsm->rxTrans == NULL)
                satallxError(xtor, fsm->rxTrans,
                        CarbonXSataLLError_InternalError,
                        "**** INTERNAL ERROR ****");
            fsm->rxTrans->frameSize =
                (satallxQueueGetCount(fsm->rxDataQueue) - 1);
            fsm->rxTrans->frameData = NEWARRAY(SatallxDword,
                    fsm->rxTrans->frameSize);
            if (fsm->rxTrans->frameData == NULL)
            {
                satallxError(xtor, fsm->rxTrans, CarbonXSataLLError_NoMemory,
                        "Can't allocate memory receive frame data.");
                return SatallxFalse;
            }
            while (satallxQueueGetCount(fsm->rxDataQueue) > 1)
            {
                data = (SatallxDword*) satallxQueuePop(fsm->rxDataQueue);
                fsm->rxTrans->frameData[i++] = *data;
                DELETE(data);
            }
            data = (SatallxDword*) satallxQueuePop(fsm->rxDataQueue);
            DELETE(data);

            /* state transition */
            if (fsm->crcDword == 0) /* CRC matched: note crc is calculated
                                       and updated on all data DWORDs
                                       including CRC sent which ideally
                                       should result zero */
            {
                nextState = LR7_GoodCRC;
            }
            else
            {
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_FrameErr);
                nextState = LR9_BadEnd;
            }
            /* There can not be case where CRC calculation is not complete */
            break;
        }
        case LR7_GoodCRC: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_R_IP);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                /* Peer Sync Abort */
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_SyncAbort);
                nextState = L1_IDLE;
            }
            else
            {
                fsm->dontDeleteRxTrans = SatallxTrue;
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_Ok);
                if (fsm->rxTrans->isGoodFis)
                {
                    nextState = LR8_GoodEnd;
                }
                else
                {
                    nextState = LR9_BadEnd;
                }
                /* After checking the FIS status reported,
                 * transactor should delete the transaction */
                carbonXSataLLTransDestroy(fsm->rxTrans);
                fsm->rxTrans = NULL;
                fsm->dontDeleteRxTrans = SatallxFalse;
            }
            /* Wait for transport layer to report GOOD or BAD FIS and
             * remain in the same state */
            break;
        }
        case LR8_GoodEnd: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_R_OK);

            /* state transition */
            /* now onwards PhyRdy is ON */
            if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                nextState = L1_IDLE;
            }
            /* otherwise remain in same state */
            break;
        }
        case LR9_BadEnd: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_R_ERR);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC))
            {
                nextState = L1_IDLE;
            }
            /* otherwise remain in same state */
            break;
        }
        /**** Power Mode State Machine ****/
        case LPM1_TPMPartial: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_PMREQ_P);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_PMACK))
            {
                nextState = LX1_ConsumePmack; /* -> LPM5_ChkPhyRdy */
                fsm->pmAckCount = 0;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_X_RDY))
            {
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_PmAbort);
                if (!satallxStartRxTrans(fsm, CarbonXSataLLTrans_Frame))
                    return SatallxFalse;
                nextState = LR2_RcvWaitFifo;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC) ||
                    satallxCheckRxPrim(fsm, SatallxPrim_R_OK))
            {
                nextState = LPM1_TPMPartial;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_P) ||
                    satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_S))
            {
                if (xtor->isHost)
                {
                    satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_PmAbort);
                    nextState = L1_IDLE;
                }
                else
                {
                    nextState = LPM1_TPMPartial;
                }
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_PMNAK))
            {
                nextState = LPM9_NoPmnak;
            }
            else if (!satallxCheckRxPrim(fsm, SatallxPrim_ALIGN))
                /* AnyDword other than above */
            {
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_PmAbort);
                nextState = L1_IDLE;
            }
            break;
        }
        case LPM2_TPMSlumber: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_PMREQ_S);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_PMACK))
            {
                nextState = LX1_ConsumePmack; /* -> LPM5_ChkPhyRdy */
                fsm->pmAckCount = 0;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_X_RDY))
            {
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_PmAbort);
                if (!satallxStartRxTrans(fsm, CarbonXSataLLTrans_Frame))
                    return SatallxFalse;
                nextState = LR2_RcvWaitFifo;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_SYNC) ||
                    satallxCheckRxPrim(fsm, SatallxPrim_R_OK))
            {
                nextState = LPM2_TPMSlumber;
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_P) ||
                    satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_S))
            {
                if (xtor->isHost)
                {
                    satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_PmAbort);
                    nextState = L1_IDLE;
                }
                else
                {
                    nextState = LPM2_TPMSlumber;
                }
            }
            else if (satallxCheckRxPrim(fsm, SatallxPrim_PMNAK))
            {
                nextState = LPM9_NoPmnak;
            }
            else if (!satallxCheckRxPrim(fsm, SatallxPrim_ALIGN))
                /* AnyDword other than above */
            {
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_PmAbort);
                nextState = L1_IDLE;
            }
            break;
        }
        case LX1_ConsumePmack: { /* This state is required to consume all
                                    PMACKp from model when transactor has
                                    requested for power mode. If all PMACKp
                                    are not consumed and we move to 
                                    LPM5_ChkPhyRdy, then in setResponse callback
                                    , the test layer can check
                                    for PhyRdy to be low and fail since
                                    model may still be sending PMACKp. This
                                    state is introduced as the transactor
                                    need to take PHY layer also */
            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_PMACK) &&
                    (fsm->pmAckCount < fsm->nPmAck))
            {
                nextState = LX1_ConsumePmack;
                fsm->pmAckCount++;
            }
            else
            {
                nextState = LPM5_ChkPhyRdy;
            }
            break;
        }
        case LPM3_PMOff: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_PMACK);

            /* state transition */
            if (fsm->pmAckCount < (fsm->nPmAck + fsm->pmAckRxLatency))
            {
                nextState = LPM3_PMOff;
                fsm->pmAckCount++;
            }
            else
            {
                nextState = LPM5_ChkPhyRdy;

                /* explicit rxTransStartCB callback, expects that user will
                 * set both host and device PARTIAL/SLUMBER lines in test */
                if (xtor->rxTransStartCB != NULL)
                    (*xtor->rxTransStartCB)(fsm->rxTrans, xtor->userData);

            }
            break;
        }
        case LPM4_PMDeny: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_PMNAK);

            /* explicit rxTransStartCB callback */
            if (xtor->rxTransStartCB != NULL)
                (*xtor->rxTransStartCB)(fsm->rxTrans, xtor->userData);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_P) ||
                    satallxCheckRxPrim(fsm, SatallxPrim_PMREQ_S))
            {
                nextState = LPM4_PMDeny;
            }
            else
            {
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_PmDeny);
                nextState = L1_IDLE;
            }
            break;
        }
        case LPM5_ChkPhyRdy: {
            /* output logic */
            satallxDepositSignal(fsm->phyIntf->PhyRdy, 0);
            satallxDepositSignal(fsm->phyIntf->DeviceDetect, 0);

            /* User should check PARTIAL or SLUMBER to be set in callback */
            if (fsm->rxTrans == NULL)
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_Ok);
            else
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_Ok);

            /* state transition */
            nextState = LPM6_NoCommPower;
            break;
        }
        case LPM6_NoCommPower: { /* At this state, PHY is in
                                    H/DR_Partial/Slumber state */
            /* output logic */
            satallxDepositSignal(fsm->phyIntf->PhyRdy, 0);
            satallxDepositSignal(fsm->phyIntf->DeviceDetect, 0);

            satallxResetTxUnit(xtor->txUnit);
            satallxResetRxUnit(xtor->rxUnit);

            /* state transition */
            if (txTrans != NULL)
            {
                switch (txTrans->transType)
                {
                    case CarbonXSataLLTrans_Reset: {
                        nextState = LS4_RESET;
                        break;
                    }
                    case CarbonXSataLLTrans_PowerWakeUp: {
                        nextState = LPM7_WakeUp1;
                        break;
                    }
                    default: {
                        /* error */
                        satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_Invalid);
                        break;
                    }
                }
            }
            else if (satallxExamineSignal(fsm->phyIntf->PARTIAL) == 0 &&
                        satallxExamineSignal(fsm->phyIntf->SLUMBER) == 0)
            {
                if (!satallxStartRxTrans(fsm, CarbonXSataLLTrans_PowerWakeUp))
                    return SatallxFalse;
                nextState = LPM7_WakeUp1;
            }
#if DEBUG
            printf("%s: State %s: PARTIAL: %d, SLUMBER: %d\n", xtor->name,
                satallxGetStateName(currentState),
                satallxExamineSignal(fsm->phyIntf->PARTIAL),
                satallxExamineSignal(fsm->phyIntf->SLUMBER));
#endif
            /* otherwise remain in same state */
            break;
        }
        case LPM7_WakeUp1: { /* At this state PHY is in H/DR_COMWAKE state */
            /* output logic */
            satallxDepositSignal(fsm->phyIntf->PhyRdy, 1);
            satallxDepositSignal(fsm->phyIntf->DeviceDetect, 1);
            satallxDepositSignal(fsm->phyIntf->COMWAKE, 1);

            /* state transition */
            nextState = LPM8_WakeUp2;
            break;
        }
        case LPM8_WakeUp2: {
            /* output logic */
            satallxDepositSignal(fsm->phyIntf->PhyRdy, 1);
            satallxDepositSignal(fsm->phyIntf->COMWAKE, 0);
            satallxSendTxPrim(fsm, SatallxPrim_ALIGN);

            if (fsm->rxTrans == NULL)
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_Ok);
            else
                satallxEndRxTrans(fsm, CarbonXSataLLTransStatus_Ok);

            /* state transition */
            nextState = L1_IDLE;
            break;
        }
        case LPM9_NoPmnak: {
            /* output logic */
            satallxSendTxPrim(fsm, SatallxPrim_SYNC);

            /* state transition */
            if (satallxCheckRxPrim(fsm, SatallxPrim_PMACK))
            {
                nextState = LPM9_NoPmnak;
            }
            else
            {
                satallxEndTxTrans(fsm, CarbonXSataLLTransStatus_PmDeny);
                nextState = L1_IDLE;
            }
            break;
        }
        default: {
            ERROR("UNSUPPORTED FSM STATE");
            nextState = L1_IDLE;
            /* error */
            satallxError(xtor, NULL, CarbonXSataLLError_FsmInvalidState,
                    "Invalid FSM State.");
            return SatallxFalse;
            break;
        }
    }

    /* Update FSM state */
    fsm->currentState = nextState;

    return SatallxTrue;

    currTime = 0; /* to remove compile time warning */
}

/* This function sends primitive to tx unit */
SatallxBool satallxSendTxPrim(SatallxFsmUnit *fsm, SatallxPrimType prim)
{
    SatallxFsmTxIntf *txIntf = fsm->txIntf;
    txIntf->isPrimitive = SatallxTrue;
    txIntf->txPrimitive = prim;
    txIntf->txStopCont = SatallxFalse;
    txIntf->txData = 0;
    return SatallxTrue;
}

/* This function breaks the CONTp sequence of tx unit */
void satallxSendTxBreakContSeq(SatallxFsmUnit *fsm)
{
    fsm->txIntf->txStopCont = SatallxTrue;
}

/* This function sends data to tx unit */
SatallxBool satallxSendTxData(SatallxFsmUnit *fsm, SatallxDword data)
{
    SatallxFsmTxIntf *txIntf = fsm->txIntf;
    txIntf->isPrimitive = SatallxFalse;
    txIntf->txPrimitive = SatallxPrim_UNDEF;
    txIntf->txStopCont = SatallxFalse;
    txIntf->txData = data;
    return SatallxTrue;
}

/* Checks if the Rx DWORD is primitve */
SatallxBool satallxCheckRxIfData(SatallxFsmUnit *fsm)
{
    return (!fsm->rxIntf->isPrimitive);
}

/* This routine checks the received Primitive against the expected
 * primitive passed as an argument.  */
SatallxBool satallxCheckRxPrim(SatallxFsmUnit *fsm, SatallxPrimType expPrim)
{
    return (fsm->rxIntf->isPrimitive &&
        fsm->rxIntf->rxPrimitive == expPrim);
}

/* This routine checks for reception error from rx unit */
SatallxBool satallxCheckRxError(SatallxFsmUnit *fsm)
{
    return fsm->rxIntf->hasError;
}

/* This routine receives data from rx unit */
SatallxDword satallxGetRxData(SatallxFsmUnit *fsm)
{
    if (!fsm->rxIntf->isPrimitive)
        return fsm->rxIntf->rxData;
    satallxError(fsm->xtor, fsm->rxTrans,
            CarbonXSataLLError_InternalError, "**** INTERNAL ERROR ****");
    return 0;
}

/* This routine checks if DWORD received at rx unit is data. If so,
 * it retrieves the data and pushes in a queue so that at EOF, RX transaction
 * data can be formed out of the queue */
SatallxBool satallxReceiveIfRxData(SatallxFsmUnit *fsm)
{
    SatallxDword *data;

    if (!satallxCheckRxIfData(fsm)) /* if rx dword is not data */
        return SatallxFalse;

    /* get the rx data and put it in rx queue */
    data = NEW(SatallxDword);
    if (data == NULL)
    {
        satallxError(fsm->xtor, fsm->rxTrans, CarbonXSataLLError_NoMemory,
                "Can't allocate memory receive frame data.");
        return SatallxFalse;
    }
    *data = satallxGetRxData(fsm);
    if (fsm->rxDataQueue == NULL)
        satallxError(fsm->xtor, fsm->rxTrans,
                CarbonXSataLLError_InternalError, "**** INTERNAL ERROR ****");
    satallxQueuePush(fsm->rxDataQueue, data);
    fsm->frameDataIndex++;

    /* update CRC */
    satallxUpdateCrc(&(fsm->crcDword), *data);

    return SatallxTrue;
}

/* Routine to complete TX transaction:
 * It pops the tx transaction from queue and calls reportTransaction
 * For example, this function is called from LT7_SendCRC state for
 * normal frame transmission */
SatallxBool satallxEndTxTrans(SatallxFsmUnit *fsm,
        CarbonXSataLLTransStatus status)
{
    SatallXtor *xtor = fsm->xtor;
    /* pops the transaction from queue so that next transaction
     * can be processed */
    SatallxTrans *trans = (SatallxTrans*) satallxQueuePop(xtor->transQueue);
    if (trans == NULL)
        return SatallxFalse;
    /* This is a special case where DMATp has been received and transactor
     * is completing transmission with CRC and EOF. At this situation,
     * we call this function with OK status from LT7_SendCRC though
     * we are ending due to DMAT - because we have no way to identify from
     * LT7_SendCRC that we have reached this state due to DMATp */
    if (trans->status == CarbonXSataLLTransStatus_DmatAbort &&
            status == CarbonXSataLLTransStatus_Ok)
        status = trans->status; /* making both same for code maintainance */
    else
        trans->status = status;
#if CLEAN_TRANS_QUEUE_ON_SYNC_ABORT
    if (trans->status == CarbonXSataLLTransStatus_SyncAbort)
    {
        while (satallxQueueGetCount(xtor->transQueue) > 0)
        {
            SatallxTrans *queueTrans = (SatallxTrans*)
                satallxQueuePop(xtor->transQueue);
            /* Do not delete by carbonXSataLLTransDestroy(queueTrans); */
        }
    }
#endif
    /* reportTransaction callback */
    if (fsm->xtor->txTransEndCB != NULL)
        (*fsm->xtor->txTransEndCB)(trans, xtor->userData);
    /* Do not delete by carbonXSataLLTransDestroy(trans); */
    return SatallxTrue;
}

/* This routine is called when a rx transaction is starting::
 * This routine creates the rx transaction object, create rx data queue for
 * frame and calls setDefaultResponse callback.  */
SatallxBool satallxStartRxTrans(SatallxFsmUnit *fsm,
        CarbonXSataLLTransType transType)
{
    SatallXtor *xtor = fsm->xtor;
    /* creates transaction object */
    fsm->rxTrans = carbonXSataLLTransCreate();
    if (fsm->rxTrans == NULL)
    {
        satallxError(xtor, fsm->rxTrans, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for received frame transaction.");
        return SatallxFalse;
    }
    if (!(transType == CarbonXSataLLTrans_Frame ||
            transType == CarbonXSataLLTrans_PowerDown ||
            transType == CarbonXSataLLTrans_PowerWakeUp))
        satallxError(xtor, fsm->rxTrans,
                CarbonXSataLLError_InternalError, "**** INTERNAL ERROR ****");
    /* if rx transaction is of frame type, then create rx data queue
     * to hold the DWORDs */
    if (transType == CarbonXSataLLTrans_Frame)
    {
        fsm->rxDataQueue = satallxCreateQueue(2048 + 2); /* one for FIS type
                                                            and one for CRC */
        if (fsm->rxDataQueue == NULL)
        {
            satallxError(xtor, fsm->rxTrans, CarbonXSataLLError_NoMemory,
                    "Can't allocate memory for receive frame data.");
            return SatallxFalse;
        }
    }
    /* setDefaultResponse callback */
    carbonXSataLLTransSetType(fsm->rxTrans, transType);
    if (transType != CarbonXSataLLTrans_PowerDown &&
            /* For Rx Power Down explicit callback rxTransStartCB */
            xtor->rxTransStartCB != NULL)
        (*xtor->rxTransStartCB)(fsm->rxTrans, xtor->userData);
    return SatallxTrue;
}

/* This routine is called to end rx transaction::
 * This routine calls setResponse callback and free ups memory for
 * rx transaction */
SatallxBool satallxEndRxTrans(SatallxFsmUnit *fsm,
        CarbonXSataLLTransStatus status)
{
    SatallXtor *xtor = fsm->xtor;
    SatallxTrans *trans = fsm->rxTrans;
    if (trans == NULL)
        return SatallxFalse;
    /* Special case for DMATp reception */
    if (trans->status == CarbonXSataLLTransStatus_DmatAbort &&
            status == CarbonXSataLLTransStatus_Ok)
        status = trans->status;
    else
        trans->status = status;
#if CLEAN_TRANS_QUEUE_ON_SYNC_ABORT
    if (trans->status == CarbonXSataLLTransStatus_SyncAbort)
    {
        while (satallxQueueGetCount(xtor->transQueue) > 0)
        {
            SatallxTrans *queueTrans = (SatallxTrans*)
                satallxQueuePop(xtor->transQueue);
            /* Do not delete by carbonXSataLLTransDestroy(queueTrans); */
        }
    }
#endif
    /* setResponse callback */
    if (xtor->rxTransEndCB != NULL)
        (*xtor->rxTransEndCB)(trans, xtor->userData);

    /* do not delete receive frame transaction when CRC is good, because
     * transactor has to wait for test layer to report Good or Bad FIS
     * thru this transaction object. Transactor FSM has to check the
     * FIS status reported and delete this transaction appropriately */
    if (!fsm->dontDeleteRxTrans)
    {
        carbonXSataLLTransDestroy(trans); /* Transactor should delete this */
        fsm->rxTrans = NULL;
    }

    /* free up memory for rx transaction */
    if (fsm->rxDataQueue != NULL)
    {
        while (satallxQueueGetCount(fsm->rxDataQueue) > 0)
        {
            SatallxDword *data = (SatallxDword*)
                satallxQueuePop(fsm->rxDataQueue);
            DELETE(data);
        }
        satallxDestroyQueue(fsm->rxDataQueue);
        fsm->rxDataQueue = NULL;
    }
    return SatallxTrue;
}

/* This routine returns the string name of state enum */
char *satallxGetStateName(SatallxFsmStateType state)
{
    char *strState = NULL;
    switch (state)
    {
        case L_UNDEF_STATE: strState = "L_UNDEF_STATE"; break;
        case L1_IDLE: strState = "L1_IDLE"; break;
        case L2_SyncEscape: strState = "L2_SyncEscape"; break;
        case LS1_NoCommErr: strState = "LS1_NoCommErr"; break;
        case LS2_NoComm: strState = "LS2_NoComm"; break;
        case LS3_SendAlign: strState = "LS3_SendAlign"; break;
        case LS4_RESET: strState = "LS4_RESET"; break;
        case LT1_H_SendChkRdy: strState = "LT1_H_SendChkRdy"; break;
        case LT2_D_SendChkRdy: strState = "LT2_D_SendChkRdy"; break;
        case LT3_SendSOF: strState = "LT3_SendSOF"; break;
        case LT4_SendData: strState = "LT4_SendData"; break;
        case LT5_RcvrHold: strState = "LT5_RcvrHold"; break;
        case LT6_SendHold: strState = "LT6_SendHold"; break;
        case LT7_SendCRC: strState = "LT7_SendCRC"; break;
        case LT8_SendEOF: strState = "LT8_SendEOF"; break;
        case LT9_Wait: strState = "LT9_Wait"; break;
        case LR1_RcvChkRdy: strState = "LR1_RcvChkRdy"; break;
        case LR2_RcvWaitFifo: strState = "LR2_RcvWaitFifo"; break;
        case LR3_RcvData: strState = "LR3_RcvData"; break;
        case LR4_Hold: strState = "LR4_Hold"; break;
        case LR5_SendHold: strState = "LR5_SendHold"; break;
        case LR6_RcvEOF: strState = "LR6_RcvEOF"; break;
        case LR7_GoodCRC: strState = "LR7_GoodCRC"; break;
        case LR8_GoodEnd: strState = "LR8_GoodEnd"; break;
        case LR9_BadEnd: strState = "LR9_BadEnd"; break;
        case LPM1_TPMPartial: strState = "LPM1_TPMPartial"; break;
        case LPM2_TPMSlumber: strState = "LPM2_TPMSlumber"; break;
        case LPM3_PMOff: strState = "LPM3_PMOff"; break;
        case LPM4_PMDeny: strState = "LPM4_PMDeny"; break;
        case LPM5_ChkPhyRdy: strState = "LPM5_ChkPhyRdy"; break;
        case LPM6_NoCommPower: strState = "LPM6_NoCommPower"; break;
        case LPM7_WakeUp1: strState = "LPM7_WakeUp1"; break;
        case LPM8_WakeUp2: strState = "LPM8_WakeUp2"; break;
        case LPM9_NoPmnak: strState = "LPM9_NoPmnak"; break;
        default: strState = NULL; break;
    }
    return strState;
}

#endif
