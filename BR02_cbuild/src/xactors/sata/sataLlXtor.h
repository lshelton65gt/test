#ifndef SATALLX_XTOR_H
#define SATALLX_XTOR_H

#include "xactors/sata/carbonXSataLL.h"
#include "sataLlXUtil.h"
#include "sataLlXIntf.h"
#include "sataLlXFsmUnit.h"
#include "sataLlXTxUnit.h"
#include "sataLlXRxUnit.h"
#include "sataLlXTrans.h"
#include "util/UtCLicense.h"

/*!
 * \file sataLlXtor.h
 * \brief SATA LL Transactor 
 *
 * This file provides the definition of the structure of transactor and 
 * declares the functions used to connect transactor signal with Carbon Model net.
 */

/*!
 * \brief Transactor Configuration Structure
 */
struct CarbonXSataLLConfigStruct {
    SatallxBool isPowerModeOn;  /*!< Configuration for power mode on or off */
    SatallxBool isScramblingOn; /*!< Configuration for scrambling support on or
                                      off */
    SatallxBool isContpGen;     /*!< Configuration for CONT primitive generation
                                     support on or off */  
};

/*!
 * \brief Carbon SATA Link Layer Internal Structure Definition
 */
struct CarbonXSataLLStruct {
    /* properties */
    SatallxString name; /*!< Transactor name */
    SatallxBool isHost; /*!< Transactor type i.e Host or Device */
    struct CarbonXSataLLConfigStruct *config; /*!< Transactor configuration */
    void *userData; /*!< User data provided by user */ 

    /* callback routines */
    void (*txTransEndCB)(CarbonXSataLLTrans*, void*);   /*!< reportTransaction 
                                                             callback pointer */
    void (*rxTransStartCB)(CarbonXSataLLTrans*, void*); /*!< setDefaultResponse
                                                             callback pointer */
    void (*rxTransEndCB)(CarbonXSataLLTrans*, void*);   /*!< setResponse
                                                             callback pointer */
    void (*errorTransCB)(CarbonXSataLLErrorType,
            CarbonXSataLLTrans*, void*); /*!< notify callback pointer */
    void (*errorCB)(CarbonXSataLLErrorType, void*); /*!< notify2 callback */  

    /* interfaces */
    SatallxQueue *transQueue; /*!< Transaction queue */
    SatallxPhyIntf *phyIntf;  /*!< Physical interface to Carbon Model consisting of
                                   10 signals */

    /* building units */
    SatallxFsmUnit *fsmUnit;  /*!< FSM Unit */ 
    SatallxTxUnit *txUnit;    /*!< Tx Unit */
    SatallxRxUnit *rxUnit;    /*!< Rx Unit */

    /* internal parameters */
    UInt clockCount;          /*!< Clock count */

  /* Carbon Licensing parameter */
  CarbonLicenseData *mCarbonLicense;

};

/*!
 * \brief Get the transactor's physical net handle
 *
 * This function takes a physical interface net name and returns the
 * handle of transactor signal wrapper
 *
 * \param xtor Transactor
 * \param xtorNetName The name of the physical interface  net 
 *
 * \returns The handle of the physical interface net
 */
SatallxPhyNet *satallxGetPhyNetByName(SatallXtor *xtor,
        SatallxConstString xtorNetName);

/*!
 * \brief Connect transactor net with Carbon Model net
 *
 * This function Connect the physical net handle of transactor to the
 * Carbon Model net
 *
 * \param xtor Transactor 
 * \param xtorNetName Net name to be connected
 * \param model Carbon Model module handle
 * \param modelNet Carbon Model net handle
 *  
 * \returns TRUE in case of proper connection and FALSE otherwise
 */ 
SatallxBool satallxConnectXtorSignal(SatallXtor *xtor,
        SatallxConstString xtorNetName,
        CarbonObjectID *model, CarbonNetID *modelNet);

/*!
 * \brief Common Erroring Routine
 *
 * This function is called whenever something error happens. Now notify or
 * notify2 callbacks are available, we use that otherwise errorm message is
 * displayed in console.
 *
 * \param xtor Transactor 
 * \param trans Transaction for which the error has occurred
 * \param errorType Error type
 * \param errorMessage Error message
 */
void satallxError(SatallXtor *xtor, SatallxTrans *trans,
        CarbonXSataLLErrorType errorType, SatallxString errorMessage);

#endif
