#ifndef SATALLX_PRIMUTIL_H
#define SATALLX_PRIMUTIL_H

#include "sataLlXUtil.h"

/*!
 * \file sataLlXPrim.h
 * \brief SATA LL Primitive Types
 * 
 *  This file provides the definition of SATA LL Primitive Types and declares
 *  the functions that can be used to get the bytes of primitive or to find the
 *  primitive type or name 
 */

/*!
 * \brief SATA LL Primitive Types
 */
typedef enum {
    SatallxPrim_UNDEF, /*!< Undefined Primitive */
    SatallxPrim_ALIGN, /*!< ALIGN Primitive */
    SatallxPrim_CONT,  /*!< CONT Primitive */
    SatallxPrim_DMAT,  /*!< DMAT Primitive */
    SatallxPrim_EOF,   /*!< EOF Primitive */
    SatallxPrim_HOLD,  /*!< HOLD Primitive */
    SatallxPrim_HOLDA, /*!< HOLDA Primitive */
    SatallxPrim_PMACK, /*!< PMACK Primitive */
    SatallxPrim_PMNAK, /*!< PMNACK Primitive */
    SatallxPrim_PMREQ_P,/*!< PMREQ_P Primitive */ 
    SatallxPrim_PMREQ_S,/*!< PMREQ_S Primitive */
    SatallxPrim_R_ERR,  /*!< R_ERR Primitive */
    SatallxPrim_R_IP,   /*!< R_IP Primitive */
    SatallxPrim_R_OK,   /*!< R_OK Primitive */
    SatallxPrim_R_RDY,  /*!< R_RDY Primitive */
    SatallxPrim_SOF,    /*!< SOF Primitive */
    SatallxPrim_SYNC,   /*!< SYNC Primitive */
    SatallxPrim_WTRM,   /*!< WTRM Primitive */
    SatallxPrim_X_RDY   /*!< X_RDY Primitive */
} SatallxPrimType;

/*!
 * \brief This function returns a byte of a specified Primitive 
 *
 * \param prim Primitive type
 * \param index Position to be mentioned to get a byte of a Primitive
 *              specified in prim  
 *
 * \returns A structure containing a byte of specified primitive and the 
 *          information whether the byte is control character or not.
 */
SatallxCharacter satallxGetPrimByte(SatallxPrimType prim, UInt index);

/*!
 * \brief Checks if a char array corresponds to primitive and returns its type
 *
 * This function takes an array of character (i.e. structure containing byte of
 * primitive and the information whether the byte is control character or not)
 * and returns the type of primitive.
 * 
 * \param charArray Array of char
 *
 * \returns Type of primitive, SatallxPrim_UNDEF if the array doesn't
 *          corresponds to a primitive
 */
SatallxPrimType satallxFindPrim(SatallxCharacter charArray[]);

/*!
 * \brief Gets the string name of a primitive
 *
 * \param prim Type of primitive
 *
 * \returns String corresponding to primitive
 */
char *satallxGetPrimName(SatallxPrimType prim);

#endif
