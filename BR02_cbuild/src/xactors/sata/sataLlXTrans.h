#ifndef SATALLX_TRANS_H
#define SATALLX_TRANS_H

#include "sataLlXDefines.h"
#include "xactors/sata/carbonXSataLLTrans.h"

/*!
 * \file sataLlXTrans.h
 * \brief Definition SATA Link Layer Transaction Structure
 *
 * This file defines link layer transaction structure and related
 * routines.
 */

/*!
 * \brief Type definition of SatallxFrameHold
 */
typedef CarbonXSataLLFrameHoldPos SatallxFrameHold;

/*!
 * \brief Carbon SATA Link Layer transaction structure
 */
typedef struct CarbonXSataLLTransStruct {
    CarbonXSataLLTransType transType;     /*!< Transaction type */
    CarbonUInt32 frameSize;               /*!< Size of the particular
                                               transaction FIS in terms of
                                               DWORDS */
    CarbonUInt32 *frameData;              /*!< Data as DWORD array for
                                               frame transaction */
    CarbonUInt32 syncAbortPos;            /*!< Position of Sync Abort 
                                               during frame transmission 
                                               (or reception), i.e., index
                                               of DWORD before which SYNCp
                                               will be sent */
    CarbonUInt32 dmatPos;                 /*!< Position of DMA Terminate
                                               during frame reception,
                                               i.e., index of DWORD before
                                               which DMATp will be sent */
    CarbonUInt32 holdListSize;            /*!< Size of the hold list pointed by
                                               holdPosList */
    SatallxFrameHold *holdPosList;        /*!< Hold position list array */
    CarbonUInt32 isGoodFis;               /*!< Staus of the received FIS
                                               reported by user */
    CarbonXSataLLPowerDownMode powerMode; /*!< Power mode for power down
                                               transaction */
    CarbonXSataLLTransStatus status;      /*!< Current transaction status 
                                               to be reported thru callbacks */
} SatallxTrans;

/*!
 * \brief Checks hold position list for updating hold count.
 * 
 * This routine checks the Tx or Rx "frameDataIndex" against the position of
 * HOLDp to be transmitted from the hold position list of the particular LL 
 * transaction structure and updates the holdCount field of the FSM unit 
 * structure.
 * \param trans Reference to the Transaction structure
 * \param frameDataIndex Current frame index of data transaction
 * \param holdCount Reference to the hold count register to be updated
 * \returns True on success, else false
 */
SatallxBool satallxCheckForHold(SatallxTrans *trans,
        UInt frameDataIndex, UInt *holdCount);

#endif
