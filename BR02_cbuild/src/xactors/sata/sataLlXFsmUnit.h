#ifndef SATALLX_FSMUNIT_H
#define SATALLX_FSMUNIT_H

#include "sataLlXIntf.h"

/*!
 * \file sataLlXFsmUnit.h
 * \brief Header file for FSM unit
 *
 * This file provides definition of FSM unit structure and state enum.
 */

/*! \brief Type for FSM states */
typedef enum {
    /* Link Idle States */
    L_UNDEF_STATE,     /*!< any undefined LL FSM state */
    L1_IDLE,           /*!< L1: L_IDLE state */
    L2_SyncEscape,     /*!< L2: L_SyncEscape state,
                            SATA 2.5 Gold only: protocol improved */
    LS1_NoCommErr,     /*!< LS1: L_NoCommErr */
    LS2_NoComm,        /*!< LS2: L_NoComm */
    LS3_SendAlign,     /*!< LS3: L_SendAlign */
    LS4_RESET,         /*!< LS4: L_RESET */

    /* Link Transmit States */
    LT1_H_SendChkRdy,  /*!< LT1: HL_SendChkRdy */
    LT2_D_SendChkRdy,  /*!< LT2: DL_SendChkRdy */
    LT3_SendSOF,       /*!< LT3: L_SendSOF */
    LT4_SendData,      /*!< LT4: L_SendData */
    LT5_RcvrHold,      /*!< LT5: L_RcvrHold */
    LT6_SendHold,      /*!< LT6: L_SendHold */
    LT7_SendCRC,       /*!< LT7: L_SendCRC */
    LT8_SendEOF,       /*!< LT8: L_SendEOF */
    LT9_Wait,          /*!< LT9: L_Wait */

    /* Link Receive States*/
    LR1_RcvChkRdy,     /*!< LR1: L_RcvChkRdy */
    LR2_RcvWaitFifo,   /*!< LR2: L_ RcvWaitFifo */
    LR3_RcvData,       /*!< LR3: L_RcvData */
    LR4_Hold,          /*!< LR4: L_Hold */
    LR5_SendHold,      /*!< LR5: L_SendHold */
    LR6_RcvEOF,        /*!< LR6: L_RcvEOF */
    LR7_GoodCRC,       /*!< LR7: L_GoodCRC */
    LR8_GoodEnd,       /*!< LR8: L_GoodEnd */
    LR9_BadEnd,        /*!< LR9: L_BadEnd */
   
    /* Link power mode States*/
    LPM1_TPMPartial,   /*!< LPM1: L_TPMPartial */
    LPM2_TPMSlumber,   /*!< LPM2: L_TPMSlumber */
    LX1_ConsumePmack,  /*!< Transactor specific state to consume PMACKp before
                            transiting to ChkPhyRdy to check PhyRdy signal */
    LPM3_PMOff,        /*!< LPM3: L_PMOff */
    LPM4_PMDeny,       /*!< LPM4: L_PMDeny */
    LPM5_ChkPhyRdy,    /*!< LPM5: L_ChkPhyRdy */
    LPM6_NoCommPower,  /*!< LPM6: L_NoCommPower */
    LPM7_WakeUp1,      /*!< LPM7: L_WakeUp1 */
    LPM8_WakeUp2,      /*!< LPM8: L_WakeUp2 */
    LPM9_NoPmnak       /*!< LPM9: L_NoPmnak state,
                            SATA 2.5 Gold only: protocol improved */
} SatallxFsmStateType;

/*!
 * \brief Structure for FSM unit
 *
 * This structure holds the current state and state related information
 * for FSM, which will be updated each time the FSM is executed.
 */
typedef struct {
    /* Interfaces */
    SatallxFsmTxIntf *txIntf;             /*!< FSM Tx Interface (reference) */
    SatallxFsmRxIntf *rxIntf;             /*!< FSM Rx Interface (reference) */
    SatallxFsmPhyIntf *phyIntf;           /*!< FSM Phy Interface (containment)*/

    /* Properties */
    SatallXtor *xtor;                     /*!< Transactor reference */
    SatallxFsmStateType currentState;     /*!< FSM State */
    UInt frameDataIndex;                  /*!< DWORD index within frame */
    SatallxDword crcDword;                /*!< Running CRC */
    SatallxQueue *rxDataQueue;            /*!< Rx Data Queue */
    SatallxTrans *rxTrans;                /*!< Rx frame under reception */
    SatallxBool dontDeleteRxTrans;        /*!< Flag to indicate that RxTrans
                                               will be deleted explicitly */
    UInt holdCount;                       /*!< Ongoing Hold Count */
    UInt nPmAck;                          /*!< Number of PMACKp to be sent
                                               at LPM3_PMOff state,
                                               4 <= nPmAck <= 16 */
    UInt pmAckRxLatency;                  /*!< Expected time the model
                                               takes to receive PMACKp and
                                               make PhyRdy low */
    UInt pmAckCount;                      /*!< PMACKp count at LPM3_PMOff */
} SatallxFsmUnit;

/*!
 * \brief Creates FSM unit
 *
 * This routine creates FSM unit structure by allocating memory for it and
 * connecting the TX and RX unit interfaces
 * \param xtor Parent transactor reference
 * \param txIntf TX unit interface reference
 * \param rxIntf RX unit interface reference
 * \returns Pointer to the created FSM unit structure
 */
SatallxFsmUnit *satallxCreateFsmUnit(SatallXtor *xtor,
        SatallxFsmTxIntf *txIntf, SatallxFsmRxIntf *rxIntf);

/*!
 * \brief Destroys the FSM unit
 * \param fsm Pointer to FSM unit to be destroyed
 */
void satallxDestroyFsmUnit(SatallxFsmUnit *fsm);

/*!
 * \brief Resets the FSM unit
 * \param fsm Pointer to FSM unit
 */
void satallxResetFsmUnit(SatallxFsmUnit *fsm);

/*!
 * \brief Updates the FSM unit
 *
 * This routine does the FSM transition of states.
 * \param fsm Pointer to FSM unit
 * \param currTime Time of simulation
 * \returns True if updation is ok, false otherwise.
 */
SatallxBool satallxUpdateFsmUnit(SatallxFsmUnit *fsm, CarbonUInt64 currTime);

/*!
 * \brief Gets the string for a state enum
 * \param state State
 * \returns String name for the state
 */
char *satallxGetStateName(SatallxFsmStateType state);

#endif
