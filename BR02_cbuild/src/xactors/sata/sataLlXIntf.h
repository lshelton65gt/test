#ifndef SATALLX_INTF_H
#define SATALLX_INTF_H

#include "sataLlXDefines.h"
#include "sataLlXPrim.h"
#include "sataLlXQueue.h"

#ifndef NOCARBON
#include "sataLlXTrans.h"

/*! 
 * \file sataLlXIntf.h
 * \brief Header file for interfaces
 *
 * This file provides the definitions for the various interfaces of the Xtor.
 */

/*!
 * \brief Structure to represent a physical net
 *
 * This is a wrapper on Carbon net object and Callback mechanism. This wrapper
 * provides a unified methodology for the transactor to interact with the Carbon Model.
 */
typedef struct {
    SatallXtor *xtor;               /*!< Transactor reference */
    SatallxString name;             /*!< Net name */
    SatallxBool isOutput;           /*!< Specifies the direction */
    SatallxBool isScalar;           /*!< Specifies if scalar or vector.
                                         Note, for vector nets e.g. DATAIN/OUT
                                         size is fixed to 10 bits. */
    SatallxDword currValue;         /*!< Buffered value of actual net.
                                         Current value to be used during
                                         tansactor evaluation. This gets
                                         updated during refresh cycle */
    /* Carbon Model connection mode */
    CarbonObjectID *module;         /*!< Carbon Model module of actual net */
    CarbonNetID *net;               /*!< Carbon Model actual net */

    /* Callback connection mode */
    CarbonXSignalChangeCB changeCB; /*!< Callback routine to refresh the
                                         currValue with actual net */
    void *cbData;                   /*!< User data for callback mode */
} SatallxPhyNet;
#else
typedef struct {
    char *name;
    SatallxDword value;
} SatallxPhyNet;
#endif

/*!
 * \brief Physical Interface of entire transactor
 *
 * This structure contains all physical nets connected with the 
 * transactor. Note, the SatallxPhyNets within this have containment
 * relationship with the structure.
 */
typedef struct {
    SatallxPhyNet *COMRESET;         /*!< Wrapper net for COMRESET */
    SatallxPhyNet *COMWAKE;          /*!< Wrapper net for COMWAKE */
    SatallxPhyNet *PhyRdy;           /*!< Wrapper net for PhyRdy */
    SatallxPhyNet *DeviceDetect;     /*!< Wrapper net for DeviceDetect */
    SatallxPhyNet *PhyInternalError; /*!< Wrapper net for PhyInternalError */
    SatallxPhyNet *COMMA;            /*!< Wrapper net for COMMA */
    SatallxPhyNet *PARTIAL;          /*!< Wrapper net for PARTIAL */
    SatallxPhyNet *SLUMBER;          /*!< Wrapper net for SLUMBER */
    SatallxPhyNet *DATAOUT;          /*!< Wrapper net for DATAOUT */
    SatallxPhyNet *DATAIN;           /*!< Wrapper net for DATAIN */
} SatallxPhyIntf;

/*!
 * \brief FSM Interface to Transmit Logic
 *
 * This interface is used by FSM to instruct the transmit unit about the
 * primitive or data DWORD to be sent.
 */
typedef struct {
    SatallxBool isPrimitive;     /*!< Denotes if the DWORD is primitive */
    SatallxPrimType txPrimitive; /*!< Primitive enum - valid if
                                      isPrimitive is true */
    SatallxBool txStopCont;      /*!< Instruction to stop CONTp generation */
    SatallxDword txData;         /*!< Data DWORD - valid if isPrimitive
                                      is false */
} SatallxFsmTxIntf;

/*!
 * \brief FSM Interface to Receive Logic
 *
 * This interface is used by the FSM to receive the incomming primitives or
 * data DWORDs and their error status from the receive unit.
 */
typedef struct {
    SatallxBool isPrimitive;     /*!< Denotes if the DWORD is primitive */
    SatallxPrimType rxPrimitive; /*!< Primitive enum - valid if
                                      isPrimitive is true */
    SatallxDword rxData;         /*!< Data DWORD - valid if isPrimitive
                                      is false */
    SatallxBool hasError;        /*!< Error Status of the received DWORD */
} SatallxFsmRxIntf;

/*!
 * \brief FSM Physical Interface
 *
 * This structure holds the reference to physical nets that are 
 * encapsulated by SatallxPhyIntf for SATA LL FSM - PHY interface.
 */
typedef struct {
    SatallxPhyNet *COMRESET;        /*!< Reference to COMRESET net of PhyIntf */
    SatallxPhyNet *COMWAKE;         /*!< Reference to COMWAKE net of PhyIntf */
    SatallxPhyNet *PhyRdy;          /*!< Reference to PhyRdy net of PhyIntf */
    SatallxPhyNet *DeviceDetect;    /*!< Reference to DeviceDetect net of 
                                         PhyIntf */
    SatallxPhyNet *PhyInternalError;/*!< Reference to PhyInternalError net of 
                                         PhyIntf */
    SatallxPhyNet *PARTIAL;         /*!< Reference to PARTIAL net of PhyIntf */
    SatallxPhyNet *SLUMBER;         /*!< Reference to SLUMBER net of PhyIntf */
} SatallxFsmPhyIntf;

/*!
 * \brief Transmit Logic Physical Interface
 *
 * This structure holds the reference to actual physical nets that are
 * encapsulated by SatallxPhyIntf for SATA LL Tx - PHY interface.
 */
typedef struct {
    SatallxPhyNet *DATAOUT;       /*!< Reference to DATAOUT net of PhyIntf */
    SatallxPhyNet *COMMA;         /*!< Reference to COMMA net of PhyIntf */
} SatallxTxPhyIntf;

/*!
 * \brief Receive Logic Physical Interface
 * 
 * This structure holds the reference to actual physical nets that are
 * encapsulated by SatallxPhyIntf for SATA LL Rx - PHY interface.
 */
typedef struct {
    SatallxPhyNet *DATAIN;        /*!< Reference to DATAIN net of PhyIntf */
} SatallxRxPhyIntf;

/*
 * Phy Net Routines
 */

#ifndef NOCARBON
/*!
 * \brief Creates SATA LL Phy Interface Net
 *
 * This routine creates the physical interface net for the Xtor.
 * \param xtor Parent transactor reference
 * \param name Net name string
 * \param isOutput Direction of the net in point of view of the Xtor
 * \param isScalar Denotes whether the net is Scalar
 * \param initValue Initial value on the net
 * \returns Pointer to the created phy net
 */
SatallxPhyNet *satallxCreatePhyNet(SatallXtor *xtor, SatallxString name,
        SatallxBool isOutput, SatallxBool isScalar, SatallxDword initValue);

/*!
 * \brief Destroys a Phy Interface Net
 *
 * This routine destroys a physical net referred by the argument.
 * \param sig Reference to the phy net to be destroyed
 */
void satallxDestroyPhyNet(SatallxPhyNet *sig);

/*!
 * \brief Connects Phy Net to Carbon Model net
 *
 * Routine to connect a PhyNet referred by sig with the Carbon Model module
 * in Carbon Model connection mode.
 * \returns SatallxTrue value on success
 */
SatallxBool satallxConnectPhyNetToVhm(SatallxPhyNet *sig,
        CarbonObjectID *module, CarbonNetID *net);

/*!
 * \brief Connects Phy net via callbacks
 *
 * Routine to connect a phy net to the model in callback connection mode.
 * \param sig Phy net reference
 * \param changeCB Callback connectivity
 * \param cbData Callback data
 * \returns SatallxTrue on success
 */
SatallxBool satallxConnectPhyNetToModelCallback(SatallxPhyNet *sig,
        CarbonXSignalChangeCB changeCB, void *cbData);

/*!
 * \brief Updates Phy Interface Net during refresh cycle
 * \param net Phy net reference
 * \param currTime Current simulation time
 * \returns SatallxTrue on success, else SatallxFalse
 */
SatallxBool satallxUpdatePhyNet(SatallxPhyNet *net, CarbonUInt64 currTime);
#endif

/*!
 * \brief Deposit value to Carbon Model scalar signal
 * \param sig Signal to be written
 * \param value Value to be written to the signal
 */
void satallxDepositSignal(SatallxPhyNet *sig, SatallxBit value);

/*!
 * \brief Examine value to Carbon Model scalar signal
 * \param sig Signal to be read
 * \returns Value read from the signal
 */
SatallxBit satallxExamineSignal(SatallxPhyNet *sig);

/*!
 * \brief Deposit value to Carbon Model 10 bit data
 * \param sig Signal to be written
 * \param value Value to be written to the signal
 */
void satallxDepositData10b(SatallxPhyNet *sig, SatallxData10b value);

/*!
 * \brief Examine value to Carbon Model 10 bit data
 * \param sig Signal to be read
 * \returns Value read from the signal
 */
SatallxData10b satallxExamineData10b(SatallxPhyNet *sig);

/*
 * Interface Routines
 */

#ifndef NOCARBON
/*!
 * \brief Creates PHY interface for the transactor and contained nets
 * \param xtor Transactor
 * \returns PHY interface created
 */
SatallxPhyIntf *satallxCreatePhyIntf(SatallXtor *xtor);

/*!
 * \brief Destroys the PHY interface
 * \param phyIntf Interface to be destroyed
 */
void satallxDestroyPhyIntf(SatallxPhyIntf *phyIntf);

/*!
 * \brief Creates FSM to TX Unit interface
 * \returns Interface created
 */
SatallxFsmTxIntf *satallxCreateFsmTxIntf();

/*!
 * \brief Destroy the FSM to TX Unit interface
 * \param fsmTxIntf Interface to be destroyed
 */
void satallxDestroyFsmTxIntf(SatallxFsmTxIntf *fsmTxIntf);

/*!
 * \brief Creates RX Unit to FSM interface
 * \returns Interface created
 */
SatallxFsmRxIntf *satallxCreateFsmRxIntf();

/*!
 * \brief Destroy the RX Unit to FSM interface
 * \param fsmRxIntf Interface to be destroyed
 */
void satallxDestroyFsmRxIntf(SatallxFsmRxIntf *fsmRxIntf);

/*!
 * \brief Creates FSM to PHY interface
 * \param phyIntf PHY interface of the transactor, from which the net
 *                references will be taken
 * \returns Interface created
 */
SatallxFsmPhyIntf *satallxCreateFsmPhyIntf(SatallxPhyIntf *phyIntf);

/*!
 * \brief Destroy the FSM to PHY interface
 * \param phyIntf Interface to be destroyed
 */
void satallxDestroyFsmPhyIntf(SatallxFsmPhyIntf *phyIntf);

/*!
 * \brief Creates TX Unit to PHY interface
 * \param phyIntf PHY interface of the transactor, from which the net
 *                references will be taken
 * \returns Interface created
 */
SatallxTxPhyIntf *satallxCreateTxPhyIntf(SatallxPhyIntf *phyIntf);

/*!
 * \brief Destroy the TX Unit to PHY interface
 * \param phyIntf Interface to be destroyed
 */
void satallxDestroyTxPhyIntf(SatallxTxPhyIntf *phyIntf);

/*!
 * \brief Creates RX Unit to PHY interface
 * \param phyIntf PHY interface of the transactor, from which the net
 *                references will be taken
 * \returns Interface created
 */
SatallxRxPhyIntf *satallxCreateRxPhyIntf(SatallxPhyIntf *phyIntf);

/*!
 * \brief Destroy the RX Unit to PHY interface
 * \param phyIntf Interface to be destroyed
 */
void satallxDestroyRxPhyIntf(SatallxRxPhyIntf *phyIntf);
#endif

#endif
