// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXSataLLConfig_h_
#define __carbonXSataLLConfig_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \addtogroup SataLL
 * @{
 */

/*!
 * \file carbonXSataLLConfig.h
 * \brief Definition of SATA Configuration Object Routines.  
 *
 * This object is owned by the transactor, so there is no create or destroy
 * interface. User need to get the current config of the transactor and
 * set or unset different supports on that respectively.
 *
 * Configuration support manipulation allowed are:
 * - Power mode support
 * - Data scrambling support
 * - CONTp primitive generation support
 */

/*!
 * \brief Carbon SATA Link Layer Configuration Object forward declaration
 *
 * This type provides a structure containing three fields:
 * - PowerMode Support ON or OFF
 * - Data Scrambling Support ON or OFF
 * - Contp Generation Support ON or OFF
 */
typedef struct CarbonXSataLLConfigStruct CarbonXSataLLConfig;

/*!
 * \brief Creates SATA configuration object
 *
 * \returns Pointer to the created configuration object
 */
CarbonXSataLLConfig *carbonXSataLLConfigCreate(void);

/*!
 * \brief Destroys SATA configuration object
 *
 * \param config Pointer to the configuration object to be destroyed.
 *
 * \retval 1 For success.
 * \retval 0 For failure. 
 */
CarbonUInt32 carbonXSataLLConfigDestroy(CarbonXSataLLConfig *config);

/*!
 * \brief Sets SATA configuration object power mode support
 *
 * \param config Pointer to the configuration object.
 * \param isOn   1 for power mode support ON and 0 for 
 *               power mode support OFF.   
 */
void carbonXSataLLConfigSetPowerModeSupport(CarbonXSataLLConfig *config,
        CarbonUInt32 isOn);

/*!
 * \brief Get SATA configuration object power mode support
 *
 * This API will check for power mode support of the given 
 * configuration object.
 *
 * \param config Pointer to the configuration object.
 *
 * \retval 1 For power mode support ON.
 * \retval 0 For power mode support OFF.
 */
CarbonUInt32 carbonXSataLLConfigGetPowerModeSupport(
        const CarbonXSataLLConfig *config);

/*!
 * \brief Sets SATA configuration object data scrambling support
 *
 * This API will set desired data scrambling support to the given 
 * configuration object.
 *
 * \param config Pointer to the configuration object.
 * \param isOn   1 for scrambling support ON and 0 for
 *               scrambling support OFF.   
 */
void carbonXSataLLConfigSetScramblingSupport(CarbonXSataLLConfig *config,
        CarbonUInt32 isOn);

/*!
 * \brief Get SATA configuration object data scrambling support
 *
 * This API will check for data scrambling support of the given 
 * configuration object.
 *
 * \param config Pointer to the configuration object.
 *
 * \retval 1 For data scrambling support ON.
 * \retval 0 For data scrambling support OFF.
 */
CarbonUInt32 carbonXSataLLConfigGetScramblingSupport(
        const CarbonXSataLLConfig *config);

/*!
 * \brief Sets SATA configuration object CONTp generation support
 *
 * This API will set desired CONTp generation support to the given 
 * configuration object.
 *
 * \param config Pointer to the configuration object.
 * \param isOn   1 for CONTp generation support ON and 0 for  CONTp 
 *               generation supportOFF. 
 * 
 */
void carbonXSataLLConfigSetContSupport(CarbonXSataLLConfig *config,
        CarbonUInt32 isOn);

/*!
 * \brief Get SATA configuration object CONTp generation support
 *
 * This API will check for CONTp generation support of the given 
 * configuration object.
 *
 * \param config Pointer to the configuration object.
 *
 * \retval 1 For CONTp generation support ON.
 * \retval 0 For CONTp generation support OFF.
 */
CarbonUInt32 carbonXSataLLConfigGetContSupport(
        const CarbonXSataLLConfig *config);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
