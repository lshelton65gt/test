// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXSataTLTrans_h_
#define __carbonXSataTLTrans_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif
#include "carbon/c_memmanager.h"
#include "xactors/sata/carbonXSataLLTrans.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \addtogroup SataLL
 * @{
 */

/*!
 * \file carbonXSataTLTrans.h
 * \brief Definition of Carbon SATA Transport Layer Transaction Object.
 *
 * This file provides definition of transaction object for transport layer.
 */

/*!
 * \brief Carbon SATA Transport Layer Transaction (FIS) Type
 */
typedef enum {
    CarbonXSataTLTrans_InvalidFis = 0x0,      /*!< Invalid FIS */
    CarbonXSataTLTrans_RegHost2DevFis = 0x27, /*!< Register FIS -
                                                   Host to Device */
    CarbonXSataTLTrans_RegDev2HostFis = 0x34, /*!< Register FIS -
                                                   Device to Host */
    CarbonXSataTLTrans_SetDevBitsFis = 0xA1,  /*!< Set Device Bits FIS -
                                                   Device to Host */
    CarbonXSataTLTrans_DmaActFis = 0x39,      /*!< DMA Activate FIS -
                                                   Device to Host */
    CarbonXSataTLTrans_DmaSetupFis = 0x41,    /*!< DMA Setup FIS -
                                                   Bidirectional */
    CarbonXSataTLTrans_PioSetupFis = 0x5F,    /*!< PIO Setup FIS -
                                                   Device to Host */
    CarbonXSataTLTrans_DataFis = 0x46,        /*!< Data FIS -
                                                   Bidirectional */
    CarbonXSataTLTrans_BistActFis = 0x58,     /*!< BIST Activate FIS -
                                                   Bidirectional */
    CarbonXSataTLTrans_ReservedFis = 0xA6,    /*!< Reserved FIS */
                                                  /* A6,B8,BF,D9 */
    CarbonXSataTLTrans_VendorSpecFis = 0xC7   /*!< Vendor Specific FIS */
                                                  /* C7,D4 */
} CarbonXSataTLTransType;

/*!
 * \brief Gets the FIS type from a link layer frame transaction
 *
 * \param transLL Pointer to link layer transaction object.
 *
 * \returns FIS type. \a CarbonXSataTLTrans_InvalidFis will be returned for
 *          frame which can't be mapped to a valid FIS type.
 */
CarbonXSataTLTransType carbonXSataTLTransGetFisType(
        const CarbonXSataLLTrans *transLL);

/*!
 * \brief Register FIS - Host to Device structure
 *
 * This provides definition of transport layer transaction corresponding to
 * \a CarbonXSataTLTrans_RegHost2DevFis type, i.e. Register FIS - Host to
 * Device. This transaction can be generated from host only to transfer the
 * contents of the Shadow Register Block from the host to the device.
 */
typedef struct { 
    CarbonUInt1 C;              /*!< Set when transaction is due to Command
                                     Register updation and cleared when due to
                                     Device Control Register updation */
    CarbonUInt8 Command;        /*!< Command Register of Shadow Register */
    CarbonUInt8 Control;        /*!< Device Control Register */
    CarbonUInt8 LbaLow;         /*!< LBA Low Register */
    CarbonUInt8 LbaLowExp;      /*!< LBA Low Register (expanded address) */
    CarbonUInt8 LbaMid;         /*!< LBA Mid Register */
    CarbonUInt8 LbaMidExp;      /*!< LBA Mid Register (expanded address) */
    CarbonUInt8 LbaHigh;        /*!< LBA High Register */
    CarbonUInt8 LbaHighExp;     /*!< LBA High Register (expanded address) */
    CarbonUInt8 Device;         /*!< Device Register of Shadow Register Block */
    CarbonUInt8 Features;       /*!< Features Register */
    CarbonUInt8 FeaturesExp;    /*!< Features Register (expanded address) */
    CarbonUInt4 PmPort;         /*!< PM Port */
    CarbonUInt8 SectorCount;    /*!< Sector Count Register */
    CarbonUInt8 SectorCountExp; /*!< Sector Count Register (expanded address) */
} CarbonXSataTLTransRegHost2DevFis;
 
/*! 
 * \brief Creates Link Layer Frame transaction from Register FIS -
 * Host to Device
 *
 * This routine can be called by the user or test layer to create a link
 * layer frame transaction from FIS. Thus memory ownership of the link layer
 * frame goes to the user and after transaction is complete,
 * carbonXSataLLTransDestroy needs to be called to destroy the link layer frame.
 *
 * \param transTL Register FIS - Host to Device
 *
 * \returns Link Layer Frame transaction
 */
CarbonXSataLLTrans *carbonXSataTLTransCreateRegHost2DevFisLLFrame(
        const CarbonXSataTLTransRegHost2DevFis *transTL);

/*!
 * \brief Gets Register FIS - Host to Device from Link Layer Frame transaction
 *
 * This routine can be called by the user or test layer to retrieve transport
 * layer FIS from received link layer frame transaction. Memory ownership of
 * this transport layer FIS is on user and user need to call carbonmem_free to
 * deallocate the memory consumed by FIS.
 *
 * \param transLL Link Layer Frame transaction
 *
 * \returns Pointer to Register FIS - Host to Device. NULL will be returned if
 *          this routine is called on a link layer frame which doesn't
 *          corresponds to this FIS type.
 */
CarbonXSataTLTransRegHost2DevFis *carbonXSataTLTransGetRegHost2DevFis(
        const CarbonXSataLLTrans *transLL);

/*!
 * \brief Register FIS - Device to Host structure
 *
 * This provides definition of transport layer transaction corresponding to
 * \a CarbonXSataTLTrans_RegDev2HostFis type, i.e. Register FIS - Device to
 * Host. This transaction can be generated from device only to transfer the
 * contents of the Shadow Register Block from the device to the host.
 */
typedef struct {
    CarbonUInt1 I;              /*!< Interrupt bit. This bit reflects the 
                                     interrupt bit line of the device. */
    CarbonUInt8 LbaLow;         /*!< LBA Low Register */
    CarbonUInt8 LbaLowExp;      /*!< LBA Low Register (expanded address) */
    CarbonUInt8 LbaMid;         /*!< LBA Mid Register */
    CarbonUInt8 LbaMidExp;      /*!< LBA Mid Register (expanded address) */
    CarbonUInt8 LbaHigh;        /*!< LBA High Register */
    CarbonUInt8 LbaHighExp;     /*!< LBA High Register (expanded address) */
    CarbonUInt8 Device;         /*!< Device Register of Shadow Register Block */
    CarbonUInt4 PmPort;         /*!< PM Port */
    CarbonUInt8 SectorCount;    /*!< Sector Count Register */
    CarbonUInt8 SectorCountExp; /*!< Sector Count Register (expanded address)*/
    CarbonUInt8 Error;          /*!< Error Register of Shadow Register Block */
    CarbonUInt8 Status;         /*!< Status Register of Shadow Register Block*/
} CarbonXSataTLTransRegDev2HostFis;
    
/*!
 * \brief Creates Link Layer Frame transaction from Register FIS -
 * Device to Host
 *
 * This routine can be called by the user or test layer to create a link
 * layer frame transaction from FIS. Thus memory ownership of the link layer
 * frame goes to the user and after transaction is complete,
 * carbonXSataLLTransDestroy needs to be called to destroy the link layer frame.
 *
 * \param transTL Ponter to the Register FIS - Device to Host
 *
 * \returns Pointer to Link Layer Frame transaction
 */
CarbonXSataLLTrans *carbonXSataTLTransCreateRegDev2HostFisLLFrame(
        const CarbonXSataTLTransRegDev2HostFis *transTL);
                                                                                
/*!
 * \brief Gets Register FIS - Device to Host from Link Layer Frame transaction
 *
 * This routine can be called by the user or test layer to retrieve transport
 * layer FIS from received link layer frame transaction. Memory ownership of
 * this transport layer FIS is on user and user need to call carbonmem_free to
 * deallocate the memory consumed by FIS.
 *    
 * \param transLL Link Layer Frame transaction 
 *
 * \returns Pointer to Register FIS - Device to Host. NULL will be returned if
 *             this routine is called on a link layer frame which doesn't
 *             corresponds to this FIS type.
 *   
 */
CarbonXSataTLTransRegDev2HostFis *carbonXSataTLTransGetRegDev2HostFis(
        const CarbonXSataLLTrans *transLL);

/*!
 * \brief Set Device Bits FIS - Device to Host structure
 *
 * This provides definition of transport layer transaction corresponding to
 * \a CarbonXSataTLTrans_SetDevBitsFis type, i.e. Set Device Bits FIS- Device to
 * Host. This transaction can be generated from device only to load Shadow 
 * Register Block bits for which the device has exclusive write access.
 */
typedef struct {
    CarbonUInt1 I;              /*!< Interrupt bit. This bit signals the host 
                                     adapter to enter an interrupt pending 
                                     state. */
    CarbonUInt1 N;              /*!< Notification bit. This bit signals the 
                                     host that the device needs attention. */
    CarbonUInt4 PmPort;         /*!< PM Port */
    CarbonUInt8 Error;          /*!< Error Register of Shadow Register Block */
    CarbonUInt8 StatusHi;       /*!< Contains the value of bits 6, 5 and 4 of 
                                     Status Register of Shadow Register Block*/
    CarbonUInt8 StatusLo;       /*!< Contains the value of bits 2, 1 and 0 of 
                                     Status Register of Shadow Register Block*/
} CarbonXSataTLTransSetDevBitsFis;


/*!
 * \brief Creates Link Layer Frame transaction from Set Device Bits FIS -
 * Device to Host
 *
 * This routine can be called by the user or test layer to create a link
 * layer frame transaction from FIS. Thus memory ownership of the link layer
 * frame goes to the user and after transaction is complete,
 * carbonXSataLLTransDestroy needs to be called to destroy the link layer frame.
 *      
 * \param transTL Ponter to the Set Device Bits FIS - Device to Host
 *
 * \returns Link Layer Frame transaction
 */
CarbonXSataLLTrans *carbonXSataTLTransCreateSetDevBitsFisLLFrame(
        const CarbonXSataTLTransSetDevBitsFis *transTL);
                                                                                
/*!
 * \brief Gets Set Device Bits FIS - Device to Host from Link Layer Frame 
 * transaction
 *
 * This routine can be called by the user or test layer to retrieve transport
 * layer FIS from received link layer frame transaction. Memory ownership of
 * this transport layer FIS is on user and user need to call carbonmem_free to
 * deallocate the memory consumed by FIS.
 *      
 * \param transLL Poniter to Link Layer Frame transaction
 *
 * \returns Pointer to Set Device Bits FIS - Device to Host.NULL will be 
 *                 returned if this routine is called on a link layer frame 
 *                 which doesn't corresponds to this FIS type.
 *   
 */
CarbonXSataTLTransSetDevBitsFis *carbonXSataTLTransGetSetDevBitsFis(
        const CarbonXSataLLTrans *transLL);

/*!
 * \brief DMA Activate FIS - Device to Host structure
 *
 * This provides definition of transport layer transaction corresponding to
 * \a CarbonXSataTLTrans_DmaActFis type, i.e. DMA Activate FIS- Device to
 * Host. This transaction can be generated from device only to signal the host 
 * to proceed with a DMA transfer of data from the host to the device.
 */
typedef struct {
    
    CarbonUInt4 PmPort;         /*!< PM Port */

} CarbonXSataTLTransDmaActFis;

/*!
 * \brief Creates Link Layer Frame transaction from DMA Activate FIS -
 * Device to Host
 *
 * This routine can be called by the user or test layer to create a link
 * layer frame transaction from FIS. Thus memory ownership of the link layer
 * frame goes to the user and after transaction is complete,
 * carbonXSataLLTransDestroy needs to be called to destroy the link layer frame.
 *      
 * \param transTL Ponter to the DMA Activate FIS - Device to Host
 *
 * \returns Link Layer Frame transaction
 */
CarbonXSataLLTrans *carbonXSataTLTransCreateDmaActFisLLFrame(
        const CarbonXSataTLTransDmaActFis *transTL);
                                                                                
/*!
 * \brief Gets DMA Activate FIS - Device to Host from Link Layer Frame 
 * transaction
 *
 * This routine can be called by the user or test layer to retrieve transport
 * layer FIS from received link layer frame transaction. Memory ownership of
 * this transport layer FIS is on user and user need to call carbonmem_free to
 * deallocate the memory consumed by FIS.
 * 
 * \param transLL Poniter to Link Layer Frame transaction
 *
 * \returns Pointer to DMA Activate FIS - Device to Host
 */
CarbonXSataTLTransDmaActFis *carbonXSataTLTransGetDmaActFis(
        const CarbonXSataLLTrans *transLL);

/*!
 * \brief DMA Setup FIS - Bidirectional structure
 *
 * This provides definition of transport layer transaction corresponding to
 * \a CarbonXSataTLTrans_DmaSetupFis type, i.e. DMA Setup FIS- Bidirectional.
 * This transaction can be generated from host or device to initiate DMA access
 * to host memory. This FIS is used to request the host or device to program 
 * its DMA controller before transferring data. 
 */
typedef struct {
    CarbonUInt1 I;              /*!< Interrupt bit. If this bit is set to one
                                     an interrupt pending shall be generated 
                                     when the DMA transfer count is exhausted*/ 
    CarbonUInt1 A;              /*!< Auto-Activate - If cleared to zero, a DMA
                                     Activate FIS is required to trigger the 
                                     transmission of the first data FIS from 
                                     the host when the data transfer direction
                                     is Host-to-Device. */
    CarbonUInt1 D;              /*!< Direction - 1 for transmitter to receiver
                                     and 0 for receiver to transmitter */
    CarbonUInt4 PmPort;         /*!< PM Port */
    CarbonUInt32 DmaBufIdLow;   /*!< This field is used to identify a DMA 
                                     buffer region in host memory */
    CarbonUInt32 DmaBufIdHigh;  /*!< This field is used to identify a DMA 
                                     buffer region in host memory */
    CarbonUInt32 DmaBufOffset;  /*!< This is the byte offset into the buffer.
                                     Bits [1:0] shall be zero. */
    CarbonUInt32 DmaTransCount; /*!< Number of bytes to be read or written*/

} CarbonXSataTLTransDmaSetupFis;

/*!
 * \brief Creates Link Layer Frame transaction from DMA Setup FIS -
 * Bidirectional
 * 
 * This routine can be called by the user or test layer to create a link
 * layer frame transaction from FIS. Thus memory ownership of the link layer
 * frame goes to the user and after transaction is complete,
 * carbonXSataLLTransDestroy needs to be called to destroy the link layer frame.
 *      
 * \param transTL Ponter to the DMA Setup FIS - Bidirectional
 *
 * \returns Pointer to Link Layer Frame transaction
 */
CarbonXSataLLTrans *carbonXSataTLTransCreateDmaDmaSetupFisLLFrame(
        const CarbonXSataTLTransDmaSetupFis *transTL);
                                                                                
/*!
 * \brief Gets DMA Setup FIS - Bidirectional from Link Layer Frame 
 * transaction
 * 
 * This routine can be called by the user or test layer to retrieve transport
 * layer FIS from received link layer frame transaction. Memory ownership of
 * this transport layer FIS is on user and user need to call carbonmem_free to
 * deallocate the memory consumed by FIS.
 * 
 * \param transLL Poniter to Link Layer Frame transaction
 *
 * \returns Pointer to DMA Setup FIS - Bidirectional
 */
CarbonXSataTLTransDmaSetupFis *carbonXSataTLTransGetDmaSetupFis(
        const CarbonXSataLLTrans *transLL);

/*!
 * \brief PIO Setup FIS - Device to Host structure
 *
 * This provides definition of transport layer transaction corresponding to
 * \a CarbonXSataTLTrans_PioSetupFis type, i.e. PIO Setup FIS - Device to
 * Host. This transaction can be generated from device only to provide the
 * the host adapter with sufficient information regarding a PIO data phase 
 * to allow the host adapter to efficiently handle PIO data transfer.  
 */
typedef struct {
    CarbonUInt1 I;              /*!< Interrupt bit. This bit reflects the 
                                     interrupt bit line of the device. */
    CarbonUInt1 D;              /*!< Indicates the data transfer direction
                                     1 for device to host and 0 for host to
                                     device */
    CarbonUInt8 LbaLow;         /*!< LBA Low Register of Command Block */
    CarbonUInt8 LbaLowExp;      /*!< LBA Low Register (expanded address) of
                                     Shadow Register Block */
    CarbonUInt8 LbaMid;         /*!< LBA Mid Register of Command Block*/
    CarbonUInt8 LbaMidExp;      /*!< LBA Mid Register (expanded address) of
                                     Shadow Register Block*/
    CarbonUInt8 LbaHigh;        /*!< LBA High Register Command Block */
    CarbonUInt8 LbaHighExp;     /*!< LBA High Register (expanded address) of
                                     Shadow Register Block  */
    CarbonUInt8 Device;         /*!< Device Register of Command Block */
    CarbonUInt4 PmPort;         /*!< PM Port */
    CarbonUInt8 SectorCount;    /*!< Sector Count Register of Command Block*/
    CarbonUInt8 SectorCountExp; /*!< Sector Count Register (expanded address)*/
    CarbonUInt8 Error;          /*!< Error Register of Command Block */
    CarbonUInt8 Status;         /*!< Status Register of Command Block */
    CarbonUInt8 E_Status;       /*!< Contains the new value of the Status 
                                     register of the Command Block at the 
                                     conclusion of the subsequent dataFis */
    CarbonUInt16 TransCount;    /*!< Number of bytes to be transfer in the
                                     subsequent Data FIS. */
} CarbonXSataTLTransPioSetupFis;

/*!
 * \brief Creates Link Layer Frame transaction from PIO Setup FIS -
 * Device to Host
 *
 * This routine can be called by the user or test layer to create a link
 * layer frame transaction from FIS. Thus memory ownership of the link layer
 * frame goes to the user and after transaction is complete,
 * carbonXSataLLTransDestroy needs to be called to destroy the link layer frame.
 *      
 * \param transTL Ponter to the PIO Setup FIS - Device to Host
 * 
 * \returns Pointer to Link Layer Frame transaction
 */
CarbonXSataLLTrans *carbonXSataTLTransCreatePioSetupFisLLFrame(
        const CarbonXSataTLTransPioSetupFis *transTL);
                                                                                
/*!
 * \brief Gets PIO Setup FIS - Device to Host from Link Layer Frame 
 * transaction
 *  
 * This routine can be called by the user or test layer to retrieve transport
 * layer FIS from received link layer frame transaction. Memory ownership of
 * this transport layer FIS is on user and user need to call carbonmem_free to
 * deallocate the memory consumed by FIS.
 *     
 * \param transLL Link Layer Frame transaction
 * 
 * \returns Pointer to PIO Setup FIS - Host to Device. NULL will be returned if
 *  this routine is called on a link layer frame which doesn't
 *  corresponds to this FIS type.
 *
 */
CarbonXSataTLTransPioSetupFis *carbonXSataTLTransGetPioSetupFis(
        const CarbonXSataLLTrans *transLL);

/*!
 * \brief Data FIS (Bidirectional) structure
 *
 * This provides definition of transport layer transaction corresponding to
 * \a CarbonXSataTLTrans_DataFis type, i.e. Data FIS. This transaction can
 * be generated from host as well as device to transmit/receive data.
 */
typedef struct { 
    CarbonUInt32 dataSize; /*!< Data size, ranges from 1 to 2048 */
    CarbonUInt32 *data;    /*!< Data as array of DWORDS of size equals to
                                \a dataSize */
    CarbonUInt4 PmPort;    /*!< PM (Port Multiplier) Port address */
} CarbonXSataTLTransDataFis;
 
/*!
 * \brief Creates Link Layer Frame transaction from transport layer Data FIS
 *
 * This routine can be called by the user or test layer to create a link
 * layer frame transaction from FIS. Thus memory ownership of the link layer
 * frame goes to the user and after transaction is complete,
 * carbonXSataLLTransDestroy needs to be called to destroy the link layer frame.
 *      
 * \param transTL Data FIS
 *
 * \returns Link Layer Frame transaction
 */
CarbonXSataLLTrans *carbonXSataTLTransCreateDataFisLLFrame(
        const CarbonXSataTLTransDataFis *transTL);

/*!
 * \brief Gets Data FIS from Link Layer Frame transaction
 *
 * This routine can be called by the user or test layer to retrieve transport
 * layer FIS from received link layer frame transaction. Memory ownership of
 * this transport layer FIS is on user and user need to call carbonmem_free to
 * deallocate the memory consumed by FIS.
 *     
 * \param transLL Link Layer Frame transaction
 *
 * \returns Pointer to Data FIS - Bidirectional. NULL will be returned if
 *          this routine is called on a link layer frame which doesn't
 *          corresponds to this FIS type.
 *   
 */
CarbonXSataTLTransDataFis *carbonXSataTLTransGetDataFis(
        const CarbonXSataLLTrans *transLL);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
