// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbonXSataLL_h_
#define __carbonXSataLL_h_

#ifndef __carbonXSataLLConfig_h_
#include "xactors/sata/carbonXSataLLConfig.h"
#endif

#ifndef __carbonXSataLLTrans_h_
#include "xactors/sata/carbonXSataLLTrans.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \defgroup SataLL SATA Link Layer C-Interface
 * \brief C-Linkable Interface to the SATA Link Layer Transactor
 */

/*!
 * \addtogroup SataLL
 * @{
 */

/*!
 * \file carbonXSataLL.h
 * \brief Definition of SATA Link Layer Transactor Object
 *
 * This file provides definition of SATA Link Layer transactor object and 
 * related APIs.
 *
 * Transactor update semantics: Following sequence of operation will be
 * followed to run the thransactor for each clock cycle
 *     -# Evaluate all transactors and the model. This should call
 *        carbonXSataLLEvaluate().
 *     -# Call carbonXSataLLRefreshInputs() and carbonXSataLLRefreshOutputs()
 *        for every transactor to update the input and output connections
 *        between the model and the transactors.
 */

struct CarbonXSataLLStruct;
/*!
 * \brief Carbon SATA Link Layer Transactor Object
 *
 * This type provides a handle or pointer to transactor object. All transactor
 * APIs need this handle to be passed as its argument.
 */
typedef struct CarbonXSataLLStruct* CarbonXSataLLID;

/*!
 * \brief Carbon SATA Link Layer transactor type
 *
 * This type specifies the transctor type i.e. device or host.
 */
typedef enum {
    CarbonXSataLL_Device = 0,               /*!< Device transactor */
    CarbonXSataLL_Host = 1                  /*!< Host transactor */
} CarbonXSataLLType;

/*!
 * \brief Carbon SATA Link Layer transactor error notification type
 *
 * This type provides the error type that can be signaled by the transactor
 * for any unmanagable case, that it might see.
 */
typedef enum {
    CarbonXSataLLError_InternalError = 1,   /*!< Internal error in code */
    CarbonXSataLLError_NoMemory,            /*!< Memory allocation error */
    CarbonXSataLLError_FsmInvalidState,     /*!< FSM reached to invalid state */
    CarbonXSataLLError_FsmInvalidTransition /*!< FSM is in invalid transition */
} CarbonXSataLLErrorType;

/*!
 * \brief Creates a transactor Transactor Object
 *
 * \param transactorName     Instance name of this transactor object.
 *
 * \param xtorType           Specifies transactor type i.e. host or device.
 *
 * \param maxTransfers       Maximum number of transaction transfers that can be
 *                           queued in the transaction buffer.
 *
 * \param reportTransaction  Callback function to be called when a transaction
 *                           is complete. This is relevant only for the
 *                           intiatior of the transaction.
 * \param setDefaultResponse Callback function used by transactors to set
 *                           the default conditions of a transaction. This is
 *                           called on the receiving side as soon as the
 *                           transaction is started, for example used to
 *                           specify the DMAT.
 *
 * \param setResponse        Callback function used by transactors to set the
 *                           response to be provided to a transction. This is
 *                           called on the receiving side once the full
 *                           transaction has been received.
 *
 * \param refreshResponse    This callback can be used to revise return values
 *                           or latency. It is not presently used in the SATA
 *                           LL transactors.
 *
 * \param notify             Callback function used to report a transactor
 *                           fault that is associated with a particular
 *                           transction.
 *
 * \param notify2            Callback function used to report a transactor
 *                           fault that cannot be associated with a
 *                           particular transction.
 *
 * \param stimulusInstance   Provided as a convenience to the user. It is
 *                           stored as a void pointer by the transactor
 *                           and returned as a parameter of the callback
 *                           functions to help identify the generator
 *                           instance that should service the callback
 *                           where more than one intrerface is in use.
 *
 * \returns Created transactor handle
 */
CarbonXSataLLID carbonXSataLLCreate(const char* transactorName,
        CarbonXSataLLType xtorType, CarbonUInt32 maxTransfers,
        void (*reportTransaction)(CarbonXSataLLTrans*, void*),
        void (*setDefaultResponse)(CarbonXSataLLTrans*, void*),
        void (*setResponse)(CarbonXSataLLTrans*, void*),
        void (*refreshResponse)(CarbonXSataLLTrans*, void*),
        void (*notify)(CarbonXSataLLErrorType, CarbonXSataLLTrans*, void*),
        void (*notify2)(CarbonXSataLLErrorType, void*),
        void *stimulusInstance);

/*!
 * \brief Destroys a transactor object
 *
 * \param xtor Transactor object.
 *
 * \retval 1 For successful destruction.
 * \retval 0 For failure in destruction.
 */
CarbonUInt32 carbonXSataLLDestroy(CarbonXSataLLID xtor);

/*!
 * \brief Gets transactor name
 *
 * \param xtor Transactor object.
 *
 * \returns Name of the transactor
 */
const char *carbonXSataLLGetName(CarbonXSataLLID xtor);

/*!
 * \brief Gets transactor type i.e. device or host
 *
 * \param xtor Transactor object.
 *
 * \returns Type of the transactor
 */
CarbonXSataLLType carbonXSataLLGetType(CarbonXSataLLID xtor);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by name
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName   Carbon Model name.
 * \param carbonModelHandle Carbon Model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXSataLLConnectByModelSignalName(CarbonXSataLLID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by
 *        CarbonNetID
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName   Carbon Model name.
 * \param carbonModelHandle Carbon Model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXSataLLConnectByModelSignalHandle(
        CarbonXSataLLID xtor,
        CarbonXInterconnectNameHandlePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon Model signals by
 *        input/output change callbacks
 *
 * \param xtor     Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXSataLLConnectToLocalSignalMethods(
        CarbonXSataLLID xtor,
        CarbonXInterconnectNameCallbackTrio *nameList);

/*!
 * \brief Checks if transactor can accept additional transactions.
 * 
 * This API will check if more transactions can be pushed into the
 * transaction queue.
 *
 * \param xtor Transactor object.
 *
 * \retval 1 If transactor can accept additional transactions.
 * \retval 0 Otherwise.
*/
CarbonUInt32 carbonXSataLLIsSpaceInTransactionBuffer(
        CarbonXSataLLID xtor);

/*!
 * \brief Starts a new transaction
 *
 * This API will push a created transaction into transaction queue of
 * the transactor.
 *
 * \param xtor Transactor object.
 * \param trans Transaction to be pushed in the transaction queue.
 *
 * \retval 1 If the transaction is successfully added to the input buffer.
 * \retval 0 Otherwise.
 */
CarbonUInt32 carbonXSataLLStartNewTransaction(CarbonXSataLLID xtor,
        CarbonXSataLLTrans *trans);

/*!
 * \brief Evaluates transactor
 * 
 * This API evaluates the transactor and updates all the functionality
 * for a clock. Note, in this API call transactor state is updated
 * but i/o of the transactor are not changed.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which evaluation is happening.
 *
 * \retval 1 For successful evaluation.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSataLLEvaluate(CarbonXSataLLID xtor,
        CarbonUInt64 currentTime);

/*!
 * \brief Refreshes transactor input signals
 *
 * In this API, all input signal ports of the transactor
 * are updated but transactor state remains unchanged. Thus at this point
 * transactor will communicate with the model for reading signal values.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which refreshing is happening.
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSataLLRefreshInputs(CarbonXSataLLID xtor,
        CarbonUInt64 currentTime);

/*!
 * \brief Refreshes transactor output signals
 *
 * In this API, all output signal ports of the transactor
 * are updated but transactor state remains unchanged. Thus at this point
 * transactor will communicate with the model for writing signal values.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which refreshing is happening.
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSataLLRefreshOutputs(CarbonXSataLLID xtor,
        CarbonUInt64 currentTime);


/*!
 * \brief Sets the configuration object of the transactor
 *
 * This API will copy the configuration parameters from configuration object
 * to the transactor.
 *
 * \param xtor Transactor object.
 * \param config Configuration object.
 */
void carbonXSataLLSetConfig(CarbonXSataLLID xtor,
        const CarbonXSataLLConfig *config);

/*!
 * \brief Gets the configuration object of the transactor
 *
 * This API will copy the configuration parameters from transactor to the
 * configuration object passed.
 *
 * \param xtor Transactor object.
 * \param config Configuration object to be updated.
 */
void carbonXSataLLGetConfig(CarbonXSataLLID xtor, CarbonXSataLLConfig *config);

/*!
 * \brief Dumps the state of transactor
 *
 * This routine dumps the transactor's internal state in console.
 * One can use this routine for debug purpose.
 *
 * \param xtor Transactor object.
 */
void carbonXSataLLDump(CarbonXSataLLID xtor);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
