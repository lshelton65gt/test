#ifndef SATALLX_QUEUE_H
#define SATALLX_QUEUE_H

#include "sataLlXUtil.h"

/*!
 * \file sataLlXQueue.h
 *  
 * \brief Header file for the queue implementation of SATA LL transactor. 
 *  
 *  This file provides declaration of the queue operations.
 */


/*! 
 * \brief Queue node structure 
 */
typedef struct SatallxQueueItemStruct {
    void *data; /*!< Data item */
    struct SatallxQueueItemStruct *prev; /*!< Link to previous node */
    struct SatallxQueueItemStruct *next; /*!< Link to next node */
} SatallxQueueItem;

/*! 
 * \brief Queue structure implemented using doubly linked list 
 */
typedef struct {
    UInt count;                /*!< No of items in queue */
    UInt size;                 /*!< Max. no of items in queue */
    SatallxQueueItem *head;    /*!< Head of queue */
    SatallxQueueItem *tail;    /*!< Tail of queue */
} SatallxQueue;

/*! 
 * \brief Creates a queue 
 * 
 * It creates a queue of specified size.
 * \param size Size of the queue. 
 * \returns Pointer to the queue.
 */
SatallxQueue *satallxCreateQueue(UInt size);

/*! 
 * \brief Destroys the  queue
 *
 * This function destroys the queue.
 * \param queue Pointer to the queue
 */
void satallxDestroyQueue(SatallxQueue *queue);

/*! 
 * \brief Gets the size of the queue.
 *
 * This function returns the size of the queue.
 * \param queue Pointer to the queue
 * \returns The size of the queue.
 */
UInt satallxQueueGetSize(SatallxQueue *queue);

/*! 
 * \brief Sets the size of the queue to a new value.
 *
 * This function sets the size (Max. no of items in queue) 
 * of the queue specified as parameter.
 * \param queue Pointer to the queue
 * \param size Size of the queue.
 */
void satallxQueueSetSize(SatallxQueue *queue, UInt size);

/*! 
 * \brief Gets number of item in the queue.
 *
 * This function returns the number of items in the queue
 * \param queue Pointer to the queue
 * \returns The number of items in the queue
 */
UInt satallxQueueGetCount(SatallxQueue *queue);

/*! 
 * \brief Pushes a node in the queue
 *
 * This function pushes a node. Value of the node is specified 
 * as parameter, to the queue 
 * \param queue Pointer to the queue
 * \param data Data to be pushed
 * \returns True if the node is pushed successfuly, else False
 */
SatallxBool satallxQueuePush(SatallxQueue *queue, void *data);

/*! 
 * \brief Pops a item from the queue, poped item is returned
 *
 * This function pops a item from the head side of the queue and returns it 
 * \param queue Pointer to the queue
 */ 
void *satallxQueuePop(SatallxQueue *queue);

/*! 
 * \brief Gets the top data of queue without popping
 *
 * This function returns the top data of the queue without 
 * popping it from the queue.
 * \param queue Pointer to the queue
 */
void *satallxQueueTop(SatallxQueue *queue);

#endif
