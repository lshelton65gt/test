// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXSataLLTrans_h_
#define __carbonXSataLLTrans_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \addtogroup SataLL
 * @{
 */

/*!
 * \file carbonXSataLLTrans.h
 * \brief Definition of Carbon SATA Link Layer Transaction Object.
 *
 * This file provides definition of transaction object.
 */

/*!
 * \brief Carbon SATA Link Layer Transaction Object
 */
typedef struct CarbonXSataLLTransStruct CarbonXSataLLTrans;

/*!
 * \brief Transaction Types
 *
 * This specifies different types of transactions that can be created.
 */
typedef enum {
    CarbonXSataLLTrans_Reset,           /*!< Reset transaction */
    CarbonXSataLLTrans_Frame,           /*!< Frame transaction */
    CarbonXSataLLTrans_PowerDown,       /*!< Power down transaction */
    CarbonXSataLLTrans_PowerWakeUp,     /*!< Wake up from power down mode 
                                             transaction */
} CarbonXSataLLTransType;

/*!
 * \brief Transmit and Receive Frame Hold position object
 *
 * This structure specifies the frame hold that can be specified for
 * a frame transaction of type \a CarbonXSataLLTrans_Frame from transmit
 * as well as receive side.
 */
typedef struct {
    CarbonUInt32 pos;                   /*!< Hold position i.e. DWORD index
                                             before which HOLDp will be sent */
    CarbonUInt32 count;                 /*!< Hold count i.e. number of HOLDp
                                             to be sent at the hold position */
} CarbonXSataLLFrameHoldPos;

/*!
 * \brief Power Modes
 *
 * This type specifies different power down modes.
 */
typedef enum {
    CarbonXSataLLPowerMode_Partial = 1, /*!< Partial power mode */
    CarbonXSataLLPowerMode_Slumber      /*!< Slumber power mode */
} CarbonXSataLLPowerDownMode;

/*!
 * \brief Transaction Status
 *
 * Status \a CarbonXSataLLTransStatus_Ok is default and indicates success
 * for any transaction types.
 *
 * \a CarbonXSataLLTransStatus_SyncAbort,
 * \a CarbonXSataLLTransStatus_DmatAbort and
 * \a CarbonXSataLLTransStatus_FrameErr status are outcome of
 * \a CarbonXSataLLTrans_Frame transaction. These status can be checked
 * by both transmit and receive sides.
 *
 * \a CarbonXSataLLTransStatus_PmDeny
 * and \a CarbonXSataLLTransStatus_PmAbort are status for transaction type
 * \a CarbonXSataLLTrans_PowerDown and will be obtained by the initiator.
 */
typedef enum {
    CarbonXSataLLTransStatus_Ok,        /*!< Default status meaning
                                             successful transaction */
    CarbonXSataLLTransStatus_SyncAbort, /*!< Sync abort happened */
    CarbonXSataLLTransStatus_DmatAbort, /*!< DMA termination happened */
    CarbonXSataLLTransStatus_FrameErr,  /*!< Frame error happened, i.e.
                                             either CRC error or BAD FIS
                                             reported */
    CarbonXSataLLTransStatus_PmDeny,    /*!< Peer link layer doesn't support
                                             power mode */
    CarbonXSataLLTransStatus_PmAbort,   /*!< Power down request aborted by
                                             any other transaction */
    CarbonXSataLLTransStatus_Invalid    /*!< Invalid transaction error */
} CarbonXSataLLTransStatus;

/*!
 * \brief Create a transaction object
 *
 * \returns Pointer to the transaction object created.
 */
CarbonXSataLLTrans* carbonXSataLLTransCreate(void);

/*!
 * \brief Destroy a transaction object
 *
 * \param trans Pointer to transaction object.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSataLLTransDestroy(CarbonXSataLLTrans *trans);

/*!
 * \brief Sets transaction object Type
 *
 * \param trans Pointer to transaction object.
 * \param transType Transaction type.
 */
void carbonXSataLLTransSetType(CarbonXSataLLTrans *trans,
        CarbonXSataLLTransType transType);

/*!
 * \brief Gets transaction object Type
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Transaction type.
 */
CarbonXSataLLTransType carbonXSataLLTransGetType(
        const CarbonXSataLLTrans *trans);

/*!
 * \brief Sets transaction object Frame Data
 *
 * This routine sets the frame data for transaction of type
 * \a CarbonXSataLLTrans_Frame from transmit side.
 *
 * \param trans Pointer to transaction object.
 * \param frameSize Frame size in DWORDs.
 * \param data Data list as array of DWORDs of size \a frameSize.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSataLLTransSetFrameData(CarbonXSataLLTrans *trans,
        CarbonUInt32 frameSize, CarbonUInt32 *data);

/*!
 * \brief Gets transaction object Frame Size
 *
 * This routine gets the frame size in DWORDs for frame transaction
 * of type \a CarbonXSataLLTrans_Frame from receive side.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Frame size in DWORDs.
 */
CarbonUInt32 carbonXSataLLTransGetFrameSize(const CarbonXSataLLTrans *trans);

/*!
 * \brief Gets transaction object Frame Data
 *
 * This routine gets the frame data for frame transaction of type
 * \a CarbonXSataLLTrans_Frame from receive side.
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Frame data as array of DWORDs.
 *
 * \sa carbonXSataLLTransGetFrameSize
 */
CarbonUInt32* carbonXSataLLTransGetFrameData(const CarbonXSataLLTrans *trans);

/*!
 * \brief Sets SYNC Abort position for frame transaction from transmit
 * or receive sides.
 *
 * This routine sets the SYNC abort postion after which frame
 * transaction will be aborted with immediate SYNCp. The position indicates
 * the index (starting at one) of DWORD data before which SYNC abortion
 * will happen.
 *
 * \param trans Pointer to transaction object.
 * \param pos Position of data in DWORD before which SYNCp will be sent.
 */
void carbonXSataLLTransSetSyncAbortPos(CarbonXSataLLTrans *trans,
        CarbonUInt32 pos);

/*!
 * \brief Sets DMA Terminate position for receive frame transaction
 *
 * This routine sets the DMA terminate postion after which receive frame
 * transaction will be aborted with DMATp. The position indicates
 * the index (starting at one) of DWORD data before which DMATp primitive
 * will be sent.
 *
 * \param trans Pointer to transaction object.
 * \param pos Position of data in DWORD before which DMATp will be sent.
 */
void carbonXSataLLTransSetDmatPos(CarbonXSataLLTrans *trans, CarbonUInt32 pos);

/*!
 * \brief Sets hold positions for frame transaction from transmit
 * or receive sides.
 *
 * This routine sets the hold postions at which frame transaction
 * will be hold sending HOLDp primitive. This will also set
 * number of HOLDp to be sent at each hold position. The hold position is
 * the index (starting at zero) of DWORD data before which HOLDp will be 
 * sent. Hold position zero means transactor is not ready to receive data
 * even at the idle state. This can be done both from transmit or from
 * receive side.
 *
 * \param trans Pointer to transaction object.
 * \param posListSize Hold list size.
 * \param holdPosList Hold list.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXSataLLTransSetHoldPosList(CarbonXSataLLTrans *trans,
        CarbonUInt32 posListSize, CarbonXSataLLFrameHoldPos *holdPosList);

/*!
 * \brief Reports FIS status after complete receive frame transaction
 *
 * Default reporting is good FIS.
 *
 * \param trans Pointer to transaction object.
 * \param isGoodFis 1 indicates good and 0 indicates bad FIS.
 */
void carbonXSataLLTransSetFisStatus(CarbonXSataLLTrans *trans,
        CarbonUInt32 isGoodFis);

/*!
 * \brief Sets the Power Down Mode for power mode request transaction
 *
 * Sets power down mode for transaction type \a CarbonXSataLLTrans_PowerDown
 *
 * \param trans Pointer to transaction object.
 * \param powerMode Power mode.
 */
void carbonXSataLLTransSetPowerDownMode(CarbonXSataLLTrans *trans,
        CarbonXSataLLPowerDownMode powerMode);

/*!
 * \brief Gets the Power Down Mode for power down request received
 *
 * Gets power down mode for transaction type \a CarbonXSataLLTrans_PowerDown
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Power mode.
 */
CarbonXSataLLPowerDownMode carbonXSataLLTransGetPowerDownMode(
        CarbonXSataLLTrans *trans);

/*!
 * \brief Gets the transaction status after a transaction
 *
 * \param trans Pointer to transaction object.
 *
 * \returns Transaction status.
 */
CarbonXSataLLTransStatus carbonXSataLLTransGetTransStatus(
        CarbonXSataLLTrans *trans);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
