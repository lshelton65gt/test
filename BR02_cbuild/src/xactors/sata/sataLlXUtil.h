#ifndef SATALLX_UTILS_H
#define SATALLX_UTILS_H

#include "sataLlXDefines.h"

/*!
 * \file sataLlXUtil.h 
 * \brief Header file for SATA LL transactor utility routines. 
 * 
 *  This file provides definition of the utility routines.
 */


/* Bit Accessing Routines */

/*! 
 * \brief Sets a new bit in register.
 *
 * This routine set a new bit on a specified index of a specified register 
 * \param reg Register by reference
 * \param index Bit index
 * \param newBit New bit value
 */
void satallxSetBit(SatallxDword *reg, UInt index, SatallxBit newBit);

/*!
 * \brief Gets a new bit from a register.
 *
 *  This routine returns a bit from the index of a specified register.
 *  \param reg Register by reference
 *  \param index Bit index
 *
 *  \returns The extracted bit value from that index of the register
 */
SatallxBit satallxGetBit(SatallxDword reg, UInt index);

/*!
 * \brief Prints a DWORD from a register in binary format
 *
 *  This routines prints the register value starting from the bit 
 *  value specified.
 *
 *  \param reg Register
 *  \param startMsb starting value
 */
void satallxPrintDword(SatallxDword reg, UInt startMsb);


/* CRC Generator Routines */

/*! 
 * \brief Initialize CRC Generator for SATA
 */
SatallxDword satallxInitCrc();

/*!
 * \brief Update running CRC
 *
 * This function takes input data and update the running CRC value.
 * \param inpData Input data on which CRC will be calculated 
 * \param crcLfsr Running CRC register by reference. Note, this should be
 *                initialized by initCrc() at the start
 */
void satallxUpdateCrc(SatallxDword *crcLfsr, SatallxDword inpData);


/* Scrambler Routines */

/*!
 * \brief Initialize Scrambler LFSR for SATA
 */
SatallxWord satallxInitScramblerLfsr();

/*!
 * \brief Scrambling Routine for DWORD data 
 * \param inpData Input data on which scrambling will be done
 * \param scramblerLfsr Scrambler LFSR register by reference
 * \returns Running scrambled value.
 */
SatallxDword satallxScramble(SatallxWord *scramblerLfsr, SatallxDword inpData);


/* 8/10 bit Encoder / Decoder Routines */

/*!
 * \brief Character packet
 *
 * This structure represents character packet. This can be of two types:
 * - Control character
 * - Data character
 *
 * Four such characters together represents primitive or 32 bit payload.
 * For primitive, 0th charcater should be control character and rest 3
 * should be data character. For payload all 4 would data characters.
 */
typedef struct {
    SatallxBool isK;               /*!< Control or Data */
    SatallxByte data;              /*!< Character data */
} SatallxCharacter;

/*! 
 * \brief 8 to 10 bit encoding
 *
 * This function encode 8 bit input character to a 10 bit value.
 * \param rd Running disparity by reference - will be updated
 * \param inpChar Input character packet
 * \param outData 10 bit output value by reference
 * \retval true For successful encoding.
 * \retval false Otherwise
 */
SatallxBool satallxEncode8To10b(SatallxBit *rd, SatallxCharacter inpChar,
        SatallxData10b *outData);

/*!
 * \brief Check if COMMA character 
 *
 * This function checks if the input data is a COMMA character or not
 * \param inpData 10 bit input character 
 * \retval true If inpData is a comma character
 * \retval false Otherwise
 */
SatallxBool satallxCheckForCommaChar(SatallxData10b inpData);

/*!
 * \brief 10 to 8 bit decoding
 *
 * This function decode 10 bit input data to 8 bit output character
 * \param rd Running disparity by reference - will be updated
 * \param inpData 10 bit input data
 * \param outChar 8 bit output character by reference
 * \retval false Decode error
 * \retval true No decode error
 */ 
SatallxBool satallxDecode10To8b(SatallxBit *rd, SatallxData10b inpData,
        SatallxCharacter *outChar);

/* Dxx.y to Byte conversion routine */

/*! 
 * brief Dxx.y to Byte conversion routine
 *
 * This routine converts Dxx.y type format to byte.
 * \param xx Value of xx of Dxx.y 
 * \param y Value of y of Dxx.y
 * \returns Converted byte
 */  
SatallxByte satallxGetByte(SatallxData5b xx, SatallxData3b y);

#endif
