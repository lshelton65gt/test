/*
 * FILENAME:  sataLlXIntf.c
 * 
 * PURPOSE :  This file implements the routines declared in sataLlXIntf.h
 */

#include "sataLlXIntf.h"
#include "sataLlXUtil.h"
#ifndef NOCARBON
#include "sataLlXtor.h"
#endif

#define DEBUG 0

/*
 * Phy Net Routines
 */

#ifndef NOCARBON
/*
 * This Phy Net Routine creates a SatallxPhyNet for the transactor, referred by
 * xtor. The newly created net's currValue is initialized
 * with the value passed as the argument initValue.
 */
SatallxPhyNet *satallxCreatePhyNet(SatallXtor *xtor, SatallxString name,
        SatallxBool isOutput, SatallxBool isScalar, SatallxDword initValue)
{
    SatallxPhyNet *phyNet = NEW(SatallxPhyNet);
    if (phyNet == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor signal.");
        return NULL;
    }
    /* Passing the transactor reference to the net */
    phyNet->xtor = xtor;
    phyNet->name = NEWARRAY(char, strlen(name) + 1);
    if (phyNet->name == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor signal.");
        return NULL;
    }
    /* Initializing the net parameters */
    strcpy(phyNet->name, name);
    phyNet->isOutput = isOutput;
    phyNet->isScalar = isScalar;
    phyNet->currValue = initValue;
    phyNet->module = NULL;
    phyNet->net = NULL;
    phyNet->changeCB = NULL;
    phyNet->cbData = NULL;
    return phyNet;
}

/*
 * Destructor for the PhyNet
 */
void satallxDestroyPhyNet(SatallxPhyNet *sig)
{
    DELETE(sig->name);
    DELETE(sig);
}

/*
 * Routine to connect a PhyNet referred by sig with the Carbon Model module
 * in Carbon Model connection mode.
 */
SatallxBool satallxConnectPhyNetToVhm(SatallxPhyNet *sig,
        CarbonObjectID *module, CarbonNetID *net)
{
    sig->module = module;
    sig->net = net;
    return SatallxTrue;
}

/*
 * Routine to connect a PhyNet referred by sig with the Signal Change Callback
 * Model in Callback connection mode.
 */
SatallxBool satallxConnectPhyNetToModelCallback(SatallxPhyNet *sig,
        CarbonXSignalChangeCB changeCB, void *cbData)
{
    sig->changeCB = changeCB;
    sig->cbData = cbData;
    return SatallxTrue;
}

/*
 * Routine to Update the PhyNet value referred by sig. Returns True on success.
 */
SatallxBool satallxUpdatePhyNet(SatallxPhyNet *sig, CarbonUInt64 currTime)
{
    CarbonUInt32 val32;
    /* static char name[1024]; */
    if (sig == NULL)
        return SatallxFalse;
    if (sig->isOutput) /* output port */
    {
        val32 = sig->currValue;
        if (sig->module && sig->net) /* for Carbon Model connectivity */
        {
            if (carbonDeposit(sig->module, sig->net, &val32, NULL)
                    != eCarbon_OK)
                return SatallxFalse;
            /*
            if (carbonGetNetName(sig->module, sig->net, name, 1024)
                != eCarbon_OK)
                return SatallxFalse;
                */
        }
        else if (sig->changeCB) /* for callback connectivity */
        {
            (*sig->changeCB)(&val32, sig->cbData);
        }
        else
        {
            /* error */
            return SatallxFalse;
        }
#if DEBUG
        printf("%s[%d]: Signal %s is deposited by ",
            sig->xtor->name, (UInt) currTime, sig->name);
        satallxPrintDword(val32, sig->isScalar? 0:9);
        printf(".\n");
#endif
    }
    else /* input port */
    {
        if (sig->module && sig->net) /* for Carbon Model connectivity */
        {
            if (carbonExamine(sig->module, sig->net, &val32, NULL)
                    != eCarbon_OK)
                return SatallxFalse;
            /*
            if (carbonGetNetName(sig->module, sig->net, name, 1024)
                    != eCarbon_OK)
                return SatallxFalse;
                */
        }
        else if (sig->changeCB) /* for callback connectivity */
        {
            (*sig->changeCB)(&val32, sig->cbData);
        }
        else
        {
            /* error */
            return SatallxFalse;
        }
        sig->currValue = val32;
#if DEBUG
        printf("%s[%d]: Signal %s is examined to have ",
            sig->xtor->name, (UInt) currTime, sig->name);
        satallxPrintDword(val32, sig->isScalar? 0:9);
        printf(".\n");
#endif
    }
    return SatallxTrue;

    currTime = 0; /* Identifier assigned with any value to remove compile time 
                     warning during normal compilation */
}
#endif


/*
 * Refresh Routines
 *  
 * Routine to deposit 1 bit value on a scalar PhyNet pointed by sig with one 
 * passed as an argument.
 */
void satallxDepositSignal(SatallxPhyNet *sig, SatallxBit value)
{
#ifdef NOCARBON
    sig->value = value;
    printf("PHY: Signal %s is deposited by %d.\n", sig->name, value);
#else
    sig->currValue = value;
#endif
}

/* 
 * Routine to return current bit value on the scalar PhyNet referred by sig.
 */
SatallxBit satallxExamineSignal(SatallxPhyNet *sig)
{
#ifdef NOCARBON
    SatallxBit value = 0;
    value = sig->value & 0x1;
    printf("PHY: Signal %s is examined to have %d.\n", sig->name, value);
    return value;
#else
    return (sig->currValue & 0x1);
#endif
}

/*
 * Routine to Deposit 10bit data value on a vector PhyNet referred by sig.
 */
void satallxDepositData10b(SatallxPhyNet *sig, SatallxData10b value)
{
#ifdef NOCARBON
    sig->value = value;
    printf("PHY: Signal %s is deposited by ", sig->name);
    satallxPrintDword(value, 9);
    printf(".\n");
#else
    sig->currValue = value;
#endif
}

/*
 * Routine to return current 10b data value on a vector PhyNet referred by sig.
 */
SatallxData10b satallxExamineData10b(SatallxPhyNet *sig)
{
#ifdef NOCARBON
    SatallxData10b value = 0;
    value = sig->value & 0x3FF;
    printf("PHY: Signal %s is examined to have ", sig->name);
    satallxPrintDword(value, 9);
    printf(".\n");
    return value;
#else
    return (sig->currValue & 0x3FF);
#endif
}

/*
 * Interface Routines
 */

#ifndef NOCARBON
SatallxPhyIntf *satallxCreatePhyIntf(SatallXtor *xtor)
{
    SatallxPhyIntf *phyIntf = NEW(SatallxPhyIntf);
    if (phyIntf == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    /* creating the PHY interface net wrappers */
    phyIntf->COMRESET = satallxCreatePhyNet(xtor, "COMRESET",
        SatallxTrue /* output */, SatallxTrue /* scalar */, 0);
    phyIntf->COMWAKE = satallxCreatePhyNet(xtor, "COMWAKE",
        SatallxTrue /* output */, SatallxTrue /* scalar */, 0);
    phyIntf->PhyRdy = satallxCreatePhyNet(xtor, "PhyRdy",
        SatallxTrue /* output */, SatallxTrue /* scalar */, 1);
    phyIntf->DeviceDetect = satallxCreatePhyNet(xtor, "DeviceDetect",
        SatallxTrue /* output */, SatallxTrue /* scalar */, 1);
    phyIntf->PhyInternalError = satallxCreatePhyNet(xtor, "PhyInternalError",
        SatallxTrue /* output */, SatallxTrue /* scalar */, 0);
    phyIntf->COMMA = satallxCreatePhyNet(xtor, "COMMA",
        SatallxTrue /* output */, SatallxTrue /* scalar */, 0);
    phyIntf->PARTIAL = satallxCreatePhyNet(xtor, "PARTIAL",
        SatallxFalse /* input */, SatallxTrue /* scalar */, 0);
    phyIntf->SLUMBER = satallxCreatePhyNet(xtor, "SLUMBER",
        SatallxFalse /* input */, SatallxTrue /* scalar */, 0);
    phyIntf->DATAOUT = satallxCreatePhyNet(xtor, "DATAOUT",
        SatallxTrue /* output */, SatallxFalse /* vector */, 0);
    phyIntf->DATAIN = satallxCreatePhyNet(xtor, "DATAIN",
        SatallxFalse /* input */, SatallxFalse /* vector */, 0);
    return phyIntf;
}

void satallxDestroyPhyIntf(SatallxPhyIntf *phyIntf)
{
    /* destroying since the relationship is of containment type */
    satallxDestroyPhyNet(phyIntf->COMRESET);
    satallxDestroyPhyNet(phyIntf->COMWAKE);
    satallxDestroyPhyNet(phyIntf->PhyRdy);
    satallxDestroyPhyNet(phyIntf->DeviceDetect);
    satallxDestroyPhyNet(phyIntf->PhyInternalError);
    satallxDestroyPhyNet(phyIntf->COMMA);
    satallxDestroyPhyNet(phyIntf->PARTIAL);
    satallxDestroyPhyNet(phyIntf->SLUMBER);
    satallxDestroyPhyNet(phyIntf->DATAOUT);
    satallxDestroyPhyNet(phyIntf->DATAIN);
    DELETE(phyIntf);
}

SatallxFsmTxIntf *satallxCreateFsmTxIntf()
{
    SatallxFsmTxIntf *fsmTxIntf = NEW(SatallxFsmTxIntf);
    if (fsmTxIntf == NULL)
        return NULL;
    /* Initializing the interface parameters */
    fsmTxIntf->isPrimitive = SatallxFalse;
    fsmTxIntf->txPrimitive = SatallxPrim_UNDEF;
    fsmTxIntf->txStopCont = SatallxFalse;
    fsmTxIntf->txData = 0;
    return fsmTxIntf;
}

void satallxDestroyFsmTxIntf(SatallxFsmTxIntf *fsmTxIntf)
{
    DELETE(fsmTxIntf);
}

SatallxFsmRxIntf *satallxCreateFsmRxIntf()
{
    SatallxFsmRxIntf *fsmRxIntf = NEW(SatallxFsmRxIntf);
    if (fsmRxIntf == NULL)
        return NULL;
    /* Initializing the interface parameters */
    fsmRxIntf->isPrimitive = SatallxFalse;
    fsmRxIntf->rxPrimitive = SatallxPrim_UNDEF;
    fsmRxIntf->rxData = 0;
    fsmRxIntf->hasError = SatallxFalse;
    return fsmRxIntf;
}

void satallxDestroyFsmRxIntf(SatallxFsmRxIntf *fsmRxIntf)
{
    DELETE(fsmRxIntf);
}

SatallxFsmPhyIntf *satallxCreateFsmPhyIntf(SatallxPhyIntf *phyIntf)
{
    SatallxFsmPhyIntf *fsmPhyIntf = NEW(SatallxFsmPhyIntf);
    if (fsmPhyIntf == NULL)
        return NULL;
    /* Assigning the reference of nets in transactor's PHY interface */
    fsmPhyIntf->COMRESET = phyIntf->COMRESET;
    fsmPhyIntf->COMWAKE = phyIntf->COMWAKE;
    fsmPhyIntf->PhyRdy = phyIntf->PhyRdy;
    fsmPhyIntf->DeviceDetect = phyIntf->DeviceDetect;
    fsmPhyIntf->PhyInternalError = phyIntf->PhyInternalError;
    fsmPhyIntf->PARTIAL = phyIntf->PARTIAL;
    fsmPhyIntf->SLUMBER = phyIntf->SLUMBER;
    return fsmPhyIntf;
}

void satallxDestroyFsmPhyIntf(SatallxFsmPhyIntf *phyIntf)
{
    /* Note that the nets are not deleted */
    DELETE(phyIntf);
}

SatallxTxPhyIntf *satallxCreateTxPhyIntf(SatallxPhyIntf *phyIntf)
{
    SatallxTxPhyIntf *txPhyIntf = NEW(SatallxTxPhyIntf);
    if (txPhyIntf == NULL)
        return NULL;
    /* Assigning the reference of nets in transactor's PHY interface */
    txPhyIntf->DATAOUT = phyIntf->DATAOUT;
    txPhyIntf->COMMA = phyIntf->COMMA;
    return txPhyIntf;
}

void satallxDestroyTxPhyIntf(SatallxTxPhyIntf *phyIntf)
{
    /* Note that the nets are not deleted */
    DELETE(phyIntf);
}

SatallxRxPhyIntf *satallxCreateRxPhyIntf(SatallxPhyIntf *phyIntf)
{
    SatallxRxPhyIntf *rxPhyIntf = NEW(SatallxRxPhyIntf);
    if (rxPhyIntf == NULL)
        return NULL;
    /* Assigning the reference of nets in transactor's PHY interface */
    rxPhyIntf->DATAIN = phyIntf->DATAIN;
    return rxPhyIntf;
}

void satallxDestroyRxPhyIntf(SatallxRxPhyIntf *phyIntf)
{
    /* Note that the nets are not deleted */
    DELETE(phyIntf);
}
#endif
