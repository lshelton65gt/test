/*
 * FILE NAME : sataLlXRxUnit.c
 *
 * PURPOSE:    This file defines the functions to create and update Rx unit.
 *             It describes how primitives and data are received.
 */

#include "sataLlXRxUnit.h"
#include "sataLlXUtil.h"
#ifndef NOCARBON
#include "sataLlXtor.h"
#endif

/* This routine creates the RxUnit.
 * Called: when Transactor is created. */
SatallxRxUnit *satallxCreateRxUnit(SatallXtor *xtor,
        SatallxFsmRxIntf *fsmIntf)
{
    SatallxRxUnit *rxUnit = NEW(SatallxRxUnit);
    if (rxUnit == NULL)
        return NULL;
    rxUnit->xtor = xtor;
    if (fsmIntf == NULL)
        return NULL;
    rxUnit->fsmIntf = fsmIntf;
#ifndef NOCARBON
    rxUnit->phyIntf = satallxCreateRxPhyIntf(xtor->phyIntf);
#endif
    satallxResetRxUnit(rxUnit);
    return rxUnit;
}

/* This routine destroys RxUnit
 * Called when transactor is destroyed */
void satallxDestroyRxUnit(SatallxRxUnit *rxUnit)
{
#ifndef NOCARBON
    satallxDestroyRxPhyIntf(rxUnit->phyIntf);
#endif
    DELETE(rxUnit);
}

/* Resets RxUnit.
 * Called: When LRESET or ComWake comes */
void satallxResetRxUnit(SatallxRxUnit *rxUnit)
{
    UInt i;
    rxUnit->dataDescramblerLfsr = 0;
    rxUnit->rdDecoder10To8b = 0; /* set rd- by default,
                                    even putting inverted tx rd is also ok */
    rxUnit->fsmIntf->isPrimitive = SatallxFalse;
    rxUnit->fsmIntf->rxPrimitive = SatallxPrim_UNDEF;
    rxUnit->fsmIntf->rxData = 0;
    rxUnit->byteCount = 0;
    for (i = 0; i < 4; i++)
    {
        (rxUnit->charArray[i]).isK = SatallxFalse;
        (rxUnit->charArray[i]).data = 0;
    }
    rxUnit->hasError = SatallxFalse;
    rxUnit->receivedContp = SatallxFalse;
}

/* Updates RxUnit and receives 10 bit packet
 * Called: At each posedge of RxClock */
#ifndef SYNC
/* This is the conventional implementation of RxUnit
 * This method is used here */
SatallxBool satallxUpdateRxUnit(SatallxRxUnit *rxUnit, CarbonUInt64 currTime)
{
    SatallxCharacter charPacket = { SatallxFalse, 0 };
    SatallxData10b inpData10b = satallxExamineData10b(
            rxUnit->phyIntf->DATAIN);
    /* 10/8b Decoding */
    SatallxBool status = satallxDecode10To8b(&(rxUnit->rdDecoder10To8b),
                inpData10b, &charPacket);  /*  this function returns false
                                                if there is any decode error*/

    if (!status) /* if there is a decode error*/
    {
        rxUnit->hasError = SatallxTrue;
        ERROR("DECODER ERROR");
        return SatallxTrue; /* error */
    }

    if (rxUnit->byteCount == 0) /* At each 4 th clock */
    {
        if ((rxUnit->charArray[0]).isK) /* possibly primitive */
        {
            SatallxPrimType prim = satallxFindPrim(rxUnit->charArray);
            if (prim == SatallxPrim_UNDEF) /* if DWORD is not a primitive*/
            {
                rxUnit->hasError = SatallxTrue;
                ERROR("INVALID PRIMITIVE ERROR");
                /*** DO NOT return SatallxTrue; ***/ /* error */
            }
            else /* for valid primitive */
            {
                if (prim == SatallxPrim_CONT) /* if the primitive is CONTp */
                {
                    if (rxUnit->fsmIntf->isPrimitive)
                        /* if last received DWORD is primitive */
                    {
                        rxUnit->receivedContp = SatallxTrue;
                    }
                    else
                    {
                        /* error when CONTp came after data */
                        ERROR("INVALID CONT ERROR");
                    }
                }
                else if (!(rxUnit->receivedContp && prim == SatallxPrim_ALIGN))
                    /* Note, ALIGNp can't terminate repeatative primitive
                     * suppression initiated by CONTp */
                {
                    /* Normal primitive reception outside CONTp sequence */
                    rxUnit->receivedContp = SatallxFalse;
                    rxUnit->fsmIntf->isPrimitive = SatallxTrue;
                    rxUnit->fsmIntf->rxPrimitive = prim;
                    if (prim == SatallxPrim_SOF)
                    {
                        /* On SOFp, reset the payload descrambler */
                        rxUnit->dataDescramblerLfsr =
                            satallxInitScramblerLfsr();
                    }
                }
                /* else if (rxUnit->receivedContp && prim == SatallxPrim_ALIGN)
                 * which means ALIGNp came during CONTp sequence. At this
                 * stage don't update FSM interface to continue on earlier
                 * repetitive primitive */
            }
        }
        else if (!rxUnit->receivedContp) /* RPS data */
        {
            Int i; /* Don't make 'i' UInt */
            SatallxDword fsmRxDword = 0x0;
            for (i = 3; i >= 0; i--)
            {
                if ((rxUnit->charArray[i]).isK)/* control character */
                {
                    rxUnit->hasError = SatallxTrue;
                    ERROR("INVALID PRIMITIVE DATA ERROR");
                    /*** DO NOT return SatallxTrue; ***/ /* error */
                }
                fsmRxDword = (fsmRxDword << 8) |
                    ((rxUnit->charArray[i]).data & 0xFF);
            }
            /* Descrambling */
#ifndef NOCARBON
            /* If configuration for scrambling is on */
            if (rxUnit->xtor->config == NULL ||
                    rxUnit->xtor->config->isScramblingOn)
#else
            if (1)
#endif
            {
            /* Data is fed for descrambling */
                rxUnit->fsmIntf->rxData =
                    satallxScramble(&(rxUnit->dataDescramblerLfsr), fsmRxDword);
            }
            else
            {
                /* Descrambling is off */
                rxUnit->fsmIntf->rxData = fsmRxDword;
            }
            rxUnit->fsmIntf->isPrimitive = SatallxFalse;
        }
        rxUnit->fsmIntf->hasError = rxUnit->hasError; /* propagate decoder
                                                         error to FSM */
        rxUnit->hasError = SatallxFalse;
    }

    if (charPacket.isK) /* control character synchronizes the DWORD formation */
        rxUnit->byteCount = 0;

    rxUnit->charArray[rxUnit->byteCount] = charPacket;
    rxUnit->byteCount = (rxUnit->byteCount + 1) % 4;

    return SatallxTrue;

    currTime = 0; /* to remove compile time warning */
}
#else
/* This is the faster implementation of RxUnit
 * This method is not used here */
UInt satallxUpdateRxUnit(SatallxRxUnit *rxUnit, CarbonUInt64 currTime)
{
    SatallxCharacter charPacket = { SatallxFalse, 0 };
    SatallxData10b inpData10b = satallxExamineData10b(
            rxUnit->phyIntf->DATAIN);
    /* 10/8b Decoding */
    SatallxBool status = satallxDecode10To8b(&(rxUnit->rdDecoder10To8b),
                inpData10b, &charPacket);
    if (!status)
    {
        rxUnit->hasError = SatallxTrue;
        ERROR("DECODER ERROR");
        return SatallxTrue; /* error */
    }

    if (charPacket.isK)
        rxUnit->byteCount = 0;

    rxUnit->charArray[rxUnit->byteCount] = charPacket;

    if (rxUnit->byteCount == 3)
    {
        if ((rxUnit->charArray[0]).isK) /* possibly primitive */
        {
            SatallxPrimType prim = satallxFindPrim(rxUnit->charArray);
            if (prim == SatallxPrim_UNDEF)
            {
                rxUnit->hasError = SatallxTrue;
                ERROR("INVALID PRIMITIVE ERROR");
                /*** DO NOT return SatallxTrue; ***/ /* error */
            }
            else
            {
                rxUnit->fsmIntf->isPrimitive = SatallxTrue;
                rxUnit->fsmIntf->rxPrimitive = prim;
                if (prim == SatallxPrim_SOF)
                {
                    /* On SOFp, reset the payload descrambler */
                    rxUnit->dataDescramblerLfsr = satallxInitScramblerLfsr();
                }
            }
        }
        else /* possibly data */
        {
            Int i; /* Don't make 'i' UInt */
            SatallxDword fsmRxDword = 0x0;
            for (i = 3; i >= 0; i--)
            {
                if ((rxUnit->charArray[i]).isK)
                {
                    rxUnit->hasError = SatallxTrue;
                    ERROR("INVALID PRIMITIVE DATA ERROR");
                    /*** DO NOT return SatallxTrue; ***/ /* error */
                }
                fsmRxDword = (fsmRxDword << 8) |
                    ((rxUnit->charArray[i]).data & 0xFF);
            }
            /* Descrambling */
            rxUnit->fsmIntf->rxData =
                satallxScramble(&(rxUnit->dataDescramblerLfsr), fsmRxDword);
            rxUnit->fsmIntf->isPrimitive = SatallxFalse;
        }
        rxUnit->fsmIntf->hasError = rxUnit->hasError;
        rxUnit->hasError = SatallxFalse;
    }

    rxUnit->byteCount = (rxUnit->byteCount + 1) % 4;

    return SatallxTrue;
}
#endif
