/*
   FILE NAME : sataLlXUtil.c 

   PURPOSE   : Carbon Sata Link Layer Transactor Utility 
               Routine Implementations.
*/

#include "sataLlXUtil.h"

/* Bit Accessing Routines */

/* This routine set a new bit on a specified index of a specified register */
void satallxSetBit(SatallxDword *reg, UInt index, SatallxBit newBit)
{
    
    SatallxDword mask = 0x1 << index;
    SatallxDword setReg = (newBit & 0x1) << index;
    *reg &= (~mask);
    *reg |= setReg;
}

/* This routine returns a bit from the index of a specified register */
SatallxBit satallxGetBit(SatallxDword reg, UInt index)
{
    SatallxDword mask = 0x1 << index;
    return (reg & mask) >> index;
}

/* This routines prints the register value starting from the MSB */
void satallxPrintDword(SatallxDword reg, UInt startMsb)
{
    Int i; /* Don't make 'i' UInt */
    for (i = startMsb; i >= 0; i--)
        printf("%d", satallxGetBit(reg, i));
}

/* CRC Generator Routines */

/* Initialize CRC Generator */
SatallxDword satallxInitCrc()
{
    /* seed specified in Sata specification */
    return 0x52325032;
}

/* Update running CRC for inpData.
 * crcLfsr should be initialized by initCrc()  */
void satallxUpdateCrc(SatallxDword *crcLfsr, SatallxDword inpData)
{
    /* polyReg represents the polynomial of CRC 32 i.e.
     *     x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10
     *                    + x^8 + x^7 + x^5 + x^4 + x^2 + x^ + 1 
     * It ignores the x^32 coefficient.
     *
     * Its implementation is similar to as given below for
     *     polynomial = x^5 + x^4 + x^2 + 1 = 110101
     *
     *        +-- dn dn-1 ... d2 d1 d0 (big-endian bit order in 32 bit DWORD)
     *        |
     *        |    x^4         x^3     x^2         x^1     x^0
     *        v   +---+       +---+   +---+       +---+   +---+
     *    +<-XOR<-|Q D|<-XOR<-|Q D|<--|Q D|<-XOR<-|Q D|<--|Q D|<-+
     *    |       |   |   ^   |   |   |   |   ^   |   |   |   |  |
     *    |       +---+   |   +---+   +---+   |   +---+   +---+  |
     *    |               |                   |                  |
     *    +---------------+-------------------+------------------+
     *
     * For more details, check PCI Express Spec Base 1.0a for L/ECRC.
     */
    static const SatallxDword polyReg = 0x04C11DB7;
    UInt i; Int j; /* Don't make 'j' UInt */
    SatallxDword newCrc = *crcLfsr;
    /* For Sata, input data is to be fed in big-endian form, i.e MSB first */
    for (j = 31; j >= 0; j--)
    {
        SatallxBit inpBit = satallxGetBit(inpData, j);
        SatallxBit bitMsb = satallxGetBit(*crcLfsr, 31);
        SatallxBit newBitLsb = bitMsb ^ inpBit;
        for (i = 0; i < 32; i++)
        {
            SatallxBit newBit;
            if (i == 0)
                newBit = newBitLsb;
            else
                newBit = satallxGetBit(*crcLfsr, i - 1) ^
                    (satallxGetBit(polyReg, i) & newBitLsb);
            satallxSetBit(&newCrc, i, newBit);
        }
        *crcLfsr = newCrc;
    }
}

/* Scrambler Routines */

/* Initialize Scrambler LFSR */
SatallxWord satallxInitScramblerLfsr()
{
    return 0xFFFF;
}

/* Scrambling Routine for SatallxDword data */
SatallxDword satallxScramble(SatallxWord *scramblerLfsr, SatallxDword inpData)
{
    /* polyReg represents the polynomial of Sata Scrambler i.e.
     *     x^16 + x^15 + x^13 + x^4 + 1
     * It ignores the x^16 coefficient.
     * It implements simple 16 bit internal LFSR, whose output
     * is XORed input data from LSB to MSB.
     */
    static const SatallxWord polyReg = 0xA011;
    UInt i, j;
    SatallxDword scramblerOut = 0x0;
    SatallxWord newLfsrReg = *scramblerLfsr;
    for (j = 0; j < 32; j++)
    {
        SatallxBit bitMsb = satallxGetBit(*scramblerLfsr, 15);
        for (i = 0; i < 16; i++)
        {
            SatallxBit newBit;
            if (i == 0)
                newBit = bitMsb;
            else
                newBit = satallxGetBit(*scramblerLfsr, i - 1) ^
                    (satallxGetBit(polyReg, i) & bitMsb);
            satallxSetBit(&newLfsrReg, i, newBit);
        }
        *scramblerLfsr = newLfsrReg;
        satallxSetBit(&scramblerOut, j, bitMsb);
    }
    return (inpData ^ scramblerOut) & 0xFFFFFFFF;
}

/* 8/10 bit Encoder / Decoder Routines */

/* Array representing ROM */
static const SatallxData6b satallxgConv5To6bDataArray[] = {
    0x18, 0x22, 0x12, 0x31, 0x0A, 0x29, 0x19, 0x07, /*  D0 -  D7 */
    0x06, 0x25, 0x15, 0x34, 0x0D, 0x2C, 0x1C, 0x28, /*  D8 - D15 */
    0x24, 0x23, 0x13, 0x32, 0x0B, 0x2A, 0x1A, 0x05, /* D16 - D23 */
    0x0C, 0x26, 0x16, 0x09, 0x0E, 0x11, 0x21, 0x14  /* D24 - D31 */
};
static const SatallxBit satallxgConv5To6bDataInvArray[] = {
    0x1, 0x1, 0x1, 0x0, 0x1, 0x0, 0x0, 0x1,         /*  D0 -  D7 */
    0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1,         /*  D8 - D15 */
    0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1,         /* D16 - D23 */
    0x1, 0x0, 0x0, 0x1, 0x0, 0x1, 0x1, 0x1          /* D24 - D31 */
};
static const SatallxData4b satallxgConv3To4bDataArray[] = {
    0x4, 0x9, 0x5, 0x3, 0x2, 0xA, 0x6, 0x01, 0x8 /* Dx.0 -  Dx.P7, Dx.A7 */
};
static const SatallxBit satallxgConv3To4bDataInvArray[] = {
    0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x01, 0x1 /* Dx.0 -  Dx.P7, Dx.A7 */
};
static const SatallxData10b satallxgControlChar28p3 = 0x30C;
static const SatallxData10b satallxgControlChar28p5 = 0x305;


/* This routine updates running disparity as per the data */
static void satallxUpdateRd(SatallxBit *rd, SatallxByte encodedOut,
        UInt size)
{
    if ((size == 6 && (encodedOut & 0x3F) == 0x07) ||
            (size == 4 && (encodedOut & 0xF) == 0x3))
        *rd = 0x1; /* special case, refer to SATA 2.5 Gold, 9.2.2.2 */
    else if ((size == 6 && (encodedOut & 0x3F) == 0x38) ||
            (size == 4 && (encodedOut & 0xF) == 0xC))
        *rd = 0x0; /* special case, refer to SATA 2.5 Gold, 9.2.2.2 */
    else
    {
        UInt i, countOne = 0;
        for (i = 0; i < size; i++)
        {
            if ((encodedOut & 0x01) == 0x01)
                countOne++;
            encodedOut = encodedOut >> 1;
        }
        if (countOne > size / 2) /* number of 1 > number of 0 */
            *rd = 0x1; /* positive disparity */
        else if (countOne < size / 2) /* number of 1 < number of 0 */
             *rd = 0x0; /* negative disparity */
        /* else when (countOne == size / 2) neutral disparity */
    }
}

/* Encode inpChar to outData. rd is running disparity.
 * Returns 1 on successful encoding. */
SatallxBool satallxEncode8To10b(SatallxBit *rd, SatallxCharacter inpChar,
        SatallxData10b *outData)
{
    SatallxData5b dataEDCBA = inpChar.data & 0x1F;
    SatallxData3b dataHGF = (inpChar.data & 0xE0) >> 5;
    SatallxData6b abcdei = 0x0;
    SatallxData4b fghi = 0x0;
    *outData = 0x0;
    if (inpChar.isK) /* for control char */
    {
       if (dataEDCBA != 28)
           return SatallxFalse;
       switch(dataHGF) /* only K28.3 and K28.5 are supported */
       {
           case 3: *outData = satallxgControlChar28p3; break;
           case 5: *outData = satallxgControlChar28p5; break;
           default: return SatallxFalse;
       }
       if (*rd == 0x0)
           *outData = ~(*outData) & 0x3FF;
       *rd = ~(*rd) & 0x1; /* rd inverts */
       return SatallxTrue;
    }
    /* for data char */
    abcdei = satallxgConv5To6bDataArray[dataEDCBA];
    if ((*rd & 0x1) == 0x0 && satallxgConv5To6bDataInvArray[dataEDCBA] == 0x1)
        abcdei = ~abcdei & 0x3F;
    satallxUpdateRd(rd, abcdei, 6);
    if (dataHGF == 0x7 && /* check if A7 can replace P7 */
            ((*rd == 0x1 && (abcdei & 0x3) == 0x0) || /* rd > 0 & (e = i = 0) */
             (*rd == 0x0 && (abcdei & 0x3) == 0x3))) /* rd < 0 & (e = i = 1) */
        fghi = satallxgConv3To4bDataArray[0x8]; /* array[8] is A7 */
    else
        fghi = satallxgConv3To4bDataArray[dataHGF];
    if ((*rd & 0x1) == 0x0 && satallxgConv3To4bDataInvArray[dataHGF] == 0x1)
        fghi = ~fghi & 0xF;
    satallxUpdateRd(rd, fghi, 4);
    *outData = ((abcdei << 4) | fghi) & 0x3FF;
    return SatallxTrue;
}

/* Check if COMMA character */
SatallxBool satallxCheckForCommaChar(SatallxData10b inpData)
{
    return ((inpData == satallxgControlChar28p5) ||
            (inpData == (~satallxgControlChar28p5 & 0x3FF)));
}

/* Decode inpData to outChar. rd is running disparity. Return
 * value determines decode error (if returns false) */
SatallxBool satallxDecode10To8b(SatallxBit *rd, SatallxData10b inpData,
        SatallxCharacter *outChar)
{
    SatallxData6b abcdei = (inpData & 0x3F0) >> 4;
    SatallxData4b fghi = inpData & 0xF;
    SatallxData5b dataEDCBA = 0x0;
    SatallxData3b dataHGF = 0x0;
    SatallxBit rdBefore6To5, rdBefore4To3;
    UInt i;
    outChar->isK = SatallxFalse;
    outChar->data = 0x0;
    rdBefore6To5 = *rd;
    satallxUpdateRd(rd, abcdei, 6);
    rdBefore4To3 = *rd;
    satallxUpdateRd(rd, fghi, 4);
    if (abcdei == 0x0F || abcdei == 0x30) /* control char */
    {
        inpData &= 0x3FF;
        if ((rdBefore6To5 == 0x1 && inpData == satallxgControlChar28p3) ||
                (rdBefore6To5 == 0x0 &&
                 inpData == (~satallxgControlChar28p3 & 0x3FF)))
            outChar->data = (3 << 5) | 28;
        else if ((rdBefore6To5 == 0x1 && inpData == satallxgControlChar28p5) ||
                (rdBefore6To5 == 0x0 &&
                 inpData == (~satallxgControlChar28p5 & 0x3FF)))
            outChar->data = (5 << 5) | 28;
        else
            return SatallxFalse; /* decode error */
        outChar->isK = SatallxTrue;
        return SatallxTrue;
    }
    for (i = 0; i < 32; i++)
    {
        SatallxData6b check = satallxgConv5To6bDataArray[i];
        if ((rdBefore6To5 & 0x1) == 0x0 &&
                satallxgConv5To6bDataInvArray[i] == 0x1)
            check = ~check & 0x3F;
        if (abcdei == check)
        {
            dataEDCBA = i;
            break;
        }
    }
    if (i == 32)
        return SatallxFalse;
    for (i = 0; i < 8; i++)
    {
        SatallxData4b check;
        if (i == 0x7 && /* check if A7 can replace P7 */
                ((rdBefore4To3 == 0x1 && (abcdei & 0x3) == 0x0) ||
                 (rdBefore4To3 == 0x0 && (abcdei & 0x3) == 0x3)))
            check = satallxgConv3To4bDataArray[0x8]; /* array[8] is A7 */
        else
            check = satallxgConv3To4bDataArray[i];
        if ((rdBefore4To3 & 0x1) == 0x0 &&
                satallxgConv3To4bDataInvArray[i] == 0x1)
            check = ~check & 0xF;
        if (fghi == check)
        {
            dataHGF = i;
            break;
        }
    }
    if (i == 32)
        return SatallxFalse;
    outChar->data = (dataHGF << 5) | dataEDCBA;
    return SatallxTrue;
}

/* Dxx.y to Byte conversion routines */

SatallxByte satallxGetByte(SatallxData5b xx, SatallxData3b y)
{
    return (y << 5 | xx);
}

