#ifndef SATALLX_RXUNIT_H
#define SATALLX_RXUNIT_H

#include "sataLlXUtil.h"
#include "sataLlXIntf.h"

/*!
 *  \file sataLlXRxUnit.h
 *  \brief Receiver unit of SATA LL Transactor 
 *  
 *  This file provides the definition of the structure of receiver unit 
 *  of SATA LL transactor and declares the functions used to create, 
 *  destroy, reset and update the unit.
 */

/*!
 *  \brief This structure defines receiver unit.
 */
typedef struct {
    /* Interfaces */
    SatallxFsmRxIntf *fsmIntf;      /*!< FSM Rx Interface */
    SatallxRxPhyIntf *phyIntf;      /*!< Phy Rx Interface */
    /* Properties */
    SatallXtor *xtor;               /*!< Transactor reference */
    SatallxWord dataDescramblerLfsr;/*!< LFSR for Data Descrambler. */
    SatallxBit rdDecoder10To8b;     /*!< Running Disparity for 10/8 bit
                                         Decoder */
    UInt byteCount;                 /*!< Byte Count runs from 0 to 3 per
                                         RxClock */
    SatallxCharacter charArray[4];  /*!< Array of character */
    SatallxBool hasError;           /*!< Indicates RX error */
    SatallxBool receivedContp;      /*!< Last received primitive is CONTp */
} SatallxRxUnit;

/*! 
 * \brief Creates Receiver Unit of transactor.
 * 
 * This function takes the fsm interface and the transactor pointer to create 
 * the receiver unit of SATA LL transactor.
 *
 * \param xtor Pointer to the transactor
 * \param fsmIntf Pointer to fsm interface
 *
 * \returns Pointer to the receiver unit.
 */
SatallxRxUnit *satallxCreateRxUnit(SatallXtor *xtor,
        SatallxFsmRxIntf *fsmIntf);


/*! 
 * \brief Destroys Receiver Unit of transactor.
 *
 * This function takes the pointer of the created receiver unit to
 * destroy it.
 *
 * \param rxUnit Pointer to the receiver unit of SATA LL transactor.
 */
void satallxDestroyRxUnit(SatallxRxUnit *rxUnit);

/*!
 * \brief Resets Receiver Unit of transactor.
 * 
 * This function takes the pointer of the created receiver unit and 
 * resets it.
 *
 * \param rxUnit Pointer to the receiver unit of SATA LL transactor.
 */
void satallxResetRxUnit(SatallxRxUnit *rxUnit);

/*! 
 * \brief Updates Receiver Unit of transactor.
 *
 * This function updates  Receiver Unit at each posedge of clock, it 
 * takes current simulation time and the reference of the receiver unit.
 *
 * \param rxUnit Pointer to the receiver unit of SATA LL transactor.
 * \param currTime Current simulation time.
 *
 * \retval true Successful updation of receiver unit
 * \retval false Otherwise.
 */
SatallxBool satallxUpdateRxUnit(SatallxRxUnit *rxUnit, CarbonUInt64 currTime);

#endif
