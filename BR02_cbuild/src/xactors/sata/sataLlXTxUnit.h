#ifndef SATALLX_TXUNIT_H
#define SATALLX_TXUNIT_H

#include "sataLlXUtil.h"
#include "sataLlXIntf.h"

/*!
 * \file sataLlXTxUnit.h
 * \brief SATA LL Tx Unit
 *
 * This file defines the structure of Tx unit and declares the functions
 * used to create, update, reset and destroy Tx unit. 
 */

/*!
 * \brief Structure of Tx Unit.
 */
typedef struct {
    /* Interfaces */
    SatallxFsmTxIntf *fsmIntf;      /*!< FSM Tx Interface */
    SatallxTxPhyIntf *phyIntf;      /*!< Phy Tx Interface */
    /* Properties */
    SatallXtor *xtor;               /*!< Transactor reference */
    SatallxWord dataScramblerLfsr;  /*!< LFSR for Data Scrambler. */
    SatallxBit rdEncoder8To10b;     /*!< Running Disparity for 8/10 bit
                                         Encoder */
    UInt byteCount;                 /*!< Byte Count runs from 0 to 3 per
                                         SystemClock (i.e TxClock) */
    SatallxCharacter charArray[4];  /*!< Array of character */
    SatallxWord rpsScramblerLfsr;   /*!< LFSR for Scrambler for data generation
                                         during repeatative primitive
                                         suppression after CONTp. */
    SatallxPrimType rpsQueue[2];    /*!< Primitive Queue for CONTp generation */
    SatallxBool transmittedContp;   /*!< Last primitive transmitted is CONTp */
    SatallxWord rpsData;            /*!< Data for CONTp */
    SatallxByte rpsCount;           /*!< CONTp suppression data count */
} SatallxTxUnit;

/*!
 * \brief Create Tx Unit
 *
 * This routine creates the TxUnit. It is called when Transactor is created.
 *
 * \param xtor Transactor
 * \param fsmIntf FSM to TX interface
 *
 * \returns Pointer to the created structure of Tx unit
 */
SatallxTxUnit *satallxCreateTxUnit(SatallXtor *xtor,
        SatallxFsmTxIntf *fsmIntf);

/*!
 * \brief Destroy Tx Unit
 *
 * This function frees the memory allocated for Tx unit structure.
 *
 * \param txUnit TX Unit
 */
void satallxDestroyTxUnit(SatallxTxUnit *txUnit);

/*!
 * \brief Resets TxUnit
 *
 * Reset the Tx unit and its interfaces when LRESET or ComWake comes.
 *
 * \param txUnit TX Unit
 */
void satallxResetTxUnit(SatallxTxUnit *txUnit);

/*!
 * \brief Updates TxUnit
 *
 * Updates TxUnit and sends 10 bit packet at each posedge of SystemClock
 *
 * \param txUnit TX Unit
 * \param currTime Current simulation time
 *
 * \retval true In case of successful updation
 * \retval false Otherwise
 */
SatallxBool satallxUpdateTxUnit(SatallxTxUnit *txUnit, CarbonUInt64 currTime);

#endif
