/*
 * FILE NAME:  sataLlXtor.c
 * 
 * PURPOSE:    This file creates the transactor and connects interfaces to it.
 *             It also defines the functions that connect the physical layer 
 *             nets of the transactor with the Carbon Model and gets and sets
 *             the transactor configuration parameters.
 */

#ifndef NOCARBON

#include <stdlib.h>
#include "sataLlXtor.h"

#define ERROR_MSG_START 0

/* This function create the transactor and connect the interfaces to it */
CarbonXSataLLID carbonXSataLLCreate(const char* transactorName,
        CarbonXSataLLType xtorType, CarbonUInt32 maxTransfers,
        void (*reportTransaction)(CarbonXSataLLTrans*, void*),
        void (*setDefaultResponse)(CarbonXSataLLTrans*, void*),
        void (*setResponse)(CarbonXSataLLTrans*, void*),
        void (*refreshResponse)(CarbonXSataLLTrans*, void*),
        void (*notify)(CarbonXSataLLErrorType, CarbonXSataLLTrans*, void*),
        void (*notify2)(CarbonXSataLLErrorType, void*),
        void *stimulusInstance)
{
    /* allocates transactor */
    SatallXtor *xtor = NEW(SatallXtor);

    /* scramble feature name and checkout license */
    CARBON_LICENSE_FEATURE(featureName, "crbn_vsp_exec_xtor_sata", 23);
    xtor->mCarbonLicense = UtLicenseWrapperCheckout(featureName, "Serial ATA Transactor");

    if (xtor == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    xtor->errorTransCB = notify;
    xtor->errorCB = notify2;
    xtor->userData = stimulusInstance;

    /* copies the name string */
    xtor->name = NEWARRAY(char, strlen(transactorName) + 1);
    if (xtor->name == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    strcpy(xtor->name, transactorName);
    xtor->isHost = (SatallxBool) xtorType;
    /* copies the config structure */
    xtor->config = NEW(struct CarbonXSataLLConfigStruct);
    if (xtor->config == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    /* configuration status */
    xtor->config->isPowerModeOn = SatallxTrue;
    xtor->config->isScramblingOn = SatallxTrue;
    xtor->config->isContpGen = SatallxTrue;
    /* user defined interfaces */
    xtor->txTransEndCB = reportTransaction;
    xtor->rxTransStartCB = setDefaultResponse;
    xtor->rxTransEndCB = setResponse;
    /* transmission queue connection */
    xtor->transQueue = satallxCreateQueue(maxTransfers);
    if (xtor->transQueue == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    /* physical layer interface */
    xtor->phyIntf = satallxCreatePhyIntf(xtor);
    if (xtor->phyIntf == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    /* Create and connect building units */
    /* FSM to Tx and Rx interfaces are created when FSM unit is created */
    xtor->fsmUnit = satallxCreateFsmUnit(xtor,
            satallxCreateFsmTxIntf(), satallxCreateFsmRxIntf());
    if (xtor->fsmUnit == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    xtor->txUnit = satallxCreateTxUnit(xtor, xtor->fsmUnit->txIntf);
    if (xtor->txUnit == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    xtor->rxUnit = satallxCreateRxUnit(xtor, xtor->fsmUnit->rxIntf);
    if (xtor->rxUnit == NULL)
    {
        satallxError(xtor, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }

    satallxResetFsmUnit(xtor->fsmUnit);
    satallxResetTxUnit(xtor->txUnit);
    satallxResetRxUnit(xtor->rxUnit);
    xtor->clockCount = 0;
    return xtor;

    refreshResponse = NULL; /* remove warning */
}

/* This function frees memmory allocated for the transactor and internal
 * components */
CarbonUInt32 carbonXSataLLDestroy(CarbonXSataLLID xtor)
{
    /* release the license */
    UtLicenseWrapperRelease(&(xtor->mCarbonLicense));

    satallxDestroyQueue(xtor->transQueue);
    satallxDestroyPhyIntf(xtor->phyIntf);

    satallxDestroyFsmTxIntf(xtor->fsmUnit->txIntf);
    satallxDestroyFsmRxIntf(xtor->fsmUnit->rxIntf);

    satallxDestroyFsmUnit(xtor->fsmUnit);
    satallxDestroyTxUnit(xtor->txUnit);
    satallxDestroyRxUnit(xtor->rxUnit);

    if (xtor->config != NULL)
        DELETE(xtor->config);
    DELETE(xtor->name);
    DELETE(xtor);
    return 1;
}

/* This function returns the name of transactor */
const char *carbonXSataLLGetName(CarbonXSataLLID xtor)
{
    return xtor->name;
}

/* This function returns the type of transactor */
CarbonXSataLLType carbonXSataLLGetType(CarbonXSataLLID xtor)
{
    return ((CarbonXSataLLType) (xtor->isHost));
}

/* This function takes a transactor's physical interface net name and returns
 * the transactor signal wrapper for that */
SatallxPhyNet *satallxGetPhyNetByName(SatallXtor *xtor,
        SatallxConstString xtorNetName)
{
    if (strcmp(xtorNetName, "COMRESET") == 0)
    {
        return xtor->phyIntf->COMRESET;
    }
    else if (strcmp(xtorNetName, "COMWAKE") == 0)
    {
        return xtor->phyIntf->COMWAKE;
    }
    else if (strcmp(xtorNetName, "PhyRdy") == 0)
    {
        return xtor->phyIntf->PhyRdy;
    }
    else if (strcmp(xtorNetName, "DeviceDetect") == 0)
    {
        return xtor->phyIntf->DeviceDetect;
    }
    else if (strcmp(xtorNetName, "PhyInternalError") == 0)
    {
        return xtor->phyIntf->PhyInternalError;
    }
    else if (strcmp(xtorNetName, "COMMA") == 0)
    {
        return xtor->phyIntf->COMMA;
    }
    else if (strcmp(xtorNetName, "PARTIAL") == 0)
    {
        return xtor->phyIntf->PARTIAL;
    }
    else if (strcmp(xtorNetName, "SLUMBER") == 0)
    {
        return xtor->phyIntf->SLUMBER;
    }
    else if (strcmp(xtorNetName, "DATAIN") == 0)
    {
        return xtor->phyIntf->DATAIN;
    }
    else if (strcmp(xtorNetName, "DATAOUT") == 0)
    {
        return xtor->phyIntf->DATAOUT;
    }
    else
    {
        return NULL;
    }
    return NULL;
}

/* It connects the physical net handle of transactor to the Carbon Model net */
SatallxBool satallxConnectXtorSignal(SatallXtor *xtor,
        SatallxConstString xtorNetName,
        CarbonObjectID *model, CarbonNetID *modelNet)
{
    SatallxPhyNet *sig;

    if (xtorNetName == NULL)
        return SatallxFalse;
    if (model == NULL)
        return SatallxFalse;
    if (modelNet == NULL)
        return SatallxFalse;
    /* Get the transactor physical net handle */
    sig = satallxGetPhyNetByName(xtor, xtorNetName);
    if (sig == NULL)
        return SatallxFalse;
    /* Connect transactor net with Carbon Model net */
    satallxConnectPhyNetToVhm(sig, model, modelNet);
    return SatallxTrue;
}

/* This function connects transactor signals by name to Carbon Model signals by name. It
 * first find the Carbon net handle given the net name and attaches
 * the net handle with transactor's respective signal wrapper */
CarbonUInt32 carbonXSataLLConnectByModelSignalName(CarbonXSataLLID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    static char hdlPath[1024];
    CarbonNetID *net;
    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameNamePair pair = nameList[i];
        sprintf(hdlPath, "%s.%s", carbonModelName, pair.ModelSignalName);
        /* get the net handle */
        net = carbonFindNet(carbonModelHandle, hdlPath);
        if (net == NULL)
            return 0;
        /* connect the net handle with transactor's respective net */
        if (!satallxConnectXtorSignal(xtor, pair.TransactorSignalName,
                    carbonModelHandle, net))
            return 0; 
    }
    return 1;
}

/* Connects transactor signals by name to Carbon Model signals by handle */
CarbonUInt32 carbonXSataLLConnectByModelSignalHandle(
        CarbonXSataLLID xtor,
        CarbonXInterconnectNameHandlePair *nameList, 
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameHandlePair pair = nameList[i];
        /* connect the net handle with transactor's respective net */
        if (!satallxConnectXtorSignal(xtor, pair.TransactorSignalName,
                    carbonModelHandle, pair.ModelSignalHandle))
            return 0; /* Failure in connection */
    }
    return 1;

    carbonModelName = NULL; /* remove warning */
}

/* Connects transactor signals by name to Carbon Model signals by
 * input/output change callback */
CarbonUInt32 carbonXSataLLConnectToLocalSignalMethods(
        CarbonXSataLLID xtor,
        CarbonXInterconnectNameCallbackTrio *nameList)
{
    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXSignalChangeCB changeCB;
        CarbonXInterconnectNameCallbackTrio pair = nameList[i];
        /* get the transactor signal wrapper */
        SatallxPhyNet *sig = satallxGetPhyNetByName(xtor,
                pair.TransactorSignalName);
        if (sig == NULL)
            return 0;
        if (sig->isOutput) /* for output signal use models input callback */
            changeCB = pair.ModelInputChangeCB;
        else /* for input signal use models output callback */
            changeCB = pair.ModelOutputChangeCB;
        if (!satallxConnectPhyNetToModelCallback(sig, changeCB,
                    pair.UserPointer))
            return 0; 
    }
    return 1;
}

/* Checks if transaction queue has space for new transaction */
CarbonUInt32 carbonXSataLLIsSpaceInTransactionBuffer(
        CarbonXSataLLID xtor)
{
    return (satallxQueueGetSize(xtor->transQueue) >
            satallxQueueGetCount(xtor->transQueue));
}

/* Pushes a new transaction structure in queue if space is there */
CarbonUInt32 carbonXSataLLStartNewTransaction(CarbonXSataLLID xtor,
        CarbonXSataLLTrans *trans)
{
    if (satallxQueueGetCount(xtor->transQueue) >=
            satallxQueueGetSize(xtor->transQueue))
        return 0;
    /* if there is space in queue, push the transaction */
    if (!satallxQueuePush(xtor->transQueue, trans))
    {
        satallxError(xtor, trans, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for while starting transaction.");
        return 0;
    }
    return 1;
}

/* This function evaluates the transactor and should be called for each
 * active edge of clock. This routine will update the Tx and Rx Units for
 * each call (i.e. for each clock) and update the FSM for each 4th call.
 * This is because FSM should run in DwordClock which is 4 time slower
 * that Tx and Rx clock */
CarbonUInt32 carbonXSataLLEvaluate(CarbonXSataLLID xtor,
        CarbonUInt64 currentTime)
{
    // tell the license server we are still using the license
    UtLicenseWrapperHeartbeat(xtor->mCarbonLicense);

    if (xtor->clockCount == 0) /* update fsm unit every 4th clock */
    {
        if (!satallxUpdateFsmUnit(xtor->fsmUnit, currentTime))
            return 0;
    }
    /* update Tx and Rx Unit for each clock */
    if (!satallxUpdateTxUnit(xtor->txUnit, currentTime))
        return 0;
    if (!satallxUpdateRxUnit(xtor->rxUnit, currentTime))
        return 0;
    xtor->clockCount = (xtor->clockCount + 1) % 4;
    return 1;
}

#if 0
CarbonUInt32 carbonXSataLLRefresh(CarbonXSataLLID xtor,
        CarbonUInt64 currentTime)
{
    CarbonUInt32 retStatus = 1;
    if (!satallxUpdatePhyNet(xtor->phyIntf->COMRESET, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->COMWAKE, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->PhyRdy, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->DeviceDetect, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->PhyInternalError,
                currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->COMMA, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->PARTIAL, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->SLUMBER, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->DATAIN, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->DATAOUT, currentTime))
    {
        retStatus = 0;
    }
    return retStatus;
}
#endif

/* In this function, all input signal ports of the transactor
 * are updated but transactor state remains unchanged. Thus at this point
 * transactor will communicate with the model for reading signal values.*/
CarbonUInt32 carbonXSataLLRefreshInputs(CarbonXSataLLID xtor,
        CarbonUInt64 currentTime)
{
    CarbonUInt32 retStatus = 1;
    /* this in turn refreshes each input signals */
    if (!satallxUpdatePhyNet(xtor->phyIntf->PARTIAL, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->SLUMBER, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->DATAIN, currentTime))
    {
        retStatus = 0;
    }
    return retStatus;
}

/* In this function, all output signal ports of the transactor are updated but
 * transactor state remains unchanged. Thus at this point transactor will 
 * communicate with the model for writing signal values. */
CarbonUInt32 carbonXSataLLRefreshOutputs(CarbonXSataLLID xtor,
        CarbonUInt64 currentTime)
{
    CarbonUInt32 retStatus = 1;
    /* this in turn refreshes each output signals */
    if (!satallxUpdatePhyNet(xtor->phyIntf->COMRESET, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->COMWAKE, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->PhyRdy, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->DeviceDetect, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->PhyInternalError,
                currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->COMMA, currentTime))
    {
        retStatus = 0;
    }
    if (!satallxUpdatePhyNet(xtor->phyIntf->DATAOUT, currentTime))
    {
        retStatus = 0;
    }
    return retStatus;
}

/* This function copies the given configuration structure to the transactor's
 *  config structure */
void carbonXSataLLSetConfig(CarbonXSataLLID xtor,
        const CarbonXSataLLConfig *config)
{
    *(xtor->config) = *config; /* copies the given configuration structure to
                                  the transactor's config structure */
}

/* This function get the transactor configuration structure and copies it 
 * to the configuration structure provided */
void carbonXSataLLGetConfig(CarbonXSataLLID xtor, CarbonXSataLLConfig *config)
{
    *config = *(xtor->config);
}

/* This routine dumps the transactor's internal state in console.
 * One can use this routine for debugging purpose.*/
void carbonXSataLLDump(CarbonXSataLLID xtor)
{
    SatallxString xtorType = "Device";
    if (xtor->isHost) xtorType = "Host";
    printf("Carbon SATA Link Layer %s Transactor: %s\n", xtorType, xtor->name);
    /*
    printf("Transactor name   : %s\nTransactor type    : %s\n",
            xtor->name, xtorType);
    */
    printf("Power Down Mode   : %s\n",
            (xtor->config == NULL || xtor->config->isPowerModeOn) ?
            "Supported" : "Not Supported");
    printf("Data Scrambling   : %s\n",
            (xtor->config == NULL || xtor->config->isScramblingOn) ?
            "Supported" : "Not Supported");
    printf("CONTp Generation  : %s\n",
            (xtor->config == NULL || xtor->config->isContpGen) ?
            "Supported" : "Not Supported");
    /* printf("\n"); */
    printf("Transaction buffer size       : %d\n",
            satallxQueueGetSize(xtor->transQueue));
    printf("Number of pending transactions: %d\n",
            satallxQueueGetCount(xtor->transQueue));
    printf("Callbacks registered:\n");
    printf("reportTransaction : %s\n",
            (xtor->txTransEndCB == NULL) ? "No " : "Yes");
    printf("setDefaultResponse: %s, setResponse: %s\n",
            (xtor->rxTransStartCB == NULL) ? "No " : "Yes",
            (xtor->rxTransEndCB == NULL) ? "No " : "Yes");
    printf("notify            : %s, notify2    : %s\n",
            (xtor->errorTransCB == NULL) ? "No " : "Yes",
            (xtor->errorCB == NULL) ? "No " : "Yes");
}

/* This routine performs the error messaging. It is used globally for error
 * handling */
void satallxError(SatallXtor *xtor, SatallxTrans *trans,
        CarbonXSataLLErrorType errorType, SatallxString errorMessage)
{
    if (xtor != NULL)
    {
        /* if transaction for which error happened is available and
         * notify callback is registered */
        if (trans != NULL && xtor->errorTransCB != NULL) 
            (*xtor->errorTransCB)(errorType, trans, xtor->userData);
        /* if notify2 callback is registered */
        else if (xtor->errorCB != NULL)
            (*xtor->errorCB)(errorType, xtor->userData);
        /* when notify and notify2 callbacks are not available
         * show the error message in console */
        else if (xtor->name != NULL)
        {
            printf("CarbonXSataLL: ERROR[%d]: Transactor %s: %s\n",
                ERROR_MSG_START + (UInt) errorType, xtor->name, errorMessage);
            exit(1);
        }
        else
        {
            printf("CarbonXSataLL: ERROR[%d]: %s\n",
                    ERROR_MSG_START + (UInt) errorType, errorMessage);
            exit(1);
        }
    }
    else /* if transactor scope is not available */
    {
        printf("CarbonXSataLL: ERROR[%d]: %s\n",
                ERROR_MSG_START + (UInt) errorType, errorMessage);
        exit(1);
    }
}

/* config routines */

/* This routine create a configuration structure and initializes its members
 * to contain default enabling value. That is PowerMode, Scrambling and
 * ContpGen supports are ON by default. */
CarbonXSataLLConfig *carbonXSataLLConfigCreate(void)
{
    CarbonXSataLLConfig *config = NEW(struct CarbonXSataLLConfigStruct);
    if (config == NULL)
    {
        satallxError(NULL, NULL, CarbonXSataLLError_NoMemory,
                "Can't allocate memory for transactor configuration.");
        return NULL;
    }
    config->isPowerModeOn = SatallxTrue; /* On by default */
    config->isScramblingOn = SatallxTrue;/* On by default */
    config->isContpGen = SatallxTrue;    /* On by default */
    return config;
}

/* This routine frees the memory allocated for transactor's configuration 
 * structure */
CarbonUInt32 carbonXSataLLConfigDestroy(CarbonXSataLLConfig *config)
{
    DELETE(config);
    return SatallxTrue;
}

/* This function modifies i.e, set or unset the isPowerModeOn member of
 * transactor's configuration structure as indicated by the value given in
 * isOn */
void carbonXSataLLConfigSetPowerModeSupport(CarbonXSataLLConfig *config,
        CarbonUInt32 isOn)
{
    config->isPowerModeOn = isOn;
}

/* This routine checkes and return the power mode support status of
 * transactor. */
CarbonUInt32 carbonXSataLLConfigGetPowerModeSupport(
        const CarbonXSataLLConfig *config)
{
    return config->isPowerModeOn;
}

/* This routine modifies the Scrambling support status of transactor as
 * desired */
void carbonXSataLLConfigSetScramblingSupport(CarbonXSataLLConfig *config,
        CarbonUInt32 isOn)
{
    config->isScramblingOn = isOn;
}

/* This routine checkes and return the Scrambling support status of
 * transactor. */
CarbonUInt32 carbonXSataLLConfigGetScramblingSupport(
        const CarbonXSataLLConfig *config)
{
    return config->isScramblingOn;
}

/* This routine modifies the Contp Generation support status of transactor as
 * desired */
void carbonXSataLLConfigSetContSupport(CarbonXSataLLConfig *config,
        CarbonUInt32 isOn)
{
    config->isContpGen = isOn;
}

/* This routine checkes and return the Contp Generation support status of
 * transactor. */
CarbonUInt32 carbonXSataLLConfigGetContSupport(
        const CarbonXSataLLConfig *config)
{
    return config->isContpGen;
}

#endif
