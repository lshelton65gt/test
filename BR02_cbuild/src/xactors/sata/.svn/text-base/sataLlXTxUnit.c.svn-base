/*
 * FILE NAME : sataLlXTxUnit.c
 * 
 * PURPOSE:    This file defines the functions to create and update Tx unit.
 *             It describes how primitives and data are transmitted. 
 */

#include "sataLlXTxUnit.h"
#include "sataLlXUtil.h"
#ifndef NOCARBON
#include "sataLlXtor.h"
#endif

/* This routine creates the TxUnit.
 * Called: when Transactor is created. */
SatallxTxUnit *satallxCreateTxUnit(SatallXtor *xtor,
        SatallxFsmTxIntf *fsmIntf)
{
    SatallxTxUnit *txUnit = NEW(SatallxTxUnit);
    if (txUnit == NULL)
        return NULL;
    /* takes transactor reference */
    txUnit->xtor = xtor;
    if (fsmIntf == NULL)
        return NULL;
    /* connect fsm interface */
    txUnit->fsmIntf = fsmIntf;
#ifndef NOCARBON
    /* create physical layer interface */
    txUnit->phyIntf = satallxCreateTxPhyIntf(xtor->phyIntf);
#endif
    satallxResetTxUnit(txUnit);
    return txUnit;
}

/* This function frees the memory allocated for Tx unit structure */
void satallxDestroyTxUnit(SatallxTxUnit *txUnit)
{
#ifndef NOCARBON
    satallxDestroyTxPhyIntf(txUnit->phyIntf);
#endif
    DELETE(txUnit);
}

/* Resets TxUnit.
 * Called: When LRESET or ComWake comes */
void satallxResetTxUnit(SatallxTxUnit *txUnit)
{
    UInt i;
    txUnit->dataScramblerLfsr = satallxInitScramblerLfsr();
    txUnit->rdEncoder8To10b = 0; /* set rd- by default */
    txUnit->fsmIntf->isPrimitive = SatallxFalse;
    txUnit->fsmIntf->txPrimitive = SatallxPrim_UNDEF;
    txUnit->fsmIntf->txData = 0;
    txUnit->byteCount = 0;
    for (i = 0; i < 4; i++)
    {
        (txUnit->charArray[i]).isK = SatallxFalse;
        (txUnit->charArray[i]).data = 0;
    }
    /* Initialize scrambler lfsr for repetitive primitive suppression */
    txUnit->rpsScramblerLfsr = satallxInitScramblerLfsr();
    /* Initialize Primitive Queue for CONTp generation */
    txUnit->rpsQueue[0] = txUnit->rpsQueue[1] = SatallxPrim_UNDEF;
    txUnit->transmittedContp = SatallxFalse;
    /* Initialize Data to be transmitted after transmitting CONTp */ 
    txUnit->rpsData = (10 | (2 << 5)); /* D10.2 */
    txUnit->rpsCount = 0;
}

/* Updates TxUnit and sends 10 bit packet
 * Called: At each posedge of SystemClock (i.e. TxCLock) */
SatallxBool satallxUpdateTxUnit(SatallxTxUnit *txUnit, CarbonUInt64 currTime)
{
    SatallxCharacter charPacket;
    SatallxData10b outData10b;

    /* every 4th time the charArray is updated from FSM to TX Unit interface
     * which is sent to PHY in next 4 clocks */
    if (txUnit->byteCount == 0)
    {
        UInt i;
        if (txUnit->fsmIntf->isPrimitive) /* for primitive */
        {
            SatallxBool sendRpsData = SatallxFalse;
            if (txUnit->fsmIntf->txPrimitive == SatallxPrim_SOF)
            {
                /* On SOFp, reset the payload scrambler */
                txUnit->dataScramblerLfsr = satallxInitScramblerLfsr();
            }
            /* CONTp Generation */
#ifndef NOCARBON
            if (txUnit->xtor->config == NULL ||
                    txUnit->xtor->config->isContpGen)
#else
            if (1)
#endif
            {
                /* if last two primitives are same as current, send CONTp */
                if (!txUnit->fsmIntf->txStopCont &&
                        txUnit->fsmIntf->txPrimitive == txUnit->rpsQueue[0] &&
                        txUnit->rpsQueue[0] != SatallxPrim_UNDEF &&
                        txUnit->rpsQueue[0] == txUnit->rpsQueue[1])
                {
                    txUnit->fsmIntf->txPrimitive = SatallxPrim_CONT;
                }
                /* ALIGNp sequence can't be suppressed using CONTp */
                else if (txUnit->fsmIntf->txPrimitive == SatallxPrim_ALIGN)
                {
                    txUnit->rpsQueue[0] = txUnit->rpsQueue[1] =
                        SatallxPrim_UNDEF;
                }
                else /* update primitive queue for other primitives */
                {
                    txUnit->rpsQueue[1] = txUnit->rpsQueue[0];
                    txUnit->rpsQueue[0] = txUnit->fsmIntf->txPrimitive;
                }
            }

            /* RPS (Repetitive Primitive Suppression) data */
            if (txUnit->transmittedContp &&
                    txUnit->fsmIntf->txPrimitive == SatallxPrim_CONT)
            {
                txUnit->rpsCount = (txUnit->rpsCount + 1) % 256;
                /* In CONTp primitive suppression sequence, send
                 * ALIGNp/ALIGNp/PRIM/PRIM/CONTp at 256 DWORD interval */
                if (txUnit->rpsCount == 252 || txUnit->rpsCount == 253)
                    txUnit->fsmIntf->txPrimitive = SatallxPrim_ALIGN;
                else if (txUnit->rpsCount == 254 || txUnit->rpsCount == 255)
                    txUnit->fsmIntf->txPrimitive = txUnit->rpsQueue[0];
                else if (txUnit->rpsCount > 0)
                    sendRpsData = SatallxTrue;
            }
            else 
            {
                txUnit->transmittedContp =
                    (txUnit->fsmIntf->txPrimitive == SatallxPrim_CONT);
                txUnit->rpsCount = 0;
            }

            if (sendRpsData)
                /* if CONTp has already been sent once,
                 * send scrambled data instead of repeating CONTp */
            {
                SatallxDword fsmTxDword;
#ifndef NOCARBON
                if (txUnit->xtor->config == NULL ||
                    txUnit->xtor->config->isScramblingOn)
#else
                if (1)
#endif
                    /* scrambled dword using Lfsr and data corresponding to 
                     * repetitive primitive supression */
                    fsmTxDword = satallxScramble(&(txUnit->rpsScramblerLfsr),
                            txUnit->rpsData);
                else
                    /* If scrambling support is Off */
                    fsmTxDword = txUnit->rpsData;
                /* Organize the Dword into 4 SatallxCharacter */
                for (i = 0; i < 4; i++)
                {
                    SatallxByte txByte = fsmTxDword & 0xFF;
                    (txUnit->charArray[i]).isK = SatallxFalse;
                    (txUnit->charArray[i]).data = txByte;
                    fsmTxDword = fsmTxDword >> 8;
                }
            }
            else /* simple primitive */
            {
                /* Get 4 SatallxCharacter corresponding to a primitive */
                for (i = 0; i < 4; i++)
                {
                    SatallxCharacter txChar = satallxGetPrimByte(
                            txUnit->fsmIntf->txPrimitive, i);
                    (txUnit->charArray[i]).isK = txChar.isK;
                    (txUnit->charArray[i]).data = txChar.data;
                }
            }
        }
        else /* simple data */
        {
            /* Scrambling */
            SatallxDword fsmTxDword;
#ifndef NOCARBON
            if (txUnit->xtor->config == NULL ||
                    txUnit->xtor->config->isScramblingOn)
#else
            if (1)
#endif
            {
                fsmTxDword = satallxScramble(&(txUnit->dataScramblerLfsr),
                            txUnit->fsmIntf->txData);
            }
            else /* If scrambling support is configured to be OFF */
            {
                fsmTxDword = txUnit->fsmIntf->txData;
            }
            /* Get 4 SatallxCharacter corresponding to a Dword */
            for (i = 0; i < 4; i++)
            {
                SatallxByte txByte = fsmTxDword & 0xFF;
                (txUnit->charArray[i]).isK = SatallxFalse;
                (txUnit->charArray[i]).data = txByte;
                fsmTxDword = fsmTxDword >> 8;
            }
            /* reset primitive queue */
            txUnit->rpsQueue[0] = txUnit->rpsQueue[1] = SatallxPrim_UNDEF;
        }
    }
    /* character to be transmitted */
    charPacket = txUnit->charArray[txUnit->byteCount];
    /* 8/10b Encoding */
    if (!satallxEncode8To10b(&(txUnit->rdEncoder8To10b),
                charPacket, &outData10b))
        satallxError(txUnit->xtor, NULL, CarbonXSataLLError_InternalError,
                "**** INTERNAL ERROR ****");
    /* Deposit the encoded value in the currValue of DATAOUT SatallxPhyNet */
    satallxDepositData10b(txUnit->phyIntf->DATAOUT, outData10b);
    /* Check and deposit the value of the net COMMA */
    satallxDepositSignal(txUnit->phyIntf->COMMA,
            (SatallxBit)(satallxCheckForCommaChar(outData10b)?1:0));

    txUnit->byteCount = (txUnit->byteCount + 1) % 4;

    return SatallxTrue;

    currTime = 0; /* to remove compile time warning */
}

