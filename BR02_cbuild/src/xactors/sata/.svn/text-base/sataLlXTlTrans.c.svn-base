/*
 * FILENAME: sataLlXTlTrans.c
 * 
 * PURPOSE : This file contains the function definitions of the utilities
 *           that are required for encoding the SATA Transport Layer info
 *           into SATA Link Layer Frame Information Structure for transmission
 *           and extracting the same from the received SATA Link Layer Frame
 *           Information Structure.
 */

#ifndef NOCARBON

#include "sataLlXDefines.h"
#include "sataLlXTrans.h"
#include "xactors/sata/carbonXSataTLTrans.h"

/* Routine to get the type of the FIS from link layet transaction */
CarbonXSataTLTransType carbonXSataTLTransGetFisType(
        const CarbonXSataLLTrans *transLL)
{
    CarbonUInt32 *data;
    CarbonXSataTLTransType transType = CarbonXSataTLTrans_InvalidFis;

    /* check for frame type transaction.*/ 
    if (transLL == NULL ||
            carbonXSataLLTransGetType(transLL) != CarbonXSataLLTrans_Frame)
    return CarbonXSataTLTrans_InvalidFis;

    /* check if the LL transaction is empty */
    if (carbonXSataLLTransGetFrameSize(transLL) == 0)
        return CarbonXSataTLTrans_InvalidFis;

    data = carbonXSataLLTransGetFrameData(transLL);
    if (data == NULL)
        return CarbonXSataTLTrans_InvalidFis;

    /* extracting the information about type of the FIS */
    switch (*data & 0xFF)
    {
        case CarbonXSataTLTrans_RegHost2DevFis:
        {
            transType = CarbonXSataTLTrans_RegHost2DevFis;
            break;
        }
        case CarbonXSataTLTrans_RegDev2HostFis:
        {
            transType = CarbonXSataTLTrans_RegDev2HostFis;
            break;
        }
        case CarbonXSataTLTrans_SetDevBitsFis:
        {
            transType = CarbonXSataTLTrans_SetDevBitsFis;
            break;
        }
        case CarbonXSataTLTrans_DmaActFis:
        {
            transType = CarbonXSataTLTrans_DmaActFis;
        	break;
        }
        case CarbonXSataTLTrans_DmaSetupFis:
        {
            transType = CarbonXSataTLTrans_DmaSetupFis;
            break;
        }
        case CarbonXSataTLTrans_PioSetupFis:
        {
            transType = CarbonXSataTLTrans_PioSetupFis;
            break;
        }
        case CarbonXSataTLTrans_DataFis:
        {
            transType = CarbonXSataTLTrans_DataFis;
            break;
        }
        case CarbonXSataTLTrans_BistActFis:
        {
            transType = CarbonXSataTLTrans_BistActFis;
            break;
        }
        /* checking for reserved FIS types */
		case 0xA6:
		case 0xB8:
		case 0xBF:
		case 0xD9:
		{
			transType = CarbonXSataTLTrans_ReservedFis;
			break;
		}
			/* checking for vendor specific FIS types */
		case 0xC7:
		case 0xD4:
		{
			transType = CarbonXSataTLTrans_VendorSpecFis;
			break;
		}
		default:
		{
			transType = CarbonXSataTLTrans_InvalidFis;
			break;
		}
    }
    return transType;
}


/* Routine to create frame DWORD for link layer from transport layer FIS 
 * Register - Host to Device */
CarbonXSataLLTrans *carbonXSataTLTransCreateRegHost2DevFisLLFrame(
    const CarbonXSataTLTransRegHost2DevFis *transTL)
{
    SatallxTrans *transLL;
    if (transTL == NULL)
        return NULL;

    /* creating a LL transaction structure */
    transLL = carbonXSataLLTransCreate();
    if (transLL == NULL)
        return NULL;

    /* setting the LL transaction as Frame type */
    transLL->transType = CarbonXSataLLTrans_Frame;
    
	/* setting the protocol standard frame size of the FIS */
    transLL->frameSize = 5;
    
    /* Array for frameData of TL transaction is created */
	transLL->frameData = NEWARRAY(CarbonUInt32, transLL->frameSize);
    if (transLL->frameData == NULL)
        return NULL;

    /* Here the frame DWORDS for LL are constructed from the supplied TL
     * transaction structure elements.*/
    transLL->frameData[0] = (CarbonXSataTLTrans_RegHost2DevFis & 0xFF) |
    (transTL->PmPort & 0x0F) << 8 |
    (transTL->C & 0x01) << 15 |
    (transTL->Command & 0xFF) << 16 |
    (transTL->Features & 0xFF) << 24;
    transLL->frameData[1] = (transTL->LbaLow & 0xFF) |
    (transTL->LbaMid & 0xFF) << 8 |
    (transTL->LbaHigh & 0xFF) << 16 |
    (transTL->Device & 0xFF) << 24;
    transLL->frameData[2] = (transTL->LbaLowExp & 0xFF) |
    (transTL->LbaMidExp & 0xFF) << 8 |
    (transTL->LbaHighExp & 0xFF) << 16 |
    (transTL->FeaturesExp & 0xFF) << 24;
    transLL->frameData[3] = (transTL->SectorCount & 0xFF) |
    (transTL->SectorCountExp & 0xFF) << 8 |
    (transTL->Control & 0xFF) << 24;
    transLL->frameData[4] = 0x0;

    return transLL;
}

/* Routine to get Register - Host to Device FIS for transport layer 
 * from link layer frame transaction */
CarbonXSataTLTransRegHost2DevFis *carbonXSataTLTransGetRegHost2DevFis(
    const CarbonXSataLLTrans *transLL)
{
    CarbonXSataTLTransRegHost2DevFis *transTL;
    const SatallxTrans *trans = transLL;

    if (carbonXSataTLTransGetFisType(trans) !=
        CarbonXSataTLTrans_RegHost2DevFis)
        return NULL;

    /* checking for protocol specific frame size */
    if (trans->frameSize != 5)
        return NULL;

    /* creating a new TL transaction*/
    transTL = NEW(CarbonXSataTLTransRegHost2DevFis);
    
	if (transTL == NULL)
        return NULL;

    /* extracting the information from LL transaction packet*/
    transTL->PmPort = (trans->frameData[0] >> 8) & 0x0F;
    transTL->C = (trans->frameData[0] >> 15) & 0x01;
    transTL->Command = (trans->frameData[0] >> 16) & 0xFF;
    transTL->Features = (trans->frameData[0] >> 24) & 0xFF;

    transTL->LbaLow = trans->frameData[1] & 0xFF;
    transTL->LbaMid = (trans->frameData[1] >> 8) & 0xFF;
    transTL->LbaHigh = (trans->frameData[1] >> 16) & 0xFF;
    transTL->Device = (trans->frameData[1] >> 24) & 0xFF;

    transTL->LbaLowExp = trans->frameData[2] & 0xFF;
    transTL->LbaMidExp = (trans->frameData[2] >> 8) & 0xFF;
    transTL->LbaHighExp = (trans->frameData[2] >> 16) & 0xFF;
    transTL->FeaturesExp = (trans->frameData[2] >> 24) & 0xFF;

    transTL->SectorCount = trans->frameData[3] & 0xFF;
    transTL->SectorCountExp = (trans->frameData[3] >> 8) & 0xFF;
    transTL->Control = (trans->frameData[3] >> 24) & 0xFF;

    /* Although, the Register - Host to Device type LL Frame consists of five
     * DWORDs, the last one is reserved field having all zeros */
    return transTL;
}

/* Routine to create frame DWORD for link layer from transport layer FIS 
 * Register - Device to Host */
CarbonXSataLLTrans *carbonXSataTLTransCreateRegDev2HostFisLLFrame(
    const CarbonXSataTLTransRegDev2HostFis *transTL)
{
    SatallxTrans *transLL;
    if (transTL == NULL)
        return NULL;
    
	/* creating a LL transaction structure */
    transLL = carbonXSataLLTransCreate();
    if (transLL == NULL)
        return NULL;

    /* setting the LL transaction as Frame type */
    transLL->transType = CarbonXSataLLTrans_Frame;
    
	/* setting the protocol standard frame size of the FIS */
    transLL->frameSize = 5;
    
	/* Array for frameData of TL transaction is created */
    transLL->frameData = NEWARRAY(CarbonUInt32, transLL->frameSize);
    if (transLL->frameData == NULL)
        return NULL;

    /* Here the frame DWORDS for LL are constructed from the supplied TL
     * transaction structure elements.*/
    transLL->frameData[0] = (CarbonXSataTLTrans_RegDev2HostFis & 0xFF) |
    (transTL->PmPort & 0x0F) << 8 |
    (transTL->I & 0x01) << 14 |
    (transTL->Status & 0xFF) << 16 |
    (transTL->Error & 0xFF) << 24;
    
    transLL->frameData[1] = (transTL->LbaLow & 0xFF) |
    (transTL->LbaMid & 0xFF) << 8 |
    (transTL->LbaHigh & 0xFF) << 16 |
    (transTL->Device & 0xFF) << 24;
    
    transLL->frameData[2] = (transTL->LbaLowExp & 0xFF) |
    (transTL->LbaMidExp & 0xFF) << 8 |
    (transTL->LbaHighExp & 0xFF) << 16;
    
    transLL->frameData[3] = (transTL->SectorCount & 0xFF) |
    (transTL->SectorCountExp & 0xFF) << 8;
    
    transLL->frameData[4] = 0x0;

    return transLL;
}


/* Routine to get Register - Device to Host FIS for 
 * transport layer from link layer frame transaction */
CarbonXSataTLTransRegDev2HostFis *carbonXSataTLTransGetRegDev2HostFis(
    const CarbonXSataLLTrans *transLL)
{

    CarbonXSataTLTransRegDev2HostFis *transTL;
    const SatallxTrans *trans = transLL;

    /* checking for Register - Device to Host type FIS */
    if (carbonXSataTLTransGetFisType(trans) !=
        CarbonXSataTLTrans_RegDev2HostFis)
        return NULL;
    
    /* checking for protocol specific frame size */
    if (trans->frameSize != 5)
        return NULL;

    /* creating a new Register - Device to Host FIS type structure */
    transTL = NEW(CarbonXSataTLTransRegDev2HostFis);
    if (transTL == NULL)
        return NULL;

    /* extracting the information from LL transaction packet*/
    transTL->PmPort = (trans->frameData[0] >> 8) & 0x0F;
    transTL->I = (trans->frameData[0] >> 14) & 0x01;
    transTL->Status = (trans->frameData[0] >> 16) & 0xFF;
    transTL->Error = (trans->frameData[0] >> 24) & 0xFF;

    transTL->LbaLow = trans->frameData[1] & 0xFF;
    transTL->LbaMid = (trans->frameData[1] >> 8) & 0xFF;
    transTL->LbaHigh = (trans->frameData[1] >> 16) & 0xFF;
    transTL->Device = (trans->frameData[1] >> 24) & 0xFF;

    transTL->LbaLowExp = trans->frameData[2] & 0xFF;
    transTL->LbaMidExp = (trans->frameData[2] >> 8) & 0xFF;
    transTL->LbaHighExp = (trans->frameData[2] >> 16) & 0xFF;

    transTL->SectorCount = trans->frameData[3] & 0xFF;
    transTL->SectorCountExp = (trans->frameData[3] >> 8) & 0xFF;

    return transTL;
}

/* Routine to create frame DWORD for link layer from transport layer FIS 
 * Set Device Bits - Device to Host */
CarbonXSataLLTrans *carbonXSataTLTransCreateSetDevBitsFisLLFrame(
    const CarbonXSataTLTransSetDevBitsFis *transTL)
{
    SatallxTrans *transLL;
    if (transTL == NULL)
        return NULL;
    
	/* creating a LL transaction structure */
    transLL = carbonXSataLLTransCreate();
    if (transLL == NULL)
        return NULL;

    /* setting the LL transaction as Frame type */
    transLL->transType = CarbonXSataLLTrans_Frame;
    
	/* setting the protocol standard frame size of the FIS */
    transLL->frameSize = 2;
    
	/* Array for frameData of TL transaction is created */
    transLL->frameData = NEWARRAY(CarbonUInt32, transLL->frameSize);
    if (transLL->frameData == NULL)
        return NULL;

    /* Here the frame DWORDS for LL are constructed from the supplied TL
     * transaction structure elements. */
    transLL->frameData[0] = (CarbonXSataTLTrans_SetDevBitsFis & 0xFF) |
    (transTL->PmPort & 0x0F) << 8 |
    (transTL->I & 0x01) << 14 |
    (transTL->N & 0x01) << 15 |
    (transTL->StatusLo & 0x07) << 16 |
    (transTL->StatusHi & 0x07) << 20 |
    (transTL->Error & 0xFF) << 24;
    
    transLL->frameData[1] = 0x0;

    return transLL;
}


/* Routine to get Set Device Bits - Device to Host FIS for transport 
 * layer from link layer frame transaction */
CarbonXSataTLTransSetDevBitsFis *carbonXSataTLTransGetSetDevBitsFis(
    const CarbonXSataLLTrans *transLL)
{
    CarbonXSataTLTransSetDevBitsFis *transTL;
    const SatallxTrans *trans = transLL;

    /* checking for Set Device Bits - Device to Host type FIS */
    if (carbonXSataTLTransGetFisType(trans) !=
        CarbonXSataTLTrans_SetDevBitsFis)
        return NULL;

    /* checking for protocol specific frame size */
    if (trans->frameSize != 2)
        return NULL;

    /* creating a new Set Device Bits - Device to Host type structure */
    transTL = NEW(CarbonXSataTLTransSetDevBitsFis);
    if (transTL == NULL)
        return NULL;

    /* extracting the information from LL transaction packet*/
    transTL->PmPort = (trans->frameData[0] >> 8) & 0x0F;
    transTL->I = (trans->frameData[0] >> 14) & 0x01;
    transTL->N = (trans->frameData[0] >> 15) & 0x01;
    transTL->StatusLo = (trans->frameData[0] >> 16) & 0x07;
    transTL->StatusHi = (trans->frameData[0] >> 20) & 0x07;
    transTL->Error = (trans->frameData[0] >> 24) & 0xFF;

    return transTL;
}

/* Routine to create frame DWORD for link layer from transport layer FIS 
 * DMA Activate - Device to Host */
CarbonXSataLLTrans *carbonXSataTLTransCreateDmaActFisLLFrame(
    const CarbonXSataTLTransDmaActFis *transTL)
{
    SatallxTrans *transLL;
    if (transTL == NULL)
        return NULL;
    
	/* creating a LL transaction structure */
    transLL = carbonXSataLLTransCreate();
    if (transLL == NULL)
        return NULL;

    /* setting the LL transaction as Frame type */
    transLL->transType = CarbonXSataLLTrans_Frame;
    
	/* setting the protocol standard frame size of the FIS */
    transLL->frameSize = 1;
    
    /* Array for frameData of TL transaction is created */
    transLL->frameData = NEWARRAY(CarbonUInt32, transLL->frameSize);
    if (transLL->frameData == NULL)
        return NULL;

    /* Here the frame DWORD is constructed from the supplied TL
     * transaction structure elements.*/
    transLL->frameData[0] = (CarbonXSataTLTrans_DmaActFis & 0xFF) |
    (transTL->PmPort & 0x0F) << 8;

    return transLL;
}


/* Routine to get DMA Activate - Device to Host  FIS for transport layer 
 * from link layer frame transaction */
CarbonXSataTLTransDmaActFis *carbonXSataTLTransGetDmaActFis(
    const CarbonXSataLLTrans *transLL)
{
    CarbonXSataTLTransDmaActFis *transTL;
    const SatallxTrans *trans = transLL;

    /* checking for DMA Activate - Device to Host type FIS */
    if (carbonXSataTLTransGetFisType(trans) !=
        CarbonXSataTLTrans_DmaActFis)
        return NULL;

    /* checking for protocol specific frame size */
    if (trans->frameSize != 1)
        return NULL;

    /* creating a new DMA Activate - Device to Host  FIS type structure */
    transTL = NEW(CarbonXSataTLTransDmaActFis);
    if (transTL == NULL)
        return NULL;

    /* extracting the information from LL transaction packet*/
    transTL->PmPort = (trans->frameData[0] >> 8) & 0x0F;

    return transTL;
}

/* Routine to create frame DWORD for link layer from transport layer FIS 
 * DMA Setup - Bidirectional */
CarbonXSataLLTrans *carbonXSataTLTransCreateDmaDmaSetupFisLLFrame(
    const CarbonXSataTLTransDmaSetupFis *transTL)
{
    SatallxTrans *transLL;
    if (transTL == NULL)
        return NULL;
	
    /* creating a LL transaction structure */
    transLL = carbonXSataLLTransCreate();
    if (transLL == NULL)
        return NULL;

    /* setting the LL transaction as Frame type */
    transLL->transType = CarbonXSataLLTrans_Frame;
    
	/* setting the protocol standard frame size of the FIS */
    transLL->frameSize = 7;
    
    /* Array for frameData of TL transaction is created */
    transLL->frameData = NEWARRAY(CarbonUInt32, transLL->frameSize);
    if (transLL->frameData == NULL)
        return NULL;
	
    /* Here the frame DWORDS for LL are constructed from the supplied TL
     * transaction structure elements.*/
    transLL->frameData[0] = (CarbonXSataTLTrans_DmaSetupFis & 0xFF) |
    (transTL->PmPort & 0x0F) << 8 |
    (transTL->D & 0x01) << 13 |
    (transTL->I & 0x01) << 14 |
    (transTL->A & 0x01) << 15;
    
    transLL->frameData[1] = transTL->DmaBufIdLow;
    transLL->frameData[2] = transTL->DmaBufIdHigh;
    transLL->frameData[3] = 0x0;
    transLL->frameData[4] = transTL->DmaBufOffset;
    transLL->frameData[5] = transTL->DmaTransCount;
    transLL->frameData[6] = 0x0;

    return transLL;
}


/* Routine to get DMA Setup - Bidirectional FIS for transport layer from 
 * link layer frame transaction */
CarbonXSataTLTransDmaSetupFis *carbonXSataTLTransGetDmaSetupFis(
    const CarbonXSataLLTrans *transLL)
{
    CarbonXSataTLTransDmaSetupFis *transTL;
    const SatallxTrans *trans = transLL;

    /* checking for DMA Setup - Bidirectional type FIS */
    if (carbonXSataTLTransGetFisType(trans) !=
        CarbonXSataTLTrans_DmaSetupFis)
        return NULL;

    /* checking for protocol specific frame size */
    if (trans->frameSize != 7)
        return NULL;

    /* creating a new DMA Activate - Device to Host  FIS type structure */
    transTL = NEW(CarbonXSataTLTransDmaSetupFis);
    if (transTL == NULL)
        return NULL;

    /* extracting the information from LL transaction packet*/
    transTL->PmPort = (trans->frameData[0] >> 8) & 0x0F;
    transTL->D = (trans->frameData[0] >> 13) & 0x01;
    transTL->I = (trans->frameData[0] >> 14) & 0x01;
    transTL->A = (trans->frameData[0] >> 15) & 0x01;
    transTL->DmaBufIdLow = trans->frameData[1];
    transTL->DmaBufIdHigh = trans->frameData[2];
    transTL->DmaBufOffset = trans->frameData[4];
    transTL->DmaTransCount = trans->frameData[5];

    return transTL;
}

/* Routine to create frame DWORD for link layer from transport layer FIS 
 * PIO Setup - Device to Host */
CarbonXSataLLTrans *carbonXSataTLTransCreatePioSetupFisLLFrame(
    const CarbonXSataTLTransPioSetupFis *transTL)
{
    SatallxTrans *transLL;
    /* checking for empty TL transaction structure */
    if (transTL == NULL)
        return NULL;
    /* creating a LL transaction structure */
    transLL = carbonXSataLLTransCreate();
    if (transLL == NULL)
        return NULL;

    /* setting the LL transaction as Frame type */
    transLL->transType = CarbonXSataLLTrans_Frame;
    /* setting the protocol standard frame size of the FIS */
    transLL->frameSize = 5;
    
	/* Array for frameData of TL transaction is created */
    transLL->frameData = NEWARRAY(CarbonUInt32, transLL->frameSize);
    if (transLL->frameData == NULL)
        return NULL;

    /* Here the frame DWORDS for LL are constructed from the supplied TL
     * transaction structure elements.*/
    transLL->frameData[0] = (CarbonXSataTLTrans_PioSetupFis & 0xFF) |
    (transTL->PmPort & 0x0F) << 8 |
    (transTL->D & 0x01) << 13 |
    (transTL->I & 0x01) << 14 |
    (transTL->Status & 0xFF) << 16 |
    (transTL->Error & 0xFF) << 24;
    
    transLL->frameData[1] = (transTL->LbaLow & 0xFF) |
    (transTL->LbaMid & 0xFF) << 8 |
    (transTL->LbaHigh & 0xFF) << 16 |
    (transTL->Device & 0xFF) << 24;
    
    transLL->frameData[2] = (transTL->LbaLowExp & 0xFF) |
    (transTL->LbaMidExp & 0xFF) << 8 |
    (transTL->LbaHighExp & 0xFF) << 16;
    
    transLL->frameData[3] = (transTL->SectorCount & 0xFF) |
    (transTL->SectorCountExp & 0xFF) << 8 |
    (transTL->E_Status & 0xFF) << 24;
    
    transLL->frameData[4] = (transTL->TransCount & 0xFFFF);

    return transLL;
}


/* Routine to get PIO Setup - Device to Host FIS for transport layer 
 * from link layer frame transaction */
CarbonXSataTLTransPioSetupFis *carbonXSataTLTransGetPioSetupFis(
    const CarbonXSataLLTrans *transLL)
{
    CarbonXSataTLTransPioSetupFis *transTL;
    const SatallxTrans *trans = transLL;

    /* checking for PIO Setup - Device to Host FIS */
    if (carbonXSataTLTransGetFisType(trans) !=
        CarbonXSataTLTrans_PioSetupFis)
        return NULL;

    /* checking for protocol specific frame size */ 
    if (trans->frameSize != 5)
        return NULL;

    /* creating a new PioSetupFis type structure */
    transTL = NEW(CarbonXSataTLTransPioSetupFis);
    if (transTL == NULL)
        return NULL;

    /* extracting the information from LL transaction packet*/
    transTL->PmPort = (trans->frameData[0] >> 8) & 0x0F;
    transTL->D = (trans->frameData[0] >> 13) & 0x01;
    transTL->I = (trans->frameData[0] >> 14) & 0x01;
    transTL->Status = (trans->frameData[0] >> 16) & 0xFF;
    transTL->Error = (trans->frameData[0] >> 24) & 0xFF;

    transTL->LbaLow = trans->frameData[1] & 0xFF;
    transTL->LbaMid = (trans->frameData[1] >> 8) & 0xFF;
    transTL->LbaHigh = (trans->frameData[1] >> 16) & 0xFF;
    transTL->Device = (trans->frameData[1] >> 24) & 0xFF;

    transTL->LbaLowExp = trans->frameData[2] & 0xFF;
    transTL->LbaMidExp = (trans->frameData[2] >> 8) & 0xFF;
    transTL->LbaHighExp = (trans->frameData[2] >> 16) & 0xFF;

    transTL->SectorCount = trans->frameData[3] & 0xFF;
    transTL->SectorCountExp = (trans->frameData[3] >> 8) & 0xFF;
    transTL->E_Status = (trans->frameData[3] >> 24) & 0xFF;

    transTL->TransCount = trans->frameData[4] & 0xFFFF;

    return transTL;
}

/* Routin to create data frame (DWORDS) for link layer from transport 
 * layer data FIS */
CarbonXSataLLTrans *carbonXSataTLTransCreateDataFisLLFrame(
    const CarbonXSataTLTransDataFis *transTL)
{
    UInt i;
    SatallxTrans *transLL;
    if (transTL == NULL)
        return NULL;
    
	/* creating a LL transaction structure */
    transLL = carbonXSataLLTransCreate();
    if (transLL == NULL)
        return NULL;

    /* setting the LL transaction as Frame type */
    transLL->transType = CarbonXSataLLTrans_Frame;
    
     /* Maximum size for data payload is 2048 DWORDs (protocol specified) */
    transLL->frameSize = transTL->dataSize + 1;/* data from LL and FIS header */
	
    transLL->frameData = NEWARRAY(CarbonUInt32, transLL->frameSize);
    if (transLL->frameData == NULL)
        return NULL;

    /* Here the frame DWORDS for LL are constructed from the supplied TL
     * transaction structure elements.*/
    transLL->frameData[0] = (CarbonXSataTLTrans_DataFis & 0xFF) |
    (transTL->PmPort & 0x0F) << 8;
    
	/* copying the data DWORDs */
    for (i = 0; i < transTL->dataSize; i++)
        transLL->frameData[i + 1] = transTL->data[i];

    return transLL;
}


/* Routine to get data FIS for transport layer from link layer 
 * frame transaction */
CarbonXSataTLTransDataFis *carbonXSataTLTransGetDataFis(
    const CarbonXSataLLTrans *transLL)
{
    UInt i;
    CarbonXSataTLTransDataFis *transTL;
    const SatallxTrans *trans = transLL;

    /* checking for Data FIS */
    if (carbonXSataTLTransGetFisType(trans) != CarbonXSataTLTrans_DataFis)
        return NULL;

    /* checking for protocol specific frame size */
    if (trans->frameSize < 2)
        return NULL;

    /* creating a new "CarbonXSataTLTransDataFis" type structure */
    transTL = NEW(CarbonXSataTLTransDataFis);
    if (transTL == NULL)
        return NULL;

    /* extracting the information from LL transaction packet*/
    transTL->PmPort = (trans->frameData[0] >> 8) & 0x0F;
    transTL->dataSize = trans->frameSize - 1;
    
    /* creating a new array to hold the received data DWORDs */
    transTL->data = NEWARRAY(CarbonUInt32, transTL->dataSize);
    if (transTL->data == NULL)
        return NULL;
    
    /* copying the data received DWORDs into the new array */
    for (i = 0; i < transTL->dataSize; i++)
        transTL->data[i] = trans->frameData[i + 1];

    return transTL;
}

#endif
