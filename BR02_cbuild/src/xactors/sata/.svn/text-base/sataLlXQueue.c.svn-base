/*
 *    FILE NAME : sataLlXQueue.c 
 *
 *    PURPOSE   : This file contains the implementation of all 
 *                the operation done on Queue.
 */

#include "sataLlXQueue.h"

/* Queue implementation:
 *
 *        +--+prev +--+prev +--+prev +--+
 *        |  |---->|  |---->|  |---->|  |------+
 *    +---|  |<----|  |<----|  |<----|  |      |
 *    |   +--+ next+--+ next+--+ next+--+      V
 *    V    ^                           ^
 *         | Tail                      | Head
 *
 *    Push ------>                     -----> Pop
 */

/* Creates a queue of  specified size */
SatallxQueue *satallxCreateQueue(UInt size)        
{
    SatallxQueue *queue = NEW(SatallxQueue); 
    if (queue == NULL)
        return NULL;
    queue->count = 0;
    queue->size = size;
    queue->head = queue->tail = NULL; 
    return queue;
}

/* Destroys the Queue */
void satallxDestroyQueue(SatallxQueue *queue)
{
    /* free up all nodes */
    while (queue->head != NULL)
    {
        SatallxQueueItem *nextHead = queue->head->next;
        DELETE(queue->head);
        queue->head = nextHead;
    }
    DELETE(queue);
}

/* Returns the size of the queue */
UInt satallxQueueGetSize(SatallxQueue *queue)
{
    return queue->size;
}

/* Sets the size of the queue as specified */
void satallxQueueSetSize(SatallxQueue *queue, UInt size)
{
    queue->size = size;
}

/* Returns the number of item in the queue */
UInt satallxQueueGetCount(SatallxQueue *queue)
{
    return queue->count;
}

/* Pushes a node in the queue */
SatallxBool satallxQueuePush(SatallxQueue *queue, void *data)
{
    SatallxQueueItem *item;
    if (queue->count >= queue->size) /* if no more items allowed */
        return SatallxFalse;
    
    /* create the node */
    item = NEW(SatallxQueueItem);
    if (item == NULL)
        return SatallxFalse;
    
    item->data = data;            
    item->next = NULL;
    if (queue->count == 0) /* when queue is empty */
    {
        item->prev = NULL;
        queue->head = queue->tail = item;  
    }
    else /* queue has some items in it */
    {
        /* items are always pushed to the tail of the queue */
        item->prev = queue->tail;
        queue->tail->next = item;
        queue->tail = item;
    }
    queue->count++;
    
    return SatallxTrue;
}

/* Pops a item from the queue, poped item is returned  */
void *satallxQueuePop(SatallxQueue *queue)
{
    SatallxQueueItem *item;
    void *data;
    
    if (queue->count == 0) /* queue is empty */
        return NULL;        
    
    /* items are always poped from the head of the queue  */
    item = queue->head;
    queue->count--;
    
    if (queue->count == 0) /* if queue becomes empty after pop */
    {
        queue->head = queue->tail = NULL;
    }
    else /* if queue has some item after pop */
    {
        queue->head = item->next;
        queue->head->prev = NULL;
    }
    /* retrieve the data and free the memory for queue node */
    data = item->data;
    DELETE(item);
    return data;
}

/* Gets the top data of queue without popping */
void *satallxQueueTop(SatallxQueue *queue)
{
    SatallxQueueItem *item;
    if (queue->count == 0) /* queue is empty */
        return NULL;
    /* get the data corresponding to head */
    item = queue->head;    
    return item->data;
}

