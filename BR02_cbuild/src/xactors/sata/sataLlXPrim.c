/*
  FILE NAME: sataLlXPrim.c 
  
  PURPOSE:   This file is used to get the bytes of primitive or to find the
             primitive type or name 
*/ 

#include "sataLlXPrim.h"
#define D(xx,y) (y<<5|xx)&0xFF

/* Primitive to character mapping table */
#define D(xx,y) (y<<5|xx)&0xFF
static const SatallxByte satallxgPrimEncoding[][4] = {
    {       0,       0,       0,       0 }, /* UNDEF   */
    { D(28,5), D(10,2), D(10,2), D(27,3) }, /* ALIGN   */
    { D(28,3), D(10,5), D(25,4), D(25,4) }, /* CONT    */
    { D(28,3), D(21,5), D(22,1), D(22,1) }, /* DMAT    */
    { D(28,3), D(21,5), D(21,6), D(21,6) }, /* EOF     */
    { D(28,3), D(10,5), D(21,6), D(21,6) }, /* HOLD    */
    { D(28,3), D(10,5), D(21,4), D(21,4) }, /* HOLDA   */
    { D(28,3), D(21,4), D(21,4), D(21,4) }, /* PMACK   */
    { D(28,3), D(21,4), D(21,7), D(21,7) }, /* PMNAK   */
    { D(28,3), D(21,5), D(23,0), D(23,0) }, /* PMREQ_P */
    { D(28,3), D(21,4), D(21,3), D(21,3) }, /* PMREQ_S */
    { D(28,3), D(21,5), D(22,2), D(22,2) }, /* R_ERR   */
    { D(28,3), D(21,5), D(21,2), D(21,2) }, /* R_IP    */
    { D(28,3), D(21,5), D(21,1), D(21,1) }, /* R_OK    */
    { D(28,3), D(21,4), D(10,2), D(10,2) }, /* R_RDY   */
    { D(28,3), D(21,5), D(23,1), D(23,1) }, /* SOF     */
    { D(28,3), D(21,4), D(21,5), D(21,5) }, /* SYNC    */
    { D(28,3), D(21,5), D(24,2), D(24,2) }, /* WTRM    */
    { D(28,3), D(21,5), D(23,2), D(23,2) }  /* X_RDY   */
};
#undef D

/* Returns the indexed byte of a primitive */
SatallxCharacter satallxGetPrimByte(SatallxPrimType prim, UInt index)
{
    SatallxCharacter primChar;
    primChar.isK = (index == 0); /* 0 th character is control */
    primChar.data = satallxgPrimEncoding[prim][index];
    return primChar;
}

/* Checks a char array to be primitive and returns its type */
SatallxPrimType satallxFindPrim(SatallxCharacter charArray[])
{
    SatallxPrimType prim;
    /* 0 th byte is control char */
    if (!charArray[0].isK || charArray[1].isK ||
            charArray[2].isK || charArray[3].isK)
        return SatallxPrim_UNDEF;
    if (charArray[0].data == satallxgPrimEncoding[SatallxPrim_ALIGN][0])
    {
        /* check for ALIGNp */
        if (charArray[1].data != charArray[2].data)
            return SatallxPrim_UNDEF;
        if (charArray[1].data == satallxgPrimEncoding[SatallxPrim_ALIGN][1] &&
                charArray[3].data == satallxgPrimEncoding[SatallxPrim_ALIGN][3])
            return SatallxPrim_ALIGN;
    }
    else if (charArray[0].data == satallxgPrimEncoding[SatallxPrim_CONT][0])
    {
        /* check for CONTp and other primitives. Note, 0 th value for
         * all primitives except ALIGNp are same as K28.3.  */
        if (charArray[2].data != charArray[3].data)
            return SatallxPrim_UNDEF;
        for (prim = SatallxPrim_CONT; prim <= SatallxPrim_X_RDY;
                prim = (SatallxPrimType) ((UInt) prim + 1))
        {
            if (charArray[1].data == satallxgPrimEncoding[prim][1] &&
                    charArray[2].data == satallxgPrimEncoding[prim][2])
                return prim;
        }
    }
    return SatallxPrim_UNDEF;
}

/* This function takes the type of primitive and returns the name of it */
char *satallxGetPrimName(SatallxPrimType prim)
{
    static char *primNameArray[] = {
        "UNDEF",
        "ALIGN",
        "CONT",
        "DMAT",
        "EOF",
        "HOLD",
        "HOLDA",
        "PMACK",
        "PMNAK",
        "PMREQ_P",
        "PMREQ_S",
        "R_ERR",
        "R_IP",
        "R_OK",
        "R_RDY",
        "SOF",
        "SYNC",
        "WTRM",
        "X_RDY"
    };
    return primNameArray[prim];
}
