/***************************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// Didn't know how to pick a unique protocol id for Carbon's transactors
// 0x153f was picked by adding the ascii values in the string
// "Carbon Design Systems Inc., 375 Totten Pond Road, Waltham, MA 02451"
#define CARBON_SOCVSP_PROTID_BASE 0x153f0000

#define CARBON_SOCVSP_PROTID_PCIM CARBON_SOCVSP_PROTID_BASE+1
#define CARBON_SOCVSP_PROTID_PCIS CARBON_SOCVSP_PROTID_BASE+2
#define CARBON_SOCVSP_PROTID_PCIE CARBON_SOCVSP_PROTID_BASE+3
