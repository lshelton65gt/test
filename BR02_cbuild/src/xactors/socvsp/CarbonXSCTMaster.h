// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxsctmaster__
#define __carbonxsctmaster__

#include "maxsim.h"
#include "xactors/systemc/CarbonXIfs.h"
#include "xactors/socvsp/CarbonXTransInfo.h"

//! Generic Blocking Master Port For Carbon Transactors
/*!
  This SoC Designer Transaction Master Port can be used
  in a user written Component to execute generic transactions on 
  Carbon's transactors. The port is using SystemC Waits to block
  and wait for transactions to be done. Therefor the methods in 
  this class has to be called from an SC_THREAD in the users component.
  An SC_THREAD can be started by calling SC_THREAD(<user_method>);
  in the init method of the users component.

  You should instantiate this class instead of the MxTransactionMasterPort
  that should be connected to the corresponding transaction slave port 
  of the Carbon Generated Component.

  You have to call the communicate and update methods in the port
  in the the respective methods in your component.
*/
class CarbonXSCTMaster : public MxTransactionMasterPort {
public:
  
  CarbonXSCTMaster(sc_mx_m_base *owner, const string& portName) :  
    MxTransactionMasterPort(owner, portName, NULL),
    mStartTransFlag(0),
    mTransInfo(0),
    mStatus(0) {
  }
  
  //! Return the status field of the transaction structure of last transaction received transaction 
  UInt32 getStatus() {
    return mStatus;
  }
  
  //! Perform a Configuration Space write to the TRANSACTOR
  /*! This is for configuration of the transactor itself, not of the DUT
   */
  void csWrite(UInt32 address, UInt32 data) {
    mReq.csWrite(address, data); _put(); _block(); _cleanup();
  }
  
  //! Perform a Configuration Space read from the TRANSACTOR
  /*! This is for configuration of the transactor itself, not of the DUT
   */
  UInt32 csRead(UInt64 address) {
    mReq.csReadReq(address); 
    _put();
    _block();
    UInt32 result = mTransInfo->read32();
    _cleanup();
    return result;
  }

  //! Perform a blocking 8 bit generic write to the given address
  /*!
    \param address Address for the transaction
    \param data Data to be written
  */
  void write8(UInt64 address, const UInt8 data) {
    mReq.write8(address, data); _put(); _block(); _cleanup();  
  }

  //! Perform a blocking 16 bit generic write to the given address
  /*!
    \param address Address for the transaction
    \param data Data to be written
  */
  void write16(UInt64 address, const UInt16 data) {
    mReq.write16(address, data); _put(); _block(); _cleanup();
  }

  //! Perform a blocking 32 bit generic write to the given address
  /*!
    \param address Address for the transaction
    \param data Data to be written
  */
  void write32(UInt64 address, const UInt32 data) {
    mReq.write32(address, data); _put(); _block(); _cleanup();       
  }

  //! Perform a blocking 64 bit generic write to the given address
  /*!
    \param address Address for the transaction
    \param data Data to be written
  */
  void write64(UInt64 address, const UInt64 data) {
    mReq.write64(address, data); _put(); _block(); _cleanup();      
  }
 
  //! Perform a blocking generic write of an array of 8 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 8 bit values to be written
  */
  void writeArray8(UInt64 address, const UInt8 *data, UInt32 size) {
    mReq.writeArray8(address, data, size); _put(); _block(); _cleanup();       
  }

  //! Perform a blocking generic write of an array of 16 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 16 bit values to be written
  */
  void writeArray16(UInt64 address, const UInt16 *data, UInt32 size) {
    mReq.writeArray16(address, data, size); _put(); _block(); _cleanup();       
  }

  //! Perform a blocking generic write of an array of 32 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 32 bit values to be written
  */
  void writeArray32(UInt64 address, const UInt32 *data, UInt32 size) {
    mReq.writeArray32(address, data, size); _put(); _block(); _cleanup();       
  }

  //! Perform a blocking generic write of an array of 64 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 64 bit values to be written
  */
  void writeArray64(UInt64 address, const UInt64 *data, UInt32 size) {
    mReq.writeArray64(address, data, size); _block(); _cleanup();_put();        
  }

  //! Perform a blocking 8 bit generic read from the given address
  /*!
    \param address Address for the transaction
    \return Data read
  */
  UInt8 read8(UInt64 address) {
    mReq.read8Req(address); 
    _put();
    _block();
    UInt8 result = mTransInfo->read8();
    _cleanup();
    return result;
  }

  //! Perform a blocking 16 bit generic read from the given address
  /*!
    \param address Address for the transaction
    \return Data read
  */
  UInt16 read16(UInt64 address) {
    mReq.read16Req(address); 
    _put();
    _block();
    UInt16 result = mTransInfo->read16();
    _cleanup();
    return result;
  }

  //! Perform a blocking 32 bit generic read from the given address
  /*!
    \param address Address for the transaction
    \return Data read
  */
  UInt32 read32(UInt64 address) {
    mReq.read32Req(address); 
    _put();
    _block();
    UInt32 result = mTransInfo->read32();
    _cleanup();
    return result;
  }

  //! Perform a blocking 64 bit generic read from the given address
  /*!
    \param address Address for the transaction
    \return Data read
  */
  UInt64 read64(UInt64 address) {
    mReq.read64Req(address); 
    _put();
    _block();
    UInt64 result = mTransInfo->read64();
    _cleanup();
    return result;
  }

  //! Perform a blocking generic read of an array of 8 bit values from the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array where read data should be put
    \param size Number of 8 bit values to be read
  */
  UInt32 readArray8(UInt64 address, UInt8 *data, UInt32 size) {
    mReq.readArray8Req(address, size);
    _put();
    _block();
    UInt32 read_size = mTransInfo->readArray8(data);
    _cleanup();
    return read_size;
  }

  //! Perform a blocking generic read of an array of 16 bit values from the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array where read data should be put
    \param size Number of 16 bit values to be read
  */
  UInt32 readArray16(UInt64 address, UInt16 *data, UInt32 size) {
    mReq.readArray16Req(address, size);
    _put();
    _block();
    UInt32 read_size = mTransInfo->readArray16(data);
    _cleanup();
    return read_size;
  }

  //! Perform a blocking generic read of an array of 32 bit values from the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array where read data should be put
    \param size Number of 32 bit values to be read
  */
  UInt32 readArray32(UInt64 address, UInt32 *data, UInt32 size) {
    mReq.readArray32Req(address, size);
    _put();
    _block();
    UInt32 read_size = mTransInfo->readArray32(data);
    _cleanup();
    return read_size;
  }

  //! Perform a blocking generic read of an array of 64 bit values from the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array where read data should be put
    \param size Number of 64 bit values to be read
  */
  UInt32 readArray64(UInt64 address, UInt64 *data, UInt32 size) {
    mReq.readArray64Req(address, size);
    _put();
    _block();
    UInt32 read_size = mTransInfo->readArray64(data);
    _cleanup();
    return read_size;
  }

  //! Communicate Mehtod
  /*!
    Should be called by the communicate method of the users component
  */
  void communicate(void) {
    if (mStartTransFlag == 1) {
      mStartTransFlag = 0;
      if(mTransInfo->dataWr) cout << "Goran: Driving Transaction: Data = " << hex << mTransInfo->dataWr[0] << endl;
      driveTransaction(mTransInfo);
    }
  }

  //! Update Mehtod
  /*!
    Should be called by the update method of the users component
  */
  void update(void) {
    if(mTransInfo) {
      if ((mTransInfo->cts == CARBONX_SOCVSP_CTS_DONE) || (mTransInfo->cts == CARBONX_SOCVSP_CTS_CANCEL)) {
	mTransDoneEvent.notify(SC_ZERO_TIME);
      }
    }
  }

protected:
  
  //! Flag to tell the communicate method that there is a transaction to be driven to the slace 
  bool                mStartTransFlag;

  //! SystemC event used to block the users SystemC thread while the transaction is running
  sc_event            mTransDoneEvent;

  //! Pointer to the SoC Deisgner Transaction Structure
  CarbonXTransInfo *  mTransInfo;
  
  //! Carbon's Transaction Structure
  CarbonXTransReqT    mReq;
  
  //! Return Status of the last transaction
  //! Valid values are dependent on the transactor we are talking to inside the Carbon Model
  UInt32              mStatus;

  //! Help Method to create a SoC Designer transaction from a Carbon transaction
  // and send to tell the communicate method to send it.
  inline void _put()   { 
    mTransInfo = new CarbonXTransInfo(this, mReq);
    mStartTransFlag = 1;
  }
  
  //! Help method to block until transaction is done and store the return status
  void _block() {
    wait(mTransDoneEvent);
    mStatus = mTransInfo->slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_STATUS];
  }

  //! Help method to clean up after transaction is done
  inline void _cleanup() {
    delete mTransInfo;
    mTransInfo = 0;
  }

};

#endif
