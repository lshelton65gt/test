// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#ifndef CarbonXT2T__H
#define CarbonXT2T__H

#include "carbon/CarbonPlatform.h"
#include "maxsim.h"
#include "xactors/carbonX.h"
#include "xactors/systemc/CarbonXIfs.h"
#include <stdarg.h>
#include <list>
#include <string>

#include "xactors/socvsp/CarbonXTransInfo.h"
#include "xactors/enet_mii/carbonXEnet.h"
#include "xactors/pci/carbonXPciMaster.h"
#include "xactors/pci/carbonXPciSlave.h"
#include "xactors/ahb/carbonXAhbMaster.h"
#include "xactors/ahb/carbonXAhbSlave.h"

typedef enum CarbonXPortRegisterT {
  eCarbonXSlave,
  eCarbonXMaster,
  eCarbonXCMaster,
  eIllegal
};


class CarbonXResetObj;

class CarbonXT2T :
  public virtual sc_mx_transaction_slave
{
public:

  // constructor / destructor
  CarbonXT2T(sc_mx_module*        carbonComp, 
	     const char*          xtorName,
	     CarbonObjectID**     carbonObj,
	     CarbonXPortRegisterT portsToRegister,
             CarbonXResetObj*     resetObj);

  void registerPort(sc_mx_p_base* port, const string& name);
  
  virtual ~CarbonXT2T();
  
  // overloaded methods for clocked components
  void communicate();
  void update();
  
  // overloaded sc_mx_module methods
  virtual void setParameter(const string &name, const string &value);
  virtual void init();
  virtual void terminate();
  virtual void reset(MxResetLevel level, const MxFileMapIF *filelist);
    
  //////////////////////////////////////////////////
  //                                              //
  // Maxsim Slave Transactor Interface            //
  //                                              //
  //////////////////////////////////////////////////
  
  virtual MxStatus read(MxU64 addr, MxU32* value, MxU32* ctrl);
  virtual MxStatus write(MxU64 addr, MxU32* value, MxU32* ctrl);
  virtual void driveTransaction(MxTransactionInfo* info);
  virtual void cancelTransaction(MxTransactionInfo* info);
  virtual MxStatus debugTransaction(MxTransactionInfo* info);
  
  void queueCsWrite(CarbonUInt32 addr, CarbonUInt32 data);

private:
  
  // Utility Methods
  void setupPortProperties(MxTransactionProperties* prop);

  ///////////////////////////////////////////////////
  //                                               //
  // CarbonX Transactor Request/Response Interface //
  //                                               //
  ///////////////////////////////////////////////////

  void setRequest(const carbonXTransactionStruct* req);
  void getRequest(carbonXTransactionStruct* req);
  void setResponse(const carbonXTransactionStruct* req);
  void getResponse(carbonXTransactionStruct* req);
  bool getCSRequest(carbonXTransactionStruct* req);
  void defaultResponse(carbonXTransactionStruct* resp);
  
  ///////////////////////////////////////////////////
  //                                               //
  // CarbonX Transactor Transaction Loops          //
  //                                               //
  ///////////////////////////////////////////////////

  static void vspx_func(CarbonXT2T* this_obj);
  void slaveTransactionLoop();
  void masterTransactionLoop();
  void cMasterTransactionLoop();

public:  
  // Master Port
  sc_port<sc_mx_transaction_if>*  mCarbonXTMaster;
  
  //private:
  
  std::string                          mXtorName;
  std::string                          mXtorPath;
  std::string                          mTMasterName;
  std::string                          mTSlaveName;
  sc_mx_module*                        mCarbonComponent;
  CarbonObjectID**                     mpCarbonObject;
  CarbonXPortRegisterT                 mPortType;
  CarbonUInt32                         mTransId;

  // Carbon Transaction Interface Member Variables
  std::list<CarbonXTransInfo*>         mTxQueue;
  std::list<CarbonXTransInfo*>         mRxQueue;
  std::list<carbonXTransactionStruct>  mCsQueue;
  std::list<carbonXTransactionStruct>  mRespQueue;
  CarbonXTransInfo*                    mMasterTrans;
  std::string                          mTxQueueReadEvent;
  std::string                          mRxQueueWrittenEvent;
  std::string                          mResponseWrittenEvent;
  CarbonXResetObj*                     mResetObj;

  // Component Parameters
  bool p_enableDebugMessages;
  
  // Flag to indicate init() complete    
  bool p_initComplete;
  
};



class CarbonXResetObj
{
public:
  virtual ~CarbonXResetObj() {};
  virtual void reset(CarbonXT2T * t2t) = 0;
};

class CarbonX_EnetMII_Master_Reset : public CarbonXResetObj
{
public :
  CarbonX_EnetMII_Master_Reset() 
  {
  };
  void reset( CarbonXT2T * t )
  {

    // Build a CSWRITE to setup the transactor
    UInt32 cside_config = ENET_MII;
    cside_config = ((cside_config & ~0x8) | ENET_FCS_ENABLE);
    mTrans.csWrite(ENET_FRTR_CONFIG, cside_config);
    t->mCsQueue.push_back(mTrans);
  };
  ~CarbonX_EnetMII_Master_Reset() 
  {
  };
  
  CarbonXTransReqT   mTrans;
};

class CarbonX_EnetMII_Slave_Reset : public CarbonXResetObj
{
public :
  CarbonX_EnetMII_Slave_Reset() 
  {
  }
  void reset( CarbonXT2T * t ) 
  {
    // Build a CSWRITE to setup the transactor
    UInt32 cside_config = ENET_MII;
    cside_config = ((cside_config & ~0x8) | ENET_FCS_ENABLE);
    mTrans.csWrite(ENET_FRTR_CONFIG, cside_config);
    t->mCsQueue.push_back(mTrans);
  };
  ~CarbonX_EnetMII_Slave_Reset() 
  {
  };

  CarbonXTransReqT   mTrans;
};

class CarbonX_EnetGMII_Master_Reset : public CarbonXResetObj
{
public :
  CarbonX_EnetGMII_Master_Reset() 
  {
    mResetTrans[0] = NULL;
    mResetTrans[1] = NULL;
  }
  void reset( CarbonXT2T * t )
  {
    // Build a CSWRITE to setup the transactor
    UInt32 cside_config = ENET_GMII;
    cside_config = ((cside_config & ~0x8) | ENET_FCS_ENABLE);
    mTrans.csWrite(ENET_FRTR_CONFIG, cside_config);
    t->mCsQueue.push_back(mTrans);
  }
  ~CarbonX_EnetGMII_Master_Reset() 
  {
  };
  
  CarbonXTransReqT   mTrans;
  CarbonXTransInfo * mResetTrans[2];
};

class CarbonX_EnetGMII_Slave_Reset : public CarbonXResetObj
{
public :
  CarbonX_EnetGMII_Slave_Reset() 
  {
  }
  void reset( CarbonXT2T * t ) 
  {
    // Build a CSWRITE to setup the transactor
    UInt32 cside_config = ENET_GMII;
    cside_config = ((cside_config & ~0x8) | ENET_FCS_ENABLE);
    mTrans.csWrite(ENET_FRTR_CONFIG, cside_config);
    t->mCsQueue.push_back(mTrans);
  };
  ~CarbonX_EnetGMII_Slave_Reset() 
  {
  };

  CarbonXTransReqT   mTrans;
};

class CarbonX_EnetXGMII_Master_Reset : public CarbonXResetObj
{
public :
  CarbonX_EnetXGMII_Master_Reset() 
  {
  }
  void reset( CarbonXT2T * t )
  {

    // Build a CSWRITE to setup the transactor
    UInt32 cside_config = XGMII_FCS_ENABLE;
    mTrans.csWrite(XGMII_FRTR_CONFIG, cside_config);
    t->mCsQueue.push_back(mTrans);
  }
  ~CarbonX_EnetXGMII_Master_Reset() 
  {
  };
  
  CarbonXTransReqT   mTrans;
  CarbonXTransInfo * mResetTrans[2];
};

class CarbonX_EnetXGMII_Slave_Reset : public CarbonXResetObj
{
public :
  CarbonX_EnetXGMII_Slave_Reset() 
  {
  }
  void reset( CarbonXT2T * t ) 
  {
    // Build a CSWRITE to setup the transactor
    UInt32 cside_config = XGMII_FCS_ENABLE;
    mTrans.csWrite(XGMII_FRTR_CONFIG, cside_config);
    t->mCsQueue.push_back(mTrans);
  };
  ~CarbonX_EnetXGMII_Slave_Reset() 
  {
  };

  CarbonXTransReqT   mTrans;
};

class CarbonX_PCI_Initiator_T2S_Reset : public CarbonXResetObj
{
public :
  CarbonX_PCI_Initiator_T2S_Reset()  { }
  void reset( CarbonXT2T * t )  { (void)t;}
  ~CarbonX_PCI_Initiator_T2S_Reset() { }
};

//! PCI Target Reset Class
/*!
  Class to define reset behaivoir for the PCI Target transactor
 */
class CarbonX_PCI_Target_S2T_Reset : public CarbonXResetObj
{
public :

  //! Constructor
  CarbonX_PCI_Target_S2T_Reset() 
  { }

  //! Reset routine sets up the transactor at reset
  void reset( CarbonXT2T * t )
  {
    // Build a CSWRITE to setup the transactor
    UInt32 cside_config = 1<<28;  // Set the cside_waits bit
    mTrans.csWrite(0, cside_config);
    t->mCsQueue.push_back(mTrans);
  }
  ~CarbonX_PCI_Target_S2T_Reset() 
  { }
  
  //! Request Transaction Structure
  CarbonXTransReqT   mTrans;
};

class CarbonX_AHB_Master_Reset : public CarbonXResetObj
{
public :
  CarbonX_AHB_Master_Reset()  { }
  void reset( CarbonXT2T * t )  { (void)t;}
  ~CarbonX_AHB_Master_Reset() { }
};

class CarbonX_AHB_Slave_Reset : public CarbonXResetObj
{
public :
  CarbonX_AHB_Slave_Reset()  { }
  void reset( CarbonXT2T * t )  { (void)t;}
  ~CarbonX_AHB_Slave_Reset() { }
};

class CarbonX_Wishbone_T2S_Reset : public CarbonXResetObj
{
public :
  CarbonX_Wishbone_T2S_Reset()  { }
  void reset( CarbonXT2T * t )  { (void)t;}
  ~CarbonX_Wishbone_T2S_Reset() { }
};

#endif
