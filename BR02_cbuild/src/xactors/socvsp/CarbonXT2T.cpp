/***************************************************************************************
  Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#include "xactors/carbonX.h"
#include "CarbonXT2T.h"
#include "shell/carbon_capi.h"

using namespace std;

//! predicate to check CTS state 
struct CarbonXT2TCtsState : public unary_function<CarbonXTransInfo *, bool> {
  UInt32 mState1;
  UInt32 mState2;
  CarbonXT2TCtsState(UInt32 cts, UInt32 cts2 = 0xffffffff) : mState1(cts), mState2(cts2) {}
  bool operator() (const CarbonXTransInfo *cmd) {
    if(cmd != NULL)
      return (cmd->cts == mState1 || cmd->cts == mState2);
    else return false;
  }
};

class DeleteTrans {
public :
  void operator()(CarbonXTransInfo* trans) {
    delete trans;
  }
};

ostream & operator<<(ostream &os, const carbonXTransactionStruct &obj) {

  const char opstr[][10] = {"ILLEGAL", "NOP", "IDLE", "SLEEP", "READ", "WRITE", 
			    "CS_READ", "CS_WRITE",
			    "NXTREQ", "INT_DONE", "FREAD", "WAKEUP", "CS_RMW"};
  std::ostringstream opcode;
  
  os << "------------------------------------"   << std::endl
     << "Transaction ID: 0x" << std::hex << obj.mTransId   << std::endl
     << "Repeat Count:   "   << std::dec << obj.mRepeatCnt << std::endl;
  
  if(obj.mOpcode < CARBONX_OP_NUM_OPCODES)
    os << "Opcode:         " << opstr[obj.mOpcode] << std::endl;
  else
    os << "Opcode:         0x" << std::hex << obj.mOpcode << std::endl;
  
  os << "Address:        0x" << std::hex << obj.mAddress   << std::endl
     << "Size:           "   << std::dec << obj.mSize      << std::endl
     << "Status:         0x" << std::hex << obj.mStatus    << std::endl
     << "Width:          0x" << std::hex << obj.mWidth     << std::endl
     << "Has Data:         " << obj.mHasData     << std::endl
     << "Payload:"                                             << std::endl;
  
  if(obj.mSize > 0 && obj.mData != NULL) {

    // Output The Payload Data based on the the Words Size
    for(UInt32 i = 0; i < obj.mSize; i += obj.mWidth&0xf) {
      UInt64 odata = 0;         // silence coverity
      switch(obj.mWidth) {
      case 1 : odata = *(reinterpret_cast<UInt8  *>(obj.mData+i)); break;
      case 2 : odata = *(reinterpret_cast<UInt16 *>(obj.mData+i)); break;
      case 4 : odata = *(reinterpret_cast<UInt32 *>(obj.mData+i)); break;
      case 8 : odata = *(reinterpret_cast<UInt64 *>(obj.mData+i)); break;
      }
      os << " " << std::dec << i << ": 0x" << std::setw(obj.mWidth&0xf) << std::setfill('0') << std::hex << odata << std::endl;
    }
  }
  return os;
}

#define PORT_OWNER mCarbonComponent
CarbonXT2T::CarbonXT2T(sc_mx_module*        carbonComp, 
		       const char*          xtorName, 
		       CarbonObjectID**     carbonObj,
		       CarbonXPortRegisterT portsToRegister,
                       CarbonXResetObj*     resetObj)
  :   sc_mx_transaction_slave(string(xtorName)),
      mCarbonXTMaster(NULL),
      mXtorName(xtorName),
      mXtorPath(xtorName),
      mTMasterName(string(xtorName) + string("_TM")),
      mTSlaveName(string(xtorName) + string("_TS")),
      mCarbonComponent(carbonComp), 
      mpCarbonObject(carbonObj),
      mPortType(portsToRegister),
      mTransId(0),
      mMasterTrans(NULL)
{

  // Setup Master Port
  if ((mPortType == eCarbonXMaster) || (mPortType == eCarbonXCMaster)) {
    mCarbonXTMaster = new sc_port<sc_mx_transaction_if,1>(PORT_OWNER, mTMasterName);
    mCarbonXTMaster->enablePort(false);   // disable the port by default, it gets enabled by the component if the master is to be connected.
    mCarbonComponent->registerPort(mCarbonXTMaster, mTMasterName.c_str());
    mMasterTrans = new CarbonXTransInfo(mCarbonXTMaster);
  }

  // Setup Slave Port
  setMxOwner(PORT_OWNER);
  if (mPortType == eCarbonXSlave)
    mCarbonComponent->registerPort(this, mTSlaveName.c_str());

  // Setup Transaction Port Properties
  MxTransactionProperties prop;
  setupPortProperties(&prop);

  // Set Properties for Master
  if(mCarbonXTMaster) mCarbonXTMaster->setProperties(&prop);  

  // Set Properties for Slave
  setProperties(&prop);  
  
  mRxQueueWrittenEvent  = string(xtorName) + string("Write");
  mTxQueueReadEvent     = string(xtorName) + string("Read");
  mResponseWrittenEvent = string(xtorName) + string("Response");
  mResetObj = resetObj;
  
  p_initComplete = false;
  
}

CarbonXT2T::~CarbonXT2T()
{
  delete mMasterTrans;
  delete mCarbonXTMaster;
  delete mResetObj;
}

void CarbonXT2T::setupPortProperties(MxTransactionProperties* prop)
{
  memset(prop,0,sizeof(MxTransactionProperties));

  prop->mxsiVersion                   = MXSI_VERSION_6;
  prop->useMultiCycleInterface        = true;
  prop->addressBitwidth               = 64;
  prop->mauBitwidth                   = 8;
  prop->dataBitwidth                  = 32;
  prop->dataBeats                     = 64;
  prop->isLittleEndian                = true;
  prop->isOptional                    = false;
  prop->supportsAddressRegions        = false;
  prop->numCtrlFields                 = 0;
  prop->numSlaveFlags                 = CARBONX_SOCVSP_NUM_SLAVE_FLAGS;
  prop->numMasterFlags                = CARBONX_SOCVSP_NUM_MASTER_FLAGS; 
  prop->numTransactionSteps           = CARBONX_SOCVSP_NTS; 
  prop->validTimingTable              = NULL;
  prop->protocolID                    = 0x00000001;
  sprintf(prop->protocolName,"Carbon");
  prop->supportsNotify                = true;
  prop->supportsBurst                 = true;
  prop->supportsSplit                 = false;
  prop->isAddrRegionForwarded         = false;
  prop->forwardAddrRegionToMasterPort = NULL;
  
}

void 
CarbonXT2T::communicate()
{
  
  // If a transaction is pending drive it to the slave 
  if(mMasterTrans) {
    if (mMasterTrans->cts == CARBONX_SOCVSP_CTS_QUEUED) {
      mMasterTrans->cts++; // Mark transaction as running
      mCarbonXTMaster->driveTransaction(mMasterTrans);
    }
    
    // Check if the response is done, and signal
    if (mMasterTrans->cts == CARBONX_SOCVSP_CTS_DONE) {
      if(mPortType == eCarbonXMaster) carbonXSetSignal(mResponseWrittenEvent.c_str());

      // C-side Masters doesn't care about the response, so reset the transaction.
      else if(mPortType == eCarbonXCMaster) mMasterTrans->reset();
    }
  }

  // Get CS Requests, if we are a Master Port
  if(mPortType == eCarbonXMaster) {
    MxU32 num_cswrite = 0;
    
    // Address 0 should hold number of CsWrites to Perform
    if(mCarbonXTMaster->read(0, &num_cswrite, NULL) == MX_STATUS_OK) {
      
      // If there are any cswrites in the queue
      for(MxU32 i = 0; i < num_cswrite; i++) {
	
	// Address 1 should hold, cs_addr in the first element, and cs_data in the second
	MxU32 cs_data[2];
	if(mCarbonXTMaster->read(1, cs_data, NULL) == MX_STATUS_OK) {
	  queueCsWrite(cs_data[0], cs_data[1]);
	}
      }
    }
  }
}

void 
CarbonXT2T::update()
{
//   // When Slave has marked the transaction canceled, remove from Tx Queue and delete them
//   list<CarbonXTransInfo*>::iterator
//     iter = find_if(mTxQueue.begin(), mTxQueue.end(), CarbonXT2TCtsState(CARBONX_SOCVSP_CTS_CANCEL));

//   while(iter != mTxQueue.end()) {
//     delete *iter;
//     iter = mTxQueue.erase(iter);
//     iter = find_if(iter, mTxQueue.end(), CarbonXT2TCtsState(CARBONX_SOCVSP_CTS_CANCEL));
//   }
}

void 
CarbonXT2T::init()
{
  carbonXAttach(carbonXGenXtorHandle(*mpCarbonObject, mXtorPath.c_str()), reinterpret_cast<CarbonXAttachFuncT>(vspx_func), 1, this);
  p_initComplete = true;
}

void
CarbonXT2T::reset(MxResetLevel, const MxFileMapIF*)
{
  // Clear Rx Queue
  mRxQueue.clear();

  // Delete all outstanding transactions, and clear the queue
  // Note that the slave port should not attempt to access any transaction
  // in the reset method since they may already been deleted by the master.
  for_each(mTxQueue.begin(), mTxQueue.end(), DeleteTrans());

  mTransId = 0;

  mResetObj->reset(this);
}

void 
CarbonXT2T::terminate()
{
  // Clear Tx Queue
  for_each(mTxQueue.begin(), mTxQueue.end(), DeleteTrans());
  
  // Clear Rx Queue
  mRxQueue.clear();
}



//////////////////////////////////////////////////
//                                              //
// Maxsim Slave Transactor Interface            //
//                                              //
//////////////////////////////////////////////////
  
MxStatus CarbonXT2T::read(MxU64, MxU32*, MxU32*){return MX_STATUS_NOTSUPPORTED;}
MxStatus CarbonXT2T::write(MxU64, MxU32*, MxU32*){return MX_STATUS_NOTSUPPORTED;}

void 
CarbonXT2T::driveTransaction(MxTransactionInfo* info)
{
  
  // Check if queue is empty
  bool empty = mRxQueue.empty();
  
  // Put command on the queue
  mRxQueue.push_back(static_cast<CarbonXTransInfo*>(info));

  // Increment cts to indicate the transaction is queued
  info->cts++;
  
  // Check if the queue was empty, if it was, we want to notify
  // anyone that might wait, that there is now data on the queue 
  if(empty) {
    carbonXSetSignal(mRxQueueWrittenEvent.c_str());
  }
}

void CarbonXT2T::cancelTransaction(MxTransactionInfo* info)
{ 
  // Mark the transaction canceled and remove it from the list
  info->cts = CARBONX_SOCVSP_CTS_CANCEL;
  mRxQueue.remove(static_cast<CarbonXTransInfo*>(info));
}

MxStatus CarbonXT2T::debugTransaction(MxTransactionInfo*)
{
  return MX_STATUS_NOTSUPPORTED;
}

//////////////////////////////////////////////////
//                                              //
// CarbonX Transactor Reqest/Response Interface //
//                                              //
//////////////////////////////////////////////////
  
void CarbonXT2T::setRequest(const carbonXTransactionStruct* req)
{
  mMasterTrans->setRequest(*req);
  mMasterTrans->cts++;
}

void CarbonXT2T::getRequest(carbonXTransactionStruct* req)
{
  // Wait for a Transaction
  while(mRxQueue.empty()) {
    carbonXWaitSignal(const_cast<char*>(mRxQueueWrittenEvent.c_str()), 0);
  }

  // Get the request from the current transaction
  mRxQueue.front()->getRequest(req);
  mRxQueue.front()->cts++; // Transaction is now running
}

void CarbonXT2T::setResponse(const carbonXTransactionStruct* resp)
{
  if(mRxQueue.empty())
    mCarbonComponent->message(MX_MSG_ERROR, "Trying to set response when no Request has been received.\n");

  else {
    mRxQueue.front()->setResponse(*resp);
    mRxQueue.front()->cts++; // Transaction is now done
    mRxQueue.pop_front();
  }
    
}

void CarbonXT2T::getResponse(carbonXTransactionStruct* resp)
{  
  while(mMasterTrans->cts != CARBONX_SOCVSP_CTS_DONE) {
    carbonXWaitSignal(const_cast<char*>(mResponseWrittenEvent.c_str()), 0);
  }
  mMasterTrans->getResponse(resp);
  mMasterTrans->reset();
}

void CarbonXT2T::queueCsWrite(CarbonUInt32 addr, CarbonUInt32 data) {

  carbonXTransactionStruct req;
  carbonXInitTransactionStruct(&req, 0);
  
  // Create Request
  carbonXSetTransaction(&req, 0x80000000, CARBONX_OP_CS_WRITE, 
			addr, &data, 4, 0, TBP_WIDTH_32);
  
  // This is a bit funcy, but I believe its correct.
  // carbonXTransactionStruct is a c-struct so it does not have a copy constructor
  // so when pushed onto the queue, all fields including pointers would be copied.
  // Once req is pushed onto the queue, the object on the queue is the new owner of those pointers
  // so req can't be touched again after it has been queued. The function that takes
  // the object off the queue is responsible for freeing the data.
  mCsQueue.push_back(req);
}

bool CarbonXT2T::getCSRequest(carbonXTransactionStruct* req){
  
  // First see if there are any CS Writes on the CS Queue
  if(!mCsQueue.empty()) {
    carbonXCopyTransactionStruct(req, &mCsQueue.front());
    
    // Destroy the object on the queue
    carbonXDestroyTransactionStruct(&mCsQueue.front());
    mCsQueue.pop_front();

    return true;
  }

  return false;
}

void CarbonXT2T::defaultResponse(carbonXTransactionStruct* resp) {
  if(mPortType == eCarbonXCMaster)
    carbonXSetTransaction(resp, 0, CARBONX_OP_READ, 0, NULL, 0, 0, TBP_WIDTH_8);
  else
    carbonXSetTransaction(resp, 0, CARBONX_OP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_64P);
}

void
CarbonXT2T::setParameter( const string &name, const string &value )
{
  MxConvertErrorCodes status = MxConvert_SUCCESS;
  
  if (name == "Enable Debug Messages")
    {
      status = MxConvertStringToValue(value, &p_enableDebugMessages);
    }
  if (name == "Transactor Path")
    {
      mXtorPath = value;
    }
  else
    {
      mCarbonComponent->message(MX_MSG_WARNING, "setParameter: cannot change parameter <%s> at runtime. Assignment ignored", name.c_str() );
    }
  if ( status == MxConvert_SUCCESS )
    {
    }
  else
    {
      mCarbonComponent->message( MX_MSG_WARNING, "CarbonXT2T::setParameter: illegal value <%s> "
	       "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

void CarbonXT2T::vspx_func(CarbonXT2T* this_obj){
  switch(this_obj->mPortType) {
  case eCarbonXSlave   : this_obj->slaveTransactionLoop();   break;
  case eCarbonXMaster  : this_obj->masterTransactionLoop();  break;
  case eCarbonXCMaster : this_obj->cMasterTransactionLoop(); break;
  default :
    this_obj->mCarbonComponent->message(MX_MSG_ERROR, "Wrong Port Type: %d\n", this_obj->mPortType);
    break;
  }
}

void CarbonXT2T::slaveTransactionLoop() {
  carbonXTransactionStruct trans_req, trans_resp;

  // Initialize Transaction Structures
  carbonXInitTransactionStruct(&trans_req, 0);
  carbonXInitTransactionStruct(&trans_resp, 0);

  while (1) {
    getRequest(&trans_req);
    carbonXReqRespTransaction(&trans_req, &trans_resp);
    setResponse(&trans_resp);
  }
}

void CarbonXT2T::masterTransactionLoop() {
  carbonXTransactionStruct trans_req, trans_resp, cs_req, cs_resp;
  
  // Initialize Transaction Structures
  carbonXInitTransactionStruct(&trans_req, 0);
  carbonXInitTransactionStruct(&trans_resp, 0);
  carbonXInitTransactionStruct(&cs_req, 0);
  carbonXInitTransactionStruct(&cs_resp, 0);

  // Set Default Response
  defaultResponse(&trans_resp);

  while (1) {
    // Process All Config Requests before Giving Response
    while(getCSRequest(&cs_req)) {
      carbonXReqRespTransaction(&cs_req, &cs_resp);
    }
    
    // Send Last Response, at the same time get Request From HDL Side
    carbonXReqRespTransaction(&trans_resp, &trans_req);
    
    // Drive Request on the master port (Non Blocking Call)
    trans_req.mTransId = mTransId++;
    setRequest(&trans_req);
    
    // Get Response From the Slave (Blocking Call)
    getResponse(&trans_resp);

  }
}

void CarbonXT2T::cMasterTransactionLoop() {
  carbonXTransactionStruct trans_req, trans_resp, cs_req, cs_resp;

  // Initialize Transaction Structures
  carbonXInitTransactionStruct(&trans_req, 0);
  carbonXInitTransactionStruct(&trans_resp, 0);
  carbonXInitTransactionStruct(&cs_req, 0);
  carbonXInitTransactionStruct(&cs_resp, 0);

  // Set Default Response
  defaultResponse(&trans_resp);

  while (1) {

    // First Process All Config Requests
    while(getCSRequest(&cs_req)) {
      carbonXReqRespTransaction(&cs_req, &cs_resp);
    }

    // Send Last Response, at the same time get Request From HDL Side
    carbonXReqRespTransaction(&trans_resp, &trans_req);

    // Drive Request on the master port (Non Blocking Call)
    trans_req.mTransId = mTransId++;
    setRequest(&trans_req);
    
    // The C-side master don't get a response from the slave.
    // Set Default Response
    defaultResponse(&trans_resp);
  }
}

    

