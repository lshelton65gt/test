// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef CarbonPciTarget__H
#define CarbonPciTarget__H

#include "maxsim.h"
#include "MxSaveRestore.h"
#include "xactors/systemc/CarbonXPciConfigRegs.h"
#include "xactors/sparse_mem/carbonXSparseMem.h"

// Place global class declarations here:
class CarbonPciTarget_MxDI;

class CarbonPciTarget : public sc_mx_module
    , public MxSaveRestore
{
    // Declare your friends here:

    friend class PCISlave_TS;

    friend class CarbonPciTarget_MxDI;

public:
    // Place instance declarations for the Ports here:
    PCISlave_TS* PCISlave_TSlave;
	
    // constructor / destructor
    CarbonPciTarget(sc_mx_m_base* c, const string &s);
    virtual ~CarbonPciTarget();

    // overloaded methods for clocked components
    void communicate();
    void update();

    // Implementation of MxSaveRestore interface methods
    virtual bool saveData( MxODataStream &data );
    virtual bool restoreData( MxIDataStream &data );


    // overloaded sc_mx_module methods
    string getName();
    void setParameter(const string &name, const string &value);
    string getProperty( MxPropertyType property );
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);
    // The MxDI interface.
    MXDI* getMxDI();

    //Initialize the Port Properties
    void initSignalPort(sc_mx_signal_p_base* signalIf);
    void initTransactionPort(sc_mx_transaction_p_base* transIf);


private:
    bool initComplete; 
    bool p_enableDbgMsg;

    // User defined parameters:
    MxU64 p_BarSize[6];
    bool  p_BarIsIOSpace[6];
    bool  p_BarIsPrefetch[6];
    bool  p_BarIs64Bit[6];

    // Declare MXDI Interface
    MXDI* mxdi;

    MxU16 r_reg0;
    MxU16 r_reg1;
    MxU8 r_DeviceID;



    // place your private functions and data members here:
    // ...
    // This shows an example state variable for the save/restore mechanism.
    // int		exampleStateVariable;
    CarbonXPciConfigRegs mCfgSpace;
    sparseMem_t          mMemSpace;
    sparseMem_t          mIOSpace;

};

#endif
