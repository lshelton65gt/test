/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "CarbonPciTarget_MxDI.h"
#include "CarbonPciTarget.h"
#include "xactors/sparse_mem/carbonXSparseMem.h"

struct SimpleRegInfo_t
{
  const char*             name;
  unsigned int            bitsWide;
  unsigned int            addr;
  MxdiRegDisplay_t        display;
  const char*             description;
};

struct GroupInfo_t
{
  const char*             description;
  int	                    start;
  int                     end;
};

struct MemSpaceInfo_t
{
  const char*             memSpaceName;
  unsigned int            bitsPerMau;
  MxdiAddrSimple_t        maxAddress;
  int                     isProgramMemory;
  unsigned int            cyclesToAccess;
  MxdiMemReadWrite_t      readWrite;
  const char*             description;
};

static const SimpleRegInfo_t CarbonPciTarget_reg_info[] =
{

  // "ConfigurationHeader" registers
  {   "DeviceID",        16,  2, MXDI_REGTYPE_HEX, "Device ID" },
  {   "VendorID",        16,  0, MXDI_REGTYPE_HEX, "Vendor ID" },
  {   "StatusReg",       16,  6, MXDI_REGTYPE_HEX, "Status Register" },
  {   "CommandReg",      16,  4, MXDI_REGTYPE_HEX, "Command Register" },
  {   "ClassCode",       24,  9, MXDI_REGTYPE_HEX, "Class Code" },
  {   "RevisionID",       8,  8, MXDI_REGTYPE_HEX, "Revision ID" },
  {   "BIST",             8, 15, MXDI_REGTYPE_HEX, "BIST Register" },
  {   "HeaderType",       8, 14, MXDI_REGTYPE_HEX, "Header Type" },
  {   "LatencyTimer",     8, 13, MXDI_REGTYPE_HEX, "Latency Timer" },
  {   "CacheLineSize",    8, 12, MXDI_REGTYPE_HEX, "Cache Line Size" },
  {   "BAR0",            32, 16, MXDI_REGTYPE_HEX, "Base Address Register 0" },
  {   "BAR1",            32, 20, MXDI_REGTYPE_HEX, "Base Address Register 1" },
  {   "BAR2",            32, 24, MXDI_REGTYPE_HEX, "Base Address Register 2" },
  {   "BAR3",            32, 28, MXDI_REGTYPE_HEX, "Base Address Register 3" },
  {   "BAR4",            32, 32, MXDI_REGTYPE_HEX, "Base Address Register 4" },
  {   "BAR5",            32, 36, MXDI_REGTYPE_HEX, "Base Address Register 5" },
  {   "CB_CIS_Ptr",      32, 40, MXDI_REGTYPE_HEX, "Cardbus CIS Pointer" },
  {   "SubId",           16, 46, MXDI_REGTYPE_HEX, "Subsystem ID" },
  {   "SubVendorId",     16, 44, MXDI_REGTYPE_HEX, "Subsystem Vendor ID" },
  {   "ExpROMBAR",       32, 48, MXDI_REGTYPE_HEX, "Expansion ROM Base Address" },
  {   "CapabilitiesPtr",  8, 52, MXDI_REGTYPE_HEX, "Capabilities Pointer" },
  {   "Max_Lat",          8, 63, MXDI_REGTYPE_HEX, "Max Latency" },
  {   "Min_Gnt",          8, 62, MXDI_REGTYPE_HEX, "Timeslice Request" },
  {   "IntrPin",          8, 61, MXDI_REGTYPE_HEX, "Interrupt Pin" },
  {   "IntrLine",         8, 60, MXDI_REGTYPE_HEX, "Interrupt Line" }
};

static const MxU32 REGISTER_COUNT	= sizeof( CarbonPciTarget_reg_info    ) / sizeof( CarbonPciTarget_reg_info[0]   );

// TODO: Define your groups here.

static const GroupInfo_t CarbonPciTarget_group_info[] =
{
  {   "ConfigurationHeader", 0, REGISTER_COUNT-1}
};

// TODO: Define your memory spaces here.

static const MemSpaceInfo_t CarbonPciTarget_mem_info[] =
{
  { "Config Space", 32, 0xFFFF, 0, 0, MXDI_MEM_ReadWrite, "Config Space" },
  { "Memory Space", 8, 0xFFFFFFFF, 0, 0, MXDI_MEM_ReadWrite, "Memory Space" },
  { "IO Space", 8, 0xFFFF, 0, 0, MXDI_MEM_ReadWrite, "IO Space" }

};

static const MxU32 GROUP_COUNT	= sizeof( CarbonPciTarget_group_info  ) / sizeof( CarbonPciTarget_group_info[0] );
static const MxU32 MEMORY_COUNT   = sizeof( CarbonPciTarget_mem_info    ) / sizeof( CarbonPciTarget_mem_info[0]   );


CarbonPciTarget_MxDI::CarbonPciTarget_MxDI(CarbonPciTarget* c) : target(c)
{
    MxU32	i;
    
    strcpy( features.targetName, "CarbonPciTarget" );
    strcpy( features.targetVersion, "1.0" );
    
    // Register related info
    features.nrRegisterGroups = GROUP_COUNT;
    
    regInfo = new MxdiRegInfo_t[ REGISTER_COUNT ];
    for ( i = 0; i < REGISTER_COUNT; i++ )
    {
        regInfo[i].regNumber                    = i;
        regInfo[i].bitsWide                     = CarbonPciTarget_reg_info[i].bitsWide;
        regInfo[i].display                      = CarbonPciTarget_reg_info[i].display;
        regInfo[i].hasSideEffects               = 0;
        regInfo[i].details.type                 = MXDI_REGTYPE_Simple;

        strcpy( regInfo[i].name,                CarbonPciTarget_reg_info[i].name );
        strcpy( regInfo[i].description,         CarbonPciTarget_reg_info[i].description );
        
    }


    
    regGroup = new MxdiRegGroup_t[ GROUP_COUNT ];
    for ( i = 0; i < GROUP_COUNT; i++ )
    {
        regGroup[i].groupID                     = i;
        regGroup[i].numRegsInGroup              = CarbonPciTarget_group_info[i].end - CarbonPciTarget_group_info[i].start + 1;

        strcpy( regGroup[i].description,        CarbonPciTarget_group_info[i].description );
    }
    
    // Memory related info
    features.nrMemSpaces = MEMORY_COUNT;
    
    memSpaceInfo = new MxdiMemSpaceInfo_t[ MEMORY_COUNT ];
    for ( i = 0; i < MEMORY_COUNT; i++ )
    {
        memSpaceInfo[i].memSpaceId            = i;
        memSpaceInfo[i].bitsPerMau            = CarbonPciTarget_mem_info[i].bitsPerMau;
        memSpaceInfo[i].maxAddress            = CarbonPciTarget_mem_info[i].maxAddress;
        memSpaceInfo[i].nrMemBlocks           = 1;
        memSpaceInfo[i].isProgramMemory       = CarbonPciTarget_mem_info[i].isProgramMemory;
        
        strcpy( memSpaceInfo[i].memSpaceName, CarbonPciTarget_mem_info[i].memSpaceName );
        strcpy( memSpaceInfo[i].description,  CarbonPciTarget_mem_info[i].description );
    }
    
    // Setup Config Space Blocks
    configMemBlockInfo = new MxdiMemBlockInfo_t[ 1 ];
    configMemBlockInfo[0].id                    = (MxU16) 0;
    configMemBlockInfo[0].parentID              = (MxU16) 0;
    configMemBlockInfo[0].startAddr             = 0;
    configMemBlockInfo[0].endAddr               = CarbonPciTarget_mem_info[0].maxAddress;
    configMemBlockInfo[0].cyclesToAccess        = CarbonPciTarget_mem_info[0].cyclesToAccess;
    configMemBlockInfo[0].readWrite             = CarbonPciTarget_mem_info[0].readWrite;
    strcpy( configMemBlockInfo[0].name, "Config Registers");

    
    { // Setup Memory Space Blocks
      // First Count Active Blocks
      MxU32 num_blocks = 1;
      for(i = 0; i < 6; i++)
	if(target->p_BarSize[i] > 0 && !target->p_BarIsIOSpace[i] ) ++num_blocks;

      memSpaceInfo[1].nrMemBlocks = num_blocks;
      memMemBlockInfo = new MxdiMemBlockInfo_t[ num_blocks];
      
      memMemBlockInfo[0].id = 0;
      memMemBlockInfo[0].parentID              = (MxU16) 1;
      memMemBlockInfo[0].startAddr             = 0;
      memMemBlockInfo[0].endAddr               = CarbonPciTarget_mem_info[1].maxAddress;
      memMemBlockInfo[0].cyclesToAccess        = CarbonPciTarget_mem_info[1].cyclesToAccess;
      memMemBlockInfo[0].readWrite             = CarbonPciTarget_mem_info[1].readWrite;
      sprintf(memMemBlockInfo[0].name, "All");

      num_blocks = 1;
      for(i = 0; i < 6; i++)
	if(target->p_BarSize[i] > 0 && !target->p_BarIsIOSpace[i]) {
	  mMemBlockBar[num_blocks] = i;
	  memMemBlockInfo[num_blocks].id = num_blocks;
	  memMemBlockInfo[num_blocks].parentID              = (MxU16) 1;
	  memMemBlockInfo[num_blocks].startAddr             = 0;
	  memMemBlockInfo[num_blocks].endAddr               = 0;
	  memMemBlockInfo[num_blocks].cyclesToAccess        = CarbonPciTarget_mem_info[1].cyclesToAccess;
	  memMemBlockInfo[num_blocks].readWrite             = CarbonPciTarget_mem_info[1].readWrite;
	  sprintf(memMemBlockInfo[num_blocks].name, "BAR %d", i);
	}
    }

    { // Setup Memory Space Blocks
      // First Count Active Blocks
      MxU32 num_blocks = 1;
      for(i = 0; i < 6; i++)
	if(target->p_BarSize[i] > 0 && target->p_BarIsIOSpace[i]) ++num_blocks;
      
      memSpaceInfo[2].nrMemBlocks = num_blocks;
      ioMemBlockInfo = new MxdiMemBlockInfo_t[ num_blocks];
      
      ioMemBlockInfo[0].id = 0;
      ioMemBlockInfo[0].parentID              = (MxU16) 2;
      ioMemBlockInfo[0].startAddr             = 0;
      ioMemBlockInfo[0].endAddr               = CarbonPciTarget_mem_info[1].maxAddress;
      ioMemBlockInfo[0].cyclesToAccess        = CarbonPciTarget_mem_info[1].cyclesToAccess;
      ioMemBlockInfo[0].readWrite             = CarbonPciTarget_mem_info[1].readWrite;
      sprintf(ioMemBlockInfo[0].name, "All");

      num_blocks = 1;
      for(i = 0; i < 6; i++)
	if(target->p_BarSize[i] > 0 && target->p_BarIsIOSpace[i]) {
	  mIOBlockBar[num_blocks] = i;
	  ioMemBlockInfo[num_blocks].id = num_blocks;
	  ioMemBlockInfo[num_blocks].parentID              = (MxU16) 2;
	  ioMemBlockInfo[num_blocks].startAddr             = 0;
	  ioMemBlockInfo[num_blocks].endAddr               = 0;
	  ioMemBlockInfo[num_blocks].cyclesToAccess        = CarbonPciTarget_mem_info[2].cyclesToAccess;
	  ioMemBlockInfo[num_blocks].readWrite             = CarbonPciTarget_mem_info[2].readWrite;
	  sprintf(ioMemBlockInfo[num_blocks].name, "BAR %d", i);
	}
    }
}

CarbonPciTarget_MxDI::~CarbonPciTarget_MxDI()
{
    if ( regInfo )
        delete [] regInfo;
    if ( regGroup )		
        delete [] regGroup;
    if ( memSpaceInfo )	
        delete [] memSpaceInfo;
    if ( configMemBlockInfo )	
        delete [] configMemBlockInfo;
    if ( memMemBlockInfo )	
        delete [] memMemBlockInfo;
    if ( ioMemBlockInfo )	
        delete [] ioMemBlockInfo;
}


// Register related functions
MxdiReturn_t
CarbonPciTarget_MxDI::MxdiRegGetGroups( MxU32 groupIndex
                                      , MxU32 desiredNumOfRegGroups
                                      ,	MxU32* actualNumOfRegGroups
                                      , MxdiRegGroup_t* grp )
{
    if ( groupIndex >= GROUP_COUNT )
    {
        return MXDI_STATUS_IllegalArgument;
    }
    
    MxU32	i;
    for( i = groupIndex; ( i < groupIndex + desiredNumOfRegGroups ) && ( i < 
        GROUP_COUNT ); i++ )
    {
        grp[i] = regGroup[i];
    }
    
    *actualNumOfRegGroups = i - groupIndex;
    
    return MXDI_STATUS_OK;
}

MxdiReturn_t
CarbonPciTarget_MxDI::MxdiRegGetMap( MxU32 groupID
                                   , MxU32 regIndex
                                   , MxU32 registerSlots
                                   , MxU32* registerCount
                                   , MxdiRegInfo_t* reg )
{
    if ( groupID >= GROUP_COUNT )
    {
        return MXDI_STATUS_IllegalArgument;
    }
    
    MxU32	i;
    MxU32	start = regIndex + CarbonPciTarget_group_info[groupID].start;
    MxU32	end = regIndex + CarbonPciTarget_group_info[groupID].end;
    for ( i = 0; ( i < registerSlots ) && ( start + i <= end ); i++ )
    {
        reg[i] = regInfo[start + i];
    }
    
    *registerCount = i;
    
    return MXDI_STATUS_OK;
}


MxdiReturn_t
CarbonPciTarget_MxDI::MxdiRegWrite( MxU32 regCount
                                  , MxdiReg_t* reg
                                  , MxU32* numRegsWritten
                                  , MxU8 doSideEffects )
{
  UNUSEDARG(doSideEffects);
  
  std::cout << "Register count = " << REGISTER_COUNT << std::endl;
  MxU32	i;
  for ( i = 0; i < regCount; i++ )
    {
      
      MxU32 tmp32 = 0;
      for(MxU32 byte = 0; byte < (CarbonPciTarget_reg_info[reg[i].regNumber].bitsWide+7)/8; byte++) {
	tmp32 |= ((MxU32)reg[i].bytes[byte] & 0xff) << (8*byte);
	setReg(CarbonPciTarget_reg_info[reg[i].regNumber].addr, tmp32, CarbonPciTarget_reg_info[reg[i].regNumber].bitsWide);
	
	if(reg[i].regNumber == 0)
	  std::cout << "CarbonPciTarget_MxDI::MxdiRegWrite: Wrote Register " << CarbonPciTarget_reg_info[reg[i].regNumber].name 
		    << " Addr: " << CarbonPciTarget_reg_info[reg[i].regNumber].addr
		    << " Data: " << tmp32 << std::endl;

      }            
    }

    *numRegsWritten = regCount;
    
    return MXDI_STATUS_OK;
}

MxdiReturn_t
CarbonPciTarget_MxDI::MxdiRegRead( MxU32 regCount
                                , MxdiReg_t* reg
                                , MxU32* numRegsRead
                                , MxU8 doSideEffects )
{
  UNUSEDARG(doSideEffects);

  MxU32	i;
  for ( i = 0; i < regCount; i++ )
    {
      MxU32 tmp32 = getReg(CarbonPciTarget_reg_info[reg[i].regNumber].addr, CarbonPciTarget_reg_info[reg[i].regNumber].bitsWide);
      
	if(reg[i].regNumber == 0)
	  std::cout << "CarbonPciTarget_MxDI::MxdiRegRead: Read Register " << CarbonPciTarget_reg_info[reg[i].regNumber].name 
		    << " Addr: " << CarbonPciTarget_reg_info[reg[i].regNumber].addr
		    << " Data: " << tmp32 << std::endl;

      for(MxU32 byte = 0; byte < (CarbonPciTarget_reg_info[reg[i].regNumber].bitsWide+7)/8; byte++) {
	reg[i].bytes[byte] = (MxU8)((tmp32 >> (byte*8)) & 0xff);
	
	if(reg[i].regNumber == 0)
	  std::cout << "reg[" << i << "].bytes[" << byte << "] = " << (int)reg[i].bytes[byte] << std::endl;
      }
    }
  
  *numRegsRead = regCount;
  return MXDI_STATUS_OK;
}


// Memory related functions
MxdiReturn_t
CarbonPciTarget_MxDI::MxdiMemGetSpaces( MxU32 spaceIndex
                                      , MxU32 memSpaceSlots
                                      , MxU32* memSpaceCount
                                      , MxdiMemSpaceInfo_t* memSpace )
{
    if ( spaceIndex >= MEMORY_COUNT )
        return MXDI_STATUS_IllegalArgument;
    
    MxU32	i;
    MxU32	start = spaceIndex;
    for( i = 0; ( i < memSpaceSlots ) && ( start + i < MEMORY_COUNT ); i++ )
    {
        memSpace[i] = memSpaceInfo[start + i];
    }
    
    *memSpaceCount = i;
    
    return MXDI_STATUS_OK;
}


MxdiReturn_t
CarbonPciTarget_MxDI::MxdiMemGetBlocks( MxU32 memorySpace
                                      , MxU32 blockIndex
                                      , MxU32 memBlockSlots
                                      , MxU32* memBlockCount
                                      , MxdiMemBlockInfo_t* memBlock )
{
    if ( memorySpace >= MEMORY_COUNT )
        return MXDI_STATUS_IllegalArgument;
    
    MxU32 i;
    MxU32 startAddr;
    
    std::cout << "IO Space Number of Blocks: "  << memSpaceInfo[2].nrMemBlocks << std::endl;
    std::cout << "Mem Space Number of Blocks: " << memSpaceInfo[1].nrMemBlocks << std::endl;

    switch(memorySpace) {
    case 0 :
      for ( i = 0; ( i < memBlockSlots ) && ( blockIndex + i < memSpaceInfo[memorySpace].nrMemBlocks ); i++ ) {
	memBlock[i] = configMemBlockInfo[blockIndex + i];
      }
      break;
    case 1 :
      // Check Base Address registers for block sizes
      for ( i = 0; ( i < memBlockSlots ) && ( blockIndex + i < memSpaceInfo[memorySpace].nrMemBlocks ); i++ ) {
	if( (blockIndex + i) != 0) {
	  startAddr = target->mCfgSpace.getRegisterValue(4 + mMemBlockBar[blockIndex+i]) & 0xfffffff0;
	  memMemBlockInfo[blockIndex+i].startAddr = startAddr;
	  memMemBlockInfo[blockIndex+i].endAddr   = startAddr + target->p_BarSize[mMemBlockBar[blockIndex+i]] -1;
	}
	memBlock[i] = memMemBlockInfo[blockIndex + i];
      }
      break;
    case 2 :
      // Check Base Address registers for block sizes
      for ( i = 0; ( i < memBlockSlots ) && ( blockIndex + i < memSpaceInfo[memorySpace].nrMemBlocks ); i++ ) {
	if( (blockIndex + i) != 0) {
	  startAddr = target->mCfgSpace.getRegisterValue(4 + mIOBlockBar[blockIndex+i]) & 0xfffffff0;
	  ioMemBlockInfo[blockIndex+i].startAddr = startAddr;
	  ioMemBlockInfo[blockIndex+i].endAddr   = startAddr + target->p_BarSize[mIOBlockBar[blockIndex+i]] -1;
	}
	memBlock[i] = ioMemBlockInfo[blockIndex + i];
      }
      break;
    default :
      i = 0;
      break;
    }
    
    *memBlockCount = i;

    std::cout << "Return Block Count = " << *memBlockCount << std::endl;
    return MXDI_STATUS_OK;
}


MxdiReturn_t
CarbonPciTarget_MxDI::MxdiMemWrite( MxdiAddrComplete_t startAddress
                                  , MxU32 unitsToWrite
                                  , MxU32 unitSizeInBytes
                                  , const MxU8 *data
                                  , MxU32 *actualNumOfUnitsWritten
                                  , MxU8 doSideEffects )
{
  UNUSEDARG(doSideEffects);

  MxU32 i = 0;
  sparseMem_t mem;
    
  // Select Memory Space
  switch(startAddress.location.space) {
  case 0 :
    for ( i = 0; i < unitsToWrite; i++ ) {
      MxU32 tmp = 0, j;
      for(j = 0; j < unitSizeInBytes; j++)
	tmp |= (data[i * unitSizeInBytes + j] << (8*j));
      target->mCfgSpace.setRegisterValue(startAddress.location.addr + i, tmp);
    }
    break;
  case 1 :
    mem = target->mMemSpace;
  case 2 :
    mem = target->mIOSpace;

    for ( i = 0; i < unitsToWrite; i++ ) {
      MxU64 addr  = startAddress.location.addr+i;
      MxU64 waddr = addr & 0xfffffffffffffffcll;
      MxU32 baddr = addr & 0x3;
      MxU32 mask  = 0xff000000 >> (8*baddr);
      MxU32 tmp   = data[i] << (24-(8*baddr));
      
      // Write the data to memory.
      tmp  = (carbonXSparseMemReadWord32(mem, waddr)) & ~mask | (tmp & mask);
      carbonXSparseMemWriteWord32(mem, waddr, tmp);
    }

    break;
  default :
    *actualNumOfUnitsWritten = 0;
    return MXDI_STATUS_GeneralError;
  }

    
  *actualNumOfUnitsWritten = i / unitSizeInBytes;
  return MXDI_STATUS_OK;
}


MxdiReturn_t
CarbonPciTarget_MxDI::MxdiMemRead( MxdiAddrComplete_t startAddress
                                 , MxU32 unitsToRead
                                 , MxU32 unitSizeInBytes
                                 , MxU8 *data
                                 , MxU32 *actualNumOfUnitsRead
                                 , MxU8 doSideEffects )
{
  UNUSEDARG(doSideEffects);
  
  MxU32 i;
  sparseMem_t mem;
  
  switch(startAddress.location.space) {
  case 0 :
    for ( i = 0; i < unitsToRead; i++ ) {
      MxU32 tmp = 0, j;
      tmp = target->mCfgSpace.getRegisterValue(startAddress.location.addr + i);
      for(j = 0; j < unitSizeInBytes; j++)
	data[i * unitSizeInBytes + j] = (tmp >> (8*j)) & 0xff;
    }
    break;
  case 1 :
    mem = target->mMemSpace;
  case 2 :
    mem = target->mIOSpace;
  
    for (i = 0; i < unitsToRead * unitSizeInBytes; i++) {    
      MxU32 tmp   = 0;
      MxU64 addr  = startAddress.location.addr+i;
      MxU64 waddr = addr & 0xfffffffffffffffcll;
      MxU32 baddr = addr & 0x3;
      
      // Read the data from memory.
      tmp = carbonXSparseMemReadWord32(mem, waddr);
      
      data[i] = (tmp >> (24-(8*baddr))) & 0xFF;
    }
    break;

  default :
    *actualNumOfUnitsRead = 0;
    return MXDI_STATUS_GeneralError;
  }
  
  *actualNumOfUnitsRead = i / unitSizeInBytes;
  return MXDI_STATUS_OK;
}


MxU32 CarbonPciTarget_MxDI::getReg(MxU32 addr, MxU32 bits) {
  MxU32 mask, shift;

  shift = (addr & 0x3) * 8;

  if(bits == 32) mask = 0xffffffff;
  else mask = ((1<<bits)-1);

  return (target->mCfgSpace.getRegisterValue(addr >> 2) >> shift) & mask;
}

void CarbonPciTarget_MxDI::setReg(MxU32 addr, MxU32 data, MxU32 bits) {
  MxU32 mask, shift;

  shift = (addr & 0x3) * 8;

  if(bits == 32) mask = 0xffffffff;
  else mask = ((1<<bits)-1) << shift;
  
  if(addr == 0)
  std::cout << hex << "CarbonPciTarget_MxDI::setReg: addr: " << addr << " data: " << data << " bits: " << bits
	    << " mask: " << mask << " shift: " << shift << std::endl;

  target->mCfgSpace.setRegisterValue(addr >> 2, ((data << shift) & mask) | (target->mCfgSpace.getRegisterValue(addr >> 2) & ~mask));
}
