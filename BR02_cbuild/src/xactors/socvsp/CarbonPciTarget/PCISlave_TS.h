// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef _PCISlave_TS_H_
#define _PCISlave_TS_H_

#include "maxsim.h"


class CarbonPciTarget;

class PCISlave_TS: public sc_mx_transaction_slave
{
    CarbonPciTarget *owner;

    //Memory Map Editor Support
    MxMemoryMapConstraints puMemoryMapConstraints;

public:
    PCISlave_TS( CarbonPciTarget *_owner);
    virtual ~PCISlave_TS() {}

public:

    // The Master Port is using the Read metod to read the CsWrite list from the slave
    // Address 0, holds the number of Cs Writes in the list
    // Address 1 returns an array with the CS Address in the first element and
    // the CS Data in the second element
    virtual MxStatus read(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus write(MxU64 addr, MxU32* value, MxU32* ctrl);
    
    /* MxSI 3.0: new shared-memory based asynchronous transaction functions */
    virtual void driveTransaction(MxTransactionInfo* info);

    /* Memory map functions */
    virtual int getNumRegions();
    virtual void getAddressRegions(MxU64* start, MxU64* size, string* name);

    /* MxSI 3.0: now the address regions can be set from the outside! */
    virtual void setAddressRegions(MxU64* start, MxU64* size, string* name);
    virtual MxMemoryMapConstraints* getMappingConstraints();

    // Config Space Region
    MxMemRegion mConfigRegion;

    //Base Address for the Slave Port 
    MxU64 port_base_address;
};

#endif

