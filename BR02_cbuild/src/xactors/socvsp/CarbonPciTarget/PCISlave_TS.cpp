/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "PCISlave_TS.h"
#include "CarbonPciTarget.h"

#include "xactors/pci/carbonXPciSlave.h"
#include "xactors/socvsp/CarbonXTransInfo.h"

PCISlave_TS::PCISlave_TS( CarbonPciTarget *_owner ) : sc_mx_transaction_slave( "PCISlave_TS" )

{
  owner = _owner;
  setMxOwner(owner);
  MxTransactionProperties prop;
  memset(&prop,0,sizeof(prop));
  prop.mxsiVersion = MXSI_VERSION_6;     //the transaction version used for this transaction
  prop.useMultiCycleInterface = false;    // using read/write interface methods
  prop.addressBitwidth = 32;      // address bitwidth used for addressing of resources
  prop.mauBitwidth = 8;           // minimal addressable unit
  prop.dataBitwidth = 32;         // maximum data bitwidth transferred in a single cycle
  prop.isLittleEndian = true;     // alignment of MAUs
  prop.isOptional = false;                                // if true this port can be disabled
  prop.supportsAddressRegions = true;     // M/S can negotiate address mapping
  prop.numCtrlFields = 1;         // # of ctrl elements used / entry is used for transfer size
  prop.protocolID = 0x00000001;           // magic number of the protocol (Vendor/Protocol-ID):
  //The upper 16 bits = protocol implementation version
	
  //The lower 16 bits = actual protocol ID
  sprintf(prop.protocolName,"Carbon PCI Slave");         // The name of the  protocol being used
  // MxSI 6.0: slave port address regions forwarding
  prop.isAddrRegionForwarded = false; //true if addr region for this slave port is actually
	
  //forwarded to a master port, false otherwise
  prop.forwardAddrRegionToMasterPort = NULL; //master port of the  same component to which this slave port's addr region is forwarded
	
  setProperties(&prop);

	
  // Setup Configuration Space
  mConfigRegion.start  = 0;
  mConfigRegion.size   = 0x10000;
  mConfigRegion.name   = "Config_Space";

  // Memory Map Support. TODO Add Memory Map Constraints for Slave Port
  puMemoryMapConstraints.minRegionSize          = 16;
  puMemoryMapConstraints.maxRegionSize          = MxU64CONST(0x100000000);
  puMemoryMapConstraints.minAddress             = 0x0;
  puMemoryMapConstraints.maxAddress             = MxU64CONST(0xffffffff);
  puMemoryMapConstraints.minNumSupportedRegions = 0x1;
  puMemoryMapConstraints.maxNumSupportedRegions = 0x6;
  puMemoryMapConstraints.alignmentBlockSize     = 0x400 ; //the min block size where this slave's regions can be mapped 
  puMemoryMapConstraints.numFixedRegions        = 1;
  puMemoryMapConstraints.fixedRegionList        = &mConfigRegion;

  // TODO:  Add any additional constructor code here.
}

/* If no regions are defined, numRegions returns 0, indicating that the
 * port wants to be called for any MxU64 address */
int
PCISlave_TS::getNumRegions()
{

  // TODO:  Add your code here.
  return 1;
}

/* This function is also not pure virtual, so that the user
 * does not need to implement it if she/he doesn't want to use
 * address mapping */
void
PCISlave_TS::getAddressRegions(MxU64* start, MxU64* size, string* name)
{
  if (start && size && name)
    {
      start[0] = mConfigRegion.start;
      size[0]  = mConfigRegion.size;
      name[0]  = mConfigRegion.name;
    }
  // TODO: Add your code here.

}

/* Method to set the address regions */
void
PCISlave_TS::setAddressRegions(MxU64* start, MxU64* size, string* name)
{
  //Memory Map Editor Support
  if (start && size && name)
    {
      int i = 0;
      while (size[i] != 0 )
        {
	  if (i == 0)
	    {
	      mConfigRegion.start = start[i];
	      mConfigRegion.size = size[i];
	      mConfigRegion.name = name[i];
            }
	  else {
	    //TODO: Add your code here
	  }
#ifdef _DEBUG_
	  owner->message(MX_MSG_INFO, "Address Region: start= 0x%I64x size = 0x%I64x Name= %s",
			 start[i], size[i],
			 name[i].c_str());
#endif
	  i++;
        }
    }

}

MxMemoryMapConstraints*
PCISlave_TS::getMappingConstraints()
{
  return &puMemoryMapConstraints;

}

MxStatus
PCISlave_TS::read(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  // ctrl is not used
  (void)ctrl;

  MxStatus status = MX_STATUS_OK;

  CarbonXPciConfigRegs::CsListItem cs_item;
  
  switch (addr)
    {
    case 0:
      value[0] = owner->mCfgSpace.csListGetSize(); 
      break;
    case 1:
      if( !owner->mCfgSpace.getNextCsListItem(&cs_item) ) status = MX_STATUS_ERROR;
      else {
	value[0] = cs_item.first;
	value[1] = cs_item.second;
      }
      break;
    default:
      break;
    }

  return status;

}

MxStatus
PCISlave_TS::write(MxU64, MxU32*, MxU32*) {
  return MX_STATUS_NOTSUPPORTED;
}

/* shared-memory based asynchronous transaction functions */
void
PCISlave_TS::driveTransaction(MxTransactionInfo* info)
{
  // Cast to Carbon's Trans Info Structure
  CarbonXTransInfo *trans_info = reinterpret_cast<CarbonXTransInfo *>(info);

  // Now Extract the reponse Structure
  CarbonXTransReqT  req;
  CarbonXTransRespT resp;

  trans_info->getRequest(&req);

  // We got here so we have a request. Process it and send back the response
  int op = req.getOpcode() & 0x1f;
  
  //std::cout << "PCISlave_TS::driveTransaction: Request " << req;

//   owner->message(MX_MSG_INFO, "Slave:          A:%08llx  OP:%08x SZ:%3d  ST:%08x \n",
//  		 req.getAddress(), req.getOpcode(), req.getSize(), req.getStatus() 
//  		 );
        
  switch(op) {
  case PCI_CFG_WRITE_CMP :
    carbonXPciSlaveCfgWrite(&owner->mCfgSpace, &req, &resp);
    break;
    
  case PCI_CFG_READ :
    carbonXPciSlaveCfgRead(&owner->mCfgSpace, &req, &resp);
    break;
    
  case PCI_MEM_WRITE_CMP  :
    carbonXPciSlvSparseMemWrite(owner->mMemSpace, &owner->mCfgSpace, &req, &resp);
    break;
    
  case PCI_MEM_READ   : 
    carbonXPciSlvSparseMemRead(owner->mMemSpace, &owner->mCfgSpace, &req, &resp);
    break;
    
  default :
    // Send Empty Response
    resp.transaction(TBP_NXTREQ, 0LL, NULL, 0, 0, TBP_WIDTH_64P);
    
  }
  
  //resp.setStatus(resp.getStatus() | status);
  
  // Send Response back to the master
  //std::cout << "PCISlave_TS::driveTransaction: Response " << resp; 
  trans_info->setResponse(resp);
  trans_info->cts = CARBONX_SOCVSP_CTS_DONE;
}
