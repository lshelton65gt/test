// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef CarbonPciTarget_MxDI_H
#define CarbonPciTarget_MxDI_H

#include "maxsim.h"

class CarbonPciTarget;

class CarbonPciTarget_MxDI : public MxDIBase
{
public:
    CarbonPciTarget_MxDI(CarbonPciTarget* c);
    virtual ~CarbonPciTarget_MxDI();

public:

    // Register access functions
    MxdiReturn_t	MxdiRegGetGroups(   MxU32 groupIndex
                                        , MxU32 desiredNumOfRegGroups
                                        , MxU32* actualNumOfRegGroups
                                        , MxdiRegGroup_t* reg );

    MxdiReturn_t	MxdiRegGetMap(      MxU32 groupID
                                        , MxU32 regIndex
                                        , MxU32 registerSlots
                                        , MxU32* registerCount
                                        , MxdiRegInfo_t* reg );

    MxdiReturn_t	MxdiRegWrite(       MxU32 regCount
                                        , MxdiReg_t* reg
                                        , MxU32* numRegsWritten
                                        , MxU8 doSideEffects );

    MxdiReturn_t	MxdiRegRead(        MxU32 regCount
                                        , MxdiReg_t* reg
                                        , MxU32* numRegsRead
                                        , MxU8 doSideEffects );


    // Memory access functions
    MxdiReturn_t    MxdiMemGetSpaces(   MxU32 spaceIndex
                                        , MxU32 memSpaceSlots
                                        , MxU32* memSpaceCount
                                        , MxdiMemSpaceInfo_t* memSpace );

    MxdiReturn_t    MxdiMemGetBlocks(   MxU32 memorySpace
                                        , MxU32 blockIndex
                                        , MxU32 memBlockSlots
                                        , MxU32* memBlockCount
                                        , MxdiMemBlockInfo_t* memBlock );

    MxdiReturn_t    MxdiMemWrite(       MxdiAddrComplete_t startAddress
                                        , MxU32 unitsToWrite
                                        , MxU32 unitSizeInBytes
                                        , const MxU8 *data
                                        , MxU32 *actualNumOfUnitsWritten
                                        , MxU8 doSideEffects );

    MxdiReturn_t    MxdiMemRead(        MxdiAddrComplete_t startAddress
                                        , MxU32 unitsToRead
                                        , MxU32 unitSizeInBytes
                                        , MxU8 *data
                                        , MxU32 *actualNumOfUnitsRead
                                        , MxU8 doSideEffects );

private:
    MxU32 getReg(MxU32 addr, MxU32 bits);
    void setReg(MxU32 addr, MxU32 data, MxU32 bits);
    CarbonPciTarget*		    target;

    // Register related info
    MxdiRegInfo_t*		    regInfo;
    MxdiRegGroup_t*		    regGroup;

    // Memory related info
    MxdiMemSpaceInfo_t*		memSpaceInfo;
    MxdiMemBlockInfo_t*		configMemBlockInfo;
    MxdiMemBlockInfo_t*		memMemBlockInfo;
    MxdiMemBlockInfo_t*		ioMemBlockInfo;
    MxU32                       mMemBlockBar[7];
    MxU32                       mIOBlockBar[7];
};

#endif

