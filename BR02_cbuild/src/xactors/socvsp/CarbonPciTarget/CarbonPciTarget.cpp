/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "util/CarbonTypes.h"
#include <stdio.h>
#include "CarbonPciTarget.h"
#include "PCISlave_TS.h"
#include "CarbonPciTarget_MxDI.h"
#include "CarbonXSOCVSP.h"

CarbonPciTarget::CarbonPciTarget(sc_mx_m_base* c, const string &s) : sc_mx_module(c, s)
{
    initComplete = false;

    PCISlave_TSlave = new PCISlave_TS( this );
    registerPort( PCISlave_TSlave, "PCISlave" );

    MxTransactionProperties prop;
    memset(&prop,0,sizeof(prop));
    prop.mxsiVersion = MXSI_VERSION_6;      //the transaction version used for this transaction
    prop.useMultiCycleInterface = false;    // using read/write interface methods
    prop.addressBitwidth = 32;              // address bitwidth used for addressing of resources 
    prop.mauBitwidth = 8;                   // minimal addressable unit 
    prop.dataBitwidth = 32;                 // maximum data bitwidth transferred in a single cycle 
    prop.isLittleEndian = true;             // alignment of MAUs 
    prop.isOptional = false;				// if true this port can be disabled 
    prop.supportsAddressRegions = true;	    // M/S can negotiate address mapping
    prop.numCtrlFields = 1;                 // # of ctrl elements used / entry is used for transfer size
    prop.protocolID = CARBON_SOCVSP_PROTID_PCIS; // magic number of the protocol (Vendor/Protocol-ID): 
				                             //The upper 16 bits = protocol implementation version
											//The lower 16 bits = actual protocol ID
    sprintf(prop.protocolName,"Carbon PCI");         // The name of the  protocol being used
    // MxSI 6.0: slave port address regions forwarding 
    prop.isAddrRegionForwarded = false;     // true if addr region for this slave port is actually 
    //                                         forwarded to a master port, false otherwise
    prop.forwardAddrRegionToMasterPort = NULL; // master port of the  same component to which this slave port's addr region is forwarded

    PCISlave_TSlave->setProperties(&prop);

    // Clocking the components
    SC_MX_CLOCKED();
    // Register ourself as a clock slave port.
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");

    // Clear Parameters
    memset(p_BarSize, 0, sizeof(MxU64) * 6);
    memset(p_BarIsIOSpace, 0, sizeof(bool) * 6);
    memset(p_BarIsPrefetch, 0, sizeof(bool) * 6);
    memset(p_BarIs64Bit, 0, sizeof(bool) * 6);

    mxdi = NULL; 

    // Place parameter definitions here:
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL);
    defineParameter( "BAR 0 Size", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "BAR 0 64 bit addr", "false", MX_PARAM_BOOL, false);
    // defineParameter( "BAR 0 is IO Space", "false", MX_PARAM_BOOL, false); // Don't support IO Space in this module yet
    defineParameter( "BAR 0 Prefetchable", "false", MX_PARAM_BOOL, false);

    defineParameter( "BAR 1 Size", "0x0", MX_PARAM_VALUE, false);
    // defineParameter( "BAR 1 is IO Space", "false", MX_PARAM_BOOL, false); // Don't support IO Space in this module yet
    defineParameter( "BAR 1 Prefetchable", "false", MX_PARAM_BOOL, false);

    defineParameter( "BAR 2 Size", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "BAR 2 64 bit addr", "false", MX_PARAM_BOOL, false);
    // defineParameter( "BAR 2 is IO Space", "false", MX_PARAM_BOOL, false); // Don't support IO Space in this module yet
    defineParameter( "BAR 2 Prefetchable", "false", MX_PARAM_BOOL, false);

    defineParameter( "BAR 3 Size", "0x0", MX_PARAM_VALUE, false);
    // defineParameter( "BAR 3 is IO Space", "false", MX_PARAM_BOOL, false); // Don't support IO Space in this module yet
    defineParameter( "BAR 3 Prefetchable", "false", MX_PARAM_BOOL, false);

    defineParameter( "BAR 4 Size", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "BAR 4 64 bit addr", "false", MX_PARAM_BOOL, false);
    // defineParameter( "BAR 4 is IO Space", "false", MX_PARAM_BOOL, false); // Don't support IO Space in this module yet
    defineParameter( "BAR 4 Prefetchable", "false", MX_PARAM_BOOL, false);

    defineParameter( "BAR 5 Size", "0x0", MX_PARAM_VALUE, false);
    // defineParameter( "BAR 5 is IO Space", "false", MX_PARAM_BOOL, false); // Don't support IO Space in this module yet
    defineParameter( "BAR 5 Prefetchable", "false", MX_PARAM_BOOL, false);

}

CarbonPciTarget::~CarbonPciTarget()
{

    delete PCISlave_TSlave;

}

void
CarbonPciTarget::communicate()
{
	// the following message will be printed only in the debug version of MxExplorer
#ifdef _DEBUG_
	if (p_enableDbgMsg == true)
	{
		message(MX_MSG_DEBUG,"Executing communicate function");
	}
#endif

	// TODO:  Add your communicate code here.
	// ...
}

void
CarbonPciTarget::update()
{
	// the following message will be printed only in the debug version of MxExplorer
#ifdef _DEBUG_
	if (p_enableDbgMsg == true)
	{
		message(MX_MSG_DEBUG,"Executing update function");
	}
#endif

	 // TODO:  Add your update code here.
	 // ...
}

void 
CarbonPciTarget::init()
{

   // Create MXDI Interface
    mxdi = new CarbonPciTarget_MxDI(this);
    r_reg0 = 0;
    r_reg1 = 0;
    r_DeviceID = 0;

	
    // Initialize ourself for save/restore.
    initStream( this );


    // Allocate memory and initialize data here:
    // ...
    mMemSpace = carbonXSparseMemCreateNew();
    mIOSpace  = carbonXSparseMemCreateNew();
    
    for(int i = 0; i < 6; i++) {
      if(p_BarSize[i] > 0) {
	CarbonXPciSlaveConfigureIF::MemSpaceT mem_type = 
	  p_BarIsIOSpace[i] ? CarbonXPciSlaveConfigureIF::eIOSpace : CarbonXPciSlaveConfigureIF::eMemSpace;
	
	CarbonXPciSlaveConfigureIF::AddrWidthT width = 
	  p_BarIs64Bit[i] ? CarbonXPciSlaveConfigureIF::eWidth64b : CarbonXPciSlaveConfigureIF::eWidth32b;
	
	CarbonXPciSlaveConfigureIF::PrefetchT prefetch = 
	  p_BarIsPrefetch[i] ? CarbonXPciSlaveConfigureIF::ePrefetch : CarbonXPciSlaveConfigureIF::eNoPrefetch;
	
	
	mCfgSpace.setupBaseAddr(i, p_BarSize[i], mem_type, width, prefetch);
      }
    }

    // Call the base class after this has been initialized.
    sc_mx_module::init();

    // Set a flag that init stage has been completed
    initComplete = true;
}

void
CarbonPciTarget::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
    // Add your reset behavior here:
    // ...



    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
}

void 
CarbonPciTarget::terminate()
{
    // Call the base class first.
    sc_mx_module::terminate();

    // Release the MXDI Interface
    if(mxdi!=NULL) {delete mxdi; mxdi=NULL;}

    // Free memory which has been allocated in init stage
    // ...
    carbonXSparseMemDestroy(mMemSpace);
    carbonXSparseMemDestroy(mIOSpace);
}

void
CarbonPciTarget::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;

    if (name == "Enable Debug Messages")
    {
      status = MxConvertStringToValue( value, &p_enableDbgMsg  );
    }

    //  BAR 0 Parameters
    else if (name == "BAR 0 Size")
    {
      status = MxConvertStringToValue( value, &p_BarSize[0] );
    }
    else if (name == "BAR 0 64 bit addr")
    {
      status = MxConvertStringToValue( value, &p_BarIs64Bit[0] );
    }
    else if (name == "BAR 0 Prefetchable")
    {
      status = MxConvertStringToValue( value, &p_BarIsPrefetch[0] );
    }
    else if (name == "BAR 0 is IO Space")
    {
      status = MxConvertStringToValue( value, &p_BarIsIOSpace[0] );
    }

    //  BAR 1 Parameters
    else if (name == "BAR 1 Size")
    {
      status = MxConvertStringToValue( value, &p_BarSize[1] );
    }
    else if (name == "BAR 1 Prefetchable")
    {
      status = MxConvertStringToValue( value, &p_BarIsPrefetch[1] );
    }
    else if (name == "BAR 1 is IO Space")
    {
      status = MxConvertStringToValue( value, &p_BarIsIOSpace[1] );
    }

    //  BAR 2 Parameters
    else if (name == "BAR 2 Size")
    {
      status = MxConvertStringToValue( value, &p_BarSize[2] );
    }
    else if (name == "BAR 2 64 bit addr")
    {
      status = MxConvertStringToValue( value, &p_BarIs64Bit[2] );
    }
    else if (name == "BAR 2 Prefetchable")
    {
      status = MxConvertStringToValue( value, &p_BarIsPrefetch[2] );
    }
    else if (name == "BAR 2 is IO Space")
    {
      status = MxConvertStringToValue( value, &p_BarIsIOSpace[2] );
    }

    //  BAR 3 Parameters
    else if (name == "BAR 3 Size")
    {
      status = MxConvertStringToValue( value, &p_BarSize[3] );
    }
    else if (name == "BAR 3 Prefetchable")
    {
      status = MxConvertStringToValue( value, &p_BarIsPrefetch[3] );
    }
    else if (name == "BAR 3 is IO Space")
    {
      status = MxConvertStringToValue( value, &p_BarIsIOSpace[3] );
    }

    //  BAR 4 Parameters
    else if (name == "BAR 4 Size")
    {
      status = MxConvertStringToValue( value, &p_BarSize[4] );
    }
    else if (name == "BAR 4 64 bit addr")
    {
      status = MxConvertStringToValue( value, &p_BarIs64Bit[4] );
    }
    else if (name == "BAR 4 Prefetchable")
    {
      status = MxConvertStringToValue( value, &p_BarIsPrefetch[4] );
    }
    else if (name == "BAR 4 is IO Space")
    {
      status = MxConvertStringToValue( value, &p_BarIsIOSpace[4] );
    }

    //  BAR 5 Parameters
    else if (name == "BAR 5 Size")
    {
      status = MxConvertStringToValue( value, &p_BarSize[5] );
    }
    else if (name == "BAR 5 Prefetchable")
    {
      status = MxConvertStringToValue( value, &p_BarIsPrefetch[5] );
    }
    else if (name == "BAR 5 is IO Space")
    {
      status = MxConvertStringToValue( value, &p_BarIsIOSpace[5] );
    }

    if ( status == MxConvert_SUCCESS )
    {
        sc_mx_module::setParameter(name, value);
    }
    else
    {
        message( MX_MSG_WARNING, "CarbonPciTarget::setParameter: Illegal value <%s> "
		 "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

string
CarbonPciTarget::getProperty( MxPropertyType property )
{
    string description; 
    switch ( property ) 
    {    
	 case MX_PROP_LOADFILE_EXTENSION:
		return "";
	 case MX_PROP_REPORT_FILE_EXT:
		return "yes";
	 case MX_PROP_COMPONENT_TYPE:
		return "Memory"; 
	 case MX_PROP_COMPONENT_VERSION:
		return "0.1";
	 case MX_PROP_MSG_PREPEND_NAME:
		return "yes"; 
	 case MX_PROP_DESCRIPTION:
		description = "Generic PCI Target Device";
		return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
	 case MX_PROP_MXDI_SUPPORT:
		return "yes";
	 case MX_PROP_SAVE_RESTORE:
		return "yes";
	 default:
		return "";
    }
}

string
CarbonPciTarget::getName(void)
{
    return "CarbonPciTarget";
}

MXDI*
CarbonPciTarget::getMxDI()
{
    return mxdi;
}


bool
CarbonPciTarget::saveData( MxODataStream &data )
{
    data << r_reg0;
    data << r_reg1;
    data << r_DeviceID;


    // TODO: Add your save code here.
    // Below line shows how the example state variable is saved. 
    // data << exampleStateVariable;


    // return save was successful
	return true;
}

bool
CarbonPciTarget::restoreData( MxIDataStream &data )
{
    data >> r_reg0;
    data >> r_reg1;
    data >> r_DeviceID;


    // TODO: Add your restore code here.
    // Below line shows how the example state variable is saved. 
    // data >> exampleStateVariable;


    // return restore was successful
	return true;
}

void
CarbonPciTarget::initSignalPort(sc_mx_signal_p_base* signalIf)
{
    MxSignalProperties prop_sm1;
    memset(&prop_sm1,0,sizeof(prop_sm1));
    prop_sm1.isOptional=false;
    prop_sm1.bitwidth=32;
    signalIf->setProperties(&prop_sm1);
}

// This initTransactionPort() is only called for Transaction Master Ports.
// Do not call this function for any port which has its own initialization code.
void
CarbonPciTarget::initTransactionPort(sc_mx_transaction_p_base* transIf)
{
    MxTransactionProperties prop;
    memset(&prop,0,sizeof(prop));
    prop.mxsiVersion = MXSI_VERSION_6;      //the transaction version used for this transaction
    prop.useMultiCycleInterface = false;    // using read/write interface methods
    prop.addressBitwidth = 32;              // address bitwidth used for addressing of resources 
    prop.mauBitwidth = 8;                   // minimal addressable unit 
    prop.dataBitwidth = 32;                 // maximum data bitwidth transferred in a single cycle 
    prop.isLittleEndian = true;             // alignment of MAUs 
    prop.isOptional = false;				// if true this port can be disabled 
    prop.supportsAddressRegions = true;	    // M/S can negotiate address mapping
    prop.numCtrlFields = 1;                 // # of ctrl elements used / entry is used for transfer size
    prop.protocolID = CARBON_SOCVSP_PROTID_PCIS; // magic number of the protocol (Vendor/Protocol-ID): 
				                             //The upper 16 bits = protocol implementation version
											//The lower 16 bits = actual protocol ID
    sprintf(prop.protocolName,"Carbon PCI");         // The name of the  protocol being used
    // MxSI 6.0: slave port address regions forwarding 
    prop.isAddrRegionForwarded = false;     // true if addr region for this slave port is actually 
    //                                         forwarded to a master port, false otherwise
    prop.forwardAddrRegionToMasterPort = NULL; // master port of the  same component to which this slave port's addr region is forwarded
    transIf->setProperties(&prop);
}

/************************
 * CarbonPciTarget Factory class
 ***********************/

class CarbonPciTargetFactory : public MxFactory
{
public:
    CarbonPciTargetFactory() : MxFactory ( "CarbonPciTarget" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
        return new CarbonPciTarget(c, id); 
    }
};

/**
 *  Entry point into the memory components (from SoC Designer)
 */
extern "C" DLLEXPORT void MxInit(void)
{
    new CarbonPciTargetFactory();
}
