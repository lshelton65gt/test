/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#include "xactors/carbonX.h"
#include "CarbonXTransInfo.h"

CarbonXTransInfo::CarbonXTransInfo(sc_mx_transaction_p_base* port,
				   const carbonXTransactionStruct& trans) : 
  MxTransactionInfo(port)
{
  init(); // Initialize Fields
  
  setRequest(trans);
}

CarbonXTransInfo::CarbonXTransInfo(sc_mx_transaction_p_base* port) : 
  MxTransactionInfo(port)
{
  init(); // Initialize Fields
}

CarbonXTransInfo::~CarbonXTransInfo()
{
    if (dataWr != NULL) {
      delete [] dataWr;
    }
    if (dataRd != NULL) {
      delete [] dataRd;
    }
    if (masterFlags != NULL) {
      delete [] masterFlags;
    }
    if (slaveFlags != NULL) {
      delete [] slaveFlags;
    }
    if (status != NULL) {
      delete [] status;
    }
}

// Copy Contructor
CarbonXTransInfo::CarbonXTransInfo(const CarbonXTransInfo &other) :
  MxTransactionInfo(other.initiator)
{
  doCopy(other);
}

void CarbonXTransInfo::init() {
  nts       = CARBONX_SOCVSP_NTS;
  cts       = CARBONX_SOCVSP_CTS_NOT_STARTED;
  notify    = MX_NOTIFY_NO;
  access    = MX_ACCESS_RMW; // Always set to read modify write for now so that any response is copied to the transaction
  dataSize  = 0;
  dataBeats = 0;  // Not used with Carbon transactors
  dataWr    = NULL;
  dataRd    = NULL;

  // Init Master Flags
  masterFlags = new MxU32 [CARBONX_SOCVSP_NUM_MASTER_FLAGS];
  memset(masterFlags, 0, sizeof(MxU32)*CARBONX_SOCVSP_NUM_MASTER_FLAGS);
  
  // Init Slave Flags
  slaveFlags = new MxU32 [CARBONX_SOCVSP_NUM_SLAVE_FLAGS];
  memset(slaveFlags, 0, sizeof(MxU32)*CARBONX_SOCVSP_NUM_SLAVE_FLAGS);
  
  // Init Maxsim status to default -- not used in Carbon transactors
  status = new MxTransactionStatus [nts];
  for (unsigned int i=0; i<nts; i++) status[i] = MX_MASTER_WAIT;
  
  // Master Port on the Slave is not enabled
  masterFlags[CARBONX_SOCVSP_MASTER_FLAG_ENABLE_MASTER_PORT] = CARBONX_SOCVSP_DISABLE_MASTER_PORT;
  
}

// Update Response Data
void CarbonXTransInfo::setResponse(const carbonXTransactionStruct& rsp) {

  // If There already is an allocated databuffer, we need to delete it
  // and allocate a new, so we can be sure that we have enough space
  // for the data.
  if(dataRd != NULL) {
    delete [] dataRd; dataRd = NULL;
  }
  // Allocate Memory for Data buffer
  if(rsp.mHasData && rsp.mSize>0) dataRd = reinterpret_cast<MxU32*>(new UInt8[carbonXCalculateActualTransactionSize(&rsp)]);
  
  addr     = rsp.mAddress;
  dataSize = rsp.mSize;
  slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_OPCODE]   = rsp.mOpcode;
  slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_STATUS]   = rsp.mStatus;
  slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_WIDTH]    = rsp.mWidth;
  slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_TRANS_ID] = rsp.mTransId;
  if ((dataRd != NULL) && (rsp.mData != NULL) && (rsp.mSize != 0)) {
    memcpy(dataRd, rsp.mData, carbonXCalculateActualTransactionSize(&rsp));
  }
}

// Update Request Data
void CarbonXTransInfo::setRequest(const carbonXTransactionStruct& req) {

  // If There already is an allocated databuffer, we need to delete it
  // and allocate a new, so we can be sure that we have enough space
  // for the data.
  if(dataWr != NULL) {
    delete [] dataWr; dataWr = NULL;
  }
  // Allocate Memory for Data buffer
  if(req.mHasData && req.mSize>0) dataWr = reinterpret_cast<MxU32*>(new UInt8[carbonXCalculateActualTransactionSize(&req)]);
  
  addr     = req.mAddress;
  dataSize = req.mSize;
  masterFlags[CARBONX_SOCVSP_MASTER_FLAG_OPCODE]   = req.mOpcode;
  masterFlags[CARBONX_SOCVSP_MASTER_FLAG_STATUS]   = req.mStatus;
  masterFlags[CARBONX_SOCVSP_MASTER_FLAG_WIDTH]    = req.mWidth;
  masterFlags[CARBONX_SOCVSP_MASTER_FLAG_TRANS_ID] = req.mTransId;
  masterFlags[CARBONX_SOCVSP_MASTER_FLAG_RPTCNT]   = req.mRepeatCnt;
  if ((dataWr != NULL) && (req.mData != NULL) && (req.mSize != 0)) {
    memcpy(dataWr, req.mData, carbonXCalculateActualTransactionSize(&req));
  }
}

void CarbonXTransInfo::getResponse(carbonXTransactionStruct* rsp) {
  rsp->mOpcode   = slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_OPCODE];
  rsp->mAddress  = addr;
  rsp->mSize     = dataSize;
  rsp->mStatus   = slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_STATUS];
  rsp->mWidth    = slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_WIDTH];
  rsp->mTransId  = slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_TRANS_ID];
  rsp->mHasData  = ((access == MX_ACCESS_READ) || (access == MX_ACCESS_RMW)) && (dataSize > 0) && (dataRd != NULL);
  
  // Allocate Data Buffer Space
  carbonXReallocateTransactionBuffer(rsp);
  
  // Copy Data
  if(rsp->mHasData) memcpy(rsp->mData, dataRd, carbonXCalculateActualTransactionSize(rsp)); 
}

// Get Request Data
void CarbonXTransInfo::getRequest(carbonXTransactionStruct* req) {
  req->mOpcode    = masterFlags[CARBONX_SOCVSP_MASTER_FLAG_OPCODE];
  req->mAddress   = addr;
  req->mSize      = dataSize;
  req->mStatus    = masterFlags[CARBONX_SOCVSP_MASTER_FLAG_STATUS];
  req->mWidth     = masterFlags[CARBONX_SOCVSP_MASTER_FLAG_WIDTH];
  req->mTransId   = masterFlags[CARBONX_SOCVSP_MASTER_FLAG_TRANS_ID];
  req->mRepeatCnt = slaveFlags[CARBONX_SOCVSP_MASTER_FLAG_RPTCNT];
  req->mHasData   = ((access == MX_ACCESS_WRITE) || (access == MX_ACCESS_RMW)) && (dataSize > 0);

  // Allocate Data Buffer Space
  carbonXReallocateTransactionBuffer(req);

  // Copy Data
  if(req->mHasData && dataWr) memcpy(req->mData, dataWr, carbonXCalculateActualTransactionSize(req)); 
}

UInt32 CarbonXTransInfo::readArray8(UInt8 *buffer) const {
  memcpy(buffer, dataRd, copySize());
    return dataSize;
}
UInt32 CarbonXTransInfo::readArray16(UInt16 *buffer) const {
  memcpy(buffer, dataRd, copySize());
  return dataSize;
}
UInt32 CarbonXTransInfo::readArray32(UInt32 *buffer) const {
  memcpy(buffer, dataRd, copySize());
  return dataSize;
}
UInt32 CarbonXTransInfo::readArray64(UInt64 *buffer) const {
  memcpy(buffer, dataRd, copySize());
  return dataSize;
}
  
UInt32 CarbonXTransInfo::getStatus() const {
  return masterFlags[CARBONX_SOCVSP_MASTER_FLAG_STATUS];
}

// = operator is private because the copy is not complete.
//    Parts of MxTransactionInfo are not copied properly.
//    Use copy constructor to make copies of transactions.
CarbonXTransInfo& CarbonXTransInfo::operator=(const CarbonXTransInfo& other) {
  if (&other != this) {
    doCopy(other);
  }
  return *this;
}

void CarbonXTransInfo::doCopy(const CarbonXTransInfo& other)
{
  // Delete Old Data
  if(dataWr != 0) delete [] dataWr;
  if(dataRd != 0) delete [] dataRd;
  
  // Initialize Structure
  init();

  initiator = other.initiator;
  nts       = other.nts;
  cts       = other.cts;
  notify    = other.notify;
  access    = other.access;
  addr      = other.addr;
  dataSize  = other.dataSize;
  dataBeats = other.dataBeats;
  dataWr    = NULL;
  dataRd    = NULL;    

  if (dataSize > 0) {
    int copy_size = (other.dataSize + 3) / 4;
    if( (access == MX_ACCESS_WRITE || access == MX_ACCESS_RMW) && other.dataWr != 0) {
      dataWr = new MxU32 [copy_size];
      memcpy(dataWr, other.dataWr, copy_size * sizeof(MxU32));
    }
    if( (access == MX_ACCESS_READ || access == MX_ACCESS_RMW) && other.dataRd != 0) {
      dataRd = new MxU32 [copy_size];
      memcpy(dataRd, other.dataRd, copy_size * sizeof(MxU32));
    }
  }

  memcpy(masterFlags, other.masterFlags, sizeof(MxU32 [CARBONX_SOCVSP_NUM_MASTER_FLAGS]));
  memcpy(slaveFlags,  other.slaveFlags,  sizeof(MxU32 [CARBONX_SOCVSP_NUM_SLAVE_FLAGS]));
  memcpy(status, other.status, sizeof(MxTransactionStatus)*nts);
}

UInt32 CarbonXTransInfo::copySize() const {
  UInt32 sizeExt;
  UInt32 size;
  UInt32 width = slaveFlags[CARBONX_SOCVSP_SLAVE_FLAG_WIDTH];
  
  sizeExt = (dataSize%(width&0xf)) ? (width&0xf)-(dataSize%(width&0xf)) : 0;
  size = dataSize + sizeExt;
  
  return size;
}
