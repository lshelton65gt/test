// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxtransinfo__
#define __carbonxtransinfo__

#include "maxsim.h"

#ifndef TBP_USR_H
#include "xactors/carbonX.h"
#endif

#define CARBONX_SOCVSP_NTS 5
#define CARBONX_SOCVSP_CTS_NOT_STARTED 0
#define CARBONX_SOCVSP_CTS_QUEUED      1
#define CARBONX_SOCVSP_CTS_RUNNING     2
#define CARBONX_SOCVSP_CTS_DONE        3
#define CARBONX_SOCVSP_CTS_CANCEL      4

#define CARBONX_SOCVSP_NUM_MASTER_FLAGS 6
#define CARBONX_SOCVSP_MASTER_FLAG_OPCODE 0
#define CARBONX_SOCVSP_MASTER_FLAG_RPTCNT 1
#define CARBONX_SOCVSP_MASTER_FLAG_STATUS 2
#define CARBONX_SOCVSP_MASTER_FLAG_WIDTH  3
#define CARBONX_SOCVSP_MASTER_FLAG_ENABLE_MASTER_PORT 4
#define CARBONX_SOCVSP_MASTER_FLAG_TRANS_ID 5

#define CARBONX_SOCVSP_NUM_SLAVE_FLAGS 4
#define CARBONX_SOCVSP_SLAVE_FLAG_STATUS 0
#define CARBONX_SOCVSP_SLAVE_FLAG_OPCODE 1
#define CARBONX_SOCVSP_SLAVE_FLAG_WIDTH  2
#define CARBONX_SOCVSP_SLAVE_FLAG_TRANS_ID 3

#define CARBONX_SOCVSP_DISABLE_MASTER_PORT 0
#define CARBONX_SOCVSP_ENABLE_MASTER_PORT  1

class CarbonXTransInfo : public MxTransactionInfo
{
public :
  
  CarbonXTransInfo(sc_mx_transaction_p_base* port,
		   const carbonXTransactionStruct& trans);
    
  CarbonXTransInfo(sc_mx_transaction_p_base* port);

  ~CarbonXTransInfo();

  // Copy Contructor
  CarbonXTransInfo(const CarbonXTransInfo &other);
  
  void init();
  
  // Update Response Data
  void setResponse(const carbonXTransactionStruct& rsp);

  // Update Request Data
  void setRequest(const carbonXTransactionStruct& req);

  void getResponse(carbonXTransactionStruct* rsp);
  
  // Get Request Data
  void getRequest(carbonXTransactionStruct* req);

  // Methods for processing return Data
  CarbonUInt8*     getData8()     const {return reinterpret_cast<CarbonUInt8*>(dataRd); }
  CarbonUInt16*    getData16()    const {return reinterpret_cast<CarbonUInt16*>(dataRd); }
  CarbonUInt32*    getData32()    const {return reinterpret_cast<CarbonUInt32*>(dataRd); }
  CarbonUInt64*    getData64()    const {return reinterpret_cast<CarbonUInt64*>(dataRd); }

  CarbonUInt8 read8() const {
    if(dataRd != NULL) return *(reinterpret_cast<CarbonUInt8 *>(dataRd));
    else              return 0;
  }
  CarbonUInt16 read16() const {
    if(dataRd != NULL) return *(reinterpret_cast<CarbonUInt16 *>(dataRd));
    else              return 0;
  }
  CarbonUInt32 read32() const {
    if(dataRd != NULL) return *(reinterpret_cast<CarbonUInt32 *>(dataRd));
    else              return 0;
  }
  CarbonUInt64 read64() const {
    if(dataRd != NULL) return *(reinterpret_cast<CarbonUInt64 *>(dataRd));
    else              return 0;
  }
  
  CarbonUInt32 readArray8(CarbonUInt8 *buffer) const;
  CarbonUInt32 readArray16(CarbonUInt16 *buffer) const;
  CarbonUInt32 readArray32(CarbonUInt32 *buffer) const;
  CarbonUInt32 readArray64(CarbonUInt64 *buffer) const;
  CarbonUInt32 getStatus() const;

private:

  // = operator is private because the copy is not complete.
  //    Parts of MxTransactionInfo are not copied properly.
  //    Use copy constructor to make copies of transactions.
  CarbonXTransInfo & operator=(const CarbonXTransInfo &other);

  void doCopy(const CarbonXTransInfo &other);

  CarbonUInt32 copySize() const;
};

#endif
