// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxpcietransinfo__
#define __carbonxpcietransinfo__

#include "maxsim.h"
#include "carbon/carbon_shelltypes.h"
#include "carbonXPcie.h"

class CarbonXPcieTransInfo : public MxTransactionInfo
{
public :
  
  enum PCIECtsType {
    eCtsNotStarted,
    eCtsQueued,
    eCtsRunning,
    eCtsDone,
    eCtsCancel
  };

  static const CarbonUInt32 cNts = 5;
  static const CarbonUInt32 cNumMasterFlags = 0;
  static const CarbonUInt32 cNumSlaveFlags = 0;
  
  //! predicate to find transaction id
  struct transid_eq : public std::unary_function<CarbonXPcieTransInfo *, bool> {
    CarbonSInt32 mTransId;
    transid_eq(SInt32 trans_id) : mTransId(trans_id){}
    bool operator() (const CarbonXPcieTransInfo *cmd) {
      if(cmd != NULL) return cmd->getTransId() == mTransId;
      else return false;
    }
  };

  //! predicate to find transaction id and command
  struct cmd_transid_eq : public std::unary_function<CarbonXPcieTransInfo *, bool> {
    CarbonSInt32 mTransId;
    CarbonXPcieCmdType mType;
    cmd_transid_eq(CarbonXPcieCmdType type, SInt32 trans_id) : mTransId(trans_id), mType(type) {}
    bool operator() (const CarbonXPcieTransInfo *cmd) {
      if(cmd != NULL)
	return (cmd->getTransId() == mTransId) && (cmd->getCmdByName() == mType);
      else return false;
    }
  };
  
  //! predicate to check CTS state 
  struct cts_state : public std::unary_function<CarbonXPcieTransInfo *, bool> {
    CarbonUInt32 mState;
    cts_state(PCIECtsType cts) : mState(cts) {}
    bool operator() (const CarbonXPcieTransInfo *cmd) {
      if(cmd != NULL)
	return (cmd->cts == mState);
      else return false;
    }
  };
  

  //! Void Constructor.
  /*!
    All values will be initialized to zero and the command type will be Msg.
  */
  CarbonXPcieTransInfo(sc_mx_transaction_p_base *port);
  //! Copy Constructor.
  /*!
    The contents of the CarbonXPcieTransInfo object passed in will be copied to the
    referenced object.
    \param orig A pointer to the CarbonXPcieTransInfo object to be copied.
  */
  CarbonXPcieTransInfo(const CarbonXPcieTransInfo& orig);
  //! CarbonXPcieTransInfo Constructor.
  /*!
    The contents of the CarbonXPcieTransInfo object passed in will be copied to the
    referenced object.
    \param orig A pointer to the CarbonXPcieTransInfo object to be copied.
  */
  CarbonXPcieTransInfo(sc_mx_transaction_p_base *port, const CarbonXPcieTLP *orig);
  //! Destructor.
  ~CarbonXPcieTransInfo();

   //! Get c-style string for the CarbonXPcieTransInfo::CarbonXPcieTransInfoType value.
   /*!
     This function is provided to enhance printing messages.
     \param type The type to get the print string for.
     \return The c-style string for message printing.
   */
  static const char* getStrFromType (CarbonXPcieCmdType type);

  //! Copy object function
  /*!
    The contents of the CarbonXPcieTransInfo object passed in will be copied
    to the referenced object.
    \param orig A pointer to the CarbonXPcieTransInfo object to be copied.
  */
  void   copy         (const CarbonXPcieTransInfo& orig);
  
  //! Sets the command type for the transaction.
  /*!
    \param cmd The 7-bit value to be set.
  */
  void   setCmd       (CarbonUInt32 cmd);
  //! Gets the command type for the transaction.
  /*!
    \return The 7-bit command type value.
  */
  CarbonUInt32 getCmd       (void) const;
  //! Sets the command type for the transaction.
  /*!
    \param cmd The CarbonXPcieCmdType value to be set.
  */
  void   setCmdByName (CarbonXPcieCmdType cmd);
  //! Gets the command type for the transaction.
  /*!
    \return The CarbonXPcieTransInfo::CarbonXPcieCmdType command type value.
  */
  CarbonXPcieCmdType getCmdByName (void) const;
  //! Sets the address for the transaction.
  /*!
    If only 32-bits are needed for the transaction then the upper 32-bits
    will be masked out when used.
    \param addr The 64-bit value to be set.
  */
  void   setAddr      (CarbonUInt64 addr);
  //! Gets the address for the transaction.
  /*!
    The value will be the same as passed into the setAddr() function,
    even if only 32-bits are used for this transaction.
    \return The 64-bit address value.
  */
  CarbonUInt64 getAddr      (void) const;
  //! Sets the requester ID for the transaction.
  /*!
    \param id The 16-bit value to be set.
  */
  void   setReqId     (CarbonUInt32 id);
  //! Gets the requester ID for the transaction.
  /*!
    \return The 16-bit requester ID.
  */
  CarbonUInt32 getReqId     (void) const;
  //! Sets the tag for the transaction.
  /*!
    \param tag The 8-bit value to be set.
  */
  void   setTag       (CarbonUInt32 tag);
  //! Gets the tag for the transaction.
  /*!
    \return The 8-bit tag value.
  */
  CarbonUInt32 getTag       (void) const;
  //! Sets the transaction register number and extended register number.
  /*!
    The numeric values are 10-bits wide, corresponding to the concatination
    of the 4-bit extended register number and 6-bit register number.
    \param reg The 10-bit value to be set.
  */
  void   setRegNum    (CarbonUInt32 reg);
  //! Gets the transaction register number and extended register number.
  /*!
    This value is 10-bits wide, corresponding to the concatination of
    the 4-bit extended register number and 6-bit register number.
    \return The 10-bits extended register number and register number.
  */
  CarbonUInt32 getRegNum    (void) const;
  //! Sets the completer ID for the transaction.
  /*!
    \param id The 16-bit value to be set.
  */
  void   setCompId    (CarbonUInt32 id);
  //! Gets the completer ID for the transaction.
  /*!
    \return The 16-bit completer ID value.
  */
  CarbonUInt32 getCompId    (void) const;
  //! Sets the message routing type for the transaction.
  /*!
    \param rout The 3-bit value to be set.
  */
  void   setMsgRout   (CarbonUInt32 rout);
  //! Gets the message routing type for the transaction.
  /*!
    \return The 3-bit message routing type.
  */
  CarbonUInt32 getMsgRout   (void) const;
  //! Sets the message code for the transaction.
  /*!
    \param code The 8-bit value to be set.
  */
  void   setMsgCode   (CarbonUInt32 code);
  //! Gets the message code for the transaction.
  /*!
    \return The 8-bit message code.
  */
  CarbonUInt32 getMsgCode   (void) const;
  //! Sets the lower address for the transaction.
  /*!
    \param addr The 7-bit value to be set.
  */
  void   setLowAddr   (CarbonUInt32 addr);
  //! Gets the lower address for the transaction.
  /*!
    \return The 7-bit lower address.
  */
  CarbonUInt32 getLowAddr   (void) const;
  //! Sets the byte count for the transaction.
  /*!
    The Byte Count field in the transaction, which equals the number
    of data bytes sent with a write or requested with a read.  In a
    Completion this may or may not equal the amount of data with the
    transaction.

    \param count The 12-bit value to be set.
  */
  void   setByteCount (CarbonUInt32 count);
  //! Gets the data byte count for the transaction.
  /*!
    \return The 12-bit byte count.
  */
  CarbonUInt32 getByteCount (void) const;
  //! Gets the double-word count for the 'Byte Count' value
  /*! 
    \deprecated
    This function has been replaced by getDataLength() and may be removed at any time.

    \return The number of DWords spanned by 'Byte Count' bytes.
    \sa getDataLength()
  */
  CarbonUInt32 getDWCount   (void) const;
  //! Sets the first double-word byte enable for the transaction.
  /*!
    \param be The 4-bit value for the FDWBE.
  */
  void   setFirstBE   (CarbonUInt32 be);
  //! Gets the first double-word byte enable value for the transaction.
  /*!
    \return The 4-bit value for the FDWBE.
  */
  CarbonUInt32 getFirstBE   (void) const;
  //! Sets the last double word byte enable for the transaction.
  /*!
    \param be The 4-bit value for the LDWBE.
  */
  void   setLastBE    (CarbonUInt32 be);
  //! Gets the last double-word byte enable value for the transaction.
  /*!
    \return The 4-bit value for the LDWBE.
  */
  CarbonUInt32 getLastBE    (void) const;
  //! Sets the completion status for the transaction.
  /*!
    \param status The 3-bit value to be set. 
  */
  void   setCplStatus (CarbonUInt32 status);
  //! Gets the completion status for the transaction.
  /*!
    \return The 3-bit completion status.
  */
  CarbonUInt32 getCplStatus (void) const;
  //! Sets the byte data for the transaction.
  //MJH - to be replaced, all data set as DWords, naturally aligned, written as 03_02_01_00.
  /*!
    \param index The index of the byte being set.
    \param data The value of the byte being set.
  */
  void   setDataByte  (CarbonUInt32 index, CarbonUInt8 data);
  //! Gets the data byte at the specified index for the transaction.
  //MJH - to be replaced, all data set as DWords, naturally aligned, written as 03_02_01_00.
  /*!
    If the byte \a index wasn't written then the return data will be 0x00.

    \param index The index of the byte being retrieved.
    \return The data byte at the specified index.
  */
  CarbonUInt8  getDataByte  (CarbonUInt32 index) const;

  //! Gets the data DWord at the specified index for the transaction.
  /*!
    If the DWord \a index wasn't written then the return data will be 0x00.

    \param index The index of the DWord being retrieved.
    \return The DWord at the specified index.
  */
  CarbonUInt32  getDWord  (CarbonUInt32 index) const;

  //! Sets a Double Word for the transaction.
  /*!
    \param index The index of the DWord being set.
    \param data The value of the DWord being set.
  */
  void   setDWord  (CarbonUInt32 index, CarbonUInt32 data);

  //! Resizes the byte data buffer
  /*!
    Either extend or truncate the byte data buffer.  If \a start is greater
    than zero then all elements from zero to \a start will be removed.  If
    \a end is before the last element, then all after it will be removed,
    otherwise zero data will be appended to the byte data buffer.

    \param start The index of the first element to keep.
    \param end The index of the last element to keep or create.
  */
  void   resizeDataBytes  (CarbonUInt32 start, CarbonUInt32 end);
  //! Gets the number of bytes in the data queue
  /*!
    The length of the data queue is from byte 0 through the last byte written.  If only
    byte 11 is written, then the data queue length is 12 bytes.

    \return The number of bytes of data set for the tranaction
  */
  CarbonUInt32 getDataByteCount (void) const;
  //! Sets the data length value in double-words
  /*!
    \param length The 10-bit data length for the transaction
  */
  void   setDataLength (CarbonUInt32 length);
  //! Gets the data length in double-words
  /*!
    \return The 10-bit data length for the transaction
  */
  CarbonUInt32 getDataLength (void) const;

  //! Gets the transaction ID
  /*!
    The transaction ID is a concatination of the Requester ID and the Tag values.  On completion
    transactions this information links the completion to the original request.  For all other
    transactions the return value is undefined.
    \return The transaction ID of the original request on completions.
  */
  SInt32 getTransId   (void) const;

  // Ease of use functions
  //! Gets whether the transaction is a read
  /*!
    \retval true If the command is a memory, i/o, or config read
    \retval false If the command is not a read
  */

  //! Set Start Time of the transaction
  void   setStartTime (CarbonUInt64 time);
  
  //! Get Start Time of the transaction
  CarbonUInt64 getStartTime (void) const;
  
  //! Set End Time of the transaction
  void   setEndTime (CarbonUInt64 time);
  
  //! Get End Time of the transaction
  CarbonUInt64 getEndTime (void) const;
  
  bool   isRead       (void) const;
  //! Gets whether the transaction is a write
  /*!
    \retval true If the command is a memory, i/o, or config write
    \retval false If the command is not a write
  */
  bool   isWrite      (void) const;
  
  //! Build a Memory Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param num_dwords Number of DWORDS to read, goes in the Length Field of the TLP
    \param first_be First BE Field
    \param last_be  Last  BE Field
  */
  void memReadReq(CarbonUInt8 tag, 
		  CarbonUInt64 addr, CarbonUInt32 num_dwords, 
		  CarbonUInt32 first_be, CarbonUInt64 last_be);
  
  //! Build Completer TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory Completer Transaction. 
    The function populates the following fields in the supplied TLP:
    Type, Length, Tag, Requester ID HDW Byte Count, Lower Address and Data
    The other fields are left unchanged!
    \param req_id Bus-, Device- and Funcion- Number of the corresponding Requester Transaction 
    \param tag Tag of the corresponding Requester Transaction
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS in the TLP
    \param byte_count Byte Count Field
    \param lower_addr Lower Address Field
  */
  void completer(CarbonUInt32 req_id, CarbonUInt8 tag, 
		 const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
		 CarbonUInt8  byte_count, CarbonUInt8 lower_addr);

  //! Build a Memory Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS to write, goes in the Length Field of the TLP
    \param first_be First BE Field
    \param last_be  Last  BE Field
  */
  void memWriteReq( CarbonUInt8 tag, 
		    CarbonUInt64 addr, const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
		    CarbonUInt32 first_be, CarbonUInt64 last_be);
  
  //! Build an IO Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a IO
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param first_be First BE Field
  */
  void ioRead32Req(CarbonUInt8 tag, 
		   CarbonUInt64 addr,
		   CarbonUInt32 first_be);

  //! Build a IO Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a IO
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void ioWrite32Req(CarbonUInt8 tag, 
		    CarbonUInt64 addr, CarbonUInt32 data, 
		    CarbonUInt32 first_be);
  
  //! Build an Cfg 0 Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr Address HDW
    \param first_be First BE Field
  */
  void cfg0ReadReq(CarbonUInt8 tag, 
		   CarbonUInt32 addr, 
		   CarbonUInt32 first_be);
  
  //! Build an Cfg 1 Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 1
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr Address HDW
    \param first_be First BE Field
  */
  void cfg1ReadReq(CarbonUInt8 tag, 
				 CarbonUInt32 addr, 
				 CarbonUInt32 first_be);

  //! Build a Cfg 0 Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs.
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void cfg0WriteReq(CarbonUInt8 tag, 
		    CarbonUInt32 addr, CarbonUInt32 data, 
		    CarbonUInt32 first_be);
  
  //! Build a Cfg 1 Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 1
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs.
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void cfg1WriteReq(CarbonUInt8 tag, 
		    CarbonUInt32 addr, CarbonUInt32 data, 
		    CarbonUInt32 first_be);
  
  //! Build an Message Base Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, MsgRout and MsgCode
    The other fields are left unchanged!
    \param tag Requester Transaction Tag for the TLP
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS in the TLP
    \param rout Msg Rout Field
    \param msg_code Msg Code Field
  */
  void msgBaseReq(CarbonUInt8 tag, 
		  const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
		  CarbonUInt8 rout, CarbonUInt8 msg_code);

  CarbonXPcieTLP * castTLP();
private:
  
  void init();
  CarbonXPcieTLP * pimpl_;

  // = operator is private because the copy is not complete.
  //    Parts of MxTransactionInfo are not copied properly.
  //    Use copy constructor to make copies of transactions.
  CarbonXPcieTransInfo & operator=(const CarbonXPcieTransInfo &other){(void)other; return *this;}
};



#endif
