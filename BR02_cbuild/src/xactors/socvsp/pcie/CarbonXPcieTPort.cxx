/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#include "CarbonXPcieTransInfo.h"
#include "CarbonXPcieTPort.h"
#include "CarbonXSOCVSP.h"
#include "CarbonXPciePciPort.h"


CarbonXPcieTPort::CarbonXPcieTPort(sc_mx_module* owner, const std::string& portName) :  
  sc_mx_transaction_slave(owner, portName + std::string("_In")),
  mOwner(owner),
  mBusNum(0), mDeviceNum(0), mFunctionNum(0), mTxTag(0),
  mEndTransCBInfo(0), mEndTransCBFn(0),
  mReceivedTransCBInfo(0), mReceivedTransCBFn(0),
  mBlocked(false), mBlockedReq(0), mBlockedCpl(0),
  mPortType(ePCIE), mPortName(portName), mPciPort(0)
{
  
  // Create Master Port
  mTMaster = new sc_port<sc_mx_transaction_if>(owner, portName + std::string("_Out"));
  mOwner->registerPort( mTMaster, portName + std::string("_Out"));

  // Register Slave Port
  mOwner->registerPort(this, portName + std::string("_In"));

  // Setup Port Properties
  MxTransactionProperties prop;
  memset(&prop,0,sizeof(prop));
  prop.mxsiVersion = MXSI_VERSION_6;
  prop.useMultiCycleInterface = true;
  prop.addressBitwidth=64;
  prop.mauBitwidth = 8;
  prop.dataBitwidth = 32;
  prop.dataBeats = 64;
  prop.isLittleEndian = true;
  prop.isOptional = false;
  prop.supportsAddressRegions = false;
  prop.numCtrlFields = 0;
  prop.numSlaveFlags = 1;
  prop.numMasterFlags = 3;
  prop.validTimingTable = NULL;
  prop.protocolID = CARBON_SOCVSP_PROTID_PCIE;
  sprintf(prop.protocolName,"Carbon PCI Express");
  prop.supportsNotify = true;
  prop.supportsBurst = true;
  prop.supportsSplit = false;
  prop.isAddrRegionForwarded = false;
  prop.forwardAddrRegionToMasterPort = NULL;
  
  // Set Properties for Master
  mTMaster->setProperties(&prop);  
  
  // Set Properties for Slave
  setProperties(&prop);  
}

CarbonXPcieTPort::~CarbonXPcieTPort()
{
  delete mTMaster;
}

void CarbonXPcieTPort::setBusNum(CarbonUInt32 num)
{
  mBusNum = num;
}

void CarbonXPcieTPort::setDeviceNum(CarbonUInt32 num)
{
  mDeviceNum = num;
}

void CarbonXPcieTPort::setFunctionNum(CarbonUInt32 num)
{
  mFunctionNum = num;
}

bool CarbonXPcieTPort::addTransaction(const CarbonXPcieTLP* tlp)
{
  // If we are connected to a PCI Port, issue a PCI transaction
  if(mPortType == ePCI) {
    return mPciPort->issueTransaction(tlp);
  }
  
  // Otherwise create a PCI Express Transaction
  else {
    // Create a new transaction, save it on the transmit queue, and drive it
    // to the transactor.
    CarbonXPcieTransInfo* trans = new CarbonXPcieTransInfo(mTMaster, tlp);
    mTransmitQueue.push_back(trans);
    
    // The parent component need to call this method in the communicate
    // phase, so we can drive the transaction right away.
    mTMaster->driveTransaction(trans);
    return true;
  }

  return false;
}
  

bool CarbonXPcieTPort::receiveTransaction(CarbonXPcieTLP* tlp)
{
  // If we are connected to a PCI Port, get transaction from PCI Port
  if(mPortType == ePCI) {
    return mPciPort->receiveTransaction(tlp);
  }
  
  else if(!mReceiveQueue.empty()) {
    // Copy transaction into the user object
    if(tlp) carbonXPcieTLPCopy(tlp, mReceiveQueue.front()->castTLP());
    else {
      pmessage("NULL pointer passed to receiveTransaction.", MX_MSG_ERROR);
      return false;
    }
    // Tell the initiating master port that it can safely delete the transaction
    mReceiveQueue.front()->cts = CarbonXPcieTransInfo::eCtsDone;

    // Remove from the Receive queue.
    mReceiveQueue.pop_front();
    
    // Report Success
    return true;
  }

  // Report No Success
  return false;
}

void CarbonXPcieTPort::setEndTransCallbackFn(void* info, TransCBFn* transCbFn)
{
  mEndTransCBInfo = info;
  mEndTransCBFn   = transCbFn;
}

void CarbonXPcieTPort::setReceivedTransCallbackFn(void* info, TransCBFn* transCbFn)
{
  mReceivedTransCBInfo = info;
  mReceivedTransCBFn   = transCbFn;
}

void CarbonXPcieTPort::cfg0Write32(CarbonUInt32 address, CarbonUInt32 data, CarbonUInt32 be)
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPCfg0WriteReq(tlp, tag, address, data, be);

  // Perform blocking operation, we can use the same object for the return data
  blockingTransaction(tlp, tlp);
  
  // There is nothing to return for this transaction, so cleanup
  carbonXPcieTLPDestroy(tlp);
}

CarbonUInt32 CarbonXPcieTPort::cfg0Read32(CarbonUInt32 address)
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPCfg0ReadReq(tlp, tag, address, 0xf);

  // Perform blocking operation, we can use the same object for the return data
  blockingTransaction(tlp, tlp);
  
  // Extract return data and cleanup
  CarbonUInt32 read_data = carbonXPcieTLPGetDWord(tlp, 0);
  carbonXPcieTLPDestroy(tlp);

  return read_data; 
}

void CarbonXPcieTPort::memWrite32(CarbonUInt64 address, CarbonUInt32 data, CarbonUInt32 be )
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPMemWriteReq(tlp, tag, address, &data, 1, be, 0);

  // Send Transaction
  sendTransaction(tlp);

  // There is nothing to return for this transaction, so cleanup
  carbonXPcieTLPDestroy(tlp);  
}

CarbonUInt32 CarbonXPcieTPort::memRead32(CarbonUInt64 address)
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPMemReadReq(tlp, tag, address, 1, 0xf, 0);

  // Perform blocking operation, we can use the same object for the return data
  blockingTransaction(tlp, tlp);
  
  // Extract return data and cleanup
  CarbonUInt32 read_data = carbonXPcieTLPGetDWord(tlp, 0);
  carbonXPcieTLPDestroy(tlp);

  return read_data; 
}

void CarbonXPcieTPort::memWrite(CarbonUInt64 address, const CarbonUInt32* data, CarbonUInt32 size,
				    UInt32 first_be, UInt32 last_be)
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPMemWriteReq(tlp, tag, address, data, size, first_be, last_be);

  // Send Transaction
  sendTransaction(tlp);

  // There is nothing to return for this transaction, so cleanup
  carbonXPcieTLPDestroy(tlp);  
}

void CarbonXPcieTPort::memRead(CarbonUInt64 address, UInt32* data, CarbonUInt32 size)
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPMemReadReq(tlp, tag, address, size, 0xf, 0xf);

  // Perform blocking operation, we can use the same object for the return data
  blockingTransaction(tlp, tlp);
  
  // Extract return data and cleanup
  for(UInt32 i = 0; i < size; i++)
    data[i] = carbonXPcieTLPGetDWord(tlp, i);

  carbonXPcieTLPDestroy(tlp);
}

void CarbonXPcieTPort::ioWrite32(CarbonUInt32 address, CarbonUInt32 data, CarbonUInt32 be)
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPIOWrite32Req(tlp, tag, address, data, be);

  // Perform blocking operation, we can use the same object for the return data
  blockingTransaction(tlp, tlp);
  
  // There is nothing to return for this transaction, so cleanup
  carbonXPcieTLPDestroy(tlp);
}

CarbonUInt32 CarbonXPcieTPort::ioRead32(CarbonUInt32 address)
{
  CarbonXPcieTLP* tlp = carbonXPcieTLPCreate();
  CarbonUInt32    tag = getNextTag();

  carbonXPcieTLPSetReqId(tlp, (mBusNum << 8) | (mDeviceNum << 3) | mFunctionNum);
  carbonXPcieTLPIORead32Req(tlp, tag, address, 0xf);

  // Perform blocking operation, we can use the same object for the return data
  blockingTransaction(tlp, tlp);
  
  // Extract return data and cleanup
  CarbonUInt32 read_data = carbonXPcieTLPGetDWord(tlp, 0);
  carbonXPcieTLPDestroy(tlp);

  return read_data; 
}

void CarbonXPcieTPort::init()
{
  mOwner->message(MX_MSG_INFO, "In: CarbonXPcieTPort::init\n");

}
void CarbonXPcieTPort::reset()
{
  // Check what kind of port we are connected to.
  const std::vector<maxsim::sc_mx_transaction_if*>& slaves = mTMaster->getSlaves();
  
  for(std::vector<maxsim::sc_mx_transaction_if*>::const_iterator iter = slaves.begin(); iter != slaves.end(); iter++) {
    const MxTransactionProperties* prop = (*iter)->getProperties();

    switch(prop->protocolID) {
    case CARBON_SOCVSP_PROTID_PCIE : mPortType = ePCIE; break;
    case CARBON_SOCVSP_PROTID_PCIS : 
      mPortType = ePCI;
      if(!mPciPort) mPciPort = new CarbonXPciePciPort(mTMaster);
      mPciPort->reset();
      break;
    default :
      mTMaster->pmessage(MX_MSG_ERROR, "%s: Port connected to incorrect port type.\n", mPortName.c_str());
      mPortType = eILLEGAL;
    }
  }
  
  
}

void CarbonXPcieTPort::communicate()
{
  // Go through the transmit queue and drive all transactions
  // that hasn't already been queued.
  std::list<CarbonXPcieTransInfo *>::iterator 
    iter = find_if(mTransmitQueue.begin(), mTransmitQueue.end(), 
		   CarbonXPcieTransInfo::cts_state(CarbonXPcieTransInfo::eCtsNotStarted));

  while(iter != mTransmitQueue.end()) {
    mTMaster->driveTransaction(*iter);
    
    iter = find_if(iter, mTransmitQueue.end(),
		   CarbonXPcieTransInfo::cts_state(CarbonXPcieTransInfo::eCtsNotStarted)); 
  }
}

void CarbonXPcieTPort::update()
{
  // Go through transmit queue and check for transactions that has finished
  // Then call the end of transaction user callback if registered
  // and delete the transaction.
  std::list<CarbonXPcieTransInfo *>::iterator 
    iter = find_if(mTransmitQueue.begin(), mTransmitQueue.end(), 
		   CarbonXPcieTransInfo::cts_state(CarbonXPcieTransInfo::eCtsDone));

  while(iter != mTransmitQueue.end()) {

    // Call End of Transaction callback
    if(mEndTransCBFn) (*mEndTransCBFn)(mEndTransCBInfo, (*iter)->castTLP());

    // Erase Transaction from queue.
    delete *iter;
    iter = mTransmitQueue.erase(iter);
    
    // Find Next Done Transaction
    iter = find_if(iter, mTransmitQueue.end(),
		   CarbonXPcieTransInfo::cts_state(CarbonXPcieTransInfo::eCtsDone)); 
  }  
}

void CarbonXPcieTPort::driveTransaction(MxTransactionInfo* info)
{
  CarbonXPcieTransInfo* trans = static_cast<CarbonXPcieTransInfo *>(info);
  
  trans->cts++; // Update state to queued.
  
  // If we're currently blocked, waiting for a completer, check if this is it
  if(mBlocked) {

    if(carbonXPcieTLPGetTransId(trans->castTLP()) ==  carbonXPcieTLPGetTransId(mBlockedReq)) {
      // Save the completer, so it can be retrieved by the blocked thread.
      mBlockedCpl = trans;
      
      // Signal the blocked thread that it is ready to run
      mBlockingEvent.notify();

      // We're done here, exit!
      return;
    }
  }

  // If the user has registered an received transaction callback, call it now
  if(mReceivedTransCBFn) {
    (*mReceivedTransCBFn)(mReceivedTransCBInfo, trans->castTLP());
    trans->cts = CarbonXPcieTransInfo::eCtsDone; // Update state to done
  }
  else {
    // Otherwise, just put the transaction on the received queue
    mReceiveQueue.push_back(trans);
  }
}

void CarbonXPcieTPort::cancelTransaction(MxTransactionInfo* info)
{
  // If transaction is still on the received queue, remove
  mReceiveQueue.remove(static_cast<CarbonXPcieTransInfo*>(info)); 
}

void CarbonXPcieTPort::sendTransaction(const CarbonXPcieTLP* req)
{
  CarbonXPcieTransInfo* trans = new CarbonXPcieTransInfo(mTMaster, req);
  
  // This call is made from an SC_THREAD and not from communicate so we need to queue the transaction
  // so it can be sent out in the next communicate phase.
  mTransmitQueue.push_back(trans);

  // The CarbonXPcieTransInfo transaction (trans) is going to be deleted by the update method 
  // when the slave has reported transaction done, so don't delete it here
}
  

void CarbonXPcieTPort::blockingTransaction(const CarbonXPcieTLP* req, CarbonXPcieTLP* cmp)
{
  // Send Request
  sendTransaction(req);

  // Save the blocking request
  mBlockedReq = req;
  
  // Mark that we're in a blocking method
  mBlocked = true;

  // Wait until we found a matching transaction
  wait(mBlockingEvent);
  
  // Copy transaction into the provided completer transaction.
  carbonXPcieTLPCopy(cmp, mBlockedCpl->castTLP());
      
  // Mark transaction done
  mBlockedCpl->cts = CarbonXPcieTransInfo::eCtsDone;
  
  // Remove block
  mBlocked = false;
}
  
  



 

