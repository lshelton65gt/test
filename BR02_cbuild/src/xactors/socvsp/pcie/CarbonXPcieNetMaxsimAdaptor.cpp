/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifdef CARBON

#include "carbonXPcie.h"
#include "CarbonXPcieNetMaxsimAdaptor.h"

CarbonXPcieNetMaxsimAdaptor::CarbonXPcieNetMaxsimAdaptor(CarbonXtorAdaptorToVhmPort * net) :
  mPcieNet(0)
{
  mNet = net;
}

const char * CarbonXPcieNetMaxsimAdaptor::getName()
{
  return "";
}

void CarbonXPcieNetMaxsimAdaptor::deposit(UInt32 * value)
{
  mNet->driveSignal(*value, NULL);
}

void CarbonXPcieNetMaxsimAdaptor::examine(UInt32 * value)
{
  (void)value;
}

void CarbonXPcieNetMaxsimAdaptor::connect(CarbonXPcieNet *net) {
  if(mPcieNet == 0) {
    mPcieNet = net;
    carbonXPcieNetSetValueChangeCB(net, netChangeCB, this);
  }
  else {
    printf("ERROR: Port Already connected to net %s\n", carbonXPcieNetGetName(mPcieNet));
  }
}

void CarbonXPcieNetMaxsimAdaptor::netChangeCB(CarbonXPcieNetID *net, CarbonClientData userData) {
  CarbonXPcieNetMaxsimAdaptor *me = reinterpret_cast<CarbonXPcieNetMaxsimAdaptor *>(userData);
  UInt32 value;
  carbonXPcieNetExamine(net, &value);
  me->mNet->driveSignal(value, NULL);
}

#endif

