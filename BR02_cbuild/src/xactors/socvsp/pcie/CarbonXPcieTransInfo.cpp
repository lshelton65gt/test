/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#include "CarbonXPcieTransInfo.h"

CarbonXPcieTransInfo::CarbonXPcieTransInfo(sc_mx_transaction_p_base *port)
  : MxTransactionInfo(port)
{
  pimpl_ = carbonXPcieTLPCreate();
  init();
}

CarbonXPcieTransInfo::CarbonXPcieTransInfo(const CarbonXPcieTransInfo& in)
  : MxTransactionInfo(in.initiator)
{  
  pimpl_ = carbonXPcieTLPCreate();
  carbonXPcieTLPCopy(pimpl_, in.pimpl_);
  init();
}

CarbonXPcieTransInfo::CarbonXPcieTransInfo(sc_mx_transaction_p_base *port, const CarbonXPcieTLP *in)
    : MxTransactionInfo(port)
{
  pimpl_ = carbonXPcieTLPCreate();
  carbonXPcieTLPCopy(pimpl_, in);
  init();
}

CarbonXPcieTransInfo::~CarbonXPcieTransInfo() {
  carbonXPcieTLPDestroy(pimpl_);
  
  if (status != NULL) {
    delete [] status;
  }
}

const char* CarbonXPcieTransInfo::getStrFromType (CarbonXPcieCmdType type) {
   switch (type) {
   case eCarbonXPcieMRd32:   return "eCarbonXPcieMRd32";
   case eCarbonXPcieMRd64:   return "eCarbonXPcieMRd64";
   case eCarbonXPcieMRdLk32: return "eCarbonXPcieMRdLk32";
   case eCarbonXPcieMRdLk64: return "eCarbonXPcieMRdLk64";
   case eCarbonXPcieMWr32:   return "eCarbonXPcieMWr32";
   case eCarbonXPcieMWr64:   return "eCarbonXPcieMWr64";
   case eCarbonXPcieIORd:    return "eCarbonXPcieIORd";
   case eCarbonXPcieIOWr:    return "eCarbonXPcieIOWr";
   case eCarbonXPcieCfgRd0:  return "eCarbonXPcieCfgRd0";
   case eCarbonXPcieCfgWr0:  return "eCarbonXPcieCfgWr0";
   case eCarbonXPcieCfgRd1:  return "eCarbonXPcieCfgRd1";
   case eCarbonXPcieCfgWr1:  return "eCarbonXPcieCfgWr1";
   case eCarbonXPcieMsg:     return "Msg";
   case eCarbonXPcieMsgD:    return "MsgD";
   case eCarbonXPcieCpl:     return "eCarbonXPcieCpl";
   case eCarbonXPcieCplD:    return "eCarbonXPcieCplD";
   case eCarbonXPcieCplLk:   return "eCarbonXPcieCplLk";
   case eCarbonXPcieCplDLk:  return "eCarbonXPcieCplDLk";
   default:               return "Invalid Type";
   }
}

void CarbonXPcieTransInfo::copy(const CarbonXPcieTransInfo& in) {
  carbonXPcieTLPCopy(pimpl_, in.pimpl_);
}

void CarbonXPcieTransInfo::setCmd(UInt32 cmd) {
  carbonXPcieTLPSetCmd(pimpl_, cmd);
}

UInt32 CarbonXPcieTransInfo::getCmd() const{
  return carbonXPcieTLPGetCmd(pimpl_);
}

void CarbonXPcieTransInfo::setCmdByName(CarbonXPcieCmdType cmd) {
  carbonXPcieTLPSetCmdByName(pimpl_, cmd);
}

CarbonXPcieCmdType CarbonXPcieTransInfo::getCmdByName() const{
  return carbonXPcieTLPGetCmdByName(pimpl_);
}

void CarbonXPcieTransInfo::setAddr(UInt64 addr) {
  carbonXPcieTLPSetAddr(pimpl_, addr);
}

UInt64 CarbonXPcieTransInfo::getAddr() const{
  return carbonXPcieTLPGetAddr(pimpl_);
}

void CarbonXPcieTransInfo::setReqId(UInt32 id) {
  carbonXPcieTLPSetReqId(pimpl_, id);
}

UInt32 CarbonXPcieTransInfo::getReqId() const{
  return carbonXPcieTLPGetReqId(pimpl_);
}

void CarbonXPcieTransInfo::setTag(UInt32 tag) {
  carbonXPcieTLPSetTag(pimpl_, tag);
}

UInt32 CarbonXPcieTransInfo::getTag() const {
  return carbonXPcieTLPGetTag(pimpl_);
}

void CarbonXPcieTransInfo::setRegNum(UInt32 reg) {
  carbonXPcieTLPSetRegNum(pimpl_, reg);
}

UInt32 CarbonXPcieTransInfo::getRegNum() const{
  return carbonXPcieTLPGetRegNum(pimpl_);
}

void CarbonXPcieTransInfo::setCompId(UInt32 id) {
  carbonXPcieTLPSetCompId(pimpl_, id);
}

UInt32 CarbonXPcieTransInfo::getCompId() const{
  return carbonXPcieTLPGetCompId(pimpl_);
}

void CarbonXPcieTransInfo::setMsgRout(UInt32 rout) {
  carbonXPcieTLPSetMsgRout(pimpl_, rout);
}

UInt32 CarbonXPcieTransInfo::getMsgRout() const{
  return carbonXPcieTLPGetMsgRout(pimpl_);
}

void CarbonXPcieTransInfo::setMsgCode(UInt32 code) {
  carbonXPcieTLPSetMsgCode(pimpl_, code);
}

UInt32 CarbonXPcieTransInfo::getMsgCode() const{
  return carbonXPcieTLPGetMsgCode(pimpl_);
}

void CarbonXPcieTransInfo::setLowAddr(UInt32 addr) {
  carbonXPcieTLPSetLowAddr(pimpl_, addr);
}

UInt32 CarbonXPcieTransInfo::getLowAddr() const{
  return carbonXPcieTLPGetLowAddr(pimpl_);
}

void CarbonXPcieTransInfo::setByteCount(UInt32 count) {
  carbonXPcieTLPSetByteCount(pimpl_, count);
}

UInt32 CarbonXPcieTransInfo::getByteCount() const{
  return carbonXPcieTLPGetByteCount(pimpl_);
}

void CarbonXPcieTransInfo::setFirstBE(UInt32 be) {
  carbonXPcieTLPSetFirstBE(pimpl_, be);
}

UInt32 CarbonXPcieTransInfo::getFirstBE() const{
  return carbonXPcieTLPGetFirstBE(pimpl_);
}

void CarbonXPcieTransInfo::setLastBE(UInt32 be) {
  carbonXPcieTLPSetLastBE(pimpl_, be);
}

UInt32 CarbonXPcieTransInfo::getLastBE() const{
  return carbonXPcieTLPGetLastBE(pimpl_);
}

void CarbonXPcieTransInfo::setCplStatus(UInt32 status) {
  carbonXPcieTLPSetCplStatus(pimpl_, status);
}

UInt32 CarbonXPcieTransInfo::getCplStatus() const{
  return carbonXPcieTLPGetCplStatus(pimpl_);
}

void CarbonXPcieTransInfo::setDataByte(UInt32 index, UInt8 data) {
  carbonXPcieTLPSetDataByte(pimpl_, index, data);
}

UInt8 CarbonXPcieTransInfo::getDataByte(UInt32 index) const{
  return carbonXPcieTLPGetDataByte(pimpl_, index);
}

void CarbonXPcieTransInfo::setDWord(UInt32 index, UInt32 data) {
  carbonXPcieTLPSetDWord(pimpl_, index, data);
}

UInt32 CarbonXPcieTransInfo::getDWord(UInt32 index) const{
  return carbonXPcieTLPGetDWord(pimpl_, index);
}

void CarbonXPcieTransInfo::resizeDataBytes(UInt32 start, UInt32 end) {
  carbonXPcieTLPResizeDataBytes(pimpl_, start, end);
}

UInt32 CarbonXPcieTransInfo::getDataByteCount(void) const{
  return carbonXPcieTLPGetDataByteCount(pimpl_);
}

void CarbonXPcieTransInfo::setDataLength(UInt32 len) {
  carbonXPcieTLPSetDataLength(pimpl_, len);
}

UInt32 CarbonXPcieTransInfo::getDataLength(void) const{
  return carbonXPcieTLPGetDataLength(pimpl_);
}

SInt32 CarbonXPcieTransInfo::getTransId() const{
  return carbonXPcieTLPGetTransId(pimpl_);
}

void CarbonXPcieTransInfo::setStartTime(UInt64 time) {
  carbonXPcieTLPSetStartTick(pimpl_, time);
}

UInt64 CarbonXPcieTransInfo::getStartTime() const {
  return carbonXPcieTLPGetStartTick(pimpl_);
}

void CarbonXPcieTransInfo::setEndTime(UInt64 time) {
  carbonXPcieTLPSetEndTick(pimpl_, time);
}

UInt64 CarbonXPcieTransInfo::getEndTime() const {
  return carbonXPcieTLPGetEndTick(pimpl_);
}

bool CarbonXPcieTransInfo::isRead() const{
  return carbonXPcieTLPIsRead(pimpl_);
}

bool CarbonXPcieTransInfo::isWrite() const{
  return carbonXPcieTLPIsWrite(pimpl_);
}

void CarbonXPcieTransInfo::memReadReq(CarbonUInt8 tag, 
				      CarbonUInt64 addr, CarbonUInt32 num_dwords, 
				      CarbonUInt32 first_be, CarbonUInt64 last_be){
  carbonXPcieTLPMemReadReq(pimpl_, tag, addr, num_dwords, first_be, last_be);
}

void CarbonXPcieTransInfo::completer(CarbonUInt32 req_id, CarbonUInt8 tag, 
				     const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
				     CarbonUInt8  byte_count, CarbonUInt8 lower_addr){
  carbonXPcieTLPCompleter(pimpl_, req_id, tag, data, num_dwords, byte_count, lower_addr);
}

void CarbonXPcieTransInfo::memWriteReq(CarbonUInt8 tag, 
				       CarbonUInt64 addr, const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
				       CarbonUInt32 first_be, CarbonUInt64 last_be){
  carbonXPcieTLPMemWriteReq(pimpl_, tag, addr, data, num_dwords, first_be, last_be);
}

void CarbonXPcieTransInfo::ioRead32Req(CarbonUInt8 tag, 
				       CarbonUInt64 addr,
				       CarbonUInt32 first_be){
  carbonXPcieTLPIORead32Req(pimpl_, tag, addr, first_be);
}

void CarbonXPcieTransInfo::ioWrite32Req(CarbonUInt8 tag, 
					CarbonUInt64 addr, CarbonUInt32 data, 
					CarbonUInt32 first_be){
  carbonXPcieTLPIOWrite32Req(pimpl_, tag, addr, data, first_be);
}

void CarbonXPcieTransInfo::cfg0ReadReq(CarbonUInt8 tag, 
				       CarbonUInt32 addr, 
				       CarbonUInt32 first_be){
  carbonXPcieTLPCfg0ReadReq(pimpl_, tag, addr, first_be);
}

void CarbonXPcieTransInfo::cfg1ReadReq(CarbonUInt8 tag, 
				       CarbonUInt32 addr, 
				       CarbonUInt32 first_be){
  carbonXPcieTLPCfg1ReadReq(pimpl_, tag, addr, first_be);
}

void CarbonXPcieTransInfo::cfg0WriteReq(CarbonUInt8 tag, 
					CarbonUInt32 addr, CarbonUInt32 data, 
					CarbonUInt32 first_be){
  carbonXPcieTLPCfg0WriteReq(pimpl_, tag, addr, data, first_be);
}

void CarbonXPcieTransInfo::cfg1WriteReq(CarbonUInt8 tag, 
					CarbonUInt32 addr, CarbonUInt32 data, 
					CarbonUInt32 first_be){
  carbonXPcieTLPCfg1WriteReq(pimpl_, tag, addr, data, first_be);
}

void CarbonXPcieTransInfo::msgBaseReq(CarbonUInt8 tag, 
				      const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
				      CarbonUInt8 rout, CarbonUInt8 msg_code){
  carbonXPcieTLPMsgBaseReq(pimpl_, tag, data, num_dwords, rout, msg_code);
}

CarbonXPcieTLP * CarbonXPcieTransInfo::castTLP(){
  return pimpl_;
}

void CarbonXPcieTransInfo::init() {
  // fill MxTransactionInfo
  nts       = cNts;
  cts       = eCtsNotStarted;
  notify    = MX_NOTIFY_NO;

  // Always set to read for now so that any response is copied to the transaction
  access    = MX_ACCESS_READ;
  addr      = 0;
  dataSize  = sizeof(CarbonXPcieTLP *);
  dataWr    = reinterpret_cast<MxU32 *>(pimpl_);
  dataRd    = NULL;
  
  masterFlags = NULL;
  slaveFlags  = NULL;
  dataBeats   = 0;  // Not used with Carbon transactors

  // Init Maxsim status to default -- not used in Carbon transactors
  status = new MxTransactionStatus [nts];
  for (UInt32 i=0; i<nts; i++) status[i] = MX_MASTER_WAIT;
}
