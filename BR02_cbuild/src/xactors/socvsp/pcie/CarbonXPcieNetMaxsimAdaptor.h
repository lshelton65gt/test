/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONXPCIENETMAXSIMADAPTOR_H_
#define _CARBONXPCIENETMAXSIMADAPTOR_H_

#ifdef CARBON

#include "maxsim.h"
#include "xactors/arm/include/carbon_arm_adaptor.h"
#include "carbonXPcie.h"

class CarbonXPcieNetMaxsimAdaptor
{

 public:

  CarbonXPcieNetMaxsimAdaptor(CarbonXtorAdaptorToVhmPort * net);

  virtual ~CarbonXPcieNetMaxsimAdaptor(void) {};

  const char * getName(void);
  void deposit(UInt32 * value);
  void examine(UInt32 * value);
  void connect(CarbonXPcieNet *net);

 private:

  static void netChangeCB(CarbonXPcieNetID *net, CarbonClientData userData);
  CarbonXtorAdaptorToVhmPort * mNet;
  CarbonXPcieNetID           * mPcieNet; 

};

#endif

#endif

