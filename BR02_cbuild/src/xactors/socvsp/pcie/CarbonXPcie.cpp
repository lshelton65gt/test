/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "CarbonXPcie.h"
#include "CarbonXPcieNet.h"
#include "CarbonXPcieTPort.h"
#include "CarbonXPcieNetMaxsimAdaptor.h"

#include <stdarg.h>
#define CarbonXPcie_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)

#define PORT_OWNER mCarbonComponent
CarbonXPcie::CarbonXPcie(sc_mx_module* carbonComp, 
                         const char *xtorName, 
			 CarbonXPcieIntfType intf,
                         UInt32 num_lanes,
                         CarbonObjectID **carbonObj, 
                         CarbonPortFactory *portFactory)
  : mXtorName(xtorName), mCarbonComponent(carbonComp), mpCarbonObject(carbonObj), mNumLanes(num_lanes), mIntf(intf)
{
  mPcieInst    = NULL;
  initComplete = false;

  char rxDataNames[][16] = {"RxData0",  "RxData1",  "RxData2",  "RxData3", 
                           "RxData4",  "RxData5",  "RxData6",  "RxData7",
                           "RxData8",  "RxData9",  "RxData10", "RxData11", 
                           "RxData12", "RxData13", "RxData14", "RxData15",
                           "RxData16", "RxData17", "RxData18", "RxData19", 
                           "RxData20", "RxData21", "RxData22", "RxData23",
                           "RxData24", "RxData25", "RxData26", "RxData27", 
                           "RxData28", "RxData29", "RxData30", "RxData31"};
  char rxDataKNames[][16] = {"RxDataK0",  "RxDataK1",  "RxDataK2",  "RxDataK3", 
                           "RxDataK4",  "RxDataK5",  "RxDataK6",  "RxDataK7",
                           "RxDataK8",  "RxDataK9",  "RxDataK10", "RxDataK11", 
                           "RxDataK12", "RxDataK13", "RxDataK14", "RxDataK15",
                           "RxDataK16", "RxDataK17", "RxDataK18", "RxDataK19", 
                           "RxDataK20", "RxDataK21", "RxDataK22", "RxDataK23",
                           "RxDataK24", "RxDataK25", "RxDataK26", "RxDataK27", 
                           "RxDataK28", "RxDataK29", "RxDataK30", "RxDataK31"};
  char rxIdleNames[][16] = {"RxElecIdle0",  "RxElecIdle1",  "RxElecIdle2",  "RxElecIdle3", 
                           "RxElecIdle4",  "RxElecIdle5",  "RxElecIdle6",  "RxElecIdle7",
                           "RxElecIdle8",  "RxElecIdle9",  "RxElecIdle10", "RxElecIdle11", 
                           "RxElecIdle12", "RxElecIdle13", "RxElecIdle14", "RxElecIdle15",
                           "RxElecIdle16", "RxElecIdle17", "RxElecIdle18", "RxElecIdle19", 
                           "RxElecIdle20", "RxElecIdle21", "RxElecIdle22", "RxElecIdle23",
                           "RxElecIdle24", "RxElecIdle25", "RxElecIdle26", "RxElecIdle27", 
                           "RxElecIdle28", "RxElecIdle29", "RxElecIdle30", "RxElecIdle31"};
  char rxStatusNames[][16] = {"RxStatus0",  "RxStatus1",  "RxStatus2",  "RxStatus3", 
                           "RxStatus4",  "RxStatus5",  "RxStatus6",  "RxStatus7",
                           "RxStatus8",  "RxStatus9",  "RxStatus10", "RxStatus11", 
                           "RxStatus12", "RxStatus13", "RxStatus14", "RxStatus15",
                           "RxStatus16", "RxStatus17", "RxStatus18", "RxStatus19", 
                           "RxStatus20", "RxStatus21", "RxStatus22", "RxStatus23",
                           "RxStatus24", "RxStatus25", "RxStatus26", "RxStatus27", 
                           "RxStatus28", "RxStatus29", "RxStatus30", "RxStatus31"};
  char rxValidNames[][16] = {"RxValid0",  "RxValid1",  "RxValid2",  "RxValid3", 
                           "RxValid4",  "RxValid5",  "RxValid6",  "RxValid7",
                           "RxValid8",  "RxValid9",  "RxValid10", "RxValid11", 
                           "RxValid12", "RxValid13", "RxValid14", "RxValid15",
                           "RxValid16", "RxValid17", "RxValid18", "RxValid19", 
                           "RxValid20", "RxValid21", "RxValid22", "RxValid23",
                           "RxValid24", "RxValid25", "RxValid26", "RxValid27", 
                           "RxValid28", "RxValid29", "RxValid30", "RxValid31"};

  // Electrical Idle wires -- one in each direction for each lane
  //prop.bitwidth = 1;
  //prop.isOptional = true;

  if(mIntf == eCarbonXPciePipeIntf) {
    mPhyStatus = portFactory(mCarbonComponent, xtorName, "PhyStatus", mpCarbonObject);
    mPhyStatusNet = new CarbonXPcieNetMaxsimAdaptor(mPhyStatus);
  }

  for (UInt32 i=0; i<mNumLanes; i++) {
    mRxData[i]        = portFactory(mCarbonComponent, xtorName, rxDataNames[i], mpCarbonObject);
    mRxDataNet[i]     = new CarbonXPcieNetMaxsimAdaptor(mRxData[i]);
    mRxElecIdle[i]    = portFactory(mCarbonComponent, xtorName, rxIdleNames[i], mpCarbonObject);
    mRxElecIdleNet[i] = new CarbonXPcieNetMaxsimAdaptor(mRxElecIdle[i]);

    if(mIntf == eCarbonXPciePipeIntf) {
      mRxDataK[i] = portFactory(mCarbonComponent, xtorName, rxDataKNames[i], mpCarbonObject);
      mRxDataKNet[i] = new CarbonXPcieNetMaxsimAdaptor(mRxDataK[i]);
      mRxStatus[i] = portFactory(mCarbonComponent, xtorName, rxStatusNames[i], mpCarbonObject);
      mRxStatusNet[i] = new CarbonXPcieNetMaxsimAdaptor(mRxStatus[i]);
      mRxValid[i] = portFactory(mCarbonComponent, xtorName, rxValidNames[i], mpCarbonObject);
      mRxValidNet[i] = new CarbonXPcieNetMaxsimAdaptor(mRxValid[i]);
    }
  }

  // Transaction Port
  mPciePort = new CarbonXPcieTPort(PORT_OWNER, string(xtorName));
}

CarbonXPcie::~CarbonXPcie()
{
  // Delete Signal Ports
  for (UInt32 i=0; i<mNumLanes; i++) {
    delete mRxElecIdle[i];
    delete mRxData[i];
    delete mRxElecIdleNet[i];
    delete mRxDataNet[i];
  }
  
  // Delete Transaction Port
  delete mPciePort;
}

void 
CarbonXPcie::pcieTransCB(CarbonXPcieID *pcie, CarbonXPcieCallbackType type, void *info)
{
  (void)pcie; (void)type; (void)info;
}

void 
CarbonXPcie::pcieEndTransCB(CarbonXPcieID *xtor, void *caller, CarbonXPcieTLP *cmd) {
  (void)xtor;
  reinterpret_cast<CarbonXPcie *>(caller)->reportTransactionDone(cmd);
}

void CarbonXPcie::reportTransactionDone(CarbonXPcieTLP *cmd) {
  (void)cmd;
  //cout << "reportTransactionDone: Cmd = " << carbonXPcieTLPGetStrFromType(carbonXPcieTLPGetCmdByName(cmd)) << endl;

  // Look up Transaction in list
//   CarbonXPcieTransInfo::cmd_transid_eq  pred(carbonXPcieTLPGetCmdByName(cmd), carbonXPcieTLPGetTransId(cmd));
//   list<CarbonXPcieTransInfo*>::iterator req_iter = find_if(mRunningTrans.begin(), mRunningTrans.end(), pred);
  
//   if(req_iter != mRunningTrans.end()) {
    
//     // Set Start and end Time
//     (*req_iter)->setStartTime(carbonXPcieTLPGetStartTick(cmd));
//     (*req_iter)->setEndTime(carbonXPcieTLPGetEndTick(cmd));

//     // Mark Transaction Done
//     (*req_iter)->cts = CarbonXPcieTransInfo::eCtsDone;
    
//     // Remove From Transaction List
//     mRunningTrans.erase(req_iter);
//   }
}

void CarbonXPcie::connect(CarbonXPcieSignalType signal, CarbonUInt32 lane, CarbonNetID* netId) {
  CarbonXPcieNet* pcieNet = carbonXPcieGetNet(mPcieInst, signal, lane);
  if(!carbonXPcieNetBindCarbonNet(pcieNet, *mpCarbonObject, netId))
    mCarbonComponent->message(MX_MSG_ERROR, "Error when binding net.");
}

void CarbonXPcie::setRateChangeCallbackFn(void *caller, CarbonXPcieRateChangeCBFn *rateChangeCbFn)
{
  carbonXPcieSetRateChangeCallbackFn(mPcieInst, caller, rateChangeCbFn);
}

void CarbonXPcie::registerPort(sc_mx_p_base* port, const string& name)
{
  if ((name == "RxXtor") || (name == "TxXtor")) {
    // register the transaction ports with the owning Carbon component
    mCarbonComponent->registerPort(port, name.c_str());
  }
}

void CarbonXPcie::registerPort(CarbonXtorAdaptorToVhmPort* port, const string& name)
{
  (void)port; (void)name;
}

void 
CarbonXPcie::init()
{
  
  // Check for error in numLanes
  // Instantiate the PCI Express Transactor
  mPcieInst = carbonXPcieCreate(mIntf, mNumLanes, mXtorName.c_str());

  if(mIntf == eCarbonXPciePipeIntf) {
    mPhyStatusNet->connect(carbonXPcieGetNet(mPcieInst, eCarbonXPciePhyStatus, 0));
  }

  // Connect the signals to the Transactor
  for (UInt32 i=0; i<mNumLanes; i++) {
    mRxDataNet[i]->connect(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxData, i));
    mRxElecIdleNet[i]->connect(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxElecIdle, i));
    
    if(mIntf == eCarbonXPciePipeIntf) {
      mRxDataKNet[i]->connect(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxDataK, i));
      mRxStatusNet[i]->connect(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxStatus, i));
      mRxValidNet[i]->connect(carbonXPcieGetNet(mPcieInst, eCarbonXPcieRxValid, i)); 
    }
  }
  
  void * transCbInfo = NULL;
  carbonXPcieSetTransCallbackFn(mPcieInst, pcieTransCB, transCbInfo);
  carbonXPcieSetEndTransCallbackFn(mPcieInst, this, pcieEndTransCB);
  
  carbonXPcieSetForceElecIdleInactive(mPcieInst, p_ForceIdleInactive);

  carbonXPcieSetPrintLevel(mPcieInst, p_DebugPrintLevel);
  initComplete = true;
}

void 
CarbonXPcie::reset(MxResetLevel, const MxFileMapIF*)
{
  mTick = 0;
  carbonXPcieReset(mPcieInst);
  mPciePort->reset();

  carbonXPcieSetMaxPayloadSize(mPcieInst, p_MaxPayloadSize);
  carbonXPcieSetRCB(mPcieInst, p_ReadCompletionBoundary);
  carbonXPcieSetMaxTransInFlight(  mPcieInst, p_MaxInFlight);
  carbonXPcieSetScrambleStatus(  mPcieInst, p_ScrambleStatus);
  //carbonXPcieSetPrintLevel(  mPcieInst, p_DebugPrintLevel);

  if (p_DoTraining)
    carbonXPcieStartTraining(mPcieInst, 0);
}

void 
CarbonXPcie::communicate()
{
  carbonXPcieStep(mPcieInst, mTick++);

  CarbonXPcieTLP* cmd = carbonXPcieTLPCreate();
  while (carbonXPcieGetReceivedTrans(mPcieInst, cmd)) {
    mPciePort->addTransaction(cmd);
  }
  while (carbonXPcieGetReturnData(mPcieInst, cmd)) {
    mPciePort->addTransaction(cmd);
  }
  carbonXPcieTLPDestroy(cmd);
}

void 
CarbonXPcie::update()
{
  // Get transactions from the TS port and pass them to the transactor
  //mPcieInst->printLinkState();
  if (carbonXPcieIsLinkActive(mPcieInst)) {
    CarbonXPcieTLP* cmd = carbonXPcieTLPCreate();
    while (mPciePort->receiveTransaction(cmd)) {
      carbonXPcieAddTransaction(mPcieInst, cmd);
    }
  } else {
    //printf("Waiting for links to initialize\n");
  }
}

void 
CarbonXPcie::terminate()
{
//   std::deque<CarbonXPcieTransInfo*>::iterator trans_itr = mTxTransQueue.begin();
//   while (trans_itr != mTxTransQueue.end()) {
//     delete *trans_itr;
//     trans_itr = mTxTransQueue.erase(trans_itr);
//   }
//   mRunningTrans.clear();
  carbonXPcieDestroy(mPcieInst);
}

void 
CarbonXPcie::setParameter(const string &name, const string &value)
{
  MxConvertErrorCodes status = MxConvert_SUCCESS;
  
  if (name == "Enable Debug Messages")
    {
      status = MxConvertStringToValue( value, &p_enableDbgMsg  );
    }
  else if (name == "MaxInFlight")
    {
      status = MxConvertStringToValue( value, &p_MaxInFlight );
      if ( initComplete == true )
        {
          carbonXPcieSetMaxTransInFlight(mPcieInst, p_MaxInFlight);
        }
    }
  else if (name == "MaxPayloadSize")
    {
      status = MxConvertStringToValue( value, &p_MaxPayloadSize );
      if ( initComplete == true )
        {
          carbonXPcieSetMaxPayloadSize(mPcieInst, p_MaxPayloadSize);
        }
    }
  else if (name == "Read Completion Boundary")
    {
      status = MxConvertStringToValue( value, &p_ReadCompletionBoundary );
      if ( initComplete == true )
        {
          carbonXPcieSetRCB(mPcieInst, p_ReadCompletionBoundary);
        }
    }
  else if (name == "Scramble Status")
    {
      status = MxConvertStringToValue( value, &p_ScrambleStatus );
      if ( initComplete == true )
        {
          carbonXPcieSetScrambleStatus(mPcieInst, p_ScrambleStatus);
        }
    }
  else if (name == "Do Training")
    {
      status = MxConvertStringToValue( value, &p_DoTraining );
    }
  else if (name == "Training Bypass")
    {
      status = MxConvertStringToValue( value, &p_TrainingBypass );
      if ( initComplete == true ) {
        carbonXPcieSetTrainingBypass(mPcieInst, p_TrainingBypass);
      }
    }
  else if (name == "Idle Is Hi True")
    {
      status = MxConvertStringToValue( value, &p_IdleIsHiTrue );
      if ( initComplete == true ) {
        if (p_IdleIsHiTrue)
          carbonXPcieSetElecIdleActiveHigh(mPcieInst);
        else
          carbonXPcieSetElecIdleActiveLow(mPcieInst);
      }
    }
  else if (name == "Transactor Debug Print Level")
    {
      status = MxConvertStringToValue( value, &p_DebugPrintLevel );
      if ( initComplete == true )
        {
          carbonXPcieSetPrintLevel(mPcieInst, p_DebugPrintLevel);
        }
    }
  else if (name == "Bus Number")
    {
      status = MxConvertStringToValue( value, &p_BusNum );
      if ( initComplete == true )
        {
          carbonXPcieSetBusNum(mPcieInst, p_BusNum);
        }
    }
  else if (name == "Device Number")
    {
      status = MxConvertStringToValue( value, &p_DevNum );
      if ( initComplete == true )
        {
          carbonXPcieSetDevNum(mPcieInst, p_DevNum);
        }
    }
  else if (name == "Function Number")
    {
      status = MxConvertStringToValue( value, &p_FuncNum );
      if ( initComplete == true )
        {
          carbonXPcieSetFuncNum(mPcieInst, p_FuncNum);
        }
    }
  else if (name == "Force Idle Inactive")
    {
      status = MxConvertStringToValue( value, &p_ForceIdleInactive );
      if(mPcieInst) carbonXPcieSetForceElecIdleInactive(mPcieInst, p_ForceIdleInactive);
    }
  else if (name == "NumLanes")
    {
      if ( initComplete == false )
        {
          status = MxConvertStringToValue( value, &p_NumLanes );
        }
      else
        {
          mCarbonComponent->message( MX_MSG_WARNING, "CarbonXPcie::setParameter: Cannot change parameter <%s> at runtime. Assignment ignored.", name.c_str() );
          return;
        }
    }
  
  if ( status == MxConvert_SUCCESS )
    {
    }
  else
    {
      mCarbonComponent->message( MX_MSG_WARNING, "CarbonXPcie::setParameter: Illegal value <%s> "
				 "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}


