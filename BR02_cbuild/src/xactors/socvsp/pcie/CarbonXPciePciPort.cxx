/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#include "carbonX.h"
#include "CarbonXPciePciPort.h"
#include "carbonXPcie.h"
#include "carbonXPciMaster.h"
#include "carbonXPciSlave.h"

/*
 * CarbonXPciePciPort::PciTrans Methods
 *
 *
 */

CarbonXPciePciPort::PciTrans::PciTrans(sc_mx_transaction_p_base *port) :
  CarbonXTransInfo(port),
  mRequester(0),
  mValid(false)
{
}

CarbonXPciePciPort::PciTrans::~PciTrans() {
  if(mRequester) carbonXPcieTLPDestroy(mRequester);
}

void CarbonXPciePciPort::PciTrans::reset() {
  MxTransactionInfo::reset();
}

bool CarbonXPciePciPort::PciTrans::setRequester(const CarbonXPcieTLP* requester)
{
  // If nothing goes wrong this transaction should be valid
  mValid = true;

  // Copy The Requester
  if(!mRequester) mRequester = carbonXPcieTLPCreate();
  carbonXPcieTLPCopy(mRequester, requester);

  UInt32 opcode     = 0;

  // Translate Opcode
  switch(carbonXPcieTLPGetCmdByName(mRequester)) {
  case eCarbonXPcieMRd32 :
  case eCarbonXPcieMRd64 :
  case eCarbonXPcieMRdLk32 :
  case eCarbonXPcieMRdLk64 :
    opcode  = PCI_MEM_READ;
    break;
  case eCarbonXPcieMWr32 :
  case eCarbonXPcieMWr64 :
    opcode  = PCI_MEM_WRITE_CMP;
    break;
  case eCarbonXPcieCfgRd0 :
  case eCarbonXPcieCfgRd1 :
    opcode  = PCI_CFG_READ;
    break;
  case eCarbonXPcieCfgWr0 :
  case eCarbonXPcieCfgWr1 :
    opcode  = PCI_CFG_WRITE_CMP;
    break;
  case eCarbonXPcieIORd :
    opcode  = PCI_IO_READ;
    break;
  case eCarbonXPcieIOWr :
    opcode  = PCI_IO_WRITE_CMP;
    break;
  default :
    // We don't support Completer or Message TLP, so report failure
    mValid = false;
  }
  
  // If There was a recognizable opcode, perform the rest of the translation
  if(mValid) {
    carbonXTransactionStruct req;
    carbonXInitTransactionStruct(&req, 0);

    req.mOpcode  = opcode;
    req.mAddress = carbonXPcieTLPGetAddr(mRequester);
    req.mSize    = (carbonXPcieTLPGetDataLength(mRequester)+(0 || (req.mAddress&0x4)))*4;
    req.mWidth   = TBP_WIDTH_64;

    // Calculate Byte Enables
    // The PCI transactions structure wants byte enables for the first 24 bytes and last 8 bytes
    req.mStatus = calcBE(req.mAddress, 
			 carbonXPcieTLPGetDataLength(mRequester),
			 carbonXPcieTLPGetFirstBE(mRequester),
			 carbonXPcieTLPGetLastBE(mRequester));

    // Copy Data if any
    if(carbonXPcieTLPGetCmd(mRequester) & 0x40) {
      req.mHasData = true;
      carbonXReallocateTransactionBuffer(&req);

      UInt32  wordCnt = 0;
      UInt64* data64  = reinterpret_cast<UInt64*>(req.mData); 
      
      // First Quad Word
      if(req.mAddress & 0x4) {
	data64[0] = ((UInt64)carbonXPcieTLPGetDWord(mRequester, 0)) << 32;
	wordCnt = 1;
      }
      else {
	data64[0] = ((UInt64)carbonXPcieTLPGetDWord(mRequester, 1)) << 32 | carbonXPcieTLPGetDWord(mRequester, 0);
	wordCnt = 2;
      }
	
      // The rest of the words
      for(UInt32 i = 1; i < (req.mSize+7)/8; i++, wordCnt +=2) {
	data64[i] = ((UInt64)carbonXPcieTLPGetDWord(mRequester, wordCnt+1)) << 32 | carbonXPcieTLPGetDWord(mRequester, wordCnt);
      }
    }
    
    req.mAddress &= ~(7LL);

    // Set PCI Transaction
    setRequest(req);      
  }

  return mValid;
}

UInt32 CarbonXPciePciPort::PciTrans::calcBE(UInt32 addr, UInt32 size, UInt32 fbe, UInt32 lbe)
{
  // The PCI transaction structure is using 64 bit words,
  // so if the address is not an even Quad Word, the first
  // data word goes into the upper part of the quad word.
  UInt32 start_idx = (addr & 0x4) ? 1 : 0;
  
  // First and Last Byte Enables need to be reversed
  UInt32 fbe_r = (
		  ( (fbe & 1) << 3 ) |
		  ( (fbe & 2) << 1 ) |
		  ( (fbe & 4) >> 1 ) |
		  ( (fbe & 8) >> 3 ) );
  UInt32 lbe_r = (
		  ( (lbe & 1) << 3 ) |
		  ( (lbe & 2) << 1 ) |
		  ( (lbe & 4) >> 1 ) |
		  ( (lbe & 8) >> 3 ) );

  // Byte Enable for the first Data Word
  UInt32 be     = fbe_r << (start_idx*4);
  UInt32 be_idx = start_idx+1;

  // There can be no intermediate words that are not enabled,
  // between the first enabled byte and last enabled byte
  for(UInt32 i = start_idx+1; i < (size+start_idx); i++) {
    if(i == (size+start_idx-1)) be |= lbe_r << ((be_idx)*4);
    else if(i < 7) be |= (0xf << (be_idx++*4)); // intermediate words are all enabled
  }

  return be;
}

bool CarbonXPciePciPort::PciTrans::createCompleter(CarbonXPcieTLP* comp)
{
  // If there is no requester, there can be no completer
  if(mRequester == NULL)
    return false;

  // If Transaction is not done yet, report that no completer was created
  if(!isDone())
    return false;
  
  // If Transaction was a posted write, no completer should be created
  if( isPosted() )
    return false;
  
  // Create Completer
  carbonXTransactionStruct resp;
  carbonXInitTransactionStruct(&resp, 0);
  getResponse(&resp);
  
  // Fill out some Default values
  carbonXPcieTLPSetCmd(comp,        eCarbonXPcieCplD);
  carbonXPcieTLPSetAddr(comp,       carbonXPcieTLPGetAddr(mRequester));
  carbonXPcieTLPSetReqId(comp,      carbonXPcieTLPGetReqId(mRequester));
  carbonXPcieTLPSetTag(comp,        carbonXPcieTLPGetTag(mRequester));
  carbonXPcieTLPSetDataLength(comp, carbonXPcieTLPGetDataLength(mRequester));
  carbonXPcieTLPSetCplStatus(comp,  0); // Successful Completion
  
  // For Read Transactions, calculate Byte Count and Lower Address, and set Return Data
  if( isRead() ) {
  
    // Calculate Byte Count
    UInt32 fbe = carbonXPcieTLPGetFirstBE(mRequester);
    UInt32 lbe = carbonXPcieTLPGetLastBE(mRequester);
    UInt32 byte_cnt = 0;
    for(UInt32 i = 0; i < 4; i++)
      if(fbe & (1<<i)) {
	byte_cnt = 4 - i;
	break;
      }
    
    if(carbonXPcieTLPGetDataLength(mRequester) > 1) {
      byte_cnt += (4* (carbonXPcieTLPGetDataLength(mRequester)-1));
      for(int i = 3; i >= 0; i--) 
	if(lbe & (1<<i) == 0) {
	  --byte_cnt;
	  break;
	}
    }
    carbonXPcieTLPSetByteCount(comp, byte_cnt);

    // Calculate Low Addr
    UInt32 low_addr = carbonXPcieTLPGetAddr(mRequester) & 0x7fcll;
    for(UInt32 i = 0; i < 4; i++) {
      if(fbe & (1<<i)) {
	// Low Add[6:2] should be addr[6:2] Low Addr [1:0] should be the first
	// readable byte in DWORD 0
	low_addr = (carbonXPcieTLPGetAddr(mRequester) & 0x7fcll) + i;
	break;
      }
    }
    carbonXPcieTLPSetLowAddr(comp, low_addr);

    //cout << "Byte Count = " << dec << byte_cnt << " Low Addr = " << hex << low_addr << endl;

    // Get Read Data from the response transaction
    UInt32  num_dw       = carbonXPcieTLPGetDataLength(mRequester);
    UInt64* read_data_64 = reinterpret_cast<UInt64*>(resp.mData);
    UInt32  start_idx    = (carbonXPcieTLPGetAddr(mRequester) & 0x4) ? 1 : 0;
    UInt32  word_idx     = 0;
    for(UInt32 i = start_idx; i < (num_dw+start_idx); i++) {
      UInt32 read_data_32 = read_data_64[i/2] >> (32*(i&1));
      carbonXPcieTLPSetDWord(comp, word_idx++, read_data_32); 
    }
    
  }
  else {
    carbonXPcieTLPSetByteCount(comp, 0);
    carbonXPcieTLPSetLowAddr(comp, 0);
  }
  
  // Report that a completer was created
  return true;
}

bool CarbonXPciePciPort::PciTrans::isDone() const {
  return (cts == CARBONX_SOCVSP_CTS_DONE); // Fixme
}

bool CarbonXPciePciPort::PciTrans::isValid() const {
  return mValid;
}

bool CarbonXPciePciPort::PciTrans::isPosted() const {
  if(!mRequester)
    return false;
  else
    return ( (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieMWr32) || 
	     (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieMWr64) );
}

bool CarbonXPciePciPort::PciTrans::isRead() const {
  if(!mRequester)
    return false;
  else
    return ( (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieMRd32)   || 
	     (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieMRd64)   ||
	     (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieMRdLk32) ||
	     (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieMRdLk64) ||
	     (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieIORd)    ||
	     (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieCfgRd0)  ||
	     (carbonXPcieTLPGetCmdByName(mRequester) == eCarbonXPcieCfgRd1) );
}

/*
 * CarbonXPciePciPort Methods
 *
 *
 */

CarbonXPciePciPort::CarbonXPciePciPort(sc_port<sc_mx_transaction_if>* master) :
  mTMaster(master)
{
  mTransaction = new PciTrans(master);
}

CarbonXPciePciPort::~CarbonXPciePciPort()
{
  delete mTransaction;
}

void CarbonXPciePciPort::reset()
{
  mTransaction->reset();
}

bool CarbonXPciePciPort::issueTransaction(const CarbonXPcieTLP* tlp)
{
  if(mTransaction->cts == CARBONX_SOCVSP_CTS_NOT_STARTED) {
    if(mTransaction->setRequester(tlp)) {
      mTransaction->cts++; // Mark that transaction is queued
      mTMaster->driveTransaction(mTransaction);
      return true;
    }
  }
  
  // If we got here, the transaction did not get driven from some reason
  return false;
}

bool CarbonXPciePciPort::receiveTransaction(CarbonXPcieTLP* tlp)
{
  // Posted transactions does not return a completer, but we still need to reset the transaction
  if(mTransaction->isPosted()) {
    mTransaction->reset();
    return false;
  }
  else if(mTransaction->createCompleter(tlp)) {
    mTransaction->reset();
    return true;
  }
  return false;
}
