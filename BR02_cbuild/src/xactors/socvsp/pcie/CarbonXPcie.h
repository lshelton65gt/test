/***************************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONXPCIE_H_
#define _CARBONXPCIE_H_

#include "maxsim.h"
#include "xactors/arm/include/carbon_arm_adaptor.h"
#include "xactors/pcie/carbonXPcie.h"
#include <queue>
#include <list>
#include <string>


// Forward Declarations
class CarbonXPcieTPort;
class CarbonXPcieTransInfo;
class CarbonXPcieNetMaxsimAdaptor;

#define MASTER_PORT_NET CarbonXPcieNetMaxsimAdaptor

class CarbonXPcie
{
public:
  // Place instance declarations for the Ports here:
  CarbonXtorAdaptorToVhmPort    * mPhyStatus;
  CarbonXtorAdaptorToVhmPort    * mRxData[32];
  CarbonXtorAdaptorToVhmPort    * mRxDataK[32];
  CarbonXtorAdaptorToVhmPort    * mRxElecIdle[32];
  CarbonXtorAdaptorToVhmPort    * mRxStatus[32];
  CarbonXtorAdaptorToVhmPort    * mRxValid[32];
  CarbonXPcieTPort*               mPciePort;
  
  MASTER_PORT_NET               * mPhyStatusNet;
  MASTER_PORT_NET               * mRxDataNet[32];
  MASTER_PORT_NET               * mRxElecIdleNet[32];
  MASTER_PORT_NET               * mRxDataKNet[32];
  MASTER_PORT_NET               * mRxStatusNet[32];
  MASTER_PORT_NET               * mRxValidNet[32];

  // constructor / destructor
  CarbonXPcie(sc_mx_module* carbonComp, 
              const char *xtorName,
	      CarbonXPcieIntfType intf,
              UInt32 num_lanes,
              CarbonObjectID **carbonObj, 
              CarbonPortFactory *portFactory);
  void registerPort(sc_mx_p_base* port, const string& name);
  void registerPort(CarbonXtorAdaptorToVhmPort* port, const string& name);
  
  // Member variables used when used in a Carbon component
  string mXtorName;
  sc_mx_module *mCarbonComponent;
  CarbonObjectID **mpCarbonObject;

  virtual ~CarbonXPcie();
  
  // overloaded methods for clocked components
  void communicate();
  void update();
  
  // overloaded sc_mx_module methods
  void setParameter(const string &name, const string &value);
  void init();
  void terminate();
  void reset(MxResetLevel level, const MxFileMapIF *filelist);
  
  // Used to connect transactor input signals to the Carbon Model
  void connect(CarbonXPcieSignalType signal, CarbonUInt32 lane, CarbonNetID* netId);

  static void pcieTransCB(CarbonXPcieID *pcie, CarbonXPcieCallbackType type, void *info);
  static void pcieEndTransCB(CarbonXPcieID *xtor, void *caller, CarbonXPcieTLP *cmd);
  
  void reportTransactionDone(CarbonXPcieTLP *cmd);
  void setRateChangeCallbackFn(void *caller, CarbonXPcieRateChangeCBFn *rateChangeCbFn);

private:
  bool initComplete; 
  bool p_enableDbgMsg;
  
  // User defined parameters:
  unsigned int p_NumLanes;
  unsigned int p_MaxPayloadSize;
  unsigned int p_ReadCompletionBoundary;
  unsigned int p_MaxInFlight;
  bool         p_ScrambleStatus;
  bool         p_DoTraining;
  bool         p_TrainingBypass;
  bool         p_IdleIsHiTrue;
  bool         p_ForceIdleInactive;
  unsigned int p_DebugPrintLevel;
  unsigned int p_BusNum;
  unsigned int p_DevNum;
  unsigned int p_FuncNum;
  

  // Transactor Member Variables
  CarbonXPcieID                   * mPcieInst;
  CarbonXPcieTransInfo            * mRxTrans;
  UInt32                            mNumLanes;
  CarbonXPcieIntfType               mIntf;
  std::deque<CarbonXPcieTransInfo*> mTxTransQueue;
  std::list<CarbonXPcieTransInfo*>  mRunningTrans;
  MxU64 mTick;
};

#endif
