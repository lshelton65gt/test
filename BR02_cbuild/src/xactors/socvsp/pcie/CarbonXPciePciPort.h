// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#ifndef __CarbonXPciePciPort__
#define __CarbonXPciePciPort__

#include "CarbonXTransInfo.h"
#include <list>

class CarbonXPcieTLP;

class CarbonXPciePciPort {
public :

  //! Class to create and hold PCI transactions to send over the PCI port
  /*!
    Class also stores a copy of the requester transaction so it can be used
    when constructing the completer.
  */
  class PciTrans : public CarbonXTransInfo {
  public:
    //! Constructor
    /*!
      \param port Handle of master port that owns the PCI transaction
      \param requester Pointer to TLP from which to create the PCI transaction
    */
    PciTrans(sc_mx_transaction_p_base *port);

    //! Destructor
    ~PciTrans();
    
    //! Reset Method
    void reset();

    //! Set Requester
    /*!
      Creates a PCI request based on the PCIE requester transaction.
      \param requester Pointer to PCIE Requester transaction to send to the PCI port.
      \retval true if \a reuester was a valid requester transaction and there were not 
                   already a transaction in progress
      \retval false if setting the requester failed for some reason.
    */
    bool setRequester(const CarbonXPcieTLP* requester);

    //! Create Completer Transaction.
    /*!
      Create completer transaction based on return data from PCI transaction and
      the requester transaction that created the PCI transaction.

      \param comp Pointer to where completer is begin returned
      \retval true if completer was successfully constructed
      \retval false if completer was not successfully constructed
    */
    bool createCompleter(CarbonXPcieTLP* comp); 
    
    //! Check if PCI transaction is completed
    /*!
      \retval true if transaction is done.
      \retval false if transaction is not done.
    */
    bool isDone() const;
    
    //! Check if PCI transaction is valid
    /*!
      \retval true if transaction was correctly constructed.
      \retval false if transaction was not correctly constructed
    */
    bool isValid() const;

    //! Check if PCI transaction is posted
    /*!
      \retval true if transaction is a posted write (MWr32 or MWr64).
      \retval false if transaction is not posted
    */
    bool isPosted() const;

    //! Check if PCI transaction is a read transaction
    /*!
      \retval true if transaction is a read.
      \retval false if transaction is not a read.
    */
    bool isRead() const;

  private :
    //! Utility function
    UInt32 calcBE(UInt32 addr, UInt32 size, UInt32 fbe, UInt32 lbe);

    //! Pointer to Copy of requester that initiated transaction 
    CarbonXPcieTLP* mRequester;

    //! Whether this transaction is contructed correctly
    bool            mValid;

  };
  
  //! Constructor
  CarbonXPciePciPort(sc_port<sc_mx_transaction_if>* master);
  
  //! Destructor
  ~CarbonXPciePciPort();

  //! Reset Method
  /*!
    Should be called by the reset method of CarbonXPcieTPort.
  */
  void reset();

  //! Issue a PCI Transaction based on a PCIE TLP object
  /*
    \param tlp Pointer to a TLP transaction object.
  */
  bool issueTransaction(const CarbonXPcieTLP* tlp);

  //! Receive a completer transaction based on the response of the pci transaction
  /*! 
    \param tlp Pointer to a TLP transaction object where to store the received transaction.
    \retval true if a transaction is available in the receive queue.
    \retval false if a transacion is not available in the receive queue.
  */
  bool receiveTransaction(CarbonXPcieTLP* tlp);

private :
  
  //! Transacion Master Port
  sc_port<sc_mx_transaction_if>*   mTMaster;
  
  //! Transaction Structure
  PciTrans*                        mTransaction;
};

#endif
