// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 
#ifndef __CarbonXPcieTPort__
#define __CarbonXPcieTPort__

#include "maxsim.h"
#include <list>
#include "xactors/pcie/carbonXPcie.h"

/*!
  \defgroup PcieSOCDesigner PCI Express Transaction Port for SOC Designer
  \brief User Interface for the PCI Express transactor in SoC Designer
*/


/*!
    \addtogroup PcieSOCDesigner
    @{
*/

// Forward Declarations
class CarbonXPcieTransInfo;
class CarbonXPciePciPort;

//! Master and Slave Ports For Carbon's PCIE Transactor
/*!
  This class instantiates ports for both directions of the PCI Express
  link. The outgoing port is a SoC Designer Transaction Master Port
  and the incoming port a SoC Designer Transaction Slave Port.
  
  This portclass handles sending CarbonXPcieTLP objects 
  This SoC Designer Transaction Master Port can be used in
  a user written Component to execute PCIE transactions on 
  Carbon's PCIE transactors. 
  
  The Port can operate in two ways. It supports a blocking interface where
  the user can execute read or write operations where posted transaction
  would block until the corresponding completer transaction is returned on
  the receving port. These methods require to be executed in an SC_THREAD
  in the component and will generate a runtime error if executed in a NON
  SC_THREAD.
  An SC_THREAD can be started by calling SC_THREAD(<user_method>);
  in the init method of the users component.

  The other sets of methods support sending and reciving PCIE Transaction
  Level Packets in a non blocking fashion more suitable to the SoC Designer
  environment, but can require more complex component code by the user.
*/
class CarbonXPcieTPort : public sc_mx_transaction_slave {
public:
  
  //! Port Constructor
  CarbonXPcieTPort(sc_mx_module* owner, const std::string& portName);

  //! Destructor
  ~CarbonXPcieTPort();

  /******************************************************************
   * Definitions
   *****************************************************************/

  //! Callback function prototype
  /*!
    \param info User pointer that will be specified when the callback is registered.
    \param cmd reference to the CarbonXPcieCmd corresponding to the TLP to get sent
  */
  typedef void (TransCBFn)(void* info, const CarbonXPcieTLP* cmd);
  
  /******************************************************************
   * Configuration Methods (Non Blocking).
   * These methods can be called from anywhere in an SoC Designer Component.
   *****************************************************************/

  //! Set Bus Number
  /*!
    Set PCI Express Bus Number for the port. 
    This is not used when sending transactions using the addTransaction method.
    \param num Bus Number
  */
  void   setBusNum(CarbonUInt32 num);

  //! Set Device Number
  /*!
    Set PCI Express Device Number for the port. 
    This is not used when sending transactions using the addTransaction method.
    \param num Bus Number
  */
  void   setDeviceNum(CarbonUInt32 num);

  //! Set Bus Number
  /*!
    Set PCI Express Function Number for the port. 
    This is not used when sending transactions using the addTransaction method.
    \param num Bus Number
  */
  void   setFunctionNum(CarbonUInt32 num);

  /******************************************************************
   * Non Blocking Methods.
   * These methods can be called from anywhere in an SoC Designer Component.
   *****************************************************************/
  
  //! Add a PCI Express Transaction.
  /*!
    Adds a Transaction Level Packet to the transaction queue of a
    transactor connected to this transaction port.. 
    \param tlp Reference to a TLP transaction object.
  */
  bool addTransaction(const CarbonXPcieTLP* tlp);

  //! Receive a PCI Express transaction.
  /*! 
    Receives a Transaction Level Packet from a transactor connected
    to this transaction port and removes the transaction from the 
    received transaction queue.
    \param tlp Pointer to a TLP transaction object where to store the received transaction.
    \retval true if a transaction is available in the receive queue.
    \retval false if a transacion is not available in the receive queue.
  */
  bool receiveTransaction(CarbonXPcieTLP* tlp);

  //! Set End of Transaction callback function.
  /*!
    Registers a callback function that will be called when a transaction
    has finished transmitting over the interface. The callback will
    be passed a transaction object with the transaction that has just
    transferred. A user can use this callback to check the start and
    endtime of the transaction if desired. The callback will be called
    in the parent component's update phase.

    \param info Pointer to user data that will be passed to the callback when called.
    \param transCBFn Pointer to callback function to be called.
  */
  void setEndTransCallbackFn(void* info, TransCBFn* transCBFn);

  //! Set Receipt of Transaction callback function.
  /*!
    Registers a callback function that will be called when a transaction
    has been received from the transaction port. The callback will
    be passed a transaction object with the transaction that has just
    been received. When the callback is called the transaction will also
    be taken off the receive queue, so no call to recieveTransaction is
    required. If the user want's to use receiveTransaction, this callback
    should not be registered. The callback will be called in the parent
    component's communicate phase.

    \param info Pointer to user data that will be passed to the callback when called.
    \param transCBFn Pointer to callback function to be called.
  */
  void setReceivedTransCallbackFn(void* info, TransCBFn* transCBFn);

  /******************************************************************
   * Blocking Methods.
   * These methods can only called from SC_THREADS, when the component
   * supports event based simulation using SystemC.
   * Beware that peformance may lag when running in event mode.
   *****************************************************************/

  //! Perform a 32 bit PCIE Memory Read 
  /*!
    \param address Address to be read
    \return Data read
  */
  CarbonUInt32 memRead32(CarbonUInt64 address );

  //! Perform a 32 bit PCIE Memory Write 
  /*!
    Write one Double Word of data to memory space at the specified address. The address has to
    be an even DWORD address. The 2 LSB bits will be masked off before use.
    Write Data is masked by the specified byte enable \a be.
    Bit 3 of \a be corresponds to the least significant byte of \a data. 
    Bit 0 of \a be corresponds to the most significant byte of \a data.
    \param address Address to be written
    \param data Data to be written
    \param be Byte Enable. 
  */
  void memWrite32(CarbonUInt64 address, CarbonUInt32 data, CarbonUInt32 be = 0xf );
  
  //! Perform a blocking PCIE Read of an array of 32 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be read
    \param size Number of 32 bit values to be read
  */
  void memRead(CarbonUInt64 address, CarbonUInt32* data, CarbonUInt32 size);

  //! Perform a blocking PCIE Memory write of an array of 32 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 32 bit values to be written
    \param first_be Byte Enable for first DWord in the transaction
    \param last_be Byte Enable for first DWord in the transaction
  */
  void memWrite(CarbonUInt64 address, const CarbonUInt32* data, CarbonUInt32 size,
		    CarbonUInt32 first_be, CarbonUInt32 last_be);

  //! Perform a 32 bit PCIE IO Read 
  /*!
    \param address Address to be read
    \return Data read
  */
  CarbonUInt32 ioRead32(CarbonUInt32 address);

  //! Perform a 32 bit PCIE IO Write 
  /*!
    \param address Address to be written
    \param data Data to be written
    \param be Byte Enable
  */
  void ioWrite32(CarbonUInt32 address, CarbonUInt32 data, CarbonUInt32 be);

  //! Perform a PCIE Configuration 0 Space Read (always 32 bit)
  /*!
    \param address Address to be read
    \return Data read
  */
  CarbonUInt32 cfg0Read32(CarbonUInt32 address);

  //! Perform a PCIE Configuration 0 Space Write (always 32 bit)
  /*!
    \param address Address to be written
    \param data Data to be written
    \param be Byte Enable
  */
  void cfg0Write32(CarbonUInt32 address, CarbonUInt32 data, CarbonUInt32 be);


  /******************************************************************
   * SoC Designer Simulation Phase Methods
   * These are the required SoC Designer Transaction Slave Methods.
   *****************************************************************/
  //! Init Method
  /*!
    Should be called by the init method in the parent component.
  */
  void init();

  //! Reset Method
  /*!
    Should be called by the reset method in the parent component.
  */
  void reset();

  //! Communicate Method
  /*!
    Should be called by the communicate method in the parent component.
  */
  void communicate();

  //! Update Method
  /*!
    Should be called by the update method in the parent component.
  */
  void update();

  /******************************************************************
   * Transaction Slave Methods.
   * These are the required SoC Designer Transaction Slave Methods.
   *****************************************************************/
  //! Transaction Slave Read Method (Not Implemented)
  MxStatus read(MxU64, MxU32*, MxU32*) { return MX_STATUS_NOTSUPPORTED; }

  //! Transaction Slave Write Method (Not Implemented)
  MxStatus write(MxU64, MxU32*, MxU32*) { return MX_STATUS_NOTSUPPORTED; }

  //! Transaction Slave Drive Transaction Method
  void driveTransaction(MxTransactionInfo* info);

  //! Transaction Slave Cancel Transaction Method
  void cancelTransaction(MxTransactionInfo* info);
  
private:
  /******************************************************************
   * Private Types.
   *****************************************************************/
  enum PortType {
    ePCIE,
    ePCI,
    eILLEGAL
  };

  /******************************************************************
   * Utility Methods.
   *****************************************************************/
  
  //! Send TLP
  void sendTransaction(const CarbonXPcieTLP* req);

  //! Perform a blocking transaction
  void blockingTransaction(const CarbonXPcieTLP* req, CarbonXPcieTLP* cmp);

  //! Increment Current Tag value
  inline CarbonUInt32 getNextTag()
  {
    mTxTag = (mTxTag + 1) & 0xff; 
    return mTxTag;
  }

  /******************************************************************
   * Private Member Variables.
   *****************************************************************/
  //! Pointer to Owning Component
  sc_mx_module* mOwner;

  //! Transaction Master Port
  sc_port<sc_mx_transaction_if>* mTMaster;

  //! Transaction Transmitt Queue
  std::list<CarbonXPcieTransInfo *> mTransmitQueue;

  //! Transaction Receive Queue
  std::list<CarbonXPcieTransInfo *> mReceiveQueue;
  
  // PCIE ID Parameters
  CarbonUInt32 mBusNum;
  CarbonUInt32 mDeviceNum;
  CarbonUInt32 mFunctionNum;
  CarbonUInt32 mTxTag;

  // Callback Parameters
  void*      mEndTransCBInfo;
  TransCBFn* mEndTransCBFn;
  void*      mReceivedTransCBInfo;
  TransCBFn* mReceivedTransCBFn;
  
  // Members used by blocking methods
  sc_event              mBlockingEvent;
  bool                  mBlocked;
  const CarbonXPcieTLP* mBlockedReq;
  CarbonXPcieTransInfo* mBlockedCpl;

  // Port Type
  PortType              mPortType;
  std::string           mPortName;

  // Used if connected to a PCI Port
  CarbonXPciePciPort*   mPciPort;
};

/*!
  @}
*/

#endif
