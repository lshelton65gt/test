// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/ 

#ifndef __carbonxpcisctmaster__
#define __carbonxpcisctmaster__

#include "xactors/socvsp/CarbonXSCTMaster.h"
#include "xactors/systemc/CarbonXPciIfs.h"

//! Blocking Master Port For Carbon's PCI Transactor
/*!
  This SoC Designer Transaction Master Port can be used
  in a user written Component to execute PCI transactions on 
  Carbon's PCI transactors. The port is using SystemC Waits to block
  and wait for transactions to be done. Therefor the methods in 
  this class has to be called from an SC_THREAD in the users component.
  An SC_THREAD can be started by calling SC_THREAD(<user_method>);
  in the init method of the users component.

  You should instantiate this class instead of the MxTransactionMasterPort
  that should be connected to the corresponding transaction slave port 
  of the Carbon Generated Component.

  You have to call the communicate and update methods in the port
  in the the respective methods in your component.
*/
class CarbonXPciSCTMaster : public CarbonXSCTMaster {
public:
  
  //! Constructor
  CarbonXPciSCTMaster(sc_mx_m_base *owner, const string& portName) :  
    CarbonXSCTMaster(owner, portName)  {

    // Setup Port Properties
    MxTransactionProperties prop;
    memset(&prop,0,sizeof(prop));
    prop.mxsiVersion = MXSI_VERSION_6;
    prop.useMultiCycleInterface = true;
    prop.addressBitwidth=64;
    prop.mauBitwidth = 8;
    prop.dataBitwidth = 32;
    prop.dataBeats = 64;
    prop.isLittleEndian = true;
    prop.isOptional = false;
    prop.supportsAddressRegions = false;
    prop.numCtrlFields = 0;
    prop.numSlaveFlags = 1;
    prop.numMasterFlags = 3;
    prop.validTimingTable = NULL;
    prop.protocolID = 0x00000001;
    sprintf(prop.protocolName,"Carbon PCI Initiator");
    prop.supportsNotify = true;
    prop.supportsBurst = true;
    prop.supportsSplit = false;
    prop.isAddrRegionForwarded = false;
    prop.forwardAddrRegionToMasterPort = NULL;
    
    setProperties(&prop);  
  }
  
  //! Perform a PCI Configuration Space Write (always 32 bit)
  /*!
    \param address Address to be written
    \param data Data to be written
  */
  void pciCfgWrite32(UInt64 address, UInt32 data) {
    mPciReq.pciCfgWriteReq(address, data);
    _pciPut();
    _block();
    _cleanup();
  }

  //! Perform a PCI Configuration Space Read (always 32 bit)
  /*!
    \param address Address to be read
    \return Data read
  */
  UInt32 pciCfgRead32(UInt64 address) {
    UInt32 ret;
    mPciReq.pciCfgReadReq(address);    
    _pciPut();
    _block();
    if(mTransInfo->dataRd) ret = mTransInfo->dataRd[0];
    else ret = 0;
    _cleanup();
    return ret;
  }

  //! Perform a 32 bit PCI Memory Write 
  /*!
    \param address Address to be written
    \param data Data to be written
  */
  void   pciMemWrite32(UInt64 address, UInt32 data ) {
    mPciReq.pciMemWriteReq(address, &data, 4, false);
    _pciPut();
    _block();
    _cleanup();
  }

  //! Perform a 16 bit PCI Memory Write 
  /*!
    \param address Address to be written
    \param data Data to be written
  */
  void   pciMemWrite16(UInt64 address, UInt16 data ){
    UInt32 data32 = data;
    mPciReq.pciMemWriteReq(address, &data32, 2, false);
    _pciPut();
    _block();
    _cleanup();
  }

  //! Perform an 8 bit PCI Memory Write 
  /*!
    \param address Address to be written
    \param data Data to be written
  */
  void   pciMemWrite8(UInt64 address, UInt8  data ){
    UInt32 data32 = data;
    mPciReq.pciMemWriteReq(address, &data32, 1, false);
    _pciPut();
    _block();
    _cleanup();
  }
  
  //! Perform a 32 bit PCI Memory Read 
  /*!
    \param address Address to be read
    \return Data read
  */
  UInt32 pciMemRead32(UInt64 address ){
    UInt32 ret;
    mPciReq.pciMemReadReq(address, 4, false);
    _pciPut();
    _block();
    if(mTransInfo->dataRd) ret = mTransInfo->dataRd[0];
    else ret = 0;
    _cleanup();
    return ret;
  }

  //! Perform a 16 bit PCI Memory Read 
  /*!
    \param address Address to be read
    \return Data read
  */
  UInt16 pciMemRead16(UInt64 address ){
    UInt32 ret;
    mPciReq.pciMemReadReq(address, 2, false);
    _pciPut();
    _block();
    if(mTransInfo->dataRd) ret = mTransInfo->dataRd[0];
    else ret = 0;
    _cleanup();
    return ret;
  }

  //! Perform a 8 bit PCI Memory Read 
  /*!
    \param address Address to be read
    \return Data read
  */
  UInt8  pciMemRead8(UInt64 address ){
    UInt32 ret;
    mPciReq.pciMemReadReq(address, 1, false);
    _pciPut();
    _block();
    if(mTransInfo->dataRd) ret = mTransInfo->dataRd[0];
    else ret = 0;
    _cleanup();
    return ret;
  }

  //! Perform a blocking PCI Memory write of an array of 32 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 32 bit values to be written
  */
  void pciMemWrite(UInt64 address, const UInt32 * data, UInt32 size){
    mPciReq.pciMemWriteReq(address, data, size, false);
    _pciPut();
    _block();
    _cleanup();
  }

  //! Perform a blocking PCI Read of an array of 32 bit values to the given address
  /*!
    \param address Address for the transaction
    \param data Pointer to the array to be read
    \param size Number of 32 bit values to be read
  */
  void pciMemRead(UInt64 address, UInt32 * data, UInt32 size){
    mPciReq.pciMemReadReq(address, size, false);
    _pciPut();
    _block();
    UInt32 cpy_size = (size > mTransInfo->dataSize) ? mTransInfo->dataSize : size;
    cpy_size = ( (cpy_size + 3)/4) *4; // Copy full Uint32 words
    memcpy(data, mTransInfo->dataRd, cpy_size);    
    _cleanup();
  }

  //! Perform a blocking Memory write of an array of 32 bit values to the given address
  /*!
    This method is using 64 bit data bus width over the PCI bus.
    Note that the data is still an array of 32 bit values.
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 32 bit values to be written
  */
  void pci64MemWrite(UInt64 address, const UInt32 * data, UInt32 size){
    mPciReq.pciMemWriteReq(address, data, size, true);
    _pciPut();
    _block();
    _cleanup();
  }

  //! Perform a blocking Memory read of an array of 32 bit values from the given address
  /*!
    This method is using 64 bit data bus width over the PCI bus.
    Note that the data is still an array of 32 bit values.
    \param address Address for the transaction
    \param data Pointer to the array to be written
    \param size Number of 32 bit values to be written
  */
  void pci64MemRead(UInt64 address, UInt32 * data, UInt32 size){
    mPciReq.pciMemReadReq(address, size, true);
    _pciPut();
    _block();
    UInt32 cpy_size = (size > mTransInfo->dataSize) ? mTransInfo->dataSize : size;
    cpy_size = ( (cpy_size + 3)/4) *4; // Copy full Uint32 words
    memcpy(data, mTransInfo->dataRd, cpy_size);    
    _cleanup();
  }

  //! Perform a 32 bit PCI IO Write 
  /*!
    \param address Address to be written
    \param data Data to be written
  */
  void pciIOWrite32(UInt32 address, UInt32 data, UInt32 req_id  = 0, UInt32 req_tag = 0) {
    mPciReq.pciIOWriteReq(address, data, 4, req_id, req_tag);
    _pciPut();
    _block();
    _cleanup();
  }

  //! Perform a 32 bit PCI IO Read 
  /*!
    \param address Address to be read
    \return Data read
  */
  UInt32 pciIORead32(UInt32 address, UInt32 req_id  = 0, UInt32 req_tag = 0) {
    UInt32 ret;
    mPciReq.pciIOReadReq(address, 4, req_id, req_tag);
    _pciPut();
    _block();
    if(mTransInfo->dataRd) ret = mTransInfo->dataRd[0];
    else ret = 0;
    _cleanup();
    return ret;
  }
    
  //! Perform an Interrupt Acknowledge transaction
  void carbonXPciIntAck(){
    mPciReq.pciIntAckReq();
    _pciPut();
    _block();
    _cleanup();
  }
  
private:
  
  //! Carbon's PCI Transaction Structure
  CarbonXPciTransReqT mPciReq;

  //! Help Method to create a SoC Designer transaction from a Carbon PCI transaction
  // and send to tell the communicate method to send it.
  inline void _pciPut()   { 
    mTransInfo = new CarbonXTransInfo(this, mPciReq);
    mStartTransFlag = 1;
  }
  
};

#endif
