//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//  Description - Synthesizable Ethernet Receiver BFM.
//                Attaches to the Tx interface of the DUT

`timescale 1ns / 100 ps

module carbonx_enetrgmii_rx (
    sys_clk   
  , sys_reset_l
  , TXD      
  , TX_CTL
  , TX_CLK
);

  // Primary IO
  input       sys_clk, sys_reset_l;
  input [3:0] TXD;     
  input       TX_CLK;  
  input       TX_CTL;

`protect
  reg [7:0] txd ;
  reg [3:0] txd_hi, txd_lo; 
  reg       tx_en,  tx_er ; 
  reg       tx_en_d,  tx_er_d ; 
  wire      crs,    col   ; 

   // Since we can't delay the clock in a carbon model
   // The data and control is captured on the wrong edge on
   // purpose since it is assumed that the DUT is putting out the data
   // on the correct edge and therefore the clock will arrive to the
   // transactor before the data instead of after as the specification states.
   always @(negedge TX_CLK)
     tx_en <= TX_CTL;
   
   always @(posedge TX_CLK)
     tx_er <= TX_CTL ^ tx_en;

   always @(negedge TX_CLK)
     txd_lo <= TXD;

   always @(posedge TX_CLK)
     txd_hi <= TXD;

   always @(negedge TX_CLK) begin
      txd     <= { txd_hi, txd_lo };
      tx_er_d <= tx_er;
      tx_en_d <= tx_en;
   end
   
  carbonx_enetgmii_rx rx (
      .sys_clk     ( ~TX_CLK     )   
    , .sys_reset_l ( sys_reset_l )
    , .TXD         ( txd         )      
    , .TX_CLK      ( TX_CLK      )
    , .TX_ER       ( tx_er_d    ) 
    , .TX_EN       ( tx_en_d   ) 
    , .CRS         ( crs         ) 
    , .COL         ( col         ) 
  );

`endprotect
endmodule
