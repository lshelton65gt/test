//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//  Description - Synthesizable Ethernet Transmitter BFM.
//                Attaches to the Rx interface of the DUT

`timescale 1ns / 100 ps

module carbonx_enetrgmii_tx (
   sys_clk
 , sys_reset_l
 , RXD       
 , RX_CTL     
 , RX_CLK    
);

  // Primary IO
   input       sys_clk, sys_reset_l;
  output       RX_CLK;   // Receive clock - 125MHz
  output       RX_CTL;   // Receive control
  output [3:0] RXD;      // Receive data

`protect
  reg  [3:0] status;
  wire [7:0] rxd;
  wire [3:0] rxd_hi = rxd[7:4];
  wire [3:0] rxd_lo = rxd[3:0];
  wire       RX_DV;
  wire       RX_ER;
  wire       RX_CTL;   
  wire interframe_n = RX_DV | RX_ER;

//GMII_RX_DV GMII_RX_ER RXD[7:0] 
//0 0 # xxxL link status 0=down, 1=up
//0 0 # xCCx RXC clock speed 00=2.5Mhz, 01=25Mhz, and 10=125Mhz, 11=reserved
//0 0 # Dxxx duplex status 0=half-duplex, 1=full duplex

  // Since we can't model a delay on the clock line, the data
  // is outputted half a cycle of in purpose (low nibble when clock is low
  // and high nibble when clock is high). This way the receiver can clock in
  // the data using correct edge, since the clock is going to be faster than
  // the data.     
  // We deal with that by inverting the output clock
  assign RX_CLK = ~sys_clk;
   
  always @(tx.half_duplex or tx.sel_clk_10 or tx.sel_clk_100 or tx.phy_link)
    begin
      status[0] =   tx.phy_link ; 
      status[3] = ! tx.half_duplex ;
    
      case( {tx.sel_clk_100, tx.sel_clk_10} )
        01:     status[2:1] = 2'b00 ;
        10:     status[2:1] = 2'b01 ;
       default: status[2:1] = 2'b10 ;
      endcase
    end

  assign RX_CTL = sys_clk ? RX_DV : RX_DV ^ RX_ER;
  assign RXD    = interframe_n ? (sys_clk ? rxd_lo : rxd_hi) : status ;

  carbonx_enetgmii_tx tx (
     .sys_clk     ( sys_clk     )
   , .sys_reset_l ( sys_reset_l )
   , .RXD         ( rxd         )       
   , .RX_DV       ( RX_DV       )     
   , .RX_ER       ( RX_ER       )     
   , .RX_CLK      ( )    
  );

`endprotect

endmodule

