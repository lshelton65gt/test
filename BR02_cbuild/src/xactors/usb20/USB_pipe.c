//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file includes all functions relating to PIPE operation,
 *                 It can be classified into 3 groups:
 *                 [USB_*, USB_PIPE_*, USB_PIPE_PA_*]
 *
 *                 USB_* are API functions that user can use for pipe related
 *                 operations. They include:
 *
 *                 carbonXUsbPipeCreate();
 *                 carbonXUsbPipeDescrUpdate();
 *                 carbonXUsbPipePushInIRP();
 *                 carbonXUsbPipePopInIRP();
 *                 carbonXUsbPipePushOutIRP();
 *                 carbonXUsbPipePopOutIRP();
 *                 carbonXUsbPipeGetState();
 *                 carbonXUsbPipeSetState();
 *                 carbonXUsbPipeGetNumComplIRPs();
 *                 carbonXUsbPipeGetNumUncomplIRPs();
 *                 carbonXUsbPipeReport();
 *                 carbonXUsbPipeDestroy();
 *                 carbonXUsbSetEpDescr();
 *                 carbonXUsbGetPipeDescr();
 *                 carbonXUsbGetDefPipe();
 *
 *                 USB_PIPE_* are functions that the SVC uses for operation on  
 *                 pipes. They include:
 *
 *                 USB_PIPE_DescrReset();
 *                 USB_PIPE_GetNewDescr();
 *                 USB_PIPE_IrpLinkGetNew();
 *                 USB_PIPE_IrpLinkFree();
 *                 USB_PIPE_Update();
 *                 USB_PIPE_RetireCompletedIRPs ();
 *                 USB_PIPE_MsgHeader();
 *                 USB_PIPE_MsgTotal();
 *                 USB_PIPE_Msg();
 *                 USB_PIPE_MsgEnd();
 *
 *                 USB_PIPE_PA_* are functions that SVC uses to operate on the 
 *                 pipe pointer array struct, they include:
 *
 *                 USB_PIPE_PA_IsFull();
 *                 USB_PIPE_PA_Cur();
 *                 USB_PIPE_PA_Free ();
 *                 USB_PIPE_PA_Reset();
 *                 USB_PIPE_PA_Init();
 *                 USB_PIPE_PA_GetPipeCount();
 *
 *                 Note:
 *                 1. Pipe ID #0
 *                   Pipe ID #0 is reserved for all device. The pipe's 
 *                   endpoint number is zero, and device address is zero. 
 *                   If the device operates at low-speed, the pipe 0 operates
 *                   speed is full-speed; otherwise, the speed is same with others.
 *                 2. Auto built feature
 *                   This is a user convinence feature implemented in this module as:
 *                   2.1) user can set "is_auto" flag to true for pipe Z during  
 *                        carbonXUsbPipeCreate() call, which will enable the following:
 *                   2.2) Pipe Z is in a state USB_PIPE_STAT_NEED_SETUP and unusable,
 *                        pipe needs to be in USB_PIPE_STAT_READY to be usable, and 
 *                        this is done by some IRP Y of some CTRL pipe X.
 *                   2.3) User could mannually monitor CTRL pipe X and IRP Y to 
 *                        trigger a state change on pipe Z. But with "is_auto=TRUE", 
 *                        auto_trigger will be set automatically by the SVC to be 
 *                        [trigger_pipe_id=X, trigger_irp_id=Y].
 *                   2.3) Each time a CTRL pipe (X') successfully executes an IRP (Y'),
 *                        we will check if (X==X' && Y==Y').
 *                        when this condition is TRUE, pipe Z becomes READY automatically.
 *                 
 *                 
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.3  2006/02/24 19:08:39  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: nightly checkin
 *               : Bugs Affected: 5501
 *               :
 *               : Fixed many warnings outputted by ICC version 9 compiler.
 *               : Fixed a test failure in AHB.
 *               : Fixed a problem in Ethernet MII and GMII in which 64 byte packets were
 *               : not being received properly.
 *               :
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.35  2005/07/11 19:54:03  hoglund_w
 *               : Updates for carbon, removing unused code and debug messages
 *               :
 *               : Revision 1.34  2004/10/21 16:21:32  zippelius
 *               : Moved some functions. Renamed some functions
 *               :
 *               : Revision 1.33  2004/09/27 19:02:43  zippelius
 *               : Re-enabled test2 added support for EP buffer map.
 *               :
 *               : Revision 1.32  2004/09/24 22:51:05  zippelius
 *               : Changed how EP buffers are being managed
 *               :
 *               : Revision 1.31  2004/09/22 17:35:56  zippelius
 *               : the same test1 can now run with either OpenCore or ARC core
 *               :
 *               : Revision 1.30  2004/08/30 20:21:33  zippelius
 *               : changed all wishbone reference to amba references
 *               :
 *               : Revision 1.29  2004/08/27 21:31:40  zippelius
 *               : Updated tests (fixes, expanded, added self-checking). Misc clean-ups
 *               :
 *               : Revision 1.28  2004/08/23 20:15:57  zippelius
 *               : Fixed Control transfers and misc other clean-ups
 *               :
 *               : Revision 1.27  2004/08/18 20:29:38  zippelius
 *               : Cleaned-up tests. Moved some code into test_support
 *               :
 *               : Revision 1.26  2004/08/13 21:43:47  zippelius
 *               : snapshot check-in
 *               :
 *               : Revision 1.25  2004/08/13 18:40:35  zippelius
 *               : Removed bogus Pipe 0; Moved toggle tracking from Pipe into EP; Fixed 4 byte alloc, when data size==0;Misc fixes
 *               :
 *               : Revision 1.24  2004/08/12 14:11:19  zippelius
 *               : Numerous fixes and changes around DATA toggle
 *               :
 *               : Revision 1.23  2004/08/11 01:59:19  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.22  2004/08/09 16:58:57  fredieu
 *               : add 2 new routines
 *               :
 *               : Revision 1.21  2004/08/06 19:21:07  zippelius
 *               : Updated test header with description
 *               :
 *               : Revision 1.20  2004/08/06 15:44:09  zippelius
 *               : Some more updates and fixes for test 23
 *               :
 *               : Revision 1.19  2004/08/06 13:51:32  zippelius
 *               : Updated/fixed test23, fixed numerous issues around User Reset
 *               :
 *               : Revision 1.18  2004/08/05 02:15:10  fredieu
 *               : Add other errors
 *               :
 *               : Revision 1.17  2004/08/02 18:53:24  zippelius
 *               : Fixed a number of bugs regarding bus time estimation
 *               :
 *               : Revision 1.16  2004/08/02 13:40:44  fredieu
 *               : Fixes to destroy pipes
 *               :
 *               : Revision 1.15  2004/07/30 21:57:13  fredieu
 *               : clean up for error injection
 *               :
 *               : Revision 1.14  2004/07/27 19:04:25  fredieu
 *               : panic on too many NAKs
 *               :
 *               : Revision 1.13  2004/07/24 03:41:25  fredieu
 *               : Support to reduce the number of PINGs
 *               :
 *               : Revision 1.12  2004/07/21 23:06:04  fredieu
 *               : Mask aborts based upon the test.
 *               :
 *               : Revision 1.11  2004/07/21 19:46:38  fredieu
 *               : Panic on aborts.
 *               :
 *               : Revision 1.10  2004/06/29 01:20:19  fredieu
 *               : Use errors and panics in the appropriate places.
 *               :
 *               : Revision 1.9  2004/06/25 18:19:24  fredieu
 *               : Fix for usb_test32 to allow for intended errors.
 *               :
 *               : Revision 1.8  2004/06/24 18:14:21  fredieu
 *               : Remove the back to back free.
 *               :
 *               : Revision 1.7  2004/06/18 22:21:41  fredieu
 *               : Change some fatal warnings to PANIC!
 *               :
 *               : Revision 1.6  2004/04/26 21:54:25  jafri
 *               : modified english in verilog and c code and added usb_test1a to test table
 *               :
 *               : Revision 1.5  2004/04/26 21:34:29  zippelius
 *               : Code clean-up regarding IRP state and misc other
 *               :
 *               : Revision 1.4  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.3  2004/04/19 18:38:57  jafri
 *               : fixed english in comments
 *               :
 *               : Revision 1.2  2004/03/26 14:59:22  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.5  2003/12/05 07:32:46  serlina
 *               : ...
 *               :
 *               : Revision 2.4  2003/12/04 10:19:06  adam
 *               : Debug testcases
 *               :
 *               : Revision 2.3  2003/12/03 02:14:01  adam
 *               : Edit testcase
 *               :
 *               : Revision 2.2  2003/11/25 09:41:33  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.1  2003/09/27 12:10:53  serlina
 *               : update pipe
 *               :
 *               : Revision 2.0  2003/09/27 06:33:45  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:18  adam
 *               : updated file directory
 *               :
 *               : Revision 1.25  2003/09/26 11:02:18  adam
 *               : Add testcases
 *               :
 *               : Revision 1.24  2003/09/25 06:24:01  serlina
 *               : add update
 *               :
 *               : Revision 1.23  2003/09/23 12:07:58  serlina
 *               : change standard cfg
 *               :
 *               : Revision 1.22  2003/09/23 09:19:11  adam
 *               : Add amba interface
 *               :
 *               : Revision 1.21  2003/09/22 01:53:26  serlina
 *               : add the dut_model = user_cfg
 *               :
 *               : Revision 1.20  2003/09/19 07:22:22  serlina
 *               : add carbonXUsbGetDefPipe() and chang pipe create function
 *               :
 *               : Revision 1.19  2003/09/18 06:27:47  serlina
 *               : add the uframe_sched parameter
 *               :
 *               : Revision 1.18  2003/09/12 14:02:02  serlina
 *               : updated for signoff review
 *               :
 *               : Revision 1.17  2003/09/12 03:45:54  serlina
 *               : delete divider
 *               :
 *               : Revision 1.16  2003/09/11 08:08:41  issac
 *               : major update after code review
 *               :
 *               : Revision 1.15  2003/09/08 14:20:40  serlina
 *               : change parameter
 *               :
 *               : Revision 1.14  2003/09/08 13:59:16  serlina
 *               : common
 *               :
 *               : Revision 1.13  2003/09/08 13:27:41  serlina
 *               : reset error
 *               :
 *               : Revision 1.12  2003/09/08 08:04:54  serlina
 *               : change link
 *               :
 *               : Revision 1.10  2003/08/29 09:34:51  serlina
 *               : change iso_cnt place
 *               :
 *               : Revision 1.9  2003/08/28 07:17:28  serlina
 *               : change report error
 *               :
 *               : Revision 1.8  2003/08/28 02:17:04  serlina
 *               : change the 0 device
 *               :
 *               : Revision 1.7  2003/08/27 15:00:56  serlina
 *               : updated Do Enum series of functions, and dev prof data structure
 *               :
 ***************************************************************************/
 

#include "USB_support.h"

//%FS===========================================================================
// USB_PIPE_DescrReset
//
// Description:
//   This function resets the specific pipe descriptor.
//   1. Reset endpt_descr.
//   2. Reset a_irp_c and a_irp_uc to empty.
//   3. Reset sys_record to empty, pipe state to USB_PIPE_STAT_NEED_SETUP
//
// Parameters: 
//   u_int32  pipe_id  I
//     It points to the pipe being reset.
//
// Return: 
//   void
//%FE===========================================================================
void USB_PIPE_DescrReset(USB_PipeDescr_t* p_pipe){
   
  //------------- Step 1 --------------------  

  MSG_Printf( "<%.10lld> ++++++++ USB_PIPE_DescrReset \n", PLAT_GetSimTime() );

  p_pipe->p_endpt_descr = NULL;

  //------------- Step 2 --------------------  
  p_pipe->a_irp_c.count           = 0;
  p_pipe->a_irp_c.p_tail              = NULL;      
  p_pipe->a_irp_c.p_head              = NULL;
   
  p_pipe->a_irp_uc.count          = 0;
  p_pipe->a_irp_uc.p_tail             = NULL;
  p_pipe->a_irp_uc.p_head             = NULL;
 
  //------------- Step 3 --------------------  
  p_pipe->sys_record.response_pid     = USB_PID_RESERVED;
  p_pipe->sys_record.nak_count        = 0;
  p_pipe->sys_record.auto_trigger     = 0;
  p_pipe->sys_record.err_cnt          = 0;
  p_pipe->sys_record.duration_est     = 0;
  p_pipe->sys_record.iso_cnt          = 0;
  p_pipe->sys_record.iso_trans_cnt    = 0;
  p_pipe->sys_record.iso_data_cnt     = 0;
  p_pipe->sys_record.iso_fb_cnt       = 0;
  p_pipe->sys_record.uframe_sched     = 0;
  p_pipe->sys_record.prev_trans_state = USB_DO_NEXT_CMD;
  p_pipe->sys_record.pipe_state       = USB_PIPE_STAT_NEED_SETUP;

  
}

//%FS===========================================================================
// USB_PIPE_GetNewDescr
//
// Description:
//   This function allocates a new pipe descriptor.
//   1. Extend Pipe Hash List if it is FULL.
//   2. Allocate new USB_PipeDescr_t X, reset X, attach X to the tail of the
//      the arrayed pointer struct, and extend arrayed pointer struct count by 1.
//   3. Since each new pipe requires new SAR buffer, grow the SAR buffer here.
//   4. returns X.
//
// Parameters:
//   void
//   
// Return:
//   USB_PipeDescr_t*
//     If OK, the new pipe descriptor will be returned.
//     If no memory, the code will exit.
//%FE===========================================================================
USB_PipeDescr_t* USB_PIPE_GetNewDescr (){
  USB_PipeAPStatic_t*    p_pipe_mgr;
  
  USB_PipeDescr_t** pp_pipe_cur;
  USB_PipeDescr_t** pp_head_new;
  
  USB_PipeDescr_t*  p_pipe_new;
  
  u_int32          i;

  MSG_Printf( "<%.10lld> ++++++++  USB_PIPE_GetNewDescr \n", PLAT_GetSimTime() );
  
  //------------- Step 1 --------------------   
  p_pipe_mgr = &( ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_pipe_mgr);
  
  if( USB_PIPE_PA_IsFull(p_pipe_mgr) ) {
    p_pipe_mgr->size += USB_PIPE_GROW_SIZE;
    pp_head_new = (USB_PipeDescr_t**)tbp_realloc(p_pipe_mgr->ppa_pipe_head,
						 (sizeof(USB_PipeDescr_t*)*(p_pipe_mgr->size - USB_PIPE_GROW_SIZE)),
						 (sizeof(USB_PipeDescr_t*)*p_pipe_mgr->size) );
   
    if ( pp_head_new == NULL ) {
      MSG_Panic("USB_PIPE_GetNewDescr(): Out of memory!\n");
    } 
    p_pipe_mgr->ppa_pipe_head = pp_head_new;

    // Reset the unused portion to NULL for safety.
    for(i=p_pipe_mgr->count;i<p_pipe_mgr->size;i++){
      *(pp_head_new + i) = NULL;
    }
  }
  
  //------------- Step 2 -------------------- 
  pp_pipe_cur = USB_PIPE_PA_Cur(p_pipe_mgr);

  p_pipe_new = (USB_PipeDescr_t*)calloc(1, sizeof(USB_PipeDescr_t));
    
  if (p_pipe_new == NULL) {
    MSG_Panic("USB_PIPE_GetNewDescr(): Out of memory!\n");
  }
  
  p_pipe_new->pipe_id = p_pipe_mgr->count;
  USB_PIPE_DescrReset(p_pipe_new);
  
  *pp_pipe_cur = p_pipe_new;
  p_pipe_mgr->count ++;
  
  //------------- Step 3 -------------------- 
  USB_SAR_ReallocAry();
  
  //------------- Step 4 -------------------- 
  return(p_pipe_new);
}


//%FS===========================================================================
// USB_PIPE_PA_IsFull
//
// Description:
//  This function checks if the pipe pointer array struct is full or not.
//
// Parameters:
//   USB_PipeAPStatic_t*  p_pipe_mgr  I
//    It points to the pipe pointer array struct.
//
// Return:
//   If TRUE, the pointer array struct is full.
//   If FALSE, the pointer array struct has space.
//%FE===========================================================================
BOOL USB_PIPE_PA_IsFull(USB_PipeAPStatic_t*  p_pipe_mgr) {
  return (p_pipe_mgr->size <= p_pipe_mgr->count);
}


//%FS===========================================================================
// USB_PIPE_PA_Cur
//
// Description:
//  This function returns the tail of the pipe pointer array struct.
//
// Parameters:
//   USB_PipeAPStatic_t*  p_pipe_mgr  I
//    It points to the pipe pointer array struct.
//
// Return:
//   USB_PipeDescr_t**
//     The tail pipe pointer will be returned.
//%FE===========================================================================
USB_PipeDescr_t** USB_PIPE_PA_Cur(USB_PipeAPStatic_t*  p_pipe_mgr) {
  return ( p_pipe_mgr->ppa_pipe_head + p_pipe_mgr->count );
}



//%FS===========================================================================
// USB_PIPE_IrpLinkGetNew
//
// Description:
//   This function allocates memory for a single 'USB_IrpLink_t'.
//
// Parameters:
//   void
//
// Return:
//   USB_IrpLink_t
//     The new IRP Link structure will be returned.
//%FE===========================================================================
USB_IrpLink_t* USB_PIPE_IrpLinkGetNew(){
  USB_IrpLink_t*  p_new;

  p_new = (USB_IrpLink_t*)calloc(1, sizeof(USB_IrpLink_t));

  if(p_new == NULL){
    MSG_Panic("USB_PIPE_IrpLinkGetNew(): Out of memory!\n");
  }
  return(p_new);
}





//%FS===========================================================================
// USB_PIPE_IrpLinkFree
//
// Description:
//   This function frees the memory of the linked list in the specific pipe.
//
// Parameters:
//   USB_PipeDescr_t* p_pipe   I
//     It points to the specific pipe.
//
// Return:
//   void
//%FE===========================================================================
void USB_PIPE_IrpLinkFree(USB_PipeDescr_t* p_pipe){
  USB_IrpLink_t*  p_cur;
  
  if(p_pipe != NULL){
    p_cur = p_pipe->a_irp_uc.p_head;
    
    while (p_cur != NULL){  
      
      if(p_pipe->a_irp_uc.p_head == p_pipe->a_irp_uc.p_tail){
        p_pipe->a_irp_uc.p_head = p_cur->p_next;
        p_pipe->a_irp_uc.p_tail = p_cur->p_next;
      }
      else {
	p_pipe->a_irp_uc.p_head = p_cur->p_next;
      }
      
      tbp_free(p_cur);
      p_cur = p_pipe->a_irp_uc.p_head;
    }
    
    p_cur = p_pipe->a_irp_c.p_head;
    
    while (p_cur != NULL){  
      
      if(p_pipe->a_irp_c.p_head == p_pipe->a_irp_c.p_tail){
        p_pipe->a_irp_c.p_head = p_cur->p_next;
        p_pipe->a_irp_c.p_tail = p_cur->p_next;
      }
      else p_pipe->a_irp_c.p_head = p_cur->p_next;
      
      tbp_free(p_cur);
      p_cur = p_pipe->a_irp_c.p_head;
    } // end while (p_cur != NULL)
  }
  else {
    MSG_Panic("USB_PIPE_IrpLinkFree(): Input pointer cannot be NULL.\n");
  }
}



//%FS===========================================================================
// USB_PIPE_Update
//
// Description:
//   This function updates a pipe upon a successful IRP transaction.
//   1. If there was no IRP in the IN FIFO, set the pipe state to IDLE.
//   2. If Iso transfer IN transaction number is less than USB_ISO_IN_TRANS_NUM,
//      continue with same IRP; Else, continue with the next IRP.
//   3. The pipe halted: If a control pipe, the current IRP will be aborted.
//      If not a control pipe, all IRP will be aborted.
//   4. Implement the "auto_built" feature as described in the file header:
//      "Auto built feature".
//      
// Parameters: 
//   USB_PipeDescr_t*   p_pipe  I
//     It points to the specific pipe being updated.
//
// Return:
//   void
//%FE===========================================================================
void USB_PIPE_Update(USB_PipeDescr_t*   p_pipe){
  USB_HostCtrlTop_t* p_top;
  USB_PipeDescr_t*   p_trigger_pipe;
  USB_IrpLink_t*     p_irp_link;
  u_int32            trigger_pipe_id;
  int                trigger_irp_id;
  u_int32            i;
  BOOL               is_null = TRUE;
  
	p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
 
  if(p_pipe != NULL) is_null = FALSE;
  
  //------------- step 1 --------------------
  if((!is_null)&&(p_pipe->a_irp_uc.count == 0)){
    p_pipe->sys_record.pipe_state = USB_PIPE_STAT_IDLE;
  }
  
  //---------------- step 2 ----------------------
  if((!is_null) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO) ){
    if(p_pipe->p_endpt_descr->direction == USB_DIR_IN){
      if(p_pipe->sys_record.iso_trans_cnt < USB_ISO_IN_TRANS_NUM){
        p_pipe->sys_record.iso_trans_cnt ++; 
      }
      else{
        p_pipe->a_irp_uc.p_head->p_irp->status = USB_IRP_STAT_COMPL;
        USB_PIPE_RetireCompletedIRPs(p_pipe);
        if(p_pipe != NULL){
          p_pipe->sys_record.iso_trans_cnt = 0;
          is_null = FALSE;
        }
        else is_null = TRUE;
      }
    }  
  }  
 
  //---------------- step 3 ----------------------
  if( (!is_null) && (p_pipe->sys_record.pipe_state == USB_PIPE_STAT_HALTED) ){
    if(p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL){
      p_pipe->a_irp_uc.p_head->p_irp->status = USB_IRP_STAT_ABORT;
      if(!p_top->ErrorMsgDisabled.transaction_aborted) MSG_Error("IRP is aborted 2\n");
      USB_PIPE_RetireCompletedIRPs(p_pipe);
    }
    else if(p_pipe->p_endpt_descr->endpt_type != USB_EPTYPE_ISO){

      while(p_pipe->a_irp_uc.count != 0 ){
        p_irp_link = p_pipe->a_irp_uc.p_head;

        p_irp_link->p_irp->status = USB_IRP_STAT_ABORT;
	if(!p_top->ErrorMsgDisabled.transaction_aborted) MSG_Error("IRP is aborted 1\n");
        USB_PIPE_RetireCompletedIRPs(p_pipe);
        
        if(p_pipe == NULL) break;
        if(p_pipe->a_irp_uc.p_head == NULL) break;
      }
    }
  }
  else if(!is_null){
    
    //------------- step 4 --------------------   
    if(p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL){
    
      for(i=0;i<p_top->sys_pipe_mgr.count;i++){
        p_trigger_pipe = *(p_top->sys_pipe_mgr.ppa_pipe_head + i);
        if(p_trigger_pipe == NULL) continue;
     
        if( (p_trigger_pipe->p_endpt_descr->is_auto)&&(p_trigger_pipe->sys_record.auto_trigger != 0) ){
          trigger_pipe_id = (p_trigger_pipe->sys_record.auto_trigger) >> 16;
          trigger_irp_id  = (p_trigger_pipe->sys_record.auto_trigger) & 0x00FF;
      
          if( (trigger_pipe_id == p_pipe->pipe_id)&&
            (trigger_irp_id == p_pipe->a_irp_c.p_tail->p_irp->irp_id) ){
            p_trigger_pipe->p_endpt_descr->is_auto = FALSE;
            p_trigger_pipe->sys_record.pipe_state = USB_PIPE_STAT_READY;
            MSG_Printf("Pipe #%d has been built.\n",i);
            break;
          }// pipe id and irp id same
        
        }
      }// end for 
    
    }
  }
}

//%FS===========================================================================
// USB_PIPE_RetireCompletedIRPs
//
// Description:
//   This function updates the specific pipe when an IRP in the pipe 
//   has been completed or the IRP is aborted.
//   1. Move the IRP
//   2. Analyze the IRP
//   3. Execute Callback fuction
//
// Parameter:
//   USB_PipeDescr_t*     p_pipe   I
//     It points to an IRP list of the specific pipe.
//
// Return: 
//   void
//%FE===========================================================================
void USB_PIPE_RetireCompletedIRPs (USB_PipeDescr_t*  p_pipe){
  USB_Irp_t*  p_irp = NULL;
  USB_HostCtrlTop_t*  p_top;
  
  p_top = XPORT_GetMyProjectData();

  if(p_pipe != NULL){
    if(p_pipe->a_irp_uc.p_head != NULL){
      p_irp = p_pipe->a_irp_uc.p_head->p_irp;
    }
  }
  else MSG_Panic("USB_PIPE_RetireCompletedIRPs(): Input pointer cannot be NULL.\n");
 
  
  if(p_irp != NULL){
    //--------------------- step 1 ------------------------
    //move the completed IRP from In FIFO to Out FIFO
    carbonXUsbPipePopInIRP(p_pipe, p_irp);
    carbonXUsbPipePushOutIRP(p_pipe, p_irp);
    
    //--------------------- step 2 ------------------------
    // Process irp for reporting and analysis
    if( (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) && (p_irp->status == USB_IRP_STAT_COMPL) ){
      USB_IRP_StdReqAnalysis(p_irp);
    }
    else if(p_irp->status == USB_IRP_STAT_COMPL){
      MSG_Printf("The IRP #%d has completed.\n",p_irp->irp_id);
    }
    else if(p_irp->status == USB_IRP_STAT_ABORT){
      if(!p_top->ErrorMsgDisabled.transaction_aborted)
	MSG_Error("The IRP #%d has aborted.\n",p_irp->irp_id);
    }

    //--------------------- step 3 ------------------------
#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> Completed IRP #%d ", PLAT_GetSimTime(), p_irp->irp_id );
    if ( p_irp->p_rcv_data != NULL ) {
      MSG_Printf( " %d bytes were received.", p_irp->p_rcv_data->size );
    }
    MSG_Printf( " %d bytes were sent.\n", p_irp->sent_cnt ); 
#endif

    //Callback fuction
    if ( p_top->pfEndOfExecIrp ) {
      // callback function exists.
      // -> Call it.
      p_top->pfEndOfExecIrp( p_top, p_irp, p_pipe );    
    }    
  }
}


//%FS===========================================================================
// USB_PIPE_MsgHeader
//
// Description:
//   This function prints the pipe message header.
//
// Parameter: 
//  void
//
// Return: 
//   void 
//%FE===========================================================================
void USB_PIPE_MsgHeader(){
  MSG_Printf("\n");
  //  MSG_LibDebug("usb",2,"\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
  MSG_Printf("+ Report Pipe Information\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
}




//%FS===========================================================================
// USB_PIPE_MsgTotal
//
// Description:
//   This function reports the pipe statistic message.
//   1. Calculate the status of all the pipes. Increments status count for each pipe.
//   2. Printf above message.
//
// Parameter: 
//   void
//
// Return: 
//   void 
//%FE===========================================================================
void USB_PIPE_MsgTotal(){
  USB_PipeDescr_t* p_pipe;
  u_int32          i;
  u_int32          pipe_cnt  = 1;
  u_int32          busy_cnt  = 1;
  u_int32          setup_cnt = 0;
  u_int32          halt_cnt  = 0;
  u_int32          idle_cnt  = 0;
  u_int32          fail_cnt  = 0;  
  USB_HostCtrlTop_t*       p_top;

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  
  //----------------- step 1 -------------------------
  for (i=0;i<p_top->sys_pipe_mgr.count;i++){
  	
    p_pipe = carbonXUsbGetPipeDescr(i);
    
    if(p_pipe == NULL) continue;
    
    if(p_pipe->pipe_id != 0){
      pipe_cnt += 1;
      switch(p_pipe->sys_record.pipe_state){
        case USB_PIPE_STAT_READY_NOTIME:
        case USB_PIPE_STAT_READY:{
          busy_cnt += 1;
          break;
        }
        case USB_PIPE_STAT_HALTED:{
          halt_cnt += 1;
          break;
        }
        case USB_PIPE_STAT_IDLE:{
          idle_cnt += 1;
          break;
        }
        case USB_PIPE_STAT_NEED_SETUP:{
          setup_cnt += 1;
          break;
        }
        case USB_PIPE_STAT_SETUP_FAIL:{
          fail_cnt += 1;
          break;
        }
      }//end of switch
    }
  }
  
  //----------------- step 2 ------------------------- 
  MSG_Printf("+ There are %d pipes \n",pipe_cnt);
  MSG_Printf("+  (Including to Default Control Pipe)\n");
  MSG_Printf("+ %d pipes have been setup\n",busy_cnt);
  MSG_Printf("+ %d pipes waiting for setup\n",setup_cnt);
  MSG_Printf("+ %d pipes setup failed\n",fail_cnt);
  MSG_Printf("+ %d pipes are idle\n",idle_cnt);
  MSG_Printf("+ %d pipes are halted\n",halt_cnt);
  
  MSG_Printf("+----------------------------------------------------------------------\n");
}

//%FS===========================================================================
// USB_PIPE_Msg
//
// Description:
//   This function reports a single pipe message.
//   1. report the pipe ID
//   2. report the device ID included in this pipe
//   3. report if the pipe is default control pipe or not, and report the IRP List
//      message in this pipe.
//   4. report the pipe type
//   5. report the pipe direction and 
//   6. report the pipe supported max packet size and corresponding endpoint number.
//   7. report the pipe current status
//   8. If period pipe report the interval and the number of transactions per 
//      (micro)frame.
//   9. report the sync type and the usage type
//   10. report the current transaction PID in this pipe.
//
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the pipe that will be reported.
//
// Return: void 
//%FE===========================================================================
void USB_PIPE_Msg(USB_PipeDescr_t*  p_pipe){
 
  USB_DevProfile_t* p_dev;

  p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
  
  //----------------- step 1 --------------------- 
  MSG_Printf("+   Information for pipe #%d :\n",p_pipe->pipe_id);
  
  //----------------- step 2 ---------------------
  if(p_dev != NULL){
    MSG_Printf("+     The pipe is in device #%d \n",p_dev->dev_id);
  }
  else MSG_Printf("+     The pipe is invalid.\n");
  
  //----------------- step 3 ---------------------
  if (p_pipe->p_endpt_descr->endpt_num == 0){
    MSG_Printf("+     The pipe is DEFAULT CONTROL PIPE.\n");
  }
 
  MSG_Printf("+     Total number of IRPs in IN FIFO  : %d\n",p_pipe->a_irp_uc.count);
  MSG_Printf("+     Total number of IRPs in OUT FIFO : %d\n",p_pipe->a_irp_c.count);
  MSG_Printf("+\n");
  MSG_Printf("+     Description:\n");
  
  //----------------- step 4 ---------------------
  //pipe type
  MSG_Printf("+       Pipe type               : %s\n",
	     carbonXUsbCvtEndPtTypeToString( p_pipe->p_endpt_descr->endpt_type ) );
  
  //----------------- step 5 ---------------------
  //pipe direction
  MSG_Printf("+       Pipe direction          : %s\n",
	     carbonXUsbCvtDirTypeToString( p_pipe->p_endpt_descr->direction ) );

  //----------------- step 6 ---------------------
  //maximum packet size 
  MSG_Printf("+       Max packet size         : %d\n",p_pipe->p_endpt_descr->max_packet_size);
  //endpoint number
  MSG_Printf("+       Endpoint number         : %d\n",p_pipe->p_endpt_descr->endpt_num);
  
  //----------------- step 7 ---------------------
  //pipe status
  switch(p_pipe->sys_record.pipe_state){
    
    case USB_PIPE_STAT_READY_NOTIME:
      MSG_Printf("+       Pipe status             : Ready, but no time avail.\n");
      break;

    case USB_PIPE_STAT_READY:
      MSG_Printf("+       Pipe status             : Ready\n");
      break;
    
    case USB_PIPE_STAT_HALTED:
      MSG_Printf("+       Pipe status             : Halted\n");
      break;
    
    case USB_PIPE_STAT_IDLE:
      MSG_Printf("+       Pipe status             : Idle\n");
      break;
    
    case USB_PIPE_STAT_NEED_SETUP:
      MSG_Printf("+       Pipe status             : Need setup\n");
      break;
    
    case USB_PIPE_STAT_SETUP_FAIL:
      MSG_Printf("+       Pipe status             : Setup failed\n");
      break;
    
  }
  
  //----------------- step 8 ---------------------
  //isochronous and interrupt
  if(p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO){
  	
    MSG_Printf("+       ISO interval            : %d\n",p_pipe->p_endpt_descr->interval);
    
    switch(p_pipe->p_endpt_descr->sync_type){
      case USB_SYNC_TYPE_ASYNC:{
        MSG_Printf("+       Synchronization type    : Asynchronous\n");
        break;
      }
      case USB_SYNC_TYPE_ADAPT:{
        MSG_Printf("+       Synchronization type    : Adaptive\n");
        break;
      }
      case USB_SYNC_TYPE_SYNC:{
        MSG_Printf("+       Synchronization type    : Synchronous\n");
        break;
      }
      default:break;
    }//end of sync type
    
    switch (p_pipe->p_endpt_descr->usage_type){
      case USB_USG_TYPE_DATA:{
        MSG_Printf("+       Usage type              : Data endpoint\n");
        break;
      }
      case USB_USG_TYPE_FD:{
        MSG_Printf("+       Usage type              : Feedback endpoint\n");
        break;
      }
      case USB_USG_TYPE_IMPLICIT:{
        MSG_Printf("+       Usage type              : Implicit feedback endpoint\n");
        break;
      }
      default: break;
    }//end of usage type
    
    MSG_Printf("+       Transactions number     : %d\n",(p_pipe->p_endpt_descr->hs_trans_num + 1) );
                   
  }
  else{
     if(p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_INTR){
       MSG_Printf("+       Interrupt interval      : %d\n",p_pipe->p_endpt_descr->interval);
       MSG_Printf("+       Pipe type               : Interrupt\n");
       MSG_Printf("+       Transactions number     : %d\n",(p_pipe->p_endpt_descr->hs_trans_num + 1) );
    }
    //----------------- step 9 ---------------------
    MSG_Printf("+       Synchronization type    : No Synchronizatin\n");
    MSG_Printf("+       Usage type              : Data endpoint\n");
  }

  //----------------- step 10 ---------------------
  // current transaction pid
  MSG_Printf("+       Current transaction PID : %s\n", carbonXUsbCvtPidToString( p_pipe->p_endpt_descr->pid ) );
      
  MSG_Printf("+----------------------------------------------------------------------\n");
 
}


//%FS===========================================================================
// USB_PIPE_MsgEnd
//
// Description:
//   This function prints the pipe message end.
//
// Parameter:
//   void
//
// Return: 
//   void 
//%FE===========================================================================
void USB_PIPE_MsgEnd(){
  MSG_Printf("+ End of pipe report\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
}


//%FS===========================================================================
// carbonXUsbGetPipeDescr
//
// Description:
//   This function gets the pipe pointer by the pipe ID.
//
// Parameters: 
//   u_int32 pipe_id  I
//     It points to the pipe being requested.
//
// Return:
//   USB_PipeDescr_t*
//     If OK, the requested pipe pointer will be returned.
//     If failing, NULL will be returned, the warning message will be reported.
//%FE===========================================================================
USB_PipeDescr_t*  carbonXUsbGetPipeDescr(u_int32 pipe_id){
  USB_PipeDescr_t*   p_pipe;
  USB_HostCtrlTop_t*         p_top;
  
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  
  if(pipe_id < p_top->sys_pipe_mgr.count){
    p_pipe = *(p_top->sys_pipe_mgr.ppa_pipe_head + pipe_id);
    return(p_pipe);
  }
  else {
    MSG_Error("carbonXUsbGetPipeDescr(): Request pipe ID error.\n");
    return(NULL);
  }
}



//%FS===========================================================================
// USB_PIPE_PA_Free
//
// Description:
//   This function frees the pipe pointer array struct. All pipes in this 
//   pointer array struct will be lost.
//
// Parameters: 
//   USB_PipeAPStatic_t*  p_pipe_mgr  I
//     It points to the pipe pointer array struct.
//
// Return:
//   void
//%FE===========================================================================
void USB_PIPE_PA_Free (USB_PipeAPStatic_t*  p_pipe_mgr){
  u_int32          i;
  USB_PipeDescr_t* p_pipe;
  
  if ( p_pipe_mgr->ppa_pipe_head != NULL ){
    
    for(i=0;i<p_pipe_mgr->count;i++){
      
      p_pipe = *(p_pipe_mgr->ppa_pipe_head + i);
      
      if (p_pipe != NULL){
      	USB_PIPE_IrpLinkFree(p_pipe);
      	tbp_free(p_pipe);
      
				//      	free(p_pipe);
      }       
    }
    tbp_free (p_pipe_mgr->ppa_pipe_head);
  }
}



//%FS===========================================================================
// USB_PIPE_PA_Reset
//
// Description:
//   This function resets the pipe pointer array struct. It will free all pipes and set 
//   the value to a default value.
//
// Parameters: 
//   USB_PipeAPStatic_t*  p_pipe_mgr  I
//     It points to the pipe pointer array struct.
//
// Return:
//   void
//
// Note:
//   If reset the all pipe information will be lost.
//%FE===========================================================================
void USB_PIPE_PA_Reset(USB_PipeAPStatic_t*  p_pipe_mgr){
  USB_PIPE_PA_Free( p_pipe_mgr );
   
  p_pipe_mgr->size          = 0;
  p_pipe_mgr->count     = 0;
  p_pipe_mgr->ppa_pipe_head = NULL;
}


//%FS===========================================================================
// USB_PIPE_PA_Init
//
// Description:
//   This function initializes the pipe pointer array struct, setting the value to 
//   default value.
//
// Parameters: 
//   USB_PipeAPStatic_t*  p_pipe_mgr  I
//     It points to the pipe pointer array struct.
//
// Return:
//   void
//
//%FE===========================================================================
void USB_PIPE_PA_Init(USB_PipeAPStatic_t*  p_pipe_mgr){
  
  p_pipe_mgr->size          = 0;
  p_pipe_mgr->count         = 0;
  p_pipe_mgr->ppa_pipe_head = NULL;
  
  return;
}


//%FS===========================================================================
// carbonXUsbPipeCreate
//
// Description:
//   This function creates a new IRP and puts it into the IRP pointer array struct.
//   1. Check if the Device exists or not. If it exists, create a new pipe and 
//      set the value.
//   2. Check if the pipe Semantic and the bandwidth is enough or not.
//      If Semantic error or the bandwidth is not enough, the pipe state will be 
//      set to USB_PIPE_STAT_SETUP_FAIL;
//   3. Check if the endpoint number is zero or not. If zero, the pipe status will 
//      be set to USB_PIPE_STAT_IDLE, the pipe can be used.
//    3.1 Check if the pipe is auto built. If TRUE, do next step. Please 
//        refer to the comment in the file header on "Auto built feature".
//     3.1.1 Check if the endpoint has been set in the user cofiguration and the DUT.
//           If user is configured but dut is not configured, use carbonXUsbSetCfg() to
//           set the configuration value to dut by Default Control pipe of this 
//           device, and record the pipe ID and the IRP ID.
//     3.1.2 If the dut has been set, the pipe status will be set to USB_PIPE_STAT_IDLE.
//     3.1.3 If the dut has not been set and user configuration has not, it cannot
//           send the standard request to the DUT automatically, the pipe status 
//           will be set to USB_PIPE_STAT_SETUP_FAIL.
//   4. Return the pipe;
//      
//
// Parameters: 
//   USB_EndPtDescr_t* p_endpt_descr  I 
//     It points to the endpoint descriptor for the new pipe.
//   
// Return:
//   USB_PipeDescr_t*
//     If OK, the new IRP pointer will be returned.
//     If error, NULL will be returned, the warning message will be reported.
//%FE===========================================================================
USB_PipeDescr_t* carbonXUsbPipeCreate(USB_EndPtDescr_t* p_endpt_descr){
  USB_PipeDescr_t*  p_pipe  = NULL;
  USB_PipeDescr_t*  p_def   = NULL;
  USB_DevProfile_t* p_dev;
  BOOL              device_valid = FALSE;
  BOOL              has_set_dut;
  BOOL              has_set_user;
  u_int32           cur_frame  = 0;

  if( p_endpt_descr != NULL ){
   
    //------------------- step 1 ----------------------
    if( !USB_CHK_DevExist( p_endpt_descr->dev_id ) ){
      MSG_Panic( "The pipe #%d can not be built. The device #%d does not exist.\n",
		 p_pipe->pipe_id, p_endpt_descr->dev_id );
    }
    else{
      cur_frame = ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_sched.uframe_count;

      p_pipe = USB_PIPE_GetNewDescr();
      
      p_pipe->p_endpt_descr = p_endpt_descr;
      
      p_pipe->sys_record.uframe_sched     = cur_frame;
   
      device_valid = TRUE;
      
      if( (p_endpt_descr->endpt_type == USB_EPTYPE_BULK)||(p_endpt_descr->endpt_type == USB_EPTYPE_INTR) ){
        p_pipe->p_endpt_descr->needs_toggle = TRUE;
      }
    }
  }
  else {
    MSG_Panic("carbonXUsbPipeCreate(): Input pointer can not be NULL.\n");
  }
    
  if(device_valid){
    //------------------- step 2 ----------------------
    if( (!USB_CHK_PipeProperties(p_pipe, TRUE)) &&
      (USB_CHK_BandWidth(p_pipe,p_endpt_descr->max_packet_size)) ){
      
      //------------------- step 3.1 ----------------------
      if( (p_endpt_descr->endpt_num != 0)&&(p_endpt_descr->is_auto) ){
        
        //------------------- step 3.1.1 ----------------------
        has_set_user = USB_CHK_UserIsCfg(p_endpt_descr);
        has_set_dut  = USB_CHK_DutIsCfg(p_endpt_descr);
        
        if(has_set_user && (!has_set_dut) ){
          p_dev = carbonXUsbGetDevProf(p_endpt_descr->dev_id);
          
          p_def = carbonXUsbGetDefPipe(p_endpt_descr->dev_id);
    
          if(p_def != NULL){
            carbonXUsbSetCfg(p_def,p_dev->user_cfg.stdcfg[0].b_cfg_val);

            p_dev->dut_model = p_dev->user_cfg;
            p_pipe->sys_record.auto_trigger += p_def->pipe_id << 16;
            p_pipe->sys_record.auto_trigger += p_def->a_irp_uc.p_tail->p_irp->irp_id;
          }
        }
        //------------------- step 3.1.2 ----------------------
        else if(has_set_dut){
          p_pipe->sys_record.pipe_state = USB_PIPE_STAT_IDLE;
        }
        //------------------- step 3.1.3 ----------------------
        else if(!has_set_user) p_pipe->sys_record.pipe_state = USB_PIPE_STAT_SETUP_FAIL;
        
      }
      else {
        if ( p_endpt_descr->endpt_num == 0 ){
           if(p_endpt_descr->is_auto){
              MSG_Error("carbonXUsbPipeCreate(): There is never an endpoint descriptor for endpoint zero.\n");
              p_endpt_descr->is_auto = FALSE;
          }
        }
        p_pipe->sys_record.pipe_state       = USB_PIPE_STAT_IDLE;
      }
    }
    else {
      p_pipe->sys_record.pipe_state       = USB_PIPE_STAT_SETUP_FAIL;
      if(p_endpt_descr->allow_errors == 0)
	MSG_Panic("carbonXUsbPipeCreate(): Create Pipe has FAILED because not the required USB bus bandwidth is not available\n");
    }
  }
  
  //------------------- step 4 ----------------------
  return(p_pipe);

}

//%FS===========================================================================
// carbonXUsbPipeDescrUpdate
//
// Description:
//   This function updates the pipe descriptor that has been created.
//   1. Check if the Device existed or not. If existed, create a new pipe and 
//      set the value.
//   2. Check if the pipe Semantic and the bandwidth is enough or not.
//      If Semantic error or the bandwidth is not enough, the pipe state will be 
//      set to USB_PIPE_STAT_SETUP_FAIL;
//   
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the pipe being updated.
//   USB_EndPtDescr_t* p_endpt_descr  I
//     It points to the new endpoint descriptor for updating the pipe.
//
// Return:
//   void
//
// Note:
//   This function can not auto send request to DUT for building pipe.
//   The input pipe must already exist.
//%FE===========================================================================
void carbonXUsbPipeDescrUpdate(USB_PipeDescr_t*  p_pipe, USB_EndPtDescr_t* p_endpt_descr){
  u_int32  cur_frame;

  if( (p_endpt_descr != NULL)&&(p_pipe != NULL) ){
    
    //------------------- step 1 ----------------------
    if( !USB_CHK_DevExist(p_endpt_descr->dev_id) ){
      MSG_Printf("The pipe #%d can not be built. The device #%d does not exist.\n",p_pipe->pipe_id,p_endpt_descr->dev_id);
    }
    else{
      cur_frame = ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_sched.uframe_count;

      p_pipe->p_endpt_descr = p_endpt_descr;
    
      p_pipe->sys_record.uframe_sched     = cur_frame;

      if( (p_endpt_descr->endpt_type == USB_EPTYPE_BULK)||(p_endpt_descr->endpt_type == USB_EPTYPE_INTR) ){
        p_pipe->p_endpt_descr->needs_toggle = TRUE;
      }
   
      //------------------- step 2 ----------------------
      if( ( !USB_CHK_PipeProperties(p_pipe, FALSE) ) && 
        (USB_CHK_BandWidth(p_pipe,p_endpt_descr->max_packet_size)) ){
        p_pipe->sys_record.pipe_state       = USB_PIPE_STAT_IDLE;
      }
      else p_pipe->sys_record.pipe_state       = USB_PIPE_STAT_SETUP_FAIL;
    } 
  }
  
  return;
}


//%FS===========================================================================
// carbonXUsbPipePushInIRP
// 
// Description:
//   This function pushes the specific IRP to the IN FIFO of the specific pipe.
//
// Parameters:
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe that will add a IRP.
//   USB_Irp_t* p_irp   I
//     It points to the specific IRP that will be added to the IN FIFO of
//     the specific pipe.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbPipePushInIRP(USB_PipeDescr_t* p_pipe, USB_Irp_t* p_irp) {

  USB_IrpLink_t      *p_new;
  USB_HostCtrlTop_t  *p_top;

  p_top = (USB_HostCtrlTop_t*) XPORT_GetMyProjectData();

  // Set trans_type of IRP
  switch ( p_pipe->p_endpt_descr->direction) {
  case USB_DIR_OUT:
    p_irp->trans_type = USB_IRP_TYPE_OUT;
    break;

  case USB_DIR_IN:
    p_irp->trans_type = USB_IRP_TYPE_IN;
    break;
    
  case USB_DIR_BI:
    p_irp->trans_type = USB_IRP_TYPE_CTRL_IN;
    break;

  default:
    break;

  }
  

  if( (p_irp  != NULL) && (p_pipe != NULL) ){
  
    p_new = USB_PIPE_IrpLinkGetNew();
    p_new->p_irp = p_irp;
    p_new->p_prev = NULL;
    p_new->p_next = NULL;
          
    if (p_pipe->a_irp_uc.p_head == NULL){
      p_pipe->a_irp_uc.p_head = p_new;
      p_pipe->a_irp_uc.p_tail = p_new;
    }
    else {
      p_new->p_prev = p_pipe->a_irp_uc.p_tail;
      p_pipe->a_irp_uc.p_tail->p_next = p_new;
      p_pipe->a_irp_uc.p_tail = p_new;        
    }
    p_pipe->a_irp_uc.count ++;
    
    if(p_pipe->sys_record.pipe_state == USB_PIPE_STAT_IDLE){
      p_pipe->sys_record.pipe_state = USB_PIPE_STAT_READY;
    }
    
    MSG_Printf( "carbonXUsbPipePushInIRP(): The IRP #%d (%d send, %d receive bytes buddy ID %d) pushed into IN FIFO of the pipe #%d.\n",
		p_irp->irp_id,
		(p_irp->p_send_data) ? p_irp->p_send_data->size : 0,
		p_irp->rcv_bytes,
		p_irp->buddy_id,
		p_pipe->pipe_id);
    
    // Execute Callback function, if it exists
    if ( p_top->pfPipePushInIRP ) {
      // callback function exists.
      // -> Call it.
      p_top->pfPipePushInIRP( p_top, p_pipe, p_irp );
    }
  }
  else {
    MSG_Panic("carbonXUsbPipePushInIRP(): Input pointer can not be NULL!\n");
  }
}



//%FS===========================================================================
// carbonXUsbPipePopInIRP
// 
// Description:
//   This function pops the specific IRP from the IN FIFO of the specific pipe.
//
// Parameters:
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe that will remove a IRP.
//   USB_Irp_t* p_irp   I
//     It points to the specific IRP that will be removed from the IN FIFO of
//     the specific pipe.
// 
// Return:
//   void
//%FE===========================================================================
void carbonXUsbPipePopInIRP(USB_PipeDescr_t* p_pipe,USB_Irp_t*  p_irp){
  u_int32    i;
  BOOL       is_found = FALSE;

  USB_IrpLink_t* p_temp;
  USB_IrpLink_t* p_chk;
  
  if( (p_irp  != NULL)&& (p_pipe != NULL) ){
    
    for(i=0;i<p_pipe->a_irp_uc.count;i++){
      if(i == 0 ){
      	p_chk = p_pipe->a_irp_uc.p_head;
      }
      else p_chk = p_chk->p_next;
      
      if(p_chk == NULL) break;
      
      if(p_chk->p_irp == p_irp){
        MSG_Printf("carbonXUsbPipePopInIRP(): Pop IRP #%d from the IN FIFO of the pipe #%d.\n",p_irp->irp_id,p_pipe->pipe_id);
        p_temp = p_chk->p_prev;
        if(p_temp != NULL) p_temp->p_next = p_chk->p_next;
        else if(p_chk == p_pipe->a_irp_uc.p_head) p_pipe->a_irp_uc.p_head = p_chk->p_next;
        
        p_temp = p_chk->p_next;
        if(p_temp != NULL) p_temp->p_prev = p_chk->p_prev;
        else if(p_chk == p_pipe->a_irp_uc.p_tail) p_pipe->a_irp_uc.p_tail = p_chk->p_prev;
        
        tbp_free(p_chk);
        
        p_pipe->a_irp_uc.count --;
        is_found = TRUE;
        break;	
      }
    
    }
  
    if(!is_found) MSG_Printf("carbonXUsbPipePopInIRP(): The IRP #%d not found in the IN FIFO of the pipe #%d\n",p_irp->irp_id,p_pipe->pipe_id);  
  }
  else MSG_Panic("carbonXUsbPipePopInIRP(): Input pointer can not be NULL!\n");
}


//%FS===========================================================================
// carbonXUsbPipePushOutIRP
// 
// Description:
//   This function pushes the specific IRP to the OUT FIFO of the specific pipe.
//
// Parameters:
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe that will add a IRP.
//   USB_Irp_t* p_irp   I
//     It points to the specific IRP that will be added to the OUT FIFO of
//     the specific pipe.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbPipePushOutIRP(USB_PipeDescr_t* p_pipe,USB_Irp_t* p_irp){
  USB_IrpLink_t* p_new;
  
  if( (p_irp  != NULL)&& (p_pipe != NULL) ){
    p_new = USB_PIPE_IrpLinkGetNew();
    p_new->p_irp = p_irp;
    p_new->p_prev = NULL;
    p_new->p_next = NULL;
      
    if (p_pipe->a_irp_c.p_head == NULL){
      p_pipe->a_irp_c.p_head = p_new;
      p_pipe->a_irp_c.p_tail = p_new;
    }
    else {
      p_new->p_prev = p_pipe->a_irp_c.p_tail;
      p_pipe->a_irp_c.p_tail->p_next = p_new;
      p_pipe->a_irp_c.p_tail = p_new;        
    }
    p_pipe->a_irp_c.count ++;
    
    MSG_Printf("carbonXUsbPipePushOutIRP(): The IRP #%d pushed into the OUT FIFO of the pipe #%d.\n",p_irp->irp_id,p_pipe->pipe_id);
    
  }
  else MSG_Panic("carbonXUsbPipePushOutIRP(): Input pointer can not be NULL!\n");
}


//%FS===========================================================================
// carbonXUsbPipePopOutIRP
// 
// Description:
//   This function pops the specific IRP from the OUT FIFO of the specific pipe.
//
// Parameters:
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe that will remove a IRP.
//   USB_Irp_t* p_irp   I
//     It points to the specific IRP that will be removed from the OUT FIFO of
//     the specific pipe.
// 
// Return:
//   void
//%FE===========================================================================
void carbonXUsbPipePopOutIRP(USB_PipeDescr_t* p_pipe,USB_Irp_t*  p_irp){
  u_int32        i;
  BOOL           is_found = FALSE;
  USB_IrpLink_t* p_temp;
  USB_IrpLink_t* p_chk;
  
 if( (p_irp  != NULL)&& (p_pipe != NULL) ){
        
    for(i=0;i<p_pipe->a_irp_c.count;i++){
      if(i == 0 ){
      	p_chk = p_pipe->a_irp_c.p_head;
      }
      else p_chk = p_chk->p_next;
      
      if(p_chk->p_irp == p_irp){
        MSG_Printf("carbonXUsbPipePopOutIRP(): Pop IRP #%d from the OUT FIFO of the pipe #%d.\n",p_irp->irp_id,p_pipe->pipe_id);
        p_temp = p_chk->p_prev;
        if(p_temp != NULL)p_temp->p_next = p_chk->p_next;
        
        p_temp = p_chk->p_next;
        if(p_temp != NULL) p_temp->p_prev = p_chk->p_prev;
        else if(p_chk == p_pipe->a_irp_uc.p_tail) p_pipe->a_irp_uc.p_tail = p_chk->p_prev;
        
        tbp_free(p_chk);
        
        p_pipe->a_irp_c.count --;
        is_found = TRUE;
        break;	
      }
         
    }
   
    if(!is_found) MSG_Printf("carbonXUsbPipePopOutIRP(): The IRP #%d not found in the OUT FIFO of the pipe #%d\n",p_irp->irp_id,p_pipe->pipe_id);
  }
  else MSG_Panic("carbonXUsbPipePopOutIRP(): Input pointer can not be NULL!\n");
}



//%FS===========================================================================
// carbonXUsbPipeGetState
//
// Description:
//   This function gets the specific pipe current state.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe      I
//     It points to the pipe being inquired about.
//
// Return:
//   USB_PipeState_e
//     If OK, the pipe current state will be returned.
//     If error, USB_PIPE_STAT_SETUP_FAIL will be returned, the warning message 
//     will be reported.
//%FE===========================================================================
USB_PipeState_e carbonXUsbPipeGetState(USB_PipeDescr_t* p_pipe){
  if(p_pipe != NULL) return(p_pipe->sys_record.pipe_state);
  else {
    MSG_Panic("carbonXUsbPipeGetState(): Input pointer can not be NULL!\n");
    return(USB_PIPE_STAT_SETUP_FAIL);
  }
}


//%FS===========================================================================
// carbonXUsbPipeSetState
//
// Description:
//   This function sets a specific pipe state.
//   1. From halted state to new state pipe system record will be reset.
//   2. If new state is USB_PIPE_STAT_HALTED, update the pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe   I
//     It points to the pipe being set.
//   USB_PipeState_e new_state  I
//     It points to the new state.
//
// Return:
//   void
//
// Note:
//   If new state is USB_PIPE_STAT_HALTED, the function USB_PIPE_Update()
//   will be done.
//%FE===========================================================================
void carbonXUsbPipeSetState(USB_PipeDescr_t* p_pipe,USB_PipeState_e new_state){
  if(p_pipe != NULL) {
    
    //---------------------- step 1 -----------------------
    if(p_pipe->sys_record.pipe_state == USB_PIPE_STAT_HALTED) {
      p_pipe->p_endpt_descr->pid              = USB_PID_DATA0; 
      p_pipe->sys_record.auto_trigger     = 0;
      p_pipe->sys_record.err_cnt          = 0;
      p_pipe->sys_record.duration_est     = 0;
      p_pipe->sys_record.iso_cnt          = 0;
      p_pipe->sys_record.iso_trans_cnt    = 0;
      p_pipe->sys_record.iso_data_cnt     = 0;
      p_pipe->sys_record.iso_fb_cnt       = 0;
      p_pipe->sys_record.uframe_sched     = 0;
      p_pipe->sys_record.prev_trans_state = USB_DO_NEXT_CMD;
    }
    
    //---------------------- step 2 -----------------------
    p_pipe->sys_record.pipe_state = new_state;
    if(new_state == USB_PIPE_STAT_HALTED) USB_PIPE_Update(p_pipe);
  }
  else MSG_Panic("carbonXUsbPipeSetState(): Input pointer can not be NULL!\n");
    
}


//%FS===========================================================================
// carbonXUsbPipeGetNumComplIRPs
//
// Description:
//   This function gets the number of IRPs in OUT FIFO of the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe   I
//     It points to the specific pipe 
//
// Return:
//   u_int32
//     If OK, the number of the IRPs in OUT FIFO will be returned.
//     If error, zero will be returned, the warning message 
//     will be reported.
//%FE===========================================================================
u_int32 carbonXUsbPipeGetNumComplIRPs(USB_PipeDescr_t*   p_pipe){
  if(p_pipe != NULL) return(p_pipe->a_irp_c.count);
  else {
    MSG_Panic("carbonXUsbPipeGetNumComplIRPs(): Input pointer can not be NULL!\n");
    return(0);
  }
}


//%FS===========================================================================
// carbonXUsbPipeGetNumUncomplIRPs
//
// Description:
//   This function gets the number of IRPs in IN FIFO of the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe   I
//     It points to the specific pipe 
//
// Return:
//   u_int32
//     If OK, the number of the IRPs in IN FIFO will be returned.
//     If error, zero will be returned, the warning message 
//     will be reported.
//%FE===========================================================================
u_int32 carbonXUsbPipeGetNumUncomplIRPs(USB_PipeDescr_t* p_pipe){
  if(p_pipe != NULL) return(p_pipe->a_irp_uc.count);
  else {
    MSG_Panic("carbonXUsbPipeGetNumUncomplIRPs(): Input pointer can not be NULL!\n");
    return(0);
  }
}


//%FS===========================================================================
// carbonXUsbPipeReport
//
// Description:
//   This function reports the pipe information
//
// Parameters: 
//   BOOL  is_one  I
//     If it is TRUE, report the specific pipe information.
//     If it is FALSE, report all pipe information in system.
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe. If is_one is TRUE, the value is effectual.
//
// Return: 
//   void
//%FE===========================================================================
void carbonXUsbPipeReport(BOOL is_one,USB_PipeDescr_t*  p_pipe){
  u_int32     i;

  USB_HostCtrlTop_t*        p_top;
  USB_PipeDescr_t*  p_temp;
  
  p_top = (USB_HostCtrlTop_t*) XPORT_GetMyProjectData();
  
    
  if( (is_one)&&(p_pipe != NULL) ){
    USB_PIPE_MsgHeader();

    USB_PIPE_Msg(p_pipe);
    
    USB_PIPE_MsgEnd();
  }
  else if( (is_one)&&(p_pipe == NULL) ){
     MSG_Panic("carbonXUsbPipeReport(): Report Pipe Information, input pointer cannot be NULL!\n");
  }
  else {
    USB_PIPE_MsgHeader();
  
    USB_PIPE_MsgTotal();
    
    for(i=0; i < p_top->sys_pipe_mgr.count; i++){
      p_temp = *(p_top->sys_pipe_mgr.ppa_pipe_head + i);
        
      if(p_temp == NULL) continue;
         
      USB_PIPE_Msg(p_temp);
    }
  
    USB_PIPE_MsgEnd();
  }
}


//%FS===========================================================================
// carbonXUsbPipeDestroy
// 
// Description:
//   This function destroys the specific pipe.
//   1. check the pipe is default pipe or not. If FALSE, do next step.
//   2. freeing the IN FIFO and OUT FIFO, and setting the the pipe pointer to NULL.
//
// Parameters: 
//   USB_PipeDescr_t*  p_pipe     I
//     It points to the pipe being destroyed.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbPipeDestroy(USB_PipeDescr_t*  p_pipe){
  USB_PipeDescr_t** pp_temp;
  u_int32           pipe_cnt;
  BOOL              found = FALSE;

  pipe_cnt = ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_pipe_mgr.count;
  
  if(p_pipe != NULL){

    //------------------- step 1 ---------------------------
    if(p_pipe->p_endpt_descr->endpt_num == 0){
      MSG_Printf("Pipe #%d is Default Control pipe for a device which device is %d.\n",p_pipe->pipe_id,p_pipe->p_endpt_descr->dev_id);  
    }
    else {
      pp_temp = ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_pipe_mgr.ppa_pipe_head + p_pipe->pipe_id;
      
      if(pp_temp != NULL){
        if(*pp_temp == p_pipe){
          found = TRUE;
        }
        else MSG_Error("carbonXUsbPipeDestroy(): Input Pipe cannot be found in the Pipe List!\n");
      }
      else MSG_Panic("carbonXUsbIrpDestroy(): Input Pipe cannot be NULL!\n");
    }
    
  }
  else MSG_Panic("carbonXUsbIrpDestroy(): Input pointer cannot be NULL!\n");
  
  //------------------- step 2 ---------------------------
  if(found){
     MSG_Printf("Destroyed pipe #%d, all information in this pipe has been lost.\n",p_pipe->pipe_id);
     USB_PIPE_IrpLinkFree(p_pipe);
     tbp_free(p_pipe);
     *pp_temp = NULL;
     p_pipe = NULL;
  }
}




//%FS==========================================================================
// carbonXUsbSetEpDescr
//
// Description:
//   This function puts the endpoint descriptor to structure.
//
// Parameters:
//   USB_EndPtDescr_t* p_ep  I/O
//     It points to the endpoint descriptor being set.
//   BOOL   is_auto  I
//     It points to auto building pipe or not.
//   u_int8  dev_id  I
//     It points to the endpoint locating which device
//   u_int32 endpt_num  I
//     It points to the endpoints number
//   u_int32  hs_trans_num  I
//     It points to the number of transactions per (micro)frame.
//   u_int32  interval I
//     It points to the period
//   u_int32   max_packet_size  I
//     It points to the endpoint supported max packet size
//   USB_Direction_e   direction I
//     It points to the endpoint direction
//   USB_EpType_e    endpt_type  I
//     It points to the endpoint type
//   USB_SyncType_e   sync_type  I
//     It points to the synchronous type for the ISO transfer.
//   USB_UsageType_e   usage_type  I
//     It points to the usage type for the ISO transfer.
//
// Return:
//   void 
//%FE==========================================================================
void  carbonXUsbSetEpDescr(
  USB_EndPtDescr_t*  p_ept,
  BOOL 		     is_auto,
  u_int8             dev_id,
  u_int32            endpt_num,
  u_int32            hs_trans_num,
  u_int32            interval,
  u_int32            max_packet_size,
  USB_Direction_e    direction,
  USB_EpType_e       endpt_type,
  USB_SyncType_e     sync_type,
  USB_UsageType_e    usage_type
){
  USB_HostCtrlTop_t* p_top;

  if(p_ept != NULL) {
    p_ept->is_auto         = is_auto;             
    p_ept->dev_id          = dev_id;               
    p_ept->endpt_type	   = endpt_type;       
    p_ept->direction	   = direction;         
    p_ept->curr_dir	   = direction;         
    p_ept->endpt_num	   = endpt_num;         
    p_ept->sync_type	   = sync_type;         
    p_ept->usage_type	   = usage_type;       
    p_ept->max_packet_size = max_packet_size; 
    p_ept->hs_trans_num	   = hs_trans_num;    
    p_ept->interval	   = interval;
    p_ept->allow_errors    = 0;

    // The very first DATA PID will always be DATA0
    p_ept->pid              = USB_PID_DATA0;
    p_ept->needs_toggle     = FALSE;
    p_ept->is_first_trans   = TRUE;

    // By initializing prev_trans_toggle_pid and curr_trans_toggle_pid to
    // USB_PID_DATA1, we make sure that toggle checking gets off to a correct start.
    // At the first DATAx transaction we copy curr_trans_toggle_pid to prev_trans_toggle_pid
    // and set curr_trans_toggle_pid to the actual DATA PID (should be DATA0)
    MSG_Printf( "<%.10lld> ++++++++ carbonXUsbSetEpDescr\n", PLAT_GetSimTime() );
    p_ept->prev_trans_toggle_pid = USB_PID_DATA1;  
    p_ept->curr_trans_toggle_pid = USB_PID_DATA1; 
    
    // Callback function. A typical call back function will write to the
    // Endpoint configuration registers of the USB IP core
    p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
    if ( p_top->pfSetEpDescr ) {
      // callback function exists.
      // -> Call it.
      p_top->pfSetEpDescr( p_top, p_ept );
    }    
  }
  else {
    MSG_Panic("carbonXUsbSetEpDescr(): Input pointer cannot be NULL!\n");
  }
}      


//%FS===========================================================================
// carbonXUsbGetDefPipe
//
// Description:
//   This function gets the Default Control Pipe for the specific device.
//
// Parameter:
//   u_int32  dev_id
//     It points to the specific device.
//
// Return:
//   USB_PipeDescr_t*
//     If Ok, the device default pipe will be returned.
//     If not, the returned value is NULL.
//%FE===========================================================================
USB_PipeDescr_t* carbonXUsbGetDefPipe(u_int32 dev_id){
  USB_PipeDescr_t* p_pipe;
  USB_PipeDescr_t* p_def = NULL;
  u_int32          i;
  u_int32          pipe_cnt;
 
  pipe_cnt = USB_PIPE_PA_GetPipeCount();
 
  for(i=0;  i<pipe_cnt; i++){
    p_pipe = carbonXUsbGetPipeDescr(i);
    if(p_pipe == NULL) continue;

    if( p_pipe->p_endpt_descr != NULL &&  p_pipe->p_endpt_descr->endpt_num == 0){
      
      if((carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id))->dev_id == dev_id){
        p_def = p_pipe;
        break;
      }
    }
  }
    
  return(p_def);
}



//%FS===========================================================================
// USB_PIPE_PA_GetPipeCount
//
// Description:
//   This function gets the pipes current level.
//
// Parameter:
//   void
//
// Return:
//   u_int32
//     The pipe current level will be returned.
//%FE===========================================================================
u_int32 USB_PIPE_PA_GetPipeCount(){
  return( ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_pipe_mgr.count );
}


void carbonXUsbError_Count_Clear(USB_Trans_t* trans) {

	trans->p_pipe->sys_record.err_cnt = 0;

}

//%FS===========================================================================
// carbonXUsbPipeReadToggle
// 
// Description:
//  This function simply returns the next expected toggle PID.
//
// Parameters: 
//   USB_PipeDescr_t*  p_pipe     I
//     It points to the pipe being destroyed.
//
// Return:
//   void
//%FE===========================================================================
u_int32 carbonXUsbPipeReadToggle(USB_PipeDescr_t*  p_pipe){

	return(	p_pipe->p_endpt_descr->prev_trans_toggle_pid);

}


//%FS===========================================================================
// carbonXUsbPipeToggleData
// 
// Description:
//  This function toggles the DATA PID.
//  @@@RZ This function should be removed. It is not doing the right thing
//
// Parameters: 
//   USB_PipeDescr_t*  p_pipe     I
//     It points to the pipe being destroyed.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbPipeToggleData(USB_PipeDescr_t*  p_pipe, USB_Pid_e prev_toggle_pid){

  MSG_Printf( "<%.10lld> ++++++++ carbonXUsbPipeToggleData\n", PLAT_GetSimTime() );

  p_pipe->p_endpt_descr->prev_trans_toggle_pid = prev_toggle_pid;

  if(prev_toggle_pid == USB_PID_DATA0) {
    p_pipe->p_endpt_descr->pid = USB_PID_DATA1;
  }
  else {
    p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
  }

  return;
}

