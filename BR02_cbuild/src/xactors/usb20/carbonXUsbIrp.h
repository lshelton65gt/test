
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifndef CARBONX_USB_IRP_H
#define CARBONX_USB_IRP_H

USB_TransStage_e  carbonXUsbGetTransferStage (USB_Irp_t* p_irp);

USB_Irp_t*  carbonXUsbGetIrp(int irp_id);

USB_Irp_t* carbonXUsbIrpCreate(
  USB_Data_t*        p_setup_data, 
  USB_Data_t*        p_send_data,
  USB_IrpTransType_e trans_type,
  int                buddy_id
);

USB_IrpStatus_e carbonXUsbIrpGetStat(USB_Irp_t* p_irp);

void carbonXUsbIrpReport(BOOL is_one,USB_PipeDescr_t* p_pipe,USB_Irp_t*  p_irp);

void carbonXUsbIrpDestroy(USB_Irp_t* p_irp);

void carbonXUsbIrpSetError(int irp_id_start, int irp_id_end, u_int16 err_mask);


// Function to generate random data
USB_Data_t* carbonXUsbCreateRandDataInRange(u_int32 range_start, u_int32 range_end,
  u_int32 test_data_size);

// Function to generate sequential data
USB_Data_t* carbonXUsbCreateSeqData(u_int32 range_start, u_int32 test_data_size);

// Function to sets the number of bytes that are expected to be received for this IRP
void carbonXUsbIrpSetRcvDataSize(USB_Irp_t* p_irp, u_int32 rcv_bytes);

#endif
