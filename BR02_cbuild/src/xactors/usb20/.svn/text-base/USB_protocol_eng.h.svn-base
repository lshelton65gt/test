//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file is required for PE. It must contain:
 *                 1. All necessary data structs used by Protocol Engine.
 *                 2. Struct USB_Trans_t and USB_ErrInjection_t are 
 *                    mainly used to interact with Scheduler and XIF 
 *                    by calling USB_DoTransfer(), carbonXSendPacket(), 
 *                    carbonXReceivePacket(), carbonXCsWrite() and carbonXCsRead().
 *                 3. Struct USB_CommandRec_t is mainly used by the HC 
 *                    FSM to control the process, alse used by C side 
 *                    monitor to log information.
 *                 4. Struct USB_HsBusRec_t is mainly used to record the
 *                    returned information from sim-side, alse used by 
 *                    C side monitor to log information.
 *                 5. Reference to USB 2.0 Host SVC Architecture
 *                    Specification and USB 2.0 standard Appendix B.
 *
 ***************************************************************************/

#ifndef USB_PROTOCOL_ENG_H
#define USB_PROTOCOL_ENG_H


#define USB_LOOP_BACK          0

// fredieu - IPG handling is not correct at the moment
//#define USB_IPG	               10
#define USB_IPG	               0
#define BFM_READY              1
#define HS_SIZE                1
#define HS_EXPECTED_DURATION   20     // Expected duration of HS packet (in BFM clock cycles)

/* Indicates what is waited, used for USB_WaitForPacket */
typedef enum {
        USB_WAIT_ITG  = 0x0,		// Inter Packet Gape
        USB_WAIT_NONE = 0x1		// No wait
} USB_Waits_e;



/* 
 * Command state that the HC must act upon, used by the Host Controller FSM.
 * Reference to USB2.0 standard P556.
 */
typedef struct {				
  BOOL            needs_toggle;		// True indicates sequence bit toggle
  BOOL            is_hs;		// True indicates high-speed
  BOOL            is_last;		// True indicates last complete split 
  					// token
  BOOL            is_ping;	        // True indicates PING token
  BOOL            is_setup;		// True indicates control setup
  u_int32         dev_addr;             // Device address is 0 - 127
  u_int32         endpt_num;            // Endpoint number is 0 - 15
  USB_DataPart_e  datapart;
  USB_Direction_e direction;
  USB_EpType_e    ep_type;
  USB_Pid_e       pid;
  USB_TransType_e trans_type;			
} USB_CommandRec_t;

/* 
 * Partial high speed transaction state from a high speed bus, used by Host 
 * Controller FSM. Reference to USB2.0 standard P556. 
 */
typedef struct{
  BOOL            did_timeout;		// True indicates there is a timeout
  BOOL            is_crc_ok;
  u_int32         dev_addr;		// Device address is 0 - 127
  u_int32         endpt_num;		// Endpoint number is 0 - 15
  u_int32         pkt_ready;		// Indicate packet ready in one of the 
  					// bus ports: 0 - 4, 0 for none, 1 for
  					// HSD1, 2 for HSD2, 3 for HSU1, 4 for
  					// HSU2. Reference to USB2.0 standard
  					// p211(figure 8-18) and p348(figure
  					// 11-31).
  USB_DataPart_e  datapart;
  USB_Direction_e direction; 
  USB_EpType_e    ep_type; 
  USB_Pid_e       pid;
} USB_HsBusRec_t;

/* Packet struct used to transfer data to XIF */
typedef struct {
  u_int8  token_pkt[4];
  u_int8  data_pkt[1028];		// For non-split transaction, the max size
  					// is 1024 bytes data packet size;
  					// For split transaction, the max size
  					// is 4 bytes token packet size + 1024 
  					// bytes data packet size.
} USB_Packet_t;


typedef enum {
  USB_DoBcintI,
  USB_DoBcintO,
  USB_DoBcIcs,
  USB_DoBcIss,
  USB_DoBcOcs,
  USB_DoBcOss,
  USB_DoHsBcO,
  USB_DoIsoIcs,
  USB_DoIntIcs,
  USB_DoIntOcs,
} USB_BusTransType_e;

/* Function to transfer data between the host and the device */
void USB_PE_DoTransfer(USB_Trans_t* p_trans);

/* Function to do a USB nonsplit transaction */
void USB_PE_DoNonsplit(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
                       USB_HsBusRec_t* p_hsx);
                      
/* Function to do a USB start split transaction */                      
void USB_PE_DoStart(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
                    USB_HsBusRec_t* p_hsx);

/* Function to do a USB start split transaction */                      
void USB_PE_DoComplete(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
                       USB_HsBusRec_t* p_hsx);                                         

/* Function to do isochronous IN non-split transactions */
void USB_PE_DoIsoI(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		   USB_HsBusRec_t* p_hsx);

/* Function to do bulk/control/interrupt IN non-split transactions */
void USB_PE_DoBcintI(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		     USB_HsBusRec_t* p_hsx);

/* 
 * Function to do interrupt or full-/low-speed bulk/control OUT non-split 
 * transactions. 
 */
void USB_PE_DoBcintO(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		     USB_HsBusRec_t* p_hsx);

/* Function to do high-speed bulk/control OUT non-split transactions */
void USB_PE_DoHsBcO(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		    USB_HsBusRec_t* p_hsx);

/* Function to do bulk/control IN start-split transactions */
void USB_PE_DoBcIss(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		    USB_HsBusRec_t* p_hsx);

/* Function to do bulk/control OUT start-split transactions */
void USB_PE_DoBcOss(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		    USB_HsBusRec_t* p_hsx);

/* Function to do isochronous IN complete-split transactions */
void USB_PE_DoIsoIcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		     USB_HsBusRec_t* p_hsx);

/* Function to do interrupt IN complete-split transactions */
void USB_PE_DoIntIcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		     USB_HsBusRec_t* p_hsx);

/* Function to do bulk/control IN complete-split transactions */
void USB_PE_DoBcIcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		    USB_HsBusRec_t* p_hsx);

/* Function to do interrupt OUT complete-split transactions */
void USB_PE_DoIntOcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		     USB_HsBusRec_t* p_hsx);

/* Function to do bulk/control OUT complete-split transactions */
void USB_PE_DoBcOcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
		    USB_HsBusRec_t* p_hsx);

/* Function to issue a particular packet */
void USB_PE_IssuePacket(USB_Trans_t* p_trans);

/* Function to write the BFM configuration register */
void USB_PE_WriteBfmReg(USB_Trans_t* p_trans);

/* Function to issue a particular handshake */
void USB_PE_IssueHandshake(USB_Pid_e hs_pid);

/* Function to wait the device's response */
void USB_PE_WaitForPacket( USB_Waits_e wait_type, 
                          USB_Trans_t* p_trans, USB_HsBusRec_t* p_hsx);

/* Function to read the BFM status register */
void USB_PE_ReadBfmReg(USB_Trans_t* p_trans, USB_HsBusRec_t* p_hsx);

/* Function to count the retry times */
void USB_PE_IncError(USB_Trans_t* p_trans);

/* Function to feed back a result to the host */
void USB_PE_RespondHc(USB_Trans_t* p_trans, USB_HcTransState_e hc_trans_state);

/* Function to record error occurring in Isochronous transfers */
void USB_PE_RecordError(USB_HsBusRec_t* p_hsx, USB_CommandRec_t* p_hccmd);

/* Function to build packet from structure USB_Trans_t */
void USB_PE_BuildPktFromTrans(USB_Trans_t* p_trans, USB_Packet_t* po_tx_packet);

/* Function to build a SOF packet */
u_int32 USB_PE_BuildSof(USB_Trans_t* p_trans);

/* Function to build a normal token packet */
u_int32 USB_PE_BuildToken(USB_Trans_t* p_trans);

/* Function to build structure USB_Trans_t from received packet */
u_int32 USB_PE_BuildSplit(USB_Trans_t* p_trans);
			   
/* Function to build a normal token packet */
void USB_PE_BuildTransFromPkt(USB_Trans_t* po_trans);

/* Function to build a PID byte from a PID enum */
u_int8 USB_BuildPid( USB_Pid_e pid );


/* Function for handling protocol errors */
void USB_PE_HandleProtocolError( USB_Trans_t* p_trans, USB_HsBusRec_t* p_hsx, USB_CommandRec_t* p_hccmd,
				 USB_BusTransType_e bus_trans_type );

BOOL USB_PE_Data01Toggle( u_int8 pid_a, u_int8 pid_b );


#endif

/*
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.3  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.2  2005/11/21 23:15:21  jstein
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: checkin / runlist -grid -xactors / xactor examples regress all
 *               : Bugs Affected: Added xactor examples for spi3, utopia2
 *               :                Updated enet_*, ahb, pci to use new carbonX calls.
 *               :                Updated usb20 (with Goran's assistance) to fix multiple problems.
 *               :                Updated runlist.bom to get runlist -grid -xactors to work on SGE
 *               :                Changed Makefile.carbon.vsp xactor command to look in dir "."
 *               :                Added log files to all test.run tests
 *               :                changed malloc to carbonmem_malloc as much as possible.
 *               :                changed Makefile.libs to search CARBON_LIB_LIST during xactor compiles.
 *               :                Updated documentation, added to html dirs.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.9  2004/08/12 14:11:19  zippelius
 *               : Numerous fixes and changes around DATA toggle
 *               :
 *               : Revision 1.8  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.7  2004/07/23 04:31:39  fredieu
 *               : Fix for IPG
 *               :
 *               : Revision 1.6  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.5  2004/04/23 17:45:12  zippelius
 *               : Major code clean up. Removed redundant code. Extracted common code into shared functions. Streamlined code. Did bug fixes. Added response field to USB_Trans_t
 *               :
 *               : Revision 1.4  2004/04/22 13:44:28  zippelius
 *               : Clean-up defines, messaging, others
 *               :
 *               : Revision 1.3  2004/04/16 20:35:15  zippelius
 *               : Some code clean up. Added defines
 *               :
 *               : Revision 1.2  2004/03/26 15:00:18  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.2  2003/12/03 02:13:54  adam
 *               : Edit testcase
 *               :
 *               : Revision 2.1  2003/11/25 09:41:10  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.0  2003/09/27 06:34:13  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:42  adam
 *               : updated file directory
 *               :
 *               : Revision 1.6  2003/09/27 04:35:11  adam
 *               : Debug
 *               :
 *               : Revision 1.5  2003/09/26 12:53:03  adam
 *               : Add testcases
 *               :
 *               : Revision 1.4  2003/09/26 11:02:44  adam
 *               : Add testcases
 *               :
 *               : Revision 1.3  2003/09/08 13:53:20  adam
 *               : Change USB_ to USB_PE_
 *               :
 *               : Revision 1.2  2003/09/08 03:53:59  adam
 *               : move define from USB_protocol_eng.c
 *               :
 *               : Revision 1.1  2003/09/08 02:26:32  adam
 *               : moved USB_pe.h to USB_protocol_eng.h
 *               :
 *               : Revision 1.6  2003/08/25 09:23:44  adam
 *               : Fixed carbonXReceivePacket() usage.
 *               :
 *               : Revision 1.4  2003/08/15 07:33:43  adam
 *               : Added PE Buffer for better system wide partitioning and protection.
 *               :
 *               : Revision 1.3  2003/08/14 16:14:45  adam
 *               : New revision, created.
 *               :
 *               : Revision 1.2  2003/08/14 14:21:35  adam
 *               : New revision, created.
 *               :
 */

