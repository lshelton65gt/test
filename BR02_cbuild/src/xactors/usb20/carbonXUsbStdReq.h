//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONX_USB_STD_REQ_H
#define CARBONX_USB_STD_REQ_H
 
void carbonXUsbDoBusEnum();

void carbonXUsbDeviceAttach();

void carbonXUsbDeviceRemove();

void carbonXUsbReset(USB_TransSpeed_e word_speed);

void carbonXUsbGetDescr(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int8           descr_type, 
  u_int8           descr_index,
  u_int16          language_id,
  u_int16          descr_length 
);

void carbonXUsbClearFeature(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int16          feature_selector, 
  u_int16          recipient
);

void carbonXUsbGetCfg(USB_PipeDescr_t* p_pipe);

void carbonXUsbGetIntf(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          interface  
);

void carbonXUsbGetStatus(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int16          recipient,
  u_int16          w_length
);

void carbonXUsbSetAddr(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          dev_addr
);

void carbonXUsbSetCfg(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          cfg_value
);

void carbonXUsbSetDescr(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int8           descr_type, 
  u_int8           descr_index,
  u_int16          language_id,
  u_int16          descr_length,
  USB_Data_t*      p_descr		// Descriptor
);

void carbonXUsbSetFeature(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int16          feature_selector, 
  u_int16          recipient
    
);

void carbonXUsbSetIntf(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          alternate_set, 
  u_int16          interface
);

void carbonXUsbSynchFrame(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          endpoint_num
);

void carbonXUsbClearTTBuffer(
  USB_PipeDescr_t* p_pipe,
  u_int8           dev_addr,
  u_int8           ep_num,
  USB_EpType_e     ep_type,
  USB_Direction_e  direction,
  u_int16          tt_port
);

#endif
