//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**************************************************************************
 *
 * Comments      : This file includes all functions relating to Data pointer array struct operation,
 *                 It can be classified into 3 groups:
 *                 [USB_CHK_*]
 *
 *                 
 *                 
 *                 USB_CHK_* are functions that SVC uses to operate on the 
 *                 data pointer array struct, they include:
 *
 *                 USB_CHK_PipeProperties();
 *                 USB_CHK_BusTimeCal();
 *                 USB_CHK_TotalBw();
 *                 USB_CHK_GetIsoBw();     
 *                 USB_CHK_GetIntrBw();    
 *                 USB_CHK_PeriodCurBw();  
 *                 USB_CHK_StateUse();     
 *                 USB_CHK_BandWidth();    
 *                 USB_CHK_MultiTransBw(); 
 *                 USB_CalcEstSofBitTime();    
 *                 USB_CHK_CalcExpBitTime();   
 *                 USB_CHK_DevExist();     
 *                 USB_CHK_HsIsoOutSplit();
 *                 USB_CHK_UserIsCfg();    
 *                 USB_CHK_DutIsCfg();
 *                 USB_CHK_DevIsSame();
 *                 USB_CHK_HubDescIsSame();
 *                 USB_CHK_DevIsSame();
 *
 *                 Note:
 *                   The USB_CHK_BusTimeCal function calculates bus time by high-speed
 *                   bit time.
 *                 
 *              
 ***************************************************************************/


#include "USB_support.h"

//%FS===========================================================================
// USB_CHK_PipeProperties
// 
// Description:
//   This function only checks semantic for the endpoint descriptor of pipe
//   1. Check if there is a duplicate endpoint number.
//   2. Check the number of all endpoints.
//   3. Unless its a ISO endpoint, the sync_type and the usage_type must set to zero.
//   4. Check control pipe semantic.
//   5. Check isochronous pipe semantic.
//   6. Check interrupt pipe semantic.
//   7. Check bulk pipe semantic.
//
// Parameter:
//  u_int32 pipe_id I
//    It points to the specific pipe.
//
// Return:
//   If TRUE, there have errors.
//   If FALSE, there has not error.
//%FE===========================================================================
BOOL USB_CHK_PipeProperties (USB_PipeDescr_t*  p_pipe, BOOL is_new) {  

  u_int32 i       = 0;
  u_int32 low_cnt = 0;
  u_int32 cnt_in  = 0;                  // For full/high-speed endpoint count
  u_int32 cnt_out = 0;
  BOOL    has_err = FALSE;
  
  USB_HostCtrlTop_t*        p_top;
  USB_DevProfile_t* p_dev;
  USB_DevProfile_t* p_chk;
  USB_PipeDescr_t*  p_pipe_chk;
  USB_EndPtDescr_t* p_ep;               
  
  p_top  = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  if(p_pipe == NULL){
    MSG_Panic("USB_CHK_PipeProperties(): Input pipe pointer is NULL.\n");
    return(FALSE);
  }
  p_dev  = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
  if(p_dev->dev_id == 0){
    MSG_Error("USB_CHK_PipeProperties(): Cannot find the device #%d.\n",p_pipe->p_endpt_descr->dev_id);
    return(FALSE);
  }
  p_ep   = p_pipe->p_endpt_descr;
  
   
  //Check the endpoint number
  for(i=1; i<p_top->sys_pipe_mgr.count; i++){
    if(p_pipe->pipe_id != i){
      p_pipe_chk = carbonXUsbGetPipeDescr(i);
      if(p_pipe_chk == NULL) continue;

      p_chk  = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
      if(p_chk == NULL){
        MSG_Warn("USB_CHK_PipeProperties(): The pipe #%d is not used by any device.\n", p_pipe->pipe_id);
      }
      
      //-------------------------- step 1 ---------------------------
      if( ( p_chk->dev_addr == p_dev->dev_addr ) && (is_new)){
        if ( (p_ep->endpt_num == p_pipe_chk->p_endpt_descr->endpt_num) &&
	     (p_ep->direction == p_pipe_chk->p_endpt_descr->direction) ){
          if((p_ep->endpt_type != USB_EPTYPE_CTRL) ){
	    MSG_Error( "USB_CHK_PipeProperties(): In Pipes #%d and #%d share the same endpoint number (%d) and direction (%s)\n",
		       p_pipe->pipe_id, p_pipe_chk->pipe_id, p_ep->endpt_num, carbonXUsbCvtDirTypeToString( p_ep->direction ) );
	    
          }
          else {
            MSG_Error("USB_CHK_PipeProperties(): Control pipes #%d and #%d use the same endpoint number %d\n",
                  p_pipe->pipe_id, p_pipe_chk->pipe_id, p_ep->endpt_num);
          }
          has_err = TRUE;          
        }
      }

      //-------------------------- step 2 ---------------------------
      //Check Low-speed only supported 3 endpoints
      if (p_dev->speed == USB_TRANS_LS){
        switch ( p_ep->endpt_type ){
          case USB_EPTYPE_CTRL:{
            low_cnt += 1;
            break;
          }
          case USB_EPTYPE_INTR:{
            low_cnt += 1;
            break;
          }
          default:{
            MSG_Warn( "USB_CHK_PipeProperties(): The device is a LOW speed device.\n%s %d\n",
		      "   It only supports the control and interrupt transfers. Not endpoint type %s",
		      carbonXUsbCvtEndPtTypeToString( p_ep->endpt_type) );
            has_err = TRUE;
            break;
          }
        }
        if ( low_cnt > 3 ) {
          MSG_Warn("USB_CHK_PipeProperties(): The device is a LOW speed device.\n    It only supports three pipes not %d.\n",low_cnt);
          has_err = TRUE;
        }
      }  
      //Check Full/High-speed support 16 IN or OUT endpoints
      else{
        switch ( p_ep->direction){
          case  USB_DIR_BI:{
            cnt_in ++;
            cnt_out ++;
            break;
          }
          case USB_DIR_IN:{
            cnt_in ++;
            break;
          }
          case USB_DIR_OUT:{
            cnt_out ++;
            break;
          }
        case USB_DIR_UNDEF:{
            MSG_Warn ("USB_CHK_PipeProperties(): Invalid direction specified\n");
            has_err = TRUE;
            break;
          }
        } 
        if (( cnt_in > 16 )||( cnt_out >16 )) {
          MSG_Warn("USB_CHK_PipeProperties(): The device only supported 16 IN or OUT endpoints\n");
          has_err = TRUE;
        }
      }//end of F/H-speed check
      
    }
  }   
  
  //-------------------------- step 3 ---------------------------
  //Check Synchonous type and usage type for no-isochronous transfer.  
  if (p_ep->endpt_type != USB_EPTYPE_ISO){
    if ((p_ep->sync_type != 0)||(p_ep->usage_type != 0)) {
      MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is not isochronous pipe\n", p_pipe->pipe_id);
      MSG_Warn("The synchronization type and usage type must be set to zero.\n");
      has_err = TRUE;
    }
  }
  //
    switch ( p_ep->endpt_type ) {
    //
    //+-------------------------------------------
    //|Check control transfer
    //|-------------------------------------------
    //|  pipe direction             : bi-directional
    //|  high-speed transfer number : 0
    //|Low-speed:
    //|  max_packet_size = 8 bytes
    //|Full-speed and High-speed:
    //|  max_packet_size = 8, 16, 32 or 64 bytes
    //|High-speed:
    //|  max_packet_size = 64 bytes
    //+-------------------------------------------
    //
    
    //-------------------------- step 4 ---------------------------
    case USB_EPTYPE_CTRL: {
      if ( p_ep->direction != USB_DIR_BI ) {
        MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a control pipe\n", p_pipe->pipe_id);
        MSG_Warn("The control pipe should be bi-directional.\n");
        has_err = TRUE;
      }
      if (p_ep->hs_trans_num != 0){
        MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a control pipe\n", p_pipe->pipe_id);
        MSG_Warn("The additional transaction per (micro)frame must be set to 0.\n");
        has_err = TRUE;
      }
      if (p_dev->speed == USB_TRANS_LS) {
        if (p_ep->max_packet_size != 8) {
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error( "Pipe #%d %s The maximum packet size must be 8-bytes and not %d.\n",
		       p_pipe->pipe_id, "is a Low-speed control pipe\n", p_ep->max_packet_size);
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
      }
      else if (p_dev->speed == USB_TRANS_FS){
        if(p_ep->max_packet_size > 64) {
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error( "Pipe #%d %s The maximum packet size must be less than 64-bytes and not %d.\n",
		       p_pipe->pipe_id, "is a Full-speed control pipe\n", p_ep->max_packet_size);
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
      }
      else if (p_dev->speed == USB_TRANS_HS) {
      	if(p_ep->max_packet_size != 64) {
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error( "Pipe #%d %s The maximum packet size must be 64-bytes and not %d.\n",
		       p_pipe->pipe_id, "is a High-speed control pipe\n", p_ep->max_packet_size);
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
     } 
     break;
    }
    //-------------------------- step 5 ---------------------------
    case USB_EPTYPE_ISO: {
    //
    //+-------------------------------------------
    //|Check isochronous transfer
    //|-------------------------------------------
    //|  synchonous type must be [1, 3]
    //|  usage type must be [0, 2]
    //|  interval must be [1, 16]
    //|Full-speed:
    //|  high-speed transfer number must be 0
    //|  max_packet_size <= 1023 bytes
    //|High-speed:
    //|  high-speed transfer number must be [0, 2]
    //|  max_packet_size <= 1024 bytes
    //+-------------------------------------------
    //
      if ( (p_ep->sync_type > 3)||(p_ep->sync_type < 1) ) {
        MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a isochronous pipe\n", p_pipe->pipe_id);
        MSG_Warn("The synchronous type must be from 1 to 3.\n");
        has_err = TRUE;
      }
      if (p_ep->usage_type > 2) {
        MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a isochronous pipe\n", p_pipe->pipe_id);
        MSG_Warn("The usage type must be from 0 to 2.\n");
        has_err = TRUE;
      }
      if ( (p_ep->interval < 1)||(p_ep->interval > 16) ) {
        MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a isochronous pipe\n", p_pipe->pipe_id);
        MSG_Warn("The interval value must be from 1 to 16.\n");
        has_err = TRUE;
      }
      //else p_ep->interval = ( pow(2, (p_ep->interval-1)) ); //@@@RZ Should not change interval

      if (p_dev->speed == USB_TRANS_FS)  {
        if (p_ep->max_packet_size > 1023) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a FULL-speed isochronous pipe\n", p_pipe->pipe_id);
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error("The maximum packet size must be less than 1023-bytes.\n");
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
        if (p_ep->hs_trans_num != 0){
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a Full-speed isochronous pipe\n", p_pipe->pipe_id);
          MSG_Warn("The additional transaction per (micro)frame must be set to 0.\n");
          has_err = TRUE;
        }
      }
      else if (p_dev->speed == USB_TRANS_HS)  {
        if (p_ep->max_packet_size > 1024) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a HIGH-speed isochronous pipe\n", p_pipe->pipe_id);
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error("The maximum packet size must be less than 1024-bytes.\n");
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
        if(p_ep->hs_trans_num > 2){
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a High-speed isochronous pipe\n", p_pipe->pipe_id);
          MSG_Warn("The additional transaction per (micro)frame must be from 0 to 2.\n");
          has_err = TRUE;
        }
      }
      break;
    }
    //-------------------------- step 6 ---------------------------
    case USB_EPTYPE_INTR: {
    //
    //+-------------------------------------------
    //|Check interrupt transfer
    //|-------------------------------------------
    //|  synchonous type and usage type must be 0
    //|Low-speed:
    //|  high-speed transfer number must be 0
    //|  max_packet_size <= 8 bytes
    //|  interval must be [10, 255]
    //|Full-speed:
    //|  high-speed transfer number must be 0
    //|  max_packet_size <= 64 bytes
    //|  interval must be [1, 255]
    //|High-speed:
    //|  high-speed transfer number must be [0, 2]
    //|  max_packet_size <= 1024 bytes
    //|  interval must be [1, 16]
    //+-------------------------------------------
    //
      if (p_dev->speed == USB_TRANS_LS) {
        if (p_ep->max_packet_size > 8) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a LOW-speed interrupt pipe\n ", p_pipe->pipe_id);
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error("The maximum packet size must be less than 8-bytes.\n");
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
        if ( (p_ep->interval < 10)||(p_ep->interval > 255) ) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a LOW-speed interrupt pipe\n%s", p_pipe->pipe_id,
		   "The interval value must be from 10 to 255.\n");
          has_err = TRUE;
        }
        if (p_ep->hs_trans_num != 0){
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a LOW-speed interrupt pipe\n%s", p_pipe->pipe_id,
		   "The additional transaction per (micro)frame must set to 0.\n");
          has_err = TRUE;
        }
      }
      else if (p_dev->speed == USB_TRANS_FS) {
        if ( (p_ep->interval < 1)||(p_ep->interval > 255) ) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a FULL-speed interrupt pipe\n%s", p_pipe->pipe_id,
		   "The interval value must be from 1 to 255.\n");
          has_err = TRUE;
        }
        if (p_ep->max_packet_size > 64) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a FULL-speed interrupt pipe\n", p_pipe->pipe_id);
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error("The maximum packet size must be less than 64-bytes.\n");
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
        if (p_ep->hs_trans_num != 0){
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a Full-speed interrupt pipe.\n %s", p_pipe->pipe_id,
		   "The additional transaction per (micro)frame must set to 0.");
          has_err = TRUE;
        }
      }
      else if (p_dev->speed == USB_TRANS_HS) {
        if ((p_ep->interval < 1)||(p_ep->interval > 16)) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a HIGH-speed interrupt pipe\n%s", p_pipe->pipe_id,
		   "The interval value must be from 1 to 16.\n" );
          has_err = TRUE;
        }
        else p_ep->interval = ( pow(2, (p_ep->interval-1)) );

        if (p_ep->max_packet_size > 1024) {
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a HIGH-speed interrupt pipe\n", p_pipe->pipe_id);
	  if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	    MSG_Error("The maximum packet size must be less than 1024-bytes.\n");
	    has_err = TRUE;
	  }
	  else MSG_Printf("ERROR detection disabled\n");
        }
        if(p_ep->hs_trans_num > 2){
          MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a High-speed interrupt pipe\n%s", p_pipe->pipe_id,
		   "The additional transaction of per (micro)frame must be from 0 to 2.\n" );
          has_err = TRUE;
        }
      }
      break;
    }
    //-------------------------- step 7 ---------------------------
    case USB_EPTYPE_BULK: {
    //
    //+-------------------------------------------
    //|Check bulk transfer
    //|-------------------------------------------
    //|  synchonous type and usage type must be 0
    //|  high-speed transfer number must be 0
    //|Full-speed:
    //|  max_packet_size <= 64 bytes
    //|High-speed:
    //|  max_packet_size <= 512 bytes
    //|  OUT direction:interval must be [0, 255]
    //+-------------------------------------------
    //
      if (p_ep->hs_trans_num != 0){
        MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d is a bulk pipe\n", p_pipe->pipe_id);
        MSG_Warn("The additional transaction per (micro)frame must set to 0.\n");
        has_err = TRUE;
      }
     if (p_dev->speed == USB_TRANS_FS)  {
       if (p_ep->max_packet_size > 64) {
         MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a FULL-speed bulk pipe\n ", p_pipe->pipe_id);
	 if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	   MSG_Error("The maximum packet size must be less than 64-bytes not %d.\n",p_ep->max_packet_size);
	   has_err = TRUE;
	 }
	 else MSG_Printf("ERROR detection disabled\n");
       }
     }
     else if (p_dev->speed == USB_TRANS_HS)  {
       if (p_ep->max_packet_size > 512) {
         MSG_Warn("Pipe #%d pipe is HIGH-speed a bulk pipe\n", p_pipe->pipe_id);
	 if(p_top->ErrorMsgDisabled.packet_max_size_exceeded != 1) {
	   MSG_Error("The maximum packet size must be less than 512-bytes.\n");
	   has_err = TRUE;
	 }
	 else MSG_Printf("ERROR detection disabled\n");
       }
       if ( (p_ep->interval > 255)&&(p_ep->direction == USB_DIR_OUT) ) {
         MSG_Warn("USB_CHK_PipeProperties(): Pipe #%d pipe is a HIGH-speed bulk OUT direction pipe\n", p_pipe->pipe_id);
         MSG_Warn("The Interval value must be in the range of 0 to 255.\n");
         has_err = TRUE;
       }
     }
     break;
    }
    default: break;
  }
  
  return(has_err);
}



//%FS===========================================================================
// USB_CHK_BusTimeCal
// 
// Description:
//   This function calculates the bus time required for sending a transaction from a pipe.
//   Reference to USB 2.0 spec P64.
//
// Parameters:
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the specific pipe being calculated.
//   u_int32   data_bc    I
//     It points to the byte count of data payload.
//
// Return:
//   u_int32
//     If OK, the time that a pipe needs will be returned.
//     If error, zero will be returned, and the warning message will be reported.
//%FE===========================================================================
u_int32 USB_CHK_BusTimeCal (USB_PipeDescr_t*  p_pipe, u_int32  data_bc ){ 

  USB_DevProfile_t*  p_dev;
  double             time=0.0;
  
  if(p_pipe == NULL){
    MSG_Panic("USB_CHK_BusTimeCal(): Input pipe pointer cannot be NULL.\n");
    return(0);
  }
  
  p_dev  = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

  switch (p_dev->speed) {
    case USB_TRANS_HS: {
      if (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO) {
        time = (38*8*2.083)+
               (2.083*(int)(3.167+USB_BITSTUFFTIME*data_bc))+USB_HOST_DELAY;
      }
      if (p_pipe->p_endpt_descr->endpt_type != USB_EPTYPE_ISO) {
        time = (55*8*2.083)+
                (2.083*(int)(3.167+USB_BITSTUFFTIME*data_bc))+USB_HOST_DELAY;
      }
      break;
    }
    case USB_TRANS_FS: {
      if (p_pipe->p_endpt_descr->endpt_type != USB_EPTYPE_ISO) {
        time = 9107+(83.54*(int)(3.167+USB_BITSTUFFTIME*data_bc))+USB_HOST_DELAY;
      }
      if (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO) {
        time = 7268+(2.083*(int)(3.167+USB_BITSTUFFTIME*data_bc))+USB_HOST_DELAY;
      }
      break;
    }
    case USB_TRANS_LS: {
      if (p_pipe->p_endpt_descr->direction == USB_DIR_IN) {
          time = 64060+(2*USB_HUB_LS_SETUP)+
                    (676.67*(int)(3.167+USB_BITSTUFFTIME*data_bc))+USB_HOST_DELAY;
      }
      if (p_pipe->p_endpt_descr->direction == USB_DIR_OUT) {
        time = 64107+(2*USB_HUB_LS_SETUP)+
                    (667*(int)(3.167+USB_BITSTUFFTIME*data_bc))+USB_HOST_DELAY;
      }
      if (p_pipe->p_endpt_descr->direction == USB_DIR_BI) {
        time = 64107+(2*USB_HUB_LS_SETUP)+
                    (676.67*(int)(3.167+USB_BITSTUFFTIME*data_bc))+USB_HOST_DELAY;
      }
      break;
    }
    default: break;
  }
  return( ((u_int32)time) );
}




//%FS===========================================================================
// USB_CHK_TotalBw
//
// Description:
//   This function calculates the total bandwidth of a (micro)frame.
//
// Parameters:
//   USB_TransSpeed_e  speed   I
//     It points to the current operation speed.
//
// Return:
//   u_int32
//     If high-speed, a microframe bit time will be returned.
//     Otherwise, a frame bit time will be returned.
//%FE===========================================================================
u_int32 USB_CHK_TotalBw(USB_TransSpeed_e  speed){
  u_int32           cnt_bw = 0;
 
  if( speed == USB_TRANS_HS ){
    cnt_bw = (USB_HS_TIMER - USB_HS_SOP - USB_HS_SOF_EOP - USB_HS_EOF)*2.083;
  }
  else if( speed == USB_TRANS_FS ){
    cnt_bw = (USB_FS_TIMER - USB_FS_SOP - USB_FS_EOP - USB_FS_EOF)*83.54;
  }
  else if( speed == USB_TRANS_LS ){
    cnt_bw = (USB_FS_TIMER - USB_FS_SOP - USB_FS_EOP - USB_FS_EOF)*676.67;
  }
  
  cnt_bw -= 24;				// SOF token_length
  
  return(cnt_bw);
}





//%FS===========================================================================
// USB_CHK_GetIsoBw
// 
// Description:
//   This function calculates the bandwidth of all isochronous transfer pipes.
//
// Parameters:
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the specific pipe being checked.
//
// Return:
//   u_int32
//     If OK, all ISO pipe occupy bit time will be returned.  //FIXME
//     Otherwise, zero will be returned, the warning message will be report.
//%FE===========================================================================
u_int32 USB_CHK_GetIsoBw(USB_PipeDescr_t*  p_pipe){
  u_int32           iso_bw = 0;
  u_int32           i;
  u_int32           pipe_cnt;
  USB_PipeDescr_t*  p_chk;

  if(p_pipe == NULL){
    MSG_Panic("USB_CHK_GetIsoBw(): Input pipe pointer cannot be NULL.\n");
    return(0);
  }
    
  pipe_cnt = USB_PIPE_PA_GetPipeCount();
  for (i=1;i<pipe_cnt;i++){
    if(p_pipe->pipe_id != i){
      p_chk = carbonXUsbGetPipeDescr(i);
      if(p_chk == NULL) continue;
      if(p_chk->p_endpt_descr->endpt_type != USB_EPTYPE_ISO) continue;
 
      if( USB_CHK_StateUse(p_chk) ){
        iso_bw += USB_CHK_PeriodCurBw(p_chk,p_pipe);
      }
    }
  }
  return(iso_bw);
}

//%FS===========================================================================
// USB_CHK_GetIntrBw
// 
// Description:
//   This function calculates the bandwidth of all interrupt transfer pipes.
//
// Parameters:
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the specific pipe being checked.
//
// Return:
//   u_int32
//     If OK, all interrupt pipe occupy bit time will be returned.
//     Otherwise, zero will be returned, the warning message will be report.
//%FE===========================================================================
u_int32 USB_CHK_GetIntrBw(USB_PipeDescr_t*  p_pipe){
  u_int32           intr_bw = 0;
  u_int32           i;
  u_int32           pipe_cnt;
  USB_PipeDescr_t*  p_chk;

  if(p_pipe == NULL){
    MSG_Panic("USB_CHK_GetIntrBw(): Input pipe pointer cannot be NULL.\n");
    return(0);
  }
  
  pipe_cnt = USB_PIPE_PA_GetPipeCount();
  for (i=1;i<pipe_cnt;i++){
    if(p_pipe->pipe_id != i){
      p_chk = carbonXUsbGetPipeDescr(i);
      if(p_chk == NULL) continue;
      if(p_chk->p_endpt_descr->endpt_type != USB_EPTYPE_INTR) continue;
      
      if( USB_CHK_StateUse(p_chk) ){
        intr_bw += USB_CHK_PeriodCurBw(p_chk,p_pipe);
      }
    }
  }
  return(intr_bw);
}

//%FS===========================================================================
// USB_CHK_PeriodCurBw
//
// Description:
//   This function calculates the period pipe bandwidth.              
//   1. If the period is one, the bandwidth needs to be calculated.
// Parameter:
//   USB_PipeDescr_t*   p_chk    I
//     It points to the specific pipe being that exists.
//   USB_PipeDescr_t*   p_pipe   I
//     It points to the specific pipe being added.
//
// Return:
//   u_int32
//     If OK, the p_pipe bandwidth will be returned.
//     If the pipe does not occupy the current frame bit time, the zero will 
//     be return.
//%FE===========================================================================
u_int32 USB_CHK_PeriodCurBw(USB_PipeDescr_t* p_chk, USB_PipeDescr_t* p_pipe){
  
  //--------------------- step 1 --------------------------------
  if(p_chk->p_endpt_descr->interval == 1){
    return( USB_CHK_MultiTransBw(p_chk) );
  }
  
  //--------------------- step 2 --------------------------------
  else if (p_chk->p_endpt_descr->interval<= p_pipe->p_endpt_descr->interval){
    if( ((p_chk->sys_record.uframe_sched - p_pipe->sys_record.uframe_sched)
        %p_chk->p_endpt_descr->interval ) == 0 ){
      return( USB_CHK_MultiTransBw(p_chk) );
    }
  }

  else if( ((p_chk->sys_record.uframe_sched - p_pipe->sys_record.uframe_sched)
        %p_pipe->p_endpt_descr->interval ) == 0 ){
    return( USB_CHK_MultiTransBw(p_chk) );
  }
  
  return(0);
}


//%FS===========================================================================
// USB_CHK_StateUse
//
// Description:
//   This function checks if the specific pipe is used or not.
//
// Parameter:
//   USB_PipeDescr_t*   p_pipe   I
//     It points to the specific pipe.
//
// Return:
//   If TRUE, the specific pipe is used and occupies the bandwidth.
//   If FALSE, the specific pipe does not occupy the bandwidth.
//%FE===========================================================================
BOOL USB_CHK_StateUse(USB_PipeDescr_t* p_pipe){
  return ( (p_pipe->sys_record.pipe_state != USB_PIPE_STAT_NEED_SETUP)
       &&(p_pipe->sys_record.pipe_state != USB_PIPE_STAT_SETUP_FAIL) );
}


//%FS===========================================================================
// USB_CHK_BandWidth
//
// Description:
//   This function checks if the current bandwidth is enough for a new pipe or not.
//   1. Calculate bandwidth that the new pipe needs, the existing occupied
//      bandwidth, and the total bandwidth.
//   2. Calculate the period bandwidth upper limit.
//   3. Check current bandwidth is enough or not.
//
// Parameters:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe being added.
//   u_int32  data_bc   I
//     It points to the bytes count of data payload.
//
// Return:
//   If TRUE, the bandwidth is enough for the new pipe.
//   If FALSE, the bandwidth is not enough.
//%FE===========================================================================
BOOL USB_CHK_BandWidth (USB_PipeDescr_t*  p_pipe,u_int32  data_bc){    
  u_int32           need_bw;		// Request bandwidth for 1 transaction
  u_int32           iso_bw;
  u_int32           intr_bw;
  u_int32           cnt_bw;
  u_int32           temp;

  (void)data_bc;
  if( (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL)||
      (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_BULK) ) {
      return(TRUE);
  }

  if(p_pipe == NULL){
    MSG_Panic("USB_CHK_BandWidth(): Input pipe pointer is NULL. Bandwidth check failed.\n");
    return(FALSE);
  }
  
  //--------------------- step 1 --------------------------------
  need_bw = USB_CHK_MultiTransBw(p_pipe);
  
  cnt_bw = USB_CHK_TotalBw((carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id))->speed);
  
  iso_bw  = USB_CHK_GetIsoBw(p_pipe);
  intr_bw = USB_CHK_GetIntrBw(p_pipe);

  //--------------------- step 2 --------------------------------
  if ((carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id))->speed == USB_TRANS_HS){
    if( (iso_bw + intr_bw + need_bw) > (cnt_bw * 0.8) ){
      MSG_Warn("High-speed endpoints can be allocated at maximum 80%% of a microframe for periodic transfer.\n");
      return(FALSE);
    }
  }
  else {
       
    temp    = (u_int32)(iso_bw/40 + 1);
    iso_bw  = temp;
    
    temp    = (u_int32)(intr_bw/40 + 1);
    intr_bw = temp;
    
    temp    = (u_int32)(need_bw/40 + 1);
    need_bw = temp;
    
    if( (iso_bw + intr_bw + need_bw) > (cnt_bw * 0.9) ){
      MSG_Warn("Full/Low-speed endpoints can be allocated at maximum 90%% of a microframe for periodic transfer.\n");
      return(FALSE);
    }
  }
  
  //--------------------- step 3 --------------------------------
  if (need_bw <= (cnt_bw - iso_bw - intr_bw)) {
    return(TRUE); 
  }
  else {
    MSG_Warn("The bandwidths are NOT enough.");
    return(FALSE);
  }
  
}//USB_CHK_BandWidth




//%FS===========================================================================
// USB_CHK_MultiTransBw
// 
// Descirption:
//   This function calculates multi-transaction bandwidth per (micro)frame.
//
// Parameters:
//   USB_PipeDescr_t* p_pipe  I
//     It points to specific pipe.
//
// Return:
//   u_int32
//     The multi-transaction bandwidth will be returned.
//%FE===========================================================================
u_int32 USB_CHK_MultiTransBw(USB_PipeDescr_t* p_pipe){
  return( USB_CHK_BusTimeCal(p_pipe,p_pipe->p_endpt_descr->max_packet_size * (p_pipe->p_endpt_descr->hs_trans_num + 1) ));
}


//%FS===========================================================================
// USB_CalcEstSofBitTime
// 
// Descirption:
//   This function calculates SOF bit time.
//
// Parameters:
//   USB_TransSpeed_e speed I
//     It points to the device operated speed.
//
// Return:
//   u_int32
//     The SOF bit time will be returned.
//
// Note:
//   If low-speed, the SOF bit time is full-speed bit time.
//%FE===========================================================================
u_int32 USB_CalcEstSofBitTime(USB_TransSpeed_e speed){

  u_int32 exp_bit_time = 0;

  if( speed == USB_TRANS_HS){
      exp_bit_time = USB_HS_SOP + USB_HS_SOF_EOP + 24;
    }
    else {
      exp_bit_time = USB_FS_SOP + USB_FS_EOP + 24;
    }
    return(exp_bit_time);
}

// Interpacket delay: Reference USB 2.0 spec, page 168
//
//+-----------------------------------------------------------
//| Control
//| (Reference USB 2.0 spec,  
//|  page 41 Table 5-1, 
//|  page 42 Table 5-2,
//|  page 43 Table 5-3,
//|  Protocol Overhead)
//|  Setup: Setup  token  + DATA0   + Handshake
//|  data:  OUT/IN token + DATA0/1 + Handshake
//+-----------------------------------------------------------
//| Low-speed:
//|   Total Protocol Overhead: 63 Bytes ( 50 Bytes without interpacket delay )
//|   15 SYNC bytes, 15 PID bytes, 6 Endpoint + CRC bytes,
//|   6 CRC bytes, 8 Setup data bytes, and a 13-byte interpacket delay (EOP, etc.)
//|   Note: the difference with respect to Full-speed transaction is due to the Preamble
//| Full-speed:
//|   Total Protocol Overhead: 45 Bytes (38 Bytes without interpacket delay ) 
//|   9 SYNC bytes, 9 PID bytes, 6 Endpoint + CRC bytes,
//|   6 CRC bytes, 8 Setup data bytes, and a 7-byte interpacket delay (EOP, etc.)
//| High-speed:
//|   Total Protocol Overhead: 173 Bytes ( 65 Bytes without interpacket delay )
//|   Based on 480Mb/s and 8 bit interpacket gap, 88 bit min bus turnaround, 32 bit sync, 8 bit EOP:
//|   9*4 SYNC bytes, 9 PID bytes, 6 EP/ADDR+CRC,6 CRC16, 8 Setup data, 9*(1+11) byte interpacket delay (EOP, etc.)
//+-----------------------------------------------------------
//| ISO
//| (Reference USB 2.0 spec,  
//|  page 45 Table 5-4, 
//|  page 46 Table 5-5,
//|  Protocol Overhead)
//|  OUT/IN token + DATAx
//+-----------------------------------------------------------
//| Low-speed:
//|   Not applicable
//| Full-speed:
//|   Total Protocol Overhead: 9 Bytes ( 8 Bytes without interpacket delay )
//|   2 SYNC bytes, 2 PID bytes, 2 Endpoint + CRC bytes, 2 CRC bytes, and a 1-byte interpacket delay
//| High-speed: 
//|   Total Protocol Overhead: 38 Bytes ( 14 Bytes without interpacket delay )
//|   Based on 480Mb/s and 8 bit interpacket gap, 88 bit min bus turnaround, 32 bit sync, 8 bit EOP:
//|   2*4 SYNC bytes, 2 PID bytes, 2 EP/ADDR+addr+CRC5, 2 CRC16, and a 2*(1+11)) byte interpacket delay (EOP, etc.)
//+-----------------------------------------------------------
//| Interrupt
//| (Reference USB 2.0 spec,  
//|  page 49 Table 5-6, 
//|  page 50 Table 5-7,
//|  page 51 Table 5-8,
//|  Protocol Overhead)
//|  OUT/IN token + DATA0/1 + Handshake
//+-----------------------------------------------------------
//| Low-speed: 
//|   Total Protocol Overhead: 19 Bytes ( 14 Bytes without interpacket delay )
//|   5 SYNC bytes, 5 PID bytes, 2 Endpoint + CRC bytes, 2 CRC bytes, and a 5-byte interpacket delay
//|   Note: the difference with respect to Full-speed transaction is due to the Preamble
//| Full-speed:
//|   Total Protocol Overhead: 13 Bytes ( 10 Bytes without interpacket delay )
//|   3 SYNC bytes, 3 PID bytes, 2 Endpoint + CRC bytes, 2 CRC bytes, and a 3-byte interpacket delay
//| High-speed:  
//|   Total Protocol Overhead: 55 Bytes ( 19 Bytes without interpacket delay )
//|   Based on 480Mb/s and 8 bit interpacket gap, 88 bit min bus turnaround, 32 bit sync, 8 bit EOP:
//||  3*4 SYNC bytes, 3 PID bytes, 2 EP/ADDR+CRC bytes, 2 CRC16 and a 3*(1+11) byte interpacket delay (EOP, etc.)
//+-----------------------------------------------------------
//|Bulk
//| (Reference USB 2.0 spec,  
//|  page 54 Table 5-9, 
//|  page 55 Table 5-10,
//|  Protocol Overhead)
//|  3 Packet transfers: OUT/IN, DATAX, HandShake
//+-----------------------------------------------------------
//| Full-speed:
//|   Total Protocol Overhead: 13 bytes ( 10 Bytes without interpacket delay )
//|   3 SYNC bytes, 3 PID bytes, 2 EP/CRC bytes, 2 CRC16 bytes, 3 InterPacketDelay bytes
//| High-speed:
//|   Total Protocol Overhead 55 bytes ( 19 Bytes without interpacket delay )
//|   3 * 4 SYNC bytes, 3 PID bytes, 2 EP/CRC bytes, 2 CRC16 bytes, 3*(1+11) InterPacketDelay bytes
//+-----------------------------------------------------------
//
// overhead_bytes stores the overhead bytes for each type of transaction (USB_TransType_e)
//                and each type of speed (not including interpacket delay bytes)
//  Transfer type
//  	0   Control
//	1   Isochronous
//	2   Bulk
//	3   Interrupt
//
//      LS = 0
//      FS = 1
//      HS = 2
//                                  CTRL: LS  FS   HS  ISO :NA  FS  HS   INT: LS FS  HS  BLK: NA FS  HS
int overhead_bytes[4][3]        = {     { 50, 38,  65 },  {  0,  8, 14 },  { 14, 10, 19 },  { 0, 10, 19} };
int inter_pkt_delay_bytes[4][3] = {     { 13,  7, 108 },  {  0,  1, 24 },  {  5,  3, 36 },  { 0,  3, 36} };

//%FS===========================================================================
// USB_CHK_CalcExpBitTime
// 
// Description:
//   This function calculates a transaction bit time.
//
// Parameters:
//   BOOL is_ping  I
//     Indicates if it is a ( Ping) token packet or not.
//   u_int32 data_length  I
//     Transaction data payload size.
//   USB_PipeDescr_t*  p_pipe  I
//     Pointer to the pipe that is associated with the transaction
//
// Return:
//   u_int32
//     The transaction bit time will be returned.
//
// Note:
//   If low-speed, the transaction bit time is full-speed bit time.
//
//   Reference USB 2.0 spec page 307, Section 11.3.3, paragraph 4, line 5
//
//   The 7/6 multiplier accounts for the worst case bit-stuffing 
//   and the inter_pkt allow for worst case turn-around.
//
// Known problems:
//   @@@RZ Split transactions do not get handled correctly
//         Currently cable delays and hub delays are not taken into account (USB 2.0 p. 168)
//%FE===========================================================================
u_int32 USB_CHK_CalcExpBitTime(BOOL is_ping, u_int32 data_length, USB_PipeDescr_t*  p_pipe){
  USB_DevProfile_t* p_dev;
  u_int32           exp_bit_time;
  u_int32           overhead_bits; 
  u_int32           inter_pkt_delay_bits;
  u_int32           traffic_type_idx;
  u_int32           speed_idx;
  //  TRANS_HANDLE_T    transactor_hdl;
  
  if(p_pipe == NULL){
    MSG_Warn("USB_CHK_CalcExpBitTime(): Input pipe #%d does not exist.\n",p_pipe->pipe_id);
    return(0);
  }

  //  transactor_hdl = PLAT_GetHandleFromTransTable();

  p_dev  = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
  
#ifdef VERBOSE_DEBUG
  //  MSG_LibDebug( transactor_hdl, MSG_TRACE,
  //		"<%.10lld> USB_CHK_CalcExpBitTime: is_ping=%d, data_length=%d\n",
  //		PLAT_GetSimTime(), is_ping, data_length
  //		);
#endif

  if(is_ping){
    //   PING + Handshake
    //   2 * 4 SYNC bytes, 2 PID bytes, 2 EP/CRC bytes, 2*(1+11) InterPacketDelay bytes
    overhead_bits        = 12 * 8; 
    inter_pkt_delay_bits = 24 * 8;

  } // end if(is_ping)
  else {

    traffic_type_idx = p_pipe->p_endpt_descr->endpt_type;
    speed_idx = p_dev->speed -1;

    overhead_bits        = overhead_bytes[traffic_type_idx][speed_idx] * 8; 
    inter_pkt_delay_bits = inter_pkt_delay_bytes[traffic_type_idx][speed_idx] * 8;
  }

  exp_bit_time = ( data_length *8 + overhead_bits ) *7/6 + inter_pkt_delay_bits;

  //
  // Reference USB 2.0 page 307, Section 11.3.3, paragraph 4, line 5
  //
  // The 7/6 multiplier accounts for the worst case bit-stuffing

#ifdef VERBOSE_DEBUG
  //  MSG_LibDebug( transactor_hdl, MSG_TRACE,
  //		"<%.10lld> USB_CHK_CalcExpBitTime: exp_bit_time=%d\n", PLAT_GetSimTime(), exp_bit_time
  //		);
#endif
 
  return(exp_bit_time);
}


//%FS===========================================================================
// USB_CHK_DevExist
//
// Description:
//   This function checks if the device exists or not.
//
// Parameters:
//   u_int32 dev_id  I
//     It points to the device ID being check.
//
// Return:
//   If TRUE, the device is existed.
//   If FALSE, the device is not existed.
//%FE===========================================================================
BOOL USB_CHK_DevExist(u_int32 dev_id){
  USB_HostCtrlTop_t* p_top;
  u_int32    i;
  
  if(dev_id >= USB_MAX_DEV_NUMBER){
    return(FALSE);
  }
  if(dev_id == 0){
    MSG_Warn("USB_CHK_DevExist(): Device #0 is reserved.\n");
    return(FALSE);
  }
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  
  for(i=1;i<USB_MAX_DEV_NUMBER;i++){
    if( p_top->a_dev_prof[i].dev_id == dev_id){
      return(TRUE);
    }
  }
  MSG_Warn("USB_CHK_DevExist(): Cannot Find the device #%d.\n",dev_id);
  return(FALSE);
}



//%FS===========================================================================
// USB_CHK_HsIsoOutSplit
//
// Description:
//   This function checks if the specific pipe is high-speed isochronous OUT 
//   direction and using start-split transfer.
//
// Parameters:
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//
// Return:
//   If TRUE, the pipe is high-speed isochronous OUT transfer using start-split.
//   If FALSE, the pipe is no.
//%FE===========================================================================
BOOL USB_CHK_HsIsoOutSplit(USB_PipeDescr_t* p_pipe){
  
  USB_DevProfile_t* p_dev;
  p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
  if(p_dev == NULL) return (FALSE);
  
  return( (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO)
          && (p_dev->speed == USB_TRANS_HS)
          && (p_pipe->p_endpt_descr->curr_dir == USB_DIR_OUT)
          && (p_dev->is_split) );
}


//%FS===========================================================================
// USB_CHK_UserIsCfg
//
// Description:
//   This function checks if the endpoint has been set 
//   in the user configuration by user or not.
//
// Parameters:
//   USB_EndPtDescr_t* p_endpt_descr  I
//     It points to the specific endpoint descriptor.
//
// Return:
//   If TRUE, the endpoint has been set by user.
//   If FALSE, the endpoint has not been set.
//
// Note:
//   This function only check the standard descriptor in the device profile.
//%FE===========================================================================
BOOL USB_CHK_UserIsCfg(USB_EndPtDescr_t* p_endpt_descr){
  USB_DevProfile_t*  p_dev;
  USB_StdEpDescr_t*  p_stdep = 0;
  
  BOOL    has_set_user = TRUE;
  u_int32 i;
  u_int32 num;
  u_int32 direct;
  u_int32 trans_type;
  u_int32 sync_type;
  u_int32 usage_type;
  u_int32 max_pk_size;
  u_int32 hs_trans_num;
  u_int32 interval;
  
  p_dev = carbonXUsbGetDevProf(p_endpt_descr->dev_id);
  
  for(i=0;i<USB_MAX_EP_DESCR_NUMBER;i++){
    num = p_dev->user_cfg.a_stdep[i].b_ep_addr;
    num &= 15;
    if(num == 0) continue;

    if(num == p_endpt_descr->endpt_num){
      p_stdep = &p_dev->user_cfg.a_stdep[i];
      break;
    }
  }
  
  direct       = p_stdep->b_ep_addr & 0x80;
  trans_type   = p_stdep->bm_attr & 0x3;
  sync_type    = p_stdep->bm_attr & 0xc;
  usage_type   = p_stdep->bm_attr & 0x30;
  max_pk_size  = p_stdep->w_max_pkt_size & 0x7ff;
  hs_trans_num = p_stdep->w_max_pkt_size & 0x1800;
  interval     = p_stdep->b_interval;
  
    switch(trans_type){
    case 0:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_CTRL){
      	has_set_user = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_user = FALSE;
      }
      break;
    }
    case 1:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_ISO){
      	has_set_user = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_user = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_OUT){
      	if(direct != 1) has_set_user = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_IN){
      	if(direct != 0) has_set_user = FALSE;
      }
      else if(p_endpt_descr->hs_trans_num != hs_trans_num){
      	has_set_user = FALSE;
      }
      else if(p_endpt_descr->interval != interval){
      	has_set_user = FALSE;
      }
      else if(p_endpt_descr->sync_type == USB_SYNC_TYPE_ASYNC){
      	if(sync_type != 1) has_set_user = FALSE;
      }
      else if(p_endpt_descr->sync_type == USB_SYNC_TYPE_ADAPT){
      	if(sync_type != 2) has_set_user = FALSE;
      }
      else if(p_endpt_descr->sync_type == USB_SYNC_TYPE_SYNC){
      	if(sync_type != 3) has_set_user = FALSE;
      }
      else if(p_endpt_descr->usage_type == USB_USG_TYPE_DATA){
      	if(usage_type != 0) has_set_user = FALSE;
      }
      else if(p_endpt_descr->usage_type == USB_USG_TYPE_FD){
      	if(usage_type != 1) has_set_user = FALSE;
      }
      else if(p_endpt_descr->usage_type == USB_USG_TYPE_IMPLICIT){
      	if(usage_type != 2) has_set_user = FALSE;
      }
      break;
    }
    case 2:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_BULK){
      	has_set_user = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_user = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_OUT){
      	if(direct != 1) has_set_user = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_IN){
      	if(direct != 0) has_set_user = FALSE;
      }
      break;
    }
    case 3:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_INTR){
      	has_set_user = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_user = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_OUT){
      	if(direct != 1) has_set_user = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_IN){
      	if(direct != 0) has_set_user = FALSE;
      }
      else if(p_endpt_descr->hs_trans_num != hs_trans_num){
      	has_set_user = FALSE;
      }
      else if(p_endpt_descr->interval != interval){
      	has_set_user = FALSE;
      }
      break;
    }
    default: break;
  }
  
  return(has_set_user);
  
}  


//%FS===========================================================================
// USB_CHK_UserIsCfg
//
// Description:
//   This function checks if the endpoint has been set 
//   in the dut configuration or not.
//
// Parameters:
//   USB_EndPtDescr_t* p_endpt_descr  I
//     It points to the specific endpoint descriptor.
//
// Return:
//   If TRUE, the endpoint has been set in the dut.
//   If FALSE, the endpoint has not been set.
//
// Note:
//   This function only check the standard descriptor in the device profile.
//%FE===========================================================================
BOOL USB_CHK_DutIsCfg(USB_EndPtDescr_t* p_endpt_descr){
  USB_DevProfile_t*  p_dev;
  USB_StdEpDescr_t*  p_stdep = 0;
  
  BOOL    has_set_dut = TRUE;
  u_int32 i;
  u_int32 num;
  u_int32 direct;
  u_int32 trans_type;
  u_int32 sync_type;
  u_int32 usage_type;
  u_int32 max_pk_size;
  u_int32 hs_trans_num;
  u_int32 interval;
   
  p_dev = carbonXUsbGetDevProf(p_endpt_descr->dev_id);
  
  for(i=0;i<USB_MAX_EP_DESCR_NUMBER;i++){
    num = p_dev->dut_model.a_stdep[i].b_ep_addr;
    num &= 15;
    if(num == 0) continue;

    if(num == p_endpt_descr->endpt_num){
      p_stdep = &p_dev->dut_model.a_stdep[i];
      break;
    }
  }
  
  direct       = p_stdep->b_ep_addr & 0x80;
  trans_type   = p_stdep->bm_attr & 0x3;
  sync_type    = p_stdep->bm_attr & 0xc;
  usage_type   = p_stdep->bm_attr & 0x30;
  max_pk_size  = p_stdep->w_max_pkt_size & 0x7ff;
  hs_trans_num = p_stdep->w_max_pkt_size & 0x1800;
  interval     = p_stdep->b_interval;
  
  switch(trans_type){
    case 0:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_CTRL){
      	has_set_dut = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_dut = FALSE;
      }
      break;
    }
    case 1:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_ISO){
      	has_set_dut = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_dut = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_OUT){
      	if(direct != 1) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_IN){
      	if(direct != 0) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->hs_trans_num != hs_trans_num){
      	has_set_dut = FALSE;
      }
      else if(p_endpt_descr->interval != interval){
      	has_set_dut = FALSE;
      }
      else if(p_endpt_descr->sync_type == USB_SYNC_TYPE_ASYNC){
      	if(sync_type != 1) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->sync_type == USB_SYNC_TYPE_ADAPT){
      	if(sync_type != 2) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->sync_type == USB_SYNC_TYPE_SYNC){
      	if(sync_type != 3) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->usage_type == USB_USG_TYPE_DATA){
      	if(usage_type != 0) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->usage_type == USB_USG_TYPE_FD){
      	if(usage_type != 1) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->usage_type == USB_USG_TYPE_IMPLICIT){
      	if(usage_type != 2) has_set_dut = FALSE;
      }
      break;
    }
    case 2:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_BULK){
      	has_set_dut = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_dut = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_OUT){
      	if(direct != 1) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_IN){
      	if(direct != 0) has_set_dut = FALSE;
      }
      break;
    }
    case 3:{
      if(p_endpt_descr->endpt_type != USB_EPTYPE_INTR){
      	has_set_dut = FALSE;
      }
      else if(p_endpt_descr->max_packet_size != max_pk_size){
        has_set_dut = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_OUT){
      	if(direct != 1) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->direction == USB_DIR_IN){
      	if(direct != 0) has_set_dut = FALSE;
      }
      else if(p_endpt_descr->hs_trans_num != hs_trans_num){
      	has_set_dut = FALSE;
      }
      else if(p_endpt_descr->interval != interval){
      	has_set_dut = FALSE;
      }
      break;
    }
    default: break;
  }
  
  return(has_set_dut);
}


//%FS===========================================================================
// USB_CHK_DevIsSame
//
// Description:
//   This function is for the bus enumeration to getting standard device 
//   descriptor. In the received device descriptor, the Vendor ID and the 
//   Product ID is same with the anyone of device profile, then the DUT can
//   do bus enumeration by the USB_DoEnum()                                           //FIXME 
//
//
// Parameters:
//   USB_DevProfile_t* p_dev   I
//     It points to the temporary device profile that have the received device
//     descriptor.
//
// Return:
//   u_int32
//     It points to the dev_id.
//%FE===========================================================================
u_int32 USB_CHK_DevIsSame(USB_DevProfile_t* p_dev){
  u_int32           i;
  u_int32           dev_id = 0;
  USB_DevProfile_t* p_chk_dev;
  
  for(i=1;i<USB_MAX_DEV_NUMBER;i++){
    p_chk_dev = carbonXUsbGetDevProf(i);
    if(p_chk_dev->dev_id == 0) continue;
    
    if( (p_dev->dut_model.stddev.id_vendor == p_chk_dev->user_cfg.stddev.id_vendor)
      &&(p_dev->dut_model.stddev.id_product == p_chk_dev->user_cfg.stddev.id_product) ){
      p_chk_dev->dut_model = p_dev->dut_model;
      p_chk_dev->dev_type  = p_dev->dev_type;
      
      dev_id = p_chk_dev->dev_id;
      break;
    }
  }
  
  return(dev_id);
}

//%FS===========================================================================
// USB_CHK_HubDescIsSame
//
// Description:
//   This function only checks if two hub standard descriptors are the same or not.
//
// Parameters:
//   USB_Descr_t* p_user_cfg  I
//     It points to the single the hub standard descriptor in the user 
//     configured file.
//   USB_Descr_t* p_dut_model  I
//     It points to the single the hub standard descriptor in the dut
//     configured file.
//
// Return:
//   If TRUE, the two Hub standard descriptor is same.
//   If FALSE, the two Hub standard descriptor is different.
//%FE===========================================================================
BOOL USB_CHK_HubDescIsSame(USB_Descr_t* p_user_cfg, USB_Descr_t* p_dut_model){
  u_int32 i;
  
  if(p_user_cfg->stdhub.b_descr_len != p_dut_model->stdhub.b_descr_len) {
    return(FALSE);
  }
  if(p_user_cfg->stdhub.b_descr_type != p_dut_model->stdhub.b_descr_type) {
    return(FALSE);
  }
  if(p_user_cfg->stdhub.b_num_ports != p_dut_model->stdhub.b_num_ports) {
    return(FALSE);
  }
  if(p_user_cfg->stdhub.w_hub_charact != p_dut_model->stdhub.w_hub_charact) {
    return(FALSE);
  }
  if(p_user_cfg->stdhub.b_pwron2good != p_dut_model->stdhub.b_pwron2good) {
    return(FALSE);
  }
  if(p_user_cfg->stdhub.b_hub_ctrl_cur != p_dut_model->stdhub.b_hub_ctrl_cur) {
    return(FALSE);
  }
  if(p_user_cfg->stdhub.b_hub_ctrl_cur != p_dut_model->stdhub.b_hub_ctrl_cur) {
    return(FALSE);
  }
  
  for(i=0;i<MAX_PORT_NUMBER;i++){
    if(p_user_cfg->stdhub.a_dev_removal[i] != p_dut_model->stdhub.a_dev_removal[i]) {
      return(FALSE);
    }
    if(p_user_cfg->stdhub.a_port_pwr_ctrl_mask[i] != p_dut_model->stdhub.a_port_pwr_ctrl_mask[i]) {
      return(FALSE);
    }
  }
  return(TRUE);
}

//%FS===========================================================================
// USB_CHK_DevDescIsSame
//
// Description:
//   This function only checks if the two device descriptors are the same or not.
//
// Parameters:
//   USB_Descr_t* p_user_cfg  I
//     It points to the single the hub standard descriptor in the user 
//     configured file.
//   USB_Descr_t* p_dut_model  I
//     It points to the single the hub standard descriptor in the dut
//     configured file.
//
// Return:
//   If TRUE, the two device standard descriptor is same.
//   If FALSE, the two device standard descriptor is different.
//%FE===========================================================================
BOOL USB_CHK_DevDescIsSame(USB_Descr_t* p_user_cfg, USB_Descr_t* p_dut_model){
  
  if(p_user_cfg->stddev.b_length != p_dut_model->stddev.b_length) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.b_descr_type != p_dut_model->stddev.b_descr_type) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.bcd_usb != p_dut_model->stddev.bcd_usb) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.b_dev_class != p_dut_model->stddev.b_dev_class) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.b_dev_sub_class != p_dut_model->stddev.b_dev_sub_class) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.b_dev_protocol != p_dut_model->stddev.b_dev_protocol) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.b_max_pkt_size != p_dut_model->stddev.b_max_pkt_size) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.id_vendor != p_dut_model->stddev.id_vendor) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.id_product != p_dut_model->stddev.id_product) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.bcd_dev != p_dut_model->stddev.bcd_dev) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.i_manu != p_dut_model->stddev.i_manu) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.i_product != p_dut_model->stddev.i_product) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.i_serial_num != p_dut_model->stddev.i_serial_num) {
    return(FALSE);
  }
  if(p_user_cfg->stddev.i_num_cfg != p_dut_model->stddev.i_num_cfg) {
    return(FALSE);
  }

  return(TRUE);
}




//%FS===========================================================================
// carbonXUsbCHK_CmpIrpDataWithBuddy
//
// Description:
//   This function checks IRP data against buddy IRP
//   1. get the IRP data
//   2. check two IRP data is same or not
//    2.1 check data size is same or not
//    2.2 check data is same or not in the specific size.
//
// Parameters: 
//   int irp_id_start  I
//     It points to the start IRP ID to be checked
//   int irp_id_end    I
//     It points to the last IRP ID to be checked. 
//
// Return: 
//   BOOL TRUE, if data matches, false otherwise
//%FE===========================================================================
BOOL carbonXUsbCHK_CmpIrpDataWithBuddy(int irp_id_start, int irp_id_end){
  USB_Irp_t*   p_irp;
  USB_Irp_t*   p_buddy_irp = 0;
  USB_Data_t*  p_first;
  USB_Data_t*  p_second;
  u_int8*      p_fir_data;
  u_int8*      p_sec_data;
  int          i;
  u_int32      j;
  u_int32      size = 0;
  BOOL         is_match = FALSE;
  
  //  MSG_Printf("\n");

  //-------------------- step 1 --------------------------
  if(irp_id_start <= irp_id_end){
    is_match = TRUE;
    
    for( i=irp_id_start; i<=irp_id_end; i++){
      p_irp = carbonXUsbGetIrp(i);
      
      if(p_irp == NULL){
        MSG_Panic("carbonXUsbCHK_CmpIrpDataWithBuddy(): Input IRP ID #%d error. Pointer is NULL\n",i);
        break;
      }
      
      p_first  = NULL;
      p_second = NULL;

      if(p_irp->buddy_id != -1 ){
        p_buddy_irp = carbonXUsbGetIrp(p_irp->buddy_id);
      
        if(p_buddy_irp != NULL){
          if(p_irp->p_rcv_data != NULL){
            p_first = p_irp->p_rcv_data;
          }
          else if(p_irp->p_send_data != NULL){
            p_first = p_irp->p_send_data;
          }
        
          if(p_buddy_irp->p_send_data != NULL){
            p_second = p_buddy_irp->p_send_data;
          }
          else if(p_buddy_irp->p_rcv_data != NULL){
            p_second = p_buddy_irp->p_rcv_data;
          }
        }
        else MSG_Error("carbonXUsbCHK_CmpIrpDataWithBuddy(): The buddy IRP %d does not exist.\n",i);
      }
      else MSG_Error("carbonXUsbCHK_CmpIrpDataWithBuddy(): This IRP %d has no buddy IRP.\n",i);
    
      //-------------------- step 2 --------------------------
      if( (p_first != NULL) && (p_second != NULL) ){

        //-------------------- step 2.1 --------------------------
	if ( p_first->size != p_second->size ) {
          MSG_Error("+  IRP #%d data size (%d) is unequal  IRP #%d data size (%d)\n",
                     p_irp->irp_id, p_first->size, p_buddy_irp->irp_id, p_second->size);
	  size = MIN( p_first->size, p_second->size );
	}
	else {
	  size = p_first->size;
	}
    
        //-------------------- step 2.2 --------------------------
        p_fir_data = p_first->p_data;
        p_sec_data = p_second->p_data;
    
        for(j = 0; j < size; j ++){
          if( p_fir_data[j] != p_sec_data[j] ){
            MSG_Printf("+   At data_byte #%-5d IRP #%-5d data 0x%x is DIFFERENT from IRP #%-5d data 0x%x\n",
		       (j + 1), p_irp->irp_id, p_fir_data[j], p_buddy_irp->irp_id, p_sec_data[j] );
            is_match = FALSE;
          }
        }
    
        if(is_match){
          MSG_Printf("+   IRP #%d data is SAME as IRP #%d. %d bytes were compared\n",
                     p_irp->irp_id, p_buddy_irp->irp_id, size);
        }
	else MSG_Error("Data comparison has failed! See above!\n");
    
        MSG_Printf("+--------------------------------------------------------------------------------\n");
      }
    }//end of for
  }
  else {
    MSG_Warn("carbonXUsbCHK_CmpIrpDataWithBuddy(): Input IRP ID error.\n");
  }

  //MSG_Printf("\n");

  return is_match;
}



//%FS===========================================================================
// USB_CHK_CmpIrpData
//
// Description:
//   This function checks the payload data of "count" IRPs pairs.
//   It uses strncmp() internally to compare data in bytes.
//   It is allowed that both IRPs have different data_size.
//   In this case, this function will compare up to the data_size as contained
//   in the "count" number of source IRPs.
//
//   The following steps are used:
//   Step 1:  pre-amble.
//            prepare parameters for later use. 
//            check parameters for legal values.
//   Step 2:  body.
//            Performs checking 
//   Step 3:  post-amble.
//            print PASS or FAIL informatin, number of bytes matched, and 
//            first occurrance of fail with failing data.
//
// Parameters: 
//   int irp_id1_start  I
//     the starting source irp id.
//   int irp_id2_start  I 
//     the starting destination irp id.
//   u_int32 count I
//     number of source irps this function will check.
//
// Return: 
//   TRUE if all data bytes match. FALSE otherwise.
//%FE===========================================================================
BOOL USB_CHK_CmpIrpData(int irp_id1_start, int irp_id2_start, u_int32 count) {
  
  //general parameters
  u_int32 i=0,j,k,l;
  BOOL is_match = TRUE;
  BOOL is_done  = FALSE;
  u_int32 c_match = 0;

  //parameters for strncmp
  u_int8* p_first = NULL;
  u_int8* p_sec = NULL;
  u_int32 n_first = 0;
  u_int32 n_sec = 0;
  u_int32 n;

  //parameters for irp manipulation
  USB_IrpAPStatic_t*    p_irp_mgr;
  USB_Irp_t**           ppa_irp_head;
  USB_Irp_t*            p_irp_first = 0;
  USB_Irp_t*            p_irp_sec = 0;
  USB_Irp_t*            p_irp_tmp;
  u_int32               first_count = 0;
  u_int32               sec_count = 0;
  BOOL                  is_first_valid = TRUE;
  BOOL                  is_sec_valid = TRUE;

  //parameters that stores future values so that current values can be printed in
  //case of mismatch
  u_int32 n_first_next;
  u_int32 n_sec_next;

  //--------------------- Step 1 -----------------------
  p_irp_mgr = &( ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_irp_mgr);
  ppa_irp_head = p_irp_mgr->ppa_irp_head;

  //make sure that
  //1. count is > 0.
  //2. the src and sec id is within the valid irp id range.
  // 
  if (count == 0) {
    MSG_Warn("USB_CHK_CmpIrpData(): count must be greater than 0.\n");
    return FALSE;
  }
  if (irp_id1_start >= (int)p_irp_mgr->count) {
    MSG_Warn("USB_CHK_CmpIrpData():  IRP id [%d] is out of bound [%d].\n",
              irp_id1_start, p_irp_mgr->count);
    return FALSE;
  }
  if (irp_id2_start >= (int)p_irp_mgr->count) {
    MSG_Warn("USB_CHK_CmpIrpData():  IRP is [%d] is out of bound [%d].\n",
              irp_id2_start, p_irp_mgr->count);
    return FALSE;
  }

  MSG_Printf("Checking Data of %d IRP pairs starting with [IRP #%d, IRP #%d]\n", 
             count, irp_id1_start, irp_id2_start);

  j = 0;
  k = 0;
  l = 0;
  //--------------------- Step 2 -----------------------
  while (is_match && !is_done) {
    j ++;
    if(j > count)  break;
    //Advance to the next non-NULL DATA_OUT for first IRP, and DATA_IN for second IRP 
    //also make sure the IRP was advanced to one that has data size > 0
    //
    while (is_first_valid) {
      if (first_count == count) { 
        is_first_valid = FALSE;  //non valid since count reached.
      }
      else if ( (irp_id1_start + first_count) >= p_irp_mgr->count) {
        is_first_valid = FALSE;  //non valid since end of irp list reached.
      }
      else {
        p_irp_tmp = ppa_irp_head[irp_id1_start + first_count];
        if ( (p_irp_tmp != NULL) &&
              ( (p_irp_tmp->trans_type & USB_IRP_TYPE_OUT) == USB_IRP_TYPE_OUT)
           ){
	  if (p_irp_tmp->p_send_data == NULL) {
	    is_match = FALSE;
	    is_first_valid = FALSE;
	  }
	  else if (p_irp_tmp->p_send_data->size > 0) { break; }
        }
        else { first_count++;}
      }
      k ++;
      if(k > count) break;
    }
    while (is_sec_valid) {
      if ((irp_id2_start + sec_count) >= p_irp_mgr->count) {
        is_sec_valid = FALSE;  //non valid since end of irp list reached.
      }
      else {
        p_irp_tmp = *(ppa_irp_head + irp_id2_start + sec_count);
        if ( (p_irp_tmp != NULL) &&
           ( (p_irp_tmp->trans_type & USB_IRP_TYPE_IN ) == USB_IRP_TYPE_IN) ) 
        {
           if (p_irp_tmp->p_rcv_data == NULL) { 
             is_match = FALSE;
             is_sec_valid = FALSE;
           }
           else if (p_irp_tmp->p_rcv_data->size > 0) { break; }
        }
        else { sec_count++;}
      }
      l ++;
      if(l > count) break;
    }

    //prepare p_irp_*
    if (is_first_valid) {
      p_irp_first = *(ppa_irp_head + irp_id1_start + first_count);
    };
    if (is_sec_valid) {
      p_irp_sec = *(ppa_irp_head + irp_id2_start + sec_count);
    };

    //check if irp bound has been reached.
    if ( (!is_first_valid) && (!is_sec_valid) ) {
      //both [src, sec] have no more DATA to be checked. we are done.
      is_done = TRUE;
      break;
    } 
    else if (is_first_valid && is_sec_valid) {
      //both [src, sec] have more DATA to be checked.
      //update pointer for strncmp
      p_first = p_irp_first->p_send_data->p_data + n_first;
      p_sec = p_irp_sec->p_rcv_data->p_data + n_sec;

      //update size for strncmp
      if ( (p_irp_first->p_send_data->size-n_first) == 
           (p_irp_sec->p_rcv_data->size-n_sec) ) {
        n = p_irp_sec->p_rcv_data->size-n_sec;
        //both are used up, advance to next one.
        n_first_next = 0;
        n_sec_next = 0;
        first_count ++;
        sec_count ++;
      } 
      else if ( (p_irp_first->p_send_data->size-n_first) < 
                (p_irp_sec->p_rcv_data->size-n_sec) ) {
        n = p_irp_first->p_send_data->size-n_first;
        //src is used up, advance to next one.
        n_first_next = 0;
        n_sec_next = n;
        first_count++;
      } 
      else {
        n = p_irp_sec->p_rcv_data->size-n_sec;
        //sec is used up, advance to next one.
        n_first_next = n;
        n_sec_next = 0;
        sec_count ++;
      }
    } 
    else if ( (is_first_valid) && (!is_sec_valid) ) {
      //src has DATA, sec is used up.
      is_match = FALSE;
      break;
    } 
    else {
      //sec has DATA, src is used up.
      is_match = FALSE;
      break;
    }

    if ( !is_done && (strncmp((char*)p_first, (char*)p_sec, n) != 0) ) {
      for (i=0; i<n; i++) {
        if ( *(p_first + i) != *(p_sec + i) ) {break;}
      }
      c_match += i;
      is_match = FALSE;
    } 
    else {
      c_match += n;
    }

    n_first = n_first_next;
    n_sec = n_sec_next;
  } //end of while
  
  //--------------------- Step 3 -----------------------
  if (is_match) {
    MSG_Printf("... PASSED with %d bytes\n", c_match); 
  } 
  else { 
    MSG_Printf("... FAILED after %d bytes\n", c_match);
    if ( (is_first_valid) && (!is_sec_valid) ) {
      //src has DATA, sec is used up.
      MSG_Printf("... First_irp #%d still has data, while Sec_irp has no more\n",
                 irp_id1_start+first_count);
    } 
    else if ( (!is_first_valid) && (is_sec_valid) ) {
      //sec has DATA, src is used up.
      MSG_Printf("... irp #%d still has data, while irp #%d has no more\n",
                 irp_id2_start+sec_count, irp_id1_start+first_count);
    } 
    else {
      if ( (p_first != NULL) && (p_sec != NULL) ) {
        MSG_Printf("Mismatch: irp #%d, byte %d, data %x with irp #%d, byte #%d, data %x\n", 
                   irp_id1_start+first_count, n_first+i, p_first[i],
                   irp_id2_start+sec_count, n_sec+i, p_sec[i] );
      }
      else {
        MSG_Printf("... Both IRPs run out of data\n");
      }
    }
  } //end if (is_match)

  return is_match;
}
