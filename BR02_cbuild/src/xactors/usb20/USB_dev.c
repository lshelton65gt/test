//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 *
 * Comments      : This file includes all functions relating to IRP operation,
 *                 It can be classified into 2 groups:
 *                 [USB_*, USB_DEV_*]
 *
 *                 USB_* are API functions that user can use for IRP related
 *                 operations, they include:
 *
 *                 1. carbonXUsbSetDevProf();
 *                 2. carbonXUsbDevReport();
 *                 3. carbonXUsbGetDevProf();
 *
 *                 USB_DEV_* are functions that SVC uses to operation on IRP,
 *                 they include:
 *
 *                 1. USB_DEV_MsgHeader();
 *                 2. USB_DEV_MsgTotal();
 *                 3. USB_DEV_Msg();
 *                 4. USB_DEV_MsgEnd();
 *                 5. USB_DEV_DevProfReset();
 *
 *
 ***************************************************************************/


#include "USB_support.h"


//%FS===========================================================================
// USB_DEV_MsgHeader
//
// Description:
//   This function prints the Device message header for the IRP List in 
//   the specific pipe only.
//
// Parameter: 
//   void
//
// Return: 
//   void 
//%FE===========================================================================
void USB_DEV_MsgHeader(){
  MSG_Printf("\n");
  //  MSG_LibDebug("usb",2,"\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
  MSG_Printf("+ Report Device Information\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
}



//%FS===========================================================================
// USB_DEV_MsgTotal
//
// Description:
//   This function prints all Device statistic messages in the system only.
//   1. Calculated the device status for all the devices.
//   2. Printf above message.
//
// Parameter: 
//   void
//
// Return:
//   void
//%FE===========================================================================
void USB_DEV_MsgTotal(){
  u_int32   dev_cnt = 0;
  u_int32   cf_cnt  = 0;
  u_int32   df_cnt  = 0;
  u_int32   ad_cnt  = 0;
  u_int32   at_cnt  = 0;
  u_int32   rm_cnt  = 0;
  u_int32   sp_cnt  = 0;
  u_int32   i;
  
  USB_DevProfile_t* p_dev;

  //--------------- step 1 -------------------
  for(i=1;i<USB_MAX_DEV_NUMBER;i++){
    
    p_dev = carbonXUsbGetDevProf(i);
    if(p_dev == NULL) break;
    if(p_dev->dev_id == 0) continue;
    
    dev_cnt += 1;
    switch(p_dev->state){
      case USB_DEV_REMOVE:{
        rm_cnt += 1;
        break;
      }
      case USB_DEV_ATTA:{
        at_cnt += 1;
        break;
      }
      case USB_DEV_DEF:{
        df_cnt += 1;
        break;
      }
      case USB_DEV_ADDR:{
        ad_cnt += 1;
        break;
      }
      case USB_DEV_CFG:{
        cf_cnt += 1;
        break;
      }
      case USB_DEV_SUSP:{
        sp_cnt += 1;
        break;
      }
      default:break;
    }
  }
  
  //--------------- step 2 -------------------
  MSG_Printf("+   There are %d devices connected.\n",dev_cnt);
  MSG_Printf("+   %d devices removed.\n",rm_cnt);
  MSG_Printf("+   %d devices in Attached status.\n",at_cnt);
  MSG_Printf("+   %d devices in Address status.\n",ad_cnt);
  MSG_Printf("+   %d devices in Default status.\n",df_cnt);
  MSG_Printf("+   %d devices in Configured status.\n",cf_cnt);
  MSG_Printf("+   %d devices suspended.\n",sp_cnt);
  MSG_Printf("+----------------------------------------------------------------------\n");
}




//%FS===========================================================================
// USB_DEV_Msg
//
// Description:
//   This function only reports single device message.
//   1. report the device ID
//   2. report the device type
//   3. report the device transfer mode: start-split or not
//   4. report the device address
//   5. report the device operation speed
//   6. report the device speed mode
//   7. report the pipe information about this device.
//
// Parameter:
//   USB_DevProfile_t* p_dev  I
//     It points to the device being reported.
//
// Return: 
//   void 
//%FE===========================================================================
void USB_DEV_Msg(USB_DevProfile_t* p_dev){
  u_int32           i;
  u_int32           pipe_cnt = 0;
  u_int32           df_pipe_id = 0;
  USB_HostCtrlTop_t*        p_top;
  USB_PipeDescr_t*  p_pipe;
  
  if(p_dev != NULL){
  
    p_top  = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
    
    MSG_Printf("+ Information for Device #%d :\n",p_dev->dev_id);
    
    if(p_dev->dev_type == USB_DEV_TYPE_HUB){
      MSG_Printf("+   Device is a HUB.\n");
    }
    else MSG_Printf("+   Device is a Function.\n");
    
    if(p_dev->is_split == TRUE){
      MSG_Printf("+   Device supports split transfers.\n");
    }
    else MSG_Printf("+   Device does not support split transfers.\n");
    
    MSG_Printf("+   Device address       : %d\n",p_dev->dev_addr);
    
    MSG_Printf("+   Device speed         : %s\n", carbonXUsbCvtSpeedToString( p_dev->speed ) );

    //------------------ step 6 -----------------------
    switch(p_dev->speed_mode){
      case USB_SM_HUB_HS_FS:{ 
        MSG_Printf("+   Device speed mode    : Hub or High-/Full-speed device mode\n");
        break;
      }
      case USB_SM_FS_LS:{
        MSG_Printf("+   Device speed mode    : Full-/Low-speed device mode\n");
        break;
      }
    }
    
    //pipe information in the device
    MSG_Printf("+   The following Pipes are defined for this Device :\n");
    MSG_Printf("+    ");

    for(i=0;i<p_top->sys_pipe_mgr.count;i++){
      p_pipe    = *(p_top->sys_pipe_mgr.ppa_pipe_head + i);
      
      if(p_pipe == NULL) continue;
      
      if(p_dev->dev_id == p_pipe->p_endpt_descr->dev_id){
        MSG_Printf(" #%d ",p_pipe->pipe_id);
        pipe_cnt += 1;
        if(p_pipe->p_endpt_descr->endpt_num == 0){
          df_pipe_id = p_pipe->pipe_id;
        }
      }
    }

    MSG_Printf("\n");
    MSG_Printf("+   %d pipes are set-up for this device.\n",pipe_cnt);
    MSG_Printf("+   Pipe #%d is the Default Control Pipe for this device.\n",df_pipe_id);
    MSG_Printf("+----------------------------------------------------------------------\n"); 
  }
}


//%FS===========================================================================
// USB_DEV_MsgEnd
//
// Description:
//   This function prints the device message tail only.
//
// Parameter: 
//   void
//
// Return:
//   void
//%FE===========================================================================
void USB_DEV_MsgEnd(){
  MSG_Printf("+ End of device report\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
}



//%FS===========================================================================
// carbonXUsbGetDevProf
//
// Description:
//   This function only gets the device profile.
// 
// Parameter:
//   u_int32 dev_id  I
//     It points to the device being requested.
// 
// Return:
//   USB_DevProfile_t*
//     If OK, the device profile pointer will be returned.
//     If error, NULL will be returned, the warning message will be report.
//%FE===========================================================================
USB_DevProfile_t*   carbonXUsbGetDevProf(u_int32 dev_id){
  USB_DevProfile_t*  p_dev;
  USB_HostCtrlTop_t*         p_top;
  
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  
  if(dev_id <= USB_MAX_DEV_NUMBER){
    p_dev  = &(p_top->a_dev_prof[dev_id]);

    return(p_dev);
  }
  else{
    MSG_Warn("carbonXUsbGetDevProf(): Request pipe ID error.\n");
    return(NULL);
  }
}


//%FS===========================================================================
// USB_DEV_DevProfReset
//
// Description:
//   This function resets the device profile to default value.
// 
// Parameter:
//   USB_DevProfile_t* p_dev_prof  I
//     It points to the device profile being reset.
// 
// Return:
//   void
//%FE===========================================================================
void USB_DEV_DevProfReset(USB_DevProfile_t* p_dev_prof) {

  u_int32 i = 0;
  
  for(i = 0; i < USB_MAX_DEV_NUMBER; i++) {

    (p_dev_prof + i)->is_split   = FALSE;
    (p_dev_prof + i)->dev_id     = 0;
    (p_dev_prof + i)->dev_addr   = 0;
    (p_dev_prof + i)->port_num   = 0;
    (p_dev_prof + i)->dev_type   = USB_DEV_TYPE_HUB;
    (p_dev_prof + i)->state      = USB_DEV_REMOVE;
    (p_dev_prof + i)->speed_mode = USB_SM_HUB_HS_FS;
    (p_dev_prof + i)->speed      = USB_TRANS_FS;
    
    USB_STD_ResetDescr(&((p_dev_prof + i)->user_cfg));
    USB_STD_ResetDescr(&((p_dev_prof + i)->dut_model));
  }
}


//%FS===========================================================================
// carbonXUsbSetDevProf
//
// Description:
//   This function sets value to the specific device profile.
//
// Parameter: 
//   BOOL   is_split    I
//     It points to the device work mode
//   u_int8  dev_id     I
//     It points to the specific device profile
//   u_int8  dev_addr   I
//     It points to the device address
//   u_int32 port_num   I
//     It points to the device attached to a hub port number
//   USB_DevType_e  dev_type   I
//     It points to the device type is hub or function
//   USB_SpeedMode_e  speed_mode I
//     It points to the device speed mode
//   USB_TransSpeed_e  speed  I
//     It points to the device operation speed
//   u_int32   def_max_pk_size  I
//     It points to the max packet size of the default control pipe 
//     for the new device.
//
// Return: 
//   void
//
//%FE===========================================================================
void carbonXUsbSetDevProf(
  BOOL                  is_split,
  u_int8                dev_id,
  u_int8                dev_addr,
  u_int32               port_num,
  USB_DevType_e         dev_type,
  USB_SpeedMode_e	speed_mode,
  USB_TransSpeed_e      speed,
  u_int32               def_max_pk_size
) {    
  USB_DevProfile_t*   p_dev=0;
  USB_EndPtDescr_t*   p_ep_descr;
  USB_PipeDescr_t*    p_pipe;
  USB_HostCtrlTop_t*  p_top;  

  if(dev_id < USB_MAX_DEV_NUMBER){
    p_dev  = carbonXUsbGetDevProf(dev_id);
  
    p_dev->dev_id        = dev_id;
    p_dev->dev_addr      = dev_addr;
    p_dev->port_num      = port_num;
    p_dev->is_split      = is_split;
    p_dev->dev_type      = dev_type;
    p_dev->speed_mode    = speed_mode;
    p_dev->speed         = speed;
  }

  if(dev_id != 0){
    // Callback function. A typical call back function will write to the
    // device configuration registers of the USB IP core
    p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
    if ( p_top->pfSetDevProf ) {
      // callback function exists.
      // -> Call it.
      p_top->pfSetDevProf( p_top, p_dev );
    }

    p_pipe = carbonXUsbGetDefPipe(dev_id);
    
    if(p_pipe == NULL){
      // Create default Control pipe
      p_ep_descr = &(p_dev->def_endpt_descr);
      carbonXUsbSetEpDescr( p_ep_descr, FALSE, dev_id, 0, 0, 0, def_max_pk_size, USB_DIR_BI, 
		      USB_EPTYPE_CTRL, USB_SYNC_TYPE_NOSYNC, USB_USG_TYPE_DATA);

      carbonXUsbPipeCreate( p_ep_descr );
    }
  }
}


//%FS===========================================================================
// carbonXUsbDevReport
//
// Description:
//   This function reports the device message.
//
// Parameter:
//   BOOL is_one  I
//     If TRUE, it only reports a single device message by dev_id.
//     If FALSE, reports all devices message in the system.
//   u_int32 dev_id  I
//     It points to the specific device. If is_one is TRUE, the value is effectual.
//
// Return: 
//   void
//%FE===========================================================================
void carbonXUsbDevReport(BOOL is_one,u_int32 dev_id){
  u_int32            i;
  USB_DevProfile_t*  p_dev;
  USB_HostCtrlTop_t* p_top;  

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  
  if((is_one)&&(dev_id != 0)){
    if(USB_CHK_DevExist(dev_id)){
      USB_DEV_MsgHeader();
     
      if(is_one){
        p_dev = carbonXUsbGetDevProf(dev_id);
        USB_DEV_Msg(p_dev);
      }
      else {
        USB_DEV_MsgTotal();
 
        for(i=0;i<USB_MAX_DEV_NUMBER;i++){
          p_dev = carbonXUsbGetDevProf(i);
          if(p_dev->dev_id != 0){
            USB_DEV_Msg(p_dev);
          }
        }
      }
  
      USB_DEV_MsgEnd(); 
    }
  }
}

