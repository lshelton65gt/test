
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : First line. 
 *                 Following line
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.10  2004/10/01 13:06:46  zippelius
 *               : Changes to support Control transfers without Data stage and zero length transactions
 *               :
 *               : Revision 1.9  2004/09/27 19:02:43  zippelius
 *               : Re-enabled test2 added support for EP buffer map.
 *               :
 *               : Revision 1.8  2004/08/27 21:31:39  zippelius
 *               : Updated tests (fixes, expanded, added self-checking). Misc clean-ups
 *               :
 *               : Revision 1.7  2004/08/24 15:12:04  zippelius
 *               : Fixed bugs when determining completion of IRP. Misc clean-ups
 *               :
 *               : Revision 1.6  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.5  2004/08/02 18:54:17  zippelius
 *               : Fixed a number of bugs regarding bus time estimation
 *               :
 *               : Revision 1.4  2004/04/26 17:08:55  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.3  2004/04/20 20:54:10  zippelius
 *               : Losts of code clean-up, comment fixes
 *               :
 *               : Revision 1.2  2004/03/26 15:00:17  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.2  2003/12/03 02:13:54  adam
 *               : Edit testcase
 *               :
 *               : Revision 2.1  2003/09/27 12:09:09  serlina
 *               : expect time
 *               :
 *               : Revision 2.0  2003/09/27 06:34:12  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:41  adam
 *               : updated file directory
 *               :
 *               : Revision 1.11  2003/09/23 12:09:01  serlina
 *               : add function
 *               :
 *               : Revision 1.10  2003/09/19 07:25:50  serlina
 *               : add three function
 *               :
 *               : Revision 1.9  2003/09/18 06:29:02  serlina
 *               : change the bandwidth calculate function
 *               :
 *               : Revision 1.8  2003/09/11 08:09:18  issac
 *               : major update after code review
 *               :
 *               : Revision 1.7  2003/09/08 14:18:17  serlina
 *               : change error
 *               :
 *               : Revision 1.6  2003/09/08 09:42:50  serlina
 *               : change parameter
 *               :
 *               : Revision 1.5  2003/09/08 09:34:48  serlina
 *               : add SOF time
 *               :
 *               : Revision 1.4  2003/09/08 07:56:05  serlina
 *               : change link
 *               :
 *               : Revision 1.3  2003/09/08 02:30:57  adam
 *               : updated data structre
 *               :
 *               : Revision 1.2  2003/08/27 15:03:11  serlina
 *               : updated Do Enum series of functions, and dev prof data structure
 *               :
 ***************************************************************************/
 
#ifndef USB_CHECK_H
#define USB_CHECK_H

#define USB_BITSTUFFTIME  (8*(7/6))

#define USB_HOST_DELAY    18
#define USB_HUB_LS_SETUP  20		// 20-bit full-speed bit time
#define USB_HS_TIMER      60000		// a microframe timer
#define USB_HS_SOP        32		// 15 KJ pairs followed by 2 K's
#define USB_HS_SOF_EOP    40
#define USB_HS_EOP        8
#define USB_HS_EOF        560		// EOF1

#define USB_FS_TIMER      12000
#define USB_FS_SOP        8
#define USB_FS_EOP        3
#define USB_FS_EOF        42

extern int overhead_bytes[4][3];  // Two array with Traffic type and Speed as dimensions
extern int inter_pkt_delay_bytes[4][3];  // Two array with Traffic type and Speed as dimensions

BOOL USB_CHK_PipeProperties (USB_PipeDescr_t*  p_pipe, BOOL is_new);

u_int32 USB_CHK_BusTimeCal (USB_PipeDescr_t*  p_pipe,u_int32  data_bc );

u_int32 USB_CHK_TotalBw(USB_TransSpeed_e  speed);

u_int32 USB_CHK_GetIsoBw(USB_PipeDescr_t*  p_pipe);

u_int32 USB_CHK_GetIntrBw(USB_PipeDescr_t*  p_pipe);

u_int32 USB_CHK_PeriodCurBw(USB_PipeDescr_t* p_chk,USB_PipeDescr_t* p_pipe );

BOOL USB_CHK_StateUse(USB_PipeDescr_t* p_pipe);

BOOL USB_CHK_BandWidth (USB_PipeDescr_t* p_pipe,u_int32  data_bc);

u_int32 USB_CHK_MultiTransBw(USB_PipeDescr_t* p_pipe);

u_int32 USB_CalcEstSofBitTime(USB_TransSpeed_e speed);

u_int32 USB_CHK_CalcExpBitTime(BOOL is_ping, u_int32 data_length, USB_PipeDescr_t*  p_pipe);

BOOL USB_CHK_DevExist(u_int32 dev_id);

BOOL USB_CHK_HsIsoOutSplit(USB_PipeDescr_t* p_pipe);

BOOL USB_CHK_UserIsCfg(USB_EndPtDescr_t* p_endpt_descr);

BOOL USB_CHK_DutIsCfg(USB_EndPtDescr_t* p_endpt_descr);

u_int32 USB_CHK_DevIsSame(USB_DevProfile_t* p_dev);

BOOL USB_CHK_HubDescIsSame(USB_Descr_t* p_user_cfg, USB_Descr_t* p_dut_model);

BOOL USB_CHK_DevDescIsSame(USB_Descr_t* p_user_cfg, USB_Descr_t* p_dut_model);

BOOL carbonXUsbCHK_CmpIrpDataWithBuddy(int irp_id_start, int irp_id_end);
BOOL USB_CHK_CmpIrpData(int irp_id1_start, int irp_id2_start, u_int32 count);

#endif
