#include "USB_support.h"
#include "util/CarbonPlatform.h"
#include <stdlib.h>

static u_int32 rand_seed;
u_int32 ECTL_GetRandom() {
  u_int32 rand1,rand2,rand3;
  u_int32 seed;
  seed = rand_seed;
  srand(seed);
  rand1 = (rand() & 0x00ffff00) >> 8;
  rand2 = (rand() & 0x00ffff00) << 8;
  rand3 = rand1 | rand2;
  rand_seed = rand3;
  return(rand3);
}
u_int32 PLAT_GetRandomInRange(u_int32 min, u_int32 max) 
{
  u_int32 temp,temp_min;
  /* block mix-use with no range -- just return the constant they provided */
  temp = min==max ? min : ECTL_GetRandom();
  /* auto swizzle if called backwards... does  reotally matter */
  if(min > max) {
    temp_min = min;
    min = max;
    max = temp_min;
  }
  if ((min == 0) && (max == 0xFFFFFFFF)) return temp;
  return (temp % ((max - min) + 1)) + min;
}

void * XPORT_GetMyProjectData(void) {
  return carbonXGetInstData();
}
void   XPORT_SetMyProjectData(void * p_dat) {
  carbonXSetInstData(p_dat);
}

void PLAT_SetLogDebugLevel(int level) {(void)level;}
void PLAT_SetScreenDebugLevel(int level) {(void)level;}

void carbonXUsbEndOfTest(void) {
  carbonXUsbHostCtrlTopFree();
  MSG_Milestone("Test completed at time = %.10lld\n", PLAT_GetSimTime());
  return;
}
void carbonXUsbSetEptHalt(USB_EndPtDescr_t* p_ept) {(void)p_ept;}

void carbonXUsbTB_Start(void) {
  carbonXUsbHostCtrlTopInit();
}

u_int32 XPORT_ReceivePacket(u_int8 *pkt) {
  u_int32 length = carbonXRead(0, pkt, 0);
  USB_BufferSwizzle((u_int32 *)pkt, length);
  return length;
}

void XPORT_SendPacket(u_int8 *pkt, u_int32 size, u_int32 ipg) {
  USB_BufferSwizzle((u_int32 *)pkt, size);
  carbonXWrite(ipg, pkt, size);
}

void USB_BufferSwizzle(u_int32 *buffer, int size)
{
  int i;
  for(i=0;i<((size+3)/4);i++){
    buffer[i] = ((buffer[i] & 0x000000ff) << 24) | ((buffer[i] & 0x0000ff00) << 8) |
      ((buffer[i] & 0x00ff0000) >> 8) | ((buffer[i] & 0xff000000) >> 24);

  }
}

