//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 *
 * Comments      :  This file is required for all C side modules. It
 *                  must contain:
 *
 *                  1. All common enums and structs for the C side.
 *
 ***************************************************************************/

#ifndef CARBONX_USB_COMMON_H
#define CARBONX_USB_COMMON_H

#define USB_MAX_CFG_NUMBER 10
#define USB_MAX_IF_NUMBER  10

#define USB_MAX_EP_NUMBER     16 // USB Spec allows for up to 16 Endpoints     
#define USB_MAX_EP_DESCR_NUMBER  (USB_MAX_EP_NUMBER*2)   // There is one IN and one OUT descriptor for each
                                                         // Endpoint implemented in the USB IP core
                                                         // Note: USB_MAX_EP_DESCR_NUMBER should be defined in usb_ip_core_defines.h

#define USB_INTERPACKET_CYCLES 6

// Command sent to Host Controller for processing 
typedef enum {                          // Transaction type
        USB_TRANS_TYPE_CSPLIT   = 0,   	// Complete Split transaction
        USB_TRANS_TYPE_NONSPLIT = 1,   	// Non-Split transaction
        USB_TRANS_TYPE_SOF      = 2,   	// Start Of Frame transaction
        USB_TRANS_TYPE_SSPLIT   = 3	// Start Split transaction
} USB_TransType_e;

// Transaction speed 
typedef enum {                          // Transaction speed
  USB_TRANS_UNDEF = 0,
  USB_TRANS_LS    = 1,                  
  USB_TRANS_FS    = 2,               
  USB_TRANS_HS    = 3                   
} USB_TransSpeed_e;

// Endpoint type 
typedef enum {                   // Endpoint type
        USB_EPTYPE_UNDEF = 0,    // Undefined
	USB_EPTYPE_CTRL =  1,    // control endpoint
	USB_EPTYPE_ISO  =  2,    // isochronous endpoint
	USB_EPTYPE_BULK =  3,    // bulk endpoint
	USB_EPTYPE_INTR =  4     // interrupt endpoint
	
} USB_EpType_e;

// Transfer direction 
typedef enum {                          // Transfer direction
	USB_DIR_UNDEF 	  = 0x0,
	USB_DIR_OUT 	  = 0x1,
	USB_DIR_IN  	  = 0x2, 
	USB_DIR_BI        = (USB_DIR_IN | USB_DIR_OUT)	// Bi-directional for control pipe
} USB_Direction_e;

#define USB_TIMEOUT   0x0
#define USB_OUT	      0x1
#define USB_ACK	      0x2
#define USB_DATA0     0x3 
#define USB_PING      0x4
#define USB_SOF	      0x5
#define USB_NYET      0x6
#define USB_DATA2     0x7
#define USB_SPLIT     0x8
#define USB_IN	      0x9
#define USB_NAK	      0xa
#define USB_DATA1     0xb
#define USB_ERR_PRE   0xc
#define USB_SETUP     0xd
#define USB_STALL     0xe
#define USB_MDATA     0xf       


//  PIDs
typedef enum {                          // PID type
	USB_PID_RESERVED  = 0x0,        // Pseudo PID for error case
	USB_PID_OUT	  = USB_OUT, 
	USB_PID_ACK	  = USB_ACK,
	USB_PID_DATA0 	  = USB_DATA0,  
	USB_PID_PING 	  = USB_PING, 
	USB_PID_SOF	  = USB_SOF, 
	USB_PID_NYET	  = USB_NYET, 
	USB_PID_DATA2	  = USB_DATA2, 
	USB_PID_SPLIT	  = USB_SPLIT, 
	USB_PID_IN	  = USB_IN, 
	USB_PID_NAK	  = USB_NAK, 
        USB_PID_DATA1	  = USB_DATA1, 
        USB_PID_ERR_PRE	  = USB_ERR_PRE, 
        USB_PID_SETUP	  = USB_SETUP, 
        USB_PID_STALL	  = USB_STALL, 
        USB_PID_MDATA	  = USB_MDATA      
} USB_Pid_e;




// Isochronous transfer data part   
typedef enum {                          // Isochronous data part for an HC command
	USB_PART_ALL   = 0x0, 
	USB_PART_BEGIN = 0x1, 
	USB_PART_MID   = 0x2,
	USB_PART_END   = 0x3
} USB_DataPart_e;


// After receiving handshake, the action Host Controller will take 
typedef enum {                          // What HC should do next for this command
	USB_DO_COMP_IMMED_NOW   = 0,	// Do complete-split immediately within 
                                        // same microfram (for periodic pipe)
	USB_DO_COMPLETE         = 1,	// Do complete-split transaction
	USB_DO_COMPLETE_IMMED   = 2,    // Do complete-split immediately before
	                                // doing a different transaction (for
	                                // asynchronous pipe)
	USB_DO_HALT             = 3,	// Do endpoint halt processing for the
	                                // endpoint of this command
        USB_DO_NEXT_CMD         = 4,	// Do next command for this endpoint
                                        // advance data pointer appropriately
        USB_DO_NEXT_COMPLETE    = 5,	// Do next comlete-split in next
                                        // microframe (for periodic pipe)
        USB_DO_NEXT_PING        = 6,
        USB_DO_OUT              = 7,    // The handshake of PING transaction is 
        				// ACK, inform the scheduler to send OUT
        				// transaction 
        USB_DO_PING             = 8,
        USB_DO_SAME_CMD         = 9,	// Do same command over again
        USB_DO_SAME_CMD_TIMEOUT =10,	// Do same command over again because of
        				// timeout
        USB_DO_START            =11	// Do start-split transaction
} USB_HcTransState_e;


// Isochronous transfer specify the synchronous type 
typedef enum {
	USB_SYNC_TYPE_NOSYNC = 0x0, 
	USB_SYNC_TYPE_ASYNC  = 0x1,
	USB_SYNC_TYPE_ADAPT  = 0x2, 
	USB_SYNC_TYPE_SYNC   = 0x3
} USB_SyncType_e;


// Isochronous transfer specify the usage type 
typedef enum { 
	USB_USG_TYPE_DATA, 
	USB_USG_TYPE_FD,                // Feedback
	USB_USG_TYPE_IMPLICIT
} USB_UsageType_e;


// Currently pipe state 
typedef enum { 
	USB_PIPE_STAT_READY       = 0x2, // Has IRP waiting to be sent.
	USB_PIPE_STAT_HALTED      = 0x3, // Pipe down due to error.
	USB_PIPE_STAT_IDLE        = 0x1, // No IRP pending, but pipe not down.
	USB_PIPE_STAT_NEED_SETUP  = 0x0, // Need to be setup, like at the beginning of test.
	USB_PIPE_STAT_SETUP_FAIL  = 0x4, // Pipe setup has failed.
	USB_PIPE_STAT_READY_NOTIME= 0x5  // Pipe is READY, but there is no time left in currect uframe.
} USB_PipeState_e;


// Currently Irp state 
typedef enum { 
        USB_IRP_STAT_READY      = 0,  // Ready, but execution has not yet started
        USB_IRP_STAT_INPROG     = 1,  // In progress
        USB_IRP_STAT_COMPL      = 2,  // Completed
        USB_IRP_STAT_ABORT      = 3   // Aborted
} USB_IrpStatus_e;


typedef enum {
	USB_TRANS_STAGE_IN    = 0x0,
	USB_TRANS_STAGE_OUT   = 0x1,
	USB_TRANS_STAGE_SETUP = 0x2,
	USB_TRANS_STAGE_STAT  = 0x3
} USB_TransStage_e;


typedef enum {
	USB_IRP_TYPE_IN       = 0x1,
	USB_IRP_TYPE_OUT      = 0x2,
	USB_IRP_TYPE_CTRL_IN  = 0x5,  // USB_IRP_TYPE_IN  | 0x4
	USB_IRP_TYPE_CTRL_OUT = 0x6   // USB_IRP_TYPE_OUT | 0x4

} USB_IrpTransType_e;


// device type 
typedef enum{
  	USB_DEV_TYPE_HUB,
  	USB_DEV_TYPE_FUNCTION
}USB_DevType_e;


// current device state diagram 
typedef enum {
	USB_DEV_REMOVE          = 0x0,	// Device has been removed     
        USB_DEV_ATTA            = 0x1,	// Device has been Attached
        USB_DEV_POW             = 0x2,	// Powered
        USB_DEV_DEF             = 0x3,	// Entered Default state
        USB_DEV_ADDR            = 0x4,	// Has been assigned a unique Address
        USB_DEV_CFG             = 0x5,	// Configured state
        USB_DEV_SUSP            = 0x6	// Suspended state
} USB_DevState_e;


typedef enum {
	USB_SM_HUB_HS_FS = 0x0,
	USB_SM_FS_LS	 = 0x1
} USB_SpeedMode_e;


// Following struct is about IRP 
typedef struct {
  u_int32	        size;           // Number of bytes in p_data
  u_int8*               p_data;         // Dynamic array of data bytes
} USB_Data_t;


// This struct is used by system 
typedef struct {
  BOOL			has_been_seg;	// Mark the IRP has been segmented or not.
  u_int32               err_mask;
  USB_DataPart_e        data_part;      // For OUT tansfers
} USB_IrpSysRecord_t;


typedef struct {
  int    		irp_id;		// Each IRP gets a unique ID

  USB_IrpTransType_e    trans_type;
  int	        	buddy_id;
  				
  USB_IrpStatus_e       status; 
  
  // These fields contain the data to be sent out
  USB_Data_t*		p_send_data;
  u_int32		sent_cnt;      // Number of bytes that have been sent out so far
  
  // These are only used in conjunction with Control Transfers
  // Data to be transmitted during the Setup
  USB_Data_t*		p_setup_send_data;
  u_int32		setup_sent_cnt;
  
  USB_TransStage_e      stage;
  
  // These fields contain the data to be received
  USB_Data_t*       	p_rcv_data;
  u_int32		rcv_bytes;
  
  USB_IrpSysRecord_t    irp_sys_record;
} USB_Irp_t;


// Following struct is for a USB Function Endpoint 
typedef struct {
  BOOL 			is_auto;	// If auto set the endpoint descriptor of DUT
  u_int8                dev_id;
  u_int32               endpt_num; 	// Endpoint number  
  u_int32               hs_trans_num;
  					// Specify the amount of additional transactions
                                        // per microframe for high-speed 
                                        // isochronous and interrupt. Must be < 3
                                        // Ref: USB 2.0 spec page 56, 270
  u_int32               interval;  	// Interval for polling endpoint for 
                                        // data transfers. Ref: USB 2.0 spec page 271
  u_int32               max_packet_size;
  					// Maximum packet size(in bytes)
                                        // endpoint zero:8, 16, 32, or 64
                                        // zero is reserved. 
                                        // power (2,n-1), 0<=n<=[16,255]
  USB_Direction_e       direction;      // Transfer direction
  USB_Direction_e       curr_dir;       // Current transfer direction.
                                        // In case of CTRL Endpoints indicates the current direction
  USB_EpType_e          endpt_type;     // Transfer type 
  USB_SyncType_e        sync_type;      // Synchronous type for isochronous
  USB_UsageType_e       usage_type;     // Usage type for isochronous

  u_int32               allow_errors;
  USB_Pid_e             pid;            // The DATAx PID for the next data transaction
  BOOL                  needs_toggle;
  BOOL                  is_first_trans; // TRUE for the very first transaction sent/received
                                        // @@@RZ not really used. Should be removed at some point in time
  USB_Pid_e             prev_trans_toggle_pid;  // DATAx PID before the most recent DATAx
  USB_Pid_e             curr_trans_toggle_pid;  // Most recent DATAx PID

} USB_EndPtDescr_t;


typedef struct {
  u_int32		auto_trigger;	// The IRP ID record for setting endpoint
  u_int32               err_cnt;        // Transfer err count, if > 3 the pipe 
                                        // will be halted.
  u_int32               duration_est;   // Expect duration of a transfer e.g. OUT + Data + Handshake
  u_int64               time_started;   // Expect time of send a transaction
  u_int32               iso_cnt;        // For Isochronous count per microframe
  u_int32               iso_trans_cnt;  // For Isochronous IN transaction count
  u_int32               iso_data_cnt;   // Data endpoint count for feedback for ISO only.
  u_int32               iso_fb_cnt;     // Feedback endpoint count for ISO only.
  u_int32               uframe_sched;   // Next periodic uframe number where IRP is scheduled.
  u_int32               nak_count;      // keep a count and clear on ACK
  USB_HcTransState_e	prev_trans_state;// The result of the last transaction
  USB_Pid_e             response_pid;   // PID that was most recently received from the USB device
  USB_PipeState_e       pipe_state;     // Current pipe state
} USB_PipeSysRecord_t;


// IRP Link List struct in a pipe.
struct USB_IrpLinkList{
  USB_Irp_t* p_irp;

  struct USB_IrpLinkList* p_next;
  struct USB_IrpLinkList* p_prev;
};


typedef struct USB_IrpLinkList USB_IrpLink_t;

typedef struct {
  u_int32        count;
  USB_IrpLink_t* p_head;
  USB_IrpLink_t* p_tail;
} USB_IrpLinkList_t;


typedef struct {
  USB_StdDevDescr_t     stddev;
  USB_StdCfgDescr_t     stdcfg[USB_MAX_CFG_NUMBER];
  USB_StdIfDescr_t      a_stdif[USB_MAX_IF_NUMBER];
  USB_StdEpDescr_t      a_stdep[USB_MAX_EP_DESCR_NUMBER];
  USB_StdDevQualDescr_t stdqual;
  USB_StdOsCfgDescr_t   stdos;  
  USB_StdHubDescr_t     stdhub;
}USB_Descr_t;

 
typedef struct {
  BOOL                  is_split;
  u_int8                dev_id;		// Device id 1 - 127
  u_int8                dev_addr;       // Device address
  u_int32               port_num;
  USB_DevType_e         dev_type;	// Hub or function
  USB_DevState_e        state;		// Current device state
  USB_SpeedMode_e	speed_mode;
  USB_TransSpeed_e      speed;          // The device speed
  USB_Descr_t           user_cfg;
  USB_Descr_t           dut_model;
  USB_EndPtDescr_t      def_endpt_descr;// Default Endpoint descriptor
                                        // It is used to initialize default Control Pipe   
} USB_DevProfile_t;


typedef struct {
  u_int32               pipe_id;        // Every pipe has a unique ID 			    	        
  USB_EndPtDescr_t*     p_endpt_descr;  // Pointer to Function EndPoint that is
			      	        // associated with this Pipe
  USB_IrpLinkList_t     a_irp_c;        // OUT FIFO for completed IRPs
  USB_IrpLinkList_t   	a_irp_uc;       // IN FIFO for uncompleted IRPs
  USB_PipeSysRecord_t   sys_record;     // Fields that are meant for internal use only
} USB_PipeDescr_t;


// Error injection FIFO record. Reference to USB2.0 standard P557. 
typedef struct {
  BOOL         do_crc_err;		// TRUE instructs BFM to inject a bad CRC
  					// to the specified packet.
  BOOL         do_data_corrupt;         // TRUE instructs BFM to corrupt transfer
  					// data.
  BOOL         do_data_drop;            // TRUE instructs BFM to drop some bits
  					// of the data packet.
  BOOL         do_pid_err;	        // TRUE instructs PE to inject bad PID
  					// check bits.
  BOOL         do_timeout_err;	        // TRUE instructs BFM to insert a
  					// timeout error.
  BOOL         do_datapart_err;	        // TRUE instructs PE to inject a bad
  					// data part.
  BOOL         is_split_phase;          // TRUE instructs PE to inject error in
                                        // split token phase.
  BOOL         is_token_phase;          // TRUE instructs PE to inject error in
                                        // token phase.
  BOOL         is_data_phase;           // TRUE instructs PE to inject error in
                                        // data phase.
  BOOL         receive_timeout;         // Fake a received timeout
  BOOL         receive_crc_err;         // Fake a received crc error
  BOOL         receive_pid_err;         // Fake a received pid error
} USB_ErrInjection_t;


// 
// Used between scheduler and protocol engine. Reference to USB 2.0 Host SVC 
// ArchSpec 2003_06_23, P32.
//
 
typedef struct {
  BOOL		      needs_pre;        // Instructs BFM, whether or not to add 
  				        // a preamble to the next transaction, 
  					// only for low speed.
  BOOL                needs_toggle;	// True indicates sequence bit toggle
  BOOL                is_crc_ok;
  BOOL                is_last;		// True indicates last complete split 
  					// token.
  u_int32             dev_addr;  	// Device address is 0 ~ 127.
  u_int32	      expected_duration;// The expected duration of the transfer in ns
  u_int32	      frame_num;	// The frame number of the transfer
  u_int32	      hub_port; 	// Hub port is 0 - 127
  u_int64	      time_first_attempted;
  					// The time, when transfer was first attempted
  u_int64	      time_started;     // The start time
  u_int64	      time_completed;   // The completion time
  u_int32             response;         // Response to the transaction
                                        // Either: USB_TIMEOUT or
                                        // PID of the response transaction (USB_ACK, USB_NAK, etc.)
  u_int32             transferred_pid;

  USB_DataPart_e      datapart;
  USB_ErrInjection_t  err_inject;
  USB_HcTransState_e  trans_state;      // One of USB_HcTransState_e
  USB_Pid_e 	      pid;
  USB_TransSpeed_e    trans_speed;	// Transaction speed
  USB_TransType_e     trans_type;       // Tranaction type: SOF, Nonsplit, 
  				        // Start SPLIT, Comoplete SPLIT.               
  USB_Data_t*         p_data_segment;   // Point to the current DataSegment 
  USB_Irp_t*          p_irp;
  USB_PipeDescr_t*    p_pipe;
} USB_Trans_t;



typedef struct {
  u_int32   start;     // start address of buffer
  u_int32   size;      // size of buffer in bytes
  u_int32   curr_ptr;  // Where the next byte shall be inserted (offset from field start)
} USB_BufDescr_t;


#define USB_BUF_DESCR_OUT 0
#define USB_BUF_DESCR_IN  1

typedef struct {
  USB_BufDescr_t     buf_descr[2];   // One for OUT, one for IN
} USB_EpBufDescr_t;


typedef enum { 
	USB_MEM_PATTERN_SEQ,
	USB_MEM_PATTERN_RAND,
	USB_MEM_PATTERN_ALL0
} USB_EndPtMemPattern_e;

// Define codes for Standard Requests

// Define codes for Class-specific Requests
//
// Hub Class Request Codes
//#define USB_GET_STATUS        0x0
//#define USB_CLEAR_FEATURE     0x1
//#define USB_SET_FEATURE       0x3
//#define USB_GET_DESCRIPTOR    0x6
//#define USB_SET_DESCRIPTOR    0x7
//#define USB_CLEAR_TT_BUFFER   0x8
//#define USB_RESET_TT          0x9
//#define USB_GET_TT_STATE      0xA
//#define USB_STOP_TT           0xB

#define USB_CLEAR_HUB_FEATURE     0x20
#define USB_CLEAR_PORT_FEATURE    0x23
#define USB_CLEAR_TT_BUFFER       0x23
#define USB_GET_HUB_DESCRIPTOR    0xA0
#define USB_GET_HUB_STATUS        0xA0
#define USB_GET_PORT_STATUS       0xA3
#define USB_RESET_TT              0x23
#define USB_SET_HUB_DESCRIPTOR    0x20
#define USB_SET_HUB_FEATURE       0x20
#define USB_SET_PORT_FEATURE      0x23
#define USB_GET_TT_STATE          0xA3
#define USB_STOP_TT               0x23

#endif
