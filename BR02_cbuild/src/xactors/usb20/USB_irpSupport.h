//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 * File Name     : $Source$
 *
 * Comments      : First line. 
 *                 Following line
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 ***************************************************************************/
 
#ifndef USB_IRP_SUPPORT_H
#define USB_IRP_SUPPORT_H

#define USB_IRP_GROW_SIZE 50

void USB_IRP_MsgHeader(u_int32 pipe_id);

void USB_IRP_AllMsgHeader();

void USB_IRP_MsgTotal(USB_PipeDescr_t* p_pipe);

void USB_IRP_AllMsgTotal();

void USB_IRP_Msg(USB_Irp_t* p_irp);

void USB_IRP_MsgEnd();

USB_Irp_t* USB_IRP_GetNewIrp ();

BOOL USB_IRP_PA_IsFull(USB_IrpAPStatic_t*  p_irp_mgr);

USB_Irp_t** USB_IRP_PA_Cur(USB_IrpAPStatic_t*  p_irp_mgr);

void USB_IRP_Clear(USB_Irp_t* p_irp);

void USB_IRP_Update(USB_PipeDescr_t*  p_pipe );

void USB_IRP_PA_Init(USB_IrpAPStatic_t*  p_irp_mgr);

void USB_IRP_PA_Reset(USB_IrpAPStatic_t*  p_irp_mgr);

void USB_IRP_PA_Free (USB_IrpAPStatic_t*  p_irp_mgr);

USB_Irp_t*  carbonXUsbGetIrp(int irp_id);

void USB_IRP_ReqGetStat(USB_Irp_t* p_irp);

void USB_IRP_ReqClearFea(USB_Irp_t* p_irp);

void USB_IRP_ReqSetFea(USB_Irp_t* p_irp);

void USB_IRP_ReqGetDesc(USB_Irp_t* p_irp);

void USB_IRP_ReqSetDesc(USB_Irp_t* p_irp);

void USB_IRP_ReqGetCfg(USB_Irp_t* p_irp);

void USB_IRP_ReqSetCfg(USB_Irp_t* p_irp);

void USB_IRP_ReqGetIf(USB_Irp_t* p_irp);

void USB_IRP_ReqSetIf(USB_Irp_t* p_irp);

void USB_IRP_ReqSynchFrame(USB_Irp_t* p_irp);

void USB_IRP_ReqClearTTBuffer(USB_Irp_t* p_irp);

void USB_IRP_ReqResetTT(USB_Irp_t* p_irp);

void USB_IRP_ReqGetTTStat(USB_Irp_t* p_irp);

void USB_IRP_ReqStopTT(USB_Irp_t* p_irp);

void USB_IRP_StdReqAnalysis(USB_Irp_t* p_irp);

USB_Irp_t* USB_IRP_CreateRandom(
  int*               p_size,
  USB_Data_t**       pp_setup_data, 
  USB_Data_t**       pp_send_data,
  USB_IrpTransType_e trans_type,
  USB_EndPtDescr_t*  p_endpoint,
  int                buddy_id
  );

#endif


/*
 * Log           : 
 *               : $Log$
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.9  2004/10/21 16:21:32  zippelius
 *               : Moved some functions. Renamed some functions
 *               :
 *               : Revision 1.8  2004/08/27 21:31:40  zippelius
 *               : Updated tests (fixes, expanded, added self-checking). Misc clean-ups
 *               :
 *               : Revision 1.7  2004/08/23 20:15:57  zippelius
 *               : Fixed Control transfers and misc other clean-ups
 *               :
 *               : Revision 1.6  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.5  2004/04/26 21:34:29  zippelius
 *               : Code clean-up regarding IRP state and misc other
 *               :
 *               : Revision 1.4  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.3  2004/04/20 20:54:10  zippelius
 *               : Losts of code clean-up, comment fixes
 *               :
 *               : Revision 1.2  2004/03/26 15:00:17  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.3  2003/12/04 10:18:59  adam
 *               : Debug testcases
 *               :
 *               : Revision 2.2  2003/12/03 02:13:54  adam
 *               : Edit testcase
 *               :
 *               : Revision 2.1  2003/11/25 09:41:10  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.0  2003/09/27 06:34:12  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:42  adam
 *               : updated file directory
 *               :
 *               : Revision 1.12  2003/09/12 03:47:50  serlina
 *               : change the function name
 *               :
 *               : Revision 1.11  2003/09/11 08:09:18  issac
 *               : major update after code review
 *               :
 *               : Revision 1.10  2003/09/08 07:56:05  serlina
 *               : change link
 *               :
 *               : Revision 1.9  2003/09/08 02:30:58  adam
 *               : updated data structre
 *               :
 *               : Revision 1.8  2003/08/29 09:36:05  serlina
 *               : change trans_t
 *               :
 *               : Revision 1.7  2003/08/28 07:40:45  serlina
 *               : change error
 *               :
 *               : Revision 1.6  2003/08/28 07:32:26  serlina
 *               : Add a function USB_IRP_Destroy
 *               :
 *               : Revision 1.5  2003/08/27 15:03:11  serlina
 *               : updated Do Enum series of functions, and dev prof data structure
 *               :
 */
