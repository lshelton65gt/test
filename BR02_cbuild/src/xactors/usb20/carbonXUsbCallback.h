//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file is for Callback functions. 
 *                 It includes all callback functions for user uses.
 *
 *               : 
 ***************************************************************************/
 
#ifndef CARBONX_USB_CALLBACK_H
#define CARBONX_USB_CALLBACK_H

struct USB_HostCtrlTop_t;

// Define call back function types
typedef void (*pfDeviceAttach_t)( struct USB_HostCtrlTop_t*  p_top );
typedef void (*pfDevSpeedNeg_t)( struct USB_HostCtrlTop_t*  p_top );
typedef void (*pfBeforeSchLoop_t)( struct USB_HostCtrlTop_t*  p_top );
typedef void (*pfPipePushInIRP_t)( struct USB_HostCtrlTop_t*  p_top, USB_PipeDescr_t* p_pipe, USB_Irp_t* p_irp );
typedef void (*pfNewIrp_t)( struct USB_HostCtrlTop_t*  p_top, USB_Irp_t* p_irp );
typedef void (*pfBeginOfExecIrp_t)( struct USB_HostCtrlTop_t*  p_top, USB_Irp_t* p_irp, USB_PipeDescr_t* p_pipe );
typedef void (*pfEndOfExecIrp_t)( struct USB_HostCtrlTop_t*  p_top, USB_Irp_t* p_irp, USB_PipeDescr_t* p_pipe );
typedef void (*pfBeginOfTransaction_t)( struct USB_HostCtrlTop_t*  p_top, USB_Trans_t* trans );
typedef void (*pfEndOfTransaction_t)( struct USB_HostCtrlTop_t*  p_top, USB_Trans_t* trans );
typedef void (*pfBeforeSof_t)( struct USB_HostCtrlTop_t*  p_top );
typedef void (*pfBeginOfExecPeriodList_t)( struct USB_HostCtrlTop_t* p_top );
typedef void (*pfBeginOfExecCtrlList_t)( struct USB_HostCtrlTop_t* p_top );
typedef void (*pfBeginOfExecBulkList_t)( struct USB_HostCtrlTop_t* p_top );
typedef void (*pfSetDevProf_t)( struct USB_HostCtrlTop_t*  p_top, USB_DevProfile_t* p_dev );
typedef void (*pfSetEpDescr_t)( struct USB_HostCtrlTop_t*  p_top, USB_EndPtDescr_t* p_ep_descr );

#endif
