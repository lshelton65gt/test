//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONX_USB_SYS_H
#define CARBONX_USB_SYS_H

#include "xactors/usb20/carbonXUsbCallback.h" 

#define USB_MAX_DEV_NUMBER         10
#define USB_MAX_PE_BUFFER_SIZE     65536

// Addresses of CsWrite registers
#define USB_BFM_ADDR_CFG_REGISTER	0x0
#define USB_BFM_ADDR_EXPECTED_DURATION	0x4

// Addresses of CsRead registers
#define USB_BFM_ADDR_STATUS_REGISTER	0x100
#define USB_BFM_ADDR_TIME_START		0x104
#define USB_BFM_ADDR_TIME_END		0x108
#define USB_BFM_ADDR_TIME_CNT		0x10c
#define USB_BFM_ADDR_CFG_READ		0x110


#define USB_CTRL_ADD_PRE               0x0004
#define USB_CTRL_TIMEOUT_ERR           0x0008
#define USB_CTRL_CRC_ERR               0x0010
#define USB_CTRL_DATA_CORRUPT          0x0020
#define USB_CTRL_DATA_DROP             0x0040
#define USB_CTRL_SPLIT_PHASE           0x0080
#define USB_CTRL_TOKEN_PHASE           0x0100
#define USB_CTRL_DATA_PHASE            0x0200
#define USB_CTRL_DEV_ATTACH            0x0400
#define USB_CTRL_DEV_REMOVE            0x0800
#define USB_CTRL_LS_ATTACH             0x1000
#define USB_CTRL_DEV_RESET             0x2000
#define USB_CTRL_DEV_SUSPEND_RESUME    0x4000
#define USB_CTRL_CORRUPT_PID           0x8000
#define USB_CTRL_VSTATUS               0xff0000
#define USB_CTRL_VSTATUS_BIT           16
#define USB_CTRL_ENABLE_TX_STALLS      0x02000000
#define USB_CTRL_ENABLE_RX_STALLS      0x04000000
#define USB_CTRL_IGNORE_RESPONSE       0x08000000
#define USB_CTRL_SQUELCH_LINESTATE     0x10000000
#define USB_CTRL_FAST_NEGOTIATE_CLR    0x20000000
#define USB_CTRL_FAST_NEGOTIATE        0x40000000
#define USB_CTRL_GLOBAL_RESET          0x80000000


// Define bit positions for Status Register bits
#define USB_STATUS_DEV_SPEED_BITS      0
#define USB_STATUS_DEV_SPEED_MASK    0x3
#define USB_STATUS_CRC_ERR_BIT         2
#define USB_STATUS_PID_ERR_BIT         3
#define USB_STATUS_RST_ERR_BIT         4
#define USB_STATUS_TIME_OUT_BIT        5
#define USB_STATUS_RX_RDY_BIT          6
#define USB_STATUS_SPEED_NEG_DONE_BIT  7
#define USB_STATUS_SPEED_FS_ONLY_BIT   8
#define USB_STATUS_SPEED_16BIT_BUS_BIT 9
#define USB_STATUS_DEV_SUSPEND_BIT    10
#define USB_STATUS_VCONTROL_BITS      11
#define USB_STATUS_VCONTROL_MASK    0x0F

#define USB_STATUS_CRC_ERR           (1<<USB_STATUS_CRC_ERR_BIT)
#define USB_STATUS_PID_ERR           (1<<USB_STATUS_PID_ERR_BIT)
#define USB_STATUS_RST_ERR           (1<<USB_STATUS_RST_ERR_BIT)
#define USB_STATUS_TIMEOUT           (1<<USB_STATUS_TIME_OUT_BIT)
#define USB_STATUS_RX_READY          (1<<USB_STATUS_RX_RDY_BIT)
#define USB_STATUS_SPEED_NEG_DONE    (1<<USB_STATUS_SPEED_NEG_DONE_BIT)
#define USB_STATUS_DATA_BUS          (1<<USB_STATUS_SPEED_16BIT_BUS_BIT)
#define USB_STATUS_SPEED_FS_ONLY     (1<<USB_STATUS_SPEED_FS_ONLY_BIT)
#define USB_STATUS_SPEED_16BIT_BUS   (1<<USB_STATUS_SPEED_16BIT_BUS_BIT)
#define USB_STATUS_DEV_SUSPEND       (1<<USB_STATUS_DEV_SUSPEND_BIT)
#define USB_STATUS_VCONTROL          (USB_STATUS_VCONTROL_MASK<<USB_STATUS_VCONTROL_BITS)



#define USB_RESET_COUNTER		      200000

// 200,000 clock cycles @ 16 ns each translate to a wait time of 3.2 ms
// We use this shortened value to speed up simulation
// According to the USB 2.0 spec (p150), the proper wait time is up to 100 ms
// #define USB_ATTACH_SETTLE_TIME	(100000000/16)
//#define USB_ATTACH_SETTLE_TIME		200000
#define USB_ATTACH_SETTLE_TIME		90000

#define USB_RESET_SPEED_NEG_WAIT_TIME   200000

typedef struct {
  u_int32          count;
  u_int32          size;
  USB_Irp_t**      ppa_irp_head;
} USB_IrpAPStatic_t;

typedef struct{
  u_int32           count;
  u_int32           size;
  USB_PipeDescr_t** ppa_pipe_head;
} USB_PipeAPStatic_t;

typedef struct{
  u_int32           count;
  u_int32           size;
  USB_Data_t**      ppa_data_head;
} USB_DataAPStatic_t;

typedef struct {
  BOOL     do_sof_err;
  BOOL     always_ping;
  BOOL     never_ping;
  BOOL     use_up_micro_frame;
  int      time_left;
  u_int64  time_of_SOF;
  u_int64  time_of_next_SOF;
  u_int32  ip_core_ctrl;      // Used to indicate special on-fly special case condition
                              // requiring changes to the IP core set-up
                              // E.g. Forcing IP core to NAK transactions or to STALL
  u_int32  period_count;
  u_int32  ctrl_count;
  u_int32  bulk_count;  
  u_int32  period_list_size;
  u_int32  ctrl_list_size;
  u_int32  bulk_list_size;
  u_int32  asyc_ctrl_index_next;
  u_int32  asyc_bulk_index_next;
  u_int32  hs_trans_count;  
  u_int32  uframe_count;
  u_int32  frame_cnt_begin;
  u_int32  frame_cnt_end;
  u_int32  sof_err_mask;
  u_int32* p_periodic;
  u_int32* p_asyc_ctrl;
  u_int32* p_asyc_bulk;
} USB_Scheduler_t;


typedef struct {
  u_int32  transaction_response_timeout;
  u_int32  transaction_halted;
  u_int32  transaction_aborted;
  u_int32  packet_max_size_exceeded;
  u_int32  transaction_response_crc;
  u_int32  transaction_response_pid_err;
  u_int32  toggle_err;
  
} USB_HCErrMsgDis_t;


typedef struct USB_HostCtrlTop_t {
  u_int32            testcase_num;
  u_int32            bfm_clock_cycle;      // BFM Clock cycle time [in ns]
  USB_PipeAPStatic_t sys_pipe_mgr;
  USB_DataAPStatic_t sys_data_mgr;
  USB_IrpAPStatic_t  sys_irp_mgr;
  USB_DevProfile_t   a_dev_prof[USB_MAX_DEV_NUMBER];
  USB_Data_t**       ppa_sys_sar_data;
  USB_Scheduler_t    sys_sched;
  USB_Data_t*        cp_sys_pe_buffer;
  USB_EpBufDescr_t   EpBufDescrAry[ USB_MAX_EP_NUMBER ]; // Manages buffer space for all endpoints
  void*              p_proj_strct;                // Generic pointer to a project or application
                                                  // specific data struct

  USB_HCErrMsgDis_t  ErrorMsgDisabled;

  // Callback function pointers
  pfDeviceAttach_t           pfDeviceAttach;
  pfDevSpeedNeg_t            pfDevSpeedNeg;
  pfBeforeSchLoop_t          pfBeforeSchLoop;
  pfPipePushInIRP_t          pfPipePushInIRP;  
  pfNewIrp_t                 pfNewIrp;  
  pfBeginOfExecIrp_t         pfBeginOfExecIrp;  
  pfEndOfExecIrp_t           pfEndOfExecIrp;  
  pfBeginOfTransaction_t     pfBeginOfTransaction;  
  pfEndOfTransaction_t       pfEndOfTransaction;  
  pfBeforeSof_t              pfBeforeSof;  
  pfBeginOfExecPeriodList_t  pfBeginOfExecPeriodList;  
  pfBeginOfExecCtrlList_t    pfBeginOfExecCtrlList;  
  pfBeginOfExecBulkList_t    pfBeginOfExecBulkList;  
  pfSetDevProf_t             pfSetDevProf;  
  pfSetEpDescr_t             pfSetEpDescr;  

} USB_HostCtrlTop_t;

void carbonXUsbHostCtrlTopInit();
void carbonXUsbHostCtrlTopFree();

void carbonXUsbSetMsgLevel(int log_dbg_level, int scr_dbg_level);
void carbonXUsbSetDevSpeed(USB_TransSpeed_e speed);
void carbonXUsbSetStartFrame(USB_PipeDescr_t* p_pipe, u_int32 uframe);

u_int32 carbonXUsbGetFrameCnt();
void carbonXUsbResetFrameCnt();
void carbonXUsbSetFrameCnt(u_int32 frame_cnt);

void carbonXUsbSofSetError(u_int32 frame_cnt_begin, u_int32 frame_cnt_end, u_int32 sof_err_mask);

void USB_CvtInt32ToByteAry( u_int32 a, u_int8 b[4] );

extern char* USB_DirTypeAry[];
char* carbonXUsbCvtDirTypeToString( USB_Direction_e direction_type );

extern char* USB_EndPtTypeAry[];
char* carbonXUsbCvtEndPtTypeToString( USB_EpType_e end_pt_type );

extern char* USB_HcTransStateAry[];
char* carbonXUsbCvtHcTransStateToString( u_int8 HcTransState  );

extern char* USB_PidStringAry[];
char* carbonXUsbCvtPidToString( u_int8   pid );

extern char* USB_SpeedAry[];
char* carbonXUsbCvtSpeedToString( USB_TransSpeed_e bus_speed  );

BOOL carbonXUsbIsDataPid( u_int8 pid );

void carbonXUsbRegisterDeviceAttachCB( USB_HostCtrlTop_t* p_top, pfDeviceAttach_t p_fct );
void carbonXUsbRegisterDevSpeedNegCB( USB_HostCtrlTop_t* p_top, pfDevSpeedNeg_t p_fct );
void carbonXUsbRegisterBeforeSchLoopCB( USB_HostCtrlTop_t* p_top, pfBeforeSchLoop_t p_fct );
void carbonXUsbRegisterPipePushInIRPCB( USB_HostCtrlTop_t* p_top, pfPipePushInIRP_t p_fct );
void carbonXUsbRegisterNewIrpCB( USB_HostCtrlTop_t* p_top, pfNewIrp_t p_fct );
void carbonXUsbRegisterBeginOfExecIrpCB( USB_HostCtrlTop_t* p_top, pfBeginOfExecIrp_t p_fct );
void carbonXUsbRegisterEndOfExecIrpCB( USB_HostCtrlTop_t* p_top, pfEndOfExecIrp_t p_fct );
void carbonXUsbRegisterBeginOfTransactionCB( USB_HostCtrlTop_t* p_top, pfBeginOfTransaction_t p_fct );
void carbonXUsbRegisterEndOfTransactionCB( USB_HostCtrlTop_t* p_top, pfEndOfTransaction_t p_fct );
void carbonXUsbRegisterBeforeSofCB( USB_HostCtrlTop_t* p_top, pfBeforeSof_t p_fct );
void carbonXUsbRegisterBeginOfExecPeriodListCB( USB_HostCtrlTop_t* p_top, pfBeginOfExecPeriodList_t p_fct );
void carbonXUsbRegisterBeginOfExecCtrlListCB( USB_HostCtrlTop_t* p_top, pfBeginOfExecCtrlList_t p_fct );
void carbonXUsbRegisterBeginOfExecBulkListCB( USB_HostCtrlTop_t* p_top, pfBeginOfExecBulkList_t p_fct );
void carbonXUsbRegisterSetDevProfCB( USB_HostCtrlTop_t* p_top, pfSetDevProf_t p_fct );
void carbonXUsbRegisterSetEpDescrCB( USB_HostCtrlTop_t* p_top, pfSetEpDescr_t p_fct );

void USB_SetSeed();

USB_BufDescr_t* carbonXUsbGetEpBufDescr( u_int32 endpt_num, USB_Direction_e direction );
void carbonXUsbSetEpBufDescr( u_int32 endpt_num, USB_Direction_e direction, USB_BufDescr_t *p_buf_descr );


#endif
