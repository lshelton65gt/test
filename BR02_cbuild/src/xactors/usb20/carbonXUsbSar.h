//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONX_USB_SAR_OP_H
#define CARBONX_USB_SAR_OP_H

void USB_SAR_SegmentData( 
  u_int16          segment_size,
  USB_Data_t*      p_segment,
  USB_Data_t*      p_sdata,
  USB_PipeDescr_t* p_pipe 
);

USB_Data_t* carbonXUsbIrpGetNextSegment(USB_PipeDescr_t* p_pipe);

void carbonXUsbIrpAddNextSegment(USB_Data_t* p_rcv_data,USB_PipeDescr_t* p_pipe );

#endif
