u_int32 ECTL_GetRandom();
u_int32 PLAT_GetRandomInRange(u_int32 min, u_int32 max);

void * XPORT_GetMyProjectData(void);
void   XPORT_SetMyProjectData(void * data);

void PLAT_SetLogDebugLevel(int level);
void PLAT_SetScreenDebugLevel(int level);

void carbonXUsbEndOfTest(void);
void carbonXUsbSetEptHalt(USB_EndPtDescr_t* p_ept);
void carbonXUsbTB_Start(void);

u_int32 XPORT_ReceivePacket(u_int8 *pkt);
void XPORT_SendPacket(u_int8 *pkt, u_int32 size, u_int32 ipg);

void USB_BufferSwizzle(u_int32 *buffer, int size);
