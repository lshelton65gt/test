
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.5  2004/10/21 16:21:32  zippelius
 *               : Moved some functions. Renamed some functions
 *               :
 *               : Revision 1.4  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.3  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.2  2004/03/26 15:00:17  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.1  2003/11/25 09:41:10  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.0  2003/09/27 06:34:12  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:42  adam
 *               : updated file directory
 *               :
 *               : Revision 1.1  2003/09/08 07:54:01  serlina
 *               : add
 *               :
 ***************************************************************************/


#ifndef USB_DATA_HL_SUPPORT_H
#define USB_DATA_HL_SUPPORT_H

#define USB_DATA_PA_GROW_SIZE 10;

void USB_DATA_PA_Init(USB_DataAPStatic_t*  p_data_mgr);

void USB_DATA_PA_Reset(USB_DataAPStatic_t*  p_data_mgr);

void USB_DATA_PA_Free (USB_DataAPStatic_t*  p_data_mgr);

BOOL USB_DATA_PA_IsFull(USB_DataAPStatic_t*  p_data_mgr);

USB_Data_t** USB_DATA_PA_Cur(USB_DataAPStatic_t*  p_data_mgr);

USB_DataAPStatic_t* USB_DATA_PA_GetDataAP();

#endif
