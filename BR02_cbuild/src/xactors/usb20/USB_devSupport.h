//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef USB_DEVSUPPORT_H
#define USB_DEVSUPPORT_H

void USB_DEV_MsgHeader();

void USB_DEV_MsgTotal();

void USB_DEV_Msg(USB_DevProfile_t* p_dev);

void USB_DEV_MsgEnd();

USB_DevProfile_t*   carbonXUsbGetDevProf(u_int32 dev_id);

void USB_DEV_DevProfReset(USB_DevProfile_t* p_dev_prof);

void carbonXUsbSetDevProf(
  BOOL                  is_split,
  u_int8                dev_id,
  u_int8                dev_addr,
  u_int32               port_num,
  USB_DevType_e         dev_type,
  USB_SpeedMode_e	speed_mode,
  USB_TransSpeed_e      speed,
  u_int32               def_max_pk_size
);

void carbonXUsbDevReport(BOOL is_one,u_int32 dev_id);



#endif

/*

$Log$
Revision 1.1  2005/11/10 20:40:53  hoglund
Reviewed by: knutson
Tests Done: checkin xactors
Bugs Affected: none

USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
because of the period.
SPI-3 and SPI-4 now have test suites.  They also have name changes on the
verilog source files.
Utopia2 also has name changes on the verilog source files.

Revision 1.2  2005/10/20 18:23:28  jstein
Reviewed by: Dylan Dobbyn
Tests Done: scripts/checkin + xactor regression suite
Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
added html/xactors dir, and html/xactors/adding_xactors.txt doc

Revision 1.1  2005/09/22 20:49:22  jstein
Reviewed by: Mark Seneski
Tests Done: xactor regression, checkin suite
Bugs Affected: none - Adding new functionality - transactors
  Initial checkin Carbon VSP Transactor library
  ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0

Revision 1.3  2004/08/11 01:59:18  fredieu
Fix to copyright statement


*/

