//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef USB_SCHEDULE_H
#define USB_SCHEDULE_H

// Function to process all the IRPs pending according to bus BW and IRP
// priority, first periodic, then control, then bulk.
void carbonXUsbExecIRPs(u_int32 ipg);

#endif
