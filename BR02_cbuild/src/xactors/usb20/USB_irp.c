//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file includes all functions relating to IRP operation,
 *                 It can be classified into 3 groups:
 *                 [USB_*, USB_IRP_*, USB_IRP_PA_*]
 *
 *                 USB_* are API functions that user can use for IRP related
 *                 operations. They include:
 *
 *                 carbonXUsbIrpCreate();
 *                 carbonXUsbIrpGetStat();
 *                 carbonXUsbIrpReport();
 *                 carbonXUsbIrpDestroy();
 *                 carbonXUsbIrpSetError();
 *                 carbonXUsbGetIrp();
 *                 carbonXUsbGetTransferStage();
 *
 *                 USB_IRP_* are functions that SVC uses to operate on IRP,
 *                 they include:
 *
 *                 1. USB_IRP_MsgHeader();
 *                 2. USB_IRP_MsgHeaderAll();
 *                 3. USB_IRP_MsgTotal();
 *                 4. USB_IRP_MsgTotalAll();
 *                 5. USB_IRP_Msg();
 *                 6. USB_IRP_MsgEnd();
 *                 7. USB_IRP_GetNewIrp ();
 *                 8. USB_IRP_Clear();
 *                 9. USB_PIPE_RetireCompletedIRPs ();
 *                 10.USB_IRP_Update();
 *                 11.USB_IRP_StdReqAnalysis();
 *                 12.USB_IRP_ReqGetStat();
 *                 13.USB_IRP_ReqClearFea();
 *                 14.USB_IRP_ReqSetFea();
 *                 15.USB_IRP_ReqGetDesc();
 *                 16.USB_IRP_ReqSetDesc();
 *                 17.USB_IRP_ReqGetCfg();
 *                 18.USB_IRP_ReqSetCfg();
 *                 19.USB_IRP_ReqGetIf();
 *                 20.USB_IRP_ReqSetIf();
 *                 21.USB_IRP_ReqSynchFrame();
 *                 22.USB_IRP_ReqClearTTBuffer();
 *                 23.USB_IRP_ReqResetTT();
 *                 24.USB_IRP_ReqGetTTStat();
 *                 25.USB_IRP_ReqStopTT();
 *
 *                 USB_IRP_PA_* are functions that SVC uses to operate on the 
 *                 IRP pointer array struct, they include:
 *
 *                 1. USB_IRP_PA_IsFull();
 *                 2. USB_IRP_PA_Cur();
 *                 3. USB_IRP_PA_Init();
 *                 4. USB_IRP_PA_Reset();
 *                 5. USB_IRP_PA_Free();
 *
 *                 Note:
 *                 1. IRP and Pipe.
 *                   In this implementation, it is assumed that a unique IRP can 
 *                   exist in at most one pipe.
 *                   This implies, if user wants to multi-cast an IRP into multiple
 *                   pipes, the user has to make a copy of the IRP with its own ID first.
 *                 2. IRP state.
 *                   Each IRP has one of 4 states [USB_IRP_STAT_READY, USB_IRP_STAT_INPROG, 
 *                   USB_IRP_STAT_COMPL, USB_IRP_STAT_ABORT].
 *                   Without a user intervention an IRP goes through this life cycle:
 *                   It is READY, when it is added to the pipe, INPROG, while it is being transmitted,
 *                   and it gets updated to "COMPL" or "ABORT" after it is terminates.
 *
 ***************************************************************************/

#include "USB_support.h"


//%FS===========================================================================
// USB_IRP_MsgHeader
//
// Description:
//   This function prints the IRP message header for the IRP List in 
//   the specific pipe.
//
// Parameter: 
//   u_int32   pipe_id   I
//     It points to the specific pipe.
// Return: 
//   void 
//%FE===========================================================================
void USB_IRP_MsgHeader(u_int32 pipe_id){
  MSG_Printf("\n");
  //MSG_LibDebug("usb",2,"\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
  MSG_Printf("+ Report IRP Information for pipe #%d\n",pipe_id);
  MSG_Printf("+----------------------------------------------------------------------\n");
}

//%FS===========================================================================
// USB_IRP_MsgHeaderAll
//
// Description:
//   This function prints the IRP message header for all IRPs in the IRP 
//   arrayed pointer struct.
//
// Parameter: 
//   void
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_MsgHeaderAll(){
  MSG_Printf("\n");
  //MSG_LibDebug("usb",2,"\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
  MSG_Printf("+ Report IRP Information \n");
  MSG_Printf("+----------------------------------------------------------------------\n");

}



//%FS===========================================================================
// USB_IRP_MsgTotal
//
// Description:
//   This function prints all IRPs statistic message for all IRPs 
//   in the IRP list of the specific pipe.
//   1. Calculated the IRPs message in the OUT FIFO of the specific pipe.
//   2. Calculated the IRPs message in the IN FIFO of the specific pipe.
//   3. Printf above message.
//
// Parameter: 
//   USB_PipeDescr_t* p_pipe I
//     It points to the specific pipe. 
//     All IRPs message in the IRP list of this pipe will be reported.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_MsgTotal(USB_PipeDescr_t* p_pipe){
  u_int32          i;
  u_int32          irp_cnt     = 0;
  u_int32          rdy_cnt     = 0;
  u_int32          com_cnt     = 0;
  u_int32          in_prog_cnt = 0;
  u_int32          abrt_cnt    = 0;
  USB_Irp_t*       p_irp;
  USB_IrpLink_t*   p_irp_link=0;
  
  if(p_pipe != NULL){
    //----------------- step 1 --------------------
    for (i=0;i<p_pipe->a_irp_c.count;i++){
      
      if(i == 0){
        p_irp_link = p_pipe->a_irp_c.p_head;
      }
      else p_irp_link = p_irp_link->p_next;
      
      if(p_irp_link != NULL) p_irp = p_irp_link->p_irp;
      else break;

      irp_cnt += 1;
      
      switch( p_irp->status){
        case USB_IRP_STAT_READY:{
          rdy_cnt += 1;
          break;
        }
        case USB_IRP_STAT_INPROG:{
          in_prog_cnt += 1;
          break;
        }
        case USB_IRP_STAT_COMPL:{
          com_cnt += 1;
          break;
        }
        case USB_IRP_STAT_ABORT:{
          abrt_cnt += 1;
          break;
        }
      }//end of switch
    }
    
    //----------------- step 2 --------------------
    for (i=0;i<p_pipe->a_irp_uc.count;i++){
      
      if(i == 0){
        p_irp_link = p_pipe->a_irp_uc.p_head;
      }
      else p_irp_link = p_irp_link->p_next;
      
      if(p_irp_link != NULL) p_irp = p_irp_link->p_irp;
      else break;
      
      irp_cnt += 1;
      
      switch( p_irp->status){
        case USB_IRP_STAT_READY:{
          rdy_cnt += 1;
          break;
        }
        case USB_IRP_STAT_INPROG:{
          in_prog_cnt += 1;
          break;
        }
        case USB_IRP_STAT_COMPL:{
          com_cnt += 1;
          break;
        }
        case USB_IRP_STAT_ABORT:{
          abrt_cnt += 1;
          break;
        }
      }//end of switch
    }

    //----------------- step 3 --------------------
    MSG_Printf("+ The total number of IRPs                 : %d\n", irp_cnt);
    MSG_Printf("+ The total number of IRPs not yet started : %d\n", rdy_cnt);
    MSG_Printf("+ The total number of IRPs in progress     : %d\n", in_prog_cnt);
    MSG_Printf("+ The total number of completed IRPs       : %d\n", com_cnt);
    MSG_Printf("+ The total number of aborted IRPs         : %d\n", abrt_cnt);
    MSG_Printf("+----------------------------------------------------------------------\n");
  }
}

//%FS===========================================================================
// USB_IRP_MsgTotalAll
//
// Description:
//   This function prints all statistics for all IRPs in 
//   the IRP arrayed pointer struct.
//   1. Caculated the number of the completed IRPs and the un-completed IRPs 
//      and the aborted IRPs.
//   2. Print above number.
//
// Parameter: 
//   void
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_MsgTotalAll(){
  u_int32          i;
  u_int32          irp_cnt     = 0;
  u_int32          rdy_cnt     = 0;
  u_int32          in_prog_cnt = 0;
  u_int32          abrt_cnt    = 0;
  u_int32          com_cnt     = 0;
  USB_Irp_t*       p_irp;
  USB_HostCtrlTop_t*       p_top;
  
  //-------------- step 1 ------------------
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  for (i=0;i<p_top->sys_irp_mgr.count;i++){
    irp_cnt += 1;
    p_irp = *(p_top->sys_irp_mgr.ppa_irp_head + i);
      
    if(p_irp == NULL) continue;
      
    switch( p_irp->status){
      case USB_IRP_STAT_READY:{
	rdy_cnt += 1;
	break;
        }
      case USB_IRP_STAT_INPROG:{
        in_prog_cnt += 1;
        break;
      }
      case USB_IRP_STAT_COMPL:{
        com_cnt += 1;
        break;
      }
      case USB_IRP_STAT_ABORT:{
        abrt_cnt += 1;
        break;
      }
    }//end of switch
  }
  
  //-------------- step 2 ------------------  
  MSG_Printf("+ The total number of IRPs                   : %d\n", irp_cnt);
    MSG_Printf("+ The total number of IRPs not yet started : %d]n", rdy_cnt);
  MSG_Printf("+ The total number of completed IRPs         : %d\n", com_cnt);
  MSG_Printf("+ The total number of uncompleted IRPs       : %d\n", in_prog_cnt);
  MSG_Printf("+ The total number of aborted IRPs           : %d\n", abrt_cnt);
  MSG_Printf("+----------------------------------------------------------------------\n");
  
}

//%FS===========================================================================
// USB_IRP_Msg
//
// Description:
//   This function reports information about the specified IRP.
//   1. report the IRP ID
//   2. report the IRP transfer type and the IRP transfer stage for the control 
//      transfer.
//   3. report the data size that will be sent and the data size that have 
//      been sent, or the data size that have been received.
//   4. report the IRP status.
//
// Parameter:
//   USB_Irp_t* p_irp  I
//     It points to the IRP that will be reported.
//
// Return: 
//   void 
//%FE===========================================================================
void USB_IRP_Msg(USB_Irp_t* p_irp){
  
  //----------------- step 1 ---------------------
  MSG_Printf("+   Information for IRP #%d:\n",p_irp->irp_id);
  MSG_Printf("+\n");
  MSG_Printf("+     Description:\n");
  
  
  if(p_irp->buddy_id == -1){
    MSG_Printf("+       Buddy IRP                 : None\n"); 
  }
  else MSG_Printf("+       Buddy IRP                 : #%d\n", p_irp->buddy_id);

  //----------------- step 2 ---------------------
  switch(p_irp->trans_type){
    case USB_IRP_TYPE_OUT:
      MSG_Printf("+       IRP Transfer type         : Data OUT\n");
      break;
    
    case USB_IRP_TYPE_IN:
      MSG_Printf("+       IRP Transfer type         : Data IN\n");
      break;
    
    case USB_IRP_TYPE_CTRL_OUT:
      MSG_Printf("+       IRP Transfer type         : Control OUT\n");
      break;
    
    case USB_IRP_TYPE_CTRL_IN:
      MSG_Printf("+       IRP Transfer type         : Control IN\n");
      break;
    
  }

  if(p_irp->p_setup_send_data != NULL){
    switch(p_irp->stage){
      case USB_TRANS_STAGE_SETUP:{
        MSG_Printf("+       IRP stage                 : Setup\n");
        break;
      }
      case USB_TRANS_STAGE_OUT:{
        MSG_Printf("+       IRP stage                 : Data OUT\n");
        break;
      }
      case USB_TRANS_STAGE_IN:{
        MSG_Printf("+       IRP stage                 : Data IN\n");
        break;
      }
      case USB_TRANS_STAGE_STAT:{
        MSG_Printf("+       IRP stage                 : Status\n");
        break;
      }
    } 
  }
  
  //----------------- step 3 ---------------------
  if(p_irp->p_setup_send_data != NULL){
    MSG_Printf("+       Setup [send] data (bytes) : %d\n",p_irp->p_setup_send_data->size);
    MSG_Printf("+          Data bytes sent so far : %d\n",p_irp->setup_sent_cnt);

  }
  
  if(p_irp->p_send_data != NULL){
    MSG_Printf("+       Send data size (bytes)    : %d\n",p_irp->p_send_data->size);
    MSG_Printf("+          Data bytes sent so far : %d\n",p_irp->sent_cnt);
  }
  
  if(p_irp->p_rcv_data != NULL){
    MSG_Printf("+       Received data size (bytes): %d\n",p_irp->p_rcv_data->size);
  }
  
  //----------------- step 4 ---------------------
  switch (p_irp->status){
    case USB_IRP_STAT_READY:{
      MSG_Printf("+       Current status            : Net yet started\n");
      break;
    }
    case USB_IRP_STAT_INPROG:{
      MSG_Printf("+       Current status            : Inprocess\n");
      break;
    }
    case USB_IRP_STAT_COMPL:{
      MSG_Printf("+       Current status            : Completed\n");
      break;
    }
    case USB_IRP_STAT_ABORT:{
      MSG_Printf("+       Current status            : Aborted\n");
      break;
    }
    default:
      MSG_Panic("USB IRP -- Illegal status = %d\n",p_irp->status);
      break;

  }//end of IRP status
  
  MSG_Printf("+----------------------------------------------------------------------\n");

}


//%FS===========================================================================
// USB_IRP_MsgEnd
//
// Description:
//   This function prints the IRP tail message.
//
// Parameter: 
//   void
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_MsgEnd(){
  MSG_Printf("+ End of IRP report\n");
  MSG_Printf("+----------------------------------------------------------------------\n");
}



//%FS===========================================================================
// USB_IRP_GetNewIrp
//
// Description:
//   This function gets a single new IRP structure.
//   1. Extend IRP Hash List if it is FULL.
//   2. Allocate new USB_Irp_t X, reset X, attach X to the tail of the
//      the pointer array struct, and extend pointer array struct count by 1.
//   3. returns X.
//
// Parameters: 
//   void
//
// Return: 
//   USB_Irp_t*
//     It points to the new IRP.
//
//%FE===========================================================================
USB_Irp_t* USB_IRP_GetNewIrp (){
  USB_IrpAPStatic_t*    p_irp_mgr;
  
  USB_Irp_t**   pp_irp_cur;
  USB_Irp_t**   pp_head_new;
  
  USB_Irp_t*    p_irp_new;
  
  u_int32       i;
  
  p_irp_mgr = &( ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_irp_mgr);
  
  //---------------- step 1 ------------------------
  if( USB_IRP_PA_IsFull(p_irp_mgr) ) {
    p_irp_mgr->size += USB_IRP_GROW_SIZE;
    pp_head_new = (USB_Irp_t**)tbp_realloc(p_irp_mgr->ppa_irp_head,
					   (sizeof(USB_Irp_t*)*(p_irp_mgr->size - USB_IRP_GROW_SIZE)),
					   (sizeof(USB_Irp_t*)*p_irp_mgr->size) );
   
    if ( pp_head_new == NULL ) {
      MSG_Panic("USB_IRP_GetNewIrp(): Out of memory!\n");
    } 
    p_irp_mgr->ppa_irp_head = pp_head_new;
    
    // Reset the unused portion to NULL for safety.    
    for(i=p_irp_mgr->count;i<p_irp_mgr->size;i++){
      *(pp_head_new + i) = NULL;
    }
  }
  
  //---------------- step 2 ------------------------
  pp_irp_cur = USB_IRP_PA_Cur(p_irp_mgr);

  p_irp_new = (USB_Irp_t*)calloc(1, sizeof(USB_Irp_t));
    
  if (p_irp_new == NULL) {
    MSG_Panic("USB_IRP_GetNewIrp(): Out of memory!\n");
  }
  
  p_irp_new->irp_id = p_irp_mgr->count;
  USB_IRP_Clear(p_irp_new);
  
  *pp_irp_cur = p_irp_new;
  p_irp_mgr->count ++;
  
  //---------------- step 3 ------------------------
  return(p_irp_new);
}



//%FS===========================================================================
// USB_IRP_PA_IsFull
//
// Description:
//  This function checks if the IRP pointer array struct is full or not.
//
// Parameters:
//   USB_IrpAPStatic_t*  p_irp_mgr  I
//     It points to the IRP pointer array struct.
//
// Return:
//   If TRUE, the IRP pointer array struct is full.
//   If FALSE, the IRP pointer array struct has space.
//%FE===========================================================================
BOOL USB_IRP_PA_IsFull(USB_IrpAPStatic_t*  p_irp_mgr) {
  return (p_irp_mgr->size <= p_irp_mgr->count);
}



//%FS===========================================================================
// USB_IRP_PA_Cur
//
// Description:
//  This function gets the tail IRP pointer in the tail of 
//  the IRP pointer array struct.
//
// Parameters:
//   USB_IrpAPStatic_t*  p_irp_mgr  I
//     It points to the IRP pointer array struct.
//
// Return:
//   USB_Irp_t**
//     The tail IRP pointer will be returned.
//%FE===========================================================================
USB_Irp_t** USB_IRP_PA_Cur(USB_IrpAPStatic_t*  p_irp_mgr) {
  return ( p_irp_mgr->ppa_irp_head + p_irp_mgr->count );
}




//%FS===========================================================================
// USB_IRP_Clear
//
// Description:
//   This function resets the specific IRP to default value.
//   1. Reset irp_sys_record to empty.
//   2. Reset the IRP structure to empty.
//   3. Reset buddy_id to 0, which indiciates no "buddy". We chose 0 since this
//      is the least likely value to be compared within a system.
//
// Parameters: 
//   USB_Irp_t*     pi     I
//     It points to the specific IRP.
//
// Return: 
//   void
//%FE===========================================================================
void USB_IRP_Clear(USB_Irp_t* p_irp){
     
  //-------------- step 1 -------------------
  p_irp->irp_sys_record.has_been_seg = FALSE;
  p_irp->irp_sys_record.data_part  = USB_PART_ALL;
  p_irp->irp_sys_record.err_mask   = 0;             
  p_irp->rcv_bytes = 0;
  
  //-------------- step 2 -------------------
  p_irp->status		           = USB_IRP_STAT_READY;
  p_irp->sent_cnt                  = 0;
  p_irp->trans_type                = USB_IRP_TYPE_OUT;
  p_irp->p_send_data               = NULL;
  p_irp->p_rcv_data                = NULL;
  p_irp->p_setup_send_data         = NULL;
  p_irp->setup_sent_cnt            = 0;
  p_irp->stage                     = USB_TRANS_STAGE_SETUP;
  
  //-------------- step 3 -------------------
  p_irp->buddy_id                  = -1;
}


//%FS===========================================================================
// USB_IRP_Update
//
// Description:
//   This function updates the IRP when a transfer is completed.
//   1. If the transaction is the last one of an IRP, then set the IRP status to 
//      USB_IRP_STAT_COMPL.
//   2. IRPs that are have completed will be moved from the In to the Out FIFO.
//   3. The has_been_seg will be set to FALSE, pointing to the IRP has been update
//      but not segmentation again.
//
// Parameters: 
//   USB_PipeDescr_t*  p_pipe
//     It points to the specific pipe. In this pipe, the IRP will be updated.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_Update(USB_PipeDescr_t*  p_pipe){
  USB_Irp_t*        p_irp;

  if (p_pipe == NULL) MSG_Panic("USB IRP Update -- Pipe Descriptor Pointer is NULL\n");
 
  if(p_pipe->a_irp_uc.p_head != NULL){
    // The uncompleted list is not empty -> get the IRP at the front of the list
    p_irp = p_pipe->a_irp_uc.p_head->p_irp;
  
    //-------------- step 1 --------------------
    if ( p_irp->irp_sys_record.data_part == USB_PART_END ||
	 ( USB_CHK_HsIsoOutSplit(p_pipe) && (p_irp->irp_sys_record.data_part == USB_PART_ALL) )
       ){
      // Set the IRP state to COMPLETED.
      p_irp->status = USB_IRP_STAT_COMPL;
    }
  
    //-------------- step 2 --------------------  
    while( p_pipe->a_irp_uc.p_head->p_irp->status != USB_IRP_STAT_READY &&
	   p_pipe->a_irp_uc.p_head->p_irp->status != USB_IRP_STAT_INPROG ){
      USB_PIPE_RetireCompletedIRPs(p_pipe);
      if(p_pipe == NULL) break;
      if(p_pipe->a_irp_uc.p_head == NULL) break;
    }
  
    //-------------- step 3 --------------------
    if((p_pipe != NULL)&&(p_pipe->a_irp_uc.p_head != NULL)){
      p_irp->irp_sys_record.has_been_seg = FALSE;
    }
  }
}



//%FS===========================================================================
// USB_IRP_PA_Init
//
// Description:
//   This function only initializes the IRP pointer array struct, setting the value to 
//   default value.
//
// Parameters: 
//   USB_IrpAPStatic_t*  p_irp_mgr  I
//     It points to the IRP pointer array struct.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_PA_Init(USB_IrpAPStatic_t*  p_irp_mgr){
 
  p_irp_mgr->size          = 0;
  p_irp_mgr->count     = 0;
  p_irp_mgr->ppa_irp_head  = NULL;
  
}




//%FS===========================================================================
// USB_IRP_PA_Reset
//
// Description:
//   This function resets the IRP pointer array struct. It will free all IRPs and set 
//   the value to default value.
//
// Parameters: 
//   USB_IrpAPStatic_t*  p_irp_mgr  I
//     It points to the IRP pointer array struct.
//
// Return:
//   void
//
// Note:
//   Once reset the all IRP information will be lost.
//%FE===========================================================================
void USB_IRP_PA_Reset(USB_IrpAPStatic_t*  p_irp_mgr){
  USB_IRP_PA_Free(p_irp_mgr);
 
  p_irp_mgr->size          = 0;
  p_irp_mgr->count     = 0;
  p_irp_mgr->ppa_irp_head = NULL;
  
}


//%FS===========================================================================
// USB_IRP_PA_Free
//
// Description:
//   This function frees the IRP pointer array struct. All IRPs in this 
//   pointer array struct will be lost.
//
// Parameters: 
//   USB_IrpAPStatic_t*  p_irp_mgr  I
//     It points to the IRP pointer array struct.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_PA_Free (USB_IrpAPStatic_t*  p_irp_mgr){
  u_int32         i;
  USB_Irp_t*      p_irp;
  
  if ( p_irp_mgr->ppa_irp_head != NULL ){
    
    for(i=0;i<p_irp_mgr->count;i++){
      
      p_irp = *(p_irp_mgr->ppa_irp_head + i);
      
      if (p_irp != NULL){
      	tbp_free(p_irp);
      }       
    }
    tbp_free (p_irp_mgr->ppa_irp_head);
  }
}



//%FS===========================================================================
// carbonXUsbGetTransferStage
//
// Description:
//   This function gets the current stage for the specific IRP .
//
// Parameters: 
//   USB_Irp_t*   p_irp      I
//     It points to the IRP being inquired about.
//
// Return:
//   USB_IrpTransType_e
//     If OK, the IRP current stage will be returned.
//     If error, USB_TRANS_STAGE_SETUP will be returned, the warning message 
//     will be reported.
//%FE===========================================================================
USB_TransStage_e  carbonXUsbGetTransferStage (USB_Irp_t* p_irp){
  if(p_irp != NULL) return(p_irp->stage);
  else {
    MSG_Warn("carbonXUsbGetTransferStage(): Input pointer cannot be NULL.\n");
    return(USB_TRANS_STAGE_SETUP);
  }
}



//%FS===========================================================================
// carbonXUsbGetIrp
//
// Description:
//   This function gets the IRP pointer by the IRP ID.
//
// Parameters: 
//   int irp_id  I
//     It points to the IRP being requested.
//
// Return:
//   USB_Irp_t*
//     If OK, the requested IRP pointer will be returned.
//     If failing, NULL will be returned, the warning message will be reported.
//%FE===========================================================================
USB_Irp_t*  carbonXUsbGetIrp(int irp_id){
  USB_Irp_t*   p_irp;
  USB_HostCtrlTop_t*   p_top;
  
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
 
  if(irp_id < (int)p_top->sys_irp_mgr.count){
    p_irp = p_top->sys_irp_mgr.ppa_irp_head[irp_id];
    return(p_irp);
  }
  else {
    MSG_Warn("carbonXUsbGetIrp(): Request IRP ID #%d error.\n",irp_id);
    return(NULL);
  }
}


//%FS===========================================================================
// USB_IRP_ReqGetStat
//
// Description:
//   This function analyzes the request GET_STATUS
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqGetStat(USB_Irp_t* p_irp){
  u_int8       request_type;
  u_int16      w_index;
  u_int32      i;
  u_int8*      p_rdata;
  
  request_type = *p_irp->p_setup_send_data->p_data;
    
  w_index      = *(p_irp->p_setup_send_data->p_data + 4);
  w_index    <<= 8;
  w_index     += *(p_irp->p_setup_send_data->p_data + 5);
  
  if(request_type == 0x80){      
    MSG_Printf("The Request 'Get_Status' for the Device status is completed.\n");
  }
  else if(request_type == 0x81){
    MSG_Printf("The Request 'Get_Status' for the Interface %d status is completed.\n",w_index);
  }
  else if(request_type == 0x82){
    MSG_Printf("The Request 'Get_Status' for the Endpoint %d status is completed.\n",w_index);
  }
  else if(request_type == 0xA0){
    MSG_Printf("The Request 'Get_Status' for the Hub status is completed.\n");
  }
  else if(request_type == 0xA3){
    MSG_Printf("The Request 'Get_Status' for the Hub port %d status is completed.\n",w_index);
  }
  
  if(p_irp->p_rcv_data != NULL){
    MSG_Printf("The status value is ");
    
    p_rdata = p_irp->p_rcv_data->p_data;
    for(i=0; i<p_irp->p_rcv_data->size;i++){
      p_rdata += i;
      MSG_Printf("%x ",*p_rdata);
    }
    MSG_Printf("\n");
  }
}


//%FS===========================================================================
// USB_IRP_ReqClearFea
//
// Description:
//   This function analyzes the request CLEAR_FEATURE. Used to print Debug statements
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqClearFea(USB_Irp_t* p_irp){
  u_int8       request_type;
  u_int8       request;
  u_int16      w_value;
  u_int8       selector;
  u_int8       number;
  u_int16      w_index;
  
  request_type = *p_irp->p_setup_send_data->p_data;
  request      = *(p_irp->p_setup_send_data->p_data + 1);

  w_value      = *(p_irp->p_setup_send_data->p_data + 2);
  w_value    <<= 8;
  w_value     += *(p_irp->p_setup_send_data->p_data + 3);
    
  w_index      = *(p_irp->p_setup_send_data->p_data + 4);
  w_index    <<= 8;
  w_index     += *(p_irp->p_setup_send_data->p_data + 5);
  
  selector     = *(p_irp->p_setup_send_data->p_data + 4);
  number         = *(p_irp->p_setup_send_data->p_data + 5);
  
  if( (request_type == 0)&&(w_value == 2) ){
    MSG_Printf("The Request 'Clear_Feature' for the Device Feature is completed.\n");
    MSG_Printf("The Feature Selected is TEST_MODE.\n");

    if(w_index == 1) MSG_Printf("Test Mode is Test_J.\n");
    else if(w_index == 2) MSG_Printf("Test Mode is Test_K.\n");
    else if(w_index == 3) MSG_Printf("Test Mode is Test_SE0_NAK.\n");
    else if(w_index == 4) MSG_Printf("Test Mode is Test_Packet.\n");
    else if(w_index == 5) MSG_Printf("Test Mode is Test_Force_Enable.\n");
  }
  else if( (request_type == 0)&&(w_value == 1) ){
    MSG_Printf("The Request 'Clear_Feature' for the Device Feature is completed.\n");
    MSG_Printf("The Feature Selected is DEVICE_REMOTE_WAKEUP.\n");
  }
  else if(request_type == 0){
    MSG_Printf("The Request 'Clear_Feature' for the Device Feature is completed.\n");
  }
  else if(request_type == 1){
    MSG_Printf("The Request 'Clear_Feature' for the Interface %d Feature is completed.\n",w_index);
  }
  else if (request_type == 2){
    MSG_Printf("The Request 'Clear_Feature' for the Endpoint %d Feature is completed.\n",w_index);
    if(w_value == 0) MSG_Printf("The Feature Selected is ENDPOINT_HALT.\n");
  }
  else if (request_type == 0x20){
    MSG_Printf("The Request 'Clear_Feature' for the Hub Feature is completed.\n");
    if(w_value == 0) MSG_Printf("The Feature Seletor is C_HUB_LOCAL_POWER.\n");
    else if(w_value == 1) MSG_Printf("The Feature Selected is C_HUB_OVER_CURRENT.\n");
  }
  else if (request_type == 0x23){
    MSG_Printf("The Request 'Clear_Feature' for the Hub Port %d  Feature is completed.\n",number);
    if(w_value == 0) MSG_Printf("The Feature Selected is PORT_CONNECTION.\n");
    else if(w_value == 1) MSG_Printf("The Feature Selected is PORT_ENABLE.\n");
    else if(w_value == 2) MSG_Printf("The Feature Selected is PORT_SUSPEND.\n");
    else if(w_value == 3) MSG_Printf("The Feature Selected is PORT_OVER_CURRENT.\n");
    else if(w_value == 4) MSG_Printf("The Feature Selected is PORT_RESET.\n");
    else if(w_value == 8) MSG_Printf("The Feature Selected is PORT_POWER.\n");
    else if(w_value == 9) MSG_Printf("The Feature Selected is PORT_LOW_SPEED.\n");
    else if(w_value == 16) MSG_Printf("The Feature Selected is C_PORT_CONNECTION.\n");
    else if(w_value == 17) MSG_Printf("The Feature Selected is C_PORT_ENABLE.\n");
    else if(w_value == 18) MSG_Printf("The Feature Selected is C_PORT_SUSPEND.\n");
    else if(w_value == 19) MSG_Printf("The Feature Selected is C_PORT_OVER_CURRENT.\n");
    else if(w_value == 20) MSG_Printf("The Feature Selected is C_PORT_RESET.\n");
    else if(w_value == 21) MSG_Printf("The Feature Selected is PORT_TEST.\n");
    else if(w_value == 22) MSG_Printf("The Feature Selected is PORT_INDICATOR.\n");
      
    if(selector == 1) MSG_Printf("Test Mode is Test_J.\n");
    else if(selector== 2) MSG_Printf("Test Mode is Test_K.\n");
    else if(selector == 3) MSG_Printf("Test Mode is Test_SE0_NAK.\n");
    else if(selector == 4) MSG_Printf("Test Mode is Test_Packet.\n");
    else if(selector == 5) MSG_Printf("Test Mode is Test_Force_Enalbe.\n");
    
  }
  
}


//%FS===========================================================================
// USB_IRP_ReqSetFea
//
// Description:
//   This function analyzes the request SET_FEATURE. Used for debugging with prints
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqSetFea(USB_Irp_t* p_irp){
  u_int8       request_type;
  u_int16      w_value;
  u_int8       number;
  u_int8       selector;
  u_int16      w_index;
  
  request_type = *p_irp->p_setup_send_data->p_data;

  w_value      = *(p_irp->p_setup_send_data->p_data + 2);
  w_value    <<= 8;
  w_value     += *(p_irp->p_setup_send_data->p_data + 3);

  w_index      = *(p_irp->p_setup_send_data->p_data + 4);
  w_index    <<= 8;
  w_index     += *(p_irp->p_setup_send_data->p_data + 5);
    
  selector     = *(p_irp->p_setup_send_data->p_data + 4);
  number         = *(p_irp->p_setup_send_data->p_data + 5);
  
  if( (request_type == 0)&&(w_value == 2) ){
    MSG_Printf("The Request 'Set_Feature' for the Device Feature is completed.\n");
    MSG_Printf("The Feature Selected is TEST_MODE.\n");

    if(w_index == 1) MSG_Printf("Test Mode is Test_J.\n");
    else if(w_index == 2) MSG_Printf("Test Mode is Test_K.\n");
    else if(w_index == 3) MSG_Printf("Test Mode is Test_SE0_NAK.\n");
    else if(w_index == 4) MSG_Printf("Test Mode is Test_Packet.\n");
    else if(w_index == 5) MSG_Printf("Test Mode is Test_Force_Enalbe.\n");
  }
  else if( (request_type == 0)&&(w_value == 1) ){
    MSG_Printf("The Request 'Set_Feature' for the Device Feature is completed.\n");
    MSG_Printf("The Feature Selected is DEVICE_REMOTE_WAKEUP.\n");
  }
  else if(request_type == 0){
    MSG_Printf("The Request 'Set_Feature' for the Device Feature is completed.\n");
  }
  else if(request_type == 1){
    MSG_Printf("The Request 'Set_Feature' for the Interface %d Feature is completed.\n", w_index);
  }
  else if(request_type == 2){
    MSG_Printf("The Request 'Set_Feature' for the Endpoint %d Feature is completed.\n",w_index);
    if(w_value == 0) MSG_Printf("The Feature Selected is ENDPOINT_HALT.\n");
  }
  else if (request_type == 0x20){
    MSG_Printf("The Request 'Set_Feature' for the Hub Feature is completed.\n");
    if(w_value == 0) MSG_Printf("The Feature Selected is C_HUB_LOCAL_POWER.\n");
    else if(w_value == 1) MSG_Printf("The Feature Selected is C_HUB_OVER_CURRENT.\n");
  }
  else if (request_type == 0x23){
    MSG_Printf("The Request 'Set_Feature' for the Hub Port %d Feature is completed.\n",number);
  
    if(w_value == 0) MSG_Printf("The Feature Selected is PORT_CONNECTION.\n");
    else if(w_value == 1) MSG_Printf("The Feature Selected is PORT_ENABLE.\n");
    else if(w_value == 2) MSG_Printf("The Feature Selected is PORT_SUSPEND.\n");
    else if(w_value == 3) MSG_Printf("The Feature Selected is PORT_OVER_CURRENT.\n");
    else if(w_value == 4) MSG_Printf("The Feature Selected is PORT_RESET.\n");
    else if(w_value == 8) MSG_Printf("The Feature Selected is PORT_POWER.\n");
    else if(w_value == 9) MSG_Printf("The Feature Selected is PORT_LOW_SPEED.\n");
    else if(w_value == 16) MSG_Printf("The Feature Selected is C_PORT_CONNECTION.\n");
    else if(w_value == 17) MSG_Printf("The Feature Selected is C_PORT_ENABLE.\n");
    else if(w_value == 18) MSG_Printf("The Feature Selected is C_PORT_SUSPEND.\n");
    else if(w_value == 19) MSG_Printf("The Feature Selected is C_PORT_OVER_CURRENT.\n");
    else if(w_value == 20) MSG_Printf("The Feature Selected is C_PORT_RESET.\n");
    else if(w_value == 21) MSG_Printf("The Feature Selected is PORT_TEST.\n");
    else if(w_value == 22) MSG_Printf("The Feature Selected is PORT_INDICATOR.\n");
    
    if(selector == 1) MSG_Printf("Test Mode is Test_J.\n");
    else if(selector == 2) MSG_Printf("Test Mode is Test_K.\n");
    else if(selector == 3) MSG_Printf("Test Mode is Test_SE0_NAK.\n");
    else if(selector == 4) MSG_Printf("Test Mode is Test_Packet.\n");
    else if(selector == 5) MSG_Printf("Test Mode is Test_Force_Enalbe.\n");
  }
}

//%FS===========================================================================
// USB_IRP_ReqGetDesc
//
// Description:
//   This function analysises the request GET_DESCRIPTOR and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqGetDesc(USB_Irp_t* p_irp){
  u_int8       request_type;
  u_int16      w_value;
  u_int8       descr_type;

  request_type = *p_irp->p_setup_send_data->p_data;

  w_value      = *(p_irp->p_setup_send_data->p_data + 2);
  w_value    <<= 8;
  w_value     += *(p_irp->p_setup_send_data->p_data + 3);

  descr_type = w_value << 8;
        
  if(descr_type == 1){
    MSG_Printf("The Request 'Get_Descriptor' for Device Descriptor is completed.\n");
  }
  else if(descr_type == 2){
    MSG_Printf("The Request 'Get_Descriptor' for Configration Descriptor is completed.\n");
  }
  else if(descr_type == 3){
    MSG_Printf("The Request 'Get_Descriptor' for String Descriptor is completed.\n");
  }
  else if(descr_type == 4){
    MSG_Printf("The Request 'Get_Descriptor' for Interface Descriptor is completed.\n");
  }
  else if(descr_type == 5){
    MSG_Printf("The Request 'Get_Descriptor' for Endpoint Descriptor is completed.\n");
  }
  else if(descr_type == 6){
    MSG_Printf("The Request 'Get_Descriptor' for Device_Qualifier Descriptor is completed.\n");
  }
  else if(descr_type == 7){
    MSG_Printf("The Request 'Get_Descriptor' for Other_Speed_Configration Descriptor is completed.\n");
  }
  else if(descr_type == 8){
    MSG_Printf("The Request 'Get_Descriptor' for Interface_Power Descriptor is completed.\n");
  }
  else if(descr_type == 0x29){
    MSG_Printf("The Request 'Get_Descriptor' for Hub Descriptor is completed.\n");
  }

}



//%FS===========================================================================
// USB_IRP_ReqSetDesc
//
// Description:
//   This function analysises the request SET_DESCRIPTOR and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqSetDesc(USB_Irp_t* p_irp){
  u_int8       request_type;
  u_int16      w_value;
  u_int8       descr_type;

  request_type = *p_irp->p_setup_send_data->p_data;

  w_value      = *(p_irp->p_setup_send_data->p_data + 2);
  w_value    <<= 8;
  w_value     += *(p_irp->p_setup_send_data->p_data + 3);

  descr_type = w_value << 8;
       
  if(descr_type == 1){
    MSG_Printf("The Request 'Set_Descriptor' for Device Descriptor is completed.\n");
  }
  else if(descr_type == 2){
    MSG_Printf("The Request 'Set_Descriptor' for Configration Descriptor is completed.\n");
  }
  else if(descr_type == 3){
    MSG_Printf("The Request 'Set_Descriptor' for String Descriptor is completed.\n");
  }
  else if(descr_type == 4){
    MSG_Printf("The Request 'Set_Descriptor' for Interface Descriptor is completed.\n");
  }
  else if(descr_type == 5){
    MSG_Printf("The Request 'Set_Descriptor' for Endpoint Descriptor is completed.\n");
  }
  else if(descr_type == 6){
    MSG_Printf("The Request 'Set_Descriptor' for Device_Qualifier Descriptor is completed.\n");
  }
  else if(descr_type == 7){
    MSG_Printf("The Request 'Set_Descriptor' for Other_Speed_Configration Descriptor is completed.\n");
  }
  else if(descr_type == 8){
    MSG_Printf("The Request 'Set_Descriptor' for Interface_Power Descriptor is completed.\n");
  }
  else if(descr_type == 0x29){
    MSG_Printf("The Request 'Set_Descriptor' for Hub Descriptor is completed.\n");
  }

}



//%FS===========================================================================
// USB_IRP_ReqGetCfg
//
// Description:
//   This function analysises the request GET_CONFIGURATION and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqGetCfg(USB_Irp_t* p_irp){
  u_int8*      p_rdata;
  u_int32      i;

  MSG_Printf("The Request 'Get_Configuration' is completed.\n");
        
  if(p_irp->p_rcv_data != NULL){
    MSG_Printf("The Configuration value is ");
         
    p_rdata = p_irp->p_rcv_data->p_data;
  
    for(i=0; i<p_irp->p_rcv_data->size;i++){
      p_rdata += i;
      MSG_Printf("%x ",*p_rdata);
    }
  
    MSG_Printf("\n");
  }

}

//%FS===========================================================================
// USB_IRP_ReqSetCfg
//
// Description:
//   This function analysises the request SET_CONFIGURATION and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqSetCfg(USB_Irp_t* p_irp){
  u_int16      w_value;

  w_value      = *(p_irp->p_setup_send_data->p_data + 2);
  w_value    <<= 8;
  w_value     += *(p_irp->p_setup_send_data->p_data + 3);

  MSG_Printf("The Request 'Set_Configuration' is completed.\n");
  MSG_Printf("The Configuration value is %x\n",w_value);

}

//%FS===========================================================================
// USB_IRP_ReqGetIf
//
// Description:
//   This function analysises the request GET_INTERFACE and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqGetIf(USB_Irp_t* p_irp){
  u_int8*   p_rdata;
  u_int32   i;
  u_int16   w_index;

  w_index      = *(p_irp->p_setup_send_data->p_data + 4);
  w_index    <<= 8;
  w_index     += *(p_irp->p_setup_send_data->p_data + 5);  
 
  MSG_Printf("The Request 'Get_Interface' for Interface %d is completed.\n",w_index);
                
  if(p_irp->p_rcv_data != NULL){
    MSG_Printf("The Alternate Setting value is ");
    
    p_rdata = p_irp->p_rcv_data->p_data;
  
    for(i=0; i<p_irp->p_rcv_data->size;i++){
      p_rdata += i;
      MSG_Printf("%x ",*p_rdata);
    }
  
    MSG_Printf("\n");
  }
        
}


//%FS===========================================================================
// USB_IRP_ReqSetIf
//
// Description:
//   This function analysises the request SET_INTERFACE and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqSetIf(USB_Irp_t* p_irp){
  u_int16      w_value;
  u_int16      w_index;

  w_value      = *(p_irp->p_setup_send_data->p_data + 2);
  w_value    <<= 8;
  w_value     += *(p_irp->p_setup_send_data->p_data + 3);

  w_index      = *(p_irp->p_setup_send_data->p_data + 4);
  w_index    <<= 8;
  w_index     += *(p_irp->p_setup_send_data->p_data + 5);

  MSG_Printf("The Request 'Set_Interface' for Interface %d is completed.\n",w_index);
  MSG_Printf("The Alternate Setting value is %x\n",w_value);

}

//%FS===========================================================================
// USB_IRP_ReqSynchFrame
//
// Description:
//   This function analysises the request SYNCH_FRAME and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqSynchFrame(USB_Irp_t* p_irp){
  u_int16      w_index;
  u_int32      i;
  u_int8*      p_rdata;

  w_index      = *(p_irp->p_setup_send_data->p_data + 4);
  w_index    <<= 8;
  w_index     += *(p_irp->p_setup_send_data->p_data + 5);

  MSG_Printf("The Request 'Synch_Frame' for Endpoint %d is completed.\n",w_index);
  MSG_Printf("The Frame Number value is ");
  p_rdata = p_irp->p_rcv_data->p_data;
 
  for(i=0; i<p_irp->p_rcv_data->size;i++){
		//    p_rdata += i;
    MSG_Printf("%x ",*p_rdata);
    p_rdata += 1;
  }
  MSG_Printf("\n");

}

//%FS===========================================================================
// USB_IRP_ReqClearTTBuffer
//
// Description:
//   This function analysises the request CLEAR_TT_BUFFER and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqClearTTBuffer(USB_Irp_t* p_irp){
  u_int8  dev_add;
  u_int8  temp;
  u_int8  ep_num;
  u_int16 w_index;
  
  dev_add = *(p_irp->p_setup_send_data->p_data + 2);
  dev_add <<= 5;
  temp    = *(p_irp->p_setup_send_data->p_data + 3);
  temp    >>= 4;
  dev_add += temp;
  
  ep_num  = *(p_irp->p_setup_send_data->p_data + 3);
  ep_num  &= 0x0f;
  
  w_index = *(p_irp->p_setup_send_data->p_data + 4);
  w_index <<= 8;
  w_index += *(p_irp->p_setup_send_data->p_data + 5);
  
  MSG_Printf("The Request 'Clear_TT_Buffer' for TT port %d, Device address %d, Endpoint %d is completed.\n",w_index, dev_add, ep_num);
}


//%FS===========================================================================
// USB_IRP_ReqResetTT
//
// Description:
//   This function analysises the request RESEET_TT and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqResetTT(USB_Irp_t* p_irp){
  u_int16 w_index;
  
  w_index = *(p_irp->p_setup_send_data->p_data + 4);
  w_index <<= 8;
  w_index += *(p_irp->p_setup_send_data->p_data + 5);
  
  MSG_Printf("The Request 'Reset_TT' for TT port %d is completed.\n",w_index);
}


//%FS===========================================================================
// USB_IRP_ReqGetTTStat
//
// Description:
//   This function analysises the request GET_TT_STATE and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqGetTTStat(USB_Irp_t* p_irp){
  u_int16 w_index;
  u_int8* p_rdata;
  u_int32 i;
 
  w_index = *(p_irp->p_setup_send_data->p_data + 4);
  w_index <<= 8;
  w_index += *(p_irp->p_setup_send_data->p_data + 5);
  
  MSG_Printf("The Request 'Get_TT_State' for TT port %d is completed.\n",w_index);
  
  if(p_irp->p_rcv_data != NULL){
    MSG_Printf("The TT status value is ");
    
    p_rdata = p_irp->p_rcv_data->p_data;
    for(i=0; i<p_irp->p_rcv_data->size;i++){
      p_rdata += i;
      MSG_Printf("%x ",*p_rdata);
    }
    MSG_Printf("\n");
  }
}


//%FS===========================================================================
// USB_IRP_ReqStopTT
//
// Description:
//   This function analysises the request STOP_TT and prints for debug purpose.
//   
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_ReqStopTT(USB_Irp_t* p_irp){
  u_int16 w_index;
  
  w_index = *(p_irp->p_setup_send_data->p_data + 4);
  w_index <<= 8;
  w_index += *(p_irp->p_setup_send_data->p_data + 5);
  
  MSG_Printf("The Request 'Stop_TT' for TT port %d is completed.\n",w_index);
}


//%FS===========================================================================
// USB_IRP_StdReqAnalysis
//
// Description:
//   This function analyzes a standard request:
//   1. Request: GET_STATUS
//   2. Request: CLEAR_FEATURE
//   3. Request: SET_FEATURE
//   4. Request: GET_DESCRIPTOR
//   5. Request: SET_DESCRIPTOR
//   7. Request: GET_CONFIGUREATION/CLEAR_TT_BUFFER
//   8. Request: SET_CONFIGUREATION/RESET_TT
//   9. Request: GET_INTERFACE/GET_TT_STATE
//   10.Request: SET_INTERFACE/STOP_TT
//   11.Request: SYNCH_FRAME
//
// Parameters: 
//   USB_Irp_t* p_irp  I
//     It points to the specific IRP.
//
// Return:
//   void
//%FE===========================================================================
void USB_IRP_StdReqAnalysis(USB_Irp_t* p_irp){
  u_int8*      p_sdata = NULL;
  u_int8       request_type;
  u_int8       request;
  u_int16      w_value;

  MSG_Printf("Calling USB_IRP_StdReqAnalysis for completed IRP #%d.\n",p_irp->irp_id);

  if(p_irp != NULL){
    if(p_irp->p_setup_send_data != NULL){
      if(p_irp->p_setup_send_data->size == 8){
        p_sdata = p_irp->p_setup_send_data->p_data;
      }
    }
  }
  else MSG_Warn("USB_IRP_StdReqAnalysis(): Input IPR pointer cannot be NULL.\n");
  
  if(p_sdata != NULL){
    request_type = *p_sdata;
    request      = *(p_sdata + 1);

    w_value      = *(p_irp->p_setup_send_data->p_data + 2);
    w_value    <<= 8;
    w_value     += *(p_irp->p_setup_send_data->p_data + 3);

    switch( request ){
      
      //---------------------------- step 1 -----------------------------
      // Get_Status
      case 0:{
        USB_IRP_ReqGetStat(p_irp);
        break;
      }
      
      //---------------------------- step 2 -----------------------------
      // Clear_Feature
      case 1:{
        USB_IRP_ReqClearFea(p_irp);
        break;
      }
      
      //---------------------------- step 3 -----------------------------
      // Set_Feature
      case 3:{
        USB_IRP_ReqSetFea(p_irp);
        break;
      }
              
      //---------------------------- step 4 -----------------------------
      // Set_Address
      case 5:{
        MSG_Printf("The Request 'Set_Address' value %d is completed.\n", w_value);
        
        break;
      }
      
      //---------------------------- step 5 -----------------------------
      // Get_Descriptor
      case 6:{
        USB_IRP_ReqGetDesc(p_irp);
        break;
      }
      
      //---------------------------- step 6 -----------------------------
      // Set_Descriptor
      case 7:{
        USB_IRP_ReqSetDesc(p_irp);
        break;
      }
      
      //---------------------------- step 7 -----------------------------
      // Get_Configuration
      case 8:{
        if(request_type == 0x80) USB_IRP_ReqGetCfg(p_irp);
        else if(request_type == 0x23) USB_IRP_ReqClearTTBuffer(p_irp);
        break;
      }
      
      //---------------------------- step 8 -----------------------------
      // Set_Configuration
      case 9:{
        if(request_type == 0) USB_IRP_ReqSetCfg(p_irp);
        else if(request_type == 0x23) USB_IRP_ReqResetTT(p_irp);
        break;
      }
      
      //---------------------------- step 9 -----------------------------
      // Get_Interface
      case 10:{
        if(request_type == 0x81) USB_IRP_ReqGetIf(p_irp);
        else if(request_type == 0xA3) USB_IRP_ReqGetTTStat(p_irp);
        break;
      }
      
      //---------------------------- step 10 -----------------------------
      // Set_Interface
      case 11:{
        if(request_type == 1) USB_IRP_ReqSetIf(p_irp);
        else if(request_type == 0x23) USB_IRP_ReqStopTT(p_irp);
        break;
      }
      
      //---------------------------- step 11 -----------------------------
      // Synch_Frame
      case 12:{
        USB_IRP_ReqSynchFrame(p_irp);
        break;
      }
      default: break;
    }    
  }
}




//%FS===========================================================================
// carbonXUsbIrpCreate
//
// Description:
//   This function creates a new IRP
//
// Parameters: 
//   USB_Data_t*  p_setup_data  I 
//     It points to the Setup data of a Control transfer.
//     Should be NULL in all other cases
//
//   USB_Data_t*  p_send_data   I
//     It points to the data to be sent out during Out transfer(s).
//     Should be NULL, if no data will be sent out.
//
//   USB_IrpTransType_e   trans_type   I
//     IRP transfer type.
//
//   int   buddy_id   I
//     ID of buddy [Out] IRP. -1 indicates that there is no buddy IRP
//     Note: If buddy_id != -1 is specified p_buddy_irp->p_send_data->size
//           will be used to determine the number of [payload] bytes that
//           shall be received. The rcv_bytes parameter shall be ignored in that case
//
//
// Return:
//   USB_Irp_t*
//     If there is no error, then a pointer to the new IRP will be returned.
//     Otherwise, NULL will be returned and the warning message will be issued.
//%FE===========================================================================
USB_Irp_t* carbonXUsbIrpCreate(
			 USB_Data_t*        p_setup_data, 
			 USB_Data_t*        p_send_data,
			 USB_IrpTransType_e trans_type,
			 int     	    buddy_id
			 ){

  USB_Irp_t          *p_irp = NULL;
  USB_Irp_t          *p_buddy_irp;
  USB_HostCtrlTop_t  *p_top;
  
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_irp = USB_IRP_GetNewIrp();
    
  p_irp->p_setup_send_data = p_setup_data;
  p_irp->p_send_data       = p_send_data;
  p_irp->trans_type	   = trans_type;
  p_irp->buddy_id          = buddy_id;

  if ( buddy_id != -1 ) {
    p_buddy_irp = carbonXUsbGetIrp ( buddy_id );
    p_irp->rcv_bytes = p_buddy_irp->p_send_data->size;
  }

  // Execute Callback function, if it exists
  if ( p_top->pfNewIrp ) {
    // callback function exists.
    // -> Call it.
    p_top->pfNewIrp( p_top, p_irp );
  }

  return p_irp;
}



USB_Irp_t* USB_IRP_CreateRandom(
  int*               p_size,
  USB_Data_t**       pp_setup_data, 
  USB_Data_t**       pp_send_data,
  USB_IrpTransType_e trans_type,
  USB_EndPtDescr_t*  p_endpoint,
  int                buddy_id
) {

  u_int32             irp_size_range;
  u_int32             ep_max_packet_size;
  USB_Irp_t          *p_irp	    = NULL;
  USB_Data_t         *p_data	    = NULL;
  USB_Data_t         *p_setup_data = NULL;

  (void)pp_setup_data; (void)pp_send_data;
  ep_max_packet_size = p_endpoint->max_packet_size;

  if ( *p_size == -1 ) {
    if ( trans_type == USB_IRP_TYPE_OUT ) {
      irp_size_range = PLAT_GetRandomInRange( 1, 7 );

      switch(irp_size_range){
      case 1:
	(*p_size) =PLAT_GetRandomInRange( 1, ep_max_packet_size-1 );
	break;
	
      case 2:
	// Corner case
	(*p_size) = ep_max_packet_size;
	break;

      case 3:
	// Corner case
	(*p_size) = ep_max_packet_size+1;
	break;
	
      case 4:
	(*p_size) = PLAT_GetRandomInRange( ep_max_packet_size+2, 3*ep_max_packet_size-1 );
	break;
	
      case 5:
	// Corner case
	(*p_size) = 3*ep_max_packet_size;
	break;

      case 6:
	// Corner case
	(*p_size) = 3*ep_max_packet_size +1;
	break;
	
      case 7:
	(*p_size) = PLAT_GetRandomInRange( 3*ep_max_packet_size+2, 8*ep_max_packet_size );
	break;
	
      default: break;
      }
      
      p_data    = carbonXUsbCreateRandDataInRange(0, 255, (*p_size));
      p_irp = carbonXUsbIrpCreate(NULL, p_data, USB_IRP_TYPE_OUT, -1);
    }
    else if ( trans_type == USB_IRP_TYPE_IN ) {
      p_irp = carbonXUsbIrpCreate(NULL, NULL, USB_IRP_TYPE_IN, buddy_id);
      if ( p_size != NULL ) {
	p_irp->rcv_bytes = *p_size;
      }
    }
    else {
      // Must be a CTRL Irp
      p_setup_data = carbonXUsbCreateRandDataInRange(0, 255, 8);
    }
  }

  return p_irp;
}




//%FS===========================================================================
// carbonXUsbIrpGetStat
// 
// Description:
//   This function the current status of the specified IRP
//
// Parameters: 
//   USB_Irp_t*   p_irp      I
//     It points to the IRP being inquired about.
//
// Return:
//   USB_IrpStatus_e
//     If OK, the IRP current status will be return.
//     If error, USB_IRP_STAT_INPROG will be returned, a warning message 
//     will be reported.
//%FE===========================================================================
USB_IrpStatus_e carbonXUsbIrpGetStat(USB_Irp_t* p_irp){
 
  if(p_irp != NULL) {
    return(p_irp->status);
  }
  else {
    MSG_Error("USB_IrpGetstat(): p_irp is NULL.\n");
    return(USB_IRP_STAT_INPROG);
  }
}



//%FS===========================================================================
// carbonXUsbIrpReport
//
// Description:
//   This function reports the IRP information.
//
// Parameters: 
//   BOOL  is_one  I
//     If TRUE and the p_pipe is not NULL, all IRPs message in this pipe 
//     will be reported.
//     If TRUE , the p_pipe is NULL and the p_irp is not NULL, a single IRP 
//     message will be reported.
//     If it is FALSE, report all IRPs message in the IRP pointer array struct.
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe. If is_one is TRUE, the value is effectual.
//   USB_Irp_t*  p_irp I
//     It points to the specific IRP. If is_one is TRUE, and the p_pipe is NULL,
//     the value is effectual.
// 
// Return: 
//   void
//%FE===========================================================================
void carbonXUsbIrpReport(BOOL is_one,USB_PipeDescr_t* p_pipe,USB_Irp_t*  p_irp){
  u_int32            i;
  u_int32            flag = 0;
  USB_Irp_t*         p_temp;
  USB_HostCtrlTop_t*         p_top;
  USB_IrpLink_t*     p_irp_link = 0;

  if(is_one){
    if(p_pipe != NULL){
      flag = 1;
    }
    else if(p_irp != NULL){
      flag = 2;
    }
    else MSG_Warn("carbonXUsbIrpReport(): Report the IRPs in an Pipe or an IRP, input pointer cannot be NULL.\n");

  }
  else flag = 3;

  if(flag != 0){  
    if(flag == 3){
      USB_IRP_MsgHeaderAll();
      USB_IRP_MsgTotalAll();
      
      p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
      for(i=0;i<p_top->sys_irp_mgr.count;i++){
        p_temp = *(p_top->sys_irp_mgr.ppa_irp_head + i);

        if(p_temp == NULL) continue;
 
        USB_IRP_Msg(p_temp);
      }
    }
    else if(flag == 2){
      USB_IRP_MsgHeaderAll();
      USB_IRP_Msg(p_irp);
    }
    else if(flag == 1){
       USB_IRP_MsgHeader(p_pipe->pipe_id);
       USB_IRP_MsgTotal(p_pipe);
       
       for(i=0;i<p_pipe->a_irp_c.count;i++){
        if(i == 0){
          p_irp_link = p_pipe->a_irp_c.p_head;
        }
        else p_irp_link = p_irp_link->p_next;
        
        if(p_irp_link != NULL) p_temp = p_irp_link->p_irp;
        else break;
        
        USB_IRP_Msg(p_temp);
      }
      for(i=0;i<p_pipe->a_irp_uc.count;i++){
        if(i == 0){
          p_irp_link = p_pipe->a_irp_uc.p_head;
        }
        else p_irp_link = p_irp_link->p_next;
        
        if(p_irp_link != NULL) p_temp = p_irp_link->p_irp;
        else break;
        
        USB_IRP_Msg(p_temp);
      }
    }

    USB_IRP_MsgEnd();
  }
}

//%FS===========================================================================
// carbonXUsbIrpDestroy
// 
// Description:
//   This function destroys the specific IRP.
//   1. Search the IRP in the FIFO of all pipes and remove it from FIFO.
//   1.1 First try to locate irp in the uncompleted list
//   1.2 Second try to locate irp in the completed list
//   2. Search the IRP in the IRP pointer array struct , freeing the IRP and setting the
//      the IRP pointer to NULL.
//
// Parameters: 
//   USB_Irp_t*   p_irp      I
//     It points to the IRP being destroyed.
//     if NULL, warning will be printed and nothing will be done.
//
// Return:
//   void
//
// Note:
//   This function assumes a single IRP can exist in at most
//   one pipe.
//%FE===========================================================================
void carbonXUsbIrpDestroy(USB_Irp_t* p_irp){
  USB_PipeDescr_t* p_pipe;
  USB_Irp_t**      pp_irp;
  u_int32          i,j;
  u_int32          pipe_cnt;
  USB_IrpLink_t*   p_chk;
  USB_IrpLink_t*   p_temp;
  BOOL             is_found = FALSE; //indicates if IRP has been found in the pipe.
  
  pipe_cnt = ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_pipe_mgr.count;
  
  if(p_irp != NULL){
    
    //------------- step 1 -----------------
    // Search in Pipe and desctroy
    for(i=0;i<pipe_cnt;i++){
      if(is_found)break;
      
      p_pipe = carbonXUsbGetPipeDescr(i);
      
      if(p_pipe == NULL) continue;

      //------------- step 1.1 -----------------      
      for(j=0;j<p_pipe->a_irp_uc.count;j++){
        if(is_found)break;
        if(i == 0){
          p_chk = p_pipe->a_irp_uc.p_head;
        }
        else p_chk = p_chk->p_next;
        
        if(p_chk->p_irp == p_irp){
          p_temp = p_chk->p_prev;
          if(p_temp != NULL) p_temp->p_next = p_chk->p_next;
          else if(p_chk == p_pipe->a_irp_uc.p_head) p_pipe->a_irp_uc.p_head = p_chk->p_next;
          
          p_temp = p_chk->p_next;
          if(p_temp != NULL) p_temp->p_prev = p_chk->p_prev;
          else if(p_chk == p_pipe->a_irp_uc.p_tail) p_pipe->a_irp_uc.p_tail = p_chk->p_prev;
          
          tbp_free(p_chk);
          
          p_pipe->a_irp_uc.count --;
          is_found = TRUE;
        }
        
      } //end for loop for a_irp_uc
      
      //------------- step 1.2 -----------------      
      for(j=0;j<p_pipe->a_irp_c.count;j++){
        if(is_found)break;
        if(i == 0){
          p_chk = p_pipe->a_irp_c.p_head;
        }
        else p_chk = p_chk->p_next;
        
        if(p_chk->p_irp == p_irp){
          p_temp = p_chk->p_prev;
          if(p_temp != NULL) p_temp->p_next = p_chk->p_next;
          else if(p_chk == p_pipe->a_irp_uc.p_head) p_pipe->a_irp_uc.p_head = p_chk->p_next;
          
          p_temp = p_chk->p_next;
          if(p_temp != NULL) p_temp->p_prev = p_chk->p_prev;
          else if(p_chk == p_pipe->a_irp_uc.p_tail) p_pipe->a_irp_uc.p_tail = p_chk->p_prev;
          
          tbp_free(p_chk);
        
          p_pipe->a_irp_c.count --;
          is_found = TRUE;
        }

      } //end for loop for a_irp_uc
    } //end for loop for pipe
    
    //------------- step 2 -----------------
    // Search in IRP list and Destroy
    pp_irp = ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_irp_mgr.ppa_irp_head + p_irp->irp_id;
    
    if(pp_irp != NULL){
      if( *pp_irp == p_irp){
        MSG_Printf("Destroy IRP #%d, all information on this IRP has been lost.\n",p_irp->irp_id);
        tbp_free(p_irp);
        *pp_irp = NULL;
      }
      else MSG_Warn("carbonXUsbIrpDestroy(): Input IRP cannot be found in the IRP List.\n");
    }
    else MSG_Warn("carbonXUsbIrpDestroy(): Input IRP cannot be found in the IRP List.\n");
    
  }
  else MSG_Warn("carbonXUsbIrpDestroy(): Input pointer cannot be NULL.\n");

}


//%FS===========================================================================
// carbonXUsbIrpSetError
//
// Description:
//   This function selects error types that will be injected during the execution
//   of the specified IRP range
//
// Parameters: 
//   int irp_id_start	I	
//     The space point error should be first inserted.
//   int irp_id_end		I	
//     The space point error should not be further inserted.
//   u_int32 err_mask		I	
//     The error inserted
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbIrpSetError(int irp_id_start, int irp_id_end, u_int16 err_mask) {
  USB_Irp_t*  p_irp;
  USB_HostCtrlTop_t*  p_top;
  s_int32 i = 0;
  
  p_top = (USB_HostCtrlTop_t* )XPORT_GetMyProjectData();
  
  if (irp_id_start >= (int)p_top->sys_irp_mgr.count) {
    MSG_Error("The irp_id_start of %d is invalid (bigger than irp list size of %d). Ignoring this error setting.\n", irp_id_start, p_top->sys_irp_mgr.count);
  }

  else if (irp_id_start > irp_id_end) {
    MSG_Error("The irp_id_start of %d is invalid (bigger than irp_id_end of %d). Ignoring this error setting.\n", irp_id_start, irp_id_end);
  }

  else {
    // if the end irp id is larger than size, end to size
    if(irp_id_end > (int) p_top->sys_irp_mgr.count) {
      irp_id_end  = p_top->sys_irp_mgr.count;
    }
    for (i=irp_id_start; i<= irp_id_end; i++){
      p_irp = *(p_top->sys_irp_mgr.ppa_irp_head + i);
      if(p_irp == NULL) continue;
      p_irp->irp_sys_record.err_mask = err_mask;
    }
  }
}




//%FS==================================================================
//
// carbonXUsbCreateRandDataInRange
//
// Description:
//   carbonXUsbCreateRandDataInRange generates random data.
//
// Parameter:
//   u_int32 range_start  I
//     It is the start range for data.
//   u_int32 range_end  I
//     It is the end range for data.
//   u_int32 test_data_size  I
//     It is the generated data size.
//
// Return value:
//   USB_Data_t*
//     The return value is the pointer which points to the
//     generated data.
//
//%FE==================================================================
USB_Data_t* carbonXUsbCreateRandDataInRange(u_int32 range_start, u_int32 range_end,
  u_int32 test_data_size) {

  u_int32 i = 0;
  u_int8  data[test_data_size];
  USB_Data_t* p_test_data;
  

  if(test_data_size < 1) 
    MSG_Warn("carbonXUsbCreateRandDataInRange called with size of %d\n",test_data_size);
  
  if(range_end > 255) MSG_Warn("carbonXUsbCreateRandDataInRange called with range value greater than 255!\n");
  if(range_start > 255) MSG_Warn("carbonXUsbCreateRandDataInRange called with range value greater than 255!\n");

  // Generate random data
  for(i = 0; i < test_data_size; i++) {
    data[i] = (u_int8)PLAT_GetRandomInRange(range_start, range_end);
  }
  
  p_test_data = carbonXUsbDataRealloc(NULL, test_data_size);
  
  p_test_data->size = test_data_size;
  memcpy(p_test_data->p_data, data, test_data_size);
  
  return (p_test_data);
}

//%FS==================================================================
//
// carbonXUsbCreateSeqData
//
// Description:
//   carbonXUsbCreateRandDataInRange generates random data.
//
// Parameter:
//   u_int32 range_start  I
//     It is the start range for data.
//   u_int32 test_data_size  I
//     It is the generated data size.
//
// Return value:
//   USB_Data_t*
//     The return value is the pointer which points to the
//     generated data.
//
//%FE==================================================================
USB_Data_t* carbonXUsbCreateSeqData(u_int32 range_start, u_int32 test_data_size) {

  u_int32 i = 0;
  u_int8  data[test_data_size];
  USB_Data_t* p_test_data;
  
  if(test_data_size < 1) 
    MSG_Warn("carbonXUsbCreateSeqData called with size of %d\n",test_data_size);
  
  if(range_start > 255) MSG_Warn("carbonXUsbCreateSeqData called with range value greater than 255!\n");

  // Generate random data
  for(i = 0; i < test_data_size; i++) {
    data[i] = (u_int8)((i+range_start) & 0xff);
  }
  
  p_test_data = carbonXUsbDataRealloc(NULL, test_data_size);
  
  p_test_data->size = test_data_size;
  memcpy(p_test_data->p_data, data, test_data_size);
  
  return (p_test_data);
}


//%FS==================================================================
//
// carbonXUsbIrpSetRcvDataSize
//
// Description:
//   Sets the number of bytes that are expected to be received for this IRP
//
// Parameter:
//   USB_Irp_t*  p_irp  IO
//     It points to the structure USB_Irp_t of the IRP.
//   u_int32  rcv_bytes  I
//     It is the input data total size.
//
// Return value:
//   void
//
//%FE==================================================================
void carbonXUsbIrpSetRcvDataSize(USB_Irp_t* p_irp, u_int32 rcv_bytes) {

  p_irp->rcv_bytes = rcv_bytes;
}
