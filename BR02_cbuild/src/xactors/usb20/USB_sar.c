//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file includes all functions relating to segmentation
 *                 and reassembly operation, it can be classified into 2 groups:
 *                 [USB_*, USB_SAR_*]
 *
 *                 USB_* functions are used by Scheduler for segmentation
 *                 and reassembly related operations, they include:
 *
 *                 1. carbonXUsbIrpGetNextSegment();
 *                 2. carbonXUsbIrpAddNextSegment();
 *                 
 *                 USB_SAR_* are functions that SVC uses to operation on the 
 *                 segmentation and reassembly, they include:
 *
 *                 1. USB_SAR_ReallocAry();
 *                 2. USB_SAR_GetMemory();
 *                 3. USB_SAR_SegmentIrp();
 *                 4. USB_SAR_SegmentData();
 *                 5. USB_SAR_AddPid();
 *                 6. USB_SAR_Reassemble();
 *                 7. USB_SAR_ReassembleData();
 *                 8. USB_SAR_CheckPid();
 *                 9. USB_SAR_HandleLastIrpTrans();
 *                 10.USB_SAR_Pid();
 *                 11.USB_SAR_SetCtrlPid();
 *                 12.USB_SAR_SetBulkAndIntrPid();
 *                 13.USB_SAR_DataToggle();
 *                 14.USB_SAR_SetIsoPid();
 *                 15.USB_SAR_HsIsoOutSplit();
 *                 16.USB_SAR_DataPart();
 *                 17.USB_SAR_IsSmall();
 *
 *
 *               :
 ***************************************************************************/


#include "USB_support.h"

//%FS===========================================================================
// USB_SAR_ReallocAry
//
// Description:
//   This function assigns a single SAR member for a single pipe.
//   1. Extend the array for a new pipe.
//   2. Allocate a USB_Data_t member for the new segmentation memory.
//
// Parameter: 
//   void
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_ReallocAry(){
  USB_HostCtrlTop_t* p_top;
  USB_Data_t**       pp_data;
  USB_Data_t*        p_sar;
  
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  
  //------------------- step 1 --------------------
  p_top->ppa_sys_sar_data = (USB_Data_t**)tbp_realloc(p_top->ppa_sys_sar_data,
						      (sizeof(USB_Data_t*)*(p_top->sys_pipe_mgr.count - 1)),
						      (sizeof(USB_Data_t*)*p_top->sys_pipe_mgr.count) );
  
  if(p_top->ppa_sys_sar_data == NULL){
    MSG_Panic("USB_SAR_ReallocAry(): Out of memory!\n");
  }
  
  pp_data = p_top->ppa_sys_sar_data + (p_top->sys_pipe_mgr.count - 1);
  
  //------------------- step 2 --------------------
  p_sar = carbonXUsbDataRealloc(NULL,0) ;
  
  *pp_data = p_sar;
}


//%FS===========================================================================
// USB_SAR_GetMemory
//
// Description:
//   This function gets the memory for storing segmentation data.
//   1. Get the segmentation pointer X
//   2. Check if the old segmentation memory size is enough or not. If no, reallocate X
//   3. Return X
//
// Parameters: 
//   u_int32   pipe_id  I
//     It points to the pipe will send or receive data.
//   u_int32   sar_size I
//     It points to the data size will be segmented.
//
// Return:
//   USB_Data_t* 
//     It points to the memory of storing the segmentation data.
//%FE===========================================================================
USB_Data_t*  USB_SAR_GetMemory(u_int32 pipe_id,u_int32 sar_size){
  
  USB_HostCtrlTop_t*    p_top;
  USB_Data_t*   p_sar;
  USB_Data_t*   p_sar_new = NULL;
  
  //------------------- step 1 --------------------
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sar = *(p_top->ppa_sys_sar_data + pipe_id);
  
  //------------------- step 2 --------------------
  if (p_sar->size < (sar_size+1)){
    p_sar_new = carbonXUsbDataRealloc(p_sar,(sar_size + 1) );
    *(p_top->ppa_sys_sar_data + pipe_id) = p_sar_new;
    
    //------------------- step 3 --------------------
    return (p_sar_new);

  }
  else return(p_sar); 
}


//%FS===========================================================================
// carbonXUsbIrpGetNextSegment
// 
// Description:
//   This function does the segmentation of an IRP.
//   1. Check, if the head IRP is USB_IRP_STAT_INPROG or not. If not, call
//      USB_IRPL_UpdateOnComplete() to get rid of unnecessary IRPs.
//   2. Check if high-speed ISO OUT start-split transfer. Assign segment size 
//      or not.
//   3. Check data PID and data Part
//   4. Do segmentation
//   5. Return the segmented data
//
// Parameters: 
//  USB_PipeDescr_t* p_pipe   I   
//    It points to the pipe. The current IRP in this pipe IN FIFO will be segmented.
//
// Return:
//   USB_Data_t*  
//     It points to the data after segmentation.
//     If error, NULL will be returned, the warning message will be reported.
//%FE===========================================================================

USB_Data_t* carbonXUsbIrpGetNextSegment(USB_PipeDescr_t* p_pipe){
  USB_Data_t*   	   p_segment = NULL;
  u_int16	           segment_size;
  
  //---------------- step 1 ---------------------  
  if(p_pipe == NULL){
    MSG_Panic("carbonXUsbIrpGetNextSegment(): Input pointer is NULL. Segmentation Failed.\n");
    return(NULL);
  }
  
  if(p_pipe->a_irp_uc.p_head == NULL){

    MSG_Panic("carbonXUsbIrpGetNextSegment(): There are no IRP in Pipe #%d. Segmentation Failed\n",p_pipe->pipe_id);

    return(NULL);
  }
    
  //---------------- step 2 ---------------------
  while( p_pipe->a_irp_uc.p_head->p_irp->status != USB_IRP_STAT_READY &&
	 p_pipe->a_irp_uc.p_head->p_irp->status != USB_IRP_STAT_INPROG ){
    USB_PIPE_RetireCompletedIRPs(p_pipe); 
		if(p_pipe == NULL) return(NULL);
    if(p_pipe->a_irp_uc.p_head == NULL) return(NULL);
  }
  
  //---------------- step 3 ---------------------
  if( USB_CHK_HsIsoOutSplit(p_pipe) ){
    segment_size = 188;
  }
  else {
    segment_size = p_pipe->p_endpt_descr->max_packet_size;
  }

  //---------------- step 4 ---------------------
  //Prepare  PID and data part
  if( !USB_CHK_HsIsoOutSplit(p_pipe) ){
#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> ++++++++ carbonXUsbIrpGetNextSegment: Calling USB_SAR_Pid: \n", PLAT_GetSimTime() );     
#endif
    USB_SAR_Pid(p_pipe, segment_size);
  }
  else {
    USB_SAR_HsIsoOutSplit(p_pipe, segment_size);
  }
  
  p_segment = USB_SAR_GetMemory(p_pipe->pipe_id, segment_size);
  
  USB_SAR_SegmentIrp (p_segment, p_pipe, segment_size);


  //---------------- step 6 ---------------------

  return( p_segment );
} 




//%FS===========================================================================
// USB_SAR_SegmentIrp
//
// Description:
//   Segment an IRP
//   1. Check if the IRP will segment the last transaction or not.
//   2. Create PID for segmentation
//   3. Segment the IRP data to the segmentation data memory.
//   4. Set has_been_seg to TRUE, indicating that the IRP has been segmented.
//
// Parameters: 
//   USB_Data_t*  p_segment   IO
//     Points to memory buffer that will hold the PID and data.
//   USB_PipeDescr_t* p_pipe   I
//     Pipe that has IRP
//   u_int16    segment_size   I
//     The chunk size of the segmented data
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_SegmentIrp (USB_Data_t* p_segment,USB_PipeDescr_t* p_pipe, u_int16  segment_size) {
  USB_Data_t*        p_sdata = NULL;
  u_int32            size;

  // Get [payload] data that needs to be segmented.
  // We use the IRP at the head of the uncompleted IRPs list
  if(p_pipe != NULL) {
    if(p_pipe->a_irp_uc.p_head != NULL){
      if( p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL  &&
          p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_SETUP ){
        p_sdata = p_pipe->a_irp_uc.p_head->p_irp->p_setup_send_data;
      } 
      else {
	p_sdata = p_pipe->a_irp_uc.p_head->p_irp->p_send_data;
      }
    }
    else {
      // There are no uncompleted IRPs. We are done.
      return;
    }
  }
  else {
    // Pipe is NULL. We are done.
    return;
  }
  
  // Check, whether there is data that needs to be sent out
  if (p_sdata != NULL){
  
  //
  //  +-------------------------------------------+
  //  | |IRP n                                    |
  //  | |0xSys_p_data0             0xSys_p_datam| |
  //  | |data0|data1|data2|data3|data4|...|datam| |
  //  | |p_sdata->p_data                          |
  //  |                            p_sdata->size| |
  //  +-------------------------------------------+  
  //  | Segmentation                              |
  //  +-------------------------------------------+
  //  | | PID |data0|data1|data2| ... |           |
  //  | |po_segment                               |
  //  | |<--- Segment DATA Length --->|           |
  //  +-------------------------------------------+
  //
  //
  // +-------------------------------------------+
  // | Control, Interrupt and Bulk Transfer      |
  // |   --(size - sent_cnt) < max_packet_size   |
  // |     --Part: EndData                       |
  // +-------------------------------------------+
  //
  
    //------------------- step 1 ---------------------
    // Following is sign for the last transaction for an IRP 
    if(p_sdata->size > p_pipe->a_irp_uc.p_head->p_irp->sent_cnt){
      size = p_sdata->size - p_pipe->a_irp_uc.p_head->p_irp->sent_cnt;
    }
    else {
      size = 0;
    }
    
    USB_SAR_HandleLastIrpTrans(p_pipe, size);
    
    if(p_pipe->p_endpt_descr->curr_dir != USB_DIR_IN){
      //------------------- step 2 ---------------------    
      //Add PID
      USB_SAR_AddPid ( p_segment, p_pipe );
    
      //------------------- step 3 ---------------------
      //Segment Data
      USB_SAR_SegmentData(segment_size, p_segment, p_sdata,p_pipe);
    
      //------------------- step 4 ---------------------
      p_pipe->a_irp_uc.p_head->p_irp->irp_sys_record.has_been_seg = TRUE;
    }
  }
  else {
    p_segment = carbonXUsbDataRealloc(NULL, 0);
  }

  return;
}


//%FS===========================================================================
// USB_SAR_SegmentData
//
// Description:
//   This function segments the IRP data.
//   1. Check if the segment_size is large than the remaining unsent data.
//   2. Segment the data to the segmentation memory.
//   3. Add 1 to the segmentation size to account for the PID byte.
//
// Parameter:
//   u_int16            segment_size   I
//     Size of data segment(s)
//   USB_Data_t*        p_segment     O
//     Points to the memory storing data being segmented.
//   USB_Data_t*        p_sdata       I
//     Points to the IRP data being sent.
//   USB_PipeDescr_t*   p_pipe        I
//     Points to the IRP of spcific pipe.
//
// Return: 
//   void
//
// Note:
//  The p_segment points to the memory of segmented data.
//%FE===========================================================================
void USB_SAR_SegmentData( 
  u_int16          segment_size,
  USB_Data_t*      p_segment,
  USB_Data_t*      p_sdata,
  USB_PipeDescr_t* p_pipe 
){
  u_int8*    p_temp = NULL;
  u_int8*    p_next = NULL;
  USB_Irp_t* p_irp  = NULL;
  BOOL       is_size_nochg   = TRUE;

  if(p_pipe->a_irp_uc.p_head != NULL){
    p_irp  = p_pipe->a_irp_uc.p_head->p_irp;
    p_next = p_segment->p_data + 1;

  //
  //The following statement blocks have the following effect :
  //[sent_cnt, p_segment] shall be updated from Fig A to Fig B.
  // Fig A.
  // +-------------------------------------------+
  // | |IRP n                                    |
  // | |0xSys_p_data0             0xSys_p_datam| |
  // | |data0|data1|data2|data3|data4|...|datam| |
  // | |p_sdata->p_data                         |
  // |                           p_sdata->size| |
  // | p_temp   = NULL                           |
  // | sent_cnt = 0                              |
  // +-------------------------------------------+  
  // | Segmentation                              |
  // +-------------------------------------------+
  // | | PID |     |     |     |     |           |
  // | |p_segment->p_data                       |
  // |       |p_next                             |
  // |       |<--- Segment size  --->|           |
  // | |<--- Segment DATA Length --->|           |
  // +-------------------------------------------+
  //  Fig B.
  // +-------------------------------------------+
  // | |IRP n                                    |
  // | |0xSys_p_data0             0xSys_p_datam| |
  // | |data0|data1|data2|data3|data4|...|datam| |
  // | |p_sdata->p_data                         |
  // |                           p_sdata->size| |
  // | |p_temp                                   |
  // | sent_cnt = 0 + p_segment->size           |
  // +-------------------------------------------+  
  // | Segmentation                              |
  // +-------------------------------------------+
  // | | PID |data0|data1| ... |                 |
  // | |p_segment->p_data                       |
  // |       |p_next                             |
  // |       |<--- Segment size  --->|           |
  // | |<--- Segment DATA Length --->|           |
  // +-------------------------------------------+
  //
  
    //------------------- step 1 ----------------------
    if( (p_irp->sent_cnt > p_sdata->size)||(p_irp->stage == USB_TRANS_STAGE_STAT) ){
      p_segment->size = 0;
      is_size_nochg = FALSE;
    }
    else if( USB_SAR_IsSmall(p_irp,p_sdata,p_pipe) ){
      if( segment_size >= (p_sdata->size - p_irp->sent_cnt) ){

        p_segment->size = p_sdata->size - p_irp->sent_cnt;
        is_size_nochg = FALSE;
      }
    }
    else{
      if( USB_CHK_HsIsoOutSplit(p_pipe) ){
        if(segment_size > (p_sdata->size - p_irp->sent_cnt) ){
          p_segment->size = p_sdata->size - p_irp->sent_cnt;
          is_size_nochg = FALSE;
        }
      }
    }
  
    if(is_size_nochg) p_segment->size = segment_size;
  
    //------------------- step 2 ----------------------
    p_temp = p_sdata->p_data + p_irp->sent_cnt;
    memcpy ( p_next, p_temp, p_segment->size);
  
    if((p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL)
       &&(p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_SETUP)){
      p_irp->setup_sent_cnt = p_segment->size;
    } 
    else p_irp->sent_cnt += p_segment->size;
  
    //------------------- step 3 ----------------------
    p_segment->size += 1; 
    
  }
}



//%FS===========================================================================
// USB_SAR_AddPid
//
// Description:
//   This function adds the PID to the head of segmented data .
//
// Parameters: 
//   USB_Data_t*  p_segment   I
//     It points to the memory storing PID .
//   USB_PipeDescr_t*  p_pipe      I
//     It points to the current PID being sent.
//
// Return: 
//   void
//%FE===========================================================================
//
//The following statement blocks have the following effect :
//[p_segment] shall be updated from Fig A to Fig B.
// Fig A.
// +-------------------------------------------+
// | Segmentation                              |
// +-------------------------------------------+
// | |     | ... | ... | ... | ... |           |
// | |p_segment                               |
// | |<--- Segment DATA Length --->|           |
// +-------------------------------------------+
//  Fig B.
// +-------------------------------------------+
// | Segmentation                              |
// +-------------------------------------------+
// | | PID | ... | ... | ... | ... |           |
// | |p_segment                               |
// | |<--- Segment DATA Length --->|           |
// +-------------------------------------------+
// 
void USB_SAR_AddPid ( USB_Data_t*  p_segment, USB_PipeDescr_t*  p_pipe ) {

  p_segment->p_data[0] = USB_BuildPid( p_pipe->p_endpt_descr->pid );    
}

//%FS===========================================================================
// carbonXUsbIrpAddNextSegment
//
// Description:
//   This function adds a segment of received data to IRP.
//   1. Check if the received data segment is empty.
//   2. Check data PID
//   3. Do reassembly.
// 
// Parameters: 
//   USB_Data_t* p_rcv_data  I
//     It points to the received data from a completed transfer.
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the pipe. 
//
// Return: 
//   void
//
// Note:
//   If input is zero size data or a NULL pointer, the reassembly will fail.
//%FE===========================================================================
void carbonXUsbIrpAddNextSegment(USB_Data_t* p_rcv_data,USB_PipeDescr_t*  p_pipe){
  
  //--------------------- step 1 -----------------------
  if((p_pipe == NULL)||(p_rcv_data == NULL) ){
    MSG_Warn("carbonXUsbIrpAddNextSegment(): Input pointer is NULL. Reassembly Failed.\n");
  }
  else if (p_rcv_data->size == 0){
    MSG_Warn("carbonXUsbIrpAddNextSegment(): Received an Empty transaction. Reassembly Failed.\n");
  }
  else {
    //--------------------- step 3 -----------------------
    // Determine, what the PID for the received packet should have been
#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> ++++++++ carbonXUsbIrpAddNextSegment: Calling USB_SAR_Pid: \n", PLAT_GetSimTime() );
#endif
    USB_SAR_Pid(p_pipe, p_rcv_data->size-1 );


    //--------------------- step 2 -----------------------
#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> ++++++++ carbonXUsbIrpAddNextSegment: Calling USB_SAR_Reassemble: \n", PLAT_GetSimTime() );
#endif
    USB_SAR_Reassemble(p_rcv_data, p_pipe);    
  }

  return;
}



//%FS==================================================================
//
// USB_SAR_NeedsSar
//
// Description:
//   Check, if the current IRP needs to do be SARed.
//   The following are the conditions that do not require SARing:
//   condition 1: Pipe state is not READY;
//   condition 2: There are no IRPs in the pipe;
//   condition 3: The data of the IRP has already been segmented;
//   condition 4: It is an IN transaction;
//   condition 5: It is a retry transaction;
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  IO
//     It points to the descriptor of the current pipe.
//
// Return value:
//   BOOL
//     The return value is TRUE to indicate that SAR is needed, FALSE to
//     indicate that SAR is not needed.
//
//%FE==================================================================
BOOL USB_SAR_NeedsSar(USB_PipeDescr_t*  p_pipe) {
  
  return ( p_pipe->sys_record.pipe_state != USB_PIPE_STAT_READY ||    
           p_pipe->a_irp_uc.count == 0 ||
           p_pipe->a_irp_uc.p_head->p_irp->irp_sys_record.has_been_seg == TRUE ||
           p_pipe->p_endpt_descr->curr_dir == USB_DIR_IN ||
           (p_pipe->sys_record.prev_trans_state != USB_DO_NEXT_CMD &&
            p_pipe->sys_record.prev_trans_state != USB_DO_COMPLETE &&
            p_pipe->sys_record.prev_trans_state != USB_DO_OUT)
         ) ? FALSE : TRUE;
}


//%FS===========================================================================
// USB_SAR_Reassemble
//
// Description:
//   This function adds the received data to the IRP and checks the received data PID.
//   1. Check the received data PID.
//   2. Check if the transaction is the last of the IRP received or not.
//   3. Reassemble the received data.
// 
// Parameters: 
//   USB_Data_t* p_rcv_data  I
//     It points to the received data from a completed transfer.
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the pipe. The IRP in this pipe will reveive data.
//
// Return: 
//   void
//%FE===========================================================================
void  USB_SAR_Reassemble(USB_Data_t* p_rcv_data,USB_PipeDescr_t*  p_pipe){

  u_int8            pid;

 //
 // The following statement block have the following effect :
 // [p_rdata->p_data,size] shall be updated from Fig A to Fig B.
 // Fig A.
 //  +-------------------------------------------+
 //  | p_rdata                                  |
 //  +-------------------------------------------+
 //  | | PID |data0|data1| ... |datan|           |
 //  | |p_rdata->p_data                         |
 //  | |<---    p_rdata->size   --->|           |
 //  +-------------------------------------------+
 //  | IRP n                                     |
 //  | p_rdata                                |
 //  +-------------------------------------------+
 //  | p_rdata->p_data = NULL                    |
 //  | p_rdata->size   = 0                       |
 //  +-------------------------------------------+
 // Fig B.
 //  +-------------------------------------------+  
 //  | p_rdata                                  |
 //  +-------------------------------------------+
 //  | | PID |data0|data1| ... |datan|           |
 //  | |p_rdata->p_data                         |
 //  | |<---    p_rdata->size   --->|           |
 //  |       |<--receive data size-->|           | 
 //  +-------------------------------------------+
 //  | IRP n                                     |
 //  +-------------------------------------------+
 //  | |0xSys_p_data0          |0xSys_p_datan    |
 //  | |data0|data1| ... |datan|                 |
 //  | |p_rdata->p_data                          |
 //  | |<--receive data size-->|                 |
 //  +-------------------------------------------+
 //
 
  //---------------------- step 1 ------------------------
  if(p_rcv_data->p_data != NULL){
    pid = p_rcv_data->p_data[0] & 0x0F;

#ifndef USB_WA_IP_NO_TOGGLE
    //Check the received PID
    USB_SAR_CheckPid( pid, p_pipe );
#endif
  }
  
  //---------------------- step 2 ------------------------
  USB_SAR_HandleLastIrpTrans(p_pipe,(p_rcv_data->size - 1) );

  //---------------------- step 3 ------------------------
  USB_SAR_ReassembleData(p_rcv_data, p_pipe);

}




//%FS===========================================================================
// USB_SAR_CheckPid
//
// Description:
//   This function checks the received PID.
//
// Parameters: 
//   u_int8  pid  I
//     This is data pid from device.
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the pipe that received the current transaction. 
//
// Return: 
//   void
//
// Note:
//   If received PID error, it only report.
//%FE===========================================================================
void USB_SAR_CheckPid( u_int8 pid, USB_PipeDescr_t*  p_pipe){
  
  //Check PID
  if ( p_pipe->p_endpt_descr->curr_dir == USB_DIR_IN ){
    if ( pid != p_pipe->p_endpt_descr->pid ){
      MSG_Warn("USB_SAR_CheckPid(): Pipe #%d received data PID error. Received pid is %s should be %s.\n",
	       p_pipe->pipe_id, carbonXUsbCvtPidToString(pid), carbonXUsbCvtPidToString(p_pipe->p_endpt_descr->pid));
    }
  }
}


//%FS===========================================================================
// USB_SAR_ReassembleData
//
// Description:
//   This function reassembles the IRP data.
//   1. Add the transaction is first received data or not. If no, the old data size                    //FIXME
//      is added to the new data size, that all data size is the IRP received data size.
//   2. For the new received data allocate memory, storing the data.
//   3. Copy the new data to IRP.
//
// Parameters: 
//   USB_Data_t*       p_rcv_data   I
//     It points to the receive data.
//   USB_PipeDescr_t*  p_pipe    I
//     It points to the pipe for searching the IRP which will receive data.
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_ReassembleData(USB_Data_t* p_rcv_data,USB_PipeDescr_t* p_pipe){
  u_int32           new_data_size = 0;
  u_int32           old_data_size = 0;
  u_int8*           p_temp = NULL;
  u_int8*           p_add  = NULL;
  USB_Data_t*       p_rdata;
  
  if( (p_pipe != NULL) && (p_rcv_data != NULL) ){
    if( (p_pipe->a_irp_uc.p_head != NULL)&&(p_rcv_data->size > 0) ){
    
      //-------------------- step 1 -------------------
      p_rdata = p_pipe->a_irp_uc.p_head->p_irp->p_rcv_data;
  
      if ( p_rdata != NULL ){
        old_data_size = p_rdata->size;
        new_data_size = p_rdata->size + p_rcv_data->size - 1;
      }
      else new_data_size = p_rcv_data->size - 1;

      //-------------------- step 2 -------------------
      p_rdata = carbonXUsbDataRealloc (p_rdata,new_data_size);
  
      p_pipe->a_irp_uc.p_head->p_irp->p_rcv_data = p_rdata;
  
      //-------------------- step 3 -------------------
      p_temp = p_rcv_data->p_data + 1;
      p_add  = p_rdata->p_data + old_data_size;
      memcpy ( p_add, p_temp, (p_rcv_data->size - 1) );
    }
  }
}




//%FS===========================================================================
// USB_SAR_HandleLastIrpTrans
//
// Description:
//   This function determines, if this is the last transaction of an IRP  or not.
//   1. If the received data size is larger than max packet size, the IRP will be
//      aborted.
//   2. If data size is smaller than max packet size, the transaction is the last one.
//   3. If it is a control transfer and stage is status stage, the transaction is the 
//      last one.
// 
// Parameters: 
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the pipe.
//   u_int32   size   I
//     In case of an outgoing transfer, the number of [payload data] bytes
//        that still need to be sent out for this IRP
//     In case of an incoming  transfer, the number of [payload data] bytes
//        that were received as part of the most recent transaction
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_HandleLastIrpTrans(USB_PipeDescr_t* p_pipe, u_int32 size){
  USB_Irp_t           *p_irp;
  USB_IrpSysRecord_t  *p_irp_sys_rec;
  USB_EndPtDescr_t    *p_ep_descr;
  
  p_irp         = p_pipe->a_irp_uc.p_head->p_irp;
  p_irp_sys_rec = &(p_irp->irp_sys_record);
  p_ep_descr    = p_pipe->p_endpt_descr;

  //------------------- step 1 ----------------------
  if( p_ep_descr->curr_dir == USB_DIR_IN   &&
      size > p_ep_descr->max_packet_size
    ){
    MSG_Error( "IRP is aborted because the bytes in packet received (%d) > max packet size (%d) defined for Endpoint\n",
	       size, p_ep_descr->max_packet_size);
    p_irp->status = USB_IRP_STAT_ABORT;
  }
  
  //------------------- step 2 ---------------------- 
  if( p_ep_descr->endpt_type != USB_EPTYPE_CTRL ){
    
    if( size  <= p_ep_descr->max_packet_size ){
      if( p_ep_descr->curr_dir != USB_DIR_IN ) {
	p_irp_sys_rec->data_part = USB_PART_END;
      }
      else if( size  < p_ep_descr->max_packet_size ) {
	p_irp_sys_rec->data_part = USB_PART_END;  // last packet is smaller than max packet.
      }
      else if( p_irp->p_rcv_data != NULL ) { // first time through is empty
	if( p_irp->rcv_bytes - p_irp->p_rcv_data->size == p_ep_descr->max_packet_size ) {
	  // Last packet is same size as max packet.
	  p_irp_sys_rec->data_part = USB_PART_END;
	}
	if( p_irp->rcv_bytes - p_irp->p_rcv_data->size  <  p_ep_descr->max_packet_size ) {
	  // Last packet may be larger than required.
	  p_irp_sys_rec->data_part = USB_PART_END;
	}
      }
      else if( p_irp->rcv_bytes == p_ep_descr->max_packet_size ) {
	// First packet is also the last packet and of max_packet size
	p_irp_sys_rec->data_part = USB_PART_END;
      }
      else if( p_irp->rcv_bytes < p_ep_descr->max_packet_size ) {
	// First packet is last and larger than required.
	p_irp_sys_rec->data_part = USB_PART_END;
      }
    }
  }
  //------------------- step 3 ----------------------
  else {
    // This must be a CONTROL transfer
    if( p_irp->stage == USB_TRANS_STAGE_STAT ){
      p_irp_sys_rec->data_part = USB_PART_END;
    }
#ifdef junk
    else if ( p_irp->stage == USB_TRANS_STAGE_STAT ) {
      if( p_irp->p_send_data == NULL) { // must be a receive
	if( p_irp->p_rcv_data != NULL) { // first time through is empty
	  if((p_irp->rcv_bytes - p_irp->p_rcv_data->size) == p_ep_descr->max_packet_size )
	    p_irp_sys_rec->data_part = USB_PART_END;  // last packet is same size as max packet.
	  if((p_irp->rcv_bytes - p_irp->p_rcv_data->size) < p_ep_descr->max_packet_size )
	    p_irp_sys_rec->data_part = USB_PART_END;  // last packet may be larger than required.
	}
	// fredieu - This gets messed up with SOFs and IRP is small so do a PANIC at the moment!!
	else if(p_irp->rcv_bytes == p_ep_descr->max_packet_size) {
	  p_irp_sys_rec->data_part = USB_PART_END;  // first packet is last and same size as max packet
	  MSG_Panic("Can't handle a small Control IRP input.  SOF confusion!\n");
	}
	else if(p_irp->rcv_bytes < p_ep_descr->max_packet_size) {
	  p_irp_sys_rec->data_part = USB_PART_END;  // first packet is last and larger than required.
	  MSG_Panic("Can't handle a small Control IRP input.  SOF confusion!\n");
	}
      }
    }
#endif

  }

  return;
}



//%FS===========================================================================
// USB_SAR_Pid
//
// Description:
//   This function sets
//     the (DATA) PID for the next transaction (if direction is OUT)
//     or the expected (DATA) PID of the most recent transaction (if direction is IN)
//
// Parameters: 
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the pipe.
//
//   u_int32           size    I
//     Payload data size (in bytes)
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_Pid ( USB_PipeDescr_t*  p_pipe, u_int32 size ) {
  
  switch ( p_pipe->p_endpt_descr->endpt_type ){
    case USB_EPTYPE_CTRL:{
      USB_SAR_SetCtrlPid(p_pipe, size);
      break;
    }
    case USB_EPTYPE_INTR:
    case USB_EPTYPE_BULK:{
      USB_SAR_SetBulkAndIntrPid(p_pipe);
      break;
    }
    case USB_EPTYPE_ISO:{
      USB_SAR_SetIsoPid(p_pipe);
      break;
    }
  case USB_EPTYPE_UNDEF:{
      // ??
      break;
    }
  } 
}


//%FS===========================================================================
// USB_SAR_SetCtrlPid
//
// Description:
//   This function sets PID related fields of the IRP data struct in case
//   of a Control transfer
//   1. Set stage.
//   2. Set PIDs for the different stages
//    2.1 Setup stage: PID is DATA0
//    2.2 Data stage: PID toggle
//    2.3 Status stage: PID is DATA1
//
// Parameters: 
//   USB_PipeDescr_t*   p_pipe   I
//     It points to the pipe.
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_SetCtrlPid(USB_PipeDescr_t* p_pipe, u_int32 size){
  USB_Irp_t*     p_irp;
  u_int32        received_bytes;

  if(p_pipe != NULL){
    p_irp = p_pipe->a_irp_uc.p_head->p_irp;
 
    //----------- step 1 ------------------
    if(p_irp->setup_sent_cnt == 0){
      p_irp->stage = USB_TRANS_STAGE_SETUP;
    }
    else if( (p_irp->stage == USB_TRANS_STAGE_SETUP) && (p_irp->setup_sent_cnt >= 8) && (p_irp->sent_cnt == 0) ){
      if ( ( p_irp->p_send_data == NULL && p_irp->rcv_bytes == 0 ) ||
	   ( p_irp->p_send_data != NULL && p_irp->p_send_data->size == 0 )
	 ){
	// No need for a Data stage
	p_irp->stage = USB_TRANS_STAGE_STAT;
	p_pipe->p_endpt_descr->needs_toggle = TRUE;

      }
      else if(p_irp->trans_type == USB_IRP_TYPE_CTRL_IN){
        p_irp->stage = USB_TRANS_STAGE_IN;
      }
      else if(p_irp->trans_type == USB_IRP_TYPE_CTRL_OUT){
        p_irp->stage = USB_TRANS_STAGE_OUT;
      }
    }
    else if(p_irp->trans_type == USB_IRP_TYPE_CTRL_OUT){
      if( (p_irp->p_send_data == NULL) || (p_irp->sent_cnt >= p_irp->p_send_data->size) ) {
	
	p_irp->stage = USB_TRANS_STAGE_STAT;
      }
    }
    else if( p_irp->trans_type == USB_IRP_TYPE_CTRL_IN ) {
      received_bytes = size + ( (p_irp->p_rcv_data != NULL ) ? p_irp->p_rcv_data->size : 0 );
      
      if ( (size < p_pipe->p_endpt_descr->max_packet_size) || 
	   ( received_bytes > p_irp->rcv_bytes) ) {

	p_irp->stage = USB_TRANS_STAGE_STAT;
      }
    }
  

    //
    // +-------------------------------------------+
    // | Control Transfer                          |
    // |   --Stage: Setup                          |
    // |     --PID: DATA0                          |
    // |   --Stage: Data                           |
    // |     --PID: DATA1 first, toggle            |
    // |   --Stage: Status                         |
    // |     --PID: DATA1                          |
    // +-------------------------------------------+
    //
    //---------------- step 2 --------------------
    switch (p_irp->stage){
    
      //-------------------- step 2.1 -----------------------
      case USB_TRANS_STAGE_SETUP:
        p_pipe->p_endpt_descr->curr_dir       = USB_DIR_OUT;
        p_pipe->p_endpt_descr->pid            = USB_PID_DATA0;
	p_pipe->p_endpt_descr->needs_toggle   = FALSE;
        break;     
    
      //-------------------- step 2.2 -----------------------
      case USB_TRANS_STAGE_IN:
#ifdef VERBOSE_DEBUG
	//  MSG_LibDebug( PLAT_GetHandleFromTransTable(),
  	//	MSG_TRACE, "<%.10lld> USB_SAR_SetCtrlPid: USB_TRANS_STAGE_IN: \n", PLAT_GetSimTime() );
	MSG_Printf( "<%.10lld> ++++++++ USB_SAR_SetCtrlPid: USB_TRANS_STAGE_IN: \n", PLAT_GetSimTime() );
#endif
	p_pipe->p_endpt_descr->needs_toggle = TRUE;

	if ( p_pipe->p_endpt_descr->curr_dir == USB_DIR_OUT ) {
	  // This must be the first DATA transaction during the Data Stage
	  // It will always start off with DATA1 PID. In order to please the toggle check, we pretend the
	  // the transaction before the IN had a DATA0 PID
	  p_pipe->p_endpt_descr->curr_trans_toggle_pid = USB_PID_DATA0;

	  p_pipe->p_endpt_descr->curr_dir = USB_DIR_IN;
	}
	else {
	  USB_SAR_DataToggle(p_pipe);
	}
	break;

      case USB_TRANS_STAGE_OUT:
#ifdef VERBOSE_DEBUG
	//  MSG_LibDebug( PLAT_GetHandleFromTransTable(),
	//  MSG_TRACE, "<%.10lld> USB_SAR_SetCtrlPid: USB_TRANS_STAGE_OUT: \n", PLAT_GetSimTime() );
	MSG_Printf( "<%.10lld> ++++++++ USB_SAR_SetCtrlPid: USB_TRANS_STAGE_OUT: \n", PLAT_GetSimTime() );
#endif
	p_pipe->p_endpt_descr->needs_toggle = TRUE;
        USB_SAR_DataToggle(p_pipe);     
        break;
      
    
      //-------------------- step 2.3 -----------------------
      case USB_TRANS_STAGE_STAT:
        if ( p_pipe->p_endpt_descr->curr_dir == USB_DIR_IN){
          p_pipe->p_endpt_descr->curr_dir = USB_DIR_OUT;
        }
        else if (p_pipe->p_endpt_descr->curr_dir == USB_DIR_OUT){
          p_pipe->p_endpt_descr->curr_dir = USB_DIR_IN;
	  // The Data stage was of direction OUT, therefore the Status Stage must be of direction IN
          // It will always use a DATA1 PID. In order to please the toggle check, we pretend the
	  // the transaction before the IN had a DATA0 PID
	  p_pipe->p_endpt_descr->curr_trans_toggle_pid = USB_PID_DATA0;
        }
        else {
	  p_pipe->p_endpt_descr->curr_dir = USB_DIR_IN;
	}
      
        if(p_pipe->p_endpt_descr->pid == USB_PID_DATA1){
	  //          p_pipe->p_endpt_descr->needs_toggle = FALSE;
          p_pipe->p_endpt_descr->needs_toggle = TRUE;
        }
        else {
	  p_pipe->p_endpt_descr->pid = USB_PID_DATA1;
	}

#ifdef VERBOSE_DEBUG
	MSG_Printf( "<%.10lld> ++++++++ USB_SAR_SetCtrlPid: USB_TRANS_STAGE_STAT:curr_dir=%s, pid=%s, prev=%s, curr=%s \n",
		    PLAT_GetSimTime(),
		    carbonXUsbCvtDirTypeToString(p_pipe->p_endpt_descr->curr_dir),
		    carbonXUsbCvtPidToString( p_pipe->p_endpt_descr->pid ),
		    carbonXUsbCvtPidToString( p_pipe->p_endpt_descr->prev_trans_toggle_pid ),
		    carbonXUsbCvtPidToString( p_pipe->p_endpt_descr->curr_trans_toggle_pid )
		    );
#endif
      
        break;
    }
  }

  return;
}

//%FS===========================================================================
// USB_SAR_SetBulkAndIntrPid
//
// Description:
//   This function sets the  PID for bulk and interrupt transfer.
//   1. First transaction PID is DATA0
//   2. Following PID toggle.
//
// Parameters: 
//   USB_PipeDescr_t*   p_pipe   I
//     It points to the pipe.
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_SetBulkAndIntrPid(USB_PipeDescr_t*  p_pipe){
  
#ifdef VERBOSE_DEBUG
  MSG_Printf( "<%.10lld> ++++++++ USB_SAR_SetBulkAndIntrPid: Calling :USB_SAR_DataToggle \n", PLAT_GetSimTime() );
#endif
  USB_SAR_DataToggle(p_pipe);
  
  //------------------ step 1 -----------------------
  //  if ( p_pipe->p_endpt_descr->is_first_trans && p_pipe->p_endpt_descr->curr_dir == USB_DIR_OUT ){
  if ( p_pipe->p_endpt_descr->is_first_trans ){
    p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
    p_pipe->p_endpt_descr->is_first_trans = FALSE;
  }
  
  return;
}



//%FS===========================================================================
// USB_SAR_DataToggle
//
// Description:
//   This function toggles the PID DATA0/DATA1.
//
// Parameters: 
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the pipe that will send the next transaction.
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_DataToggle(USB_PipeDescr_t*  p_pipe){
  if (p_pipe->p_endpt_descr->pid == USB_PID_DATA1){
    p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
  }
  else if (p_pipe->p_endpt_descr->pid == USB_PID_DATA0) {
    p_pipe->p_endpt_descr->pid =  USB_PID_DATA1;
  }

#ifdef VERBOSE_DEBUG
  MSG_Printf( "<%.10lld> ++++++++ USB_SAR_DataToggle: Setting p_pipe->p_endpt_descr->pid to %s \n",
	      PLAT_GetSimTime(), carbonXUsbCvtPidToString( p_pipe->p_endpt_descr->pid ) );
#endif
  return;
}



//%FS===========================================================================
// USB_SAR_SetIsoPid
//
// Description:
//   This function checks the current PID for isochronous transfer.
//   1. Device operated in full-speed, the ISO transaction PID is DATA0
//   2. Device operated in high-speed, if OUT direction, the PID sequence refer
//      USB 2.0 spec, page 58, figure 5-12.
//   3. Device operated in high-speed, if IN direction, the PID sequence refer
//      USB 2.0 spec, page 57, figure 5-11.
//
// Parameters: 
//   USB_PipeDescr_t*   p_pipe   I
//     It points to the pipe.
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_SetIsoPid(USB_PipeDescr_t* p_pipe){
  USB_DevProfile_t* p_prof;
  
  p_prof = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
  
  if(p_prof != NULL){
  //
  //+-------------------------------------------+
  //| Isochronous Transfer                      |
  //|   --Speed: Full                           |
  //|       --PID: DATA0                        |
  //|   --Speed: High                           |
  //|       --Direction: DataOUT                |        
  //|         --transaction: 1                  |
  //|           --PID: DATA0                    |
  //|         --transaction: 2                  |
  //|           --PID: MDATA and DATA1          |
  //|         --transaction: 3                  |
  //|           --PID: MDATA, MDATA and DATA2   |
  //|       --Direction: DataIN                 |
  //|         --transaction: 1                  |
  //|           --PID: DATA0                    |
  //|         --transaction: 2                  |
  //|           --PID: DATA1 and DATA0          |
  //|         --transaction: 3                  |
  //|           --PID: DATA2, DATA1 and DATA0   |
  //+-------------------------------------------+
  //
    
    //--------------------- step 1 ------------------------
    if(p_prof->speed == USB_TRANS_FS){
      p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
    }
    else if (p_prof->speed == USB_TRANS_HS){
    
      switch (p_pipe->p_endpt_descr->curr_dir){
      	
        //--------------------- step 2 ------------------------
        case USB_DIR_OUT:{
        
          //1 transactions per microframe use DATA0
          if (p_pipe->p_endpt_descr->hs_trans_num == 0){
            p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
          }
      
          //2 transactions per microframe use MDATA and DATA0
          else if (p_pipe->p_endpt_descr->hs_trans_num == 1){
            if ( p_pipe->sys_record.iso_cnt == 0 ){
              p_pipe->p_endpt_descr->pid = USB_PID_MDATA;
              p_pipe->sys_record.iso_cnt ++;
            }
            else if (p_pipe->sys_record.iso_cnt == 1){
              p_pipe->p_endpt_descr->pid = USB_PID_DATA1;
              p_pipe->sys_record.iso_cnt --;
            }
          }
      
          //3 transactions per microframe use two MDATA and one DATA0
          else if(p_pipe->p_endpt_descr->hs_trans_num == 2){
            if ( p_pipe->sys_record.iso_cnt == 0 ){
              p_pipe->p_endpt_descr->pid = USB_PID_MDATA;
              p_pipe->sys_record.iso_cnt ++;
            }
            else if (p_pipe->sys_record.iso_cnt == 1) {
              p_pipe->p_endpt_descr->pid = USB_PID_MDATA;
              p_pipe->sys_record.iso_cnt ++;
            }
            else if (p_pipe->sys_record.iso_cnt == 2){
              p_pipe->p_endpt_descr->pid = USB_PID_DATA2;
              p_pipe->sys_record.iso_cnt -= 2;
            }
          }
          break;
        } //end of case USB_DIR_OUT
        
        //--------------------- step 3 ------------------------
        case USB_DIR_IN:{
        
          //1 transaction per microframe use DATA0
          if (p_pipe->p_endpt_descr->hs_trans_num == 0){
            p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
          }
      
          //2 transactions per microframe use MDATA and DATA0
          else if(p_pipe->p_endpt_descr->hs_trans_num == 1){
            if ( p_pipe->sys_record.iso_cnt == 0 ){
              p_pipe->p_endpt_descr->pid = USB_PID_DATA1;
              p_pipe->sys_record.iso_cnt ++;
            }
            else if (p_pipe->sys_record.iso_cnt == 1){
              p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
              p_pipe->sys_record.iso_cnt --;
            }
          }
          
          //3 transactions per microframe use two MDATA and one DATA0
          else if(p_pipe->p_endpt_descr->hs_trans_num == 2){
            if ( p_pipe->sys_record.iso_cnt == 0 ){
              p_pipe->p_endpt_descr->pid = USB_PID_DATA2;
              p_pipe->sys_record.iso_cnt ++;
            }
           else if (p_pipe->sys_record.iso_cnt == 1) {
              p_pipe->p_endpt_descr->pid = USB_PID_DATA1;
              p_pipe->sys_record.iso_cnt ++;
            }
            else if (p_pipe->sys_record.iso_cnt == 2){
              p_pipe->p_endpt_descr->pid = USB_PID_DATA0;
              p_pipe->sys_record.iso_cnt -= 2;
            }
          }
          break;
        } //end of USB_DIR_IN
      default : {
	MSG_Panic("USB_SAR_SetIsoPid -- Should not be here!\n");
      }
        
      } //end of switch
    } //end of else if.
  }
}




//%FS===========================================================================
// USB_SAR_HsIsoOutSplit
//
// Description:
//   This function prepares the next PID and Data Part for high-speed 
//   OUT isochronous start-split transfer.
//   1. Prepare  PID for next transaction. 
//   2. Prepare the Data Part for start-split packet
//
// Parameters: 
//   USB_PipeDescr_t*   p_pipe   I
//     It points to the pipe.
//   u_int16  segment_size        I
//     It points to the next transactions data size.
//
// Return: 
//   void
//%FE===========================================================================

void USB_SAR_HsIsoOutSplit(USB_PipeDescr_t*  p_pipe, u_int16 segment_size){
 USB_Irp_t*        p_irp;
  
  p_irp = p_pipe->a_irp_uc.p_head->p_irp;
  
  //-------------------- step 1 ------------------
  if((p_irp->irp_sys_record.data_part == USB_PART_ALL)
    ||(p_irp->irp_sys_record.data_part == USB_PART_END) ){
    USB_SAR_Pid(p_pipe, segment_size);
  }
  
  //-------------------- step 2 ------------------
  USB_SAR_DataPart(p_irp, segment_size);
}



//%FS===========================================================================
// USB_SAR_DataPart
//
// Description:
//   This function prepares the next Data Part high-speed OUT isochronous 
//   start-split transfer.
//   1. When the segment size is larger than the data size being sent,
//      if it is first transaction, the data part is ALL; otherwise, it is END
//   2. When the segment size is smaller than the data size being sent,
//      if it is first transaction, the data part is BEGIN; otherwise, it is MID
//
// Parameters: 
//   USB_Irp_t*    p_irp         IO
//     It points to the IRP data struct
//   u_int16  segment_size        I
//     Segment size of next transaction
//
// Return: 
//   void
//%FE===========================================================================
void USB_SAR_DataPart(USB_Irp_t* p_irp, u_int16 segment_size){
  
  if( (p_irp->p_send_data->size - p_irp->sent_cnt) <= segment_size ){
   
    //-------------------- step 1 ------------------------
    if( p_irp->sent_cnt == 0 ){
      p_irp->irp_sys_record.data_part = USB_PART_ALL;
    }
    else p_irp->irp_sys_record.data_part = USB_PART_END;
  
  }
  else{
  
    //-------------------- step 2 ------------------------
    if( p_irp->sent_cnt == 0 ){
      p_irp->irp_sys_record.data_part = USB_PART_BEGIN;
    }
    else p_irp->irp_sys_record.data_part = USB_PART_MID;
  
  }
}


//%FS===========================================================================
// USB_SAR_IsSmall
//
// Description:
//   This function compares the current data size which has not been sent with 
//   the maximum packet size.
//
// Parameters: 
//   USB_Irp_t*   p_irp   I
//     It points to the IRP.
//   USB_Data+_t*  p_sdata   I
//     It points to the data being sent.
//   USB_PipeDescr_t*  p_pipe   I
//     It points to max_packet_size in the pipe.
//
// Return: 
//   If TRUE, the spare data size is smaller than mazimum packet size.
//   If FALSE, the spare data size is equal to the max size or larger than
//   the max size.
//%FE===========================================================================
BOOL USB_SAR_IsSmall(
  USB_Irp_t*        p_irp,
  USB_Data_t*       p_sdata,
  USB_PipeDescr_t*  p_pipe
){
  return ( (p_sdata->size - p_irp->sent_cnt) < 
           (p_pipe->p_endpt_descr->max_packet_size) );
}
