//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file includes all functions relating to IRP operation,
 *                 It can be classified into 3 groups:
 *                 [USB_*, USB_DEV_*,USB_*]
 *
 *                 USB_* are API functions that user can use for the Standard 
 *                 Requests related operations, they include:
 *
 *                 carbonXUsbDoBusEnum
 *                 carbonXUsbDeviceAttach
 *                 carbonXUsbDeviceRemove
 *                 carbonXUsbReset
 *                 carbonXUsbGetDescr
 *                 carbonXUsbClearFeature
 *                 carbonXUsbGetCfg
 *                 carbonXUsbGetIntf
 *                 carbonXUsbGetStatus
 *                 carbonXUsbSetAddr
 *                 carbonXUsbSetCfg
 *                 carbonXUsbSetDescr
 *                 carbonXUsbSetFeature
 *                 carbonXUsbSetIntf
 *                 carbonXUsbSynchFrame
 *                 carbonXUsbClearTTBuffer
 *
 *                 USB_STD_* are functions that SVC uses for Standard Requests,
 *                 they include:
 *                 USB_STD_SetDevProfFromStdDevDescr
 *                 USB_STD_SetDevProfFromStdCfgDescr
 *                 USB_STD_SetDevFromStdDevDescr(
 *                 USB_STD_SetStdDescr
 *                 USB_STD_SetStdDevDescr
 *                 USB_STD_SetStdCfgDescr
 *                 USB_STD_SetStdIfDescr
 *                 USB_STD_SetStdEpDescr
 *                 USB_STD_SetStdQualDescr
 *                 USB_STD_SetStdOthSpeedDescr
 *                 USB_STD_GetStdDevDescr
 *                 USB_STD_GetStdCfgDescr
 *                 USB_STD_GetStdIfDescr
 *                 USB_STD_GetStdEpDescr
 *                 USB_STD_GetStdQualDescr
 *                 USB_STD_GetStdOthSpeedDescr
 *                 USB_STD_SetStdHubDescr
 *                 USB_STD_GetStdHubDescr
 *                 USB_STD_ResetDescr
 *
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : 
 *               :
 ***************************************************************************/
 
 
#include "USB_support.h"


//%FS===========================================================================
// carbonXUsbDoBusEnum
//
// Description:
//   This function does bus enumeration when the descriptor is standard.
//   1. Reset all DUT.
//   2. Record all devices. Each device uses 1-bit mark. 
//      1: device exists.
//      0: no device.
//   3. Descriptor operation.
//    3.1 Set Address 
//    3.2 Get all descriptor
//    3.3 Set device descriptor and configuration value.
//
// Parameter:
//   void
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbDoBusEnum() {
  USB_DevProfile_t* p_dev;
  USB_DevProfile_t* p_def_dev;
  USB_PipeDescr_t*  p_pipe;
  USB_PipeDescr_t*  p_def_pipe;
  u_int32           i;
  u_int32           addr=0;
  u_int32           dev_id;

  u_int32           mark_one   = 0;
  u_int32           mark_two   = 0;
  u_int32           mark_three = 0;
  u_int32           mark_four  = 0;
  u_int32           one_dev = 0;
  
  //-------------- step 1 -----------------
  carbonXUsbReset(USB_TRANS_FS);
  
  p_def_pipe = carbonXUsbGetPipeDescr(0);
  p_def_dev = carbonXUsbGetDevProf(p_def_pipe->p_endpt_descr->dev_id);
  
  //-------------- step 2 -----------------
  for(i=1;i<USB_MAX_DEV_NUMBER;i++){
    p_dev = carbonXUsbGetDevProf(i);
    if(p_dev->dev_id == 0) continue;
    
    one_dev = 1;
    if(i < 33){
      one_dev <<= (i - 1);
      mark_one += one_dev;
    }
    else if( (i < 65)&&(i > 32) ){
      one_dev <<= (i - 33);
      mark_two += one_dev;
    }
    else if( (i < 97)&&(i > 64) ){
      one_dev   <<= (i - 65);
      mark_three += one_dev;
    }
    else if( (i < 128)&&(i > 96) ){
      one_dev <<= (i - 97);
      mark_four += one_dev;
    }
  }
  
  //-------------- step 3 -----------------
  i = USB_MAX_DEV_NUMBER;
  
  while( (mark_one != 0)||(mark_two != 0)||(mark_three != 0)||(mark_four != 0) ){

    //-------------- step 3.1 -----------------
    carbonXUsbSetAddr(p_def_pipe, addr);
    
    //-------------- step 3.2 -----------------
    USB_STD_SetDevProfFromStdDevDescr( p_def_pipe );
    
    dev_id = USB_CHK_DevIsSame(p_def_dev);
    
    if(dev_id != 0){
      if(dev_id < 33){
        one_dev <<= (dev_id - 1);
        mark_one -= one_dev;
      }
      else if( (dev_id < 65)&&(dev_id > 32) ){
        one_dev <<= (dev_id - 33);
        mark_two -= one_dev;
      }
      else if( (dev_id < 97)&&(dev_id > 64) ){
        one_dev   <<= (dev_id - 65);
        mark_three -= one_dev;
      }
      else if( (dev_id < 128)&&(dev_id > 96) ){
        one_dev <<= (dev_id - 97);
        mark_four -= one_dev;
      }
      p_dev = carbonXUsbGetDevProf(dev_id);

      p_pipe = carbonXUsbGetDefPipe(dev_id);
      
      if(p_dev->dev_type != USB_DEV_TYPE_HUB){
      	p_dev->dev_addr = addr;
      	USB_STD_SetDevProfFromStdCfgDescr(p_pipe);
      }
      
      //-------------- step 3.3 -----------------
      USB_STD_SetDevFromStdDevDescr(p_pipe);
    }
    
    addr ++;
    
    //Safe for loop
    i --;
    if(i <= 0) break;
  }
  
}


//%FS===========================================================================
// carbonXUsbDeviceAttach
// 
// Description:
//   Write a 'attach' command to inform BFM that a device is attached.
//
// Parameters:
//   void
//
/// Return:
//   void
//
//%FE===========================================================================
void carbonXUsbDeviceAttach() {
  USB_HostCtrlTop_t*   p_top;
  u_int32    dev_speed;
	u_int32    data;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  data = carbonXCsRead(USB_BFM_ADDR_CFG_READ);
  // Write BFM a 'attach' command
  carbonXCsWrite(USB_BFM_ADDR_CFG_REGISTER, USB_CTRL_DEV_ATTACH | data);
  MSG_Milestone("<time: %.10lld>: Set attach signal 0x%x\n",
                PLAT_GetSimTime(), USB_CTRL_DEV_ATTACH);

  // Callback function written by user
  if ( p_top->pfDeviceAttach ) {
    // callback function exists.
    // -> Call it.
    // This Call Back is typically used to read a USB IP core status register
    // that indicates that the USB device is now attached and settled.
		MSG_Milestone("Call back function being called in carbonXUsbDeviceAttach\n");
    p_top->pfDeviceAttach( p_top );
  }    
  MSG_Milestone("<time: %.10lld>: Device should now be attached and settled.\n",
                PLAT_GetSimTime());

  dev_speed = carbonXCsRead(USB_BFM_ADDR_STATUS_REGISTER);
  dev_speed &= 0x3;
  MSG_Milestone("<time: %.10lld>: Device speed (1=LS, 2=FS, 3=HS): %x.\n",
                PLAT_GetSimTime(), dev_speed);
}

//%FS===========================================================================
// carbonXUsbDeviceRemove
// 
// Description:
//   Send a 'remove' command to the BFM that a device is removed.
//
// Parameters:
//   void
//
// Return:
//   void
//
//%FE===========================================================================
void carbonXUsbDeviceRemove() {
	u_int32 read_data;

	read_data = carbonXCsRead(USB_BFM_ADDR_CFG_READ);
  carbonXCsWrite(USB_BFM_ADDR_CFG_REGISTER, USB_CTRL_DEV_REMOVE | read_data);
  					// Write BFM a 'remove' command
  MSG_Milestone("<time: %.10lld>: Set remove signal 0x%x\n",
                PLAT_GetSimTime(), USB_CTRL_DEV_REMOVE);
}


//%FS===========================================================================
// carbonXUsbReset
// 
// Description:
//   Send a 'reset' command to the BFM to reset the device
//   and then assign the device a unique address.
//   1. Reset all DUT
//   2. Read the bus speed, and set the speed to device profile.
//
// Parameters:
//   void
//
// Return:
//   USB_TransSpeed_e   work_speed   I
//      It points to work data.
//
// Note:
//   When resetting, the program will be set all device operated speed to bus
//   speed, except low-speed the device #0 will be operated in full-speed.
//%FE===========================================================================
void carbonXUsbReset(USB_TransSpeed_e work_speed) {
  USB_HostCtrlTop_t* p_top;
  u_int32    dev_speed;
  u_int32    i;
  u_int32    speed;
	u_int32    data;

  USB_DevProfile_t*  p_dev;
  
  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();


  //-------------------- step 1 -------------------------
  carbonXIdle(4);

  // Set speed and device reset bits in configuration register
  // .. read
	//  speed = carbonXCsRead(USB_BFM_ADDR_CFG_REGISTER); 
  speed = carbonXCsRead(USB_BFM_ADDR_CFG_READ); 
  // .. modify
  speed &= ~(USB_STATUS_DEV_SPEED_MASK << USB_STATUS_DEV_SPEED_BITS);
  speed = speed | (u_int32)work_speed;
	if(work_speed == USB_TRANS_LS) speed = speed |  USB_CTRL_LS_ATTACH;
  // .. write
	//  carbonXCsWrite(USB_BFM_ADDR_CFG_REGISTER, speed | USB_CTRL_DEV_RESET);
  carbonXCsWrite(USB_BFM_ADDR_CFG_REGISTER, speed);
	carbonXIdle(2);

  MSG_Milestone("<time: %.10lld>: Writing speed (%x) to BFM configuration register \n%s\n",
  		  PLAT_GetSimTime(), work_speed, "  and triggering USB Reset with HS Detection Handshake"
               );

  // Callback function written by user
  if ( p_top->pfDevSpeedNeg ) {
    // callback function exists.
    // -> Call it.
    // This Call Back function is typically used to determine the end of the
    // High Speed Negotiation Handshake
    MSG_Milestone("Speed Negotiation call back function being called in carbonXUsbReset\n");
    p_top->pfDevSpeedNeg( p_top );
  }
  else {
    carbonXIdle( USB_RESET_SPEED_NEG_WAIT_TIME ); 
  }

  MSG_Milestone("<time: %.10lld>: Reset with speed negotiation cycle is completed. Ready for traffic.\n",
                PLAT_GetSimTime());

  data = carbonXCsRead(USB_BFM_ADDR_STATUS_REGISTER);
  dev_speed = data & (USB_STATUS_DEV_SPEED_MASK << USB_STATUS_DEV_SPEED_BITS) ;
  MSG_Milestone("<time: %.10lld>: Device speed (1=LS, 2=FS, 3=HS): %x.\n",
                PLAT_GetSimTime(), dev_speed);
  if(dev_speed != work_speed)	MSG_Error("Speed was not set properly!\n");

  // the following clears USB_CTRL_DEV_RESET
  carbonXCsWrite(USB_BFM_ADDR_CFG_REGISTER, speed);
  carbonXIdle(5);
 
  //-------------------- step 2 -------------------------
  for(i=0;i<USB_MAX_DEV_NUMBER;i++){
    p_dev = carbonXUsbGetDevProf(i);
    if(p_dev == NULL) break;
    if(p_dev->dev_id == 0) continue;

    p_dev->state = USB_DEV_DEF;
      
    if(dev_speed == 2){
      p_dev->speed = USB_TRANS_FS;
    }
    else if(dev_speed == 3){
      p_dev->speed = USB_TRANS_HS;
    }
    else if(dev_speed == 1){
      if(i == 0)p_dev->speed = USB_TRANS_FS;
      p_dev->speed = USB_TRANS_LS;
    }
  }
}


//%FS===========================================================================
// USB_STD_SetDevProfFromStdDevDescr
//
// Description:
//   This function gets the standard device descriptor and pushes it to 
//   the device profile.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the specific pipe being added request.
//
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetDevProfFromStdDevDescr( USB_PipeDescr_t*  p_pipe ){
  USB_DevProfile_t* p_dev;
  u_int8            req_type;
  u_int8            descr_type;
  u_int8            descr_index;
  u_int16           language_id;
  u_int16           descr_length;
  u_int32           flag = 0;
  USB_Irp_t*        p_irp;

 
  req_type    = 0x80;
  descr_type  = 1;
  descr_index = 0;
  language_id  = 0;
  descr_length = 18;
  
  p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
  
  if( (p_pipe != NULL)&&(p_dev != NULL) ){
    carbonXUsbGetDescr(p_pipe, req_type, descr_type, descr_index, language_id, descr_length);
    
    carbonXUsbExecIRPs(0);
    
    if(p_pipe->a_irp_c.p_tail != NULL) p_irp = p_pipe->a_irp_c.p_tail->p_irp;
    else p_irp = NULL;
    
    if(p_irp != NULL){

      if( (p_irp->p_rcv_data != NULL)&&
         ( (carbonXUsbIrpGetStat(p_irp) ) == USB_IRP_STAT_COMPL ) ){

        if( (*(p_irp->p_rcv_data->p_data + 1)) == 29 ){
          p_dev->dev_type = USB_DEV_TYPE_HUB;
          descr_type   = 0x29;
          descr_length = *(p_irp->p_rcv_data->p_data);
          flag = 2;
        }
        else flag = 1;

      }
    }
  }
  
  if (flag == 1){
    USB_STD_SetStdDevDescr(&p_dev->dut_model.stddev, p_irp->p_rcv_data->p_data);
  }
  else {

    carbonXUsbGetDescr(p_pipe, req_type, descr_type, descr_index, language_id, descr_length);
  
    carbonXUsbExecIRPs(0);

    p_irp = p_pipe->a_irp_c.p_tail->p_irp;
    if( (carbonXUsbIrpGetStat(p_irp)) == USB_IRP_STAT_COMPL){
      USB_STD_SetStdHubDescr(&p_dev->dut_model.stdhub, p_irp->p_rcv_data->p_data);
    }        
  }     
}


//%FS===========================================================================
// USB_STD_SetDevProfFromStdCfgDescr
//
// Description:
//   This function gets the standard configuration descriptor and uses it to 
//   set the device profile.
//   1. Get standard descriptor for configuration. Set the wLength field to 9.
//   2. Reset the wLength field to the wTotalLength of the returned descriptor.
//      Get the descriptor again.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe   I
//     It points to the specific pipe being added.
//
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetDevProfFromStdCfgDescr( USB_PipeDescr_t*  p_pipe ){
  u_int8            req_type; 
  u_int8            descr_type;
  u_int8            descr_index;
  u_int16           language_id;
  u_int16           descr_length;
  
  u_int32           i;
  BOOL              is_found = FALSE;
  USB_Irp_t*        p_irp;
  USB_DevProfile_t* p_dev;
  
  //---------------- step 1 -------------------
  req_type     = 0x80;
  descr_type   = 2;
  descr_index  = 0;
  language_id  = 0;
  descr_length = 9;
  
  if(p_pipe != NULL){
    
    p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
    if( (p_dev == NULL)&&(p_dev->dev_id != p_pipe->p_endpt_descr->dev_id) ) is_found = FALSE;
    
    for(i=0;i<p_dev->user_cfg.stddev.i_num_cfg;i++){
      descr_index  = i;
      descr_length = 9;

      carbonXUsbGetDescr(p_pipe, req_type, descr_type, descr_index, language_id, descr_length);
          
      carbonXUsbExecIRPs(0);
    
      if(p_pipe->a_irp_c.p_tail != NULL) p_irp = p_pipe->a_irp_c.p_tail->p_irp;
      else p_irp = NULL;
      
      if(p_irp != NULL){
        if(p_irp->status == USB_IRP_STAT_COMPL) is_found = TRUE;
      }
      
      //---------------- step 2 -------------------
      if(is_found){
      
        descr_length = (*(p_irp->p_rcv_data->p_data + 2) << 8) + *(p_irp->p_rcv_data->p_data + 3);
  
        carbonXUsbGetDescr(p_pipe, req_type, descr_type, descr_index, language_id, descr_length);
  
        carbonXUsbExecIRPs(0);
  
        p_irp = p_pipe->a_irp_c.p_tail->p_irp;
    
        if( (p_irp != NULL)&&(p_irp->status == USB_IRP_STAT_COMPL) ){
          USB_STD_SetStdDescr(p_dev,p_irp->p_rcv_data,descr_index);
        }
      }
      
      if(i >= USB_MAX_CFG_NUMBER) break;
    }
  }
  
}



//%FS===========================================================================
// USB_STD_SetStdDesc
//
// Description:
//   This function pushes the standard descriptor from the specific device profile.
//
// Parameter:
//   USB_DevProfile_t* p_dev   I
//     It points to the specific device profile.
//   USB_Data_t* p_data_t   I
//     It points to the descriptor data and length.
//
// Return:
//   void
//
// Note:
//   This function pushes the configuration, interface and endpoint descriptor.
//   The single interface has more endpoints. 
//   The endpoints descriptors for the first interface descriptor follows 
//   the first interface descriptor. 
//   Additional interfaces descriptor and endpoint descriptors follows the first's
//   endpoint descriptors.
//%FE===========================================================================
void USB_STD_SetStdDescr(USB_DevProfile_t* p_dev,USB_Data_t* p_data_t,u_int8 descr_index){
  u_int8*  p_data;
  u_int8*  p_temp;
  u_int32  size;
  u_int32  if_num = 0;
  u_int32  ep_num = 0;
  u_int32  i = 0;
  
  size   = p_data_t->size;
  p_data = p_data_t->p_data;
  
  for(i=0;i<USB_MAX_IF_NUMBER;i++){
    if(p_dev->dut_model.a_stdif[i].b_length == 0) break;
    if_num ++;
  }
  for(i=0;i<USB_MAX_EP_DESCR_NUMBER;i++){
    if(p_dev->dut_model.a_stdep[i].b_length == 0) break;
    ep_num ++;
  }
  
  while(size > 0){
    p_temp = p_data + 1;
    switch(*p_temp){
      case 2:{
        USB_STD_SetStdCfgDescr(&p_dev->dut_model.stdcfg[descr_index], p_data);
        size   -= 9;
        p_data += 9;
        break;
      }
      case 4:{
        USB_STD_SetStdIfDescr(&p_dev->dut_model.a_stdif[if_num], p_data);
        size   -= 9;
        p_data += 9;
        if_num ++;
        break;
      }
      case 5:{
        USB_STD_SetStdEpDescr(&p_dev->dut_model.a_stdep[ep_num], p_data);
        size   -= 7;
        p_data += 7;
        ep_num ++;
        break;
      }
      default: break;
    }
    //Safe for while
    if( (if_num == USB_MAX_IF_NUMBER)||(ep_num == USB_MAX_EP_DESCR_NUMBER) ) break;
  }
    
}


//%FS===========================================================================
// USB_STD_SetDevFromStdDevDescr
//
// Description:
//   This function sets the standard device descriptor of the target device
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe that the request will be added.
//
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetDevFromStdDevDescr(USB_PipeDescr_t* p_pipe){
  u_int8            req_type;
  u_int8            descr_type; 
  u_int8            descr_index;
  u_int16           language_id;
  u_int16           descr_length;
  USB_Data_t*       p_descr;
  USB_DevProfile_t* p_dev;

  req_type     = 0;
  descr_index  = 0;
  language_id  = 0;
  
  p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
  
  if ( (p_dev != NULL)&&(p_dev->dev_id == p_pipe->p_endpt_descr->dev_id) ){
    p_descr = carbonXUsbDataRealloc(NULL,18);

    if( (p_dev->dev_type != USB_DEV_TYPE_HUB)
      && !(USB_CHK_DevDescIsSame(&p_dev->user_cfg, &p_dev->dut_model)) ){
      
      descr_type   = 1;
      descr_length = p_dev->user_cfg.stddev.b_length;
      
      USB_STD_GetStdDevDescr(&p_dev->user_cfg.stddev, p_descr->p_data);
      carbonXUsbSetDescr(p_pipe, req_type, descr_type, descr_index, language_id, descr_length, p_descr);
      
      carbonXUsbSetCfg(p_pipe, p_dev->user_cfg.stdcfg[0].b_cfg_val);
      
      carbonXUsbExecIRPs(0);
      
      p_dev->dut_model = p_dev->user_cfg;
      p_dev->state     = USB_DEV_CFG;
    }
    else if( !(USB_CHK_HubDescIsSame(&p_dev->user_cfg, &p_dev->dut_model)) ){
    	
      req_type     = 0x20;
      descr_type   = 0x29;
      descr_length = p_dev->user_cfg.stdhub.b_descr_len;
      
      USB_STD_GetStdHubDescr(&p_dev->user_cfg.stdhub, p_descr->p_data);
      carbonXUsbSetDescr(p_pipe, req_type, descr_type, descr_index, language_id, descr_length, p_descr);
      
      carbonXUsbExecIRPs(0);
      
      p_dev->dut_model = p_dev->user_cfg;
      p_dev->state     = USB_DEV_CFG;
      
    }
    
  }
  else MSG_Error("USB_STD_SetDevFromStdDevDescr(): The device does not exist.\n");
}



//%FS===========================================================================
// carbonXUsbGetDesc
//
// Description:
//   This function is the standard request GET_DESCRIPTOR. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int8   request_type    I
//     It points to the characteristics of request, including direction, type 
//     and ther recipient. Reference USB 2.0 spec page 248, Table 9-2.
//   u_int8   descr_type   I
//     It points to the descriptor type. Refer to USB 2.0 spec page 251, 
//     Table 9-5. If HUB value is 29H.
//   u_int8   descr_index   I
//     It points to descriptor index used to select a specific descriptor
//     (only for configureation and string descriptors)
//   u_int16  language_id   I
//     It points to Language ID for string descriptors
//   u_int16  descr_length   I
//     It points to descriptor length being returned.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbGetDescr(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int8           descr_type, 
  u_int8           descr_index,
  u_int16          language_id,
  u_int16          descr_length 
){
  USB_Data_t*      p_send_data;
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = request_type;
    a_request[1] = 0x06;
    a_request[2] = descr_type;
    a_request[3] = descr_index;
    a_request[4] = language_id >> 8;
    a_request[5] = language_id;
    a_request[6] = descr_length >> 8;
    a_request[7] = descr_length;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_send_data, USB_IRP_TYPE_CTRL_IN, -1);
    
    carbonXUsbPipePushInIRP(p_pipe, p_irp);
  }
  else MSG_Error("USB_STD_GetDescr(): The pipe does not exist or is not a Control Pipe\n");
}


//%FS===========================================================================
// carbonXUsbClearFeature
//
// Description:
//   This function is the standard request CLEAR_FEATURE. Calling it 
//   will add a standard request IRP to the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int8     request_type   I
//     Type of request, including direction, type and ther recipient.
//     Refer to USB 2.0 spec page 248, Table 9-2.
//   u_int16    feature_selector    I
//     Feature selector value
//     Refer to USB 2.0 spec page 252, 
//     Table 9-6.
//   u_int16    recipient   I
//     Recipient device, interface or endpoints, ect.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbClearFeature(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int16          feature_selector, 
  u_int16          recipient
){
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = request_type;
    a_request[1] = 0x01;
    a_request[2] = feature_selector >> 8;
    a_request[3] = feature_selector;
    a_request[4] = recipient >> 8;
    a_request[5] = recipient;
    a_request[6] = 0;
    a_request[7] = 0;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    
    memcpy (p_setup_data->p_data, a_request, 8);
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, NULL, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe, p_irp);
  }
  else MSG_Error("carbonXUsbClearFeature(): The pipe does not exist or is not a Control Pipe\n");
}


//%FS===========================================================================
// carbonXUsbGetCfg
//
// Description:
//   This function is the standard request GET_CONFIGURATION. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbGetCfg(USB_PipeDescr_t* p_pipe){
  USB_Data_t*      p_send_data;
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = 0x80;
    a_request[1] = 0x08;
    a_request[2] = 0;
    a_request[3] = 0;
    a_request[4] = 0;
    a_request[5] = 0;
    a_request[6] = 0;
    a_request[7] = 1;
   
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
  
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_send_data, USB_IRP_TYPE_CTRL_IN,-1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("carbonXUsbGetCfg(): The pipe does not exist or is not a Control Pipe\n");
}

//%FS===========================================================================
// carbonXUsbGetIntf
//
// Description:
//   This function is the standard request GET_INTERFACE. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int16   interface   I
//     It point to the interface number.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbGetIntf(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          interface  
){
  USB_Data_t*      p_send_data;
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = 0x81;
    a_request[1] = 0xa;
    a_request[2] = 0;
    a_request[3] = 0;
    a_request[4] = interface >> 8;
    a_request[5] = interface;
    a_request[6] = 0;
    a_request[7] = 1;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
    
    p_irp = carbonXUsbIrpCreate(p_setup_data,p_send_data,USB_IRP_TYPE_CTRL_IN,-1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("USB_STD_GetIntf(): The pipe does not exist or is not a Control Pipe\n");
}




//%FS===========================================================================
// carbonXUsbGetStatus
//
// Description:
//   This function is the standard request GET_STATUS. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int8   request_type   I
//     It points to the characteristics of request, including direction, type 
//     and ther recipient. Refer to USB 2.0 spec page 248, Table 9-2.
//   u_int16   recipient    I
//     It points to the recipient device, interface or endpoints, ect.
//   u_int16   w_length   I
//     It points to the number of bytes to transfer if there is Data stage.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbGetStatus(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int16          recipient,
  u_int16          w_length
){
  USB_Data_t*      p_send_data;
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = request_type;
    a_request[1] = 0x0;
    a_request[2] = 0;
    a_request[3] = 0;
    a_request[4] = recipient >> 8;
    a_request[5] = recipient;
    a_request[6] = w_length >> 8;
    a_request[7] = w_length;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_send_data, USB_IRP_TYPE_CTRL_IN, -1);
    
    carbonXUsbPipePushInIRP(p_pipe, p_irp);
  }
  else MSG_Error("USB_STD_GetStatus(): The pipe does not exist or is not a Control Pipe\n");
}


//%FS===========================================================================
// carbonXUsbSetAddr
//
// Description:
//   This function is the standard request SET_ADDRESS. Call to the function 
//   It will add a Standard Request IRP to the specified pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int16   dev_addr    I
//     It points to the device address.
//
// Return:
//   void
//
// Note:
//   This function will be change the device address in the device profile.
//%FE===========================================================================
void carbonXUsbSetAddr(USB_PipeDescr_t* p_pipe, u_int16 dev_addr){
  USB_DevProfile_t* p_dev;
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
   
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    p_setup_data = carbonXUsbDataRealloc( NULL, 8 );

    p_setup_data->p_data[0] = 0x0;
    p_setup_data->p_data[1] = 0x5;  // SET_ADDRESS
    p_setup_data->p_data[2] = dev_addr & 0xFF;           // Low Byte
    p_setup_data->p_data[3] = (dev_addr & 0xFF00 ) >> 8; // High Byte
    p_setup_data->p_data[4] = 0;
    p_setup_data->p_data[5] = 0;
    p_setup_data->p_data[6] = 0;
    p_setup_data->p_data[7] = 0;
        
    p_irp = carbonXUsbIrpCreate(p_setup_data, NULL, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe, p_irp);
    
    // Set the device address
    p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);
    p_dev->dev_addr = dev_addr;
  }
  else {
    MSG_Error("carbonXUsbSetAddr(): The pipe does not exist or is not a Control Pipe\n");
  }
}





//%FS===========================================================================
// carbonXUsbSetCfg
//
// Description:
//   This function is the standard request SET_CONFIGRATION.
//   It will add a Standard Request IRP to the specified pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int16   cfg_value    I
//     It points to the configured values.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbSetCfg( USB_PipeDescr_t* p_pipe, u_int16 cfg_value){
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
   
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    p_setup_data = carbonXUsbDataRealloc( NULL, 8 );

    p_setup_data->p_data[0] = 0x0;
    p_setup_data->p_data[1] = 0x9;  // SET_CONFIGURATION
    p_setup_data->p_data[2] = cfg_value & 0xFF;           // Low Byte
    p_setup_data->p_data[3] = (cfg_value & 0xFF00 ) >> 8; // High Byte
    p_setup_data->p_data[4] = 0;
    p_setup_data->p_data[5] = 0;
    p_setup_data->p_data[6] = 0;
    p_setup_data->p_data[7] = 0;
        
    p_irp = carbonXUsbIrpCreate(p_setup_data, NULL, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe, p_irp);
    
  }
  else {
    MSG_Error("carbonXUsbSetCfg(): The pipe does not exist or is not a Control Pipe\n");
  }
}




//%FS===========================================================================
// USB_SetDesc
//
// Description:
//   This function is the standard request SET_DESCRIPTOR. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int8     request_type   I
//     It points to the characteristics of request, including direction, type 
//     and ther recipient. Refer to USB 2.0 spec page 248, Table 9-2.
//   u_int8   descr_type   I
//     It points to the descriptor type. Refer to USB 2.0 spec page 251, 
//     Table 9-5.  If HUB value is 29H.
//   u_int8   descr_index   I
//     It points to descriptor index used to select a specific descriptor
//     (only for configureation and string descriptors)
//   u_int16  language_id   I
//     It points to Language ID for string descriptors
//   u_int16  descr_length   I
//     It points to descriptor length being returned.
//   USB_Data_t*   p_descr   I
//     It points to the descriptor data and data length.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbSetDescr(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int8           descr_type, 
  u_int8           descr_index,
  u_int16          language_id,
  u_int16          descr_length,
  USB_Data_t*      p_descr		// Descriptor
){
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = request_type;
    a_request[1] = 0x07;
    a_request[2] = descr_type;
    a_request[3] = descr_index;
    a_request[4] = language_id >> 8;
    a_request[5] = language_id;
    a_request[6] = descr_length >>8;
    a_request[7] = descr_length;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    memcpy (p_setup_data->p_data, a_request, 8);
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_descr, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("carbonXUsbSetDescr(): The pipe does not exist or is not a Control Pipe\n");
}


//%FS===========================================================================
// carbonXUsbSetFeature
//
// Description:
//   This function is the standard request SET_FEATURE. Call to the function, 
//   will add the standard request IRP to the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int8   request_type   I
//     It points to the characteristics of request, including direction, type 
//     and ther recipient. Refer to USB 2.0 spec page 248, Table 9-2.
//   u_int16   feature_selector   I
//     It points to the feature selector values, refer to USB 2.0 spec page 252, 
//     Table 9-6.
//   u_int16   recipient    I
//     It points to the recipient device, interface or endpoints, ect.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbSetFeature(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int8           request_type,
  u_int16          feature_selector, 
  u_int16          recipient
    
){
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = request_type;
    a_request[1] = 0x03;
    a_request[2] = feature_selector >> 8;
    a_request[3] = feature_selector;
    a_request[4] = recipient >> 8;
    a_request[5] = recipient;
    a_request[6] = 0;
    a_request[7] = 0;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    memcpy (p_setup_data->p_data, a_request, 8);
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, NULL,USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("carbonXUsbSetFeature(): The pipe does not exist or is not a Control Pipe\n");
}



//%FS===========================================================================
// carbonXUsbSetIntf
//
// Description:
//   This function is standard request Set_Interface. Call to the function will 
//   add the standard request IRP to the specific pipe.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe.
//   u_int16  alternate_set    I
//     It points to the alternate setting value.
//   u_int16  interface    I
//     It points to the specified interface.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbSetIntf(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          alternate_set, 
  u_int16          interface
){
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = 0x01;
    a_request[1] = 0x0b;
    a_request[2] = alternate_set >> 8;
    a_request[3] = alternate_set;
    a_request[4] = interface >> 8;
    a_request[5] = interface;
    a_request[6] = 0;
    a_request[7] = 0;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    memcpy (p_setup_data->p_data, a_request, 8);
    
    p_irp = carbonXUsbIrpCreate(p_setup_data,NULL,USB_IRP_TYPE_CTRL_OUT,-1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("carbonXUsbSetIntf(): The pipe does not exist or is not a Control Pipe\n");
}



//%FS===========================================================================
// carbonXUsbSynchFrame
//
// Description:
//   This function is standard request SYNCH_FRAME. Call to the function will 
//   add the standard request IRP to the specific pipe.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe.
//   u_int16  endpoint_num    I
//     It points to the specific endpoint.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbSynchFrame(
  USB_PipeDescr_t* p_pipe,		// Control pipe
  u_int16          endpoint_num
){
  USB_Data_t*      p_setup_data;
  USB_Data_t*      p_send_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = 0x82;
    a_request[1] = 0xc;
    a_request[2] = 0;
    a_request[3] = 0;
    a_request[4] = endpoint_num >> 8;
    a_request[5] = endpoint_num;
    a_request[6] = 0;
    a_request[7] = 2;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_send_data, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("carbonXUsbSynchFrame(): The pipe does not exist or is not a Control Pipe\n");
}


//%FS===========================================================================
// carbonXUsbClearTTBuffer
//
// Description:
//   This function is standard request CLEAR_TT_BUFFER for HUB. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe.
//   u_int8   dev_addr    I
//     It points to the device address connected the hub.
//   u_int8   ep_num     I
//     It points to the endpoint number  being cleared.
//   USB_EpType_e   ep_type   I
//     It points to the endpoint type being cleared.
//   USB_Direction_e  direction  I
//     It points to the endpoint direction  being cleared.
//   u_int16   tt_port     I
//     It points to the TT_port number.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbClearTTBuffer(
  USB_PipeDescr_t* p_pipe,
  u_int8           dev_addr,
  u_int8           ep_num,
  USB_EpType_e     ep_type,
  USB_Direction_e  direction,
  u_int16          tt_port
){
  USB_Data_t*      p_setup_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  u_int16          temp;
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    if(direction == USB_DIR_OUT){
      temp = 0;
    }
    else {
      temp = 1;
    }
  
    temp <<= 4;

    temp = (direction == USB_DIR_OUT) ? 0 : 0x10;
    
    if(ep_type == USB_EPTYPE_ISO){
      temp += 1;
    }
    else if (ep_type == USB_EPTYPE_BULK){
      temp += 2;
    }
    else if (ep_type == USB_EPTYPE_INTR){
      temp += 3;
    }
    temp <<= 7;
    
    temp += dev_addr;
    temp <<= 4;
    temp += ep_num;
    
    a_request[0] = USB_CLEAR_TT_BUFFER;
    a_request[1] = 0x08;
    a_request[2] = temp >> 8;
    a_request[3] = temp;
    a_request[4] = tt_port >> 8;
    a_request[5] = tt_port;
    a_request[6] = 0;
    a_request[7] = 0;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    memcpy (p_setup_data->p_data, a_request, 8);
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, NULL, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("carbonXUsbClearTTBuffer(): The pipe does not exist or is not a Control Pipe\n");
}



//%FS===========================================================================
// carbonXUsbResetTT
//
// Description:
//   This function is standard request RESET_TT for HUB. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe.
//   u_int16   tt_port     I
//     It points to the TT_port number.
//
// Return:
//   void
//%FE===========================================================================
void carbonXUsbResetTT(USB_PipeDescr_t*  p_pipe, u_int16 tt_port){
  USB_Data_t*      p_setup_data;
  USB_Data_t*      p_send_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if(p_pipe != NULL){
    a_request[0] = USB_RESET_TT;
    a_request[1] = 0x9;
    a_request[2] = 0;
    a_request[3] = 0;
    a_request[4] = tt_port >> 8;
    a_request[5] = tt_port;
    a_request[6] = 0;
    a_request[7] = 0;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_send_data, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe, p_irp);
  }
  else MSG_Error("USB_STD_ResetTT(): The pipe does not exist or is not a Control Pipe\n");
}



//%FS===========================================================================
// USB_GetTTState
//
// Description:
//   This function is standard request GET_TT_STATE for HUB. Call to the function 
//   it will add the standard request IRP to the specific pipe.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe.
//   u_int16   tt_flags    I
//     It points to vendor specific usage.
//   u_int16   tt_port     I
//     It points to the TT_port number.
//   u_int16   tt_state_length    I
//     It points to the number of bytes to transfer
//
// Return:
//   void
//%FE===========================================================================
void USB_GetTTState(
  USB_PipeDescr_t*  p_pipe, 
  u_int16           tt_flags,
  u_int16           tt_port,
  u_int16           tt_state_length
){
  USB_Data_t*      p_setup_data;
  USB_Data_t*      p_send_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = USB_GET_TT_STATE;
    a_request[1] = 0xa;
    a_request[2] = 0;
    a_request[3] = tt_flags;
    a_request[4] = tt_port >> 8;
    a_request[5] = tt_port;
    a_request[6] = tt_state_length >> 8;
    a_request[7] = tt_state_length;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_send_data, USB_IRP_TYPE_CTRL_IN, -1);
    
    carbonXUsbPipePushInIRP(p_pipe, p_irp);
  }
  else MSG_Error("USB_GetTTState(): The pipe does not exist or is not a Control Pipe\n");
}



//%FS===========================================================================
// USB_StopTT
//
// Description:
//   This function is standard request STOP_TT for HUB. Call to the function 
//   will add the standard request IRP to the specific pipe.
//
// Parameter:
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the specific pipe.
//   u_int16   tt_port     I
//     It points to the TT_port number.
//
// Return:
//   void
//%FE===========================================================================
void USB_StopTT(USB_PipeDescr_t*  p_pipe, u_int16 tt_port){
  USB_Data_t*      p_setup_data;
  USB_Data_t*      p_send_data;
  USB_Irp_t*       p_irp;
  u_int8           a_request[8];
  
  if( (p_pipe != NULL) && (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL) ){
    a_request[0] = USB_STOP_TT;
    a_request[1] = 0xb;
    a_request[2] = 0;
    a_request[3] = 0;
    a_request[4] = tt_port >> 8;
    a_request[5] = tt_port;
    a_request[6] = 0;
    a_request[7] = 0;
    
    p_setup_data = carbonXUsbDataRealloc( NULL,8 );
    memcpy (p_setup_data->p_data, a_request, 8);
    
    //    p_send_data = carbonXUsbDataRealloc(NULL, 0);
    p_send_data = NULL;
    
    p_irp = carbonXUsbIrpCreate(p_setup_data, p_send_data, USB_IRP_TYPE_CTRL_OUT, -1);
    
    carbonXUsbPipePushInIRP(p_pipe,p_irp);
  }
  else MSG_Error("USB_STD_StopTT(): The pipe does not exist.\n");
}


//%FS===========================================================================
// USB_STD_SetStdDevDescr
// 
// Description:
//   Sets the standard device descriptor.
//
// Parameter:
//   USB_StdDevDescr_t* p_stddev	I
//     It points to the standard device descriptor.
//   u_int8* 		p_byte		I
//     It is the data array to store the received device descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetStdDevDescr(USB_StdDevDescr_t* p_stddev, u_int8* p_byte) {
  p_stddev->b_length         = *p_byte;
  p_stddev->b_descr_type     = *(p_byte- + 1);
  p_stddev->bcd_usb          = (*(p_byte + 2) << 8) + *(p_byte + 3);
  p_stddev->b_dev_class      = *(p_byte + 4);
  p_stddev->b_dev_sub_class  = *(p_byte + 5);
  p_stddev->b_dev_protocol   = *(p_byte + 6);
  p_stddev->b_max_pkt_size   = *(p_byte + 7);
  p_stddev->id_vendor        = (*(p_byte + 8) << 8) + *(p_byte + 9);
  p_stddev->id_product       = (*(p_byte + 10) << 8) + *(p_byte + 11);
  p_stddev->bcd_dev          = (*(p_byte + 12) << 8) + *(p_byte + 13);
  p_stddev->i_manu           = *(p_byte + 14);
  p_stddev->i_product        = *(p_byte + 15);
  p_stddev->i_serial_num     = *(p_byte + 16);
  p_stddev->i_num_cfg        = *(p_byte + 17);
}



//%FS===========================================================================
// USB_STD_SetStdCfgDescr
// 
// Description:
//   Sets standard device configuration descriptor.
//
// Parameter:
//   USB_StdCfgDescr_t* p_stdcfg	I
//     It points to the standard device configuration descriptor.
//   u_int8* 		p_byte		I
//     It is the data array to store the received device configuration descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetStdCfgDescr(USB_StdCfgDescr_t* p_stdcfg, u_int8* p_byte) {
  p_stdcfg->b_length        = *p_byte;
  p_stdcfg->b_descr_type    = *(p_byte + 1);
  p_stdcfg->w_total_length  = (*(p_byte + 2) << 8) + *(p_byte + 3);
  p_stdcfg->b_num_if        = *(p_byte + 4);
  p_stdcfg->b_cfg_val       = *(p_byte + 5);
  p_stdcfg->i_cfg           = *(p_byte + 6);
  p_stdcfg->bm_attr         = *(p_byte + 7);
  p_stdcfg->b_max_power     = *(p_byte + 8);
}



//%FS===========================================================================
// USB_STD_SetStdIfDescr
// 
// Description:
//   Sets standard device interface descriptor.
//
// Parameter:
//   USB_StdIfDescr_t*  p_stdif		I
//     It points to the standard device interface descriptor.
//   u_int8* 		p_byte		I
//     It is the data array to store the received device interface descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetStdIfDescr(USB_StdIfDescr_t* p_stdif, u_int8* p_byte){
  p_stdif->b_length         = *p_byte;
  p_stdif->b_descr_type     = *(p_byte + 1);
  p_stdif->b_if_num         = *(p_byte + 2);
  p_stdif->b_alt_set        = *(p_byte + 3);
  p_stdif->b_num_ep         = *(p_byte + 4);
  p_stdif->b_if_class       = *(p_byte + 5);
  p_stdif->b_if_sub_class   = *(p_byte + 6);
  p_stdif->b_if_protocol    = *(p_byte + 7);
  p_stdif->i_if   	    = *(p_byte + 8);
}



//%FS===========================================================================
// USB_STD_SetStdEpDescr
// 
// Description:
//   Sets standard device endpoint descriptor.
//
// Parameter:
//   USB_StdEpDescr_t* 	p_stdep		I
//     It points to the standard device endpoint descriptor.
//   u_int8* 		p_byte		I
//     It is the data array to store the received device endpoint descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetStdEpDescr(USB_StdEpDescr_t* p_stdep, u_int8* p_byte){
 p_stdep->b_length	= *p_byte;
 p_stdep->b_descr_type	= *(p_byte + 1);
 p_stdep->b_ep_addr	= *(p_byte + 2);
 p_stdep->bm_attr	= *(p_byte + 3);
 p_stdep->w_max_pkt_size= (*(p_byte + 4) << 8) + *(p_byte + 5);
 p_stdep->b_interval	= *(p_byte + 6);
}



//%FS==========================================================================
// USB_STD_SetStdQualDescr
// 
// Description:
//   Sets standard device qualifier descriptor.
//
// Parameter:
//   USB_StdDevQualDescr_t* p_stdqual	I
//     It points to the standard device qualifier descriptor.
//   u_int8* 		    p_byte	I
//     It is the data array to store the received device qualifier descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetStdQualDescr(USB_StdDevQualDescr_t* p_stdqual, u_int8* p_byte){
  //@@@RZ possible Endian portability issue. Needs to be checked.
  p_stdqual->b_length	     = *p_byte;
  p_stdqual->b_descr_type    = *(p_byte + 1);
  p_stdqual->bcd_usb	     = (*(p_byte + 2) << 8) + *(p_byte + 3);
  p_stdqual->b_dev_class     = *(p_byte + 4);
  p_stdqual->b_dev_sub_class = *(p_byte + 5);
  p_stdqual->b_dev_protocol  = *(p_byte + 6);
  p_stdqual->b_max_pkt_size  = *(p_byte + 7);
  p_stdqual->b_num_cfg	     = *(p_byte + 8);
  p_stdqual->b_rsv	     = *(p_byte + 9);
}



//%FS===========================================================================
// USB_STD_SetStdOthSpeedDescr
// 
// Description:
//   Sets standard device other speed descriptor.
//
// Parameter:
//   USB_StdOsCfgDescr_t* p_stdos	I
//     It points to the standard device other speed descriptor.
//   u_int8* 		  p_byte	I
//     It is the data array to store the received device other speed descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetStdOthSpeedDescr(USB_StdOsCfgDescr_t* p_stdos, u_int8* p_byte){
  p_stdos->b_length	  = *p_byte;
  p_stdos->b_descr_type	  = *(p_byte + 1);
  p_stdos->w_total_length = (*(p_byte + 2) << 8) + *(p_byte + 3);
  p_stdos->b_num_if	  = *(p_byte + 4);
  p_stdos->b_cfg_value	  = *(p_byte + 5);
  p_stdos->i_cfg	  = *(p_byte + 6);
  p_stdos->bm_attr	  = *(p_byte + 7);
  p_stdos->b_max_power	  = *(p_byte + 8);
}


//%FS===========================================================================
// USB_STD_GetStdDevDescr
// 
// Description:
//   Gets the standard device descriptor.
//
// Parameter:
//   USB_StdDevDescr_t* p_stddev	I
//     It points to the standard device descriptor.
//   u_int8* 		p_byte		I
//     It is the data array to send the device descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_GetStdDevDescr(USB_StdDevDescr_t* p_stddev, u_int8* p_byte) {
  *p_byte       = p_stddev->b_length;
  *(p_byte+1)   = p_stddev->b_descr_type;
  *(p_byte+2)   = p_stddev->bcd_usb >> 8;
  *(p_byte+3)   = p_stddev->bcd_usb;
  *(p_byte+4)   = p_stddev->b_dev_class;
  *(p_byte+5)   = p_stddev->b_dev_sub_class;
  *(p_byte+6)   = p_stddev->b_dev_protocol;
  *(p_byte+7)   = p_stddev->b_max_pkt_size;
  *(p_byte+8)   = p_stddev->id_vendor >> 8;
  *(p_byte+9)   = p_stddev->id_vendor;
  *(p_byte+10)  = p_stddev->id_product >> 8;
  *(p_byte+11)  = p_stddev->id_product;
  *(p_byte+12)  = p_stddev->bcd_dev >> 8;
  *(p_byte+13)  = p_stddev->bcd_dev;
  *(p_byte+14)  = p_stddev->i_manu;
  *(p_byte+15)  = p_stddev->i_product;
  *(p_byte+16)  = p_stddev->i_serial_num;
  *(p_byte+17)  = p_stddev->i_num_cfg;
}					// end of USB_STD_get_stddev()



//%FS===========================================================================
// USB_STD_GetStdCfgDescr
// 
// Description:
//   Gets standard device configuration descriptor.
//
// Parameter:
//   USB_StdCfgDescr_t* p_stdcfg	I
//     It points to the standard device configuration descriptor.
//   u_int8* 		p_byte		I
//     It is the data array to send the device configuration descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_GetStdCfgDescr(USB_StdCfgDescr_t* p_stdcfg, u_int8* p_byte) {
  *p_byte       = p_stdcfg->b_length;
  *(p_byte+1)   = p_stdcfg->b_descr_type;
  *(p_byte+2)   = p_stdcfg->w_total_length >> 8;
  *(p_byte+3)   = p_stdcfg->w_total_length;
  *(p_byte+4)   = p_stdcfg->b_num_if;
  *(p_byte+5)   = p_stdcfg->b_cfg_val;
  *(p_byte+6)   = p_stdcfg->i_cfg;
  *(p_byte+7)   = p_stdcfg->bm_attr;
  *(p_byte+8)   = p_stdcfg->b_max_power;
}



//%FS===========================================================================
// USB_STD_GetStdIfDescr
// 
// Description:
//   Gets standard device interface descriptor.
//
// Parameter:
//   USB_StdIfDescr_t*  p_stdif		I
//     It points to the standard device interface descriptor.
//   u_int8* 		p_byte		I
//     It is the data array to send the device interface descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_GetStdIf(USB_StdIfDescr_t* p_stdif, u_int8* p_byte){
  *p_byte       = p_stdif->b_length;
  *(p_byte+1)   = p_stdif->b_descr_type;
  *(p_byte+2)   = p_stdif->b_if_num;
  *(p_byte+3)   = p_stdif->b_alt_set;
  *(p_byte+4)   = p_stdif->b_num_ep;
  *(p_byte+5)   = p_stdif->b_if_class;
  *(p_byte+6)   = p_stdif->b_if_sub_class;
  *(p_byte+7)   = p_stdif->b_if_protocol;
  *(p_byte+8)   = p_stdif->i_if;
}



//%FS===========================================================================
// USB_STD_GetStdEpDescr
// 
// Description:
//   Gets standard device endpoint descriptor.
//
// Parameter:
//   USB_StdEpDescr_t*  p_stdep		I	
//     It points to the standard device endpoint descriptor.
//   u_int8* 		p_byte		I	
//     It is the data array to send the device endpoint descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_GetStdEpDescr(USB_StdEpDescr_t* p_stdep, u_int8* p_byte) {
  *p_byte       = p_stdep->b_length;
  *(p_byte+1)   = p_stdep->b_descr_type;
  *(p_byte+2)   = p_stdep->b_ep_addr;
  *(p_byte+3)   = p_stdep->bm_attr;
  *(p_byte+4)   = p_stdep->w_max_pkt_size >> 8;
  *(p_byte+5)   = p_stdep->w_max_pkt_size;
  *(p_byte+6)   = p_stdep->b_interval;
}




//%FS===========================================================================
// USB_STD_GetStdQualDescr
// 
// Description:
//   Gets standard device qualifier descriptor.
// 
// Parameter:
//   USB_StdDevQualDescr_t* p_stdqual 	I	
//     It points to the standard device qualifier descriptor.
//   u_int8* 		    p_byte	I	
//     It is the data array to send the device qualifier descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_GetStdQualDescr(USB_StdDevQualDescr_t* p_stdqual, u_int8* p_byte) {
  *p_byte       = p_stdqual->b_length;         
  *(p_byte+1)   = p_stdqual->b_descr_type;     
  *(p_byte+2)   = p_stdqual->bcd_usb >> 8;     
  *(p_byte+3)   = p_stdqual->bcd_usb;          
  *(p_byte+4)   = p_stdqual->b_dev_class;      
  *(p_byte+5)   = p_stdqual->b_dev_sub_class;  
  *(p_byte+6)   = p_stdqual->b_dev_protocol;   
  *(p_byte+7)   = p_stdqual->b_max_pkt_size;   
  *(p_byte+8)   = p_stdqual->b_num_cfg;        
  *(p_byte+9)   = p_stdqual->b_rsv;            
}   



//%FS===========================================================================
// USB_STD_GetStdOthSpeedDescr
// 
// Description:
//   Gets standard device other speed descriptor.
//
// Parameter:
//   USB_StdOsCfgDescr_t* p_stdos  I
//     It points to the standard device other speed descriptor.
//   u_int8* p_byte  I
//     It is the data array to send the received device other speed descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_GetStdOthSpeedDescr(USB_StdOsCfgDescr_t* p_stdos, u_int8* p_byte){
  *p_byte       = p_stdos->b_length;
  *(p_byte+1)   = p_stdos->b_descr_type;
  *(p_byte+2)   = p_stdos->w_total_length >> 8;
  *(p_byte+3)   = p_stdos->w_total_length;
  *(p_byte+4)   = p_stdos->b_num_if;
  *(p_byte+5)   = p_stdos->b_cfg_value;
  *(p_byte+6)   = p_stdos->i_cfg;
  *(p_byte+7)   = p_stdos->bm_attr;
  *(p_byte+8)   = p_stdos->b_max_power;
}


//%FS=========================================================================*
// USB_STD_SetStdHubDescr
// 
// Description:
//   Sets standard hub descriptor.
//
// Parameter:
//   USB_StdOsCfgDescr_t* p_stdos	I	
//     It points to the standard hub descriptor.
//   u_int8* 		  p_byte	I	
//     It is the data array to send the received hub descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_SetStdHubDescr(USB_StdHubDescr_t* p_stdhub, u_int8* p_byte){
  u_int32 i;
  u_int32 num;
  u_int8* p_data;

  p_stdhub->b_descr_len        = *p_byte;
  p_stdhub->b_descr_type       = *(p_byte + 1);
  p_stdhub->b_num_ports        = *(p_byte + 2);
  p_stdhub->w_hub_charact      = (*(p_byte + 3) << 8) + *(p_byte + 4);
  p_stdhub->b_pwron2good       = *(p_byte + 5);
  p_stdhub->b_hub_ctrl_cur     = *(p_byte + 6);
  
  num = p_stdhub->b_num_ports;
  p_data = p_byte + 7;
  for(i=0;i<num;i++){
    p_stdhub->a_dev_removal[i] = *p_data;
    p_data ++;
  }
  for(i=0;i<num;i++){
   p_stdhub->a_port_pwr_ctrl_mask[i] = *p_data;
   p_data ++;
  }
}


//%FS=========================================================================*
// USB_STD_GetStdHubDescr
// 
// Description:
//   Gets standard hub descriptor.
//
// Parameter:
//   USB_StdOsCfgDescr_t* p_stdos	I	
//     It points to the standard hub descriptor.
//   u_int8* 		  p_byte	I
//     It is the data array to send the received hub descriptor. 
// 
// Return:
//   void
//%FE===========================================================================
void USB_STD_GetStdHubDescr(USB_StdHubDescr_t* p_stdhub, u_int8* p_byte){
  u_int32 i;
  u_int32 num;
  u_int8* p_data;


  *p_byte       = p_stdhub->b_descr_len;
  *(p_byte + 1) = p_stdhub->b_descr_type;
  *(p_byte + 2) = p_stdhub->b_num_ports;
  *(p_byte + 3) = p_stdhub->w_hub_charact >> 8;
  *(p_byte + 4) = p_stdhub->w_hub_charact;
  *(p_byte + 5) = p_stdhub->b_pwron2good;
  *(p_byte + 6) = p_stdhub->b_hub_ctrl_cur;
  
  num = p_stdhub->b_num_ports;
  p_data = p_byte + 7;
  for(i=0;i<num;i++){
    *p_data = p_stdhub->a_dev_removal[i];
    p_data ++;
  }
  for(i=0;i<num;i++){
   *p_data = p_stdhub->a_port_pwr_ctrl_mask[i];
   p_data ++;
  }

}


//%FS===========================================================================
// USB_STD_ResetDescr
//
// Description:
//   This function resets the standard descriptors to be default values.
//
// Parameter:
//   USB_Descr_t*  p_desr  I
//     It points to structure USB_Descr_t that stores standard
//     descriptors.
//     1. Reset the standard device descriptor
//     2. Reset the standard configuration descriptor
//     3. Reset the standard interface descriptor
//     4. Reset the standard endpoint descriptor
//     5. Reset the standard device_qualifier descriptor
//     6. Reset the standard other_speed_configuration descriptor
//     7. Reset the standard hub descriptor
//
// Return value:
//   void
//%FE===========================================================================
void USB_STD_ResetDescr(USB_Descr_t* p_desr) {

  u_int32 i = 0;
  
  //--------------------- step 1 --------------------------
  p_desr->stddev.b_length        = 0;
  p_desr->stddev.b_descr_type    = 0;
  p_desr->stddev.bcd_usb         = 0;
  p_desr->stddev.b_dev_class     = 0;
  p_desr->stddev.b_dev_sub_class = 0;
  p_desr->stddev.b_dev_protocol  = 0;
  p_desr->stddev.b_max_pkt_size  = 0;
  p_desr->stddev.id_vendor       = 0;
  p_desr->stddev.id_product      = 0;
  p_desr->stddev.bcd_dev         = 0;
  p_desr->stddev.i_manu          = 0;
  p_desr->stddev.i_product       = 0;
  p_desr->stddev.i_serial_num    = 0;
  p_desr->stddev.i_num_cfg       = 0;

  //--------------------- step 2 ---------------------------
  for(i = 0; i < USB_MAX_CFG_NUMBER; i++) {
  	
    p_desr->stdcfg[i].b_length       = 0;
    p_desr->stdcfg[i].b_descr_type   = 0;
    p_desr->stdcfg[i].w_total_length = 0;
    p_desr->stdcfg[i].b_num_if       = 0;
    p_desr->stdcfg[i].b_cfg_val      = 0;
    p_desr->stdcfg[i].i_cfg          = 0;
    p_desr->stdcfg[i].bm_attr        = 0;
    p_desr->stdcfg[i].b_max_power    = 0;
    
  }
  
  //-------------------- step 3 ----------------------------
  for(i = 0; i < USB_MAX_IF_NUMBER; i++) {

    p_desr->a_stdif[i].b_length	 	= 0;
    p_desr->a_stdif[i].b_descr_type	= 0;
    p_desr->a_stdif[i].b_if_num	 	= 0;
    p_desr->a_stdif[i].b_alt_set	= 0;
    p_desr->a_stdif[i].b_num_ep	 	= 0;
    p_desr->a_stdif[i].b_if_class  	= 0;
    p_desr->a_stdif[i].b_if_sub_class 	= 0;
    p_desr->a_stdif[i].b_if_protocol	= 0;
    p_desr->a_stdif[i].i_if		= 0;
  
  }
  
  //-------------------- step 4 ----------------------------
  for(i = 0; i < USB_MAX_EP_DESCR_NUMBER; i++) {
    p_desr->a_stdep[i].b_length		= 0;
    p_desr->a_stdep[i].b_descr_type	= 0;
    p_desr->a_stdep[i].b_ep_addr	= 0;
    p_desr->a_stdep[i].bm_attr		= 0;
    p_desr->a_stdep[i].w_max_pkt_size	= 0;
    p_desr->a_stdep[i].b_interval	= 0;
  }
  
  //------------------- step 5 -----------------------------
  p_desr->stdqual.b_length	  = 0;
  p_desr->stdqual.b_descr_type	  = 0;
  p_desr->stdqual.bcd_usb	  = 0;
  p_desr->stdqual.b_dev_class	  = 0;
  p_desr->stdqual.b_dev_sub_class = 0;
  p_desr->stdqual.b_dev_protocol  = 0;
  p_desr->stdqual.b_max_pkt_size  = 0;
  p_desr->stdqual.b_num_cfg	  = 0;
  p_desr->stdqual.b_rsv		  = 0;
  
  //------------------- step 6 -----------------------------
  p_desr->stdos.b_length	= 0;
  p_desr->stdos.b_descr_type	= 0;
  p_desr->stdos.w_total_length	= 0;
  p_desr->stdos.b_num_if	= 0;
  p_desr->stdos.b_cfg_value	= 0;
  p_desr->stdos.i_cfg		= 0;
  p_desr->stdos.bm_attr		= 0;
  p_desr->stdos.b_max_power	= 0;

  //-------------------- step 7 ---------------------------
  p_desr->stdhub.b_descr_len	    = 0;
  p_desr->stdhub.b_descr_type	    = 0;
  p_desr->stdhub.b_num_ports	    = 0;
  p_desr->stdhub.w_hub_charact	    = 0;
  p_desr->stdhub.b_pwron2good	    = 0;
  p_desr->stdhub.b_hub_ctrl_cur	    = 0;

  for(i = 0 ;i < MAX_PORT_NUMBER; i++){
    p_desr->stdhub.a_dev_removal[i]	   = 0;
    p_desr->stdhub.a_port_pwr_ctrl_mask[i] = 0;
  }
}

