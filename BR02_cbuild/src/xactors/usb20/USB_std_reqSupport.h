//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 * File Name     : $Source$
 *
 * Comments      : First line. 
 *                 Following line
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 *
 ***************************************************************************/
#ifndef USB_STD_REQ_SUPPORT_H
#define USB_STD_REQ_SUPPORT_H
 
void USB_STD_SetDevProfFromStdDevDescr( USB_PipeDescr_t*  p_pipe );

void USB_STD_SetDevProfFromStdCfgDescr( USB_PipeDescr_t*  p_pipe );

void USB_STD_SetDevFromStdDevDescr(USB_PipeDescr_t* p_pipe);

void USB_STD_SetStdDescr(USB_DevProfile_t* p_dev,USB_Data_t* p_data_t,u_int8 descr_index);

void USB_STD_SetStdDevDescr(USB_StdDevDescr_t* p_stddev, u_int8* p_byte);

void USB_STD_SetStdCfgDescr(USB_StdCfgDescr_t* p_stdcfg, u_int8* p_byte);

void USB_STD_SetStdIfDescr(USB_StdIfDescr_t* p_stdif, u_int8* p_byte);

void USB_STD_SetStdEpDescr(USB_StdEpDescr_t* p_stdep, u_int8* p_byte);

void USB_STD_SetStdQualDescr(USB_StdDevQualDescr_t* p_stdqual, u_int8* p_byte);

void USB_STD_SetStdOthSpeedDescr(USB_StdOsCfgDescr_t* p_stdos, u_int8* p_byte);

void USB_STD_GetStdDevDescr(USB_StdDevDescr_t* p_stddev, u_int8* p_byte);

void USB_STD_GetStdCfgDescr(USB_StdCfgDescr_t* p_stdcfg, u_int8* p_byte);

void USB_STD_GetStdIfDescr(USB_StdIfDescr_t* p_stdif, u_int8* p_byte);

void USB_STD_GetStdEpDescr(USB_StdEpDescr_t* p_stdep, u_int8* p_byte);

void USB_STD_GetStdQualDescr(USB_StdDevQualDescr_t* p_stdqual, u_int8* p_byte);

void USB_STD_GetStdOthSpeedDescr(USB_StdOsCfgDescr_t* p_stdos, u_int8* p_byte);

void USB_STD_SetStdHubDescr(USB_StdHubDescr_t* p_stdhub, u_int8* p_byte);

void USB_STD_GetStdHubDescr(USB_StdHubDescr_t* p_stdhub, u_int8* p_byte);

void USB_STD_ResetDescr(USB_Descr_t* p_desr);

#endif

/*
 * Log           : 
 *               : $Log$
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.5  2004/08/27 21:31:40  zippelius
 *               : Updated tests (fixes, expanded, added self-checking). Misc clean-ups
 *               :
 *               : Revision 1.4  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.3  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.2  2004/03/26 15:00:18  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.2  2003/11/25 09:41:10  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.1  2003/09/27 08:17:38  serlina
 *               : reset add speed
 *               :
 *               : Revision 2.0  2003/09/27 06:34:13  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:43  adam
 *               : updated file directory
 *               :
 *               : Revision 1.8  2003/09/26 12:27:52  adam
 *               : Debug
 *               :
 *               : Revision 1.7  2003/09/23 12:08:34  serlina
 *               : change std cfg
 *               :
 *               : Revision 1.6  2003/09/19 07:24:54  serlina
 *               : change the enum function
 *               :
 *               : Revision 1.5  2003/09/11 08:09:18  issac
 *               : major update after code review
 *               :
 *               : Revision 1.4  2003/09/08 14:19:14  serlina
 *               : change function name
 *               :
 *               : Revision 1.3  2003/09/08 07:56:05  serlina
 *               : change link
 *               :
 *               : Revision 1.2  2003/09/08 02:30:58  adam
 *               : updated data structre
 *               :
 *               : Revision 1.1  2003/08/27 15:04:02  serlina
 *               : updated Do Enum series of functions, and dev prof data structure
 *               :
 */
