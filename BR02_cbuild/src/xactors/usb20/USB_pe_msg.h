//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 *
 * Comments      : This file is required for PE. It must contain:
 *
 *                 1. All necessary function for C side monitor to log 
 *                    message.
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.5  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.4  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.3  2004/04/16 20:35:15  zippelius
 *               : Some code clean up. Added defines
 *               :
 *               : Revision 1.2  2004/03/26 15:00:17  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.0  2003/09/27 06:34:13  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:42  adam
 *               : updated file directory
 *               :
 *               : Revision 1.4  2003/09/08 13:53:20  adam
 *               : Change USB_ to USB_PE_
 *               :
 *               : Revision 1.3  2003/08/25 07:01:14  adam
 *               : New revision, created.
 *               :
 *               : Revision 1.2  2003/08/14 14:22:02  adam
 *               : New revison, created.
 *               :
 ***************************************************************************/

#ifndef USB_PE_MSG_H
#define USB_PE_MSG_H

/* Function to print the header of the log message */
void USB_PE_MsgHeader();

/* Function to log trans speed of the transaction */
void USB_PE_MsgSpeed(USB_Trans_t* p_trans);

/* Function to log endpoint type of the transaction */
void USB_PE_MsgEptType(USB_Trans_t* p_trans);

/* Function to log error injection information of the transaction */
void USB_PE_MsgErrInject(USB_Trans_t* p_trans);

/* Function to log token packet of the transaction */
void USB_PE_MsgToken(u_int8 token_pid, u_int32 token_addr, u_int32 token_ept_num);

/* Function to log data packet of the transaction */
void USB_PE_MsgData(u_int8* p_data, u_int32 data_size);

/* Function to log packets after a split token packet of the transaction */
void USB_PE_MsgAfterSplit(u_int8* p_after_split);

/* 
 * Function to logs all handshake packets (including handshake packet and data 
 * packet) of the transaction.
 */
void USB_PE_MsgHandshake(u_int8* p_msg_hs, USB_Pid_e msg_hs, u_int32 msg_hs_size);


#endif
