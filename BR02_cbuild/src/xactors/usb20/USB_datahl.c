
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file includes all functions relating to HostCtrlTop and speed operation,
 *                 It can be classified into 2 groups:
 *                 [USB_*, USB_DATA_PA_*]
 *
 *                 USB_* are API functions that user can use for API
 *                 related operations, they include:
 *
 *                 1. carbonXUsbDataRealloc();
 *                 
 *                 USB_DATA_PA_* are DATAAP functions that SVC uses to operate 
 *                 on Data pointer array struct related operations, they include:
 *
 *                 1. USB_DATA_PA_Init();
 *                 2. USB_DATA_PA_Reset();
 *                 3. USB_DATA_PA_Free();
 *                 4. USB_DATA_PA_IsFull();
 *                 5. USB_DATA_PA_Cur();
 *                 6. USB_DATA_PA_GetDataAP();
 *
 *               : Revision 1.12  2004/10/21 16:21:32  zippelius
 *               : Moved some functions. Renamed some functions
 *               :
 *               : Revision 1.11  2004/08/18 20:29:38  zippelius
 *               : Cleaned-up tests. Moved some code into test_support
 *               :
 *               : Revision 1.10  2004/08/13 18:40:35  zippelius
 *               : Removed bogus Pipe 0; Moved toggle tracking from Pipe into EP; Fixed 4 byte alloc, when data size==0;Misc fixes
 *               :
 *               : Revision 1.9  2004/08/11 01:59:19  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.8  2004/08/06 19:21:07  zippelius
 *               : Updated test header with description
 *               :
 *               : Revision 1.7  2004/06/24 18:13:42  fredieu
 *               : Do not malloc zero bytes!
 *               :
 *               : Revision 1.6  2004/04/26 21:54:25  jafri
 *               : modified english in verilog and c code and added usb_test1a to test table
 *               :
 *               : Revision 1.5  2004/04/26 21:34:29  zippelius
 *               : Code clean-up regarding IRP state and misc other
 *               :
 *               : Revision 1.4  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.3  2004/04/19 18:38:57  jafri
 *               : fixed english in comments
 *               :
 *               : Revision 1.2  2004/03/26 14:59:22  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.1  2003/11/25 09:41:33  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.0  2003/09/27 06:33:45  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:18  adam
 *               : updated file directory
 *               :
 *               : Revision 1.2  2003/09/08 13:59:16  serlina
 *               : common
 *               :
 *               : Revision 1.1  2003/09/08 07:59:34  serlina
 *               : change link
 *               :
 ***************************************************************************/


#include "USB_support.h"

//%FS=======================================================================
// USB_DATA_PA_Init
//
// Description:
//   This function only initializes the data pointer array struct.
//
// Parameters: 
//   USB_DataAPStatic_t*  p_data_mgr  I
//     It points to the data pointer array struct.
//
// Return: 
//   void
//%FE=======================================================================
void USB_DATA_PA_Init(USB_DataAPStatic_t*  p_data_mgr){
  p_data_mgr->size          = 0;
  p_data_mgr->count     = 0;
  p_data_mgr->ppa_data_head = NULL;
}



//%FS===========================================================================
// USB_DATA_PA_Reset
//
// Description:
//   This function resets the data pointer array struct. It will free all data and set 
//   the value to default value.
//
// Parameters: 
//   USB_DataAPStatic_t*  p_data_mgr  I
//     It points to the data pointer array struct.
//
// Return:
//   void
//
// Note:
//   If reset, all the Data information will be lost.
//%FE===========================================================================
void USB_DATA_PA_Reset(USB_DataAPStatic_t*  p_data_mgr){
  USB_DATA_PA_Free(p_data_mgr);
  
  p_data_mgr->size          = 0;
  p_data_mgr->count     = 0;
  p_data_mgr->ppa_data_head = NULL;
}



//%FS===========================================================================
// USB_DATA_PA_Free
//
// Description:
//   This function frees the data pointer array struct. All data in this 
//   pointer array will be lost.
//
// Parameters: 
//   USB_DataAPStatic_t*  p_data_mgr  I
//     It points to the IRP pointer array struct.
//
// Return:
//   void
//%FE===========================================================================
void USB_DATA_PA_Free (USB_DataAPStatic_t*  p_data_mgr){
  u_int32     i;
  USB_Data_t* p_data_temp;
  
  if ( p_data_mgr->ppa_data_head != NULL ){
    
    for(i=0;i<p_data_mgr->count;i++){
      
      p_data_temp = *(p_data_mgr->ppa_data_head + i);
      
      if (p_data_temp != NULL){
      	if (p_data_temp->size >0 ){
	  //	  printf( "++++++ USB_DATA_PA_Free (1) i=%d\n", i);
      	  tbp_free(p_data_temp->p_data);
      	}
	//	printf( "++++++ USB_DATA_PA_Free (2) i=%d\n", i);
        tbp_free(p_data_temp);
      }       
    }
    //    printf( "++++++ USB_DATA_PA_Free (3) i=%d\n", i);
    tbp_free (p_data_mgr->ppa_data_head);
  }
}



//%FS===========================================================================
// carbonXUsbDataRealloc
//
// Description:
//   This function allocates the memory for storing data with the specified size.
//   1. If input pointer is not NULL, check if the pointer is in the data arrayed 
//      pointer struct or not.
//      If the pointer is in the data pointer array struct, allocate a new memory and 
//      return.
//      If the pointer is not in the data pointer array struct, do next step.
//   2. Check the data pointer array struct is full or not. If full, extend the list.
//      If it has space, do next step.
//   3. Allocate new USB_Data_t X, reset X, attach X to the tail of the
//      the pointer array struct, and extend pointer array struct count by 1.
//   4. Allocate new u_int8, size is the specific data size.
//      It is for storing the data.
//   5. Return X
//
// Parameter:
//   USB_Data_t*    pio_data  IO
//     It points to the data list.
//   u_int32        data_size I
//     It is the size needed to store data.
//
// Return: 
//   USB_Data_t*
//     It points to a USB_Data_t struct. 
//     The strcut member--size equals data_size.
//     The strcut member--p_data points to the memory of storing data.
//     If no memory, the code will exit.
//
// Note:
//   The input parameter "p_data" must be the value which is returned value
//   of calling this function last time.
//   If there is no value, it must be set to NULL;  
//   otherwise,the old value will be lost.
//%FE===========================================================================
USB_Data_t* carbonXUsbDataRealloc(USB_Data_t* p_data, u_int32 data_size){
  
  USB_DataAPStatic_t*   p_data_mgr;
  
  USB_Data_t**     pp_data_cur;
  USB_Data_t**     pp_head_new;
  
  USB_Data_t*      p_data_cur;
  USB_Data_t*      p_data_t_new;
  
  u_int8*          p_data_new = 0;
  
  u_int32          i;
  //
  // +-------------------------------------------------+
  // | Data_t Array                                   |
  // +-------------------------------------------------+
  // | |0xSys_0|0xSys_1| ...                           |
  // | |Data_t1|Data_t2|Data_t3| ...... |Data_tn|  |
  // |                            |count           |
  // | |<---          Data_t list size         --->|  |
  // +-------------------------------------------------+
  // | Data_t1       0xSys_p_data_t1                   |
  // |   --Data_t    :                                 |
  // |     --size    : val = 10                        |
  // |     --p_data  : val = [0xSys_data1,0xSys_data10]|
  // +-------------------------------------------------+
  // | Data_t2       0xSys_p_data_t2                   |
  // |   --Data_t    :                                 |
  // |     --size    : val = 0                         |
  // |     --p_data  : val = NULL                      |
  // +-------------------------------------------------+
  // | Data_t3       0xSys_p_data_t3                   |
  // |   --Data_t    :                                 |
  // |     --size    : val = m                         |
  // |     --p_data  : val = [0xSys_data1,0xSys_datam] |
  // +-------------------------------------------------+
  // |    ......                                       |
  // +-------------------------------------------------+
  //
  p_data_mgr = USB_DATA_PA_GetDataAP();
  
  //-------------- step 1 --------------------
  //if p_data is already in the list, just reallocate it,
  if ( p_data != NULL ){ 
    if ( p_data_mgr != NULL ){
      for(i=0;i<p_data_mgr->count;i++){
        p_data_cur = *(p_data_mgr->ppa_data_head + i);
        
        if (p_data == p_data_cur){
	  if ( p_data->size > 0 ) {
	    p_data_new = (u_int8*)tbp_realloc(p_data->p_data, data_size, data_size);
          
	    if ( p_data_new == NULL ){
	      MSG_Panic("carbonXUsbDataRealloc(): Out of memory!\n");
	    }
	    p_data->size   = data_size;
	    p_data->p_data = p_data_new;
          
	  }
	  else {
	    if(data_size > 0) {
	      p_data_new = (u_int8*)tbp_malloc(data_size*sizeof(u_int8));
	      
	      if ( p_data_new == NULL ){
		MSG_Panic("carbonXUsbDataRealloc(): Out of memory!\n");
	      }
	      p_data->size = data_size;	    
	      p_data->p_data = p_data_new;
	    }
	  }
	  return ( p_data );
        }
      }
    }
  }
  
  //-------------- step 2 --------------------
  if( USB_DATA_PA_IsFull(p_data_mgr) ) {
    p_data_mgr->size += USB_DATA_PA_GROW_SIZE;
    pp_head_new = (USB_Data_t**)tbp_realloc(p_data_mgr->ppa_data_head,
					(sizeof(USB_Data_t*)*(p_data_mgr->size - 1)),
					(sizeof(USB_Data_t*)*p_data_mgr->size) );
   
    if ( pp_head_new == NULL ) {
      MSG_Panic("carbonXUsbDataRealloc(): Out of memory!\n");
    } 
     // Reset the unused portion to NULL for safety.
    for(i=p_data_mgr->count;i<p_data_mgr->size;i++){
      *(pp_head_new + i) = NULL;
    }
    p_data_mgr->ppa_data_head = pp_head_new;
  }      

  //-------------- step 3 --------------------
  pp_data_cur = USB_DATA_PA_Cur(p_data_mgr);
  
  p_data_t_new = (USB_Data_t*)calloc(1, sizeof(USB_Data_t));
    
  if (p_data_t_new == NULL) {
    MSG_Panic("carbonXUsbDataRealloc(): Out of memory!\n");
  }
  p_data_t_new->p_data = NULL;
  p_data_t_new->size   = 0;
  
  *pp_data_cur = p_data_t_new;
  p_data_mgr->count ++;
    
  //-------------- step 4 --------------------
  if(data_size > 0) {
    p_data_new = (u_int8*)tbp_malloc(data_size*sizeof(u_int8));

    if ( p_data_new == NULL ){
      MSG_Panic("carbonXUsbDataRealloc(): Out of memory!\n");
    }
  }

  p_data_t_new->size = data_size;
  
  p_data_t_new->p_data = p_data_new;
  
  //-------------- step 5 --------------------
  return (p_data_t_new);
}



//%FS=======================================================================
// USB_DATA_PA_IsFull
//
// Description:
//   The function checks if the data arrayed pointer struct is full or not.
//
// Parameters: 
//   USB_DataAPStatic_t*  p_data_mgr  I
//    It points to the data pointer array struct.
//
// Return:
//   If TRUE, the list is full.
//   If FALSE, the list has space.
//%FE=======================================================================
BOOL USB_DATA_PA_IsFull(USB_DataAPStatic_t*  p_data_mgr) {
  return (p_data_mgr->size <= p_data_mgr->count);
}


//%FS=======================================================================
// USB_DATA_PA_Cur
//
// Description:
//   The function gets the tail of the data pointer array struct.
//
// Parameters: 
//   USB_DataAPStatic_t*  p_data_mgr  I
//     It points to the data pointer array struct.
//
// Return:
//   USB_Data_t**
//     The tail pointer of the data pointer array struct will be returned.
//%FE=======================================================================
USB_Data_t** USB_DATA_PA_Cur(USB_DataAPStatic_t*  p_data_mgr) {
  return ( p_data_mgr->ppa_data_head + p_data_mgr->count );
}


//%FS=======================================================================
// USB_DATA_PA_GetDataAP
//
// Description:
//  This function gets the data pointer array struct.
//
// Parameters: 
//   void
//
// Return:
//   USB_DataAPStatic_t*
//     The data pointer array struct will be returned.
//%FE=======================================================================
USB_DataAPStatic_t* USB_DATA_PA_GetDataAP(){
  return( & ((USB_HostCtrlTop_t*)XPORT_GetMyProjectData())->sys_data_mgr );
}


