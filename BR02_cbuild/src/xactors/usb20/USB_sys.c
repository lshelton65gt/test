//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file includes all functions relating to HostCtrlTop and speed operation,
 *
 *                 USB_* are API functions that user can use for IRP related
 *                 operations, they include:
 *
 *
 ***************************************************************************/
 
#include "USB_support.h"


//%FS=========================================================
// carbonXUsbHostCtrlTopInit
//
// Description:
//   This function initializes the top member. It is used before the test.
//   1. Get a memory for the top structure.
//   2. Set the project data
//   3. Initialize the all members under the structure
//
// Parameter: 
//   void
//
// Return: 
//   void
//%FE=========================================================
void carbonXUsbHostCtrlTopInit (){
  
  USB_HostCtrlTop_t*  p_top;
  
  //------------------ step 1 ---------------------
  p_top = (USB_HostCtrlTop_t*)tbp_calloc(1, sizeof(USB_HostCtrlTop_t));

  if(p_top == NULL){
    MSG_Panic("carbonXUsbHostCtrlTopInit(): OUT of memory!\n");
  }

  //------------------ step 2 ---------------------
  XPORT_SetMyProjectData( (void *) p_top);
  
  //------------------ step 3 ---------------------
  p_top->ppa_sys_sar_data = NULL;
  
  USB_DEV_DevProfReset(p_top->a_dev_prof);
  USB_SCH_Clear(&p_top->sys_sched);
  USB_DATA_PA_Init(&p_top->sys_data_mgr);
 
  USB_PIPE_PA_Init(&p_top->sys_pipe_mgr); 
  USB_IRP_PA_Init(&p_top->sys_irp_mgr);
  p_top->testcase_num = 0;
  p_top->bfm_clock_cycle = 0;
  p_top->ErrorMsgDisabled.transaction_response_timeout = 0;
  p_top->ErrorMsgDisabled.transaction_halted = 0;
  p_top->ErrorMsgDisabled.transaction_aborted = 0;
  p_top->ErrorMsgDisabled.packet_max_size_exceeded = 0;
  p_top->ErrorMsgDisabled.transaction_response_crc = 0;
  p_top->ErrorMsgDisabled.transaction_response_pid_err = 0;
  p_top->ErrorMsgDisabled.toggle_err = 0;
  
  p_top->cp_sys_pe_buffer = carbonXUsbDataRealloc(NULL, USB_MAX_PE_BUFFER_SIZE);
  
  return;
}


//%FS=======================================================================
// carbonXUsbHostCtrlTopFree
//
// Description:
//   This function frees the system memory by malloc() or realloc().
//
// Parameters: 
//   void
//
// Return: 
//   void
//%FE=======================================================================
void carbonXUsbHostCtrlTopFree (){
  USB_HostCtrlTop_t*    p_top;
  
  p_top = (USB_HostCtrlTop_t*) XPORT_GetMyProjectData();
  
  if ( p_top != NULL ){
  	
    if(p_top->ppa_sys_sar_data != NULL){
      tbp_free(p_top->ppa_sys_sar_data);
    }
    USB_DATA_PA_Free(&p_top->sys_data_mgr);
    USB_PIPE_PA_Free(&p_top->sys_pipe_mgr);
    USB_IRP_PA_Free(&p_top->sys_irp_mgr);

    USB_SCH_Free(&p_top->sys_sched);
    tbp_free(p_top);
  }
}

//%FS===========================================================================
// carbonXUsbSetMsgLevel
//
// Description:
//   carbonXUsbSetMsgLevel sets log debug level and screen debug level.
//
// Parameters: 
//   int  log_dbg_level  I
//     It is the log debug level.
//   int  scr_dbg_level  I 
//     It is the screen debug level.
//
// Return: 
//   void
//%FE===========================================================================
void carbonXUsbSetMsgLevel(int log_dbg_level, int scr_dbg_level) {

  PLAT_SetLogDebugLevel(log_dbg_level);
  PLAT_SetScreenDebugLevel(scr_dbg_level);  
}

//%FS===========================================================================
// carbonXUsbSetDevSpeed
//
// Description:
//   This function sets the USB device speed.

//
// Parameters: 
//   USB_TransSpeed_e speed  I
//     It points to the speed being set.
//
// Return: 
//   void
//%FE===========================================================================
void carbonXUsbSetDevSpeed(USB_TransSpeed_e speed){

  carbonXUsbReset(speed);
  
}

//%FS===========================================================================
// carbonXUsbSetStartFrame
//
// Description:
//   This function sets the start frame for the specific pipe.
//
// Parameters: 
//   USB_PipeDescr_t* p_pipe  I
//     It points to the specific pipe.
//   u_int32 uframe  I
//     It points to the start frame number.
//
// Return: 
//   void
//%FE===========================================================================
void carbonXUsbSetStartFrame (USB_PipeDescr_t* p_pipe, u_int32 uframe){
  u_int32    old_frame;

  if(p_pipe != NULL){
    old_frame = p_pipe->sys_record.uframe_sched;
    p_pipe->sys_record.uframe_sched = uframe;

    if( !USB_CHK_BandWidth(p_pipe, p_pipe->p_endpt_descr->max_packet_size) ){
      MSG_Printf("carbonXUsbSetStartFrame(): Set start frame %d failed.\n", uframe);
      p_pipe->sys_record.uframe_sched = old_frame;
    }
  }
  else MSG_Panic("carbonXUsbSetStartFrame(): Input pointer cannot be NULL.\n");
}


//%FS===========================================================================
// carbonXUsbGetFrameCnt
//
// Description:
//   This function gets the current (micro)frame count.
//
// Parameters: 
//   void
//
// Return: 
//   The return value is the current frame number.
//
//%FE===========================================================================
u_int32 carbonXUsbGetFrameCnt () {

  USB_HostCtrlTop_t*  p_top;

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  return( p_top->sys_sched.uframe_count );
}

//%FS===========================================================================
// carbonXUsbResetFrameCnt
//
// Description:
//   This function resets the (micro)frame count to 0.
//
// Parameters: 
//   void
//
// Return: 
//   void
//
//%FE===========================================================================
void carbonXUsbResetFrameCnt () {

  USB_HostCtrlTop_t*  p_top;

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  p_top->sys_sched.uframe_count = 0;
}

//%FS===========================================================================
// carbonXUsbSetFrameCnt
//
// Description:
//   This function sets the current (micro)frame count.
//
// Parameters: 
//   u_int32  frame_cnt  I
//     It is the frame number that sets for the system.
//
// Return: 
//   void.
//
//%FE===========================================================================
void carbonXUsbSetFrameCnt (u_int32 frame_cnt) {

  USB_HostCtrlTop_t*  p_top;

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  p_top->sys_sched.uframe_count = frame_cnt;
}

//%FS===========================================================================
//
// carbonXUsbSofSetError
//
// Description:
//   This function sets the error to a specific SOF.
//
// Parameters: 
//   u_int32 frame_cnt_begin  I	
//     The beginning frame count that error should be injected.
//   u_int32 frame_cnt_end  I	
//     The end frame count that error should be injected.
//   u_int32 sof_err_mask  I	
//     It indicates the injected error types.
//
// Return:
//   void
//
//%FE===========================================================================
void carbonXUsbSofSetError (u_int32 frame_cnt_begin, u_int32 frame_cnt_end,
  u_int32 sof_err_mask) {

  USB_HostCtrlTop_t*  p_top;
  
  p_top = (USB_HostCtrlTop_t* )XPORT_GetMyProjectData();
  
  p_top->sys_sched.do_sof_err 		= TRUE;
  p_top->sys_sched.frame_cnt_begin 	= frame_cnt_begin;
  p_top->sys_sched.frame_cnt_end 	= frame_cnt_end;
  p_top->sys_sched.sof_err_mask         = sof_err_mask;
}

void USB_CvtInt32ToByteAry( u_int32 a, u_int8 b[4] ) {
  int i;

  for ( i=3; i>=0; i-- ) {
    b[i] = (u_int8)(a & 0xFF);
    a = a >> 8;
  }
}


//%FS===========================================================================
//
// carbonXUsbIsDataPid
//
// Description:
//   Determine, whether PID is one of DATA0, DATA1, DATA2, MDATA
//
// Parameters: 
//   u_int32  pid  I   PID
//
// Return:
//   BOOL  TRUE, if PID is DATA0, DATA1, DATA2, MDATA
//         FALSE otherwise
//
//%FE===========================================================================

BOOL carbonXUsbIsDataPid ( u_int8 pid ) {
  return ( ( pid & 0x3) == 0x3 ) ? TRUE : FALSE;
  
}

//%FS===========================================================================
//
// carbonXUsbRegisterDeviceAttachCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t* p_top    I  pointer to HostCtrlTop stucture
//   pfDeviceAttach_t  p_fct     I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterDeviceAttachCB ( USB_HostCtrlTop_t* p_top, pfDeviceAttach_t p_fct ) {
  p_top->pfDeviceAttach = p_fct;

  return;
}


//%FS===========================================================================
//
// carbonXUsbRegisterDevSpeedNegCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t* p_top    I  pointer to HostCtrlTop stucture
//   pfDevSpeedNeg_t  p_fct      I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterDevSpeedNegCB ( USB_HostCtrlTop_t* p_top, pfDevSpeedNeg_t p_fct ) {
  p_top->pfDevSpeedNeg = p_fct;

  return;
}


//%FS===========================================================================
//
// carbonXUsbRegisterBeforeSchLoopCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t* p_top    I  pointer to HostCtrlTop stucture
//   pfBeforeSchLoop_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterBeforeSchLoopCB ( USB_HostCtrlTop_t* p_top, pfBeforeSchLoop_t p_fct ) {
  p_top->pfBeforeSchLoop = p_fct;

  return;
}


//%FS===========================================================================
//
// USB_
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t* p_top    I  pointer to HostCtrlTop stucture
//   pfPipePushInIRP_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================
void carbonXUsbRegisterPipePushInIRPCB ( USB_HostCtrlTop_t* p_top, pfPipePushInIRP_t p_fct ) {
  p_top->pfPipePushInIRP = p_fct;

  return;
}

//%FS===========================================================================
//
// USB_
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t* p_top    I  pointer to HostCtrlTop stucture
//   pfNewIrpCB_t  p_fct         I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================
void carbonXUsbRegisterNewIrpCB ( USB_HostCtrlTop_t* p_top, pfNewIrp_t p_fct ) {
  p_top->pfNewIrp = p_fct;

  return;
}


//%FS===========================================================================
//
// carbonXUsbRegisterBeginOfExecIrpCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*            p_top  I  pointer to HostCtrlTop stucture
//   pfRegisterBeginOfExecIrpCB_t  p_fct  I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterBeginOfExecIrpCB ( USB_HostCtrlTop_t* p_top, pfBeginOfExecIrp_t p_fct ) {
  p_top->pfBeginOfExecIrp = p_fct;

  return;
}


//%FS===========================================================================
//
// carbonXUsbRegisterEndOfExecIrpCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*          p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterEndOfExecIrpCB_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterEndOfExecIrpCB( USB_HostCtrlTop_t* p_top, pfEndOfExecIrp_t p_fct ) {
  p_top->pfEndOfExecIrp = p_fct;

  return;
}



//%FS===========================================================================
//
// carbonXUsbRegisterBeginOfTransactionCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*                p_top   I  pointer to HostCtrlTop stucture
//   pfRegisterBeginOfTransactionCB_t  p_fct   I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterBeginOfTransactionCB( USB_HostCtrlTop_t* p_top, pfBeginOfTransaction_t p_fct ) {
  p_top->pfBeginOfTransaction = p_fct;

  return;
}



//%FS===========================================================================
//
// carbonXUsbRegisterEndOfTransactionCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*              p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterEndOfTransactionCB_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterEndOfTransactionCB( USB_HostCtrlTop_t* p_top, pfEndOfTransaction_t p_fct ) {
  p_top->pfEndOfTransaction = p_fct;

  return;
}



//%FS===========================================================================
//
// carbonXUsbRegisterBeforeSofCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*       p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterBeforeSofCB_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterBeforeSofCB ( USB_HostCtrlTop_t* p_top, pfBeforeSof_t p_fct ) {
  p_top->pfBeforeSof = p_fct;

  return;
}



//%FS===========================================================================
//
// carbonXUsbRegisterBeginOfExecPeriodListCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*                   p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterBeginOfExecPeriodListCB_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterBeginOfExecPeriodListCB ( USB_HostCtrlTop_t* p_top, pfBeginOfExecPeriodList_t p_fct ) {
  p_top->pfBeginOfExecPeriodList = p_fct;

  return;
}



//%FS===========================================================================
//
// carbonXUsbRegisterBeginOfExecCtrlListCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*                 p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterBeginOfExecCtrlListCB_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterBeginOfExecCtrlListCB ( USB_HostCtrlTop_t* p_top, pfBeginOfExecCtrlList_t pfBeginOfExecCtrlList ) {
  p_top->pfBeginOfExecCtrlList = pfBeginOfExecCtrlList;

  return;
}



//%FS===========================================================================
//
// carbonXUsbRegisterBeginOfExecBulkListCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*                 p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterBeginOfExecBulkListCB_t  p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterBeginOfExecBulkListCB ( USB_HostCtrlTop_t* p_top, pfBeginOfExecBulkList_t pfBeginOfExecBulkList ) {
  p_top->pfBeginOfExecBulkList = pfBeginOfExecBulkList;

  return;
}


//%FS===========================================================================
//
// carbonXUsbRegisterSetDevProfCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*                 p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterSetDevProfCB_t           p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterSetDevProfCB ( USB_HostCtrlTop_t* p_top, pfSetDevProf_t pfSetDevProf ) {
  p_top->pfSetDevProf = pfSetDevProf;

  return;
}

//%FS===========================================================================
//
// carbonXUsbRegisterSetEpDescrCB
//
// Description:
//   This function will register a callback function
//
// Parameters: 
//   USB_HostCtrlTop_t*                 p_top    I  pointer to HostCtrlTop stucture
//   pfRegisterSetEpDescrCB_t           p_fct    I  call back function pointer 	
//
// Return:
//   void
//
//%FE===========================================================================

void carbonXUsbRegisterSetEpDescrCB ( USB_HostCtrlTop_t* p_top, pfSetEpDescr_t pfSetEpDescr ) {
  p_top->pfSetEpDescr = pfSetEpDescr;

  return;
}




char* USB_DirTypeAry[] = { "Undefined", "OUT", "IN", "INOUT" };

//%FS===========================================================================
//
// carbonXUsbCvtDirTypeToString
//
// Description:
//   This function converts a direction type to an ASCII string
//
// Parameters: 
//   USB_Direction_e direction_type I    Direction
//
// Return:
//   A pointer to the text string that represents the direction
//
//%FE===========================================================================

char* carbonXUsbCvtDirTypeToString ( USB_Direction_e direction_type ) {
  return USB_DirTypeAry[ (u_int8)direction_type  ];
}


char* USB_EndPtTypeAry[] = { "Undefined", "Ctrl", "Iso", "Bulk", "Intr" };

//%FS===========================================================================
//
// carbonXUsbCvtEndPtTypeToString
//
// Description:
//   This function converts a USB Endpoint Type to an ASCII string
//
// Parameters: 
//   USB_EpType_e    I    endpoint type
//
// Return:
//   A pointer to the text string that represents the Endpoint Type
//
//%FE===========================================================================

char* carbonXUsbCvtEndPtTypeToString ( USB_EpType_e end_pt_type ){
  return USB_EndPtTypeAry[ (u_int8)end_pt_type ];
}


char* USB_HcTransStateAry[] = { "DO_COMP_IMMED_NOW", "DO_COMPLETE", "DO_COMPLETE_IMMED",
				"DO_HALT", "DO_NEXT_CMD", "DO_NEXT_COMPLETE", "DO_NEXT_PING",
				"DO_OUT", "DO_PING", "DO_SAME_CMD", "DO_START"
                            };

//%FS===========================================================================
//
// carbonXUsbCvtHcTransStateToString
//
// Description:
//   This function converts a PID to an ASCII string
//
// Parameters: 
//   u_int8   HcTransState    I   Transaction State
//
// Return:
//   A pointer to the text string that represents the Transaction State
//
//%FE===========================================================================

char* carbonXUsbCvtHcTransStateToString ( u_int8 HcTransState  ) {
  return USB_HcTransStateAry[ HcTransState  ];
}


char* USB_PidStringAry[] = { "RESERVED - TIMEOUT?", "OUT", "ACK", "DATA0", "PING", "SOF", "NYET",
                              "DATA2", "SPLIT", "IN", "NAK", "DATA1", "ERR_PRE", "SETUP",
                              "STALL", "MDATA"
                            };

//%FS===========================================================================
//
// carbonXUsbCvtPidToString
//
// Description:
//   This function converts a PID to an ASCII string
//
// Parameters: 
//   u_int8   pid    I   USB PID
//
// Return:
//   A pointer to the text string that represents the PID
//
//%FE===========================================================================

char* carbonXUsbCvtPidToString( u_int8   pid ) {
  return USB_PidStringAry[ pid &0xF  ];
}




char* USB_SpeedAry[] = { "Undefined", "Low", "Full", "High" };

//%FS===========================================================================
//
// carbonXUsbCvtSpeedToString
//
// Description:
//   This function converts a USB bus speed to an ASCII string
//
// Parameters: 
//  USB_TransSpeed_e   I    bus_speed 
//
// Return:
//   A pointer to the text string that represents the Transaction State
//
//%FE===========================================================================

char* carbonXUsbCvtSpeedToString ( USB_TransSpeed_e bus_speed  ){
  return USB_SpeedAry[ (u_int8)bus_speed ];
}



//%FS==================================================================
//
// carbonXUsbGetEpBufDescr
//
// Description:
//   Return the buffer descriptor for the specified endpoint and direction
//
// Parameter:
//   u_int32 endpt_num  I
//     Endpoint number
//
//  USB_Direction_e direction I
//     Direction (one of USB_DIR_OUT or USB_DIR_IN )
//   
// Return value:
//   Pointer to buffer descriptor data struct
//
//%FE==================================================================

USB_BufDescr_t* carbonXUsbGetEpBufDescr ( u_int32 endpt_num, USB_Direction_e direction ) {
  USB_HostCtrlTop_t    *p_top;
  USB_BufDescr_t       *p_buf_descr = NULL;
  

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  if ( direction == USB_DIR_OUT ) {
    p_buf_descr = &(p_top->EpBufDescrAry[endpt_num].buf_descr[USB_BUF_DESCR_OUT]);
  }
  else if ( direction == USB_DIR_IN ) {
    p_buf_descr = &(p_top->EpBufDescrAry[endpt_num].buf_descr[USB_BUF_DESCR_IN]);
  }
  else {
    MSG_Panic( "carbonXUsbGetEpBufDescr direction needs to be IN or OUT, but is %s\n",
	       carbonXUsbCvtDirTypeToString(direction) );
  }

  return  p_buf_descr; 
}

//%FS==================================================================
//
// carbonXUsbSetEpBufDescr
//
// Description:
//   Set the buffer descriptor for the specified endpoint and direction
//
// Parameter:
//   u_int32 endpt_num  I
//     Endpoint number
//
//  USB_Direction_e direction I
//     Direction (one of USB_DIR_OUT or USB_DIR_IN )
//
//  USB_BufDescr_t *p_buf_descr I
//     Buffer descriptor
//   
// Return value:
//   void
//
//%FE==================================================================

void carbonXUsbSetEpBufDescr ( u_int32 endpt_num, USB_Direction_e direction, USB_BufDescr_t *p_buf_descr ) {
  USB_BufDescr_t   *p_descr;

  // Get the Endpoint Buffer Descriptor for the specified Endpoint
  p_descr = carbonXUsbGetEpBufDescr( endpt_num, direction );

  // Set all the fields of p_descr
  p_descr->start    = p_buf_descr->start;
  p_descr->size     = p_buf_descr->size;
  p_descr->curr_ptr = p_buf_descr->curr_ptr;

  return;
}

/*
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.3  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.2  2005/11/21 23:15:21  jstein
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: checkin / runlist -grid -xactors / xactor examples regress all
 *               : Bugs Affected: Added xactor examples for spi3, utopia2
 *               :                Updated enet_*, ahb, pci to use new carbonX calls.
 *               :                Updated usb20 (with Goran's assistance) to fix multiple problems.
 *               :                Updated runlist.bom to get runlist -grid -xactors to work on SGE
 *               :                Changed Makefile.carbon.vsp xactor command to look in dir "."
 *               :                Added log files to all test.run tests
 *               :                changed malloc to carbonmem_malloc as much as possible.
 *               :                changed Makefile.libs to search CARBON_LIB_LIST during xactor compiles.
 *               :                Updated documentation, added to html dirs.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.32  2005/07/11 19:54:04  hoglund_w
 *               : Updates for carbon, removing unused code and debug messages
 *               :
 *               : Revision 1.31  2004/10/21 16:21:33  zippelius
 *               : Moved some functions. Renamed some functions
 *               :
 *               : Revision 1.30  2004/09/26 18:07:29  zippelius
 *               : Added USB_ prefix to a number of defines. Other smaller changes
 *               :
 *               : Revision 1.29  2004/09/24 22:51:05  zippelius
 *               : Changed how EP buffers are being managed
 *               :
 *               : Revision 1.28  2004/09/22 17:35:57  zippelius
 *               : the same test1 can now run with either OpenCore or ARC core
 *               :
 *               : Revision 1.27  2004/08/30 20:21:33  zippelius
 *               : changed all wishbone reference to amba references
 *               :
 *               : Revision 1.26  2004/08/24 15:12:04  zippelius
 *               : Fixed bugs when determining completion of IRP. Misc clean-ups
 *               :
 *               : Revision 1.25  2004/08/23 20:15:57  zippelius
 *               : Fixed Control transfers and misc other clean-ups
 *               :
 *               : Revision 1.24  2004/08/18 20:29:38  zippelius
 *               : Cleaned-up tests. Moved some code into test_support
 *               :
 *               : Revision 1.23  2004/08/13 21:43:48  zippelius
 *               : snapshot check-in
 *               :
 *               : Revision 1.22  2004/08/13 18:40:35  zippelius
 *               : Removed bogus Pipe 0; Moved toggle tracking from Pipe into EP; Fixed 4 byte alloc, when data size==0;Misc fixes
 *               :
 *               : Revision 1.21  2004/08/12 14:11:19  zippelius
 *               : Numerous fixes and changes around DATA toggle
 *               :
 *               : Revision 1.20  2004/08/11 01:59:20  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.19  2004/08/06 19:21:07  zippelius
 *               : Updated test header with description
 *               :
 *               : Revision 1.18  2004/08/05 02:10:24  fredieu
 *               : Add other errors
 *               :
 *               : Revision 1.17  2004/08/02 18:53:25  zippelius
 *               : Fixed a number of bugs regarding bus time estimation
 *               :
 *               : Revision 1.16  2004/07/30 21:57:13  fredieu
 *               : clean up for error injection
 *               :
 *               : Revision 1.15  2004/07/24 03:40:22  fredieu
 *               : Add some controls for pings and using all the micro frame.
 *               :
 *               : Revision 1.14  2004/07/19 19:22:07  fredieu
 *               : Allow for the max packet size to be exceeded with no errors.
 *               :
 *               : Revision 1.13  2004/07/16 21:41:10  fredieu
 *               : Init some integers.
 *               :
 *               : Revision 1.12  2004/07/09 15:36:41  fredieu
 *               : Change some warnings to error or panics.
 *               :
 *               : Revision 1.11  2004/06/18 22:23:30  fredieu
 *               : Add in a SEED control routine.
 *               :
 *               : Revision 1.10  2004/04/26 21:54:26  jafri
 *               : modified english in verilog and c code and added usb_test1a to test table
 *               :
 *               : Revision 1.9  2004/04/26 21:34:29  zippelius
 *               : Code clean-up regarding IRP state and misc other
 *               :
 *               : Revision 1.8  2004/04/23 17:42:39  zippelius
 *               : More code clean-up
 *               :
 *               : Revision 1.7  2004/04/22 13:44:28  zippelius
 *               : Clean-up defines, messaging, others
 *               :
 *               : Revision 1.6  2004/04/20 20:54:10  zippelius
 *               : Losts of code clean-up, comment fixes
 *               :
 *               : Revision 1.5  2004/04/19 18:38:57  jafri
 *               : fixed english in comments
 *               :
 *               : Revision 1.4  2004/04/16 20:35:15  zippelius
 *               : Some code clean up. Added defines
 *               :
 *               : Revision 1.3  2004/04/15 14:50:53  zippelius
 *               : Major code clean-ups. Upgraded USB IP core to latest version. Fixed call-backs.Added Error masking. Fixed LineState state machines.
 *               :
 *               : Revision 1.2  2004/03/26 14:59:23  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.6  2003/12/05 08:49:25  adam
 *               : Debug testcases
 *               :
 *               : Revision 2.5  2003/12/03 02:14:02  adam
 *               : Edit testcase
 *               :
 *               : Revision 2.4  2003/11/25 09:41:33  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.3  2003/09/27 14:43:12  serlina
 *               : printf change
 *               :
 *               : Revision 2.2  2003/09/27 14:22:42  serlina
 *               : data check print
 *               :
 *               : Revision 2.1  2003/09/27 08:18:51  serlina
 *               :  reset function
 *               :
 *               : Revision 2.0  2003/09/27 06:33:46  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:19  adam
 *               : updated file directory
 *               :
 *               : Revision 1.20  2003/09/27 04:34:48  adam
 *               : Debug
 *               :
 *               : Revision 1.19  2003/09/27 03:24:12  serlina
 *               : set speed
 *               :
 *               : Revision 1.18  2003/09/26 12:52:48  adam
 *               : Add testcases
 *               :
 *               : Revision 1.17  2003/09/26 11:02:18  adam
 *               : Add testcases
 *               :
 *               : Revision 1.16  2003/09/25 06:24:27  serlina
 *               : change safe for while
 *               :
 *               : Revision 1.15  2003/09/24 09:53:19  adam
 *               : Add carbonXUsbSetMsgLevel()
 *               :
 *               : Revision 1.14  2003/09/23 09:19:12  adam
 *               : Add amba interface
 *               :
 *               : Revision 1.13  2003/09/19 07:20:21  serlina
 *               :  add common
 *               :
 *               : Revision 1.12  2003/09/18 06:27:47  serlina
 *               : add the uframe_sched parameter
 *               :
 *               : Revision 1.11  2003/09/12 13:58:37  serlina
 *               : updated USB_CHK_CmpIrpData()
 *               :
 *               : Revision 1.10  2003/09/11 08:08:19  issac
 *               : added USB_DataChecK() function
 *               :
 *               : Revision 1.9  2003/09/08 14:17:24  serlina
 *               : change function name
 *               :
 *               : Revision 1.8  2003/09/08 13:59:16  serlina
 *               : common
 *               :
 *               : Revision 1.7  2003/09/08 07:59:34  serlina
 *               : change link
 *               :
 */
