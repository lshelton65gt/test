//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef USB_DATA_HL_H
#define USB_DATA_HL_H

USB_Data_t* carbonXUsbDataRealloc(USB_Data_t* p_data, u_int32 data_size);

#endif
