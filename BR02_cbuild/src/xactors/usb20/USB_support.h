//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef USB_SUPPORT_H
#define USB_SUPPORT_H


#ifdef __cplusplus
extern "C" {
#endif

#include <assert.h>
#include <math.h>
#include "tbp.h"
#include "tbp_malloc.h"
#include "carbonXUsbStdDescriptor.h"
#include "carbonXUsbCommon.h"
#include "carbonXUsbSys.h"
#include "USB_devSupport.h"
#include "carbonXUsbDev.h"
#include "carbonXUsbDatahl.h"
#include "USB_datahlSupport.h"
#include "USB_irpSupport.h"
#include "carbonXUsbIrp.h"
#include "USB_sarSupport.h"
#include "carbonXUsbSar.h"
#include "USB_check.h"
#include "USB_pipeSupport.h"
#include "carbonXUsbPipe.h"
#include "USB_std_reqSupport.h"
#include "carbonXUsbStdReq.h"
#include "USB_pe_msg.h"
#include "USB_protocol_eng.h"
#include "USB_scheduleSupport.h"
#include "carbonXUsbSchedule.h"
#include "carbonXUsbCallback.h"
#include "prep_stuff.h"

#ifdef DM
#include <dm.h>
#endif

#ifndef MIN
#define MIN(x, y)   ((x) < (y) ? (x) : (y))
#endif

#undef PLAT_GetSimTime
#undef PLAT_GetSimtime
#define PLAT_GetSimTime() ((u_int64)((carbonXCsRead(0x10c)+31)*16.66667))
#define PLAT_GetSimtime() ((u_int64)((carbonXCsRead(0x10c)+31)*16.66667))

#ifdef __cplusplus
}
#endif


#endif
