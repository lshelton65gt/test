//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef USB_SAR_SUPPORT_H
#define USB_SAR_SUPPORT_H

void USB_SAR_SegmentData( 
  u_int16          segment_size,
  USB_Data_t*      p_segment,
  USB_Data_t*      p_sdata,
  USB_PipeDescr_t* p_pipe 
);

void USB_SAR_ReallocAry();

USB_Data_t* USB_SAR_GetMemory(u_int32 pipe_id,u_int32 sar_size);

void USB_SAR_SegmentIrp (USB_Data_t* p_segment, USB_PipeDescr_t* p_pipe, u_int16  segment_size);

void USB_SAR_AddPid ( USB_Data_t*  p_segment, USB_PipeDescr_t* p_pipe );

void USB_SAR_CheckPid( u_int8 pid, USB_PipeDescr_t*  p_pipe);

BOOL USB_SAR_NeedsSar(USB_PipeDescr_t* p_pipe);

void USB_SAR_Reassemble(USB_Data_t* p_rcv_data,USB_PipeDescr_t*  p_pipe);

void USB_SAR_ReassembleData(USB_Data_t* p_rcv_data,USB_PipeDescr_t* p_pipe);

void USB_SAR_HandleLastIrpTrans(USB_PipeDescr_t* p_pipe,u_int32 size);

void USB_SAR_Pid ( USB_PipeDescr_t* p_pipe, u_int32 size );

void USB_SAR_SetCtrlPid(USB_PipeDescr_t* p_pipe, u_int32 size);

void USB_SAR_DataToggle(USB_PipeDescr_t*  p_pipe);

void USB_SAR_SetBulkAndIntrPid(USB_PipeDescr_t*  p_pipe);

void USB_SAR_SetIsoPid(USB_PipeDescr_t* p_pipe);

void USB_SAR_HsIsoOutSplit(USB_PipeDescr_t* p_pipe, u_int16 segment_size);

void USB_SAR_DataPart(USB_Irp_t*    p_irp, u_int16 segment_size);

BOOL USB_SAR_IsSmall(
  USB_Irp_t*        p_irp,
  USB_Data_t*       p_sdata,
  USB_PipeDescr_t*  p_pipe
);

#endif

/* * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.6  2004/08/23 20:15:57  zippelius
 *               : Fixed Control transfers and misc other clean-ups
 *               :
 *               : Revision 1.5  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.4  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.3  2004/04/20 20:54:10  zippelius
 *               : Losts of code clean-up, comment fixes
 *               :
 *               : Revision 1.2  2004/03/26 15:00:18  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.0  2003/09/27 06:34:13  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:42  adam
 *               : updated file directory
 *               :
 *               : Revision 1.8  2003/09/26 11:53:32  serlina
 *               : add parameter
 *               :
 *               : Revision 1.7  2003/09/11 08:09:18  issac
 *               : major update after code review
 *               :
 *               : Revision 1.6  2003/09/08 07:56:05  serlina
 *               : change link
 *               :
 *               : Revision 1.5  2003/09/08 02:30:58  adam
 *               : updated data structre
 *               :
 *               : Revision 1.4  2003/08/27 15:03:12  serlina
 *               : updated Do Enum series of functions, and dev prof data structure
 *               :
 */
