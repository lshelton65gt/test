//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 * File Name     : $Source$
 *
 * Comments      : This file is for Scheduler. 
 *                 It includes all functions that Scheduler uses.
 *
 *
 ***************************************************************************/
 
#ifndef USB_SCHEDULE_SUPPORT_H
#define USB_SCHEDULE_SUPPORT_H

#define USB_ERR_TIMEOUT			0x1
#define USB_ERR_CRC  			0x2
#define USB_ERR_DATA_CORRUPT		0x4
#define USB_ERR_DATA_DROP		0x8
#define USB_ERR_PID			0x10
#define USB_ERR_PART_ALL     		0x20
#define USB_ERR_PART_BEGIN   		0x40
#define USB_ERR_PART_MID     		0x80
#define USB_ERR_PART_END     		0x100
#define USB_ERR_PHASE_SPLIT  		0x200
#define USB_ERR_PHASE_TOKEN  		0x400
#define USB_ERR_PHASE_DATA   		0x800
#define USB_ERR_RECEIVE_TIMEOUT 0x1000
#define USB_ERR_RECEIVE_CRC 0x2000
#define USB_ERR_RECEIVE_PID 0x4000

#define USB_SOF_ERR_BADCRC  		0x1
#define USB_SOF_ERR_DATA_CORRUPT	0x2
#define USB_SOF_ERR_DATA_DROP		0x4

#define USB_FRAME_TIME          	1000000
#define USB_MICROFRAME_TIME     	125000
#define USB_SCH_LIST_GROW_SIZE  	16
#define USB_SPLIT_TOKEN_LEN     	4
#define USB_TOKEN_LEN     		3

#define USB_DUMMY_TIMEOUT_MAX    	0x7FFFFFFF

// Functions in this file can be classified into [principle functions, and 
// supporting functions.].

//////////////////////////////////////////////////////////////////////
//[principle functions]
//

// Function to perform inter packet gap in terms of (micro)frame at the
// beginning of each scheduling loop.
void USB_SCH_IpgUframe( u_int32 ipg, USB_DevProfile_t* p_dev );

// Function to implement SOF transactions.
void USB_SCH_IssueSOF(USB_Scheduler_t* p_sched, USB_DevProfile_t* p_dev);

// Function to implement all periodic transactions scheduled to be transferred
// in the current (micro)frame.
void USB_SCH_DoPeriodicPipes();
  
// Function to implement all control transactions scheduled to be transferred
// in the current (micro)frame.
void USB_SCH_DoCtrlPipes();

// Function to implement all bulk transactions scheduled to be transferred in
// the current (micro)frame.
void USB_SCH_DoBulkPipes();

// Function to use up the left time of the current (micro)frame.
void USB_SCH_DoIdle(USB_Scheduler_t* p_sched);

// Function to execute a transfer
void USB_SCH_ExecTrans( USB_Trans_t* p_trans );

// Function to build USB_Trans_t, prepare for calling USB_DoTransfer.
void USB_SCH_UpdateTrans( USB_Trans_t* p_trans, USB_PipeDescr_t* p_pipe );

// Function to process the transaction result returned from USB_DoTransfer,
// and decides to retry or do next transaction.  
void USB_SCH_DoHcTransState( USB_Trans_t* p_trans );

// Function to report the Scheduler information.  
void USB_SCH_DoReport( USB_Trans_t* p_trans );
  
//////////////////////////////////////////////////////////////////////
// [supporting functions]
//

// Function to set up periodic list and asynchronous list.
void USB_SCH_SetList(USB_HostCtrlTop_t* p_top, USB_Scheduler_t* p_sched);

// Function that checks, if there is IRP pending or not.
BOOL USB_SCH_IsIrpPending(USB_HostCtrlTop_t* p_top);

// Function to change bit time to BFM cycle.
u_int32 USB_SCH_EstimateDuration(BOOL is_token, u_int32 bytes,
  USB_PipeDescr_t* p_pipe, USB_Scheduler_t* p_sched);

// Function to build USB_Trans_t for split transactions, prepares for calling
// USB_DoTransfer.
void USB_SCH_BuildSplitTransT( USB_Trans_t* p_trans, USB_Data_t* p_sar );

// Function to set the PID of the token packet of the current transaction.
USB_Pid_e USB_SCH_SetTransPid( USB_Trans_t* p_trans );

// Function to get err_mask information from USB_Irp_t and sets USB_ErrInjection_t.
void USB_SCH_SetErrInjection(USB_Trans_t* p_trans);

// Function to retry a transaction failed because there is timeout.
void USB_SCH_RetryForTimeout( USB_Trans_t*  p_trans );

// Function to retry a complete-split transaction before doing a different
// transaction.
void USB_SCH_RetryCsImmed( USB_Trans_t* p_trans );

// Function to print the transaction state in character format.
void USB_SCH_PrintTransState(USB_Trans_t* p_trans);

// Function to reset members of USB_Scheduler_t to be the default values.
void USB_SCH_Clear(USB_Scheduler_t*  p_sched);

// Function to free the memory for Scheduler using.
void USB_SCH_Free(USB_Scheduler_t*  p_sched);

// Function convert bit time into wall clock time
u_int32 USB_SCH_CvtBitTimeToDuration ( USB_TransSpeed_e  bus_speed, u_int32 bit_time );

#endif

/*
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.4  2006/02/24 19:08:39  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: nightly checkin
 *               : Bugs Affected: 5501
 *               :
 *               : Fixed many warnings outputted by ICC version 9 compiler.
 *               : Fixed a test failure in AHB.
 *               : Fixed a problem in Ethernet MII and GMII in which 64 byte packets were
 *               : not being received properly.
 *               :
 *               : Revision 1.3  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.2  2005/11/21 23:15:21  jstein
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: checkin / runlist -grid -xactors / xactor examples regress all
 *               : Bugs Affected: Added xactor examples for spi3, utopia2
 *               :                Updated enet_*, ahb, pci to use new carbonX calls.
 *               :                Updated usb20 (with Goran's assistance) to fix multiple problems.
 *               :                Updated runlist.bom to get runlist -grid -xactors to work on SGE
 *               :                Changed Makefile.carbon.vsp xactor command to look in dir "."
 *               :                Added log files to all test.run tests
 *               :                changed malloc to carbonmem_malloc as much as possible.
 *               :                changed Makefile.libs to search CARBON_LIB_LIST during xactor compiles.
 *               :                Updated documentation, added to html dirs.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.10  2005/07/11 19:54:02  hoglund_w
 *               : Updates for carbon, removing unused code and debug messages
 *               :
 *               : Revision 1.9  2004/10/21 16:21:32  zippelius
 *               : Moved some functions. Renamed some functions
 *               :
 *               : Revision 1.8  2004/08/23 20:15:57  zippelius
 *               : Fixed Control transfers and misc other clean-ups
 *               :
 *               : Revision 1.7  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.6  2004/08/05 02:15:59  fredieu
 *               : Add other errors
 *               :
 *               : Revision 1.5  2004/08/02 18:54:17  zippelius
 *               : Fixed a number of bugs regarding bus time estimation
 *               :
 *               : Revision 1.4  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.3  2004/04/20 20:54:10  zippelius
 *               : Losts of code clean-up, comment fixes
 *               :
 *               : Revision 1.2  2004/03/26 15:00:18  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.3  2003/12/04 10:18:59  adam
 *               : Debug testcases
 *               :
 *               : Revision 2.2  2003/11/25 09:41:10  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.1  2003/09/27 15:45:39  adam
 *               : Add testcases
 *               :
 *               : Revision 2.0  2003/09/27 06:34:13  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:43  adam
 *               : updated file directory
 *               :
 *               : Revision 1.9  2003/09/26 11:02:44  adam
 *               : Add testcases
 *               :
 *               : Revision 1.8  2003/09/25 09:46:22  adam
 *               : Add testcases
 *               :
 *               : Revision 1.7  2003/09/24 08:34:31  adam
 *               : Edit for random data
 *               :
 *               : Revision 1.6  2003/09/23 11:16:08  issac
 *               : added standard PREP style service loop
 *               :
 *               : Revision 1.5  2003/09/18 07:52:04  adam
 *               : Edit left_time to time_left
 *               :
 *               : Revision 1.4  2003/09/12 08:01:46  adam
 *               : Fixed something for Reinhard's feed-back on Sep 11th.
 *               :
 *               : Revision 1.3  2003/09/09 10:59:33  adam
 *               : Edit function name
 *               :
 *               : Revision 1.2  2003/09/08 13:53:20  adam
 *               : Change USB_ to USB_PE_
 *               :
 *               : Revision 1.1  2003/09/08 02:24:34  adam
 *               : moved USB_do_schedule_loop.h to here
 *               :
 *               : Revision 1.8  2003/08/25 09:23:44  adam
 *               : Fixed carbonXReceivePacket() usage.
 *               :
 *               : Revision 1.6  2003/08/15 06:17:02  serlina
 *               : Bug fix upon integration run
 *               :
 *               : Revision 1.5  2003/08/14 18:55:49  serlina
 *               : Fixed all compilation errors/warnings
 *               :
 *               : Revision 1.4  2003/08/14 18:15:39  serlina
 *               : Enhancement with compilation error fix
 *               :
 *               : Revision 1.3  2003/08/14 15:00:17  adam
 *               : New revision, created.
 *               :
 *               : Revision 1.1  2003/08/14 12:52:03  adam
 *               : Initial revision, created.
 */
