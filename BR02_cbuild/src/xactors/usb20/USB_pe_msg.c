//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file is required for PE.  It contains:
 *
 *                 1. All necessary functions for C side monitor to log
 *                    message.
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.10  2004/08/13 18:40:35  zippelius
 *               : Removed bogus Pipe 0; Moved toggle tracking from Pipe into EP; Fixed 4 byte alloc, when data size==0;Misc fixes
 *               :
 *               : Revision 1.9  2004/08/12 14:11:19  zippelius
 *               : Numerous fixes and changes around DATA toggle
 *               :
 *               : Revision 1.8  2004/08/11 01:59:19  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.7  2004/08/02 18:53:24  zippelius
 *               : Fixed a number of bugs regarding bus time estimation
 *               :
 *               : Revision 1.6  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.5  2004/04/22 13:44:28  zippelius
 *               : Clean-up defines, messaging, others
 *               :
 *               : Revision 1.4  2004/04/19 18:38:57  jafri
 *               : fixed english in comments
 *               :
 *               : Revision 1.3  2004/04/16 20:35:15  zippelius
 *               : Some code clean up. Added defines
 *               :
 *               : Revision 1.2  2004/03/26 14:59:22  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.2  2003/12/04 10:19:06  adam
 *               : Debug testcases
 *               :
 *               : Revision 2.1  2003/11/25 09:41:33  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.0  2003/09/27 06:33:45  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:18  adam
 *               : updated file directory
 *               :
 *               : Revision 1.21  2003/09/27 03:34:02  adam
 *               : Add testcases and debug
 *               :
 *               : Revision 1.20  2003/09/26 11:02:18  adam
 *               : Add testcases
 *               :
 *               : Revision 1.19  2003/09/12 08:02:05  adam
 *               : Fixed something for Reinhard's feed-back on Sep 11th.
 *               :
 *               : Revision 1.18  2003/09/09 02:03:02  adam
 *               : Edit a minor issue
 *               :
 *               : Revision 1.17  2003/09/08 13:52:46  adam
 *               : Change USB_ to USB_PE_
 *               :
 *               : Revision 1.16  2003/09/08 02:20:07  adam
 *               : moved to USB_protocol_eng.c
 *               :
 *               : Revision 1.15  2003/08/29 13:50:53  adam
 *               : Fixed bulk test.
 *               :
 *               : Revision 1.14  2003/08/29 05:58:38  adam
 *               : Edit USB_Trans_t.
 *               :
 *               : Revision 1.13  2003/08/29 02:58:16  adam
 *               : Edit print message.
 *               :
 *               : Revision 1.12  2003/08/28 08:13:07  adam
 *               : Edit some print format
 *               :
 *               : Revision 1.11  2003/08/27 15:00:56  serlina
 *               : updated Do Enum series of functions, and dev prof data structure
 *               :
 *               : Revision 1.10  2003/08/27 14:13:35  serlina
 *               : minor printing update
 *               :
 *               : Revision 1.9  2003/08/27 09:23:03  adam
 *               : only print 8 or less data bytes
 *               :
 *               : Revision 1.8  2003/08/27 02:43:08  issac
 *               : updated USB_PE_MsgData() for better formatting
 *               :
 *               : Revision 1.7  2003/08/27 02:02:00  adam
 *               : New revision, created.
 *               :
 *               : Revision 1.6  2003/08/25 09:23:04  adam
 *               : Fixed SIM_ReceivePacket() usage.
 *               :
 *               : Revision 1.4  2003/08/15 11:02:02  serlina
 *               : Changed error injection, fixed SAR return error
 *               :
 *               : Revision 1.3  2003/08/14 14:20:54  adam
 *               : New revision, created.
 *               :
 ***************************************************************************/

#include "USB_support.h"

//%FS==================================================================
//
// USB_PE_MsgHeader
//
// Description:
//   USB_PE_MsgHeader prints the header of the log message.
//
// Parameter:
//   void
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_MsgHeader() {

  MSG_Printf("\n+--------------------------------------------------------------------------------\n");
  MSG_Printf(  "+ <time: %.10lld> USB_PE_DoTransfer function was called \n", PLAT_GetSimTime());
  MSG_Printf(  "+--------------------------------------------------------------------------------\n");
  MSG_Printf(  "+   Information on transaction:\n");
}

//%FS==================================================================
//
// USB_PE_MsgSpeed
//
// Description:
//   USB_PE_MsgSpeed logs trans speed of the transaction.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the struct being transfered, this function
//     get trans speed from the struct.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_MsgSpeed(USB_Trans_t* p_trans) {
  
      MSG_Printf("+       Trans speed:          %s\n", carbonXUsbCvtSpeedToString( p_trans->trans_speed ) );
}

//%FS==================================================================
//
// USB_PE_MsgEptType
//
// Description:
//   USB_PE_MsgEptType logs endpoint type of the transaction.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the struct being transfered, this function
//     get endpoint type from the struct.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_MsgEptType(USB_Trans_t* p_trans) {

  MSG_Printf("+       Endpoint type:        %s\n",
	     carbonXUsbCvtEndPtTypeToString(p_trans->p_pipe->p_endpt_descr->endpt_type ) );
  return;
}

//%FS==================================================================
//
// USB_PE_MsgErrInject
//
// Description:
//   USB_PE_MsgErrInject logs error injection information of the 
//   transaction.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the struct should be transfered, this function
//     get error injection information from the struct.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_MsgErrInject(USB_Trans_t* p_trans) {
  
  u_int32 c_msg = 0;
  char    msg[200];

  if(!p_trans->err_inject.is_split_phase &&
     !p_trans->err_inject.is_token_phase &&
     !p_trans->err_inject.is_data_phase) {
    return;
  }

  if (p_trans->err_inject.do_timeout_err ||
      p_trans->err_inject.do_crc_err ||
      p_trans->err_inject.do_data_corrupt ||
      p_trans->err_inject.do_data_drop ||
      p_trans->err_inject.do_pid_err ||
      p_trans->err_inject.do_datapart_err) {

    c_msg = sprintf(msg, "+       Error injected phase:");

    if (c_msg != 0) {

      if(p_trans->err_inject.is_split_phase) {
        c_msg += sprintf(&msg[c_msg], " <Split Token Phase>");
      }

      if(p_trans->err_inject.is_token_phase) {
        c_msg += sprintf(&msg[c_msg], " <Token Phase>");
      }

      if(p_trans->err_inject.is_data_phase) {
        c_msg += sprintf(&msg[c_msg], " <Data Phase>");
      }

      msg[c_msg++] = '\n';
      msg[c_msg] = 0;
      MSG_Printf("%s", msg);
      MSG_Printf("+       Error injected type:\n");
    }    
  }

  if(p_trans->err_inject.do_timeout_err) {
    MSG_Printf("+                             <Timeout>\n");
  }
  
  if(p_trans->err_inject.do_crc_err) {
    MSG_Printf("+                             <Bad crc>\n");
  }
  
  if(p_trans->err_inject.do_data_corrupt) {
    MSG_Printf("+                             <Data corruption>\n");
  }

  if(p_trans->err_inject.do_data_drop) {
    MSG_Printf("+                             <Data drop>\n");
  }
  
  if(p_trans->err_inject.do_pid_err) {
    MSG_Printf("+                             <Pid error>\n");
  }

  if(p_trans->err_inject.do_datapart_err) {
    MSG_Printf("+                             <Data part error>\n");
  }
}

//%FS==================================================================
//
// USB_PE_MsgToken
//
// Description:
//   USB_PE_MsgToken logs token packet Information of the transaction.
//
// Parameter:
//   u_int8  token_pid  I
//     It is the pid of the token packet.
//   u_int32  token_addr  I
//     It is the device address of the token packet.
//   u_int32  token_ept_num  I
//     It is the endpoint number of the token packet.
//
// Return value:
//   void
//
//%FE==================================================================

void USB_PE_MsgToken(u_int8 token_pid, u_int32 token_addr, u_int32 token_ept_num) {

  
  MSG_Printf("+       <time: %.10lld>:   %s <Addr: 0x%x Endpt: 0x%x>\n", 
	     PLAT_GetSimTime(), carbonXUsbCvtPidToString( token_pid ), token_addr, token_ept_num );


  return;
}

//%FS==================================================================
//
// USB_PE_MsgData
//
// Description:
//   USB_PE_MsgData logs data packet Information of the transaction.
//
// Parameter:
//   u_int8*  p_data  I
//     It points to the data packet.
//   u_int32  data_size  I
//     It is the size of the data packet.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_MsgData(u_int8* p_data, u_int32 data_size) {

  u_int8  pid;
  u_int32 i;
  u_int32 c_msg;
  char    msg[200];

  pid = p_data[0];
  c_msg = sprintf(msg, "%s <", carbonXUsbCvtPidToString( pid ) );

  
  if (data_size > 1) {
    for(i = 1; i < data_size; i++) {
      c_msg += sprintf(&msg[c_msg], " 0x%x",  p_data[i] );

      // Stop printing after the first 8 bytes
      if( i == 8 ) { break; }
    }

    if ( data_size > 9 ) {
      c_msg += sprintf(&msg[c_msg], "\n+                              .. " );

      // Print the last 8 bytes of the transaction
      for( i = (data_size-8 > 8 ? data_size-8 : 9 ); i<data_size; i++ ) {
	c_msg += sprintf(&msg[c_msg], " 0x%x",  p_data[i] );
      }
    }

    // Add string terminator
    msg[++c_msg] = 0;

  }

  MSG_Printf("+       <time: %.10lld>:   %s>\n", PLAT_GetSimTime(), msg);
  MSG_Printf("+       Data Size:            %d\n", data_size-1);

  return;
}

//%FS==================================================================
//
// USB_PE_MsgAfterSplit
//
// Description:
//   USB_PE_MsgAfterSplit logs packet information after a split token packet  
//   for the transaction.
//
// Parameter:
//   u_int8*  p_after_split  I
//     It points to the packets after a split token packet.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_MsgAfterSplit(u_int8* p_after_split) {

  u_int32 after_split   = 0;
  u_int8  pid     = 0;
  u_int32 addr    = 0;
  u_int32 ept_num = 0;
  
  pid   = p_after_split[0] & 0xF; // Get PID
  
  after_split   = p_after_split[1] << 8;
  after_split  |= p_after_split[2];
 
  addr    = (after_split >> 9) & 0x7F;  // Get device address
  ept_num = (after_split >> 5) &0xf;	// Get endpoint number

  USB_PE_MsgToken( pid, addr, ept_num );
}

//%FS==================================================================
//
// USB_PE_MsgHandshake
//
// Description:
//   USB_PE_MsgHandshake logs all handshake packet information (including 
//   handshake packets and data packets) for the transaction.
//
// Parameter:
//   u_int8*  p_msg_hs  I
//     It points to the handshake packet.
//   USB_Pid_e  msg_hs  I
//     It indicates which handshake the HC should be issued.
//   u_int32  msg_hs_size  I
//     It is the size of the data packet.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_MsgHandshake(u_int8* p_msg_hs, USB_Pid_e msg_hs, u_int32 msg_hs_size) {

  if(msg_hs == USB_PID_DATA0 || msg_hs == USB_PID_DATA1 || 
     msg_hs == USB_PID_DATA2 || msg_hs == USB_PID_MDATA) {

    USB_PE_MsgData(p_msg_hs, msg_hs_size);    
  }
  else if ( msg_hs == USB_PID_ACK || msg_hs == USB_PID_NAK || msg_hs == USB_PID_NYET || 
     msg_hs == USB_PID_STALL || msg_hs == USB_PID_ERR_PRE ) {

    MSG_Printf("+       <time: %.10lld>:    %s\n", PLAT_GetSimTime(), carbonXUsbCvtPidToString( (u_int8)msg_hs ) );    
  }

  return;
}
