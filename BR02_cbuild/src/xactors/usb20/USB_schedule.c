//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/***************************************************************************
 *
 * Comments      : This file is for the Scheduler. 
 *                 It includes all functions that the Scheduler uses.
 *
 *                 Functions in this file can be classified into
 *                 [principle functions, and supporting functions.].
 *                 The [principle functions] are listed below and they
 *                 strictly follow section 2.4 of the Implementation Spec v1.8:
 *                 carbonXUsbExecIRPs()
 *                 USB_SCH_IpgUframe()
 *                 USB_SCH_IssueSOF()
 *                 USB_SCH_DoPeriodicPipes()
 *                 USB_SCH_DoCtrlPipes()
 *                 USB_SCH_DoBulkPipes()
 *                 USB_SCH_DoIdle()
 *                 USB_SCH_ExecTrans()
 *                 USB_SCH_UpdateTrans()
 *                 USB_SCH_DoHcTransState()
 *                 USB_SCH_DoReport()
 *    
 *                 Other implementation notes:
 *                 While implementing time mechanism, 3 time units are used:
 *                    [bits_time;cycle;NS]
 *                 1. Bits_time is used in the Pipe and IRP module.
 *                 2. Cycle is used in the BFM.
 *                 3. NS is used in the Scheduler module.
 *                 3 time units exit since each is most suitable for a
 *                 specific task, conversion is done in this file.
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 *
 ***************************************************************************/

#include "USB_support.h"

//static frame_rolled = 0;


//%FS==================================================================
//
// carbonXUsbExecIRPs
//
// Description:
//   carbonXUsbExecIRPs processes all the IRPs pending 
//   according to bus BW and IRP priority, first periodic, 
//   then control, then bulk.
//   This function strictly follows section 2.4 of the
//   Implementation Spec v1.8.
//
// Parameter:
//   u_int32  ipg  I
//     It indicates inter packet gap in terms of (micro)frames,
//     at the beginning of the scheduling loop..
//
// Return value:
//   void
//
//%FE==================================================================
void carbonXUsbExecIRPs(u_int32 ipg) {

  u_int32              status_info   = 0;  
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  USB_DevProfile_t*    p_dev;
 
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  p_pipe  = p_top->sys_pipe_mgr.ppa_pipe_head[ 0 ]; //@@@RZ
  //  p_pipe  = p_top->sys_pipe_mgr.ppa_pipe_head[ 1 ];
  p_dev   = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

  // Calculate BFM clock
  if(p_dev->speed_mode == USB_SM_HUB_HS_FS) {
    status_info        = carbonXCsRead(USB_BFM_ADDR_STATUS_REGISTER);
    status_info       &= USB_STATUS_DATA_BUS;
    p_top->bfm_clock_cycle = (u_int32) ( (status_info == 1) ?
      (1000/30 + 1) : (1000/60 + 1) ); 
  }
  
  else {
    if(p_dev->speed == USB_TRANS_FS) {
      p_top->bfm_clock_cycle = (u_int32) (1000/48 + 1);
    }
    else if(p_dev->speed == USB_TRANS_LS) {
      p_top->bfm_clock_cycle = (u_int32) (1000/6 + 1);
    }
  }
  
  // Callback function written by user
  if ( p_top->pfBeforeSchLoop ) {
    // callback function exists.
    // -> Call it.
    p_top->pfBeforeSchLoop( p_top );    
  }    
 
  // Each time Scheduler loop is called, we update the [Periodic, CTRL, BULK] lists.
  // This is how we handle the case when user inserts more pipes to the system
  // between different calls of carbonXUsbExecIRPs().
  USB_SCH_SetList(p_top, p_sched);

  USB_SCH_IpgUframe(ipg, p_dev);

  while(USB_SCH_IsIrpPending(p_top)) {

    p_sched->time_of_SOF = PLAT_GetSimTime();
    p_sched->time_of_next_SOF = p_sched->time_of_SOF +
      ((p_dev->speed == USB_TRANS_HS) ? USB_MICROFRAME_TIME : USB_FRAME_TIME);
    p_sched->time_left = p_sched->time_of_next_SOF - p_sched->time_of_SOF;

#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> carbonXUsbExecIRPs: Calling USB_SCH_IssueSOF()\n", PLAT_GetSimTime() );
#endif

    USB_SCH_IssueSOF(p_sched, p_dev);

#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> carbonXUsbExecIRPs: Calling USB_SCH_DoPeriodicPipes()\n", PLAT_GetSimTime() );
#endif

    USB_SCH_DoPeriodicPipes();

#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> carbonXUsbExecIRPs: Calling USB_SCH_DoCtrlPipes()\n", PLAT_GetSimTime() );
#endif

    USB_SCH_DoCtrlPipes();
    
#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> carbonXUsbExecIRPs: Calling USB_SCH_DoBulkPipes()\n", PLAT_GetSimTime() );
#endif

    USB_SCH_DoBulkPipes();
    
#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> carbonXUsbExecIRPs: Calling USB_SCH_DoIdle()\n", PLAT_GetSimTime() );
#endif

    USB_SCH_DoIdle(p_sched);  

		//    speed_modulus = (p_dev->speed == USB_TRANS_HS) ? 8 : 1;
		//		frame_rolled = (p_sched->uframe_count == (speed_modulus * 0x800) - 1) ? 1 : 0;
		//    p_sched->uframe_count  = (p_sched->uframe_count == (speed_modulus * 0x800) - 1) ?
		//                             0 : p_sched->uframe_count + 1;
		p_sched->uframe_count++;
              // The microframe count rolls over upon
              // reaching its maximum value of 8*0x800 - 1;
          // The frame count rolls over upon
              // reaching its maximum value of 0x7ff.
  }         // End while ... do
}

//%FS==================================================================
//
// USB_SCH_IpgUframe
//
// Description:
//   USB_SCH_IpgUframe performs inter packet gap in terms of
//   (micro)frame at the beginning of each scheduling loop.
//
// Parameter:
//   u_int32  ipg  I
//     It indicates inter packet gap in terms of (micro)frames,
//     at the beginning of the scheduling loop.
//   USB_DevProfile_t*  p_dev  I
//     It points to the structure USB_DevProfile_t of the current
//     device.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_IpgUframe( u_int32 ipg, USB_DevProfile_t* p_dev ) {
  
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
 
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 

  while(ipg > 0) {      // While ... do

    USB_SCH_IssueSOF(p_sched, p_dev);
    
    USB_SCH_DoIdle(p_sched);  

    //    speed_modulus = (p_dev->speed == USB_TRANS_HS) ? 8 : 1;
    //		frame_rolled = (p_sched->uframe_count == (speed_modulus * 0x800) - 1) ? 1 : 0;
    //    p_sched->uframe_count  = (p_sched->uframe_count == speed_modulus * 0x800 - 1) ?
    //                             0 : p_sched->uframe_count + 1;
    p_sched->uframe_count++;
    // The microframe count rolls over upon
    // reaching its maximum value of 8*0x800 - 1;
    // The frame count rolls over upon
    // reaching its maximum value of 0x7ff.
    ipg --;
  }         // End while ... do
}

//%FS==================================================================
//
// USB_SCH_IssueSOF
//
// Description:
//   USB_SCH_IssueSOF implements SOF transactions.
//
// Parameter:
//   USB_Scheduler_t*  p_sched  IO
//     It points to structure USB_Scheduler_t that stores
//     Scheduler information.
//   USB_DevProfile_t*  p_dev  I
//     It points to the structure USB_DevProfile_t of the current
//     device.
//
// Return value:581

//   void
//
//%FE==================================================================
void USB_SCH_IssueSOF(USB_Scheduler_t* p_sched, USB_DevProfile_t* p_dev) {

  u_int32            speed_modulus = 0;
  u_int64            trans_start;
  u_int64            trans_end;
  
  USB_Trans_t        trans;
  USB_Data_t         data_null;
  USB_HostCtrlTop_t *p_top;

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  // Callback function before SOF is being dispatched.
  if ( p_top->pfBeforeSof ) {
    // callback function exists.
    // -> Call it.
    p_top->pfBeforeSof( p_top );    
  }    

  data_null.p_data = NULL;
  data_null.size   = 0;

  trans.p_pipe         = NULL;
  trans.p_irp        = NULL;           

  trans.p_data_segment = &data_null;

  speed_modulus   = (p_dev->speed == USB_TRANS_HS) ? 8 : 1;
  trans.frame_num = p_sched->uframe_count/speed_modulus;
  trans.frame_num = (trans.frame_num > 0x7ff) ? 0 : trans.frame_num;
          // The frame number rolls over upon
              // reaching its maximum value of 0x7ff. 
  trans.pid                  = USB_PID_SOF;
  trans.trans_type           = USB_TRANS_TYPE_SOF;
  trans.time_first_attempted = 0;
  trans.time_started         = 0;
  trans.time_completed       = 0;

  trans.err_inject.do_timeout_err   = FALSE;
  trans.err_inject.do_pid_err       = FALSE;
  trans.err_inject.do_datapart_err  = FALSE;
  trans.err_inject.is_split_phase   = FALSE;
  trans.err_inject.is_data_phase    = FALSE;
  
  if(p_sched->do_sof_err && (p_sched->uframe_count >= p_sched->frame_cnt_begin) &&
     (p_sched->uframe_count <= p_sched->frame_cnt_end)) {

    trans.err_inject.is_token_phase = TRUE;

    // Check to inject a bad CRC into specified packet
    trans.err_inject.do_crc_err = ( (USB_SOF_ERR_BADCRC &
      p_sched->sof_err_mask) == USB_SOF_ERR_BADCRC) ? TRUE : FALSE;  
    // Check to do data corruption
    trans.err_inject.do_data_corrupt = ( (USB_SOF_ERR_DATA_CORRUPT &
      p_sched->sof_err_mask) == USB_SOF_ERR_DATA_CORRUPT) ? TRUE : FALSE;  
    // Check to drop some data
    trans.err_inject.do_data_drop = ( (USB_SOF_ERR_DATA_DROP &
      p_sched->sof_err_mask) == USB_SOF_ERR_DATA_DROP) ? TRUE : FALSE;  
  }
  else {
    trans.err_inject.is_token_phase   = FALSE;
    trans.err_inject.do_crc_err       = FALSE;
    trans.err_inject.do_data_corrupt  = FALSE;
    trans.err_inject.do_data_drop     = FALSE;
  }

  trans.trans_speed = (p_dev->speed == USB_TRANS_HS) ? USB_TRANS_HS : USB_TRANS_FS;

  // Change bit time to BFM cycle
  trans.expected_duration  = USB_SCH_CvtBitTimeToDuration(
             p_dev->speed, USB_CalcEstSofBitTime( p_dev->speed ) );

  trans_start =  PLAT_GetSimTime();

  USB_PE_DoTransfer(&trans);


  trans_end =  PLAT_GetSimTime();
  
  p_sched->time_left -= (trans_end - trans_start);

}

//%FS==================================================================
//
// USB_SCH_DoPeriodicPipes
//
// Description:
//   USB_SCH_DoPeriodicPipes implements all periodic transactions
//   scheduled to be transferred in the current (micro)frame.
//
// Parameter:
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_DoPeriodicPipes() {

  u_int32              id           = 0;
  u_int32              pipe_id      = 0;
	u_int32              i = 0;
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  USB_DevProfile_t*    p_dev;

  USB_Trans_t          trans;
  USB_Trans_t*         p_trans = &trans;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
    
  // Callback function at the beginning of the execution of the periodic
  // transaction list.
  if ( p_top->pfBeginOfExecPeriodList ) {
    // callback function exists.
    // -> Call it.
    p_top->pfBeginOfExecPeriodList( p_top );
  }    

  // Do Periodic pipes
  //
  // Go through all periodic pipes, find those that satisfy condition A & B & C
  // A: Pipe_State is USB_PIPE_STAT_READY and pipe is non-empty
  // B: The current frame number is >= scheduled_frame number
  // C: We schedule peroidic pipes using 3 parameters [CUR_FRAME_NUMBER, 
  //    SCH_FRAME_NUMBER, PERIOD]. We only grant permission to the periodic
  //    pipe if (CUR_FRAME_NUMBER-SCH_FRAME_NUMBER)%PERIOD == 0.
  //
  for(id = 0; id < p_sched->period_count; id++) {
    
    pipe_id = p_sched->p_periodic[ id ];
    p_pipe  = carbonXUsbGetPipeDescr(pipe_id);
    p_dev   = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

    p_sched->hs_trans_count = 0;

		i = 0;
		//		if((p_pipe->sys_record.uframe_sched >= ((p_dev->speed == USB_TRANS_HS) ? (0x800*8)-1 : 0x7ff)) && frame_rolled) {
		//			p_pipe->sys_record.uframe_sched = p_pipe->sys_record.uframe_sched - ((p_dev->speed == USB_TRANS_HS) ? (0x800*8)-1 : 0x7ff);
		//		}

    while( (p_sched->hs_trans_count <= p_pipe->p_endpt_descr->hs_trans_num)  &&  (p_sched->time_left > 0) ) {

      // Condition A, B, C check.
      if((p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY) &&
         (p_pipe->a_irp_uc.count != 0) &&
				 //         (p_sched->uframe_count >= p_pipe->sys_record.uframe_sched) &&
         (p_sched->uframe_count >= p_pipe->sys_record.uframe_sched)
				 ) {
				//         ((p_sched->uframe_count - p_pipe->sys_record.uframe_sched) % 
				//          (1 << (p_pipe->p_endpt_descr->interval-1) ) == 0) ) {


        // Check, if this Pipe has an IRP that is ready for execution and that needs to be SARed.
        if(USB_SAR_NeedsSar(p_pipe)) {
          carbonXUsbIrpGetNextSegment(p_pipe);
        }

        USB_SCH_UpdateTrans( p_trans, p_pipe );

        // Calculate the time remaining in this (micro-) for periodic traffic
        p_sched->time_left = p_sched->time_of_next_SOF -
          (p_dev->speed == USB_TRANS_HS) ? USB_MICROFRAME_TIME * 0.2 : USB_FRAME_TIME * 0.1
          - PLAT_GetSimTime();
  
        // Calculate the time remaining in this (micro-) for periodic traffic
        //  p_sched->time_left = p_sched->time_of_next_SOF - PLAT_GetSimTime();
        //  p_sched->time_left -= (p_dev->speed == USB_TRANS_HS) ? USB_MICROFRAME_TIME * 0.2 : USB_FRAME_TIME * 0.1;
        
        p_sched->time_left = p_sched->time_of_next_SOF - PLAT_GetSimTime() -
          ((p_dev->speed == USB_TRANS_HS) ? USB_MICROFRAME_TIME * 0.2 : USB_FRAME_TIME * 0.1);
        
        if ( p_sched->time_left >= (int)p_trans->expected_duration ) {
          USB_SCH_ExecTrans( p_trans );
        }
      }
      
      p_sched->hs_trans_count ++;
    }
  }

  if(p_sched->time_left <0)
    MSG_Panic("p_sched->time_left = %d but should be 0 or greater\n",p_sched->time_left);
}

//%FS==================================================================
//
// USB_SCH_DoCtrlPipes
//
// Description:
//   USB_SCH_DoCtrlPipes implements all control transactions
//   scheduled to be transferred in the current (micro)frame.
//
// Parameter:
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_DoCtrlPipes() {

  u_int32 id        = 0;
  u_int32 pipe_id         = 0; 
  u_int32 cnt_inactive_pipe     = 0;
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  USB_DevProfile_t*    p_dev;

  USB_Trans_t          trans;
  USB_Trans_t*         p_trans = &trans;
 
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 

  // Callback function at the beginning of the execution of the control
  // transaction list.
  if ( p_top->pfBeginOfExecCtrlList ) {
    // callback function exists.
    // -> Call it.
    p_top->pfBeginOfExecCtrlList( p_top );    
  }    

  // Count all the control pipes that do not have IRP pending.
  for (id = 0; id < p_sched->ctrl_count; id++) {

    pipe_id = *(p_sched->p_asyc_ctrl + id);
    p_pipe  = carbonXUsbGetPipeDescr(pipe_id);
       
    if(p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY_NOTIME) {
       p_pipe->sys_record.pipe_state = USB_PIPE_STAT_READY;
    }
    // cnt_inactive_pipe is the amount of the inactive control pipes
    cnt_inactive_pipe += (p_pipe->sys_record.pipe_state != USB_PIPE_STAT_READY
                             || p_pipe->a_irp_uc.count == 0) ? 1 : 0;
  }

  // id needs to pick up from the record of last microframe when scheduling was over.
  id = p_sched->asyc_ctrl_index_next;
  
  // Do Ctrl pipes
  //
  // Go through all ctrl pipes, find those that satisfy condition A & B & C
  // A: pipe_state is USB_PIPE_STAT_READY
  // B: there is enough time for transaction in the current (micro)frame. 
  // C: the pipe does not become inactive after transaction
  //
  // Note that we exit from the while loop when there is no bandwidth left for 
  // all ctrl pipes.
  while(cnt_inactive_pipe < p_sched->ctrl_count) {  

    pipe_id  = p_sched->p_asyc_ctrl[ id ];
    p_pipe   = carbonXUsbGetPipeDescr(pipe_id);
    p_dev    = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

    // Condition A check.
    if(p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY) {
      // Check if IRP needs to SARing.
      if(USB_SAR_NeedsSar(p_pipe)) {
        carbonXUsbIrpGetNextSegment(p_pipe);
      }

      // Calculate the time remaining in this (micro-) for control traffic
      p_sched->time_left = p_sched->time_of_next_SOF - PLAT_GetSimTime();

      if(p_sched->time_left <0)
	MSG_Panic("p_sched->time_left = %d but should be 0 or greater - next SOF @ %d\n",
		  p_sched->time_left,p_sched->time_of_next_SOF);

      USB_SCH_UpdateTrans( p_trans, p_pipe );

      // Condition B check.
      // fredieu - added on 5 cycles!  Some core accesses cause this.
      if ( p_sched->time_left >= (int)(p_trans->expected_duration + 84)) {
        USB_SCH_ExecTrans( p_trans );

        // Condition C check
        if(p_pipe->sys_record.pipe_state != USB_PIPE_STAT_READY) {
          cnt_inactive_pipe ++;
        }
      }
      else {
        p_pipe->sys_record.pipe_state = USB_PIPE_STAT_READY_NOTIME;
        cnt_inactive_pipe ++;
      }
    }

    id = (id == (p_sched->ctrl_count - 1)) ? 0 : id + 1;
              // Round robin increment of id.
  }         // End while ... do
  if(p_sched->time_left <0)
    MSG_Panic("p_sched->time_left = %d but should be 0 or greater\n",p_sched->time_left);
  
  p_sched->asyc_ctrl_index_next = id;   // Record at which index next microframe
            // should start processing. 
}


//%FS==================================================================
//
// USB_SCH_DoBulkPipes
//
// Description:
//   USB_SCH_DoBulkPipes implements all bulk transactions
//   scheduled to be transferred in the current (micro)frame.
//
// Parameter:
//     device.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_DoBulkPipes() {

  u_int32              id             = 0;
  u_int32              pipe_id        = 0; 
  u_int32              cnt_inactive_pipe  = 0;
  u_int32              exp_time       = 0;
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  USB_DevProfile_t*    p_dev;
  USB_Trans_t          trans;
  USB_Trans_t*         p_trans = &trans;
  //  TRANS_HANDLE_T       transactor_hdl;
 
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  //  transactor_hdl = PLAT_GetHandleFromTransTable();

  // Callback function at the beginning of the execution of the bulk
  // transaction list.
  if ( p_top->pfBeginOfExecBulkList ) {
    // callback function exists.
    // -> Call it.
    p_top->pfBeginOfExecBulkList( p_top );
  }    

  // Count all the bulk pipes that do not have IRP pending.
  for(id = 0; id < p_sched->bulk_count; id++) {

    pipe_id = *(p_sched->p_asyc_bulk + id);
    p_pipe  = carbonXUsbGetPipeDescr(pipe_id);

    if(p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY_NOTIME) {
       p_pipe->sys_record.pipe_state = USB_PIPE_STAT_READY;
    }
    // cnt_inactive_pipe is the amount of the inactive bulk pipes
    cnt_inactive_pipe += (p_pipe->sys_record.pipe_state != USB_PIPE_STAT_READY
                             || p_pipe->a_irp_uc.count == 0) ? 1 : 0;
  }

  // id needs to pick up from the record of last microframe when scheduling was over.
  id = p_sched->asyc_bulk_index_next;
 
  // Do Bulk pipes
  //
  // Go through all bulk pipes, find those that satisfy condition A & B
  // A: pipe_state is USB_PIPE_STAT_READY
  // B: there is enough time for transaction in the current (micro)frame. 
  // C: the pipe does not become empty after transaction
  //
  // Note that we exit from the while loop when there is no transmission time left for 
  // all ctrl pipes.
  //
  while(cnt_inactive_pipe < p_sched->bulk_count) {  

    exp_time = 0;   
    pipe_id  = p_sched->p_asyc_bulk[ id ];
    p_pipe   = carbonXUsbGetPipeDescr(pipe_id);
    p_dev    = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

    // Condition A check.
    if(p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY) {

      // Check if IRP needs to SARing.
      if( USB_SAR_NeedsSar(p_pipe) ) {
        carbonXUsbIrpGetNextSegment(p_pipe);
      }

      // Calculate the time remaining in this (micro-) for bulk traffic
      p_sched->time_left = p_sched->time_of_next_SOF - PLAT_GetSimTime();

      if(p_sched->time_left < 0)
  MSG_Panic("p_sched->time_left = %d but should be 0 or greater\n",p_sched->time_left);

#ifdef VERBOSE_DEBUG
      MSG_LibDebug( transactor_hdl, MSG_TRACE, "<%.10lld> Calling USB_SCH_UpdateTrans\n", PLAT_GetSimTime() );
#endif
      USB_SCH_UpdateTrans( p_trans, p_pipe );

      // Condition B check.
      if ( p_sched->time_left >= (int)p_trans->expected_duration ) {

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( transactor_hdl, MSG_TRACE, "<%.10lld> Calling USB_SCH_ExecTrans\n", PLAT_GetSimTime() );
#endif

  USB_SCH_ExecTrans( p_trans );

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( transactor_hdl, MSG_TRACE, "<%.10lld> Finished USB_SCH_ExecTrans\n", PLAT_GetSimTime() );
#endif

        // Condition C check
        if(p_pipe->sys_record.pipe_state != USB_PIPE_STAT_READY) {
          cnt_inactive_pipe ++;
        }
      }
      else {
        p_pipe->sys_record.pipe_state = USB_PIPE_STAT_READY_NOTIME;
        cnt_inactive_pipe ++;
      }
    }

    id = (id == (p_sched->bulk_count - 1)) ? 0 : id + 1;
              // Round robin increment of id.
  } // End while ... do

  if(p_sched->time_left <0)
    MSG_Panic("p_sched->time_left = %d but should be 0 or greater\n",p_sched->time_left);

  p_sched->asyc_bulk_index_next = id;   // Record at which index next microframe
            // should start processing.  
}

//%FS==================================================================
//
// USB_SCH_DoIdle
//
// Description:
//   USB_SCH_DoIdle uses up the left time of the current (micro)frame.
//
// Parameter:
//   USB_Scheduler_t*  p_sched  I
//     It points to structure USB_Scheduler_t that stores
//     Scheduler information.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_DoIdle(USB_Scheduler_t* p_sched) {

  u_int32              idle_time = 0;
  USB_HostCtrlTop_t*   p_top;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  if( p_top->sys_sched.use_up_micro_frame == 0 ) {
    return;
  }

  // Calculate the time remaining in this (micro-) for control traffic
  p_sched->time_left = p_sched->time_of_next_SOF - PLAT_GetSimTime();

  idle_time = p_sched->time_left;

  if( idle_time == 0 ) {
    return;
  }

  idle_time = idle_time / p_top->bfm_clock_cycle;
  carbonXIdle(idle_time);
}

//%FS==================================================================
//
// USB_SCH_ExecTrans
//
// Description:
//   USB_SCH_ExecTrans builds USB_Trans_t, calls USB_PE_DoTransfer,
//   and processes the result.
//   This functions executes atomic transfers, e.g.:
//       OUT DATAx HandshakeToken
//       IN  DATAx HandshakeToken
//       SETUP DATA0 HandshakeToken
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the USB_Trans_t struct to be sent to the PE.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_ExecTrans( USB_Trans_t* p_trans ) {

  u_int64              trans_start;
  u_int64              trans_end;
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  //  TRANS_HANDLE_T       transactor_hdl;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  //  transactor_hdl = PLAT_GetHandleFromTransTable();

  if(p_trans == NULL) MSG_Panic("NULL pointer!\n");

  p_pipe  = p_trans->p_pipe;
 
  if(((p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY) && 
      (p_pipe->a_irp_uc.count != 0)) == 0) MSG_Panic("Things are not good here!\n");

  // Callback function at the beginning of the execution of a transaction.
  if ( p_top->pfBeginOfTransaction ) {
    // callback function exists.
    // -> Call it.
    p_top->pfBeginOfTransaction( p_top, p_trans );    
  }    

  trans_start =  PLAT_GetSimTime();

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( transactor_hdl, MSG_TRACE, "  USB_SCH_ExecTrans trans_start= %d\n", trans_start );
#endif


  USB_PE_DoTransfer( p_trans );

  trans_end =  PLAT_GetSimTime();

  // Adjust the time we have left for transmitting in this frame
  p_sched->time_left -= (trans_end - trans_start);

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( transactor_hdl, MSG_TRACE, "  USB_SCH_ExecTrans trans_end  = %d\n", trans_end );
  MSG_LibDebug( transactor_hdl, MSG_TRACE, "  USB_SCH_ExecTrans time_left  = %d\n", p_sched->time_left );
#endif

  USB_SCH_DoReport( p_trans );

  // Callback function at the end of the execution of a transaction.
  if ( p_top->pfEndOfTransaction ) {
    // callback function exists.
    // -> Call it.
    p_top->pfEndOfTransaction( p_top, p_trans );    
  }    

  USB_SCH_DoHcTransState( p_trans );

  return;
}

//%FS==================================================================
//
// USB_SCH_UpdateTrans
//
// Description:
//   USB_SCH_UpdateTrans builds USB_Trans_t, prepares for
//   calling USB_PE_DoTransfer.
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the structure USB_Trans_t sent to PE.
//
//   USB_PipeDescr_t* p_pipe
//     It points to the pipe that this transfer belongs to
//
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_UpdateTrans( USB_Trans_t* p_trans, USB_PipeDescr_t* p_pipe) {

  u_int32                speed_modulus   = 0; 
  USB_HostCtrlTop_t*     p_top;
  USB_Scheduler_t*       p_sched; 
  USB_DevProfile_t*      p_dev;
  USB_Data_t*            p_sar;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  p_dev   = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

  p_trans->expected_duration    = 0;
  p_trans->time_first_attempted = 0;
  p_trans->time_completed       = 0;
  p_trans->time_started         = 0;

  p_sar = p_top->ppa_sys_sar_data[ p_pipe->pipe_id];

  // Set some fields of the p_trans struct
  p_trans->needs_toggle  = p_pipe->p_endpt_descr->needs_toggle;

#ifdef USB_WA_IP_NO_TOGGLE
  p_trans->needs_toggle = FALSE;
#endif

  p_trans->dev_addr    = p_dev->dev_addr;
  p_trans->trans_speed = p_dev->speed;  
  p_trans->p_pipe      = p_pipe;           
  p_trans->p_irp       = p_pipe->a_irp_uc.p_head->p_irp;           
  p_trans->needs_pre   = (p_dev->speed == USB_TRANS_LS) ? TRUE : FALSE;      
  p_trans->datapart    = (p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO) ?
                           p_trans->p_irp->irp_sys_record.data_part :
                           p_trans->datapart;

  // Set frame number
  speed_modulus         = (p_dev->speed == USB_TRANS_HS) ? 8 : 1;
  p_trans->frame_num  = ((p_sched->uframe_count)/speed_modulus) & 0x7ff;
  if(p_trans->frame_num > 0x7ff) {  // The frame number rolls over upon
              // reaching its maximum value of 0x7ff.
    p_trans->frame_num = 0x0;
  }


  if ( p_trans->p_irp->status == USB_IRP_STAT_READY ) {

#ifdef VERBOSE_DEBUG
    MSG_Printf( "<%.10lld> Starting execution of IRP #%d; buddy ID=#%d\n",
    PLAT_GetSimTime(), p_trans->p_irp->irp_id, p_trans->p_irp->buddy_id );
#endif

    // Callback function at the end of the execution of a transaction.
    if ( p_top->pfBeginOfExecIrp ) {
      // callback function exists.
      // -> Call it.
      p_top->pfBeginOfExecIrp( p_top, p_trans->p_irp, p_pipe );    
    }    
  }

  p_trans->p_irp->status = USB_IRP_STAT_INPROG;


  USB_SCH_SetErrInjection(p_trans);

  // Build non-split transaction
  if(!p_dev->is_split) {
    
    p_trans->trans_type = USB_TRANS_TYPE_NONSPLIT;
      
    // Transfer data from SAR to the PE buffer.
    memcpy(p_top->cp_sys_pe_buffer->p_data, p_sar->p_data, p_sar->size);
    p_top->cp_sys_pe_buffer->size = p_sar->size;

    p_trans->p_data_segment = p_top->cp_sys_pe_buffer;

    USB_SCH_SetTransPid( p_trans );
  }    
  // Build split transaction
  else {
    USB_SCH_BuildSplitTransT( p_trans, p_sar );
  }


  // Estimate the duration of the transaction
  //
  // In case of a split transaction account for the Split token

  //MSG_Printf( "\n++++ USB_SCH_UpdateTrans estimating duration of transaction PID=%s\n",
  //            carbonXUsbCvtPidToString(p_trans->pid) );

  p_trans->expected_duration += (p_dev->is_split) ?
    USB_SCH_EstimateDuration(TRUE, USB_SPLIT_TOKEN_LEN, p_trans->p_pipe, p_sched) : 0;

  if(p_trans->pid == USB_PID_IN) {
    p_trans->expected_duration += USB_SCH_EstimateDuration(FALSE,
      p_trans->p_pipe->p_endpt_descr->max_packet_size, p_trans->p_pipe, p_sched);
  }
  else if(p_dev->is_split) {
    p_trans->expected_duration += USB_SCH_EstimateDuration(FALSE,
      p_trans->p_data_segment->size - 4, p_trans->p_pipe, p_sched);
  }
  else {
    if(p_trans->pid != USB_PID_PING) {
      // We substract 1 from the data segment size to account for the PID byte
      p_trans->expected_duration += USB_SCH_EstimateDuration(FALSE,
        p_trans->p_data_segment->size-1, p_trans->p_pipe, p_sched);
    }
    else {
      p_trans->expected_duration +=
        USB_SCH_EstimateDuration(TRUE, 0, p_trans->p_pipe, p_sched);
    }
  }
}

//%FS==================================================================
//
// USB_SCH_DoHcTransState
//
// Description:
//   USB_SCH_DoHcTransState processes the transaction result
//   returned from USB_PE_DoTransfer, and decides to retry or to do
//   the next transaction.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the structure USB_Trans_t sent to PE.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_DoHcTransState( USB_Trans_t* p_trans ) {

  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  p_pipe = p_trans->p_pipe;
  
  // Do update using [p_trans->trans_state, p_trans->pid] combination.
  switch (p_trans->trans_state) {     // Switch
        
  case USB_DO_COMP_IMMED_NOW : 
    p_pipe->sys_record.prev_trans_state = USB_DO_COMP_IMMED_NOW;
    USB_SCH_RetryCsImmed( p_trans );
    break;

  case USB_DO_COMPLETE : 
    p_pipe->sys_record.prev_trans_state = USB_DO_COMPLETE;
    p_pipe->sys_record.uframe_sched     = p_sched->uframe_count + 1;
    break;

  case USB_DO_COMPLETE_IMMED : 
    p_pipe->sys_record.prev_trans_state = USB_DO_COMPLETE_IMMED;
    USB_SCH_RetryCsImmed( p_trans );
    break;

  case USB_DO_HALT : 
    p_pipe->sys_record.prev_trans_state = USB_DO_HALT;
    p_pipe->sys_record.pipe_state = USB_PIPE_STAT_HALTED;
    if ( !p_top->ErrorMsgDisabled.transaction_halted ) {
      MSG_Error("USB transaction for IRP %d errored/timed out 3 times resulting in HALT, pipe #%d was halted!\n",
		p_trans->p_irp->irp_id, p_pipe->pipe_id);
    }
    USB_IRP_Update( p_pipe );
    USB_PIPE_Update( p_pipe );
    break;
    
  case USB_DO_NEXT_CMD :
    p_pipe->sys_record.prev_trans_state = USB_DO_NEXT_CMD;

    if( carbonXUsbIsDataPid(  p_trans->pid ) ) {
      carbonXUsbIrpAddNextSegment(p_trans->p_data_segment, p_pipe);
    }

    p_pipe->sys_record.uframe_sched = p_sched->uframe_count +
      ((p_sched->hs_trans_count >= p_pipe->p_endpt_descr->hs_trans_num) ?
        (1 << (p_pipe->p_endpt_descr->interval-1) ) : 0);
    USB_IRP_Update(p_pipe);
    USB_PIPE_Update(p_pipe);
    break;
    
  case USB_DO_NEXT_COMPLETE :
    p_pipe->sys_record.prev_trans_state = USB_DO_NEXT_COMPLETE;
    p_pipe->sys_record.uframe_sched     = p_sched->uframe_count + 1;
    
    if(p_trans->pid == USB_PID_MDATA) {
      carbonXUsbIrpAddNextSegment(p_trans->p_data_segment, p_pipe);
    }
    break;

  case USB_DO_NEXT_PING :
    p_pipe->sys_record.prev_trans_state = USB_DO_NEXT_PING;
    break;
        
  case USB_DO_OUT : 
    p_pipe->sys_record.prev_trans_state = USB_DO_OUT;
    break;

  case USB_DO_PING : 
    p_pipe->sys_record.prev_trans_state = USB_DO_PING;
    break;

  case USB_DO_SAME_CMD :
    p_pipe->sys_record.prev_trans_state = USB_DO_SAME_CMD;
    p_pipe->sys_record.uframe_sched = p_sched->uframe_count + ((p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_INTR) ?    
      (1 << (p_pipe->p_endpt_descr->interval-1) ) : 0);
    break;

  case USB_DO_SAME_CMD_TIMEOUT :
    p_pipe->sys_record.prev_trans_state = USB_DO_SAME_CMD_TIMEOUT;
    USB_SCH_RetryForTimeout( p_trans );
    break;

	case USB_DO_START : 
		p_pipe->sys_record.prev_trans_state = USB_DO_START;
		p_pipe->sys_record.uframe_sched     = p_sched->uframe_count + 1;
		break;

  default :
    MSG_Panic("Should not be here!\n");
    p_pipe->sys_record.prev_trans_state = USB_DO_NEXT_CMD;
    break;
  }         // End switch  

  return;
}

//%FS==================================================================
//
// USB_SCH_DoReport
//
// Description:
//   USB_SCH_DoReport reports the Scheduler information.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the structure USB_Trans_t sent to PE.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_DoReport( USB_Trans_t* p_trans ) {
  
  u_int32              expect_time = 0;
  u_int32              actual_time = 0;
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;

  if(p_trans == NULL) MSG_Panic("NULL pointer!\n");
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  p_pipe  = p_trans->p_pipe;

  expect_time = p_trans->expected_duration;
  actual_time = p_trans->time_completed - p_trans->time_first_attempted;



  MSG_Printf("\n");
  MSG_Printf("+--------------------------------------------------------------------------------\n");
  MSG_Printf("+ Scheduler info for the last transaction.\n");
  MSG_Printf("+--------------------------------------------------------------------------------\n");
  MSG_Printf("+     Current pipe id     =  %d\n", p_pipe->pipe_id);
  MSG_Printf("+     Current irp id      =  %d\n", p_trans->p_irp->irp_id);

  if(p_trans->trans_speed == USB_TRANS_HS) {
    MSG_Printf("+     Current microframe  =  %d\n", p_sched->uframe_count & 0x7ff);
  }
  else {
    MSG_Printf("+     Current frame       =  %d\n", p_sched->uframe_count & 0x7ff);
  }

  MSG_Printf("+     Frame time left     =  %.7d ns\n", p_sched->time_left);
  //  MSG_Printf("+     Time first started  =  %.7lld ns\n", p_trans->time_first_attempted );
  MSG_Printf("+     Time last started   =  %.7lld ns\n", p_trans->time_started );
  MSG_Printf("+     Time completed      =  %.7lld ns\n", p_trans->time_completed);
  MSG_Printf("+     Expected duration   =  %.7d ns\n", expect_time);
  MSG_Printf("+     Actual duration     =  %.7d ns\n", actual_time);
  USB_SCH_PrintTransState(p_trans);

}

//%FS==================================================================
//
// USB_SCH_SetList
//
// Description:
//   USB_SCH_SetList sets up periodic list and asynchronous list.
//
// Parameter:
//   USB_HostCtrlTop_t*  p_top  I
//     It points to structure USB_HostCtrlTop_t that stores all system
//     information.
//   USB_Scheduler_t*  p_sched  IO
//     It points to structure USB_Scheduler_t that stores
//     Scheduler information.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_SetList(USB_HostCtrlTop_t* p_top, USB_Scheduler_t* p_sched) {

  u_int32   pipe_id    = 0;
  u_int32*  p_head_new = NULL;
  USB_PipeDescr_t* p_pipe;
 
  p_sched->period_count = 0;
  p_sched->ctrl_count   = 0;
  p_sched->bulk_count   = 0;
  p_sched->period_list_size = 0;
  p_sched->ctrl_list_size   = 0;
  p_sched->bulk_list_size   = 0;

  for(pipe_id = 0; pipe_id < (p_top->sys_pipe_mgr.count); pipe_id++) {
  
    p_pipe = carbonXUsbGetPipeDescr(pipe_id);
    if(p_pipe == NULL) continue;  // must have been destroyed!
    
    switch (p_pipe->p_endpt_descr->endpt_type) {
  
      case USB_EPTYPE_ISO:
      case USB_EPTYPE_INTR: {

        if(p_sched->period_count >= p_sched->period_list_size) {

          //grow the size and the list.
          p_sched->period_list_size += USB_SCH_LIST_GROW_SIZE;
          p_head_new = (u_int32*)tbp_realloc(p_sched->p_periodic,
					     sizeof(u_int32) * (p_sched->period_list_size - USB_SCH_LIST_GROW_SIZE),
					     sizeof(u_int32) * p_sched->period_list_size);
    
          //Not enough memory
          if (p_head_new == NULL) {
            MSG_Panic("USB_SCH_GrowList(): Out of memory!\n");  
          }
              
          p_sched->p_periodic = p_head_new;
        }
        
        *(p_sched->p_periodic + p_sched->period_count) = pipe_id;
        p_sched->period_count ++;
      } break;

      case USB_EPTYPE_CTRL: {

        if(p_sched->ctrl_count >= p_sched->ctrl_list_size) {

          //grow the size and the list.
          p_sched->ctrl_list_size += USB_SCH_LIST_GROW_SIZE; 
          p_head_new = (u_int32*)tbp_realloc(p_sched->p_asyc_ctrl,
					 sizeof(u_int32) * (p_sched->ctrl_list_size - USB_SCH_LIST_GROW_SIZE),
					 sizeof(u_int32) * p_sched->ctrl_list_size);
    
          //Not enough memory
          if (p_head_new == NULL) {
            MSG_Panic("USB_SCH_GrowList(): Out of memory!\n");  
          }
              
          p_sched->p_asyc_ctrl = p_head_new;
        }
        
        *(p_sched->p_asyc_ctrl + p_sched->ctrl_count) = pipe_id;
        p_sched->ctrl_count ++;
      } break;
  
      case USB_EPTYPE_BULK: {

        if(p_sched->bulk_count >= p_sched->bulk_list_size) {

          //grow the size and the list.
          p_sched->bulk_list_size += USB_SCH_LIST_GROW_SIZE;
          p_head_new = (u_int32*)tbp_realloc(p_sched->p_asyc_bulk,
					 sizeof(u_int32) * (p_sched->bulk_list_size - USB_SCH_LIST_GROW_SIZE),
					 sizeof(u_int32) * p_sched->bulk_list_size);
    
          //Not enough memory
          if (p_head_new == NULL) {
            MSG_Panic("USB_SCH_GrowList(): Out of memory!\n");  
          }
              
          p_sched->p_asyc_bulk = p_head_new;
        }
        
        *(p_sched->p_asyc_bulk + p_sched->bulk_count) = pipe_id;
        p_sched->bulk_count ++;
      } break;
      
      default: break;
    }    
  }
}

//%FS==================================================================
//
// USB_SCH_IsIrpPending
//
// Description:
//   USB_SCH_IsIrpPending judges if there is IRP pending or not.
//   The following are the conditions, then there is no IRP
//   pending:
//   condition 1: Pipe state is READY or READY_NOTIME;
//   condition 2: Current level of IRP list is not zero.
//
// Parameter:
//   USB_HostCtrlTop_t*  p_top  I
//     It points to structure USB_HostCtrlTop_t that stores all system
//     information.
//
// Return value:
//   BOOL
//     The return value is TRUE to indicate there is IRP pending, 
//     FALSE to indicate not.
//
//%FE==================================================================
BOOL USB_SCH_IsIrpPending(USB_HostCtrlTop_t* p_top) {

  u_int32 pipe_id = 0;
  USB_PipeDescr_t* p_pipe;
  
  for(pipe_id = 0; pipe_id < (p_top->sys_pipe_mgr.count); pipe_id++) {
    
    p_pipe = carbonXUsbGetPipeDescr(pipe_id);
    if(p_pipe == NULL) continue; // must have been destroyed.
    
    if((p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY ||
        p_pipe->sys_record.pipe_state == USB_PIPE_STAT_READY_NOTIME) &&
       p_pipe->a_irp_uc.count != 0) {
      return TRUE;
    }
  }

  return FALSE;
}

//%FS==================================================================
//
// USB_SCH_EstimateDuration
//
// Description:
//   USB_SCH_EstimateDuration changes bit time to wall clock time
//   The duration can be expressed as  bits time, bus cycles, wall clock time
//   1. Bit times are being used in the Pipe and IRP module.
//   2. Bus cycles are being used in the BFM.
//   3. Wall clock time is used in the Scheduler module.
//
// Parameter:
//   BOOL  is_token  I
//     It indicates if it is a (Split or Ping) token packet or not.
//   u_int32  bytes  I
//     It indicates the number of non-overhead bytes.
//   USB_PipeDescr_t*  p_pipe  I
//     It points to the descriptor of the current pipe.
//   USB_Scheduler_t*  p_sched  I
//     It points to structure USB_Scheduler_t that stores
//     Scheduler information.
//
// Return value:
//   u_int32
//     The return value is duration in ns
//
//%FE==================================================================
u_int32 USB_SCH_EstimateDuration(BOOL is_token, u_int32 bytes,
  USB_PipeDescr_t* p_pipe, USB_Scheduler_t* p_sched) {
  
  u_int32        bit_time;
  u_int32        duration;
  USB_DevProfile_t*  p_dev;

  (void)p_sched;
  if(p_pipe == NULL) MSG_Panic("NULL pointer!\n");

  p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

  // Get bit time
  bit_time  = USB_CHK_CalcExpBitTime(is_token, bytes, p_pipe);

  // Convert to duration
  duration = USB_SCH_CvtBitTimeToDuration( p_dev->speed, bit_time );

  return (duration);
}

//%FS==================================================================
//
// USB_SCH_CvtBitTimeToDuration
//
// Description:
//   This function converts USB Bit Time into a wall clock time
//
// Parameter:
//   USB_TransSpeed_e    bus_speed   I
//   u_int32             bit_time    I
//
// Return value:
//   u_int32
//     The return duration in ns
//
//%FE==================================================================

u_int32 USB_SCH_CvtBitTimeToDuration ( USB_TransSpeed_e  bus_speed, u_int32 bit_time ) {
  u_int32        duration=0;

  // Calculate bus [clock] cycles
  switch ( bus_speed ) {
  case USB_TRANS_HS:
    duration = bit_time * 1000 / 480;
    // @@@fredieu - problem so temporarily add this
    //        duration = duration * 4;
    break;

  case USB_TRANS_FS:
    duration = bit_time * 1000 / 12;
    break;

  case USB_TRANS_LS:
    duration = bit_time * 10000 / 15;
    break;

  default:
    break;
    duration = 0;
  }

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( PLAT_GetHandleFromTransTable(),
		MSG_TRACE, "<%.10lld> USB_SCH_CvtBitTimeToDuration: duration=%d\n", PLAT_GetSimTime(), duration );
#endif

  return duration;
}


//%FS==================================================================
//
// USB_SCH_BuildSplitTransT
//
// Description:
//   USB_SCH_BuildSplitTransT builds USB_Trans_t for split
//   transactions, prepares for calling USB_PE_DoTransfer.
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the structure USB_Trans_t sent to PE.
//   USB_Data_t*  p_sar  I
//     It points to the segmented data.
//   
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_BuildSplitTransT( USB_Trans_t* p_trans, USB_Data_t* p_sar) {

  static u_int32 csplit_count = 0;   //@@@ Needs fix. Can't have static won't work in case of multiple instances
  
  u_int32 pid          = 0;
  u_int32 addr         = 0;
  u_int32 ept_num      = 0;
  u_int32 i            = 0; 
  u_int32 data_size    = 0;
  u_int8* p_data       = NULL;
  
  USB_Packet_t         split_pkt;
  USB_Data_t           split_data;
  USB_HostCtrlTop_t*   p_top;
  USB_PipeDescr_t*     p_pipe;
  USB_DevProfile_t*    p_dev;
 
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_pipe  = p_trans->p_pipe;
  p_dev   = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

  split_pkt.token_pkt[0]  = 0;
  split_pkt.token_pkt[1]  = 0;
  split_pkt.token_pkt[2]  = 0;
  split_pkt.token_pkt[3]  = 0;
      
  // Set hub port
  p_trans->hub_port = p_dev->port_num;
   
  if(p_pipe->sys_record.prev_trans_state == USB_DO_COMP_IMMED_NOW ||
     p_pipe->sys_record.prev_trans_state == USB_DO_COMPLETE ||
     p_pipe->sys_record.prev_trans_state == USB_DO_COMPLETE_IMMED ||
     p_pipe->sys_record.prev_trans_state == USB_DO_NEXT_COMPLETE) {
    // Set transaction type to be CSPLIT
    p_trans->trans_type = USB_TRANS_TYPE_CSPLIT;      
    csplit_count += 1;

    // Check if this is the last CSPLIT of the three
    if(csplit_count >= 3) {
      p_trans->is_last = TRUE;  
      csplit_count = 0;
    }
    else {
      p_trans->is_last = FALSE;  
    }           
  }
    
  else {
    // Set transaction type to be SSPLIT
    p_trans->trans_type = USB_TRANS_TYPE_SSPLIT;      
  }

  //
  // Build token packet
  //
  // Get token PID
  pid         = USB_BuildPid( USB_SCH_SetTransPid( p_trans ) );
  
  // Get device address
  addr = p_trans->dev_addr;
  
  // Get endpoint number   
  ept_num = p_pipe->p_endpt_descr->endpt_num;

  pid     <<= 24;
  addr    <<= 17;
  ept_num <<= 13;


  USB_CvtInt32ToByteAry( (pid<<24) | (addr<<17) | (ept_num<<13), split_pkt.token_pkt);     


  // Get data size
  data_size = (p_trans->trans_type == USB_TRANS_TYPE_SSPLIT) ? p_sar->size : 0;

  // Get data
  if(data_size != 0) {
    
    p_data = p_sar->p_data;

    for(i = 0;i < data_size; i++) {
      split_pkt.data_pkt[i] = *p_data;
      p_data += 1;
    }       // Build data packet
  }

  split_data.p_data = (u_int8*)&split_pkt;
  split_data.size   = data_size + 4;

  // Put data into structure USB_Trans_t
  memcpy(p_top->cp_sys_pe_buffer->p_data, split_data.p_data, split_data.size);
  p_top->cp_sys_pe_buffer->size = split_data.size;
  p_trans->p_data_segment     = p_top->cp_sys_pe_buffer;
//  p_trans->p_data_segment = &split_data;
}

//%FS==================================================================
//
// USB_SCH_SetTransPid
//
// Description:
//   USB_SCH_SetTransPid sets the PID of the token packet of
//   the current transaction.
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the structure USB_Trans_t sent to PE.
//
// Return value:
//   u_int8
//     The PID (in case of a Split transaction it is the PID after the
//     Split token).
//
//%FE==================================================================
USB_Pid_e USB_SCH_SetTransPid(USB_Trans_t* p_trans) {
  
  USB_Pid_e             pid_type = USB_PID_RESERVED;
  USB_PipeDescr_t*   p_pipe;
  USB_DevProfile_t*  p_dev;
  USB_HostCtrlTop_t*  p_top;

  p_pipe = p_trans->p_pipe;
  p_dev  = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();


  switch(p_pipe->a_irp_uc.p_head->p_irp->trans_type) {

  case USB_IRP_TYPE_IN:
    if(p_dev->is_split) {
      p_trans->pid = USB_PID_SPLIT;
      pid_type       = USB_PID_IN;
    }
    else {
      p_trans->pid = USB_PID_IN;
    }
    
    p_trans->p_data_segment->size = 0;
    break;

  case USB_IRP_TYPE_CTRL_IN:
    // Check if it is a Control SETUP transaction    
    if( p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_SETUP ) {
      
      if(p_dev->is_split) {
	p_trans->pid = USB_PID_SPLIT;
	pid_type     = USB_PID_SETUP;
      }
      else {
	p_trans->pid = USB_PID_SETUP;
      }
    }   
    // Check if it is a Control Data transaction    
    else if ( p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_IN) {
      if(p_dev->is_split) {
	p_trans->pid = USB_PID_SPLIT;
	pid_type     = USB_PID_IN;
      }
      else {
	p_trans->pid = USB_PID_IN;
      }
      
      p_trans->p_data_segment->size = 0;
    }
    // Check if it is a control STATUS transaction    
    else if( p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_STAT ) {
      if(p_dev->is_split) {
	p_trans->pid = USB_PID_SPLIT;
	pid_type       = USB_PID_OUT;
      }
      else {
	if(p_trans->trans_speed == USB_TRANS_HS &&
	   p_pipe->sys_record.prev_trans_state != USB_DO_OUT) {
	  p_trans->pid                  = USB_PID_PING;
	  p_trans->p_data_segment->size = 0;
	}
	else {
	  p_trans->pid = USB_PID_OUT;       
	}        
      }
    }
  break;
  
  case USB_IRP_TYPE_OUT:
    if(p_dev->is_split) {
      p_trans->pid = USB_PID_SPLIT;
      pid_type     = USB_PID_OUT;
    }
    else {
      // Check, whether we should send out a Ping or the OUT transaction
      if( p_trans->trans_speed == USB_TRANS_HS &&
	  p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_BULK  &&
	  p_pipe->sys_record.prev_trans_state != USB_DO_OUT &&
	  ( ( p_pipe->sys_record.response_pid != USB_PID_ACK ||
	      p_top->sys_sched.always_ping
	      )  && !p_top->sys_sched.never_ping
	    )
	  ) {
	p_trans->pid = USB_PID_PING;
	p_trans->p_data_segment->size = 0;
      }
      else {
	p_trans->pid = USB_PID_OUT;
      }
    }
    break;

  case USB_IRP_TYPE_CTRL_OUT:
    // Check if it is a Control Setup transaction    
    if (  p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_SETUP) {      
      if(p_dev->is_split) {
	p_trans->pid = USB_PID_SPLIT;
	pid_type     = USB_PID_SETUP;
      }
      else {
	p_trans->pid = USB_PID_SETUP;
      }
    }
    // Check if it is a Control Data transaction    
    else if ( p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_OUT) {
      if(p_dev->is_split) {
	p_trans->pid = USB_PID_SPLIT;
	pid_type     = USB_PID_OUT;
      }
      else {
	// Note: no Ping protocol!
	p_trans->pid = USB_PID_OUT;
      }
    }
    // Check if it is a control STATUS transaction    
    else if( p_pipe->a_irp_uc.p_head->p_irp->stage == USB_TRANS_STAGE_STAT) {
      if(p_dev->is_split) {
	p_trans->pid = USB_PID_SPLIT;
	pid_type     = USB_PID_IN;
      }
      else {
	p_trans->pid = USB_PID_IN;
      }
      
      p_trans->p_data_segment->size = 0;
    }
    break;
  } // end switch
  
  return (pid_type);
}

//%FS==================================================================
//
// USB_SCH_SetErrInjection
//
// Description:
//   USB_SCH_SetErrInjection gets err_mask information from
//   USB_Irp_t and sets USB_ErrInjection_t.
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the structure USB_Trans_t sent to PE.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_SetErrInjection(USB_Trans_t* p_trans) {

  // Check to inject a timeout
  p_trans->err_inject.do_timeout_err = ( (USB_ERR_TIMEOUT &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_TIMEOUT) ?
    TRUE : FALSE;  

  // Check to inject a bad CRC into specified packet
  p_trans->err_inject.do_crc_err = ( (USB_ERR_CRC &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_CRC) ?
    TRUE : FALSE;  

  // Check to do data corruption
  p_trans->err_inject.do_data_corrupt = ( (USB_ERR_DATA_CORRUPT &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_DATA_CORRUPT) ?
    TRUE : FALSE;

  // Check to drop some data
  p_trans->err_inject.do_data_drop = ( (USB_ERR_DATA_DROP &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_DATA_DROP) ?
    TRUE : FALSE;  

  // Check to inject bad PID check bits
  p_trans->err_inject.do_pid_err = ( (USB_ERR_PID &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_PID) ?
    TRUE : FALSE;  
            
  // Check, whether to inject a bad data part
  p_trans->err_inject.do_datapart_err = (
    ((USB_ERR_PART_ALL & p_trans->p_irp->irp_sys_record.err_mask)
     == USB_ERR_PART_ALL && p_trans->datapart == USB_PART_ALL) ||
    ((USB_ERR_PART_BEGIN & p_trans->p_irp->irp_sys_record.err_mask)
     == USB_ERR_PART_BEGIN && p_trans->datapart == USB_PART_BEGIN) ||
    ((USB_ERR_PART_MID & p_trans->p_irp->irp_sys_record.err_mask)
     == USB_ERR_PART_MID && p_trans->datapart == USB_PART_MID) ||
    ((USB_ERR_PART_END & p_trans->p_irp->irp_sys_record.err_mask)
     == USB_ERR_PART_END && p_trans->datapart == USB_PART_END)
    ) ? TRUE : FALSE;  

  // Check to inject error in split token phase
  p_trans->err_inject.is_split_phase = ( (USB_ERR_PHASE_SPLIT &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_PHASE_SPLIT) ?
    TRUE : FALSE;  

  // Check to inject error in token phase
  p_trans->err_inject.is_token_phase = ( (USB_ERR_PHASE_TOKEN &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_PHASE_TOKEN) ?
    TRUE : FALSE;  

  // Check to inject error in data phase
  p_trans->err_inject.is_data_phase = ( (USB_ERR_PHASE_DATA &
    p_trans->p_irp->irp_sys_record.err_mask) == USB_ERR_PHASE_DATA) ?
    TRUE : FALSE;  

  // Check to FAKE a received Timeout
  p_trans->err_inject.receive_timeout = p_trans->p_irp->irp_sys_record.err_mask == USB_ERR_RECEIVE_TIMEOUT ?
    TRUE : FALSE;  

  // Check to FAKE a received CRC Error
  p_trans->err_inject.receive_crc_err = p_trans->p_irp->irp_sys_record.err_mask == USB_ERR_RECEIVE_CRC ?
    TRUE : FALSE;  

  // Check to FAKE a received PID Error
  p_trans->err_inject.receive_pid_err = p_trans->p_irp->irp_sys_record.err_mask == USB_ERR_RECEIVE_PID ?
    TRUE : FALSE;  

}

//%FS==================================================================
//
// USB_SCH_RetryForTimeout
//
// Description:
//   USB_SCH_RetryForTimeout retries a transaction failed
//   because there is timeout.
//   If it is a high-speed, high bandwidth interrupt transaction,
//   the function must check if the maximum number of transactions
//   per microframe has been reached, if not, the HC may immediate
//   retry the transaction during the current microframe. Refer to
//   USB20 standard 5.9.1(p56). 
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the USB_Trans_t struct that needs to be retried.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_RetryForTimeout( USB_Trans_t*  p_trans ) {

  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  USB_DevProfile_t*    p_dev;
 
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  p_pipe  = p_trans->p_pipe;
  p_dev   = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);


  // Check maximum number of transactions per microframe of high-speed high
  // bandwidth interrupt or iso transfer.
  if( ( p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_INTR  ||
        p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO
      )
      && 
      p_dev->speed == USB_TRANS_HS ) {
    
    if(p_sched->hs_trans_count <= p_pipe->p_endpt_descr->hs_trans_num) {
      
      // Calculate the time remaining in this (micro-) for periodic traffic
      p_sched->time_left = p_sched->time_of_next_SOF - USB_MICROFRAME_TIME * 0.2 - PLAT_GetSimTime();

      if ( p_sched->time_left >= (int)p_trans->expected_duration ) {
        USB_SCH_ExecTrans( p_trans );          
      }
      p_sched->hs_trans_count ++;
    }   
    else {
      p_pipe->sys_record.uframe_sched =  p_sched->uframe_count + (1 << (p_pipe->p_endpt_descr->interval-1));
    }
  }
  
  else {    
    p_pipe->sys_record.uframe_sched = p_sched->uframe_count + 1;
  }
}

//%FS==================================================================
//
// USB_SCH_RetryCsImmed
//
// Description:
//   USB_SCH_RetryCsImmed retries a complete-split transaction
//   before doing a different transaction.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the structure USB_Trans_t sent to PE.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_RetryCsImmed( USB_Trans_t* p_trans ) {
  USB_HostCtrlTop_t*   p_top;
  USB_Scheduler_t*     p_sched;
  USB_PipeDescr_t*     p_pipe;
  USB_DevProfile_t*    p_dev;
  
  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  p_sched = & (p_top->sys_sched); 
  p_pipe = p_trans->p_pipe;
  p_dev = carbonXUsbGetDevProf(p_pipe->p_endpt_descr->dev_id);


  if ( p_sched->time_left >= (int)p_trans->expected_duration ) {
        USB_SCH_ExecTrans( p_trans );          
  }

  if( ( p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_INTR  ||
        p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO
      )
      && 
      p_dev->speed == USB_TRANS_HS ) {

    p_sched->hs_trans_count ++;
  }
}

//%FS==================================================================
//
// USB_SCH_PrintTransState
//
// Description:
//   USB_SCH_PrintTransState prints the next action and transaction response
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the structure USB_Trans_t transfered
//     between Scheduler and PE.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_PrintTransState(USB_Trans_t* p_trans) {

  MSG_Printf("+     Response      =  %s\n", carbonXUsbCvtPidToString( p_trans->response ) );
  MSG_Printf("+     Next action   =  %s\n", carbonXUsbCvtHcTransStateToString( p_trans->trans_state ) );
}

//%FS==================================================================
//
// USB_SCH_Clear
//
// Description:
//   USB_SCH_Clear resets members of USB_Scheduler_t to be the
//   default values.
//
// Parameter:
//   USB_Scheduler_t*  p_sched  I
//     It points to structure USB_Scheduler_t that stores
//     Scheduler information.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_Clear(USB_Scheduler_t*  p_sched) {

  p_sched->do_sof_err       = FALSE;
  p_sched->ip_core_ctrl     = 0;
  p_sched->always_ping          = 0;
  p_sched->never_ping           = 0;
  p_sched->use_up_micro_frame   = 1;

  p_sched->time_of_SOF          = 0;
  p_sched->time_of_next_SOF     = 0;
  p_sched->time_left            = 0;
  p_sched->period_count     = 0;
  p_sched->ctrl_count       = 0;
  p_sched->bulk_count       = 0;
  p_sched->period_list_size     = 0;
  p_sched->ctrl_list_size       = 0;
  p_sched->bulk_list_size       = 0;
  p_sched->asyc_ctrl_index_next = 0;
  p_sched->asyc_bulk_index_next = 0;
  p_sched->hs_trans_count       = 0;
  p_sched->uframe_count         = 0;
  p_sched->frame_cnt_begin      = 0;
  p_sched->frame_cnt_end        = 0;
  p_sched->sof_err_mask         = 0;
  p_sched->p_periodic   = NULL;
  p_sched->p_asyc_ctrl    = NULL;
  p_sched->p_asyc_bulk    = NULL;
}

//%FS==================================================================
//
// USB_SCH_Free
//
// Description:
//   USB_SCH_Free frees the memory for Scheduler use.
//
// Parameter:
//   USB_Scheduler_t*  p_sched  I
//     It points to structure USB_Scheduler_t that stores
//     Scheduler information.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_SCH_Free(USB_Scheduler_t*  p_sched) {

  /***  gcc-4.0 complains about the (void **) &(u_int32*)p_sched
   ***  gcc-3.4.3 is OK with it.
  
    tbp_free( (u_int32*)p_sched->p_periodic );
    tbp_free( (u_int32*)p_sched->p_asyc_ctrl );
    tbp_free( (u_int32*)p_sched->p_asyc_bulk );
  ****/

  u_int32* ptr;
  ptr  = (u_int32*)p_sched->p_periodic;  tbp_free( ptr );
  ptr  = (u_int32*)p_sched->p_asyc_ctrl; tbp_free( ptr );
  ptr  = (u_int32*)p_sched->p_asyc_bulk; tbp_free( ptr );

}

