
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//
//////////////////////////////////////////////////////////////////////////
//  U S B    2.0     H O S T    B U S   F U N C T I O N A L    M O D E L
//////////////////////////////////////////////////////////////////////////
//
//

`timescale 1ns/1ns

// include "carbonx_usb_define.v"

module carbonx_usb_top(Clock_60, 
                      Clock_30,
                      Clock_48,
                      Clock_6,
               
                      //UTMI signals
                      CLK,
                      Reset_UTMI,
                      XcvrSelect,
                      Attach,
                      TermSelect,
                      SuspendM,
                      LineState,
                      VcontrolLoadM,
                      Vcontrol,
                      VStatus,
                      OpMode,
                      DataIn0_7,
                      DataIn8_15,
                      TxValid,
                      TxValidH,
                      TxReady,
                      DataOut0_7,
                      DataOut8_15,
                      RxValidH,
                      RxValid,
                      RxActive,
                      RxError,
                      DataBus16_8,
                      Global_Reset_Out,
                      fast_negotiate
                                     
                     )/*synthesis syn_heir = "flatten,remove"*/;

//input
input                 Clock_60;       //input 60MHz clock
input                 Clock_30;       // input 30MHz
input                 Clock_48;       // input 48MHz
input                 Clock_6;        // input 6MHz


//input from UTMI standard interface
input                 Reset_UTMI;     //HW reset 
input                 XcvrSelect;     //select Transceiver
input                 TermSelect;     //Termination select
input                 SuspendM;       //Device suspend signal
input                 VcontrolLoadM;  //load Vcontrol 
input[3:0]            Vcontrol;       //vendor control
input[1:0]            OpMode;         //operation mode
input[7:0]            DataIn0_7;      //input low 8bit data bus
input[7:0]            DataIn8_15;     //input high 8bit data bus
input                 TxValid;        //DUT begin transfer data
input                 TxValidH;       //hign 8bit data bus valid
input                 DataBus16_8;    //bus select 

//output to UTMI standard interface
output                CLK;            //DUT  clock
output                Attach;
output[1:0]           LineState;      //line state
output[7:0]           VStatus;        //vendor status
output                TxReady;        //input data bus has valid data;
output[7:0]           DataOut0_7;     //outpu low 8bit data bus
output[7:0]           DataOut8_15;    //output high 8bit data bus
output                RxValid;        //output bus has valid data
output                RxValidH;       //the same as RxValid
output                RxActive;       //begin transfer  data to DUT
output                RxError;        //received data error
output                Global_Reset_Out;  // for use to drive an overall reset.  Output only!  Not used internally at all!
output                fast_negotiate;
   
`include "carbonx_usb_params.vh"

//XIF signals
parameter
                      BFMid = 1,      // Indicate my switch port ID: -1 for PLI mode                      
                      ClockId = 1,    // Indicate which clock transactor is using                      
                      DataWidth = 32, // Databus width: XIF_put_data, XIF_Get_data                      
                                      //(Note this needs work - only 32 or 64 will work)
   
//                      DataAdWid = 14, // Max is 15, min is 3 (2 for 64 bit DataWidth)
                      DataAdWid = 11, // Max is 15, min is 3 (2 for 64 bit DataWidth)

                      GCSwid    = USB_GCS_WIDTH,
                      PCSwid    = USB_PCS_WIDTH;

`protect
// carbon license crbn_vsp_exec_xtor_usb2 
//  
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//  12-Sep-2005    Jim Stein   Carbon Designs remodelling
//  10-Jan-2006    Jim Stein   converted ALL defines to parameters
//
//
// Update: 1) 073003: change BFM signals from XIF_get * and BFM_put * to sce*;
//         2) 073003: get rid of BFM_CS block's BFM_Read_Data signal;
//         3) 073003: change submodule's name from U1(U2,U3,..) to
//                    USB20_HOSTBFM_*(submodule name);
//         4) 081403: add a clock select module       
//
// Revision      : $Revision$
// Log           :
//               : Revision 1.3  2006/01/17 03:37:33  jstein
//               : Reviewed by:  John Hoglund
//               : Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
//               : Bugs Affected: DW02_prod_sum1 sign extension
//               : Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
//               : removed all `defines from xactor verilog (mostly usb)
//               : added test/xactors/usb20
//               :
//               : Revision 1.2  2005/11/21 23:15:21  jstein
//               : Reviewed by: Goran Knutson
//               : Tests Done: checkin / runlist -grid -xactors / xactor examples regress all
//               : Bugs Affected: Added xactor examples for spi3, utopia2
//               :                Updated enet_*, ahb, pci to use new carbonX calls.
//               :                Updated usb20 (with Goran's assistance) to fix multiple problems.
//               :                Updated runlist.bom to get runlist -grid -xactors to work on SGE
//               :                Changed Makefile.carbon.vsp xactor command to look in dir "."
//               :                Added log files to all test.run tests
//               :                changed malloc to carbonmem_malloc as much as possible.
//               :                changed Makefile.libs to search CARBON_LIB_LIST during xactor compiles.
//               :                Updated documentation, added to html dirs.
//               :
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.3  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.2  2005/09/29 23:53:43  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done:  checkin, xactor regression
//               : Bugs Affected: none
//               :  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
//               :  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
//               :  so they will not be seen by the customer
//               :  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.25  2004/09/15 18:41:47  fredieu
//               : Changes for 16 bit at 60MHz
//               :
//               : Revision 1.24  2004/09/14 19:07:25  fredieu
//               : Use the correct clocks
//               :
//               : Revision 1.23  2004/09/14 18:38:22  fredieu
//               : All 60 at 16 bit
//               :
//               : Revision 1.22  2004/09/13 18:41:2  4  zippelius
//               : Added 60 Mhz clock to Tx_DataOutReg
//               :
//               : Revision 1.21  2004/09/11 14:46:48  zippelius
//               : Re-wrote large portions of Tx_DataOutReg  code. Not yet fully working
//               :
//               : Revision 1.20  2004/09/09 16:53:15  fredieu
//               : Fix for 16 bits
//               :
//               : Revision 1.19  2004/09/08 19:42:03  zippelius
//               : Changed WriteEnd to LastData
//               :
//               : Revision 1.18  2004/09/03 13:54:19  zippelius
//               : Some minor changes
//               :
//               : Revision 1.17  2004/09/01 21:06:18  zippelius
//               : Fixed clock problem, when running 16 bit UTMI mode
//               :
//               : Revision 1.16  2004/08/18 15:14:07  fredieu
//               : Signals misconnected
//               :
//               : Revision 1.15  2004/08/05 02:22:37  fredieu
//               : fake a timeout from the dut
//               :
//               : Revision 1.14  2004/08/03 13:32:42  fredieu
//               : Allow response to Timeout
//               :
//               : Revision 1.13  2004/07/23 03:00:10  fredieu
//               : Put in an IPG before a packet
//               :
//               : Revision 1.12  2004/07/14 18:19:59  fredieu
//               : Some missing BFM_Put_... stuff
//               :
//               : Revision 1.11  2004/06/30 03:06:02  fredieu
//               : Fix to clear fast_negotiate bit.
//               : Fix of some syntax.
//               :
//               : Revision 1.10  2004/06/21 19:45:14  fredieu
//               : Add an external fast_negotiate signal.
//               :
//               : Revision 1.9  2004/06/18 22:16:28  fredieu
//               : Fix up reset.
//               :
//               : Revision 1.8  2004/06/04 18:13:03  fredieu
//               : Adjust some clocks.
//               :
//               : Revision 1.7  2004/05/06 20:58:54  zippelius
//               : Snap-shot check-in: more code clean-up and bug fixes
//               :
//               : Revision 1.6  2004/04/20 20:54:11  zippelius
//               : Losts of code clean-up, comment fixes
//               :
//               : Revision 1.5  2004/04/19 18:34:13  zippelius
//               : Fixed Synplicity synthesis errors
//               :
//               : Revision 1.4  2004/04/19 15:30:30  zippelius
//               : HC is now synthesizable with Synplicigy
//               :
//               : Revision 1.3  2004/04/15 14:50:55  zippelius
//               : Major code clean-ups. Upgraded USB IP core to latest version. Fixed call-backs.Added Error masking. Fixed LineState state machines.
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.1  2003/12/04 02:35:49  adam
//               : add SuspendM
//               :
//               : Revision 2.0  2003/09/27 07:39:10  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.2  2003/09/26 03:56:57  adam
//               : add load completetime when timeout
//               :
//               : Revision 1.1  2003/09/23 07:54:11  thor
//               : split into two dir
//               :
//               : Revision 1.17  2003/09/10 14:14:16  thor
//               : synthesis
//               :
//               : Revision 1.16  2003/09/10 07:58:33  thor
//               : add comment
//               :
//               : Revision 1.15  2003/09/09 09:45:29  thor
//               : change Config_Reg bits
//               :
//               : Revision 1.14  2003/09/05 02:00:35  thor
//               : add blank
//               :
//               : Revision 1.13  2003/09/03 09:07:49  thor
//               : add blank
//               :
//               : Revision 1.12  2003/09/03 03:38:13  thor
//               : get rid of unuse signal
//               :
//               : Revision 1.11  2003/09/01 13:35:48  thor
//               : add PID_SOF time
//               :
//               : Revision 1.10  2003/09/01 11:03:32  thor
//               : add PID_SOF transfer time
//               :
//               : Revision 1.9  2003/08/29 06:19:52  thor
//               : add blank
//               :
//               : Revision 1.8  2003/08/26 03:10:53  thor
//               : add TimeOut and TxValid
//               :
//               : Revision 1.7  2003/08/25 12:58:19  thor
//               : add submodule TimeOut signal
//               :
//               : Revision 1.6  2003/08/25 11:52:02  thor
//               : add LS_Attach/FS_Attach to top module
//               :
//               : Revision 1.5  2003/08/25 07:23:54  thor
//               : change Config_Reg/Reset_Reg/ExpectedDuration_Reg from reg to wire and change name
//               :
//               : Revision 1.4  2003/08/15 07:26:16  thor
//               : add BFM_xrun_Read
//               :
//               : Revision 1.3  2003/08/15 01:59:59  thor
//               : add Config to Opcode_Decode module
//               :
//               : Revision 1.2  2003/08/14 14:06:49  thor
//               : add header
//               :
`include "xif_core.vh"
 
wire                  Clock;          // BFM  clock
wire                  DUT_clock;      // DUT  clock
wire                  Reset;
wire                  BFM_Write_Data;
wire                  BFM_Read_Data;
wire[31:0]            Config;
wire [31:0]           Time_Counter;
wire                  CRCErr;
wire                  PIDErr;             
wire                  Highspeed_Handshake_Detect_Finish;
wire                  TimeOut;        // transfer time out 
wire                  TxDone;         //BFM receive data from XIF finish
wire                  RxDone;         //BFM receive data from DUT finish
wire                  Attach;         //a device attach
wire                  FullSpeed;
wire                  LowSpeed;
wire                  HighSpeed;      // working at highspeed
wire                  LS_Attach;      //a low speed device attach
wire                  BFM_Reset_Device;// device  do Highspeed Handshake Detecting
wire                  BFM_Suspend_Resume_Device;   // Suspend/Resume device signal
wire                  FS_Attach;      // after Highspeed Handshake Detect find the device is a FS only devcie4
wire                  SusReFinish;    //Device suspend/Resume finish;
wire[14:0]            BFM_gp_daddr_Read; //BFM_Read address
wire[14:0]            BFM_gp_daddr_Write; //BFM_Write address
wire                  BFM_xrun_Read;
wire                  BFM_xrun_Write;
wire                  PID_SOF;
wire                  LowSpeed_Contr;
wire                  TimeOut_Load;
wire[1:0]             Dev_Speed_pre;
wire                  Load_Dev_Speed;
wire                  TxReady_pre_gated;
wire          TxValid_gated;
wire          TxValidH_gated;
          

  assign                CLK          = DUT_clock;

  //assign                BFM_clock    = DUT_clock;
  assign                BFM_clock    = Clock_60;

  assign                BFM_reset    = Reset_UTMI;

  assign                BFM_gp_daddr = BFM_Write_Data ? BFM_gp_daddr_Write: (BFM_Read_Data ? BFM_gp_daddr_Read: 32'b0);

  assign                BFM_xrun     = BFM_xrun_Write|BFM_xrun_Read;

  //assign                Reset        = XIF_reset| Reset_UTMI;
  assign                Reset        = Reset_UTMI;
  assign     BFM_put_operation = BFM_NXTREQ;
//  assign     BFM_put_status    = 0; // Status always OK.
  assign    BFM_put_status  = {Time_Counter[28:0], CRCErr, PIDErr, TimeOut};
 
reg Global_Reset_Out;
reg [5:0] reset_count;
reg reset_detected;
reg fast_negotiate;

//reg config_ignore_response;
//reg ignore_response;
wire ignore_response;
//reg TxValid_r;

always @ (posedge CLK) begin
  Global_Reset_Out <= 1'b0;
  reset_detected <= XIF_get_csdata[GLOBAL_RESET_OUT_BIT];
  if(XIF_get_csdata[GLOBAL_RESET_OUT_BIT] & ~reset_detected) begin
      Global_Reset_Out <= 1'b1;
      reset_count <= 6'b0;
  end
  else if(reset_count < 6'h3f) begin
          Global_Reset_Out <= 1'b1;
          reset_count <= reset_count + 1'b1;
       end
end


always @ (posedge CLK) begin
  if(Reset) fast_negotiate <= 0;
  else if(XIF_get_csdata[FAST_NEGOTIATE_BIT]) fast_negotiate <= 1;
  else if(XIF_get_csdata[FAST_NEGOTIATE_BIT_CLR]) fast_negotiate <= 0;
end

/*
always @ (posedge Clock) begin
  if(Reset) begin
    config_ignore_response <= 0;
    ignore_response <= 0;
  end
  else begin
    config_ignore_response <= Config[IGNORE_RESPONSE];
    if((Config[IGNORE_RESPONSE]==1) && (config_ignore_response == 0))
      ignore_response <= 1;
//    else if(TimeOut || (Config[IGNORE_RESPONSE]==0)) ignore_response <= 0;
        else if(Config[IGNORE_RESPONSE]==0) ignore_response <= 0;
//    else if(TimeOut) ignore_response <= 0;
  end
end
*/

//  always @ (posedge Clock) TxValid_r <= TxValid;
  
  assign ignore_response = Config[IGNORE_RESPONSE];
  assign TxValid_gated = ignore_response ? 0 : TxValid;
  assign TxValidH_gated = ignore_response ? 0 : TxValidH;
//  assign TxReady = ignore_response ? TxValid_r : TxReady_pre_gated;
  assign TxReady = ignore_response ? TxValid : TxReady_pre_gated;

   
carbonx_usb_confstatus ConfStatus(
                      .Clock(Clock_60),
                      .Clock_60(Clock_60),
                      .Reset(Reset),
              
                      .XIF_get_csdata(XIF_get_csdata),
                      .BFM_put_csdata(BFM_put_csdata),
                      .BFM_Write_Data(BFM_Write_Data),
              
                      .Config(Config),
                      .Time_Counter(Time_Counter),
                      .FS_Attach(FS_Attach),
                      .CRCErr(CRCErr),
                      .PIDErr(PIDErr),
                      .Highspeed_Handshake_Detect_Finish(Highspeed_Handshake_Detect_Finish),
                      .TimeOut(TimeOut),
                      .TimeOut_Load(TimeOut_Load),
                      .RxValid(RxValid),
                      .TxReady(TxReady_pre_gated),
              
                      .RxDone(RxDone),
                      .Attach(Attach),
                      .LS_Attach(LS_Attach),
                      .FullSpeed(FullSpeed),
                      .LowSpeed(LowSpeed),
                      .HighSpeed(HighSpeed),
                      .XcvrSelect(XcvrSelect),
                      .TermSelect(TermSelect),
                      .DataBus16_8(DataBus16_8),
//                      .TxValid(TxValid_gated),
                      .TxValid(TxValid_Adjusted),

                      .PID_SOF(PID_SOF),
                      .Dev_Speed_pre(Dev_Speed_pre),
                      .Load_Dev_Speed(Load_Dev_Speed),
              
                      .VStatus(VStatus),
                      .VcontrolLoadM(VcontrolLoadM),
                      .Vcontrol(Vcontrol),
//                      .Reset_counter(Reset_counter),
                      .SuspendM(SuspendM));
              
carbonx_usb_rxdatainreg   Rx_DataInReg(
                      .Clock(Clock_60),   // must always be Clock_60
//                      .Clock_60(Clock_60),
                      .Clock_30(Clock_30),
                      .Reset(Reset),
                         
                      //UTMI interface 
                      .TxValid(TxValid_gated),
                      .TxValidH(TxValidH_gated),
                      .TxValid_out(TxValid_Adjusted),  // 8 bit format
                      .TxReady(TxReady_pre_gated),
                      .DataBus16_8(DataBus16_8),
                      .DataIn0_7(DataIn0_7),
                      .DataIn8_15(DataIn8_15),
                      .OpMode(OpMode),
                        
                      //XIF_core interface
                      .BFM_put_data(BFM_put_data),
                      .BFM_put_size(BFM_put_size),
                      .BFM_put_dwe(BFM_put_dwe),
                      .BFM_gp_daddr(BFM_gp_daddr_Read),
                      .BFM_xrun(BFM_xrun_Read),
                        
                      //internal block interface Rise_TxValid, 
                      .BFM_Read_Data(BFM_Read_Data), 
                      .PIDErr(PIDErr), 
                      .CRCErr(CRCErr), 
                      .RxDone(RxDone),
                      .TimeOut(TimeOut),
                      .Config(Config));
                         
carbonx_usb_devicelinestate DeviceLineState(
                      .Clock(Clock_60), 
                      .Reset(Reset),
                      .rst_UTMI(Reset_UTMI),
                      .Attach(Attach),
                      .LS_Attach(LS_Attach),
                      .LowSpeed(LowSpeed),
                      .FullSpeed(FullSpeed),
                      .HighSpeed(HighSpeed),
                           
                      // From BFM_Opcode block 
                      .BFM_Reset_Device(BFM_Reset_Device),
                      .BFM_Suspend_Resume_Device(BFM_Suspend_Resume_Device),
                           
                      // From UTMI interface
                      .SuspendM(SuspendM),
                      .OpMode(OpMode),
//                      .TxValid(TxValid_gated),
                      .TxValid(TxValid_Adjusted),

                      //to UTMI interface                           
                      .LineState(LineState),
                                                      
                      //to SW
                      .FS_Attach(FS_Attach),
                      .Highspeed_Handshake_Detect_Finish(Highspeed_Handshake_Detect_Finish), 
                      .Dev_Speed_pre(Dev_Speed_pre),
                      .Load_Dev_Speed(Load_Dev_Speed),
                           
                      .SusReFinish(SusReFinish),
                      .fast_negotiate(fast_negotiate),
                      .squelch_linestate(Config[SQUELCH_LINESTATE]));
                           
carbonx_usb_txdataoutreg   Tx_DataOutReg(
                      .Clock(Clock),
                      .Clock_60(Clock_60),
                      .Reset(Reset),
                          
                      .XIF_get_data(XIF_get_data),
                      .XIF_get_size(XIF_get_size),
                      .XIF_xav(XIF_xav),
                      .BFM_Write_Data(BFM_Write_Data),
                      .BFM_xrun(BFM_xrun_Write),
                      .BFM_gp_daddr(BFM_gp_daddr_Write),
                          
                      .DataBus16_8(DataBus16_8),
                      .RxActive(RxActive),
                      .RxValid(RxValid),
                      .RxValidH(RxValidH),
                      .RxError(RxError),
                      .DataOut0_7(DataOut0_7),
                      .DataOut8_15(DataOut8_15),
                      .PID_SOF(PID_SOF),
                      .LowSpeed_Contr(LowSpeed_Contr),    
                      .TxDone(TxDone),
                      .LastData(LastData),
                      .Config(Config));
                                                   
carbonx_usb_opcode_decode  Opcode_Decode(
                      .Clock(Clock_60),
//                      .Clock(Clock), 
                      .Reset(Reset),
                      .XIF_xav(XIF_xav), 
                      .XIF_get_operation(XIF_get_operation),
                      .XIF_get_address(XIF_get_address),
                      .Config(Config), 
                      .BFM_Reset_Device(BFM_Reset_Device), 
        
                      .BFM_Suspend_Resume_Device(BFM_Suspend_Resume_Device), 
                      .BFM_Read_Data(BFM_Read_Data), 
                      .BFM_Write_Data(BFM_Write_Data),
                         
                      .Highspeed_Handshake_Detect_Finish(Highspeed_Handshake_Detect_Finish), 
                      .ReadEnd(RxDone), 
                      .TimeOut(TimeOut),
                      .LastData(LastData), 
                      .SusReFinish(SusReFinish));                
                         
carbonx_usb_timeout  Trans_TimeOut(
//                       .Clock(Clock),
                       .Clock(Clock_60),
                       .Reset(Reset), 
                       .LowSpeed(LowSpeed),
                       .TxValid(TxValid_Adjusted),
//                       .TxValid(TxValid_gated),
                       .LastData(LastData),
                       .PIDErr(PIDErr),
                       .Trans_TimeOut(TimeOut),
                       .TimeOut_Load(TimeOut_Load),
                       .LS_Attach(LS_Attach),
                       .FS_Attach(FS_Attach),
//                       .DataBus16_8(DataBus16_8));
                       .DataBus16_8(1'b0));
               
carbonx_usb_clocksel Clock_Select(
                       .Clock_60(Clock_60),
                       .Clock_30(Clock_30),
                       .Clock_48(Clock_48), 
                       .Clock_6(Clock_6),
                                 
                       .Reset(Reset),

                       .LS_Attach(LS_Attach),
                       .FS_Attach(FS_Attach),
                       .SuspendM(SuspendM),
                       .DataBus16_8(DataBus16_8),
                       .BFM_Write_Data(BFM_Write_Data),
//                       .Config_bit2(Config[2]),
//                       .Config_bit11(Config[11]),
                       .Config_bit2(Config[PREAMBLE_BIT]),
                       .Config_bit11(Config[REMOVE_BIT]),
                       .LowSpeed_Contr(LowSpeed_Contr),
                       .Clock(Clock),
                       .DUT_clock(DUT_clock));




endmodule

