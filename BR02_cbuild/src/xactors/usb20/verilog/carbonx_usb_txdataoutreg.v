///////////////////////////////////////////////////////////////////////////////
//
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
//
// Description:
//  This module receives 32 bit chunks of data from XIF_core, does CRC5 and CRC16 generation and
//  sends data and crc to 16 bit output register (DataOut_Reg). From there data is sent out over 8bits/
//  16bits data bus towards the USB IP core.
//
// Notes
//  #1 delays have been inserted to make waveforms more readable in simulation.
//  They will be ignored by the synthesis tool 
//
// Known limitations:
//  Split transactions are not properly supported
//  Preamble bytes for Low Speed transfers are also not poperly supported
//
//////////////////////////////////////////////////////////////////////////////////


`timescale 1ns/1ns

module carbonx_usb_txdataoutreg (
                                    Clock,
                                    Clock_60,
                                    Reset,
                          
                                    XIF_get_data,
                                    XIF_get_size,
                                    XIF_xav,
                                    BFM_Write_Data,
                                    BFM_xrun,
                                    BFM_gp_daddr,
                                    
                                    DataBus16_8,
                                    RxActive,
                                    RxValid,
                                    RxValidH,
                                    RxError,
                                    DataOut0_7,
                                    DataOut8_15,
                                    
                                    TxDone,
                                    LastData,
                                    Config, 
                                    PID_SOF,
                                    LowSpeed_Contr
                                    );

// Input signals
  input         Clock;            // System clock
  input         Clock_60;         // 60 MHz clock
  input         Reset;            // Reset signal
  input         XIF_xav;          // Transaction available signal
  input         BFM_Write_Data;   // Indicates, if XIF core is writing data to BFM
  input [31:0]  XIF_get_data;     // Transaction data  (received from XIF core)
  input [31:0]  XIF_get_size;     // Transaction size in bytes (received from XIF core)
  input         DataBus16_8;      // Selects between 16 (when "1") and 8 (when "0") bit UTMI data bus
  input [31:0]  Config;           // Configuration register


// Output signals
  output        BFM_xrun;       // BFM running
  output [14:0] BFM_gp_daddr;   // BFM output address
  output        RxActive;       // UTMI output signal
  output        RxValid;        // DataOut_Reg[7:0] has valid data 
  output        RxValidH;       // DataOut_Reg[15:8] has valid data
  output        RxError;        // data error
  output [7:0]  DataOut0_7;     // UTMI output (lower 8bits of output data)
  reg    [7:0]  DataOut0_7;     // UTMI output (lower 8bits of output data)
  output [7:0]  DataOut8_15;    // UTMI output higher 8bits of output data)
  reg    [7:0]  DataOut8_15;    // UTMI output higher 8bits of output data)
  output        TxDone;         // Done with transaction 
  output        LastData;       // Last data chunk received from XIF core
  reg           LastData;
  output        PID_SOF;        // Sending out a SOF packet
  output        LowSpeed_Contr; // Lowspeed control transfer

   
// Internal wires and registers
  reg           RxValid_pre;              // Low 8bits output bus the next clock cycle has valid output data
  reg           RxValidH_pre;             // High 8bits output bus the next clock cycle has valid output data
  wire          RxActive_tmp;
  reg           RxValid_tmp;             
  reg           RxValidH_tmp;
  reg           RxActive;
  reg           RxValid;             
  reg           RxValidH;
  wire          RxValid_enable;
  reg           BFM_xrun;
  reg           BFM_xrun_r;
  wire          Start_of_Trans;           // rising edge of BFM_Write_Data marks start of transaction 
  reg           Start_of_Trans_r;         // Start_of_Trans delayed by one clock cycle
  reg           LastData_h;       
        

// Tx_Reg related signals
  reg           BFM_Write_Data_r;         // BFM_Write_Data delayed by one clock cycle

// Counters
  reg [10:0]    byte_count;                // The maximum transaction size is 1024 payload bytes + PID + CRC16
  reg [14:0]    gp_daddr;                  // gp_daddr counter

// Signals for specific tokens
  wire          PID_ACK;
  wire          PID_NAK;
  wire          PID_IN;
  wire          PID_OUT;
  wire          PID_SOF;
  wire          PID_SETUP;
  wire          PID_PING;
  wire          PID_SPLIT;
  wire          PID_DATAx;

  wire          PID_handshake;
  reg           Handshake_xaction_h;// 8 bit ACK or NAK Handshake transaction 

  wire          PID_token;        // Token transaction with 19 bit (PID, ADDR, EP) + 5 bit CRC
                                  //   PID_IN, PID_OUT, PID_SOF, PID_SETUP, PID_PING 
  reg           Token_xaction_h;  // Set to 1, when a PID_token is detected and then stays high
                                  // until the end of the transaction
                                  // It will be cleared by  TxDone clear
  reg           DATAx_xaction_h;  // Set to 1 during a DATAx transaction
  reg           Split_xaction_h;  // Set to 1 during a SPLIT transaction

 
// DataOut_Reg signals
  wire [15:0]   DataOut_Reg_Feed; // Holds data received from tx_data, feeds into DataOut_Reg.
  reg  [15:0]   DataOut_Reg;      // Holds output data; directly feeds DataOut0_7 and DataOut8_15
  reg  [31:0]   tx_data;                   // Transmit data register
  wire [ 7:0]   data_out_l;
  wire [ 7:0]   data_out_h;
  
// transfer control signals
  //   
  reg           TxDone_Handshake; // Asserted, when finishing a  Handshake transaction
  reg           TxDone_Token;     // Asserted, when finishing a token transaction
  reg           TxDone_CRC16;     // Asserted, when finishing CRC16 (only applicable to data transactions)
  

// CRC control signals
  wire [ 7:0]   crc16_data_in;
  wire [ 7:0]   crc16_data_in_rev_order;
  reg  [15:0]   crc16_in;
  wire [15:0]   crc16_out_tmp;
  wire [15:0]   crc16_out;
  reg           crc16_insertion;

  reg [10:0]    crc5_data_in;
  wire [10:0]   crc5_data_in_rev_order;
  wire [ 4:0]   crc5_out_tmp;
  wire [ 4:0]   crc5_out;

//  reg [18:0]  CRC5_SPLIT_In;     // SPLIT token do crc5 input data;
//  wire [4:0]  CRC5_SPLIT_Out;    // SPLIT token crc5 output data;
//  wire [4:0]  crc5_SPLIT_out;    // SPLIT CRC5 first out

 `include "carbonx_usb_params.vh"        

//
// Detect the start of a new transaction
//
  always @(posedge Clock_60)
    begin 
      if(Reset | TxDone )
        BFM_Write_Data_r <= #1 0;
      else
        BFM_Write_Data_r <= #1 BFM_Write_Data;
    end // always @ (posedge Clock_60)
  
  assign Start_of_Trans = BFM_Write_Data & (!BFM_Write_Data_r);

  always @(posedge Clock_60)
    begin 
      if(Reset)
        Start_of_Trans_r <= #1 0;
      else 
        Start_of_Trans_r <= #1 Start_of_Trans;
    end

  
// When LowSpeed device does a Control transfer, it must use fullspeed signalling.
// //  In that case we need to change the clock rate
  assign LowSpeed_Contr = 0; //@@@RZ currently not supported
 

// 
// Decode PID
//   
  assign PID_ACK    = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_ACK);
  assign PID_NAK    = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_NACK);

  assign PID_handshake = (PID_ACK | PID_NAK);

  assign PID_IN     = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_IN);                   
  assign PID_OUT    = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_OUT);                  
  assign PID_SOF    = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_SOF);
  assign PID_SETUP  = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_SETUP);
  assign PID_PING   = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_PING);
  assign PID_SPLIT  = Start_of_Trans_r & (XIF_get_data[27:24] == USB_BFM_PID_SPLIT);                                                

  assign PID_token = PID_IN | PID_OUT | PID_SOF | PID_SETUP | PID_PING;

// The PID of DATAx equal binary  --11
// The PID of DATA0 equals binary 0011
  assign PID_DATAx  = Start_of_Trans_r & ( (XIF_get_data[27:24] & USB_BFM_PID_DATA0) == USB_BFM_PID_DATA0);


//
// Assert xaction-in-progress signals  
  always @(posedge Clock_60)
    begin 
      if(Reset | TxDone )
        Handshake_xaction_h <= #1 0;
      else if(PID_handshake)
        Handshake_xaction_h <= #1 1;
    end

  
  always @(posedge Clock_60)
    begin 
      if(Reset | TxDone )
        Token_xaction_h <= #1 0;
      else if(PID_token)
        Token_xaction_h <= #1 1;
    end

  always @(posedge Clock_60)
    begin 
      if(Reset | TxDone)
        DATAx_xaction_h <= #1 0;
      else if(PID_DATAx)
        DATAx_xaction_h <= #1 1;
    end
   
  always @(posedge Clock_60)
    begin 
      if(Reset | TxDone)
        Split_xaction_h <= #1 0;
      else if(PID_SPLIT)
        Split_xaction_h <= #1 1;
    end


  //
  // The following section is the core of the Tx module
  // Load a 32 bit transaction word from XIF_get_data into tx_data every 4 cycles [of the 60 MHz clock].
  // During the other 3 cycles, the left-most byte of tx_data gets shifted out
  //  and the left-most byte of the next transaction word gets shifted into the right-most byte position
  //  of tx_data
  // At appropriate points in time we insert CRC5 or CRC16 data.
  always @(posedge Clock_60)
    begin      
      if ( BFM_xrun )
        begin
          if ( byte_count[2:0] == 3'b0 )
            begin
              crc5_data_in <= #1 {XIF_get_data[16:13], XIF_get_data[23:17]} ;
            end

	  if ( PID_handshake | Handshake_xaction_h )
	    begin
	      tx_data <= #1 XIF_get_data;
	    end	  
          else if ( (byte_count == XIF_get_size) && !PID_handshake )
            begin
              // Insert CRC16 after the last payload data byte

              // When Config[CORRUPTDATA_BIT] = 1, we corrupt data by flipping 1 bit. 
              tx_data[31:0] <= #1  Config[CORRUPTDATA_BIT] ? {tx_data[23:17], ~tx_data[16], crc16_out, 8'b0 } 
                               : {tx_data[23:16], crc16_out, 8'b0 };
              crc16_insertion <= #1 1;

            end // if ( (byte_count == XIF_get_size) && !PID_handshake )
          else
            begin
              if( (byte_count[1:0] == 2'b1) && !crc16_insertion )
                begin     
                  //  Load data
                  if ( PID_token )
                    // In case of a Token transaction we need to insert the CRC5 into the third byte
                    // When we load the data, we also need to be some bit reordering due to the fact that
                    // [token] transactions get assembled on the C-side with the LSB (Bit 0), being the right most bit
                    // The 32 bit word in XIF_get_data is organized like this:
                    //     PID<7:0> Addr<6:0> Endp<3:0> 5'b0 8'b0
                    // Ultimately the transaction will be sent out LSB first (by bit swapping each byte), so we need to
                    // convert the 4 byte XIF_get_data into this:
                    //    PID<7:0> Endp<0>Addr<6:0> CRC5<4:0>Endp<3:1> 8'b0
                    if(Config[CORRUPT_PID])
                      tx_data <= #1 { (XIF_get_data[31:24] ^ 8'h1f), XIF_get_data[13], XIF_get_data[23:17], crc5_out[4:0], XIF_get_data[16:14], 8'b0 };
                    else
                      tx_data <= #1 { XIF_get_data[31:24], XIF_get_data[13], XIF_get_data[23:17], crc5_out[4:0], XIF_get_data[16:14], 8'b0 };
                  else
                    tx_data <= #1 XIF_get_data;
                end // if ( byte_count[1:0] == 2'b1 )
              else if ( ( DATAx_xaction_h | Token_xaction_h ) & RxValid_enable)
                begin
                  // Shift
                  tx_data[31:0] <= #1 {tx_data[23:0], XIF_get_data[31:24]};       
                end // if ( ( DATAx_xaction_h | Handshake_xaction_h | Token_xaction_h ) & RxValid_enable)
            end // else: !if( (byte_count == XIF_get_size) && !PID_handshake )
        end // if ( BFM_xrun )
      else
        begin
          crc16_insertion <= #1 0;
          tx_data <= #1 32'b0;
        end // else: !if( BFM_xrun )
          
      //
      //  Handle byte_count and gp_daddr
      if ( Reset || LastData_h || !BFM_xrun )
        begin
          gp_daddr <= #1 0;
          byte_count <= #1 0;
        end // if ( Reset || LastData_h || !BFM_xrun )
      else
        begin
          // Handle counter and gp_daddr incrementing
          if ( byte_count[1:0] == 2'b0 )
            begin
              // Increment gp_daddr every 4 cycles
              gp_daddr <= #1 gp_daddr +1;
            end // if ( byte_count[1:0] == 2'b0 )

          if ( RxValid_enable )
            begin
              byte_count <= #1 byte_count +1;
            end // if ( RxValid_enable )
          
        end // else: !if( Reset || LastData_h || !BFM_xrun )      
    end

  //
  // TxDone signaling
  always @(posedge Clock_60)
    begin
      if ( BFM_xrun | BFM_xrun_r )
	begin
	  if ( Handshake_xaction_h )
	    begin
	      TxDone_Handshake <= #1 1;
	    end

          if ( Token_xaction_h )
            begin
              if ( byte_count == XIF_get_size )
                begin
                  TxDone_Token <= #1 1;
                end // if ( byte_count == XIF_get_size )
              else if ( byte_count == XIF_get_size+1)
                begin
                  TxDone_Token <= #1 0;
                end
            end // if ( Token_xaction_h )
          
          if ( byte_count == XIF_get_size+2+(DataBus16_8 & XIF_get_size[0]) )
            begin
              TxDone_CRC16 <= #1 1;
            end
        end // if ( BFM_xrun | BFM_xrun_r )
      else
        begin
          TxDone_CRC16 <= #1 0;
          TxDone_Token <= #1 0;
          TxDone_Handshake <= #1 0;
        end // else: !if( BFM_xrun | BFM_xrun_r )
    end // always @ (posedge Clock_60)
        

  assign data_out_l = tx_data[31:24];
  assign data_out_h = tx_data[23:16];

  // CRC16 does not include the PID byte, that why we feed the second byte of the tx_data shift register into
  // the CRC16 calculator 
  assign crc16_data_in = tx_data[23:16];
  
  assign DataOut_Reg_Feed[15:0] = (Handshake_xaction_h |  Token_xaction_h | DATAx_xaction_h |  Split_xaction_h ) ?
                                     {data_out_h, data_out_l} : 16'b0;


  assign BFM_gp_daddr = (TxDone || Reset) ? 15'b0 : gp_daddr;
  
  // crc16_in initialization and result feed-back
  always @(posedge Clock_60)
    begin
      if(Reset)
        crc16_in <= #1 16'b0;     
      else if( PID_DATAx & byte_count[0] )  // & byte_count[0] qualifier required for 16 bit bus mode
        crc16_in <= #1 16'hffff;
      else if( DATAx_xaction_h )
        crc16_in <= #1 crc16_out_tmp;
    end

  assign crc16_data_in_rev_order = {crc16_data_in[0], crc16_data_in[1],crc16_data_in[2],crc16_data_in[3],
                         crc16_data_in[4],crc16_data_in[5],crc16_data_in[6],crc16_data_in[7]};   

  carbonx_usb_crc16 
    CRC16_calc(
                   .CRC_DIN(  crc16_in ),
                   .DataIn(   crc16_data_in_rev_order ),
                   .CRC_DOUT( crc16_out_tmp ) 
                   );

  // Byte swap, bit swap and invert crc16_out_tmp
  assign crc16_out[15:0] = ~{ crc16_out_tmp[ 8], crc16_out_tmp[ 9], crc16_out_tmp[10], crc16_out_tmp[11], 
                              crc16_out_tmp[12], crc16_out_tmp[13], crc16_out_tmp[14], crc16_out_tmp[15],
                              crc16_out_tmp[ 0], crc16_out_tmp[ 1], crc16_out_tmp[ 2], crc16_out_tmp[ 3],
                              crc16_out_tmp[ 4], crc16_out_tmp[ 5], crc16_out_tmp[ 6], crc16_out_tmp[ 7]
                             };
  

  
// Begin CRC5 Generation block
////////////////////////////////////////////////////

  assign crc5_data_in_rev_order = {crc5_data_in[0], crc5_data_in[1], crc5_data_in[2], crc5_data_in[3],
                                   crc5_data_in[4], crc5_data_in[5], crc5_data_in[6], crc5_data_in[7],
                                   crc5_data_in[8], crc5_data_in[9], crc5_data_in[10]
                                   };
                           
  carbonx_usb_crc5
    CRC5_calc (.CRC_DIN(  5'h1f ),
                  .DataIN(   crc5_data_in_rev_order ), 
                  .CRC_DOUT( crc5_out_tmp )
                  );


  assign  crc5_out[4:0] = ~{crc5_out_tmp[0], crc5_out_tmp[1], crc5_out_tmp[2], crc5_out_tmp[3], crc5_out_tmp[4]};
  
  


  carbonx_usb_txtrafficthrottle Tx_traffic_throttle (
                                                         .allow_stalls((BFM_xrun_r || BFM_xrun) && Config[ENABLE_TX_STALLS]),
                                                         .Clock(Clock_60),
                                                         .Reset(Reset),
                                                         .run(RxValid_enable)
                                                         );
   

  //
  // Assign DataOut_Reg_Feed to DataOut_Reg, handling one of several error insertion cases
  // 
  always @(posedge Clock_60)
    begin 
      if(Reset)
        DataOut_Reg <= #1 16'b0;
      
      else if ( !DataBus16_8 || ( DataBus16_8 && byte_count[0] == 1'b0 && !TxDone ) )
        begin
          
          if(Config[PREAMBLE_BIT] & XIF_xav)
            DataOut_Reg <= #1 {8'b0, 8'h3c};
          
          else if( Config[DROPPKT_BIT] | ((PID_DATAx | DATAx_xaction_h) & Config[TIMEOUTERR_BIT]) )
            // Handle Drop packet and Time Out errors by blanking out data.
            //  Note: This does not change RxValid RxActive signalling.
            DataOut_Reg <= #1 16'b0;
          
          else if( Split_xaction_h & Config[CRCERR_BIT] & Config[SPLITTOKENERR_BIT] & (byte_count[3:0] == 1) )
            DataOut_Reg <= #1 {DataOut_Reg_Feed[15:1], ~DataOut_Reg_Feed[0]};
    
           else if( Token_xaction_h & Config[CRCERR_BIT] & Config[TOKENERR_BIT]  & (byte_count[3:0] == 3) )
             DataOut_Reg <= #1 {DataOut_Reg_Feed[15:1], ~DataOut_Reg_Feed[0]};
    
           else if( (PID_DATAx | DATAx_xaction_h) & Config[CRCERR_BIT] & Config[DATAERR_BIT] & LastData )
             DataOut_Reg <= #1 {DataOut_Reg_Feed[15:1], ~DataOut_Reg_Feed[0]};
    
           else
             // If there is no error injection necessary, we do a simple assignment
             DataOut_Reg <= #1 DataOut_Reg_Feed;
        end // if ( !DataBus16_8 || ( DataBus16_8 && byte_count[0] == 1'b0 && !TxDone ) )
    end



// Handle RxValid and LastData signals
  always @( DataBus16_8 or Handshake_xaction_h or Token_xaction_h or  DATAx_xaction_h or Split_xaction_h or byte_count[0] or
            TxDone_CRC16 or RxValid_enable or PID_SPLIT or XIF_get_size or LastData_h)
         
    begin
      RxValid_pre               = 0;
      RxValidH_pre              = 0;
      LastData                  = 0;
      
      if(  Handshake_xaction_h ) 
        begin                                                           
          RxValid_pre             = RxValid_enable;
          LastData                = RxValid_enable;
        end 
      else if(  Token_xaction_h )
        // 3 byte Token transaction (or PING)
        begin
          if ( byte_count[3:0] < (5'h5 + DataBus16_8) ) RxValid_pre  = RxValid_enable;
          if ( (byte_count[3:0] < 4'h4) && DataBus16_8 ) RxValidH_pre  = RxValid_enable;
          
          if ( byte_count == XIF_get_size ) LastData = RxValid_enable;                 
      end
    
      else if(PID_SPLIT | Split_xaction_h)
        // Send 8 bit PID + 19 bit header bits + 5 bit CRC (for 19 bit header)
        begin
          // TBD
        end // if (PID_SPLIT | Split_xaction_h)
      
      else if( DATAx_xaction_h)
        begin
          RxValid_pre  = 1;
          if ( DataBus16_8 )
            begin
              // Determine RxValidH for the last data phase, which sends out the CRC16
              if ( !LastData_h | (LastData_h & ~XIF_get_size[0]) )
                RxValidH_pre  = RxValid_enable;
            end
          
          if ( byte_count == XIF_get_size+1+DataBus16_8 ) LastData = 1;
        end // if ( DATAx_xaction_h)
    end // always @ ( DataBus16_8 or Handshake_xaction_h or Token_xaction_h or  DATAx_xaction_h or Split_xaction_h or byte_count[0] or...
  
  always @(posedge Clock_60)
    begin
      if(Reset | TxDone )
        LastData_h <= #1 0;
      else if(LastData)
        LastData_h <=1;
    end


  assign TxDone = TxDone_Handshake | TxDone_Token | TxDone_CRC16;



// Generate RxValid, RxValidH, BFM_xrun signals
// When Config[PREAMBLE_BIT]=1, then at the begining of each transaction we need to add a preamble
// token before the packet that needs to be sent. We must assert  RxValid and RxValidH to send it to DUT.
// When data is dropped, we need not send data and RxValid/RxValidH remains low.
// When data shall be corrupted, then, if DataBus16_8=1,
// the crc16 output is dropped and if DataBus16_8=0, lower 8bits of crc16 output are dropped.
// In this case we need not assert RxValid/RxValidH to send data. 
// 
always @(posedge Clock_60)
  begin
   if(Reset)
    begin
     RxValid_tmp   <= #1 0;
     RxValidH_tmp  <= #1 0;
    end
   else if(Config[PREAMBLE_BIT] & XIF_xav)
    begin
     RxValid_tmp   <= #1 1;
     RxValidH_tmp  <= #1 DataBus16_8;
    end
   else if( Config[DROPPKT_BIT] | (PID_DATAx | DATAx_xaction_h) & Config[TIMEOUTERR_BIT] )
     begin
      RxValid_tmp  <= #1 0;
      RxValidH_tmp <= #1 0;
     end
   else
    begin
     RxValid_tmp   <= #1 RxValid_pre && RxValid_enable;
     RxValidH_tmp  <= #1 RxValidH_pre;
    end
  end

  
  always @ (posedge Clock_60) begin
    BFM_xrun_r <= #1 BFM_xrun;
  end

  assign RxActive_tmp = BFM_xrun | BFM_xrun_r;
  
  
  // RxError
  assign RxError = 0;

  always @(posedge Clock_60)
    begin
      if(Reset)
        BFM_xrun <= #1 0;
      else if(TxDone)
        BFM_xrun <= #1 0;
      else
        BFM_xrun <= #1 |XIF_get_size;
    end

//  assign DataOut0_7    = RxValid  ? DataOut_Reg[7:0]  : 8'b0;
//  assign DataOut8_15   = RxValidH ? DataOut_Reg[15:8] : 8'b0;
    
  always @(posedge Clock)
    begin
      DataOut0_7 <= #1 RxValid_tmp  ? DataOut_Reg[7:0]  : 8'b0;
      DataOut8_15<= #1 RxValidH_tmp ? DataOut_Reg[15:8] : 8'b0;
    end
  
  always @(posedge Clock)
    begin
      if(Reset)
        begin
          RxActive <= #1 0;
          RxValid  <= #1 0;
          RxValidH <= #1 0;
        end
      else
        begin
          RxActive <= #1 RxActive_tmp;
          RxValid  <= #1 RxValid_tmp;
          RxValidH <= #1 RxValidH_tmp;
        end
    end
   
// Begin SPLIT CRC5 Block
///////////////////////////////////////////////////////////////////////////
// 19 bits input to do crc5
// CRC5_SPLIT SPLIT_CRC(.CRC_DIN(5'h1f),
//            .DataIN(CRC5_SPLIT_In),
//            .CRC_DOUT(crc5_SPLIT_out));
//          
//assign CRC5_SPLIT_Out = ~crc5_SPLIT_out;

// End of SPLIT CRC5 block
//////////////////////////////////////////////////////////////////////////

endmodule 

         /*
  
   Revision      : $Revision$
   Log           :
                 : Revision 1.2  2006/01/17 03:37:33  jstein
                 : Reviewed by:  John Hoglund
                 : Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
                 : Bugs Affected: DW02_prod_sum1 sign extension
                 : Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
                 : removed all `defines from xactor verilog (mostly usb)
                 : added test/xactors/usb20
                 :
                 : Revision 1.1  2005/11/10 20:40:53  hoglund
                 : Reviewed by: knutson
                 : Tests Done: checkin xactors
                 : Bugs Affected: none
                 :
                 : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
                 : because of the period.
                 : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
                 : verilog source files.
                 : Utopia2 also has name changes on the verilog source files.
                 :
                 : Revision 1.3  2005/10/20 18:23:28  jstein
                 : Reviewed by: Dylan Dobbyn
                 : Tests Done: scripts/checkin + xactor regression suite
                 : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
                 : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
                 : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
                 : added html/xactors dir, and html/xactors/adding_xactors.txt doc
                 :
                 : Revision 1.2  2005/09/29 23:53:43  jstein
                 : Reviewed by: Dylan Dobbyn
                 : Tests Done:  checkin, xactor regression
                 : Bugs Affected: none
                 :  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
                 :  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
                 :  so they will not be seen by the customer
                 :  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
                 :
                 : Revision 1.1  2005/09/25 16:58:31  jstein
                 : Reviewed by: none
                 : Tests Done: usb2.0 xactor test
                 : Bugs Affected: Adding Verilog unprotected source code to sandbox
                 :
                 : Revision 1.41  2004/09/27 18:56:54  zippelius
                 : Fixed bug with handshake transactions in 16 bit mode
                 :
                 : Revision 1.40  2004/09/21 21:45:00  fredieu
                 : Add back in the PID error forcing
                 :
                 : Revision 1.39  2004/09/21 18:55:11  zippelius
                 : removed some redundant code
                 :
                 : Revision 1.38  2004/09/21 18:20:54  zippelius
                 : Fixed bug, when transmitting USB transaction with even number of bytes
                 :
                 : Revision 1.37  2004/09/16 19:17:29  zippelius
                 : Untabified
                 :
                 : Revision 1.36  2004/09/16 18:55:20  zippelius
                 : Updated comments, some performance tuning
                 :
                 : Revision 1.35  2004/09/16 16:13:51  zippelius
                 : Added sync stage for 16 bit mode that always snaps the DataOut and RxValid changes to the rising edge of the 30 MHz clock
                 :
                 : Revision 1.34  2004/09/16 15:31:30  zippelius
                 : Fixed bug, with 3 byte DATAx packet
                 :
                 : Revision 1.33  2004/09/16 03:30:44  fredieu
                 : minor fix for synthesis
                 :
                 : Revision 1.32  2004/09/15 20:55:44  zippelius
                 : Fixed RxValid signaling problem in 8 bit bus mode
                 :
                 : Revision 1.31  2004/09/15 20:05:49  zippelius
                 : Changed RxValidH timing
                 :
                 : Revision 1.30  2004/09/15 17:50:19  zippelius
                 : Now working with 60 MHz XIF core
                 :
                 : Revision 1.29  2004/09/14 17:44:17  fredieu
                 : Slight syntax
                 :
                 : Revision 1.28  2004/09/14 14:30:36  zippelius
                 : Adjusted RxActive timing
                 :
                 : Revision 1.27  2004/09/14 13:05:28  zippelius
                 : DATAx, Token transaction working
                 :
                 : Revision 1.26  2004/09/14 01:53:21  zippelius
                 : More major clean-ups. Passes tests in 8 bit mode
                 :
                 : Revision 1.25  2004/09/13 18:41:24  zippelius
                 : Added 60 Mhz clock to Tx_DataOutReg
                 :
                 : Revision 1.24  2004/09/12 03:37:46  zippelius
                 : Added modifications to support 16bit bus mode
                 :
                 : Revision 1.23  2004/09/12 03:22:46  zippelius
                 : Misc. minor changes
                 :
                 : Revision 1.22  2004/09/12 01:54:23  zippelius
                 : Replaced sections for Token transactions and Handshake transactions with new code
                 :
                 : Revision 1.21  2004/09/11 17:38:37  zippelius
                 : Fixed CRC16 insertion bug
                 :
                 : Revision 1.20  2004/09/11 16:36:32  zippelius
                 : LastData now works, but some transactions still time out
                 :
                 : Revision 1.19  2004/09/11 14:46:48  zippelius
                 : Re-wrote large portions of Tx_DataOutReg  code. Not yet fully working
                 :

                */
