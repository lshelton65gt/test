// ///////////////////////////////////////////////////////////////////
// The define of BFM
//////////////////////////////////////////////////////////////////////
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//

// ifdef CARBONX_USB_DEFINE_V
// else
// define CARBONX_USB_DEFINE_V

//define the varilable used by BFM

// PID Encodings
parameter USB_BFM_PID_OUT   = 	4'b0001;
parameter USB_BFM_PID_IN    = 	4'b1001;
parameter USB_BFM_PID_SOF   = 	4'b0101;
parameter USB_BFM_PID_SETUP = 	4'b1101;
parameter USB_BFM_PID_DATA0 = 	4'b0011;
parameter USB_BFM_PID_DATA1 = 	4'b1011;
parameter USB_BFM_PID_DATA2 = 	4'b0111;
parameter USB_BFM_PID_MDATA = 	4'b1111;
parameter USB_BFM_PID_ACK   =	4'b0010;
parameter USB_BFM_PID_NACK  =   4'b1010;
parameter USB_BFM_PID_STALL = 	4'b1110;
parameter USB_BFM_PID_NYET  = 	4'b0110;
parameter USB_BFM_PID_PRE   = 	4'b1100;
parameter USB_BFM_PID_ERR   = 	4'b1100;
parameter USB_BFM_PID_SPLIT = 	4'b1000;
parameter USB_BFM_PID_PING  = 	4'b0100;
parameter USB_BFM_PID_RES   =  	4'b0000;

// --------------------------------------------------
// USB Line state & Speed Negotiation Time Values


// Prescaler Clear value.
parameter USB_BFM1_PS_250_NS =	4'd13;

// uS counter representation of 2.5uS (2.5/0.25=10)
parameter USB_BFM1_C_2_5_US =	8'd10;

// uS counter clear value
// The uS counter counts the time in 0.25uS intervals. It also generates
// a count enable to the mS counter, every 62.5 uS.
// The clear value is 62.5uS/0.25uS=250 cycles.
parameter USB_BFM1_C_62_5_US = 	8'd250;

// mS counter representation of 100uS (100/62.5=1.6 -> 2)
parameter USB_BFM1_C_100_US =	7'd2;

// mS counter representation of 3.0mS (3.0/0.0625=48)
parameter USB_BFM1_C_3_0_MS = 	7'd48;

// mS counter representation of 3.125mS (3.125/0.0625=50)
parameter USB_BFM1_C_3_125_MS = 	7'd50;

// mS counter representation of 5mS (5/0.0625=80)
// fredieu - this would seem to need to be decimal 80.
//define USB_BFM1_C_5_MS	7'd50
parameter USB_BFM1_C_5_MS = 	7'd80;

// Multi purpose Counter Prescaler, generate 2.5 uS period
// 2500/16.667ns=150 (minus 2 for pipeline)
parameter USB_BFM2_C_2_5_US = 	8'd148;

// Generate 0.5mS period from the 2.5 uS clock
// 500/2.5 = 200
parameter USB_BFM2_C_0_5_MS = 	8'd200;

// Indicate when internal wakeup has completed
// me_cnt counts 0.5 mS intervals. E.g.: 5.0mS are (5/0.5) 10 ticks
// Must be 0 =< 10 mS
parameter  USB_BFM2_C_WAKEUP = 	10;

// Indicate when 100uS have passed
// me_ps2 counts 2.5uS intervals. 100uS are (100/2.5) 40 ticks
parameter  USB_BFM2_C_100_US = 	40;

// Indicate when 1.0 mS have passed
// me_cnt counts 0.5 mS intervals. 1.0mS are (1/0.5) 2 ticks
parameter  USB_BFM2_C_1_0_MS = 	2;

// Indicate when 1.2 mS have passed
// me_cnt counts 0.5 mS intervals. 1.2mS are (1.2/0.5) 2 ticks
parameter  USB_BFM2_C_1_2_MS = 	2;

parameter  USB_BFM2_C_1_5_MS = 	3;

// // Indicate when 100 mS have passed
// // me_cnt counts 0.5 mS intervals. 100mS are (100/0.5) 200 ticks
// define EN_SIM_SHORTCUT 1
// `ifdef EN_SIM_SHORTCUT
// // shorten 100ms to 2 ms
// parameter USB_BFM2_C_100_MS = 4;
//`else
parameter  USB_BFM2_C_100_MS = 	200;
//`endif

parameter  USB_BFM2_C_3_MS = 6;


// Defines for get_csdata bit slices
// Note:
// If these slices change, then the corresponding defines in sys.h need
// to be updated
parameter  USB_HC_CONFIG_BITS_H = 31;
parameter  USB_HC_CONFIG_BITS_L = 0;
parameter  USB_HC_EXPECTED_DURATION_BITS_H = 63;
parameter  USB_HC_EXPECTED_DURATION_BITS_L = 32;
parameter  USB_GCS_WIDTH = 64;

// Defines for put_csdata bit slices
parameter  USB_HC_STATUS_BITS_H = 31;
parameter  USB_HC_STATUS_BITS_L =  0;
parameter  USB_HC_FIRST_TIME_BITS_H = 63;
parameter  USB_HC_FIRST_TIME_BITS_L = 32;
parameter  USB_HC_COMPLETION_TIME_BITS_H = 95;
parameter  USB_HC_COMPLETION_TIME_BITS_L = 64;
//parameter  USB_PCS_WIDTH = 96;
parameter  USB_PCS_WIDTH = 160;


// Config Register bits
parameter  BUS_SPEED_BITS_H = 1;
parameter  BUS_SPEED_BITS_L = 0;
parameter  PREAMBLE_BIT = 2;
parameter  TIMEOUTERR_BIT = 3;
parameter  CRCERR_BIT = 4;
parameter  CORRUPTDATA_BIT = 5;
parameter  DROPPKT_BIT = 6;
parameter  SPLITTOKENERR_BIT = 7;
parameter  TOKENERR_BIT = 8;
parameter  DATAERR_BIT = 9;
parameter  DEVICEATTACH_BIT = 10;
parameter  REMOVE_BIT = 11;
parameter  LS_ATTACH_BIT = 12;
parameter  RESET_DEV_BIT = 13;
parameter  SUSRESUME_DEV_BIT = 14;
parameter  CORRUPT_PID = 15;
parameter  VStatus_BITS_H = 23;
parameter  VStatus_BITS_L = 16;
parameter  ENABLE_TX_STALLS = 25;
parameter  ENABLE_RX_STALLS = 26;
parameter  IGNORE_RESPONSE = 27;
parameter  SQUELCH_LINESTATE = 28;
parameter  FAST_NEGOTIATE_BIT_CLR = 29;
parameter  FAST_NEGOTIATE_BIT = 30;
parameter  GLOBAL_RESET_OUT_BIT = 31;


parameter  USB_LS = 2'b01;
parameter  USB_FS = 2'b10;
parameter  USB_HS = 2'b11;

parameter  LineState_0 = 2'b00;
parameter  LineState_J = 2'b01;
parameter  LineState_K = 2'b10;
parameter  LineState_1 = 2'b11;

// endif
