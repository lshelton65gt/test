// ///////////////////////////////////////////////////////////////////
// The Device LineState Block of BFM
//////////////////////////////////////////////////////////////////////
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
//
//
//
// This module determines the LineState value that the Host Controller drives towards the USB Device.
// The two LineState bits are used to signal SE0, J, K and SE1 states, which in turn
// determines the state of the attached USB device. 
// According to the USB2.0 spec (p150) after a POR (Power On Reset) within 100ms the attached USB device has
// to signal "Attached", then after a debounce interval of at least 100ms, the Host Controller can drive
// reset to the device, which may then be followed by the High-Speed Detection Handshake
//
////////////////////////////////////////////////////////////////////////////
// 
//
//
//Update: 1) 073103: Attach state add wait 100ms then go to reset state  
//        2) 080103: 
//        3) 080303: in order to reduse reset waiting time, add bd_T3_gt_Reset_cnt
//                    signal to control reset time after attach;
//        4) 080503: In state machine,change control signal OpMode_K to 
//                   Fall_OpMode to make state change;
//
// Revision      : $Revision$
// Log           :
//               : Revision 1.2  2006/01/17 03:37:33  jstein
//               : Reviewed by:  John Hoglund
//               : Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
//               : Bugs Affected: DW02_prod_sum1 sign extension
//               : Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
//               : removed all `defines from xactor verilog (mostly usb)
//               : added test/xactors/usb20
//               :
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.3  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.2  2005/09/29 23:53:43  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done:  checkin, xactor regression
//               : Bugs Affected: none
//               :  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
//               :  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
//               :  so they will not be seen by the customer
//               :  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.18  2004/09/14 17:44:18  fredieu
//               : Slight syntax
//               :
//               : Revision 1.17  2004/09/03 13:54:19  zippelius
//               : Some minor changes
//               :
//               : Revision 1.16  2004/09/01 21:26:40  zippelius
//               : modified action in Dev_Attach state to get things working with ARC core
//               :
//               : Revision 1.15  2004/08/17 01:04:32  fredieu
//               : fix for usb_test17
//               :
//               : Revision 1.14  2004/08/11 20:05:52  zippelius
//               : Fixed problem that causes hang in FS mode
//               :
//               : Revision 1.13  2004/08/06 13:51:36  zippelius
//               : Updated/fixed test23, fixed numerous issues around User Reset
//               :
//               : Revision 1.12  2004/07/30 22:13:47  fredieu
//               : fix for suspend/resume
//               :
//               : Revision 1.11  2004/06/30 03:07:04  fredieu
//               : Make the LineState toggle about in HS mode.
//               :
//               : Revision 1.10  2004/06/24 19:50:12  fredieu
//               : Formatting change only.
//               :
//               : Revision 1.9  2004/06/21 19:45:14  fredieu
//               : Add an external fast_negotiate signal.
//               :
//               : Revision 1.8  2004/06/18 22:16:00  fredieu
//               : Put in some regs in prep to get rid of sensitivity list.
//               :
//               : Revision 1.7  2004/06/04 18:04:12  fredieu
//               : Fix to "always @ sensitivity list" to get rid of latches.
//               :
//               : Revision 1.6  2004/04/26 21:54:26  jafri
//               : modified english in verilog and c code and added usb_test1a to test table
//               :
//               : Revision 1.5  2004/04/20 20:54:10  zippelius
//               : Losts of code clean-up, comment fixes
//               :
//               : Revision 1.4  2004/04/19 15:30:30  zippelius
//               : HC is now synthesizable with Synplicigy
//               :
//               : Revision 1.3  2004/04/15 14:50:55  zippelius
//               : Major code clean-ups. Upgraded USB IP core to latest version. Fixed call-backs.Added Error masking. Fixed LineState state machines.
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.3  2003/12/04 02:48:09  adam
//               : ...
//               :
//               : Revision 2.2  2003/12/04 02:08:47  adam
//               : ..
//               :
//               : Revision 2.1  2003/09/27 10:46:15  thor
//               : Debug
//               :
//               : Revision 2.0  2003/09/27 07:39:09  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.1  2003/09/23 07:54:10  thor
//               : split into two dir
//               :
//               : Revision 1.10  2003/09/10 14:13:31  thor
//               : *** empty log message ***
//               :
//               : Revision 1.9  2003/09/10 07:58:32  thor
//               : add comment
//               :
//               : Revision 1.8  2003/09/09 09:44:57  thor
//               : change Config_Reg bits
//               :
//               : Revision 1.7  2003/09/05 02:00:35  thor
//               : add blank
//               :
//               : Revision 1.6  2003/09/03 09:07:49  thor
//               : add blank
//               :
//               : Revision 1.5  2003/09/02 10:37:27  thor
//               : change me_cnt length
//               :
//               : Revision 1.4  2003/08/20 14:11:15  thor
//               : add Lowspeed  change
//               :
//               : Revision 1.3  2003/08/15 10:56:32  thor
//               : change reset time from 180000 to 179996
//               :
//               : Revision 1.2  2003/08/14 14:05:56  thor
//               : add header
//               :



        
`timescale 1ns/1ns

module carbonx_usb_devicelinestate(Clock, 
                           Reset,
                           rst_UTMI,
                           Attach,
                           LS_Attach,
                           LowSpeed,
                           FullSpeed,
                           HighSpeed,
                           
// From BFM_Opcode block 
                           BFM_Reset_Device,
                           BFM_Suspend_Resume_Device,
                           
// From UTMI interface
                           SuspendM,
                           OpMode,
               TxValid,

//to UTMI interface                           
                           LineState,
                                                      
//to SW
                           FS_Attach,
                           Highspeed_Handshake_Detect_Finish,
                           Dev_Speed_pre,
                           Load_Dev_Speed,
  
                           
                           SusReFinish,

                           fast_negotiate,
                           squelch_linestate
);

// define Input/Output signals
input       Clock;                                //BFM working clock;

input       Reset;                                //BFM reset signal;

input       rst_UTMI;                             //UTMI input reset

input       Attach;                               //hub detect a device attach;

input       LowSpeed;                             //device use lowspeed working;  

input       FullSpeed;                            //fullspeed transfer

input       HighSpeed;                            //highspeed transfer

input       LS_Attach;                            //lowspeed attach;

input       SuspendM;                             //high is valid; 

input[1:0]  OpMode;                               //indicate the Device whether to send chirp K;

input       TxValid;                              //asserted, when transmit data is valid

input       BFM_Reset_Device;                     //user wants to do a reset

input       BFM_Suspend_Resume_Device;            //user want to check dut's Suspend/Resume function

output[1:0] LineState;                            //bus state

output      Highspeed_Handshake_Detect_Finish;    //finish the Highspeed_Handshake_detect(full speed or highspeed);if LS_Attach device  
                                                  //it means that the reset after attach finish.

output      FS_Attach;                            //a full speed only device attach  

//
output      SusReFinish;                          //indicate device suspend/resume end

output[1:0] Dev_Speed_pre;

output      Load_Dev_Speed;

input       fast_negotiate;

input       squelch_linestate;  // only in NORMAL state

reg [1:0]    LineState;
reg [1:0]    LineState_r;
reg [1:0]    LineState_r1;



reg         Highspeed_Handshake_Detect_Finish_set;
reg         Highspeed_Handshake_Detect_Finish;
                  
reg         SusReFinish;

reg[1:0]    Dev_Speed_pre_set;
reg         Load_Dev_Speed_set;
reg[1:0]    Dev_Speed_pre;
reg         Load_Dev_Speed;
   
 `include "carbonx_usb_params.vh"
           
//state machine parameter
parameter     
            Idle            =  12'h800,
            Dev_Attach      =  12'h001,
            Dev_Reset       =  12'h002,
            Dev_Suspend     =  12'h004,
            Dev_Resume      =  12'h008,
            Normal          =  12'h010,
            Reset_Suspend   =  12'h020,
            Speed_Neg       =  12'h040,
            Speed_Neg_K     =  12'h080,
            Speed_Neg_J     =  12'h100,
            Speed_Neg_FS    =  12'h200,
            Speed_Neg_HS    =  12'h400;


             
// set internal wire and reg signals

wire      SE0_long;

wire      J_long;

//Misc Events Counter
wire      me_ps_2_5_us;                           //every 2.5 us generate a pluse;

wire      me_ps2_0_5_ms;                          // every 0.5ms generate a pluse;

reg       me_cnt_clr;                             //me_cnt counter clear signal;

reg[7:0]  me_cnt_ps1;                             //2.5us counter;

reg[7:0]  me_cnt_ps2;                             //0.5ms counter;

reg[7:0]  me_cnt;                                 //ms counter


//Idle Counter
reg       Idle_cnt_clr;                           //Idle_counter clear signal;

wire      ps_cnt_clr;                             //ps_cnt clear signal;

wire      Idle_cnt1_clr;                          //Idle_cnt1 clear signal;

reg[3:0]  ps_cnt;                                 //0.25us counter;

reg[7:0]  Idle_cnt1;                              //us counter;

reg[6:0]  Idle_cnt2;                              //ms conter;

reg       FS_Attach;

//state machine 
reg[11:0] state;                                  //state machine registers;

reg[11:0] next_state;                             //state machine registers;

//Chirp Counter
reg[2:0]  Chirp_count;                            //count Chirp K or J

reg       Chirp_count_inc;                        //Chirp_count add signal

reg       Chirp_count_clr;                        //Chirp_count clear signal

//state change waiting time;
reg       T1_gt_2_5_us;                           //Idle state longer than 2.5us

reg       T1_st_3_0_ms;                           //Idle state shoter than 3.0ms

reg       T1_gt_3_0_ms;                           //longer  than 3.0ms 

reg       T1_gt_5_0_ms;                           //longer than 5.0ms

reg       T1_gt_3_125_ms;                         //longer than 3.125ms

reg       T2_gt_1_5_ms;                           //longer than 1.5ms
  
reg       T2_gt_1_2_ms;                           //longer than 1.2ms

reg       T2_gt_1_0_ms;                           //longer than 1.0ms

reg       T2_gt_100_us;                           //

reg       T2_gt_2_5_us;                           //

reg       me_cnt_3_ms;                            //

reg       me_cnt_100_ms;

wire      Idle_long;

reg       OpMode_r;                               //one cycle delay of OpMode == 2'b10
wire      Fall_OpMode;                            //falling edge of OpMode == 2'b10
reg       TxValid_r; 
wire      Fall_TxValid;                           //falling edge of TxValid
wire      Rise_TxValid;                           //rising edge of TxValid

reg       FS_Attach_h;

reg       SuspendM_r;

wire      Fall_SuspendM;

//Begin main module
///////////////////////////////////////////////////////////////


assign    SE0_long = (LineState == LineState_0);

assign    J_long   = (LineState == LineState_J);

assign    Idle_long= (LS_Attach |FS_Attach)? J_long:(FullSpeed?(BFM_Reset_Device? SE0_long : J_long):SE0_long);
   
always @(posedge Clock)
  begin
    if(Reset)
      FS_Attach_h <= 0;
    else if(FS_Attach)
      FS_Attach_h <= 1;
    end


// Idle Counter
//////////////////////////////////////////////
//Create a 0.25us counter
//Prescaler Clear value.
//The prescaler generates a 0.25uS pulse, from a nominal PHY clock of
//60 Mhz. 250nS/16.667ns=15. The prescaler has to be cleared every 15
//cycles. Due to the pipeline, subtract 2 from 15, resulting in 13 cycles.
//!!! This is the only place that needs to be changed if a PHY with different
//!!! clock output is used.

always @(posedge Clock )
     begin
        if(Reset)
            ps_cnt <= 4'd0;
        else if( !Idle_long | Idle_cnt_clr | ps_cnt_clr )
            ps_cnt <= 4'd0;
        else 
            ps_cnt <= ps_cnt + 4'd1;
      end

// fredieu - add a one to account for this being a wire versus a reg.
assign ps_cnt_clr = (ps_cnt == USB_BFM1_PS_250_NS + 1);

//Count us
//The uS counter counts the time in 0.25uS intervals. It also generates
//a count enable to the mS counter, every 62.5 uS.
//The clear value is 62.5uS/0.25uS=250 cycles.

always @(posedge Clock )
     begin
        if(Reset)
          Idle_cnt1 <= 8'd0;
        else if( !Idle_long | Idle_cnt_clr | Idle_cnt1_clr)
          Idle_cnt1 <= 8'd0;
        else if( !T1_gt_3_125_ms & ps_cnt_clr)
          Idle_cnt1 <= Idle_cnt1 + 8'd1 ;
     end
     
assign Idle_cnt1_clr = (Idle_cnt1 == USB_BFM1_C_62_5_US );

//Count ms
always @(posedge Clock )
     begin
        if(Reset)
          Idle_cnt2 <= 7'd0;
        else if( !Idle_long | Idle_cnt_clr )
          Idle_cnt2 <= 7'd0;
        else if( !T1_gt_5_0_ms & Idle_cnt1_clr)
          Idle_cnt2 <= Idle_cnt2 + 7'd1 ;
     end

//2.5us/0.25us = 10;     

always @(posedge Clock)
    begin
    T1_gt_2_5_us  <= !Idle_cnt_clr & (Idle_cnt1 > USB_BFM1_C_2_5_US );
  end

//mS counter representation of 3.0mS (3.0/0.0625=48)

//get rid of latch

always @(posedge Clock)
  begin
		// fredieu - the +3 seems incorrect!
//    T1_gt_3_0_ms  <= !Idle_cnt_clr & (Idle_cnt2 > USB_BFM1_C_3_0_MS +3 );
			T1_gt_3_0_ms  <= !Idle_cnt_clr & (Idle_cnt2 > USB_BFM1_C_3_0_MS);
  end

always @(posedge Clock)
    begin
    T1_st_3_0_ms   <= !Idle_cnt_clr & (Idle_cnt2 < USB_BFM1_C_3_0_MS );
    end

// mS counter representation of 5.0mS (5.0/0.0625=148)
always @(posedge Clock)
    begin
    T1_gt_5_0_ms   <= !Idle_cnt_clr & (Idle_cnt2 > USB_BFM1_C_5_MS );
  end

always @(posedge Clock)
    begin
    T1_gt_3_125_ms   <= !Idle_cnt_clr & (Idle_cnt2 > USB_BFM1_C_3_125_MS );
  end
//End of Idle Counter/////
//////////////////////////

//Misc Events Counter
///////////////////////////
//Multi purpose Counter Prescaler, generate 2.5 uS period
//2500/16.667ns=150 (minus 2 for pipeline)
//create 2.5us counter

always @ (posedge Clock )
     begin
        if(Reset)
           me_cnt_ps1 <= 8'd0;
        else if(me_cnt_clr | me_ps_2_5_us )
           me_cnt_ps1 <=8'd0;
        else 
           me_cnt_ps1 <= me_cnt_ps1 + 8'd1;
     end 
     
assign me_ps_2_5_us = (me_cnt_ps1 == USB_BFM2_C_2_5_US );

//create 0.5ms counter
//0.5ms/2.5us = 200

always @ (posedge Clock )
     begin
        if(Reset)
           me_cnt_ps2 <= 8'd0;
        else if(me_cnt_clr | me_ps2_0_5_ms )
           me_cnt_ps2 <=8'd0;
        else if(me_ps_2_5_us )
           me_cnt_ps2 <= me_cnt_ps2 + 8'd1;
     end            
     
assign me_ps2_0_5_ms = (me_cnt_ps2 == USB_BFM2_C_0_5_MS);

//ms counter
always @ (posedge Clock )
     begin
        if(Reset)
           me_cnt <= 8'd0;
        else if( me_cnt_clr)
           me_cnt <=8'd0;
        else if( me_ps2_0_5_ms )
           me_cnt <= me_cnt + 8'd1;
     end    
     
//the same as T1_gt_*
always @(posedge Clock)
  begin
    T2_gt_2_5_us  <= !me_cnt_clr & (me_cnt_ps2 > 1);
    T2_gt_100_us  <= !me_cnt_clr & (me_cnt_ps2 > USB_BFM2_C_100_US + 3 ); 
    T2_gt_1_5_ms  <= !me_cnt_clr & (me_cnt > USB_BFM2_C_1_5_MS );
    T2_gt_1_2_ms  <= !me_cnt_clr & (me_cnt > USB_BFM2_C_1_2_MS);
    T2_gt_1_0_ms  <= !me_cnt_clr & (me_cnt > USB_BFM2_C_1_0_MS);
    me_cnt_3_ms   <= !me_cnt_clr & (me_cnt > USB_BFM2_C_3_MS);
  end

always @(posedge Clock)
    if(fast_negotiate) me_cnt_100_ms  <= !me_cnt_clr & (me_cnt > 4);
    else me_cnt_100_ms  <= !me_cnt_clr & (me_cnt > USB_BFM2_C_100_MS); 

//End of Misc Events Counter////
////////////////////////////////


//When OpMode changes from LineState_K to LineState_0, it means that the IP core
//changed state from "Drive K_Chirp" on the bus to receive K Chirp from 
//host. Therefore host state must also change to drive K chirp on Linestate.   

always @(posedge Clock)
   begin
    if(Reset)
     OpMode_r <= 0;
    else
     OpMode_r <= (OpMode == 2'b10);
  end
  
assign Fall_OpMode = (!(OpMode == 2'b10)) & OpMode_r;

always @(posedge Clock)
  begin
    TxValid_r <= #1 TxValid;
    Highspeed_Handshake_Detect_Finish <= Highspeed_Handshake_Detect_Finish_set;
    LineState_r <= LineState;
    LineState_r1 <= LineState_r;
    Dev_Speed_pre <= Dev_Speed_pre_set;
    Load_Dev_Speed <= Load_Dev_Speed_set;
  end

assign Fall_TxValid = (!TxValid) & TxValid_r;

assign Rise_TxValid = (TxValid) & !TxValid_r;
      
//Chirp Counter
///////////////////////////////
// Track the number of K-J chirps during the K-J-K-J-K-J chirp sequence
always @ (posedge Clock ) 
  begin
    if(Reset | Highspeed_Handshake_Detect_Finish_set)
      Chirp_count <= 3'd0;
    else if(Chirp_count_inc)
      Chirp_count <= Chirp_count + 3'd1;
  end
     

always @(posedge Clock)
     if(Reset)
       SuspendM_r <= 0;
     else
       SuspendM_r <= SuspendM;


assign Fall_SuspendM = (~SuspendM) & SuspendM;

//End of Chirp Counter////////
//////////////////////////////

//Main State Machine
/////////////////////////////
//Wishbone interface reset signal is the same as 
//rst_UTMI signal. Just use an "assign" create rst_UTMI
//signal.
 
always @ (posedge Clock )
     begin
        if(Reset&(!rst_UTMI))
           state <= Idle;
        else if(rst_UTMI)
           state <= Dev_Attach;
        else
           state <= next_state;
     end


// When the DUT is being cold reset (due to Power On Reset or by USB device driver software)
// its state changes to POR,
// then transitions into the Attach state; After the device has been attached,
// the USB device can be reset by the HC over the USB (by driving SE0) to initiate the bus speed negotiation.
// The USB device is being reset via the USB by driving LineState to SE0.
// Once in USB device is in the RESET state, the USB Controller waits for a K Chirp from the USB device,
// which indicates the
// beginning of the High-Speed Negotiation handshake, which is marked by a K-J-K-J-K-J chirp sequence.
// If the Host Controller detects this chirp sequence, then it changes TermSelect and XcvSelect
// signals to go to
// Highspeed Idle state, otherwise the Host Controller transistions into the  Fullspeed state.

     
always @ ( Reset or state or LowSpeed or FullSpeed or HighSpeed or LS_Attach or Attach or Highspeed_Handshake_Detect_Finish
           or BFM_Reset_Device or BFM_Suspend_Resume_Device or SuspendM or Fall_OpMode or OpMode or Fall_TxValid or Rise_TxValid
           or TxValid or T1_gt_2_5_us or T1_gt_3_0_ms or T1_st_3_0_ms or T2_gt_100_us or Chirp_count or LineState_r1
//           or T2_gt_1_5_ms or T2_gt_1_2_ms or T2_gt_1_0_ms or T2_gt_2_5_us or me_cnt_100_ms or Fall_SuspendM  )
           or T2_gt_1_2_ms or T2_gt_1_0_ms or T2_gt_2_5_us or me_cnt_100_ms or Fall_SuspendM or squelch_linestate)
                
  if ( Reset | BFM_Reset_Device )
    begin
      Highspeed_Handshake_Detect_Finish_set = 0;
      LineState = LineState_1;
      Idle_cnt_clr    = 0 ;
      me_cnt_clr      = 0 ;
      Chirp_count_inc = 0;
      FS_Attach       = 0;
      SusReFinish     = 0;
      Chirp_count_clr = 0;
      Dev_Speed_pre_set   = 2'b00;
      Load_Dev_Speed_set  = 0;
      
      if ( Reset ) next_state = Idle;
      else         next_state = Dev_Reset;
    end
  else
    begin
       Highspeed_Handshake_Detect_Finish_set = Highspeed_Handshake_Detect_Finish;
       next_state      = state ;
       
       LineState       = LineState_1;
       Idle_cnt_clr    = 0 ;
       me_cnt_clr      = 0 ;
       Chirp_count_inc = 0;
       FS_Attach       = 0;
       SusReFinish     = 0;
       Chirp_count_clr = 0;
       Dev_Speed_pre_set   = 2'b00;
       Load_Dev_Speed_set  = 0;
       
       case(state)
         Idle:            
           begin
              LineState = LineState_1;
              if(BFM_Reset_Device)
                begin
                   next_state = Dev_Reset;
                end
              else if(Attach)
                begin
                   next_state = Dev_Attach;
                end
              else if(!SuspendM )
                //SuspendM is low  (i.e. device is not suspend), so we change state to
                //normal .
                begin
                   next_state = Normal;
                   Idle_cnt_clr  = 1;
                   me_cnt_clr    = 1;
                end
              else if( SuspendM )
                //device is suspended: we must wake up and then do following action.
                begin
                   next_state = Dev_Suspend;
                   Idle_cnt_clr  = 1;
                   me_cnt_clr    = 1;
                end
              else if(BFM_Suspend_Resume_Device & !SuspendM )
                //device is not Suspend and we then let it go to Suspend, first go to Normal and assert LineState
                //to the correct value.
                begin
                   next_state = Normal;
                   Idle_cnt_clr  = 1;
                   me_cnt_clr    = 1;
                end // if (BFM_Suspend_Resume_Device & !SuspendM )
              else if(BFM_Suspend_Resume_Device & SuspendM )
                //device is in Suspend and we go to Dev_Resume state then wake up.
                begin
                   next_state =Dev_Resume;
                   Idle_cnt_clr  = 1;
                   me_cnt_clr    = 1;
                end
           end // case: Idle
         
         Normal:     
           begin
              if(HighSpeed)
                // device working at highspeed, if finding se0 on bus is longer than 3.0ms 
                // then go to Reset_Suspend state
                begin
//                   LineState = LineState_0;
                   if(squelch_linestate == 1) LineState = LineState_0;
                   else if(LineState_r1 == LineState_0) LineState = LineState_1;
                        else LineState = LineState_0;

                   if(T1_gt_3_0_ms)
                     begin
                        next_state   = Reset_Suspend ;
                        me_cnt_clr = 1;
                     end
                end
              else if(LowSpeed | FullSpeed )
                //device working at Lowspeed or Fullspeed, after detecting Idle longer than 3.0ms
                //dut goes to Suspend state. 
                begin
                   if(BFM_Suspend_Resume_Device)
                     begin
                        LineState = LineState_J;
                        if(T1_gt_3_0_ms)
                          begin
                             next_state = Dev_Suspend;
                             Idle_cnt_clr=1;
                          end
                     end
                   else if(BFM_Reset_Device)
                     //If fullspeed lowspeed detects se0 longer than 2.5us and shorter than 3.0ms
                     //then device resets itself
                     begin
                        LineState = LineState_0;
                        if(T1_gt_2_5_us & T1_st_3_0_ms)
                          begin
                             next_state = Dev_Reset;
                             me_cnt_clr = 1;
                          end
                     end
                end // if (LowSpeed | FullSpeed )
           end // case: Normal
                
         Reset_Suspend:
           begin
              if(BFM_Suspend_Resume_Device)
                //user wants to check device's suspend and resume function, so change state to Dev_Suspend
                begin
                   LineState = LineState_J;
                   if(T2_gt_100_us)
                     begin
                        next_state = Dev_Suspend;
                        SusReFinish = 1;
                        Idle_cnt_clr = 1;
                     end
                end
              else if(SuspendM & Fall_OpMode )
                //device remote-wakeup and device finish drive K on the bus
                begin       
                   LineState = LineState_K;
                   next_state    = Dev_Resume;
                end // if (SuspendM & Fall_OpMode ) 
              else
                //after 100us go to highspeed handshake detect
                begin if(BFM_Reset_Device)
                   LineState = LineState_0;
                   if(T2_gt_100_us)
                     begin
                        next_state = Dev_Reset;
                        me_cnt_clr = 1;
                     end
                end
          end // case: Reset_Suspend
       
              
         Dev_Resume:
           begin
              LineState = LineState_K;
              next_state = Idle;
              SusReFinish = 1;
           end
         
         Dev_Suspend:
           begin
              //at suspend state, detect se0 longer than 2.5us,
              // reset device
	      if(Fall_SuspendM)
                //user want to check device resume function
                begin
                   LineState = LineState_K;
                   next_state    = Dev_Resume;
                end
              else
                begin
                   LineState = LineState_0;
                   if(T1_gt_2_5_us)
                     begin
                        next_state = Dev_Reset;
                        me_cnt_clr = 1;
                     end
                end
           end
         
         Dev_Attach:
           begin
              LineState = LineState_0;
              begin 
//                 if(me_cnt_100_ms) // @@@RZ disable wait time to get HC working with ARC core
//                   begin
                      me_cnt_clr = 1;
                      next_state = Dev_Reset;
//                   end
              end
           end
         
         
         Dev_Reset:
           begin
              Highspeed_Handshake_Detect_Finish_set = 0;
              LineState = LineState_0;
              if(LS_Attach | LowSpeed)
                //because lowspeed device does not do Highspeed_Handshake_Detect, reset is now finished. 
                begin
                   next_state  = Idle;
                   Highspeed_Handshake_Detect_Finish_set  = 1;
                end
              //                  else if(T2_gt_1_2_ms & Rise_TxValid )
              //wait for at least 1.2 ms and a rising edge of TxValid, then do highspeed handshake detect
              else if(Rise_TxValid ) //@@@
                // Wait for a rising edge of TxValid, then do highspeed handshake detect
                begin
                   next_state = Speed_Neg;
                   me_cnt_clr = 1;
                end
           end
         
         Speed_Neg:      
           // If we detect K (generated by USB Device) on bus, which is longer than 1.0 ms, then
           // the Host Controller responds by generating K-J-K-J-K-J chirp on bus 
           begin
              if( (TxValid && !(OpMode == 2'b10 ) && !T2_gt_1_0_ms ) )
                // Did not detect device K chirp or device K chirp is shorter than 1.0 ms,
                //  then finish highspeed handshake detect. This  means that a FS only device attached
                begin
                   FS_Attach   = 1;
                   next_state  = Speed_Neg_FS;
                   Dev_Speed_pre_set = USB_FS;
                   Load_Dev_Speed_set = 1;
                end
              else if((!LS_Attach) & Fall_TxValid & T2_gt_1_0_ms ) 
                // device finished driving K -> go to Speed_Neg_K state to drive K chirp
                begin
                   next_state = Speed_Neg_K;
                   me_cnt_clr = 1;
                   Chirp_count_clr = 1;
                end // if ((!LS_Attach) & Fall_TxValid & T2_gt_1_0_ms )
           end // case: Speed_Neg
         
         Speed_Neg_K:
           begin
              if(Chirp_count == 6 )
                //if device detect K-J-K-J-K-J chirp then go to Highspeed
                begin
                   next_state = Speed_Neg_HS;
                   me_cnt_clr = 1;
                   Dev_Speed_pre_set = USB_HS;
                   Load_Dev_Speed_set = 1;
                   LineState = LineState_0;
                end
              else
                begin
                   if( HighSpeed ) //@@@
                     //user wants to change device speed to high speed
                     begin
                        LineState = LineState_K;
                        if(T2_gt_2_5_us)
                          begin
                             next_state = Speed_Neg_J;
                             Chirp_count_inc =1 ;
                             me_cnt_clr = 1;
                          end
                     end // if ( HighSpeed )
                   else if( FullSpeed )
                     //user wants to use fullspeed transfer
                     begin
                        LineState = LineState_0;
                        next_state = Speed_Neg_FS;
                        Dev_Speed_pre_set = USB_FS;
                        Load_Dev_Speed_set = 1;
                     end
                   else 
                     begin
                        LineState = LineState_0;
                        next_state = Speed_Neg_FS;
                        Dev_Speed_pre_set = USB_FS;
                        Load_Dev_Speed_set = 1;
                     end // else: !if( FullSpeed )
                end // else: !if(Chirp_count == 6 )
           end // case: Speed_Neg_K

      
         Speed_Neg_J: //the same as Speed_Neg_K
           begin
              if(Chirp_count == 6 )
                begin
                   next_state = Speed_Neg_HS;
                   me_cnt_clr = 1;
                   Dev_Speed_pre_set = USB_HS;
                   Load_Dev_Speed_set = 1;
                   LineState = LineState_0;
                end
              else
                begin
                   if( HighSpeed )
                     begin
                        LineState = LineState_J;
                        if(T2_gt_2_5_us)
                          begin
                             next_state = Speed_Neg_K;
                             Chirp_count_inc =1 ;
                             if(Chirp_count != 5)
                               me_cnt_clr = 1;
                             else 
                               me_cnt_clr = 0; 
                          end
                     end
                   else if( FullSpeed )
                     begin
                        LineState = LineState_0;
                        next_state = Speed_Neg_FS;
                        Dev_Speed_pre_set = USB_FS;
                        Load_Dev_Speed_set = 1; 
                     end
                   else
                     begin
                        LineState = LineState_0;
                        next_state = Speed_Neg_FS;
                        Dev_Speed_pre_set = USB_FS;
                        Load_Dev_Speed_set = 1; 
                     end
                end
           end
                                         
         Speed_Neg_HS:
           begin
              LineState = LineState_0;
              next_state    = Idle;
              Highspeed_Handshake_Detect_Finish_set    = 1;
           end
               
         Speed_Neg_FS:
           begin
              LineState = LineState_0;
              next_state    = Idle;
              Highspeed_Handshake_Detect_Finish_set    = 1;
           end
         default: begin next_state = Idle; end
       endcase
    end
    
//End of Main State Machine
/////////////////////////////////


endmodule
      
