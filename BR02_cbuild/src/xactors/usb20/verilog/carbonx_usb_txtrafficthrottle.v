// /////////////////////////////////////////////////////////////////// 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
//
//
// This module simply generates a pattern used to stall transmits to the HOST
// by deasserting TxReady.  It is assumed that this file will be edited by
// users to produce a pattern typical of what they might expect.
//////////////////////////////////////////////////////////////////////////


`timescale 1ns/1ns

module carbonx_usb_txtrafficthrottle (
         allow_stalls,
         Clock,
         Reset,
         run
);

input allow_stalls;
input Clock;
input Reset;
output run;

// include "carbonx_usb_define.v"  // not used


reg [17:0] rotate;

reg run;

always @ (posedge Clock) begin
  if(Reset) rotate <= 18'h2916e;  // a relatively interesting sequence
   
  if(!allow_stalls) run <= 1;
  else begin
     run <= rotate[0];
     rotate <= {rotate[16:0],rotate[17]};
  end

end

endmodule

/*

   $Revision$

   Revision 1.2  2006/01/17 03:37:33  jstein
   Reviewed by:  John Hoglund
   Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
   Bugs Affected: DW02_prod_sum1 sign extension
   Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
   removed all `defines from xactor verilog (mostly usb)
   added test/xactors/usb20

   Revision 1.1  2005/11/10 20:40:53  hoglund
   Reviewed by: knutson
   Tests Done: checkin xactors
   Bugs Affected: none

   USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
   because of the period.
   SPI-3 and SPI-4 now have test suites.  They also have name changes on the
   verilog source files.
   Utopia2 also has name changes on the verilog source files.

   Revision 1.2  2005/10/20 18:23:28  jstein
   Reviewed by: Dylan Dobbyn
   Tests Done: scripts/checkin + xactor regression suite
   Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
   removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
   removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
   added html/xactors dir, and html/xactors/adding_xactors.txt doc

   Revision 1.1  2005/09/25 16:58:31  jstein
   Reviewed by: none
   Tests Done: usb2.0 xactor test
   Bugs Affected: Adding Verilog unprotected source code to sandbox

   Revision 1.1  2004/07/27 19:11:59  fredieu
   Changes to allow stalls on the transmit side



*/

