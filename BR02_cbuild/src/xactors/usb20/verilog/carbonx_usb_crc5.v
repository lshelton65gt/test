// ///////////////////////////////////////////////////////////////////
// The CRC5 Block of BFM
//////////////////////////////////////////////////////////////////////
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
// Revision      : $Revision$
// Log           :
//               : $Log$
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.2  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.0  2003/09/27 07:39:09  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.3  2003/09/26 02:56:01  thor
//               : add always block
//               :
//               : Revision 1.2  2003/09/24 08:43:31  thor
//               : change signals name
//               :
//               : Revision 1.1  2003/09/23 07:54:10  thor
//               : split into two dir
//               :
//               : Revision 1.2  2003/08/14 14:05:45  thor
//               : add header
//               :
//
//
//
// 
//

`timescale 1ns/1ns

///////////////////////////////////////////////////////////////////
//
// CRC5
//
///////////////////////////////////////////////////////////////////

module carbonx_usb_crc5(CRC_DIN, DataIN, CRC_DOUT);
input	[4:0]	CRC_DIN;
input	[10:0]	DataIN;
output	[4:0]	CRC_DOUT;

reg      DataIN_tmp1;
reg      CRC_DIN_tmp1;

always @(DataIN[10] or DataIN[9] or DataIN[8])
  begin
   DataIN_tmp1 =  DataIN[10] ^ DataIN[9] ^ DataIN[8];
  end

always @(CRC_DIN[2] or  CRC_DIN[3] or CRC_DIN[4])
  begin
    CRC_DIN_tmp1 = CRC_DIN[2] ^ CRC_DIN[3] ^ CRC_DIN[4];
  end



assign CRC_DOUT[0] =	DataIN_tmp1 ^DataIN[8] ^ DataIN[6] ^ DataIN[5] ^ DataIN[3] ^DataIN[0] ^ CRC_DIN[0] ^ CRC_DIN[2] ^ CRC_DIN_tmp1;

assign CRC_DOUT[1] =	DataIN[10] ^ DataIN[7] ^ DataIN[6] ^ DataIN[4] ^ DataIN[1] ^CRC_DIN[0] ^ CRC_DIN[1] ^ CRC_DIN[4];

assign CRC_DOUT[2] =	DataIN_tmp1^ DataIN[7] ^ DataIN[6] ^ DataIN[3] ^ DataIN[2] ^ DataIN[0] ^ CRC_DIN[0] ^ CRC_DIN[1] ^ CRC_DIN_tmp1;

assign CRC_DOUT[3] =	DataIN_tmp1 ^ DataIN[7] ^ DataIN[4] ^ DataIN[3] ^ DataIN[1] ^ CRC_DIN[1] ^ CRC_DIN_tmp1;

assign CRC_DOUT[4] =	DataIN_tmp1 ^ DataIN[5] ^ DataIN[4] ^ DataIN[2] ^ CRC_DIN_tmp1;

endmodule
