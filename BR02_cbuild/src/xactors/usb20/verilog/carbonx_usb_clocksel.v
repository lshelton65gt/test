// ///////////////////////////////////////////////////////////////////
// The Clock Select module of BFM
//////////////////////////////////////////////////////////////////////
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
//
//
//
// This block select the BFM work clock between : 60MHz, 30MHz and 48MHz or 6MHz.
//
//
// Revision      : $Revision$
// Log           :
//               : $Log$
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.3  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.2  2005/09/29 23:53:43  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done:  checkin, xactor regression
//               : Bugs Affected: none
//               :  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
//               :  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
//               :  so they will not be seen by the customer
//               :  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.4  2004/09/01 21:06:18  zippelius
//               : Fixed clock problem, when running 16 bit UTMI mode
//               :
//               : Revision 1.3  2004/04/20 20:54:10  zippelius
//               : Losts of code clean-up, comment fixes
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.0  2003/09/27 07:39:09  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.1  2003/09/23 07:54:10  thor
//               : split into two dir
//               :
//               : Revision 1.8  2003/09/10 07:58:32  thor
//               : add comment
//               :
//               : Revision 1.7  2003/09/09 09:44:57  thor
//               : change Config_Reg bits
//               :
//               : Revision 1.6  2003/09/05 02:00:34  thor
//               : add blank
//               :
//               : Revision 1.5  2003/09/03 09:07:13  thor
//               : add blank
//               :
//               : Revision 1.4  2003/08/25 07:22:14  thor
//               : change Config_Reg/Reset_Reg/ExpectedDuration_Reg from reg to wire and change name
//               :
//               : Revision 1.3  2003/08/15 01:57:48  thor
//               : add blank
//               :
//               : Revision 1.2  2003/08/14 13:52:26  thor
//               : add header
//               :



`timescale 1ns/1ns

module carbonx_usb_clocksel(Clock_60,
                              Clock_30,
                              Clock_48,
                              Clock_6,
                              
                              Reset,
                              
                              LS_Attach,
                              FS_Attach,
                              SuspendM,
                              DataBus16_8,
                              BFM_Write_Data,
                              Config_bit2,
                              Config_bit11,
                              LowSpeed_Contr,
                              Clock,
                              DUT_clock);
                              
                            
//input  
input                 Clock_60;       //input 60MHz clock

input                 Clock_30;       // input 30MHz

input                 Clock_48;       // input 48MHz

input                 Clock_6;        // input 6MHz

input                 Reset;          //system reset signal

input                 LS_Attach;      //indicate if a lowspeed device attach;

input                 FS_Attach;      //indicate if a fullspeed device attach;                       

input                 SuspendM;       //when high indicate DUT is being suspend state;

input                 DataBus16_8;    //indicate DUT input output databus's width

input                 BFM_Write_Data; //asserted indicate BFM send data to DUT

input                 Config_bit2;    //indicate whether to send a preamble token before send packet

input                 Config_bit11;   //indicate if a device remove

input                 LowSpeed_Contr; //lowspeed control transfer

output                Clock;          //BFM clock

output                DUT_clock;      //DUT clock



//internal signals

wire                  Rise_Write_Data;//rise edge of BFM_Write_Data

reg                   BFM_Write_Data_r;//one clock delay

reg                   FS_Attach_h;    //FS_Attach hold signal


//Begin main program/////////////
////////////////////////////////////////////
//when DUT is being suspend state, its clock (DUT_clock) must be "freeze"

assign DUT_clock = Clock & (!SuspendM);


//rise edge of BFM_Write_Data  
always @(posedge Clock)
  begin
    if(Reset)
      BFM_Write_Data_r <= 0;
    else
      BFM_Write_Data_r <= BFM_Write_Data;
  end
  
assign Rise_Write_Data = BFM_Write_Data & (!BFM_Write_Data_r);

//FS_Attach_h signal

always @(posedge Clock)
  begin
   if(Reset)
     FS_Attach_h <= 0;
   else if(Config_bit11)
     FS_Attach_h <= 0;
   else if(FS_Attach)
     FS_Attach_h <= 1;
  end
  
//Because when do preamble, we need to send preamble token at fullspeed and then send packet at lowspeed,
//and at Rise_Write_Data clock cycle we send preamble token,so at this clock the clock is 48MHz, else clock is 6MHz.
//When lowspeed control transfer, we need use fullspeed to transfer packet, so we use Clock_48;
//When a FS only device attach,BFM use 48M clock to work;If a HS/FS device attached,when DataBus16_8 = 1(means use 16bits databus),
//the working clock is 30MHz,else the working clock is 60MHz.

assign Clock = (Config_bit2 & Rise_Write_Data | LowSpeed_Contr) ? Clock_48:
                                                   (LS_Attach   ? Clock_6:
                                                   (FS_Attach_h ? Clock_48:
                                                   (DataBus16_8 ? Clock_30:
                                                                  Clock_60 )));
       
//End module

endmodule
