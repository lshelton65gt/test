// ///////////////////////////////////////////////////////////////////
// The BFM_Opcode Decode Block of BFM
//////////////////////////////////////////////////////////////////////
//
//
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$

//
//
//
// This block decodes XIF_get_operation, and drives BFM_Read_Data 
// BFM_Write_Data, BFM_Reset_Device and BFM_Suspend_Resume_Device
// control signals, which are used by other blocks of the USB transactor.
// ///////////////////////////////////////////////////////////////////

//
// Update: 1) 080303: Because the get_operation's value is valid for only one clock,
//                    change the decode block to hold the signal until read/
//                    write end;
//
// Revision      : $Revision$
// Log           :
//               : Revision 1.2  2006/01/17 03:37:33  jstein
//               : Reviewed by:  John Hoglund
//               : Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
//               : Bugs Affected: DW02_prod_sum1 sign extension
//               : Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
//               : removed all `defines from xactor verilog (mostly usb)
//               : added test/xactors/usb20
//               :
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.3  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.2  2005/09/29 23:53:43  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done:  checkin, xactor regression
//               : Bugs Affected: none
//               :  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
//               :  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
//               :  so they will not be seen by the customer
//               :  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.11  2004/09/08 19:42:03  zippelius
//               : Changed WriteEnd to LastData
//               :
//               : Revision 1.10  2004/08/06 13:51:36  zippelius
//               : Updated/fixed test23, fixed numerous issues around User Reset
//               :
//               : Revision 1.9  2004/07/30 22:12:56  fredieu
//               : Fix for suspend/resume
//               :
//               : Revision 1.8  2004/07/23 03:00:10  fredieu
//               : Put in an IPG before a packet
//               :
//               : Revision 1.7  2004/07/09 19:22:38  fredieu
//               : Minor changes.
//               : Reset a signal.
//               :
//               : Revision 1.6  2004/06/04 18:04:57  fredieu
//               : Add synthesis pragma
//               :
//               : Revision 1.5  2004/04/26 21:54:26  jafri
//               : modified english in verilog and c code and added usb_test1a to test table
//               :
//               : Revision 1.4  2004/04/20 20:54:10  zippelius
//               : Losts of code clean-up, comment fixes
//               :
//               : Revision 1.3  2004/04/15 14:50:55  zippelius
//               : Major code clean-ups. Upgraded USB IP core to latest version. Fixed call-backs.Added Error masking. Fixed LineState state machines.
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.0  2003/09/27 07:39:09  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.1  2003/09/23 07:54:10  thor
//               : split into two dir
//               :
//               : Revision 1.10  2003/09/09 09:44:57  thor
//               : change Config_Reg bits
//               :
//               : Revision 1.9  2003/09/03 09:07:49  thor
//               : add blank
//               :
//               : Revision 1.8  2003/09/03 03:37:36  thor
//               : get rid of unuse signal
//               :
//               : Revision 1.7  2003/08/26 03:11:30  thor
//               : add TimeOut
//               :
//               : Revision 1.6  2003/08/25 07:23:12  thor
//               : change Config_Reg/Reset_Reg/ExpectedDuration_Reg from reg to wire and change name
//               :
//               : Revision 1.5  2003/08/20 14:12:09  thor
//               : add speed change between LS and FS/HS
//               :
//               : Revision 1.4  2003/08/15 07:23:44  thor
//               : add XIF_xav when decode operation
//               :
//               : Revision 1.3  2003/08/15 02:35:21  thor
//               : get rid of user defined operation,and use Config to indicate whether to do reser device or 
//                 suspend/resume device.Use speed change to indicate that device must to do Highspeed_Handshake_Detect
//               :
//               : Revision 1.2  2003/08/14 14:06:16  thor
//               : add header
//               :






`timescale 1ns/1ns

module carbonx_usb_opcode_decode(Clock, 
                         Reset,
                         XIF_xav, 
                         XIF_get_operation,
                         XIF_get_address,
                         Config,
                         BFM_Reset_Device, 
        
                         BFM_Suspend_Resume_Device, 
                         BFM_Read_Data, 
                         BFM_Write_Data,
                         
                         Highspeed_Handshake_Detect_Finish, 
                         ReadEnd, 
                         LastData,
                         TimeOut, 
                         SusReFinish
)/* synthesis syn_hier = "firm"*/;

//Input Signals
input                    Clock;                      // BFM working clock;

input                    Reset;                      // BFM reset signal;

input                    XIF_xav;                    // XIF_core output signal;

input                    Highspeed_Handshake_Detect_Finish; //Device finish Highspeed detection handshake;

input                    ReadEnd;                    // BFM finish put data to XIF_core;

input                    LastData;                   // XIF_core finish writing data to BFM;

input                    SusReFinish;                // check device suspend/resume  finish;

input[31:0]              XIF_get_operation;          // operation code bus;

input[63:0]              XIF_get_address;            // operation address

input[31:0]              Config;                     // configuration register

input                    TimeOut;

//Output signals
output                   BFM_Reset_Device;           // Instructs the BFM to reset Device followed by a 
                                                     // HighSpeed detection handshake.
                                         
output                   BFM_Suspend_Resume_Device;  // Instructs the BFM to check Device suspend and 
                                                     // Resume functions;
                                         
output                   BFM_Read_Data;              // Instructs BFM to receive data from UTMI interface;

output                   BFM_Write_Data;             // Instructs XIF_core send data to BFM;


                
//define wire and reg signals
wire                     BFM_Reset_tmp;              // Indicates user initiated reset

wire                     BFM_Read_tmp;               // Read data from DUT

wire                     BFM_Write_tmp;              // Write data to DUT

wire                     BFM_SusRe_tmp;              //

         
reg                      BFM_Nop_Reg;                //Nop register

reg                      BFM_Reset_Device;           //reset register

reg                      BFM_Suspend_Resume_Device;  //

reg                      BFM_Read_Data;              //

reg                      BFM_Write_Data;             //

wire                     HighSpeed;                  //user want to use highspeed to send data,and use Config to configuration

wire                     FullSpeed;

wire                     LowSpeed;

wire                     Rise_HighSpeed;             // rising edge of HighSpeed

wire                     Rise_FullSpeed;             // rising edge of FullSpeed;

wire                     Fall_HighSpeed;             // falling edge of HighSpeed

wire                     Fall_FullSpeed;             // falling edge of FullSpeed;

wire                     Rise_LowSpeed;              // rising edge of LowSpeed;

wire                     Fall_LowSpeed;              // falling edge of LowSpeed;

reg                      HighSpeed_r;                // HighSpeed delayed by one clock

reg                      HighSpeed_r1;               // HighSpeed delayed by two clocks

reg                      FullSpeed_r;                // FullSpeed dalayed by one clock

reg                      FullSpeed_r1;               // FullSpeed dalayed by two clocks

reg                      LowSpeed_r;                 // LowSpeed dalayed by one clock

reg                      LowSpeed_r1;                // LowSpeed dalayed by two clocks

   
`include "carbonx_usb_params.vh"

//for synthesis

//    include "/eda/zaiq/prep4.0/tools/tbp/include/verilog_include/dvpf_parms.vh"
`include "dvpf_parms.vh"

//define the BFM_OpCode

//  parameter
//      BFM_NOP             =    32'h1,
      
//      BFM_WRITE           =    32'h5,
      
//      BFM_READ            =    32'h4;
      
//user define operation
      
 //     BFM_RST_DEV         =    32'h1ff,
      
 //     BFM_SUS_RE_DEV      =    32'h1fff;
      
      
//Begin The NOP Block
//////////////////////////////


always @ (posedge Clock)
   begin
       if(Reset)
          BFM_Nop_Reg <= 0;
       else 
          BFM_Nop_Reg <= (XIF_get_operation == BFM_NOP);
   end
   
   
//End of NOP ///
///////////////////////////////


//Begin BFM READ Block
/////////////////////////////////

assign BFM_Read_tmp = (XIF_get_operation == BFM_READ) & XIF_xav;

always @(posedge Clock)
  begin
      if(Reset)
         BFM_Read_Data <= 0;
      else if(ReadEnd | TimeOut)
         BFM_Read_Data <= 0;
      else if(BFM_Read_tmp)
         BFM_Read_Data <= 1;
  end
  
//End of BFM READ Block ////
//////////////////////////////////

         
  
//Begin BFM WRITE Block
//////////////////////////////////

assign BFM_Write_tmp = (XIF_get_operation == BFM_WRITE) & XIF_xav;

reg [7:0] ipg_count;
reg ipg_enable;

always @(posedge Clock)
  begin
      if(Reset) begin
         BFM_Write_Data <= 0;
         ipg_enable <= 0;
         ipg_count <= 0;
      end
      else if(LastData)
         BFM_Write_Data <= 0;
      else if(BFM_Write_tmp && (XIF_get_address[7:0] == 0))
         BFM_Write_Data <= 1;
      else if(BFM_Write_tmp && (XIF_get_address[7:0] != 0)) begin
         ipg_enable <= 1;
         ipg_count <= XIF_get_address[7:0];
      end
      else if(ipg_enable && (ipg_count > 0)) begin
         ipg_count <= ipg_count - 1;
      end
      else if(ipg_enable && (ipg_count < 1)) begin
         ipg_enable <= 0;
         BFM_Write_Data <= 1;
      end
  end

                 
//End of BFM WRITE Block
///////////////////////////////////


//Begin BFM RESET Block
//////////////////////////////////
// By setting Config[RESET_DEV_BIT] to 1 the user indicates that he wants to do a reset.
// Changing the USB speed also requires a reset, followed by a High-Speed Detection Handshake
// Highspeed_Handshake_Detect_Finish being asserted indicates the completion the
// High-Speed Detection Handshake.

always @(posedge Clock)
  begin
    if(Reset)
      begin
	HighSpeed_r  <= 0;
	HighSpeed_r1 <= 0;
	FullSpeed_r  <= 0;
	FullSpeed_r1 <= 0;
	LowSpeed_r   <= 0;
	LowSpeed_r1  <= 0;
      end
    else
      begin
	HighSpeed_r  <= HighSpeed;
	HighSpeed_r1 <= HighSpeed_r;
	FullSpeed_r  <= FullSpeed;
	FullSpeed_r1 <= FullSpeed_r;
	LowSpeed_r   <= LowSpeed;
	LowSpeed_r1  <= LowSpeed_r;
      end
  end

wire[1:0] ConfigSPEED = Config[BUS_SPEED_BITS_H:BUS_SPEED_BITS_L];   // Config[1:0] (1/2006)

assign HighSpeed      = (ConfigSPEED == USB_HS);
assign FullSpeed      = (ConfigSPEED == USB_FS); 
assign LowSpeed       = (ConfigSPEED == USB_LS);

assign Rise_HighSpeed = HighSpeed & (!HighSpeed_r1);

assign Fall_HighSpeed = (!HighSpeed) & HighSpeed_r1;

assign Rise_FullSpeed = FullSpeed & (!FullSpeed_r1);

assign Fall_FullSpeed = (!FullSpeed) & FullSpeed_r1;

assign Rise_LowSpeed  = LowSpeed & (!LowSpeed_r);

assign Fall_LowSpeed  = (!LowSpeed) & LowSpeed_r1;
 

assign BFM_Reset_tmp = Config[RESET_DEV_BIT] | (Rise_HighSpeed & Fall_FullSpeed) |(Rise_FullSpeed & Fall_HighSpeed)
                                      | (Rise_HighSpeed & Fall_LowSpeed) | (Rise_LowSpeed & Fall_HighSpeed)
                                      | (Rise_LowSpeed & Fall_FullSpeed) | (Rise_FullSpeed & Fall_LowSpeed);

always @(posedge Clock)
  begin
      if(Reset)
         BFM_Reset_Device <= 0;
//      else if(Highspeed_Handshake_Detect_Finish && (Config[RESET_DEV_BIT] == 0))
//         BFM_Reset_Device <= 0;
//      else if(BFM_Reset_tmp)
    else
//         BFM_Reset_Device <= 1;
      BFM_Reset_Device <= BFM_Reset_tmp;
  end

                 
//End of BFM RESET Block
///////////////////////////////////
                  
                  
//Begin BFM SUSPEND/RESUME Block
//////////////////////////////////

assign BFM_SusRe_tmp = Config[SUSRESUME_DEV_BIT];

always @(posedge Clock)
  begin
      if(Reset)
         BFM_Suspend_Resume_Device <= 0;
      else if(SusReFinish)
         BFM_Suspend_Resume_Device <= 0;
      else if(BFM_SusRe_tmp)
         BFM_Suspend_Resume_Device <= 1;
  end

                 
//End of BFM SUSPEND/RESUME Block
///////////////////////////////////
           
endmodule                                  
