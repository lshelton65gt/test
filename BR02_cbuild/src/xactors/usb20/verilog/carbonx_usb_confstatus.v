// ///////////////////////////////////////////////////////////////////
// The Config and Status Register Block of BFM
//////////////////////////////////////////////////////////////////////
//
// 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
// Comment       : 
//
// This block receives cs_data from XIF and then decodes it to control 
// BFM transfer data,then receives BFM internal status signals and writes them 
// to Config_Status_Reg, then sends it on BFM_put_csdata bus ,send to XIF
//
//
// Update: 1) 073003: change XIF_get_csdata[31:0] to change XIF_get_csdata[95:0]
//                    change BFM_put_csdata t0 96bit;
//         2) 073003: combine the small always block to a big one
//         3) 081203: change XIF_get_csdata[95:0] to 192bit, so we can define 6 
//                    32bits registers:address = 0x00 is Config,0x04 is 
//                    Reset_counter,0x08 is ExpectedDuration_Reg, 0x0C is Status_Reg,
//                    0x10 is FirstTime_Reg and 0x14 is CompletionTime_Reg. The first 3 
//                    registers are wtiten by SIM_CsWrite and last 3 registers are read 
//                    by SIM_CsRead. 
//               
// Revision      : $Revision$
// Log           :
//               : Revision 1.2  2006/01/17 03:37:33  jstein
//               : Reviewed by:  John Hoglund
//               : Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
//               : Bugs Affected: DW02_prod_sum1 sign extension
//               : Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
//               : removed all `defines from xactor verilog (mostly usb)
//               : added test/xactors/usb20
//               :
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.3  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.2  2005/09/29 23:53:43  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done:  checkin, xactor regression
//               : Bugs Affected: none
//               :  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
//               :  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
//               :  so they will not be seen by the customer
//               :  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.15  2004/09/15 18:41:47  fredieu
//               : Changes for 16 bit at 60MHz
//               :
//               : Revision 1.14  2004/08/06 13:51:36  zippelius
//               : Updated/fixed test23, fixed numerous issues around User Reset
//               :
//               : Revision 1.13  2004/07/24 03:44:11  fredieu
//               : Need to hold the timeout info.
//               :
//               : Revision 1.12  2004/06/30 03:05:00  fredieu
//               : Fix to read back the config register.
//               :
//               : Revision 1.11  2004/06/24 19:49:07  fredieu
//               : Remove some old stuff.
//               : Register the timeout.
//               :
//               : Revision 1.10  2004/06/21 19:45:14  fredieu
//               : Add an external fast_negotiate signal.
//               :
//               : Revision 1.9  2004/06/18 22:14:45  fredieu
//               : Make Attach a full cycle.
//               :
//               : Revision 1.8  2004/06/04 18:02:03  fredieu
//               : Added counter to replace PLAT_GetSimTime
//               :
//               : Revision 1.7  2004/04/26 21:54:26  jafri
//               : modified english in verilog and c code and added usb_test1a to test table
//               :
//               : Revision 1.6  2004/04/26 21:34:29  zippelius
//               : Code clean-up regarding IRP state and misc other
//               :
//               : Revision 1.5  2004/04/22 13:44:29  zippelius
//               : Clean-up defines, messaging, others
//               :
//               : Revision 1.4  2004/04/20 20:54:10  zippelius
//               : Losts of code clean-up, comment fixes
//               :
//               : Revision 1.3  2004/04/15 14:50:54  zippelius
//               : Major code clean-ups. Upgraded USB IP core to latest version. Fixed call-backs.Added Error masking. Fixed LineState state machines.
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.3  2003/12/04 02:35:49  adam
//               : add SuspendM
//               :
//               : Revision 2.2  2003/12/03 02:12:39  serlina
//               : add vcontrol
//               :
//               : Revision 2.1  2003/09/27 10:46:15  thor
//               : Debug
//               :
//               : Revision 2.0  2003/09/27 07:39:09  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.3  2003/09/26 03:56:57  adam
//               : add load completetime when timeout
//               :
//               : Revision 1.2  2003/09/24 09:41:54  adam
//               : add timeout complete time
//               :
//               : Revision 1.1  2003/09/23 07:54:10  thor
//               : split into two dir
//               :
//               : Revision 1.16  2003/09/10 14:12:37  thor
//               : add comment
//               :
//               : Revision 1.15  2003/09/10 07:58:32  thor
//               : add comment
//               :
//               : Revision 1.14  2003/09/09 09:44:57  thor
//               : change Config_Reg bits
//               :
//               : Revision 1.13  2003/09/05 02:00:34  thor
//               : add blank
//               :
//               : Revision 1.12  2003/09/03 09:07:13  thor
//               : add blank
//               :
//               : Revision 1.11  2003/09/02 10:35:45  thor
//               : get rid of unuse signal
//               :
//               : Revision 1.10  2003/09/01 13:35:06  thor
//               : add  PID_SOF time
//               :
//               : Revision 1.9  2003/09/01 11:02:46  thor
//               : add PID_SOF transfer time
//               :
//               : Revision 1.8  2003/08/25 12:55:51  thor
//               : change ExpectedDuration
//               :
//               : Revision 1.7  2003/08/25 07:22:44  thor
//               : change Config_Reg/Reset_Reg/ExpectedDuration_Reg from reg to wire and change name
//               :
//               : Revision 1.6  2003/08/25 06:14:22  thor
//               : change Status_Reg
//               :
//               : Revision 1.5  2003/08/15 12:56:44  serlina
//               : to debug SIM_CsRead, add status regiser and other registers' value to BFM_put_data's high 96bits
//               :
//               : Revision 1.4  2003/08/15 11:44:31  thor
//               : get rid of BFM_Read_Data_r
//               :
//               : Revision 1.3  2003/08/15 01:59:18  thor
//               : add blank
//               :
//               : Revision 1.2  2003/08/14 13:40:49  thor
//               : Updated DevSpeed for FS Only case
//               :
//
//***************************************************************************

`timescale 1ns/1ns

module carbonx_usb_confstatus(Clock, Clock_60,
              Reset,
              
              XIF_get_csdata,
              BFM_put_csdata,
              BFM_Write_Data,
              
              Config,
              Time_Counter,
              FS_Attach,
              CRCErr,
              PIDErr,
              Highspeed_Handshake_Detect_Finish,
              TimeOut,
              TimeOut_Load,
              RxValid,
              TxReady,

              Dev_Speed_pre,
              Load_Dev_Speed,
              
              RxDone,
              Attach,
              LS_Attach,
              FullSpeed,
              LowSpeed,
              HighSpeed,
              XcvrSelect,
              TermSelect,
              DataBus16_8,
              TxValid,
              PID_SOF,
              VStatus,
              VcontrolLoadM,
              Vcontrol,
              SuspendM
)/* synthesis syn_hier = "firm" */;

// parameter USB_GCS_WIDTH = 64;
// parameter USB_PCS_WIDTH = 160;
`include "carbonx_usb_params.vh"
   
//input signals
input         Clock;
input         Clock_60;

input         Reset;

//input[191:0]  XIF_get_csdata;   // ? USB_GCS_WIDTH = 64 (1/2006) ?
input[USB_GCS_WIDTH-1:0]  XIF_get_csdata; 

input         FS_Attach;                        //device speed after highspeed handshake detect 

input         CRCErr;                           //crc error

input         PIDErr;                           //PID error

input         Highspeed_Handshake_Detect_Finish;//finished highspeed handshake detect

input         RxValid;                          //output bus to DUT has valid data 

input         TxReady;                          //input data bus from DUT has valid data

input         TimeOut;                          //host wait time out

input         TimeOut_Load;

input         RxDone;                           //BFM receive data from DUT finish 

input         BFM_Write_Data;                   //XIF write data to BFM enable signal

input         XcvrSelect;                       //highspeed or full speed transactor select signal

input         DataBus16_8;                      //indicate DUT databus is 16bits or 8bits

input         TermSelect;                       //termination select signal

input         VcontrolLoadM;                    //vendor control message load signal

input[3:0]    Vcontrol;                         //vendor control data 

input         TxValid;                          //data bus input to BFM have valid data

input         PID_SOF;                          //c-side send a SOF packet,in order to calculate SOF packet transfer time

input[1:0]    Dev_Speed_pre;

input         Load_Dev_Speed;

input         SuspendM;

//output signal

output [USB_PCS_WIDTH-1:0] BFM_put_csdata;                   // [159:0] 1/2006

output        FullSpeed;                        //working speed 

output        LowSpeed;                         //use low speed transfer packet

output        HighSpeed;                        //

output        Attach;                           //no low speed device device attach

output        LS_Attach;                        //low speed device attach 

output[7:0]   VStatus;                          //vendor status

output[31:0]  Config;                           //configuration value

output [31:0] Time_Counter;

//internal wire and reg signals

wire[31:0]    Config;

wire [31:0]   Time_Counter;

wire[31:0]    ExpectedDuration;                //Stores expected duration [in bus cycles] of transaction

wire[31:0]    Status;                          //Because we need to get status value a clock early than when we think before ,so we
                                               //do not use a register;

reg[31:0]     FirstTime_Reg;                   //transfer start time

reg[31:0]     CompletionTime_Reg;              //transfer completion time

reg[31:0]     Time_cnt;                        //a counter to count transfer start and finish time
reg[31:0]     Time_cnt_1;

reg           BFM_Write_Data_r;

reg           TimeOut_Exp;                     //a transfer's time longer than expect time

//wire          TimeOut_two;                     //TimeOut | TimeOut_Exp
reg          TimeOut_two;                     //TimeOut | TimeOut_Exp

wire          Rise_BFM_Write;  

wire          MResetErr;                       //when dut receive or send data,the reset signal
                                               //assert, that is an error.
                                             
reg           FS_Attach_h;                     //FS_Attach hold signal

reg           Attach_r;                        //one clock cycle delay of Config[DEVICEATTACH_BIT]=1;
reg           Attach_r1;                        //one clock cycle delay of Config[DEVICEATTACH_BIT]=1;

reg[3:0]      Vcontrol_Reg;

reg[1:0]      DevSpeed;                        //after Highspeed_handshake_detect,the device's speed:01 is fullspeed,10 is highspeed;
                                               //Because lowspeed device does'nt do Highspeed_handshake_detect,there has no lowspeed.

wire          Fall_TxValid;                    //fall edge of Txvalid signal

reg           TxValid_r;                       //one clock delay of Txvalid

reg           DataBus16_8_r;
   


assign LS_Attach      = Config[LS_ATTACH_BIT];

//
always @(posedge Clock)
  begin
   if(Reset)
    begin 
     BFM_Write_Data_r <= 0;
     TxValid_r        <= 0;
     DataBus16_8_r    <= 0;
    end
   else
    begin
     BFM_Write_Data_r <=  BFM_Write_Data;
     TxValid_r        <=  TxValid;
     DataBus16_8_r      <= DataBus16_8;
    end
  end



assign Rise_BFM_Write = BFM_Write_Data & (!BFM_Write_Data_r);
assign Fall_TxValid   = !TxValid_r & TxValid;

 
assign Config           = XIF_get_csdata[USB_HC_CONFIG_BITS_H:USB_HC_CONFIG_BITS_L];
assign ExpectedDuration = XIF_get_csdata[USB_HC_EXPECTED_DURATION_BITS_H:USB_HC_EXPECTED_DURATION_BITS_L];  

assign Time_Counter = Time_cnt_1;
  
//Begin Status  Block////////////////////////////////////
/////////////////////////////////////////////////////////////
  
assign Status = { 18'b0, Vcontrol_Reg, SuspendM, DataBus16_8_r, FS_Attach_h, Highspeed_Handshake_Detect_Finish,
      RxDone, TimeOut_two || TimeOut, MResetErr, PIDErr, CRCErr, DevSpeed };
  
assign BFM_put_csdata = { Config, Time_cnt_1, CompletionTime_Reg, FirstTime_Reg, Status };
//assign BFM_put_csdata = { CompletionTime_Reg, FirstTime_Reg, Status };
//assign BFM_put_csdata = { Time_cnt, FirstTime_Reg, Status };


always @(posedge Clock)
  begin
   if(Reset)
     FS_Attach_h <= 0;
   else if(Config[REMOVE_BIT])
     FS_Attach_h <= 0;
   else if(FS_Attach)
     FS_Attach_h <= 1;
  end
      


//End of Status_Reg block//////////////////////////////////////
///////////////////////////////////////////////////////////////

// Begin TimeOut and speed decode Block///////////////////////// 
///////////////////////////////////////////////////////////////
// We store the start and end time of a transaction, then calculate 
// the time the transaction took and compare this time with the time that host 
// expect this transaction to take.
// If the transaction time is greater than the time expected by the host,
// then a timeout condition is being signaled.

always @(posedge Clock)
 begin
  if(Reset)
   Time_cnt <= 32'b0;
  else 
   Time_cnt <= Time_cnt + 32'd1;
 end

always @(posedge Clock_60)
 begin
  if(Reset)
   Time_cnt_1 <= 32'b0;
  else 
   Time_cnt_1 <= Time_cnt_1 + 32'd1;
 end


always @(posedge Clock)
 begin
  if(Reset)
   FirstTime_Reg <= 32'b0;
  else if(Rise_BFM_Write)
   FirstTime_Reg <= Time_cnt;
 end

always @(posedge Clock)
 begin
  if(Reset)
   CompletionTime_Reg <= 32'b0;
  else if(Fall_TxValid | TimeOut_Load)
   CompletionTime_Reg <= Time_cnt ;
  else if(PID_SOF)
   CompletionTime_Reg <= FirstTime_Reg + 32'd5;
 end


always @(posedge Clock)
 begin
  if(Reset)
    TimeOut_Exp <=0;
  else
    TimeOut_Exp <= ((CompletionTime_Reg - FirstTime_Reg) > ExpectedDuration) & RxDone;   
 end

always @(posedge Clock)
 begin
  if(Reset) TimeOut_two <= 0;
  else if(BFM_Write_Data) TimeOut_two <= 0;
  else if(!TimeOut_two) TimeOut_two <= TimeOut_Exp | TimeOut;
end

//assign TimeOut_two = TimeOut_Exp | TimeOut ;


assign MResetErr = (RxValid | TxReady) & Reset ;

always @(posedge Clock)
 begin
  if(Reset) begin
    Attach_r <=0;
    Attach_r1 <= 0;
  end
  else begin
    Attach_r <= Config[DEVICEATTACH_BIT];
    Attach_r1 <= Attach_r;
  end
 end
 
//assign Attach    = Config[DEVICEATTACH_BIT] & (!Attach_r);
assign Attach    = Attach_r & ~Attach_r1;

 wire [1:0] ConfigSPEED = Config[BUS_SPEED_BITS_H:BUS_SPEED_BITS_L]; // 1:0 (1/2006)
   
assign LowSpeed  = (ConfigSPEED == USB_LS);  // 1 (1/2006)
assign FullSpeed = (ConfigSPEED == USB_FS);  // 2 (1/2006)
assign HighSpeed = (ConfigSPEED == USB_HS);  // 3 (1/2006)


always @(posedge Clock)
 begin
  if(Reset)
    DevSpeed <= 2'b0;
  else if(LS_Attach)
    DevSpeed <= 2'b01;
  else if(Load_Dev_Speed)
    DevSpeed <= Dev_Speed_pre ;
 end



always @(posedge Clock)
 begin
  if(Reset)
    Vcontrol_Reg <= 4'b0;
  else if(VcontrolLoadM)
    Vcontrol_Reg <= Vcontrol ;
 end

assign VStatus = Config[VStatus_BITS_H:VStatus_BITS_L];

//End of TimeOut Block//////////////////////////////////
////////////////////////////////////////////////////////

endmodule
