// ///////////////////////////////////////////////////////////////////
// The Rx And PID Decode and  CRC16 Check and DataInReg Block of BFM
//////////////////////////////////////////////////////////////////////
// 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
//
//
// This module receives data from the device,does PID decode, and if it is a
// token then sends it to XIF; if it is a data packet, gets rid of the last 
// 2byte(crc16) and sends the results to XIF.Because the input databus 
// is 16bit or 8bit(This IP core is only 8bit), and the output bus to XIF 
// is 32bit, we use a Rx_Reg to receive 32bit data to send to XIF.
//////////////////////////////////////////////////////////////////////////



`timescale 1ns/1ns

module carbonx_usb_rxdatainreg (
                         Clock,     // should always be 60 MHz
                         Clock_30,  // used at utmi in 16 bit mode
                         Reset,
                         
                         //UTMI interface 
                         TxValid,
                         TxValidH,
                         TxReady,
                         DataBus16_8,
                         DataIn0_7,
                         DataIn8_15,
                         OpMode,
                         
                         //XIF_core interface
                         BFM_put_data,
                         BFM_put_size,
                         BFM_put_dwe,
                         BFM_gp_daddr,
                         BFM_xrun,
                         
                         //internal block interface
                         BFM_Read_Data,
                         PIDErr,
                         CRCErr,
                         RxDone,
                         TimeOut,
                         Config,
                         TxValid_out
)/* synthesis syn_hier = "firm"*/;

//input signals
input                   Clock;             //system clock always 60 MHz
input                   Clock_30;          //system clock used for 16 bit mode
input                   Reset;             //reset signal;
input                   TxValid;           //UTMI input bus low 8bit have valid data;
input                   TxValidH;          //UTMI input bus high 8bit have valid data;
input                   DataBus16_8;       //indicates if UTMI use 16bits or 8bits bus;
input[7:0]              DataIn0_7;         //UTMI low 8bit input;
input[7:0]              DataIn8_15;        //UTMI high 8bit input;
input                   BFM_Read_Data;     //OpCode output, indicate XIF read data from BFM;
input[1:0]              OpMode;            //UTMI input signal
input                   TimeOut;           //the current transfer is timeout
input [31:0]            Config;

//output signals
output                  TxReady;           //BFM input UTMI input data into DataIn_Reg;
output                  PIDErr;            //the received packet's PID error;
output                  CRCErr;            //Data from UTMI CRC error;
output                  RxDone;            //send data to dut finish
output                  BFM_put_dwe;       //memory write enable signal;
output[31:0]            BFM_put_data;      //output data
output[23:0]            BFM_put_size;      //the size of BFM output data;
output[14:0]            BFM_gp_daddr;      //memory address;
output                  BFM_xrun;          //indicate that BFM is doing transfer
output                  TxValid_out;       // used by other modules instead of TxValid

//define ouput signals type
reg[31:0]               BFM_put_data; 
reg[23:0]               BFM_put_size;
reg                     BFM_xrun;


//internal wire and reg signals
wire                    PID_Token;         //packet's PID is a token packet;
wire                    PID_DATA;          //packet's PID is a data packet;
wire                    PID_ACK;           //ACK token; the same as following;
wire                    PID_NAK;           //NAk token
wire                    PID_STALL;         //
wire                    PID_NYET;          //
wire                    PID_ERR;           //PID ERR
wire                    PID_DATA1;         //
wire                    PID_DATA0;         //
wire                    PID_DATA2;         //
wire                    PID_DATAM;         //
wire                    Rise_TxValid;      //rise edge of TxValid

wire                    Count_rx_inc;      //Count_rx increase control signal;
wire[2:0]               Count_rx_tmp;      //Count_rx  register input;
wire                    Count_rx_set;      //Count_rx set signal;

//wire                    PID_Data_h_tmp;    //PID_Data hold signal input 
wire                    Fall_TxValid;      //fall edge of TxValid
wire                    Fall_TxValidH;     //fall edge of TxValidH
wire                    Rise_BFM_Read;     //Rise edge of BFM_Read_Data signal
wire[14:0]              Daddr_cnt_tmp;     //gp_daddr counter input
wire[3:0]               Data_PID_tmp;      //data PID tmp

reg[15:0]               DataIn_Reg;        //data register of dat input from device

reg[2:0]                Count_rx;          //in order to put the correct data of DataIn_Reg
                                           //to Rx_Reg on correct clock cycle.because it is
                                           //8bit input and 32bit output

reg[14:0]               Daddr_cnt;         //gp_daddr counter

reg                     PID_Token_r;       //one clock cycle delay of  PID_Token
reg                     PID_Token_h;       //PID_Token holding signal
reg                     BFM_Read_Data_r;   //one clock cycle delay of BFM_Read_Data 
reg                     Rise_TxValid_rr;   //one cycle delay of Rise_TxValid_r

reg                     TxValid_r;         //one cycle delay of TxValid
//reg                     TxValidH_r;        //one cycle delay of TxValidH
reg                     Fall_TxValid_r;    //one cycle delay of Fall_TxValid
//reg                     Fall_TxValidH_r;   //one cycle delay of Fall_TxValidH
reg                     Fall_TxValid_rr;   //one cycle delay of Fall_TxValid_r
//reg                     Fall_TxValidH_rr;  //one cycle delay of Fall_TxValidH_r
reg                     Rise_TxValid_r;    //one cycle delay of  Rise_TxValid                    
reg                     PID_Data_h;        //PID_Data holding signal
reg[31:0]               Byte_cnt;          //byte counter

reg                     dwe_setup_in;      //write Setup Status stage In token;

//output data register
wire[31:0]              Rx_Reg;            //32bit receive register
reg[7:0]                Rx_Reg_tmp_Token;  //PID_Token receive register
reg[31:0]               Rx_Reg_tmp_Data;   //data receiver register


//CRC16 control
wire [7:0]              CRC16_Din_cal1;    //rise clock edge crc16 calculate block data in
reg[15:0]               CRC16_sum_cal1;    //rise clock edge crc16 calculate block tmp output
wire[15:0]              CRC16_out_cal1;    //rise clock edge crc16 calculate block last output

reg TxReady_r;
wire TxReady_enable;

wire [7:0] DataIn0_7_int;
reg 			 Clock_30_int;
reg 			 Clock_30_phased;
reg 			 TxValid_30_r;
reg 			 TxValidH_30_r;
reg 			 TxValidH_30_rr;
wire 			 TxValid_int;
reg  [7:0] DataIn0_7_30_r;
reg  [7:0] DataIn8_15_30_r;

`include "carbonx_usb_params.vh"	
	
//Start Main module
//////////////////////////////////////////////

//Begin Rise_TxValid and Fall_TxValid  block
///////////////////////////////////////////////

assign TxValid_out = TxValid_int;

  
always @(negedge Clock)
  Clock_30_phased <= !Clock_30;

always @(posedge Clock)
  begin
    if(Reset)
      begin
        TxValid_r  <= 0;
        Clock_30_int <= 0;
      end
    else
      begin
        TxValid_r  <= TxValid_int ;
        Clock_30_int <= Clock_30_phased;
      end
  end


always @(posedge Clock)
  begin
    if(Reset)
      begin
//        TxValid_30_r <= 0;
        TxValidH_30_r <= 0;
//        TxValidH_30_rr <= 0;
      end
    else if(!Clock_30_int && DataBus16_8)
      begin
//        TxValid_30_r <= TxValid;
        TxValidH_30_r <= TxValidH;
//        TxValidH_30_rr <= TxValidH_30_r;
      end
    else
      begin
//        TxValid_30_r <= 0;
        TxValidH_30_r <= 0;
      end
  end


always @(posedge Clock)
	begin
		if(Reset)
			begin
//				DataIn0_7_30_r <= 8'b0;
				DataIn8_15_30_r <= 8'b0;
			end
		else if(DataBus16_8)
			begin
				if(!Clock_30_int)
					begin
//						DataIn0_7_30_r <= DataIn0_7;
						DataIn8_15_30_r <= DataIn8_15;
					end
				end
//		else DataIn0_7_30_r <= 8'b0;
		else DataIn8_15_30_r <= 8'b0;
	end
				

//assign  DataIn0_7_int = DataBus16_8 ? (Clock_30_int ? DataIn0_7_30_r : DataIn8_15_30_r) :
//                        DataIn0_7;
assign  DataIn0_7_int = DataBus16_8 ? (Clock_30_int ? DataIn8_15_30_r : DataIn0_7) :
                        DataIn0_7;
												
//assign  TxValid_int   = DataBus16_8 ? (TxValid_30_r || TxValidH_30_rr) : TxValid;
assign  TxValid_int   = DataBus16_8 ? (Clock_30_int ? TxValidH_30_r : TxValid) : TxValid;
 
assign  Rise_TxValid  = TxValid_int   & (~TxValid_r) & (OpMode != 2'b10);

assign  Fall_TxValid  = TxValid_r & (~TxValid_int)   & (OpMode != 2'b10) ;


//End of Rise_TxValid and Fall_TxValid  block
//////////////////////////////////////////////////                         
                         

//Begin DataIn_Reg  block
//////////////////////////////////////////////////
//when device does Highspeed Handshake Detect,it will drive K chirp on the bus for 1.2 ms,
//Since this is not input data, DataIn_Reg does not receive this K chirp.
//And when TxValid is asserted, it indicates that device has data to send, TxReady indicates 
//that the BFM is ready to receive data.

always @(posedge Clock)
   begin
     if(Reset)
       DataIn_Reg[7:0] <= 8'b0;
     else if(TxValid_int && TxReady && (OpMode != 2'b10))
       DataIn_Reg[7:0] <= DataIn0_7_int;
     else
       DataIn_Reg[7:0] <= 8'h0;
   end
   

//End of DataIn_Reg block
//////////////////////////////////////////////////
                            
//Begin PID Decode block
/////////////////////////////////////////////////
//once TxValid is asserted, the transactor begins to send SYNC on 
//the bus,so we at the Rise_TxValid_r clock accept first 8bits
//data into DataIn_Reg,and then at next clock decode the PID,at the same 
//time send it to Rx_Reg[7:0] 

always @(posedge Clock)
   begin
      if(Reset)
         Rise_TxValid_r <= 0;
      else
         Rise_TxValid_r <= Rise_TxValid;
   end
   
   
assign Data_PID_tmp = DataIn_Reg[3:0];

assign PIDErr       = Rise_TxValid_r & (DataIn_Reg[3:0] != {~DataIn_Reg[7],~DataIn_Reg[6],~DataIn_Reg[5],~DataIn_Reg[4]});

assign PID_ACK      = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_ACK) & !PIDErr;

assign PID_NAK      = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_NACK) & !PIDErr;

assign PID_STALL    = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_STALL) & !PIDErr;

assign PID_NYET     = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_NYET) & !PIDErr;

assign PID_ERR      = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_ERR) & !PIDErr;

assign PID_DATA0    = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_DATA0) & !PIDErr;

assign PID_DATA1    = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_DATA1) & !PIDErr;

assign PID_DATA2    = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_DATA2) & !PIDErr;

assign PID_DATAM    = Rise_TxValid_r & (Data_PID_tmp == USB_BFM_PID_MDATA) & !PIDErr;

assign PID_Token    = PID_ACK | PID_NAK | PID_STALL | PID_NYET | PID_ERR ;

assign PID_DATA     = PID_DATA0 | PID_DATA1 | PID_DATA2 | PID_DATAM ;


//End of PID Decode block
///////////////////////////////////////////////////////



//Begin Count_rx block
/////////////////////////////////////////////////////////
//use this counter to control DataIn_Reg's data to Rx_Reg[31:0]'s.
//When DataBus16_8 = 0, output 32bit data every 4 clocks cycles, counter is set to 1 every 4 clocks.
//when DataBus16_8 = 1, output 32bits data to XIF every 2 clock cycles, counter is set to 1 every 2 clocks.

assign Count_rx_tmp     = Count_rx_set ? 3'b1:(BFM_Read_Data ? (Count_rx + 3'b1): Count_rx); 

assign Count_rx_inc     = Rise_TxValid | PID_DATA | PID_Data_h; // similar to  TxReady!!

assign Count_rx_set     = (Count_rx == 4) & BFM_Read_Data;

// PID_Data hold signal   
always @(posedge Clock)
   begin
      if(Reset)
        PID_Data_h  <= 0;
      else if(Fall_TxValid | RxDone) 
        PID_Data_h  <= 0;
      else 
        PID_Data_h  <= PID_DATA ? 1 :PID_Data_h;
    end
    
always @(posedge Clock)
   begin
      if(Reset)
        Count_rx <= 3'b0;
      else if((!Count_rx_inc) | Fall_TxValid)
        Count_rx <= 3'b0;
      else if(Count_rx_inc && TxReady)
        Count_rx <= Count_rx_tmp;
   end
   
//End of Count_rx block
/////////////////////////////////////////////////////////////////

//Begin gp_daddr Counter
/////////////////////////////////////////////////////////////////
// Data can be transfered as 8bit or 16 bit blocks to complete 32bit data. If we are in 8bit transfer mode
// it takes 4 cycles to get the complete 32bit block and in 16bit transfer mode it takes 2 cycles 
// Depending on the mode of operation address (gp_daddr) is incremented every 4 cycles or every 2 cycles.
// gp_daddr is used to receive data. When received data is 8bit Token, gp_daddr need not increase,
// When received data is Data and greater than 32bits and DataBus16_8 = 0, every 4 clocks gp_daddr is incremented by 1;
// When received data is Data and greater than 32bits and DataBus16_8 = 1, every 2 clocks gp_daddr is incremented by 1;
// DataBus16_8 set to 0, 
//     At Count_rx=1 send DataIn_Reg[7:0] to Rx_Reg[7:0],
//     At Count_rx=2 send DataIn_Reg[7:0] to Rx_Reg[15:8],
//     At Count_rx=3 send DataIn_Reg[7:0] to Rx_Reg[23:16],
//     At Count_rx=4 send DataIn_Reg[7:0] to Rx_Reg[31:24];
// DataBus16_8 set to 1
//     At Count_rx=1 send DataIn_Reg[15:0] to Rx_Reg[15:0],
//     At Count_rx=2 send DataIn_Reg[15:0] to Rx_Reg[31:16],
// The next clock, Rx_Reg's value is  the first 32bits of packet, at the same time assert put_dwe signal to write data 
// to XIF's memory and increment gp_daddr by 1 .

assign Daddr_cnt_tmp   = (!Rise_TxValid_r & BFM_Read_Data & (Count_rx == 1) && TxReady) ? (Daddr_cnt + 1) : Daddr_cnt;

always @(posedge Clock)
  begin
    if(Reset)
      Rise_TxValid_rr <= 0;
    else 
      Rise_TxValid_rr <= Rise_TxValid_r;
  end

 
always @ (posedge Clock )
  begin
    if(Reset)
      Daddr_cnt  <= 15'b0;
    else if(Rise_TxValid | RxDone)
      Daddr_cnt  <= 15'b0;
    else
      Daddr_cnt  <= Daddr_cnt_tmp;
  end
  
assign BFM_gp_daddr = Daddr_cnt;

//End of gp_daddr counter block
//////////////////////////////////////////////////////////////////


//Begin TxReady and BFM_put_dwe block
/////////////////////////////////////////////////////////////////

always @(posedge Clock )
   begin
     if(Reset)
       PID_Token_h <= 0;
     else if(BFM_put_dwe )
       PID_Token_h <= 0;
     else if(PID_Token | (PID_DATA & Fall_TxValid))
       PID_Token_h <= 1;
   end

always @(posedge Clock )
   begin
     if(Reset)
       PID_Token_r <= 0;
     else 
       PID_Token_r <= PID_Token;
   end
   
always @(posedge Clock)
   begin
     if(Reset)
       BFM_Read_Data_r <= 0;
     else 
       BFM_Read_Data_r <= BFM_Read_Data;
   end

//write zero-length packet to XIF

always @(posedge Clock)
   begin
     if(Reset)
       dwe_setup_in <= 0;
     else 
       dwe_setup_in <= PID_DATA & Fall_TxValid;
   end
 
assign Rise_BFM_Read =  BFM_Read_Data & (!BFM_Read_Data_r);

//If received packet is Token, at the PID_Token_r clock cycle write 8bits to memory;
//If received packet is data and received data is not the last 32bits, Count_rx = 1 write;
//else received data is the last 32bits data, we need to get rid of 16bit crc16 data,and
//when Fall_TxValid is asserted, the transfer is completed and the last 16bits data are crc16
//data, so we use Count_rx to determine to write or not, and write how many bits.

assign  BFM_put_dwe   = (PID_Token_r | ((!PID_Token) & (Count_rx == 1) && TxReady_r & (!Fall_TxValid) & (!Rise_TxValid_r)))
                       | (!(PID_Token | PID_DATA) & Fall_TxValid & ((Count_rx==1) | (Count_rx==3) | (Count_rx==4)))
                       | dwe_setup_in;


//At Rise_TxValid_r clock, receive the 8bit PID, if it is Token only 8bits transfer;             //FIXME  
//else if it is data and DataBus16_8 = 1, every clock receive 16bits data(Count_rx =2 or 1 receive 16bits data,       //FIXME
//and Count_rx =1 and 2 send data to Rx_Reg. DataBus16_8=0,each clock must receive data from dut.                     //FIXME
                       
assign TxReady = Rise_TxValid | (PID_Data_h && TxReady_enable) | PID_DATA;

always @ (posedge Clock)
	begin
		if(Reset) TxReady_r <= 0;
		else TxReady_r <= TxReady;
	end
	

carbonx_usb_rxtrafficthrottle carbonx_usb_rxtrafficthrottle (
         .allow_stalls(PID_Data_h && Config[ENABLE_RX_STALLS]),
         .Clock(Clock),
         .Reset(Reset),
         .run(TxReady_enable)
);

always @(BFM_Read_Data or RxDone or TimeOut)
  begin
    BFM_xrun=0;
    if(RxDone | TimeOut)
      BFM_xrun = 0;
    else if(BFM_Read_Data)
      BFM_xrun = 1;
  end


              
//End of  TxReady and BFM_put_dwe block;
/////////////////////////////////////////////////////////////////////

//Begin Rx_Reg block
/////////////////////////////////////////////////////////////////////
// Data can be transfered as 8bit or 16 bit blocks to complete 32bit data.
// 8bit transfer is completed over 4 clocks with DataBus16_8 set to 0, 
//     At Count_rx=1 send DataIn_Reg[7:0] to Rx_Reg[7:0],
//     At Count_rx=2 send DataIn_Reg[7:0] to Rx_Reg[15:8],
//     At Count_rx=3 send DataIn_Reg[7:0] to Rx_Reg[23:16],
//     At Count_rx=4 send DataIn_Reg[7:0] to Rx_Reg[31:24];

  
always @(posedge Clock)
  begin
    if(Reset)
      Rx_Reg_tmp_Data[7:0] <= 8'b0;
    else if(Count_rx == 3'd1)
      Rx_Reg_tmp_Data[7:0] <= DataIn_Reg[7:0];
  end
   
always @(posedge Clock)
   begin
     if(Reset)
       Rx_Reg_tmp_Data[15:8] <= 8'b0;
     else if(Count_rx==3'd2)
       Rx_Reg_tmp_Data[15:8] <=  DataIn_Reg[7:0];
   end 

always @(posedge Clock)
   begin
     if(Reset)
       Rx_Reg_tmp_Data[23:16] <= 8'b0;
     else if(Count_rx==3'd3)
       begin
         Rx_Reg_tmp_Data[23:16] <= DataIn_Reg[7:0];
       end
   end 
     
     
always @(posedge Clock)
   begin
     if(Reset)
       Rx_Reg_tmp_Data[31:24] <= 8'b0;
     else if(Count_rx==3'd4)
       Rx_Reg_tmp_Data[31:24] <= DataIn_Reg[7:0];
   end 



//send Token just 8bits to send   
always @(posedge Clock)
   begin
     if(Reset)
       Rx_Reg_tmp_Token <= 8'b0;
     else if(PID_Token_h | PID_Token | (PID_DATA & Fall_TxValid))
       Rx_Reg_tmp_Token <= DataIn_Reg[7:0];
     else
       Rx_Reg_tmp_Token <= 8'b0;
   end

assign Rx_Reg  = PID_Data_h ? Rx_Reg_tmp_Data : {24'b0,Rx_Reg_tmp_Token};   
   
//End of Rx_Reg block
//////////////////////////////////////////////////////////

//Begin of BFM_put_data block
///////////////////////////////////////////////////////////
//when put data to BFM_put_data bus,we need to inverse bytes order to Rx_Reg:
//Rx_Reg[7:0] to BFM_put_data[31:24], and Rx_Reg[15:8] to BFM_put_data[23:16],
//Rx_Reg[23:16] to BFM_put_data[15:8],Rx_Reg[31:24] to BFM_put_data[7:0],

always @(PID_Token_h or PID_Data_h or Fall_TxValid or Rx_Reg or Count_rx or PID_Token_r)
   begin
     BFM_put_data        = 32'habcddcba;//to debug
     if(PID_Token_h | PID_Token_r)
       BFM_put_data = {Rx_Reg[7:0],24'b0};
     else if(PID_Data_h)
       begin
         if(!Fall_TxValid)
           //not last 32bits data, all data are valid
           begin
             BFM_put_data = {Rx_Reg[7:0],Rx_Reg[15:8],Rx_Reg[23:16],Rx_Reg[31:24]};
           end
         else if(Fall_TxValid)
           begin
             case (Count_rx)
               //There have last 8bits in DataIn_Reg[7:0], so Rx_Reg[31:24] is crc16 data; 
               1: begin
                 BFM_put_data = {Rx_Reg[7:0],Rx_Reg[15:8],Rx_Reg[23:16],8'b0};
               end
               //There have last 8bits in DataIn_Reg[7:0], so Rx_Reg[7:0] is crc16 data,
               //that there has no valid data to transfer;
               2: begin
                 BFM_put_data = 32'b0;
               end
               //There have last 8bits in DataIn_Reg[7:0], so Rx_Reg[15:8] is crc16 data;
               3: begin
                 BFM_put_data = {Rx_Reg[7:0],24'b0};
               end
               //There have last 8bits in DataIn_Reg[7:0], so Rx_Reg[23:16] is crc16 data;
               4: begin
                 BFM_put_data = {Rx_Reg[7:0],Rx_Reg[15:8],16'b0};
               end
               default: BFM_put_data = 32'b0;
             endcase
           end
       end
   end

//End of BFM_put_data block
/////////////////////////////////////////////////////////////////////////

//Begin Byte_cnt block
////////////////////////////////////////////////////////////////////////
        
always @ (PID_Token_h or  Fall_TxValid or Rx_Reg or Count_rx or PID_Token or Daddr_cnt)
  begin
    Byte_cnt = 32'b0;
    if(PID_Token_h | PID_Token)
      //only 1byte data to send
      Byte_cnt = 1;
    else if(Fall_TxValid)
      begin
        case (Count_rx)
          
          //Every time input 8bits data,when Count_rx=1,that is to say, want to write this
          //8bits data to Rx_Reg[7:0],so at this clock,data in Rx_Reg[31:24] are crc16 first byte data,
          //and Daddr_cnt not yet to add 1 and Daddr_cnt is count from 0, so Byte_cnt is ((Daddr_cnt+1)<<2'd2) -1,
          
          1: Byte_cnt = ((Daddr_cnt+1)<<2'd2) -1 ;
          
          //Count_rx=2,Daddr_cnt add 1 already,and last 16bits are crc16 data, so:
          
          2: Byte_cnt = ((Daddr_cnt)<<2'd2) ;
          
          //Count_tx=3,Daddr_cnt add 1 already and need to send 1byte data again,so this adress ia valid:
          
          3: Byte_cnt = ((Daddr_cnt+1)<<2'd2) -3 ;
          
          //Count_tx=4,Daddr_cnt add 1 already and need to send 2bytes data,so:
          
          4: Byte_cnt = ((Daddr_cnt+1)<<2'd2) -2;
          
          default:Byte_cnt = 32'b0 ;
        endcase
      end
    end
   
always @(posedge Clock)
  begin
    if(Reset)
      begin
        Fall_TxValid_r   <= 0;
        Fall_TxValid_rr  <= 0;
//        Fall_TxValidH_r  <= 0;
//        Fall_TxValidH_rr <= 0;
      end
    else
      begin
        Fall_TxValid_r   <= Fall_TxValid;
//        Fall_TxValidH_r  <= Fall_TxValidH;
//        Fall_TxValidH_rr <= Fall_TxValidH_r;
        Fall_TxValid_rr  <= Fall_TxValid_r; 
      end
  end
   
 
//BFM_put_size    
always @(Rise_TxValid or Fall_TxValid_rr or Fall_TxValid or Byte_cnt or PID_Token_h or Fall_TxValid_r or PID_Data_h)
  begin
    BFM_put_size = 0;
    if(Rise_TxValid | Fall_TxValid_rr)
      BFM_put_size = 24'b0;
    else if(Fall_TxValid && PID_Data_h)
      BFM_put_size = Byte_cnt ;
    else if(Fall_TxValid_r && PID_Token_h)
      BFM_put_size = Byte_cnt ; 
  end
      
//End of Byte_cnt block
/////////////////////////////////////////////////////////////////



 
//Begin  CRC16 Checking block

always @(posedge Clock)
  begin
   if(Reset)
     CRC16_sum_cal1 <= 16'h0;
   else if(PID_DATA)
     CRC16_sum_cal1 <=16'hffff;
   else if(~TxReady_r)
     CRC16_sum_cal1 <= CRC16_sum_cal1;
   else if(PID_Data_h)
     CRC16_sum_cal1 <= CRC16_out_cal1;
   else CRC16_sum_cal1 <= 16'h0;
  end
  

assign CRC16_Din_cal1 = PID_Data_h ?
                        {DataIn_Reg[0],DataIn_Reg[1],DataIn_Reg[2],DataIn_Reg[3],DataIn_Reg[4],DataIn_Reg[5],DataIn_Reg[6],DataIn_Reg[7]}
                        : 8'b0;

  
  
carbonx_usb_crc16
 CRC16_Check_cal1(.CRC_DIN(CRC16_sum_cal1),
                  .DataIn(CRC16_Din_cal1),
                  .CRC_DOUT(CRC16_out_cal1));
   

assign  CRCErr  = Fall_TxValid & (CRC16_out_cal1 != 16'h800d);

assign  RxDone  = (PID_Token_h & Fall_TxValid_r) | (Fall_TxValid & PID_Data_h);

//End of CRC16 Check block
/////////////////////////////////////////////////////////////////    
   

      
endmodule


//
// Update: 1) 073103: Add OpMode, when DUT drive K on bus DataInReg no to 
//            receive the data
//         2) 080103: change put_data's byte 
// Revision      : $Revision$
// Log           :
//               : Revision 1.2  2006/01/17 03:37:33  jstein
//               : Reviewed by:  John Hoglund
//               : Tests Done: runlist -xactors, example/regressALL, runsingle test/dw, checkin
//               : Bugs Affected: DW02_prod_sum1 sign extension
//               : Added examples/xactors: pcix, multi_hier_pci, sc_multi_inst
//               : removed all `defines from xactor verilog (mostly usb)
//               : added test/xactors/usb20
//               :
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.2  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.12  2004/09/14 18:38:22  fredieu
//               : All 60 at 16 bit
//               :
//               : Revision 1.11  2004/09/09 02:18:15  fredieu
//               : Fixes for CRC in 16 bit mode
//               :
//               : Revision 1.10  2004/09/08 18:04:18  vshilman
//               : Fixed Byte Counting
//               :
//               : Revision 1.8  2004/09/03 13:54:19  zippelius
//               : Some minor changes
//               :
//               : Revision 1.7  2004/09/03 13:51:55  fredieu
//               : Fix for 16 bit mode
//               :
//               : Revision 1.6  2004/08/03 13:33:32  fredieu
//               : Allow stalls to be enabled/disabled
//               :
//               : Revision 1.5  2004/07/23 03:09:52  fredieu
//               : Add in the throttle control.
//               :
//               : Revision 1.4  2004/06/04 18:08:17  fredieu
//               : Added synthesys pragmas
//               :
//               : Revision 1.3  2004/04/26 21:54:26  jafri
//               : modified english in verilog and c code and added usb_test1a to test table
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.0  2003/09/27 07:39:10  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.2  2003/09/24 08:43:31  thor
//               : change signals name
//               :
//               : Revision 1.1  2003/09/23 07:54:11  thor
//               : split into two dir
//               :
//               : Revision 1.14  2003/09/10 14:14:16  thor
//               : synthesis
//               :
//               : Revision 1.13  2003/09/10 07:58:32  thor
//               : add comment
//               :
//               : Revision 1.12  2003/09/09 09:44:57  thor
//               : change Config_Reg bits
//               :
//               : Revision 1.11  2003/09/03 09:07:49  thor
//               : add blank
//               :
//               : Revision 1.10  2003/09/03 03:37:46  thor
//               : get rid of unuse signal
//               :
//               : Revision 1.9  2003/08/29 06:15:51  thor
//               : add receive zero length packet from DUT
//               :
//               : Revision 1.8  2003/08/26 07:20:57  thor
//               : change RxDone:send token RxDone is the clock Fall_TxValid_r, send Data is the clock Fall_TxValid
//               :
//               : Revision 1.7  2003/08/26 03:10:26  thor
//               : change put_size
//               :
//               : Revision 1.6  2003/08/25 12:56:48  thor
//               :  add TimeOut to clear BFM_xrun
//               :
//               : Revision 1.5  2003/08/25 06:19:18  thor
//               : change BFM_xrun
//               :
//               : Revision 1.4  2003/08/20 14:13:00  thor
//               : add 16bit databus support
//               :
//               : Revision 1.3  2003/08/15 07:24:17  thor
//               : add BFM_xrun signal
//               :
//               : Revision 1.2  2003/08/14 14:06:26  thor
//               : add header
//               :
//               :
