
// ///////////////////////////////////////////////////////////////////
// The TimeOut Block of BFM
//////////////////////////////////////////////////////////////////////
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
//
//
// This module is the TimeOut function block.
// After the host sends a IN token, it waits to receive data from device.
// If the BFM does not receive any data within a certain amount of time
// ( specified by USB2.0 standard), it is a TimeOut condition.
// After the host sends data to device, it waits for a handshake transaction.
// If the waiting time longer than defined by the spec, This is also a TimeOut condition.
///////////////////////////////////////////////////////////////////////// 
//
//
// Update: 1) 080403: change PID_Error to PIDErr 
//
// Revision      : $Revision$
// Log           :
//               : $Log$
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.3  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.2  2005/09/29 23:53:43  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done:  checkin, xactor regression
//               : Bugs Affected: none
//               :  Removed BLKMEMDP_V4_0.v and replaced with dnp_synch_ram.v embedded inside slave_dpram.v
//               :  moved TONS of Zaiq related code comments and Authorship  to protected areas of the module
//               :  so they will not be seen by the customer
//               :  removed tbp_user.h from <xtor>/include directories only need 1 copy in common/include
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.9  2004/09/27 22:15:32  zippelius
//               : Adjusted 16 bit Data Bus time-out, because core is now always running at 60 MHz
//               :
//               : Revision 1.8  2004/09/08 19:42:03  zippelius
//               : Changed WriteEnd to LastData
//               :
//               : Revision 1.7  2004/07/30 22:17:25  fredieu
//               : Fix for some simultaneous events
//               :
//               : Revision 1.6  2004/06/04 18:08:17  fredieu
//               : Added synthesys pragmas
//               :
//               : Revision 1.5  2004/04/26 21:54:26  jafri
//               : modified english in verilog and c code and added usb_test1a to test table
//               :
//               : Revision 1.4  2004/04/22 13:44:29  zippelius
//               : Clean-up defines, messaging, others
//               :
//               : Revision 1.3  2004/04/20 20:54:10  zippelius
//               : Losts of code clean-up, comment fixes
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.0  2003/09/27 07:39:10  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.2  2003/09/26 03:56:57  adam
//               : add load completetime when timeout
//               :
//               : Revision 1.1  2003/09/23 07:54:11  thor
//               : split into two dir
//               :
//               : Revision 1.8  2003/09/05 02:00:35  thor
//               : add blank
//               :
//               : Revision 1.7  2003/09/03 09:07:49  thor
//               : add blank
//               :
//               : Revision 1.6  2003/09/03 03:37:55 thor
//               : get rid of unuse signal
//               :
//               : Revision 1.5  2003/08/25 12:57:42  thor
//               : add LastData to clear TimeOut_cnt
//               :
//               : Revision 1.4  2003/08/25 11:51:40  thor
//               : add LS_Attach/FS_Attach to top module and timeout module:  when work on different speed, the timeout wait clock counts are different
//               :
//               : Revision 1.3  2003/08/15 12:01:32  thor
//               : clear  Timeout_cnt using LastData
//               :
//               : Revision 1.2  2003/08/14 14:06:39  thor
//               : add header
//               :
//               :





`timescale 1ns/1ns

module carbonx_usb_timeout(
			     Clock,
			     Reset,       
			     LowSpeed,
			     TxValid,
			     LastData,
			     DataBus16_8,
			     FS_Attach,
			     LS_Attach,
			     PIDErr,
			     Trans_TimeOut,
			     TimeOut_Load
)/* synthesis syn_hier = "firm"*/;

//input signals

input              Clock;               //BFM system clock;

input              Reset;               //BFM reset signal;

input              LowSpeed;            //lowspeed transfer;

input              TxValid;             //rising edge of TxValid signal, indicates that DUT will begin 
                                        //to send packet to BFM.
                                                                    
input              LastData;            //indicates the end of the host send data; if the time between 
                                        //LastData and Rise_TxVaid is longer than the spec defined,
                                        //that will indicate a TimeOut.
                                   
input              PIDErr;              //receivd error PID

input              LS_Attach;           //LS only device attach;

input              FS_Attach;           //FS only device attach;

input              DataBus16_8;         //databus width;
                                   
//output signals

output             Trans_TimeOut;       //indicates whether have a TimeOut error;

output             TimeOut_Load;


//internal wire and reg signals
wire               Rise_TxValid;         //rise edge of TxValid        

wire               TimeOut_cnt_inc_tmp;  //TimeOut_cnt_inc tmp

wire[9:0]          TimeOut_cnt_tmp_add;  //TimeOut_cnt increase 1 or clear 10'b0;    

wire[9:0]          TimeOut_cnt_tmp_hold; //TimeOut_cnt hold current value or input TimeOut_cnt_tmp_add

reg[9:0]           TimeOut_cnt;

reg                TimeOut_cnt_inc;      //TimeOut_cnt add signal

reg                TxValid_r;            //one cycle delay of TxValid 

wire               TimeOut_Load;         //load completion time  to CompletionTime_Reg;              

//Begin Main module
///////////////////////////
//After XIF write to BFM is complete, BFM waits to receive data or token from
//DUT. When Rise_TxValid is asserted, it means that there is data or token to
//be sent. We use a "TimeOut_cnt" counter to count the time between XIF finish 
//write and DUT begin transfer data, and compare this time with the USB2.0 
//spec  defined time. If TimeOut_cnt is greater than the USB2.0 spec defined 
//time, TimeOut is signalled.   

assign  TimeOut_cnt_inc_tmp = (LastData | PIDErr)? 1 : TimeOut_cnt_inc;

always @(posedge Clock )
   begin
     if(Reset)
       TxValid_r <=0;
     else
       TxValid_r <= TxValid;
   end
   
assign Rise_TxValid = TxValid & (!TxValid_r);
     
always @(posedge Clock )
   begin
     if(Reset)
       TimeOut_cnt_inc <= 0;
     else if(Rise_TxValid | (Trans_TimeOut & !LastData))
        TimeOut_cnt_inc <= 0;
     else
        TimeOut_cnt_inc <= TimeOut_cnt_inc_tmp;
   end
   
assign TimeOut_cnt_tmp_add   = TimeOut_cnt_inc ? (TimeOut_cnt + 1) : TimeOut_cnt;

assign  TimeOut_cnt_tmp_hold = Trans_TimeOut ? TimeOut_cnt : TimeOut_cnt_tmp_add;

always @(posedge Clock )
   begin
     if(Reset)
       TimeOut_cnt <= 10'b0;
     else if(Rise_TxValid | PIDErr | Trans_TimeOut |LastData)
        TimeOut_cnt <= 0;
     else
        TimeOut_cnt <= TimeOut_cnt_tmp_hold;
   end


// A high-speed host or device expecting a response to a transmission must not timeout the transaction
// if the interpacket delay is less than 736 bit times, and it must timeout the transaction
// if no signaling is seen within 816 bit times. (USB2.0 spec p.169
  
assign Trans_TimeOut = (((LS_Attach|LowSpeed) & (TimeOut_cnt == 10'd72)) 
                        | (FS_Attach & (TimeOut_cnt == 10'd72))
                        | ((!LS_Attach) & (!FS_Attach) & (TimeOut_cnt == 10'd102))) &
		       !LastData;
                       
assign TimeOut_Load = ((LS_Attach|LowSpeed) & (TimeOut_cnt == 10'd71)) 
                        | (FS_Attach & (TimeOut_cnt == 10'd71))
                        | ((!LS_Attach) & (!FS_Attach) & (TimeOut_cnt == 10'd101));
                       
//End of main module
////////////////////////////////
                   
                   
endmodule                            
