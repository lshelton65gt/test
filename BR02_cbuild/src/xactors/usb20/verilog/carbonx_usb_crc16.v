// ///////////////////////////////////////////////////////////////////
// The CRC16 Block of BFM
//////////////////////////////////////////////////////////////////////
//
// 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// Author        : $Author$
//
// Filename      : $Source$
//
// Modified Date : $Date$
//
// Revision      : $Revision$
// Log           :
//               : $Log$
//               : Revision 1.1  2005/11/10 20:40:53  hoglund
//               : Reviewed by: knutson
//               : Tests Done: checkin xactors
//               : Bugs Affected: none
//               :
//               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
//               : because of the period.
//               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
//               : verilog source files.
//               : Utopia2 also has name changes on the verilog source files.
//               :
//               : Revision 1.2  2005/10/20 18:23:28  jstein
//               : Reviewed by: Dylan Dobbyn
//               : Tests Done: scripts/checkin + xactor regression suite
//               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
//               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
//               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
//               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
//               :
//               : Revision 1.1  2005/09/25 16:58:31  jstein
//               : Reviewed by: none
//               : Tests Done: usb2.0 xactor test
//               : Bugs Affected: Adding Verilog unprotected source code to sandbox
//               :
//               : Revision 1.2  2004/03/26 15:17:35  zippelius
//               : Some clean-up
//               :
//               : Revision 2.0  2003/09/27 07:39:09  adam
//               : include carbonx_usb_define.v
//               :
//               : Revision 1.3  2003/09/26 02:56:01  thor
//               : add always block
//               :
//               : Revision 1.2  2003/09/24 08:43:31  thor
//               : change signals name
//               :
//               : Revision 1.1  2003/09/23 07:54:10  thor
//               : split into two dir
//               :
//               : Revision 1.2  2003/08/14 14:05:23  thor
//               : add header
//               :

// 
//

`timescale 1ns/1ns


///////////////////////////////////////////////////////////////////
//
// CRC16
//
///////////////////////////////////////////////////////////////////

module carbonx_usb_crc16(CRC_DIN, DataIn, CRC_DOUT);
input	[15:0]	CRC_DIN;
input	[7:0]	DataIn;
output	[15:0]	CRC_DOUT;

reg     DataIn_tmp1 ;

reg     CRC_DIN_tmp;

always @(DataIn[7] or  DataIn[6] or DataIn[5] or DataIn[4] or DataIn[3] or DataIn[2] or DataIn[1])
   begin 
     DataIn_tmp1 = DataIn[7] ^ DataIn[6] ^ DataIn[5] ^ DataIn[4] ^ DataIn[3] ^ DataIn[2] ^ DataIn[1];
   end       


always @(CRC_DIN[9]or CRC_DIN[10] or CRC_DIN[11] or CRC_DIN[12] or CRC_DIN[13] or CRC_DIN[14] or CRC_DIN[15])
  begin
     CRC_DIN_tmp = CRC_DIN[9] ^ CRC_DIN[10] ^ CRC_DIN[11] ^ CRC_DIN[12] ^ CRC_DIN[13] ^ CRC_DIN[14] ^ CRC_DIN[15];
  end

assign CRC_DOUT[0] =	DataIn_tmp1 ^ DataIn[0] ^ CRC_DIN[8] ^ CRC_DIN_tmp ;

assign CRC_DOUT[1] =	DataIn_tmp1 ^ CRC_DIN_tmp;

assign CRC_DOUT[2] =	DataIn[1] ^ DataIn[0] ^ CRC_DIN[8] ^ CRC_DIN[9];

assign CRC_DOUT[3] =	DataIn[2] ^ DataIn[1] ^ CRC_DIN[9] ^ CRC_DIN[10];

assign CRC_DOUT[4] =	DataIn[3] ^ DataIn[2] ^ CRC_DIN[10] ^ CRC_DIN[11];

assign CRC_DOUT[5] =	DataIn[4] ^ DataIn[3] ^ CRC_DIN[11] ^ CRC_DIN[12];

assign CRC_DOUT[6] =	DataIn[5] ^ DataIn[4] ^ CRC_DIN[12] ^ CRC_DIN[13];

assign CRC_DOUT[7] =	DataIn[6] ^ DataIn[5] ^ CRC_DIN[13] ^ CRC_DIN[14];

assign CRC_DOUT[8] =	DataIn[7] ^ DataIn[6] ^ CRC_DIN[0] ^ CRC_DIN[14] ^ CRC_DIN[15];

assign CRC_DOUT[9] =	DataIn[7] ^ CRC_DIN[1] ^ CRC_DIN[15];

assign CRC_DOUT[10] =	CRC_DIN[2];

assign CRC_DOUT[11] =	CRC_DIN[3];

assign CRC_DOUT[12] =	CRC_DIN[4];

assign CRC_DOUT[13] =	CRC_DIN[5];

assign CRC_DOUT[14] =	CRC_DIN[6];

assign CRC_DOUT[15] =	DataIn_tmp1 ^ DataIn[0] ^ CRC_DIN[7] ^ CRC_DIN[8] ^ CRC_DIN_tmp;

endmodule

