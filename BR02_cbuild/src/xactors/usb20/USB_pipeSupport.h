//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 *
 * Comments      : First line. 
 *                 Following line
 *
 * Author        : $Author$
 * Modified Date : $Date$
 * Revision      : $Revision$
 * Log           : 
 *               : $Log$
 *               : Revision 1.2  2005/12/20 18:27:14  hoglund
 *               : Reviewed by: Goran Knutson
 *               : Tests Done: xactors checkin
 *               : Bugs Affected: 5463
 *               :
 *               : Fixed a bunch of compiler warnings listed in bug 5463.
 *               :
 *               : Revision 1.1  2005/11/10 20:40:53  hoglund
 *               : Reviewed by: knutson
 *               : Tests Done: checkin xactors
 *               : Bugs Affected: none
 *               :
 *               : USB source is integrated.  It is now called usb20.  The usb2.0 got in trouble
 *               : because of the period.
 *               : SPI-3 and SPI-4 now have test suites.  They also have name changes on the
 *               : verilog source files.
 *               : Utopia2 also has name changes on the verilog source files.
 *               :
 *               : Revision 1.2  2005/10/20 18:23:28  jstein
 *               : Reviewed by: Dylan Dobbyn
 *               : Tests Done: scripts/checkin + xactor regression suite
 *               : Bugs Affected: adding spi-4 transactor. Requires new XIF_core.v
 *               : removed "zaiq" references in user visible / callable code sections of spi4Src/Snk .c
 *               : removed trailing quote mark from ALL Carbon copyright notices (which affected almost every xactor module).
 *               : added html/xactors dir, and html/xactors/adding_xactors.txt doc
 *               :
 *               : Revision 1.1  2005/09/22 20:49:22  jstein
 *               : Reviewed by: Mark Seneski
 *               : Tests Done: xactor regression, checkin suite
 *               : Bugs Affected: none - Adding new functionality - transactors
 *               :   Initial checkin Carbon VSP Transactor library
 *               :   ahb, pci, ddr, enet_mii, enet_gmii, enet_xgmii, usb2.0
 *               :
 *               : Revision 1.10  2004/08/13 21:43:47  zippelius
 *               : snapshot check-in
 *               :
 *               : Revision 1.9  2004/08/13 18:40:34  zippelius
 *               : Removed bogus Pipe 0; Moved toggle tracking from Pipe into EP; Fixed 4 byte alloc, when data size==0;Misc fixes
 *               :
 *               : Revision 1.8  2004/08/11 01:59:18  fredieu
 *               : Fix to copyright statement
 *               :
 *               : Revision 1.7  2004/08/09 16:58:57  fredieu
 *               : add 2 new routines
 *               :
 *               : Revision 1.6  2004/08/06 13:51:32  zippelius
 *               : Updated/fixed test23, fixed numerous issues around User Reset
 *               :
 *               : Revision 1.5  2004/07/30 21:58:03  fredieu
 *               : Need to be able to clear the error count
 *               :
 *               : Revision 1.4  2004/04/26 21:34:29  zippelius
 *               : Code clean-up regarding IRP state and misc other
 *               :
 *               : Revision 1.3  2004/04/26 17:08:56  zippelius
 *               : Simplified parameter lists of scheduler functions. Some bug fixes in PE.
 *               :
 *               : Revision 1.2  2004/03/26 15:00:18  zippelius
 *               : Some clean-up. Mostly regarding callbacks
 *               :
 *               : Revision 2.4  2003/12/03 02:13:54  adam
 *               : Edit testcase
 *               :
 *               : Revision 2.3  2003/11/25 09:41:10  adam
 *               : Edit for feed-back issues
 *               :
 *               : Revision 2.2  2003/09/27 15:45:39  adam
 *               : Add testcases
 *               :
 *               : Revision 2.1  2003/09/27 12:09:33  serlina
 *               : pipe update
 *               :
 *               : Revision 2.0  2003/09/27 06:34:13  adam
 *               : Update file directory.
 *               :
 *               : Revision 1.1  2003/09/27 06:28:42  adam
 *               : updated file directory
 *               :
 *               : Revision 1.8  2003/09/19 07:25:23  serlina
 *               :  add carbonXUsbGetDefPipe()
 *               :
 *               : Revision 1.7  2003/09/12 14:02:34  serlina
 *               : updated for signoff review
 *               :
 *               : Revision 1.6  2003/09/11 08:09:18  issac
 *               : major update after code review
 *               :
 *               : Revision 1.5  2003/09/08 07:56:05  serlina
 *               : change link
 *               :
 *               : Revision 1.4  2003/09/08 02:30:58  adam
 *               : updated data structre
 *               :
 *               : Revision 1.3  2003/08/27 15:03:11  serlina
 *               : updated Do Enum series of functions, and dev prof data structure
 *               :
 ***************************************************************************/
 
#ifndef USB_PIPE_SUPPORT_H
#define USB_PIPE_SUPPORT_H

#define USB_PIPE_GROW_SIZE 10
#define USB_ISO_IN_TRANS_NUM 100 

void USB_PIPE_DescrReset(USB_PipeDescr_t* p_pipe);

USB_PipeDescr_t* USB_PIPE_GetNewDescr ();

BOOL USB_PIPE_PA_IsFull(USB_PipeAPStatic_t*  p_pipe_mgr);

USB_PipeDescr_t** USB_PIPE_PA_Cur(USB_PipeAPStatic_t*  p_pipe_mgr);

USB_IrpLink_t* USB_PIPE_IrpLinkGetNew();

void USB_PIPE_IrpLinkFree(USB_PipeDescr_t* p_pipe);

void USB_PIPE_Update(USB_PipeDescr_t* p_pipe);

void USB_PIPE_MsgHeader();

void USB_PIPE_MsgTotal();

void USB_PIPE_Msg(USB_PipeDescr_t*  p_pipe);

void USB_PIPE_MsgEnd();

void USB_PIPE_RetireCompletedIRPs (USB_PipeDescr_t*  p_pipe);

void USB_PIPE_PA_Free (USB_PipeAPStatic_t*  p_pipe_mgr);

void USB_PIPE_PA_Reset(USB_PipeAPStatic_t*  p_pipe_mgr);

void USB_PIPE_PA_Init(USB_PipeAPStatic_t*  p_pipe_mgr);

USB_PipeDescr_t* carbonXUsbGetDefPipe(u_int32 dev_id);

u_int32 USB_PIPE_PA_GetPipeCount();

#endif
