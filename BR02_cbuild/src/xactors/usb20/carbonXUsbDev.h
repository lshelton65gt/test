
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONX_USB_DEV_H
#define CARBONX_USB_DEV_H

USB_DevProfile_t*   carbonXUsbGetDevProf(u_int32 dev_id);

void carbonXUsbSetDevProf(
  BOOL                  is_split,
  u_int8                dev_id,
  u_int8                dev_addr,
  u_int32               port_num,
  USB_DevType_e         dev_type,
  USB_SpeedMode_e	speed_mode,
  USB_TransSpeed_e      speed,
  u_int32               def_max_pk_size
);

void carbonXUsbDevReport(BOOL is_one,u_int32 dev_id);

#endif
