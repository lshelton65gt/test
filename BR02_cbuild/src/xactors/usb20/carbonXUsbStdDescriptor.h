//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
#ifndef CARBONX_USB_STD_REQUEST_H
#define CARBONX_USB_STD_REQUEST_H

#define MAX_PORT_NUMBER 31

/*
 *Standard Device Descriptor
 *except 'bLength' and 'bDescriptorType', all element reset to zero.
 */
typedef struct {
  u_int8       b_length;		// Size of this descriptor in bytes
					// default equal 18
  u_int8       b_descr_type;
  u_int16      bcd_usb;			// USB release version number
  u_int8       b_dev_class;		// If the value between [1,FEH], This value
					// identifies the class definition. 
					// If FFH, device class is vendor-specific.
  u_int8       b_dev_sub_class;		// Subclass code.
					// Using bDeviceClass configured the value.
					// If bDeviceClass is 0, the value is 0.
					// If it isn't FFH, the value are reserved.
  u_int8       b_dev_protocol;		// Protocol code.
					// Using bDeviceClass configured the value.
  u_int8       b_max_pkt_size;		// Max packet size for endpoint zero
					// (only 8,16,32,or 64 are valid)
  u_int16      id_vendor;               // Vendor ID(assigned by the USB-IF)
  u_int16      id_product;              // Product ID(assigned by the manufacturer)
  u_int16      bcd_dev;			// Device release number in binary-coded 
					// decimal.                                     
  u_int8       i_manu;          	// Index of string descriptor describing 
					// manufacture
  u_int8       i_product;               // Index of string descriptor describing 
					// product.                                
  u_int8       i_serial_num;		// Index of string descriptor describing 
					// the device's serial number.
  u_int8       i_num_cfg;		// Number fo possible configurations.
} USB_StdDevDescr_t;

//Device Qualifier Descriptor
typedef struct {
  u_int8       b_length;		// Seze of descriptor
                                        // default equal 10
  u_int8       b_descr_type;   		// Device Qualifier Type
  u_int16      bcd_usb;			// USB spec version number
  u_int8       b_dev_class;          	// Class Code
  u_int8       b_dev_sub_class;      	// SubClass Code
  u_int8       b_dev_protocol;       	// Protocol Code
  u_int8       b_max_pkt_size;      	// Maximum packet size for other speed
  u_int8       b_num_cfg;    		// Number of Other-speed Configurations
  u_int8       b_rsv;
} USB_StdDevQualDescr_t;
/*
 *Standard Configuration Descriptor
 */
typedef struct {
  u_int8       b_length;		// Size of this descriptor in bytes
                                        // default equal 9
  u_int8       b_descr_type;	   	// CONFIGURATION Descriptor Type
  u_int16      w_total_length;          // Total lenth of data returned for 
                                        // this configuration.
                                        // Includes the combined length of 
                                        // all descriptors.
  u_int8       b_num_if;         	// Number of interfaces supported by this
                                        // configuration.
  u_int8       b_cfg_val;   		// Value to use as an argument to 
                                        // SetConfiguration() request to 
                                        // select this configuratio.
  u_int8       i_cfg;         		// Index of string descriptor describing
                                        // this configuration.
  u_int8       bm_attr;           	// Configuration characteristics
                                        // D7:   Reserved(set to one)
                                        // D6:   Self-powered
                                        // (Using the GetStatus(DEVICE) request 
                                        // determine)
                                        // D5:   Remote Wakeup
                                        // D4..0:Reserved(reset to zero)
  u_int8       b_max_power;             // Maximum power consumption of the device.
} USB_StdCfgDescr_t;
/*
 *Other_Speed_Configuration Descriptor
 */
typedef struct {
  u_int8       b_length;		// Size of descriptor
                                        // default equal 9
  u_int8       b_descr_type;		// Other_speed_Configuration Type
  u_int16      w_total_length;          // Total length of data returned
  u_int8       b_num_if;		// Number of interfaces supported by
                                        // this speed configuration.
  u_int8       b_cfg_value;		// Value to use to select configuraton
  u_int8       i_cfg;			// Indeex of string descriptor
  u_int8       bm_attr;			// Same as Configuration descriptor
  u_int8       b_max_power;             // Same as Configuration descriptor
} USB_StdOsCfgDescr_t;
/*
 *Standard Interface Descriptor
 */
typedef struct {
  u_int8       b_length;		// Size of this descriptor in bytes
                                        // default equal 9
  u_int8       b_descr_type;		// INTERFACE Descriptor Type.
  u_int8       b_if_num;      		// Number of this interface.
  u_int8       b_alt_set;     		// Value used to select this alternate
                                        // setting for the interface identified
                                        // in the prior field
  u_int8       b_num_ep;         	// Number or endpoints used by this 
                                        // interface(excluding endpoint 0).If the  
                                        // value is 0, this interface only uses   
                                        // Default Control Pipe.
  u_int8       b_if_class;       	// Class code.
                                        // Set to FFH, the interface class is 
                                        // vendor-specific.
                                        // All other value of zero is reserved.
  u_int8       b_if_sub_class;    	// Subclass code.
                                        // Using bInterfaceClass field configure
                                        // If bInterfaceClass is zero,
                                        // this value must 0.
                                        // If bInterfaceClass is not set to FFH,
                                        // all value are reserved.
  u_int8       b_if_protocol;    	// Protocol code.
                                        // Using bInterfaceClass field 
                                        // configure.
                                        // If this field is 0, the device does 
                                        // not use a class-specific protocol.
                                        // If this field is FFH, the device
                                        // uses a vendor-specific protocol.
  u_int8       i_if;			// Index of string descriptor describing 
                                        // this interface.
} USB_StdIfDescr_t; 
/*
 *Standard Endpoint Description type
 */
typedef struct {
  u_int8       b_length;		// Size of this descriptor in bytes
                                        // default equal 7
  u_int8       b_descr_type;	   	// ENDPOINT Descriptor Type
  u_int8       b_ep_addr;      		// The Address of the endpoint
                                        // Bit 3..0:The endpoint number
                                        // Bit 6..4:Reserved,reset to zero
                                        // Bit 7:    Direction, ignored for
                                        //           control endpoints
                                        //       0=OUT endpoint
                                        //       1=IN endpoint
  u_int8       bm_attr;           	// the endpoint's attributes when using 
                                        // the bConfigurationValue configured.
                                        // Bits 1..0:Transfer Type
                                        //     00=Control
                                        //     01=Isochronous
                                        //     10=Bulk
                                        //     11=Interrupt
                                        // If not an isochronous endpoint, 
                                        // bits 5..2 are reserved and must be 0.
                                        // Otherwise,
                                        // Bits 3..2:Synchronization Type
                                        //     00=No Synchronization
                                        //     01=Asynochronous
                                        //     10=Adaptive
                                        //     11=Synchronous
                                        // Bits 5..4: Usage Type
                                        //     00=Data endpoint
                                        //     01=Feedback endpoint
                                        //     10=Implicit feedback Data endpoint
                                        //     11=Reserved
                                        // Other bits are reserved and 
                                        // must be reset to zero.
  u_int16      w_max_pkt_size;       	// Bits 10..0 specify 
                                        // the maximum packet size(in bytes).
                                        // For High-speed ISO and Interrupt endpt:
                                        // Bits 12..11 specify the number of 
                                        // additional transaction opportunities
                                        // per microframe:
                                        //    00=None(1 transaction per microframe)
                                        //    01=1 additional(2 per microframe)
                                        //    10=2 additional(2 per microframe)
                                        //    11=Reserved
                                        // Bits 15..13 are reserved and
                                        // must be set to zero.
  u_int8       b_interval;		// Interval for polling endpoint 
                                        // for data transfer.
                                        // Full/high-speed ISO endpoints,
                                        // the value must be [1,16].
                                        // Full-/low-speed interrupt endpoints, 
                                        // the value may be [1,255].
                                        // High-speed interrupt endpoints,
                                        // This value must be [1,16].
                                        // High-speed bulk/control OUT endpoints,
                                        // This value must be [0,255].
} USB_StdEpDescr_t;

typedef struct {
  u_int8	b_descr_len;
  u_int8	b_descr_type;		// 29H indicates it is a hub
  u_int8	b_num_ports;		// Number of ports hub supported
  u_int16	w_hub_charact;		// D1...D0: Logical power switch mode
  					//  00: Ganged power switching
  					//  01: Individual port power switching
  					//  1x: Reserved
  					// D2:      Identifies a compound device
  					//  0: Hub isn't part of a compound device
  					//  1: Hub is part of a compound device
  					// D4...D3: Over=current protection mode
  					//  00: global over-current protection
  					//  01: Individual port over-current protection
  					//  1x: No over-current protection
  					// D6...D5: TT think time
  					//  00: TT requires at most 8 FS bit 
  					//      times of inter transaction gap on
  					//      a FS/LS downstream bus.
  					//  01: at most 16 FS bit times
  					//  02: at most 24 FS bit times
  					//  02: at most 32 FS bit times
  					// D7:      Port Indicators supported
  					//  0: Port indicators not supported
  					//  1: Port indicators supported.
  u_int8        b_pwron2good;		// Time(in 2 ms intervals) from the time
  					// the power-on sequence begins on a port
  					// until power is good on that port.
  u_int8        b_hub_ctrl_cur;		// Maximum current requirement.
  u_int8        a_dev_removal[MAX_PORT_NUMBER];		
                                        // Indicates if a port has a removable
  					// device attached.
  					// bit value: 
  					//  0: device is removable
  					//  1: device is non-removable
  					// bitmap:
  					//  bit 0: reserved
  					//  bit 1: port 1
  					//  bit 2: port 2
  					//  ...
  					//  bit n: port n (maximum num is 255)
  u_int8        a_port_pwr_ctrl_mask[MAX_PORT_NUMBER];	
                                        // compatible with 1.0 compliant devices.
} USB_StdHubDescr_t;

#endif
