//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONX_USB_API_H
#define CARBONX_USB_API_H

#include "xactors/usb20/carbonXUsbStdDescriptor.h"
#include "xactors/usb20/carbonXUsbCommon.h"
#include "xactors/usb20/carbonXUsbSys.h"
#include "xactors/usb20/carbonXUsbDev.h"
#include "xactors/usb20/carbonXUsbDatahl.h"
#include "xactors/usb20/carbonXUsbIrp.h"
#include "xactors/usb20/carbonXUsbSar.h"
#include "xactors/usb20/carbonXUsbPipe.h"
#include "xactors/usb20/carbonXUsbStdReq.h"
#include "xactors/usb20/carbonXUsbSchedule.h"
#include "xactors/usb20/carbonXUsbCallback.h"

#endif
