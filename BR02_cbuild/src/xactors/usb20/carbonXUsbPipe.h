//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONX_USB_PIPE_OP_H
#define CARBONX_USB_PIPE_OP_H

USB_PipeDescr_t*  carbonXUsbGetPipeDescr(u_int32 pipe_id);

USB_PipeDescr_t* carbonXUsbPipeCreate(USB_EndPtDescr_t* p_endpt_descr);

void carbonXUsbPipeDescrUpdate(USB_PipeDescr_t* p_pipe, USB_EndPtDescr_t* p_endpt_descr);

void carbonXUsbPipePushInIRP(USB_PipeDescr_t* p_pipe,USB_Irp_t* p_irp);

void carbonXUsbPipePopInIRP(USB_PipeDescr_t* p_pipe,USB_Irp_t*  p_irp);

void carbonXUsbPipePushOutIRP(USB_PipeDescr_t* p_pipe,USB_Irp_t* p_irp);

void carbonXUsbPipePopOutIRP(USB_PipeDescr_t* p_pipe,USB_Irp_t*  p_irp);

USB_PipeState_e carbonXUsbPipeGetState(USB_PipeDescr_t* p_pipe);

void carbonXUsbPipeSetState(USB_PipeDescr_t* p_pipe,USB_PipeState_e new_state);

u_int32 carbonXUsbPipeGetNumComplIRPs(USB_PipeDescr_t*   p_pipe);

u_int32 carbonXUsbPipeGetNumUncomplIRPs(USB_PipeDescr_t* p_pipe);

void carbonXUsbPipeReport(BOOL is_one,USB_PipeDescr_t*  p_pipe);

void carbonXUsbPipeDestroy(USB_PipeDescr_t*  p_pipe);

void carbonXUsbError_Count_Clear(USB_Trans_t* trans);

void carbonXUsbPipeToggleData(USB_PipeDescr_t*  p_pipe, USB_Pid_e next_toggle);

u_int32 carbonXUsbPipeReadToggle(USB_PipeDescr_t*  p_pipe);

void carbonXUsbSetEpDescr(
  USB_EndPtDescr_t* p_ep,
  BOOL 		    is_auto,
  u_int8            dev_id,
  u_int32           endpt_num,
  u_int32           hs_trans_num,
  u_int32           interval,
  u_int32           max_packet_size,
  USB_Direction_e   direction,
  USB_EpType_e      endpt_type,
  USB_SyncType_e    sync_type,
  USB_UsageType_e   usage_type
);

USB_PipeDescr_t* carbonXUsbGetDefPipe(u_int32 dev_id);

#endif
