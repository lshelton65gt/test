//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/***************************************************************************
 *
 * Comments      : This file is required for PE.
 *
 *                 1. This file is used by the Protocol Engine, including all 
 *                    four kinds of transfer types and two directions
 *                    (IN and OUT). 
 *                 2. According to the Host Controller(HC) FSM, four 
 *                    possible transfers are judged and four branches
 *                    are divided.
 *                 3. Then IN or OUT are two ways.
 *                 4. Then separates by four kinds of transfer: 
 *                    CTRL, BULK, ISO, INTR.
 *                 5. FSM processing.
 *                 6. Send packet and control information to XIF.
 *                 7. Receive packet and status information from XIF.
 *                 8. After analysis, send the corresponding results to scheduler.
 *
 ***************************************************************************/

#include "USB_support.h"


//%FS==================================================================
//
// USB_PE_DoTransfer
//
// Description:
//   USB_PE_DoTransfer implements a transaction between Scheduler and the Verilog BFM.
//   Refer to USB2.0 standard p351 (Figure 11-35).
//   This function includes 3 steps:
//   Step 1: Build USB_HcCmd with type USB_CommandRec_t, which is used
//           to control the state machine.
//   Step 2: Build USB_Hsx with type USB_HsBusRec_t, which is used to
//           record the returned information from sim-side.
//   Step 3: Process transfers.
//
//   Note:
//   One transfer may consist of a number of atomic USB packets/transactions
//   E.g.: A Token packet (e.g. OUT), followed by a Data packet (e.g. DATA0),
//         followed by a Handshake packet (e.g. ACK)
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the struct that shall be transfered. USB_PE_DoTransfer
//     gets all necessary information to build USB_Hccmd and USB_Hsx,
//     then initializes it.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoTransfer(USB_Trans_t* p_trans) {

  USB_CommandRec_t   USB_HcCmd;
  USB_HsBusRec_t     USB_Hsx;

  if(p_trans->p_data_segment == NULL) MSG_Panic("Pointer is NULL!\n");

  if(p_trans->p_pipe != NULL) {

    // Step 1: Build USB_HcCmd with type USB_CommandRec_t, which is used
    //         to control the state machine.
    USB_HcCmd.ep_type    = p_trans->p_pipe->p_endpt_descr->endpt_type;
    USB_HcCmd.trans_type = p_trans->trans_type;
   
    USB_HcCmd.pid = p_trans->pid;
      
    // Set members of USB_HcCmd
    USB_HcCmd.dev_addr     = p_trans->dev_addr;
    USB_HcCmd.endpt_num    = p_trans->p_pipe->p_endpt_descr->endpt_num;
    USB_HcCmd.direction    = p_trans->p_pipe->p_endpt_descr->curr_dir;
    USB_HcCmd.datapart     = p_trans->datapart;
    USB_HcCmd.is_last      = p_trans->is_last;
    USB_HcCmd.needs_toggle = p_trans->needs_toggle;
    
    if(p_trans->pid == USB_PID_SETUP) {
      USB_HcCmd.is_setup = TRUE;
    }
    else {
      USB_HcCmd.is_setup = FALSE;
    }
    
    if(p_trans->pid == USB_PID_PING) {
      USB_HcCmd.is_ping = TRUE;
    }
    else {
      USB_HcCmd.is_ping = FALSE;
    }
    
    if(p_trans->trans_speed == USB_TRANS_HS) {
      USB_HcCmd.is_hs = TRUE;
    }
    else {
      USB_HcCmd.is_hs = FALSE;
    }
    
    // Step 2: Build USB_Hsx with type USB_HsBusRec_t, which is used
    //         to record the returned information from sim-side.  
    USB_Hsx.ep_type     = p_trans->p_pipe->p_endpt_descr->endpt_type;
    USB_Hsx.dev_addr    = p_trans->dev_addr;
    USB_Hsx.endpt_num   = p_trans->p_pipe->p_endpt_descr->endpt_num;
    USB_Hsx.direction   = p_trans->p_pipe->p_endpt_descr->curr_dir;
    USB_Hsx.datapart    = p_trans->datapart;
    USB_Hsx.pid         = USB_PID_RESERVED;
  }
  
  USB_PE_MsgHeader();

  // Step 3: Do the transfer
  switch (p_trans->trans_type) {
    
  case USB_TRANS_TYPE_SOF:  // SOF transaction
    MSG_Printf("+       Trans type:           SOF\n");  
    USB_PE_IssuePacket(p_trans);
    break;
    
  case USB_TRANS_TYPE_SSPLIT: // Start-split transaction
    MSG_Printf("+       Trans type:           SSPLIT\n"); 
    USB_PE_DoStart(p_trans, &USB_HcCmd, &USB_Hsx);
    break;

  case USB_TRANS_TYPE_CSPLIT: // Complete-split transaction
    MSG_Printf("+       Trans type:           CSPLIT\n"); 
    USB_PE_DoComplete(p_trans, &USB_HcCmd, &USB_Hsx);
    break;
   
  case USB_TRANS_TYPE_NONSPLIT:   // Non-split transaction
    MSG_Printf("+       Trans type:           NONSPLIT\n"); 
    USB_PE_DoNonsplit(p_trans, &USB_HcCmd, &USB_Hsx);
    break;

  default:
    break; 
   
  } // End switch (p_trans->trans_type) 

  return;
}

//%FS==================================================================
//
// USB_PE_DoNonsplit
//
// Description:
//   USB_PE_DoNonsplit implements a non-split transaction.
//   Refer to USB2.0 standard p216(figure 8-26).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoNonsplit(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
                      USB_HsBusRec_t* p_hsx) {

  USB_HostCtrlTop_t*   p_top;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( PLAT_GetHandleFromTransTable(),
                MSG_TRACE, "<%.10lld> USB_PE_DoNonsplit: PID: %s, cur dir: %s, EP type: %s \n",
                PLAT_GetSimTime(), carbonXUsbCvtPidToString( p_trans->pid ), carbonXUsbCvtDirTypeToString(p_hccmd->direction),
                carbonXUsbCvtEndPtTypeToString( p_hccmd->ep_type ) );
#endif

  switch (p_hccmd->direction) {
  case USB_DIR_IN:
    switch ( p_hccmd->ep_type ) {
    case USB_EPTYPE_ISO:
      // ISOCHRONOUS IN transaction
      USB_PE_DoIsoI(p_trans, p_hccmd, p_hsx);      
      break;  // End ISOCHRONOUS IN transaction
    
    case USB_EPTYPE_BULK:
    case USB_EPTYPE_CTRL:
    case USB_EPTYPE_INTR:
      // BULK/CONTROL/INTERRUPT IN transaction
      USB_PE_DoBcintI(p_trans, p_hccmd, p_hsx);
      break; // End BULK/CONTROL/INTERRUPT IN transaction
    default:
      MSG_Panic("USB_PE_DoNonsplit: Illegal Endpoint type: %d!\n", p_hccmd->ep_type );
      break;
      
    } // End IN direction transaction

    break; // End IN direction
  
  case  USB_DIR_OUT:
    switch ( p_hccmd->ep_type ) {
    case USB_EPTYPE_ISO:
      // ISOCHRONOUS OUT transaction, reference
      // to USB2.0 standard p230(figure 8-40). 
      USB_PE_IssuePacket(p_trans);

      USB_PE_ReadBfmReg(p_trans, p_hsx);
      
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
      break;// End ISOCHRONOUS OUT transaction
              
    case USB_EPTYPE_BULK:
    case USB_EPTYPE_CTRL:
    case USB_EPTYPE_INTR:
      if( (p_hccmd->ep_type == USB_EPTYPE_INTR) || !p_hccmd->is_hs ) {
        // INTERRUPT OUT transaction, full-/low- speed BULK/CONTROL OUT transaction.
        USB_PE_DoBcintO(p_trans, p_hccmd, p_hsx);
      }
      else {
        // High-speed BULK/CONTROL transaction
        USB_PE_DoHsBcO(p_trans, p_hccmd, p_hsx);
      }
      break;
    default:
      break;
    } // End switch ( p_hccmd->ep_type )
        
    break;// End OUT direction

  default:
    break;

  } // End switch (p_hccmd->direction)
}  // End non-split transaction

//%FS==================================================================
//
// USB_PE_DoStart
//
// Description:
//   USB_PE_DoStart implements a start-split transaction.
//   Refer to USB2.0 standard p352(figure 11-36).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoStart(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
                   USB_HsBusRec_t* p_hsx) {

  USB_HostCtrlTop_t*   p_top;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  switch (p_hccmd->direction) {
  case USB_DIR_IN:
    // IN direction transaction
    
    switch ( p_hccmd->ep_type ) {
    case USB_EPTYPE_ISO:
      // ISOCHRONOUS IN SSPLIT transaction,
      // reference to USB2.0 standard 
      // p400(figure 11-88)     

      USB_PE_IssuePacket(p_trans); 

      USB_PE_ReadBfmReg(p_trans, p_hsx);
      USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);
      break;  // End ISOCHRONOUS IN SSPLIT transaction

    case USB_EPTYPE_INTR:
      // INTERRUPT IN SSPLIT transaction,
      // reference to USB2.0 standard 
      // p389(figure 11-76)


      USB_PE_IssuePacket(p_trans);

      USB_PE_ReadBfmReg(p_trans, p_hsx);
      USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);     
      break;  // End INTERRUPT IN SSPLIT transaction
    
    case USB_EPTYPE_BULK:
    case USB_EPTYPE_CTRL:
      // BULK/CONTROL IN SSPLIT transaction

      USB_PE_DoBcIss(p_trans, p_hccmd, p_hsx);
      break; // End BULK/CONTROL IN SSPLIT transaction

    default:
      break;
    } // End switch ( p_hccmd->ep_type )

    break;  // End IN direction transaction
  
  case  USB_DIR_OUT:
    // OUT direction transaction
    switch ( p_hccmd->ep_type ) {
    case USB_EPTYPE_ISO:
      // ISOCHRONOUS OUT SSPLIT transaction, 
      // reference to USB2.0 standard
      // p398(figure 11-86) 

      USB_PE_IssuePacket(p_trans); 

      USB_PE_ReadBfmReg(p_trans, p_hsx);
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
      break;  // End ISOCHRONOUS OUT transaction
    
    case USB_EPTYPE_INTR:
      // INTERRUPT OUT SSPLIT transaction,
      // reference to USB2.0 standard
      // p386(figure 11-72)

      USB_PE_IssuePacket(p_trans); 

      USB_PE_ReadBfmReg(p_trans, p_hsx);
      USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);    
      break;  // End INTERRUPT OUT SSPLIT transaction
    
    case USB_EPTYPE_BULK:
    case USB_EPTYPE_CTRL:
      // BULK/CONTROL OUT SSPLIT transaction
      USB_PE_DoBcOss(p_trans, p_hccmd, p_hsx);
      break;  // End BULK/CONTROL OUT SSPLIT transaction

    default:
      break;
    } // End switch ( p_hccmd->ep_type )

  default:
    break;
  } // End switch (p_hccmd->direction)

  return;

}// End start split transaction

//%FS==================================================================
//
// USB_PE_DoComplete
//
// Description:
//   USB_PE_DoComplete implements a complete-split transaction.
//   Refer to USB2.0 standard p353(figure 11-37).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoComplete(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
                      USB_HsBusRec_t* p_hsx) {
  
  switch (p_hccmd->direction) {
  case USB_DIR_IN:
    // IN direction transaction
    
    switch ( p_hccmd->ep_type ) {
    case USB_EPTYPE_ISO:
      // ISOCHRONOUS IN CSPLIT transaction
      USB_PE_DoIsoIcs(p_trans, p_hccmd, p_hsx);
      break;  // End ISOCHRONOUS IN CSPLIT transaction
    
    case USB_EPTYPE_INTR:
      // INTERRUPT IN CSPLIT transaction
      USB_PE_DoIntIcs(p_trans, p_hccmd, p_hsx);
      break;  // End INTERRUPT IN CSPLIT transaction
    
    case USB_EPTYPE_BULK:
    case USB_EPTYPE_CTRL:
      // BULK/CONTROL IN CSPLIT transaction
      USB_PE_DoBcIcs(p_trans, p_hccmd, p_hsx);
      break;  // End BULK/CONTROL IN CSPLIT transaction

    default:
      break;
    } // End switch ( p_hccmd->ep_type )

    break;  // End IN direction transaction
  
  case  USB_DIR_OUT:
    // OUT direction transaction
    switch ( p_hccmd->ep_type ) {
    case USB_EPTYPE_INTR:
      // INTERRUPT OUT CSPLIT transaction
      USB_PE_DoIntOcs(p_trans, p_hccmd, p_hsx);
      break; // End INTERRUPT OUT CSPLIT transaction    

    case USB_EPTYPE_BULK:
    case USB_EPTYPE_CTRL:
      // BULK/CONTROL OUT CSPLIT transaction
      USB_PE_DoBcOcs(p_trans, p_hccmd, p_hsx);
      break;  // End BULK/CONTROL OUT CSPLIT transaction

    default:
      break;
    } // End switch ( p_hccmd->ep_type )

    break;  // End OUT direction transaction

  default:
    break;
  } // End switch (p_hccmd->direction)

  return;
}  // End complete split transaction



//%FS==================================================================
//
// USB_PE_DoIsoI
//
// Description:
//   USB_PE_DoIsoI implements an isochronnous IN non-split transaction.
//   Refer to USB2.0 standard p231(figure 8-42).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================

void USB_PE_DoIsoI( USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
        USB_HsBusRec_t* p_hsx) {
  
  USB_PE_IssuePacket(p_trans);
      
  USB_PE_WaitForPacket( USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;

  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response
    p_trans->response = USB_TIMEOUT;

    // Isochronous IN transaction error count inc by one and log it
    USB_PE_RecordError(p_hsx, p_hccmd);
  }
  else if ( p_hsx->is_crc_ok ) {
    //@@@ Issue CRC Error message, unless error is masked

    // Isochronous IN transaction error count inc by one and log it
    USB_PE_RecordError(p_hsx, p_hccmd);
  }
        
  USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);

  return;
} // End Isochronous IN non-split transaction


//%FS==================================================================
//
// USB_PE_DoBcintI
//
// Description:
//   USB_PE_DoBcintI implements a bulk/control/interrupt IN non-split
//   transaction.
//   Refer to USB2.0 standard p224(figure 8-33).
//
// Parameter:
//   USB_Trans_t*  p_trans       IO
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx      I 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoBcintI(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
      USB_HsBusRec_t* p_hsx) {

  BOOL                did_toggle;
  USB_HostCtrlTop_t  *p_top;

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

  USB_PE_IssuePacket(p_trans);
       
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);
  p_trans->response = p_hsx->pid;

  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintI  );
  }
  else if ( !p_hsx->is_crc_ok) {
    //@@@ Issue CRC Error message, unless error is masked
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintI  );
  }

  switch ( p_hsx->pid ) {
  case USB_PID_DATA0:
  case USB_PID_DATA1:
    did_toggle = USB_PE_Data01Toggle( p_trans->p_pipe->p_endpt_descr->prev_trans_toggle_pid,
                                      p_trans->p_pipe->p_endpt_descr->curr_trans_toggle_pid );

    if ( p_hccmd->needs_toggle == did_toggle ) {
      USB_PE_IssueHandshake(USB_PID_ACK);
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
    else {
      // Response is data but data pid did not toggle
      USB_PE_IssueHandshake(USB_PID_NAK);

      if(p_top->ErrorMsgDisabled.toggle_err == 0) {
      //      MSG_Printf("+       <time: %.10lld>:  Toggle Error\n", PLAT_GetSimTime());    
				MSG_Error("+       <time: %.10lld>:  Toggle Error  did_toggle=%d needs_toggle=%d\n",
									PLAT_GetSimTime(), did_toggle, p_hccmd->needs_toggle );
      }
      else MSG_Printf("+       <time: %.10lld>:  Allowed Toggle Error\n", PLAT_GetSimTime());
                  
      USB_PE_RespondHc(p_trans, USB_DO_SAME_CMD);
    }
    break;

  case USB_PID_NAK:
    // Response is NAK
		// fredieu - might want to have this as a maskable error.
    USB_PE_RespondHc(p_trans, USB_DO_SAME_CMD);
    break;

    // fredieu - I added this
  case USB_PID_ACK:
    // Response is ACK and it should not be.
    MSG_Error("Did not receive proper response in USB_PE_DoBcintI! PID = %s.  ACK is illegal\n",carbonXUsbCvtPidToString(p_hsx->pid));
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintI  );
    break;

  case USB_PID_STALL:
    // Response is STALL
    USB_PE_RespondHc(p_trans, USB_DO_HALT);
    break;

  case USB_PID_RESERVED:
    if(p_top->ErrorMsgDisabled.transaction_response_timeout == 0) {
      MSG_Error("Did not receive proper response in USB_PE_DoBcintI! Received PID = RESERVED/TIMEOUT\n");
    }
    else {
      MSG_Printf("Received an allowed PID = RESERVED/TIMEOUT in USB_PE_DoBcintI!\n");
    }
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintI  );
    break;

  default:
    // Response is not data or proper handshake
		//    MSG_Error("Did not receive proper response in USB_PE_DoBcintI! PID = %d see USB_common.h\n",p_hsx->pid);
    MSG_Error("Did not receive proper response in USB_PE_DoBcintI! PID = %s\n",carbonXUsbCvtPidToString(p_hsx->pid));
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintI  );
    break;

  } // End switch ( p_hsx->pid )

  return;
}  // End bulk/control/interrupt IN non-split transaction



//%FS==================================================================
//
// USB_PE_DoBcintO
//
// Description:
//   USB_PE_DoBcintO implements an interrupt or full-speed/low-speed 
//   bulk/control OUT non-split transaction.
//   Refer to USB2.0 standard p222(figure 8-31).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoBcintO(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
      USB_HsBusRec_t* p_hsx) {


  USB_PE_IssuePacket(p_trans);

  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintO );
  }
  else if( p_hsx->pid == USB_PID_ACK ) {
      // Response is ACK
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
  else if( p_hsx->pid == USB_PID_NYET ) {
      // Response is NYET
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
  else if ( !p_hccmd->is_setup ) {
    if(p_hsx->pid == USB_PID_STALL) {
      // Response is STALL
      USB_PE_RespondHc(p_trans, USB_DO_HALT);
    }
    else if(p_hsx->pid == USB_PID_NAK) {
      // Response is NAK
      USB_PE_RespondHc(p_trans, USB_DO_SAME_CMD);
    }
    else {
      // Response is not a proper handshake packet
      MSG_Error("Did not receive proper response in USB_PE_DoBcintO - PID = %d!\n",p_hsx->pid);
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintO );
    }
  }
  else {
      // Response is not a proper handshake packet
      MSG_Error("Did not receive proper response 1 in USB_PE_DoBcintO!\n");
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcintO );
  }
}  // End interrupt or full-/low-speed bulk/control/ OUT non-split transaction



//%FS==================================================================
//
// USB_PE_DoHsBcO
//
// Description:
//   USB_PE_DoHsBcO implements a high-speed bulk/control OUT non-split 
//   transaction.
//   Refer to USB2.0 standard p221(figure 8-30).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoHsBcO(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
     USB_HsBusRec_t* p_hsx) {

  USB_PE_IssuePacket(p_trans);
             
  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoHsBcO );
  }
  else if( p_hsx->pid == USB_PID_ACK ) {
    if((p_hccmd->is_setup) || (!p_hccmd->is_ping)) {
      // Response is ACK
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
    else USB_PE_RespondHc(p_trans, USB_DO_OUT);
  }
  else if ( !p_hccmd->is_setup ) {
    if(p_hsx->pid == USB_PID_STALL) {
      // Response is STALL
      USB_PE_RespondHc(p_trans, USB_DO_HALT);
    }
    else if(p_hsx->pid == USB_PID_NAK) {
      // Response is NAK
      USB_PE_RespondHc(p_trans, USB_DO_PING);
    }    
    else if(p_hsx->pid == USB_PID_NYET && !p_hccmd->is_ping ) {
      // Response is NYET
      //      USB_PE_RespondHc(p_trans, USB_DO_NEXT_PING);
      // If we set response to USB_DO_NEXT_PING, then tests will fail,
      // if they receive a NYET for the last valid data transfer.
      // Rather than wrapping up, the test will start issuing PINGs which will be NAKed.
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
     }     
    else {
      // Response is not a proper handshake packet
      MSG_Error("Did not receive proper response in USB_PE_DoHsBcO! PID = %d\n",p_hsx->pid);
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoHsBcO );
    }
  }
  else {
      // Response is not a proper handshake packet
      MSG_Error("Did not receive proper response 1 in USB_PE_DoHsBcO!\n");
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoHsBcO );
  }
}  // End high-speed bulk/control OUT non-split transaction



//%FS==================================================================
//
// USB_PE_DoBcIss
//
// Description:
//   USB_PE_DoBcIss implements a bulk/control IN start-split transaction.
//   Refer to USB2.0 standard p369(figure 11-56).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoBcIss(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
     USB_HsBusRec_t* p_hsx) {

  USB_PE_IssuePacket(p_trans);


  // Wait for Handshake packet

  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcIss );
  }         
  else if(p_hsx->pid == USB_PID_NAK) {
    // Response is NAK
    USB_PE_RespondHc(p_trans, USB_DO_START);
  }
  else if(p_hsx->pid == USB_PID_ACK) {
    // Response is ACK
    USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);
  }
  else {
    // Response is not a proper handshake packet
    MSG_Error("Did not receive proper response in USB_PE_DoBcIss!\n");
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcIss  );
  }
}  // End bulk/control IN start-split transaction



//%FS==================================================================
//
// USB_PE_DoBcOss
//
// Description:
//   USB_PE_DoBcOss implements a bulk/control OUT start-split
//   transaction.
//   Refere to USB2.0 standard p366(figure 11-52).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoBcOss(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
     USB_HsBusRec_t* p_hsx) {
  
  USB_PE_IssuePacket(p_trans);
       
  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcOss );
  }
  else if(p_hsx->pid == USB_PID_ACK) {
    // Response is ACK
    USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);
  }
  else if(p_hsx->pid == USB_PID_NAK) {
    // Response is NAK
    USB_PE_RespondHc(p_trans, USB_DO_START);
  }
  else {
    // Response is not a proper handshake packet
    MSG_Error("Did not receive proper response in USB_PE_DoBcOss!\n");
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcOss  );
  }


  return;
}  // End bulk/control OUT start-split transaction



//%FS==================================================================
//
// USB_PE_DoIsoIcs
//
// Description:
//   USB_PE_DoIsoIcs implements an isochronous IN complete-split
//   transaction.
//   Refer to USB2.0 standard p401(figure 11-89).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoIsoIcs( USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
          USB_HsBusRec_t* p_hsx ) {

  USB_PE_IssuePacket(p_trans);

  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoIsoIcs );
  }
  else if( p_hsx->pid == USB_PID_DATA0 || p_hsx->pid == USB_PID_DATA1 || p_hsx->pid == USB_PID_DATA2 ) {
    // Response is DATAX     
    USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
  }
  else if( p_hsx->pid == USB_PID_MDATA || p_hsx->pid == USB_PID_NYET) {
    // Response is MDATA  or  NYET     
    if(p_hccmd->is_last) {
      // This is the last CSPLIT of the three        
      USB_PE_RecordError(p_hsx, p_hccmd);
      
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
    else {
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_COMPLETE);
    }
  }
  else if(p_hsx->pid == USB_PID_ERR_PRE) {
    // Response is handshake ERR      
    USB_PE_RecordError(p_hsx, p_hccmd);
    
    USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD); 
  }
  else{
    // Response is not a proper handshake packet
    MSG_Error("Did not receive proper response 1 in USB_PE_DoIsoIcs!\n");
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoIsoIcs  );
  }

  return;

}  // End isochronous IN complete-split transaction



//%FS==================================================================
//
// USB_PE_DoIntIcs
//
// Description:
//   USB_PE_DoIntIcs implements an interrupt IN complete-split 
//   transaction.
//   Refer to USB2.0 standard p390(figure 11-77) and p391(figure 11-79) 
//   p391(figure 11-78).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoIntIcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
      USB_HsBusRec_t* p_hsx) {

  BOOL  did_toggle;


  USB_PE_IssuePacket(p_trans);

  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoIntIcs );
  }
  else if(p_hsx->pid == USB_PID_NYET) {
    // Response is NYET
    if(p_hccmd->is_last) {
      // This is the last CSPLIT of the three        
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoIntIcs );
    }
    else {
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_COMPLETE);
    }          
  }
  else if(p_hsx->pid == USB_PID_STALL) {
    // Response is STALL
    USB_PE_RespondHc(p_trans, USB_DO_HALT); 
  }
  else if(p_hsx->pid == USB_PID_NAK) {
    // Response is NAK
    USB_PE_RespondHc(p_trans, USB_DO_START);
  }
  else {
    // Response is not handshake    
    if( p_hsx->pid == USB_PID_DATA0 || p_hsx->pid == USB_PID_DATA1 ) {
      // Response is DATAx
      did_toggle = USB_PE_Data01Toggle( p_trans->p_pipe->p_endpt_descr->prev_trans_toggle_pid,
                                        p_trans->p_pipe->p_endpt_descr->curr_trans_toggle_pid );
  
      if( p_hccmd->needs_toggle == did_toggle ) {
        USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
      }
      else {
        //        MSG_Warn("+       <time: %.10lld>:  Toggle Error\n", PLAT_GetSimTime());
        MSG_Error("+       <time: %.10lld>:  Toggle Error\n", PLAT_GetSimTime());
        USB_PE_RespondHc(p_trans, USB_DO_START);
      }
    }
    else if(p_hsx->pid == USB_PID_MDATA) {
      // Response is MDATA (ref. USB 2.0 spec page 393)
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_COMPLETE);
    }
    else {    
      // Response is not a proper packet
      USB_PE_IncError(p_trans);

      if(p_trans->p_pipe->sys_record.err_cnt < 3) {
        USB_PE_RespondHc(p_trans, USB_DO_COMP_IMMED_NOW);
      }
      else {
          USB_PE_RespondHc(p_trans, USB_DO_HALT);
      }
    }
  }

  return;
}  // End interrupt IN complete-split transaction



//%FS==================================================================
//
// USB_PE_DoBcIcs
//
// Description:
//   USB_PE_DoBcIcs implements a bulk/control IN complete-split 
//   transaction.
//   Refer to USB2.0 standard p370(figure 11-57).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoBcIcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
     USB_HsBusRec_t* p_hsx) {

  BOOL  did_toggle;


  USB_PE_IssuePacket(p_trans);

  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcIcs );
  }
  else if(p_hsx->pid == USB_PID_NAK) {
    // Response is NAK
    USB_PE_RespondHc(p_trans, USB_DO_START);
  }
  else if(p_hsx->pid == USB_PID_STALL) {
    // Response is STALL
    USB_PE_RespondHc(p_trans, USB_DO_HALT);
  }
  else if(p_hsx->pid == USB_PID_NYET) {
    // Response is NYET
    USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);
  }
  else if(p_hsx->pid == USB_PID_DATA0 || p_hsx->pid == USB_PID_DATA1 ) {
    // Response is DATAX    

    did_toggle = USB_PE_Data01Toggle( p_trans->p_pipe->p_endpt_descr->prev_trans_toggle_pid,
                                      p_trans->p_pipe->p_endpt_descr->curr_trans_toggle_pid );

    if( did_toggle == p_hccmd->needs_toggle ) {
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
    else {
      //      MSG_Warn("+       <time: %.10lld>:  Toggle Error\n", PLAT_GetSimTime());    
      MSG_Error("+       <time: %.10lld>:  Toggle Error\n", PLAT_GetSimTime());    
      USB_PE_RespondHc(p_trans, USB_DO_START);
    }
  }
  else {
    // Response is improper    
    MSG_Error("Did not receive proper response in USB_PE_DoBcIcs!\n");
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcIcs  );
  }

  return;
}  // End bulk/control IN complete-split transaction



//%FS==================================================================
//
// USB_PE_DoIntOcs
//
// Description:
//   USB_PE_DoIntOcs implements an interrupt OUT complete-split 
//   transaction.
//   Refer to USB2.0 standard p387(figure 11-73).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoIntOcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
      USB_HsBusRec_t* p_hsx) {
          
  USB_PE_IssuePacket(p_trans);

  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoIntOcs );
  }
  else if(p_hsx->pid == USB_PID_ACK) {
    // Response is ACK
    USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
  }   
  else if(p_hsx->pid == USB_PID_NAK) {
    // Response is NAK
    USB_PE_RespondHc(p_trans, USB_DO_START);
  }   
  else if(p_hsx->pid == USB_PID_NYET) {
    // Response is NYET    
    if(p_hccmd->is_last) {
      // This is the last CSPLIT of the three        
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoIntOcs  );
    }
    else {
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_COMPLETE);
    }
  }
  else if(p_hsx->pid == USB_PID_STALL) {
    // Response is STALL
    USB_PE_RespondHc(p_trans, USB_DO_HALT);
  }
  else if(p_hsx->pid == USB_PID_ERR_PRE) {
    // Response is handshake ERR      
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoIntOcs  );
  }
  else {
    // Response is improper
    USB_PE_IncError(p_trans);

    if(p_trans->p_pipe->sys_record.err_cnt < 3) {
      USB_PE_RespondHc(p_trans, USB_DO_COMP_IMMED_NOW);
    }
    else {
      USB_PE_RespondHc(p_trans, USB_DO_HALT);
    }
  }

  return;

}  // End interrupt OUT complete-split transaction



//%FS==================================================================
//
// USB_PE_DoBcOcs
//
// Description:
//   USB_PE_DoBcOcs implements a bulk/control OUT complete-split 
//   transaction.
//   Refer to USB2.0 standard p367(figure 11-53).
//
// Parameter:
//   USB_Trans_t*  p_trans  IO 
//     It points to the struct that shall be transfered.
//   USB_CommandRec_t*  p_hccmd  IO 
//     It points to the struct that is used to control the state 
//     machine by the HC FSM.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_DoBcOcs(USB_Trans_t* p_trans, USB_CommandRec_t* p_hccmd,
     USB_HsBusRec_t* p_hsx) {

  u_int8  bcocs_pid       = 0;
        
  
  bcocs_pid   = *(p_trans->p_data_segment->p_data) & 0xF;

  USB_PE_IssuePacket(p_trans);

  // Wait for Handshake packet
  USB_PE_WaitForPacket(USB_WAIT_ITG, p_trans, p_hsx);

  p_trans->response = p_hsx->pid;
  
  if ( p_hsx->did_timeout ) {
    // Timed out waiting for response

    p_trans->response = USB_TIMEOUT;
    USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcOcs );
  }
  else if(p_hccmd->ep_type == USB_EPTYPE_CTRL && bcocs_pid == USB_PID_SETUP) {
    // Control SETUP transaction    
        
    if(p_hsx->pid == USB_PID_NYET) {
      // Response is NYET
      USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);
    }
    else if(p_hsx->pid == USB_PID_STALL) {
      // Response is STALL
      USB_PE_RespondHc(p_trans, USB_DO_HALT);
    }
    else if(p_hsx->pid == USB_PID_ACK) {
      // Response is ACK
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
    else {
      // Response is not a proper handshake packet
      MSG_Error("Did not receive proper response in USB_PE_DoBcOcs!\n");
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcOcs  );
    }
  } // End Control SETUP transaction
  else if((p_hccmd->ep_type == USB_EPTYPE_CTRL && bcocs_pid != USB_PID_SETUP) 
          || p_hccmd->ep_type == USB_EPTYPE_BULK ) {
    // OUT data transaction
      
    if(p_hsx->pid == USB_PID_NYET) {
      // Response is NYET
      USB_PE_RespondHc(p_trans, USB_DO_COMPLETE);
    }
    else if(p_hsx->pid == USB_PID_STALL) {
      // Response is STALL
      USB_PE_RespondHc(p_trans, USB_DO_HALT);
    }
    else if(p_hsx->pid == USB_PID_ACK) {
      // Response is ACK
      USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
    }
    else if(p_hsx->pid == USB_PID_NAK) {
      // Response is NAK
      USB_PE_RespondHc(p_trans, USB_DO_START);
    }
    else {
      // Response is not a proper handshake packet
      MSG_Error("Did not receive proper response in USB_PE_DoBcOcs!\n");
      USB_PE_HandleProtocolError( p_trans, p_hsx, p_hccmd, USB_DoBcOcs  );
    }
  }  // End OUT data transaction

  return;
}  // End bulk/control OUT complete-split transaction



//%FS==================================================================
//
// USB_PE_IssuePacket
//
// Description:
//   USB_PE_IssuePacket is the low level function responsible for
//   sending one atomic USB packet to the BFM.
//   During the generation, it checks if an error should be injected 
//   and which kind of error should be injected. It calls carbonXCsWrite
//   to send the information to the BFM configuration register. 
//   Then the packets are transferred to the BFM by calling SIM_SendPacket.
//
//   Refer to USB2.0 standard p558~559.
//
// Parameter:
//   USB_Trans_t*  p_trans  I 
//     It points to the struct that shall be transfered..
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_IssuePacket(USB_Trans_t* p_trans) {
  u_int32 pkt_size        = 0;
  u_int8* p_tx_pkt        = NULL;
  
  USB_Packet_t      usb_pkt;
  USB_Packet_t*     p_usb_pkt = NULL;

  static u_int32 was_sof = 0;

  p_usb_pkt = &usb_pkt;

  p_usb_pkt->token_pkt[0] = 0;
  p_usb_pkt->token_pkt[1] = 0;
  p_usb_pkt->token_pkt[2] = 0;
  p_usb_pkt->token_pkt[3] = 0;

  if ( p_trans->time_first_attempted == 0 ) {
    //@@@RZ Note: We are currently building a p_trans from scratch each time
    // we are calling USB_SCH_ExecTrans, even if it is a retry!
    // Therefore time_first_attempted is always 0 and we never get here.
    p_trans->time_first_attempted = PLAT_GetSimTime();
  }

  p_trans->time_started = PLAT_GetSimTime();

#if (USB_LOOP_BACK == 0)
  USB_PE_WriteBfmReg(p_trans);
#endif  

  // Log some messages
  USB_PE_MsgSpeed(p_trans);

  if (p_trans->trans_type != USB_TRANS_TYPE_SOF) {
    USB_PE_MsgEptType(p_trans);
  }

  USB_PE_MsgErrInject(p_trans);

  USB_PE_BuildPktFromTrans(p_trans, p_usb_pkt); // Build packet
  p_tx_pkt = (u_int8*)p_usb_pkt;

  p_trans->transferred_pid = p_trans->pid;
  
  if(p_trans->pid == USB_PID_SPLIT) {

#if (USB_LOOP_BACK == 0)
    if(USB_IPG != 0) MSG_Panic("USB_IPG in USB_protocol_eng.h may only be 0 at the moment!\n");

    XPORT_SendPacket(p_tx_pkt, 4, USB_IPG);   // Send SPLIT token packet
    carbonXIdle( USB_INTERPACKET_CYCLES );
#endif

    p_tx_pkt += 4;  
    USB_PE_MsgAfterSplit(p_tx_pkt);
  }

#if (USB_LOOP_BACK == 0)
  if(USB_IPG != 0) MSG_Panic("USB_IPG in USB_protocol_eng.h may only be 0 at the moment!\n");

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( PLAT_GetHandleFromTransTable(),
                MSG_TRACE, "<%.10lld> USB_PE_IssuePacket: calling SendPacket: \n", PLAT_GetSimTime() );
#endif  // ifdef VERBOSE_DEBUG

  XPORT_SendPacket(p_tx_pkt, 4, USB_IPG);     // Send token packet
  carbonXIdle( USB_INTERPACKET_CYCLES );

#endif // if (USB_LOOP_BACK == 0)

  p_tx_pkt += 4;
  pkt_size = (p_trans->pid == USB_PID_SPLIT) ? p_trans->p_data_segment->size - 4 :
    p_trans->p_data_segment->size;

  if(p_tx_pkt != NULL && pkt_size != 0) {

    USB_PE_MsgData(p_tx_pkt, pkt_size);

#if (USB_LOOP_BACK == 0)
    if(USB_IPG != 0) MSG_Panic("USB_IPG in USB_protocol_eng.h may only be 0 at the moment!\n");

#ifdef VERBOSE_DEBUG
    MSG_LibDebug( PLAT_GetHandleFromTransTable(),
		  MSG_TRACE, "<%.10lld> USB_PE_IssuePacket: calling SendPacket: \n", PLAT_GetSimTime() );
#endif // ifdef VERBOSE_DEBUG

    XPORT_SendPacket(p_tx_pkt, pkt_size, USB_IPG); // Send data packet
    carbonXIdle( USB_INTERPACKET_CYCLES ); 

#endif
  }

  p_trans->time_completed = PLAT_GetSimTime();

  if (p_trans->trans_type != USB_TRANS_TYPE_SOF) was_sof = 0;
  else was_sof = 1;

}

//%FS==================================================================
//
// USB_PE_WriteBfmReg
//
// Description:
//   USB_PE_WriteBfmReg calculates the control information that should be 
//   sent to XIF, then calls carbonXCsWrite to write these information
//   to the BFM register.
//
// Parameter:
//   USB_Trans_t*  p_trans  I 
//     It points to the struct that shall be transfered.
//     USB_PE_WriteBfmReg collects all necessary information from it.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_WriteBfmReg(USB_Trans_t* p_trans) {  
  
  u_int32 ctrl_info = 0;
  USB_HostCtrlTop_t*   p_top;
  static int first = 1;
  static u_int32 ctrl_info_last = 0;

  p_top   = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
 
  // carbonXIdle(2);

  if(first) {
    first = 0;
    ctrl_info = carbonXCsRead(USB_BFM_ADDR_CFG_READ);
  }
  else ctrl_info = ctrl_info_last;

  ctrl_info &= ~(0x3 + USB_CTRL_LS_ATTACH);
  ctrl_info &= ~(USB_CTRL_ADD_PRE + USB_CTRL_TIMEOUT_ERR + USB_CTRL_CRC_ERR +
                 USB_CTRL_DATA_CORRUPT + USB_CTRL_DATA_DROP + USB_CTRL_CORRUPT_PID +
                 USB_CTRL_SPLIT_PHASE + USB_CTRL_TOKEN_PHASE + USB_CTRL_DATA_PHASE);

  // Bus speed  
  ctrl_info |= ( p_trans->trans_speed << USB_STATUS_DEV_SPEED_BITS );

  if(p_trans->trans_speed == USB_TRANS_LS) ctrl_info = ctrl_info | USB_CTRL_LS_ATTACH; // LS_Attach

  // Check, if preamble error should be injected by BFM
  ctrl_info |= (p_trans->pid == USB_PID_ERR_PRE) ? USB_CTRL_ADD_PRE : 0;   

  // Judge if error(s) should be injected by BFM
  ctrl_info |= (p_trans->err_inject.do_timeout_err) ? USB_CTRL_TIMEOUT_ERR : 0;   
                      // Inject timeout
  ctrl_info |= (p_trans->err_inject.do_crc_err) ? USB_CTRL_CRC_ERR : 0;
                      // Inject crc error   
  ctrl_info |= (p_trans->err_inject.do_data_corrupt) ? USB_CTRL_DATA_CORRUPT : 0;   
                      // Inject data corruption   
  ctrl_info |= (p_trans->err_inject.do_data_drop) ? USB_CTRL_DATA_DROP : 0;   
                      // Inject data drop
  ctrl_info |= (p_trans->err_inject.do_pid_err) ? USB_CTRL_CORRUPT_PID : 0;


  // Decide the phase(split token phase, token phase, or data phase) error injected
  ctrl_info |= (p_trans->err_inject.is_split_phase) ? USB_CTRL_SPLIT_PHASE : 0;   
                      // Inject error in split token phase
  ctrl_info |= (p_trans->err_inject.is_token_phase) ? USB_CTRL_TOKEN_PHASE : 0;
                      // Inject error in token phase
  ctrl_info |= (p_trans->err_inject.is_data_phase) ? USB_CTRL_DATA_PHASE : 0;   
                      // Inject error in data phase   
  // Write to BFM configuration register
  if(ctrl_info_last != ctrl_info) {
    carbonXCsWrite(USB_BFM_ADDR_CFG_REGISTER, ctrl_info);
    ctrl_info_last = ctrl_info;
  }

  //MSG_LibDebug( "usb", 2, "<time: %.10lld>: Writing Expected Duration (%d cycles) to BFM with %d ns clock cycle.\n",
  //  PLAT_GetSimTime(), p_trans->expected_duration / p_top->bfm_clock_cycle, p_top->bfm_clock_cycle);
  //@@@ Not really needed - removed by Fredieu
  //  carbonXCsWrite(USB_BFM_ADDR_EXPECTED_DURATION, p_trans->expected_duration / p_top->bfm_clock_cycle);
}   

//%FS==================================================================
//
// USB_PE_IssueHandshake
//
// Description:
//   USB_PE_IssueHandshake issues a specified handshake according to
//   the request, this function is used by the HC FSM to respond to
//   the device.
//
// Parameter:
//   USB_Pid_e  hs_pid  I 
//     It requests USB_PE_IssueHandshake to issue a specified handshake.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_IssueHandshake(USB_Pid_e hnds_pid) {
  
  //  u_int8  usb_hnds;
  u_int32  usb_hnds;  // @@@RZ use 32 bit instead of 8 bit as work-around for TBP endianness problem

  usb_hnds = USB_BuildPid( hnds_pid );

#if (USB_LOOP_BACK == 0)

  // MSG_LibDebug( "usb", 2, <time: %.10lld>: Writing Expected Duration (%d cycles) to BFM.\n",
  //       PLAT_GetSimTime(), HS_EXPECTED_DURATION);
  // removed by Fredieu
  //  carbonXCsWrite(USB_BFM_ADDR_EXPECTED_DURATION, HS_EXPECTED_DURATION);
          
  if(USB_IPG != 0) MSG_Panic("USB_IPG in USB_protocol_eng.h may only be 0 at the moment!\n");

#ifdef VERBOSE_DEBUG
  MSG_LibDebug( PLAT_GetHandleFromTransTable(),
                MSG_TRACE, "<%.10lld> USB_PE_IssueHandshake: calling SendPacket: \n", PLAT_GetSimTime() );
#endif

  XPORT_SendPacket((u_int8 *)&usb_hnds, HS_SIZE, USB_IPG);
  carbonXIdle( USB_INTERPACKET_CYCLES );

#endif
  
  USB_PE_MsgHandshake( (u_int8*)&usb_hnds, hnds_pid, HS_SIZE );  
}

//%FS==================================================================
//
// USB_PE_WaitForPacket
//
// Description:
//   USB_PE_WaitForPacket waits for the device's response.
//   If there is no timeout, it receives a packet
//   from the device and passes it to the USB_Trans_t struct.
//
//   Refer to USB2.0 standard p556~557.
//
// Parameter:
//   USB_Waits_e  wait_type  I
//     It requests USB_PE_WaitForPacket to wait for the inter packet gap
//     or wait forever for next packet.
//   USB_Trans_t*  p_trans  IO
//     It points to the data struct into which the packet will be transfered.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side.  This function changes some
//     fields of the struct. 
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_WaitForPacket( USB_Waits_e wait_type, 
                       USB_Trans_t* p_trans, USB_HsBusRec_t* p_hsx) {
          
  u_int32   rx_pkt_length = 0;
  u_int8*   p_recvd_pkt   = NULL;

#if (USB_LOOP_BACK == 2)
  u_int8    rx_pkt[1] = {210};

  p_recvd_pkt   = rx_pkt;   
  rx_pkt_length = 1;      // ****** For debug
#endif  

  (void) wait_type;

#if (USB_LOOP_BACK == 1)
  p_recvd_pkt    = p_trans->p_data_segment->p_data;
  rx_pkt_length  = p_trans->p_data_segment->size;
          // ****** For loop back test 
#endif

#if (USB_LOOP_BACK == 0)
  p_recvd_pkt   = ( (USB_HostCtrlTop_t*)XPORT_GetMyProjectData() )->cp_sys_pe_buffer->p_data;

//MSG_LibDebug( "usb", 2, "A packet is expected from DUT at time %.10lld.\n", PLAT_GetSimTime() ); 
  rx_pkt_length = XPORT_ReceivePacket(p_recvd_pkt);
  //  carbonXIdle( USB_INTERPACKET_CYCLES );

  ( (USB_HostCtrlTop_t*)XPORT_GetMyProjectData() )->cp_sys_pe_buffer->size = rx_pkt_length;

  USB_PE_ReadBfmReg(p_trans, p_hsx);  // Read BFM status register
#endif


  // Store completion time
  p_trans->time_completed = PLAT_GetSimTime();
  
  // Packet is received
  if(rx_pkt_length == 0 && p_hsx->did_timeout == FALSE) {
    MSG_Warn("The returned data size is 0\n");    
  }
  else {
//    MSG_LibDebug("usb", 3, "<time: %.10lld>:  Received packet of length %d bytes\n",
//                   PLAT_GetSimTime(), rx_pkt_length);
      
    if(p_hsx->did_timeout == FALSE) {
      
      USB_PE_BuildTransFromPkt(p_trans); // Build USB_Trans_t from received packet          

      // If this is a DATA PID store it for later consideration
      if ( carbonXUsbIsDataPid( p_trans->pid ) ){
        p_trans->p_pipe->p_endpt_descr->prev_trans_toggle_pid = p_trans->p_pipe->p_endpt_descr->curr_trans_toggle_pid;
        p_trans->p_pipe->p_endpt_descr->curr_trans_toggle_pid = p_trans->pid; 
      }

      p_hsx->pid = p_trans->pid;
    }
  }

  p_trans->p_pipe->sys_record.response_pid = p_hsx->pid;

  if(p_hsx->pid == USB_PID_NAK) {
    p_trans->p_pipe->sys_record.nak_count++;
  }
  else {
    p_trans->p_pipe->sys_record.nak_count = 0;
  }
  if(p_trans->p_pipe->sys_record.nak_count > 100) {
    MSG_Panic("Too many NAKs\n");
  }
                    
}// End USB_PE_WaitForPacket

//%FS==================================================================
//
// USB_PE_ReadBfmReg
//
// Description:
//   USB_PE_ReadBfmReg reads the BFM status register, then collects some
//   information.
//
// Parameter:
//   USB_Trans_t*  p_trans  IO
//     It points to the struct that shall be transfered, some
//     fields will be changed by this function.
//   USB_HsBusRec_t*  p_hsx  IO 
//     It points to the struct that is used to record the returned
//     information from sim-side, some fields will be changed in 
//     by function.  
//
// Return value:
//   u_int32 
//     The returned value is the BFM status register information.
//
//%FE==================================================================
void USB_PE_ReadBfmReg(USB_Trans_t* p_trans, USB_HsBusRec_t* p_hsx) {

  u_int32 status_crc_err  = 0;
  u_int32 status_pid_err  = 0;
  u_int32 status_timeout  = 0;
  u_int32 status_info     = 0;
  USB_HostCtrlTop_t*  p_top;

  p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();
  
#if (USB_LOOP_BACK == 0)
  status_info                     = carbonXCsRead(USB_BFM_ADDR_STATUS_REGISTER);
  // p_trans->time_first_attempted = carbonXCsRead(USB_BFM_ADDR_TIME_START);  //@@@ Should be removed
  // p_trans->time_completed       = carbonXCsRead(USB_BFM_ADDR_TIME_END);    //@@@ Should be removed
#endif

  if(p_trans->err_inject.receive_timeout) status_info |= USB_STATUS_TIMEOUT;
  if(p_trans->err_inject.receive_crc_err) status_info |= USB_STATUS_CRC_ERR;
  if(p_trans->err_inject.receive_pid_err) status_info |= USB_STATUS_PID_ERR;

  status_timeout  = status_info & USB_STATUS_TIMEOUT;
  
  if(status_timeout == USB_STATUS_TIMEOUT) {

    // There is a timeout
    if(p_top->ErrorMsgDisabled.transaction_response_timeout == 0) {
      MSG_Printf("\n\n***************************************************\n\n");
      MSG_Error("+       Timeout From Status 0x%x & 0x%x\n", status_info,USB_STATUS_TIMEOUT);
      MSG_Printf("\n\nTO MASK USE\n");
      MSG_Printf("\n\nUSB_HostCtrlTop_t*  p_top;\n");
      MSG_Printf("p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();\n");
      MSG_Printf("p_top->ErrorMsgDisabled.transaction_response_timeout = 1;\n");
      MSG_Printf("\n\n*************************************************\n\n\n");
    }
    else MSG_Printf("+       Allowed Timeout From Status 0x%x & 0x%x\n", status_info, USB_STATUS_TIMEOUT); 

    p_hsx->did_timeout = TRUE;
  }
  
  else{
    p_hsx->did_timeout = FALSE;
    
    status_crc_err     = USB_STATUS_CRC_ERR;
    status_crc_err    &= status_info;
    
    if(status_crc_err == USB_STATUS_CRC_ERR) {
                // Crc is bad
      p_hsx->is_crc_ok   = FALSE;
      p_trans->is_crc_ok = FALSE;
      if(p_top->ErrorMsgDisabled.transaction_response_crc == 0) {
        MSG_Printf("\n\n***************************************************\n\n");
        MSG_Error("+       CRC Error from Status 0x%x & 0x%x\n", status_info,USB_STATUS_CRC_ERR);    
        MSG_Printf("\n\nTO MASK USE\n");
        MSG_Printf("\n\nUSB_HostCtrlTop_t*  p_top;\n");
        MSG_Printf("p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();\n");
        MSG_Printf("p_top->ErrorMsgDisabled.transaction_response_crc = 1;\n");
        MSG_Printf("\n\n*************************************************\n\n\n");
      }
      else MSG_Printf("+       Allowed CRC Error from Status 0x%x & 0x%x\n", status_info,USB_STATUS_CRC_ERR);
    }
    else {        // Crc is good
      p_hsx->is_crc_ok   = TRUE;
      p_trans->is_crc_ok = TRUE;
    }

    status_pid_err     = USB_STATUS_PID_ERR;
    status_pid_err    &= status_info;

    if(status_pid_err == USB_STATUS_PID_ERR) {
                // PID is bad
      if(p_top->ErrorMsgDisabled.transaction_response_pid_err == 0) {
        MSG_Printf("\n\n***************************************************\n\n");
        MSG_Error("+       PID Error from Status 0x%x & 0x%x\n",status_info,USB_STATUS_PID_ERR);    
        MSG_Printf("\n\nTO MASK USE\n");
        MSG_Printf("\n\nUSB_HostCtrlTop_t*  p_top;\n");
        MSG_Printf("p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();\n");
        MSG_Printf("p_top->ErrorMsgDisabled.transaction_response_pid_err = 1;\n");
        MSG_Printf("\n\n*************************************************\n\n\n");
      }
      else MSG_Printf("+       Allowed PID Error from Status 0x%x & 0x%x\n", status_info,USB_STATUS_PID_ERR); 
    }
  }
}

//%FS==================================================================
//
// USB_PE_IncError
//
// Description:
//   USB_PE_IncError increases the error count of a pipe.
//   Refer to USB2.0 standard p559.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to a USB transfer struct
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_IncError(USB_Trans_t* p_trans) {
   
  // Increase error count of pipe associated with this transfer
  p_trans->p_pipe->sys_record.err_cnt += 1;
            
//  MSG_LibDebug("usb", 3, "<time: %.10lld>:  There is an error, now the error count is %d.\n", 
//               PLAT_GetSimTime(), p_trans->p_pipe->sys_record.err_cnt);
  MSG_Printf("<time: %.10lld>:  There is an error, now the error count is %d.\n", 
               PLAT_GetSimTime(), p_trans->p_pipe->sys_record.err_cnt);
}

//%FS==================================================================
//
// USB_PE_RespondHc
//
// Description:
//   USB_PE_RespondHc responds with a HC trans state to the scheduler.
//   Refer to USB2.0 standard p559.
//
// Parameter:
//   USB_Trans_t*  p_trans               IO
//     It points to the struct should be transfered, USB_PE_RespondHc
//     writes a trans state to the struct. 
//   USB_HcTransState_e  hc_trans_state   I
//     USB_PE_RespondHc writes it to the struct. 
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_RespondHc(USB_Trans_t* p_trans, USB_HcTransState_e hc_trans_state) {
    
  p_trans->trans_state = hc_trans_state;
  
//  MSG_LibDebug("usb", 3, "<time: %.10lld>:  Trans state is %s.\n", 
//           PLAT_GetSimTime(), carbonXUsbCvtHcTransStateToString( p_trans->trans_state ) );
}

//%FS==================================================================
//
// USB_PE_RecordError
//
// Description:
//   USB_PE_RecordError is used in isochronous transfers. Because
//   isochronous transfers have no handshake phase, this function is
//   used to record an error in case of an isochronous transfers.
//
// Parameter:
//   USB_HsBusRec_t*  p_hsx  I
//     It points to the struct that is used to record the returned
//     information from sim-side, USB_PE_RecordError collects some 
//     information from it.
//   USB_CommandRec_t*  p_hccmd  I
//     It points to the struct that is used to control the process, 
//     USB_PE_RecordError collects some information from it.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_RecordError(USB_HsBusRec_t* p_hsx, USB_CommandRec_t* p_hccmd) {
  
  static u_int32 iso_err_count = 0;

  iso_err_count += 1;
  
//  MSG_LibDebug("usb", 3, "<time: %.10lld>:  Record an isochronous transaction error, the error count is %d now.\n",
//               PLAT_GetSimTime(), iso_err_count);
  
  if(p_hsx->pid != USB_PID_DATA0 && p_hsx->pid != USB_PID_DATA1 
     && p_hsx->pid != USB_PID_DATA2) {
//    MSG_LibDebug("usb", 3, "<time: %.10lld>:  No Data error.\n", PLAT_GetSimTime());
  }
  
  else if((p_hsx->pid == USB_PID_DATA0 || p_hsx->pid == USB_PID_DATA1
          || p_hsx->pid == USB_PID_DATA2) && !p_hsx->is_crc_ok) {
//    MSG_LibDebug("usb", 3, "<time: %.10lld>:  DATA with crc error.\n", PLAT_GetSimTime());
  }
  
  else if(p_hsx->did_timeout) {
//    MSG_LibDebug("usb", 3, "<time: %.10lld>:  Timeout occurs.\n", PLAT_GetSimTime());
  }
  
  else if (p_hsx->pid == USB_PID_ERR_PRE) {
//    MSG_LibDebug("usb", 3, "<time: %.10lld>:  IN CSPLIT transfer respond ERR.\n", 
//                 PLAT_GetSimTime());
  }
  
  else if(p_hsx->pid == USB_PID_NYET && p_hccmd->is_last) {
//    MSG_LibDebug("usb", 3, "<time: %.10lld>:  Three times NYET handshake.\n", 
//                 PLAT_GetSimTime());
  }
}

//%FS==================================================================
//
// USB_PE_BuildPktFromTrans
//
// Description:
//   USB_PE_BuildPktFromTrans builds a packet according to the 
//   requirement of the USB_Trans_t.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the struct should be transfered, this function
//     gets all necessary information from the struct.
//   USB_Packet_t*  po_tx_packet  O
//     It points to the packet sent to the XIF.
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_BuildPktFromTrans(USB_Trans_t* p_trans, USB_Packet_t* po_tx_packet) {
  
  u_int32 i         = 0;
  u_int32 data_size = 0;
  u_int8* p_data;
 
  if(p_trans->pid == USB_PID_SOF) {
    // Build SOF packet
    USB_CvtInt32ToByteAry( USB_PE_BuildSof(p_trans), po_tx_packet->token_pkt);     
  }   
  else if(p_trans->pid == USB_PID_OUT || p_trans->pid == USB_PID_IN || 
          p_trans->pid == USB_PID_SETUP || p_trans->pid == USB_PID_PING) {
    // Build token packet   
    USB_CvtInt32ToByteAry( USB_PE_BuildToken(p_trans), po_tx_packet->token_pkt);     
  }
  else if(p_trans->pid == USB_PID_SPLIT) {
            // Build SPLIT token packet      
    USB_CvtInt32ToByteAry( USB_PE_BuildSplit(p_trans), po_tx_packet->token_pkt);     
  }

  data_size = p_trans->p_data_segment->size;

  if(data_size > 1028)
    MSG_Panic("USB_PE_BuildPktFromTrans: data_size of %d is too big.  1028 is the limit\n",data_size);

  if(data_size != 0) {
    
    p_data = p_trans->p_data_segment->p_data;
 
    for(i = 0;i < data_size; i++) {
      po_tx_packet->data_pkt[i] = *p_data;
      p_data += 1;
    }         // Build data packet
  }
  // fredieu - the following may or may not be needed but it needs some work.
  //  else if((p_trans->pid != USB_PID_PING) && (p_trans->pid != USB_PID_SOF)) {
  //    if( p_trans->pid != USB_IN) {
  //      MSG_Warn("USB_PE_BuildPktFromTrans: Packet size of 0  PID = %s !\n",
  //         carbonXUsbCvtPidToString(p_trans->pid));
  //    }
  //    else {
  //      MSG_Warn("USB_PE_BuildPktFromTrans: Packet size of 0 for USB_IN!\n");
  //    }
  //  }
}

//%FS==================================================================
//
// USB_PE_BuildSof
//
// Description:
//   USB_PE_BuildSof builds a SOF packet according to the information 
//   from the USB_Trans_t.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the struct should be transfered, this function
//     gets all necessary information from the struct.
//
// Return value:
//   u_int32
//     The return value is the SOF packet.
//
//%FE==================================================================
u_int32 USB_PE_BuildSof(USB_Trans_t* p_trans) {

  u_int32 sof_pid;
  u_int32 frame_num;
  u_int32 sof_pkt;

  sof_pid   = USB_BuildPid( p_trans->pid );    
  frame_num = p_trans->frame_num;      // Get frame number

  MSG_Printf( "+       <time: %.10lld>:  SOF <FRAME 0x%x>\n",
        PLAT_GetSimTime(), frame_num);    

  sof_pkt = (sof_pid << 24) | ( frame_num << 13); // Build SOF packet
  
  return (sof_pkt);
}

//%FS==================================================================
//
// USB_PE_BuildToken
//
// Description:
//   USB_PE_BuildToken builds a normal token packet according to the 
//   information from the USB_Trans_t.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the struct should be transfered, this function
//     get all necessary information from the struct.
//
// Return value:
//   u_int32
//     The return value is the token packet.
//
//%FE==================================================================
u_int32 USB_PE_BuildToken(USB_Trans_t* p_trans) {

  u_int32 pid       = 0;
  u_int32 addr      = 0;
  u_int32 ept_num   = 0;
  u_int32 token     = 0;

  pid = USB_BuildPid( p_trans->pid );

  if(p_trans->err_inject.do_pid_err && p_trans->err_inject.is_token_phase) {
    // Generate a bad pid by inverting the check bits

    //    pid = pid ^ 0x0f;
  }

  addr    = p_trans->dev_addr;         // Get device address
  ept_num = p_trans->p_pipe->p_endpt_descr->endpt_num;
            // Get endpoint number
  USB_PE_MsgToken(p_trans->pid, addr, ept_num);
    
  // Build token packet
  token = (pid << 24) | (addr << 17) | (ept_num << 13);

  return (token);
}

//%FS==================================================================
//
// USB_PE_BuildSplit
//
// Description:
//   USB_PE_BuildSplit builds a split token packet according to the 
//   information from the USB_Trans_t.
//
// Parameter:
//   USB_Trans_t*  p_trans  I
//     It points to the struct should be transfered, this function
//     gets all necessary information from the struct.
//
// Return value:
//   u_int32
//     The return value is the split token packet.
//
//%FE==================================================================
u_int32 USB_PE_BuildSplit(USB_Trans_t* p_trans) {

  u_int32 split_pid   = 0;
  u_int32 hub_addr    = 0;
  u_int32 start_comp  = 0;
  u_int32 hub_port    = 0;
  u_int32 split_s     = 0;
  u_int32 ssplit_e    = 0;
  u_int32 csplit_u    = 0;
  u_int32 ept_type    = 0;
  u_int32 split_token = 0;


  split_pid = USB_BuildPid( p_trans->pid );

  if(p_trans->err_inject.do_pid_err && p_trans->err_inject.is_split_phase) {
    // Generate a bad pid by inverting the check bits

    split_pid = split_pid ^ 0xF0;
  }                        

  hub_addr  = p_trans->dev_addr;  // Get hub address    
  hub_port  = p_trans->hub_port;  // Get hub port
  ept_type  = p_trans->p_pipe->p_endpt_descr->endpt_type;
            // Get endpoint type
  if((p_trans->p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL ||
      p_trans->p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_INTR) &&
      p_trans->trans_speed == USB_TRANS_LS) {
      
    split_s = 1;
  }

  if(p_trans->trans_type == USB_TRANS_TYPE_SSPLIT) {
          // SSPLIT token packet            
    start_comp   = 0;
          
    if(p_trans->p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_ISO &&
       p_trans->p_pipe->p_endpt_descr->curr_dir == USB_DIR_OUT && 
       p_trans->trans_speed == USB_TRANS_FS) {
      
      if(p_trans->err_inject.do_datapart_err  == TRUE) {
	// If a data part error should be
	// injected, change the data part to be
	// the next one.     
	switch(p_trans->datapart) {
        case USB_PART_ALL: {
	  p_trans->datapart = USB_PART_BEGIN;
        }
	  break;
        case USB_PART_BEGIN: {
	  p_trans->datapart = USB_PART_MID;
        }
	  break;
        case USB_PART_END: {
	  p_trans->datapart = USB_PART_END;
        }
	  break;
        case USB_PART_MID: {
	  p_trans->datapart = USB_PART_ALL;
        }
	  break;        
	}
      }
      switch(p_trans->datapart) {
      
        case USB_PART_ALL: {
          split_s  = 1;
          ssplit_e = 1;
        }
        break;

        case USB_PART_BEGIN: {
          split_s  = 1;
          ssplit_e = 0;
        }
        break;
 
        case USB_PART_END: {
          split_s  = 0;
          ssplit_e = 1;
        }
        break;

        case USB_PART_MID: {
          split_s  = 0;
          ssplit_e = 0;
        }
        break;        
      }
    }

    MSG_Printf("+       <time: %.10lld>:  SSPLIT <Hub_addr: 0x%x SC: 0x%x Hub_port: 0x%x S: 0x%x E: 0x%x ET: 0x%x>\n",
               PLAT_GetSimTime(),hub_addr, start_comp, hub_port, split_s, ssplit_e, ept_type);        
     

    // Start building SSPLIT token packet
    split_token = ssplit_e << 7;
  }
    
  else if(p_trans->trans_type == USB_TRANS_TYPE_CSPLIT) {
            // CSPLIT token packet
    start_comp = 1;
    csplit_u   = 0;

    MSG_Printf("+       <time: %.10lld>:  CSPLIT <Hub_addr: 0x%x SC: 0x%x Hub_port: 0x%x S: 0x%x U: 0x%x ET: 0x%x>\n",
               PLAT_GetSimTime(), hub_addr, start_comp, hub_port, split_s, csplit_u, ept_type);        

    // Build CSPLIT token packet
    split_token = csplit_u << 7;
  }
  else {
    MSG_Error( "In USB_PE_BuildSplit: Expected  p_trans->trans_type to be either USB_TRANS_TYPE_CSPLIT or USB_TRANS_TYPE_SSPLIT.\n");
  }

  // Start building token packet
  split_token |= (split_pid << 24) | (hub_addr << 17) | (start_comp << 16) | (hub_port << 9) |
    (split_s << 8) |  (ept_type << 5);

  return (split_token);
}

//%FS==================================================================
//
// USB_PE_BuildTransFromPkt
//
// Description:
//   USB_PE_BuildTransFromPkt changes member "p_data_segment" and 
//   "pid" of the struct USB_Trans_t.
//
// Parameter:
//   USB_Trans_t*  po_trans  IO
//     It points to the struct that shall be transferd. 
//
// Return value:
//   void
//
//%FE==================================================================
void USB_PE_BuildTransFromPkt(USB_Trans_t* po_trans) {
         
  u_int8      usb_recvd_pid = 0;
  USB_Pid_e   usb_handshake;
  USB_Data_t  data_t;

  // Get PID
  usb_recvd_pid   = *(po_trans->p_data_segment->p_data);

  if((usb_recvd_pid & 0xf) != (~(usb_recvd_pid >> 4) & 0xf))
    MSG_Error("Received PID is corrupted!  0x%x received.  The 0x%x should match the inverted 0x%x (0x%x)\n",
              usb_recvd_pid, usb_recvd_pid & 0xf, (usb_recvd_pid >> 4) & 0xf,~(usb_recvd_pid >> 4) & 0xf);

  // Clear the 4 MSBs that hold the inverted PID bits
  usb_recvd_pid  &= 0x0F;

  usb_handshake = (USB_Pid_e)usb_recvd_pid;

  // Put data into USB_Trans_t
  data_t.p_data = po_trans->p_data_segment->p_data;
  data_t.size   = po_trans->p_data_segment->size;
  
  USB_PE_MsgHandshake(data_t.p_data, usb_handshake, data_t.size);

  // Put PID into USB_Trans_t
  po_trans->pid = usb_handshake;    
}


//%FS==================================================================
//
// USB_PE_BuildTransFromPkt
//
// Description:
//   Build a PID byte from a PID enum
//
// Parameter:
//   USB_Pid_e    pid  PID as enum
//
// Return value:
//   u_int8  PID byte consisting of 4 bit packet type and 4 bit check field
//
//%FE======================================================================
u_int8 USB_BuildPid( USB_Pid_e pid ) {  
  u_int8  pid_byte;

  pid_byte = ((~pid) << 4 ) | pid;
  
  return pid_byte;
}



//%FS==================================================================
//
// USB_PE_HandleProtocolError
//
// Description:
//   Handle a protocol error
//
// Parameter:
//   USB_Trans_t*  p_trans       IO
//     It points to the struct that shall be transfered.
//   USB_HsBusRec_t*  p_hsx      I 
//     It points to the struct that is used to record the returned
//     information from sim-side.  
//   USB_CommandRec_t*  p_hccmd  I
//     It points to the struct that is used to control the process, 
//   USB_BusTransType_e bus_trans_type  I
//     Bus Transfer Tye
//
// Return value:
//   none
//
//%FE======================================================================

void USB_PE_HandleProtocolError( USB_Trans_t* p_trans, USB_HsBusRec_t* p_hsx, USB_CommandRec_t* p_hccmd,
         USB_BusTransType_e bus_trans_type ) {
    u_int32   err_count;
    USB_HostCtrlTop_t*  p_top;

    p_top = (USB_HostCtrlTop_t*)XPORT_GetMyProjectData();

    USB_PE_IncError(p_trans);    
    err_count = p_trans->p_pipe->sys_record.err_cnt;

    switch ( bus_trans_type ) {
    case  USB_DoBcintI:
    case  USB_DoBcintO:
    case  USB_DoHsBcO:
    case  USB_DoBcIss:
    case  USB_DoBcOss:      
      if(err_count < 3) {
        switch ( bus_trans_type ) {
        case  USB_DoBcintI:     
        case  USB_DoBcintO:
          if(p_hsx->did_timeout) {
            USB_PE_RespondHc(p_trans, USB_DO_SAME_CMD_TIMEOUT);
          }          
          else {
            USB_PE_RespondHc(p_trans, USB_DO_SAME_CMD);
          }
          break;
          
        case  USB_DoHsBcO:
          USB_PE_RespondHc(p_trans, USB_DO_PING);
          break;
          
        case  USB_DoBcIss:
        case  USB_DoBcOss:
          USB_PE_RespondHc(p_trans, USB_DO_START);
          break;
        default:
          break;
        }
      }
      else {        
        if(p_trans->p_pipe->p_endpt_descr->endpt_type == USB_EPTYPE_CTRL && 
           p_trans->p_pipe->p_endpt_descr->endpt_num == 0) {
          // This is a transaction by the default Control Pipe
          
          // Abort the current IRP  
          p_trans->p_irp->status = USB_IRP_STAT_ABORT;
          p_trans->p_pipe->sys_record.err_cnt = 0;
          if(!p_top->ErrorMsgDisabled.transaction_aborted)
            MSG_Error("Aborting the IRP!\n");

          USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
        }
        else {
          USB_PE_RespondHc(p_trans, USB_DO_HALT);
        }
      }
      break;

    case USB_DoIsoIcs:
      if(err_count < 3) {
        USB_PE_RespondHc(p_trans, USB_DO_COMP_IMMED_NOW); 
      }
      else {  
        USB_PE_RecordError(p_hsx, p_hccmd);
        USB_PE_RespondHc(p_trans, USB_DO_NEXT_CMD);
      }
      break;

    case USB_DoIntIcs:
    case USB_DoIntOcs:
      if(err_count < 3) {
        USB_PE_RespondHc(p_trans, USB_DO_START);
      }
      else {
        USB_PE_RespondHc(p_trans, USB_DO_HALT);
      }
      break;

    case USB_DoBcIcs:
    case USB_DoBcOcs:
      if(err_count < 3) {
  USB_PE_RespondHc(p_trans, USB_DO_COMPLETE_IMMED);
      }
      else {
  USB_PE_RespondHc(p_trans, USB_DO_HALT);
      }

    default:
      break;
    }

    return;
}


//%FS==================================================================
//
// USB_PE_Data01Toggle
//
// Description:
//   Look at two PIDs and determine, whether they form a
//   DATA0, DATA1 or DATA1, Data0 pair
//
// Parameter:
//   
//
// Return value:
//   u_int8  PID byte consisting of 4 bit packet type and 4 bit check field
//
//%FE======================================================================

 BOOL USB_PE_Data01Toggle( u_int8 pid_a, u_int8 pid_b ) {
   BOOL  does_toggle;

   // DATA0 = 0011B
   // DATA1 = 1011B
   // We do have a toggle, if the 3 LSB of both PIDs are 011B and the MSB is different

   does_toggle = ( (pid_a & 7) == 3 && (pid_a & 7) == 3 && ( (pid_a ^ pid_b) == 8 ) ) ? TRUE : FALSE;
   
   return does_toggle;
 }
