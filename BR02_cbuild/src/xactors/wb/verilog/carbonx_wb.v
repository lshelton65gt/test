/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

`timescale 1ns/1ns


module carbonx_wb(
		  clk,
		  reset,
		  wb_addr_o, 
		  wb_data_i,
		  wb_data_o,
		  wb_ack_i,
		  wb_we_o,
		  wb_stb_o,
		  wb_cyc_o,
                  wb_sel_o,
		  inta_i,
		  intb_i,
		  dma_req_i,
		  dma_ack_o,
		  susp_i,
		  resume_req_o
		  );
   
   parameter	ADDR_SIZE = 32;
   parameter	DATA_SIZE = 32;
   
   // input/output list
   input 	clk,
		reset,
		wb_ack_i,
		inta_i,
		intb_i,
		susp_i;
   input [DATA_SIZE-1:0] wb_data_i;
   input [15:0]          dma_req_i;
   
   output [ADDR_SIZE-1:0] wb_addr_o;
   output [DATA_SIZE-1:0] wb_data_o;
   output                 wb_we_o,
		          wb_stb_o,
		          wb_cyc_o,
		          resume_req_o;
   output [3:0]           wb_sel_o;
   output [15:0]          dma_ack_o;
   

`protect
// carbon license crbn_vsp_exec_xtor_wb

   // We need this handful of parameters to properly configure the transactor interface
   parameter              BFMid     =  1,
	                  ClockId   =  1,
	                  DataWidth = DATA_SIZE,
	                  DataAdWid = 10,		// Max is 15
	                  GCSwid    = 32,
	                  PCSwid    = 128;
   
   
   //
   // Include C interface file
   //
`include "xif_core.vh"
   
   assign                 BFM_clock = clk;
   
   // internal registers and wires
   reg                    cpu_wr,
	                  cpu_rd,
	                  xrun,
	                  xlast;
   
   
   reg [DataAdWid-1:0]    gp_daddr;
   
   reg [ADDR_SIZE-1:0]    wb_addr_o;
   reg                    wb_cyc_o;  
   reg [31:0]             bcount;
   
   wire                   rst;
   
   // continuous assignments
   
   // once we support DMA and suspend/resume the following
   // assignments signals will have to be changed:
   assign                 dma_ack_o        = 16'h0;
   assign                 resume_req_o     = 1'b0; //XIF_get_csdata[0];
   
   
   assign                 wb_stb_o       = cpu_wr | cpu_rd;
   assign                 wb_we_o        = cpu_wr;
   assign                 BFM_reset      = reset;
   assign                 rst            = BFM_reset;
   
   assign                 wb_data_o        = XIF_get_data;	// Write data from test
   assign                 wb_sel_o = (XIF_get_size == 1) ? 4'h1 : 4'hF; // Write data byte selects, only 8-bit or 32-bit for now
   assign                 BFM_put_operation = BFM_NXTREQ;  // Always getting next request
   assign                 BFM_put_data   = wb_data_i;      // Read data to test
   assign                 BFM_put_status = 0;		// Status always OK
   assign                 BFM_put_size   = XIF_get_size;	// Size always what was asked
   
   // These have to be combinatorial off of wb_ack_i
   assign                 BFM_put_dwe    = cpu_rd & wb_ack_i;
   // Notice for cpu_wr, we are READing from the ram, and have to present
   // the address a cycle ahead of time
   assign                 BFM_gp_daddr   =  cpu_wr & wb_ack_i &  xrun ? gp_daddr + 1 :
			  gp_daddr;
   
   assign                 BFM_xrun = xrun | xlast & ~wb_ack_i;	// run is combinatorial off cpu ack
   assign                 BFM_put_csdata = 32'b0;
   
   always @(posedge clk) begin
      if (rst) begin
         cpu_wr      <=  0;
         cpu_rd      <=  0;
         wb_addr_o   <=  0;
         bcount      <=  0;
         gp_daddr    <=  0;
         xrun        <=  0;
         xlast       <=  0;
      end else begin
         // First cycle of a transaction
         if (XIF_xav) begin
	    wb_addr_o    <=  XIF_get_address;
	    bcount       <=  XIF_get_size;
	    gp_daddr     <=  0;
	    wb_cyc_o     <=  1;                // Mark beginning of bus cycle
            
	    if (XIF_get_operation == BFM_WRITE) begin
	       xrun        <=  XIF_get_size > 4;  // More than 1 xfer left
	       xlast       <=  1;		     // At least 1 xfer left
	       cpu_rd      <=  0;
	       cpu_wr      <=  1;
	    end else if (XIF_get_operation == BFM_READ)  begin
	       xrun        <=  XIF_get_size > 4;
	       xlast       <=  1;
	       cpu_rd      <=  1;
	       cpu_wr      <=  0;
	    end else begin
	       cpu_wr      <=  0;
	       cpu_rd      <=  0;
	       xrun        <=  0;
	       xlast       <=  0;
	       wb_cyc_o    <=  0;                // Mark end of bus cycle
	    end	  
	    // Last cycle -- actually we should always get xav
         end else if(wb_ack_i & xlast & ~xrun) begin
	    cpu_wr      <=  0;
	    cpu_rd      <=  0;
	    wb_addr_o   <=  0;
	    bcount      <=  0;
	    gp_daddr    <=  0;
	    xrun        <=  0;
	    xlast       <=  0;
	    wb_cyc_o    <=  0;                // Mark end of bus cycle
            
	    // Middle cycles
         end else if (wb_ack_i) begin
	    wb_addr_o <=  wb_addr_o + 4;
	    gp_daddr  <=  gp_daddr + 1;
	    bcount    <=  bcount - 4;
	    xrun      <=  bcount > 8;  // More than 1 xfer left
	    xlast     <=  bcount > 4;  // At least 1 xfer left
	    
         end
      end // else: !if(rst)
   end // always @ (posedge clk)
   
`endprotect
   
endmodule
