/***************************************************************************************
  Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#include <cstdio>
#include <cstring>
#include "CarbonXPcieCommonTypes.h"
#include "CarbonXPciePhyRx.h"
#include "CarbonXPciePhyLayer.h"
#include "PCIETransaction.h"
#include "shell/carbon_interface.h"

CarbonXPciePhyRx::CarbonXPciePhyRx(CarbonXPcieConfig& cfg, CarbonXPciePhyLayer *parent, CarbonXPcieIntfType intf) :
  mCfg(cfg),
  mScrambler(mCfg.mLinkWidth),
  mState(eIdle),
  mOsCycle(0),
  mCurrTs(mCfg.mLinkWidth),
  mParent(parent), mIntfType(intf),
  mLinkState(Detect_Quiet), mStatus(mCfg.mLinkWidth)
{
}

CarbonXPciePhyRx::~CarbonXPciePhyRx() {
}

void CarbonXPciePhyRx::step() {
  while(mPacket.hasPacket()) {
    
    // Check if we have a packet
    if(mPacket.isPacket()) {
      mPacket.extract(mPacketList);
      
      if(mCfg.mVerboseReceiverHigh) {
        UtOStream* ostr = carbonInterfaceGetCout();
        carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
        carbonInterfaceWriteStringToOStream(ostr, "  Info: Packet List:");
        carbonInterfaceOStreamEndl(ostr);
        mPacketList.printAllPackets();
      }
    }
    
    // If it is not a packet it's garbage, throw away
    else mPacket.discard();
  }         
}

void CarbonXPciePhyRx::putCycleData(CarbonXPcieCycleData& data)
{
  parse(data.mData, data.mElecIdle);
}

void CarbonXPciePhyRx::parse(CarbonXPcieSym* data, bool* elecIdle)
{
  
  // Descramble Data if scrambling is enabled
  if(mCfg.mScrambleEnable) {
    for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
      // Descramble when in packet or idle, but always run the scrambler so its state is up to date
      if(mState == eIdle || mState == eInPacket)
        data[i] = mScrambler(data[i], i);
      else
        mScrambler(data[i], i);
    }
  }

  if(!elecIdle[0]) {

    switch(mState) {
    case eIdle : case eInPacket :
      
      // Increment Idle Count
      if(mState == eIdle) ++mStatus.mConsecIdles;

      for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
        if(mState == eIdle) {
          // Check for Ordered sets
          if(data[0] == SYM_COM) {
            //info_str << "Found Ordered Set at " << sc_time_stamp() << endl;
            mState   = eInOs;
            mOsCycle = 1;
            mStatus.mConsecIdles = 0;
            //mPacket << data[0];
            break; // Only check first lane for ordered sets for performance reasons
          }
          else if(data[0] == SYM_SKP || data[0] == SYM_FTS || data[0] == SYM_IDL) {
            // This is an error, but just ignore it for now
            mState = eIdle;
            break; // Only check first lane for ordered sets for performance reasons
          }

          // There can be PAD symbols or Idle Data between packets
          else if(data[i] == SYM_PAD || data[i] == 0) {
            // Do nothing stay in Idle, don't save the data
            mState = eIdle;
          }
          else if(data[i] == SYM_STP || data[i] == SYM_SDP) {
            mState   = eInPacket;
            mStatus.mConsecIdles = 0;
            mPacket << data[i];
          }
        }
        
        else { // In Packet
          if(data[i] == SYM_END || data[i] == SYM_EDB) {
            mPacket << data[i];
            mState = eIdle;
            mPacket.mark();
          }
          else if(data[i].isData()) {
            mPacket << data[i];
          }
          else {
            // Does not expect to see anything but data or END or EDB during a packet, 
            // so this is an error, return to Idle state
            gotoIdle();
            mPacket.mark();
          }
        }
      }
      break;

    case eInOs :
      ++mOsCycle;
      if(data[0] == SYM_COM) {
        mState   = eIdle;
        mOsCycle = 0;
        //gotoIdle();
      }
      else if(mOsCycle == 2) {
        if(data[0] == SYM_SKP || data[0] == SYM_FTS || data[0] == SYM_IDL) {
          mExpSym = data[0];
        }
        else if( (data[0] == SYM_PAD) || data[0].isData()) {
        
          // Assume that this is a TS1 or TS2
          mState   = eInTSOs;
          
          // Save all lanes for Link Number and Lane number in a TS
	  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++)
	    mCurrTs.setLinkNum(i, (data[0] == SYM_PAD) ? 255 : data[i].mData);
        }
      }
      
      else if(data[0] != mExpSym) {
        mState   = eIdle;
        mOsCycle = 0;
      }
        
      else if(mOsCycle == 4) {
        if(mExpSym == SYM_SKP) {
          
          if(mCfg.mVerboseReceiverMedium) {
            UtOStream* ostr = carbonInterfaceGetCout();
            carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
            carbonInterfaceWriteStringToOStream(ostr, "  Info: Received transaction: Clock Tolerance Compensation");
            carbonInterfaceOStreamEndl(ostr);
          }
          
          mStatus.rcvdOS(eSkipOS);
          mParent->rcvdSkipOs();
        }
        else if(mExpSym == SYM_IDL) {
          
          if(mCfg.mVerboseReceiverMedium) {
            UtOStream* ostr = carbonInterfaceGetCout();
            carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
            carbonInterfaceWriteStringToOStream(ostr, "  Info: Received Transaction: Electrical Idle Sequence. (Power save currently not supported, ignored.)");
            carbonInterfaceOStreamEndl(ostr);
          }
          
          mStatus.rcvdOS(eEIdleOS);
          mParent->rcvdEIdleOs();
        }
        else if(mExpSym == SYM_FTS) {
          
          if(mCfg.mVerboseReceiverMedium) {
            UtOStream* ostr = carbonInterfaceGetCout();
            carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
            carbonInterfaceWriteStringToOStream(ostr, "  Info: Received transaction: Fast Training Sequence");
            carbonInterfaceOStreamEndl(ostr);
          }
          
          mStatus.rcvdOS(eFtsOS);
          mParent->rcvdFtsOs();
        }
        mState   = eIdle;
        mOsCycle = 0;
      }
      break;
  
  
    case eInTSOs :
      ++mOsCycle;

      switch(mOsCycle) {
      case 3 : // We shouldn't be here in cycle 1 and 2
        
        // If not PAD and not Data, TS is corrupt, go back to idle state
        if(data[0] != SYM_PAD && !data[0].isData() ) {
          gotoIdle();
	}
        else {
          // Save all lanes in a TS
	  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++)
	    mCurrTs.setLaneNum(i, (data[0] == SYM_PAD) ? 255 : data[i].mData);
	  
        }
	break;
      case 4 :
	// For NTS any data value is legal, just check that it's a data value
        if(!data[0].isData()) {
          gotoIdle();
        }
        else {
          // Save all lanes in a TS
	  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++)
	    mCurrTs.setNFts(i, data[i].mData);
        }
          
        break;
      case 5 :
        // For Data Rate, just check that it is a data value
        if(!data[0].isData()) {
          gotoIdle();
        }
        else {
          // Save all lanes in a TS
	  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++)
	    mCurrTs.setDataRate(i, data[i].mData);
        }
	break;
      case 6 :
        // For Training Control bits, just check that it is a data value
        if(!data[0].isData()) {
          gotoIdle();
        }
        else {
          // Save all lanes in a TS
	  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++)
	    mCurrTs.setCtrl(i, data[i].mData);
        }
	break;
      case 7 :
	if(data[0] == 0x4A || data[0] == 0x45) {
          mExpSym = data[0]; 
	  if(data[0] == 0x4A) mCurrTs.setTs1();
	  else mCurrTs.setTs2();
	}
        // Corrupt TS, go back to idle state
	else {
          gotoIdle();
	}
	break;
      case  8 : case 9 : case 10 : case 11 : case 12 : 
      case 13 :case 14 : case 15 : case 16 :
        // Check that it's either TS1 or TS2
        if(data[0] == mExpSym) {
          //mPacket << data[0];
          if(mOsCycle == 16) {
	    gotoIdle(); // This is the last cycle of the TS
	    mStatus.rcvdTS(mCurrTs);

	    // Check Scramble Bit to see if we should disable Scrambling
	    if(mCurrTs.getCtrl(0) & 0x8) {
	      mCfg.setScrambleEnable(false);
	    }
	    
	    if(mCfg.mVerboseReceiverMedium) {
              UtOStream* ostr = carbonInterfaceGetCout();
              carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
              carbonInterfaceWriteStringToOStream(ostr, "  Info: Received transaction: ");
              carbonInterfaceWriteStringToOStream(ostr, mCurrTs.isTs1() ? "TS1" : "TS2");
              carbonInterfaceWriteStringToOStream(ostr, " (at ");
              carbonInterfaceWriteStringToOStream(ostr, mCfg.mCurrTimeString.c_str());
              carbonInterfaceWriteStringToOStream(ostr, ")");
              carbonInterfaceOStreamEndl(ostr);
            }
	  }
        }
        else {
          gotoIdle();
	}
        break;

      default :
        // Shouldn't be here goto idle
        gotoIdle();
        break;
      }
      
      break; // case eInTSOs
      
    default :
      // Shouldn't be here goto idle
      mState   = eIdle;
      mOsCycle = 0;
      break;
    }
  }
  else {
    mState   = eIdle;
    mOsCycle = 0;
  }  
}

void CarbonXPciePhyRx::setLinkState(pcie_LTSM_state_t state) {
  mLinkState = state;
}

pcie_LTSM_state_t CarbonXPciePhyRx::getLinkState() const {
  return mLinkState;
}
bool CarbonXPciePhyRx::getPhysPacket(CarbonXPciePhyLayer::PhysPacket *pkt)
{
  if(!mPacketList.hasPacket()) return false;
  
  CarbonXList<CarbonXPcieSym> raw;
  mPacketList.extract(raw);
  
  CarbonXList<CarbonXPcieSym>::iterator iter = raw.begin();

  pkt->mType   = (*iter == SYM_SDP) ? CarbonXPciePhyLayer::eDLLP : CarbonXPciePhyLayer::eLLTP;
  pkt->mLength = raw.size();
  pkt->mData   = CARBON_ALLOC_VEC(UInt32, pkt->mLength);
  if(pkt->mType == CarbonXPciePhyLayer::eDLLP)
    pkt->mSym    = CARBON_ALLOC_VEC(bool, pkt->mLength);
  else pkt->mSym = NULL;

  for(UInt32 i = 0; i < pkt->mLength; i++, ++iter) {
    pkt->mData[i] = iter->mData;
    if(pkt->mType == CarbonXPciePhyLayer::eDLLP)
      pkt->mSym[i]  = iter->mCmd;
  }
  
  // Since we got a packet return true
  return true;
}

bool CarbonXPciePhyRx::getPhysPacket(CarbonXList<CarbonXPcieSym>* pkt)
{
  if(!mPacketList.hasPacket()) return false;
  return mPacketList.extract(*pkt);
}

void CarbonXPciePhyRx::reset() {
  mState                     = eIdle;
  mLinkState                 = Detect_Quiet;
  mStatus.mConsecOSs         = 0;
  mStatus.mConsecTSs         = 0;
  mStatus.mConsecIdles       = 0;
}

void CarbonXPciePhyRx::TSType::writeToOStream(UtOStream* os)
{
  carbonInterfaceWriteStringToOStream(os, mIsTs1 ? "TS1: " : "TS2: ");
  carbonInterfaceWriteUInt32ToOStream(os, mLinkNum[0]);
  carbonInterfaceWriteStringToOStream(os, " ");
  carbonInterfaceWriteUInt32ToOStream(os, mLaneNum[0]);
  carbonInterfaceWriteStringToOStream(os, " ");
  carbonInterfaceWriteUInt32ToOStream(os, mNFts[0]);
  carbonInterfaceWriteStringToOStream(os, " ");
  carbonInterfaceWriteUInt32ToOStream(os, mDataRate[0]);
  carbonInterfaceWriteStringToOStream(os, " ");
  carbonInterfaceWriteUInt32ToOStream(os, mCtrl[0]);
  carbonInterfaceWriteStringToOStream(os, " ");
}
