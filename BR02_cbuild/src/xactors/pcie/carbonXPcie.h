// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

/**********************************************************
 * C Callable API Functions for the PCI Express Transactor
 *********************************************************/

#ifndef carbonXPcie__H
#define carbonXPcie__H

#include "carbon/carbon_shelltypes.h"

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
    \brief Macro to allow both C and C++ compilers to use this include file in
    a type-safe manner.
    \hideinitializer
  */
#define STRUCT struct
#endif

  /*!
    \defgroup PcieIF PCI Express C-Interface
    \brief C-Linkable Interface to the PCI Express Transactor

  */

  /*!
    \addtogroup PcieIF
    @{
  */

  //! Supported interface types
  /*!
    The transactor currently supports two types of interfaces.  One takes 10-bit encoded and
    scrambled data as it should look just before going to the serializers, \a eCarbonXPcie10BitIntf.
    The other follows the Intel PIPE Specification interface, \a eCarbonXPciePipeIntf.
  */
  typedef enum {
    eCarbonXPcie10BitIntf,  //!< 10-bit data interface
    eCarbonXPciePipeIntf    //!< Phy Interface for PCI-Express (Intel PIPE interface)
  } CarbonXPcieIntfType;


  //! Reasons for callback function being called
  typedef enum {
    eCarbonXPcieReturnData,    //!< Received return data from a transaction
    eCarbonXPcieReceivedTrans  //!< Received transaction from DUT
  } CarbonXPcieCallbackType;


  //! Signal type definitions for connecting Nets
  /*!
    Not all signal types are available for all interface types.  The \a eCarbonXPcie10BitIntf
    interface uses the \a eCarbonXPcieClock, \a eCarbonXPcieRxData, \a eCarbonXPcieRxElecIdle, \a eCarbonXPcieTxData, and
    \a eCarbonXPcieTxElecIdle types.  The \a eCarbonXPciePipeIntf interface uses the \a eCarbonXPcieClock, \a eCarbonXPciePhyStatus,
    \a eCarbonXPciePowerDown, \a eCarbonXPcieResetN, \a eCarbonXPcieRxData, \a eCarbonXPcieCarbonXPcieRxDataK, \a eCarbonXPcieRxElecIdle, \a eCarbonXPcieRxPolarity,
    \a eCarbonXPcieRxStatus, \a eCarbonXPcieRxValid, \a eCarbonXPcieTxCompliance, \a eCarbonXPcieTxData, \a eCarbonXPcieCarbonXPcieTxDataK, \a eCarbonXPcieTxDetectRx,
    and \a eCarbonXPcieTxElecIdle.

    All signal types, with the exception of \a eCarbonXPcieClock, \a eCarbonXPciePhyStatus, \a eCarbonXPciePowerDown,
    \a eCarbonXPcieResetN, and \a eCarbonXPcieTxDetectRx, are connected with one instance per lane of the link.
    Therefore if the link width is 4, there would be four signals of type \a eCarbonXPcieTxData
    connected, with the indecies 0 through 3.
  */
  typedef enum {
    eCarbonXPcieClock,        //!< Interface clock
    eCarbonXPciePhyStatus,    //!< DUT input PHY status
    eCarbonXPciePowerDown,    //!< DUT output power down state
    eCarbonXPcieResetN,       //!< DUT output active-low reset
    eCarbonXPcieRxData,       //!< DUT input receiver data
    eCarbonXPcieRxDataK,      //!< DUT input receiver symbol flag
    eCarbonXPcieRxElecIdle,   //!< DUT input receiver electrical idle
    eCarbonXPcieRxPolarity,   //!< DUT output receiver polarity inversion
    eCarbonXPcieRxStatus,     //!< DUT input receiver status
    eCarbonXPcieRxValid,      //!< DUT input receiver valid
    eCarbonXPcieTxCompliance, //!< DUT output transmitter compliance pattern
    eCarbonXPcieTxData,       //!< DUT output transmitter data
    eCarbonXPcieTxDataK,      //!< DUT output transmitter symbol flag
    eCarbonXPcieTxDetectRx,   //!< DUT output transmitter detects receiver or begin loopback
    eCarbonXPcieTxElecIdle    //!< DUT output transmitter electrical idle
  } CarbonXPcieSignalType;
  

   //!  Types of transactions available in the PCI-Express Base Specification Rev 1.0a.
   /*!
     The numeric values are 7-bits wide, corresponding to the concatination
     of the 2-bit format and 5-bit type.
   */
   typedef enum {
      eCarbonXPcieMRd32 = 0x00,   //!< Memory Read Request, 32-bit address
      eCarbonXPcieMRd64 = 0x20,   //!< Memory Read Request, 64-bit address
      eCarbonXPcieMRdLk32 = 0x01, //!< Memory Read Request - Locked, 32-bit address
      eCarbonXPcieMRdLk64 = 0x21, //!< Memory Read Request - Locked, 64-bit address
      eCarbonXPcieMWr32 = 0x40,   //!< Memory Write Request, 32-bit address
      eCarbonXPcieMWr64 = 0x60,   //!< Memory Write Request, 64-bit address
      eCarbonXPcieIORd = 0x02,    //!< I/O Read Request
      eCarbonXPcieIOWr = 0x42,    //!< I/O Write Request
      eCarbonXPcieCfgRd0 = 0x04,  //!< Configuration Read Type 0
      eCarbonXPcieCfgWr0 = 0x44,  //!< Configuration Write Type 0
      eCarbonXPcieCfgRd1 = 0x05,  //!< Configuration Read Type 1
      eCarbonXPcieCfgWr1 = 0x45,  //!< Configuration Write Type 1
      eCarbonXPcieMsg = 0x30,     //!< Message Request
      eCarbonXPcieMsgD = 0x70,    //!< Message Request with data payload
      eCarbonXPcieCpl = 0x0A,     //!< Completion without Data
      eCarbonXPcieCplD = 0x4A,    //!< Completion with Data
      eCarbonXPcieCplLk = 0x0B,   //!< Completion for Locked Memory Read without Data
      eCarbonXPcieCplDLk = 0x4B,  //!< Completion for Locked Memory Read with Data
      eCarbonXPcieUnknown = 0xFF  //!< Unknown Command Type
   } CarbonXPcieCmdType;


  /*!
    \brief CarbonXPcie reference structure.

  */
  typedef STRUCT CarbonXPcieTransactor CarbonXPcieID;

  /*!
    \brief CarbonXPcieNet reference structure.

  */
  typedef STRUCT CarbonXPcieNet CarbonXPcieNetID;

  /*!
    \brief CarbonXPcieTLP reference structure.

  */
  STRUCT CarbonXPcieTLP;

  //! Callback function to indicate that the transactor is done processing transactions.
  /*!
    This function will be called, if set, each time the transactor is called  when the 
    transactor has no transactions to process, either on the transmitter or receiver.
    
    \param info Any information the user would like passed back to the callback function.
  */
  typedef void (CarbonXPcieDoneCBFn)(void *info);

  //! Callback function to indicate that the transactor has received a transaction from the DUT.
  /*!
    This function will be called each time the transactor receives a transaction (TLP packet)
    from the DUT.  The \a type will specify if the reason for calling is that read data is
    being returned or that a new transaction was received.
    
    \param pcie The calling transactor.
    \param type The reason for the function being called.
    \param info Any information the user would like passed back to the callback function.
  */
  typedef void (CarbonXPcieTransCBFn)(CarbonXPcieID *pcie, CarbonXPcieCallbackType type, void *info);
  
  //! Callback function that gets call just before starting sending a TLP
  /*!
    \param caller Can be used to pass a pointer to the calling class to the callback functiion
    \param cmd pointer to the CarbonXPcieCmd corresponding to the TLP to get sent
  */
  typedef void (CarbonXPcieStartTransCBFn)(CarbonXPcieID *pcie, void *caller, CarbonXPcieTLP *cmd);
  
  //! Callback function that gets call just after starting sending a TLP
  /*!
    \param caller Can be used to pass a pointer to the calling class to the callback functiion
    \param cmd pointer to the CarbonXPcieCmd corresponding to the TLP that just got sent
  */
  typedef CarbonXPcieStartTransCBFn CarbonXPcieEndTransCBFn;

  // Callback function that gets called when transactor changes data rate
  /*!
    \param caller Can be used to pass a pointer to the calling class to the callback functiion
    \param rate New Rate the transactor is changing to. A 2 indicates Gen2 a 1 Indicates Gen 1
  */
  typedef void (CarbonXPcieRateChangeCBFn)(void *caller, CarbonUInt32 rate);

  // Callback function that gets call just before starting sending a TLP
  /*!
    \param net       Pointer to the Net Object
    \param userData  Pointer an arbitrary user data structure
  */
  typedef void (CarbonXPcieNetChangeCBFn)(CarbonXPcieNetID *net, CarbonClientData userData);
  
  //! Create an instance of the PCIE transactor.
  /*!
    \param intf Interface type. eCarbonXPcie10BitIntf or eCarbonXPciePipeIntf
    \param link_width Number of lanes on the interface (1, 2, 4, 8, 12, 16 or 32)
    \param print_name Name of transactor used in messages.
  */
  CarbonXPcieID * carbonXPcieCreate(CarbonXPcieIntfType intf, 
				    CarbonUInt32        link_width,
				    const char *        print_name);
  
  //! Destroy the CarbonXPcieID object
  /*!
    \param xtor Pointer to the transactor handle to be destroyed.
  */
  void carbonXPcieDestroy(CarbonXPcieID * xtor);

  //! Get a Net object based on signal type and lane index
  /*!
    \param xtor Pointer to trasactor object
    \param sigType Signal type of object to get.
    \param index Lane index of object to get. If there is only one signal given type for the interface index should be set to 0.
  */
  CarbonXPcieNetID * carbonXPcieGetNet(CarbonXPcieID *       xtor, 
				       CarbonXPcieSignalType sigType, 
				       CarbonUInt32          index);

  //! Resets the state of the transactor
  /* This call will only reset the transactors statemachines and clear
     the transaction queues. Parameters will keep their values.
  */
  void carbonXPcieReset(CarbonXPcieID * xtor);

  //! Set the timescale for the transactor
  /*!
    Sets the time period of each tick as specified by carbonXPcieStep
    this is used so that debug messages would report time that is in
    sync with the rest of the environment.

    \param xtor Pointer to transactor object.
    \param timescale Timescale for 1 tick
  */
  void carbonXPcieSetTimescale(CarbonXPcieID * xtor, CarbonTimescale timescale);

  //! Set message printing level for transactor.
  /*!
    The available levels are:
    - 3+ : High, print copious amounts of data about every cycle.
    - 2  : Medium, print large amount of information during all transactions.
    - 1  : Low, print basic informational messages during simulations.
    - 0  : Off, print no message except for a few configuration error messages.

    The default value is '1' when the transactor is instanciated.

    \param xtor Pointer to transactor object.
    \param level The print level to be used.
  */
  void   carbonXPcieSetPrintLevel(CarbonXPcieID *xtor, CarbonUInt32 level);

  //! Get message printing level for transactor.
  /*!
    \retval 3 If the message printing is set to High.
    \retval 2 If the message printing is set to Medium.
    \retval 1 If the message printing is set to Low.
    \retval 0 If the message printing is set to Off.
  */
  CarbonUInt32 carbonXPcieGetPrintLevel(CarbonXPcieID *xtor);

  //! Set PCI style Bus Number for config transactions
  /*!
    \param xtor Pointer to transactor object.
    \param num Bus number
  */
  void   carbonXPcieSetBusNum(CarbonXPcieID *xtor, CarbonUInt32 num);

  //! Get PCI style Bus Number for config transactions
  /*!
    \param xtor Pointer to transactor object.
  */
  CarbonUInt32 carbonXPcieGetBusNum(CarbonXPcieID *xtor);

  //! Set PCI style Device Number for config transactions
  /*!
    \param xtor Pointer to transactor object.
    \param num Device number
  */
  void   carbonXPcieSetDevNum(CarbonXPcieID *xtor, CarbonUInt32 num);

  //! Get PCI style Device Number for config transactions
  /*!
    \param xtor Pointer to transactor object.
  */
  CarbonUInt32 carbonXPcieGetDevNum(CarbonXPcieID *xtor);

  //! Set PCI style Function Number for config transactions
  /*!
    \param xtor Pointer to transactor object.
    \param num Function number
  */
  void   carbonXPcieSetFuncNum(CarbonXPcieID *xtor, CarbonUInt32 num);

  //! Get PCI style Function Number for config transactions
  /*!
    \param xtor Pointer to transactor object.
  */
  CarbonUInt32 carbonXPcieGetFuncNum(CarbonXPcieID *xtor);

  //! Sets whether data scrambling is enabled or disabled.
  /*!
    This option will effect both the transmitter and receiver functionality, ie. they must
    both or neither be sending scrambled data across the link.  This function should be called
    before the transactor starts training the link, and if it is called during the simulation
    then the transactor will need to retrain the link before it takes effect.

    \param xtor Pointer to transactor object.
    \param tf The value TRUE will enable data scrambling while FALSE will disable it.
  */
  void  carbonXPcieSetScrambleStatus(CarbonXPcieID *xtor, CarbonUInt32 tf);
  //! Gets whether data scrambling is enabled or disabled.
  /*!
    This function will return the latest value set with setScrambleStatus().

    \param xtor Pointer to transactor object.
    \retval true If data scrambling is enabled.
    \retval false If data scrambling is disabled.
  */
  CarbonUInt32 carbonXPcieGetScrambleStatus(CarbonXPcieID *xtor);
  //! Sets the maximum number of in-flight transactions
  /*!
    This option will set the maximum number of transactions from the transactor that can be
    in-flight at one time.  In-flight means that it is activly being transmitted, waiting for
    an acknowledgement, or waiting for a completion message.

    Once this limit is reached, all transactions will be held in the queue and idle will be
    transmitted on the bus until one or more of the in-flight transactions completes.

    The PCI-Express 1.0a specification states that the maximum number of in-flight transactions
    is 32.  If this function sets a value of 0 or more than 32, the transactor will respect the
    limit of 32 in the specification.  Any value between those two points will be used as is.

    \param xtor Pointer to transactor object.
    \param max The maximum number of in-flight transactions.
  */
  void   carbonXPcieSetMaxTransInFlight(CarbonXPcieID *xtor, CarbonUInt32 max);
  //! Gets the maximum number of in-flight transactions
  /*!
    This option will return the maximum number of in-flight transactions that is allowed by
    the transactor.  This value is set by setMaxTransInFlight() and the default value is 32.

    \param xtor Pointer to transactor object.
    \return The maximum number of in-flight transactions.
  */
  CarbonUInt32 carbonXPcieGetMaxTransInFlight(CarbonXPcieID *xtor);

  //! Start link training
  /*!
    Start training the link after time \a tick if the link is currently in the electrical
    idle state.  If the link is not in electrical idle then this function will have no effect.
    The link will immediately transition from electrical idle to logical idle, stay in that
    state for time \a tick, and then start training.

    The value \a tick is in the same unit used for the step method.

    \param xtor Pointer to transactor object.
    \param tick The length of time to spend in logical idle before training starts.
    \retval true If the transactor will start link training.
    \retval false If the transactor will not start link training.
  */
  CarbonUInt32 carbonXPcieStartTraining(CarbonXPcieID *xtor, CarbonUInt64 tick);

  //! Enable or disable link training bypass
  /*!
    Enabling the link training bypass will cause the transactor to go from an electrical
    idle to logical idle state with all lanes of the link trained.  The transactor will
    go into the flow control initialization state next.

    Disabling the link training bypass will cause the transactor to go through the full
    link training sequence when it is next necessary, upon transitioning either from the
    electrical idle state or to the recovery state.

    By default the training bypass is disabled.

    \param xtor Pointer to transactor object.
    \param tf The value TRUE will bypass the link training while FALSE will disable the bypass.
  */
  void   carbonXPcieSetTrainingBypass(CarbonXPcieID *xtor, CarbonUInt32 tf);

  //! Get the current state of the training bypass feature.
  /*!
    Returns whether the transactor has the link training bypass enabled or disabled.

    \param xtor Pointer to transactor object.
    \retval true If the link training is bypassed.
    \retval false If the link training is not bypassed.
  */
  CarbonUInt32 carbonXPcieGetTrainingBypass(CarbonXPcieID *xtor);

  //! Sets the maximum payload size for transmitted transactions
  /*!
    Sets the Max_Payload_Size parameter in the PCI-Express Device
    Control Register (Offset 0x8).  See the PCI-Express Base
    Specification Rev 1.0a, Section 7.8.4 for a description of this
    configuration register.

    \param xtor Pointer to transactor object.
    \param val The 3-bit value to be set.
  */
  void   carbonXPcieSetMaxPayloadSize(CarbonXPcieID *xtor, CarbonUInt32 val);

  //! Gets the maximum payload size for transmitted transactions
  /*!
    Gets the Max_Payload_Size parameter in the PCI-Express Device
    Control Register (Offset 0x8).  See the PCI-Express Base
    Specification Rev 1.0a, Section 7.8.4 for a description of this
    configuration register.

    \param xtor Pointer to transactor object.
    \return The 3-bit parameter value.
  */
  CarbonUInt32 carbonXPcieGetMaxPayloadSize(CarbonXPcieID *xtor);

  //! Sets the Read Completion Boundary for the transactor
  /*!
    \param xtor Pointer to transactor object.
    \param val Either 64 or 128 byte boundry.
  */
  void   carbonXPcieSetRCB(CarbonXPcieID *xtor, CarbonUInt32 val);

  //! Gets the Read Completion Boundary
  /*!
    \return 64 or 128 for RCB
  */
  CarbonUInt32 carbonXPcieGetRCB(CarbonXPcieID *xtor);

  //! Sets the Supported Speeds for the transactor
  /*!
    \param xtor Pointer to transactor object.
    \param val 1: Only Gen1 Speed Supported. 3: Gen1 and Gen2 Speeds are supported.
  */
  void   carbonXPcieSetSupportedSpeeds(CarbonXPcieID *xtor, CarbonUInt32 val);

  //! Gets the supported speeds
  /*!
    \param xtor Pointer to transactor object.
    \return SupportedSpeeds
  */
  CarbonUInt32 carbonXPcieGetSupportedSpeeds(CarbonXPcieID *xtor);

  //! Gets the current Speed the transactor is running
  /*!
    \param xtor Pointer to transactor object.
    \retval 1 Transactor is running in Gen1 Speed
    \retval 2 Transactor is running in Gen2 Speed
  */
  CarbonUInt32 carbonXPcieGetCurrentSpeed(CarbonXPcieID *xtor);

  //! Forces The Electrical Idle Input (Tx) to be inactive
  /*!
    \param xtor Pointer to transactor object.
    \param inactive A 1 sets the input inactive, a 0 sets the input to not be forces inactive
  */
  void   carbonXPcieSetForceElecIdleInactive(CarbonXPcieID *xtor, CarbonUInt32 inactive);

  //! Gets the Setting for Force Electrical Idle Inactive
  /*!
    \param xtor Pointer to transactor object.
    \return 1 If Electrical Idle is forced inactive, 0, if not forced inactive.
  */
  CarbonUInt32 carbonXPcieGetForceElecIdleInactive(CarbonXPcieID *xtor);

  //! Sets the callback function for received transactions and return data
  /*!
    This function must be set so the transactor can notify the calling program when it
    receives a transaction from the DUT.  The \a transCbInfo parameter is passed to the
    callback exactly as it was passed to this function.

    \param xtor Pointer to transactor object.
    \param transCbFn The function to be called by the transactor.
    \param transCbInfo Any information the user would like passed back to the callback function.
  */

  void   carbonXPcieSetTransCallbackFn(CarbonXPcieID *xtor, CarbonXPcieTransCBFn *transCbFn, void* transCbInfo);

  //! Returns the callback function pointer called for received transactions and return data
  /*!
    This function passes back the function pointer that was passed in as the first parameter
    to the setTransCallbackFn() function.

    \param xtor Pointer to transactor object.

    \sa setTransCallbackFn(CarbonXPcieTransCBFn *transCbFn, void* transCbInfo)
  */
  CarbonXPcieTransCBFn* carbonXPcieGetTransCallbackFn(CarbonXPcieID *xtor);

  //! Returns the callback data pointer passed to the callback function for received transactions and return data
  /*!
    This function passes back the data pointer that was passed in as the second parameter
    to the setTransCallbackFn() function.

    \param xtor Pointer to transactor object.

    \sa setTransCallbackFn(CarbonXPcieID *xtor, CarbonXPcieTransCBFn *transCbFn, void* transCbInfo)
  */
  void*  carbonXPcieGetTransCallbackData(CarbonXPcieID *xtor);

  //! Sets the callback function for start of transaction
  /*!
    \param xtor Pointer to transactor object.
    \param caller Can be used to pass a pointer to the calling class to the callback functiion
    \param startTransCbFn The function to be called by the transactor.
  */
  void   carbonXPcieSetStartTransCallbackFn(CarbonXPcieID *xtor, void *caller, CarbonXPcieStartTransCBFn *startTransCbFn);

  //! Returns the callback function pointer called for start of transmitting transaction 
  /*!
    This function passes back the function pointer that was passed in as parameter
    to the setStartTransCallbackFn() function.

    \sa setStartTransCallbackFn(CarbonXPcieID *xtor, CarbonXPcieStartTransCBFn *startTransCbFn)
  */
  CarbonXPcieStartTransCBFn* carbonXPcieGetStartTransCallbackFn(CarbonXPcieID *xtor);

  //! Sets the callback function for end of transaction
  /*!
    \param xtor Pointer to transactor object.
    \param caller Can be used to pass a pointer to the calling class to the callback functiion
    \param endTransCbFn The function to be called by the transactor.
  */
  void   carbonXPcieSetEndTransCallbackFn(CarbonXPcieID *xtor, void *caller, CarbonXPcieEndTransCBFn *endTransCbFn);

  //! Returns the callback function pointer called for end of transmitting transaction 
  /*!
    This function passes back the function pointer that was passed in as parameter
    to the setEndTransCallbackFn() function.

    \param xtor Pointer to transactor object.

    \sa setEndTransCallbackFn(CarbonXPcieID *xtor, CarbonXPcieEndTransCBFn *endTransCbFn)
  */
  CarbonXPcieEndTransCBFn* carbonXPcieGetEndTransCallbackFn(CarbonXPcieID *xtor);

  //! Sets the callback function for Rate Change
  /*!
    This function sets the callback function to be called when the transactor changes rate from
    Gen1 to Gen2 or from Gen2 to Gen2 Speeds.
    \param xtor Pointer to transactor object.
    \param caller Can be used to pass a pointer to the calling class to the callback functiion
    \param rateChangeCbFn The function to be called by the transactor.
  */
  void   carbonXPcieSetRateChangeCallbackFn(CarbonXPcieID *xtor, void *caller, CarbonXPcieRateChangeCBFn *rateChangeCbFn);

  //! Returns the callback function pointer called for rate change 
  /*!
    This function passes back the function pointer that was passed in as parameter
    to the setRateChangeCallbackFn() function.

    \param xtor Pointer to transactor object.

    \sa setRateChangeCallbackFn(CarbonXPcieID *xtor, CarbonXPcieRateChangeCBFn *rateChangeCbFn)
  */
  CarbonXPcieRateChangeCBFn* carbonXPcieGetRateChangeCallbackFn(CarbonXPcieID *xtor);

  //! Acknoledge data rate change
  /* Should be called by the external environment when it has swithed to call
     step with the new data rate. This method will only have affect when
     the checkDataRateChange() method returns true.

    \param xtor Pointer to transactor object.
  */
  void carbonXPcieAckDataRateChange(CarbonXPcieID *xtor);

  //! Gets a data return transaction in a CarbonXPcieTLP object.
  /*!
    The user must pass in a pointer to an allocated CarbonXPcieTLP object in which the transactor
    will populate the fields with the data from the received completion transaction.  If
    \a cmd is a null pointer that will be caught and an error message given.

    This command can be called as many times as necessary in order to get each of the stored
    CarbonXPcieTLP objects.

    \param xtor Pointer to transactor object.
    \param cmd A pointer to an allocated CarbonXPcieTLP object.
    \retval true A valid transaction was placed into \a cmd.
    \retval false An error occurred or there are no new completion transactions to be read.
  */
  CarbonUInt32 	  carbonXPcieGetReturnData(CarbonXPcieID *xtor, CarbonXPcieTLP *cmd);

  //! Gets the number of data return transactions which need to be processed.
  /*!
    Find the number of CarbonXPcieTLP objects in the queue of data return transactions (completions).

    \retval length Number of transactions in queue
  */
  CarbonUInt32  carbonXPcieGetReturnDataQueueLength(CarbonXPcieID *xtor);

  //! Gets a received transaction in a CarbonXPcieTLP object.
  /*!
    The user must pass in a pointer to an allocated CarbonXPcieTLP object in which the transactor
    will populate the fields with the data from the received completion transaction.  If
    \a cmd is a null pointer that will be caught and an error message given.

    This command can be called as many times as necessary in order to get each of the stored
    CarbonXPcieTLP objects.

    \param xtor Pointer to transactor object.
    \param cmd A pointer to an allocated CarbonXPcieTLP object.
    \retval true A valid transaction was placed into \a cmd.
    \retval false An error occurred or there are no new completion transactions to be read.
  */
  CarbonUInt32 carbonXPcieGetReceivedTrans(CarbonXPcieID *xtor, CarbonXPcieTLP *cmd);                     // Get CarbonXPcieTLP object with received transaction information

  //! Gets the number of received transactions which need to be processed.
  /*!
    Find the number of CarbonXPcieTLP objects in the queue of received transactions.

    \param xtor Pointer to transactor object.
    \retval length Number of transactions in queue
  */
  CarbonUInt32  carbonXPcieGetReceivedTransQueueLength(CarbonXPcieID *xtor);

  //! Gets the transactor's link width
  /*!
    This function gets the link width, or number of lanes in the link, as specified when the
    transactor was instantiated.  This value can not change after the PCIEXtor object is
    created.

    \return The link width used by the transactor.
  */
  CarbonUInt32 carbonXPcieGetLinkWidth(CarbonXPcieID *xtor);

  //! Gets the transactor's message print name.
  /*!
    This function gets the transactor's print name as specified when the transactor was
    instantiated.  This value can not change after the PCIEXtor object is created and is
    prepended to all messages printed from the transactor.

    \param xtor Pointer to transactor object.
    \return The c-style string used in all printing messages.
  */
  const char*  carbonXPcieGetPrintName(CarbonXPcieID *xtor);

  //! Gets the transactor's interface type.
  /*!
    This function gets the transactor's interface type as specified when the transactor was
    instantiated.  This value can not change after the PCIEXtor object is created.

    \return The \a CarbonXPcieIntfType interface type used by this transactor instance.
  */
  CarbonXPcieIntfType carbonXPcieGetInterfaceType(CarbonXPcieID *xtor);

  //! Sets the callback function for notifying that the transactor is finished.
  /*!
    This function may be set so the transactor can notify the calling program when it
    no longer has any transactions to process, either on the transmitter or receiver.
    The \a doneCbInfo parameter is passed to the callback exactly as it was passed to
    this function.

    This function will be called each time the transactor is called (rising edge of the
    signal connected with type \a eCarbonXPcieClock) as long as isDone() would return \a true.

    \param xtor Pointer to transactor object.
    \param doneCbFn The function to be called by the transactor.
    \param doneCbInfo Any information the user would like passed back to the callback function.
  */
  void   carbonXPcieSetDoneCallbackFn(CarbonXPcieID *xtor, CarbonXPcieDoneCBFn *doneCbFn, void* doneCbInfo);

  //! Returns the callback function pointer called for notification that the transactor is finished
  /*!
    This function passes back the function pointer that was passed in as the first parameter
    to the setDonCallbackFn() function.

    \param xtor Pointer to transactor object.

    \sa setDoneCallbackFn(CarbonXPcieID *xtor, CarbonXPcieDoneCBFn *doneCbFn, void* doneCbInfo)
  */
  CarbonXPcieDoneCBFn* carbonXPcieGetDoneCallbackFn(CarbonXPcieID *xtor);

  //! Returns the callback data pointer passed to the callback function for notification that the transactor is finished
  /*!
    This function passes back the data pointer that was passed in as the second parameter
    to the setDoneCallbackFn() function.

    \param xtor Pointer to transactor object.

    \sa setDoneCallbackFn(CarbonXPcieID *xtor, CarbonXPcieDoneCBFn *doneCbFn, void* doneCbInfo)
  */
  void*  carbonXPcieGetDoneCallbackData(CarbonXPcieID *xtor);

  //! Enqueue a new transaction
  /*!
    Information from the \a cmd object is used to create a new transaction, which is pushed
    onto the end of the queue.  The transation ID is returned upon success and is a unique
    identifier for this transaction.

    \param xtor Pointer to transactor object.
    \param cmd The information for a new transaction.
    \retval -1 There was an error and no new transaction was added.
    \retval 0+ The new transaction was successfully enqueued and the transaction ID was returned.
  */
  CarbonSInt32 carbonXPcieAddTransaction(CarbonXPcieID *xtor, const CarbonXPcieTLP *cmd);

  //!Sets the electrical idle nets as active high
  /*!
    This function determins whether the signals connected as \a eCarbonXPcieRxElecIdle and
    \a eCarbonXPcieTxElecIdle are active high or active low.  Both the transmitter and receiver
    will adhere to the calling of this function.  After this function is called, the
    specified signals will show that the receivers/transmitters are in the electrical
    idle state (powered down) if they have the value '1'.

    \param xtor Pointer to transactor object.

    \note This is only valid for the \a eCarbonXPcie10BitIntf interface.  The \a eCarbonXPciePipeIntf
    specifies that the signals be active high.
  */
  void   carbonXPcieSetElecIdleActiveHigh(CarbonXPcieID *xtor);

  //!Sets the electrical idle nets as active low
  /*!
    This function determins whether the signals connected as \a eCarbonXPcieRxElecIdle and
    \a eCarbonXPcieTxElecIdle are active high or active low.  Both the transmitter and receiver
    will adhere to the calling of this function.  After this function is called, the
    specified signals will show that the receivers/transmitters are in the electrical
    idle state(powered down) if they have the value '0'.

    \param xtor Pointer to transactor object.

    \note This is only valid for the \a eCarbonXPcie10BitIntf interface.  The \a eCarbonXPciePipeIntf
    specifies that the signals be active high.
  */
  void   carbonXPcieSetElecIdleActiveLow(CarbonXPcieID *xtor);

  //! Gets whether the transactor fully configured and ready for use.
  /*!
    This function should be called after all of the necessary configuration
    functions have been called to determine if the transactor is ready to
    interact with the DUT.  If this function returns \c true then the transactor
    will function when its Clock transitions.  If this function
    returns \c false then the transactor will not execute when its clock
    transitions and will print a warning message to the screen.

    To determine what caused this function to return \c false, call
    printConfiguration().

    \param xtor Pointer to transactor object.
    \retval true The transactor is fully configured.
    \retval false The transactor has one or more necessary connections open.

    \sa printConfiguration()
  */
  CarbonUInt32 carbonXPcieIsConfigured(CarbonXPcieID *xtor);

  //! Print out all configuration information.
  /*!
    This function can be called at any time to view the current configuration
    information.  A list of all DUT nets connected to transactor signals, the
    link width, etc., will be printed to STDOUT.

    For programability, the function isConfigured() is provided to return
    whether the transactor is fully configured or not.

    \param xtor Pointer to transactor object.

    \sa isConfigured()
  */
  void   carbonXPciePrintConfiguration(CarbonXPcieID *xtor);

  //! Gets whether the transactor is currently finished processing transactions.
  /*!
    \param xtor Pointer to transactor object.
    \retval true The transactor is done processing its transactions.
    \retval false The transactor has transactions to send out or is currently receiving
    a new transaction.
  */
  CarbonUInt32 carbonXPcieIsDone(CarbonXPcieID *xtor); 

  //! Determine if the link is in the active state.
  /*!
    This function will return whether the link has been trained and is ready for use
    or not.  A return value of \c true means that the link training state machine (LTSM)
    and data link layer state machine (DLLSM) have both completed training.

    \retval true The link is trained.
    \retval false The link has not been fully trained.

    \param xtor Pointer to transactor object.

    \sa printLinkState()
  */
  CarbonUInt32 carbonXPcieIsLinkActive(CarbonXPcieID *xtor);

  //! Print the current state of link training.
  /*!
    This function can be called at any time to view the current state of the link
    training while debugging.  The output will be printed to STDOUT for the current
    state of the link training state machine (LTSM) and data link layer state machine
    (DLLSM).

    For programability, the function isLinkActive() is provided to return whether the
    link is fully trained or not.

    \param xtor Pointer to transactor object.

    \sa isLinkActive()
  */
  void   carbonXPciePrintLinkState(CarbonXPcieID *xtor);

  //! Tell the transactor to run.
  /*!
    This function needs to be called when the pertinent clock changes value, at which
    time the transctor will process its stimulus and return any results.  If this
    transctor is used within an environment then this function will be called
    automatically when the transactor is bound to a Clock.

    \param xtor Pointer to transactor object.
    \param tick The current simulation time.
  */
  void   carbonXPcieStep(CarbonXPcieID *xtor, CarbonUInt64 tick);

   //! Add an idle transaction.
   /*!
     Submit a logical idle transaction that will last \c cycles clock cycles.  This
     transaction will be appended to the transaction queue and can be used to space
     out transactions or keep the transactor running while waiting for the DUT (and 
     not have isDone() return true or related callback called).

     \param xtor Pointer to transactor object.
     \param cycles The number of clock cycles to keep the transmitter in logical idle.
   */
  void   carbonXPcieIdle(CarbonXPcieID *xtor, CarbonUInt64 cycles);
  
  //! Setup Available Buffer Space, in the transactors receiver port
  /*! 
    The buffer sizes are used to advertize the available buffer space
    on the transactors receiving port to the opposite side's transmitting port
    as port of the flow control protocoll, the buffer sizes are specified in FCC
    values each FCC value requires 16 bytes of buffer. I.E an FCC value of 1 
    required 16 bytes of available buffer space. The default FCC value for all 
    buffer types are 0 or Infinate.
    
    \param xtor Pointer to transactor object.
    \param vc The virtual channel to setup buffer space for, currently only VC0 is supported.
    \param posted_hdr FCC value for Posted Header Buffer Space
    \param posted_data FCC value for Posted Data Buffer Space
    \param non_posted_hdr FCC value for Non Posted Header Buffer Space
    \param non_posted_data FCC value for Non Data Header Buffer Space
    \param cpl_hdr FCC value for Completion Header Buffer Space
    \param cpl_data FCC value for Completion Data Buffer Space
  */
  void carbonXPcieSetupBufferSizes(CarbonXPcieTransactor * xtor, CarbonUInt32 vc, 
				   CarbonUInt32 posted_hdr,     CarbonUInt32 posted_data, 
				   CarbonUInt32 non_posted_hdr, CarbonUInt32 non_posted_data, 
				   CarbonUInt32 cpl_hdr,        CarbonUInt32 cpl_data);

  /*!
    \brief Adds a net change callback to a PCIE Transactor output port.
    
    This function registers a callback that will be executed whenever the
    specified output port from the transactor changes after a step function call. 
    The Net object is passed to the callback function, so it can take the appropriate
    action

    \param net A valid PCIE Net ID.
    \param fn The function that is called whenever the specified net
    changes.
    \param userData User-supplied data that gets passed into the
    callback function.
  */
  void carbonXPcieNetSetValueChangeCB(CarbonXPcieNetID *       net,
				      CarbonXPcieNetChangeCBFn fn,
				      CarbonClientData         userData
				      );
  
  /*!
    \brief Sets a value on the specified Net ID

    This function sets a value on a signal net in the PCI Express transactor
    The user should deposit to the transactor input nets when the corresponding 
    Carbon Model output net changes

    \param net A valid PCIE Net ID.
    \param value The value to set on the net
  */
  void carbonXPcieNetDeposit(CarbonXPcieNetID * net, CarbonUInt32 value);

  /*!
    \brief Gets a value from the specified Net ID

    This function sets a value on a signal net in the PCI Express transactor
    The user should deposit to the transactor input nets when the corresponding 
    Carbon Model output net changes

    \param net A valid PCIE Net ID
    \param value The current value on the selected net
  */
  void carbonXPcieNetExamine(CarbonXPcieNetID * net, CarbonUInt32 *value);

  /*!
    \brief Gets the name of the specified Net ID

    This function returns the name of the specified net.

    \param net A valid PCIE Net ID
    \return Name of the selected net
  */
  const char *carbonXPcieNetGetName(CarbonXPcieNetID * net);

  /*!
    \brief Binds a Pcie Net to a CarbonNet

    This function binds CarbonXPcieNet to a CarbonNet, so that CarbonNet value changes
    gets driven into the CarbonPcieNet, and CarbonPcieNet value changes gets driven
    into the CarbonNet.

    \param pcieNet A valid PCIE Net ID
    \param context pointer to a valid Carbon Model
    \param carbonNet pointer to a valid carbonNet, should be coming from a findNet
    \retval true if successful
    \retval false if not successful
  */
  bool carbonXPcieNetBindCarbonNet(CarbonXPcieNetID * pcieNet, CarbonObjectID* context, CarbonNetID* carbonNet);

  
  //! Create a CarbonXPcieTLP object.
  CarbonXPcieTLP * carbonXPcieTLPCreate();
  
  //! Destroy a CarbonXPcieTLP object
  /*!
    \param tlp Pointer to object to destroy.
  */
  void carbonXPcieTLPDestroy(CarbonXPcieTLP *tlp);

  //! Get c-style string for the CarbonXPcieCmdType value.
  /*!
    This function is provided to enhance printing messages.
    \param type The type to get the print string for.
    \return The c-style string for message printing.
  */
  const char* carbonXPcieTLPGetStrFromType (CarbonXPcieCmdType type);

  //! Copy object function
  /*!
    The contents of the CarbonXPcieTLP object passed in will be copied
    to the referenced object.
    \param tlp Pointer to object to copy.
    \param orig A pointer to the CarbonXPcieTLP object to be copied.
  */
  void   carbonXPcieTLPCopy         (CarbonXPcieTLP *tlp, const CarbonXPcieTLP *orig);

  //! Sets the command type for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param cmd The 7-bit value to be set.
  */
  void   carbonXPcieTLPSetCmd       (CarbonXPcieTLP *tlp, CarbonUInt32 cmd);

  //! Gets the command type for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 7-bit command type value.
  */
  CarbonUInt32 carbonXPcieTLPGetCmd       (const CarbonXPcieTLP *tlp);

  //! Sets the command type for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param cmd The CarbonXPcieCmdType value to be set.
  */
  void   carbonXPcieTLPSetCmdByName (CarbonXPcieTLP *tlp, CarbonXPcieCmdType cmd);

  //! Gets the command type for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The CarbonXPcieCmdType command type value.
  */
  CarbonXPcieCmdType carbonXPcieTLPGetCmdByName (const CarbonXPcieTLP *tlp);

  //! Sets the address for the transaction.
  /*!
    If only 32-bits are needed for the transaction then the upper 32-bits
    will be masked out when used.
    \param tlp Pointer to TLP object.
    \param addr The 64-bit value to be set.
  */
  void   carbonXPcieTLPSetAddr      (CarbonXPcieTLP *tlp, CarbonUInt64 addr);

  //! Gets the address for the transaction.
  /*!
    The value will be the same as passed into the setAddr() function,
    even if only 32-bits are used for this transaction.
    \param tlp Pointer to TLP object.
    \return The 64-bit address value.
  */
  CarbonUInt64 carbonXPcieTLPGetAddr      (const CarbonXPcieTLP *tlp);

  //! Sets the requester ID for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param id The 16-bit value to be set.
  */
  void   carbonXPcieTLPSetReqId     (CarbonXPcieTLP *tlp, CarbonUInt32 id);

  //! Gets the requester ID for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 16-bit requester ID.
  */
  CarbonUInt32 carbonXPcieTLPGetReqId     (const CarbonXPcieTLP *tlp);

  //! Sets the tag for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param tag The 8-bit value to be set.
  */
  void   carbonXPcieTLPSetTag       (CarbonXPcieTLP *tlp, CarbonUInt32 tag);

  //! Gets the tag for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 8-bit tag value.
  */
  CarbonUInt32 carbonXPcieTLPGetTag       (const CarbonXPcieTLP *tlp);

  //! Sets the transaction register number and extended register number.
  /*!
    The numeric values are 10-bits wide, corresponding to the concatination
    of the 4-bit extended register number and 6-bit register number.
    \param tlp Pointer to TLP object.
    \param reg The 10-bit value to be set.
  */
  void   carbonXPcieTLPSetRegNum    (CarbonXPcieTLP *tlp, CarbonUInt32 reg);

  //! Gets the transaction register number and extended register number.
  /*!
    This value is 10-bits wide, corresponding to the concatination of
    the 4-bit extended register number and 6-bit register number.
    \param tlp Pointer to TLP object.
    \return The 10-bits extended register number and register number.
  */
  CarbonUInt32 carbonXPcieTLPGetRegNum    (const CarbonXPcieTLP *tlp);

  //! Sets the completer ID for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param id The 16-bit value to be set.
  */
  void   carbonXPcieTLPSetCompId    (CarbonXPcieTLP *tlp, CarbonUInt32 id);

  //! Gets the completer ID for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 16-bit completer ID value.
  */
  CarbonUInt32 carbonXPcieTLPGetCompId    (const CarbonXPcieTLP *tlp);

  //! Sets the message routing type for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param rout The 3-bit value to be set.
  */
  void   carbonXPcieTLPSetMsgRout   (CarbonXPcieTLP *tlp, CarbonUInt32 rout);

  //! Gets the message routing type for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 3-bit message routing type.
  */
  CarbonUInt32 carbonXPcieTLPGetMsgRout   (const CarbonXPcieTLP *tlp);

  //! Sets the message code for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param code The 8-bit value to be set.
  */
  void   carbonXPcieTLPSetMsgCode   (CarbonXPcieTLP *tlp, CarbonUInt32 code);

  //! Gets the message code for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 8-bit message code.
  */
  CarbonUInt32 carbonXPcieTLPGetMsgCode   (const CarbonXPcieTLP *tlp);

  //! Sets the lower address for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param addr The 7-bit value to be set.
  */
  void   carbonXPcieTLPSetLowAddr   (CarbonXPcieTLP *tlp, CarbonUInt32 addr);

  //! Gets the lower address for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 7-bit lower address.
  */
  CarbonUInt32 carbonXPcieTLPGetLowAddr   (const CarbonXPcieTLP *tlp);

  //! Sets the byte count for the transaction.
  /*!
    The Byte Count field in the transaction, which equals the number
    of data bytes sent with a write or requested with a read.  In a
    Completion this may or may not equal the amount of data with the
    transaction.

    \param tlp Pointer to TLP object.
    \param count The 12-bit value to be set.
  */
  void   carbonXPcieTLPSetByteCount (CarbonXPcieTLP *tlp, CarbonUInt32 count);

  //! Gets the data byte count for the transaction.
  /*!
    \return The 12-bit byte count.
  */
  CarbonUInt32 carbonXPcieTLPGetByteCount (const CarbonXPcieTLP *tlp);

  //! Sets the first double-word byte enable for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param be The 4-bit value for the FDWBE.
  */
  void   carbonXPcieTLPSetFirstBE   (CarbonXPcieTLP *tlp, CarbonUInt32 be);

  //! Gets the first double-word byte enable value for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 4-bit value for the FDWBE.
  */
  CarbonUInt32 carbonXPcieTLPGetFirstBE   (const CarbonXPcieTLP *tlp);

  //! Sets the last double word byte enable for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param be The 4-bit value for the LDWBE.
  */
  void   carbonXPcieTLPSetLastBE    (CarbonXPcieTLP *tlp, CarbonUInt32 be);

  //! Gets the last double-word byte enable value for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 4-bit value for the LDWBE.
  */
  CarbonUInt32 carbonXPcieTLPGetLastBE    (const CarbonXPcieTLP *tlp);

  //! Sets the completion status for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param status The 3-bit value to be set. 
  */
  void   carbonXPcieTLPSetCplStatus (CarbonXPcieTLP *tlp, CarbonUInt32 status);

  //! Gets the completion status for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \return The 3-bit completion status.
  */
  CarbonUInt32 carbonXPcieTLPGetCplStatus (const CarbonXPcieTLP *tlp);

  //! Sets the byte data for the transaction.
  //MJH - to be replaced, all data set as DWords, naturally aligned, written as 03_02_01_00.
  /*!
    \param tlp Pointer to TLP object.
    \param index The index of the byte being set.
    \param data The value of the byte being set.
  */
  void   carbonXPcieTLPSetDataByte  (CarbonXPcieTLP *tlp, CarbonUInt32 index, CarbonUInt8 data);

  //! Gets the data DWord at the specified index for the transaction.
  /*!
    If the DWord \a index wasn't written then the return data will be 0x00.
     
    \param tlp Pointer to TLP object.
    \param index The index of the DWord being retrieved.
    \return The DWord at the specified index.
  */
  CarbonUInt32  carbonXPcieTLPGetDWord  (const CarbonXPcieTLP *tlp, CarbonUInt32 index);
   
  //! Sets a Double Word for the transaction.
  /*!
    \param tlp Pointer to TLP object.
    \param index The index of the DWord being set.
    \param data The value of the DWord being set.
  */
  void   carbonXPcieTLPSetDWord  (CarbonXPcieTLP *tlp, CarbonUInt32 index, CarbonUInt32 data);

  //! Gets the data byte at the specified index for the transaction.
  //MJH - to be replaced, all data set as DWords, naturally aligned, written as 03_02_01_00.
  /*!
    If the byte \a index wasn't written then the return data will be 0x00.

    \param tlp Pointer to TLP object.
    \param index The index of the byte being retrieved.
    \return The data byte at the specified index.
  */
  CarbonUInt8  carbonXPcieTLPGetDataByte  (const CarbonXPcieTLP *tlp, CarbonUInt32 index);

  //! Resizes the byte data buffer
  /*!
    Either extend or truncate the byte data buffer.  If \a start is greater
    than zero then all elements from zero to \a start will be removed.  If
    \a end is before the last element, then all after it will be removed,
    otherwise zero data will be appended to the byte data buffer.

    \param tlp Pointer to TLP object.
    \param start The index of the first element to keep.
    \param end The index of the last element to keep or create.
  */
  void   carbonXPcieTLPResizeDataBytes  (CarbonXPcieTLP *tlp, CarbonUInt32 start, CarbonUInt32 end);

  //! Gets the number of bytes in the data queue
  /*!
    The length of the data queue is from byte 0 through the last byte written.  If only
    byte 11 is written, then the data queue length is 12 bytes.

    \param tlp Pointer to TLP object.
    \return The number of bytes of data set for the tranaction
  */
  CarbonUInt32 carbonXPcieTLPGetDataByteCount (const CarbonXPcieTLP *tlp);

  //! Sets the data length value in double-words
  /*!
    \param tlp Pointer to TLP object.
    \param length The 10-bit data length for the transaction
  */
  void   carbonXPcieTLPSetDataLength (CarbonXPcieTLP *tlp, CarbonUInt32 length);
  
  //! Gets the data length in double-words
  /*!
    \param tlp Pointer to TLP object.
    \return The 10-bit data length for the transaction
  */
  CarbonUInt32 carbonXPcieTLPGetDataLength (const CarbonXPcieTLP *tlp);
  
  //! Gets the transaction ID
  /*!
    The transaction ID is a concatination of the Requester ID and the Tag values.  On completion
    transactions this information links the completion to the original request.  For all other
    transactions the return value is undefined.
    \param tlp Pointer to TLP object.
    \return The transaction ID of the original request on completions.
  */
  CarbonSInt32 carbonXPcieTLPGetTransId   (const CarbonXPcieTLP *tlp);
  
  //! Set Start Tick of the transaction
  /*!
    \param tlp Pointer to TLP object.
    \param tick Tick count for start of transaction.
  */
  void   carbonXPcieTLPSetStartTick (CarbonXPcieTLP *tlp, CarbonUInt64 tick);

  //! Get Start Tick of the transaction
  /*!
    \param tlp Pointer to TLP object.
  */
  CarbonUInt64 carbonXPcieTLPGetStartTick (const CarbonXPcieTLP *tlp);
  
  //! Set End Tick of the transaction
  /*!
    \param tlp Pointer to TLP object.
    \param tick Tick count for end of transaction.
  */
  void   carbonXPcieTLPSetEndTick (CarbonXPcieTLP *tlp, CarbonUInt64 tick);
  
  //! Get End Tick of the transaction
  /*!
    \param tlp Pointer to TLP object.
  */
  CarbonUInt64 carbonXPcieTLPGetEndTick (const CarbonXPcieTLP *tlp);
  
  // Ease of use functions
  //! Gets whether the transaction is a read
  /*!
    \param tlp Pointer to TLP object.
    \retval true If the command is a memory, i/o, or config read
    \retval false If the command is not a read
  */
  CarbonUInt32  carbonXPcieTLPIsRead       (const CarbonXPcieTLP *tlp);

  //! Gets whether the transaction is a write
  /*!
    \param tlp Pointer to TLP object.
    \retval true If the command is a memory, i/o, or config write
    \retval false If the command is not a write
  */
  CarbonUInt32 carbonXPcieTLPIsWrite      (const CarbonXPcieTLP *tlp);
  
  
  //! Build a Memory Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param num_dwords Number of DWORDS to read, goes in the Length Field of the TLP
    \param first_be First BE Field
    \param last_be  Last  BE Field
  */
  void carbonXPcieTLPMemReadReq(CarbonXPcieTLP *tlp, 
				CarbonUInt8 tag, 
				CarbonUInt64 addr, CarbonUInt32 num_dwords, 
				CarbonUInt32 first_be, CarbonUInt64 last_be);
  
  //! Build Completer TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory Completer Transaction. 
    The function populates the following fields in the supplied TLP:
    Type, Length, Tag, Requester ID HDW Byte Count, Lower Address and Data
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param req_id Bus-, Device- and Funcion- Number of the corresponding Requester Transaction 
    \param tag Tag of the corresponding Requester Transaction
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS in the TLP
    \param byte_count Byte Count Field
    \param lower_addr Lower Address Field
  */
  void carbonXPcieTLPCompleter(CarbonXPcieTLP *tlp, 
			       CarbonUInt32 req_id, CarbonUInt8 tag, 
			       const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
			       CarbonUInt8  byte_count, CarbonUInt8 lower_addr);

  //! Build a Memory Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Memory
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS to write, goes in the Length Field of the TLP
    \param first_be First BE Field
    \param last_be  Last  BE Field
  */
  void carbonXPcieTLPMemWriteReq(CarbonXPcieTLP *tlp, 
				 CarbonUInt8 tag, 
				 CarbonUInt64 addr, const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
				 CarbonUInt32 first_be, CarbonUInt64 last_be);
  
  //! Build an IO Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a IO
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param first_be First BE Field
  */
  void carbonXPcieTLPIORead32Req(CarbonXPcieTLP *tlp, 
				 CarbonUInt8 tag, 
				 CarbonUInt64 addr,
				 CarbonUInt32 first_be);

  //! Build a IO Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a IO
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs. Bit 1:0 of \a addr must be 0
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void carbonXPcieTLPIOWrite32Req(CarbonXPcieTLP *tlp, 
				  CarbonUInt8 tag, 
				  CarbonUInt64 addr, CarbonUInt32 data, 
				  CarbonUInt32 first_be);
  
  //! Build an Cfg 0 Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr Address HDW
    \param first_be First BE Field
  */
  void carbonXPcieTLPCfg0ReadReq(CarbonXPcieTLP *tlp, 
				 CarbonUInt8 tag, 
				 CarbonUInt32 addr, 
				 CarbonUInt32 first_be);
  
  //! Build an Cfg 1 Read Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 1
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE and Address
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr Address HDW
    \param first_be First BE Field
  */
  void carbonXPcieTLPCfg1ReadReq(CarbonXPcieTLP *tlp, 
				 CarbonUInt8 tag, 
				 CarbonUInt32 addr, 
				 CarbonUInt32 first_be);

  //! Build a Cfg 0 Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs.
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void carbonXPcieTLPCfg0WriteReq(CarbonXPcieTLP *tlp,
				 CarbonUInt8 tag, 
				 CarbonUInt32 addr, CarbonUInt32 data, 
				 CarbonUInt32 first_be);
  
  //! Build a Cfg 1 Write Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 1
    Write Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, Last BE, First BE, Address and Data DW
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param addr 64 Bit Address HDWs.
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param first_be First BE Field
  */
  void carbonXPcieTLPCfg1WriteReq(CarbonXPcieTLP *tlp,
				 CarbonUInt8 tag, 
				 CarbonUInt32 addr, CarbonUInt32 data, 
				 CarbonUInt32 first_be);
  
  //! Build an Message Base Requester TLP in the specified TLP \a tlp
  /*! Higher Level Interface for populating the fields in a Cfg 0
    Read Request Transaction. The function populates the following fields
    in the supplied TLP:
    Type, Length, Tag, MsgRout and MsgCode
    The other fields are left unchanged!
    \param tlp Pointer to TLP to build
    \param tag Requester Transaction Tag for the TLP
    \param data Pointer to a buffer of DWORDS to put in the TLP, must hold space for \a num_words DWORDS
    \param num_dwords Number of DWORDS in the TLP
    \param rout Msg Rout Field
    \param msg_code Msg Code Field
  */
  void carbonXPcieTLPMsgBaseReq(CarbonXPcieTLP *tlp, 
				CarbonUInt8 tag, 
				const CarbonUInt32 *data, CarbonUInt32 num_dwords, 
				CarbonUInt8 rout, CarbonUInt8 msg_code);

/*!
  @}
*/

#ifdef __cplusplus
}
#endif

#endif
