/***************************************************************************************
  Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONXPCIENET_H_
#define _CARBONXPCIENET_H_

#include "carbon/carbon_shelltypes.h"
#include "CarbonXString.h"
#include "util/c_memmanager.h"
#include "carbonXPcie.h"

class CarbonXPcieNet
{

 public: CARBONMEM_OVERRIDES;

  enum NetDirection {
    eInput,
    eOutput,
    eUnknown
  };

  CarbonXPcieNet(NetDirection direction);
  CarbonXPcieNet(NetDirection direction, const char *name);
  ~CarbonXPcieNet(void) {};

  const char * getName(void);
  void setName(const char * name);
  
  UInt32 getBitWidth(void);
  void   setBitWidth(UInt32 bit_width);

  void deposit(const UInt32 * value);
  void examine(UInt32 * value);
  
  void setChangeCBFn(CarbonXPcieNetChangeCBFn *fn, CarbonClientData userData);
  
  bool bindCarbonNet(CarbonObjectID* context, CarbonNetID* carbonNet);

 private:
  static void carbonValueChangeCB(CarbonObjectID*, CarbonNetID*, CarbonClientData, CarbonUInt32*, CarbonUInt32*);
  void valueChange(UInt32 new_value);

  CarbonXString              mName;
  UInt32                     mBitWidth;
  NetDirection               mDirection;
  UInt32                     mState;
  CarbonXPcieNetChangeCBFn * mCBFunc;
  CarbonClientData           mCBUserData;
  CarbonObjectID *           mCarbonDepositContext;
  CarbonNetID *              mCarbonDepositNet;

};

#endif

