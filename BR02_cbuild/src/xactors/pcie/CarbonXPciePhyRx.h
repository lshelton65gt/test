// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPciePhyRx_H_
#define __CarbonXPciePhyRx_H_

#include "carbonXPcie.h"
#include "CarbonXPcieNet.h"
#include "CarbonXPcieScrambler.h"
#include "CarbonXPciePhyLayer.h"
#include "CarbonXArray.h"
#include "CarbonXList.h"

class CarbonX8b10b;
class CarbonXPcieConfig;

//! A streaming packet buffer.
/*!
   The Packet streams stores packet symbols on a list
   and marks where one packet end the next one starts.
*/
class CarbonXPciePktIStream : public CarbonXPcieSymList {
public : 
  CARBONMEM_OVERRIDES
  
  //! Constructor
  CarbonXPciePktIStream() { }
  
  //! Store Symbol in the buffer
  void operator<<(const CarbonXPcieSym& sym) {
    push(sym);
  }

  bool isOS() const {
    if(!mBuf.front().empty()) {
      return mBuf.front().front() == SYM_COM;
    }
    else return false;
  }
  bool isSKP() const {
    CarbonXList<CarbonXPcieSym>::const_iterator iter = mBuf.front().begin();

    // Check if first symbol is COM
    if(iter == mBuf.front().end())  return false;
    if(*iter != SYM_COM) return false;

    // Check that second is SKP
    if(++iter == mBuf.front().end()) return false;
    if(*iter != SYM_SKP)  return false;

    // No need to check any further, because it was checked when it was put into
    // the list
    return true;
  }
  bool isIDL() const {
    CarbonXList<CarbonXPcieSym>::const_iterator iter = mBuf.front().begin();

    // Check if first symbol is COM
    if(iter == mBuf.front().end())  return false;
    if(*iter != SYM_COM) return false;

    // Check that second is IDL
    if(++iter == mBuf.front().end()) return false;
    if(*iter != SYM_IDL)  return false;

    // No need to check any further, because it was checked when it was put into
    // the list
    return true;
  }
  bool isFTS() const {
    CarbonXList<CarbonXPcieSym>::const_iterator iter = mBuf.front().begin();

    // Check if first symbol is COM
    if(iter == mBuf.front().end())  return false;
    if(*iter != SYM_COM) return false;

    // Check that second is FTS
    if(++iter == mBuf.front().end()) return false;
    if(*iter != SYM_FTS)  return false;

    // No need to check any further, because it was checked when it was put into
    // the list
    return true;
  }

  //! Returns true Packet is a TS1 or TS2, otherwise returns false
  bool isTS() const {
    CarbonXList<CarbonXPcieSym>::const_iterator iter = mBuf.front().begin();

    // Check if first symbol is COM
    if(iter == mBuf.front().end())  return 0;
    if(*iter != SYM_COM) return 0;
    
    // Check if next symbol is PAD or data
    if(++iter == mBuf.front().end())  return 0;
    if(*iter != SYM_PAD && !iter->isData() ) return 0;

    // Dont check anyhing else here, if this is not a correct TS, it will fail elsewhere
    return 1;
  }

  bool isPacket() const {
    if(!mBuf.front().empty()) {
      return mBuf.front().front() == SYM_SDP || mBuf.front().front() == SYM_STP;
    }
    else return false;
  }
};

class CarbonXPciePhyRx {
 public: CARBONMEM_OVERRIDES;
  
  struct LinkConfig {
    UInt8 mLinkNum;
    UInt8 mNFts;
    UInt8 mDataBitRate;
    UInt8 mTrainCtrl;
  };

  //! Class For Storing Training Sequence Parameter Info
  class TSType {
    UInt32 mLinkWidth;
    bool   mIsTs1;
    CarbonXArray<UInt32> mLinkNum, mLaneNum, mNFts, mDataRate, mCtrl;
    bool   mValid;

  public: CARBONMEM_OVERRIDES;
    TSType(UInt32 linkwidth) : mLinkWidth(linkwidth), mIsTs1(true),
			       mLinkNum(linkwidth), mLaneNum(linkwidth), 
			       mNFts(linkwidth), mDataRate(linkwidth), 
                               mCtrl(linkwidth), mValid(true) {
    }
    
    TSType(UInt32 linkwidth, CarbonXList<CarbonXPcieSym>& list) :
      mLinkWidth(linkwidth), mIsTs1(true),
      mLinkNum(linkwidth), mLaneNum(linkwidth), 
      mNFts(linkwidth), mDataRate(linkwidth), 
      mCtrl(linkwidth), mValid(false) {
      
      // First check is the list has correct size, if not
      // the TS is corrupt. Just return.
      if(list.size() != (11 + 5*linkwidth)) return;

      // First Symbol
      CarbonXList<CarbonXPcieSym>::iterator iter = list.begin();

      // Skip over commma
      ++iter;
      
      // Extract Link Number
      for(UInt32 i = 0; i < linkwidth; i++) {
        mLinkNum[i] = (*iter == SYM_PAD) ? 255 : iter->mData;
        ++iter;
      }

      // Extract Lane Number
      for(UInt32 i = 0; i < linkwidth; i++) {
        mLaneNum[i] = (*iter == SYM_PAD) ? 255 : iter->mData;
        ++iter;
      }

      // Extract Number of Fts's
      for(UInt32 i = 0; i < linkwidth; i++) {
        mNFts[i] = iter->mData;
        ++iter;
      }
      
      // Extract Data Rate
      for(UInt32 i = 0; i < linkwidth; i++) {
        mCtrl[i] = iter->mData;
        ++iter;
      }

      // Extract Link Control
      for(UInt32 i = 0; i < linkwidth; i++) {
        mCtrl[i] = iter->mData;
        ++iter;
      }

      // Check if it's TS1 or TS2
      if(*iter == 0x4A) {
        mIsTs1 = true;
        mValid = true;
      }
      else if(*iter == 0x45) {
        mIsTs1 = false;
        mValid = true;
      }
      else
        mValid = false;
    }
    
    bool operator==(const TSType &other) {
      return ((mIsTs1    == other.mIsTs1) &&
	      (mLinkNum  == other.mLinkNum) &&
	      (mLaneNum  == other.mLaneNum) &&
	      (mNFts     == other.mNFts) &&
	      (mDataRate == other.mDataRate) &&
	      (mCtrl     == other.mCtrl) &&
              (mValid    == other.mValid));
    }
    
    inline void setTs1() { mIsTs1 = true; }
    inline void setTs2() { mIsTs1 = false; }
    inline void setLinkNum(UInt32 lane, UInt8 num)  { mLinkNum[lane]  = num; }
    inline void setLaneNum(UInt32 lane, UInt8 num)  { mLaneNum[lane]  = num; }
    inline void setNFts(UInt32 lane, UInt8 num)     { mNFts[lane]     = num; }
    inline void setDataRate(UInt32 lane, UInt8 num) { mDataRate[lane] = num; }
    inline void setCtrl(UInt32 lane, UInt8 num)     { mCtrl[lane]     = num; }
    
    inline UInt8 getNFts(UInt32 lane) const { return mNFts[lane]; }
    inline UInt8 getDataRate(UInt32 lane) const { return mDataRate[lane]; }
    inline UInt8 getCtrl(UInt32 lane) const { return mCtrl[lane]; }
    inline bool isValid() const { return mValid; }
    inline bool isTs1() const {return mIsTs1; }
    inline bool isTs2() const { return !mIsTs1; }
    
    inline bool checkTs1PadPad() const {
      if(isTs2()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] != 255 || mLaneNum[i] != 255) return false;
    }
      return true;
    }
    inline bool checkTs2PadPad() const {
      if(isTs1()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] != 255 || mLaneNum[i] != 255) return false;
      }
      return true;
    }
    inline bool checkTs1LinkPad(UInt8 link_num) const {
      if(isTs2()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] != link_num || mLaneNum[i] != 255) return false;
      }
      return true;
    }
    inline bool checkTs1NotPadPad() const {
      if(isTs2()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] == 255 || mLaneNum[i] != 255) return false;
      }
      return true;
    }
    inline bool checkTs2NotPadPad() const {
      if(isTs1()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] == 255 || mLaneNum[i] != 255) return false;
      }
      return true;
    }
    inline bool checkTs2LinkPad(UInt8 link_num) const {
      if(isTs1()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] != link_num || mLaneNum[i] != 255) return false;
      }
      return true;
    }
    inline bool checkTs1LinkLane(UInt8 link_num) const {
      if(isTs2()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] != link_num || mLaneNum[i] != i) return false;
      }
      return true;
    }
    inline bool checkTs2LinkLane(UInt8 link_num) const {
      if(isTs1()) return false;
      for(UInt32 i = 0 ; i < mLinkWidth; i++) {
	if(mLinkNum[i] != link_num || mLaneNum[i] != i) return false;
      }
      return true;
    }

    void writeToOStream(UtOStream* ostr);
  };

  enum OSType {
    eSkipOS,
    eEIdleOS,
    eFtsOS,
    eTs1OS,
    eTs2OS,
    ePacket,
    eNone
  };
  
  class Status {
  public :
    Status(int linkwidth) : 
      mConsecOSs(0), mConsecTSs(0), 
      mConsecIdles(0), mLastOS(eNone), mLastTS(linkwidth)
    { }

    void rcvdOS(OSType os) {
      if(mLastOS == os) ++mConsecOSs;
      else {
        mLastOS   = os;
        mConsecOSs = 1;
        mConsecTSs = 1;
      }
    }

    void rcvdTS(TSType ts) {
      if(ts == mLastTS)
        ++mConsecTSs;
      else {
        mLastTS    = ts;
        mConsecTSs = 1;
      }
    }

    UInt32   mConsecOSs;
    UInt32   mConsecTSs;
    UInt32   mConsecIdles;
    OSType   mLastOS;
    TSType   mLastTS;
  };

  CarbonXPciePhyRx(CarbonXPcieConfig& cfg, CarbonXPciePhyLayer *parent, CarbonXPcieIntfType intf);

  ~CarbonXPciePhyRx();
  
  // -------------------------
  // Public Interfaces (Used by CarbonXPciePhyLayer)

  //! This method needs to get called every symbol period
  void step();

  //! Set LTSSM State
  void setLinkState(pcie_LTSM_state_t state);
  
  //! Get LTSSM State
  pcie_LTSM_state_t getLinkState() const;

  //! Set Debug Level
  void verbose(UInt32);

  //! Get number of Consectutive Logical Idles
  /* Return number of cycles that the inputs have seen
     Logical Idles, since the last non Idle cycle. If the
     inputs are not currently seing Idles, 0 is returned.
     \return Number Of Idles
  */
  UInt32 getConsecIdles() const { return mStatus.mConsecIdles; }

  //! Get number of Consectutive OSs 
  /* Return number of consecutive Ordered Sets of the same kind
     that has arrived at the input since the last time any other
     type of OS/Packet or Idles were seen on the inputs. The type
     of OS counted can be found by the getLastOS method. This
     method does not return the cound of Training Set OS's as
     those has it's own method. The count is meant to be used 
     by the LTSSM to determine state changes.
     \return Number Of OS's
  */
  UInt32 getConsecOSs() const { return mStatus.mConsecOSs; }

  //! Get Last Ordered Set seen on the inputs
  /* Get the type of the last ordered set that arrived
     on the inputs. The number of consecutive OS's of this
     type received can be found using the getConsecOSs
     method.
     \return Type of OS
  */
  OSType & getLastOS() { return mStatus.mLastOS; }

  //! Get number of Consectutive TS's 
  /* Return number of consecutive Training Sequence
     Ordered Sets of the same type that has arrived since any
     other type of Training Sequence Ordered set arrived.
     With any other type is meant TS1 or TS2, Link # has a 
     PAD symbol or not and Lane # has a PAD symbol or not.
     The count is meant to be used by the LTSSM to 
     determine state changes.
     \return Number Of TS's
  */
  UInt32 getConsecTSs() const { return mStatus.mConsecTSs; }

  //! Get Last Training Sequence Ordered Set seen on the inputs
  /* Get the last training sequence that arrived on the input.
     The number of consecutive TS's of the same
     type received can be found using the getConsecTSs
     method.
     \return Type of TS
  */  
  TSType & getLastTS() { return mStatus.mLastTS; }

  //! Clear Number of Consecutive TS's 
  void clearConsecTSs() { mStatus.mConsecTSs = 0; }

  //! Clear Number of Consecutive OS's
  void clearConsecOSs() { mStatus.mConsecOSs = 0; }

  //! Reset Phy Receiver
  void reset();

  //! Try to get Physical Packet
  /* The methods checks if there is a packet in the receive
     queue and if there is puts the packet in \apkt
     \retval true if a packet was put in \apkt
     \retval false if a packet was not put in \pkt
  */
  bool getPhysPacket(CarbonXPciePhyLayer::PhysPacket *pkt);
  bool getPhysPacket(CarbonXList<CarbonXPcieSym>* pkt);
  
  // -------------------------
  // Public Interfaces (Used by Signal Interface)

  //! Put One cycle worth of data to send out on the interface
  /*! Note that this method could be called more than once in a time cycle
    if the Signal interface can handle more than one byte per lane.
    \param data Reference top the next set of cycle data for all lanes.
  */
  void putCycleData(CarbonXPcieCycleData& data);

private:

  enum StateT {
    eIdle,
    eInPacket,
    eInOs,
    eInTSOs
  };

  void parse(CarbonXPcieSym* data, bool* eIdle);
  inline void gotoIdle() {
    mState   = eIdle;
    mOsCycle = 0;
    //mPacket.mark(); // << CarbonXPciePktIStream::endp;
  }
  
  // Configuration Structure
  CarbonXPcieConfig&    mCfg;

  CarbonXPciePktIStream mPacket;
  CarbonXPcieScrambler  mScrambler;

  StateT                mState;
  int                   mOsCycle;
  CarbonXPcieSym        mExpSym;
  TSType                mCurrTs;

  // Transactor Parameters
  CarbonXPciePhyLayer * mParent;
  CarbonXPcieIntfType   mIntfType;           // what is the interface type (10bit, pipe, other)
  pcie_LTSM_state_t     mLinkState;
  Status                mStatus;

  // Receiver Requested Lane Config
  LinkConfig mRxReqConfig;
  
  // Received DLLP's and LLTP's
  CarbonXList<CarbonXPciePhyLayer::PhysPacket> mRecvdPackets;
  CarbonXPcieSymList mPacketList;
};

  
#endif
