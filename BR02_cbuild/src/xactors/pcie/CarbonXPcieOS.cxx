/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonXPcieOS.h"

// transaction special characters
// Define the Data Bytes associated with the Special Character Symbol Codes
#define PCIE_COM	0xBC	// K28.5 // Used for Lane and Link Initialization
#define PCIE_STP	0xFB	// K27.7 // Marks the start of a Transaction Layer Packet
#define PCIE_SDP	0x5C	// K28.2 // Marks the start of a Data Link Layer Packet
#define PCIE_END	0xFD	// K29.7 // Marks the end of a TLP or DLLP
#define PCIE_EDB	0xFE	// K30.7 // Marks the end of a nullified TLP
#define PCIE_PAD	0xF7	// K23.7 // Used in Framing and Link Width and Lane ordering negotiations
#define PCIE_SKP	0x1C	// K28.0 // Used for compensating for different bit rates for two communicating ports
#define PCIE_FTS	0x3C	// K28.1 // Used within an ordered set to exit from L0s to L0 (standby to idle)
#define PCIE_IDL	0x7C	// K28.3 // Symbol used in the Electrical Idle ordered set
#define PCIE_RSVD1	0x9C	// K28.4 // RESERVED
#define PCIE_RSVD2	0xDC	// K28.6 // RESERVED
#define PCIE_RSVD3	0xFC	// K28.7 // RESERVED

CarbonXPcieOS::CarbonXPcieOS(UInt32 linkwidth) {
  mLinkWidth = linkwidth;
  mCurrCycle = 0;
  mOsSize    = 0;
  mScramble  = false;
  mCycleData = CARBON_ALLOC_VEC(UInt32, linkwidth);
  mCycleSym  = CARBON_ALLOC_VEC(bool, linkwidth);
}

CarbonXPcieOS::~CarbonXPcieOS() {
  CARBON_FREE_VEC(mCycleData, UInt32, mLinkWidth);
  CARBON_FREE_VEC(mCycleSym, bool, mLinkWidth);
}

void CarbonXPcieOS::skipOs() {
  mScramble  = false;
  mOsSize    = 4;
  mCurrCycle = 0;
  mData.resize(mOsSize*mLinkWidth);
  mSym.resize(mOsSize*mLinkWidth);
  for(UInt32 i=0; i < mOsSize; i++) {
    for(UInt32 j=0; j < mLinkWidth; j++) {
      mData[((i*mLinkWidth)+j)] = ((i==0) ? PCIE_COM : PCIE_SKP);
      mSym[((i*mLinkWidth)+j)] = true;
    }
  }
}

void CarbonXPcieOS::ftsOs(UInt32 repeats) {
  mScramble  = false;
  mOsSize    = 4 * repeats;
  mCurrCycle = 0;
  mData.resize(mOsSize*mLinkWidth);
  mSym.resize(mOsSize*mLinkWidth);
  for(UInt32 i=0; i < mOsSize; i++) {
    for(UInt32 j=0; j < mLinkWidth; j++) {
      mData[((i*mLinkWidth)+j)] = ((i%4) ? PCIE_COM : PCIE_FTS);
      mSym[((i*mLinkWidth)+j)] = true;
    }
  }
}

void CarbonXPcieOS::eIdleOs(){
  mScramble  = false;
  mOsSize    = 4;
  mCurrCycle = 0;
  mData.resize(mOsSize*mLinkWidth);
  mSym.resize(mOsSize*mLinkWidth);
  for(UInt32 i=0; i < mOsSize; i++) {
    for(UInt32 j=0; j < mLinkWidth; j++) {
      mData[((i*mLinkWidth)+j)] = ((i==0) ? PCIE_COM : PCIE_IDL);
      mSym[((i*mLinkWidth)+j)] = true;
    }
  }
}
void CarbonXPcieOS::tsOs(bool                    this_is_ts1, 
			 bool                    useLinkPad, 
			 bool                    useLanePad,
			 UInt8                   linkNum,
			 UInt8                   nFts,
			 UInt8                   dataRate,
			 UInt8                   trainCtrl) {

  mScramble  = false;
  mOsSize    = 16;
  mCurrCycle = 0;
  mData.resize(mOsSize*mLinkWidth);
  mSym.resize(mOsSize*mLinkWidth);
  for(UInt32 i=0; i < mLinkWidth; i++) {
    mData[0*mLinkWidth +i] = PCIE_COM;
    mSym[0*mLinkWidth +i]  = true;
    
    // Link Number or PAD
    mData[1*mLinkWidth +i] = useLinkPad ? PCIE_PAD : linkNum;
    mSym[1*mLinkWidth +i]  = useLinkPad;
    
    // Lane Number or PAD
    mData[2*mLinkWidth +i] = useLanePad ? PCIE_PAD : i;
    mSym[2*mLinkWidth +i]  = useLanePad;

    // N_FTS
    mData[3*mLinkWidth +i] = nFts;
    mSym[3*mLinkWidth +i]  = false;
    
    // Data Rate
    mData[4*mLinkWidth +i] = dataRate;
    mSym[4*mLinkWidth +i]  = false;
    
    // Training Control
    mData[5*mLinkWidth +i] = trainCtrl;
    mSym[5*mLinkWidth +i]  = false;
    
    // TS1/2 Identifier
    for(UInt32 j=6; j < 16; j++) {
      mData[j*mLinkWidth +i] = this_is_ts1 ? 0x4a : 0x45;
      mSym[j*mLinkWidth +i]  = false;
    }
      
  }
}

void CarbonXPcieOS::logicalIdles(UInt32 repeats) {
  mScramble  = true;;
  mOsSize    = repeats;
  mCurrCycle = 0;
  mData.resize(mOsSize*mLinkWidth);
  mSym.resize(mOsSize*mLinkWidth);
  for(UInt32 i=0; i < mOsSize; i++) {
    for(UInt32 j=0; j < mLinkWidth; j++) {
      mData[((i*mLinkWidth)+j)] = 0;
      mSym[((i*mLinkWidth)+j)] = false;
    }
  }
}

const UInt32 * CarbonXPcieOS::getData() const {
  if(mCurrCycle >= mOsSize)
    return 0;
  else {
    for(UInt32 i = 0; i < mLinkWidth; i++)
      mCycleData[i] = mData[(mCurrCycle * mLinkWidth) + i]; 
    return mCycleData;
  }
}

const bool * CarbonXPcieOS::getSym() const {
  if(mCurrCycle >= mOsSize)
    return 0;
  else {
    for(UInt32 i = 0; i < mLinkWidth; i++)
      mCycleSym[i] = mSym[(mCurrCycle * mLinkWidth) + i]; 
    return mCycleSym;
  }
}

CarbonXPcieOS & CarbonXPcieOS::operator++() {
  ++mCurrCycle;
  return *this;
}

bool CarbonXPcieOS::doScramble() {
  return mScramble;
}
